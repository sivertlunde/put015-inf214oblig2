package task5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		DollFactory dcb = new DollFactory();
		dcb.execution(100);
	}
}

class Doll { 
	int id;
	int qualityScoreMachine;
	boolean imperfect, isPainted;
	
	public Doll(int id, int qualityScoreMachine) {
		this.id = id;
		this.qualityScoreMachine = qualityScoreMachine;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQualityScore() {
		return qualityScoreMachine;
	}
	public void setQualityScoreMachine(int qualityScoreMachine) {
		this.qualityScoreMachine = qualityScoreMachine;
	}
	public boolean isImperfect() {
		return imperfect;
	}
	public void hasImperfections(boolean imperfect) {
		this.imperfect = imperfect;
	}
	public boolean isPainted() {
		return isPainted;
	}
	public void setPainted(boolean isPainted) {
		this.isPainted = isPainted;
	}

	@Override
	public String toString() {
		return "Doll [id=" + id + ", qualityScoreMachine=" + qualityScoreMachine + ", imperfect=" + imperfect
				+ ", isPainted=" + isPainted + "]";
	}
	
	
}

class DollFactory {
	List<Doll> dolls;
	private CyclicBarrier stageA, stageB, stageC;
	
	public void execution(int dollsNumber) throws InterruptedException {
		stageA = new CyclicBarrier(dollsNumber); 
		stageB = new CyclicBarrier(dollsNumber); 
		stageC = new CyclicBarrier(dollsNumber + 1); 
		dolls = new ArrayList<>(dollsNumber);
		for (int i = 0; i < dollsNumber; i++) { 
			Process task = new Process(i);
			Thread t = new Thread(task);
			t.start();
		}
		try {
			stageC.await(); 
			System.out.println("Packaging process D\n"); 
			for (Doll d : dolls) {
				System.out.println(d.toString());
			}
			System.out.println("\nNumber of dolls which passed quality control: " + dolls.size());
		} catch (BrokenBarrierException e) { 
			e.printStackTrace(); 
		}
	}
	
	// class Process implements Runnable ...
	class Process implements Runnable { 
		int id;
		
		public Process(int id) { 
			this.id = id; 
		} 
		
		public void run() {
	        try {
				Doll d = assembly();
				stageA.await();
				painting(d);
				stageB.await();
				qualityControl(d);
				stageC.await();
			} catch (BrokenBarrierException | InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		void painting(Doll d) { 
			d.setPainted(true); 
		} 
		
		Doll assembly() {
	       Random r = new Random();
	       return new Doll(id, r.nextInt(4) + 7); 
	    }
		
		void qualityControl(Doll d) {
			if (d.getQualityScore() >= 9) {
				d.hasImperfections(false); 
				dolls.add(d); 
			}
		}
	}
}







