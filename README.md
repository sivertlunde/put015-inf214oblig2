# put015-inf214oblig2

## Task 1 
In this task, we use the Runnable interface. This is a simple interface for implementing code that should be executed in a seperate thread. This is done by overriding the run()-method. This is the method that is called when a thread is started by executing thread.start(). 
In the waitForThreads()-method, we invoke the method thread.join(). This method simply waits for the thread to die, meaning that the thread has finished executing it's code. Once the thread is dead, the program moves on. 

**Results**

The concurrent MatrixMult-class isn't always faster than the sequential SerialMult-class. When testing various sizes of matrixes, I found that SerialMult was faster until the matrixes reached a size of 800x800. This is because starting a thread comes at a certain cost. If the problem is small enough, the cost of starting multiple threads will result in a longer execution time than simply executing everything sequentially. However, when the problem reaches a certain size, the performance boost of multiple threads will outweigh the cost of starting them. In the case of my computer this happened when the matrixes reached a size of roughly 800x800, but this number will vary based on the processor running the program.  


## Task 2

#### 2.1
This method looks at the HashMap<> invertedIndex, finds the mapping for the given keyword "query" if there is a mapping for it, and returns the value associated with the key as a table of Strings. InvertedIndex is a mapping from (words) -> (files containing that word). As words are found in files and registered in the HashMap, the file in question is appended to the value of the mapping, seperated by ";". So the method searchQuery() seperates the filenames by splitting on ";".

#### 2.2
The class IndexingTask implements the Callable interface. This interface is very similar in functionality and implementation to the Runnable interface. The main difference is that in the Callable interface, you override a method "call()" which can return something - whereas in the Runnable interface, you override a void method "run()". In addition to being able to return something, the call()-method can handle exceptions - something run() cannot do.

The implementation of the call() method here, returns a Document. It first uses DocumentParser's method for parsing a file - creates a document with the class variable "file" and the hashMap returned from DocumentParser then returns the newly created Document.

#### 2.3
The class InvertedIndexTask implements the Runnable interface. This is explained in further detail in task 1. In the run()-method here, it loops while the thread is not interrupted. In this first loop, it tries to get a Document from any finished task. Finished tasks are stored in the completionService variable where it waits to be handled. The InvertedIndexTask gets a document from a task that is waiting in the completionService, or it waits for one to complete if there aren't any there. 

If the thread finds that it is interrupted when checking Thread.interrupted(), it moves on to the second loop where it tries to get a document by calling the .poll()-method, rather than the .take()-method. The difference between the two is that .poll() doesn't wait for a finished task if there are none - it just returns null in that case. 

If the method at any point catches an InterruptedException, it calls the method Thread.interrupt() in the catch-block. This is because the interrupted status flag of the thread is set to false after an InterruptedException is thrown, so by calling Thread.interrupt() you ensure that the thread's interruption flag is still true after catching the exception.

In the updateInvertedIndex()-method, you can see that a shared resource invertedIndex is accesses and updated without any specified mutual exclusion. That is because it is a ConcurrentHashMap, rather than a regular HashMap, so the ConcurrentHashMap deals with synchronization itself. That doesn't mean that it locks the map when it is being accesses, but it is thread safe.

#### 2.4
Here we have the declaration of the ConcurrentHashMap invertedIndex mentioned above in 2.3. This is also further described/explained in 2.3. 

We also have a ExecutorCompletionService completionService that is mentioned previously. This is a service which is declared with an executor that handles tasks, and which holds the completed tasks by that executor in a queue. 

The executor is declared in line 13 as a ThreadPoolExecutor. Just before the declaration of the executor, we use Runtime.getRuntime().availableProcessors() to check which processing resources we have available to use. This number of processors is given to the ThreadPoolExecutor which will then distribute it's tasks amongst it's available processors. 

Next, we start two instances of the InvertedIndexTask class in seperate threads. 

We then loop over the files given to the index()-method and submit each file to the completionService as IndexingTasks. If the number of tasks exceeds the number of processors available to the executor, they are added to the executors's queue. In the code, we check to see if that queue has exceeded a size of 1000, and if it has, we tell the thread to sleep until the queue is smaller than 1000.

Finally, we stop the executor by calling the .shutdown()-method. This method doesn't immediately kill the executor, but tells it to not accept any more tasks and allows it to execute the tasks already in it's queue. 
We then awaitTermination() so that we don't proceed until all the remaining tasks have finished executing. Once they have finished, we interrupt both InvertedIndexTasks so that they don't get stuck waiting for tasks. Thread.join() waits for the threads to die. 


## Task 3

#### 3.1
The only sequential execution here is that write(1) happens before write(2), write(3) happens before write(4) and write(5) happens before write(6). Other than these three sets of one before the other, the order of execution is uncertain. 
This means that write(1), write(3), write(4), write(2) can happen as this still follows the mentioned certainties. 
The total amount of different outputs would be 6! if we had no certainties. We know that 2 would come before 1 in half of the different outcomes, and the same can be said for 4 before 3, and 6 before 5. We therefore get the formula 

6!/2^(number of cases where one number cannot come before the other \[1,2], \[3,4], \[5,6] = 3)

6!/2^3 = 90


#### 3.2

```
printer() {
    sem s2, s3 = 1;
    process P1 { write("1"); write("2"); V(s2); }
    process P2 { P(s2); write("3"); write("4"); V(s3); V(s2); }
    process P3 { P(s3); write("5"); write("6"); V(s3); }
}
```


## Task 4

__Executable code can also be found under folder task4__

#### 4.1

```java
public class Sem {
    private int permits;

    public Sem(int permits) { 
        this.permits = permits; 
    }

    public synchronized void acquire() throws InterruptedException {
        if (permits > 0) {
            permits--;
        } else {
            while (permits <= 0) {
                this.wait();
            }
            permits--;
        }
    }

    public synchronized void release() {
        permits++;
        if (permits > 0) {
            this.notify();
        }
    }
}
```


#### 4.2

```java
public void printer() {
    Sem s2, s3 = new Semaphore(0);
    Process p1 = new Process(null, s2, "1", "2");
    Process p2 = new Process(s2, s3, "3", "4");
    Process p3 = new Process(s3, null, "5", "6");
    Thread t1 = new Thread(p1);
    Thread t2 = new Thread(p2);
    Thread t3 = new Thread(p3);
    t1.start();
    t2.start();
    t3.start();
    try {
      t1.join();
      t2.join();
      t3.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
}

public class Process implements Runnable {
    private Sem before, after;
    private String first, second;

    public Process(Sem before, Sem after, String first, String second) {
        this.before = before;
        this.after = after;
        this.first = first;
        this.second = second;
    }

    @Override
    public void run() {
        try {
            if (before != null) {
                before.acquire();
            }
            System.out.println(first);
            System.out.println(second);
            if (after != null) {
                after.release();
            }
            if (before != null) {
                before.release();
            }
      } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
      }  
    }
}
```


## Task 5

#### Description
What's new in this task compared to the previous ones, is the CyclicBarrier.
CyclicBarriers are used for synchronization when different processes need to wait until a certain number of processes have reached the same point in execution, before they continue. CyclicBarriers er initiated with the number of processes that are required to continue execution. When a process reaches the point where it has to wait, it calls the .await()-method on the CyclicBarrier. This essentially increments the CyclicBarrier's counter of how many processes have reached the barrier. Once the required amount of processes have reached the barrier, the CyclicBarrier notifies all processes that they can continue. 

#### Output of running the program
![Image of doll production](./images/INF214_DollProduction.png)
