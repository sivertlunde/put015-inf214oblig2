package task1;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		double[][] C, A, B;
		int m, n, k;
		m = 800;
		n = 800;
		k = 800;
		A = generate(m, n);
		B = generate(n, k);
		C = new double[m][k];
		
		MatrixMult.multiply(A, B, C);
		SerialMult.multiply(A, B, C);
	}

	public static double[][] generate(int rows, int columns) {
		double[][] ret = new double[rows][columns];
		Random random = new Random();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				ret[i][j] = random.nextDouble() * 10;
			}
		}
		return ret;
	}

}
