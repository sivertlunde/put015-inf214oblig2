package task1;

public class SerialMult {
	
	public static void multiply(double[][] A, double[][] B, double[][] C) {
		long startTime = System.nanoTime();
		int m = A.length;
		int n = A[0].length;
		int k = B[0].length;
		for (int I = 0; I < m; I++) {
			for (int J = 0; J < k; J++) {
				C[I][J] = 0;
				for (int K = 0; K < n; K++) {
					C[I][J] += A[I][K] * B[K][J];
				}
//				System.out.println("Serial: " + C[I][J]);
			}
		}
		long endTime = System.nanoTime();
		System.out.println("SerialMulti duration: " + (endTime - startTime) + " nanoseconds");
	}
}