package task1;

import java.util.ArrayList;
import java.util.List;

public class MatrixMult {
	
	public static void multiply(double[][] A, double[][] B, double[][] C) {
		long startTime = System.nanoTime();
		List<Thread> threads = new ArrayList<>();
		for (int i = 0; i < A.length; i++) {
			RowMultiplierTask task = new RowMultiplierTask(C, A, B, i);
			Thread thread = new Thread(task);
			threads.add(thread);
			thread.start();
			if (threads.size() % 10 == 0) {
				waitForThreads(threads);
			}
		}
		waitForThreads(threads);
		long endTime = System.nanoTime();
		System.out.println("MatrixMulti duration: " + (endTime - startTime) + " nanoseconds");
	}

	private static void waitForThreads(List<Thread> threads) {
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		threads.clear();
	}
}
