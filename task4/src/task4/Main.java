package task4;

public class Main {

	public static void main(String[] args) {

		printer();

	}

	public static void printer() {
		Sem s2 = new Sem(0);
		Sem s3 = new Sem(0);
		Process p1 = new Process(null, s2, "1", "2");
		Process p2 = new Process(s2, s3, "3", "4");
		Process p3 = new Process(s3, null, "5", "6");
		Thread t1 = new Thread(p1);
		Thread t2 = new Thread(p2);
		Thread t3 = new Thread(p3);
		t1.start();
		t2.start();
		t3.start();
		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class Sem {
	private int permits;

	public Sem(int permits) {
		this.permits = permits;
	}

	public synchronized void acquire() throws InterruptedException {
		if (permits > 0) {
			permits--;
		} else {
			while (permits <= 0) {
				this.wait();
			}
			permits--;
		}
	}

	public synchronized void release() {
		permits++;
		if (permits > 0) {
			this.notify();
		}
	}
}

class Process implements Runnable {
	private Sem before, after;
	private String first, second;

	public Process(Sem before, Sem after, String first, String second) {
		this.before = before;
		this.after = after;
		this.first = first;
		this.second = second;
	}

	@Override
	public void run() {
		try {
			if (before != null) {
				before.acquire();
			}
			System.out.println(first);
			System.out.println(second);
			if (after != null) {
				after.release();
			}
			if (before != null) {
				before.release();
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}
