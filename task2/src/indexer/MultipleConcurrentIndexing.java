package indexer;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class MultipleConcurrentIndexing {
    final int numberDocsPerTask;
    private ConcurrentHashMap<String, StringBuffer> invertedIndex;
    private ExecutorCompletionService<List<Document>> completionService;
    private int executionTime;

    public MultipleConcurrentIndexing(int documentsPerTask) {
        numberDocsPerTask = documentsPerTask;
    }

    public int getExecutionTime() {
        return executionTime;
    }

    public void index(File[] files) {

        int numCores = Runtime.getRuntime().availableProcessors();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(Math.max(numCores - 1, 1));
        completionService = new ExecutorCompletionService<>(executor);
        invertedIndex = new ConcurrentHashMap<>();

        Date start = new Date();

        InvertedIndexTask invertedIndexTask = new InvertedIndexTask();
        Thread thread1 = new Thread(invertedIndexTask);
        thread1.start();
        InvertedIndexTask invertedIndexTask2 = new InvertedIndexTask();
        Thread thread2 = new Thread(invertedIndexTask2);
        thread2.start();

        List<File> taskFiles = new ArrayList<>();
        for (File file : files) {
            taskFiles.add(file);
            if (taskFiles.size() == numberDocsPerTask) {
                IndexingTask task = new IndexingTask(taskFiles);
                completionService.submit(task);
                taskFiles = new ArrayList<>();
            }

            if (executor.getQueue().size() > 10) {
                do {
                    try {
                        TimeUnit.MILLISECONDS.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } while (executor.getQueue().size() > 10);
            }
        }

        if (! taskFiles.isEmpty() ) {
            IndexingTask task = new IndexingTask(taskFiles);
            completionService.submit(task);
        }


        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
            thread1.interrupt();
            thread2.interrupt();
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Date end = new Date();
        executionTime = (int) (end.getTime() - start.getTime());

        thread1.interrupt();
        thread2.interrupt();

    }

    public String[] searchQuery(String query) {
        return invertedIndex.get(query).toString().split(";");
    }

    public static class IndexingTask implements Callable<List<Document>> {

        private final List<File> files;

        public IndexingTask(List<File> files) {
            this.files = files;
        }

        @Override
        public List<Document> call() {
            List<Document> documents = new ArrayList<>();
            DocumentParser parser = new DocumentParser();
            for (File file : files) {
                Map<String, Integer> voc = parser.parse(file
                        .getAbsolutePath());

                Document document = new Document();
                document.setFileName(file.getName());
                document.setVoc(voc);
                documents.add(document);
            }
            return documents;
        }
    }

    public class InvertedIndexTask implements Runnable {

        @Override
        public void run() {
            try {
                while (!Thread.interrupted()) {
                    try {
                        List<Document> documents = completionService.take().get();
                        for (Document document : documents) {
                            updateInvertedIndex(document.getVoc(), document.getFileName());
                        }
                    } catch (InterruptedException e) {
                        break;
                    }
                }
                while (true) {
                    Future<List<Document>> future = completionService.poll();
                    if (future == null)
                        break;
                    List<Document> documents = future.get();
                    for (Document document : documents) {
                        updateInvertedIndex(document.getVoc(), document.getFileName());
                    }
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        private void updateInvertedIndex(
                Map<String, Integer> voc,
                String fileName) {

            for (String word : voc.keySet()) {
                if (word.length() >= 3) {
                    StringBuffer buffer = invertedIndex.computeIfAbsent(word, k -> new StringBuffer());
                    //noinspection SynchronizationOnLocalVariableOrMethodParameter
                    synchronized (buffer) {
                        buffer.append(fileName).append(";");
                    }
                }
            }
        }
    }
}
