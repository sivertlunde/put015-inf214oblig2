package indexer;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SerialIndexing {

    private Map<String, StringBuffer> invertedIndex;
    private Map<String, Integer> voc;
    private int executionTime;

    public int getExecutionTime() {
        return executionTime;
    }

    private void updateInvertedIndex(String fileName) {

        for (String word : voc.keySet())
            if (word.length() >= 3)
                invertedIndex
                        .computeIfAbsent(word, k -> new StringBuffer())
                        .append(fileName)
                        .append(";");
    }

    public void index(File[] files) {
        invertedIndex = new HashMap<>();
        voc = new HashMap<>();

        Date start = new Date();

        for (File file : files) {
            DocumentParser parser = new DocumentParser();
            if (file.getName().endsWith(".txt")) {
                voc = parser.parse(file.getAbsolutePath());
                updateInvertedIndex(file.getName());
            }
        }
        Date end = new Date();
        executionTime = (int) (end.getTime() - start.getTime());
    }

    public String[] searchQuery(String query) {
        return invertedIndex.get(query).toString().split(";");
    }

}