package indexer;

import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.*;


public class ConcurrentIndexing {

  private ConcurrentHashMap<String, StringBuffer> invertedIndex;
  private ExecutorCompletionService<Document> completionService;
  private int executionTime;

  public int getExecutionTime() {
    return executionTime;
  }

  public void index(File[] files) {
    int numCores = Math.max(Runtime.getRuntime().availableProcessors() - 1, 1);
    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(numCores);
    completionService = new ExecutorCompletionService<>(executor);
    invertedIndex = new ConcurrentHashMap<>();

    Date start = new Date();

    InvertedIndexTask invertedIndexTask = new InvertedIndexTask();
    Thread thread1 = new Thread(invertedIndexTask);
    thread1.start();

    InvertedIndexTask invertedIndexTask2 = new InvertedIndexTask();
    Thread thread2 = new Thread(invertedIndexTask2);
    thread2.start();

    for (File file : files) {
      IndexingTask task = new IndexingTask(file);
      completionService.submit(task);
      if (executor.getQueue().size() > 1000) {
        do {
          try {
            TimeUnit.MILLISECONDS.sleep(50);
          } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
          }
        } while (executor.getQueue().size() > 1000);
      }
    }

    executor.shutdown();
    try {
      executor.awaitTermination(1, TimeUnit.DAYS);
      thread1.interrupt(); thread2.interrupt();
      thread1.join(); thread2.join();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }

    Date end = new Date();
    executionTime = (int) (end.getTime() - start.getTime());
  }

  public String[] searchQuery(String query) {
    return invertedIndex.get(query).toString().split(";");
  }

  public static class IndexingTask implements Callable<Document> {
    private final File file;
    public IndexingTask(File file) {
      this.file = file;
    }
    @Override
    public Document call() {
      DocumentParser parser = new DocumentParser();
      Map<String, Integer> voc = parser.parse(file.getAbsolutePath());
      Document document = new Document();
      document.setFileName(file.getName());
      document.setVoc(voc);
      return document;
    }
  }

  public class InvertedIndexTask implements Runnable {
    @Override
    public void run() {
      try {
        while (!Thread.interrupted()) {
          Document document = completionService.take().get();
          updateInvertedIndex(document.getVoc(), document.getFileName());
        }
        while (true) {
          Future<Document> future = completionService.poll();
          if (future == null) break;
          Document document = future.get();
          updateInvertedIndex(document.getVoc(), document.getFileName());
        }
      } catch (InterruptedException | ExecutionException e) {
    	  Thread.currentThread().interrupt();
      }
    }

    private void updateInvertedIndex(Map<String, Integer> voc, String fileName) {
      for (String word : voc.keySet()) {
        if (word.length() >= 3) {
          StringBuffer buffer = invertedIndex.computeIfAbsent(word, k -> new StringBuffer());
           buffer.append(fileName + ";");
        }
      }
    }
  }
}
