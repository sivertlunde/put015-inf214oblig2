Beautiful Girls (film)
 
{{Infobox film
| name           = Beautiful Girls
| image          = Beautiful girlsmovieposter.jpg
| caption        = Theatrical release poster
| genere         = Romantic Comedy
| director       = Ted Demme
| producer       = Cary Woods
| writer         = Scott Rosenberg
| starring       = Matt Dillon Lauren Holly Timothy Hutton Rosie ODonnell Martha Plimpton Natalie Portman Michael Rapaport Mira Sorvino Uma Thurman
| music          = David A. Stewart
| cinematography = Adam Kimmel
| editing        = Jeffrey Wolf
| distributor    = Miramax Films
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $10.6 million   
}}
 romantic Comedy-drama|comedy-drama film directed by Ted Demme from a screenplay written by Scott Rosenberg, starring Matt Dillon, Lauren Holly, Timothy Hutton, Rosie ODonnell, Martha Plimpton, Natalie Portman, Michael Rapaport, Mira Sorvino and Uma Thurman.

== Plot ==
Willie Conway goes home for his high school class reunion in Knights Ridge, Massachusetts. He is at a crossroads in his life and cannot decide if he should marry his girlfriend. He cannot decide if he should quit his music and take a job as a salesman. Over the course of the film, he spends time with his old friends who are all at similar crossroads. By the end they all discover what it is that they want.

== Cast ==
=== The men ===
* Timothy Hutton as Willie Conway, the only one of the group to move away to New York City, working as a semi-successful pianist. Generally the good guy in the group, his home life is somewhat fractured. His mother is deceased and his father is emotionally withdrawn. He also has a juvenile younger brother, Bobby (David Arquette).
* Matt Dillon as Tommy "Birdman" Rowland, head snow plower and former high school football star. Tommy seems to be ashamed that he never rose to his full potential in life. Known as the tough one of his group. Tommy seems not to have let go of his youth, and is still having an affair with Darian Smalls, an old high school flame. This puts a considerable strain on his relationship with his current girlfriend, Sharon.
* Noah Emmerich as Michael "Mo" Morris, a factory manager by profession. Unlike his friends, humble Mo has happily settled down with a family, but still does have a temper when provoked. Admires Willie greatly for how far he has traveled in his life.
* Max Perlich as Kev, snow plower and Tommys co-worker. Kev is cast as kind of a pathetic little guy. Hes not quite a dork or a geek but definitely would not be considered popular. However he is likely the most loyal of friends despite being a bit on the outer fringe of the circle. The wisecracking Kev has to reluctantly look the other way when Darian enters the picture, well aware of Tommys relationship with her of which he does not approve.
* Michael Rapaport as Paul Kirkwood, snow plower who is obsessed with centerfold models. Paul cannot let go of his old relationship with a waitress (Martha Plimpton) after she delivered an ultimatum to him. A crass guy who cannot resist tormenting his ex-girlfriend by plowing snow into her garage door.
* Pruitt Taylor Vince as Stanley "Stinky" Womack, the hardworking and humble proprietor/bartender of a local bar that he recently purchased and renovated. A generally good guy who knows the limits of right and wrong.

=== The women ===
* Annabeth Gish as Tracy Stover, Willies long-term girlfriend, a beautiful attorney who might be Willies future spouse.
* Lauren Holly as Darian Smalls, Tommys old love interest from high school, unhappily married with one daughter. Darian can not resist Tommy, which upsets both parties.
* Rosie ODonnell as Gina Barrisano, friend and hair stylist of the women. A fast-talker who spouts pearls of wisdom in a heavy New York accent.
* Martha Plimpton as Jan, Pauls on-again-off-again girlfriend who is vegetarian and dating a meat-cutter. This irritates Paul to no end.
* Natalie Portman as Marty, who describes her thirteen-year-old self as "an old soul." She is the Conway familys neighbor, who has eyes for Willie and wishes that he will wait until she turns 18 so that they can "walk through this world together."
* Mira Sorvino as Sharon Cassidy, Tommys current girlfriend, who tries her best to make Tommy happy.
* Uma Thurman as Andera, Stinkys cousin from Chicago. Very attractive and fairly wise, aids Paul in making his ex jealous. A girl from the big city who is able to appreciate the pace of a small town. She is everything that a guy from Knights Ridge would want in a woman. Ultimately she helps Willie find a little more direction in life.
* Anne Bobby as Sarah Morris, Mos wife and high school sweetheart, mother of two less than handsome children. She seems to lack self-confidence and is a bit passive aggressive. She does not seem to have strong opinions of her own and is kind of the third wheel of her group. Makes killer waffles.

=== Minor characters === Richard Bright as Dick Conway, Willies father.
* David Arquette as Bobby Conway, Willies brother.
* Sam Robards as Steve Rossmore, Darians husband.
* John Carroll Lynch as Frank Womack

The band The Afghan Whigs has a cameo appearance in the film. Greg Dulli, the bands lead singer, was close friends with Ted Demme.

== Origins ==
Screenwriter Scott Rosenberg was living in Needham, MA, waiting to see if Disney would use his script for Con Air. He said in an interview, "It was the worst winter ever in this small hometown. Snow plows were coming by, and I was just tired of writing these movies with people getting shot and killed. So I said, There is more action going on in my hometown with my friends dealing with the fact that they cannot deal with turning 30 or with commitment—all that became Beautiful Girls."    Originally, James L. Brooks was interested in directing the film according to actress Leslie Mann who auditioned for a role but was not cast.   

Ted Demme had the entire cast come to Minneapolis and live together for two to three weeks so that they could bond. He also made sure that the setting is a character unto itself. He "wanted to make it look like its Anytown USA, primarily East Coast. And I also wanted it to feel like a real working class town".    To this end, Demme drew inspiration from Michael Ciminos The Deer Hunter (1978). "The first third of the film is really an amazing buddy movie with those five actors. You could tell they were best friends, but they all had stuff amongst them that was personal to each one of them".  He screened the movie for the cast and crew.
 October Road. The show is loosely based on what happened after Beautiful Girls came out and how his friends reacted to a movie about their lives. Both Beautiful Girls and October Road take place in the fictional Massachusetts town of Knights Ridge, and have similar characters, jobs, plot lines.

== Soundtrack ==
{{Infobox album   
| Name        = Beautiful Girls: Music from the Motion Picture
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = Beautiful Girls.jpg
| Released    = January 30, 1996
| Recorded    =
| Genre       = Soundtrack
| Length      = 57:16 Elektra
| Producer    =
}}
{{Album ratings
| rev1      = AllMusic
| rev1Score =   
}}

=== Track listing ===
# Roland Gift—"Thats How Strong My Love Is" 6:18
# Afghan Whigs—"Be for Real" 4:16
# Howlin Maggie—"Easy to Be Stupid" 4:51
# Billy Paul—"Me and Mrs. Jones" 4:48
# Satchel (band)|Satchel—"Suffering" 4:49
# Chris Isaak—"Graduation Day" 3:10
# Pete Droge & the Sinners—"Beautiful Girl" 4:34
# Ween—"Ill Miss You" 2:56
# Afghan Whigs—"Cant Get Enough of Your Love, Babe" 5:21 The Spinners—"Could It Be Im Falling in Love" 4:31
# Kiss (band)|Kiss—"Beth (song)|Beth" 2:46
# King Floyd—"Groove Me" 3:01
# The Diamonds—"The Stroll" 2:31
# Neil Diamond—"Sweet Caroline" 3:24

=== Additional tracks (not included on soundtrack) ===
# Greg Kihn Band—"The Breakup Song (They Dont Write Em)"
# Split Enz—"I Got You"
# A Flock Of Seagulls—"I Ran (So Far Away)"
# Billy Preston—"Will It Go Round in Circles" Jethro Tull—"Locomotive Breath" Never on Sunday" Walk on the Wild Side"
# Rolling Stones—"Fool to Cry"
# Morphine (band)|Morphine—"Honey White"

== Reception ==
=== Box office ===
The film was released on February 9, 1996 in 752 theaters, grossing $2.7 million on its opening weekend. It went on to make $10.5 million in North America. 

=== Critical response ===
The film received fairly positive reviews and currently has a 78% rating on  , Desson Howe praised Natalie Portmans performance: "As a self-described old soul who connects spiritually with Hutton (theyre both existential searchers), shes the movies most poignant and witty presence."   

However, Jack Mathews, in the   review Janet Maslin wrote that Natalie Portman got the films "archest dialogue", and called her "a budding knockout, and scene-stealingly good even in an overly showy role."   

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*   at Kid in the Front Row Film Blog

 

 
 
 
 
 
 
 
 
 
 
 
 
 