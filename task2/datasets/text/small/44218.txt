San Francisco (1936 film)
{{Infobox film
| name           = San Francisco
| image          = San Francisco (film) poster.jpg
| image_size     =
| alt            =
| caption        = Original Film Poster
| director       = W. S. Van Dyke, D. W. Griffith John Emerson   Bernard H. Hyman Robert E. Hopkins  Anita Loos
| narrator       = Jack Holt  Jessie Ralph  Ted Healy Edward Ward
| cinematography = Oliver T. Marsh
| editing        = Tom Held
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 115 minutes United States English
| budget         = $1,300,000     . 
| gross          = $2,868,000 (Domestic earnings)  $2,405,000 (Foreign earnings) 
| preceded_by    =
| followed_by    =
}}
 1936 musical drama directed Rose Marie. Famous silent film directors D. W. Griffith and Erich von Stroheim work on the film without credit. Griffith directed some of the mob scenes while von Stroheim contributed to the screenplay. 

==Plot summary== Barbary Coast, owns the Paradise Club on Pacific Street.  He hires a promising but impoverished classically trained singer from Benson, Colorado named Mary Blake (Jeanette MacDonald), who becomes a star attraction at the Paradise. The piano player at the club, dubbed "The Professor" (Al Shean) can tell Mary has a professionally trained voice. Mat (Ted Healy), Blackies good friend at the Paradise, wisely predicts that Mary is not going to stay on the "Coast."

Blackies childhood friend, Roman Catholic priest Father Tim Mullen (Spencer Tracy), makes several attempts to reform him, while the other nightclub owners attempt to convince Blackie to run for the City and County of San Francisco Board of Supervisors in order to protect their crooked interests. Encouraged by Father Tim, who believes he can use the supervisor position to implement reform, Blackie decides to run for office.  However, despite Father Tims best efforts, Blackie remains a jaunty Barbary Coast atheist, although we later learn that Blackie paid for the new organ in Father Tims church.

Blackies feelings for Mary intensify but complications arise when she is offered an opportunity to sing in the opera.  Although she initially refuses to break her contract with Blackie she later leaves the Paradise Club due to the overtly sexual manifestation
of Blackies feelings for her.
 Jack Holt). Blackie wants to stop Mary singing at the Tivoli, and although he arrives the night of her premiere with a process server to shut down the show, when he hears her sing he decides not to stop the opera. After her performance Blackie visits Mary in her dressing room and realizing she still loves him, Mary asks him to marry her.  Blackie agrees but their reunion is soon interrupted by Burley, who had earlier proclaimed his love for Mary and proposed to her.  Blackie, seeing Burley as competition for Marys affections, is happy to tell him of their intent to marry.  However, as Blackie gloatingly tells Burley of their plans it becomes clear that Blackie intends to take Mary away from the Tivoli and put her back on stage at the Paradise.  Burley appeals to Mary but Blackie presents Mary with an ultimatum by asking if she wants to marry him or stay at the Tivoli.

Marys choice becomes apparent by her return to the Paradise.  Backstage, before the opening night of her return performance, she asks Blackie if they can set the date for their wedding.  Blackie agrees but wants to postpone getting married until after the election.  Father Tim soon pays them a visit and angered by Marys skimpy costume, defies Blackie to put her on the stage in front of the rowdy Paradise audience.  Mary, observing Blackies reaction to Father Tims statements, decides to leave with the priest after Blackie strikes him in the face.

Mary goes back to Burley and eventually meets his mother (Jessie Ralph) at her Nob Hill mansion. She tells Mary that she started out as Massie, the washerwoman in 1850 on Portsmouth Square and that although she also once had a "Blackie" in her life she chose to marry the elder Burley.  This cements Marys decision to accept Burleys proposal of marriage.

On order of Burley, on April 17, 1906 the San Francisco Police Department padlocks the Paradise. Blackie, distraught about the future of his club, ends up at the citys annual Chickens Ball  where Mary and Burley are in attendance.  That night Mary, after learning of the clubs padlocking, sings the song Theme from San Francisco and wins the Chickens Ball competition for the Paradise, but Blackie angrily refuses the prize money, stating that Mary had no right to sing on behalf of his club.  Before an embarrassed Mary can leave the ball with Burley, at 5:13&nbsp;a.m. April 18, 1906, the earthquake hits the city.  Widespread devastation and loss of life occurs as buildings are destroyed and fires rage out of control.  Firemen are ill equipped to fight the fires due to broken water mains.
 Presidio prepare to blow up the mansions to create firebreaks.

Blackie later meets Father Tim who takes him to Golden Gate Park, where a tent encampment has been established. Its there that Blackie hears Marys voice lifted in song with those in mourning.  After seeing Mary, Blackie falls to his knees and gives his heartfelt thanks to God for sparing Marys life.  Mary sees Blackie praying and as she walks toward him word spreads through camp that "The fires out!" As people shout about building a new San Francisco, Blackie and Mary join the crowd (a surprisingly multi-racial group, given the era of the film) as they leave Golden Gate Park marching arm-in-arm, singing The Battle Hymn of the Republic.  The film ends as the smoldering ruins dissolve into the "modern" San Francisco of the mid 1930s. The scenes of the city in the 1930s when shown near April 18 on San Francisco Bay Area television stations has substituted stock news footage of the City in the modern era.

==Cast==
 
 
* Clark Gable as Blackie Norton
* Jeanette MacDonald as Mary Blake
* Spencer Tracy as Father Tim Mullin Jack Holt as Jack Burley
* Jessie Ralph as Mrs.Burley
* Ted Healy as Mat
* Shirley Ross as Trixie
* Margaret Irving as Della Bailey
* Harold Huber as Babe
 
* Edgar Kennedy as Sheriff
* Al Shean as Professor
* William Ricciardi as Signor Baldini
* Kenneth Harlan as Chick
* Roger Imhof as Alaska
* Charles Judells as Tony Russell Simpson as Red Kelly
* Bert Roach as Freddie Duane
* Warren B. Hymer as Hazeltine
 

==Production==
  John Hoffman. The Barbary Coast barroom set was built on a special platform that rocked and shook to simulate the historical temblor. (Similar sets were built for the 1974 disaster film Earthquake.)

There are two versions of the ending. The original release features a stylish montage of then-current (1936) scenes of a bustling San Francisco, including Market Street and the construction of the Golden Gate Bridge.  When the film was re-released in 1948, it was thought these scenes were dated and the film fades out on a single long shot of the modern business district.  However, the TV and 16mm versions of the film seen in the 1950s and 60s were struck from the original version which includes the montage.  The current DVD and cable version features the shorter, 1948 version. 
 Test Pilot Boom Town, top billing clause in his MGM contract that Gable had enjoyed, effectively ending one of the American cinemas most famous screen teams.

Gable had played an extremely similar character also named "Blackie" two years earlier in the smash hit gangster epic Manhattan Melodrama, with William Powell and Myrna Loy.

==Music== title song may be the best-remembered part of the film. It was composed by Bronislaw Kaper and Walter Jurmann, with lyrics by Gus Kahn. It is sung by Jeanette MacDonald a half-dozen times in the film, and becomes an anthem for the survivors of the earthquake. It has now become a popular sentimental sing-along at public events such as the citys annual earthquake commemoration, as well as one of two official city songs, along with "I Left My Heart in San Francisco". 
 The Darktown Strutters Ball" can be heard; this is a historically inaccurate inclusion, since the song was written in 1917.

During the two operatic scenes in the film, MacDonald sang excerpts from Charles Gounods Faust (opera)|Faust and Giuseppe Verdis La Traviata.

==Box Office==
According to MGM records the film earned $5,273,000 and made a profit of $2,237,000. 

==Academy Awards==
The film won one Academy Award and was nominated for five more.   
{| class="wikitable" border="1"
|-
! Award !! Result !! Winner
|- Outstanding Production MGM (John John Emerson and Bernard H. Hyman)    Winner was Hunt Stromberg (Metro-Goldwyn-Mayer|MGM) - The Great Ziegfeld 
|- Best Director ||   || W. S. Van Dyke    Winner was Frank Capra - Mr. Deeds Goes to Town 
|- Best Actor ||   || Spencer Tracy    Winner was Paul Muni - The Story of Louis Pasteur 
|- Best Writing Robert Hopkins    Winner was Pierre Collings and Sheridan Gibney - The Story of Louis Pasteur 
|- Best Assistant Jack Sullivan The Charge of the Light Brigade 
|- Best Sound Recording ||   || Douglas Shearer
|-
|}

==Other awards==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#B0C4DE" align="center" Year
! Award
! Result
! Category
! Recipient
|- style="background-color: #EAEAEA;" John Emerson and Bernard H. Hyman
|-
|}
==Footnotes==
 
 
==References==
*  
*  

==External links==
*  
*  
*  
*   at Jeanette MacDonald and Nelson Eddy: A Tribute
*   at Virtual History
*  
*   at Turner Classic Movies Archives Database

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 