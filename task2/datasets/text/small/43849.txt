The Apartment
  
{{Infobox film
| name           = The Apartment
| image          = Apartment 60.jpg
| caption        = original film poster
| director       = Billy Wilder
| producer       = Billy Wilder
| writer         =  
| starring       =  Jack Kruschen
| music          = Adolph Deutsch
| cinematography = Joseph LaShelle
| editing        = Daniel Mandell
| studio         = The Mirisch Company
| distributor    = United Artists
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $3 million
| gross          = $24,600,000 
}}
The Apartment is a 1960 American comedy-drama film produced and directed by Billy Wilder, which stars Jack Lemmon, Shirley MacLaine, and Fred MacMurray.
 Best Picture. The film was the basis of the 1968 Broadway musical Promises, Promises (musical)|Promises, Promises, with book by Neil Simon, music by Burt Bacharach, and lyrics by Hal David.

==Plot==
Calvin Clifford (C. C.) "Bud" Baxter (Jack Lemmon) is a lonely office drudge at a national insurance corporation in a high-rise building in New York City.  In order to climb the corporate ladder, Bud allows four company managers, who reinforce their position over him by regularly calling him "Buddy Boy", to take turns borrowing his Upper West Side apartment for their various extramarital liaisons, which are so noisy that his neighbors assume that he is bringing home different women every night.
 David Lewis, David White) write glowing reports about Bud, who hopes for a promotion from the personnel director, Jeff D. Sheldrake (Fred MacMurray).  Sheldrake calls Bud to his office but says that he has found out why they were so enthusiastic. Then he goes on to promote him in return for exclusive privileges to borrow the apartment.  He insists on using it that same night and, as compensation for such short notice, gives Baxter two company-sponsored tickets to the hit Broadway musical The Music Man.

After work, Bud catches Fran Kubelik (Shirley MacLaine), an elevator operator on whom he has had his eye, and asks her to go to the musical with him.  They agree to meet at the theater after she has a drink with a former fling.  The man whom she meets, by coincidence, is Sheldrake, who convinces her that he is about to divorce his wife for her.  They go to Buds apartment as Bud waits forlornly outside the theater.

 ) and Fran Kubelik ( 

Several weeks later, at the companys raucous Christmas party, Sheldrakes secretary Miss Olsen (Edie Adams), drunkenly reveals to Fran that Fran is just the latest in a string of female employees whom Sheldrake has seduced into affairs with the promise of divorcing his wife, with Miss Olsen herself being one of them.  At Buds apartment, Fran confronts Sheldrake, upset with herself for believing his lies.  Sheldrake maintains that he genuinely loves her but then leaves to return to his suburban family as usual.

Meanwhile, Bud accidentally finds out about Sheldrake and Fran.  Disappointed, he picks up a woman (Hope Holiday) at a local bar.  When they arrive at his apartment, he is shocked to find Fran in his bed, fully clothed and unconscious from an intentional overdose of his sleeping pills.  He enlists the help of his neighbor, Dr. Dreyfuss (Jack Kruschen), to revive Fran without notifying the authorities and sends his confused bar pickup home.  To protect his job, he lets Dreyfuss believe that he and Fran are lovers who had fought, which he took so lightly that he was meeting another woman while she was attempting suicide.  Fran spends two days recuperating at his apartment, while Bud tries entertaining and distracting her from any further suicidal thoughts, talking her into playing numerous hands of gin rummy.
 Johnny Seven) comes to the office looking for her.  She has not been there and neither has Bud.  The previous day, one of the executives had seen Fran in the bedroom when he came to the apartment hoping to borrow it and mentioned it to the other executives. Resenting Bud for denying them access to his apartment, the executives direct the man there.  Bud again takes responsibility for Frans actions, and Karl punches him twice in the face.

Sheldrake rewards Bud with a further promotion and fires Miss Olsen for telling Fran his history of womanizing.  However, Miss Olsen retaliates by telling his wife, who promptly throws him out.  Sheldrake moves into a room at his athletic club but now figures that he can string Fran along while he enjoys his newfound bachelorhood.  When Sheldrake asks Bud for access to the apartment on New Years Eve, Bud refuses and quits the firm.  Sheldrake tells Fran about Bud quitting at a New Years party they are attending.  Fran finally realizes that Bud is the man who truly loves her.  Fran then deserts Sheldrake at the party, and runs to Buds apartment.  Arriving at the door, she hears a loud noise like a gunshot.  Afraid that Bud has shot himself, Fran pounds on the door.  Bud, holding a bottle of overflowing champagne, finally opens the door, surprised and delighted that Fran is there.  Bud has been packing for a move to another job and city.  Fran insists on resuming their gin rummy game, telling Bud that she is now free as well.  When he declares his love for her, her reply is the now-famous final line of the film: "Shut up and deal", delivered with a loving and radiant smile.

==Cast==
 
* Jack Lemmon as Calvin Clifford (C. C.) "Bud" Baxter
* Shirley MacLaine as Fran Kubelik
* Fred MacMurray as Jeff D. Sheldrake
* Ray Walston as Joe Dobisch
* Jack Kruschen as Dr. Dreyfuss David Lewis as Al Kirkeby
* Hope Holiday as Mrs. Margie MacDougall
* Joan Shawlee as Sylvia
* Naomi Stevens as Mrs. Mildred Dreyfuss Johnny Seven as Karl Matuschka
* Joyce Jameson as the blonde in the bar Hal Smith as Santa Claus in the bar
* Willard Waterman as Mr. Vanderhoff David White as Mr. Eichelberger
* Edie Adams as Miss Olsen
 

==Production== Paul Douglas as Jeff Sheldrake; however, after he died unexpectedly, Fred MacMurray was cast.

The initial concept for the film came from Brief Encounter by Noël Coward, in which Celia Johnson has an affair with Trevor Howard in his friends apartment. However, due to the Hays Production Code, Wilder was unable to make a film about adultery in the 1940s. Wilder and Diamond also based the film partially on a Hollywood scandal in which high-powered agent Jennings Lang was shot by producer Walter Wanger for having an affair with Wangers wife, actress Joan Bennett. During the affair, Lang used a low-level employees apartment.  Another element of the plot was based on the experience of one of Diamonds friends, who returned home after breaking up with his girlfriend to find that she had committed suicide in his bed.

Although Wilder generally required his actors to adhere exactly to the script, he allowed Jack Lemmon to improvise in two scenes: in one scene he squirted a bottle of nose drops across the room, and in another he sang while making a meal of spaghetti (which he strains through the grid of a tennis racket). In another scene, where Lemmon was supposed to mime being punched, he failed to move correctly and was accidentally knocked down. Wilder chose to use the shot of the genuine punch in the film. Lemmon also caught a cold when one scene on a park bench was filmed in sub-zero weather.

Art director Alexandre Trauner used forced perspective to create the set of a large insurance company office. The set appeared to be a very long room full of desks and workers; however, successively smaller people and desks were placed to the back of the room ending up with children. He designed the set of Baxters apartment to appear smaller and shabbier than the spacious apartments that usually appeared in films of the day. He used items from thrift stores and even some of Wilders own furniture for the set. Chandler, Charlotte. Nobodys perfect: Billy Wilder : a personal biography. 
 Charles Williams and originally titled "Jealous Lover", was first heard in the 1949 film The Romantic Age.    A recording by Ferrante & Teicher, released as "The Theme from The Apartment", reached #10 on the Billboard Hot 100|Billboard Hot 100 chart later in 1960.

==Reception==
At the time of release, the film was a critical and commercial success, making $25 million at the box office and receiving a range of positive reviews. The New York Times film critic Bosley Crowther enjoyed the film, calling it, "A gleeful, tender, and even sentimental film." Chicago Sun-Times film critic Roger Ebert and ReelViews film critic James Berardinelli both praised the film, giving it four stars out of four, with Ebert adding it to his The Great Movies list. The film has a 93% "Certified Fresh" rating on Rotten Tomatoes, based on 54 reviews; the sites consensus states that "Director Billy Wilders customary cynicism is leavened here by tender humor, romance, and genuine pathos." As of March 2015, the film holds a 4.00/5 weighted mean rating on rating aggregator site RateItAll.
 Saturday Review called it "a dirty fairy tale".  According to Fred MacMurray, after the films release he was accosted by women in the street who berated him for making a "dirty filthy movie" and once one of them hit him with her purse. 

The film earned a profit of over $1 million during its theatrical run. Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 170 
===33rd Academy Awards (Oscars)&nbsp;– 1960=== Academy Award nominations and won 5 Academy Awards.      

{| class="wikitable" border="1"
|-
! Award !! Result !! Nominee
|- Best Picture
|  
| Billy Wilder
|- Best Director
|  
| Billy Wilder
|- Best Writing (Original Screenplay)
|  
| I. A. L. Diamond Billy Wilder
|- Best Actor
|  
| Jack Lemmon
|- Best Actress
|  
| Shirley MacLaine
|- Best Supporting Actor
|  
| Jack Kruschen
|- Best Cinematography (Black-and-White)
|  
| Joseph LaShelle
|- Best Film Editing
|  
| Daniel Mandell
|- Best Art Direction-Set Decoration, Black-and-White
|  
| Alexander Trauner Edward G. Boyle
|- Best Sound
|  
| Gordon E. Sawyer
|}
 American Beauty (1999)  to Lemmons performance.  According to the behind-the-scenes feature on the American Beauty DVD, the films director, Sam Mendes, had watched The Apartment (among other classic American films) as inspiration in preparation for shooting his film.
 The Artist (2011).

===Other awards and honors=== BAFTA and Top 100 Films, as well as at #20 on their list of 100 Laughs 100 Passions Top 100 list to #80. In 1994, The Apartment was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry. In 2002, a poll of film directors conducted by Sight and Sound magazine listed the film as the 14th greatest film of all time (tied with La Dolce Vita).  In 2006, Premiere voted this film as one of "The 50 Greatest Comedies Of All Time".

==See also==
* Life in a... Metro

==References==
 

==External links==
 
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 