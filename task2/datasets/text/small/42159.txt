The Best Years of Our Lives
For other uses see The Best Years of Our Lives (disambiguation).
 
{{Infobox film
| name           = The Best Years of Our Lives
| image          = The Best Years of Our Lives film poster.jpg
| image_size    = 225px
| caption        = Theatrical release poster
| director       = William Wyler
| screenplay     = Robert E. Sherwood
| based on       =  
| starring       = Myrna Loy Fredric March Dana Andrews Teresa Wright Virginia Mayo Harold Russell
| producer       = Samuel Goldwyn
| music          = Hugo Friedhofer
| cinematography = Gregg Toland
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 172 minutes
| country        = United States
| language       = English
| budget         = $2.1 million Thomson 1993, pp 490–491. 
| gross          = $23,650,000 
|}}
The Best Years of Our Lives is a 1946 American   then adapted the novella as a screenplay. 

The Best Years of Our Lives won seven  , selling approximately 55 million tickets in the United States   which equaled a gross of $23,650,000.  It remains the sixth most-attended film of all time in the UK, with over 20 million tickets sold.  The film had one of the highest viewing figures of all time, with ticket sales exceeding $20.4 million. 
 

==Plot== bombardier in Europe. Homer lost both hands from burns suffered when his aircraft carrier was sunk, and now uses mechanical hook prostheses. Al served as an infantry platoon sergeant in the Pacific. All three have trouble adjusting to civilian life.

Al has a comfortable home and a loving family: wife Milly (Myrna Loy), adult daughter Peggy (Teresa Wright, who was only thirteen years Loys junior), and college freshman son Rob (Michael Hall, who is absent after the first one-third of the film). He returns to his old job as a bank loan officer. The bank president views his military experience as valuable in dealing with other returning servicemen. When Al approves a loan (without collateral (finance)|collateral) to a young Navy veteran, however, the president advises him against making a habit of it.  Later, at a banquet held in his honor, a slightly inebriated Al expounds his belief that the bank (and America) must stand with the vets who risked everything to defend the country and give them every chance to rebuild their lives.

  photography.]]
Before the war, Fred had been an unskilled drugstore soda jerk. He wants something better, but the tight postwar job market forces him to return to his old job. Fred had met Marie (Virginia Mayo) while in flight training and married her shortly afterward, before shipping out less than a month later. She became a nightclub waitress while Fred was overseas. Marie makes it clear she does not enjoy being married to a lowly soda jerk.

Homer was a football quarterback and became engaged to his next door neighbor, Wilma (Cathy ODonnell), before joining the Navy. Both Homer and his parents now have trouble dealing with his disability. He does not want to burden Wilma with his handicap and so pushes her away, although she still wants to marry him.

Peggy meets Fred while bringing her father home from a bar where the three men meet once again. They are attracted to each other. Peggy dislikes Marie, and informs her parents she intends to end Fred and Maries marriage, but they tell her that their own marriage overcame similar problems. Concerned, Al demands that Fred stop seeing his daughter. Fred agrees, but the friendship between the two men is strained.

At the drugstore, an obnoxious customer, who claims that the war was fought against the wrong enemies, gets into a fight with Homer. Fred intervenes and knocks the man into a glass counter, costing him his job. Later, Fred encourages Homer to put his misgivings behind him and marry Wilma, offering to be his best man.

One evening, Wilma visits Homer and tells him that her parents want her to leave Boone City for an extended period to try to forget him. Homer bluntly demonstrates to her how hard life with him would be. When Wilma is undaunted, Homer reconsiders.
 Distinguished Flying General Doolittle. At the airport, Fred books space on the first outbound aircraft, without regard for the destination. While waiting, he wanders into a vast aircraft boneyard. Inside the nose of a Boeing B-17 Flying Fortress|B-17, he relives the intense memories of combat. The boss of a work crew rouses him from his flashback. When the man says the aluminum from the aircraft is being salvaged to build housing, Fred persuades the boss to hire him.

At the Parrish home, people have gathered for the wedding of Homer and Wilma. Fred, now-divorced, is Homers best man. While the vows are exchanged Fred and Peggy glance across at one another. At the conclusion everyone gathers around the newlyweds. Still gazing over at Peggy, Fred walks across the room, takes her in his arms and kisses her. He asks if she knows how things will be for them, that it will be a hard at first, that it could take years before they can get a life established. All the while Peggy smiles fondly at Fred, and then kisses him back.

==Cast==
 
* Myrna Loy as Milly Stephenson
* Fredric March as Sergeant First Class Al Stephenson
* Dana Andrews as Captain Fred Derry
* Teresa Wright as Peggy Stephenson
* Virginia Mayo as Marie Derry
* Cathy ODonnell as Wilma Cameron
* Hoagy Carmichael as Uncle Butch
* Harold Russell as Petty Officer 2nd Class Homer Parrish
* Gladys George as Hortense Derry
* Roman Bohnen as Pat Derry Ray Collins as Mr. Milton
* Minna Gombell as Mrs. Parrish
 
* Walter Baldwin as Mr. Parrish
* Steve Cochran as Cliff
* Dorothy Adams as Mrs. Cameron
* Don Beddoe as Mr. Cameron
* Marlene Aames as Luella Parrish
* Charles Halton as Prew
* Ray Teal as Mr. Mollett
* Howland Chamberlain as Thorpe
* Dean White as Novak
* Erskine Sanford as Bullard
* Michael Hall as Rob Stephenson
* Victor Cutler as Woody
 
Michael Hall is the last surviving primary cast member.

Casting brought together established stars as well as character actors and relative unknowns. Famed drummer Gene Krupa was seen in archival footage, while Tennessee Ernie Ford, later a famous television star, appeared as an uncredited "hillbilly singer" (in the first of his only three film appearances).  Blake Edwards, later notable as a film producer and director, appeared fleetingly as an uncredited "Corporal". Actress Judy Wyler was cast in her first role in her fathers production.  Sean Penns father, Leo, played the uncredited part of the soldier working as the scheduling clerk at the beginning of the film (the Dana Andrews character asks him, "Which flight leaves first?").

==Production==
Director William Wyler had flown combat missions over Europe in filming   (1944) and worked hard to get accurate depictions of the combat veterans he had encountered. Wyler changed the original casting that had featured a veteran suffering from posttraumatic stress disorder and sought out Harold Russell, a non-actor to take on the exacting role of Homer Parrish. 

For The Best Years of Our Lives, he asked the principal actors to purchase their own clothes, in order to connect with daily life and produce an authentic feeling. Other Wyler touches included constructing life-size sets, which went against the standard larger sets that were more suited to camera positions. The impact for the audience was immediate, as each scene played out in a realistic, natural way. Orriss 1984, p. 121. 

The Best Years of Our Lives began filming on April 15, 1946 at a variety of locations, including the  . Retrieved: April 26, 2007.  For the passage of Fred Derrys reliving a combat mission while sitting in the remains of a former bomber, Wyler used "zoom" effects to simulate Derrys subjective state. 

The "Jackson High" football stadium seen early in aerial footage of the bomber flying over the fictional Boone City, is Corcoran Stadium located at Xavier University in Cincinnati. A few seconds later Walnut Hills High School with its distinctive dome and football field can be seen along with the downtown Cincinnati skyline (Carew Tower and PNC Tower) in the background. 

After the war, the combat aircraft featured in the film were being destroyed and disassembled for reuse as scrap material.  The scene of Derrys walking among aircraft ruins was filmed at the Ontario Army Air Field in Ontario, California.  The former training facility had been converted into a scrap yard, housing nearly 2,000 former combat aircraft in various states of disassembly and reclamation. 

==Reception==

===Critical response===
Upon its release, The Best Years of Our Lives received extremely positive reviews from critics. Shortly after its premiere at the Astor Theater, New York, Bosley Crowther, film critic for The New York Times, hailed the film as a masterpiece.  He wrote,
 
 David Thomson offered tempered praise: "I would concede that Best Years is decent and humane... acutely observed, despite being so meticulous a package.  It would have taken uncommon genius and daring at that time to sneak a view of an untidy or unresolved America past Goldwyn or the public." 

The Best Years of Our Lives has a 97% "Fresh" rating at Rotten Tomatoes, based on 37 reviews.  Chicago Sun Times film critic Roger Ebert put the film on his "Great Movies" list in 2007, calling it "...modern, lean, and honest." 

The Best Years of Our Lives was a massive popular success, earning an estimated $11.5 million at the North American box office during its initial theatrical run.  When box office prices are adjusted for inflation, it remains one of the top 100 grossing films in U.S. history. Among films released before 1950, only Gone With the Wind, The Bells of St. Marys, and four Disney titles have done more total business, in part due to later re-releases.  (Reliable box office figures for certain early films such as Birth of a Nation and Charlie Chaplins comedies are unavailable.) 

===Awards and honors===
1947 Academy Awards 
The Best Years of Our Lives received nine Academy Awards. Fredric March won his second Best Actor award (also having won in 1932 for Dr. Jekyll and Mr. Hyde (1931 film)|Dr. Jekyll and Mr. Hyde).

Despite his Oscar-nominated performance, Harold Russell was not a professional actor. As the Academy Board of Governors considered him a long shot to win, they gave him an honorary award "for bringing hope and courage to his fellow veterans through his appearance."  When Russell won Best Supporting Actor, there was an enthusiastic response.  He is the only actor to have received two Academy Awards for the same performance.  He later sold his Best Supporting Actor award at auction for $60,500, to pay his wifes medical bills. Bergan, Ronald.   The Guardian,  February 6, 2002. Retrieved: June 12, 2012. 

{| class="wikitable" border="1"
|- Award !! Winner
|- Best Motion Picture  ||   || Samuel Goldwyn Productions (Samuel Goldwyn, Producer)
|- Best Director  ||   || William Wyler
|- Best Actor  ||   || Fredric March
|- Best Writing (Screenplay)  ||   || Robert E. Sherwood
|- Best Supporting Actor  ||   || Harold Russell
|- Best Film Editing  ||   || Daniel Mandell
|- Best Music (Score of a Dramatic or Comedy Picture)  ||   || Hugo Friedhofer
|- Best Sound Recording  ||   || Gordon E. Sawyer    Winner was John P. Livadary - The Jolson Story 
|- Honorary Award  ||   || To Harold Russell
|- Memorial Award ||   || Samuel Goldwyn
|}

 Some posters say the film won nine Academy Awards due to the honorary award won by Harold Russell, and the Irving G. Thalberg Memorial Award won by Samuel Goldwyn, in addition to its seven awards for Best Picture, Best Director, Best Actor, Best Screenplay, Best Supporting Actor, Best Editing, and Best Music Score. 

1947 Golden Globe Awards
* Won: Best Dramatic Motion Picture
* Won: Special Award for Best Non-Professional Acting - Harold Russell

1947 Brussels World Film Festival
* Won: Best Actress Of The Years - Myrna Loy

1948 BAFTA Awards
* Won: BAFTA Award for Best Film from any Source

Other wins
* National Board of Review: NBR Award Best Director, William Wyler; 1946.
* New York Film Critics Circle Awards: NYFCC Award Best Director, William Wyler; Best Film; 1946.
* Bodil Awards: Bodil; Best American Film, William Wyler; 1948.
* Cinema Writers Circle Awards, Spain: CEC Award; Best Foreign Film, USA; 1948.

In 1989, the National Film Registry selected it for preservation in the United States Library of Congress as "culturally, historically, or aesthetically significant."

American Film Institute recognition
*1998 AFIs 100 Years... 100 Movies #37
*2006 AFIs 100 Years... 100 Cheers #11
*2007 AFIs 100 Years... 100 Movies (10th Anniversary Edition) #37

==References==
Notes
 
Citations
 
Bibliography
 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Flood, Richard. "Reel crank - critic Manny Farber." Artforum,  Volume 37, Issue 1,  September 1998. ISSN 0004-3532.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies", in The Making of the Great Aviation Films. General Aviation Series, Volume 2, 1989.
* Kinn, Gail and Jim Piazza. The Academy Awards: The Complete Unofficial History. New York: Black Dog & Leventhal, 2008. ISBN 978-1-57912-772-5.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorn, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
* Thomson, David. Showman: The Life of David O. Selznick. London: Abacus, 1993. ISBN  978-0-2339-8791-0.
* Thomson, David. "Wyler, William". A Biographical Dictionary of Film. London: Little, Brown, 2002. ISBN 0-316-85905-2.
 

==External links==
 
 
*  
*  
*  
*  
*   detailed synopsis/analysis at Film Site by Tim Dirks
*   film article at Reel Classics. Includes MP3s
*   at the Golden Years web site

===Streaming audio===
*   on Screen Guild Theater: November 24, 1947
*   on Screen Directors Playhouse: April 17, 1949

 
 
 
 
 
 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 