The Phantom Edit
 
 
 
Star Wars Episode I.I: The Phantom Edit is a  , removing many elements of the original film. The purpose of the edit, according to creator Mike J. Nichols, was to make a much stronger version of The Phantom Menace based on the previous execution and philosophies of film storytelling and editing of George Lucas.  The Phantom Edit was the first unauthorized re-edit of The Phantom Menace to receive major publicity and acclaim. 
"The general consensus of fans on the Internet seems to be that the new edit is an improvement on the original version...."If you havent watched  , you dont know," said Jercan. "Whoever did the job did a hell of a job. Its like theres no break. So it had to be a professional. Because two kids cant do this."" Zap2it. "The Phantom Editor had apparently used new, cheap computing power to assemble an alternative, professional-quality movie -- exactly the same kind of new, cheap computing power that had allowed Lucas to make the original film, with its digitally created characters and special effects." PBS. 

==History==
The Phantom Edit was originally circulated in Hollywood studios in 2000 and 2001, and was followed by media attention. Salon.com,  National Public Radio|NPR,  Public Broadcasting Service|PBS,  and the BBC  all covered the edit to various degrees.

Rumor attributed The Phantom Edit to Kevin Smith, who admitted to having seen the re-edit but denied that he was the editor. The editor was revealed to be Mike J. Nichols of Santa Clarita, California in the September 7, 2001, edition of the Washington Post.  
 commentary track Easter eggs. The DVD version has also been relabeled as Episode I.II, has a slightly different podrace from the VHS version, and contains more extensive editing to individual images and sounds that have not been entirely cut. 

Lucasfilm, the production company of series creator George Lucas, condoned the edit and did not pursue legal action against its distributors. 

==Changes==
Changes made from the original film in The Phantom Edit
* Opening crawl replaced with a new one explaining why the edit was made
* Re-editing of nearly all scenes featuring Jar Jar Binks and removing some of what Nichols dubs Jar Jar Antics
* Removal or re-editing of most of the Battle Droid dialogue exposition throughout the film
* Trimming scenes involving politics original Star Wars trilogys presentation style
* Removal of "Yippee" and "Oops" from Anakin Skywalker|Anakins dialogue Force sensitivity
* Reinstatement of deleted scenes in order to fill in plot holes in the film narrative
There were a total of 18 minutes cut from the original film, reducing the run time from 136 minutes to 118 minutes.

==Reviews==
Critics and filmmakers have commented on the original Phantom Edit, in most cases providing the approval and recognition which furthered the fan edit movement.

* "Smart editing to say the least" — Kevin Smith, Film Director 
* "...Materialized from out of nowhere was a good film that had been hidden inside the disappointing original one." — Daniel Kraus, Salon.com (November 5, 2001) 
* " ; someone with a gift for editing!" — Michael Wilmington, Chicago Tribune Film Critic  

The 2010 documentary film The People vs. George Lucas cites The Phantom Edit as a key example of the remix culture created by the Star Wars franchise.

==Sequel: Attack of the Phantom==
Nichols followed up his edits of Episode I with an edit of  .

Called Star Wars Episode II.I: Attack of the Phantom, the DVD contains a re-edited version (38 minutes cut, new runtime of 104 minutes) of Episode II in surround sound, with a commentary track.  At points during the commentary, the viewer has the option to pause the film to view in more detail some of the things that the editor is discussing.

==References==
 

==External links==
*  , BBC News Article, Thursday, June 7, 2001
*  , an NPR story, Star Wars - The Phantom Edit, Robert Siegel talks with Andrew Rodgers from Zap2it.com, July 16, 2001
*  , a Salon.com article by Daniel Kraus, November 5, 2001
*  , a PBS Article by Charles C. Mann, November 22, 2001
*  , Joshua Griffins April 29, 2005 review of Episode II.I: Attack of the Phantom, on TheForce.net fan site

 

 
 
 
 
 
 
 