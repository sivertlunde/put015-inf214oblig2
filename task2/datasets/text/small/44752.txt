Grand Hotel (film)
{{Infobox film
| name           = Grand Hotel
| image          = GrandHotelFilmPoster.jpg
| caption        = Original poster
| director       = Edmund Goulding
| producer       = Irving Thalberg
| writer         = William A. Drake
| based on       =   and  
| starring       = Greta Garbo John Barrymore Joan Crawford Wallace Beery Lionel Barrymore Lewis Stone Jean Hersholt
| music          = William Axt Charles Maxwell
| cinematography = William H. Daniels
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 112 minutes
| language       = English
| country        = United States
| budget         = $750,000 
| gross = $2,250,000 
}}
Grand Hotel is a 1932 American drama film directed by Edmund Goulding. The screenplay by William A. Drake is based on the 1930 play of the same title by Drake, who had adapted it from the 1929 novel Menschen im Hotel by Vicki Baum.  , the film is the only one to have won the Academy Award for Best Picture without it or its participants being nominated in any other category.
 stage musical MGM Grand Hotel, was considered. 
 London Suite. In 2007, the film was selected for preservation in the United States National Film Registry by the Library of Congress for being "culturally, historically, or aesthetically significant."

==Plot==
Doctor Otternschlag (Lewis Stone), a disfigured veteran of World War I and a permanent resident of the Grand Hotel in Berlin, wryly observes, "People come and go. Nothing ever happens," after which a great deal transpires. Baron Felix von Geigern (John Barrymore), who squandered his fortune and supports himself as a card player and occasional jewel thief, befriends Otto Kringelein (Lionel Barrymore), a meek accountant who, having discovered he is dying, has decided to spend his remaining days in the lap of luxury. Kringeleins former employer, industrialist General Director Preysing (Wallace Beery), is at the hotel to close an important deal, and he hires stenographer Flaemmchen (Joan Crawford) to assist him. She aspires to be an actress and shows Preysing some magazine photos for which she posed, implying she is willing to offer him more than typing if he is willing to help advance her career.
 ballerina Grusinskaya (Greta Garbo), whose career is on the wane. She is high strung and seemingly on the verge of a breakdown. When the Baron is in her room to steal her jewelry and she returns from the theatre, he hides in her room and overhears her as she talks to herself in despair about wanting to end it all, holding a vial of medication in her hand. He comes out of hiding and engages her in conversation, and Grusinskaya finds herself attracted to him.

The following morning, a repentant Baron returns Grusinskayas jewels, and she is able to forgive his crime. Instead, she invites him to accompany her to Vienna, an offer he accepts.

The Baron joins Kringelein and Flaemmchen at the hotel bar, and she cajoles the ailing man into dancing with her. Preysing interrupts them and imperiously demands she join him. Irritated by his former employers coarse behavior, Kringelein – who is aware of Preysings many swindles – tells him what he thinks of him. Surprised by his uncharacteristic audacity, Preysing attacks Kringelein and the two men must be separated. The Baron is desperate for money to pay his way out of the criminal group he had been working with.  He and Kringelein decide to get a card game going, and Kringelein wins everything, and then becomes intoxicated. When he drops his wallet, the Baron locates and quietly stashes it in his jacket pocket, intending to keep the winnings for himself.  However, after Kringelein begins to frantically search for his lost belongings, the Baron – who desperately needs the money but has become very fond of Kringelein – pretends to have suddenly discovered the wallet and returns it to him.
As part of a current desperate merger plan, Preysing must travel to London, and he asks Flaemmchen to accompany him. Later, when the two are in her room, which opens on to his, Preysing sees the shadow of the Baron rifling through his belongings. He confronts the Baron; the two struggle, and Preysing bludgeons the Baron with the telephone, killing him. Flaemmchen comes in and sees what happened and tells Kringelein, who confronts Preysing. He insists he acted in self-defense, but Kringelein summons the police and Preysing is arrested.

Grusinskaya departs for the train station, fully expecting to find the Baron waiting for her there. Meanwhile, Kringelein offers to take care of Flaemmchen, who suggests they go to Paris and seek a cure for his illness. As they leave the hotel, Doctor Otternschlag once again observes, "Grand Hotel. Always the same. People come. People go. Nothing ever happens."

==Cast==
  
* Greta Garbo as Grusinskaya, the dancer
* John Barrymore as the Baron Felix von Gaigern
* Joan Crawford as Flaemmchen, the stenographer
* Wallace Beery as General Director Preysing
* Lionel Barrymore as Otto Kringelein
* Lewis Stone as Dr Otternschlag
* Jean Hersholt as Senf, the porter
* Robert McWade as Meierheim
 
* Purnell Pratt as Zinnowitz
* Ferdinand Gottschalk as Pimenov
* Rafaela Ottiano as Suzette
* Morgan Wallace as the chauffeur
* Tully Marshall as Gerstenkorn Frank Conroy as Rohna
* Murray Kinnell as Schweimann
* Edwin Maxwell as Dr Waitz
 

==Production==
Producer Irving Thalberg purchased the rights to Vicki Baums novel Menschen im Hotel for $13,000 and then commissioned William A. Drake to adapt it for the stage. Chandler, Charlotte, Not the Girl Next Door: Joan Crawford, A Personal Biography. New York: Simon & Schuster 2008. ISBN 1-4165-4751-7, p. 100  Broadway at National Theatre on November 13, 1930 and ran for 459 performances.  Pleased with its success, Thalberg had Drake and Béla Balázs write the screenplay and budgeted the project at $700,000. 
There was also some controversy about Greta Garbo, with her strong Swedish accent, playing a Russian. 

The film was also seen as an artistic achievement in its art direction and production quality. The art director, Cedric Gibbons, was one of the most important and influential in the history of American film. The lobby scenes were extremely well done, portraying a 360° desk. This allowed audiences to watch the hotel action from all around the characters. It changed the way sets were made from that point onward. 

==="I want to be alone"===
As Grusinskaya, Greta Garbo delivers the line "I want to be alone" and, immediately following, "I just want to be alone." Soon after,  in conversation with Baron Felix von Gaigern, she says "And I want to be alone." Referring to its legendary use as a characterization of her personal life, Garbo later insisted, "I never said I want to be alone; I only said I want to be let alone. There is all the difference." 

==Critical reception==
Alfred Rushford Greason of Variety (magazine)|Variety said the film "may not entirely please the theatregoers who were fascinated by its deft stage direction and restrained acting, but it will attract and hold the wider public to which it is now addressed." He added, "The drama unfolds with a speed that never loses its grip, even for the extreme length of nearly two hours, and there is a captivating pattern of unexpected comedy that runs through it all, always fresh and always pat." 

Mordaunt Hall of The New York Times praised the performances of Greta Garbo and Lionel Barrymore, in a mostly positive review. "The picture adheres faithfully to the original," he said, "and while it undoubtedly lacks the life and depth and color of the play, by means of excellent characterizations it keeps the audience on the qui vive." 

Film Daily called it an "engrossing drama" that "never lags" and "one of the classiest moving picture affairs youve seen in a long time." 
 John Mosher of The New Yorker called it a "tricky, clever film", praising Goulding as "a director at last to give Garbo her due" and for his "ingenious" camera work, "relishing, I suspect, the advantages the screen offers in these respects over the stage, where the awkward constant shifting of scenes clogged the action of the play." 

The film currently holds a rating of 85% on the film review aggregating website Rotten Tomatoes with the site stating the critics consensus as "Perhaps less a true film than a series of star-studded vignettes, Grand Hotel still remains an entertaining look back at a bygone Hollywood era." 

Writing in 2009, Blake Goble of The Michigan Daily called it "the original Oceans 11 (1960 film)|Oceans Eleven for its star power" and compared it to Gosford Park "for its dense structure and stories." He added, " he pacing is quick, the acting is eloquent and the stories are actually interesting. It’s pure theatricality. But Hotel lasted thanks to its simplicity, and the star power doesnt hurt either. This is grand, old Hollywood captured on film." 

==Home video release==
  with highlights of the Hollywood premiere; Nothing Ever Happens, a 1933 Vitaphone short film spoofing Grand Hotel; and theatrical trailers.

The 2013 Warner Home Video Blu-ray release of Grand Hotel contains an audio commentary track by film historians Jeffrey Vance and Mark A. Vieira.

==References==
 

==External links==
 
 
*  
*  
*  
*  

{{Navboxes
|title=Articles and topics related to  
|state=collapsed
|list1= 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 