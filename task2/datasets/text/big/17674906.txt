My Father My Lord
 
{{Infobox film
| name           = My Father My Lord
| image          = My Father My Lord.png
| caption        = 
| director       = David Volach
| producer       = Eyal Shiray
| writer         = David Vollach
| starring       = Assi Dayan Ilan Griff Sharon Hacohen
| music          = 
| cinematography = Boaz Yehonatan Yaacov
| editing        = Haim Tabacmen
| distributor    = Kino International Corp.
| released       =  
| runtime        = 73 minutes
| rating         =
| country        = Israel
| language       = Hebrew
| budget         = 
}}
My Father My Lord ( , lit. Summer Vacation) is a 2007 Israeli film directed by David Volach, a former Israeli Haredi.  It won the Founders Award for Best Narrative Film at the Tribeca Film Festival.

==Plot==
Rabbi Avraham and his wife Esther have one son, Menachem, whose birth they regard as miraculous. Menachems curiosity about the world is repeatedly stymied by his father, who in one instance forces him to rip up an "idolatrous" picture.  Foreshadowed by an instance of Shiluach haken a trip to the Dead Sea, the eponymous "summer vacation.

==Cast==
*Assi Dayan &ndash; Rabbi Avraham 
*Ilan Griff
*Sharon HaCohen

== Gallery ==

Places that appear in the movie :

  the school of Menachem Stairs street  Shabbat Square
 

==External links==
* 
* 

 
 
 

 