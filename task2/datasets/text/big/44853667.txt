Bhale Bullodu
{{Infobox film
| name           = Bhale Bullodu
| image          =
| caption        =
| writer         =
| story          =
| screenplay     =
| producer       = V. B. Rajendra Prasad
| director       = Sarath
| starring       = Jagapathi Babu Soundarya Koti
| cinematography =
| editing        =
| studio         = Jagapathi Art Pictures
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Bhale Bullodu  is a 1995 Telugu cinema|Telugu, drama film produced by V. B. Rajendra Prasad on Jagapathi Art Pictures banner, directed by Sarath. Starring Jagapathi Babu, Soundarya in the lead roles and music composed by Saluri Koteswara Rao|Koti.   

==Cast==
*Jagapathi Babu as Krishna 
*Soundarya
*Kota Srinivasa Rao as Papa Rao Srihari
*Betha Sudhakar
*Giri Babu
*Babu Mohan Allu Ramalingaiah
*Raja
*Jayasudha
*Jayalalita

==Soundtrack==
{{Infobox album
| Name        = Bhale Bullodu
| Tagline     = 
| Type        = film Koti
| Cover       = 
| Released    = 1995
| Recorded    = 26:22
| Genre       = Soundtrack
| Length      = 
| Label       = Supreme Music Koti
| Reviews     =
| Last album  = Love Game   (1995)
| This album  = Bhale Bullodu   (1995)
| Next album  = Rikshavodu   (1995)
}}

Music composed by Saluri Koteswara Rao|Koti. Music released on Supreme Music Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 26:22
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Akkada Ekkada
| lyrics1 = Veturi Sundararama Murthy SP Balu, Chitra
| length1 = 4:54

| title2  = Yesukora Naarigaa 
| lyrics2 = Veturi Sundararama Murthy
| extra2  = S. P. Sailaja,Chitra
| length2 = 4:08

| title3  = Muddu Mudduga
| lyrics3 = Bhuvanachandra
| extra3  = SP Balu,Chitra
| length3 = 4:20

| title4  = Nee Bumper Shoku 
| lyrics4 = Veturi Sundararama Murthy
| extra4  = SP Balu,Chitra
| length4 = 4:44

| title5  = Amma Nanna Leni
| lyrics5 = Veturi Sundararama Murthy
| extra5  = Radhika
| length5 = 3:23

| title6  = Chinnadani Chirachudu  
| lyrics6 = Bhuvanachandra  
| extra6  = SP Balu,Chitra
| length6 = 4:53
}}

==References==
 

 
 
 


 