Aakrosh (2004 film)
 
{{Infobox film
| name           = Aakrosh
| image          = 
| image_size     =
| caption        = 
| director       = Prashanta Nanda
| producer       = Kusum Dhanuka
| writer        = Prashant Nanda
| story          = Prashant Nanda
| screenplay     = Jeet Rituparna Rajesh Sharma Shyamal Dutta Sagnik Chattopadhyay Pushpita Mukherjee 
| music          = Prashanta Nanda
| cinematography = 
| editing        =
| studio         = Pradip Production
| genre           = 
| distributor    =
| released       =  
| runtime        = 2:30:51
| country        = India Oriya
| budget         =
| gross          =
}}
 Bengali film Jeet & Rituparna Sengupta in lead roles.

==Cast== Jeet as Abhimanyu das/Obhi
* Rituparna Sengupta as Nandita
* Mihir Das as Abinash das
* Sabyasachi Chakraborty
* Subhasish Mukhopadhyay
* Anamika Saha as Obhis Mother Rajesh Sharma
* Shyamal Dutta as Nanditas father
* Sagnik Chattopadhyay as Prokash
* Pushpita Mukherjee as Nisha

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Music !! Duration
|-
| 1
| Ei Shahore Lege Gelo Aagun
| Prashanta Nanda
| 
|-
| 2
| Ei Shunsan Kalo Ratrire
| Prashanta Nanda
| 4:07
|-
| 3
| Kalo Kalo Chokhe
| Prashanta Nanda
| 
|-
| 4
| Ki Kore Je Banchi
| Prashanta Nanda
| 6:20
|-
| 5
| Tumi Aami Pathohara
| Prashanta Nanda
| 4:41   
|}

==See also==
 

== References ==
 

==External links==
 
* http://www.filmlinks4u.to/2010/05/aakrosh-2004-bengali-movie-watch-online.html

 
 
 
 
 


 