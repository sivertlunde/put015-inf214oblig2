The Deep (2012 film)
 
{{Infobox film
| name           = The Deep
| image          = The Deep poster.jpg
| caption        = Theatrical release poster
| director       = Baltasar Kormákur
| producer       = 
| writer         = Jón Atli Jónasson Baltasar Kormákur
| starring       = Ólafur Darri Ólafsson Ben Frost
| cinematography = Bergsteinn Björgúlfsson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Iceland
| language       = Icelandic
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards,    making the January shortlist.    It was also nominated for the 2013 Nordic Council Film Prize. The film is based on the true story of Guðlaugur Friðþórsson.

==Cast==
* Ólafur Darri Ólafsson as Gulli
* Jóhann G. Jóhannsson as Palli
* Þorbjörg Helga Þorgilsdóttir as Halla
* Theodór Júlíusson as Gullis Father
* María Sigurðardóttir as Gullis Mother
* Björn Thors as Hannes
* Þröstur Leó Gunnarsson as Lárus
* Guðjón Pedersen as Erlingur
* Walter Grímsson as Raggi
* Stefán Hallur Stefánsson as Jón

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Icelandic submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 