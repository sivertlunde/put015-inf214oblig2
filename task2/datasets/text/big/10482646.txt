Live in East Berlin
  German thrash Noise Records. It was recorded on the Extreme Aggression tour.

==Track listing==
#"Some Pain Will Last" - 6:15
#"Extreme Aggression" - 4:27
#"Under the Guillotine" - 4:36
#"Toxic Trace" - 4:22
#"Bringer of Torture" - 2:45
#"Pleasure to Kill" - 6:14 
#"Flag of Hate" - 2:55 
#"Drum Solo" - 2:51 
#"Terrible Certainty" - 5:53 
#"Riot of Violence" - 8:00
#"Love Us or Hate Us" - 3:54 
#"Behind the Mirror" - 4:52 
#"Betrayer" - 7:34 
#"Awakening of the Gods" - 7:09 
#"Tormentor" - 5:26

* Tracks 1,2,5,11,13 from the LP "Extreme Aggression" (1989)
* Tracks 3,6,10 from the LP "Pleasure To Kill" (1986)
* Tracks 4,9,12 from the LP "Terrible Certainty" (1987)
* Tracks 7,15 from the LP "Endless Pain" (1985)
* Track 14 from the EP "Flag Of Hate" (1986)

 
 
 
 


 