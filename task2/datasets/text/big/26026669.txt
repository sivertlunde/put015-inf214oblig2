Troublesome Night 8
 
 
{{Infobox film
| name = Troublesome Night 8
| image =
| caption =
| film name = {{Film name| traditional = 陰陽路八之棺材仔
| simplified = 阴阳路八之棺材仔
| pinyin = Yīn Yáng Lù Bā Zhī Guān Cái Zǎi
| jyutping = Jam1 Joeng4 Lou6 Baat3 Zi1 Gun1 Coi4 Zai2}}
| director = Edmond Yuen
| producer = Nam Yin
| writer = Jameson Lam
| starring = 
| music = Mak Jan-hung
| cinematography = Andy Fan
| editing = Eric Cheung
| studio = Nam Yin Production Co., Ltd. East Entertainment Limited B&S Limited
| distributor = B&S Films Distribution Company Limited
| released =  
| runtime =  90 minutes
| country = Hong Kong
| language = Cantonese
| budget =
| gross = HK$33,045
}}
Troublesome Night 8 is a 2000 Hong Kong horror comedy film produced by Nam Yin and directed by Edmond Yuen. It is the eighth of the 19 films in the Troublesome Night film series.

==Plot==
Bud Pit and his girlfriend Moon leave the city and move to the countryside for a new life. While moving in, Bud Pit accidentally knocks over a small cremation urn and is disturbed by a spirit for a while. On the other hand, he is attracted to his new, cheerful neighbour Olive. He becomes suspicious of her when he sees her roaming the streets at night, not recognising him and behaving like a completely different person. He seeks help from his mother, Mrs Bud Lung, an expert ghostbuster, who helps them unravel the mystery behind Olive. They learn that Olive is actually possessed by a female ghost who is looking for her lost son. Mrs Bud Lung promises to help the ghost reunite with her (also deceased) son on the condition that she leaves Olives body after the reunion.

==Cast==
* Nadia Chan as Olive
* Simon Lui as Bud Pit
* Law Lan as Mrs Bud Lung
* Maggie Cheung Ho-yee
* Tong Ka-fai as Bud Gay
* Halina Tam as Moon / Ah Sau
* Kwai Chung as Baat
* Kau Man-lung as Kau
* Alson Wong
* Law Wai-cheung
* Jameson Lam
* Jeff Kam
* Wong Sin-gee
* Cheuk Man

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 