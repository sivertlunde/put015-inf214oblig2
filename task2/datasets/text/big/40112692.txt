Le Barbier de Séville (film)
 
  Rossini based the play Beaumarchais (in the translation by Castil-Blaze). Filmed at the Boulogne-Billancourt studio it uses the Théâtre national de lOpéra-Comique production of the time. 

It was filmed in 1947, released in May 1948, and lasts around 95 minutes. 

The opera had been seen at all the principal lyric theatres in Paris; at the Salle Favart it had been performed over 500 times by the time of the film, which features several popular singers from the company. 

The film director is Jean Loubignac, director of photography  , sets by Louis Le Barbenchon, and producer Claude Dolbert, for Codo-Cinéma.

==Cast==
*Roger Bussonnet as Figaro
*Raymond Amade as Almaviva
*  as Rosine
*Louis Musy as Don Bartolo
*Roger Bourdin as Don Bazile
*Renée Gilly as Marceline
*Jean Vieuille as Pédrille
*Gustave Wion as LOfficier
*Serge Rallier as LAlcade
*Jean Retty as Le Notaire

The chorus and orchestra of the Opéra-Comique are conducted by André Cluytens.

==References==
 

==External links==
* 
*  at Ciné-Ressources

 

 
 
 
 
 
 

 
 