Excellent Cadavers (film)
{{Infobox film
| name = Excellent Cadavers
| image = Excellent Cadavers (film).jpg
| director = Ricky Tognazzi
| writer =
| starring = Chazz Palminteri
| music = Michael Tavera
| cinematography = Alessio Gelsini Torresi
| editing = Roberto Silvi
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy United States
| language =
| budget =
}}
Excellent Cadavers (also known as Falcone)  is a 1999 television film directed by Ricky Tognazzi. 
 the book with the same name by Alexander Stille and tells the real life events of judge Giovanni Falcone. It was filmed in Palermo and Rome.   

The TV movie was broadcast by HBO October 16, 1999.  In Italy it was released theatrically with the title I giudici - vittime eccellenti on August 2000. 

== Cast ==
* Chazz Palminteri as Giovanni Falcone
* F. Murray Abraham	 as  	Tommaso Buscetta Francesca Falcone
* Andy Luotto	 as  	Paolo Borsellino
* Lina Sastri	 as  	Agnese Borsellino
* Arnoldo Foà	 as 	Judge Antonino Caponnetto
* Ivo Garrani	 as 	Judge Gaetano Costa
* Gianmarco Tognazzi	 as  	Ninì Cassara
* Pierfrancesco Favino	 as  	Mario
* Mattia Sbragia	 as 	Judge Quinzi
* Francesco Benigno	 as  	Pino Greco
* Mario Erpichini	 as 	Judge Rocco Chinnici
* Victor Cavallo	 as  	Totò Riina
* Giuseppe Cederna	 as  	Judge Giuseppe Ayala
* Luigi Maria Burruano	 as 	Luciano Liggio
* Tony Sperandeo	as 	Stefano Bontade
*  Lorenzo Wilde    as Reporter

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 