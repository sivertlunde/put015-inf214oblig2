Le braghe del padrone
 
{{Infobox film
| name           = Le braghe del padrone
| image          = Le braghe del padrone.jpg
| caption        = 
| director       = Flavio Mogherini
| producer       = Giorgio Salvioni
| writer         = Flavio Mogherini Alberto Silvestri
| starring       = Enrico Montesano
| music          = Riz Ortolani
| cinematography = Carlo Carlini
| editing        = Adriano Tagliavia
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}
Le braghe del padrone is a 1978 Italian comedy film directed by Flavio Mogherini and starring Enrico Montesano.

==Cast==
* Enrico Montesano - Vittorio Pieroni
* Adolfo Celi - Il presidente
* Milena Vukotic - Lilly
* Paolo Poli - Il diavolo
* Felice Andreasi
* Enrico Beruschi - Larchitetto Silvestri
* Eugene Walter
* Rebecca Reder
* Vanna Brosio
* Silvano Bernabei
* Ferruccio Brembilla
* Mara Krupp
* Giulio Massimini
* Benito Barbieri
* Veronica Wells
* Mario Granato
* Carlo Ceruti
* Alba Maiolini

==External links==
* 

 
 
 
 
 
 
 

 
 