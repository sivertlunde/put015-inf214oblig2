Death Ship (1980 film)
{{Infobox film
| name           = Death Ship
| image          = DeathShp.jpg
| caption        = Death Ship theatrical poster
| director       = Alvin Rakoff
| producer       = Derek Gibson Harold Greenberg
| writer         = Jack Hill David P. Lewis John Robins
| starring       = George Kennedy Richard Crenna Nick Mancuso Sally Ann Howes
| music          = Ivor Slaney
| cinematography = René Verzier
| editing        = Mike Campbell Avco Embassy Pictures
| released       =  
| runtime        = 91 minutes
| country        = Canada United Kingdom
| language       = English
| budget         = 
| gross          = $1.75 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 259 
}} George Kennedy, Black Christmas actor Nick Mancuso.

==Plot== freighter sails through the night, apparently deserted. Detecting a cruise ship close by, the ship alters course as disembodied voices announce in German, "Battle Stations! Enemy in sight!"  Aboard the cruise ship, the prickly Captain Ashland (Kennedy) is making his final voyage, attended by his replacement, Captain Trevor Marshall (Crenna), who has brought along his family. Outside, the freighter heads right for them, blasting its horn, and despite Ashlands best efforts, collides with the cruise ship, sinking it (Some of the sinking scenes were taken from the 1960 film The Last Voyage and darkened to match the nightly effect.). The next morning, a handful of survivors, including Marshall, his wife Margaret (Howes), their children Robin and Ben, a young officer named Nick (Mancuso), his love interest Lori (Burgoyne), the ships comic Jackie (Rubinek), and a passenger, Mrs. Morgan (Reid),  are adrift on a large piece of wreckage. Ashland surfaces nearby, and is brought aboard barely conscious. Later, the survivors come upon the black freighter, unaware it is the ship that attacked them. Finding a boarding ladder slung from the stern, they climb aboard, but not before the ladder plunges into the sea as the officers try to climb it with the injured Ashland. When all are finally aboard, Jackie tries to rally the survivors with humour, but a cable seizes him by the ankle, and he swung outboard by one of the ships cranes, which lowers him into the water before cutting him loose, to be swept astern and lost.

Shocked, the survivors explore the corridors of the empty, echoing vessel, finding only cobwebs and 1930s memorabilia. Hatches open and close by themselves and lights go on and off while a swinging block knocks out Nick, who is exploring above deck.  Meanwhile, a delirious Ashland hears a mysterious voice speaking to him in German, telling him "This ship has been waiting for you...It is your new ship....Your chance has come!" The others finally set up in a dusty bunk room, and break up to retrieve supplies and the injured Captain.  Finding a gramophone and a movie projector that apparently play by themselves. While watching the film (1936s "Everything Is Rhythm"), and eating a piece of hard candy from one of the ships cupboards, Mrs. Morgan becomes grotesquely deformed and stumbles back to the bunk room, where a possessed Ashland strangles her.
 lifeboats lower Mauser rifle.
 steering gear and is crushed to death. His agonized screams echo through the ship, joining those of its earlier victims. Above, the Marshalls rejoice as the freighter turns and sails away. After drifting for some time, they are spotted by a search helicopter and rescued.  

The Death Ship is shown afterwards steaming along at full speed. The ghosts of the crew once again announcing "Enemy in sight!" in German. It heads for another passenger ship, and the sounds of the collision are accompanied by the triumphant blasting of its horn.

==Cast==
* George Kennedy as Captain Ashland
* Richard Crenna as Trevor Marshall
* Nick Mancuso as Nick
* Sally Ann Howes as Margaret Marshall
* Kate Reid as Sylvia Morgan
* Victoria Burgoyne as Lori
* Jennifer Mckinney as Robin Marshall
* Danny Higham as Ben Marshall
* Saul Rubinek as Jackie
* Murray Cruchley as Parsons

==Reception==
 
 
Death Ship received mixed reviews upon its original release. 
The film currently holds a two-star rating (4.5/10) on IMDb. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 