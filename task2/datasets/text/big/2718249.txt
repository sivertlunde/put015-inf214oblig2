The Shock (1923 film)
{{Infobox film
| name           = The Shock
| image          = The Shock (1923) - Chaney.jpg
| caption        = Ad for the film using its working title
| director       = Lambert Hillyer
| producer       = Universal Pictures
| writer         = William Dudley Pelley (story) Arthur Statter (?scenario) Charles Kenyon (?scenario) Lon Chaney
| music          =
| cinematography = Dwight Warren
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 7 reels (6,738 ft)
| country        = United States Silent (English intertitles)
}}
 1923 American Lon Chaney as a cripple named Wilse Dilling. The film was based on a story by William Dudley Pelley. This is one of the rare Lon Chaney films where he gets the girl.

==Plot==
Wilse Dilling, a crippled brute living in the savage streets of Chinatown, receives a coded message to go to the home of his boss, Ann Cardington, known as Queen Ann, a powerful crime boss feared in the underworld. When Wilse meets with her, she sends him to the town of Fallbrook, where he is to await her instructions in dealing with a former lover of hers, a banker named Micha Hadley, who had betrayed her. Being practically wheelchair-bound has not stopped Dilling from committing a lengthy series of crimes, but to his surprise, he finds that the small town atmosphere makes him feel differently about everything. He finds a good friend in Hadleys daughter Gertrude, whom Wilse not only falls in love with but she helps him believe that he can make a fresh start. But Wilses new-found contentment is soon shattered by a series of new developments which includes trying to stop Queen Anns plot towards Hadley and Gertrude being killed.

==Cast== Lon Chaney as Wilse Dilling
* Virginia Valli as Gertrude Hadley
* Jack Mower as Jack Cooper William Welsh as Micha Hadley
* Henry A. Barrows as John Cooper, Sr.
* Christine Mayo as Ann Cardington, AKA "Queen Anne"
* Harry De Vere as Olaf Wismer John Beck as Bill Walter Long as The Captain
* Bob Kortman as Henchman

==Production==
The working title of the film was Bittersweet.   at silentera.com 

==Preservation status==
A print of the film is maintained in an archive. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 