Wonderful World (2009 film)
 
{{Infobox film
| name          = Wonderful World
| image         = Wonderful world poster.jpg
| caption       = Theatrical release poster
| director      = Joshua Goldin
| producer      = Miranda Bailey  Matthew Leutwyler  Glenn Williamson
| writer        = Joshua Goldin
| starring      = Matthew Broderick  Sanaa Lathan  Michael K. Williams Jodelle Ferland
| music         = Craig Richey
| editing       = Jeff Canavan
| released      =  
| country       = United States
| language      = English
| gross         =  $9,309 
}}
Wonderful World  is a 2009 dark comedy-drama film directed and written by Joshua Goldin, who in this movie makes his directorial debut. The film stars Matthew Broderick, Sanaa Lathan, Michael K. Williams and Jodelle Ferland.

The film was produced by Ambush Entertainment, Back Lot Pictures and Cold Iron Pictures with K5 International handling the world sales. In the summer of 2009, the film was picked up by Magnolia Pictures for distribution in 2010. Filming took place in Shreveport, Louisiana.

==Plot== diabetic coma and is taken to the hospital, his sister Khadi (Sanaa Lathan) arrives from Senegal to take care of him. After Khadi and Ben eventually fall in love, circumstances lead Ben to reconsider his way of thinking.

==Reception==
The film has received mixed or average reviews from critics.    Based on 24 reviews, Rotten Tomatoes reported that 38% of critics gave positive reviews with an average rating of 5.2/10.  Another review aggretator, Metacritic, gave the film an average rating of 47% based on 14 reviews.   
 New York Magazine film critic David Edelstein gave the film a positive review stating "the movie is unfailingly likable and finally impressive."  

==Cast==
* Matthew Broderick as Ben Singer
* Sanaa Lathan as Khadi
* Michael K. Williams as Ibou
* Jodelle Ferland as Sandra
* Philip Baker Hall as The Man
* Jesse Tyler Ferguson as Cyril
* Patrick Carney as Evan
* Ally Walker as Eliza
* Dan Zanes as Sweeny
* David L. J. George as the Male Nurse

==See also==

*List of films featuring diabetes

==References==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 