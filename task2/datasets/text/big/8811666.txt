McLaren's Negatives
{{Infobox film
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Marie-Josée Saint-Pierre
| producer       = Marie-Josée Saint-Pierre
| writer         = Marie-Josée Saint-Pierre
| starring       = Patrick Coutu
| music          = Dominique Cote Tyler Mauney Adam OCallaghan
| cinematography =Brigitte Archambault Marie-Josée Saint-Pierre
| editing        = Kara Blake
| studio         = MJSTP Films
| distributor    = Videographe Inc.
| released       =  
| runtime        = 10 minutes
| country        = Canada
| language       = English
}}
McLarens Negatives is a 2006 short animated documentary directed by French Canadian filmmaker Marie-Josée Saint-Pierre. The film is a study of the Canadian animator Norman McLaren, and his personal view of film making. The short film won several awards, including the 2007 Jutra Award for best animated short film. 

==Cast==
{|class=wikitable
|-
!Actor !!Role
|- Patrick Coutu Norman
|}

==Awards==
*JUTRA for Best Animated Film 2007 (Québec)
*Sterling Short Honorable Mention, Silverdocs 2006 (USA)
*Best Debut Film, Animation, Message to Man International Film Festival 2006 (Russia)
*Best Short Contemporary Film, Sapporo International Shortfest 2006 (Japan)
*Bar in Gold, Festival Der Nationen 2006 (Austria)
*Best Short Documentary Film, Animation Block Party New York 2006 (USA)
*Best Animation Film, Dokufest 2006 (Kosovo)
*Best International Documentary, Santiago International Short Film Festival 2006 (Chile)
*Best Director Animation, Monstramundo 2006 (Brazil)
*Best Script Animation, Monstramundo 2006 (Brazil)
*Youth Jury Award for Most Inspirational Short Film, Real 2 Real International Film Festival for Youth 2007 (Canada)
*First Prize in the Documentary Section, 10 Mostra Internacional de Curtmetratges de Sagunt 2007 (Spain)
*Best Documentary, PA Film Institute Festival 2007 (USA)
*Platinum Remi, Best Animation, Worldfest Houston 2007 (USA)
*Special Jury Prize, Outstanding Direction Documentary, Indianapolis Film Festival 2007 (USA)
*Best Documentary, Arizona International Film Festival 2007 (USA)
*Special Jury Award for Best Animated Film, Ismailia International Film Festival 2007 (Egypt)
*Best Documentary, Filmest Badalona Festival 2007 (Spain)
*Honorable Mention, Best Short International Film, Festival International de Curtas de Belo Horizonte 2006 (Brazil)
*Honorable Mention, International Animation Competition, Monterrey International Festival 2006 (Mexico)
*Special Mention Animation, Seddicorto International Festival 2006 (Italia)
*Special Mention Animation, Luciana Film Festival 2006 (Italia)

==See also==
*Ryan (film)|Ryan

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 

 
 