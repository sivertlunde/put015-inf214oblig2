Tarawa Beachhead
{{Infobox film
| name           = Tarawa Beachhead
| image          = Tarbeach.jpg
| image_size     =
| caption        = Original film poster
| director       = Paul Wendkos
| producer       = Charles H. Schneer
| writer         = Richard Alan Simmons
| narrator       =
| starring       = Kerwin Mathews Ray Danton Julie Adams
| music          =
| cinematography = Henry Freulich
| editing        =
| studio         = Columbia Pictures
| distributor    =
| released       = November 2, 1958
| runtime        = 77 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}} 1958 film directed by Paul Wendkos. It stars Columbia Pictures contract star Kerwin Mathews in his first leading role and the husband and wife team of Ray Danton and Julie Adams.   The working title of the film was Flag over Tarawa and was originally to have starred Ronald Reagan. 

==Plot== 2nd Marine Division headquarters in New Zealand.

Lt. Sloan meets Campbells widow, Ruth (Julie Adams) to bring her letters written by Johnny.  However he meets Brady who is keeping company with Ruths sister (Karen Sharpe).

Sloan lands on Tarawa with Brady, now a Captain; each hating each other more than the Japanese.

==Production==
The film contains extensive amounts of actual footage of the Battle of Tarawa, however the United States Marine Corps refused to cooperate with the producers due to the theme of an officer out for glory killing his own men and a sergeant out for revenge. 

==Cast==
*Kerwin Mathews as Sergeant (later Lieutenant) Thomas A. Tom Sloan
*Julie Adams as Ruth Nelson Campbell
*Ray Danton as Lieutenant (later Captain) Joel Brady
*Karen Sharpe as Paula Nelson
*Onslow Stevens as General Nathan Keller

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 