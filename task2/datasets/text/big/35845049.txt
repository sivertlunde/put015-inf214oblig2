Dragonworld: The Legend Continues
{{Infobox film
| name           = Dragonworld: The Legend Continues   Shadow of the Knight    (1999 USA/Canada VHS title) 
| image	         =
| caption        =
| director       = Ted Nicolaou
| producer       = Robert Bernacchi Vlad Paunescu
| writer         = Ted Nicolaou
| starring       = Drake Bell Richard Trask Andrew Keir James Ellis Tina Martin Judith Paris
| music          = Richard Kosinski William Levine
| cinematography = Vlad Paunescu
| editing        = Gregory Sanders
| studio         = The Kushner-Locke Company Castel Film Romania
| distributor    = Full Moon Entertainment Paramount Pictures   Tango Entertainment  (2005 USA/Canada Release) 
| released       =  
| runtime        = 78 minutes
| country        = United States Scotland United Kingdom Romania English
| budget         =
| gross          =
}}

Dragonworld: The Legend Continues (also known as: Dragonworld II and originally released on   sequel to the original 1994 film, Dragonworld. The film was actually originally filmed in 1996, but did not see a release until 1999. Andrew Keir is the only cast member to reprise his role as Angus McGowan from the original film, and its the last film that he has acted in, during the 1996 production, before his death in 1997, making this film dedicating towards him as a memory. 

==Plot==
The film bears little relation to the first movie. Johnny McGowan and his only true friend who is Yowler, the last dragon on Earth, is in serious trouble. The Dark Knight, who happens to be the enemy of Yowler, has returned and has determined to plan on slaying Yowler, so that way, he can get a hold of the magical powers inside the dragons blood to bring in a new evil age of dark generations to the planet. Young John is now gained an opportunity to begin a new quest to protect Yowler, defeat the Dark Knight and save the world for all future generations.

==Cast==
*Drake Bell as Johnny McGowan
*Richard Trask as Yowler the Dragon
*Andrew Keir as Angus McGowan
*James Ellis as McCoy
*Tina Martin as Mrs. Cosgrove
*Judith Paris as Mrs. Churchill
*Constantin Barbulescu as MacClain
*Avram Besoiu as Kimison
*Julius Liptac as Kimison #1 (credited as Iulius Liptac)
*Cezar Boghina as Kimison #2
*Mihai Verbintschi as Kimison #3 (credited as Mihai Verbitchi)
*Gheorghe Flonda as Mob 1
*Dan Glasu as Mob 2
*Ovidiu Mot as Mob 3

==Development== Castel Film Studios of Bucharest, Romania.

==Reception== original film. Rotten Tomatoes currently holds a 36% score of this film.  The film has been panned for its poor directing, poor storyline, poor acting and poor designed special effects, especially with Yowler, who was played by Richard Trask in a Dragon suit, giving the film a poor design reaction towards Critics.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 