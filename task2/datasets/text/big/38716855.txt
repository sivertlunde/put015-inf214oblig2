War, Love, God, & Madness
{{Infobox film
| name           = War, Love, God, & Madness
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| Website        = www.kevinsolutions.net
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
|genre= documentary
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}} documentary directed by Mohamed Al-Daradji in 2008. The film runs 83 minutes and is based on the Iraq War.  The film documents the efforts of the making of the film Ahlaam in Iraq, during the War.   
 English subtitles are available.

==Reception==
Mark Kermode, of the BBC called it "An amazing and uplifting documentary, which shows precisely how dangerous shooting in Iraq can be."   

Jay Weissberg, writing for Variety, said, "Visuals are often blurred, but given the dangers of filming, it’s miraculous they have so much." 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 
 