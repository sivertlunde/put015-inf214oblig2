The Swimmer (2013 film)
{{Infobox film
| name           = The Swimmer
| image          = TheSwimmer_2013_OfficialPoster.jpg
| alt            = 
| caption        = Official Poster
| director       = Giovanni Basso
| writer         = Giovanni Basso
| producer       = Giovanni Basso
| starring       = Tommaso Puccini Daniel Wilkins Adam T Perkins Olivia Charlotte
| studio         = Magnet Films
| released       =  
| runtime        = 16 minutes
| country        = Australia
| language       = English
}}
The Swimmer is a 2013 Australian drama short film written and directed by Giovanni Basso and produced by Magnet Films.

It held its US premiere on March 21, 2013 at the Gasparilla Film Festival in Florida. As of November 2013 it has been accepted in competition by more than 20 international film festivals,  including the Hollywood Film Festival,  the New York City Horror Film Festival  and the New Orleans Film Festival. On November 19, 2013 it was released worldwide on the  .

==Plot==
After being attacked by a shark, 10 year-old Sam struggles to swim as he used to. Harassed by a local bully hes forced to face his fears for the very last time.

==Cast==
*Tommaso Puccini as Sam
*Daniel Wilkins as Jay
*Adam T Perkins as Carl
*Olivia Charlotte as Alice
*Finleigh Dignam as Ellie
*Luca Wilkins as Dale
*Shae Kelly as Video Store Guy

==Notes== Red Epic X camera. 

==Locations== Mount Lawley, Coogee Beach. 

==See also==
 
* Shark attack

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 
 