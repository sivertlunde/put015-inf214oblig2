Honolulu Lu
{{Infobox film
| name           = Honolulu Lu
| image          = 
| image size     = 
| alt            = 
| caption        =  Charles Barton
| producer       = Wallace MacDonald
| writer         = Eliot Gibbons
| based on       = 
| screenplay     = Paul Yawitz Eliot Gibbons Ned Dandy
| narrator       = 
| starring       = Lupe Velez Bruce Bennett
| music          = Saul Chaplin Sammy Cahn
| cinematography = Franz Planer James Sweeney
| studio         = Franz F. Planer
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 72 min.
| country        =   English
| budget         = 
| gross          = 
}}
 1941 Musical Charles Barton, starring Lupe Velez and Bruce Bennett.

==Plot summary==
 
In Hawaii, Consuelo Cordoba (Lupe Velez) is a risque nightclub act and due to her involvement with a group of sailors becomes a beauty queen. 

==Cast==
* Lupe Velez as Consuelo Cordoba
* Bruce Bennett as Skelly
* Leo Carrillo as Don Estaban Cordoba
* Marjorie Gateson as Mrs. Van Derholt
* Don Beddoe as Bennie Blanchard
* Forrest Tucker as Barney
* George McKay as Horseface
* Nina Campana as Aloha
* Roger Clark as Bill Van Derhoolt
* Helen Dickson as Mrs. Smythe
* Curtis Railing as Mrs. Frobisher

==References==
 	

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 