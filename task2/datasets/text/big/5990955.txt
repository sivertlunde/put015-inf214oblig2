Thotti Jaya
{{Infobox film
| name           = Thotti Jaya
| image          = Thotti Jaya DVD Cover.jpg
| caption        = DVD Cover Durai
| producer       = Kalaipuli Dhanu Durai
| Pradeep Rawat Vincent Asokan
| music          = Harris Jayaraj
| cinematography = R. D. Rajasekhar Anthony
| distributor    = V.Creations
| released       = 9 September 2005
| runtime        = 162 minutes
| country        = India
| language       = Tamil
}}
Thotti Jaya is an Indian film released in 2005. It was written and directed by V. Z. Durai|Durai. The soundtrack was composed by Harris Jayaraj.The film received positive reviews and was declared as a blockbuster.

==Plot==
  Pradeep Rawat) and hired as a hench man.In an incident, Jaya gets entangled in a political and police trap. To escape from police, Jaya slips to Calcutta and goes into hiding. Meanwhile Brinda (Gopika), a college lass from Kanyakumari comes to Calcutta on a tour along with her friends. Pimps operating in the red light area of Calcutta take away Brinda. Jaya accidentally meets Brinda when she tries to escape from the gang. He helps Brinda escape from the gang and takes her safely to Kanyakumari. On her way back home by train, Brinda starts admiring Jayas niceties and gradually falls in love with him. When she expresses her desire, Jaya reciprocates her love, and both decide to enter into wedlock but little does he know that Brindha is actually the daughter of Seena Thana. Jaya takes away Brinda from her house and thus, earns the wrath of Seena Thana. Angered by this, Seena Thana summons his rowdy gangs and plots to bump off Thotti Jaya. How Thottijaya accomplishes his hopes of marrying Brinda is told in the remaining part of the story.

==Cast==
*Silambarasan as Jayachandran alias "Thotti Jaya"
*Gopika as Brinda Pradeep Rawat as Seena Thana

==Production== Jeevan to play lead role and also conducted test shoots with him but director was not impressed with his performance he replaced him with Simbhu. Thanu was initially reluctant to have him as lead actor since Thanu had misunderstandings with his father T. Rajender during the time of Coolikaaran, later he made up his mind. Nayanthara was initially selected as heroine but she was replaced with Gopika.  Simbhu refused to dub for the film after shooting citing salary problems, the problem was resolved in Tamilnadu Producers Council. 

==Soundtrack==
{{Infobox album|  
  Name        = Thotti Jaya
|  Type        = Soundtrack
|  Artist      = Harris Jayaraj
|  Cover       =
|  Released    = 2005
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = An Ak Audio
|  Producer    =
|  Reviews     =
|  Last album  = Arul (2004)
|  This album  = Thotti Jaya (2005)
|  Next album  = Ullam Ketkumae (2005)
}}
There are six songs composed by Harris Jayaraj collaborated with Durai for first and only time. Lyrics written by  Na. Muthukumar, Thamarai, Kabilan (lyricist)|Kabilan.  Yuvan Shankar Raja, who was first signed as the music director,  composed one song for the film, not included in the soundtrack and is credited in the title card. "Yaaridamum" was re-used from Harriss own song "Ye Chilipi" from Telugu film Gharshana, remake of Kaakha Kaakha.

{| class="wikitable" width="50%"
|-
! Song title !! Singers
|-
| Yaaridamum || Harini, Ramesh Vinayagam
|-
| Thotta Poweru Da  || Shankar Mahadevan
|-
| Yaari Singaari || Karthik (singer)|Karthik, Srilekha Parthasarathy, Palakkad Sriram, Ceylon Manohar
|-
| Acchu Vellam Karumbe || Ranjith (singer)|Ranjith, Saindhavi, Shankar Mahadevan
|-
| Uyire En Uyire || Karthik (singer)|Karthik, Anuradha Sriram, Bombay Jayashri
|-
| Theme Music || INSTRUMENTS HARRIS
|-
|  Intha ooru  || Yuvan shankar raja
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 