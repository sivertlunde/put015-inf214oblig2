The Secret of the Loch
{{Infobox film
| name           = The Secret of the Loch
| image          = 
| caption        = 
| director       = Milton Rosmer
| producer       =  Bray Wyndham Charles Bennett Billie Bristow
| starring       = Seymour Hicks
| music          = Peter Mendoza James Wilson
| editing        = David Lean
| studio         = Wyndham Productions
| distributor    = 
| released       = May 1934  (UK)
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross = 
}}
The Secret of the Loch is a 1934 film about the Loch Ness Monster. It is the first movie made about the monster.  

Charles Bennett says the film was based on his original idea. He later admitted it was "terrible... but amusing". Tom Waver, Double Feature Creature Attack: A Monster Merger of Two More Volumes of Classic Interviews McFarland, 2003 p 18-19 

==Plot==
Professor Heggie is determined to prove to a sceptical sciencific community the existence of a dinosaur in Loch Ness. Young London reporter Jimmy Anderson believes him and offers to help. He also falls in love with Maggie, the professors daughter. Jimmy finally plucks up the courage to enter the Loch himself, and comes face to face with the monster.

==Cast==
*Professor Heggie -	Seymour Hicks
*Angela Heggie - 	Nancy ONeil
*Angus - 	Gibson Gowland
*Jimmy Anderson - 	 Frederick Peisley
*Jack Campbell, the Diver - 	Eric Hales
*Maggie Fraser, the Barmaid - 	Rosamund John
*Piermaster - 	Ben Field
*Reporter - 	Robert Wilton
*Professor Blenkinsop Fothergill - 	Hubert Harben
*Scientist at Meeting - 	Fewlass Llewellyn
*Macdonald - 	 Stafford Hilliard
*Judge -	D. J. Williams (actor)|D.J. Williams
*Reporter/Photographer in Pub - 	Clive Morton
*Mate - 	 Cyril Mclaglen

==Critical reception==
TV Guide called the film "a trite programmer which doesnt make one believe in the humans actions, much less the sea serpents" ;   while Allmovie called it a "fairly amusing British monster movie...obscure but entertaining oddity" ;  and Britmovie noted an "enjoyable comic romp."  

==References==
 

==External links==
* 

 
 
 

 