Kranthiveera Sangolli Rayanna
{{Infobox film 
| name           = Sangolli Rayanna
| image          =  
| caption        = 
| director       = B. T. Athani Gurubala
| producer       = Neminatha Gat
| story          = Rajajith Desai
| writer         = Rajajith Desai (Translation: T. K. Patil) (dialogues)
| screenplay     = Rajajith Desai
| starring       = V. S. Patil Kamini Kadama Dada Salavi Rajashekar
| music          = Lakshman Beralekar
| cinematography = Shankar Savekar
| editing        = Vasantha Shelake
| studio         = Chithravani
| distributor    = Chithravani
| released       =  
| runtime        = 135 min
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, directed by B. T. Athani and produced by Neminatha Gat. The film stars V. S. Patil, Kamini Kadama, Dada Salavi and Rajashekar in lead roles. The film had musical score by Lakshman Beralekar.  

==Cast==
 
*V. S. Patil
*Kamini Kadama
*Dada Salavi
*Rajashekar
*Leela Gandhi
*Saroja Borakara
*Thara
*Shanthamma
*Renukadevi
*Chandrakantha
*Gururaja
*Kodanda Rao
*Padesura Basavaraj
*Srinath
*Maruthi Pailwan
*Sharanayya
*Rama Sevekari
*Sannachari
*Obaleshwara
*Abhi Bhattacharya in guest appearance
 

==References==
 

==External links==
*  


 
 

 