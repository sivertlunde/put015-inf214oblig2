Kurukshetra (1945 film)
{{Infobox film
| name           = Kurukshetra
| image          = 
| image_size     = 
| caption        = 
| director       = Rameshwar Sharma
| producer       = Unity Pictures
| writer         = 
| narrator       = 
| starring       = K. L. Saigal Radharani Nawab Shanti
| music          = Ganpat Rao
| cinematography = 
| editing        = 
| distributor    =
| studio         = Unity Pictures
| released       = 1945
| runtime        = 135 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1945 Hindi film directed by Rameshwar Sharma.    Produced Under the banner of Unity Pictures it starred K. L. Saigal, Radharani, Nawab, Shanti, Biman Bannerji and Shamli and had music composed by Ganpatrao.   

The film was cited as an offbeat film of K. L. Saigal, directed by a relatively unknown director; its stated to be a "forerunner" to later Art Cinema.   

==Cast==
* K. L. Saigal
* Nawab Kashmiri
* Agha Muhamad
* Kashmiri
* Shamli
* Udwadia
* Biman Banerjee
* Shanti
* Radharani Ajit

==Soundtrack==
The music director was Pandit Ganpat Rao who was a classical musician and had trained under Abdul Karim Khan;    the only other film he composed music for was Bebus (1950) where he was co-credited with S. K. Pal.    The lyrics were written by Jameel Mazhari while the singers were Saigal, Radharani, Satya Choudhary and Kalyani Das.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Aaj Kirnon Ka Karke Singaar Chali
| Chorus
|-
| 2
| Aayi Hai Tu Toh, Kaise Dil Apna Dikhaoon Main
| K. L. Saigal
|-
| 3
| Kidhar Hai Tu Ae Meri Tamanna
| K. L. Saigal
|-
| 4
| Tu Aa Gayi Dil Ki Tamanna Jaag Uthi
| K. L. Saigal
|-
| 5
| Mohabbat Ke Gul Hain
| K. L. Saigal
|-
| 6
| Mere Sapno Ke Baasi
| Kalyani Das
|-
| 7
| Ae Dukhi Mann
| Radharani
|-
| 8
| Dil Bekaraar So Ja
| Radharani
|-
| 9
| Kya Karna Hai Pooch Rahe
| Satya Choudhary
|-
| 10
| Bin Kaaj Aaj Maharaj Laaj Gayi Mer
| Radharani
|-
| 11
| Ae Baavle Man Kya Karoon
|
|-
| 12
| Gaj Vadan Gun Sadan
| Chorus
|}

==References==
 

==External links==
* 

 
 
 
 

 