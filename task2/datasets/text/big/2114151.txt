The St. Valentine's Day Massacre (film)
 
{{Infobox film
| name           = The St. Valentines Day Massacre
| image          = The St. Valentines Day Massacre film poster.jpg
| image_size     =
| caption        =
| director       = Roger Corman
| producer       = Roger Corman
| writer         = Howard Browne
| narrator       = Paul Frees
| starring       =  
| cinematography = Milton R. Krasner
| music          = Lionel Newman Fred Steiner
| editing        = William B. Murphy
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 100 min.
| country        = United States
| language       =
| budget         = $2,175,000 
| gross          = $1,700,000 (US/ Canada)  691,328 admissions (France) 
}}
 gangster film Chicago Saint mass murder of seven members of the Northside Gang (led by George "Bugs" Moran) on orders from Al Capone. It was directed by Roger Corman and written by Howard Browne.

The film starred Jason Robards as Al Capone, George Segal as Peter Gusenberg, David Canary as Frank Gusenberg and Ralph Meeker as George "Bugs" Moran.

Orson Welles was originally supposed to play Capone, but Twentieth Century Fox vetoed the deal, fearing that Welles was "undirectable."  The films narration has a style similar to that of Welles but was actually narrated by renowned Hollywood voice actor Paul Frees.

A young Bruce Dern plays one of the victims of the massacre, and Jack Nicholson has a bit part as a gangster. Also featured are Jan Merlin as one of Morans lieutenants and veteran Corman actor Dick Miller as one of the phony policemen involved in the massacre.

==Plot==
An organized crime war breaks out between two rival gangs in Chicago during the Roaring Twenties. The leader of the Southside Mob is the notorious Al Capone, who resents his nemesis George "Bugs" Morans activity in the city. Moran, too, wants control of the towns bootlegging and gambling operations. His lieutenants Peter and Frank Gusenberg use threats and intimidation to make tavern owners do business with them in exchange for "protection." Moran gives the order to have a crony of Capones eliminated as the Chicago body count escalates. Inclusive are flashbacks to a lunchtime attack on Capone at a restaurant outside of Chicago by Hymie Weiss and Moran in September 1926 and the murders of Weiss in October 1926 and Dion OBanion in November 1924 by Capones gang.

In a bid to get rid of Moran once and for all, Capone goes to his winter home in Miami, Florida to establish an alibi while his henchmen, some dressed as cops, ambush and execute seven members of Morans gang in a northside garage on February 14, 1929.
Each character is given a verbal voiceover biography as they are introduced, and in some video releases, the biographies of Rheinhard Schwimmer and Adam Heyer, two of the massacre victims, are removed from the soundtrack, possibly due to protest from surviving family members.

==Cast==
* Jason Robards as Al Capone
* George Segal as Peter Gusenberg
* Ralph Meeker as George "Bugs" Moran
* Jean Hale as Myrtle
* Jan Merlin as Willie Marks
* Clint Ritchie as Machine Gun Jack McGurn
* David Canary as Frank Gusenberg
* Harold J. Stone as Frank Nitti
* Frank Silvera as Nick Sorello
* Joseph Campanella as Albert Wienshank
* Richard Bakalyan as John Scalise
* Charles Dierkop as Salvanti
* John Agar as Dion OBannion Joseph Turkel as Jake Gusik
* Bruce Dern as Johnny May
* Mickey Deems as Reinhardt Schwimmer
* Reed Hadley as Hymie Weiss Alex DArcy as Joe Aiello
* Leo Gordon as Heitler
* Jack Nicholson as Gino, hitman (uncredited)
* Buck Taylor as Poolside interviewer 2 (uncredited)
* Celia Lovsky as Josephine Schwimmer (Reinhardt Schwimmers mother)
* Milton Frome as Adam Hyer
* Mary Grace Canfield as Mrs. Doody (uncredited)

==Background== ABC TV The Untouchables, but is one of many motion pictures adapted from a CBS Playhouse 90 episode; Seven Against The Wall, broadcast on Playhouse 90 in December 1958, was also written by Harold Browne and featured actors Milton Frome, Celia Lovsky and Frank Silvera in the same roles that they play in the film.

To make certain the film would have the look of a gangster film, Roger Corman shot the film at the Desilu studios and used other sections of the back lot for different locales of Chicago. He filmed the Massacre scene in one of the Desilu lots which got converted to look like the garage where the crime was committed. (The real garage was torn down by the time the movie started production). Another matter was the recreation of the Massacre itself: before filming, Corman found photos of the mass murders. Then he had the actors for the scene study the stills, followed by rehearsals and the actual shoot. After one take, the massacre came in the way it looked in the old photos and the collapse of each actors followed the positions the murder victims fell in the real massacre.
 
The film was one of the few that Roger Corman directed from a major Hollywood studio with a generous budget and an open-ended schedule.  While most directors would love such an assignment, Corman was disgusted with the incredible waste of time and money involved with "typical" movie production techniques. He was given a $2.5 million budget and made it for $400,000 less. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p266  Corman, an independent director, was most comfortable in his own style:  shoestring budgets, and shooting schedules measured in days, rather than weeks. Nonetheless, it is generally considered one of his best films as a director.

==Trivia==
In 2009 Empire (magazine)|Empire magazine named it #7 in a poll of the 20 Greatest Gangster Movies Youve Never Seen* (*Probably)

==DVD==
The St. Valentines Day Massacre was released to DVD by 20th Century Fox Home Entertainment on May 23rd, 2006 as a Region 1 widescreen DVD.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 