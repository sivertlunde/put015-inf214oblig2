Good Morning (film)
{{Infobox film
| name           = Good Morning
| image          = Good morning dvd.jpg
| caption        = Criterion DVD cover
| director       = Yasujirō Ozu
| writer         = Kōgo Noda Yasujirō Ozu
| starring       = Keiji Sada Yoshiko Kuga Chishū Ryū Kuniko Miyake
| producer       =
| music          = Toshirō Mayuzumi
| cinematography = Yūharu Atsuta
| distributor    = Shōchiku| Shōchiku Films Ltd.
| released       = May 12, 1959
| runtime        = 94 minutes
| country        = Japan
| language       = Japanese
}}

  is a 1959 comedy film by Japanese director Yasujirō Ozu. It is a loose remake of his own 1932 silent film I Was Born, But..., and is Ozus second film in color.

==Plot==
The film takes place in suburban Tokyo, and begins with a group of boy students going home.

The film steers into a subplot concerning the local womens club monthly dues.  Everyone in the neighborhood club believes that Mrs Hayashi, the treasurer, has given the dues to the chairwoman, Mrs Haraguchi (Haruko Sugimura), but Mrs Haraguchi denies it.  They gossip amongst themselves who could have taken the money, and speculate that Mrs Haraguchi could have used the money to buy for herself a new washing machine.  Later Mrs Haraguchi confronts Mrs Hayashi for starting the rumor and ruining her reputation, but Mrs Hayashi states that she has indeed handed the dues money to Haraguchis mother.  Only later does Mrs Haraguchi realize it was her mistake (her mother being quite senile and forgetful), and she goes to apologize.

The boys are all attracted to a neighbors house because they have a television set, where they can watch their favorite sumo wrestling matches.  (At the time of the films release in Japan, the medium was rapidly gaining popularity.)  However, their conservative parents forbid them to visit their bohemian neighbors because the wife is thought to be a cabaret singer.

As a result of this, the young boys of the Hayashi family, Minoru and Isamu, pressure their mother into buying them a television set, but their mother refuses.  When their father (Chishū Ryū) comes to know about it, he asks the boys to keep quiet when they kick a tantrum.  Minoru throws an anger fit, and states that adults always engage in pointless niceties like "good morning" and refuse to say exactly what they mean.  Back in their room, Minoru and Isamu decide on a silence strike against all adults.  The first neighbor to bear the brunt of this snub is Mrs Haraguchi.

Mrs Haraguchi, angered by this snub, speculates it is Mrs Hayashi who instigates this in revenge over their earlier misunderstanding, and tells this to busybody Mrs. Tomizawa (Teruko Nagaoka).  Soon, everybody thinks Mrs Hayashi is a petty, vengeful person, and is all queueing up to return their loaned items to her.

Minoru and Isamu continue their strike in school, and even against their English tutor.  Finally, their schoolteacher visits to find the root of their silence.  The two boys run off from home with a pot of rice due to hunger, but are caught by a passing policeman.  They disappear for hours into the evening, until their English tutor finds them outside a station watching television.

At the end of the film, the boys find out their parents have indeed purchased a television set to support a neighbour in his new job as a salesman.  Jubilant, they stop their strike at once.  Their English tutor and their aunt appear to be starting a fresh romance.

==Style==
Despite Ozus reputation in the West as an austere and refined director, Good Morning does not shy away from depicting many of the neighborhood boys   an envelope containing money is accidentally dropped into a urinal, and during An Autumn Afternoon in 1962, his final film, jokes are made about an ageing husbands sexual potency. 

Good Morning satirises television ownership. It was released in the year that attendance at Japanese cinemas began to decline, primarily as a result of rising numbers of TV sets. 

== Cast ==
* Keiji Sada ... Heiichiro Fukui
* Yoshiko Kuga ... Setsuko Arita
* Chishū Ryū ... Keitaro Hayashi
* Kuniko Miyake ... Tamiko Hayashi
* Haruko Sugimura ... Kikue Haraguchi
* Shitara Koji ... Minoru Hayashi
* Masahiko Shimazu ... Isamu Hayashi
* Kyoko Izumi ... Midori Maruyama
* Taiji Tonoyama as the door-to-door salesman

==Release==

===Home media===
In 2011, the BFI released a Region 2 Dual Format Edition (Blu-ray + DVD).  Included with this release is a standard definition presentation of I Was Born, But....

==References==
 

==External links==
* 
*  
* 
* 
*  
* 
 

 
 
 
 
 
 
 
 
 