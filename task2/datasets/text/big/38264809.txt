Kurosawa's Way
{{Infobox film
| name           = Kurosawas Way
| image          = Kurosawas-Way-Poster.jpg
| alt            = 
| caption        = French film poster for Kurosawas Way
| director       = Catherine Cadou
| producer       = Michiko Yoshitake   
| writer         = Catherine Cadou
| based on       =  
| narrator       = Catherine Cadou 
| starring       = 
| music          = 
| cinematography = Alexis Kavyrchine Simon Beaufils Tsuneo Azuma Tempei Iwakiri Yoshihiro Ikeuchi 
| editing        = Cedric Defert 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 52 minutes
| country        = France  Persian Korean Persian    
| budget         = 
| gross          = 
}}

Kurosawas Way ( ) is a 2011 French documentary directed and written by Catherine Cadou. The film features 11 major filmmakers from Asia, America and Europe as they discuss how the films of Japanese director Akira Kurosawa influenced them.

==Sypnopsis==
Kurosawas Way is a documentary that is intercut with the Catherine Cadous narration on her own experiences with Kurosawa, and interviews with various directors and archival photographs. The directors interviews focus on both philosophical and technical observations of Kurosawas films. 

==Interviewees==
* Bernardo Bertolucci
* Julie Taymor
* Theo Angelopoulos
* Alejandro González Iñárritu
* Abbas Kiarostami
* Shinya Tsukamoto
* Hayao Miyazaki
* John Woo
* Martin Scorsese
* Clint Eastwood
* Bong Joon-ho

==Release==
The film was shown at the 2011 Cannes Film Festival and the 24th Tokyo International Film Festival.  

==Reception==
Variety (magazine)|Variety praised the film, referring to it as "Meticulously crafted" and "engrossing". 

==Notes==
 

==External links==
*  

 
 
 
 


 
 