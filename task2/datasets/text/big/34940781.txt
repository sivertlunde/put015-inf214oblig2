Midsummer Madness (1921 film)
{{Infobox film
| name           = Midsummer Madness
| image          = Midsummer Madness (1921) - Nagel & Wilson.jpg
| image size     = 
| alt            = 
| caption        = Nagel and Wilson in a film scene
| director       = William C. deMille
| producer       = Adolph Zukor Jesse Lasky William C. deMille
| writer         = Olga Printzlau (scenario)
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Jack Holt Lois Wilson Lila Lee
| music          = 
| cinematography = L. Guy Wilky
| editing        = 
| studio         = 
| distributor    = Paramount Pictures Famous Players-Lasky Corporation
| released       =  
| runtime        = 60 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 silent drama film produced by Famous Players-Lasky and released by Paramount Pictures. It is based on the novel, His Friend and His Wife, by Cosmo Hamilton.
 Jack Holt, Lois Wilson and Lila Lee. It is the film debut of Ethel Wales.  A copy of Midsummer Madness is preserved at the Library of Congress.    

==Cast== Jack Holt - Bob Meredith
*Conrad Nagel - Julian Osborne Lois Wilson - Margaret Meredith
*Lila Lee - Daisy Osborne
*Betty Francisco - Mary Miller
*Claire McDowell - Mrs. Osborne
*Peaches Jackson - Peggy Meredith
*Ethel Wales - Mrs. Hicks
*Charles Ogle - Caretaker (*aka Charles Stanton Ogle)
*Lillian Leighton - Caretakers Wife
*George Kuwa - Japanese Servant

==References==
 

==External links==
 
*  
*  
* 

 

 
 
 
 
 
 
 


 
 