The Saltmen of Tibet
{{Infobox film
| name           = The Saltmen of Tibet
| image          = 
| caption        = 
| producer       = Christoph Bicker, Alfi Sinniger, Knut Winkler
| director       = Ulrike Koch
| based on       = 
| screenplay     = Ulrike Koch
| starring       = Margen, Pargen, Zopon
| music          = Frank Wulff and Stefan Wulff
| cinematography = Pio Corradi
| editing        =Magdolna Rokob
| studio         = 
| distributor    = 
| released       = 1997
| budget         = 
| gross          = 
| runtime        = 108 minutes
| language       = Tibetan, Secret Saltmen Language, German
| country        = Germany/Switzerland
}} Tibetan salt King Gesar of Ling, a traditional Tibetan epic. Stephen Holden,  , The New York Times, July 22, 1998, retrieved 26 April 2014 

The dialogue in the film is in Tibetan with English subtitles.

==Awards==
*Cineprix Swisscom Swiss audiences choice as best documentary of 1997
*Cariddi dOro (Best Film) 1997 Taormina Film Fest
*Sonje Award for Best Foreign Independent Film 1997 Pusan International Film Festival
*Golden Spire Award 1998 San Francisco International Film Festival
*Prix Nanook (Grand Prix) 1998 Dix-septième Bilan du Film Ethnographique, Paris

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 