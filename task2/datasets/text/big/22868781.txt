Panneer Pushpangal
{{Infobox film
| name           = Panneer Pushpangal
| image          = Panneer pushpangal.jpg
| caption        = Theatrical poster Vasu
| producer       = Padmini
| writer         = Santhana Bharathi K. Rajeshwar|K. Soma Sundareswaran (dialogues) Suresh  Archana  Shanthi Krishna  Jr.Manohar  Venniradai Moorthy  Prathap K. Pothan
| music          = Ilaiyaraaja
| cinematography = M. C. Sekar
| editing        = Vijayan
| studio         = Padmini Productions
| distributor    = Padmini Productions
| released       = 3 July 1981
| runtime        = 14 Reels
| country        = India Tamil
| budget         =
| gross          =
| website        =
}}
 Tamil film Vasu about a high school romance set in a hills station school located in Ooty. The movie carries an original plot and is not a remake of A Little Romance; as alleged earlier. 

==Plot==

Panneer Pushpangal is a love story between Suresh and Shantikrishna supported by their teacher Prathap Pothen.

==Cast==
 Suresh
*Archana Archana
*Shanthi Krishna
*Jr.Manohar
*Venniradai Moorthy
*Prathap K. Pothan

==Soundtrack==
{{Infobox album Name     = Panneer Pushpangal Type     = film Artist   = Illayaraja Cover    = Released =  Music    = Illayaraja Genre  Feature film soundtrack Length   = 18:58 Label    = Echo
}}

The soundtrack was composed by Ilaiyaraja while lyrics were written by Gangai Amaran  Ilaiyaraja later reused "Ananda Ragam" as "Saara Yeh Aalam" in Shiva (2006 film)|Shiva (2006). 

{|class="wikitable"
! Song !! Singer !! Duration
|---
| Aanandha Raagam...
| Deepan Chakravarthy, Uma Ramanan
| 4:26
|---
| Kodai Kaala Kaatre...
| Malaysia Vasudevan
| 5:11
|---
| Poonthalir Aada...
| S. P. Balasubrahmanyam, S. Janaki
| 4:45
|---
| Venkaaya Sambarum...
| Jyothi Raman
| 4:36
|---
|}

==Production==
During an impromptu discussion, K. Rajeswar had narrated the story of two convent students coming of age to Gangai Amaran. Moved by the tale, Gangai Amaran gave the directorial opportunity to directors P Vasu and Santhana Bharathi at the helm.  P. Vasu, son of make up man Peethambaram and Santhanabharathi, son of producer M. R. Santhanam, both assisted C. V. Sridhar earlier and the film marked their directorial debuts.  

A photographer friend of his dads opined that Suresh could make it as a lead actor and with a portfolio, Suresh approached director C. V. Sridhar to feature in his films, but his effort went in vain. He then met director duo Santhana Bharathi and P. Vasu who decided to cast him their venture, Panneer Pushpangal, while simultaneously he was cast in the lead role in Bharathirajas Alaigal Oivathillai presenting him with a dilemma of which film to choose.  He chose the former, but both films went on to becoming large commercial successes during the period.

==References==
 

==External links==
*  
 
 

 
 
 
 
 
 
 

 