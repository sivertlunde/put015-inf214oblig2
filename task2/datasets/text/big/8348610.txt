A sega nakade?
 
{{Infobox film|
| name           = A sega nakade
| image          = 
| caption        = 
| director       = Rangel Vulchanov
| producer       = Dimitar Gologanov
| writer         = Georgi Danailov
| starring       = Albena Stavreva
| music          = Kiril Donchev
| cinematography = Radoslav Spassov
| editing        = Yordanka Bachvarova
| distributor    = National Film Center Boyana Film
| released       = 17 October 1988
| runtime        = 93 minutes
| country        = Bulgaria Bulgarian
| budget         = 
| followed_by    = 
| amg_id         = 
| imdb_id        = 
}}
 corruption and immorality in important exams.

==Release and acclaim==
The film premiered on 17 October 1988 in Bulgaria. The film was produced by Boyana Film.

==Cast==
* Albena Stavreva as Momicheto Krasi s belega (The Scarred Girl Krasi)
* Ani Vulchanova as Svetla
* Antoaneta Stancheva as Nina
* Georgi Enchev as Montyorat (The Mechanic/Fitter)
* Georgi Staykov as Ivan (as Georgi Staikov)
* Genadi Nikolov as Zdravenyakat (The Tough Guy)
* Darina Georgieva as Manekenkata (The Female Model)
* Dimitar Goranov as Blediyat (The Pale Man)
* Elena Arsova as Toni
* Krasimira Miteva as Bremennata (The Pregnant)
* Konstantin Trendafilov as Buntaryat (The Rebel/Malcontent)
* Iliana Kitanova as Pauzata (The Pause)
* Mariana Milanova as Momicheto s okoto (The Boy With the Eye)
* Mihail Bilalov as Sofiyskoto kopele (The Sofia Bastard)
* Mihail Vitanov as Trompetistat (The Trumpeter)

==See also==
*List of Bulgarian films

==External links==
*  

 
 
 
 
 


 
 