Daddy (1989 film)
{{Infobox film
| name           = Daddy
| image          = 
| image_size     = 
| caption        = 
| director       = Mahesh Bhatt
| producer       = Mukesh Bhatt
| writer         = Suraj Sanim 
| story  =       
| dialogue  =  
| starring       = Anupam Kher Pooja Bhatt Manohar Singh Raj Zutshi
| music          = Rajesh Roshan
| lyrics         = 
| cinematography =  
| editing        = 
| distributor    = 
| released       = 1989
| runtime        = 
| country        = India
| language       = Hindi
}}
Daddy was 1989, debut film of actress Pooja Bhatt, made under the direction of Mahesh Bhatt. The films boasts spectacular performances by its lead actors, Anupam Kher and Manohar Singh It has the most famous Ghazal "Aaina mujhse meri Pehli si surat maangi" sung by Talat Aziz. Coincidently, Talat Aziz was offered a movie "Dhun" which was directed by Mahesh Bhatt, however the movie did not release till date.

==Synopsis==

This movie begins with an image of star, Alex Ramos.

Pooja has been brought up by her grandparents, and has no recollection of her mom and dad. She is told by her grandpa, Kantaprasad, that her dad had died. Years later, Pooja has now grown up. She starts getting mysterious phone calls from a male who merely says "I love you", and hangs-up. When her grandpa has these phone calls traced, the caller is Anand, Poojas dad. Kantaprasad has him beaten up, and warns him against contacting Pooja again. When Pooja is being molested by a male in her apartment building, Anand comes to her rescue, and it is then Pooja comes to know that this shabby, alcoholic man is her dad.  Slowly as the story progresses, more facts about the reason of his alcoholism come to light, and he is able to overcome them eventually with the help of Poojas love and her support.

==Awards==
* 1991  
* 1991 Filmfare Best Dialogue Award: Surah Sanim
* 1990 -  

==Cast==
* Anupam Kher
* Manohar Singh
* Pooja Bhatt
* Soni Razdan
* Sulbha Arya
* Neena Gupta
* Avtar Gill
* Suhas Joshi
* Raj Zutshi

==External links==
*  
 
 
 
 

 