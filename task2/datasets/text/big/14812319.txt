Good Dick
{{Infobox Film
| name           = Good Dick
| image          = Good dick.jpg
| caption        = Film poster
| director       = Marianna Palka
| producer       = Cora Olson Jennifer Dubin
| writer         = Marianna Palka Tom Arnold Mark Webber Martin Starr Eric Edelstein
| music          = 
| cinematography = Andre Lascaris
| editing        = Chris Kroll
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $39,924 
}}

Good Dick is a 2008 film directed by Marianna Palka.

==Plot== relationships was fueled by sexual abuse at the hands of her father. The film ends with a confrontation with her father, and ultimately Ritters character reuniting with her.
 Tom Arnold has a short but significant role as "The Dick". Jason Ritters brother Tyler Ritter has a brief appearance, Nancy Morgan also makes an appearance as a waitress. Bryce Dallas Howard and husband Seth Gabel also make cameo appearances.

==Festivals and competitions==
The film competed among others in the Dramatic Competition at the 2008 Sundance Film Festival.   

==Reception==
The New York Times said the film "surmounts its indie-movie quirkiness with exceptional acting and a sincere belief in the salvation of its wounded characters."   

==References==
 

==External links==
*  
*   
*  
*  
*  

 
 
 
 
 
 


 