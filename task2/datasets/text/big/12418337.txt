Aarilirunthu Arubathu Varai
{{Infobox film
| name = Aarilirunthu Arubathu Varai
| image =Aarilirunthu-Arubathu-Varai-1979.jpg
| caption =
| director = SP. Muthuraman
| writer = Cho Sangeetha Jayalakshmi
| producer = Panju Arunachalam
| music = Ilayaraja
| editor =
| released =  
| runtime = 150 minutes
| country = India
| language = Tamil
| budget =
}}
 Thengai Seenivasan, T.K.Bakavathi, Phataphat Jayalaxmi|Jayalakshmi, Sangeetha, Jaya and Mallika. 

The music was composed by Ilayaraja. The songs were sung by S. P. Balasubramaniam, S. P. Sailaja, Jayachandran and S. Janaki. The lyrics were penned by Panchu Arunachalam. 

The movie is a portrayal of the life of Santhanam (Rajinikanth), the eldest son of a poor widow who struggles to make the ends meet. He spends the first half of his life toiling day and night to provide his younger siblings an education & spends second half watching them leading comfortable lives while he continues his struggle for survival.

The film was screened for 25 weeks in the Midland theatre,  .

==Plot==
The story begins with a young boy, the eldest son of a poor widow barely able to make ends meet, doing odd jobs along with working at a printing press to provide income to his family and to provide education to his 2 younger brothers and younger sister, sacrificing his own education. He grows up to become Santhanam (Rajinikanth).

Due to his extreme poverty, his lover in the office (Sangeetha) leaves him for a richer man and he ends up marrying Lakshmi (Phataphat Jayalaxmi|Jayalakshmi), a poor girl. Being extremely poor, they could not even feed their children, let alone live comfortably. He keeps getting money from his friend (Cho Ramaswamy) for almost everything. His younger brothers, having studied and become rich, refuse to help him. His younger sister (Jaya), for whom Santhanam had spent all his money in order to conduct her marriage, treats him very badly whenever he goes to visit her. He is even fired from his job by his old employers son who took over the press when the former becomes paralyzed after an accident.

Eventually, Santhanam leaves his house as he does not have money to pay the rent even after selling everything they had and he and his family move to a slum. Here he begins to write stories for a magazine in order to earn money. His wife dies due to a fire in the slum. Slowly he becomes rich as his stories were very popular.Santhanam becomes a famous author, his children are being sent abroad for higher education and his younger siblings begins to treat him with respect shortly before Santhanam dies at end.

==Production==
The success of Rajinikanths previous movie, Bhuvana Oru Kelvikkuri encouraged the director, SP. Muthuraman, to cast Rajinikanth in a melodramatic, sacrificial role in Aarilirunthu Arubathu Varai. The film was originally offered to Sivakumar who refused the film.  While making the film, Rajinikanth had misgivings whether the audience would accept him in such a melodrama since he had been an entertainer till then. However, S. P. Muthuraman convinced him and showed him 5000 feet of edited version, he was highly impressed, and then showed great involvement in its making.  http://www.cinemalead.com/visitor-column-id-the-journey-of-living-legend-rajinikanth-part-2-rajinikanth-20-08-133154.htm 

==Box Office==
The film was screened for 25 weeks in the Midland theatre, Chennai, the film ran for 175 days.  However, the box-office success of the film made him popular among women audiences and is considered a turning point in his career. 

==Sound Track==
* Kanmaniyae Kadhal Enbathu
* Vazhkaiye Vesham
* Aan Pillai Endralum

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 