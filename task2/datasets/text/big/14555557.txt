Crash Goes the Hash
{{Infobox Film |
  | name           = Crash Goes the Hash |
  | image          = Crashhash44.jpg|
  | caption        = |
  | director       = Jules White Felix Adler|
  | starring       = Moe Howard Larry Fine Curly Howard Bud Jamison Dick Curtis Symona Boniface Vernon Dent Victor Travers|
  | cinematography = George Meehan | 
  | editing        = Charles Hochberg |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       = February 4, 1944 (United States|U.S.) |
  | runtime        = 17 36"|
  | country        = United States
  | language       = English
}}

Crash Goes the Hash is the 77th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
Fuller Bull (Vernon Dent), the head of the ailing Daily News, confronts the reporters he hired for not getting him a story to keep up with a competing newspaper called the Daily Star Press. Fuller Bull catches three shirtmen (the Stooges) outside thinking they are reporters from the Daily Star Press and immediately hires them to get a picture of visiting Prince Shaam of Ubeedarn (Dick Curtis). Word has it that Shaam has plans to marry local wealthy socialite Mrs. Van Bustle (Symona Boniface). The trio disguise themselves as servants, and work their way into a party being held at Mrs. Van Bustles home in the honor of the prince.

The Stooges all but sabotage the festivity by serving hors dœuvres consisting of peas and dog biscuits, and a turkey stuffed with a live parrot. The prince leaves in disgust, with the butler (Bud Jamison) following close behind. Undaunted, the Stooges manage to expose both the prince and butler as crooks who were planning to rob the house.

The next day, the Stooges tells Fuller Bull that a man claiming to be Prince Shaam is not a prince and they had both him and the butler arrested. As a result of their findings, he is excited and tells the people to stop the presses for an extra. He gives the boys a large bonus and Mrs. Van Bustle thanks the boys for preventing her from marrying Shaam.

==Curly Howard fades==
The Stooges made many public appearances during the height of World War II in support of the war effort. The demands of the heavy touring took its toll on Curly in particular, whose timing and energy began to deteriorate. In Crash Goes the Hash, Curlys speech is slightly slower and his falsetto had begun to lose its crisp high pitch. The dialogue spoken at the lemonade table where he covertly tells Larry to take a picture of the Prince features Curly talking in his normal speaking voice, which is deeper than Moes or Larrys. 

==Production notes== Type 2 Christian Scientist, Jamison refused to take insulin when the symptoms of his diabetes acted up. As a result, he went into diabetic shock and died at age 50.   
 Jeepers Creepers, Whered You Get Those Peepers?") and the parrots "What a night! from the Stooges 1936 entry Disorder in the Court. 
 Felix Adler, Tom Kennedy, which was written by Al Giebler, Elwood Ullman and Searle Kramer. 

==Quotes==
**Larry: "Ive been running my legs off all morning til the cuffs on my pants are frayed."
**Moe: " Fraid of what?!"

**Curly: "What good is a $100 bogus?"
**Larry: "Not bogus. Bonus! Dont you know what bonus is?"
**Curly: "Soiteny, Spanish! Bonus Naches! Si, si, senor!"

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 