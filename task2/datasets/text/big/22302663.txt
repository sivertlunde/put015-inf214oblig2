Tales of Vesperia: The First Strike
 
{{Infobox film
| name                 = Tales of Vesperia: The First Strike
| image                = ToV_FS_Poster.jpg
| caption              = Theatrical release poster
| director             = Kanta Kamei
| producer             = {{plainlist|
* Makoto Yoshidumi
* Atsushi Yukawa
* Hidekazu Terakawa
}}
| screenplay           = Reiko Yoshida
| story                = Namco Bandai Games
| starring             = {{plainlist|
* Kōsuke Toriumi
* Mamoru Miyano
* Mai Nakahara
* Rika Morinaga
* Eiji Takemoto
}}
| music                = Akira Senju
| cinematography       = Kazuhiro Yamada
| editing              = Junichi Uematsu
| production companies = {{plainlist|
* Namco Bandai Games
* Bandai Visual
* Production I.G
}}
| distributor          = Kadokawa Pictures
| released             =  
| runtime              = 110 minutes
| country              = Japan
| language             = Japanese
| budget               = 
| gross                = United States dollar|$941,610
}}
 Flynn Scifo, from the Niren Corps as they protect a town from monsters with their allies.

The project was led by Kenta Kamei who previously worked on the animated cutscenes from the Tales games. He expressed pressure due to First Strike being his debut as a director and also elaborated on how difficult was creating the storyboard. Yoshida was given the role of screenwriter in order to appeal to the female fanbase the series has always had. It was released in Japan on October 3, 2009, as a result of delays in production. Kadokawa Pictures distributed the film. It was released in North America on June 26, 2012, by Funimation Entertainment and in the United Kingdom on December 3, 2012, on DVD and Blu-ray by U.K. Anime Industry.

The film grossed a total of US$941,610 in Japan. Its home media release had positive sales in Japan, with the Blu-ray being one of the bestselling from the year it was released. Critical reception of the film has been generally positive for its accessibility to the non-gamers of Tales of Vesperia as well as the appeal of the main cast and the production value. Nevertheless, it has also attracted criticism for its slow pace.

==Plot== Flynn Scifo, two knights who have just joined the Niren Corps, together with two of their senior knights and fellow Corps members, Hisca and Chastel, are sent on a mission to investigate an abnormal gushing of aer in the area.

After their first assignment to help in destroying a monster infestation near the town, Yuri, Flynn, Hisca, and Chastel meet the leader of the local guild, Melzome Kaid. Niren, the captain of the knights stationed in Shizontania, was once dedicated to following the letter of the rules and regulation of the knights. However, he attributes the death of his wife and daughter to this trait, saying that if he had defied orders, he may have been able to protect them. The knights efforts to repel the monsters around the area prove to be in vain. Despite being called by Commandant Alexei to attend a formal gathering at the imperial capital of Zaphias, Niren sends Flynn in his stead to attend the gathering and to request aid in defending the town. Niren and Chastel leave in search of a "strange blastia expert" named Rita Mordio. Yuri and Hisca are left to care for Lambert, Nirens personal hound, and Repede, Lamberts son. Niren succeeds in finding Rita Mordio and obtaining a device with which to return the aer to normal. Flynn is admonished by Commandant Alexei, and his request for reinforcements to hold Shizontonia is denied until the end of the gathering. Yuri and Hisca assist in defending the town against a strange monster made out of aer, and while they successfully repel the invasion, the monster takes control of Lambert along with two other wardogs. As Lambert rages out of control, Yuri is forced to kill him to protect Hisca.

When Flynn returns from Zaphias, he and Yuri come to blows, as each believes the other had an effortless assignment. Meanwhile, a platoon of high-ranking knights from central are seen in the forest being slaughtered by monsters. One survives and flees to Shizontonias front gate, where he delivers a report to an unseen individual in the city. Niren, upon receiving the news that reinforcements would not be arriving anytime soon, decides to take the knights to investigate the source of the aer, which he believes to be a fortress at the center of a nearby lake. Flynn disagrees with Nirens decision, viewing it as disobedience of a direct order.

Garista, the strategist for the Niren Corps, attempts to dissuade Niren but to no avail. The investigation in the fortress turns out to be more dangerous as the aer begins to turn the fortress into a living weapon to attack the knights. The knights make their way into the center of the fortress, where a giant blastia is found to be using the aer. Niren uses the device given by Rita to stifle the gushing of aer, but a backlash is caused by the sudden ceasing of aer flow, and the blastia explodes, destroying the barrier blastia that keeps Shizontonia safe, knocking out Chastel, and causing the fortress to begin to crumble. As Niren attempts to carry Chastel to safety, the floor beneath him crumbles. Niren is able to throw Chastel to Yuri and is stranded in a sinkhole of gushing aer. Despite Yuris attempts to save him, due to a wound he received during the investigation, Niren is unable to pull himself to safety. Leaving his blastia with Yuri, Niren has the knights leave without him.

The knights and the entire town mourn over Nirens death. Flynn realizes that the blastia used in the fortress where the aer abnormality originated, along with the piece he spotted on the floor, were a specialized type used only by Garista. Yuri and Flynn confront Garista with this information, and Garista reveals that he was attempting to create a new kind of blastia. Yuri and Flynn attack and kill Garista. In the aftermath, Shizontonia is evacuated due to the failure of the barrier blastia. Yuri leaves the knights, taking Nirens blastia as a keepsake and bringing Repede along with him, saying his goodbyes to everyone in the town. Flynn sees him off at the gates, and the two share a fond farewell, swearing to meet again one day.

==Characters==
 

*Yuri Lowell
:He is one of the main protagonists of the story. Yuri was born and raised in the Lower Quarter of the Imperial Capital, Zaphias. He joined the Imperial Knights because like Flynn, he wanted to change the imperial law and justice. He was voiced by Kosuke Toriumi in Japanese and Troy Baker in English.       Flynn Scifo
: He is also one of the main protagonists of the story. Flynn is a childhood friend of Yuris, as well as a friend to Estelle. He joined the Imperial Knights in order to uphold law and justice. The two were once childhood friends, brought up in the lower parts of the Empire. He was voiced by Mamoru Miyano in Japanese and Sam Riegel in English.  
*Niren Fedrok
:He is the commanding officer of the Niren Corps. Although he has the aura of a commanding officer, he also exudes that of a friendly middle-aged man. Both Yuri and Flynn are under his command. Niren was voiced by Takashi Taniguchi in Japanese and Christopher Sabat in English.  
*Hisca Aiheap
:She is a female knight who belongs to the Niren Corps, together with her twin sister, Chastel. She is a knight senior to Yuri and Flynn and is the little sister of the twins. She was earlier seen in the first few character arts released for the movie. She was voiced by Arisa Ogasawara in Japanese and Trina Nishimura in English.  
*Chastel Aiheap
:She is also a female knight like her twin sister, Hisca, in the Niren Corps. She was voiced by Fumie Mizusawa in Japanese and Leah Clark in English.  
*Garista Luodur
:He is the strategist of the Niren Corps; he often showcases his wisdom in military affairs and especially excels those of others. He is later revealed to be the movies main antagonist. He was voiced by Mitsuru Miyamoto in Japanese and J. Michael Tatum in English.  
*Yurgis
:He is the vice captain of the Niren Corps, second in command to Niren.
*Lambert
:He is the official wardog of the Niren Corps and is also Repedes sire. Repede
:He is a puppy who acts as the mascot for the Niren Corps and is always carrying something in his mouth.   

==Development==
Tales of Vesperia: The First Strike was officially announced in early 2009 with the staff confirming the return of Kousuke Toriumi and Mamoru Miyano as the protagonists.  Production of the movie started when Tales of Vesperia was still in plot stage. The team discarded the idea of making an adaptation of the game due to its large length. The team was also against making a sequel or a sidestory due to the required time-consuming character introductions that would make it required to play the game to enjoy.  Director Kenta Kamei wanted a movie that anybody would enjoy. As a result, it was decided to make it a prequel as they could explore Yuri and Flynns background. Kamei was originally involved in the movie parts of the Tales series. When he was assigned as the movies director, he recalls feeling "honoured, surprised and a bit nervous at the same time".   

The First Strike was Kameis first movie, and he wanted to rely on an experienced screenwriter. He eventually requested Reiko Yoshidas help as the Tales series had a strong female fanbase and thus needed "feminine sensitiveness" in designing the script.  The film uses 2D and computer animation|computer-generated animation. The director was  demanding with the 3DCG director, and he relied on a very skilled team. He started from the director of photography, Kazuhiro Yamada, who he personally insisted to join the production. Kamei elaborated on how blending hand-drawn animation and CGI was one of the most difficult and time-consuming tasks.  As a result, especially in the opening scene, the film had hand-drawn characters running on a 3D background. It took a while before he could get the result he was aiming for. The plot took two years and a half to produce. Originally, there were plans to release the movie before the game, but a delay in the schedule resulted in Vesperia being released first. Kamei believes that this was due to how demanding he was.   

Much of the plot was affected by Yuri and Flynns youth. Niren Fedrok was created to serve as a mentor figure as well as the possible future of the twos careers. Since Yuri was surer of his interest, the coming-of-age story was more focused on Flynn.  This is meant to act as a counterpart to the video game, where not much of Flynns past and his weaknesses are exposed. The friendship between Yuri and Flynn was seen as "priceless" by Kamei due to the mutual respect they share as well as the occasion each gets angry.   

The storyboard took six months to complete. Kamei struggled with several difficulties as he was under pressure due to how it would influence the entire film. The climax of the film also proved difficult to make.  He also noted how the first scene had the challenge of attracting the audiences attention. Kameis favorite scene is when Yuri looks up at the sky of Shizontania the night before the knights expedition to the old ruins. It meant to emphasize the importance of Yuris decision in regards to his future. 

===Music===
{{Infobox album
| Name     = Tales of Vesperia The First Strike Original Soundtrack 
| Type     = soundtrack
| Artist   = Akira Senju
| Cover    = 
| Released =  
| Length   = 53:17  Lantis
| Producer = 
}} Lantis on October 21, 2009. 
{{track listing
| collapsed = yes
| headline = Tales of Vesperia The First Strike Original Soundtrack 
| all_music = 
| lyrics_credits = yes
| total_length = 53:17 
| title1 = 序曲～魔力と魔物～ / 千住明
| length1 = 1:22
| title2 = First Operations / 千住明
| length2 = 2:51
| title3 = 魔導器 / 千住明
| length3 = 1:51
| title4 = テイルズ オブ ヴェスペリア～The First Strike～ / 千住明
| length4 = 2:09
| title5 = シゾンタニアの騎士団 / 千住明
| length5 =2:24
| title6 = 一日を終えて / 千住明
| length6 = 0:50
| title7 =  
| length7 = 1:32
| title8 = Bridge ～Bonus Piece～ / 千住明
| length8 = 0:22
| title9 = 父の追憶 / 千住明
| length9 = 0:41
| title10 = 赤いエアルの謎 / 千住明
| length10 = 2:02
| title11 = 帝都との軋轢 / 千住明
| length11 = 2:53
| title12 = 予期せぬ襲撃 / 千住明
| length12 = 2:53
| title13 = 魔物への変貌 / 千住明
| length13 = 1:49
| title14 = 見えない作為 / 千住明
| length14 = 1:32
| title15 = 騎士の葛藤 / 千住明
| length15=3:07
| title16 =出動へ / 千住明
| length16=2:45
| title17 =突入のとき / 千住明
| length17=2:32
| title18 =異常現象 / 千住明
| length18=2:35
| title19 =Golem Fight / 千住明
| length19=1:32
| title20 =大きな陰謀 / 千住明
| length20=0:44
| title21 =ナイレン / 千住明
| length21=2:58
| title22 =悲しみを迎えて / 千住明
| length22=0:47
| title23 =勇者との別れ / 千住明
| length23=3:18
| title24 =最後の敬礼 / 千住明
| length24=0:34
| title25 =黒幕との決別 / 千住明
| length25=2:08
| title26 =それぞれの道、一つの友情 / 千住明
| length26=2:15
| title27 =テイルズ オブ ヴェスペリア～The First Strike～ (エンディング) / 千住明
| length27=2:51
}}
 

==Release==
On October 3, 2009, Tales of Vesperia: The First Strike debuted in Japans box office in seventh place with US$230,917.   It grossed a total of US$941,610 in Japan and US$4,351 in Taiwan for a total of US$945,961.  It was released on home media on May 28, 2010, in Japan. It was released on three different formats: DVD, Blu-ray Disc, and Universal Media Disc.  In its first week of release, the DVD sold 14,314 units, while the Blu-ray ranked second in the charts.   In 2010, the Blu-ray sold a total of 36,059 units. 

Funimation Entertainment announced in April 2011 that they would release the movie in 2012 on Blu-ray and DVD in North America. The movie was released on June 26, 2012.    In June 2011, Madman Entertainment announced they also licensed the film for an Australian release.  In the United Kingdom, the film was released in December 2012 by U.K. Anime Industry.  The film debuted in the sixth International Animation Film Festival of Cordoba feature film competition section. 

===Critical reaction=== EGMNow gave it a 9 out of 10 praising its accessibility and the work from the English voice cast. He said "The story is good and absorbing with only a few times when it feels like it’s tripping up" criticizing some aspects that felt poorly translated from the video game.   

The film also attracted criticism. UK Anime Networks Andy Hanley stated "It has potential in its characters and concept, but does very little with them over the course of this film to make for a disappointly mediocre movie."    Similar to Silverman, Jeffrey Kauffman from Blu-ray.com noted that the film had a slow pace in developing Yuri and Flynns relationship. He gave high praise to the animation used in the story. He criticized its supporting cast as he believes "Tales of Vesperia: The First Strike is a standalone offering, or at least one that might have done better had it focused solely on Yuri and Flynn and left the rest of the cast of thousands far in the background."    Gay was saddened by the lack of appearances from the side characters such as Siren, Hisca, and Chastel. 

The production value has been met with positive critical response. Silverman praised the films animation, stating that "the movie is gorgeous, and not just for theatrical animation." Silverman also praised the cast, most notably Yuris voice actors.  EGMnow also gave high praise to the animation calling it "beautiful and rich" and said "It is top-of-the-line in artwork, artistic style and coloring. The action sequences, though reminiscent of the game, are not repetitive nor silly. This is a very serious movie and it was treated as such."  The Fandom Post agreed saying "the feature has some absolutely stunning visuals which create lush and vibrant environments for the characters to live in which adds a wonderful polish to the work."  Similar praise was given by Jeffrey Kauffman from Blu-ray.com.  Andy Hanley praised the distinctive character designs despite criticizing some aspects from the fight scenes. 

==References==
 

==External links==
*   
* 

 
 

 
 
 
 
 
 
 
 
 