Dear Jack
{{Infobox film
 | name           = Dear Jack
 | image          = Dear_Jack_movie_poster.jpg
 | director       = Joshua Morrisroe and Corey Moss
 | producer       = Holly Adams
 | starring       = Andrew McMahon
 | narrator = Tommy Lee
 | music          = 
 | cinematography = Joshua Morrisroe
 | editing        = Joshua Morrisroe
 | distributor    = Sire Records
 | released       = October 8, 2009
 | runtime        = 67 min.
 | country        = United States
 | language       = English
 | budget         = $50,000
}}
Dear Jack is a 2009 American documentary film starring Andrew McMahon,  the vocalist, pianist and primary songwriter for the bands Something Corporate and Jacks Mannequin.  The documentary chronicles McMahon on a rollercoaster year, through the highs of recording and releasing a solo album and the lows of being diagnosed with leukemia and breaking up with his girlfriend. 

==Synopsis==
On May 27, 2005, McMahon was forced to cancel all of his upcoming concerts   after a medical examination in connection with a relentless case of laryngitis forced him into being admitted into a hospital in New York City. On June 1, 2005, he was diagnosed with acute lymphoblastic leukemia, the same day he finished recording his debut album under the Jacks Mannequin moniker, Everything in Transit.  Since the illness was diagnosed in its early stages, McMahons doctors had high hopes for a full recovery. 

Using a handheld video camera that his record label gave to him initially to document the process of recording his album, McMahon recorded everything from inside his hospital room and onward, from spinal taps to radiation and commentary on his deteriorating physical and mental state. The film follows him from diagnoses to recovery, including the stem cell transplant that saved his life and the first show he performed after being well again. 

==References==
 

 
 

 
 
 
 
 
 
 

 