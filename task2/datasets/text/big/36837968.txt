Unconditional (film)
{{Infobox film
| name           = Unconditional
| image          = Unconditional Press Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Brent McCorkle
| producer       = Jason Atkins and J. Wesley Legg
| writer         = Brent McCorkle
| starring       = Lynn Collins Michael Ealy Bruce McGill Kwesi Boakye Diego Klattenhoff Cedric Pendleton Joanne Morgan Danielle Lewis Gabriella Phillips
| music          = Mark Petrie Brent McCorkle
| cinematography = Michael Regalbuto
| editing        = Brent McCorkle
| studio         = Harbinger Media Partners Veracity Moving Pictures Free To Love
| distributor    = Harbinger Media Partners
| released       =  
| country        = United States
| language       = English
| budget         = $700,000
| gross          = $1,005,800 
}}
Unconditional is a 2012  biography drama film written and directed by Brent McCorkle, inspired by true events.  It is the first film by Harbinger Media Partners, which aims to "produce high quality theatrical films that honor God and inspire viewers to pursue him and serve others."   The producers of the movie have partnered with a number of charitable and non-profit organizations to encourage moviegoers to meet the needs of others in their communities. 

The film is based on the actual story of Joe Bradford, who grew up in a rural area of Tennessee.  When he developed kidney disease, Joe and his wife Denise were forced to move to a low-income area of Nashville.  When they arrived, they were confronted by the needs of the underprivileged children in their neighborhood.  Joe and Denise began to reach out to them and also started directing a choir of inner-city children.  Many of the fatherless children embraced Joe, who became known as "Papa Joe."  Together with his wife, he founded Elijahs Heart, a non-profit organization, in 2005 to help children in need. 

The film is the first feature-length project directed by Brent McCorkle, who also wrote the screenplay and edited the film.  He previously worked on several short films, including The Rift, which won an award in the 2009 Doorpost Film Project. 

== Plot ==
Samantha Crawford lives a dream life. She is happily married on a ranch where she keeps her beloved horse, and the stories shes told and illustrated since childhood have become published books.

When her husband Billy is tragically killed, Sam loses her faith and will to live. A death-defying encounter with two children leads to a reunion with Joe, her oldest friend. As Sam watches "Papa" Joe care for and love the kids in his under-resourced neighborhood, she begins to believe that the love of God is always reaching out to her.

== Cast ==
*Lynn Collins as Samantha Crawford
*Michael Ealy as "Papa Joe" Bradford
*Bruce McGill as Detective Miller
*Kwesi Boakye as Macon
*Montrel Miller as Grady

== Release ==
The film was released to theaters on September 21, 2012. Released on DVD March 5, 2013.

=== Critical reception === John Carter) as Sam and Michael Ealy (Think Like a Man) as Joe, it has an elegant script and striking visuals."  Gary Goldstein of the Los Angeles Times was not as favorable in his review, saying that the film included too many cliches and that director Brent McCorkle needed to have "a bit more, er, faith in his audience." 

The film has received an outstanding reception by Christian leaders and film critics. Russ Breimeier of Christianity Today said, "Unconditional sets itself apart with quality filmmaking and redemptive storytelling delivered with authenticity."  Ted Baehr of Movieguide called the film "wholesome" and "redemptive", praising the film as "a beautifully made movie with a captivating, inspiring story." 

== References ==
 

== External links ==
*  , the non-profit organization started by the real Joe Bradford
*  
*  

 
 
 
 