Deadly Intruder
{{Infobox Film
| name           = Death Screams aka The Deadly Intruder
| image          =
| caption        = Someone out there is watching you... Dont unlock your door!
| director       = John McCauley
| producer       = Bruce R. Cook(as Bruce Cook)/John Walton (executive producer )
| writer         = Tony Crupi   Daniel Greene
| music          = John McCauley
| cinematography = Tom Jewett(as Thomas Jewett) 
| editing        = 
| distributor    = Channel One Productions/Thorn EMI/Home Box Office Home Video (HBO) 1985
| runtime        = 86 min
| country        = USA English
}}
Deadly Intruder (aka The Deadly Intruder) is a 1985 horror film.

==Synopsis==
In a place called...Midvale, a sanitarium becomes minus one patient, when one of those interred breaks out of said loony bin after dispatching security specialists there and heads off to looking for the love of his life. Our disturbed individual is obsessively jealous, slaying anyone whom he fears may be endangering his relationship with his current girlfriend. Can the police catch him before he catches up to her?

==Cast==
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Molly Cheek
| Jessie
|-
| David Schroeder
| Grotowski 
|-
| Danny Bonaduce
| John
|-
| Stuart Whitman
| Captain Pritchett
|- Daniel Greene
| Danny
|-
| Santos Morales
| Carlos
|-
| Steve Perry
| Wayne
|-
| Chris Holder
| Ben 
|-
| Laura Melton
| Amy
|-
| Marcy Hansen
| Cathy
|-
| Curt Bryant
| Jim
|-
| Stephen Paul
| Rogers
|-
| Todd Martin
| Mr. West 
|-
| Tommy Ramone (as Tommy Reamon)
| Foreman
|-
| W.T. Zacha
| Mechanic
|-
| Tony Crupi
| Drifter
|-
| Kay St. Germain Wells
| Neighbor
|-
| Suzanne Benoit
| Salesgirl
|-
| Gerry Landrum
| Lineman
|}

==Release==
The film was released in Finland as Murhaaja saapuu öisin and in West Germany as Der Tödliche Feind.

==See also==
*Horror-of-personality
*Exploitation film Splatter
*Slasher films

==External links==
* 

 
 
 


 