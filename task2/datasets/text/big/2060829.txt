One Man Band (film)
{{Infobox film
| name           = One Man Band
| image          = One Man Band poster.jpg
| image_size     = 
| caption        = Original Poster Mark Andrews
| producer       = Osnat Shurer
| writer         = Andrew Jimenez Mark Andrews
| music          = Michael Giacchino
| editing        = Steve Bloom
| studio         = Pixar Animation Studios Walt Disney Pictures Buena Vista Pictures
| released       =  
| runtime        = 4 minutes, 33 seconds
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
One Man Band is a 2005 Pixar computer animated short musical comedy film. The film made its world premiere at the 29th Annecy International Animated Film Festival in Annecy, France,  and won the Platinum Grand Prize at the Future Film Festival in Bologna, Italy.  It was shown with the theatrical release of Cars (film)|Cars.

  Mark Andrews and produced by Osnat Shurer, head of Pixars Shorts group. The score for the short was written by Michael Giacchino, who also composed the scores for Pixars animated feature films The Incredibles, Ratatouille (film)|Ratatouille, Up (2009 film)|Up and Cars 2.

Like many Pixar shorts, the film is completely free of dialogue, instead using music (played by the characters) and pantomime to tell the story.
 Animation Show of Shows in 2005.

== Plot == street performer, plays a routine tune in a deserted Italian village square in the afternoon, waiting for a pedestrian to tip him in his rusty iron cup. Soon, he spots Tippy, a humble peasant girl clutching a big gold coin, with the intention of dropping it in the piazza fountain to make a wish. Bass, seizing the opportunity, immediately plays an impromptu piece, capturing the young girls attention. Just when Tippy is about to drop the coin into Basss cup, a newcomer steps onto the scene. Treble, a suave and flamboyant street performer, plays a more attractive song, effectively stealing Tippys attention, much to the anger of Bass. Not to be outdone, Bass ups his ante, with Treble daring to take it even further. As the two rivals unleash their arsenal of musical weapons, trying to vie for the attention (or rather, the tip) of Tippy, the girl cowers in their wild musical cacophony, and in the process, drops her sole gold coin, which falls down a drain and gets lost in the sewers of the village.

Heartbroken, Tippy sheds a single tear, but then angrily demands from Treble and Bass a replacement coin for the one they made her lose. When the two musicians come up empty-handed, Tippy insists she take one of Trebles violins and Basss iron cup in an attempt to get her money back by playing solo. She then tunes the violin and begins to play it like a true virtuoso, prompting a passing pedestrian to drop a large bag of gold coins into her cup. Elated, Tippy hugs the bag and approaches the fountain, but not before she pulls two coins out of her bag and tempts Treble and Bass. But as they eagerly reach out to grab them, she tosses the coins into the top of the fountain, out of reach, to their dismay. A post-credits scene shows that it is now nighttime, with Treble standing on Bass, trying to reach for the coins in vain. As the two start to fall backwards, the film ends.

== Music ==
The violinists featured in the score for the film are:
* Clayton Haslop ("Treble")
* Mark Robertson ("Tippy")

The score was recorded at the Paramount Scoring Stage in Hollywood, CA. The filmmakers used a 38-piece orchestra as well as several soloists, including the ones listed above. 

The music during the credits is Pablo de Sarasates Zigeunerweisen.

==Release==
The film premiered on June 11, 2005, at the Annecy International Animated Film Festival in Annecy, France.  It was shown with the theatrical release of Cars (film)|Cars,  which was released in the United States on June 9, 2006.

== DVD release ==
Pixar included the film on the DVD release of Cars in 2006 and as part of Pixar Short Films Collection, Volume 1 in 2007.

==References==
 

== External links ==
*  
*  
*  

 
{{succession box
 | before = Jack-Jack Attack short films
 | years  = 2005
 | after  = Mater and the Ghostlight}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 