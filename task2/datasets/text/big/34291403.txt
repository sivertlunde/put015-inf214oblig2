The Llano Kid
 
{{Infobox film
| name           = The Llano Kid
| image          =
| caption        =
| director       = Edward Venturini
| producer       =
| writer         = original story, O. Henry screenplay Wanda Tuchock
| starring       = Tito Guízar Gale Sondergaard Alan Mowbray Jan Clayton
| music          = Victor Young
| cinematography =
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
}}
 Western film The Texan.  The Llano Kid was directed by Edward Venturini.  Others in the film include Gale Sondergaard as Lora Travers, Alan Mowbray as John Travers, and Jan Clayton as Lupita Sandoval.

==Plot==
The Llano Kid is a romantic highwayman who robs stagecoaches along the Texas-Mexico border.  In the film, John and Lora Travers are searching for Enrique Ibarra to inform him that he is the heir to a substantial fortune and ranch/hacienda south of the border.  If they find him, they will receive a significant reward.  During their search for Ibarra, their stage falls victim to the Llano Kid.  Noticing that the Llano Kid bear an uncanny resemblance to Ibarra, the Travers conspire to persuade the Kid to pose as Ibarra so they can collect their reward.

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 