The Black Sleep
 
{{Infobox film
| name           = The Black Sleep
| image          =  Blacksleepposter.jpg
| image_size     =
| caption        =
| director       = Reginald Le Borg
| producer       = Howard W. Koch executive Aubrey Schenck
| writer         = John C. Higgins
| based on       = story by Gerald Drayson Adams
| narrator       = Basil Rathbone
| starring       = Basil Rathbone Akim Tamiroff Lon Chaney, Jr. John Carradine Bela Lugosi Herbert Rudley Tor Johnson
| music          = Les Baxter
| cinematography = Gordon Avil
| editing        = John Schreyer
| studio         = Bel-Air Productions (Prospect Productions)
| distributor    = United Artists
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $225,000 Tom Weaver, Interviews with B Science Fiction and Horror Movie Makers: Writers, Producers, Directors, Actors, Moguls and Makeup McFarland, 1 Jan 2006 p 211 
| gross          =
}}
 American black-and-white producers Aubrey Schenck and Howard W. Koch, who had a four-picture finance-for-distribution arrangement with United Artists. The film was re-released in 1962 as Dr. Cadmans Secret. 
 directed by Lon Chaney, John Carradine, and Akim Tamiroff in a role originally written for Peter Lorre.  In a "prominent" supporting role was Ed Wood regular Tor Johnson.  

==Plot==
 
Set in England in 1872, the story concerned a prominent, knighted surgeon whose wife has fallen into a coma caused by a deep-seated brain tumor. Due to medicines state of the art at the time, he does not know how to reach the tumor without risking brain damage or death to the woman he loves, so he undertakes to secretly experiment on the brains of living, but involuntary, human subjects who are under the influence of a powerful Indian anesthetic, Nind Andhera, which he calls the "Black Sleep". Once he has finished his experiment, surviving subjects are revived and placed, in seriously degenerated and mutilated states, in a hidden cellar in the gloomy, abandoned country abbey where he conducts his experiments.

==Cast==
*Basil Rathbone as Sir Joel Cadman 
*Akim Tamiroff as Udu the Gypsy
*Lon Chaney Jr. as Dr. Monroe aka Mungo
*John Carradine as "Bohemund"
*Bela Lugosi as Casimir
*Herbert Rudley as Dr. Gordon Ramsay  Patricia Blake as Laurie Munroe
*Phyllis Stanley as Daphnae 
*Tor Johnson as Curry
*George Sawaya as Sailor Subject 
*Sally Yarnell as Female Subject  Peter Gordon as Det. Sgt. Steele 
*Claire Carleton as Carmoda Daily John Sheffield as Det. Redford 
*Clive Morgan as Roundsman Blevins
*Louanna Gardner as Angelina Cadman 
*Aubrey Schenck as Prison Coroners Aide (uncredited)

==Release== House of Frankenstein and House of Dracula, only relying on a completely new cadre of human monsters.  
 The Creeping Unknown.

==References==
 

==External links==
* 
*  
*  at Basilrathbone.net
*  at TCMDB

 

 
 
 
 
 
 
 
 