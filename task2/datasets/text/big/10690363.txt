Caught in a Cabaret
 
{{Infobox film
| name = Caught in a Cabaret
| image = Caught in a Cabaret (poster).jpg
| image size =
| caption = Caught in a Cabaret in a French re-issue as Charlot Garçon de Bar
| director = Mabel Normand
| producer = Mack Sennett
| writer = Mabel Normand
| starring = Mabel Normand Charles Chaplin Harry McCoy Chester Conklin Edgar Kennedy Minta Durfee Phyllis Allen
| music = Frank D. Williams
| editing =
| distributor = Keystone Studios
| released =  
| runtime = 16 minutes English (Original titles)
| country = United States
| budget =
}}
  1914 short comedy film written and directed by Mabel Normand and starring Normand and Charles Chaplin.

==Plot==
Chaplin plays a waiter who fakes being a Greek Ambassador to impress a girl. He then is invited to a garden party where he gets in trouble with the girls jealous boyfriend.  Mabel Normand wrote and directed comedies before Chaplin and mentored her young co-star.

==Cast==
* Mabel Normand - Mabel
* Charles Chaplin - Waiter
* Harry McCoy - Lover
* Chester Conklin - Waiter
* Edgar Kennedy - Cafe proprietor
* Minta Durfee - Dancer
* Phyllis Allen - Dancer
* Josef Swickard - Father
* Alice Davenport - Mother
* Gordon Griffith - Boy
* Alice Howell - Party Guest
* Hank Mann - Cabaret Patron Billy Gilbert - Cabaret Patron
* Wallace MacDonald - Party guest

==See also==
* Charlie Chaplin filmography
*List of American films of 1914

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 