Vanangamudi
{{Infobox film
| name = Vanagamudi வணங்காமுடி
| image = 
| director = P. Pullaiah
| writer = A. K. Velan
| screenplay = P. Pullaiah
| story = A. K. Velan Savitri  M. K. Radha Chittor V. Nagaiah Pasupuleti Kannamba|P. Kannamba M. N. Nambiar Rajasulochana  K. A. Thangavelu  M. Saroja
| producer = M. Somasundaram
| music = G. Ramanathan
| cinematography = P. Ramasamy
| editing = K. Govindasamy
| studio = Saravanabhava & Unity Pictures
| distributor = Saravanabhava & Unity Pictures 
| released = 12 April 1957
| runtime = 170 mins
| country = India Tamil
| budget = 
}} Tamil film Savithri in the lead role. The film directed by P. Pullaiah, had musical score by G. Ramanathan and was released on April 12, 1957.  The film was super hit at box-oofice.

==Plot==
The film revolves around a king (M. K. Radha) and a talented sculptor (Sivaji Ganesan). Chitrashilpi, the sculptor, could make stones sing and dance with his artistic touch. Son of the kings bodyguard (Nagaiah), whose kind-hearted wife is played by Kannamba, he meets the princess (Savitri) in a forest and falls in love with her. However, each is not aware of the others identity.

Enters the villain (Nambiar) who has an eye on the throne and the princess. He has a mistress — a court dancer with a heart of gold (Rajasulochana). He employees all the tricks in his bag to get his desire fulfilled. He throws the sculptor in prison and introduces a princess-look-alike, a tribal girl (Savitri, again) and pulls the wool over the eyes of the king and marries the fake princess!

After many events, somewhat predictable, the hero exposes the villain and happiness is restored in the royal family.

==Cast==
*Sivaji Ganesan as Chithrasenan Savitri as Devasundari
*M. K. Radha as King
*Chittor V. Nagaiah as 
*P. Kannamba as Mangalam
*M. N. Nambiar as Narendran
*Rajasulochana as Ambiga
*K. A. Thangavelu as Paramjothi
*M. Saroja
*S. Balasubramanian|Gemini Balu 
*Nott Annaji Rao
*M. R. Santhanam
*Thangappan

==Crew==
*Producer: M. Somasundaram
*Production Company: Saravanabhava & Unity Pictures
*Director: P. Pullaiah
*Music: G. Ramanathan
*Lyrics: Thanjai N. Ramaiah Dass
*Story: A. K. Velan
*Screenplay: P. Pullaiah
*Dialogues: A. K. Velan
*Art Direction: S. V. S. Rama Rao
*Editing: K. Govindasamy
*Choreography: K. N. Dhandayudhapani Pillai
*Cinematography: P. Ramasamy
*Stunt: Stunt Somu Helen

==Soundtrack== Playback singers are T. M. Soundararajan, A. M. Rajah, Seerkazhi Govindarajan, S. C. Krishnan, M. L. Vasanthakumari, P. Leela, Jikki, P. Suseela & T. V. Rathinam.

One of the songs in the Carnatic raga Thodi, Ennai Pol Penn, rendered by P. Suseela with much feeling attracted attention. Even Carnatic musicians considered it the best film song composition in Thodi. The popular comedy pair, Thangavelu-Saroja, provided the laughs and there was a sizzling dance number by Helen (actress)|Helen.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Thanjai N. Ramaiah Dass || 02:51
|-
| 2 || Malaiye Un Nilaiye || Seerkazhi Govindarajan || 03:00
|-
| 3 || Paattum Baradhamum || T. M. Soundararajan || 02:48
|-
| 4 || Ennai Pol Pennallavo || P. Suseela || 03:06
|-
| 5 || Siramathil Thigazhvadhu.... Vaa Vaa Vaa Valarmadhiye Vaa || M. L. Vasanthakumari || 04:23
|-
| 6 || Kattazhagu Mama || P. Leela || 04:06
|-
| 7 || Mogana Punnagai || T. M. Soundararajan & P. Suseela || 03:20
|-
| 8 || Aatchiyin || Seerkazhi Govindarajan || 03:14
|-
| 9 || Kuthu Kummangu Koyya || Jikki || 03:18
|-
| 10 || Vaazhvinile Vaazhvinile || A. M. Rajah & P. Suseela || 03:04
|-
| 11 || Eerainthu Madhame || T. M. Soundararajan || 02:03
|}

==Reviews==
The film was super hit at box-office and remembered for the excellent performances by Sivaji Ganesan, Savithri and Nambiar and the melodious music.

==References==
*  

 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 


 