Mayandi Kudumbathar
{{Infobox film
| name           = Mayandi Kudumbathar
| image          = 
| alt            =  
| caption        = 
| director       = Rasu Madhuravan
| producer       = S. K. Selvakumar
| writer         = Rasu Madhuravan Seeman K. Jegannath Tarun Raj Kapoor
| music          = Sabesh-Murali
| cinematography = Balabharani
| editing        = Suresh Urs
| studio         = United Arts
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =   2.3 million
| gross          =   5.8 million 
}}
 Indian Tamil Tamil film Paandi previously. Starring 10 Tamil film directors, including Manivannan, Seeman (director)|Seeman, Tarun Gopi and Ponvannan, in lead and supporting roles.  The film, scored by Sabesh-Murali and filmed by Balabharani, was released on 5 June 2009, going on to become successful at the box office.

==Cast==
* Manivannan as Mayandi
* Ponvannan as Thavasi Mayandi (Mayandis son) Seeman as Virumandi Mayandi (Mayandis son)
* K. P. Jagannath as Cheenichamy Mayandi (Mayandis son)
* Tarun Gopi as Paraman Mayandi (Mayandis son)
* G. M. Kumar as Virumandi (Mayandis brother)
* Ravi Mariya as Chokkan Virumandi (Virumandis son)
* Nandha Periyasamy as Chinnu Virumandi (Virumandis son)
* Singampuli as Mayandi Virumandi (Virumandis mentally challenged son) Raj Kapoor as Sonakkaruppu (Mayandis son-in-law & Money lender)
* Poongodi as Poongodi
* Deepa as Mayakka Sonakkarupu
* Poovitha as Thavamani Thavasi
* Kaniya as Pavunu Virumandi
* S. Priya as Azhagamma
* Hema as Dhanam Cheenichamy
* Thamizharasi as Pecchi
* Mayilsamy
* Ilavarasu

==Plot==
Manivannan (Mayandi) and G M Kumar (Virumandi) are brothers who gets split following a property dispute. The latter and his sons are keen to take revenge on Mayandi and his wards. They often cross swords with each other, but a good samaritan Mayandi prevents the inevitable from happening.

Mayandi showers all his love and affection on his younger son (Paraman) and his elder brothers (Pon Vannan, Seeman and Jagan) too care for him a lot. However after Mayandhis death, life takes a turn and their wives starts to treat Paraman as a dirt. Fearing a catastrophe in the family because of him, Paraman decides to go away. Adding fuel to fire in his life is his girl friends unexpected wedding with someone else.

However he realises his fathers dream of completing his higher education and getting employed in a big firm. Now enters the family of Virumandi to settle scores with Paraman and his brothers. Did they succeed form the climax.

==References==
 

==External links==
*  
*  

 

 
 
 
 