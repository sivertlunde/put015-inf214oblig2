Forty Winks (film)
{{Infobox film
| name           = Forty Winks
| image          = 
| alt            = 
| caption        = 
| director       = Paul Iribe Frank Urson
| producer       = 
| screenplay     = Bertram Millhauser William Boyd Anna May Wong
| music          = 
| cinematography = J. Peverell Marley
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent William Boyd and Anna May Wong.   The film was released on February 2, 1925, by Paramount Pictures.

The film is presumed lost. {{cite web
 | title = Forty Winks
 | work = Progressive Silent Film List
 | publisher = Silent Era
 | url = http://www.silentera.com/PSFL/data/F/FortyWinks1925.html
 | date = 2009-08-02
 | accessdate = 2015-03-12
}} 
 
==Plot==
 

== Cast ==
*Raymond Griffith as Lord Chumley
*Theodore Roberts as Adam Butter
*Cyril Chadwick as Gasper Le Sage William Boyd as Lt. Gerald Hugh Butterworth
*Anna May Wong as Annabelle Wu

== References ==
 

== External links ==
*  
*  
 
 
 
 
 
 
 

 