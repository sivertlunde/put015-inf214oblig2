Yoroi Samurai Zombie
{{Infobox film name        = Yoroi Samurai Zombie image       = caption     = director    = Tak Sakaguchi writer      = Ryuhei Kitamura starring    = Hiromi Ueda Nana Natsume producer    = distributor = GAGA Bogeydom Licensing budget      = N/A released    =   runtime     = 91 minutes country     = Japan language    = Japanese
}}

  is a 2008 Japanese comic horror film directed by Tak Sakaguchi and written by Ryuhei Kitamura, who had previously collaborated on Versus (film)|Versus.  A family taken hostage and their kidnappers become prey to an undead samurai in a haunted cemetery.

== Plot ==
A family of four (Shigeo, Yasuko, Asami and Ryota) on a road trip has their idyllic holiday turned into a nightmare with a bizarre series of events. They stop their car after accidentally hitting a man dressed in white (Aihara) but when he stands up brandishing a gun, another man, Jirō, shoots Aihara. Jirō and his manic girlfriend Lisa then hijack the familys car, holding the father at gun point. When one of the tires is punctured, Jirō tells the father, Shigeo, that they are in the land of the undead and that the father must go off to find help, leaving his family with the captors. When Shigeo finds a ghostly cemetery he starts digging until he finds a blade which he then uses to decapitate himself, with the resulting fountain of blood spraying an ancient tomb. Some time later an eerie samurai warrior appears from the tomb and begins to hunt the family and the captors. The fugitives think to find shelter in an abandoned building but more horrors and surprises await.

== Cast ==
* Hiromi Ueda as Jirō
* Nana Natsume as Lisa
* Kyōsuke Yabe as Tateishi
* Issei Ishida as Aihara
* Airi Nakajima as Asami
* Keiko Oginome as Yasuko

* Shintarō Matsubara as Yoshioka
* Mitsuru Fukikoshi as Shigeo

== Release ==
The film had its world premiere at the Puchon International Fantastic Film Festival as part of the World Fantastic Cinema section.   It was released theatrically by Xanadeux ( ) in Japan on February 14, 2009.  The film also appeared at the New York Asian Film Festival in June 2009.  It was also released as a DVD in 2009 under the title Samurai Zombie with English and Chinese subtitles. 

== Reception == Battle Royale or Audition (1999 film)|Audition, but as far as contemporary Japanese horror goes, Samurai Zombie is certainly one of the stronger entries Ive seen for a while."   James Mudge of Beyond Hollywood described it as "a cut above" the typical Japanese splatter film. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 