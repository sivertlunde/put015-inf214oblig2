You Can't Hurry Love (film)
{{Infobox film
| name = You Cant Hurry Love
| image = YouCantHurryLove.jpg
| image size =
| alt =
| caption =
| director = Richard Martini
| producer = Jonathan D. Krane Simon R. Lewis Lawrence Kasanoff William J. Rouhana Jr. Anthony Santa Croce Ellen Steloff
| writer = Richard Martini
| narrator =
| starring = David Leisure Scott McGinnis Anthony Geary Bridget Fonda
| music = Bob Esty
| cinematography = Peter Lyons Collister John Schwartzman
| editing = Richard Candib
| studio = Vestron Pictures
| distributor = Lightning Pictures
| released =  
| runtime = 92 min
| country = USA English
| budget =
| gross = $333,152 (USA)
| preceded by =
| followed by =
}}
You Cant Hurry Love is a 1988 romantic comedy film written and directed by Richard Martini. A guy who moonlights as a low-budget director of commercials is looking for someone to love, so he pays a dating service and is videotaped on several occasions. The film shows the many women who seduce our hero, until he finally finds his true love...

==DVD release== Love Hurts.

==Cast==
* David Leisure - Newcomb
* Scott McGinnis - Skip
* Anthony Geary - Tony
* Bridget Fonda - Peggy Kellogg
* Frank Bonner - Chuck Hayes
* Lu Leonard - Miss Frigget
* Merete Van Kamp - Monique
* David Packer - Eddie
* Charles Grodin - Mr. Glerman
* Sally Kellerman - Kelly Bones
* Kristy McNichol - Rhonda
* Luana Anders - Macie Hayes
* Jake Steinfeld - Sparky
* Judy Balduzzi - Glenda
* Danitza Kingsley - Tracey
* Kimber Sissons - Brenda
* Kimberly Foster - Girl Reading Book
* Winifred Freedman - Sample Videotape #2
* Michael Sorich - Drug Dealer

==External links==
*  
*  
*  

 
 
 
 
 


 