Premium Rush
{{Infobox film
| name           = Premium Rush 
| image          = Premium rush film.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = David Koepp
| producer       = Gavin Polone
| writer         = David Koepp John Kamps
| starring       = Joseph Gordon-Levitt Michael Shannon Dania Ramirez Jamie Chung Wolé Parks Aasif Mandvi
| music          = David Sardy 
| cinematography = Mitchell Amundsen 
| editing        = Derek Ambrosi    Jill Savitt 
| studio         = Pariah
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 92 minutes  
| country        = United States
| language       = English
| budget         = $35 million 
| gross          = $31.1 million    
}} thriller film directed by David Koepp and written by Koepp and John Kamps. The film stars Joseph Gordon-Levitt, Michael Shannon, Dania Ramirez and Jamie Chung. It follows a bicycle messenger chased around New York City by a corrupt police officer who wants an envelope the messenger has. It was released on August 24, 2012 by Columbia Pictures.

==Plot==
The film has a nonlinear narrative. It contains numerous flash-forward and flashback cuts, indicated with an overlaid digital clock showing the time advancing rapidly forward/backward to the next scene. In chronological order:

Wilee (Joseph Gordon-Levitt) is a disenchanted Columbia Law School graduate who has put off taking the bar exam because he could not bear to enter the humdrum life of the legal profession. He finds meaning and purpose in being employed as a thrill-seeking New York City bicycle messenger despite arguments with his girlfriend and fellow bike messenger, Vanessa (Dania Ramirez), who insists that he should make something of himself.
 gangs smuggle people from China to the United States.
 NYPD detective who owes him money, offering to clear Mondays debt if Monday gets him the ticket. Monday begins searching for Nima, who decides to hire Wilee to deliver the envelope with the ticket to Sister Chen by 7 pm. After Wilee leaves, Nima is confronted by Monday, who coerces her into revealing that Wilee has the ticket and handing over the delivery receipt. After Monday leaves, Vanessa finds Nima, and learns the contents of the envelope.

Monday catches up with Wilee before he leaves the campus where Nima lives and threatens him for the ticket. Wilee escapes and heads to the police station to report Monday, only to find out hes a police officer. Wilee hides in the bathroom, where he opens the envelope and finds the ticket.

After he escapes the station, Wilee angrily tells his dispatcher, Raj (Aasif Mandvi), that he is returning the package so that someone else can drop it off. Returning to Nimas college, Wilee leaves the envelope, which is picked up by his rival, Manny (Wolé Parks). Before Manny picks it up, however, Monday calls the dispatch to redirect the delivery to a different address.

As he is about to leave the college after returning the envelope, Wilee runs into Nima. He confronts her about the ticket, and she reveals the truth. Guilt-ridden, Wilee tries to catch up to Manny, who refuses to give Wilee his drop. They race each other and in the process, are chased by a bike cop who had earlier tried to arrest Wilee. As they approach Mondays location, the bike cop tackles Manny off his bike and arrests him. Vanessa, who learns of Mondays trickery and races over to warn Manny, appears, grabs Mannys bag and gives it to Wilee.

As they were about to escape, Wilee is hit by an oncoming taxi. He is put in an ambulance with Monday, while his damaged bike is taken to an impound lot, with the envelope hidden in the handlebars. Monday tortures Wilee by pressing on his injuries, and Wilee offers to give Monday the envelope in exchange for his bike.

Wilee tells Monday that the envelope is in Mannys bag, and Monday leaves to search it, while Wilee meets with Vanessa in the impound lot. She gives him the envelope, which she had retrieved, and he escapes on a stolen bike. Monday, realizing Wilee has tricked him, pursues Wilee to Sister Chens place. Meanwhile, Nima calls Mr. Leung for help. He deploys his enforcer, the Sudoku Man, to help her.

As Wilee reaches Chinatown, Manhattan|Chinatown, he is confronted by Monday, who is threatening to kill him. However, Vanessa arrives with a flash mob of messengers, dispatched by Raj, who delay Monday by hitting him numerous times, giving Wilee time to deliver the ticket to Sister Chen. She calls the captain of her ship and tells him to allow Nimas family inside. Outside, Monday is confronted by the Sudoku Man, who shoots Monday in the back of his head with a silenced pistol. Dying and feeling faint, Monday tries to get in his car saying that he just wants to think for a second, but dies before he can do so. Nimas mother calls her and confirms that she and Nimas son have gotten on the ship. Nima meets with Wilee and Vanessa while they are finally reunited again.

==Cast==
 
*Joseph Gordon-Levitt as Wilee, young bicycle messenger
*Michael Shannon as Bobby Monday, police officer
*Dania Ramirez as Vanessa, bicycle messenger, Wilees girlfriend
*Wolé Parks as Manny, bicycle messenger, Wilees rival
*Aasif Mandvi as Raj, bicycle messenger dispatcher
*Jamie Chung as Nima, Vanessas roommate
*Christopher Place as Roselli, Bike Cop 
*Henry O as Mr. Leung, Chinese hawaladar
*Boyce Wong as Mr. Lin, a local loan shark
*Brian Koppelman as Loan Shark
*Kevin Bolger as Squid
*BoJun Wang as Nimas Son
*Sean Kennedy as Marco
*Kym Perfetto as Polo
*Anthony Chisholm as Tito
*Lauren Ashley Carter as Phoebe
*Aaron Tveit as Kyle
*Darlene Violette as Debra
*Mario DLeon as Moosey
*Djani Johnson as Johnny
*Wai Ching Ho as Sister Chen
 

==Production==
 )]] Canal Street.   

==Lawsuit and charges of copyright infringement==
In 2011, a lawsuit claiming copyright infringement was filed in the Northern District of California by author Joe Quirk, claiming Premium Rush was based on his 1998 novel The Ultimate Rush. The suit claimed many plot, character name, and scene similarities to Quirks original novel.  In July 2012, federal judge Richard Seeborg declined to dismiss Quirks claim that Sony Pictures, parent company of Columbia Pictures, had breached an implied contract. The production company Pariah, director David Koepp and co-screenwriter John Kamps are also named in the suit.    On April 2, 2013, U.S. District Judge Richard Seeborg dismissed this case, finding that the two works were not substantially similar. 

==Reception==
The film has received positive reviews from critics. It currently holds a 76% "Certified Fresh" rating on   critic Roger Ebert awarded the film 3.5 stars out of 4, calling it a "breakneck chase movie". 

Metacritic gave the film 66/100 based on 36 critics. 

On its opening weekend, Premium Rush opened at #8, grossing at a disappointing $6.03 million.   As of July 20, 2013, the film has grossed $20,275,446 in North America while grossing $10,808,153 in foreign markets, totaling a worldwide income of $31,083,599,  just short of its budget.

==References==
{{reflist|2|refs=
   

   

   

   

   
   

<!-- Unused citations
      

   
-->
}}

==External links==
*  official site
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 