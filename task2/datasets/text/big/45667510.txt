Small Claims: White Wedding
{{Infobox film
| name           = Small Claims: White Wedding
| image          = 
| alt            = 
| caption        = 
| director       = Cherie Nowlan
| producer       = Rosemary Blight  Ben Grant Sue Masters
| writer         = Kaye Bendle Keith Thompson
| screenplay     = 
| story          = 
| based on       =  
| starring       = Claudia Karvan Rebecca Gibney Alyssa McClelland
| music          =  Anna Howard 
| editing        = Mark Perry
| studio         = Goalpost Pictures
| distributor    = Network Ten
| released       =  
| runtime        = 
| country        = Australia
| language       = English
| budget         = 
| gross          =  
}}

Small Claims: White Wedding is an   in 2005. The film was a co-production with subscription television and was also broadcast on the Foxtel, Austar, and Optus Television Subscription Television services.  The series was written by husband and wife team, Keith Thompson and Kaye Bendle. 

This is part one of a mystery series about two overworked young mums, de-skilled beyond their worst nightmares, who become a formidable pair of sleuths, directed by Cherie Nowlan.  Their cases are the murders, greed and dark passions that lurk behind the anonymous facade of the suburbs. 

==Cast==
* Rebecca Gibney, as Chrissy Hindmarsh
* Claudia Karvan, as Jo Collins
* Alyssa McClelland, as Kiara Duffy
* Deborah Kennedy as Trudy Duffy
* Paul Barry, as Greg Collins
* Carol Burns, as Pamela
* Gyton Grantley, as Detective Senior Constable Brett
* Wayne Blair, as Detective Senior Constable Lacey
* Roy Billing, as Ron Duffy
* Brooke Satchwell, as Imogen
* Michael Dorman, as Sean
* Victoria Thaine, as Tori
* Rupert Reid, as David

==See also==
* Australian films of 2004
* Cinema of Australia
* List of films shot in Sydney
* List of Australian films
*  
*   Website

==External links==
*  

==References==
 

 

 
 