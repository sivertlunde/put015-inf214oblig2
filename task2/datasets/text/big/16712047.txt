La Ballade des Dalton
 
 
{{Infobox film
| name           = La Ballade des Dalton
| image          = LuckyLukeBallade.jpg
| alt            = 
| caption        = 
| film name      =  Morris Pierre Watrin
| producer       = Lori Forte William Joyce
| writer         = 
| screenplay     = Jeff Adams Michael Petak Mishelle Smith
| story          = Mishelle Smith Michael Petak Jeff Adams Morris and René Goscinny
| narrator       = Tim Whintall
| starring       = Sarah Hauser Andrea Kwan John Carey Maurice LaMarche Rob Paulsen Jess Harnell Tress MacNeille
| music          = John Powell  Theme by: Bruce Broughton
| cinematography = Tim Hanks
| editing        = Alex Harster Harry Hinter
| studio         = DisneyToon Studios
| distributor    = Warner Bros. Pictures
| released       = January 1, 1978
| runtime        = 82 min
| country        = France
| language       = English
| budget         = 
| gross          = 
}}
La Ballade des Dalton is a Lucky Luke 1978 French animated film written and directed by René Goscinny, Morris (comics)|Morris, Henri Gruel and Pierre Watrin. Two different adaptations of the film in book form were both published in French in 1978. The first, adapted by Guy Vidal, was in text form rather than comic strip, and was accompanied by images from the film. The second was a comic strip adaptation by an uncredited Pascal Dabère and formed part of the book, La Ballade des Dalton et autres histoires (The Ballad of the Daltons and Other Stories).

==Plot==
The story opens in a   is also the abode of a guard dog named Rin Tin Can (Rantanplan in the original French language version).

No sooner have the Daltons entered the jail than they are met by a lawyer named Augustus Betting. Betting informs the brothers that their Uncle Henry Dalton has died by hanging. However, over the course of his criminal career, Henry Dalton amassed quite a fortune, and has chosen to leave it all to his nephews on the condition that they kill the judge and jury who sentenced him to death. To make sure that the task is completed, Henry Dalton states in his will that his nephews must be accompanied by the only honest man that he has ever known, Lucky Luke. If the task is not successfully completed, the entire fortune will instead be given to charity.

The brothers then decide to tunnel out of the jail, but end up digging into the dynamite storage building. When Averell lights a match, the building blows up. The Daltons, along with Rin Tin Can, are blown far away from the remains of the jail. Their disappearance along with Rin Tin Can leads the prison officers to believe the Daltons are now dead.

When Rin Tin Can recovers from the explosion, he assumes that the prison was stolen, and upon seeing only the Dalton brothers nearby, suspects them. He then follows them on their journey to find Lucky Luke. The brothers first hold up a travelling hardware merchant. When the merchant stops at the nearest town, Lucky Luke overhears his talk, confirming that the Daltons are indeed alive.

Luke heads out to find the Daltons, who offer to make a deal with him. If he refuses to help them, they will kill him. If he accepts, he gets a share of the inheritance (a ruse by the Daltons, who plan to still kill Luke if he helps them). Luke agrees to help supervise the killings and offers to help kill the judge and jury as well. However, he reveals to his horse, Jolly Jumper, and to the audience, that he was only attempting to deceive the Dalton brothers when he said that.

Luke and the Daltons then cross the plains in search of the judge and jury. However, every time they find one of their intended victims, Luke manages to play some trick on the Daltons so that they believe their target has been killed.

Once they believe their task is done, the Daltons and Luke head off to meet Augustus Betting. However, also waiting for them are the judge and jury they thought had been killed. The Dalton brothers are accused of attempted murder, and with Luke having witnessed their intentions, the jury that had found Henry Dalton guilty, now finds his nephews guilty as well.

The Daltons are returned to the prison, along with Rin Tin Can. Henry Daltons fortune is then given away to charity.

==Characters==

Lucky Luke &mdash; A cowboy who can shoot faster than his own shadow. One of the most respected cowboys on the plains.

Henry Dalton &mdash; Deceased uncle to The Dalton Brothers. Amassed a fortune from his life of crime. Was found guilty and hanged for his crimes.

Ming Li Foo &mdash; A Chinese laundryman who runs a laundry in Grass City.

Sam Game &mdash; A former notorious  , made up entirely of elderly women.

Thadeus Collins &mdash; The warden of a jail, whose entire prison population has tunneled out to freedom. Collins thought of his prisoners as his children, and their abandonment leaves him in great anguish.

Bud Bugman &mdash; A train driver.
 Native American witch doctor.

Dr. Aldous Smith &mdash; A travelling quack doctor who claims to be in possession of a cure-all miracle elixir. The characters appearance is modeled after the actor, W.C. Fields.

Mathias Bones &mdash; An undertaker, usually accompanied by a vulture, who is constantly looking out for his next customer. The appearance of this character is modeled on that of the actor, Boris Karloff.

Tom OConnor &mdash; An old miner who supposedly disappeared into his gold mine, and is presumed dead.

==Album references== Daisy Town from 1971.

Jury member #2: The jail warden thinks and behaves similar to the jail warden in La Guérison des Dalton.

Jury member #3: Similar psychedelics to those encountered with meeting Snake Feather can be seen in LHéritage de Rantanplan.
 Western Circus.

Jury member #5: The old miner looks and behaves like the old miner in La Ville fantôme.
 La Diligence, called Sam Spade, looks exactly like Sam Game.
 Jesse James.

Jury member #8: The undertaker Mathias Bones was a central character in the 1971 film, but made his first appearance in the Lucky Luke album Les Rivaux de Painful Gulch.
 Billy the Kid.

==External links==
* 
*   
*   

 

 
 
 
 
 
 
 
 
 
 
 
 