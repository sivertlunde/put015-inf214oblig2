The Bottom of the Bottle
{{Infobox film
| name           = The Bottom of the Bottle
| image          = Poster of the movie The Bottom of the Bottle.jpg
| image_size     =
| caption        =
| director       = Henry Hathaway
| producer       = Buddy Adler
| writer         = Sydney Boehm (Writer) Georges Simenon (Novel)
| starring       = Joseph Cotten Van Johnson
| music          = Leigh Harline
| cinematography = Lee Garmes
| editing        = David Bretherton
| distributor    = 20th Century Fox
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $1,695,000 
| gross          = $1,100,000 (US rentals)  
}}

The Bottom of the Bottle is a 1956 CinemaScope American drama film based on the novel written by Georges Simenon during his stay in Nogales, Arizona. The novel was adapted for film by Sydney Boehm and directed by Henry Hathaway.

==Plot==
Patrick Martin (Joseph Cotten), known as P.M., is a wealthy attorney and rancher in the border town of Nogales, Arizona. He returns home to find his brother Donald (Van Johnson) hiding in his garage. A former drunkard, Donald had been sent to the penitentiary five years previously for killing a man in a barroom brawl. It was in self-defense but P.M. hadnt defended his brother and he was convicted.
 Santa Cruz Shawn Smith) and children (Kim Charney and Sandy Descher) are in dire straits. The straits get even more dire when P.M. tells him the river is flooded and it will be days before anyone can cross.

P.M. is all atwitter because his wife Nora (Ruth Roman), whom he married after Donald had gone to prison, doesnt know about his jail-bird brother. He introduces Donald to Nora and the rest of his Cadillac Cowboy and ranch society friends as an old friend, Eric Bell, and is kept busy trying to make sure Donald doesnt find anything harder than ginger ale to drink.

Donald gets a telephone call telling him that his family has gone from dire straits to destitution, and when P.M. refuses to help through his contacts in the Mexico side of Nogales, Donald knocks him down, grabs a couple of bottles of whiskey and dashes out of the house into the rain.

Nora eventually discovers Donalds true identity and persuades P.M. to help Donalds family. But after a report that Donald has committed a theft, Hal Breckinridge (Jack Carson) forms a posse to bring Donald back. Although reluctant to aid a felon, P.M. shows Donald a good place in the river to cross safely into Mexico, but falls off his horse and nearly drowns. Donald saves his life, then surrenders to the law.

==Cast==
* Joseph Cotten as P.M.
* Van Johnson as Donald
* Ruth Roman as Nora
* Jack Carson as Breckinridge
* Brad Dexter as Miller Jim Davis as Cady

==References==
 

==External links==
*   
*  

 

 
 
 
 
 
 
 
 
 


 