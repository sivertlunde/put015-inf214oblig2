La familia Pérez
{{Infobox film
| name           =La familia Pérez
| image          = 
| image size     =
| caption        =
| director       =
| producer       =
| writer         = 
| narrator       =
| starring       =Sara García
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 1948
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1948 Mexico|Mexican film. It stars Sara García.

== Cast ==
*Joaquín Pardavé - Gumaro Pérez
*Sara García - Natalia Vivanco de Pérez
*Manuel Fábregas - Luis Robles del Valle (as Manolo Fabregas)
*Beatriz Aguirre - Clara
*José Elías Moreno - Toribio Sánchez
*Alma Rosa Aguirre - Patricia
*Lilia Prado - Rosa
*Felipe de Alba - Roberto
*Conchita Carracedo - Margarita
*José Baviera - Don Ricardo
*Ana María Villaseñor - Irene (as Ana Ma. Villaseñor)
*Óscar Pulido - Raymundo
*Celia Duarte - Petra
*Isaac Norton - Ramón
*Eduardo Noriega - Felipe
*José Ángel Espinosa Ferrusquilla - Narrator
*Jorge Fábregas - (uncredited)
*Gloria Lozano - Empleada en oficina (uncredited)
*Paco Martínez - Invitado a boda (uncredited)
*Carlos Múzquiz	- Rodríguez (uncredited)
*Manuel Trejo - (uncredited)
*Hernán Vera - Movedor (uncredited)
*Oscar Villela - (uncredited)

==External links==
*  

 
 
 
 


 