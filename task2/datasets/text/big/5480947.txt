Jack Smith and the Destruction of Atlantis
{{Infobox film
| name           = Jack Smith and the Destruction of Atlantis
| image          = Js-atlantis-poster.jpg
| caption        =  Mary Jordan
| producer       = Mary Jordan Kenneth Wayne Peralta
| writer         = Mary Jordan John Waters Mary Sue Slater (sister) Richard Foreman Mario Montez Gary Indiana Jonas Mekas Sylvère Lotringer Thomas Lanigan-Schmidt Uzi Parnes John Zorn
| music          = 
| cinematography = 
| editing        = 
| distributor    = Tongue Press Monk Media
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Jack Smith. Mary Jordan and produced by Tongue Press Productions. 

The film was given a limited release in New York movie theaters beginning on April 11, 2007.
 John Waters, Smiths sister Mary Sue Slater, playwright Richard Foreman, Smith and Warhol star Mario Montez, writer Gary Indiana, and musician John Zorn, among others. Ebony concludes that the film " n the end ... manages to evoke the quirky and often cantankerous personality of its subject without ever making him seem merely a disgruntled artist and social misfit, as some may think him. ... I feel that Jordans multifaceted and impassioned portrait rings true. Smith, in fact, comes off in the film as an ingenious art-world Cassandra, more relevant today than ever."   By David Ebony, Art in America, May 07, p.47. Retrieved 2-3-09. 

Wesley Morris, whose review appeared in the Boston Globe, was impressed that Jordan managed to convey Smiths "unmitigated avant-gardism  ... his manias and paranoia, his peculiar genius  ... his financial poverty, credibly suggest  his victimization through artistic robbery ... ( hats how Smith felt anyway...)  ... also telling a story about art in America." Morris wrote that "Jordan wrangles the obligatory talking heads, but the things they say are smart, vivid, complex, miffed, and uncensored ...  Judith Malina, Taylor Mead" among the others listed above and more. 

==References==
 

==External links==
*  
*  
* New York Magazine  

 
 
 
 