Screamplay
{{Infobox Film
  | name = Screamplay
  | image = ScreamplayDVD.jpg
  | caption = DVD cover
  | director = Rufus Butler Seder 
  | producer = Dennis M. Piana
  | writer = Ed Greenberg Rufus Butler Seder
  | starring = George Kuchar Katy Bolger Rufus Butler Seder Basil J. Bova Ed Callahan
  | music = Basil J. Bova George Cordeiro 
  | cinematography = 
  | editing = Rufus Butler Seder   
  | distributor = Troma Entertainment
  | released = 1985 
  | runtime = 91 minutes
  | country = United States
  | language = English
  | budget =  
}} American horror film directed by Rufus Butler Seder.

==Plot==
Aspiring screenwriter Edgar Allen (Rufus B. Seder) arrives in Hollywood carrying his most valuable possessions: a battered suitcase and a typewriter. Edgar Allens best attribute is his wild imagination. He imagines scenes so vividly for the murder mystery he is writing that they seem to come to life...and they do! As mysterious gruesome murders pile up, Edgar Allen must confront aging actresses, rock stars and the police in a bleak setting of broken dreams and hideously broken bodies in Hollywood. As the line between reality and imagination becomes more blurred, Edgar Allen, convinced the only way to be a real writer is to suffer, is driven slowly mad.

==Release==
After a theatrical and video release, Troma released the film on DVD in 2005.   This release is currently out of print.

==Reception==
The film is currently distributed by   influence, lacking the gratuitous gore, nudity, and/or cheap gags present in most Troma films.

==References==
 

==External links==
* 

 
 
 
 

 