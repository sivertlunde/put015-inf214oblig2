The Sorcerer and the White Snake
{{Infobox film
| name = The Sorcerer and the White Snake
| image = The Sorcerer and the White Snake poster.jpg
| alt = 
| caption = 
| film name = {{Film name| traditional = 白蛇傳說之法海
| simplified = 白蛇传说之法海
| pinyin = Bái Shé Chuán Shuō Zhī Fǎ Hǎi
| jyutping = Baak6 Se4 Cyun4 Syut3 Zi1 Faat3 Hoi2}}
| director = Ching Siu-tung
| producer = Chui Po Chu Yang Zi
| writer = Charcoal Tan Tsang Kan Cheung Szeto Cheuk Hon
| starring = Jet Li Huang Shengyi Raymond Lam Charlene Choi Wen Zhang Vivian Hsu
| music = Mark Lui
| cinematography = Venus Keung
| editing = Lam On Yee
| studio = China Juli Entertainment Media
| distributor = Distribution Workshop   
| released =  
| runtime = 
| country = China  Hong Kong
| language = Mandarin
| budget = HK$200 million   
| gross = US$32,172,621 ( ) 
}}
The Sorcerer and the White Snake, previously known as, Its Love and Madame White Snake is a 2011 film directed by Ching Siu-tung and starring Jet Li.  It is based on the Chinese Legend of the White Snake. Production started in September 10, 2010  and ended on January 16, 2011.  The film is in 3-D film|3-D    and was shown out of competition at the 68th Venice International Film Festival on 3 September 2011.  It was released in mainland China on 28 September 2011 and Hong Kong on 29 September. 

==Plot==
The film starts with Abbott Fahai and his assistant Neng Ren heading towards a snow blizzard through a magical door. An ice harpy appears at the top of the mountains and turns the impetuous Neng Ren into an ice statue. She then reveals her past and reasons to kill all men. Unable to persuade the ice harpy to turn over a new leaf, Fahai is forced to fight the ice harpy. The battle ends with Fahai capturing the ice harpy using a demon trapper, which releases Neng Ren from ice. Neng Ren is then tasked to confine the ice harpy at Lei Feng Pagoda, and the master and assistant head back to the door which disappears after closing behind them. At Lei Feng Pagoda, Neng Ren transfers the ice harpy from the demon trapper into the magic circle, which holds other trapped demons in it.
 Vital Essence to flow from her into his body thus saving him. Soon, Xu Xian wakes up and tells his friends about being kissed by a beautiful girl, which only makes them laugh.

After finding a victim of a bat demon, Fahai and Neng Ren leave the temple immediately to subdue the bat demon to avoid more casualties. Xu Xian came across them and offers a boat ride to the city. Susu starts thinking about the day she kissed Xu Xian and decides to head to the city to find Xu Xian. Meanwhile, while Qingqing is exploring the city, she came across Neng Ren and decided to help him subdue the bat demon by revealing its location. Neng Ren defeats the bat demons cohorts, but is unable to subdue the bat demon king who bites him. Though he is subsequently saved by Fahai, Neng Ren starts turning into a bat demon himself the next day and decides to run away.

In the meanwhile, after a series of pranks, Xu Xian recognizes Susu and they spend the night together, which is rather unfortunate for Xu Xian because he does not know that he was making love to a snake in a human form.

Neng Ren is found by Quinqing and the two befriend each other. They realize that Neng Ren, despite becoming a bat demon, still has all his human taste for human food, and most of his human qualities. Meanwhile Xu Xian and Susu wed. Shortly thereafter Fahai finds Xu Xian and after seeing a mysterious substance in his medicines he gives Xu Xian a spirit blade. Susu is pursued by Fahai, who tells her to leave Xu Xian. She doesnt, which causes Fahai and his disciples to invade her and Xu Xians cottage. Susu fights the battle in her snake form, but is stabbed by Xu Xian unaware of her true identity. Susu escapes but is gravely injured. Xu Xian, after realizing what he has done, decided to get the spirit root to heal her.

Helped by Susus friend, a mouse, Xu Xian manages to retrieve a root kept inside the Lei Feng Pagoda that could save Susu, but is possessed by demons as a result of this. Fahai and the other monks capture Xu Xian and prepare to cast spells to banish the demons from his body. Susu recovers and she goes to find Xu Xian along with Quinqing. They are confronted by Fahai, who tries to explain to them that the spell should not be broken before it is complete. Susu did not believe him and thought he was saying this to separate them so the two sisters start battling Fahai. After countless wounds Fahai lies back exhausted. As he looks around he sees Neng Ren helping to save his former colleagues, the monks of the temple who were trying to complete the spell to release Xu Xian from the demons. He looks up at the sky and questions whether he was right to have been a demon hunter all his life.

Susu releases Xu Xian from the spell after which Xu Xian does not have any memory of Susu. Susu blames this on Fahai, after which they have one last battle. Fahai manages to trap Susu in the temple of Lei Feng Pagoda. At this point, Susu repents and asks to see Xu Xian just one last time, as she could accepted any punishments as long as she could see him for the last time. Simultaneously, Fahai gets the answer to his question and understands what he must do. Filled with the divine spirit, he lifts up the pagoda to allow Susus last wish to come true.

After a brief reunion with Xu Xian, she tells him of her story of having meditated for a thousand years before she met him. She tells him that it was all worth less than a moment with him, that even when he didnt remember her, her memory of love with him is enough, and that even if she has to die or live with pain, she wont regret, how much she has to suffer to see him one last time . She kisses him, causing him to remember everything. As the pair go to kiss one last time Susu is sucked back into the temple, leaving Xu Xian chasing her as both cry and reach out for each other.

Qingqing, watching all this from a distance with Neng Ren, tells him that she doesnt want to love anyone as her sister loved Xu Xian, and leaves saying that he will never be a true bat demon anyway.

After this, we see Xu Xian picking herbs around the temple and inside Susu has returned to her true form as a white snake trapped inside the temple. Fahai is seen walking the mountainside when suddenly Neng Ren (now a complete bat demon) appears alongside him. Throwing him an apple to eat Fahai tells him that his new look suits him, and they continue on the journey together again.

==Cast==
*Jet Li as Abott Fahai (法海)
*Huang Shengyi as White Snake/Susu (白蛇)
*Raymond Lam as Xu Xian (許仙)
*Charlene Choi as Green Snake/Qingqing (小青)
*Wen Zhang as Neng Ren (能忍)
*Jiang Wu as Turtle Devil (龜妖)
*Vivian Hsu as Ice Harpy(雪妖)
*Miriam Yeung as Rabbit Devil (兔妖)
*Chapman To as Toad Monster/Gugu (蛤蟆怪)
*Law Kar-ying as Mysterious Herbalist (神秘藥師)
*Lam Suet as Chicken Devil (雞妖)
*Sonija Kwok as Bu Ming (不明)
*Angela Tong as Cat Devil (貓妖) 	
*Michelle Wai as Bat Devil (蝙蝠妖女)

==Production==
Leading actors Ethan Juan, Peter Ho, Mark Chao and Raymond Lam auditioned for the lead role in the film. In the end, Lam got the role. It was reported that Juan and Chao were dropped as they were deemed to be not well known enough in China and Hos image did not suit the male lead. 
 Eva Huang as White Snake, Raymond Lam as Xu Xian, Charlene Choi as Green Snake and Wen Zhang, whom Li invited to play his disciple Neng Ren. 

Regarding the action scenes, Jet Li said he had never been this exhausted before. Li says,
 

Raymond Lam said he was always being hit by others,
 

Filming wrapped on January 16, 2011. Originally titled Madame White Snake in English, the film logo was unveiled during the production wrap press conference with the new official English title, Its Love.  However, the English title was changed to The Sorcerer and the White Snake when the distributor was announced.   But the actual title used has been The Emperor and the White Snake, both on DVD and at IMDb. 

==See also==
* Jet Li filmography
* Legend of the White Snake
* Green Snake
* New Legend of Madame White Snake
* Madam White Snake (TV series)
* The Legend of the White Serpent (1956 film)
* The Tale of the White Serpent
* Hong Kong films of 2011

==References==
 

==External links==
*  
* 
*    
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 