Carry On Girls
 
 
 
{{Infobox film
| name           = Carry On Girls
| image          = Carry On Girls.jpg
| image_size     =
| caption        = Publicity photograph
| director       = Gerald Thomas
| producer       = Peter Rogers
| writer         = Talbot Rothwell Jack Douglas Patsy Rowlands Jimmy Logan Eric Rogers
| cinematography = Alan Hume
| editing        = Alfred Roome
| distributor    = Rank Organisation
| released       = November 1973
| runtime        = 88 mins
| country        = United Kingdom
| language       = English
| budget         = £205,962
}} the series Charles Hawtrey. Jack Douglas makes his third appearance, this time upgraded to a main role. Jimmy Logan makes a guest appearance in his second and final Carry On.

==Plot== Jack Douglas), David Lodge and Billy Cornelius) to investigate the male pageant contestant but Peters previously prim girlfriend, Paula (Valerie Leon), has a makeover and steps into the breach as the mysterious girl. Prodworthys gang put "Operation Spoilsport" into action, sabotaging the final contest with water, mud and itching powder. With an angry mob after his blood, Sidney makes his escape with Hope on her motorcycle.

==Certification== certificate (open to families) than the more restrictive AA certificate, barring entry to the under-fourteens.

==Cast==
 
 
*Sid James as Sidney Fiddler
*Barbara Windsor as Hope Springs (real name Muriel Bloggs)
*Joan Sims as Connie Philpotts
*Kenneth Connor as Mayor Frederick Bumble
*Bernard Bresslaw as Peter Potter
*Peter Butterworth as Admiral
*June Whitfield as Augusta Prodworthy Jack Douglas as William
*Patsy Rowlands as Mildred Bumble
*Joan Hickson as Mrs Dukes David Lodge as Police Inspector
*Valerie Leon as Paula Perkins
*Margaret Nolan as Dawn Brakes
*Angela Grant as Miss Bangor
*Sally Geeson as Debra
*Wendy Richard as Ida Downes
*Jimmy Logan as Cecil Gaybody
*Arnold Ridley as Alderman Pratt
*Robin Askwith as Larry
 
*Patricia Franklin as Rosemary
*Brian Osborne as "Half a quid" citizen
*Bill Pertwee as Fire chief
*Marianne Stone as Miss Drew
*Brenda Cowling as Matron
*Zena Clifton as Susan Brooks
*Laraine Humphrys as Eileen Denby
*Pauline Peart as Gloria Winch
*Caroline Whitaker as Mary Parker
*Barbara Wise as Julia Oates
*Carol Wyler as Maureen Darcy
*Mavis Fyson as Francis Cake
*Billy Cornelius as Constable
*Edward Palmer as Elderly resident
*Michael Nightingale as City gent
*Hugh Futcher as "Theres Fiddler" citizen
*Elsie Winsor as Cloakroom attendant
*Nick Hobbs as Stunt double
*Ron Tarr as Bearded man
 

==Crew==
*Screenplay – Talbot Rothwell Eric Rogers
*Production Manager – Roy Goddard
*Art Director – Robert Jones
*Director of Photography – Alan Hume
*Editor – Alfred Roome
*Camera Operator – Jimmy Devis
*Assistant Director – Jack Causey
*Sound Recordists – Paul Lemare & Ken Barker
*Continuity – Marjorie Lavelly
*Make-up – Geoffrey Rodway
*Hairdresser – Stella Rivers
*Costume Design – Courtenay Elliott
*Set Dresser – Kenneth MacCallum Tait
*Dubbing Editor – Patrick Foster
*Assistant Editor – Jack Gardner Larry
*Titles – GSE Ltd
*Processor – Rank Film Laboratories
*Producer – Peter Rogers
*Director – Gerald Thomas

==Filming and locations==

*Filming dates – 16 April-25 May 1973

Interiors:
* Pinewood Studios, Buckinghamshire

Exteriors:
* Brightons West Pier. The Palace Pier, Brighton was used a couple of years earlier in Carry On at Your Convenience.
* Slough Town Hall, Slough
* Clarges Hotel, Brighton
* Brighton Beach, Brighton
* Marylebone Railway Station, London

==Notes==
Valerie Leons voice for the film was dubbed by co-star June Whitfield. 

==References==
 

==Bibliography==
* 
* 
* 
* 
*Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (third edition) (2007) (Reynolds & Hearn Books)
* 
* 
* 
* 
* 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 