Dharam Kanta
{{Infobox film
| name           = Dharam Kanta
| image          = Dharam Kanta.Jpg
| caption        = Movie Poster
| director       = Sultan Ahmed
| producer       = Sultan Ahmed
| writer         = 
| screenplay     = Bharat B. Bhalla	
| story          = 
| based on       =  
| narrator       = 
| starring       = Raaj Kumar Rajesh Khanna Jeetendra Waheeda Rehman Reena Roy Sulakshana Pandit Amjad Khan
|
| music          = Naushad
| lyrics         = Majrooh
| cinematography = R.D. Mathur
| editing        = M.S. Shinde
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film produced and directed by Sultan Ahmed. It stars Raaj Kumar, Rajesh Khanna, Jeetendra, Waheeda Rehman, Reena Roy, Sulakshana Pandit and Amjad Khan in pivotal roles. 

==Cast==
* Raaj Kumar...Thakur Bhavani Singh
* Rajesh Khanna...Ram / Shanker
* Jeetendra...Laxman / Shiva
* Waheeda Rehman...Radha Singh
* Reena Roy...Bijli
* Sulakshana Pandit...Chanda
* Amjad Khan...Chandan Singh / Jwala Singh
* Heena Kausar...Ganga Singh

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Duniyan Chhoote Yaar Na Chhoote" Bhupinder Singh
|- 
| 2
| "Ghunghroo Toot Gaye"
| Asha Bhosle
|-
| 3
| "Tera Naam Liya Dil Tham Liya"
| Mohammed Rafi, Asha Bhosle
|- 
| 4
| "Yeh Gotey Dar Lahenga"
| Mohammed Rafi, Asha Bhosle
|-
| 5
| "Duniyan Chhoote Yaar Na Chhoote"
| Mohammed Rafi, Bhupinder Singh
|-
| 6
| "Teri Meri Hai Nazar Qatil Ki"
| Asha Bhosle
|}

==References==
 

==External links==
* 

 
 
 

 