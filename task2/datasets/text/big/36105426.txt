Annum Innum Ennum
{{Infobox film
| name           = Annum Innum Ennum
| image          = Annum Innum Ennum.jpg
| alt            =  
| caption        = 
| director       = Rajesh Nair
| producer       = Usha Rajesh
| writer         = Rajesh Nair Nishan Jishnu Jishnu Siddique Siddique Fareisa Radhika
| music          = Varun Unni
| cinematography = Prashanth Krishna
| editing        = V SaaJan
| studio         = Vaya Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Annum Innum Ennum ( ) is a 2013 Malayalam film directed and scripted by Rajesh Nair, starring Nishan (actor)|Nishan, Jishnu (actor)|Jishnu, Siddique (actor)|Siddique, Fareisa Joemmanbaks, Tashu Kaushik and Radhika (Malayalam actress)|Radhika. Produced by Usha Rajesh under the Vaya Films banner, the film was shot mostly from Trivandrum.

==Plot==
The film revolves around a young married couple and their relationship in various levels. It illustrates the dry humour on how pretentious human relations can be and how there is a quid pro quo in all relations no matter how genuine they may seem. It deals with the irony of life.

==Cast== Nishan as Roy Jishnu as Sridhar Siddique as Sidharth Menon
* Fareisa Joemmanbaks as Dolly
* Tashu Kaushik as Riya Radhika as Anjana
* Thilakan as Dr. Benjamin Bruno Rekha as Indu Ashokan
* Salim Kumar as Lopez
* Seema G. Nair as Clara
* Bijukuttan
* Rosin Jolly as Daisy
* Nobi (Vodafone Comedy stars Fame)as Pauly

==Production==
: "Annun Innum Ennum is an all-generation movie from a new generation team. Through my movie, I am trying to explain about two generations, which are always at loggerheads with each other." - Rajesh Nair 

The films cast consists mostly of youngters. Surinamese model and former Miss India-World Fareisa Joemmanbaks is making her debut through this film.  Rajesh Nair roped in Bollywood actress Tashu Kaushik for an important role. Nair had promised her a role when both met in Uganda some three years before.  About his role, Nishan, who rose to fame with Ee Adutha Kalathu says: "My role is of a guy who owns an ad agency and happy go lucky person." 

The films shooting commenced on 5 May 2012 in Trivandrum. The first schedule of filming completed on 15 May and took place entirely in the city of Trivandrum. The last schedule including songs will start on 22 June. The music for the movie was composed by Varun Unni. Although multiple tracks were initially composed for the film, only the title track "Ayyo" was used. Director Rajesh Nair commented that the other songs including "Peyyum Mazhayye" were being saved for his upcoming release Escape from Uganda which would star Rima Kallingal  

==Reception==
The film received mixed to positive reviews upon release. Veeyen of nowrunning.com rated the film 1.5/5 saying " The film finds itself lost somewhere in between being an adult comedy and a morality tale and it satisfies the enthusiasts of neither."   Indiaglitz.com gave the movie a 4.5 rating saying "The movie tries to tell a little serious things and issues about of love, sex, heartaches and fragile relationships from each ones life but is overly concerned with the male concupiscence in each of the cases" citing this as its shortcoming.  The movie is currently among the highest rated Malayalam movies on IMDB and among the few Indian movies to enjoy an Imdb rating of above 8. As of 31 January 2013; Annum Innum Ennum was rated at 8.5 on IMDB. Indicating that despite its average box office performance, the movie was well enjoyed by audiences. 
  The movies uncanny title track "Ayyo vishaada meghamayi..." was well received by music buffs and the video of the song went viral on YouTube garnering over 50,000 views within the first few days of its release. Director Rajesh Nair and composer Varun Unni are expected to join forces again in the upcoming Rima Kallingal starrer Escape from Uganda. 

==External links==
*  

==References==
 

 
 
 
 
 