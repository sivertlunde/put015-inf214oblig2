The Boys & Girls Guide to Getting Down
 
{{Infobox film
| name = The Boys & Girls Guide to Getting Down
| caption = 
| director = Paul Sapiano
| image = Boys & Girls Guide to Getting Down FilmPoster.jpeg
| producer =  Hani Selim Enrique Aguirre Sarah M. Wasserman  
| writer = Paul Sapiano 
| starring =
| music = Aminé Ramer Dan Mancini
| cinematography = Roman Jackobi 
| editing = Enrique Aguirre
| distributor = Cricket Leigh Kat Turner Steve Monroe Michael Fitzgibbon Leyla Milani Selena Fara Jeff B. Davis Juan Pacheco Brendan McNamara Davin Anderson Justin Cotta Richard Blair Pete Czechvala Navia Nguyen C.C. Sheffield Kate Gilbert Greg Studley Mario Diaz Juliette Long Cassie Hamilton Jessica Ross Billy Bonez Mitchel Evens James Bonadio Ravi Patel Dennis Haskins 
| released =  
| runtime = 92 minutes
| country = United States
| language = English
}}
The Boys & Girls Guide to Getting Down is an independent film directed by Paul Sapiano. The Boys & Girls Guide to Getting Down was the winner of five independent film awards at the Los Angeles Film Festival 2006.
 Kava Kava.

==Plot==
Part documentary, part narrative, part instructional format, The Boys & Girls Guide to Getting Down aims to teach young inexperienced youth about all things involved with "getting down", while also pointing out some of the pitfalls associated with the party lifestyle.

==Cast==
* Cricket Leigh as Bernice
* Kat Turner as Kate
* Selena Fara as Tiffany
* Natalie Taylor as Sarah
* Dominique Purdy as Jonny
* Benny Ciaramello as Orlando
* Steve Monroe as Bryce
* Michael Fitzgibbon as Andy
* Leyla Milani as Brittany
* Jeff B. Davis as Marty
* Juan Pacheco as Tony
* Brendan McNamara as Calum
* Davin Anderson as Peter
* Justin Cotta as Eugene
* Richard Blair as Daniel
* Pete Czechvala as Robert
* Navia Nguyen as June
* C.C. Sheffield as Rebecca
* Kate Gilbert as April
* Greg Studley as Greg
* Mario Diaz as John/Mario

==References==
 

==External links==
*  
*  

 
 
 

 