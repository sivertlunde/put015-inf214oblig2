The Big Steal
:This article is about a 1949 film. For the 1990 Australian film, see The Big Steal (1990 film).
{{Infobox film
| name           = The Big Steal
| image          = The Big Steal original poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Don Siegel
| producer       = Jack J. Gross
| screenplay     = Gerald Drayson Adams Daniel Mainwaring
| based on       =  
| starring       = Robert Mitchum Jane Greer William Bendix Patric Knowles Ramón Novarro
| music          = Leigh Harline
| cinematography = Harry J. Wild
| editing        = Samuel E. Beetley
| distributor    = RKO Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Big Steal is a 1949 American black-and-white film noir/comedy reteaming Out of the Past stars Robert Mitchum and Jane Greer.  The film was directed by Don Siegel, based on the short story "The Road to Carmichaels" by Richard Wormser. 

 

==Plot==
U.S. Army Lieutenant Duke Halliday (Robert Mitchum) is robbed of a $300,000 payroll by Jim Fiske (Patric Knowles). When Hallidays superior, Captain Vincent Blake (William Bendix), suspects him of having taken part in the theft, Halliday has no choice but to pursue Fiske into Mexico. Along the way, he runs into Joan Graham (Jane Greer), who is after the $2000 she loaned to her boyfriend, Fiske. The two join forces, though they are not sure at first if they can trust each other. Fiske stays one step ahead of the couple, while they are in turn chased by Blake. When Halliday is knocked down trying to stop Fiske from getting away, he comes to the attention of Police Inspector General Ortega (Ramon Novarro). Ortega lets him go after Halliday claims to be Blake (using identification he took from the captain after a brawl), but keeps an eye on him. His suspicions are confirmed when the real Blake shows up at his office for help.
 fence who offers Fiske $150,000 in untraceable bills in exchange for the payroll. The couple are captured by Setons henchmen. When Blake shows up, Halliday is initially relieved to be rescued, until he learns that Blake is actually Fiskes partner in crime.

Fiske wants to take Graham with him, but Blake makes it clear that he intends to dispose of both her and Halliday. Fiske reluctantly gives in. However, when he starts to leave, Blake shoots him in the back, explaining that his ex-partner, apparently still at large, can take the blame for the missing payroll. Halliday then points out to Seton that if Blake got rid of him too, he could give the stolen money back to the army and keep the $150,000 for himself. Taking no chances, Seton pulls a gun on Blake. When Graham creates a distraction, a fight breaks out, which Graham and Halliday win.

==Cast==
 
* Robert Mitchum as Lt. Duke Halliday
* Jane Greer as Joan Graham
* William Bendix as Capt. Vincent Blake
* Patric Knowles as Jim Fiske
* Ramon Novarro as Inspector General Ortega
* Don Alvarado as Lt. Ruiz
* John Qualen as Julius Seton

==Production==
The movie was filmed in Los Angeles and on location in Tehuacán, Puebla, Mexico.

==Reception==

===Critical response===
Channel 4 film reviews describes the movie as, "Sparkling dialogues, fast-paced chases and the occasional twist make this an at first somewhat confusing but ultimately hugely entertaining film." 

Hal Erickson writing for Allmovie calls the film "tautly directed by Don Siegel, who manages to pack plenty of twists and turns into the films crowded 71 minutes." 

==References==

===Notes===
 

===Additional sources===
 
#   
 

==External links==
*  
*  
*  
*  
*   at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 
 
 
 
 