Heartbeat (2014 film)
 
{{Infobox film
| name           = Heartbeat
| image          = 
| caption        = 
| director       = Andrea Dorfman
| producer       = 
| writer         = Andrea Dorfman
| starring       = Tanya Davis
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Canada
| language       = English
| budget         = 
}}

Heartbeat is a 2014 Canadian drama film written and directed by Andrea Dorfman. It was screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.    The film stars poet and musician Tanya Davis as Justine, an unfulfilled advertising copywriter who dreams of becoming a musician but struggles with stage fright. 

==Plot==
After Justine (Tanya Davis) faints at an open mic performance out of stage fright she gives up music for a while.

When her friend, Lorna, gives birth she decides that she too wants a baby, but when she suggests it to Ben, (Stewart Legere), the ex she is still sleeping with, he ends things for good, later informing her that he will be leaving town. Inspired by the breakup Justine begins playing her guitar again. She spots a girl playing drums in the window of her local music shop but while the two acknowledge each other they do not talk. 

Justine meets the girl again when a frisbee lands on her windshield belonging to the girl, Ruby (Stephanie Clattenburg). Ruby invites Justine to one of her performances and afterwards Justine tries to play one of her songs for Ruby but is overcome with fear. Ruby, however, refuses to let her give up and Justine finally is able to play the song for her. The two begin meeting regularly to play in the music shop window performing in front of passersby.  

Justine develops a crush on Ruby and the two end up sleeping together. Afterwards Ruby tells Justine that she has girlfriend. Heartbroken, Justine returns home where she rearranges the postcards she has been getting from Ben into a portrait which form a cabin she recognizes. Justine goes to Ben but when she tries to get back together with him he tells her he doesnt want to be with her.

After returning home Justine and Ruby talk and Ruby apologizes for not telling Justine about her girlfriend. Justine forgives her and tells her that she wants to play music with her. She quits her job and goes to work at the local music store. 

In order to raise funds for her roof Justine and Ruby organize a concert at her house. Before they play Justine calls Ben and dedicates her first song to him. 
==Cast==
* Tanya Davis as Justine
* Stephanie Clattenburg as Ruby
* Kristin Langille as Lorna Glen Matthews as Drew
* Jackie Torrens as Louise
* Stewart Legere as Ben
* Jim Henman as Arlo
* Naomi Blackhall-Butler as Maureen

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 