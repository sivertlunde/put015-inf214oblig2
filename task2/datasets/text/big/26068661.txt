Bandido (2004 film)
{{Infobox film
| name           = Bandido
| image          = Bandido2004.jpg
| image_size     =
| caption        = Roger Christian
| writer         = Scott Duncan
| narrator       = Carlos Gallardo
| music          = Mike Southon
| editing        =
| distributor    =
| released       = 2004
| runtime        = 95 mins.
| country        = Mexico United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} Carlos Gallardo Roger Christian.  The story revolves around the CIA framing skilled thief Max Cruz aka "Bandido" (Gallardo) in order to gain his cooperation in helping them recover intelligence data that was stolen by Beno Gildemontes (Kim Coates), a Mexican Crime lord.

==Cast== Carlos Gallardo	... 	Max Cruz or Bandido
*Edy Arellano	... 	Armas
*Michel Bos	... 	CIA Agent
*Kim Coates	... 	Beno
*Matt Craven	... 	Fletcher
*Marintia Escobedo	... 	Scar
*Angie Everhart	... 	Natalie
*Ana La Salvia	... 	Sofia
*Karyme Lozano	... 	Rosalia (as Carime Lozano)
*Veronica Segura	... 	Delgado
*Manuel Vela	... 	Peña
*Ernesto Yáñez	... 	Quintana

==Release and reception==
The film was released December 10, 2004 by Innovation Film Group.  It received generally negative reviews.  Carlos Gallardo came up with the idea for the film directly after his involvement with cult classic El Mariachi.  He created that movie for a mere $7,000. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 