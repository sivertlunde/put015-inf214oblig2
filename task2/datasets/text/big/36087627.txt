Do Matwale
{{Infobox film
| name           = Do Matwale
| image          = DoMatwalefilm.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional Poster
| director       = Ajay Kashyap
| producer       = Pushpa S. Choudhary	
| writer         = 
| screenplay     = 
| story          = Ajay Kashyap
| based on       =  
| narrator       =  Sonam Shilpa Shirodkar
| music          = Laxmikant-Pyarelal
| cinematography = K.V. Ramanna
| editing        = Waman B. Bhosle Gurudutt Shirali
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Sonam and Shilpa Shirodkar in pivotal roles.

==Cast==
* Sanjay Dutt as Ajay
* Chunky Pandey as Amar  Sonam as Sonu
* Shilpa Shirodkar as Dr. Pooja
* Kader Khan as Gorakh Nath
* Gulshan Grover as Pyaremohan
* Anjana Mumtaz as Sarita G. Nath
* Sushma Seth as Amars mother
* Shakti Kapoor as Sampath (ward boy) / Champath / Ganpath

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Anarkali Tere Sar Ki Kasam"
| Jolly Mukherjee, Sudesh Bhosle, Alka Yagnik, Arun
|-
| 2
| "Ho Jayegi Kahau Tu"
| Mohammed Aziz, Alpna Deshpande
|-
| 3
| "Kar Gayi Muhalle Mein Halla"
| Amit Kumar
|-
| 4
| "Koi Hai Hum Hain"
| Kavita Krishnamurthy
|-
| 5
| "Main Aaj Bolta Hoon"
| Mohammed Aziz, Kavita Krishnamurthy
|}

==External links==
* 

 
 
 
 


 