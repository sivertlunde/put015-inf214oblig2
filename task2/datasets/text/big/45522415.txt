Evangeline (2013 film)
{{Infobox film
| name           = Evangeline
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Karen Lam
| producers       = Karen Lam and Karen Wong
| writer         = Karen Lam
| screenplay     = 
| story          = 
| based on       =  
| starring       = Kat de Lieva and Richard Harmon
| narrator       =  
| music          = Patric Caird
| cinematography = Michael Balfry
| editing        = Jeanne Slater
| studio         = Opiate Pictures
| distributor    = Uncorkd Entertainment (USA)
| released       =  
| runtime        = 85 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          =  
}}
 thriller film, which was written and directed by Karen Lam. 

== Cast ==
* Kat de Lieva as Evangeline Pullman
* Richard Harmon as Michael Konner
* Mayumi Yoshida as Shannon  David Lewis as Mr. K 
* Kelvin Redvers as Billy  John Shaw as Jim 
* Nelson Leis as Dee 
* Dejan Loyola as Ali 
* Madison Smith as Mitch 
* Anthony Shim as Mark
* Natalie Grace as Molly 
* Ella Kosor as Clara 
* Lucy Harvey as Brandice 

== Production == Pickton murders in the Vancouver area, and the Highway of Tears murders in northern British Columbia.     

== Reception ==
The film is described as a supernatural revenge fantasy, about a university student (played by   (retributive justice) versus turning the other cheek.  

== Awards ==
Evangeline received nine nominations for the Leo Awards, winning two.  The film also won two awards, Best Director and Best Cinematography, at the 2013 Blood in the Snow Canadian Film Festival. 

== Release ==
The film premiered on September 28, 2013 in the Monsters of Film festival, in Stockholm,  and opened the 2014 Vancouver International Women in Film Festival.    The film was released in the United States as Direct to video production, on 8 May 2015 on Video on Demand  and on June 9, 2015 on DVD and Blu-Ray. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 