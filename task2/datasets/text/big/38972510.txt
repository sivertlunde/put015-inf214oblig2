Sant Tukaram (film)
{{Infobox film
| name           = Sant Tukaram
| image          = Sant Tukaram.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =  Film poster
| director       = Vishnupant Govind Damle Sheikh Fattelal
| producer       = 
| writer         = Shivram Washikar
| screenplay     = 
| story          = 
| based on       = Sant Tukaram
| narrator       = 
| starring       = Vishnupant Pagnis Gauri B. Nandrekar
| music          = Keshavrao Bhole
| cinematography = V. Avadho
| editing        = 
| studio         = Prabhat Film Company
| distributor    = 
| released       = 12 December 1936 
| runtime        = 131 minutes
| country        = India Marathi
| budget         = 
| gross          = 
}} Marathi film, saint and spiritual poet of the Bhakti movement in India. The film was directed by Vishnupant Govind Damle and Sheikh Fattelal and featured Vishnupant Pagnis in the lead role of the saint.

Sant Tukaram is revered as a great Indian film. It was the first Indian film to receive international recognition. It was adjudged one of the three best films of the world at Venice Film Festival and was showcased in other international film festivals. It was a runaway success and broke the box office records, by being the first Indian film to run in a single theatre for more than a year. It was Prabhats and Pagniss most famous film and became archetype of a devotional film.

==Plot==
Set in 17th century Dehu, Maharashtra, Tukaram - a farmer and grocer - loses interest in the material world after losing his first wife and child in a famine. He neglects his worldly duties to his second wife Jijai (Awali) and their two children. The Brahmin Salomalo is jealous of the religious following and popularity of the shudra (a caste lower than the Brahmin) saint. He claims that Tukaram stole his verses, and questions a shudras right to examine the Hindu scriptures, Vedas, a right reserved for Brahmins (priest caste). Pandit Rameshwar Shastri, a learned Brahmin scholar and religious authority, is invited by Salomalo to examine his claims, which he backs by fabricating evidence. Shastri orders that Tukaram immerse his works into the river and never discuss religion in public. Tukaram complies and sits on a fast on the river bank with his family for thirteen days, when God returns him his works. Shastri falls seriously sick, which he interprets as divine retribution and becomes Tukarams devotee. Salomalo then approaches the reigning king Shivaji, the founder of the Maratha Empire. When Shivaji tests Tukaram by offering material gifts, the saint refuses and in turn, Shivaji becomes a disciple too. Salomalo then informs the Mughals, Shivajis enemies that the king was in town, but God protects Shivaji at behest of Tukaram, when the Mughals come to Dehu. His saintliness brought hordes of people from different regions of the state offering worship at his feet and also offering him huge gifts which could enrich him but he refuses to accept any kind of rewards. When Tukarams work is done on earth, God comes to take him to heaven. Tukaram asks his wife to join, but she refuses as she has to look after the children.     

The film also depicts various miracles of Tukaram like God visiting him, an army being created from Vithobas image, showering of grains from the sky, curing a sick boy, retrieving his works from the river which was intentionally thrown there, and in the end, going to the heaven in a celestial chariot.   Dwyer p. 78–9 

==Cast==
 

The cast included:   
*Vishnupant Pagnis as Tukaram, the 17th century saint from the Varkari sect of Hinduism
*Gauri as Jijai (Awali   ), Tukarams second wife
*Sri Bhagwat as Salomalo, the village priest and chief antagonist
*B. Nandrekar 
*Shankar Kulkarni
*Kusum Bhagwat 
*Shanta Majumdar 
*Master Chhotu 
*Pandit Damle
*B. Nandrekar
*Shankar Kulkarni

==Characters and casting==
The film is based on the life of Tukaram, one of most revered saints of Maharashtra and a devotee of the god Vithoba (the patron of the Varkaris), who propagated his dogma of secular living with no distinction of class, creed and gender. He wrote religious poetry in Marathi language|Marathi, the vernacular language of Maharashtra, which had mass appeal. It touched the heart strings of the common people, particularly those who were downtrodden or oppressed by the Brahminical hegemony. His preaching, rendered in rhythmic poetry, thus had great mass appeal and was considered the beginning of an “emancipatory movement in the country.”  Tukaram through his devotional songs conveys a message to the people that offering prayers to God sincerely in one’s own humble way  without resorting to Vedic rituals was also a way of worship.  His devotional verses were selected for translation by UNESCO. 

Pagnis was a kirtankar, a minstrel who sang devotional songs.  Before being cast by V. Shantaram of the producer company Prabhat as Tukaram, Pagnis was a specialist actor in donning female roles in plays by his theatre group Swadesh Hitinchal Mandali. His selection was initially resented by directors Damle and Fattelal as they felt uncomfortable with his effeminate mannerisms. But it proved to be a blessing as Pagnis adopted his feminine style to match the saintly role of Tukaram to the "T"; a saint whose verses reflect an urge towards God as a woman pines for her lover. Pagnis also did a dedicatory visit to the samadhi  (memorial shrine) of Tukaram in Dehu, before starting shooting for the film. After the film was made, film critiques stated that Pagnis was ideal for the role as his face was an “iconic but also indexical of both actor and character.” Dwyer p. 77 

 

Availabai, Tukarams second wife (although the film makes no mention of his first wife), is portrayed as a hard-working village woman with a practical earthy sense. She is a practical woman who has to endure poverty because her husband is a pious and unworldly simpleton. She is often impatient because of this, but she understands and respects her husband for his saintly goodness, and also defers to him and acts according to his wishes at the end of every issue that arises. Though devoted to her husband, she is irritated by his devotional ways, but nevertheless always obeys Tukarams unworldly, charitable wishes. She frequently loses her temper on Tukaram for ignoring the family needs to feed their children and their upkeep. In a famous scene, when their son is ill and there is no money for treatment, and Tukaram expresses the view that prayer and faith will see them through the crisis, an infuriated Availabai drags her son to the temple by one hand and holds a chappal (sandal) in her other hand to threaten Tukarams patron God Vithoba with a beating for bringing her family into dire straits. A miracle follows and the son is cured instantly in the precincts of the temple.

Availabai routinely admonishes her husband, telling him that singing bhajans (devotional songs) alone will not sustain his family. However, she also maintains the dignity of her wifely obligations to her husband. In one of the most touching scenes of the film, when Tukaram wanders in the forest to praise God, (the song shown is vrikshavalli amha), Availabai follows him with a basket of food so that he will eat in time. She explains that just as Tukaram is following his God in the path dharma through the forest of life, she also is following her personal God (her husband Tukaram) in the path of dharma through this forest. She then playfully asks if his God is kinder or hers; does his God sit down to dine with his devotee the way her God is eating now? In the final scenes of the film, Tukaram has a premonition that he will be transported in his mortal body to heaven by God. He reveals this to Availabai and asks her to accompany him. Availabai receives this priceless communication with her usual caustic derision and goes about her duties, cooking poli (Chapati) for dinner. A final miracle ensues and a heavenly chariot indeed appears to convey Tukaram to heaven. Availabai runs out of her kitchen and is blessed with a vision of her husband being flown from earth to the skies on this chariot. This is the end of the film.  Dwyer p. 78 

Gauri, who played the part of Availabai, was not a professional actress. She started as a sweeper in Prabhat and worked as an extra in films, finally graduating to the main role segment. 

Salomalo is a bigoted Brahmin village priest and the chief adversary of Tukaram, who tries to ostracise Tukaram time and again.
Salomalo, as the aspirant to the status of a great devotional poet (like Tukaram), is jealous of Tukaram and harasses him frequently. Salomalos verses are in refined language, incomprehensible to unread villagers, contrasting with Tukarams simple poetry, which quickly becomes popular.  Salomalo is also shown to lack dignity, moving in awry motion as in a tamasha, against Tukarams calm, composed nature. Salomalos goal is not to surpass Tukaram in scholarship, but just to ruin him. He ridicules Tukarams devotional poetry and parodies it into a vulgar version. He visits courtesans, a sign of "spiritual bankruptcy" and also the dancing girls performances adds glamour to the film to sustain the audiences film experience. 

Shastri, a learned Brahmin, is a contrast to Salomalo, an orthodox village Brahmin. Shivaji, the king – a symbol of worldly power, being a disciple of the saint satisfies Availabais practical mind. Shivaji is antipole of Tukarams lack of worldliness and Salomalos unjust jealousy. The film captures the historical fact of the union of Shivaji and the Varkari sect. 

==Production==
 

Sant Tukaram was directed by Vishnupant Govind Damle and Sheikh Fattelal. The script was written by Shivram Washikar. It was produced at the Prabhat Film Company at Pune owned by V. Shantaram, a notable and internationally acclaimed film maker of India. The film was made in Marathi language. The running time is 131 minutes.
 hagiographical telling, rather than complete historical accuracy. Dwyer p. 91 

Damle and Fattelal created the settings to ensure that the features of the actors were accentuated distinctly in the film by having minimum background settings and scenes, which was termed as "compositional style”.    The film was made in Neorealism (art)|neo-realistic style (a style which came into vogue much later in foreign films) and provided telling performances from all the artists but the most impressive, which won the hearts of the people, was that of Jijai, wife of Tukaram played by Gauri.   

The filming is done in traditional style. After the title cards are presented, a song dedicated to Vithoba, seen as a black stone image, is presented. The song which begins with the title cards is also continued. The meditative shot of Tukaram gives a Bhakti ethos which  is continued for about 2 minutes. This is followed by the saintly presence of Tukaram in a traditional pose, cross legged with folded hands and holding a musical instrument and with religious markings on his face. Sitting on the ground in deep meditation for a while, his singing merges with the background song. The song is one of the Tukarams 4,000 verses and is aptly fitted with the devotional worship of Vithoba. The songs first stanza is Panduranga Dhyani, Panduranga mani (meaning "I meditate on the figure of Panduranaga (Vithoba); his thoughts fill my mind"). This is an aradhana (prayer) shot where the saint, the God and the audience are brought to the same ethereal plane of worship. The scene is set by the directors in a very "scopic aspect of the worship", the mise en scène. In the scene that follows, the song is continued but is now sung in a theatrical style (Sangeet Natak style) by Tukarams arch rival, the Brahmin priest Salomalo, in a temple setting. Salomalo claims that the song is his own composition and not that of Tukaram. With this scene, the story of the film starts with clever editing of scenes in which the two adversaries exchange claims to the song, in a shot reverse shot technique.   

==Music==
Most of the songs of the film were Tukarams own verses, except one song by Shantaram Athavale, written so realistically in Tukarams style of writing that confused scholars contacted the film makers to know more about the source. 

The rendering of music had a telling impact on the audience, as the narration was poetic and devotional. The music score was provided by Keshavrao Bhole. He had moved to Prabhat Films with an impressive record of giving music scores for 14 films, in many languages. His music compositions adopted innovations by introducing piano, Hawaian guitar, and violin. This blending is stated to have given a ”catholic, modern and cerebral outlook to his film music” for his devotional films. For Sant Tukaram, Bhole followed the traditional music style of the Varkari sect, which turned out to be innovative without sacrificing the traditional originality of the bhakti music.    Damle was also an accomplished singer, and he rendered many songs in the film.

==Release and reception==
The film was first released on 12 December 1936 at the Central Cinema hall in Bombay, now Mumbai. 

Sant Tukaram was the first Indian film to receive international recognition.  The film was screened at the 1937 Venice Film Festival (the 5th Mostra Internazionale dArte Cinematographica) and was the first Indian entry to get a screening at an international film festival. The film was adjudged as one of the three best films of the World, the other two being Maria Nover of Hungary and Flying Doctor from Australia.    However, the citation document was lost. It was reportedly discovered in 1974 by Sunny Joseph, a cinematographer, from Thiruvananthapuram, in a garbage can on Law College road in Pune.  Joseph restored it to the National Film Archive of India, Pune for safekeeping on 23 March 2004.  In 1982, the film made a very honoured re-entry on the occasion of the celebration of the 50th Anniversary of the Mostra Internazionale del Cinema (La Biennale di Venezia), along with another Prabhat film Duniya Na Mane; when best films from previous festivals were screened. 

The film was also selected to be screened at various other international film festivals.  It was also viewed by the then Maharaja of Mysore and Lady Linlithgow, wife of Victor Hope, 2nd Marquess of Linlithgow, the then Viceroy of India. A special screening was also arranged for the foreign consulates in Mumbai. 

The film is described to emphasize the Gandhian philosophy of non-violence.    The Indian Motion Pictures Congress in its 1939 session praised the film as extolling the ethos of nationalism, Gandhian ideals and devotional bhakti movement. Dwyer p. 69 

Sant Tukaram broke box office records by being the first Indian film to run in a single theatre for more than a year.  It was then a runaway success and an all-time record for the time, as it ran for 57 weeks continually. The film was seen by 6 million people in Maharashtra alone.  It drew hoards of people from across villages wherever it was shown. Filmindia (1941) records a screening in a village with a population of 300 having a crowd of 1,500 for the film. Dwyer p. 79  Film scholar and professor on Indian cinema Gayatri Chatterjee describes how Sant Tukaram has a spiritual effect on audience of all ages, sections of society, Indian or foreign. She specially narrates about the experience of a Canadian Muslim student, who had a spiritual experience by watching this film. 

On the eve of the celebration of 100 years of Indian cinema, Shyam Benegal, a doyen among film makers of India, considers this classic film, set in the true Maharashtrian rural cultural setting, when the Bhakti movement was at its peak frenzy in the state, as one of his five favourite films.    Benegal does not consider this "somewhat primitively made film" a "work of cinematic craftsmanship", however regards it "an extraordinary achievement", as it was "culturally true". The dialogues between Tukaram and Jijai are correctly catching the nuances of their relationship.  Critics like Dwyer and Shahani echo similar sentiments about its "timeless quality" and cultural accuracy. 

Sant Tukaram is regarded as one of the greatest Indian films.    Dwyer p. 71     It became Prabhats "most famous and acclaimed" film. Dwyer p. 76  The film is regarded as the archetypal classic of sant film genre,   and also devotional films in general.  Pagnis, became immortal in the role of Tukaram and also performed as the saint in live performances in his later life.  Even, later portraits of Tukaram used for worship resemble Pagniss face and his attire in the film.   

The film is an amalgamation of asymmetrical character of Tukaram with symmetrical character of his wife Jijai, a practical reality is the way Lyle Peason, a film critique explains the film. He extends this observation with a further saying that "Sant Tukaram does not give out concepts but...demonstrates their function in actual life...that a realistic story with songs and magic."    The film was described as a "human document of great value."  A simple theme was presented effectively and with appealing emotions by the main actors of the film. 

==References==
 

*  
* {{cite web|url= http://www.academia.edu/368220/Paper_read_on_the_Prabhat_Film_Sant_Tukaram_at_the_Sahitya_Academy_New_Delhi_to_celebrate_the_400th_birth_anniversary_of_the_saint_poet_Tukaram| title =IN THE BEGINNING WAS THE WORD: On sound and poetry in the film Sant Tukaram (1936)}}
*  
*  
*  

==External links==
 
* 
*   
* 

 
 
 
 
 
 
 
 
 