The Color of Lies
{{Infobox film
| name           = The Color of Lies
| image          = The Color of Lies.jpg
| alt            = 
| caption        = DVD cover
| director       = Claude Chabrol
| producer       = Marin Karmitz
| screenplay     = Claude Chabrol Odile Barski
| starring       = Sandrine Bonnaire Jacques Gamblin
| music          = Matthieu Chabrol   
| cinematography = Eduardo Serra
| editing        = Monique Fardoulis Canal Diffusion France 2 Cinéma MK2 Productions
| distributor    = MK2 Diffusion 
| released       =  
| runtime        = 111 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $111,143 (USA)
}}
The Color of Lies is a 1999 film co-written and directed by Claude Chabrol. Its title in French is Au cœur du mensonge (literally At the heart of the lie). The film was entered into the 49th Berlin International Film Festival.   

==Plot== de Caunes), Ogier and Simsolo).

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Sandrine Bonnaire || Vivianne Sterne  
|-
| Jacques Gamblin || René Sterne  
|-
| Antoine de Caunes ||Germain-Roland Desmot 
|-
| Valeria Bruni Tedeschi || Frédérique Lesage  
|-
| Bernard Verley || Inspecteur Loudun  
|-
| Bulle Ogier  ||  Évelyne Bordier  
|-
| Pierre Martot ||  Regis Marchal  
|-
| Adrienne Pauly || Anna
|}

==Critical reception==
The film received favorable reviews.

Michael Thomson of BBC Films:
 

Michael Atkinson of The Village Voice:
 

Christopher Null of AMC (TV channel)|Filmcritic.com gave the film a good review but had only one issue of contention:
 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 