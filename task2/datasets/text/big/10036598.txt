ESPY (film)
{{Infobox Film 
  | name = ESPY
  | image = 
  | caption = 
  | director =  Jun Fukuda
  | producer = Fumio Tanaka Tomoyuki Tanaka
  | writer = Sakyo Komatsu(novel) Ei Ogawa(screenplay)
  | starring = Hiroshi Fujioka Kaoru Yumi Masao Kusakari Eiji Okada Tomisaburo Wakayama
  | music = Masaaki Hirao Kiyohiko Ozaki
  | cinematography = Shôji Ueda
  | editing = Michiko Ikeda
  | distributor = Toho 
  | released = December 28, 1974 (Japan)
  | runtime = 94 minutes
  | country = Japan Japanese
  }}
  is a 1974 film based on the novel of the same name by Sakyo Komatsu. The film was directed by Jun Fukuda from a screenplay by Ei Ogawa.  It stars Masao Kusakari, Kaoru Yumi, Tomisaburo Wakayama, and Eiji Okada.  The film was released to U.S. television under the title E.S.P./Spy, which remained onscreen for its VHS release under the international title, ESPY. 

==Plot==
The film deals with the recruitment of racecar driver Jiro Miki (Kusakari) and his dog, Cheetah, to a group of people who use Extra-sensory perception|ESP, psychokinesis, and other special mental abilities to fight crime.  The major villain is Wolf (Wakayama), whose behavior stems as a result of the prejudicial murder of his father over his fathers psychic abilities.

==Cast==
*Yoshio Tamura  .........  Hiroshi Fujioka
*Maria Harada  .........  Kaoru Yumi
*Jirou Miki  ........  Masao Kusakari
*Houjou  ........  Yuuzou Kayama
*Wolf  .......  Tomisaburo Wakayama
*Gorou Tatsumi  ...... Katsumasa Uchida Steve Green
*Sarabad  ........ Eiji Okada
*Teraoka  ........ Gorou Mutsumi
*Godnof  .......  Jimmy Show
*United Nations negotiation committee member Ａ  ........ Roger Woode
*United Nations negotiation committee member Ｂ  ........ Annest Harness Andrew Hughes
*Abdullah  ......... Willie Dorsey
*Bohl  .........  Hatsuo Yamaya  
*Gyaku-ESPY（Counter ESPY） A  .......... Ralph Jesser 
*Gyaku-ESPY（Counter ESPY） B  ......... Gelmal Riner  Franz Gruber   
*Gyaku-ESPY（Counter ESPY） D  ......... Burt Johansson
*Secretary prime minister Baltonia  ......... Jan Dickene
*Prime minister guards official Baltonia ..........Gunter Grave
*Captain special passenger plane  ........ Robert Dunham 
*Special passenger plane vice-captain  ......... Henry
*Miki of boyhood  .........  Julie Club 
*Judy   ........ Luna Takamura
*Judy of girlhood  ........ Kelly Vancis

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 
 