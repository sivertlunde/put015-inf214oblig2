Teatertosset
 
{{Infobox film
| name           = Teatertosset
| image          = Teatertosset.jpg
| caption        = DVD cover
| director       = Alice OFredericks
| producer       = Henning Karmark
| writer         = Paul Sarauw
| starring       = Marguerite Viby
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Teatertosset is a 1944 Danish family film directed by Alice OFredericks.

==Cast==
* Marguerite Viby as Dorrit Madsen
* Hans Kurt as Knud Andersen
* Karl Gustav Ahlefeldt as Ole Vang
* Johannes Meyer as Hr. Fuglsang
* Else Jarlbak as Nanna Sten
* Sigrid Horne-Rasmussen as Gerda
* Preben Neergaard as Harald
* Erik Sjøgreen as Jens
* Helga Frier as Fru Vildemose Henry Nielsen as Regissør Lund
* Knud Heglund as Instruktør Iversen
* Else Colber as Vera
* Ib Schønberg as Teaterdirektør Brummer

==External links==
* 

 

 
 
 
 
 
 


 