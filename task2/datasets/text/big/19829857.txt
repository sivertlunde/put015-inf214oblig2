Bangaru Babu (2009 film)
{{Infobox film
| name           = Bangaru Babu
| image          =
| caption        =
| writer         = Madhukuri Raja  
| story          = Dasari Narayana Rao
| screenplay     = Dasari Narayana Rao Shashank Gowri Munjal
| director       = Jonnalagadda Srinivas 
| producer       = K. Ramakrishna Prasad
| studio         = Swobhagya Media Limited
| music          = M. M. Srilekha 
| cinematography = Ch. Ramana Raju
| editing        = B. Krishanam Raju
| released       =  
| runtime        = 2:34:17
| country        =   India
| language       = Telugu
| budget         = 
}}

  Telugu film produced by K.Ramakrishna Prasad on Swobhagya Media Limited banner, story & screenplay by Dasari Narayana Rao, directed by Jonnalagadda Srinivas. Starring Jagapathi Babu, Meera Jasmine, Shashank (actor)|Shashank, Gowri Munjal in lead roles and music composed by M. M. Srilekha. The film recorded as flop at box office. Most of the scenes in the film has been shot extensively in Jammu & Kashmir.

==Plot==
Hari (Shashank (actor)|Shashank) is a junkie with enough rage to cause heat waves on Pluto. He turned out this way because hes never seen his dad, who unceremoniously ditched his unmarried mother, who then died some years later. Now Haris going around getting doped up with ganja etc., and beating up lecturers. His college wants to expel him, but Ravindra (Jagapati Babu), chairman of the board, declines to do so. He wants to reform Hari and make him top the college. A de-addiction session makes Hari behave like a bull that just saw Govindas pants, but Ravi plods on.

Meanwhile, in a soggy romantic track, Ravi gets hitched with Meera (Meera Jasmine) when he sees her at a traffic light. Now, Meeras complaint is that Ravi keeps dashing away to attend to Hari. The last straw is when Ravi bunks his engagement to be with him. Even Ravis family is dead against such behaviour. They dont want him, the chairman of a few dozen companies, getting so close to a wastrel. Maybe because Hari, a supporting character, is eclipsing him with his acting skills.

Now Ravi is forced to tell everyone his secret - Haris father is none other than Ravis deceased father. Apparently, the dying gentleman (Murali Mohan) had revealed many secrets in a VCD which he gave Ravi. At this, his mother (Jayasudha) faints. How could her husband do this? He gets away with being in a garlanded photograph and a couple of flashback scenes, whereas she is stuck with this smelly script.

However, she decides she can do nothing but join Ravi in paying for her husbands mistake. She, Ravi and her daughter (Hema (actress)|Hema) go all out to help the other family in every way they can. But Ravis elder brother (Sonu Sood) is still cheesed off at the whole thing. He just delivered an all-time blockbuster, and now, this? He makes sure Hari gets to know the murky truth, albeit in a very tasteless way.   

==Cast==
{{columns-list|3|
* Jagapathi Babu as Ravindra
* Meera Jasmine as Meera
* Sayaji Shinde
* Sonu Sood
* Murali Mohan
* Jayasudha Shashank as Hari
* Gowri Munjal
* Jaya Prakash Reddy
* Dharmavarapu Subramanyam
* M. S. Narayana
* L. B. Sriram
* Amith
* Raja Sridhar
* Ping Pong Surya Kondavalasa
* Lakshmipathi
* Vizag Prasad
* Melkoti
* Malladi Raghava
* Narayana Rao
* Navabharat Balaji
* Gundu Sudarshan
* Ambati Srinivas
* Fish Venkat
* Uttej Jenny
* Jyothi Lakshmi
* Sudha Hema
* Sudeepa Pinky
* Rama Prabha
* Vinaya Prasad
* Jhansi
* Padma Jayanth
* Srilalitha
* Monica Chowdary
}}

==Soundtrack==
{{Infobox album
| Name        = Bangaru Babu
| Tagline     = 
| Type        = film
| Artist      = M. M. Srilekha 
| Cover       = 
| Released    = 2009
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:11
| Label       = Mayuri Audio 
| Producer    = M. M. Srilekha 
| Reviews     = 
| Last album  = Aadi Vishnu   (2008)
| This album  = Bangaru Babu   (2008)  A Aa E Ee   2009 
}}

Music composed by M. M. Srilekha. Music released on Mayuri Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:11
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Gulabi Puvvu Chandrabose
| extra1  = Tippu (singer)|Tippu,MM Srilekha 
| length1 = 3:54

| title2  = Ningiloni
| lyrics2 = Chinnicharan
| extra2  = Hariharan (singer)|Hariharan,Nitya Santhoshini
| length2 = 4:34

| title3  = Galu Galu Galu
| lyrics3 = Chandrabose
| extra3  = Syrona
| length3 = 4:43

| title4  = 16 Va Eata
| lyrics4 = Chinnicharan Chitra
| length4 = 4:00

| title5  = Sannani Nadumuki
| lyrics5 = Chandrabose SP Balu,MM Srilekha
| length5 = 4:12

| title6  = Devude Navvedure  
| lyrics6 = Suddala Ashok Teja  
| extra6  = Vandemataram Srinivas
| length6 = 3:48
}}

==See also==
* Telugu films of 2009

==References==
 

==External links==
*  

 
 
 


 