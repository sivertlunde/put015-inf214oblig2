Whispers of Dead Zone
{{Infobox film
| name           = Whispers of Dead Zone
| image          = WODZTurkPosterFirst.jpg
| alt            = 
| caption        = Original film poster
| director       = Fırat Çağrı Beyaz
| producer       = Recep Süleyman Önüt, Metin Gönen
| writer         = Metin Gönen, Fırat Çağrı Beyaz
| starring       = Mete Pere, Leman Özdogan , and Sevcan Çerkez
| music          = Konstantinos Kittou
| cinematography = Ahmet Gençünal
| editing        = Fırat Çağrı Beyaz, Taner Sarf
| studio         = ParadoksFilm, Kans Film
| released       =  
| runtime        = 86 minutes
| country        = Turkey
| language       = Turkish
| budget         =  400,000    
| gross          =
}}
Whispers of Dead Zone ( ) is a 2012 drama in Turkish-French co-production,  directed by Fırat Çağrı Beyaz.   The film premiered at the Istanbul International Film Festival. 

== Synopsis ==
Mete (Mete Pere) is a successful young television director from Northern Cyprus that has settled down in Istanbul, yet doesnt quite feel like he belongs there. He returns to his homeland of Cyprus with the intent to make a documentary. While on the island, Mete feels the division between the Greek and Turkish cultures that exist within him, especially while visiting the graves of his ex-girlfriend, mother, and old friends. He also slowly begins to build a relationship with Rüyam (Leman Özdogan ), a beautiful woman that he meets on the island while taking photographs. Their meeting forces him to choose between leaving the island and his homeland or looking beyond his problems with where he belongs and the disintegration of his being. 

 
File:Director.jpg 
File:Writers.png  
 

== Cast ==
*Mete Pere as Mete
*Leman Özdogan as Rüyam
*Sevcan Çerkez as Nurdan
*Toprak Altay
*Cenk Gürcağ
*Haluk Ramon Serhun
*Sahir Cacci
*Başak Ekenoğlu
*Kıvanç Giritli
*Şerife Akman
*Emre Yazgın

==Photos From The Movie Set==
 
File:Photos From the Set.png
File:Whispersofdeadzone 1.jpg
File:Director.jpg
File:Whispersofdeadzone 1.jpg
File:Whispersofdeadzone 3.jpg
File:Whispersofdeadzone 4.jpg
File:Whispersofdeadzone 5.jpg
File:Whispersofdeadzone 6.jpg
File:Whispersofdeadzone 7.jpg
File:Whispersofdeadzone 8.jpg
File:Whispersofdeadzone 9.jpg
File:Whispersofdeadzone 10.jpg
File:Whispersofdeadzone 11.jpg
File:Whispersofdeadzone 13.jpg
 
 Photos From The Movie Set 2011    

== Photos From The Movie ==
 
File:Whispers of Dead Zone.jpg
File:Pics from Whispers of Dead Zone Movie (2012).jpg
File:Whispers of Dead Zone Movie (2012).jpg
File:Whispersof Dead Zone from Movie 1.jpg
File:Whispersof Dead Zone from Movie 2.jpg
File:Whispersof Dead Zone from Movie 3.jpg
File:Whispersof Dead Zone from Movie 4.jpg
File:Whispersof Dead Zone from Movie 5.jpg
File:Whispersof Dead Zone from Movie 6.jpg
File:Whispersof Dead Zone from Movie 7.jpg
File:Whispersof Dead Zone from Movie 8.jpg
File:Whispersof Dead Zone from Movie 9.jpg
File:Whispersof Dead Zone from Movie 10.jpg
File:Whispersof Dead Zone from Movie 11.jpg
File:Whispersof Dead Zone from Movie 12.jpg
File:Whispersof Dead Zone from Movie 13.jpg
File:Whispersof Dead Zone from Movie 14.jpg
File:Whispersof Dead Zone from Movie 15.jpg
File:Whispersof Dead Zone from Movie 16.jpg

File:Réconciliation dans la séparation-1.jpg
File:Réconciliation dans la séparation-2.png
 

Photos From The Movie  

==Development== Ministry of Culture and Tourism, helping its promotion in international festivals.   
 Kyrenia and Karpaz districts,  and was Beyazs directing debut.   

The film has already been sold for television broadcast and online streaming. 

== Festivals==
Ölü Bölgeden Fısıltılar has been picked as an official selection at the following film festivals:
 2012 International Istanbul Film Festival  
*2012 Izmir International Film Festival 
*2012 Sinemardin Mardin International Film Festival 
*2012 Amed International Film Festival 

==References==
 

== External links==
* 
*  
*  
*  

 
 
 
 
 