Apeksha
{{Infobox film
| name           = Apeksha
| director       = H.D. Premaratne
| producer       = Walter Rodrigo
| screenplay     = H.D. Premaratne
| starring       = Malani Fonseka, Amarasiri Kalansuriya 
| music          = Clarence Wijewardena
| cinematography = Donald Karunarathna
| editing        = Jayatissa Dillimuni
| released       =  
| runtime        = 120 minutes
| country        = Sri Lanka Sinhala 
}}

Apeksha is a 1979 Sri Lankan film directed by H.D. Premaratne.

==Plot==
Niranjala (Malini Fonseka), a member of the English speaking elite, and Nimal (Amarasiri Kalansooriya), a young university graduate, regularly meet at the gas station where he works. They are attracted to each other, and over time, start meeting outside of the stations confines.

One of these occasions occurs at Niranjalas university fair. Nimal comes to meet her, but during the fair, one of Niranjalas friends is attacked by a gang of men. Nimal and his close friend Pradeep (Robin Fernando) help the friend get out of trouble. Later, when Nimal returns to the gas station, this gang of men come after him, and the fighting leads to the gas station owner firing Nimal.

Niranjala heads back to her familys tea estate in the hill country after she finishes university. Her father informs her that she will meet Samson (Ranjan Mendis), the son of an old friend and the new estate manager. Samson was educated in the UK and is returning to Sri Lanka.

Samson exits the airport only to see a poster for a singer named Erine. He watches her perform and when he meets her, it is revealed that they had a relationship in the UK. Samson and Erine spend time together in Colombo and Samson decides to skirt on his promised responsibilities, while Nimal tries to come up with a way to reunite with Niranjala. Meneka (Geetha Kumarasinghe), Niranjalas close friend, and Pradeep scheme for respectable ways for them to get back together. Instead, Nimal finds the letter of invitation in Samsons pocket and uses it to pass for him in the tea country.

Nimal arrives in the tea country and seamlessly convinces Niranjalas father that he is Samson. Nimal learns about tea estate life and as the new estate manager, earns the respect of the estate workers.

Samsons father arrives in Sri Lanka, and realizing that Samson has not moved to the hill country, drags him there, unannounced. Niranjalas father realizes what has happened and kicks Nimal out and forces Niranjala into the arranged marriage between her and Samson. A break-in occurs at the estate, and although no one can find the missing items, the blame falls on Nimal. This ensure his imprisonment until after the wedding.

On the day of the wedding, Nimal escapes and drives off with Niranjala. It comes out to everyone that Samson stole the items, gave some to Erine, and Nimal and Pradeep proceed to beat him up. The movie ends with Niranjala and Nimal running towards each other. 

==Cast==
*Malani Fonseka - Niranjala
*Amarasiri Kalansooriya - Nimal
*Geetha Kumarasinghe - Meneka
*Robin Fernando - Pradeep
*Ranjan Mendis - Samson

==Social Commentary==
When Niranjala and Meneka leave from the university, a brash young man pulls up and offers Meneka a ride. She refuses and instead of continuing on his way, he insults her. Later, Niranjala and Meneka get into their car, and the young man drives up to them. He is rude and harasses Meneka through the car window. Their driver asks, "Isnt that the son of someone important?" to which Niranjala replies, "It doesnt matter; he wasnt raised well."

Nimal has a degree from a Sri Lankan university, yet with the high unemployment rate in the late 1970s, the only job he can attain is that of a gas attendant. When he is unemployed, he fears that other people will assume he is part of the Janatha Vimukthi Peramuna. Niranjalas social identity as part of the English speaking elite is pointed at this socio-political situation.

== References ==
 

==External links==
*  

 
 
 