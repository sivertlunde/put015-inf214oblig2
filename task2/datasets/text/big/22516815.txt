Colt .45 (film)
{{Infobox film
| name           = Colt .45 
| image          = Colt .45 FilmPoster.jpeg
| border         = yes
| caption        = Theatrical release poster
| director       = Edwin L. Marin
| producer       = Saul Elkins
| writer         = Thomas W. Blackburn
| starring       = {{Plainlist|
* Randolph Scott
* Ruth Roman
* Zachary Scott
* Lloyd Bridges
* Alan Hale, Sr.
* Chief Thundercloud
}}
| music          = William Lava
| cinematography = Wilfred M. Cline
| editing        = Frank Magee
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film Colt .45 seven years later. Written by Thomas W. Blackburn, author of the lyrics to The Ballad of Davy Crockett, the film is about a gun salesman and gunfighter who tracks down a killer who stole two new Colt .45 repeating pistols leaving a trail of dead bodies behind him. The revolvers used in the movie were actually first model .44 Caliber Colt revolving belt pistols made in 1849 and reaching final form by 1850. Scott correctly demonstrated how to load them so the producers of the film were most likely aware of the anachronism in the title.

==Plot==
In the town of Red Rock, gun salesman Steve Farrell (Randolph Scott) demonstrates the new Colt .45 repeating pistols to the sheriff who is impressed that the United States government just ordered two thousand of these powerful weapons for the army. The demonstration is interrupted when men arrive to transfer one of the prisoners to another jail. As hes being led away, prisoner Jason Brett (Zachary Scott) grabs the pistols, shoots the sheriff, and escapes, pretending that Farrell was his partner. Convinced that Farrell was involved in the escape, the townspeople arrest the innocent gun salesman. In the coming days, Brett initiates a campaign of robberies and cold blooded murder, with regular guns being no match for his Colt .45 pistols.

Four months later, Farrell is released from jail due to a lack of evidence. The new sheriff offers him a letter clearing him of the charges if he reveals Bretts whereabouts. Reasserting his innocence, Farrell vows to go after Brett to retrieve his guns. Farrell tracks his prey into Texas and comes across a band of Indians whom Brett has killed to provide cover for a stagecoach robbery. The only surrvivor of the attack, Walking Bear (Chief Thundercloud), tells Steve about Bretts plan. As the stagecoach approaches, Steve jumps onto the stage from a rock outcropping just in time to fight off the attack by Bretts gang with his own set of Colt .45s. The only passenger on the stage, Beth Donovan (Ruth Roman), tries to prevent him from fighting off the robbers.

After Bretts gang pulls back and retreats, Farrell stops the stage and notices a white scarf hanging outside the stagecoach window. Believing it to be a signal to the robbers, Farrell suspects that Beth is part of the gang and says he intends to take her to the sheriff. While assisting the wounded stagecoach driver, however, Beth is able to escape on horseback. Farrell does not know that Beth is the wife of Paul Donovan (Lloyd Bridges), one of Bretts associates. Beth returns to her home, which is being used by Brett as a hideout. Although she believes that her husband has been forced to work with Brett, he is actually plotting with the killer to take over the nearby town of Bonanza Creek.

Unknown to the citizens of Bonanza Creek, Sheriff Harris (Alan Hale, Sr.) is working with Brett and his gang. When Farrell arrives in town, Harris agrees to make him his deputy. Harris then rides out to Bretts hideout and reveals that Farrell is in town. Brett and Harris plot an ambush to eliminate Farrell. Meanwhile, Farrell learns Beths identity. Harris later encourages him to ride out to her house, knowing Brett and his gang will be lying in wait. As he approaches, Bretts gang ride in for the kill, but Farrell is able to evade the ambush with the help of Walking Bear and his fellow Indians, who capture two gang members.

Back at the hideout, Beth overhears Paul plotting with Brett and realizes her husband is actively working with the gang. After she denounces her husband, Paul locks her in a store room. Later, she manages to escape and hurries into town, planning to reveal what she knows to the authorities. Just outside town, Paul tries to stop his wife, and as she rides past him, he shoots her. Hearing the shots, Farrell rides to Beth lying on the ground, takes her in his arms, and rides off seeking refuge with Walking Bear and his people. After being treated for her wound, Beth warns Farrell about Bretts plan to take over Bonanza Creek.

Soon after, the Indians discover Pauls body, shot in the back by a .45. When Farrell learns that the Indians intend to go on the warpath, he tries to talk them out of it, but he and Beth are held captive. When Beth escapes to warn the townspeople, Farrell rides after her. Along the trail, Harris and members of the gang set a trap and capture Farrell, but the Indians come to his rescue and kill his captors. Then they ride to Bonanza Creek and quietly go about killing Bretts men in the streets. The injured Harris makes his way back to town to warn Brett, whos holed up in the jail with Beth as his hostage. When Farrell and the Indians arrive at the jail, the cowardly Brett uses Beth as a shield and tries to escape, but Beth breaks away. Farrell enters the jail alone and sees Brett is out of ammunition. He puts down his .45s and the two men fight. During the struggle, Brett goes for Farrells guns and Farrell shoots him. Afterwards, Farrell walks out into the street and is embraced by Beth.

==Cast==
* Randolph Scott as Steve Farrell
* Ruth Roman as Beth Donovan
* Zachary Scott as Jason Brett
* Lloyd Bridges as Paul Donovan
* Alan Hale, Sr. as Sheriff Harris Ian MacDonald as Miller
* Chief Thundercloud as Walking Bear
* Luther Crockett as Judge Tucker
* Walter Coy as Carl Charles Evans as Redrock Sheriff
* Carl Andre as Indian
* Clyde Hudkins, Jr. as Indian
* Leroy Johnson as Indian   

==Production==
Colt .45 was filmed on location at Iverson Movie Ranch in Chatsworth, California, Vasquez Rocks in Agua Dulce, California, and in Santa Clarita, California.   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 