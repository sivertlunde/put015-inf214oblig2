The Mistress (1962 film)
 
{{Infobox film
| name           = The Mistress
| image          = The Mistress.jpg
| caption        = Swedish theatrical poster
| director       = Vilgot Sjöman
| producer       = Lars-Owe Carlberg Allan Ekelund
| writer         = Vilgot Sjöman
| starring       = Bibi Andersson
| music          = 
| cinematography = Lasse Björne
| editing        = Lennart Wallén
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Best Foreign Language Film at the 35th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Bibi Andersson - The Girl
* Birger Lensander - Conductor
* Per Myrberg - The Boy Gunnar Olsson - Old Man
* Birgitta Valberg - Motherly Woman
* Max von Sydow - Married Man
* Öllegård Wellton - Married Woman

==See also==
* List of submissions to the 35th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 

 