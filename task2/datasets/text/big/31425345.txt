Julius Sizzer
 
{{Infobox film
| name           = Julius Sizzer
| image          =
| image_size     =
| caption        =
| director       = Edward Ludwig
| producer       =
| writer         = Edward Ludwig Benny Rubin
| narrator       =
| starring       = See below
| music          =
| cinematography =
| editing        =
| distributor    = RKO Radio Pictures
| released       = 7 September 1931
| runtime        = 18 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Julius Sizzer (1931) is an American two-reel short film directed by Edward Ludwig.

== Plot summary ==
In this short, (Benny Rubin) plays a dual role of a man and his brother who have to dodge gangsters trying to put them on the spot. 

== Cast ==
*Benny Rubin
*Gwen Lee
*Lena Malena
*Matthew Betz
*Maurice Black
*G. Pat Collins
*Tom McGuire
*Clifford Dempsey

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 