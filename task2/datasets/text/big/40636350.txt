Hours for Jerome
Hours for Jerome (1980–82) is an experimental film in two parts directed by Nathaniel Dorsky. The two parts are included in the US National Film Registry and hence selected for preservation in the Library of Congress.  

==Production==

The footage for the film was shot in the late 1960s but Dorsky did not begin editing it until 1980.  It is a "silent tone poem"  recording the daily events of Dorsky and his partner, artist Jerome Hiler around Lake Owassa in New Jersey and in Manhattan.     Together the two parts cover a year, with part one depicting spring and summer and part two fall and winter, although the title refers to the religious Book of Hours which covers prayers for the course of a day. Nonetheless, the images are according to Scott MacDonald intended to act as prayers and "reaccess something of the sacred".     Like most of Dorskys work, it is intended to be projected at silent film speeds of 17-20 frames per second, rather than the usual 24 fps of sound films.   

==Critical reception==

Critic Matthew Flanagan chose it as one of the ten greatest films ever for the 2012 Sight & Sound poll. 

Scott MacDonald praised its depiction of the passing year, calling it "Americas most compelling cinematic paean to temperate-zone seasonality". 

==References==
 

 

 
 
 


 