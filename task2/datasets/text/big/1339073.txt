Visitors (2003 film)
 
{{Infobox film
| name           = Visitors
| image          = Visitors (2003 film).jpg
| caption        = Richard Franklin Richard Franklin   Jennifer Hadden
| writer         = Everett De Roche
| starring       = Radha Mitchell   Susannah York   Ray Barrett
| music          = Nerida Tyson-Chew
| cinematography = Ellery Ryan
| editing        = David Pulbrook
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = Australia
| language       = English
| budget         = nearly A$6 million 
| gross          =  $34,270 (Australia)
}}  Richard Franklin and was produced by Jennifer Hadden. This film in some ways is close to the theme of "The Shining" by Stanley Kubrick. Director Richard Franklin attempts to create an atmosphere of isolation and cabin-fever phenomena in the lonely girl on the water.

==Cast==
* Radha Mitchell as Georgia Perry
* Dominic Purcell as Luke
* Tottie Goldsmith as Casey
* Susannah York as Carolyn Perry
* Ray Barrett as Bill Perry
* Che Timmins as Kai
* Christopher Kirby as Rob
* Phil Ceberano as Pirate Captain

==Production==
Richard Franklin says he wanted to make a thriller along the lines of Patrick.
 

==Box office==
Visitors grossed $34,270 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 
 