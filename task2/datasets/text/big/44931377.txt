Lousy Little Sixpence
 
{{Infobox film
| name           = Lousy Little Sixpence
| image          = 
| alt            = 
| caption        = 
| director       = Alec Morgan
| producer       = Alec Morgan Gerald Bostock
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = 
| cinematography = Martha Ansara Jaems Grant Allesandro Cavadini Fabio Cavadini
| editing        = John Scott Rhonda MacGregor
| studio         = 
| distributor    = Ronin Films
| released       =   
| runtime        = 54 minutes
| country        = Australia English
| budget         = 
| gross          = 
}}
 Aboriginal Australians against the Aboriginal Protection Board in the 1930s.      The films title references the amount of pocket money that Aboriginal children were to be paid for their forced labour, although few ever received it.    

==Overview== Australian Aborigines Day of Mourning on 26 January 1938, which marked 150 years of European settlement in Australia.  

==Production== Victoria while receiving unemployment benefits, looking for information on the Stolen Generations to include such as newspaper articles, films and photographs. 

The film screened for six weeks at Dendy cinemas in Sydney. 

==Cast==
*Margaret Tucker as herself
*Bill Reid as himself
*Geraldine Briggs as herself
*Flo Caldwell as herself
*Violet Shea as herself
*Chicka Dixon as Narrator  

==See also==
*Stolen Generations
*Day of Mourning (Australia)
*Aborigines Progressive Association
*Aboriginal Protection Board

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 