Sherlock Holmes Baffled
{{Infobox film
| name = Sherlock Holmes Baffled
| image = Sherlock Holmes Baffled.jpg
| alt = A frame of the black-and-white film. Sherlock Holmes enters his parlour and taps the shoulder of a burglar who is collecting Holmes tablewares into a sack. Holmes is wearing a dressing gown and smoking a cigar, the thief is dressed in black.
| caption = Holmes first encounters the intruder.
| director = Arthur Marvin
| producer =
| starring =
| cinematography = Arthur Marvin
| editing =
| distributor = American Mutoscope and Biograph Company
| released = 1900–1903
| runtime = 30 seconds
| country = United States
| language = Silent film
| budget =
}}
 very short earliest known detective film.     In the film, a thief who can appear and disappear at will steals a sack of items from Sherlock Holmes. At each point, Holmess attempts to thwart the intruder end in failure.   
 lost for many years, the film was rediscovered in 1968 as a paper print in the Library of Congress. 

==Action== dressing gown pocket and firing it at the intruder, who vanishes. After Holmes recovers his property, the bag vanishes from his hand into that of the thief, who promptly disappears through a window. At this point the movie ends abruptly with Holmes looking "baffled".  

==Production==
 ]]
 motion picture device, patented by Herman Casler in 1894.  Like Thomas Edisons Kinetoscope the Mutoscope did not project on a screen, and provided viewing to only one person at a time. Cheaper and simpler than the Kinetoscope, the system marketed by the American Mutoscope Company quickly dominated the coin-in-the-slot "peep show|peep-show" business. 

The Mutoscope worked on the same principle as a flip book, with individual image frames printed onto flexible cards attached to a circular core which revolved with the turn of a user-operated hand crank.  The cards were lit by electric light bulbs inside the machine, a system devised by Arthur Marvins brother, Henry, one of the founders of the Biograph company. Earlier machines had relied on reflected natural light. 

To avoid violating Edisons patents, Biograph cameras from 1895 to 1902 used a large-format film measuring 2-23/32 inches (68&nbsp;mm) wide, with an image area of 2 × 2½ inches, four times that of Edisons 35 mm film|35&nbsp;mm format.  Biograph film was not Film perforations|ready-perforated; the camera itself punched a sprocket hole on each side of the frame as the film was exposed at 30 frames per second. {{cite journal
 | url = http://www.soc.org/opcam/06_sp95/mg06_biocam.html
 | archiveurl = http://web.archive.org/web/20080625012101/http://www.soc.org/opcam/06_sp95/mg06_biocam.html
 | archivedate = 25 June 2008
 | title = The Biograph Camera
 | journal = The Operating Cameraman location =Toluca Lake, California
 | date = Spring 1995
 | author = Bitzer, Billy
 | accessdate = 30 November 2004
 }}   Sherlock Holmes Baffled ran to 86.56 metres in length, giving the film a running time of 30 seconds (although in practice, due to the hand-cranked gearing of the Mutoscope this would have varied). 

The director and cinematographer of Sherlock Holmes Baffled was Arthur W. Marvin (May 1859 –   1911), a staff cameraman for Biograph.    Marvin completed over 418 short films between 1897 and 1911, and was known for filming vaudeville entertainers. He later became known as the cameraman for the early silent films of D. W. Griffith.  The identities of the first screen Holmes and his assailant are not recorded.   
 actualities (documentary Broadway in New York City.  According to Christopher Redmonds Sherlock Holmes Handbook, the film was shot on April 26, 1900.    Julie McKuras states that the film was released in May of the same year.  Despite being in circulation, Sherlock Holmes Baffled was only registered on February 24, 1903, and this is the date seen on the films copyright title card.  The occasionally suggested date of 1905 is probably due to confusion with a Vitagraph film titled Adventures of Sherlock Holmes; or, Held for Ransom (1905). 

==Rediscovery== lost for paper copy was identified in 1968 in the Library of Congress Paper Print archive by Michael Pointer, a historian of Sherlock Holmes films.    Because motion pictures were not covered by copyright laws until 1912, paper prints were submitted by studios wishing to register their works. These were made using light-sensitive paper of the same width and length as the film itself, and developed as though a still photograph. Both the Edison Company and the Biograph Company submitted entire motion pictures as paper prints, and it is in this form that most of them survive.  The film has subsequently been transferred to 16 mm film in the Library of Congress collection. 

==Analysis==
  canonical Sherlock film editing effects, particularly the stop trick first developed four years earlier in 1896 by French director Georges Méliès. 
 Sherlock Holmes Sherlock Holmes Broadway debut Garrick Theater on November 6, 1899. 

Michael Pointers report on the rediscovery of Sherlock Holmes Baffled in 1968 stated "it is an early trick film clearly made for viewing on a mutoscope or peepshow machine. Although a tiny, trivial piece, it is historic as being the earliest known use of Sherlock Holmes in moving pictures."   By extension of being the first Sherlock Holmes story, the inclusion of the character also makes it the first known example of a detective film.   It has been posited that Sherlock Holmes has become the most prolific screen character in the history of cinema. 

==See also==
* List of rediscovered films

==References==
 

==External links==
*  
*  available for free download at  

 
 

 
 
 
 
 
 
 

 