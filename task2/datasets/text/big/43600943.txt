Nuit de chien
{{Infobox film
 | name = Nuit de Chien
 | image =  Nuit de Chien.jpg
 | caption =
 | director = Werner Schroeter
 | writer =   
 | starring =  
 | music = Eberhard Kloke	 
 | cinematography = Thomas Plenert
 | editing =  Peter Przygodda
 | producer =   
 | released =   
 | language =French
 }}
Nuit de Chien (This Night) is a 2008 French-German-Portuguese drama film directed by Werner Schroeter. It is based on the novel Para esta noche by  Juan Carlos Onetti. It was entered into the competition at the 65th Venice International Film Festival.   

==Cast==
* Pascal Greggory as Ossorio 
* Bruno Todeschini  as Morasan
* Amira Casar  as Irene
* Éric Caravaca  as Villar
* Nathalie Delon  as Risso
* Marc Barbe  as Vargas
* Jean-François Stévenin  as Martins
* Bulle Ogier  as D. Inês
* Laura Martin  as Victoria 
* Filipe Duarte  as Júlio
* Sami Frey  as Barcala  
* Isabel Ruth
* Laura Soveral
* Teresa Tavares
* Elsa Zylberstein

==References==
 

==External links==
* 
 
 
  
 
 
 
 
 
 
 
 
 
 
 