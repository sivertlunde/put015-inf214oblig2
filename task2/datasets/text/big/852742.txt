Kama Sutra: A Tale of Love
 
{{Infobox film name = Kama Sutra: A Tale of Love image = Kamsutraposter.jpg caption = Theatrical release poster director = Mira Nair producer = Caroline Baron Lydia Dean Pilcher Mira Nair writer = Helena Kriel Mira Nair starring = Rekha Indira Varma Naveen Andrews Maricel Aquino Sarita Choudhury Arundathi Nag cinematography = Declan Quinn editing = Kristina Boden distributor = Trimark Pictures released = 28 February 1997 runtime = 117 minutes country = India language = English budget = $300,000 (estimated)
}}

Kama Sutra: A Tale of Love is a 1996 Indian film directed by Mira Nair. The film takes its title from the ancient Indian text, the Kama Sutra, but this only serves as a common link between the characters.  

==Overview==
The plot takes its origin from a short story by Urdu author Vājidah Tabassum titled "Utran" ("Hand-Me-Downs" or "Cast-Offs").  The portion of plot derived from "Utran" takes place from the films beginning until the scene where Maya says: "Now something I have used is yours forever." After that the story is the screenwriters creation.

During filming in India, the name of the project was not revealed to government officials who would have denied the petition to film in India had it been called "Kama Sutra."  Instead, it was called "Maya & Tara."  Since government officials made many periodic visits to the set to ensure proper Indian film etiquette, the cast had to improvise fake scenes which avoided the nudity and sexuality central to the story.  Upon completion, authorities screened the film and it was subsequently banned in India because of the erotic scenes that contained heterosexual as well as homosexual elements (the lesbianism was depicted in an explicit scene, whereas the male homosexuality was more implied).

==Plot==
Set in 16th century India, this movie depicts the story of two girls who were raised together, though they came from different social classes. Tara (Sarita Choudhury) is an Forward Castes|upper-caste princess while Maya (Indira Varma) is her beautiful servant. The two girls are best friends, but an undercurrent of jealousy and resentment is caused by Taras haughtiness, symbolized by the fact that Maya is given Taras hand-me-down clothes and never anything new to wear. As the girls approach marriageable age, Tara resents that Maya is a better classical dancer than she is, and that her parents and hunchback brother, Prince Bikram (aka "Viki") show affection for her servant. 

Tara is prepared to marry Prince Raj Singh (Naveen Andrews), and Maya is forced into the role of the inferior servant at their wedding festival. When the prince comes to view his future wife, he is instantly infatuated with Maya instead. Noticing this, Tara spits in Mayas face, sending her from the wedding in tears. Maya decides to take revenge when she chances on Raj sleeping alone, before he has completed the marriage rites with Tara. Maya has her first sexual experience with Raj, but unknown to both, Taras brother, Prince "Viki", hides and watches the two of them together. Viki is crushed that his childhood infatuation has slept with his future brother-in-law, but at first keeps the knowledge to himself, and the wedding rites are completed the next day. 

As Tara is leaving home as a newly-wedded bride to Raj, Maya tells her that just as Maya wore the princesss used clothes all her life, Tara will now have something Maya has used. During her wedding night, Tara, a sheltered virgin full of romantic dreams, is hesitant to consummate their relationship. This angers and sexually frustrates Raj, who rapes his horrified bride, setting a tone of violence and humiliation for the marriage. Despite this, Tara still yearns for a loving relationship with her indifferent husband.

To save Mayas honor, Viki sends a marriage proposal for her. When she refuses, he publicly brands her as a whore, and she is forced to leave her home. Wandering on her own, she meets a young stone sculptor, Jai Kumar (Ramon Tikaram) who works for Raj. He reveals that Maya has been the inspiration for his beautiful and highly erotic statues. Realizing she has nowhere to stay, Jai takes her to an older woman named Rasa Devi (Rekha), who is a teacher of the Kama Sutra, the ancient art of seduction and love making. Maya begins an intense romantic and sexual relationship with Jai that is abruptly halted when he fears he might not be able to work properly with Maya consuming his thoughts. Rejected by her first real lover, Maya finds comfort with Rasa Devi, making the decision to learn the courtesans art. 
 visage as Mayas in one of Jais sculptures. He dispatches his attendants to find Maya and she is delivered to the King as his new concubine. Soon after, Raj and Jai have a "friendly" wrestling competition, in which Jai wins. Jai gets the Kings favor, but is also warned that there will be dire consequences if he should ever defeat the King again. Jai then learns of Mayas new status as the favored concubine. Jai understands that his king is a dangerous man and he must keep his former relationship with Maya a secret, for their mutual safety.

In the meantime, the threat of an invading  , he becomes irresponsible with his duties as King. He insults Viki sexually and for being a hunchback. In retaliation, Viki writes a letter to the Shah to rid the kingdom of Raj, who now taxes the poor for his own perverted pleasure. Jai and Maya rekindle their passion and the two begin meeting in secret. As tensions between Jai and Raj grow, Maya and Jai exchange wedding vows in private. Raj later catches the two lovers together, and sentences Jai to death.

After finding Tara in the midst of a suicide attempt, Maya reconciles with her childhood friend. Maya then teaches Tara how to seduce the King, while Tara promises to help Maya escape to visit Jai. However, when Tara goes to her husband, he recognizes Mayas style of seduction and again tries to humiliate his wife. Finally free of her tormentor, Tara tells Raj that she doesnt even love him enough to hate him, and leaves. 

Maya leaves the castle and visits Jai one last time. Telling Jai she is his forever, Maya hacks off her long hair, symbolizing that she will be his widow. Maya then tries her best to convince Raj to free Jai by promising him her total surrender. But knowing he cant have her heart, Raj rejects her plea. Just before the execution, a box arrives from the Shah, holding the severed head of the grand vizier Rajs brother. Jai is killed, while Maya watches from the crowd. Meanwhile, soldiers of the invading Shah take the kings palace. Maya walks away into the distance, meditating on her new spiritual freedom: "My heart is as open as the sky."

== Cast ==
* Indira Varma as Maya
* Sarita Choudhury as Tara the princess
* Naveen Andrews as Raj Singh
* Ramon Tikaram as Jai Kumar
* Rekha as Rasa Devi, teacher of Kamasutra

==Awards and nominations==
Declan Quinn won the 1998 Independent Spirit Award for Best Cinematography for his work in the film.  The film was also nominated for the Golden Seashell award at the 1996 San Sebastián International Film Festival.

==See also==
* 
* 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 