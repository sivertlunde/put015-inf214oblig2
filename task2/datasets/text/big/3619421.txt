Winnie the Pooh and a Day for Eeyore
{{Infobox film name = Winnie the Pooh and a   image = Winnie the Pooh and a Day for Eeyore.jpg caption = VHS cover director = Rick Reinert producer = Rick Reinert writer = Peter Young   Steve Hulett   Tony L. Marino   Ron Clements (uncredited)   A. A. Milne narrator = Laurie Main starring = Hal Smith   Ralph Wright   Paul Winchell   Will Ryan   Kim Christianson   Julie McWhirter   John Fiedler music = Robert & Richard Sherman (songs) studio = Walt Disney Productions distributor = Buena Vista Distribution released =   runtime = 25 minutes country = United States language = English
}}
 Disney Winnie Winnie the The Sword Pooh books by A. A. Milne.

Produced by Rick Reinert Productions, this was the first Disney animated film since the 1938 Silly Symphonies short Merbabies to be produced by an outside studio.  (The company had also previously produced the educational Disney short Winnie the Pooh Discovers the Seasons in 1981.)

==Plot== Pooh takes a walk to a wooden bridge over a river where he likes to do nothing in particular. On this day, though, he finds a fir cone and picks it up. Pooh thinks up a rhyme to go with the fir cone, but he accidentally trips on a tree root and drops it in the river. Noticing that the flow of the river takes the cone under the bridge, Pooh invents a racing game out of it. As the game uses sticks instead of cones, he calls it.
 Rabbit and Eeyore floating the narrator, the animals find out he had indeed deliberately bounced Eeyore. Tigger says it was all a joke, but nobody else feels that way. Tigger disgustedly says that they have no sense of humor, and bounces away.
 Owl for advice. Owl suggests that he writes to Eeyore on the pot so that Eeyore could use it to put things in. Owl ends up writing a misspelled greeting (hipy papy bthuthdth thuthda bthuthdy) on the pot and flies off to tell Christopher Robin about the birthday. Piglet, who heard about Eeyores birthday from Pooh, planned to give a red balloon to Eeyore, but when Owl greets him from the sky, Piglet not looking where he is going, hits a tree and causes it to accidentally burst the balloon.

Piglet is very sad that his gift for Eeyore is spoiled, but he presents it to him anyway, and only a minute later, Pooh brings the empty pot. Eeyore is gladdened, as he can now put the busted balloon into the pot and remove it again (he also claimed that he likes the color red). Pooh and his friends then pitch in and plan a surprise party for their friend.

During the party, Tigger arrives and bounces Rabbit out of his chair. Roo welcomes him to the festivities as Rabbit draws himself up from being bounced on by Tigger, incensed. Rabbit opines that Tigger should leave because of the way he treated Eeyore earlier. Roo wants Tigger to stay, and Christopher Robins solution is for everyone to go to the bridge and play Poohsticks. Eeyore, a first-time player, wins the most games, while Tigger does not win at all, causing him to conclude that "Tiggers dont like Poohsticks". Eeyores secret for winning, as he explains to Tigger afterwards, is to "let his stick drop in a twitchy sort of way." As Tigger bounces Eeyore again, Christopher Robin, Pooh and finally Piglet all decide that "Tiggers all right, really".

==Voice cast== Hal Smith Owl
*Ralph Wright as Eeyore
*Paul Winchell as Tigger Rabbit
*Kim Christianson as Christopher Robin Piglet
*Julie Kanga
*Dick Billingsley as Roo
*Narrated by Laurie Main 

Only Smith, Wright, Fiedler, and Winchell returned in the roles they had originated in. Kim Christianson became the fourth different actor to portray Christopher Robin in as many featurettes, after Bruce Reitherman, Jon Walmsley, and Timothy Turner. Dick Billingsley assumed the role of Roo after Dori Whitaker portrayed him in the previous two featurettes.

Smith and Laurie Main first took the roles of Winnie the Pooh and The Narrator in the 1981 educational film Winnie the Pooh Discovers the Seasons, as Sterling Holloway elected not to continue the role of Pooh and Sebastian Cabot, the original narrator, died shortly after the making of the feature film The Many Adventures of Winnie the Pooh. The deaths of Junius Matthews in 1978 and Barbara Luddy in 1979 also necessitated changes in Rabbits and Kangas portrayers; Will Ryan began a three-year stint as Rabbits voice in this featurette while Julie McWhirter portrayed Kanga only once. Additionally, Main, Smith, Ryan, and Christianson would later appear in the live-action series Welcome to Pooh Corner as the Narrator, Pooh, Rabbit, Owl, and Roo respectively.

Ralph Wright, one of two voice actors who appeared in all four theatrical releases, became the third principal Winnie the Pooh featurette voice to pass away shortly after the release of the film; he died of a heart attack on December 30, 1983. He has since been followed in death by Sterling Holloway (1992), Hal Smith (1994), and finally Paul Winchell and John Fiedler, who died on consecutive days in June 2005.

On DVD and Blu-Ray releases of The Many Adventures of Winnie the Pooh, the short carries different voice credits than those on previous video releases. Jim Cummings (Pooh), Ken Sansom (Rabbit), Tress MacNeille (Kanga), Trevyn Savage, and Aaron Spann are listed, despite that the original soundtrack (with Hal Smith as Pooh, Will Ryan as Rabbit, etc.) appears unaltered. This could imply that a re-dub was attempted with these actors, but ultimately was not used.

==Home video== high definition.)

==Trivia== Sebastian Cabot.
The film is the only Winnie the Pooh feature film to not feature the vocals of the original Winnie the Pooh theme song at any part of the film.
*This featurette appears as clips in A Poem Is... short, "In the Fashion".

==Winnie the Pooh featurettes==
*Winnie the Pooh and the Honey Tree (1966)
*Winnie the Pooh and the Blustery Day (1968)
*Winnie the Pooh and Tigger Too (1974)
*Winnie the Pooh and a Day for Eeyore (1983)

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 