Kurbaan (1991 film)
 

{{Infobox Film
 | name = Kurbaan
 | image = Kurbaan.jpg
 | caption = Audi Cassette Cover
 | director = Deepak Bahry 
 | producer = Bubby Kent
 | writer = Vinay Shukla (story)
 | starring = Salman Khan Ayesha Jhulka Sunil Dutt Kabir Bedi Gulshan Grover Rohini Hattangadi
 | distributor = Rajshri Productions  Hindi
  | music = Anand-Milind  
 | released = May 31, 1991
 | cinematography = Thomas A. Xavier  
 | runtime = 192 minutes
 | editing = Mukhtar Ahmed  
 | language = Hindi
 | distributor = Karishma Internationals
}}

Kurbaan ( ) is an Indian Bollywood movie directed by Deepak Bahry, starring Salman Khan and Ayesha Jhulka. The film, which was released on May 31, 1991, marked the debut of Delhi girl Ayesha Jhulka.

It is a romantic love story in the backdrop of extreme violence.

The film had stawarts like Sunil Dutt and Kabir Bedi pitted against each other in the form of a Dacoit and a top cop.

Their children fall in love in each other and thus begins a major second confrontation between the two biggies amidst the obstacles faced by the young lovers in the course of their chosen destiny which was Love against all odds.

==Synopsis==

Maan Singh and the honest Prithvi Singh are engaged in a legal property dispute. When the Court gives verdict in Prithvis favor, Maan Singh, in a fit of rage, hires a renowned bandit, Panna Singh, to eliminate Prithvis entire family. Panna partly succeeds, and escapes wounded to a forest to escape Prithvis fury. Prithvi loses his sister, wife and other members of his family due to the attack, and vows to avenge by killing Maan Singhs family in return, and he does it with partial success.

Maan Singhs brother, the Police Inspector Suraj Singh, who incidentally was Prithvis trusted and best friend testifies against Prithvi in the case of carnage as his duty. This however upsets Prithvi to no end and puts an end to their friendship.

Prithvi escapes prison, plunders and takes over Panna Singhs gang and becomes Daku Prithvi Singh. The only survivors from his family are his daughter, Chanda, his former housekeeper Kaaki, and her son, Himmat. He goes into hiding with his gang and family.

On the other side, Inspector Suraj Singh lives with his son, Akash. Years later, an educated Akash returns and while visiting a local fair spots an uneducated Chanda and they both fall in love with each other.

When their respective fathers find out, they are enraged and prohibit the young lovers from seeing each other by locking them up and their marriages are fixed elsewhere.

Both Akash and Chanda believe in their love, and stage their escape but are unaware of the consequences- another bloodbath at the hands of the vengeful Panna Singh is on the horizon, as he wants to kill Prithvi and his remaining family.

== Cast ==
* Salman Khan ... Akash
* Ayesha Jhulka ... Chanda
* Sunil Dutt ... Prithvi Singh
* Kabir Bedi ... Suraj Singh
* Gulshan Grover ... Himmat Singh
* Rohini Hattangadi ... Kaaki Maa
* Bharat Kapoor ... Maan Singh
* Goga Kapoor ... Panna Singh
* Kunika ... Gayatri Prithvi Singh
* (RAJU      ... AS

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s)!! Length
|-
| 1
| Aao Main Padhadoon Tumhain A, B, C
| Abhijeet Bhattacharya|Abhijeet, Sarika Kapoor
| 08:24
|-
| 2
| Baitha Neeli Jheel Kinare 
| Suresh Wadkar, Anuradha Paudwal 
| 07:27
|-
| 3
| Deewanon Se Poochho Mohabbat Hai Kya
| Sukhwinder Singh
| 05:26
|- 
| 4
| Main Tujhpe Kurbaan 
| Mohammad Aziz
| 06:21
|-
| 5
| Yeh Dharti Chand Sitare
| Udit Narayan, Anuradha Paudwal
| 10:58
|- 
| 6
| Zuba Zuba
| Sapna Mukherjee
| 03:34
|}

==Notes==

Kurbaan was the 6th release as well as 5th hit for  , Sanam Bewafa, and Patthar Ke Phool.

== External links ==
* 

 
 
 
 


 