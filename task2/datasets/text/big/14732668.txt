The Road to Yesterday
{{Infobox film
| name           = The Road to Yesterday
| image	         = The Road to Yesterday FilmPoster.jpeg
| caption        = Film poster
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| writer         = Beulah Marie Dix Howard Hawks Jeanie MacPherson Evelyn Greenleaf Sutherland
| starring       = Joseph Schildkraut
| music          = Rudolph Berliner
| cinematography = J. Peverell Marley
| editing        = Anne Bauchens
| distributor    = Producers Distributing Corporation
| released       =  
| runtime        = 107 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

The Road to Yesterday is a 1925 American silent film directed by Cecil B. DeMille. Prints of the film reportedly survive at George Eastman House and in private collections.      

==Cast==
* Joseph Schildkraut - Kenneth Paulton
* Jetta Goudal - Malena Paulton William Boyd - Jack Moreland
* Vera Reynolds - Beth Tyrell
* Trixie Friganza - Harriet Tyrell
* Casson Ferguson - Adrian Thompkyns
* Julia Faye - Dolly Foules
* Clarence Burton - Hugh Armstrong Charles West - Watt Earnshaw
* Josephine Norman - Anne Vener
* Dick Sutherland - Torturer (uncredited)
* Chester Morris - Guest (uncredited)
* Sally Rand - (uncredited) Walter Long - thug at Burning Stake scene

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 