Pushpaka Vimana (film)
 
 
{{Infobox film
| name = Pushpaka Vimana Pushpaka Vimanamu Pushpak
| image = Pushpaka_Vimanam.jpg
| caption = 
| director =  Singeetam Srinivasa Rao
| producer = Singeetam Srinivasa Rao Shringar Nagaraj
| story = Singeetam Srinivasa Rao
| screenplay = Singeetam Srinivasa Rao Amala Tinu Anand Farida Jalal P. L. Narayana K.S Ramesh Prathap Pothan Loknath
| music = L. Vaidyanathan
| cinematography = B. C. Gowrishankar
| released = 10 September 1987
| runtime = 131 minutes
| budget =   
| language = Silent Film
}} Indian silent film|silent, black comedy film written, and directed by Singeetam Srinivasa Rao.
 
 
  Touted to be the first Indian silent full-length feature film,   it was produced by Singeetam Srinivasa Rao and Shringar Nagaraj. Upon release, the film garnered highly positive reviews, and remained a box office hit. The film had a 35-week theatrical run in Bengaluru. 
 Filmfare Award South for Best Film.  The film was premiered at the International Film Festival of India, 1988 Cannes Film Festival in the International Critics Week,    and retrospective at the Shanghai Film Festival, and Whistling Woods International Institute   The film is listed among CNN-IBNs hundred greatest Indian films of all time. 

==Summary== Identity of a rich man. The young man gets to live in a 5 star hotel, where he meets a girl who could be the love of his life. However, a hired killer (Tinu Anand) has also set his eyes on the young man, as he has mistaken the young man for the rich man too. What turn will this story take next?

Posing as the rich man, our hero enters the hotel and starts living a life of luxury. There, he meets a girl, who is the daughter and assistant of a magician (K. S. Ramesh), who is residing in the hotel with his daughter and wife (Farida Jalal). Unknown to everybody, the rich mans brother has ordered a hit on him. The brother plans to usurp the whole estate by later killing his sister-in-law (Ramya). The hired killer too thinks like everybody else that the hero is actually his target.

The killer comes up with ingenious ways to kill the hero, but he always fails, falling in his own trap. All this time, the hero remains ignorant about the danger his life is in. One day, however, the hero figures out that the killer is in the hotel to kill someone, but is unable to find the identity of his target. Suddenly, the hotel owner (Loknath) dies in his own hotel of old age. In a montage shown about the owner, the hero realises that the owner was a poor man, just like him. Seeing what the owner achieved by fair means, the hero begins to question his actions.

Some days later, the roadside beggar dies and municipality people come to take his dead body. However, seeing the beggars stash, they nearly throw his body on the pavement and start stealing the money. The hero decides to make things right. Meanwhile, the killer and the rich mans brother realise that they were targeting the wrong man all the time. At this time, the rich mans wife learns of her brother-in-laws treachery, upon which he locks her up in the bungalow.

The hero figures out the killers plan and sets the rich man free. He explains the situation to the rich man. The rich man goes back to his bungalow, where he learns the truth. While the rich man and the hero are able to rescue the rich mans wife, the police arrest the villains. After the matter has been taken care of, the hero decides to come clean to the heroine as well. He learns that the heroines family is about to leave the hotel.

The hero confesses the truth to the heroine, but to his surprise she forgives him. While leaving the hotel, she drops a rose wrapped in a paper from her car, meaning that she has given him her address. However, before the hero can get the rose, another car comes and mows down the rose, taking it with it implying the lovers never unite . The hero is not able to get the heroine, but some days later, he is shown in a line for a job vacancy, although this time he has a rose in his hands and hope in his eyes. While it is not clear that the hero got the job or not, it is certain that his outlook towards life has changed.

==Cast==
* Kamal Haasan as Unemployed youth Amala as Magicians daughter
* Tinu Anand as Killer
* P. L. Narayana as Beggar
* Farida Jalal as Magicians wife
* Sameer Khakhar as The millionaire
* Ramya as Millionaires wife
* Loknath as Hotel owner
* K. S. Ramesh as Magician
* Prathap K. Pothan as The lover of the wife of the millionaire

==Awards== National Film Awards
*National Film Award for Best Popular Film Providing Wholesome Entertainment (1988) - Singeetam Srinivasa Rao and Shringar Nagaraj 

;Filmfare Awards South Filmfare Award South for Best Film - Singeetam Srinivasa Rao and Shringar Nagaraj
*Filmfare Award for Best Actor – Kannada - Kamal Hassan 

==References==
 

 
 

 
 
 
 
 
 
 
 
 

 