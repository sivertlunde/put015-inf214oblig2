Shooting Stars (1927 film)
{{Infobox film
| name           = Shooting Stars
| image          = 
| image_size     = 
| caption        = 
| director       = Anthony Asquith   A.V. Bramble
| producer       = Harry Bruce Woolfe
| writer         = Anthony Asquith   John Orton
| narrator       = 
| starring       = Annette Benson Brian Aherne Donald Calthrop Wally Patch
| music          =  Henry Harris   Stanley Rodwell
| editing        = 
| studio         = British Instructional Films
| distributor    = New Era Films 
| released       = 10 June 1928 (US)
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British drama film directed by Anthony Asquith and A.V. Bramble and starring Annette Benson, Brian Aherne and Wally Patch. 

==Production==
It was Asquiths first film as a director. It was made at Cricklewood Studios in North London for British Instructional Films. The novelisation of the film was written by the popular novelist E. C. Vivian|E. Charles Vivian. 

==Cast==
* Annette Benson - Mae Feather 
* Brian Aherne - Julian Gordon 
* Donald Calthrop - Andy Wilkes 
* Wally Patch - Property Man 
* David Brooks - Turner 
* Ella Daincourt - Asphodel Smythe, Journalist 
* Chili Bouchier - Winnie, Bathing Beauty 
* Tubby Phillips - Fatty  Ian Wilson - Reporter 
* Judd Green - Lighting Man 
* Jack Rawl - Hero

==References==
 

==External links==
 

 
 

 
 
 
 
 
 
 
 
 
 


 