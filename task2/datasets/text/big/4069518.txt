The Abominable Snowman (film)
 
 
{{Infobox film
| name           = The Abominable Snowman
| image          = Abominable Snowman movie.jpg
| caption        = Theatrical release poster
| director       = Val Guest
| producer       = Aubrey Baring
| writer         = Nigel Kneale
| based on       =   Robert Brown
| music          = Humphrey Searle Arthur Grant
| editing        = Bill Lenny
| studio         = Hammer Film Productions
| distributor    = Warner Bros. (UK) 20th Century Fox (US)
| released       =  
| runtime        = 91 minutes
| country        = United Kingdom
| language       = Slovakia
}}
 Robert Brown appear in supporting roles.

==Plot== botanical expedition Robert Brown), Sherpa guide Kusang (Wolfe Morris), arrives at the monastery in search of the legendary Yeti or Abominable Snowman. Rollason, despite the objections of his wife and the Lama, decides to join Friends expedition. Whereas Rollason is motivated by scientific curiosity to learn more about the creature, Friend seeks fame and fortune and wants to capture a live Yeti and present it to the worlds press.
 bear trap laid by Friend to catch the Yeti and later dies in a fall. Kusang flees back to the monastery from where Helen and Fox decide to mount a rescue mission. Meanwhile, Shelley succeeds in shooting and killing a Yeti, an act that enrages the creatures fellows. When Shelley is killed in a failed attempt to catch a live specimen, Friend finally decides to cut his losses and leave with the body of the dead Yeti. The Yeti close in on the two survivors, however, and Friend is killed in an avalanche.

Rollason takes refuge in an ice cave and watches in amazement as a number of Yeti arrive and take away the body of their fallen compatriot. He realises the Yeti are an intelligent species biding their time to claim the Earth when humanity has destroyed itself.

The rescue party finds Rollason and brings him back to the monastery where, when questioned by the Lama, he claims the expedition found nothing.

===Casting===
 ) and Tom Friend (Forrest Tucker) in a scene from The Abominable Snowman. The claustrophobic feel created by Val Guest in the film is evident.]] The Strange World of Planet X (1958) and The Trollenberg Terror (1958). 

*   (1952) and Beau Brummell (1954) as well as the Cartier/Kneale production of Nineteen Eighty-Four.  The Abominable Snowman was his second picture for Hammer; the first had been The Curse of Frankenstein (1957), the film that would bring him international fame and establish his long association with the Hammer horror brand.  Recalling how the cast and crew were entertained by Cushings improvisation with props, Val Guest said, “We used to call him Props Cushing, because he was forever coming out with props. When he was examining the Yeti tooth, he was pulling these things out totally unrehearsed and we found it very difficult keeping quiet”. 

The film also stars 
* Maureen Connell as Helen Rollason
* Richard Wattis as Peter Fox Robert Brown as Ed Shelley. 
* Michael Brill as Andrew McNee
* Wolfe Morris as Kusang
* Arnold Marlé as Lhama
* Anthony Chinn as Majordomo

Like Cushing, Arnold Marlé and Wolfe Morris reprised their roles from The Creature as the Lama and Kusang, respectively. 

==Production==
===Origins: The Creature=== Nineteen Eighty-Four live from Lime Grove Studios on Sunday, 30 January 1955 and a repeat performance was broadcast live the following Wednesday, 2 February.  The broadcast was not recorded and the only record of the production that survives is a series of screen images, known as tele-snaps, taken by photographer John Cura. 
 The Listener the television sequel. Val Guest, who had directed the two Quatermass films, was assigned to direct; this would be his third, and last, collaboration with Nigel Kneale. 

===Writing=== of the same name.  According to Kneale, Hammer wanted a title more literal than The Creature, which played on the ambiguity as to whether the real monster of the piece was the Yeti or its human pursuers, and settled on The Abominable Snowman.  The screenplay adds two characters: Rollasons wife Helen and his assistant Peter Fox.  The addition of the character of Helen, who is named after Cushings wife, was prompted by Cushings desire to flesh out Rollasons character by representing a womans point of view of his obsession with the Yeti.  Kneale was able to modify the ending of the story by using the characters of Mrs Rollason and Peter Fox to develop a subplot in which they mount a rescue mission for the expedition.  The characters of Pierre Brosset and Andrew McPhee are renamed as Ed Shelley and Andrew McNee respectively; these names were used by Kneale in early drafts of The Creature.  Although Kneale is the only credited screenwriter, Guest performed his own rewrite of the script in advance of the production, removing a lot of dialogue he felt to be unnecessary.  Guest said, “You cant have long speeches with people on the screen unless its a closing argument in a court case or something”. 

===Filming=== Barings banking location shoot Arthur Grant, Len Harris Mitchell BFC camera, which failed numerous times on account of the cold. 
 anamorphic wide screen format called CinemaScope|Regalscope, renamed "Hammerscope" by the company.  Val Guest found it an unsatisfactory format to work in, which made getting in close to the actors difficult and required careful framing of scenes.  This was the first film Arthur Grant worked on for Hammer as cinematographer and his reputation for being fast and cheap meant he soon replaced Jack Asher as Hammers regular cinematographer.  Just as he had done with the Quatermass films, Guest tried to give the film "an almost documentary approach of someone going on an expedition with a camera for Panorama (TV series)|Panorama or something".  To this effect, he made extensive use of hand-held camera and overlapping dialogue. 

  Bray and Pinewood studios. Bernard Robinson, Buddhist temple in Guildford to choreograph the monks chanting.  Most of the extras were waiters in Chinese restaurants in London.  It was realised early in production that there was insufficient space at Bray for the sets depicting the snowscapes of the Himalayas and so production shifted to Pinewood.  Each element of the set was built on a wheeled rostrum so the set could be reconfigured to show many different panoramic backdrops.  The set was decorated with artificial snow made of polystyrene and salt.  Matching the footage shot in the Pyrenees with the scenes filmed in Pinewood represented a major challenge for Guest and his editor Bill Lenny.  Guest had a Moviola editing machine brought on set so he could view scenes from the location shoot and synchronise them up with the scenes being shot at Pinewood. 

It was Val Guests view that the Yeti should be kept largely offscreen, bar a few glimpses of hands and arms, leaving the rest to the audiences imagination.  By contrast, Nigel Kneale felt that the creatures should be shown in their entirety to get across the message of the script that the Yeti are harmless, gentle creatures.  In the climactic scene where Rollason comes face to face with the Yeti, only the eyes are seen: Guest used Fred Johnson to play the Yeti in this scene, relying on his "eyes of worldly understanding" to convey the benign nature of the Yeti. 

===Music===
The musical score was provided by   (1948), composed by Ralph Vaughan Williams. 

==Reception==
The Abominable Snowman was released on 26 August 1957, with an   found it to be "among the best of British science-fiction thrillers".  Derek Prouse in The Sunday Times welcomed that fact that "for once an engaging monster is neither bombed, roasted nor electrocuted".  By contrast, Robert Kennedy of the Daily Worker said, "The film gets bogged down in its own contradictions, including the notion that the Yeti are at the same time horrific and super-sensitive".  William Whitebait in The New Statesman said, "The film lacks grip… the most authentic thrills are of men going mad and stumbling off into the snows where dead mens voices join the wail of the Yeti".  The critic in the Monthly Film Bulletin said the film was "on the whole a disappointingly tame and ineffectual screen version of Nigel Kneales intriguing TV play". 

The release of the film was overshadowed somewhat by the huge success of Hammers The Curse of Frankenstein, released the same year, and it was a relative financial failure, a fact Val Guest attributed to the intelligence of the script, saying, "It was too subtle and I also think it had too much to say. No one was expecting films from Hammer that said anything but this one did… audiences didnt want that sort of thing from Hammer.  This was the last film Hammer made in association with Robert L. Lippert; following the success of The Curse of Frankenstein, Hammer was now in a position to be able to deal directly with the major American distributors. 
 Bill Warren John Baxter felt that "in recreating a peak in the Himalayas, the set designer had more control over the film than the director, and despite some tense action the story drags".  Baxter acknowledged however that the film exercises "a certain eerie influence",  a view echoed by Hammer historians Marcus Hearn and Alan Barnes that "the film conveys a taut, paranoid atmosphere; set largely in wide open spaces, its remarkably claustrophobic in scale".  Nigel Kneales biographer, Andy Murray, finds the film "eerie and effective" and also suggests the scenes of the expedition members calling out to their lost colleagues across the wastelands influenced similar scenes in The Blair Witch Project (1999). 

==Remake== Let Me The Woman Matthew Read and Jon Croker.

==References==

===Notes===
 

===Bibliography===
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
*   at  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 