What! No Beer?
{{Infobox film
| name           = What! No Beer?
| image          =What No Beer.jpg
| image size     =
| caption        =
| director       = Edward Sedgwick
| producer       = Lawrence Weingarten (uncredited)
| writer         =
| narrator       =
| starring       = Buster Keaton Jimmy Durante
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer 1933
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
| preceded by    =
| followed by    =
}}
What! No Beer? is a 1933 comedy film starring Buster Keaton and Jimmy Durante, and directed by Edward Sedgwick.  The studio had also paired Keaton and Durante as a comedy team during this period in The Passionate Plumber and Speak Easily.

Mild-mannered Elmer (Keaton), a taxidermist, and gregarious Jimmy (Durante), a barber, are duped by a confidence woman (Phyllis Barry) and an underworld boss (John Miljan). Elmer and Jimmy reactivate an abandoned brewery in anticipation of the repeal of Prohibition. Their success prompts the crooks to get tough, but Elmer foils them with a truckload of beer barrels.

The filming of What! No Beer? was difficult. Since joining MGM in 1928, Keaton was not accorded the creative freedom that he had enjoyed during the silent era. By 1933 personal problems and a messy divorce were interfering with Keatons work; he often showed up drunk or not at all for the filming of What! No Beer? He was enough of a professional (and a trained acrobat) to complete the film, doing extreme pratfalls even while visibly impaired.

A myth persists that the Keaton talkies were critical and popular failures that virtually finished Keatons career. While it is true that these later Keaton efforts were artistically inferior to his best work, many of them were solid moneymakers. The Keaton series might have continued (MGM had already announced that Keaton and Durante would be co-starring with Jackie Cooper), but What! No Beer? turned out to be Keatons last MGM feature, and his last starring feature in the United States. Keaton then starred in 26 short subjects, and usually played featured roles after 1941.

==Plot==
Elmer J. Butts, taxidermist, goes to a dry rally, where he follows the beautiful Hortense and her gangster boyfriend Butch Laredo into the meeting hall. He sits by Hortense, only to be thrown out after the speaker asks if they want liquor back in this country, and he calls out “Yes!”

The next day, Jimmy Potts, driving a car covered in pro-booze stickers, brings a fish to Elmer’s shop for stuffing. Its Election Day, and theres a referendum on Prohibition on the ballot. Jimmy convinces Elmer to vote wet, and they go to the polls only to cause confusion and collapse the booths.

Later, at Jimmy’s barbershop, the radio reports that the country has voted to repeal Prohibition. At a hotel, a group of Spike Moran’s gangsters realize that their bootlegging operation is washed up. They wonder what Butch will do. At Butch’s place, Hortense asks if this means that she can’t have her Rolls Royce town car. Butch tells her she’ll be lucky to have a wheelbarrow, and he shoots the radio. Back at the barbershop, Jimmy breaks off of the celebratory conga line to tell Elmer his million dollar idea: buying a brewery. Elmer wants to be rich, too, so he can marry – and he has $10,000 hidden in his stuffed animals. They collect the money and take it to the president of the bank that foreclosed on the local brewery. Jimmy’s offer of $10,000 cash plus $5,00 a month is quickly accepted.

Elmer and Jimmy arrive at the brewery, toting bags of supplies. They find three unemployed homeless men there, and they hire them. After dousing themselves with an unpredictable water hose, they assemble the ingredients for a five gallon batch of beer in the huge tank. It only makes a small puddle at the bottom of the tank. They realize that they need 500 gallons, so after donning raincoats, they start work. Later, they open the vat. Suds bubble up over the top. They bottle as much as the can, having several mishaps with exploding bottles and foam piling up over their heads.

They put up a sign: “Real beer – 5 cents” and wait for customers. Instead the cops come in and raid them. The vote didn’t repeal Prohibition, it was only advisory. In court, the judge reads the charges and Jimmy protests that it’s persecution, but the chemist reports that it wasn’t beer, it was only brown dishwater.

On the stoop outside the brewery, Jimmy consoles Elmer on the loss of his nest egg. Jimmy goes in and talks to the workers, and one, Schultz, reveals that he’s a master brewer. To make back Elmer’s money, Jimmy decides to make real beer and tell Elmer that it’s near beer.

Weeks later, Spike and Butch meet to discuss who’s cut in on their racket. Butch vows to kill him.

Spike and an associate interrupt Elmer, who’s reading a book: Modern Salesmanship and Big Business. Spike offers to buy 1,000 barrels a day, and gives Elmer $10,000 down payment. Full of the advice from the book, Elmer agrees. Spike says that his partners stay partners as long as they live, and he leaves. Elmer tells the three workers that they need to increase production, then goes to the State Employment Bureau for 50 more men. After they start work, Jimmy comes in and learns about the contract; he has a meltdown. He puts the $10,000 in his overcoat pocket, which he hangs on the office coat rack, and leaves. Hortense drives up and pretends to turn her ankle, so Elmer must rescue her and carry her to the office. After she fakes a faint, he douses her with water, so she takes off her dress and puts on Jimmy’s overcoat. She vamps Elmer until he mentions that Spike is their partner. Having learned what she came for, she leaves. Jimmy comes in, looking for his overcoat. Elmer tells him that Hortense has it. When Jimmy tells him about the money, Elmer doesn’t mind: she’s the girl for whom he wants to make a million.

Hortense tells Butch that Spike is working with the brewers. When the $10,000 falls out of the coat, Butch calls her a tramp and hits her. She calls Elmer and he asks about the money. She denies seeing it, but he tells her to keep it and buy herself a Rolls. He asks her out on a walk in the park the next day. She’s appalled.

At Spike’s office, two men say that Butch threatened to kill them if they picked up the beer. Elmer volunteered to deliver it. At the brewery, Elmer drives the truck away and down the street. Butch’s men decide to kill him at the top of a hill, but the trucks’ tire blows out halfway up, and the barrels fall off of the back and chase the gangsters away. Jimmy arrives, and Elmer mourns the loss of the near beer. Jimmy explains that it was real beer, and they’re involved with gangsters. Elmer won’t leave town, because he’s got a date at the park.

Hortense and Elmer picnic, until a paperboy calls out the news: there’s a new gang war. Hortense kisses Elmer, sending him into the pond, and leaves. At the brewery, Jimmy learns that Butch killed Spike. Butch arrives and announces that now he’s their partner. Elmer comes in and tells them that Hortense loves him, but Butch asks “does she?” Meanwhile, the cops are planning to raid the brewery. Back at the brewery office, Hortense intercepts a man who’s going to tell Butch about the raid. On the brewery floor, Butch orders that no one may come in or out, and he posts guards on all of the doors. While giving Elmer the brush-off, Hortense slips him a note about the raid. Elmer escapes in a barrel, grabs a blackboard, and drives away. He shows what he’s written on the board to everyone on the street: Free Beer at the Brewery. The factory is mobbed, and by the time the police arrive, there’s no beer left.

Later, a Senator speaks to Congress, telling the story of a town in his state where the gangsters were put out of business when the people stormed the brewery. He calls for an end to Prohibition. After the headline “Beer Legalized,” crowds cheer, grain gets harvested, and beer gets made and delivered. At Butt’s Beer Garden, Elmer and Jimmy arrive in an open car. Jimmy offers free beer, and the two get mobbed for autographs. The crowd steals their clothes, too. Hortense joins the and asks if Elmer is hurt – he isn’t. Jimmy, holding a frosty brew aloft, addresses the camera: “It’s your turn next folks. It won’t be long now.” He blows off the head and chugs some down.

==Cast==
*Buster Keaton as Elmer Butts
*Jimmy Durante as Jimmy Potts
*Roscoe Ates as Schultz
*Phyllis Barry as Hortense
*John Miljan as Butch Lorado
*Henry Armetta as Tony
*Edward Brophy as Spike Moran
*Charles Dunbar as Mulligan
*Charles Giblyn as Chief
*Sidney Bracey as Dr. Smith, Prohibition speaker
*James Donlan as Al
*Al Jackson as Stool pigeon
*Pat Harman as Moran’s assistant

==External links==
* 
 
 
 
 
 
 
 