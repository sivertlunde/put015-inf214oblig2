Fast Film (film)
{{Infobox film
| name           = Fast Film
| image          =
| image_size     =
| caption        =
| director       = Virgil Widrich
| producer       = Philippe Bober (co-producer) Alexander Dumreicher-Ivanceanu (co-producer) Gabriele Kranzelbinder (co-producer) Bady Minck (producer) Nicolas Schmerkin (co-producer) Virgil Widrich (producer)
| writer         = Virgil Widrich (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Martin Putz
| editing        = Virgil Widrich
| studio         =
| distributor    =
| released       = 2003
| runtime        = 14 minutes
| country        = Austria Germany Luxembourg
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Fast Film is a 2003 Austrian/Luxembourgian film directed by Virgil Widrich. The film tells the story of a chase using scraps of other films.

== Plot summary ==
Bits of found film and different types of animation illustrate a classic chase scene scenario: A woman is abducted and a man comes to her rescue, but during their escape they find themselves in the enemys secret headquarters.

== Soundtrack ==
 

== Awards ==
The film was nominated for a Palme dOr for Best Short Film at the 2003 Cannes Film Festival and won several other awards. 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 