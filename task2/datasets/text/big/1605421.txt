Mondovino
{{Infobox film
| name = Mondovino
| image = Mondovino movie.jpg
| caption =
| director = Jonathan Nossiter
| producer = Jonathan Nossiter Emmanuel Giraud
| writer = Jonathan Nossiter
| associate editor = Laurent Gorse
| Associate producer, assistant to the director = Laurent Gorse Robert Parker Michel Rolland
| distributor = ThinkFilm
| released =  
| runtime = 135 minutes
| country = United States
| language = English, French
| budget =
}} 2004 documentary directed by American film maker Jonathan Nossiter. It was nominated for the Palme dOr at the 2004 Cannes Film Festival    and a César Award.
 globalization on Robert Parker and consultants like Michel Rolland in defining an international style. It pits the ambitions of large, multinational wine producers, in particular Robert Mondavi, against the small, single estate wineries who have traditionally boasted wines with individual character driven by their terroir.

Mondovino was originally intended to be a two-month affair as a break between feature projects upon the completion of Nossiters film Signs & Wonders (2000). The film gave Nossiter a chance to utilize his knowledge as a trained sommelier from his time working at Balthazar in New York as well as an opportunity to visit some of the great wine regions of the world.

==Production==
Mondovino was filmed with a hand held Sony PD-150 digital camcorder over the course of 4 years by Jonathan Nossiter with the assistance of Uruguayan filmmaker Juan Pittaluga and  Caribbean photographer Stephanie Pommez for a budget around $400,000. Over 500 hours of original film was shot at locations in seven countries on three continents in five languages (French, Italian, Spanish, English and Portuguese). The footage from the handheld DV was blown up and transferred to 35&nbsp;mm by Tommaso Vergallo.

The film was shot entirely in single camera, about 60% of the time operated by Nossiter with the camera on his hip while he is conversing with the subject. The film features no narration. The cinematography does frequently employ "intense" zooms, sometimes right up to the subjects eyeballs, which Nossiter explains as a necessary means to keep the handheld camera in focus. 

Mondovino opened in France November 3, 2004 and received mostly positive reviews among film critics and lots of buzz (both positive and negative) among the French wine industry. It was later released in the United Kingdom on December 10 of that year and in U.S. release March 23, 2005. On July 12, 2005, the DVD version was released, including Director Commentary as well as a Bonus featurette Quo Vademus?, an episode of a ten-hour television series that is scheduled for an eventual DVD release of its own.

==Film festivals==
Mondovino earned a rare competition slot in the Official Selection of the 2004 Cannes Film Festival, one of only four documentaries ever nominated for the Palme dOr in the history of the festival. This is the third major competition selection for Jonathan Nossiter, with Signs & Wonders in Berlin in 2000 and Sunday (1997 film)|Sunday, winner of the Sundance Film Festival in 1997. Mondovino was promoted to its competition slot from a non competitive category just hours before the deadline by Cannes artistic director Thierry Frémaux. In May 2004, it was featured at Cannes with a run time of 2 hours 49 minutes. The film was edited down to 2 hrs 15 minutes after the screening.

Other notable Film Festivals that featured Mondovino and subsequent nominations, honors & awards
* Bangkok International Film Festival 2005: Windows on the World
* French Film Festival in Australia 2005: Screening
* London Film Festival 2004: Documentary Gala
* Era New Horizons Film Festival 2005 (Cieszyn, Poland)
* Titanic International Filmpresence Festival 2005 (Budapest, Hungary)
* Copenhagen International Documentary Film Festival 2004 (Copenhagen, Denmark)
* Deauville Festival of American Film 2004 (Deauville, France)
* Toronto International Film Festival 2004 (Toronto, Canada)
* Vienna International Film Festival 2004 (Vienna, Austria)

==Critical Review==
In the US, Mondovino received mostly positive reviews with 70% positive rating given by film critics featured on the website Rotten Tomatoes.  Some notable reviews:
* Peter Travers, Rolling Stone: "Although Nossiter set out merely to find the characters behind the wine industry, he ended up with a poignant look at some important issues, including deforestation, the corporation versus the independent company and even communism. The result is an inside examination of a world very few people see." 
* Wesley Morris, Boston Globe: "But what is it that Nossiter wants us to know about this world and its inhabitants? We visit lots of places but what do we see? The indictments, recriminations, and musings just sit there, and the movie feels incomplete and uncentered. Its like a grand magazine profile thats all reportage and absolutely no prose." 
* Michael Wilmington, Chicago Tribune: "Its shot simply and cheaply, a model of how to use the new, lightweight equipment and shooting methods. But its such a knowledgeable work and so pleasantly obsessed with its subject that it will interest even audiences whose attraction to wine is only casual. And it may, like "Sideways," make you a little thirstier when its over." 
* Peter DeBruge, Miami Herald: "Mondovino is an earnest but unfocused attempt to rescue winemaking from the hands of profit-mongering capitalists. Too passive-aggressive to qualify as a proper exposé, the movie suggests the world has lost the art of appreciating fine wines, thanks to the proliferation of a popular American style in which the flavor imparted by new wood barrels overpowers the individual terroir, or region-specific quality, that gives each wine its personality." 

==Vineyard locations in Mondovino==
 (In order of appearance in film) 
*Domaine de Souch (6 hectares), Jurancon, France (Pyrenees)
*Giovanni Battista Columbu, Bosa, Sardinia
*Château Le Gay, Pomerol, Bordeaux
*Mas de Daumas Gassac, Aniane, Languedoc (South of France)
*Château Clinet, Pomerol, Bordeaux
*Robert Mondavi Winery, Napa Valley, California
*Staglin Family Vineyards (18 hectares), Napa Valley, California
*Domaine de Montille (8 hectares), Burgundy (region)|Burgundy, France
*Taillepieds Vineyard (Domaine de Montille), Volnay, Côte-dOr|Volnay, France
*Opus One Winery, Napa Valley, California
*Château Petit-Village, Pomerol, Bordeaux
*Château Rouget, Pomerol, Bordeaux
*Château Valandraud, Saint-Émilion, Bordeaux
*Ornellaia Winery, Bolgheri, Tuscany
*Bianchetti Winery, Pernambuco, Brazil
*Bodega San Pedro de Yacochuya, Cafayate, Argentina
*Domaine de la Romanée-Conti, St. Vivant, Burgundy. (Bonus featurette) La Tâche Vineyard, St. Vivant, Burgundy. (Bonus featurette)
*Domaine Lafarge, Volnay, France. (Bonus featurette)

==See also==
*A State of Vine (film)

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 
 
 