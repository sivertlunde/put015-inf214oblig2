Ormayil Nee Maathram
{{Infobox film 
| name           = Ormayil Nee Maathram
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = R Devarajan
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Jayabharathi Jayasumathi Priyamvada
| music          = G. Devarajan
| cinematography = C Ramachandra Menon
| editing        =
| studio         = Sreevardhini Productions
| distributor    = Sreevardhini Productions
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by J. Sasikumar and produced by R Devarajan. The film stars Prem Nazir, Jayabharathi, Jayasumathi and Priyamvada in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir
*Jayabharathi
*Jayasumathi
*Priyamvada
*Sudheesh

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mundirichaarinu lahariyundo || P. Madhuri || Yusufali Kechery || 
|-
| 2 || Paathiraavin neelayamuna || K. J. Yesudas || Yusufali Kechery || 
|-
| 3 || Sneham daivam ezhuthiya || P Susheela, Raju Felix || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 