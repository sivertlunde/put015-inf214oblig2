Last Days in the Desert
 
{{Infobox film
| name           = Last Days in the Desert
| image          = Last Days in the Desert poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        =  Rodrigo García
| producer       = Bonnie Curtis Julie Lynn Wicks Walker
| writer         = Rodrigo García
| starring       = Ewan McGregor Tye Sheridan Ciarán Hinds Ayelet Zurer
| music          = Danny Bensi Saunder Jurriaans
| cinematography = Emmanuel Lubezki
| editing        = Matt Maddox
| studio         = Division Films Mockingbird Pictures
| distributor    = 
| released       =  
| runtime        = 98 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Rodrigo García. It stars Ewan McGregor, Tye Sheridan, Ciarán Hinds and Ayelet Zurer. The film premiered at the 2015 Sundance Film Festival on January 25, 2015.

== Cast == The Devil
* Tye Sheridan as Son
* Ciarán Hinds as Father
* Ayelet Zurer as Mother

== Production == Deadline that Rodrigo García and Ewan McGregor. He said "Its a tiny little beautiful, extraordinary script that Rodrigo wrote that we’re going to shoot for five weeks."  On February 5, 2014 two actors, Ewan McGregor and Tye Sheridan, joined the film cast. McGregor plays the dual roles of a holy man and a demon.    The film is written and directed by Rodrigo García, while Division Films and Mockingbird Pictures produced the film.  Ayelet Zurer and Ciarán Hinds play the roles of Sheridans characters parents.  The film premiered at the 2015 Sundance Film Festival.  The movie was shot in Anza Borrego. 

The film was awarded the Dolby Family fellowship, a grant that allows filmmakers to finish their sound design and mix in Dolby Atmos.  Sound Designers and Re-recording mixers J.M. Davey and Zach Seivers completed the original sound design and mix as well as the Dolby Atmos remix. Skip Lievsay, who won the Academy Award for Best Sound for his work on Gravity (film)|Gravity, served as a mentor to Davey and Seivers for the Atmos remix. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 