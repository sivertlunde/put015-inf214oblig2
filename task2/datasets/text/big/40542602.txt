The Hound That Thought He Was a Raccoon
 

{{Infobox film
| name           = The Hound That Thought He Was a Raccoon
| image          = 
| image_size     = 
| caption        = Film poster cover
| directors      = Tom McGowan
| producer       = Winston Hibler
| based on       = an original story by Rutherford Montgomery
| screenplay     = 
| starring       = Rex Allen
| music          = Buddy Baker
| sound          = Robert O. Cook
| cinematography = 
| editing        = George Gale
| studio         = Walt Disney Productions Buena Vista Distribution
| released       = August 10, 1960
| runtime        = 47 minutes
| country        = United States of America
| language       = English
| budget         =
}}

The Hound That Thought He Was a Raccoon is a 1960 Walt Disney film directed by Tom McGowan.
 truck and rabbit hutch. cage and escapes for a nighttime romp with Nubbin, during which he wrecks Jeffs workshop. When an iron rod falls against a grinding wheel Weecha has accidentally started, sparks fly and start a fire, but Weecha races back to his cage. After Jeff is awakened by his hounds barking and extinguishes the fire, he discovers Weecha and Nubbin sleeping innocently. Three weeks later, Jeff begins to train Nubbin and his other young hounds to locate raccoons by following their scent. At first Jeff uses a raccoon skin as bait, but when he switches to Weecha, Nubbin refuses to work and fights off all the other dogs. Several months later, Jeff is invited on a coon hunt in a neighboring valley and leaves with his favorite hound, Rounder. Nubbin and Weecha are now full grown although Weecha is still a captive in the rabbit hutch. However, the hound helps his friend to escape and Weecha, having learned how to manipulate the latches on the hutches, releases all of Jeffs rabbits. Unfortunately, he steps on the trigger of a shotgun and the shot alerts the hounds, who break out of their enclosure to chase the rabbits. Weecha hides inside a barrel, which the dogs roll over just as Jeff returns with Rounder. Weecha manages to escape but is pursued by Rounder. While fighting in a pond, Weecha grabs Rounder by the neck and almost drowns him, then escapes. By autumn, Weecha has completely reverted to the wild and survives by stealing duck eggs, as well as nuts and berries from pine squirrel nests. In the spring, as Jeff and his friends begin a new hunting season with plans for a kill, Weecha finds and courts a mate, Waheena. When Weecha hears the hounds approaching, he instinctively uses a decoy trick learned from his father to draw the pack away from his mate. Nubbin is the leader of the pack, and when he corners Weecha, he recognizes his old friend and barks happily. Unfortunately, Nubbins barking attracts the other dogs and he finds himself fighting his own kind in order to protect his friend. After Jeff arrives and sizes up the situation, he asks the other hunters to call off their dogs. As Weecha runs off, followed by Nubbin, Jeff explains their history. When Nubbin finds Weecha with his mate, Weecha chases him away, and Nubbin, realizing that their friendship can no longer continue, vows never to hunt Weecha again and returns to Jeff, his friend and master.

==References==
* 
* 

 
 
 
 
 
 