Come Back, Little Sheba (1952 film)
{{Infobox film 
| name     = Come Back, Little Sheba
| image    = Come back little sheba.jpeg
| caption  = Original film poster
| director = Daniel Mann
| producer = Hal B. Wallis
| writer   = Ketti Frings William Inge (play) Terry Moore
| music    = Franz Waxman
| cinematography = James Wong Howe
| distributor = Paramount Pictures
| released =  
| runtime  = 99 minutes
| language = English
| gross = $3.5 million (US) 
}} Terry Moore and Richard Jaeckel. The title refers to the wifes little dog that became lost months before the story begins and which she still openly misses.
 1950 play of the same title by William Inge and was directed by Daniel Mann (making his directorial debut).

==Plot==
"Doc" Delaney (Burt Lancaster) is a recovering alcoholic who, in his younger days, was a promising medical student who dropped out of school to take a job to support a pregnant Lola, whose premarital intercourse with Doc caused her to be thrown out of her familys house by her father.  Doc, thinking he was doing the right thing, married Lola (Shirley Booth).  The child later died, and in the process rendered Lola unable to have any further children, leaving the couple childless. As a result, Doc turned to drinking excessively, causing him to have bouts of anger and murderous rage, and later drinking away a sizeable inheritance from his parents. He eventually joined Alcoholics Anonymous and was able to quit drinking, though he still keeps a bottle in the house to remind him of his past life.
 Terry Moore) is a young college student who rents a spare room from the Delaneys.  One day she brings home a young man, Turk (Richard Jaeckel), who is a star on the track team. Hes wearing his track outfit which shows off his muscles, as she intends to use him as a figure model for a poster shes doing for a major athletic competition being held in the town.  Mrs. Delaney encourages the couple in their "modeling session", though Doc, who walks in to find Turk underdressed, thinks it borders on pornography.  Later, after Marie and Turk leave, it is revealed that Marie is promised to be married to another man, Bruce, who is away but due to return soon.

As Maries infatuation with Turk grows, Doc becomes more agitated. Lola reminds him that Marie is so much like Lola in her younger days, when she was young and pretty and Doc was dashing and handsome, before Lola became "old, fat, and sloppy". Doc calms down but still voices his disagreement over Marie seeing another boy while Bruce is away.

One night, Turk and Marie return from a school dance. Marie forgot her key, so Turk enters through a window, unlocks the front door, and lets Marie in. They sneak into Maries room. Meanwhile, this has been witnessed by Doc, who believes that they are up to no good, and he begins to remember what he was like with Lola.  He goes back to the kitchen and reaches for his bottle hidden in the cupboard. Back in Maries room, she changes her mind about Turk and asks him to leave.  He accuses her of being a big tease and is about to force his way onto her when he changes his mind and leaves, unseen by Doc, through a window.

The next morning Doc takes the whiskey he has not touched for a year from the cabinet, and returns much later that night, drunk and angry at what he perceives to be Maries infidelity.  Doc is in a murderous rage and attacks Lola with a knife before she manages to call two of Docs support group friends to come take him to the hospital just as Doc follows her into the parlor. Doc stumbles and drops the knife, but passes out while trying to choke Lola.  A neighbor hears the commotion and comes running over, just as the two men come to take Doc away.

After Doc is carried off, a shaken Lola calls her mother to see if she can stay there for a few days, but is told that her father still will not welcome her into the family house. Her mother offers to come to Lolas, but Lola declines.

Bruce returns and carries Marie away, where they get married. Her drawing was featured in the athletic poster and Turk becomes the star of the competition. Doc returns from the hospital and is welcomed by Lola. Doc realizes that he loves Lola after all, and begs her never to leave him. Lola promises to stay with him forever.

==Cast==
* Burt Lancaster - Doc Delaney
* Shirley Booth - Lola Delaney Terry Moore - Marie Buckholder
* Richard Jaeckel - Turk Fisher
* Philip Ober - Ed Anderson
* Edwin Max - Elmo Huston
* Lisa Golm - Mrs. Coffman
* Walter Kelley - Bruce

==Reception==

Come Back, Little Sheba was entered into the 1953 Cannes Film Festival.   

===Academy Awards===
;Wins Best Actress: Shirley Booth

;Nominations Best Actress in a Supporting Role: Terry Moore
*  

==Other versions== television version of the original play was made in 1977, starring Laurence Olivier, Joanne Woodward and Carrie Fisher. It was directed by Silvio Narizzano.

The play was integrated into a sketch on the Colgate Comedy Hour, starring Dean Martin, Jerry Lewis and Burt Lancaster.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 