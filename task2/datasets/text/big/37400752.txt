The Coolangatta Gold (film)
{{Infobox film
| name           = The Coolangatta Gold
| image          = COOLANGATTAGOLD(film).jpg
| image size     =
| caption        = 
| director       = Igor Auzins
| producer       = John Weiley
| writer         = Peter Schreck
| based on       = developed from an idea by Max Oldfield
| story by       = Ted Robinson Peter Schreck
| narrator       =
| starring       = Joss McWilliam Colin Friels Nick Tate Josephine Smulders Robyn Nevin
| music          = Bill Conti
| cinematography = Keith Wagstaff
| editing        = Tim Wellburn
| studio = Hoyts Edgley
| distributor    = Hoyts Umbrella Entertainment
| released       = 1984
| runtime        = 112 minutes
| country        = Australia English
| budget         = AU $5.6 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p357-359 
| gross = AU $1,365,000 
| preceded by    =
| followed by    =
}}
The Coolangatta Gold is a 1984 Australian film which led to the establishment of the iron man race The Coolangatta Gold. 

==Plot==
Joe Lucas is determined that his son Adam becomes a champion iron man. He neglects his younger son, Steve, who is an aspiring band manager and enjoys karate, as well as being his brothers training partner.

The leading iron man event is The Coolangatta Road, an arduous competition for $20,000 prize money. The favourite for this event is champion iron man Grant Kenny. Kennys father beat Joe Lucas for the iron man title in 1960.

Steve falls in love with a ballet dancer, Kerri, who inspires him to compete with his brother in The Coolangatta Gold.

==Cast==
*Joss McWilliam as Steve Lucas
*Nick Tate as Joe Lucas
*Colin Friels as Adam Lucas
*Josephine Smulders as Kerri Dean
*Robyn Nevin as Roslyn Lucas
*Grant Kenny as himself
*Melanie Day as Gilda
*Melissa Jaffer as Ballet Teacher
*Wilbur Wilde as one of The Band
*Kate Mailman as a child on beach

==Production==
Writer Peter Schreck and director Igor Auzins had worked together successfully on We of the Never Never (1982) and decided to collaborate on another project. They wanted to do a contemporary love story in the sporting genre, and originally thought of doing a $1.5 million film shot in Bondi. However after a few days it became obvious they wanted to do something more ambitious. Auzins and Schreck formed a company, Angoloro Productions, with John Weiley as a third partner in December 1982. They then approached Hoyts Edgley who agreed to finance within 24 hours. Jim Schembri, "Peter Schreck", Cinema Papers, Feb-March 1985 p35-37, 84-85 

==Reception==
The film performed disappointingly at the box office. Jonathan Chissick, managing director of Hoyts, said "I thought it was the best script to come out of Australia. So, we failed there somewhere. I dont want to point my finger but there was obviously a failure." 
 The Late Show. The sketch "The Last Aussie Auteur: A Tribute to Filmmaker Warren Perso" features excerpts from various (fictional) Australian films, one being a box-office flop titled The Bermagui Bronze.

==Home Media==
The Coolangatta Gold was released on DVD by Umbrella Entertainment in November 2012. The DVD is compatible with all region codes and includes special features such as a photo gallery, the making of The Coolangatta Gold and a Good Morning Australia segment.   

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 