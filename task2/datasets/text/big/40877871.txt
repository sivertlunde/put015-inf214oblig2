Penitentiary (1938 film)
{{Infobox film
| name           = Penitentiary
| image          = Penitentiary 1938 film poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = John Brahm
| producer       = Robert North
| screenplay     = Seton I. Miller Fred Niblo, Jr.
| based on       =  
| narrator       =  John Howard Jean Parker Robert Barrat
| music          = Morris Stoloff
| cinematography = Lucien Ballard
| editing        = Viola Lawrence
| distributor    = Columbia Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} John Howard, 1929 stage play The Criminal Code by Martin Flavin, after Howard Hawks The Criminal Code (1931) and followed by Henry Levins Convicted (1950 film)|Convicted (1950).

==Plot==
William Jordan (Howard) is befriended by the man who sent him to prison on a manslaughter charge, former DA now prison warden Matthews (Connolly).  In order to give Jordan the opportunity to rehabilitate himself Matthews allows him to work as chauffeur to his daughter Elizabeth (Parker), though hes a bit uncomfortable when  Elizabeth falls in love with the young convict. All of this extra effort goes out the window when Jordan, adhering to the "criminal code" of never snitching on a fellow con, allows himself to be implicated in the murder of another convict. Jordan is saved from the death penalty by a last-minute confession of his hard-bitten but honorable cellmate.

==Cast==
 
 
* Walter Connolly as Thomas Matthews 	  John Howard as William Jordan 	 
* Jean Parker as Elizabeth Matthews 	 
* Robert Barrat as Captain Grady 	 
* Marc Lawrence as Jack Hawkins 	 
* Arthur Hohl as Finch 	 
* Dick Curtis as Tex 	 
* Paul Fix as Runch 	 
* Marjorie Main as Katie Matthews 	 
* Edward Van Sloan as Dr. Rinewulf 	 
* Ann Doran as Blanche Williams
* Dick Elliott as McNaulty
  
* Charles Halton as Leonard Nettleford 	 
* Thurston Hall as Judge 	 
* Ward Bond as Red, prison barber 	 
* John Gallaudet as States attorney 	 
* Stanley Andrews as Captain Dorn 	 
* James Flavin as Doran 	 
* Perry Ivins as Lou 	 
* Gennaro Curci as Spelvin Al Hill as Kitchen trusty Bruce Mitchell as Bailiff 
* George Magrill as Richard Robert Allen as Doctor
 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 