America is Still the Place
 
 

America is Still the Place (2015) is a North American feature film based on the book "America is still the place", written by Charlie Walker. The film recounts the racial injustice Mr. Walker endured in an effort to clean up Stinson Beach in Marin County, California after an oil spill threatened the beach town and much of the Bay Areas coast line. Starring Mike Colter and Dylan Baker, the film was written and directed by Patrick Gilles.
 

 
 

 