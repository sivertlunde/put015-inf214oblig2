Immoral Women
{{Infobox film
| name           = Immoral Women
| image          = Les héroïnes du mal.jpg
| image size     = 175px
| caption        = Official Teaser Poster
| director       = Walerian Borowczyk
| producer       = Pierre Braunberger
| writer         = Walerian Borowczyk André Pieyre de Mandiargues 
| narrator       = 
| starring       = Françoise Quéré Marina Pierro Jean-Claude Dreyfus
| music          = Philippe dAram Olivier Dassault 	
| cinematography = Bernard Daillencourt
| editing        = Walerian Borowczyk
| studio         = Argos Films Films du Jeudi
| distributor    = Argos Films
| released       = March 07, 1979
| runtime        = 114 minutes
| country        = France French Italian Italian
| budget         = 
| preceded by    = 
| followed by    = 
}} French erotic erotic drama drama directed by Walerian Borowczyk,  written by Borowczyk and André Pieyre de Mandiargues  and starring Jean-Claude Dreyfus, Marina Pierro and Françoise Quéré. 

==Synopsis==
The film is divided into three self-contained episodes, set in different time periods and featuring female protagonists whose names all start with letter M as in the word mal (evil) and who commit crimes of passion.
* "Margherita", the ambitious mistress of the painter Raphael. Rome, 1520.
* "Marceline", the desirous adolescent daughter of a bourgeois family. Fin de siècle France.
* "Marie", the clever wife of a wealthy gallery owner. Modern day Paris.

==Cast==
* Marina Pierro as Margherita Luti
* Gaëlle Legrand as Marceline Caïn
* Pascale Christophe as Marie
* François Guétary as Raphael Sanzio
* Jean-Claude Dreyfus as Bini
* Jean Martinelli as Pope
* Pierre Benedetti as Mad Painter
* Philippe Desboeuf as Doctor
* Noël Simsolo as Julio Romano
* Roger Lefrere	as Michelangelo
* Gérard Falconetti as Tomaso
* Hassane Fall as Petrus
* France Rumilly as Madame Cain
* Yves Gourvil as Cain
* Lisbeth Arno as Floka
* Gérard Ismaël	as Antoine
* Henri Piégay as Husband
* Mathieu Rivollier		
* Robert Capia		
* Daniel Marty		
* Jacky Baudet		
* Sylvain Ramsamy		
* Jean Boullu		
* Françoise Quéré		
* Mazouz Ould-Abderrahmane		
* Bernard Hiard

==Soundtrack==
The score was composed by Philippe dAram and Olivier Dassault. 

==Release==
The film premiered on 7 March 1979 in a cinema release. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 
 