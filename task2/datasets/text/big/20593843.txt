Major Saab
{{Infobox film
| name           = Major Saab
| image          = Major_Saab.jpg
| image size     =
| caption        = VCD Cover
| director       = Tinnu Anand
| producer       = Amitabh Bachchan
| story          = Robin Bhatt
| narrator       =
| starring       = Amitabh Bachchan Ajay Devgan Sonali Bendre Nafisa Ali
| music          = Anand Raj Anand Aadesh Shrivastav
| cinematography = Ravi K. Chandran
| editing        = Prashant Khedekar
| distributor    =
| released       = June 26, 1998
| runtime        = 165 minutes
| country        = India
| language       = Hindi
}}

Major Saab is a 1998 Hindi film starring Amitabh Bachchan, Ajay Devgan, Sonali Bendre and Ashish Vidhyarthi.

==Plot==
The plot concerns playboy Virendra Pratap Singh (Ajay Devgan). A clause in the will of his wealthy father forces him to enter a military academy in order to inherit. Virendras repeated attempts to get himself discharged are foiled by Major Jasbir Singh Rana (Amitabh Bachchan). Then Virendra falls in love with Nisha (Sonali Bendre), the sister of a gangster named Shankar (Ashish Vidhyarthi). Shankar wants Nisha to marry the son of his friend Parshuram Bihari and hatches a plot to separate the couple, by pretending to accept Virendra, however soon beats him up badly & attempts to break both his arms and legs. Now Major Jasbir trains Virendra to get back into his strength, trains him to fight against Shankar, and win over Nisha.

==Cast==
* Amitabh Bachchan as Maj. Jasbir Singh Rana
* Ajay Devgan as Virendra Pratap Singh aka Veer
* Sonali Bendre as Nisha
* Nafisa Ali as Dr. Priya Rana
* Ashish Vidhyarthi as Shankar
* Mohan Joshi as Parshuram Bihari
* Shahbaaz Khan as Vicky
* Naveen Nischol as Brig. Satish Khurana
* Mushtaq Khan as Hanuman Prasad
* Avtar Gill as Commissioner of Police
* Dinesh Hingoo Special Appearance as Jai Singhaniya (Virendras solicitor)
* Kulbhushan Kharbanda as Raja Thakur (Guest Appearance)
* Siddharth Ray
*  Jasbir Thandi  As Cadet Amrik singh

==Box Office== Bollywood films of 1998).

==Soundtrack==
The music is composed by Anand Raj Anand and Aadesh Shrivastav. Lyrics are penned by Anand Raj Anand and Dev Kohli.

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = yes
| title1 = Akeli Na Bazaar Jaya Karo
| extra1 = Udit Narayan
| music1 = Anand Raj Anand
| length1 = 05:49
| title2 = Deewana Ban
| extra2 = Sudesh Bhosle
| music2 = Anand Raj Anand
| length2 = 03:18
| title3 = Pyaar Karna Hai 
| extra3 = Alka Yagnik, Anand Raj Anand
| music3 = Anand Raj Anand
| length3 = 04:50
| title4 = Pyaar Kiya Toh Nibhana (I)
| note4 = 
| extra4 = Udit Narayan, Anuradha Paudwal
| music4 = Anand Raj Anand
| length4 = 03:04
| title5 =  Pyaar Kiya Toh Nibhana (II)
| note5 =
| extra5 = Anuradha Paudwal
| music5 = Anand Raj Anand
| length5 = 03:04
| title6 = Sona Sona
| note6 =
| extra6 = Sudesh Bhosle, Sonu Nigam, Jaspinder Narula
| music6 = Aadesh Shrivastav
| length6 = 06:48
| title7 = Tere Pyar Mein
| note7 =
| extra7 = Kumar Sanu
| music7 = Anand Raj Anand
| length7 = 04:38
}}

==References==
  

==External links==
* 
* 
* 

 
 