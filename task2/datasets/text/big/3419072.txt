Abraxas, Guardian of the Universe
{{Infobox film
| name           = Abraxas, Guardian of the Universe
| image          = Abraxasdvd.jpg
| image size     =
| caption        = DVD cover for the film
| director       = Damian Lee
| producer       = Damian Lee Reid Dennison William Dever Armand Leo David Mitchell Curtis Petersen Alexander Ruge 
| writer         = Damian Lee
| narrator       =
| starring       = Jesse Ventura, Sven-Ole Thorsen, Damian Lee, Jerry Levitan, Marjorie Bransfield, Ken Quinn, James Belushi
| music          = Carlos Lopes
| cinematography = Curtis Petersen	
| editing        = Reid Dennison	
| studio         = Rose & Ruby Productions
| distributor    = Cineplex Odeon Films
| released       = December 18, 1990    February 28, 1991    March 1, 1991  
| runtime        = 90 minutes
| country        = United States, Canada English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Jesse "The Body" Ventura and Sven-Ole Thorsen, with a cameo by James Belushi.

==Plot summary==

Abraxas (Jesse Ventura) and Secundus (Sven-Ole Thorsen), are intergalactic police officers, or Finders, from a planet called Sargacia. Their race is physically similar to humans but with an expanded lifespan; Abraxas has been a Finder for almost 10,000 years. Each Finder is equipped with an Answer Box, which serves as a communicator and scanner. It can also detect any object from a distance based on the objects vibration. When testing for the Anti-Life Equation, the subject being scanned will disintegrate if they do not contain the equation.

Secundus wants to access a negative universe which he believes will give him omnipotent powers and make him immortal. To do this he needs the solution to the Anti-Life Equation. He travels to Earth and impregnates the first human female he finds, Sonia Murray (Marjorie Bransfield), simply by holding his hand over her belly.  The resulting baby will be a prodigy able to solve the equation. Only a few minutes later, Sonia gives birth to a boy and names him Tommy. Meanwhile, Abraxas corners Secundus so other Finders can lock onto their location and transport Secundus to the prison planet Tyrannus 7. Abraxas is ordered to kill Sonia before she can give birth, but he cant bring himself to do it and leaves her with Tommy. When Sonia goes home her parents kick her out of their house since she doesnt know who the babys father is. They dont seem to mind that the baby was conceived and born on the same day, though.

Five years later, Tommy doesnt speak but does have strange abilities; when he is picked on at school, he makes the bully wet his pants. The school principal, who is probably Sucundus, played by James Belushi, calls Sonia in about this problem, but she refuses to admit that Tommy has problems. Secundus escapes from the prison and teleports to Earth. The Finders send Abraxas right after Secundus with the same technology, but their transport paths cross and their weapons are destroyed. Abraxas chases Secundus, but loses him; Secundus uses the fuse box at an automotive shop to recharge his Answer Box. When the owner confronts him, he uses his Answer Box to test the shop-owner about the Anti-Life Equation. The test causes the owner to explode. Secundus then goes on a rampage, stealing cars, killing innocent people and causing chaos. He continues to scan people, looking for the chosen one who knows the Anti-Life Equation.

He is pursued by Abraxas, who isnt able to do much as Secundus escapes and finds Sonias residence.  There, he sees the child with one of Sonias friends, who is currently watching the house while Sonia and Tommy are out at a movie, and tries to test the equation on him.  But Abraxas arrives before any harm can be done to the boy and they fight once more.  However, Abraxas is stabbed in the stomach and Secundus left after saying he will let Abraxas live to see everything destroyed.  Sonia returns home with Tommy, and finds Abraxas there in the living room.  She expresses her anger about being impregnated by Secundus to him, but is convinced by Abraxas to drive somewhere safe with him and Tommy.

==Cast==
* Jesse Ventura as Abraxas Sven Ole-Thorsen as Secundus 
* Damian Lee as Dar
* Jerry Levitan as Hite
* Marjorie Bransfield as Sonia Murray
* Ken Quinn as Carl
* Marilyn Lightstone as Abraxas Answer Box (voice)
* Moses Znaimer as Secundus Answer Box (voice)
* Robert Nasmith as Sonias Father
* Kris Michaels as Sonias Mother
* Layne Coleman as Registry Clerk Francis Mitchell as Tommy Murray
* Sonja Belliveau as Doctor Scundundas Jim Belushi as Principal Latimer
* Erica Heenan as School Secretary

== References==
 
== External links ==
*  

 

 
 
 
 
 
 