Man in the Sky
{{Infobox film
| name           = Man in the Sky
| image_size     = 
| image	=	Man in the Sky FilmPoster.jpeg
| caption        = A poster with the films US title: Decision Against Time
| director       = Charles Crichton
| producer       = Michael Balcon William Rose (story and screenplay)
| starring       = Jack Hawkins Elizabeth Sellars
| music          = 
| cinematography = Douglas Slocombe
| editing        =  Ealing Films
| distributor    = Metro-Goldwyn-Mayer
| released       = January 1957
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = $486,000  . 
| gross          = $500,000 
}} 1957 film starring Jack Hawkins and produced by Ealing Studios, although the on - screen credit was changed to Ealing Films as this was the first Ealing production to be made at MGM British Studios following the sale of the Ealing site.

==Plot==
Test pilot John Mitchell (Jack Hawkins) disappoints his wife Mary (Elizabeth Sellars) by refusing to increase their unsuccessful bid for a house. What she does not know is that the aircraft manufacturing company he works for is in desperate financial straits. Owner Reginald Conway (Walter Fitzgerald) needs to convince Ashmore (Eddie Byrne) to place an order soon or the firm will go bankrupt.

Mitchell takes the only prototype of a new aeroplane for a flight, with Ashmore and several others aboard. During testing, one engine catches fire. Ashmore and the others parachute to safety. Then, despite Conways order and the urgings of others, Mitchell decides to try to land the aeroplane rather than crashing it into the sea. However, he has to fly back and forth for half an hour to use up the fuel.

During the tense wait, co-worker Mrs. Snowden (Megs Jenkins) takes it upon herself to notify Mitchells wife. Mary goes to the airfield and watches as her husband manages to land safely. Later, at home, she demands to know why he risked his life when everyone told him to bail out. He explains that it was his duty and the companys fate hung in the balance. Then he phones their real estate agent and agrees to the sellers price.

==Cast==
*Jack Hawkins as John Mitchell
*Elizabeth Sellars as Mary Mitchell
*Jeremy Bodkin as Nicholas Mitchell, Johns young son
*Gerard Lohan as Philip Mitchell, Johns other young son
*Walter Fitzgerald as Reginald Conway John Stratton as Peter Hook
*Eddie Byrne as Ashmore
*Victor Maddern as Joe Biggs
*Lionel Jeffries as Keith
*Donald Pleasence as Crabtree
*Catherine Lacey as Marys Mother
*Megs Jenkins as Mrs. Snowden
*Ernest Clark as Maine
*Raymond Francis as Jenkins
*Russell Waters as Sim
==Reception==
According to MGM records the film earned $150,000 in the US and Canada and $350,000 elsewhere, resulting in a loss of $176,000. 
==References==
 
==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 