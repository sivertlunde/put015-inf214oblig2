T-Bird Gang
{{Infobox film
| name           = T-Bird Gang
| image_size     =
| image	=	T-Bird Gang FilmPoster.jpeg
| caption        =
| director       = Richard Harbinger
| producer       = Stanley Bickman
| writer         = Screenplay by Tony Miller and John Brinkley
| narrator       = Edwin Nelson Tony Miller Russ Freeman Charley Mariano Stu Williamson
| cinematography = Larry Raimond
| editing        = Marvin Walowitz
| distributor    = The Filmgroup Copyright 1958 by Sparta Productions
| released       = October 16, 1959
| runtime        = 65 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

T-Bird Gang is a 1959 American film directed by Richard Harbinger.

==Production==
Shot in 1958 under the title of Cry Out in Vengeance, executive producer Roger Corman released the film as a double feature with High School Big Shot as the first release of his Filmgroup company. 

Corman financed the movie. Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 137 

== Cast ==
  
*John Brinkley as Frank Simmons, alias Frank Minor Edwin Nelson as Alex Hendricks
*Tony Miller as Raymond Gunderson
 
*with Pat George as Marla Stanosky, alias Marla Stanley
*Coleman Francis as Capt. R. M. Prell
*Nola Thorp as Kay
*Beech Dickerson as Barney Adams
*Trent Dollar
*Gene Walker
 
*and Steve Harris
*Robert Wendell
*Henry Randolph
*Vic Tabback
*Glenn Campbell
*Earl Miles
 

==Notes==
 

== External links ==
* 
* 

 
 
 
 
 
 
 

 