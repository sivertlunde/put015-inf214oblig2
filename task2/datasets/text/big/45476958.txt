Maha Maha
{{Infobox film
| name            = Maha Maha
| image           = Maha Maha tamil movie release poster.jpg 
| caption         = Release poster
| director        =  
| producer        = Sakthi Screens
| writer          =  
| screenplay      =  
| starring        =    Melissa Indira Nizhalgal Ravi Anupama Kumar Meera Krishnan
| music           = Pavalar Shiva
| editing         = Suresh Urs
| released        = March 6, 2015
| runtime         = 109 minutes
| country         = India
| language        = Tamil 
}}

Maha Maha (Tamil: மகா மகா, English: Great Great) is a 2015 Indian Tamil romantic thriller film directed by Mathivanan Sakthivel. The film stars Mathivanan Sakthivel, Melissa, Indira, Nizhalgal Ravi, Anupama Kumar and Meera Krishnan. The film features music composed by Pavalar Shiva, and editing by Suresh Urs. 

The film was predominately filmed in Australia with local Australian cast.  The film released on 6 March 2015 in Tamil Nadu, Australia and England.   

== Plot ==
Hero Vijay played by Mathivanan relocates to Australia as a result of his job transfer. There he meets an Australian girl, Emily played by Melissa and falls in love. Meanwhile, Australia Police are in search of Emily who is claimed to be missing for the past 10 days. Meantime, Police find Emily’s dead body that was killed and buried in Vijay’s garden and hence he is arrested. Emily’s postmortem report says that she was murdered and was buried. Further it shocks the police as they find that Vijay had actually arrived at Australia, just a day before Emily’s murder. As a result, Vijay is released by the police. Following which, Vijay is all set himself to find about Emily, that who was she actually, who would have murdered her and what would have been the reason behind it, how is it possible for a deal girl to meet and speak wit him.. Finally, all these investigations result to unfold many interesting and shocking informations. 

== Release ==
The film released on 6 March 2015 in  .

== Production ==
The film is produced by Sakthi Screens and it was written and directed by Mathivanan Sakthivel.  Mathivanan did a short course in film making at the Australian Film Base in Sydney before production of this film.  The film was predominately filmed in Australia in a small country townTaralga. 

== Music ==
The film has three tracks composed by Pavalr Shiva, who is Isaighani Ilayarajas brother Pavalar Varadarajan’son. 

The audio was released by Isaighani Ilayaraja.   Isaighani Ilayaraja commented that the songs are good and he liked the title of the movie "Maha Maha". 

Sulekha who reviewed the music said that the title track ‘Maha Maha’ is the top pick.  Cinesnacks said that the words in the songs, sound perfectly ‘clear cut’ in his tones. 

The Hindu reported that there are only three tracks in the album, all of which are pleasing to the ears. ‘Ennamo Pannuthe’ is sung by Prasanna and Prashanthini, while ‘Agaramodu Lagaram’ is sung by Prashanthini, Priya and Vallavan. Pavalar Siva’s rendition of the title number ‘Maha Maha’ is good. 

== Critical Reception ==
Sulekha in their review stated that Mathivanan Sakthivel impresses the audience, as a director and also as a male lead. He has succeeded in creating the right emotions for both the romance scenes and the ones that involve mystery. His performance is simple and elegant. Melissa looks pretty and has made a decent debut through ‘Maha Maha’. 

Sulekha review of the movie also said that The film is overall a good attempt by Mathivanan Sakthivel. He has given the Kollywood audience something refreshing, as Maha Maha happens to be the first Tamil film to be independently shot in and around Sydney. NRIs in Australia can learn a lot about Tamil culture through this romantic thriller flick. Unique concept and clever twists add verve to the simple and loveable story. Although the film culminates in a rather sad end, it keeps the audience riveted from start to finish and offers what it promised in the trailer – a thriller!. 

  review of the movie said that Director Mathivanan Sakthivel has cleverly woven a horror movie as a Romantic story in an interesting and impressing style. Mathivanan has appealingly drafted a fascinating screenplay within a short duration of time and thus have successfully delivered his thoughts in a very clear and elegant manner. He has not clearly portrayed the approach with which the Actress was murdered and the reason behind it. Rather he had finished the movie with a note of ‘to be continued’ and it ultimately makes the audience to give him a huge applause for his unique presentation. 

iflicks review reported that Hero Mathivanan has acted casually without much hype. Melisa has done the given job well. Anusri looks beautiful and tries to impress audience with her acting. Nizhalgal Ravi has performed well in his own style. 

  review said that the movie does not have much commercial element, however the director has done a fast pace screenplay for which he needs to be congratulated. 

J Sukumar from   said that the Director Mathivanan has done this thriller subject in a romantic and interseting way and that he has told the story elegantly. The movie even though was short, is filled with good screenplay. 

  review said that the director has explained the connection between the australian aboriginal and indian customs very well and should receive appaluse for this. 
 Gtamil Cinema said that while we would expect that a movie made in Australia will be highly technical and lavish, Maha Maha stands out from this thinking as the movie revolves around a country side in Australia which is very different. The story and diaglogues by Mathivanan is ok, however if he enhances his natural acting skills further, he is sure to be a good actor in the industry. The lyrics by Thenmozhli Das for the song "agaramodu lagaram" is a praise to the tamil language.  

==References==
 

==External links==
 
* https://www.facebook.com/mahamahaa

 