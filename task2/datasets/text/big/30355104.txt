Kill Keith
{{Infobox film
| name           = Kill Keith
| image_size     = 
| image	=	Kill Keith FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Andy Thompson
| producer       = Paul Atherton Christian James Tim Major Andy Thompson
| writer         = Pete Benson Tim Major Andy Thompson
| starring       = Susannah Fielding Simon Phillips Keith Chegwin Joe Tracini
| music          = John Zealey
| cinematography = Luke Bryant
| editing        = Richard Colton
| studio         = Dead on Arrival Digital Gaia Media
| distributor    = 
| released       =   
| runtime        = 93 minutes   
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Kill Keith is a 2011 British comedy horror film starring Susannah Fielding, Simon Phillips, and Keith Chegwin.    {{Cite web|url=http://www.thesun.co.uk/sol/homepage/showbiz/tv/3136881/Vanessa-Feltz-explodes-scoffing-porridge.html The Sun|accessdate=8 January 2011}} 

==Cast==
* Susannah Fielding as Dawn
* Marc Pickering as Danny
* David Easter as Cliff Simon Phillips as Andy
* Keith Chegwin as himself
* Joe Tracini as Tony Blackburn
* Russell Grant as himself
* Tony Blackburn as himself
* Christopher Trott as Man Wearing Jacket, among other roles.

==References==
 
*http://danielhg.blogspot.com/2010/08/kill-keith-filming-awaits.html
*http://www.chortle.co.uk/news/2010/07/23/11419/kill_keith_chegwin!?rss
*http://titanmagazinesus.com/news/5493-vanessa-feltz-and-keith-chegwin-to-meet-grizzly-end-in-kill-keith
*http://www.uk.castingcallpro.com/film_view.php?uid=33479

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 