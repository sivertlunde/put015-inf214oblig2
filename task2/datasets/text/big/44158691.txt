Lessons in Dissent
 
 
{{Infobox film
| name           = Lessons in Dissent
| image          = 
| caption        = 
| director       = Matthew Torne
| producer       = Matthew Torne, Grace Wong, Li Ping Kong
| writer         =  Matthew Torne
| starring       = Joshua Wong, Ma Wan Ke 
| music          = Phila Yuen
| cinematography = Eddie Hung Hon-chuen
| editing        = Lam Kin-kuan
| distributor    = Torne Films Ltd, Edko Films (HK), Journeyman Pictures (World).
| released       =  
| runtime        = 97 minutes
| language       = Cantonese
| budget         = HK$3 million Lana Lam,  , South China Morning Post, 10 October 2014 
| gross          = 
}}
Lessons in Dissent is a 2014 documentary film about young political activists in Hong Kong.

==Synopsis== Joshua Wong and Ma Jai, as they protest the proposed adoption of the Moral and National Education curriculum for Hong Kongs schools.  They felt the curriculum amounted to brainwashing.  Wong, a secondary school student at the time, is portrayed as a charismatic young leader.  He founded a student group called Scholarism to fight the changes and became famous in his opposition.  Ma Jai, a school drop-out, remained active in the League of Social Democrats as an organiser and protestor.  Both were arrested for their efforts but the protests succeeded and that National Education curriculum was made optional.  , The Economist, 4 July 2014 

==History==
The film was released in the April 2014 and played in theatres in Hong Kong in that summer.  It gained renewed notoriety during the Umbrella Revolution of the fall of 2014 with Joshua Wongs prominent role.  It was screened at the Mumbai International Film Festival. Clarence Tsui,  , The Hollywood Reporter, 14 October 2014 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 