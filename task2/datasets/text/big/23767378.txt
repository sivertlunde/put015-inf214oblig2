Shapoklyak (film)
{{Infobox film
| name           = Shapoklyak Шапокляк
| image          = 
| image_size     = 
| caption        =  Roman Kachanov
| producer       =  Roman Kachanov
| narrator       = 
| starring       = See below
| music          = Vladimir Shainsky
| cinematography = Teodor Bunimovich Aleksandr Zhukovskiy
| editing        = Nadezhda Treshchyova
| distributor    = 
| released       =  
| runtime        = 19 min 48 sec
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}} Roman Kachanov.

==Synopsis==
Crocodile Gena and Cheburashka decide to go to the sea on vacation. Shapoklyak steals their train tickets, and so they are kicked off the train. They stop hikers from poaching, and a factory from polluting the environment.

==Creators== Roman Kachanov Roman Kachanov
*Art director: Leonid Shvartsman
*Operators: Alexander Zhukovsky, Theodor Bunimovich
*Composer: Vladimir Shainsky
*Sound technician: Georgy Martynyuk
*Animators: Maya Buzinova, Natalya Dabizha, Yuriy Norshteyn, Pavel Petrov, Boris Savin
*Editor: Natalya Abramova
*Director: Nathan Bitman

==Cast==
*Vasily Livanov as Gena the Crocodile
*Irina Mazing as Shapoklyak
*Klara Rumyanova as Cheburashka
*Vladimir Ferapontov as Gena (singing voice)

==Soundtrack==
This film contains a second famous Russian song called "Blue (Train)car" (RUS: Голубой Вагон) of the composer Vladimir Shainsky on Eduard Uspenskys verses sound. It is sung at the end of the film by Gena.

==Interesting facts==
*The Cheburashka from Gena on the platform of the station get on the train No. 8 the message "Moscow — Yalta". Thus that railway communication with Yalta is absent (trains go to Simferopol).
*On the line Moscow — Simferopol is absent any railway tunnel shown in the animated film.
*The crocodile Gena and the Cheburashka was landed at the first stop already for 200 km from Moscow (Tula-1 Kursk). It, in principle, is possible: for example, the train "Moscow — Voronezh" has the first stop at Uzunovo station, and these are about 170 km from MKAD. wires of a contact network.
*Having settled down in a nest at top of a tree, Shapoklyak, examining a  . However in the wood berries, nuts and mushrooms, and consequently, time of action of the animated film — the end of August-September already ripened. Before, in conversation with the Cheburashka between the 200th and 199th kilometers, Gena notes: "In the fall on cross ties there are neither pools, nor dirt".
*Children give a young frog with words "But he green!" Before, in the wood at the polluted factory of the small river, we see a young frog in blue-red spots. Genas crocodile, having stopped up a drain pipe, I cleared the small river, and together with it — and the young frog living there.

==External links==
*  at Animator.ru
* 
* 

 
 

 
 
 
 
 
 
 
 
 


 