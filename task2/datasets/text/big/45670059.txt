The Farmer's Daughter (1940 film)
{{Infobox film
| name           = The Farmers Daughter
| image          = 
| alt            = 
| caption        = Theatrical release poster James P. Hogan
| producer       = William C. Thomas
| screenplay     = Lewis R. Foster
| story          = Delmer Daves
| starring       = Martha Raye Charlie Ruggles Richard Denning Gertrude Michael William Frawley Inez Courtney William Demarest
| music          = 
| cinematography = Leo Tover
| editing        = Archie Marshek
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 James P. Hogan and written by Lewis R. Foster. The film stars Martha Raye, Charlie Ruggles, Richard Denning, Gertrude Michael, William Frawley, Inez Courtney and William Demarest. The film was released on March 26, 1940, by Paramount Pictures.  

==Plot==
 

==Cast==
*Martha Raye as Patience Bingham
*Charlie Ruggles as Nickie North
*Richard Denning as Dennis Crane
*Gertrude Michael as Clarice Sheldon
*William Frawley as Scoop Trimble
*Inez Courtney as Emily French
*William Demarest as Victor Walsh
*Jack Norton as Shimmy Conway William Duncan as Tom Bingham
*Ann Shoemaker as Mrs. Bingham
*Benny Baker as Monk Gordon
*Tom Dugan as Forbes
*Lorraine Krueger as Valerie
*Sheila Ryan as Dorinda 
*Anne Harrison as Rosalie 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 