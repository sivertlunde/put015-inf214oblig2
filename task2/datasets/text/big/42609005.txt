No Parking
{{Infobox film
| name           = No Parking
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Jack Raymond
| producer       = Herbert Wilcox
| writer         = Gerald Elliott
| screenplay     = 
| story          = Carol Reed
| based on       =  
| narrator       = 
| starring       = {{Plain list| Charles Carson
* Geraldo Fred Groves
* Gordon Harker
* George Hayes
* Leslie Perrins
}}
| music          = Geraldo
| cinematography = Francis Carver
| editing        = 
| studio         = Herbert Wilcox Productions
| distributor    = British Lion Film Corporation (UK) (theatrical)
| released       =     
| runtime        = 72 min 
| country        = Britain English
| budget         = 
| gross          = 
}}
 English comedy Charles Carson, Fred Groves, Gordon Harker and Leslie Perrins in the lead roles.

The story was written by Carol Reed who later directed The Third Man. 

The film is considered lost, as no prints are known to exist. 

==Plot==
 

==Cast==
 Charles Carson as Hardcastle
* Geraldo as Orchestra Leader Fred Groves as Walsh
* Gordon Harker as Albert
* George Hayes as James Selby
* Leslie Perrins as Captain Sneyd
* Cyril Smith as Stanley Frank Stanmore as Gus
* Irene Ware as Olga

==References==

 

==External links==

 

 
 
 
 
 


 