Murai Maman
{{Infobox film
| name           = Murai Maman
| image          = 
| image_size     = 
| caption        = 
| director       = Sundar C.
| producer       = N. Prabavathy J. Jyothi Lakshmi N. Vishnuram N. Raghuram
| writer         = Sundar C. K. Selva Bharathy (dialogues)
| starring       =   Vidyasagar
| cinematography = U. K. Senthil Kumar
| editing        = B. S. Vasu
| distributor    = Ganga Gowri Productions
| studio         = Ganga Gowri Productions
| released       = 19 May 1995
| runtime        = 140 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1995 Tamil Vidyasagar and was released on 19 May 1995.  

==Plot==

Sirasu (Jayaram) and his elder brother (Goundamani) own a cinema theatre and they are bachelors. For several years, his family are in feud with Parameswarans family because Sirasus father killed Parameswarans brother and Saratha got married with Parameswarans relative secretly.

Saratha (Sangeetha (Telugu actress)|Sangeetha), Sirasus sister, comes from Mumbai with her husband (Jaiganesh) and her daughter, Indhu (Kushboo). Sirasu falls immediately in love with Indhu. Ratnam (Kazan Khan) and his father Parameswaran (Vinu Chakravarthy) proposes to Sarathas husband to marry Ratnam to his daughter. Indhus father forces her to marry but she refuses. Indhu decides to leave the village, Sirasu stops her, she says that she doesnt want to marry Ratnam and she isnt in love with Sirasu. The villagers misunderstand their relation. Indhu tells to Ratnam that they were just talking but Sirasu lies to marry Indhu.

Sirasu, as he wanted, gets married with Indhu. However, Indhu refuses to live with him as his wife. Then, she becomes very ill and Sirasu takes care of her. Sirasus mother explain that he must place a god statue in the village temple to save Indhu. Sirasu accomplishes it with his brother help and Indhu understands Sirasus feelings.
 Thaali and kidnaps her. Sirasu saves his wife, he apologizes Parameswaran and Ratnam. The two families united together and they lived happy.

==Cast==

*Jayaram as Sirasu
*Kushboo as Indhu
*Goundamani as Sirasus elder brother Manorama as Sirasus mother Sangeetha as Saratha
*Jaiganesh as Sarathas husband Senthil as Chinna Thambi
*Kazan Khan as Ratnam
*Vinu Chakravarthy as Parameswaran

==Production==
Sundar C. said "For Murai Maaman, we needed an action hero. But Jayaram was the only hero available for me. I tapped into his innate sense of humour, as he was very famous for his comedy in Malayalam and the output was hilarious. Till then I never knew I could direct a comedy film. It was because of Jayaram that we infused humour in the script and gave it a shot".  It was during this film that Sundar met his future wife Kushboo Sundar|Kushboo.

==Soundtrack==

{{Infobox album |  
  Name        = Murai Maman |
  Type        = soundtrack | Vidyasagar |
  Cover       = |
  Released    = 1995 |
  Recorded    = 1995 | Feature film soundtrack |
  Length      = 27:27 |
  Label       = Lahari Music | Vidyasagar |  
  Reviews     = |
}}

The film score and the soundtrack were composed by film composer Vidyasagar (music director)|Vidyasagar. The soundtrack, released in 1995, features 6 tracks with lyrics written by Vairamuthu and Pazhani Bharathi.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Lyrcis !! Duration 
|-  1 || Pazhani Bharathi || 4:44
|- 2 || Manorama || 5:24
|-  3 || Vairamuthu || 4:12
|- 4 || Poove Poove || Chatten || 4:04
|-  5 || Pazhani Bharathi || 4:39
|- 6 || Yennachi Yennachi || Arunmozhi, Sirkazhi G. Sivachidambaram, Swarnalatha || 4:24
|}

==References==
 

 

 
 
 
 
 