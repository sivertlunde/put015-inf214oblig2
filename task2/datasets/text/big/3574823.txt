Jakob the Liar
 
{{Infobox film
| name           = Jakob the Liar
| image          = Jakob the liar poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Peter Kassovitz
| producer       = Steven Haft Marsha Garces Williams
| writer         = Jurek Becker (novel) Peter Kassovitz &  Didier Decoin (screenplay)
| starring       = Robin Williams Alan Arkin Liev Schreiber Hannah Taylor-Gordon Bob Balaban
| music          = Edward Shearmur
| cinematography = Elemér Ragályi
| editing        = Claire Simpson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 120 min.
| country        = United States
| language       = English
| budget         = $45 million
| gross          = $4,956,401 (domestic) 
}}
 ghetto in the book German Deutsche DEFA film Jakob der Lügner from 1975.

==Plot==
In Poland of early 1944, a History of the Jews in Poland|Polish-Jewish shopkeeper named Jakob is summoned to the German headquarters after being falsely accused of being out after curfew. While waiting for the commander, Jakob overhears a German radio broadcast speaking about Soviet offensives. Returned to the ghetto, Jakob shares his information with a friend, sparking rumors that there is a secret radio within the ghetto. After hesitating, Jakob decides to use the chance to spread hope throughout the ghetto by continuing to tell the optimistic, fantastic tales that he allegedly heard from "his secret radio" and his lies keep hope and humor alive among the isolated ghetto inhabitants. He also has a real secret in that he is hiding a young Jewish girl who escaped from an extermination camp deportation train. 

The Gestapo learn of the mythical radio, however, and begin a search for the resistance hero who dares operate it. Jakob surrenders himself to the Germans as they demand the person with the radio give himself up or risk hostages being killed. During interrogation, Jakob tells the police commander that he had only listened to the radio inside his office. He is ordered to announce publicly that this was all a lie, so the ghettos liquidation would then proceed in an orderly fashion. When presented to the public, Jakob refuses to tell the truth, but is shot before he can make his own speech. 

In the films ending, Jakob says, post-mortem, that all the ghettos residents were then deported and were never seen again. As in the novel, there is an alternate fairy tale-style ending where the Soviet forces arrive following Jakobs death, just in time to save the Jews.

==Cast==
*Robin Williams as Jakob
*Alan Arkin as Frankfurter
*Liev Schreiber as Mischa
*Hannah Taylor-Gordon as Lina
*Bob Balaban as Kowalsky
*Mark Margolis as Fajngold
*Michael Jeter as Avron

==Reception==
Produced on a budget of $45 million, Jakob the Liar was released on September 24, 1999. According to Box Office Mojo, it opened in 1,200 theaters and made $2,056,647 in its opening weekend, placing eighth at the box office. The films total domestic gross was just $4,956,401. The response was mixed to negative, with many  feeling it was not half as good as the comedy Life Is Beautiful.

The movie currently holds a rating of "rotten" on Rotten Tomatoes with only 29% positive reviews and an average rating of 4.7/10. Roger Ebert gave the film two stars (out of four), comparing it to the similarly themed Life Is Beautiful by saying, "I prefer Life Is Beautiful, which is clearly a fantasy, to Jakob the Liar, which is just as contrived and manipulative but pretends it is not." He went on to say about the acting in the film: "Williams is a talented performer who moves me in the right roles but has a weakness for the wrong ones. The screenplay and direction are lugubrious, as the characters march in their overwritten and often overacted roles toward a foregone conclusion." 
 Worst Actor Bicentennial Man, Big Daddy.

==See also==
*Jacob the Liar|Jacob the Liar (novel)
*List of Holocaust films

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 