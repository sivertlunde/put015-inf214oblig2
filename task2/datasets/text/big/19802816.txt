Death Comes at High Noon
{{Infobox film
| name           = Death Comes at High Noon
| image          = 
| caption        = 
| director       = Erik Balling
| producer       = Bo Christensen
| writer         = Erik Balling Peter Zander
| starring       = Poul Reichhardt
| music          = 
| cinematography = Arne Abrahamsen Jørgen Skov
| editing        = 
| distributor    = Nordisk Film
| released       = 31 July 1964
| runtime        = 98 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Death Comes at High Noon ( ) is a 1964 Danish crime film directed by Erik Balling and starring Poul Reichhardt.

==Cast==
* Poul Reichhardt - Peter Sander
* Helle Virkner - Eva Lindberg
* Birgitte Federspiel - Merete Lindberg
* Jan Priiskorn-Schmidt - John Lindberg
* Morten Grunwald - Bertel Lindberg
* Kirsten Søberg - Frk. Jørgensen
* Karl Stegger - Overbetjent Duus Jensen
* Pouel Kern - Overbetjent Hald
* Gunnar Lauring - Doktor Lund
* Gunnar Strømvad - Autoforhandler H. F. Kærgaard
* Ebba Amfeldt - Fru Kærgaard
* Kai Holm - Tømmerhandleren
* Einar Nørby - Lastbilchauffør
* Johannes Meyer - Dr. Joachim Lindberg

==External links==
* 

 
 
 
 
 
 
 


 
 