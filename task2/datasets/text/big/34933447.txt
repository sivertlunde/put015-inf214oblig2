A Jamaâ
 

{{Infobox film
| name           = A Jamaâ (The Mosque)
| image          =
| caption        =
| director       = Daoud Aoulad-Syad
| producer       = Les Films du Sud, Chinguitty Films
| writer         =
| starring       = Abdelhadi Touhrach, Bouchra Hraich, Mustapha Tahtah, Naceur Oujri, Salem Dabella
| screenplay     = Daoud Aoulad-Syad
| cinematography = Thierry Lebigre
| editing        = Nathalie Perrey
| music          = Aquallal/Zagora
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = France Morocco
| language       =
| budget         =
| gross          =
}}
A Jamaâ  ( ) is a 2010 film directed by Daoud Aoulad-Syad. It was selected by African Film Festival of Cordoba - FCAT, San Sebastián International Film Festival  and other international film festivals.

== Plot ==
To make Daoud Aoulad-Syad’s previous film, En attendant Pasolini, sets were built on plots rented from those living in the village. A mosque was erected on the plot belonging to Moha, one of the neighbors. When they finished shooting, the film crew left the village. The neighbours demolished all of the sets, except for the mosque, which had become a real place of worship for those who live there. However, this is disastrous for Moha, who used to grow vegetables to feed his family on this land.

== Prizes ==
* Cinemed 2010
* San Sebastián 2010
* Tetuan 2011

== References ==
 

==External links==
 
* 
* 
* 

 
 
 
 

 