Sarapancharam
{{Infobox film
| name = Sarapancharam
| image = Sharapanjaram.jpg
| caption = DVD Cover Hariharan
| screenplay = Hariharan
| story = Malayattoor Ramakrishnan
| starring = Jayan Sheela Sathaar Nellikodu Bhaskaran
| producer = G. P. Balan
| music = Devarajan
| cinematography = Melli Irani
| editing = V. P. Krishnan
| studio =
| distributor = Angel Films
| released =  
| runtime =
| country = India
| language = Malayalam
| budget =
}} Hariharan starring Jayan, Sheela, Sathaar, P.K. Abraham and Oduvil Unnikrishnan in major roles. It was released in the golden era of Jayan, a popular superstar of Malayalam cinema.  He played the main villain role in the film. It was also one of the first notable films of Oduvil Unnikrishnan.  The storyline of the film is loosely based on D. H. Lawrences 1928 novel Lady Chatterleys Lover though there are significant differences in plot and characterization. 

==Plot==
The story concerns a young married woman, Soudamini (Sheela), whose upper-class husband (P.K. Abraham) has been paralyzed and rendered impotent. Her sexual frustration leads her into an affair with the servant, Chandrasekharan (Jayan). She eventually gets married to him but later discovers that he has had relationship with many ladies and he aimed only at her wealth. She and her only daughter Baby (born from her first husband) are helpless as they are not able to put him out of their lives. Later, a young man named Prabhakaran, who is the son of an ex-servant of Soudamini, enters their life and helps them to get rid of Chandrasekharan. In the climax, Chandrasekharan is shot dead by Soudamini.

== Box office ==
This was a blockbuster hit film which strengthen the position of lead actor Jayan both as a villain and as a hero. This film also broke many box office records and was declared as the highest grossing movie ever released until another Jayan film Angadi  released.

==Cast==
* Jayan as  Chandrasekharan
* Sheela as Soudamini
* Sathaar as Prabhakaran
* Nellikkodu Bhaskaran as Sidhayyan
* Oduvil Unnikrishnan as Subbaiyer Shankar as Babys Friend
* Baby Sumathi as Young Baby
* P. K. Abraham as Soudaminis first husband

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Ambalakkulathile || K. J. Yesudas || Yusufali Kechery 
|-
| 2 || Malarinte manamulla || P. Madhuri || Yusufali Kechery 
|-
| 3 || Saaraswatha Madhuventhum || Vani Jairam || Yusufali Kechery 
|-
| 4 || Sringaaram virunnorukki || P Susheela || Yusufali Kechery 
|-
| 5 || Theyyaka theyyaka || P Jayachandran, P. Madhuri || Yusufali Kechery 
|}

==External links==
*  

==References==
 

 

 
 
 
 
 
 


 