Khoya Khoya Chand
{{Infobox Film |
 name     = Khoya Khoya Chand (film)|
  image          =Khoya Khoya Chand.jpg |
  caption        = Movie poster for Khoya Khoya Chand.|
    writer         = Sudhir Mishra |
  starring       = Shiney Ahuja,  Soha Ali Khan,  Rajat Kapoor,  Sushmita Mukherjee |
  director       = Sudhir Mishra |
  producer       = Prakash Jha|
  distributor    = Holy Cow Pictures|
  cinematography = Sachin Kumar Krishnan|
  released   =7 December 2007 |
  runtime        = |
  language = Hindi |
  music          = Shantanu Moitra |
  lyrics         = Swanand Kirkire  |
  budget         = Rs. 7 crores |
  gross          = Rs. 5.83 crores |
}}

Khoya Khoya Chand, (  film directed by Sudhir Mishra which released on 7 December 2007.  The film stars Shiney Ahuja and Soha Ali Khan in the lead roles with Rajat Kapoor, Sushmita Mukherjee, Sonya Jehan and Vinay Pathak in important roles. The film captures the lifestyle of celebrities with aplomb with the 1950s film industry as its backdrop.

== Cast ==
* Shiney Ahuja as ... Zaffar
* Soha Ali Khan as ... Nikhat
* Rajat Kapoor as ... Prem Kumar
* Sonya Jehan as ... Ratanbala
* Sushmita Mukherjee as ... Sharda
* Vinay Pathak as ... Shaymol

==Music==
The soundtrack of the film was released on 8 November 2007.Composed by Shantanu Moitra, lyrics by Swanand Kirkire. 

{{Infobox album |  
  Name        = Khoya Khoya Chand |
  Type        = Album |
  Artist      = Shantanu Moitra |
  Cover       = Khoya khoya chand 5.jpg|
  Released    =  8 November 2007 (India)|
  Recorded    = | Feature film soundtrack | 1255219218
  Label       =   Adlabs Presents |
  Producer    =  |
  Reviews     = |
  Last album  = Laaga Chunari Mein Daag  (2007) |
  This album  =  Khoya Khoya Chand  (2007) |
  Next album  = TBA|
}}
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Picturised on
|-
|Chale Aao Saiyan
| Shreya Ghoshal
|
|-
|Khoya Khoya Chand
| Ajay Jhingran & Swanand Kirkire
| Shiney Ahuja and Soha Ali Khan
|-
| Khushboo Sa
| Hamsika Iyer
|
|-
|O Re Paakhi
| Sonu Nigam
|
|-
|Sakhi Piya
| Pranab Biswas & Shreya Ghosal
|
|-
|Thirak Thirak
| Sonu Nigam & Shreya Ghosal
|
|-
| Yeh Nigahein
| Sonu Nigam & Antara Chowdhury
| Rajat Kapoor, Soha Ali Khan, Sonya Jehan and Sushmita Mukherjee
|}

== External links ==
*  

== References ==
 

 
 
 
 
 

 