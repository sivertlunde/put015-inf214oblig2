Up in Arms
{{Infobox film
| name           = Up in Arms
| image          = Up in Arms 1944 poster.jpg
| image_size     = 220px
| caption        = 1944 US Theatrical Poster
| director       = Elliott Nugent
| producer       = Samuel Goldwyn
| writer         =
| narrator       =
| starring       = Danny Kaye Dinah Shore
| music          =
| cinematography =
| editing        = Daniel Mandell James Newcom
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 106 mins.
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}}
Up in Arms (1944 in film|1944) is a film directed by Elliott Nugent, and starring Danny Kaye and Dinah Shore.  It was nominated for two Academy Awards in 1945. 

==Plot Synopsis==
Danny Weems works as an elevator operator in a New York Medical building, so that he can be close to doctors and nurses and get free advice on his supposed illnesses. The doctors know him well and consider him a hypochondriac. So when he is drafted into the US Army for war service, he is devastated. His best friend Joe gets himself also drafted so that he can keep an eye on Danny.

Danny is in love with nurse Mary Morgan, but shes really in love with Joe, and Joes girl Virginia is secretly in love with Danny. The boys get through basic training, and as they embark by ship to the South Pacific, they discover that Mary and Virginia have also enlisted as army nurses. But as officers, they cannot fraternise with the boys.

Danny contrives to smuggle Mary on board, and during the voyage he tries to keep her hidden. But the truth eventually comes out and Danny is hauled before Colonel Ashley - who has him sent to the brig.

When the troops are landed on a Pacific island, Danny is again imprisoned, but is rescued by a Japanese patrol. The try to interrogate him, but Danny mananges to bamboozle them and eventually impersonate the commander. He gives orders that the soldiers surrender to the Americans - and they obey orders to the letter. And Dannys a hero.

==Cast==
*Danny Kaye as Danny Weems
*Dinah Shore as Nurse Lt. Virginia Merrill 
*Dana Andrews as Joe Nelson 
*Constance Dowling as Nurse Lt. Mary Morgan
*Louis Calhern as Col. Phil Ashley
*Margaret Dumont as Mrs. Willoughby

==Production notes==
* Production Dates: late Jun-late Sep 1943
* The working title of this film was With Flying Colors. 
* All of the actresses who played nurses in Up in Arms are listed collectively onscreen as "The Goldwyn Girls." 
* Danny Kayes character was based on the character "The Nervous Wreck" from the play of the same name by Owen Davis, which opened in New York in 1923. The play, which bears little resemblance to the film, was in turn based on the 1921 magazine serial The Wreck by Edith J. Rath and Sam H. Harris, which was published as a novel called The Nervous Wreck in 1923. In 1928, Florenz Ziegfeld staged a musical version of Davis play called Whoopee starring Eddie Cantor.
*  Up in Arms marked the motion picture feature debut of Broadway star Danny Kaye (1913-1987), and opened to uniformly rave reviews. The popular star, who began on Broadway in 1939, had already turned down a contract with Metro-Goldwyn-Mayer, when Goldwyn cast him in Up in Arms. After this film, made Kaye an international success, he went on to do four more in succession with Goldwyn before moving on to Warner Brothers, 20th Century-Fox and Paramount Pictures.
* The film received Academy Award nominations in the Music (Scoring of a Musical Picture) and Music (Song-"Now I Know") categories

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 