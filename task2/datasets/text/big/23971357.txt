Vennira Aadai
 
{{Infobox film
| name           = Vennira Aadai வெண்ணிற ஆடை
| image          =
| image_size     =
| caption        =
| director       = C. V. Sridhar
| producer       = C. V. Sridhar
| writer         = C. V. Sridhar Chitralaya Gopu
| story          = C. V. Sridhar Nirmala  Moorthy
| music          = Viswanathan–Ramamoorthy
| cinematography = G. Balakrishna
| editing        = N. M. Shankar
| studio         = Chithralaya Pictures
| distributor    = Chithralaya Pictures
| released       =  
| runtime        = 178 mins
| country        = India Tamil
}}
 1965 Tamil Nirmala and Major Sundarrajan in lead roles. 
The film, produced by Chithralaya Pictures and had musical score by Viswanathan–Ramamoorthy. 

==Plot==
Sreekanth is a famous psychiatrist and has earned much praise from his mentor and people in the town. He is in love with Venniradai Nirmala. One day he is assigned a task to cure a mentally unstable women (Jayalalitha) who lives in Ooty. Srikanth informs about this to her lover and request her to wait for him until he finishes this case. Srikanth promises he will return for her and sets out to Ooty. On arriving, he is been welcomed by Jayalalaithas parents who are very much distressed with their daughters condition. Srikanth questions them about her past but they conceal initially as it might have a severe consequence on her future. Upon reiteration that knowing her past will be very useful for him to cure her, Jayalalitha parents reveal her awful past. Jayalalitha at a very young age got married and within hours, the couple meet with an accident and her spouse dies. Due to the aftermath of the accident and upon learning her husbands death, Jayalalitha loses her mental stability. As days pass by, Srikanth cures her for good. Her parents are happy on learning that Jayalalitha has completely forgotten her past to begin a new life. Jayalalitha begins to fall in love with Srikanth but is reluctant to profess her love to him, instead she tells her parents. Srikanth upon learning this fears that she might go back to her old condition if she learns that a girl is awaiting him. Srikanth having no courage, informs about this to Jayalalithas parents. Srikanth marries Venniradai Nirmala and Jayalalitha on the other hand tells she indeed remembers her past and wears a white saree (tradition where in women wears a white saree after her spouse death) and convinces herself that this her destiny.

==Cast==
*Jayalalithaa
*Sreekanth Nirmala
*Venniradai Moorthy
*Major Sundarrajan
* Asha

==Crew==
*Director: C. V. Sridhar
*Producer: C. V. Sridhar
*Production Company: Chithralaya Pictures
*Music: Viswanathan–Ramamoorthy
*Lyrics: Kannadasan
*Story: C. V. Sridhar
*Screenplay: C. V. Sridhar
*Dialogues: C. V. Sridhar & Chitralaya Gopu
*Cinematography: G. Balakrishna
*Editing: N. M. Shankar
*Art Direction: Ganga
*Choreography: Thangappan
*Audiography: T. S. Rangasamy

==Production==
The film launched four faces to the Tamil Cinema namely Srikanth, Jayalalitha, Nirmala and Moorthy. Nirmala & Moorthy had been called Vennira aadai nirmala and Vennira aadai Moorthy after that. Vennira aadai Moorthy who is still in the limelight of Tamil Films on comic and parental roles, while vennira aadai Nirmala makes occasional appearances.

==Soundtrack==

The music composed by Viswanathan–Ramamoorthy.  
"Neeraadum Kangal" was not included in the film.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:13
|-
| 2 || Ammamma || P. Susheela || 03:23
|-
| 3 || Chithirame || P. B. Sreenivas, P. Susheela || 03:34
|-
| 4 || Enna Enna Vaarthaigalo || P. Susheela || 03:44
|-
| 5 || Kannan Ennum || P. Susheela || 03:25
|-
| 6 || Nee Enbathenna || L. R. Eswari || 05:03
|-
| 7 || Neeraadum Kangal || P. Susheela || 03:14
|-
| 8 || Neethi Ithuthana || P. B. Sreenivas || 02:55
|}

==Reception==

The film was a huge commercial success in 1965 and well remembered for the first Tamil film of J. Jayalalithaa|Jayalalitha, who went on to become a huge star and later the Chief Minister of Tamil Nadu.

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 