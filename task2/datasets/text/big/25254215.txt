Bienvenue en Suisse
{{Infobox Film
| name           = Bienvenue en Suisse
| image          = 
| image_size     = 
| caption        = 
| director       = Léa Fazer
| producer       = Hélène Delale Bruno Pésery Ruth Waldburger
| writer         = Léa Fazer
| starring       = Vincent Perez
| music          = 
| cinematography = Myriam Vinocour
| editing        = Hervé de Luze
| distributor    = 
| released       = 13 May 2004
| runtime        = 105 minutes
| country        = France Switzerland
| language       = French
| budget         = 
}}

Bienvenue en Suisse is a 2004 French-Swiss comedy film directed by Léa Fazer. It was screened in the Un Certain Regard section at the 2004 Cannes Film Festival.   

==Cast==
* Vincent Perez - Aloïs Couchepin
* Emmanuelle Devos - Sophie
* Denis Podalydès - Thierry
* Walo Lüönd - Adolf Sempach
* Peter Wyssbrod - Kurt Sempach
* Marianne Basler - Béatrice
* Scali Delpeyrat - Vincent
* Mariama Sylla - Amélia
* Julien George - Damien
* Carola Regnier - Heidi
* Heidi Züger - Silvia
* Jacques Michel - Rémi
* Suzanne Thommen - Lisa

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 