Mondo Cannibale
 

{{Infobox film
| name           = White Cannibal Queen
| image          = Mondo Cannibale.jpg
| image_size     = 
| caption        = 
| director       = Jesús Franco
| producer       = Marius Lesoeur   Franco Prosperi
| writer         = Jesús Franco
| narrator       = 
| starring       = Al Cliver   Sabrina Siani   Shirley Knight
| music          = Roberto Pregadio
| cinematography = Luis Colombo   Juan Soler
| editing        = 
| distributor    = Blue Underground 1980
| runtime        = 90 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 exploitation director Jesús Franco which starred a 17-year-old Sabrina Siani . It is one of two cannibal films directed by Franco starring Al Cliver, the other being Man Hunter (aka Devil Hunter, aka Sexo Canibal).

Mondo Cannibale is considered even by Franco himself to be the worst cannibal film ever made, due to its slow pacing, bad acting, terrible special effects and awful camera work. An uncut DVD was released by Blue Underground. The DVD features an interview with Jesús Franco and the French theatrical trailer. Franco said in the interview that he only did the two cannibal films for the money, and admitted he had no idea why anyone would want to watch them. He said that Sabrina Siani was the worst actress he ever worked with in his life (second only to Romina Power) and that Sianis only good quality was her "delectable derrière", which he shows off to good effect in this film.

The film shares a significant amount of footage with the 1981 film Cannibal Terror. While many sources suggest that Francos footage was "borrowed" for Cannibal Terror, a closer examination reveals that there are more connections than this between the two films. Both films share a number of locations, cast, and even dubbing actors. Some connections which suggest more than a mere borrowing of footage are:

Sabrina Siani is the eponymous White Cannibal Queen of Mondo Cannibale, and also appears (as a fully clothed adult) in a bar scene in Cannibal Terror.
Several shots of the dancing cannibal tribe in their village are common to both films, and several shots appear only in one or the other.
One actor with a very distinctive face and large Mick Jagger type of mouth is seen in Cannibal Terror in no less than three roles (two cannibals and one border guard) and is also quite visible as one of the cannibals devouring Al Clivers wife in Mondo Cannibale. 
Porn star Pamela Stanford plays Manuella in Cannibal Terror, and has the brief role of the unfortunate Mrs. Jeremy Taylor in Cannibals. She also appeared in a number of Jesus Francos other films around this time period, including Lorna the Exorcist. The actor who plays Roberto in Cannibal Terror is the captain of the boat at the beginning of Mondo Cannibale.

In addition to this are the obvious cast parallels of Olivier Mathot and Antonio Mayans, both of whom have starring roles in both films.

== Plot ==

The film follows a fathers attempt to rescue his teenage daughter from a tribe of man eating primitives who have made her their queen.

==External links==
*  
 

 
 
 
 
 


 
 