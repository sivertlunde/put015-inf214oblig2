Chain Gang (1950 film)
{{Infobox film name = Chain Gang image = image_size = caption = director = Lew Landers producer = Sam Katzman writer = Howard J. Green narrator = starring = Douglas Kennedy              music = cinematography = Ira H. Morgan editing = distributor = Columbia Pictures released = November 1950 runtime = 70 minutes country = United States language = English budget = gross =
}} Douglas Kennedy    as a newspaper reporter who goes undercover to expose political corruption and the exploitation of chain gang labour.   

==Production==
Chain Gang was produced by Sam Katzman at Columbia Pictures and filmed in black and white by Ira H. Morgan.  It was reissued on DVD in 2012 as part of the Sony Choice Collection. Mavis, Paul.  . DVD Talk. 

== Plot ==

After a state senators bill to abolish chain gangs is rejected by the Senate, newspaper reporter Cliff Roberts (Douglas Kennedy)    persuades his boss Pop ODonnel (Harry Cheshire) at the liberal Capitol City Evening Standard to arrange for him to go undercover in a chain gang prison. Equipped with false employment records and a tiny microfilm camera disguised as a cigarette lighter, he tells everyone - including girlfriend Rita McKelvey (Marjorie Lord)    - that he is going on a fishing trip, but actually heads for Cloverdale Prison Farm in the deep south, scene of recent incidents which left three inmates dead.
 sweatbox as flogs Snead at the whipping post. Later he visits Snead in solitary confinement to apologise for the whipping and gains the inmates trust.

Roberts secret photographs are published in the newspaper, much to McKelveys consternation.  A foreman on the construction project (who knows Roberts as Jack Granger) sees his photograph in McKelvey house.  His cover blown, Roberts uses the confusion of fellow prisoner Sneads escape to make a run for it.  The two men try to outrun the guards and their dogs across wilderness and through woodland. Roberts is shot and left for dead by Captain Duncan,    who later pins the blame on Snead. Roberts eventually makes it to safety and is reunited with Rita. Snead is killed while on the run and McKelvey is charged with the exploitation of convict labor for personal gain.

==Critical response==
The film was praised for its acting, direction, action sequences and technical qualities, but Greens improbable storyline and dialogue came in for criticism.     

== References ==
 

== External links ==
*  at Internet Movie Database.
*  Last accessed june 28 2013

 
 
 
 
 
 