Dr. Plonk
 
{{Infobox film
| name           = Dr Plonk
| image          = DrPlonk.jpg
| image size     =
| caption        = Theatrical film poster
| director       = Rolf de Heer
| producer       = Rolf de Heer Julie Ryan
| writer         = Rolf de Heer
| based on = 
| narrator       =
| starring       = Nigel Martin
| music          = Graham Tardif
| cinematography = Judd Overton
| editing        = Tania Nehme
| studio = Australian Film Finance Corporation Vertigo Productions Pty. Ltd.
| distributor    = Fandango
| released       = 5 March 2007 (Adelaide Film Festival) 30 August 2007 (Australia)
| runtime        = 85 minutes
| country        = Australia English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}} Australian film directed by Adelaide-based Australian director Rolf de Heer. It premiered in Australia in at the 2007 Adelaide Film Festival. It is a silent film and at the premiere had live accompaniment by the Stiletto Sisters. The film was also screened at the launch of Australias National Film and Sound Archives new cinema, Arc, in August 2007. Its public cinema release is 30 August 2007.

The film, set primarily in 1907, has been described as "a time-travelling satire".  The Adelaide Film Festival program described it as "a black and white, silent comedy shot with a hand-cranked camera and brimming with romance, action and especially, slapstick comedy".  Its score was composed by Graham Tardif. It is also notable for a cameo appearance by the South Australian Premier, Mike Rann, playing the 2007 Prime Minister.

==Plot==
The story focuses on Dr Plonk, a scientist and inventor who, in 1907, determines that the world will end in 101 years. However, he is ridiculed for his beliefs and so invents a time machine in order to collect evidence from the future to prove his case. But each visit he makes to 2007 only causes him more problems, and he eventually becomes a wanted man...

==Cast==
*Nigel Lunghi, as Dr Plonk
*Magda Szubanski, as Mrs Plonk
*Paul Blackwell, as Dr Plonks assistant, Paulus
*Reg, the dog, as Tiberius the dog

==Production==
De Heer was inspired to make the film after discovering old raw film stock, which prompted him to make a silent movie. 

==Distribution==
Dr Plonk was distributed in Australia by Palace Films.

==Reception==
===Commercial===
Dr Plonk grossed $83,450 at the box office in Australia. 

===Critical===
Dr. Plonk received mixed to positive reviews from critics and audiences. Despite currently having no approval rating on Rotten Tomatoes, the film received an Audience rating of 68%.

Urban Cinefile Critics gave a positive review; "Beyond its novelty value, Dr Plonk is fresh and funny, wacky and outlandish as it combines slapstick, situation comedy and an audacious premise."

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Australian Cinematographers Society SA & WA Silver Award for Best Cinematography Judd Overton
| 
|- Film Critics FCCA Award Best Music Score Graham Tardif
| 
|-
|}

==See also==
* Cinema of Australia
* South Australian Film Corporation

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 
 