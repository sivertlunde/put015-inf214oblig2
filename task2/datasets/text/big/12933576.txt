The Iron Crown
{{Infobox film
| name           = The Iron Crown
| image          = The_Iron_Crown.jpg
| writer         = Alessandro Blasetti Renato Castellani Corrado Pavolini Guglielmo Zorzi Giuseppe Zucca
| starring       = Massimo Girotti Gino Cervi
| director       = Alessandro Blasetti
| producer       =
| cinematography = Mario Craveri Václav Vích
| distributor    = Lux film (Italy)
| released       =  : June 10, 1949 New York City
| runtime        = 97 min. Italy
| Italian
| music          = Alessandro Cicognini
| awards         =
| budget         =
}} Italian award-winning fantasy film written and directed by  Alessandro Blasetti. The film won a Coppa Mussolini award, which is the ancestor to the Golden Lion.

==Plot==
Sedemondo (Gino Cervi) succeeds his brother Licinio (Massimo Girotti) upon his death as king of Kindaor, and a messenger bearing a crown made from a nail from the true cross requests permission to cross the kingdom. The crown by legend will stay wherever injustice and corruption prevail. Sedemondo takes it to a gorge where it is swallowed by the earth. A wise woman prophesies to the king that his wife will bear a daughter and Licinios widow (Elisa Cegani) a son, that the two will fall in love, and the son take the kingdom from Sedemondo. When he gets home, he is told that his wife has given birth to a boy (the daughter having been switched with the child of Licinio) and so believes the prophesy to be invalid. He raises both the boy Arminio and girl Elsa. After some strife between the Sedemondo and Arminio, the king orders Arminio to be taken to the gorge and slain. Twenty years later, with Arminio (Massimo Girotti) having grown up in the forest, Sedemondo arranges a tournament to determine who will marry Elsa (Elisa Cegani). Tundra (Luisa Ferida) leads the resistance among the people against the king. The tournament, with various characters attending in disguise, sets up whether the prophesy will come to pass.   

==Cast==
Several actors portrayed more than one character in the film.
*Elisa Cegani: the mother of Elsa & Elsa
*Luisa Ferida: Kavaora, mother of Tundra & Tundra
*Rina Morelli: the wise old woman
*Gino Cervi: Sedemondo, the king of Kindaor
*Massimo Girotti: Licinio & Arminio, his son
*Osvaldo Valenti: Eriberto
*Paolo Stoppa: Trifilli
*Primo Carnera: Klasa, the servant of Tundra

;Dubbing
*Gualtiero De Angelis	 .... 	voice dubbing: Massimo Girotti, role of "Arminio" only (uncredited)
*Lauro Gazzolo	.... 	voice dubbing: Osvaldo Valenti (uncredited)
*Augusto Marcacci	.... 	voice dubbing: Massimo Girotti, role of "Licinio" only (uncredited)
*Cesare Polacco	.... 	voice dubbing: Primo Carnera (uncredited)
*Giovanna Scotto	.... 	voice dubbing: Dina Perbellini (uncredited)

==Production== bare breast for moments in the film, and may have been the first actress to do so in an Italian sound film. However, the credit for this is normally given to Clara Calamai in Blasettis next film, La cena delle beffe (1941),   probably because Calamai is the protagonist of that film.

==Reception==
Someone asserted that the film, while a fairy tale, critiqued the abuse of power in fascist Italy with images involving the usurpation of authority, imprisonment, and violence,  and presented the theme of the rise of a chosen ruler that will bring peace and prosperity.  The documentary The Goebbels Experiment, directed by Lutz Hachmeister, states that Nazi propaganda minister Joseph Goebbels disliked the film when he saw it at the Venice Film Festival. His comment, due to the pacifism of the plot, was that, had a German director made this film, that director would have been shot. The movie won the "Mussolini Cup" at Venice film festival in 1941.

==Awards==
*Venice Film Festival: Best Italian Film.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 