Behind the Green Door: the Sequel
{{Infobox film
| name           = Behind the Green Door: The Sequel
| image          = behind_the_green_door_the_sequel.jpg
| caption        = Videotape box cover Artie Mitchell Sharon McNight
| producer       = Jim Mitchell
| eproducer      =
| aproducer      = Artie Mitchell
| starring       = Elisa Florez (as Missy Manners) 
| music          =
| cinematography =
| editing        =
| distributor    = Mitchell Brothers Pictures
| released       = 1986
| runtime        = 90 minutes
| rating         = X
| country        = United States
| awards         =
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} AIDS crisis.  {{cite web | title = medical exam gloves.

==Cast== Republican United Senator Orrin Hatch, and daughter of the Undersecretary of Education during the administration of George H. W. Bush.   

Though Florez was in a relationship with producer Artie Mitchell, she maintains that she auditioned for the lead role in the film, a claim which many who were significantly involved in the project disputed. Her links to Republican party officials were emphasized in publicity for the film    in much the same fashion as her predecessor, Marilyn Chambers, having modeled for Ivory Snow.

==Reception==
Behind The Green Door: The Sequel was a critical and commercial disaster, and James and Arthur Mitchell ended up losing hundreds of thousands of dollars in revenue.

==References==
 

==External links==
* 
*  at the Adult Film Database

 
 
 
 
 
 

 