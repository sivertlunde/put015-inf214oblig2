The Art Dealer
{{Infobox film
| name           = The Art Dealer
| image          = LAntiquaire poster.jpg
| caption        = Film poster
| director       = François Margolin 
| producer       = François Margolin 
| screenplay     = Jean-Claude Grumberg   François Margolin  	Vincent Mariette   Sophie Seligmann 
| based on       =   Robert Hirsch Louis-Do de Lencquesaing
| music          = 
| cinematography = Caroline Champetier Olivier Guerbois 
| editing        = Xavier Sirven 
| studio         = Margo Cinema   Commune Image Média
| distributor    = Margo Cinema
| released       =  
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

The Art Dealer (French title: LAntiquaire) is a 2015 French thriller drama film produced and directed by François Margolin. The film is about a young womans quest to recover the collection of paintings stolen from her Jewish family during the Second World War. It was released in France on 18 March 2015. 

== Cast ==
* Anna Sigalevitch as Esther 
* Michel Bouquet as Raoul  Robert Hirsch as Claude Weinstein 
* François Berléand as Simon  
* Louis-Do de Lencquesaing as Melchior  
* Adam Sigalevitch as Gaspard  
* Alice de Lencquesaing as Jeanne   
* Niels Schneider as Klaus    
* Benjamin Siksou as Jean  
* Fabienne Babe as Fabienne 
* Christophe Bourseiller as Hurtado   
* Olga Grumberg as The Editor
* Lolita Chammah as Sophie

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 