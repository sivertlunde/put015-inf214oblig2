The Battle (1911 film)
 
{{Infobox film
| name           = The Battle
| image          = The Battle (1911 film).jpg
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         =  Charles West Blanche Sweet
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 19 minutes (16 Frame rate|frame/s)
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}

The Battle is a 1911 American war film directed by  D. W. Griffith. The film was set during the American Civil War. It was shot in Fort Lee, New Jersey, where  many early film studios in Americas first motion picture industry were based at the beginning of the 20th century.    Prints of the film survive in several film archives around the world including the Museum of Modern Art, UCLA Film and Television Archive, George Eastman House and the Filmoteca Española.    It was Lionel Barrymores motion picture debut.

==Cast== Charles West - The Boy (as Charles H. West)
* Blanche Sweet - The Boys Sweetheart
* Charles Hill Mailes - The Union Commander
* Robert Harron - A Union soldier
* Donald Crisp - A Union Soldier
* Spottiswoode Aitken
* Edwin August - A Union Officer
* Lionel Barrymore - Wagon Driver
* Kate Bruce - In the Town
* William J. Butler - A Union Officer/At Farewell
* Christy Cabanne - A Union Soldier (as W. Christy Cabanne)
* Edna Foster - At Dance
* Joseph Graybill - A Union Officer
* Guy Hedlund - A Union Soldier
* Dell Henderson - A Union Officer
* Harry Hyde - A Union Soldier
* J. Jiquel Lanoe - A Union Officer
* W. Chrystie Miller - At Dance
* Alfred Paget - Confederate Officer
* W. C. Robinson - A Union Soldier
* Kate Toncray - At Dance/At Farewell

==See also==
* List of American films of 1911
* D. W. Griffith filmography
* Blanche Sweet filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 