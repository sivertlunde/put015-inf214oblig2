Ma tante Aline
{{Infobox film
| name           = Ma tante Aline
| image          = Ma tante Aline VideoCover.jpeg
| caption        = 
| director       = Gabriel Pelletier Lorraine Richard Luc Martineau
| writer         = Stéphane J. Bureau Frédéric Ouellet
| starring       = Béatrice Picard Sylvie Léonard
| music          = Benoît Charest
| cinematography = Eric Cayla
| editing        = Gaétan Huot
| distributor    = Alliance Atlantis Vivafilm
| released       =  
| runtime        = 107 minutes
| country        = Canada
| language       = French
| budget         =
| gross          = 
}}
Ma tante Aline ( ) is a 2007 Canadian comedy film.

==Plot==
* Geneviève Saint-Louis (Léonard) is a successful career woman who does nothing but work. One day, her aunt Aline (Picard) shows up unexpectedly on her doorstep penniless and just one step away from a retirement home. Determined to stay out, Aline turns Genevièves life upside-down as she takes her on the ride of her life that includes a stop in Cuba.

==Recognition==
* Béatrice Picard - Genie Award for Best Performance by an Actress in a Leading Role - Nominee

==External links==
*  
*  
*  

 
 
 
 
 
 