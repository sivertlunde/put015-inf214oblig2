Las Apariencias engañan
{{Infobox film
| name = Las Apariencias engañan
| image = LasAparienciasenganan.jpg
| image size =
| caption = Lobby card
| director = Carlos Rinaldi
| producer =
| writer = Carlos A. Petit
| narrator =
| starring = Perla Alvarado   Ana Arneodo
| music =
| cinematography =
| editing =
| distributor =
| released = 1958
| runtime = 104 minutes
| country = Argentina Spanish
| budget =
| preceded_by =
| followed_by =
}}
 Argentinean romantic comedy film directed by Carlos Rinaldi and written by Carlos A. Petit, based on the novel by Rafael Maluenda.  It was released on August 27th, 1953 and rated PG 16. 

==Cast==
* Perla Alvarado
* Ana Arneodo
* Alberto Bello
* Cristina Berys
* Patricia Castell
* Benito Cibrián
* Florindo Ferrario
* Enrique García Satur
* Amadeo Novoa
* Adolfo Stray

==References==
 

==External links==
*  

 
 
 
 
 

 