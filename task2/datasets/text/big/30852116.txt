The Rain Children
{{Infobox film
| name           = The Rain Children
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Philippe Leclerc
| producer       = 
| writer         = Philippe Caza and Laurent Turner, based upon a novel by Serge Brussolo
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Didier Lockwood
| cinematography = 
| editing        = 
| studio         = Praxinos
| distributor    = MK2
| released       =  
| runtime        = 86 min.
| country        = France
| language       = French
| budget         = 6 million euros
| gross          =
}}
The Rain Children ( ) is a 2003 French and South Korean animated fantasy film directed by Philippe Leclerc. The plot is loosely inspired by Serge Brussolos novel A limage du dragon.

== Plot ==
Since the Great Sundering, the world has been divided into two parts : the land of fire, a vast desert inhabited by the people of the Pyross, and the land of water, inhabited by the Hydross. The Pyross have red skin, and stocky, muscular features; water burns their flesh and rain is lethal to them. They use sunstones (shining crystals) as both money and energy source. During the rainy season, they stay locked in their city of stone and cannot get out; they protect themselves from the rain and from the wild water dragons who wander into Pyross territory during the rainy season. The Hydross, on the other hand, have curvy features and blue or turquoise skin, and water is vital to them. In summer, the Hydross turn into stone statues and are thus vulnerable, while the Pyross can get out and venture into Hydross territory to destroy as many Hydross statues as they can before the rainy season starts again. The two people know very little about one another, and they cannot even touch one another, as the contact of Hydross skin burns the Pyross, and vice versa.

Peace between Hydross and Pyross seems impossible, but two young people, a Pyross named Skan and a young Hydross, Kallisto, meet one another in improbable circumstances and try desperately to stop the eternal war.

== Production ==
The setting and synopsis of the film were freely adapted from Serge Brussolos novel A limage du dragon, with a slightly lighter tone. The characters, backgrounds and general looks of the film were conceived by French illustrator and comic author Caza. The original soundtrack was composed by Didier Lockwood.

The film was presented at the Annecy International Animated Film Festival in 2003.

== Release ==
The DVD is distributed by MK2 Diffusion. It includes the original French version and an English dub.

== External links ==
* 

 
 
 
 
 
 
 
 


 
 