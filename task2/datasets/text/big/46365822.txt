The Golden Pavement
{{Infobox film
| name           = The Golden Pavement
| image          =
| caption        =
| director       = Cecil M. Hepworth
| producer       = 
| writer         = Percy Manton
| starring       = Alma Taylor Stewart Rome Lionelle Howard
| cinematography =
| editing        =
| studio         = Hepworth Pictures
| distributor    = Hepworth Pictures
| released       =  
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}}
The Golden Pavement is a 1915 British silent drama film directed by Cecil M. Hepworth and starring Alma Taylor, Stewart Rome and Lionelle Howard. 

==Cast==
*    Alma Taylor as Brenda Crayle  
* Stewart Rome as Dennis  
* Lionelle Howard as Martin Lestrange  
* William Felton as The crook 
* Henry Vibart as The nobleman

==References==
 

==Bibliography==
* Palmer, Scott. British Film Actors Credits, 1895-1987. McFarland, 1988. 

==External links==
* 

 
 
 
 
 
 
 
 
 

 