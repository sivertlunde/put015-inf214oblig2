The Players Club
 The Players}}
{{Infobox film
| name           = Players Club
| image          = Players club.jpg
| caption        = Theatrical release poster
| director       = Ice Cube
| producer       = Patricia Charbonnet Carl Craig Ice Cube (also executive producer)
| writer         = Ice Cube Alex Thomas Faizon Love Charlie Murphy  Adele Givens Chrystale Wilson Tracy Jones Terrence Howard Larry McCoy Ronn Riser Dick Anthony Williams Tiny Lister LisaRaye Jamie Foxx John Amos
| music          = Frank Fitzpatrick
| cinematography = Malik Hassan Sayeed
| editing        = Suzanne Hines 
| studio         = Ghettobird Productions Cube Vision Productions
| distributor    = New Line Cinema
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $5 million 
| gross          = $23,261,485 
}}
 American comedy/drama film written and directed by Ice Cube, who made his directorial debut and also has a small role in the film. The film stars Bernie Mac, Jamie Foxx, Alex Thomas, Faizon Love, John Amos, Terrance Howard, Charlie Murphy, Monica Calhoun and introducing LisaRaye.

==Plot==
Diana Armstrong (LisaRaye) arrives at the scene of the charred, destroyed Players Club. She narrates of that she used to work at the club and begun when she moved out of her parents home after an argument with her father, as she was pregnant with her son. Diana ends up working at a shoe store, when she meets Ronnie (Chrystale Wilson) and Tricks (Adele Givens), who work for Dolla Bill (Bernie Mac) at The Players Club. They convince Diana she would make better money stripping, by saying, "Use what you got to get what you want."

Dolla Bill gives Diana a job, giving her the name Diamond. Everything is fine until four years into the game her cousin Ebony Armstrong (Monica Calhoun) comes to live with her. After listening to Dollas rendition of "The Strippin Game", she starts working at the club.

Ebony is soon out of control, excessively drinking, staying out all night and influenced more and more by Ronnie and Tricks, who encourage Ebony to do more out-of-club parties for groups of men. Diana tries to warn Ebony to stay away from "those two" and quit doing house parties, but Ebony declines being told what to do, choosing to ignore Dianas advice as well as cold-heartedly reminding her about the time when Ronnie sexually took advantage of Diana when she was passed out at a previous party.

Meanwhile, Dolla Bill gets confronted by a man who works for Saint Louis who is a druglord that Dolla Bill owes money to. He warns Dolla Bill if he doesnt pay Sain Louis his money he will hunt him down. That same night Saint Louis comes to his club to collect. Little Man who is the Doorman of the club tells Saint Louis Dolla Bill is not at the club and they leave. The next day as Dolla Bill Tries to leave the club he gets confronted by Sain Louis men Brooklyn and KC. They beat him unconscious and throw him into the back of his car. Luckily, for Dollar however, they are stopped by Freeman and Peters, two crooked cops. However Dolla Bill is found in the trunk and is arrested on warrants. The Following night,  when a famous rapper, Luke (Luther Campbell) comes to the strip club, Reggie (Ice Cube) and his friend Clyde (Alex Thomas) are discussing about him. When Dolla Bill, who bailed out of jail and returned to his club, is notified that Luke is at his club he alerts the strippers via a money alarm and believes he will gain a fortune profit. Clyde tries to meet Luke but his bodyguard (Michael Clarke Duncan) informs Clyde that Luke is trying to relax. Clyde insults the bodyguard causing him to hit Clyde in the back of his head as he leaves, leaving him unconscious. In retaliation, Reggie beats down the bodyguard and belligerently fights against Luke and his friends, only for him to be defeated and thrown into a glass window.

The dazed Reggie opens fire wildly before being knocked unconscious by the bouncer XL (Tiny Lister). Further tension develops between Ebony and Diana when Diana returns home one night after fleeing her obsessive customer Myrin, who admitted to stalking her and then tried to force entry to her apartment, to find Ebony in bed with her boyfriend Lance. Diana chases Lance out with her gun, shooting at him several times. Diana threatens and taunts Ebony. When Diana feigns leaving,  Ebony is hit when she opens the door and is thrown out the apartment.

Diana then begins dating Blue (Jamie Foxx), a DJ at the Players Club. Meanwhile, Ebony is offered a gig to dance at Ronnies brother Juniors (Samuel Monroe Jr.) bachelor party, under the pretense from Ronnie that other girls from the club will be dancing there, too. When she realizes that she will be the only woman there in a hotel room full of hungry men, she desperately tries calling Diana to come and pick her up, but Diana doesnt, still being mad at her.

Reggie and Clyde, both feeling dissed by Diana and Ebony from a previous encounter, lie to Junior that Ebony will have sex with him, claiming that they "ran a train" on her. Excited, Junior bursts in on Ebony while she is changing. Ebony resists, prompting Junior to beat and rape her. Soon, Reggie, Clyde and the other guests leave, wanting no involvement with Junior anymore. When Ronnie discovers Ebony unconscious, she and Junior flee the hotel room as she taunts him. Later, Diana has a change of heart and she and Blue decide to check up on Ebony at the hotel, only to discover her bloody and unconscious body on a bed.

This proves to be the last straw to Diana, who was growing tired of the stripper game anyway. Angered, she grabs her gun and goes to the Players Club, where Ronnie and Tricks are hiding out. After scaring the other strippers away by firing a warning shot Diana gives the gun to Blue to cover her while she gets into a brutal fistfight with Ronnie, leaving Ronnie badly beaten. She quits in front of Dolla Bill, punches Tricks and leaves along with Blue.

Ronnie and Tricks are arrested by the police on charges of the rape of Ebony, and Junior had been arrested moments before. Later that night, Dianas timing proves to be perfect when  St. Louis, comes to collect. He personally shoots up the club (though he does give warning to the customers beforehand, allowing them to leave). After his thugs grab Dolla he is face to face with St. Louiss associate who had confronted him earlier and thrown into the trunk of his car, St. Louis has his henchman Brooklyn (Charlie Murphy) destroy the club with a LAW rocket.

Ebony, still sporting the bruises from her rape, now has a job working at the shoe store. Having been berated by two strippers who work at a new club, called Club Sugar Daddys, with the same slogan that influenced her to strip, she firmly stands her ground. In the end, Diamond narrates that Ebony moved back to Tallahassee, FL to be with her mother. DJ Blue is a top DJ at a radio station, and that he and Diamond are still dating. Ronnie and Tricks got jobs at Club Sugar Daddys after their release from jail. Junior is serving time in prison for raping Ebony, and that he never got married. Reggie and Clyde were last seen in Atlanta. St. Louis is still "running the South" along with his crew. Little Man is Managing a Strip Club in Chicago. Dolla Bill was never heard from again (it can be safely assumed that he was murdered by St. Louiss men for his debts, as he was last seen being stuffed into the trunk of their car). Diamond is now a successful reporter.

==Cast==
* Bernie Mac as Dollar Bill: the dim owner of The Players Club who is threatened by a loan shark name St. Louis as Dollar Bill has yet to pay him. At one point earlier in the film, he was captured by Brooklyn and K.C. but escapes death when the two loan sharks are arrested. At the end, he was re-captured by St. Louis men and is presumed to be murdered after.
* LisaRaye as Diana "Diamond" Armstrong: a college worker who is employed at The Players Club to raise money for her college funds as she aspires herself into becoming a journalist. However, Ronnie and Tricks opposes a rivalry against her and she quits after a beatdown between her and Ronnie.
* Monica Calhoun as Ebony: Diamonds younger cousin who becomes sensibly out-of-control after being employed in The Players Club. She befriends Ronnie and Tricks, who manipulates and prompts her throughout. In the end, she begins working at a shoe store after being beaten and raped by Ronnies younger brother.
* Jamie Foxx as Blue: the clubs radio disc jockey and Diamonds love interest of the film.
* Chrystale Wilson as Ronnie: A stripper at The Players Club who rivals against Diamond as she aids Ebony. She is later beaten by Diamond due to her affiliation with Ebonys beating and rape (though she was unaware about this) and was arrested.
* Adele Givens as Tricks: A stripper at The Players Club and a right-hand of Ronnie. She was arrested at the end of the film. Anthony Johnson as Lil Man: the clubs doorman who is frequently taunted and harmed by St. Louis and his crooks when he covers up for Dollar Bills absences.
* Tiny Lister as XL the Bouncer: a tall, muscular bouncer of the club. Larry McCoy as St. Louis: a wheelchair-bound gangster who Dollar Bill owes money to.
* Charlie Murphy as Brooklyn: St. Louis main henchman.
* Terrence Howard as K.C.: St. Louis secondary henchman and Brooklyns partner in crime.
* Ice Cube as Reggie: a regular who is somehow working for St. Louis and has affection towards Ebony. Alex Thomas as Clyde: Reggies best friend who, like Reggie, has an affection towards Ebony and works for St. Louis.
* Samuel Monroe Jr. as Junior: Ronnies younger brother.
* Faizon Love as Officer Peters: a police officer who harasses suspects.
* John Amos as Officer Freeman: another police officer who harasses suspects.
* Luther Campbell as Luke: a rapper who visits the club.
* Michael Clarke Duncan as Bodyguard: the tall, muscular bodyguard for Luke.
* Montae Russell as Lance: Diamonds boyfriend who she ended her relationship with due to his attraction towards Ebony.

==Soundtrack==
 
A successful soundtrack was released on March 17, 1998, peaking at #10 on the Billboard 200|Billboard 200 and #2 on the Top R&B/Hip-Hop Albums. It featured artists such as Ice Cube, DMX (rapper)|DMX, Master P and Jay-Z, among others.

===Box office===

The movie debuted at No. 5.  It wents on to gross $23,047,939 Domestically, and  $213,546 	in Foreign Markets. For a total Lifetime gross of $23,261,485.    	 	

==Trivia==
 Ice Cube have worked together. The first was Friday (1995 film)|Friday and the third time was Ice Cube guest starring on The Bernie Mac Show.
* This film marks the screen debut of LisaRaye.
* This was Bernie Macs first starring / lead role. Friday as Diana was studying. Ice Cube, Bernie Mac, Faizon Love, Tiny Lister and A.J. Johnson starred in Friday (1995 film)|Friday.
* In the scene where Officers Peters and Freeman are looking for Dollar Bill. Lil Man tells Officer Freeman "You look just like the father from Good Times". John Amos, who plays Officer Freeman, actually played James Evans on the show for three seasons until the character was killed off.
* This is the first time that Jamie Foxx and Terrence Howard worked together, the second being the film Ray (film)|Ray. This is also the first time that Howard and Mac worked together, the second being the film Pride (2007 film)|Pride.
* This is the second time that Jamie Foxx and Bernie Mac worked together, the first being the film Booty Call. The Best Man.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 