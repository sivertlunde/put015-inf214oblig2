The Oranges (film)
{{Infobox film
| name           = The Oranges
| image          = Orangesfilmposter.jpg
| alt            = 
| caption        = 
| director       = Julian Farino
| producer       = {{Plainlist|
* Anthony Bregman
* Leslie Urdang
* Dean Vanech
}}
| writer         = {{Plainlist|
* Ian Helfer
* Jay Reiss
}}
| narrator       = Alia Shawkat
| starring       = {{Plainlist|
* Hugh Laurie
* Catherine Keener
* Oliver Platt
* Allison Janney
* Alia Shawkat
* Adam Brody
* Leighton Meester
}}
| music          = {{Plainlist|
* Klaus Badelt
* Andrew Raiher
}}
| cinematography = Steven Fierberg
| editing        = {{Plainlist|
* Jeffrey M. Werner
* Carole Kravetz Aykanian
}}	
| studio         = {{Plainlist|
* Olympus Pictures
* Likely Story
}}
| distributor    = ATO Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $7 million    
| gross          = $366,377    
}}

The Oranges is an American romantic comedy directed by Julian Farino and starring Hugh Laurie, Leighton Meester, Catherine Keener, Oliver Platt, Allison Janney, Alia Shawkat, and Adam Brody. The film chronicles how two families deal with a scandal involving a married man and his friends daughter. Set in The Oranges area of Essex County, New Jersey, The Oranges was primarily filmed in New Rochelle, New York. It premiered at the Toronto International Film Festival on September 10, 2011 and was released by ATO Pictures in the United States on October 5, 2012. The film received mixed reviews upon its release.

==Plot==
David and Paige Walling (Hugh Laurie and Catherine Keener) and Terry and Cathy Ostroff (Oliver Platt and Allison Janney) are best friends and neighbors living on Orange Drive in suburban West Orange, New Jersey. Their comfortable existence goes awry when prodigal daughter Nina Ostroff (Leighton Meester), newly broken up with her fiancé Ethan (Sam Rosen), returns home for Thanksgiving after a five-year absence. Rather than developing an interest in the successful son of her neighbors, Toby Walling (Adam Brody), which would please both families, it is her parents best friend David who captures Ninas attention. When the connection between Nina and David is discovered, everyones lives are thrown into upheaval, particularly Vanessa Wallings (Alia Shawkat), Ninas childhood best friend. It is not long before the ramifications of the affair begin to work on both families in unexpected ways, leading everyone to reassess what it means to be happy.

==Cast==
===The Wallings===
* Hugh Laurie as David (father)
* Catherine Keener as Paige (mother)
* Adam Brody as Toby (son)
* Alia Shawkat as Vanessa (daughter)

===The Ostroffs===
* Oliver Platt as Terry (father)
* Allison Janney as Cathy (mother)
* Leighton Meester as Nina (daughter)

===Others===
* Sam Rosen as Ethan
* Tim Guinee as Roger
* Cassidy Gard as Samantha
* Heidi Kristoffer as Meredith
* Jennifer Bronstein as Amy
* Stephen Badalamenti as Taxi Driver
* John Srednicki as Waiter
* Rachel Gittler as Rave Dancer (uncredited)
* Taylor Clarke-Pepper as Dog Walker (uncredited)
* Steven T. Jenkins as Standing Man (uncredited)
* Gibbins von Hammerschmidt as The Silently Gawking Man (uncredited)
* Jillian C. Williams as Fat Sad Woman (uncredited)
* Vivica A. Section as Frightening Pediatrician (uncredited)

==Production==
  The Beaver Writers Guild of America strike.    The film marks Julian Farinos debut as motion picture director; he had previously directed episodes of Entourage (TV series)|Entourage.

Richard Geres agent Ed Limato called Farino to tell him Gere was interested in the script,    but Farino wanted only Hugh Laurie to play the role of David because he had "that innate decency that could carry this thing".    On February 8, 2010, Laurie was reported to be in talks to play his first feature lead role.    Leighton Meester and Mila Kunis were also said to join the cast as his love interest.  On February 28, it was confirmed that Meester had won the role over Kunis, while Adam Brody, Alia Shawkat, Catherine Keener and Allison Janney were all in negotiations.  

Meester and Laurie had previously worked together when she guest-starred in two episodes of   for the part. After Page turned it down, Shawkat auditioned twice and was cast.    Allison Janney and Olvier Platt had also worked together before in the television series The West Wing. 
 Tropicana Casino in Atlantic City, New Jersey.  The film was shot with Red cameras.  The narration was provided by Shawkats character Vanessa whose voice-over was recorded in post-production during the editing of the film.   ATO Pictures acquired the rights of distribution in September 2011. 

==Release==
 
The film premiered at the Toronto International Film Festival on September 10, 2011 and opened the Montclair Film Festival on May 1, 2012.  It received a limited release in the United States on October 5, 2012, being screened in 110 theaters.  The film was released in the UK on December 7, 2012. 

===Critical response===
The Oranges received mixed reviews. Review aggregate   it has a weighted score of 46 out of 100, based on 23 critics, indicating "Mixed to Average Reviews".   

Giving it 3 stars out of 4, Moira Macdonald of  s Nina is absolutely radiant, but Hugh Laurie|Lauries David is a dour dullard. There are some genuinely moving moments, but the mayhem-in-suburbia slapstick falls flat. The film is certainly unpredictable, but thats partly because it doesnt know its own mind." 

The    Lou Lumenick wrote: "While there are laughs, the farcical elements of The Oranges are not presented with sufficient discipline to live up to the full potential of its cast."  The Hollywood Reporter critic David Rooney praised members of the cast "who manage to hit a sweet spot even in this mediocre material," noting that the film "runs out of juice" when it becomes serious and that the films "cathartic moments feel fabricated" and concluded that the cast "deserves better." 

Stephen Holden of The New York Times said the films problem is that its creators did not decide what genre The Oranges would be; "a dangerous comic satire or a serious dramatic downer," and instead made "a wishy-washy middle ground. As comedy, it isn’t funny; as serious drama, it lacks a moral and emotional center."  Despite Vanessa (Alia Shawkat) "the most interesting character" praising her "sardonic perspective," the Los Angeles Times reviewer wrote that the film "never fully comes to life."   While Entertainment Weekly reviewer Owen Gleiberman  found Meester "charming,"  The Washington Post  Michael OSullivan felt that lightness and brightness were missing from her performance and that she took the film too seriously. 

===Home media===
The Oranges was released on DVD and Blu-ray disc|Blu-ray Disc on May 7, 2013. 

==References==
 

==External links==
*   
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 