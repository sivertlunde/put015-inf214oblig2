Wildflower (1914 film)
{{infobox film
| name           = Wildflower
| image          = Wildflower - 1914.jpg
| caption        = 1914 lobby poster
| director       = Allan Dwan
| producer       = Adolph Zukor Daniel Frohman
| writer         = Allan Dwan (scenario) Eve Unsell (scenario)
| story          = Mary Germaine
| starring       = Marguerite Clark Harold Lockwood Jack Pickford
| music          =
| cinematography = Henry Lyman Broening
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 4 reel#Motion picture terminology|reels; 4,163 feet
| country        = United States
| language       = Silent English intertitles
}}

Wildflower was a 1914 American silent romantic drama film produced by Adolph Zukor and directed by Allan Dwan. It stars stage actress Marguerite Clark in her first motion picture. Clark would be one of the few stage stars to go on to superstardom in silent pictures.  The film is now presumed Lost film|lost. 

==Cast==
* Marguerite Clark - Letty Roberts
* Harold Lockwood - Arnold Boyd
* James Cooley - Gerald Boyd
* Edgar L. Davenport - The Lawyer
* Jack Pickford - Bud Haskins

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 