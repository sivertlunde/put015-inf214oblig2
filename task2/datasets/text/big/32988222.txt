Marilyn (1963 film)
{{Infobox film
| name = Marilyn
| director = Harold Medford 
| studio = 20th Century Fox
| distributed by = 20th Century Fox
| narrator = Rock Hudson
| starring = Marilyn Monroe  (archive footage)
| released =     
| runtime = 83 minutes   
| country = USA  English
}} 1963 documentary film based on the life of the 1950s sex symbol Marilyn Monroe. The film, directed by Harold Medford,    was released by 20th Century Fox,    and was narrated by Rock Hudson.   

==Background== 1953 film Gentlemen Prefer Blondes (1953), How to Marry a Millionaire (1953), and River of No Return (1954).
 Bus Stop (1956), which was her last film with Fox until 1960. However, Monroes biggest success in a film was in 1959s Some Like It Hot, a comedy co-starring Tony Curtis and Jack Lemmon.

However, Marilyn only focuses on the films that Monroe made with 20th Century Fox, which excludes Some Like It Hot, as it was released by Metro-Goldwyn-Mayer. Monroes last film with Fox was the 1960 romance film|romantic-musical comedy Lets Make Love, also starring Yves Montand. In 1962, she signed on with Fox to remake a 1940 film called My Favorite Wife. The film was re-titled Somethings Got to Give and cast Monroe in the lead alongside Dean Martin, Cyd Charisse, Wally Cox, and Phil Silvers. Monroe died before the films completion. In 1963, Fox recast Somethings Got to Give, with Doris Day, James Garner, and Polly Bergen, and re-titled the film to Move Over, Darling.

==Reception==
The reception of Marilyn was both good and bad. Some called the film "20th Century Foxs final chance to make some bucks off of Marilyn Monroe," while some called the film "marvelous." It drew both good and bad press reviews.

==Releases==
To date, Marilyn has not been released on either DVD or VHS.

==External links==
* 

 

 

 
 
 
 
 
 
 