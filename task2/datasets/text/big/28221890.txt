Satyamev Jayate (film)
 
 
{{Infobox film
| name           =Satyamev Jayate
| image          =Satyamev_Jayate_(1987_film).jpg
| image_size     = 206px
| border         = 
| caption        = DVD release cover
| director       = Raj N. Sippy
| producer       = Romu N. Sippy	
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Madhavi Anita Raj
| music          = Bappi Lahiri
| cinematography = Anwar Siraj	 
| editing        = Ashok Honda	 	
| studio         = 
| distributor    = 
| released       =    
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
Satyamev Jayate is a Bollywood action film released in 1987 directed by Raj N. Sippy and starring Vinod Khanna and Meenakshi Sheshadri in the lead roles. The other cast members included Madhavi (actress)|Madhavi, Anita Raj, A. K. Hangal, Renu Joshi, Alankar, Neeta Puri, Saahil Chadha, Satyen Kappu,Sudhir Dalvi, Vinod Mehra, Shakti Kapoor and Anupam Kher.

The movie is a remake of the Malayalam film Aavanazhi, starring Mammootty.

==Cast==
* Vinod Khanna...Police Inspector Arjun Singh 
* Meenakshi Sheshadri...Seema
* Madhavi (actress)|Madhavi...Pooja Shastri 
* Anita Raj...Vidya Kaul 
* Asrani...Mewaram 
* Anupam Kher...Advocate Amar N. Kaul 
* Shakti Kapoor...Chaman Bagga 
* Gulshan Grover...Satya Prakash 
* Vinod Mehra...Police Inspector Mirza
* OmShiv Puri...Minister

==Plot==

Inspector Arjun Singh (Vinod Khanna) of Bombay Police has attained a sordid reputation of being of the most ruthless policemen in India, and is known for his torture and brutality. When a young man is killed in custody, Arjun is spoken to and warned, and subsequently transferred to a small town of Tehsil. Arjun denies these charges and asserts, in vain, that the death was not his fault. Arjun re-locates there, and finds to his horror that the family of the young man who died in his care, are his neighbors, and that this town regards him as enemy number one. Arjun must now come to terms with his past and compromise in this town, and find out what were the circumstances behind this young mans death or just get another transfer. However he is abandoned by his lover Vidya (Anita Raj) and a determined Pooja (Madhvi) is doing all she can to bring justice to her brothers murder. Arjun finds solace in alcoholism and a prostitute, Seema (Meenaskhi Sheshadri).

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jano Meri Jano (Male)"
| Bappi Lahiri
|-
| 2
| "Jano Meri Jano (Female)"
| S. Janaki
|-
| 3
| "Jano Meri Jano (Sad)"
| Bappi Lahiri
|-
| 4
| "Tan Hai Hamara Himalay Jaisa"
| Shailender Singh
|-
| 5
| "De Rahi Hai Duya Behna Tere"
| Kavita Krishnamurthy
|-
| 6
| "Tu Jaan Se Pyaara Hai"
| Mitalee Mukherjee
|-
| 7
| "De Rahi Hai Duya Behna Tere (Sad)" Kavita Krishnamurthy 
|}
==External links==
* 

 
 
 
 