Snow White and the Three Stooges
{{Infobox film
| name = Snow White and the Three Stooges
| image = Snow white one sheet.jpg
| director = Walter Lang
| based on = Snow White by The Brothers Grimm
| producer = Harry A. Romm Charles Z. Wick
| writer = Noel Langley Elwood Ullman Charles Wick
| starring = Moe Howard Larry Fine Joe DeRita Carol Heiss Edson Stroll Patricia Medina Harry Harris Lyn Murray
| cinematography = Leon Shamroy
| editing = Jack W. Holmes
| distributor = 20th Century Fox
| released =  
| runtime = 107 21" 
| country  = United States
| language = English
| budget = $3,500,000 
}}
 second feature Snow White and the Seven Dwarfs. The film was retitled Snow White and the Three Clowns in Great Britain.

Olympic gold medalist figure skater Carol Heiss starred as Snow White, who must flee her home after The Evil Queen, her evil stepmother, wishes her to be dead. Seeking refuge in the cottage of the seven dwarfs, she accidentally meets the Stooges, who are house sitting for them while they are away.

==Plot== Queen is a beautiful, but evil woman who soon becomes jealous of Snow Whites beauty.

On her 17th birthday, Snow Whites father dies and the wicked queen immediately imprisons her. Eventually, the queens jealousy of her stepdaughter becomes so great that she orders her killed. Snow White escapes her hired assassin and finds refuge in the empty cottage of the seven dwarfs, soon to be joined by the Three Stooges, who are traveling to the castle with their ward Quatro. But the boy they have raised since childhood (also narrowly escaping an assassination attempt by the queen) is in reality Prince Charming, who though he has lost his memory, is betrothed to Snow White.

Snow White and the Prince fall in love, but the queen has him kidnapped when she suspects his true identity. The Stooges, disguised as cooks, attempt to rescue him, but he falls from a staircase in the palace and is presumed dead. Meanwhile, the queen learns from her magic mirror that Snow White is still alive. With the help of her magician, Count Oga, she transforms herself into a witch and succeeds in getting Snow White to take a bite from a poisoned apple.

 
As she rides back to the palace, she encounters the Stooges, and thanks to an inadvertent wish they make on a magic sword (stolen from Count Oga), she crashes her broom into a mountainside and falls to her death. The Stooges then find the poisoned Snow White, but they do not bury her. Instead, they place her on a bed, and pray to her each day.

Meanwhile, the Prince (Quatro) has not died from his fall. Instead, he is saved by a group of men who want to revolt against the Evil Queens rule over Fortunia. As the prince recovers, he realizes that his memory has returned, and so he knows that he is indeed a Prince, and that Snow White is the princess he was destined to marry.

After leading a successful revolt which places him on the throne of Fortunia, the prince sends out searchers to find Snow White and the Stooges, unaware that, thanks to yet another inadvertent wish on Count Ogas sword, they are no longer in the country of Fortunia. All searches are fruitless, and Prince Charming is close to giving up hope when he learns of the Evil Queens magic mirror. The mirror responds truthfully to the desperate Princes pleas, and the Prince sets off on his journey. He arrives at the Stooges cabin just in time to dispel the effects of the poisoned apple. Snow White and Prince Charming are married and live happily ever after.

==Primary Cast==
 ) and Snow White (Carol Heiss)]]
*Moe Howard: Moe
*Larry Fine: Larry
*Joe DeRita: Curly Joe
* 
*Edson Stroll: Quatro/Prince Charming
* 
*Guy Rolfe: Count Oga Michael David: Rolf
*Buddy Baer: Hordred
*Edgar Barrier: King Augustus Peter Coe: Captain Mark Bailey: Captain of the Guard
*Mel Blanc: voice of Quinto
*Herbie Faye: Head Cook
*Blossom Rock: Servant
*  (voice)

==Production==
Frank Tashlin was originally set to direct the film at a budget of $750,000 ($ }} today). Fox then replaced him with Walter Lang, who had previously directed the Stooges in 1933s Meet the Baron. Lang transformed the film into a lavish epic with a budget of $3.5 million ($ }} today), making it the most expensive film the Stooges ever made.     The film was produced and co-written by future U.S. Information Agency head Charles Z. Wick.
 Bill Lee. 

Patricia Medina later recalled that Carol Heiss repeatedly tried to get her fired from the film. 

==Reception==
Despite the significant investment, Snow White and the Three Stooges did poorly at the box office. The only 1960s Stooge feature filmed in color, it also became the least popular. Critics did not take kindly to the film, citing a lack of on-screen time for the trio. The studio was unable to recover the films costs because the comedy-fantasy was tailored specifically to children who paid only fifty cents each for admission. It would have taken an audience of over 15 million minors for Snow White and the Three Stooges to merely break even. Moe Howard himself often referred to the film as "a Technicolor mistake."   

The film, however, was nominated for the Writers Guild of America award for Best Musical Screenplay for 1961.

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 