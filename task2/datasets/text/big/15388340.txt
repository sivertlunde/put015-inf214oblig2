A Banda das Velhas Virgens

{{Infobox film
| name           = A Banda das Velhas Virgens
| image          = ABandadasVelhasVirgens1979Poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical Poster
| director       = Amácio Mazzaropi   Pio Zamuner
| producer       = Amácio Mazzaropi
| writer         = Rajá de Aragão
| screenplay     = Amácio Mazzaropi
| story          = Amácio Mazzaropi
| based on       =  
| narrator       = 
| starring       = Amácio Mazzaropi Geny Prado Renato Restier André Luiz de Toledo Cristina Neves
| music          = Hector Lagna Fietta
| cinematography = Pio Zamuner
| editing        = Walter Wanni
| studio         = 
| distributor    = PAM Filmes
| released       =  
| runtime        = 100 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1979 Brazilian film by Amácio Mazzaropi. 

In this movie, the character key, called Gostoso, is responsible for a band formed by older women and religious women. Everything goes well until the farmer is expelled, along with the family of their land. He restarts his life in the capital, collecting scrap, and becomes the main suspect of a theft. 

==Notes==
 


 
 
 
 
 
 
 
 