Anokha
{{Infobox film
| name           = Anokha
| image          = Anokha75.jpg
| image_size     = 
| caption        = 
| director       = Jugal Kishore
| producer       = Jugal Kishore, Jugal Productions
| writer         = Roshanlal Bhardwaj
| narrator       = 
| starring       =
| music          = Kalyanji Anandji
| cinematography = 
| editing        = Satish Kumar Bajaj
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1975 Bollywood film directed by Jugal Kishore.

==Cast==
*A.K. Hangal - Hridaynath Jeevan - Manchanda
*Zarina Wahab - Sudha Manchanda Hercules - Hercules, Manchandas goon Kanhaiyalal - Lala Kanhaiyalal
*Imtiaz Khan - Shambu Khanna
*Master Kotak - Kishan Maruti - Gambler
*Shatrughan Sinha - Ram/Anokha/Shambu Khanna Paintal - Raja, Rams friend
*Mohan Sherry - Dhanoo
*Meena T. - Dhanoos wife
*Jayshree T. - Suzy

==Crew==
*Director - Jugal Kishore
*Producer - Jugal Kishore
*Story - Roshanlal Bhardwaj
*Editor - Satish Kumar Bajaj
*Production Company - Jugal Productions
*Music Director - Kalyanji Anandji Saawan Kumar
*Playback Singers - Amit Kumar, Kishore Kumar, Mahendra Kapoor, Mukesh (singer)|Mukesh, Mohammad Rafi, Suman Kalyanpur, Usha Timothy

==Music==
{|class="wikitable"
|-
!Song Title !!Singers
|-
|"Apna desh videsh ke aagey" Mukesh
|-
|"Jaaneman aap humse mohabbat" Mohammad Rafi, Suman Kalyanpur
|-
|"Jab aai hai gaonse hamar goribala" Mahendra Kapoor, Kishore Kumar & chorus
|-
|"Meri batonse tum bore ho gai" Amit Kumar
|-
|}

==External links==
*  

 
 
 
 

 