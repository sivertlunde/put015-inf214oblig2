Chicken Park
{{Infobox Film |
 name =Chicken Park |
 image = Chicken Park.jpg|
 writer =Gino Capone Galliano Juso |
 starring =Jerry Calà Alessia Marcuzzi Rossy de Palma  |
 director =Jerry Calà |
 cinematography =Blasco Giurato |
 producer = Galliano Juso|
 distributor = Metro Film|
 released =April 5, 1996  (Turkey)  August 21, 1996  (Spain)  |
 runtime = 93 min|
 music = Giovanni Nuti Umberto Smaila |
 awards = |
 language = English language|English|
 budget = |
}} comedy parody Jurassic Park.  The movie was released on April 5, 1996 in Turkey and Calàs directorial debut.   

==Synopsis==
When Vladimiros (Jerry Calà) chicken is stolen while hes in the Dominican Republic for a cockfight, he immediately begins trying to get it back. During his searches he discovers an entire zoo full of gigantic chickens. 

==Cast==
*Jerry Calà as Vladimiro
*Demetra Hampton as Sigourney	
*Alessia Marcuzzi as Airplane hostess
*Lawrence Steven Meyers as Dr. Eggs
*Rossy de Palma as Necrophelia Addams

==Production==
The film was shot between Santo Domingo and Rome.     It was filmed in English.     The budget was two billion and a half lire.     The film marked both the directorial debut of Calà and the film debut of actress Alessia Marcuzzi.  It was also the last work as special effects artist for Antonio Margheriti.  Calà had previously announced his debut as director with a completely different project, a biographical drama film about Umberto Bossi and Lega Nord, Il Longobardo, that was eventually abandoned. 

== Parodies  ==
The film mainly parodies Jurassic Park but also references many outher films, many companies and cultural icons such as...

*The Addams Family
*Godzilla
*Home Alone
*The Deer Hunter
*Full Metal Jacket
*Sister Act
*Pretty Woman
*Hot Shots!
*Ferrero Rocher
*Mandrake the Magician
*Pope John Paul II appears as washer
* 
*Edward Scissorhands

==Release==
Chicken Park premiered at the 14th edition of Fantafestival, in which it entered the main competition.  Due to production problems, it was not released theatrically in Italy.  Broadcast by Italia 1, at its television premiere the film was seen by over 4 million people.  The film was later released in Turkey as Piliç parki and subsequently in Spain as Pollo jurásico.

==Reception==
Critical reception for the film was generally negative,  with Lespresso calling the film "trash" and MDZ Online commenting that the film was of such poor quality that it had the potential to become a cult classic.   Film critic Marco Giusti referred to the film as "supertrash", "more stupid than funny" and a film in which the best actor probably is Jo, the chicken owned by Calà.    According to film critic Paolo Mereghetti the film fails almost entirely to raise a smile and the English version of the film, with Calà self-dubbing himself, is a must for trash-lovers.  According to author Massimo Bertarelli, the film is just made of "exasperating drollery and depressing sloppiness". 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 