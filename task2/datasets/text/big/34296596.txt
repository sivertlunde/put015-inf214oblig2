Surakshaa (1995 film)
{{Infobox film
| name           = Surakshaa
| image          = Surakshaa199film.jpg
| image_size     =
| caption        =
| director       = Raju Mavani	 	
| producer       = Nitin Mavani
| writer         = Taluk Daars
| narrator       =
| starring       = Sunil Shetty Saif Ali Khan Aditya Pancholi Monica Bedi
| music          = Anu Malik
| cinematography = Sanjay Malvankar
| editing        = A. Muthu
| distributor    = 
| released       = March 3, 1995
| runtime        = 148 mins
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Bollywood film directed by Raju Mavani and produced by Nitin Mavani. It stars Saif Ali Khan, Aditya Pancholi, Sunil Shetty, Sheeba, Divya Dutta and Monica Bedi in pivotal roles. Other casts include Kader Khan, Mukesh Rishi, Tinnu Anand. 

==Cast==
* Sunil Shetty...Raja
* Aditya Pancholi...Suraj / Prince Vijay
* Saif Ali Khan...Amar / Prince Vijay
* Sheeba Akashdeep...Diana
* Divya Dutta...Bindiya
* Monica Bedi...Kiran
* Mukesh Rishi...Jagtap
* Kader Khan...Manager
* Tinnu Anand...Dhanraj 
* Arun Bakshi...Laxman Singh
* Achyut Potdar...Vikram Singh
* Waheeda Rehman...Managers mom

==Plot==
Story revolves around characters from different walks of life who soon get involved with each other - millionaires, goons. Raja (Sunil Shetty) is a small time good hearted goon who does what he wants without any regard for cops or criminals, thus making him an enemy of underworld don Dhanraj. Suraj is another small time goon who also does good for people and lives with his girlfriend Bindiya, trying to make a living by working against criminals, thus falling into Dhanrajs bad books too. And Dhanrajs arch rival is JAgtap. News one day reaches them that a millionaire Laxman Singhs (Arun Bakshi)  niece, Kiran is coming from the U.S. and has a ring worth 2.5 million. It is also known that Prince Vijay, nephew of Laxman Singhs closest friend Vikram Singh (Achyut Potdar), is coming from London to wed Kiran.

The criminals soon plot to kidnap Kiran and get the ring as well as money from her. Jagdap approaches Raja, who is otherwise uninterested in this, and offers him 5 million to kidnap Kiran for him, promising hiom that he wont harm her.

The film now moves to Kirans hotel, where Amar (Saif ali khan) is seen joining as a waiter, much to the irritation of the comical hotel manager (Kader Khan). Raja manages to impress Kiran on her way to the hotel and get a room, and Kiran sees Amar at the hotel and gets highly impressed with his looks. Suraj in the meantime captures prince Vijay and goes as an impostor to Kirans hotel himself. Everyone but Amar and Raja think he is Prince Vijay.

As time passes, Laxman Singh and Vikram Singh arrive at the hotel, and Suraj is exposed. He then confesses that he had only come to the hotel to steal Kirans ring, and promises to release the real prince Vijay.

==Soundtrack==
With Anu Maliks groovy music and Faaiz Anwars awesome songwriting, Bam Bam Bam and O Mere Sanam are two popular tracks of the movie.
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bam Bam Bam (Female)"
| Alisha Chinai
|-
| 2
| "Bam Bam Bam (Male)"  
| Udit Narayan
|-
| 3
| "Dil Mein Ho Pyar"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Kaali Aankhon Wali"
| Kumar Sanu
|-
| 5
| "Masoom Sanam"
| Kumar Sanu, Alka Yagnik
|-
| 6
| "O Mere Sanam"
| Abhijeet Bhattacharya|Abhijeet, Udit Narayan, Alisha Chinai
|}

==References==
 

==External links==
* 

 
 