Pooja Phalam
 

{{Infobox film
| name           = Pooja Phalam
| image          =
| image_size     =
| caption        =
| director       = B. N. Reddy
| producer       = Daggubati Lakshmi Narayana Chowdary Savitri Jamuna Jamuna Rajasree
| Music Director = Saluri Rajeshwara Rao
| Release Date =  June 16, 1964
| runtime        = 156 minutes
| country        = India
| language       = Telugu
}}
 Savitri and Jamuna directed by B. N. Reddy.

==Plot==
Madhu also called Chinnababu (Akkineni Nageswara Rao) a zamindars son is very gullible who every one laughs at.  One day he meets a girl Vasanthi (Jamuna) who stays in a portion of his house.  Vasanthi becomes very friendly with Madhu and behaves very close to him as she sees Madhu just as a brother and nothing else where as in Madhu mistakens Vasanthis friendly nature to love.  One day Madhu has to leave to Chennai for higher studies.  He writes letters but he never get any reply from Vasanthi to any of his letters.  When he comes back he finds out Vasanthi and her family left the city without telling anybody. Madhu finds a letter from which he realizes that Vasanthi only saw him as a brother.  He gets very depressed and falls sick.  Seetha (Savitri), Ramakrishnaiah (Gummadi)s daughter takes care of Madhu by taking care of his health and giving him company. The whole story is how he recovers from the depression he faces because of Vasanthi.

==Cast==
{| class="wikitable"
|-
! Actor/Actress !! Character
|-
| Akkineni Nageswara Rao || Madhu
|- Jamuna || Vaasanthi
|- Savitri || Seetha
|- Gummadi || Ramakrishnaiah
|-
| Kongara Jaggaiah || Sriram
|-
| Ramana Reddy || Govindaiaah
|-
| Relangi Venkataramaiah || GovindaRao
|-
| Bommireddy Narasimha Reddy
|- Rajasree ||
-
|}

==Soundtrack==
There are 9 songs in the film. 
* "Andhenaa Ee Chethula"
:*Lyrics: Kosaraju
:*Singers: P Susheela
* "Endhu Daagi Unnavo"
:*Lyrics: C Narayana Reddy
:*Singers: P Susheela
* "Madhanaa Manasaayeraa"
:*Lyrics: C Narayana Reddy
:*Singers: S Janaki
* "Neruthuno Ledho"
:*Lyrics: C Narayana Reddy
:*Singers: P. Susheela
* "Ninna leni andamedo"
:*Lyrics: C Narayana Reddy
:*Singers: Ghantasala Venkateswara Rao
* "O Basthi Doragaaru"
:*Lyrics: Kosaraju
:*Singers: B Vasantha and Basaveshwar
* "Pagale Vennela Jagame Vooyala"
:*Lyrics: C Narayana Reddy
:*Singers: S Janaki
* "Siva Dheekshaa"
:*Lyrics: Ghanam seenayya
:*Singers: S Janaki
* "Vasthaavu Pothaavu"
:*Lyrics: C Narayana Reddy
:*Singers: B Vasantha

== References ==
 

==External links==
*  
*  

 
 
 
 


 