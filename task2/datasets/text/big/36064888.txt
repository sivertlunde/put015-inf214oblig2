Kounty Fair
 
{{Infobox Hollywood cartoon|
| cartoon_name = Kounty Fair
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz
| story_artist =  Bill Nolan Tom Palmer
| voice_actor = 
| musician = David Broekman
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = January 6, 1930
| color_process = B&W
| runtime = 6:51 English
| preceded_by = Ozzie of the Circus
| followed_by = Chilly Con Carmen
}}

Kounty Fair is a 1930 animated cartoon released by Universal Pictures starring Oswald the Lucky Rabbit.

==Storyline==
Oswald is merrily riding on a pony, heading towards the residence of his buddy Julius Cat. Julius is doing some chores but finishes the moment the rabbit arrives. Following a short greeting, the two friends head for a little trip to the fair.

At the fair grounds, Oswald and Julius are intrigued by the numerous games and services being offered. The cat and the rabbit first come to a hotdog stand for something eat. While the two are waiting for their order, something suddenly catches Oswalds attention. In this, Oswald quickly leaves, pulling along Julius who takes a hotdog without paying, much to the vendors disgust.

What interests Oswald is a tap dancing contest. Oswald and Julius consider giving it a try as they are both skilled in dancing. In no time, the vendor comes to get back at them for not paying previously.

But instead of tormenting Oswald and Julius, the vendor also finds interest in the dancing event and decides to perform as he goes first. The vendor dances pretty well and goes on to pick up the pace. As he speeds up further, however, the bottom of his garments somehow catches fire, prompting him to run and sit in a barrel of water.

Finally it is Oswald and Julius turn to take center stage. When they begin, their moves are fluent, going according to plan, and they never mess up. They go on to win the contest and the coveted trophy.

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 