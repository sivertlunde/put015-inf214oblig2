Thapaswini
{{Infobox film 
| name           = Thapaswini
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = PIM Kasim
| writer         = Jagathy NK Achari
| screenplay     = Jagathy NK Achari
| starring       = Prem Nazir Sheela Adoor Bhasi Jose Prakash
| music          = G. Devarajan
| cinematography = Dathu
| editing        = VP Krishnan
| studio         = Sony Pictures
| distributor    = Sony Pictures
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by PIM Kasim. The film stars Prem Nazir, Sheela, Adoor Bhasi and Jose Prakash in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Sheela
*Adoor Bhasi
*Jose Prakash Prema
*Sankaradi Raghavan
*T. S. Muthaiah
*K. P. Ummer
*Philomina Sujatha
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambadi Kuyilkkunje || P Susheela, P. Madhuri || Vayalar Ramavarma || 
|-
| 2 || Kadalinu Thee Pidikkunnu || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Puthrakameshti || P. Madhuri || Vayalar Ramavarma || 
|-
| 4 || Sarppasundaree Swapnasundaree || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 