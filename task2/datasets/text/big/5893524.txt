The Warrior and the Sorceress
 
{{Infobox film
| name            = The Warrior and the Sorceress
| image           = Warrior and the sorceressposter.jpg
| caption         = Theatrical release film poster John C. Broderick
| art_direction   = Emilio Basaldua
| producer        = John C. Broderick Frank K. Isaac Héctor Olivera Alejandro Sessa Roger Corman (executive producer&nbsp;– uncredited)
| screenplay      = John C. Broderick (screenplay/story) William Stout (story)
| based on        =  
| starring = {{plainlist|
* David Carradine
* Luke Askew
* María Socas
* Anthony De Longis
* Harry Townes
}}
| music           = Louis Saunders
| cinematography  = Leonardo Rodríguez Solís
| editing         = Silvia Ripoll
| costume design  = María Julia Bertotto
| special effects = Ricardo Lanzoni New Horizon Picture Corp
| distributor     = New Horizons
| released        =   (USA)
| runtime         = 81 minutes
| country         = Argentina United States
| language        = English
| budget          = $600,000 - $4,000,000  (Estimated)
| gross           = $2,886,225 (USA)  
}} fantasy action John C. Broderick and starring David Carradine, María Socas and Luke Askew. It was written by Broderick (story and screenplay) and William Stout (story).
 Kurosawa film cult classic. 

== Synopsis == sorceress that has been taken captive by Zeg changes Kains purpose to take the well for himself, instead choosing to save Naja and the village people. Kain starts entangling the situation, and by taking advantage of the ongoing feud, he seeks to debilitate the rival warlords and defeat them.

== Cast ==
* David Carradine ... Kain the Warrior
* María Socas ... Naja the Sorceress
* Luke Askew ... Zeg the Tyrant
* Anthony De Longis ... Kief, Zegs Captain (as Anthony DeLongis)
* Harry Townes ... Bludge the Prelate
* Guillermo Marín ... Bal Caz (as William Marin)
* Armando Capo ... Burgo the Slaver (as Arthur Clark)
* Daniel March ... Blather, Bal Cazs Fool
* John Overby ... Gabble, Bal Cazs Fool
* Richard Paley ... Scar-face
* Marcos Woinski ... Burgos Captain (as Mark Welles) 
* Cecilia Narova ... Exotic Dancer (as Cecilia North)
* Dylan Willias ... Zegs Guard
* José Casanova ... Zegs Guard (as Joe Cass)
* Miguel Zavaleta ... Zegs Guard (as Michael Zane)
* Herman Cass ... Zegs Guard
* Arturo Noal ... Zegs Guard (as Arthur Neal)
* Hernán Gené ... Zegs Guard (as Herman Gere)
* Gus Parker ... Zegs Guard
* Ned Ivers ... Slave
* Liliana Cameroni ... Zegs Drowned Slave (as Lillian Cameron)
* Eva Adanaylo ... Woman at Well (as Eve Adams)
* Noëlle Balfour ... (uncredited)

== Production == San Juan, also known as Valle de la Luna ("Valley of the Moon", due to its otherworldly appearance). Most of the film was shot inside Estudios Baires Film S.A. and Campo de Mayo, in Buenos Aires Province. Cinematófilos,   Cinematófilos.com.ar   

Before production started and during a discussion with his girlfriend, David Carradine punched a wall and fractured his right hand. To conceal the plaster for the hand, Carradine used a pointed black glove on his right arm during all the shooting. 

The outfit that Carradine uses for his character of Kain is the same he wore for the B movie/post-apocalyptic|post-apocalyptic action film Dune Warriors (1991).  In a rather obvious coincidence, Luke Askew again played the antagonist role in the latter film. 

== Similarities with Yojimbo ==
According to David Carradines book Spirit of Shaolin, it was clear before production started that the film was going to be a version of Akira Kurosawas 1961 Samurai film Yojimbo (film)|Yojimbo, and Carradine talked about it with executive producer Roger Corman:

{| class="rquote float" style="border-collapse:collapse; border-style:none; float:; margin:0.5em 0.75em; width:50%;"
|- style="text-align:left; vertical-align:top;"
| style="color:#b2b7f2; font-size:3.3em; font-family:Times New Roman,serif; font-weight:bold; padding:4px 2px 2px; width:0.5em;"| “
| style="padding:0 10px;"|  It (The Warrior and the Sorceress) was essentially a remake of Yojimbo, the samurai movie by the great Japanese director, Akira Kurosawa. I called up Roger and told him I loved the script; but what about the Yojimbo factor. Roger said, "Yes, it is rather like Yojimbo."
I said, "Its not like Yojimbo. It is Yojimbo."
Roger said, "Let me tell you a story. When Fistful of Dollars opened in Tokyo, Kurosawas friends called him up and said You must see this picture.
Kurosawa said, Yes, I understand it is rather like Yojimbo.

-No, its not like Yojimbo; it is Yojimbo. You have to sue these people.

-I cant sue them, he responded.

-Why not?

-Because -Kurosawa confessed-, Yojimbo is Dashiel Hammets Red Harvest."
I went for it.   
| style="color:#b2b7f2; font-size:3.3em; font-family:Times New Roman,serif; font-weight:bold; padding:4px 2px 2px; text-align:right; vertical-align:bottom; width:0.5em;"| ”
|-
| class="rquotecite" colspan="3" style="padding-top:10px;"|
|}

The story however appears to be apocryphal, as Kurosawa and Toho Studios did in fact successfully sue Sergio Leone.  

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 