Rumble in the Bronx
 
 
{{Infobox film
| name           = Rumble in the Bronx
| image          = Rumble-in-the-Bronx-poster.jpg
| caption        = Rumble in the Bronx Hong Kong theatrical poster
| director       = Stanley Tong
| producer       = Barbie Tung Roberta Chow Raymond Chow Leonard Ho
| writer         = Edward Tang Fibe Ma
| starring       = Jackie Chan Anita Mui Françoise Yip Marc Akerstream
| music          = Nathan Wong J. Peter Robinson
| cinematography = Jingle Ma
| editing        = Peter Cheung Golden Harvest
| runtime        = 106 minutes 87 minutes (US version)
| released       =   (Hong Kong)   (US) 
| country        = Hong Kong English 
| budget         = USD|US$ 7.5 million
| gross          = Hong Kong dollar|HK$56,911,136 (HK) USD|US$ 32,392,047 (U.S.)
}}
 1995 Cinema Hong Kong martial arts action comedy Bronx area of New York City but was filmed in and around Vancouver. 

==Plot==
 
Ma Hon Keung (Jackie Chan), a Hong Kong cop comes to New York to attend the wedding of his Uncle Bill (Bill Tung) who introduces his new, African-American wife Whitney to him down at his supermarket, which he owns and has sold to Elaine (Anita Mui).  Uncle Bills friend, Steven Lo (Jamie Luk), loans him a vintage automobile for the wedding. Later that night, a street gang starts a motorcycle race near Uncle Bills apartment. They are about to run over his friends car until Keung jumps down and stops them. He soon starts a rivalry with the street gang when he drives them away from the supermarket that they were robbing and vandalizing. This starts a series of brawls in which the bikies try to corner Keung and finish him off. When a member of the street gang named Angelo (Garvin Cross) gets involved in an illegal diamond deal gone bad and steals the diamonds, the small-time gangsters become the victims of a much larger and more effective criminal syndicate led by White Tiger (Kris Lord). While running away with the diamonds, Angelo leaves them in a cushion, which is unknowingly used by Keung for the wheelchair of a disabled Chinese American boy named Danny (Morgan Lam), who is raised by his elder sister Nancy (Françoise Yip), a lingerie model/dancer who works in a seedy bar and is an associate/girlfriend of the bikies. Keung befriends Nancy and advises her to stay away from crime. When the gangsters see this, they chase Keung and Nancy. After failing to confront Keung, the bikies turn up at Elaines supermarket and start vandalizing it, during which two of Angelos men are captured by White Tigers men, who turn up at the supermarket in search of Angelo. Angelos colleagues are unaware of his diamond heist and one is executed in a tree-shredder; his remains are given back to the other gangster to show to his friends as a warning to return the multi-million dollar goods. In the meantime, Keung and Nancy go to bikies shed after the latest supermarket attack, and Keung defeats them in another brawl, at which point the shredded remains are brought back.

Keung agrees to help the bikies leader, Tony (Marc Akerstream). Keung convinces the street gangsters to reform, then brings the big-time criminals to justice after another long-winded street battle. The syndicate and Keung work out the diamonds are in the boys wheelchair, and the handover is botched after Nancy and Tony are held hostage by the syndicate; the diamonds are lost after the syndicate uses towtrucks to pull the supermarket apart and the diamonds are spilled as Keung is in the building and knocked over. A long battle occurs in the Hudson River after White Tigers men hijack a hovercraft and are pursued by Keung and the New York Police Department. The hovercraft finally ends up running through the streets, causing much damage to property. Keung ends the chase by stealing a large sword from a museum and clamping it onto a sports car window and driving into the hovercraft, shredding the rubber skirt and immobilising the vehicle and capturing the syndicate men. After shooting one of them non-fatally to force them to reveal White Tigers location, Keung drives the hovercraft, with the skirt crudely repaired with duct tape, across town to a golf course where White Tiger is playing with subordinates. He runs them over and squashes them non-fatally into the ground. The film ends with White Tiger being squashed, his clothes ripped off his back, leaving him naked.

==Cast==
* Jackie Chan as Ma Hon Keung (T: 馬漢強, S: 马汉强, P: Mǎ Hànqiáng)
* Anita Mui as Elaine
* Françoise Yip as Nancy
* Bill Tung as Uncle Bill Ma (T: 馬 驃, S: 马 骠, J: maa5 piu3, P: Mǎ Piào)
* Marc Akerstream as Tony
* Garvin Cross as Angelo
* Morgan Lam as Danny
* Kris Lord as White Tiger
* Carrie Cain Sparks as Whitney Ma
* Elliot Ngok (Yueh Hua) as Wah, the Realtor (T: 華, S: 华) (uncredited)
* Eddy Ko as Prospective market buyer Emil Chau as Ice cream salesman
* Alex To as Ice cream customer
* Richard Faraci as a syndicate gangster with a ponytail
* Jamie Luk as Steven Lo
* Ailen Sit as Tonys Gang Member
* Chan Man Ching as Tonys Gang Member
* Rocky Lai (extra)/(stunts) Mars (stunt) (uncredited)
* Colin Flora (stunt) (uncredited)

==Box office==
In Hong Kong, Rumble in the Bronx broke the box office record earning HK $56,911,136 making it the biggest film in Hong Kong at that time   and one of Chans biggest ever.

It was also Chans North American breakthrough. Opening on 1,736 North American screens, it was number one at the box office in its opening weekend, grossing US $9,858,380 ($5,678 per screen). It finished its North American run with US $32,392,047.

==Awards and nominations== 1996 Hong Kong Film Awards
** Winner: Best Action Choreography (Jackie Chan, Stanley Tong)
**Nomination: Best Actor (Jackie Chan)
**Nomination: Best Actress (Anita Mui)
**Nomination: Best Film Editing (Peter Cheung)
**Nomination: Best New Performer (Françoise Yip)
**Nomination: Best Picture (Barbie Tang)
**Nomination: Best Supporting Actress (Françoise Yip)

*1997 Key Art Awards
**Winner: Best of Show – Audiovisual
For the "Ben Knows" comedy TV spot

*1996 MTV Movie Awards
**Nomination: Best Fight (Jackie Chan)

==Filming and production==
 

In  his autobiography, I am Jackie Chan: My life in Action, Jackie Chan talked about the initial difficulty of filming a movie set in New York in Vancouver. The production team initially had to put up fake graffiti during the day and take it all down during the evening, while simultaneously making sure that no mountains made it into the background. However, Chan decided that it was best that the production team focus on the action only without worrying too much about scenery. Viewers have noted mountains in the background, which doesnt exist in the NYC landscape.

The original spoken dialogue consisted of all of the actors speaking their native language most of the time. In the completely undubbed soundtrack, available on the Warner Japanese R2 DVD release, Jackie Chan actually speaks his native Cantonese while Françoise Yip and Morgan Lam (the actors playing Nancy and Danny) speak English. All of the original dialogue was intended to be dubbed over in the international and Hong Kong film markets, and New Line cinema overdubbed and slightly changed the original English dialogue.

During filming, Chan injured his right leg while performing a stunt. He spent much of the remaining shooting time with one leg in a cast. When it came to the films climax, the crew colored a sock to resemble the shoe on his good foot, which Chan wore over his cast. His foot still had not completely healed when he went on to shoot his next film, Thunderbolt (1995 film)|Thunderbolt (filmed the same year but released earlier).   

The lead actress and several stunt doubles were also injured during the shooting of a motorcycle stunt, with several people suffering broken limbs and ankles.

==New Line Cinema edit==
New Line Cinema acquired the film for international distribution and commissioned a new music score and English dub (with participation from Jackie Chan). A scene of Keungs airplane flying to New York was added to the opening credits. Two scenes added exclusively for the international version are Keung and Nancy escaping from the nightclub after the bikers spot them together, and White Tiger taking a golf shot before a subordinate approaches him with his phone. Neither of these scenes were in the original Hong Kong release.

In comparison to the Hong Kong version, 17 minutes of cuts were made, including:

* Some more footage of Keung and Uncle Bill in the car talking about the supermarket.
* A scene in which two biker gang members extort some money while Uncle Bill is showing his supermarket to Elaine, and then steal some items from beside the cash register.
* Keung does more comical antics in front of the one-way mirror in Uncle Bills office.
* The motorcycle race and climactic fight between Keung and the gang in their house are trimmed slightly.
* A scene where Uncle Bill and his wife sing a lament in Chinese at their wedding. This scene is restored in the US cable version.
* Keung does more exercises before he rolls into a headstand.
* Keung spots more shoplifting by the gangs Cantonese-speaking member from the office with the one-way mirror.
* A lecture by Keung on martial arts after fighting off the biker gang the first time.
* An entire scene in the morning after the night of the alley ambush by the bikers where Keung arrives at the supermarket to find it has been robbed by some youths. Simultaneously, the two gangsters from the beginning of the film return to extort money, only to be scared off by Keung. Immediately afterward, the entire biker gang shows up and Keung tries to intimidate them, but eventually calls the police. This scene is restored in the US cable version. This scene also explains why the motorcycle gang chases Keung to the parking garage.
* On the parking garage roof top, Keung begs for a womans help, but inadvertently scares her away.
* More angles of the parking garage jump are shown.
* The long haired syndicate thug asking Keung if he has seen any diamonds.
* Two policemen waiting for Angelo in their car as he attempts to retrieve the diamonds.
* A full scene after one of Angelos men is murdered by a syndicate using a tree-shredder, where Keung and Nancy return to the apartment with shopping. A displeased Elaine emerges and describes what happened to the store.
* Slightly more footage of Keung trying to bargain about the diamonds with the gang in the boathouse.
 Kung Fu" by the band Ash (band)|Ash, the lyrics of which mention Jackie Chan, as well as other Asian figures and characters ubiquitous in the west.

==Media==
The majority of DVD versions of the film contain the heavily edited US New Line Cinema cut, with the relevant dubs created for each market. However, other versions exist, which are closer to the original theatrical release.

===Warner===
* A DVD was produced by Warner Brothers HK for Hong Kong and South Korea. This contains the New Line Cinema version with additional abridged Cantonese and Mandarin soundtracks. It has an aspect ratio of 2.35:1, but includes no English subtitles.

* Warner Home Video also released a DVD in Japan of the Hong Kong version. This version contains the Hong Kong cut of the film. The dialogue is completely undubbed in a mono 2.0. However, its aspect ratio is cropped to 1.85:1 and contains no English subtitles.

* In Hong Kong, a VCD containing the Hong Kong version in Cantonese, with newly generated English and Chinese subtitles was also released. Its 2.35:1.

===Thakral/Chinastar===
It appears that a joint-distribution deal was made, with Thakral releasing the film in China, and Chinastar releasing it in Hong Kong. This version contains no credits, not even the film title,  but is otherwise the Hong Kong version. There are no English subtitles and the ratio is roughly 2.10:1.

===Speedy===
Malaysian distributor Speedy released a VCD featuring the Cantonese/English soundtrack. The subtitles are in three languages – English, Chinese and Malay. In comparison to the Hong Kong version, it cuts footage of strong language and offensive gestures. Unlike the Hong Kong release, during a scene in which Angelo insults Keung in the car-park, he keeps his trousers up. For some dialogue scenes, it actually dubs the normally English-responding characters into Cantonese. Although the correct ratio is 2:35:1, it is distorted into roughly 1:60:1.

===Funny===
The film had three separate DVD releases by Taiwanese distributor Funny. Two of these DVDs feature the Taiwanese Mandarin-dubbed version with embedded subtitles. One of these contains a Dolby 5.1 soundtrack only, whilst the other contains both Dolby and DTS soundtracks. The third release is a double-sided disc, featuring the Taiwanese Mandarin dub on one side and the English-dubbed New Line Cinema version on the other. Despite containing a dubbed soundtrack, these DVDs are the only releases to contain English subtitles for a Chinese version. All three are presented in 2.35:1.

===4 Film Favorites===
*Another DVD was released as part of the 4 Film Favorites: Martial Arts collection. The film is exactly like the New Line Cinema but put on the other side of the disc. The film is also attached to The Corruptor, Showdown in Little Tokyo, and Bloodsport (film)|Bloodsport.

==Critical reception==
When released in North America, Rumble in the Bronx received generally positive reviews, as most critics were happy that a Jackie Chan film was finally getting a wide theatrical release in North America.    The film currently has a 79% approval rating on Rotten Tomatoes.  Most critics agreed that the plot and acting were lacking, but the action, stunts, and Chans charm made up for it.

  – and, like Astaire and Rogers, he does what he does better than anybody. There is a physical confidence, a grace, an elegance to the way he moves. There is humor to the choreography of the fights (which are never too gruesome).

Hes having fun. If we allow ourselves to get in the right frame of mind, so are we.  

==See also==
 
* Jackie Chan filmography
* List of Hong Kong films
 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 