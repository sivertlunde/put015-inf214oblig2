The Wonderful Story (1922 film)
{{Infobox film
| name           = The Wonderful Story 
| image          =
| caption        =
| director       = Graham Cutts
| producer       = Herbert Wilcox
| writer         = I.A.R. Wylie (novel)   Patrick L. Mannock
| starring       = Lillian Hall-Davis Herbert Langley Olaf Hytten
| music          =
| cinematography = 
| editing        = 
| studio         = Graham-Wilcox Productions
| distributor    = Astra-National
| released       = 1922
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British drama film directed by Graham Cutts and starring Lillian Hall-Davis, Herbert Langley and Olaf Hytten.  The fiancee of a farmer falls in love with his brother. It was based on the novel The Wonderful Story by I.A.R. Wylie. The producer Herbert Wilcox chose the story because he believed as it was homely and had few characters it would be suited for the cinema.  The film was made on  budget of £1,400 and Wilcox sold the rights to it for £4,000. Despite being acclaimed by the critics it was considered a failure at the box office. 

==Cast==
* Lillian Hall-Davis – Kate Richards
* Herbert Langley – Robert Martin
* Olaf Hytten – Jimmy Martin Bernard Vaughan – Vicar

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 


 