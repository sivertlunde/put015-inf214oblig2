Pacific Blackout
{{Infobox film
| name           = Pacific Blackout
| image          =
| caption        =
| director       = Ralph Murphy
| producer       = 
| writer         = Franz Schulz and Curt Siodmak (story)
| starring       = 
| music          = 
| cinematography = Theodor Sparkuhl
| editing        = Thomas Scott
| distributor    = 
| released       = 31 December 1941
| runtime        = 76 min.
| country        = United States
| language       = English 
| budget         =
}}

Pacific Blackout is a 1941 American film.

Inventor and engineer Robert Draper is unjustly found guilty for the murder of his partner.  Just as hes sent to prison, the prison truck crashes in the midst of a civil defense blackout, propelling him into a search for the real killers who framed him.  Czech-American screenwriter Franz Schulz was billed as Francis Spencer for the film.

== Cast ==
 Robert Preston as Robert Draper
* Martha ODriscoll as Mary Jones
* Philip Merivale as John Runnell
* Eva Gabor as Marie Duval
* Louis Jean Heydt as Harold Kermin
* Thurston Hall as Williams (Civil Defense Official)
* Mary Treen as Irene 
* J. Edward Bromberg as Pickpocket
* Spencer Charters as Cornelius 
* Cy Kendall as Hotel Clerk Russell Hicks as Commanding Officer
* Paul Stanton as Judge
* Clem Bevans as Night-watchman
* Robert Emmett Keane as Defense Attorney
* Edwin Maxwell as District attorney Rod Cameron as Pilot
* unbilled players include Monte Blue, Wade Boteler, Ralph Dunn, Bess Flowers, Jack Norton, Betty Farrington and Lee Shumway

== External links ==

*  
*  
 
 

 
 
 
 
 

 