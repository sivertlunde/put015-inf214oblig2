Panic in the House of Ardon
{{Infobox film
| name           = Panic in the House of Ardon
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = Ernst Reicher
| writer         = Robert Wiene
| narrator       = 
| starring       = Stella Harf   Max Kronert   Paul Mederow   Georg H. Schnell
| music          = 
| editing        = 
| cinematography = 
| studio         = Stuart Webbs-Film Company
| distributor    = 
| released       =  
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
}} German silent silent crime Expressionist style that had been used for Wienes earlier hit The Cabinet of Dr. Caligari. It features the popular detective Stuart Webbs, closely modeled on Sherlock Holmes.

==Cast==
* Stella Harf   
* Max Kronert   
* Paul Mederow   
* Georg H. Schnell   
* Ernst Stahl-Nachbaur   
* Kurt von Wangenheim

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 


 
 