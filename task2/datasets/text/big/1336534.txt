Warlock (1959 film)
{{Infobox film
| name           = Warlock
| image          = Warlock 1959.jpg
| caption        = 1959 movie poster
| director       = Edward Dmytryk
| producer       = Edward Dmytryk
| writer         = Robert Alan Aurthur based on the novel by Oakley Hall
| starring       = Richard Widmark Henry Fonda Anthony Quinn Dorothy Malone Dolores Michaels
| music          = Leigh Harline
| cinematography = Joseph MacDonald
| editing        = Jack W. Holmes
| distributor    = 20th Century Fox
| released       =  
| runtime        = 122 minutes
| country        = United States
| awards         = English
| budget         = $2.4 million 
| gross = $1.7 million (est. US/ Canada rentals) 
}}
 Western adapted the novel by Oakley Hall (screenplay written by Robert Alan Aurthur). Directed by Edward Dmytryk, it stars Richard Widmark, Henry Fonda, and Anthony Quinn.
 Star Trek, Frank Gorshin, known for his role as the Riddler on the television series Batman (TV series)|Batman, and Tom Drake, probably best known as John Truett, "The Boy Next Door", in Meet Me In St. Louis.

==Plot==
Warlock is a small Utah mining town of the early 1880s. A group of local cowboys led by Abe McQuown (Tom Drake) often come into town to shoot the place up, kill on just a whim, and beat up and humiliate any lawman who tries to stand up to them.

The Citizens Committee decides to hire Clay Blaisedell (Henry Fonda), a renowned gunfighter, as town marshal in spite of the misgivings of some, such as old Judge Holloway (Wallace Ford) who insists that the situation should be handled within the law (though he admits that a loophole prevents it from being done effectively). Blaisedell is famous for his golden-handled guns.
 saloon and rename it the "French Palace" (something they appear to have done in previous towns, since they bring the signboard with them).

Their first encounter with McQuowns men is without bloodshed, though the cowboys are humiliated and one of them, Johnny Gannon (Richard Widmark), stays behind. He has been put off by the gangs activities for some time now and resolves to be more law-abiding.

Some time later Morgan learns that his old flame, Lily Dollar (Dorothy Malone), is coming to town on the stagecoach, and she is accompanied by Bob Nicholson (Sol Gorss), brother of Big Ben Nicholson, who was recently killed by Blaisedell. Lily had left Morgan for Big Ben and knows that Morgan pushed Ben into challenging Blaisedell, who killed him as a result.

Morgan sets out to meet the stagecoach which is being ambushed by some of McQuowns men as he watches from a distance. He takes advantage of the situation to kill Bob Nicholson unseen. Lily arrives in town and sees Morgan there. She believes that he pulled the trigger, although this is based on intuition rather than evidence.

The robbers are arrested, sent to Bright City for trial, and subsequently cleared by a jury intimidated by McQuown. In the meantime, the Bright City sheriff, who disapproves of Blaisedell, appoints Johnny Gannon as official deputy. Although he was once a thug himself, Gannon takes his law enforcement duties seriously.

The scene is set for a number of confrontations between Gannon, Morgan, Blaisedell, and the McQuown gang.

Wanting revenge for their arrests, the released robbers, led by Johnny Gannons brother Billy (Frank Gorshin), go to Warlock to confront Blaisedell and Morgan. The deputy urgently asks them to leave, and tells Billy, "I aint backin him, because youre my brother, and I aint backin you, because youre wrong." The outlaws proceed to provoke Blaisedell, and he kills two, including Billy. Soon after, the remaining outlaws try to give themselves legitimacy by posting notices declaring themselves "regulators," mocking Blaisedells quasi-legal status as hired marshal in a town where a sheriff and deputies theoretically enforce the law.

What is more, some of the inhabitants are getting tired of Blaisedell and Morgan, something which, based on previous experience, the pair had predicted would happen. Blaisedell, however, has started a relationship with local girl Jessie Marlow (Dolores Michaels) and decides to marry and settle down, much to the surprise of Morgan, who wants to move on to another town and insists that Clay is nothing without him.

Gannon, with some assistance from townsfolk, unexpectedly manages to break up the gang without the help of Blaisedell, who was kept away from the fight at gunpoint by Morgan. It appears that Warlock has outgrown its need for the two gunfighters, but Morgan cannot tolerate the idea that Gannon is now more of a hero than Blaisedell. In the course of an argument, Blaisedell learns the truth about the deaths of the Nicholson brothers, and turns his back on Morgan, who resorts to drinking.

That evening, in a drunken state, Morgan shoots up the town and calls for Gannon, but the deputy is locked into his own cell by Blaisedell who insists that "Tom Morgans my responsibility". Morgan has insisted that he was always covering Blaisedells back in almost all their confrontations with their enemies, and that he is the better gunman. To prove his point he shoots off Blaisedells hat, and the other, on instinct, shoots back, killing Morgan. Satisfied that Blaisedell is a hero again, Morgans dying words are "I won, Clay, I won!"

The grief-stricken Blaisedell carries his friends body into the saloon which, to the sound of thunder in the sky, he burns down, giving Morgan a "Viking funeral". The humiliated Gannon tells Blaisedell that he will arrest him in the morning if he does not leave town.

After what has happened, Blaisedell cant face staying in Warlock and decides to leave anyway. Jessie, however, wont accompany him. She insists that she is no Tom Morgan. Lily Dollar, for her part, looks as if she takes little satisfaction in finally getting her revenge. She has in the meantime taken up with Gannon.

The next day Gannon and Blaisedell face one another, the latter wearing his famous golden-handled guns. Blaisedell outdraws Gannon, but then throws his guns into the sand, smiles at Gannon, mounts his horse, and leaves town.

==Cast==
* Richard Widmark as Johnny Gannon
* Henry Fonda as Clay Blaisedell
* Anthony Quinn as Tom Morgan
* Dorothy Malone as Lily Dollar
* Dolores Michaels as Jessie Marlow
* Wallace Ford as Judge Holloway
* Tom Drake as Abe McQuown
* Richard Arlen as Bacon
* DeForest Kelley as Curley Burne
* Regis Toomey as Skinner Vaughn Taylor as Henry Richardson
* Don Beddoe as Dr. Wagner
* Whit Bissell as Petrix
* Bartlett Robinson as Buck Slavin
* Frank Gorshin as Billy Gannon
* June Blair as Dance hall girl Robert Adler as Foss (uncredited)
* Joel Ashley as Murch (uncredited)
* Don Red Barry as Edward Calhoun (uncredited)
* Wally Campo as Barber (uncredited) Harry Carter as Bartender (uncredited)
* Paul Comi as Luke Friendly (uncredited)
* Walter Coy as Deputy Sheriff Roy Tompson (uncredited)
* Sheryl Deauville as Dance hall girl (uncredited)
* Ann Doran as Mrs. Richardson (uncredited) David Garcia as George Pony Benner (uncredited)
* Sol Gorss as Bob Nicholson (uncredited)
* J. Anthony Hughes as Shaw (uncredited)
* Roy Jenson as Hasty (uncredited)
* L.Q. Jones as Fen Jiggs (uncredited)
* Stan Kamber as Hutchinson (uncredited)
* Gary Lockwood as Gang member (uncredited) Ian MacDonald as MacDonald (uncredited)
* Robert Osterloh as Professor (uncredited)
* James Philbrook as Cade (uncredited)
* Hugh Sanders as Sheriff Keller (uncredited)
* Roy N. Sickner as Bush (uncredited)
* Mickey Simpson as Fitzsimmons (uncredited)
* Bert Stevens as Townsman (uncredited)
* Joe Turkel as Chet Haggin (uncredited) Tom Wilson as Townsman (uncredited) Harry Worth as (uncredited)
* Henry Worth as Burbage (uncredited)

==Inspiration== Kate Fisher.
 Dodge City, Tombstone, Arizona|Tombstone) who took up law enforcement for short periods of time. He would clean the place up in an often dubious way and earned far more than the average lawmans pay. Some critics have likened his methods to "Protection racket|protection". Pictorial History of the Wild West by James D. Horan and Paul Sann ISBN 0-600-03103-9, ISBN 978-0-600-03103-1 

*While Morgan walks with a limp, Doc Holliday suffered from tuberculosis, but that did not make him any less a gunman or a killer. He was also a heavy drinker and notorious gambler. He was very close to Earp, and Bat Masterson, who knew them well, is quoted as saying that "Doc idolized him". 

*The name Lily Dollar implies that it is a pseudonym with obvious implications, and indeed it is revealed that she "worked" whenever Morgan was short of money. "Kate Fisher" was also an assumed name, and she was also allegedly a prostitute. Blaisedell tells how Lily once set fire to a house to help him and Morgan escape some enemies; Fisher is said to have done the same thing for Earp and Holliday.  On the other hand, there does not seem to be a record of Fisher eventually turning against her two friends.

==Alternative titles==
*Lhomme aux colts dor -  ) Italian title - (translation: Last Night in Warlock) Spanish title - (translation: The Man with the Golden Guns)

==See also==
* List of American films of 1959

==References==
 

==External links==
*  
*  
*  
*   Review of the film & DVD by Erik Rupp at  
*   Review by Eamonn McCusker at  

 

 
 
 
 
 
 
 
 
 
 