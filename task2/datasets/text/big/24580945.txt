Wicked (1931 film)
{{Infobox film
| name           = Wicked
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Allan Dwan
| producer       = 
| writer         = Kenyon Nicholson Kathryn Scola
| narrator       = 
| starring       = Elissa Landi Victor McLaglen Una Merkel
| music          = R.H. Bassett
| cinematography = J. Peverell Marley Jack Dennis
| studio         = Fox Film Corporation
| distributor    = Fox Film Corporation
| released       =    	
| runtime        = 57 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Wicked (1931 in film|1931) is an American black and white prison melodrama musical film, the story of woman who commits murder while trying to save her bandit husband and bears a child in prison.  The movie is also known as Condannata in Italy and Malvada in Spain. The production dates were from early June to early July 1931.

==Cast==
*Elissa Landi - Margot Rande
*Victor McLaglen - Scott Burrows
*Una Merkel - June
*Irene Rich - Mrs. Luther
*Alan Dinehart - Blake
*Theodore von Eltz - Rony Rande
*Oscar Apfel - Judge Luther
*Mae Busch - Arlene
*Ruth Donnelly - Fanny
*Eileen Percy - Stella
*Joseph W. Reilly - 	
*Kathleen Kerrigan - Miss Peck
*Lucille Williams - Prisoner
*Alice Lake - Prisoner
*William Pawley - Cop
*G. Pat Collins - Cop
*Jack Grey - Cop Clarence Wilson - Juryman
*Edwin Maxwell - Owner of property
*George Kuwa - Tonys friend
*Lloyd Whitlock - Tonys Friend
*Jackie Lyn Dufton - Tonia (as Jacquie Lynn)
*Edmund Breese -

==References==
 

==Further reading==
* Hall, Mordaunt. The Screen; A Mother in Prison. In: The New York Times: Amusements, Books Section, 19 September 1931, Pg. 10, (NP)

==External links==
*  
* http://www.ovguide.com/movies_tv/wicked_1931.htm
* http://www.tcm.com/tcmdb/title.jsp?stid=95772
* http://www.reelzchannel.com/movie/205310/wicked
* http://www.filmsuggest.com/movies/1269791/wicked-1931
* http://www.reelzchannel.com/movie/205310/wicked/comments

 

 
 
 
 
 

 