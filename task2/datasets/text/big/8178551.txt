Abbott and Costello in the Foreign Legion
{{Infobox film
| name           = Abbott and Costello in the Foreign Legion
| image          = File:Film_Poster_for_Abbott_and_Costello_in_the_Foreign_Legion.jpg
| caption        = Theatrical release poster
| director       = Charles Lamont Robert Arthur John Grant Leonard Stern
| starring       = Bud Abbott Lou Costello Patricia Medina Walter Slezak Jeff Chandler
| music          = Joseph Gershenson
| cinematography =
| editing        = Frank Gross
| distributor    = Universal-International
| released       =  
| runtime        = 80 min.
| country        = United States
| language       = English
| budget         = $679,687 
}}
Abbott and Costello in the Foreign Legion is a 1950 film starring the comedy team of Abbott and Costello.

==Plot==
Bud Jones (Bud Abbott) and Lou Hotchkiss (Lou Costello) are wrestling promoters.  Their star, Abdullah (Wee Willie Davis), no longer wishes to follow the script for their crooked matches, especially since he is supposed to lose his next match. Abdullah leaves America to return to his homeland, French Algeria|Algeria.  The promoters financiers, a syndicate that has lent them $5,000 to bring Abdullah to the States, are now requiring them to return the money or face the consequences. The two men follow Abdullah to Algeria in hopes of bringing him back.

Meanwhile, Abdullahs cousin, Sheik Hamud El Khalid (Douglass Dumbrille) and a crooked Foreign Legionnaire, Sergeant Axmann (Water Slezak), have been raiding a railroad construction site in order to extort "protection" money from the railroad company.  When Bud and Lou arrive, they are mistaken for company spies, and the Sheik and Axmann attempt to murder them. As each attempt fails, the assassins hatred for Bud and Lou intensifies, especially when Lou outbids the Sheik for six slave girls, one of whom, Nicole (Patricia Medina), is actually a French spy assigned to gain entry into the Sheiks camp.  The boys are then chased, only to wind up hiding at the Foreign Legion headquarters, where Axmann convinces them to join.

Meanwhile, the Foreign Legion Commandant (Fred Nurney) suspects that there is a traitor among the Legionnaires, as the Sheik anticipates every one of the Legions moves (secretly through Axmann).  The Commandant then grants Bud and Lou a pass into town where they meet up with Nicole.  She informs them that they must  search Axmanns room for proof that he is a traitor, but he catches them in the act.  However, they are spared, only to end up at a Legionnaire desert camp.  Just before the camp is ambushed by the Sheiks men, Bud and Lou wander off in search of a camel, and escape death. They are eventually captured, along with Nicole, who is put in Sheik Hamuds harem. The Shiek orders that one of his wrestlers execute them. The wrestler turns out to be Abdullah, who helps them escape.  They head to Fort Apar, where they lure the Sheiks men, and blow it up. They are given awards by the Commandant and honourably discharged from the Legion. Lou thanks Nicole for helping them and gives his award to her before they leave, only for Bud to find out that Lou is taking the six slave girls with them back to the States.

==Cast==
* Bud Abbott
* Lou Costello
* Patricia Medina
* Walter Slezak
* Douglass Dumbrille
* Leon Belasco
* Marc Lawrence
* William Wee Willie Davis
* Tor Johnson
* Sammy Menacker
* Jack Raymond
* Fred Nurney
* Paul Fierro
* Henry Corden
* Candy Candido

==Production==
Originally scheduled to begin in December 1949, filming was postponed when Costello had to undergo an operation for a gangrenous gall bladder in November 1949. Filming eventually began on April 28, 1950, and ended on May 29, 1950. Despite having a stunt double, Costello did his own wrestling in the film, only to be rewarded with a wrenched arm socket and a stretched tendon. Furmanek, Bob and Ron Palumbo (1991). Abbott and Costello in Hollywood. New York: Perigee Books. ISBN 0-399-51605-0 

In 1948, Abbott and Costello fired their agent, Eddie Sherman. Just before the filming of this picture, they reconciled with Sherman and rehired him. 

David Gorcey, a member of the comedy team The Bowery Boys, has a cameo appearance in the film. The voice of the skeleton in the film was provided by Candy Candido, who briefly became Abbotts partner in the 1960s after Costello had died.

==DVD releases==
This film has been released three times on DVD.  Originally released as single DVD on August 12, 1998, it was released twice as part of two different Abbott and Costello collections, The Best of Abbott and Costello Volume Three, on August 3, 2004, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 