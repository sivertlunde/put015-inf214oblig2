Red Dust (2004 film)
 
{{Infobox film
| name           = Red Dust
| image          = RedDust2004DVDCover.jpg
| caption        = DVD cover art
| director       = Tom Hooper David Thompson
| screenplay     = Troy Kennedy Martin 
| based on       =  
| starring       = Hilary Swank Chiwetel Ejiofor
| music          = Robert Lane
| cinematography = Larry Smith
| editing        = Avril Beukes
| distributor    = BBC Films
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom South Africa
| language       = English
| budget         =
}}
Red Dust is a 2004 British drama film starring Hilary Swank and Chiwetel Ejiofor. It was directed by Tom Hooper. The story, written by Troy Kennedy Martin, is based on the novel Red Dust (novel)|Red Dust by Gillian Slovo. The film was predominantly shot on location in South Africa, specifically in the town of Graaff Reinet.

== Plot ==
Sarah Barcant (Hilary Swank), a lawyer in New York City who grew up in South Africa, returns to her childhood dwelling place to intercede for Alex Mpondo (Chiwetel Ejiofor), a Black South African politician who was tortured during apartheid. Under the Truth and Reconciliation terms, the whole truth must come out. As it is, under duress Mpodo had identified one of his underground comrades, Steve Sizela.  But he also confirms that he kept a much more important secret – a buried list that names some new recruits.  This is still where he left it, years ago.
 Ian Roberts).  Also that much of the torture was carried out at a ranch rather than at the police station – thus confirming Alexs apparently false memories of a dirt floor and a tap in the corridor.  Visiting the ranch, he puts details together.  Dirk admits where he buried Steve Sizela.  The bones are found and dug from the ground; Mpondo decides to allow amnesty as the whole truth has been said. Muller, who denied the charges and pleaded not guilty, ironically applies for amnesty himself, infuriating members of the Black South African community.

Parallel with this story is Sarah Barcants confrontation with her own past.  She was arrested as a teenager for having a black boyfriend, breaking the apartheid laws.  She got out after one night, thanks to Ben Hoffman, a white lawyer who has worked all of his life against apartheid and is a strong believer in Truth and Reconciliation.  Sarah Barcant is there because she owes him a debt, and he is now too weak to take the case himself.  He sees the outcome as positive.

== Cast ==
* Hilary Swank as Sarah Barcant
* Chiwetel Ejiofor as Alex Mpondo
* Jamie Bartlett as Dirk Hendricks Ian Roberts as Piet Müller
* Nomhle Nkyonyeni as Mrs. Sizela 
* Marius Weyers as Ben Hoffman

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 