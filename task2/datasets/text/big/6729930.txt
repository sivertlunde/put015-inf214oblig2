Tomei Ningen
{{Infobox Film
| image          = Tomei ningen poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Motoyoshi Oda
| producer       = Takeo Kita
| writer         = Toshikazu Yamano Hajime Takaiwa
| narrator       = 
| starring       = Seizaburou Kawadu Miki Sanjo Minoru Takada
| music          = Kyousuke Kami
| cinematography = Katsumi Yanagishima
| editing        = Shuichi Anbara
| distributor    = Toho
| released       =  
| runtime        = 70 min.
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 action / horror film, originally released in 1954. Produced by the Toho studio, the film is a loose adaption of the science fiction novella entitled The Invisible Man, written by British author H.G. Wells in 1897.  The film was directed by Motoyoshi Oda, and written by Hiroshi Beppu and Shigeaki Hidaka.

The two stories do not share many plot details, aside from the general premise of a man who can turn himself invisibility|invisible, but it was produced with the intent of adapting Wells work. The film follows a circus clown and a gang, both of whom can become invisible due to ingesting a formula developed by the government during World War II. The gang of criminals has been using their powers to wreak havoc and cause chaos throughout Japan, and the clown (portrayed by Seizaburô Kawazu) is the only one with the courage to stand up to them. Ultimately, he defeats them, but in doing so is forced to sacrifice his own life.

The character Invisible Man aka Takemitsu Nanjo is kaijin (怪人, literally "mysterious person", referring to roughly humanoid monsters), a type of kaiju.

The film was moderately successful, but was overshadowed by the huge crossover success of Godzilla (1954 film)|Godzilla, earlier in 1954. It has never been released outside of Japan.

Special effects was made by Eiji Tsuburaya.

 

== Cast ==
Actors in the film include: 
* Seizaburo Kozu as Invisible Man aka Takemitsu Nanjo
* Miki Sanjo as the singer
* Yoshio Tsuchiya as young reporter
* Minoru Takada as gang boss
* Keiko Kondo as the little girl Mariko
* Kamatari Fujiwara as Marikos grandfather
* Sonosuke Sawamura
* Shoichi Hirose
* Fuyuki Murakami
* Yutaka Sada
* Akira Sera
* Yasuhisa Tsutsumi
* Shin Otomo
* Haruo Nakajima
* Kazuo Suzuki as the man in nightclub

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 


 
 