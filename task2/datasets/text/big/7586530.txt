The Road to Reno
{{Infobox film
| name = The Road to Reno
| image =
| caption = The Road to Reno Richard Wallace
| writer = Virginia Kellogg (story)  Josephine Lovett (screenplay)
| starring = Lilyan Tashman Charles Buddy Rogers Peggy Shannon William "Stage" Boyd
| producer =
| distributor = Paramount Pictures
| budget =
| released =  
| runtime = 74 min. English
| country = United States
}} Richard Wallace.

==Plot==
Twice divorced Jackie Millet tries one more time with number three. Unfortunately, her wedding is suddenly halted when the womans son kills the groom during the ceremony, and then shoots himself.

==Cast==
* Lilyan Tashman
* Charles Buddy Rogers
* Peggy Shannon
* William "Stage" Boyd
* Irving Pichel
* Wynne Gibson
* Richard "Skeets" Gallagher
* Tom Douglas
* Judith Wood
* Leni Stengel
* Emile Chautard
* Adrienne Ames		 Charles D. Brown		
* Claire Dodd		
* Davison Clark Bill Elliott
* Theresa Harris
* Anderson Lawler
* Philo McCullough
* Harold Minjir
* Frances Moffett
* Larry Steers
* Kent Taylor

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 