So Does an Automobile
 
{{Infobox Hollywood cartoon|
| cartoon_name = So Does an Automobile
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist = Thomas Johnson Harold Walker
| voice_actor = Margie Hines
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = 31 March 1939
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}

So Does an Automobile is a 1939 Fleischer Studios animated short film starring Margie Hines as Betty Boop.

==Synopsis==
Betty operates an auto repair garage for sick and injured automobiles. Betty sings as she restores broken-down taxicabs, high-price limousines and restores a police car with sore and flat feet (tires) back to perfect condition again.

==Notes==
Instrumental versions of the theme song for this cartoon were later used in Bettys next film, Musical Mountaineers, and later in the 1940 Popeye cartoon, Wimmin Hadnt Oughta Drive.
 

 
 
 
 
 


 