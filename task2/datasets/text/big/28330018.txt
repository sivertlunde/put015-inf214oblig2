Nagarathil Samsara Vishayam
{{Infobox film
| name           = Nagarathil Samsara Vishayam
| image          = Nagarathil Samsara Vishayam.jpg
| image_size     =
| caption        = VCD cover Prashanth
| producer       = Mumthaz Basheer
| screenplay     = Kaloor Dennis 
| story          = Alwayn Siddique Chitra Innocent Kuthiravattam Pappu Baiju Keerikkadan Jose Johnson
| cinematography = Prathapan
| studio = Simple Productions
| editing        = G. Venkitaraman
| distributor    = Kirthi Films Jubilee (in association with)
| released       = 1991
| runtime        = 110 minutes 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Malayalam comedy thriller film written by Kaloor Dennis, directed by Chellappan|Prashanth, and starring Jagadish and Siddique (actor)|Siddique.

== Cast ==
* Jagadish	 ...	Gopinatha Menon Siddique	 ...	Samson
* Kuthiravattam Pappu	 ...	Pappi
* Baiju	 ...	Krishnankutti Chithra	 ...	Susan
* Keerikkadan Jose	 ...	Vikraman
* Jagannatha Varma	 ...	Achutha Menon
* Geetha Vijayan	 ...	Saritha
* Thodupuzha Vasanthi	 ...	Bharathi
* K.P.A.C. Sunny		 Innocent	 ...	Gap Swamy
* Valsala Menon		 Zainuddin	 ...	Sundareshan
* Unnimary	 ...	Subhashini Shivaji	 ...	Alex
* Santhakumari		
* Viji Thampi	 ...	Himself
* Suma Jayaram	 ...	Herself

==External links==
*  

 
 
 


 