Fear of Clowns 2
{{multiple issues|
 
 
}}

{{Infobox film|
 name=Fear of Clowns 2|
 image=fear-of-clowns-2-2007-poster.jpg|
 caption=Movie poster|
 director=Kevin Kangas|
 writer=Kevin Kangas|
 starring=Jacky Reres Mark Lassise Frank A. Lama Johnny Alonso Phillip Levine Clarence McNatt Vincent T. Brown Adam Ciesielski Lars Stevens Savannah Costello Leanna Chamish|
 released= |
 runtime=107 minutes|
 language=English|
 preceded=Fear of Clowns
 }}
Fear of Clowns 2 is a 2007 horror film and a sequel to Fear of Clowns. The film takes place two years after the events of the previous one and continues the story of a young woman (Jacky Reres) getting stalked by a clown killer (Mark Lassise).

==Plot==
The film begins two years after the events of the first film, with Detective Peters (Frank A. Lama reprising his role) at the Doctor (Neil Conway)s office, who reveals to Peters that he has Creutzfeldt-Jakob disease (CJD), and there is no treatment; its fatal.

Two months later, it is revealed that after being sent to jail two years ago, Doug Richardson a.k.a. "Shivers The Clown" (Mark Lassise reprising his role) was sent to a mental asylum, and Lynn Blodgett (Jacky Reres reprising her role) has quit her painting career and gone on to write a book called Coulrophobia, which talks about her "fear of clowns" and the story about the stalking killer clown two years ago; also she and Tucker Reid have broken up and havent seen each other for a while. But things go terribly wrong: An orderly named Ralph (Johnny Alonso) helps Shivers and two other inmates escape the asylum. Shivers takes Ralph and the inmates back to his old house, which is now occupied by a junkie (Jed Duvall), who is later strangled to death by Shivers. The next day, after hearing Shivers escape, Lynn calls Detective Peters and tells him that she will be home soon. Meanwhile, the Junkies group of friends (Valerie Saunders, Clayton Myers, Johnny Sullivan) arrive at the house, only to be killed by the inmates, Giggles The Clown (Phillip Levine) and Ogre The Clown (Clarence McNatt). Meanwhile, Lynn arrives back home and meets Detective Peters at a restaurant, where he reveals that hes going to kill Doug Richardson. As Detective Peters takes Lynn home, they find out that Shivers has just killed Tucker outside an amusement park, where Tucker was constructing a new design for a roller coaster. Peters then calls two police officers (Rob Stull, Steve Carson) to guard Lynns house before they leave at 9:00 to secluded cabins in the woods. Before they leave, Ogre kills the police officers, while Shivers tries to kill Lynn. But during the process, Shivers kills Giggles instead.

Peters and Lynn escape through the cellar, while Shivers takes off in the van (where Ralph is handcuffed in), leaving Ogre behind to mourn over Giggles dead body, who was Ogres best friend. Ogre then takes off to go after Shivers for revenge. The next day, Peters and Lynn arrive at the cabins, where they are greeted by Hot Rod (Adam Ciesielski), Stoltz (Lars Stevens), and Rego (Tom Proctor), who are all friends of Peters. Meanwhile, Shivers leaves Ralph in the van while he takes a leak, and Ralph calls an anonymous caller, telling him that he needs to pay him (Ralph) more money if he (the caller) wants the job to be done. Then, Shivers leaves to go find Lynn, only end up at the Horners place, killing the entire family: Old Man Horner (Dave "Bullet" Wooter), Mrs. Horner (Michelle Trout), Craig (Mike Baldwin), Owen (Chris OBrocki), and Craig and Owens sex girlfriend Maggie (Savannah Costello). Shivers then continues to find the cabins.

Peters goes outside to see if the coast is clear, only to be axed in the side. Lynn then takes off into the woods with Shivers on her tail, leaving the axe inside Peters. Peters who is still alive, reloads his gun and drags himself to where Lynn and Shivers ran off to. Peters finds them, and shoots Shivers two times: once in the hand, the other in the back, and Shivers falls into a gully. Lynn goes to check to see if Peters is okay, but then looks to see Shivers rise up out of the gully. Lynn takes Peters gun and points it at Shivers, but before she can pull the trigger, Stoltz (who was only injured in the face) pops out of nowhere and shoots Shivers three times in the chest, knocking him back into the gully. It is unknown whether if Peters is dead, or hes just simply unconscious. The police arrive, only to find out that theres no body in the gully. All thats there is a trail of blood that leads a hundred feet south, and then Shivers climbed out of the gully and went into the woods. Sgt. Raup (Vincent T. Brown) decides to break the news to Lynn in the morning after she gets a good night sleep. It is unknown if Shivers is alive, or he died after heading out into the woods.

==Cast==
*Jacky Reres  as Lynn Blodgett
*Mark Lassise  as Shivers The Clown
*Frank A. Lama  as Detective Peters
*Tom Proctor  as Rego
*Phillip Levine  as Giggles The Clown
*Clarence McNatt  as Ogre The Clown
*Johnny Alonso  as Ralph
*Adam Ciesielski  as Hot Rod
*Savannah Costello  as Maggie
*Lars Stevens  as Stoltz
*Leanna Chamish  as Carol
*John C. Bailey  as Doctor Jones
*Vincent T. Brown  as Sgt. Raup
*Dave "Bullet" Wooters  as Old Man Horner
*Michelle Trout  as Mrs. Horner
*Mike Baldwin  as Craig
*Chris OBrocki  as Owen
*Jay McCarey  as Officer Ripley
*Rob Stull  as Officer Rickenhouse
*Steve Carson  as Officer Stewie

==External links==
*  

 
 
 
 
 