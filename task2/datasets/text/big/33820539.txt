Yasmin (1955 film)
{{Infobox film
| name           = Yasmin
| image          = yasmin_1955.jpg
| image_size     =
| caption        =
| director       = Abdur Rashid Kardar
| producer       = Abdur Rashid Kardar
| writer         = Story: Abdur Rashid Kardar Dialogue: Jagdish Kanwal & S. N. Banerjee
| narrator       = Jayant
| music          = C. Ramchandra
| cinematography = Dwarka Divecha
| editing        = M. S. Hajee
| studio         = Kardar Studio
| distributor    = Musical Pictures
| released       = 1955
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}} 1955 Hindi Rashid Khan, Maruti Rao, S. N. Banerjee, Shyam Kumar and Rajan Haksar forms an ensemble cast. The film was produced at Kardar Studio. The films score was composed by C. Ramchandra duo with lyrics provided by Jan Nisar Akhtar. Editing was done by M. S. Hajee and was filmed by Dwarka Divecha with the audiography was done by Ishan Ghosh. The story is about Ahmad who is in love with Yasmin.

==Plot==
The story revolves around Sheikh who has lots of properties, and recovering taxes and dues on them. He lives in a palatial house with his wife, Zubeda, and son, Ahmad. When Ahmad grows up, Zubeda notices that he is spending too much time with women in their harem, and wants his father to get him involved in their business. Later Ahmad mend his ways, give up on Yasmin and return home, where he has planned his marriage with Nadira. He refuses to marry Nadira, and runs away from home. What happens is Ahmad life is the next part of the story?

==Cast==
* Vyjayanthimala as Yasmin
* Suresh as Ahmad Jayant
* Rashid Khan
* Maruti Rao
* S. N. Banerjee
* Shyam Kumar
* Rajan Haksar Mumtaz Begum as Zubeda Sheikh

==Soundtrack==
The films soundtrack was compose by C. Ramchandra with the lyric were penned by Jan Nisar Akhtar.  The album was one of the last album that feature Lata Mangeshkar with C. Ramchandra, they parted their ways after the film. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyrics || Length (m:ss) ||Notes
|-
| 1|| "Mujhpe Ilzam-E-Bewafai" ||  
|-
| 2|| "Ankhon Mein Sama Jao" ||  
|-
| 3|| "Tum Apni Yaad Bhi Dil Se" ||   and Suresh
|-
| 4|| "Bechain Nazar Betab Jigar" || Talat Mahmood || Jan Nisar Akhtar || 03:46 || Featuring actor Suresh
|}

==Awards==
;Filmfare Awards
* Filmfare Award for Best Cinematographer (B&W category) - Dwarka Divecha {{cite news|title=Filmfare Awards (1955)
|url=http://www.gomolo.in/Awards/Awards.aspx?AwardCode=2&YearCode=1955 |publisher=Gomolo.com|accessdate=2011-11-21}} 

==References==
 

==External links==
*  
*   at Upperstall.com

 

 
 
 
 
 