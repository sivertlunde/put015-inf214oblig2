The Plague (1992 film)
{{Infobox film
 | name = The Plague
 | image = The Plague (1992 film).jpg
 | caption =
 | director = Luis Puenzo
 | writer = 
 | starring = 
 | music = Vangelis
 | cinematography = Félix Monti 
 | editing = Juan Carlos Macías 
 | producer = 
 }}
The Plague (original title: La Peste) is a 1992 Argentine-French-British drama film directed by Luis Puenzo and based on the novel La Peste by Albert Camus. It entered the competition at the 49th Venice International Film Festival, and won no awards.  

==Cast==
*William Hurt: Dr. Rieux
*Sandrine Bonnaire: Martine Rambert 
*Jean-Marc Barr: Jean Tarrou 
*Robert Duvall: Joseph Grand 
*Raúl Juliá: Cottard 
*Victoria Tennant: Alicia Rieux 
*Jorge Luz: Old Man with the cats

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 