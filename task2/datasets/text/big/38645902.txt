Jana Gana Mana (film)
{{Infobox film
| name           = Jana Gana Mana
| image          = Jana_Gana_Mana_Movie_Poster.png
| alt            =  
| caption        = Poster
| director       = Amit Abhyankar
| producer       = Sandeep Kadam and Sachin Kadam
| writer         = Sameer Joshi
| starring       = Nandu Madhav    Chinmay Sant   Madhura Welankar-Satam   Santosh Juvekar   Asmita Joglekar
| music          =Saleel Kulkarni
| cinematography = Rajesh Khale
| editing        = Shankh Rajadhyaksh
| studio         = Golden Dreams Productions Pvt. Ltd
| distributor    = Outreach Motion Pictures Pvt. Ltd
| released       =  
| runtime        = 105 minutes
| country        = India
| language       = Marathi
}}

Jana Gana Mana is a 2012 Indian film.

==Plot==

In a tribal village of Maharashtra is a school which is a part of ‘Sarva Shiksha Abhiyaan’ (Education for All) run by the government. Ramchandra Sontakke (Nandu Madhav) is the sole teacher in the school. Sontakke is a romantic; he wants to build a nation of educated kids but is in for a shock when he realizes that the handful of kids who attend his school come basically for the free mid day meal rather than to study. Sontakke decides to impress his boss by putting up a grand show on the Independence Day and coax him into transferring Sontakke to a town school closer home.

Katu, a tribal boy wants to be the leader and sing the national anthem on Independence Day. But, he can do so only if he fulfills Sontakke’s condition of sporting a white tunic and shorts. Katu goes to great extent to from stealing clothes to begging to even risking run down by a speeding car in an attempt to extract blood money for his uniform.  However, Sontakke learns the stark realities of life and manages to get himself transferred even before the Independence Day. With no need to impress his boss will all the efforts he and his students have taken go down the drain?

== Cast ==

* Nandu Madhav 
* Chinmay Sant 
* Madhura Welankar-Satam 
* Santosh Juvekar 
* Asmita Joglekar

== Awards ==

* The film premièred at the Pune International Film Festival in the competition section and was awarded special jury award for the child actor.
* The Film also won The Best Feature Film with Rural Backdrop and Best Director handling Rural issue at the 49th Maharashtra State Film Awards presented by The Government of Maharashtra. Actor Nandu Madhav was also awarded special Jury Award.
* Best Film Award at Utkarsh Film Festival, Johannesburg.
* The Film was also selected at The Riverside International Film Festival, Los Angeles. http://www.youtube.com/playlist?list=PL28CCC03E849A9F05

==Release==
 26 January 2012 and did a fifty day theatrical run. It was the first Indian Feature Film to be released simultaneously on innovative digital platform L3  along with its theatrical release.
   

== References ==
 
 

== External links ==
* http://punemirror.in/article/58/2012012920120129081329257e828b72/Untamed-Pride.html
* http://www.madaboutmoviez.com/2012/01/jana-gana-mana-movie-review-brilliance-undone/
* http://thecommonmanspeaks.com/2012/01/29/jana-gana-mana-marathi-movie-review/
* http://www.thepunekar.com/2012/01/12/marathi-films-at-pune-international-film-festival-2012/

 
 
 
 
 
 
 
 