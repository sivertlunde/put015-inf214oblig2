I Love You, Beth Cooper (film)
{{Infobox film
| name           = I Love You, Beth Cooper
| image          = I love you beth cooper.jpg
| caption        = Theatrical release poster Chris Columbus
| producer       = Chris Columbus Mark Radcliffe Michael Barnathan Larry Doyle
| based on       =   Jack T. Carpenter Lauren London 
| music          = Christophe Beck
| cinematography = Phil Abraham
| editing        = Peter Honess
| studio         = 1492 Pictures The Bridge Studios Ingenious Film Partners
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes 
| country        = Canada
| language       = English
| budget         = $18 million
| gross          = $15,821,907
}}
 Chris Columbus. novel of Larry Doyle, with Doyle also writing the films screenplay. The film stars Hayden Panettiere and Paul Rust.

==Plot== Army soldier Jack T. Carpenter) to come out of the closet and finally admit hes gay. Afterwards, Denis invites Beth and her friends, Cammy (Lauren London) and Treece (Lauren Storm), to a party he has planned at his house.

At Deniss house, his parents leave but not before his father (Alan Ruck) revealing that he has condoms upstairs if he needs them. Beth, Cammy, and Treece arrive at Denis’s house for the party. Kevin and his trendy four-wheeler soon come barging in, with his Army buddies vowing revenge on Denis and all coked up to the eyeballs. They attempt to beat Denis up, but it results in Denis, Beth, Rich, Cammy, and Treece escaping in Beths car.
 tip one KISS and is told that she was named after that very song. Denis thinks its cool that beth has two "headbangers" for parents. In Beths eyes one can see shes slowly warming to Denis, and is becoming aware that he truly does genuinely love her, much to her amazement. 
 Echo and drive without lights. They then stumble upon Deniss father and mother (Cynthia Stevenson) (who are both having car sex) by almost crashing into them. Rather than facing Denis’s father with his pants down, Beth decides to drive them away unseen to a private party at Valli Wooley’s house, encountering Kevin once again who had tracked them there. Kevin challenges Denis to a fight, as a now fed-up Beth crashes his car through the wall of the house. They drive away in Kevin’s Hummer H2 to the vacant Buffalo Glenn High School, parking halfway up the front flight of stairs, and enter the school with Beth’s head cheerleader key. After showcasing their cheerleading routine, Beth, Treece, and Cammy decide to run to the girls locker room to take a shower. Just as Denis is undressing to join his friend and the girls in the shower, Kevin and his buddies appear in the locker room and jump him once again. Rich challenges Kevin and his friends in a towel whipping duel, as he has been training for years after being towel whipped as a bullied young kid. Rich finally manages to defeat the three guys, towel-whipping them unconscious down a flight of stairs. They escape to Treeces fathers cabin in Beths Echo where Rich, Treece, and Cammy have a threesome, as Beth and Denis enjoy the sun rise and finally share their first kiss.

They head to Deniss house afterwards where his parents are delighted to see he has hooked up , but make him aware he still needs to be punished for leaving the house in a wreck. Beth says goodbye to Denis, gives him a kiss, and touchingly thanks him for loving her. Denis tells her "whats not to love" and that she mustnt forget that. Then promises that he will see her at their high school reunion and if they are both still single that he will marry her. Beth smiles and tells him its a deal. After Beth leaves with Treece and Cammy, Rich proclaims to Denis that he might be homosexual after all, or perhaps bisexual (plus jokes that after last nights antics hes still more heterosexual than Denis). Denis informs Rich that he was just playing about waiting until the reunion to talk to Beth again. He tells Rich that he is going to leave Beth a message on Facebook and ask her out. Rich disagrees with him and tells Denis that he should make a grand gesture by going to her house with a boom box and wait for her. They then begin debating on how Denis should go about asking Beth on a date.

==Cast==
 
* Hayden Panettiere as Beth Cooper
* Paul Rust as Denis Cooverman Jack T. Carpenter as Richard "Rich/Dick" Munsch
* Lauren London as Cameron "Cammy" Alcott
* Lauren Storm as Teresa "Treece" Kilmer
* Shawn Roberts as Kevin Micheals
* Brendan Penny as Sean Doyle
* Jared Keeso as Dustin Klepacki
* Alan Ruck as Mr. Cooverman
* Cynthia Stevenson as Mrs. Cooverman Pat Finn as Coach Raupp
* Andrea Savage as Dr. Gleason
* Samm Levine as Convenience Store Clerk
* Maggie Ma as Raupps Sophomore
* Marie Avgeropoulos as Valerie "Valli" Wooley
* Josh Emerson as Gregory "Greg" Saloga
 

==Production== Chris Columbus, with the screenplay written by Doyle.
 Centennial Secondary School, Magee Secondary School and at St. Patricks Regional Secondary; which are all located in Vancouver, British Columbia.
 widget that allowed for customization of the trailer and that could be sent to loved ones, friends and family. 

The film was produced and originally to be released by Fox specialty subsidiary Fox Atomic, but theatrical distribution reverted to 20th Century Fox after Atomic folded in April 2009.   

==Release  ==

===Reviews=== At the Movies called the film "The Worst Film of 2009 So Far", criticizing it for its stereotypical portrayal of high school students and glorification of drinking and driving.

Cinematical stated that much of the dialogue was lifted directly from the book, but when spoken on screen, the lines fall flat. "The adaptation from R-rated novel to PG-13 film comes into play." In a scene where Beth and Denis try to buy beer, Beth offers to kiss the store clerk so that she can buy the beer but the viewer does not see the kiss. Cinematical calls Panettieres performance unconvincing as a "rule-breaking girl who walks on the wild side". 
 Chris Columbus. The publication felt that Columbus flattened every joke and sucked the life out of the actors. In addition, the publication felt that Columbus flair for sight gags reached its peak when Denis uses two tampons to stem a nosebleed. 

===Box office===
In its opening weekend (July 10–12), the film grossed $4,919,433 at 1,858 theaters, which was enough for seventh place, and it finished its worldwide theatrical run with $15,821,907. 

==Soundtrack==
The first track is performed by Christopher Columbus daughter.
{{tracklist
| headline =  {{cite web
| url         = 
| title       =allmusic: I Love You, Beth Cooper: Overivew
| first       =William
| last        =Ruhlmann
| work        =Allmovie
| accessdate  =May 21, 2010
}} 
| extra_column    = Artist
| collapsed = no
| title1          = Forget Me
| extra1          = Violet Columbus
| length1         = 2:50
| title2          = Try It Again
| extra2          = The Hives
| length2         = 3:30
| title3          = Come Out of the Shade The Perishers
| length3         = 3:58 Sway
| extra4          = The Kooks
| length4         = 3:35
| title5          = Last Kiss
| extra5          = Christophe Beck
| length5         = 2:44
| title6          = Catch Me If You Can
| extra6          = Gym Class Heroes
| length6         = 5:08 Too Much, Too Young, Too Fast Airbourne
| length7         = 3:43
| title8          = A Good Idea at the Time
| extra8          = OK Go
| length8         = 3:12
| title9          = Beth Cooper Suite
| extra9          = Christophe Beck
| length9         = 4:41 Beth
| Kiss
| length10         = 2:46
| title11          = Cruisin (Smokey Robinson song)|Cruisin
| extra11          = Smokey Robinson
| length11         = 5:54
| title12          = Who Knew?
| extra12          = Christophe Beck
| length12         = 1:07
| title13          = Feels Like the First Time Foreigner
| length13         = 3:51
| title14          = Schools Out (song)|Schools Out
| extra14          = Alice Cooper
| length14         = 3:30
| title15          = Forget Me
| extra15          = Eleni Mandell
| length15         = 3:13
}}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 