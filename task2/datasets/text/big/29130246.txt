Simple Simon (2010 film)
 
{{Infobox film
| name           = Simple Simon
| image          = Simple Simon.jpg
| caption        = Film poster
| director       = Andreas Öhman
| producer       = {{plainlist|
* Jonathan Sjoberg
* Bonnie Skoog Feeney
}}
| writer         = {{plainlist|
* Andreas Öhman
* Jonathan Sjöberg
}}
| starring       = Bill Skarsgård
| music          = Josef Tuulse
| cinematography = Niklas Johansson
| editing        = {{plainlist|
* Mikael Johansson
* Andreas Öhman
}}
| studio         = {{plainlist|
* Naive AB
* Sonet Film AB
}}
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Best Foreign Language Film at the 83rd Academy Awards.    It made it to the penultimate round of nominations and competed with eight other films.   

==Plot==
Simon is an 18-year old man with Asperger syndrome.  Incapable of living independently, he is cared for by his endlessly loving and patient brother, Sam, and Sams girlfriend, Frida. He lives by an unchanging daily routine and finds any change in his life very stressful. He spends his day working as a groundskeeper in a special program for mentally disabled people, tending athletic fields and roadsides.  He imposes routine on his two housemates with a wall chart that allocates chores and schedules leisure.  When the world becomes too stressful, Simon seals himself in a large pot and imagines himself floating in space, where everything is perfectly quiet and predictable.  These habits cause a lot of tension with his two housemates.  One night, Simon interrupts Sam and Frida while they are having sex in order to ask for toilet paper.  For Frida, this is the last straw, and the next day she dumps Sam and moves out.

Simon returns home from work to find the house empty and dinner unprepared.  When Sam returns home, he decides to order pizza for dinner, but this further distresses Simon as they usually eat tacos on Friday evenings.  When Sam announces that Frida is definitely gone for good and that they must adapt, Simon suffers a meltdown.  Sam, upset at Fridas departure, tries to rationalize away his pain by criticizing Fridas habits, such as being bad at sex and eating caviar sandwiches.  Simon interprets these remarks literally and visits Fridas house, asking her to return and change her habits, since Simon is incapable of changing his.  Frida angrily drives him off, telling him he ought to find another girl to take her place.

Simon, as is his nature, takes Fridas remark seriously, and sets off to find Sam a new girlfriend who will better fit in with their lifestyle. Unaware of how love and courtship work, he compiles a list of Sams tastes and habits and proceeds to look for women who match the profile.  He stops random women in the streets and asks 13 certain questions (such as "do you prefer dogs to cats?" and "do you make noises when having sex?"), and when he finds a woman who answers all 13 questions correctly, he takes their details and photographs them.  He presents to Sam an album of suitable matches he has found and asks him to choose his new girlfriend from them.  Sam rejects the list, telling Simon it would be boring for him to have a girlfriend who matches his habits exactly, and suggests that a girl who differs from him would be more attractive, in the same way that the opposing poles of a magnet attract.

==Cast==
* Bill Skarsgård as Simon
* Martin Wallström as Sam
* Cecilia Forss as Jennifer
* Sofie Hamilton as Frida
* Susanne Thorson as Jonna Kristoffer Berglund as Peter 
* Lotta Tejle as Simons mum
* Per Andersson as the French Chef

== Reception ==
Robert Koehler of Variety (magazine)|Variety called it "well mounted but overloaded with whimsy".   Kirk Honeycutt of The Hollywood Reporter wrote, "The surprising Swedish film Simple Simon performs the neat trick of finding laughs and wisdom about a central character who suffers from Aspergers syndrome, yet does so without trivializing the serious nature of the affliction." 

Simple Simon was nominated for four Guldbagge Awards: best film, best actor in a lead role (Bill Skarsgård), best actress in a supporting role (Cecilia Forss), and best screenplay. 

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film
* Online dating service

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 