My Sucky Teen Romance
{{Infobox film
| name           = My Sucky Teen Romance
| image          = File:MySuckyTeenRomanceMoviePoster2011.jpg
| alt            = 
| caption        = 
| director       = Emily Hagins
| producer       = 
| writer         = Emily Hagins
| screenplay     = 
| story          = 
| starring       = Elaine Hurt Patrick Delgado Santiago Dietche 
| music          = Christopher Thomas
| cinematography = Jeffrey Buras
| editing        = Shane Gibson
| studio         = Cheesy Nuggets Productions Arcanum Pictures
| distributor    = Dark Sky Films MPI Media Group
| released       =   }}
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
My Sucky Teen Romance is a 2011 supernatural comedy by American director Emily Hagins and her third feature length film. It first released on March 15, 2011 at the South by Southwest film festival and stars Elaine Hurt as a young teenager that falls in love with a teen vampire at a sci-fi convention. My Sucky Teen Romance was partially funded through Indiegogo.  

==Synopsis==
When 17-year-old Kate (Elaine Hurt) and her friends decide to attend the science fiction convention they traditionally go to each year, they arent prepared for what they find. For it is at this convention that Kate meets and falls for Paul (Patrick Delgado), a handsome teen boy and newly turned vampire. He accidentally bites her, beginning her transformation. Her friends must find a way to return Kates humanity while fending off the attacks of other vampires that attend the event.

==Cast==
* Elaine Hurt as Kate
* Patrick Delgado as Paul
* Santiago Dietche as Jason
* Lauren Lee as Allison
* Tony Vespe as Mark
* Lauren Vunderink as Cindy
* Devin Bonnée as Vince
* Sam Eidson as Lyle
* Tina Rodriguez as Gina
* Kristoffer Aaron Morgan as 1950s Guy
* Harry Jay Knowles as Con Vampire Expert
* Christopher Gonzalez as Max
* Rebecca Robinson as Kates Mom
* Ben Gonzalez as Kates Dad
* Megan Hagins as Jasons Mom

==Production==
Filming took place in Austin, Texas during 2010.  For the movie, Hagins pulled on experiences from her personal life such as regular attendance at CONvergence (convention)|CONvergence, a yearly speculative literature convention.    Paul Gandersman joined the film as producer after expressing interest in My Sucky Teen Romance at a party and much of the films cast were people that Hagins had previously worked with on prior projects.  

==Release==
My Sucky Teenage Romance premiered at the 2011 SXSW.   MPI Media Group released the film theatrically on August 22, 2012, where it played in New York, Los Angeles, Chicago, and Austin.  It was released on home video on September 4, 2012. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 60% of five surveyed critics gave the film a positive review; the average rating was 6.6/10.   Fearnet called the film "a sweet little winner" with inexperienced but committed actors.  Variety (magazine)|Variety gave a mostly positive review, saying that although the performances were uneven overall, the movie was "surprisingly appealing" and was a "micro-budget horror-comedy tailor-made for teen geeks who are sufficiently self-aware to laugh at themselves".  DVD Talk expressed admiration at what the then 17-year-old Hagins was able to do with the film and said that while the movie was "hit and miss throughout" it was "worth seeing for its heart and soul and creativity".  Dread Central panned My Sucky Teen Romance overall, giving it two out of five blades and said that "While Hagins shows that she’s becoming more than capable behind the camera, she still needs a bit of work at honing her abilities as a screenwriter."   Screen Daily wrote, "Hagins’ youth is obviously exploitable in publicity terms, but the film has winning qualities which would be impressive in any low-budget movie."   In a mixed review, Pop Matters criticized the films dialog but wrote that the film "has its moments" and is "admirably sincere". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 