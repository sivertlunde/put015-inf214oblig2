Premashilpi
{{Infobox film
| name = Premashilpi
| image =
| caption =
| director = VT Thyagarajan
| producer =
| writer = Sreekumaran Thampi
| screenplay =
| starring = Jayabharathi Jagathy Sreekumar Sankaradi Sreelatha Namboothiri
| music = V. Dakshinamoorthy
| cinematography =
| editing =
| studio = Usha Cine Arts
| distributor = Usha Cine Arts
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed VT Thyagarajan. The film stars Jayabharathi, Jagathy Sreekumar, Sankaradi and Sreelatha Namboothiri in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Jayabharathi 
*Jagathy Sreekumar 
*Sankaradi 
*Sreelatha Namboothiri 
*K. P. Ummer 
*MG Soman  Meena

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Amme Amme || Vani Jairam || Sreekumaran Thampi || 
|- 
| 2 || Kathirmandapathil || Vani Jairam || Sreekumaran Thampi || 
|- 
| 3 || Thulliyaadum Vaarmudiyil || K. J. Yesudas || Sreekumaran Thampi || 
|- 
| 4 || Vannu Njan Ee Varna || P Jayachandran || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 