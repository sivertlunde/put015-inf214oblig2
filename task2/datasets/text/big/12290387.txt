Aayirathil Oruvan (2010 film)
 
 
{{Infobox film
| name           = Aayirathil Oruvan
| image          = Aayirathil Oruvan.jpg
| caption        = Theatrical release poster
| director       = Selvaraghavan
| producer       = R. Ravindran
| writer         = Selvaraghavan
| starring       =  Karthi Reemma Sen Andrea Jeremiah R. Parthiepan
| music          = G. V. Prakash Kumar
| cinematography = Ramji
| editing        = Kola Bhaskar
| studio         = Dream Valley Corporation
| distributor    = Ayngaran International Dream Valley Corporation
| released       =  
| runtime        = 183 minutes
| country        = India
| language       = Tamil
| budget         =   
| gross          =   
}} Tamil fantasy-adventure Parthiepan playing a pivotal role.
 Chola Emperor, pending invasion, sends his people to a mystery location, to survive the threat. The story resumes with an archaeologist, a coolie and a member of the army going in search of the archaeologists father to the ruined city that was the place the exiled Chola Prince retreated to. An expedition is promptly arranged, and on the course they stumble on the lost Chola civilization and its king and find unexplained links between them and the culture. The shocking events and the problems that arise forms the crux of the story.
 Telugu dubbing version, titled Yuganikki Okkadu, following six weeks later. Upon release, the film received mixed reviews whilst proceeding to become a successful venture commercially.

==Plot== southern India. To escape them and save the life of his successor the Chola emperor sends his son along with the Chola people to a secret territory. The refugees take along an idol sacred to the Pandyas, angering them. To capture the escaped Cholas and the stolen idol, the Pandyas extend their invasion to unexplored territories but cannot find them.

Centuries later, in 2009, Indian archaeologists continue searching for the existence of the lost Chola group based on clues left by the ancient Pandyan warriors. All archaeologists who attempted to search for the secret land have disappeared. Archaeologist Chandramouli is the most recent person to have gone missing.

The Indian government organizes a search expedition led by officer Anitha to find Chandramouli and the Chola empire; she is assisted by the Indian army led by Ravisekharan. They recruit archaeologist Lavanya, the estranged daughter of Chandramouli, because her insight is essential for the success of the expedition. She hands over crucial documents on the Chola dynasty, prepared by her father, with instructions on the route to reach the destination.
 porters headed by Muthu, to transport the baggage during the journey. The crew embark on their voyage leading them to an island, Min-gua, near Vietnam. They face seven traps set by the Cholas: sea creatures, cannibals, warriors, snakes, hunger, quicksand and a village. Many porters and army men are killed by these traps.

Muthu, Anitha and Lavanya get separated from the others. They reach the ruins of a village where they are subjected to black magic and nearly go mad before reaching the secret hideout of the Chola. The three find an ethnic isolated primitive Tamil group, ruled by a Chola king. The king and his people are in hiding, awaiting the arrival of the fabled messenger who will bring glory and prosperity back to their land and lead them back to Thanjavur, their motherland. The king and the priest consult the gods for omens and order Muthu, Anitha and Lavanya to be burnt alive as sacrifices.

Anitha tells the king that she is the messenger sent from the homeland. Muthu and Lavanya are enslaved while Anitha is given a chance to prove herself. She tries to seduce and convince the Chola king to march towards the homeland in two days so he can be crowned properly as a king. He suspects her bona fides since none of her actions match those described by the kings ancestors. The ancestors wrote that the messenger will be preceded by hail and following his ill-treatment will eventually help the tribe.

Meanwhile, Anitha drugs the priest and poisons the water sources. She catches a glimpse of the Pandyas sacred idol and leaves, finally exposing her identity as a descendant of the Pandya Dynasty. For generations, her race has been trying to find the whereabouts of the Chola prince and his people. The central minister, who sponsors the expedition, is also shown to be a Pandyan. The Chola king is shattered for having believed in Anitha. Anitha and Ravishekaran gather an army and assault the hidden kingdom. The king discovers that Muthu is the true messenger who would save the Cholas from the clutches of Anitha and the army.

The Cholas fight bravely, but eventually lose and are taken prisoners. Their women are molested and raped by the army.The king is killed and the men drown in the seas. Muthu is able to escape and save the Chola prince. The seed of the Chola dynasty is brought back to their motherland by the messenger as prophesied.

==Cast== MGR fan, Chola kingdom.      
* Reemma Sen as Anitha Pandiyan, an intelligence officer who gathers a team to investigate the disappearance of Chandramouli. Anitha is strong-minded, ruthless and determined. However she nurtures a soft feeling for Muthu during the course of the journey. The film became Reemma Sens first attempt at parallel cinema following a series of roles in commercial projects, and her portrayal was highly appreciated by film critics.   
* Andrea Jeremiah as Lavanya Chandramouli, the estanged daughter of Chandramouli, who also embarks on the journey to find her father. An archaeologist by profession, Lavanya becomes the quiet tactical leader of the journey through her knowledge of the culture and the history of the Chola dynasty. Andrea Jeremiah, like Karthi, appears in her second major role for the film, also singing parts of two songs which appear in the soundtrack. During the making of the film news websites linked Andrea with the director, Selvaraghavan, albeit to the pairs displeasure.   
* R. Parthiepan as Chola King, successor and last of the Chola dynasty after escaping an attack from the Pandyas. The city that the Cholas lived in perished, resulting in the city becoming haunted with Parthiepan, the head of the people. The images for the Chola King, not dated or named in the film, were kept under wraps prior to the movies release. Parthiepan signed for the role after much speculation that Dhanush would be selected,  and his performance won plaudits from critics.  Pratap Pothan as Chandramouli, an archaeologist who went missing during a search for the Chola kingdom, prompting a manhunt. Chandramouli earlier walked out on his daughter, Lavanya, and is later found trapped in the Chola kingdom.
* Azhagam Perumal as Ravisekharan, who leads the army brigade in the search for Chandramouli. Known for his exploitative and violent ways, Ravisekharan develops a mutual foe in Muthu and his fellow workers.

==Production==

===Development=== Karthi and Aayirathil Oruvan. The producer was announced to be R. Ravindran whilst Yuvan Shankar Raja was appointed as music director following five previous successful soundtracks in Selvaraghavan films.    Despite early indications that the directors brother Dhanush was going to play a guest role,  it became evident that the role was subsequently given to R. Parthiepan.    Andrea Jeremiah was also signed for a role in the film in October 2007, in her second film after Pachaikili Muthucharam and director-actor Azhagam Perumal followed suit in November 2007.   

Nearly six months after filming began, Yuvan Shankar Raja left the project, because he could not spend as much time on this film as Selvaraghavan wanted. Subsequently, the role of the music director was handed to G. V. Prakash Kumar, for whom Aayirathil Oruvan became his biggest project to date.    Rambo Rajkumar, the films stunt director, died in April 2009 and was posthumously praised for his action choreography. The films music released two months later to much appraisal in a well-held audio release function. Soon after the filming finished, the lead actors moved onto other projects as did Selvaraghavan whilst post-production continued. In August 2009, Selvaraghavan divorced his wife, Sonia Agarwal with his close proximity to Andrea being a speculated cause.    During the period, G. V. Prakash Kumar and Selva also worked on the music in Mumbai whilst re-recording was also held in Austria and London.  Towards the end of the year, the film began to announce release date of Christmas which was later further delayed to coincide with the Pongal festival.    A date clash occurred with Karthis Paiyaa, with an eventual hearing leading to the Karthis latter film being delayed.    Throughout December 2009, release work began with a trailer and promotional songs being released on 13 December with Kamal Haasan and Surya Sivakumar attending as special guests.    The film was subsequently censored before the end of the year by the Central Board of Film Certification and settled with an adult rating, after Selvaraghavan refused to get rid of gory scenes.    On 31 December, it was announced that the film was sold worldwide for 350&nbsp;million rupees for theatrical, television and other rights.   

===Filming===
After extensive development and pre-production which took four months for scripting,    the film started the first schedule in the forests of Chalakudy in Kerala with Karthi, Reemma Sen and Andrea during October 2007.  The project developed a reputation for its gruelling shoots, a novel concept in Tamil films, at an early stage of production. Thirty-five days into the shoot, Selvaraghavan gave a statement that the film was forty percent over and the film should release by May 2008 whilst also mentioning that rains in Kerala led to the budget going over expectations two months into the project.  In January 2008, the unit moved to Jaisalmer in Rajasthan to shoot in the deserts in the region, however they were delayed again by unseasonal rains.    Missing its original release date, the films progress carried on through 2008, with shooting occurring towards the end of the year inside sets at Ramoji Film City in Hyderabad, India|Hyderabad.    In the studios, choreographer Shivshankar composed a classical dance for Reemma Sen and Parthiepan, and the sequence was shot over twenty days.  Shooting carried on in sets for three more months with second half scenes being recorded.    Shoots in all regions were tough and demanding for the crew as the film featured more than three thousand junior artistes from a variety of unions across India, with the language barrier becoming a problem.  The project, eighteen months into shoot, soon began to face questions about its progress, with the producer, Ravindran, having to complain to the Tamil Film Producers Council that Karthi was trying to change his look for his next film, Paiyaa, following the long period he had spent with Aayirathil Oruvan.    In February 2009, filming was completed after 263 days of shooting; therefore the producers signaled for a summer release but it was postponed by six months. 

When inquired by the media in regard to the long periods of shoot, whilst in production, the lead actor expressed that they were also unaware of how long the films shoot was going to carry on. Reemma Sen originally signed for forty days whilst Andrea signed for three months, without knowing that the film would eventually take 263 days of filming.    Furthermore, Parthiepan claimed to have been signed for forty days, whilst his segment lasted up to 140 days.

==Release== Sun TV for  .       The film was given an "A" certificate by the Indian Censor Board.

===Box office===
Released in 600 screens worldwide,  Aayirathil Oruvan was declared a superhit in Telugu and hit in Tamil.    Aayirathil Oruvan took the biggest opening by a considerable distance earning   on its opening weekend in Chennai.    In the United Kingdom, the film opened across 7 screens and grossed £29,517 ($44,868) in the opening week. The film, distributed by Ayngaran International opened at 22nd place.    The film grossed $340,082 in the second week in Malaysia, after opening in seventh.    Similarly, the Telugu version of the film which released on 5 February, Yuganiki Okkadu, took a strong opening.    The Telugu version released across 93 screens across Andhra Pradesh and grossed   on its opening weekend, and   in its lifetime. 

===Reception===
Upon release, the film remarkably gained mixed reviews. Sify, cited that the film represented "something new in the placid world of Tamil cinema" adding that it "broke away from the shackles of the stereotypes".    Selvaraghavan also was praised by the reviewer with claims that "the director transports us to a whole new world and at the end of it all, we are dumb stuck by the visuals, the packaging and the new way of storytelling".  Rediff.com gave the film 3.5 out of 5 claiming that viewers should "steel your stomach before   watch it" and "regardless of the minor discrepancies, AO is definitely a movie to watch".  In contrast, Behindwoods.com gave the film 0.5 out of 5 describing the film as "wildly crass", dismissing that "the underdeveloped script lacks everything – starting from strong plot twists to captive locations to graphics to credibility, above all".   

==Soundtrack==
{{Infobox album
| Name       = Aayirathil Oruvan
| Type       = Soundtrack
| Artist     = G. V. Prakash Kumar
| Cover      =
| Released   = 14 June 2009
| Recorded   = 2008–2009 Feature film soundtrack
| Length     = 51:23
| Label      = Sony Music
| Producer   = G. V. Prakash Kumar
| Last album = Angadi Theru (2009)
| This album = Aayirathil Oruvan (2009)
| Next album = Irumbu Kottai Murattu Singam (2010)
}}
{{Album ratings rev1 = Behindwoods rev1Score =   
}}

The film was launched in 2007 with Selvaraghavans regular music director, Yuvan Shankar Raja, following five successive successful albums together. However, Yuvan Shankar Raja was ousted from the project in March 2008 due to differences of opinion. Subsequently, G. V. Prakash Kumar was signed on for his biggest project to date and work for the soundtrack began again from scratch.    The number Adada, composed by Yuvan Shankar was removed from the album after his departure, and was replaced by the similarly sounding Un Mela Aasadhan, composed by Prakash Kumar.  

The soundtrack to Aayirathil Oruvan was released on 14 June 2009 at a University Auditorium in Chennai, in a critically praised event.    Prominent film personalities across the South Indian film industry attended the launch, which became one of the first films to play live music at the audio launch. The launch featured live performances from G. V. Prakash Kumar and Andrea Jeremiah for several songs, as well as songs from Selvaraghavans brother, Dhanush, and his wife, Aishwarya Dhanush. Furthermore, the night featured a fashion show from Erum Ali, a Kalari performance, Chenda Melam by women from Kerala, a classical dance performance by actress Poorna and choreographed by Sivashankar. 

The album features ten tunes; six songs, two alternate versions and another two theme songs. The album featured vocals from singers Karthik (singer)|Karthik, Vijay Yesudas, Bombay Jayashri, Nithyasree Mahadevan and P. B. Sreenivas, who made a comeback to playback singing with his song. Moreover husband-wife personality, Dhanush and Aishwarya Dhanush, sung for the album along with the composer, Prakash Kumar and lead actress, Andrea Jeremiah. Lyrics for the songs were written by Vairamuthu, Veturi Sundararama Murthy, Selvaraghavan and Andrea Jeremiah.    For a song set in the thirteenth century, research was carried out to find instruments used during that period. A Yazh|Yaazh, a melodic instrument used in the Sangam Period, and a horn, a brass instrument made from animal horns from Bhutan, were used.  The soundtrack garnered critical acclaim and was considered Prakash Kumars finest work to date. Furthermore, shortly after the music release, an album success meet was held.   
 Aayirathil Oruvan,  which was bought from the original copyright holders of the song in December 2007.  The films picturisation of the three exploring the ruins of the fallen kingdom in the song, were praised. 

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 51:23
| title1          = Oh Eesa (Composers Mix)
| extra1          = Karthik (singer)|Karthik, Andrea Jeremiah
| length1         = 5:22
| title2          = Maalai Neram
| extra2          = Andrea Jeremiah G. V. Prakash Kumar
| length2         = 5:58
| title3          = Un Mela Aasadhaan
| extra3          = Dhanush, Aishwarya Dhanush, Andrea Jeremiah
| length3         = 4:30
| title4          = The King Arrives
| extra4          = Neil Mukheriee & Madras Augustin Choir                    
| length4         = 3:02
| title5          = Thaai Thindra Mannae (The Cholan Ecstasy)
| extra5          = Vijay Yesudas, Nithyasree Mahadevan, Shri Krishna
| length5         = 5:57
| title6          = Pemmane
| extra6          = P. B. Sreenivas, Bombay Jayashri
| length6         = 5:59
| title7          = Celebration of Life
| extra7          = Instrumental 
| length7         = 3:32
| title8          = Thaai Thindra Mannae (Classical Version)
| extra8          = Vijay Yesudas
| length8         = 7:17
| title9          = Indha Padhai
| extra9          = G. V. Prakash Kumar
| length9         = 4:53
| title10         = Oh Eesa (Club Mix)
| extra10         = Big Nikk
| length10        = 4:53
}}

==Accolades==
{| class="wikitable" style="font-size:95%;"
|-
! Ceremony
! Award
! Category
! Nominee
! Outcome
|- Edison Awards 2nd Edison Awards Edison Awards Edison Award Best Thriller Film Selvaraghavan
| 
|- 58th Filmfare Awards South Filmfare Awards South Filmfare Award Best Film
|R. Ravindran
| 
|- Filmfare Award Best Director Selvaraghavan
| 
|- Filmfare Award Best Actor Karthi
| 
|- Filmfare Award Best Actress Reemma Sen
| 
|- Filmfare Award Best Supporting Actor
|R. Parthiepan
| 
|- Filmfare Award Best Supporting Actress Andrea Jeremiah
| 
|- Filmfare Award Best Music Director
|G. V. Prakash Kumar
| 
|- Filmfare Award Best Male Playback Singer Dhanush for "Un Mela Aasadhaan"
| 
|- Filmfare Award Female Playback Singer Andrea Jeremiah,  Aishwarya R. Dhanush for "Un Mela Aasadhaan"
| 
|- 5th Vijay Awards Vijay Awards Vijay Award for Best Actress Reemma Sen
| 
|- Vijay Award for Best Supporting Actor
|R. Parthiepan
| 
|- Vijay Award for Best Art Director
|T. Santhanam
| 
|- Vijay Award for Best Stunt Director Rambo Rajkumar
| 
|- Vijay Award for Best Costume Designer Erum Ali
| 
|- Vijay Award for Favourite Song
|"Unmela Aasadhaan"
| 
|}

==See also==
* List of longest films in India by running time

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 