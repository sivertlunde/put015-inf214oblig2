The End of Pinky
{{Infobox film
| name           = The End of Pinky
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Claire Blanchet
| producer       = Michael Fukushima
| writer         = 
| screenplay     = Claire Blanchet
| story          = 
| based on       =  
| narrator       = Heather ONeill (English) Marc-André Grondin (French)
| starring       = Marc-André Grondin
| music          = Geneviève Levasseur
| cinematography = 
| editing        = 
| studio         = National Film Board of Canada
| distributor    = 
| released       =  
| runtime        = 8 min 14 s
| country        = Canada
| language       = English/French
| budget         = 
| gross          = 
}}
The End of Pinky is a 2013 stereoscopic National Film Board of Canada animated short directed by Claire Blanchet, based on a short story of the same name by Heather ONeill. Described by the director as an "animated film noir set in a dream-like version of Red-Light District, Montreal|Montreals Red Light District," the film is narrated in its English version by ONeill and in French by Quebec actor Marc-André Grondin. Music for the film was composed by Genevieve Levasseur. ONeills story was originally published in the 2008 January–February edition of The Walrus. The film had its world premiere on September 11 in the Short Cuts Canada Programme of the 2013 Toronto International Film Festival.         

In December 2013, The End of Pinky was named to the Toronto International Film Festivals annual top ten list, in the short film category. 

==References==
 

==External links==
* 
* , ONeills original story, and   in The Walrus
* 
*  at TIFF.net

 
 
 
 
 
 
 
 
 
 
 
 


 
 
 