Beautiful Losers (film)
{{Infobox film
| name           = Beautiful Losers
| image          = BL_poster.jpg
| caption        = Release Poster
| director       = Aaron Rose Joshua Leonard
| producer       = Jon Barlow Rich Lim Noah Khoshbin Chris Green
| writer         = Thomas Campbell Mike Mills Stephen Powers II Claire E. Rojas Aaron Rose Deanna Templeton Ed Templeton
| music          = Money Mark
| cinematography = Tobin Yelland
| editing        = Lenny Mesina
| distributor    =
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Stephen Powers II

It premiered at South by Southwest on March 9, 2008   and later in general release on August 8, 2008 at the IFC Center in New York City.

==Subject matter== Thomas Campbell, Mike Mills, Steven "Espo" Powers, Aaron Rose, Ed Templeton and Deanna Templeton.

A series of interviews with these artists explains their reasoning behind their "do-it-yourself" style of street art. As some of these artists discuss their growth in popular artistic culture they explain how becoming renowned and admired in the art world was something that never occurred to them from their various roots in street culture, or simply creating art for themselves. As many of the artists began to be recognizable and sought after they discuss their series of commercial success: creating advertisements for popular products, designing products themselves, working in film and being hired to paint and create artwork in well known locations. The personal feelings and convictions of some of the artists and how creating work for corporations compares to their beginnings in street culture is also discussed. The film portrays the artists as outside the realm of contemporary art.

 

==Art book==
The film was preceded by a published art collection titled Beautiful Losers.(ISBN 1933045302) The book was co-published by Iconoclast Editions and Distributed Art Publishers, and edited by co-curators Christian Strike and Aaron Rose, who directed the film. 

==Traveling museum exhibition==
The publication of the book was to complement and commemorate the traveling museum exhibition, which itself was one of the settings and subjects for the film. The exhibit is a large scale group exhibition featuring the artists from the film as well as others, and which continues to tour around the world.  Opening in Cincinnati, Ohio at the Contemporary Arts Center in March 2004, the exhibition is currently in Europe through 2009. The exhibition was conceived and produced by Iconoclast, where founders Christian Strike and Aaron Rose also acted as co-curators of the exhibition, and as the editors and publishers of the book.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 