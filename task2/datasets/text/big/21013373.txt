Give 'Em Hell, Malone
{{Infobox film
| name           = Give Em Hell, Malone
| image          = Give Em Hell Malone.jpg
| caption        = 
| director       = Russell Mulcahy
| producer       = Erik Anderson Johnny Martin Brian Oliver Richard Rionda Del Castro Richard Salvatore
| writer         = Mark Hosack
| starring       = Thomas Jane Ving Rhames Elsa Pataky David C. Williams
| cinematography = Jonathan Hall
| editing        = Robert A. Ferretti
| studio         = Malone Productions North by Northwest Productions Continental Entertainment Capitol Blue Rider Finance
| distributor    = Hannibal Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $15 million
}}
Give Em Hell, Malone is a 2009 crime film directed by Russell Mulcahy and starring Thomas Jane, Ving Rhames and Elsa Pataky. 

==Plot==
An ex-private eye turned gun for hire named Malone is hired to retrieve a suitcase from a building full of armed mobsters, but a violent shootout ensues and Malone is eventually left as the only survivor.  Suspecting a set-up, he retains the only noteworthy item contained in the case - a small painted animal referred to as "the meaning of love" -  for himself, prompting several different parties in the employ of a local gangster - Whitmore - to pursue Malone in attempt to discern the meaning of the cases contents.

After a series of violent encounters leaving many dead, Malone eventually confronts Whitmore, who admits he was responsible for hiring Malone and planted the toy - a keepsake belonging to Malones young son - as a means to trick Malone into exterminating Whitmores criminal help, allowing Whitmore to become a legitimate businessman without worrying about being tainted by potential loose ends from his criminal past.  Malone kills Whitmore and phones his (Malones) wife and son - previously presumed dead - but does not engage them in conversation.

==Cast==
*Thomas Jane as Malone
*Ving Rhames as Boulder
*Elsa Pataky as Evelyn
*French Stewart as Frankie the Crooner
*Leland Orser as Murphy
*Chris Yen as Mauler
*William Abadie as Pretty Boy
*Gregory Harrison as Whitmore
*Doug Hutchison as Matchstick

==Release==
The film was released on DVD and Blu-ray January 26, 2010. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 