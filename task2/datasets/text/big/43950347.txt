Samagamam
{{Infobox film
| name = Samaagamam
| image =
| image_size =
| caption =
| director = George Kithu
| producer = Babu Thiruvalla John Paul John Paul Rohini Sukumari Thilakan Johnson
| cinematography = K. P. Nambiathiri
| editing = N. P. Satheesh
| studio = Symphony Creations
| distributor = Symphony Creations
| released =  
| country = India Malayalam
}}
 1993 Cinema Indian Malayalam Malayalam film, directed by George Kithu and produced by Babu Thiruvalla. The film stars Jayaram, Rohini (actress)|Rohini, Sukumari and Thilakan in lead roles. The film had a musical score by Johnson (composer)|Johnson.   

==Cast==
  
*Jayaram as Johnson Rohini as Elsamma
*Sukumari
*Thilakan as Pallivathukkal Kariyachan
*Kaviyoor Ponnamma as Annakutty
*Nedumudi Venu as Fr. Puthenthara
*Jose Pellissery as Eenashu
*Kozhikode Narayanan Nair as Principal
*Idavela Babu as Babu Ashokan as Joy
 

==Soundtrack==
The music was composed by Johnson (composer)|Johnson.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Manjum Nilaavum || K. J. Yesudas, Chorus || ONV Kurup || 
|-  Johnson || ONV Kurup || 
|-  Janaki || ONV Kurup || 
|- 
| 4 || Vaazhthidunnithaa (bit) || S. Janaki|Janaki, CO Anto || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 


 