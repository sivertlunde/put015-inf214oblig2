Chinnavar
{{Infobox film
| name           = Chinnavar
| image          = 
| image_size     = 
| caption        = 
| director       = Gangai Amaran
| producer       = C. H. Venu
| writer         = Gangai Amaran
| starring       =  
| music          = Ilaiyaraaja
| cinematography = A. Sabapathy 
| editing        = V. T. Vijayan B. Lenin
| distributor    = Indrani Movies
| studio         =
| released       = 24 April 1992
| runtime        = 135 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1992 Tamil Chandrasekhar and Kasthuri in lead roles. The film was released on 24 April 1992.  

==Plot==

Muthu (Prabhu (actor)|Prabhu) and Veerasamy (Vagai Chandrasekhar|Chandrasekhar) are fishermen and they fish together in the same boat. Muthu lives with his mother (S. N. Lakshmi). Veerasamy drinks lot of alcohol and he has a sister, Meena (Kasthuri (actress)|Kasthuri) who is in love with Muthu. Muthu decides to marry his carefree friend Veerasamy to Ponni (Chithra (actress)|Chithra). In a financial trouble, Veerasamy joins Kumars boat, Kumar (Radha Ravi) is a rich fishermen union leader. Veerasamy realizes that Meena is in love with Muthu and he promises to Muthu his sisters hand. Meanwhile, Kumar asks Veerasamy to marry Meena, compelled by his wife, Veerasamy accepts. Hopeless, Meena ties to herself a Mangalsutra|Thaali, she says to Veerasamy that Muthu marries her and Muthu confirms to save her honor. Veerasamy gets angry but when Muthu says the truth, he apologizes to him. Kumar beats Muthus mother and kidnaps Meena, he arranges a forced marriage between him and Meena. Muthu saves her and he marry her with his best friend blessing.

==Cast==
 Prabhu as Muthu alias Chinnavar Chandrasekhar as Veerasamy Kasthuri as Meena Chithra as Ponni
*Radha Ravi as Kumar
*Goundamani  Senthil
*Kovai Sarala as Sevattamma
*S. N. Lakshmi as Muthus mother

==Soundtrack==

{{Infobox album |  
  Name        = Chinnavar
|  Type        = soundtrack
|  Artist      = Ilaiyaraaja
|  Cover       = 
|  Released    = 1992
|  Recorded    = 1992 Feature film soundtrack
|  Length      = 35:22
|  Label       = 
|  Producer    = Ilaiyaraaja
|  Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1992, features 7 tracks with lyrics written by, the director himself, Gangai Amaran.  

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration 
|-  1 || Mano ||  5:01
|- 2 || Goondru Gongura || Mano || 5:06
|- 3 || Kadalorak Kavithayileu || Chorus || 4:56
|- 4 || Kottukkili || S. P. Balasubrahmanyam, K. S. Chithra || 5:11
|- 5 || Maaraappu Chela || Mano, K. S. Chithra || 4:59
|- 6 || Padakottum Pattammah || S. P. Balasubrahmanyam, K. S. Chithra, Malaysia Vasudevan || 5:10
|- 7 || Uttalangkiri Kiri || Malaysia Vasudevan, K. S. Chithra || 4:59
|}

==References==

 

==External links==
* 

 
 
 
 
 