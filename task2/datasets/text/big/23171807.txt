The Card (1952 film)
{{Infobox film
| name           = The Card
| caption        = A poster bearing the films American title: The Promoter
| image	=	The Card FilmPoster.jpeg
| director       = Ronald Neame John Bryan Earl St. John (exec.) Bob McNaught (assoc.)
| writer         = Arnold Bennett (novel) Eric Ambler
| starring       = Alec Guinness Petula Clark Valerie Hobson Glynis Johns
| music          = William Alwyn  
| cinematography = Oswald Morris
| editing        = Clive Donner
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
}}
 the novel American audience, it was adapted by Eric Ambler and directed by Ronald Neame. It was released in 1952. It starred Alec Guinness as Denry Machin, Petula Clark as Nellie Cotterill, Valerie Hobson as the Countess, and Glynis Johns as Ruth Earp. The film was nominated for the Academy Award for Best Sound.   

It is mainly faithful to the novel, omitting some minor incidents.

==Plot==
The film follows the adventures and misadventures of Edward Henry (Denry) Machin, an ambitious young man from a poor background.
 clerk to Mr. Duncalf, the town clerk and a solicitor. He meets the charming and socially well-connected Countess of Chell, a client of Duncalfs, and is given the job of sending out invitations to a grand municipal ball (dance)|ball. He invites himself, and wins a five-pound bet that he will dance with the Countess. This earns him the reputation of a "card" (a "character", someone able to set tongues wagging) – a reputation he is determined to cement. But the next day, Duncalf angrily sacks Denry.

Denry offers his services as a rent collector to a dissatisfied former client of Duncalfs, Mrs Codleyn. His reputation as an efficient and no-nonsense collector brings the business of Mr Calvert. But Denry quickly realises that he can make more money by advancing loans, at a highly profitable interest rate, to the many tenants who are in arrears. He also discovers that Ruth Earp, the dancing teacher who is attracted to Denry, is herself heavily in debt. Despite this, he and Ruth become engaged.

While on holiday in Llandudno with Ruth and her friend Nellie Cotterill (as Chaperone (social)|chaperone), he witnesses a shipwreck and the rescue of the sailors—an event that he turns to his financial advantage. He also realises Ruths spendthrift nature, and they part on bitter terms.

Denry starts up the Five Towns Universal Thrift Club, a bold venture that allows members to purchase goods on credit. This increases Denrys wealth and reputation, and he is able to expand further, thanks to the patronage of the Countess.
 football club.

Ruth reappears, now the widow of a rich, older, titled man. He considers renewing their relationship but is unsure of his (and her) feelings.

Nellies father, a builder, is bankrupt (again), and the family decide to migrate to Canada, with Denrys assistance. As they are boarding the ocean liner at Liverpool, Denry realises that Nellie is devastated at her potential loss and that he really loves only her. Ruth, who is also present, is furious, but quickly starts a fresh relationship with another older titled gentleman.

Nellie and Denry marry. Denry becomes the youngest mayor in the history of Bursley.

==Cast==
* Alec Guinness - Denry Machin
* Valerie Hobson - Countess of Chell
* Petula Clark - Nellie Cotterill
* Glynis Johns - Ruth Earp
* Edward Chapman - Herbert Duncalf
* Veronica Turleigh - Mrs Machin
* Joan Hickson - Mrs Codleyn
* George Devine - Herbert Calvert
* Wilfred Hyde-White - Lord at Liverpool dock (uncredited)

For Guinness, playing the romantic lead was a departure from his previously comic roles. The film was one of the first adult screen roles for Clark, who received her first screen kiss.

==Production==
It was largely filmed in Burslem in Stoke-on-Trent, the basis for the fictional location of Bursley, and in Llandudno, North Wales.  

==Music==
Clark recorded a vocal version of the films theme, with lyrics by her long-term accompanist, Joe "Mr Piano" Henderson.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 