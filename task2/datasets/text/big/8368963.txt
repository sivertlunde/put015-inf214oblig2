Eating Out 2: Sloppy Seconds
{{Infobox film
| name           = Eating Out 2: Sloppy Seconds
| image          = Eating Out 2 Sloppy Second DVD.jpg
| caption        = Australian DVD Cover
| director       = Phillip J. Bartell
| producer       = Q. Allan Brocka J.D. Disalvatore Jeffrey Schwarz Michael Shoel
| writer         = Phillip J. Bartell Q. Allan Brocka
| starring       = Jim Verraros Emily Brooke Hands Rebekah Kochan Marco Dapper Brett Chukerman
| cinematography = Lisa Wiegand
| editing        = Phillip J. Bartell Scott Hatcher
| distributor    = Ariztical Entertainment
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $37,072  
}}
Eating Out 2: Sloppy Seconds is a gay-themed romantic comedy film released in 2006. It is the sequel to Eating Out, and Jim Verraros, Rebekah Kochan, and Emily Brooke Hands reprise their roles from the original. The film debuted at the Outfest film festival before a limited theatrical release.

==Plot==
Kyle (Jim Verraros) breaks up with Marc (Brett Chukerman), his love interest from the first movie, accusing him of flirting with hotter men. Kyle, Tiffani (Rebekah Kochan) and Gwen, friends from the first movie, all become attracted to Troy (Marco Dapper), a chiseled farm boy from Troy, Illinois, who poses nude for their art class. Troy befriends and confides in them that he has slept with both women and men, but is reluctant to embrace any gay feelings. Kyle and the girls devise a scheme in which Kyle pretends to be an ex-gay who is dating Tiffani, to overcome Troys inhibitions and get him to sleep with the both of them.

While Kyle and Troy start attending meetings with the campus ex-gay ministry, Marc notices Kyle becoming close with Troy and decides to try to seduce Troy himself. Troy eventually succumbs to Marcs advances during Gwens homoerotic photo shoot, and the two fool around, but Marc cannot go through with it because he still has feelings for Kyle.  Troy then overhears Gwen and Marc talking about the entire scheme.
 bisexual (to which everyone shouts out, "Theres no such thing!", although they later accept it) and Kyle admits he was wrong to leave Marc. 
 out Jacob (Scott Vickaryous), the closeted leader of the ex-gay ministry, to his mother by tricking him to have sex with Octavio, another member of the ministry, in a portable toilet on wheels. Jacob finally comes out to his mother (after he inadvertently ejaculates on her coat as his sexuality is revealed), and flees with Octavio. Troy takes a liking to Tiffani and they start a relationship.  

In the end, Marc and Kyle get back together after confessing their feelings to each other and start kissing. Gwen starts to date a girl experimentally.

==Cast==
{| class="wikitable"
|-
! Actor / Actress
! Character
! Notes
|- Jim Verraros Kyle
|American Idol Contestant in Top 10
|- Emily Brooke Hands 
| Gwen Anderson
|
|- Rebekah Kochan 
| Tiffani von der Sloot
|
|- Brett Chukerman 
| Marc Everhard
| Replaces Ryan Carnes, who portrayed Marc in the original.
|- Marco Dapper 
| Troy
|
|- Scott Vickaryous 
| Jacob
|
|- Mink Stole Helen
|
|-
|Adrián Quiñonez  Octavio
|
|- Jessie Gold Violet Müfdaver
|
|- Joseph Morales 
| Derek
|
|}

== Critical reception ==
Review aggregator Rotten Tomatoes reports that 44% of professional critics gave the film a positive review." 

==Sequels==
 
Eating Out 2 is the second film in the Eating Out franchise. It was followed in 2009 by  .  , two additional sequels,   and   are in simultaneous production. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 