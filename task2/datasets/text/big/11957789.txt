Sky Lovers (film)
{{Infobox film
| name           = Sky Lovers
| image          = SkyLoversPoster.jpg
| image_size     = 
| caption        = 
| director       = Jiang Qinmin
| producer       = 
| writer         = Xi Dong (novel) Liu Ye Tao Hong
| music          =
| cinematography = Dan Shao
| editing        =
| distributor    = 
| released       =
| runtime        = 93 min. China
| Mandarin Shanghainese
| budget         = 
}}
Sky Lovers ( )  2002 film based on the novel "Life Without Language" by Xi Dong which won the first Lu Xun Literature Award.  It was directed by Jiang Qinmin.

==Cast== Liu Ye- Jia Kuan, an incredibly deaf villager who also suggests an air of being slightly mentally challenged
* Dong Jie- Yu Chen, a mute girl
* Feng Enhe (冯恩鹤) - Jia Kuans father Tao Hong- Zhu Ling

==Plot==
The story is based in a remote village in the mountainous area of Guangxi.  The story begins after the takeover of the Communists  Liu Ye). Jia Kuans demeanor is friendly and he smiles at her. The village boys catch up and start yelling to Jia Kuan that this girl harmed his father. Yu Chen cannot defend herself, as she is a mute. Luckily Jia Kuan doesnt believe them, so they all go to Jia Kuans house to confirm it with Jia Kuans father, who inevitably confirms that he had hurt himself by accident.

From this incident, Yu Chen begins to live with Jia Kuan and his father. She cooks for them, and takes on a small maternal role in their family. Because Jia Kuans father is blind and Yu Chen is mute, she communicates to him by writing words onto Jia Kuans fathers hand. It is by these small interactions that Jia Kuans father discovers that Yu Chen was in the area because she was looking for her brother, as her parents had died.

From here the story carries on without further exploring Yu Chens background. Jia Kuan is in love with Zhu Ling, the village beauty, and in his childish manner aims to gain her love, however Zhu Ling is not interested in Jia Kuan. Instead she carries an affair with the only educated man living in the district.

==Awards==
Best Artistic Contribution Award at the 15th Tokyo International Film Festival

== Notes ==
 

==External links==
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 


 
 