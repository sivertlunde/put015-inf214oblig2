Ghare Baire (film)
{{Infobox film
| name           = Ghare Baire (The Home and the World)
| image          = Dvd ghare baire satyajit ray.jpg
| caption        = A poster for Ghare Baire
| director       = Satyajit Ray NFDC
| writer         = Satyajit Ray, from thenovel by Rabindranath Tagore
| starring       = Swatilekha Chatterjee (Sengupta) Victor Banerjee Jennifer Kendal Soumitra Chatterjee
| original_music = Satyajit Ray
| cinematography = Soumendu Roy
| editing        = Dulal Dutta
| distributor    =
| released       =  
| runtime        = 140 minutes
| country       = India English
| budget         =
| awards         =
}}

Ghare Baire (The Home and the World) is a 1984 Indian   at the 1984 Cannes Film Festival.   

==Plot synopsis== noble Nikhilesh Lord Curzons partition of Bengal into Muslim and Hindu states; the nationalist movement is trying to impose a boycott against all foreign goods (by claiming that imports are at the root of Indian poverty). He lives happily with his beautiful wife Bimala (Swatilekha Sengupta) until the appearance of his friend and radical revolutionist, Sandip (Soumitra Chatterjee). 
 
Sandip, a passionate and active man, is a contradiction to the peace-loving and somewhat passive Nikhil. He easily attracts the innocent and unsuspecting Bimala, creating a love triangle.

Although Nikhilesh figures out what is happening, he is a mature person and grants Bimala the freedom to grow and choose what she wants in life (as their marriage was arranged when she was a girl). Meanwhile Bimala experiences the emotions of love for the first time in a manner that helps her understand that it is indeed her husband Nikhilesh who really loves her.

Importantly, Nikhilesh tells Bimala that he would like her to have a life not only inside the home, but outside of it as well &mdash; a most controversial matter when the novel was written.

==Cast==
*Soumitra Chatterjee ...  Sandip Mukherjee 
*Victor Banerjee ...  Nikhilesh Choudhury 
*Swatilekha Sengupta...  Bimala Choudhury 
*Gopa Aich ...  sister-in-law 
*Jennifer Kendal ...  Miss Gilby (as Jennifer Kapoor) 
*Manoj Mitra...  Headmaster 
*Indrapramit Roy ...  Amulya 
*Bimala Chatterjee ...  Kulada
* Master Ajay Ghosh

==Other credits==

* Art direction: Ashoke Bose
* Sound designers: Robin Sen Gupta, Jyoti Chatterjee, Anup Mukherjee

==Production==
In 1983, during the shooting of the film, Ray suffered two massive heart attacks. His son, Sandip Ray, completed the project from his detailed instructions.

==Reception==
The film did well commercially when initially released. Rays heart attack may have played a role in this. For the Indian audience, there was an additional interest, since it featured the first full-fledged kiss in Rays films.

Critical response was mostly positive. Pauline Kael wrote: "Toward the end, Bimala, who was   into independence by her husband, becomes desperate to express that independence &mdash; recklessly, heedlessly. When it comes to truthfulnesss about womens lives, this great Indian moviemaker Satyajit Ray shames the American and European directors of both sexes." 
Vincent Canby wrote in the New York Times: "As with the works of any great director, The Home and the World defies easy categorization. In close-up, its a love story, but its one so fully defined that, as in a long-shot, it also succeeds in dramatizing the events seen on the far horizon - including the political differences between Gandhi, who led the nationalist movement, and Tagore, who, like Nikhil, stood for civilized compromise."    Roger Ebert noted that the real story of the film takes place within Bimalas heart and mind. He added: "It is a contemplative movie -- quiet, slow, a series of conversations punctuated by sudden bursts of activity."   

==Awards==
* National Film Award for Best Feature Film in Bengali at 32nd National Film Awards   
* Best Supporting Actor (Biswajit dutt) at 32nd National Film Awards 
* Best Costume Design at 32nd National Film Awards 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 