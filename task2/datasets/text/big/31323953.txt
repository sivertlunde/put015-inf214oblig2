Der Frühling braucht Zeit
{{Infobox film
| name           = Der Frühling braucht Zeit
| image          = 
| caption        =
| director       = Günter Stahnke
| producer       =
| writer         = Günter Stahnke Hermann O. Lauterbach Konrad Schwalbe
| starring       = Eberhard Mellies
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 1966
| runtime        = 75 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}}
 East German drama film directed by Günter Stahnke. It was released in List of East German films|1966.

==Cast==
* Eberhard Mellies as Heinz Solter
* Günther Simon as Erhard Faber
* Doris Abeßer as Inge Solter
* Karla Runkehl as Luise Faber
* Rolf Hoppe as Rudi Wiesen
* Hans Hardt-Hardtloff as Kuhlmey
* Erik S. Klein as Staatsanwalt Burger Friedrich Richter as Dr. Kranz
* Elfriede Née as Ruth Solter
* Agnes Kraus as Ursula Schmitz
* Heinz Scholz as Meermann
* Horst Schön as Schellhorn
* Kurt Barthel as Jensen
* Hans Flössel as Lehmann

==See also==
* Film censorship in East Germany

==External links==
*  

 
 
 
 
 
 
 