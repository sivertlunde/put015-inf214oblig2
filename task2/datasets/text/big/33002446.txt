Sitamgar
  
{{Infobox film
| name           =Sitamgar
| image          = 
| image_size     = 
| caption        = 
| director       =Raj N. Sippy
| producer       =
| writer         =
| narrator       = 
| starring       =Dharmendra Rishi Kapoor Parveen Babi  Poonam Dhillon
| music          =Rahul Dev Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1985
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Sitamgar  is a 1985 Bollywood film starring Dharmendra, Rishi Kapoor, Parveen Babi, Poonam Dhillon.

==Cast==
* Dharmendra...Sonu / Shankar
* Rishi Kapoor...Jai Kumar
* Parveen Babi...Sheela
* Poonam Dhillon...Nisha Nath
* Shreeram Lagoo...Mr. Nath
* Prem Chopra...Jeevan Kumar
* Jagdish Raj...Michael - bartender

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mausam Pyar Ka"
| Kishore Kumar, Asha Bhosle
|- 
| 2
| "Tum Dil Walon Ke Aage"
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Pyar Jab Na Diya Zindagi Ne Kabhi"
| Kishore Kumar
|-
| 4
| "Chand Roz Aur Meri Jaan"
| Kishore Kumar
|-
| 5
| "Meri Tarah Allah Kare"
| Asha Bhosle, Kishore Kumar
|-
| 6
| "Ham Kitne Nadan The Yaaron"
| Kishore Kumar
|}

==External links==
*  

 
 
 
 


 