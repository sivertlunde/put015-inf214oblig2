The House of Lies (1926 film)
{{Infobox film
| name = The House of Lies
| image =
| caption =
| director = Lupu Pick
| producer = Lupu Pick
| writer =  Henrik Ibsen (play) Fanny Carlsen Lupu Pick Mary Johnson Lucie Höflich Fritz Rasp
| cinematography = Carl Drews
| editing =
| studio = Rex-Film UFA
| released =  
| runtime = 101 minutes
| country = Germany German intertitles
| budget =
| gross =
}} Mary Johnson and Lucie Höflich. It is an adaptation of Ibsens 1884 play The Wild Duck.  The films art direction was by Albin Grau.

==Cast==
* Werner Krauss as Hjalmar Ekdal   Mary Johnson as Hedwig, Helmars Tochter  
* Lucie Höflich as Gina, Hjalmars Frau  
* Fritz Rasp as Kandidat Molwik 
* Paul Henckels as Alter Ekdal 
* Walter Janssen as Gregers, Werles Sohn 
* Albert Steinrück as Jan Werle 
* Agnes Straub as Frau Sörby 
* Eduard von Winterstein as Dr. Helling  

== References ==
 

==Bibliography==
* Hans-Michael Bock and Tim Bergfelder. The Concise Cinegraph: An Encyclopedia of German Cinema. Berghahn Books.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 

 