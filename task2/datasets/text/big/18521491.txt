Midnight in Saint Petersburg
{{Infobox Film
| name           = Midnight in Saint Petersburg
| image_size     = 
| image	=	Midnight in Saint Petersburg FilmPoster.jpeg
| caption        =  Douglas Jackson John Dunning Aleksandr Golutva André Link Edward Simons Kent Walwin
| writer         = Harry Alan Towers (as Peter Welbeck) Len Deighton (characters)
| narrator       = 
| starring       = Michael Caine Jason Connery
| music          = Rick Wakeman
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 86 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
Midnight in Saint Petersburg is a 1996 thriller film starring Michael Caine for the fifth time as British secret agent Harry Palmer.

It served as semi-sequel to Bullet to Beijing which had been released the year before, the two films having been shot back-to-back. Three previous films featuring Caine as Palmer were released in the 1960s, beginning with The Ipcress File.

==Plot==
Harry Palmer heads a private investigation business based in Moscow. His associates are Nikolai "Nick" Petrov (Jason Connery), ex-CIA agent Craig (Michael Sarrazin), and ex-KGB Colonel Gradsky (Lev Prygunov). They take on the job of finding 1000 grams of weapons-grade plutonium stolen from the Russian government, though they do not know the identity of their client.

This leads Harry back to Saint Petersburg, where (in Bullet to Beijing) he managed to make enemies of both of the leading rival gangsters, Alex (Michael Gambon) and Yuri (Anatoli Davidov). Nonetheless, suspecting that Alex is involved, Harry talks Yuri into helping him.

As a complication, Nicks ballerina girlfriend Tatiana (Tanya Jackson) is kidnapped by a gang working for Alex into order to pressure her father, the head curator of the Hermitage Museum, into helping steal valuable artwork for crooked art dealer Dr. Vestry (Serge Houde). Also in the mix is reporter Brandy (Michelle Burke), who turns out also to be working for Alex. Nick is captured when he goes looking for Tatiana, but manages to escape in time to assist Harry, with Yuris help, foil both schemes.

==External links==
* 

 

 
 
 
 
 
 

 