Paganini (1989 film)
 
{{Infobox Film
| name           = Kinski Paganini
| image          = Kinski Paganini.jpg
| caption        = A promotional poster for Kinski Paganini
| director       = Klaus Kinski
| producer       = Augusto Caminito
| writer         = Klaus Kinski
| narrator       =  Debora Kinski   Nikolai Kinski
| music          = Niccolò Paganini
| cinematography = Pier Luigi Santi
| editing        = Klaus Kinski
| distributor    = Filmverleih Carsten Frank
| released       = 7 October 1989 (Germany)  	
| runtime        = 81 min.   Directors Cut: 95 minutes
| country        =       Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1989 film based on the life and career of composer and virtuoso violinist Niccolò Paganini. The movie was written and directed by Klaus Kinski, who also performs the lead role.  It was to be Kinskis final film before his death in 1991.
 Debora Kinski) and son (Nikolai Kinski) alongside him.  Klaus Kinski felt that he and Paganini had led similar lives, and both gave "demonic" performances in their own fields that often sparked great controversy.

In his 1999 documentary My Best Fiend, frequent collaborator Werner Herzog says Kinski repeatedly asked him to direct the film, but he, Herzog, refused because he thought the script was "unfilmable". Herzog also states that the preparation for his role in Kinski Paganini caused the actor to take on an uncomfortable "alien" air that disrupted Kinskis performance in their last film together, Cobra Verde.

==Plot==
The film is a biopic about the life of Niccolò Paganini, who many consider to be one of the greatest violinists who ever lived. 

==Cast==
*Klaus Kinski as Niccolò Paganini
*Debora Caprioglio as  Antonia Bianchi (credited as Debora Kinski) Nicolai Kinski as  Achille Paganini
*Dalila Di Lazzaro as  Helene von Feuerbach
*Tosca DAquino as  Angiolina Cavanna Maria Anna Elisa Bonaparte
*Donatella Rettore as  Miss Wells
*Bernard Blier as  Father Caffarelli

==Release information==

Since its theatrical run, the film had only been released on DVD and VHS in Germany, but in late 2011, the film was released for the first time in North America on a two disc special edition DVD. The release contained deleted and extended scenes, the Cannes Film Festival interviews for the film, and both the theatrical and directors cut which added another 14 minutes of previously cut footage.  

== Notes ==
 

== External links ==
*  

 
 
 
 