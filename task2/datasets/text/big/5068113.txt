Imjaeobtneun naleutbae
{{Infobox film name            = Imjaeobtneun naleutbae image           = Imjaeobtneun naleutbae.jpg caption         = Na Woon-gyu in Imjaeobtneun naleutbae (1932)
| film name      = {{Film name hangul          =     hanja           =  rr              = Imjaeopneun narutbae mr              = Imja ŏmnŭn narutpae }} director        = Lee Kyu-hwan  producer        = Kang Jeong-won  writer          = Lee Kyu-hwan  starring        = Na Woon-gyu Moon Yae-bong Kim Yeon-sil Lim Woon-hak music           = cinematography  = Lee Myeong-woo   editing         = Lee Kyu-hwan  distributor     = Yoo Shin Kinema Company released        =   runtime         =  language        = Silent film  Korean intertitles country         = Korea budget          = 1,200 won
}} Korean film starring Na Woon-gyu. It premiered at Dan Sung Sa theater in downtown Seoul. This was director Lee Gyu-hwans first film. This film is significant as the last pre-liberation film that was able to present an openly nationalistic message, because of increasing governmental censorship at this time. 

==Plot==
The plot concerns Soo-sam, a farmer who goes to Seoul and works as a rickshaw man. He is jailed for stealing money to pay for his wifes hospital bills. Upon release from jail, he learns that his wife has had an affair. Disgusted, Soo-sam returns to his village with his daughter and becomes a ferry boat operator. When a bridge is constructed 10 years later, he loses his job. After the bridge engineer tries to rape his daughter, Soo-sam dies when he is hit by a train while trying to destroy the bridge. After their house burns, killing his daughter, Soo-sams ferry boat remains as the "Ownerless Ferryboat" referred to in the title.  The original final scene had Soo-sam taking an axe to the bridge. This was cut by governmental censors because, as Lee says, "to axe the bridge was to describe the anger of the Korean people against the Japanese occupation."  

== References ==
*  
*    

==Notes==
 

== External links ==
*   at  

==References==
*  
*  

==See also==
* Korea under Japanese rule
* List of Korean language films
* List of Korea-related topics

 
 
 
 


 
 