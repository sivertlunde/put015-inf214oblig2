The Day They Robbed the Bank of England
 
 
{{Infobox film
| name           = The Day They Robbed the Bank of England
| image          = DayTheyRobbedtheBankofEngland.jpg
| caption        = Poster
| director       = John Guillermin
| producer       = Jules Buck
| writer         = Howard Clewes (screenplay and adaptation) Richard Maibaum (adaptation) John Brophy (novel)
| starring       = Aldo Ray Elizabeth Sellars Peter OToole
| music          = Edwin Astley
| cinematography = Georges Périnal
| editing        = Frank Clarke
| distributor    = Metro-Goldwyn Mayer
| released       = 17 May 1960 (UK)
| runtime        = 85 min.
| country        = United Kingdom
| awards         = English
| budget         =$457,000  .  gross = $805,000 
}}

The Day They Robbed the Bank of England is a 1960 British crime film directed by John Guillermin.   It was written by Howard Clewes and Richard Maibaum and based upon a novel by John Brophy.
 Lawrence of Arabia two years later.   

==Plot==
The film is set in London at the turn of the 20th century, in 1901. While Ireland struggles for independence, Charles Norgate (Aldo Ray), an Irish-American, arrives in London after being recruited by Irish revolutionaries to undertake a robbery of the Bank of England. Iris Muldoon, the widow of a martyr in the Irish independence movement, had previously travelled to New York to hire Norgate on behalf of the movement. The Irish revolutionaries, led by OShea (Hugh Griffith), plan to rob a million pounds worth of gold bullion from the bank vaults as a political offensive. At first, the other revolutionaries are wary of Norgate but he gains their confidence by acknowledging his Irish lineage. Informed that the bank is considered impregnable, Norgate seeks a weakness in the Bank Picquet provided by the Brigade of Guards, which keeps watch on the gold.

After a visit to a local public house frequented by Her Majestys Guardsmen, Norgate befriends Lt. Monte Fitch (Peter OToole) of the Guard. After expressing an interest in architecture, Fitch directs him to a museum that holds the original designs of the banks architect. The following evening, Norgate breaks into the museum and traces the plans. Walsh (Kieron Moore), one of the revolutionaries that dislikes Norgate, is convinced that there is no weakness to be found in the banks security. Walsh is enamored by Muldoon and attempts to persuade her to leave the movement and settle with him but she refuses. In addition, although Muldoon had an affair with Norgate in New York, she no longer wishes to be involved with him either.

After being invited to the bank, Norgate gets Lt. Fitch to show him the location of the bank vaults and he counts the paces of the guardsmen to obtain a scale for the plans he traced earlier. When he learns that the guards are plagued by rats and that the floor has been reinforced, he goes to the Sewage Commission Records Department and discovers that a long-forgotten underground sewer runs directly under the bank vaults. Norgate finds an old knowledgeable tosher and after posing as an archaeologist trying to locate ancient Roman temple ruins, persuades the tosher to show him where the sewer had been sealed. The revolutionaries dig through an old entrance to the sewer and pickaxe their way into the wall leading directly under the vaults. They choose to carry out their heist on the first weekend in August, a long weekend wherein Monday is a bank holiday and most employees would be on vacation.

Lt. Finch begins to have suspicions about Norgate, whose professional intentions for being in London seem suspect. Later, further suspicious are aroused when Lt. Finch discovers that Norgate had suddenly checked out of his hotel room. While digging, one of the revolutionaries hits and punctures a gas pipe causing mantle lanterns to dim in the underground bank corridors. The absence of rats in the banks underground levels as well as the sound of faint pickaxing compels Lt. Fitch to order that the vault doors be opened to see if the bank was being compromised. However, there are three bank agents each with a separate key to the vault and one of the keyholders has gone away on holiday. He sends two guards to find and fetch the missing keyholder, who is unhappy about being disturbed and rushed to the bank.
 Parliament and that the bank heist must be halted to prevent jeopardising the bills passage. OShea announces that the movement would dissociate itself from the thieves, prompting Muldoon to convince Walsh to accompany her and inform Norgate of the change in plans. However, discovering that Norgate has indeed broken through the floor of the bank vault, Walsh says nothing and begins to take gold bars down through the tunnel they dug. After managing to steal away a million pounds worth of gold, they encounter Muldoon, who has sent away their escape tugboat. Despite her pleas, Norgate and Walsh load the gold onto a horse-drawn cart and Walsh leads it away on the streets. When Norgate realises that the tosher has not come out of the sewers, he goes back to search for him. The tosher, meanwhile, has revived after being overcome by the escaping gas, and arrives in the vault in search of Norgate, who is not the gentleman he thought he was. Norgate finally catches up with the tosher in the vault. At that moment, Lt. Finch and a section of guards open the vault doors. On the street, the cart has been greedily overloaded by Walsh and the weight of the gold breaks through in front of a passing bobby on duty.
In the last scene of the film, Norgate and Walsh are led to a police wagon in handcuffs as Iris Muldoon tearfully looks into Norgates eyes. She walks off, and the tosher wanders away carrying a fragment of a statue which he believes is a relic.

==Cast==
*Aldo Ray – Norgate
*Elizabeth Sellars – Iris Muldoon
*Peter OToole – Captain Fitch
*Kieron Moore – Walsh
*Albert Sharpe – Tosher
*Joseph Tomelty -Cohoun
*Wolf Frees – Dr. Hagen
*John Le Mesurier – Green
*Miles Malleson – Assistant Curator
*Colin Gordon – Keeper
*Andrew Keir – Sergeant of the Guard
*Hugh Griffith – OShea
==Box Office==
According to MGM records the film earned $180,000 in the US and Canada and $625,000 elsewhere resulting in a loss of $57,000. 
==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 