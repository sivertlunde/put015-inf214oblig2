The Devil Rides Out (film)
 
 
{{Infobox film
| name           = The Devil Rides Out
| image          = Thedevilridesout.jpg
| image_size     = 220
| alt            = l
| caption        = Theatrical release poster.
| director       = Terence Fisher
| producer       = Anthony Nelson Keys
| writer         = Richard Matheson
| based on       =   Charles Gray Sarah Lawson Paul Eddington Rosalyn Landor Russell Waters Eddie Powell  (uncredited)  James Bernard Arthur Grant
| editing        = Spencer Reeve Associated British-Pathé Hammer Film Productions Seven Arts Productions
| distributor    = Warner-Pathé Distributors|Warner-Pathé (UK) 20th Century Fox (US)
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = £285,000 Marcus Hearn & Alan Barnes, The Hammer Story: The Authorised History of Hammer Films, Titan Books, 2007 p 121  gross = 276,459 admissions (France) 
}}
 British Horror horror film, of the Charles Gray, Nike Arrighi|Niké Arrighi, Leon Greene and Patrick Mower.

== Plot ==
Set in London and the south of England in 1929, the story finds Duke de Richleau|Nicholas, Duc de Richleau, investigating the strange actions of the son of a friend, Simon Aron, who has a house complete with strange markings and a pentagram. He quickly deduces that Simon is involved with the occult. Nicholas de Richleau and Rex Van Ryn manage to rescue Simon and another young initiate, Tanith, from a Theistic Satanism|devil-worshipping cult. During the rescue they disrupt a ceremony on Salisbury Plain in which the Devil (Baphomet) himself appears.
 psychic connection conjuring of angel of take a possess Peggys mother in order to find Mocata, but they are only able to get a single clue, from which Rex realizes that the cultists are at a house he visited earlier.
 kills all transforms their coven room into a church. When the Duc and his companions awaken, then they discover that the spell Peggy was led into casting has reversed time and changed the future in their favour.
 pays the eternal damnation wrongly summoned God that they must be thankful for.

== Cast ==
* Christopher Lee – Duke de Richleau|Nicholas, Duc de Richleau Charles Gray – Mocata
* Nike Arrighi|Niké Arrighi – Tanith Carlisle
* Leon Greene – Rex Van Ryn (dubbed by Patrick Allen)
* Patrick Mower – Simon Aron
* Gwen Ffrangcon-Davies – Countess Sarah Lawson – Marie Eaton
* Paul Eddington – Richard Eaton
* Rosalyn Landor – Peggy Eaton
* Russell Waters – Malin The Goat of Mendes  (uncredited) 

=== Uncredited ===
* John Bown – Receptionist
* Yemi Ajibade – African
* Ahmed Khalil – Indian
* Zoe Starr – Indian girl
* Willie Payne – Servant
* Keith Pyott – Max
* Mohan Singh – Mocatas servant
* Liane Aukin – Satanist
* John Falconer – Satanist
* Anne Godley – Satanist
* Richard Scott – Satanist
* Peter Swanwick – Satanist
* Bert Vivian – Satanist

=== Others ===
* John Brown  Richard Huggett
== Background == Charles Gray, Patrick Mower and Paul Eddington. The screenplay was adapted by Richard Matheson from Wheatleys novel. In the United States the film was retitled The Devils Bride. Christopher Lee has often stated that of all his vast back catalogue of films this is his favourite and the one he would like to see remade with modern special effects and with him playing a mature Duke de Richleau. 

The A-side of British rock band Icaruss debut single, "The Devil Rides Out", was inspired by the advance publicity for the film of the same name. Though the song does not appear in the film, the singles release was timed to coincide with the films premiere, and the band themselves were invited to the premiere. 
==Release==
 
== Reception ==
Reviews of the film have been widely favorable. It currently has a 93% "Fresh} rating on Rotten Tomatoes.   

  |sign=Howard Thompson|source=New York Times Review}}

  ||sign=Staff review|source=Variety (magazine)|Variety}}

== References ==
 
*  

== External links ==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 