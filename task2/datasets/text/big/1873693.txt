Taste of Cherry
{{Infobox film
| name = Taste of Cherry
| image = Tasteofcherryposter.jpg
| caption = Film Poster
| director = Abbas Kiarostami
| writer = Abbas Kiarostami
| starring = Homayon Ershadi Abdolrahman Bagheri Afshin Khorshid Bakhtiari Safar Ali Moradi
| producer = Abbas Kiarostami
| cinematography = Homayun Payvar
| distributor = Zeitgeist Films
| released = May, 1997 (Cannes Film Festival|Cannes) September 28, 1997 (New York Film Festival) January 30, 1998 (U.S.) June 5, 1998 (UK)
| country = Iran
| runtime = 95 minutes Persian
}} 1997 film minimalist film about a man who drives through a city suburb looking for someone who can carry out the task of burying him after he has died. It was awarded the Palme dOr at the 1997 Cannes Film Festival.

==Plot==
Mr Badii (Homayon Ershadi), a middle-aged man, drives through Tehran looking for someone to do a job for him, and he offers a large amount of money in return. During his drives with prospective candidates, Badii reveals that he plans to kill himself and has already dug the grave. He needs someone to throw earth on his body, after his death. He does not discuss why he wants to commit suicide.
 Afghan seminarist, who also declines because he has religious objections to suicide. The third is an Azeri taxidermist. He is willing to help Badii because he needs the money for his sick child, but tries to talk him out of it; he reveals that he too wanted to commit suicide a long time ago but chose to live when he tasted mulberry|mulberries. The Azeri promises to throw earth on Badii if he finds him dead in the morning. That night, Badii lies in his grave while a thunderstorm begins. After a long blackout, the film ends by breaking the fourth wall with camcorder footage of Kiarostami and the film crew filming Taste of Cherry.

==Cast==
* Homayoun Ershadi - Mr. Baadi
* Abdolrahman Bagheri - Mr. Bagheri
* Afshin Khorshid Bakhtiari - Soldier
* Safar Ali Moradi - The soldier
* Mir Hossein Noori - The seminarian

==Style==
 ) in Taste of Cherry is later juxtaposed by a panoramic overhead view as his car moves across the hills]]
The film is minimalist in that it is shot primarily with long takes; the pace is leisurely and there are long periods of silence. Mr. Badii is rarely shown in the same shot as the person he is talking to (this is partly because during the filming, director Kiarostami was sitting in the cars passenger seat).

Kiarostamis style in the film is notable for the use of long shots, such as in the closing sequences. He creates distance for the audience from the characters to stimulate reflection on their fate. Taste of Cherry is punctuated throughout by shots of this kind, including distant overhead shots of the Badiis car moving across the hills, usually while he is conversing with a passenger. The visual distancing stand in contrast to the sound of the dialog, which always remains in the foreground. Like the coexistence of private and public space, or the frequent framing of landscapes through car windows, this fusion of distance with proximity can be seen as a way of generating suspense in the most mundane of moments.  

==Music==
The film does not include a background score, except for the ending titles. This features a trumpet piece, Louis Armstrongs adaptation of "St. James Infirmary." The only song featured in the film is "Khuda Bowad Yaret" (May God be your protector) by Afghan singer Ahmad Zaher, which is played in the background on a radio.

==Reception== The Eel.

When the film was released in the United States, however, it met with a split reaction amongst critics and audiences, and a minor controversy erupted  when  Roger Ebert wrote a scathing review in the Chicago Sun-Times, awarding the film a mere 1 out of 4 stars, as well as placing this film under his list of "most hated films." Ebert dismissed the film as "excruciatingly boring" and added,   "I understand intellectually what Kiarostami is doing. I am not impatiently asking for action or incident. What I do feel, however, is that Kiarostamis style here is an affectation; the subject matter does not make it necessary, and is not benefited by it. If were to feel sympathy for Badhi, wouldnt it help to know more about him? To know, in fact, anything at all about him? What purpose does it serve to suggest at first he may be a homosexual? (Not what purpose for the audience--what purpose for Badhi himself? Surely he must be aware his intentions are being misinterpreted.) And why must we see Kiarostamis camera crew--a tiresome distancing strategy to remind us we are seeing a movie? If there is one thing Taste of Cherry does not need, it is such a reminder: The film is such a lifeless drone that we experience it only as a movie." 

In his own review of Kiarostamis film, critic Jonathan Rosenbaum of the Chicago Reader awarded it a total of 4 out of 4 stars, and hailed it as a masterpiece. Responding to Eberts criticisms, Rosenbaum wrote,   "A colleague who finds Taste of Cherry “excruciatingly boring” objects in particular to the fact that we don’t know anything about Badii, to what he sees as the distracting suggestion that Badii might be a homosexual looking for sex, and to what he sees as the tired “distancing strategy” of reminding us at the end that we’re seeing a movie. From the perspective of the history of commercial Western cinema, he has a point on all three counts. But Kiarostami couldn’t care less about conforming to that perspective, and given what he can do, I can’t think of any reason he should care... the most important thing about the joyful finale is that it’s the precise opposite of a “distancing effect.” It does invite us into the laboratory from which the film sprang and places us on an equal footing with the filmmaker, yet it does this in a spirit of collective euphoria, suddenly liberating us from the oppressive solitude and darkness of Badii alone in his grave. Shifting to the soldiers reminds us of the happiest part of Badii’s life, and a tree in full bloom reminds us of the Turkish taxidermist’s epiphany — though the soldiers also signify the wars that made both the Kurdish soldier and the Afghan seminarian refugees, and a tree is where the Turk almost hung himself. Kiarostami is representing life in all its rich complexity, reconfiguring elements from the preceding 80-odd minutes in video to clarify what’s real and what’s concocted. (The “army” is under Kiarostami’s command, but it is Ershadi — an architect friend of the filmmaker in real life — who passes Kiarostami a cigarette.) Far from affirming that Taste of Cherry is “only” a movie, this wonderful ending is saying, among other things, that it’s also a movie. And we don’t have to remember all of the lyrics of “St. James Infirmary” to know that death is waiting for us around the corner." 

The Criterion Collection entered Taste of Cherry into their exclusive film collection on June 1, 1999.

==References==
 

==External links==
* 
*  
*  
* 

 
 

 
 
 
 
 
 
 
 