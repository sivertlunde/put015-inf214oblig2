Normal (2003 film)
{{Infobox film
| name            = Normal
| caption         =
| image	=	Normal FilmPoster.jpeg
| director        = Jane Anderson
| producer        = Thomas J. Busch Cary Brokaw Lydia Dean Pilcher
| writer          = Jane Anderson
| starring        = Jessica Lange Tom Wilkinson
| music           = Alex Wurman
| cinematography  = Alar Kivilo
| editing         = Lisa Fruchtman
| distributor     = HBO Films
| released        =  
| runtime         = 110 minutes
| country         = United States
| language        = English
}} writer and gender transition Midwestern factory transition to living as a woman. The film was praised by most critics,  and was nominated for numerous awards.

==Plot==
Applewood (Tom Wilkinson), after fainting on the night of her 25th marriage anniversary, shocks her wife Irma (Jessica Lange) by revealing that she suffers from gender identity disorder and plans to transition into living as a woman and be renamed Ruth. While Ruth tries to keep the family together, Irmas initial reaction is to separate from her. Patty Ann (Hayden Panettiere), their daughter, is more accepting, but Wayne (Joe Sikora), their son, struggles with the transition.

Ruth faces ostracism at church and at work. She finds understanding from her boss but not from her minister. In the end, Irma discovers that love transcends gender and the family survives.

==Cast==
* Jessica Lange as Irma Applewood
* Tom Wilkinson as Ruth Applewood
* Clancy Brown as Frank
* Hayden Panettiere as Patty Ann Applewood
* Joe Sikora as Wayne Applewood Richard Bull as Roy Applewood, Sr.
* Mary Siebel as Em Applewood

==Awards and nominations==
Normal was nominated for three Golden Globe Awards, won one Primetime Emmy Award and was nominated for another five.

Jessica Lange and Tom Wilkinson both received acting nominations for the Golden Globes, Primetime Emmys, Online Film & Television Association Awards and Satellite Awards.

{| class="wikitable sortable"
|-
! Year 
! Association
! Category
! Nominee
! Result
|-
| rowspan="11"| 2003
| rowspan="6"| Primetime Emmy Awards  Outstanding Lead Actor in a Miniseries or a Movie
| Tom Wilkinson
|  
|- Outstanding Lead Actress in a Miniseries or a Movie
| Jessica Lange
|  
|- Outstanding Made for Television Movie
| Normal
|  
|- Outstanding Main Title Design
| Antoine Tinguely, Jasmine Jodry, Jakob Trollbeck, Laurent Fauchere
|  
|- Outstanding Makeup for a Miniseries, Movie or a Special
| Hallie DAmore, Linda Melazzo, Dorothy J. Pearl
|  
|- Outstanding Writing for a Miniseries, Movie or a Dramatic Special
| Jane Anderson
|  
|- Online Film & Television Association Awards 
| Best Motion Picture Made for Television
| Normal
|  
|-
| Best Actor in a Motion Picture or Miniseries
| Tom Wilkinson
|  
|-
| Best Actress in a Motion Picture or Miniseries
| Jessica Lange
|  
|-
| Best Writing of a Motion Picture or Miniseries
| Jane Anderson
|  
|-
| Best Makeup/Hairstyling in a Motion Picture or Miniseries
| Normal
|  
|-
| rowspan="9"| 2004
| Directors Guild of America  Outstanding Directorial Achievement in Movie for Television
| Jane Anderson
|  
|-
| GLAAD Media Awards  Outstanding Television Movie or Miniseries
| Normal
|  
|-
| rowspan="3"| Golden Globe Awards  Best Actor – Miniseries or Television Film
| Tom Wilkinson
|  
|- Best Actress – Miniseries or Television Film
| Jessica Lange
|  
|- Best Miniseries or Television Film
| Normal
|  
|-
| Gracie Allen Awards  Best Female Lead – Dramatic Special
| Jessica Lange
|  
|-
| rowspan="3"| Satellite Awards  Best Actor – Miniseries or Television Film
| Tom Wilkinson
|  
|- Best Actress – Miniseries or Television Film
| Jessica Lange
|  
|- Best Miniseries or Television Film
| Normal
|  
|}

==See also==
* Transgender in film and television

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 