The Day Mars Invaded Earth
{{Infobox Film
| name           = The Day Mars Invaded Earth
| image          = DayMarsInvadedEarth.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Maury Dexter
| producer       = Maury Dexter
| writer         = Harry Spalding
| starring       = Kent Taylor Marie Windsor William Mims 
| music          = Richard LaSalle
| cinematography = John M. Nickolaus Jr.
| editing        = Jodi Copelan
| distributor    = Twentieth Century Fox
| released       =   
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Day Mars Invaded Earth is a science fiction film, made by Robert Lippert|API, released in 1963 by Twentieth Century Fox and directed and produced by Maury Dexter.  Dexter said the title was thought up by Robert Lippert to evoke memories of The Day the Earth Stood Still. 
 The War Invasion of the Body Snatchers (1956), and involves Martians duplicating a scientist and his family to prepare for their invasion. The film was shot at Greystone Mansion.

==Plot summary==
A scientist and his family are replaced by Martians hoping to invade planet Earth.

==Cast==
* Kent Taylor as Dr. David Fielding
* Marie Windsor as Claire Fielding
* William Mims as Dr. Web Spencer
* Betty Beall as Judi Fielding
* Lowell Brown as Frank Hazard
* Gregg Shank as Rocky Fielding
* Henrietta Moore as Mrs. Moore
* Troy Melton as Police Officer
* George Riley as Cab Driver

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 