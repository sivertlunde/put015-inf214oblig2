Eleanor's Secret
 

{{Infobox film
| name           = Eleanors Secret
| image          =Eleanors Secret.jpg
| caption        = 
| director       = Dominique Monfery
| producer       = Roberto Baratta Clément Calvet Christian Davin Maria Fares Xavier Julliot	
| screenplay     = Anik Leray Alexandre Reverend
| starring       = Arthur Dubois 
| editing        = Cédric Chauveau
| studio         = Haut et Court Gaumont
| released       =  
| runtime        = 80 minutes
| country        = France Italy
| language       = French Italian English
| budget         = 
| gross          =
}}

Eleanors Secret (original French title Kérity, la maison des contes) is a 2009 Franco-Italian animated feature film directed by Dominique Monfery. It won the special distinction prize at the 2010 Annecy International Animated Film Festival.  The film was produced in separate versions with French and English soundtracks.

Eleanors Secret had a limited UK cinema showing and was released on DVD in 2011 by Soda Pictures. 

It had no general release in the United States, but was made available for individual bookings. It was later released direct-to-video on 12 August 2014 from Cinedigm. 

==Plot==
The seven-year-old hero, Nathaniel, is not a confident reader. When his eccentric old aunt Eleanor dies, she leaves her house to his parents and her huge book collection to the young boy. Nathaniel discovers that the books shelter all the heroes found in childrens literature. Among them are Alice in Wonderland, Puss in Boots, Pinocchio and Little Red Riding Hood and they are counting on him for protection: if they leave the library, they will disappear along with their stories forever. When his parents start selling off the books, Nathaniel is shrunk by the evil witch Carabosse and must brave everything to save his miniature friends.

==Cast==
*Arthur Dubois as Nat
*Stephen Fleming as Angelica
*Julie Gayet as Mom
*Denis Podalydès as Dad

==See also==
*Works based on Alice in Wonderland

==References==
 

==External links==
* 

 
 
 
 
 

 