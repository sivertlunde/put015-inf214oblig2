Aanayum Ambaariyum
{{Infobox film
| name           = Aanayum Ambaariyum
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       =
| writer         = MK Mani CP Antony (dialogues)
| screenplay     = CP Antony Ravikumar Sudheer Sudheer
| Shyam
| cinematography = EN Balakrishnan
| editing        = Chakrapani
| studio         = Rose Movies
| distributor    = Rose Movies
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Ravikumar and Sudheer in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Kamalamma
*Leela Ravikumar
*Sudheer Sudheer
*Vincent Vincent

==Soundtrack== Shyam and lyrics was written by Bharanikkavu Sivakumar and Kaniyapuram Ramachandran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hari Om Bhakshanadaayakane || K. J. Yesudas, P Jayachandran, KP Brahmanandan || Bharanikkavu Sivakumar, Kaniyapuram Ramachandran ||
|-
| 2 || Kandanaal Muthal || S Janaki || Bharanikkavu Sivakumar, Kaniyapuram Ramachandran ||
|-
| 3 || Njaan Ninne Kinaavu Kandu || K. J. Yesudas, Chorus || Bharanikkavu Sivakumar, Kaniyapuram Ramachandran ||
|-
| 4 || Vasanthathin Theril || K. J. Yesudas || Bharanikkavu Sivakumar, Kaniyapuram Ramachandran ||
|}

==References==
 

==External links==
*  

 
 
 


 