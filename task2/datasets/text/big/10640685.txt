Mappillai (1989 film)
 
{{Infobox film|
| name = Mappillai
| image = Mappillai (1989 film).jpg
| caption = Film poster Rajasekhar
| writer = Panju Arunachalam
| screenplay = Panju Arunachalam
| story = Geetha Arts Story Department
| starring =  
| producer = Allu Aravind
| music = Ilaiyaraaja
| cinematography = V. Ranga
| editing = M. Vellaisamy
| studio = Geetha Arts
| distributor = Pyramid Movies
| released = 28 October 1989
| runtime =
| country = India
| language = Tamil
| budget =
}}
 Tamil film Rajasekhar starring Amala in Telugu film Athaku Yamudu Ammayiki Mogudu, the Tamil version was produced by Geetha Arts, and Chiranjeevi who played the lead role in the original version made a special appearance. Initially the pivotal role of Rajinikanths mother-in-law was offered to Vyjayanthimala. But she refused the role after which it was offered to Srividya. The film opened to positive response from the public and was a Mega hit Blockbuster movie ran 200 days at the Box-office history of Tamil cinema 890 shows were houseful, all the credits once again goes to superb mass style acting approach of Superstar Rajinikanth.   

==Plot==
Aarumugam makes a blazing entrance, gatecrashing a marriage to abduct the bride. Geetha, the brides friend, gets him arrested but then learns that he had rescued the girl from a forced marriage. Further revelations about him that he is a gold medalist, has a good heart, make her fall in love with him. After some convincing, Aarumugam reciprocates too. Meanwhile, Aarumugams sister is in love with the son of a rich woman Rajarajeswari. When Rajarajeswari learns of this, she foists a false case on her and puts her in jail. It is then that Aarumugam has his first encounter with Rajarajeswari. He then learns that she is none other than Geethas mother. He also invites Geethas mother Rajarajeswari to his marriage. At that time he calls his friend (played by Chiranjeevi) and tells him that during his marriage there are goons to stop this marriage, he requests him to look after the rowdies and Chiranjeevi comes and fights off the goons and attends the marriage and he goes. He marries Geetha against her mothers wishes. Rajarajeswari vows to separate Geetha from him while he vows to prevent that and make Rajarajeswari understand that love and affection are more important than money.Which is an important message delivered to the audience.

The one-on-one confrontations between Rajinikanth and Srividya are the highpoints of the movie and each of these has been handled superlatively. Rajinikanth bristles with energy during their meetings and Srividya is suitably haughty and hits all the right notes. Their first encounter in her house, their private conversation immediately after the wedding and their talk in the garden before the climax in the house all reveal excellent execution by the director.

==Cast==
* Rajinikanth as Aarumugam Amala as Geetha
* Srividya as Rajarajeswari
* Jaishankar
* Nizhalgal Ravi as Ravi
* Vinu Chakravarthy
* S. S. Chandran
* Dubbing Janaki Dilip
* Raja
* Chiranjeevi in a special appearance
* Srihari in a special appearance

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ennathaan Sugamo || S. P. Balasubrahmanyam, S. Janaki || Panju Arunachalam || 04:26
|-
| 2 || Ennoda Raasi || Malaysia Vasudevan || Gangai Amaran || 04:22
|- Vaali (poet)|Vaali || 04:27
|-
| 4 || Unai Thaan Nitham || S. P. Balasubrahmanyam, S. Janaki || 04:17
|-
| 5 || Veru Velai Unakku || S. P. Balasubrahmanyam, S. Janaki || Piraisoodan || 04:30
|}

==References==
 

 
 
 
 
 
 
 