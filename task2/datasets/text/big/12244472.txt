Marma (film)
{{Infobox film
| name           = Marma
| image          =
| image_size     =
| caption        =
| director       = Sunil Kumar Desai
| producer       =
| writer         =
| narrator       =
| starring       = Prema (actress)|Prema, Anand
| music          =
| cinematography = H.C. Venu
| editing        =
| distributor    =
| released       = 2002
| runtime        =
| country        = India
| language       = Kannada
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

Marma ( ) is a 2002 Kannada movie directed by Sunil Kumar Desai. The movie mainly deals with sensitive issues such as schizophrenia and hallucination. It is a story of a woman who suffers from a split personality and post traumatic stress disorder (PTSD).

==Plot details==
The story begins with Sudha (Prema (actress)|Prema) and Anand (Anand) getting engaged. To celebrate, Anand decides to throw a party and promises Sudha he will pick her up. Anand doesnt pick her up and Sudha drives to his estate. She gets stuck in the rain and knocks on the door of a nearby house. There she discovers the body of a girl. Suddenly she senses that she is being attacked. In the struggle, she falls from the roof. While unconscious, she is admitted to a hospital. When she reaches her home, she describes what she experienced that night, but no one believes her. She never recovers from that traumatic experience. Later, she starts a fight with someone in her imagination and tries to kill that person. A psychiatrist fails to bring Sudha out of her mental state. Her fiancée also tries to bring her to a normal condition.

The psychiatrist later believes when Sudha points to a person in a TV program as the killer. He researches a lot and finds the house where Sudha has experienced her frightful night.And he takes her  to that wooden house along with her family

Sudha is clear about what she saw that night. She saw the body of a girl. She has the button of a dress as evidence. When Sudha is taken back to the house, she does not find the body. She finds that the person who attacked her on that night is deaf and blind. She also comes to know the person who is following her and later attacked by her is a relative of the residents of that house and also the girl who died has got engaged to him prior to her death. He explains the reson the behind his following as to describe the truth he knew to their family. Now a big question arises in her mind as the persons whom she saw and attacked both are innocent.

She then finds a tie pin a corner in the house and later identifies it as the one belonging to Anand. When she went to recover the tie-pin at the house she comes face to face with Anand who is already there for his tie-pin. When she confronts him for the murder he explains the reason behind his involvement in this murder. This part of the movie is a flashback.  Anand drives in rain on the dark night hurriedly to pick up Sudha. He hits a school boy in the way with his car and kills him noticeably. When he buries the body a girl saw the incident who was also killed by him unintended. Because of ill fate he was bound to do this. The mystery unfolds as he explains he is the person who joined Sudha in hospital and disappeared the body of the girl.

 
 
 
 
 

 