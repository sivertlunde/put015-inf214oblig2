Rice People
 
{{Infobox film
| name           = Rice People
| image          = Rice People DVD cover.jpg
| caption        = The DVD cover.
| director       = Rithy Panh
| producer       = Jacques Bidou Pierre-Alain Meier
| writer         = Shahnon Ahmad  (novel)  Ève Deboise Rithy Panh
| narrator       = 
| starring       = Peng Phan Mom Soth Chhim Naline
| music          = Jean-Claude Brisson Marc Marder
| cinematography = Jacques Bouquin
| editing        = Andrée Davanture Marie-Christine Rougerie	 	
| distributor    = Facets Video
| released       = May 1994 (France) September 15, 1994 (Canada) June 9, 1995 (UK)
| runtime        = 125 minutes
| country        = Cambodia
| language       = Khmer
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
  1994 Cinema Cambodian drama Kien Svay and Boeung Thom  areas of Kandal Province near Phnom Penh, on the banks of the Mekong River. The cast features both professional and non-professional actors.
 submitted to Best Foreign Language Film.

==Plot== communist Khmer Rouges genocidal bid to transform the country into an agrarian utopia, it is ironic that people have lost touch with the land. For a generation of children, the rice comes not from the ground, but from a sack, offloaded from the back of a United Nations relief truck.

So it is in these uncertain times, that a Cambodian family is attempting to grow rice. The father, Pouev, is concerned that the familys plot of land is shrinking, and he might not be able to grow a big enough crop.

The mother, Om, is worried for her husband, and her worst fears are confirmed when Poeuv steps on a poisonous thorn, and then, after a protracted period of being bedridden, dies of infection.

Om is unable to handle the pressure of being the head of the family, nor does she have the strength to tend to the rice fields. She turns to alcohol and gambling and is eventually locked up for her mental illness.

Responsibility for bringing in the crop and raising her six sisters falls on the oldest girl, Sakha.

==Cast==
* Peng Phan as Om
* Mom Soth as Poeuv
* Chhim Naline as Sakha
* Va Simorn as Sokhoeun
* Sophy Sodany as Sokhon
* Muong Danyda as Sophon
* Pen Sopheary as Sophoeun
* Proum Mary as Sophat
* Sam Kourour as Sopheap

==Release== Pulp Fiction. The film had its North American premiere at the Toronto International Film Festival.
 submitted to Best Foreign Language Film, the first time a Cambodian film had been submitted to the Academy Awards.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 