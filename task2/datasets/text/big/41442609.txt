Sivan (film)
{{Infobox film
| name           = Sivan
| image          = 
| image_size     =
| caption        = 
| director       = Velu Prabhakaran
| producer       = J. Paramasivam Y. J. Sathish
| writer         = Velu Prabhakaran
| starring       =  
| music          = Adithyan
| cinematography = Velu Prabhakaran
| editing        = V. Chidambaram
| distributor    =
| studio         = V. J. Combines
| released       =  
| runtime        = 100 minutes
| country        = India
| language       = Tamil
}}
 1999 Tamil Tamil crime Napoleon and C. Arunpandian in lead roles. The film, produced by J. Paramasivam and Y. J. Sathish, had musical score by Adithyan and was released on 17 May 1999. The film is heavily inspired by the 1987 film RoboCop (1987 film)|RoboCop.  

==Plot==
 Uday Prakash) and his mother died from heart attack. Alex begins by killing Jupiters henchmen and partners one by one.

==Cast==
 Napoleon as Murugan
*C. Arunpandian as Alex
*Raadhika Sarathkumar as Sivagami
*Naga Kannan as Jupiter Swathi as Shenbagam
*Aahana as Alexs wife
*Kalpana as Meenakshi, Murugans sisther
*K. Janaki as Murugans mother
*Padmini
*Idichapuli Selvaraj
*King Kong as Nattu Uday Prakash as Uday
*Min Mini Poochigal Raju
*Kalyan in a guest appearance Rani in a guest appearance
*Alphonsa in a guest appearance

==Soundtrack==

{{Infobox album |  
| Name        = Sivan
| Type        = soundtrack
| Artist      = Adithyan
| Cover       = 
| Released    = 1999
| Recorded    = 1999 Feature film soundtrack |
| Length      = 23:14
| Label       = 
| Producer    = Adithyan
| Reviews     = 
}}
 Vaali and Vaasan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Egypt Nattu Cleopatra || Adithyan, Swarnalatha  || 5:08
|- 2 || Ooh Enn Pennmaiyai || Swarnalatha || 4:57
|- 3 || Pesarat Pesarat || Malgudi Subha || 4:48
|- 4 || Rukkuthan Rukkuthan || Mano (singer)|Mano, Malgudi Subha || 3:51
|- 5 || Uthere Kili || Gopal Sharma, Devi || 4:30
|}

==References==
 

 
 
 
 
 
 


 
 