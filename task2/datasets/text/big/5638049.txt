Braddock: Missing in Action III
{{Infobox film
| name           = Braddock: Missing in Action III
| image          = Missinginaction3.jpg
| image size     = 190px
| caption        = Theatrical release poster
| director       = Aaron Norris
| producer       = Menahem Golan Yoram Globus
| writer         = Arthur Silver Larry Levinson James Bruner Chuck Norris Steve Bing (characters)
| narrator       =
| starring       = Chuck Norris Aki Aleong Yehuda Efroni Roland Harrah III
| music          = Jay Chattaway
| cinematography = João Fernandes (cinematographer)|João Fernandes
| editing        = Ken Bornstein Michael J. Duthie
| distributor    = Cannon Films
| released       = January 22, 1988 (USA)
| runtime        = 101 min.
| country        = United States
| language       = English
| budget         = 
| gross = $6,193,901 (USA)
}}
 Missing in Action series. The film stars Chuck Norris, who co-wrote the screenplay with James Bruner. The film was directed by Norris brother, Aaron Norris.

==Plot==
Colonel James Braddock (Chuck Norris), Vietnam veteran, had believed his Asian wife Lin Tan Cang (Miki Kim) to be dead since the war, but he hears from a missionary, Reverend Polanski (Yehuda Efroni), that Lin is not only alive, but has a 12-year-old son named Van Tan Cang (Roland Harrah III), who is Braddocks son.

At first, Braddock does not believe it, but when cold-blooded CIA boss Littlejohn (Jack Rader) tells Braddock to disregard that information, thats when Braddock knows its true. Braddock heads back into Vietnam through Parachute deployment and with the help of an Australian C-47 pilot. After parachute descent, Braddock outruns Vietnamese Navy Patrol Boats with a Jet-Powered speedboat.

Reverend Polanski leads Braddock to Lin and Van. Attempting to flee the country, Braddock, Lin, and Van are captured by the soldiers of General Quoc (Aki Aleong). Quoc kills Lin on the spot, and has his soldiers take Braddock and Van to a compound to be tortured.

Later, Braddock overpowers his guards, frees Van, and heads for the mission that is run by Polanski. Quoc anticipates the move and takes all the mission children into captivity, along with Van and Polanski, and Braddock sets out to free them all from Quoc by going to his weapons cache that he had hidden a few days prior. He equips himself with a modified Heckler & Koch G3 battle rifle with an underslung 6-shot rotary grenade launcher and attachments including a spring-loaded bayonet. He raids the camp killing the guards and loading up one of the trucks with all the children including his son, Van and the Reverend. Soon after escaping they are followed and attacked by a Vietnamese-captured American UH-1 Huey.

After they escape Braddock takes the children on foot and find a Vietnamese airstrip. Braddock silently takes out the guards and hijacks a C-47 Dakota plane. The plane is then assaulted by Vietnamese guards causing fuel to leak out of the plane, eventually crashing just outside the Cambodian-Thailand border. Braddock then raids the border station where American and Thailand troops are watching on the other side cheering Braddock on. When Braddock kills all the opposing troops more pour in. Braddock is injured by a grenade. When General Quoc then flies in on a Vietnamese Mil-24 Hind gunship thinking he has Braddock all to himself, two American helicopters on the side of the Thai border confront Quocs gunship. Taunting each other to cross, Braddock and his son Van fire at Quocs ship, hitting the pilot. The gunship crashes, killing Quoc. Then all the American troops pour over the border and bridge and help the wounded Braddock and the children.

==Cast==

*Chuck Norris as Col. James Braddock
*Aki Aleong as General Quoc
*Keith David as Embassy Gate Captain
*Roland Harrah III  as Van Tan Cang

==Production==

On May 30, 1987, four military and police officers were killed in an Sikorsky S-76|AUH-76 helicopter of the Philippine Air Force accident during filming in the Philippines. 

==Reception==

===Box office===
Braddock: Missing in Action III was a not a commercial success, and was the last of the Cannon Groups films. The movie debuted at No. 5 at the box office with $2,210,401 in the opening weekend.  It was the least financially successful film in the Missing in Action series.

===Critical reception===
The  movie was met with mixed reactions from critics.   

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 