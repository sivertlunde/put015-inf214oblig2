Royal Blood (film)
 
{{Infobox film
| name           = Royal Blood
| image          = 
| caption        = 
| director       = Will Louis
| producer       = Louis Burstein
| writer         = 
| starring       = Oliver Hardy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 silent comedy film starring Oliver Hardy.

==Plot==
Mrs Vandergrift wants her daughter to marry a count. However, in spite of mothers best efforts, the daughter is in love with Plump. To win over Mrs Vandergrift, Plump and his pal Runt dress up in fancy clothes and pose as members of society. Their efforts to appear distinguished fail miserably and the count arrives on the scene to complicate matters. But in the end, Mrs. Vandergrift changes her mind anyway, and she gives Plump and her daughter her blessings.  , The New York Times, retrieved 31 December 2013 

==Cast==
* Oliver Hardy - Plump (as Babe Hardy)
* Billy Ruge - Runt
* Edna Reynolds - Mrs. Vandergrift
* Ray Godfrey - Her daughter
* Bert Tracy - The Count
* Florence McLaughlin

==See also==
* List of American films of 1916
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 