The Breach (film)
 
{{Infobox film
| name           = The Breach ("La Rupture")
| image          = The Breach 1970.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Claude Chabrol
| producer       = 
| screenplay     = Claude Chabrol
| based on       =  
| narrator       = 
| starring       = Stéphane Audran Jean-Pierre Cassel Michel Bouquet Annie Cordy
| music          = Pierre Jansen   
| cinematography = Jean Rabier
| editing        = Jacques Gaillard
| studio         = 
| distributor    = Gaumont Film Company New Line Cinema 
| released       = 26 August 1970
| runtime        = 111 minutes
| country        = France Italy Belgium
| language       = French
| budget         = 
| gross          = $5,566,068 
| preceded_by    = 
| followed_by    = 
}}
The Breach ( ) is a 1970 film written and directed by Claude Chabrol, based on the novel The Balloon Man by Charlotte Armstrong.  The film was also known as The Breakup at times in its release in the United States.  The film had a total of 927,678 admissions in France.   

==Plot== mentally ill manipulative parents, scapegoat for their sons mental state and decide to take over custody of Michel by any means necessary. While the boy is recovering in a local hospital, Hélène moves to a boarding house nearby. The Régniers hire Paul Thomas (Jean-Pierre Cassel|Cassel), an impoverished family acquaintance, to find out something about Hélène which would help them in their custody battle. Paul moves into the boarding house and, with the help of his girlfriend Sonia (Catherine Rouvel|Rouvel), plots to ruin Hélènes reputation and then possibly kill her.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Stéphane Audran || Hélène Régnier   
|-
| Jean-Pierre Cassel ||  Paul Thomas   
|-
| Michel Bouquet || Ludovic Régnier   
|-
| Annie Cordy || Mme. Pinelli  
|-
| Jean-Claude Drouot ||  Charles Régnier    
|-
| Jean Carmet ||  Henri Pinelli    
|-
| Catherine Rouvel || Sonia
|-
| Claude Chabrol ||  Un passager dans le tramway   
|}

==Critical reception==
Vincent Canby of The New York Times:
 

Dave Kerr of The Chicago Reader:
 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 