Second Marriage Dot Com
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Second Marriage Dot Com
| image          = 
| alt            =  
| caption        = 
| director       = Gaurav Panjwani
| producer       = Vinod Mehta
| story          = Dinkar Sharma Mohit Chauhan Vishal Nayak Charu Rahtogi Sayani Gupta
| music          = 
| art director   = Priyanka Agarwal
| cinematography = 
| editing        = A. Muthu
| studio         = Vin Mehta Films Pvt Ltd.
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}} Mohit Chauhan, Vishal Nayak, Charu Rahtogi and Sayani Gupta in the lead roles. The film is produced by Vinod Mehta under the banner of Vin Mehta Films Pvt Ltd. and was released on August 10, 2012. 

==Plot==
The film kicks off when Akshay, young IT professional from Delhi; only child of a single parent Suneel Narang embarks on a journey to get his father, a widower; married so to finally put an end to his prolonged loneliness. Coincidentally in Jaipur, a vibrant young girl Poonam is on the same hunt to find a partner for her mother, Shoma; whom shes seen as a divorcee since childhood. They get in touch with each other through a matrimonial website named secondmarriage.com and after initial denial of the idea by their parents they finally sail through and get they married.

Poonam and Shoma then join Akshay and Suneel into a new adequate flat in Gurgaon and a distinguished family scenario arises for the four, filled with a new cook hailing from a rustic background, Bihaari and a north-eastern maid Flower. Suneel and Shoma are not at ease with each other to start with but gradually spark of romance start to fly off. Poonam and Akshay whod become good friends from the word go and who themselves are lonely souls with bitter memories; are happy to discover a feeling which always eluded them – the joy of a family. But, little does anyone know the course of destiny and upheaval which it is to bring to their family and which shall germinate from the seed of love!

It’s when Poonam and Akshay find themselves in love with each other after a night of irrepressible emotions which result into an inconceivable event; that the happiness of the four goes for a toss. The story takes sharp turns and explore dark zones of the human psyche before the characters finally square up for an unaccustomed but fairly logical solution to their situation.

==Cast== Mohit Chauhan as Suneel
* Charu Rahtogi as Shoma
* Vishal Nayak as Akshay
* Sayani Gupta as Poonam
* Satish Kaushik
* Akhilendra Mishra
* Ankit Sharma as veeja
* Vijay Raaz
* Murli Sharma
* Amit Mistry
* Mustaq Khan
* Arun Verma
* Rajat Rawail
* Veena Malik Special appearance in song "Channo"
* Anita Mudgal as Seeta
* Pankaj Sharma as Lakshman
* Mohit Baghel as Waiter

==Reception==

===Critical Reception===

===Box Office===

==Soundtrack==
The soundtrack is composed by Aditya Agarwal and Manan Munjal. The music has received generally positive reviews with songs like Manchala, Barsaat,Kamli and Balam receiving positive reviews.Balam is sung by Rekha Bharadwaj.

==References==
 

==External links==
*  

 
 
 