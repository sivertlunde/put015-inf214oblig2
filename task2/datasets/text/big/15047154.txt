The Class (2007 film)
{{Infobox film
| name           = The Class    
| image          = Klass (film) poster.jpg
| caption        = DVD Cover
| director       = Ilmar Raag
| producer       = 
| writer         = Ilmar Raag
| screenplay     = 
| story          = 
| based on       =  
| starring       = Vallo Kirs  Pärt Uusberg  Lauri Pedaja  Paula Solvak
| music          = Martin Kallasvee Paul Oja Timo Steiner
| cinematography = Kristjan-Jaak Nuudi
| editing        = Tambet Tasuja
| studio         = 
| distributor    = Estonian Culture Film
| released       =  
| runtime        = 99 minutes
| country        = Estonia
| language       = Estonian
| budget         = 
| gross          = 
}}
The Class ( ) is an Estonian film about school violence directed by Ilmar Raag. It was released on March 16, 2007. There has also been produced a 7 episode lasting series that tells what happens after the initial movie, entitled Class: Life After ( ).

==Plot== bullied by his entire high school class. The class continually beats Joosep, and harasses him in other ways as well, such as fully undressing him and then pushing him in the girls changing room. His classmate Kaspar tries to protect him and breaks from the group, which results in the violence being directed to him as well. Also, in a homophobic atmosphere Joseph and Kaspar are ridiculed for supposedly having gay feelings for each other. Nevertheless, when the school administration and Jooseps parents try to find out who is to blame for the treatment Joosep has been receiving, the class unanimously but falsely accuses Kaspar of mistreating him.
 fellate Joosep, at knife point, and photograph the sexual act without showing the knife.

The boys decide to avenge themselves. Joosep steals two pistols, a bolt-action rifle and ammunition from his militaristic fathers gun safe, and the two proceed to school. As they proceed, students and teachers notice the guns in their hands and after letting a female emo student (Kerli) go pass them, they begin the massacre on the students responsible for their torment. To their regret, they accidentally shot and killed an eighth grade female student from another class. Finally Joosep and Kaspar, facing one another, each aim a gun at their own head, decide to commit suicide together after counting to three. Joosep pulls the trigger and dies, but the film ends with Kaspar still standing there with his gun aiming at his head.

==Cast==
* Vallo Kirs - Kaspar
* Pärt Uusberg - Joosep
* Lauri Pedaja - Anders
* Paula Solvak - Thea
* Mikk Mägi - Paul
* Riina Ries - Riina
* Joonas Paas - Toomas
* Kadi Metsla - Kati
* Triin Tenso - Kerli
* Virgo Ernits - Tiit
* Karl Sakrits - Olav

==Critical reception==
It currently holds an 89% approval rating among users on Rotten Tomatoes. 

==Awards== submission to the Best Foreign Language Film Category of the 80th Academy Awards. 

==Political invocations==
Since the movie was released, two school shootings have taken place in nearby Finland &mdash; the Jokela school shooting and the Kauhajoki school shooting. In analysis of both events, the movie has been raised as an illustration by columnists and other media pundits. 

==References==
 

==External links==
* 
* 
*  ETV archives (Only in Estonian)

 
 
 
 
 