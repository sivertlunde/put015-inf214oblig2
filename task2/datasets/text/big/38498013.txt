Hard Steel
{{Infobox film
| name           = Hard Steel
| image          =
| image_size     =
| caption        = Norman Walker
| producer       = James B. Sloan
| writer         = Roger Dataller (novel)  Lydia Hayward 
| narrator       = Wilfrid Lawson John Stuart   George Carney
| music          = Percival Mackey
| cinematography = Claude Friese-Greene
| editing        = Sam Simmonds
| studio         = G.H.W. Productions
| distributor    = General Film Distributors
| released       = 18 May 1942
| runtime        = 86 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Norman Walker Wilfrid Lawson, John Stuart. It was based on the novel Steel Saraband by Roger Dataller. The film was one of four made by G.H.W. Productions backed by the Rank Organisation.  The film follows the rise of an ambitious steel worker as he is appointed to run his local steel mill. He soon outrages the employees with his ruthless behaviour - and his negligence leads to the accidental death of one of the workers. As the Second World War breaks out he realises what he has become, and seeks a chance of redemption.

==Cast== Wilfrid Lawson as Walter Haddon 
* Betty Stockfeld as Freda Haddon  John Stuart as Alan Saunders 
* George Carney as Bert Mortimer 
* Joan Kemp-Welch as Janet Mortimer 
* James Harcourt as Jim Calver  Frank Atkinson as Dick Sefton 
* Arthur Hambling as Mr. Lamport  John Turnbull as Mr. Rowlandson 
* Hay Petrie as Mr. Kissack 
* Mignon ODoherty as Mrs. Kissack 
* Philip Godfrey as Forester  Cameron Hall as Flavell 
* Charles Rolfe as Richards 
* Kenneth Griffith as Dixon  Harry Riley as Phillips 
* H. Victor Weske as Bateman  Charles Groves as Old Sam 
* Clifton Boyne as Carter 
* D. J. Williams (actor)|D.J. Williams as Jimpson 
* Leonard Sharp as Baker 
* Roberta Read as Bertha 
* Roddy Hughes as Coroner 
* Arthur Seaton as Newcombe 
* Dane Gordon as Willis 
* David Trickett as Tommy Mortimer 
* Angela Glynne as Bunty Phillips

==References==
 

==Bibliography==
* Murphy, Robert. Realism and Tinsel: Cinema and Society in Britain 1939-48. Routledge, 1992.

==External links==
* 

 
 
 
 
 
 
 
 
 

 

 