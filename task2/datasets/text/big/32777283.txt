Dust in the Sun
 
{{Infobox film
| name           = Dust in the Sun
| image          = 
| image size     = 
| caption        =  Lee Robinson
| producer       = Chips Rafferty Lee Robinson Joy Cavill W. P. Lipscomb
| based on       = Justin Bayard by Jon Cleary
| starring       = Jill Adams Ken Wayne Robert Tudawali
| music          = Wilbur Sampson
| cinematography = Carl Kayser
| editing        = Stanley Moore
| distributor    = Universal (Australia)
| studio         = Southern International Productions
| released       = October 1958 (premiere) August 1960 (Australia) 1960 (England)
| runtime        = 86 mins
| country        = Australia
| language       = English
| budget         = ₤50,000 
}} Lee Robinson and Chips Rafferty.

==Synopsis==
Justin Bayard, a Northern Territory policeman, is escorting an aboriginal warrior, Emu Foot, to Alice Springs to be tried for a tribal killing. They are attacked by some Aborigines and forced to take refuge at an isolated cattle station. Julie, the bored wife of the station owner Tad Kirkbridge, sets Emu Foot free and is later murdered. Bayard romances stockmans daughter Chris. Emu Foot is killed by aboriginals and Bayard exposes Julies murderer.

==Cast==
* Jill Adams as Julie Kirkbride
* Ken Wayne as Justin Bayard
* Maureen Lanagan as Chris Palady
* Robert Tudawali as Emu Foot James Forrest as Tad Kirkbride He Flipped a Coin and Won a Career
Henniger, Paul. Los Angeles Times (1923-Current File)   24 Dec 1965: a17. 
* Jack Hume as Ned Palady
* Henry Murdoch as Spider
* Reg Lye as Dirks
* Alan Light as Inspector Prichett

==Production==
In May 1956 Robinson and Rafferty bought the film studios at Bondi which were once owned by Cinesound Productions. It was meant to be used as a basis for their television company, Australian Television Enterprises, but it was used for this film. 

They optioned Jon Clearys novel Justin Bayard. Robinson later recalled:
 On that film we were aiming to do very well in the English market, because we had always done well there. For instance King of the Coral Sea earned much more than its production cost out of England whilst it earned its production cost in Australia. Walk into Paradise had also gone terribly well in England. England was a very strong market for us at that time. In fact it was probably a better market for us than the United States.   
===Casting=== Lee Robinson and Chips Rafferty but the first one in which Rafferty did not act, although he was originally meant to, with Charles Tingwell to play the second lead, a station manager.
 The Shiralee Walter Brown. 

(At one stage American star John Ericson was sought to play the lead role. )

Jill Adams was imported from England to play the female lead. Maureen Lanagan was a Sydney model making her first film – Robinson also used models turned actors in The Phantom Stockman and King of the Coral Sea. (He often expressed frustration at what he saw was a lack of good looking young women who could act in Australia. )

This was Robert Tudawalis second film role after Jedda. His contract was negotiated by Southern International, Actors Equity and the Department of Native Affairs. 

===Production===
Shooting took place in the studio at Bondi and on location near Alice Springs in October and November 1956. 

Three weeks into filming Robinson and Raffety decided to fire Brown because he seemed "too soft". They offered his role to Tingwell, who declined, and then cast Ken Wayne. Larkin p 113 

==Release==
The film premiered at the Sydney Film Festival in 1958 but was not released in Australia and England until 1960. It did not perform well at the box office. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 226.  According to Raffetys biographer "with television making serious inroads into movie attendances world wide and no Chips Rafferty to exploit for distribution, Dust in the Sun was just another badly made independent cheapie, and gathered its own dust on the shelf for some four years." 

Lee Robinson later said, "I dont think it was a good script and I dont think that we had a very strong supporting cast and it was the first picture that we had done in which Chips didnt play the lead... I think our mistake there was to make a picture not geared for Chips."   

The movie was the first job in the Australian industry for Jill Robb, who was Jill Adams stand in and went on to become a leading producer. 

==References==
 
*Larkin, Bob Chips: The Life and Films of Chips Rafferty, MacMillan 1986

==External links==
* 
*  at National Film and Sound Archive
*  at Oz Movies
 
 

 
 
 
 
 
 
 