Rugma
{{Infobox film
| name = Rugma 
| image = Rugmafilm.jpg
| caption = Promotional Notice
| director = P. G. Viswambharan
| writer = Thoppil Bhasi Menaka Seema Seema
| producer = Cherupushpam Jose Kutty Royal Achankunju  for Royal Movie Makers
| music = MB Sreenivasan
| cinematography = CE Babu 
| editor = G Venkittaraman
| studio         = Royal Movie Makers
| distributor         = Royal Movie Makers
| released =  
| runtime = Malayalam
| budget =
}}
 1983 Cinema directed by Seema and Venu Nagavally in lead roles.   


==Plot==

The film is the story of Rugma (Seema (actress)|Seema) and her struggle to bring up her daughter (Menaka (actress)|Menaka) after the death of her husband (Mammootty).

==Cast==
 Seema - Rugma
*Mammootty
*Venu Nagavally Menaka
*Raghuvaran Shubha 
*Sukumari   Rohini 
*Adoor Bhasi
*T. G. Ravi
*Charuhasan Kunchan
*Alummoodan

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by  and P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ananyaashchinthayantho || K. J. Yesudas ||  ||
|-
| 2 || Ghoraandhakaarathin   ||  || P. Bhaskaran ||
|-
| 3 || Omanathinkal Kidaavo   ||  ||  ||
|-
| 4 || Remember Remember September || K. J. Yesudas, Sujatha Mohan || P. Bhaskaran ||
|-
| 5 || Sankalpa pushpavanam || K. J. Yesudas || P. Bhaskaran ||
|-
| 6 || Sree padmanaabha || K. J. Yesudas || P. Bhaskaran ||
|}

==References==
 

==External links==
 

 
 
 


 