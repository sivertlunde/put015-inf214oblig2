9 Month Stretch
 
{{Infobox film name           = 9 Month Stretch director       = Albert Dupontel producer       = Catherine Bozorgan writer         = Albert Dupontel starring       = Sandrine Kiberlain Albert Dupontel Bouli Lanners Yolande Moreau Jean Dujardin Terry Gilliam Ray Cooper Gaspar Noé Jan Kounen released       =   distributor    = Wild Bunch Distribution runtime        = 82 minutes country        = France language       = French budget         = €7,120,000 gross          = €25,905,367
}} Best Film Best Director Best Actor Best Actress Best Original Screenplay.      

==Plot==
Ariane Felder is pregnant. This is all the more surprising since this examining magistrate is an old-fashioned single person. But even more surprising is the fact that, according to DNA tests, the father is no other than Bob, a criminal prosecuted for atrocious assault and battery. Ariane, who does not remember anything, tries to understand what happened.

==Cast==
* Sandrine Kiberlain as Ariane Felder
* Albert Dupontel as Bob Nolan
* Philippe Uchan as judge de Bernard
* Nicolas Marié as attorney Trolos
* Bouli Lanners as policeman
* Gilles Gaston-Dreyfus as Monsieur De Lime
* Christian Hecq as lieutenant Édouard
* Philippe Duquesne as coroner Toulate
* Michel Fau as the gynecologist
* Yolande Moreau as Bobs mother
* Jean Dujardin as the sign language interpreter
* Terry Gilliam as Charles Meatson, the "Famous Man-Eater"
* Ray Cooper as the CNN journalist
* Gaspar Noé as bald inmate #1
* Jan Kounen as bald inmate #2

==Anecdote== 10e chambre, instants daudience, a documentary by Raymond Depardon in which appears judge Michèle Bernard-Requin who also plays a judge in Neuf mois ferme.

Albert Dupontel first intended to make Neuf mois ferme his first English-language movie, with Emma Thompson playing Ariane Felder.

The name of the coroner Toulate ("too late") character comes from this first intention. 
He chose to name his main character Ariane in reference to Ariadne as this character loses the thread. 
Attorney Trolos name means stutterer in Ancient Greek.

==Awards==

===César Awards 2014|César Awards===

* Best Actress (Sandrine Kiberlain)
* Best Original Screenplay

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 