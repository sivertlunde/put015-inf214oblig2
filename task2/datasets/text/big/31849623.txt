The Blue Angel (1959 film)
 
{{Infobox film
| name           = The Blue Angel
| image	         = The Blue Angel FilmPoster.jpeg
| caption        = Film poster
| director       = Edward Dmytryk Jack Cummings
| writer         = Heinrich Mann
| starring       = Curd Jürgens
| music          = Hugo Friedhofer
| cinematography = Leon Shamroy
| editing        = Jack W. Holmes
| distributor    = 20th Century Fox
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $1.7 million 
| gross = $1.4 million (est. US/ Canada rentals) 
}}
 1930 film about the vicious Lola-Lola and the troubled, aged Professor Rath, who plays into her hands.

==Cast==
* Curd Jürgens as Professor Immanuel Rath (as Curt Jurgens)
* May Britt as Lola-Lola
* Theodore Bikel as Klepert
* John Banner as Principal Harter
* Fabrizio Mioni as Rolf
* Ludwig Stössel as Dr. Fritz Heine
* Wolfe Barzell as Clown
* Ina Anders as Gussie

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 