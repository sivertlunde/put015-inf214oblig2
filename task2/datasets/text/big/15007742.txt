Victory (2009 film)
 
{{Infobox film name           = Victory
|  image          = Victory hindi.jpg
|  caption        = Poster for Victory
|  director       = Ajit Pal Mangat
|  music          = Anu Malik
|  writer         = Ajit Pal Mangat
|  starring       = Harman Baweja   Amrita Rao   Anupam Kher   Gulshan Grover   Several International Cricketers {{Cite news|title=England duo land Bollywood roles| work= BBC|url=http://news.bbc.co.uk/sport1/hi/cricket/7168281.stm
| accessdate=2008-01-02 | date=2 January 2008}} 
| producer        =  Manmohan Shetty   AjitPal Mangat
| cinematography = Attar Singh Saini
|  distributor    =
| country         = India
|  released       =  
|  runtime        = 159 minutes
|  language       = Hindi
|  budget         = 
|  gross          = 
}}
Victory is a 2009 Indian cricket-based sports film starring Harman Baweja, Amrita Rao and Anupam Kher. It is Harman Bawejas second release after his debut film Love Story 2050, which performed very badly at the box office. The movie was filmed in Australia and India, and is said to be one of Bollywoods most expensive movies to date. {{Cite web|title=Harman Bawejas Victory| work= IndiaFM|url=http://movies.indiainfo.com/2007/10/03/0710031744_harman.html
| accessdate=2007-10-03}}   Victory tells the story of a struggling cricketer who defies all odds to realize an almost impossible dream.

==Plot== Indian cricket team one day. His dream eventually comes true when Vijay makes it to the Indian team. His childhood friend Nandini (Amrita Rao) has believed in him since the very first time he spoke the name of cricket and never ever doubted him. However, this happiness is short-lived as Vijay soon loses his place in the team, as well as the respect he has earned, when he lets his new-found fame get the better of him.

Vijay soon discovers how quickly the media and the public can make a villain out of a hero. Unable to bear the shock and humiliation, Ram suffers a brain hemorrhage. Nandini takes the responsibility to care for Ram and take in Vijay. Vijay and Nandini fly to Australia to have surgery on his back. The surgery is successful and Vijay soon realises that Nandini, his childhood friend, could be his life partner. He nurtures his feelings for her but Nandini is still healing after Vijays bad side took over and is finding it hard to trust him and behave like his true friend again. They return to Jaisalmer and Vijay vows to be a better cricketer for his country. He proves himself to his father and Nandini that he can play for his country and his coach relies on him too. He gets selected once again to play in the major international cricket tournament. He injures himself whilst playing but vows to play no matter his health. He wins the cricket tournament and returns to his hometown Jaisalmer, to see his father and Nandini. Vijay and his father reconcile and just when everything is going fine Ram dies in Vijays arms. They hold the funeral and we see Vijay and Nandini comforting each other in the end scene. We assume they get together. And it finishes with Vijay once again playing in a cricket tournament and playing for his country.

==Cast==
{| class="wikitable" border="1" border="1" width="400px"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Harman Baweja || Vijay Shekhawat
|-
| Amrita Rao || Nandini
|-
| Anupam Kher || Ram Shekhawat
|-
| Gulshan Grover || Andy Singh
|-
| Dalip Tahil || Indian Team Coach
|}

==Box office== Raaz 2 and Dev D and failed to do well. With a huge budget it only managed to gross 1 Crore and was declared an disaster by boxoffice-india. It was Harman Bawejas second film which failed to do well at the box office. 

==Production==
Victory has been produced by Manmohan Shetty and AjitPal Mangat under the banner of Walkwater Media and  , and directed by AjitPal Mangat. It was released on 16 January 2009.

===Cameo appearances===
Victory is notable for special appearances by several current and past international cricketers from India, Pakistan, Australia, South Africa, Sri Lanka and New Zealand. This has been done to give the film an authentic look and feel. Some of the cricketers featured include:

* Neel Shah
* Mohinder Amarnath
* Rao Iftikhar Anjum
* Allan Border
* Stuart Clark
* Martin Crowe
* Dilhara Fernando
* Brad Haddin
* Brad Hogg
* Mike Hussey
* Sanath Jayasuriya Dean Jones Simon Jones
* Suresh Raina
* Rohit Sharma
* Dinesh Karthik
* Praveen Kumar
* Brett Lee
* Farveez Maharoof
* Sajid Mahmood
* Shoaib Malik
* Dimitri Mascarenhas
* Ajantha Mendis
* Albie Morkel
* Morné Morkel
* Muttiah Muralitharan
* Ashish Nehra
* André Nel
* Yusuf Pathan
* Ramesh Powar
* Kumar Sangakkara
* Ishant Sharma
* Navjot Singh Sidhu
* Harbhajan Singh Maninder Singh
* RP Singh
* Michael Slater
* Graeme Smith
* Pat Symcox
* Sohail Tanvir
* Chaminda Vaas
* Atul Wassan
* Waqar Younis
* Noor Hashimzay

==References==
 

==External links==
* 
* 
* 
*  at Bollywood Hungama
* 

 
 
 
 
 
 