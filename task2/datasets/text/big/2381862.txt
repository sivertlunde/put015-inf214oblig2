Night of the Comet
{{Infobox film
| name           = Night of the Comet
| image          = NightoftheCometPoster.jpg
| caption        = Original 1984 theatrical poster
| director       = Thom Eberhardt Andrew Lane Wayne Crawford
| writer         = Thom Eberhardt
| starring = {{plainlist|
* Robert Beltran
* Catherine Mary Stewart
* Kelli Maroney
* Sharon Farrell
* Mary Woronov Geoffrey Lewis
}} David Richard Campbell
| cinematography = Arthur Albert
| editing        = Fred Stafford
| studio         = Thomas Coleman and Michael Rosenblatt Productions Film Development Fund
| distributor    = Atlantic Releasing Corporation 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $3 million
| gross          = $14,418,922
}}

Night of the Comet is a 1984 disaster film|disaster-comedy film written and directed by Thom Eberhardt and starring Catherine Mary Stewart, Robert Beltran, and Kelli Maroney. 

The film was voted number 10 in Bloody Disgustings Top 10 Doomsday Horror Films in 2009. 

The film is also noted as one of the first mainstream films to carry the PG-13 rating. 

== Plot ==
The Earth is passing through the tail of a comet, an event which has not occurred in 65 million years, the last time coinciding with the extinction event that wiped out the dinosaurs. On the night of the comets passage (which takes place eleven days before Christmas), large crowds gather outside to watch and celebrate.
 arcade game, all the other scores being hers. She stays after the theater closes to play until DMKs score is removed and have sex with her boyfriend Larry, the theater projectionist, in the steel-lined projection booth. Meanwhile, Reggies 16-year-old sister Samantha "Sam" (Kelli Maroney) argues with their stepmother Doris (Sharon Farrell), who punches her in the face.
 Michael Bowen) goes outside and is killed by a zombie. When Reggie goes looking for Larry, she encounters the zombie. She heads home to find her sister. Fortunately, Sam spent the night in a metal shed, which shielded her from the comets effects.

After figuring out what has happened, they hear a disc jockey and race to the radio station, only to find it was just a recording. They come across another survivor there, Hector Gomez (Robert Beltran), who spent the night in the back of his steel Semi-trailer truck|semi. When Sam talks into the microphone, she is heard by researchers in an underground installation out in the desert. As they listen to Reggie, Sam and Hector debate what to do, the scientists note that the zombies, though less exposed to the comet, will eventually disintegrate into dust themselves. 

Hector leaves to see if any of his family survived, but promises to come back. Reggie and Sam then go shopping at a mall. After a firefight with some zombie stock boys, the girls are taken prisoner, but are saved by a rescue team sent by the scientists.
 euthanizing Sam by injecting her with a sedative that only put her to sleep, she kills the other remaining scientist. When Hector returns, Audrey briefs him on the situation and then gives herself a lethal injection. He and Sam set out to rescue Reggie. 

The researchers had suspected and prepared for the comets effects, but inadvertently left their ventilation system open during the comets passage, allowing the deadly dust to permeate their base. Meanwhile, Reggie has become suspicious, escapes, and discovers that the dying scientists have hunted down and rendered healthy survivors brain dead. They harvest their untainted blood to keep the disease at bay while they search desperately for a cure. Reggie saves a boy and a girl before they are processed, then unplugs the other victims from their life support machines. Hector and Sam get the trio out of the base.

Eventually, rain washes away the red dust, leaving the world in a pristine condition. With Reggie pairing up with Hector and the other two being just kids, Sam feels left out. When she ignores Reggies warning and crosses a deserted street against the still-operating signal light, she is almost run over by a sports car driven by Danny Mason Keener (Marc Poppel), a teenager about her own age. After apologizing, he invites her to go for a ride. As they drive off, the car is shown sporting the initials "DMK" on the vanity plate.

== Cast ==
* Catherine Mary Stewart as Regina "Reggie" Belmont
* Kelli Maroney as Samantha "Sam" Belmont
* Robert Beltran as Hector Gomez
* Sharon Farrell as Doris, Reggie and Sams stepmother
* Mary Woronov as Audrey White Geoffrey Lewis as Dr. Carter, the leader of the think tank
* Peter Fox as Dr. Wilson, one of the researchers
* John Achorn as Oscar Michael Bowen as Larry Dupree
* Devon Ericson as Minder
* Lissa Layng as Davenport
* Janice Kawaye as Sarah, the young rescued girl
* Chance Boyer as Brian, the young rescued boy
* Ivan E. Roth as Willy
* Dick Rude as Stockboy Chris Pedersen as Stockboy
* John Stuart West as Monster Cop
* Alex Brown as Monster in Alley
* Marc Poppel as Danny Mason Keener / DMK
* Timothy Ellison as Kid on base

== Release ==
=== Home media ===
Night of the Comet was released on VHS cassette and CED Videodisc on August 30, 1985, and distributed by CBS/FOX Video.  A second U.S. VHS printing, distributed by Goodtimes Video, was released on August 30, 1990.  The film was officially released on Region 1 DVD on March 6, 2007, and on Region 2 DVD in the U.K. on January 18, 2010. Night of the Comet was released in a Collectors Edition on Blu-ray by Shout! Factory on November 19, 2013.

== Reception ==
Rotten Tomatoes, a review aggregator, gave the film an 82% based on 28 critics reviews. 
 The Day of the Triffids, The Omega Man, Dawn of the Dead and Last Woman on Earth. They concluded "a successful pastiche of numerous science fiction films, executed with an entertaining, tongue-in-cheek flair that compensates for its absence in originality." 

== Soundtrack == vinyl LP Record and Audio Cassette from Macola Records shortly after the movie was released.  The soundtracks "Learn to Love Again", a love duet performed by Amy Holland and Chris Farren, played in the final scene in the movie and in the closing credits. Other songs include "The Whole World is Celebratin" (also performed by Chris Farren), "Lady in Love" by Revolver,  "Strong Heart" by John Townsend , "Trouble" by Skip Adams, "Living on the Edge" by Jocko Marcellino, "Virgin in Love" by Thom Pace, and "Hard Act to Follow" by Diana DeWitt.

== References ==
 

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 