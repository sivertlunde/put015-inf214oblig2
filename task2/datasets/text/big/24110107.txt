Leaving (2009 film)
{{Infobox film name           = Leaving image          = Partirkstposter.jpg director       = Catherine Corsini writer         = Catherine Corsini Gaelle Mace producer       = Stéphane Parthenay Michel Seydoux Fabienne Vonier starring  Sergi López Kristin Scott Thomas  cinematography = Agnès Godard editing        = Simon Jacquet
| released      =   runtime        = 85 minutes country        = France language  French
}}
 French film Sergi López and Kristin Scott Thomas.

== Plot == Sergi López), a Spanish ex-con hired to do the building, meet, their mutual attraction is sudden and violent.
 Cartier watch at a gas station. In their extreme need, Suzanne and Ivan rob Samuels house of its paintings and valuables, but Ivan is arrested when he fences the stolen goods for them. Suzanne tries to convince her husband that she was solely responsible for the burglary, and that she only took what was hers. She offers to do anything to keep Ivan out jail, and her husband tells her that if she comes home, Ivan will be free. At this dilemma, she faints.

Back in their home, Suzanne is distant with her family, yet tolerant of her husbands sexual advances. Soon, though, she takes a rifle and shells from a closet, and kills Samuel as he sleeps. Driving through the night, she is eventually reunited with Ivan at the ruined house in the hills they had once dreamed of restoring. They embrace and she sobs hysterically. In the distance, a police siren can be heard.

== Cast ==
* Kristin Scott Thomas as Suzanne Sergi López as Ivan
* Yvan Attal as Samuel
* Bernard Blancan as Rémi
* Aladin Reibel as Dubreuil
* Alexandre Vidal as David
* Daisy Broom as Marion
* Berta Esquirol as Berta 
* Gérard Lartigau as Lagache 
* Geneviève Casile as Samuels mother 
* Philippe Laudenbach as Samuels father 
* Michèle Ernou as Madame Aubouy

==Reception==
The film has been moderately well received by critics, and currently holds a rating of 69% on Rotten Tomatoes. 

==References==
Notes
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 