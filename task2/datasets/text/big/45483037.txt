Confessions of a Womanizer
{{Infobox film
| name           = Confessions of a Womanizer
| image          = Confessions of a Womanizer film poster.png
| alt            = 
| caption        = Film poster
| director       = Miguel Ali
| producer       =  
| writer         = Miguel Ali
| story          = Miguel Ali
| starring       =  
| music          = Leland Cox 
| cinematography = Erick M. Crespo 
| editing        = Seth Flaum Brad E. Wilhite 
| studio         =  
| distributor    = Bosch Media Harbor House Films 
| released       =  
| runtime        = 106 minutes
| country        = U.S.
| language       = English
| budget         = 
| gross          =  
}}
 Andrew Lawrence and Jillian Rose Reed.

==Story== Andrew Lawrence) is an arrogant guy whose only priority is a new girl every week until he finds himself in a real relationship with Megan (Jillian Rose Reed).    It causes him to confess his womanizing and how it was sparked by a scar caused years ago. His new best friend, Ginger, a transgender prostitute, helps bring these wounds to the surface.

==Cast==
* Gary Busey as Gary
* C. Thomas Howell as Tony Andrew Lawrence as Ritchie
**Andrew Miller and Jakob Miller as Young Ritchie
* Jillian Rose Reed as Megan
* Kelly Mantle as Ginger
* Nosheen Phoenix as Jasmine
* Andrew Caldwell as Matt Nicole Garcia and Brianna Danielson as Erica and Sally Patricia de Leon as Patricia

==Production==
In September 2012, The Hollywood Reporter announced that Jillian Rose Reed would star as Richies love interest.  Filming took place at the Silver Dream Factory Studio in Laguna Hills, California and at the Harbor House Diner in Sunset Beach, California.

==Review==
The film received a 92% audience score on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  

 
 