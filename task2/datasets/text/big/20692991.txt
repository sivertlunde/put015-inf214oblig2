Last Night (2010 film)
{{Infobox film
| name = Last Night
| image = Last Night.jpg
| border = yes
| caption  = Theatrical release poster
| director = Massy Tadjedin Nick Wechsler
| writer = Massy Tadjedin
| starring = Keira Knightley Sam Worthington Eva Mendes Guillaume Canet Stephanie Romanov
| music = Clint Mansell
| cinematography = Peter Deming
| editing = Susan E. Morse Gaumont Miramax Walmark Films
| released =  
| runtime = 92 minutes
| country = France United States
| language = English
| budget =
| gross = $7,743,923
}}

Last Night is a 2010 drama romance film written and directed by Massy Tadjedin. It stars Keira Knightley, Sam Worthington, Eva Mendes and Guillaume Canet. The films official trailer was released on November 6, 2010. It was released both in theaters and video-on-demand on May 6, 2011 in the United States. The movie centers on Joanna (Keira Knightley) and Michael Reed (Sam Worthington), a successful and happy couple. They are moving along in their lives together until Joanna meets Laura (Eva Mendes), the stunningly beautiful work colleague whom Michael never mentioned. While Michael is away with Laura on a business trip, Joanna runs into an old but never quite forgotten love, Alex (Guillaume Canet). As the night progresses and temptation increases, each must confront who they really are. 

==Plot==
Joanna is a writer who is married to Michael, a commercial real estate agent. They live in New York. The film begins with Joanna and Michael attending a party with Michaels colleagues. Joanna notices Michael spending most of his time chatting with the beautiful Laura, one of his colleagues, whom he has never mentioned to her before. She suspects him of having an affair and confronts him when they return home. They argue but reconcile later in the night.

Michael leaves for Philadelphia the next morning with associates Laura and Andy for a business trip. Joanna goes out for coffee and bumps into Alex, an old love. They meet later that day for a drink, eventually ending up having dinner with Sandra and Truman, friends of Alex. In the conversations while having dinner, the film then reveals the history behind Joanna and Alex and the nature of her relationship with Michael. Joanna met Alex four years ago in Paris while she was working on a story. She has never told Michael about Alex.

Joanna and Alex return to friend Andys apartment, where they discuss their previous romance. It is revealed that they were together during the period Joanna and Michael briefly broke off their relationship. Joanna then goes out to walk Andys dog Lucy; Alex forgets the keys when he comes down to greet her, and they are locked outside. They then go to a party where Sandra and Truman are at, with the dog. There, they grow even closer and passionately kiss in the elevator. They return to Alexs hotel room after the party. Joanna refuses to have sex with Alex. Instead, they spend the night together embracing in bed.

Joanna and Alexs night together are interspersed with scenes of Michael and Laura on their business trip. After dinner with their client, Laura invites Michael out for more drinks, which he accepts. Their conversations at the hotel bar reveal that Michael has never cheated on his wife. They then proceed to the hotel pool, where they swim in their underwear. They continue to talk about infidelity, which further tempts Michael. They return to her room and have sex.

The next morning, Michael tries to speak to Laura about what had happened but is unable to. He also discovers a note written by Joanna slipped into his clothes where she confirmed her confidence in his honesty. Fraught with guilt, he leaves Philadelphia early, leaving Laura and Andy alone for the presentation to their clients. Joanna and Alex have a passionate farewell. Alex leaves New York broken-hearted.

Michael comes home early to find Joanna in tears. They make plans for the day, trying to resume their normal lives. They embrace, and Michael tells Joanna he loves her and notices Joannas shoes left over there that were worn the night before. Joanna is puzzled by this sudden show of affection coupled with Michaels early return. Michael then notices that Joanna is wearing her best underwear, which she had not removed after last night. As Joanna prepares to speak, the film cuts to black...

==Cast==
* Keira Knightley as Joanna Reed
* Sam Worthington as Michael Reed
* Eva Mendes as Laura Nunez
* Guillaume Canet as  Alex Mann
* Stephanie Romanov as Sandra
* Griffin Dunne as Truman
* Chriselle Almeida as Chris
* Scott Adsit as Stuart
* Daniel Eric Gold as Andy
* Steve Antonucci as Soho Guy
* Rae Ritke as Barbara
* Cheryl Ann Leaser as Cynthia

==Production==
The film was shot during late 2008 in New York, with large portions filmed in SoHo, Manhattan.

==Reception==
The film currently holds a 50% rating on review aggregation website Rotten Tomatoes out of 56 reviews, with the consensus being: "Last Night doesnt opt for easy answers, but the framework and characters overall are too slight and plain to be compelling." 

The Guardian has given the film three stars out of five. Peter Bradshaw wrote: "This is a talky film, and arguably a little exasperating, but it is targeted at grownups, and addresses the grown-up issue of monogamy: is this institution a natural state which lovers should strive for, or a wholly artificial state created to protect us from the pain and loss that must always follow from acting on our desires? Not a perfect film, but an interesting one." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 