The Yellow Dog (film)
  French crime film directed by Jean Tarride and starring Abel Tarride, Rosine Deréan and Rolla Norman. It is an adaptation of the novel Maigret and the Yellow Dog by the Belgian writer Georges Simenon. Abel Tarride was the directors father.

==Cast==
*  
* Rosine Deréan : Emma
* Rolla Norman : Léon
* Robert Le Vigan : Le docteur Ernest Michoux
* Jacques Henley : Le Pommeret
* Anthony Gildès : Le pharmacien
* Robert Lepers : Linspecteur
* Jean Gobet : Le voyageur de commerce
* Paul Azaïs : Le marin
* Paul Clerget : Le maire
* Fred Marche : Servières
* Jeanne Lory : Lhôtelière

==Bibliography==
* Conway, Kelley. Chanteuse in the City: The Realist Singer in French Film. University of California Press, 2004.

==External links==
* 

 
 
 
 
 
 
 


 