Titus (film)
{{Infobox film
| name           = Titus
| image          = Titus ver1.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Julie Taymor
| producer       = Julie Taymor Conchita Airoldi Jody Allen
| screenplay     = Julie Taymor
| based on       =  
| starring       = Anthony Hopkins Jessica Lange Alan Cumming Colm Feore James Frain Laura Fraser Harry Lennix Angus Macfadyen Matthew Rhys Jonathan Rhys-Meyers 
| music          = Elliot Goldenthal
| cinematography = Luciano Tovoli
| editing        = Françoise Bonnot
| studio         = Overseas Filmgroup Clear Blue Sky Productions
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 162 minutes
| country        = Italy  United States  United Kingdom
| language       = English
| budget         = $20 million
| gross          = $2,007,290
}} fantasy thriller thriller film adaptation of Roman general. Starring Anthony Hopkins and Jessica Lange, it was the first theatrically-released feature film adaptation of the play. The film was made by Overseas Filmgroup and Clear Blue Sky Productions and released by Fox Searchlight Pictures. It was the film directorial debut of Julie Taymor who co-produced and wrote the screenplay. It was produced by Jody Patton, Conchita Airoldi and executive produced by Paul G. Allen.

==Plot== Titus Andronicus, the general at the center of the play, return victorious from war. They bring back as spoils Tamora, Queen of the Goths, her sons, and Aaron the Moors|Moor. Titus sacrifices Tamora’s eldest son, Alarbus, so the spirits of his 21 dead sons might be appeased. Tamora eloquently begged for the life of Alarbus, but Titus refused her plea.

Caesar, the Emperor of Rome, died. His sons Saturninus and Bassianus squabble over who will succeed him. The Tribune of the People, Marcus Andronicus, announces the peoples choice for new emperor is his brother, Titus. He refuses the throne and hands it to the late emperors eldest son Saturninus, much to the latters delight. The new emperor states he will take Lavinia, Titus daughter, as his bride to honor and elevate the family. She is already betrothed to Saturninus brother, Bassianus, who steals her away. Tituss surviving sons aid in the couples run for the Pantheon, where they are to marry. Titus, angry with his sons because in his eyes theyre being disloyal to Rome, kills his son Mutius as he defends the escape. The new emperor, Saturninus, dishonors Titus and marries Tamora instead. Tamora persuades the Emperor to feign forgiveness to Bassianus, Titus and his family and postpone punishment to a later day, thereby revealing her intention to avenge herself on all the Andronici.

During a hunting party the next day, Tamoras lover, Aaron the Moor, meets Tamoras sons Chiron and Demetrius. The two argue over which should take sexual advantage of the newly-wed Lavinia. Aaron easily persuades them to ambush Bassianus and kill him in the presence of Tamora and Lavinia, in order to have their way with her. Lavinia begs Tamora to stop her sons, but Tamora refuses. Chiron and Demetrius throw Bassianuss body in a pit, as Aaron directed them, then take Lavinia away and rape her. To keep her from revealing what she saw and endured, they cut out her tongue as well as her hands, replacing them with tree branches. When Marcus discovers her, he begs her to reveal the identity of her assailants; Lavinia leans towards the camera and opens her bloodied mouth in a silent scream.

Aaron brings Titus sons Martius and Quintus and frames them for the murder of Bassianus with a forged letter outlining their plan to kill him. Angry, the Emperor arrests them. Later on, Marcus takes Lavinia to her father, whos overcome with grief. He and his remaining son Lucius begged for the lives of Martius and Quintus, but the two are found guilty and are marched off to execution. Aaron enters, and tells Titus, Lucius, and Marcus the emperor will spare the prisoners if one of the three sacrifices a hand. Each demands the right to do so. Titus has Aaron cut off his (Tituss) left hand and take it to the emperor. Aarons story is revealed to have been false, as a messenger brings Titus the heads of his sons and his own severed hand. In Renaissance semiotics, the hand is a representation of political and personal agency.  With his hand chopped off, Titus truly lost power.   Desperate for revenge, Titus orders Lucius to flee Rome and raise an army among their former enemy, the Goths.

Tituss grandson (Luciuss son and the boy from the opening), who  helped Titus read to Lavinia, complains she will not leave his books alone. In the book, she indicates to Titus and Marcus the story of Philomela (princess of Athens)|Philomela, in which a similarly mute victim "wrote" the name of her wrongdoer. Marcus gives her a stick to hold with her mouth and stumps. She writes the names of her attackers on the ground. Titus vows revenge. Feigning madness, he ties written prayers for justice to arrows and commands his kinsmen to aim them at the sky so they may reach the gods. Understanding the method in Tituss "madness", Marcus directs the arrows to land inside the palace of Saturninus, who is enraged by this added to the fact Lucius is at the gates of Rome with an army of Goths.

Tamora delivers a mixed-race child, fathered by Aaron. To hide his affair from the Emperor, Aaron kills the nurse and flees with the baby. Lucius, marching on Rome with an army, captures Aaron and threatens to hang the infant. To save the baby, Aaron reveals the entire plot to Lucius, relishing every murder, rape and dismemberment.
 cook them into a pie for their mother.

The next day, during the feast at his house, Lavinia enters the dining room. Titus asks Saturninus whether a father should kill his daughter if she is raped. When the Emperor agrees, Titus snaps Lavinias neck, to the horror of the dinner guests, and tells Saturninus what Tamoras sons did. When Saturninus demands Chiron and Demetrius be brought before him, Titus reveals they were in the pie Tamora enjoyed, and kills Tamora. Saturninus kills Titus after which Lucius kills Saturninus to avenge his fathers death.

Back in the Roman Arena, Lucius tells his familys story to the people and is proclaimed Emperor. He orders Saturninus be given a proper burial, Tamoras body be thrown to the wild beasts, and Aaron be buried chest-deep and left to die of thirst and starvation. Aaron is unrepentant to the end. Young Lucius picks up Aarons child and carries him away into the sunrise.

==Cast== Titus Andronicus, victorious Roman General who declines a nomination for Emperor upon his return to Rome. Following a post-war ritual of executing the proudest warrior of his enemy, the vanquished Goths, Titus draws the ire of Tamora, unaware of her inevitable appointment as the Queen of Rome. prisoner of war, using her newly claimed powers as Queen of Rome.
* Harry Lennix as Aaron, servant and illicit lover to Tamora, and the chief architect of her vengeful plans against the Andronicus family.
* Angus Macfadyen as Lucius, Tituss eldest son and loyal soldier. Following his failed attempts at freeing his condemned brothers, Lucius is banished from Rome and defects to the Goths, where he rallies a sizable army to challenge Saturninus.
* Alan Cumming as Saturninus, brother to Bassianus and newly crowned Emperor of Rome. Spurned by his attempts at claiming Lavinia as his new bride, he forsakes the Andronicus family and turns instead to Tamora as his new bride.
* Laura Fraser as Lavinia, daughter of Titus and fiancee of Bassianus
* James Frain as Bassianus, one of the deceased emperors sons and brother to Saturninus.
* Colm Feore as Marcus Andronicus, Roman Senator and staunch ally of his brother, the ailing Titus.
* Jonathan Rhys-Meyers as Chiron, one of Tamoras sons.
* Matthew Rhys as Demetrius, one of Tamoras sons.
* Kenny Doughty as Quintus, one of Tituss sons.
* Blake Ritson as Mutius, one of Tituss sons. Colin Wells as Martius, one of Tituss sons.
* Osheen Jones as Young Lucius, Tituss grandson and a key-character in the sense that he observes most of the key events.
* Raz Degan as Alarbus, Tamoras eldest son who is sacrificed, setting the events of the play into motion.
* Geraldine McEwan as the Nurse, who brings Aaron his son

==Background and production== anachronistic fantasy world that uses locations, costumes and imagery from many periods of history, including Ancient Rome and Benito Mussolini|Mussolinis Italy, to give the impression of a Roman Empire that survived into the modern era. The opening scenes commence with a heavily choreographed triumphal march of the Roman troops, complete with motorcycle outriders. The selection of music is similarly diverse. 

Apart from the deliberate anachronisms, the film follows the play quite closely. One of the experimental concepts in the film was that the character of Young Lucius (Titus grandson) is initially introduced as a boy from the present who finds himself transported to the fantastical reality of the film. At the beginning of the film his toy soldiers turn into Titus Roman army. At the end, when Titus son Lucius avenges his father by condemning the villainous Aaron to a painful death, the boy takes pity on Aarons infant son, carrying him away from the violence as he walks slowly into the sunrise. This ending is perhaps more positive than the ending of other productions of the play, including Taymors stage production, in which Young Lucius is fixated upon the "tiny black coffin" holding the dead infant. 
 Arena in aqueduct in Rome, and the streets where he rounds up his conspirators are the Roman Ghetto. 

==Soundtrack==
 
The score to the film was created by Taymors long-time friend and partner Elliot Goldenthal and is a typical Goldenthal soundtrack with an epic, inventive and dissonant feel.

==Reception==

===Box office=== box office lackbuster  , earning only $22,313 on its opening weekend due to the limited release in only 2 theaters. Its widest release in the US was only in 35 theaters, resulting the film to end with only $2,007,290 in North America.

===Critical response===
Titus received a mixed to positive response from critics. It holds a 57% average on Metacritic based on reviews from 29 critics indicating "Mixed or average reviews"  and a 68% on Rotten Tomatoes based on 74 reviews with the consensus, "The movie stretches too long to be entertaining despite a strong cast." 

Stephen Holden of the New York Times gave the film a positive review, making it a "Critics Pick".   Roger Ebert of the Chicago Sun-Times also praised Titus and gave it three and a half out of four stars, referring to the source material as "the least of Shakespeares tragedies" and concluding, "Anyone who doesnt enjoy this film for what it is must explain: How could it be more? This is the film Shakespeares play deserves, and perhaps even a little more." 

==See also==
*List of William Shakespeare screen adaptations
* Box office bomb
* Rape and revenge film

==References==
 

==External links==
*  
*  
*  
*  
*  

*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 