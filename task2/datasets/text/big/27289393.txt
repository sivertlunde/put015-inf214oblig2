Vintage Wine
{{Infobox film
| name           = Vintage Wine
| image          = 
| image_size     = 
| caption        =  Henry Edwards
| producer       = Julius Hagen
| writer         = Ashley Dukes   Seymour Hicks   H. Fowler Mear
| narrator       = 
| starring       = Seymour Hicks Claire Luce Eva Moore Judy Gunn
| music          = W. L. Trytel
| cinematography = Sydney Blythe
| editing        = Baynham Honri   Ralph Kemplen
| studio         = Twickenham Studios
| distributor    = Gaumont British Distributors
| released       = 20 June 1935
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Henry Edwards and starring Seymour Hicks, Claire Luce, Eva Moore and Judy Gunn.  The film was made at Julius Hagens Twickenham Studios, but was released by Gaumont British Distributors which was the largest British film company at the time. The film was loosely based on a German play by Alexander Engels.

==Synopsis== mistress in Rome and generally living a wild life. Unbeknownst to them he has happily re-married and had a son with a much younger woman. She believes he is twenty years younger than he really is and is shocked when his relatives including his mother, grown-up sons and granddaughter arrive in Italy.

==Cast==
* Seymour Hicks as Charles Popinot
* Claire Luce as Nina Popinot
* Eva Moore as Josephine Popinot
* Judy Gunn as Blanche Popinot
* Miles Malleson as Henri Popinot
* Kynaston Reeves as Benedict Popinot
* Michael Shepley as Richard Emsley
* A. Bromley Davenport as Pierre
* Amy Brandon Thomas as Minor role
* Elisabeth Croft as Minor role
* Kathleen Weston as Family Member.    

==References==
 

==Bibliography==
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 