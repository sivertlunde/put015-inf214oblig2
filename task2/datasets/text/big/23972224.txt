The Sundowners (1950 film)
 Sundowner}}

{{Infobox film
| name           = The Sundowners
| image          =
| image_size     =
| caption        =
| director       = George Templeton
| producer       = Alan Le May George Templeton
| writer         = Alan Le May (screenplay and story "Thunder in the Dust")
| narrator       =
| starring       = See below
| music          = Alberto Colombo Leonid Raab Rudy Schrager
| cinematography = Winton C. Hoch
| editing        = Jack Ogilvie
| distributor    =
| released       = 2 February 1950
| runtime        = 83 minutes
| country        = USA English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Sundowners is a 1950 American film directed by George Templeton. The film is also known as Thunder in the Dust in the United Kingdom.

== Plot summary ==
A rash of cattle rustling leads to a range war, centered around the disputed grazing rights to a fertile canyon. Hostilities escalate with the arrival of a gunman, who disrupts both sides in the conflict.

== Cast == Robert Preston as James Cloud (Kid Wichita)
*Robert Sterling as Tom Cloud
*Chill Wills as Sam Beers
*Cathy Downs as Kathleen Boyce
*John Litel as John Gall
*Jack Elam as Earl Boyce
*Don Haggerty as Sheriff Elmer Gall
*Stanley Price as Steve Fletcher
*Clem Fuller as Turkey
*Frank Cordell as Jim Strake
*Dave Kashner as Gill Bassen (The Whip)
*John Drew Barrymore as Jeff Cloud - the Younger Brother

== Soundtrack ==
*"ORiley" (Written by Alberto Colombo)

== External links ==
*  
* 
* 
 

 
 
 
 
 
 
 


 