Chakravarthy (1991 film)
{{Infobox film 
| name           = Chakravarthy
| image          =  
| caption        = 
| director       = A Sreekumar
| producer       = Pappachan
| writer         = A Sreekumar NK Sasidharan (dialogues)
| screenplay     = A Sreekumar
| starring       = Sukumari Jagathy Sreekumar Suresh Gopi Manoj K Jayan
| music          = PC Susi
| cinematography = BR Ramakrishna
| editing        = MV Natarajan
| studio         = Zion Movies
| distributor    = Zion Movies
| released       =  
| country        = India Malayalam
}}
 1991 Cinema Indian Malayalam Malayalam film, directed by A Sreekumar and produced by Pappachan. The film stars Sukumari, Jagathy Sreekumar, Suresh Gopi and Manoj K Jayan in lead roles. The film had musical score by PC Susi.   

==Cast==
 
*Sukumari
*Jagathy Sreekumar
*Suresh Gopi
*Manoj K Jayan
*Captain Raju
*Rajan P Dev
*Sukumaran
*Babu Antony
*Jagannatha Varma
*KPAC Sunny Kasthuri
*Santhi Moorthy
 

==Soundtrack==
The music was composed by PC Susi and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Meshavilakkinte nertha || K. J. Yesudas || Bichu Thirumala || 
|-
| 2 || Pon Kolaraalappantam || K. J. Yesudas || Bichu Thirumala || 
|-
| 3 || Thoomanju Peyyum Raakenthu Kando || K. J. Yesudas, KS Chithra || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 