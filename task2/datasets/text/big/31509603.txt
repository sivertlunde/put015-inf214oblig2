Betrayal (1929 film)
{{Infobox film
| name           = Betrayal
| image          = Betrayal Lobby Card 1929.jpg
| alt            = 
| caption        = Lobby card
| director       = Lewis Milestone
| producer       = {{Plainlist|
* Lewis Milestone
* David O. Selznick
}}
| writer         = {{Plainlist|
* Hanns Kräly
* Julian Johnson  
}}
| story          = {{Plainlist|
* Victor Schertzinger
* Nicholas Soussanin
}}
| starring       = {{Plainlist|
* Emil Jannings
* Esther Ralston
* Gary Cooper
}}
| music          = {{Plainlist|
* Louis De Francesco
* J.S. Zamecnik
}}
| cinematography = Henry W. Gerrard
| editing        = Del Andrews
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes 8 reels, 6,614 ft
| country        = United States
| language       = Silent, English intertitles
| budget         = 
| gross          = 
}}
 1929 drama film produced for Famous Players-Lasky and released by Paramount Pictures.     The film is the last silent film directed by Lewis Milestone,  the last silent performance by Gary Cooper,    the last silent performance by Germanys Emil Jannings,   and the only onscreen pairing of Cooper and Jannings.   It is considered a lost film. 

==Background==
Filmed on locations near Lake Tahoe, the film was intended to be a "part-talkie" and incorporated talking sequences, synchronized music, and sound effects,     but because of Janningss heavy German accent and the poor recording of Ralstons voice, it was released as a silent. 

==Plot== Swiss peasant Viennese artist Andre Frey (Gary Cooper).  When Andre later returns to Switzerland, he learns that Vroni has been forced to marry wealthy burgomeister Poldi Moser (Emil Jannings).  Explaining Andres appearance, Vroni introduces him as a young man who has just lost his sweetheart, and in sympathy, Poldi invites Andre to be a guest in his house.  

Several times over the next few years Andre visits, during which time Poldi and Vroni have two children.  Andre is overwrought by his repressed feelings toward Vroni, and after seven years,  begs her to run off with him.  She refuses, but agrees to one last tryst.  While speeding down a dangerous run on a toboggan together, Vroni is killed and Andre fatally injured. Poldi learns the truth of the relationship while attending Vronis funeral, and swears vengeance but discovers that Andre has died from the severity of his injuries.

==Cast==
* Emil Jannings as Poldi Moser 
* Esther Ralston as Vroni 
* Gary Cooper as Andre Frey 
* Douglas Haig as Peter 
* Ann Brody 
* Jada Weller as Hans 
* Bodil Rosing as Andres Mother  
* Paul Guertzman 
* Leone Lane 

==References==
 

==External links==
*   at the Internet Movie Database
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 