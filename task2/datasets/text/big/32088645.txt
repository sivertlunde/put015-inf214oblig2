Baazi (1984 film)
{{Infobox film
| name           =  Baazi
| image          =  Baazi, film poster.jpg
| caption        =
| director       = Raj N. Sippy
| producer       = K K Talwar
| writer         = 
| starring       = Dharmendra Rekha Mithun Chakraborty Ranjeeta Shakti Kapoor Madan Puri
| music          = Laxmikant-Pyarelal
| cinematography = Anwar Siraj
| editing        = 
| studio         =
| distributor    =
| released       =  
| runtime        = 125 minutes
| country        = India
| language       = Hindi Rs 2.2 Crores
| gross          = 
}}
Baazi  ( : بازی) is a 1984 Bollywood film directed by Raj N. Sippy.  The film stars Dharmendra, Rekha, Mithun Chakraborty, Ranjeeta, Shakti Kapoor, Madan Puri and Mac Mohan.

==Plot==
Ajay Sharma (Dharmendra), is a Police Inspector of notable wealth. He is happily married to Asha (Rekha) with two children. Many a times he goes out of his work area to destructively capture dangerous criminals and inadvertedly falls under disciplinary warning with his concerned superiors. After repeated warnings he is discharged from the Police force. He decides to go back to his boyhood town by joining his father (Madan Puri) in the family business. They are living happily. However a notable Criminal going by the name of Rocky (Shakti Kapoor) and his uncle (Pinchoo Kapoor) trace Ajay to his home town and decide to set up a casino. Past its grand opening, Ajay and his friend Albert (Jalal Agha) visit the casino and fall under a confrontation with the gamekeeper and management when they are cheated during the game. During the fight, Albert is killed and Ajay is seriously wounded. 

In a parallel storyline, a young man named Salim (Mithun) seeks employment in the town and beings to court a woman named Noora (Ranjeeta), a dabbewali. Rocky is impressed with Salims strength and hires him as his henchman. When Ajay comes to the casino to avenge Alberts death, Ajay & Salim become embroiled in a serious a fight. Rocky accidentally injures Salim, however insists on diverting the blame to Ajay. When Salim sees ajays cut, he realises that it is Rocky who has cut both Ajay and himself. Ajay & salim join hands and fight the goons. The goons while trying to shoot Ajay kill Asha instead. Ajay & Salim avenge ashas death by killing Rocky & the Chairman (Pinchoo Kapoor) who is their leader.

==Cast==
*Dharmendra	... 	Police Inspector Ajay Sharma
*Rekha	... 	Mrs. Asha Sharma
*Mithun Chakraborty	... 	Salim Khan
*Ranjeeta	... 	Noora
*Shakti Kapoor	... 	Rocky
*Madan Puri	... 	Durgaprasad Sharma
*Mac Mohan	... 	Rockys accomplice

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mere Sathi Jeevan Sathi"
| Lata Mangeshkar, Shabbir Kumar
|-
| 2
| "Kaise Pyar Hota Hai"
| | Asha Bhosle
|-
| 3
| "Noora De De"
| Kishore Kumar, Asha Bhosle
|-
| 4
| "Thodisi Khusi Hai Thodasa Hai"
| Kishore Kumar, Asha Bhosle, Shivangi Kolhapure & Baby Rajeshwari
|-
| 5
| "Gupchup Hai Mummy Goomsoom Hai Daddy"
| Kishore Kumar, Asha Bhosle, Shivangi Kolhapure & Baby Rajeshwari
|-
| 6
| "Mere Sathi Jeevan Sathi (Party)"
| Lata Mangeskar, Shabbir Kumar
|}

== References ==
 

== External links ==
*  

 
 
 
 


 