Bruce Lee: The Curse of the Dragon
{{Infobox film
| name           = Bruce Lee: The Curse of the Dragon
| image          = BruceLeeCurseoftheDragon.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Fred Weintraub Tom Khun
| producer       = Tom Khun Fred Weintraub
| writer         = Davis Miller
| screenplay     = 
| story          = 
| based on       = 
| narrator       = George Takei
| starring       = Bruce Lee Jackie Chan Brandon Lee Linda Lee Cadwell
| music          = David Wheatley Fred Wintraub
| cinematography = John Axelson
| editing        = David Klandrud
| studio         = 
| distributor    = Warner Brothers
| released       =  
| runtime        = 90 minutes United States English
| budget         = 
| gross          = 
}}
Bruce Lee: The Curse of the Dragon is a 1993 documentary film about Bruce Lee. The film includes interviews from some of his fellow students and opponents who worked alongside him in his movies. The film is directed by Tom Kuhn and Fred Weintraub and written by Davis Miller, author of the books My Dinner with Ali and The Tao of Bruce Lee.

==Interviews==
Curse of the Dragon contains interviews from the following people:
*Chuck Norris, who trained with Lee and starred in Way of the Dragon|Way of the Dragon as Lees final opponent.

*Dan Inosanto, a student of Bruce Lee who appeared in Game of Death|Game of Death as an opponent of Lee.

*Kareem Abdul-Jabbar, a professional basketball player and student of Lee who also appeared in Game of Death as an opponent.

*Robert Wall, a student of Chuck Norris who became friends with Lee after meeting him at a Kung-Fu demonstration in San Francisco. 
 Robert Baker, who starred as Lees final opponent in Fist of Fury|Fist of Fury. Baker was a student of Lees.  

*Taky Kimura, a student of Lee who was slated to play the second floor guardian in Game of Death|Game of Death. The scene was never shot.

*Brandon Lee, the son of Lee, who would die only a year later while filming The Crow|The Crow.

*Linda Lee Cadwell, the wife of Bruce Lee.

==Release==
In 2000,   was released, containing lost footage of Lees Game of Death. DVDs of Curse of the Dragon were released with A Warriors Journey as a bonus disc after its release. A Warriors Journey  may also be found as a bonus disc on the 2004 special edition of Enter the Dragon.

==Other Similar Documentaries==
*Bruce Lee: The Legend
*The Legend of Bruce Lee
*Bruce Lee: In His Own Words
*Death by Misadventure
*Path of the Dragon
*Intercepting the Fist
*Martial Arts Maser
*The Unbeatable Bruce Lee
*The Man and the Myth/Legend

==References==
 

==External links==
*  
*  
*  
*  

 
 