Hearts of the West
{{Infobox film
| name           = Hearts of the West
| image          =
| image_size     =
| caption        =
| director       = Howard Zieff
| producer       = Tony Bill Rob Thompson
| narrator       =
| starring       = Jeff Bridges Andy Griffith Donald Pleasence Blythe Danner Alan Arkin
| music          = Ken Lauber
| cinematography = Mario Tosi
| editing        = Edward Warschilka
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

Hearts of the West, released in Europe as Hollywood Cowboy, is a 1975 comedy film directed by Howard Zieff, and starring Jeff Bridges, Andy Griffith, Blythe Danner, and Alan Arkin. The story revolves around a wannabe 1930s writer who finds himself cast as a leading man in several B-movie Western (genre)|westerns.

Despite good reviews, the film was not a hit upon release in 1975 but in years since, it has developed a significant cult following among midnight showings and college campuses. 
 Rob Thompson launched his career with this film. He went on to be a major creative talent on the television series Northern Exposure (for which he won an Emmy) and Monk (TV series)|Monk.

==Plot==
 
Lewis Tater (Jeff Bridges), a 1930s-era aspiring novelist who harbors dreams of becoming the next Zane Grey, decides to leave his family home in Iowa to go to the University of Titan in Nevada so he can soak up the western atmosphere. He arrives to find that there is no university, only a mail order correspondence course scam run by two crooks out of the local hotel. He tries to spend the night at the hotel, and is attacked by one of the men. He escapes his attacker and steals their car, pulling over when it runs out of gas. 

He wanders through the desert and happens upon a threadbare film-unit grinding out "B" westerns called Tumbleweed Productions. He catches a lift with the cowboy actors to Los Angeles. After applying at Tumbleweed, he is referred by crusty old extra Howard Pike (Andy Griffith) to the Rio, a western-themed restaurant. While washing dishes at the Rio, he is called by Tumbleweed, where Howard mentors him to be an actor. After proving himself as a stuntman, unit manager Kessler (Alan Arkin) offers him a speaking role. Tater then falls in love with spunky script girl Miss Trout (Blythe Danner). Meanwhile, the crooks trace him to Los Angeles to retrieve the safe-box containing their money that was in the car stolen by Lewis.

==Cast==
* Jeff Bridges as Lewis Tater aka Neddy Wales
* Andy Griffith as Howard Pike aka Billy Pueblo
* Donald Pleasence as A.J. Neitz
* Blythe Danner as Miss Trout
* Alan Arkin as Bert Kessler
* Richard B. Shull as Stout Crook
* Herb Edelman as Polo (as Herbert Edelman)
* Alex Rocco as Earl
* Frank Cady as Pa Tater Anthony James as Lean Crook
* Burton Gilliam as Lester Matt Clark as Jackson
* Candice Azzara as Waitress (as Candy Azzara)
* Thayer David as Bank Manager
* Marie Windsor as Hotel Manager Anthony Holland as Guest at Beach Party
* Dub Taylor as Ticket Agent
* William Christopher as Bank Teller
* Stuart Nisbet as Lucky
* Tucker Smith as Noodle in Pith Helmet
* Richard Stahl as Barber
* Granville Van Dusen as WWI Pilot

==Reception==
Roger Ebert called it "a lovely little comedy, a movie to feel fond of" and that Bridges "brings a nice complexity to the role". {{cite web |url=http://www.rogerebert.com/reviews/hearts-of-west-1975 
|title=Hearts of West   |author=Roger Ebert |date=January 1, 1975 |accessdate=March 22, 2015}} 

==Awards==
It was named one of the   for 1975.  Arkin won the New York Film Critics Circle Award for Best Supporting Actor. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 