You and I (1938 film)
{{Infobox film
| name = You and I
| image =
| image_size =
| caption =
| director = Wolfgang Liebeneiner
| producer = Walter Tost
| writer =  Eberhard Frowein (novel)   Curt J. Braun 
| narrator =
| starring = Brigitte Horney   Joachim Gottschalk   Paul Bildt   Heinz Welzel
| music = Wolfgang Zeller  
| cinematography = Bruno Mondi  
| editing =  Walter von Bonhorst     
| studio = Minerva-Tonfilm 
| distributor = Terra Film
| released = 14 September 1938 
| runtime = 106 minutes
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
You and I (German:Du und ich) is a 1938 German romance film directed by Wolfgang Liebeneiner and starring Brigitte Horney, Joachim Gottschalk and Paul Bildt. 

==Cast==
* Brigitte Horney as Anna Uhlig  
* Joachim Gottschalk as Johann Uhlig  
* Paul Bildt as Balke  
* Heinz Welzel as Otz Uhlig  
* Werner Schott as Schütz  
* Elsa Wagner as Direktrice bei Schütz 
* Karl Hannemann as Merkner  
* André Saint-Germain as Forescu  
* Just Scheu as Pulvermacher  
* Eduard Wenck as Zirbel  
* Arthur Fritz Eugens as Otz Uhlig als Kind 
* Yvonne Klußmann as Friedel Schütz als Kind  
* Cordula Grun as Friedel Schütz  
* Falk May as Kurt Schütz als Kind  
* Fritz Kösling as Kurt Schütz  
* Elisabeth Eygk as Frau Pulvermacher  
* Borwin Walth as Selbter  
* Richard Weimer as Tschirwitz  
* Otto Below as Unger  
* Marga Eckardt as Hulda  
* Else Ehser as Nettie  
* Katja Bennefeld as Kundin bei Schütz  
* Annemarie Korff as Dame bei Ball  
* Ruth Claus as Verkäuferin bei Schütz  
* Charlotte Witthauer as Minna  
* Gerhard Dammann as Gerichtsvollzieher 
* Karl Fochler as Strumpfwirker  
* Günther Hadank as Pfarrer  
* Jochen Kuhlmey as Gast beim Postwirt  
* Wilhelm P. Krüger as Schrankenwärter  
* Leopold von Ledebur as Oberregierungsrat  
* Paul Luka as Strumpfwirker  
* Karl Morvilius as Sekretär bei Merkner  
* Hans Meyer-Hanno as Hauswirt Schröder 
* Albert Venohr as Postwirt 
* Herbert Weissbach as Mann, der die Kriegsgefahr verkündet
* Walter Werner as Helling   Karl Vogt as Rechtsanwalt  
* Hella Gantzert as Verkäuferin bei Schütz  
* Walter Albrecht as Vorsitzender 
* Elisabeth Bechtel as Küstersfrau 
* Fritz Eberth as Matrose  
* Dr. Prasch as Oberregierungsrat  
* Ernst G. Schiffner as Kaufmann   Richard Ulrich as Ministerialrat 
* Alf von Sievers as Ingenieur Beier  
* Ellen Plessow
* Wera Schultz
* Rudolf Essek
* Kurt Felden Paul May
* Karl Platen
* Ellen Köhler   
* Käthe Kamossa 
* Gerda Schaefer 
* Alfred Pussert

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 


 