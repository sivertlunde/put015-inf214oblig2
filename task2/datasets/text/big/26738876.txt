Nutcracker: The Motion Picture
{{Infobox film
| name           = Nutcracker: The Motion Picture
| image          = Nutcracker the motion picture theatrical poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Carroll Ballard Peter Locke Willard Carroll Thomas L. Wilhite
| writer         =
| based on       =   and  
| narrator       = Julie Harris
| starring       =
| music          = Tchaikovsky
| production_design = Maurice Sendak
| cinematography = Stephen H. Burum
| editing        = John Nutt Michael Silvers
| studio         = Pacific Northwest Ballet Hyperion Pictures The Kushner-Locke Company|Kushner-Locke
| distributor    = Atlantic Releasing Corporation
| released       =  
| runtime        = 89 mins.
| country        = United States
| language       = English
| budget         =
| gross          = $781,727 
| website        =
| amg_id         =
}}
Nutcracker: The Motion Picture (a.k.a. Pacific Northwest Ballets Nutcracker or Nutcracker), is a 1986 Christmas film produced by Pacific Northwest Ballet in associates with Hyperion Pictures and The Kushner-Locke Company|Kushner/Locke. It is a film adaptation of the ballet The Nutcracker by Pyotr Ilyich Tchaikovsky as well as based on the short story by E.T.A. Hoffmann.

==Plot==
The plot in this version is considerably darker than in most productions of the ballet. The film begins with all the clocks ticking and tocking and Herr Drosselmeyer in his toyworkshop. Suddenly getting an idea, he begins work on an intricate mechanical project.  After it is apparently completed and he falls asleep from all the hard work, the stage opens revealing the scene of Claras bedroom, where she dreams of dancing with a prince, which is interrupted by her younger brother Fritz, with whom she gets into a pillow fight.  Her hand is then bitten by a rat summoned by Fritz. This causes her own face to become grotesque and she awakens in terror. Her face is normal; it was all a dream. But when she goes to the Christmas party and sees Fritz playing with a hand puppet rat that strongly resembles the one in the dream, she becomes very uneasy.
 masquerade dancers, but Clara, in this version of the ballet, is noticeably uncomfortable around Drosselmeyer, who seems to be leering at her. Suddenly, an object drops off the Christmas tree; it is a Nutcracker. Clara, who has found the Nutcracker, dances happily around the room, but Fritz snatches it away and damages it with a toy sword, which makes her cry. Luckily, he is stopped by his father and to make her feel better, Herr Drosselmeyer mends the Nutcracker with a handkerchief. After everybody dances, the guests depart the party, Clara and Fritz are sent off to bed and Claras parents go upstairs.

Near midnight, Clara finds her Nutcracker and puts him on the shelf. And as the clock strikes 12, the Christmas tree gets bigger and all the toy soldiers, as well as the Nutcracker, come to life and battle the mice. As the Mouse King appears through a hole in the floor, he grows to giant-size and his head multiplies until he has seven heads. When the mice overpower the soldiers and the Nutcracker himself is threatened, Clara takes off her slipper, which glows, and throws it at the multi-headed Mouse King, causing his body to shrink back to mouse-size and become one-headed again. What remains of the giant Mouse King is his coat and his crown. The Nutcracker crawls in the sleeve after the fleeing mouse and Clara follows him, becoming an adult as she wanders through the coats passageways.  She emerges from the coat onto a wintry pavilion, where she finds the Nutcracker transformed into a handsome prince. They dance romantically, and as they depart the snow falls and the snow fairies appear to dance the "Waltz of the Snowflakes".

Across the sea, Clara and the Prince sail to a castle where they are welcomed by the Princes Royal Court. There, the Prince and the jealous, one-eyed Pasha, who strongly resembles Drosselmeyer, develop a rivalry over Clara. Under the Pashas direction, the members of the court perform the famous Act II divertissements (Trepak, Chinese Dance, Arabian Dance, Dance of the Toy Flutes, etc.) as well as the Waltz of the Flowers, and Clara performs the Dance of the Sugar Plum Fairy. She and the Prince dance a romantic Pas de Deux. At the end, she and the Prince, locked in each others arms, are magically levitated by the Pasha after bidding farewell to the Court. Suddenly the Pasha waves his hand, and Clara and her Prince are separated and begin to free-fall. Before they can hit the ground, the Prince turns back into a Nutcracker and Clara (a young girl again) is jolted awake from what has turned out to be a dream.

==Themes== sexual awakening, as she approaches adolescence; similar themes occur in many of Sendaks books.  The film especially emphasizes the darker aspects of Hoffmanns original story and the significance of dreams and the imagination.  The cinematography, by making considerable use of closeups and medium shots, attempts to bring viewers closer to the psychology of the main characters. 

==Production==
===Stage production===
Pyotr Ilyich Tchaikovskys popular 1892 ballet The Nutcracker is derived from E. T. A. Hoffmanns 1816 story The Nutcracker and the Mouse King. The ballets scenario, crafted by Ivan Vsevolozhsky and Marius Petipa after a French adaptation by Alexandre Dumas, is far simpler and less nuanced than Hoffmanns original story. Vsevolozhsky and Petipa entirely omitted the Nutcracker characters complex backstory, "The Story of the Hard Nut," and expanded a short, satiric passage set in a Kingdom of Sweets to cover the whole of Act II. The ballets scenario also introduced smaller changes, such as changing the heroines name from Marie to Clara.   

Choreographer Kent Stowell, the artistic director of the Pacific Northwest Ballet (PNB), first invited the author-illustrator Maurice Sendak to collaborate on a Nutcracker production in 1979,  after Stowells wife and colleague Francia Russell saw a Sendak-designed performance of Mozarts The Magic Flute in Houston.  Sendak initially rejected Stowells invitation,  later explaining:

 

Stowell gave up for the moment, but contacted Sendak the following year, asking him to reconsider and suggesting that they "start from scratch." They began collaborating in earnest in 1981, developing a Nutcracker concept different from traditional productions, and closer to the themes in Hoffmanns original story. 

PNBs Stowell/Sendak Nutcracker premiered in Seattle on December 13, 1983. It was a critical and popular success.  Lincoln Kirstein, director of the New York City Ballet, called Sendaks production design "absolutely magnificent, and I was filled with a violent greed and envy."  PNB continued to perform the Stowell/Sendak production annually for 31 years, ending in 2014; its artistic director, Peter Boal, announced that the 2015 production would use George Balanchines choreography, with new designs by the illustrator Ian Falconer.   

===Adaptation for film=== Walt Disney The Black Never Cry Wolf, agreed to do the film after watching a performance in Seattle with his wife and 5-year-old daughter.   

Ballard described his directorial approach as follows: "I tried to do two things. To use photography to capture, as best we could, the qualities of the dancers, and to strengthen the story so it would be more appropriate for the film."    Ballard especially wanted to clarify that most of the story is dreamed by Clara. In the process of adaptation to film, Stowell revised large portions of his choreography; Sendak revised some designs, and created additional ones from scratch. 

Part of Ballards adaptation was intended to focus the characters psychology, as he later elaborated:

 
 Korda picture."  The animator Henry Selick assisted in the adaptation, shooting second unit for the film as well as drawing storyboards and contributing new fantasy sequences. 

===Filming and music===
Due to budget restrictions, all footage for Nutcracker was filmed over a period of ten days.  Meany Hall for the Performing Arts, on the University of Washington campus, was used as a filming location.    The films cast is made up of PNB dancers from the stage production. Vanessa Sharp, a 12-year-old dancer, played Clara. Hugh Bigney, at 30, used a false chin, nose, and bald cap to play the much older role of Drosselmeyer; Bigneys young daughter also appeared in the film as a baby mouse.  Other leads included Patricia Barker as the older Clara in the dream, and Wade Walthall as the Nutcracker Prince. The actress Julie Harris recorded narration for the film. 

In an interview during production, Ballard noted he was eager to avoid the cinematic idiosyncrasies of recent dance films, such as Nijinsky (film)|Nijinsky (1980), which had filmed its dancers mostly from the knee up. "I think its important to see the whole dancer … Where we have a great performance, youll see it all," he said. "I want to avoid movie trickery. Its a helluva lot tougher to shoot than I thought it would be." 

During production, Ballard had numerous disputes with Sendak. "More often, he won," reported Sendak. "After all, he was working in a medium in which I was a novice." 
 The Queen Sarah Walker.    Telarc released the complete soundtrack on compact disc, coinciding with the release of the film. 

==Reception==
Reviewers criticized the films camerawork and editing, particularly for its use of closeups and medium shots.  For example, in  s Chicago Sun Times review was similarly mixed: "It has been staged with great care and considerable beauty but it is nevertheless just a respectable version of a cultural artifact." 

Ballard responded to criticism about the editing in a post-release The New York Times interview, saying that the editing style was not what he had initially planned, but was a necessary result of the tight filming schedule. 

The film was nominated for a Young Artist Award for Best Family Motion Picture Drama in 1988.   

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 