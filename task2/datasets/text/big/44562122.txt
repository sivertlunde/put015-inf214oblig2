Kachche Heere
 
{{Infobox film
| name = Kachche Heere
| image = Kachche Heere 1982 film poster.jpg caption = Theatrical release poster
| director       = Narender Bedi
| producer       = Netra Pal Singh
| starring       = Feroz Khan Reena Roy Danny Denzongpa Aruna Irani Shakti Kapoor Narendra Nath
| music          = Rahul Dev Burman
| released       = 12 February 1982
| country        = India
| language       = Hindi
}}

Kachche Heere is a 1982 Bollywood Hindi Action film directed by Narender Bedi and starring Feroz Khan, Reena Roy, Danny Denzongpa, Aruna Irani and Shakti Kapoor.  The movie was produced by Netra Pal Singh and the music was composed by Rahul Dev Burman. The film was some what a sequel the 1974 Blockbuster Khote Sikkay. The film had the same star cast and almost exact storyline . A few changes were that Ranjeet was replaced by Shakti Kapoor.

== Soundtrack ==
* Chadhti Jawani
* Haare Na Insaan
* Kaanta lagao re
* Mere Jhumke ke
* Kachche Heere Music - 1& 2

==Cast==
* Feroz Khan as Kamal Singhs Nephew 
* Reena Roy as Rani
* Danny Denzongpa as Arjun
* Aruna Irani as Ganga
* Helen 
* Shakti Kapoor as Salim
* Dulari  
* Jankidas as Lala Jankidas
* Kamal Kapoor as Kamal Singh Thakur
* Prakash Mehra as himself 
* Dev Kumar as Lukka Singh Thakur
* Viju Khote as Kaaliya
* Shubha Khote  as Shobha
* Birbal
* Paintal as Ramu
* Narendra Nath as Jaggu
* Mac Mohan as False Income Tax Officer
* Dev Kumar as Lukka Singh Thakur

==References==
 

==External links==
 

 
 
 
 


 