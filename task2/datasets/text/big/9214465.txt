Nob Hill (film)
{{Infobox film
| name           = Nob Hill
| image          = Poster - Nob Hill 01.jpg
| image_size     =
| caption        =
| director       = Henry Hathaway
| producer       = André Daven
| writer         = Eleanore Griffin (story) Norman Reilly Raine (writer) Wanda Tuchock
| narrator       =
| starring       = George Raft Joan Bennett Vivian Blaine
| music          = David Buttolph
| cinematography = Edward Cronjager
| editing        = Harmon Jones
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 95 min.
| country        = United States
| language       = English
| budget         =
| gross          = $3,104,000 Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 104 
| website        =
}}
 1945 technicolor Barbary Coast saloon keeper starring George Raft and Joan Bennett. Part musical and part drama, the movie was directed by Henry Hathaway.

==Plot==
 

==Cast==
* George Raft as Tony Angelo
* Joan Bennett as Harriet Carruthers
* Vivian Blaine as Sally Templeton
* Peggy Ann Garner as Katie Flanagan
* Alan Reed as Dapper Jack Harrigan
* B.S. Pully as Joe the Bartender
* Rory Calhoun as Boxer sparring with Tony (uncredited)
* Mike Mazurki as Raffertys fighter (uncredited)

==Reception==
The film was one of the most popular releases of the year. 

==References==
 

==External links==
 
*  
*  
*  
*   at NY Times

 

 
 
 
 
 
 


 