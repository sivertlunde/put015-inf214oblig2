Power (2014 Kannada film)
{{Infobox film
| name           = Power ***
| image          = Power Kannada film poster.jpg
| caption        = Movie poster
| director       = K. Madesh
| producer       = Ram Achanta Gopichand Achanta Anil Sunkara
| writer         = Gopimohan Kona Venkat Sreenu Vaitla
| based on       =   Trisha Krishnan Prabhu Neetu Chandra Rangayana Raghu Tennis Krishna Sadhu Kokila Doddanna
| music          = S. Thaman
| cinematography = G. G. Krishna Kumar
| editing        = Deepu S. Kumar
| studio         = 14 Reels Entertainment Kolla Entertainment
| Distributor    = Samarth Ventures
| released       =  
| runtime        = 173 minutes
| country        = India
| language       = Kannada
| budget  =
| box office =
| gross =
| ratings =
}}
 Trisha Krishnan in lead roles. 

== Plot ==
Krishna Prasad (Prabhu (actor)|Prabhu) is a politician driven by idealistic values and a sense of social service. His followers are his brother Sathya (Dharma), and supporters (Shobharaj, Sharath Lohitashwa). The people of Krishnannas constituency elect him as a Member of the Legislative Assembly|M.L.A.. Krishna an ardent follower of Dr. Rajkumar, establishes his own independent political empire, consensus with ideals proposed by Rajkumar. Krishna Prasads dream is to see his son as a politician revered by people. During this time, he meets with a fatal accident, in which his brother and his followers are also killed.

14 years later, Krishna Prasads son Bharath (Puneeth Rajkumar) becomes a daring super-cop in Mumbai. He is on a mission to apprehend mafia don Nayak (Kelly Dorjee), who is involved in illegal drug trade, extortion and arms trafficking. It is later revealed that Krishna prasad actually survived the accident but ended up in a state of coma. However, this truth is hidden by Krishna Prasads family from the people. Bharath maintains a low profile to catch Nayak. In an undercover operation in Spain, Bharath gets hold of Nayaks brother, Bunty. Later, it is revealed that corrupt opposition leader (Doddanna), who is also tied up with Nayak, was the mastermind behind Krishna Prasads accident, and had planned to derail Krishnas political empire as he was opposing his illegal businesses and relation with Nayak.

Bharath falls in love with Prasanthi (Trisha Krishnan), whom he meets in Spain during his operation. Prasanthi is the daughter of Bharaths higher officer(Avinash), a cop who reports to the police commissioner (Vinod Alwa) in Bangalore city. When Bharaths dad comes out of the coma, doctors advise his family that Krishna Prasad is at risk if he encounters or hears anything upsetting, disturbing or shocking. Bharath hides the events surrounding his dads accident & shifts his family to his dads previously abandoned mansion, which is now being used for film making.

Bharath creates a dummy political set-up at this mansion. In the guise of a reality television program, Bharath tricks an aspiring but unsuccessful film actor P. Vibhushan(Rangayana Raghu), by making him believe that the television show is being sponsored by film star V. Ravichandran, and that Ravichandran wants to offer P. Vibhushan very high remuneration for his realistic performance in the show. On the other hand, an aspiring actor Kamangi Kidney (Sadhu Kokila|Sadhukokila) is also tricked by Bharath. Bharath also tricks Gouda with a real estate business deal, to exploit his criminal nexus. Bharath keeps this drama under wraps from Krishna Prasad, by making Krishna prasad believe that Bharath is now an M.L.A., revered by people fulfilling his dads wishes. He also marries his love, Prasanthi, to make his father happy. The rest of the plot reveals the politician-criminal nexus behind Krishna Prasads accident, the comic misadventures which lead to Krishna Prasad knowing the truth, and Bharaths retaliation over Nayak and Rudreshs political corruption.

==Cast==
 
* Puneeth Rajkumar Trisha Krishnan Prabhu
* Kelly Dorjee
* Rangayana Raghu
* Tennis Krishna
* Thilak Shekar
* Adi Lokesh
* Jai Jagadish
* Shobaraj
* Harsha
* Avinash
* Doddanna
* Shashikumar
* Om Prakash Rao
* Harish Raj
* Mandya Ramesh
* Neetu Chandra for item song
* Sharath Lohitashwa
* Sunder Raj
* Mohan Guneja
 

==Production== Telugu Blockbuster Trisha Krishnan debuts into Sandalwood and will star opposite Puneeth Rajkumar in this film.Audio of this movie was  launched   at Bellary by Telugu super star  Mahesh Babu on 29 June, 2014.

There will be a many problems related with the titles of the movie, First the movie was named as Rajakumara after puneeth confirmed the name as Ashwathama, then further title changed into Power, As this name is already Registered with KFCC it ha gone to Court. Finally it was changed to Power***.

== Soundtrack ==
{{Infobox album 
| Name       = Power
| Artist     = S. Thaman
| Type       = Soundtrack
| Released   =  
| Recorded   = 2014
| Genre      = Film soundtrack
| Length     =
| Label      = Lahari Music
| Producer   = S. Thaman
| Last album = Vaalu (2014)
| This album = Power (2014)
| Next album = Vaa Deal (2014)
}}

The soundtrack of the film was composed by S. Thaman and released through Lahari Music label on June 28, 2014. The album consists of five songs. The launch took place at the Bellary Municipal grounds with the presence of Telugu actor Mahesh Babu.

==Release==
The film released August 28, 2014 in 275+ screens including 116 screens in Banglore alone.The film got U/A certificate with few mutes from censor board.

===Critical Reception===
The film received Positive Reviews from critics as well as audience alike.
Sify stated "With a star-cast this huge and a good technical crew a neat screenplay is expected and this is where the director could have worked on. In spite of being a Telugu remake, director K. Madesh could have put some effort to make it? more local?. Unfortunately director has failed to do the same. Yet, Power Star is definitely worth a watch!"  Indiaglitz rated 3.25/5 stated There is no reason for you to miss this film. Desimartini rated 3.5/5 stated Puneeth Rajkumars powerful performance and Trishas refreshing screen presence make Power*** an entertaining watch. Though confusing at times, the humor, action and music make the film worth your while.  Chitraloka.com rated 3.5/5 stated Another first timer in the film is Kelly Dorjee. He plays the main villain and is a perfect cast. He is menacing and matches Puneeths heroism with his bad man act. Shivaji Prabhu has become a regular in Kannada films and it is good to see him in such weighty roles. Power*** is the perfect gift for the Ganesha festival you can give yourself and your friends and family. It is a great film to watch in a group.

===Box office===
The film opened to packed house and collected   in its first day which is the biggest opening for any Kannada Film followed by Maanikya,Bhajarangi, and Ninnindale.  The film made an remarkable business during first weekend while it grossed around   which is the biggest opening weekend for any Kannada movie followed by sudeeps Maanikya which grossed around  .The film is the first kannada film to cross  22 crore in just 6 days which broke the record of Maanikya.  The film grossed {{INRConvert000000000001


|c}} in two weeks. 

{{tracklist
| headline       = Tracklist
| extra_column   = Singer(s)
| total_length   =
| lyrics_credits = yes
| title1         = Dham Power
| lyrics1        = Chandan
| extra1         = Ranjith (singer)|Ranjith, Nivas, Yasin Nisar, Chandan
| length1        =
| title2         = Guruvara Sanje Kaviraj
| extra2         = Puneeth Rajkumar
| length2        =
| title3         = Mehabooba Mehabooba
| lyrics3        = V. Nagendra Prasad
| extra3         = Suchith Suresan, Priya Raghavan
| length3        =
| title4         = Jagatthe Namadhu
| lyrics4        = V. Nagendra Prasad
| extra4         = Nakul (actor)|Nakul, Santhosh, Anuradha Bhat, Sneha Ravindran
| length4        =
| title5         = Why Why (YY)
| lyrics5        = V. Nagendra Prasad
| extra5         = M. M. Manasi, Naveen Madhav
| length5        =
}}

== See also ==
* Puneeth Rajkumar

==References==
 

 
 
 
 
 
 