Kungliga patrasket
{{Infobox film
| name=Kungliga patrasket
| image= 
| caption= 
| director=Hasse Ekman
| producer=Lorens Marmstedt, Terrafilm
| writer=Hasse Ekman
| starring=Edvin Adolphson Ester Roeck-Hansen Hasse Ekman Eva Henning Olof Winnerstrand Hilda Borgström
| music=Sune Waldimir
| released=19 January 1945
| runtime=117 min
| country=Sweden Swedish
}}
 Swedish drama drama film directed by Hasse Ekman. Ekman was inspired by his father, Gösta Ekman (senior)|Gösta Ekman and the Barrymore family.

==Plot summary==
The film circles around the Anker family who are all actors work together at Stefan Ankers theatre, Kungsteatern, in Stockholm. 
But different complications threaten to break up the happy union between three generations of Ankers, how will it end?

==Cast==
*Edvin Adolphson as Stefan Anker, actor and theatre manager
*Ester Roeck-Hansen as Elisabet "Betty" Anker, his wife
*Hasse Ekman as Tommy Anker, his son
*Eva Henning as Monica Anker, his daughter
*Olof Winnerstrand as Karl-Hugo Anker, his father
*Hilda Borgström as Charlotta Anker, his mother
*Gudrun Brost as Sonja Swedje, his star
*Erik Strandmark as Göran Wallsenius, Monicas husband
*Stig Järrel as Stridström, author 
*Hugo Tranberg as Ernst 
*Bengt Ekerot as Rolf Eriksson, actor Wiktor "Kulörten" Andersson as Mille

==External links==
* 

 
 

 
 
 
 
 