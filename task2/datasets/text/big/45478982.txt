Fitting Master
{{Infobox film name      = Fitting Master director  = E. V. V. Satyanarayana starring  = Allari Naresh Madalsa Sharma Sayaji Shinde language  = Telugu released  =  
}}
 Telugu Action Action film directed by E. V. V. Satyanarayana, featuring his son Allari Naresh in the lead titular role of Fitting Master, along with Madalsa Sharma, Sayaji Shinde, Ali, Srinivas Reddy and Amit Kumar Tiwari in the lead roles. The film released on 14 January 2009, along with getting mixed reviews from critics.  The film was able to do well and was an Average at the Box Office.  The film has also been dubbed in Hindi as Shastra The Revenge. 

==Synopsis==

Sampath AKA Fitting Master (Allari Naresh) is a gym trainer, who tries to avoid Meghna (Madalsa Sharma), a girl who falls in love with him, after she is rescued by him. She wants to marry him, but he avoids her, saying he wont be able to keep her happy, after which she calls her brother (Amit Kumar Tiwari) from Dubai to India to make Sampath ready for Marriage. On the day of his arrival, he gets murdered. After a policeman (Sayaji Shinde) starts investigating, Meghna begins to doubt Sampath, and she finds out that he had killed her brother, after which Sampath reveals her brothers truth. Meghnas brother and 2 of his friends had embarrassed Sampaths sister by posting dirty videos of her in the internet. One of the friends had pretended to fall in love with her and she ran away with him due to which her mother dies of a Heart Attack. The boy who pretended to be in love with her tells her that his friends had filmed them. Because of this the girl commits suicide. When Sampaths and the girls father come to know of it he too commits suicide. Sampath had gotten angry on this and decided to take revenge. He had made Meghna purposefully fall in love with him and it was him who got his friend to tell Meghna to call her brother. When her brother revealed that he and his parents were actually against the marriage, Sampath overpowers the guy and guns him down. He now kills the other 2 as well. In the end, when Meghna again tries to convince him of her love, he refuses saying that her parents will never forgive their sons killer and that he cannot stay in the house with that guilt. He tells her to listen to her parents and they are her well wishers. As she begins to cry at losing him, he goes out of the house with tears in his eyes.

==Cast==

*Allari Naresh as Sampath AKA Fitting Master
*Madalsa Sharma as Meghna Chandra Mohan as Sampaths father
*Sayaji Shinde as Police officer
*Amit Kumar Tiwari as Meghnas brother
*Ali
*Rao Ramesh
*Srinivas Reddy

==Reception==

Fitting Master received mixed reviews from critics. 123telugu rated it 3/5.  Greatandhra rated it 2.5/5.  Fullyhyd rated it 2/5.  Rediff rated it 2/5.  Teluguone rated it 2.5/5. 

==Box Office==

Since the film had mixed responses from critics, along with Allari Nareshs Action Hero Image in the film, the film managed to do average business at the Box Office and emerged as a Moderate Commercial Success 

==References==
 