Shuttle (film)
{{Infobox film
| name           = Shuttle
| image          = Shuttleposter.jpg
| caption        = Theatrical release poster
| director       = Edward Anderson
| producer       =  Mark Donadio, Mark Williams, Michael A. Pierce, Allan Jones
| writer         = Edward Anderson Peyton List James Snyder Tony Curran 
| music          = Henning Lohner
| cinematography = Michael Fimognari
| editing        = William Yeh
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} airport shuttle Peyton List, and Cameron Goodman. 

Shuttle premiered at South-by-Southwest Music and Film Festival March 8, 2008 in Austin, Texas. The film opened theatrically in limited release in the United States on March 6, 2009. 

==Plot== Peyton List) James Snyder) and Matt (Dave Power) arrive and introduce themselves to them. Jules takes Mel into the bathroom to help her deal with her motion sickness, where Mel tells her that she has broken up with her fiancé.

Mels luggage is lost in the airport, so she must return the next day to get it. Outside, they board a shuttle bus after its Driver (Tony Curran) offers to charge them half the price of a regular shuttle. On board, they meet Andy (Cullen Douglas), a shy family man. Seth and Matt see them and attempt to board, but the Driver tells them he can only make three stops at a time. Jules informs him that the men are with them. 
 signing skills jack and onto Matts hand, severing his fingers. They quickly drive off to the hospital.

During the ride, the Driver calmly pulls over, much to the astonishment of the passengers. He pulls a gun on them and demands cooperation, taking everyones phones and wallets. While Mel and Seth argue about doing something, Jules finds a flare under the seat from the safety kit used to bandage Matts hand, and Mel comes up with a plan to get the window open. Andy sees her and causes a commotion. A struggle ensues and the flare is set ablaze. The Driver sees this, stops the bus and removes the flare. Jules runs for the front and finds the door locked and the key missing. 

The Driver re-enters and slashes Seths face with a switchblade as a warning to the others. He informs them of a "change of plans", and drives them to an indoor ATM booth and has Jules extract some money. While she is inside, she knocks some paper into a bin and sets it on fire to set off an alarm. The Driver sees this and rushes towards her, but she slams the door. Mel escapes, but the Driver shouts at her that he will let Jules suffocate from smoke inhalation if she doesnt return to the bus. She has no choice but to surrender.

He then takes them to an all-night supermarket, where he tells Mel to buy "nine items" on a list hes written for her and nothing else. On the bus, he chains Seth and Andy together and makes them take valuables and passports from the luggage on board. Seth tells Andy if they get loose, the Driver can only get one of them. Inside, Mel sees a security camera and, through sign language, says she needs help.  She goes to the front register, and hands a note to the cashier that tells her to check the security tape and call the police when she leaves. Seth gets free and makes a run for it, only to be run over by the bus. The Driver places the body in the back.

He then scolds Mel for buying ice when she didnt need to, but she tells him its for Matts fingers. Back in the store, the cashier checks the cameras only to find that they were never switched on. Later, Mel shows Matt a knife she hid in the ice, which she uses to cut their belts off. Matt uses the ice bag to smash the window, and they all shout for help. The Driver walks towards them and pulls out the gun. Mel cuts his wrist with the knife, and Matt hits him over the head with the bag. Mel grabs the gun and threatens to shoot the Driver. She makes him hand over the keys to the belts, and Jules is able to get herself and Andy free.

She takes the wheel of the bus and drives, leaving Matt with the gun and Andy with the knife. Andy then nonchalantly stabs Matt in the throat, which no one notices at first. He grabs the gun and puts it to Mels head, telling her to get away from the pedals. Andy then reveals himself to be in league with the Driver. They stop at a bridge to throw the boys bodies off, and afterward, Andy torments the girls for a few minutes before the Driver gets him to stop.

Later, Mel manages to get a tire iron from under the seat, and keeps it by her side. Andy wraps tape around the girls mouths and takes a cuff of their hair. Mel hits him several times with the tire iron and the Driver is forced to stop. She pulls herself free, and the Driver attempts to get the gun, only for her to hit him on the hand, and knock it towards Jules. Mel seizes control and drives away, but he rushes towards her. She slams the brakes and accelerates to throw him about. She reaches for a knife and stabs him in the knee. Andy comes over to attack her but she grabs a fire extinguisher and hits him with it. The bus then crashes into a wall during the struggle.
 gasses her with a tube connected to the exhaust.

He finds Mel, who threatens to cut her face with a shard of glass, which would make her useless to him as well. Instead, she stabs him in the arm with it. He attempts to disarm her, but she stabs it into his thigh before hitting him in the face with some light fixtures. She finds the gun and shoots him, the bullet skimming his head. Thinking he is dead, she tries to escape in the bus. However, before it can start, the Driver attacks her again. They grapple for a minute before he forces her into a large wooden crate, which contains the items Mel purchased earlier in the grocery store: a flashlight, a loaf of bread, two jugs of water, two magazines, kitty litter, and a litter box, as well as her motion sickness pills.

Trapped inside the dark crate, Mel yells for help, but no one is there, and when someone with a forklift comes, he turns out to be in on the operation. While being carried away, she finds a photo of seven young white women in what looks like a filthy underground cellar. The photo depicts the despondent-looking girls completely naked, hair dyed blonde, and wearing white high heels (like Mel and Jules were forced to wear). It is heavily implied that these girls were also kidnapped by the Driver at some point and forced to become sex slaves overseas. Mel looks at the photo in horror as she realizes that she is to become a sex slave too. The crate is then loaded onto a cargo ship destined for East Asia.

The final shot is of Mels lost luggage turning up at the airport as another day begins.

==Critical reception==
The film received mixed reviews from critics. The review aggregator Rotten Tomatoes reported that 55% of critics gave the film positive reviews, based on 22 reviews. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 