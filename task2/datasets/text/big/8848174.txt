Marigold (2007 film)
{{Infobox film name        = Marigold image       = Marigoldfilmposter.jpg caption     = Marigold film poster writer      = Willard Carroll starring    = Salman Khan Ali Larter director    = Willard Carroll producer    = Charles Salmon editing     = Anuradha Singh distributor = released    = August 17, 2007 (U.S.) runtime     = 110 mins language    = English budget      = cinematography = Anil Mehta music       = Shankar-Ehsaan-Loy soundtrack  =  country     = USA/India/UK awards      = followed_by =
}}
 romantic musical musical comedy about an American actress who begins a personal transformation and becomes enamored with India as she experiences Bollywood firsthand. Director Willard Carroll intended the film to bridge "the gap between Indian and American cinema."

==Plot==

Marigold Lexton (Ali Larter), a self-centered and temperamental young American actress, arrives in India expecting to be treated like a star, despite the fact that she has been making nothing but B movie sequels for some time. She is stranded in Goa after the film she was to star in is canceled, and a sympathetic crew member offers her a ride, which brings her to the set of another movie, a Bollywood musical film|musical. She actually tells her boyfriend Barry that she was hoping she wouldnt have to marry him if this trip was successful, and soon finds herself the center of attraction on the set, where she quickly lands a minor role and a date with the spoiled young lead actor. But after she rebuffs his crude proposal that night she winds up talking with Prem (Salman Khan), the films choreographer. He knows she lied about being able to dance, and takes her in hand, while showing her the nearby towns and countryside in their spare time.

As they grow closer Prem talks to her about the importance of family, and she not only learns that he is estranged from his father, but that he is also a prince. He had not seen his family in three years, but the day before he received a call from his sister, asking him to come home for her wedding.  He asks Marigold to go home with him to Jodhpur, Rajasthan, for the wedding, since shooting on the movie had been shut down for a week. She is entranced by the generosity and opulence of his family leading up to the wedding, but afterwards is shocked to discover that he has been betrothed to another since childhood. He has fallen totally in love with Marigold, but has neglected to mention the long arranged marriage, and his father has not encouraged him to follow his heart. She feels betrayed and storms out, followed by Prems fiance, who offers to buy her a drink. She confesses to Marigold that although she loves Prem she doesnt believe he has ever really loved her.
 Holy fire, Prem believes he is marrying the woman he has been engaged to since childhood. With the marriage complete, Prem lifts his wifes veil, and he and most of the guests are astonished to find Marigold standing before him—and it appears that Barry has married Prems former fiance as well. The movies director and their friends from the crew appear in the crowd, cheering, then Prem sings and dances with Marigold and a full chorus, just like a happy ending in a Bollywood musical.

==Cast==
* Salman Khan ... Prem
* Ali Larter ... Marigold Lexton
* Simone Singh ...Shazia
* Nandana Sen ... Jaanvi
* Ian Bohen ... Barry
* Shari Watson ... Doreen Helen Richardson
* Vikas Bhalla
* Suchitra Pillai ... Rani
* Vijayendra Ghatge
* Roopak Saluja ... Mani
* Kiran Juneja
* Gulshan Grover . . . Vikram
* Rakesh Bedi ... Manoj
* Marc Allen Lewis ... Marc
* Lea Moreno Young ... Valjean
* Catherine Fulop ... Sister Fernandéz

==Production==
While in India, Carroll had seen a Bollywood film (with Salman Khan), and, after seeing 150 such films, he hired Larter (whom he had worked with before) and Khan to be in a "crossover" film (although Carroll says he dislikes the term and prefers the idea of an "international audience").  Larter said that the her role in the film, to some degree, mirrored her life at the time.  Larter lacked professional dance training and worked with a choreographer to prepare for the filming. One of the songs features her own singing. 

Composer Graeme Revell augmented the songs with a Western (and in one case an Indian) sound and ended up striking an overall balance between the two.  Carroll decided that some of the songs should be in Hindi. 

CGI effects were used to create most of the white Taj Mahal (there were some set pieces built) and all of the mirroring black structure.   

==Soundtrack==
{{Infobox album|
| Name = Marigold
| Type = Soundtrack
| Artist = Shankar Ehsaan Loy
| Cover =
| Border = yes
| Alt = 
| Caption = Original CD Cover
| Released = 17 July 2007
| Recorded = 2006 Feature film soundtrack
| Length = Hindi English English
| Label = Big Music
| Producer = Charles Salmon Tom Wilhite
| Last album = Heyy Babyy (2007)
| This album = Marigold (2007)
| Next album = Johnny Gaddaar (2007)
}}

The music of the film is composed by Shankar-Ehsaan-Loy while the lyrics are penned by Javed Akhtar.
{{track listing
| headline        = Tracks
| extra_column    = Artist(s)
| total_length    =
| title1          = Yeh Pyaar Kya Hai (Seven Stages Of Love) Shaan
| title2          = Yeh Pyaar Hai (Thats Love) Shaan
| title3          = Paagal Si Saari Leheren (Beach Blanket Bollywood)
| extra3          = Alka Yagnik, Vikas Bhalla
| title4          = Sachha Pyaar (The Meaning Of Love)
| extra4          = Nikita Nigam
| title5          = Tan Man (Marigold Erupts)
| extra5          = Alka Yagnik, Nihira Joshi, Sneha Pant, Vikas Bhalla
| title6          = Listen To The Music (English)
| extra6          = Ali Larter, Shaan (singer)|Shaan, Truth Hurts
| title7          = The Meaning Of Love (English)
| extra7          = Truth Hurts
| title8          = Seven Stages Of Love (English)
| extra8           =Shaan (singer)|Shaan, Truth Hurts
}}

==Filming locations==
* Goa, India
* Jodhpur, Rajasthan, India
* Khimsar, Rajasthan, India
* Mumbai, Maharashtra, India
* Rajasthan, India
* London, England, UK
* Vancouver, British Columbia, Canada Global Village, Dubai, United Arab Emirates

==References==
 
 
 

==External links==
*  
*  
*   at the Internet Movie Database
*  

 
 
 
 
 