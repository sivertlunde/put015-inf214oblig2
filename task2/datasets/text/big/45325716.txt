The Mad Locomotive
{{Infobox Hollywood cartoon|
| cartoon_name = The Mad Locomotive
| series = Jerry on the Job
| image = 
| caption = 
| director = Walter Lantz
| story_artist = Walter Hoban
| animator = Walter Lantz
| voice_actor = 
| musician = 
| producer = 
| studio = Bray Productions, Inc.
| distributor = Goldwyn
| release_date = June 4, 1922
| color_process = Black and white
| runtime = 2:51 English
| preceded_by = Male Or Female
| followed_by = 
}}

The Mad Locomotive is a short animated film and part of a series of films based on the comic strip Jerry on the Job by Walter Hoban. The film marks the final animated adaptation of the strip.

==Plot==
Mr. Givneys trains need coal to run but the station is out of supply. Because of this, Mr. Givney decides to collect coal from passengers.  Moments later, three men who want to go to some park arrive at the train station. With the new rules in place, the three men offer coal as well as their fares.

With the things collected, and the passengers on board, Mr. Givney and his employee Jerry set off in their train. After traveling several miles, the coaches are lost somehow, and only the locomotive is seen running on the rails. The locomotive starts to travel roughly until Jerry and Mr. Givney fall off. Mr. Givney appears to be happy, knowing he still has the passengers fares. Jerry goes on to remind him that they no longer have a train.

The locomotive reaches some city, and seems animated. Hungry for fuel, the locomotive spots a truck loaded with coal. The locomotive eats every lump of coal before traveling again.

Back at the scene where Jerry and Mr. Givney were dropped off, the two guys sit around not knowing what to do. Momentarily, the locomotive comes back, much to their delight.

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 
 
 
 
 
 
 


 