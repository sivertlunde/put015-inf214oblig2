Conquest of Cochise
{{Infobox film name           =Conquest of Cochise image          =Concocpos.jpg caption        =Film poster director       =William Castle producer       =Sam Katzman writer         = screenplay     =DeVallon Scott (story and screenplay) Arthur Lewis   (screenplay)    starring       =Robert Stack John Hodiak music          =Mischa Bakaleinikoff (musical director) various composers cinematography =Henry Freulich editing        =Al Clark studio         =Columbia Pictures distributor    =Columbia Pictures released       =  runtime        =70 minutes country        =United States language       =English budget         = gross          =
}}
 1953 fictional Western set in 1853 at the time of the Gadsen Purchase.  Produced by "Jungle" Sam Katzman and directed by William Castle, it stars Robert Stack and John Hodiak.

==Plot== Dragoons into Tucson in the area now known as Arizona that has recently been purchased by the United States.  Both the "dreaded" Apache and "savage" Comanche Indian tribes are at war with the Mexican population.  In addition to the three stakeholders, Major Burke faces a treacherous businessman whose profits of selling alcohol to all parties is threatened by peace.

==Production==
The film was shot at Santa Clarita, California, Corriganville movie ranch and the Vasquez Rocks Natural Area Park Agua Dulce, California. 

==Cast==
Robert Stack  ...  Maj. Tom Burke  
John Hodiak  ...  Cochise   
Joy Page  ...  Consuelo de Cordova 
Rico Alaniz  ...  Felipe  
Fortunio Bonanova  ...  Mexican Minister  
Edward Colmans  ...  Don Francisco de Cordova   
Alex Montoya  ...  Jose Antonio Felicisimo de la Vega y Garcia   
Steven Ritch  ...  Tukiwah, Cochises Lieutenant  
Robert Griffin...  Sam Maddock  
Carol Thurston  ...  Terua, Cochises Wife   
Rodd Redwing  ...  Red Knife, Comanche Chief   
Snub Pollard ...  Barfly

==Notes==
 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 