Ilzaam
{{Infobox film
| name           = Ilzaam
| image          = 
| image_size     = 
| caption        = 
| director       = Shibu Mitra
| producer       = Pankaj Nihalani
| writer         = Ram Kelkar Dr. Rahi Masoom Reza Faiz Saleem
| narrator       =  Govinda Neelam Neelam
| music          = Bappi Lahiri
| cinematography = Nadeem Khan
| editing        = Nand Kumar
| studio         = 
| distributor    = Vishaldeep International
| released       = 28 February 1986
| runtime        =  165 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Govinda in his film debut alongside Neelam Kothari|Neelam, Shatrughan Sinha, Shashi Kapoor, Prem Chopra and Anita Raj.

==Synopsis==

Orphaned and poor Ajay (Govinda (actor)|Govinda) meets with wealthy Aarti (Neelam Kothari|Neelam), and both fall in love. Their hopes to marry are dashed to the ground, when Aartis dad, Dhanraj (Prem Chopra), opposes Ajay and Aartis marriage until he gets wealthy. Ajay disappears from Aartis life, and Aarti is heartbroken but does not get Ajay out of her mind. Several months later, she meets with a young man who looks like Ajay, but claims that he is Vijay. She finds out that he is living with a sister, Laxmi, his mother, and an older brother, Inspector Surajprasad (Shatrughan Sinha) While investigating an unrelated matter, Surajprasad finds out that Vijay is a career criminal, who sings and dances on the streets, distracting people, while his colleagues break into apartments and rob the residents. It was a box-office Hit.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "I Am A Street Dancer"
| Amit Kumar
|-
| 2
| "Pehle Pehle Pyar Ki"
| Amit Kumar, Asha Bhosle
|-
| 3
| "Main Aaya Tere Liye"
| Nazia Hassan, Zohaib Hassan
|-
| 4
| "Yeh Tujhe Kya Hua"
| Amit Kumar, Asha Bhosle
|-
| 5
| "Duniya Ki Aisi Tesi"
| Shabbir Kumar, Asha Bhosle     
|-
| 6
| "Da Da Dadai Dadai"
| S. Janaki
|}

== External links ==
*  

 
 
 

 