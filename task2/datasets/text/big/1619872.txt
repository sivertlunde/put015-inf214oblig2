Downhill (1927 film)
 
 

{{Infobox film
| name           = Downhill
| image          = Downhill1.jpg
| caption        = Original Movie Poster
| director       = Alfred Hitchcock
| producer       = Michael Balcon  C. M. Woolf
| writer         = Play:    Ian Hunter  Violet Farebrother
| music          =
| cinematography = Claude L. McDonnell	
| editing        = Ivor Montagu  Lionel Rich
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service (UK)  Sono Art-World Wide Pictures (US)
| released       =  
| runtime        = 80   (UK)  74 min (USA)
| country        = United Kingdom
| language       = Silent film  English intertitles
| budget         =
}}
 silent drama film directed by Alfred Hitchcock, starring Ivor Novello, Robin Irvine, and Isabel Jeans, and based on the play Down Hill by Novello and Constance Collier. The film was made by Gainsborough Pictures at their Islington studios. The film is Hitchcocks fifth film as director. The American alternative title for this film was When Boys Leave Home.

== Plot ==

At an expensive English boarding school for boys, Roddy Berwick (Ivor Novello) is School Captain and star rugby player. He and his best friend Tim (Robin Irvine) start seeing a waitress Mabel (Annette Benson). Out of pique, she tells the headmaster that she is pregnant and that Roddy is the father. In fact it was Tim, who cannot afford to be expelled because he needs to win a scholarship to attend Oxford University. Promising Tim that he will never reveal the truth, Roddy accepts expulsion.

Returning to his parents’ home, he finds that his father (Norman McKinnel) believes him guilty of the false accusation. 
 Ian Hunter) and discards Roddy after his inheritance is exhausted. He becomes a gigolo in a Paris music hall but soon quits over self-loathing at romancing older women for money.

Roddy ends up alone and delirious in a shabby room in Marseilles. Some sailors take pity on him and ship him back home, possibly hoping for reward. Roddys father has learned the truth about the waitresss false accusation during his sons absence and joyfully welcomes him back. Roddy resumes his previous life.

==Cast==
* Ivor Novello – Roddy Berwick
* Robin Irvine – Tim Wakely
* Isabel Jeans – Julia Ian Hunter – Archie
* Norman McKinnel – Sir Thomas Berwick
* Annette Benson – Mabel
* Sybil Rhoda – Sybil Wakely
* Lilian Braithwaite – Lady Berwick
* Violet Farebrother –  The Poet Ben Webster – Dr. Dawson
* Hannah Jones – The Dressmaker
* Jerrold Robertshaw – Reverend Henry Wakely
* Barbara Gott – Madame Michet
* Alf Goddard – The Swede
* J. Nelson – Hibbert

==Production==
The film is based on the play, Down Hill, written by its star Ivor Novello and Constance Collier under the combined alias David LEstrange.
 Sunday Times, wrote "The scent of good honest soap crosses the footlights". Hitchcock included a similar scene of Novello for the film in which he is shown naked from the waist up.
 title cards, German expressionist films of the time.
 tinted a "sickly" green to express mental torment and nausea.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 