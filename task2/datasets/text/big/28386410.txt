Horrible Bosses
{{Infobox film
| image    = Horrible_Bosses.jpg
| alt      =  
| caption  = Theatrical release poster
| director = Seth Gordon
| producer = {{Plain list |
*Brett Ratner
*Jay Stern
}}
| screenplay = {{Plain list |
*Michael Markowitz
*John Francis Daley Jonathan Goldstein
}}
| story    = Michael Markowitz
| starring ={{Plain list |
*Jason Bateman
*Charlie Day
*Jason Sudeikis
*Jennifer Aniston
*Colin Farrell
*Kevin Spacey
*Donald Sutherland
*Jamie Foxx
}}
| music          = Christopher Lennertz
| cinematography = David Hennings
| editing        = Peter Teschner
| studio         = {{Plain list |
*New Line Cinema
*Rat Entertainment
}} Warner Bros. Pictures
| released       =  
| runtime        = 98 minutes 
| country        = United States
| language       = English
| budget         = $35–$37 million  
| gross          = $209.6 million 
}} Jonathan Goldstein, based on a story by Markowitz. It stars Jason Bateman, Charlie Day, Jason Sudeikis, Jennifer Aniston, Colin Farrell, Kevin Spacey, and Jamie Foxx. The plot follows three friends, played by Bateman, Day, and Sudeikis, who decide to murder their respective overbearing, abusive bosses, portrayed by Spacey, Aniston and Farrell.

Markowitzs script was bought by New Line Cinema in 2005 and the film spent six years in various states of pre-production, with a variety of actors attached to different roles. By 2010, Goldstein and Daley had rewritten the script, and the film finally went into production.
 The War of the Roses in 1990. The film grossed over $209&nbsp;million worldwide during its theatrical run.

The film opened to positive critical reception, with several critics praising the ensemble cast, with each lead being singled out for their performances across reviews. The plot received a more mixed response; some reviewers felt that its dark, humorous premise was explored well, while others felt the jokes were racism|racist, homophobia|homophobic, and misogyny|misogynistic. A sequel, Horrible Bosses 2, was released on November 26, 2014.

==Plot==
  sadistic David sexually harassed by his boss, Dr. Julia Harris (Aniston); she threatens to tell his fiancee Stacy (Lindsay Sloane) that he had sex with her unless he actually has sex with her. Nick and Dales accountant friend Kurt Buckman (Sudeikis) enjoys working for Jack Pellitt (Donald Sutherland) at a chemical company, but after Jack unexpectedly dies of a heart attack, the company is taken over by Jacks cocaine-addicted son Bobby (Farrell), whose apathy and incompetence threaten the future of the company.

At night, over drinks, Kurt jokingly suggests that their lives would be happier if their bosses were no longer around. Initially hesitant, they eventually agree to kill their employers. In search of a hitman, the trio meet Dean "Motherfucker" Jones (Foxx), an ex-con who agrees to be their "murder consultant". Jones suggests that Dale, Kurt and Nick kill each others bosses to hide their motive while making the deaths look like accidents.

The three reconnoiter Bobbys house, and Kurt steals Bobbys phone. They next go to Harkens house, where Kurt and Nick go inside while Dale waits in the car. Harken returns home and confronts Dale for littering, but then has an allergy attack from the peanut butter on the litter. Dale saves Harken by stabbing him with an EpiPen. Nick and Kurt think Dale is stabbing Harken to death and flee, with Kurt accidentally dropping Bobbys phone in Harkens bedroom. The next night, Kurt watches Julias home, but she seduces and has sex with him. Nick and Dale wait outside Bobbys and Harkens houses, respectively, to commit the murders. Harken discovers Bobbys cellphone in his bedroom and uses it to find his address, suspecting his wife Rhonda (Julie Bowen) is having an affair. He drives over and kills Bobby, with Nick as a secret witness. 
 Snow Falling on Cedars. Jones suggests that they get Harken to confess and secretly tape it. The three accidentally crash Harkens surprise birthday party, where Nick and Dale get Harken to confess to the murder before realizing that Kurt, who has the audio recorder, is elsewhere having sex with Rhonda. Harken threatens to kill all three for attempting to blackmail him. They flee by car, but Harken gives chase and repeatedly rams their vehicle. Believing they have committed a crime, the cars navigation-system operator remotely disables Kurts car, allowing Harken to catch and hold them at gunpoint. Harken shoots himself in the leg as he boasts about his plan to frame them for murdering Bobby and attempting to kill him to get rid of the witness.

The police arrest Nick, Dale and Kurt, but the navigation-system operator reveals that the entire conversation was recorded. Harken is sentenced to 25 years to life in prison, while the friends get their charges waived. Nick is promoted to president of the company under a sadistic CEO, Kurt retains his job under a new boss, and Dale blackmails Julia into ending her harassment by convincing her to sexually harass a supposedly unconscious patient, while Jones secretly records the act.
 

==Cast==
{{multiple image
 | direction = vertical
 | width = 175
 | footer = Top to bottom: Aniston, Farrell and Spacey, who portray the titular horrible bosses.
 | image1 = JenniferAnistonFeb09.jpg
 | alt1 = 
 | caption1 = 
 | image2 = Colin Farrell by David Shankbone.jpg
 | alt2 = 
 | caption2 =
 | image3 = Kevin Spacey Premiere.jpg
 | alt3 = 
 | caption3 = 
 }}
*Jason Bateman as Nick Hendricks
:An executive at a financial firm who is manipulated into jumping through hoops in order to get a promotion that his boss never intended to give him.  Markowitz wrote the role specifically for Bateman. 
*Charlie Day as Dale Arbus Going the Distance—Reuters reported that industry insiders believed his performance overshadowed the main stars. 
*Jason Sudeikis as Kurt Buckman 
:An account manager at a chemical company dealing with a new, drug-addicted boss after his beloved former boss dies.  Sudeikis was cast in May 2010. 
*Jennifer Aniston as Dr. Julia Harris, D.D.S.
:Markowitz based the character on a former boss, claiming she was "very sexually aggressive with everybody". When writing the script, Markowitz intended for the role to go to Aniston.  He stated, "but   looked more like Cruella de Vil. It was like flirting with a cobweb. So I decided for the sake of the movie, let’s go with Jennifer Aniston.”  The actress insisted on wearing a brown wig for the role, wanting to look different from other characters she had played. 
*Colin Farrell as Bobby Pellitt
:Described as a "weaselly scion"  and a "corrupt and incompetent jerk whos in charge of things but clearly has no idea what hes doing."    Farrell explained the motivation he gave to the character, stating "This guy thinks hes Gods gift to women, Gods gift to intellect, to humor, to the club scene, to everything. Its all part of his grandiose sense of self-esteem, which is probably masking a deeper sense of being a disappointment to his father and being riddled with envy over the relationship his father had with Kurt, and all kinds of other things. With Pellit, Seth gave me complete license to act as pathologically screwed up as possible." Farrell contributed significantly to the appearance of his character, suggesting the comb over hairstyle, pot-belly and an affinity for Chinese dragons. 
*Kevin Spacey as David Harken
:President of Comnidyne Industries.  Tom Cruise, Philip Seymour Hoffman and Jeff Bridges had been approached by New Line Cinema to take the role, described as a psychopathic master manipulator with an attractive wife. Spacey signed up for the role in June 2010.  The part was considered "integral" to the film.  Gordon commented that the character was an amalgamation of several real bosses (rather than one single person) to avoid being sued. 
*Jamie Foxx as Dean "Motherfucker" Jones
:The character had the more "colorful"  name "Cocksucker Jones", but it was changed at Foxxs request, with producer Jay Stern commenting that Foxx felt it "was over the line".  The current name was said to be subject to further change, prior to the release of the film.  Foxx contributed to his characters appearance, suggesting full-scalp tattoos and a retro clothing style. Foxx described the appearance as "a guy who maybe went to jail for a minute and now hes living in his own time capsule. When he got out he went right back to the clothes he thought were hot when he went in." 

During the six-year development of the film, several actors were in negotiations to star, including Owen Wilson, Vince Vaughn, Matthew McConaughey, Ryan Reynolds, Dax Shepard, and Johnny Knoxville.  

Donald Sutherland portrays Jack Pellitt, Bobbys father and Kurts boss.  On July 27, 2010, Isaiah Mustafa was confirmed as joining the cast. Mustafa was quoted as saying "Its a smaller role".  He appears as Officer Wilkens. Julie Bowen appears in the film as Rhonda, Harkens wife. Bowen stated that her character "may or may not be a hussy", the character described as intentionally making her husband jealous.    Ioan Gruffudd has a cameo as a male prostitute erroneously hired as a hitman. Lindsay Sloane appears as Dales fiancee Stacy.  P.J. Byrne plays Kenny Sommerfeld, a former investment manager, now scrounging for drinks, while Wendell Pierce and Ron White play a pair of cops.  Bob Newhart makes a cameo as sadistic Comnidyne CEO Louis Sherman.  John Francis Daley, a screenwriter on the film, cameos as Nicks co-worker Carter.

==Production==

===Development=== David Dobkin Jonathan Goldstein and John Francis Daley rewrote the script in 2010, and the project went into production with Seth Gordon directing.

===Design===
 
Production designer Shepherd Frankel specifically set out to create distinctly different environments for the three employees and their respective bosses homes and offices. Nick and Harkens workplace is the "Comnidyne" bullpen, which was designed to "enhance the discomfort and anxiety of lower-level employees clustered in the center of the room where every movement is monitored by the boss from his corner office." The design team met with financial strategists and management companies to learn about the architecture of their office layouts to visually represent the experience of starting from a low-ranking position in a cubicle and aspiring to an office. Costume designer Carol Ramsey worked with Frankel and set decorator Jan Pascale to match Harkens suit to that of the surrounding "cold grey and blue" color palette of his office. Harkens home was described as "equally lacking in warmth" as the office but more lavishly decorated and "for show", including an intentionally oversized portrait of him with his "trophy wife". 

Designing Julias office was described as a "challenge", infusing a "sensual vibe" into a dental office. Frankel approached the design through Julias mentality, stating, "Shes a Type A professional at the top of her game, who likes to play cat-and-mouse, so its a completely controlled environment, with apertures and views into other rooms so she always knows whats going on". "Its highly designed, with rich wallpaper and tones, sumptuous artwork and subtle lighting—all very
disarming till you step into her private office. The blinds close, the door locks and you think, Its the Temple of Doom." Similarly approaching the characters home, the design allowed for wide windows which face onto a public street "which afford her the opportunity to put on the kind of show she couldnt get away with at work." 

Bobbys environments were designed with more contrast, the character being new to the work area. Frankel described the contrast as "the company reflects   human touch, whereas   home is a shameless shrine to himself and his hedonistic appetites." Frankel continued, "It features a mishmash of anything he finds exotic and erotic, mostly Egyptian and Asian motifs with an 80s Studio 54 vibe, a makeshift dojo, lots of mirrors and a massage table." Some parts of the house design were provided by Farrell and Gordons interpretation of the character and his "infatuation" with martial arts and "his delusions of prowess". 

===Filming===
Filming of Horrible Bosses took place in and around Los Angeles.  The production team attempted to find locations "that people havent already seen a hundred times in movies and on TV", aiming for the film to appear as if it could be taking place anywhere in America "where people are trying to pursue the American dream but getting stopped by a horrible boss." "Comnidyine" was represented by an office building in Torrance, California, with the crew building the set on a vacant floor.  For "Pellitt Chemical", the production team found a "perfect landscape of pipes and containers" in Santa Fe Springs, surrounding an unoccupied water cleaning and storage facility. To take advantage of the surrounding imagery, the warehouse required an extensive series of overhauls, including cutting windows into concrete walls and creating new doorways to allow for visuals of the warehouse exterior and provide a setting for the final scene of Sutherlands character.  A T.G.I. Fridays in Woodland Hills, Los Angeles, was used as a bar frequented by Nick, Dale, and Kurt, while the bar scene where they meet with Jones was staged in downtown Los Angeles. 
 digitally using Panavision Genesis camera.  Gordon encouraged the actors to improvise, though Aniston claimed to not have taken advantage of the offer as much as her co-stars, stating, "My dialogue was just so beautifully choreographed that there wasn’t much that I needed to do”. 

===Music===
The soundtrack was composed by award-winning composer Christopher Lennertz, with music contributed by Mike McCready of Pearl Jam, Stefan Lessard of Dave Matthews Band and Money Mark—a collaborator with the Beastie Boys.  McCready, Lessard, and Mark worked with musicians Matt Chamberlain, David Levita, Aaron Kaplan, Victor Indrizzo, Chris Chaney, Davey Chegwidden and DJ Cheapshot  to develop the music.  Major contributions were provided by Mark on keyboard, McCready and Levita on guitar, Chaney and Lessard on bass, Indrizzo on drums and DJ Cheapshot on turntables.
 The Village analog tape, vinyl record.  He explained, "The idea was to put together a band that would record the score together the same way that they would make an album. It isnt over-produced or shiny and digital in any way. Its brash, noisy, and full of bravado and swagger. I knew that if we could harness some of this sonic magic in the score, then the toughness and confidence of the music would play against Bateman, Sudeikis, and Charlie Day to really emphasize and elevate the humor in the situations that transpire."  Lennertz continued, "We tracked through tape before Pro Tools to get that fat sound, and made every choice based on feel rather than perfection. We even used the same Wurlitzer that Money Mark played on Becks classic ‘Where Its At’. At the end of the day, Seth   and I wanted to produce a score that is as irreverent and full of attitude as the movie itself. I think we did it...and most of all, everyone had a blast in the process." 

Horrible Bosses: The Original Motion Picture Soundtrack was released in physical and digital formats on July 5, 2011, by WaterTower Music.   The soundtrack consists of 33 tracks with a runtime of 63 minutes.  

{{Track listing
|| collapsed =yes
| headline =Horrible Bosses: The Original Motion Picture Soundtrack
| writing_credits =no
| extra_column =Artist
| total_length =1:02:55
| title1 =Motel Meet Up
| extra1 =Christopher Lennertz, Mike McCready & Money Mark
| length1 =1:13
| title2 =Total Fucking Asshole
| extra2 =Christopher Lennertz
| length2 =2:34
| title3 =Heart Attack
| extra3 =Christopher Lennertz, Mike McCready & Money Mark
| length3 =0:51
| title4 =Whose Promotion?
| extra4 =Christopher Lennertz
| length4 =1:31
| title5 =Fucker
| extra5 =Christopher Lennertz, Mike McCready, Money Mark & Chris Chaney
| length5 =2:38
| title6 =Hey Dickwad...What the Fuck?
| extra6 =Christopher Lennertz, Mike McCready & Dave Levita
| length6 =1:43
| title7 =Can You See My Pussy?
| extra7 =Christopher Lennertz
| length7 =1:38
| title8 =Lets Kill This Bitch
| extra8 =Christopher Lennertz, Mike McCready, Money Mark & Chris Chaney
| length8 =0:25
| title9 =Gimme That Dong Dale
| extra9 =Christopher Lennertz, Stefan Lessard, Money Mark & Matt Chamberlain
| length9 =0:59
| title10 =Wet Work
| extra10 =Christopher Lennertz
| length10 =1:39
| title11 =Men Seeking A Man
| extra11 =Christopher Lennertz
| length11 =0:51
| title12 =Mother Fucker Jones
| extra12 =Christopher Lennertz, Money Mark, Victor Indrizzo & Chris Chaney
| length12 =0:59
| title13 =Douchebag Museum
| extra13 =Christopher Lennertz
| length13 =1:14
| title14 =Crazy Bitch Whore
| extra14 =Christopher Lennertz, Stefan Lessard, Money Mark & Matt Chamberlain
| length14 =1:23
| title15 =Coke In A Dustbuster
| extra15 =Christopher Lennertz, Stefan Lessard, Money Mark & Matt Chamberlain
| length15 =3:13
| title16 =Four Honks
| extra16 =Christopher Lennertz
| length16 =1:03
| title17 =These People Fucking Love Cats
| extra17 =Christopher Lennertz, Money Mark & Dave Levita
| length17 =1:47
| title18 =Penis...Peanuts
| extra18 =Christopher Lennertz, Mike McCready, Money Mark & Victor Indrizzo
| length18 =1:28
| title19 =Raped In Prison
| extra19 =Christopher Lennertz & Money Mark
| length19 =1:08
| title20 =Harkin Finds The Phone
| extra20 =Christopher Lennertz
| length20 =1:08
| title21 =We Got The Cheese
| extra21 =Christopher Lennertz, Mike McCready, Money Mark & Chris Chaney
| length21 =1:02
| title22 =Penis Shaped Food
| extra22 =Christopher Lennertz, Stefan Lessard, Money Mark & Matt Chamberlain
| length22 =0:48
| title23 =Ipso Facto
| extra23 =Christopher Lennertz, Stefan Lessard & Davey Chedwiggen
| length23 =1:10
| title24 =Blackmailing Harkin
| extra24 =Christopher Lennertz, Stefan Lessard, Money Mark & Aaron Kaplan
| length24 =2:41
| title25 =Car Chase...Dancing On Boobies
| extra25 =Christopher Lennertz, Mike McCready, Money Mark & Chris Chaney
| length25 =1:26
| title26 =Confessing To Gregory Cheapshot & Aaron Kaplan
| length26 =1:48
| title27 =Harkin Goes Down...I Fucked Your Wife
| extra27 =Christopher Lennertz, Mike McCready & Aaron Kaplan
| length27 =3:18
| title28 =Oh Fuck
| extra28 =Christopher Lennertz, Money Mark & Chris Chaney
| length28 =1:53
| title29 =Pissing In A Playground
| extra29 =Christopher Lennertz, Chris Chaney, Money Mark & Victor Indrizzo
| length29 =3:07
| title30 =Tour Of The Mouth
| extra30 =Christopher Lennertz, Money Mark, Victor Indrizzo & Dave Levita
| length30=3:21
| title31 =Your Balls Are So Smooth
| extra31 =Christopher Lennertz, Money Mark, Victor Indrizzo & Chris Chaney
| length31 =3:04
| title32 =Murdering Some Ass
| extra32 =Christopher Lennertz, Mike McCready, Money Mark & Chris Chaney
| length32 =7:12
| title33 =This Is How I Roll
| extra33 =Money Mark
| length33 =2:20
}}

==Release==
  premiere in August 2011]] world premiere of Horrible Bosses took place on June 30, 2011 at Graumans Chinese Theatre in Hollywood|Hollywood, California. 

===Box office===
Horrible Bosses has earned $117,538,559 (56.1%) in the North America, and a further $92,100,000 (43.9%) in other territories, for a worldwide box-office gross of $209,638,559 since release. 

;North America The Stepford The War of the Roses ($86.8M) to become the highest grossing dark/black comedy film in unadjusted dollars. 

;Other territories
Horrible Bosses was released on July 7, 2011, in the United Arab Emirates ($258,108), and on July 8 in Estonia ($24,471), Latvia ($15,750), Lebanon ($36,316) and Lithuania ($13,676), grossing $348,321 for the opening weekend  and accruing a total of $855,009 in the first 17 days.  On the weekend of July 21–24, the film opened in the United Kingdom ($3,386,876), Greece ($367,845), Israel ($200,372), South Africa ($193,632), Norway ($109,252) and East Africa ($7,324). 

===Critical reception===
 
Horrible Bosses received generally positive reviews from critics. On Rotten Tomatoes, the film holds a rating of 69%, based on 206 reviews, with an average rating of 6.2/10. The sites critical consensus reads, "Its nasty, uneven, and far from original, but thanks to a smartly assembled cast that makes the most of a solid premise, Horrible Bosses works."  Review aggregate Metacritic gave the film a score of 57 out of 100, based on 40 critics, indicating "mixed or average reviews". 

Roger Ebert gave the film three and a half stars out of&nbsp;four, calling it "well-cast" and commending it for playing to each actors strengths. Ebert gave particular praise to Spacey, labeling him "superb", and Aniston, judging her performance to be a "surprise" and a return to form, stating "she has acute comic timing and hilariously enacts alarming sexual hungers". Ebert called Horrible Bosses "cheerful and wicked".  Lisa Schwarzbaum of Entertainment Weekly reacted positively, calling the film "a&nbsp;bouncy, well-built, delightfully nasty tale of resentment, desperation, and amoral revenge" and complimented the casting of the protagonists and antagonists.  The A.V. Club s Nathan Rabin also praised the cast, stating that the picture "succeeds almost entirely on the chemistry of its three leads, who remain likeable even while resorting to homicide", adding the "acting more than compensates for the films other failings." Rabin singled out Days performance as "a&nbsp;potent illustration of how a brilliant character actor with a spark of madness can elevate a ramshackle lowbrow farce into a solid mainstream comedy through sheer force of charisma."  Edward Douglas of ComingSoon.net credited director Seth Gordon with having assembled "the perfect cast", claiming "the six leads kill in every scene", but echoed Nathan Rabins sentiments that Day is the "real standout". Douglas summarized the picture as "dark fun that works better than expected due to a well-developed script, an impeccable cast and a director who knows how to put the two together".  A. O. Scott of The New York Times stated "the timing of the cast...is impeccable" and appreciated that the script did not attempt "to cut its coarseness with a hypocritical dose of sweetness or respectability". The review concluded that "in the ways that count and even when it shouldn’t, Horrible Bosses works." 

USA Today s Scott Bowles awarded the film three out of four stars, labeling it a "surprising comedy that rivals Bridesmaids (2011 film)|Bridesmaids as the funniest film of the summer, if not the year." Bowles added that "the characters are so likable", giving particular credit to Sudeikis though also adding praise for the performances of Bateman and Day. The dialogue was also lauded by Bowles, which commented that "Seth Gordon has a deft touch with water-cooler talk—even when the water cooler might be spiked with poison."  Leonard Maltin of indieWire considered Day to have had the "breakout role" and offered praise to the performances of the cast, but lamented the lack of screen time for Farrells character. Maltin concluded "the movie has just enough raunchiness to identify it as a 2011 comedy, just enough cleverness to admire, and just the right camaraderie among its three male stars, which turns out to be the movie’s greatest strength."  Rolling Stone s Peter Travers gave kudos to the "killer cast", with specific credit given to Bateman and Day, but was critical of the movie, stating "it wussies out on a sharp premise" and that it is a "hit-and-miss farce that leaves you wishing it was funnier than it is"  The Guardian s Philip French called Horrible Bosses "a&nbsp;lumbering, misogynistic affair", but admitted "I&nbsp;laughed frequently, probably to the detriment of my self-respect."  Nicholas Barber of The Independent gave a positive review, complimenting Gordon for not allowing the actors improvisation to be detrimental to the pacing, but felt the movie was not as "dark" as its premise required, saying "what edginess the film does have comes instead from the inordinate quantity of swearing, plus a smattering of homophobia and misogyny." 

Salon.com|Salon s Andrew OHehir offered a mixed response, characterizing the film as a "lot funnier in theory than in practice, but it wont ruin your Saturday night". Salon appreciated the "effortless comic chemistry" between Sudeikis, Bateman and Day and singled out Bateman, Aniston and Spacey for their performances. OHehir was however critical of the perceived homophobia, sexism and racism.  The Hollywood Reporter s Kirk Honeycutt responded negatively, stating the jokes failed to be funny, stating "Seth Gordon shows no flair for turning the absurdities and cartoonish characters in the script...into anything more than a collection of moments in search of laughs."  Karina Longworth of The Village Voice was critical of the premise, which she felt lacked any legitimate "rage" against the characters bosses, stating "...theres every sign that, even without these particular emasculators, Dale, Kurt and Nick would still be—for lack of a better word—total pussies." Longworth felt that the humor was "rarely actually laugh-out-loud funny, and never truly dark or daring". She particularly criticized the all-white, male protagonists and a plot she deemed racist and filled with "stereotypes".  Justin Chang of Variety (magazine)|Variety praised the performance of the ensemble cast, but considered the plot to be "predictably moronic, vulgar and juvenile". Chang echoed the sentiments of The Village Voice in lamenting that the film failed to pursue the premise to "darker, more daring territory" and faulted it for falling back on "over-the-top comic exaggeration". 

===Accolades=== Satellite Award Comedy Awards, including Comedy Actor for Bateman, Comedy Actress for Aniston, and best Comedy Film.  Farrell and Aniston were both nominated for Best On-Screen Dirt Bag at the 2012 MTV Movie Awards, with Aniston claiming the award. Farrell also received a nomination for Best On-Screen Transformation. 
{| class="wikitable"
|- style="text-align:center;"
! colspan=6 style="background:#B0C4DE;" | List of awards and nominations
|- style="text-align:center;"
! style="background:#ccc;" width="10%"| Year
! style="background:#ccc;" width="25%"| Award / Film Festival
! style="background:#ccc;" width="30%"| Category
! style="background:#ccc;" width="40%"| Recipient(s) and nominee(s)
! style="background:#ccc;" width="15%"| Result
! scope="col" class="unsortable"| Ref.
|- style="border-top:2px solid gray;"
|-
|rowspan="3"| 2011
| Broadcast Film Critics Association|Critics Choice Awards
| Best Comedy Film
| Horrible Bosses
| 
| style="text-align:center;" | 
|-
| Satellite Award Best Supporting Actor - Motion Picture
| Colin Farrell
| 
| style="text-align:center;" | 
|-
| Teen Choice Awards
| Choice Summer Movie
| Horrible Bosses
|  
| style="text-align:center;" | 
|-
|rowspan="9"| 2012 Artios Awards
| Outstanding Achievement in Casting - Big Budget Comedy Feature
| Horrible Bosses - Lisa Beach and Sarah Katzman
| 
| style="text-align:center;" |   
|- BMI Film & TV Awards
| Film Music Award
| Christopher Lennertz
|  
| style="text-align:center;" | 
|-
| Golden Trailer Awards
| Best Comedy TV Spot
| Horrible Bosses, Warner Bros., Seismic Productions 
|  
| style="text-align:center;" | 
|-
|rowspan="3"|  MTV Movie Awards
| Best On-Screen Dirt Bag
| Colin Farrell
| 
| style="text-align:center;" rowspan="3" |     
|-
| Best On-Screen Dirt Bag
| Jennifer Aniston
| 
|-
| Best On-Screen Transformation
| Colin Farrell
| 
|-
|rowspan="3"| The Comedy Awards
| Comedy Actor - Film
| Jason Bateman
| 
| rowspan="3" style="text-align:center;" | 
|-
| Comedy Actress - Film
| Jennifer Aniston
| 
|-
| Comedy Film
| Horrible Bosses
| 
|-
|}

===Home media===
On July 26, 2011, FX (TV channel)|FX obtained the rights to the network premiere of the film. 
 Green Lantern, The Lion King.  As of  , it has sold an estimated 1.3 million units and earned $18.3 million. 
 Green Lantern, Warner Bros. included a code that allows the owner to access a version of the film via UltraViolet (system)|UltraViolet, a cloud storage service which allows streaming or downloading to a variety of devices. 

==Sequel==
 

  was announced as Gordons replacement, with John Morris joining the production as a producer. The pair had previously performed a rewrite on Goldsteins and Daleys sequel script.  Filming had been scheduled to begin in summer 2013, but began in November 2013.   Foxx, Jennifer Aniston, and Kevin Spacey will reprise their roles, with Christoph Waltz and Chris Pine joining the cast. 

==References==
{{Reflist|30em|refs=
   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

}}

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 