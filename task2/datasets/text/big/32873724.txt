Kadal Pookkal
{{Infobox film
| name           = Kadal Pookkal
| image          =  Bharathiraja
| producer       = Sivasakthi Pandiyan
| writer         =  Murali Manoj Manoj Sindhu Menon Uma Pratyusha Deva
| cinematography = B. Kannan
| editing        = Pazhanivel
| distributor    = 
| released       = December 14, 2001
| runtime        =
| country        = India Tamil
}}
 Tamil drama Bharathiraja which Murali and Manoj in the leading roles, with Sindhu Menon, Pratyusha and Uma in supporting roles.      The film was delayed several times before release but opened to critical acclaim with Bharatiraja winning the National Film Award for Best Screenplay and Murali winning the Tamil Nadu State Film Award for Best Actor.

==Plot==
Karuthayya and Peter (Murali - Manoj) are the best of friends, sharing with each other their hopes and dreams. Karuthayyas plan, to get his beloved sister Kayal (Uma) married elsewhere, is thwarted when he learns that Kayal is in love with Peter. He hears this from Uppili (Pratyksha) who has a soft corner for Karuthayya and vice versa. For his sisters sake, Karuthayya approaches Peter with the marriage proposal. A shocked Peter, who had all along shared an easy relationship with Uma, consents for the sake of friendship. Peter then makes a counter proposal - that Karuthayya marry his sister Maria. Peter however withholds the fact that Maria had been seduced by a man from the city and was carrying his child. Family honour being uppermost in his mind, friendship takes a back seat. But Peter does not know that Karuthayya was already aware of his sisters affair, though not of her pregnancy. Karuthayya knowing his friends aggressive nature had not told Peter about the matter earlier. Karuthayya now throws the ball back in Peters court, asking him to first get Marias consent. Sure that Maria would not give her consent for the marriage. But to his shock Maria agrees.

Forced by her mother and Peter to toe the line, she had no other option. The two pairs get married. While Peter and Kayal are blissfully happy, Karuthayya and Maria have their own crosses to bear. A guild-stricken Maria finally confesses to Karuthayya about Peters treacherous act. A shocked and hurt Karuthayya goes to confront Peter. The man from the city appears on the scene. A furious Peter takes the unsuspecting youth on a boat-ride, ties a stone on him and dumps him into the sea. Karuthayya who reaches the place tries to save the guy.

==Cast== Murali as Karuthaiya Manoj as Peter
*Sindhu Menon
*Pratyusha as Uppili
*Umashankari as Kayal
*Shyam Ganesh Puthu Kavithai Jyothi
*Vaiyapuri
*Santhana Bharathi
*Janagaraj Anjathe Sridhar

==Soundtrack==
* Aadumeyuthe - Krishnaraj, Sathya
* Alai Alai Alaikadale - Srinivas, Unni Menon, Swarnalatha
* Paithiyamaanene - Unnikrishnan, Harini
* Ogama - Malaysia Vasudevan, Ganga
* Siluvaigale - Unni Menon, Anuradha Sriram

==References==
 

 

 
 
 
 
 
 