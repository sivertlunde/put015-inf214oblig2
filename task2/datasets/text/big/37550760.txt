Coast of Skeletons
 
 
{{Infobox film
| name           = Coast of Skeletons
| image          = Coast_of_Skeletons_US_1965_poster.jpg
| image_size     =
| caption        = US poster from 1965 Robert Lynn
| producer       = Harry Alan Towers
| screenplay     = Anthony Scott Veitch
| story          = Harry Alan Towers
| based on       =  
| starring       = Richard Todd Dale Robertson Heinz Drache Marianne Koch
| music          = Christopher Whelen
| cinematography = Stephen Dade
| editing        = John Trumper
| studio         = Towers of London Films S.A. Film Studios
| distributor    = Towers of London
| released       =  
| runtime        = 91 min.
| country        = United Kingdom South Africa
| language       = English
}}
 1965 British-South Robert Lynn 1935 film based on the novel, but placed in a totally different story.

==Plot== Commissioner Harry dredge for diamonds in the shallow waters off South West Africa.

Sanders soon finds himself drawn into a web of insurance fraud, a secret hunt for World War II gold bullion, and a rivalrous love triangle between the flamboyant American diamond prospecting|prospector, a former German U-Boat commander in the employ of the American, and the German’s very young wife.

==Main cast==
* Richard Todd as Commissioner Harry Sanders 
* Dale Robertson as A.J. Magnus 
* Heinz Drache as Janny von Koltze 
* Marianne Koch as Helga 
* Elga Andersen as Elisabeth von Koltze 
* Derek Nimmo as Tom Hamilton 
* Gabriel Bayman as Charlie Singer  George Leech as Carlo Seton 
* Gordon Mulholland as Mr. Spyker 
* Josh du Toit as Hajo Petersen
* Dietmar Schönherr as Piet van Houten

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 