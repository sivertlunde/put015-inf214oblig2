A Farewell Song
 
{{Infobox film
| name           = A Farewell Song
| image          = A Farewell Song still.jpg
| image size     = 
| alt            = 
| caption        = Promo shot from A Farewell Song
| director       = Arthur Jones, Luther Jones
| producer       = Luo Tong
| writer         = 
| narrator       = 
| starring       = Luo Shoucheng, Weng Zhenfa (翁鎮發), Tu Weigang, Chen Dawei
| music          = Chen Dawei and others
| cinematography = 
| editing        = 
| studio         = LostPensivos Films
| distributor    = 
| released       = 2006
| runtime        = 76 min. (original cut), 52 min. (DVD cut)
| country        = 
| language       = Mandarin
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

A Farewell Song is a 2006 documentary film about a group of retired Chinese musicians who reunite to perform a series of concerts outside of the state-backed music system. It was directed by Arthur Jones and Luther Jones, and supported by the British Documentary Film Foundation (BRITDOC).

==Synopsis==
The film is centred on the lives of traditional Chinese musicians,  ; Chen Dawei enjoys time with his young son; and Weng Zhenfa continues his reinvention of his instrument (the sheng) with master craftsmen at a local workshop.

Composer Chen Daweis sudden death takes them all by surprise and the second half of the film follows the remaining musicians attempts to organize a tribute concert in his name.

The film was shot over three years in Shanghai, and backed by the Channel 4 BRITDOC Foundation. It won the Special Jury Prize for documentaries at the 2006 Syracuse International Film Festival. It has also played at BRITDOC Film Festival, Globians Film Festival, Santa Cruz International Film Festival, Cologne Chinese Film Festival, Pyongyang Film Festival, Chongqing Independent Film and Video Festival, and Action on Film International Film Festival.  

== Soundtrack ==

{{Infobox album  
| Name        = A Farewell Song – The Soundtrack
| Type        = soundtrack
| Artist      = Various Artists
| Cover       = A Farewell Song soundtrack.jpg
| Alt         = 
| Released    = 2008
| Recorded    = 2006
| Genre       = Chinese traditional music
| Length      = 55 min.
| Label       = LostPensivos Films
}}

The soundtrack for A Farewell Song (A Farewell Song – The Soundtrack) consists of complete versions of all the music featured in film. All the music was recorded live during filming, with one of the tracks ("A Farewell Song") coming from a studio recording that features in the film. All tracks are ancient tunes, rearranged by the musicians themselves.

=== Tracking listing ===

# "Festival of the Flaming Torch" (Chen Dawei) – 3:51
# "Pine in the Snow Storm" (Luo Shoucheng) – 1:54
# "A Farewell Song" (Chen Dawei) – 6:11
# "The Rivulet" (Chen Dawei) – 3:19
# "Three Six" (Chen Jingchao) – 3:54
# "A Hundred Birds Pay Homage to the Phoenix" (Liu Ying) – 7:07
# "The Moon Mirrored in the No. Two Spring" (Hua Yanjun) – 6:45
# "The Jin Tune" (Weng Zhenfa) – 5:36
# "Ambush on all Sides" (Tu Weigang) – 6:45
# "Night Bell from the Old Temple" (Chen Dawei) – 9:36

==See also==
* Chinese traditional musical instruments
* Chinese musicology
* Guoyue
* Music of China
* Chinese orchestra

==References==
 

==External links==
*  
*   at the Internet Movie Database
*   on Directors Notes
*  
*   on iTunes

 
 
 
 