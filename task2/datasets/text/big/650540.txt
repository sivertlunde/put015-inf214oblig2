Dracula: Dead and Loving It
 
{{Infobox film
| name = Dracula: Dead and Loving It
| image = Drac_dead_and_loving_it.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Mel Brooks
| producer = Mel Brooks Executive: Peter Schindler
| screenplay = Mel Brooks Rudy De Luca Steve Haberman
| story = Rudy De Luca Steve Haberman
| based on =   Steven Weber Amy Yasbeck Lysette Anthony Harvey Korman
| music = Hummie Mann
| cinematography = Michael D. OShea
| editing = Adam Weiss Gaumont Mel Brooksfilms Castle Rock Entertainment
| distributor = Columbia Pictures
| released =  
| runtime = 90 minutes  
| country = United States France
| language = English German
| budget = $30 million
| gross = $10.7 million 
}} satirical Horror comedy horror spoof of Bram Stokers novel Dracula, and of some of the films it inspired. As of 2015, this is the most recent film Brooks has directed.
 Van Helsing. Steven Weber, Amy Yasbeck, Peter MacNicol, Harvey Korman, and Anne Bancroft.
 Hammer Horror films. It spoofed, among other films, The Fearless Vampire Killers (1967) and Bram Stokers Dracula (1992).

In the film references are made to fictitious books Transavia Folk Law, The Theory and the Theology of the Evil Undead, same name released in 1922.

==Plot==
  Thomas Renfield travels all the way from London to "Castle Dracula" in Transylvania to finalize Count Draculas purchase of Carfax Abbey in England. As the sun sets, and the stagecoach driver refuses to take him any further, Renfield continues on foot despite the villagers pleading with him to turn back.

Renfield arrives safely and meets Count Dracula, a charming but rather strange man who (unbeknownst to him) is a vampire. He then casts a hypnotic spell on the highly suggestible Renfield, making him his slave. Dracula and Renfield soon embark for England. During the voyage, Dracula dines upon the ships crew. When the ship arrives and Renfield, (by this time raving mad in the style of Dwight Frye), is discovered alone on the ship, he is confined to a lunatic asylum.
 administrator and Mina (Sewards Lucy (Sewards equally nubile Ward (law)|ward). Dracula flirts with Lucy and, later that night, enters her bedroom and feeds on her blood.

The next day, Mina discovers Lucy still in bed late in the morning, looking strangely pale. Seward, puzzled by the odd puncture marks on her throat, calls in an expert on obscure diseases, Dr. Abraham Van Helsing. Van Helsing informs the skeptical Dr. Seward that Lucy has been attacked by a vampire. After some hesitation, Seward and Harker allow garlic to be placed in Lucys bedroom to repel the vampire. After a failed attempt by Renfield to remove the garlic, Dracula uses mind-control to make Lucy leave her room, and kills her in the garden. Despite Van Helsings warnings, Seward refuses to believe him.

Van Helsing meets Dracula and begins to suspect him of being the local vampire after the two trade words, phrases and insults in Moldavian, each attempting to have the last word in the foreign language discussion. Lucy, now a vampire herself, rises from her crypt, drains the blood from her guard, and tries to attack and seduce Harker, who had come to watch over her grave to be sure if Van Helsing was indeed right. Van Helsing rushes in just in time and chases her back to her coffin with a crucifix. Jonathan drives a stake into Lucys heart, allowing her to at last rest in peace.

Draculas next victim is Mina, but he has bigger plans for her; he wants her to be his undead bride throughout eternity. He spirits her away to Carfax Abbey, where they dance, and he sucks her blood. The following morning, she is unusually frisky, and tries to seduce the prudish Jonathan. Dr Seward mistakenly assumes Jonathon to be seducing Mina and orders him to leave. However, Van Helsing becomes suspicious at this strange behavior. Noticing a scarf around Minas neck, he removes it, revealing two puncture marks. Though she lies about how she got them, Van Hesling confirms she has been attacked by a vampire by placing a cross on her hand, which burns a mark into it.

Van Helsing devises a plan to reveal the vampires secret identity. Both Dracula and Renfield are invited to a Ball (dance)|ball, where Van Helsing has placed a huge mirror, covered with a curtain, on one of the walls. While Dracula and Mina perform an excellent dance routine the curtain over the mirror is dropped, and guests are stunned to see that Dracula has no reflection. Dracula grabs Mina and escapes out a window.

Van Helsing deduces that Renfield is Draculas slave, and thus might know where he has taken his coffin after a search of Carfax turns up empty. He lets him out of his cell, and the three men secretly follow him to Draculas lair. Once discovered, the Count locks himself in a room to finish making Mina his bride. His pursuers break down the door, and they fight. Van Helsing, noticing sunlight creeping into the room, starts opening the blinds. As his body begins to burn, Dracula transforms himself into a bat to flee, but is inadvertently killed by the inept Renfield opening a panel in the roof.

With Dracula finally vanquished, Renfield falls into despair with no master to serve and scrapes Draculas ashes into the coffin. Seward tells him "You are free, now", and he realizes this to be true with Dracula gone, and seems relieved. But the instant Dr. Seward calls for Renfield to follow him out of the church, he follows with "Yes, Master". Van Helsing dusts himself off, opens Draculas coffin and yells something in Moldavian to ensure that he has the final word between himself and the count.
 after the end credits roll, he makes one final statement in the same language, at which point the film fades entirely to black.

==Cast==
 
* Leslie Nielsen as Count Dracula
* Mel Brooks as Dr. Abraham Van Helsing Thomas Renfield Steven Weber as Jonathan Harker Mina Seward
* Lysette Anthony as Lucy Westenra
* Harvey Korman as John Seward|Dr. Seward
* Anne Bancroft as Madame Ouspenskaya (Gypsy Woman)
* Ezio Greggio as the coachman
 
* Megan Cavanagh as Essie
* Chuck McCann as Innkeeper
* Mark Blankfield as Martin
* Clive Revill as Sykes
* Gregg Binkley as Woodbridge
* Rudy De Luca as Guard
* Richard Steven Horvitz as Scientist #11
* Avery Schreiber as Male Peasant on Coach
* Cherie Franklin as Female Peasant on Coach 
 

==Production==
Principal photography began in May 1995 and wrapped in September 1995.

==Reception==
Critical reaction to Dracula: Dead and Loving It has been overwhelmingly negative, with the film earning a "rotten" rating of only 11% on Rotten Tomatoes based on 36 reviews and a 3.1/10 rating. 

 . Its a toothless parody that misses more often than it hits. ... Given the comic turn his career has taken since the early 80s, its hard to believe that Leslie Nielsen was once a serious actor. These days, thanks to the Zucker brothers  , he has become an accomplished satirical performer. His sense of timing is impeccable, and this asset has made him a sought-after commodity for a wide variety of spoofs. Here, Nielsen takes on the title role, but his presence cant resurrect this stillborn lampoon. Unless youre a die hard Mel Brooks fan, theres no compelling reason to sit through Dracula: Dead and Loving It. The sporadic humor promises some laughs, but the ninety minutes will go by slowly." 

Joe Leydon of Variety (magazine)|Variety wrote: "Leslie Nielsen toplines to agreeable effect as Count Dracula, depicted here as a dead-serious but frequently flustered fellow whos prone to slipping on bat droppings in his baroque castle. ... Trouble is, while Dead and Loving It earns a fair share of grins and giggles, it never really cuts loose and goes for the belly laughs. Compared with the recent glut of dumb, dumber and dumbest comedies, Brookss pic seems positively understated. Indeed, there isnt much here that would have seemed out of place (or too tasteless) in comedy sketches for TV variety shows of the 1950s. ... As a result, unfortunately, Dead and Loving It is so mild, it comes perilously close to blandness. ... The only real sparks are set off by MacNicol as Renfield, the solicitor who develops a taste for flies and spiders after being bitten by Dracula." 

Despite the reviews, the film, like many of Brookss later films, has gained a cult following in later years. 

===Box office===
The film debuted at #10.  By the end of its run, Dracula grossed $10,772,144.   Retrieved October 14, 2013 

==See also==
* Vampire film

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 