Les Deux Timides
 Les Deux Timides}}

{{Infobox film
| name           = Les Deux Timides Two Shy Souls
| image          = 
| image_size     =
| caption        =
| director       = René Clair
| producer       = Alexandre Kamenka
| writer         = Play: Eugène Marin Labiche|Eugène Labiche Screenplay: René Clair
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Films Albatros
| released       = (France) 1 March 1929 (USA) 5 December 1928
| runtime        = 72 minutes (19f/s)
| country        = France
| language       = Silent
| budget         =
}}
 1928 French silent film comedy directed by René Clair, and based on the 1860 play Les Deux Timides by Eugène Marin Labiche|Eugène Labiche.               

==Plot==
A very shy lawyer, Fremissin, is tasked with defending Garadoux, a man charged rightfully with beating his wife. Fremissin gets nervous at the trial, and ends up demanding the harshest possible sentence for Garadoux, making him spend several months in prison. After a few years, Fremissin has fallen in love with a woman (Cecile Thibaudier). Garadoux sees this and tries to seduce her to get back at Fremissin for getting him sent to prison. Garadoux abuses Fremissins timid nature, in hilarious acts like posing as a bandit and leaving him disturbing notes telling him not to leave home. After various trials, and meeting his shy counterpart in Ceciles father, Fremissin finally gets to Cecile in time to ask for her hand in marriage, and has a big fight with the Thibaudier and Garadoux family. He then defends the Thibaudier family successfully in court.

==Cast==
* Pierre Batcheff as Fremissin
* Jim Gérald as Garadoux
* Véra Flory as Cecile Thibaudier
* Maurice de Féraudy as Thibaudier

==References==
 

==External links==
* 

 
 
 
 
 


 
 