Our New Errand Boy
{{Infobox film
| name           = Our New Errand Boy
| image          = OurNewErrandBoy.jpg
| image_size     = 
| caption        = Screenshot from the film James Williamson
| producer       =
| writer         = 
| narrator       =
| starring       = Tom Williamson James Williamson
| music          =
| cinematography = James Williamson
| editing        =
| studio         = Williamson Kinematograph Company
| distributor    =
| released       =  
| runtime        = 5 mins 46 secs
| country        = United Kingdom Silent
| budget         =
}}
 1905 UK|British short  silent comedy James Williamson, about a new errand boy, engaged by a grocer who soon regrets the appointment. This "relatively unambitious" chase comedy, according to Michael Brooke of BFI Screenonline, "is one of a number of Williamson films featuring a mischievous child, played by the directors son Tom." "Although essentially a series of sketches," this film, according to David Fisher, "demonstrates the extent to which Williamson had developed film technique For a start, the film has a title frame, which includes the logo of the Williamson Cinematograph Company," and, "the chase section anticipates the American comedies of the next decade."      

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 