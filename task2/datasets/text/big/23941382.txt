Immortals (2011 film)
 
 
{{Infobox film
| name           = Immortals
| image          = Immortals poster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Tarsem Singh
| producer       = {{plainlist|
* Gianni Nunnari
* Mark Canton
* Ryan Kavanaugh }}
| writer         = {{plainlist|
* Vlas Parlapanides
* Charley Parlapanides }}
| starring       = {{plainlist|
* Henry Cavill
* Stephen Dorff Luke Evans
* Isabel Lucas
* Kellan Lutz
* Freida Pinto
* Mickey Rourke }}  Trevor Morris
| cinematography = Brendan Galvin
| editing        = {{plainlist|
* Wyatt Jones
* Stuart Levy }}
| studio         = {{plainlist|
* Relativity Media
* Virgin Produced Rogue }}
| distributor    = Relativity Media   Universal Pictures  
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $75 million   
| gross          = $226,904,017 
}}
 3D Greek mythology fantasy Luke Evans, Joseph Morgan, Stephen Dorff, Daniel Sharman, Alan van Sprang, Isabel Lucas, Corey Sevier, and John Hurt. The film was previously named Dawn of War and War of the Gods before being officially named Immortals, and is loosely based on the Greek myths of Theseus and the Minotaur and the Titanomachy.

Principal photography started on April 5, 2010 in Montreal, and the film was released in 2D and in Stereoscopy|3-D (using the Real D 3D and Digital 3D formats) on November 11, 2011 by Universal Pictures and Relativity Media. 

==Plot== gods while Titans and Hyperion (Mickey Phaedra (Freida Pinto), believing that she can use her visions to find the Epirus Bows resting place.
 Joseph Morgan). Theseus ably battles multiple opponents until the Athenian officer Helios (Peter Stebbings) intervenes and discharges Lysander from the army for his actions. Lysander travels to Hyperion, offering his service and the villages location. Hyperion accepts, but hammers Lysanders groin for being a traitor. Hyperions forces attack Theseuss village, murdering the villagers and Aethra, and taking Theseus captive.
 Luke Evans) where he meets with his fellow gods Athena (Isabel Lucas), Poseidon (Kellan Lutz), Ares (Daniel Sharman), Apollo (Corey Sevier), and Heracles (Steve Byers). Zeus warns them not to interfere in mortal affairs as gods where Zeus believes that until the Titans are released, they must have faith in mankind to defeat Hyperion. Theseus is enslaved alongside the thief Stavros (Stephen Dorff). Phaedra, who is held captive nearby, sees a vision of Theseus. Phaedra organizes a riot, using the chaos to escape with Theseus, Stavros, and the other slaves. Theseus decides to pursue Hyperion and attempts to capture a boat, but he and his allies are overwhelmed by Hyperions forces. Poseidon purposefully falls from Olympus into the ocean causing a tsunami that wipes out Hyperions men. Phaedra sees another vision of Theseus standing near a shrouded body. She determines that Theseus must return home to bury Aethra.

While laying Aethra to rest, Theseus discovers the Epirus Bow embedded in nearby rock. He frees the Epirus Bow, but is attacked by Hyperions henchman the Minotaur (Robert Maillet). Theseus kills the Minotaur and uses the Bow to save his allies from being executed before collapsing from poisoned scratches inflicted by the Minotaur. Phaedra tends to Theseus and later falls in love with him and they make love to each other, stripping her of the visions she deemed a curse. The group returns to Phaedras temple while Hyperion and his forces are away at Mount Tartarus. At the temple, Stavros and Theseus are lured into an ambush and Theseus loses the Epirus Bow. Outnumbered by Hyperions men, Ares directly intervenes to save Theseus and Athena provides the men with horses to reach Mount Tartarus. Zeus arrives and angrily kills Ares for disobeying his command. Zeus warns Theseus that he and his allies will receive no more help from the gods and he must justify the faith Zeus has in him alone. Before leaving with Athena, Zeus tells Theseus to prove him right. The lost Epirus Bow is brought to Hyperion.

Theseus, Stavros, and Phaedra travel to Mount Tartarus. Theseus tries in vain to warn Greeks|Hellenics King Cassander (Stephen McHattie) of Hyperions plans, but Cassander dismisses his talk of gods as myth, intending to negotiate peace with Hyperion. The following day, Hyperion uses the Bow to destroy Mount Tartarus seemingly indestructible gate. Theseus leads the Hellenic army to war against the Hyperion forces, killing Lysander. Hyperion ignores the battle, storms through to Mount Tartarus killing Helios and Cassander and uses the Epirus Bow to breach the mountain and free the Titans before Stavros and Theseus can stop him. The force of the release knocks the mortals down. Stavros takes the Epirus Bow and kills a Titan, but is killed by the other Titans. Zeus, Poseidon, Athena, Heracles and Apollo arrive and battle the Titans while Theseus fights Hyperion. Zeus destroys the Epirus Bow, and the gods prove more than a match for the Titans, but they are overwhelmed by their sheer numbers, with all but Zeus and Athena being killed. Theseus kills Hyperion and Zeus collapses Mount Tartarus on the Titans before ascending to Olympus with Athena. The collapsing mountain wipes out Hyperions men. The mortally wounded Theseus is also transported to Olympus for his sacrifice and given a place among the gods.
 Acamas (Gage Munroe). Acamas is met by the old man who informs the child that in the future, he too will one day fight against evil. Acamas sees a vision of the sky filled with gods and Titans fighting with Theseus leading the charge.

==Cast== Luke Evans, Henry Cavill and Isabel Lucas at WonderCon 2011.]]
* Henry Cavill as Theseus, a mortal chosen by Zeus to fight evil.
* Stephen Dorff as Stavros, a cunning slave and master thief who joins Theseus on his quest.  Luke Evans as Zeus, god of the sky and king of the gods.
* John Hurt as Old Man, a disguise used by Zeus to interact with mortals. 
* Isabel Lucas as Athena, goddess of wisdom. 
* Kellan Lutz as Poseidon, god of the sea. 
* Freida Pinto as Phaedra (mythology)|Phaedra, an Oracle priestess who joins Theseus on his quest. 
* Mickey Rourke as King Hyperion (mythology)|Hyperion, the King of Heraklion. Joseph Morgan as Lysander, a traitorous Athenian soldier who joins King Hyperion after being discharged.
* Peter Stebbings as Helios, an Athenian general.
* Daniel Sharman as Ares, god of war.
* Anne Day-Jones as Aethra (Greek mythology)|Aethra, mother of Theseus.
* Greg Bryk as Nycomedes, a monk in service to Phaedra.
* Corey Sevier as Apollo, god of light.
* Steve Byers as Heracles,   the god who forged the Epirus Bow.
* Robert Maillet as The Minotaur, Hyperions powerful henchman who wears a metal bull mask.
* Romano Orzari as Icarus, an Athenian soldier.
* Alan van Sprang as Dareios, a slave who joins Theseus on his quest.
* Stephen McHattie as Cassander, the King of the Hellenics.
* Mark Margolis as The New Priest, a monk. Robert Naylor as Young Theseus.
* Gage Munroe as Acamas (son of Theseus)|Acamas, son of Theseus and Phaedra.
* Tamas Menyhart as Heraklion

==Production==
  classical Ancient Greek myths 3D technology.  Director Tarsem Singh said that he was planning an action film using Renaissance painting styles. He then went on to say that the film is "Basically, Caravaggio meets Fight Club. Its a really hardcore action film done in Renaissance painting style. I want to see how that goes; its turned into something really cool. Im going for a very contemporary look on top of that so Im kind of going with, you know, Renaissance time with electricity. So its a bit like Baz Luhrmann doing Romeo + Juliet in Mexico; its just taking a particular Greek tale and half (make it contemporary) and telling it."   The film had a production budget of $80 million ($75 million after tax rebates)  to $120 million  and cost "at least" $35 million to market.   

===Soundtrack=== Trevor Morris and has been released on 8 November 2011.

{{Track listing
| headline = Track listing
| title1 = Immortal and Divine
| length1= 1:30
| title2 = War in the Heavens
| length2= 2:32
| title3 = Hyperions Siren
| length3= 3:47
| title4 = Witness Hell
| length4= 1:56
| title5 = To Mount Olympus
| length5= 2:54
| title6 = Enter the Oracles
| length6= 2:30
| title7 = Theseus and Phaedra
| length7= 1:37
| title8 = Poseidons Leap
| length8= 1:24
| title9 = This Is Your Calling
| length9= 1:31
| title10 = Theseus Fights the Minotaur
| length10= 2:13
| title11 = Theseus Fires the Bow
| length11= 2:16
| title12 = My Own Heart
| length12= 3:03
| title13 = Zeus Punishment
| length13= 2:27
| title14 = Ride to the Gates
| length14= 1:00
| title15 = In War Fathers Bury Their Sons
| length15= 1:05
| title16 = The Gods Chose Well
| length16= 1:18
| title17 = Fight So Your Name Survives
| length17= 3:07
| title18 = Battle in the Tunnels
| length18= 2:44
| title19 = Immortal Combat
| length19= 3:34
| title20 = Do Not Forsake Mankind
| length20= 4:33
| title21 = Apotheosis
| length21= 1:44
| title22 = Sky Fight/End Credits
| length22= 2:22
}}

==Release==

===Critical reception===
Immortals received mixed reviews. The film holds a 35% "Rotten" rating at the review aggregator website Rotten Tomatoes, based on 114 reviews that average a score of 5 out of 10. The consensus on Rotten Tomatoes states, "The melding of real sets, CG work, and Tarsems signature style produces fireworks, though the same cant be said for Immortals slack, boring storytelling.".  Metacritic assigns the film a weighted average score of 46 (out of 100) based on 23 reviews from mainstream critics, considered to be "mixed or average reviews."  The polls by the market research form CinemaScore reported that the average grade moviegoers gave the film was a "B" on an A+ to F scale and a "B+" from the under-25 crowd.    However, several critics have praised the film. It received an honorable mention from MTV.com as one of the years best films  as well as making Guy Lodges top twenty films of 2011 list on HitFlix.  Furthermore it was on Toro Magazines Top Ten list  as well as Glasgow To The Movies Top Ten Films of 2011.  Marc Eastman, of Are You Screening.com, named IMMORTALS the #3 film of 2011.   It also was nominated for several Saturn Awards, including Best Fantasy Film. 

===Box office===
In North America, it was released on November 11, 2011. Immortals had a $1.4 million midnight showings and then grossed a total of $14.8 million on its opening day, topping the daily box office.  It then finished the weekend of November 11–13, 2011 at #1 with $32.2 million, ranking as Relativity Medias biggest opening weekend to date, against newcomers J. Edgar and Jack and Jill.  3D showings accounted for a substantial 66% of the weekend gross. The films audience was 60 percent male, 75 percent under the age of 35. 

Outside North America, it earned $38 million overseas from 35 countries on its opening weekend. Its highest-grossing territories were Russia ($8.2 million), China ($5.7 million) and South Korea ($4.5 million).  The film has earned $83,504,017 in the United States and Canada and $143,400,000 in other countries, for a worldwide total of $226,904,017. 

===Accolades===
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient!! Result !! Ref.
|-
|rowspan="3"| 2012
|rowspan="3"| Saturn Awards
| Best Fantasy Film
| Immortals
| 
| rowspan="3" style="text-align:center;"|   
|-
| Best Production Design
| Tom Foden
| 
|-
| Best Make Up
| Annick Chartier, Adrien Morot, and Nikoletta Skarlatos
| 
|-
|}

===Home media===
Immortals was released on DVD, Blu-ray Disc, and Blu-ray 3D on March 5, 2012 in the United Kingdom and on March 6, 2012 in the United States and Canada.  In its first week of release 20th Century Fox Home Entertainment sold more than 1.2 million units of the film  making it the weeks #1 film in Home Entertainment.  It sold 648,947 DVD units for a total of $11,116,462 and 926,964 Blu-ray Disc units for a total of $21,310,902 for the week ending March 11, 2012.   An additional 100,000 3-D units sold totaling almost $40,000,000 in home entertainment sales in its first week of release in the US.

===Other media===
Archaia Press released a graphic novel tie-in. Called Immortals: Gods and Heroes, the hardcover book featured new stories that expanded on the universe established in the film. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 