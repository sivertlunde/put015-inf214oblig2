Daddy Day Camp
 
{{Infobox film name            = Daddy Day Camp image           = Daddy day campmp.jpg caption         = Theatrical release poster director        = Fred Savage producer        = William Sherak Jason Shuman screenplay      = Geoff Rodkey J. David Stem David N. Weiss story  Joel Cohen Alec Sokolow based on        =   starring        = Cuba Gooding Jr. Lochlyn Munro Richard Gant Tamala Jones Paul Rae Brian Doyle-Murray music  Jim Dooley cinematography  = Geno Salvatori editing         = Michael Aller studio  Davis Entertainment Company Blue Star Entertainment distributor     = TriStar Pictures released        =   runtime         = 89 minutes country         = United States language        = English budget          = $6 million gross           = $18,197,398
}}
Daddy Day Camp is a 2007 American comedy film starring Cuba Gooding Jr., and directed by Fred Savage in his directorial debut. It is the sequel to the 2003s Daddy Day Care, with a recast of Eddie Murphy and the other characters that appeared in the original film. The film was produced by Revolution Studios and released by TriStar Pictures. The film was released in the United States on August 8, 2007 and received extremely negative reviews from critics, who panned the films use of gross humor. The film became a box office failure and only managed to gross $18.2 million compared to its predecessor Daddy Day Care, which grossed $164 million worldwide.

==Plot==
Four years after the events of Daddy Day Care, Charlie and Phil take their kids to Camp Driftwood, a camp they attended as kids. But once there, they discover that Camp Driftwood is no longer the kindhearted camp site of their time. To save the run down site, Charlie and Phil buy a partnership from the older man that ran it when they were children, after the other partner runs off on vacation. They turn it into Daddy Day Camp. They run into misadventures along the way when the owner, Lance Warner, in the rival camp Canola tries to tear it down. The first day of camp turns out to be a disaster involving a skunk and a bathroom explosion, which leaves them left with only 7 campers instead of the original 35 and in need of help to improve their financial situation. 

When the camp is raided by Camp Canola, which has been joined by the 28 campers who left Camp Driftwood, Charlie calls his military father, Colonel Buck Hinton, for help to whip the kids into shape, since they have problems following orders. After getting revenge on Lance for getting his campers to steal the Camp Driftwood flag, he then challenges Camp Driftwood to the Camp Olympian but the kids have to train for it. The kids love Buck because of his military ways and support,  However, Charlie disapproves as he recounts that he doesnt want the kids to become like Buck because Charlie believes that Buck only cares about toughness and that he, Charlie, was a disappointment to him.  He starts to regret his decision to call Buck when his son runs off to the woods, after some campers tease him about his fathers over-protectiveness, because his grandpa Buck told him that he became tough when he ran off to the woods. They find him but when Charlie complains to Phil about Buck, Buck overhears their conversation and leaves camp.

On the day of the Olympian, the others find out that Buck has left.  Seeing the kids discouraged, Charlie goes to find Buck and bring him back. He finds Buck and resolves all his problems with his dad.  When they return the kids report that they found out that the rival camp is cheating, and have been doing so for several past years; this is especially true when its revealed that Charlie lost to Lance when they were kids. Charlie lets Ben do the climbing course, since Ben knows how to climb, but he falls. However, Becca tells everyone that Lance greased the wall, making everyone realize that Lance cheated in every game in the Olympian. While climbing a wall, Ben uses the tree next to it with enough time left to hit the bell. 

A few minutes later, a wall falls over all of Lances trophies because his son kicks him, for all the times his step father refused to be his dad. Camp Driftwood wins, and the parents who signed their kids to be in Camp Canola originally then ask Charlie for their kids to be in Camp Driftwood, which saves it from foreclosure.

==Cast==
*Cuba Gooding Jr. as Charlie Hinton, the co-owner of Daddy Day Care and teacher. He was played by Eddie Murphy in the original film.
*Lochlyn Munro as Lance Warner, Charlies childhood enemy, and the owner of the rival camp. 
*Richard Gant as Col. Buck Hinton, Charlies estranged father. He is a military officer who takes army tasks very seriously. However, he displays a soft spot for his grandson, Ben, as well as the other campers.
*Tamala Jones as Kim Hinton, Charlies wife. She was played by Regina King in the original film.
*Paul Rae as Phil Ryerson, co-owner of Daddy Day Care, Charlies best friend and teacher. He was played by Jeff Garlin in the original film.
*Josh McLerran as Dale, an oafish young counselor at Camp Driftwood. He is a replacement for the character "Marvin", who was played by Steve Zahn in the original film.
*Spencir Bridges as Ben Hinton, Charlies son. He was played by Khamani Griffin in the original film.
*Brian Doyle-Murray as "Uncle" Morty, former owner of Camp Driftwood. 
*Dallin Boyce as Max Ryerson, Phils son. He was played by Max Burkholder in the original film.
*Telise Galanis as Juliette, one of the campers.
*Molly Jepson as Becca, one of the campers and a student at Daddy Day Care. She was played by Hailey Noelle Johnson in the original film. She is the only child in the camp who appeared in the first film.
*Sean Patrick Flanery as Bobby J, Lances sidekick. 
* Taggart Hurtubise as Carl, the more independent 6 year old brother of Robert. 
* Tad DAgostino as Robert, a shy, nerdy and socially awkward boy. 
*Tyger Rawlings as Billy, a bully. 
*Talon G. Ackerman as Jack, a nerdy boy (and presumably, the youngest of all the campers). He frequently gets sick and vomits.

==Production== Park City and Provo, Utah on August 23 and October 4, 2006.

==Release==

===Box office===
On opening day Daddy Day Camp grossed $773,706, and grossed $3,402,678 on opening weekend on over 2,000 screens. It went on to gross $18.2 million worldwide. 

===Critical reception===
Daddy Day Camp was universally panned by critics, generally for its excessive use of toilet and gross-out humor. Rotten Tomatoes ranked the film 16th in the 100 worst reviewed films of the 2000s, with a rating of 1% on Rotten Tomatoes and 13 on Metacritic. The film received a rare "F" from The A.V. Club.  On its first day of release, the film came in 9th place with United States dollar|$773,706. Its opening weekend totaled $3,402,678 in over 2,000 screens.

Film critic Fred Topel of Hollywood.com is the only critic represented on Rotten Tomatoes "Tomatometer" to give the film a "fresh" (positive) rating.  

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Golden Raspberry Awards Golden Raspberry Worst Actor Cuba Gooding, Jr.
| 
|- Golden Raspberry Worst Screenplay Geoff Rodkey
| 
|-
|J. David Stem
| 
|- David N. Weiss
| 
|- Golden Raspberry Worst Picture William Sherak
| 
|- Jason Shuman
| 
|- Golden Raspberry Worst Director Fred Savage
| 
|- Golden Raspberry Worst Prequel or Sequel
| 
|-
|}

===Home media=== Region 1 in the United States on January 29, 2008, and also Region 2 in the United Kingdom on 18 February 2008, it was distributed by Sony Pictures Home Entertainment.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 