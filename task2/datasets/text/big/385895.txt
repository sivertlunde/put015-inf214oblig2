Alexander Nevsky (film)
 
 
{{Infobox film
| name           = Alexander Nevsky
| image          = Alexander Nevsky Poster.jpg
| caption        = DVD cover Dmitri Vasilyev
| producer       =
| writer         = Sergei Eisenstein Pyotr Pavlenko
| starring       = Nikolai Cherkasov Nikolai Okhlopkov Andrei Abrikosov
| music          = Sergei Prokofiev
| cinematography = Eduard Tisse
| editing        =
| distributor    =
| studio         = Mosfilm
| released       =  
| runtime        = 111 minutes
| country        = Soviet Union
| language       = Russian
| budget         =
}}

Alexander Nevsky ( ) is a 1938 historical drama film directed by Sergei Eisenstein. It depicts the attempted invasion of Novgorod in the 13th century by the Teutonic Knights of the Holy Roman Empire and their defeat by Prince Alexander, known popularly as Alexander Nevsky (1220–1263).
 Dmitri Vasilyev Abrikosov were awarded the Stalin Prize for the film.
 Italian publishing house "Arnoldo Mondadori Editore|A. Mondadori". 

==Plot== Lake Peipus or Chudskoe, they defeat the Teutonic knights. The story ends in the retaken Pskov where the ordinary foot-soldiers are set free, the surviving Teutonic knights will be held for ransom and Tverdilo is swarmed over by the vengeful people (and supposedly torn to pieces).

A subplot throughout the film concerns Vasili Buslai and Gavrilo Oleksich, two famous warriors from Novgorod and friends, who become commanders of the Novgorod forces and who engage in a contest of courage and fighting skill throughout the Battle on the Ice in order to decide which of them will win the hand of Olga Danilovna, a Novgorod maiden whom both of them are courting.  Vasilisa, the daughter of a boyar of Pskov killed by the Germans, joins the Novgorod forces as a front-line soldier, and she and Buslai fight side by side (which makes a strong impression on Buslai); she also personally slays the traitor Ananias.  After both Gavrilo and Buslai have been seriously wounded, Buslai publicly states that neither he nor Gavrilo was the bravest in battle: that honor goes to Vasilisa, and that after her came Gavrilo.  Thus Gavrilo and Olga are united, while Buslai chooses Vasilisa as his bride-to-be (with her unspoken consent).

==Cast==
* Nikolai Cherkasov as Prince Aleksandr Nevsky
* Nikolai Okhlopkov as Vasili Buslaev
* Andrei Abrikosov as Gavrilo Oleksich
* Dmitri Orlov as Ignat, the master armorer
* Vasili Novikov as Pavsha, a voivode of Pskov
* Nikolai Arsky as Domash Tverdislavich, a Novgorod boyar
* Varvara Massalitinova as Amelfa Timoferevna, Buslays Mother
* Vera Ivashova as Olga Danilovna, a maid of Novgorod Aleksandra Danilova as Vasilisa, a maid of Pskov Hermann von Balk, the Grand Master of the Teutonic Order
* Sergei Blinnikov as Tverdilo, the traitor of Pskov
* Ivan Lagutin as Anani, a Monk
* Lev Fenin as the Archbishop
* Naum Rogozhin as the Black-Hooded Monk

==1930s political context==
 ) declines a Mongol ambassadors offer to join the Golden Horde]]
 Stalinist era, somewhat strained relations between the Soviet Union and Nazi Germany. The film contains elements of obvious allegory that reflect the political situation between the two countries at the time of production. Some types of helmets worn by the Teutonic infantry resemble mock-ups of Stahlhelms from World War I. In the first draft of the Alexander Nevsky script, swastikas even appeared on the invaders helmets.  The film portrays Alexander as a folk hero and shows him bypassing a fight with the Mongols, his old foes, in order to face the more dangerous enemy.

The film also conveys highly anti-clerical and anti-Catholic messages.  The knights bishops miter is adorned with swastikas, while religion plays a minor role on the Russian side, being present mostly as a backdrop in the form of Novgorods St. Nicholas Cathedral and the clerics with their icons during the victorious entry of Nevsky into the city after the battle.
 socialist realist orthodoxy. The authorities could rely on Pavlenko, in his role of "consultant", to report any wayward tendencies on Eisensteins part.   
 enemies of the people who do nothing, a motif that was heavily employed. 
 ]]
While shooting the film, Eisenstein published an article in the official newspaper of record Izvestia entitled "Alexander Nevsky and the Rout of the Germans". He drew a specific parallel between Nevsky and Joseph Stalin|Stalin.     As a result, the Kremlin requested an advance screening and, without Eisenstein being consulted, his assistants showed the footage to the dictator. During the process of this screening, one of the reels, which featured a scene depicting a brawl among the populace of Novgorod, disappeared.   Whether it was left behind in the editing room inadvertently or whether Stalin saw the footage and objected to it, the filmmakers decided to destroy the reel permanently, since it had not received Stalins explicit approval. 

The picture was released in December 1938, and became a great success with audiences: on 15 April 1939, Semen Dukelsky - the chairman of the State Committee for Cinematography - reported that it had already been viewed by 23,000,000 people and was the most popular of the films made in recent times. 
 invasion of the Soviet Union, and the film rapidly returned to Soviet and western screens.

==Style==
Alexander Nevsky is less experimental in its narrative structure than Eisensteins previous films: it tells one story with a single narrative arc and focuses on one main character. The special effects and cinematography were some of the most advanced at the time. 
 Henry V, Spartacus (film)|Spartacus, The Empire Strikes Back).  This climactic set piece was the first to be filmed and, since it was shot during a blazing hot summer on a location outside Moscow, cinematographer Eduard Tisse had to take extraordinary steps to render a wintry landscape, including:  use of a filter to suggest winter light, painting all the trees light blue and dusting them with chalk, creating an artificial horizon out of sand, and constructing simulated ice sheets out of asphalt and melted glass, supported by floating pontoons that were deflated on cue so that the ersatz ice sheets would shatter under the weight of the Teutonic knights according to pre-cut patterns.   

==Musical score==
 
 
 
The film was the first of Eisensteins dramatic films to use sound. (The earlier  , the principal conductor of the London Symphony Orchestra, has stated his opinion that Prokofievs music for this film is "the best ever composed for the cinema". 

==Film and concerts==
In the 1990s a new, cleaner print became available. A number of symphony orchestras gave performances of Prokofievs cantata, synchronized with a showing of the new print. The New York Philharmonic,  the Detroit Symphony Orchestra, the San Francisco Symphony, the Philadelphia Orchestra, and the Baltimore Symphony Orchestra  are five such ensembles. The concerts were quite popular, because Prokofievs music is badly degraded by the original soundtrack recording, which suffers from extreme distortion and limited frequency response, as well as cuts to the original score to fit scenes that had already been shot. The cantata not only restored cuts but considerably expanded parts of the score.

==New edition of the film== digital stereo by Yuri Temirkanov conducting the St. Petersburg Philharmonic Orchestra and Chorus, although the dialogue portions of the soundtrack were left unchanged. This enabled a new generation to experience Eisensteins film and Prokofievs score in high fidelity, rather than having to settle for the badly recorded musical portion that had existed since the films original release. There is no version of the re-recorded score available on DVD.

==In popular culture==
  Doctor Zhivago King Arthur (2004).  Scenes from the film were later incorporated into the American propaganda film The Battle of Russia (1943).

In Star Wars (1977), when Darth Vader first appears, he bends over the corpses of Rebels like one of the Teutonic Knights in Alexander Nevsky after the storming of Pskov.
 West German sleeper agent on charges of terrorism.  While airing Nevsky immediately prior to the bombing may have been intended to inflame the Soviet population in favor of war with the West, the timing of the two events led the Americans to suspect the plot.

Prokofievs music from Alexander Nevsky accompanies the battle scene in Woody Allens comedy Love and Death (1975).

Ralph Bakshis 1977 film Wizards (film)|Wizards rotoscoped  footage of the ice-battle scene from Nevsky to create parts of Blackwolfs mutant army.

Certain scenes in John Millius’s fantasy epic Conan the Barbarian (1982 film) were influenced by Alexander Nevsky. The introduction of Thulsa Doom and his henchmen after the destruction of Conan’s village is reminiscent of the depiction of the Grand Master of the Teutonic Knights and his fellow knights after the conquest of Pskov. With its score and choreography, the final ride and attack of the Riders of Doom against Conan is similar to the Teutonic Orders cavalry approaching Nevsky in the Battle of Lake Peipus. Also, the Riddle of Steel, a major theme of the Conan film, is already mentioned by Nevsky during the final Battle of the Ice. 

The film makes a brief appearance in John Millius’s 1984 war film Red Dawn, too. When the protagonists return to their hometown occupied by the Soviet army, they walk by the "Serf" town cinema. The theater marquee says "All Saturday Come, Alexander Nevsky, Admission Free."

In 2012, a graphic novel adaptation of the film, entitled Nevsky: Hero of the People, was scheduled for publication, written by Ben McCool, with art by Mario Guevara and published by IDW Publishing. 

==References==

 

==External links==
 
*  
*  
*  
*  
*   at official Mosfilm site with English subtitles
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 