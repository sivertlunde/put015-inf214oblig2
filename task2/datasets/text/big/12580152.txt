The Rum Diary (film)
{{Infobox film
| name           = The Rum Diary
| image          = The Rum Diary Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Bruce Robinson
| producer       = Johnny Depp Graham King Christi Dembrowski Anthony Rhulen Robert Kravis Tim Headington
| writer         = Bruce Robinson
| based on       =  
| starring       = Johnny Depp Aaron Eckhart Michael Rispoli Amber Heard Richard Jenkins Giovanni Ribisi
| music          = Christopher Young
| cinematography = Dariusz Wolski
| editing        = Carol Littleton GK Films Infinitum Nihil Film Engine
| distributor    = FilmDistrict   
| released       =  
| runtime        = 120 minutes  
| country        = United States
| language       = English
| budget         = $45 million    
| gross          = $23.9 million 
}} novel of the same name by Hunter S. Thompson.  The film was written and directed by Bruce Robinson and stars Johnny Depp. Filming began in Puerto Rico in March 2009. It was released on October 28, 2011.   

==Plot==
Paul Kemp (Johnny Depp) is an author who hasnt been able to sell a book. He gets a job at a newspaper in San Juan, Puerto Rico. There, he meets Sala (Michael Rispoli), who gets him acclimatised and tells him he thinks the newspaper will fold soon. Kemp checks into a hotel and while idling about on a boat in the sea, meets Chenault (Amber Heard), who is skinny-dipping while avoiding a Union Carbide party. Kemp is immediately smitten with her.

Kemp and Sala immediately go on a drinking binge, which earns Kemp the enmity of his editor, Lotterman (Richard Jenkins). Kemp also meets Moburg (Giovanni Ribisi), a deadbeat reporter who cant be fired. While waiting for an interview, Kemp meets Sanderson (Aaron Eckhart), a freelance realtor, who offers him a job writing ads for his latest venture. Sanderson is engaged to Chenault, who pretends not to know Kemp.

Later, Kemp moves in with Sala, who also rooms with Moburg. Kemp begins to see the poverty of San Juan, but Lotterman doesnt want him to write about it, as its bad for tourism. Moburg returns with leftover filters from a rum plant; they contain high-proof alcohol. Moburg has been fired, and rants about killing Lotterman.

Kemp visits Sanderson and spies him making love to Chenault. He meets Zimburger (Bill Smitrovich) and Segarra (Amaury Nolasco), who want him to help with a real estate scam. Later, Sala and Kemp go to a restaurant and berate the owner for refusing them service; Kemp senses that the owner wants to kill them, and he and Sala beat a hasty retreat, pursued by angry locals. The police arrive and break up the fight, then throw Sala and Kemp in jail. Sanderson bails them out.

The next day, Kemp meets with Sandersons crew, who tell him that the US military is relinquishing the lease on some prime real estate, and is asked to pick up Chenault from her house. Kemp and Chenault share a moment, but resist temptation.

Zimburger takes Kemp and Sala to see the island property, then they head to Saint Thomas, U.S. Virgin Islands|St. Thomas for Carnival. Kemp finds Chenault, and they wind up on Sandersons boat. Sanderson berates Kemp for involving Sala in the deal. At night, they go to a club, and a drunk Chenault dances with local men to provoke Sanderson, with whom she has been fighting. The black owners of the bar beat up Sanderson and throw Kemp out of the club. Chenault is left behind at the bar, but where she ends up is not known.

The next day, Chenault is gone, and Sanderson tells Kemp that their business arrangement is over. When Sala and Kemp return home, Moburg tells them that Lotterman has left and that the paper will go out of business. He also sells them hallucinogens, which they take. Kemp has an epiphany while under the influence, and resolves to write an exposé on Sandersons shady deals.

Lotterman returns, but wont publish Kemps story. Chenault shows up at Kemps place, after Sanderson disowns her. Out of spite, Sanderson withdraws his bail, meaning that Kemp and Sala are now wanted by the police. Moburg also tells them that Lotterman has closed the paper. Kemp decides to print a last issue, telling the truth about Lotterman and Sanderson, as well as the stories Lotterman declined.

To make money to print the last edition, Kemp, Sala and Moburg place a big cockfighting bet. They visit Papa Nebo (Karimah Westbrook), Moburgs hermaphrodite witch doctor, to lay a blessing on Salas prize cockerel. They win, but return to the office to find that the printing presses have been confiscated.

Kemp continues his quest, leaving Puerto Rico on a sailboat. The end credits explain that Kemp makes it back to New York, marries Chenault, and becomes a successful journalist, having finally found his voice as a writer.

==Cast==
*Johnny Depp as Paul Kemp, a journalist for The San Juan Star 
*Aaron Eckhart as Hal Sanderson, a businessman 
*Michael Rispoli as Bob Sala 
*Amber Heard as Chenault, Sandersons fiancée and Kemps love interest 
*Richard Jenkins as Edward J. Lotterman, the editor of The San Juan Star 
*Giovanni Ribisi as Moburg 
*Amaury Nolasco as Segurra 
*Marshall Bell as Donovan 
*Bill Smitrovich as Art Zimburger 
*Julian Holloway as Wolsey 
*Karen Austin as Mrs. Zimburger Jason Smith as Davey
*Bruno Irizarry as Lazar
*Karimah Westbrook as Papa Nebo
*Jorge Antares 

== Production ==
  in November 2011, at a premiere of the film in Paris ]] The Rum Diary in 1961, but it was not published until 1998.    The independent production companies Shooting Gallery and SPi Films sought to adapt the novel into a film in 2000, and Johnny Depp was signed to star and to serve as executive producer. Nick Nolte was also signed to star alongside Depp.    The project did not move past the development stage.  During this stage, the author became so frustrated as to fire off an obscenity-laden letter calling the process a "waterhead fuckaround". 
 1998 film adaptation of Thompsons novel Fear and Loathing in Las Vegas, was cast as the freelance journalist Paul Kemp.  Amber Heard is reported to have been preferred over Scarlett Johansson and Keira Knightley. Bruce Robinson joined to write the screenplay and to direct The Rum Diary.  In 2009, Depps production company Infinitum Nihil took on the project with the financial backing of King and his production company GK Films. Principal photography began in Puerto Rico on March&nbsp;25, 2009.    Composer Christopher Young signed on to compose the films soundtrack.   

Robinson had been sober for six and a half years before he started writing the screenplay for The Rum Diary.    The film maker found himself suffering from writers block. He started drinking a bottle of wine a day until he finished the script and then he quit drinking again. He spent a year filming in Puerto Rico, Mexico and Hollywood and resisted drinking until the crew arrived in Fajardo. Robinson remembers, "It was 100 degrees at two in the morning and very humid. Everyones drenched in sweat. One of the prop guys goes by with a barrow-load of ice and Corona (beer)|Coronas. I said: Johnny, this doesnt mean anything. And reached for a Corona. ... Some savage drinking took place. When I was no longer in Johnnys environment I went back to sobriety." 

In regard to playing the character of Kemp, Depp compared and related it to his previous role in Fear and Loathing in Las Vegas. He said “The way I approached it was that the character of Paul Kemp is Raoul Duke as he was learning to speak. It was like playing the same character, only 15 years before. This guy’s got something; there’s an energy burning underneath it, it’s just ready to pop up, shoot out.”

==Reception==
The Rum Diary received mixed reviews from critics. Review aggregator Rotten Tomatoes reports that 50% of 160 critics have given the film a positive review, with a rating average of 5.6 out of 10. The websites consensus is, "Its colorful and amiable enough, and Depps heart is clearly in the right place, but The Rum Diary fails to add sufficient focus to its rambling source material."  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 56 based on 37 reviews.   CinemaScore polls reported that the average grade moviegoers gave the film was a "C" on an A+ to F scale.   

Wyatt Williams, writing for Creative Loafing, argues that "the movie version amounts to Thompsons whole vision of journalism, glossed and made plain by Hollywood." 

The films release was not without controversy, however, as animal rights groups strongly objected to the movies scene showing an actual cock fight.

== See also ==
 
* List of films set in Puerto Rico

== References ==
 

== External links ==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 