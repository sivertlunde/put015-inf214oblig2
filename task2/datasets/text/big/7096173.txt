Le Gros et le maigre
{{Infobox film
| name           = Le Gros et le maigre
| writer         = Roman Polanski
| director       = Roman Polanski
| starring       = André Katelbach Roman Polanski
| cinematography = Jean-Michel Boussaguet
| music          = Krzysztof Komeda
| length         = 15 mins.
}}
 The National Film School in Łódź in 1959; it was made in France and was Polanskis last film before the international breakthrough of his 1962 debut feature, Knife in the Water.  The Fat and the Lean features the music of Krzysztof Komeda, who composed the scores for all but one of the directors films between Two Men and a Wardrobe (1958) and Rosemarys Baby (film)|Rosemarys Baby (1968).

In The Fat and the Lean, Polanski plays a barefoot slave playing a flute and beating a drum to entertain his master who rocks in a rocking chair in front of his mansion in the countryside overlooking Paris. The slave wipes his masters brow, feeds him, washes his feet, shades him from the sun with an umbrella, and holds a urinal for him, all the while longing to escape to Paris, which we can see in the distance.  A humorous psychological game between the master and the slave transpires whereby the master attempts to prevent the slave from escaping to Paris.
 Pozzo and Lucky in Samuel Becketts Waiting for Godot.  Indeed, as with many of Polanskis early short films, the influence of Beckett and the Theater of the Absurd is very strong.

The Fat and the Lean has been interpreted   as allegorical work expressing Polanskis youthful desire to flee the repressive communist regime in postwar Poland and escape to the West.  Portions of the film were featured in Marina Zenovichs controversial 2008 documentary,  .  In the context of the documentary, the situation depicted in The Fat in the Lean seems a bitterly ironic commentary on Polanskis legal troubles during 1977 — almost fifteen years after he had left Poland and was living and working in Los Angeles as a successful Hollywood director.

The slave tries to flee to Paris; the master gives the slave a goat in order to persuade him to stay. But the goat is chained to the slaves ankle, and becomes so inconvenient that the slave tries to flee once more. The master frees the goat and unfastens the chain, and the slave is so overjoyed that he remains to serve the master with renewed vigor and enthusiasm.

If there is an allegory here it might be about human nature and subjugation.

==External links==
* 

 

 
 
 
 
 
 


 
 