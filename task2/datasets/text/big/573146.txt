A Wedding for Bella
{{Infobox film
| name           = A Wedding for Bella
| image_size     = 
| image          = A Wedding for Bella FilmPoster.jpeg
| caption        = A poster bearing the films previous title: The Bread, My Sweet
| director       = Melissa Martin
| producer       = William C. Hulley Adrienne Wehr
| writer         = 
| narrator       = 
| starring       = Scott Baio Kristin Minter Rosemary Prinz John Amplas Zachary Mott
| music          = 
| cinematography = Mark Knobil
| editing        = 
| distributor    = 
| released       = 
| runtime        = 105 min.
| country        = United States Italian / English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
A Wedding for Bella (formerly titled The Bread, My Sweet) is a 2001 motion picture that told the story of a successful businessman who trades in his single lifestyle to marry the estranged daughter of a terminally ill elderly woman whom he loves like a mother.  The marriage is a sham in order to give the elderly woman happiness before her death.

Written and directed by Melissa Martin, the film is set in Pittsburghs Strip District, a thriving and diverse open-air market. Part love-story to the city that serves as its setting, the film is also a loving homage to the personalities of three people who each had a powerful impact on Ms. Martin.

Bella, the title character, and her husband, Massimo, are the creative embodiments of an elderly Italian-American couple who owned, and lived above The Enrico Biscotti Company (the real-life Pittsburgh bakery currently owned by Ms. Martins ex- husband, Larry Lagattuta, which served as an inspiration for the film).

The film was directed by Martin, and starred Scott Baio as the businessman, Rosemary Prinz (as the dying elderly woman), Kristin Minter as Lucca, the estranged daughter, and Shuler Hensley. Then 70 years old, Prinz, a soap opera mainstay, made her feature film debut. This film has been presented at many film festivals across America.

==References==
 

==External links==
*   at Who Knew Productions
*  

 
 
 
 


 