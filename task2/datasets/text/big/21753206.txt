Witches' Brew (film)
 Witches Brew}}
{{Infobox film
|name=Witches Brew
|image=Witches Brew cover.jpg
|image_size=160px
|producer=Donna Ashbrook   Jack Bean   (executive producer)   W.L. Zeltonoga (supervising producer)  
|music=John Carl Parker   
|cinematography=Norman Gerard    
|editing=Herbert L. Strock    
|country= United States
|released=October 31, 1980
|runtime=98 min.
|language=English
}} horror comedy film directed by Herbert L. Strock (additional sequences) and Richard Shorr who co-wrote screenplay with Syd Dutton. It was based (though without any screen credit) on Fritz Leiber Jr.s novel Conjure Wife. Witches Brew is notable for being the final feature film appearance of Lana Turner.

==Plot==

As in earlier film versions of Leibers story such as Weird Woman (1944) and Night of the Eagle (1962), the story is set around a college campus where rivalries for various chairmanships of faculties take place. While the scripts touch is notably lighter than in earlier film versions, verging on comedic in places, the story is basically the same.

Several of the wives practice witchcraft in order to advance their husbands careers. Joshua Lightman (Richard Benjamin) does not believe that his wife Margarets spells and hocus-pocus have been helping her, and makes her cease practising witchcraft. Immediately things begin to go wrong for Lightman. he cuts himself shaving; he is accused by a male student of having accosted him (which loses him the chairmanship of the psychology department); and a disgruntled female student tries to kill him by sniping with a rifle from the college rooftop. Meanwhile, Vivian Cross (Lana Turner) is controlling several of the other wives via a sculpture of an egg (modeled on a demonic witches egg they find in a book on witchcraft) in which a being is hatched. This winged creatures whose eyes shoot green flames chases Joshuas car and nearly kills him before Vivian destroys it via her magic. Vivian, who is close to death, then hatches a plot to trade bodies with Margaret. Margaret is sent driving off a pier in her car; but Joshua uses magic to save her. Vivian succeeds in swapping souls with Margaret, but the tables are turned on her; Vivian is destroyed and Margaret is returned to her own body and starts practicing witchcraft again to help Joshua.

==Cast==
*Teri Garr as  Margaret Lightman 
*Richard Benjamin as  Joshua Lightman 
*Lana Turner as  Vivian Cross 
*James Winkler as  Linus Cross 
*Kathryn Leigh Scott as  Susan Carey 
*Bill Sorrells as  Nick Carey 
*Kelly Jean Peters as  Linda Reynolds 
*Jordan Charney as  Charlie Reynolds 
*Nathan Roth as  Ben Cohn 
*Barbara Minkus as  Saleswoman  
*Bonnie Gondel as  Marcia Groton 
*Angus Scrimm as  Carl Groton

==Other versions==
Filmed in 1944 as Weird Woman starring Lon Chaney Jr.; and as the 1962  film Night of the Eagle (aka Burn Witch Burn! starring Peter Wyngarde and Janet Blair.

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 