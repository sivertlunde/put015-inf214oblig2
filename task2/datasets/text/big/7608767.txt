Cafe Mascot
{{Infobox Film
| name = Cafe Mascot
| image = Geraldine Fitzgerald in Dark Victory trailer.jpg
| image_size =
| caption =
| director = Lawrence Huntington
| producer = Gabriel Pascal
| writer = Gerald Elliott   Cecil Arthur Lewis
| narrator =
| starring = Geraldine Fitzgerald Derrick De Marney
| music =
| cinematography =
| editing =
| studio = Gabriel Pascal Productions 
| distributor = Paramount British Pictures
| released =
| runtime = 77 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
}}
Cafe Mascot is a 1936 British comedy film directed by Lawrence Huntington and starring Geraldine Fitzgerald. It was produced by Gabriel Pascal, and made at Wembley Studios.

A young man discovers £1,000 in a taxi. The kindly man gives it to an impoverished Irish girl (Geraldine Fitzgerald) by investing it in her cafe, The Cafe Mascot.

==Cast==
* Geraldine Fitzgerald as Moira OFlynn  
* Derrick De Marney as Jerry Wilson 
* George Mozart as George Juppley  
* Clifford Heatherley as Dudhope   Richard Norris as Nat Dawson   Paul Neville as Peters  
* Julian Vedey as Francois   George Turner as Miles  
* Geoffrey Clark as Benton

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 


 