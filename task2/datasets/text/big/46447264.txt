Chronicles of the Gray House
{{Infobox film
| name = Chronicles of the Gray House
| image = 
| alt = 
| caption = 
| director = Arthur von Gerlach
| producer = Erich Pommer
| screenplay = Thea von Harbou
| based on = A Chapter in the History of Grieshuus by Theodor Storm
| starring = 
| music = Gottfried Huppertz
| cinematography = Fritz Arno Wagner Carl Drews Erich Nitzschmann
| editing = 
| studio = Universum Film AG
| distributor = 
| released =  
| runtime = 109 minutes
| country = Germany
| language = German
| budget = 
| gross = 
}} Paul Hartmann and Rudolf Forster. It is also known as At the Grey House. The narrative is set in the 17th century and follows the intrigues when the son of a feudal landowner falls in love with the daughter of one of the serfs, causing his younger brother to see an opportunity for himself. The screenplay by Thea von Harbou is based on Theodor Storms novella A Chapter in the History of Grieshuus.   

Erich Pommer produced the film for Universum Film AG. Principal photography took place from May 1923 to November 1924 around the Lüneburg Heath and Neubabelsberg. The premiere took place in Berlin on 11 February 1925. 

==Cast==
* Arthur Kraußneck as Burgherr von Grieshuus Paul Hartmann as Junker Hinrich
* Rudolf Forster as Junker Detlev
* Rudolf Rittnerv as Owe Heiken
* Lil Dagover as Bärbe
* Gertrud Welcker as Gesine
* Gertrud Arnold as Matte
* Hanspeter Peterhans as Enzio
* Christian Bummerstedt as Christof
* Josef Peterhans as Bereiter

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 