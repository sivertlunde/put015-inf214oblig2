The Korean Wedding Chest
{{Infobox film
| name           = The Korean Wedding Chest Die koreanische Hochzeitstruhe
| image          = German film-The Korean Wedding Chest-Poster-01.jpg
| alt            =  
| caption        = 
| director       = Ulrike Ottinger
| producer       = 
| writer         = Ulrike Ottinger
| starring       = 
| music          = Kim So-Young
| cinematography = Ulrike Ottinger, Lee Sun-Young
| editing        = Bettina Blickwede
| studio         = Ulrike Ottinger Filmproduktion
| distributor    = Women Make Movies
| released       =  
| runtime        = 82 minutes
| country        = Germany South Korea
| language       = German, Korean
| budget         = 
| gross          = 
}}
 Korean wedding traditions directed by Ulrike Ottinger.  The German language film was described as "capturing the collision of ancient tradition and modern culture on the subject of love and marriage in Korea in a film that echoes the beauty, precision and care of the rituals she examines" by the Los Angeles Times. Betsy Sharkey   : Ulrike Ottinger in top artistic form October 1, 2009
Los Angeles Times  The Washington Posts website refers to the surrealist style of the film as being well suited to "the regal pacing of the ritual" and calls the film one of Ottingers most praised works.  

==See also==
*Pyebaek
*Marriage in South Korea

==References==
 

==External links==
* 
* 
*   at Women Make Movies
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 