Hisss
 
 
{{Infobox film
| name           = Hisss
| image          = Hisss (movie poster).jpg
| caption        = 
| director       = Jennifer Chambers Lynch
| producer       = Govind Menon Vikram Singh Ratan Jain William Sees Keenan
| writer         = Jennifer Chambers Lynch
| screenplay     = 
| story          = 
| based on       =  
| starring       = Mallika Sherawat Divya Dutta Irrfan Khan Jeff Doucette
| music          = Anu Malik Julian Lennon Panjabi MC  DKFP (David Kushner & Franky Perez)
| cinematography = Madhu Ambat
| editing        = Tony Ciccone|
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States India
| language       = Hindi English Tamil Telugu Malayalam
| budget         = $5–15&nbsp;million 
| gross          = 
}}
Hisss (also known as Nagin: The Snake Woman) is a 2010 film,  directed by Jennifer Chambers Lynch.  Bollywood actress Mallika Sherawat plays the lead role as  

==Plot==
 
George States (Jeff Doucette) is suffering from the last stage of brain cancer and has only six months to live. To prevent death and gain immortality, he decides to extract the Nagmani from a Ichchhadhari Nag|shape-shifting snake who can take human form, Nagin (Mallika Sherawat). He captures the nag   so that the nagin   comes after the capturer to free her lover, thus allowing him to obtain the nagmani by force. He keeps the nag in a glass box where he electrocutes and tortures him. His plan works and the nagin starts following him. On her way, she is helped by a police inspector (Irrfan Khan) and his wife (Divya Dutta), who is infertile, causing a strain in their relationship. The nagin also helps a few women who are helpless: One who is beaten mercilessly by her husband; one being raped by a man and so on. She brutally murders those men who were involved in the capture of her mate as well as men who torture women. The police inspector who is trying to help her find her mate investigates the murders. Finally, she reaches Georges hideout where she reclaims her mate and they engage both in sex leading to Nagin becoming pregnant. George attempts to capture the nagin during intercourse since this will be when she will be at her most vulnerable. Wearing a suit that hides his heat signature, he lures her by using her dying mate as bait to a trap. He captures her and tries taking the nagmani but at that moment, the police inspector arrives and helps her. Angered by the death of her mate by the hands of George, she takes on a huge form: half snake, half woman, and throws him in the same glass box where her mate was kept and electrocutes him.

==Cast==
* Mallika Sherawat as Nagin 
* Irrfan Khan as Inspector Vinkram Gupta 
* Jeff Doucette as George States   at Twitch.com 
* Divya Dutta as Maya Gupta, Inspectors wife 

* Shruti hassan as herself 

==Production==
The film was shot simultaneously in English and Hindi.  Famed special effects designer Robert Kurtzman was responsible for developing the look of the Snake Woman in the film.  The film was shot in the jungle of Kerala, India.  The film was also shot in Mumbai, Chennai, Madh Island, and in the studios of Filmistan. 

The film is edited by American film editor Tony Ciccone, who is also credited with additional sound design.  His film editing credit is misrepresented on preliminary on-line posters as "Anthony Ciccone." 

Mooppan Raghavan, tribal leader of Thalikakal settlement in Kerala, and over a dozen members of his tribe were recruited as extras for the film. 

A documentary, titled the Gods, about director Jennifer Lynchs struggle to make the film was released in 2012.

==Release== Tamil and Malayalam version was released on 12 November.

==Premiere==
The film was set to premiere at the Montreal Festival du Nouveau Cinéma on 14 October  and at the Gotham Screen Film Festival & Screenplay Contest in New York on 15 October  but was pulled from both festivals shortly before they opened.  

==Reception==
 
Hisss attracted negative reviews from various critics. Nikhat Kazmi of Times of India gave it 2 out of 5 stars and stated – "A film like Hisss should have scored with its special effects. But once again, the carnage that the serpent unleashes is grotesque and her transformation from seductress woman to venomous reptile is more funny than eyeball-grabbing." 
Taran Adarsh from Bollywood Hungama gave it 1/2 star out of five & stated that " If there were Razzies in Bollywood, HISSS would win hands down. Films like HISSS make you realize whats going wrong in Bollywood today. On one hand we celebrate the new stories being told in our movies and on the other, we churn out a HISSS, which is badly scripted, poorly enacted and carelessly directed. Believe me, its easy to solve the crossword puzzle in newspapers than it is to understand what exactly is going on in this film.As for director Jennifer Lynch, she needs a crash course in film-making pronto. The visual effects seem straight out of a B-grade Bollywood film.On the whole, HISSS is best avoided. "
Rajeev Masand from CNN-IBN gave it 1 star out of five & stated that "In Hisss, directed by Jennifer Lynch, Mallika stars as a shape-shifting snake thats been separated from her reptile lover by a crazy foreigner whos seeking a naagmani that will cure his cancer. The outrageous plot involves this ichchadhari nagin emerging as something of a feminist superhero who comes to the rescue of various tormented women, even as shes seeking out her kidnapped partner. Irrfan Khan plays the cop investigating a series of mysterious killings, and Divya Dutta his wife.At 90 minutes its mercifully short, but watch it only if youre not easily disgusted."
Anupama Chopra from NDTV gave it 1/2 star out of five & stated that "Hisss has marginally better special effects and much more nudity but the script is so deliriously inept that, in comparison, the average Ramsay horror film looks like a class act."
Mayank Shekher from Hindustan Times gave it 1 star out of five & stated that "This is pornography for the hormonally demented teen. Or maybe this is erotica."
The film scores only 2 out of 10 on the reviewgang.com .

== Music ==
{{Infobox album  
| Name       = Hisss
| Type       = soundtrack
| Artist     = Anu Malik, David Kushner, Franky Perez, Julian Lennon, Mark Sipro, Peter-John Vettese, Shraddha Pandit, Panjabi MC
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Venus
| Producer   = 
| Chronology = Anu Malik
| Last album = Teree Sang (2009)
| This album = Hisss (2010)
| Next album = Toonpur Ka Super Hero (2010)
}}
Anu Malik composed the on camera songs for the film.  Alexander von Bubenheim composed the original score.   The soundtrack consists of six original recordings, composed by Anu Malik, David Kushner, Panjabi MC, Alexander Von Bubenheim, Salim-Sulaiman and Julian Lennon, and lyrics penned by Sameer (lyricist)|Sameer, Shruti Haasan, Mallika Sherawat, Shweta Pandit, Shraddha Pandit and Sayeed Quadri. The soundtrack also featured hit songs from the films Robot (film)|Robot and De Dana Dan. Mallika Sherawat makes her musical debut on the films soundtrack alongside Julian Lennon.  Shruti Haasan sings and appears in a promotional video with music by Dave Kushner for the film.     Music Supervisor for the film, Marcus Barone,  co produced the Soundtrack Album for Split Image Soundtracks with Venus Records and Tapes along with co producer of the film William Keenan. Official Bollywood Soundtrack Charts  charted "I Got That Poison" performed by Panjabi MC  and Shweta Pandit at No. 8 from 24 October 2010 to 5 November 2010.

{{Track listing
| collapsed       = 
| headline        = Hisss (Original Motion Picture Soundtrack)
| extra_column    = Artist(s)
| total_length    = 46:37
| lyrics_credits  = yes
| music_credits   = yes
| title1          = Lagi Lagi Milan Dhun Lagi
| note1           = Version 1 Sameer
| music1          = Anu Malik
| extra1          = Shreya Ghoshal
| length1         = 6:05
| title2          = Beyond The Snake
| lyrics2         = Shruti Haasan
| music2          = David Kushner, Franky Perez
| extra2          = Shruti Haasan
| length2         = 3:11
| title3          = Sway
| note3           = Mann Dole
| lyrics3         = Mallika Sherawat
| music3          = Julian Lennon, Mark Sipro, Peter-John Vettese
| extra3          = Mallika Sherawat
| length3         = 3:32
| title4          = I Got The Poison
| note4           = Hisss Remix
| lyrics4         = Shweta Pandit
| music4          = Shraddha Pandit, Panjabi MC
| extra4          = Shweta Pandit
| length4         = 3:42
| title5          = Lagi Lagi Milan Dhun Lagi
| note5           = Version 2
| lyrics5         = Sameer
| music5          = Anu Malik
| extra5          = Sunidhi Chauhan
| length5         = 6:05
| title6          = Hisss
| lyrics6         = Shweta Pandit, Shraddha Pandit
| music6          = Shraddha Pandit
| extra6          = Shweta Pandit
| length6         = 2:59
| title7          = Lafanaa
| lyrics7         = Sayeed Quadri
| music7          = Anu Malik
| extra7          = Sunidhi Chauhan
| length7         = 5:52
| title8          = Naina Miley
| note8           = From Enthiran|Robot
| lyrics8         = Swanand Kirkire
| music8          = A. R. Rahman
| extra8          = A. R. Rahman, Suzanne DMello, Lady Kash and Krissy
| length8         = 5:16
| title9          = Rishte Naate
| note9           = From De Dana Dan
| lyrics9         = Sayeed Quadri
| music9          = Pritam Chakraborty
| extra9          = Rahat Fateh Ali Khan, Suzanne DMello
| length9         = 4:25
| title10         = Kilimanjaro
| note10          = From Robot
| lyrics10        = Swanand Kirkire
| music10         = A. R. Rahman
| extra10         = Javed Ali, Chinmayi
| length10        = 5:30
}}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 