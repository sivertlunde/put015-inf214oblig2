Attack on the Gold Escort
 
{{Infobox film
  | name     = Attack on the Gold Escort
  | image    = 
  | caption  = 
  | director = 		
  | producer = 
  | writer   = 
  | based on = 
  | starring = "The Bohemians at Geelong"   
  | music    = 
  | cinematography = 
  | editing  = 
  | distributor = 
  | studio = Pathe Frere 
  | released = 19 June 1911 
  | runtime  = 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

Attack on the Gold Escort is a 1911 Australian silent film.

It was sometimes known as Captain Midnight, King of the Bushrangers, or Attack of the Gold Escort, or Captain Starlights Attack on the Gold Escort. 

It is considered a lost film.

The film was called "an Exciting and Thrilling Reproduction of Australian Early Days. A vivid portrayal of bush adventure, acted by Australian artistes, amid Australian scenery" which was filmed "at the exact spot where the incident happened."   

The movie is often confused with Captain Midnight, the Bush King and Captain Starlight, or Gentleman of the Road. However it seems it was a separate movie. 

==Plot==
The gold escort from the Bank of Australia is attacked by bushrangers. It is chased down Evandsford Hill. 

==Reception==
The film made its debut on 19 June 1911 in Geelong. 

The Kapunda Herald said the film "portrayed the terrors of the road, during the time when bushranging was rife, in a vivid and realistic manner." 

==References==
 

==External links==
*  at AustLit
*  at IMDB
 

 
 
 
 


 