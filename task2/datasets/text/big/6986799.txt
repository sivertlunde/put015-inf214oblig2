The Grinch Grinches the Cat in the Hat
{{Infobox television show_name = The Grinch Grinches the Cat in the Hat image =   caption = Original VHS cover writer = Dr. Seuss genre = Musical Comedy creator = director = Bill Perez company = Marvel Productions DePatie-Freleng Enterprises|DePatie-Freleng (In-name only) voices = Bob Holt Mason Adams Frank Welker Joe Eich narrated = Mason Adams as the Cat music = Joe Raposo country = United States language = English executive_producer = David H. DePatie producer = Friz Freleng Dr. Seuss runtime = 25 minutes network = ABC
|audio_format Mono
|first_aired =  
}} animated Musical musical television crossover starring The Grinch ABC and won two Emmy Award|Emmys.

==Plot==
On a morning so beautiful, even the Grinch wakes up in a good mood. But his cheerfulness is soon revoked when his reflection in the mirror prompts him to repeat the "Grinchs Oath" and prove himself a Grinch. Meanwhile, the Cat in the Hat goes on a picnic. Paths crossed between the Grinch and the Cat escalate into a fierce car chase.

The Cat returns to the safety of his house, but the Grinch follows him there and messes with his voice using a device he has invented, the "Vacusound Sweeper", in the process sabotaging other sounds within a 50-mile radius. The Grinch then proceeds to his "darkhouse", a lighthouse that spreads darkness, to mess with the Cats sight.

The Cat becomes upset with the Grinchs hijinks and has a psychiatric session with him in a thought bubble to find out what makes him so mean-spirited. Predictably, he gets nowhere with the imaginary Grinch, so he then decides to go over and have a talk with him, but the Grinch makes it so dark he cant see where hes going and he crashes his car when he passes a "Dead End" sign.

The Cat attempts to hide from the Grinch in a nearby restaurant, but the Grinchs machine continues to mess with reality, making the restaurant and everything with it literally come crazily to life, and his hijinks result in confusion all over the restaurant. The Cat is now furious with the Grinch and ponders to himself how he can change the Grinch, eventually racing through a door and sending himself hurtling into the Grinchs Plane (esotericism)|Dimension. He soon figures it out and rallies everybody in the restaurant to follow him to the Grinchs house.

There, he leads everyone in a song to remind the Grinch of all the love he received from his mother and implore him to change his ways ("Deep down in your brain, must you give her more pain? / Please soften your heart, make Mom happy again"). The Grinch cries when he hears this, disassembles his machines, and continues his change of heart into the next morning. When his reflection tries to convert him back to his old self, though, Max drains out his voice with the Vacusound Sweeper.

==Voice cast==
*Mason Adams as The Cat in the Hat Bob Holt The Grinch
*Frank Welker as Max / Waiter / Additional voices
*Joe Eich as Chef

==Musical numbers==
#"A Beelzeberry Day" - The Cat
#"Relax-ification" - The Cat
#"Master of Everyones Ears" - The Grinch
#"Most Horrible Things" - The Grinch
#"Psychiatry Song" - The Cat
#"Remember Your Mother" - The Cat, Chef, Musicians and Waiters

==Awards== Outstanding Animated Program. 

==Production notes== Marvel and one of the last DFE cartoons to be involved with Friz Freleng |Freleng. 

==Home media== CBS Video and Fox Kids Video). It was also re-released on VHS in 2000 by Paramount Home Entertainment. It was later released on DVD by Universal Studios Home Entertainment. The DVD cuts out half of the car chase sequence, ending the chase after the Grinch drives into a mud pit. 
 How the Grinch Stole Christmas! and Halloween Is Grinch Night.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 