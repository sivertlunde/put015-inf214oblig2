Particle Fever
 
{{Infobox film
| name           = Particle Fever
| genre = Documentary Film
| image          = Particle Fever.jpg
| caption        = Theatrical release poster Mark Levinson
| starring       = Savas Dimopoulos Nima Arkani-Hamed Fabiola Gianotti Monica Dunford Martin Aleksa Mike Lamont David Kaplan
| cinematography = Claudia Raschke-Robinson
| editing = Walter Murch
| narrator = David Kaplan
| music = Robert Miller
| studio         = Anthos Media
| released       =  
| runtime        = 99 minutes
| country = United States
| language       = English   
| budget         = $1.2 million 
}} experimental physicists theoretical physicists who attempt to provide a conceptual framework for the LHCs results. The film begins in 2008 with the first firing of the LHC and concludes in 2012 with the successful identification of the Higgs boson.   

==Production== Mark Levinson, David Kaplan, a professor of physics at Johns Hopkins University and producers Andrea Miller, Carla Solomon  and Wendy Sax.  The team gathered nearly 500 hours of footage from both professional camera crews and amateur video self-recordings shot by the physicists themselves.    This footage was then edited by Walter Murch, who had previously won Academy Awards for his work on Apocalypse Now and The English Patient.   

The film premiered at Sheffield Doc/Fest on July 14, 2013.   

==Synopsis==
The film is composed of two narrative threads. One follows the large team of experimental physicists at CERN as they try to get the LHC running properly. After a promising initial test run, the LHC suffers a liquid helium leak in 2008 that damages its electromagnets. Fabiola Gianotti, Martin Aleksa, and Monica Dunford are all shown discussing how to handle the negative publicity surrounding the accident, and how to proceed. After repairs in 2009, the LHC begins to run experiments again at half power.

The other thread follows the competing theories of Nima Arkani-Hamed and his mentor Savas Dimopoulos. In the film, Arkani-Hamed advocates for the "multiverse" theory, which predicts the mass of the Higgs boson to be approximately 140 electronvolt|giga-electronvolts. Dimopolous argues for the more-established supersymmetry theory, which predicts the mass of the Higgs boson to be approximately 115 GeV.

The narrative threads combine at the end of the film, when CERN announces the confirmed existence of a Higgs-like particle, with a mass of approximately 125 GeV. The discovery of the particle is met with a standing ovation, and Peter Higgs is shown wiping away tears. However, neither of the competing theories of the universe is definitively supported by the finding.

Later, Kaplan is shown admitting that none of his theoretical models are supported by this finding, and that the long-term implications of the discovery are unclear.       

==Release==
The film was shown at several festivals before opening in limited release in the United States on March 5, 2014. 

==Critical response==
Critical reception was overwhelmingly positive, with reviewers praising the film for making theoretical arguments seem comprehensible, for making  scientific experiments seem thrilling, for making particle physicists seem human, and for promoting physics outreach. Several reviewers singled out Murchs editing for praise.   
 rating average of 7.8 out of 10.   

On his blog, theoretical physicist and string theory critic Peter Woit called the movie "fantastically good," but cautioned that Arkani-Hameds linking of the Higgs boson to multiverse theory was a tenuous proposition, as this theory did not currently make testable predictions. 

==References==
 

==External links==
* 
* 
*  with Jon Niccum (Broken)

 
 
 
 
 
 
 