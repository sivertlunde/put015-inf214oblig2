I Am Suzanne
{{Infobox film
| name           = I Am Suzanne
| image          =
| imagesize      =
| caption        =
| director       = Rowland V. Lee
| producer       = Fox Film Corporation
| writer         = Edwin Justus Mayer
| starring       = Lilian Harvey
| music          = Louis De Francesco Friedrich Hollaender
| cinematography = Lee Garmes
| editing        =
| distributor    = Fox Film Corporation
| released       = December 25, 1933
| runtime        = 98 minutes
| country        = United States
| language       = English
}}
I Am Suzanne! is a 1933 American romance film involving puppeteers in Paris written by Edwin Justus Mayer, directed by Rowland V. Lee,  and starring Lilian Harvey, Gene Raymond and Leslie Banks.   The pictures puppetry sequences feature the Yale Puppeteers  and Podreccas Piccoli Theater. The Museum of Modern Art  in New York City owns and periodically exhibits a 35mm print of the film while the Eastman House in Rochester, New York, archives a 16mm copy.

==Cast==
 
* Lilian Harvey - Suzanne
* Gene Raymond - Tony Malatini
* Leslie Banks - Adolphe Baron Herring
* Georgia Caine - Mama
* Murray Kinnell - Luigi Malatini
* Geneva Mitchell - Fifi
* Halliwell Hobbes - Dr. Lorenzo
* Edward Keane - Manager
* Lionel Belmore - Satan
* Lynn Bari - Audience member (uncredited)
 
==Reception==
The film was not a success at the box office. THE YEAR IN HOLLYWOOD: 1984 May Be Remembered as the Beginning of the Sweetness-and-Light Era
By DOUGLAS W. CHURCHILL.HOLLYWOOD.. New York Times (1923-Current file)   30 Dec 1934: X5.  

==References==
 

 

 
 
 
 
 
 


 
 