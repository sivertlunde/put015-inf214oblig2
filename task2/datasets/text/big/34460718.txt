Country of the Deaf
 
{{Infobox film
| name           = Country of the Deaf
| image          = 
| caption        = 
| director       = Valery Todorovsky
| producer       = Sergei Chliyants Sergey Livnev
| writer         = Yuriy Korotkov Renata Litvinova Valery Todorovsky
| starring       = Chulpan Khamatova Dina Korzun
| music          = 
| cinematography = Yuri Shajgardanov
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Russia
| language       = Russian Sign language
| budget         = 
}}

Country of the Deaf ( , Transliteration|translit.&nbsp;Strana glukhikh) is a 1998 Russian crime film directed by Valery Todorovsky, loosely based on the Renata Litvinovas novel To Own and Belong. The film set in a fictional underworld of deaf-mute people in Moscow.

The film was entered into the 48th Berlin International Film Festival.   

==Cast==
* Chulpan Khamatova as Rita
* Dina Korzun as Yaya
* Maksim Sukhanov as Svinya
* Nikita Tyunin as Alyosha Aleksandr Yatsko as The Albino
* Aleksey Gorbunov as Landlord
* Pavel Pajmalov as Mao
* Sergey Yushkevich as Nuna
* Alexey Diakov as Molodoy (as Aleksey Dyakov)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 