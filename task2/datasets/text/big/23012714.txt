Christopher's House
 
{{Infobox film
| name           = Christophers House
| image          = 
| caption        = 
| director       = Lars Lennart Forsberg
| producer       = Måns Reuterswärd
| writer         = Johan Bargum Lars Lennart Forsberg Vilgot Sjöman
| starring       = Thommy Berggren
| music          = 
| cinematography = Lennart Carlsson
| editing        = Lars Lennart Forsberg
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}

Christophers House ( ) is a 1979 Swedish drama film directed by Lars Lennart Forsberg. It competed in the Un Certain Regard section at the 1980 Cannes Film Festival.   

==Cast==
* Thommy Berggren - Kristoffer
* Agneta Eckemyr - Hanna
* Börje Ahlstedt - Börje
* Mimi Pollak - Grandma
* Gunnel Broström - The Mother
* Pia Garde - The Sick Girl
* Majlis Granlund - The Sick Girls Mother
* Linda Megner - Frida
* Birgitta Andersson - Helena Sandholm
* Per Oscarsson - Kräftan
* Silvija Bardh - Siri
* Stig Ossian Ericson - Stenkil
* Gunnar Friberg - Journalist
* Marie Göranzon - Mona
* Sten Johan Hedman - Leif

==References==
 

==External links==
* 

 
 
 
 
 
 