Gopi Krishna (1992 film)
{{Infobox film
| name           = Gopikrishna
| image          = 
| image_size     = 
| caption        =
| director       =V. Ravichandran
| producer       = N. Veeraswamy
| writer         = P. Vasu
| screenplay     = 
| narrator       =  Ravichandran Rupini Rupini   Lokesh  
| music          = Hamsalekha
| lyrics         = Hamsalekha
| cinematography = G S V Seetharam
| editing        = K. Balu
| studio         = Eshwari Productions
| released       = 1992
| runtime        = 129 minutes
| country        = India
| language       = Kannada
| budget         = 
}}
 Kannada romantic Ravichandran in Sumithra and Kushboo and partially based on Hindi film Professor (film)|Professor.

Music and lyrics was composed by Hamsalekha.  The film was produced by N. Veeraswamy for his home banner Eshwari Productions.

==Cast== Ravichandran as Gopikrishna Rupini  Sumithra 
* Lokesh 
* Avinash 
* Mukhyamantri Chandru
* Jyothi
* Girija Lokesh
* Rekha Das
* Mandeep Roy

==Tracklist==
{| class=wikitable sortable
|-
! # !! Title !! Singer(s) || Lyrics
|-
|  1 || "Oho Vasantha" || K. J. Yesudas, S. Janaki || Hamsalekha
|-
|  2 || "Nayakara O Nayaka" || Mano (singer)|Mano, K. S. Chithra || Hamsalekha
|-
|  3 || "Jagavella Jagavella" || Mano, K. S. Chithra || Hamsalekha
|-
|  4 || "En Uduge Idu" || Mano, S. Janaki || Hamsalekha
|-
|  5 || "Sharadammanavare"  || Mano,  K. S. Chithra || Hamsalekha
|-
|  6 || "Chori Chori" || Mano, K. S. Chithra || Hamsalekha
|}

==Reception==
The film and music composed by Hamsalekha was well received and the audio sales hit a record high.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 