Dweepu
{{Infobox film
| name = Dweepu
| image =
| caption =
| director = Ramu Kariyat
| producer =
| writer = Ramu Kariyat Vijayan Karote
| screenplay = Ramu Kariyat Vijayan Karote Jose Shobha Kuttyedathi Vilasini Alleppey Ashraf
| music = MS Baburaj
| cinematography = Ramachandra Babu
| editing = Hrishikesh Mukherjee
| studio = Priya Films
| distributor = Priya Films
| released =  
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed Ramu Kariyat. The film stars Jose (actor)|Jose, Shobha, Kuttyedathi Vilasini and Alleppey Ashraf in lead roles. The film had musical score by MS Baburaj.   

==Cast==
   Jose 
*Shobha 
*Kuttyedathi Vilasini 
*Alleppey Ashraf 
*Prathapachandran 
*Aboobacker
*Baby Ambika
*KPAC Premachandran
*Kedamangalam Ali
*Lakshmisre 
*Prathima
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Vayalar Ramavarma and Yusufali Kechery. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aaranyaanthara   || Kalanilayam Rajasekharan || Vayalar Ramavarma || 
|- 
| 2 || Allithaamara Mizhiyaale || P Jayachandran || Yusufali Kechery || 
|- 
| 3 || Kadale Neelakkadale || Talat Mahmood || Yusufali Kechery || 
|- 
| 4 || Kanneerin Mazhayathum || P Susheela || Yusufali Kechery || 
|- 
| 5 || Kanneerin Mazhayathum || Kalyani Menon || Yusufali Kechery || 
|- 
| 6 || Manimeghappallakkil || P Jayachandran || Yusufali Kechery || 
|}

==References==
 
 
==External links==
*  

 
 
 


 