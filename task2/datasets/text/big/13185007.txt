Do You Love Your Wife?
 
{{Infobox film
| name           = Do You Love Your Wife?
| image          = 
| caption        = 
| director       = Hal Roach
| producer       = Hal Roach
| writer         = 
| starring       = Stan Laurel
| music          = 
| cinematography = Robert Doran
| editing        = Thomas J. Crizer
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  English intertitles
| budget         = 
}}

Do You Love Your Wife? is a 1919 American film starring Stan Laurel.   

==Cast==
* Stan Laurel - The janitor
* Bunny Bixby
* Mary Burns
* Mildred Forbes
* William Gillespie
* Bud Jamison
* Gus Leonard
* Belle Mitchell
* Marie Mosquini
* Lois Neilson
* James Parrott
* William Petterson Charles Stevenson
* Dorothea Wolbert
* Noah Young

==See also==
* List of American films of 1919
* Filmography of Stan Laurel

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 