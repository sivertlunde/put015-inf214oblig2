Mass Effect: Paragon Lost
 
{{Infobox film
| name         = Mass Effect: Paragon Lost
| image        = Mass Effect Paragon Lost.png
| caption      = DVD/Blu-ray Disc cover
| director     =  
| producer     =  
| writer       =  
| starring     =  
| studio       = Production I.G FUNimation Entertainment
| released     =   
| runtime      = 84 minutes
| country      =  
| language     =  
| gross        = 
}}
Mass Effect: Paragon Lost is an animated feature film set in the Mass Effect science fiction universe during the events of Mass Effect 2.  It is produced by BioWare, FUNimation, T.O Entertainment and animated by Production I.G. The film was screened in select theaters on November 29, 2012.  It was released for digital download on Xbox Live and PlayStation Network on December 14, 2012,  and on DVD and Blu-ray on December 28, 2012. 

==Plot==
In 2183, the human colony of Fehl Prime, a major producer of pharmaceuticals for the Systems Alliance, comes under attack by a group of Blood Pack mercenaries led by the krogan Archuk. In response, the Alliance dispatches several teams of Marines to aid the beleaguered colonists. During the landing attempt, each of the Alliance shuttles are shot down by the Blood Pack. Only one Marine unit, Delta Squad, survives the crash landing of their shuttle, but the Blood Pack execute several members before the rest can retreat to cover. Lieutenant James Vega stays behind to help Delta Squads commanding officer, Captain Toni, whose leg was badly broken in the crash. Vega sets Tonis leg, causing him to scream and alert the Blood Pack to their presence. Under heavy fire, Vega and Toni scramble to meet up with the rest of the squad, consisting of Kamille, Nicky, Milque, Essex, and Mason. When Archuk challenges Delta Squad to show themselves and fight, Vega decides to duel Archuk, distracting the Blood Pack while Kamille and Essex flank them and ambush from behind. Nicky aids Vega by sending out a Combat Drone, while Milque provides covering fire with his sniper rifle. At first, Vega is outmatched by Archuk, unable to penetrate the krogans heavy shields, but with the help of Nickys Combat Drone, Archuks shields are overloaded and Vega is able to chip away at his armor. Archuk charges and grabs Vega, but Milque kills the krogan before Archuk can strike the final blow. Meanwhile, Kamille and Essex take out the remaining Blood Pack. The sole survivor, Archuks second-in-command Brood is spared by Vega and taken prisoner. In the aftermath, Admiral Steven Hackett orders Delta Squad to remain on Fehl Prime and protect it from future attacks.

Two years later, Fehl Prime has recovered and the Alliance has bolstered the colonys defenses with a heavy anti-ship cannon and military-grade kinetic barriers. Delta Squad, now a familiar presence to the colonists, is working to finish installing the defenses when Captain Toni orders them to investigate a strange jamming signal originating from the colonys outskirts. The squad is accompanied by asari anthropologist Treeya and the colonist Messner, who lends them his M35 Mako. At the signals location the squad discovers an unknown alien device is its cause; Essex destroys it with his biotics, much to Treeyas chagrin. Taking a fragment of the device, Treeya leads the group to the site of an ancient Prothean communication system, which she uses to contact her mentor, Liara TSoni. When asked what she thinks the device is, Liara reasons that it may be related to Reaper tech before the communication system is suddenly jammed. Treeya voices her doubts to the squad, explaining that Liara once worked with Commander Shepard and has since become obsessed with the myths about the Reapers. As the group begins to head back to the colony, the source of the new jamming is revealed: a Collector ship flies overhead toward the colony.

The Collector ship deploys Seeker Swarms to incapacitate Fehl Primes colonists, including Captain Toni, Christine, and her daughter April, who looks up to Vega as a hero. Only Delta Squad, Treeya, and Messner remain free, having arrived at the colony after the initial attack. Unable to prevent the colonists capture, Vega decides Delta Squad has to disable the Collector ship so the colonists can be saved. They make their way to the colonys anti-ship cannon and begin charging it, but Treeya suggests that a low-power shot might be preferable in order to ensure that the colonists are not harmed. Vega agrees and the cannon is fired. Unfortunately, the shot is too weak and is deflected by the Collector ships shields. Alerted to Delta Squads presence, the Collector ship retaliates with a powerful laser blast that destroys the anti-ship cannon, and releases dozens of Collector drones and more Seeker Swarms. As Delta Squad retreats to the secure underground labs beneath the colony, Kamille is snatched away by drones and Essex is paralyzed by Seekers while defending the labs door.

While the Collector ship demolishes much of the colony above ground, Delta Squad searches the labs Alliance computer banks and learns that Messner is a Cerberus agent who had been trying to get into the labs. Messner confirms this, revealing that he had been tasked with investigating the recent disappearances of human colonies, but points out that he and the members of Delta Squad now have the common goal of surviving the Collector attack. Further investigation of the labs leads the group to a possible antidote for the Seeker Swarms paralyzing agent, as well as the krogan Brood, who had been kept in a tank as a test subject in the labs because it was deemed too dangerous to transport him to the Citadel for trial. The antidote is tested on Essex, but has seemingly no effect. Without warning, the door to the labs is then torn open by a Praetorian with Kamilles body incorporated into it. Mason runs at the Praetorian to save Kamille, but is vaporized. Unable to shoot through the Praetorians heavy shields and armor, Vega, Milque, Nicky, Treeya, and Messner flee deeper into the labs, buying some time by sealing security doors behind them. Brood offers to lead them to his ship, which has remained hidden on the colony after all this time, explaining that helping them would repay his debt to Vega for sparing his life. As the group lifts off aboard Broods ship, the Praetorian emerges and gives chase. A revived Essex appears at the last minute and Vega is able to kill the Praetorian with his help, but not before Nicky is fatally impaled by the Praetorians claw. Vega decides to take the fight to the Collectors and offers the remaining members of the group a drink from his flask before the battle. Brood then pilots the ship toward the Collector ship, blasts a hole in its side, and heads for the drive core. Armed with heavy weapons from Broods personal armory, Vega, Essex, Milque, and Treeya defend Broods ship from Collector drones. Before Brood can take out the drive core, however, Messner shoots him in the head. Without its pilot, Broods ship crashes and the group is taken prisoner by the Collectors.

Now in orbit, the Collector ship fires its main proton cannon, incinerating the remains of the Fehl Prime colony and removing any evidence of the Collector attack. Deep within the ship, Messner explains to Treeya that he was the one who planted the alien device, which was a signal to attract the Collectors. Messner sought to give up the Fehl Prime colonists so that he could learn more about the Collectors and their technology for Cerberuss benefit, an end he believed justified the means. He takes Treeya with him to another part of the ship, leaving Vega, Essex, and Milque locked in pods. Unbeknownst to Messner or the Collectors, the drink Vega had given everyone earlier had been spiked with the Seeker Swarm antidote, and the three are able to escape and take down their guards. Vega finds the dying Brood, who thanks Vega for allowing him to die a warriors death and gives the Marine his last power cell. Vega gives the cell to Essex to charge his biotics, then plants canisters of the antidote in the Collector ships pod transport tubes so that the colonists on board the ship will be exposed to it and be able to free themselves. Vega and Essex then go after Messner and Treeya, while Milque is left behind to pilot a freighter that the Collectors had taken aboard and which Delta Squad can use to escape the ship with the colonists.

Elsewhere, Messner shows Treeya a Prothean archive built into the Collector ship. He had been using colonists to try to interact with it, but the process killed them; since he knows Treeya could use the Prothean communication system they visited earlier, he forces her to access the archive. Treeya succeeds and is able to witness the Protheans last stand against the Reapers the Fehl Prime communication system, their subsequent transformation by the Reapers into Collectors, and the Collectors plan to harvest humans for their genetic material in order to build a Human-Reaper at their homeworld. Messner records this information on his bracelet, realizing it will be invaluable for Cerberus. The Collectors have other plans, however: informing Messner that he is no longer of any use to them and has potentially contaminated their harvest by bringing an asari with him, the Collectors prepare a pod for his "ascension". Messner tries to bargain with the Collectors, but Treeya suddenly makes a break for it, grabbing Messners bracelet before stumbling into the pod. Just as Vega and Essex arrive, the pod is ejected out into space to remove the "contaminant".

Vega and Essex fight the Collectors, killing them and damaging the Collector ships controls. Essex is impaled by the claw of the ships pilot and uses his biotics to fling them both into a chasm, where they fall to their deaths. Messner fires on Vega, but Vega disarms and stabs him. Vega demands Messners intel on the Collectors, but he cries and begs for his life. Treeya contacts Vega and tells him that she has the intel and her pod is beginning to enter Fehl Primes atmosphere; simultaneously, the unpiloted Collector ship has begun to fall from orbit as well. As another group of Collectors moves in on Vegas position, Messner is killed but Milque arrives on the freighter to rescue Vega. Vega now faces a difficult choice: evacuate the colonists, who have been revived by the antidote and are also under attack by the Collectors, or go after Treeya and the vital intel she carries. Vega chooses the latter. As Treeyas pod is retrieved, the Collector ship crashes into Fehl Primes surface and explodes, killing all of the colonists and Collectors aboard, who had hoped until the last second that Vega would come back for them.

On the Citadel, Vega is commended by David Anderson and Admiral Hackett, who inform him that the intel he saved could help the Alliance counter the Collectors in the short term, provide new weapons and technologies in the long term, and provide an antidote for use against Collector swarms. They also inform him that he may one day get to meet his hero, Commander Shepard, who is seemingly alive after all, and give him new orders to report for N7 training. Vega, Milque, and Treeya return to Fehl Prime as the Alliance sifts through the wreckage. After finding a ruined stuffed animal that belonged to the girl April, Vega becomes distraught and runs aimlessly through the remains of the colony, while Milque and Treeya follow, coming to rest at a monument erected in the memory of the lost colonists. Vega resolves to do what he can to honor their sacrifice and ensure that it was not made in vain.

==Cast==
 
{| class="wikitable"
|-
! Cast Member !! Character
|-
| Freddie Prinze, Jr.|| James Vega
|-
| Monica Rial|| Treeya Nuwani
|-
| Eric Vale || Essex
|- Laura Bailey || Kamille
|-
| Vic Mignogna|| Messner
|-
| Marc Swint || Mason
|-
| Todd Haberkorn || Milque
|-
| Josh Grelle || Nicky
|-
| Kara Edwards || Christine
|-
| Jad Saxton || April
|-
| Travis Willingham || Captain Toni
|-
| Jason Douglas || Archuk
|-
| Justin Cook || Brood
|-
| Bruce Carey || Admiral Steven Hackett
|-
| Patrick Seitz || David Anderson
|-
| Jamie Marchi || Liara TSoni
|}

==Soundtrack==
 
{{Infobox album
| Name = Mass Effect: Paragon Lost Original Motion Picture Soundtrack
| Cover = Mass Effect Paragon Lost Original Videogame Score cover.jpg
| Type = Soundtrack
| Artist = Joshua Mosley & David Kates
| Released = November 13, 2012 Motion picture soundtrack
| Length = 1:09:28
| Italic title= no
}}
 Mass Effect and Mass Effect 2. 

{{Track listing
| collapsed= no
| headline = Mass Effect: Paragon Lost Original Motion Picture Soundtrack
| total_length = 1:09:28
| music_credits=yes<!--
-->
| title1   = Paragon Lost - Main Tile
| note1    = sic
| music1   = Joshua Mosley & David Kates
| length1  = 0:50<!--
-->
| title2   = Alliance Marines
| music2   = Joshua Mosley & David Kates
| length2  = 2:18<!--
-->
| title3   = Krogan Battle
| music3   = Joshua Mosley & David Kates
| length3  = 4:04<!--
-->
| title4   = Execution
| music4   = Joshua Mosley & David Kates
| length4  = 0:51<!--
-->
| title5   = Wreckage
| music5   = Joshua Mosley & David Kates
| length5  = 2:26<!--
-->
| title6   = Vegas Tactical
| music6   = Joshua Mosley & David Kates
| length6  = 6:12<!--
-->
| title7   = Hell of a Battle, Son
| music7   = Joshua Mosley & David Kates
| length7  = 1:20<!--
-->
| title8   = Terminus
| music8   = Joshua Mosley & David Kates
| length8  = 2:04<!--
-->
| title9   = April
| music9   = Joshua Mosley & David Kates
| length9  = 2:08<!--
-->
| title10  = The Mass Effect
| music10  = Joshua Mosley & David Kates
| length10 = 3:02<!--
-->
| title11  = Collectors
| music11  = Joshua Mosley & David Kates
| length11 = 2:00<!--
-->
| title12  = Down the Sinkhole
| music12  = Joshua Mosley & David Kates
| length12 = 2:30<!--
-->
| title13  = The Cannon
| music13  = Joshua Mosley & David Kates
| length13 = 3:23<!--
-->
| title14  = Collectors Carnage
| music14  = Joshua Mosley & David Kates
| length14 = 2:55<!--
-->
| title15  = A Spy Revealed
| music15  = Joshua Mosley & David Kates
| length15 = 1:30<!--
-->
| title16  = Enter Brood
| music16  = Joshua Mosley & David Kates
| length16 = 1:14<!--
-->
| title17  = Your Are James Vega
| note17   = sic
| music17  = Joshua Mosley & David Kates
| length17 = 1:10<!--
-->
| title18  = We Got Company
| music18  = Joshua Mosley & David Kates
| length18 = 4:47<!--
-->
| title19  = Nicky
| music19  = Joshua Mosley & David Kates
| length19 = 0:52<!--
-->
| title20  = Lost Souls
| music20  = Joshua Mosley & David Kates
| length20 = 1:46<!--
-->
| title21  = The Cure
| music21  = Joshua Mosley & David Kates
| length21 = 2:11<!--
-->
| title22  = Ascension
| music22  = Joshua Mosley & David Kates
| length22 = 3:08<!--
-->
| title23  = Collect This!
| music23  = Joshua Mosley & David Kates
| length23 = 2:38<!--
-->
| title24  = Paragon Is Lost
| music24  = Joshua Mosley & David Kates
| length24 = 3:33<!--
-->
| title25  = The Emergance of Vega
| note25   = sic
| music25  = Joshua Mosley & David Kates
| length25 = 6:25<!--
-->
| title26  = A Leader Is Born
| music26  = Joshua Mosley & David Kates
| length26 = 0:47<!--
-->
| title27  = Warning Signs
| music27  = The Anix
| length27 = 3:24
}}

 

==References==
 

== External links ==
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 