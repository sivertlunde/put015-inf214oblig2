McBain (film)
 
{{Infobox film
| name     = McBain
| director = James Glickenhaus
| image	   = McBain FilmPoster.jpeg
| writer   = James Glickenhaus Steve James Chick Vennera Jay Patterson
| released =  
| runtime  = 102 minutes
| country  = United States
| language = English
| budget   =
| gross    = $456,127
}}
 Colombian dictator who killed his old friend, a freedom fighter. McBain starred Christopher Walken, Michael Ironside and María Conchita Alonso. Luis Guzmán also appears as a drug dealer named "Papo".

The film was not very successful, taking in less than $500,000 at the United States box office.

== Cast ==
* Christopher Walken as McBain
* Michael Ironside as Frank Bruce Steve James as Eastland
* María Conchita Alonso as Christina Santos
* Victor Argo as El Presidente
* Thomas G. Waites as Gill
* Chick Vennera as Roberto Santos
* Jay Patterson as Dalton
* Luis Guzmán as Papo
* Dick Boccelli as John Gambotti

== The Simpsons connection == analogue of the same name. His appearance on The Simpsons predates the release of this film, and apart from the name, the film has little relation to the character.
 Rainier Wolfcastle, intended as the name of the actor who portrayed McBain (the McBain name has continued to be used to refer to the in-universe series of films and their character). 

== Home video releases==
The movie was released on videocassette in the USA in 1992 by MCA/Universal Home Video, and in Canada that same year by C/FP Video. Years later, Goodtimes released a budget tape of the movie. Synapse Films will release Mcbain on Blu-ray from a newly restored 2K transfer. 

On January 25, 2013 Rifftrax released a Video On Demand version of the movie including a running mocking commentary by Mystery Science Theater 3000 stars Mike Nelson, Kevin Murphy and Bill Corbett. 

==References==
 

== External links ==
*  
*   - Fan community page devoted to the movie

 

 
 
 
 


 