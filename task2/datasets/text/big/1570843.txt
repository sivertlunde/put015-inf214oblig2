The Hills Have Eyes (1977 film)
 
{{Infobox film
| name           = The Hills Have Eyes
| image          = Hillshaveeyesposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Wes Craven
| producer       = Pete Locke
| writer         = Wes Craven Robert Houston Lance Gordon Russ Grieve
| music          = Don Peake
| cinematography = Eric Saarinen
| editing        = Wes Craven
| studio         = Blood Relations Co. Vanguard
| released       = 22 July 1977
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $230,000  (estimated) 
| gross          = $25,000,000
}}
 horror film Nevada desert. cult classic. It is the first film in the The Hills Have Eyes (franchise)|The Hills Have Eyes franchise.

==Plot== John Steadman) is packing his truck when a ragged and somewhat feral teenage girl named Ruby (Janus Blythe) approaches him. She offers to trade what she has in her bag for food, but the old man refuses. They walk into a small cabin, and Fred scolds her for what "they" have done. Ruby says that her family ambushed a nearby airfield because they were hungry and no one passes by their home anymore. She pleads with Fred to take her with him, and he tells Ruby if "the pack", in particular someone named Jupiter, learns what she is doing, she could be in danger. She replies that his life will be in danger as well, if Jupiter finds out hes trying to leave. A noise distracts them, and Ruby hides.
 Robert Houston) and Brenda (Susan Lanier), eldest daughter Lynne (Dee Wallace), Lynnes husband Doug Wood (Martin Speer), their baby daughter Katie, and their dogs, Beauty and Beast. They stop at Freds Oasis for fuel, and Fred urges them to stay on the main road as they leave. Freds truck suddenly explodes, preventing him from leaving. Ignoring Freds warning, the Carters skid off a desert road and crash. Bob walks back to Freds Oasis to get help. Bobby chases a panicked Beauty into the hills and finds her mutilated body. Frightened, he runs and falls, knocking himself unconscious.

As night falls, Bob reaches the gas station, where he finds Fred trying to hang himself. Fred tells him his sons family of deranged cannibals dwell in the hills through which the Carters are traveling. They are commanded by Papa Jupiter (James Whitworth), who killed his mother, Freds wife, during childbirth. As a child, he killed the livestock on his fathers farm and later murdered his sister. Fred attacked his son with a tire iron and left him in the hills to die. Jupiter survived, however, and had children with a depraved, alcoholic prostitute known as Mama (Cordy Clark). Together, they had three sons, Mars (Lance Gordon), Pluto, (Michael Berryman) and Mercury (Arthur King), and an abused daughter, Ruby. They survive by stealing from and cannibalizing travelers. Papa Jupiter suddenly arrives and takes Bob prisoner, after killing Fred with a tire iron.

Bobby returns to the trailer but doesnt mention Beautys death, not wanting to frighten the rest of the family. He gets locked out of the trailer and asks Doug for his keys, unaware the trailer is locked because Pluto is looking through their valuables, while Ethel and Brenda sleep. Before Bobby enters the trailer, Pluto signals Papa Jupiter to set Bob ablaze on a stake in the distance. Ethel, Lynne, Doug, and Bobby rush to save Bob, while Brenda stays in the trailer with the baby. As they try to extinguish the fire, Pluto and Mars ransack the camper, and Mars rapes Brenda. The Carters eventually extinguish the fire, but Bob dies shortly afterward. When Ethel and Lynne return to the trailer, Lynne is attacked by Mars, and Ethel hits him with a broom. After Mars shoots Ethel and Lynne,  Pluto abducts the baby, and the brothers flee. Hearing the screams, Doug and Bobby rush in the camper to find Lynne dead and Ethel mortally wounded.

The men of the clan return to the cave, but Beast pushes Mercury off a hilltop, to his death. Ruby is chained outside the cave, with Mama tormenting her, and is forced to eat Beauty as punishment for her betrayal. 

The next morning, shortly after Ethel dies, Doug sets out to find his baby, while Papa Jupiter and Pluto set out to kill the survivors. Beast tears Plutos throat out, and Papa Jupiter is killed by a trap set by Brenda and Bobby using their mothers corpse. Doug sees Ruby knock out Mama and escape with Katie into the hills.

Doug catches up with Ruby and the baby, but Mars follows them. As he tries to kill Doug, Ruby interferes by putting a rattlesnake on Mars neck, enabling Doug to overpower him, and Doug frantically stabs Mars to death while Ruby weeps over her brothers body.

==Cast==

* Susan Lanier as Brenda Carter
* Michael Berryman as Pluto
* Dee Wallace as Lynne Wood John Steadman as Fred Robert Houston as Bobby Carter
* Martin Speer as Doug Wood
* Russ Grieve as Bob Carter
* James Whitworth as Papa Jupiter
* Virginia Vincent as Ethel Carter
* Lance Gordon as Mars
* Janus Blythe as Ruby
* Cordy Clark as Mama Arthur King as Mercury
* Brenda Marinoff as Baby Katie Wood

==Production==
 

==Conception==
 
The film was conceived as a modern retelling of the  uous family members, similar to the Sawney Bean family that inspired the story. In addition, the film was set in 1994, took place in a forest, rather than a desert, and most of the major cannibals (such as Mars, Pluto and Mercury) were adolescence|adolescents. The baby was stolen not for food, but for a perverted religious ritual.

==Release== X rating by the MPAA. The Umbrella DVD contains a commentary with Director Wes Craven, and Producer Peter Locke in which they discuss the censorship problems.  Following scenes were cut for R rating;

* Scene where Fred is beaten to death by Jupiter was cut down a lot.
* Scene where Mars and Pluto kidnap the baby was also cut down.
* Originally while eating Big Bobs arm, Jupiter thrust it into Big Bobs face and stick its finger into Bobs eye. More of this scene was also cut down.
* Scene where Katie hits Jupiter in back with ax.
* Scene where Doug repeatedly stabs Mars was cut down but apparently all the cuts were snuck back in.

The deleted footage is believed to be lost, though the alternate ending was included on the 2003 Anchor Bay DVD.

==Reception==
  cult following. The film currently has an approval rating of 64% on review aggregator website Rotten Tomatoes, based on 22 reviews, and is certified "fresh".  Austin Chronicle wrote, "Inventive story ideas and humorous touches give this horror picture an enduring relevancy and stylistic flourish."  Roger Ebert criticized the film for being "decadent."   

The Hills Have Eyes was ranked No. 41 on Bravo (US TV channel)|Bravos 100 Scariest Movie Moments for the scene where Mars and Pluto attack the trailer and try to steal the baby. The film was nominated for AFIs 100 Years...100 Thrills. 

==Intertextuality==
 intertextual references The Evil Dead. Wes Craven then portrayed his characters watching Evil Dead on television in his 1984 movie A Nightmare on Elm Street. Raimi explained the intertextuality in the 1989 documentary Stephen Kings This is Horror:
:One of the things they see is a picture of a Jaws poster, the monster, thats been like ripped in half. So I took it to mean that Wes Craven, the director of the movie, was saying: "Jaws was just pop horror. What I have here is real horror. Its rip that baby in half."
:So as a joke and as an homage to Wes Craven I took a Hills Have Eyes poster and in Evil Dead I put it in the cellar in one of the sets. And I ripped it in half, to say "No Wes, your picture is pop horror, this is real horror." But just as a joke of course.
:Now, Wes Craven has responded in his picture by putting a clip of Evil Dead 1 as a horror movie in a scene that a character is watching in his movie Nightmare on Elm Street. I guess its to say: "No, your movie Evil Dead is just a horror movie. Nightmare on Elm Street is the real horror". 

==Sequel and remake==
 remake of sequel to the remake in 2007.

==Music==
 
The films soundtrack was written and performed by Don Peake. The extensive score containing a total of 41 cues was released in 2009 on CD by Hitchcock Media Records. In 2014 it was re-released on vinyl and cassette by One Way Static Records. The vinyl edition contains extensive liner notes by Don Peake and the films cast and crew.

==See also==
*List of incomplete or partially lost films

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 