Typhon sur Nagasaki
{{Infobox film
| name           = Typhon sur Nagasaki
| image          = 
| caption        = 
| director       = Yves Ciampi
| producer       = Cila Films
| writer         = Jean-Charles Tacchella Yves Ciampi
| starring       = Jean Marais Danielle Darrieux
| music          = Chuji Kinoshita
| cinematography = 
| editing        = 
| distributor    = 
| released       = 6 February 1957 (France); 13 September 1957 (Finland)
| runtime        = 115 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
}}
 French Drama drama Romance romance film from 1957, directed by Yves Ciampi, written by Jean-Charles Tacchella, starring Jean Marais.  The film was known under the title "Typhoon Over Nagasaki" (international English title). 

== Cast ==
* Danielle Darrieux : Françoise Fabre
* Jean Marais : Pierre Marsac
* Keiko Kishi : Noriko Sakurai
* Sō Yamamura : Hori
* Hitomi Nozoe : Saeko Sakurai
* Kumeko Urabe : Fujita
* Gert Fröbe : Ritter
* Shinobu Asaji : Keiko Ritter

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 
 
 
 


 