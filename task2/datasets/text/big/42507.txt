In the Bedroom
{{Infobox film
| name           = In the Bedroom
| image          = In_the_Bedroom_Theatrical_Release_Poster,_2001.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Todd Field
| producer       = Todd Field Ross Katz Graham Leader
| screenplay     = Todd Field Robert Festinger
| based on       =  
| starring       = Sissy Spacek Tom Wilkinson Nick Stahl Marisa Tomei
| music          = Thomas Newman
| cinematography = Antonio Calvache
| editing        = Frank Reynolds
| studio         = Good Machine Eastern Standard Film Company GreeneStreet Films
| distributor    = Miramax Films
| released       =  
| runtime        = 131 minutes
| country        = United States
| language       = English
| budget         = $1.7 million http://www.boxofficemojo.com/movies/?id=inthebedroom.htm 
| gross          = $43.4 million 
}} crime drama film directed by Todd Field, and dedicated to Andre Dubus, whose short story Killings is the source material on which the screenplay, by Field and Robert Festinger, is based. The film stars Sissy Spacek, Tom Wilkinson, Nick Stahl, Marisa Tomei, and William Mapother.

The title refers to the rear compartment of a lobster trap known as the "bedroom" and the fact that it can only hold up to two lobsters before they begin to turn on each other.

==Plot==
The film is set in the Mid-Coast town of Camden, Maine|Camden, Maine. Ruth Fowler (Sissy Spacek) and Matt Fowler (Tom Wilkinson) enjoy a happy marriage and a good relationship with their son Frank (Nick Stahl), a recent college graduate who has come home for the summer. Frank has fallen in love with an older woman with children, Natalie Strout (Marisa Tomei). Frank is also applying to graduate school for architecture, but is considering staying in town to work in the fishing industry and be near to Natalie. Natalies ex-husband, Richard Strout (William Mapother), whose family owns a local fish-processing and delivery business, is violent and abusive. Richard tries to find a way into his ex-wife and sons lives, going to increasingly violent lengths to get his intentions across to Natalie. Ruth is openly concerned about Franks relationship with Natalie, while Matt sees past his wifes worries.

Midway through the film, Richard kills Frank during a confrontation at Natalies house following a domestic dispute. Though equally devastated, Matt and Ruth grieve in different ways, with Matt putting on a brave face while Ruth becomes reclusive and quiet. Richard is set free on bail, paid by his well-to-do family, and both Matt and Ruth are forced to see Richard around town. The tension between the pair increases when they learn that the lack of a witness to their sons shooting allows the killer to avoid murder charges, since the district attorney may have difficulty proving that Richard killed Frank intentionally, as opposed to accidental manslaughter during a struggle, which defense attorney Marla Keyes (Karen Allen) argues. The silence between the couple erupts in an argument where each is confronted with the truth about each parents relationship with their son: Ruth was overbearing, and Matt let him get away with everything. With the strain between them broken, the couple is finally able to find common ground in their grief.

Matt then abducts and kills Richard. He and a friend bury the body on the friends wooded property. Matt returns home to Ruth, who is awake and smoking in bed. She asks him, "Did you do it?" Matt appears troubled and unresponsive. He climbs into bed and then turns away from her. Finally, Ruth gets up to make coffee. Matt rolls over onto his back and pulls a band-aid from a finger he injured hauling traps. Ruth calls from the kitchen, "Matt, do you want coffee?" Matt doesnt answer.

==Cast==
* Sissy Spacek as Ruth Fowler
* Tom Wilkinson as Matt Fowler
* Nick Stahl as Frank Fowler
* Marisa Tomei as Natalie Strout
* William Mapother as Richard Strout
* Celia Weston as Katie Grinnel
* Karen Allen as Marla Keyes
* William Wise as Willis Grinnel
* Justin Ashforth as Tim Bryson
* Camden Munson as Jason Strout
* Frank T. Wells as Henry

==Critical reception==
  and Sissy Spacek|Spaceks performances were highly praised by film critics.]]
Upon its release, the film received positive responses for its direction, script, and performances (notably Wilkinson and Spacek), garnering a 93% certified fresh rating on Rotten Tomatoes based on 137 reviews with an average score of 7.9/10. The sites consensus states "Expertly crafted and performed, In the Bedroom is a quietly wrenching portrayal of grief."   

David Edelstein of Slate Magazine wrote on his review that it is the "best movie of the last several years" and described it "the most evocative, the most mysterious, the most inconsolably devastating" film. He further mentioned that the effect of the film "isnt over when you leave the theater" and that its "always going to be there". He also called In the Bedroom a "masterpiece". 
 Ozu and Ingmar Bergman|Bergman, but that he fully understands their processes... Fields achievement is such a perfectly consummated marriage of intent and execution that he need never make another movie. I would not be alone, I think, in hoping he will make many more." 

William Arnold of the Seattle Post-Intelligencer compared Fields direction to Kubrick saying that it "manages to feel both highly controlled and effortlessly spontaneous at the same time; and his lifting of the facade of this picturesque, Norman Rockwell setting is carried out with surgical precision". He further mentioned that "like Kubrick, Field doesnt make any moral judgments about his characters, and his film remains stubbornly enigmatic. It can be read as a high-class revenge thriller, an ode to the futility of vengeance or almost anything in between." 

Roger Ebert of the Chicago Sun-Times stated on his review that it is "one of the best-directed films of the year" and that "every performance has a perfect tone".     He listed In the Bedroom as his third best film of the year 2001.   
 Oscar would be a fool" if they ignore Sissy Spacek and Tom Wilkinsons "career-crowning performances".   
 New York Times essay "The most important films of the past decade — and why they mattered." 

Among the negative reviews of the film include Paul Tatara of the CNN mentioning that the film "flounders" despite the good performances.     Stephen Hunter of The Washington Post said "it opens brilliantly" but goes on to "self-negating absurdity."

===Sundance===
In the Bedroom was the first official Sundance Film Festival film to get an Academy Award nomination for Best Picture including three more nominations for acting and an adapted screenplay nomination. It nabbed the most nominations than any Sundance film until 2009s Precious (film)|Precious.   

===Box office===
With the exception of Napoleon Dynamite, In the Bedroom had the largest box office of any film premiering at the Sundance Film Festival in the last decade.  The film grossed a worldwide total of $43,368,779.  It went on to become, at-the-time, the highest-grossing non-IMAX film in history to never reach the top 10 in a given week. 

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipients and nominees
! Outcome
|- Academy Awards
| Best Picture
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Best Actor
| Tom Wilkinson
|  
|-
| Best Actress
| Sissy Spacek
|  
|-
| Best Adapted Screenplay
| Robert Festinger and Todd Field
|  
|-
| Best Supporting Actress
| Marisa Tomei
|  
|- 
|-style="border-top:2px solid gray;" American Film American Film Institute Awards
| Top 10 Films
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Actor of the Year
| Tom Wilkinson
|  
|-
| Actress of the Year
| Sissy Spacek
|  
|- 
|-style="border-top:2px solid gray;" BAFTA Awards
| Best Actor in a Leading Role
| Tom Wilkinson
|  
|-
| Best Actress in a Leading Role
| Sissy Spacek
|  
|- 
|-style="border-top:2px solid gray;" Broadcast Film Critics Association Awards
| Best Film
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Best Actress
| Sissy Spacek
|  
|-
| Best Supporting Actress
| Marisa Tomei
|  
|- 
|-style="border-top:2px solid gray;" Chicago Film Critics Association Awards
| Best Film
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Best Actor
| Tom Wilkinson
|  
|-
| Best Actress
| Sissy Spacek
|  
|-
| Best Supporting Actress
| Marisa Tomei
|  
|-
|-style="border-top:2px solid gray;"
| rowspan="2"|Dallas-Fort Worth Film Critics Association Awards
| Best Actress
| Sissy Spacek
|  
|-
| Best Supporting Actress
| Marisa Tomei
|  
|- 
|-style="border-top:2px solid gray;"
| Florida Film Critics Circle Awards
| Best Actress
| Sissy Spacek
|  
|- 
|-style="border-top:2px solid gray;" Golden Globe Awards
| Best Motion Picture - Drama
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Best Actress - Motion Picture, Drama
| Sissy Spacek
|  
|-
| Best Supporting Actress - Motion Picture
| Marisa Tomei
|  
|- 
|-style="border-top:2px solid gray;" Independent Spirit Awards
| Best First Feature
| Todd Field
|  
|-
| Best Male Lead
| Tom Wilkinson
|  
|-
| Best Female Lead
| Sissy Spacek
|  
|- 
| Best Screenplay
| Robert Festinger and Todd Field
|  
|-
|-style="border-top:2px solid gray;" Los Angeles Film Critics Association Awards
| Best Film
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Best Actress
| Sissy Spacek
|  
|-
|-style="border-top:2px solid gray;" National Board of Review Awards
| Best Director
| Todd Field
|  
|-
| Best Screenplay
| Robert Festinger and Todd Field
|  
|-
|-style="border-top:2px solid gray;" New York Film Critics Circle Awards
| Best First Film
| Todd Field
|  
|-
| Best Actor
| Tom Wilkinson
|  
|-
| Best Actress
| Sissy Spacek
|  
|-
|-style="border-top:2px solid gray;" Online Film Critics Society Awards
| Best Film
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Best Director
| Todd Field
|  
|-
| Best Actor
| Tom Wilkinson
|  
|-
| Best Actress
| Sissy Spacek
|  
|-
| Best Supporting Actress
| Marisa Tomei
|  
|-
| Best Screenplay - Adapted
| Robert Festinger and Todd Field
|  
|-
| Best Breakthrough Filmmaker
| Todd Field
|  
|-
|-style="border-top:2px solid gray;" Satellite Awards
| Best Film - Drama
| Graham Leader, Ross Katz, Todd Field
|  
|-
| Best Actress - Drama
| Sissy Spacek
|  
|-
| Best Supporting Actress - Drama
| Marisa Tomei
|  
|-
| Best Screenplay - Adapted
| Robert Festinger and Todd Field
|  
|-
|-style="border-top:2px solid gray;" Screen Actors Guild Awards
| Outstanding Performance by a Male Actor in a Leading Role
| Tom Wilkinson
|  
|-
| Outstanding Performance by a Female Actor in a Leading Role
| Sissy Spacek
|  
|-
| Outstanding Performance by a Cast in a Motion Picture
| William Mapother, Sissy Spacek, Nick Stahl, Marisa Tomei, Celia Weston, Tom Wilkinson, William Wise
|  
|-
|-style="border-top:2px solid gray;" Southeastern Film Critics Association Awards
| Best Actress
| Sissy Spacek
|  
|-
| Best Supporting Actress
| Marisa Tomei
|  
|-
|-style="border-top:2px solid gray;" Sundance Film Festival Awards
| Special Jury Prize - Dramatic Acting
| Sissy Spacek and Tom Wilkinson
|  
|-
|-style="border-top:2px solid gray;"
| USC Scripter Award
| USC Scripter Award
| Robert Festinger and Todd Field (screenwriters) and Andre Dubus (author)
|  
|-
|-style="border-top:2px solid gray;"
| Vancouver Film Critics Circle
| Best Actress
| Sissy Spacek
|  
|-
|}

==Film archives== safety print is housed in the permanent collection of the UCLA Film & Television Archive.  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

;Publications
*   –  9 August 2002, New York Times
*   –  3 January 2003, New York Times
*  , The Film Journal Review of In the Bedroom by Rick Curnutte
*  
*  , preface by Todd Field

;Academic papers
*   30 April 2009

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 