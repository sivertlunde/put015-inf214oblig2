Go West (2005 film)
{{Infobox Film
| name           = Go West
| image_size     = 
| image	=	Go West FilmPoster.jpeg
| caption        =  Ahmed Imamović
| producer       = Samir Smajić
| writer         = Ahmed Imamović Enver Puška
| narrator       = 
| starring       = Mario Drmać Tarik Filipović
| music          = Enes Bure Zlatar
| cinematography = Mustafa Mustafić
| editing        = Andrija Zafranović Mirsad Tabaković
| distributor    = Comprex
| released       = 2005
| runtime        = 97 min.
| country        = Bosnia and Herzegovina Bosnian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Ahmed Imamović. Bosniak and the other a Serb, during the Bosnian War. It was developed jointly by two studios, one being in Bosnia and the other in Croatia.

==Plot==

Kenan (Mario Drmać), a Bosniak classical musician, and Milan (Tarik Filipović), a Serb, live in Sarajevo, Bosnia and Herzegovina in a clandestine gay relationship. When the Bosnian War breaks out in 1992, they try to escape from the city. In order to hide from Serbian militamen, Kenan disguises himself as a woman and Milan passes him of as his wife. Together, they go to Milans village in Eastern Bosnia, a Serb stronghold, where they continue to live in deception. However, Milan is soon conscripted into the army and Kenan is left behind in the village. Ranka, a woman from the village, discovers Kenans secret and seduces him.

==Cast==
*Tarik Filipović as Milan
*Rade Šerbedžija as Ljubo
*Mirjana Karanović as Ranka
*Mario Drmać as Kenan
*Haris Burina as Lunjo
*Jeanne Moreau as Journalist
*Nermin Tulić as Priest Nemanja
*Almedin Leleta as Alen
*Almir Kurt as Drago
*Milan Pavlović as Milo
*Orijana Kunčić as Posilna

==Release==
The film premiered at the 2005 Cannes Film Festival in May.

==Reception==
Go West was nominated for the Grand Prix des Amériques award at the Montréal World Film Festival, 2005. The film also won the audience award for the best film at the 2006 Bosnian-Herzegovinian Film Festival in New York. 
  

The film has received the prize for the Best Film at the Madrid Móstoles International Film Festival, celebrated on October 20&ndash;28, 2007.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 