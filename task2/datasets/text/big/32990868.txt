Death's Marathon
{{infobox film
| name            = Deaths Marathon
| image           =
| imagesize       =
| caption         =
| director        = D. W. Griffith
| producer        =
| writer          = William E. Wing
| starring        = Blanche Sweet Henry B. Walthall Lionel Barrymore
| music           =
| cinematographer = G. W. Bitzer
| editing         =
| distributor     = Biograph Company
| released        = June 14, 1913
| runtime         = 2 reels; (17 mins @ 16fps)
| country         = United States
| language        = Silent film(English intertitles)
}}
Deaths Marathon is a 1913 silent film short directed by D. W. Griffith and distributed by Biograph Company|Biograph. It stars Blanche Sweet and Henry B. Walthall and was filmed in the Los Angeles area. This film survives and is available on DVD. 

==Cast==
*Blanche Sweet - The Wife
*Henry B. Walthall - The Husband Walter Miller - The Friend, The Husbands Partner
*Lionel Barrymore - The Financial Backer
*Kate Bruce - The Nanny
*Robert Harron - The Messenger

uncredited
*William J. Butler - Man at Club
*Harry Hyde - A Fried at Club
*J. Jiquel Lanoe - Man at Club
*Adolph Lestina - Man at Club
*Charles Hill Mailes - 
*Alfred Paget - A Gambler at Club
*W. C. Robinson - Man at Club

==References==
 

==External links==
* 
*   on YouTube
* 
*  (archive.org)

 
 
 
 
 
 
 
 
 


 
 