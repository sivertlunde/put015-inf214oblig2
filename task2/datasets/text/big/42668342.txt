Sathyavan Savithri
 
{{Infobox film
| name = Sathyavan Savithri
| image = 
| caption =
| director = Ramesh Arvind
| writer = Rajendra Karanth Cactus Flower (1969)
| starring = Ramesh Aravind  Daisy Bopanna  Jennifer Kotwal
| producer = Ajay Chandani
| music = Gurukiran
| cinematography = PKH Das
| editing = P. R. Soundar Rajan
| studio = Ajay Films
| released =  
| runtime = 137 minutes
| language = Kannada
| country = India
| budget =
}}
 comedy film Cactus Flower (1969) and Hindi as Maine Pyaar Kyun Kiya? (2005).

The film features Ramesh Aravind, Jennifer Kotwal and Daisy Bopanna in leading roles.  It features soundtrack and score by Gurukiran.

== Cast ==
* Ramesh Aravind as Sathyavan
* Jennifer Kotwal as Monisha
* Daisy Bopanna as Subbulakshmi
* Nethra
* Mohan Shankar
* Aniruddh
* Komal Kumar
* H. G. Dattatreya
* Sadashiva Brahmavar
* Sundar Raj
* Shobha Raghavendra
* Pal Chandani

==Plot==
The film is all about the hilarious incidents that happen in a dentist Sathyavans (Ramesh Aravind) life who is a big time flirt and never believes in marriage. His grandfather (Dattatreya) plays a game of handing over his luxurious property to a charitable trust if he does not heed to the request of marriage. Sathyavan meets a girl Monisha (Jennifer Kotwal) and tries to woo her for marriage. Meanwhile, another girl Subbulakshmi (Daisy Bopanna) comes into his life as his secretary and the proceedings lies on whom Sathyavan chooses to marry.

== Soundtrack ==
The music was composed by Gurukiran for Anand Audio company. The audio was launched and released to the market on June 13, 2007. The song "Dr. Sathya" is heavily inspired from Barbie Girl song performed by Danish based dance group Aqua (band)|Aqua.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Dr. Sathya
| extra1 = Gurukiran, Chaitra H. G. Kaviraj
| length1 = 
| title2 = First Time Ninna Nodidaaga
| extra2 = Lakshmi Manmohan Kaviraj
| length2 = 
| title3 = Hello Boys
| extra3 = Apoorva, L. N. Shastry, Gurukiran
| lyrics3 = Rajendra Karanth
| length3 = 
}}

==Review==
Upon release, the film met with favorable critical reviews for its comical content and character portrayals. Rediff.com reviewed with 3.5 stars saying the film is hilarious and is an enjoyable fare for family audiences.  Deccan Herald reviewed saying the film is a clean entertainer. 

==See also==
* Cactus Flower (film)
* Maine Pyar Kyun Kiya?

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 