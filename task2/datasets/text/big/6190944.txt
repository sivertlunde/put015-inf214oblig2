Mamba (film)
{{Infobox film
| name           = Mamba
| image          = Mamba_Title_Card.jpg
| caption        =
| director       = Albert S. Rogell
| producer       =
| writer         = Ferdinand Schumann-Heink (story) John Reinhardt (story) Tim Miranda (screenplay) Winifred Dunn (screenplay)
| starring       = Jean Hersholt Eleanor Boardman Ralph Forbes
| music          = James C. Bradford
| cinematography = Charles P. Boyle (Technicolor)
| editing        =
| distributor    = Tiffany Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}}
Mamba (1930 in film|1930) was released by Tiffany Pictures.  It was shot entirely in Technicolor and stars Jean Hersholt, Eleanor Boardman, Ralph Forbes, Josef Swickard, Claude Fleming, William Stanton and William von Brincken. It was based on a story by Ferdinand Schumann-Heink and John Reinhardt and was advertised as the First Drama In Natural Color as all previous color features in sound had featured musical numbers.

==Plot==
The film takes place in Neu Posen, German East Africa sometime before the First World War.

"Mamba" is the name given to a South African snake. The reptile of this adventure is Auguste Bolte (played by Jean Hersholt), who is constantly reminding those with whom he has a chance to converse that he can buy anything. He neglects his appearance and does not even bother to shave or brush his hair. The German officers hold themselves aloof from him and the only individual he has an opportunity to talk to at length is his valet-secretary, a Cockney, who feeds his master with flattery. One afternoon Bolte recalls that he has received a letter asking for $100,000 from Count von Linden. The Count is in Germany and in a footnote it is written that Bolte might marry von Lindens daughter, Helen. The white people of the post have as little to do with Bolte as possible and the British officers across the frontier also spurn him. It occurs to Bolte that a beautiful wife would perhaps help to make life more agreeable for him. He thinks also that the officers would then overlook some of his failings and be quite impressed. He therefore allies himself to Germany.

Helen (played by Eleanor Boardman), like most daughters who marry wealthy villains in melodramas, does so to save her father from ruin. There is a flash of the wedding and soon Helen and her ignoble husband are seen aboard the steamship bound for East Africa. On the same vessel is Karl von Reiden, the officer who is to take charge of the Neu Posen post. He is not averse to a little flirtation with a beautiful woman and therefore when Helen goes out on deck to avoid Bolte, Karl succeeds in meeting her. These scenes are fairly well filmed and the color effects are capital. Karl, played by Ralph Forbes, is a handsome fellow. So soon as he knows that Bolte is Helens husband he realizes that the marriage is not to her liking. Later these passengers are on the river boat, and when that craft reaches Neu Posen. Bolte stands on the aft deck hoping to make all the German officers envious of his attractive bride. He later gives a feast and takes good care to make a show of his wealth, even to having a procession of natives carrying the viands.

A visit from a native woman interrupts the proceedings, and in a subsequent passage Bolte, enraged with his wife, is about to flog her with a whip when Karl comes to the rescue. All this happens just prior to the World War, and in the closing chapters word is received by both the Germans and the British that hostilities have been declared. Bolte, the snake, believes that money can buy his freedom from military service, but soon he learns otherwise. He is compelled to don a uniform and then decides to run away. His end is sudden, for he fires at one group of natives without knowing that others are behind him. They know something about Bolte and his pleas for his life fall on deaf ears. There follow episodes in which Karl goes to the rescue of Helen and others, who are in danger of an attack by the natives. These are pictured with due attention for red blood on the heros shirt. It seems that the Britishers might have been more solicitous about Karls wounds, but all the British commandant says when he comes up to Karl is to ask him whether he will have another Piccadilly cigarette.

==Cast==
*Jean Hersholt ...  August Bolte (Mamba)  
*Eleanor Boardman   ...  Helen von Linden  
*Ralph Forbes   ...  Karl von Reiden  
*Claude Fleming   ...  Major Cromwell   Will Stanton   ...  Cockney servant  
*Wilhelm von Brincken   ...  Major von Shultz  
*Hazel Jones   ...  Hassims daughter   Arthur Stone   ...  British soldier  
*Torben Meyer   ...  German soldier  
*Andrés de Segurola   ...  Guido  
*Edward Martindel   ...  Fullerton  
*Noble Johnson   ...  Hassim

==Production background==
According to an advertisement from the studio, the film cost more than $500,000 to produce.  The film was well received by the public. The color photography was especially commented on as being outstanding. According to the Los Angeles Times, the film was banned in Germany because the censors deemed it to be denigrating to Germans. 

The film is now in the public domain as Tiffany filed for bankruptcy in 1932 and the copyright was never renewed.

==Preservation status==
Mamba was previously considered partially a lost film. The final reel of the film exists at the UCLA Film and Television Archive and the complete soundtrack survives on Vitaphone sound disks.

An article in the Spring/Summer 2009 issue of the Vitaphone Project newsletter announced that a collector in Australia owns a complete 35mm nitrate print of Mamba.  The article went on to say that a complete restoration is now possible, and only lacks funding. The films first screening in 81 years took place on November 21, 2011, at The Astor Theatre in Melbourne, although a full-fledged restoration had not occurred at the time of the screening.  

In January 2012, the Vitaphone Project announced that the U.S. premiere of the film would be in March 2012 at Cinefest in Syracuse, New York. 

==See also==
*List of early color feature films
*List of rediscovered films

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 