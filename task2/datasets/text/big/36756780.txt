Home in Wyomin'
{{Infobox film
| name           = Home in Wyomin
| image          = Home_in_Wyomin_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = William Morgan
| producer       = Harry Grey
| screenplay     = {{Plainlist|
* Robert Tasker
* M. Coates Webster
}}
| story          = Stuart Palmer
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Fay McKenzie
}}
| music          = Raoul Kraushaar (supervisor) Ernest Miller
| editing        = Edward Mann
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 67 minutes Magers 2007, p. 211. 
| country        = United States
| language       = English
| budget         = $84,006 
| gross          = 
}} Western film directed by William Morgan and starring Gene Autry, Smiley Burnette, and Fay McKenzie.     Based on a story by Stuart Palmer, the film is about a singing cowboy who helps out a former employer in trouble with his failing rodeo while romancing a woman reporter.  In Home in Wyomin, Autry sang his hit songs "Be Honest with Me", "Back in the Saddle Again", and "Tweedle OTwill", as well as Irving Berlins "Any Bonds Today", becoming the first major star to sing the official song of the U.S. Defense Bond campaign during the war. 

==Plot==
Singing cowboy Gene Autry (Gene Autry) appears on a radio program to promote U.S. Defense Bonds. In the audience, wisecracking photographer Clementine Benson (Fay McKenzie) and reporter "Hack" Hackett (Chick Chandler) make fun of the singer and his devoted fans. After the program, Clementine and Hack try to get an embarrassing story on Gene, who knows their game and eludes the pair, who previously ridiculed him in print.

Gene is approached by Pop Harrison (Forrest Taylor), the owner of the rodeo troupe that gave Gene and his sidekick Frog Millhouse (Smiley Burnette) their start. Pop is worried that his son Tex is destroying their rodeo through his irresponsible drinking and gambling. Always ready to help out a friend, Gene decides to drive to Gold Ridge, Wyoming to lend a hand with the rodeo and straighten Tex out. Frog and Frogs brother Tadpole accompany him to Wyoming. They are followed by Clementine and Hack, who were ordered to follow Gene.

When they arrive at Bartletts dude ranch in Wyoming, they watch Tex and the boys are preparing for their next rodeo. Hacks laughter at Texs drunkenness provokes a fistfight, and Hack, a former prizefighter, easily beats up the drunken cowboy. Afterwards, Gene uses the incident as an opportunity to give Tex a stern lecture. Tex agrees to start acting more responsibly, and Gene tells him to attend that nights barbecue so the others can see that hes changed his ways.

That night, Hack follows three suspicious ranch guests to a saloon, where he joins them in a card game along with Tex and Sunrise (Olin Howland), an eccentric miner who shows off some gold he found. Hack quickly wins several hands, happy to take their money and Sunrises gold. The leader of the three suspicious ranch guests, Crowley (George Douglas), is unnerved when Hack implies that he knows them from Chicago and is aware of Crowleys shady past. Gene comes to the saloon and retrieves Tex. After Hack leaves the saloon, Crowley, who is actually a gangster named Luigi Scalese, and his men follow him. On the road, someone takes a shot at Hack, nearly missing him.

The following day, as Tex performs in a staged shootout during the rodeo, Hack is shot and killed in the audience by someone who then loads a real bullet in Texs gun which was dropped during the confusion. When the sheriff (Hal Price) investigates, he discovers the real bullet in Texs gun and arrests him, despite the protests of Gene and his friends. Later, Clementine tells Gene about Hacks suspicion of Crowley, and Gene prevents Crowley and his men from leaving the ranch.

Crowly sends letters to Clementine and Gene, making it appear that each is inviting the other on a date that evening. While Clementine is away, Crowley breaks into her room and steals her photograph case in order to destroy pictures of himself in the crowd. Later Gene sends the remaining photographs off for fingerprinting, and soon after, Pop receives a telegram identifying the fingerprints on the photographs as belonging to Chicago mobster Luigi Scalese, Crowleys true identity.

Later that day, someone breaks into Pops home, shoots him, and takes the incriminating telegram. Gene suspects Crowley and races off to arrest him. Clementine stays behind and learns from Pop that there is a secret entrance to a mine beneath his house. Clementine asks Sunrise to lead her through the tunnels of the mine, and quickly discovers that Sunrise is insane, and that he killed Hack and wounded Pop fearing they would divulge the location of his precious gold. As Sunrise attempts to kill Clementine, Gene arrives and saves her, as the crazy miner falls down a mine shaft to his death. Afterwards, as Pop recovers from his wound, Tex promises him that he will really straighten up this time, and Clementine and Gene ride off together singing a song. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* Fay McKenzie as Clementine Benson
* Olin Howland as Sunrise
* Chick Chandler as "Hack" Hackett
* Joe Strauch Jr. as Tadpole
* Forrest Taylor as Pop Harrison
* James Seay as Tex Harrison
* George Douglas as Luigi Scalese aka Crowley Charles Lane as Newspaper Editor
* Hal Price as Sheriff
* Spade Cooley as First Fiddle Player (uncredited)
* Tom Hanlon as Radio Announcer (uncredited)
* Rex Lease as Poker Player (uncredited)
* Cactus Mack as Cowhand, Rodeo Rider (uncredited)
* Frankie Marvin as Rodeo Hand, Musician (uncredited)
* Kermit Maynard as Sam Hatcher (uncredited)
* James McNamara as Bartlett (uncredited)
* Champion as Genes Horse (uncredited)    

==Production==

===Casting===
Between 1941 and 1942, Fay McKenzie appeared as the leading lady in five Gene Autry films: Down Mexico Way (1941), Sierra Sue (1941), Cowboy Serenade (1942), Heart of the Rio Grande (1942), and Home in Wyomin (1942). Magers 2007, pp. 198–199.  She would later remember Autry with fondness, "Gene was a bright and marvelous man, a joy and inspiration to work with. It was wonderful for the whole family to be able to go and enjoy his films. It was such a privilege to work with Gene. It was the high point for me; a happy time."   
 Melody Ranch CBS radio program. 

===Filming and budget===
Home in Wyomin was filmed February 25 to March 14, 1942. The film had an operating budget of $84,006 (equal to $ }} today), and a negative cost of $85,024. 

===Filming locations===
* Russell Ranch, Westlake Village, California, USA
* Agoura Ranch, Agoura, California, USA    

===Stuntwork===
* Ken Cooper
* Bud Geary
* Ted Mapes
* Kermit Maynard
* Joe Yrigoyen (Gene Autrys stunt double)  

===Soundtrack===
* "Be Honest with Me" (Gene Autry, Fred Rose) by Gene Autry
* "Any Bonds Today" (Irving Berlin) by Gene Autry
* "Back in the Saddle" (Gene Autry, Ray Whitley) by Gene Autry
* "Tweedle OTwill" (Gene Autry, Fred Rose) by Gene Autry
* "Modern Design" (Smiley Burnette) by Smiley Burnette
* "Im Thinking Tonight of My Blue Eyes" (A.P. Carter, Don Marcotte) by Gene Autry
* "Twilight in Old Wyomin" (Fred Rose, Johnny Marvin) by Gene Autry
* "(Oh My Darling) Clementine" (Percy Montrose, H.S. Thompson, arr. Tex Ritter) by Gene Autry Magers 2007, pp. 210–211.    

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 