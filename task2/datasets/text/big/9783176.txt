Scared Sacred
{{Infobox Film
| name           =  Scared Sacred
| image          =  
| image_size     = 
| caption        =  
| director       =  Velcrow Ripper
| producer       =   
| writer         =   
| narrator       =   
| starring       =   
| music          =   
| cinematography =   
| editing        =   
| studio         =   
| distributor    =   
| released       = September 14, 2004 (Canada) 
| runtime        =   
| country        =  
| language       =   
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Scared Sacred is an independent film produced in 2004 and released in 2006 by director Velcrow Ripper.

Scared Sacred is an award winning feature length documentary that takes viewers to many of the places in the world that have experienced great suffering in recent years including Bhopal, Hiroshima, Israel and Palestine. The film portrays Rippers own search for meaning, and communicates stories of hope in spite of oppression.

Co-produced by the National Film Board of Canada and Producers on Davie Pictures, Scared Sacred received a Genie Award for Best Documentary.   
 Fierce Light builds on where Scared Sacred ends. Fierce Light is about spiritual activists.

==References==
 

==External links==
*  
*  
*  
*  , an interview with ascent magazine.
*  (Video interview from  )

 
 
 
 
 
 
 
 
 
 