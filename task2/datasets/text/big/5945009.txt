Aatank Hi Aatank
{{Infobox Film
| name               = Aatank Hi Aatank
| image              = Aatank hi aatank.jpg
| image_size         = 
| caption            =
| director           = Dilip Shankar
| producer           = Xavier Marquis  (Presenter)  Mangal
| writer             = Dilip shankar
| based on       =  
| starring           = Rajinikanth Aamir Khan Juhi Chawla
| music              = Bappi Lahiri
| cinematography     = Ashok Gunjal
| editing            = Dilip Shankar
| studio             = Suyash Films Pvt.Ltd
| distributor        = Goldmines Media
| released           = 4 August 1995
| runtime            =
| country            = India
| language           = Hindi
}}

Aatank Hi Aatank ( ;English:Terror everywhere) is a 1995 Indian crime film was written, edited and directed by Dilip Shankar. The movie is highly inspired by The Godfather. It stars  Rajinikanth, Aamir Khan, Juhi Chawla and Pooja Bedi in the lead.
In 2000, the film was dubbed into Tamil as Aandavan.

==Plot==
Shiv Charan Sharma, a farmer, moves to the city to make a life with his son Rohan, daughter Radha Seth and his wife. He meets Munna, an orphan. Shiv and Munna work hard in the underbelly of the city outside the law and go on to lead a syndicate of gangsters. Years pass by and Shiv Charan Sharma is shown to have become an untouchable ganglord. Aslam Pathan and Billa Singh Thakur, rival crime bosses, try to kill Shiv in hopes of overtaking his territory and get rid of the opposition he was proving to be in their plans to increase drug traffic within the city.
Munna meanwhile falls in love with Razia, who is the daughter of Aslam Pathan. She elopes with Munna and gets married. Aslam Pathan attempts to get back at the father of the groom, by sending Gogia Advani to Shiv Charan Sharma with a drug proposition as he thinks that Shivs acceptance of Gogias offer would create dissent amongst the crime cirlces. Shiv Charan Sharma refuses, but Munna seems interested. 
Shiv Charan Sharma gets shot by goons hired by Pathan and Thakur. They think that Munna will follow up on the drug deal if the father is out of the picture. The father survives though. At this point Rohan enters the picture with girlfriend Neha.He has kept away from the family business till this point. Rohan then avenges his fathers shooting by taking out Gogia Advani with Munnas help. Following the shooting, Rohan is on the run where he meets Ganga whom he falls for too. Four years later, Rohan becomes the crime boss. In the end, Sharad Joshi takes a killing contact from Aslam Pathan and Billa Singh Thakur to kill Shiv Charan Sharma and Munna. It is to be seen how Rohan protects his brother and his father.

==Production==
•AATANK HI AATANK (‘95) was produced by director Dilip Shankar’s wife, Mangal. Initially, they had thought of naming it ‘Aatank’, and had planned to cast Shah Rukh Khan and Rajinikanth in the lead roles. However, their dates were not matching, and finally, Dilip and Mangal Shankar approached Aamir Khan, for Shah Rukh Khan’s role. Fortunately, for them, Aamir agreed to do the film. Incidentally, this is the only film, in which Aamir Khan and Rajinikanth have been seen in, together. 

•For a sequence in the song ‘Aakha Hai Bombai’, Aamir Khan was to play the ‘dafli’. While the unit broke for lunch, Aamir Khan walked up to the choreographer, and requested the choreographer to let him practice playing the ‘dafli’, to get the correct beat, and not look uncomfortable onscreen. He skipped lunch, and practiced through the lunch break, and finally, when the sequence was shot, he gave a perfect shot 

•To get a perfect look in the film, Aamir Khan bought himself a new wig and was very excited to wear it. Director Dilip Shankar was not very convinced about the look, but he did not want to disappoint Aamir, or dampen his spirits and, hence, allowed him to wear the wig in the film. Dilip Shankar says that, till date, he regrets allowing Aamir to wear the wig, and is convinced that he would have looked better without it 

•Actor Rajinikanth and director Dilip Shankar became good friends while shooting for this film. Once, when they couldn’t get a good car, Dilip’s assistant sent an old, run-down car to pick up Rajinikanth. He traveled without any complains, despite the car breaking down once. Another time, Dilip Shankar told Rajinikanth at 4 pm, that his shots were over for the day, but Rajinikanth did not leave, and waited for almost 5 hours, helping Dilip Shankar finish the shoot, so that they could go and have dinner together 

•Aamir Khan is known for his honesty and professionalism. Once, director Dilip Shankar had requested Aamir to come in for a 7 am shift. Unfortunately, by the time Aamir reached the sets, it was well past 9 am. Dilip Shankar expected Aamir to throw starry tantrums, when asked for the reason of delay, but Dilip was surprised, when Aamir apologized profusely, saying he had overslept 

•Director Dilip Shankar, who is also the screenplay and storywriter of this film, had conceived a scene of a conflict between Om Puri and Aamir Khan. However, when he discussed the same with the actors, Aamir thought, that the scene did not make sense in the film, and that they should not shoot it. Dilip Shankar thought otherwise, and shot the scene. While editing that scene, he realized that Aamir was right, and finally, decided not to keep the scene in the film. 

•Director Dilip Shankar missed a near fatal accident, while shooting for AATANK HI AATANK (‘95). Once, while shooting on the banks of river Chenab, Dilip Shankar was so engrossed in the shot that he did not look where he was going. He almost tripped and could have fallen into an uncovered deep well, had it not been for the presence of mind of his fight director, who saved him, in the nick of time. 

==References==
 

==External links==
* 

 
 