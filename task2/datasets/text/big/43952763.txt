Parasparam
{{Infobox film
| name           = Parasparam
| image          =
| caption        =
| director       = Shajiyem
| producer       = Jose Brothers
| writer         = Shajiyem
| screenplay     = VR Gopinath, Shajiyem
| music          = MB Sreenivasan
| released       =  
| cinematography = Vipin Mohan
| editing        = G Murali
| studio         = Jose Brothers Films
| lyrics         =
| distributor    =
| starring       = Venu Nagavally, Zarina Wahab
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by Shajiyem and produced by Jose Brothers.  The film stars Venu Nagavally, Zarina Wahab in lead roles. 
  The film had musical score by MB Sreenivasan.  

==Cast==
 
* Venu Nagavally as Vishwanathan
* Zarina Wahab as Meera
* Nedumudi Venu as Jagadish
* Sankaradi as Appachan
* Jagathy Sreekumar Kunchan as Sudheer Kumar
* Rajkumar Sethupathi as Issac
* Kanakalatha
* Sukumari
* Nanditha Bose
* Sunanda
* Jagannatha Varma
* EA Rajendran
 

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anantha neela vinnil || K. J. Yesudas, Chorus || ONV Kurup ||
|-
| 2 || Kilivaathilinarikil || K. J. Yesudas, S Janaki || ONV Kurup ||
|-
| 3 || Nirangal than nritham || S Janaki || ONV Kurup ||
|-
| 4 || Nirangal than nritham   || S Janaki || ONV Kurup ||
|}

==References==
 

==External links==
*  

 
 
 


 