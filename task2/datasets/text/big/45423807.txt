Joan the Maiden
{{Infobox film name           = Joan the Maiden image          = caption        = director       = Jacques Rivette producer       = writer         = Scenario:Pascal Bonitzer Christine Laurent Jacques Rivette starring       = Sandrine Bonnaire Tatiana Moukhine Jean-Marie Richier Baptiste Roussillon music          = Jordi Savall cinematography = William Lubtchansky editing        = Nicole Lubtchansky distributor    = released       = 9 February 1994 runtime        = Part 1: 160 minutes Part 2: 176 minutes country        = France language       = French budget         =
}}

Joan the Maiden( ) is a two-part 1994 French film directed by Jacques Rivette. It chronicles the Life of Joan of Arc and its two parts are Joan the Maiden, Part 1: The Battles and Joan the Maiden, Part 2: The Prisons.

==Cast==
*Sandrine Bonnaire as Jeanne dArc 
*Tatiana Moukhine as Isabelle Romée 
*Jean-Marie Richier as Durand Laxart 
*Baptiste Roussillon as Baudricourt 
*Jean-Luc Petit as Henri Le Royer 
*Bernadette Giraud as Catherine Le Royer 
*Jean-Claude Jay as Jacques Alain 
*Olivier Cruveiller as Jean de Metz 
*Benjamin Rataud as Bertrand de Poulengy
*André Marcon as Charles, Dauphin de France
*Jean-Louis Richard as La Trémoille
*Marcel Bozonnet as Regnault de Chartres
*Patrick Le Mauff as Jean Bâtard dOrléans
*Didier Sauvegrain as Raoul de Gaucourt
*Jean-Pierre Lorit as Jean dAlençon
*Bruno Wolkowitch as Gilles de Laval
*Hélène de Fougerolles as Jeanne de Bar

==References==
 

==Further reading==
*  

== External links ==
*  
*  

 

 
 
 
 
 
 

 