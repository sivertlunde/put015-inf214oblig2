Love at First Sight (film)
 
{{Infobox Film
| name           = Love at first sight
| image          = 
| image_size     = 
| caption        = 
| director       = Rezo Esadze
| producer       = 
| writer         = Bella Abramova, screenplay by Eduard Topol, lyrics by Bulat Okudzhava
| narrator       = 
| starring       = 
| music          = Yakov Bobohidze
| cinematography = Yuri Vorontsov
| editing        = 
| distributor    = Lenfilm, Gruziafilm
| released       = 1977
| runtime        = 90 minutes
| country        = Soviet Union
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1977 Georgia Soviet comedy film by Lenfilm and Kartuli Pilmi (Romance/Drama).

== Plot summary ==
Film narrated about the first love of Murad Rasulov, an Azerbaijani ninth-former and a passionate football fan. Hes in love with a girl two years older than him. This seemingly insignificant circumstance together with the girls family tradition became a serious but brief obstacle for newlyweds.

The directors cut of the film released in 1988.

== Cast ==
* Vakhtang Panchulidze as Murad
* Natalya Yurizditskaya as Anya
* Ramaz Chkhikvadze	as Murads father
* Kakhi Kavsadze as Murads uncle
* Salome Kancheli as Murads mother

==External links==
* 

 
 
 
 
 
 
 
 


 
 