Man in the River
{{Infobox film
| name = Man in the River
| image =
| image_size =
| caption =
| director = Eugen York
| producer = Artur Brauner   Helmut Ungerland 
| writer =  Siegfried Lenz (novel)   Jochen Huth  
| narrator =
| starring = Hans Albers   Gina Albert   Helmut Schmid   Jochen Brockmann
| music = Hans-Martin Majewski 
| cinematography = Ekkehard Kyrath 
| editing =  Ira Oberberg    
| studio = CCC Films
| distributor = Europa Film
| released =15 August 1958
| runtime = 95 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Man in the River (German:Der Mann im Strom) is a 1958 West German crime film directed by Eugen York and starring Hans Albers, Gina Albert and Helmut Schmid.  It was one of the final appearances of the veteran star Albers.

==Cast==
* Hans Albers as Paul Hinrichs  
* Gina Albert as Lena Hinrichs  
* Helmut Schmid as Manfred Thelen  
* Jochen Brockmann as Kuddel   Hans Nielsen as Egon Iversen   Roland Kaiser as Timm Hinrichs  
* Carsta Löck as Sekretärin 
* Josef Dahmen as Bergungsinspektor Garms  
* Wolfgang Völz as Mike  
* Ludwig Linkmann as Herr Buchmann 
* Joseph Offenbach as Friseur 
* Joachim Rathmann as Albert 
* Maly Delschaft

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 
 
 
 
 
 
 


 