The Story of PuPu
 
{{Infobox Film
| name           = The Story of PuPu
| image          = The Story of PuPu ad card.jpg
| image_size     = 
| caption        = Promotional card
| director       = Kensaku Watanabe
| producer       = Masakazu Takei
| writer         = Kensaku Watanabe
| narrator       = 
| starring       = Sakura Uehara Reiko Matsuo Seijun Suzuki Yoshio Harada
| music          = Jun Miyake
| cinematography = Naoto Muraishi
| editing        = Shinji Miura
| distributor    = Little More Co., Ltd
| released       = April 4, 1998
| runtime        = 73 min
| country        = Japan Japanese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1998 Cinema Japanese film anarchistic girls on a road trip to visit the grave of a piglet named PuPu. It is Watanabes directorial debut. Cult film director Seijun Suzuki appears in a small role. {{cite web
| title = The History of PuPu
| url = http://www2.uol.com.br/mostra/23/english/filmes/103.htm
| publisher = São Paulo International Film Festival
| year = 1999
| accessdate = 2007-08-11
}} 

==Cast==
* Sakura Uehara as Suzu
* Jun Kunimura as Kijima
* Tatsushi Omori as Gesuo
* Reiko Matsuo as Fu
* Yoshio Harada as Joji
* Seijun Suzuki as the old man
* Rei Yamanaka as Trunk Man
* Mitsuaki Tuda as Iruka
* Daizo Sakurai as the golfer

==References==
 

==External links==
*  
*  
*     at the Japanese Movie Database
*    

 
 
 
 
 
 
 


 
 