Sightseers
 
 
{{Infobox film
| name           = Sightseers
| image          = Sightseers film poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Ben Wheatley
| producer       = Edgar Wright Jenny Borgars Katherine Butler Claire Jones Matthew Justice Nira Park Danny Perkins Andrew Starke
| writer         = Alice Lowe Steve Oram Additional Material: Amy Jump
| starring       = Alice Lowe Steve Oram Jim Williams
| cinematography = Laurie Rose
| editing        = Robin Hill Amy Jump Ben Wheatley Big Talk BFI Film Fund Rook Films
| distributor    = StudioCanal UK
| released       =  
| runtime        = 85 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  $2,102,166  
}}
Sightseers is a British black comedy film directed by Ben Wheatley and written by and starring Alice Lowe and Steve Oram, with additional material written by co-editor Amy Jump. 

It is produced by Edgar Wright and Nira Park, among others. The film was selected to be screened in the Directors Fortnight section at the 2012 Cannes Film Festival.  

==Plot==
Chris (Steve Oram) is a caravan fan and aspiring writer who takes his girlfriend Tina (Alice Lowe) on a road trip, much to the chagrin of Tinas mother, who has never forgiven Tina for the death of their dog "Poppy". At their first stop, a tramway museum (filmed at Crich Tramway Village, the home of the National Tramway Museum), Chris confronts a man (Tony Way) who is littering, and the man refuses to pick up his rubbish. When they get back to their car, Chris runs him over and kills him, which upsets the couple but they continue onward on their trip. Chris claims that the death was an accident, but his smirk—visible to the camera but not to Tina—in the immediate aftermath makes it clear it was deliberate.

At a caravan park, Chris manages to beat a couple to a good spot. They later meet the couple, Janice and Ian (Monica Dolan and Jonathan Aris), the latter of whom is asserted to be an accomplished writer by Janice, something that makes Chris jealous. When Ian reveals that he is going for a walk the next morning, Chris follows him, hits him in the head with a rock and pushes him off the cliff. When they are about to leave, Tina spots Ians dog Banjo who bears a striking resemblance to the deceased dog Poppy. They take Banjo and drive off to their next stop, Chris calling the dog Banjo, and Tina calling the dog Poppy.

They go clothes shopping and the store owner informs them of Ians death. Tina, who noticed Chris had bloody hands and was out when Ian is supposed to have died, quickly grows suspicious. When she discovers that Chris has been using Ians camera, he confesses to Ians murder. At a World Heritage park, Banjo defecates on the ground and a tourist (Richard Lumsden) tells Tina to clear up the mess. Chris arrives and encourages Tina to claim that the man tried to rape her. A row ensues, and Chris hits him on the head with a branch. He then smashes the body into a rock, kills him, and makes it look like the man was raped.

While driving, they hear on the radio that police are investigating the mans death. At the caravan park, Chris meets Martin (Richard Glover), a caravan engineer who is testing a mini-caravan that can be attached to the back of a bicycle. While on a romantic date at a restaurant, Tina goes to the bathroom, while Chailey Morris, a bride-to-be, has a hen party a few tables over. When Tina returns, she finds Chris kissing Chailey as part of a bachelorette dare for the bride. Upset, Tina leaves the building and follows Chailey and pushes Chailey over the railing, where her head explodes. While waiting for Tina, Chris witnesses the murder. When Tina gets in the car, Chris says he couldnt kill a woman. That night, Chris has a surreal dream where he chases Tina through a forest. In the forest, he catches up to her, only to discover she is a vampire. Intercut with the dream are quick scenes of them fighting, an owl, them at another tourist site, a group of policemen walking Tina off a bridge, Chris talking to Tinas mother, him writing in a notebook, and a red river.

The next morning, Tina discovers that she has lost a necklace that Chris had given her, possibly when killing Chailey. They are about to go to a tourist attraction, but Chris reveals he is helping Martin make some modifications to his caravan. They have an argument, which ends in Tina driving off. Back at the caravan park, Martin and Chris smoke marijuana and test the caravan. At the museum, Tina writes Chris a letter that says she wanted to tell him something, but she tears the letter apart. Crying, she calls her mother and is about to confess to the murders, when her mother hangs up. Later that night, Tina tries to seduce Chris by talking about the murders, but turns him off.

Chris wakes up to find Tina has left him sleeping in the caravan and is speeding down the highway. He calls her and tells her to pull over. On the brink of insanity, Tina notices a jogger and pulls over, running him over in the process. Chris panics and Tina mocks him by talking over the similarities of the first murder and calling it the first murder "theyve done together". Chris, who was writing a book, calls her a bad influence and that the murders havent been helping his writing process. The couple argues before Chris hides the body in the forest in a location where it could be seen from the road, which Tina proceeds to mock.

While fleeing the scene, they hear on the radio that police have found the body of Chailey Morris and police are looking for people fitting the descriptions of Chris and Tina. They drive to a mountain, where they set up camp and Chris is happy to see Ribblehead Viaduct in the distance, the final destination on their sightseeing holiday.  When a hailstorm makes them go inside the caravan, Chris falls asleep and Tina looks at his notebook. She finds a drawing of her and Chris standing on the viaduct, and realises that Chris has planned on committing suicide from the viaduct with Tina.

A few minutes later, Martin arrives on his bike with his mini-caravan and Banjo. While Chris is outside, she tells Martin that Chris is manipulative and domineering. She tries to seduce him, but when they are interrupted by Chriss return, she tells Chris that Martin propositioned her in a particularly implausible and repulsive manner. Chris accepts that if the allegations are true, as an intellectual exercise, it would be grounds to murder Martin, but he communicates this in a sufficiently ambiguous way that Martin is unaware of the real topic of discussion, and Martin might even think that Chris is saying that he has no problem with Martins supposed behavior. Chris agrees with Martin that Martin should go back to his mini-caravan, and after Martin has gone, Chris and Tina have a fight over whether the dog should be called by the name "Poppy" or "Banjo". Upset, Tina pushes Martins mini-caravan off the cliff, with him still in it. She re-enters their caravan and calmly starts knitting. She says the problem is over and Chris runs outside, only to find Martins dead body. He insults Tina and they fight, which ends in them having sex.

Chris sets the caravan on fire and he passionately kisses Tina as they watch the caravan burn in the distance. They run to the Ribblehead Viaduct with Banjo. They go to the top of the viaduct and climb onto the ledge, holding hands. Chris asks Tina if she enjoyed the holiday and she says it was brilliant. He apologises for insulting her and asks if she really wants to kill herself. Tina lets Banjo off her leash and she runs off. Just as Chris steps off the viaduct, Tina lets go of his hand, watching as he falls to the ground and dies. Tina stares at her hand as the screen cuts to black.

==Cast==
*Steve Oram as Chris
*Alice Lowe as Tina

==Production==
The characters came together seven years before the film came out as Lowe and Oram swapped stories based on their common background and childhood holiday experiences. However, the pitch kept getting turned down for being too dark, so they put it online and Lowe sent the link to Edgar Wright, with whom she had worked on Hot Fuzz. Wright greenlit the project, so Lowe and Oram did more research and took a caravanning holiday to the locations that would go on to be featured in the film.    Ben Wheatley has said that all the locations were very helpful, even after they explained the nature of the film, because they "tried to make sure that it was open and fair to places, and that they weren’t the butt of jokes."   

The two were also inspired by Withnail and I. 

==Reception==
The critical reception has been positive, with review aggregator Rotten Tomatoes giving it a rating of 85% based on 94 reviews. 
 Caravan Magazine for his opinion and he thought the film, which he described as "absolutely brilliant", accurately captured the details of caravanning holidays.   

However, the praise wasnt unanimous. The Financial Times   Nigel Andrews conclusion was "There are a few laughs; a few wise nods. But before the end fatigue arrives and doesn’t go away."    

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 