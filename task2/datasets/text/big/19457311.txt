Ouaga-Saga
{{Infobox film
| name = Ouaga-Saga
| image = Ouaga-Saga.jpg
| image_size = 
| caption =  
| director = Dani Kouyaté 
| producer = Papa Kouyate, Sahelis Productions
| writer = Jean-Denis Berembaum Michel Mifsud
| narrator = 
| starring = Amidou Bonsa 
| music = Moktar Samba
| release = 
| cinematography = 
| editing = Jean Daniel Fernandez 2004
| runtime = 86 minutes
| country = Burkina Faso
| language = French
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
| distributor = 
}} 
  2004 comedy film by Burkina Faso-based filmmaker Dani Kouyaté. This film was one of the two to three a year that the Burkina Faso government produces. Its plot revolves around ten children striving to reach their goal in life.

==Cast==
*Amidou Bonsa ...  Bourémiah 
*Sébastien Belém ...  Bouba 
*Aguibou Sanou ...  Moussa 
*Tom Ouedraogo ...  Kadou 
*José Sorgho ...  Cyrille 
*Yacouba Dembélé ...  Le Shérif 
*Gérôme Kaboré ...  Pelé 
*Delphine Ouattara ...  Mama 
*Yasminh Sidibé ...  Faustine 
*Patricia MBaidélé ...  Awa

==Plot==
Each one of the ten children has a goal, although their poverty is in the way of them realising their dreams. In order to make some money, if dishonestly, they steal a bicycle from a wealthy woman and sell it. They divide the money amongst themselves at their compound where they live with Bourémah, an older friend.

Shortly thereafter, a neighbour becomes suspicious of the friends and begins to eavesdrop. The children, discovering this, malign him so horribly that he notifies the police. Bourémah realises that the police will find the stolen money if they search the compound, so he places it in a bag and leaves. His friends thinks he is stealing this from them instead of trying to help.

Meanwhile, Bourémah leaves for another town and stakes out in a hotel. He decides to play the lottery with what money he has left. Winning the jackpot, he comes back to Ouaga to help his friends fulfil their goals.

==Production==
This film was one of the two to three a year that the Burkina Faso government produces. It cost an estimated US$1.2 million.  Dani Kouyaté, the director, had five weeks to film this, though it took longer to complete.   

==Release==
Ouaga premiered at the Panafrican Film and Television Festival of Ouagadougou in 2005.  The New York Times praised it, calling it "the perfect medicine in todays hectic life" due to its offering "the viewer many light hearted moments."   

==References==
 

== External links ==
*  

 
 
 
 
 