Nivedyam (1978 film)
{{Infobox film
| name = Nivedyam
| image =Nivedyam1978.jpg
| caption =
| director = J. Sasikumar
| producer =
| writer = Joseph Anand S. L. Puram Sadanandan (dialogues)
| screenplay = S. L. Puram Sadanandan
| starring = Prem Nazir Adoor Bhasi Sankaradi Sreelatha Namboothiri
| music = G. Devarajan
| cinematography = M C Sekhar
| editing = K Sankunni
| studio = Make-up Movies
| distributor = Make-up Movies
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed J. Sasikumar. The film stars Prem Nazir, Adoor Bhasi, Sankaradi and Sreelatha Namboothiri in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir 
*Adoor Bhasi 
*Sankaradi 
*Sreelatha Namboothiri 
*Unnimary 
*K. P. Ummer 
*K. R. Vijaya 
*MG Soman

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi, Mankombu Gopalakrishnan, Yusufali Kechery and Chirayinkeezhu Ramakrishnan Nair. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ammathan || P. Madhuri || Sreekumaran Thampi || 
|- 
| 2 || Kavilathenikkoru || P Jayachandran, Vani Jairam || Mankombu Gopalakrishnan || 
|- 
| 3 || Mini Skirtkaari || P Jayachandran || Yusufali Kechery || 
|- 
| 4 || Paadasaram Aniyunna || K. J. Yesudas, P. Madhuri || Chirayinkeezhu Ramakrishnan Nair || 
|}

==References==
 

==External links==
*  

 
 
 


 