Jogi (film)
:This article is about the Kannada film. For the caste, please see Jogi (castes).
{{Infobox film
| name = Jogi
| image = Jogi.jpg
| writer = Prem
| starring = Shivarajkumar Jennifer Kotwal Arundathi Nag Prem
| producer = Ramprasad
| released =  
| runtime = 137 minutes
| language = Kannada
| music = Gurukiran
| awards =
| budget =  4 crores 
| gross =  22 crores   
}}

Jogi  is a 2005 Kannada film with Shivrajkumar and Jennifer Kotwal in the lead roles; it was directed by Prem (film director)|Prem, who had previously directed Kariya.  Jogi was released on 19 August 2005. Record box office collection was reported across Karnataka and the movie completed 100 days in more than 61 theaters.   The film was noted for its technical prowess, colloquial Kannada dialogue and lyrics. Its story revolves around an old lady who comes to the city from her village in search of her presumably lost son.
It was remade in Telugu as Yogi (2007 film)|Yogi starring Prabhas and Nayantara.

Gurukiran scored the music, while director Prem penned lyrics for all the songs. The audio, released by Ashwini Audio, was a critical and commercial success. 

==Production==
With a budget of  4 crores, Shivrajkumars film Jogi started filming in 2004. The team chalked out a 100-day shooting schedule in Bangalore, Mysore and Rajasthan. Directed by Prem (film director)|Prem, Jogi was the home production of Ramprasad, produced under Ashwini Productions. The movie marked the debut of Mumbai actresses Jennifer Kotwal and Yana Gupta in Kannada-language films. 

== Plot ==
The film is narrated in a non-linear fashion, with the use of several multiple flashbacks. In the opening scene, a dreaded underworld don is brutally hacked to death by unknown assailants. The police arrive at the crime scene and arrest the unknown assailant revealed as the superstar hero of the movie. The murder is revealed to be the handiwork of a novice, Madesha alias Jogi, played by Kannada superstar Shivarajkumar who works in a roadside tea stall.

In the meantime, news about the murder reaches the echelons of the underworld. A rival gang of the slain don bails out Madesha from the lock-up and requests him to be their associate. Madesha rejects their offer and returns to his tea stall. An inquisitive trainee journalist Nivedhitha (Jennifer Kotwal) is eager to know about Madesha and requests an appointment with him. Despite trailing Madesha for several days, she is unable to interview him. Subsequently, she meets an old woman (Arundathi Nag) from a remote village, who has come in search of her lost son. After listening to her story, Nivedhitha assures her that she will locate her son.

In a flashback, it is revealed that the old ladys son is none other than Madesha. Madesha originally hailed from a village called Singanallur, Kollegal Taluk, South Karnataka and lived with his parents. His father (Ramesh Bhat) earned his living as a Jogi - a wandering minstrel, who went from one household to another and collected alms in return for singing. Due to the strenuous workload, he falls ill and dies. Enticed by his childhood friend, who flaunts his success, Madesha decides to try his luck in the city. However, in the city, he gets tangled with anti-social elements and ends up in jail. His anxious mother comes to the city in search of him.

Jogi is misled and convinced by underworld dons that if he takes up arms, it would be easy for him to find his mother. His mother dies before even seeing her son. Jogi, who does not know this, dances in front of her dead body. When he discovers that he performed the last rites of his mother without realising it, he is completely broken. On the other hand, the underworld men (who are introduced during the course of the movie) and who wanted to hack Jogi to death, also drop arms saying, "Jogi took up arms for his mother, for what reason are we fighting?". The movie thus ends with a message of peace and love for all.

==Cast==
* Shivarajkumar - Madesha alias Jogi
* Jennifer Kotwal - Nivedhitha
* Arundhati Nag - Bhagyakka
* Ramesh Bhat
* Kishan Shrikanth
* Adhi Lokesh - Bidda
* Yana Gupta - Truck Dancer
* Gururaj Hoskote

==Soundtrack==
{{Infobox album  
| Name        = Jogi
| Type        = Soundtrack
| Artist      = Gurukiran
| Cover       = Jogi-audio.jpg
| Alt         = Jogi audio release poster
| Released    =  
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Ashwini Audio
}}

The lyrics were penned by  Prem (film director)|Prem.
{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| Track # || Song || Singer(s)
|- SPB
|-
| 2 || "Bin Ladennu Nan Maava" || Sonu Kakkar, Gurukiran
|- Prem
|-
| 4 || "Chikubuku Railu" || Sunidhi Chauhan, Hariharan (singer)|Hariharan, B. Jayashree
|-
| 5 || "Hodi Maga" || Gurukiran, Gururaj Hoskote, Prem, Vijay Yesudas
|-
| 6 || "Ello Jogappa"|| Sunitha, Shankar Mahadevan
|}

==Reception and critical reaction==
Jogi generated tremendous hype, partly due to Shivrajkumars peculiar hairdo, the music, and the success of director Prems previous ventures (Kariya and Excuse Me).  Nowrunning.com praised the film for its authentic portrayal of the underworld and praised Shivrajkumar and Arundhati Nags acting. Rediff.com felt that the movie justified its hype, but could have been trimmed further.   Jogi was remade in Tamil as Parattai Engira Azhagu Sundaram, starring Dhanush and Meera Jasmin,  and in Telugu as Yogi (2007 film)|Yogi, starring Prabhas and Nayanthara.

After Jogis huge success, director Prem under his home banner Prem Pictures has started to film a sequel for Jogi in 2010. The sequel has been named Jogayya. 

==Box office earnings==
The film was the biggest hit in Kannada film industry. It collected more than  22 crores. 

==Awards==
* ETV Film Awards – Jogi received 11 awards at the maiden film awards
* State Awards - Best Actor - Shivarajkumar

==References==
 

==External links==
* http://in.rediff.com/movies/2006/jan/20kan.htm
* http://www.rediff.com/movies/2005/aug/22jogi.htm
* http://specials.rediff.com/movies/2006/sep/07sd1.htm

 
 
 
 
 
 
 
 