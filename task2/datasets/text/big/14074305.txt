Love, Loot and Crash
 
{{Infobox film
| name           = Love, Loot and Crash
| image          =
| caption        =
| director       = Mack Sennett
| producer       = Mack Sennett
| writer         =
| starring       = Charley Chase
| music          =
| cinematography =
| editing        =
| distributor    = Mutual Film Corporation
| released       =  
| runtime        = 11 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film. It features Harold Lloyd in an uncredited role.   

==Plot==
Dora and her father are lost in the kitchen (they have just fired their cook).  An ad for new one in the newspaper attracts two crooks (one of which is Fritz Schade).  He dresses like a woman to apply for the job.  At his first opportunity he plans to loot the house, but just then, a cop on the beat stops in for coffee. Fritz locks the cop in the basement, picks up what things of value he can and escapes.  He and his pal drive off in a Model T.  Along the way Dora is kidnapped, the Keystone Cops give chase and all ends well in the end.

==Cast==
* Charley Chase as Harold, Doras Suitor Dora Rodgers as Mary, The Bankers Daughter
* Josef Swickard as Marys Father, a Banker
* Nick Cogley
* William Hauber as (as W.C. Hauber)
* Fritz Schade as Plump crook
* Joseph Sweet as Crook
* Harold Lloyd as Italian Fruit Vendor (uncredited)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 