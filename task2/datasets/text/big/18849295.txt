Dhaasippen
 
 
{{Infobox film
| name = Dhaasippen தாசிப்பெண்
| image =
| director = Ellis R. Dungan
| writer =  Pammal Sambandha Mudaliar
| starring = T. R. Mahalingam (actor)|T. R. Mahalingam M. G. Ramachandran N. S. Krishnan T. A. Madhuram
| producer =  Bhuvaneshwari Pictures
| music = Lalitha Venkatraman S. Rajeswara Rao
| distributor =
| released = 3 March 1943
| runtime = 150 min. (13,623 Feet) Tamil
| budget =
}}

Dhaasippen also spelled as Daasippenn ( ) is a 1943 Tamil film directed by Ellis R. Dungan and starring T. R. Mahalingam (actor)|T. R. Mahalingam and M. G. Ramachandran. The film was also had an alternate title - Jothi Malar ( ).         

==Production==
Dhaasippen (lit. Dancing Girl or Prostitute) was based on a play of the same name written by Pammal Sambandha Mudaliar.  It was produced by Buvaneshwari Pictures and directed by Ellis Dungan. T. R. Mahalingam played the lead role and M. G. Ramachandran (later Chief Minister of Tamil Nadu) was cast in a supporting role. Comic relief was provided by the husband and wife comedy duo of N. S. Krishnan- T. A. Madhuram. This film was short (13,623 feet) when compared to the Tamil films of 1930s due to the shortage of film negatives during World War II. It was released on 3 March 1943 in tandem with another film -  Kizhattu Mappilai ( ). 

==Cast and crew==
*T. R. Mahalingam (actor)|T. R. Mahalingam
*M. G. Ramachandran
*Balasaraswathi
*M. R. Santhanalakshmi
*N. S. Krishnan
*T. A. Mathuram
*Krishnamurthy
*Ellis R. Dungan - Director
*R. S. Mani - Assistant Director
*Lalitha Venkatraman - Music
*S. Rajeswara Rao - Music
*Pammal Sambandha Mudaliar - Story 

==References==
 

==External links==
*  

 
 
 
 