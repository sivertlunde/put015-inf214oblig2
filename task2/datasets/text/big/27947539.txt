Metamorphosis of a Melody
{{Infobox film
| name           = Metamorphosis of a Melody
| image          = 
| caption        = 
| director       = Amos Gitai
| producer       = Gala Dona Dela Rosa Shuki Friedman Amos Gitai Ilan Moscovitch Laurent Truchot
| writer         = 
| starring       = Ronit Elkabetz Samuel Fuller
| music          = Markus Stockhausen Simon Stockhausen
| cinematography = Etai Arbel Amos Gitai Jorge Gurvich
| editing        = Oren Medics Marco Melani
| studio         = Agav Films Capital Studios
| distributor    =  
| released       = (Israel) 1996
| runtime        = 93 min
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}} Israeli drama film by Amos Gitai. The film starring Ronit Elkabetz and Samuel Fuller is based on The Jewish War by Josephus. It is a cinematic realisation of stage-based productions by Gitai.

==Plot== 
The film follows Jewish resistance as they face a series of challenges against the Romans. Chronicling the capture of Jerusalem by the Seleucid ruler Antiochus IV Epiphanes in 164 BC to the fall and destruction of Jerusalem in the First Jewish-Roman War in AD 70. 

==Cast==
*Ronit Elkabetz
*Samuel Fuller as Flavius (narrator)
*Efratia Gitai as Memory Holder
*Masha Itkina as Nightingale
*Jerome Koenig as Emperor Titus
*Enrico Lo Verso as Defender of Masada
*Ilan Mosovitch 
*Shuli Rand as The Cantor
*Hanna Schygulla as Spirit of Exile

== References ==
 

==External links==
* 
 
 
 
 
 
 
 

 