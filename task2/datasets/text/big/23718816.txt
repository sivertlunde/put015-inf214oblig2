First Prize for the Cello
 
{{Infobox film
| name = First Prize for the Cello
| image =
| caption =
| director =
| producer =
| writer =
| starring =
| cinematography =
| editing =
| distributor = Pathé
| released =  
| runtime = 3 minutes
| language = Silent film French (Original intertitles)
| country = France
| budget =
}}

First Prize for the Cello  is a French made comedy Silent film from 1907.

==Plot==
A man walks along with a Cello under his arm. He then sits down on a stool and proceeds to play it, very badly, while the neighbors proceed throw all manner of things at him to make him stop. At one point, a Couch is thrown at him, but neither this nor any other projectile is a successful deterrent. At the end of the film, a girl comes up to him and gives him some flowers. The man then stops playing and walks off, happy that his talents have been recognised.

==See also==
  | Silent comedy | 1907 in film

 
 
 
 


 