Musical Justice
{{Infobox Film
| name           = Musical Justice (1931)
| director       = 
| starring       = Rudy Vallée Mae Questel Victor Young
| distributor    = Paramount Pictures
| released       = 26 December 1931
| runtime        = 11 min 
| country        = USA
| language       = English
}}

Musical Justice (1931 in film|1931) is an Paramount Pictures musical short starring Betty Boop and Rudy Vallée.

== Plot summary ==
Musical Justice stars Rudy Vallée as judge and His Connecticut Yankees as jury presiding over the Court of Musical Justice.  The judge hears three separate cases. 

The final case is the State vs. Betty Boop, in which the judge tells Betty Boop (Mae Questel) that "she has broken every law of music". Boops rendition of "Dont Take My Boop-Oop-A-Doop Away" results in a verdict of not guilty,

== Cast ==
*Rudy Vallée as Judge
*Victor Young as Judicial Bandleader
*Mae Questel as Betty Boop

== Soundtrack ==
*"Dont Take My Boop-Oop-A-Doop Away" music by Sammy Timberg	
:Sung by Mae Questel

== Production background ==
*This is one of only two movies to portray a live-action Betty Boop. The other is a 1932 episode of the Paramount series Hollywood on Parade, in which Bonnie Poe portrays Betty Boop.
*According to a draft of the script, Betty Boop was originally to be played in Musical Justice by Margie Hines.

== External links ==
* 
* 

 
 
 
 
 
 

 