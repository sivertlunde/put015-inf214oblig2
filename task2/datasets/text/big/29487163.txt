Sindhu Samaveli
{{Infobox film
| name = Sindhu Samaveli
| image = 
| caption =  Samy
| producer = Michael Royappan
| writer = Samy
| starring = Amala Paul Harish Kalyan Ghajini Ganja Karuppu
| music = Sundar C Babu
| cinematography = Udhbal V. Nayanaar 
| editing = G. Sasikumar
| studio = Gobal Infotaiment
| distributor = Gobal Infotaiment
| released =  
| runtime = 
| country = India
| language = Tamil
| budget =
| gross =
}}
Sindhu Samaveli is a 2010 Tamil drama film directed by Samy (director)|Samy. The film featured debutants Anaka (Amala Paul), Harish Kalyan and Ghajini in the lead roles. The story revolves around an illicit relationship between a girl and her father-in-law. The film became Samys third controversial film in succession, achieving an "A" certificate upon censor following Uyir and Mirugam. The film released on September 3, 2010 and did poor at the box office. 

==Plot==
Anbu (Harish Kalyan) is a brilliant student in a village school, in Kanyakumari area. His mother teaches in the same school and his dad Veerasami (Ghajini) is a CRPF soldier in Assam. His classmate is Sundari (Amala Paul), who is a failed student and elder to him by three years falls for him. One day, his father gets injured in a militant attack and takes voluntary retirement and comes back to the village. The father dotes on his family, especially his son, but tragedy strikes as his wife dies due to a snake bite.

Both father and son are heartbroken. Anbu decides to fulfil his mother’s dream of becoming a teacher. Around this time, his father and other relatives pressurize him to marry Sundari. The love birds live together for hardly a month, before they are separated as Anbu has to go to the teacher’s training school. Sundari is left in the house to look after his dad, and soon due to certain incidents and circumstances they enter into an illicit relationship. One day Anbu comes back and finds out the bitter truth.

==Cast==
* Amala Paul as Sundari
* Harish Kalyan as Anbu
* Ghajini as Veerasamy
* Ganja Karuppu

==References==
 

== External links ==
*  

 
 
 
 
 
 
 


 