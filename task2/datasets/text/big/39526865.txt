An Even Break
{{Infobox film
| name           = An Even Break
| image          = File:An Even Break poster.jpg
| image size     = 
| caption        = Film poster
| director       = Lambert Hillyer
| producer       = 
| writer         = Lambert Hillyer (scenario) Charles Gunn
| music          = 
| cinematography = John Stuart Stumar
| editing        = 
| studio         = 
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent comedy-drama film written and directed by Lambert Hillyer, and starring Olive Thomas and Charles Gunn. A print of the film is preserved at the Library of Congress. 

==Plot==
As described in a film magazine review,  as children Jimmie Strang (Gunn), Mary (Thompson), and Claire Curtis (Thomas) tell each other what they will be when they grow up, and Jimmie succeeds in becoming a noted inventor. He goes to New York City to secure the manufacture of his machine, which has been ordered by the firm operated by Luther Collins (Burke), which hopes to revive its weakened finances with the invention. Arriving in New York, he is taken in hand by the son David (French) from Harding & Co., which contracts to build the machine. But this firm is avaricious and decides to bankrupt Collins and keep the machine as its own asset. Jimmie is initiated into the gay life of the cabaret. He discovers that the renown dancer Claire, loved by all but won by none, is his childhood friend. He obtains what no one else has accomplished and meets with Claire, and their childhood fondness is renewed. Then Mary arrives, and it turns out that Jimmie had in a moment of haste proposed to her back home. Claire decides she will give Mary a fighting chance for Jimmies love, and takes her in hand and teaches her the city fast life. Mary likes it so much that when Jimmie believes his machine has been taken and his friends back home ruined, she refuses to return home and decides to go on the stage. Claire takes Jimmie back to his home, and in the nick of time saves his machine. When she mentions that Mary will be glad, Jimmie tells her that Mary has turned him down, and said that if he thought he could make Claire care for him, he should try. This results in a happy ending.

==Cast==
* Olive Thomas as Claire Curtis
* Charles Gunn as Jimmie Strang
* Margaret Thompson as Mary
* Darrel Foss as Ralph Harding 
* Charles K. French as David Harding
* J. Frank Burke as Luther Collins
* Louis Durnham as Canning 
* Adolphe Menjou - Bit Part (uncredited)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 