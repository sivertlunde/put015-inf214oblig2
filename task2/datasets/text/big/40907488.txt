Know Your Men
{{infobox film
| title          = Know Your Men
| image          =File:Know Your Men 1921.jpg
| caption        =Film poster
| director       = Charles Giblyn William Fox Paul Sloane]] (aka Paul H. Sloane)
| starring       = Pearl White
| music          =
| cinematography = Joseph Ruttenberg
| editing        =
| distributor    = Fox Film Corporation
| released       = March 13, 1921
| runtime        = 6 reels; 5,315 feet
| country        = United States
| language       = Silent film (English intertitles)
}}
Know Your Men (1921 in film|1921) is an American silent melodrama film produced and distributed by Fox Film Corporation, directed by Charles Giblyn, and starring Pearl White. The film is now considered a lost film. 

==Plot==
As described in a film publication summary,    Ellen (White) is in love with Roy Phelps (Lytell), but marries Warren Schuyler (Clarke), who is a good man, to protect her fathers honor. Roy later returns and rekindles her love, and Ellen goes with him. However, she soon discovers his villainy, and, penitent, returns and is taken back by her husband, whom she now realizes she loves.

==Cast==
*Pearl White - Ellen Schuyler
*Wilfred Lytell - Roy Phelps
*Downing Clarke - Warren Schuyler
*Harry C. Browne - John Barrett
*Estar Banks - Mrs. Barrett
*Byron Douglas - Van Horn
*William Eville - Watson

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 