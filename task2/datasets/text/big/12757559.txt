Humanité
 
{{Infobox Film
| name           = Humanité
| image          = Humanite.jpg
| image_size     = 
| caption        = 
| director       = Bruno Dumont
| producer       = Rachid Bouchareb	& Jean Bréhat
| writer         = Bruno Dumont
| starring       = 
| music          = 
| cinematography = Yves Cape
| editing        = Guy Lecorne
| distributor    = 
| released       = 17 May 1999
| runtime        = 148 min
| country        = France French / English
| budget         = 
}} 1999 film directed by Bruno Dumont. It tells the story of a detective who has lost touch with his emotions, and who investigates the murder of a little girl.

==Plot==
Superintendent Pharaon De Winter (Emmanuel Schotte) investigates the murder of a girl aged 11, who had also been raped. The film starts with her body being examined by De Winter in a remote rural field next to a railway line. The film then focuses on De Winters struggle to deal with the realities of the world. In the weekend that follows, he spends time at a restaurant and at the seaside with his neighbours, Domino (Severine Caneele) and Joseph (Philippe Tullier), both of whom are in a relationship. Joseph is a crude man, as revealed from his disturbing carnal desires, seen from his actions and his use of language, in contrast to Domino, whom De Winter considers as tender and to whom he is attracted.

Domino and her colleagues vote to go on strike at the factory where they work. The police authorities in Lille and Paris are starting to pressure the local police, in particular De Winters boss, to find the murderer. Eventually, after the Lille authorities take over the investigation, Joseph is arrested. Domino is distraught, and De Winter becomes similarly emotionally unstable.

There is only the barest outline of a plot. A girl is murdered, a policeman is assigned to solve the murder. The film is shot in contemplative and symbolical style, which corresponds to De Winters character and his method of investigation.

==Cast==
* Emmanuel Schotté - Pharaon De Winter
* Séverine Caneele - Domino
* Philippe Tullier - Joseph
* Ghislain Ghesquère - Police Chief
* Ginette Allegre - Eliane
* Daniel Leroux - Nurse
* Arnaud Brejon de la Lavergnee - Conservationist
* Daniel Petillon - Jean, the cop
* Robert Bunzi - English cop
* Dominique Pruvost - Angry worker
* Jean-Luc Dumont - CRS
* Diane Gray - British traveller Paul Gray - British traveller
* Sophie Vercamer - Worker
* Murielle Houche - Worker

==Awards==
The film was entered into the 1999 Cannes Film Festival where it won the following awards:    Grand Prix Best Actor (Emmanuel Schotte) Best Actress (Séverine Caneele)

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 