Queer Duck: The Movie
{{Infobox film
| name           = Queer Duck: The Movie
| image          = 
| caption        = 
| director       = Xeth Feinberg
| producer       = Tal Vigderson Mike Mendel Mike Reiss
| writer         = Mike Reiss Billy West Maurice LaMarche Jeff Glenn Bennett Tim Curry Conan OBrien David Duchovny
| music          = Sam Elwitt
| cinematography =
| editing        = Damon P. Yoches
| distributor    = Icebox.com Paramount Pictures
| studio         = Disgracie Films Icebox.com
| released       =   (airdate in Logo)   (DVD)
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
}} Logo in July 16, 2006, then it was released on DVD July 18, 2006. Paramount and Viacom now owns the rights to the film.
 Jeff Glen Bennett as the main antagonist; a homophobic bigoted priest named Reverend Vandergelding, Mark Hamill as a hot dog vendor, Bruce Vilanch as himself, Andy Dick as former drag queen Rex (formerly Regina), Jackie Hoffman as Broadway actress Lola Buzzard, April Winchell doing additional voices and David Duchovny as "Tiny Jesus". 

==Production== Showtime but since the network quit supporting gay shows, the series was dropped and it became a full Icebox production.   The animation has approved a little such as Queer Duck frolicking more smoothly, Openly Gators mouth being shorter and Oscar Wildcat gaining weight, but still had the limited animation style from the webisodes. Also, the theater sign that says "Queer Duck - The Musical" changed to "Queer Duck - The Movie" to support the movie. The theme song has also extended as well. Footage of each actors recording sessions were provided in the credits.

Several past characters such as Queer Ducks family return for the film and just like the original series, several celebrities were parodied and voiced by imitators, with the exceptions of Bruce Villanch and Conan OBrien. Unlike the original series, non-celebrity humans also appear such as the gay baseball players, a hot dog vendor (Mark Hamill) and the Happy Family who own the themepark Happyland, now Fairyland. The film was released to DVD in all the US except Utah. This was their latest Queer Duck and presumably final one since 2001. It was the first Internet animation to be screened during the Polish Festival of Equality 2008 in Warsaw. Creator Mike Reiss considered the film the "best thing he ever wrote". 

==Plot==
The film focuses on Queer Duck who wakes up from a late night party and watches a commercial for an amusement park called Happyland. Declaring it a gay day for the park ("Gay Day at Happyland"), they were closed down by the officials and were told to leave because they were gay. Soaking in bed, he realized that theres no point of being homosexual if almost everyone is against it. During work, he meets the quirky, energetic and suave Broadway actor Lola Buzzard (Jackie Hoffman) which stroke his heart from her sweet and sassyness along with her upbeat attitude ("Smile, Damn You, Smile"). He encourages her to do Broadway acting once more which she does in a play called "Still Alive", which she won an award from Rosie ODonnells show. Because he starts developing a crush on her, he is deciding whenever to stay gay or turn straight. Oscar Wildcat insisted that marrying Lola would be his only chance of a relationship unlike his chance. During his youth, in the 60s, since homosexuality was a crime, gay bars were hidden and disguised and only revealed at times when authorities were gone. When he went to the bar, he meets a drag queen named Rex who calls herself Regina as she sings "Shamalama" with her band, The Blueballs. The two dance and before they kissed, the Stonewall riots occurred and he was separated from Regina and beaten up but he managed to keep his sixties earring as a remembrance becoming a nipple ring. This decision also has an effect on his lover, Openly Gator who wants Queer Duck to accept for who he is without change. Queer Duck asks Openly Gator about this and he states (in hidden emotion) that whatever makes Queer Duck happy will make him happy. Queer Duck decides to marry Lola but needs help turning straight so Lola recommends him to a homophobic bigoted priest named Reverend Vandergelding that can turn him straight. All of the reverends procedures failed and so he creates an elixir that turns him straight. When Queer Duck drinks it, he is muscle bounded, becomes fat, straight and monotonish and marries Lola Buzzard until her unexpected death, leaving him to wish to turn gay again. Openly Gator, still sad from losing his lover, takes his frustrations out on Conan OBrien who keeps eating at the restaurant. He also plans to stop Queer Ducks wedding before he realizes that Queer Duck didnt show up and when he did, he told him to beat it and called him a "homo".

After turning gay by Barbra Streisand, he loses the love of his life; Openly Gator, who states that hes in a relationship with Liza Minnelli thinking that the Liza he got was just an imitator but it turns out to be the real one, but gains respect and independence of homosexuals. The reverend was arrested for kidnapping and intoxicating clients after Queer Duck returned to him as a gay man again in which he threatened him in response. Lola gave all of her fortune to Queer Duck when she died and so, he used it by buying the gay bashing theme park Happyland, giving Bi-Polar Bear a baseball stadium since as a child, he always gets picked last ("Baseball is Gay"), and gave Oscar Wildcat his own antique variety show. Oscar reunites with Regina as she tries to pawn off her earring. Regina has become Rex again and gave up his drag life, being a customer of Reverend Vandergelding. Oscar, realizing that he can stay a homosexual and get the love of his life, shows him her earring he snatched. Since they now recognize each other from when they were young, they no longer have to worry since there both gay and they save sex on national TV. Vandergelding is so irritated with so much references of homosexuality from Oscars live sex routine to the announcement of the worlds first gay theme park, that he escapes prison, kidnaps Queer Duck and vows to pour his big pot of elixir all over Fairy Land (formerly Happyland) to turn all gay people into heterosexuals, but Openly Gator, after hearing that Queer Duck is in trouble when he was assigned as a captain of a ride in the park by his agent, comes to the rescue and stops the Reverend and kicks him out where he is splashed with his own elixir and pink hair is stuck on him, in which a gay bull charges him and kisses him, thinking he was another bull.

Openly Gator and Queer Duck kiss and make up, which Queer Duck states that hes gay to stay, which they end the movie with their last hit number "Im Glad Im Gay".

==Voice cast==
*Jim J. Bullock as Adam Seymour "Queer Duck" Duckstein: A gay duck who works as a nurse and gained the fortune from his ex-wife Lola Buzzard, who died, which he used to make the worlds first gay theme park and made his friends dreams come true.
*Jackie Hoffman as Lola Buzzard: A 78-year old Broadway actress who was full of power and married Queer Duck before her death which was caused from too much toxin in her blood.
* .
* . He works at a perfume stand in a mall.
*Maurice LaMarche as Oscar Wildcat: One of Queer Ducks friends, an alcoholic who reunites with a drag queen named Regina (now Rex) who he loved during his youth. He works at a Shirley Temple antique store. LaMarche also voices Morty Duckstein and others.
*Jeff Glenn Bennett as Reverend Vandergelding: A bigoted homophobe priest that made Rex and Queer Duck straight and was arrested for toxicating his clients.
*Tim Curry as Peccary: Lolas (formerly) and Queer Ducks metrosexual pig butler who loves to drive the limosuine very fast and has an affair with a hot dog vendor.
*Conan OBrien as Himself
*David Duchovny as Tiny Jesus: A miniature model of Jesus Christ in a cross that makes some small lines towards the end of the film.

==Other voices==
*Bruce Vilanch as Himself
*Estelle Harris as Mrs. Duckstein
*Tress MacNeille as Melissa and others
*Andy Dick as Regina/Rex
*Mark Hamill as A hot dog vendor, Police Officer, Owl Doctor

==Additional voices== Billy West
*Kevin Michael Richardson
*Mark Hamill
*Howard Hoffman
*Tress MacNeille
*Barbara Goodson
*April Winchell
*Kevin Chamberlin
*Nick Jameson

==Celebrity appearances==
*Cher (as overturned chairs)
*Stevie Wonder (as a child)
*Stephen Hawking (as a child)
*Shirley Temple (voiced by Tress MacNeille)
*Rupert Everett
*Daniel Day Lewis (mentioned)
*Jay Leno
*Jon Lovitz Robert Blake
*Jennifer Lopez
*Bjork (voiced by Tress MacNeille)
*Joan Rivers (voiced by Debi Mae West)
*Coco Chanel (as a corpse)
*Rosie ODonnell (voiced by Audrey Wasilewski)
*Elaine Stritch
*Bea Arthur
*Gwen Verdon Billy West)
*Stanley Kubrick
*Richard Nixon
*Pat Nixon Chris Cox) Bubbles
*Elizabeth Taylor (voiced by Barbara Goodson Billy West)
*Dr. Laura
*Cameron Diaz (as a photo)
*Camryn Manheim (as a photo)
*Sister Wendy (as a photo)
*The Houston Astros
*Barbra Streisand (voiced by Tress MacNeille)
*Liza Minnelli
*Queer Eye for the Straight Girl cast members
*Emilio Estevez
*Linda Blair
*Liberace
*David Sedaris (as a dwarf)
*Dick Cheney (voiced by Maurice LaMarche)
*Rock Hudson (as an angel)
*Louie Armstrong (voiced by Kevin Michael Richardson)

==References==
 

 
 
 
 
 