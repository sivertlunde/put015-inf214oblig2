Chilakamma Cheppindi
{{Infobox film
| name = Chilakamma Cheppindi
| image =
| caption =
| director = Eranki Sharma
| producer = Chalasani Gopi
| writer = Pamman Sangeetha Sripriya Narayana Rao
| music = M. S. Viswanathan
| editor =
| released = 1977
| runtime = 155 minutes Telugu
| budget =
}}

Chilakamma Cheppindi (  drama film written and directed by Eranki Sharma. The film won the state Nandi Award for Best Feature Film. The film was a remake of Malayalam film Adimakal. The film starred Rajinikanth in his first lead role. The film was later remade into Tamil as Nizhal Nijamagiradhu by K. Balachander with kamal Hassan in the lead role. The film was premiered at the 1978 International Film Festival of India.  

==The plot== Sangeetha and Laxmikanth are siblings. Sripriya is a poor girl who work under Sangeetha. Poor man Narayana Rao likes Sripriya. Rajinikanth, a friend of Laxmikanth comes to the village as a government servant. Sangeetha is a music teacher and hates men. Sripriya gets pregnant before marriage. Sangeetha suspects Rajinikanth for her pregnancy. Narayana Rao supports Sripriya. Who is the real culprit? How Sangeetha and Rajinikanth get together is the rest of the story.

==Cast==
{| class="wikitable"
|-
! Actor
! Character
|-
| Rajinikanth
| Ravi
|- Sangeetha
| Bharati
|-
| Sripriya
| Malli
|}

==Songs==
* Chitti Chitti Chepallara Selayeti Papallara Chialakamma Cheppindoyi Challanimata (Music: M. S. Viswanathan)
* Enduku Neekee Daaparikamu Ennallu Daastavu Daagani Nijamu (Music: M. S. Viswanathan)
* Kurradanukoni Kunukuluteese Verridanikee Pilupu (Music:  )

==References==
 

==External links==
*  

 
 
 
 
 
 


 