Mr. Woodcock
{{Infobox film
| name           = Mr. Woodcock
| image          = Mr woodcock poster.jpg
| caption        = Theatrical release poster
| alt            =  David Dobkin (uncredited) Bob Cooper David Dobkin
| writer         = Michael Carnes Josh Gilbert
| starring       = Seann William Scott Billy Bob Thornton Ethan Suplee Amy Poehler Susan Sarandon Theodore Shapiro
| cinematography = Tami Reiker
| editing        = Al E. Baumgarten
| distributor    = New Line Cinema
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $33 million
}}
 sports comedy film directed by Craig Gillespie, and starring Seann William Scott, Billy Bob Thornton, Susan Sarandon, Amy Poehler, and Ethan Suplee. The film was released on September 14, 2007.

==Plot==
John Farley (Seann William Scott) is a successful self-help author who returns to his hometown in Nebraska to receive an award. Farley arrives at home and learns that his widowed mother Beverly (Susan Sarandon) is dating his former gym teacher Jasper Woodcock (Billy Bob Thornton). Farley disapproves of the relationship because he remembers Woodcock as an abusive bully.

Woodcock and Beverly become engaged and the majority of the film centers on Farleys attempts to convince his mother to break off the relationship. Farley becomes increasingly obsessed with beating Woodcock at various competitions and with proving that Woodcock is not a suitable mate for Beverly. Farleys antics are so over-the-top and childish that his new love interest Tracy (Melissa Sagemiller), a former classmate, refuses to see him again.

Farley is set to receive his award at the same ceremony where Woodcock will be presented with an award for "Educator of the Year." Woodcock receives his award first and is praised by numerous members of the community for being a great teacher and influence on children. Farley is incredulous and devotes his acceptance speech to explaining why Woodcock is the "biggest asshole on the planet." Woodcock and various crowd members refute Farleys points, and Woodcock then challenges Farley to a fight. Beverly witnesses this and dumps Woodcock.

The next day, Farley has a heart-to-heart conversation with his mother, who tells him that he is self-centered and has always sabotaged her relationships with men. Farley realizes she is correct and attempts to apologize to Woodcock. The two have a final cathartic fight, which leads to Woodcock suffering a concussion. Farley and Beverly visit Woodcock in the hospital and all three seemingly make peace. Farley declares that the key to life is not "getting past your past" but instead learning to embrace your past. He opines that Woodcocks rough treatment in gym class helped him become the man he is today.

The film ends with several short scenes cut into the final credits. These scenes reveal that Woodcock and Beverly got married, Farley was reunited with Tracy, and Farley wrote a second book entitled Backbone: The Definite Guide to Self Confidence.

==Cast==
* Billy Bob Thornton as Jasper Woodcock
* Seann William Scott as John Farley
* Susan Sarandon as Beverly Farley
* Ethan Suplee as Nedderman
* Amy Poehler as Maggie Hoffman
* Melissa Sagemiller as Tracy Detweiller
* Bill Macy as Mr. Woodcocks father
* Tyra Banks as Herself
* Kyley Baldridge as Young John Farley
* Alec George as Young Nedderman
* Melissa Leo as Sally Jansen
* Jennifer Aspen as Cindy
* Allisyn Ashley Arm as Scout Girl
* M.C. Gainey as Hal the Barber

==Reception==
  
Mr. Woodcock received generally unfavorable reviews, and currently holds a 13% rotten score, based on 110 reviews, on the review aggregator site Rotten Tomatoes. {{cite web 
|url= http://www.rottentomatoes.com/m/mr_woodcock/ 
|title= Mr. Woodcock 
|accessdate= 2009-11-08 
|publisher= Flixster
| work = Rotten Tomatoes}}  
On Metacritic, the film had an average score of 41%, based on 25 reviews. {{cite web 
|url=http://www.metacritic.com/film/titles/mrwoodcock 
|title=Mr. Woodcock (2007): Reviews |accessdate=2009-11-08
|publisher=CBS 
| work = Metacritic 
}} 

Although the film was panned by most critics, Roger Ebert gave the film 3 out of 4 stars. 

In a 2009 interview on The Opie & Anthony Show, Seann William Scott said that he and Thornton spent time on set discussing how terrible the movie was. Scott said "theres nothing worse than going to a movie set knowing that   could end my career." 

==Home media==
  
The Blu-ray Disc and DVD were released on January 15, 2008. The HD DVD version of the movie was scheduled to be released shortly after the Blu-ray version, but Warner Bros./New Lines decision to exclusively support Blu-ray led to the cancellation of all New Line HD DVD titles (along with all Warner Bros. HD DVD titles after May 2008). 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 