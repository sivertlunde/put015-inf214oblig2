Peut-être toi
{{Infobox single
| Name           = Peut-être toi
| Cover          = Peut-être toi (single).jpg
| Artist         = Mylène Farmer
| Album          = Avant que lombre... release history) 
| B-side         = Instrumental  (CD single)    Remixes  (CD maxi) digital download
| Recorded       = 2005 in music|2005, France house
| Length         = 3:40  (single version)    3:45  (radio edit)    4:55  (album version) 
| Label          = Polydor
| Writer         = Lyrics:  
| Producer       = Laurent Boutonnat
| Last single    = "Lamour nest rien..." (2006)
| This single    = "Peut-être toi" (2006) Slipping Away (Crier la vie)" (2006)
| Misc           = 
{{Extra album cover
| Upper caption  = Alternative cover
| Type=single
| Cover          = Peut-être toi.jpg 
| Lower caption  = CD maxi
}}
{{Extra track listing
| Album          = Avant que lombre...
| Type           = single
| prev_track     = "Jattends"
| prev_no        = 12
| this_track     = "Peut-être toi"
| track_no       = 13
| next_track     = "Et pourtant..."
| next_no        = 14
}}
}}

{{Infobox film
| name            = Peut-etre Toi
| image           = 
| caption         = 
| director        = Naoko Kusumi
| writer          = 
| starring        =
| music           = Mylène Farmer
| producer        =  
| editing         =  
| cinematography  =  
| studio          = Production I.G
| distributor     =  
| released        =    
| runtime         =  7 minutes
| country         = Japan
| language        = 
| budget          =
| gross           =
}}  2005 song French artist single from her sixth studio album, Avant que lombre..., and was released on 21 August 2006. The music video was produced as an animated feature and the lyrics deals with a love relationship. It was a top three hit in France, but its sales were rather modest.

==Background and release==
In May 2005, the fans were disappointed when "Q.I" was chosen as the second single, as "Peut-être toi" was already broadcast on radio and then seemed to be a next single. #Cachin|Cachin, 2006, p. 195.  Later, as of 22 May 2006, a rumor announced "Dans les rues de Londres" as the fifth single from the album.  However, on 16 June 2006, it was officially announced that "Peut-être toi" would be the fifth and last single from Avant que lombre....

The song was broadcast for the first time in its radio edit version on 19 June on  , CD maxi et 12"|12" vinyl.    The first remix was produced by Bionix, who also produced one of them for "Lamour nest rien...", and the two others were made by Chris Cox who also remixed "Désenchantée" and "Q.I". #Rigal|Rigal, 2010, pp. 157-59. 
 2006 Farmers tour as the first song of the concert, and thus it is available on the live album Avant que lombre... à Bercy.

==Lyrics and music== dance and electro sonorities.   The psychologist Hugues Royer believes that in this song, "there is some caution in the declaration  , a decency to say the passion that could be extinguished", which is expressed by the "Shut Up" in the chorus. Love is not seen as being eternal and is only related to the present. It seals the pact of "a requirement of absolute sincerity" in which no error must be committed by one of the lovers, and no routine must appear. #Royer|Royer, 2008, pp. 284-85. 

==Music video==
The music video was produced as an anime by Production I.G, a famous Japanese anime company and was written by Katsuhiro Ôtomo after a strip cartoon by Osamu Tezuka, and was directed by Kusumi Naoko, Kazuchika Kise and Shuichi Hirata.    It was originally intended to illustrate "Fuck Them All",  as the booklet of Music Video IV proves it, indicating that the video was produced in 2005. 

On 7 July 2006, the music video was presented by  , i.e. only death can guarantee the eternity of love.  Major themes of the video are rebellion and love. 
 MCM on 25 February 2007, this video reached number four (behind those of "Fuck Them All" at number one, and "Redonne-moi" at number three) thanks to the voters of the television viewers.  On the Yahoo Music France site, the video was awarded "Best Animated Clip 2006". 

==Promotion and live performances==
"Peut-être toi" was never performed on television, but was the opening song of the 2006 series of concerts. It began with a huge "Shut up!", then a cylindrical glass coffin suspended in the airs in which the singer was locked up appeared, went down to the central cross-shaped stage. Six men dressed in black moved it to the center of the stage and placed it on a pedestal. It bowed upright and opened. While the whole stage was then illuminated in red, Farmer, dressed in light brown, came out of it and began to perform the song, starting with the first verse, instead of the refrain. 

==Chart performances==
The single obtained its highest position in France where it debuted at number three on 26 August, selling a peak of 18,144 units.    The next weeks, it dropped and remained for a total of nine weeks in top 50 and 15 weeks on the chart.    According to the SNEP, "Peut-être toi" was the 97th best-selling singles from the first nine months of 2006. 
 Belgian (Wallonia) Ultratop 40 Single Chart on 2 September, and jumped to a peak of number 12 the week after, then dropped. It fell off the chart after seven weeks.    In Switzerland, "Peut-être toi" peaked at number 34 in the first week, on 3 September. Then, it crumbled very quickly and remained in the top 100 for seven weeks.   

==Formats and track listings==
These are the formats and track listings of single releases of "Peut-être toi":   

 
 
* CD single - Digipack - Limited edition
{{Tracklist
| title1 = Peut-être toi
| note1  = 
| length1= 3:40
| title2 = Peut-être toi
| note2  = instrumental
| length2= 3:46
}}

* CD maxi - Digipack - Limited edition
{{Tracklist
| title1 = Peut-être toi
| note1  = single version
| length1= 3:40
| title2 = Peut-être toi
| note2  = Miss Farmers remix
| length2= 4:10
| title3 = Peut-être toi
| note3  = Coxs remix club mix
| length3= 6:58
| title4 = Peut-être toi
| note4  = full vocal Coxs remix
| length4= 8:46
}}

* CD single - Promo
{{Tracklist
| title1 = Peut-être toi
| note1  = radio edit
| length1= 3:45
| title2 = Peut-être toi
| note2  = Miss Farmers remix
| length2= 4:10
}}

 

* 7" maxi / 7" maxi - Promo
{{Tracklist
| title1 = Peut-être toi
| note1  = Coxs remix - club mix
| length1= 6:58
| title2 = Peut-être toi
| note2  = Miss Farmers remix
| length2= 4:10
| title3 = Peut-être toi
| note3  = single version
| length3= 3:45
}}

* Digital download
{{Tracklist
| title1 = Peut-être toi
| note1  = radio edit
| length1= 3:45
| title2 = Peut-être toi
| note2  = Miss Farmers remix
| length2= 4:10
| title3 = Peut-être toi
| note3  = album version
| length3= 4:45
| title4 = Peut-être toi
| note4  = 2006 live version
| length4= 3:27
| title5 = Peut-être toi
| note5  = video
| length5= 4:04
}}

* DVD - Promo
{{Tracklist
| title1 = Peut-être toi
| note1  = radio edit
| length1= 4:04
}}
 

==Release history==
{| class="wikitable" width="60%" border="1"
! Date  !! Label !! Region !! Format !! Catalog
|-  26 June 2006 Polydor
|rowspan=5 align=center|France, Belgium, Switzerland CD single - Promo 11 233
|- July 2006
|align=center|7" maxi - Promo 11 234
|- 21 August 2006
|align=center|7" maxi  984 206-8
|- CD single 984 206-9
|- CD maxi 984 207-0
|-
|}

==Official versions==
{| class="wikitable sortable" width="100%" border="1"
! Version  !! Length !! Album !! Remixed by !! Year !! Comment  
|- Album version
|align="center"|4:55
|align="center"|Avant que lombre...
|align="center"|— 2005
| See the previous sections 
|- Single version / Radio edit
|align="center"|3:45
|align="center"|— Laurent Boutonnat 2006
|This remixed version is shorter, more dynamic and with additional sonorities (sounds of bells, more synthesizers and drum) than the album version.
|- Instrumental
|align="center"|3:46
|align="center"|— Laurent Boutonnat 2006
|All the lyrics, even the "Shut up" in the vocals, are deleted.
|- Miss Farmers remix
|align="center"|4:10
|align="center"|— The Bionix 2006
|This remix had R&B sonorities and some "Shut the fuck up" repeated throughout the song. 
|-
|Coxs remix - club mix
|align="center"|6:58
|align="center"|— Chris Cox Chris Cox 2006
|
|- Full vocal Coxs remix
|align="center"|8:46
|align="center"|— Chris Cox 2006
|
|- Music video
|align="center"|4:19
|align="center"|Music Videos IV
|align="center"|— 2006
|
|- Live version   (recorded in 2006) 
|align="center"|3:27  (audio)    3:22  (video) 
|align="center"|Avant que lombre... à Bercy
|align="center"|— 2006
| See Avant que lombre... à Bercy (tour) 
|}

==Credits and personnel==
These are the credits and the personnel as they appear on the back of the single:  
 
 
* Mylène Farmer – lyrics
* Laurent Boutonnat –  music
* Requiem Publishing – editions
* Polydor – recording company
* Kazuchika Kise (Production I.G) – illustration
* Henry Neu – design
 
; Full vocal Coxs remix
* Chris Cox – producer
* Keith "KLM" Litman – additional arranging
* Chris Cox at Noho Porn Labs, Los Angeles, CA – all programming, engineering and mixing
 

==Charts and sales==
 
 
{| class="wikitable sortable"
!Chart (2006)
!Peak position
|- Belgian (Wallonia) Singles Chart  12
|- Belgian Singles Chart (global)  18
|- Eurochart Hot 100 13
|- French SNEP Singles Chart  3
|- Swiss Singles Chart  34
|-
|}
 
{| class="wikitable"
!Country
!Certification
!Physical sales
|- France
|align="center"|—
|align="center"|50,000   
|-
|}
 

==References==
 
*   
*   
*   
*   
*   
*   
 

==Notes==
 

==External links==
*     All about the song, on Mylene.net

 
 

 
 
 
 
 
 
 
 
 