Love on Wheels
{{Infobox film
| name           = Love on Wheels
| image          = 
| image_size     = 
| caption        = 
| director       = Victor Saville
| producer       = Michael Balcon Robert Stevenson   Victor Saville
| narrator       = 
| starring       = Jack Hulbert Leonora Corbett Gordon Harker Edmund Gwenn
| music          = Bretton Byrd
| cinematography = Mutz Greenbaum
| editing        = Ian Dalrymple
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service
| released       = 1932
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British musical musical comedy film directed by Victor Saville and starring Jack Hulbert, Leonora Corbett, Gordon Harker and Edmund Gwenn. 

==Synopsis== Green Line bus from the suburbs to Central London Fred Hopkins romantically pursues a fellow passenger Jane with the help of Briggs the bus conductor. His hopes are thwarted when he is fired from his job at a major department store. However he is eventually able to return, securing both his dream job as advertising manager in charge of window dressing and the girl he loves.

==Production==
The film was made at the Islington Studios of Gainsborough Pictures. Gainsborough was part of the larger Gaumont British empire, and specialised in making comedies during the 1930s. Hulbert became one of the studios top start during the early 1930s, often appearing with his wife Cicely Courtneidge.

==Cast==
* Jack Hulbert - Fred Hopkins
* Leonora Corbett - Jane Russell
* Gordon Harker - Briggs
* Edmund Gwenn - Philpotts
* Tony De Lungo - Bronelli
* Percy Parsons - American Crook
* Roland Culver - Salesman
* Miles Malleson - Academy of Music Porter
* Martita Hunt - Piano Demonstrator
* Maria Milza - Mrs Bronelli

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 