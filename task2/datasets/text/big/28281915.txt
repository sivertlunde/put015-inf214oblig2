Kolpaçino
{{Infobox film
| name           = Kolpaçino
| image          = Kolpaçino Theatrical Poster.jpg
| alt            =  
| caption        = Theatrical Poster
| director       = Atil Inac
| producer       = Şenol Zencir Selin Altınel
| writer         = Şafak Sezer Kaan Ertem Suat Özkan
| starring       = Şafak Sezer Aydemir Akbaş Ali Çatalbaş Hakan Ural Ali Sürmeli Eriş Akman Hakan Aysev
| music          = 
| cinematography = Feza Çaldıran
| editing        = Hakan Akol
| studio         = İyi Seyirler Film
| distributor    = Özen Film
| released       =  
| runtime        = 100 minutes
| country        = Turkey
| language       = Turkish
| budget         = $2,000,000
| gross          = $3,002,945
}}
Kolpaçino ( ) is a 2009 Turkish comedy film, directed by Atil Inac, which stars Şafak Sezer as a rich kid who sets up an illegal casino in his parents home to help his friend pay off a mob boss. The film, which went on nationwide general release across Turkey on  , was one of the highest-grossing Turkish films of 2009.   

==Synopsis==
Tayfun is an errand boy working for Ateş, one of the most powerful mafia bosses in İstanbul. Tayfun has always been envious of the life led by the spoilt children of the jet set to whom he sells drugs. One day, with the intent of hitting it big, he steals Ateşs money. However, Ateş finds out about this and threatens Tayfun that he will have to pay for it. Frightened, Tayfun goes to another mafia boss, Sabri, for some idea of how to get the amount he owes to Ateş. Sabri tells him that the best way to collect money is to run an illegal casino. However, all the venues run by Sabri are known by the police, so they need a “clean” place to set up the casino. Özgür, one of the kids Tayfun sells drugs to, is the son of a well-to-do family that lives in a large mansion in an upscale neighborhood. Their house is the last place the police would think of raiding, so Tayfun and Sabri go to Özgürs house to ask for his permission to turn the mansion into a casino. Özgür, for the sake of some adrenaline, agrees to turn his familys mansion into an underground casino while his parents are away on an overseas trip.

==Release==
The film opened in 32 screens across Germany on  , 252 screens across Turkey and 6 screens across Austria  on   at number two in the Turkish box office chart with a worldwide opening weekend gross of US$824,777.   

{| class="wikitable sortable" align="center" style="margin:auto;"
|+ Opening weekend gross
|-
!Date!!Territory!!Screens!!Rank!!Gross
|-
|  
| Turkey
| 252
| 2
| US$628,236 
|-
|  
| Germany
| 32
| 18
| US$168,120
|-
|  
| Austria
| 6
| 14
| US$28,421
|}

==Reception==

===Box Office=== Turkish film of 2009 with a worldwide total gross of US$2,424,173. That figure has subsequently risen to US$3,002,945. 

==References==
 

==External links==
*   for the film (Turkish)
*  
*  
*  

 
 
 
 
 