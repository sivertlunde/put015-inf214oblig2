Puppet Master 4
 
{{Infobox film 
| name           = Puppet Master 4: The Demon
| image          = Puppet Master 4.jpg
| caption        = Film poster
| director       = Jeff Burr
| producer       = Charles Band
| writer         = Charles Band Douglas Aarniokoski Steven E. Carr Jo Duffy Todd Henschell Keith S. Payson David Schmoeller
| starring       = Gordon Currie Chandra West Ash Adams Teresa Hill Felton Perry Stacie Randall Guy Rolfe
| music          = Richard Band
| cinematography = Adolfo Bartoli
| editing        = Mark S. Manos Margaret-Anne Smith Full Moon Entertainment Paramount Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
}}
 first and second films. Originally, Puppet Master 4 was intended to have the subtitle The Demon.

Puppet Master 4, as well as the  , and   installments of the series, were only available in DVD format through a Full Moon Features box set that has since been discontinued. However, in 2007, Full Moon Features reacquired the rights to the first five films, and the boxset has since been reissued and is available directly from Full Moon, as well as through several online retailers.

==Plot== secret of magic Andre Andre Toulon used to give his puppets life. It transpires also that a team of researchers working on the development of artificial intelligence are close to discovering Toulons secret. Sutekh sends one of the Totems as a package to two of the researchers involved, Dr. Piper and Dr. Baker of the Phoenix Division, who are taken by surprise, killed and stripped of their souls by the foul creature.
 caretaker at Bodega Bay Inn and has also been using it for a place to conduct his experiments on the A.I. project. The same night Drs. Piper and Baker are murdered, Ricks friends Suzie, Lauren, and Cameron come to visit him. At dinner, Lauren, who is a psychic, finds Blade (who had been discovered earlier by Rick inside the house and is still animate) and then Toulons old trunk, with the puppets, Toulons diary and some phials with the Potion|life-giving formula inside. Out of curiosity, Rick and his friends use the fluid on the puppets, and one by one they awaken; next to Blade, they find Pinhead, Six Shooter, Tunneler and Jester. (Torch, who joins the puppet cast in the sequel, makes no appearance here.)
 strange gameboard contact Toulon for its exact composition (the recipe of which was not recorded in the diary). But the glowing pyramid icon which goes with the board is a conduit between the mortal world and the underworld; Sutekh uses the link to send two of his Totems to attack. Cameron and Lauren attempt to flee by car, but Cameron is ambushed by one of the Totems inside his car and killed, while Lauren manages to get back into the hotel. When Rick looks after Cameron, the Totem attacks him as well, but he manages to escape.
 lariat to divert some of the lightnings power into the Totem. Decapitron briefly awakens, and his head morphs into the likeness of Toulon, who explains to Rick the origin and the secret of the life-giving formula. The phial, however, turns out to be missing; immediately suspecting Cameron, Rick goes back to search his body, where he does find the phial.

Meanwhile, the last Totem corners the panicked Lauren and prepares to drain her life away when Suzie interferes and douses it with acid. Toulon speaks through Lauren, urging Rick to animate Decapitron to destroy the Totem, and Rick uses his computer to divert power from his generator into Decapitron, bringing him to life. As the Totem attacks, Decapitron exchanges his plastic head for an electron-bolt launching system and destroys the creature. Afterwards, Toulon speaks to Rick yet again, surrendering custody of his puppets and the formula to him and promising his help in times of need.

==Cast== Andre Toulon
* Gordon Currie – Rick Myers
* Chandra West – Susie
* Ash Adams – Cameron
* Teresa Hill – Lauren
* Felton Perry – Dr. Carl Baker
* Stacie Randall – Dr. Leslie Piper
* Michael Shamus Wiles – Stanley
* Dan Zukovic – Delivery Man
* Jake McKinnon - Sutekh (Uncredited)

===Featured puppets=== Blade
* Pinhead
* Jester
* Tunneler Six Shooter Decapitron
* Totem
* Torch (movie poster only)

==Timeline Issue== Eastern Front, whose conduct of operations didnt take place until summer of 1941. In this film, Toulons diary recounts Major Krausss death as being on April 7. Since in previously established timelines, Toulon made it to America almost one year after escaping, Toulon would have to had killed himself on March 15 either in 1943 up to 1945.
* The ending to part 2 of the franchise is not referenced, and the continuity does not match. His Unholy Creations.

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 