No Way Back (1995 film)
{{Infobox film
| name           = No Way Back
| image          = Russell Crowe Helen Slater No Way Back 1996.jpg
| caption        = Blu-Ray cover  Frank A. Cappello
| writer         = Frank Cappello Derrick DeMarney Michael Lerner Etsushi Toyokawa David C. Williams
| cinematography = Richard Clabaugh
| editing        = Sonny Baskin Columbia Tri-Star
| distributor    = Sony
| released       =    
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
No Way Back is a 1995 American action thriller film directed by Frank Cappello and starring Russell Crowe and Helen Slater. 

==Plot== skin head Michael Lerner), the mafioso retaliates by kidnapping the son of Zach Grant (Russell Crowe), the FBI agent in charge of the botched undercover sting.  Simultaneously, Yuji (Etsushi Toyokawa), wanted dead by the Yakuza for forthcoming legal testimony, breaks free from Grants supervision during a trans-Atlantic flight.  Thus, causing their airplane, along with Mary (Helen Slater), the ditzy stewerdess accompanying them, to make an emergency-landing.  In an attempt to free Yuji from the gaze of the Yakuza and regain custody of Zachs child from the mafia, the three crash survivors go off of the grid and set into motion a dangerous plan that could quickly facilitate their untimely demise. 

==Cast==
* Russell Crowe as FBI Agent Zack Grant
* Helen Slater as Mary
* Etsushi Toyokawa as Yuji Kobayashi Michael Lerner as Frank Serlano
* Kyūsaku Shimada as Tetsuro
* Ian Ziering as Victor Serlano

==Reception==
The movie has received mixed-to-negative reviews with Rotten Tomatoes reporting a 30% audience approval rating.   The performances of Crowe and Slater, in particular, were lauded as being a cut above the quintessential action-genre norm. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 