Rome 11:00
{{Infobox film
| name           = Roma ore 11
| image          = Rome_11_00-384807191-large.jpg
| caption        =  
| director       = Giuseppe De Santis
| producer       = 
| writer         = Cesare Zavattini Basilio Franchina Giuseppe De Santis Rodolfo Sonego Gianni Puccini  
| narrator       = 
| starring       = Carla Del Poggio Lucia Bosé Raf Vallone
| music          = Mario Nascimbene
| cinematography = Otello Martelli
| editing        = Gabriele Varriale
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Italian drama Neorealist filmmaking. Augusto Genina made the film named Tre storie proibite,  based on the same tragic accident at Savoia Street that Rome ore 11 is about.

==Plot==
The film is based on a real story, an accident that happened in Rome, when a staircase fell down because of the weight of hundreds of women waiting for a job interview as a secretary.

In response to a job ad in a newspaper, two hundred girls present themselves to an address in Via Savoia to get a job of typist in the office of an accountant. In an Italy exasperated by general unemployment, but even more by the impossibility for women to get a job, the girls that want to have the interview are from the most diverse young women: fallen nobles and prostitutes seeking to change their lives, wives with unemployed husbands, affluent daughters where the pension is not enough to survive.

Crowded on the stairways of the small building, they exchange impressions and made mention to their lives of misery and tricks for a living. Then, a furious fight for the priorities in the line unchain the girls, whose arrogance to switch ahead turns in tragedy: the staircase railing gives up, destroying one by one the steps,  and plunging the women; some of them remain seriously injured, while one, Anna Maria Baraldi, dies.

Taken to hospital, the bitter discovery: to be treated, the hospital is demanding a payment of 2,300 Lire, daily. So, many of them are forced to rush home because unable to pay.

==Cast==
*Lucia Bosé, Simona
*Carla Del Poggio, Luciana Renzoni
*Maria Grazia Francia, Cornelia Riva
*Lea Padovani, Caterina
*Delia Scala, Angelina
*Elena Varzi, Adriana
*Raf Vallone, Carlo
*Massimo Girotti, Nando
*Paolo Stoppa, Impiegato
*Armando Francioli, Romoletto
*Paola Borboni, Matilde
*Irene Galter, Clara
*Eva Vanicek, Gianna
*Checco Durante, father of Adriana
*Alberto Farnese, Augusto
*Bianca Beltrami
*Cabiria Guadagnino
*Teresa Ellati
*Maria Pia Trepaoli
*Fulvia Trozzi
*Donatella Trombadori
*Helène Vallier
*Nando Di Claudio
*Fausto Guerzoni
*Michele Riccardini
*Renato Mordenti
*Pietro Tordi
*Ezio Rossi
*Henry Vilbert
*Marco Vicario
*Mino Argentieri
*Anna Maria Zijno (real survivor at via Savoia)
*Maria Ammassari (real survivor at via Savoia)
*Renata Ciaffi (real survivor at via Savoia)

==Awards==
*Nastro dArgento Best Music (Mario Nascimbene)

== External links ==
*  
 

 
 
 
 
 
 
 
 
 
 
 
 