The Business of Fancydancing
{{Infobox film
| name           = The Business of Fancydancing
| image          = BusinessOfFancydancing.jpg
| image_size     =
| caption        = DVD cover
| director       = Sherman Alexie
| producer       = Larry Estes Scott Rosenfelt
| writer         = Sherman Alexie
| narrator       =
| starring       = Evan Adams  Michelle St. John   Gene Tagaban
 Swil Kanim Rebecca Carroll
| music          = Brent Michael Davids
| cinematography = Holly Taylor
| editing        = Holly Taylor
| distributor    = Outrider Pictures
| released       = 2002
| runtime        = 103 min.
| country        = United States|U.S. English
| budget         =
| preceded_by    =
| followed_by    =
}}

The Business of Fancydancing is a 2002 film written and directed by Sherman Alexie.   
 Indian heritage gay man white boyfriend plays out in multiple cultures and relationships over his college and early adult years. His literary success as a famed American Indian poet, resulting in accolades from non-Indians, contrasts with a lack of approval from those he grew up with back on the reservation. The protagonist struggles with discomfort and alienation in both worlds. 

Seymour returns to the reservation for the funeral of his friend Mouse (Swil Kanim), a violinist, and Seymours internal conflict becomes external as his childhood friends and relatives on the reservation question his motivation for writing Indian-themed poems and selling them to the mainstream public.  The film examines several issues that contemporary American Indians face, including cultural assimilation (both on the reservation and in urban areas), difficult stereotypes, and substance abuse.

In the DVD commentary, Alexie refers to Michelle St. Johns character, Agnes Roth, a mixed-race (Spokane/Jewish) woman who moves to the reservation to teach in the school, as "the moral center of the film". Agnes is also an ex-lover of Polatkins, with the two of them still maintaining a deep friendship. 
 Mohican composer Brent Michael Davids. The violin solos were composed and performed by Swil Kanim, and a number of the actors sing. The film also features Alexies poetry, and the authors mother served as a language consultant.  

The film was made in an experimental and largely non-hierarchical manner, with a predominantly female crew; many scenes were improvised, with biographical details from the lives of the actors as well as the writer/director. This is discussed in detail on the DVD commentary and the behind the scenes documentary included in the DVD release, where Alexie comments that he wanted to make a film that not only discussed his politics, but put them into practice in the making of the film.

==See also==
 
* Fancy dance
* List of American films of 2002
* Sherman Alexie Smoke Signals
 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 