Foxhole in Cairo
 
{{Infobox film
| name           = Foxhole in Cairo
| image          =
| image_size     =
| caption        =
| director       = John Llewelly Moxey
| producer       = Steven Pallos
| writer         = Leonard Mosley
| narrator       =
| starring       = James Robertson Justice Adrian Hoven Fenella Fielding
| cinematography = Desmond Dickinson Ken Jones
| distributor    = British Lion Film Corporation
| released       =  
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
Foxhole in Cairo is a 1960 British war film directed by John Llewellyn Moxey and based on a novel by Leonard Mosley itself based upon the real-life Operation Salaam. It starred James Robertson Justice, Adrian Hoven, Fenella Fielding and Henry Oscar. Future star Michael Caine makes a brief appearance as a German soldier, in one of his earlier screen roles. 

==Synopsis== Second World Eighth Army. They are able to monitor every move of the British. It falls to British intelligence to hunt down the spies before they do too much damage to the war effort.

==Cast==
* James Robertson Justice as Captain Robertson John Eppler
* Niall MacGinnis as Radek Cont Almasky Robert Urquhart as Major Wilson Neil McCallum as Sandy
* Fenella Fielding as Yvette
* Gloria Mestre as Amina Erwin Rommel John Westbrook as Roger
* Lee Montague as Aberle
* Henry Oscar as Col. Zeltinger
* Howard Marion-Crawford as British Major
* Anthony Newlands as S.S. Colonel
* Richard Vernon as British General
* Michael Caine as Weber
* Jerome Willis as 1st British Signals Sergeant Philip Bond as German signals sergeant

==Reception==
A 1961 New York Times review described the film as "a routine British-made espionage yarn" calling the plot "slack and predictable", while praising the professional performance of James Robertson Justice. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 