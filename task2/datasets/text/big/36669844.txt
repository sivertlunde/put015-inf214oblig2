La cigüeña distraída
{{Infobox film
| name           = La cigüeña distraída
| image          = La cigüeña distraída lobby card.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical lobby card
| director       = Emilio Gómez Muriel
| producer       = Mario A. Zacarías
| writer         = Roberto Gómez Bolaños
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Marco Antonio Campos Gaspar Henaine Alma Delia Fuentes María Duval Emily Cranz Rosa María Vázquez Antonio Henaine Francisco Navarrete Fannie Kauffman
| music          = Manuel Esperón
| cinematography = Ezequiel Carrasco
| editing        = José W. Bustos
| studio         = Estudios Churubusco
| distributor    = Producciones Zacarías
| released       =  
| runtime        = 88 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
La cigüeña distraída ( ) is a 1966 Mexican comedy film directed by Emilio Gómez Muriel and starring the double act Viruta y Capulina, performed by Marco Antonio Campos and Gaspar Henaine.

==Plot==

When two different women give birth to twin brothers in a hospital, a confused nurse changes the kids. Some years after, one of the couples is a pair of rich and arrogant brothers that are meant to be married to a couple of rich sisters, Alma and Maria Castillo, while the other couple of brothers, a poor but friendly unemployed guys, fall in love with Emilia and Rosita, the maids of the Castillo sisters. When the two couples meet, confusion is ensued in all sides. After several situations that almost break the compromise of all four couples, the girls realize about the problem and make amends with the boys. Some time later, the spouses of both Capulinas give birth to twins.

==Cast==
*Marco Antonio Campos as Viruta Palacios / Viruta Corrales 
*Gaspar Henaine as Capulina Palacios / Capulina Corrales 
*Alma Delia Fuentes as María Castillo
*María Duval as Alma Castillo
*Emily Cranz as Emilia
*Rosa María Vázquez as Rosita
*Antonio Henaine as Capulina, Jr.
*Francisco Navarrete as Viruta, Jr.
*Fannie Kauffman as Mrs. Palacios
*Óscar Ortiz de Pinedo as Attorney Atenógeres Castillo
*Arturo Castro as Attorney Arturo Castro 
*Mary Ellen as Miss Offside
*Antonio Raxel as Don Antonio
*Mario García "Harapos" as The Short Thief
*Pedro de Urdimalas as Jaime
*Nathanael León as Bowler
*Gloria Chávez as The First Midwife
*Arturo Correa as Bermúdez
*Rosa Carbajal as Doña Regina
*Roy Fletcher as Journalist
*Ramón Valdés as The Skinny Thief 
*Evelyn Ortiz as Mrs. Corrales
*Perla Walter as Bowler Woman
*José Jasso as Mr. Cejudo (uncredited)
*Armando Gutiérrez as Don Gelasio (uncredited)

==External links==
* 

 

 
 

 