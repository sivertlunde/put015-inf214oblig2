Losing Balance
 
{{Infobox film
| name           = Losing Balance
| image          = 
| alt            =  
| caption        = 
| director       = Felix Fuchssteiner
| producer       = Katharina Schöde
| writer         = Felix Fuchssteiner Katharina Schöde
| starring       = Elisa Schlott Michael Lott Petra Kleinert Sina Tkotsch
| music          = Philipp F. Klömmel
| cinematography = Matthias Schellenberg Ralf Schlotter
| editing        = Nicole Kortlücke
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}

Losing Balance is a 2009 drama film, written and directed by Felix Fuchssteiner and Katharina Schöde.

==Plot==

Despite a dark secret, 14-year-old Jessika (Elisa Schlott) fights to keep her broken family together. 
When Jessikas father (Michael Lott) loses his job, their family life becomes an impossible struggle. While her parents try to repair their already off-balance relationship and her sister Caro (Sina Tkotsch) becomes increasingly obsessed with boys, Jessika is left to her own devices. The lack of communication between the parents and their incapability to manage their off kilter situation, culminates in a horrifying event, that Jessika is forced to witness. In the face of her parents delusional denial, Jessica begins to fall into a deep self-destructive depression. To save herself from her familys dark abyss, and despite her parents continuous attempts to hold her back, Jessika decides to take her future into her own hands and takes a very courageous step….

==Awards==
Losing Balance was awarded the Young German Cinema Award as Best Production. In addition Elisa Schlott won the award for Best New Actress. The Young German Cinema Award is awarded by the Bavarian Broadcasting Company (BR), the Bavaria Film Production and the Hypovereins Bank and is offered to promising young filmmakers every year as part of the Munich Film Festival.
The film was also awarded as Best First Feature at Biberacher Filmfestspiele and got the Award of the Student Jury.

== Reception ==
"Powerful themes of familial trauma, of a type that soap operas gleefully exploit, are handled by Fuchssteiner with remarkable integrity, wit, realism and restraint. LOSING BALANCE defies expectation. The story is profoundly affecting but never melodramatic; moving but never mawkish; clever but never cliched."   Cambridge Film Festival Daily 

"A beautifully crafted story that provides the opportunity for insight into the particular problems of a dysfunctional family. The lead character does a wonderful job as the "peacemaker" and the dynamic between the sisters and the contrast of their characters was incredibly convincing. It is a very touching film. Realistic, yet heartbreaking and hopeful in the end." 

== References ==
 

==External links==
* 
* 

 
 
 
 