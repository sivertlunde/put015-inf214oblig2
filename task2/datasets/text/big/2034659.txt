The Man with My Face (film)
{{Infobox film
| name           = The Man with My Face
| image          = The Man With My Face.JPG
| alt            =
| caption        = Theatrical release poster Charles Kerr (assistant)
| producer       = Ed Gardner
| screenplay     = Tom McGowan Edward Montagne Samuel W. Taylor Vin Bogert
| based on       =  
| starring       = Barry Nelson Carole Mathews Lynn Ainley Robert McBride
| cinematography = Fred Jackman, Jr.
| editing        = Gene Milford
| distributor    = United Artists
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English}}
 crime thriller thriller directed by Edward Montagne, featuring Barry Nelson, Carole Mathews, and Lynn Ainley. The film marks Jack Wardens movie debut. 

Though the movie takes place in Puerto Rico, the original novel is set in California. However, The Man with My Face holds the distinction of being the only film noir shot on location in that country.

==Plot==
Set in Puerto Rico, where Charles "Chick" Graham (Nelson) settled down after the war to run a small business with his old Army buddy, now his brother-in-law, Buster Cox (Harvey). Graham comes home one evening to find his wife, Cora (Ainley), acting as if he is an insane stranger.
He finds that a double has taken his place. There is a man who looks exactly like him, Albert "Bert" Rand (Nelson), playing cards and drinking in his living room. Cora and Buster, and even Grahams dog, do not recognize him and think the double is him.
 Miami who made away with half a million dollars. As he runs from the police, he attempts to solve the mystery with the help of an old girlfriend, Mary Davis (Mathews), whom he jilted to marry Cora. Marys protective brother, Walt Davis (Warden), is wary, but soon joins in trying to figure out the puzzle.

Grahams criminal double attempts to have him killed by hiring an attack dog specialist to have a Doberman go after him. Albert, the evil double has been in on this sinister plan with Cora and her brother, Buster, since before their marriage.  

==Cast==
* Barry Nelson as Charles "Chick" Graham / Albert "Bert" Rand
* Carole Mathews as Mary Davis
* Lynn Ainley as Cora Cox Graham John Harvey as Buster Cox
* Jim Boles as Meadows
* Jack Warden as Walt Davis
* Henry Lascoe as The Police Sergeant
* Johnny Kane as Al Grant
* Chinita as Juanita
* Armando Miranda as Nightclub Bartender

==Reception==
===Critical response===
Film critic Dennis Schwartz was lukewarm about the film, writing, "The B film has a good premise over mistaken identity, but a lousy execution. Montagne keeps it good enough as a diversionary time killer, but its just too bad it never was convincing." 
 
==References==
 

==External links==
*  
*  
*  
*   film scene at Veoh

 

 
 
 
 
 
 
 
 
 
 
 