We Found Love (music video)
 
{{Infobox film
| name           = We Found Love
| narrator       = Agyness Deyn
| starring       =  
| director       = Melina Matsoukas
| producer       = {{plain list|
*Juliette Larthe
*Ben Sullivan
*Prettybird   (co-producer)  
*Candice Ouaknine   (executive producer)  
}}
| studio         = {{Flat list| Def Jam
*SRP}} Universal Music and Video Distribution
| released       =    (premiere) |   (download) }}
| runtime        =  
| editing        = Jeff Selis
| country        = United States English
| music          = Calvin Harris
}}
 New Lodge location of the set informed BBC that traffic in the area was congested as drivers wanted to see the singer. The video premiered on October 19, 2011, and was made available to download digitally three days later on October 22. As of April 2015, the video has amassed over 460 million views on VEVO.
 DJ scenes, while the video has regular references to popular culture, such as themes of films and content of other singers videos.
 Man Down". Best Short Video of Polish Top Airplay TV Chart.

== Development ==
 , Northern Ireland, where recording was halted when the fields owner, Alan Graham, expressed distaste at Rihanna stripping to the waist on his land.]]
The music video for "We Found Love" was shot on September 26–28, 2011, in   where both photographers and fans were barred from watching the singer.   

 :

 We love, obviously, to do provocative imagery&nbsp;... we always try to definitely push the limits&nbsp;... I think because, in the end, its not really at all about domestic violence. Its really just about it being toxic, and theyre on this drug trip and that definitely plays a part, but I think its also about being triumphant over those weaknesses, and she leaves him. Its not trying to glorify that type of relationship. The bad parts of it, thats what you dont want. In the end, her leaving, it represents her getting that out of her life. The drugs and the addiction and the toxic—thats what brings her downfall and brings a lot of harm.    
 Chris Brown in February 2009, saying that it is not a reenactment of what happened between Rihanna and Brown, but rather that Rihanna is acting in the video. Matsoukas said:

   totally rave-y&nbsp;... and thats the feeling, just music rushing over you, and then I started thinking about drugs and addiction and love and how thats an addiction&nbsp;... weve all lived the ups and downs of being in a toxic relationship. Its really about the obstacles of trying to let it go, but at the same time how great it makes you feel, so its hard to let it go. Again, it goes back to a story that we all can relate to&nbsp;... Its not Rihannas story; its her story in the video, and shes acting. But everybodys  . Obviously, theres a lot of comparisons to her real life, and thats not at all the intention. Its just that I guess people naturally go there because art imitates life, and its a story we all relate to and weve all experienced. Like, its based on my life; its based on her life; its based on your life, like, everybody.  
 Man Down" buttock cheek was not part of the videos treatment, and that OShaughnessy improvised it. Upon the videos release, multiple media outlets and music critics compared the content of the video to a reenactment of Rihanna and Browns physical altercation. When asked about this, Matsoukas clarified that the video was not linked to Brown in any circumstance, and said, "  doesnt even really look like Chris Brown to me."   

== Synopsis ==
 s.]]
The video begins with a monologue by fashion model Agyness Deyn about love and heartbreak:   
 

During the narration, Rihanna and her lover are depicted in both love and hate scenarios. Before the song begins to play, lightning bolts are projected onto a wall in which Rihanna stands in front of. She and her lover are depicted as enamored with each other, and enjoying activities such as fairground rides and eating in fast food restaurants. During the chorus, images of drugs, pills, and dilated pupils are shown, while brief scenes of Rihanna and her boyfriend preparing to have sex are shown.

The chorus continues and the video cuts to a scene in which Rihanna and other people dance at an outside rave; Calvin Harris features as the DJ. As the second chorus begins, Rihanna and her boyfriend are happily running amok in a supermarket, pushing each other in a shopping cart and spraying canned drinks at each other. However, they begin arguing in a car. The video then shows the couple experiencing mounting difficulties in their relationship. They eventually begin to physically abuse one another. During the final chorus, Rihanna is seen vomiting streamers; she is also seen unconscious on the street while her boyfriend tries to revive her. After having had enough of the relationship, and finding him unconscious on the floor of his apartment, Rihanna leaves him. The video ends with Rihanna curled up in the corner of a room, crying.   

== Release and reception ==
  (1996), directed by Danny Boyle (pictured), because of its overly sexualized and violent themes, as well as the opening monologue by Agyness Deyn. ]] Blue Valentine due to its sexualized content and a narrative that consists of a couple in a turbulent relationship.    Erika Ramirez of Billboard (magazine)|Billboard magazine called the video artful and poignant, and noted that it shows "neon colors, explicit sex, bathtub embraces and painful arguments of the couples world".    Jocelyn Vena of MTV commented that although the song is upbeat and carefree, the video represents the antithesis, depicting "a dark look at love and substance abuse". Vena, like Coplan, noted that the song has a cinematic feel, calling it a "mini-movie". She compared the video to the films Trainspotting and Requiem for a Dream, with regard to its opening monologue, which Vena thought foreshadowed the video.   
 New York magazine was more critical of the video; she criticized Rihannas undressing in a farmers field as "the least of its provocations" and listed the activities the couple partake in: "smoking, drinking, dancing on fast-food tables, dancing at raves, heavy petting, stealing groceries, drunk wheelies, and bathing while fully clothed   candy-colored drugs". Dobbins noted that the list of illicit and frowned upon activities may aggravate parents and committees because of the explicit nature of the video, which caused controversy for Rihanna in her videos for "S&M" and "Man Down".    Charlotte Cowles of the same publication commented that the voice-over provided by Deyn at the start of the video sounded "incredibly depressing". 

Leah Greenblatt of Entertainment Weekly described the video as "a Trainspotting-meets-Drugstore Cowboy portrait of wasted youth and finding love in an apparently very pharmaceutical place". Greenblatt continued her review in a more negative manner, asking if it "paint  a too-glamorous portrait of crazy, stupid love for her young fans? Or is it  s prerogative to push the boundaries of dilated pupils, couch sex, and how many cigarettes two people can conceivably smoke simultaneously?"    Jamie Lewis of the International Business Times noted that the video is likely to cause controversy among sections of the public due to the "multitude of illicit and illegal acts", and condemned the activities that the singer appeared to be partaking in, writing, "Rihanna can be seen swallowing unmarked pills, smoking what looks like marijuana, publically stripping, stealing goods and vandalising". Lewis also wrote that Rihanna had received mixed reactions from her fans via her Twitter and Facebook feeds.   

=== Accolades === MTV Video Best Pop Video of the Year.    The video received the Grammy Award for Best Short Form Music Video at the 55th Grammy Awards which was held on February 10, 2013 at the Staples Center, Los Angeles. 

== Controversy == The Daily Star that the clip "is a disgrace. It sends the message that she is an object to be possessed by men, which is disturbingly what we see in real violence cases".  The video was also criticized by Brandon Ward, a youth pastor of Oasis Christian Center in Staten Island, New York, for "damaging the moral and self-worth of young impressionable teens."    Ward wrote how he thought the video affects females sense of self-worth:

 The real issue is that it moves the moral center more towards the obscene. That it becomes more normal to be more sexually promiscuous, because they are bombarded with imagery that is loaded with innuendo, and that is seen as normal, even preferred&nbsp;... if girls and women find their identity and self-worth in the approval of people, they will do whatever it takes to become popular and loved. When stars like Rihanna, who blast sexuality, are thrust into the limelight, girls tend to think that is the way for them to be valuable. God tells us that we are fearfully and wonderfully made&nbsp;... bearing His image. Rihanna is selling a lie.  

John Colonnello, a youth pastor from   (PTC) criticized Rihanna for depicting a "cold, calculated execution of murder" in the video and for portraying a scenario of rape.  On November 23, 2011, the music video for "We Found Love" was banned from broadcast before 10&nbsp;pm in France, owing to its violent, dangerous, and sexually suggestive nature.  Ulster Cancer Foundation, an anti-smoking organization, condemned Rihanna for smoking in the music video.    Doreen Reegan, a spokesperson for the organization, commented on the singers decision to include smoking as part of her promotional image, saying:

 Three-quarters of adult smokers start the habit as teenagers, which is why it is so irresponsible of Rihanna to influence her young fan base in this way&nbsp;... After so much hype around the filming of the video it was very disappointing to see Rihanna so blatantly smoking throughout it&nbsp;... Artists such as Rihanna are held in high esteem and regarded as role models by millions of young people.  

== Comparisons ==

=== Chris Brown ===
{{double image
|1=right
|2=Chris Brown singing at Brisbane Entertainment Centre 2.jpg
|3=130
|4=FFT26.jpg
|5=133 Chris Brown (pictured, left) and Dudley OShaughnessy, as well as how the content of the video appeared to be a reenactment of Browns assault on Rihanna. The plot and themes of the video also received comparisons to Britney Spears (pictured, right) "Criminal (Britney Spears song)|Criminal" video, which premiered two days before "We Found Love". 
}}
 2009 Grammy Awards. Chris Doplan of Consequence of Sound said that OShaughnessy strongly resembled Brown in his appearance.  Amanda Dobbins of New York magazine concurred with Doplan, and wrote that OShaughnessy has short blond hair like Brown once had.  Erika Ramirez of Billboard magazine also thought that OShaughnessys character is a depiction of Brown, especially in relation to the scene which features the couple in a car, with OShaughnessy driving recklessly and Rihanna begging him to stop.  William Goodman of Spin (magazine)|Spin provided an analysis of the video with regard to the Brown comparisons. Like many reviewers, he noted the resemblance between Brown and OShaughnessy, writing "The clip stars a dead ringer for Brown, complete with dyed hair and bulging biceps". Goodman commented upon the videos opening narration, which was actually voiced by Agyness Deyn, "The video opens with a Rihanna voice-over that warns, You almost feel ashamed that someone could be that important. No one will ever understand how much it hurts&nbsp;... you almost wish that you could have all that bad stuff back, so that you have the good ." In conclusion, Goodman commented that  the scene showing an altercation in a car is reminiscent of Browns assault on Rihanna, which occurred inside a car.    Goodman wrote: "during a heated argument between the couple in a muscle car, the Brown look-alike gives the Barbadian beauty a slap on the face. Convinced yet?"   

=== Britney Spears "Criminal" ===
The video also received comparisons to Spears "Criminal (Britney Spears song)|Criminal", which was released two days before "We Found Love", on October 17, 2011.     Critics noted that both videos were filmed in the United Kingdom and caused controversy there, both contain scenes of sex, violence, and crime, and both featured "bad boy" stereotypes and evoke the personal lives of the artists.    Katherine St Asaph of PopDust noted that although most pop stars release videos that draw from their personal lives, they do not make the viewer uncomfortable. The same cannot be said for Spears and Rihanna, regardless of whether they have moved on from past situations, because the discussion about them has not. St Asaph also expressed that neither Spears nor Rihanna said much about the parallels to their lives, but that they do not need to—their videos are much more effective than anything the singers could reveal in an interview.  Rae Alexandra of the SF Weekly wrote that both videos feature an anti-British sentiment and that the villains in the video, Rihannas boyfriend and Spears policemen, are British, whereas Spears savior in her video is an American criminal. Alexandra noted that Spears and Rihanna chose to film their grittiest videos in a country with a lower crime rate than the United States. According to her, the videos continue with a tradition of xenophobic portrayals of British people as villains by American film and video directors. 

=== References to popular culture ===
The video for "We Found Love" references popular culture in the form of films. According to James Montgomery of MTV News, its main themes are sex, drugs, and violence, which feature heavily in works by independent filmmaker Gregg Araki.    Many of Arakis films, including Totally Fucked Up (1993), The Doom Generation (1995) and Nowhere (film)|Nowhere (1997), depict scenes of a sexual nature, drug abuse and violence as part of their narratives.  "We Found Love" s use of vibrant colors and imagery is reminiscent of Oliver Stones 1994 film Natural Born Killers, which depicts two murderers, Mickey and Mallory, on a killing spree in the southwest of the United States. The film incorporates "nightmarish-colors" and rear wall projected imagery, similar to that shown in "We Found Love".  Critics noted that the music video features visual effects that resembled those in Darren Aronofsky s film Requiem for a Dream (2000), which incorporates a close-up shot of a dilated pupil; this effect was used many times in "We Found Love s video. 
 marijuana exhalations Ray of Light" (1998). James Montgomery of MTV News noted that "Ray of Light" s video features "a whole lot of high-speed, time-lapse shots taken in cities around the world", the same technique used for the drug scenes in "We Found Love". 

== Credits ==
 
 

;Video credits
* Melina Matsoukas via Prettybird ― direction 
* Juliette Larthe & Ben Sullivan ― produced   
* Candice Ouaknine ― executive producer 
* Paul Laufer ― direction of photography 
* Mark Gerahty ― art director 
* Mark Geraghty ― production designer 
* Bert Yukich and Amy Yukich (KromA) ― visual effects 
* Jeff Selis ― editional 
* Agyness Deyn ― narration 
* Calvin Harris ― cameo appearance 
* Dudley OShaughnessy ― starring role 
* Rihanna ― starring role 
 

;Song credits 
* Rihanna ― vocals mixing and instrumentation
* vocal recording
* Alejandro Barajs ― assistant recording engineer
* Phil Tan ― mixing
* Damien Lewis ― assistant mixing

 

== Charts ==
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
! scope="col" | Chart (2011)
! scope="col" | Peak position
|- Poland (Polish Top Airplay TV Chart)  1
|}

== Release history ==
 
{|class="wikitable plainrowheaders"
|-
! scope="col"| Country
! scope="col"| Date
! scope="col"| Format
! scope="col"| Label
! scope="col"| Ref.
|-
!scope="row"| Worldwide
| October 19, 2011
| Online premiere (via VEVO)
|  
| 
|-
!scope="row"| Argentina October 22, 2011 Digital download Def Jam Recordings
|   
|-
!scope="row"| Australia
|   
|-
!scope="row"| Austria
|   
|-
!scope="row"| Brazil
|   
|-
!scope="row"| Czech Republic
|   
|-
!scope="row"| France Universal Music Universal Music
|   
|-
!scope="row"| Germany
|   
|-
!scope="row"| Greece Def Jam Recordings
|   
|-
!scope="row"| Hungary
|   
|-
!scope="row"| Ireland
|   
|-
!scope="row"| Italy
|   
|-
!scope="row"| Japan
|   
|-
!scope="row"| The Netherlands
|   
|-
!scope="row"| New Zealand
|   
|-
!scope="row"| Spain
|   
|-
!scope="row"| Switzerland
|   
|-
!scope="row"| United States October 25, 2011
|   
|}

== References ==
 

== External links ==
*  

 
 
 

 

 
 
 
 
 