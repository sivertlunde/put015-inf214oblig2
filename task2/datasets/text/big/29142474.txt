My Friend Sainfoin
{{Infobox film
| name           = Mon ami Sainfoin
| image          = 
| caption        = 
| director       = Marc-Gilbert Sauvajon
| producer       = 
| writer         = Paul-Adrien Schaye Roger Vercel
| starring       = Pierre Blanchar
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 10 May 1950
| runtime        = 85 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1950, directed by Paul-Adrien Schaye, written by Bernard de Latour, and starring Pierre Blanchar. The film features Louis de Funès as guide. 

== Cast ==
* Pierre Blanchar: Sainfoin, friend and shopper
* Sophie Desmarets: Eugénie de Puycharmois, Guillaumes wife
* Jacqueline Porel: Yolande, the driver
* Denise Grey: Eugénies mother
* Alfred Adam: Guillaume de Puycharmois, Eugénies husband
* Jean Hebey: the cabaret artist
* Henry Charrett: father Machin
* Louis de Funès: guide
* Eugène Frouhins: the peasant
* Albert Michel: the boy
* Nicolas Amato: the doctor

== References ==
 

== External links ==
*  

 
 
 
 
 


 