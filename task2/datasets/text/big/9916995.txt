The Airmail Mystery
{{Infobox film
| name           = The Airmail Mystery
| image          = Airmail Mystery.jpeg
| image size     = 150px
| caption        = Film poster Ray Taylor
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       =   
| runtime        = 12 chapters (225 min)
| country        = United States English
| budget         = 
| preceded by    = 
| followed by    = 
}} Universal Serial movie serial. It is considered to be lost.

==Plot== Al Wilson), Bobs best friend. The Black Hawk carries out a series of attacks on Bobs ore shipments by air. He uses an unusual catapult that launches aircraft into the sky to intercept Bobs aircraft. With Mary Ross (Lucile Browne), his sweetheart, Bob constantly battles against his enemy, and eventually is able to defeat him.
==Chapter titles==
# Pirates of the Air
# Hovering Death
# A Leap for Life
# A Fatal Crash
# The Hawk Strikes
# The Bridge of Destruction
# The Hawks Treachery
# The Aerial Third Degree
# The Attack on the Mine
# The Hawks Lair
# The Law Strikes
# The Mail Must Go Through
 Source:  

==Cast==
  
* James Flavin as Bob Lee
* Lucile Browne as Mary Ross
* Wheeler Oakman as Judson Ward ("The Black Hawk")
* Frank Hagney as Moran
* Sidney Bracey as Driscoll 
* Nelson McDowell as "Silent" Simms
* Walter Brennan as Holly Al Wilson as Jimmy Ross Bruce Mitchell as Capt. Grant
* Jack Holley as Andy 

==Production==
The Airmail Mystery was Universals first aviation serial and it set the pattern for the serials and feature films to follow. 
 Al Wilson worked together with stuntmen like Frank Clarke and Wally Timm and also for movie companies, including Universal Pictures. After numerous appearances in stunt roles, he started his actor career in 1923, with the serial,The Eagles Talons.  He produced his own movies until 1927, when he went back to work with Universal. Wilson was also one of the pilots in Hells Angels (film)|Hells Angels (1930) and during filming, he was involved in an accident where the mechanic Phil Jones died. This episode marked the end of his career as stunt pilot in movies, although he continued to work as an actor.    Silents are Golden. Retrieved: January 16, 2011. 

Wilsons last role was in The Airmail Mystery. After production was complete, during the National Air Races in Cleveland in 1932, Wilsons aircraft crashed and he died a few days later in hospital due to the injuries he suffered. The accident is documented in the film Pylon Dusters: 1932 and 1938 Air Races, an historic film about the 1932 Cleveland Air Race.   Davis-Monthan Airfield Register Website. Retrieved: January 16, 2011. 

==See also==
* List of American films of 1932
* List of film serials by year
* List of film serials by studio

==References==
===Notes===
 
===Bibliography===
 
* Cline, William C. "3. The Six Faces of Adventure". In the Nick of Time. Jefferson, North Carolina: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Cline, William C. "Filmography". In the Nick of Time. Jefferson, North Carolina: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Harmon, Jim and Donald F. Glut. The Great Movie Serials: Their Sound and Fury. London: Routledge, 1973. ISBN 978-0-7130-0097-9.
* Weiss, Ken and Ed Goodgold. To be Continued ...: A Complete Guide to Motion Picture Serials. New York: Bonanza Books, 1973. ISBN 0-517-166259.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  

 
{{succession box  Universal Serial Serial 
| before=Detective Lloyd (1932)
| years=The Airmail Mystery (1932) Heroes of the West (1932)}}
 

 

 
 
 
 
 
 
 
 
 
 
 