Mack the Knife (film)
{{Infobox film
| name           = Mack the Knife
| image          = Mack the Knife (film).jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Menahem Golan
| producer       = Stanley Chase
| screenplay     = Menahem Golan
| based on       =  
| starring       = Raúl Juliá Richard Harris Julia Migenes Roger Daltrey Julie Walters
| music          = Kurt Weill
| cinematography = Elemér Ragályi
| editing        = Alain Jakubowicz Henry Richardson
| studio         = Golan-Globus|Golan-Globus Productions
| distributor    = 21st Century Film Corporation
| released       =  
| runtime        = 120 minutes
| country        = Netherlands
| language       = English
| budget         = United States dollar|USD$9 million
}} Weill Musical musical The Threepenny Opera.            

==Plot==
In 19th century London, young Polly Peachum falls for the famous womanizing criminal Macheath and they decide to get married, but because of her familys disapproval, her father ("the king of thieves") has MacHeath arrested.

==Cast==
* Raúl Juliá as Macheath
* Richard Harris as Mr. Peachum
* Julia Migenes as Jenny Diver
* Roger Daltrey as The Street Singer
* Julie Walters as Mrs. Peachum
* Bill Nighy as Tiger Brown
* Rachel Robertson as Polly Peachum
* Clive Revill as Money Matthew
* Erin Donovan as Lucy Brown
* Louise Plowright as Dolly
* Elizabeth Seal as Molly
* Chrissie Kendall as Betty
* Mark Northover as Jimmy Jewels

==Production== Tony nomination for a 1976 revival on Broadway. The film shows obvious signs of last minute editing, including several musical numbers that appear on the soundtrack album but are not in the final cut.

==Home media==
The film has never been released on DVD and as of April 2012, Sony Pictures Home Entertainment has yet to announce any plans for a DVD release.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 