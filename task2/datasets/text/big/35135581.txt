Tovaritch (film)
  French comedy film directed by Jacques Deval, Germain Fried, Jean Tarride and Victor Trivas. It starred Iréne Zilahy, André Lefaur and Marguerite Deval. It is based on the 1933 play Tovaritch (play)|Tovaritch by Jacques Deval. In 1930s Paris two exiled Russian aristocrats take jobs as domestic servants.

==Cast==
* André Lefaur : General Mikhaïl Ouratieff
* Irène Zilahy : Tatiana Ouratieff
* André Alerme : Mr Arbeziah
* Pierre Renoir : Gorotchenko
* Pierre Palau : Lhôtelier
* Marguerite Deval : Mme Arbeziah
* Junie Astor : Augustine
* Jean Forest : Georges
* Germaine Michel : La cusinière
* Ariane Borg Hélène
* Georges Mauloy : Chauffourier-Dubief
* Wina Winfried : Lady Carrigan
* Camille Bert : Comte Breginsky
* Gabrielle Calvi
* Laman
* Fabienne Orfiz
* Louis-Ferdinand Céline : Un figurant

==Bibliography==
* Alonso, Harriet Hyman. Robert E. Sherwood: The Playwright in Peace and War. University of Massachusetts Press, 2007.

==External links==
* 

 
 
 
 
 
 
 


 