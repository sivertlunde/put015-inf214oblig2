Ben and Me
{{Infobox film
| name           = Ben and me
| image          = Bendxd.jpg
| image_size     =
| caption        = Mini-Classics Artwork
| director       =Hamilton Luske
| producer       = Walt Disney Robert Lawson (novel)  Bill Peet (screen story)
| narrator      = Sterling Holloway Bill Thompson
| music          = Oliver Wallace
| cinematography = 
| editing        = 
| distributor    = Buena Vista Distribution
| released       = 
 
| runtime        = 20 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Robert Lawson founding father Benjamin Franklin, the book, with illustrations by Lawson, focused more heavily on actual historical events and personages, and included incidents from Franklins French career at Versailles (city)|Versailles.
 Academy Award Best Short Subject, Two-reel.   
  It was released on VHS under the Walt Disney Mini Classics label in 1989 and was later released on DVD as a short film in the "Disney Rarities" volume of the Walt Disney Treasures collection.  It was also released on DVD in 2012 under the Disney Generations Collection.

This short was also notable for being the first release on the Buena Vista Distribution label. On its release, Ben and Me was packaged with a live action short called Stormy and the True-Life Adventures | True-Life Adventure documentary The Living Desert. When Disneys regular distributor RKO Pictures resisted the idea of a full length True-Life Adventure, Disney formed his own distribution company to handle future Disney releases. 

==Plot==
In present day, two tour groups are simultaneously visiting a statue of Benjamin Franklin. The human tour group in front of the statue discusses Franklins life and achievements, while the leader of a mouse tour group which is standing at the top of Franklins hat reveals the contributions of a mouse named Amos to Franklins career.
 Christ Church in Philadelphia, decides to leave his family - thus relieving them of another mouth to feed - and find work somewhere. After no luck, and while trying to take shelter from a freezing and snowy night, Amos befriends Benjamin Franklin in his printing shop. Eventually Amos aids in Franklins publishing, inventing, and political career. Amongst Amos contributions were making bifocals, inspiring Franklin to build the Franklin stove and suggesting how to fix a major problem with it, and encouraging Franklin to print an event-oriented newspaper which Amos names the Pennsylvania Gazette.

After Bens experiments with electricity endanger Amos life, Amos leaves Ben, ignoring Bens pleas for him to return, and moves back in with his family.
 colonial attempt the king. But the mission is a failure. Franklin tells the crowd when he gets off a boat that "The King was unreasonable. He wouldnt listen." Amos, hearing this and seeing the confusion and anger of the Colonial history of the United States|colonists—realises that he could help, but he initially refuses. Amos and Franklin finally resolve their disagreements in the midst of the American Revolution, and Amos and Franklin play a key role aiding Thomas Jefferson with the drafting of the United States Declaration of Independence.

==Voices==
*Sterling Holloway as Amos Mouse
*Charles Ruggles as Benjamin Franklin Bill Thompson Governor Keith Thomas "Red" Jefferson / Crook 

==References==
 

==External links==
*  
*  
*  at Don Marksteins Toonopedia.   from the original on April 4, 2012.

 
 
 

 
 
 
 
 
 
 
 
 