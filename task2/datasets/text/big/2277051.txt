You Ought to Be in Pictures
{{Infobox Hollywood cartoon
|series=Looney Tunes (Daffy Duck/Porky Pig)
|image=You Ought to Be in Pictures (screenshot).jpg
|caption=Daffy tries to convince Leon Schlesinger that he should become the new star of Warner Bros. cartoons.
|director=Friz Freleng|I. Freleng Jack Miller Herman Cohen Gil Turner Cal Dalton 
|voice_actor=Mel Blanc
|musician=Carl W. Stalling
|producer=Leon Schlesinger
|studio=Warner Bros. Cartoons
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|release_date=May 18, 1940 (USA)
|color_process=Black and white(computer colorized in 1995)
|runtime=9 minutes
|movie_language=English
}}
 1940 Warner Bros. Looney Tunes short film featuring Porky Pig and Daffy Duck. The film combined live-action and animation, and features live-action appearances by Leon Schlesinger, writer Michael Maltese, animator Gerry Chiniquy and other Schlesinger Productions staff members. In 1994 it was voted #34 of the 50 Greatest Cartoons of all time by members of the animation field.  The title comes from the popular 1934 song "You Oughta Be in Pictures" by Dana Suesse and Edward Heyman, which plays in the beginning of the film.

==Summary==
Daffy wants to be the top star in the studio. To this end, he persuades Porky to resign from the Schlesinger studios to pursue a career in feature films as Bette Davis leading man. Porky goes to Leon Schlesinger and asks to have his contract torn up. Schlesinger reluctantly agrees, and wishes Porky the best of luck. Schlesinger looks at the audience and assures the audience that Porky will be back after Porky is out of earshot.

Porky spends the rest of the film trying to get into the lots and sets of an unnamed studio, with little success. After several failures (from convincing the security guard (played by Michael Maltese, voiced by Mel Blanc) to let him in and dressing up as Oliver Hardy to gain access, until the guard gave chase) and inadvertently interrupting the shooting of a ballet film, he decides to see if Schlesinger will take him back. 

He returns to Schlesingers office after frantically dodging his cartooned car in and out of live-action Los Angeles traffic, only to see Daffy doing a wild audition to become the new star of Warner Bros. cartoons, openly disparaging Porky. Porky then takes Daffy with him to another room, where he beats Daffy up. After this, he hurriedly runs into Schlesingers office to beg for his job back.  Schlesinger, laughing heartily and saying he knew hed return, reveals that he didnt really rip up Porkys contract, and happily tells him to get back to work. Porky gladly thanks him and runs back into the animation paper that he was in when the short started. Daffy, still not quite having learned his lesson after being beaten by Porky, again attempts to persuade Porky to resign and work with Greta Garbo, only to get splattered with a tomato, which irritates him.

==Production==
 

*First Daffy Duck cartoon directed by Friz Freleng. cartoon division.
*As noted, many staff members have cameos in this short:
**Leon Schlesinger &mdash; appears as himself
**Chuck Jones &mdash; one of the crowd rushing out during the lunch break
**Bob Clampett &mdash; another one of the Termite Terrace employees rushing frantically off to lunch
**Michael Maltese &mdash; the studio security guard (also voiced by Blanc)
**Gerry Chiniquy &mdash; studio director calling for quiet
**Henry Binder, Paul Marin &mdash; stagehands also calling for quiet. Binder is also the stagehand throwing Porky off the set dubbed in later (which is why most of them were dubbed by Mel Blanc except Leon Schlesinger).
*To keep the short on-budget, relatively few special effects were used to marry the animation and live action.  Where possible, the crew simply took still pictures of the office background and had them enlarged and placed directly on the animation stand.
*Despite being in black and white, this short was shown regularly on Looney Tunes on Nickelodeon, particularly during the Nick at Nite version.
*In 1995, the film was computer colorized and became a regular part of the Cartoon Network rotation. The film could also be seen in its original black and white form on the short-lived "Golden Jubilee" video collection of the mid-1980s, Cartoon Networks installment show Late Night Black and White, and Nick at Nites version of Looney Tunes on Nickelodeon.

==Availability==
You Ought to Be in Pictures is available on  , on Disc 4 and also on  , on Disc 1.

==References==
 

===Sources===
*Beck, Jerry and Friedwald, Will (1989): Looney Tunes and Merrie Melodies: A Complete Illustrated Guide to the Warner Bros. Cartoons. Henry Holt and Company.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 