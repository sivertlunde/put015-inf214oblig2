The Prisoner or: How I Planned to Kill Tony Blair
{{Infobox film
| name         = The Prisoner or: How I Planned to Kill Tony Blair
| image        = How_I_Planned_to_Kill_Tony_Blair_Poster.jpg Michael Tucker Petra Epperlein
| producer     = Petra Epperlein
| writer       = Michael Tucker Petra Epperlein
| starring     =
| distributor  = Truly Indie
| released     =  
| runtime      = 72 minutes
| language     = English
}} Michael Tucker.

The film depicts Yunis Khatayer Abbas, an Iraqi journalist who was detained by US troops in 2003 and later imprisoned at Abu Ghraib prison for nine months.  Although innocent, he was accused by American military officials of plotting to assassinate then British prime minister Tony Blair along with his two brothers.

==Production notes==
Directors Tucker and Epperlein were working on their film Gunner Palace, when they first encountered Abbas. 

== See also ==
 
* Abu Ghraib Prison
* Human rights in post-Saddam Iraq
* Iraq War
* Human rights violations in Iraq

==References==
 

==External links==
*  
* A   of the film by the Wild River Review
*  

 
 
 
 
 


 