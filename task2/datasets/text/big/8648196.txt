A Way of Life
 
{{Infobox film
| name = A Way of Life
| image = WayOfLife.jpg
| caption = Theatrical release poster
| director = Amma Asante Peter Edwards
| writer = Amma Asante
| narrator = Nathan Jones Marged Esli Oliver Haden David Gray Ian Wilson
| editing = Clare Douglas Stephen Singleton
| distributor =
| released =
| runtime = 93 minutes
| country = United Kingdom English
| budget =
| preceded_by =
| followed_by =}} British film 2004 starring Stephanie James and Brenda Blethyn. It is the debut film of director Amma Asante, who was a child actor and later screenwriter who founded her own production company. It was filmed in South Wales. The film is a fictional drama portraying Leigh-Anne Williams, a teenage mother living in a dilapidated Cardiff council flat. Brenda Blethyn plays the childs paternal grandmother.

==Cast ==
*Stephanie James as Leigh-Anne Williams Nathan Jones as Gavin Williams
*Brenda Blethyn as Annette (surname unknown)
*Marged Esli as a Social Worker
*Oliver Haden as Hassan Osman
*Sara Gregory as Julie Osman
*Dean Wong as Stephen Rajan
*Gary Sheppeard as Robbie Matthews
*Nicholas McGaughey as Terry Williams
*Amy Morgan as Karen Williams
*Lynsey France as a Housing Officer
*Phillip Howe as Jacob
*Victoria Pugh as a Social Worker
*Danielle Clare Symonds as a Barmaid

==Plot==
Leigh-Anne Williams is less than 20 years old and has already suffered the suicide of her mother when she was a child. Both Leigh-Anne and her brother Gavin were abused by their father, and they worry about their younger sister in her teens. Their father married their mothers sister and they have a young daughter.

Leigh-Anne has had a baby daughter Rebecca and is very protective toward her. The father is in prison (for an undisclosed reason.) She is visited by Annette, Rebeccas paternal grandmother, who suggests that Rebecca would be better off in her care.
 Turkish Islam|Muslim neighbour Hassan Osman, and is so desperate to support her baby that she will commit shocking acts. She tricks a man out of £30 by pretending to be a pimp. She gets a younger girl (under the age of consent) to have sex with the man, who is much older.

Three of the few people who have good relationships with Leigh-Anne are her brother Gavin, and Gavins friends Robbie and Stephen. But they are actively involved in crime and anti-social behaviour, and Leigh-Anne is a willing participant in many of their crimes. She receives regular visits from a social worker, and fears that Rebecca will be taken away from her. When Leigh-Anne sees her social worker talking with Hassan Osman, she fears that Osman is trying to get Rebecca "taken into care" by the state.

Leigh-Anne, Gavin, Robbie and Stephen are at a library one day when they come across Osman and his daughter Julie, an ex-girlfriend of Gavin. Julies father Osman effectively their relationship by his disapproval. An argument starts as they leave the centre, and it turns into a full-scale street fight in which the three boys attack Osman while Leigh-Anne looks on.

After Leigh-Anne returns home, she is visited by the police, who want to question her related to an attack on Osman resulting in his death from the beating. She leaves to go to the police station with them and sees her three friends being taken away, with Robbie still wearing the bloodstained jumper that he wore during the attack.

The last scene shows Leigh-Anne crying uncontrollably in police custody after her daughter has been transferred to Social Services. Her social worker says that no one was planning to remove her daughter, and she had been speaking to Osman about his own daughter Julies issues. Julie tells the police that she is pregnant with her brother Gavins baby.

==Awards== 2004 London Carl Foreman 2005 South 2005 Miami 2004 San Sebastian International Film Festival.

==See also ==
*Racism

==External links==
* 
*  
*  at The British Councils British Films Website

 
 
 
 
 
 