The Sixth Battalion
{{Infobox film
| name           = The Sixth Battalion
| image          =The Sixth Battalion9b.jpg
| image_size     =
| caption        = 
| director       = Mirek Pazdera / Howard E. Green
| producer       = Dušan Šimko / Mirek Pazdera
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Mirek Pazdera
| editing        = 
| distributor    =
| released       = 1998
| runtime        = 56 min.
| country        =  German (Original) English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Sixth Battalion is a 1998 documentary film that examines the history of Jewish soldiers who fought for the Slovak Republic, which was closely aligned with Nazi Germany during World War II. The documentary combines interviews with archival footage and photographs of the Slovak Republic in order to provide a brief history of the state, exploring the rise of antisemitism and how it affected these Jewish soldiers.

==Summary==
“Everywhere there were fleas and bugs. We slept in barns,” remembers one former soldier, “the work was very hard—I worked with a pick ax and shovel. The foreman  threatened that we would be sent away to Poland if we didnt achieve the quota.” Forced to take on grueling construction projects for the army, these Jews were treated as a lower class of soldiers who constantly faced the possibility of being deported to Nazi concentration camps.

In 1939, Adolf Hitler suggested that the Slovak Republic split from Czechoslovakia. After a unanimous vote in their Parliament of Slovakia|parliament, the Slovak Republic was formed.  But the new nation, small and impressionable, became a puppet for Nazi Germany.

The first Prime Minister, Jozef Tiso, spouted anti-Semitic statements that echoed Hitler. “Is it not humane if the Slovak people want to get rid of their perpetual enemy—the Jew? Is it not Christian?” his booming voice once asked. Answering his own question, he continued “The precept of love to thy self is God given, and this command for love for myself commands me to get rid of everything that is harmful to me—everything that threatens my life... We act according to Gods will. Slovak! Get rid of thy pest!”

Strong antisemitic propaganda was also spread in newspapers and magazines, paving the way for deportations and the systematic extermination of Eastern European Jewry during the Holocaust.

Then, young, able bodied Jewish men were forced to serve in the army. They were discriminated against and most of their service was spent building infrastructure for the state. “We took the pick axes and shovels we had cleaned the day before and went to dig the canal,” remembers one soldier, “it was a hard and exhausting job.”  Many of the bridges and streets they built are still in use.

The documentary shares their private stories. Often, instead of being applauded for good deeds, the soldiers were simply subject to more antisemitism. While off duty, one young soldier leaped into a river to save a girl from drowning. Once she was safe and on shore, a crowd formed around the two of them. When the mob began to whisper that he was a Jew, and grew suspicious of his association with the girl, the soldier became fearful for his own life, refused to disclose his name, and ran away.

The Jewish soldiers of the Sixth Battalion witnessed the atrocities of war from many angles. In addition to facing the grueling hardships of military life, they experienced prejudice themselves and watched helplessly as their friends and family members were deported. Their ability to work and remain useful to the state saved their own lives, but experiencing incredible inhumanity filled them with rage and sorrow.

==Reception==
 
==See also==
*Holocaust
*Slovak Republic
*History of the Jews in Russia and the Soviet Union
*History of antisemitism

Other documentaries about Jews during World War II:
*Marions Triumph
*Polas March
*A Story about a Bad Dream
*Chaim Rumkowski and the Jews of Lodz
*Goodbye Holland
*Paradise Camp
*Shadows of Memory
*The Boys of Buchenwald
*They Were Not Silent

==References==
{{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title = The Sixth Battalion
  | work =
  | publisher = Media Rights
  | date =
  | url =http://www.mediarights.org/film/the_sixth_battalion.php
  | accessdate =14 August }}

==External links==
* 

 
 
 
 
 
 
 
 