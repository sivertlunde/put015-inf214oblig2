Uyir
{{Infobox film
| name           = Uyir
| image          = 
| caption        =  Samy
| producer       = 
| writer         = Samy Srikanth Sangeetha Samvrutha Sunil
| music          = Joshua Sridhar
| cinematography = Fowzia Fathima 
| editing        = G. Sasikumar
| studio         = 
| distributor    = 
| released       =  
| country        = India
| language       = Tamil 
| runtime        = 
| budget         = 
| gross          = 
}}  Tamil drama film directed by Samy (director)|Samy. Starring Srikanth (actor)|Srikanth, Sangeetha and Samvrutha Sunil, the film revolves around a woman who falls in love with her brother-in-law after the demise of her husband. The film was released on June 30, 2006. The films music was composed by Joshua Sridhar. 

== Plot ==
Sundar (Srikanth) moves in with his brothers family, consisting of his brother, sister-in-law Arundathi (Sangeetha) and their daughter Aishwarya (Ramya). Sundar becomes very close to all three of them, but unbeknownst to him, Arundathi is interested in him. Sundar drops his niece off at school every day and meets his girlfriend Anandi (Samvritha) there. His brother is excited to learn about their relationship but soon afterward commits suicide. Arundathi claims that his suicide was caused by problems at work. Sundar becomes obligated to take care of his late brothers family, as Arundathi and Aishwarya have nowhere else to go. Arundathi tries her best to end Sundars relationship with Anandi while subtly hinting to Sundar about her love for him. Anandi tries to warn Sundar about his conniving sister-in-law, but Arundathi succeeds in her plan. Did Sundar get together with Anandi or not is rest of the plot.

== Cast == Srikanth as Sundar
*Sangeetha as Arundathi
*Samvritha as Anandhi
*Ganja Karuppu
*Baby Ramya as Aishwarya
*Asim Sharma as Sundar brother
*Balaji

==Release==
The satellite rights of the film were sold to Raj TV. The film was given a "U/A" certificate by the Indian Censor Board.

== References ==
 

 
 
 
 
 
 


 
 