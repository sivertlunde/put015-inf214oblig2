The Life of Larry and Larry & Steve
 
  animated short animated sitcom World Premiere Toons in 1997.
 Fox saw first season of Family Guy. MacFarlane was offered a $2 million per-season contract.   

==The Life of Larry (1995)==
{{Infobox film
| italic title=no
| name           = The Life of Larry
| image          = The Life of Larry title card.png
| caption        = Title card for The Life of Larry.
| director       = Seth MacFarlane
| producer       = Seth MacFarlane
| writer         = Seth MacFarlane
| starring       = Seth MacFarlane Chang S. Han
| music          = Seth MacFarlane
| editing        = Seth MacFarlane
| sound engineer = Greg J. Scalzo
| studio         = Rhode Island School of Design Microscope Films
| distributor    = Rhode Island School of Design
| released       = 1995
| runtime        = 11 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Larry Cummings, his cynical talking dog, Brian Griffin|Steve, supportive wife Lois Griffin|Lois, and pudgy teenage son Chris Griffin|Milt. The film also features live-action segments shot at MacFarlanes home in Kent, Connecticut where he describes the film and its characters in the form of a pitch to a television network. During the live action segments, MacFarlane is being served cheesecake by his Asian servant, Wang, played by fellow student Chang S. Han.

The Life of Larry was created as MacFarlanes thesis film during 1994 and 1995, while he was studying at the Rhode Island School of Design.   The animated film was created almost entirely by MacFarlane alone, with the exception being the live action sequence, which was filmed by classmates Sean Leahy and Greg Scalzo. Many of the jokes and cutaway gags used in The Life of Larry were later used in many of the first season Family Guy episodes.

==Larry & Steve (1997)==
{{Infobox film italic title=no
| name           = Larry & Steve
| image          = Larry & Steve titlecard.png
| caption        = Title card for Larry & Steve.
| director       = Seth MacFarlane
| producer       = Davis Doi   Victoria McCollum   Executive producer: Larry Huber Sherry Gunther
| writer         = Seth MacFarlane
| starring       = Seth MacFarlane Lori Alan Ron Jones Gary Lionelli   Bodie Chandler  
| editing        = Pat Foley Paul Douglas  
| sound engineer = Preston Oliver Ed Collins  
| studio         = Cartoon Network Studios Hanna-Barbera|Hanna-Barbara Cartoons
| distributor    = Cartoon Network
| released       = February 5, 1997
| runtime        = 7 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
While MacFarlane was working for Hanna-Barbera|Hanna-Barbera Studios, and Bigbc studios working on shows such as Johnny Bravo, Dexters Laboratory, I Am Weasel, and Cow and Chicken,  he made a sequel of sorts, to The Life of Larry, which Cartoon Network broadcast in 1997 as part of their World Premiere Toons series.
 Peter Griffins infant son. The short was rated TV-Y7 for cartoonish slapstick violence, comic peril, and mild adult references.

==Relationship with Family Guy==
In basic form, The Life of Larry is very similar in format to Family Guy. Steve would be the main inspiration behind Brian.  MacFarlane based Peters voice, which was similar to Larrys,    on the voice of a security guard he once overheard talking while he was attending the RISD.  While Larry and Peters wives share the same name, they do not resemble one another. Larrys son Milt, by contrast, harbors a basic design similarity to Peters son Chris. The pilot of the plane that crashes in the store in Larry & Steve has a chin and voice that are both similar to Quagmires, who is also a pilot. A bit character from both films, Shelley Boothbishop, made his way into the Family Guy episode "Long John Peter".

==See also==
 
* Family Guy
* The Cartoon Cartoon Show|What-A-Cartoon!

==References==
 

==External links==
*    at Google Videos
*    at Google Videos

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 