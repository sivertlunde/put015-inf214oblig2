Muzzy Comes Back
 
{{Infobox television film
| bgcolour = 
| name = Muzzy Comes Back (Muzzy 2)
| image = Muzzy.png
| caption = Muzzy
| format = Animation
| runtime =
| creator = Richard Taylor
| producer = Richard Taylor Cartoon Films
| writer = Wendy Harris Richard Taylor Joe Hambrook Peter Jones
| music = Peter Shade
| country =   United Kingdom
| language = English
| network = BBC
| released = 15 May 1989 (UK)
| first_aired =
| last_aired =
| num_episodes =
| preceded_by = Muzzy in Gondoland
| followed_by =
}}

Muzzy Comes Back (also known as Muzzy 2) is a sequel to the animated TV film Muzzy in Gondoland, created by the BBC in 1989 as a way of teaching English as a second language.

==Plot==
Muzzy, the friendly green clock-eating monster from outer space, returns for more adventures with his friends in Gondoland. Everyone expects him, and King Nigel and Queen Ezra decide to arrange a ball for the christening of Bob and Sylvias daughter Amanda.   Meanwhile, servant Corvax invents an invisibility device and, with his assistant named Thimbo (the Terrible), a thief on parole, plans to kidnap Amanda, to avenge Bob for tricking him and taking Sylvia away from him.

Corvax and Thimbo succeed in kidnapping Amanda and they get away on Nigels yacht. However, Amanda sinks the boat by pulling the plug out of the hull. The three manage to survive and they go to Corvaxs hideout – a small hut which in fact turns out to be Corvaxs operations centre. Meanwhile, Muzzy, Nigel, Ezra, Bob and Sylvia follow Corvaxs trail, recover Nigels yacht and track Corvax to his hideout.

At the same time, Corvax and Thimbo have a problem: Amanda gets hungry and wants to eat; while preparing the food, Thimbo lets the invisibility device loose and it is picked up by Amanda, who activates it. Corvax and Thimbo try to find Amanda but fail. Muzzy, Nigel, Bob, Ezra and Sylvia enter the hideout and confront Corvax. Thimbo commits treason and reveals to Nigel that this hut is an operations center. Meanwhile Amanda is found but she cant be seen. Muzzy constructs another invisibility device and uses it to make Amanda visible. All then go back to the palace. Corvax and his coward friend Thimbo ride with sheep, to receive the ultimate punishment, which in Britain is 250 years in prison.

==Characters== extraterrestrial – played by Jack May
* King Nigel – a lion, Sylvias father, married to queen Erza – played by Willie Rushton
* Queen Erza – a rat, Sylvias mother, married to king Nigel – played by Miriam Margolyes
* Princess Sylvia – a rat, Bobs wife – played by Susan Sheridan
* Bob – a mouse, Sylvias husband – played by Derek Griffiths
* Amanda – Bob and Sylvias daughter – a rat – played by Susan Sheridan
* Corvax – a green goblin, a villain, has a crush on Sylvia – played by Derek Griffiths Peter Jones
* Norman – a human – played by Benjamin Whitrow
* Mary – a human, Normans Wife – played by Miriam Margolyes
* The Lady Guest – a kangaroo – played by Susan Sheridan Peter Jones
* Guest 2 – a rat – played by Derek Griffiths old English sheepdog – played by Willie Rushton
* Guard 2 – a beagle – played by Derek Griffiths
* The Servant Maid – a bulldog – played by Miriam Margolyes
* The Office Boy – a rat – played by Susan Sheridan Peter Jones
* The Shopkeeper – a human – played by Derek Griffiths
* The Policeman, the Clock-Maker and Normans Friend – humans – played by Willie Rushton
* The Boy and the Girl – human children – played by Susan Sheridan

==Credits==
* Directed by: Richard Taylor
* Executive Producer: Joe Hambrook
* Original English Scripts: Wendy Harris, Joe Hambrook, Richard Taylor
* Language Course Designer: Diana Webster
* Animation: Richard Taylor Cartoon Films Ltd.
* Additional Animation: Bob Godfrey Films Ltd.
* Music by: Peter Shade
* Produced by Richard Taylor Cartoon Films for BBC English by Television

==External links==
*  
* BBC Muzzy —  
* Early Advantage –  

 
 
 
 
 
 
 
 