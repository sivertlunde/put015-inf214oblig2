Lekhu Lekhu Lekhi Deli
{{Infobox film
| name = Lekhu Lekhu Lekhi Deli
| image = 
| director = Susanta Mani
| producer = Sitaram Agrawal
| screenplay = Srikant Goutam
| story = Siddharth 
| cinematography = Sitanshu Mahapatro
| starring = Babushan Jhilik Aparajita Mohanty Harihara Mohapatra Mahasweta Ray
| studio = Sarthak Entertainment Pvt Ltd
| released =  
| country = India
| language = Oriya
}}
Lekhu Lekhu Lekhi Deli( ) is a 2014 Oriya romance comedy-drama film produced by Sitaram Agrawal (Sarthak Film). Babushan and Jhilik are in the lead roles for the second time in a romantic role after Akhire Akhire.

==Cast==

* Babushan
* Jhilik Bhattacharya
* Mahasweta Ray
* Satyaki Mishra
* Prithiviraj Nayak
* Aparajita Mohanty
* Harihara Mohapatra
* Priyanka
* Ankita Bhowmick (In item song)
* Nicky & Adyasha (Child Artist)

==Music==
This film has tracks composed by Bikash Das and music arrangement by Prafulla Ch Behera.

* Music Director - Bikash Das
* Music Arrangement - Prafulla Chandra Behera
* Male Vocals - Udit Narayan, Babul Supriyo,Bibhu kishore, Babushan
* Female Vocals - Asima Panda

== References ==
 

 
 
 


 