Manos: The Hands of Fate
 
{{Infobox film
| name = Manos: The Hands of Fate
| image = Manosposter.jpg
| image_size = 215px
| alt = Poster for film showing a gripping hand in the foreground, and a flame between a woman on the left and apparently the same woman on the left. The top of the poster has the word "shocking" in large letters.
| caption = Film poster
| director = Harold P. Warren
| producer = Harold P. Warren
| writer = Harold P. Warren
| starring = Harold P. Warren Diane Mahree Jackie Neyman Tom Neyman John Reynolds
| music = Ross Huddleston Robert Smith Jr.
| cinematography = Robert Guidry
| editing = James Sullivan
| studio = Sun City Films
| distributor = Emerson Film Enterprises
| released =  
| runtime = 74 minutes
| country = United States
| language = English
| budget = United States dollar|$19,000
| box office = United States dollar|$0
}}
Manos: The Hands of Fate is a 1966 American  . 
 polygamous pagan cult, and they attempt to escape as the cults members decide what to do with them. The film is infamous for its technical deficiencies, especially its significant editing and continuity flaws; its soundtrack and visuals not being synchronized; tedious pacing; abysmal acting; and several scenes that are seemingly inexplicable or disconnected from the overall plot, such as a couple making out in a car or The Masters wives breaking out in catfights.   

Warren was an insurance and fertilizer salesman from El Paso, Texas, who produced the film as the result of a bet. He also starred in it, alongside El Paso theater actors Tom Neyman and John Reynolds. Manos was an independent production by a crew with little or no background or experience in filmmaking and a very limited budget at its disposal. Upon its theatrical debut, the film was poorly received, playing only at the Capri Theater in El Paso and some drive-ins in West Texas and New Mexico. It remained obscure until its Mystery Science Theater appearance, which sparked two DVD releases (the original film and the three separate releases of DVDs featuring the MST3K episode of the film). 

==Plot==
While on a road trip near El Paso, Texas, Michael, Margaret, their young daughter Debbie, and their dog, Peppy, search for the "Valley Lodge." Michael and his family finally reach a house which is tended by the bizarre, satyr-like Torgo, who takes care of the house "while the Master is away." Michael and Margaret ask Torgo for directions to Valley Lodge; Torgo simply replies that there is no place like that around here. With this information, Michael asks Torgo to let him and his family stay the night, despite objections from both Torgo and Margaret.

Inside the home, the family sees a disturbing painting of a dark, malevolent-looking man and a black dog with glowing eyes; the man it depicts is the Master. Margaret becomes frightened upon hearing an ominous howl; Michael investigates, retrieving a flashlight and revolver from his car, and later finds Peppy lying dead on the ground. Torgo reveals his attraction to Margaret and tells her that, although she is doomed to become yet another bride of The Master, he intends to keep her for himself. Margaret threatens to tell Michael of Torgos advances, but Torgo convinces her not to say anything to her husband by promising to protect her. Michael returns, unable to start the car. With the revelation that there is no phone in the house, the family reluctantly decides to stay the night.

Michael and Margaret stumble upon "The Master" and several women dressed in translucent nightgowns and later revealed to be his wives. They are all asleep. Torgo uses a stick to knock out Michael, and then ties Michael to a pole, after dragging him to it, and The Master suddenly comes to life. His wives also awake, and a short argument over the fate of the family ensues. The Master decides he must sacrifice Torgo and his first wife to the films mysterious deity and namesake, "Manos." When The Master leaves, his wives engage in further argument that soon degenerates into a fight, and the women wrestle in the sand.
 hypnotic spell by The Master. The Master stops the fight, and has his first wife tied to a pole to be human sacrifice|sacrificed. Torgo is laid on a stone bed, where he is attacked by The Masters other wives, but this in itself does not prove fatal. Evoking some mysterious power, The Master severs and horribly burns Torgos left hand. Torgo runs off into the darkness, waving the burning stump that remains. The Master laughs maniacally and goes to look for the family and subsequently sacrifices his first wife.

The family run off into the desert. When a rattlesnake appears in front of them, Michael shoots it, attracting the attention of nearby deputies. Margaret and Michael are later convinced to return to the Masters house, where the Master welcomes them. Michael fires several shots into The Masters face at point-blank range, but they have no effect. The screen fades to black, likely indicating that The Master has again applied his hypnotic power.

An undisclosed amount of time later, an entranced Michael greets two new lost travelers. Margaret and Debbie have become wives of The Master. The film concludes with Michael echoing Torgos line of "I take care of the place while the Master is away." The production credits are superimposed over past scenes from the film with the words: "The End?"

==Cast==
 
* Harold P. Warren as Michael
* Diane Mahree as Margaret
* Jackey Neyman as Debbie
* John Reynolds as Torgo
* Tom Neyman as The Master
* Stephanie Nielson, Sherry Proctor, Robin Redd, Jay Hall, Bettie Burns, and Lelaine Hansard as the Masters wives
* Bernie Rosenblum as Teenage boy
* Joyce Molleur as Teenage girl
* William Bryan Jennings and George Cavender as Cops
* Pat Coburn as Girl in convertible

==Production==
  and actor John Reynolds wore the metal rigging backwards under his trousers.]]  Route 66, where he met screenwriter Stirling Silliphant. While chatting with Silliphant in a local coffee shop, Warren claimed that it was not difficult to make a horror film, and bet Silliphant that he could make an entire film on his own. After placing the bet, Warren began the first outline of his script on a napkin, right inside the coffee shop.  To finance the film, Warren accumulated a substantial, but nevertheless insufficient, $19,000 cash (equivalent to $ |r=0}}}} in   dollars), and hired a group of actors from a local theater, many of whom he had worked with before, as well as a modeling agency.  Because he was unable to pay the cast and crew any wages, Warren promised them a share in the films profits.      
 El Paso County. Most of the equipment used for production was rented, so Warren had to rush through as many shots as possible to complete filming before the deadline for returning the equipment.  Footage was shot with a 16 mm film|16&nbsp;mm Bell & Howell camera which had to be wound by hand and thus could only take 32 seconds of footage at a time.  Albert Walker of agonybooth.com believes this is the source of the many editing problems present in the final cut.  Rather than using double-system recording, all sound effects and dialogue were dubbed later in post-production, done by only a handful of people, including Warren, Tom Neyman, and Warrens wife, Norma.     Later during production, Warren renamed the film from its working title to Manos: The Hands of Fate.  What makes this title a discrepancy is that "manos" is Spanish for "hands", which means that the title literally translates to Hands: The Hands of Fate. Warrens small crew became so bemused by his amateurishness and irascibility that they derisively called the film Mangos: The Cans of Fruit behind his back.  

During filming, Warren knew that presenting Diane Mahree as the Texas Beauty Queen would generate good publicity for his movie. He signed Mahree up for a regional West Texas beauty pageant that would lead to Miss Texas and then to the Miss America pageant, but he neglected to tell her about it until she was accepted as an entrant. She went along with it, and soon found herself onstage as one of the finalists.

Warren contracted with a modeling agency to provide the actresses who would play the Masters wives, including Joyce Molleur. Molleur broke her foot early in production, so to keep her in the film, Warren rewrote the script to include a young couple making out in a car on the side of the road who are seemingly completely incidental to the films plot.  

 
 cloven hooves should also have been part of Reynolds satyr costume, but he is instead clearly shown wearing boots in several scenes, which can even be seen in the Mystery Science Theater 3000 version. The films dialogue never mentions Torgos satyr nature, and none of the characters seem to notice anything unusual about his appearance. 

Warren decided to shoot night-for-night scenes, because many of the cast and crew also held day jobs.  In many of the night scenes, the camera and lights attracted swarms of moths, which can be seen in the films final production.     In the scene in which the cops "investigate" Mikes gunfire, they could walk only a few feet forward, because there was not enough light to illuminate the scenery for a panning shot,  creating the unintentionally amusing impression that the officers hear the gunfire, step out of their car, consider investigating but then give up and leave before making a proper check of the scene.

  over these shots, but either forgot to add them or did not have the post-production budget to do so. 

John Reynolds, the actor who played Torgo, committed suicide by shooting himself in the head with a shotgun on October 16, 1966,  a month before the film was to premiere.  Reynolds was 25; Manos would be his first (and only) film appearance.

{{listen
|filename=Haunting Torgo Suite.ogg
|title=Torgo Suite
|description= A brief arrangement of the "haunting" Torgo theme from the film, by Ross Huddleston and Robert Smith, Jr.}}

==Reception==
The film premiered at the Capri Theater in Warrens hometown of El Paso, Texas on November 15, 1966 as a benefit for the local cerebral palsy fund.     Warren arranged for a searchlight to be used at the cinema,  and for the cast to be brought to the premiere by a limousine, in order to enhance the Hollywood feel of the event. Warren could afford only a single limousine, however, and so the driver had to drop off one group, then drive around the block and pick up another.    Jackey Neyman-Jones, who played Debbie and was 7 years old at the time, remembered weeping in disappointment at the premiere, particularly when another womans (dubbed) voice came out of her mouth onscreen.  The following day, a review of the film was featured in the El Paso Herald-Post, which described the film as a "brave experiment." It criticized some elements, such as the attempted murder of Torgo by being "massaged to death" by The Masters wives, and Margarets claim of "Its getting dark" while she stands in front of a bright midday sun.  The review nonetheless noted Reynolds screen presence by crediting him as the films "hero".
 Las Cruces.     Reports that the only crew members who were compensated for their work in the film were Jackey Neyman and her familys dog, who received a bicycle and a large quantity of dog food, respectively, would seem to indicate that even with its extremely low budget, the film failed to break even financially.   Official box office figures for the film are unknown, if indeed they ever existed. Although the film received poor reception, Warren did win his bet against Stirling Silliphant, proving that he was capable of creating an entire film on his own. 

The majority of the cast and crew never appeared in another film following Manos. Warren attempted to pitch another script he had written called Wild Desert Bikers, but with the failure of Manos, no one he approached showed any interest in producing it.  Attempts to turn the screenplay into a novel were equally unsuccessful. 

===Obscurity===
Following these few local screenings, Manos was almost entirely forgotten. When Jackey Neyman attended University of California, Berkeley, her friends unsuccessfully made an effort to track down a copy of the film.  A 1981 newspaper article by cinematographer Bob Guidrys ex-wife Pat Ellis Taylor  reports the film may have appeared on a local television station, and that it was "listed at the bottom of a page in a film catalogue for rent for $20."  The film re-surfaced through a 16&nbsp;mm print, presumably from this television package, which was introduced into the home video collecting market by a number of public domain film suppliers. One of these suppliers was ultimately the one that offered the film to Comedy Central, after which it found its way into a box of films sent to Frank Conniff in 1992, when he chose Manos as one of the films to be shown on Mystery Science Theater 3000. 

===Mystery Science Theater 3000 and RiffTrax===
  chose Manos to be featured on Mystery Science Theater 3000 in 1992.]]
 Joel and Mike Nelson, Operation Double Samson vs. The Vampire Women, when he appeared as "Torgo the White" to bring Frank to "Second Banana Heaven"    Both Forrester and Frank were shown apologizing for showing the film, which they admitted was abysmal and went beyond even their acceptable limits for torturing Joel and the bots. 

During a Q&A session at the 2008 San Diego Comic-Con International, a question was put to the cast and writers of MST3K about any film they passed on that was worse than Manos, and many cited the film Child Bride. 

The Manos episode has been described as one of the best of the MST3K series by Entertainment Weekly.   
 Kevin Murphy were part of the cast when MST3K riffed on Manos, neither their fellow Rifftrax star Bill Corbett nor their writers were involved in the original episode; the riffing was expected to be all new. This live version used a cleaner print of the original Manos, allowing them to joke on things not obvious in the original television episode. 

===Popularity as a cult film===
 
 

The MST3K episode featuring the film was released on DVD on its own in 2001, and in the Mystery Science Theater 3000 Essentials collection in 2004.    

A DVD of the original version of Manos has also been made available through   released a special edition of the film which includes both the MST3K and uncut versions called Manos y Manos  .   

Manos holds a 0% rating on Rotten Tomatoes based on 11 reviews.    The book Hollywoods Most Wanted lists Manos as the #2 in the list of "The Worst Movies Ever Made," following Plan 9 from Outer Space.   Entertainment Weekly proclaimed Manos "The Worst Movie Ever Made."  The scene in which the seven-year-old Debbie is dressed as one of the Masters wives was included in a list of "The Most Disgusting Things Weve Ever Seen" by the Mystery Science Theater 3000 crew.   

Four comedy stage adaptations of the film have been made. The first, by Last Rites Productions, was given in   in October 2007.  The third, a puppet musical titled Manos&nbsp;– The Hands of Felt, was performed by Puppet This in Seattle in April 2011.  The fourth, by Capital I Productions, took place in Portland, Oregon in April 2013. 

In March 2008, the How I Met Your Mother episode "Ten Sessions" featured main character Ted Mosby arguing that Manos is the worst film ever made, even when compared to Plan 9 from Outer Space. The show featured a brief discussion of the film, and a condensed, 12-second screening of the film as part of a two-minute date. 

In November 2008, a 27-minute documentary film about Manos was released on DVD, titled Hotel Torgo. 

In March 2015, the murderers on the Elementary (TV series)|Elementary episode "List of Elementary episodes|T-Bone And The Iceman" used the physical features of Torgo (portrayed by John Reynolds) to compose a fake facial composite to get the NYPD off their trail. It worked for a while before they were caught, due to the character of Dr. Joan Watson having recognized Torgos features from the film. The film’s editor, James Sullivan, was the namesake of one of the characters in the episode.   

==Restoration==
In 2011, the original   Manos for a  , Retrieved 2012-08-13  The new restoration will be released on Blu-Ray in 2015. 

==Video game adaptation==
  Android port were later released.

==See also==
* List of films considered the worst
* Z movie

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  , in which she shares some of her stories about the making of the film
*  

===Mystery Science Theater 3000===
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 