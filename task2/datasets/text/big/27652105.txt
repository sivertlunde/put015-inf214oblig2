Loose in London
{{Infobox film
| name           = Loose in London
| image          =
| caption        =
| director       = Edward Bernds
| producer       = Ben Schwalb
| writer         = Elwood Ullman Edward Bernds
| narrator       =
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey
| music          = Marlin Skiles
| cinematography = Harry Neumann
| editing        = Allan K. Wood Allied Artists
| distributor    = Allied Artists
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Allied Artists and is the thirtieth film in the series.

==Plot==
Sach receives notice that a dying British earl is his long-lost relative.  He travels there with the rest of the gang after exchanging his free first-class ticket for four economy class tickets.  Meanwhile, Louie, who is on board to say goodbye, gets locked in a closet and becomes a stowaway.  When the boys arrive in London they are treated with disdain from the earls other relatives, who are secretly plotting to kill the earl.  Sach livens up the earl by telling him to eat ice cream instead of his medicine, and generally making him laugh.  The earls health begins to improve and he decides to make Sach is sole heir.  The other relatives decide they need to kill off the earl immediately, but are foiled by the boys.  Unfortunately just before the earl is to sign the paperwork making Sach the heir, his lawyer arrives and informs them that he made an error and Sach isnt really his relative. 

==Production==
The film was originally planned to be produced in 1950, under then title Knights of the Square Table.   This is the first film in the series where Chuck and Butch are given last names, Anderson and Williams, respectively. 

==Cast==
===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Chuck Anderson (Credited as David Condon)
*Bennie Bartlett as Butch Williams

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski 
*Walter Kingsford as Sir Percy, Earl of Walsingham
*Norma Varden as Aunt Agatha Angela Greene as Lady Marcia Matthew Boulton as Ames

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Two" on April 9, 2013.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958 Jalopy 1953 Clipped Wings 1953}}
 

 

 
 
 
 
 
 
 