Thiruvannamalai (film)
{{Infobox film
| name = Thiruvannamalai
| image = 
| director = Perarasu
| writer = Perarasu Saikumar Vaiyapuri Vidharth
| producer =  Pushpa Kandasamy
| music = Srikanth Deva
| cinematography = Padmesh
| editing        = V. Jaishankar
| distributor = Ayngaran International
| released =  
| runtime = 
| studio = Kavithalayaa Productions
| country = India
| language = Tamil
| budget = 
}}
Thiruvannamalai is a Tamil language action film released in 2008. It stars Arjun Sarja, Pooja Gandhi and Karunaas in the lead roles. The film was a below-average grosser.  The film was later dubbed in Hindi as Main Hoon Vinashak.

==Plot== MLA (Sai Kumar) after he exposes his corrupt and greedy ways through his cable TV channel. Fearing trouble, his mom takes him to a saint in Thiruvannamalai. The Swami resembles Easwaran. A sequence of events forces them to swap places. The soft-spoken Swami tries to sort all issues through non-violent means (Gandhian philosophy). Half way through Karunas dies in the hand of (Vidharth) and the corrupt MLA. In the climax Easwaran kills off the MLA.

==Cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Arjun Sarja || Easwaran/Swamiji
|-
| Pooja Gandhi || Malathy
|-
| Karunas ||
|-
| Vidharth ||
|-
|}and, Sai Kumar (telugu actor)

==Production==
Immediately after the release of Pazhani (film)|Pazhani in January 2008, Perarasu announced that he would make another action film starring Bharath titled Thiruthani.  However the actors commitment to V. Z. Durai|Durais Nepali (film)|Nepali and Venkateshs Killadi meant that he was unable to start the project at the time, so Perarasu announced his next project Thiruvannamalai with Arjun Sarja.    

Sanya Vakil was selected to play the lead heroine but she opted out  instead Pooja Gandhi was selected. 

During the shoot, Arjun Sarja and Perarasu had misunderstanding but later crew sorted out the problems. 

==Reviews==
Indiaglitz wrote: "Though the screenplay and script are quick-paced, several sequences sans logic fails to attract attention".  Behindwoods wrote: " Thiruvannamalai is a full length Arjun action show in true Perarasu style".  Kollywoodtoday wrote: "On the whole, it’s a ridiculous film to watch for   audiences who are fond of watching avant-garde filmmakers".  Sify wrote: "The plodding plot by Perarasu is a rehash of so many films and is as stale as day before yesterday’s Sambar!
It follows the hoary formula of all mass masalas, and there is not even one scene or dialogue which is original".  Cinefundas wrote: "On the whole, Perarasu makes you feel that he has done something different on the interval where a great challenge is established between two characters. But with everyone anticipating that the screenplay would be sans stunts, it’s completely letting down us with a regretful work". 

==Box office==
The film was a below-average grosser.

==References==
 

 
 

 
 
 
 
 
 
 