The Starlit Garden
{{Infobox film
| name           = The Starlit Garden
| image          =
| caption        =
| director       = Guy Newall  George Clark
| writer         = Henry De Vere Stacpoole (novel)   Guy Newall
| starring       = Guy Newall   Ivy Duke   Valia Venitshaya   Lawford Davidson
| music          = 
| cinematography = 
| editing        = 
| studio         = George Clark Productions
| distributor    = Stoll Pictures
| released       = July 1923
| runtime        = 6,400 feet  
| country        = United Kingdom 
| language       = Silent   English intertitles
| budget         = 
}} British silent silent romantic ward and her guardian.  It was made at Beaconsfield Studios. 

==Cast==
* Guy Newall as Richard Pinckney 
* Ivy Duke as Phyllis Berknowles 
* Valia Venitshaya as Frances Blett 
* A. Bromley Davenport as Colonel Grangerson 
* Lawford Davidson as Silas Grangerson 
* Mary Rorke as Aunt Maria 
* Cecil Morton York as Hennessey 
* John Alexander as Rafferty 
* Marie Ault as Old Prue

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.
* Warren, Patricia. British Film Studios: An Illustrated History. Batsford, 2001.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 