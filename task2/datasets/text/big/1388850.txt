The Falcon and the Snowman
{{Infobox film
| name           = The Falcon and the Snowman
| image          = Falcon and the snowman ver3.jpg
| caption        = Promotional movie poster for the film
| director       = John Schlesinger
| producer       = Gabriel Katzka John Schlesinger
| screenplay     = Steven Zaillian Robert Lindsey
| starring = {{plainlist|
* Timothy Hutton
* Sean Penn
}}
| music          = Lyle Mays Pat Metheny
| cinematography = Allen Daviau
| editing        = Richard Marden
| studio         = Hemdale Film Corporation
| distributor    = Orion Pictures
| released       =  
| runtime        = 131 min.
| country        = United States English
| budget         = $7 million
| gross          = $17,130,087
}}
The Falcon and the Snowman is a 1985 spy film directed by  , and features the song "This Is Not America", written and performed by David Bowie and the Pat Metheny Group.

==Plot synopsis==
The Falcon and the Snowman is based on the true story of former altar boy and former Catholic seminary student Christopher Boyce and drug dealer Andrew Daulton Lee, two young men from wealthy California families who sold classified government information to the Soviet Union during the mid-1970s.
 the CIAs depose the Prime Minister of Australia.  Frustrated by this duplicity, Boyce decides to repay his government by passing classified secrets to the Soviets.

Lee is a drug addict and minor smuggler, sometimes called "the Snowman" (in reference to his cocaine sales), who has frustrated and alienated his family. Lee agrees to actually contact and deal with the KGBs agents in Mexico on Boyces behalf, motivated not by idealism but by what he perceives as an opportunity to make money, then eventually settle in his idea of paradise, Costa Rica.

As the pair become increasingly involved with espionage, Lees ambition to create a major espionage business coupled with his excessive drug use began alienating the two from each other. Alex, their Soviet handler, becomes increasingly reluctant to deal through Lee as the middleman because of Lees periods of irrationality.  Boyce wants to end the espionage so that he can resume a normal life with his girlfriend Lana and attend college. He meets with Lees KGB handler to explain the situation.

Lee is desperate to regain the Soviets regard after realizing that the KGB no longer needs him as a courier now that they have direct contact with Boyce. Lee is observed tossing a note over the fence at the Soviet embassy in Mexico City. He is arrested by Mexican police, and a U.S. Foreign Services officer accompanies him to the police station.

When the police search his pockets and find film (from a Minox spy camera Boyce used to photograph documents) and a postcard (used by the Soviets to show the haphazard Lee the location of a drop zone), they produce pictures of the same location that was on the postcard, showing officers surrounding a dead man on the street. The Foreign Services officer explains that the Mexican police are trying to implicate him with the murder of a policeman.  The police drag away Lee and torture him.

Hours later, he reveals that he is a Soviet spy ... the real reason the police had been ordered to detain him.  Told by the Mexican police that he will be deported, Lee is offered a choice of where to be sent.  Lee suggests Costa Rica, but the choice is merely between the Soviet Union and the United States. Lee reluctantly agrees to go back to America and is arrested as he walks across the border.

Knowing that he too will soon be captured, Boyce releases his pet falcon named "Fawkes" in a field and then sits down to wait.  Moments later, U.S. Marshals and FBI agents surround and capture him. The movie ends with both Lee and Boyce in prisoner jumpsuits and shackles, flanked on either side by officers escorting them to prison.

==Cast==
* Timothy Hutton as Christopher Boyce
* Sean Penn as Andrew Daulton Lee
* Pat Hingle as Mr. Boyce
* Joyce Van Patten as Mrs. Boyce
* Rob Reed as Boyce child
* Richard Dysart as Dr. Lee
* Priscilla Pointer as Mrs. Lee
* Chris Makepeace as David Lee
* Dorian Harewood as Gene
* Mady Kaplan as Laurie
* Macon McCalman as Larry Rogers
* Jerry Hardin as Tony Owens
* Lori Singer as Lana
* David Suchet as Alex 
* Burke Byrnes as U.S. Customs Official

==Critical reception==

The Falcon and The Snowman received generally positive notices upon release in 1985 and currently has 87% on Rotten Tomatoes. Film critic Roger Ebert gave it a perfect four-star rating, citing one of the many strengths as that "it succeeds, in an admirably matter-of-fact way, in showing us exactly how these two young men got in way over their heads. This is a movie about spies, but it is not a thriller in any routine sense of the word. Its just the meticulously observant record of how naiveté, inexperience, misplaced idealism and greed led to one of the most peculiar cases of treason in American history."

==HBO Satellite Jamming== Captain Midnight.

==Music==
 

==External links==
* 
* 
* 
* 
* , the Peregrine Falcon used in the movie.
* 

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 