His Double Life
{{Infobox film
| name           = His Double Life
| image size     = 
| image	=	His Double Life FilmPoster.jpeg
| caption        = His Double Life (1933) Movie Poster.
| director       = Arthur Hopkins William C. deMille(uncredited)
| producer       = Eddie Dowling Arthur Hopkins
| based on       =  
| writer         = Clara Beranger Arthur Hopkins
| narrator       = 
| starring       = Roland Young Lillian Gish
| music          = James F. Hanley Karl Stark John Rochetti
| cinematography = Arthur Edeson
| editing        = Arthur Ellis A. Pam Blumenthal
| distributor    = Paramount Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 The Great Holy Matrimony. It is preserved at the Library of Congress, Washington D.C.  and available on DVD.

==Plot==
Priam Farrel Roland Young is Englands most famous living painter. A recluse who hates fame, he has not been seen by anyone, for years, not even his agent, or cousin. He is glad to be mistaken for his valet by everyone, including his cousin, when he returns to England. In this case of mistaken identity, hes happy to live a quiet country life with his manservants mail order bride Lillian Gish. Until, he gets hauled into court for bigamy and fraud.

==Cast==
*Roland Young as Priam Farrel
*Lillian Gish as Alice Chalice
*Montagu Love as Duncan Farrel
*Lumsden Hare as Oxford
*Lucy Beaumont as Mrs. Leek Charles Richman as Witt
*Oliver Smith as Leek Twin - John
*Philip Tonge as Leek Twin - Henry
*Audrey Ridgewell as Lady Helen
*Regina DeValet as Mary

==Soundtrack==
*"Someday, Sometime, Somewhere" (Written by James F. Hanley and Karl Stark)
*"Springtime in Old Granada" (Written by James F. Hanley and Karl Stark)

==See also==
* Lillian Gish filmography

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 