Bariera
Barrier Polish drama film directed by Jerzy Skolimowski, released in 1966 in film|1966.

== Details ==
* Title : Barrier
* Original title: Bariera
* Director: Jerzy Skolimowski
* Writer: Jerzy Skolimowski
* Country of origin: Poland Mono - 35 mm
* Genre : Drama
* Length: 77 minutes 1966

== Cast ==
* Joanna Szczerbic : Tram conductor
* Jan Nowicki : Protagonist
* Tadeusz Lomnicki : Doctor
* Maria Malicka : Businesswoman
* Zdzislaw Maklakiewicz : Paper seller
* Ryszard Pietruski : Oberwaiter
* Bogdan Baer : Man at bar
* Henryk Bak : Doctor
* Stefan Friedmann : Tram conductor asking for help
* Andrzej Herder : Manius
* Malgorzata Lorentowicz : Owner of the apartment
* Zygmunt Malanowicz : Eddy

== External links ==
*  

 

 

 
 
 
 
 
 

 