A Somewhat Gentle Man
 
 
{{Infobox film
| name           = A Somewhat Gentle Man
| image          = En ganske snill mann.jpg
| caption        = Norwegian theatrical release poster
| director       = Hans Petter Moland
| producer       =  
| writer         = Kim Fupz Aakeson
| starring       = Stellan Skarsgård
| music          = 
| cinematography = Philip Øgaard
| editing        = Jens Christian Fodstad
| distributor    = 
| released       =  
| runtime        = 
| country        = Norway
| language       = Norwegian
| budget         = 
}}
A Somewhat Gentle Man (  and also known as Regnskap) is a 2010 Norwegian comedy film directed by Hans Petter Moland. It was nominated for the Golden Bear at the 60th Berlin International Film Festival.   

==Cast==
* Stellan Skarsgård as Ulrik
* Bjørn Floberg as Rune Jensen
* Gard B. Eidsvold as Rolf
* Jorunn Kjellsby as Karen Margrethe
* Bjørn Sundquist as Sven
* Jon Øigarden as Kristian
* Kjersti Holmen as Wenche
* Jan Gunnar Røise as Geir
* Julia Bache-Wiig as Silje
* Aksel Hennie as Samí
* Henrik Mestad as Kenny Jannike Kruse as Merete
* Ane H. Røvik Wahlen as Kennys wife

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 