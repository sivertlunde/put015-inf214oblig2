Contract Killers
 
{{Infobox film
| name           = Contract Killers
| image          = Contract killers post.jpg
| caption        = Theatrical release poster
| director       = Justin B. Rhodes
| producer       = G. Anthony Joseph
| writer         = Ric Moxley Justin B. Rhodes
| starring       = Frida Farrell Nick Mancuso Rhett Giles Brewier Welch Wolf Muser Paul Cram
| music          = Michael Mouracade
| cinematography = Andre Lascaris
| editing        = Justin B. Rhodes
| distributor    = Birch Tree Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} assassin on the run from the law. The film was directed by Justin B. Rhodes, and stars Frida Farrell, Nick Mancuso, and Rhett Giles with an appearance by Paul Cram.

==Plot==
 

==Cast==
* Steve Boergadine as "Winston Scott"
* Paul Cram as "Chuck Dittmer"
* Frida Farrell as "Jane"
* Rhett Giles as "Purnell"
* G. Anthony Joseph as "Monoven"
* Nick Mancuso as "Witkoff"
* Wolf Muser as "Targonsky"
* Christian Willis as "Lars"

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 
 