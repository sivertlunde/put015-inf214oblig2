Irrikku M.D. Akathudu
{{Infobox film
| name           = Irrikku M.D. Akathudu
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       = VM Basheer
| writer         = Kalabhavan Ansar Kaloor Dennis (dialogues)
| screenplay     = Innocent Mukesh Mukesh Sunitha Sunitha A. C. Zainuddin Shyam
| cinematography = K Ramachandrababu
| editing        =
| studio         = Vimbis Productions
| distributor    = Vimbis Productions
| released       =  
| country        = India Malayalam
}}
 1991 Cinema Indian Malayalam Malayalam film, Sunitha in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast== Innocent
*Mukesh Mukesh
*Sunitha Sunitha
*A. C. Zainuddin
*Kalabhavan Ansar Chithra
*Geetha Geetha
*Saikumar Saikumar

==Soundtrack== Shyam and lyrics was written by Pradeep Ashtamichira, Ranjith Mattanchery, RK Damodaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaromale Nee En || MG Sreekumar || Pradeep Ashtamichira, Ranjith Mattanchery, RK Damodaran || 
|-
| 2 || Kodiyuduthetho || MG Sreekumar, Sujatha Mohan, Chorus || Pradeep Ashtamichira, Ranjith Mattanchery, RK Damodaran || 
|-
| 3 || Ningalkkoru joli || MG Sreekumar, Chorus || Pradeep Ashtamichira, Ranjith Mattanchery, RK Damodaran || 
|}

==References==
 

==External links==
*  

 
 
 

 