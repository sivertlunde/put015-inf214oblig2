Parisian Nights
 
{{Infobox film
| name           = Parisian Nights
| image          = 
| caption        = 
| director       = Alfred Santell
| producer       = 
| writer         = Emil Forst Doty Hobart Fred Myton
| starring       = Elaine Hammerstein Gaston Glass
| music          = 
| cinematography = Ernest Haller
| editing        = 
| distributor    = Film Booking Offices of America 
| released       =  
| runtime        = 70 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}

Parisian Nights is a 1925 American drama film directed by Alfred Santell and featuring Boris Karloff.    A print of the film exists at the Cinematheque Royale de Belgique.   

==Cast==
* Elaine Hammerstein - Adele
* Gaston Glass - Jacques
* Lou Tellegen - Jean
* William J. Kelly - Fontane
* Boris Karloff - Pierre
* Renée Adorée - Marie

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 