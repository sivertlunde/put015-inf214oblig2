The Robbers (film)
{{Infobox film
| name           = The Robbers
| image          = 
| caption        = 
| director       = Francisco Rovira Beleta
| producer       = 
| writer         = Francisco Rovira Beleta Manuel María Saló Tomás Salvador
| starring       = Pierre Brice
| music          = 
| cinematography = Aurelio G. Larraya
| editing        = Juan Luis Oliver
| distributor    = 
| released       = 26 February 1962
| runtime        = 109 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

The Robbers ( ) is a 1962 Spanish crime film directed by Francisco Rovira Beleta. It was entered into the 12th Berlin International Film Festival.   

==Cast==
* Pierre Brice - El Señorito
* Manuel Gil - Ramón Orea Bellido Chico
* Julián Mateos - Carmelo Barrachina Compadre
* Agnès Spaak - Isabel
* Antonia Oyamburu
* Rosa Fúster
* Carlos Ibarzábal
* Alejo del Peral Mariano Martín
* Carmen Pradillo
* Carlos Miguel Solá
* Camino Delgado - (as Mª Camino Delgado)
* Ana Morera
* Gustavo Re - Cómplice del mago que acosa a Isabel
* Joaquín Ferré
* Enrique Guitart - Abogado
* María Asquerino - Asunción

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 