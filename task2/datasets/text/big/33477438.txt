89 Millimeter
 

 
{{Infobox film
| name           =89 Millimeter
| image          = 
| image_size     = 
| caption        = 
| director       =
| producer       =
| writer         =
| narrator       = 
| starring       =
| music          =
| cinematography = 
| editing        = 
| distributor    = 
| released       =
| runtime        = 
| country        = Germany German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
89 Millimeter is a documentary by filmmaker Sebastian Heinzel, filmed from 2004 to 2005 in Belarus.

== Subject Matter ==
The film explores the life of six young Belarusians facing their futures in the wake of the collapse of the Soviet Union.  The titular 89 millimeters refer to the difference between the gauge of the railway tracks of Belarus and those of its western neighbors.  The difference is small, but symbolizes how the border of the "last dictatorship in Europe" marks the edge of a different world.

== Selected Festival Participation ==

*"One World Festival" in Prague and Bratislava
*"Cologne Conference "(" Spectrum Young Film ") in Cologne
*"Anonimul Festival" in Sfântu Gheorghe / Romania
*"Document Festival" in Glasgow / Scotland
*"EU XXL" in Vienna and Krems
*"Big Sky Documentary Festival" in Missoula / USA

== Awards ==

*Valuable Predicate, German Film and Media Review (FBW)
*Special Mention of the Jury, Kassel Documentary Film and Video Festival
*Best Editing, International Student Film Festival Sehsüchte in Potsdam
*Best Cinematography, Documentary Festival in Prizren / Kosovo

== Reviews ==

*Lexikon des Internationalen Films : "The smart structure creates an illuminating portrait, which presents not only people and their hopes, but also reflects the state of a region that is largely cut off from Western Europe."
*Deutsche Welle : "A real revelation of the situation in this little-known country in Central Europe."
*Sueddeutsche Zeitung : "In this remarkable film, the young director explores the subtle mechanisms of Lukashenkas dictatorship."
*Jury, Kassel Documentary Film and Video Festival: "A kaleidoscope of encounters, which fuse together into an exciting picture."
*Jury, Sehsüchte Festival in Potsdam: "Free of prefabricated patterns of interpretation, the film creates a multi-faceted world in which the viewer can look around freely."

==External links==
*  

 
 
 
 


 
 