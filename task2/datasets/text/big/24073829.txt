Selena: Greatest Hits
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Selena: Greatest Hits
| image          = DVD cover of the movie Selena- Greatest Hits.jpg
| image_size     =
| alt            = 
| caption        = DVD cover
| director       = Abraham Quintanilla Jr.
| producer       = Abraham Quintanilla Jr.
| writer         = Mark Boal 
| narrator       = 
| starring       = Pete Astudillo Barrio Boyz Jennifer Lopez Alvaro De Torres Joe Ojeda Chris Pérez
| music          = 
| cinematography = 
| editing        = Abraham Quintanilla Jr.
| studio         = 
| distributor    = EMI Latin
| released       = August 5, 2003 (USA)
| runtime        = 
| country        = United States English Spanish-language|Spanish
| budget         = 
| gross          = 
}}
 2003 USA|American English and Spanish are used. It is set in Joshua Tree, California.

==Cast==
* Pete Astudillo - Back-up singer (segment "La Carcacha")
* Barrio Boyz - Singers (segment "Dondequiere Que Estes")
* Jennifer Lopez - Selena (segment "Siempre Hace Frio")
* Alvaro De Torres - Singer (segment "Buenos Amigos")
* Joe Ojeda - Keyboard (segment "La Carcacha")
* Chris Pérez - Guitarist (segment "La Carcacha")
* A.B. Quintanilla - Bass Guitarist (segment "La Carcacha")
* Suzette Quintanilla - Drummer (segment "La Carcacha")
* Ricky Vela - Keyboard (segment "La Carcacha")

==External links==
*  

 

 
 
 
 
 