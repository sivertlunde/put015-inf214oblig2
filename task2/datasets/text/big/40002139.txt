The Phantom Treehouse
{{Infobox film
| name           = The Phantom Treehouse
| image size     = 
| image	=	
| caption        = 
| director       = Paul Williams
| producer       = 
| writer         = Paul Williams
| based on       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| studio         = 
| released       = 1982
| runtime        = 
| country        = Australia
| language       = English
| budget         = $100,000 
}} animated fantasy adventure film directed by Paul Williams. Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p120  

==Plot==
A boy named Tom is playing make-believe by himself when his dog, Rex, chases his neighbour Lucys cat out of the house. Lucy runs after the animals but is still missing hours later, when the animals have returned home by themselves. Tom and Rex follow Lucys trail all the way to a treehouse in the middle of the swamp. While exploring the treehouse they fall through a hole in the floor and wake up in a land where birds are see-through and trees can talk.

Tom sees a pirate ship lead by Captain Blackjack McGregor, and rescues a bunyip from being captured by them. The bunyip has met Lucy, and tells Tom that she is being held prisoner by the pirates. Tom then meets Professor Crankwhistle, a friendly inventor with a flying airship, and they follow the pirates to their hideaway on Fire Island. Blackjack has mistaken Lucy for Princess Diana of Bongo Island based on a painting of the princess that he has in his treasure stash. Despite Lucys protests, Blackjack wants to marry her to his son. With Professor Crankwhistles help, Tom and Rex manage to rescue Lucy and escape on a rowboat. Blackjack gives chase on his ship, but a storm hits. 

Tom, Lucy and Rex fall overboard and wake up on Bongo Island, where they are brought to the Queen. The Queen explains that Princess Diana is her ancestor from three hundred years ago. When Blackjack arrives, he refuses to believe that Lucy isnt a princess and threatens to blow up the village if she isnt handed over. Lucy boards the ship but Blackjack fires his cannon anyway. A fight ensues, during which Lucy sabotages their gunpowder and the bunyip damages the pirate ship, forcing them to retreat to Fire Island.

Professor Crankwhistle redesigns the Queens longboats into warships, and together with her soldiers they head to Fire Island and attack the pirates while theyre unprepared. The pirates are defeated by the Queen, and when Blackjack accidentally lights gunpowder the volcanoes on Fire Island start to erupt. Tom, Lucy and Rex are stuck on the island as it explodes, but when they wake up they find themselves back in the treehouse.

The three return home and find that only hours have passed for their parents. No one believes their story, but the children insist they go to the treehouse again. When they arrive there a cyclone sweeps through the forest, taking the treehouse with it. Tom and Lucy are dismayed that they cant prove their story, but the bunyip is on top of the tree and laughs at the adults for their skepticism.

==References==
 

==External links==
* 

 
 
 


 