The Great Alaskan Mystery
{{Infobox film
| name = The Great Alaskan Mystery
| image = The Great Alaskan Mystery (1944 serial) still 1.JPG
| image size =
| caption = Ray Taylor
| producer = Henry MacRae
| writer = Jack Foley George H. Plympton Maurice Tombragel
| narrator =
| starring = Milburn Stone Marjorie Weaver Edgar Kennedy Samuel S. Hinds Martin Kosleck Ralph Morgan
| music =
| cinematography = Harry Neumann William A. Sickner
| editing = Irving Birnbaum Jack Dolan Ace Herman Alvin Todd Edgar Zane
| distributor = Universal Pictures
| released =  
| runtime = 13 chapters (223 min)
| country = United States
| language = English
| budget =
| preceded by =
| followed by =
}} Universal Serial film serial about government agents trying to stop Nazi spies from getting their hands on futuristic weapons.

==Plot==
James Jim Hudson, an adventurer and accompanied by allies, goes after Nazi agents who have a new death ray called the Paratron...

==Cast==
* Milburn Stone as Jim Hudson
* Marjorie Weaver as Ruth Miller
* Edgar Kennedy as Bosun Higgins
* Samuel S. Hinds as Herman Brock
* Martin Kosleck as Dr Hauss
* Ralph Morgan as Dr Miller
* Joseph Crehan as Bill Hudson
* Fuzzy Knight as "Grit" Hartman
* Harry Cording as Captain Greeder
* Anthony Warde as Brandon

==Critical reception==
Cline considers this to be a mediocre serial but possessing a good cast and all the necessary "ingredients" of a good serial. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 3. The Six Faces of Adventure
 | page = 50
 }} 

==Chapter titles==
# Shipwrecked Among Icebergs (18 min 03s)
# Thundering Doom (18 min 56s)
# Battle in the Clouds (17 min 02s)
# Masked Murder (17 min 32s)
# The Bridge of Disaster (17 min 46s)
# Shattering Doom (16 min 34s)
# Crashing Timbers (15 min 35s)
# In a Flaming Plane (17 min 24s)
# Hurtling Through Space (17 min 19s)
# Tricked by a Booby Trap (17 min 04s)
# The Tunnel of Terror (16 min 37s)
# Electrocuted (33 min 42s)
# The Boomerang (16 min 34s)
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 237
 }} 

==See also==
* List of film serials by year
* List of film serials by studio
* List of films in the public domain

==References==
 

==External links==
* 
* 

===Download or view online===
*   (several formats available)
  
*  
*  
*  
*  
*  
*  
 
*  
*  
*  
*  
*  
*  
 

 
{{succession box Universal Serial Serial
| before=Adventures of the Flying Cadets (1943 in film|1943)
| years=The Great Alaskan Mystery (1944 in film|1944)
| after=Raiders of Ghost City (1944 in film|1944)}}
 

 

 
 
 
 
 
 
 
 
 
 
 


 