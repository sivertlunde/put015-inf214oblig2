Thousand Pieces of Gold (film)
{{Infobox film
| name           = Thousand Pieces of Gold
| image          = Thousand Pieces of Gold film.jpg
| image_size     =
| caption        = Original theatrical poster
| director       = Nancy Kelly
| producer       = Sidney Kantor, Lindsay Law and John Sham
| writer         = Ruthanne Lum McCunn (novel); Anne Makepeace (screenplay)
| narrator       =
| starring       = Chris Cooper Rosalind Chao Michael Paul Chan Dennis Dun Beth Broderick
| music          = Gary Malkin
| cinematography = Bobby Bukowski
| editing        = Kenji Yamamoto
| distributor    = Greycat Films
| released       =  
| runtime        = 105 minutes
| country        = United States Chinese
}}
 novel of the same name.

==Plot== Civil War. Upon her arrival in California, she meets Jim (Dennis Dun), a Chinese "wife trader" who sells her to Hong King (Michael Paul Chan), a successful Chinese merchant who lives in a rural Idaho mining town. The two set off on the long journey to Idaho and eventually strike up a friendship along the way.

When they finally arrive in the rough, isolated town, she is distraught to discover that she is not going to be Hong Kings wife. Instead, she is to work in his saloon as his newest prostitute under a new name, "China Polly". She is further dismayed when Jim abruptly disappears, leaving her to fend for herself.
 virtue to Caucasian partner. She placates a furious Hong King and convinces him to allow her to be his servant and saloon maid in order to repay the cost of her purchase. Hong King agrees to let her buy her freedom for the impossible sum of a thousand pieces of gold.

Polly, as Lalu comes to be known, endures great hardship. At one point, she is sexually assaulted by Hong King. However, she refuses to give up. She works hard and makes friends with the local townspeople. She also grows closer to Charlie, who begins to fall deeply in love with her. Meanwhile, Hong King is beset with financial problems and decides to sell Polly to the highest bidder. In a rare stroke of luck, Charlie wins her in a game of poker. She moves in with him but insists they remain platonic and keep separate quarters.

Jim comes back and wants her to be with him but he then leaves her again when he finds out that she is living with Charlie. The "white demons" begin to run out the Chinese people from their town so it will be a purely white town and the Chinese will stop getting all of the gold.

Polly works in many jobs for herself and saves money to go back to China and her family but she ultimately ends up falling in love with Charlie. She marries him and lives the rest of her life with him in a different area so she would not be harassed by the white demons anymore.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 