Black & White (2008 Indian film)
{{Infobox film
| name           = Black and White
| image          = Black&white2.jpg
| image_size     = 
| caption        = Movie poster
| director       = Subhash Ghai
| producer       = Raju Farooqui Mukta Ghai Subhash Ghai Rahul Puri
| writer         = Sachin Bhowmick Subhash Ghai Akash Khurana
| starring       = Anil Kapoor Anurag Sinha Shefali Shah Aditi Sharma
| music          = Sukhwinder Singh
| cinematography = Somak Mukherjee
| editing        = Amitabh Shukla
| assistant director = Anshuman Jha
| distributor    = Mukta Arts
| released       = 7 March 2008
| runtime        = 138 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Independence Day.   
Upon release, the film received wide critical acclaim, and was premiered at the International Film Festivals of Pune and Delhi. 

==Plot==
Rajan Mathur (Anil Kapoor) is a Urdu professor who lives in Chandni Chowk district in New Delhi, with his wife Roma Mathur (Shefali Shah), who is a social activist and Feminism|feminist, and their young daughter.

Professor Mathur meets Numair Qazi (Anurag Sinha), who informs him that he is a victim of communal riots in Gujarat. He is actually a suicide bomber of an Islamic fundamentalist group who has been ordered to set off a bomb near Red Fort during the Independence day celebrations.

Numair wins the trust of the professor and his wife. While assisting Numair to get an entry pass for the celebrations at Red Fort, Professor Mathur introduces him to people living in harmony in Chandni Chowk regardless of faith.

Numair is no longer sure if he should carry out the orders of his superiors or not. Although he is a deep-rooted fundamentalist, he sees this area as colourful and loving. There is no Black and White. Nonetheless, he goes forward to accomplish his mission.

==Cast==
* Anil Kapoor: Rajan Mathur 
* Anurag Sinha: Numair Qazi
* Habib Tanvir: Qazi Saab
* Shefali Chhaya: Roma Mathur 
* Aditi Sharma: Shagufta
* Sukhwinder Singh: special appearance
* Sai Tamhankar: Nimmo
* Arun Bakshi: Naeem Shaikh
* Milind Gunaji: Hamid

==Music==
{| class="wikitable" border="1"
! Song !! Singer(s)!! Duration
|-
| Haq Allah
| Sukhwinder Singh & Hans Raj Hans
| 5:33
|-
| Peer Manava
| Sukhwinder Singh & Sharda Pandit
| 4:16
|-
| Yeh Hindustan hai (Part 1)
| Jagjit Singh
| 5:02
|-
| Main Chala
| Sukhwinder Singh
| 5:43
|-
| Jogi Aaya
| Sukhwinder Singh & Sadhana Sargam
| 4:49
|-
| Yeh Hindustan hai (Part 2)
| Udit Narayan
| 5:02
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 