Broken Flowers
 
{{Infobox film
| name           = Broken Flowers
| image          = Broken_Flowers_poster.jpg
| caption        = Theatrical release poster
| director       = Jim Jarmusch
| producer       = Jon Kilik Stacey Smith
| writer         = Jim Jarmusch  (Inspired by an idea from Bill Raden and Sara Driver)  Jeffrey Wright Mark Webber Christopher McDonald Alexis Dziena
| music          = Mulatu Astatke
| cinematography = Frederick Elmes Jay Rabinowitz
| studio         = Five Roses Bac Films
| distributor    = Focus Features
| released       =  
| runtime        = 106 minutes
| country        = United States France
| language       = English
| budget         = $10 million 
| gross          = $46,720,491   
}} Jeffrey Wright, Mark Webber.

==Plot== Jeffrey Wright), who is a mystery novel enthusiast, urges Don to investigate. Winston researches the current locations of the five women most likely to have written the letter and gives Don the information along with maps and flight reservations, and urges him to visit them. Don finally agrees and Winston tells Don that he will drive him to the airport the next morning.

Ultimately Don meets with four women, each encounter worse than the last:
* Laura (Sharon Stone) works as a closet and drawer organizer and is the widow of a race car driver. She has a teenage daughter, Lolita (Alexis Dziena), who parades nude in front of Don. That night, Laura sleeps with Don. Christopher McDonald). animal communicator."  Don recalls how she was formerly so passionate about becoming a lawyer.  But "passion is a funny thing," she says. She is cold to Don and appears to be having an affair with her secretary (Chloë Sevigny).
* Penny (Tilda Swinton) lives in a rural area amongst bikers.  She holds an ongoing grudge against Don. When Don asks her whether she has a son, she becomes enraged, which results in one of her biker friends punching Don out. The next morning, Don finds himself in his car, in the middle of a field, with a nasty cut near his left eye.
 Mark Webber) whom he suspects may be his son. He buys him a meal, but when he remarks that the young man believes that Don is his father, the young man becomes agitated and flees. As Don attempts to chase the man, he notices a Volkswagen Beetle drive past. A young man in the passenger seat (played by Homer Murray, the real-life son of Bill Murray) is listening to the music which Don himself listens to as Don is left standing in the middle of a crossroads.

==Cast==
* Bill Murray as Don Johnston Jeffrey Wright as Winston
* Sharon Stone as Laura Daniels Miller
* Frances Conroy as Dora Anderson
* Christopher McDonald as Ron Anderson
* Jessica Lange as Dr. Carmen Markowski
* Tilda Swinton as Penny
* Julie Delpy as Sherry
* Alexis Dziena as Lolita Miller
* Chloë Sevigny as Carmens assistant
* Pell James as Sun Green
* Meredith Patterson as Flight attendant
* Ryan Donowho as Young man on bus Mark Webber as The Kid
 

==Production==
The film is dedicated to French director Jean Eustache. In an interview, Jarmusch said he felt close to Eustache for his commitment to making films in a unique and independent fashion.   
 amalgamation of those four letters in the finished film, "using pieces of their own language". 

===Lawsuit===
Reed Martin sued Jarmusch in March 2006, claiming that the director stole the films concept from a very similar script that had circulated among several people eventually involved in the production.  Jarmusch denied the charges and stated in response that Martins claim has "absolutely no merit". On September 28, 2007, a federal district court judge dismissed Martins lawsuit that Jarmusch and Focus Films stole the screenplay from him. 

==Release== premiered in Europe at 2005 Cannes Film Festival on May 16, 2005.  It opened on August 5, 2005 in the US in a limited release.   It was released on video January 3, 2006. 

===Box office===
The film was released theatrically on August 5, 2005 earning $780,408 from 27 theaters. After 15 weeks in release, the movie ended with a domestic total of $13,744,960. The film fared much better internationally, taking in $32,975,531 to bring its total gross to $46,720,491. 

===Critical reception=== Grand Prix.    Review aggregator Rotten Tomatoes reports that 88% of 184 surveyed critics wrote a positive review, with the sites consensus stating: "Bill Murrays subtle and understated style complements director Jim Jarmuschs minimalist storytelling in this quirky, but deadpan comedy." 

==Soundtrack==
{{Infobox album  
| Name        = Music from Broken Flowers
| Type        = Soundtrack
| Longtype    = 
| Artist      = Various artists
| Cover       = Brokenflowerssoundtrackalbumcover.jpg
| Released    = August 2, 2005
| Recorded    =  classical
| Length      = 38:01 Decca
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}{{Album ratings
| rev1      = Allmusic
| rev1Score =   }} Holly Golightly), soul (Marvin classical (Gabriel Faurés Requiem (Fauré)|Requiem).
 Holly Golightly with The Greenhornes) - 3:05
# "Yegelle Tezeta" (Mulatu Astatke) - 3:14
# "Ride Yu Donkey" (The Tennors) - 2:03 I Want You" (Marvin Gaye) - 3:57
# "Yekermo Sew" (Mulatu Astatke) - 4:03 Not if You Were the Last Dandy on Earth" (The Brian Jonestown Massacre) - 2:49 Holly Golightly) - 2:02
#* Written by Ray Davies
# "Gubèlyé" (Mulatu Astatke) - 4:35
# "Dopesmoker" (Sleep (band)|Sleep) - 3:57
#* Abridged version of 63:31-minute track. Requiem in D minor, Op. 48 ("Pie Jesu") (Oxford Camerata) - 3:30
#* Composed by Gabriel Fauré, fourth of seven-movement work. Dengue Fever) - 4:38
#* Instrumental, composed by Mulatu Astatke
# "Unnatural Habitat" (The Greenhornes) - 2:08

;Other songs in the film
Several songs in the film are not on the soundtrack album. They include:
* "The Allman Brothers Band (album)|Dreams" by The Allman Brothers Band
* "El Bang Bang" - Jackie Mittoo
* "Playboy Cha-Cha" - Mulatu Astatke
* "Mascaram Setaba" - Mulatu Astatke Fretwork
* "Fantasy" (A 6 in F Major) composed by William Lawes, performed by Fretwork
* "Alone in the Crowd" - Mulatu Astatke

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 