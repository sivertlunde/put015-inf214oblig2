Sad Vacation
{{Infobox film
| name           = Sad Vacation
| image          = Sad vacation one sheet.jpg
| caption        =
| director       = Shinji Aoyama
| producer       = Naoki Kai
| writer         = Shinji Aoyama
| starring       = Tadanobu Asano Eri Ishida Aoi Miyazaki Yuka Itaya
| music          = Hiroyuki Nagashima
| cinematography = Masaki Tamura
| editing        = Yuji Oshige
| distributor    = Style Jam
| released       =  
| runtime        = 110 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}
  is a 2007 Japanese drama film written and directed by Shinji Aoyama, adapted from his own novel. It was named after the Johnny Thunders song.  

==Story==
The film concerns a Japanese man, Kenji Shiraishi (Tadanobu Asano), who is involved in the human trafficking of illegal immigrants from China. When one of the children is left behind by his father, Kenji takes him in.

==Cast==
* Tadanobu Asano as Kenji Shiraishi
* Eri Ishida as Chiyoko Mamiya
* Aoi Miyazaki as Kozue Tamura
* Yuka Itaya as Saeko Shiina
* Katsuo Nakamura as Shigeki Mamiya
* Kengo Kora as Yusuke Mamiya
* Kaori Tsuji as Yuri Matsumura
* Joe Odagiri as Goto
* Ken Mitsuishi as Shigeo
* Yoichiro Saito as Akihiko
* Maho Toyota as Makimura
* Kōsuke Toyohara as Kawashima
* Kyūsaku Shimada as Sone
* Yusuke Kawazu as Kijima

==Reception==
Travis Mackenzie Hoover of   described the film as "a quietly powerful drama in which Aoyama manages to address blood ties, fate and regeneration". 

==Awards==
It won the Jury Award at the 2008 New York Asian Film Festival. 

==References==
 

==External links==
*  
*  

 
 
 
 
 

 