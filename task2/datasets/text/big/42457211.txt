Sweedie Learns to Swim
{{Infobox film
| title          = Sweedie Learns to Swim
| image          =
| caption        =
| director       =
| producer       = Essanay Film Manufacturing Company
| writer         =
| starring       = Wallace Beery Ben Turpin
| cinematography =
| editing        =
| distributor    = Essanay Film Manufacturing Company
| released       =  
| runtime        = One reel
| country        = United States
| language       = Silent film (English)
}}
Sweedie Learns to Swim is a 1914 silent short 1-reel comedy film starring Wallace Beery and Ben Turpin and produced and distributed by the Essanay Company. 

This short survives in the Library of Congress collection, UCLA Film and Television and others. 

==Cast==
*Wallace Beery - Sweedie
*Betty Brown - Mrs. Rich
*Ben Turpin - Captain of the Life Savers
*Leo White - Mr. Rich
*Charlotte Mineau -

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 