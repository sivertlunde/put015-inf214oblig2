Glory Road (film)
{{Infobox film
| name           = Glory Road
| image          = Glory road.jpg
| caption        = Theatrical release poster
| director       = James Gartner
| producer       = Jerry Bruckheimer
| screenplay     = Chris Cleveland Bettina Gilois Gregory Allen Howard
| based on       = Glory Road by Don Haskins and Daniel Wetzel
| starring       = Josh Lucas Derek Luke Jon Voight
| music          = Trevor Rabin
| cinematography = Jeffrey L. Kimball John Toon John Wright
| studio         = Walt Disney Pictures Jerry Bruckheimer Films Texas Western Productions Glory Road Productions Buena Vista Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $42,938,449   
}}
 drama List sports film 1966 NCAA Texas Western a team with an all-black starting lineup, a first in NCAA history. Glory Road explores racism, discrimination, and student athletics. Supporting actors Jon Voight and Derek Luke also star in principal roles.

The film was a co-production between the motion picture studios of Walt Disney Pictures, Jerry Bruckheimer Films, Texas Western Productions, and Glory Road Productions. It was commercially distributed by Buena Vista Pictures theatrically and by the Buena Vista Home Entertainment division for the video rental market. It premiered in theaters nationwide in the United States on January 13, 2006, grossing $42,938,449 in box office business. Glory Road was nominated for a number of awards including the Humanitas Prize; the film won the 2006 ESPY Award for Best Sports Movie.

The film presently holds a 55% score on Rotten Tomatoes and a rating of "mixed or average" from Metacritic.   On January 10, 2006, the original motion picture soundtrack was released by the Hollywood Records music label. The soundtrack was composed and orchestrated by musician Trevor Rabin. With its initial foray into the home media marketplace; the films widescreen DVD edition, featuring theatrical trailers, extended interviews with players and colleagues of coach Haskins, and deleted scenes among other highlights, was released in the U.S. on June 6, 2006.

==Plot== El Paso, lacking necessary financial resources, makes an effort to recruit the best players regardless of race to form a team that can compete for a national championship. Some of the young men he brings in possess skill, but are raw in talent when it comes to organized teamwork focusing on defense and ball distribution. In the end, his Texas Western Miners team comprises seven black and five white athletes; a balance that raises eyebrows among university personnel. Haskins puts his players through a rigorous training program, threatening to cut anyone who doesnt work as hard as he demands, while trying to integrate his players into a single team with a common goal.

Following initial victories against mediocre local teams, Haskins quickly discovers that he has to give his black players more free room on the court. Yet the more victories his team achieves with its flamboyant style, up until this point rarely seen in college basketball, the more racial hatred mounts on his squad. This culminates in threats to his own family, the beating of a player while on the road and ultimately the ravage of his teams motel rooms by racists while they are at an away game. Increasingly frightened, the team loses its last game of the regular season after the black players stop playing with passion. Thus the Texas Western Miners finish the 1965-66 regular season with a 23–1 record, entering the 1966 NCAA tournament ranked third in the nation.
 University of Wildcats squad firmly believes that his opponent stands no chance. On the eve of the decisive game, Haskins decides to experiment with a bold strategy, informing his team that he intends to start an all-black lineup in the game, and also only using the two other black players in the rotation.
 forward and team captain Harry Flournoy (Brooks) leaving the game with an injury, and their center David Lattin (Kerr) in foul trouble. In a close game, the Miners narrowly lead at halftime, but finally manage to beat Kentucky 72–65 with some impressive steal (basketball)|steals, rebounding and passing techniques in the second half. The film ends with the players exiting the plane that brought them back to El Paso to the greeting of a raucous crowd.

==Cast==
 
* Josh Lucas as Don Haskins
* Derek Luke as Bobby Joe Hill
* Austin Nichols as Jerry Armstrong  
* Jon Voight as Adolph Rupp Evan Jones as Moe Iba
* Schin A.S. Kerr as David Lattin
* Alphonso McAuley as Orsten Artis
* Mehcad Brooks as Harry Flournoy
* Sam Jones III as Willie Worsley
* Damaine Radcliff as Willie Cager
* Emily Deschanel as Mary Haskins, wife of Don Haskins
* Al Shearer as Nevil Shed
* Tatyana M. Ali as Waltina "Tina" Malichi

==Production==

===Development===
 
Glory Road was inspired by a true story, as described by Texas Westerns head coach Don Haskins in his autobiography of the same title, a national bestseller released in 2005 by Hyperion Books. The book details Haskins early life as a player (including a one-on-one game against a black friend that opened his eyes) and womens basketball coach. Like the film, it then focuses on the 1966 Texas Western mens basketball team and the aftermath of the championship. It was reprinted five times in its first four months of release and was selected as an "Editors Choice" by the New York Times Book Review. Additionally, Glory Road is the name of a street on the UTEP campus near the Sun Bowl which was renamed to commemorate the 1966 NCAA championship.

Later asked about his decision to start five black players, Haskins downplayed the significance of his decision. "I really didnt think about starting five black guys. I just wanted to put my five best guys on the court. I just wanted to win the game."  Though credited with setting in motion the desegregation of college basketball teams in the American South|South, he wrote in his book "I certainly did not expect to be some racial pioneer or change the world." 

Dunking was banned in the NCAA from 1967 to 1976, not least due to the success of the Texas Western team and a UCLA player named Lew Alcindor (better known later as Kareem Abdul-Jabbar) then entering the league.
 NBA head coach Pat Riley. The real life Don Haskins was cast as an extra in the film as a gas station attendant, and David Lattin was cast as an extra as a military bartender.

The players on the 1966 team were David Lattin, Bobby Joe Hill, Willie Cager, Willie Worsley, Jerry Armstrong, Orsten Artis, Nevil Shed, Harry Flournoy, Togo Railey, Louis Baudoin, Dick Myers,and David Palacio.
The team was nominated in its entirety for the Naismith Memorial Basketball Hall of Fame, and was inducted on September 7, 2007, ten years after coach Don Haskins had already been enshrined.

The movie skipped a crucial game that Texas-Western had played. On March 18, 1966, the Miners defeated Utah 85-78 in the Final Four to advance to the national championship game the following night. In the movie the team played against Kansas in the regional final and the following game was the national championship, which is incorrect. 

===Filming=== Jesuit High Wells Fargo Plaza and the Chase Bank Building in downtown El Paso can be seen in the top left corner. The Wells Fargo Plaza was not completed until 1971, and the Chase Bank Building was still the Texas Commerce Bank building until the early 1990s. In addition, Ralph Strangis (the Dallas Stars play-by-play announcer) had a small speaking role as a courtside broadcaster. Ben Affleck was the original choice for the role of coach Don Haskins, but had to drop out of the filming due to prior commitments. NBA point guard Kirk Hinrich was offered a role in the film, but chose not to participate "because of time constraints". 

During the scene of the Texas Western-Seattle University basketball game broadcast the announcers inadvertently used the call letters WTSM which is a  FM radio sports station from Tallahassee Florida instead of KTSM-AM which is a radio station in El Paso Texas.

===Controversy===
Kentucky Wildcat fans and other Rupp supporters said the film at least implicitly portrayed University of Kentucky Coach Adolph Rupp as a racist, with such lines as Bobby Joe Hills that Rupp would not have recruited him. Like other teams in the Southeastern Conference, Kentucky was indeed all-white, but they were the first (and for about a decade only) SEC team to regularly play (inter-conference) opponents with black players (starting in the 1950s) and in fact, took the place of Alabama (1956) Mississippi State (1959. 1961) in the NCAA Tournament after their respective state legislatures and/or university leadership refused the invites because they may play against integrated squads.   Starting in 1964 Rupp had recruited Kentuckians   did regarding Jackie Robinson) and Unseld and Beard went to Louisville instead. Rupps critics insist that Rupp shouldve been more aggressive in his attempts to integrate his team.

One other inaccuracy was there were no displays of the Rebel flags and no band played Dixie in Cole Field House during the game.  Cole Field House in on the campus of the University of Maryland which was the school that was first to integrate Atlantic Coast Conference athletics in 1964.
 East Texas Disney and the makers of the film.  Disney did not directly apologize, rather, it explained that the movie was not a documentary and that it had been necessary to consolidate events given the time limitations of the film, and that Disney did not intentionally set out to misrepresent any group and was sorry for any misunderstanding.  . Texas A&M University Commerce. Retrieved 2013-02-13.  The President of Texas A&M–Commerce said that, given the way the school was shown in the film, it was hard to believe that Disney could plausibly argue that the portrayal of the school was unintentional.  The scene even prompted the Texas state senate to consider a bill which would allow financial assistance from the state to be withheld for films that portray the state negatively. 

===Soundtrack===
On January 10, 2006, the soundtrack was released on the Hollywood Records label. The film score was orchestrated by musician Trevor Rabin and features music composed by various artists. 
{{Infobox album
| Name = Glory Road Original Soundtrack
| Type = Film score
| Artist = Various Artists
| Cover = GloryRoadSoundT.jpg
| Released = January 10, 2006
| Length = 32:43 Hollywood
| Reviews =
}}

{{Track listing
| collapsed       = no
| headline        = Glory Road Original Soundtrack
| total_length    = 32:43
| title1          = People Get Ready
| length1         = 2:43
| title2          = Aint That Peculiar
| length2         = 3:00
| title3          = Uptight (Everythings Alright)
| length3         = 2:54
| title4          = Dancing in the Street
| length4         = 2:40
| title5          = Im on my way to Canaan
| length5         = 3:23
| title6          = Can You Do It
| length6         = 2:20
| title7          = Shake It Up Baby (aka Twist And Shout)
| length7         = 2:30
| title8          = Down in the Boondocks
| length8         = 2:36
| title9          = Ive Been Loving You Too Long (To Stop Now)
| length9         = 3:13
| title10         = Aint That Good News
| length10        = 2:40
| title11         = I Will Make The Darkness Light
| length11        = 2:25
| title12         = Glory Road
| length12        = 4:19
}}

==Release==

===Home media=== Region 1 widescreen Pan and scan edition of the motion picture was released on DVD in the United States on June 6, 2006.  A Special Edition widescreen format DVD and a PlayStation Portable of the film were also released on June 6, 2006.

A restored widescreen high definition Blu-ray Disc|Blu-ray was released on October 17, 2006. Special features include backstage reature film: audio commentary with producer Jerry Bruckheimer & director James Gartner; Audio commentary with screenwriters Chris Cleveland and Bettina Gilois; "Surviving Practice" – inside a typical grueling Haskins practice with former NBA star Tim Hardaway and seamless menus. 

==Reception==

===Critical response=== weighted average out of 100 to critics reviews, Glory Road received a score of 58 based on 33 reviews. 
 Black Reel Awards. 

===Box office=== box office straight to DVD or just shown on TV in some countries that have no connection to college basketball. The film grossed a total of $42,938,449 worldwide. 

==See also==
 
*2006 in film
*1965–66 Texas Western Miners basketball team
 

==References==
Notes
 

Bibliography
*Haskins, Don with Dan Wetzel. Glory Road. New York:Hyperion, 2006. ISBN 1-4013-0791-4.

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 