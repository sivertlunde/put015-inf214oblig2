Pepo (film)
 
{{Infobox film
| name           = Pepo
| image          =  
| image_size     = 
| caption        = 
| director       = Hamo Beknazarian Armen Gulakyan
| producer       =
| writer         = Hamo Beknazarian
| narrator       =
| starring       = Hrachia Nersisyan Avet Avetisyan Hasmik Agopyan Grigor Avetyan
| music          = Aram Khachaturian
| cinematography = Dmitri Feldman
| editing        =
| distributor    = Amkino Corporation (1935) (USA) (subtitled)
| released       = 9 October 1935
| runtime        = 88 min.
| country        = Soviet Union Armenian
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} Soviet film 1876 play of the same name. Pepo was the first Armenian language sound film ever created. Music for the film was created by Aram Khachaturian, a well-known Armenian composer, who was born in Tiflis himself. Considered the most outstanding film in Soviet cinema before the outbreak of World War II,  the film has gained international recognition and has come to represent Armenian culture abroad. 

==Plot== Armenian fisherman Pepo (Hrachia Nersisyan) who opposes a cunning trader Arutin Kirakozovich Zimzimov (Avet Avetisyan), who has robbed the former by trickery.    The story comes to a conclusion of sorts when Pepo falls in love. {{Cite web| url= http://movies.msn.com/movies/movie.aspx?m=376471&silentchk=1&mp=syn| title=
Pepo:Synopsis|accessdate=11 August 2007}} 
 
==Cast==
 
*Hrachia Nersisyan – Pepo
*Tatyana Makhmuryan – Kekel, his sister
*David Malyan – Kakuli, a friend
*Avet Avetisyan – Arutin Kirakozovich Zimzimov
*Hambartsum Khachanyan – Darcho, merchant (as A. Khazanyan)
*Hasmik Agopyan – Shushan (as Hasmik)
*Grigor Avetyan – Giko (as G. Avetyan)
*Nina Manucharyan – Natel (as N. Manuizauryan)
*Armen Gulakyan – Duduli, a friend
*N. Gevorgyan – Efemia
*Gurgen Gabrielyan – Kinto
*A. Kefchiyan – Pichkhul
*H. Vanyan – Margurit
*M. Garagash – Gevorg, clerk
*Vladimir Barsky – Judge
*V. Bagratuni – Samson
*M. Beroyan – Darchos mother
*M. Jrpetyan – Gossiper
 

 
Pepo statue (3).jpg|Pepos statue in Yerevan]] Pepo
Pepo Pepo and Zimzimov Kekel
 

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 

 