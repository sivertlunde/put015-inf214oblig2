La intrusa (1954 film)
{{Infobox film
| name           =La intrusa 
| image          = 
| image size     =
| caption        =
| director       = Miguel Morayta
| producer       =Fernando de Fuentes
| writer         =  Caridad Bravo Adams (radioplay "La Intrusa"), Paulino Masip
| narrator       =
| starring       = Rosario Granados, Eduardo Fajardo, Evangelina Elizondo 
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 22 July 1954 (Mexico)
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1954 Mexico|Mexican film. It was produced by 
Fernando de Fuentes.

==Cast==

* 	 Rosario Granados		
* 	 Eduardo Fajardo		
* 	 Evangelina Elizondo		
* 	 Luis Beristáin		
* 	 Carlos Martínez Baena		
* 	 Miguel Ángel Ferriz		
* 	 Enrique García Álvarez		
* 	 Matilde Palou		
* 	 María Herrero		
* 	 Salvador Quiroz		
* 	 Rosa Elena Durgel		
* 	 Lupe Suárez		
* 	 Rodolfo Landa

==External links==
*  

 
 
 
 


 