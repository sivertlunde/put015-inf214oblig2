Zero Tolerance (1995 film)
{{Infobox film
| name           = Zero Tolerance
| image          = Zero Tolerance poster.jpg
| director       = Joseph Merhi
| producer       = Joseph Merhi Richard Pepin
| writer         = Jacobsen Hart
| starring       = Robert Patrick Titus Welliver Mick Fleetwood
| music          = John Gonzales
| cinematography = Ken Blakey
| editing        = Chris Maybach Chris Worland
| distributor    = PM Entertainment Group Inc. 1995
| runtime        = 88 min.
| country        = United States|U.S. English
}} 1995 United American action film from PM Entertainment Group directed by Joseph Merhi and starring Robert Patrick and Mick Fleetwood.

== Plot == Michael Gregory) as they transport Mafia boss Raymond Manta (Titus Welliver) out of a Mexican jail, FBI agent Jeff Douglas (Robert Patrick) becomes an unwitting pawn of the White Hand mafia cartel.

Erroneously informed that his already slain family is being held hostage, Jeff is forced to turn one-time courier for the White Hand, whose leaders are Manta and four others—Helmut Vitch (Mick Fleetwood), Milt Kowalski (Miles OKeeffe), Russ LaFleur (Jeffrey Anderson-Gunter), and Hansel Lee (Gustav Vintas).

After surviving a car-bombing in Las Vegas, Jeff learns the truth about his family, and a devastated Jeff angrily sets out to exterminate the White Hand cartel.

Officially censured but unofficially aided by concerned agent Megan (Kristen Meadows), whose mother was raped and murdered years ago, Jeff steals some FBI files and kicks off his cartel extermination campaign by killing Vitch in Las Vegas, at Vitchs casino.

In New Orleans, Jeff sabotages LaFleurs liquid-heroin deal with a factory fire, and then at LaFleurs night club, Jeff finishes him off in full view of a satellite hook-up watched by Manta and Kowalski at their hide-out.

After that, Jeff kills Lee in his heavily guarded mansion in Seattle. After Megan arrives to speed up Jeffs getaway, the couple is apprehended by local cops, and then ambushed by Mantas men.

Although Megan is captured and taken away in a helicopter, Jeff momentarily flees. Tired of the whining Kowalski, Manta kills Kowalski at his complex, but endures a savage beating from Jeff, who rescues Megan.

Under arrest at FBI headquarters, Manta says its not over by a long shot. Manta grabs a gun, and in an effort to disarm Manta, Jeff plows into Manta, sending Manta through a window. As a result, Manta takes a fall to his death, which completes Jeffs mission to wipe out the White Hand cartel.

== Cast (selection) ==
* Robert Patrick - Jeff Douglas
* Titus Welliver - Manta
* Mick Fleetwood - Vitch
* Miles OKeeffe - Kowalski
* Kristen Meadows -  Megan
* Barbara Patrick - Wendy

==Home video==
The film had been released on home video in the UK by First Independent Films.

==External links==
* 

 
 
 
 


 