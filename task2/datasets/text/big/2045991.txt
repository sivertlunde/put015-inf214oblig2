Now and Then (film)
{{Infobox film
| name           = Now and Then
| image          = Now and Then (1995 film) poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Lesli Linka Glatter
| producer       = Demi Moore Suzanne Todd
| writer         = I. Marlene King
| starring       = Gaby Hoffmann Demi Moore Thora Birch Melanie Griffith Christina Ricci Rosie ODonnell Ashleigh Aston Moore Rita Wilson
| music          = Cliff Eidelman
| cinematography = Ueli Steiger
| editing        = Jacqueline Cambas
| distributor    = New Line Cinema
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English 
| budget         = $12,000,000
| gross          = $37,591,674
}}
 American coming-of-age story|coming-of-age film directed by Lesli Linka Glatter and starring Christina Ricci, Rosie ODonnell, Thora Birch, Melanie Griffith, Gaby Hoffmann, Demi Moore, Ashleigh Aston Moore and Rita Wilson.  The supporting cast features Hank Azaria, Janeane Garofalo, Cloris Leachman, and Bonnie Hunt, among many others. The plot follows four women who recount a pivotal summer they shared together as adolescents in 1970.

It was filmed largely in Winchester, Indiana, using the Gaslight Addition and Old Town Cemetery although in the movie called Shelbyville, Indiana Statesboro, Georgia, highlighting the downtown area.  Statesboro locations include the Bulloch County Court House (also featured in the film "1969") and the building now housing the Averitt Center for the Arts.

A dramatic sequence in the film features a storm drain in a rainstorm that is on Statesboros West Main Street, across the street from Main Street Billiards and near 119 Chops Restaurant.
 Pretty Little Liars.   However, the project did not move past the development stage.

==Plot==
In 1991, four childhood friends reunite in their hometown of Shelby, Indiana. Samantha Albertson (Demi Moore), a science-fiction writer, narrates the story. As an adolescent (played by Gaby Hoffmann) Samantha was considered the "weird" girl who liked performing seances and was interested in science and the supernatural. Roberta Martin (Rosie ODonnell), a doctor, was a tough tomboy (played by Christina Ricci) whose mother died when she was four-years-old. Chrissy DeWitt (Rita Wilson), who lives in her childhood home, is married and about to give birth to her first child. As a naïve youngster (played by Ashleigh Aston Moore), she was overly sheltered by her mother. Tina "Teeny" Tercell (Melanie Griffith) is a successful Hollywood actress; as a child (played by Thora Birch), she had always dreamed of fame. Teeny and Samantha have not visited their hometown in ten years.

The story flashes back to 1970 when the girls had two goals: saving enough money to buy a tree house and avoiding the Wormer brothers. One night, they sneak out to the cemetery to perform a seance. A cracked tombstone convinces them they have resurrected the spirit of a young boy identified only as Dear Johnny, who died in 1945 at the age of twelve. Intrigued, they search for information at the library but find nothing. Later, while heading for the library in a nearby town, they spy the Wormer brothers skinny dipping in the lake. To retaliate for a prank the boys played on them, the girls steal the boys clothes, tossing them onto the road while riding off. At the library, Roberta discovers an article about her mother being killed in a car accident, a fact previously unknown to her. Samantha finds a story about Dear Johnny and his mother tragically dying, but a part is missing, leaving the cause of their deaths a mystery. The girls then visit a local psychic (Janeane Garafalo) who determines he was murdered.

Samantha goes home and unexpectedly meets Bud Kent (Hank Azaria), a man her newly-single mother invited to dinner. Upset, she storms out and flees to Teenys. They hang out in the tree house display at the store where Samantha confesses her parents are getting divorced. Teeny comforts her, then breaks her favorite necklace in two, giving one half to Samantha as a "best friends for life" bracelet. On their way home during a thunder storm, Samantha loses it in a storm drain. When she climbs down to retrieve it, the water rises, trapping her. Crazy Pete, an old vagrant, pulls her out. Grateful, the girls now see him differently. At the same time, Roberta is playing basketball in her driveway when Scott Wormer (Devon Sawa) suddenly arrives. They question why they fight all the time before sharing a kiss. 
 
The girls consult Samanthas grandmother (Cloris Leachman) about Dear Johnnys death, and discover from a newspaper article that he and his mother were murdered. Roberta becomes upset and angry that two innocent people were killed and also by the realization that her mother died violently, contrary to what she was told. Samantha announces that her parents are divorcing, and the four make a pact to always be there for one another. To put Dear Johnnys soul to rest, the girls go to the cemetery to perform another seance. Johnnys tombstone suddenly rises surrounded by bright light. A figure appears from behind, but it is only the groundskeeper who explains that the stone was damaged and is being replaced. Realizing they never resurrected Dear Johnny, they agree to stop the seances. While leaving, they notice Crazy Pete, and Samantha follows him back to Dear Johnnys grave. Realizing that he is Dear Johnnys father, she comforts him, while he advises her not to dwell on things. Some time after, the tree house is finally bought, and Samantha narrates, "The tree house was supposed to bring us more independence. But what the summer actually brought was independence from each other."

The film returns to 1991, and Chrissy goes into labor and gives birth to a girl. Later, in their old tree house, it is revealed by Roberta that Crazy Pete had died the previous year. They then discuss how happy they are in life and make another pact to visit more often.

==Cast==
===Main characters===
*Samantha Albertson (Gaby Hoffmann/Demi Moore) narrates the film. As a girl, she is considered "weird", and believes in the paranormal and conducts the seances in the graveyard with her friends, who for the most part believe it to be all pretend. From the outside, her home life appears normal with her parents and younger sister, Angela. However, her parents had been having marital issues for some time now, much to the point that it had reached a level of consistency that never seemed to bother her. However, this came to an abrupt change when one night, her father moves out, and within a few weeks she learns her mother is seeing another man. As an adult, she is a popular science-fiction author who has commitment issues. At age 12, she was the most invested in the mystery of Dear Johnny, whose spirit the girls believe they have resurrected from his grave. She alone learns the truth behind his death, and receives valuable advice that later helps her come to terms with her current struggles in life

*Roberta Martin (Christina Ricci/Rosie ODonnell) is the proclaimed tomboy of the group, stemming primarily from her upbringing in a family consisting of her, her father and three older brothers, her mother having died in a tragic car accident when she was four. As a result, she became a tomboy with femininity issues and would tape her breasts to flatten them, play sports, and never hesitated to fight a boy. She usually leads the girls in their rivalry with the Wormer brothers, but eventually shares a kiss with Scott Wormer (Devon Sawa). Afterwards, she no longer tapes her breasts, indicating that she accepts that she is growing into a woman. Her struggle to come to terms with her mothers death is highlighted in the film when she fakes her own death before her friends by pretending to have drowned while they were swimming, as well as in another instance in which Samantha recalls Roberta having jumped off the roof and pretended to have broken her neck earlier that summer. As an adult, she is a doctor (an obstetrician), and lives with her boyfriend. At the end of the movie, she delivers Chrissys baby.

*Chrissy DeWitt (Ashleigh Aston Moore/Rita Wilson) was raised by an overbearing, fastidious mother (played by Bonnie Hunt) who sheltered her. Her naivete, particularly about all things sexual, is often laughed at by her friends. She is the "good girl," who chastises the others for cussing (as children and adults). Being the most responsible, she closely monitors the "tree house money" they are saving. She always questions the others schemes, but is fiercely loyal to them.  As an adult, she marries the nerdy Morton Williams, and they live in her mothers old house. The pending birth of her first child brings Samantha and Teeny back to their hometown.

*Tina "Teeny" Tercell (Thora Birch/Melanie Griffith) lives with her rich country-club parents who are rarely around, which according to Samanthas narration is, " a typical upbringing for actors and pathological liars." Teeny loves glamour, dressing up, and using makeup, and watches the movies at the drive-in theater from her rooftop. Among the girls, she is the most interested in sexuality and boys and often flirts. Teeny desires a bigger bust, and has breast implants when shes an adult. She is now a successful actress and has had multiple marriages. The limousine she arrives in is later used to transport Chrissy to the hospital when she goes into labor.

===Supporting===
 
*Tucker Stone as Young Morton Williams
*Carl Espy as Dr. Morton Williams
*Devon Sawa as Scott Wormer
*Lolita Davidovich as Mrs. Albertson, Samanthas Mom
*Rumer Willis as Angela Albertson, Samanthas sister (credited as Willa Glen)
*Cloris Leachman as Grandma Albertson
*Hank Azaria as Bud Kent, Mrs. Albertsons boyfriend
*Bonnie Hunt as Mrs. DeWitt, Chrissys Mom
*Janeane Garofalo as Wiladene
*Walter Sparrow as Crazy Pete
*Bradley Coryell as Wormer Brother
*Travis Robertson as Wormer Brother
*Justin Humphrey as Wormer Brother
*Brendan Fraser as Vietnam veteran (uncredited) Toby Ganger as himself
 

==Reception==
The film was released on October 20, 1995 and was critically panned. Based on reviews collected by   likewise did not recommend the movie and wished that the story had focused more on the adults than the "inconsequential" story of the children.  Ebert opined the reverse. They both praised the talent of all four young lead actresses.

==Soundtrack and score==
Columbia Records released a soundtrack album on October 17, 1995. Except for Susanna Hoffss end credit song, the album was made up of tunes from the period.

# "Sugar, Sugar" – The Archies (2:45)
# "Knock Three Times" – Tony Orlando/Dawn (2:54)
# "I Want You Back" – The Jackson 5 (2:53)
# "Signed, Sealed, Delivered Im Yours" – Stevie Wonder (2:39) Band of Gold" – Freda Payne (2:53)
# "Daydream Believer" – The Monkees (2:49) No Matter What" – Badfinger (2:59)
# "Hitchin a Ride (Vanity Fare song)|Hitchin a Ride" – Vanity Fare (2:55) Free (5:29)
# "Im Gonna Make You Love Me" – Supremes/Temptations (3:06)
# "Ill Be There (The Jackson 5 song)|Ill Be There" – The Jackson 5 (3:56)
# "Now and Then" – Susanna Hoffs (5:34)

Varèse Sarabande issued an album of Cliff Eidelmans score on October 24, 1995.

# "Main Title" (3:05)
# "Remembrance" (1:57)
# "A Secret Meeting" (2:11)
# "On the Swing" (1:26)
# "Its My Mom" (2:32)
# "Spirits Are Here" (2:17)
# "Sams Dad Leaves" (1:56)
# "Its a Girl" (1:48)
# "Roberta Fakes Death" (1:26)
# "Best Friends for Life" (3:07)
# "Pete Saves Sam" (2:29)
# "The Pact" (3:10)
# "No More Seances" (1:44)
# "Rest in Peace Johnny" (4:22)

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 