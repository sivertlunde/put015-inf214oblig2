Raising a Riot
{{Infobox film
| name           = Raising a Riot
| image          = 
| caption        = 
| director       = Wendy Toye 
| producer       = 
| writer         = Ian Dalrymple Hugh Perceval
| based on = novel by Alfred Toombs
| starring       = Kenneth More 
| cinematography = 
| music          = 
| studio = London Films
| distribution   = British Lion Films
| released       = 1955
| runtime        = 
| country        = United Kingdom
| language       = English
| gross = £231,148 (UK) 
}}Raising a Riot is a 1955 British comedy film directed by Wendy Toye and starring Kenneth More, Shelagh Fraser and Mandy Miller about a naval officer who attempts to look after his three children in his wifes absence.  

==Plot==
Commander Peter Kent of the Royal Navy and his wife May have three children, ranging form five to eleven years: Peter, Anne and Fusty. Kent comes home after three years abroad with no idea how to handle the children. When Mary has to fly to Canada, Peter takes his children to his fathers new country home, which turns out to be a windmill. They end up clashing with an American family in the neighbourhood.

==Cast==
* Kenneth More – Peter Kent
* Shelagh Fraser – Mary Kent
* Mandy Miller – Anne Kent
* Gary Billings – Peter Kent
* Fusty Bentine – Fusty Kent
* Ronald Squire – Grampy
* Olga Lindo – Aunt Maud
* Lionel Murton – Hary
* Mary Laura Wood – Jacqueline
* Jan Miller – Sue
* Nora Nicholson – Miss Pettigrew
* Anita Sharp-Bolster – Mrs Buttons
* Michael Bentine – The Professor
* Dorothy Dewhurst – Mother
* Robin Brown – Junior

==Production==
The movie was based on a book by American writer Alfred Toombs. The book was based on Toombs real life experience of having to look after his children after having been away from them at war for three years. 

==Reception==
The film was the eighth most popular movie at the British box office in 1955. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 