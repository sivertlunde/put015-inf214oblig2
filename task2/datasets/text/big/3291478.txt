Jamaica Inn (film)
 
 
{{Infobox film
| name           = Jamaica Inn
| image          = Original movie poster for the film Jamaica Inn.jpg
| caption        = Film poster for the US release
| director       = Alfred Hitchcock
| producer       = Erich Pommer Charles Laughton Joan Harrison Alma Reville J. B. Priestley
| starring       = Charles Laughton Maureen OHara Emlyn Williams
| music          = Eric Fenby Harry Stradling
| editing        = Robert Hamer
| studio         = Kino International, Ltd.
| distributor    = Mayflower Pictures (UK) Paramount Pictures (US)
| released       = 15 May 1939 (UK) 13 October 1939 (US)
| runtime        = 108 minutes (UK) 98 minutes (US)
| country        = United Kingdom
| language       = English
| budget         =
| gross         =
| preceded_by    =
| followed_by    =
}} novel of The Birds"). It stars Charles Laughton and Maureen OHara. It is the last film Hitchcock made in the United Kingdom before he moved to the United States.
 pub on the edge of Bodmin Moor. The score was written by Eric Fenby.

==Plot== Cornish coast. Then they kill the surviving sailors and steal the cargo from the wrecks.
 Sir Humphrey Pengallan (Charles Laughton).  She meets him and requests the loan of a horse so she can go back to Jamaica Inn.  Pengallan warns her not to go there.  Nevertheless, Mary says she just came from Ireland, and as the orphaned niece of Josss wife Patience (Marie Ney), she intends to live at Jamaica Inn. The next day, Pengallan accompanies Mary to Jamaica Inn.

When Joss meets Mary, he tries to kiss her and push himself on her.  Patience is distraught to learn her sister, Marys mother, has died. When Mary tells Joss that Pengallan has accompanied her to Jamaica Inn, Joss goes upstairs and reports to Pengallan.  We learn that Pengallan is the secret criminal mastermind behind the wrecking gang, who finds out when well-laden ships are passing near the coast, determines when and where the wrecks are to be done, disposes of the stolen cargo, and uses the booty to support his lavish lifestyle, passing a small fraction of the takings back to Joss and the gang.

In another part of the inn, the gang convenes to discuss why they get so little money for their efforts.  They suspect Jem Traherne (Robert Newton), a gang member who has been with them for only two months, of embezzling goods.  They hang him from one of the rafters of the inn, but Mary cuts the rope afterwards and saves his life. Together, Traherne and Mary flee from the gang.  They are almost recaptured and have to swim for their lives in order to escape.

The next morning, they seek the protection of Pengallan, unaware that Pengallan is the secret leader of  Josss gang.  Traherne reveals to Pengallen that he is actually an undercover law-officer on a mission to investigate the wrecks. Pengallan is alarmed, but he maintains his composure and pretends to join forces with Traherne.  Mary overhears this conversation and goes to the inn to warn Patience that law enforcement knows about the wrecking gang and that she must flee in order to avoid being arrested as an accomplice.  However, Patience refuses to leave her husband.  Traherne and Pengallan also go to the Inn to search for evidence.  While Traherne is searching the Inn, Joss and the gang return. Pengallan takes Joss aside and tells him a wreck must be performed immediately because he needs the money to escape to France now that the gangs activity has come to the attention of law enforcement.  He advises Joss to go into hiding as well as soon as the wreck is complete.  Joss and the gang go to do the wreck, leaving Traherne tied up.  Joss also pretends to tie up Pengallan as a ploy to fool Traherne, but he secretly leaves Pengallans hands free.

After the gang leaves, Pengallan releases himself from his bonds and reveals himself to Traherne.  He gives Patience a loaded pistol and instructs her to shoot Traherne if he gets loose.  Pengallen then leaves.  Traherne reasons with Patience, and promises to let her and Joss escape if she releases him.  She agrees and unties him.  He sets out for a nearby military camp to get backup.  Meanwhile, Mary goes to the wrecking site, and re-lights the warning beacon which the wreckers had extinguished.  The crew of the ship see the beacon and turn away; the ship does not wreck.  The gang capture Mary and resolve to kill her for preventing the wreck. Joss rescues her and the two escape by horse-cart, but Joss is shot in the back while escaping and collapses when they reach Jamaica Inn. As Patience is about to tell Mary that Pengallan is the secret leader of the wrecking gang, Pengallan shoots and kills Patience from offstage.  Joss then dies of his wound as well.  While Mary is reeling from the shock of witnessing these two violent deaths, Pengallan reveals himself to her, takes her hostage, ties her hands behind her back, gags her, and tells her that he plans to keep her and take care of her now that she has no one else in the world.  He drives her, still tied up and covered by a heavy cloak, to the harbor and they board a ship bound for France.

The gang returns to Jamaica Inn to find Joss and Patience dead.  Just then, Traherne arrives with a posse of soldiers, who take the gang into custody.  Traherne goes to the harbor with some of his soldiers to rescue Mary, and Pengallan is cornered on the ship.  He climbs to the top of a mast, where he clumsily drops his gun.  As Trahernes soldiers climb up after him, he jumps to his death, shouting "Make way for Pengallan!" As Traherne leads Mary away from the scene, Pengallans horrified butler (Horace Hodges) is left wondering to himself, with a memory of Pengallans voice ringing in his ears.

==Cast==
* Charles Laughton – Sir Humphrey Pengallan
* Leslie Banks – Joss Merlyn
* Maureen OHara – Mary Yellen
* Robert Newton – James Jem Traherne
* Marie Ney – Patience Merlyn
* Horace Hodges – Butler (Chadwick)
* Hay Petrie – Groom
* Frederick Piper – Agent Herbert Lomas – Tenant
* Clare Greet – Tenant William Devlin – Tenant
* Emlyn Williams – Harry the Pedlar
* Jeanne de Casalis – Sir Humphreys friend
* Mabel Terry-Lewis – Lady Beston
* A. Bromley Davenport – Ringwood George Curzon – Captain Murray
* Basil Radford – Lord George

==Character actors==
Besides Laughton and OHara, secondary characters are played by several notable stage-and-screen character actors of the time, including "bruiser-type" actor Leslie Banks (who played General Zharov in The Most Dangerous Game) as Joss Merlin, and Robert Newton in an uncharacteristic role as Jem Trahearne, a suave young secret-police agent.

==Production==
Charles Laughton was a co-producer on this movie, and he reportedly interfered greatly with Hitchcocks direction. Laughton was originally cast as Joss, but he cast himself in the role of the villainous Pengallan, which was originally to be a hypocritical preacher but was rewritten as a squire because unsympathetic portrayals of the clergy were forbidden by the Production Code in Hollywood. {{cite book
  | last = Harris
  | first = Richard A.
  | authorlink =
  |author2=Michael S. Lasky
  | title = The Complete Films of Alfred Hitchcock
  | publisher = Citadel Press Film Series
  | edition = revised edition
  | date = 1 December 2002
  | location =
  | pages =
  | url = http://www.britmovie.co.uk/directors/a_hitchcock/filmography/023.html
  | doi =
  | id =
  | isbn = }}  Laughton then demanded that Hitchcock give his character greater screen time. This forced Hitchcock to reveal that Pengallan was a villain in league with the smugglers earlier in the film than Hitchcock had initially planned. 

Laughtons acting was a problem point as well for Hitchcock. Laughton portrayed Pengallan as having a mincing walk, to the beat of a German waltz which he played in his head, {{cite web
  | last = Duguid
  | first = Mark
  | authorlink =
  | title = Jamaica Inn (1939)
  | work = filmonline
  | publisher = British Film Institute
  | url = http://www.screenonline.org.uk/film/id/441604/index.html
  | doi = Esmeralda opposite The Hunchback of Notre Dame, where she became an international star.

In March 1939, Hitchcock moved to Hollywood to begin his contract with David O. Selznick. Thus, as it was said in the beginning of this article, Jamaica Inn  was his last British picture, as well as one of his most successful. 

==Reception==
Critics disparaged the film, largely because of the lack of atmosphere and tension which was present in the book, with its light-hearted, often camp banter, and portly landlord, far-removed from the darker characters and sinister inn and coastline depicted in the book. Today it is considered one of Hitchcocks worst films. {{cite book
| last = Spoto
| first = Donald
| authorlink =
| title = The Dark Side of Genius: The Life of Alfred Hitchcock 
| publisher = Da Capo
| year = 1999
| pages = 184–185
| doi =
| isbn = 0-306-80932-X }}     Hitchcock himself was disgusted with the film even before it was finished and stated that it was a "completely absurd" idea.    However, the film still garnered a large profit (US$3.7 million, a huge success at the time) at the box office. {{cite book
  | last = Leitch
  | first = Thomas
  | authorlink =
  | title = The Encyclopedia of Alfred Hitchcock: From Alfred Hitchcock Presents to Vertigo
  | publisher = Facts on File
  | date = 31 May 2002
  | location =
  | pages =
  | url = http://www.labyrinth.net.au/~muffin/jamaica_irony.html
  | doi =
  | id =
  | isbn = }} 
Daphne du Maurier was also not pleased with the finished production and for a while she considered withholding the film rights to Rebecca. 

In 1978, film critic Michael Medved gave Jamaica Inn a place in his book The Fifty Worst Films of All Time. 

== References ==
 

== External links ==
* 
*  
* 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 