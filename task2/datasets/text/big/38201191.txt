Little Miss London
{{Infobox film
| name           = Little Miss London
| image          =
| caption        = Harry Hughes 
| producer       = 
| writer         =  Frank Stanmore Pauline Johnson
| music          =
| cinematography =  
| editing        = 
| studio         = British Instructional Films
| distributor    = Fox Film Corporation
| released       = March 1929
| runtime        = 6,912 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent comedy Harry Hughes Frank Stanmore and Reginald Fox. It was made by British Instructional Films at Bushey Studios. A business magnate poses as a poor man, while his daughter falls in love with a man posing as an aristocracy (class)|aristocrat. 

==Cast==
* Pamela Parr as Moly Carr Frank Stanmore as  Ephraim Smith
* Reginald Fox as Burton Gregg Pauline Johnson as Jill Smith
* Eric Bransby Williams as Jack
* Marie Ault as Mrs Higgins
* Charles Dormer as Lord Blurberry

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.
* Shafer, Stephen C. British Popular 1929-1939: The Cinema of Reassurance. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 