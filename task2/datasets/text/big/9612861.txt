Disorderlies
{{Infobox film
| name           = Disorderlies
| image          = Disorderlies movie poster.jpg
| caption        = The theatrical poster for Disorderlies
| director       = Michael Schultz George Jackson Michael Jaffe Doug McHenry Joseph E. Zynczak
| writer         = Mark Feldberg Mitchell Klebanoff
| starring       = The Fat Boys Ralph Bellamy Tony Plana Anthony Geary
| music          = Anne Dudley J. J. Jeczalik
| distributor    = Warner Bros.
| released       =  
| runtime        = 86 min
| country        = United States English
| budget         =
| gross          = $10,348,437
}} rap group, The Fat Boys, and Ralph Bellamy.

==Plot summary== orderlies he can possibly find. The trio, Markie, Buffy and Kool, only mean well, however, and their good-natured antics actually help re-energize the ailing Albert. They find out about Winslows scheme and try to stop it from taking place.

==Cast== Mark Morales as Markie Darren Robinson as Buffy Damon Wimbley as Kool
* Ralph Bellamy as Albert Dennison
* Anthony Geary as Winslow Lowry
* Tony Plana as Miguel Marco Rodríguez as Luis Montana
* Helen Reddy as Happy Socialite
* Sam Chew Jr. as the Doctor
* Ray Parker Jr. as the Pizza Deliveryman
* Robert V. Barron as the Funeral Home Director

==Box Office==
The movie made more than $10 million at the box office. 

==Soundtrack==
In addition to the score co-composed by Anne Dudley from the Art of Noise, the soundtrack features The Fat Boys performing a cover version of The Beatles "Baby, Youre a Rich Man", as well as other rap, pop and rock tracks. The soundtrack CD was last issued in 1995 and has since gone out of print.

===Track listing===
# "Baby, Youre a Rich Man" – The Fat Boys
# "I Heard a Rumour" – Bananarama
# "Disorderly Conduct" – Latin Rascals
# "Big Money" – Ca$hflow Anita
# "Edge of a Broken Heart" – Bon Jovi
# "Trying to Dance" – Tom Kimmel
# "Roller One" – Art of Noise
# "Fat Off My Back" – Gwen Guthrie
# "Work Me Down" – Laura Hunter

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 