My Darling Is a Foreigner
{{Infobox animanga/Header
| name            = 
| image           = 
| caption         = 
| ja_kanji        = 
| ja_romaji       = 
| genre           = Romance
}}
{{Infobox animanga/Print
| type            = movie
| author          = Saori Oguri
| publisher       = Media Factory
| publisher_other =     Digital Manga Publishing
| demographic     = 
| magazine        = 
| first           = 2002
| last            = 
| volumes         = 3
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = 
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        =  
| runtime         = 
}}
 

  is a manga series written by Saori Oguri. The English subtitle of the Japanese editions of the first two entries is "My darling is ambidextrous"; the official English version is called My Darling is a Foreigner.

The books are published by Media Factory, Incorporated in Japan, and bilingual versions have been produced. 

The series is about Oguris relationship with Tony László, an American writer of half-Hungarian half-Italian descent who lives in Tokyo.  The book Is He Turning Japanese?, a spin-off of the main manga written by László and illustrated by Oguri, was published by Digital Manga Publishing in North America. 

==Film version==
{{Infobox film
| name           = My Darling Is a Foreigner
| image          = My Darling Is a Foreigner.jpg
| alt            = 
| caption        = 
| director       = Kazuaki Ue
| producer       = 
| writer         = 
| screenplay     = Satomi Oshima
| story          = 
| narrator       = 
| starring       = Mao Inoue Jonathan Sherr
| music          = Tokio Noguchi
| cinematography = Hitoshi Kato
| editing        = Yoshimasa Kogure
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

A film adaptation of the manga was produced in 2010. Official media related to the film version spell the title in English as My darling is a foreigner.

Mao Inoue and Jonathan Sherr star in the film version, released in Japan in April 2010.  

===Cast===
* Mao Inoue as Saori Oguri
* Jonathan Sherr as Tony László
* Ryoko Kuninaka as Mika Oguri
* Naho Toda as Orange Pop Editor
* Jun Kunimura as Saoris dad
* Shinobu Otake as Saoris mom
* Daijiro Kawaoka
* Dante Carver
* Masato Irie
* Takumi Bando

===Reception===
Mark Shilling of The Japan Times gave the film a rating of 2.5 stars out of a total of 5 stars.     He praised actor Jonathan Sherr as "miles above the usual foreign actor in Japanese films, meaning he is not totally embarrassing to watch", but added that when he is "required to show something deeper than affability, he is lost".  The Hollywood Reporter also gave the film an average review.  Variety (magazine)|Variety stated that "narrative tension flounders due to excessive fidelity to Oguris experience. Sherr has little to do but be sweet and understanding; Inoue is more rounded, but cant transcend the scripts limitations. Occasional use of Oguri-style animation will appeal to the mangas fans, but merely emphasize the charm thats mostly missing. Helming and lensing are as flat as Sherrs character. Tech credits are solid."  Joe Kern in Metropolis (free magazine)|Metropolis criticized its stereotyped portrayals. 

==References==
 

==External links==
 
 
*  
*   from JapanReviewed.

 
 
 
 
 
 