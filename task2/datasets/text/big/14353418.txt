Maisie (film)
{{Infobox film
| name           = Maisie
| image	         = Maisie FilmPoster.jpeg
| caption        = Theatrical Film Poster
| director       = Edwin L. Marin
| producer       = J. Walter Ruben
| writer         = Wilson Collison (novel) Mary C. McCall, Jr.
| narrator       = Robert Young Cliff Edwards Edward Ward
| cinematography = Leonard Smith
| editing        = Fredrick Y. Smith
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Robert Young Maisie Ravier. In Mary C. McCall, Jr.s screenplay, Maisie is stranded penniless in a small Wyoming town, takes a job at a ranch, and gets caught in a web of romantic entanglements.

==Plot== burlesque showgirl midway Carnival shooting gallery.
 Robert Young), the manager of a ranch. Slim accidentally drops his wallet full of money. Rico picks it up and leaves town. Slim has Maisie arrested for theft, but when a search finds she only has 15 cents, he admits his mistake. The deputy sheriff informs Maisie that as a vagrant she must leave town by midnight, so she hides in the back of Slims truck. When Slim returns to the ranch, he is displeased to discover the stowaway.
 Ian Hunter) John Hubbard).

Maisies warm personality gradually overcomes Slims hostility.  Slims demeanor is the result of past hard luck: he confessed to embezzlement to protect his girlfriend and spent a year in prison, only to discover after his release that she had run off with another man. Maisie also becomes friends with Cliff.

Maisie and Cliff volunteer to drive needed supplies to the old ranch house, but their car overturns and Cliff is pinned under the wreck. Maisie limps to the house and walks in on Sybil kissing Ray Raymond. Maisie sends the ranch hands to rescue Cliff, who is not seriously injured.

Slim asks Maisie to marry him, and she gleefully accepts. Sybil privately confronts Maisie about Ray. Maisie informs her that she has told no one, to spare Cliffs feelings, but Sybil remains fearful that Maisie may expose her affair. Sybil lies to Slim that Maisie has been pursuing Cliff romantically, and that she only settled for Slim after she realized that Cliff would not leave her. Slim confronts Maisie.  Maisie is insulted by Slims lack of trust, so she breaks their engagement and leaves.

Cliff commits suicide after realizing his wife is still unfaithful. The death is ruled a homicide, and Slim is accused of the crime. Maisie rushes to the courtroom, but she is unable to convince the judge that Slim is innocent. Fortunately, Cliff mailed a letter to his lawyer to deliver to Maisie. The letter details the reasons for Cliffs suicide, exonerating Slim, and names Maisie as Cliffs sole heir. Maisie inherits the ranch and plenty of money to run it.

==Cast==
* Ann Sothern as Maisie Ravier / Mary Anastasia OConnor  Robert Young as Charles "Slim" Martin
* Ruth Hussey as Sybil Ames  Ian Hunter as Clifford "Cliff" Ames 
* Cliff Edwards as "Shorty" Miller, ranch hand  John Hubbard as Richard "Ray" Raymond (as Anthony Allan) 
* Art Mix as "Red" Donnen, ranch hand 
* George Tobias as Rico 
* Richard Carle as Roger Bannerman 
* Minor Watson as Prosecuting Attorney 
* Harlan Briggs as Deputy Sheriff Cal Hoskins 
* Paul Everton as Judge 
* Joseph Crehan as Defense Attorney Wilcox 
* Frank Puglia as Mr. Ernie, a barber 
* Willie Fung as Lee, the ranch cook Charles Coleman as Hicks

==References==
 	

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 