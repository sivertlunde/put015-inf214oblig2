Exodus (2007 British film)
{{Infobox film
| name           = Exodus
| image          =
| image_size     =
| caption        = 
| director       = Penny Woolcock
| producer       = Ruth Kenley-Letts Andrew Litvin
| writer         = Penny Woolcock
| starring       = Bernard Hill Clare-Hope Ashitey Daniel Percival Louis Constant Tom Wiles
| music          = Malcolm Lindsay
| cinematography = Jakob Ihre
| editing        = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Exodus is a contemporary retelling of the Biblical story of Book of Exodus|Exodus, that was released in 2007. It was directed by Penny Woolcock and was shot on location in Margate, Kent, England. The film, which had a working title The Margate Exodus features the burning of a large sculpture of a man made out of waste by Antony Gormley. The film was shown on Channel 4 on 19 November 2007.

== Synopsis ==
The leader of the country, called Pharaoh (who is plagued by voices), declares war upon societys undesirables. Drug abusers, refugees, criminals and the homeless are all considered equally worthless and entered into a restricted ghetto, called Dreamland (a former fun fair), where they cannot leave.

When Moses learns he was adopted by Pharaoh and is actually the son of an asylum seeker, he shuns his life of privilege to lead the ghettos inhabitants in a revolt against his father.

A series of plagues echoing the biblical story attack the inhabitants of the promised land including algal blooms which turn the sea red (like the first plague of turning the River Nile to blood) and a computer virus (possibly a modern reworking of the sickness in live stock plague).

The plagues are treated as acts of terrorism by those in the promised land and violent reprisals are sought against those in Dreamland.

== External links ==
*   
*  

 

 
 
 
 
 
 


 