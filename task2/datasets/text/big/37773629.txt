Delivery Man (film)
{{Infobox film
| name           = Delivery Man
| image          = Delivery Man Poster.jpg
| alt            =
| caption        = Theatrical release poster Ken Scott
| producer       = André Rouleau
| based on =  
| screenplay         = Ken Scott
| starring       = Vince Vaughn Chris Pratt Cobie Smulders
| music          = Jon Brion
| cinematography = Eric Edwards
| editing        = Priscilla Nedd-Friendly DreamWorks Pictures Reliance Entertainment Walt Disney Studios Motion Pictures
| released       =   
| runtime        = 104 minutes  
| country        = United States
| language       = English
| budget         = $26 million 
| gross          = $51.2 million 
}} Ken Scott, produced by DreamWorks Pictures, and starring Vince Vaughn, Chris Pratt, and Cobie Smulders. The film was released by Touchstone Pictures on November 22, 2013. It is a remake of Scotts 2011 French-Canadian film, Starbuck (film)|Starbuck. 

==Plot== alias he had used.
 random acts of kindness. David considers identifying himself, but after the thugs assault his father, he agrees with his lawyer to counter-sue the sperm bank for punitive damages. He wins the lawsuit: he receives $200,000 and keeps his identity a secret.

David has regrets and thinks about revealing his identity. However, if he chooses to do so, he would lose the $200,000 that was won in the countersuit. He reveals to his father that he is Starbuck. His father decides to pay off Davids debt. David finally reveals his identity on Facebook. He goes to Emmas house and finds that she is going into premature labor. At the hospital, his baby is born, he proposes to Emma, and many of the children show up to see him.

==Cast==
* Vince Vaughn as David Wozniak
* Chris Pratt as Brett
* Cobie Smulders as Emma
*   as Mikolaj Wozniak
* Simon Delaney as Victor 
* Bobby Moynihan as Aleksy
* Dave Patten as Adam  
* Adam Chanler-Berat as Viggo 
* Britt Robertson as Kristen
* Jack Reynor as Josh
* Madison McGrew as Rachel
* Matthew Daddario as Channing  Jessica Williams as Tanya

Talk show hosts Jay Leno and Bill Maher have cameo appearances as themselves.

==Production==
The film is a remake of the 2011 French-Canadian film Starbuck (film)|Starbuck which was also directed by Scott; Starbuck was also the working title for the English-language production. 
 Filming began in October 2012 in the Hudson Valley of New York. Filming then moved to New York City locales, in Brooklyn      and Manhattan.  Concerning the number of extras as the children, actor Dave Patten said, "There were ten of us who were constants on the sets so we didn’t really bond with the others who were extras. But the ten of us became a huge pack of friends and it was really nice. There are usually a lot of egos on set when it comes to a big cast which causes people not to get along but we didn’t have that issue." 

==Reception==
Delivery Man received mixed reviews from critics. Review aggregation website   the film received a score of 44/100 based on reviews from 33 critics. 

Alonso Duralde of The Wrap gave the film a negative review, stating that "Delivery Man offers comedy and sentimentality in equal doses and, unfortunately, equal efficacy—the jokes, the characters and the situations arent very funny, and the would-be heart-tuggery is mostly embarrassing."  
Peter Debruge of Variety (magazine)|Variety wrote, "Delivery Man is virtually nothing like a   Vince Vaughn movie, but rather a heartfelt celebration of the act of parenthood presented under radically exaggerated circumstances... the director demonstrates the good sense not to mess with success, engineering what amounts to a scene-for-scene remake of that earlier feel-good outing&mdash;with the notable addition of Chris Pratt in his funniest supporting performance yet."    
Michael Rechtshaffen of The Hollywood Reporter wrote that "its nice to see Vaughn moving out of his fast-talking comfort zone in a role that requires him to be more quietly reactive; while Parks and Recreation’s Pratt comically raises the second banana|second-banana bar as a put-upon dad itching to get back into the courtroom. While Smulders, unfortunately, isnt given the same opportunity to show off her comedic chops regularly on display on How I Met Your Mother, acclaimed Polish actor Andrzej Blumenfeld (in his American debut) makes a more empathetic impression as the warm Wozniak family patriarch."   

==See also== French remake of Starbuck and Hindi movie "Vicky Donor"  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 