Vasantha Poornima
{{Infobox film 
| name           = Vasantha Poornima
| image          =  
| caption        = 
| director       = H. R. Bhargava
| producer       = S A Srinivas
| story          = Dinesh Babu Chi. Udaya Shankar
| writer         = Chi. Udaya Shankar
| screenplay     = Chi. Udaya Shankar
| starring       = Ambarish Priyanka Balaraj Anand
| music          = Shankar Ganesh
| cinematography = D V Rajaram
| editing        = Manohar
| studio         = Sri Chowdeshwari Art Combines
| distributor    = Sri Chowdeshwari Art Combines
| released       =  
| runtime        = 123 min
| country        = India Kannada
}}
 1993 Cinema Indian Kannada Kannada film, directed by H. R. Bhargava and produced by S A Srinivas. The film stars Ambarish, Priyanka, Balaraj and Anand in lead roles. The film had musical score by Shankar Ganesh.

==Cast==
 
*Ambarish
*Priyanka
*Balaraj
*Anand
*K. S. Ashwath
*Ramesh Bhat
*Shanimahadevappa
*Naveen
*Nadig
*Master Vijayaraghavendra
*Sushma
*Pandari Bai
*Shobha Raghavendra
*Sriraksha
*Srimathi Rao
*Rama Aravind
*Honnavalli Krishna
*Devaraj
 

==Soundtrack==
The music was composed by Shankar Ganesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hathira Hathira Barale || S. P. Balasubrahmanyam, Manjula Gururaj || Chi. Udaya Shankar || 
|-
| 2 || Lajje Yeke || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 
|-
| 3 || Manasu Manasu || S. P. Balasubrahmanyam, Manjula Gururaj || Chi. Udaya Shankar || 
|-
| 4 || Oho Darling || S. P. Balasubrahmanyam, Manjula Gururaj || Chi. Udaya Shankar || 
|-
| 5 || Samagama Sarigama || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 