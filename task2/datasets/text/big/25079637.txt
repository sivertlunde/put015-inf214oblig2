Teenage Doll
{{Infobox film
| name           = Teenage Doll
| image          = Teenage Doll poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Roger Corman
| producer       = Roger Corman
| writer         = Charles B. Griffith
| narrator       =
| starring       = June Kenney John Brinkley
| music          =
| cinematography =
| editing        =
| studio         = Woolner Brothers Allied Artists
| released       =  
| runtime        = 71 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Teenage Doll is a 1957 film directed by Roger Corman. 

==Plot== Family dysfunction; but that of their "average American" quarry is no better. Full of shadowy urban night scenes.

==Production==
Roger Corman made the film for syndicates of theatre owners. Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 84 

Charles Griffith was hired to write the script:
 They wanted a gang picture, as it was the time of the street gangs and juvenile delinquents. I told them I had one called “The Rat Pack” and they said they wanted a girl gang. So I got to work on Teenage Doll, which was Larry Woolner taking the title of   Kazan’s Baby Doll  . But the Johnson Bureau, or the Hays Office – I forget which was in at the time – rejected the story.   accessed 25 June 2012  
This meant Griffith had to rewrite the script over the weekend:
 In the original version, the girls were all stealing weapons or making weapons in order to kill the good girl...  I wrote all these jokes in English to be said in Spanish. Roger called up the only Spanish agent around for that and it wound up that they were the best actors in the picture. But they were in the background the entire time! In the foreground, this Mexican girl makes a potato grenade. She sticks a potato peeler in one end for a handle and then a double-edge razor-blade all around the potato so she could just flip the handle and the grenade would hit somebody... Another girl stole her father’s pistol from his holster and, while she’s stealing it from his bed, the phone rings and the father has a conversation on the phone without opening his eyes and hangs up again. But the Hays office made me change these things so that they were stealing these weapons to sell for money to get a lawyer to attack the girls in some legal way. I mean really obnoxious and really stupid. It all had to be redone overnight.  

==References==
 

==External links==
* 
*  at TCMDB

 
 

 
 
 
 
 
 
 