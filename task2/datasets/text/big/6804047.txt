In the Realms of the Unreal
{{Infobox Film
| name           = In the Realms of the Unreal
| image          = In the Realms of the Unreal (film poster).jpg
| image_size     = 
| caption        = 
| director       = Jessica Yu
| producer       = Susan West
| co producer    = Joan Huang
| writer         = Jessica Yu
| narrator       = Dakota Fanning
| starring       = Larry Pine Frier McCollister Wally Wingert Janice Hong
| music          = Jeff Beal
| cinematography = Russell Harper
| editing        = Jessica Yu
| distributor    = Forward Entertainment Mongrel Media Wellspring Media
| released       =  
| runtime        = 81 min.
| country        = United States English
}}

In the Realms of the Unreal (2004) is documentary film directed by Jessica Yu about American outsider artist Henry Darger (1892-1973). 

An obscure janitor during his life, Darger is known for the posthumous discovery of his elaborate 15,143-page fantasy manuscript entitled The Story of the Vivian Girls, in What is Known as the Realms of the Unreal, of the Glandeco-Angelinnian War Storm, Caused by the Child Slave Rebellion, along with several hundred watercolor paintings and other drawings illustrating the story.
 National Film Board Award for Best Documentary at the 2004 Vancouver International Film Festival.

==Content== magnum opus, which is also surveyed in detail. Interviews with his few neighbors and other acquaintances are included.

==External links==
*  at Public Broadcasting Service|PBS.org
* 

 
 
 
 
 
 
 

 