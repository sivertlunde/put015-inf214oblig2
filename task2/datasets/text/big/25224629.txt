Billy the Kid Trapped
{{Infobox film
| name           = Billy the Kid Trapped
| image_size     =
| image	=	Billy the Kid Trapped FilmPoster.jpeg
| caption        =
| director       = Sam Newfield
| producer       = Sigmund Neufeld Joseph ODonnell
| narrator       =
| starring       =
| music          =
| cinematography = Jack Greenhalgh
| editing        = Holbrook N. Todd
| distributor    =
| released       = 27 February 1942
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Billy the Kid Trapped is a 1942 American film directed by Sam Newfield.

==Plot==
Imprisoned and sentenced to death for crimes they did not commit, Billy, Fuzzy and Jeff break out of jail.  The three escapees discover that there are three impersonators who dress as them committing the crimes.  On their mission to clear their names and bring the three impersonators to justice, the trio discovers the town of Mesa Verde where outlaws are given sanctuary in exchange for paying for legal protection.

==Cast==
*Buster Crabbe as Billy the Kid
*Al St. John as Fuzzy Q. Jones
*Malcolm Bud McTaggart as Jeff Walker
*Anne Jeffreys as Sally Crane
*Glenn Strange as Boss Stanton
*Walter McGrail as Judge Jack McConnell Ted Adams as Sheriff John Masters Jack Ingram as Henchman Red Barton
*Milton Kibbee as Judge Clarke Eddie Phillips as Stage Driver Dave Evans
*Budd Buster as Montana / Fake Fuzzy

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 