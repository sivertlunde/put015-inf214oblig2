Tom and Huck
:Not to be confused with the 1918 film Huck and Tom.
{{Infobox film name           = Tom and Huck image          = Tomandhuck poster.jpg caption        = Theatrical release poster director  Peter Hewitt producer       = Laurence Mark Stephen Sommers story          = Mark Twain (novel) screenplay     = Stephen Sommers David Loughery starring       = Jonathan Taylor Thomas Brad Renfro Eric Schweig Charles Rocket Amy Wright Mike McShane Marian Seldes Rachael Leigh Cook music          = Stephen Endelman cinematography = Bobby Bukowski editing        = David Freeman studio         = Walt Disney Pictures distributor  Buena Vista Pictures released       =   runtime        = 97 minutes country        = United States language       = English gross          = $23,920,048 
}}
 adventure comedy-drama Peter Hewitt The Adventures of Huck Finn). The movie was released in the U.S. and Canada on December 22, 1995.   Retrieved July 24, 2007 
 Native American Huck Finn, a boy with no future and no family, and is forced to choose between honoring a friendship or honoring an oath, when the town drunk is accused of the murder. 

==Plot==
The film opens with Injun Joe (Eric Schweig) accepting a job from mortician Doc Robinson (William Newman). Then Tom Sawyer (Jonathan Taylor Thomas) is seen running away from home. He and his friends ride down the Mississippi River on a raft, but hit a sharp rock, which throws Tom into the water. His friends find him washed up on the shore, and Tom finds it was Huck Finn (Brad Renfro) who carried him to safety. Huck learns of an unusual way to remove warts - by taking a dead cat to the graveyard at night. There they witness Injun Joe and Muff Potter (Mike McShane), the town drunk, digging up the grave of Vic "One-Eyed" Murrell for Doc Robinson. A treasure map is discovered and when Doc tries to betray the two men, Injun Joe murders him with Muffs knife.

The next morning, Muff is charged for the murder; unfortunately, Tom and Huck had signed an oath saying that if either of them came forward about it, they would drop dead and rot. The duo then goes on a search for Injun Joes treasure map, so they can declare Muff innocent and still keep their oath. The only problem is, the map is in Injun Joes pocket. After Injun Joe finds the last treasure, he burns the map and discovers that Tom was a witness to the murder. He finds Tom and warns him that if he ever told anyone about the murder, he will kill him. However, at the time, the entire town thought he was dead, and the friendship between Tom and Huck starts to decline because of the fact that their evidence (the map) to prove Muff innocent, while preserving their oath, is destroyed.

At the trial of Muff Potter, Tom decides that his friendship with Muff is more important than his oath with Huck and tells the truth to the court, which finds Muff innocent of all charges and goes after Injun Joe. As a result, Injun Joe decides to hold up his end of the bargain by killing Tom. When Injun Joe returns to the tavern, he kills his partner Emmett for cheating him. Huck becomes angry with Tom for breaking their oath and leaves town. During a festival the next day, a group of children, including Tom and Becky Thatcher (Rachael Leigh Cook), a girl for whom he has expressed romantic interest previously in the film, enter the caves where Tom and Becky become lost. They stumble upon Injun Joe (who was looking for Tom) in McDougals Cave. He traps them, but Tom and Becky manage to escape. Then they find the treasure and Tom tells Becky to go get her father and bring him back.

Just then, Injun Joe finds Tom, and again tries to kill him. But Huck returns to help save Tom, and battles Injun Joe. But Injun Joe easily overpowers Huck, just as he is about to kill him, Tom holds the treasure chest over a chasm. Injun Joe then tries to get the chest from Tom, only to fall into the chasm to his death (with the chest which was empty). The boys reconcile, and are declared heroes by the people. Tom is praised on the front page of the newspaper, and Widow Douglas (Marian Seldes) decides to adopt Huck Finn.

==Cast==
*Jonathan Taylor Thomas as Thomas "Tom" Sawyer 
*Brad Renfro as Huckleberry "Huck" Finn
*Eric Schweig as Injun Joe
*Charles Rocket as Judge Thatcher
*Amy Wright as Aunt Polly
*Mike McShane as Muff Potter
*Marian Seldes as Widow Douglas
*Rachael Leigh Cook as Rebecca "Becky" Thatcher
*Courtland Mead as Cousin Sid
*Joey Stinson as Joseph "Joe" Harper
*Blake Heron as Benjamin "Ben" Rodgers
*Lanny Flaherty as Emmett: Injun Joes accomplice.
*Peter MacKenzie as Mr. Sneed
*Heath Lamberts as Mr. Dobbins William Newman as Doctor Robinson Andy Stahl as Sheriff
*Bronwen Murray as Cousin Mary

==Reception==

===Box office===
The movie debuted at No.9.  In its second week it rose to No.8. {{cite news|title= Weekend Box Office : A Very Happy New Years Holiday for Toy Story and Jumanji|publisher= Los Angeles Times|date= 1996-01-03|url=http://articles.latimes.com/1996-01-03/entertainment/ca-20514_1_toy-story
|accessdate=2012-06-14|first=Claudia|last=Puig}}  The U.S. and Canada box office for Tom and Huck was $23,920,048.   Retrieved 2007-07-24 

===Critical===
The movie received mixed to negative reviews,     with a rotten 25% on review aggregate Rotten Tomatoes  

==See also==
*The Adventures of Huck Finn (1993 film)|The Adventures of Huck Finn (1993) - Disneys previous Twain adaptation, also worked on by Sommers, and starring Elijah Wood, Courtney B. Vance and Jason Robards.

==References==
 

==External links==
*   
*  
*   at Rotten Tomatoes
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 