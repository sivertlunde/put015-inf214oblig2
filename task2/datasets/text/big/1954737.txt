Break of Hearts
{{Infobox film
| name           = Break of Hearts
| image          =File:Break of Hearts lobby card.jpg
| image_size     =
| caption        =Lobby card
| director       = Philip Moeller
| producer       = Pandro S. Berman
| writer         = Lester Cohen (story) Victor Heerman Sarah Y. Mason Victor Heerman
| narrator       =
| starring       = Katharine Hepburn Charles Boyer
| music          = Max Steiner
| cinematography = Robert De Grasse William Hamilton
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 78 min.
| country        = United States English
| budget         = $427,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56 
| gross          = $695,000 
}} 1935 RKO film starring Katharine Hepburn and Charles Boyer.  The screenplay was written by the team of  Sarah Y. Mason and Victor Heerman, with Anthony Veiller,  from a story by Lester Cohen, specifically for Hepburn.

Boyer played Franz Roberti, the passionate and eminent musical conductor while Hepburn was Constance Dane, an aspiring but unknown composer. She wants to see his concert, but it is all sold out. When she sneaks into his rehearsal he is smitten by her devotion and gets his orchestra to get it right as they play just for her. Constance marries Franz: he says she is "a most exciting creature" and she has been in love with him for a long time (i.e., "since late this afternoon").
 John Beal). Johnny wants to marry Constance, but she cannot forget her husband. Franz has been hitting the bottle and pretty much throwing away his career, although exactly which of his many sins is driving him to drink is not really clear. Fortunately, Constance has been working on her concerto.
 A Star Is Born but the result is one of Hepburns least successful efforts at RKO.

Originally Break of Hearts was intended as a vehicle for Hepburn and   actually was first signed-up lead, but the producers replaced him with Charles Boyer.)

This film made a slim profit of only $16,000. 

==References==
 

== External links ==
*  

 
 
 
 
 


 