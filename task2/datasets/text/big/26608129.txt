Another Year (film)
 
 
{{Infobox film
| name           = Another Year
| image          = Another year poster.jpg
| image_size     = 
| alt            = 
| caption        = British cinema poster
| director       = Mike Leigh
| producer       = Georgina Lowe
| writer         = Mike Leigh
| starring       = Lesley Manville Jim Broadbent Ruth Sheen
| music          = Gary Yershon Dick Pope
| editing        = Jon Gregory
| studio         = Thin Man Films
| distributor    = Momentum Pictures   Sony Pictures Classics  
| released       =  
| runtime        = 129 minutes  
| country        = United Kingdom
| language       = English
| budget         = $8 million 
| gross          = $18,124,262 
}} premiered at the 2010 Cannes Film Festival in competition for the Palme dOr.  The film was shown at the 54th London Film Festival before its general British release date on 5 November 2010.  At the 83rd Academy Awards, Mike Leigh was nominated for an Academy Award for Best Original Screenplay.

==Plot==
Tom Hepple, a geologist, and Gerri Hepple, a counsellor, are an older married couple who have a comfortable, loving relationship. The film observes them over the course of the four seasons of a year, surrounded by family and friends who mostly suffer some degree of unhappiness. Gerris friend and colleague, Mary, works as a receptionist at the health centre. She is a middle-aged divorcee seeking a new relationship, and despite telling everyone she is happy, appears desperate and depressed. She often seems to drink too much. The Hepples only child, Joe, is 30 and unmarried and works as a solicitor giving advice on housing.

In the summer, the Hepples are visited by Ken, Toms old friend from his student days. Ken is overweight, eats, smokes and drinks compulsively and seems very unhappy. Tom and Gerri host a barbecue in Kens honour. Mary drives her newly bought car to the party, but gets lost and arrives late. Having had some wine, she flirts with Joe, whom she has known since he was a child. He remains friendly but does not reciprocate the flirtation. After the party, Mary reluctantly gives Ken a lift to the train station. He makes a clumsy romantic advance and Mary irritably rejects him.

Months later, in the autumn, Mary is once again at Tom and Gerris home. Joe arrives with a new girlfriend, Katie. Mary appears rude and hostile towards Katie, which is not appreciated by Tom and Gerri. This creates a rift between Gerri and Mary.

In the winter, Tom, Gerri, and Joe attend the funeral for the wife of Toms brother, Ronnie. Towards the end of the service, Ronnies estranged son, Carl, arrives, and angrily asks why the ceremony was not delayed for him. At the reception at Ronnies house, Carl becomes aggressive and walks out. Tom and Gerri invite Ronnie back to London to stay with them for a while and Ronnie agrees.

While Tom and Gerri are at their garden allotment Mary arrives unannounced at their home and persuades Ronnie to let her in. Her car has just been written off and she is upset. The two have a cup of tea and a desultory chat before Mary takes a nap on the settee. When Tom and Gerri return they are unhappy to find Mary at their house. Gerri explains to Mary that she feels let down by her earlier behaviour towards Katie. Mary apologises and weeps. Gerri gradually extends a degree of warmth to Mary, suggesting that she should seek professional help and inviting her to stay for dinner, and the two women set the table. Joe and Katie arrive, their relationship still appearing strong and happy. The Hepples enjoy dinner together. Mary eats with them but appears lost and uncertain.

==Cast==
* Jim Broadbent as Tom Hepple
* Ruth Sheen as Gerri Hepple
* Lesley Manville as Mary Smith Peter Wight as Ken
* Oliver Maltman as Joe Hepple David Bradley as Ronnie Hepple
* Karina Fernandez as Katie Martin Savage as Carl Hepple
* Michele Austin as Tanya Philip Davis as Jack
* Stuart McQuarrie as Toms colleague
* Imelda Staunton as Janet

==Production== Focus Features International.  The project received £1.2 million from the UK Film Council.  The production involved a budget of around US$8 million, which Leigh said was "the lowest budget Ive had for a long time".   
 Dick Pope used four different film stocks, and much attention was paid to details in the props so that the passage of time would appear believable. 

The location used for Tom and Gerri Hepples house is St Margaret’s Road, Wanstead, East London. 

==Reception==
The film was well received by critics. According to review aggregation website, Rotten Tomatoes, 93% of critics have given the film a positive review, with a rating average of 8.2 out of 10 from 154 reviews.  The film debuted at the 2010 Cannes Film Festival in competition for the Palme dOr and although it failed to receive any prizes, it was highly praised by critics,    scoring an 3.4/4 average at Screen Internationals annual Cannes Jury Grid, which polls international film critics from publications such as Sight & Sound, The Australian, Positif (magazine)|Positif, LUnità and Der Tagesspiegel. 

Wendy Ide of   described Another Year as "a rare treat",  and Geoffrey Macnab of The Independent described the film as "an acutely well-observed study of needy and unhappy people desperately trying to make sense of their lives." 

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipient(s)
! Result
|- Academy Awards 
| 27 February 2011 Best Original Screenplay
| Mike Leigh
|  
|-
| Belgian Syndicate of Cinema Critics 
| 8 January 2011 Grand Prix
| 
|  
|- 64th British British Academy Film Awards  21 February 2011 Best Supporting Actress
| Lesley Manville
|  
|- Best British Film
|
|  
|- British Independent British Independent Film Awards 
| rowspan="4"| 5 December 2010
| Best Director
| Mike Leigh
|  
|-
| Best Actress
| Ruth Sheen
|  
|-
| Best Actor
| Jim Broadbent
|  
|-
| Best Supporting Actress
| Lesley Manville
|  
|- Cannes Film Festival 
| 23 May 2010
| Palme dOr
|
|  
|- Chicago Film Critics Association Awards 
| 20 December 2010
| Best Actress
| Lesley Manville
|  
|- European Film European Film Awards 
| rowspan="2"| 4 December 2010 Best Actress
| Lesley Manville
|  
|- Best European Composer
| Gary Yershon
|  
|- London Film London Film Critics Circle Awards  10 February 2011
| Best British Actor
| Jim Broadbent
|  
|-
| Best British Actress
| Lesley Manville
|  
|-
| Best British Actress
| Ruth Sheen
|  
|-
| Best British Director
| Mike Leigh
|  
|-
| Best British Film
|
|  
|-
| Best British Supporting Actor David Bradley
|  
|-
| Best British Supporting Actor Peter Wight
|  
|- London Film Festival Awards 
| 27 October 2010 Best Film
|
|  
|- National Board National Board of Review Awards  2 December 2010
| Top Ten Film
|
|  
|- Best Actress
| Lesley Manville
|  
|- San Diego San Diego Film Critics Society Awards  14 December 2010 Best Supporting Actress
| Lesley Manville
|  
|- Best Ensemble Cast
|
|  
|- Washington D.C. Area Film Critics Association Awards 
| 6 December 2010 Best Original Screenplay
| Mike Leigh
|  
|}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 