Now and Forever (1983 film)
 
{{Infobox film
| name           = Now and Forever
| image          = 
| image size     =
| caption        = 
| director       = 
| producer       =
| writer         = 
| based on = novel by Danielle Steel
| narrator       =
| starring       = Cheryl Ladd
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1983
| runtime        = 
| country        = Australia English
| budget         =
| gross = AU $34,671 (Australia) 
| preceded by    =
| followed by    =
}} Australian drama film directed by Adrian Carr and starring Cheryl Ladd, Robert Coleby and Carmen Duncan.  A seemingly perfect couples marriage is destroyed when the husband is accused of rape by another woman. It was based on a novel by Danielle Steel.

==Cast==
* Cheryl Ladd - Jessie Clarke
* Robert Coleby - Ian Clarke
* Carmen Duncan - Astrid Bonner
* Christine Amor - Margaret Burton
* Aileen Britton - Bethanie Alex Scott - Andrew Wundham
* Kris McQuade - Matilda Spencer John Allen - Martin Harrington
* Rod Mullinar - Geoffrey Bates
* Kevin Healy - Jock Martin Michael Long - William Horton Tim Burns - Kent Adams
* Henri Szeps - Barry York
* Redmond Phillips - Judge
* Amanda Ma - Kit
* Sarah De Teliga - Zina Ray Marshall - Harvey Green
* Paul Bertram - James Eaton
* Alan Tobin - Wayne Buttery
* Reg Gillam - Magistrate

==References==
 

==External links==
* 
*  at Oz Movies
 
 
 
 


 