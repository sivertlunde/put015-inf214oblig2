Jim Brown: All-American
{{Infobox film
| name=Jim Brown: All-American
| image= Jimbrownallamericancover.jpg
| caption = The DVD cover for Jim Brown: All-American.
| writer=
| starring=Jim Brown
| director=Spike Lee
| music=Terence Blanchard	 
| distributor=Home Box Office
| released= 
| runtime=140 min.
| language=English
| movie_series=
| awards=
| producer=Spike Lee
| budget=
}}
Jim Brown: All-American is a 2002 documentary film directed by Spike Lee.  The film takes a look at the life of NFL Hall-of-Famer Jim Brown.  The film delves into his life—past, present and future—focusing on his athletic career, acting and activism.  Many people from Hollywood and sports backgrounds were interviewed for the film.  Members of Browns family were also interviewed for the film.  It debuted on HBO and is subsequently aired at various times.

==Notable appearances==
*Art Modell
*Oliver Stone
*Stuart Scott
*Bernie Casey
*Hank Aaron
*Bill Russell
*Fred Williamson
*Raquel Welch
*Melvin Van Peebles
*Johnnie L. Cochran Jr.
*Michael Wilbon
*Joe Frazier

==External links==
* 

 

 
 
 
 
 
 


 