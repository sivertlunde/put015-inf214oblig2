Little Old New York
{{Infobox film
| name           = Little Old New York
| image          = Littleoldnewyorkalicefaye.jpg
| image_size     =
| caption        = Theatrical release poster
| writer         = Play:   Screenplay: Harry Tugend
| starring       = Alice Faye Fred MacMurray Richard Greene Henry King
| producer       = Darryl F. Zanuck Alfred Newman
| cinematography = Leon Shamroy
| editing        = Barbara McLean
| distributor    = 20th Century Fox
| released       = February 3, 1940
| runtime        = 100 minutes
| country        = United States English
| budget         = over $2 million 52 FEATURE FILMS ON FOX 39-40 LIST: Five Will Cost $2,000,000 Each--Zanuck to Supervise 24 Large Productions THE RAINS CAME ON BILL Drums Along Mohawk, Little Old New York, Brigham Young Scheduled Edmondss Story in Color Elsa Maxwell Featured
New York Times (1923-Current file)   04 Apr 1939: 29. 
|}} 1940 20th Henry King. Douglas Wood, and Donald Meek.

The film tells the story of the hardships and politics of Robert Fultons building the first successful steam-powered ship in America, which would then go on to revolutionize river and ocean transportation around the world.

==Plot ==
In this fictionalized account of the invention of the first successful steamboat, North River Steamboat|Clermont, engineer and inventor Robert Fulton (Richard Greene) comes to New York in 1807, where he meets tavern keeper Pat ODay (Alice Faye). ODay strongly supports Fulton and his visionary steamship, but her suitor Charles Browne (Fred MacMurray) isnt happy about her association with the dapper engineer, or his invention. With ODays help, Fulton obtains the financing needed to build his revolutionary paddle steamer, while Browne manages to turn every local deck hand and sail-powered passenger boat operator against the engineer, exploiting their fear of losing their livelihoods to a steam-powered vessel. In the end, against adversity and opposition, Fulton finally builds his Clermont and launches the steamboat to great success, silencing his critics and the doubters who had previously labeled his venture "Fultons Folly."

==Cast==
* Alice Faye&nbsp;– Pat ODay
* Fred MacMurray&nbsp;– Charles Browne
* Richard Greene&nbsp;– Robert Fulton Brenda Joyce&nbsp;– Harriet Livingstone
* Andy Devine&nbsp;– Commodore&nbsp;– Ferry Boat Owner
* Henry Stephenson&nbsp;– Chancellor Robert L. Livingstone
* Fritz Feld&nbsp;– Pearl Street Tavern Keeper
* Ward Bond&nbsp;– Regan&nbsp;– Shipyard Owner
* Virginia Brissac&nbsp;– Mrs. Rudolph Brevoort

==Production==
This is one of Fayes few non-musical features, and her fans complained about her not singing in it while the film was still in production, so a song was later included; Fayes participation in the added song was minimal. 

Both a 12-foot shooting miniature and a full size mock-up of Clermont were built in Hollywood for the Fox production. Both were based on plans of an original full-sized 1909 Clermont reproduction that had been broken up for scrap in New York by her owners several years before, as a result of financial hardships brought on by the Great Depression.

North River Steamboat is the actual name of the historic steamboat upon which this film is based; the vessel was never known as Clermont in its era. Arthur G. Adams, The Hudson through the Years. Westwood NJ : Lind Publications, 1983. 
 silent era 1923 version was directed by Sidney Olcott and starred Marion Davies, Stephen Carr, and J. M. Kerrigan.

==Soundtrack==
*Who Is the Beau of the Belle Of New York
**Music and Lyrics by Mack Gordon
**Performed by Tyler Brooke and joined by Alice Faye and other dancing patrons of Krausmeyers Pavilion
*Ach Du Lieber Augustine
**Traditional German folksong
**Played as dance music at Krausmeyers Pavilion
*Du, du liegst mir im Herzen
**Traditional German folksong
**Played as dance music at Krausmeyers Pavilion

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 