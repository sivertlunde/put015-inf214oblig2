See You Next Tuesday (film)
{{Infobox film
| name           = See You Next TuesdaY
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Drew Tobia
| producer       = Rachel Wolther
| writer         = Drew Tobia
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Eleanore Pienta, Dana Eskelson, Molly Plunk
| music          = Brian McOmber
| cinematography = Andrew J. Whittaker
| editing        = Sofi Marshall
| co-producer         = Jason Klorfein
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
See You Next Tuesday is a 2013 independent drama film directed by Drew Tobia and his first full length feature film.  The movie had its world premiere on October 13, 2013 at the London Film Festival and Eleanore Pienta as a pregnant woman that has a complicated personal relationship with her family.
 Sunset Park, and Bushwick, Brooklyn|Bushwick. 

==Synopsis== toxic as her relationship with her mother, and her presence soon puts an additional strain on Jordan and Sylves own relationship.

==Cast==
*Eleanore Pienta as Mona
*Dana Eskelson as May
*Molly Plunk as Jordan
*Keisha Zollar as Sylve
*Taylor Dior as Alicia
*Stephan Goldbach as Dogfood Guy
*Michele Meises as Meeting Leader
*Michele Ann Suttile as AA Member
*Levi Wilson as AA Sex Addict Maria Wilson as Naomi
*Sirita Wright as Shondra

==Reception==
Critical reception for See You Next Tuesday has been mixed to positive and Tallie Medel (via Paste (magazine)|Paste magazine) considered the film to be one of her favorite movies for 2013.  The Tucson Weekly and Toronto Star both praised the film,  and the Toronto Star commented that the film was sometimes "abrasive" and that "Flashes of humour and some unexpected moments of poignancy help temper the rough stuff".  Critics for IndieWire lauded the film, with one critic citing the movies birth scene as a highlight while the other remarked that See You Next Tuesday deserved more attention than it got.   

The Hollywood Reporter panned See You Next Tuesday overall, as they felt that it was "deeply unpleasant" and that it was "solely for viewers who demand extremes."  

===Awards===
*Jambor-Franklin Founders Award for Best Narrative at the Birmingham Sidewalk Moving Picture Festival (2013, won)
*Audience Choice Award for Best Feature Film at the Chicago Underground Film Festival (2013, won)  
*Duncan-Williams Scriptwriting Award at the Indie Memphis Film Festival (2013, won)    
*Special Jury Award for outstanding performance at the Indie Memphis Film Festival (2013, won - Eleanor Pienta) 

==References==
 

==External links==
*  
*  

 
 
 