The Sniper (2009 film)
 
 
{{Infobox film
| name = The Sniper
| image = Sniper2009filmposter.jpg
| caption = Theatrical poster
| producer = Candy Leung
| director = Dante Lam
| writer = Jack Ng
| starring = Richie Ren Edison Chen Huang Xiaoming
| editing = Angie Lam
| cinematography = Cheung Man-Po Media Asia Films Blue Fiction Media Asia Distribution (Hong Kong)
| released =  
| runtime = 
| country = Hong Kong
| language = Cantonese English
| budget = HK$30 million Sina.com. " ." 兩片棄內地市場. Retrieved on 2009-03-28. 
}}
The Sniper ( ) is a 2009 Hong Kong action thriller film directed by Dante Lam and starring Richie Ren, Edison Chen and Huang Xiaoming, as top snipers for the Hong Kong Police Force.  The film was originally set to be released in May 2008, but was delayed due to the Edison Chen photo scandal. This is Edison Chens final film in Hong Kong as of 2013.

==Plot==
Hartman Fong (Richie Ren) succeeds Shane (Bowie Lam) as the leader of Hong Kongs Special Duties Unit Sniper Team. Hartman who is by-the-book and hierarchical, got the job despite being the second best shooter in team history. The best was Hartmans former teammate Lincoln (Huang Xiaoming), whose unorthodox methods and confidence makes him arrogant and disdainful of authority. Hartman and Lincoln are two alpha males with a long-running rivalry, with only room at the top for one sharpshooter.

Lincoln however was dismissed and jailed for accidentally killing a hostage (the son of the bank chairman) during a bank robbery committed by crime boss Tao (Jack Kao). Lincoln maintained in defense that Tao was about to pull the pin of a grenade but no one else on the Sniper Team could support his story, indeed there is a feeling that Hartman (who was the only other person who could have been in a position to see Tao with the grenade) may have withheld key evidence that would have exonerated Lincoln. Back to the present, upon his release from prison after a four-year stint, Lincoln still harbors a grudge against his former SDU teammates despite being welcomed back into society by Shane. Within days Lincoln has purchased an illegal sniper rifle. He also rekindles his relationship with his wife Crystal (Mango Wong) who encourages him to seek revenge, after they view the wedding ring in the aquarium.

In a flashback, there are two rookie cops getting involved in criminal shootout. One of them is OJ, who manages to stay calm and kills a criminal. Lincoln sees potential in this youngster and decides to take him under his wing. Yet when OJ becomes fascinated by Lincoln and determines to top his shooting skills, he becomes another wild card on the loose. OJs unorthodox means leads him into frequent conflicts with Hartmans rigid authoritarian style.

Within a few days of being a free man, Lincoln helps crime boss Tao escape prisoner transfer convoy. An off-duty Hartman witnesses the incident, killing several of Taos men but unable to prevent them from breaking their boss free, after Lincoln tips him off to the crimes location as kind of a "ha ha, Im helping the bad guys," taunt. Ming and his team are dispatched to profile the suspect. OJ (Edison Chen), the hot-headed rookie on the team, surprises everyone by accurately replicating the suspect’s impossible shots. He manages to do this because of advice from Lincoln.

As Hartman investigates the suspect, he inadvertently runs into crime boss Tao and his henchmen in the elevator of an apartment building. Hartman manages to kill one of the criminals but while chasing after Tao he is foiled by Lincoln. Hartman then pursues Lincoln to the roof and Lincoln loses his grip on a rope and falls down. Tao escapes but his right-hand man is cornered by police, so he flees into a restaurant and takes hostages. Hartman, now in the police command van, coordinates his sniper team but only OJ has a good shot. Although Hartmans order is "shoot to kill", OJ instead wounds the criminal in the arm holding the gun. Although the hostage situation has been resolved without loss of innocent life, Hartman and OJ have a heated argument over the disobeyed order, with OJ arguing that this allowed for the mobsters capture so the case can be further investigated.

Later it is revealed Lincoln has survived the fall with no major injuries and continues his plan of revenge and kidnaps Shane, who is Lincolns last friend and supporter from the SDU. While Lincoln and Crystal make conversation, Shane regains consciousness and reminds him that Crystal is dead and it is revealed that Lincoln has been hallucinating all this time. In a fit of rage, Lincoln takes his rifle and fires at the ghosts of Hartman and the bank chairmans son, before realizing that he has inadvertently killed Shane. A flashback shows Crystal visiting Lincoln in prison, but he tells her to go away. Crystal then returns home and drops the wedding ring into the aquarium. Standing out on the balcony of her apartment looking at a photo of them in happier times, a wind blows away the photo, and as she lunges for it she tumbles over the railing and falls to her death (likely a suicide). 

In the ending, Lincoln decides to lure Fong and the SDU sniper team into a trap at an auto scrap warehouse, leading to the ultimate showdown amongst three expert snipers. First Lincoln forces the crime boss Tao reenact the bank robbery hostage taking, and unlike four years ago, this time Lincoln successfully kills Tao and saves the hostage. In the final gun battle, most of Hartmans team is wounded or killed, but Hartman volunteers to sacrifice himself and draw Lincolns fire, allowing OJ to kill Lincoln and become the best sniper in the SDU.

==Cast==
*Richie Ren as Hartman Fong
*Edison Chen as OJ
*Huang Xiaoming as Lincoln Ching
*Mango Wong as Cystal
*Michelle Ye as Mon
*Bowie Lam as Shane
*Wilfred Lau as Iceman
*Charmaine Fong as Kit
*Jack Kao as Tao, a ruthless Triad leader and one of the antagonists of the film
*Liu Kai Chi as Er Ge, Taos right hand lieutenant
*Patrick Tang as Police Chung (Cameo)

==Production==
Principal photography commenced in August 2007 with a budget at HK$30 million.   The Sniper was originally set to be released in May 2008, but was delayed due to the Edison Chen photo scandal. Many of the scenes were edited to limit scenes of Edison Chen due to the scandal.

==Release== death threat letter, but was able to participate in the Singaporean promotions.  
 Breaking News, Anthony Wong bulletproof vest during the promotions as they can just aim for the head. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 