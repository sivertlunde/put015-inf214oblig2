Rendezvous in Paris
{{Infobox film
| name           = Rendezvous in Paris
| image          = "Rendezvous_in_Paris".jpg
| image_size     =
| caption        = 
| director       = Gabi Kubach
| producer       = Susi Dölfes Jan Kadlec Mathias Wittich
| writer         =   Gabi Kubach
| narrator       = Barry Stokes
| music          = Paul Vincent Gunia (as Paul Vincent)
| cinematography =  Helge Weindler
| editing        = Rolf Basedow
| studio         = France 3 (FR 3)  Bavaria-Filmkunst
| distributor    = Bavaria-Filmkunst Verleih  (all media)
| released       = 11 December 1982 (France) 23 December 1983 (West Germany)
| runtime        = 104 minutes
| country        = France / West Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} German film Barry Stokes. It is based on a novel by Vicki Baum.

==Plot==
In Berlin in 1930, sensitive Evelyne Droste (Claude Jade) leads a sheltered life married to respectable lawyer Kurt (Harald Kuhlmann). The children are cared for by a governess and a nanny, and she feels somehow superfluous and unfulfilled on a personal level. But when she meets the American Frank Davies (Barry Stokes) at a party, her passion and vitality return. Evelyn finally agrees to Franks invitation for a weekend in Paris. But while Evelyn puts her middle-class life on the line, for Frank it is perhaps only an adventure ... 

==Cast==
*  Claude Jade - Evelyne Droste
* Harald Kuhlmann - Kurt Droste Barry Stokes - Frank Davis
* Vérénice Rudolph - Marianne
* Gunther Malzacher - Von Gebhardt
* Frantisek Peterka - Herr Rupp
* Barbara Morawiecz - Frau Rupp
* Chantal Bronner - Mlle Michel

==External links==
* 

==References==
 

 
 
 
 
 


 
 