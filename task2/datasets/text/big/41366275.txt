Earth to Echo
{{Infobox film
| name=Earth to Echo
| image=Earth to Echo.jpg
| caption= Theatrical release poster Dave Green
| producer=Ryan Kavanaugh Andrew Panay
| story=Henry Gayden Andrew Panay
| screenplay=Henry Gayden Brian "Astro" Bradley Reese C. Hartwig Ella Wahlestedt
| music=Joseph Trapanese
| cinematography=Maxime Alexandre
| editing=Carsten Kurpanek Crispin Struthers
| studio= Panay Films
| distributor=Relativity Media
| released=  
| runtime=89 minutes 
| country=United States
| language=English
| budget=$13 million 
| gross= $45.3 million   
}} found footage science fiction adventure film Dave Green, and produced by Robbie Brenner and Andrew Panay. The film was originally developed and produced by Walt Disney Pictures, who eventually sold the distribution rights to Relativity Media, which released the completed film in theaters on July 2, 2014.

==Plot==
Alex Nichols, Tuck Simms, and Reginald "Munch" Barrett are a trio of inseparable friends whose lives are about to change. Their Las Vegas suburb, Mulberry Woods, is being destroyed by a highway construction project that is forcing their families to move away. They mourn what will surely be the end of their happiness and friendship as their families move to separate ends of the country.

During the last week together before Alex moves to another foster home, Alexs phone, as well as his familys, begins "barfing"—displaying weird electronic signals. Munch and Tuck figure out these signals only start at a certain point in the neighborhood: Alexs backyard. At one point men from the construction crew visit their homes, offering to replace the "barfed" phones, which they claim are a result of an electrical shortage on the construction site, however the boys refuse to take advantage of this and keep their malfunctioning phones.

Munch discovers that the image on his phone is identical to a desert about 20 miles away. While at school, they plan to tell their parents they are sleeping out at one of the other boys houses and then ride their bikes out to the desert to find what the image leads to.
 alien robot pawn shop; a house that happens to be Emmas, (a girl at their school all three have a vague crush on - and who then joins them to get away from her parents); a bar; and a closed amusement arcade.

However, they are being chased by government officials who have gone undercover as construction workers to investigate a spaceship that entered Earths atmosphere near the construction site. They shot Echo down believing that if Echo rebuilds his ship it will kill everyone on Earth. After collecting a few of the pieces, the kids and Echo are caught, and the government is almost able to kill Echo before they escape. They steal the governments van and follow the last map on their phones to reach Echos spaceship.

The map leads them to Alexs backyard; the spaceship was under the neighborhood all along. Tuck, Munch, and Emma are concerned that when the spaceship takes off it will destroy the small town, but Alex puts Echo in the spaceship anyway and tearfully says goodbye. The spaceship rebuilds itself by pulling each piece out of the ground, sparing the neighborhood any real damage, and blasts off into the sky. Only the four kids are around to see it (except Munchs mother, whom no one believes) and everyone thinks the holes appear because of a brief earthquake.

Having been wrong about their predictions regarding the spaceship, the government officials depart. The kids parents discover that they were out all night, getting them in trouble. While they saved their neighborhood, Alex and Munch still have to move away;  Tuck is able to stay but regrets that it isnt the same without his friends. Despite this, the group realizes that true best friends remain so despite whatever distance separates them, miles or light years, and they remain friends for life.

==Cast==
 
* Teo Halm as Alex Nichols Brian "Astro" Bradley as Tuck Simms
* Reese Hartwig as Reginald "Munch" Barrett
* Ella Wahlestedt as Emma
* Jason Gray-Stanford as Dr. Lawrence Masden
* Alga Smith as Marcus Simms
* Cassius Willis as Calvin Simms
* Sonya Leslie as Theresa Simms
* Kerry OMalley as Janice Douglas
* Virginia Louise Smith as Betty Barrett
* Peter Mackenzie as James Hastings
* Valerie Wildman as Christine Hastings
* Mary Pat Gleason as Dusty (Mullet Lady at Bar)
* Chris Wylde as Security Guard
*Brooke Dillman as Diner Waitress
* Myk Watford as Blake Douglas
* Tiffany Espensen as Charlie
* Israel Broussard as Cameron
* Drake Kemper as Mookie
* Sean Carroll as Podcast Voice (voice)
 

==Production== Walt Disney final cut distribution rights to Relativity Media in 2013.   

==Distribution==

===Release===
The film was initially scheduled for release on January 10, 2014 and April 25, 2014.    After being delayed, Earth to Echo premiered on June 14, 2014 at the Los Angeles Film Festival and opened in theaters across the U.S. on July 2, 2014.

===Marketing===
The first trailer was released on December 12, 2013. 

===Home media===
The film was released on DVD and Blu-ray on October 21, 2014. 

===Box office===
Earth to Echo opened on July 2, 2014 in the United States in 3,179 theaters, ranking at #6, and accumulating $8,364,658 over its 3-day opening weekend (an average  of $2,590 per venue) and $13,567,557 since its Wednesday launch.  , the film had grossed $38.9 million in the U.S. and $6.4 million overseas, for an total of $45.3 million worldwide, against a $13 million budget, making it a moderate box office success. 

===Critical reception=== rating average of 5.4 out of 10.   the website had assigned the film a score of 48%.    Another aggregator Metacritic surveyed 31 critics and assessed 14 reviews as positive, 15 as mixed, and 2 as negative. Based on the reviews, it gave the film a score of 53 out of 100, which indicate "mixed or average reviews". 

==Awards and nominations==
{| class="wikitable plainrowheaders" style="text-align:center;"
|-
! scope="col" | Year
! scope="col" | Award
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
| scope="row" | 2014 Teen Choice Awards
| Choice Summer Movie 
| Earth To Echo
|  
|-
| scope="row" | 2014
| Heartland Film Festival
| Truly Moving Picture Award 
| Earth To Echo
|  
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 