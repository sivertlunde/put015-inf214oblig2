Front Page Woman
{{Infobox film
| name           = 
| image          = Front-page-woman-1935.jpg
| image_size     =180px
| caption        = Theatrical release poster 
| director       = Michael Curtiz
| producer       = Samuel Bischoff
| writer         = Laird Doyle Lillie Hayward Roy Chanslor
| narrator       = 
| starring       = Bette Davis George Brent
| music          = 
| cinematography =
| editing        =
| distributor    = Warner Bros.
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Front Page Woman is a 1935 American comedy film directed by Michael Curtiz. The screenplay by Laird Doyle, Lillie Hayward and Roy Chansloris based on the novel Women Are Bum Newspapermen by Richard Macauley.

==Plot==
Ellen Garfield refuses to marry fellow reporter Curt Devlin until he admits she is as good at her craft as any man. The two work for rival newspapers, and their ongoing efforts to better each other eventually leads to Ellen getting fired when Curt tricks her into misreporting the verdict of a murder trial. The tables are turned when she scoops him by getting the real perpetrator, Inez Cordoza, to confess to the crime. Forced to admit Ellen is a good reporter, he finally wins her hand.

==Cast==
* Bette Davis as Ellen Garfield 
* George Brent as Curt Devlin 
* Roscoe Karns as Toots OGrady 
* Wini Shaw as Inez Cordoza  Walter Walker as Judge Hugo Rickard 
* J. Carrol Naish as Robert Cardoza 
* June Martel as Olive Wilson 
* J. Farrell MacDonald as Hallohan
* Dorothy Dare as Mae LaRue
* I. Stanford Jolley (uncredited; his first acting role)

==Production==
The films working title was Women Are Born Newspapermen. The plots of the 1937 release Back in Circulation, allegedly based on a story by Adela Rogers St. Johns, and the 1938 Torchy Blane film Blondes at Work are very similar to Front Page Woman. 

The Warner Bros. release was one of three 1935 films co-starring Bette Davis and George Brent. The two were paired on-screen a total of thirteen times.

This was the fourth collaboration for Davis and director Michael Curtiz. The two worked together a total of seven times.

==Critical reception==
The New York Times said, "The three writers who adapted it . . . did a clever script job and Michael Curtiz directed at a brisk pace. Add to that a cast with a neat sense of comedy and you have an excellent tonic for the mid-July doldrums." 

Variety (magazine)|Variety said, "  lacks authenticity and is so far fetched itll hand newsscribes around the country a constant run of ripples. But its light and has some funny lines and situations." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 