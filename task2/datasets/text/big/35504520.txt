Sakaler Rang
 
 
{{Infobox film
| name           = Sakaler Rang
| image          = Sakaler rang poster.jpg
| image_size     = 150px
| border         =
| alt            = Sakaler Rang film poster
| caption        = Sakaler Rang, 2009 Bengali film poster
| director       = Suvamoy Chattopadhyay
| producer       = Srabanti (Dona) Das
| writer         =
| screenplay     = Suvamoy Chattopadhyay
| story          = Suvamoy Chattopadhyay
| based on       =  
| narrator       =
| starring       = Churni Ganguly Monu Mukhopadhyay Taranga Sarkar
| music          =
| cinematography =
| editing        = Suvamoy Chattopadhyay
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India Bengali
| budget         =  
| gross          =
}} Bengali film directed by Suvamoy Chattopadhyay. This was first movie of Suvamoy as a director.    This budget of the film  . Because of this reason Suvamoy had to take burden of almost all the departments of the film.   

==Plot==
 

==Cast and crew==

===Cast===
*Taranga Sarkar as Prananath.
*Paulomi DeSakina.
*Monu Mukhopadhyay as Teacher.
*Churni Ganguly as Bhabi.

===Crew===
*Direction: Suvomoy Chattopadhyay
*Producer: Srabanti (Dona) Das.
*Music direction: Suvomoy Chattopadhyay
*Lyrics: Suvomoy Chattopadhyay
*Story: Suvomoy Chattopadhyay
*Screenplay: Suvomoy Chattopadhyay
*Editor: Suvomoy Chattopadhyay

==References==
 

 
 
 
 


 