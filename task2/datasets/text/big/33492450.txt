Starship Troopers: Invasion
 
{{Infobox film
| name           = Starship Troopers: Invasion
| image          = Starship Troopers Invasion poster.jpg
| caption        =
| director       = Shinji Aramaki 
| producer       = Joseph Chou 
| writer         = Flint Dille 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Sony Pictures Entertainment
| released       =  
| runtime        = 89 minutes
| country        = United States Japan
| language       = English
| budget         =
| studio         = Sola Digital Arts
| gross          = US$190,471 
}} computer animated Starship Troopers film series, it was released in Japan on July 21, 2012 and in North America on August 28, 2012 as a direct-to-video title.

==Plot== Terran Federations Mobile Infantry troopers, by dropship, to seize and control the hangar and rescue any survivors. Lieutenant Daughertys Alpha team lands and immediately engages bugs, fighting through to rendezvous with the surviving Fort Casey troopers. After setting explosives charges, the troopers head to the starship John A. Warden for evacuation, only to see it leave dock without them – Minister Carl Jenkins has commandeered the Warden from Captain Carmen Ibanez, sending her to Alesia. Before leaving, Jenkins orders that Major Henry "Hero" Varro, the commander of Fort Caseys K-12 troopers, be escorted to Alesia as a prisoner. Alesia docks with Fort Casey for emergency evacuation of the surviving troopers, and the Fort Casey asteroid is successfully destroyed.
 General Johnny Rico from High Command at L-6 Base.  When he orders Alesia to search for the Warden, which has broken all contact, the Fort Casey troopers agree to do so on condition that Varro leads them during the mission.  When they find the Warden, Daughertys team escorts Ibanez to the bridge while Varros team secures the Engine Room, both teams finding nothing but dead crewmen and a few dead bugs.  Varro finds a deranged Jenkins and is warned – too late – not to power up the Warden as she has hacked all the systems. As the engines recharge the Warden, an Arachnid Queen inside takes control of all systems and opens bulkhead doors to release her bugs. As Ibanez and the troopers attempt to return to Alesia, the Queen uses the Wardens main weapons to destroy the other ship, then flies Warden into a wormhole whose outlet is in near-Earth space. The troopers return to the Wardens bridge, where Varro reveals he was arrested when Jenkins had ordered his unit to capture the Queen alive on Fort Casey, but Varro had refused to sacrifice his squad.

General Rico deploys three starships from the L-6 Base in Earth orbit, but they are unable to stop the Warden. The Queen sets the bug-infested Warden on a trajectory to crash land in Paris, but Alpha Team sniper Trig manages to shoot out the wires linking the Queen to the ship, allowing Ibanez to re-direct the Warden to crash in a mountain range.  Meanwhile, General Rico leads a squad of troopers in Marauder suits to stop the bugs from escaping the crash site, while high command gives him 30 minutes before they will drop nukes from the L-6 Station to sanitize the site.

Back on the Warden, the five surviving troopers make their way towards the Queen. Ice Blonde protects Ibanez at a nearby airlock while Mech and Ratzass go to the engine room to blow it up.  When Varro and Bugspray find Trigs corpse, Bugspray uses Trigs family-made sniper rifle to buy Varro some time to reach the Queen.  Jenkins, having recovered from his mental breakdown, provides Varro back-up with bugs under his mind control – revealing an important Terran breakthrough in the war and the reason for capturing a live Queen.

Rico is the only one from his squad to reach the Warden. The extraction shuttle from L-6, and the nuclear strike, are all destroyed by the Queens control of the Wardens weapons.  As Jenkins leads the team to his shuttle from Fort Casey, Rico rushes to distract the queen and rescue Varro.  Varro, critically injured, blows a grenade when surrounded by bugs.  Rico abandons his broken Marauder suit and uses a combat knife on one of the Queens eyes, buying Ibanez time to make take-off preparations.  Rico barely sprints back into the dropship as it escapes the Warden before the explosives detonate and destroy the Warden.

Mech, Ice and Ratzass pay their respect to their fallen comrades while Ibanez confronts Jenkins over the recent crises. Jenkins sidesteps the issue, telling Rico and Ibanez that his research will someday save the entire galaxy.  In the aftermath, one warrior bug is seen navigating a sewer system, having survived the Wardens destruction.

==Cast==
 
The voice cast as presented in order of closing credits: Carmen Ibanez, captain of the John A. Warden Carl Jenkins, Minister of Paranormal Warfare Johnny Rico, general from High Command at L-6 Base.
* David Wald as Hero, Major Henry Varro, commander of K-12 team
* Andrew Love as Bugspray, Lieutenant Otis Hacks, acting commander of K-12 team
* Leraldo Anzaldua as Ratzass, sergeant from K-12 team
* Sam Roman as Daugherty, Lieutenant Tony Daugherty, commander of A-1 team
* Emily Neves as Trig, sniper from A-1 team
* Melissa Davis as Ice Blonde, corporal from A-1 team
* Kalob Martinez as Holyman, private from K-12 team
* Chris Patton as Kharon
* Adam Gibbs as Shock Jock, medic from A-1 team
* Jovan Jackson as Mech, sergeant and explosives specialist from A-1 team
* Corey Hartzog as Chase
* Josh Grelle as Chow
* Karl Glusman as Gunfodder
* Shelley Calene-Black as Captain Jonah, captain of the Alesia
* Noel Burkeen as Crysoch
* Andy McAvin as High Command
* Michael Keeney as Communication Officer 1
* Kris Carr as Communication Officer 2

==Production==
 ]]

 , are attached as executive producers.  According to Neumeier, he was allowed to have as much impact as he wished in his consulting role but his involvement ended up rather limited. Once the script was finished, it took about a year and a half to complete the film. If the film is successful, there is a possibility of further sequels by the same creative team. 

The characters of Johnny Rico, Carl Jenkins and Carmen Ibanez make a return; despite some pre-release rumors to the contrary, they are not voiced by their respective actors from the previous installments.  

The recording was done at Seraphim Digital in Houston, Texas. Several of the actors that were in the film also did the motion capture for several of the characters.
 App Store on November 13, 2012.

==Reception==

Starship Troopers Invasion Scored 51% On rotten tomatoes. Opinions were generally mixed.

==See also==
 

* List of films featuring space stations

==References==
{{Reflist|refs=
   
}}

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 