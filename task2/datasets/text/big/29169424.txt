The Garment Jungle
{{Infobox film
| name           = The Garment Jungle
| image          = The Garment Jungle film poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Vincent Sherman Robert Aldrich
| producer       = Harry Kleiner
| screenplay     = Harry Kleiner
| based on       =  
| narrator       =
| starring       = Lee J. Cobb Kerwin Mathews Gia Scala
| music          = Leith Stevens
| cinematography = Joseph F. Biroc William Lyon
| distributor    = Columbia Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $1,050,000 Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 249 
| gross          = 260,086 admissions (France)   at Box Office Story 
}}
The Garment Jungle is a 1957 American crime film noir directed by Vincent Sherman and Robert Aldrich and written by Lester Velie and Harry Kleiner.  The drama features Gia Scala, Lee J. Cobb, Kerwin Mathews and Richard Boone. 

==Plot== union out.

Walters partner, Fred Kenner, sympathizes with the unions goals.  After he tells Walter to sever his ties with the hoodlum enforcers, Kenner is killed when the freight elevator he enters, which was just fixed by one of the hoods disguised as a repairman, plunges 12 stories to the bottom of the shaft.  Tulio Renata is a union organizer trying to organize the factory who also later gets murdered by Ravidges men, and his wife Theresa Renata endures threats against her and their child.

Alan Mitchell comes to sympathize with the plight of the workers. When he finally convinces his father to fire the union busting gangsters, Walter is killed and Ravidge attempts to take over the factory. Theresa Renata takes copies of Mitchells records to the police, who arrest Ravidge.

==Cast==
* Lee J. Cobb as Walter Mitchell
* Kerwin Mathews as Alan Mitchell
* Gia Scala as Theresa Renata
* Richard Boone as Artie Ravidge Valerie French as Lee Hackett
* Robert Loggia as Tulio Renata
* Joseph Wiseman as George Kovan
* Harold J. Stone as Tony Adam Williams as Ox
* Wesley Addy as Mr Paul
* Willis Bouchey as Dave Bronson
* Robert Ellenstein as Fred Kenner
* Celia Lovsky as Tulios mother
* Marilyn Hanold as the model
* Jon Shepodd as Alredi

==Production==
Vincent Sherman replaced Aldrich towards the end of filming. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 