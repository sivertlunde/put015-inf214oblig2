Rhythm Is It!
{{Infobox film
| name           = Rhythm Is It!
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Thomas Grube Enrique Sánchez Lansch
| producer       = 
| writer         = 
| starring       = Simon Rattle Royston Maldoom
| music          = 
| cinematography = René Dame Marcus Winterbauer
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Germany
| language       = German
| budget         = 
}}
 2004 German documentary film directed by Thomas Grube and Enrique Sánchez Lansch. The film documents a project undertaken by the Berlin Philharmonic principal conductor Simon Rattle and choreographer Royston Maldoom who decided to popularize classical music by staging a performance of Igor Stravinskys ballet The Rite of Spring with a cast of 250 children recruited from Berlins public schools.   

In Germany the film was released in theaters in September 2004 and by February 2005 it sold 400,000 tickets. The film also won several prizes, including two for Best Editing and Best Documentary at the 2005 German Film Awards.   

==Awards==
{| class="wikitable" style="font-size:95%"
|- style="text-align:center;"
! style="background:#B0C4DE;" | Year
! style="background:#B0C4DE;" | Awarding Body
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Nominee(s)
! style="background:#B0C4DE;" | Result
|-
|  2005 
| Bavarian Film Awards  Best Documentary 
| Thomas Grube Enrique Sánchez Lansch 
|  
|-
|rowspan="2"| 2005
|rowspan="2"| German Film Awards
| Best Editing
| Dirk Grau Martin Hoffmann
|  
|- Best Documentary
| Uwe Dierks Thomas Grube Andrea Thilo 
|  
|-
|}

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 


 