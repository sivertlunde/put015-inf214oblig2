My Lady's Lips
{{Infobox film
| name       = My Ladys Lips
| image      = My Ladys Lips theatrical poster.jpg
| image size =
| border     =
| alt        =
| caption    = Theatrical poster James P. Hogan
| producer   = B.P. Schulberg
| writer     =
| screenplay = John F. Goodrich
| story      = John F. Goodrich
| based on   =
| narrator   =
| starring   = Alyce Mills William Powell Clara Bow Frank Keenan
| music      =
| cinematography = Allen G. Siegler
| editing =
| studio = B.P. Schulberg|B.P. Schulberg Productions Preferred Pictures Corporation Paramount Pictures Corporation
| released =  
| runtime = 70 minutes 6609 feet (7 reels)
| country = United States
| language = Silent film English intertitles
| budget =
| gross =
}}
 silent drama James P. Preferred Pictures Corporation.  The film stars Alyce Mills, and represents an early role for actress Clara Bow.  It is the tenth ever film for William Powell (better known for his later work in talking pictures),    and the first of only two films where Powell and Bow worked together. 
 16mm nitrate nitrate print survives and is preserved at UCLA Film and Television Archive.   Considered a "Silent Classic", the film was remastered by the National Film Preservation Foundation.   The film screened in Italy in 2003 at the Pordenone Silent Film Festival.   

==Plot==
Newspaper magnate Forbes Lombard (Frank Keenan) discovers that his daughter Lola (Clara Bow) is mixed up with a gang of gamblers. Reporter Scott Seldon (William Powell) pretends to be a felon and goes undercover to infiltrate the mob and get a news scoop. He falls in love with the gangs leader, female crook Dora Blake (Alyce Mills).  The two are captured in a police raid and under extreme questioning are forced to sign confessions. When Scott is released from prison, he tracks down Dora and finds she has returned to her old ways. After he vows his love, the two marry and begin a new life.

==Cast==
* Alyce Mills as Dora Blake
* William Powell as Scott Seldon
* Clara Bow as Lola Lombard
* Frank Keenan as Forbes Lombard
* Ford Sterling as Smike
* John St. Polis as Inspector
* Gertrude Short as Crook girl
* Matthew Betz as Eddie Gault
* Sôjin

==Reception==
Hal Erickson of AllRovi made note that Clara Bows role as the daughter of a media leader in this film was well received albeit minor, and that film critics in 1926 did not like the casting of William Powell as the hero Scott Seldon, offering that "the actor would be wise to continue playing villains lest he lose his standing in Hollywood." 

Pittsburgh Post-Gazette wrote that the film had a difficult time passing the Pennsylvania state board of censors, and that actress Alyce Mills "made the most of" her role as "crook girl" Dora Blake, and that William Powell was "excellent as the reporter". 

==References==
 

==External links==
*   at the Internet Movie Database
* 

 

 
 
 
 
 
 
 
 
 