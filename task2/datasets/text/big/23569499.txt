I Love You (1986 film)
{{Infobox film
| name           = I Love You
| image          = I Love You (1986 film).jpg
| caption        = Film poster
| director       = Marco Ferreri
| producer       = Maurice Bernart
| writer         = Marco Ferreri Enrico Oldoini Didier Kaminka
| starring       = Christopher Lambert
| music          = 
| cinematography = William Lubtchansky
| editing        = Ruggero Mastroianni
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = France Italy
| language       = French
| budget         = 
}}

I Love You is a 1986 French-Italian drama film directed by Marco Ferreri. It was entered into the 1986 Cannes Film Festival.   

==Cast==
* Christopher Lambert - Michel (as Christophe Lambert)
* Eddy Mitchell - Yves
* Flora Barillaro - Maria
* Agnès Soral - Hélène (as Agnes Soral)
* Anémone - Barbara (as Anemone)
* Marc Berman - Pierre
* Patrice Bertrand - Le client grincheux
* Paula Dehelly - La mère de Pierre
* Maurizio Donadoni - Georges
* Fabrice Dumeur - Marcel
* Carole Fredericks - Angèle
* Laurence Le Guellan - Nicole (as Laurence Leguellan)
* Olinka Hardiman - La maîtresse (as Olivia Link)
* Laura Manszky - Camelia / Isabelle
* Jeanne Marine - La prostituée

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 