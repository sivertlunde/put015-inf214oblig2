Mother (1955 film)
 
{{Infobox film
| name           = Mother 1905
| film name      = Мать (Mat)
| image          = 
| caption        = 
| director       = Mark Donskoy
| producer       = Mark Donskoy Aleksandr Kozyr
| writer         = Mark Donskoy Maxim Gorky Nikolai Kovarsky
| starring       = Vera Maretskaya
| music          = Lev Shvarts
| cinematography = Aleksei Mishurin
| editing        = N. Gorbenko
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}} the eponymous novel by Maxim Gorky. It was entered into the 1956 Cannes Film Festival. 

==Cast==
* Vera Maretskaya as Pelagea Nilovna Vlassovna, the mother
* Aleksey Batalov as Pavel Vlassov
* Tatyana Piletskaya as Sasha Andrei Petrov as Andrei Nakhodka
* Sergei Kurilov as Nikolai Ivanovich
* Liliya Gritsenko as Sophia Ivanovna
* Pavel Usovnichenko as Rybin
* Pavel Volkov as Nikolai Suzov
* Nikifor Kolofidin as Mr. Vlassov
* Ivan Neganov (as I. Neganov) as Agent
* Vladimir Marenkov as Vesoshukov

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 