Gooli
{{Infobox film
| name           = Gooli
| image          = 
| caption        = 
| director       = P. Satya
| producer       = Ramu
| writer         = 
| narrator       = 
| starring       = Sudeep Mamta Mohandas
| music          = Anoop Seelin
| cinematography = 
| editing        = Munraj
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          =
}}
Gooli is a Kannada film, directed by P. Sathya, released in 2008 in film|2008. It stars Sudeep, Mamta Mohandas in lead roles. The film is produced by Ramu under the banner Ramu Enterprises and music is by Anoop Seelin.    This movie was later released in Tamil as Korukkupettai Kooli in 2012. 

== Plot ==
The central character Ghooli (Sudeep) is an uneducated, rude youngster and deadly mercy less cruel rowdy with whom a girl named Ramya (Mamta) accidentally falls in love by seeing his pure and unadulterated heart. Ramya continues to keep in touch with Ghooli despite opposition from her family and although Ghooli tries to keep Ramya at a distance, he finds he has also has fallen in love with her. They marry but separate shortly afterwards. Later Ghooli hears shocking news about the apparent death of Ramya, however she is not dead but in a mental asylum due to severe brain damage.

==Cast==
* Sudeep ... Ghooli
* Mamta Mohandas .... Ramya
* Kishore ....
* Bhavya....
* Sathyaraj...
* Rajashekhar...
* Yethiraaj
* Sathya...
* Rekha V. Kumar

Music Director: Anoop Seelin

==References==
 

==External links==
*  
*  

 
 
 
 


 