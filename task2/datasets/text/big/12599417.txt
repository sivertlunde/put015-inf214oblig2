Mrigayaa
{{Infobox film
| name           = Mrigayaa
| image          = Mrigayaa.jpg
| caption        = 
| director       = Mrinal Sen
| producer       = K. Rajeshwara Rao
| based on       =  
| writer         =  
| starring       =  
| music          = Salil Choudhury
| cinematography = K. K. Mahajan
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
}}
 period drama directed by Mrinal Sen and produced by K. Rajeshwara Rao. Based on a short story by Bhagbati Charan Panigrahi, the film portrayed the relationship between the British colonialists and native villagers, and their exploitation by Indian landlords in 1920s India. It also depicts the friendship between a British administrator, who has a flair for game hunting, and a native tribal, who is an expert archer. The lead actors Mithun Chakraborty and Mamata Shankar, both made their cinematic debuts through the film.
 Best Feature Best Actor. It also won the Filmfare Critics Award for Best Movie apart from being nominated for the Golden Prize at the 10th Moscow International Film Festival in 1977.

==Plot==
Set in the 1930s, the film is about a group of tribals who live in a small village in Orissa amidst wild animals like tigers and boars. Apart from the problems faced by the animals that ruin the harvest, they also suffer in the hands of the greedy moneylenders and police informers. Around this time, a newly posted British administrator arrives at the village who happens to be passionate on hunting. He befriends Ghinua, a native tribal who is also an exceptional archer. The two get into a deal where Ghinua will be rewarded if he brings a "big game".

The story then focusses on Sholpu, a young revolutionary surreptitiously comes into the village to meet his mother. Knowing this, the police informer chases him down till he reaches his house, but returns back after seeing the whole village turn against him. However, he waits for his turn to punish Sholpu. Suddenly there is a robbery in the village and one policeman is killed. The blame falls on Sholpu and the administrator declares a reward for his head. The informer takes the opportunity and kills Sholpu thereby claiming the reward. Sholpus death creates a tension between the tribals and non-tribals. During this time, Dungri, Ghinuas wife, is abducted by a moneylender. Ghinua kills the moneylender to bring his wife back. Thinking that the time has come for the "big game", he goes happily to meet the Sahab, the administrator. The Sahab, however, hangs him for committing a murder. Till his death, Ghinua fails to understand why for the same action one is rewarded while the other is punished.

==Cast==
*Mithun Chakraborty as Ghinua
*Mamata Shankar as Dungri
*Robert Wright as the British administrator
*Asit Bandopadhyay
*Shekhar Chatterji
*Sadhu Meher Dora
*Ganesh Mukherjee
*Anoop Kumar
*Sajal Roy Chowdhury as moneylender
*Samit Bhanja as Sholpu
*Tom Alter

==Themes and influences== Santhal revolt that took place in the 1850s;    Sen claimed that the story "could have happened anytime, anywhere". The theme of the villagers facing a tough time in the form of wild animals, and cruel moneylenders on the other hand are connected in the opening sequence where a boar is seen destroying the crops, following which a moneylender arrives. 

==Production==
 
Sen, who was making political films till then, decided to experiment on films that focus on personal relationships. He decided to film a village-based story.  He incurred heavy losses from his previous venture Chorus (1974 film)|Chorus (1974). He was not able to repay the loan owing to the films financial failure. Mrigayaa was produced by K. Rajeshwara Rao. It was Sens first colour film,  and for the role of Ghinua, he wanted to cast a henman. Unable to find one, he happened to come across Mithun Chakraborty in one of his teaching sessions at the Film and Television Institute of India where the latter was a student.  For the female lead, another newcomer, danseuse Mamata Shankar, Uday Shankars daughter, was cast as Dungri.   The post-production work was done at Madras. 

==Reception==
Mrigyaa was an average grosser at the box-office. It received mixed response from the critics and audience who did not like the idea of mixing "story with history".  While Mithuns portrayal as a tribal fetched him unanimous acclaim, Shankars performance was noted as being tense.  The performances of rest of the cast that includes Sadhu Meher, Samit Bhanja and Sajal Roy Chowdhury were well received. 

==Awards==
 
{| class="wikitable"
|-
! Award !! Ceremony !! Category !! Nominee(s) !! Outcome
|- National Film Awards
| rowspan="2" | 24th National Film Awards (1976) Best Feature Film || K. Rajeshwara Rao (Producer) Mrinal Sen (Director) ||  
|- Best Actor || Mithun Chakraborty ||   
|- Critics Award for Best Movie || K. Rajeshwara Rao ||   
|-
|| Moscow International Film Festival || 10th Moscow International Film Festival (1977) || Golden St. George || Mrinal Sen ||    
|}

==Notes==
 

==References==
 
* 
* 
* }}
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 