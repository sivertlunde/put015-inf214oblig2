The Rugrats Movie
{{Infobox film image          = TheRugratsMovieposter.jpg caption        =  Theatrical release poster director       = Igor Kovalyov Norton Virgien producer       = Arlene Klasky Gábor Csupó writer         = David N. Weiss J. David Stem based on       =   starring       = {{plainlist|
* Elizabeth Daily|E.G. Daily
* Christine Cavanaugh
* Kath Soucie
* Cheryl Chase
* Tara Strong
}} music          = Mark Mothersbaugh editing        = John Bryant Kimberly Rettberg studio         = Nickelodeon Movies Klasky Csupo distributor    = Paramount Pictures released       =    runtime        = 81 minutes country        = United States language       = English budget         = $24 million gross          = $140.9 million (worldwide)
}}

The Rugrats Movie is a 1998 American  .
 guest stars David Spade, Whoopi Goldberg, Margaret Cho, Busta Rhymes, and Tim Curry.

==Plot==
The film starts with a parody of the 1981 Indiana Jones action film Raiders of the Lost Ark. Afterwards, the babies are chased out of the temple by a boulder, which is revealed to be Didi Pickles who is at the baby shower at the Pickles house, where Didi is pregnant with a second child, which everyone believes will be a girl. Didi goes into labor and her friends rush her to the hospital. While this is happening, the kids crawl off. When the baby is finally born, it turns out to be a boy and they name him Dillon, after Didis cousin. Dil quickly becomes a very spoiled baby, crying non-stop for attention, keeping all of the babies toys for himself and refusing to share with Tommy. Stu has a conversation with Tommy about being a big brother and the responsibility he now has and assures him that one day hell be happy to have Dil as his little brother. He also gives Tommy a locket with a picture of Tommy and Dil taped together and a watch inside which he calls his "sponsativity", his term for responsibility.

When Dil pushes the other babies too far, they decide to take him back to the hospital despite Tommys disapproval and end up driving recklessly through the streets in a Reptar Wagon Stu had built until they crash in the woods. As Dil had secretly pinched Angelicas Cynthia doll, Angelica and Spike embark on a mission to find the babies.

When the babies realize they are lost, they spot a rangers cabin which they believe that a "lizard" (a mispronunciation of wizard (fantasy)|wizard) lives there, and decide to go there, believing that it can take them home. After an encounter with runaway circus monkeys, Dil is taken away by the monkeys. Tommy vows to find Dil by himself, because the other babies still agree they are better off without him. Tommy eventually finds Dil during a storm, and are forced to take shelter under a tree, but Dils selfish acts push Tommy too far and snaps at Dil. Both the storm and Tommys rage scare Dil, who turns over a new leaf.

After the storm, Tommy and the other babies reconcile, and after running into Angelica and Spike they make their way to the "lizard." While on a bridge, they are confronted first by the monkeys and later by a wolf that has been hunting them since the babies arrived in the woods (for some reason, the monkeys are scared of the wolf). The wolf rushes after them, but Spike intervenes. After a brief tussle, Spike, dangling over the bridge, drags the wolf and himself to their apparent deaths. Meanwhile, Stu, who has been looking for the babies via aircraft finds them, but crash land into the rangers cabin. Believing he is the "lizard," the saddened babies wish for Spike back instead of going home. Stu falls through the bridge and finds Spike, who has survived the fall. The babies are then reunited with their families. In the final scene, the babies are having the same imaginary adventure they had when the movie began, but this time are successful with Dils help, finally accepting him as one of them.

==Cast==

===Main===
* Elizabeth Daily|E.G. Daily as Tommy Pickles
* Christine Cavanaugh as Chuckie Finster
* Kath Soucie as Philip and Lillian DeVille; Soucie was also the voice of Betty DeVille, Philip and Lillians mother
* Cheryl Chase as Angelica Pickles
* Tara Strong (credited as Tara Charendoff) as Dillon Prescott Pickles, Tommys younger brother

===Supporting=== Jack Riley as Stu Pickles
* Melanie Chartoff as Didi Pickles; Chartoff was also the voice of Grandma Minka
* Joe Alaskey as Grandpa Lou Michael Bell as Drew Pickles; Bell was also the voice of both Chas  Finster and Grandpa Boris
* Tress MacNeille as Charlotte Pickles
* Cree Summer as Susie Carmichael Phil Proctor as Howard DeVille

===Guest stars===
* Busta Rhymes as Reptar Wagon
* David Spade as Ranger Frank
* Whoopi Goldberg as Ranger Margaret
* Tim Curry as Rex Pester
*Roger Clinton, Jr. as Air Crewman
* Margaret Cho as Lt. Klavin
* Edie McClurg as Nurse
* Charlie Adler as United Express driver
* Gregg Berger as Circus TV announcer
* Tony Jay as Dr. Lipschitz

===Baby singers (Musical number&nbsp;– "This World Is Something New To Me")===
* Lenny Kravitz
* Lou Rawls
* Iggy Pop
* Lisa Loeb
* Gordon Gano
* B-Real
* Fred Schneider
* Patti Smith
* Kate Pierson
* Jakob Dylan
* Phife Dawg
* Beck
* Dawn Robinson
* Laurie Anderson
* Cindy Wilson

==Production==
Two songs were cut from the film during production. The first sequence revolved around Stu and Didi in a nightmare sequence where Dr. Lipschitz berates their parenting through song. The other sequence occurs as the Rugrats are pushing the Reptar Wagon through the woods, debating what to do about Dil in army chant style. These two scenes were cut from the theatrical version and the VHS and DVD releases. However, they were already animated at the time, and the scenes are shown on CBS and Nickelodeon TV airings of the film as the uncut version is only available on TV. These scenes were present in the print novelization.

The film was released in theaters with a CatDog short titled "Fetch", in which Cat wins a radio contest and attempts to answer the phone as Dog chases down his tennis ball. This short was later broadcast in CatDog episode 21. However, the home video VHS and DVD release contains a different CatDog short, "Winslows Home Videos".

==Media==

===Home video===
The Rugrats Movie was released on VHS and DVD March 30, 1999 by Paramount Pictures, Paramount Home Video also released the film on Laserdisc. In 2011, the film was re-released in a 3-disc trilogy set alongside its sequels in honor of Rugrats 20th anniversary.     

===Soundtrack===
{{Infobox album
| Name        = The Rugrats Movie: Music From the Motion Picture
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = RugratsMovie-Soundtrack.jpg
| Cover size  = 500
| Released    = November 3, 1998
| Recorded    = 1998 hip hop, pop
| Length      = 41:51
| Label       = Interscope Records
| Chronology = Rugrats soundtrack
| This album  = The Rugrats Movie: Music From the Motion Picture   (1998)
| Next album  =     (2000) Misc = {{Singles
 | Name            = The Rugrats Movie: Music From the Motion Picture
 | Type         = soundtrack Take Me There
 | Single 1 date = February 2, 1999
}}}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =    
| rev2 = Entertainment Weekly
| rev2Score = C  
}}
The Rugrats Movie: Music From the Motion Picture was released on November 3, 1998.    The enhanced soundtrack contained 13 tracks, bonus CD-ROM demos and commercials.  Amazon.coms Richard Gehr praised the CD for "  demographics as nimbly as the   show itself  " and for songs "fans of all ages will love".  Entertainment Weeklys David Browne rated the Music From the Motion Picture with a C.    Browne noted that, while the soundtrack is enjoyable for children and does "  concessions" for parents, adults may dislike the amount of rap.  Allmusics William Ruhlmann reviewed the soundtrack positively, saying "the result" of the singers and songs "is a romp in keeping with the tone of the show and the film".    Music From the Motion Picture spent 26 weeks on Billboard 200|Billboard 200, peaking at number 19.   

===Track listing===
{{Track listing
| extra_column    = Artist(s)
| total_length    = 41:51 Take Me There
| length1         = 4:04
| extra1          = Blackstreet & Mýa featuring Mase & Blinky Blink
| title2          = I Throw My Toys Around
| length2         = 3:02
| extra2          = No Doubt featuring Elvis Costello
| title3          = This World Is Something New To Me
| extra3          = Dawn Robinson, Lisa Loeb, B Real, Patti Smith, Lou Rawls, Laurie Anderson, Gordon Gano, Fred Schneider, Kate Pierson, Cindy Wilson, Phife Dawg, Lenny Kravitz, Beck, Jakob Dylan and Iggy Pop
| length3         = 1:59
| title4          = All Day
| extra4          = Lisa Loeb
| length4         = 3:30
| title5          = Dil-A-Bye
| extra5          = Elizabeth Daily|E.G. Daily
| length5         = 3:43
| title6          = A Baby Is A Gift From Bob
| extra6          = Cree Summer & Cheryl Chase
| length6         = 1:57
| title7          = One Way or Another
| extra7          = Chase
| length7         = 3:17
| title8          = Wild Ride
| extra8          = Kevi featuring Lisa Stone
| length8         = 2:43
| title9          = On Your Marks, Get Set, Ready, Go!
| extra9          = Busta Rhymes
| length9         = 3:41 Witch Doctor
| extra10         = Devo
| length10        = 3:33
| title11         = Take The Train
| extra11         = Rakim and Danny Saber
| length11        = 4:05
| title12         = Yo Ho Ho And A Bottle Of Yum
| extra12         = E.G. Daily, Christine Cavanaugh & Kath Soucie
| length12        = 2:18
| title13         = Take Me There (Want U Back Mix)
| extra13         = Blackstreet & Mýa featuring Mase & Blinky Blink
| length13        = 3:59
}} 

===Video games===
Video games were released for   and released by  , The Rugrats Movie is rated as a 55%    while Rugrats: The Movie earned a 61.75%.   

A   and released on September 14, 1998. It features six games and a bonus level that can be attained if a certain item is obtained in a game.   

===Books===
Several books were released by Simon & Schusters Simon Spotlight branch and Nickelodeon inspired by The Rugrats Movie. Tommys New Playmate and The Rugrats Versus the Monkeys were also released on October 1, 1998, authored by Luke David and illustrated by John Kurtz and Sandrina Kurtz.       The Rugrats Movie Storybook, released on the same date and using the same illustrators and publishers, was written by Sarah Wilson.    The same date saw the release of The Rugrats Movie: Hang On To Your Diapies, Babies, Were Going In!: Trivia from the Hit Movie!, a trivia book written by Kitty Richards.   
 Hal Leonard Publishing Corporation released a book titled The Rugrats Movie.   

==Reception==

===Box office===
The Rugrats Movie was released on November 20, 1998, and made United States dollar|US$27,321,470 in its opening weekend,    from 2,782 theaters, averaging about $9,821 per venue and ranking as the #1 movie that weekend.    In total, The Rugrats Movie made US$140,894,675, $100,494,675 from the domestic market and $40,400,000 from its foreign release.  It also debuted #1 at the UK box office.   

===Critical reception===
 , The Rugrats Movie held a 59% on the review aggregator website Rotten Tomatoes, with 29 "Fresh" reviews and 20 "Rotten" reviews, certifying it as "Rotten".    Roger Ebert gave the film 2 stars out of 4.    Ebert noted that the films target audience was more for younger children, and that, while he as an adult disliked it, he "might have" liked it if he were younger and would recommend it for children.  The New York Timess Anita Gates reviewed The Rugrats Movie positively, calling it a "delight".    Lisa Schwarzbaum of Entertainment Weekly graded the film with a B.    Schwarzbaum praised the movie for its appeal to both adult and child audiences, "juxtaposing the blithely self-absorbed parallel universes of small, diapered children and their large, Dockered parents".  However, other Entertainment Weekly reviewer Ty Burr gave The Rugrats Movie a B−, criticizing that the films issues sprung from it being "bigger" than the original series, thus it having more cultural references, out-of-place CGI scenes, and "  into scary territory".    Despite these faults, Burr did praise the "escaped circus monkeys" for being "scary in a good way", as well as a joke that was accessible to younger audiences. 

==Sequels==
Two sequels have been released:  , which was released November 17, 2000,    and Rugrats Go Wild, which was released June 13, 2003.   

==See also==
 
*  
* Rugrats Go Wild

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 