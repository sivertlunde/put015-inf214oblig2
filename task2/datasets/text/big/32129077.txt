The Navy Comes Through
{{Infobox film
| name           = The Navy Comes Through
| image          = 
| caption        = 
| director       = A. Edward Sutherland
| producer       = Islin Auster
| writer         = Borden Chase (story) Earl Baldwin (adaptation) John Twist (adaptation) Roy Chanslor Æneas MacKenzie Pat OBrien George Murphy Jane Wyatt
| music          = 
| cinematography = 
| editing        = 
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Pat OBrien, George Murphy and Jane Wyatt. It was nominated for an Academy Award for Best Visual Effects (Vernon L. Walker, James G. Stewart).   

==Plot==
In 1940, the testimony of  Chief Petty Officer Mike Mallory (Pat OBrien) at a United States Navy board of inquiry regarding a fatal gun turret accident helps end the career of Lieutenant Tom Sands (George Murphy). The situation is complicated by the fact that Sands and Mallorys sister Myra (Jane Wyatt) are in love. Afterward, Sands resigns his commission and breaks up with Myra, telling her there is no future for them.

When the United States enters World War II, however, Sands joins as an enlisted man. By chance, he is assigned to Mallory, to their mutual displeasure. They and the rest of Mallorys men are disappointed to be assigned to man the guns of the freighter Sybil Gray. When Myra comes to see her brother off (though she is assigned to the same convoy as a Navy nurse), she encounters Sands, whom she had not seen since the inquiry.
 Max Baer) recognizes Sands, making him a pariah among the navy sailors. On the voyage to England, they are attacked by a German U-boat on the surface. They exchange fire, before the submarine is driven off by escorting warships. As Bayless is seriously injured, doctor Lieutenant Commander Murray and Myra are brought aboard. They remain on the ship to avoid delaying the convoy further. A near encounter with a German pocket battleship in the fog causes Sands to reveal that he still loves Myra.

Later, two German airplanes strafe and bomb the Sybil Gray. When Myra is knocked out by falling debris, Sands abandons his machine gun to carry her to safety. While he is gone, Berringer, the other sailor manning the gun, is fatally wounded. The two aircraft are shot down, but the sailors now believe that Sands is a coward.

When "Babe" Duttsons (Jackie Cooper) radio intercepts a German message, "Dutch" Croner (Carl Esmond) is able to interpret it. He informs Mallory that a German U-boat supply ship is nearby. Mallory persuades the freighters captain to change course and capture the vessel. Unbeknownst to the Americans, the German captain has one of the torpedoes rigged to explode, but a suspicious Sands foils that scheme.

Then, with a ready supply of detonators, he suggests to Mallory that they load unsuspecting U-boats with booby-trapped torpedoes. As the only qualified navigator, Sands refuses to sail to Belfast, so Mallory has no choice but to agree. The plan goes without a hitch the first three times, blowing up enemy submarines, but an officer on the fourth recognizes Dutch as a famous anti-Nazi violinist. The two ships fire at each other, and another U-boat joins the battle. The Americans sink both submarines, but the hold of the supply ship is set on fire. When Mallory goes to deal with it during the fighting, he is overcome by the fumes. Sands rescues him. For his actions, Sands is reinstated as an officer.

==Cast== Pat OBrien as Chief Michael "Mike" Mallory
*George Murphy as Lieutenant Thomas L. "Tom" Sands
*Jane Wyatt as Myra Mallory
*Jackie Cooper as Joe "Babe" Duttson
*Carl Esmond as Richard "Dutch" Croner Max Baer as Coxswain G. Berringer
*Desi Arnaz as Pat Tarriba, a Cuban who enlists to repay American help in freeing Cuba Ray Collins as Captain McCall
*Lee Bonnell as Kovac
*Frank Jenks as Sampier
*John Maguire as James Bayless Frank Fenton as Hodum Joey Ray as James Dennis
*Marten Lamont as Lieutenant Commander Murray

==Reception==
The film was a surprise hit and earned a profit of $542,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p176 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 