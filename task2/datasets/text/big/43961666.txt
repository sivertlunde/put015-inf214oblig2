Romeo (1976 film)
{{Infobox film
| name           = Romeo
| image          =
| caption        =
| director       = SS Nair
| producer       =
| writer         = Kallada Vasudevan
| screenplay     = Kallada Vasudevan
| starring       = Sheela Kaviyoor Ponnamma Thikkurissi Sukumaran Nair Bahadoor
| music          = G. Devarajan
| cinematography = RN Pillai
| editing        = VP Krishnan
| studio         = Jeevan Pictures
| distributor    = Jeevan Pictures
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film,  directed SS Nair. The film stars Sheela, Kaviyoor Ponnamma, Thikkurissi Sukumaran Nair and Bahadoor in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Sheela
*Kaviyoor Ponnamma
*Thikkurissi Sukumaran Nair
*Bahadoor
*Jayasudha
*MG Soman
*Murali
*Radhamani Ravikumar
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chaarulathe || K. J. Yesudas || Vayalar ||
|-
| 2 || Kaalathe Manju Kondu || P. Madhuri || Vayalar ||
|-
| 3 || Mrigaangabimbamudichu || Sreekanth || Vayalar ||
|-
| 4 || Night is Young || P. Madhuri || Vayalar ||
|-
| 5 || Pushpolsavappanthalil || Sreekanth || Vayalar ||
|-
| 6 || Swimming Pool || P. Madhuri || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 


 