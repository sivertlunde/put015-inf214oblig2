Agnes Browne
 
 
{{Infobox film
| name = Agnes Browne
| image	=	Agnes Browne FilmPoster.jpeg
| director = Anjelica Huston
| producer = Anjelica Huston Jim Sheridan
| writer = Brendan OCarroll ( ) John Goldsmith
| starring = Anjelica Huston Marion ODwyer Ray Winstone
| music = Paddy Moloney
| cinematography = Anthony B. Richmond
| editing = Eva Gardos
| studio = October Films
| distributor = USA Films
| released = December 3, 1999 ( ) March 3, 2000
| runtime = 92 minutes
| country = United States Ireland
| language = English French
| budget = 
| gross = $148,853 
}} romantic comedy-drama film directed and produced by, and starring Anjelica Huston, based on the book The Mammy by Brendan OCarroll.   

==Plot== Tom Jones French baker. Her kids pool their money and buy her a new dress to wear on her first date. Of course, eventually the family has to face the loan shark and the French baker has an eye for Agnes as she hangs out with Marion and goes to a bar and later goes on a date with the French baker but the loan shark has begun to lose patience. Agnes and Marion have fun in a car and Marion dies of a heart attack. The next day Agnes brings the children to the hotel but she is attacked by the loan shark at her own house and the children run from manager to the elevator where they meet Tom Jones himself. The children tell Tom Jones about the whole situation, he helps them through a crowd of screaming fans. Suddenly Agnes hears the doorbell ring and thinking it is the loan shark again, grabs her hammer. When she answers it however, she sees that it is Tom Jones instead. Agnes and the children later come outside with Tom Jones, the loan shark gets paid and punched by one of the kids being warned to never come after their mammy again as they ride in the limousine. Tom Jones dedicates Shes a Lady to Agnes as the audience jams and the French baker joins Agnes and the children as the credits roll.

==Cast==
* Anjelica Huston as Agnes Browne
* Marion ODwyer as Marion Monks
* Ray Winstone as Mr. Billy
* Niall OShea as Mark Browne
* Ciaran Owens as Frankie Browne
* Roxanna Williams as Cathy Browne
* Carl Power as Simon Browne
* Mark Power as Dermot Browne
* Gareth OConnor as Rory Browne
* James Lappin as Trevor Browne Tom Jones as Himself (stunt double Will Anderson)
* Steve Blount as Tommo Monks Kate OToole as Senior Sister Magdalen
* June Rodgers as Fat Annie
* Jennifer Gibney as Winnie the Mackerel
* Brendan OCarroll as Seamus the drunk
* Eamonn Hunt as Mr. Foley

==Reception==
Agnes Browne was not well received in the United States.   Franz Lidz, a senior writer at Sports Illustrated, called it a flimsy whimsy and chided Roger Ebert for liking it.  William Arnold felt that the ending trivialized the story, leaving the audience "with the uncomfortable feeling that weve just viewed some episode of a TV sitcom of the era."  New York Times reviewer Stephen Holden found it "nothing more than a series of homey skits loosely woven into a portrait of a working-class saint." 

The film had a better reception in Europe, winning the Youth Jury Award at the 1999 San Sebastián International Film Festival. It also received a Grand Prix nomination at the Ghent International Film Festival the same year. 

==Author==
There were also two more books which followed The Mammy: The Chiselers and The Granny. However, these were not made into films.

Brendan OCarroll has had his own success with the Browne family in Mrs. Browns Boys, both in the theatre and on television.

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 