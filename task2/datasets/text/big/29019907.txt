Gaston's War
Gastons War is a 1997 drama film directed by Robbe De Hert and starring Werner De Smedt, Mapi Galán and Peter Firth.  Based on a novel by Allan Mayer, the film is set many decades after the Second World War, and tells the story of a Belgian resistance fighter, Gaston Vandermeerssche, who tries to discover who betrayed them to the Nazis.

==Main cast==
* Werner De Smedt ...  Gaston Vandermeerssche
* Mapi Galán ...  Veronique
* Stuart Laing ...  Harry
* Oliver Windross ...  Doug
* Peter Firth ...  Major Smith
* Christian Crahay ...  Hachez
* Olivia Williams ...  Nicky
* Lukas Smolders ...  Louis
* Clive Swift ...  General James
* Marilou Mermans ...  Farmers wife
* René van Asten ...  Van der Waals
* Sylvia Kristel ...  Miep Visser
* Stefan Perceval ...  German soldier
* Gert-Jan Dröge ...  Cohen
* Serge Marechal ...  Frenchman

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 