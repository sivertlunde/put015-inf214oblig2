Jangan Pandang Belakang
 
 
{{Infobox film name           = Jangan Pandang Belakang image          = Jangan-Pandang-Belakang.gif caption        = director       = Ahmad Idham producer       = David Teo writer         = Ahmad Idham Pierre Andre starring       = Pierre Andre Intan Ladyana Khatijah Tan Ruminah Sidek Rio Elani music          = Brian Ng cinematography = Indra Che Muda editing        = Ahmad Mustadha distributor    = Metrowealth Movies Production Sdn. Bhd. released       =   runtime        = 99 minutes country        = Malaysia language       = Malay budget         = gross          = RM6.33 million
}}

Jangan Pandang Belakang (translates as "Dont Look Behind") is a 2007 Malaysian horror film directed by Ahmad Idham. It holds the record as the highest-grossing film in Malaysia, which was previously held by 1994 drama film Sembilu.
  . artisglamer.fotopages.com. Retrieved 9 May 2007. 

== Plot ==
Darma (Pierre Andre) is traumatised by the mysterious death of his fiancee, Rose (Intan Ladyana) who had killed herself. Unknown to him, Rose had been haunted by a malicious spirit which they had brought to her home after picking up a small jar found washed up at the beach. Apparently, the spirit had been imprisoned and was inadvertently released by the couple.

At her home, after her death, before leaving Darma had taken the small jar with him, and with it, the malignant spirit.

Not satisfied with why Rose had suddenly killed herself, Darma decides to investigate the case with the help of Roses twin sister, Seri, (also played by Intan Ladyana). As they probe into the death, a series of strange occurrences start to unravel, leaving Darma in a disturbed state of mind that affects his life and career. When he is advised to take a weeks break from work, he returns to his village to rest. Unknown to him, he is only getting closer to solving his fiancees death and the truth behind the strange happenings.

It was Darmas senile grandmother who could see the ghost and had unwittingly invited the spirit into the house. In Malay belief, certain spirits cannot enter the house unless invited in. This is similar to the vampire mythology in the West.
The ghost in this movie is called Saka. Saka was a ghost that has been kept by the elder for some uncertified reason. Mostly, Saka starts to give problems when its owner die and Saka needs to be feed. Mostly, Saka will haunt the next generation of the owner.

==Cast==
* Pierre Andre as Darma
* Intan Ladyana as Rose & Seri
* Khatijah Tan as Mak Lang (Darmas aunt)
* Ruminah Sidek as Opah (Grandmother)
* Rio Elani as Lin
* Hafidzuddin Fazi as Tok Imam

A stuntman Nazari Abu Bakar died during the shooting of the film. The unexpected thing happen when the cameraman try to take a shot at the location, suddenly capture a mystery entity. That photo are being use as a teaser poster of this film. 

==Reception==
The film was released on 5 April 2007 on 51 theatres across Malaysia, Indonesia, Brunei, and Philippines. On its opening weekend, the film debuted at number 1 in Malaysia, grossing $433,589 in 55 theatres with an average $7,883 per theatre.  In Philippines, the film debuted at number 9 grossing $19,748 in 20 theatres with an average $987 per theatre.  In Singapore, the film grossed $91,030 on its first premiere, ranking at number 2 in 5 theatres with an average $18,206 per theatre. Overall, the film was an international success, grossing RM6.33 million, becoming the highest-grossing film of all time in Malaysia.

==Awards and nominations==
20th Malaysian Film Festival, 2007  
Won
* Box Office Film

Nominated
* Best Actor - Pierre Andre
* Best Actress in a Supporting Role - Ruminah Sidek
* Best Cinematography - Indra Che Muda Redzuan
* Best Editor - Ahmad Mustadha

==References==
 

==External links==
*   at Sinema Malaysia.
*  
*  

 

 
 
 
 
 