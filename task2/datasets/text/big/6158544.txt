The Mother of Tears
{{Infobox film
| name           = The Mother of Tears
| image          = The Third Mother poster.jpg
| caption        = Theatrical release poster
| director       = Dario Argento
| producer       = Claudio Argento Dario Argento Marina Berlusconi Giulia Marletta
| writer         = Jace Anderson Dario Argento Walter Fasano Adam Gierasch Simona Simonetti
| based on       =  
| starring       = Asia Argento Daria Nicolodi Moran Atias Coralina Cataldi-Tassoni
| music          = Claudio Simonetti
| cinematography = Frederic Fasano
| editing        = Walter Fasano
| studio         = Film Commission Torino-Piemonte Medusa Film Medusa Produzione Myriad Pictures  Opera Film Produzione Sky
| distributor    = Medusa Distribuzione
| released       =  
| runtime        = 102 minutes
| country        = Italy United States
| awards         =
| language       = English Italian
| budget         =
| gross          =
}}
The Mother of Tears ( , literally The Third Mother) is a 2007 Italian-American supernatural horror film written and directed by  , Walter Fasano, Adam Gierasch and Simona Simonetti, the film is the concluding installment of Argentos supernatural horror trilogy The Three Mothers, which began with Suspiria in 1977. The film depicts the confrontation with the final "Mother" witchcraft|witch, known as Mater Lachrymarum.

==Plot==
The film begins with members of the Catholic Church digging up the body of a 19th-century church official, whose casket has a box-shaped urn chained to it. Inside the box they discover artifacts belonging to Mater Lachrymarum (Moran Atias), the last surviving member of the Three Mothers; an ancient trio of powerful black witches. In particular, the box contains a magic cloak that, when worn by Mater Lachrymarum, increases her powers significantly.

The urn is shipped to the Museum of Ancient Art in Rome where Sarah Mandy (Asia Argento), an American studying art restoration, works. Sarah is dating the curator Michael Pierce, a single father who is away from the museum that night. With help from the assistant curator, Sarah opens the box and finds the cloak, a dagger, and three statues representing the three witches. Sending Sarah to her office to retrieve tools to help her translate the text on the artifacts, the curator is promptly attacked by the demonic agents of Mater Lachrymarum. Sarah arrives too late to save her boss (who is being disembowelled by the monsters) and starts to flee the museum. Unfortunately, she is pursued by Mater Lachrymarums familiar (a baboon) and is only able to escape when a disembodied voice magically throws open a series of locked doors keeping her trapped inside the museum. Sarah tells the police what happened as she spends the night with Michael and his son. Michael visits the Cardinal who sent him the urn only to find out that, shortly after mailing the urn to him, he had a severe stroke and is now in a coma. An assistant of the priest gives Michael a piece of paper, which the Cardinal was writing on before collapsing. On it is scrawled the name "Mater Lachrymarum". As he leaves the hospital, a pair of witches observe Michael leaving the building.

Back in Rome, chaos descends as a wave of mass suicides, murder, and violence engulfs the city. Sarah continues her own research only to be summoned by Michael to his apartment. The witches have kidnapped his young son and wont return the boy to him unless he stops his investigation. Sarah begs him to call the police (who are tailing Sarah, ever since the murder at the museum) but Michael refuses to and instead opts to visit a local priest who is a trained exorcist. This goes badly for Michael; the two witches see him and he is soon captured and murdered, along with his son, whose body is cannibalized by the rapidly expanding coven. However, before he is killed, Michael calls Sarah and begs for her to come and help him. As she makes her way through a crowded train station, Sarah is spotted by a gang of witches who, like so many other witches, have arrived in Rome in order to pledge their loyalty to Mater Lachrymarum. Pursued by the witches and the police, the disembodied voice from before instructs Sarah on how to magically make herself invisible. She uses this to avoid the police detective, though she is forced to kill a witch who catches and corners her on the train.

At the priests home, Sarah meets Marta, a fellow white witch and friend of Sarahs deceased mother. Realizing that Sarahs mother is the voice guiding her, Marta reveals details to Sarah about her parents. Her mother was a powerful white witch who dared to challenge and severely wound Mater Suspiriorum, the eldest and wisest of the Three Mothers. In response to this, Suspiriorum caused the fatal car crash that killed Sarahs parents. Though Mater Suspiriorum and her sister Mater Tenebrarum are now dead, their sibling Mater Lachrymarum has emerged from the shadows to bring about the second age of magic, with the fall of Rome as her coming out party. They talk to the priest, only for him to be killed before he can give the two a copy of a book that would explain Mater Lachrymarums backstory to them by a patient of his. Escaping back to the city, Sarah goes to her own home but finds Mater Lachrymarums goons waiting for her. She heads to Martas house, but once again Mater Lachrymarums minions strike and Marta and her lesbian lover are murdered. Fleeing, Sarah spots Michael, who takes her back to his apartment. Unfortunately, Sarah soon realizes that Michael is dead and that Mater Lachrymarum is animating his body in an attempt to kill her. As she burns her lovers still-animate body, the ghost of her mother intervenes one final time to grab Michael and banish him (and possibly herself) to Hell.

Sarah locates a powerful alchemist, who Marta mentioned as her only hope to learn how to fight Mater Lachrymarum. After being briefly paralyzed by the alchemist (so that he could perform a test on her to see if she was a white witch or an evil witch), the alchemist gives Sarah the only help he has in locating Mater Lachrymarums dwelling. Sarah is given a copy of "The Three Mothers" to read, and from this (and from following a group of witches) Sarah finds Mater Lachrymarums lair; a now run-down and disrepaired mansion. At this point, she is joined by one of the police detectives hunting her and the two go into the catacombs to find Mater Lachrymarum. However, the two become separated, and the detective is tortured alongside the alchemist and his assistant, who dies after his arm is chopped off by one of Mater Lachrymarums minions. Sarah is caught and brought before Mater Lachrymarum, who offers Sarah up to her cannibal followers but makes the mistake of removing her cloak. Sarah, having healed the detectives wounds, grabs the cloak and tosses it into a nearby fire. This causes the mansion to collapse as a pillar falls and impales Mater Lachrymarum. With the Mothers followers crushed as the caves collapse, Sarah and the detective laugh in horror and shock as they reach the surface, as they realize that the threat of the Three Mothers has been defeated once and for all.

==Cast==
*Asia Argento as Sarah Mandy
*Daria Nicolodi as Elisa Mandy
*Udo Kier as Padre Johannes
*Moran Atias as Mater Lachrimarum
*Coralina Cataldi-Tassoni as Giselle Philippe Leroy as Guglielmo De Witt Adam James as Michael Pierce
*Valeria Cavalli as Marta
*Clive Riche as Uomo col Cappotto
*Massimo Sarchielli
*Silvia Rubino as Elga
*Jun Ichikawa as Katerina

==Production==

===Pre-production===

====The Three Mothers trilogy====
 

The Third Mother is the final film in Argentos trilogy known as   recast De Quinceys Three Sorrows as three malevolent witches who rule the world with tears, sighs and shadows. When released in 1977, the first film, Suspiria, introduced the major stylistic elements of the series, including the bold use of primary colors and elaborate setpieces for each murder. The sequel, Inferno (1980 film)|Inferno, developed the overarching plot continuities concerning the three central witches when released in 1980.

====Nicolodi script (1980s)====
As early as 1984 Daria Nicolodi asserted in an interview with Fangoria – alongside Argento – that they had "finished the script for the third   but there are a few things we are still working on to perfect the project, a couple of special effects and locations, that sort of thing."  Although Nicolodi mentioned her version of the script again in an interview for Alan Jones book, Profondo Argento: The Man, the Myths and the Magic, it was not used in whole or part for The Third Mother.

====Argento script (2003/4)====
On 29 November 2003, at the Trieste Science Plus Fiction Festival in Northern Italy, Argento revealed that he hoped to start filming The Third Mother in August 2004 and was currently working on the script.    Thematically it concerned "mysticism, alchemy, terrorism and Gnosticism  . So many people were tortured because the Church said Gnosticism was heresy, and that will be the starting point for the story.   It has been over 20 years since I left the Three Mothers behind   and it has felt surprisingly good to go back and explore the whole story from a retrospective point of view."  The film was to be set in Rome and begin with Mater Lachrymarum in the Middle Ages.  Argento originally hoped to cast a Russian model in the role of Mater Lachrymarum.  (He later chose Israeli actress Moran Atias. ) Argento also said that a Hollywood studio might finance the film. 

====Anderson and Gierasch script (2005/6)====
In late 2005 Argento travelled to the North of Europe to begin conceptual work on The Third Mother.  Soon after, it was announced that Jace Anderson and Adam Gierasch had been asked by Argento to help him write the films script. "When we got there   Dario had already done his own pass on the treatment, and we spent three weeks holed up in an apartment, meeting with Dario, visiting the catacombs, and getting the first draft done."  Around this time, Fangoria reported that the film would be entitled Mater Lachrymarum. 

The script for The Third Mother was still being refined in February 2006, with Anderson and Gierasch having composed a first draft which Argento then revised.    This early script began immediately after Inferno (1980 film)|Inferno, with a witch who survived the destruction of Mater Tenebrarums home watching a detective (Ennio Fantastichini) investigating a series of murders at a University.    Other tentative cast members were Chiara Caselli as a psychiatrist, Max von Sydow as a mysterious university professor, and Giordano Petri as a young investigator who takes the case when Fantastichinis character is killed.  At this point, shooting was set to begin in late spring of the same year and was to be released between November 2006 and January 2007.  However, in early 2006 rumors circulated that Argento had dismissed Anderson and Gierasch after being displeased with their script. French horror magazine LÉcran Fantastique reported that Argento alone would receive a screenwriting credit. On the tenth of March it was announced that shooting The Third Mother would be delayed until September.  In mid-April it was announced that Argento would return to Italy in June to immediately begin filming The Third Mother, which would be "a big budget feature, produced by Medusa along with a major American company  ."  In May 2006 the title Mother of Tears surfaced as a possible name for the film. According to journalist Alan Jones, this title "was never in the running as far as Dario was concerned. That was the title the originally contacted American sales agent Myriad wanted for international distribution."    In the same month, rumors from the Cannes Film Festival linked actress Sienna Miller to the films lead female role.    Also at Cannes, Medusas CEO Giampaolo Letta was quoted by Anderson and Gierasch as saying "This is going to be vintage Argento. Pretty strong stuff."  In July it was revealed that The Third Mother had been delayed yet again until "next November or later" and that Argentos daughter, Asia Argento|Asia, had been cast in the film. 

===Filming===
 

In mid October 2006, Gierasch revealed that The Third Mother would finally begin filming later in the month.  Primary filming occurred in Rome, although some parts were filmed in Turin and at the studios of Cinecittà at Terni.   
 

===Post-production===
The editing of The Third Mother was more or less finished by March 2007.    Dubbing the soundtrack into the Italian and English language versions of the film was finished on 5 April 2007. 
 cool color palette that will segue to red as the film progresses. 

The Italian distributor of The Third Mother, Medusa Film, believed the film was too violent and want it to be edited.  Medusas main objection is to "the depiction of perverse sex in the witch gathering satanic scenes and one cannibal killing of a major character."    Argento was asked to re-edit the film to make it more mainstream.  It was confirmed on 28 May 2007 that the film would receive a rating of 14 in Italy, necessitating the removal of "all hardcore gore" which would later "be re-instated for the dvd release." 

==Promotion==
Promotion of The Third Mother before Cannes 2007 was limited. Several behind-the-scenes photographs surfaced, the first official one at Fangoria on 27 November 2006.  A short, eighteen-second  preview of The Third Mother was released on 18 December 2006 at Cinecitta.com.  Several black-and-white photographs of the filming were published on 19 January 2007 in the book Dario Argento et le cinéma by Bernard Joisten.  In May 2007, just before the event at Cannes, a promotional poster for The Third Mother was featured on the cover of Variety magazines digital edition. The Third Mother premiered at the Toronto International Film Festival on 6 September 2007, just moments before midnight and Argentos 67th birthday (on the 7th).    The film debuted in Italy on 24 October 2007 at the Rome Film Festival.  Its Italian wide release occurred on 31 October 2007, Halloween.    In the United States, Myriad Pictures released the film uncut in select cities in June 2008.   

===Cannes 2007===
The   premiered 20 minutes of footage from the film, consisting of eight lengthy scenes, to a packed audience.    The preview was preceded by a credit roll and disclaimer that warned of graphic violence.  The eight scenes included: the complete beginning to the point where Asia opens the Mother of Tears urn, the arrival of several demons, Daria Nicolodis "powder puff" scene, a lesbian death scene, Udo Kiers major scene, Asia running through the streets of Rome, Adam James major scene, and the entrance of Mater Lachrymarum.  According to reporter Alan Jones the audiences reaction was mixed: the acting quality varied and the script contained too much exposition, but the cinematography was beautiful. 

===Post-Cannes===
The day after the   were on The Third Mother discussion panel at  .

===US release===
The film had its US premiere at the San Francisco International Film Festival on 25 April 2008. This was followed by a limited theatrical run in June courtesy of Myriad Pictures and a DVD release by The Weinstein Company via Dimension Extreme DVDs on 23 September 2008.

==Response==

===Critical reaction===
Critical response was mixed, although many reviewers felt the film, despite its flaws, was entertaining. The concluding film of The Three Mothers trilogy provided some reviewers with an opportunity to reflect on Argentos career as a whole, and parallels were often drawn between The Mother of Tears and Argentos films from the 1970s and 1980s.
 camp classic." De Palma giallo The Bird with the Crystal Plumage, and his 1990 segment of the anthology film Two Evil Eyes — its painful to watch the Hieronymus Bosch of 70s horror sink this low...If you believe someone of Dario Argentos proven talent would make a movie so deliberately sucky, feel free to join in."     Maitland McDonagh hated the film, describing it as "sadly lacking in the baroque atmosphere and visual aesthetic that elevated Argento above the horror hacks—its flatly lit, indifferently staged, coarsely violent and brutally straightforward. The English-language dubbing is the final indignity: even the voices are ugly."     

Writing in   was extremely enthusiastic about the film, and said that "Mother of Tears is a great movie, and well worth the wait. Does it have flaws? Oh yeah, but so do Tenebrae (film)|Tenebrae, Phenomena (film)|Phenomena, Suspiria and Inferno, and theyre now all part of the accepted canon of classic Argento cinema."    Cinefantastique s Steve Biodrowski also felt the film was worthy of praise, noting that "the experience of watching Mother of Tears is like a delirious descent into primordial chaos, where the powers of darkness hold sway...As a long-awaited coda to Argentos "Three Mothers" trilogy,   may not be exactly what was expected, but it is perfectly satisfying resolution...."   

Argento has noted that he is dismissive of critical reaction, saying that "the critics dont understand very well. But critics are not important – absolutely not important. Because now audiences dont believe anymore in critics. Many years ago critics wrote long articles about films. Now in seven lines they are finished: The story is this. The actor is this. The color is good." 
  

===Box office performance===
In Italy, La Terza madre generated $827,000 in two days at 273 theaters. By the end of its opening week in 303 theaters it had amassed $1,917,934 and took the 4th spot at the Italian box office. To date it has taken 2,077,000 Euros ($3,114,070).   During its first week of limited theatrical release in the United States, The Mother of Tears grossed $19,418 at seven theaters, for a per theater gross of $2,774, taking 55th place on Varietys weekly box office chart. 

==Soundtrack== classical style Gothic influences present in many of the choruses. Simonetti described the score as "very different" from his previous work due to the subject matter of the film.  The music was influenced by his own work for Argentos Masters of Horror episodes ("Jenifer (Masters of Horror episode)|Jenifer" and "Pelts (Masters of Horror episode)|Pelts") as well as composers such as Carl Orff, Jerry Goldsmith, and Bernard Herrmann (among others).  The score also incorporates electronic music and influences from Simonettis earlier work on Argento films, such as Suspiria and Phenomena (film)|Phenomena. 

The piece at the end of Simonettis "Mater Lachrimarum" is called "Dulcis in Fondo" and was performed by his heavy metal band, Daemonia.  Cradle of Filth frontman Dani Filth recorded a song with Simonetti, Mater Lacrimarum, for the soundtrack of the film. 

The soundtrack was recorded in the Acquario Studio of Castelnuovo in Porto-Roma. The symphonic orchestra parts were performed by the Orchestra D.I.M.I. The choral parts were performed by the Nova Lyrica chorus in February 2007.  Both were recorded in Lead Studios in Rome with the help of sound-man Giuseppe Ranieri. Filmmakers finished dubbing the soundtrack into the film on 5 April 2007.  At the preview during the Cannes Film Festival, journalist Alan Jones described Simonettis score as an "unqualified success". 

The soundtrack was released around the same time as the films Italian wide release (31 October 2007) by Edel Music. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 