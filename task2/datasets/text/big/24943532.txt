Legend of the Fist: The Return of Chen Zhen
 
 
{{Infobox film
| name = Legend of the Fist:  The Return of Chen Zhen
| image = Legend of the Fist- The Return of Chen Zhen poster.jpg
| caption = Theatrical poster
| film name = {{Film name| traditional = 精武風雲－陳真
| simplified = 精武风云－陈真
| pinyin = Jīng Wǔ Fēng Yún－Chén Zhēn
| jyutping = Zing1 Mou5 Fung1 Wan4－Can4 Zan1}}
| director = Andrew Lau
| writer = Cheung Chi-shing Gordon Chan
| producer = Andrew Lau Gordon Chan Anthony Wong Huang Bo
| music = Chan Kwong-wing
| editing = Azrael Chung
| cinematography = Andrew Lau Ng Man-ching Media Asia Films Enlight Pictures Shanghai Film Media Asia Basic Pictures Media Asia Distribution
| released =  
| runtime = 106 minutes
| country = Hong Kong China
| language = Cantonese Mandarin Japanese
| budget = ¥120 million 
| gross = ¥136 million
}}
Legend of the Fist: The Return of Chen Zhen is a 2010 Hong Kong  , China. The film was shown out of competition during the opening night of the 67th Venice International Film Festival,  and 2010 Toronto International Film Festival.  The film was released in Chinese theatres on 21 September 2010 and two days later in Hong Kong on 23 September 2010.

==Plot== Allies fight the Germans in France during World War I. He returns to China after the war and takes on the identity of Qi Tianyuan, a comrade who was killed in action.

Back in Shanghai International Settlement|Shanghai, Chen joins an underground resistance movement to stop the Empire of Japan from invading China. He befriends Liu Yutian, the owner of a nightclub frequented by foreign dignitaries. He becomes attracted to Kiki, a nightclub singer who is actually a Japanese undercover agent. One night, Chen discovers that the Japanese are planning to assassinate General Zeng, the son of a northern warlord, and push the blame to General Zhuo, a rival warlord. Zengs death will spark off a civil war between the two warlords and aid the subsequent Japanese invasion. Chen disguises himself as a masked superhero and defeats the assassins and saves Zeng.

With the operation failed, Tokyo sends a name list of anti-Japanese activists to Colonel Chikaraishi Takeshi, the leader of the Japanese secret agency in Shanghai, ordering him to kill the people on the list. Chikaraishi leaks out the list, causing panic among the populace, and pays a visit to the nightclub. Chikaraishi is aware of Chens true identity and he challenges Chen to save the people on the list. Chen and Chikaraishi engage in a race of time to save and assassinate the activists respectively. Some are killed while others managed to escape from Shanghai. Eventually, Chikaraishis younger brother leads a team of assassins to murder the editor of the Shanghai Times. Chen kills Chikaraishis brother, but fails to save the editor in time.

In the meantime, Chen figures out that Kiki is a spy and warns her to leave. Chikaraishi starts to distrust Kiki and forces her to kill General Zengs girlfriend, one of her close friends. The blame is pushed to General Zhuo and the angered General Zeng attacks Zhuo with support from Japanese forces. One night, the Japanese corner Chen on the street and bring him to their headquarters to torture him. Concurrently, Chens friends stage a raid on the Japanese headquarters and cause serious damage with explosives before fleeing the scene. The Japanese track down Chens comrades and murder them in revenge later. Chen is thrown out of a car in front of the nightclub and remains in coma for days while he recovers from his injuries.

With the Japanese invasion underway and General Zhuo killed in action with his forces in full retreat, there seems to be nothing that the resistance movement can do to prevent the Japanese from occupying Shanghai. Chikaraishi challenges Chen to fight him, and kills Kiki after he shows up. In anger, Chen defeats many Japanese combatants all at once, after which he faces Chikaraishi in a one-on-one bout and wins. At the end of the film, Chikaraishi is replaced with another officer while Chen continues to help the resistance movement oppose the invaders.

==Cast== Chen Zhen, the protagonist of the film.
* Shu Qi as Kiki / Fang Qing / Captain Yamaguchi Yumi, a Japanese spy. Anthony Wong as Liu Yutian, the owner of the nightclub Casablanca.
* Huang Bo as Inspector Huang Haolong, a police inspector.
* Kohata Ryu as Colonel Chikaraishi Takeshi, the antagonist of the film. Akira as Chikaraishi Sasaki, Colonel Chikaraishis younger brother.
* Yasuaki Kurata as Tsuyoshi Chikaraishi, Colonel Chikaraishis father, the Hongkou dojo master who was defeated and killed by Chen Zhen years ago.
* Zhou Yang as Qi Zhishan, Qi Tianyuans sister.
* Huo Siyan as Weiwei, General Zengs girlfriend.
* Shawn Yue as General Zeng, a northern warlord whom Chen Zhen saves from assassins.
* Ma Yue as General Zhuo, General Zengs rival. Ma Su as General Zhuos wife
* Chen Jiajia as Huang Yun, a Japanese spy working at the Casablanca.
* Zhang Songwen as Wenzai, the editor of the Shanghai Times.
* Lü Xiaolin as Qiuting, a student protester.

==Development== Chen Zhen Fist of film of the same title which starred Bruce Lee as Chen Zhen.

In February 2007, Gordon Chan announced plans to make a follow-up feature film to the 1994 film Fist of Legend. Chan, who was also the writer and director of Fist of Legend, announced that Donnie Yen would replace Jet Li as Chen Zhen in the film.   

==Production== Media Asia Films, Laus production company Basic Pictures, and Chinese film producer Enlight Pictures. 
 Chen Zhen is in his 20s, while in this film Chen would be in his 30s. 

===Casting=== Anthony Wong was announced to be a part of the supporting cast, prior to principal photography, the supporting cast members for the film was announced. Zhou Yang and Shu Qi will play two women competitively vying for Chen Zhens affection; Huang Bo will play an underdog fighter; Anthony Wong plays a local crime boss.   

===Filming===
Principal photography for Legend of the Fist: The Return of Chen Zhen began on 15 November 2009, following a press conference attended by the cast and crew; filming took place in Shanghai.  and concluded in early February.   

===Action choreography===
The fight scenes were choreographed by Donnie Yen. For this film, Yen mentioned that he included nunchaku and the screaming elements as a tribute to Bruce Lee, who played Chen Zhen in the 1972 film Fist of Fury.    Furthermore, he incorporated many mixed martial arts (MMA) elements in the film, coupled with the utilisation of Wing Chun. MMA is an interdisciplinary form of fighting utilising elements of Brazilian Jiu-Jitsu, judo, karate, boxing, kickboxing & wrestling, which are evidently in the film.

Yen also stated that the concept behind Bruce Lees Jeet Kune Do is similar to that of Mixed martial arts|MMA, hence the incorporation of many forms of martial arts is a necessity in this film.   

===As a tribute to Bruce Lee===
 
 Chen Zhen The Green Hornet), Legend of the Fist: The Return of Chen Zhen is a tribute and dedication to Lee. 

==Release==
Legend of the Fist: The Return of Chen Zhen was released theatrically in Hong Kong on 23 September 2010.  It was further announced that the film will be released to theatres in the United States in its original, uncut international version by Variance Films on 22 April 2011. Thus, a total of 10 minutes of non-fighting scenes will be included.

==Box office==
The film grossed ¥65 million and ranked first in the box office during the first week of its theatrical run in China. Despite so, its total gross rose to only ¥136 million a month later. Donnie Yen was unhappy with the film distributors because many scenes were removed (about a total of 10 minutes of non-fighting scenes), and he commented on his Sina Weibo that the films overall gross would not exceed ¥200 million.  However, producers Andrew Lau and Gordon Chan think that the film will still express what they want and gross well in other countries.   

==Reception==
Legend of the Fist: The Return of Chen Zhen has been praised for its action scenes and Donnie Yens performance, and received mostly positive reviews.    

As per negative reviews, Total Film gave the film two stars out of five, stating that the film "only comes alive when showcasing Yens flamboyant fight choreography, glimpsed far too seldom – the longeurs in between the snappy scraps are sloppily written, and the clumsy Chinese nationalism is tedious."  The Guardian gave the film two stars out of five finding the plot to be confusing.  The Hollywood Reporter gave the film a mixed review referring to the film as "a popcorn movie of epic proportions" and that "one expects more from producer Gordon Chan and director Andrew Lau".  On Rotten Tomatoes, the film received a 48% "rotten" rating based on 33 reviews. 

Other reviews commented that the film is not about Chen Zhen, but a Hollywood-style superhero film; a combination of James Bond (film series)|007, Spider-Man in film|Spider-Man, and Batman in film|Batman.  Director Andrew Lau responded that a Hollywood-style superhero film is what people want to see, and he thinks that a 6:4 ratio of Hollywood style to traditional style is just right for the film. 

==See also==
*Donnie Yen filmography
*Hong Kong films of 2010
*Fist of Legend
*Fist of Fury Chen Zhen

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 