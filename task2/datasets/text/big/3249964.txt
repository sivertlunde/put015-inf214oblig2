House by the River
{{Infobox film
| name           = House by the River
| image          = HouseByTheRiver.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Fritz Lang
| producer       = Howard Welsch
| screenplay     = Mel Dinelli
| based on       =  
| starring       = Louis Hayward Jane Wyatt Lee Bowman Dorothy Patrick
| music          = George Antheil Edward J. Cronjager Arthur Roberts
| studio         = Fidelity Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
House by the River is a 1950 Gothic film noir directed by Fritz Lang, and starring Louis Hayward, Jane Wyatt, Lee Bowman, and Dorothy Patrick. 

==Plot==
A rich novelist, Stephen Byrne, who lives and works by a river, kills his attractive maid after she begins screaming following a drunken pass.  The writer, with the help of his limping brother, loads the body into an old sack and the pair throws the body into the river.  Unfortunately, the body comes back up and floats by the house days later.  Despite desperate attempts by the brothers to get the body, the police end up recovering it.

Byrne, who uses the womans disappearance and killing for publicity for his books, realizes that his brother is the one who is going to be accused of the crime.  Meanwhile, he begins writing a book about the crime in another attempt to cash in on the scandal.  His wife and brother begin to fall in love, despite the fact that he has become the prime suspect.

==Cast==
* Louis Hayward as Stephen Byrne
* Jane Wyatt as Marjorie Byrne
* Lee Bowman as John Byrne
* Dorothy Patrick as Emily Gaunt
* Ann Shoemaker as Mrs. Ambrose
* Jody Gilbert as Flora Bantam
* Peter Brocco as Harry - Coroner
* Howland Chamberlain as District Attorney
* Margaret Seddon as Mrs. Whittaker - Party Guest
* Sarah Padden as Mrs. Beach
* Kathleen Freeman as Effie Ferguson - Party Guest Will Wright as Inspector Sarten
* Leslie Kimmell as Mr. Gaunt
* Effie Laird as Mrs. Gaunt

==Reception==

===Critical response===
When the film was first released, film critic for The New York Times, Bosley Crowther, panned the film, writing, "... we fear that neither the enlightenment nor the excitement that a customer might expect in such a flickering melodrama is provided by this film ... the script by Mel Dinelli, based on a novel by A. P. Herbert, is shy on genuine melodrama, it provides little in the way of suspense (since you know that the killer is bound to get his) and it comes to a weak and cheerless end. It seems that the killer is a novelist and unconsciously writes an exposure in his new book. This is about as measly a way to catch a man as we know." 

More recently, film critic Tom Vick praised the film, writing, "Lang beautifully evokes the Victorian era with his customary attention to detail. Cinematographer Edward J. Cronjagers low-key lighting fills the Byrnes mansion with appropriately gloomy shadows, and the moonlit river scenes make it seem as if nature itself is offended by the crime. Avant-garde composer George Antheils haunting score is the perfect accompaniment to this chilling and unconventional exercise in suspense." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 