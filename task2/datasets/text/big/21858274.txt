Hello, That's Me!
 
{{Infobox film
| name           = Hello, Thats Me! (Բարև, ես եմ)
| image          = 
| caption        = 
| director       = Frunze Dovlatyan
| producer       = 
| writer         = Arnold Agababov
| narrator       = 
| starring       = Armen Dzhigarkhanyan
| music          = 
| cinematography = Albert Yavuryan
| editing        = G. Miloserdova
| distributor    = 
| released       =  
| runtime        = 137 minutes
| country        = Soviet Union
| language       = Armenian
| budget         = 
}}

Hello, Thats Me! ( ; Transliteration|translit.&nbsp;Barev, yes em) is a 1966 Armenian drama film directed by Frunze Dovlatyan.  It was entered into the 1966 Cannes Film Festival, nominated to Palme dOr    and awarded by the State Prize of Armenia in 1967.

The film is based on Artem Alikhanians biography.  

==Plot==
Artyom Manvelyan is a famous physicist and founder of a cosmology laboratory in Aragats. With loyalty and gentleness he keeps the memories of World War period, lost love and his friends. 

==Cast==
* Armen Dzhigarkhanyan - Artyom Manvelyan
* Rolan Bykov - Oleg Ponomaryov
* Natalya Fateyeva - Lyusya
* Margarita Terekhova - Tanya
* Luchana Babichkova - Irina Pavlovna
* Frunze Dovlatyan - Zaryan
* Galya Novents - Nazi
* Georgi Tusuzov - Aharon Izrailevich
* Aleksei Bakhar - Stepfather
* Natalya Vorobyova - Tanya in childhood (as Natasha Vorobyova)
* Martyn Vartazaryan - (as Martin Vardazaryan)

==See also==
* List of Armenian films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 