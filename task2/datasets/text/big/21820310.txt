Challenge (2009 film)
 
{{Infobox film
| name = Challenge
| image = Challengefilm.jpg
| caption = Theatrical Release Poster
| director = Raj Chakraborty
| writer = 
| producer = Shree Venkatesh Films Dev Subhashree Ganguly
| cinematography =Soumik Haldar {{Cite news|url=http://www.telegraphindia.com/1081108/jsp/entertainment/story_10079598.jsp
|title=Spotlight|work=The Telegraph|accessdate=6 March 2009|location=Calcutta, India|date=8 November 2008}} 
| editing = 
| music = Jeet Ganguly
| distributor = 
| released =   
| runtime = 120 minutes
| country = India
| language = Bengali
| budget         =
| gross          = screenplay = N.K. Salil
}} Bengali romantic Dev and Subhashree Ganguly.The film is partially based on the Tamil film Kuthu which stars Silambarasan and Divya Spandana in lead roles. And it is an official  remake of the Telugu Film   Bunny  {{Cite web
|url=http://calcuttatube.com/2009/01/25/subhashree-exclusive-interview/ |title=Subhashree – Exclusive Interview|publisher=calcuttatube.com|accessdate=5 March 2009}}       For few scenes and music-videos, shooting was done in Dubai,  Australia and New Zealand. The film has a blockbuster sequel named Challenge 2.

==Plot==
It is a romantic comedy film. Subhasree Ganguly portrays a girl with an extremely conservative/obsessive father. Her father just cant tolelate any other guy looking at his daughter face to face. He can not accept any boy who tries to be friend with his daughter. He terrifies, beats everybody to protect his daughter. Pooja Subhasree Ganguly in the film also loves her dad but at the same time dreams about someone capable enough to challenge her dad. Who can really fight her dad for her. Here comes our hero Abhir Dev to save and romance Pooja. Challenge is a stunning love story. In the fear of Poojas boyfriend, her father sends her to his father-in-laws home. But at last, the couples get married and after some years they had a son.


Director Raj Chakraborty keeps the tenor breezy with music, humour and cute romance. Somak Mukherjees cinematography helps do the trick for Dev.

Challenge belongs to Dev. He does set hearts aflutter. He looks good, dances well and carries off the angry young man attitude with panache. And yes, he can act too. Subhashree Ganguly, sweet and sassy, plays Miss Touch-me-not with poise but a little more effort would have helped her match up to her suave and spunky hero.

== References ==
 

==External links==
* 

 