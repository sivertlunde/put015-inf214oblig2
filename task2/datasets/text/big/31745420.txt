Condemned to Death
{{Infobox film
| name           = Condemned to Death
| image          = 
| image_size     = 
| caption        = 
| director       = Walter Forde
| producer       = Julius Hagen Brock Williams
| starring       = Arthur Wontner Gillian Lind Gordon Harker Cyril Raymond
| music          = Baynham Honri
| cinematography = Sydney Blythe   William Luff Jack Harris
| studio         = Twickenham Film Studios
| distributor    = Woolf & Freedman Film Service
| released       = 1932
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} British crime film directed by Walter Forde and starring Arthur Wontner, Gillian Lind and Gordon Harker.  It was adapted from the play Jack OLantern by James Dawson which was itself based on a novel by George Goodchild.

Thought to have been Lost film|lost, "a cut version dubbed in French" was found as a result of a 1992 British Film Institute campaign to locate missing movies. 

==Plot==
A respected judge leads a double life as a murderer.

==Cast==
* Arthur Wontner – Sir Charles Wallington 
* Gillian Lind – Kate Banting 
* Gordon Harker – Sam Knudge 
* Cyril Raymond – Jim Wrench 
* Jane Welsh – Sonia Wallington 
* Norah Howard – Gwen Banting 
* Edmund Gwenn – Banting 
* Griffith Humphreys – Professor Michaels 
* T. Gordon Blythe – Ali 
* James Cunningham – Inspector Sweeting 
* Gilbert Davis – Doctor Cornell 
* Bernard Brunel – Tobias Lantern 
* H. St. Barbe West – Sir Rudolph Cantler

==References==
 

 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 