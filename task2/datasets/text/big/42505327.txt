December-1
{{Infobox film
| name           = December-1
| image          = December-1 Kannad Movie.jpg
| director       = P. Sheshadri
| producer       = Basantkumar Patil
| writer         = P. Sheshadri
| starring       = Niveditha Santosh Uppina H. G. Dattatreya
| music          = V. Manohar
| cinematography = Ashok V. Raman
| editing        = B. S. Kemparaju
| released       =  
| runtime        = 98 minutes 
| country        = India Kannada
}} Kannada film written and directed by P. Sheshadri starring Niveditha, Santosh Uppina and H. G. Dattatreya in lead roles.
 National Film Best Feature Best Screenplay Best Actress. 

==Plot==
Basupura is a tiny village where Madevappa works here in a flour mill, and his wife Devakka makes rotis and sells in nearby towns. A school-going son, a daughter still in cradle and Madevappas aged mother are the others in the family. It is a hand-to-mouth existence; but they have got used to it, with their own small pleasures and distant dreams.

The whole village is getting geared up for the Chief Minister (CM)s visit on 1st December.  CMs officially scheduled overnight stay in Madevappas house  naturally gives  Madevappa & family  extra attention from all quarters giving them the feeling of a higher status and a brighter future. They look forward to the event with all the zeal and enthusiasm.

CMs visit goes on well with a grand welcome, dinner with Madevappa, Photo Shoots, Press Meets, discussions with local leaders etc. Madevappa & family are over-whelmed by the proceedings, but hardly get to interact with the CM.  Instead, they face embarrassing moments feeling as though they are outsiders in their own house.

Wide media coverage highlights CM’s exhortations on the need for humanitarian approach in dealing with people suffering from serious health setbacks.  Though well intended it creates difficulties to Madevappa and family driving them to despair.

== Cast ==
* Niveditha as Devakka
* Santosh Uppina as Madevappa
* Mujahid M a.k.a Shayar Mujju
* Shanthabai Joshi as Devakkas mother
* Shashikumar

== Reception ==
December-1 opened to positive response from critics upon its theatrical release. G. S. Kumar of The Times of India gave the film a rating of four out of five and wrote, "The script has elements of tension, suspense, sarcasm and humour. Kudos to Sheshadri for the excellent narration that keeps the film moving at a fast pace." and praised the roles of acting, cinematography, music and costume designing departments in the film.  A. Sharadhaa of The New Indian Express wrote, "Crisp and witty dialogues, great performances, a pithy screenplay, edgy camerawork...nothing has gone wrong with the film, which is about a family of a struggling husband, an enterprising wife, their two children and a grandmother." and concluded writing, "Despite being an artistic film, December 1 has great potential to do well commercially."  Shyam Prasad S. of the Bangalore Mirror wrote of the film, "... December 1 is an art film that has the best potential for commercial success among all such films in recent years. The director brings to life a telling presentation of a village family destroyed by a political gimmick." and concluded writing, "There is nothing legally wrong in what the CM and the media did in the film. But if the film sends the wrong message to the intended audience, it makes for a bad postscript for December 1."  

==Awards==
;61st National Film Awards Best Screenplay (original) – P. Sheshadri  Best Feature Film in Kannada – P. Sheshadri and Basantkumar Patil

;2013 Karnataka State Film Awards Best Actress – Niveditha

==References==
 

==External links==
*  

 
 

 
 
 