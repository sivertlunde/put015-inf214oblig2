Think Like a Man
{{Infobox film
| image          = ThinkLikeAManPoster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Tim Story  Will Packer
| screenplay     = Keith Marryman  David A. Newman
| based on       =   Kevin Hart
| starring       = {{Plain list|
* Michael Ealy 
* Jerry Ferrara 
* Meagan Good 
* Regina Hall  Kevin Hart 
* Terrence J 
* Taraji P. Henson 
* Romany Malco 
* Gabrielle Union 
}}
| music          = Christopher Lennertz
| cinematography = Larry Blanford
| editing        = Peter S. Elliot
| studio         = Rainforest Films
| distributor    = Screen Gems
| released       =  
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = $12 million 
| gross          = $96,070,507 
}} ensemble American romantic comedy film directed by Tim Story, written by Keith Marryman and David A. Newman, and based on Steve Harveys 2009 book Act Like a Lady, Think Like a Man. The film was released on April 20, 2012 by Screen Gems. 

==Plot==
The film follows four storylines about each of the couples, titled:

* "The Mamas Boy" vs. "The Single Mom"
* "The Non-Committer" vs. "The Girl Who Wants the Ring"
* "The Dreamer" vs. "The Woman Who Is Her Own Man"
* "The Player" vs. "The 90 Day Rule Girl"

Each of the women are readers of Steve Harveys book Act Like a Lady, Think Like a Man. When the men learn that the women are hooked on Harveys advice, they try to turn the tables on their mates, which later seems to backfire.

==Cast==
  Kevin Hart as Cedric, "The Happier Divorced Guy"
* Michael Ealy as Dominic, "The Dreamer"
* Taraji P. Henson as Lauren Harris, "The Woman Who Is Her Own Man"
* Terrence J as Michael Hanover, "The Mamas Boy"
* Regina Hall as Candace Hall, "The Single Mom"
* Jerry Ferrara as Jeremy Kern, "The Non-Committer"
* Gabrielle Union as Kristen, "The Girl Who Wants the Ring"
* Romany Malco as Zeke Freeman, "The Player"
* Meagan Good as Mya, "The 90 Day Rule Girl"
* Steve Harvey as Himself Gary Owen as Bennett, "The Happily Married Man"
* Wendy Williams as Gail, Cedrics ex-wife Chris Brown as Alex, Myas ex
* Keri Hilson as Heather
* Jenifer Lewis as Loretta Hanover, Michaels mom
* La La Anthony as Sonia, Myas friend
* Caleel Harris as Duke, Candaces son
* Morris Chestnut as James Merrill, Laurens ex
* Arielle Kebbel as Gina, Kristens friend
* Kelly Rowland as Brenda
* Sherri Shepherd as Vicki
* Tika Sumpter as Dominics girlfriend
* Tony Rock as Xavier, Zekes friend
* Luenell as Aunt Winnie Hall

 

Six professional basketball players made cameo appearances as themselves:
* Matt Barnes
* Shannon Brown
* Rasual Butler
* Darren Collison
* Lisa Leslie
* Metta World Peace

==Reception==

===Critical response===
The film received mixed reviews. Review aggregation website Rotten Tomatoes gives the film a rating of 53%, based on 96 reviews, with an average rating of 5.6/10, and the sites critical consensus states, "In Think Like a Man, an otherwise standard romantic comedy|rom-com is partially elevated by a committed&mdash;and attractive&mdash;cast, resulting in a funny take on modern romance".    The film also holds a score of 51 out of 100 on the website Metacritic, based on 30 reviews, indicating "mixed or average reviews".    
 Kevin Hart. Michael Phillips of the Chicago Tribune commended the film for "stick  to a formula without falling prey to it" and commented that "its hangout factor is considerable, because the actors charms are considerable." 

===Box office=== The Hunger Games  four-week run at the List of 2012 box office number-one films in the United States|#1 spot at the U.S. box office.  The romantic comedy film remained on top of the competition during its second week as well, bringing in $17.6 million.  

As of June 24, 2012, Think Like a Man has earned $91,547,205 in both the United States and Canada, along with  $4,523,302 in other countries, for a worldwide total of $96,070,507. The movies production budget was $12.5 million. 

==Soundtrack==
The films soundtrack includes songs by Kelly Rowland, Jennifer Hudson, Keri Hilson, John Legend and Future (rapper)|Future. 
{{Track listing
| headline        =Original soundtrack  . Retrieved April 28, 2012 
| extra_column    = Performer(s)
| writing_credits = yes Think Like a Man
| length1         = 4:01
| writer1         = Ne-Yo, Harmony Samuels, Al Sherrod Lambert, Courtney Harrell, Eric Bellinger
| extra1          = Jennifer Hudson and Ne-Yo featuring Rick Ross
| title2          = Tonight (Best You Ever Had)
| writer2         = Allen&nbsp;Arthur, Christopher Bridges, Keith&nbsp;Justice, Miguel (singer)|Miguel, Clayton&nbsp;Reilly, John Legend
| extra2          = John Legend featuring Ludacris
| length2         = 3:59
| title3          = Need a Reason
| length3         = 4:16
| writer3         = Kelly Rowland, Bei Maejor, Vanessa Carlton, T. Boi Future and Bei Maejor
| title4          = Wont Make a Fool Out of You
| note4           = 
| writer4         = Marcus Canty
| extra4          = Marcus Canty
| length4         = 4:13
| title5          = Baby Be Mine
| writer5         = Rod Temperton
| extra5          = Quadron
| length5         = 4:17
| title6          = Thats the Way of the World (Earth, Wind & Fire song)|Thats the Way of the World
| note6           =
| writer6         = Maurice White, Charles Stepney, Verdine White
| extra6          = Earth, Wind & Fire
| length6         = 5:45
| title7          = Freedom Ride
| note7           =
| writer7         = Keri Hilson, Arden&nbsp;Altino, Olivier&nbsp;Castelli, Akene&nbsp;Dunkley, Jerry Duplessis
| extra7          = Keri Hilson
| length7         = 3:43
| title8          = Shake That Jelly
| writer8         = Patrizio&nbsp;Pigliapoco, Christopher&nbsp;Trujillo, William&nbsp;Wesson
| extra8          = Billy Wes
| length8         = 3:15
| title9          = Same Ole BS
| length9         = 3:44
| writer9         = Ronnie&nbsp;Jackson, Philip&nbsp;Cornish, Eric&nbsp;Cire
| extra9          = RaVaughn
| title10         = Fire
| length10        = 3:47
| writer10        = Brandon Hines, Antoine Harris, J-Hype
| extra10         = Brandon Hines
| title11         = Motion Picture
| note11          = 
| length11        = 4:04 Future
| extra11         = Future
| title12         = Never Too Much
| note12          = 
| writer12        = Luther Vandross
| extra12         = Luther Vandross
| length12        = 3:50
| total_length    = 48:50
}}

==Sequel==
On June 28, 2012 Screen Gems announced plans for a sequel Think Like a Man Too, with Keith Merryman and David A. Newman again writing the script.  The film was released June 20, 2014. 

==References==
{{Reflist|refs=

   

   

   

}}

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 