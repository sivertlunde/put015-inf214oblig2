Kahte Hain Mujhko Raja
{{Infobox film
| name           = Kahte Hain Mujhko Raja
| image          = 
| image_size     = 
| caption        = 
| director       = Biswajeet
| producer       = 
| writer         =
| narrator       = 
| starring       = Dharmendra  Biswajeet  Rekha   Hema Malini
| music          = Rahul Dev Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1975
| runtime        = 2:06:11
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1975 Bollywood film directed by Biswajeet. The film stars Dharmendra, Biswajeet, Rekha and Hema Malini.

==Cast==
*Dharmendra   
*Biswajeet   
*Hema Malini   
*Rekha   
*Shatrughan Sinha   
*Nadira   
*Bipin Gupta
*Keshto Mukherjee   
*Abhi Bhattacharya    Alka

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Aiyo Re Gaya Kaam Se"
| Kishore Kumar, Asha Bhosle
| 4:07
|-
| 2
| "Kahtey Hain Mujhko Raja"
| Kishore Kumar
| 5:38
|-
| 3
| "Bam Chike Chike Cham Chike Chike"
| Kishore Kumar
| 4:36
|-
| 4
| "Jiya Mein Toofan Jaga Ke"
| Asha Bhosle
| 4:58
|-
| 5
| "Maine Kab Chaha Ki"
| Mohammed Rafi, Asha Bhosle
| 5:38
|-
| 6
| "Liyo Na Babu Tanik Piyo Na"
| Asha Bhosle
| 3:17
|}

==External links==
*  

 
 
 
 
 