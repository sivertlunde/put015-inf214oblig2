Landfall (film)
{{Infobox film
| name           = Landfall
| image_size     =
| image	=	
| caption        =
| director       = Ken Annakin
| producer       = 
| writer         = 
| based on = novel by Nevil Shute
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = 
| released       = 1949
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         =
| gross          =£141,127 (UK) 
| preceded_by    =
| followed_by    =
}}
Landfall is a  , written by author Nevil Shute.

==Cast==
* Michael Denison as Rick 
* Patricia Plunkett as Mona 
* Kathleen Harrison as Monas Mother 
* Denis ODea as Captain Burnaby 
* David Tomlinson as Binks 
* Joan Dowling as Miriam, the Barmaid 
* A.E. Matthews as Air Raid Warden 
* Maurice Denham as Wing Commander Hewitt 
* Margaretta Scott as Mrs. Burnaby  Sebastian Shaw as Wing Commander Dickens 
* Nora Swinburne as Admirals Wife 
* Charles Victor as Monas Father 
* Laurence Harvey as Petty Officer Hooper  Paul Carpenter as Petty Officer Morgan 
* Frederick Leister as Admiral Blackett 
* Hubert Gregg as Lieutenant Commander Dale 
* Walter Hudd as Professor Legge
* Margaret Barton as Rosemary - Ricks Sister 
* Edith Sharpe as Mrs. Chambers - Ricks Mother 
* Ivan Samson as Commander Rutherford

==Synopsis==
A British coastal command pilot sinks what he believes   to be a German submarine, which had sunk a British submarine, for which sinking the pilot is blamed. He is charged with neglect, and volunteers for a deadly mission. His girlfriend meanwhile tries to prove that he is innocent, and did sink a German U-boat rather than a British one. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 