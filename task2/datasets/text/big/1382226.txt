The Naked Jungle
 
{{Infobox film
| name           = The Naked Jungle
| image          = Thenakedjungle.jpg
| image size     =
| caption        = Promotional film poster
| director       = Byron Haskin
| producer       = George Pal Carl Stephenson
| starring       = Eleanor Parker Charlton Heston Abraham Sofaer William Conrad
| music          = Daniele Amfitheatrof
| cinematography = Ernest Laszlo, ASC
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 min.
| country        = United States
| awards         = English
| budget         =
| gross          = $2.3 million (US) 
}}
 Carl Stephenson. 

==Plot==
 cocoa plantation to meet her new husband, plantation owner Christopher Leiningen (Charlton Heston), whom she has married by proxy.

Leiningen is cold and remote to her, rebuffing all her attempts to make friends with him. Shes beautiful, independent, and arrives ready to be his stalwart helpmate; however, no one has told him shes a widow. He rejects her.
 Marabunta - will strike in a few days time. Leiningen refuses to give up the home he fought so hard to create. Instead of evacuating, he resolves to make a stand against this indomitable natural predator. Joanna joins the fight to save the plantation; their courage and his probable loss of all hes worked for may crack his resolve to send her away.

==Production== The War Tom Thumb, The Time Machine.
 Marabunta ants are not exactly like the ones in the movie. They dont lie dormant for 27 years and they dont eat large vertebrates. They are also accompanied by Antbirds and various other birds that eat some of what they cant.

William Conrad, who had starred as Leiningen in a 1948 adaptation of Stephensons story for the radio program Escape (radio program)|Escape, appears in the film as a district commissioner.   escape-suspense.com. Retrieved: August 8, 2013. 

The unique "sound" of the ants devouring everything in their path was created by swirling a straw in a glass of water with crushed ice, which was then amplified.

Much of the Rio Negro (Amazon) jungle riverscape, as well as the bridge dynamiting and sluice scenes, are second-unit stock footage shot in Florahome, Florida, according to IMDb.

==References==
 

== External links ==
*  
*  
Streaming audio
*  on Escape (radio program)|Escape: January 14, 1948 
*  on Lux Radio Theater: June 7, 1954

 

 
 
 
 
 
 
 
 
 
 
 
 