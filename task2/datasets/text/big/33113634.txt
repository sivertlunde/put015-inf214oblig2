The Unforgettable Year 1919
{{Infobox film
| name           =  The Unforgettable Year 1919
| image          =
| alt            =
| caption        =
| director       = Mikheil Chiaureli
| producer       = Viktor Tsirgiladze
| writer        = Vsevolod Vishnevsky
| screenplay = Vsevolod Vishnevsky, Alexander Filmonov, Mikheil Chiaureli
| narrator       =
| starring       = Mikheil Gelovani
| music          =Dmitri Shostakovich (composer), Aleksandr Gauk (conductor)
| cinematography = Leonid Kosmatov, Vitaly Nikolaev
| editing        = Tatiana Likhacheva
| studio         = Mosfilm
| distributor    =
| runtime        = 108 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 10,936,000 Soviet Ruble 
| gross          =
}}
 
The Unforgettable Year 1919 (Russian language|Russian: Незабываемый 1919-й)
is a 1951 Soviet film directed by Mikheil Chiaureli. It is considered an important representation of Joseph Stalins cult of personality.

==Plot== High Soviet is demoralized and about to order an evacuation, while the White fifth column inside it plots an insurrection. The Krasnaya Gorka fort dispatches a detachment of Baltic Fleet sailors to assist Petrograd, among them the young Vladimir Shibaev. As the Red Army faces defeat by the Whites, Joseph Stalin arrives on the battlefield, rallies the communists and routs the enemy, saving the city.

==Cast==
*Mikheil Gelovani  - Joseph Stalin
*Pavel Molchanov - Vladimir Lenin Boris Andreyev - Shibaev
*Gabriel Belov  - Mikhail Kalinin
*Victor Stanitsin  - Winston Churchill
*Gnat Yura  - Georges Clemenceau
*Viktor Koltsov  - Lloyd George
*Nikolai Komissarov  - General Neklyudov
*Sergei Lukyanov  - General Rodzyanko
*Paul Massalsky  - Colonel Vadbolsky
*Vladimir Ratomsky  - Potapov
*Gleb Romanov  - commander of the armored vehicles
*Marina Kovaleva  - Katya Danilova
*Angelina Stepanova  - Olga Butkevich
*Eugene Morgunov  - Anarchist
*Vsevold Sanaev - Boris Savinkov

==Production==
The script was adapted from a play by the same name,  that was composed by  ‏ wrote that Vishnevskys play "magnified Stalins Russian Civil War record beyond all recognition".  Chiaurelis work was one of the only nine Soviet pictures produced during 1951.  With a budget of nearly 11,000,000 rubles,   arbinada.com  it was also the most expensive film made in the Soviet Union up to that time.  In addition, it was the last of Chiaurelis "super-productions about Stalin." 

==Reception==
The Unforgettable Year 1919 was heavily promoted by the Soviet press months before its release. It was watched by 31.6 million people in the USSR, becoming the countrys fifth highest-grossing picture of 1952, coming behind four old American Tarzan movies from the 1930s. 

The film won the   critic wrote that, in 1919, "Young Stalin stands in white-silk armor and arranges the defense of Leningrad... While the traitors receive their deserved bullet in the head". He added that it was only screened in East Germany. 
 Central Committee for "having significant shortcomings and lower ideological-artistic merits than those previously released by the director."  In the summer of 1953, after Stalins death, it was removed from circulation. 

In February 1956, Premier  , our dear friend, find the necessary courage and write the truth about Stalin; after all, he knows how Stalin had fought."  At March, the pro-Stalin protesters in the 1956 Georgian demonstrations included re-screenings of the film in their list of demands. 

Peter Kenez noted that the film was the last made about the October Revolution and Civil War in the Stalinist period.  Louis Menashe regarded 1919 as one of the post-war pictures in which "Stalin monopolized all heroism".  William Luhr described it as "a highly elaborate and costly production... Another attempt at myth-making... In which Stalin is given the sole credit for crushing the anti-Bolshevik uprising."  Ann Lloyd and David Robinson referred to as "the eminently forgettable The Unforgettable Year 1919." 
 The Fall of Berlin, it still contained "a grain of historical truth... Stalin was the USSRs leader during World War II." But in 1919, he was depicted in a completely a-historical manner: "he was not the head of the party at 1919, nor was he a Civil War hero." Denise J. Youngblood. Russian War Films: On the Cinema Front, 1914-2005. Lawrence: University Press of Kansas (2007). ISBN 0-7006-1489-3. page 102-103.  John Riley added that during the relevant period in the Civil War, Stalin was stationed in Moscow, where he functioned as the Peoples Commissar for Nationalities. 

Nikolas Hüllbusch, who researched Stalins representations in cinema, wrote that the portrayal of premiers propagandistic "screen alter-ego" reached its "zenith" in the The Fall of Berlin, and "this development marked its atrophic crisis." According to Hüllbusch, the officially-sanctioned artistic line took a turn already in 1952, and the attempts to use Stalins figure were frowned upon. Consequently, The Unforgettable Year 1919 and other Stalinist works from that year "had little notability... And were forgotten after the political reshuffle of 1953." 

==References==
 

==External links==
*  on the IMDb.
*  on Rotten Tomatoes.
*  on kino-teatr.ru.

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 