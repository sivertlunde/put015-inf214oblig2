Rash (film)
 
 
{{Infobox film
| name           = Rash
| image = Rash documentary film poster.jpg
| director       = Nicholas Hansen
| producer       = Nicholas Hansen
| writer         = Nicholas Hansen Rhyno Music
| editing        = Nicholas Hansen
| distributor    = Madman Entertainment
| released       = 2005
| runtime        = 73 min
| country        = Australia English
}}

Rash, written RASH, is a 2005 Australian documentary film, directed by Nicholas Hansen. Its subject is contemporary urban Australia and the artists who are making it a host for illegal street art. With the tagline Scratch it and it spreads, Rash explores the cultural value of unsanctioned public art and the ways that street art and graffiti contribute to public dialogue.

Directed by Nicholas Hansen and Mutiny Media, Rash was three years in the making and includes interviews with many of Melbourne’s inspired street art and graffiti artists as well as visitors who came to Melbourne and leave their mark. Rash is the first feature length documentary in Australia to focus on the new art form of street art.

Filming began in 2002 on this documentary, which conveys the commitment, ideals and beliefs demonstrated in Melbourne street art. Artists use a variety of approaches including bill posters, stencils, and performance art put the artwork right in the public eye.
 Melbourne 2006 London 2012 Summer Olympics  

In Rash the spirit of rebellion is channeled into street art and the visual conversations are spread across the walls of Melbourne. Rash offers a rare look inside these graffiti artists world-view.

A 2006 review stated ‘It is this rare spotlight on this hidden subculture that makes Rash so fascinating and potentially illuminating to those who view graffiti artists as merely vandals. In fact, it is this misconception of the graffiti artist and their role in society that Hansen’s film centrally explores. Rash reveals that Melbourne’s graffiti subculture is not only a very tight knit community where everyone knows what each other is doing. It is also governed by a set of ‘street etiquette’’. 

Prior to its 2006 TV broadcast in Australia Sacha Molitorisz of   in London.  Archival footage from Rash was licensed to Banksy|Banksys film production company Paranoid Pictures for the opening montage of documentary Exit Through the Gift Shop. 
 Civilian Dest, Dlux AKA James Dodd, KAB 101, Kano, Lister, Meek (street artist)|Meek, Miles Allinson, Prism (street artist)|Prism, Psalm, Reka, Sixten (street artist)|Sixten, Snog, Sync (street artist)|Sync, Tai Snaith, Tower, and Vexta.

The Rash DVD extras include short films: Girls Do Street Art, Gallery VS Street, Drawing Room, Phibs, Urban Express – Short Film 2004, Shepard Fairey (Interview) OBEY GIANT USA, SCIEN (Interview 8.5 mins), 123Klan – France.

==Awards==
* Best Australian Documentary, 2005, Film Critics Circle of Australia
* Runner up Audience Favourite Documentary, 2005 Melbourne International Film Festival

==See also==
* Street art
* Street art in Melbourne

==External links==
*  
*  

==References==
 

 
 
 
 
 
 