Valentine Road
Valentine Road is a documentary film by director Marta Cunningham. In 2008 Cunningham read a Southern Poverty Law Center article about the murder of an Out gender non-conforming 15 year-old, Lawrence King. He was shot and killed in his middle school classroom by fellow classmate 14 year-old Brandon McInerney.

Investigating the case Cunningham felt compelled to challenge what she perceived as a homophobic portrayal of King in the mainstream media. She began attending McInerney’s pre-trial motions and hearings. “The more she looked into the case, the more she uncovered a web of complications and nuance that just wasn’t being given a fair hearing by the media, let alone the courts.” 

Embedding herself in the rural blue collar town of Oxnard, Cunningham spent five years developing trust with the community and accumulating over 350 hours of footage. The result was a 89 minute documentary in which: “Cunningham masterfully weaves a kind of cinematic memorial quilt to King, who, just prior to his death, was living in a group home/treatment center away from his adoptive parents. Emotional input from the boys deeply supportive friends and teachers, including several who witnessed the classroom shooting, provides much of the backbone… The cases prosecuting attorney and McInerneys devoted defense lawyers, along with police detectives, hate-crime experts and several unapologetically pro-McInerney jurors, also weigh in. Archival news footage, school surveillance video and courtroom renderings round out this powerful, heartbreaking reminder of the bold, cross-dressing boy with a misplaced crush who was too often deemed the cause of his own murder.”  

Following a successful launch at the 2013 Sundance Film Festival and a well received tour of the film festival circuit it was included in a round up of Oscar worthy films directed by African American women. 

HBO aired the documentary at the start of their 2013 Fall season. As a result Valentine Road was nominated for Emmy’s in the Best Documentary and Best Longform Narrative category at the 35th Annual News & Documentary Emmy Awards.

USA Today praised the film as: “Haunting, heartfelt and even handed” recommended that: “Valentine Road should be required viewing in teaching tolerance on middle school and high school campuses.” 

It was Cunningham’s long-term goal that Valentine Road be used as an educational tool for school administrations. Organizations who are already using the film as a teaching tools include GLSEN, Museum of Tolerance, and the United States Congress.

==References==
 

 
 
 