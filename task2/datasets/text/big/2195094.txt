Kevin of the North
{{Infobox film
| name           = Chilly Dogs
| image          = 
| producer       = Jamie Brown Thomas Hedman Lisa Richardson
| director       = Bob Spiers
| writer         = William Osbourne
| starring       = Skeet Ulrich Natasha Henstridge Leslie Nielsen Rik Mayall Colin Gibson Harvey Summers
| cinematography = David Geddes Curtis Petersen
| editing        = Lara Mazur
| distributor    = Universal Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Alaskan Iditarod Trail Sled Dog Race in which Kevin Manley, whose grandfather has passed on and now must participate in the states Iditarod Trail Sled Dog Race in order to prove hes worthy enough for his grandfathers estate. The film was released to DVD in the United States with the alternate title of Chilly Dogs on February 4, 2003.

==Plot== travel agent, receives the message that his grandpa has passed and left everything to him. However, he must go to Alaska in order to collect his inheritance. He leaves Canoga Park (Los Angeles), quits his job, despite his boss warning him that he will give up and decide to come back begging for his job back, and heads to Anchorage, Alaska.

Upon arrival, Manley finds out that if he proves he is manly by participating in the yearly Iditarod Trail Sled Dog Race he can obtain the land that his grandfather left to him and if he doesnt participate, the land wouldnt be signed over.

While in Alaska, he meets some other participants including: the beautiful Bonnie Livengood (Natasha Henstridge), the stupid English Carter (Rik Mayall), ex-sheriff Ned Parker (Lochlyn Munro) and the local attorney Clive Thornton (Leslie Nielsen) who initially informed him of conditions of the race.

Manley eventually discovers a box of various items that had belonged to his grandfather that included a fur cap and coat, a sword, and a diary which informed Manley that his grandfather had found gold on the land with a relative of a participant he had met earlier, Bonnie Livengood.

While preparing for the event, two other participants, Clive and Carter, attempt to sabotage Manleys chances at winning the race so they can get the land and the gold. Clive promises to pay Carter a thousand dollars if he could prevent Kevin from entering or even finishing the race. Carter succeeds in stealing Kevins team of huskies and burning down the shed containing his sled, tent, and supplies. 

Despite his losses, Kevin uses some of his remaining savings to buy a new sled, a tent, some winter clothing, and food supplies for him and his dogs. He also buys a new team of dogs to replace the ones he lost: Farty, a Nova Scotia Duck Tolling Retriever, Trooper, a German Shepherd, Pierre, a black poodle, Barker, a large dark-brown wiry-haired mongrel, Gumly, a large St. Bernard, Snowflake, a white American bulldog, and a Jack Russell terrier named Riddles.

After the Iditarod race begins and the teams head off into the wilderness, Kevin begins to learn how to survive on the trail and face the perils that seem to pull on him. He and his dogs begin to form a bond of friendship. Along the trail, Kevin rescues Bonnie from Parker, who was in the process of trying to rape her, and gives him the "Canoga Park Coffee Zap", in which he pours hot coffee on his private area. Shortly afterwards, Bonnie and Kevin form a deep relationship and fall in love with each other.

Clive and Carter, who are both anxious to have Kevin lose, try several plans to stop Kevin from finishing the race. Carter cuts half of one of the lines attached to the sled, in which it snaps free as the team rush on down the trail and Kevin has an accident, but is rescued by an Indian tribe and nursed back to health. Another night, Clive and Carter scatter strips of meat around the camp, which attracts a grizzly bear that tears down Kevins tent and scares him and his dogs off. After the duo blow up Kevins grandfathers cabin with dynamite, Kevin soon finds out of Clives and Carters plan and warn them both to stay away from him and his team.

While camping in a snow cave during a blizzard, Bonnie and Kevin discover a map inside Kevins jacket that belonged to his grandfather. The map tells them of a place known as Wolf Mountain, where Kevin believes that his grandfather hid his gold. When the storm clears, they reach Wolf Mountain and recovered the chest full of gold nuggets underneath the snow. 

Unfortunately, Clive and Carter arrive, take the chest of gold, and take Bonnie hostage after burying Kevin in the spot where they uncovered the gold. However, Kevin orders his lead dog, Farty to dig him out and he follows Clive and Bonnie all the way to Nome, which is the finish line of the Iditarod Sled Dog race. He manages to beat Parker and Carter, wins the race, and races onward to cut off Thorntons escape. He rescues Bonnie and Clive draws out a gun and fires at them, but he misses. The bullet glances off several metal objects inside an abandoned garage and finally hits an oil barrel, which blows up the garage with Clive inside it. At the same moment, the gold survives the explosion and falls around Bonnie and Kevin, who are surprised and overjoyed.

Clive, who had recently survived the explosion, and Carter are arrested for their actions against Kevin and Bonnie, and Kevin is rewarded with the prize money of the race, a gold cup, and a bouquet of flowers, which he gives to Bonnie. He asks Bonnie to marry him and she happily agrees. With some of the price money and his grandfathers gold, Bonnie and Kevin get married, build a new cabin, and settle down with their dogs and their first child, a baby girl..

==Cast==
*Skeet Ulrich as Kevin Manley:  The main character sent up to Alaska to collect his grandfathers inheritance by competing in a dog race.
*Natasha Henstridge as Bonnie Livengood: A participant in the Iditarod Trail Sled Dog Race.
*Rik Mayall as Carter: A participant in the Iditarod Trail Sled Dog Race and an accomplice in attempting to stop Manley from winning the sled dog race. 
*Lochlyn Munro as Ned Parker: The ex-sheriff of the town. 
*Leslie Nielsen as Clive Thornton: A local attorney whos keen on the property Manley is in the area for. He attempts to gain the land due to the gold in it through sabotaging Manley in a variety of ways. 
*Jay Brazeau as Mr. Riskind

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 