Charlie Chan and the Curse of the Dragon Queen
{{Infobox film
| name           = Charlie Chan and the Curse of the Dragon Queen
| image          = Charlie-chan-and-the-curse-of-the-dragon-queen-movie-poster-1981.jpg
| image_size     = 
| caption        = 
| director       = Clive Donner
| writer         = 
| narrator       = 
| starring       = Peter Ustinov
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = February 1981
| runtime        = 97 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Charlie Chan and the Curse of the Dragon Queen is a 1981 film directed by Clive Donner that stars Peter Ustinov, Angie Dickinson and Lee Grant.   

==Plot==
Retired detective Charlie Chan is asked for his help by the San Francisco police to solve a new series of murders. But this time his usual sidekick, Number One Son, has been replaced by his own son, Lee Chan, Jr.

The prime suspect in the killings is a shady lady known as the Dragon Queen, but soon Chans suspicions fall elsewhere. Among those at risk are Lees maternal grandmother, Mrs. Lupowitz, and even though Charlie Chans son is, as is usually the case, rarely accurate in reading clues, he has the full love and support of Cordelia, his beautiful fiancee. 

==Cast==
*Peter Ustinov as Charlie Chan
*Lee Grant as Mrs. Lupowitz
*Angie Dickinson as Dragon Queen Richard Hatch as Lee Chan, Jr.
*Brian Keith as Police Chief Baxter
*Roddy McDowall as Gillespie Rachel Roberts as Mrs. Dangers
*Michelle Pfeiffer as Cordelia Farenington Paul Ryan as Masten
*Johnny Sekka as Stefan

==References==
 

==External links==
* 

 
 

 
 
 