The House of the Spirits (film)
{{Infobox film
| name        = The House of the Spirits
| image       = The_House_of_Spirits_Poster.jpg
| caption     = Promotional poster (US)
| director    = Bille August
| producer    = Bernd Eichinger
| writer      = Isabel Allende (The House of the Spirits|novel) Bille August (screenplay)
| starring    = Meryl Streep Glenn Close Jeremy Irons Winona Ryder Antonio Banderas Vanessa Redgrave
| music       = Hans Zimmer
| cinematography = Jörgen Persson
| editing     = Janus Billeskov Jansen
| studio      = Constantin Film
| distributor = Miramax Films (US)
| released    = October 17, 1993 (Germany) April 1, 1994 (US)
| runtime     = 140 minutes
| country     = Germany Denmark Portugal
| language    = English
| budget      = $40 million   
| gross = $6,265,311 (US) 
}} Portuguese period drama directed by Bille August and starring Jeremy Irons, Meryl Streep, Glenn Close, Winona Ryder, Antonio Banderas and Vanessa Redgrave. The supporting cast includes María Conchita Alonso, Armin Mueller-Stahl, and Jan Niklas.
 La Casa military dictatorship Golden Screen (Germany), Havana Film Festival, and Robert Festival (Denmark), the German Phono Academy and the Guild of German Art House Cinemas).   

==Plot==
 
;Prologue
A young woman, Blanca Trueba (Winona Ryder), arrives at a house with an old man and the  young woman starts remembering her life.

;Clara and Esteban
Blancas mother, Clara del Valle (Meryl Streep)was a child with psychic power  when Esteban Trueba (Jeremy Irons) came to propose to Claras sister, Rosa del Valle. Esteban left his fiancée with her family to earn money for their wedding. One day, CLara has a vision and tells her sister Rosa that there will be a death in the family. The next day, Rosa dies after drinking poison intended for her father, Senator Severo. Clara blames herself for her sisters death, and after watching her sisters autopsy, decides never to speak again.

Esteban was heartbroken . At home, his sister, Férula (Glenn Close)lives and take care of their sick mother. Esteban used the money he earned from mining and bought a hacienda, Tres Marías. He finds many natives living on his land and tells them to work for him for food and shelter. For the next twenty years, Esteban makes Tres Marías an example of a successful Hacienda. He spends some nights with Tránsito, a prostitute (María Conchita Alonso), to whom he loans money so she can start a new career in the capital. One day he rapes a peasant girl, Pancha García (Sarita Choudhury)and she gave birth to a son.

Twenty years later, Esteban receives a letter that his mother has died. After her funeral, Esteban decides to ask for Claras hand, despite Férulas protests that Clara is too sickly and will not take care of him properly. When he shows up at the Del Valle familys house, Clara asks him right away if he has come to ask her to marry him, thus speaking again for the first time in twenty years.

Férula meets Clara at a coffee shop to talk  about her own future, and Clara, sensing Férulas worries, promises her that she can live with her and Esteban in Tres Marías after the wedding and the two will be like sisters. 

Clara gives birth to a girl as she predicted, and names her Blanca. One day,the girl whom Esteben raped, Pancha García appears at the family house with  Estebans illegitimate teenage son, Esteban García and asked for money. Esteban Trueba gave them some money and asked them never to come back. 

Clara holds classes to the peasant children and Blanca. Pedro Tercero, the young son of  peasant Segundo at Tres Marias befriends Blanca and the two become playmates. Esteban Trueba does not like  his daughter to play with a peasant boy and  send Blanca to a boarding school.

;Blanca and Pedro Tercero
After graduating from school, Blanca returns home to Tres Marías and will meet with Pedro Tercero (Antonio Banderas)by the lagoon every night. One night while Esteban attends a political party, there is an earthquake, he worries about Clara and Blanca and come home  to Tres Marías and finds that Férula has climbed into bed with Clara. He is so angry that he throws Férula out of the house telling her never to come back.

One day Esteban brings the French Count Jean de Satigny (Jan Niklas) to his home intending to arrange a marriage between him and Blanca. Clara senses that the French "nobleman" is a fraud while reading cards but Esteban dismisses her as folly. While Satigny is still visiting, Esteban catches Pedro preaching revolutionary ideas to the peasant people, and punishes him with a fierce whipping and banishing him from Tres Marías. That night at dinner, Férula suddenly appears in the house, kisses Clara on the forehead and leaves again. Clara tells the rest of the family that Férula is dead. Clara and Esteban drive into town to Férulas modest house, where they find her dead on the bed.

Pedro has returned to Tres Marías and talks to the peasants about their rights and nearly gets shot by Esteban. That night, the Count Jean de Satigny, who is visiting again, watches Blanca and Pedro meeting secretly at the lagoon. He reveals Blancas lover to her father who immediately drags Blanca back to the house. When Clara tries to persuade him against violence, Esteban hits his wife and she falls. When Clara rises, she coldly tells him that she will never speak to him again. Clara moves with Blanca to her parents house in the capital.

Blanca becomes pregnant with Pedros child and gave birth to a girl.

;Revolution
Esteban is busy with his political career, but as an old man he is lonely and finds comfort in the arms of Tránsito, who has become a successful business woman .

During the national election, Esteban believes his Conservative Party will win as usual, but the Peoples Front ends up winning control of the government.  Blanca goes out on the street to celebrate and to meet Pedro.Clara stayed home to decorate the house with Alba and died .

Meanwhile, a conspiracy between some Conservative Party members and the military leads to a coup détat, and the military takes control of the country. At first Esteban believes it is good for the country and that the military will hand power back to the Conservative Party, but he soon learns that the military have other plans. Under the control of the military, people associated with the Peoples Party are captured and even killed. Blanca is highly involved and eventually the police come to arrest her for being with Pedro Tercero. Blanca  reveals to her father that Pedro has been hiding in their houses cellar and begs him to help Pedro get out of the country.

In the coming days, Blanca is tortured and abused by her half-brother Esteban García, now an important member of the military.Esteban honours his daughters wishes and helps Pedro Tercero find exile in Canada. Esteban then  turns to Tránsito, now an influential Madam with lots of connections to help free Blanca. One morning a beaten and dirty Blanca arrives at her home and Esteban told her Pedro is waiting for her in Canada. 

;Epilogue
Blanca and Esteban return to Tres Marías with Alba. Esteban is finally visited by Claras spirit who has come to help the old man to the next world. Blanca sits outside and ponders her life,looking forward to a life with Pedro and her daughter Alba.

==Cast==
 
 

* Alexandre de Sousa as Estebans Driver
* António Assunção as Man at Cattlemarket
* Antonio Banderas as Pedro Tercero García
* Armin Mueller-Stahl as Severo del Valle Carlos César as Ambassadors Driver Carlos Rodrigues as Postman
* Denys Hawthorne as Politician Edith Clement as Midwife
* Fran Fullenwider as Emer Trueba
* Franco Diogent as Man at the Party Frank Baker as Intelligence Officer
* Frank Lenart as Interviewer
* Glenn Close as Férula Trueba
* Grace Gummer as Young Clara
* Hannah Taylor-Gordon as Blanca Trueba, as a Child
* Hans Wyprächtiger as Dr. Cuevas    

* Hellmuth O. Stuven as TV General
* Jaime Tirelli as Oliver
* Jan Niklas as Count Jean de Satigny Jean Michel as Musician
* Jeremy Irons as Esteban Trueba
* João Cabral (actor)|João Cabral as Soldier
* Joaquín Martínez as Pedro Segundo García
* Joost Siedhoff as Father Antonio
 

* José Mora Ramos as Soldier Josh Maguire as Pedro Tercero García, as a Child
* Julie Balloo as Young Lady
* Lone Lindorff as Maid
* Luís Pinhão as Woodcarver
* Manuela Santos as Saleswoman
* María Conchita Alonso as Tránsito Soto
* Martin Umbach as TV Reporter
* Meryl Streep as Clara del Valle Trueba
* Miguel Guilherme as Soldier
* Míriam Colón as Nana
* Oscar A. Colon as Sheriff
* Pedro Efe as Interrogation Officer
* Rogério Claro as Man in Club
* Sarita Choudhury as Pancha Garcia
* Sasha Hanau as Alba García de Satigny Trueba Steve Mason as Maestro
* Teri Polo as Rosa del Valle
* Vanessa Redgrave as Nívea del Valle Victor Rocha as Soldier
* Vincent Gallo as Esteban García
* Vivian Reis as Indian Girl
* Winona Ryder as Blanca Trueba 

==Production==
Principal photography took place in Denmark, but some scenes were filmed in Lisbon and Alentejo, Portugal.

==Soundtrack==
The music was composed by award winner  ", a Spanish–Cuban–Mexican tune sung by popular Chilean singer Rosita Serrano; and "La Cumparsita", a classic Uruguayan tango tune performed by German bandleader Adalbert Lutter and his orchestra.   

==Reception==
The film grossed $6 million in the United States and more than $55 million in Europe.  It was also not well received by critics (two oft-cited reasons were its diffusely episodic structure and a cast of mostly American & European actors in Latin American roles) and currently holds a rotten 29% rating on Rotten Tomatoes based on 34 reviews. Despite this, however, it received several awards:

===Awards and nominations===
* 1994 Bavarian Film Award for Best Costume Design (Barbara Baum) Won
* 1994 Bavarian Film Award for Best Production (Bernd Eichinger) Won
* 1994 German Film Award in Gold for Outstanding Individual Achievement: Over All Concept (Bernd Eichinger) Won
* 1994 German Phono Academy Echo Award for Film Music of the Year (Hans Zimmer) Won
* 1994 Guild of German Art House Cinemas Award (Gold) for German Film (Bille August) Won
* 1994 Havana Film Festival Coral Award for Best Work of a Non-Latin American Director on a Latin America Subject (Bille August) Won
* 1994 Robert Award for Best Editing (Janus Billeskov Jansen) Won
* 1994 Robert Award for Best Film (Bille August) Won
* 1994 Robert Award for Best Screenplay (Bille August) Won
* 1994 Robert Award for Best Sound (Niels Arild) Won   

==References==
 

==External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 