The White Viking
{{Infobox Film
| name           = Hvíti Víkingurinn
| image          =The_White_Viking_1991.jpg
| caption        = 
| director       = Hrafn Gunnlaugsson
| producer       = Christer Abrahamsen
| writer         = Hrafn Gunnlaugsson
| narrator       = 
| starring       = Reine Brynolfsson Tinna Gunnlaugsdóttir Egill Ólafsson Sune Mangs Kristbjörg Kjeld
| music          = 
| cinematography = 
| editing        = Hrafn Gunnlaugsson
| distributor    = 
| released       =  
| runtime        = 123 minutes
| country        = Iceland
| language       = Icelandic language|Icelandic, German
| budget         = ISK 200,000,000
}}

The White Viking (alternative title Embla,  ,  ) is a 1991 film set in Norway and Iceland during the reign of Olaf I of Norway.

Embla is the directors cut of the White Viking and released on DVD in 2007. It was premiered at the Reykjavik International Film Festival on October 6, 2007. It is the directors cut of the original film as the director imagined it. Gunnlaugsson chose to call the film Embla so that it is not confused with the White Viking, compiled by the producers of the film in 1992. Embla is the third film of the legendary Raven Trilogy (also known as the Viking Trilogy) that consists of three Viking films:

*1) When the Raven Flies (1984) - (original Icelandic title: Hrafninn flýgur) - usually known as simply The Raven or Revenge of the Barbarians.
*2) In The Shadow of the Raven (1987) - (original Icelandic title: Í skugga hrafnsins).
*3) Embla (2007) - (original Icelandic title: Hvíti víkingurinn) - the directors cut of The White Viking.

Embla, as played by Maria Bonnevie, was her first screen role when she was only fifteen years of age.

==Plot== Christian who seeks to root out paganism in Norway.  Embla is captured, and as punishment, Askur is sent to Iceland with the task of converting the people there to Christianity in order to free Embla.  The choice of names for the young married couple comes from Nordic mythology, in which the first two humans are named Ask and Embla.

This film was directed and co-written by Hrafn Gunnlaugsson.

==Cast==
* Gotti Sigurdarson as "Askur"
* Maria Bonnevie as "Embla"
* Tomas Norström as "Bishop Þangbrandr"
* Egill Ólafsson as "Olaf I of Norway"

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 

 