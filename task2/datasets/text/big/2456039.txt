Un secreto de Esperanza
 
{{Infobox film
| name           = Un secreto de Esperanza
| image          = 
| caption        = 
| director       = Leopoldo Laborde
| producer       = Moises Jafif
| writer         = Leopoldo Laborde Imanol   María Karunna Hill
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}} Mexican film written and directed by Leopoldo Laborde and starring Katy Jurado, Imanol Landeta|Imanol, Jaime Aymerich and Ana de la Reguera. It is an example of a cinematic homage for the last screen appearance of Katy Jurado. This film captures the magical realism of Mexico as well as telling a genuinely touching tale about the unlikeliest of friendships.

It was won several awards in film festivals around the world.

==See also==
*A Rumor of Angels

==External links==
*  

 
 
 
 
 
 
 

 
 