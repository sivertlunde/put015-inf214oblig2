She Hate Me
{{Infobox film
| name           = She Hate Me
| image          = She Hate Me film poster.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Spike Lee
| producer       = Spike Lee Preston Holmes Fernando Sulichin
| screenplay     = Michael Genet Spike Lee
| story          = Michael Genet
| starring       = Anthony Mackie Kerry Washington Ellen Barkin Monica Bellucci Jim Brown Jamel Debbouze Brian Dennehy Woody Harrelson Bai Ling Q-Tip (musician)|Q-Tip Dania Ramirez John Turturro
| music          = Terence Blanchard 
| cinematography = Matthew Libatique 
| editing        = Barry Alexander Brown
| studio         = 40 Acres and a Mule Filmworks
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 138 minutes
| country        = United States
| language       = English
| budget         = $8 million 
| gross          = $1,522,377
}}
 independent comedy-drama film directed by Spike Lee and starring Anthony Mackie, Kerry Washington, Ellen Barkin, Monica Bellucci, Brian Dennehy, Woody Harrelson, Bai Ling and John Turturro.

The film garnered controversy, and, as with many of Lees films, touches on comedy film|comedy, drama film|drama, and politics.  Unlike many prior works, Spike Lee does not have an acting credit in this film.

The film was shot mostly on location in New York City, including each of the citys five boroughs.  It was nominated for various awards (see below), but did not win.  She Hate Me was released in July 2004 and grossed almost half a million dollars at the box office in limited release,  with overall revenues of around $1.5 million. The shooting budget was estimated at $8 million.

==Plot==
John Henry "Jack" Armstrong (Anthony Mackie) is a financially successful and upwardly mobile executive at a biotechnology firm who, following the suicide of a colleague, Dr. Herman Schiller, is falsely accused of securities fraud by his superior, Leland Powell (Woody Harrelson). Armstrongs assets are frozen, and he finds himself unable to maintain his quality of life.

In order to make ends meet, he becomes a sperm donor, initially by acquiescing to the desires of Fatima Goodrich (Kerry Washington), his ex-fiancée who came out as a lesbian and now wants a child. Although there is still unresolved bitterness and tension between them over Armstrong and Goodrichs prior relationship, she and her girlfriend, Alex Guerrero (Dania Ramirez), offer him a substantial sum of money to impregnate them both.  This leads to Goodrich goading Armstrong into establishing a business in which groups of lesbians come over to his house and pay him $10,000 each to have sex with them in order to become pregnant.

One of the women who Armstrong impregnates is the daughter of a mafia boss, Don Angelo Bonasera (played by John Turturro). Armstrongs employers learn of his impregnation business, and they use it in their campaign to sully his image in order to deflect attention from their own criminal business activities Conflict is also depicted in the turbulent relationship between Armstrongs mother and his dependent diabetic father (Jim Brown).

At the films climax, Armstrongs situation is portrayed as a cause célèbre, with protests being held in support of or against him, and the news media interviewing people on the street with respect to his sexual activities. Armstrong is called before a committee of the United States Senate investigating his alleged securities fraud, where both his services to lesbians and his relationship to the "Bonasera crime family" are raised.
 Frank Wills, the security guard who discovered the break-in that led to the Watergate scandal, which brought down President Richard M. Nixon|Nixon. He eventually wins the case and is seen with nineteen of the children he helped his lesbian acquaintances make at the end.
 polyamorous relationship, and Armstrong apparently maintains a friendship with all of the eighteen women who became pregnant by him.

==Cast==
 
*Anthony Mackie as John Henry "Jack" Armstrong
*Kerry Washington as Fatima Goodrich
*Jim Brown as Geronimo Armstrong  
*John Turturro as Don Angelo Bonasera 
*Q-Tip (musician)|Q-Tip as Vada Huff                   
*Woody Harrelson as Leland Powell                                                       
*Dania Ramirez as Alex Guerro                                        
*Monica Bellucci as Simona Bonasera
*Lonette McKee as Lottie Armstrong
*Michael Genet as Jamal Armstrong
*Ossie Davis as Judge Buchanan
*Brian Dennehy as Chairman Billy Church
*Ellen Barkin as Margo Chadwick   
*David Bennent as Dr. Herman Schiller 
*Joie Lee as Gloria Reid                                Frank Wills
*Michole Briana White as Nadiyah
*Paula Jai Parker as Evelyn
*Bai Ling as Oni
*Sarita Choudhury as Song
*Jamel Debbouze as Doak Isiah Whitlock	as Agent Amos Flood Lemon as Eugenio Martinez
*Kim Director as Grace
*Rick Aiello as Rocco Bonasera
 

==Critical reaction==
She Hate Me received a sharply negative reaction from film critics, with a score of just 19% on the site Rotten Tomatoes.  "Its not only unfathomable and borderline offensive, its never-ending, leaving its bewildered and battered audience in the dark for well over two hours," wrote Detroit Free Press critic Terry Lawson. Roger Ebert, who gave the film three stars out of four, said the film "will get some terrible reviews. Scorched earth reviews. Its logic, style, presumption and sexual politics will be ridiculed. The Tomatometer will be down around 20"—a spot-on prediction, as it turned out.  "Many of the things you read in those reviews may be true from a conventional point of view. Most of the critics will be on safe ground. I will seem to be wrong. Seeming to be wrong about this movie is one of the most interesting things Ive done recently. Ive learned from it."

The film grossed a total of $1,522,377 in the box office. 

==Production== football player legend of John Henry, who worked hard against his opponent (the steel driving machine) and won, but soon afterward died of heart failure.

Actor Isiah Whitlock Jr. appears in the film as Agent Amos Flood, with the same last name of a role he played in the Spike Lee film 25th Hour, though there are no obvious parallels between the two films. This was the last film featuring Ossie Davis, as he died in February 2005. German actor David Bennent re-appeared after a long film career absence, and portrays the German scientist who jumps to his end. Raul Midon sang the theme song for the movie, "Adam N Eve N Eve". In the film, Armstrongs brother disapproves of his impregnating lesbians and also uses the phrase "Adam N Eve N Eve.

Some real human births are seen up-close in the film. A live human birth is also portrayed in another Spike Lee film, Mo Better Blues.

==Award nominations==
*BET Comedy Awards
**Outstanding Directing for a Theatrical Film (Spike Lee)
**Outstanding Writing for a Theatrical Film (Michael Genet and Spike Lee)

*Black Reel Awards
**Best Breakthrough Performance (Anthony Mackie)
**Best Director (Spike Lee)
**Best Original Score (Terence Blanchard)
**Best Screenplay, Original or Adapted (Michael Genet and Spike Lee)

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 