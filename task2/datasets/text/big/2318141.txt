Mystery Street
{{Infobox film
| name           = Mystery Street
| image          = Mystery Street.JPG
| alt            =
| caption        = Theatrical release poster
| director       = John Sturges 
| producer       = Frank E. Taylor
| screenplay     = Sydney Boehm Richard Brooks
| story          = Leonard Spigelgass
| starring       = Ricardo Montalban Sally Forrest Bruce Bennett Elsa Lanchester
| music          = Rudolph G. Kopp	
| cinematography = John Alton	
| editing        = Ferris Webster
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $730,000  .  Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008 p55. 
| gross          = $775,000 
}}
Mystery Street is a 1950 black-and-white film noir directed by John Sturges with cinematography by cinematographer John Alton. The film features Ricardo Montalban, Bruce Bennett, and Elsa Lanchester. 
 Best Story for the 23rd Academy Awards.

==Plot== Barnstable cop Peter Morales (Montalban) teams up with Boston police and uses forensics with the help of Dr. McAdoo, a Harvard doctor (Bennett), to figure out who the woman is.

The cop then tries to figure out how she died and, later, who killed her. Vivians nosy landlady (Lanchester) attempts to blackmail the father of Vivians child, whom she had been calling from her boarding house, going so far as to visit the wealthy married man and steal his gun. Morales tracks down the stolen car from police records and questions Henry Shanway, the drunk man Vivian was with the night she disappeared. Eventually Morales finds Shanways car and hes identified in a police lineup. The innocent man is arrested and charged with the murder. Morales and Dr. McAdoo find the bullet still stuck in the car. Morales then finds out that the landlady has the gun, but not before she tries to blackmail the owner and is knocked over the head, and later dies. Morales chases after but loses the killer. Morales
finds a hidden baggage check in the landladys room, which sends Morales racing to catch the killer before the murder weapon can be disposed of.

==Cast==
* Ricardo Montalban as Lieutenant Peter Morales 
* Sally Forrest as Grace Shanway 
* Bruce Bennett as Dr. McAdoo, of Harvard Medical School 
* Elsa Lanchester as Mrs. Smerrling, the landlady 
* Marshall Thompson as Henry Shanway, Graces husband
* Jan Sterling as Vivian Heldon, bar-girl and murder victim
* Edmon Ryan as James Joshua Harkley
* Betsy Blair as Jackie Elcott
* Ralph Dumke as A Tattooist
* Willard Waterman as A Mortician
* Walter Burke as An Ornithologist

==Reception==
According to MGM records the film earned $429,000 domestically and $346,000 foreign, resulting in a loss of $284,000. 

===Critical response===
Time (magazine)|Time magazine called it a "low-budget melodrama without box-office stars or advance ballyhoo   does not pretend to do much more than tell a straightaway, logical story of scientific crime detection" but notes that "within such modest limits, Director John Sturges and Scripters Sydney Boehm and Richard Brooks have treated the picture with such taste and craftsmanship that it is just about perfect."   The New York Times called it  "an adventure which, despite a low budget, is not low in taste or its attention to technical detail, backgrounds and plausibility" with a performance by Montalban that is "natural and unassuming." 

The staff at Aint It Cool News characterized the film as a precursor to CSI (franchise)|CSI-style police procedurals, with a Harvard Medical School professor providing the forensic science. 

===Accolades===
Nominated
* Academy Awards: Best Writing, Motion Picture Story, Leonard Spigelgass; 1951.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 