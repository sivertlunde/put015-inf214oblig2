Ride with the Devil (film)
{{Infobox film
| name           = Ride with the Devil
| image          = Rwtdposter2.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Ang Lee
| producer       = James Schamus Robert F. Colesberry Ted Hope
| writer         = James Schamus
| based on       =   Jewel Jeffrey Jeffrey Wright Simon Baker Jonathan Rhys Meyers Jim Caviezel Tom Guiry Jonathan Brandis
| music          = Mychael Danna
| cinematography = Frederick Elmes
| editing        = Tim Squyres Universal Pictures Good Machine
| distributor    = USA Films
| released       =  
| runtime        = 138 minutes  
| country        = United States
| language       = English
| budget         = $38 million   
| gross          = $635,096 
}}
Ride with the Devil is a 1999 American Civil War film directed by Ang Lee. The storyline was conceived from a screenplay written by James Schamus, based on a book entitled Woe to Live On, by author Daniel Woodrell.
 Union soldiers.
 Jeffrey Wright, Jonathan Brandis, Jim Caviezel and musician Jewel (singer)|Jewel.

The film was a  . 
 Jewel also contributed a musical track from her second studio album Spirit (Jewel album)|Spirit.

Principal photography began on March 25, 1998. Ride with the Devil premiered in theaters nationwide in the United States on November 26, 1999 grossing $635,096. Taking into account its $38 million budget costs, the film was considered a major box office bomb. With its initial foray into the home video market, the widescreen DVD edition featuring the theatrical trailer, scene selections, and production notes, was released in the United States on July 18, 2000.

==Plot==
Jake Roedel and Jack Bull Chiles are friends in Missouri when the American Civil War breaks out. During the mayhem, Chiless father is murdered by Kansas Union Jayhawkers. The two men join the First Missouri Irregulars, also known as the Bushwhackers; informal units loyal to Missouri in 1861. They later meet George Clyde and former slave Daniel Holt, whose freedom Clyde has previously granted. 

The Bushwhackers battle Jayhawkers using guerrilla warfare tactics while trying to evade capture. The men manage to hide out in a coarsely-built shelter on the property of a pro-Confederacy family, the Evanses. A young widow in the household, Sue Lee Shelley, becomes romantically involved with Chiles. When Chiles dies of gangrenous wounds received during a skirmish, Roedel escorts Shelley to a refuge dwelling where another pro-Confederate kindred, the Browns family, reside. 

Following the collapse and destruction of a makeshift prison holding the female relatives of guerrillas, a complementary clan of Bushwhackers led by William Quantrill plot a revenge attack against the Union and raid Lawrence, Kansas. 

In the midst of the offense, a quarrel arises between Roedel and fellow Bushwhacker Pitt Mackeson. Roedel, a German American, was born in Germany but raised by his immigrant father in Missouri. He suffers from sporadic anti-German suspicion from other Southerners, because the German population in the state is largely sympathetic to the Union. In an episode of hostility, Mackeson purposely shoots Roedel in the leg shortly after the raid on Lawrence, while retreating from a counterattack by Union forces. The perceived prejudice contributes to Roedels sympathy to the plight faced by Holt, a former slave coping with racism. 

Meanwhile, Shelley gives birth to Chiless daughter. Holt and Roedel, both wounded, recover at the same residence that took in Shelley occupied by the Browns folk. The Browns, who mistakenly suppose Roedel is the childs father, pressure Roedel to marry her, which he is reluctant to do. However, after spending time with Shelley and the child, Roedel begins to have feelings for both of them. 

Meanwhile, Anderson and many other Bushwhackers have been killed, taken prisoner or otherwise rendered inactive. Pitt Mackeson has gathered some of the survivors into a gang which no longer fights the Yankees, but instead robs, murders and plunders Unionists and Southerners alike. Word comes from one of Roedels compatriots that Mackeson and his gang are headed South and plan on visiting Roedel soon.

One day Mr. Brown takes Holt to town and returns with a reverend and Roedel, after realizing he does love Shelley and she him, marries her in an abrupt wedding. Roedels feelings toward Shelley are further deepened by a tender wedding night together.

Proclaiming himself finished with war, Roedel gives up being a Bushwhacker and takes his new family to California. 

On the way, they meet Mackesen and the last of his men, Turner, ragged, injured and on the run. They report Black John and Quantrill are both dead and agree with Roedel the war is lost. Mackesen tells them of his plan to ride into Newport despite the fact the town is full of Federal soldiers, and his strange manner causes Roedel and Holt to hold guns on Pitt and Turner, but the two ride off without violence.

Holt rides with Roedel and his family toward California, until their roads part, and then Daniel tells Jake farewell, while Shelley and the baby sleep. Holt leaves for Texas, a free man, to find his long lost mother. 

==Cast==
 
* Tobey Maguire as Jake Roedel
* Skeet Ulrich as Jack Bull Chiles Jewel as Sue Lee Shelley Jeffrey Wright as Daniel Holt
* Simon Baker as George Clyde
* Jonathan Rhys Meyers as Pitt Mackeson
* Jim Caviezel as Black John Ambrose
* Tom Guiry as Riley Crawford
* Jonathan Brandis as Cave Wyatt
* Mark Ruffalo as Alf Bowden
* Tom Wilkinson as Orton Brown
* Margo Martindale as Wilma Brown
* John Ales as William Quantrill
* Celia Weston as Mrs. Clark

==Production==

===Themes and analysis===
  Confederate guerrillas William Clarke Quantrill. Associate producer Anne Carey had read and taken a liking to Woodrells novel and delivered it to director Ang Lee. Lee remarked, "Its dramatic," and described how it related to "young people coming of age in the worst possible time in American history. I liked the theme of self-emancipation."  Quantrill gained infamy during the American Civil War for his atrocities against citizens and Union soldiers. He served the Confederacy and perhaps hoped to secure recognition and a high military rank from its leaders. But Quantrills activities indicated that he fought for plunder and personal revenge.    In 1863, Quantrill undertook the great raid that made his name famous in the region. After bringing together over 400 Missouri riders for the Confederate cause in a carefully planned rendezvous on August 21 in Lawrence, Kansas, Quantrill led his men into the town, looting, burning, and executing between 150 and 200 adult males in reprisal for murderous raids conducted by Kansas Jayhawkers and Redleggers in Missouri.    In the North, this event became known as the Lawrence Massacre; and was vilified as one of the worst events of the war.   
 partisans staged James Henry Lane, had his men burn and loot both Loyal and Confederate sympathizers indiscriminately.  Quantrill was later shot in an ambush by Union militia on May 10, 1865 during a raid in Kentucky, and died in a Louisville prison on June 6. However, he quickly became an admired figure of the Civil War from a southern perspective. He was a hero to his supporters in Missouri, and his fame actually assisted several other outlaw figures of the old west.   As such, in August 1864, a clash occurred by Fort Gibson between Federal troops and remnants of Quantrills Raiders. During this skirmish, American outlaw Jesse James was deliberately shot in the right lung while attempting to surrender to Union militia.   Between 1888 and 1929, members of Quantrills Raiders gathered to recount their war efforts. Today, there is the "William Clarke Quantrill Society" which was established to dedicate the study of Quantrill, his followers, and their involvement in the Civil War era border wars.  Director Lee summarized that Ride with the Devil was "not simply a war movie. Its more about the love and friendships that take place during a war. The film is both big and small, both epic and domestic." 

===Casting and set design=== boot camp to prepare them for their roles. During shooting, Maguire hesitated under the grueling heat and 16-hour workdays, but pressed on to complete the filming. The actors first trained shooting blank loads, and then live ammunition for action conflict scenes.  More than 250 Civil War black-powder pistols were used during the production phase.  Over 140 extras played Lawrence residents, and more than 200 Civil War re-enactors were brought in to relay their style of living to the filming sequences. 

Principal photography began on March 25, 1998. Filming took place primarily on location in Sibley, Missouri, Kansas City, Kansas and Kansas City, Missouri.  Pattonsburg, Missouri also stood in as a primary filming set locale.  The set design production team removed telephone poles and utilized truckloads of dirt to cover existing asphalt and concrete.  Production designer Mark Friedberg created numerous indoor and outdoor sets of the time period to ensure and maintain historical accuracy. 

===Music and soundtrack=== Jewel contributed vocals to the score with her song Whats Simple Is True, from her 1998 album Spirit (Jewel album)|Spirit. 

{{Infobox album
| Name = Ride with the Devil: Music from and inspired by the Motion Picture
| Type = Film score
| Artist = Mychael Danna
| Cover = RWTDSoundtrack.jpg
| Released = 11/23/1999
| Length = 53:21
| Label = Atlantic Records
| Reviews =
}}

{{Track listing
| collapsed       = no
| headline        = Ride with the Devil: Music from and inspired by the Motion Picture
| total_length    = 53:21
| title1          = Opening Credits
| length1         = 3:01
| title2          = Miss McLeods Reel
| length2         = 1:41
| title3          = Jayhawkers and Bushwhackers
| length3         = 3:20
| title4          = Clark Farm Shootout
| length4         = 3:05
| title5          = Fireside Letter
| length5         = 1:50
| title6          = Sally in the Garden
| length6         = 1:21
| title7          = Settling in for Winter
| length7         = 0:49
| title8          = Ride to the Evans/Hilltop Letter
| length8         = 2:10
| title9          = Sue Lee/Dinner at the Evans
| length9         = 1:28
| title10         = The Ambush
| length10        = 2:52
| title11         = George Clyde Clears Out
| length11        = 1:44
| title12         = Jack Bulls Death
| length12        = 4:45
| title13         = Old King Crow
| length13        = 2:06
| title14         = Quantrills Arrival/Ride to Lawrence
| length14        = 2:37
| title15         = Sacking Lawrence
| length15        = 4:05
| title16         = Dont Think You Are a Good Man
| length16        = 2:11
| title17         = Battle and Betrayal
| length17        = 3:13
| title18         = Freedom
| length18        = 2:42
| title19         = A Chicken at the End of It
| length19        = 1:36
| title20         = Finale
| length20        = 3:09
| title21         = Whats Simple Is True
| length21        = 3:36
}}

==Marketing==

===Novel===
The basis for the film, Daniel Woodrells novel Woe to Live On (originally published in 1987) was released as a movie tie-in edition, re-titled Ride With the Devil, by Pocket Books on November 1, 1999. The book dramatizes the events of the American Civil War during the 1860s, as depicted in the film. It expands on the inner-fighting between rebel Bushwhackers and Union Jayhawkers, with civilians caught in the crossfire.  The story relates a coming of age experience for Roedel as he emotionally comprehends the losses of his best friend, father and comrades. On a separate front, Roedel expresses love for his best friends widow, and learns about tolerance from his contact with a reserved black Irregular military|Irregular.

==Release==

===Theatrical run===
Ride with the Devil made an initial screening on November 24, 1999 in  . Retrieved 2011-12-01.  For most of its limited release, the film fluctuated between 11 and 60 theater screening counts. At its most competitive showing, the filmed ranked in 37th place for the December 17–19 weekend in 1999. 

===Home media===
Following its cinematic release in theaters, the   on April 27, 2010. Special features include; Two audio commentaries one featuring Lee and producer-screenwriter James Schamus and one featuring Elmes, sound designer Drew Kunin, and production designer Mark Friedberg; a new video interview with star Jeffrey Wright, and a booklet featuring essays by critic Godfrey Cheshire and Edward E. Leslie, author of The Devil Knows How to Ride: The True Story of William Clarke Quantrill and his Confederate Raiders. 

A restored widescreen hi-definition   is available as well. 

==Reception==

===Critical response=== weighted average out of 100 to critics reviews, Ride with the Devil received a score of 69 based on 29 reviews.  The film however, failed to receive any honor nominations for its dramatics or visual aspects.

{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"From a technical perspective, Ride with the Devil is nearly perfect. The attention to detail invested by Lee and his crew shows. From costumes to props, everything has the unmistakable hallmark of authenticity. The only Civil War drama able to boast an equal level of historical accuracy is Gettysburg (1993 film)|Gettysburg."
|-
|style="text-align: left;"|—James Berardinelli, writing in ReelViews 
|}
Peter Stack, writing in the San Francisco Chronicle, said in outward positive sentiment, "Lees approach mixes an unsettling grittiness with an appealing, often luminous elegance (thanks to Frederick Elmes cinematography) in picturing a patch of America at war with itself."  Left impressed, Stephen Hunter in The Washington Post, wrote that the film was "terrific" and that it contained the "most terrifying kind of close-in gunplay, with big, pulsing holes blown into human beings for a variety of reasons ranging from the political to the nonsensical."  In a mixed to positive review, Stephen Holden of The New York Times, described the films production aspects as being of "meditative quality and its attention to detail and the rough-hewn textures of 19th-century life are also what keep the story at a distance and make "Ride with the Devil" dramatically skimpy, even though the movie stirs together themes of love, sex, death and war."  Wesley Morris of The San Francisco Examiner, commented that Ride with the Devil was "downright hot-blooded in the nameless violence going on west of marquee Civil War battles. Never has this war been filmed with such ragged glory. The boys grasping their rifles look like trigger-happy rock stars of the prairies, so much so that they threaten to transform the film into a great hair movie."  In a slightly upbeat conviction, Andrew OHehir of Salon.com asserted that "for all its clumsy dialogue and loose plotting, this is historical filmmaking of a high order, both visually and thematically ambitious."  Todd McCarthy of Variety (magazine)|Variety, added to the exuberant tone by declaring, "Impressing once again with the diversity of his choices of subject matter and milieu, director Ang Lee has made a brutal but sensitively observed film about the fringes of the Civil War". 

The film however, was not without its detractors. Writing for the  . Retrieved 2011-12-01.  In a primarily negative review, Lisa Schwarzbaum writing for Entertainment Weekly, called the film "an oddly unengaging one, not because of any weak performances (even crooning poetess Jewel acquits herself pleasantly in her film debut), but because the waxy yellow buildup of earnest tastefulness (the curse of the Burns school of history) seals off every character from our access."  Describing a favorable opinion, Russell Smith of The Austin Chronicle professed the film as exhibiting "unostentatious originality, psychological insight, and stark beauty". While following up, he stressed "Theres an odd blend of stylization and extreme realism to this film. The dialogue is stilted, full of archaic $20-words and dime-novel flamboyance — all the more jarring when delivered by these teenaged bumpkin characters." 

{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"Its a film that would inspire useful discussion in a history class, but for ordinary moviegoers, its slow and forbidding."
|-
| style="text-align: left;" |—Roger Ebert, writing for the Chicago Sun-Times 
|}
 . Retrieved 2011-12-01.  In consummate verbiage, David Sterritt writing for The Christian Science Monitor reasoned, "The movie is longer and slower than necessary, but it explores interesting questions of wartime violence, personal integrity, and what it means to come of age in a society ripping apart at the seams."  Film critic Steve Simels of TV Guide was consumed with the nature of the subject matter exclaiming, "A nicely ambiguous ending and terrific acting by the mostly young cast mostly makes up for the longeurs, however, and for the record, Jewel acquits herself well in a not particularly demanding role." 

===Box office=== Stuart Little starring Geena Davis opened in 1st place with $11,214,503 in revenue.  Ride with the Devil went on to top out domestically at $635,096 in total ticket sales through a 6-week theatrical run.  For 1999 as a whole, the film would cumulatively rank at a box office performance position of 219. 

==See also==
 
* 1999 in film
* Quantrills Raiders
* Lawrence Massacre

==References==
;Footnotes
 

;Further reading
 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 