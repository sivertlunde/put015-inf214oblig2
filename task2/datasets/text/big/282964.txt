The Killing (film)
 
{{Infobox film
| name           = The Killing
| image          = TheKillingPosterKubrick.jpg
| caption        = Theatrical release poster
| director       = Stanley Kubrick
| producer       = James B. Harris
| screenplay     = Stanley Kubrick
| based on       =  
| starring       = {{Plain list|
* Sterling Hayden
* Coleen Gray
* Vince Edwards
* Jay C. Flippen
* Marie Windsor
* Ted de Corsia
}}
| narrator       = Art Gilmore
| music          = Gerald Fried
| cinematography = Lucien Ballard
| editing        = Betty Steinberg
| studio         = Harris-Kubrick Pictures Corporation
| distributor    = United Artists
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $320,000 
| gross          =
}} Jim Thompson and based on the novel Clean Break by Lionel White. The drama features Sterling Hayden, Coleen Gray, Vince Edwards, Marie Windsor, and Elisha Cook Jr. 

==Plot==
Johnny Clay (Sterling Hayden) is a veteran criminal planning one last heist before settling down and marrying Fay (Coleen Gray). He plans to rob two million dollars from the money-counting room of a race-track during a featured race. He assembles a team consisting of a corrupt cop (Ted de Corsia), a betting window teller (Elisha Cook Jr.) to gain access to the backroom, a sharpshooter (Timothy Carey) to shoot the favorite horse during the race to distract the crowd, a wrestler (Kola Kwariani) to provide another distraction by provoking a fight at the track bar, and a track bartender (Joe Sawyer).

George Peatty, the teller, tells his wife Sherry (Marie Windsor) about the impending robbery. Sherry is bitter at George for not delivering on the promises of wealth he once made her, so George hopes telling her about the robbery will placate and impress her. Sherry does not believe him at first but, after learning that the robbery is real, she enlists her lover Val Cannon (Vince Edwards) to steal the money from George and his associates.

The heist is successful, although the sharp-shooter is shot and killed by the police. The conspirators gather at the apartment where they are to meet Johnny and divide the money. Before Johnny arrives, Val appears and holds them up. A shootout ensues and a badly wounded George is the sole survivor. He goes home and shoots Sherry before dying.

Johnny, on his way to the apartment, sees George staggering in the street and knows that something is wrong. He buys the biggest suitcase he can find to put the money in (and struggles to lock it properly). At the airport, Johnny and Fay arent allowed to take the suitcase along as hand luggage because of its size, and instead must check it in as regular luggage. Johnny reluctantly complies. While waiting to board their plane, they watch as the suitcase falls off a cart, breaks open and the loose bank-notes are swept away by the wind. They leave the airport but are unsuccessful at finding a cab. Fay urges Johnny to flee but he refuses, stating that there is no use trying to escape and utters the tagline for the movie: "Whats the difference?". The film ends with two officers coming to arrest him.

==Cast==
* Sterling Hayden as Johnny Clay
* Coleen Gray as Fay
* Vince Edwards as Val Cannon
* Jay C. Flippen as Marvin Unger
* Elisha Cook Jr. as George Peatty
* Marie Windsor as Sherry Peatty
* Ted de Corsia as Policeman Randy Kennan
* Joe Sawyer as Mike OReilly James Edwards as Track Parking Attendant
* Timothy Carey as Nikki Arane
* Joe Turkel as Tiny
* Jay Adler as Leo the Loanshark
* Kola Kwariani as Maurice Oboukhoff
* Dorothy Adams as Mrs. Ruthie OReilly

==Production==
 

 
 Jim Thompson to write the script for the film. United Artists stated that they would help finance the film if Harris and Kubrick could find a high-profile actor to star, which they did through Hayden, who agreed to accept $40,000 for the film. However, Hayden wasnt a big enough star for United Artists so they only put down $200,000 for the film; Harris financed the rest with $80,000 of his own money and a $50,000 loan from his father.   The film was the first of three on which Harris and Kubrick collaborated as producer and director in a span of less than ten years.     Working titles for the film were Clean Break and Bed of Fear. 
 Crime Wave. The art director, Ruth Sobotka, was Kubricks wife at the time.  Kubrick and Harris  moved from New York to L.A. to shoot the picture, and Kubrick went unpaid for the time being, surviving on loans from Harris. Aside from Hayden whom Kubrick had been impressed with in The Asphalt Jungle, he cast actors from film noirs he had liked such as Timothy Carey, Ted de Corsia, Elisha Cook Jr. and Marie Windsor, and the wrestler in the film was an old chess friend of Kubricks, Kola Kwariana.  The union in Hollywood stated that Kubrick would not be permitted to be both the director and cinematographer of the movie, so veteran cinematographer Lucien Ballard was hired for the shooting. The two men clashed during the shooting, and on one occasion Kubrick favored a long tracking shot, with the camera close to the actors with a 25mm wide angle lens to provide slight distortion of the image, but Ballard moved it further away and began using a 50mm lens. Kubrick sternly ordered Ballard to put the camera back or he was fired. 

==Reception==

===Critical response===
The film performed poorly at the box office, failing to secure a proper release across the United States, and it was only at the last minute that it was promoted as a second feature to Bandido! (1956), but failed to make money. However, it garnered critical acclaim, landing on several critical Top-Ten lists for the year. Time (magazine)|Time magazine wrongly predicted that it would "make a killing at the cash booths", asserting that Kubrick "has shown more audacity with dialogue and camera than Hollywood has seen since the obstreperous Orson Welles went riding out of town on an exhibitors poll"   —recording a loss of $130,000. Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 158  
A.H. Weiler, film critic for The New York Times, wrote, "Though The Killing is composed of familiar ingredients and it calls for fuller explanations, it evolves as a fairly diverting melodrama. ... Aficionados of the sport of kings will discover that Mr. Kubricks cameras have captured some colorful shots of the ponies at Bay Meadows track. Other observers should find The Killing an engrossing little adventure."  Variety (magazine)|Variety magazine liked the acting and wrote, "This story of a $2 million race track holdup and steps leading up to the robbery, occasionally told in a documentary style which at first tends to be somewhat confusing, soon settles into a tense and suspenseful vein which carries through to an unexpected and ironic windup...Hayden socks over a restrained characterization, and Cook is a particular standout. Windsor is particularly good, as she digs the plan out of her husband and reveals it to her boyfriend."  Although Kubrick and Harris had thought that the positive reception from critics had made their presence known in Hollywood, Max Youngstein of United Artists still considered them to be "Not far from the bottom" of the pool of new talent at the time,  but Dore Schary of Metro-Goldwyn-Mayer was impressed with the film, and offered the duo $75,000 to write, direct and produce a film, which became Paths of Glory (1957). 

Fifty seven years after release   placed the film at 15th among his top 25 favorite noir films, saying "If you believe that a good script is a succession of great scenes, you cant do better than this. Hey, that scene was so good, lets do it again from somebody elses perspective".   In his 1999 book Dark City: The Lost World of Film Noir, Muller wrote, "With The Killing, Stanley Kubrick offered a monument to the classic caper film, and a fresh gust of filmmaking in one package. Who knew when he wrapped it, that it would be the last amusing movie hed ever make?" 

In 1999, film critic Mike Emery wrote, "Kubricks camerawork was well on the way to finding the fluid style of his later work, and the sparse, low-budget circumstances give the film a raw, urgent sort of look. As good as the story and direction are, though, the true strength of The Killing lies in the characters and characterizations."   The same year, director Peter Bogdanovich, writing for The New York Times, noted that while The Killing did not make money, it, along with Harris-Kubricks second film Paths of Glory, established "Kubricks reputation as a budding genius among critics and studio executives.". 

On January 9, 2012, Roger Ebert added The Killing in his list of "Great Movies". In his opening remarks, Ebert writes, "Stanley Kubrick considered The Killing (1956) to be his first mature feature, after a couple of short warm-ups. He was 28 when it was released, having already been an obsessed chess player, a photographer for Look magazine and a director of March of Time newsreels. Its tempting to search here for themes and a style he would return to in his later masterpieces, but few directors seemed so determined to make every one of his films an individual, free-standing work. Seeing it without his credit, would you guess it was by Kubrick? Would you connect Dr. Strangelove with Barry Lyndon?" 

===Influence=== fractured time-line from different character perspectives to tell their story, a technique Tarantino copied again for 1994s Pulp Fiction.

===Awards===
  
Nominations
* BAFTA Award for Best Film#1950s|B.A.F.T.A. Film Award, Best Film from any Source, USA; 1957.

==Home media==
 
A digitally restored version of the film was released on D.V.D. and Blu-ray by The Criterion Collection. 

==References==
 
;Bibliography
* 

==External links==
*  
*  
*  
*  
*  
*   essay by Haden Guest at The Criterion Collection
*   essay by Chuck Stephens at The Criterion Collection
*   film trailer at the Internet Movie Database

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 