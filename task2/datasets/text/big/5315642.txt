Le Signe du Lion
 
{{Infobox film
| name           = The Sign of Leo
| image          =
| image_size     =
| caption        =
| director       = Éric Rohmer
| producer       = Claude Chabrol
| writer         = Éric Rohmer (scenario) Paul Gégauff (dialogue)
| narrator       =
| starring       = Jess Hahn Michèle Girardon Van Doude
| music          = Louis Saguer
| cinematography = Nicholas Hayer
| editing        = Anne-Marie Cotret
| distributor    =
| released       =  
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Le Signe du lion (The Sign of Leo) is a French film directed by  .

It stars Jess Hahn, Michèle Girardon, and Van Doude.

== Plot ==
Pierre (Jess Hahn) is an American-born, 39-year-old bohemian and aspiring composer living in Paris. One morning he receives a telegram informing him that his wealthy aunt has died. Assuming that he has inherited her factories in Germany and Switzerland, he throws a lavish party with his friend Jean-Francois (Van Doude), a reporter for Paris-Match. Pierre borrows large sums of money, believing that hell be able to pay everyone back with his inheritance; however, he soon discovers that his aunt left everything to his cousin. Penniless and abandoned by his friends, he soon finds himself homeless.

== Production ==
The film was photographed in black and white by Nicholas Hayer and printed at Laboratoires GTC in Joinville-le-Pont.  Most of the shooting, including all outdoor scenes, was done on location. Rohmer, the eldest of the members of the Cahiers du cinéma circle of the Nouvelle Vague, was 38 at the time of production.

The film was produced by fellow New Wave director and Cahiers du cinéma critic Claude Chabrol through Ajym Films, which had produced his debut feature, Le Beau Serge and would go on to produce Paris nous appartient, the debut feature of their colleague and friend Jacques Rivette.

=== Style ===
The film is often noted for the differences in tone and cinematic style it has from the work Rohmer is best known for.    The film is shot in a wider aspect ratio, 1.66, than most of Rohmers features; the director has used the 1.37 aspect ratio (also known as Academy ratio) for almost all of his films. It also features a musical score (rare for Rohmer) by Louis Saguer and a cameo by fellow New Wave director and Cahiers du cinéma critic Jean-Luc Godard; though the other French New Wave directors  frequently appeared in cameo roles in each others films (or, as in the case of Godard, cast directors they admired in their films), this is the only instance of such casting in a feature film directed by Rohmer. Writing for the website kamera.co.uk, Chris Weigand notes that in The Sign of Leo "perhaps more than in any other New Wave work," Paris appears to be  "a filthy and unattractive city, viewed through the eyes of the desperate and needy." 

Also unlike most of the directors films, Rohmer did not write the dialogue for the screenplay.  The work is credited to Paul Gégauff, and Rohmer is instead only credited with the films story.

== Reception and Influence ==
Though praised by other members of the Nouvelle Vague (including Jean-Luc Godard, who put it on his top ten for 1962,  The Sign of Leo was a commercial failure,  a fact that kept Rohmer from making another feature until 1966.
 naturalist novels realists such as Theodore Dreiser. It marks Rohmer out as one of the most literary of New Wave directors - always devoting particular attention to his characters complex emotions and inner thoughts." 
 homage to it with his first short film, Der Stadtstreicher. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 