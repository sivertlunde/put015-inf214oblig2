Nizhal Moodiya Nirangal
{{Infobox film
| name           = Nizhal Moodiya Nirangal
| image          =
| caption        =
| director       = Jesey
| producer       = PA Thomas
| writer         = John Alunkal Joseph Madappally (dialogues)
| screenplay     = Joseph Madappally Sharada Ratheesh Ambika
| music          = KJ Joy
| cinematography = Vipin Das
| editing        = M Umanath
| studio         = Thomas Pictures
| distributor    = Thomas Pictures
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by KJ Joy.   

==Cast==
  Sharada as Sosamma
*Ratheesh as Baby
*Bharath Gopi as Unni Ambika as Molamma
*Balan K Nair as Thambi
*Jagathy Sreekumar as Nanappan
*Kaviyoor Ponnamma as Thambi & Unnis Mother
*Manavalan Joseph as Pillechan 
*PA Thomas as Cherian
*Master Piyush
*Master Prince
*Kalaranjini as Daisy
*Achankunju as Achankunju
*Alummoodan
*Baby Sangeetha
*Baby Vandana
*Bahadoor as Kunjaalikka
*Chandraji
*Mala Aravindan as Kuruppachan
*Ranipadmini as Leela 
*Ravi Menon as Boban
*Santhakumari as Thresya
*Silk Smitha as Cabaret Dancer
 

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaliyarangil || Vani Jairam || Sreekumaran Thampi || 
|-
| 2 || Ormakal Paadiya || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Oru Maalayil || P Susheela, Chorus || Sreekumaran Thampi || 
|-
| 4 || Poomaram Oru Poomaram || Vani Jairam || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 