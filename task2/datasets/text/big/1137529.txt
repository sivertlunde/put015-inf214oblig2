Deathwatch (2002 film)
 
 
 
{{Infobox film name = Deathwatch image = Deathwatch film.jpg caption =  director = Michael J. Bassett  writer = Michael J. Bassett starring = Jamie Bell, Laurence Fox, Kris Marshall, Andy Serkis, Hugo Speer, Hugh OConor,  producer = Mike Downey  distributor = Lions Gate Entertainment  budget = released =   runtime = 94 min.  language = German, French, English 
|}}
 2002 British horror film directed by Michael J. Bassett.

== Plot ==
  British soldiers is seen assaulting a German trench at night. Sergeant David Tate (Hugo Speer) is caught in the barbed wire and calls out to Private Charlie Shakespeare (Jamie Bell), a 16-year-old member of the squad, to rescue him from the entanglement. Shakespeare, however, has completely broken down from fear due to the chaos around him, and runs off into the fighting, only to be seemingly buried by shellfire moments later.

The next morning, the squad finds itself slowly advancing through a dense   and German bayonets sticking out of many. While detonating charges to close off some passages, they hear a demon-like growl, and as they walk away they fail to notice vast amounts of blood pouring from the mud. 

Later, while Private Jack Hawkstone (Hans Matheson) is exploring more of the trench, he is called away by the other soldiers. As he turns to leave, he notices a body covered in mud, leaning against the wall of the trench. It turns out to be the second German, who had escaped from their initial encounter. As Hawkstone calls for help, the German lunges at him with a makeshift weapon and Hawkstone is forced to shoot him, wounding him and resulting in a fistfight between the two in the mud. During the skirmish, Private Barry Starinski (Kris Marshall) runs up and shoots the German, who falls to his knees. Quinn taunts and then executes him with a pistol round to the skull.

The isolation and sinister nature of the trenches soon starts to exact a toll on the British.  Private Colin Chevasse (Ruaidhri Conroy) is slowly dying from a spinal injury he received during the prologue, and the German radio set they found cuts out after its first legible message, denying them any means to call for support. Angry and fearful of the strange environment, they vent their rage upon the German prisoner, who is abused by everyone except Shakespeare, who becomes disgusted by the behavior, and Chevasse, who is too injured to move. During their first night, Starinski, who has isolated himself in a secluded area of the trench and is masturbating to pornographic images he looted from the German dead, is distracted by strange sounds and led deeper into the trenches, where he finds three German corpses wrapped in barbed wire in a standing position. As he shouts to alert the others of the scene, one of the corpses suddenly comes to life and ambushes him, his screams audible to the others. By the time they reach him, they are too late, with Shakespeare finding Starinskis corpse lashed to the wall of the trench with barbed wire. Suspecting that German troops hiding in the dugouts are responsible, the men violently interrogate the prisoner. Shakespeare manages to translate his ramblings from French, saying that they will turn on each other and that theres "evil" in the trenches.

Later that night, Captain Bramwell Jennings (Laurence Fox), the commander of the battered company, begins to hear the sounds of artillery and an infantry attack, despite the fact that no one is in the area. In a panic, he stumbles through the trenches in search of Sergeant Tate, who has sent Hawkstone searching for the captain, finally stumbling into the central area of the trench, where the squad has piled the German dead into a massive mound. His nerves on edge, he turns and mistakenly shoots Hawkstone when he hears him coming from behind. Despite the Captains manslaughter, Tate remains fiercely devoted to his orders and refuses to abandon the trench. That same day, the men, believing the attacks and the strange events to be perpetrated by Germans still hiding in the trench, rig and collapse sections of the trench with explosives to give them a more defensible position.

The next night, more strange noises are heard, and the mens morale and discipline deteriorates even further, with Quinn reaching near-homicidal insanity. After McNess is pursued by an eerie red mist and completely drenched in blood and fear, he climbs out of the trenches and runs into no-mans land, but is shot in the leg by Private Anthony Bradford (Hugh OConor). A rescue attempt is made by Corporal "Doc" Fairweather (Matthew Rhys), the company medic, but McNess, crawling across the ground, is pursued by a moving mud mound that drags him underground. Meanwhile, Bradford, who has been found by Shakespeare, is convinced that both he and the trenches are possessed by death. Convinced that he will kill others, he asks Shakespeare to shoot him. When the latter refuses, Bradford runs off.

In the morning, Quinn, having finally lost his sanity, crucifies the German prisoner, Friedrich, on a wooden beam in no-mans land, binding his arms and legs with barbed wire. He then proceeds to beat him with his spiked club,  daring any Germans there might be to shoot him and save their comrade. In the trench, Jennings, appearing to have also lost touch with reality, suddenly decides he wants a company inspection, and relieves Tate of duty when he refuses to comply. Fearing for the outcome of the standoff, both Shakespeare and Fairweather comply with the order, saving Tate from discipline. In the meantime, Jennings notices Friedrich screaming and moves off, going after Quinn. Apparently oblivious to Quinns madness, he demands that he fall in for the inspection, but Quinn, already in a murderous frenzy, turns and begins to strike him with the club, decreeing his hatred for officers, and kills him. At this point Tate moves into the no mans land and fights Quinn with his knife, but Quinn gains the upper hand and forces Tate to his knees. As Shakespeare rushes out of the trench save Tate, Quinn whispers to him to "make your peace" before stabbing him to death with a knife. Shakespeare confronts him with his rifle raised, though he cannot bring himself to shoot him. Quinn taunts Shakespeare and, when he refuses to kill him, berates him as a coward. Shakespeare shouts that he at least isnt a murderer, to which Quinn replies that murder is all that soldiers do. Quinn raises the club and starts towards Shakespeare, but is stopped by living strands of barbed wire that rise up from the dirt and begin to wrap around and skewer him. Shakespeare then shoots Quinn, ending his torment, before cutting Friedrich loose.

Back in the trench, Shakespeare arms Friedrich, who has been crippled by Quinns torture, with a rifle to defend himself. He then runs off, looking for Fairweather. Finding the paralysed Chevasse, now pale and pestered by flies, he discovers that he appears to be able to move his legs again. However, when Shakespeare lifts up the blanket that was covering him, he finds, to their horror, that rats have been eating Chevasses legs. Horrified, Chevasse begins to scream uncontrollably, forcing Shakespeare to shoot him in the head to put him out of his misery. Shakespeare runs off again and finds the two remaining soldiers, Bradford and Fairweather. Bradford has tied Fairweather up with barbed wire and shoots Fairweather in the head before Shakespeare can stop him. Shakespeare finally gives Bradford what he wanted and stabs him in the stomach with his bayonet and then shoots him.

At that point, the soil under the German dead starts to cave in, while barbed wire blocks off every passage. Shakespeare tries to escape, but he stumbles and is sucked down into the darkness. He wakes up in a dark cave filled with corpses, at which end he finds living versions of the whole group while they were eating just the other night, including himself. He shouts defiantly that he isnt dead and runs off, reaching the surface of the trenches. Friedrich, now capable of walking and in perfect health, appears and points his rifle at him. Shakespeare, exasperated by the apparent betrayal, shouts in both English and French that he tried to help him. He acknowledges, in perfect English, that Shakespeare was the only one who helped him, and, pointing to a ladder leading up into no-mans land, tells him that thats why hes free to go. Shakespeare asks whats out there, but Friedrich has already disappeared. Shakespeare climbs out of the trench and leaves to an unknown fate, disappearing into the mist. 

An indeterminate amount of time later, another team of British soldiers arrive at the trench. Seeing Friedrich sitting idly, they shout at him to surrender, to which he complies, lifting his hands. He gives the camera a knowing stare before the screen fades.

== Cast ==
*Jamie Bell as Private Charlie Shakespeare
*Ruaidhri Conroy as Private Colin Chevasse Mike Downey as Captain Martin Plummer
*Laurence Fox as Captain Bramwell Jennings
*Dean Lennox Kelly as Private Willie McNess
*Torben Liebrecht as Friedrich
*Kris Marshall as Private Barry Starinski
*Hans Matheson as Private Jack Hawkstone
*Hugh OConor as Private Anthony Bradford
*Matthew Rhys as Corporal "Doc" Fairweather
*Andy Serkis as Private Thomas Quinn
*Hugo Speer as Sergeant David Tate
==Release==
 
==Reception==
The film received mixed to negative reviews by most critics who praised its creative premise and atmosphere, but criticized its story execution and editing.

Allmovie gave the film a positive review calling it "a highly crafted atmospheric creep-out that knows when to go for the jugular and when to slather on the paranoia".  
TV Guide awarded the film 2.5 / 4 stars stating, "Bassett deserves half a salute for Twilight Zone-ish wallow in WWI misery, which works up some creepy atmosphere between scenes of dehumanizing combat. But the spook show element ultimately seems simultaneously ghoulish and hokey, and the pacifist moral is hammered home with blunt obviousness".   The Guardian gave the film a mixed review praising the films premise and direction but panned the films dull script. 
It currently has a 25% "Rotten" on Rotten Tomatoes. 

==References==
 
== External links ==
* 
*  
*  

 

 
 
 
 
 
 
 