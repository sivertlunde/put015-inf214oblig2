The Magic Box
 Magic Box}}
 
 
 
{{Infobox film
| name           = The Magic Box
| image          = Magic_box_1.jpg
| caption        =
| director       = John Boulting
| producer       = Ronald Neame
| writer         = Ray Allister and Eric Ambler
| starring       = Robert Donat Margaret Johnston Maria Schell Robert Beatty Margaret Rutherford
| music          = William Alwyn
| cinematography = Jack Cardiff Richard Best
| distributor    = British Lion Films
| released       = 1951
| runtime        = 118 min.
| country        = UK
| language       = English
| budget         =
| gross          = £82,398 (UK) 
}}

The Magic Box is a 1951 British, Technicolor, biographical drama film, directed by John Boulting. The film stars Robert Donat as William Friese-Greene, with a host of cameo appearances by such actors as Peter Ustinov and Laurence Olivier. It was produced by Ronald Neame and distributed by British Lion Film Corporation. The film was a project of the Festival of Britain and adapted by Eric Ambler from the controversial biography by Ray Allister.

==Background==
This biographical drama gives an account of the life of William Friese-Greene, who first designed and patented one of the earliest working cinematic cameras. The film was completed and shown just before the end of the 1951 Festival of Britain, but the general release was not until 1952. Told in flashback, the film details Friese-Greenes tireless experiments with the "moving image," leading inexorably to a series of failures and disappointments, as others hog the credit for the protagonists discoveries. 

==Plot summary==
In 1921, William Friese-Greene, in dire financial straits and separated from his wife, but still working, attends a film conference in London. He is saddened that all the attendees are businessmen interested only in moneymaking. He attempts to speak, but no-one is interested and he sits down. He thinks back to his early pioneering days.

Young "Willie" works as an assistant to photographer Maurice Guttenberg, who will not let him take portraits his way. He leaves and, with his new wife, a client of his former employer, he opens a studio. After a slow start, he does well and opens other studios, but he is more interested in developing moving pictures and colour films. He single-mindedly works on his ideas, spending more and more money, and is eventually declared bankrupt. With the coming of World War I, their sons (one under age) enlist in the army to relieve their parents of the burden of providing for them.
 Hyde Park that afternoon. Excited, he rushes out and drags in a passing policeman to witness the success of the film. The policeman is dumbfounded, not quite comprehending what he has just seen.

Back at the conference, Friese-Greene again stands up to speak, but becomes incoherent and is forced to sit down. He collapses. A doctor is called, but it is too late. Examining the contents of his pockets in an attempt to identify him, the doctor comments that all the money he could find was just enough for a ticket to the cinema.

==Awards== Best Film and Best British Film.

== Cast ==
* Robert Donat as William Friese-Greene
* Margaret Johnston as Edith Harrison
* Maria Schell as Helena Friese-Greene
* David Oake as Claude Friese-Greene
* Janette Scott as Ethel Friese-Greene
* John Howard Davies as Maurice Friese-Greene
* Robert Beatty as Lord Beaverbrook
* Richard Attenborough as Jack Carter
* Basil Sydney as William Fox Talbot
* Bernard Miles as Cousin Alfred
* Eric Portman as Arthur Collings
* Mary Ellis as Mrs Collings
* Muir Mathieson as Sir Arthur Sullivan
* Joyce Grenfell as Mrs Claire
* Dennis Price as Harold
* Margaret Rutherford as Lady Pond
* Mervyn Johns as Goitz
* Glynis Johns as May Jones
* Frederick Valk as Maurice Guttenberg
* Ronald Shiner as the Fairground Barker Peter Reynolds as Bridegroom

===Cameos=== Barry Jones as a doctor
* Bessie Love as wedding group member
* Cecil Parker at the Connaught Rooms John Rudge
* David Tomlinson as a Lab Assistant
* Emlyn Williams as a Bank Manager
* Ernest Thesiger as "man"
* Kay Walsh as a receptionist
* Laurence Olivier and Jack Hulbert as Police officers
* Leo Genn as a doctor
* Marius Goring as an estate agent
* Michael Denison as a reporter
* Michael Hordern as the Official Receiver
* Miles Malleson as an orchestra conductor
* Peter Ustinov as an "industry man"
* Sheila Sim as a nursemaid
* Sid James as an Army sergeant in storeroom
* Stanley Holloway as a brokers man
* Thora Hird as a housekeeper
* William Hartnell as a Recruiting sergeant John McCallum, Patrick Holt, Robertson Hare, Richard Murdoch and Sybil Thorndike as sitters Henry Edwards as the Butler at Fox Talbots
* Renee Asherson as Miss Tagg
* Martin Boddey as SItter in Bath Studio Edward Chapman as Father in family group
* Maurice Colbourne as Brides father in wedding group
* Roland Culver as 1st Company promoter
* Joan Dowling as Maggie
* Marjorie Fielding as Elderly Viscountess
* Robert Flemyng as Doctor in surgery
* Everley Gregg as Bridegrooms mother in wedding group
* Kathleen Harrison as Mother in family group
* Joan Hickson as Mrs Stukely
* Jack Hulbert as 1st Holborn Policeman Peter Jones as Industry Man
* Ann Lancaster as Bridesmaid in Wedding Group Herbert Lomas as Warehouse manager
* John Longden as Speaker in Connaught rooms
* Garry Marsh as Second company promoter
* Michael Trubshawe as Sitter in Bath studio
* Amy Veness as Grandmother in wedding group
* Charles Victor as Industry man
* Harcourt Williams as Tom
* Frank Pettingell as Bridegroos father in Wedding Group
* Norman Pierce as Speaker in Connaught rooms
* Michael Redgrave as Mr. Lege
* Oda Slobodskaya as Soloist at Bath concert John Stuart as 2nd Platform man at Connaught
* Marianne Stone as Bride in wedding group

==References==
 
* TimeOut Film Guide – published by Penguin Books – ISBN 0-14-029395-7

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 