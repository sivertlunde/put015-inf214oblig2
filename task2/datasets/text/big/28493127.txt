Sleep Sweet, My Darling
{{Infobox film
| name           = Sleep Sweet, My Darling
| image          = 
| alt            = 
| caption        = 
| director       = Neven Hitrec
| producer       = Ivan Maloca
| writer         = Hrvoje Hitrec
| starring       = Ljubomir Kerekes
| music          = 
| cinematography = Stanko Herceg
| editing        = Slaven Zecevic
| studio         = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Croatia
| language       = Croatian
| budget         = 
| gross          = 
}} Croatian comedy film directed by Neven Hitrec. It was released in 2005 and was entered into the 28th Moscow International Film Festival.   

==Cast==
* Ljubomir Kerekeš - Darko Skrinjar
* Ivan Glowatzky - Tomica
* Ines Bojanić - Tonka
* Alan Malnar - Tomica
* Franka Kos - Janja Bartolic
* Vlatko Dulić - Djed
* Višnja Babić - Mira Skrinjar
* Ksenija Marinković - Teta Nadica
* Marija Kohn - Neda Glazar
* Danko Ljuština - Djed Ladovic

==References==
 

==External links==
*  

 
 
 
 
 
 