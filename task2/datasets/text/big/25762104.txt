Shubhamangala
{{Infobox film name           = Shubhamangala image          =  image_size     =  caption        =  director       = Puttanna Kanagal producer       = Ravi writer  Vani
|screenplay     = Puttanna Kanagal based on       =   starring       = Srinath  Aarathi music          = Vijaya Bhaskar cinematography = N. G. Rao editing        = Bal G. Yadav studio         = Raghunandan International distributor    =  released       =   runtime        = 161 minutes country        = India language  Kannada
|budget         =  preceded_by    =  followed_by    = 
}}

Shubhamangala ( ) is a 1975 Indian Kannada language film directed by Puttanna Kanagal, based on a novel of the same name by Vani (writer)|Vani, and starring Aarathi and Srinath in lead roles. The supporting cast features Shivaram, Ambareesh, Musuri Krishnamurthy and K. S. Ashwath. 

It is said that Puttanna and Aarathi became close during the filming of this movie. They were later married. The dialogues were written by renowned Kannada writer Bichi.

==Plot==
Hema is a pampered girl brought up in a small town by her father. Timma and Mooga are her servants, but she treats them just like her friends. Hema is playful and acts like a kid in all matters even though she has reached marriageable age. She meets Prabhakara, her cousin who is visiting her village after many years. Hemas father doesnt like Hema mingling with Prabhakar and tells her about a family fight he had with Prabhakaras father years ago. Hema begins to hate Prabhakara from then on. After her father passes away, Hema & her servants are forced to live with Prabhakara. The story is about the growth of Hema from a pampered girl into a self-reliant woman who earns a living, not wanting to depend on others.

==Cast==
* Aarathi as Hema
* Srinath as Prabhakara
* Shivaram as Timma
* Ambareesh as Mooga
* Upasane Seetharam as Srinivas Rao (Hemas father)
* Loknath as Uncle Anand
* B. V. Radha as Radha Ashwath as the doctor
* B. Jaya (actress)|B. Jaya

== Soundtrack ==
The music of the film was composed by Vijaya Bhaskar with lyrics of the soundtrack penned by Kanagal Prabhakara Shasthry, Vijaya Narasimha, Chi. Udaya Shankar and M. N. Vyasa Rao

=== Track List ===
{{Track listing
| lyrics_credits = yes
| extra_column = Singer(s)
| title1 = Shubhamangala Sumuhurthave
| title2 = Hoovondu Bali Bandu
| title3 = Snehada Kadalalli
| title4 = Ee Shathamanada Maadari Hennu
| title5 = Suryangu Chandrangu
| title6 = Hema Naakondla Naaku 
| 
| lyrics1 = Kanagal Prabhakara Shastry
| lyrics2 = Vijaya Narasimha
| lyrics3 = Chi. Udaya Shankar
| lyrics4 = Vijaya Narasimha
| lyrics5 = M. N. Vyasa Rao
| lyrics6 = M. N. Vyasa Rao
| 
| extra1 = P. B. Srinivas, Vani Jayaram
| extra2 = R. N. Sudarshan
| extra3 = S. P. Balasubramanyam
| extra4 = Vani Jayaram
| extra5 = Ravi
| extra6 = S. P. Balasubramanyam
}}

== References ==
 

== External links ==
*  

 
 
 
 


 