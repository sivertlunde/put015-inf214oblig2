The Invisible Woman (2013 film)
 
 
{{Infobox film
| name           = The Invisible Woman
| image          = The Invisible Woman poster.jpg
| caption        = Theatrical release poster
| producer       = {{Plainlist|
* Christian Baute
* Carolyn Marks Blackwood
* Stewart Mackinnon
* Gabrielle Tana
}}
| director       = Ralph Fiennes
| screenplay     = Abi Morgan
| starring       = {{Plainlist|
* Ralph Fiennes
* Felicity Jones
* Kristin Scott Thomas
* Tom Hollander
}}
| based on       =  
| music          = Ilan Eshkeri
| cinematography = Rob Hardy
| editing        = Nicolas Gaster
| studio         = {{Plainlist|
* BBC Films
* Headline Pictures
* Magnolia Mae Films
* Taeoo Entertainment
}}
| distributor    = {{Plainlist| Lionsgate  
* Sony Pictures Classics  
}}
| released       =   
| runtime        = 111 minutes
| country        = United Kingdom
| language       = English
| budget         = £12 million 
| gross          = $3.2 million 
}} Nelly Ternan, Best Costume Michael OConnor) at the 86th Academy Awards. 

==Plot== Ellen "Nelly" Ternan (Felicity Jones) is noticed by forty-five-year-old writer Charles Dickens (Ralph Fiennes) while she is performing at Londons Haymarket Theatre. Soon after, he casts her, along with her mother (Kristin Scott Thomas) and sister Maria (Perdita Weeks), in a performance of The Frozen Deep by Wilkie Collins at Dickens Free Trade Hall in Manchester. At a party following the performance, the famous author and the actress share a brief moment alone.
 Harrow Speech Room in London. Afterwards, Dickens is delighted to see Nelly again. Soon after, Dickens takes the Ternan family to Doncaster Racecourse and begins to spend more time with them. Having become disillusioned with his wife, who does not share his energy and passion for literature and ideas, Dickens cherishes his time with the young actress who shares his interests and passions. Nelly in turn loves spending time with the famous novelist.
 fallen women" and their children, Dickens invites the Ternan family to his home, where Nelly examines with fascination the authors books, manuscripts, and writing instruments. When they are alone, they share details and secrets about their lives and upbringings, and the two grow closer.
 Catherine (Joanna Scanlan) visits Nelly at her home to deliver a jeweled bracelet birthday gift from her husband, which was delivered to her by mistake.

After the birthday party, Dickens and Collins arrive and take her to the house that Collins shares with his mistress Caroline Graves (Michelle Fairley) and her daughter. There, Nelly sees the kind of arrangement Dickens may have in mind for her. Later in the carriage outside her cottage, she confronts Dickens about the suggested arrangement and objects to the idea of being his "whore". After apologizing and confessing that he no longer loves his wife, Dickens accompanies Nelly inside where he comforts her. Soon after, Dickens announces in The Times his "amicable" separation from his wife while boldly denying the rumors of an affair with Nelly. Dickens wife and children are devastated by the news.
 Estella and Pip together. Dickens reads to her from the novel as if speaking directly to her: 
  }}
 derails killing ten passengers. Dickens rescues Nelly, and then, with her encouragement, pretends that he was travelling alone, to avoid the scandal of it being known they were travelling together. Dickens leaves Nelly in the care of others, and tends to the injured and dying along the train. Nelly observes him retrieve a manuscript page of an episode of Our Mutual Friend on which he had been working.

In the coming years, Nelly remains his secret mistress until his death in 1870. In 1876, she marries Oxford graduate George Wharton Robinson, twelve years her junior. The couple have a son and run a boys school at Margate. While knowing that she knew Charles Dickens as a child, George does not suspect that she was his mistress. Only the Reverend Benham knows her secret. As she watches her son perform in a school play, she remembers the epilogue lines she spoke on stage in The Frozen Deep for Dickens:
 

==Cast==
* Ralph Fiennes as Charles Dickens Nelly Ternan
* Kristin Scott Thomas as Mrs. Ternan
* Tom Hollander as Wilkie Collins
* Joanna Scanlan as Catherine Dickens
* Michelle Fairley as Caroline Graves
* Jonathan Harden as Mr. Arnott Tom Burke as Mr. George Wharton Robinson
* Perdita Weeks as Maria Ternan John Kavanagh as Reverend Benham
* Amanda Hale as Fanny Ternan

==Production==
Headline Pictures Stewart Mackinnon first acquired the film rights to Claire Tomalins biography and commissioned Abi Morgan to write the screenplay with development funding from BBC Films and the British Film Institute. The screenplay was written and Mackinnon then approached a number of co-producers and directors before contracting Gabrielle Tana, who had worked with Fiennes on Coriolanus (film)|Coriolanus, his directorial debut.  She proposed the project to Fiennes in 2010, after he finished Coriolanus.     Headline then contracted Fiennes and Tana. Fiennes participation as director was announced in July 2011.    He did not know much about Dickens before taking on the project: "I was ignorant. I had only read Little Dorrit. I knew his obvious ones—Nicholas Nickleby, Oliver Twist, Great Expectations—through adaptations. And Christmas Carol. I didnt know much about the man." 

Fiennes initially approached another actor to play the role of Dickens but it did not work out and he eventually played the part. He worked closely with Abi Morgan on the script and little by little he warmed up to the idea of playing Dickens.  Fiennes and Morgan often met with Tomalin who provided guidance, but she wished to remain outside the actual screenwriting.    The screenplay is structured around a series of "small tragedies and moments of catalyst" described in Tomalins book, which defined their affair according to her.   The actresses considered for the role of Nelly Ternan included Carey Mulligan, Abbie Cornish and Felicity Jones.  Jones was officially cast in December 2011.  Her casting occurred before Fiennes agreed to portray Dickens.  
 Leavesden Film Studios in Hertfordshire.     The Bluebell Railway in Sussex was used for exteriors of the derailment, and featured the Furness Railway Trusts No. 20, the oldest operational standard gauge locomotive in the UK.  

The film had an operating budget of £12 million.   

==Release==
The Invisible Woman premiered at the Telluride Film Festival on 31 August 2013.  The first trailer was launched on 4 October 2013.  The film had a limited release in the United States on 25 December 2013 and  opened in the United Kingdom on 7 February 2014. 

==Reception==

===Box office===
The Invisible Woman earned $1,229,853 at the box office in the United States and $1,373,682 in the United Kingdom.    The total worldwide gross was $3,184,853.   

===Critical response===
Review aggregator Rotten Tomatoes calculated an 72% approval rating, with an average score of 6.8/10, based on 48 reviews. The websites consensus reads, "Its deliberate pace will frustrate some viewers, but for fans of handsomely mounted period drama, The Invisible Woman offers visual as well as emotional cinematic nourishment."  On another review aggregator, Metacritic, which assigns a weighted mean rating out of 100 to reviews from mainstream critics, the film holds an average score of 77, based on 22 reviews, considered to be "generally favorable reviews". 

In his review on the Roger Ebert website, Godfrey Cheshire gave the film three and a half out of four stars, calling it "a formidable achievement for Fiennes as both actor and director".    Cheshire wrote that the story is told with "extraordinary delicacy and cinematic intelligence" and with a "finely calibrated poetic obliqueness that draws the viewer into the relationships gradual unfolding".  Cheshire continued:
 

Cheshire also praised the performances of the leading actors, including Fiennes who "creates an exuberant portrait of Dickens that encompasses his vanity and selfishness as well as his bounteousness and thirst for life", Jones who is "luminous" and "conveys the young womans mix of awe, intoxication and anxiety as she is drawn inexorably into the orbit of a powerful older man", and Scanlan who shows Catherine Dickens "dignity and grace in heart-rending circumstances".  Cheshire concluded:
 

In his review for The Guardian, Peter Bradshaw gave the film four out of five stars, calling the film "piercingly intimate and intelligent" and praises Fiennes for his "strength as a director" and for his "richly sanguine" portrayal of Charles Dickens.    Bradshaw also praises Scanlan for her "shrewd and sensitive performance as Dickenss neglected wife".  Bradshaw concluded, "This is an engrossing drama, with excellent performances and tremendous design by Maria Djurkovic." 

In his review for The Telegraph, Tim Robey gave the film four out of five stars.    Robey focused on the acting performances, especially Scanlan who "gives arguably the standout performance in this generally smashing cast ... in two perfectly weighted, emotionally crushing scenes". 

In his review in the New York Observer, Rex Reed called the film "a cogently written and elegantly appointed period piece that relates passages in his books to emotions in his personal life, holding the attention and shedding light on one of literature’s most fascinating footnotes".   

==References==
;Notes
 
;Citations
 
;Bibliography
 
*  
 

==External links==
 
*  
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 