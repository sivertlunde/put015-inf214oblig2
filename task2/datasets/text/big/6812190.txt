Jolson Sings Again
{{Infobox film
| name = Jolson Sings Again
| image = Jolson Sings Again - 1949 Poster.jpg
| image size = 190px
| border = yes
| caption = 1949 Theatrical Poster
| director = Henry Levin
| producer = Sidney Buchman
| writer = Sidney Buchman
| starring = Larry Parks Barbara Hale William Demarest Bill Goodwin
| music = George Duning Morris Stoloff William E. Snyder
| editing =	William A. Lyon
| distributor = Columbia Pictures
| released =   }}
| runtime = 96 min
| language = English
| budget =
| gross = $5 million (est. US/ Canada rentals) 
}}
Jolson Sings Again is the 1950 film sequel to The Jolson Story, both of which cover the life of singer Al Jolson.

==Synopsis==

In this follow-up to The Jolson Story, we pick up the singers career just as he has returned to the stage after a premature retirement. But his wife has left him and the appeal of the spotlight is not what it used to be. This time Jolson ( ) and the beginning of World War II, Jolson comes back to earth—and returns to the stage. 

Once again teamed with manager Steve Martin (William Demarest), Jolson travels the world entertaining troops everywhere from Alaska to Africa. When he finally collapses from exhaustion it takes young, pretty nurse Ellen Clark (Barbara Hale) to show him theres more to life than "just rushing around".

==Reviews==
"Jolson Sings Again bids fair to par The Jolson Story grosses and may even top them. In short, a smasheroo of unqualified proportions." Abel., Variety, August 1949    "...Jolsons voice is still a formidable, awesome, and grandiously captivating instrument." 

"There is heart, humor, tragedy and a warm sprinkling of sentiment in Mr. Buchmans story. Much of the latter is conjured up by a succession of nostalgic songs which run all through the film and are sung in grand style by Mr. Jolson himself. The vitality of the Jolson voice is suitably matched in the physical representation provided by Larry Parks, who by now comes close to perfection in aping the vigorous expression with which Jolson tacks a song." T.M.P., New York Times, August 18, 1949  

==Commentary==
"Jolson Sings Again is a well-made sequel to The Jolson Story. In some ways, it betters the original. If anything, Jolsons voice sounds even better in this movie, and Larry Parks Jolson is a warmer, more human character here." 

In this sequel, the story reaches the point in Jolsons life where the film of his life is to be made (first film: The Jolson Story), and in preparation for the film Jolson meets the actor who is to portray him. In what is probably a cinema first, Parks plays both Jolson and himself (the young Larry Parks) as they meet in a split-screen scene. Fisher, James. "Al Jolson - a Bio-bibliography" (1994)   
 MUSIC (Scoring WRITING (Story and Screenplay) by Sidney Buchman.  

==Footnotes==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 