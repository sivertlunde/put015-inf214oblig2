Ennu Nathante Nimmi
{{Infobox film
| name           = Ennu Nathante Nimmi
| image          =
| caption        = Sajan
| producer       = PT Xavier
| writer         = Dinesh Murali S. N. Swamy (dialogues)
| screenplay     = S. N. Swamy Raadhu Rahman Rahman Sukumari Shyam
| cinematography = K Ramachandrababu
| editing        = VP Krishnan
| studio         = Vijaya Productions
| distributor    = Vijaya Productions
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Sajan and Rahman and Sukumari in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Mammootty as Mahesh Raadhu as Nirmala Rahman as Nathan
*Sukumari Mukesh
*Thilakan as Chidambaram Kunchan
*Mala Aravindan

==Soundtrack== Shyam and lyrics was written by Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chembaneer poo polen || K. J. Yesudas, KS Chithra || Chunakkara Ramankutty || 
|-
| 2 || Poove arimullappoove || KS Chithra, Unni Menon || Chunakkara Ramankutty || 
|-
| 3 || Sandhyakale || K. J. Yesudas || Chunakkara Ramankutty || 
|-
| 4 || Sharathkaala Raavum || KS Chithra || Chunakkara Ramankutty || 
|-
| 5 || Ullam thulli || Krishnachandran || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 