New Moon (1940 film)
{{Infobox film
| name           = New Moon
| image          = Poster - New Moon (1940) 01.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Z. Leonard W. S. Van Dyke
| producer       = Robert Z. Leonard
| based on       =   Robert Arthur Jacques Deval
| narrator       =
| starring       = Jeanette MacDonald Nelson Eddy Mary Boland George Zucco
| music          = Herbert Stothart
| cinematography = William H. Daniels
| editing        = Harold F. Kress
| studio         = Metro-Goldwyn-Mayer
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $1,487,000   
| gross          = $1,290,000 (domestic earnings)  $1,237,000 (foreign earnings) 
}}

New Moon is a 1940 musical film released by Metro-Goldwyn-Mayer and directed by Robert Z. Leonard, with uncredited direction by W. S. Van Dyke.
 Broadway in New Moon, which premiered in 1930, was less faithful to the stage version.

==Plot summary==
 
During the 18th century in New Orleans, Louisiana, a French nobleman in disguise as a Indentured servant|bondsman, Charles (Nelson Eddy) leads his fellow bondsman in revolt against his ships captain, commandeering the ship and heading out to sea.

==Cast==
* Jeanette MacDonald as Marianne de Beaumanoir
* Nelson Eddy as Charles (Henri), Duc de Villiers
* Mary Boland as Valerie de Rossac
* George Zucco as Vicomte Ribaud
* H. B. Warner as Father Michel Grant Mitchell as Governor of New Orleans
* Stanley Fields as Tambour
* Dick Purcell as Alexander
* John Miljan as Pierre Brugnon
* Ivan F. Simpson as Guizot
* William Tannen as Pierre
* Bunty Cutler as Julie
* Claude King as Monsieur Dubois
* Cecil Cunningham as Governors Wife
* Joe Yule as Maurice

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
   
 
 