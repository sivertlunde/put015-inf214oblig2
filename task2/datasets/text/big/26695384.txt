Praja
 
{{Infobox film
| name           = Praja
| image          = Praja.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Joshiy
| producer       = 
| writer         = K.S Ashik
| narrator       =  Aishwarya  Cochin Haneefa
| music          = M. G. Radhakrishnan Gireesh Puthenchery, M D Rajendran (lyrics)
| cinematography = 
| editing        =    
| studio         = 
| distributor    = 
| released       = 2001
| runtime        = 179 minutes
| country        = India
| language       = Malayalam
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Praja(English: Citizen) is a 2001 Malayalam film by Joshiy starring Mohanlal, N. F. Varghese, Cochin Haneefa and Aishwarya (actress)|Aishwarya. The film was produced by Joshiy and Ashik K.S under the banner of Mukhyadhara Films. It received heavy initial pull, but against the expectations the film was a moderate success.

==Plot==
Ex-underworld don Zakkir Ali Hussein (Mohanlal), after giving up violence, is settled peacefully in the suburbs of Kochi. It was under the strong influence of his foster father, Bappu Haji Mustafa (Anupam Kher), that he left Mumbai. An unsuccessful assassination attempt on  Haji Mustafa in Mumbai by Raman Naik, an underworld don ignites old wounds in Zakir. He, despite repeated pleas from Haji Mustafa and Ilanthaloor Rama Varma (Babu Namboothiri), a father like figure, sets out to Mumbai along with his trusted lieutenant Hamid Plavilakandi Mather, alias Malayalees (Cochin Haneefa). With the help of his friend Arjun (Biju Menon), Zakeer kills down Raman Naik and reaches back in Kochi. On his way back, at Kochi, he meets up with Maya Mary Kurien (Aishwarya (actress)|Aishwarya), Asst. Commissioner of Police, who slowly starts developing a crush on him. Zakir Hussein is visited by Balaraman (Shammi Thilakan), a liquor baron and ex-MP, who warns Zakkir of dire consequences if he continues to interfere in the Mumbai crime world. He also attempts to woo Zakir, but the latter refuses to bow down, sending strong warning to Balaraman and his group. Balaraman, along with Devadevan Nambiar, alias DD (Vijayaraghavan (actor)|Vijayaraghavan), and Arun Naik, the brother of Raman Naik in Mumbai, is planning to finish off Zakir Hussein and Haji Mustafa to regain the lost turf in Mumbai. They are supported by Lahayil Vakkachan (N. F. Varghese), the tainted state home minister. Joseph Madachery, the IG of police conducts a raid at Haji Mustafas poor home on behalf of Vakachan, and beats up Rama Varma Thirumulpad brutally, but the sudden arrival of Zakir saves him. Joseph is severely beaten by Zakir in full public presence, which infuriates Balaraman and DD, who try to demoralize Zakir by publishing fabricated stories about his relationship with ACP Maya Kurein and inmates of Haji Mustafas poor home. Lahayil Vakachan tries to create a truce between Zakir and Balaraman, but fails miserably. This leads to a series of problems, including the arrest of Jaganathan, a close buddy of Zakir, on false narcotic drug charges. Jaganathan is killed in police custody. Balaraman and DD kill Rama Varma Thirumulpad, which prompts Zakir to take the law in his own hands. At a public gathering while addressing thousands of party workers, Zakir Hussein enters by duping police and kills Balaraman, DD and Lahayil Vakkachan.

==Cast==
*Mohanlal as Zakkir Ali Hussein Aishwarya as Maya Mary Kurien
*Babu Namboothiri as Raman Thilumulppadu
*Cochin Haneefa as Hamid Methar / Malayalees
*Manoj K. Jayan as  David Abraham
*Biju Menon as Arjun
*N. F. Varghese as Lahayil Vakkachan (Home Minister) Vijayaraghavan as Devadevan Nambiar(DD)
*Shammi Thilakan as Balaraman Baburaj as Joseph Madacherry(D.I.G. of Police)

*Jagannadha Varma as Mannel Mammachan
*Anupam Kher as Haji Mustafa
* Joju George Vijayakumar as Jagannadhan Alias Appu Suresh Krishna as a Journalist

==Soundtrack==
{{Infobox album 
| Name        = Praja
| Type        = Soundtrack
| Artist      = M. G. Radhakrishnan
| Cover       =
| Background  = 
| Recorded    = 
| Released    = 2001
| Genre       = Film 
| Length      =
| Label       = Satyam Audios
| Producer    = Mukhyadhara
| Reviews     =  Pilots
| This album  = Praja
| Next album  = Megasandesam
|}}
This film includes 4 songs written by late Gireesh Puthenchery, M. D. Rajendran and M. P. Muralidharan. The songs were composed by composer M. G. Radhakrishnan. There is also the song Yeh Zindagi Usi Ki Hai from the 1953 movie Anarkali (1953 film)|Anarkali.
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s) !! Lyricist !! Other notes
|-
| 1 || Allikalil ||  
|-
| 2 || Chandanamani || M. G. Sreekumar || Gireesh Puthenchery || Raga: Hameer Kalyani
|-
| 3 || Aaja || Vasundara Das, Mohanlal || M. P. Mralidharan ||
|-
| 4 || Akale || M. G. Sreekumar || M. D. Rajendran ||
|-
| 5 || Allikalil || M. G. Sreekumar || M. D. Rajendran ||
|-
| 6 || Yeh Zindagi || Sujatha ||  
|}

==External links==
*  
* http://popcorn.oneindia.in/title/3036/praja.html

 
 
 
 
 