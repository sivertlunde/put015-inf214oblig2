Hrudayavantha
{{Infobox film|
| name = Hrudaya Geethe
| image = 
| caption =
| director = P. Vasu
| writer = P. Vasu Vishnuvardhan  Nagma   Anu Prabhakar 
| producer = K. Manju
| music = Hamsalekha
| cinematography = Ramesh Babu
| editing = P. R. Soundar Raj
| studio = Lakshmishree Combines
| released = October 2, 2003
| runtime = 155 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada drama drama film directed and written by P. Vasu and produced by K. Manju. The film features Vishnuvardhan (actor)|Vishnuvardhan, Nagma and Anu Prabhakar in the lead roles.  The film met with average reviews upon release and failed at the box office.  The soundtrack and score was composed by Hamsalekha.

== Cast == Vishnuvardhan 
* Nagma
* Anu Prabhakar 
* Srinath
* Doddanna 
* Shobharaj
* Renuka Prasad
* Pragathi
* Shivaram
* Rangayana Raghu
* Ramesh Bhat
* Mandya Ramesh

== Soundtrack ==
The music of the film was composed and written by Hamsalekha. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Anna Needore
| extra1 = S. P. Balasubrahmanyam 
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Jhumu Jhumu
| extra2 = S. P. Balasubrahmanyam , K. S. Chithra
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Thangiye Thangiye
| extra3 = S. P. Balasubrahmanyam
| lyrics3 =Hamsalekha
| length3 = 
| title4 = Annayya Hrudayavantha
| extra4 = Rajesh Krishnan, K. S. Chithra
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Ghama Ghama
| extra5 = Mano (singer)|Mano, K. S. Chithra, Anuradha Sriram
| lyrics5 = Hamsalekha
| length5 = 
| title6 = Madhu Magalagi
| extra6 = S. P. Balasubrahmanyam
| lyrics6 =Hamsalekha
| length6 =
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 