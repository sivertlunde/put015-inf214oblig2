The Corsican Brothers (1917 film)
{{Infobox film
| name = The Corsican Brothers
| image =
| caption =
| director = André Antoine
| producer = Alexandre Dumas (novel)
| starring = Henry Krauss Romuald Joubé Rose Dione
| music =
| cinematography = Paul Castanet
| editing =
| studio = Pathé Frères
| distributor = Pathé Frères
| released =   
| runtime =
| country = France French intertitles
| budget =
| gross =
}} silent adventure film directed by André Antoine and starring Henry Krauss, Romuald Joubé and Rose Dione.  It is based on the novel The Corsican Brothers by Alexandre Dumas.

==Cast==
* Henry Krauss as Dumas père 
* Romuald Joubé 
* Rose Dione
* Jacques Grétillat
* Henry Roussel 
* Gaston Glass 
* Philippe Garnier 
* André Brule 
* Max Charlier

== References ==
 

==Bibliography==
* Levine, Alison. Framing the Nation: Documentary Film in Interwar France. A&C Black, 2011.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 

 