Fish Above Sea Level
{{Infobox film
| name           = Fish Above Sea Level
| image          = Fish Above Sea Level poster-fasl.jpg
| caption        = Theatrical release poster
| director       = Hazim Bitar
| producer       = Hazim Bitar
| writer         = Hazim Bitar
| starring       = Abdallah Dghemaal Rabee Zureikat
| music          = Aziz Maraqa
| cinematography = Hazim Bitar
| editing        = Hazim Bitar
| studio         = Tashkeel Films
| distributor    = Hazim Bitar (JO)
| released       =  
| runtime        = 80 minutes
| country        = Jordan
| language       = Arabic  English Subtitles
}}
Fish Above Sea Level is an upcoming drama film directed and written by Hazim Bitar. Leading Jordanian film critic Adnan Madanat called the film "The best Jordanian feature film."

== Synopsis ==
Fish Above Sea Level is about Talal (Rabee Zureikat) a young Ammani professional, who discovers upon his fathers death that he is penniless. His late father, in an attempt to shore up his failing investments, mortgaged the family house. Talal has few days to save the family house in Amman from foreclosure. His only way out is to sell a farm house once owned by his late father in a village by the Dead Sea with a dark past. At the village he meets Dawoud (Abdallah Dghemaat) a young farmer who lives in the farm house. Before Talal can do anything with the farm house, Dawouds approval is essential.

== Main Film Festivals ==
* African Diaspora Film Festival 2011
* Hollywood Black Film Festival 2011

== External links ==
*   
*  
*   on Facebook
*  
*  
*  
*  
*  
*  

 
 
 
 
 