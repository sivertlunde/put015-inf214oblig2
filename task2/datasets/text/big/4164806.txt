Bangaram (film)
{{Infobox film name           = Bangaram caption        = image          = bangaramposter.jpg writer  Dharani  (story & screenplay)  Siva Akula  (Dialogues )  starring       = Pawan Kalyan Meera Chopra Reemma Sen Ashutosh Rana Mukesh Rishi Raghu Babu director  Dharani
|producer       = A.M. Ratnam distributor    = Sri Surya Movies cinematography = Gopinath editing        = V. T. Vijayan released       =   runtime        = 178 minutes country        = India language       = Telugu music  Vidyasagar
|budget         =
}} Telugu film which released on 3 May 2006 and was directed by Tamil director Dharani (director)|Dharani. This film has Pawan Kalyan in the lead role and Meera Chopra, Raja Abel, Reema Sen, Ashutosh Rana, Mukesh Rishi, and  playing the supporting roles.The movie didnt do well at the box office.

== Plot ==
Bangaram(pavan kalyan) is a reporter at a news channel. He dreams of getting into an international news channel of BBC. To fulfill his own dream he ends up at Peddi Reddy (Mukesh Rishi) who is a factionist. He needs Peddis signature to get his dream fulfilled but in the meanwhile he gets entangled with Peddi"s daughter Sandhya(Meera chopra)and Vindhya(Sanusha). Later Bhooma Reddy(Ashutosh Rana) wins over Peddis heart and is able to convince him to marry Sandhya to him. But Sandhya has already got her own love story with Vinay(Raja).

Bangaram feeling guilty about foiling Sandhyas escape plan and the mean looks from Vindhya, he finally decides to help her by taking her to the city. But then the villains are not too far behind. Later Vinay also loses contact with them because of the confusion caused by a bomb blast. Then Bangaram is able to find Vinay with the help of a driver(Raghu babu) and his company. but just then Bhooma kidnaps Vindhya(who just turns 18) and blackmails Bangaram that he will marry her if Sandhya is not returned. But he later goes to Bhoooma and is able to save Vindhya and kill Bhooma which forms the climax.

In the end it is revealed that, Bangaram finds his own love( Trisha Krishnan ) in a train journey.

==Cast==
* Pawan Kalyan ... Bangaram
* Meera Chopra ... Sandhya
* Meera Jasmin...Vindhya
* Raghu Babu ... Driver
* Reema Sen ... Reporter
* Ashutosh Rana ... Bhooma Reddy
* Mukesh Rishi ... Peddi Reddy
* Raja Abel ... Vinay Venu Madhav

==Soundtrack==
{{Infobox album |  
| Name       = Bangaram
| Type       = Soundtrack Vidyasagar
| Cover      = Bangaram ACD.jpg
| Released   = 16 March 2006
| Recorded   = 2005-2006
| Genres     = World Music
| Length     =
| Label      = Aditya Music Vidyasagar
| Last album = Sivappathigaram (2006)
| This album = Bangaram (2006)
| Next album = Pasa Kiligal (2006)
}} Vijay attended this function as guest. 
{| class="wikitable" style="width:70%;"
|-
! Song Title !! Singers !! Duration !! Lyricist
|-
| Jai Shambo Shambo ||  
|-
| Yegire Chilakamma||  
|-
| Ra Ra Bangaram ||  
|-
| Maro Masti Maro ||  
|-
| Ra Ra Bangaram (Remix) || Pravin Mani,  
|-
| Chedugudante || KK (singer)|KK,  Anuradha Sriram, Sahithi, Dharani || 04:19 || Sahithi
|}

==References==
 

==External links==
* 
* 
*  

 
 

 
 
 