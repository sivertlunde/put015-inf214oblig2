Always Another Dawn
 
 
{{Infobox film name           = Always Another Dawn image          =  caption        =  producer       = T.O. McCreadie A.K. McCreadie (executive) director       = T.O. McCreadie  writer         = Zelma Roberts T.O. McCreadie based on       = novel by Zelma Roberts starring       = Charles Tingwell Guy Doleman music          = Wilbur Sampson cinematography = Harry Malcolm editing        = Alex Ezard studio          = McCreadie Brothers Embassy Pictures  distributor    = Universal Pictures (Aust) Eroc Films (UK) released       = 24 September 1948 runtime        = 108 mins (Aust) 73 mins (UK) country        = Australia language       = English  budget         = ₤30,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 206. 
}}
Always Another Dawn is a 1948 Australian wartime melodrama directed by T.O. McCreadie. It was the first leading role for Charles Tingwell. 

==Synopsis==
Terry Regan, from Camden, New South Wales, is the son of Molly Regan and a naval officer who died in action in 1916. Terry is called up to serve in the navy during World War II, and turns down a commission in order to see action early. He becomes friends with fellow sailor Warren and serves in the Mediterranean on HMAS Dauntless for two years. While home on leave he falls in love with his neighbours daughter, Patricia, and they plan to marry on his next leave. Dauntless is attacked and sunk by the Japanese during the Battle of the Java Sea; Terry is killed but Warren is among a handful of survivors. He goes to visit Molly and they talk about Terry.

==Cast==
*Charles Tingwell as Terry Regan
*Guy Doleman as Warren Melville
*Queenie Ashton as Molly Regan
*Betty McDowall as Patricia
*Douglas Herald as the Commodore
*Charles Zoli as Scotty McGrath
*Russell Jarrett as Dixie Dean
*Max Gibb as Bill Carson
*Brian Farmer as Kanga Campbell
*William J Mason as James T Henderson
*Norton Howarth as tanker captain
*David Lowe as tanker mate
*Glenn Clark as first merchant seaman
*Michael Brand as second merchant seaman
*Kevin Gunn as postman
*Frank Waters as Commander Regan
*Terrence Coy as small boy
*officers and men of the Royal Australian Navy

==Production==
The script was co-written by New Zealand author Zelma Roberts whose husband had been killed on active service with the New Zealand armed forces. 

It was Charles Tingwells first lead role and only his second film. Terrence Coy, who plays Tingwell as a boy, won his role in a competition. 

Although the ship in the film, Dauntless, was fictitious, it is based on the real-life  , which was sunk by the Japanese in 1942 with only 13 survivors. 

The film was made with the co-operation of the Royal Australian Navy. Shooting began in February 1947 and lasted six months, taking place at Flinders Naval Depot, Camden, New South Wales|Camden, and aboard the destroyer  . A small studio was provided by Commonwealth Film Laboratories. During filming of the final battle in Port Phillip Bay, £300 went missing from the Bataan which represented payroll for the film crew.  

Post production took another four months. 

==Release==
A novel was published in 1948. 
===Box Office===
The film only lasted in Sydney cinemas for two weeks but a shortened version was released in England. 
===Critical===
Critical reception was not strong, the critic from the Sydney Morning Herald claiming that "the dialogue is stilted and unreal, character development is inadequate and stodgy, and the tale is not crystallised in terms of fluent camera action."  The Argus thought "the film scores in its camera work - and in being so close to home. The handling of its rather tragic story and its efforts to introduce comedy are not quite so impressive. Charles Tingwell and Gus Doleman are interesting male leads." 

==References==
 

==External links==
* 
*  at Oz Movies
*  - discusses making of the film 
*  at National Film and Sound Archive

 
 
 
 