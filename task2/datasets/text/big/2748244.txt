Yeh Raaste Hain Pyaar Ke
 
{{Infobox film
| name           = Yeh Raaste Hain Pyaar Ke
| image          = Yehraaste.jpg
| writer         = Deepak Shivdasani
| starring       = Ajay Devgan Madhuri Dixit Preity Zinta
| director       = Deepak Shivdasani
| producer       = Deepak Shivdasani Pradeep Sadarangani
| distributor    = Tips Industries
| released       = 10 August 2001
| runtime        =
| language       = Hindi
| music          = Sanjeev Darshan
| awards         =
| budget         =
}}

Yeh Raaste Hain Pyaar Ke ( ,   romantic drama film. It is a love triangle starring Ajay Devgan, Madhuri Dixit and Preity Zinta directed by Deepak Shivdasani. 

== Synopsis ==

Two con artistes and car thieves, Vicky (Ajay Devgan) and Sakshi (Preity Zinta), are faced with death when they accidentally kill Bhanwarlals (Deep Dhillon) brother. Bhanwarlal and his other brother swear to avenge the death of their brother and mistakenly kill Rohit Verma (Ajay Devgan), is a look-alike of Vicky.

The mistaken identity causes Sakshi to think that Vicky is dead and she is devastated. Meanwhile unknown to Sakshi and Bhanwarlal, Vicky is alive and reaches Manali where he is constantly confused for Rohit. On discovering Rohits wealth Vicky realizes that he has hit the jackpot and decides to play along. Soon Rohits dad, Pratap Verma (Vikram Gokhale), arrives home to find Vicky in his bed.

Vicky tries to trick Pratap Verma, but he already knows that Rohit is dead. He convinces Vicky to pose as Rohit for the sake of his daughter-in-law, Neha (Madhuri Dixit) who is in denial/shock to the fact that her husband died on the same day that they got married.  He agrees to do the job for money. However, half way through the job he decides to grab the money and returns to Sakshi. They are about to forget all about Rohit and start a life of their own, when Vicky discovers that he is responsible for Rohits death since Bhanwarlal meant to kill him and not Rohit. He realizes the debt he owes to Rohit and decides to return, leaving Sakshi once again.

Meanwhile Sakshis uncle and aunt try to get her married off to Bhanwarlals youngest brother. She runs away to Manali to be with Vicky.  Vicky at this point cant tell Neha the truth and so tells Sakshi that he cant be with her. Soon, with the arrival of Bhanwarlal and Sakshis aunt and uncle, the truth unravels. Finally, Neha realizes that her husband is actually dead and she accepts the reality. Vicky and Sakshi get back together. It is implied that Neha may find happiness again and start a new life with her childhood buddy Sagar (Sunny Deol, in a cameo) who loved her from the beginning.

== Cast ==
* Ajay Devgan ...  Vicky/Rohit Verma
* Madhuri Dixit ...  Neha
* Preity Zinta ...  Sakshi
* Vikram Gokhale ...  Pratap Verma
* Deep Dhillon ...  Bhanwarlal
* Smita Jaykar    ..... Nehas Mother
* Sunny Deol ...  Sagar (Special Appearance)

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mera Dil Ek Khali Kamra"
| Kumar Sanu, Anuradha Paudwal
|-
| 2
| "Jo Pyaar Karta Hai"
| Manohar Shetty, Anuradha Paudwal, Kavita Krishnamurthy
|-
| 3
| "Yeh Raaste Hain Pyaar Ke"
| Shaan (singer)|Shaan, Jaspinder Narula
|-
| 4
| "Aaja Aaja"
| Asha Bhosle
|-
| 5
| "Bam Bhole"
| Vinod Rathod, Alka Yagnik
|-
| 6
| "Yeh Dil Mohabbat Mein"
| Udit Narayan, Alka Yagnik
|-
| 7
| "Halle Halle"
| Vinod Rathod, Alka Yagnik
|-
| 8
| "Khoya Khoya Chand Hai"
| Udit Narayan
|}

==References==
 

== External links ==
*  

 

 
 
 