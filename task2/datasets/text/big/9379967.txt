The Deer's Bell
{{Infobox Film
| name = The Deers Bell
| image = DeersBell.jpg
| caption = 
| director = Wu Qiang, Tang Cheng
| producer = 
| writer = Sang Hu 
| starring =
| music = 
| cinematography =
| editing = 
| distributor = 
| released = 1982
| runtime = 20 mins
| country = China Mandarin
| budget = 
| preceded_by = 
| followed_by = 
}}
 Chinese animated film produced by Shanghai Animation Film Studio.  It is also referred to as "Bell on a Deer" and "Lu Ling".

==Story==
An old man and his grand daughter rescued an injured young deer.  The girl and the young deer would develop a close relationship.  After the recovery, the deer returned to the wild.  Feeling sad the girl put a bell on the deer.  Disappearing into the mountain valley, the deers bell continues to echo. 

==Awards==
* The film won best animation in the 1983 Golden Rooster Awards. 
* Awarded the best fine arts piece prize in 1983 by Chinas Ministry of Culture.
* Won the outstanding movie prize at the 13th Moscow International Film Festival animated cartoon special prize in 1982.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 
 