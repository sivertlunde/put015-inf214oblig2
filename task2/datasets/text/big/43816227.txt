Les Enfants du placard
 
{{Infobox film
| name           = Les Enfants du placard
| image          = 
| caption        = 
| director       = Benoît Jacquot
| producer       = Michel Chanderli
| starring       = Brigitte Fossey Lou Castel Jean Sorel Georges Marchal Danièle Gégauff
| screenplay     = Benoît Jacquot
| based on       = 
| cinematography = Pierre Lhomme
| editing        = Fanette Simonet
| distributor    = MK2 Diffusion Artificial Eye
| released       =    
| runtime        = 
| country        = France
| language       = French
| budget         = 
| gross          = 
}} drama directed by Benoît Jacquot.

==Cast==
* Brigitte Fossey as Juliette
* Lou Castel as Nicola
* Jean Sorel as Berlu
* Georges Marchal as the Father
* Danièle Gégauff as the Mother
* Christian Rist as Julien
* Isabelle Weingarten as Laure
* Hassane Fall as Diop
* Sophie Baragnon as Young Juliette
* Vincent Balvet as Young Nicola 
* Martine Simonet as the Prostitute

==Accolades==
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient !! Result
|-
| 1978
| César Award Best Actress
| Brigitte Fossey
|  
|-
|}

==References==
 

==External links==
*  

 
 
 

 
 