Hote Hote Pyar Ho Gaya
{{Infobox film
| name = Hote Hote Pyar Ho Gaya
| image =HHPHG.jpg
| image_size =
| caption = DVD cover
| director = Firoz Irani
| producer = Firoz Irani
| writer = Anil Kalekar
| narrator =
| starring =Jackie Shroff Kajol Atul Agnihotri
| music = Anand Raj Anand,  Pradeep Laad,  Naeem Ejaz Rani Malik (lyrics)
| cinematography = Damodar Naidu
| editing = Hussain Burmawala
| distributor =
| released = 2 July 1999
| runtime =
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 Indian Bollywood film directed by Firoz Irani, released on 2 July 1999. The film stars Jackie Shroff, Kajol and Atul Agnihotri in lead roles.

==Plot==
Pinky (Kajol) and Bunty (Atul Atul Agnihotri|Agnihotri) are in love with each other, and plan to marry. However, their respective families strongly oppose to this marriage, as their life-partners have already been picked for them. Pinkys dad, (Kulbhushan Kharbanda) has plans her to get married to Arjun (Jackie Shroff); while Buntys parents, have picked Shobha (Ayesha Jhulka) as his bride. Bunty and Pinky decide to accept their parents choices, and then eventually divorce their respective partners. Both of them get married by their parents will. Afterwards, both Pinky and Bunty try to make their best to ruin their own family life. Their plans seem to be going on track, until they come to know that Shobha and Arjun are meeting on the sly but then they find out and plan a divorce and pinky and bunty marry each other

==Cast==
*Jackie Shroff ... Police Officer Arjun
*Kajol ... Pinky
*Atul Agnihotri ... Atul (Bunty)
*Ayesha Jhulka ... Shobha
*Aruna Irani ... Buaji
*Kulbhushan Kharbanda ... Colonel (Pinkys dad)
*Prem Chopra ... Jagawar
*Rita Bhaduri ... Asha
*Anil Dhawan ... Ajit
*Anjana Mumtaz ... Arjuns mom

== Music ==
{{Infobox album|  
| Name = Hote Hote Pyar Ho Gaya
| Type = Album
| Artist = Anand Raj Anand, Pradeep Laad
| Cover =
| Released =   1999 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =
| Producer = Anand Raj Anand, Pradeep Laad
| Reviews =
| Last album =
| This album =
| Next album =
}}

The soundtrack of the film contains 7 songs. The songs were composed by Anand Raj Anand and Pradeep Laad, with the song Haiyo Hikko Nikko Ni being composed in collaboration with Naeem Ejaz. Lyrics were authored by Rani Malik, with the song O Jane Jaa being written by Zameer Kazmi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
| Haiyo Hikko Nikko Ni Kavita Krishnamurthy
|-
| Haiyo Hikko Nikko Ni Anand Raj Anand
|-
| Hote Hote Pyaar Ho Gaya Alka Yagnik & Abhijeet Bhattacharya
|-
| Jab Tum Mere Kumar Sanu
|-
| Laddu Motichur Ka Alka Yagnik, Poornima
|-
| O Jane Jaa Udit Narayan, Sadhana Sargam
|-
| Pyar Wale Rang
|K. S. Chitra
|}

== External links ==
* 

 
 
 