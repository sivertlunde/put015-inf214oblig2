Hidalgo (film)
{{Infobox film name = Hidalgo image = Hidalgo_film.jpg caption = Theatrical release poster director = Joe Johnston writer = John Fusco starring = Viggo Mortensen Omar Sharif Saïd Taghmaoui producer = Casey Silver music = James Newton Howard cinematography = Shelly Johnson editing = Robert Dalva studio = Carey Silver Productions distributor = Touchstone Pictures budget = $40 million gross = $108.1 million
|released=  runtime = 136 minutes
|country= United States language = Arabic
}} mustang Hidalgo. It recounts Hopkins racing his horse in Arabia in 1891 against Bedouins riding pure-blooded Arabian horses. The movie was written by John Fusco and directed by Joe Johnston. It stars Viggo Mortensen, Zuleikha Robinson, and Omar Sharif.

==Plot summary==
In 1891, American Frank Hopkins (Viggo Mortensen) and his mustang, Hidalgo, are part of  Buffalo Bills Wild West show, where they are advertised as "the worlds greatest distance horse and rider". Hopkins had been a famous distance rider, a cowboy, and a dispatch rider for the United States government; in the latter capacity he carried a message to the U.S. 7th Cavalry Regiment authorizing the Wounded Knee Massacre of Lakota Sioux. 

Wealthy Sheikh Riyadh (Omar Sharif) has sent his attaché Aziz (Adam Alexi-Malle) to ask the show to either stop using the phrase "the worlds greatest distance horse and rider" or allow Hopkins and Hidalgo to prove themselves by entering the Middle Eastern "Ocean of Fire" race:
an annual 3,000-mile survival race across the Najd desert region. The Sheikh is custodian of the 
al Khamsa|al-Khamsa line, considered to be the greatest distance horses in the world, and traditionally the race has been restricted to pure-bred Arabian horses and Bedouin or Arab riders.
 
In addition to the grueling conditions, prevailing animosity and contempt for an infidel and “impure” horse, horse and rider face stiff competition, including the wealthy and unscrupulous British horse breeder Lady Anne Davenport (Louise Lombard).

To complicate matters, Sheikh Riyadh has promised his daughter Jazira (Zuleikha Robinson), his only surviving child, in marriage to the rider of his horse, should he win. She hopes to prevent this by giving Hopkins advice and information to help him win, thereby resulting in greater danger for them both. Sheikh Riyadhs outcast brigand nephew, who will stop at nothing to gain control of the al-Khamsa line, raids the race camp, kidnaps Jazira, and threatens to kill her unless he gets his uncles prize stallion racer as her ransom. Hopkins manages to rescue Jazira. However, Davenport and the Sheikhs nephew try to sabotage the race by eliminating the rival riders, but are thwarted when Hopkins kills the nephew.

For Hopkins the Ocean of Fire becomes not only a matter of pride, honor and survival, but of identity as well: it emerges that his father was European American while his mother was Lakota Sioux. The Lakota call him “Blue Child” or “Far Rider.” As a Mixed-race|half-breed he feels sympathy and pity for his mothers people, but does not generally reveal his heritage, especially after the Wounded Knee massacre, for which he feels partly responsible. Jazira compares his relation to his heritage to her desire to avoid wearing the sartorial hijab|veil, saying that he mustnt "go through life hiding what God made you.... like me." 

Nearing the end of the race, Hidalgo is severely injured and Hopkins is dying of thirst; hallucinating, he sings a prayer to Wakan Tanka as his death song. But suddenly Hidalgo struggles up, and Hopkins rides bareback to win the race. Hopkins wins the respect and admiration of the Arabs, and becomes friends with the Sheikh, giving him his revolver as a gift.
 Native Americans to convert to farming. Hopkins has the horses released and frees Hidalgo to join them in the wild. The epilogue states that Hopkins went on to reportedly win 400 long-distance races and was an outspoken supporter for wild mustangs until his death in 1951. Hidalgos descendants live free in the wild.

==Cast==
*Viggo Mortensen, as Frank Hopkins, a noted long-distance rider.
*Zuleikha Robinson, as Jazira, determined daughter of the Sheikh.
*Omar Sharif, as Sheikh Riyadh, an insightful leader. He owns and breeds Arabian horses of the Al Khamsa bloodline.
*Louise Lombard, as Lady Anne Davenport, British aristocrat who bets her horse against Hopkins
*Adam Alexi-Malle, as Aziz, emissary and the Sheikhs attaché.
*Saïd Taghmaoui, as Prince Bin Al Reeh, Jaziras cousin who wishes to marry her by force
*Silas Carson, as Katib.
*Harsh Nayyar, as Yusef.
*J.K. Simmons, as Buffalo Bill Cody.
*Adoni Maropis, as Sakr.
*Victor Talmadge, as Rau Rasmussen.
*Peter Mensah, as Jaffa, Jaziras personal guard.
*Joshua Wolf Coleman, as The Kurd.
*Franky Mwangi, as Slave Boy.
*Floyd Red Crow Westerman, as Chief Eagle Horn, a member of Hopkins Lakota nation, performing with him in the Wild West Show. Elizabeth Berridge, as Annie Oakley.
*C. Thomas Howell, as Preston Webb.
*David Midthunder, as Black Coyote.
*Malcolm McDowell, as Major Davenport (uncredited).

==Production==
Actor Viggo Mortensen, who is fluent in Spanish, voiced his own character (Frank Hopkins) in the Spanish dubs of the film.

==Fact and fiction==
The Native American historian Vine Deloria questioned Hopkins claims of Lakota ancestry, as presented uncritically in the film. {{cite web
|url=http://www.independent.co.uk/news/world/americas/disney-rides-into-trouble-with-story-of-cowboy-who-conquers-the-middle-east-565827.html
|title=Disney rides into trouble with story of cowboy who conquers the Middle East
|publisher=The Independent 
|accessdate=2011-08-14
|date=10 March 2004
|author=Andrew Gumbel
|quote=Vine Deloria of the University of Colorado, is furious at the uncritical repetition of Hopkins claims about his role in Sioux history. He wrote: "Hopkins claims are so outrageously false that one wonders why Disney were attracted to this material at all, except of course the constant propensity to make money under any conditions available."
}}  {{cite web
|url=http://www.thelongridersguild.com/deloria.htm
|title=Dr. Vine Deloria Jr. denounces Frank Hopkins as a fraud
|publisher= Longriders Guild
|quote=Hopkins claims are so outrageously false that one wonders why the Disney people were attracted to this material at all....Try this on for size - Hopkins claimed to be the grandson of Geronimo who, he confided, was really a Sioux and not an Apache at all. 
Hopkins, according to himself and wife, was very popular with the Indians because he was half Sioux himself, his mother being a lady called Nah-Kwa - her more formal name was Valley Naw-Kwa or "Valley of Silence" - hardly fitting for a woman who had such illustrious relatives. Hopkins spoke "the Indian language" so he was a natural interpreter for the Army - although his name does not appear on any treaty documents where the interpreters are listed or in any correspondence in government files wherein interpreters were needed. 
}} 

But, Nakota filmmaker Angelique Midthunder said during the controversy that "the story of the half Indian who took his pinto mustang across the sea to race in the big desert has been told to children of the northern plains tribes for generations."  , Official Website   Lakota elder Sonny Richards writes, "Kaiyuzeya Sunkanyanke (Frank Hopkins) was a South Dakota native and Lakota half-breed."   

Based on Hopkins account of his mixed-race ancestry, the movie production employed Lakota historians, medicine men, and tribal leaders as consultants to advise during every scene that represented their culture. Many of the Ghost Dancers who reenact the sacred ceremony of 1890 in Hidalgo had participated in the film Thunderheart (1992) and the mini-series Dreamkeeper, both written by Fusco. The screenwriter was adopted as ah honorary relative of the Oglala Nation in a Hunkyapi ceremony (Making of Relatives) on September 3, 1989 on the Pine Ridge Indian Reservation.  

Because the Disney Corporation marketed the movie as a true story, some historians criticized the film both because of the legendary status of Hopkins claims and for the films divergence from his accounts.  , The Longriders Guild  They contend that many of the events, especially the featured race, never took place.  Historians of distance riding said that most of Hopkins claims as depicted in the film, including the race, have been tall tales or hoaxes.  , The Longriders Guild  
 Blackjack Mountain in Oklahoma. By Hopkins original account, he decided to leave his horse in Arabia after the race. 

In 2006, John Fusco, the screenwriter of Hidalgo, responded to criticism about the historical basis of the film. He had done research on Hopkins for years. He said that he used parts of Hopkins 1891 desert memoirs (unpublished during the riders lifetime) and "heightened the Based On story to create an entertaining theatrical film."  He held that the story of the man and his horse is true. Fusco offered quotes from surviving friends of Hopkins, notably former distance riders Walt and Edith Pyle, and Lt Col William Zimmerman, along with information found in horse history texts, as verification.  

According to the Longriders Guild, the Yemen Government, United Arab Emirates and Saudi Arabian Government say officially that there has never been an "Ocean of Fire" race. Hopkins never named the event; he referred to it in his writings as an annual ceremonial ride in the region.   According to the Arab historian Dr. Awad al-Badi, such a lengthy race was impossible. He said,
 "There is no record or reference to Hopkins with or without his mustangs ever having set foot on Arabian soil. The idea of a historic long-distance Arab horse race is pure nonsense and flies against all reason. Such an event in Arabia any time in the past is impossible simply from a technical, logistical, cultural and geopolitical point of view. It has never been part of our rich traditions and equestrian heritage."  , Arab News, 13 May 2003, accessed 2010-12-28  

==Reception==
===Critical response===
The movie received mixed reviews from mainstream critics, receiving a 47% approval rating from Rotten Tomatoes and a 54 from Metacritic.

Roger Ebert offered a positive review of the film (three out of four stars), saying its "Bold, exuberant and swashbuckling," the kind of fun, rip-snorting adventure film Hollywood rarely makes anymore.  He added, "please ignore any tiresome scolds who complain that the movie is not really based on fact. Duh." 

==Honors==
*John Fusco won the Spur Award for Best Western Drama Script; although most of the plot of Hidalgo was not set in the American West, it featured an American cowboy figure.

===Box office===
*  67,303,450
*Other International Earnings: 	 $40,800,000
*Gross Worldwide Earnings: 	 $108,103,450

==The horses== American Paint horses were used to portray Hidalgo.  The actor Viggo Mortensen later bought RH Tecontender, one of the horses used in the film. The screenwriter John Fusco bought Oscar, the main stunt horse, and retired him at Red Road Farm, his American Indian horse conservancy.

==References==
 

==External links==
* 
* 
* 
* 
* , Official Website sponsored by John Fusco, has articles about the Hidalgo film, including Fuscos responses to criticism.
* , Red Road Farm Official Website

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 