My Name Is David (film)
 
{{Infobox film
| name = My Name Is David
| image = 
| director = Chris Gallego Wong
| producer = Keith Powell, Jonathan Whittaker
| writer = Howard Emanuel, Keith Powell
| starring = {{plainlist|
* Keith Powell
* Judy Reyes
* Adepero Oduye
* Otto Sanchez
* Anthony Chisholm
* Rich Sommer
* Scarlett Rose Giuliano
* Serafina Ivy Giuliano
}}
| cinematography = Mattia Palombi
| editing = Chris Gallego Wong, Jose Rodriguez Robles
| music = Giancalro Vulcano
| distributor = 
| released =  
| country = United States
| language = English
| gross    =
}}
My Name Is David is an American drama film produced by Keith Powell and Jonathan Whittaker, written by Howard Emanuel and Keith Powell, and directed by Chris Gallego Wong. The film stars Keith Powell as David, a prescription drug addict who finds an abandoned baby on the subway.  The film also stars Judy Reyes, Adepero Oduye, Otto Sanchez, Anthony Chisholm, and Rich Sommer.  It had its premiere at the San Luis Obispo International Film Festival in March 2015.  As a narrative device, none of the characters are named, with the exception of the main protagonist, David.

==Plot==
David Howard, an obsessive-compulsive workaholic with a severe addiction to prescription drugs, struggles to find order in a world full of chaos. Desperately in search of a life better than the one he leads, David often relies on his neighbor, a hot-tempered single mother trying to make ends meet.

Returning home on the subway late one night, David finds an abandoned baby—which could be the key to his happiness, or the catalyst that spirals his world completely out of control.

==Production== Red One camera.  Shooting took 18 days during the summer of 2013.  

==Release==

===Festivals===
My Name Is David was selected to screen at the following film festivals:

*2015 San Luis Obispo International Film Festival 
*2015 Newport Beach Film Festival

==Reception==

 

==References==
 

 