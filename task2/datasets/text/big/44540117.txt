Nan Hendthi Chennagidale
{{Infobox film
| name           = Nan Hendthi Chennagidale
| image          = 
| caption        = 
| director       = Dinesh Babu
| producer       = Chithra
| writer         = Dinesh Babu Vishnuvardhan (Special Ramesh Ramakrishna Sharan Manadeep Roy Ramesh Bhat
| music          = Rajesh Ramanath
| cinematography = P. K. H. Das
| editing        = B. S. Kemparaj
| studio         = Bhavani Chithra
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 2000 Indian comedy film in Kannada language starring Kashi, Anjani Takkar and Ananth Nag in the lead roles along with Vishnuvardhan (actor)|Vishnuvardhan,  and Ramesh Aravind in special appearances. The film is directed and written by Dinesh Babu. 

==Cast==
* Kashi
* Anjani Takkar
* Ananth Nag
* K. S. Ashwath
* Vishnuvardhan (actor)|Vishnuvardhan...special appearance
* Ramesh Aravind...special appearance Ramakrishna
* Sharan
* Umashri
* Chitra Shenoy
* Doddanna
* M.N Lakshmi Devi
* Tennis Krishna

==Release==
The film was released on March, 2000 all over Karnataka. The film met with average and negative critical response at the box-office.

==Soundtrack==
All the songs are composed and scored by Rajesh Ramanath. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|-
| 1 || "Nanna Hendthi" || Rajesh Krishnan || K. Kalyan
|-
| 2 || "Srirangadinda Srikrishna" || Nanditha || K. Kalyan
|-
| 3 || "Hunja Hunja" || Manjula Gururaj || K. Kalyan
|-
| 4 || "Maduve Madi Nodu" || Rajesh Krishnan || K. Kalyan
|-
| 5 || "Bhoomigondu Banna" || Rajesh Krishnan || K. Kalyan
|-
|}

==References==
 

== External links ==
* 

 
 
 
 
 


 

 