Memoirs of a Survivor (film)
 
{{Infobox film
| name           = Memoirs of a Survivor
| image          = Memoirs of a Survivor poster.jpg
| caption        = DVD cover
| director       = David Gladwell
| producer       = Penny Clark Michael Medwin
| writer         = David Gladwell Kerry Crabbe Doris Lessing
| starring       = Julie Christie
| music          = 
| cinematography = Walter Lassally
| editing        = Bill Shapter
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
 same name by Doris Lessing.

It is scheduled to be released on DVD in June 2014 by Network Distributing.   

==Cast==
* Julie Christie - D
* Christopher Guard - Gerald
* Leonie Mellinger - Emily Mary Cartwright
* Debbie Hutchings - June
* Nigel Hawthorne - Victorian Father
* Pat Keen - Victorian Mother
* Georgina Griffiths - Victorian Emily
* Christopher Tsangarides - Victorian Son
* Mark Dignam - Newsvendor
* Alison Dowling - Janet White
* John Franklyn-Robbins - Prof. White
* Rowena Cooper - Mrs. White
* Barbara Hicks - Woman on Waste Ground
* John Comer - Man Delivering Emily
* Adrienne Byrne - Maureen

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 