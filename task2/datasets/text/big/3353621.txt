Sarah Silverman: Jesus Is Magic
{{Infobox film
| name           = Jesus Is Magic
| image          = Jesus is Magic.jpeg
| image_size     =
| caption        = Sarah Silverman Liam Lynch
| producer       = Heidi Herzon Grant Jue Randy Sosin Mark Williams
| writer         = Sarah Silverman
| narrator       =
| starring       = Sarah Silverman Laura Silverman Brian Posehn Bob Odenkirk 
| music          = Liam Lynch Sarah Silverman
| cinematography = Rhet W. Bear
| editing        = Liam Lynch Showtime Visual Entertainment
| distributor    = Roadside Attractions
| released       =  
| runtime        = 72 min.
| country        = United States
| language       = English
| budget         =
}} comedy written Liam Lynch and distributed by Roadside Attractions. 
 flashbacks and comedic sketches. Silverman addresses a number of topics, including religion, AIDS, the Holocaust, racism|race, sexism, political parties, people with disability|disabilities, homeless people, and dwarfism|dwarves. Silverman also performs several original songs in the film. 

The film was released November 11, 2005 in eight theatres. Receiving positive reviews, it made just under $125,000 during opening weekend. Its performance led to an expanded release in as many as 57 theatres, resulting in a box office take of more than $1.2 million.  The movie was released on DVD on June 6, 2006 in the United States, June 13 in Canada, and October 13, 2008 in the United Kingdom. A soundtrack CD was also released featuring most of the musical numbers, excerpts from Silvermans stand-up comedy, and several additional songs which did not appear in the film. 

==Critical reception==
A. O. Scott of The New York Times wrote, "Most of the humor in "Jesus Is Magic" depends on the scandal of hearing a nice, middle-class Jewish girl make jokes about rape, anal sex, the Holocaust and AIDS. She makes fun of religion. She riffs on 9/11. But Ms. Silverman is not smashing taboos so much as she is desperately searching for them." 

PopMatters journalist J.C. Maçek III  wrote, "I would certainly recommend this film for at the very least, an attempt, especially if you like, or can handle jokes about Jesus, Aids, Hitler, 9/11 and more insults to African Americans than a skinhead pool party." 

==References==
 

==External links==
* 

 
 
 
 
 


 