Autumn Crocus (film)
{{Infobox film
| name = Autumn Crocus 
| image = "Autumn_Crocus"_(1934).jpg
| image_size =
| caption = Ivor Novello and Fay Compton
| director = Basil Dean
| producer = Basil Dean
| writer = Dodie Smith (play)   Dorothy Farnum    Basil Dean
| narrator =
| starring = Ivor Novello   Fay Compton   Muriel Aked   Esme Church  
| music = Ernest Irving  Robert Martin
| editing = Walter S. Stern
| studio = Associated Talking Pictures  Associated British  
| released = February 1934
| runtime = 86 minutes
| country = United Kingdom
| language = English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Autumn Crocus, West End hit for director Basil Dean.    The film was made at Ealing Studios, with art direction by Edward Carrick. It was the final film appearance of its star, Ivor Novello. A contemporary reviewer wrote, "Novellos schoolboy knees under his Tyrolean shorts make the audience, if not the players, feel bashful". 

==Cast==
* Ivor Novello as Andreas Steiner 
* Fay Compton as Jenny Grey 
* Muriel Aked as Miss Mayne 
* Esme Church as Edith 
* Frederick Ranalow as Herr Feldmann 
* Jack Hawkins as Alaric 
* Diana Beaumont as Audrey 
* Mignon ODoherty as Frau Feldmann 
* George Zucco as Reverend Mayne 
* Gertrude Gould as Frau Steiner 
* Alyce Sandor as Minna
* Pamela Blake as Lenchen

==Critical reception==
The New York Times wrote, "the wistful romance of the fading English schoolmistress and the cheerful Tyrolean inn-keeper drags in its telling, and this in the face of the presence of Fay Compton and Ivor Novello in the principal rôles and of Basil Deans direction."  

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 