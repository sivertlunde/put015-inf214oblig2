A Beautiful Valley
{{Infobox film
| name           = A Beautiful Valley
| image          = A Beautiful Valley Poster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Hadar Friedlich
| producer       = Yael Fogiel Yochanan Kredo Eylon Ratzkovsky Laetitia Gonzalez Guy Jacoel Yossi Uzrad
| writer         = Hadar Friedlich
| starring       = Batia Bar Hadar Avigad Gili Ben-Ozilio Eli Ben-rey Hadas Porat Ruth Geller
| music          = Uri Ophir
| cinematography = Talia Galon
| editing        = Hadar Friedlich Nelly Quettier
| studio         = July August Productions
| distributor    = Les Films du Poisson (France)
| released       =  
| runtime        = 86 minutes
| country        = Israel France
| language       = Hebrew
| budget         = 
| gross          = 
}}
A Beautiful Valley ( ) is a 2011 Israeli drama by Hadar Friedlich in her feature directorial debut.

The film premiered at the Jerusalem Film Festival in July 2011 where it received the award for Best Full-Length Debut. In a controversial decision by the festival management, however, the prize was revoked at the last minute following a complaint of alleged conflict of interest by one of the jury members.  It was also nominated for a 2011 Ophir Award in the Best Actress and Best Cinematography categories, received a Special Jury Mention at the San Sebastián International Film Festival,  and won the Critics Prize at the Cinémed Montpellier Film Festival. 

==Plot==
Hanna Mendelssohn (Batia Bar), an elderly widow, is a proud founding member of her kibbutz and has devoted most of her life to its development. When it is threatened with bankruptcy and Privatization|privatized, she is forced out of her job as the communitys gardener, turning from a hardworking productive member of society into a dependent burden.  Although she still believes in the values of social equality and cooperation that characterized the kibbutz in its early years, Hanna struggles to maintain her usefulness and sense of worth in a society undergoing a sudden and profound transformation.

==Cast==
*Batia Bar as Hanna
*Gili Ben-Ozilio as Yael
*Hadar Avigad as Naama
*Eli Ben-rey as Shimon
*Hadas Porat as Odeda
*Ruth Geller as Miriam

==Critical reception==
Jay Weissberg of Variety (magazine)|Variety said Friedlichs debut "tackles a forgotten subject with sensitivity". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 