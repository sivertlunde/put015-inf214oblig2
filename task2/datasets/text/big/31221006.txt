The Scorpion King 3: Battle for Redemption
{{multiple issues|
 
 
}}

{{Infobox film
| name = The Scorpion King 3:  Battle for Redemption
| image = Scorpion King 3 DVD Cover.jpg
| caption = DVD release cover
| director = Roel Reine
| producer = Leslie Belzberg
| screenplay =  Brendan Cowles Shane Kuhn
| story = Randall McCormick
| starring = Victor Webster Bostin Christopher Temuera Morrison Krystal Vee Selina Lo Kimbo Slice Dave Bautista Billy Zane Ron Perlman Trevor Morris
| cinematography = Roel Reine
| editing = Matthew Friedman Radu Ion Universal 1440 Entertainment
| released =  
| runtime = 105 minutes
| language = English
| country = United States
| budget =
}}
The Scorpion King 3: Battle for Redemption (previously known as The Scorpion King: Book of the Dead) is a 2012 direct-to-video film released on January 10, 2012. It is the third installment in The Scorpion King franchise and stars Victor Webster in the title role. This film continues the story of List of The Mummy characters#Mathayus (The Scorpion King)|Mathayus, after he becomes the Scorpion King at the end of The Scorpion King and focuses on Mathayus battling Talus and trying to stop him from claiming the Book of the Dead.

==Plot==
A handful of years after giving Mathayus the prophecy that his peaceful kingdom wouldnt last forever, Cassandra dies. Mathayus allows his kingdom to fall apart in the aftermath of the deadly plague which claimed his wifes life and he believes his reign of nobility to be over. Mathayus then becomes a mercenary once more, just as he was prior to his war with Memnon. Talus who is the younger brother of Horus, a powerful king of Egypt wishes to conquer his brothers kingdom since Horus was made the king over him. In order to do so, Talus along with an army goes to the Far East to steal the Book of the Dead from Ramusan, a king who is an ally of Horus. To stop Talus, Horus hires Mathayus and pairs him up with the Scandinavian warrior Olaf. In turn, Talus kidnaps Ramusan daughter, Silda. Ramusan then tells Mathayus that if he can save his daughter, he will have the right to wed her and once again raise a kingdom.

Before Mathayus manages to rescue Silda, she is whisked away by the ninja army of the mysterious "Cobra". Talus hires Mathayus and Olaf to bring back the princess as well as Cobras head. They wind up in an exiles camp lead by Cobra, who turns out to be Silda herself. Talus then arrives at Ramusans palace and takes the Book of the Dead. With this he reanimates the dead warriors Zulu Kondo, Agromael and Tsukai. In a test to see their strength, Talus orders them to kill his best men, which they do easily. Tsukai and Zulu Kondo are ordered to attack the exiles camp. Working together, Mathayus, Olaf and Sildas ninjas manage to defeat Zulu Kondo in battle. However, Tsukai manages to escape.

Mathayus and Olaf return to Ramusans palace, now Talus headquarters. They pretend to have rescued Silda and present a severed head supposedly belonging to Cobra. Talus still intends to marry Silda and takes her to his sleeping chambers. Mathayus attacks Talus, who is saved by the timely intervention of Tsukai. Mathayus pursues Talus while Silda faces Tsukai. At the same time, Olaf attempts to get the Book of the Dead but has to fight Agromael. While Talus faces the wrath of the ninjas, Mathayus somehow finds the ailing Ramusan and together they use the Book of the Dead to prevent Tsukai and Agromael from killing Silda and Olaf, respectively.

With Ramusan dying in his daughters arms and Talus left to face the wrath of the ninjas, Tsukai and Agromael bow down to Mathayus as the new ruler of Ramusan and Talus kingdoms. When Horus arrives at the city gates, he is greeted by Mathayus, who has taken up the mantle of Scorpion King once more.
 The Mummy and the Scorpion Kings own journey paved for the opening sequence of The Mummy Returns.

==Cast==
* Victor Webster as Mathayus, the Scorpion King
* Bostin Christopher as Olaf
* Temuera Morrison as King Ramusan
* Krystal Vee as Princess Silda
* Selina Lo as Tsukai
* Kimbo Slice as Zulu Kondo (as Kevin Kimbo Slice Ferguson)
* Dave Bautista as Agromael
* Billy Zane as King Talus
* Ron Perlman as Horus
* Brandon Cohen as Salim
* Kelly Hu as Cassandra (appears in flashbacks only; uncredited)

==Reception==
The Scorpion King 3: Battle for Redemption was met with mixed to negative critical reception.   gave the film a 7/10 rating and commented: "If I had to rank them, Id say The Scorpion King 3: Battle for Redemption is the least of the Scorpion King movies (theyve been subtly deteriotating since the first one), lacking as it does a dashing lead performance or a particularly involving storyline. but it’s still a damned fun B-movie, with likable characters and a series of entertainingly produced action sequences for fantasy movie fans of all stripes."   

==Sequel==
 
A fourth film in the franchise has been released, entitled  . Victor Webster reprised his role from the third film. Michael Biehn, Rutger Hauer, Lou Ferrigno, and former 2007 WWE Diva Search winner Eve Torres also joined the cast.  

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 