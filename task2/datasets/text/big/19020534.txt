Two Girls and a Sailor
{{Infobox film
| name           = Two Girls and a Sailor
| image          = Two-Girls-And-A-Sailor-1944.jpg
| image_size     = 225px
| caption        = Film poster
| director       = Richard Thorpe
| producer       = Joe Pasternak
| writer         = Richard Connell Gladys Lehman
| starring       = June Allyson Gloria DeHaven Van Johnson
| music          = Calvin Jackson George Stoll Robert Surtees
| editing        = George Boemler
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $1,420,000  . 
| gross          = $4,576,000 
}}
 1944 musical film about two singing sisters who are helped to set up a canteen to entertain soldiers by a mysterious wealthy admirer. It featured a host of celebrity performances, including Jimmy Durante doing his hallmark "Inka Dinka Doo", Gracie Allen, and Lena Horne. Richard Connell and Gladys Lehman were nominated for the Academy Award for Best Original Screenplay.

==Plot==
Two sisters, Jean and Patsy Deyo, are born into a vaudeville family, and when they grow up, start an act themselves. One night, they invite a bunch of servicemen to their apartment. They are both attracted to a sailor named Johnny. Jean points out to Johnny an unused nearby warehouse they wish they could make into a canteen to entertain the troops. 

An anonymous benefactor they call "Somebody" starts fulfilling that goal. First, a Mr. Nizby shows up and hands them the keys to the warehouse, announcing they now own it. As the two sisters explore the dusty building, they discover that Billy Kipp, an old vaudeville performer they knew as kids, has been squatting there ever since his wife left him and took their infant son many years ago. A horde of cleaners tidy up, and the place is made into an inviting canteen, all courtesy of "Somebody". Famous entertainers perform, as do Jean and Patsy.

Johnny starts dating Jean, unaware that Patsy is also in love with him. Meanwhile, Patsy tries to discover who "Somebody" is. Finally, she learns that it is none other than Johnny. It also turns out that Johnny is in love with Patsy, and Jean with Sergeant Frank Miller, but both did not want to hurt the other. Everything gets straightened out in the end. To top it off, Billy spots a sailor who looks just like a younger version of himself, down to his nose. He and his son are joyfully reunited.

==Cast==
  
* June Allyson as Patsy Deyo
* Gloria DeHaven as Jean Deyo
* Van Johnson as John Dyckman Brown III
* Tom Drake as Sergeant Frank Miller
* Henry Stephenson as John Dyckman Brown I
* Henry ONeill as John Dyckman Brown II
* Frank Jenks as Dick Deyo
* Donald Meek as Mr. Nizby Harry James and his Music Makers as themselves
* Ben Blue as himself
* Carlos Ramírez as himself
  Albert Coates as himself
* Amparo Iturbi as herself
* José Iturbi as himself
* Jimmy Durante as Billy "Junior" Kipp
* Gracie Allen as herself
* Lena Horne as herself
* Virginia OBrien as herself The Wilde Twins as themselves
* Helen Forrest as herself Xavier Cugat and His Orchestra as themselves
 

==Soundtrack== Jules Lemare - Performed by June Allyson (dubbed by Virginia Rees) and Gloria DeHaven (dubbed by Dorothy Jackson)
* My Mother Told Me - Sung by Gloria DeHaven
* In A Moment of Madness - words by Ralph Freed, music by Jimmy McHugh - Sung by Helen Forrest, accompanied by Harry James and His Music Makers
* A Tisket, a Tasket - words music by Al Feldman and Ella Fitzgerald - Performed by June Allyson and Gloria DeHaven
* Babalu - words music by Margarita Lecuona - Performed by Lina Romay with Xavier Cugat and His Orchestra
* Charmaine - by Erno Rapee and Lew Pollack - Performed by Harry James and His Music Makers Albert Coates
* Estrellita - music by M. M. Ponce - Performed by Harry James and His Music Makers
* A Love Like Ours - words by Mann Holiner, music by Alberta Nichols - Performed by June Allyson (dubbed by Virginia Rees) and Gloria DeHaven, with Harry James and His Music Makers Carlos Ramírez, with Xavier Cugat and His Orchestra
* Paper Doll - words music by Johnny S. Black - Performed by Lena Horne
* Rumba Rumba - words by Sammy Gallop, music by José Pafumy - Performed by Lina Romay with Xavier Cugat and His Orchestra Irving Taylor, Vic Mizzy  - Performed by Virginia OBrien, Lee Wilde, Lyn Wilde, and Lina Romay with Xavier Cugat and His Orchestra
* Anchors Aweigh - Performed by an unidentified marching band in the dream sequence
* Did You Ever Have the Feelin? - Written and performed by Jimmy Durante Ben Ryan, Harry Donnelly - Performed by Jimmy Durante
* Thrill of a New Romance - Played by Xavier Cugat and His Orchestra. Danced by Ben Blue and Lina Romay
* You, Dear - words by Ralph Freed, music by Sammy Fain  - Performed by Harry James and His Music Makers
* Who Will Be with You When Im Far Away - Performed, words, music by Jimmy Durante
* The Young Man with a Horn - words by Ralph Freed, music by Georgie Stoll - Performed by June Allyson and Harry James and His Music Makers
* Ritual Fire Dance - by Manuel de Falla - Performed on pianos by José Iturbi and Amparo Iturbi
* Flash - by Harry James
* I Gotta Go, I Gotta Stay words music by Jimmy Durante  

==Reception==
According to MGM records the film earned $2,852,000 in the US and Canada and $1,724,000 elsewhere, resulting in a profit of $1,726,000. 

==References==
Notes
 

== External links ==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 