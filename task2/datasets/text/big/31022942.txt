Fall Guy (1947 film)
{{Infobox film
| name           = Fall Guy
| image          = Fall Guy 1947 Poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Reginald Le Borg
| producer       = Walter Mirisch
| screenplay     = Jerry Warner John ODea
| based on       =  
| starring       = {{plainlist|
* Leo Penn Robert Armstrong
* Teala Loring
}}
| music          = Edward J. Kay
| cinematography = Mack Stengler
| editing        = William Austin
| studio         = 
| distributor    = Monogram Pictures
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
}} Robert Armstrong and Teala Loring.  The film is based on Cornell Woolrichs short story, "Cocaine."

==Plot==
With no memory of the night in question and a few clues, a man tries to prove he did not murder an attractive woman.

==Cast==
* Leo Penn as Tom Cochrane (billed as Clifford Penn) Robert Armstrong as Mac McLaine
* Teala Loring as Lois Walter
* Elisha Cook Jr. as Joe
* Douglas Fowley as Inspector Shannon
* Charles Arnt as Uncle Jim Grossett
* Virginia Dale as Marie
* Iris Adrian as Mrs. Sindell

==Reception==
TV Guide rated it 2/4 stars.  

==References==
 

==Further reading==
*  

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 