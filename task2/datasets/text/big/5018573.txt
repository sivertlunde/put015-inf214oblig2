Pet Sematary (film)
{{Infobox film
| name = Pet Sematary
| image = Pet_sematary_poster.jpg
| alt =
| caption = Theatrical release poster
| director = Mary Lambert
| producer = Richard P. Rubinstein
| screenplay = Stephen King
| based on =  
| starring = {{plainlist|
* Dale Midkiff
* Fred Gwynne
* Denise Crosby
* Miko Hughes
* Blaze Berdahl
* Brad Greenquist
* Michael Lombard
}}
| music = Elliot Goldenthal Peter Stein
| editing = {{plainlist|
* Daniel P. Hanley Mike Hill
}}
| studio = Paramount Pictures
| distributor = Paramount Pictures
| released =  
| runtime = 103 minutes
| country = United States
| language = English
| budget = $11.5 million 
| gross = $57.5 million
}} adaptation of novel of the same name. Directed by Mary Lambert and written by King, the film features Dale Midkiff as Louis Creed, Denise Crosby as Rachel Creed, Blaze Berdahl as Ellie Creed, Miko Hughes as Gage Creed, and Fred Gwynne as Jud Crandall. Andrew Hubatsek was cast for Zeldas role. Author King has a cameo as a minister. 

A sequel, Pet Sematary Two, was released which was met with less financial and critical success.

==Plot== Chicago to Ludlow, Maine, where they end up befriending the elderly neighbor Jud Crandall, who takes them to an isolated pet cemetery hidden behind the Creeds new home.

While working at the University of Maine, Louis meets Victor Pascow, who is brought in with severe injuries from a car accident. He manages to warn Louis about the pet cemetery near his home before he dies, calling Louis by name, despite the fact they hadnt met before. After he dies, Victor comes to Louis in a dream to tell him about the dangers inherent at the cemetery. Louis awakens and assumes it was a dream, but notices his feet are covered in dirt, indicating he had gone to the cemetery. During Thanksgiving while the family is gone, Ellies cat Church is run down on the highway by the house, Jud takes Louis beyond the cemetery and buries Church where he says the real burial ground is. Church comes back to life, though a shell of what he was before, he now appears more vicious. Sometime later, Gage is killed by a truck along the same highway. When Louis questions Jud on whether humans had been buried in the cemetery before he recounts a story of a friend named Bill who had buried his son who had died in World War II at the site but he had come back changed. Realizing the horror that he brought to the townsfolk, Jud, Bill and some friends tried to destroy Timothy by burning him to death in the house, but Bill was attacked by Timothy in the process and both were killed.

Rachel and Ellie go on a trip and Louis remains home alone. Despite Juds warnings and Victors attempts to stop him, Louis exhumes his sons body from the cemetery he was at and buries him instead at the ritual site. Victor appears to Rachel and warns her that Louis has done something terrible. She tries to reach out to Louis, then to Jud, informing him that she is returning home. She hangs up before Jud can warn her not to return. That night, Gage returns home and steals a scalpel from his fathers bag. He taunts Jud before slashing his Achilles tendon and kills him. Later, Rachel returns home and begins having visions of her disfigured sister Zelda before she had died, only to discover that its Gage, holding a scalpel. In shock and disbelief, Rachel reaches down to hug her son and he kills her as well.

Waking up from his sleep, Louis notices Gages footprints in the house and realizes his scalpel is missing. Getting a message from Gage that he has "played" with Jud and "Mommy" and wants to play with him now, he fills two syringes with morphine and heads to Juds house. Encountering Church, who attacks him, he kills the cat with an injection. Gage taunts him further within Juds house and Louis discovers Rachels corpse, falling hanged from the attic before he is attacked by his son. After a brief battle, Louis kills Gage with the morphine injection. He then lights the house on fire, leaving it to burn as he carries Rachel from the fire. Despite Victors continued insistence not to, Louis determines Rachel wasnt dead as long as Gage was, and that burying her would bring her back to him. Victor cries out in frustration and vanishes as Louis passes through him.

That night, playing solitaire alone, Rachel returns to the house and she and Louis kiss. Unknown to him, she takes a knife from the counter and as the screen goes dark, Rachel stabs Louis.

==Cast==
* Dale Midkiff as Louis Creed
* Fred Gwynne as Jud Crandall
* Denise Crosby as Rachel Creed
* Miko Hughes as Gage Creed
* Blaze Berdahl as Ellie Creed
* Brad Greenquist as Victor Pascow
* Michael Lombard as Irwin Goldman
* Susan Blommaert as Missy Dandridge
* Mara Clark as Marcy Charlton
* Kavi Raz as Steve Masterton
* Mary Louise Wilson as Dory Goldman
* Andrew Hubatsek as Zelda
* Lisa Stathoplos as Juds mother
* Stephen King as Minister
* Chuck Courtney as Bill Baterman
* Peter Stader as Timmy Baterman

==Production== Mount Hope Cemetery, at Kings behest. 

==Music==
 
The films score was written by  " appears in a scene, and "Pet Sematary", a new track written specially for the movie, plays over the credits.  

The song "Pet Sematary" became one of the Ramones biggest charting hits, reaching number four on the Billboard Modern Rock Tracks list, despite being, in the words of AMG, "reviled by most of the bands hardcore fans". 

==Release==
The Los Angeles Times wrote that the film "defied the critics and opened at blockbuster levels". 
The film grossed $57 million in North America.   Released in 1989 by Paramount Home Video, Pet Sematary was a best-selling video.   Paramount released it on DVD in 2006 and on Blu-Ray in 2012.   

==Reception==  Kevin Thomas of the Los Angeles Times wrote, "Lambert goes for strong, succinct images and never stops to worry whether theres a lack of credibility or motivation."   Richard Harrington of The Washington Post called it "bland, cliched, cheap".     Harrington criticized Gages actions as disturbing and the climax as "an ugly payoff to an inept setup".   Bloody Disgusting rated it 4.5/5 stars and wrote, "The plot alone would make for a scary movie, but by injecting excellent atmosphere, capable acting and generally nightmarish scenes, Pet Semetery is a truly effective horror flick and well worth the price of admission."   At Dread Central, Steve Barton rated it 4/5 stars and called it one of the best King adaptations;  Jason Jenkins rated it 3.5/5 stars and called it "one of the better King adaptations of the period". 

==Legacy==
A sequel,   was announced as directing the remake in October 2013. 

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 