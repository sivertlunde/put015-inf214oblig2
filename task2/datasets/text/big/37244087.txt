The Revisionaries
{{Infobox film
| name           = The Revisionaries
| image          = the-revisionaries.jpg
| caption        = Theatrical release poster
| director       = Scott Thurman
| writers =  Jawad Metni Scott Thurman
| producers = Pierson Silver Orlando Wood Scott Thurman
| studio = Silver Lining Films Magic Hour Entertainment Naked Edge Films
| executive producers = Jim Butterworth Vijay Dewan
| co-producers = Chandra C. Silver Daniel J. Chalfen
| music = Mark Orton
| editing = Jawad Metni Kino Lorber
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| gross = $21,731 
}}
The Revisionaries is a 2012 documentary film about the re-election of Don McLeroy, the former chairman of the Texas Board of Education.  The film also details how the Texas Boards decisions on textbook content influence textbooks across the nation and affect the American culture war.   The Revisionaries was directed by Scott Thurman and produced by Silver Lining Films, Magic Hour Productions and Naked Edge Films.

The film generated a great deal of buzz prior to its premiere on April 20, 2012 at the Tribeca Film Festival.     Texas Monthly reported that " he film received rave reviews after its Tribeca premiere."  The Revisionaries went on to win the Festivals Special Jury Prize.  During the awards presentation, Michael Moore stated "I hope every American sees this film," and called The Revisionaries "a must-see film for anyone concerned about enforced ignorance and intolerance, and for those who still believe in science and in Thomas Jefferson." 
 Kino Lorber announced that it had acquired all North American rights to The Revisionaries.  The film premiered theatrically on October 5, 2012 in Dallas, Texas,  and the Public Broadcasting Services (PBS) Independent Lens aired an abridged version of the film in late January 2013.    The Revisionaries went on to win the 2013 PBS Independent Lens Audience Award  and 2014 duPont Award for excellence in broadcast journalism. 

==References==
 

==External links==
*  
*  
* http://www.itvs.org/films/revisionaries
* http://www.pbs.org/independentlens/revisionaries/
* http://www.pbs.org/independentlens/award/
* http://www.rottentomatoes.com/m/the_revisionaries_2012/
* https://www.facebook.com/TheRevisionaries
* http://www.amazon.com/The-Revisionaries-Scott-Thurman/dp/B00AOCDEDY

 
 
 
 
 
 
 
 

 