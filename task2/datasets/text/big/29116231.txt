The White Rose (1923 film)
 
{{Infobox film
| name           = The White Rose
| image          =File:White Rose lobby card.JPG
| caption        =Lobby card
| director       = D. W. Griffith Herbert Sutch (asst. director)
| producer       = D. W. Griffith
| writer         = Irene Sinclair (pen name of Griffith) Neil Hamilton
| music          = Joseph Breil
| cinematography = Billy Bitzer Hendrik Sartov Harold Sintzenich
| editing        =
| distributor    = United Artists
| released       =   reels
| country        = United States Silent (English intertitles)
}}
 silent drama Neil Hamilton.

The film was shot in several locations throughout Florida and Louisiana. Though this film is extant, it is one of Griffiths rarely seen films. According to film historian William K. Everson, a tinted and toned 35&nbsp;mm nitrate print was allowed to decompose in the 1960s, leaving only a sole black and white print. 

==Cast==
*Mae Marsh - Bessie Teazie Williams
*Carol Dempster - Marie Carrington
*Ivor Novello - Joseph Beaugarde Neil Hamilton - John White
*Lucille La Verne - Auntie Easter
*Porter Strong - Apollo
*Jane Thomas - Cigarstand Girl
*Kate Bruce - An Aunt
*Erville Alderson - Man of the World
*Herbert Sutch - The Bishop
*Joseph Burke - The Landlord
*Mary Foy - The Landlady
*Charles Emmett Mack - Guest
*Uncle Tom Jenkins - old black man

==References==
 

==External links==
* 
* 
*  at silenthollywood.com
*  at moviessilently.com
*  at George Eastman House

 

 
 
 
 
 
 
 


 