The Water Engine
 
{{Infobox play
| name       = The Water Engine
| image      = 
 
| caption    = 
| writer     = David Mamet
| characters = Charles Lang Rita Morton Gross Lawrence Oberman Mrs. Varěc Mr. Wallace Bernie Dave Murray
| setting    = Chicago, 1934
| premiere   = 1977 
| place      = Off-Broadway
| orig_lang  = English
| subject    = 
| genre      = Drama
| web        = 
}}
 

The Water Engine is a play by David Mamet that centers on the violent suppression of a disruptive alternative energy technology.

==Plot==
Charles Lang works at a menial job at a factory and lives with his blind sister Rita in an apartment in Chicago during the 1934 World’s Fair. But he is also an amateur inventor, and the play centers around a machine he designs that can create electricity from distilled water. Seeking to patent his idea, he finds a lawyer, Morton Gross, in the phone book and shows him the machine, but Gross’s motivations seem to differ from Lang’s. Gross recruits another lawyer, Lawrence Oberman, and together they menace Lang and eventually his sister. It is heavily implied that the two of them serve the corporate establishment whose profits Lang’s engine threatens.

By the time Lang realizes he is being taken advantage of, however, the lawyers have him trapped. He attempts to contact a newspaper reporter, but Gross and Oberman hold his sister hostage to prevent him from telling his story. He then meets a barker at the World’s Fair right before it closes for the night who tells him of a chain letter he has just received, which gives him an idea.

The lawyers try to force Lang into giving them his plans, but he says he no longer has them; the audience finds out elliptically from a scene in the newspaper reporter’s office that he and Rita have been killed. The play ends with Bernie, a young friend of the family who has previously shown mechanical aptitude, receiving the plans for the Water Engine in the mail.   

==Themes==
The Century of Progress theme of the 1934 Chicago Worlds Fair informs that of the play. Technology is interspersed throughout the dialogue as the voices of various announcing figures, over radios, on physical soapboxes, and, in the case of the Chain Letter, of indeterminate origin, reinforce the notion of a rising tide of change as they herald the advent of a new technological era. The superstition represented by the Chain Letter contrasts with its eventual saving of Langs invention and yet also coincides with it, as both the inventor and the letter seek explanations and justice in a world that often—particularly in the cases of both the lawyers, the knowingly bombastic newspaper reporter Dave Murray, and the Fair itself—seems more intent on flowery rhetoric than on the pursuit of truth or the greatest good of society.   

The play plays with the form of daytime radio serials, as its plot and structure, with clearly defined heroes and antagonists, riffs off the suspense thrillers that were popular around the time the play is set. That it was originally written as a radio play positions it as an homage to the genre.   

==Production== Broadway as a double-bill with a short Mamet play entitled Mr. Happiness, and ran for 24 performances. In this production Patti LuPone was featured as Rita.  The play was nominated for the Drama Desk Award for Outstanding New Play.   
 Amblin Television and broadcast by Turner Network Television|TNT. 

==See also==
*Water-fuelled car

== References ==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 