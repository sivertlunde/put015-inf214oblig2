Goodbye Love (film)
{{Infobox film
| name           = Goodbye Love
| image          = Charles Ruggles, in Goodbye Love (1933).jpg
| image_size     = thumb
| caption        = Charles Ruggles in Goodbye Love (1933)
| director       = H. Bruce Humberstone
| producer       = Joseph I. Schnitzer (producer) Samuel Zierler (producer)
| writer         = Hampton Del Ruth (story) Hampton Del Ruth (writer) and George Rosener (writer) and John Howard Lawson (writer)
| narrator       =
| starring       = See below Howard Jackson Milan Roder Oliver Wallace
| cinematography = Charles Edgar Schoenbaum
| editing        = Rose Loewinger
| distributor    = RKO Radio Pictures
| released       = 1933
| runtime        = 67 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Goodbye Love is a 1933 American film directed by H. Bruce Humberstone.

==Plot summary==
Chester Hamilton (Sidney Blackmer) is sent to prison for non-payment of alimony to Phyllis Van Kamp (Verree Teasdale). Hamiltons valet, Oswald Groggs (Charles Ruggles), uses his boss’s reservations at an exclusive resort to go on vacation under the assumed identity of wealthy eccentric "Sir Oswald".

Phyllis tries to marry "Sir Oswald" for his money. When Oswald seems to fall for her, the question is who will be left standing at the altar.
 Ray Walker) witness all these shenanigans.

==Cast==
*Charles Ruggles as Oswald Groggs
*Verree Teasdale as Phyllis Van Kamp aka Fanny Malone
*Sidney Blackmer as Chester Hamilton
*Phyllis Barry as Dorothy Blaine Ray Walker as Brooks
*Mayo Methot as Sandra Hamilton John Kelly as Sergeant Dugan the Jailer
*Grace Hayle as Lura "Ducky" Groggs
*Luis Alberni as Tony Richard Tucker as Eddie the Lawyer
*Edward Van Sloan as Judge
*Gerald Fielding as Dunwoodie - Sandras Beau
*Drew Demorest as Desk Clerk

==Soundtrack==
*"Goodbye Love" (Written by Con Conrad, Archie Gottler and Sidney D. Mitchell)

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 