Nikki and the Perfect Stranger
{{Infobox film
 | name = Nikki and the Perfect Stranger
 | image size =
 | alt =
 | caption =
 | director = Jefferson Moore Shane Sooter
 | producer = Jefferson Moore Kelly Worthington Moore
 | writer = Jefferson Moore (screenplay and story)
 | narrator = Matt Wallace
 | music = Tyler Franklin 
 | cinematography = Philip Vetter  Pate Walters
 | studio = Kellys Filmworks
 | distributor = Kellys Filmworks LTD
 | released = October 5, 2013
 | runtime =
 | country = United States
 | language = English
 | budget =
 | gross =
 | followed by =
 | image = 
}}
 Christian Drama drama movie The Perfect Matt Wallace (reprising his role as Tony Vincent from The Perfect Gift), and Juliana Allen as Nikki Cominsky (replacing Pamela Brumley who played Nikki in the first movie).

== Plot ==

The third and final chapter in the Perfect Stranger movie series, features the return of Nikki Cominskey, now in her forties and no longer a high-powered attorney, who has done everything she knows to grow spiritually, and wonders where her closeness with Jesus has gone. Burned out and hopeless, she wails her complaints to God during a late-night interstate trip. Running out of fuel, she finds Jesus once again....along the roadside with a can of gas. The Wonderful Counselor hops in and offers answers she never heard in a church and a nighttime of adventure ensues beyond anything Nikki could have ever imagined.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 

 