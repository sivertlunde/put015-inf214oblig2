Her Kind of Man
{{Infobox film
| name           = Her Kind of Man
| image          = Her kind of man 1946 poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Frederick De Cordova
| producer       = Alex Gottlieb
| screenplay     = Leopold Atlas Gordon Kahn Charles Hoffman James V. Kern
| narrator       =
| starring       = Dane Clark Janis Paige Zachary Scott
| music          = Franz Waxman
| cinematography = Carl E. Guthrie
| editing        = Dick Richards
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 80 minutes 
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Her Kind of Man is a 1946 American crime film noir directed by Frederick De Cordova, and starring Dane Clark, Janis Paige and Zachary Scott. 

==Plot==
A nightclub singer (Janis Paige) is caught between a big-time gangster (Zachary Scott) and a tough Broadway columnist (Dane Clark).

==Cast==
* Dane Clark as Don Corwin
* Janis Paige as Georgia King
* Zachary Scott as Steve Maddux
* Faye Emerson as Ruby Marino
* George Tobias as Joe Marino Howard Smith as Bill Fellows Harry Lewis as Candy
* Sheldon Leonard as Felix Bender

==Reception==

===Critical response===
Film critic Bosley Crowther gave the film a lukewarm review, "There are gun fights, slugging matches and gambling sessions of the usual hard-boiled sort—and, except for a certain flair in dialogue, it is just another one of those things." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 