The Golem and the Dancing Girl
{{Infobox film
|  name           = The Golem and the Dancing Girl
|  image          =
|  caption        =
|  writer         = Paul Wegener
|  starring       = Paul Wegener Lyda Salmonova Rochus Gliese
|  director       = Rochus Gliese Paul Wegener
|  producer       = 
|  distributor    =
|  released       =  
|  runtime        =  German intertitles
|  country        = German Empire
|  budget         =
|  music          =
|  awards         =
|}}
The Golem and the Dancing Girl (original German title:  ) is a 1917 fantasy horror film. It is part of a trilogy, preceded by   (1920). Paul Wegener and Rochus Gliese co-directed and acted in the film. Wegener also wrote the screenplay. This was the screen debut of Fritz Feld.

The Golem and the Dancing Girl is now considered a lost film, though silentera.com reports a print may exist in an "eastern European film archive". 

==Cast==
*Paul Wegener
*Lyda Salmonova as Helga
*Rochus Gliese
*Wilhelm Diegelmann		
*Fritz Feld as Hotel Page
*Emilie Kurz		
*Mr. Meschugge		
*Erich Schönfelder		
*Friedrich Veilchenfeld		
*Ernst Waldow

IMDb credits Wegener as playing the Golem (as he does in the other two films in the trilogy), while silentera.com states this role was played by Gliese.  

==See also==
*List of films made in the German Empire (1895-1918)
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 