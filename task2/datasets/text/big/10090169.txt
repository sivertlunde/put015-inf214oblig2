Silent Raiders
{{Infobox film
| name           = Silent Raiders
| image          = Tsrpos.jpg
| image_size     =
| caption        = Original film poster
| director       = Richard Bartlett
| producer       = Earle Lyon Richard Bartlett
| writer         = Richard Bartlett
| based on       =
| screenplay     =
| narrator       =
| starring       = Richard Bartlett Earle Lyon Jeanette Bordeaux
| music          = Elmer Bernstein
| cinematography = Glen Gano
| editing        = Al Walker
| distributor    = Lippert Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
}} 1954 low budget American war film directed, starring and co-produced by Richard Bartlett. It was the first film of the L&B Production Company, consisting of Earle Lyon  and Richard Bartlett. 

==Plot==
Prior to the Dieppe Raid, seven US Army Rangers come ashore. Their mission is to destroy a German communications centre that controls the coastal guns that threaten the Canadian amphibious assault.

==Production==
Produced for $27,000 with the working titles of Dieppe Raid and Three Miles to Dawn, it was filmed in Malibu, California in 1953. 

The producers were able to use composer Elmer Bernstein who was relegated to minor studios during the Hollywood blacklist period.

==Cast==
Richard Bartlett ...  Sgt. Jack  
Earle Lyon ...  Sgt. Malloy  
Jeanette Bordeaux ...  French Girl  
Earl Hansen ...  Pepe  
Robert Knapp ...  Lt. Finch  
Dean Fredericks ...  Chief  
Frank Stanlow ...  Horse  
Carl Swanstrom ...  Wetzel

==Notes==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 