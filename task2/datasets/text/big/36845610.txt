Salvation Nell (1921 film)
{{Infobox film
| name           = Salvation Nell
| image          = Salvation Nell (1921).jpg
| imagesize      =
| caption        = Film still
| director       = Kenneth Webb
| producer       = Whitman Bennett
| writer         = Edward Sheldon (play Salvation Nell) Dorothy Farnum (adaptation)
| starring       = Pauline Starke
| cinematography = Ernest Haller
| editing        = Associated First National Pictures
| released       =  
| runtime        =
| country        = United States
| language       = Silent(English intertitles)
}} silent film produced by Whitman Bennett and distributed by Associated First National Pictures, later First National Pictures. It was directed by Kenneth Webb and stars Pauline Starke. The film is based on a successful 1908 Broadway play that starred Minnie Maddern Fiske.

This film survives in the George Eastman House and the Library of Congress collections.    
 1931 talkie version made by Tiffany Pictures survives.

==Cast==
*Pauline Starke - Nell Saunders Joe King - Jim Platt
*Gypsy OBrien - Myrtle Hawes
*Edward Langford - Mfajor Williams
*Evelyn Carter Carrington - Hallelujah 
*Maggie - 
*Charles Mcdonald - Sid McGovern
*Matthew Betz - Al McGovern
*Marie Haynes - Hash House Sal
*Arthur Earle - Giffen (*as A. Earl)
*William Nally - Callahan
*Lawrence Johnson - Jimmie

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 