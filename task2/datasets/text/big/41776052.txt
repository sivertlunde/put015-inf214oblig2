Mimesis: Night of the Living Dead
{{Infobox film
| name           = Mimesis: Night of the Living Dead
| image          = Mimesis Night of the Living Dead.jpg
| alt            =
| caption        =
| director       = Douglas Schulze
| producer       = Kurt Eli Mayry Douglas Schulze Gavin Grazer
| screenplay     = Joshua Wagner Douglas Schulze
| story          = Douglas Schulze
| starring       = Allen Maldonado Lauren Mae Shafer Taylor Piedmonte David G.B. Brown Courtney Gains Sid Haig
| music          = Diego Navarro
| cinematography = Lon Stratton
| editing        = Rob Frenette
| studio         = Dead Wait Productions MPI Films
| distributor    = Anchor Bay Entertainment
| released       =   }}
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Mimesis: Night of the Living Dead (also Mimesis) is a 2011 American horror film directed by Douglas Schulze, written by Joshua Wagner and Schulze, and starring Allen Maldonado, Lauren Mae Shafer, Taylor Piedmonte, and David G.B. Brown.

== Plot ==
During a horror film convention, several people are invited to an exclusive party.  After they consume spiked drinks, they wake up to find themselves involuntary participants in a recreation of George A. Romeros 1968 film Night of the Living Dead.  After being attacked, the participants flee to an abandoned farmhouse, which subsequently undergoes a zombie siege.

== Cast ==
* Allen Maldonado as Duane
* Lauren Mae Shafer as Judith
* Taylor Piedmonte as Russell
* David G.B. Brown as Keith
* Jana Thompson as Karen
* Gavin Grazer as Karl
* Courtney Gains as Gordon
* Sid Haig as Alfonso Betz

== Production ==
Director Douglas Shulze conceived the idea for the film after attending horror conventions and seeing costumed fans who took their in-character roleplaying too far.  The film is not meant as a remake; instead, Schulze wanted to address themes of obsessed fans. 

== Release ==
Mimesis: Night of the Living Dead premiered at the Blue Water Film Festival on October 7, 2011.  Sid Haig attended the event. 

Anchor Bay Entertainment released it on home video on February 12, 2013. 

== Reception ==
Dennis Harvey of Variety (magazine)|Variety wrote that the film is "just sporadically scary, yet the plots twists and turns keep viewers hooked."   Rod Lott of the Oklahoma Gazette called it a pointless, shoddy remake of Night of the Living Dead.   Steve Barton of Dread Central rated it 3/5 stars and called it "a movie with an excellent premise thats executed in the most standard of ways."   Adam Tyner of DVD Talk rated it 3/5 stars and called it "an uneven but surprisingly effective homage".   Patrick Naugle of DVD Verdict called it "a movie with a semi-interesting plot but uninspired execution." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 