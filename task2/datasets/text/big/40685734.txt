Smash & Grab
 
{{Infobox film
| name           = Smash & Grab: The Story of the Pink Panthers
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Havana Marking
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Tomislav Tom Benzon, Daniel Vivian, Jasmin Topalusic Simon Russell
| cinematography = Richard Gillespie,  Joshua Z. Weinstein (segment "New York City")
| editing        = Joby Gee
| studio         = Roast Beef Productions 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = United Kingdom, United States, Serbia and Montenegro
| language       = English
| budget         = 
| gross          = 
}}

Smash & Grab: The Story of the Pink Panthers is a 2013 documentary by Havana Marking based upon the international jewel thief network called the Pink Panthers. The movie had a limited release on Jul 31, 2013 and had a wider release in the United Kingdom on September 27, 2013.

==Synopis==
The film features Closed-circuit television footage from several of the jewel heists attributed to the Pink Panthers, who are credited with over 300 jewel thefts throughout the world. Interspersed throughout the documentary are interviews with various personas such as crime experts as well as anonymous interviews with persons claiming to be members of the Pink Panthers. Smash & Grab also features several segments that follow Mike (Tomislav Tom Benzon), Mr. Green (Daniel Vivian), and Lena (Jasmin Topalusic), fictionalized depictions of members of the Pink Panthers.

==Reception==
Critical reception has been mixed to positive.   As of October 2, 2013, review aggregator Rotten Tomatoes has the film listed as 83% "fresh" based upon 15 reviews.  The Guardian gave the film four stars, commenting that it "asks some fascinating questions – and has fascinating footage to match". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 