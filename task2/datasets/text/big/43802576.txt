We're on the Jury
{{Infobox film
| name           = Were on the Jury
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Ben Holmes Bob Barnes (assistant)
| producer       = Lee Marcus Joseph Henry Steele (associate)
| writer         = 
| screenplay     = Franklin Coen
| story          = 
| based on       =  
| starring       = Victor Moore Helen Broderick
| narrator       = 
| music          = 
| cinematography = Nick Musuraca
| editing        = Ted Cheesman John Lockert RKO Radio Pictures
| distributor    = 
| released       =   |ref2= }}
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Were on the Jury is a 1937 American comedy film directed by Ben Holmes from a screenplay by Franklin Coen, based on the 1929 play, Ladies of the Jury, written by John Frederick Ballard.  The film stars Victor Moore and Helen Broderick, and was produced by RKO Radio Pictures which premiered the film in New York City on February 11, 1937, with a national release the following day on February 12.

==References==
 

==External links==
* 

 
 
 
 
 


 