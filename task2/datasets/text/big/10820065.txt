Kaliyugaya (film)
{{Infobox film
 | name = Kaliyugaya
 | image = File:A_scene_from_the_Sinhala_film_Kaliyugaya.jpg
 | imagesize = 500
 | caption = Punya Heendeniya as Nanda and Henry Jayasena (as Piyal) in a scene from the film
 | director = Lester James Peries
 | producer =
 | writer = A.J. Gunawardena
 | starring = Punya Heendeniya Henry Jayasena Trelicia Gunawardena Wickrema Bogoda
 | music = Premasiri Khemadasa
 | cinematography = Donald Karunarathne
 | editing = Gladwin Fernando
 | distributor = Tharanga Films
 | country    = Sri Lanka
 | released =  
| runtime = 85 min  Sinhala
 | budget = 
}}
Kaliyugaya is a 1981 Sri Lankan drama film directed by Lester James Peries; it was adapted from the novel Kaliyugaya by Martin Wickramasinghe, and follows the events of the film Gamperaliya (film)|Gamperaliya.

It was a Directors Fortnight selection at the 1982 Cannes Film Festival     
 in 1983.

== Synopsis ==

Piyal (Henry Jayasena) and Nanda (Punya Heendeniya) from Gamperaliya have now aged, and their children have left them. Priya reminisces on her life after her son sends her a letter from London accusing her and her husband of various faults.

== Production ==
The film features the four main actors of Gamperaliya; it was shot on 35mm and used eastmancolor.    The film also featured the famous stately home Lakshmigiri. 


==References==
 

==External links==
* 
* 
*  

 
 
 
 


 
 