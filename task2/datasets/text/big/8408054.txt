The Abominable Snow Rabbit

 

{{Infobox Hollywood cartoon
| cartoon_name = The Abominable Snow Rabbit
| series = Looney Tunes/Bugs Bunny/Daffy Duck
| image = The Abominable Snow Rabbit title card.png
| caption =
| director = Chuck Jones   Maurice Noble
| story_artist = Tedd Pierce Richard Thompson
| voice_actor = Mel Blanc
| musician = Milt Franklyn John Burton
|studio=Warner Bros. Cartoons
| distributor = Warner Bros. Pictures   Vitaphone Corporation
| release_date = May 20, 1961 
| color_process = Technicolor
| runtime = 6 minutes, 05 seconds
| movie_language = English
}}
 The Abominable Snowman. It was the final original Chuck Jones theatrical cartoon with Daffy Duck.

==Plot==
Bugs tunnels through the Himalayan mountains, followed by Daffy. After a failed attempt by Daffy to go swimming in a frozen pond, the two realize that they are not at their intended destination, Palm Springs.
 The Abominable Of Mice and Men, casting Hugo as Lennie Small to Daffys George Milton), and gives him crippling hugs, believing Daffy is a rabbit, when actually he just tied his sweater round his head for warmth, with the sleeves on top. Daffy reveals this by angrily yelling his head off, and George is punished for his deception with spankings. However, Daffy imparts to him where he can find a real rabbit i.e. Bugs. As Bugs starts experiencing Hugos overbearing love, Daffy sneaks away. Bugs is sat by Hugos rump like a hen, and sneaks out under the snow, carrying Daffy toward Hugo.

As Hugo doubts that Daffy is or isnt a rabbit, Daffy asks what the rabbit really looks like. Hugo responds that rabbits have long ears, making Bugs tie his own ears and gestures his both fingers behind Daffys head as rabbits ears. After Hugo painfully hugs the duck again but realizes that he doesnt have long ears and feathers, Daffy points out the tunneling Bugs to Hugo, who chases him underground. Eager to see the incidents conclusion, Daffy follows.

Later, in Palm Springs, a profusely sweating Hugo, and a disguised Bugs, discuss Hugos failure to catch Bugs. Hugo believes that he will not be able to see the rabbit again, but Bugs encourages him that "if he loves you, he will come back." When Daffy emerges from the hole, Bugs puts fake rabbit ears on him, causing Hugo to recognize him as a rabbit again. While Daffy is cuddled for the last time, Hugo is completely melted due to the intense heat of the environment. Bugs comments that "He really was a snowman!" and Daffy soaked in water replies, "Abominable, that is!"

==Home video releases==
The Abominable Snow Rabbit is included on the DVD collection   as well as the VHS collections Looney Tunes After Dark and Bugs Bunny: Big Top Bunny. Most of the footage was also used in the compilation movie, Daffy Ducks Quackbusters for one part when Bugs and Daffy traveled to the Himalayas.

==See also==
*List of Bugs Bunny cartoons 
*List of Daffy Duck cartoons

==External links==
* 

 
{{Succession box |
before= Lighter Than Hare | Bugs Bunny Cartoons |
years= 1961 |
after= Compressed Hare|}}
 

 
 
  
 
 
 
 
 
 