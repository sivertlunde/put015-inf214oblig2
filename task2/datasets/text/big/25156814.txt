Hello (1999 film)
{{Infobox film
| name = Hello
| image = Hello_1999.jpg
| director = K. Selva Bharathy
| writer = K. Selva Bharathy Ranjith Manivannan Senthil Charlie Charlie Vaiyapuri
| producer = Thiruvengadam Deva
| cinematography = S. D. Vijay Milton
| editing = B.S.Vasu - Saleem
| studio = Serene Moviee Makers
| distributor =
| released = 7 November 1999
| runtime = 
| country = India Tamil
| budget =
}}
 Tamil Kollywood|film directed by K. Selva Bharathy. The film stars Prashanth and Preeti Jhangiani in the lead roles. The film was one among 1999 Deepavali releases and did moderate business too.

==Plot==

Chandru Prashanth – graduate, works in a Florist shop owned by Manivannan. Chandru visits his friend’s STD booth, where he see many girls waiting for their boy friend’s phone call. Since Chandru doesnt  have a girlfriend he gets jealous. Chandru’s friend Gayathri Sujitha plans to introduce one of her friends as Chandru’s girl friend. Chandru and his two friends go to the temple to meet her but by mistake he points out a girl - Swetha Preeti Jhangiani (friend of Gayathri) as his girlfriend. There arises a huge pell-mell.

Chandru avoids attending his friend Suresh’s engagement function, without knowing that the bride was Swetha. Once Chandru’s friends see Swetha, they stops the engagement. A big complication rises in Swetha’s family. Because of such problem Swetha tries to commit suicide and gets admitted in the hospital. In the mean time Swetha’s brother threatens Chandru’s friends asking for Chandru, who is the reason to stop the engagement. After getting to know about this matter, Chandru admits his two friends in the hospital where Swetha got admitted. There he meets Gayathri along with Swetha. That was the first time Swetha and Chandru getting introduced each other. Gradually Chandru becomes a good companion to Swetha and her family. Slowly Chandru gets into Swetha’s good books and wins her heart. Gayathri realizes Swetha’s love and transmit to Chandru. Finally Swetha’s family accept their marriage. When things are going happily, the friend who was ready to marry Swetha in the beginning comes to wish Chandru for his marriage. Then the calamity starts and a sudden hurricane raises in Swetha’s family. Whether Chandru and Swetha get over the adversity and unite is the rest of the story.

==Cast==
*Prashanth as Chandru
*Preeti Jhangiani as Swetha
*Sujitha as Gayathri Ranjith
*Manivannan Senthil
*Charlie Charlie
*Vaiyapuri
*Kaka Radhakrishnan

==Music== Deva with lyrics by Vairamuthu and Na. Muthukumar. 

==Soundtrack==

{{Infobox Album |  
| Name        = Hello
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1999
| Recorded    = 1999 Feature film soundtrack |
| Length      = 
| Label       = Five Star Audio Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1999, features 6 tracks. All the songs were massive hits.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Salam Gulamu || Sukhwinder Singh ||
|- 2 || Chella Chella || Srinivas & Anuradha Sriram ||
|- 3 || B.B.C Pola || S.P.Balasubramaniam ||
|- 4 || Shankar Mahadevan & Sabesh ||
|- 5 || Intha Nimisham || Hariharan (singer)|Hariharan, K.S.Chithra | Chithra ||
|- 6 || Salam Gulamu II || Naveen ||
|}

==References==
 

 
 
 
 