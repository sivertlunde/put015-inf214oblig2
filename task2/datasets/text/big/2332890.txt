Les Patterson Saves the World
 
 
{{Infobox film
| name           = Les Patterson Saves the World
| image          = George Miller
| producer       = Sue Milliken
| writer         = Barry Humphries Diane Millstead
| starring       = Barry Humphries Pamela Stephenson Joy Westmore Thaao Penghlis
| cinematography = David Connell
| editor         = Tim Wellburn
| music          = Tim Finn
| distributor    = Hoyts (Australia) Recorded Releasing (UK)
| released       =  
| runtime        = 98 minutess
| country        = Australia
| language       = English
| music          =
| awards         =
| budget         = A$7.3 million "Australian Productions Top $175 million", Cinema Papers, March 1986 p64 
| gross          = A$626,000 (Australia)
}}

Les Patterson Saves the World is a 1987 Australian comedy film starring Barry Humphries as his stage creations Sir Les Patterson and Dame Edna Everage.

==Plot==
The uncouth Sir Les Patterson, played by Barry Humphries, teams up with Dame Edna Everage (again played by Barry Humphries) to save the world from a virulent bioterror attack ordered by Colonel Richard Godowni of the Gulf State of Abu Niveah.

==Cast==
*Barry Humphries as Sir Les Patterson / Dame Edna Everage
*Pamela Stephenson as Veronique Crudité
*Thaao Penghlis as Colonel Richard Godowni Andrew Clarke as Neville Thonge
*Henri Szeps as Dr. Charles Herpes / Desiree Herpes
*Hugh Keays-Byrne as Inspector Farouk
*Elizabeth McIvor as Nancy Borovansky
*Garth Meade as Mustafa Toul
*Arthur Sherman as General Evans
*Josef Drewniak as Mossolov
*Esben Storm as Russian Scientist
*Joy Westmore as Lady Gwen Patterson
*Connie Hobbs as Madge Allsop
*Joan Rivers as the U.S. President 

==Production== George Miller The Man from Snowy River fame.

The movie was originally meant to be made by Thorn EMI in Britain but was eventually established in Australia with entirely Australian money. 

==Box Office==
Les Patterson Saves the World grossed $626,000 at the box office in Australia.  "It was a disaster of major proportions," said Jonathan Chissick of Hoyts, who distributed the film in Australia. David Stratton, The Avacado Plantation, Pan MacMillan, 1990 p308  David Stratton wrote in 1990, "The gala opening was an embarrassing occasion, and it is still rumoured in the industry today that the Federal Treasurer Paul Keating, who attended, was so angry that he decide to end rorts in the film industry." 

The movie was released to British cinemas in 1988 but was not successful there either. 

==Critical Reception== worst ever" Australian films.  

==See also==
* Cinema of Australia

==References==
 

==External links==
*  at the National Film and Sound Archive
* 

 

 
 
 
 
 


 
 