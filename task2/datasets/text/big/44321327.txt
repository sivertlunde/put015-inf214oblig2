The Lazarus Effect (2015 film)
 
{{Infobox film
| name           = The Lazarus Effect
| image          = The Lazarus Effect (2015 film) poster.jpg
| alt            = The right side of a womans face with full black eyeballs with scarring all around that same eye. The words "The Lazarus Effect" are at the bottom right in white, 5 cast members names above the title, and the tagline "Evil Will Rise" at the bottom middle.
| caption        = Theatrical release poster
| director       = David Gelb
| producer       = {{Plainlist|
* Jason Blum
* Luke Dawson
* Matt Kaplan
* Jimmy Miller
* Cody Zwieg}}
| writer         = {{Plainlist|
* Luke Dawson
* Jeremy Slater}}
| starring       = {{Plainlist|
* Mark Duplass
* Olivia Wilde
* Donald Glover
* Evan Peters
* Sarah Bolger}}
| music          = Sarah Schachner
| cinematography = Michael Fimognari
| editing        = Michael N. Knue
| studio         = Blumhouse Productions
| distributor    = Relativity Media
| released       =   
| runtime        = 83 minutes 
| country        = United States
| language       = English
| budget         = $3.3 million 
| gross          = $27 million 
}}
The Lazarus Effect is a 2015 American science fiction horror film directed by David Gelb and written by Luke Dawson and Jeremy Slater. The film stars Mark Duplass, Olivia Wilde, Donald Glover, Evan Peters, and Sarah Bolger. The film was theatrically released on February 27, 2015 by Relativity Media.

==Plot==
Medical researchers Frank (Mark Duplass) and his fiancée, Zoe (Olivia Wilde), have developed a serum, code-named "Lazarus of Bethany|Lazarus". It was intended to assist coma patients but is shown to actually be able to bring the dead back to life. 

With the assistance of their friends, Niko (Donald Glover), Clay (Evan Peters)  and videographer Eva (Sarah Bolger), they run a successful trial on a recently deceased dog. However, they notice that the dog is behaving differently than it did when it was alive - its cataracts disappear, it loses its appetite, and it demonstrates other strange abilities. Tests reveal that the serum, instead of dissipating, is constructing strange new synapses within the dog’s brain.

When the dean of their university learns of their underground experiments, their project is shut down. They are also informed that a major pharmaceutical corporation has bought out the company that funded their research. The company and their attorneys confiscate everything associated with the project. 

Frank and his team sneak back into their lab to duplicate the experiment so that they can prove that they created the serum. During this attempt things go horribly wrong and Zoe is electrocuted and dies. Unwilling to let her go, Frank uses the serum to resurrect her. Initially, the procedure appears to be a success, but the team soon realizes that something is wrong with Zoe. She claims that when she died, she went to her version of Hell which was a nightmare originating from her childhood; as a child, Zoes apartment building had caught on fire and she watched her neighbors burn to death after they were trapped inside, causing recurring nightmares. She also begins to demonstrate unusual psychic abilities. The Lazarus serum causes brains to "evolve" incredibly rapidly, giving Zoe inhuman powers such as telekinesis and telepathy. 

The serum also causes increased aggression with horrifying consequences. Niko walks into a room and is startled by Zoe. After he refuses to kiss her, she uses telekinesis to throw him into a closet and crushes it with him inside, killing him. When Clay demands to know where Niko is, she kills him as well by choking him with an electric cigarette. Later, Zoe kills Frank after he attempts to kill her with a different serum. After this, Zoe injects herself with an entire bag of the Lazarus serum, making her abilities stronger than before.

Using her newly enhanced abilities, she cuts the power to the entire lab. Eva, who is still alive and trying to locate Zoe to inject her and finally kill her, is left in the dark. Zoe eventually finds Eva and sends her into the hell that she went to when she died. Inside the nightmare, it is revealed that Zoe was the one that caused the fire in the building, causing her to go to hell upon death. Eva is able to escape and inject Zoe with the syringe, but Zoe survives and kills Eva anyway.  She then injects Frank, in an attempt to bring him back from the dead, presumably turning him into a monster as well. Niko, Clay, and Eva also lay dead next to Frank, which indicates Zoe intends to bring them back as well.

==Cast==
* Mark Duplass as Frank Walton
* Olivia Wilde as Zoe McConnell
* Sarah Bolger as Eva
* Evan Peters as Clay
* Donald Glover as Niko
* Ray Wise as Mr. Wallace
* Amy Aquino as President Dalley

==Release==
On December 17, 2013, it was announced that the film (then titled Lazarus) would be released on January 30, 2015, with Lionsgate distributing the film.  On November 4, 2014, Relativity Media acquired the film from Lionsgate and set the films release date for February 20, 2015.  In December 2014, it was then announced that the film would be retitled The Lazarus Effect, and be released a week later than previously planned, on February 27, 2015. 

===Marketing===
The first still of the film was released on January 5, 2015, along with the theatrical poster. 

==Reception==

===Critical response=== rating average of 3.8/10. The sites consensus reads "The Lazarus Effect has a talented cast and the glimmer of an interesting idea, but wastes it all on insipid characters and dull, recycled plot points."  On Metacritic, which assigns a normalized rating, the film has a score of 31 out of 100, based on 29 critics, indicating "generally unfavorable reviews". 

Frank Scheck of The Hollywood Reporter gave the film a negative review, saying "The film squanders whatever potential it had, not to mention the talents of such performers as Duplass and Wilde who clearly deserve better."  James Rocchi of The Wrap gave the film four out of five stars, saying "The Lazarus Effect doesnt exactly break new ground, but it nonetheless finds plenty to relish in the mouldering bits it stitches together as it gives classic themes about death, life and the soul a literal and figurative shot in the arm."  Geoff Berkshire of Variety (magazine)|Variety gave the film a negative review, saying "Mark Duplass and Olivia Wilde struggle to breathe life into a recycled thriller about the horrors of reanimation."  Mick LaSalle of the San Francisco Chronicle gave the film one out of four stars, saying "This is an 83-minute movie that feels a half hour longer and, if it werent for the loud crescendos, it would put people to sleep."  Claudia Puig of USA Today gave the film two and a half stars out of four, saying "Absorbing, well-crafted and appropriately tense, with a smart cast that raises it a notch above average."  Lou Lumenick of the New York Post gave the film zero stars, saying "Looks like it was directed by a blind and deaf person over a weekend on a leftover laboratory set from a TV show and edited with a roulette wheel."  Peter Keough of The Boston Globe gave the film a negative review, saying "There are lessons to be learned from this minimalist thriller. The first is that scaring people requires more than just tossing furniture around, turning the lights off and on, and basically sneaking up from behind and shouting "Boo!" 

A.A. Dowd of The A.V. Club gave the film a C-, saying "Like too many horror films, this one seems targeted at a hypothetical audience using only 10 percent of its brainpower."  Joe Neumaier of the New York Daily News gave the film two out of five stars, saying "The Lazarus Effect, clocking in at a brief 86 minutes, doesn’t go far enough, isn’t scary enough and has mad scientists who just aren’t mad enough. You watch it hoping it revives itself, but that dream is dead and buried."  Michael OSullivan The Washington Post gave the film one out of four stars, saying "It staggers, zombielike, from one jump-scare to another before petering out, a scant 83 minutes after rising from the slab."  Bruce Demara of the Toronto Star gave the film two out of four stars, saying "While The Lazarus Effect isnt the worst scary movie film youll see this year, it is probably one of the most predictable and lazily plotted."  James Berardinelli of ReelViews gave the film two out of four stars, saying "The Lazarus Effect begins with an intriguing premise then proceeds to squander all the early goodwill through a slow, inexorable descent into cheap horror gimmicks."  Kevin C. Johnson of the St. Louis Post-Dispatch gave the film two out of four stars, saying "The Lazarus Effect boasts nothing special. Its not going to provide much relief for horror-starved audiences."  Keith Staskiewicz of Entertainment Weekly gave the film a C-, saying "I would have loved to see more from the filmmakers, daring to fail while staking out some new terror incognita instead of just going through the motions of an experiment for which we already have the results." 

===Box office=== Fifty Shades of Grey. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 