In the Fog
 
{{Infobox film
| name           = In the Fog
| image          = In the Fog poster.jpg
| caption        = Theatrical release poster
| director       = Sergei Loznitsa
| producer       = Heino Deckert Rick McCallum 
| writer         = Sergei Loznitsa
| based on       =  
| starring       = Vladimir Svirskiy
| music          = 
| cinematography = Oleg Mutu
| editing        = 
| studio         = {{Plainlist |
* Belarusfilm
* GP Cinema Company
* Lemming Film
* Ma.ja.de. Fiction
* Rija Films
}}
| distributor    = 
| released       =  
| runtime        = 127 minutes
| country        = 
{{Plainlist |
* Belarus
* Germany
* Latvia
* Netherlands
* Russia
}}
| language       = Russian
| budget         = 
}}

In the Fog ( ) is a 2012 drama film directed by Sergei Loznitsa.    The film competed for the Palme dOr at the 2012 Cannes Film Festival.       The film won the Golden Apricot at the 2012 Yerevan International Film Festival, Armenia, for Best Feature Film.

==Plot== Partisans and hatred of local people. The Partisans suspect Sushenya, a track-walker, of collaboration with the Nazis because he was first arrested and then suddenly released after they had blown up a German train. Two of them capture Sushenya and lead him to the forest where they are going to shoot him as a traitor. They fall into a trap set by the Germans who severely wound one of the partisans. Sushenya attempts to save his executioners life by carrying the wounded Partisan on his back to the nearest village. Nevertheless Sushenya is under suspicion. He laments that he was a well-respected and trustworthy village resident, raising a family in peace, however the war changed that forever.

==Reception==
On Rotten Tomatoes the film is certified fresh with 86% of critics giving the movie favorable reviews. The consensus on the site states, "While it treads familiar narrative ground -- and is a mite predictable at times -- In the Fog proves a smart, thought-provoking antidote to Hollywood action movies." Writing for The Guardian, Peter Bradshaw said it was "an intense, slow-burning and haunting drama."   

==Cast==
* Vladimir Svirskiy as Sushenya
* Vladislav Abashin as Burov
* Sergei Kolesov as Voitik
* Yulia Peresild as Anelya
* Nadezhda Markina as Burovs mother
* Vlad Ivanov as Grossmeier

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 