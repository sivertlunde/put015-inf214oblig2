My Neighbor, My Killer
 
{{Infobox film
| name           = My Neighbor, My Killer
| image          =
| caption        = 
| director       = Anne Aghion
| producer       = Anne Aghion
| writer         = Anne Aghion
| screenplay     = 
| story          = 
| starring       = 
| music          = 
| cinematography = James Kakwerere Linette Frewin Claire Bailly du Bois Mathieu Hagnery
| editing        = Nadia Ben Rachid
| studio         = Gacaca Productions
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = France United States
| language       = Kinyarwanda with English and French subtitles
| budget         = 
| gross          = 
}}

My Neighbor, My Killer ( ) is a 2009 French-American documentary film directed by Anne Aghion that focuses on the process of the Gacaca courts, a citizen-based justice system that was put into place in Rwanda after the 1994 genocide. Filmed over ten years, it makes us reflect on how people can live together after such a traumatic experience. Through the story and the words of the inhabitants of a small rural community, we see survivors and killers learn how to coexist.

==Critical reception and awards==
It was an Official Selection at the 2009 Cannes Film Festival,    the winner of the Human Rights Watchs Nestor Almendros Prize  for courage in filmmaking, a nominee for the 2009 Gotham Best Documentary Award  and the winner of the best documentary at Montreal Black Festival. The film has been shown at film festivals and universities around the world, including in Rwanda. It is rated 100% on Rotten Tomatoes. 

My Neighbor, My Killer is the feature length based on the Gacaca Series, composed of three films that Anne Aghion made over the years in Rwanda, one of which - "In Rwanda We Say…The Family That Does Not Speak Dies"  - received an Emmy Award.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 