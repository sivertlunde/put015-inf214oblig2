The Stig-Helmer Story
{{Infobox film
| name           = The Stig-Helmer Story
| image          = The Stig-Helmer Story.jpg
| alt            = 
| caption        = 
| director       = Lasse Åberg Bo Jonsson Serina Björnbom Christer Abrahamsen
| writer         = Lasse Åberg
| narrator       = 
| starring       = Lasse Åberg Jon Skolmen
| music          =  
| cinematography = Mats Axby
| editing        = 
| studio         = 
| distributor    = AB Svensk Filmindustri
| released       =  
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

The Stig-Helmer Story is a 2011 Swedish comedy film directed by Lasse Åberg. The film is the sixth in the series about Stig-Helmer Olsson.

== Cast ==
*Lasse Åberg as Stig-Helmer (71 years old)
*Tobias Jacobsson as Stig-Helmer (20 years old)
*Filip Arsic Johnsson as Stig-Helmer (12 years old)
*Jon Skolmen as Ole Bramserud
*Ida Högberg as Annika (18 years old)
*Tove Edfeldt as Hjördis
*Bill Hugg as Julle
*Jonas Bane as Biffen (20 years old)
*Nils Bodner as Biffen (12 years old) Andreas Nilsson as the major
*Elisabet Carlsson as Märta
*Stefan Sauk as Biffens father
*Cecilia Ljung as Svea
* Patrick Jakobsson as Mr Cheng

== Reception ==
The film has received mixed reviews. The Swedish newspaper Aftonbladet rated the film as 3/5,  Expressen as 2/5,  Metro (Sweden)|Metro as 3/5  and Svenska Dagbladet as 4/6. 

== Referencers ==
{{reflist|refs=

   

   

   

   

}}

== External links ==
*  
*  
*    

 

 

 
 
 

 