Udaan (1997 film)
{{Infobox film
| name           = Udaan
| image          =
| caption        =
| director       = Asrani|
| producer       = Guddu Rangeela,  Lalit Yadav,  Raju Narula
| writer         =
| starring       = Rekha Saif Ali Khan Prem Chopra Madhoo
| music          = Anand-Milind
| cinematography = Sripad Natu
| editing        = A. Muthu
| distributor    =
| released       =  1997
| runtime        =
| country        = India
| language       = Hindi
| Budget         =
| preceded_by    =
| followed_by    =
| awards         =
| Gross          =
}}
Udaan (English translation - Flight) is 1997 Hindi language movie directed by Asrani and starring Rekha, Saif Ali Khan, Prem Chopra, Danny Denzongpa, Dalip Tahil, Asrani, Deven Varma, Annu Kapoor, Mohnish Bahl.

==Plot==
A wealthy industrialist is killed by a villainous trio. The industrialists daughter Varsha (Rekha) slowly starts to unravel the truth about her fathers death, she is quickly disposed off to an asylum. Varsha escapes from there with the help of a man named Raja (Saif Ali Khan). Now, Varsha and Raja together start on a difficult journey to bring the villains to justice. The film with other subjects also deals with product Adulteration and injustice in society.

==Cast==
*Rekha	...	Varsha Sahay
*Saif Ali Khan	...	Raja
*Madhoo	...	Madhu 
*Prem Chopra	...Mr. Sood
*Dalip Tahil	...Mr. Sethi
*Mohan Joshi	...Dr. Bhatia
*Danny Denzongpa	... Mr. Rana
*Asrani	... Baba Shree Shree 108
*Deven Verma	... Madhus Uncle (Mama)
*Saeed Jaffrey	... Mr. Sahay 
*Mohnish Bahl	... Inspector Sharma
*Annu Kapoor	...	Anand Lagpade 
*Narendra Gupta	
*Rana Jung Bahadur	... Inmate
*Makrand Deshpande	...	Masoombhai Dayachan 
*Achyut Potdar	...	Chandra Prakash

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jab Jab Dekhun Tujhe"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Chahun Tujhe" 
| Vinod Rathod, Sadhana Sargam
|-
| 3
| "Badal Garja Bijlee Chamki"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Kal Raat Sapne Mein"
| Kumar Sanu, Alka Yagnik
|-
| 5
| "Chhat Ke Upar"
| Shweta Shetty
|-
| 6
| "Angrezee Bhasha Mein" Poornima
|}

== External links ==
*  

 
 
 


 