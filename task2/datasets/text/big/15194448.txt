The Things of Life
{{Infobox Film
| name           = The Things of Life
| image	         = The Things of Life FilmPoster.jpeg
| caption        = Film poster
| director       = Claude Sautet
| producer       = Jean Bolvary Raymond Danon Roland Girard
| screenplay     = Sandro Continenza Jean-Loup Dabadie Claude Sautet Paul Guimard
| based on       = Intersection (novel)|Intersection by Paul Guimard
| starring       = Michel Piccoli
| music          = Philippe Sarde
| cinematography = Jean Boffety
| editing        = Jacqueline Thiédot
| distributor    = Compagnie Française de Distribution Cinématographique 
| released       =  
| runtime        = 89 minutes
| country        = France
| language       = French
| budget         = 
}}
 1970 Cinema French film directed by Claude Sautet. It is based on the novel Intersection (novel)|Intersection by Paul Guimard. It was nominated for the Palme dOr at the 1970 Cannes Film Festival.    The film was a success in France with 2,959,682 admissions becoming the 8th highest earning film of the year.  http://www.jpbox-office.com/fichfilm.php?id=8905&affich=france 

== Plot == Alfa Romeo Giulietta Sprint swerves to avoid it and crashes into an orchard, hurling the driver onto the grass. As he loses consciousness, he revisits the essential things which made up his life.

A Paris architect in his forties driving to a meeting at Rennes, he had quarrelled with his lover Hélène the night before. They were due to leave together for a job he was offered in Tunis but he had not signed the documents. And he had agreed to take his teenage son Bertrand, who lived with his estranged wife Catherine, for a fortnight to the familys holiday home on the Île de Ré. Stopping at a café, he wrote a letter to Hélène calling everything off, but did not post it. Driving past a wedding, he realised that the letter was quite wrong and he should in fact marry Hélène, so giving a purpose to the rest of their lives.

Rushed to hospital in Le Mans, he does not recover and Catherine as his widow is given his effects, including the unsent letter which she tears to pieces. Hélène arrives at the hospital and is told by a nurse that she is too late.

== Cast ==
* Michel Piccoli as Pierre Bérard 
* Romy Schneider as Hélène Haltig
* Gérard Lartigau as Bertrand Bérard 
* Lea Massari as Catherine Bérard
* Jean Bouise as François
* Boby Lapointe as the driver of the animal transporter
* Hervé Sand as the lorry driver Jacques Richard as the nurse
* Betty Beckers as the female hitchhiker
* Dominique Zardi as the male hitchhiker

==Awards==
* 1969 Louis Delluc Prize: won
* 1970 Cannes Film Festival Palme dOr: nominated

==Remake== director Mark 1994 as Intersection (1994 film)|Intersection with Richard Gere, Sharon Stone and Lolita Davidovich in the leading roles.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 