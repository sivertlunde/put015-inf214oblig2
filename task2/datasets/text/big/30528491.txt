The Texas Rangers (1936 film)
{{Infobox film 
 | name = The Texas Rangers
 | caption = 
 | director = King Vidor
 | producer = King Vidor
 | writer = King Vidor  Elizabeth Hill Louis Stevens Walter Prescott Webb
 | starring = Fred MacMurray Jack Oakie Jean Parker
 | music = Gerard Carbonara
 | cinematography = Edward Cronjager
 | editing = Doane Harrison
 | distributor = Paramount Pictures
 | released =  
 | runtime = 98 minutes
 | language = English
 | country = United States
 | budget = 
 | image = The Texas Rangers VideoCover.jpeg
}}
 Western film Best Sound (Franklin B. Hansen) at the 1936 Oscars.      Retrieved 2011-08-09. 
 Texas Rangers who must arrest an old friend turned outlaw.  The supporting cast features Lloyd Nolan and George "Gabby" Hayes.

==Cast==
*Fred MacMurray as Jim Hawkins 
*Jack Oakie as Henry B. "Wahoo" Jones 
*Jean Parker as Amanda Bailey 
*Lloyd Nolan as Sam "Polka Dot" McGee  Edward Ellis as Major Bailey 
*Benny Bartlett as David
*Frank Shannon as Capt. Stafford Frank Cordell as Ranger Ditson 
*Richard Carle as Casper Johnson 
*Jed Prouty as District Attorney Dave Twitchell
*Fred Kohler as Jess Higgens
*George "Gabby" Hayes as Judge Snow

==Sequel==
The movie was followed by a sequel in 1940 called The Texas Rangers Ride Again with a new cast.

==Remake== Streets of Laredo.

==References==
 

== External links ==
*  
* 

 

 
 
 
 
 
 
 
 
 
 