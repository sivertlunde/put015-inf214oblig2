Hercules Unchained
 
{{Infobox film
| name = Hercules Unchained (Ercole e la regina di Lidia)
| image = Herculesqueenlydia.jpg
| image_size = 215px
| alt = 
| caption = Original release poster
| director = Pietro Francisci
| producer = Bruno Vailati Ferruccio De Martino
| writer = Ennio De Concini Pietro Francisci
| starring = Steve Reeves Sylvia Lopez Sylva Koscina
| music = Enzo Masetti
| cinematography = Mario Bava
| editing = Mario Serandrei
| studio = Lux Film
| distributor = Warner Bros. (USA)
| released =  
| runtime = 97 minutes  
| country = Italy France  
| language = Italian
| budget =
| gross = $2,500,000   
}}
Hercules Unchained ( , "Hercules and the Queen of Lydia") is a 1959 Italian-French epic fantasy feature film starring Steve Reeves and Sylva Koscina in a story about two warring brothers and Hercules tribulations in the court of Queen Omphale. The film is the sequel to the Reeves vehicle Hercules (1958 film)|Hercules (1957) and marks Reeves second - and last - appearance as Hercules.  The films screenplay, loosely based upon various Greek myths and dramas, was written by Ennio De Concini and Pietro Francisci with Francisci directing and Bruno Vailati and Ferruccio De Martino producing the film.  

==Plot== Ulysses tries to help him regain his memory, Hercules wife, Iole, finds herself in danger from Eteocles, current ruler of Ancient Thebes (Boeotia)|Thebes, who plans on throwing her to the wild beasts in his entertainment arena. Hercules slays three tigers in succession and rescues his wife, then assists the Theban army in repelling mercenary attackers hired by Polynices. The two brothers ultimately fight one another for the throne and end up killing each other; the good high priest Creon is elected by acclaim.

==Cast==
* Steve Reeves as Hercules
* Sylvia Lopez as Queen Omphale of Lydia
* Sylva Koscina as Iole
* Sergio Fantoni as Eteocles
* Mimmo Palmara as Polynices Ulysses
* Fulvio Carrara and Willi Colombini as Castor and Pollux
* Gian Paolo Rosmino as Aesculapius
* Gino Mattera as Orpheus
* Primo Carnera as Antaeus
* Cesare Fantoni as King Oedipus
* Daniele Vargas as Amphiaraus
* Carlo DAngelo as High Priest Creon
* Fulvia Franco as Anticlea
* Colleen Bennett as the prima ballerina
* Nando Cicero as Lastene

==Production==
The tale of Hercules and Queen Omphale is taken from the ancient Greek myth, of which there are several variations throughout history. Character names are drawn from a mixture of various Greek legends and plays, notably The Seven Against Thebes by Aeschylus and Oedipus at Colonus by Sophocles. Hercules line "I wove the threads   together" is a reference to his task of spinning thread and weaving with Omphales attendants. The film is only very loosely based on the source material, randomly mixing events and featuring characterizations varying from those depicted in the sources.

==Reception==
Film critic Howard Hughes argues that due to a better script, "punchier action" and more convincing acting the film was "superior to its predecessor" Hercules (1958 film)|Hercules. Concerning the cast he praises in particular the French actress Sylvia Lopez ("movingly effective") whose career ended prematurely when in 1958, soon after finishing this film, she died at the age of 28 of leukaemia. 

The film was the third most popular movie at the British box office in 1960.

==Legacy== Italian title means "Hercules and the Queen of Lydia". The film was also featured in an episode of Mystery Science Theater 3000.

==See also==
* Peplum film genre
* Samson
* Hercules
* Sons of Hercules
* Maciste
* Ursus
* Goliath
* Sandokan
* Steve Reeves

==Biography==
* 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 