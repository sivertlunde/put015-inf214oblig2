Sabrina (1954 film)
 
{{Infobox film
| name = Sabrina
| image = Sabrina 1954 film poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical re-release poster
| director = Billy Wilder
| producer = Billy Wilder
| screenplay = Billy Wilder Ernest Lehman
| based on =  
| starring = Humphrey Bogart Audrey Hepburn William Holden
| music = Frederick Hollander
| cinematography = Charles Lang
| editing = Arthur P. Schmidt
| distributor = Paramount Pictures
| released =  
| runtime = 113 minutes  
| country = United States
| language = English
| budget = $2,238,813
| gross = $4 million  (rentals)  
}}
Sabrina (Sabrina Fair in the United Kingdom) is a 1954 American romantic comedy film directed by Billy Wilder, adapted for the screen by Wilder, Samuel A. Taylor, and Ernest Lehman from Taylors play Sabrina Fair. It stars Humphrey Bogart, Audrey Hepburn, and William Holden. This was Wilders last film released by Paramount Pictures, ending a 12-year business relationship with Wilder and the company. The film was selected for preservation in the United States National Film Registry by the Library of Congress in 2002. 

==Plot==
 
Sabrina Fairchild (Audrey Hepburn) is the young daughter of the Larrabee familys chauffeur, Thomas, and she has been in love with David Larrabee (William Holden) all her life. David is an oft-married, idle playboy, crazy for women, who has never noticed Sabrina, much to her and the household staffs dismay. 

Sabrina then attends culinary school in Paris, and she returns home as an attractive and sophisticated woman. David, after initially not recognizing her, is quickly drawn to her. 

Davids workaholic older brother, Linus (Humphrey Bogart), sees this and fears that Davids imminent marriage to Elizabeth Tyson (Martha Hyer) may be endangered. If the engagement is broken off, it would ruin a great corporate deal between the Larrabee business and Elizabeths very wealthy father. Linus confronts David about his irresponsibility to the family, the business, and Elizabeth, but David is unrepentant.

Linus then tries to distract Sabrina from David by drawing her affections to himself. He succeeds, but in the process falls in love with her, though he cannot admit this even to himself.

Linus reveals his maneuver to Sabrina, leaving her disillusioned about him and David. Sabrina agrees to leave and never come back, and Linus arranges for her to return to Paris by ship the next day. 

The next morning, Linus has second thoughts and decides to send David to Paris with Sabrina. This means calling off Davids wedding with Elizabeth and the big Tyson deal, and he schedules a meeting of the Larrabee board to announce this. However, David shows up at the meeting and declares that hes decided to marry Elizabeth after all. As a result, Linus finally recognizes his own feelings for Sabrina. He rushes off to join her on the ship, and they sail away together to Paris.

==Cast==
 
 
* Humphrey Bogart as Linus Larrabee
* Audrey Hepburn as Sabrina Fairchild
* William Holden as David Larrabee John Williams as Thomas Fairchild, Sabrinas father
* Walter Hampden as Oliver Larrabee, Linus and Davids father
* Nella Walker as Maude Larrabee, Linus and Davids mother
* Martha Hyer as Elizabeth Tyson, Davids fiancée
* Marcel Dalio as Baron St. Fontanel
*  Marcel Hillaire as The Professor
* Ellen Corby as Miss McCardle, Linus secretary
* Francis X. Bushman as Mr. Tyson, Elizabeths father
*  Joan Vohs as Gretchen Van Horn
* Nancy Kulp (uncredited) as one of the house servants

==Production==
Initially,   (TCM) and Turner Entertainment. 2004.  and the role was taken by Bogart.

During production of the film, Hepburn and Holden entered into a brief, but passionate and much-publicized love affair. Bogart, meanwhile, complained that Hepburn required too many takes to get her dialogue right and pointed out her inexperience. His behavior towards Hepburn, however, was better than his behavior towards other members of the cast and crew.  

Bogart was very unhappy during the filming, convinced that he was totally wrong for this kind of film, mad at not being Wilders first choice, and not liking Holden or Wilder.  But Wilders offbeat casting produced a performance that critics generally considered successful. Bogart later apologized to Wilder for his behavior on-set, citing problems in his personal life.  
 Oscar for Best Costumes, most of Hepburns outfits are rumored to have been created by Hubert de Givenchy and chosen personally by the star. Head, as the films official costume designer, was given credit for the costumes, although the Academys votes were obviously for Hepburns attire.  Edith Head did not refuse the Oscar. In a 1974 interview, Head stated that she was responsible for creating the dresses, with inspiration from some Givenchy designs that Hepburn liked, but that she made important changes, and the dresses were not by Givenchy.  After Heads death, Givenchy stated that Sabrinas iconic black cocktail dress was produced at Paramount under Heads supervision, but claimed it was his design. 

The film began a lifelong association between Givenchy and Hepburn. It has been reported that when Hepburn called on Givenchy for the first time in Paris, he assumed that it was Katharine Hepburn in his salon. 

The location used to portray the Larrabee familys mansion in Glen Cove, New York, was Hill Grove, the home of George Lewis in Beverly Hills, California.  This mansion was later demolished during the 1960s. The location used to portray the Glen Cove train station was Glen Cove (LIRR station), which is a train station along the Oyster Bay Branch of the Long Island Rail Road. 

===La Vie en rose=== French singer Édith Piaf which had been highly popular in the English-speaking world as well as in France. The occasion for Hepburn to sing it is at the episode of Sabrinas return from Paris, when she is far more assertive than before setting out, and her life does turn rosier.

==Awards==
  for Audrey Hepburn in the film "Sabrina".]]
;Wins
* Academy Award for Best Costume Design - Edith Head   

;Nominations
* Academy Award for Best Director - Billy Wilder
* Academy Award for Best Actress - Audrey Hepburn Academy Award Walter Tyler; (Set Decoration) Sam Comer and Ray Moyer Academy Award for Best Cinematography (Black-and-White) - Charles Lang, Jr. Best Story and Screenplay - Billy Wilder, Samuel A. Taylor and Ernest Lehman

==Remakes==
In 1995, Sabrina (1995 film) was produced, starring Harrison Ford, Julia Ormond, and Greg Kinnear in the roles originally played by Bogart, Hepburn, and Holden respectively.

It also served as the inspiration for the 1994 Hindi film Yeh Dillagi, starring Akshay Kumar, Kajol and Saif Ali Khan. While there are changes to the plot, it was a considerable success at the box office. It also boosted the careers of Akshay Kumar and Kajol, both of whom got best actor nominations at the Filmfare Awards for their performances.

Manappandal (1961) was inspired by this movie in Tamil_language|Tamil.  It featured S.S.Rajendran, S.A. Ashokan and B.Saroja Devi.  It was a box-office success and ran for 100 days in many cinemas.    In Telugu also it was remade as Intiki Deepam Illaley, featuring N.T. Ramarao, Kongara Jaggaiah and B. Saroja Devi.

==References==
 

;Further reading Mark Shaw for LIFE magazine during production of the film.)
*   (LIFE article on Audrey including some of the photos from the Sabrina set.)

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 