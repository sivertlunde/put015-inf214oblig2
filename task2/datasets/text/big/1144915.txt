Delgo (film)
{{Infobox film
| name           = Delgo
| image          = Delgo.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Marc F. Adler Jason Maurer
| producer       = Marc F. Adler
| writer         = Marc F. Adler Scott Biear Patrick J. Cowan Carl Dream Jennifer A. Jones
| narrator       = Sally Kellerman Louis Gossett Jr. Val Kilmer Malcolm McDowell
| music          = Geoff Zanelli
| cinematography = Herb Kossover
| editing        = Manuel Sicilia
| studio         = Electric Eye Entertainment Corporation Fathom Studios
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $40 million   
| gross          = $694,782   
}} Anima Mundi,  the films box office was one of the lowest-grossing wide releases in recent history.  Delgo grossed just $694,782 in theaters against an estimated budget of $40 million, according to box office tracking site Box Office Mojo. The film was released independently with a large screen count (over 2,000 screens) and a small marketing budget. 20th Century Fox acquired the film rights for international and DVD distribution.    Delgo was the final film for actors Anne Bancroft and John Vernon. The film is dedicated to Bancroft.

==Plot==
After having left their own world due to a loss of natural resources, the winged humanoid Nohrin settle on Jhamora with the permission of the ground-dwelling Lokni. But some of the Nohrin, led by would-be conqueror Sedessa, believe in the superiority of their own race and try to take land away from the Lokni. The parents of Delgo, a Lokni, are killed in the resulting conflict. Nohrin King Zahn is horrified by the war and admonishes Sedessa, who then poisons the Queen and almost kills Zahn as well. She is subsequently banished, and her wings are clipped off. Delgo, meanwhile, is raised by Elder Marley, who tries to teach him how to use the power of magical stones. Once Delgo grows up, however, he gives in to his desire for revenge against the Nohrin as a whole. But then he meets Princess Kyla of the Nohrin and develops a tentative friendship with her. When she is kidnapped by Nohrin General Raius, who is actually working for Sedessa, Delgo and his friend Filo are blamed and arrested. In the Nohrin prison, Delgo meets Nohrin General Bogardus, who was forced to illegally gamble with his weapons by Raius, because Bogardus opposed an all out war with the Lokni. Delgo, Filo, and Bogardus escape into some underground caverns and eventually reach Sedessas stronghold and rescue Kyla. Together they hurry back to try to stop the war from taking place. They are too late, for the war has already begun. Bogardus meanwhile fights and defeats Raius, but is soon after mortally injured. Just as Bogardus dies, Delgo realizes that he was the Nohrin soldier who spared his life many years ago during the first war between the Nohrin and the Lokni. Meanwhile, Sedessas army of monsters join in the battle. Kyla convinces the Nohrin generals to direct their troops to stop fighting the Lokni and instead pick them up and fly them away from the battlefield. Filo then directs an entire stampede of large animals onto the battlefield, sending Sedessas minions fleeing for their lives. Delgo goes off to face Sedessa and find King Zahn, whom she has taken prisoner. He finally manages to master the stone magic, and defeats Sedessa. He also puts the past behind him by saving her rather than letting her fall to her death. However, Sedessa then attacks Kyla, who has come to Delgos aid. The two struggle and Sedessa finally falls, her artificial wings being of no use. Later, during the celebrations, it turns out Raius wasnt dead, and he makes one last attempt to kill Delgo. He is subdued by a Nohrin, but not before he throws his spear at Delgo. Then, out of nowhere, the spear breaks in mid-air. Everyone turns to look at Filo, who has finally mastered his slingshot. Later, Delgo and Kylas friendship blossoms into romance when they finally kiss.

==Cast==
* Freddie Prinze, Jr. as Delgo
* Jennifer Love Hewitt as Princess Kyla
* Anne Bancroft as Empress Sedessa
* Chris Kattan as Filo
* Louis Gossett, Jr. as King Zahn
* Val Kilmer as General Bogardus
* Malcolm McDowell as General Raius
* Michael Clarke Duncan as Elder Marley
* Eric Idle as Spig
* Kelly Ripa as Kurrin
* Burt Reynolds as Delgos father
* Brad Abrell as Spog Mary Matilyn Mouser as Baby Delgo
* David Heyer as Talusi
* John Vernon as Judge Nohrin
* Jed Rhein as Ando
* Melissa McBride as Miss Sutley, Elder Pearo
* Jeff Winter as Giddy, Lochni Man
* Armin Shimerman as Nohrin Merchant
* Don Stallings as Gelmore, Elder Kiros
* Tristan Rogers as Nohrin Officer
* Gustavo Rex as Elder Canta
* Nika Futterman as Elder Jaspin
* Susan Bennett as Melsa
* Louis K. Adler as the Soldiers
* Sally Kellerman as the Narrator

==Production== Roger Dean, Yes and Asia (band)|Asia, on their backgrounds.  A partnership with Dell provided Fathom Studios with the hardware required to render Delgo. 

==Distribution==
Distributor-for-hire  Freestyle Releasing distributed the film in 2,160 screens in the United States.

==Reception==
===Box office===
Delgo is notable for producing, at the time, the worst opening ever for a film playing in over 2,000 theaters, earning $511,920 at 2,160 sites.  According to Yahoo! Movies, this averages to approximately 2 viewers per screening.  In 2012, The Oogieloves in the Big Balloon Adventure became the new holder of this record, earning only $443,901 on its opening weekend.  Delgo is also the lowest-grossing computer-animated film of all time, with just $694,782 made worldwide, a record formerly held by the 2006 film Doogal ($7.2 million in the United States).

===Critical response=== Lord of the Rings spin-off".  Aside from the death of Bancroft, the film had several other setbacks which delayed its release.  MGM was originally expected to release the picture but an executive restructuring altered these plans. In addition, Kevin Foster, the president of Fathom Studios parent company Macquarium, died of heart failure during production, causing attention to be drawn away from the film for almost a year. 

===Awards=== Annecy (France), Anima Mundi (Brazil).  Delgo received the "Best Feature Film" award at 2008s Anima Mundi. 

==Home media== DVD extras include an audio commentary from the directors, featurettes, six deleted scenes, and the short "Chroma Chameleon". 

==Impact==
According to the Wall Street Journal, "the failure of Delgo to attract audiences reflects a glut of films in the crowded holiday corridor and highlights the challenges facing films made and marketed outside the Hollywood system." 

==See also==
* List of animated feature films
* List of computer-animated films List of biggest box office bombs

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 