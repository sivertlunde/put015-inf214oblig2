The Rebel (2007 film)
{{Infobox film
| name           = The Rebel
| image          = The Rebel poster.jpg
| caption        = The film poster.
| director       = Charlie Nguyen
| producer       = Jimmy Pham Nghiem Charlie Nguyen Johnny Tri Nguyen
| writer         = Charlie Nguyen Johnny Tri Nguyen Dominic Pereira
| starring       = Johnny Tri Nguyen Ngo Thanh Van Dustin Nguyen
| music          = Christopher Wong
| cinematography = Dominic Pereira
| editing        = Charlie Nguyen Ham Tran
| distributor    = Cinema Pictures The Weinstein Company
| released       =  
| runtime        = 103 minutes
| country        = Vietnam Vietnamese French French
| budget         = US$1.5 million   
}} 2007 Cinema Vietnamese martial arts film directed by Charlie Nguyen and starring Johnny Tri Nguyen, Dustin Nguyen and Ngo Thanh Van. It premiered on April 12, 2007 at the Vietnamese International Film Festival in Irvine, California. It was released on April 27, 2007 in Hanoi and Ho Chi Minh City and played as the Closing Night film at the 2007 VC FilmFest in Los Angeles.

==Plot==
The setting is 1922, in French Indochina|French-occupied Vietnam, and anti-French rebellions by peasants have emerged all over the country. In response, the French have activated units of Vietnamese secret agents to track and destroy the rebels. One agent is Le Van Cuong. Although he has a perfect track record, Cuongs conscience is troubled by the sea of Vietnamese blood he has spilled. Following the assassination of a high-ranking French official, Cuong is assigned to seek and kill the notorious leader of the resistance. Cuong encounters Vo Thanh Thuy, a relentless revolutionary fighter and the daughter of the rebel leader. She is captured and imprisoned by Cuongs cruel superior, Sy. Cuong suspects that Sy knew about the attack on the French official before it happened, and could have prevented it. Suspicious, he warns Thuy that her organization has a mole, helps break her out of prison and becomes a fugitive himself. Her fiery patriotism inspires Cuong, and he develops feelings for the young woman as well. Meanwhile, Sy is tracking Cuong and Thuy, knowing the pair will lead him to Thuys father.

==Cast and characters==
===Primary Characters===
*Johnny Tri Nguyen as Le Van Cuong - The French-trained protagonist, who is a master of martial arts.  He is working as an agent under the French colonizers to put an end to a rebellions in Vietnam, but is secretly wracked with guilt over his role.
*Ngo Thanh Van as Vo Thanh Thuy - The rebel leaders daughter who is used by the agents as a pawn to track down her father. She is also a skilled martial artist.
*Dustin Nguyen as Sy - Cuongs sadistic superior in the secret police.  He is a formidable fighter with almost supernatural abilities, and is not adverse to using torture.

===Secondary Characters===
*Chanh Tin Nguyen as Cuongs Father - Cuongs Father once had power within the former Vietnamese regime until the French took over the country. Now he wastes away most of his days in an opium house. Cuong blames his father for something that happened to his mother years ago.
*Nguyen Thang as Hua Danh - Another Vietnamese agent working with Sy to stop the rebellion. His specialty is knife fighting.
*Stephane Gauger as Dereu - Head of the French Secret Police force. He has been stationed in Vietnam for 10 years and looks forward to retirement so he can return to France. He often insults and condescends to Sy for his failures.
*David Minetti as Tessier - A powerful French officer who Cuong faces off with in one of the films pivotal fight scenes. 

==Production==
"The Rebels" cast and crew shot for 80 days in Vietnam, where the local film industry is still developing. They had to deal with a number of obstacles, including crew members who got sick, actors who were injured, and cultural officers who monitored the productions every move. In an interview with Johnny Tri Nguyen, he states that one of the hardest problems in making the film was finding the right actress to play the female lead.

Parts of the film, mainly street scenes in the first half of the film, were shot in the old port town of Hội An (standing in for Hanoi).

==Reception==
 , where the film was screened twice.]]
The film premiered in Vietnam in April 2007, where it proved to be a huge success, garnering the highest box office gross for a locally made film up to that point.  At the time, it was the most expensive Vietnamese film with a budget of $1.5 million. The Rebel was released in the United States as a two-disc DVD set on September 30, 2008 (from Dragon Dynasty). In the special features, Johnny Nguyen, Dustin Nguyen, and Ngo Thanh Van not only provide audio commentary, but also dubbed their own characters for the films English language track. It was also featured at the 2007 Bangkok International Film Festival,  the Austin Film Festival, the Hawaii International Film Festival, and was the Opening Night screening at the 2007 Mammoth Film Festival. 
  Johnny Tri Nguyen. Veronica Ngo.
 
  Dustin Nguyen. Veronica and Johnny. Charlie Nguyen.
 

==Honours and awards== VIFF 2007 Feature film of 15th Vietnam Film Festival

==See also==
* List of Dragon Dynasty releases

==References==
 

==External links==
 

* 

 
 
 
 
 
 
 
 
 