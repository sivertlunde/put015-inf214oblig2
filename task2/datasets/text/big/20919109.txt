Forty Thousand Horsemen
 
 
{{Infobox film name           =  Forty Thousand Horsemen image          = Forty Thousand Horsemen.jpg caption        =  director  Charles Chauvel producer       = Charles Chauvel writer         = Charles Chauvel  Elsa Chauvel E. V. Timms starring  Grant Taylor Chips Rafferty Pat Twohill music          = Lindley Evans  cinematography = George Heath Frank Hurley John Heyer editing        = William Shepherd studio = Famous Feature Films distributor    = Universal Pictures (Australia) Goodwill Pictures Corporation Of Local Origin
New York Times (1923-Current file)   14 Aug 1941: 21.  Monogram Pictures (USA) released       = 26 December 1940 (Australia) 22 August 1941 (UK)  14 August 1941 (USA) runtime        = 100 minutes (Aust) 89 mins (UK) country        = Australia language       = English budget         = ₤30,000  
| gross = £130,000 
}} Charles Chauvel. Battle of Beersheba which is reputedly "the last successful cavalry charge in history". The film was clearly a propaganda weapon, to aid in recruitment and lift the pride of Australians at home during World War II. It was one of the most successful Australian movies of its day. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p. 192. 

==Prologue==

When Germany stretched greedy hands towards the Middle East in the war or 1914-1918, a great cavalry force came into being.

They were the men from Australia and New Zealand - The ANZACS - the "Mad Bushmen" - the men from "Downunder". Call them what you will - their glories can never grow dim.

They met the Germanised Army in the burning desert of Sinai.

They fought and suffered to emerge triumphant - the greatest cavalry force of modern times.

To these dauntless riders and their gallant horses this story is dedicated. To them with pride, their own sons are saying today -

"The touch you threw to us, we caught and now our hands will hold it high. Its glorious light will never die!"  

==Plot==
In 1916 Jerusalem, German troops led by Von Schiller arrest French wine seller Paul Rouget for spying and hang him. His daughter Juliet goes into hiding dressed as a boy and starts spying on the Germans.

Three members of the Australian Lighthorse, Red, Larry and Jim, are enjoying themselves (including a game of two-up)  on leave in Cairo, when called to fight the Turks. They take part in several battles including the march to Ogratina and the Battle of Romani. Red is separated from the others after one battle and has his life saved by Juliet, who he thinks is an Arab boy.

Red is reunited with his friends and they arrive at an Arab village. He meets Juliet and realises she was the boy who saved his life. They begin a romance.
 Battle of Battle of Beersheba, and stops Von Schiller before he detonates the explosives. The Germans and Turks are defeated and a wounded Red is reunited with Juliet.

==Cast==
  Grant Taylor as Red Gallagher
*Betty Bryant as Juliette Rouget
*Pat Twohill as Larry
*Chips Rafferty as Jim
*Eric Reiman as Von Schiller
*Joe Valli as Scotty
*Kenneth Brampton as German officer
*Albert C. Winn as Sheik Abu
*Harvey Adams as Von Hausen
*Norman Maxwell as Ismet
*Harry Abdy as Paul Rouget
*Pat Penny as Captain Seidi
*Charles Zoli as cafe owner
*Claude Turton as Othman
*Theo Lianos as Abdul
*Roy Mannix as Light Horse sergeant
*Edna Emmett
*Vera Kandy
*Iris Kennedy
*joy Heart
*Michael Pate as Arab
 

==Production==

===Development===
Chauvel was the nephew of Sir Harry Chauvel, commander of the Australian Light Horse during the Sinai and Palestine Campaign and had long planned a film based on the exploits of the Light Horse. It was originally to be titled Thunder Over the Desert.  

To raise funds for a movie, Chauvel shot a £5,000 "teaser" sequence, consisting of a cavalry charge based around the Battle of Beersheba. The cost for this was paid for by Herc McIntyre, managing director of Universal Pictures in Australia who was a long-time friend and associate of Chauvels. Filming of this sequence took place on 1 February 1938 on the Cronulla sand dunes using a cavalry division of the Australian Light Horse, which had been performing in the New South Wales sesquicentenary celebrations. 

The charge was filmed by a four-camera unit, composed of Frank Hurley, Tasman Higgins, Bert Nicholas and John Heyer.  A cavalryman was injured during the shoot. 

In 1939 Chauvel and McIntyre formed Famous Films Ltd to make the movie.  Chauvel used the footage to raise the budget, which was originally announced at £25,000.  £5,000 was provided by McIntyre and £10,000 from Hoyts. The New South Wales government agreed to guarantee a bank overdraft of £15,000 although they did not invest directly in the movie.  

===Casting===
The movie marked the first lead role for Grant Taylor, who rose to prominence in Dad Rudd, MP (1940). It was the first sizeable role for Chips Rafferty, who had been cast after a screen test.  Chauvel described him as "a cross between Slim Summerville and Jame Stewart, and has a variety of droll yet natural humour."  Joe Valli reprised his Scottish soldier from Pat Hannas Digger Shows.

Betty Bryant was a discovery of Elsa Chauvels. She beat out Pat Firman for the role. 

===Shooting===
Shooting began in May 1940. Interiors were shot in the Cinesound studios at Bondi which Chauvel leased from Cinesound Productions for a three-month period. A second unit was used to build a desert village at Cronulla. The battle scenes were shot there in July and August, using the First Light Horse (Machine Gun) Regiment.  

==Censorship==
After the films preview, the Commonwealth film censor, Creswell OReilly, requested three major cuts – display of the dancing girls in a cabaret, the love scene between Red and Juliette in a hut, and alleged cruelty to horses during the final charge.   This threatened Chauvels ability to export the film and screen it in Victoria. Eventually the Minister for Customs, Eric Harrison, overruled the decision and allowed the movie to be shot uncut.  The movie was also passed uncut in Victoria. 

==Release==
Reviews were overwhelmingly positive.   The critic from the Sydney Morning Herald claimed that "there have been some good Australian films before this one, but Forty Thousand Horsemen has every right to be regarded as the first really great Australian picture." 

===Box office===
It was a massive success at the box office, grossing £10,000 within its first three weeks of release, enabling Famous Features Ltd to buy out the interest of the New South Wales government for £15,000.     The film was seen by 287,000 in Sydney alone during a ten-week run on first release. 

Female lead Betty Bryant was sent to Singapore for the films premiere in June 1941. While there she met MGM executive Mauriece Silverstein who she would later marry, leading to her retirement from acting.  
 

===Foreign release===
The movie was released in the USA by Sherman S. Krellberg for Monogram Pictures and was very well received. 

"Yippee for brawling, boisterous entertainment", wrote the critic for the New York Times, praising Betty Bryant ("whatever it is that leaps across the celluloid barrier, she has") although claiming the story was "foolish".  Forty Thousand Horsemen, Otherwise the Anzacs of World War I, Charges Into the Globe Theatre
T.S.. New York Times (1923-Current file)   15 Aug 1941: 13.  The Los Angeles Times said the film was "conventional in formula but enlivened by stirring battle scenes – and new faces." Australia Sends Spectacular Film, 40,000 Horsemen
Scheuer, Philip K. Los Angeles Times (1923-Current File)   25 Dec 1941: 16.  "Contains all the color and lusty vigor of the men themselves" said The Washington Post. The Anzac Heroes Ride Again
The Washington Post (1923-1954)   18 Feb 1943: 14. 

It earned over ₤40,000 in the UK.   

In 1954 the film was cut down to 50 minutes for screening on US television. 

==References==
 
* 

==External links==
* 
*  
*  at National Film and Sound Archive
*  at Australian Screen Online
*  at Oz Movies
 
 

 
 
 
 
 
 