Que viva la lucha
{{Infobox film name           = Que viva la lucha image          = caption        = director       = Gustavo Vazquez producer       = Gustavo Vazquez writer         =  starring       = music          = Camilo Landa cinematography = Luis Angel Martel editing        = Jonathan Parra released       = 2007 runtime        = 54 minutes country        = Mexico language       = Spanish
}}

¡Que viva la lucha! is a 2007 film directed and produced by Gustavo Vazquez about Lucha libre in Tijuana, Mexico, considered to be a form of extreme lucha libre. The film follows Extreme Tiger, an up-and-coming professional luchador from Tijuana, on a journey that places him in a mask vs. hair match against Joe Líder. In addition, he follows other new talent, Angel Negro Jr. and Pancho Cachondo. In the process, the film also interviews other luchadores, promoters, commissioners, families, and fans who discuss the cultural significance of lucha libre as sport, ritual, and spectacle.The film includes music by Carne Cruda, Marziano, and Nortec Collective. ¡Que viva la lucha! premiered at the 30th annual Mill Valley Film Festival on Saturday, Oct. 13, 2007, and has won two awards; at the Latino Film Festival and at the San Francisco International Film Festival.   

==Production==
While reminiscing with old friend Edmundo Soto about his childhood experience with lucha libre in Tijuana, Vazquez and Soto decided undertake this documentary project in order to capture the ritual immersed in the lucha libre culture. Vazquez comments on the production of the documentary stating "I shot on video because it allowed for high shooting ration. During the wrestling matches we had three cameras in order to capture the action from multiple angles and to get the viewer closer to the real experience".  The production spanned two years as it followed emerging Tijuana wrestlers and their dual lives and encountered some obstacles. While filming, Extreme Tiger had acquired appendicitis, which stalled production, but filming in Tijuana did have its advantages due to its more lenient filming and liability regulations.

==Synopsis==
The film opens at the Auditorio de Tijuana to scenes of wrestlers and cheering fans. Following these scenes, the film proceeds to breakdown the concept of lucha libre and its cultural significance by following the dual lives of professional luchadores: Extreme Tiger, Angel Negro Jr., and Pancho Cachondo, all working class individuals from Tijuana who become local heroes in the community the moment they put on their masks. The film breaks down Tijuana’s lucha libre components respectively:

==Los Técnicos== good versus evil, underdog versus bully, the noble hero outwitting the corrupt nemesis- all themes which play out within Mexican lucha libre. ¡Que viva la lucha! provides captures the dual lives led by professional wrestlers who represent the good (Los Técnicos) versus the evil (Los Rudos) amongst other various themes. The film introduces Extreme Tiger, a local who repairs cars by day, and wrestles as a Face (professional wrestling)|técnico by night. Within the context of the film, Extreme Tiger is a new star in Tijuana’s lucha libre who is quickly moving up the ranks, and aspires to take his professional wrestling career abroad to Japan. Ultimately, his journey brings him to a mask versus hair match against his rival, where he risks losing his mask, and more importantly, his career. Extreme Tiger prevails and takes Tijuana wrestling to Japan. The técnicos within lucha libre are characterized as charismatic, righteous, and always seeking justice.

==Los Rudos== Mexican congressman thong at a bar/table dance. The role of the rudos in lucha libre serve to represent the corrupt, dishonest evil-doers in society.

==La Máscara==
"The mask is one the unique iconographies that distinguishes Mexican wrestling". Through the mask, wrestlers are able to construct and transform into a character that contrasts to their working-class identities. 
The film discusses how the Mexican wrestling mask is associated with ancestral indigenous masking and is representative of Mexican culture and history.

==Los Exoticos, Los Referis, Las Luchadoras and other participants==
The film also addresses the incorporation of other participants in addition to the rudos and técnicos in lucha libre. Other participants include women wrestlers, gay wrestlers (Diamantina), midget wrestlers, and wrestling referees. The film captures the interactions between the spectators and wrestlers, such as Diamantina, and the spectators ambivalent feelings towards these wrestlers. In response to the mixed reactions of Los Exoticos by spectators of Lucha Libre, Vazquez states "On the one hand, Lucha Libre means free wrestling. That can loosely translated as everything goes. That’s a public space in the wrestling arena, where everything goes"   

==Los Fanaticos and Spectators==
¡Que viva la lucha! demonstrates how lucha libre in Tijuana is enjoyed by a vast range of spectators ranging from young children to older generations, and across genders. It is a space where fanatics and spectators are able to actively participate through the cheering, insults, and mixed emotions. The film also interviews extreme fanatics to collect lucha libre memorabilia, and dedicated fans that have gained free access to shows (La Tiburona). In response to the spectatorship of lucha libre, Vazquez states "The arenas are considered a free place to let out all anger, excitement, passions"   

==References==
 

== External links ==
*  

 
 
 