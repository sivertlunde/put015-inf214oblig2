Change of Sex
 
{{Infobox Film
| name           = Cambio de Sexo
| image          = Cambio de Sexo.jpg
| caption        = 
| director       = Vicente Aranda 
| producer       = Jaime Fernandez Cid
| writer         = Vicente Aranda   Joaquin Jordá   Carlos Durán
| starring       = Victoria Abril   Lou Castel  Fernando Sancho Rafaela Aparicio Bibiana Fernández|Bibí Andersen
| music          = Ricardo Miralles 
| cinematography = Néstor Almendros  José Luis Alcaine
| editing        = Maricel Bautista 
| distributor    = Morgana Films 
| released       =   May 13, 1977
| language       = Spanish
| budget         = 
| runtime        = 108 minutes |
| country        = Spain |
|
|}} 1976 Spanish film, written and directed by Vicente Aranda. It stars Victoria Abril. The film dramatizes the story of a young effeminate boy, who moves to the city to explore his desire to become a woman.

==Synopsis ==
In a small town, José Maria is a teenager with a big problem: He imagines that inside of him he is really a woman. His family environment makes things more difficult for him. His rude father is a male chauvinist who afraid of his son softness put him to work chopping wood and tries to force him to have sex with a prostitute. However, José Maria flees from that adventure feeling guilty. He escapes to the city where he tries to be accepted as woman with tragic results. While working as a hair dresser he meets, Bibi, a transvestite who works in a cabaret and José Maria goes to work with her. He is not José Maria anymore, but Maria José. In the cabaret, he falls in love with Durán, the owner of the bar. Durán, reluctant at the beginning, nevertheless starts a relationship with Maria José, who wants to go all the way, and plans to travel to London to have a sex change operation.

==Cast==
*Victoria Abril - José María/ María José
*Lou Castel -  Durán
*Fernando Sancho - José Marias father
*Rafaela Aparicio - Pilar
*Daniel Martin - Pedro
*Montserrat Carulla - José Marias mother
*Bibi Andersen - Bibi
*Mario Gas - Alvaro
*Maria Elias - Lolita

==External links==
* 

 

 
 
 
 
 
 
 


 
 