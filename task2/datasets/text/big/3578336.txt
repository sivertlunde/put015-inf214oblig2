Bubble (film)
{{Infobox film name = Bubble image      = Bubble poster.jpg director   = Steven Soderbergh writer     = Coleman Hough producer = Gregory Jacobs starring   = Debbie Doebereiner, Dustin James Ashley, Misty Dawn Wilkins music = Robert Pollard cinematography = Steven Soderbergh editing = Steven Soderbergh distributor = Magnolia Pictures budget     =  gross      =  US$261,966  released   =   runtime    = 73 minutes country    = United States language   = English
}}

Bubble is a 2005 film directed by Steven Soderbergh.  It was shot on high-definition video.
 Full Frontal. maiden name, respectively).

The film uses non-professional actors recruited from the Parkersburg, West Virginia / Belpre, Ohio area, where the film was shot. For example, the lead, Debbie Doebereiner, was found working the drive-through window in a Parkersburg KFC.

Bubble was released simultaneously in movie theaters and on the cable/satellite TV network HDNet Movies on January 27, 2006. The DVD was released a few days later on January 31.

It was nominated for Best Director for Steven Soderbergh at the 2007 Independent Spirit Awards.

Bubble is the first of six films Soderbergh planned to shoot and release in the same manner.

The score for the movie was composed by Robert Pollard, who lives in Ohio. {{Citation
  | last = Olsen
  | first = Mark
  | title = The Indie Boy in the Soundtrack Bubble
  | newspaper = New York Times
  | date = 2006-01-29
  | url = http://www.nytimes.com/2006/01/29/movies/29olse.html}}
 

==Plot==
The movie is about three people living along the Ohio River who are just able to make ends meet. The first character is Martha (Debbie Doebereiner), a portly middle-aged woman seen getting ready for work and taking care of her elderly father. On the way to work at the doll factory, she picks up co-worker Kyle (Dustin James Ashley). He is, as she tells him, Marthas best (and perhaps only) friend, as she has to spend most of her time working and taking care of her father. Kyle is a tall, thin young man who is intensely shy and very quiet, although he opens up with Martha.

In order to meet demand, the doll factory hires Rose (Misty Dawn Wilkins), an attractive single mother. Martha appears to dislike Rose from the beginning, perhaps feeling threatened by Kyles staring at the younger woman. Rose has a two-year-old daughter and used to work in a nursing home. Rose irritates Martha by asking her for a ride to her second job, though Martha agrees to drive her.

Rose later asks Martha if she would like to make a little extra money, as she has a date and needs a Babysitting|baby-sitter. Martha accepts but frames her response as if she is doing Rose the favor. It is not until Kyle arrives to pick Rose up that Martha learns the identity of Roses date. Martha later says to Rose that being so surprised made her feel like an idiot.

During the date Kyle and Rose share that they each dropped out of high school, Kyle because of having social anxiety disorder and Rose because she wanted to rebel. Later in the evening, at Kyles house, Rose sifts through his drawers and steals some cash while Kyle is grabbing a couple of beers to drink. When Kyle drops Rose off at home, he refuses to go inside with her because he felt a "weird vibe" from Martha earlier.

Almost as soon as Rose enters her house, her ex-boyfriend (who is the father of her child) barges in and accuses her of stealing money and marijuana from his house. They get in a shouting match and he eventually leaves. Martha sits silently on the couch while they argue. After he leaves, she asks Rose if he was her daughters father. Rose angrily tells her to mind her own business.
 pawning jewelry (which she claims were inherited from family members) in order to afford some fishing equipment as a gift for Kyle. She also makes a trip to the beauty parlor. When she arrives at Kyles house to give him the gift, he tells her of Roses murder. Surprised, she says she knows nothing. When the detective talks to her, she repeats this.

During follow-up questioning, the detective tells Martha that the fingerprints found around Roses neck match hers. He urges her to confess, as there can be no doubt as to the murderers identity. Martha maintains her innocence. Soon the detective visits Marthas father, who is being cared for by someone else (revealed in the credits to be Marthas niece). When the detective tells Marthas father of her arrest, he seems more saddened than shocked.

Kyle visits Martha in prison. As they sit with a glass wall between them, Martha pleads with him to help her. She swears that she doesnt know what happened and that she did not murder Rose. Though she mentions a headache and Roses rudeness. Kyle appears wary of Martha and unsure. Later, in her jail cell, she sees a bright light, then herself standing over Roses dead body, and she mutters, "Oh my God."
 CAT scan, blackouts and highly abnormal behavior.

==Reception==

Soderbergh was nominated for Best Director at the 2007 Independent Spirit Awards for this movie.

==References==
 

== External links ==
* , Boing Boing
* 
* 
* http://www.journalwatchdog.com/crime/1442-woman-gets-11-years-after-pleading-guilty-to-dui-killing
 

 
 
 
 
 
 
 
 
 