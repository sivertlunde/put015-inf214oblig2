The Craving (1918 film)
 
{{Infobox film
| name           = The Craving
| image          = The Craving (1918 film).jpg
| caption        = Theatrical poster to The Craving (1918) Francis Ford John Ford
| producer       =
| writer         = John Ford
| screenplay     = Francis Ford
| story          = Francis Ford
| starring       = Francis Ford Mae Gaston
| music          =
| cinematography = Edward Gheller
| editing        = Bluebird Photoplays
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent English intertitles
| budget         =
}}
 silent drama John and Francis Ford. The film is now presumed lost film|lost.   

==Cast== Francis Ford as Carroll Wayles
* Mae Gaston as Beulah Grey
* Peter Gerald as Ala Kasarib
* Duke Worne as Dick Wayles
* Jean Hathaway as Mrs. Wayles

==Plot==
Carroll Wayles (Ford) is a chemist who has discovered the formula for a high explosive. This is a secret All Kasarib (Gerald) wishes to learn. 

He uses his ward, Beulah Grey (Gaston), who is under his hypnotic power, to tempt Wayles with liquor, knowing that he has formerly been addicted to drink, but had overcome it. Wayles returns to his former mode of living. Kasarib gains the ascendency over him and learns the secret. Wayles’ spirit is taken on an imaginary trip over battlegrounds and through scenes of lust to show him the pitfalls that await slaves of the flesh. 

So Wayles awakens a changed man. He goes to the laboratory of Kasarib, where there is a struggle, during which an explosion kills Kasarib. Wayles and the ward are then free to marry each other. 

==See also==
*List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 