Vaaranam Aayiram
{{Infobox film
| name = Vaaranam Aayiram
| image = Vaaranam-aayiram-wallpaper.jpg
| caption = Film poster
| director = Gautham Vasudev Menon
| producer = Viswanathan Ravichandran
| writer = Gautham Vasudev Menon Suriya Simran Simran Divya Spandana Sameera Reddy
| music = Harris Jayaraj
| cinematography = R. Rathnavelu Anthony Gonsalves Aascar Film Pvt. Ltd  (India)  Cloud Nine Movies  (worldwide)  Bharat Creations  (United States|US) 
| released =   
| runtime = 169 minutes
| country = India
| language = Tamil
| gross = 
}} Tamil romantic Suriya plays dual lead roles, with Simran (actress)|Simran, Divya Spandana, and Sameera Reddy . The film had been under production since late 2006, and was released worldwide on November 14, 2008. It was dubbed and released in Telugu as Surya s/o Krishnan.

The film illustrates the theme of how a father often came across his sons life as a hero and inspiration, whose death was deeply mourned in the end. As a tribute to the father of the director, the film opened in several countries with critical acclaim. The film was produced by Venu Ravichandran and has musical score by Harris Jayaraj, making Vaaranam Aayiram his final project with Gautham Menon before the formal break-up of their partnership. It won many awards including the National Film Award for Best Feature Film in Tamil. The film was also a commercial success, eventually becoming the biggest hit in Suriyas career then. 

==Plot==
The film begins with elderly Krishnan (Suriya (actor)|Suriya) returning home after a haircut. Once he reaches home he vomits a profuse amounts of blood and is kept to bed rest. After the doctor checks him, he is reported dead after a few moments of grief with his wife Malini (Simran (actress)|Simran), daughter Shreya, and daughter-in-law Priya (Divya Spandana). This news is informed to his son Surya (Suriya (actor)|Suriya) who is in Kashmir on a military mission traveling in a helicopter, to rescue a kidnapped journalist from terrorists . He begins to shed tears and starts thinking about his memorable moments with his father and the story of how his parents loved each other during their college days.
 Regional College of Engineering,Tiruchirapalli. He soon proposes to her. She rejects him, saying that they have known each other for a very short period and its not possible to fall in love without taking time to understand him. Surya then tells her that wherever she goes, he will find her.

Surya is disturbed constantly by Meghnas thoughts and consequently ends up going to her house. There she reveals him that she is going to University of California, Berkeley in a week to pursue higher education, Surya bids adieu to her. After that Krishnan, Suryas father, gets his first mild heart attack and is admitted to a hospital.
 Greyhound bus. When he reaches there, he witnesses the Oklahoma City bombing and realizes that Meghna is amongst those injured. She dies on her way to the hospital. He meets Shankar Menon at the airport who tries to console him with words and offers him encouragement.A heartbroken Surya returns to India.

Surya still cant take the situation. He starts hanging out with his friends to get over his loss. He meets Priya (Divya Spandana), his sisters friend. However, the thoughts of losing Meghna still haunts him, and he resorts to taking drugs. Suryas parents try to get him to break the habit by locking him up in his room and removing him from all external contact. Once they detect his withdrawal symptoms, they ask him to take a long break to rejuvenate himself. Subsequently, he takes a leisure trip to Kashmir. There he learns that Aditya, the son of Shankar Menon, the person who inspired him in the airport was kidnapped. He visits them in Delhi. On seeing the lethargic attitude of the police, he takes matters into his own hands and goes in search of the kidnappers. After days of roaming streets and gathering information, he locates the kidnapper (Prithviraj (Tamil actor)|Prithviraj) and the place where Aditya is kept. He battles the kidnappers single-handedly, overpowers them and rescues Aditya.

Back home, Surya starts to go out with his sisters friend Priya. One day Priya proposes to him, but he doesnt respond. Surya decides that the only way to forget the pain in his body is to prepare it for bigger things. He goes on a strict workout training and joins the army. After six years, Priya comes to the Indian Military Academy where he is posted to meet him and proposes again; this time he accepts. They marry and have a son. Suddenly Krishnan is diagnosed with throat cancer and is operated on. After some days Surya is called upon to go on a mission to rescue a journalist which brings the scene back to the present day. After completing the military mission with the help of a masked man (Gautham Menon) who tells them where the culprits were, Surya returns home to see his dead father. Surya, his sister Shriya, Malini and Priya let go his fathers ashes in the sea and Malini relates her husbands life to Surya by uttering a verse from a prayer. The film ends on a happy note as the four walk away smiling; conveying the message that "Whatever happens, Life has to go on."

==Cast== Suriya as Krishnan and Surya Krishnan Simran as Malini Krishnan Divya Spandana as Priya
* Sameera Reddy as Meghna
* Deepa Narendran as Shreya Krishnan
* Shreya Gupta as Teen Shreya 
* Babloo Prithviraj as Asad, the kidnapper Ganesh Janardhanan as Antony (Librarian)
* Sathish Krishnan as Suryas Friend
* Danny as Priyas father
* Indra as Priyas mother
* Rajeevan as Meghanas father
* Gautham Menon as Commando informer
* Veera Bahu

==Production==

===Development===
Following Kaaka Kaaka in 2003, the director Gautham Menon and Surya Sivakumar expressed their desire to work together on a film. In early 2005, the pair got together for a film tentatively titled Chennaiyil Oru Mazhaikaalam which featured Asin Thottumkal in the lead role and Daniel Balaji in a supporting one. However, after a photo shoot, the film was delayed and then subsequently called off.    Nevertheless, in 2006, producer Venu Ravichandran signed up the pair for a new film, which they confirmed was not going to be a sequel of their previous venture; it was going to be a romantic thriller. Initially titles such as Naan Thaan  and Udal Porul Aavi    were considered for the project, but Menon announced the title was Vaaranam Aayiram, title derived from "Thirupaavai" with the literal meaning of "the strength of a thousand elephants".   
  served as shooting spot in "Adiye Kolluthe" song]]
Before the start of shooting, technical crew members were signed up, and the shoot commenced without a leading lady on 24 November 2006 at a nightclub in Chennai. In 2007, it was announced that a 10-day shoot in Afghanistan would be followed by shooting in Malaysia, Russia and the United States of America. However, the film only completed a shoot at the University of California, Berkeley in Berkeley, California|Berkeley, USA, and shooting was not held in Afghanistan. {{cite web|author=|year=2007|title=Vaaranam Ayiram in Afghanistan
|publisher=Cinesouth.com|accessdate=2008-01-22|url=http://www.cinesouth.com/masala/hotnews/new/07042007-5.shtml}} 

In November 2007, Gautham Menon decided to send the reels back to Chennai from San Francisco. Two of the production managers were assigned the task of bringing the reels to the producers office. But they stayed in Singapore for a couple of days before returning to Chennai. Following their arrival, it was understood the reels went missing in the hotel they stayed in at Singapore, but a search by the police proved unsuccessful. The whole crew of Vaaranam Aayiram was in a fix as the reels held fight scenes, songs and other scenes worth $500,000. Soon after, the reels were found with the Singapore airport authorities. {{cite web|author=|year=2007|title=Vaaranam Aayiram reels go missing!
|publisher=Chennai365.com|accessdate=2008-01-22|url=http://www.chennai365.com/news/varanam-aayiram-reels-go-missing/}}  The film finished its shoot in August 2008.   

A song was filmed by an assistant cameraman with a high-tech camera, costing about Rs. 1.5 crore, fastened to his hip. While the assistant was moving behind the actors canning the shots, he tripped unexpectedly and the camera broke to pieces. The shot resumed a day later with a similar camera. 

Vaaranam Aayiram was believed to be based on the Dutch film by Mike van Diem, Character (1997 film)|Character; others have claimed that it is inspired by the 1994 Hollywood film, Forrest Gump, starring Tom Hanks. Menon too admitted the film will draw inspirations from Forrest Gump but will be fixed to suit the Indian audience.    Furthermore Menon stated that Surya Sivakumar will be seen playing the dual roles of a 21-year-old and his father and that the film will be society themed. It was set to release in 2007 and was delayed for a 14 November 2008 release.    The audio of Vaaranam Aayiram was released on 24 September 2008. Menon has described the film as "autobiographical and a very personal story and if people didn’t know, that 70% of this   is from my life".   Suriya lost weight using capsules and prepared a six-pack for the film through an eight-month fitness regime.  

===Casting===
After the cancellation of Chennaiyil Oru Mazhaikaalam, originally starring Surya Sivakumar, Asin Thottumkal and Daniel Balaji, Gautham Menon, keeping the male leads the same, wanted to opt for a new heroine to begin Vaaranam Aayiram. Deepika Padukone was signed up and set to make her debut in Tamil films; she backed out due to the inactivity and signed the Bollywood blockbuster, Om Shanti Om.     Soon after, another confirmed heroine, Andrea Jeremiah, was dropped from the film for unknown reasons.    Asin expressed her desire to be a part of the project, but the male lead, Surya, refuted her offer citing that she had walked out of his previous film, Sillunu Oru Kadhal and refused to act opposite him.   
 Tabu had rejected the role.    TV actress Deepa Venkat and playback singer Chinmayi dubbed for actresses Simran Bagga and Sameera Reddy respectively.

==Soundtrack==
{{Infobox album|  
| Name = Vaaranam Aayiram.
| Type = soundtrack
| Artist = Harris Jayaraj
| Cover = Vaaranam aayiram cover.jpg
| Released = 24 September 2008
| Recorded = Chennai Feature film soundtrack
| Length =
| Label = Sony BMG
| Producer = Harris Jayaraj
| Last album = Dhaam Dhoom (2008)
| This album = Vaaranam Aayiram (2008)
| Next album = Ayan (film)|Ayan (2009)
}}
{{Album ratings
| rev1 = Behindwoods
| rev1Score = *  
| rev2 = Rediff
| rev2Score = *  
}}
The film has seven songs composed by Harris Jayaraj. All Lyrics are penned by Thamarai, expect Yethi Yethi song (Na. Muthukumar). The songs from the albums were released to four leading radio stations in Chennai, with one song per station, in attempt to popularise it five days before the official launch. The move by Sony BMG was the first of its kind in Indian cinema.   

===Tamil (Original)===
{| class="wikitable"
|-
! Song title !! Singers !! Length  (m:ss)  !! Description
|- Krish and Shruti Haasan || 5:17 || Filmed at the University of California and locations across the state with Surya and Sameera. Depicts Meghna falling in love with Surya.
|-  Devan and Prasanna || 6:11 || Filmed in various locations on Surya and Sameera. Depicts the moment after Surya tells Meghna about his love.
|-
| "Yethi Yethi Yeththi" || Kunal Ganjawala, Benny Dayal, Naresh Iyer and Solar Sai || 4:55 || Filmed in locations across South India with Surya and background dancers. Depicts Suryas school days.
|-
| "Mundhinam Parthene" || Naresh Iyer, Prashanthini and Suriya || 5:43 || Filmed with Surya and Simran. Depicts when Krishnan loved Malini during their college days.
|-
| "Oh Shanthi Shanthi" || Clinton Cerejo and S. P. B. Charan || 3:05 || Depicts Surya searching for Meghna on the university campus.
|-
| "Ava Enna" ||   folk-style song with Surya. Depicts Surya becoming hysterical from memories of Meghna.
|-
| "Annal Mele Panithuli" || Sudha Raghunathan || 5:22 || Filmed on Surya and Divya. Depicts the moment when Surya finally falls in love with Priya.
|-
| "Ragasiyam" ||   || 3:22 || 
|-
 
|}

===Telugu (Dubbed)===

All Lyrics are Penned by Veturi Sundararama Murthy.

{| class="wikitable"
|-
! Song title !! Singers !! Length  (m:ss)  !! Description
|- Krish and Bhargavi Pillai || 5:17 || Filmed at the University of California and locations across the state with Surya and Sameera. Depicts Meghna falling in love with Surya.
|-  Devan and Prasanna || 6:11 || Filmed in various locations on Surya and Sameera. Depicts the moment after Surya tells Meghna about his love.
|-
| "Yegasi Yegasi" || Benny Dayal, Naresh Iyer and Chandran || 4:55 || Filmed in locations across South India with Surya and background dancers. Depicts Suryas school days.
|-
| "Monna Kanipinchavu" || Naresh Iyer and Prashanthini || 5:43 || Filmed with Surya and Simran. Depicts when Krishnan loved Malini during their college days.
|-
| "Oh Shanthi Shanthi" || Krish and S. P. B. Charan || 3:05 || Depicts Surya searching for Meghna on the university campus.
|-
| "Adhey Nannu" ||   folk-style song with Surya. Depicts Surya becoming hysterical from memories of Meghna.
|-
| "Nidhare Kala" || Sudha Raghunathan || 5:22 || Filmed on Surya and Divya. Depicts the moment when Surya finally falls in love with Priya.
|-
|}

===Critical response===
The album received tremendous response from music critics as well as the public. Indiaglitz stated "Varanam Aayiram is no doubt one of the best from Harris Jeyaraj and Gautham duo. It is energetic and entertaining. A must listen musical score that is serene even while repeated."  Malathy Sundaram from Behindwoods gave a 4/5 rating and praised the album calling it a "blockbuster" further citing "The guitar simply enthralls you. So do the native drums. There is a slight monotony in the instrumentation, but it doesn’t seem to affect the overall effect. Thaamarai needs to be complimented for the soft lyrics."    Pavithra Srinivasan from Rediff gave a 3.5/5 rating and said "Put together, you definitely feel that Harris Jayaraj has set out to provide  quality music with a difference."   

==Release==
The satellite rights of the film were sold to Kalaignar TV|Kalaignar. The film was given a "U" certificate by the Indian Censor Board.

===Box office=== blockbuster for Suriya since Ghajini (2005 film)|Ghajini, grossing  5.66 crore in Chennai alone.    In overseas market, the film grossed $921,007 from both United Kingdom and Malaysia.     
The film has grossed around 80 crore for its theatrical run.

===Critical reception===
Varanam Aayiram received positive reviews. Sify stated that it is a "film straight from the heart".  Indiaglitz reviewed that it "proves to be a tame affair" but stated that "Suriya the actor rocks throughout".  Behindwoods rated it 2.5/5 stating that "what could have been a subtly told story turns into a sloppy fare" and "there are touching and heart-warming moments in the movie but, they are few and far between".  Rediff gave the film 3/5 and called it a "moving story", further mentioning that "It might be just a feather in Gautams hat. As for Surya, its an ostrich plume, a justified triumph."  The Hindu said, "The same combo came together for Kaakha Kaakha and signed off with a flourish not so long ago! Of course Vaaranam … does have some great moments, but it’s a lengthy film, and you feel it!" 

==Accolades==
In addition to the following list of awards and nominations, prominent Indian film websites (including Rediff, Sify and Behindwoods) named Vaaranam Aayiram one of the 10 best Tamil films of 2008. Before its release, the films title had appeared on film websites "most awaited" lists.

{| class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|- 56th National 2008 National Film Awards National Film Best Feature Film in Tamil Venu Ravichandran Gautham Menon
|rowspan="3"  
|- 56th Filmfare 2008 Filmfare Awards South Filmfare Best Best Actor Surya Sivakumar
|- Filmfare Best Best Supporting Actress Simran (actress)|Simran
|- Filmfare Best Best Director Gautham Menon
|rowspan="2"  
|- Filmfare Best Best Film Venu Ravichandran
|- Filmfare Best Best Lyricist Thamarai (Nenjukkul Peithidum)
|rowspan="2"  
|- Filmfare Best Best Male Playback Naresh Iyer (Mundhinam Partheney)
|- Filmfare Best Best Male Playback Hariharan (singer)|Hariharan (Nenjukkul Peithidum)
| 
|- Filmfare Best Best Music Director Harris Jayaraj
|rowspan="7"  
|- 2008 Tamil Nadu State Film Awards Tamil Nadu Special Award for Best Actor Surya Sivakumar
|- Tamil Nadu Best Art Director Rajeevan
|- Tamil Nadu Best Audiographer Ravi
|- 2008 International Tamil Film Awards ITFA Best Best Music Director Harris Jayaraj
|- 2008 Vijay Awards Vijay Award Best Actor Surya Sivakumar
|- Vijay Award Best Supporting Actress Simran (actress)|Simran
|- Vijay Award Best Art Director Rajeevan
|rowspan="5"  
|- Vijay Award Best Cinematographer
|R. Rathnavelu
|- Vijay Award Best Choreographer Brindha (choreographer)|Brindha Ava Enna
|- Vijay Award Best Debut Actress Sameera Reddy
|- Vijay Award Best Director Gautham Menon
|- Vijay Award Favourite Director Gautham Menon
| 
|- Vijay Award Best Editor Anthony (film Anthony
|rowspan="5"  
|- Vijay Award Best Entertainer Surya Sivakumar
|- Vijay Award Favourite Hero Surya Sivakumar
|- Vijay Award Best Female Playback Singer Sudha Raghunathan (Anal Mele)
|- Vijay Award Best Film Venu Ravichandran
|- Vijay Award Favourite Film Venu Ravichandran
|rowspan="14"  
|- Vijay Award Best Lyricist Thamarai (Nenjukkul Peithidum)
|- Vijay Award Best Make-Up Yogesh & Banu
|- Vijay Award Best Male Playback Hariharan (Nenjukkul Peithidum)
|- Vijay Award Best Music Director Harris Jayaraj
|- Vijay Award Favourite Song Harris Jayaraj (Ava Enna)
|- 2008 Meera Isaiaruvi Tamil Music Awards Rising Star Singer – Female Shruti Haasan
|- Best Picturisation Award
|R. Rathnavelu
|- Best Art Direction Rajeevan
|- Best Lyricist Award Thamarai Nenjukkul Peithidum
|- Best Album of the Year Harris Jayaraj
|- Best Playback Singer – Male Hariharan (singer)|Hariharan Nenjukkul Peithidum
|- Best Music Director Harris Jayaraj
|- Voice of the Year Karthik (singer)|Karthik Ava Enna
|-
|rowspan=3| 2009 South Scope Awards Most Stylish Actor – Tamil Surya Sivakumar
| 
|- Most Stylish Music Director – Tamil Harris Jayaraj
|rowspan="2"  
|- Style Youth Icon of the Year Surya Sivakumar
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 