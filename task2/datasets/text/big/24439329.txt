Trip for Tat
 
{{Infobox Hollywood cartoon|
| cartoon_name = Trip For Tat Granny
| image=
| caption =
| director = Friz Freleng
| story_artist = Michael Maltese Tom Ray   Virgil Ross
| backgrounds = Tom OLoughlin
| layouts = Hawley Pratt
| voice_actor = Mel Blanc June Foray (uncredited)
| musician = Milt Franklyn
| producer = Warner Bros. Pictures
| release_date = October 29, 1960 (USA premiere)
| color_process = Technicolor
| runtime = 7 min (one reel)
| movie_language = English
}}

Trip For Tat is a 1960 Merrie Melodies animated short starring Sylvester (Looney Tunes)|Sylvester, Tweety, and Granny (Looney Tunes)|Granny.  Although it contains a new plot, wherein Granny and Tweety travel to various locations (Paris, Swiss Alps, Japan, and Italy) while Sylvester tries to eat Tweety in every one, the cartoon is mostly made up of footage from previous cartoons.  Here are the cartoons which the short borrows animation from, in order of appearance:

* Tweetys S.O.S. (1951):  The entire boat sequence.
* Tree Cornered Tweety (1956):  The sequence where Sylvester tries to catch Tweety on skis, as well as the bridge scene (with the American fisherman changed to a stereotypical Japanese fisherman).
* Tweet Tweet Tweety (1951):  The sequence where Sylvester swings towards Tweety on a balcony while barely avoiding a construction pillar.
* A Pizza Tweety Pie (1958):  The final sequence where Sylvester eats spaghetti in the restaurant.

The only new animation in the short is at the beginning when the world tour is described to Granny, the finger painting sequence, when Sylvester is first in The Alps and Japan, and Tweety looking up at Sylvester cutting through the floorboards.

 
 
 
 
 
 