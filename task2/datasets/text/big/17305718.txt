All Is Well (film)
{{Infobox film
| name           = All Is Well 
| image          = 
| caption        = 
| director       = Umesh Shukla
| producer       = Bhushan Kumar Kishan Kumar Shyam Bajaj Varun Bajaj
| co-Producer    = Ajay Kapoor
| writer         = Sumit Arora Niren Bhatt
| starring       = Abhishek Bachchan Asin Rishi Kapoor Supriya Pathak
| music          = Himesh Reshammiya
| cinematography = Sameer Arya
| editing        = Sanjay Sankla
| studio         = T-Series Alchemy productions
| distributor    =
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
All Is Well is an upcoming Bollywood family drama film directed by Umesh Shukla and produced by Bhushan Kumar, Krishan Kumar, Shyam Bajaj and Varun Bajaj and co-produced by Ajay Kapoor. It stars Abhishek Bachchan, Rishi Kapoor, Asin and Supriya Pathak in lead roles. The film tackles a thorny issue with a social message similar to Shuklas previous venture OMG – Oh My God!. The film is scheduled for release on August 21, 2015.

==Cast==
* Abhishek Bachchan
* Rishi Kapoor
* Asin
* Supriya Pathak

==Plot==
All Is well is about a road trip undertaken by Rishi Kapoor and Abhishek Bachchan, who are later joined by their mother. Reportedly, Asin also plays an important role in the film.

==Filming==
Following the two week workshop from October 1 involving lead actors Asin and Abhishek Bachchan,    the shooting of the film began on the auspicious Eid on 16 October 2013 which was confirmed by Abhishek Bachchan on Twitter.  The director of the film Umesh Shukla is shooting the film in Nashik,Himachal, Pradesh, Sikkim, Dubai & London and is expected to shoot in different cities in India.  In May 2014 it was reported that all the cast of the film were shooting in Shimla. 

==Development==
Director Umesh Shukla spotted many locations for filming in Dubai and different cities in India.  After the location was fixed, the whole team including lead actors Abhishek Bachchan and Asin were part of a workshop to prepare shooting for the film.

==References==
 

==External links==
*  

 
 
 
 