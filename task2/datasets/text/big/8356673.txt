O. Henry's Full House
{{Infobox film
| name           = O. Henrys Full House
| image          = O. Henrys Full House Poster.jpg
| caption        = Theatrical film poster Henry King
| producer       = André Hakim
| screenplay     = Richard L. Breen Walter Bullock Ivan Goff Ben Hecht Nunnally Johnson Charles Lederer Ben Roberts Lamar Trotti
| based on       =  
| starring       = Fred Allen  Anne Baxter Jeanne Crain Farley Granger Charles Laughton Oscar Levant  Marilyn Monroe Jean Peters Gregory Ratoff Dale Robertson David Wayne Richard Widmark
| narrator       = John Steinbeck Alfred Newman
| cinematography = Lloyd Ahern Lucien Ballard Milton R. Krasner Joseph MacDonald
| editing        = Nick DeMaggio Barbara McLean William B. Murphy
| distributor    = 20th Century Fox
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1 million (US rentals) 
}}
 Alfred Newman. The film is narrated by author John Steinbeck, who made a rare on-camera appearance to introduce each story.

==The five stories==
==="The Cop and the Anthem"===
Directed by Henry Koster, from a screenplay by Lamar Trotti, it stars  Charles Laughton, Marilyn Monroe and David Wayne.

==="The Clarion Call"===
Directed by Henry Hathaway, from a screenplay by Richard L. Breen, it stars Dale Robertson and Richard Widmark.

==="The Last Leaf"===
Directed by Jean Negulesco, from a screenplay by Ivan Goff and Ben Roberts, it stars Anne Baxter, Jean Peters and Gregory Ratoff.

==="The Ransom of Red Chief"===
Directed by Howard Hawks, from a screenplay by Ben Hecht, Nunnally Johnson and Charles Lederer, it stars Fred Allen, Oscar Levant and Lee Aaker.

==="The Gift of the Magi"=== Henry King, from a screenplay by Walter Bullock, it stars Jeanne Crain and Farley Granger.

==References==
 

==External links==
*  
*  
*  
*  
*  at  
*  at  
*  by Chris Hicks from   (deseretnews.com)

 
  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 