Raktham
{{Infobox film 
| name           = Raktham
| image          =
| caption        =
| director       = Joshiy
| producer       = Jagan Appachan
| writer         = Kaloor Dennis
| screenplay     = Kaloor Dennis Madhu Srividya Jagathy Sreekumar Johnson
| cinematography = NA Thara
| editing        = K Sankunni
| studio         = Jagan Pictures
| distributor    = Jagan Pictures
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by Joshiy and produced by Jagan Appachan. The film stars Prem Nazir, Madhu (actor)|Madhu, Srividya and Jagathy Sreekumar in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
*Prem Nazir Madhu
*Srividya
*Jagathy Sreekumar
*Sankaradi
*Captain Raju
*Balan K Nair
*MG Soman
*Mala Aravindan
*Roja Ramani

==Soundtrack== Johnson and lyrics was written by RK Damodaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ayyapantamma Neyyapam chuttu || K. J. Yesudas, Vani Jairam || RK Damodaran || 
|-
| 2 || Manjil Chekkerum || K. J. Yesudas, Vani Jairam || RK Damodaran || 
|-
| 3 || Sukham oru greeshmamirangiya || K. J. Yesudas || RK Damodaran || 
|}

==References==
 

==External links==
*  

 
 
 

 