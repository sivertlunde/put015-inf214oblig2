Ping Pong Playa
{{Infobox film
| name           = Ping Pong Playa
| image          = Ping pong playa.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Jessica Yu
| producer       = Anne Clements Joan Huang Jeffrey Gou
| writer         = Jimmy Tsai Jessica Yu 
| starring       = Jimmy Tsai Andrew Vo Khary Payton Jim Lau Roger Fan  Elizabeth Sung Javin Reid Kevin Chung Peter Paige Smith Cho Scott Lowell Stephnie Weir
| music          = Jeff Beal
| cinematography = Frank G. Demarco
| editing        = Zene Baker
| distributor    = IFC Films
| released       =  
| runtime        =  96 minutes
| country        = United States Chinese
}}
Ping Pong Playa is a 2007 comedy film directed by Jessica Yu and written by Jessica Yu and Jimmy Tsai.    The story centers on a Chinese ping pong family living in California with a buffoonish and irreverent son.

==Plot==
Christopher Wang is a young man (25 years old) who lives under his Asian-American parents in a suburb in southern California. His father, who was a prominent ping pong player in his youth, owns a sports goods shop, and his mother teaches young children at the sport at the local community center. Outperformed by his older brother, Michael, who is considered by their parents as more mature and responsible and have brought pride to the family by winning regional ping pong tournaments, Chris seems to lack motivation and a realistic vision for his career, indulging in defeating young kids in sports and playing video games.

After his mother and Michael are injured in a minor car accident, Chris comes to learn about taking responsibility by taking over his mothers ping pong class and representing his family in the forthcoming ping pong tournament. With the training by his father and the support of his friends including a posse of young pupils, Chris strives to prove to those who put trust in him that he is capable of making serious effort while following through a commitment.

==Cast==
*Jimmy Tsai as Christopher "C-Dub" Wang
*Andrew Vo as Felix
*Khary Payton as JP Money
*Jim Lau as Mr. Wang
*Roger Fan as Michael Wang
*Elizabeth Sung as Mrs. Wang
*Javin Reid as Prabaka
*Kevin Chung as William Lin
*Peter Paige as Gerald Harcourt
*Smith Cho as Jennifer
*Scott Lowell as Tom
*Stephnie Weir as Cheryl Davis Sir Jonathan Oliver as Jon Howard
*Shelley Malil as DB Reddy
*Martin Chow as Jerry Lin

==References==
 

==External links==
*  
*  

 
 
 
 
 


 