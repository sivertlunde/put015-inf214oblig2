Nieng Arp
{{Infobox Film
| name           = Nieng Arp
| image          = Ladyvampire.jpg
| caption        = theatrical poster
| director       = Kam Chanty
| producer       = Kam Chanty
| writer         = Kam Chanty
| narrator       = 
| starring       = Dan Monika Sovan Makar
| music          = 
| cinematography = 
| editing        = 
| distributor    = FCI production Cambodia
| released       = August 24, 2004
| runtime        = 120 minutes
| country        = Cambodia
| language       = Khmer
| budget         = 9000 $
| preceded_by    = 
| followed_by    = 
}} 2004 Cinema Cambodian horror thriller in its release year.

==Plot==
In the middle of a long night, a young woman and her boyfriend are confronted by a group of gangsters on their way home. The girl is then violently raped and her lover is killed under the brutal action of the cruel gangsters. During her loss of consciousness, she later is possessed by an old witch because the saliva of a witchs head accidentally fell on her mouth, immediately making her part of the next generation of Ap ghosts. After becoming an Ap, she takes vengeance on the gang that raped her.

A group of students come to her house, discovering her true identity by unlocking several secrets in the basement under her lonely villa. One by one the students are killed by Ap. However, all of the female students are spared. One male survivor does escape and manages to find a new way to be rid of the evil spirit of Ap and thus save his lover, who happens to be the Aps daughter, from becoming the next Ap.

==Critical reception==
The Khmer film industry is small and new Khmer films often meet a muted response in the mainstream media. One website which did review the film, The Fright, described the film in their article
Leaks, Penanggalans and Aps, Oh My! positively, stating that it is very much in the throat-ripping tradition of Mystics in Bali and Demonic Beauty, and the film is all the better for it. The review strongly praised the camera work and lighting. They criticized the low quality of special effects, however, as not able to match expectations when it comes to special effects in modern film production. 

==Box office==
This film is considered to be the first successful Khmer horror films in recent years, having grossed over $100,000 solely from the six movie theatres in Phnom Penh. According to some reports, tickets were sold out during the opening weekend, accompanied by thousands of late viewers creating traffic jams on the streets. It set a local box office record by running for almost three months.

In the DVD version, the film consists of two discs with no special features or scene selection.    English subtitles were included on a DVD release for foreign audiences.

==Special effects==
Special effects are primarily not computer generated. In one scene, a dolls head filled with pig intestines was used to make the Aps head. Whilst not realistic, the films sound effects resulted in many viewers screaming nonetheless.

==Origins==
In the Khmer ghost legends Ap was a complete person who was able to remove its head at night to fly across the rice fields in search of sustenance, usually blood. Stomach, heart and intestines trail from her neck. 

This nocturnal female ghost with a floating head is part of popular Southeast Asian mythology, namely the folklore of Cambodia, Indonesia, Laos, Malaysia and Thai folklore|Thailand. In Laos this ghost is called Pi-Kasu while in Thailand it is known as Krasue and in Malaysia Penanggalan.

==References==
 

==External links==
*  
* 

 
 
 
 
 