Badges of Fury
 
 
 
{{Infobox film
| name           = Badges of Fury
| image          = BadgesOfFury.jpg
| caption        = Film poster
| film name      = {{Film name
 | traditional    = 不二神探
 | simplified     = 不二神探
 | pinyin         = Bú Èr Shén Tàn
 | jyutping       = Bat1 Ji6 San6 Taam3
 | poj            = }}
| director       = Wong Tsz-ming
| producer       = Chui Po Chu
| screenplay     = Carbon Cheung
| starring       = Jet Li Wen Zhang Michelle Chen Liu Shishi Ada Liu Raymond Wong
| cinematography = Kenny Tse
| editing        = Angie Lam
| studio         = Beijing Enlight Pictures Hong Kong Pictures International My Way Film Company Intrend Entertainemt & Production HK Screen Art
| distributor    = Hong Kong: Newport Entertainment Worldwide: Easternlight Films 
| released       =  
| runtime        = 97 minutes
| country        = China    Hong Kong 
| language       = Mandarin
| gross          = US$46,495,296 
}}
Badges of Fury ( , also known as The One Detective)  is a 2013 Chinese-Hong Kong  action comedy film directed by Wong Tsz-ming in his directorial debut. The film stars Jet Li and Wen Zhang in their third collaboration after Ocean Heaven and The Sorcerer and the White Snake. The film was theatrically released on 21 June 2013. 

==Plot==
 
In just 3 days, three cases of Smiling Murder shock Hong Kong. As he looks into the homicide, the young detective Wang Bu Er (Wen Zhang), the police stations reckless buffoon, makes a shocking statement that this is a serial murder.

He and his buddy Huang Fei Hong (Jet Li) embarks on an investigation full of excitement and unexpected events. Huang may appear to be no less muddle-headed than Wang, but in reality, he is the real master of kung fu, and would, without fail, at the most crucial moments, help Wang get out of sticky situations.

Wang initially believes that budding actress Liu Jin Shui (Liu Shishi) is the prime suspect, but later, she is found to be innocent. Next, he shifts his focus on her sister Dai Yiyi (Ada Liu), among others. Eventually, Wang decides to pose as Liuis boyfriend to lure out the murderer. The closer he gets to the truth, the greater the danger he is in.

==Cast==
*Jet Li as Huang Fei Hong (黃非紅,a homonym/spoof spelling of 黄飞鸿Wong Fei-hung), a veteran inspector
*Wen Zhang as Wang Bu Er (王不二,literally "not stupid Wang"). a young detective
*Michelle Chen as Angela, Huang and Wangs superior
*Liu Shishi as Liu Jin Shui (劉金水), a female star
*Ada Liu as Dai Yiyi (戴依依), Lius older half-sister
*Leung Siu-lung as Liu Xing (劉星), Liu Jin Shuis uncle
*Stephen Fung as Liu Jin Shuis cousin
*Lin Shuang as Sun Ling (孫玲)
*Michael Tse as Yao Yi Wei (姚一偉), a ballroom dancer who is one of the victims of the Smiling Murder who died during dancing (cameo)
*Kevin Cheng as Li Tian Ci (李天賜), movie star who is one of the victims of the Smiling Murder who died during filming (Cameo) Tian Liang as Zhang Liang (鄭亮) a diver who is one of victims of the Smiling Murder who died during diving (cameo)
*Collin Chou as Chen Hu (陳虎), a wanted criminal (cameo) Wu Jing as Insurance manager (cameo)
*Bryan Leung as Uncle Lucky (祥叔) (cameo)
*Aaron Shang as Ni Chuan, Interpol liaison
*Tong Dawei as Wang Feng (王峰), a Real Estate Manager who is one of the victims of the Smiling Murder (cameo)
*Grace Huang
*Oscar Chan
*Stephy Tang as female driver (cameo)
*Huang Xiaoming as Interpol officer in black (cameo)
*Raymond Lam as Gao Min (高敏), one of the victims of the Smiling Murder who died during his second proposal to Liu Jin Shui  (Cameo) Alex Fong as Fortune teller (Cameo)
*Zhang Zilin as Huang Fei Hongs wife (cameo)
*Lam Suet as taxi driver (cameo)
*Tin Kai-Man as Party guest (cameo)
*Josie Ho (cameo)
*Joe Cheung as chef (cameo)
*Ma Yili as Commissioner (Cameo)
*Wang Zhifei as Mr. Mai
*Feng Danying as Ms. Zhou

==Production==
Badges of Fury began filming in July 2012 in Hong Kong. An accident occurred while filming a car chase scene at Sha Tin Water Treatment Works where a prop car lost control and flipped over nearly killing the cinematographer and causing nine injuries including 8 men and 1 woman and many people being stuck in the car. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 