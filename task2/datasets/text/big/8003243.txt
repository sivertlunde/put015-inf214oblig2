Resurrection (1999 film)
 
 
{{Infobox film
| name        = Resurrection
| image       = Resurrect00.jpg
| caption     = Resurrection original film poster
| writer      = Brad Mirman Christopher Lambert
| starring    = Christopher Lambert Leland Orser Robert Joy
| director    = Russell Mulcahy
| producer    = Howard Baldwin Karen Elise Baldwin
| distributor = Columbia TriStar Home Video
| released    =  1999
| runtime     = 108 minutes
| language    = English
| budget      =
}}
Resurrection is a 1999 horror film film directed by Russell Mulcahy.

==Plot==
Detective John Prudhomme, a Cajun transferred to Chicago, is assigned to investigate the savage murder of a man who has bled to death from a severed arm. A message, "He Is Coming", written in blood on the victims window, is a dark, foreboding clue. After two more victims with missing body parts are discovered, Prudhomme realises he is on the trail of a serial killer who is using the missing body parts to reconstruct the body of Christ in time for Easter.

==Filming locations==
Some of the film was shot in Chicago, Illinois and New Orleans, Louisiana, but most of it was filmed in Toronto, Canada.

==Release==
Resurrection was released in the fall of 1999 on cable television in the United States. In other countries such as France, Spain and Switzerland, it was shown in theatres. In France, it was a modest success with nearly 400,000 film admissions. The film was quite popular in Spain, with 1,198,684 admissions.

==Critical reception== Arrow in the Head called it "a taut, clever thriller, directed with kinetic style and energy" and "one of the more entertaining serial killer movies on the block." Fallon added, "If you can get past the similarities with Seven (film)|Seven, you will surely enjoy this razor sharp, nasty flick."  Chuck OLeary of FulvueDrive-in.com said the film is "a blatant rip-off of Seven, but pretty scary and unsettling in its own right."  Carlo Cavagna of About Film called Resurrection "an entertaining movie," remarking that "the cinematography is quite good by any standard, and the writing is decent enough to keep you interested." Cavagna felt that the plot was "by-the-numbers," but added that it   a new twist on the killers motivation." 

==Cast==
*Christopher Lambert — John Prudhomme
*Leland Orser — Det. Andrew Hollinsworth
*Robert Joy — Gerald Demus
*Barbara Tyson — Sara Prudhomme
*Rick Fox — Scholfield
*David Cronenberg — Father Rousell
*Jonathan Potts — Detective Moltz
*Peter MacNeill — Captain Whippley
* Phillip Williams – Detective Rousch
*Jayne Eastwood — Dolores Koontz David Ferry — Mr. Breslauer
*Chaz Thorne — David Elkins
* Darren Enkin – John Ordway
*Michael Olah- Michael Prudhomme

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 