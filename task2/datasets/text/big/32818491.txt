The Penal Code
{{Infobox film
| name           = The Penal Code
| image          =
| image_size     =
| caption        =
| director       = George Melford
| producer       = Burton L. King (producer) John R. Freuler (executive producer)  (uncredited)
| writer         = F. Hugh Herbert Edward T. Lowe Jr. (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Edward A. Kull
| editing        = Frederick Bain
| distributor    =
| released       = 1932
| runtime        = 62 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Penal Code is a 1932 American film directed by George Melford.

== Plot summary ==
 

== Cast ==
*Regis Toomey as Robert Palmer
*Helen Cohan as Marguerite ("Margie") Shannon Pat OMalley as Sergeant Detective W. J. Bender Robert Ellis as James Forrester
*Virginia True Boardman as Mrs. Sarah Palmer Henry Hall as Mr. Shannon
*Leander De Cordova as Isaac Lewin John Ince as Warden
*Murdock MacQuarrie as Lefty
*Olin Francis as McCarthy

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 

 