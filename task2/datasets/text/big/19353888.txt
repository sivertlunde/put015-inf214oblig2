Ram-Avtar
{{Infobox film
| name           = Ram-Avtar
| image          = 
| image_size     =
| caption        =
| director       = Sunil Hingorani
| producer       = Sunil Hingorani
| writer         = Anwar Khan (dialogue)   Khalid Siddiqui (screenplay and story)
| narrator       =
| starring       =  Anil Kapoor  Sunny Deol   Sridevi
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 170 min.
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Ram-Avtar is a 1988 movie starting Sunny Deol, Anil Kapoor, Sridevi and Shakti Kapoor. It was directed by Sunil Hingorani, written by Khalid, and produced by Sunil Hingorani.

==Cast==

* Anil Kapoor ... Avtar
* Sunny Deol ... Ram
* Sridevi ... Sangeeta Shano
* Shakti Kapoor ... Gundappa Swami
* Bharat Bhushan ... Rams Grandfather
* Subbiraj ... Sangeetas Father
* Yunus Parvez ... Sangeetas Accountant
* Dinesh Hingoo ... Gangiya
* Master Vikas ... Young Avtar
* Master Keval Shah... Young Ram
* Subiraj... Sangeetas Father
* Manek Irani... Markoni

==Plot==
Ram and Avtar are both childhood best friends. The difference between the two friends is that Ram would willingly make priceless sacrifice for his friend; the time when both are separated is when Ram goes abroad to further his studies. In the mean while Avtar takes up employment in an organization run by Sangeeta. Avtar is romantically attracted to Sangeeta but Sangeeta falls in love with Ram; when Ram gets to know that Avtar is in love with Sangeeta he decides to sacrifice his love for his friend sake by getting Avtar married to Sangeeta. But one of Sangeetas enemies Gundappaswami is willing to expose Ram and Sangeetas relationship to Avtar.
While dying, he tells Avtar that his wife is a woman of questionable character. Avtar starts suspecting his wife, and his wife calls upon Ram for help. How will Rams arrival assist their marriage?

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Anguli Mein Angoothi"
| Mohammad Aziz, Lata Mangeshkar
|- 
| 2
| "Na Na Karte"
| Mohammad Aziz, Udit Narayan, Anuradha Paudwal
|- 
| 3
| "Ek Duje Ke Vaste 1"
| Mohammad Aziz, Manhar Udhas
|- 
| 4
| "Teri Bewafai Ka Shikwa"
| Mohammad Aziz
|- 
| 5
| "Nigore Mardon Ka"
| Anuradha Paudwal
|- 
| 6
| "Anguli Mein Angoothi Part 1"
| Mohammad Aziz, Lata Mangeshkar
|- 
| 7
| "Anguli Mein Angoothi Part 2"
| Lata Mangeshkar
|- 
| 8
| ""Anguli Mein Angoothi Part 3"
| Mohammad Aziz
|}

==External links==
* 

 
 
 
 


 