Haunted – 3D
 
 
{{Infobox film
| name           = Haunted – 3D
| image          = Haunted3D-filmposter.jpg 
| caption        = Theatrical release poster
| director       = Vikram Bhatt
| producer       = Vikram Bhatt Arun Rangachari
| story          = Amin Hajee
| starring       = Mahaakshay Chakraborty Twinkle Bajpai Achint Kaur Arif Zakaria
| music          = Background Score: Raju Rao Songs:  Chirantan Bhatt
| cinematography = Praveen Bhatt
| editing        = Kuldeep Mehan
| distributor    = ASA Productions and Enterprises Pvt. Ltd. DAR Motion Pictures BVG Films
| studio         =
| released       =  
| runtime        = 143 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =   worldwide
}} stereoscopic 3D horror film.   The promos and first look of the film were released on 7 February 2011.  The film was released on 6 May 2011 to mixed reviews but became a surprise hit at the box office.  

The sequel of this movie Khamoshiyan said by Vikram Bhatt was released in 2015.

==Plot==
Rehan (Mahaakshay Chakraborty) is sent to a mansion by his father, who is in the real estate business. The mansions caretaker died two days ago, and the mansion is going to be sold off in ten days. Rehans father suspects this as the doing of a rival real estate company, so he sends Rehan to investigate. He meets a rag-picker on the way who tells him that only he can do it. (It means that only he can stop that hauntings of the Glen Manor.) Mysterious things happen that night. Rehan sees a girl playing piano who vanishes when the door is opened; a book drops from a bookshelf on its own and Rehan finds a letter inside. The letter, written by Meera (Tia Bajpai), tells her story from 1936 when her parents left for Delhi and left her alone with her nanny Margaret (Achint Kaur), a servant and a driver. In the absence of her parents, her piano teacher Iyar (Arif Zakaria) tries to rape her. However, she saves herself by hitting Iyer with a candle stand on his head, which results in his death. Soon, the police corroborate Meeras story, as they find explicit sketches of her in Iyars house. However, Iyer returns in the form of an evil spirit, killing both the servants and the nanny by cutting their heads. The spirit rapes Meera repeatedly, which leads her to commit suicide, but even after her death her spirit is tortured and trapped in the mansion by the evil spirit.

After reading the letter, Rehan feels sympathetic and brings a psychic (Prachi Shah) to help, but the lady leaves after realising how strong the evil spirit is and asks Rehan to do the same. Rehan ignores her warning and stays there, challenging the evil spirit by repeating its name as it is believed that saying an evil spirits name makes it more powerful.

The next morning when he wakes up he finds the rag-picker in front of him, casting a spell. The spell sends him back 75 years to 17 August 1936, the day when Meeras parents leave for Delhi. Rehan befriends Meera by following her and pretending to be lost newcomer to the town. The next day, in order to prevent the death of Iyer, Rehan comes to Meeras house with a basket of chocolates as a gift for showing him the way to the city. Rehan says that he would like to listen to Meeras performance as he is also interested in music, but Iyer asks him to leave. Rehan insists on staying outside the door so that he will not interfere in their class and can still listen to the music. As Rehan listens to the music, Iyer hits him, throwing him out on the lawn. Iyer bolts the door from inside and tries to rape Meera while Rehan tries to prevent Iyers death. But, as written in fate, Meera kills Iyer. A hurt Rehan is admitted to the hospital where he dreams about the evil spirit.

Rehan explains everything to Meera and asks her to meet him the next day at noon. The next day, Rehan, Meera and Margaret meet a priest, who tells them that far from the town there is a Dargah, a mosque where there is a Sufi Baba who could help them. He tells them to start the journey at 3:00&nbsp;pm because spirits have the strongest power at 3:00&nbsp;am and are weakest at 3:00&nbsp;pm. As the travelling continues into dusk, the three stay in a hotel, but the evil spirit kills Margaret and tries to rape Meera. Rehan fights it and takes Meera the rest of the way to the Dargah. The next day, the priest is killed by a snake. Margarets body, now possessed by the spirit, tries to stop them from entering the Dargah, but Rehan manages to put Margarets leg at the doorstep of the Dargah thereby destroying her body. Inside they find the rag-picker who sent Rehan into the past.

He tells them that miles away is a town that was being chanted by Khwaja, and there is a well in which Meera has to show the way to Iyers spirit, which is connected to Meera by a blood-stained pendant that Meera is still wearing. As they reach the town, Rehan has to drop the fire, mud, and the pendent. As Rehan is performing the act, Meera is attacked by Iyer, but Rehan manages to throw the pendant in the well. After saving her, Rehan falls into the well and reaches back to the year 2011. He finds another letter in the same place and discovers that Meera lived a happy life after that event. The house is no longer haunted.

==Cast==
* Mahaakshay Chakraborty as Rehan
* Twinkle Bajpai as Meera
* Achint Kaur as Margaret Malini
* Arif Zakaria as Professor Iyer
* Krishna Bhatt as Stevens
* Prachi Shah as Mrs. Stephens

==Reception==

===Critical response=== NDTV Movies awarded the film one and a half stars describing it as "an absolutely ridiculous horror movie that works better as comedy, unintentional of course."  Preeti Arora of Rediff also gave it one and a half stars saying that "Haunted fails at reviving the age old haunted-house premise, bringing nothing new to the platter." 

===Box office===
According to boxofficeindia.com, Haunted – 3D recorded the highest opening ever for an Indian horror film. Released with over 1044 prints, the film grossed   130&nbsp;million during its opening weekend.  By the end of its first week, the film grossed   143.0&nbsp;million.  In the second weekend, the film experienced a drop of 55% from its first weekend collections, grossing   3.50 crore for a ten day total of   180&nbsp;million.  After four weeks, the film grossed   262.5&nbsp;million in India,    and   350&nbsp;million worldwide.    It was declared a Hit by Boxoffice-india. 

==Soundtrack==
The music is composed by Chirantan Bhatt while the lyrics are penned by Shakeel Azmi and Junaid Wasi.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Jaaniya
| extra1 = Siddharth Basrur 
| lyrics1 = Junaid Wasi
| length1 = 5:07
| title2 = Mujhe De De Har Gum Tera
| extra2 = Siddharth Basrur  
| lyrics2 = Junaid Wasi
| length2 = 4:58
| title3 = Sau Baras
| extra3 = Tia Bajpai
| lyrics3 = Junaid Wasi
| length3 = 5:05
| title4 = Tera Hi Bas Hona Chahoon
| extra4 = Jojo, Najam Sheraz
| lyrics4 = Junaid Wasi
| length4 = 5:39
| title5 = Tum Ho Mera Pyaar
| extra5 = Krishnakumar Kunnath|KK, Suzanne DMello
| lyrics5 = Shakeel Azmi
| length5 = 5:06

}}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 