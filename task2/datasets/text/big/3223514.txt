Evil Alien Conquerors
 
{{Infobox film  name           = Evil Alien Conquerors image          = caption        =  director       = Chris Matheson producer       = Debra Greico Joel Hatch Erica Huggins writer         = Chris Matheson Ryan Rowe starring       = Diedrich Bader Chris Parnell Michael Weston  music          = David E. Russo cinematography = Russ Lyster editing        = Mike Murphy distributor    = First Look International released       =   runtime        = 98 minutes  country        = United States language       = English budget         = United States dollar|$500,000
}}
Evil Alien Conquerors is a film directed by Chris Matheson (who co-wrote Bill & Teds Excellent Adventure) in 2003 in film|2003, and is a blend of science-fiction and comedy. The film follows two aliens who are sent to Earth to destroy mankind, but when they arrive they are unable to complete their mission. The two aliens befriend a fast food employee who helps them.

==Plot==
Two Evil Alien Conquerors are sent to Earth with the order to behead all humans within 48 hours. If they fail, they will be destroyed by Croker, a   giant.

Kenny (Michael Weston) witnesses the Evil Alien Conquerors, My-ik (Diedrich Bader) and Du-ug (Chris Parnell), as they arrive on Earth, literally falling out of the sky with their beheading swords. He offers them shelter at his home, which he shares with Ron, an unpleasant, oversexed infomercial producer.

The conquering duo have a chance to experience Earth culture, where they become friends with Kenny, develop a fondness for Schmirnoff Ice and unexpectedly fall in love with two local women, who have a secret of their own. They begin to doubt what they were sent to do, but know they still must attempt to complete their impossible mission despite their lack of skill, any plan or experience.

The giant Croker then arrives to destroy the two inept conquerors and the rest of the Earths population, but he shrinks down to normal size during transportation. Delusional enough to still believe he is a giant, he attempts to wreak havoc on the town, obviously without success.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| Diedrich Bader || My-ik
|-
| Chris Parnell || Du-ug
|-
| Michael Weston || Kenny
|-
| Elden Henson || Ron
|-
| Phil LaMarr || Vel-Dan
|-
| Tyler Labine || Croker
|-
| Beth Grant || Sheila
|-
| Mike McShane || Rabirr
|-
| Martin Spanjers || Jimmy
|-
| Joel McCrary || Mr. Breen
|-
| NiCole Robinson || Gail
|-
| Missy Yager || Penny
|-
| Tori Spelling || Jan (uncredited)
|-
| Benjamin John Parrillo || Tan Guy (uncredited)
|-
|}

==External links==
*  
*  
*  

 
 
 
 

 