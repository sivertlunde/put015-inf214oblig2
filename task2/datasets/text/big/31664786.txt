Sons of Steel (1934 film)
{{Infobox film
| name           = Sons of Steel
| image          = 
| image_size     = 
| caption        = 
| director       = Charles Lamont
| producer       = Lon Young (producer)
| writer         = Charles Belden
| narrator       = 
| starring       = See below
| music          = 
| cinematography = M.A. Anderson
| editing        = Roland D. Reed
| distributor    = 
| released       = 1934
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Sons of Steel is a 1934 American film directed by Charles Lamont.

== Cast ==
*Charles Starrett as Phillip Mason Chadburne
*Polly Ann Young as Rose Mason
*William Bakewell as Roland Chadburne Walter Walker as John Chadburne
*Holmes Herbert as Curtis Chadburne
*Richard Carlyle as Tom Mason
*Florence Roberts as Sarah Mason
*Aileen Pringle as Enid Chadburne
*Adolph Milar as Stanislaus
*Edgar Norton as Higgins Barbara Bedford as Miss Peters
*Tom Ricketts as Williams Frank LaRue as Mike - the Foreman
*Al Thompson as Carson
*Harry Semels as Ryan
*Jack Shutta as Warehouse Manager
*Lloyd Ingraham as Draftsman
*Edward LeSaint as Mr. Herman

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 