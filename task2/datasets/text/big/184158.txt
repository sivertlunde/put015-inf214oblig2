Iris (2001 film)
 
 
 
{{Infobox film
| name           = Iris
| image          = Iris poster.jpg
| caption        = Theatrical poster
| director       = Richard Eyre Robert Fox Scott Rudin
| based on       =   Charles Wood
| starring       = Judi Dench Kate Winslet Jim Broadbent Hugh Bonneville
| music          = James Horner Roger Pratt Martin Walsh Intermedia BBC Fox Iris Productions
| distributor    = Miramax Films
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $5.5 million
| gross          = $16,153,953
}}
 British novelist John Bayley.    The film contrasts the start of their relationship, when Murdoch (Kate Winslet) was an outgoing, dominant individual as compared to her timid and scholarly partner Bayley (Hugh Bonneville), and their later life, when Murdoch (Judi Dench) was suffering from Alzheimers disease and tended to by a frustrated Bayley (Jim Broadbent) in their North Oxford home in Charlbury Road.

The film, which was directed by Richard Eyre, is based on Bayleys memoir Elegy for Iris. The beach scenes were filmed at Southwold in Suffolk, one of Murdochs favourite haunts.
 Best Actress Best Supporting Actress respectively.

==Plot==
When the young Iris Murdoch meets fellow student John Bayley at the University of Oxford, he is a naive virgin easily flummoxed by her libertine spirit, arch personality, and obvious artistic talent. Decades later, little has changed and the couple keeps house, with John doting on his more famous wife. When Iris begins experiencing forgetfulness and dementia, however, the devoted John struggles with hopelessness and frustration to become her caretaker, as his wifes mind deteriorates from the ravages of Alzheimers disease.

==Cast==
*Judi Dench as Iris Murdoch
*Kate Winslet as Young Iris Murdoch John Bayley John Bayley
*Penelope Wilton as Iriss life long friend, Janet Stone
*Juliet Aubrey as Young Janet Stone
*Timothy West as Maurice
*Samuel West as Young Maurice
*Kris Marshall as Dr Gudgeon

==Awards and nominations==
* Academy Awards
** Best Actress in a Leading Role: Judi Dench
** Best Actor in a Supporting Role: Jim Broadbent (won)
** Best Actress in a Supporting Role: Kate Winslet
{| class="infobox" style="width: 23em; font-size: 85%;"
|- bgcolor="#cccccc" align=center
! colspan="2" | Academy Awards record
|-
| 1. Best Supporting Actor
|- bgcolor="#cccccc" align=center
! colspan="2" | BAFTA Awards record
|-
| 1. Best Actress in a Leading Role
|- bgcolor="#cccccc" align=center
! colspan="2" | Golden Globe Awards record
|-
| 1. Best Supporting Actor
|}
* BAFTA Awards
** Best British Film
** Best Performance by an Actor in a Leading Role: Jim Broadbent
** Best Performance by an Actress in a Leading Role: Judi Dench (won)
** Best Performance by an Actor in a Supporting Role: Hugh Bonneville
** Best Performance by an Actress in a Supporting Role: Kate Winslet
** Best Screenplay - Adapted

* Broadcast Film Critics Association
** Best Supporting Actor - Jim Broadbent

* Golden Globe Awards
** Best Performance by an Actress in a Motion Picture - Drama: Judi Dench
** Best Supporting Actor - Motion Picture - Jim Broadbent (won)
** Best Supporting Actress - Motion Picture: Kate Winslet

*Los Angeles Film Critics Association 
** Best Supporting Actor: Jim Broadbent (also for Moulin Rouge!) (won)
** Best Supporting Actress: Kate Winslet (won)

* Satellite Awards
** Best Performance by an Actress in a Motion Picture, Drama: Judi Dench
** Best Performance by an Actor in a Supporting Role, Drama: Jim Broadbent
** Best Performance by an Actress in a Supporting Role, Drama: Kate Winslet

* Screen Actors Guild Awards
** Outstanding Performance by a Female Actor in a Leading Role: Judi Dench
** Outstanding Performance by a Male Actor in a Supporting Role: Jim Broadbent

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 