Grimm Love
 
{{Infobox film
| name = Rohtenburg
| image = Grimm Love.jpg
| caption = Promotional poster for the U.S. release of Rohtenburg
| director = Martin Weisz
| producer = Marco Weber Vanessa Coifman
| writer = T.S. Faull
| starring = Keri Russell Thomas Kretschmann Thomas Huber 
| music = Steven Gutheinz
| cinematography = Jonathan Sela
| editing = Sue Blainey
| released =  
| runtime = 87 minutes
| country = Germany 
| language = English
}}
Grimm Love (original German title Rohtenburg) is a 2006 psychological horror film inspired by the Armin Meiwes cannibal murder case. 

==Plot==
 ). Oliver dreamed of eating a willing victim, and thanks to the internet, he was able to find a volunteer, a young man Simon Grombeck (played by Thomas Huber).

The story is told in flashbacks as Katie researches these men and their pasts. Events culminate in Katies discovery of a snuff tape that documents the crime.

==Cast==
* Keri Russell as Katie Armstrong
* Thomas Kretschmann as Oliver Hartwin Thomas Huber as Simon Grombeck
* Rainier Meissner as Young Oliver
* Angelika Bartsch as Viktoria
* Alexander Martschewski as Rudy
* Nils Dommning	as Karl
* Marcus Lucas as Felix
* Pascal Andres as Young Simon
* Helga Bellinghausen as Simons Mom
* Tatjana Clasing as Hanna
* Stefan Gebelhoff as Simons Dad
* Jonas Gruber as Rainer
* Nikolai Kinski as Otto Hauser
* Renate Naujoks as House Wife
* Valerie Niehaus as Margit
* Jörg Reimers as Olivers Dad
* Sybille J. Schedwill as Frau Schinder
* Pierre Shrady	as The principal
* Axel Wedekind	as Domino

==Production==
The film is directed by music-video specialist Martin Weisz and written by T.S. Faull. The producers made Igby Goes Down. 

==Release==
The film had its world premiere at London FrightFest Film Festival on 27 August 2006 under the title Grimm Love.

In October 2006, the film won four awards at the  . 
 Federal Court of Justice annulled the ban in favor of freedom of arts.

The film has also screened at Austins SXSW Festival, among others, in advance of its US release. It is part of the 2010 Fangoria FrightFest. 

==Notes==
 

==External links==
*  
*  

 
 
 
 
 
 
 