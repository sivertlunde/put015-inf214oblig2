Pieces of Easter
{{multiple issues|
 
 
}}

{{Infobox film
| name = Pieces of Easter
| image    =  Pieces of Easter Movie Poster.jpg
| director = Jefferson Moore
| writer = Jefferson Moore
| starring = Christina Karis
| studio = Kellys Filmworks
| released =  
}}
Pieces of Easter is an independent holiday film produced by Kellys Filmworks. Written and directed by Jefferson Moore, it stars Christina Karis.

== Plot ==
Alza Reese Bennett (Christina Karis) is a young African-American executive traveling cross country to be reunited with her estranged family for the Easter holiday. An auto mishap en route leaves her stranded, and Alza winds up on the doorstep of a reclusive farmer named Lincoln (Jefferson Moore) whose main goal in life is to be left alone. In the moment of Alzas greatest desperation, a deal is struck - Lincoln and his antique Chevy pickup will transport her the rest of the way for the price of $1000; the only caveat - no interstate highways. Amidst nonstop head-butting (attributed to Alzas spoiled arrogance and Lincolns general grumpiness), the mismatched pair sets out on a three-day odyssey of misadventures along the highways and byways of America, where they encounter rat-infested motels, convenience store holdups, rural rabbit stalkers, and a star struck couple from the local trailer park. Best of all, they are forced to encounter each other, as the two worlds of the princess and the hermit collide at every turn.

== Cast ==
* Christina Karis as Alza Reese Bennett
* Jefferson Moore as Lincoln James
* Keith McGill as Willis Reese
* Pattie Crawford as Phylicia Reese
* Tom Luce as The Police Detective
* Matt Wallace as The Woodsman

== Production == Lexington served as the stand-in for Alzas fictional hometown of New Bristoll, North Carolina. Screenwriter/producer Jefferson Moore conceived the idea for the script with two things in mind: to produce a film for the "cinematically-underserved" Easter holiday, and also to entertain family audiences with a romantic comedy that was "squeaky clean."

The part of Alza was written with Christina Karis in mind; longtime friends Moore and Karis had previously worked together in the feature film Another Perfect Stranger and also in the "Mary and Martha" episode of the Stranger television miniserial.

== External links ==
*  

 
 
 
[