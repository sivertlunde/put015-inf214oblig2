Prince Vladimir (film)
{{Infobox film
| name = Prince Vladimir
| image = KnyazVladimirposter.jpg
| caption = Release poster
| director = Yuriy Kulakov
| producer = Andrey Dobrunov
| writer = Andrey Dobrunov Yuriy Batanin Yuriy Kulakov
| starring = Yuri Berkun Irina Bezrukova Sergei Bezrukov Olga Churayeva Vladimir Gostyukhin Sergey Starostin Andrei Usachev (songs)
| cinematography = Mariya Erohina
| editing = Sergei Minakin CIS and Baltic countries)
| released =  
| runtime = 78 minutes
| country = Russia
| language = Russian
| budget = 
}} animated feature converted Kievan Rus (a predecessor state of Russia, Ukraine, and Belarus) to Christianity a thousand years ago. This is the first film to focus on the story of Prince Vladimir.

==Plot==
The plot follows the events surrounding Vladimir from childhood and into adulthood.

In the beginning of the film, being under the influence of the high priest Krivzha, the Prince is a young, impulsive and cruel pagan. Fighting for supreme power, Vladimir wins a battle that kills his brother. Regretting what he has done, Vladimir does not suspect a conspiracy between the priest and the Pechenegs. Vladimir is concerned about gathering the Slavic tribes into one united state. Solving this major task, he faces obstacles, which Vladimir overcomes in the end, defeating Krivzha and winning the battle against Kurya (khan)|Kurya, a Pecheneg chief.

==Characters==
*Prince Vladimir - Grandson of the legendary princess Olga and son of great warlord Svyatoslav.
*Krivzha - A priest and wizard, pontificates on behalf of Perun, Prince Vladimirs prime martial god, but in fact just skillfully shelters himself behind this, only wishing absolute power. There is gossip that he did bad things while in the guise of a bear, but those who saw him in this guise have gone missing.
*Alexei (Aleksha) - An inquisitive and clever boy. Having lost his parents, he has been fostered by his grandfather. Kurya - A Pecheneg Prince. Used to be at enmity with Prince Svyatoslav, Vladimirs father. Since then, he has been a cruel and imperious leader of half-wild steppe tribe, planning and doing everything to invade Russian lands and harry Kiev.
*Boyan - A kind and honorable old man. Contrary to Krivzha, hes a sympathetic Slavic paganism|pagan, the "wood grandsire"; he is a part of the nature, he talks with living trees and worships them. His staff is a home for bees, which tell him about all the news of the wood. Boyan is a personification of ancient Slavic wisdom, open-mindedness and patience.
*Dobrynya - A voivode|waywode, Prince Vladimirs uncle and tutor. Harsh and straightforward. A warrior of tremendous power, sophisticated, whole-hearted in his devotion to the young Prince. Heads the Slavic troops.
*Olaf the Red-haired - A centurion. Used to be a waywode of Norwegian sea-king Harald the Blue Teeths troops.  Disagreed about carving-up the spoils of war, left the sea-king and now swears to serve young Prince Vladimir.  A brave and cruel warrior.
*Hoten and Kostyanin - Brothers, Boyans sons. Both are giants. Always together, and always searching for places where they can boast of their bravery.
*Olga - Waywode Dobrynyas daughter. Grew up without a mother, a playful and lively girl.
*Giyar - A son of Pecheneg Prince Kuri. A petty proudling, Alexeis everlasting rival.
*Anastasius - An adviser of the Byzantine Emperor.
*Byzantine Emperor - A wise politician, who sees in Russia not a rival or enemy but a possible ally, a defendant of orthodox Christians from eastern barbarians. Anna - A Byzantine Czarevna and beauty. A sister of Byzantine emperors Basil II and Constantine VIII. Clever and proud-hearted. Yaropolk - Vladimirs elder brother and by the will of Heaven – an obstacle on his way to the power.

==Background==
Production started in 1997 with research into the customs of the time period as well as character design. Originally, the story was to be told through a series of 30-minute shorts, but the idea was scrapped. The first proposal presentation of Prince Vladimir took place on April 17, 2000, at the Russian Cultural Fund. Soon after, work began in earnest, and about 120 animators were employed on the film. At the 2002 Annecy International Animated Film Festival, Prince Vladimir was named one of the worlds 12 most anticipated upcoming animated films.  The first official presentation of the finished film took place on February 3, 2006, for the press.

A sequel, Prince Vladimir - The Feat (Князь Владимир. Подвиг) was scheduled for release in 2008.

==Reception== pandering to its main sponsor, the Russian Orthodox Church. 

It is the highest-grossing Russian animated film of all time, taking in $5.8 million since its release,  and is the third highest-grossing animated film within Russia (behind Madagascar (2005 film)|Madagascar and Flushed Away).   It cost $5 million to make, however (far more than Melnitsas films, which are made for about $300,000 or a million), the film is thought to have either lost money or narrowly broken even. 

==Voice cast==
{| class="wikitable" width="40%"
|- bgcolor="#CCCCCC"
! Actor !! Role(s)
|-
| Sergei Bezrukov || Prince Vladimir
|-
| Alexander Barinov || Krivzha
|- Boyan
|- Volkhv
|- Olaf the Red-Haired
|- Dobrynya
|- Kurya
|-
| Thomas Schlecker || Alexei
|-
| Lisa Martirosova || Olga
|-
| Kolya Rastorguev || Prince Giyar
|-
| Alexei Kolgan || Kosnyatin and Khotyon
|- The Byzantine Emperor
|- Queen Anna
|-
| Alexander Ryzhkov || The Adviser
|-
| Vasiliy Dakhnenko || Anastasiy
|- Prince Yaropolk
|- Princess Olga
|}

==See also==
*History of Russian animation
*List of animated feature films
*List of historical drama films

==External links==
* 
* 
* 
*  - info and artwork from the film (2005-early 2006 entries)

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 