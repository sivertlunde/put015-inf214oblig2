A Sidewalk Astronomer
{{Multiple issues
| 
 
 
}}

{{Infobox film
| name = A Sidewalk Astronomer
| image =
| image size =
| alt =
| caption =
| director = Jeffrey Jacobs
| producer =
| writer =
| narrator = John Lowry Dobson John Angier
| cinematography = Jeffrey Jacobs
| editing = Jeanne Vitale
| studio =
| distributor =
| released = 2005 
| runtime = 79 minutes
| country = United States
| language = English
| budget =
| gross =
| preceded by =
| followed by =
}} amateur astronomer John Dobson. The film follows Dobson to state parks, astronomy clubs, and downtown streets as he promotes awareness of astronomy through his own personal style of sidewalk astronomy. The documentary includes voice overs by Dobson himself promoting his unorthodox views on religion and cosmology.

==Crews==
* Produced and directed: Jeffrey Fox Jacobs
* Director of photography: Jeffrey Fox Jacobs
* Editor: Jeanne Vitale
* Music: John Angier
* Release: Jacobs Entertainment Inc
* Running time: 78 minutes
* This film is not rated

==Review==
"AN INSPIRING FILM ABOUT AN INSPIRED TEACHER".. New York Times

==Screenings==
Shown at: Tribeca Film Festival 2005 ; Singapore Film Festival 2005; Maine Film Festival 2005; Avignon Film Festival 2005 ; Green Mountain Film Festival 2006

Taiwan Sidewalk Astronomer => familystar

==See also== John Dobson

==External links==
*  
*   by Dana Stevens
*  
*  

 
 
 
 
 
 
 
 


 