Telling the World (film)
{{Infobox film
| name           = Telling the World
| image          = 
| alt            = 
| caption        =
| director       = Sam Wood
| producer       = 
| screenplay     = Joseph Farnham Raymond L. Schrock
| story          = Dale Van Every
| starring       = William Haines Anita Page Eileen Percy Frank Currier Polly Moran
| music          = 
| cinematography = William H. Daniels John Colton
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 comedy silent film directed by Sam Wood and written by Joseph Farnham and Raymond L. Schrock. The film stars William Haines, Anita Page, Eileen Percy, Frank Currier and Polly Moran. The film was released on June 30, 1928, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==	
*William Haines as Don Davis
*Anita Page as Chrystal Malone
*Eileen Percy as Maizie
*Frank Currier as Mr. Davis
*Polly Moran as Landlady
*Bert Roach as Lane
*William V. Mong as City Editor
*Matthew Betz as The Killer 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 