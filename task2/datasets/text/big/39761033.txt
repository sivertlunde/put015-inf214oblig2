Kean (film)
{{Infobox film
| name           = Kean
| image          = 
| image_size     = 
| caption        = 
| director       = Alexandre Volkoff
| producer       = Alexandre Kamenka
| writer         = Kenelm Foss Alexandre Mozzhukhin Alexandre Volkoff
| story          = 
| based on       = play Kean by Alexandre Dumas, père
| narrator       = 
| starring       = Ivan Mozzhukhin Nathalie Lissenko
| music          = 
| cinematography = 
| editing        = 
| studio         = Films Albatros
| distributor    = Compagnie Vitagraph de France (France) Sequana Films (worldwide)
| released       =  
| runtime        = 136 minutes 
| country        = France
| language       = Silent
| budget         = 
| gross          = 
}}
Kean, ou Désordre et génie (also known as Edmund Kean: Prince Among Lovers) is a 1924 French drama film directed by Alexandre Volkoff, and starring Ivan Mozzhukhin as the actor Edmund Kean.

==Cast==
*Ivan Mozzhukhin as Edmund Kean (as Ivan Mosjoukine)
*Nathalie Lissenko as La comtesse Elena de Koefeld
*Nicolas Koline as Solomon
*Otto Detlefsen as Prince of Wales
*Mary Odette as Anna Danby
*Kenelm Foss as Lord Mewill
*Pauline Po as Ophélie / Juliette

==References==
 

==External links==
* 
* 
*  at silentera.com

 
 
 
 
 
 
 
 
 
 
 
 

 
 