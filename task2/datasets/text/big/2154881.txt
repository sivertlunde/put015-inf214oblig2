The Eiger Sanction (film)
{{Infobox film
| name           = The Eiger Sanction
| image          = Eiger_sanction.jpg
| caption        = Theatrical release poster
| border         = yes
| director       = Clint Eastwood Robert Daley David Brown
| screenplay     = Rod Whitaker Hal Dresner Warren Murphy
| based on       =  
| narrator       =
| starring       = Clint Eastwood George Kennedy Jack Cassidy Vonetta McGee
| music          = John Williams Frank Stanley
| editing        = Ferris Webster The Malpaso Company Universal Pictures
| released       =  
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = $9 million  
| gross          = $14,200,000 Hughes (2009), p.174  
}} action thriller thriller directed by and starring Clint Eastwood. Based on the novel The Eiger Sanction by Trevanian,  the film is about an art history professor and mountain climber who doubles as a professional assassin and is coerced out of retirement to avenge the murder of an old friend.   

==Plot==
College art history professor Dr. Jonathan Hemlock (Clint Eastwood) is a retired government assassin who performed "sanctions", a euphemism for officially approved killings. He also has a reputation as one of the worlds top mountaineers.
 albino ex-Nazi confined to semi-darkness and kept alive by blood transfusions. He has an uncouth, inept aide, Pope (Gregory Walcott), a man Hemlock cant stand.

Dragon wants Hemlock to kill two men responsible for the death of another government agent, code name Wormwood. Insisting he is retired, Hemlock refuses until Dragon threatens to expose Hemlocks art collection to the Internal Revenue Service|IRS. Hemlock then agrees, travels to Zurich, and carries out the first sanction for $20,000, twice his usual fee, plus a letter guaranteeing no trouble from the IRS.

Returning from Europe, Hemlock meets C2 courier Jemima Brown (Vonetta McGee), who seduces him, then steals his money and IRS exemption letter. Dragon agrees to return them if Hemlock completes another assassination. Hemlock learns the murdered C2 agent, Wormwood, was in fact his old friend Henri Baq (Frank Redmond), who once saved Hemlocks life. He then demands $100,000 plus expenses.

Hemlock is particularly qualified because the target is a member of an international mountain climbing team which, in the summer, will ascend the north face of the Eiger in Switzerland. It is arranged for Hemlock to be the American member of the team. He must kill one of the climbers. C2 is unsure of the targets identity, and Hemlock is only told that the man walks with a limp.
 Southeast Asia. die in the sun.

Hemlock travels to Switzerland with Bowman, the "ground man" (supervisor) of the climb. There they meet the other members of the climbing party—one German, one Austrian, one Frenchman—at the Hotel Bellevue des Alpes. There is a brief conflict, mainly because the headstrong German member, Karl Freytag (Reiner Schöne), insists upon being the teams leader. Jemima Brown turns up to clear the air with Hemlock for betraying him, as does Pope, whose continued interference earns him a beating from Hemlock.

The men begin their ascent of the Eigers north face in good weather, but the conditions abruptly turn icy and treacherous. The French climber Jean-Paul Montaigne is struck by falling rocks and dies, and the Germans arrogant planning has left the survivors with no route of retreat. Hemlock assumes leadership of the team.

The climbers make their way toward a tunnel window that connects to a railroad station inside the mountain, carrying the dead climber between them. At the last moment, Freytag and the Austrian Anderl Meyer (Michael Grimm) fall to their deaths when their anchors come loose; the Frenchmans corpse plummets as well. Hemlocks life is left almost literally hanging by a thread; he dangles alone a few meters from the tunnel window.

Bowman and a rescue crew make their way through the Eiger to the tunnel window, where they attempt to throw Hemlock a rope. Hemlock notices, "Youre limping, Ben," an indication that Bowman is the traitor hes after. Bowman throws him the rope, and Hemlock attaches it and then, having no other choice, cuts his own rope. Bowman pulls him into the tunnel to safety.

On the train, Bowman admits he first became involved with "the other side" due to his daughter Georges addiction to drugs but had no idea there would be any killing. Back at Kleine Scheidegg, Bowman wants to know if he needs to spend the rest of his life concerned about Hemlocks coming after him. Hemlock tells him, "Forget it".

Rejoined by Jemima Brown, Hemlock takes a phone call from Dragon, who assumes Hemlock killed all three of the other climbers intentionally, to ensure he killed the target. Jemima is curious about whether Dragon could be correct.

==Cast==
 
* Clint Eastwood as Dr. Jonathan Hemlock
* George Kennedy as Ben Bowman
* Vonetta McGee as Jemima Brown
* Jack Cassidy as Miles Mellough
* Thayer David as Y.A. Dragon
* Gregory Walcott as Clemnet Pope
* Reiner Schöne as Karl Freytag
* Michael Grimm as Anderl Meyer
* Heidi Brühl as Anna Montaigne
* Jean-Pierre Bernard as Jean-Paul Montaigne
* Brenda Venus as George
* Candice Rialson as Art Student
* Elaine Shore as Miss Cerberus
* Dan Howard as Dewayne
* Jack Kosslyn as Reporter
* Walter Kraus as Garcia Kruger
* Frank Redmond as Henri Baq (Wormwood)
* Siegfried Wallach as Hotel Manager
* Susan Morgan as Buns
 

==Production==

===Casting=== David Brown production. Paul Newman was intended for Jonathan Hemlock. After reading the script, Newman declined, believing the film was too violent. McGilligan (1999), p.241 
 The Destroyer assassin series) in Connecticut in February 1974 for assistance, despite Murphys having never read the book nor written for a film before. McGilligan (1999), p.242  Murphy read the novel and agreed to write the script, but he was unhappy with the novels tone, which he believed patronized readers.  Murphy completed a draft in late March and a revised script a month later. McGilligan (1999), p.243 

George Kennedy, who had recently finished filming Thunderbolt and Lightfoot with Eastwood, was cast as Big Ben Bowman, Hemlocks friend and secret adversary. Jack Cassidy was cast as the colorful (and flamboyantly gay) assassin Miles Mellough. Thayer David, a year before his role as a boxing promoter in Rocky,  was cast as the albino "Dragon" (named "Yurasis Dragon" in the book). Vonetta McGee of Thomasine and Bushrod was cast as the African-American female C2 operative, Jemima Brown, whose name results in one of the films characters making a crude pancake joke. McGilligan (1999), p.244 

===Filming===
 ]]
In the summer of 1974, Eastwood travelled to Yosemite National Park where he trained in mountain climbing. Filming in Grindelwald, Switzerland began on August 12, 1974 with a team of climbing experts and advisers from America, Canada, Germany, Switzerland, and the United Kingdom.  The climbers were based at the Hotel Bellevue des Alpes at Kleine Scheidegg. McGilligan (1999), p.245 
 Frank Stanley, who thought climbing a perilous mountain to shoot a film was unnecessary.  According to cameraman Rexford Metz, it was a boyhood fantasy of Eastwoods to climb such a mountain, and he enjoyed displaying heroic machismo. McGilligan (1999), p.248 

A number of accidents occurred during the filming of The Eiger Sanction. A 27-year-old Scottish climber, David Knowles, who was a body double and photographer, was killed during a rock fall, and Hoover narrowly escaped with his life. Eliot (2009), p.161  Eastwood almost abandoned the project but proceeded because he did not want Knowles to have died in vain. McGilligan (1999), p.247  Eastwood insisted on doing all his own climbing and stunts. Stanley also fell but survived and used a wheelchair for some time. McGilligan (1999), p.249  Stanley, who completed filming under pressure from Eastwood, blamed Eastwood for the accident because of lack of preparation, describing him as a director and actor as a "very impatient man who doesnt really plan his pictures or do any homework. He figures he can go right in and sail through these things." McGilligan (1999), p.250  Stanley was never hired by Eastwood or Malpaso Productions again. Several other accidents and events apparently took place during the filming, but the producers hid them from the public. 

Speaking with Roger Ebert of the Chicago Sun-Times, Eastwood discussed the stunt in which he dangled from a mountain on the end of a cable:
  }}

===Filming locations===
* Carmel-by-the-Sea, California|Carmel-by-the-Sea, California, USA 
* Eiger, Bernese Alps, Switzerland
* Kleine Scheidegg, Bernese Alps, Switzerland 
* Los Angeles, California, USA 
* Monterey, California, USA 
* Monument Valley, Arizona, USA
* Stage 28, Universal Studios, Universal City, California, USA
* Zion National Park, Springdale, Utah, USA 
* Zürich, Kanton Zürich, Switzerland   

==Soundtrack==
{{Infobox album 
 | Name         = The Eiger Sanction
 | Type         = Film score John Williams
 | Cover        = The Eiger Sanction Soundtrack.jpg
 | Released     = 1975
 | Recorded     = 1975
 | Genre        = Orchestral
 | Length       = 34:38 MCA Varèse Sarabande
 | Producer     = John Williams The Towering Inferno
 | This album   = The Eiger Sanction Jaws
}}
Eastwood chose John Williams to compose the original music soundtrack for The Eiger Sanction—his only score in the spy genre. A main theme is presented initially on piano evoking a sense of sophistication and mystery, then given a much jazzier or pop rendition reminiscent of Lalo Schifrins material.   

The most impressive sections of the score are the grand orchestral cues composed for the mountain scenes. Pieces such as "The Icy Ascent" and "The Top of the World" capture both the beauty of the alpine surroundings and the inherent dangers. The latter title presents the kind of rapturous orchestral celebration now commonly associated with Williams music.  The pseudo-baroque piece "Training with George" presents a lovely string arrangement "demonstrating Williamss remarkable versatility while retaining that musical signature that makes all of his scores so recognisably his." The main theme is reprised in "George Sets the Pace" as a guitar solo with flute harmony. "Microfilm" is a low key action piece, and "Up the Drainpipe" is pure suspense music, different in tone from the rest of the album. The album concludes with "The Eiger", a triumphant and beautiful finale.
 MCA label, and in 1991 it was issued as a CD on the Varèse Sarabande label.   

# "Main Title" – 2:24
# "Theme from The Eiger Sanction" – 2:53
# "Fifty Miles of Desert" – 2:50
# "The Icy Ascent" – 3:41
# "Friends And Enemies" – 3:01
# "The Top of the World" – 3:05
# "Theme from The Eiger Sanction" – 2:09
# "Training With George" – 2:13
# "Theme from The Eiger Sanction" – 2:07
# "George Sets The Pace" – 2:39
# "The Microfilm Killing" – 2:04
# "Up The Drainpipe" – 3:18
# "The Eiger" – 2:14

==Reception==
The Eiger Sanction was panned by many critics on its release in May 1975. A number  criticized Eastwoods performance as lacking the sophistication of the books character. Playboy described the film as "a James Bond reject." McGilligan (1999), p.253  Joy Gould Boyum of the Wall Street Journal remarked, "The film situates villainy in homosexuals and physically disabled men."  Several critics did not understand the plot, and Pauline Kael of New York Magazine described the film as "a total travesty".  It was a commercial failure, taking $23.8 million at the box office.  Eastwood blamed the production company for poor earnings and publicly left Universal Studios. McGilligan (1999), p.256 

==See also== Eiger North Face North Face

==References==
;Notes
 
;Citations
 
;Bibliography
 
*  
* 
*  
*  
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 