Undertow (1949 film)
{{Infobox film
| name           = Undertow
| image          = Undertow (1949 film).jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = William Castle
| producer       = Ralph Dietrich
| screenplay     = Arthur T. Horman Lee Loeb
| story          = Arthur T. Horman
| starring       = Scott Brady  Peggy Dow Bruce Bennett
| music          = Milton Schwarzwald
| cinematography = Irving Glassberg
| editing        = Ralph Dawson
| distributor    = Universal Studios
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English}}
 thriller film John Russell. It is the story of an ex-con, a former Chicago mobster, who is accused of the murder of a high ranking Chicago boss.  The movie marks the second film to feature a young actor named Rock Hudson and the first in which he received a film credit for his work. 

==Plot== John Russell). It turns out they are both engaged to be married.

On his way home to Chicago, Reagan shares the flight with a schoolteacher, Ann McKnight (Peggy Dow), someone he met at a Reno casino and helped win at the gambling table. Reagan arrives home and is met by the police. It seems that Reagan is a suspect as a potential trouble maker due to his past dispute with a Chicago mob kingpin, Big Jim, his fiancees uncle. The police put a tail on him, which he shakes on a Chicago elevated train. Reagan meets up with his bride-to-be, Sally Lee (Dorothy Hart). He tells her he will go to Big Jim to make peace. But when the uncle is murdered, Reagan is framed for the murder.

On the run from both the police and from the unknown murderers, Reagan enlists the help of McKnight and an old buddy, Charles Recklilng (Bruce Bennett), a detective. They discover the truth: Morgan is also engaged to Sally Lee, and together they are responsible for murdering her uncle and framing Reagan.

Reagan manages to clear himself, however, after which he and McKnight end up in each others arms, bound to that lodge in Reno.

==Cast==
* Scott Brady as Tony Reagan
* Peggy Dow as Ann McKnight
* Bruce Bennett as Detective Charles Reckling
* Dorothy Hart as Sally Lee John Russell as Danny Morgan

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 