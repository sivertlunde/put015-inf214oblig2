Opfergang
Opfergang ( ) is a 1944 German film directed by Veit Harlan.   It is based on Rudolf G. Bindings work of the same title, with alterations for propaganda purposes. 

==Synopsis==
Albrecht Froben, though married to Octavia, falls in love with his neighbor, Äls Flodéen.  She, however, is slowly dying from a debilitating disease.  During an epidemic, Albrecht goes to bring her daughter to safety but he catches typhoid and is quarantined in hospital. Octavia, realising the love match, and hearing that Als is now bedridden and dying, dresses up as him and rides by her gates every day to keep her spirits up - her bed is next to the window.  Albrecht returns.  Äls has a dream in which she talks to her projection of Albrecht and concludes that she does not wish to take part in this union and accepts death.  Albrecht is reconciled with his wife.

==Cast==
*Irene von Meyendorff - Octavia
*Kristina Söderbaum - Aels
*Franz Schafheitlin - Mathias
*Ernst Stahl-Nachbaur - Sanitätsrat Terboven 
*Otto Treßler - Senator Froben 
*Ludwig Schmitz - Büttenredner 
*Paul Bildt - Notar

==Motifs==
Nazism makes no overt appearance in the film, which appears a work of entertainment, but it includes themes frequently in Nazi propaganda. 

Sacrifice and death are constant motifs in the movie—Äls even recounts how she had to put down her ill dog—and Albrechts return to his wife is a reflection of a realization of the tragic side of life. 

Although Äls is a danger to the marriage, she is not presented as wholly negative, owing to her love of nature.   She dies in a reversal of the source material, where the husband dies. Cinzia Romani, Tainted Goddesses: Female Film Stars of the Third Reich p20 ISBN 0-9627613-1-1   This reflected a need to avoid temptation to adultery, when many families were separated, and Joseph Goebbels himself insisted that it must be the woman rather than the man who paid.   Nevertheless, her death is surrounded by a heavenly chorus and transcendence. 

==Distribution==
Owing to the shortage of raw film, and its full color spectacular nature, it received only very limited release. 

==References==
 

==External links==
* 
*  at Virtual History

 
 
 
 
 


 