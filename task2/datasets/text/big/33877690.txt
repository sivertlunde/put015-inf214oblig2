Buddy's Bearcats
{{Infobox Hollywood cartoon
| series = Looney Tunes (Buddy (Looney Tunes)|Buddy)
| image =   
| caption =   Jack King
| story_artist = 
| animator = Ben Clopton Jack Carr Bernice Hansen Billy Bletcher (all uncredited) Norman Spencer
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = June 23, 1934 (USA)
| color_process = Black-and-white
| runtime = 7 minutes
| movie_language = English
| preceded_by = Buddy of the Apes (1934)
| followed_by = Buddys Circus (1934)
}} animated short Jack King; musical direction was by Norman Spencer.

==Summary==
We come to a sign that announces "  sits beneath the same curly-haired man from before, and another fellow uses the canines tail as a crank that curves the dogs midsection upwards, allowing the young man a far better view of the field (or simply a chance to leap over the fence.) An apparently  , who swallows a ball thrown, in his direction, by Buddy. Buddy rubs his hands with dirt; a Bruiser squirts oil under his arms, and throws a pitch to Buddy, who hits the ball and runs (and skates) to base. The fans are very pleased. In the next scene, Buddy throws a tricky ball to a Bruiser, who can not seem to hit it; he throws down his bat, blows air (through a bug spray apparatus) at the ball that it falls (as would a dying fly), and simply picks it up, tosses it into the air, & hits it. An outfielder catches the ball. The score, we see in the next scene, stands at forty-nine to forty-seven. The people want Buddy! But Our Hero, behind the scenes, is all too nervous to emerge & play; alone, he genuflects, and appears, for a moment, to pray. Cookie approaches him and tells him of the great clamor for him from the spectators: Buddy is bashfully convinced. Buddy gladly takes the bat from another player (who looks like a taller, balder version of Buddy), hits a ball thrown by a maniacally laughing, mustachioed Bruiser, & runs about the diamond, cheered on by Cookie, who stands at base. The game is won, & the two sweethearts, embracing, are buried in a deluge of the hats of happy fans.

==Hot dog vendor==
Willie King, the concession stand owner, played by Billy Bletcher, sings an original song by Norman Spencer, the musical director of the short. In the history of Warner Bros. cartoons, Willie King was, in fact, a concession stand owner who operated his business outside Leon Schlesingers studio.

==Cookie in this short==
This is the first of but a few Buddy shorts in which Buddys sweetheart Cookie has blond, braided hair (as contrasted with her usual black.) This would seem to be characteristic only of those Buddy cartoons supervised by Jack King, though not all of them.

==References==
 

==External links==
*  

 

 
 
 
 
 