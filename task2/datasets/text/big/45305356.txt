Twilight of Love
{{Infobox film
| name =Twilight of Love
| image =Twilight of Love.jpg
| caption =
| director = Luigi Scattini Claude Fournier  Giacomo Rossi-Stuart Luigi Scattini  Vittorio Schiraldi 	
| starring =  
| music =  Piero Umiliani 
| cinematography =Benito Frattari 	 producer = Carlo Ponti    Fulvio Lucisano 
| released =  
| language = Italian
| budget =
}}  1977 Italian romance film written and directed by Luigi Scattini. It is based on the novel Il corpo by Alfredo Todisco.       

== Plot ==
Richard, an elderly director of a  Montreal advertising agency, met Dyanne, a young girl who appears to be a perfect model for the launch of a new perfume.
During the stay on a beautiful deserted island, where they had gone to make the photo shoot, Richard falls in love with the girl.

== Cast ==
 Anthony Steel as Richard Butler 
*Annie Belle as Dyanne 
*Hugo Pratt as  Pierre
*Pam Grier as  Sandra
*Giacomo Rossi-Stuart as  Guide 
*Alain Montpetit as  Photographer 
*Gerardo Amato as  Philip 

==References==
 

==External links==
* 

 
  
 
 
 
 
 
 

 
 