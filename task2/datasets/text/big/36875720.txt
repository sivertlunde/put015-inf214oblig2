List of lesbian, gay, bisexual or transgender-related films of 1971
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1971. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1971==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|- Les Amis|| 1971 ||   ||   || Drama||
|-
|A Casa Assassina|| 1971 ||   ||   || Drama||
|-
|Daughters of Darkness|| 1971 ||   ||       || Horror ||
|- Death in Venice|| 1971 ||   ||     || Drama||
|-
|Fortune and Mens Eyes|| 1971 ||   ||    || Drama||
|-
|La noche de Walpurgis|| 1971 ||   ||    || Horror||
|-
|Pink Narcissus|| 1971 ||   ||  || Drama||
|-
|Some of My Best Friends Are...|| 1971 ||   ||  || Drama||
|- Sunday Bloody Sunday|| 1971 ||   ||   || Drama||
|- Vanishing Point|| 1971 ||   ||     || Action drama||
|-
|Villain (1971 film)|Villain|| 1971 ||   ||   || Crime, thriller ||
|-
|Women in Revolt|| 1971 ||   ||  || Comedy, Drama||
|}

 

 
 
 