Showdown (1993 film)
{{Infobox film name           = Showdown image          =  caption        = director       = Robert Radler producer  Alan Schechter Ash R. Shah screenplay     = Stuart Gibbs starring       = Billy Blanks Kenn Scott Christine Taylor John Mallory Asher Patrick Kilpatrick Brion James music  Stephen Edwards cinematography = Bryan Duggan editing        = Florent Retz distributor    = Imperial Entertainment Tilford Productions released       =   runtime        = 100 minutes country         = United States language       = English
}}
 The Karate Kid.

== Plot ==

Ken Marks is the new student at his new school in Phoenix, AZ. There he meets the girl of his dreams named Julie. Unfortunately, she has a boyfriend, Tom, who is not only possessive of her but is also a brutal karate expert and considers himself "King of the Campus". Ken also meets Billy Grant, the school janitor. When Toms bulling becomes too much for Ken to handle, Billy, who is not only a martial arts expert himself but also an ex-cop, decides to train Ken to defend himself. But when Billy learns that Toms sensei is none other than Lee, an old enemy who wants nothing more than to see Billy dead, both Ken and Billy are pitted against their adversaries in two separate fighting matches at Lees dojo where only one side can win, student vs. student, sensei vs. sensei.

== Cast ==

* Billy Blanks as Billy Grant Kenn Scott as Ken Marks
* Christine Taylor as Julie
* John Mallory Asher as Mike
* Ken McLeod as Tom
* Patrick Kilpatrick as Lee
* Linda Dona as Kate
* Michael Cavalieri as Rob
* Seidy López as Gina
* Brion James as Vice Principal Kowalski
* Michael James Genovese as Officer Spinelli (as Michael Genovese) Nicholas Hill as James

== External links ==
*  

 
 
 
 

 