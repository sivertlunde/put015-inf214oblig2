One Crazy Summer
 
{{Infobox Film
| name           = One Crazy Summer
| image          = One crazy summer.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Savage Steve Holland
| producer       = Gil Friesen (executive producer) Michael Jaffe (producer) Andrew Meyer (executive producer) Claudia Sloan (animation producer) William Strom (associate producer)
 | writer         = Savage Steve Holland
| starring       = John Cusack Demi Moore Bobcat Goldthwait Curtis Armstrong Joel Murray
| music          = Cory Lerios
| cinematography = Isidore Mankofsky
| editing        = Alan Balsam
| distributor    = Warner Bros.
| studio         = A&M Films
| released       = August 8, 1986
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $13,432,000
}}
One Crazy Summer is a 1986 romantic comedy film written and directed by Savage Steve Holland, and starring John Cusack, Demi Moore, Bobcat Goldthwait, Curtis Armstrong, and Joel Murray. The original film score was composed by Cory Lerios.

==Plot summary==
Hoops McCann (Cusack) is a recent high school graduate who failed to get a basketball scholarship, disappointing his parents. Hoops hopes to be admitted to the Rhode Island School of Design, and must write and illustrate a love story for his application. He joins his friend George Calamari (Murray), and Georges sister Squid, to spend the summer on the island of Nantucket, Massachusetts. En route, they pick up a young rock singer named Cassandra Eldridge (Moore); who is being chased by a motorcycle gang at the time. Once on the island, Hoops and George, along with Georges island friends the Stork twins and Ack-Ack Raymond, must help Cassandra save her grandfathers house from the greedy Beckersted Family. Along the way, Hoops must find a way to write his cartoon love story.

==Cast==
* John Cusack as Hoops McCann
* Demi Moore as Cassandra Eldridge
* Curtis Armstrong as Ack Ack Raymond
* Bobcat Goldthwait as Egg Stork
* Joel Murray as George Calamari William Hickey as Old Man Beckerstead
* Joe Flaherty as General Raymond
* Mark Metcalf as Aquilla Beckerstead
* John Matuszak as Stan
* Kimberly Foster as Cookie Campbell
* Matt Mulhern as Teddy Beckerstead
* Rich Little as Radio contest DJ
* Tom Villard as Clay Stork
* Jeremy Piven as Ty
* Rich Hall as Wilbur, Gas station attendant
* Taylor Negron as Taylor, Gas station attendant
* Billie Bird as Grandma Calamari
* Bruce Wagner as Uncle Frank
* Kristen Goelz as Squid Calamari
* Laura Waterbury as Crossing guard
* Donald Li as Chong Freen
* Jennifer Yahoodik as Andrea

==Soundtrack==

{| class="wikitable"
|- 
! Song Title
! Artist
|-
| Dont Look Back
| Demi Moore
|-
| Take A Bow
| Jaime Segel
|-
| Easy Street 
| David Lee Roth
|-
| Be Chrool To Your Scuel
| Twisted Sister
|-
| What Does It Take
| Honeymoon Suite
|-
| Dirty Dog
| ZZ Top
|-
| Do It Again
| The Beach Boys
|-
| Wouldnt It Be Nice
| The Beach Boys
|-
| Fun, Fun, Fun
| The Beach Boys
|-
| Fandango
| Herb Alpert
|-
| I Go To Rio Peter Allen
|-
| Outa-Space
| Billy Preston
|-
| Dancing In The Street
| Martha and The Vandellas
|-
| Would I Lie To You
| Eurythmics
|-
| Born To Be Wild Steppenwolf
|-
| Down On The Corner
| Creedence Clearwater Revival
|-
| Wipe Out
| The Surfaris
|-
| Theme from Jaws
| John Williams
|-
| In My Room
| The Beach Boys
|}

==Production==
Several locations on   (as Generic High School), Hyannis West Elementary School (as Generic Elementary), the Woods Hole, Marthas Vineyard And Nantucket Steamship Authority ferry dock in Woods Hole,  where the characters board the ferry, and the motorcycle gang leader jumps his motorcycle into the water. The internal scene of the gas station bathroom were shot on a stage built in the MBL Club in Woods Hole. The inside of General Raymonds Army-Navy store is Mass Bay Company, located at 595 Main Street, Hyannis, MA. The Stork brothers gas station is the (then-Amoco) located at 1098 Main Street in Dennis, MA.

Hoops McCann is named after the protagonist in Steely Dans song "Glamour Profession" from the Gaucho (album)|Gaucho album, who is introduced as a basketball aficionado.
 Better Off Dead, which led to the gag with the two bunnies that get blown up at the end of the movie who resemble the movie critics. 

==Reception==
Nina Darnton of the New York Times wrote, "In spite of the directors flair for zany humor, this film is just absurd."   Pat Graham of the Chicago Reader found it "Not a bad film, and certainly more polished than Hollands Better Off Dead debut, though its marred by unevenness and the directors ineradicable penchant for infantile clowning."   The film maintains a 60% score at Rotten Tomatoes.

==See also==
* List of American films of 1986

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 