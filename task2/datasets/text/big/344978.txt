Swingers (1996 film)
{{multiple issues|
 
 
 
}}

{{Infobox film
| name = Swingers
| image = Swingers ver2.jpg
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Doug Liman
| producer = Victor Simpkins Jon Favreau
| writer = Jon Favreau Alex Desert Heather Graham
| music = Don George Johnny Hodges Harry James
| cinematography = Doug Liman
| editing = Stephen Mirrione
| studio = Independent Pictures
| distributor = Miramax Films (USA) Pathé (UK)
| released = October 18, 1996
| runtime = 96 minutes
| country = United States
| language = English
| budget = $200,000
| gross = $4,555,020
}}
Swingers is a 1996 comedy-drama film about the lives of single, unemployed actors living on the eastside of Hollywood, Los Angeles, California|Hollywood, California during the 1990s swing revival. Written by Jon Favreau and directed by Doug Liman, the movie starred Favreau and Vince Vaughn, and also featured performances by Ron Livingston and Heather Graham.

A critical and commercial hit the film help propel Favreau, Livingston, and Vaughn to stardom while also launching Doug Limans directing career as he won the MTV award for Best New Filmmaker

This film was rated #57 on Bravo (US TV channel)|Bravos "100 Funniest Movies."  The film was honored on the 2007 Spike TV Guys Choice Awards.

==Plot==
Mike Peters (Jon Favreau) is a struggling actor who left New York, and a girlfriend behind, to find success in L.A. The move caused his girlfriend of six years to split up with him six months earlier and left him feeling alone and heartbroken.

In the opening scenes of the film, Mike talks about his situation with his friend Rob (Ron Livingston), another thespian from back east. Mike feels miserable about their conclusions. Afterwards, while on the phone, Mike is coaxed into an impromptu Las Vegas trip by his new best friend, and also actor, Trent (Vince Vaughn) to help Mike get over his ex. The trip starts out on a high note with excitement and anticipation but soon takes a turn for the worse when Mike crashes and burns at the casino. Soon after, the guys manage to meet some ladies and just when it seems as if Mike can make some progress and salvage the trip, it all falls apart again and this time Trent goes down with him.

On the ride home, Trent gets Mike to feel better about himself and to look at the positive side of things. Mike promises to try his best to move on.

Now back in L.A., Mike and Rob get together for some golf and to talk shop. Later that night, Mike and Trent are getting ready to hit the Hollywood hills, at their actor friend Sues (Patrick Van Horn) apartment. With the vibes feeling right and things going well, Mike and Rob meet up with Mikes pal Charles (Alex Désert), yet another starving actor, at a local bar where they admire the beautiful women. Soon afterward they rendezvous with the others and they all finally make it to a party where Mike makes an ill-fated attempt to get back into the game, while Trent gives the guys a lesson in how to handle the opposite sex.

The guys agree to head to their favorite after hours spot and, after watching Trent and Sue effortlessly meet some girls, Mike is clearly shown feeling lower than ever but not yet defeated. Trent and Sue convince Mike that he is in control so he finally makes his move like hes got nothing to lose and he actually meets Nikki (Brooke Langton) and gets her phone number. Success at last, or so he hopes.

The swingers leave the lounge and narrowly miss getting into a brawl in the parking lot, caused by Sues temper yet averted by Sue pulling a gun which no one else was aware was even a part of his attire. The group splits up but not before angry words are exchanged among friends. Mike is left feeling desolate once again.

To make himself feel better, Mike decides to call Nikki but he blows any chance he has with her when he leaves a series of increasingly awkward and desperate messages. Now he feels as if he has truly hit bottom and he sits alone in his apartment, missing his ex more than ever and contemplating a move back to New York. Rob comes over to console him and, after some serious talk, he feels like its time to get back in the saddle again.

Mike meets up with the guys at Sues, where he discovers that he has missed some changes in the group dynamic. That aside, apologies are exchanged and the nightlife once again awaits them. The next stop is a Hollywood night club for swing night. The guys enter through a makeshift VIP entrance in a style that pays homage to Director/Writer/Producer Martin Scorsese.

Once inside, the guys let the good times roll and Mike spots a beautiful woman whom he decides he wants to meet. He gathers all his courage and approaches her confidently. He finds himself in an actual interesting conversation with the young and single Lorraine (Heather Graham). They are soon swing dancing and the chemistry between them is incredible. The night ends well and Mike, Trent and Sue head to an afterhours meal to discuss the particulars. Mike finally appears to be on his way to moving on and feeling good about himself.

The following morning Mike receives a phone call from his ex. She wants to talk about their state of affairs, but when Mike decides to answer another incoming call, he is greeted by Lorraine. He has to make an immediate decision as to which path he wants to go down and in a moment that solidifies his regained self-esteem, he puts his past behind him and chooses to take a chance on someone new.

In the closing scene, Mike meets Trent for coffee and they enjoy one more good heart-to-heart while Trent is brought down to earth by a reminder that even when you have got it, you cannot win them all, in an ending that suits the swingers perfectly before we leave them on another sunny L.A. day.

==Cast== New York, Mike recently broke up with his longtime girlfriend. Still pining over his loss, and unfamiliar with the dating game, Mike has difficulty meeting new women.
* Vince Vaughn as Trent Walker, an aspiring actor, Trent is Mikes closest friend. Trent is also a loud, charismatic, and occasionally obnoxious swinger. He makes it his personal mission to teach Mike the swinging lifestyle.
* Ron Livingston as Rob, Mikes friend from New York, Rob is a recent arrival to Los Angeles and even less knowledgeable about the swinging scene, but he is not fresh out of a relationship and has a much more optimistic view of life. He provides a sympathetic ear to Mike and considers taking a job at Disneyland to pay the rent.
* Patrick Van Horn as Sue, a hot-headed swinger; Sue discusses the finer points of seduction with Trent, but is somewhat less sympathetic to Mikes problems. Mike later tells Rob that he is named after the Johnny Cash song "A Boy Named Sue."
* Alex Désert as Charles, a swinging acquaintance of the group and another struggling actor. Charles accompanies the group while bar-hopping around Hollywood, proclaiming each place "dead" as they leave.
* Heather Graham as Lorraine, a woman that Mike meets at a bar, Lorraine has a good sense of humor and is not overly concerned about the rules of dating. She is also new to the LA scene, having left a relationship behind in Wisconsin.
* Deena Martin as Christy
* Katherine Kendall as Lisa
* Brooke Langton as Nikki

==Production==
 
Favreau wrote the screenplay to Swingers in two weeks, with various friends in mind for key roles. One scene, in which Trent yells at Sue for insulting Mike, was written at Vaughns request to make it clear that beneath Trents swagger, he truly cared for Mike as a friend. Favreau and his friends gave Readers Theatre|readers theater performances of the script to drum up interest in and capital for the movie.

Swingers was filmed on location at several Los Angeles nightclubs, particularly in the hip Los Feliz neighborhood, including the Dresden Lounge and the Derby. The Las Vegas scenes were filmed primarily in two locations, with the exterior casino shots taking place at the Stardust Resort & Casino and all the subsequent interior shots being filmed at the Fremont Hotel and Casino, farther north in downtown Las Vegas.
 cameo roles to their family members. Vince Vaughns father, Vernon Vaughn, plays the lucky gambler at the $100-minimum blackjack table, while Favreaus grandmother, Joan Favreau, is the lucky gambler at the $5-minimum blackjack table.

;Filming locations
* Mikeys apartment is located in the Franklin Village area of Los Angeles, a few miles from the Dresden Room.
* The Dresden room is a popular (classic) bar and club in the Los Feliz neighborhood, located at 1760 N. Vermont Ave. Marty & Elayne, who are performing in the Dresden scene in the movie, perform at the Dresden in real life several nights a week.
* The cafe where various factions of the crew meet and eat was the Hollywood Hills Coffee Shop (now renamed under new ownership as the 101 Cafe) and is part of the Best Western Hotel on Franklin Avenue near Hollywood, and near the 101 freeway.
* The bar where the characters dance is The Derby in Los Feliz, on the corner of Hillhurst and Los Feliz Blvd. In January 2009, the nightclub closed its doors.

==Reception==
Swingers was a critical and financial success. It holds an 87% "Fresh" rating on review site Rotten Tomatoes, based on 52 reviews. The critical consensus states " Funny, heartfelt, and effortlessly cool, Swingers made stars out of Vince Vaughn and Jon Favreau, established Doug Liman as a director to watch."    The Bourne Identity), and it was the first major film for Ron Livingston.
 swing music. Some of the slang used in the movie became popular in the years following its release, especially the use of the word "money" as a catch-all term of approval or quality. The exclamation "Vegas, baby!" also became a common quote when referencing the city. The film also gave exposure to the term "wingman (social)|Wingman" in its social interaction context. 

The film was voted as the 14th best film set in Los Angeles in the last 25 years by a group of Los Angeles Times writers and editors with two criteria: "The movie had to communicate some inherent truth about the L.A. experience, and only one film per director was allowed on the list".    

American Film Institute recognition:
*AFIs 100 Years... 100 Laughs - Nominated 
*AFIs 100 Years...100 Movie Quotes:
**"Youre so money, and you dont even know it." - Nominated   
**"Vegas, baby." - Nominated 

==Soundtrack==
{{Infobox album|  
| Name        = Swingers Original Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = Swingers_OST.jpg
| Cover size  =
| Caption     = Swingers Original Soundtrack
| Background  =
| Released    = October 15, 1996
| Recorded    =
| Genre       = Soundtrack
| Length      = 41:21
| Label       = Hollywood Records
| Producer    =
| Reviews     =
}} Heart (which had most of an instrumental section edited out, and was cut down to 3 minutes and 29 seconds) and a studio version of "Staying Alive" by lounge act Marty and Elayne, with other tracks inspired by the film.

;Swingers - Music From The Miramax Motion Picture
# "Youre Nobody till Somebody Loves You" - Dean Martin   Love Jones  
# "With Plenty of Money and You" - Count Basie/Tony Bennett  
# "You & Me & The Bottle Makes 3 Tonight (Baby)" - Big Bad Voodoo Daddy  
# "Knock Me a Kiss" - Louis Jordan  
# "Wake Up" - The Jazz Jury  
# "Groove Me" - King Floyd  
# "I Wanna Be Like You" - Big Bad Voodoo Daddy  
# "Muccis Jag M.K. II" - Joey Altruda   King of the Road" - Roger Miller  
# "Pictures" - The Jazz Jury  
# "She Thinks I Still Care" - George Jones  
# "Car Train" - The Jazz Jury   Pick Up the Pieces" - Average White Band  
# "Go Daddy-O" - Big Bad Voodoo Daddy  
# "Im Beginning to See the Light" - Bobby Darin  

;Swingers Too! - More Music From... "Swingers"
# "Aint That a Kick in the Head?|Aint That a Kick in the Head" - (Dean Martin)  
# "Adam and Eve" - Paul Anka Heart  
# "Shes A Woman (W-O-M-A-N)" - (Sammy Davis, Jr.) with (Count Basie)
# "Baby (Youve Got What It Takes)" - (Dinah Washington)/(Brook Benton)  
# "Down For Double" - (Mel Tormé)
# "Staying Alive" - (Marty & Elayne)
# "Therell Be Some Changes Made" - (Ann-Margret)
# "One Mint Julep" - (Xavier Cugat)  
# "Gimme That Wine" - Lambert, Hendricks & Ross  
# "Datin With No Dough" - (Royal Crown Revue) Bring Me Sunshine" - (Willie Nelson)  

==See also==
* Made (2001 film)|Made (2001 film)

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 