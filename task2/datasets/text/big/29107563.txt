Who Is the Man?
{{Infobox film
  | name           = Who Is the Man?
  | image          = 
  | image_size     = 
  | caption        = 
  | director       = Walter Summers
  | producer       = G. B. Samuelson
  | writer         = Walter Summers
  | based on       =  
  | starring       = John Gielgud Isobel Elsom
  | music          =
  | cinematography = 
  | editing        = 
  | studio         = Napoleon Films
  | distributor    = Napoleon Films
  | released       =  
  | runtime        = 60 minutes
  | country        = United Kingdom
  | language       = Silent English intertitles
  }}
Who Is The Man? (1924 in film|1924) is a British silent film drama directed by Walter Summers.  The film was based on the successful French play Daniel by Louis Verneuil and is notable as the first screen appearance of John Gielgud.   

==Plot==
Daniel Arnault (Gielgud), an impecunious sculptor, is in love with the beautiful Genevieve (Isobel Elsom).  Spurred on by her mercenary and socially ambitious mother however, Genevieve consents to marry Daniels wealthy brother Albert (Langhorn Burton).  In despair, Daniel sinks into drug addiction.  

The marriage is not a success, and Genevieve feels ignored and neglected by Albert.  She begins a flirtation with family friend Maurice Granger (Lewis Drayton) and the pair gradually fall in love.  One day Genevieve decides to pay a call on Daniel, and by chance meets Maurice who is also visiting.  

Unknown to Genevieve, Albert has become suspicious of her and has followed her to Daniels studio.  He shows up in a fury, and Daniel manages to hide Genevieve and Maurice.  Knowing that Genevieve has been there, Albert accuses his brother of being her lover and attacks him brutally.  Daniel fails to recover from the assault, and as he is dying he begs his brother to give Genevieve her freedom and allow her to go off with Maurice

==Cast==
* John Gielgud as Daniel Arnault
* Isobel Elsom as Genevieve Arnault
* Langhorn Burton as Albert Arnault
* Lewis Dayton as Maurice Granger
* Henry Vibart as Doctor
* Hugh Dempster as Robert Borden
* Mary Rorke as Mrs. Gerard

==Reception==
Surviving reviews suggest that Who Is the Man? received a mixed reception from critics.  While the standard of acting and the films visuals were well-appreciated, it was generally felt that there were always going to be problems arising from trying to capture a wordy stage play in silent film form, with nuances of character and motivation inevitably being lost.  A particular criticism was that the films intertitles were not only excessive in number, but also written in a slipshod, clumsy and ungrammatical style.

==Status== National Archive, and has included it on the "BFI 75 Most Wanted" list of missing British feature films.   

==See also==
*List of lost films

==References==
 

== External links ==
*  , with extensive notes
*  
*  

 
 
 
 
 
 
 
 
 
 

 