Shirdi Ke Sai Baba
{{Infobox film
| name           = Shirdi Ke Sai Baba
| image          = 
| image_size     = 
| caption        = 
| director       = Ashok V. Bhushan
| screenplay     = Manoj Kumar
| producer       = Sarla Charities Trust
| Genre          = Drama
| writer         = 
| narrator       = 
| starring       = Sudhir Dalvi ... Sai Baba
Manoj Kumar ... Devotee & Scientist
Rajendra Kumar ... Doctor (Poojas husband)
Hema Malini ... Pooja
| facebook       = www.facebook.com/ShirdiKeSaiBaba.Official
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = https://www.facebook.com/ShirdiKeSaiBaba.Official

}}
 1977 Bollywood film directed by Ashok V. Bhushan.

==Story==
Pooja lives with her doctor husband and a young son. Her son becomes 
seriously ill and does not respond to any medication, her husband wants 
him to be moved to a hospital for further treatment. Pooja has heard of  Sai Baba and wants her son to be treated there. 
Her son regains consciousness for a short while and demands that he be 
taken to Shirdi, the town of Sai Baba.

They travel there and meet with an ardent devotee of Sai Baba who 
recounts the tale behind the Baba: A young boy who traveled with a 
marriage procession to Shirdi. He stayed there, grew up, and was looked 
after by the kind townspeople, who subsequently named him Sai Baba. He 
showed Ganpat Rao that he is one with Vithoba and Bhagwan Shri Kishan; 
he demonstrated to Som Dev that he is one with Bhagwan Shri Shiv; former
jailbird Heera regained his eyesight with Sais blessing. During all of
these miracles, Sai went door to door begging for alms and food. The 
legend grew, people flocked all over to witness this new Messiah, one 
whom the Muslims called "Allah Sai", Christians "Jesus Sai", Sikhs 
"Nanak Sai", and Hindus "Bhola Sai". Sai could recite scriptures from 
all the holy books, whether it was the Quran, the Bible, the 
Granth Sahib, or the Geeta, and always blessed Hindus with 
"Allah Malik" (Allah bless you) and Muslims with "Ram Ram" or "Hare 
Krishna" (Krishna bless you). After his death, a temple was built in his
memory. That temple is still standing, losing none of its majesty, in 
Shirdi. In that temple, the place where Babas idol is sitting, is the 
exact place where Baba once sat, to rest, while the temple was under 
construction.

It is here that a doubting doctor has to decide whether his ailing son 
can be treated by Sai Baba, and it is here where we will witness whether
faith or scientific reality and logic will prevail.

==Cast==
*Sudhir Dalvi ...  Sai Baba 
*Manoj Kumar ...  Devotee & Scientist
*Rajendra Kumar ...  Doctor (Poojas husband) 
*Hema Malini ...  Pooja 
*Shatrughan Sinha ...  Heera 
*Raj Mehra ...  Murthy 
*Birbal ...  Kulkarni 
*Usha Chavan ...  Laxmi 
*C.S. Dubey ...  Grooms dad 
*Manmohan Krishna ...  Ganpat Rao 
*Dheeraj Kumar ...  Tatya (Baijammas son) 
*Prem Nath ...  Som Dev 
*Kanwarjit Paintal ...  Karim 
*Madan Puri ...  Ranbir Singh 
*Ratnamala ...  Baijamma  Sachin ...  Sarju 
*Master Tito ...  Beta (ved khanna)
* Sudhir Moghe ... Gokhle
* Namrata Shirodkar .. girl with Shatrughna sinha

==External links==
By Antonio Rigopoulos (1993) The Life and Teachings of Sai Baba of Shirdi is published in India by Sri Satguru Publications, Indian Books Centre, Delhi. ISBN 81-7030-378-9
*  

 
 
 


 