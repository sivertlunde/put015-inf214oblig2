Cattive ragazze
{{Infobox film
| name           = Cattive ragazze
| image          = Cattive ragazze.JPG
| image size     =
| caption        = 
| director       = Marina Ripa Di Meana
| producer       = Alberto Tarallo   Achille Manzotti
| writer         = Marina Ripa di Meana
| narrator       =
| starring       = Eva Grimaldi  Brando Giorgi   Anita Ekberg  Burt Young
| music          = 
| cinematography = 
| editing        = 
| distributor    = Artisti Associati
| released       = 15 August 1992
| runtime        = 99 minutes 
| country        = Italy
| language       = Italian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Ministry of Cultural Heritage and Activities.
 one of the worst films ever made.  Paolo Mereghetti in his film encyclopaedia Dizionario dei Film described the film as a "vapid mess that can only serve those incapable of understanding what cinema is", and considered it able to "compete for the title of worst film in cinema history and win!"  G. Giraud wrote in Il Lavoro, that Cattive ragazze "does not resemble anything in in a real movie, or even recall anything previously seen at the cinema, even in its worst". Film critic Marco Giusti refers to the film as "one of the pillars of Italian trash cinema".    Meana defended herself from critics, claiming that it was never her intention to create a second Battleship Potemkin.    Cattive ragazze was Di Meanas directorial debut, she never directed another film.

==See also==
* List of films considered the worst

==References==
 

==External links==
*  

 
 
 
 
 