List of horror films of 1963
 
 
A list of horror films released in 1963 in film|1963.

{| class="wikitable sortable"
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" | Notes
|-
!   | At Midnight Ill Take Your Soul
|   || José Mojica Marins ||   ||  
|-
!   |  
|   || Rod Taylor, Tippi Hedren, Jessica Tandy ||   ||  
|- Black Sabbath
|   || Boris Karloff, Michele Mercier, Mark Damon ||    ||  
|-
!   | Black Zoo
|   || Michael Gough, Jeanne Cooper, Rod Lauren ||   ||  
|-
!   |  
|   || Gérard Tichy, Leo Anchóriz, Ombretta Colli ||    ||  
|-
!   | Blood Feast
|   || William Kerwin, Mal Arnold, Connie Mason ||   ||  
|-
!   |  
|   || Vincent Price, Peter Lorre, Boris Karloff ||   ||  
|-
!   | Dementia 13 William Campbell, Luana Anders, Bart Patton ||   ||  
|- Diary of a Madman
|   || Vincent Price, Nancy Kovack, Chris Warfield ||   ||  
|-
!   |   Peter Baldwin, Leonard Elliott ||   ||  
|-
!   |  
|   || Vincent Price, Debra Paget, Lon Chaney, Jr. ||   ||  
|-
!   |  
|   || Julie Harris, Claire Bloom, Russ Tamblyn ||    ||  
|-
!   | House of the Damned Richard Crane, Ron Foster ||   ||  
|-
!   |  
|   || Clifford Evans, Noel Willman, Edward de Souza ||    ||  
|-
!   |  
|   || Barbara Steele, George Ardisson, Umberto Raho ||   ||  
|-
!   | Matango
|   || Yoshio Tsuchiya, Akira Kubo, Hiroshi Koizumi ||   ||  
|-
!   |  
|   || Tom Poston, Robert Morley, Janette Scott ||    ||  
|-
!   | Paranoiac (1963 film)|Paranoiac
|   || Janette Scott, Oliver Reed, Sheila Burrell ||    ||  
|-
!   |  
|   || Vincent Price, Peter Lorre, Boris Karloff ||   ||  
|-
!   | Slime People Robert Burton ||   || Science fiction horror 
|-
!   |  
|   || Jack Nicholson, Boris Karloff, Sandra Knight ||    ||  
|-
!   | Twice-Told Tales (film)|Twice-Told Tales Sebastian Cabot, Mari Blanchard ||   ||  
|-
!   |  
|   || Christopher Lee, Georges Riviere, Rossana Podestà ||   ||  
|-
!   |  
|   || Christopher Lee, Daliah Lavi, Luciano Pigozzi ||     ||  
|}

==References==
 


 
 
 

 

==External links==
*   at the Internet Movie Database


 
 
 