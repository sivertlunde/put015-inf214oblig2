Zara (2014 film)
 
{{Infobox film
| name           = Zara
| image          = 
| border         = yes
| caption        = DVD release poster
| director       = Preshanthan Moodley
| producer       = Shahrina Ramphaul
| writer         = Shahrina Ramphaul
| starring       = Shahrina Ramphaul Rahul Brijnath Ronica Bagwandin Umi June
| music          = Jae
| editing        = Preshanthan Moodley
| distributor    = AA Video
| released       = 25 August 2014
| genre         = Canon 5D Mark III
| format         = 1080p (Full HD)
| aspect ratio   = 16 x 9
| runtime        = 40 minutes
| country        = South Africa
| language       = English ZAR 75,000
}} ZAR 75,000. It was released on DVD by the Durban based distributor AA Video.

==Plot==
Zara, was born into a Muslim family and fell in love with Ajay, a Hindu. Religion however, has no influence on this couple’s relationship. Unfortunately the story revolves around Zara who was exposed to an unfaithful, physically, emotionally and mentally abusive husband for many years.
Zara did not immediately abandon Islam but eventually embraced Hinduism for the sake of her husband and daughter. Ajay comes from a wealthy family and although he showers his wife and daughter with materialistic possessions, he feels this licenses him to illicit affairs – from having an affair with Zara’s sister to having an affair with a young black 18 year old. Ajay sees no wrong in his behavior as he is the sole provider in his house-hold. Zara is hugely dependent on Ajay, her parents died when she was young; her brothers are married and have their own lives to deal with, and her “best friend” - her sister turned out to be her worst enemy. Zara, like many other women is afraid of society and how they will stigmatize her if she had to leave her husband or seek help….she eventually finds a way out of her misery, but…is it a “way out”?

==Cast==
* Shahrina Ramphaul as Zara
* Rahul Brijnath as Ajay
* Ronica Bagwandin as Rashmika
* Neha Marielall
* Youshika Sudhama
* Youtanya Marielall
* Nokuvela Lubanyana
* Nkosinathi Lubanyana
* Umi June as Michelle

==Production==
The couple this movie is based upon, unfortunately still experience violence and abuse. The wife has asked that their names be kept confidential, due to any unforeseen embarrassment from society and/or the community. We respect their privacy but felt that this was a story to be told. As the shooting went on this production and word got out, many people could relate to this story. Shahrina Ramphaul says "We realized that Zara isn’t the story of ONE woman or family but a story of many families and many women.

==Objective==
The objective was to launch Zara during the “16 Days of Activism for NO Violence against Women and Children” campaign, in the hope that the message creates awareness to men and women to combat violence and abuse. Although this story is based on an Indian family, the hope is that one woman’s story helps other women to speak out, stand up and defend their honor. It is interesting to note, the Department of Justice estimates that 1 out of every four South African women are survivors of domestic violence. According to People Opposing Women Abuse (POWA) 1 in every 6 women who die in Gauteng are killed by an intimate partner. Abuse is NO excuse.

==External links==
* http://www.youtube.com/watch?v=FuDhrdufKiw
* http://www.highbeam.com/doc/1G1-351119573.html
* http://indiego.co.za/index.php/92-sa-movies/131-sa-movies-scene?ygstart=0

 