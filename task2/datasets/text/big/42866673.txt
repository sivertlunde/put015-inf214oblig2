Hendtheer Darbar
{{Infobox film
| name = Hendtheer Darbar
| image =
| caption = 
| director = V. Sekhar
| writer = V. Sekhar
| based on = Varavu Ettana Selavu Pathana
| producer = G. Ramachandran Meena
| music = Sadhu Kokila
| cinematography = Raju Mahendran
| editing = Jo Ni Harsha
| studio  = G R Gold Films
| released =  
| runtime = 159&nbsp;minutes
| language = Kannada
| country = India
}}
 2010 Indian Meena along with Rangayana Raghu and Sadhu Kokila in pivotal roles. The film is a remake of Sekhars own Tamil film Varavu Ettana Selavu Pathana released 14 years ago in 1994.

The film released on 29 June 2010 across Karnataka. Upon release, the film generally met with negative reviews from the critics and audience citing that the film was an age old concept with nothing fresh to offer.  The film was a sleeper hit at the box-office with the collections picking up slowly after the release. 

== Cast ==
* Ramesh Aravind as Shivaramu Meena as Radha
* Rangayana Raghu 
* Sadhu Kokila
* Ambika Soni
* G. Ramachandran
* Anantha Velu
* Layendra
* Preethi

== Soundtrack ==
{{Infobox album  
| Name        = Hendtheer Darbar
| Type        = Soundtrack
| Artist      = Sadhu Kokila
| Cover       = 
| Released    = 
| Recorded    = 
| Genre       = Film soundtrack
| Length      =
| Label       = 
| Producer    = 
| Reviews     =
| Last album  = 
| This album  = 
| Next album  = 
}}

{{tracklist
| headline = Track listing
| all_music = Sadhu Kokila
| all_lyrics = V. Nagendra Prasad, Tushar Ranganath and Ram Narayan
| extra_column = Singer(s)
| title1 = Bele Bele
| extra1 = Anoop Seelin
| length1 = 
| title2 = Muddata Oddata
| extra2 = Shamitha Malnad
| length2 = 
| title3 = Devathe Devathe
| extra3 = Achal Khan, Nanditha
| length3 = 
| title4 = Barlaa Maga
| extra4 = Hemanth Kumar
| length4 = 
| title5 = Are Nodu Hendtheera Darbar
| extra5 = Hemanth Kumar, Sadhu Kokila, Shamitha Malnad, Usha
| length5 =
}}

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 

 