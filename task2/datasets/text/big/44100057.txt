Hi! Dharma 2: Showdown in Seoul
{{Infobox film name           = Hi! Dharma 2: Showdown in Seoul image          =  director       = Yook Sang-hyo producer       = Cho Chul-hyun   Jung Seung-hye writer         = Choi Seok-hwan    Yook Sang-hyo starring  Shin Hyun-joon   Jung Jin-young  music          = Bang Jun-seok   Jang Min-seung cinematography = Park Hee-ju   editing        = Kim Sang-beom   Kim Jae-beom distributor    = Cinema Service studio         = Cineworld   Tiger Pictures   KM Culture released       =   runtime        = 101 minutes country        = South Korea language       = Korean
}}
Hi! Dharma 2: Showdown in Seoul ( ; lit. "Hey Dharma, Lets Go to Seoul") is a 2004 South Korean gangster comedy film.  It is the sequel to Hi! Dharma! (2001).  

==Plot==
In order to deliver a package for their recently departed head monk, Jeong-myeong, Hyeon-gak and Dae-bong travel to Musim-sa Temple in Seoul, in their first contact with civilization in years. But they find Musim-sa in financial trouble and in danger of being taken over by Beom-shik and his gangsters, who plan to build an apartment complex on the land. The monk trio have no choice but to stay in the city to protect the temple, and ready themselves for another showdown.

==Cast== Shin Hyun-joon as Beom-shik
*Jung Jin-young as Monk Jeong-myeong
*Lee Won-jong as Monk Hyeon-gak
*Lee Moon-sik as Monk Dae-bong
*Yang Jin-woo as Monk Mu-jin
*Yoo Hae-jin as Yong-dae
*Kim Seok-hwan as Sang-geun
*Lee Hyung-chul as Gu-man Kim Ji-young bosal
*Jung Han-yong as Company president Park
*Han Hye-jin as Mi-seon
*Park Gun-tae as Boy monk
*Choi Min-kum as Kimbap ajumma
*Park Gyeong-ok as Boutique designer Jung
*Lee Ye-won as Jae-gyus wife
*Park Shin-yang as Jae-gyu (cameo appearance|cameo)

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 