Three Times
{{Infobox film 
 | name = Three Times
 | image = three times.jpg
 | caption =
 | director = Hou Hsiao-Hsien 
 | writer = Hou Hsiao-Hsien, Chu Tien-wen
 | starring = Shu Qi, Chang Chen 
 | producer = Chang Hua-fu 
 | cinematography = Mark Lee Ping Bin
 | distributor = First Distributors 
 | budget = 
 | released = 20 May 2005 ( Cannes Film Festival ) 
 | runtime = 120 minutes 
 | country  = Taiwan Mandarin 
 | }} Taiwanese film directed by Hou Hsiao-Hsien. It features three chronologically separate stories of love between May and Chen, set in 1911, 1966 and 2005, using the same lead actors, Shu Qi and Chang Chen.

The film was nominated for the  , Armenia, for Best Feature Film and received positive reviews.

==Background==
Three Times was originally meant to be an omnibus collection of short films, with Hou Hsiao-hsien directing only one of the segments.  The producers were unable to rustle the financing to be able to hire three directors, so instead, Hou took over production. 

==Plot==

===A Time for Love===

( : liàn ài mèng) Set in Kaohsiung in 1966, with dialogue in Taiwanese Hokkien.

===A Time for Freedom===

( : zì yóu mèng) Set in Dadaocheng in 1911, with dialogue presented only through on-screen captions. The theme of freedom joins with the theme of love  -  the young courtesan dreams of liberty and Taiwan itself at this time was occupied by the Japanese.

===A Time for Youth===

( : qīng chūn mèng) Set in Taipei in 2005, with dialogue in Standard Chinese|Mandarin.

==Critical reception==
Three Times received generally positive, sometimes ecstatic reviews when it was released in North America.  It currently holds an 86% approval rating on Rotten Tomatoes.   Most critics agreed that the opening segment, A Time for Love, was the most successful, and that the final segment, A Time for Youth (which was frequently compared to Hous Millennium Mambo) was the least successful.  Response was somewhat mixed for the second segment, A Time for Freedom, with many critics likening it to Hous Flowers of Shanghai.

Roger Ebert, who championed the film at 2005 Cannes Film Festival|Cannes, gave it four stars out of four in his review for the Chicago Sun-Times: "Three stories about a man and a woman, all three using the same actors. Three years: 1966, 1911, 2005. Three varieties of love: unfulfilled, mercenary, meaningless. All photographed with such visual beauty that watching the movie is like holding your breath so the butterfly won’t stir"  

Kay Weissberg in  s ambivalent relationship with time and memory, Three Times forms a handy connecting arc between the Taiwanese helmers earlier work and the increasingly fragmentary direction of his recent films. Best appreciated by those familiar with his slow rhythms and pessimistic take on contempo life, pic presents three stories using the same leads set in three time periods to explore love and how the present circumscribes lives."  

Stephen Whitty of the Star-Ledger was not impressed: "According to one American critic, Three Times is why cinema exists. Only if you think that cinema has no higher calling than presenting a long series of gorgeously lit close-ups of beautiful actresses are you likely to agree." 

Independent filmmaker Jim Jarmusch:  "Hou Hsiao-hsien is not only the crowning jewel of contemporary Taiwanese cinema, but an international treasure.  His films are, for me, among the most inspiring of the past thirty years, and his grace and subtlety as a filmmaker remain unrivaled.  Film after film, Hou Hsiao-hsien is able to adeptly balance a historical and cultural overview with the smallest, most quiet and intimate details of individual interactions.  His narratives can appear offhand and non-dramatic, and yet the structures of the films themselves are all about storytelling and the beauty of its variations.  And Hous camera placement is never less than exquisite.  
 
His newest film, THREE TIMES, is also his newest masterpiece.  A trilogy of three love stories, Chang Chen and Shu Qi beautifully portray Taiwanese lovers in three distinct time periods:  1966, 1911 and 2005.  The first section (in 1966), just on its own, is one of the most perfect pieces of cinema I’ve ever seen.  The second, set in a brothel in 1911, remarkably explores dialogue and verbal exchange by almost completely eliminating sound itself (!), while the final piece leaves us in present-day Taipei — a city of rapidly changing social and physical landscapes where technology has a harsh effect on delicate interpersonal communication.  The resonance of these combined stories, their differences and similarities, their quietness and seeming simplicity, left me in a near dream-state - something that only happens to me after the most striking cinematic experiences.  
 
Now, for the first time, one of Hou Hsiao-hsiens films is finally being properly released (by IFC) in the U.S.  And this makes me, as a true fan, very, very happy."  

==Box office==

Three Times was released in the United States on April 26, 2006, and was only the second film by Hou Hsiao-hsien to receive theatrical distribution in the USA (the first was Millennium Mambo).  In its opening weekend on three screens, it grossed $14,197 USD ($4,732 per screen).  Never playing at more than five theatres at any point during its theatrical run, it eventually grossed $151,922 USD in total.

==Awards and nominations==

* 2005 Cannes Film Festival
** Nominated: Palme dOr 

* 2005 Golden Horse Awards
** Won: Best Taiwanese Film of the Year
** Won: Best Actress (Shu Qi)
** Won: Best Taiwanese Filmmaker (Hou Hsiao-hsien)
** Nominated: Best Actor (Chang Chen)
** Nominated: Best Art Direction
** Nominated: Best Cinematography
** Nominated: Best Director
** Nominated: Best Editing
** Nominated: Best Makeup and Costume Design
** Nominated: Best Picture
** Nominated: Best Original Screenplay

*2005 Hong Kong Film Awards
** Nominated: Best Asian Film (Taiwan)

*2006 Yerevan International Film Festival
** Won: Golden Apricot - Best Film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 