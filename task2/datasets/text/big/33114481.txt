In the Last Stride
 
{{Infobox film
  | name     = In the Last Stride
  | image    = 
  | caption  = 
  | director = Martyn Keith
  | producer = W.S. Jerdan
  | writer   = W.S. Jerdan Arthur Wright Dave Smith
  | music    = 
  | cinematography = Bert Segerberg   
  | editing  = 
| studio = Double A Productions
  | distributor = 
  | released = 22 November 1915 (private screening)    1 May 1916
  | runtime  = 5 reels 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Arthur Wright. Dave Smith, was a champion heavyweight boxer who had fought Les Darcy. There was also an appearance from boxer Les ODonnell. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 63  

Wright later described it as "a film of action, depicting the racecourse, opium-running on the Harbor, a football match, and a glove fight in which Dave Smith and Les ODonnell participated." 

It is considered a lost film.

==Plot==
Dave Smith, retired heavyweight boxing champion travels around Australia having various adventures, including working as a swagman, fighting Les ODonnell, a speedboat chase across Sydney harbour, a football game involving rugby league stars, and a climax in which Daves horse Sunlocks wins the Sydney Cup.

==Cast==
*Alma Rock Phillips as Enid King
*Dave Smith as Alick Wallace
*Les ODonnell
*Dunstan Webb as a black American and a young Australian squatter
*Mick King
*Jack Munro Charles Villiers
*Rock Phillips
*Pat McGrath
*Harry Jerdan
*Percy Walshe

==Original Novel==
{{Infobox book|  
| name          = In the Last Stride
| title_orig    =
| translator    =
| image         =
| caption = Arthur Wright
| illustrator   = Lionel Lindsay
| cover_artist  =
| country       = Australia
| language      = English
| series        = Bookstall series
| genre         = sporting
| publisher     = NSW Bookstall Company
| release_date  = 1914
| english_release_date =
| media_type    =
| pages         = 232
| isbn          =
| preceded_by   =
| followed_by   =
}}
The film is based on a novel by Arthur Wright, which sold 30,000 copies and had been serialised in Referee magazine. 

===Plot===
Alick Wallace, a champion rugby player and boxer, is loved by Enid King, who is desired by a football umpire, Norton, who also dabbles in opium smuggling. During a game, Norton sends Alick off the ground in disgrace. Alick boxes Norton at Sydney stadium, knocks him out and is fired from his job as a clerk by his employer, Enids father.

Alick finds himself falsely accused of burglary, robbery and smuggling, as well as the death of old King.  He heads to the Queensland bush and trains a horse which winds up winning the Winter Stakes at Randwick.

===Reception===
"No reader will be able to complain that he has not received his moneys worth of sensation", said the Sydney Morning Herald.  According to the Adelaide Register:
 Mr. Wright has produced another rattling yarn. He understands sport thoroughly. You might doubt it in finding a professional pugilist play football in the afternoon with a big fight booked for the evening; but the author admits that it looked an act of pure madness, and it was necessary for the story.  
The Western Mail said that Wright had manufactured "a story almost entirely composed of police court, football, boxing, and racing reports, written up in tho style and with all the literary graces of diction that distinguish the average horse Reporter of a Sydney sporting weekly." 

==Production== Dave Smith.  Smith was invited to play the lead. "Lets hope the enterprise meets with the success it deserves", wrote the Motion Picture News. 

The movie was shot in Sydney in 1915. One scene was shot on Mosman Oval featuring rugby league stars Dan Frawley, Bob Tidyman and Harold Horder. 

Dave Smith later wrote some reminisces of filming, in particular fight scenes with fellow boxer Les ODonnell: For the Term of His Natural Life (1927) also played with us. One scene was on a launch up in Middle Harbor, and Webb in a melee, was to be shot and then fall back into the water. We ran through the stuff all but the falling in, before the take, and when the director was satisfied the operator started to film. We floated through our parts and Webb was shot, but when falling back into the water, thought of the grey nurses and tiger sharks lurking below arid turned finishing his fall with a neat header into the water.  

==Reception==
The movie was previewed at the Globe Theatre in Sydney in November 1915  where, according to one account, "all present were fully convinced that the production was quite equal to any of the Imported films." 

However the movie was not a popular success: Wright later wrote that he "doubt if they ever recovered" the budget for the film at the box office. 

==References==
 

==External links==
*  
*  at AustLit
*  at National Archives of Australia
*The original story was serialised at the Port Macquarie News in 1939 –  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  
*  at Australian Screen Online
 

 
 
 
 
 
 
 


 