The Last Post (film)
{{Infobox film
| name           = The Last Post
| image          =
| caption        =
| director       = Dinah Shurey
| producer       = Dinah Shurey
| writer         = Dinah Shurey Lydia Hayward
| starring       = John Longden Frank Vosper Cynthia Murtagh
| cinematography = D. P. Cooper
| editing        =
| studio         = Britannia Films
| distributor    = Gaumont British
| released       =  
| runtime        = 8040 feet
| country        = United Kingdom
| language       =
}}
 1929 British silent drama film directed by Dinah Shurey and starring John Longden, Frank Vosper and Alf Goddard.  The film was the first (and would turn out to be the only) solo directorial venture by Shurey, who was the only female producer and director working in the British film industry at the time.  It also became the main source of a libel action launched by Shurey against the magazine Film Weekly.

==Plot==
Identical twin brothers Martin and David have grown up both loving the same girl, Christine.  Christine in turn loves Martin, the more stable and easy-going of the two where David is more impulsive and volatile in temperament.  The brothers both go off to serve in World War I, where David is badly wounded and sent home to England to recuperate.  Despite her love for Martin, Christine agrees to marry David who she feels needs her more.

After the war, David becomes increasingly restless and disillusioned with the society he sees around him.  He becomes involved with anti-establishment elements and joins a Bolshevik group.  During the General Strike of 1926 the group hatch a plot to steal ammunition, which goes badly wrong when a soldier is killed during the botched operation.  However it is Martin who is arrested, charged and sentenced to death for the crime, accepting his fate out of loyalty to his brother and the wish for the unknowing Christine not to have to face the fact that her husband is a killer.  But at the last moment, David is unable to watch his brother die for his crime and confesses his guilt. 
 
==Cast==
* John Longden as David / Martin
* Frank Vosper as Paul
* Cynthia Murtagh as Christine
* Alf Goddard as Tiny
* J. Fisher White as Mr. Blair
* Johnny Butt as Goodson
* Rolf Leslie as Stephan

==Libel case==
On its theatrical release, The Last Post picked up mixed reviews.  While The Bioscope found it had "many points of strong interest, working up to a profoundly impressive climax" and Kine Weekly concluded "although the theme is heavy and the unfolding somewhat prolonged, the picture combines sentiment, sacrifice and patriotism in just the right proportions to appeal to the masses", Herbert Thompson of Film Weekly damned the film as "clap-trap patriotism, one of the worst films I have ever seen...the direction is unenterprising, and the film rarely moves faster than the funeral procession with which it opens".   

In a column published on 10 June 1929, Thompsons Film Weekly colleague, the famously blunt and outspoken critic Nerina Shute, posed the question "Can Women Direct Films?"  Using The Last Post as prime evidence, Shute concluded: "It is pathetically obvious that women can’t produce films. In England only one lady has had the temerity to try. Dinah Shurey (who will go to heaven by reason of her great courage) has created several appalling pictures."   The highly offended Shurey promptly launched a libel action against Film Weekly.  When the case came to trial in February 1930, the defence counsel for Film Weekly avoided a debate about the merits or otherwise of Shureys films, instead putting the case that regardless of the unpalatability to Shurey of Shutes opinions, under English law Shute had every right to express them, and Film Weekly had every right to publish them.  The jury thought differently however and found in favour of Shurey, awarding £500 damages and costs against Film Weekly. 

==Later history== 75 Most Wanted" list of missing British feature films.  

==References==
 

== External links ==
*  
*   - contains numerous stills

 
 
 
 
 
 
 
 