Geliebte weiße Maus
 
{{Infobox film
| name           =Geliebte weiße Maus 
| image          = 
| caption        =
| director       = Gottfried Kolditz
| producer       = Eric Kuhne
| writer         = 
| starring       = Rolf Herricht Karin Schroder
| music          = Conny Odd
| cinematography = Gunter Haubold
| editing        =
| distributor    = 
| released       = 1964
| runtime        =
| country        = East Germany
| language       = German
| budget         =
| gross          =
}}
Geliebte weiße Maus  (Beloved White Mouse) is a 1964 East German musical film. Produced by Eric Kuhne, the film had music by Conny Odd with cinematography by Gunter Haubold.    Fritz Bachmann (Rolf Herricht) is the Weiss Maus (White Mouse), a term for the traffic policemen in East Germany because of their white uniform and cap, who gets involved with a girl, Helene Brauer (Karin Schroder), on a red scooter who drives his route everyday for work.

==Cast==
* Rolf Herricht: Fritz Bachmann
* Karin Schröder: Helene Bräuer
* Marianne Wünscher: Mrs. Messmer
* Gerd Ehlers: Mr. Simmel
* Jochen Thomas: Captain Gabler
* Mathilde Danegger: Mother Hirsch
* Werner Lierck: advertiser
* Carola Braunbock: neighbor
* Peter Dommisch: Lörke
* Siegfried Göhler: officer Rudolf Donath: policeman Rudolf Fleck: waiter
* Ingeborg Krabbe: secretary

==References==
 

==External links==
*  

 
 
 
 
 


 