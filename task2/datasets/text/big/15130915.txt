The Million Dollar Mystery
 
{{Infobox film
| name           = The Million Dollar Mystery
| image          = Themilliondollarmystery-1914-Episode-3-front.jpg
| image_size     =
| caption        = Brochure for Episode 3 showing actor James Cruze
| director       = Howell Hansel
| producer       = Edwin Thanhouser
| writer         = Lloyd Lonergan Harold McGrath
| narrator       =
| starring       = Florence La Badie Marguerite Snow James Cruze Frank Farrington Sidney Bracey Lila Chester
| music          = George Webber
| editing        =
| distributor    = Thanhouser Film Corporation
| released       =   
| runtime        = 23 chapters
| country        = United States English
| budget         = $125,000 approx. {{cite book
 | last = Stedman
 | first = Raymond William
 | authorlink =
 | title = Serials: Suspense and Drama By Installment
 | origyear = 1977
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-1695-2
 | pages = 14 & 22
 | chapter = Drama by Installment
 }} 
| gross          = $1,500,000 
}} film serial directed by Howell Hansel, and starring Florence La Badie and James Cruze. 

==Production background==
The film was produced by Thanhouser Film Corporation and filmed in New Rochelle, New York. The film serial was the companys biggest success, largely due to the popularity of La Badie who performed her owns stunts.  Publicity gimmicks, including a prize for the member of the public who sent in the best idea for a conclusion to the serial, were used to boost the serials success.  The Million Dollar Mystery was the new serial project from the Chicago Tribune following the success of The Adventures of Kathlyn.  The serial was released with the gimmick that the last chapter was unwritten.  Twenty two chapters were written based only on the title, while the serial was left purposefully unfinished with no final chapter.  

A prize of $10,000 was offered for the best suggestion (advertised as "$10,000 for 100 words").   Thousands of letters were received in response to this and Ida Damon, a secretary from St. Louis, Missouri|St. Louis, won the prize.  {{cite book
 | last = Lahue
 | first = Kalten C.
 | title = Continued Next Week
 | year =
 | publisher =
 | isbn =
 | pages = 6
 | chapter = 1. A Bolt From The Blue
 }}   In a further publicity stunt, the character Florence Hargreaves was actually reported missing.  Details of the plot were fed to newspapers and the police as if they were real events.  Seven days passed before this story was exposed as fiction. 

==Chapter titles==
#The Airship in the Night
#The False Friend
#A Leap in the Dark
#The Top Floor Flat
#At the Bottom of the Sea
#The Coaching Party of the Countess
#The Doom of the Auto Bandits
#The Wiles of a Woman
#The Leap from an Ocean Liner
#The Elusive Treasure Box 
#In the Path of the Fast Express
#(Unknown)
#(Unknown)
#(Unknown)
#The Borrowed Hydroplane
#Drawn Into the Quicksand
#A Battle of Wits
#(Unknown)
#(Unknown)
#(Unknown)
#(Unknown)
#The Million Dollar Mystery
#The Mystery Solved
 SOURCE:  

== Plot ==
A prologue for The Million Dollar Mystery introduced the characters and groups. After the opening title card shows "hundreds of hands" grasping for the money and then a shot of the check for $10,000 the solver of the mystery is shown prior to the beginning of the first reel. Baby Florence is left at a boarding school with a note and half a bracelet instructing that her father will come to take her back upon her eighteenth birthday. Hargreaves, here played by Alfred Norton, is chased by the Black Hundred, but he receives a note and money before attempting to escape by balloon on the top of a building. The balloon is shot down and the first chapter ends.  

==Cast==
*Florence La Badie as Florence Hargreaves   raised under the name "Florence Gray"    
*Albert Norton as Stanley Hargreaves, Florences father.  
*Sidney Bracey as John - Hargreaves Butler    
*Marguerite Snow as Countess Olga Petroff  
*James Cruze as Norton, a newspaper reporter  
*Frank Farrington as Braine, the leading conspirator.  
*Lila Chester as Susan, friend of Florence.  

*Donald Gallaher in Bit Part (possible cameo as himself)

*May Wallace as the wife.  

Important figures cast in scenes included William Montagu, 9th Duke of Manchester.   The Duke of Manchester was cast in a scene aiding Florence in escaping from Countess Olga and her "band of conspirators".  

== Production ==
The story was written Harold MacGrath and the scenario was written by Lloyd Lonergan.  
 Francis Wilson was purchased for use in the production and would also be the meeting location of the final details surrounding the release by the members of the Thanhouser Company.   The historic Francis Wilson house was known as the "house of mystery" in the production. Lonergan insisted that Thanhouser Company president Charles J. Hite acquire the house for the production, but attempts to rent the house for the production failed after the plans to use the grounds for "balloonists, soldiers and the actors for several months".   Hite would provide the $200,000 for its purchase and the house was quickly made ready for the production.     In a tour of the house, a Motion Picture News writer declared "is furnished in keeping with the imposing exterior of the mansion. Priceless Persian rugs carpet the entrance hall. In the dining-room heavy carven furniture groan under the weight of the silver service and in every room on the three floors it is apparent that exceptionally good taste has been used in the selection of the works of art which eventually will be seen in the completed film, "The Million-Dollar Mystery."  

Other sets used by the Thanhouser company included libraries stocked with real books and a Tudor staircase removed from an English country home.   Costume costs for the production were noted to be quite high, with Marguerite Snows costume bills reaching over $2000 despite having only acted in six of the 46 reels and expects it to exceed $10,000.   A walking staff once worn at the Russian court was given to Snow by Colonel Sandor Radanivich, said staff was claimed to be over fifty years old and displays iridescence when held up to the light.  

For the production of the underwater scene, Carl Gregory was reported to have sailed to the Bahamas.   John Ernest Williamson used his fathers invention to allow for the first underwater films to be shot. For the scene, Gregory was lowered into the ocean depths in a unique diving bell-like apparatus and shoots the film with lighting aid provided by the mothership.    

Another scene was done with the permission of the White Star Line and it involved a steamer unloading and a watchmans shanty at the edge of a pier on the Hudson River.  

The film was not produced without its own close calls in its stunts. For the production of the first episode, a professional aeronaut assisted Albert Norton in the balloon escape from the roof of a building. Danson Michell of The Motion Picture News would write that "Norton narrowly escaped losing his life in this attempt."  

The production was released through Syndicate Film Corporation.  

== Publicity ==
Motion Picture News reported that the meeting to finalize the arrangements regarding the serialization of the story in the Chicago Tribune would take place on April 10, 1914 between Thanhouser Company president Charles J. Hite, Harold MacGrath the novelist and Joseph Medill Patterson of Chicago Tribune.     More than 200 different papers, including the Chicago Tribune would begin serialization of the story on June 28.  

The famous detective William J. Burns would also become a part of the fervor surrounding The Million Dollar Mystery and published his insights into solving the mystery in The Movie Pictorial.    

== Release ==
Before the official release of the film, film exhibitors and some friends, were noted to have viewed the first two reels of The Million Dollar Mystery before the public debut. This early viewing, attended by about a hundred viewers, was authorized in part by F. P. Glenn of the Syndicate Film Company.  

The film series is now considered to be lost film|lost.   

==Sequel and remake==
*The serial was highly successful and the stockholders received a return of 700% on their investment, receiving $1.5M at the box office for a cost of $125,000 "or so."  

*As a result, another serial already in production was changed to become a sequel to The Million Dollar Mystery.  The serial Zudora was renamed The Twenty Million Dollar Mystery and changes were made to the plot halfway through its release.  It was not, however, as successful as its predecessor.  

*In June 1918, The Million Dollar Mystery was re-edited to six reels and released as a feature film through Arrow Film Corporation. 
 James Kirkwood.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 