What Happens Next? (film)
{{Infobox film
| name           = What Happens Next?
| image          = WhatHappensNextCoverPhoto.jpg
| producer       = Brent Hodge Jon Siddall
| director       = Brent Hodge Jon Siddall
| starring       = Dan Mangan Kenton Loewen Gordon Grdina Kirsten Slenning
| released       =  
| runtime        = 44 minutes
| country        = Canada
| language       = English
}}

What Happens Next? is a documentary film about the Canadian singer-songwriter, Dan Mangan. It explores Mangans ideas on fate and destiny as hes about to perform in his biggest performance to date, a sold out show at the Vancouver, British Columbia Orpheum (Vancouver)|Orpheum. It premiered on CBC Television on August 25, 2012.  The movie was directed and produced by Brent Hodge and Jon Siddall.  It was nominated for a Leo Award in 2013. 

==Cast==
* Dan Mangan
* Kenton Loewen 
* Gordon Grdina 
* Kirsten Slenning

==Synopsis==
The movie starts by introducing Dan Mangan, a singer-songwriter from Vancouver, who has built his reputation through continuous touring, with and without a backing band. He counters those who say that "making it in life has everything to do with luck" with a quote from his father, that those who work the hardest tend to be the luckiest.

The film interleaves the build up to the show at the Orpheum with footage from a poker game with drummer Kenton Loewen, and guitarist Gordon Grdina as well as Dans then fiancee (now wife) Kirsten Slenning, and Dan, as they discuss the upcoming show, the end of their current 42 show tour,  creativity, the past, and life after playing the Orpheum(a lifelong goal of Dans). 
It also contains some behind the scenes footage of the filming of the music video for the song About As Helpful As You Can Be Without Being Any Help At All off his album Oh Fortune directed by Stuart Gillies. 
The main theme of the movie is how stardom doesnt happen over night. That its a slow progression of winning over a fan, one at a time, through hard work and touring. It also touches on the fleeting nature of fame, and impermanence of art. 
The movie concludes with the opening of the show, a few small clips of the performance, and then the band and accompanying orchestra bowing on stage, begging the question, what happens next?

==Reception==
The film received mostly positive reviews and was nominated for a Leo Award.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 