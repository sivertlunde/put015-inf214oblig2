Child of the Danube
Child Austrian musical film directed by Georg Jacoby and starring Marika Rökk, Fred Liewehr and Harry Fuß. It was one of a cycle of popular musicals made by Jacoby and Rökk.

==Partial cast==
* Marika Rökk - Marika 
* Fred Liewehr - Georg 
* Harry Fuß - Heinrich 
* Fritz Muliar - Oskar 
* Joseph Egger - Christoph 
* Annie Rosar - Frau Kovacs 
* Helli Servi - Edith 
* Nadja Tiller

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 


 