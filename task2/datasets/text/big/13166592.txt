Bye Bye Bluebird
{{Infobox Film 
| name           = Bye Bye Bluebird
| image          = Bye Bye Bluebird DVD.jpg
| caption        = Front cover of the DVD for Bye Bye Bluebird
| director       = Katrin Ottarsdóttir
| producer       = Annette Nørregaard
| writer         = Katrin Ottarsdóttir
| starring       = Hildigunn Eyðfinsdóttir Sigri Mitra Gaïni
| music          = Hilmar Örn Hilmarsson
| cinematography = Jørgen Johansen
| editing        = Elisabet Ronaldsdóttir
| distributor    = Peter Bech Film
| released       = 29 January 2000
| runtime        = 97 minutes
| country        = Denmark Faroese Danish Danish English English French French
| budget         = 
}}
Bye Bye Bluebird is a 1999 Danish-Faroese comedy film directed by Katrin Ottarsdóttir and starring Hildigunn Eyðfinsdóttir and Sigri Mitra Gaïni. The satirical film relates the tale of two eccentric young women who, after years abroad, return to their native Faroe Islands, and embark on a strange road trip. The film received awards at several film festivals including Lübeck Nordic Film Festival, Rouen Nordic Film Festival and the International Film Festival Rotterdam.

==Cast==
*Hildigunn Eyðfinsdóttir as Rannvá
*Sigri Mitra Gaïni as Barba
*Johan Dalsgaard as Rúni
*Elin K. Mouritsen as Barbas mother
*Peter Hesse Overgaard as Rannvás stepfather
*Nora Bærentsen as Rannvás mother
*Egi Dam as Rannvás father
*Lovisa Køtlum Petersen as Rannvás daughter
*Adelborg Linklett as Rannvás grandmother
*Sverri Egholm as Rannvás grandfather
*Birita Mohr as Waitress / Singer
*Sjúrður Sólstein as Smukke
*Høgni Johansen as Helmsman
*Kári Øster as Hærget Mand
*Anna Kristin Bæk as Blafferpige

==External links==
*  at Det Danske Filminstitut (in Danish)
*  

 
 
 
 
 
 
 

 
 