À Nous la Liberté
{{Infobox film
| name           = À nous la liberté
| image          = A-nous-la-liberte-poster.jpg
| caption        =
| director       = René Clair Frank Clifford Uncredited: Alexandre Kamenka
| writer         = René Clair Henri Marchand Raymond Cordy Rolla France
| music          = Georges Auric
| cinematography = Georges Périnal
| editing        = René Le Hénaff
| distributor    = Films Sonores Tobis Joseph Burstyn (1954 US re-release)
| released       =  
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         =
}}

À nous la liberté (English: Freedom for Us) is a 1931 French film directed by René Clair. With a score by Georges Auric, this film has more music than any of Clairs early films.

==Plot summary==
 
The film opens with an image of a wooden toy horse. Gradually we observe that this is an assembly line in a prison, staffed by prisoners.  They sing (La liberté, cest pour les heureux = "Freedom is for the happy") as they work.  Close-ups of two prisoners (Louis and Émile, the films main protagonists) indicate theyve taken a work tool.  The prisoner next to Louis occasionally looks on, looking somewhat bored.  After dinner everyone goes back to their cell.  After feigning sleep during a guards nightly rounds, Louis and Émile sing the title song as they resume a project of sawing off the prison window.   Émile cuts himself, and Louis kindly mends the wound with a handkerchief.  The window breaks free and they attempt to escape.   Louis is able to get over the retaining wall, but Émile is not successful.  Louis escapes, accidentally knocks someone off a bicycle, and rides off on the bicycle.  Meanwhile we hear a chorus suggesting hes about to be captured (Ce sera bientôt fini = "Itll soon be over").  Louis heads into a village emblazoned with the words "Finishing Line" - the cyclist he knocked over was in a bicycle race, and Louis has won first prize.

Louis enters a store to purchase some handkerchiefs.  While the proprietor is looking in a backroom, he hears Louiss muffled cries for help.  After being unbound, Louis explains that someone robbed the store and made off with the money.  He points the direction and a group of people run after the thief, leaving Louis alone, revealing that it was he who stole the money, feigning the story.  

A montage sequence follows in which we see Louis transform himself from a poor record merchant, to the well-attired and well-mannered head of an industrial factory that produces record players.  Interior shots of the assembly line bear a strong resemblance to the assembly line seen at the beginning of the film.

Meanwhile, behind the factory we see an open field.  Émile (apparently now out of prison) has been sleeping, and wakes up to a beautiful day.  A flower sings "Ami, lombre de la prison a cédé la place au soleil" = "Friend, the shadow of prison has given way to the sun."  A policeman comes by and tells Émile he must get to work.  But he is put in a cell for resisting arrest.  Through the prison window, he sees more flowers surrounding an apartment window and thinks he hears them singing ("Viens, toi que jaimerai" = "Come, you who I will love").  Then a lovely woman, Jeanne, appears at the window and appears to be the source of the singing.  Sad at his predicament, he tries to hang himself from the prison window.  But the gate is too weak, comes loose and falls on Émiles head, enabling him to escape.  He stands by the apartment entrance, looking up at the flower-covered window, but is slightly disappointed to realize that the flowers are not singing, and that its only a recording.  Then he realizes that Jeanne has emerged from the apartment with her Uncle, who appears to be overly-protective and pulls her away from Émile, and kicks him.  A commotion and chase ensue in which Émile runs as we realize that Jeanne already has a boyfriend, Paul.  
 Modern Times see below.)  A guard tries to stop Émile from talking to Jeanne and they chase after him.  He is stopped by guards near the foot of a grand staircase at the doorway of an office.  Louis emerges with aides, and Émile gets his attention.  At first Louis does not appear to remember his prison friend, but takes him into his office, thinking this is an extortion plot.  After a scuffle, Émile cuts himself.  As Louis tends to the wound with his handkerchief, he recalls that he did similarly when the two tried to break out of prison at the films outset.  His attitude changes to one of friendship, as he embraces Émile and sing a brief reprise of the title song.

The scene is a dinner party at Louiss house.  Neither his guests nor his wife Maud, nor her gigolo lover think much of Louis.  Meanwhile, Louis and Émile dont care, and enjoy sending up the haughtiness of his life style, climaxing in another reprise of the title song, as the two friends dance in front of a painting of Louis which he has damaged with a bottle of wine.

With her suitcases packed, Maud leaves the house.  Émile also leaves, and briefly encounters an ex-prisoner on the street.  (This is the same person who was sitting next to Louis at the films outset.)  Louis is thrilled to look out the door at the departing Maud, but does not see the ex-prisoner, who makes a nod of understanding to himself.

Back at the factory, Émile tries to make overtures to Jeanne, but is thwarted by guards, eventually finding himself in Louiss office.  Louis has been explaining that his new factory will opening the next day, increasing productivity.  Upon seeing his friend interrupt, Louis is slightly annoyed until Émile explains that he wants to court Jeanne, another worker.  From his office card files, Louis is able to produce a picture profile of Jeanne, but this is automatically followed by a profile of her Uncle.  Louis invites both Jeanne and her Uncle into his office to explain Émiles interest, and offers some money along with it.  Uncle is impressed, but Jeanne is bewildered and unhappy.

Louis arrives at home, wondering where his servants are.  He discovers them tied up, and enters a room filled with ex-convicts, now gangsters, led by the person who sat next to him in the opening scene and who encountered Émile the other night.  They sing briefly, before the lead gangster reveals their purpose.  They want to extort money from Louis by threatening to reveal that hes an escaped convict.  Louis refuses to accept.  

Lovers quarrel in the Magic Park.  The scene opens in Luna Park, with Émile happily talking with Uncle, while Jeanne looks very unhappy.  Paul sitting at a distance, looks on disapprovingly.  During the scene, Jeanne is able to get away and be alone with Paul.  Émile, unseen, finally finds her and realizes that she already has a boyfriend.

Émile longingly looks up at Jeannes apartment window, as she happily waves to Paul.  He realizes that he cant have her.  While hes alone on the street, a policeman tries to question Émile, who runs into the factory for safety, but is pursued by guards.

Meanwhile, the gangsters have asked to visit Louiss factory.  He leads them into a secret room within his office, and then seals the door behind him.  As the gangsters realize theyre trapped, Louis empties his safe of all his money, putting it into a small suitcase on his desk.  Émile finds him, and Louis explains that hes about to be denounced by the gangsters.  He then hides Émile while he briefly speaks to the guards who have been searching for his friend.  At the same time, another ex-convict enters Louiss office and takes the suitcase.  When Louis returns with Émile, he realizes the suitcase is missing and tries to look for the person with it.  Pursued by guards, Émile accidentally opens the secret room through which all the gangsters emerge.  Both Émile and the gangsters chase after Louis, who chases after the ex-con who took the suitcase.  This ex-con is able to penetrate to the roof the factory, but is caught, leaving the suitcase on the roof.  Apprehended, the gangsters show the police Louiss picture as a convict, but they are taken away.

It is the inauguration of the new factory with crowds of dignitaries and workers assembled.  Louis gives a speech extolling its virtues of productivity.  A deaf old man cuts the ribbon, and a chorus sings a slow march, "Gloire au bonheur" = "Hail to happiness," as the automatic assembly line produces portable phonographs.  Another speaker begins a speech, while Louis sees the policeman.  The policeman has realized that Louis is an escaped ex-convict, and patiently waits for the festivities to end to apprehend him.  Louis makes a concluding speech in which he gives the factory to the workers, and says that fate will take him to a different place.  As another speaker continues, a wind begins to pick up, gently blowing the decorations.  It also gradually begins to blow the money that was in Louiss suitcase, still sitting on the roof of the factory.  Gradually, bills appear on the factory grounds, testing everyones propriety, as they look frustrated at not wanting to appear uncivilized by bending down to pick up the money.  But Louis has no inhibitions:  He immediately recognizes what and where the money is and begins going after it.  This is turn launches everyone into a chase after the money.  The scene turns to a merry chaos as the dignitaries in top hats wildly chase after money.

The final scene of the film shows the now-idyllic factory.  Instead of working, only a few workers now play cards, as the automated factory does all the work.  The camera pans along to another area by a brook, festively decorated with ribbons.  Most of the workers are here, dancing and enjoying themselves to a reprise of the song "Ami, lombre de la prison" which blends into "Viens, toi que jaimerai" as we see Jeanne and Paul, happily dancing with each other.  A cut takes us to Louis and Émile, now tramps, entertaining people on a roadside by singing the third verse of the title song.  The people throw coins at the two tramps.  A rich car passes, making Louis momentarily dream of what he once had.  After a swift kick in the rear by Émile, the two head off down the road to the final strains of the title song.

==Cast== Henri Marchand as Emile
*Raymond Cordy as Louis
*Rolla France as Jeanne
*Paul Ollivier as LOncle
*Jacques Shelly as Paul
*Andre Michaud as Le Contremaitre
*Germaine Aussey as Maud - La Femme De Louis
*Leon Lorin as Le Vieux Monsieur Sourd
*William Burke as LAncien Detenu
*Vincent Hyspa as Le Vieil Orateur

==Production== Sous les toits de Paris (1930) and Le Million (1931), À nous la liberté shows Clair continuing to experiment with the possibilities of sound film. The image of a flower in combination with an unseen voice leads the viewer to think the flower is singing.  Once accepted, the viewer is led to accept that a chorus of flowers is singing when Émile views the window from prison. As he does with narrative, Clair reveals the truth slowly and in a circuitous way so as to produce comedy and satire, in this case, by first suggesting the flowers are singing, and then that Jeanne is singing, when in fact it is a phonograph—revealed only because it runs down.

An aural flashback occurs when Émile re-encounters Louis, and a small argument results in Émile getting cut. As Louis bandages the cut, the soundtrack plays the non-musical marching of the prisoners (who wore wooden clogs).

Many sound effects are achieved not through natural sound but through Aurics musical score. In the phonograph factory, the "sound" of assembly line mechanization is done through music (using xylophones, among other instruments). There are several passages such as when Louis keys in the numbers to retrieve the profiles of Jeanne and her Uncle, where only the music supplies an accompanying sound.

==Release and reception==

===Politics===
À nous la liberté comments on society by depicting industrial working conditions as not much different from being in prison. DVD Verdicts Barrie Maxwell adds that the film depicts "a France oblivious to all going on around it, as portrayed by the sequence in which an aging French politician drones on to his audience about justice and liberty and patriotism, while the audience has long since lost interest, preferring instead to concentrate on chasing money that has accidentally fallen out of a bag and is now blowing in the wind." 

The film had two scenes cut out of it after its original release by its director. Many fans of the film have objected to this since the cut footage adds up to about ten minutes of footage. 

===Chaplin controversy=== Modern Times (1936), which bore some similarities to this film, such as the conveyor belt gags. In the end, instead of going to court, they reached a settlement,  but the whole controversy took around a decade. Chaplin maintained that he had never seen the film, as did everyone else at the studio. 

René Clair himself was never a part of the case and was actually quite embarrassed by it, since he had great admiration for Chaplin and had always maintained that they were all in debt to him, and any inspiration Chaplin might have gotten from his film would be an honor for him. A speculation over this case was that it was a conspiracy from Nazi Germany to discredit Chaplin; À Nous la Libertés production company,  , was German. It is notable that the out-of-court settlement was reached only after the end of World War II.

===1950 version===
In 1950, Clair re-released the film, deleting two scenes:
#The sequence where the flower sings as Émile wakes up on a field; and
#Lovers quarrel in the Magic Park.

==Bibliography==
*À Nous La Liberté, and Entracte: films by René Clair, English translation and description of the action by Richard Jacques and Nicola Hayden, New York: Simon and Schuster, 1970, ISBN 0-671-20617-6
*Cinema yesterday and today, by René Clair, translated by Stanley Appelbaum, edited, and with an introduction and annotations by R. C. Dale, New York: Dover, 1972, ISBN 0-486-22775-8

==See also==
* Entracte (film)

==External links==
*   Michael Atkinson

 

 
 
 
 
 
 
 
 
 
 