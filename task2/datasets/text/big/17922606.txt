Aram + Aram = Kinnaram
{{Infobox film
|  name           = Aram + Aram = Kinnaram
|  image          = Aram_+_Aram_Kinnaram_300.jpg Sreenivasan  Sreenivasan  Shankar Sreenivasan Sreenivasan Jagathi Lizy Maniyanpilla Raju
|  director       = Priyadarshan
|  cinematography = S. Kumar
|  editing        = N. Gopalakrishnan
|  released       =  
|  country        = India
|  language       = Malayalam
|  Budget         = 1Cr. Rs
}} Lizy and Puja Saxena in leading roles, this film was a huge hit at the box office.   

== Plot ==
Story revolves around two friends Narayanan Kutty (Mohanlal) and Balan (Shankar Panicker). Working as a mechanic at K&K Automobiles in Chennai, Narayanan Kutty is always lusty towards girls. Though he has never been successful in wooing any girl, Narayan Kutty finds fun in boasting about his encounters with rich girls in and around the city. Manoharan (Jagathy Sreekumar), the proprietor of K&K Automobiles always falls in deep trouble because of the careless attitude of Narayan Kutty at work place. Balan  a close buddy of Mala imposters himself as Mala and contacts Narayanan Kutty. Kutty, who has never seen Mala, starts believing Balan to be original Mala. But as part of a plan to fool him, Mala approaches him as Sujatha, a news reporter to interview him about his relationship with the daughter of M.N Nambiar. In short time, Narayanan Kutty falls in love with Mala, whom he mistakes to be Sujatha (Lizy Priyadarshan). Things get much complicated for him as she also reciprocates in the same manner. On the other hand, the life of Balan was going too smoothly until the parents of his fiancée arrives in the city along with her to see his office. Balan, who is actually an attender at the office had lied to them that he is a manager of the company. To add more trouble, M.N Nambiar decides to marry again, which his daughter objects vehemently, inviting more clashes. How Balan and Narayanan Kutty solve out the troubles and loose the knots that they created, forms the rest of the story.

== Cast ==
* Mohanlal as Narayanankutty
* Shankar Panicker as Balan
* Pooja Saxena as Mala
* Lizy Priyadarshan as Sujatha
* C. I. Paul
*Meenakumari
* Sukumari as Karthiyayani
* Kundara Johny
* Maniyanpilla Raju as Sharavanan
* Jagathy Sreekumar as Manoharan
* Thilakan as M.N.Nambiar Sreenivasan as Gopi Krishna
* Ragini as Ammukutty

==Soundtrack== 
The music was composed by Raghu Kumar and lyrics was written by Poovachal Khader. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Poru Neeyen Devi || K. J. Yesudas || Poovachal Khader || 
|- 
| 2 || Premichu Poy Ninne || P Jayachandran, Unni Menon, Miss Reddy || Poovachal Khader || 
|} 

==References== 
 

== External links ==
*  
*  

 

 
 
 
 
 
 

 