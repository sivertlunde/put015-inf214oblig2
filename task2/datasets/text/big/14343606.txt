The Life and Adventures of Nicholas Nickleby (1947 film)
 
 
:For other uses, see The Life and Adventures of Nicholas Nickleby (disambiguation).

{{Infobox film
| name           =The Life and Adventures of Nicholas Nickleby
| image          = Nicholas_Nickelby_1947_UK_poster.jpg
| image_size     =
| caption        = Original UK 1947 quad format poster Cavalcanti
| producer       = Michael Balcon
| screenplay       = John Dighton
| based on       =  
| starring       = Stanley Holloway Derek Bond Cedric Hardwicke Sally Ann Howes 14th Baron Berners
| cinematography = Gordon Dines Leslie Norman
| studio         = Ealing Studios
| distributor    = General Film Distributors
| released       =  
| runtime        = 108 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 1947 Cinema British drama sound screen adaptation of the book followed silent films released in 1903 and 1912.

==Plot==
After the patriarch of the family dies and leaves them with no source of income, Nicholas Nickleby, his mother, and his younger sister Kate venture to London to seek help from their wealthy, cold-hearted uncle Ralph, an investor who arranges for Nicholas to be hired as a tutor at Dotheboys Hall in Yorkshire and finds Kate work as a seamstress. Nicholas meets Mr. Squeers just as he concludes business with Mr. Snawley, who is "boarding" his two unwanted stepsons.

Nicholas is horrified to discover his employers, the sadistic Mr. and Mrs. Squeers, run their boarding school like a prison and physically, verbally, and emotionally abuse their young charges on a regular basis. He eventually rebels and escapes, taking with him young crippled Smike.
 French for the Kenwig daughters comes to comic disaster. He and Smike decide to search for work elsewhere. As they are leaving the city, they make the acquaintance of Madeline Bray, the sole support of her father, who gambled away his fortune and now is indebted to Nicholass uncle.

In search of food and lodging, they stop at an inn, and the proprietor introduces them to actor-manager Vincent Crummles, who owns and operates a travelling a theatrical troupe with his wife. Crummles hires them as actors and casts them in a production of Romeo and Juliet. Despite its success, Nicholas decides to return to London when he receives a letter from his uncles valet, Newman Noggs, who urges him to come back as quickly as possible, as his uncle has put his sister in great jeopardy despite a promise by him to make certain they come to no harm.

Kate has been subjected to unwanted attention from Sir Mulberry Hawk and Lord Verisopht, clients of her uncle, and when Nicholas overhears them bawdily discussing her in a tavern he is determined to defend his sisters honour. The cowardly Hawk refuses Nicholass demand to "step outside" and flees, resulting in a carriage accident in which Hawk is injured. Hawk and Lord Verisopht argue over Hawks lack of honour, and Hawk kills Lord Verisopht in a duel with pistols. Ralph Nickleby loses 6000 pounds in debt owed him, much to the delight of Noggs, who harbours a hidden desire for revenge against his employer.

Nicholas finds employment as a clerk with the benevolent Cheerybles, portly twin brothers whose nephew Frank begins to court Kate. They provide him a cottage in which Nicholas can place his family and Smike, who has been accepted warmly by all. Meanwhile, Squeers returns to London, planning to capture Smike and bring him back to Dotheboys Hall, and is engaged by Ralph Nickleby to stalk Nicholas and Smike. Squeers and Mr. Snawley make off with Smike "on the wishes of his father". Nicholas, aided by Noggs, intercepts them and foils the plot. Smike, severely beaten by Squeers, is nursed by Kate and falls in love with her.

Nicholas meets Madeline a third time when the Cheerybles assign Nicholas to help her situation in secrecy from her father. His uncle has been trying to coerce her father into giving Ralph her hand in marriage in exchange for settlement of his debt, and Mr. Bray finally accedes. Noggs warns Nicholas, who arrives at the Bray lodgings to find Madeline, unhappily dressed in a wedding gown, awaiting her fate. In a showdown with Ralph, they discover her father dead in his bedroom after Kate reveals to Madeline the true nature of Ralph Nicklebys character. Madeline faints and Nicholas carries her away, warning Ralph to leave her alone as she is now free of all obligations.

Ralphs hatred of Nicholas makes him determined to ruin him, but he is brought up short by Noggs, who has realized from the facts told him by Nicholas that Smike is actually Ralphs son, whom Ralph had Noggs take to Dotheboys. Ralphs hold over Noggs has compelled him to harbour the secret for fifteen years. Smike was sent to the Squeers after his mothers death, using a forged birth certificate, so that Ralph could keep her inheritance rather than let their child have it, as dictated by law. Further, Squeers hired Snawley to act the part of Smikes father to make his kidnapping appear legal. Noggs delights in telling Ralph that Squeers has confessed the conspiracy to the authorities, and Ralph now faces prison and financial ruin.

Smike, fallen into hopelessless because Kate is in love with Frank, succumbs to his various ailments and dies just before Ralph arrives at Smikes deathbed. The police come to Ralphs house to arrest him. Ralph flees to his garret and hangs himself. True love prevails, and Nicholas and Madeline and Kate and Frank are wed.

==Main cast==
*Sir Cedric Hardwicke as Ralph Nickleby
*Stanley Holloway as Vincent Crummles
*Derek Bond as Nicholas Nickleby
*Mary Merrall as Mrs Nickleby
*Sally Ann Howes as Kate Nickleby
*Aubrey Woods as Smike
*Jill Balcon as Madeline Bray
*Bernard Miles as Newman Noggs
*Alfred Drayton as Wackford Squeers
*Sybil Thorndyke as Mrs Squeers
*Vera Pearce as Mrs. Crummles James Hayter as Ned and Charles Cheeryble Emrys Jones as Frank Cheeryble 
*Cecil Ramage as Sir Mulberry Hawk 
*Timothy Bateson as Lord Verisopht
*George Relph as Mr Bray
*Frederick Burtwell as Sheriff Murray

==Critical reception== Great Expectations puts it somewhat in the shade, mainly because the former was so much more exciting as to plot and a good bit more satisfying in the nature and performance of its characters. There are just no two ways about it; the story of Nicholas Nickleby — at least, in screen translation — is a good whole cut below that of Great Expectations and its tension is nowhere near as well sustained . . . No doubt, the compression of details and incidents compelled by John Dightons script put Director Cavalcanti on a somewhat unenviable spot, which is evidenced by his failure to get real pace or tension in the films last half. And this overabundance also hampers the rounding of characters . . . Withal, Nicholas Nickleby is amusing as a chromo of Dickensian life. It is only that Great Expectations has led us to expect so much more". 
 Champagne Charlie he collaborated with art director Michel Relph to create an impressively atmospheric Victorian London, but stylish visuals hardly compensate for the flat, cursory rendering of some of Dickens best drawn characters. Only Bernard Miles as Noggs and Cedric Hardwicke as wicked Uncle Ralph are given enough space to establish a proper presence. Meagre and one-dimensional, the film is finally smothered by Ealings cosy sentimentality". 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 