Terminal Island (film)
{{Infobox film
| name           = Terminal Island
| image          =
| caption        =
| director       = Stephanie Rothman
| producer       = Charles S. Swartz
| writer         = Stephanie Rothman Charles S. Swartz Jack Barrett Don Marshall Roger E. Mosley Barbara Leigh
| music          =
| cinematography = Daniel Lacambre
| editing        = Dimension Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States English
| budget         =
| gross          =
}}
Terminal Island, released theatrically in the UK as Knuckle Men, is a 1973 American film directed by Stephanie Rothman. It features an early screen performance by Tom Selleck. Although an exploitation film, it has been treated with much serious discussion by critics and academics over the years.  It is regarded as a cult film. 

==Plot==
A TV news program does a segment on Terminal Island, an off shore island established after the abolition of the death penalty. First degree murderers are shipped off to spend the rest of their days fending for themselves. 

Carmen is dropped off Terminal Island. The first prisoner she meets is a former doctor. The comes to realise there are two main factions on the island. A civil war breaks out.

==Release==
Rothman later said that that she was asked to have a rape scene in the film but could not bring herself to shoot it. "I would not want to be responsible in any way for showing how it could be done," said Rothman. 
 
The film was originally more violent but scenes had to be cut out. Rothman was uncomfortable with the violence that she did show. "I was unhappy with the movie and still continue to feel so," she said in 1981. Tony Williams, Feminism, Fantasy and Violence: An Interview with Stephanie Rothman, Journal of Popular Film & Television 9. 2 (Summer 1981): 84. 

Film critic Roger Ebert rates Terminal Island with one star out of four, dismissing it as "the kind of movie that can almost be reviewed by watching the trailer" 

==References==
 

==External links==
* 
*  at DVD Drive In

 

 
 
 

 