Zatoichi Challenged
{{Infobox film
| name = Zatoichi Challenged
| image =
| film name      =  {{Infobox name module
| kanji          = 座頭市血煙り街道
| romaji         = Zatōichi chikemurikaidō
}}
| director = Kenji Misumi
| producer = Ikuo Kubodera
| writer = Ryozo Kasahara
| based on       =  
| starring = Shintaro Katsu Jushiro Konoe Miwa Takada Yukiji Asaoka
| music = Akira Ifukube
| cinematography = Chikashi Makiura
| editing        = Toshio Taniguchi
| studio         = Daiei Studios
| distributor    =
| released =  
| runtime = 87 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Zatoichi Challenged is the seventeenth episode in the 26-part film series devoted to the character of Zatoichi.

==Plot==
 
Zatoichi (Katsu) checks into an inn where he shares a room with an ill woman and her young son named Ryota. Before the woman dies, she requests that Zatoichi take her son to his father, an artist living in the nearby town of Maebara.

==Cast==
*Shintaro Katsu as Zatoichi
*Jushiro Konoe as Akazuka
*Miwa Takada as Omitsu
*Yukiji Asaoka as Tomoe
*Mikiko Tsubouchi as Osen
*Mie Nakao as Miyuki
*Takao Ito as Shokichi
*Midori Isomura as Omine
*Eitaro Ozawa as Torikoshi
*Asao Koike as Boss Gonzo 

==Reception==

===Critical response===
J. Doyle Wallis, in a review for DVD Talk, wrote that " hile it had the great Kenji Misumi, one of samurai cinemas greats and a personal favorite director of mine, behind the camera, not every film in such a long film cycle can be perfect. Unfortunately this is one of the weaker films. Misumis direction is still quite good and his signature perfect framing is as fantastic as it ever was, particularly in the great finale which features one of Ichis longest duels. Katsu is also, as he always was, great. The man could act with any part of his body (insert dirty joke here) and he displays some of the finest ear and foot acting youre likely to see. But, while entertaining enough for Katsu and Misums inherent skill, the film suffers form a slapdash script and that damn annoying kid factor. The series one major fault was its lack of development and reliance on formula. While usually that formula is a winner, here it just feels a tad tired."   

== Adaptation==
The 1989 American samurai-action film Blind Fury (starring Rutger Hauer) is a loosely based, modernized version of Zatoichi Challenged.

==References==
 

==External links==
* 
* 
* 
* , review by D. Trull for Lard Biscuit Enterprises
* , review by Steve Kopian for Unseen Films (19 February 2014)
* , by Thomas Raven for freakengine (March 2012)
* , by Mark Pollard for Kung Fu Cinema
* , review by Andrew Pragasam for The Spinning Image 
* , review by Paghat the Ratgirl for Wild Realm Reviews

 

 
 
 
 
 
 
 
 
 
 


 