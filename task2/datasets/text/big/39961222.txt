Camp X-Ray (film)
{{Infobox film
| name           = Camp X-Ray
| image          = Camp X-Ray - Movie Poster.jpg
| alt            = 
| caption        = Theatrical film poster
| director       = Peter Sattler
| producer       = {{Plain list | 
* Ellen Goldsmith-Vein
* David Gordon Green 
* Gina Kwon
* Lindsay Williams
* Sophia Lin
* Emmy Ellison
* Cassandra B. Laymon
}}
| writer         = Peter Sattler
| starring       = {{Plain list | 
* Kristen Stewart
* Peyman Moaadi
* John Carroll Lynch
* Lane Garrison
* Joseph Julian Soria
* Cory Michael Smith
}}
| music          = Jess Stroup
| cinematography = James Laxton
| editing        = Geraud Brisson
| studio         = GNK Productions   Gotham Group   Rough House Pictures   The Young Gang
| distributor    = IFC Films 
| released       =  
| runtime        = 117 minutes   
| country        = United States
| language       = English
| budget         = $1 million   
| gross          = $60,581 
}} Camp X-Ray at the Guantanamo Bay detention camp. The film is the directorial debut of Peter Sattler who also wrote the screenplay. It stars Kristen Stewart and Peyman Moaadi with John Carroll Lynch, Lane Garrison, and Joseph Julian Soria in supporting roles. The film premiered on January 17, 2014 at 2014 Sundance Film Festival in the U.S. dramatic competition category  and released on October 17, 2014 by IFC Films.

== Plot ==
Army private first class Amy Cole (Kristen Stewart) is placed as a guard at Guantanamo Bay detention camp, specifically Camp Delta. Her convictions become less certain after she strikes up a tenuous friendship with one of the detainees.

== Cast ==
* Kristen Stewart as PFC Amy Cole 
* Peyman Moaadi as Ali Amir (credited as Payman Maadi)  
* Julia Duffy as Betty Cole
* John Carroll Lynch as COL James Drummond 
* Lane Garrison as CPL "Randy" Ransdell
* Joseph Julian Soria as PFC Rico Cruz
* Tara Holt as PFC Mary Winters
* SerDarius Blain as PFC Raymond Jackson
* Cory Michael Smith as PFC Bergen
* Mark Naji as Detainee #1
* Anoop Simon as Detainee #2
* Robert Tarpinian as Detainee #3
* Yousuf Azami as Ehan
* Marco Khan as Mahmoud
* Kyle Bornheimer as Night Shift C.O.
* Nawal Bengholam as Newscaster
* LaDell Preston as IRF #1
* Daniel Leavitt as IRF #2

== Production ==
On February 6, 2014, IFC Films announced their acquisition of the North American rights to the film.  Shooting Stars LLC acquired the rights to distribute the film in the United Arab Emirates. EDGE Entertainment will distribute Camp X-Ray in Denmark, Finland, Norway, and Sweden.  The film will also be distributed in Lebanon and Iraq with an October 30, 2014 release date.

=== Filming ===
Production for Camp X-Ray took place in Los Angeles and Whittier, California.  Principal photography began on July 17, 2013 and ended in mid-August.   The location used for filming the prison scenes was the abandoned Fred C. Nelles Youth Correctional Facility in Whittier, California. 

== Promotion and marketing ==
The film moved to post-production in late summer 2013. The special effects were edited by Comen VFX company.  On December 5, 2013, it was announced that the film would premiere on January 17 at the Sundance Film Festival in the U.S Dramatic Competition category.  On July 3, 2014, ten new stills from the film were released.  IFC Films released the official trailer on August 8, 2014 on its YouTube channel. 

Camp X-Ray has been rated "R" by the MPAA for language and brief nude images.  The film will be available in select theaters and through Video on Demand services including iTunes Movies and Amazon.com Video starting October 17, 2014.  The film was also a selection for the Atlantic Film Festival, Deauville American Film Festival, BFI London Film Festival, Abu Dhabi Film Festival, Leiden International Film Festival, Hof International Film Festival, and the Stockholm International Film Festival. 

Camp X-Ray premiered with a special screening on October 6, 2014 in New York City. 

== Soundtrack == The Antlers Aquilo is featured in the trailer released by IFC Films. 

Jess Stroups original score for the film soundtrack will be released through iTunes by Lakeshore Records on October 14, 2014. 

== Reception ==
Camp X-Ray premiered at Sundance Film Festival with generally positive reviews, with specific praise of Stewart and Moaadis performances. {{cite news|url= http://www.thedailybeast.com/articles/2014/01/18/camp-x-ray-a-kristen-stewart-starring-guantanamo-bay-film-premieres-at-sundance.html| title= Camp X-Ray, A Kristen Stewart-Starring Guantanamo Bay Film, Premieres at Sundance| publisher   = Daily Beast|author= Marlow Stern| date = 2014-01-18|isbn=| accessdate  = 2014-01-19| archivedate = 2014-01-18 archiveurl  = https://web.archive.org/articles/2014/01/18/camp-x-ray-a-kristen-stewart-starring-guantanamo-bay-film-premieres-at-sundance.html|deadurl= No | quote= But, by the end of Camp X-Ray, you’re won over by Stewart’s layered turn as Cole, and Maadi’s as the defiant Ali. It’s a role perfectly suited to her strengths—vulnerability and hidden courage—and few young actresses, with the exception of Jennifer Lawrence, can hold a close-up like Stewart.}}  {{cite news|url=http://www.ottawacitizen.com/entertainment/movie-guide/Sundance+premiere+Camp+XRay+uncovers+prison+life+Guantanamo/9404001/story.html| title=Sundance premiere Camp X-Ray uncovers prison life at Guantanamo Bay|newspaper= Ottawa Citizen| author=Jessica Herndon| date= 2014-01-18| accessdate  = 2014-01-19| archivedate = 2014-01-19| archiveurl  = https://web.archive.org/web/20140119154641/http://www.ottawacitizen.com/entertainment/movie-guide/Sundance+premiere+Camp+XRay+uncovers+prison+life+Guantanamo/9404001/story.html| deadurl= No 
| quote       = Sattler originally intended Stewarts role for a male, but he shifted to a female lead because he felt it created more conflict between (Amy and Ali. "And Muslims extremist relationship toward women also complicated (the story)," he said. "So I clicked into that."}}  The review aggregator website Rotten Tomatoes indicates that 72% of 54 film critics gave the film a positive review, with an average rating 6.3 out of 10. The critics consensus states: "Camp X-Rays treatment of its subject verges on the shallow, but benefits greatly from a pair of impressive performances from Kristen Stewart and Peyman Moaadi." 
 Variety said that "Camp X-Ray is most commendable for believably depicting the U.S. military from a females point of view" and that "The two leads (Stewart and Maadi) are excellent and play off each other deftly."  Scott Mendelson of Forbes wrote, "Kristen Stewart is engaging and Peyman Moaadi avoids the “noble savage” cliché with ease.  The performances are stronger than the film which contains them, but since the picture is mostly a two-hander that’s not entirely a fatal flaw."  Matt Zoller Seitz of RogerEbert.com gave the film three stars and praised Peter Sattlers direction, as well as Stewart and Maadi. Seitz wrote of Stewart that: "There are silent film-quality close-ups where you can read every fluctuation in her mood even though she’s barely moving a muscle. This is a true movie star performance." Seitz remarked that, "The relationship between Amy, a strong-silent type, and Ali, a chatterbox provocateur, has a ‘70s-movie feel." 
 Into the Wild and Adventureland (film)|Adventureland", but criticized the film in general, saying "the supporting players are little more than equal opportunity stereotypes (frothing Islamists; brutish grunts), while the dialogue is a clatter of cookie-cutter exposition, intent on telling us everything but explaining very little".  Eric Kohn of Indiewire criticized the screenplay and direction by saying that "Sattlers frustratingly on-the-nose screenplay" and "Its a powerful assertion about the prospects of being trapped by misguided intentions, which sadly applies to Camp X-Ray itself" but ultimately praised Stewarts performance.  Owen Gleiberman of Entertainment Weekly gave the film a negative review by saying that "it’s also a flatly made movie" and said that Stewart was miscast in the role as "she has no toughness, no moxie, no callouses on her hide." 

==Nominations==
Writer/Director Peter Sattler earned a nomination for a Humanitas Prize in the Sundance category.  Casting director Richard Hicks earned an Artios Awards nomination for Outstanding Achievement in Casting - Low Budget Feature - Drama.  The Women Film Critics Circle also nominated Kristen Stewart for Best Actress and the film for "Best Movie about Women." 

==Box office==
The film opened on October 17, showing in one theater in New York City. The film grossed $1,316. The film expanded to three screens in its second week and posted an increase  of 134% of $3,480. As of November 9, the film has grossed $9,837  The film also debuted on video on demand and rose to #12 in overall releases on iTunes. "Camp X-Ray" grossed $50,744 in the United Arab Emirates. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 