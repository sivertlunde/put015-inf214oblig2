Cold Light
{{Infobox film
| name           = Cold Light 
| director       = Hilmar Oddsson
| producer       = Mike Downey   Sam Taylor   Kate Barry   Helga Bähr   Jóna Finnsdóttir   Friðrik Þór Friðriksson   Anna María Karlsdóttir   Hilmar Oddsson   Zorana Piggott   Egil Ødegård 
| writer         = Vigdís Grímsdóttir   Hilmar Oddsson   Freyr Thor   Freyr Þormóðsson
| starring       = 
| distributor    = Film and Music Entertainment   Icelandic Film   Filmhuset AS   Lichtblick Film-und Fernsehproduktion (I)   Invicta Capital   íslenska Kvikmyndasamsteypan 
| image          = 
}}
 2004 Cinema Icelandic film directed by Hilmar Oddsson.

==Plot==
   

==Cast==
* Ingvar Eggert Sigurðsson as Older Grímur
* Unnur Ösp Stefánsdóttir as Woman in reception
* Ruth Olafsdottir as Linda
* Steinunn Harðardóttir as Svava
* Borghildur Thors as Woman in a window
* Áslákur Ingvarsson as Young Grímur
* Katla M. Þorgeirsdóttir as Lára
* Helga Braga Jónsdóttir as Guðbjörg
* Snæfríður Ingvarsdóttir as Gottína
* Kristbjörg Kjeld as Álfrún
* Thórey Sigthórsdóttir as Thóra, Grimurs mother
* Sara Dögg Ásgeirsdóttir as Anna
* Marta Nordal as Woman at the exhibition
* Elís Philip William Scobie as Young Tumi
* Björn Hlynur Haraldsson as Indriði
* Valdimar Örn Flygenring as Pabbi Grims (Hermundur)
* Christine Carr as Thorbjörg
* Jóhann Sigurðarson as Stranger
* Vilborg Halldórsdóttir as Woman on a pier
* Hilmar Jonsson as Older Tumi (as Hilmar Jónsson)
* Edda Heidrún Backman as Birna, Tumis Mother
* Hilmar Oddsson as Tumis Father
* Bjarney Ólafsdóttir as Myndlistanemandi
* Bjarni Massi as Myndlistanemandi
* Björn Halldór Helgason as Myndlistanemandi
* Elín Guðmundsdóttir as Myndlistanemandi
* Guðmundur Thoroddsen as Myndlistanemandi
* Gunnar Már Pétursson as Myndlistanemandi
* Hörn Harðardóttir as Myndlistanemandi
* Íris Eggertsdóttir as Myndlistanemandi
* Jóhanna Helga Þorkelsdóttir as Myndlistanemandi
* Ólafur Þór Jóhannesson as Myndlistanemandi
* Rebekka Ragnarsdóttir as Myndlistanemandi
* Bjarni Friðrik Jóhannsson as Himself (Singapore Sling)
* Einar Þór Kristjánsson as Himself (Singapore Sling)
* Helgi Örn Pétursson as Himself (Singapore Sling)
* Henrik Baldvin Björnsson as Himself
* Sigurður Magnús Finnsson as Himself (Singapore Sling)
* Þorgeir Guðmundsson as Himself

==Released==
Cold Light  was released on 26 September 2005.

==Awards==
It was Icelands submission to the 77th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

Edda Awards in Iceland 

In 2004 Cold Light won the Edda Awards in Iceland for ‘Best Film’, ‘Actor or Actress of the Year’ (Ingvar Eggert Sigurðsson), ‘Supporting Actor or Supporting Actress of the Year’ (Kristbjörg Kjeld), ‘Director of the Year’ (Hilmar Oddsson) and ‘Professional Category: Sound/Vision’ (Sigurður Sverrir Pálsson, for the cinematographer).  They were nominated at the Edda Awards in Iceland for ‘Actor or Actress of the Year’ (Áslákur Ingvarsson), ‘Supporting Actor or Supporting Actress of the Year’ (Helga Braga Jónsdóttir) and ‘Supporting Actor or Supporting Actress of the Year’ (Snæfríður Ingvarsdóttir). 

European Films Awards

In 2004 at the European Films Awards, Cold Light was nominated for the Audience Award for the ‘Best Director’ for Hilmar Oddisson. It was nominated for the Audience Award for ‘Best Acrtor’ (Ingvar Eggert Sigurðsson). 

Festróia International Film Festival

Festróia International Film Festival in 2004, Cold Light won a Silver Dolphin for ‘Best Actor’ (Ingvar Eggert Sigurðsson). It also won the ‘Prize of the City of Setubal (Hilmar Oddsson) and it was nominated for the Golden Dolphin award for (Hilmar Oddsson).

Mar del Plata Film Festival

In 2004, Cold Light won the ‘SIGNIS Award’ for (Hilmar Oddsson). It was also nominated for ‘Best Film’ (Hilmar Oddsson). 

Verona Love Screens Film Festival

In 2005, at the Verona Love Screens Film Festival, Cold Light won the ‘Audience Award’ for (Hilmar Oddsson). They won ‘Best Artistic Contribution’ for Best Cinematography (Sigurður Sverrir Pálsson) and they won ‘Best Film’ (Hilmar Oddsson). 



==References==

 

==See also==
 
*Cinema of Iceland
*List of submissions to the 77th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 


 
 