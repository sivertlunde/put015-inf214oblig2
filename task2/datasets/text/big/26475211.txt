Novaya Zemlya (film)
 
{{Infobox film
| name = Novaya Zemlya
| director = Aleksandr Melnik
| writer = Arif Aliyev
| starring = Konstantin Lavronenko  Andrei Feskov  Marat Basharov  Pavel Sborshchikov  Sergei Zhigunov  Aleksandr Samoylenko   Tommy Tiny Lister  Ingeborga Dapkunaite
| studio = Andreevsky Flag Film Company
| distributor= Nashe Kino
| released =  
| runtime = 115 minutes
| country = Russia
| language = Russian   English
}}
Novaya Zemlya ( ) is an action film, directed by Aleksandr Melnik and released in 2008. The film stars Konstantin Lavronenko, Andrei Feskov, Marat Basharov, Pavel Sborshchikov, Sergei Zhigunov, Aleksandr Samoylenko, Tommy Tiny Lister and Ingeborga Dapkunaite. It was filmed in Russia, Ukraine, Malta and Norway.

==Plot==
In 2013, capital punishment has been abolished.  With prison spaces running out, a group of prisoners are taken to an island called Novaya Zemlya for the sake of an experiment.

==Cast==
* Konstantin Lavronenko as Zhilin
* Andrei Feskov as Sipa
* Marat Basharov as Tolya
* Pavel Sborshchikov as Obezyan
* Sergei Zhigunov as Polkovnik
* Aleksandr Samoylenko as Ali
* Tommy Tiny Lister as Sewing Dude
* Ingeborga Dapkunaite as Marta
* Sergei Koltakov as Makhov
* Evgeny Titov as Moryak
* Vladislav Abashin as Arzhanov
* Zaza Chichinadze as Amurbek
* Viktor Zhalsanov as Yakut
* Nikolai Stotsky as Volynets
* Igor Pismenniy as Olafson

== External links ==
*  

 
 
 
 
 

 