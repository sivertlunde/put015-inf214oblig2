Kapò
 
{{Infobox film
| name           = Kapò
| image          = Kapo film.jpg
| caption        = Italian film poster
| director       = Gillo Pontecorvo
| producer       = Franco Cristaldi Moris Ergas
| writer         = Gillo Pontecorvo Franco Solinas
| starring       = Susan Strasberg Didi Perego Laurent Terzieff 
| music          = Carlo Rustichelli
| cinematography = Aleksandar Sekulovic
| editing        = 
| distributor    = Cineriz
| released       =September 29, 1960 (Italy)  1 June 1964 USA
| runtime        = 116 minutes (US) 118 minutes (Spain)
| country        = Italy France Yugoslavia 
| language       = Italian
| budget         = 
| gross          = 
}}

Kapò ( ) is a 1960 Italian film about the Holocaust directed by Gillo Pontecorvo. It was nominated for the Academy Award as Best Foreign Language Film.    It was an Italian-French co-production filmed in Yugoslavia.

==Plot==
Naive fourteen-year-old Edith (Susan Strasberg) and her Jewish parents are sent to a concentration camp, where the latter are killed. Sofia (Didi Perego), an older, political prisoner, and a kindly camp doctor save her from a similar fate by giving her a new, non-Jewish identity, that of the newly dead Nichole Niepas. 

As time goes by, she becomes more hardened to the brutal life. She first sells her body to a German guard in return for food. She becomes fond of another guard, Karl (Gianni Garko). The fraternization helps her become a kapo (concentration camp)|kapo, one of those put in charge of the other prisoners. She thrives while the idealistic Sofia grows steadily weaker. 

When she falls in love with Sascha (Laurent Terzieff), a Russian prisoner of war, Edith is persuaded to play a crucial role in a mass escape, turning off the power. Most of the would-be escapees are killed, but some get away. Edith is not one of them. As she lies dying, she tells Karl, "They screwed us over, Karl, they screwed us both over." She dies saying the traditional prayer Shema Yisrael, to feel again her real identity.

==Cast==
* Susan Strasberg as Edith, alias Nicole Niepas
* Laurent Terzieff as Sascha
* Emmanuelle Riva as Terese
* Didi Perego as Sofia
* Gianni Garko as Karl
* Annabella Besi
* Graziella Galvani
* Paola Pitagora

==Controversy==
From the Wall Street Journal article, "Hollywoods Nazi Revisionism", by Bernard-Henri Lévy: 
 The Italian filmmaker Gillo Pontecorvo earned "the deepest contempt" of French director Jacques Rivette in an article in Cahiers du cinéma nearly 50 years ago for a scarcely more insistent shot in the 1959 film "Kapo." The shot was of the raised hand of actress Emmanuelle Riva, her character Terese electrocuted on the barbed wire of the concentration camp from which she was trying to escape. The criticism hung over Pontecorvo until his dying day. He was ostracized, almost cursed, for a shot, just one.  Shutter Island.

== See also ==
* List of Holocaust films
* List of submissions to the 33rd Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

== References==
 

== External links ==
*  
*   , LoBservatoire site.

 

 
 
 
 
 
 
 
 
 