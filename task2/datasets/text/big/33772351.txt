Eccentricities of a Blonde-haired Girl
{{Infobox film
| name           = Eccentricities of a Blonde-haired Girl
| image          = 
| alt            =  
| caption        = 
| director       = Manoel de Oliveira
| producer       = François dArtemare Maria João Mayer Luis Miñarro
| writer         = Manoel de Oliveira Eça de Queirós (short story)
| starring       = Ricardo Trêpa Catarina Wallenstein
| music          = 
| cinematography = Sabine Lancelin
| editing        = Manoel de Oliveira Catherine Krassovsky
| studio         = 
| distributor    = 
| released       =  
| runtime        = 64 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
| gross          = 
}}
Eccentricities of a Blonde-haired Girl ( ) is a 2009 Portuguese film directed by Manoel de Oliveira. 

==Plot==
 

==Cast==
* Ricardo Trêpa as Macário
* Catarina Wallenstein as Luísa
* Diogo Dória as Francisco
* Júlia Buisel as D. Vilaça
* Leonor Silveira as Senhora
* Luís Miguel Cintra as Himself
* Glória de Matos as D. Sande
* Filipe Vargas as Amigo
* Rogério Samora as Chapéu de Palha

==Reception==
Eccentricities of a Blonde-haired Girl has received generally favorable reviews from critics. Rotten Tomatoes gave the film a rating of 79%, based on 19 reviews. 

===Awards and nominations===
*Golden Globes (Portugal) - Best Actress (Catarina Wallenstein)

==References==
 

==External links==
*  

 

 
 
 
 
 

 
 