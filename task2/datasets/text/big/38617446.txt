Boggy Creek II: And the Legend Continues
{{Infobox film
| name           = Boggy Creek II: And the Legend Continues
| image          = File:Film_Poster_for_Boggy_Creek_II_And_the_Legend_Continues.jpg
| caption        = Theatrical release poster
| director       = Charles B. Pierce
| writer         = Charles B. Pierce
| starring       = Charles B. Pierce Cindy Butler Chuck Pierce, Jr.
| music          = Frank McKelvey Lori McKelvey  
| cinematography = Shirak Kojayan
| editing        = Shirak Kojayan
| studio         = 
| distributor    = Howco International Pictures
| released       = December 1985
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Boggy Creek II: And the Legend Continues is a 1985 film directed by  , following 1977s Return to Boggy Creek. The film was followed by two additional sequels, Boggy Creek: The Legend Is True (2010) and The Legacy of Boggy Creek (2011). The "Big Creature" in the film was portrayed by Hollywood stuntman and bodyguard, James Faubus Griffith. 
 sixth episode of season 10. 

==Plot Synopsis==
Dr. Brant Lockhart (Charles B. Pierce) is a professor of cryptozoology at the University Arkansas. He receives a call from a sheriff, who reports sighting a bigfoot-like creature in a remote town in Southwest Arkansas.  Lockhart recruits the help of two of his students, Tim (Chuck Pierce, Jr.) and Tanya, as well as Tanyas friend Leslie. 

The group heads for an area near Boggy Creek, close to the town of Fouke, and set-up camp in the woods with a pop-up camper trailer, and secure their perimeter with a SONAR system.  Lockhart sets about investigating the recent sighting, while relating to the group tales he had heard centered around the creature, presented in flash backs. 
The tales include:

*A local rancher who, while having lunch, mysteriously lost his herd of cattle, and saw the creature leave the scene.
*A local-man who encountered the creature while repairing a flat-tire on his vehicle. The man was rendered unconscious in the encounter, and never came out of it in order to relate his story to others. (How Lockhart then knows of it is unexplained.)
*A local attorney who was in an outhouse that was attacked by the creature, soiling his pants in the encounter.
*The local sheriff who encountered the creature behind his home following a fishing trip. The creature and its young ran off with the sheriffs catch. This is the story Lockhart has come to investigate.

While talking with locals, Lockhart is met with resistance and disbelief by most. Of those willing to talk with him, hes directed to speak with "Old Man Crenshaw" who lives in a shack along the river bank. Lockhart leases a boat, and takes off to meet with Crenshaw. Crenshaw is a sexagenarian man, fitting the stereotypical notion of a hillbilly or mountain-man, living alone on his property. While somewhat welcoming to Lockhart and his entourage, he seems unwilling to talk too much about the creature, or why he is maintaining a series of bonfires on his land. A severe storm closes in, and makes heading back down the river dangerous, forcing Lockhart and the students to have to stay the night in Crenshaws cabin.

Believing Lockhart to be a medical doctor, Crenshaw enlists his help in tending to an animal he has caught. To Lockharts amazement, it is the adolescent creature. Lockhart determines that the adult creature has been more hostile in the area recently due to the capture its child, who is now near death. Lockhart commandeers Crensahws gun and ammo and returns the adolescent creature to the adult when it attacks the cabin in the night.  With its young in its arms, the creature leaves the cabin without further incident.  The following morning, Crenshaw agrees with Lockharts assessment that the creatures should be left alone. Lockhart decides not to tell others about his experiences while in the Boggy Creek area, and returns down the river with his students.

==References==
 

==External links==
*  

 

 
 
 
 
 

 