Subah Ka Sitara
{{Infobox film
| name           = Subah Ka Sitara
| image          = 
| image_size     = 
| caption        = 
| director       = Premankur Atorthy
| producer       = New Theatres
| writer         = 
| narrator       =  Kumar 
| music          = R. C. Boral
| cinematography = Nitin Bose
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1932
| runtime        = 128 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1932 Urdu/Hindi Zinda Lash. Puran Bhagat that he changed his screen name to "Kumar".    

The story revolved around the son of a nobleman and a slave girl.    
Though the songs of K. L. Saigal and Rattanbai were well appreciated, the movie did not fare well at the box-office.   

==Cast==
*K. L. Saigal
*Rattanbai
*Mazhar Khan
*Ali Mir Kumar, 
*Radhabai
*A. M. Shiraji
*Sheela
*Ansari
*Siddiqui

==Songs==
The music direction was by R. C. Boral.   
===Songlist===
*Ab Dawa Dete Hain Woh Aur Na Dua Dete Hain
*Arzoo Itni Ha Ab mere Dil-e-nashaad Ki
*Iltaza khaalik Se yeh Hai
*Hat Jaiji Na Hamko Sataoji
*Asq Bahe Bahaa Kare
*Khuli Hain Botal Bhare Hain Sagar
*Khayal Kashh Woh Karte Apne Bismil Ka
*Na Saroor Hoon Na Khumar Hoon
*Na Hanste Hain Tere Qaidi


==References==
 

==External Links==
* 

 
 
 
 
 