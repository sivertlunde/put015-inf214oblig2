Shakal Pe Mat Ja
{{Infobox film
| name           = Shakal Pe Mat Ja
| image          = Shakal_Pe_Mat_Ja.jpg
| alt            =  
| caption        = 
| director       = Shubh Mukherjee
| producer       = Ipix Movies Linc Entertainment Saritaa Deepak Jalan Co-Produced by Hrishitaa Bhatt 
| writer         = Shubh Mukherjee
| starring       = Shubh Saurabh Shukhla Raghubir Yadav Aamna Sharif Zakhir Hussain Umang Jain Pratik Katare Chitrak Bandyopadhyay Harsh Parekh Mushtaq Khan Joy Sen Gupta Aditya Lakhia
| music          = Nitin kumar gupta Salim-Sulaiman Amar Mohile  Honey Singh
| cinematography = Sahir Raza
| editing        = Nipun Ashok Gupta
| studio         = 
| distributor    = Reliance Entertainment
| released       =    
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Shakal Pe Mat Ja is a comic drama film, directed by Shubh Mukherjee. The film stars Shubh Mukherjee, Saurabh Shukhla, Raghubir Yadav, Aamna Sharif, Zakhir Hussain, Umang Jain, Pratik, Katare, Chitrak Bandyopadhyay, Harsh Parekh, Mushtaq Khan, Joy Sen Gupta, Aditya Lakhia.  The movie was released in India on November 18, 2011. 

==Story==

Shakal Pe Mat Ja is a story of four young boys who get caught up as suspects at the Delhi international airport, on a high alert day. They get caught while capturing an American Airlines aircraft landing on their camera, which they claim to have shot for a documentary that they are making. 

With their explanation and situation not being fruitful they are brought into the interrogation room of the International airport to be questioned by the CISF. 

With looks that are totally anti-terrorist, these simple guys get into crazy trouble, as their hold up doesn’t turn up the way anyone expected. After all, looks are deceptive. Everything that they say or possess or even do leads them to further obstacles. Whether it’s the suspicious footage of the boys being at Rashtriya pati bhawan, or the Arabic and Pakistani Porn that they find in Rohan’s bag, Whether it’s the bomb like looking device in Dhruv’s bag which he states to be his physics project or the fact that the 27-year-old Bulai who resembles a most wanted terrorist and doesn’t carry any id or even a cell phone. At the same time the Intelligence has reports that a group of terrorists are planning some sort of an attack. Do these 4 guys have any involvement with the terror group or do they have different plans altogether. 

Are looks always deceptive? Could these four innocent looking guys be hard core terrorists involved in one of the biggest terror plans? Shakal Pe Mat Ja… is a film which deals with all such deceptive goof ups, mistaken identities & madness created due to being there at the wrong place at the wrong time. Innocent Looking maniacs, amateur terror groups, and Security forces who are never on time is what Shakal Pe Mat Ja is all about! 

==Cast==
* Shubh as Ankit Sharma
* Pratik Katare as Dhruv Sharma
* Harsh Parekh as Bulai
* Chitrak Bandyopadhyay as Rohan Malhotra
* Raghuvir Yadav as Omprakash
* Saurabh Shukla as ATS Chauhaan
* Umang Jain as Prachi
* Aamna Sharif as Amina
* Zakhir Hussain as Omama (Terrorist)

==Box Office Report==

With a total box office collection of just 6.5 million Indian Rupees,  after the first week, the film flopped at the box office. The film failed to generate positive response from critics also because of its bad screenplay and even worse direction  

==References==
 

==External links==
*  

 
 
 
 