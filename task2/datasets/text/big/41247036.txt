No Man's Land (2013 Chinese film)
{{Infobox film
| name           = No Mans Land
| image          = No Mans Land 2013 poster.jpg
| border         = 
| alt            = 
| caption        = 
| director       = Ning Hao
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 117 minutes
| country        = China
| language       = Mandarin
| budget         = Renminbi|CN¥20 million   
| gross          = Renminbi|CN¥258 million (US$42.6 million) 
}} western thriller Xu Zheng, Yu Nan, Huang Bo and Duo Buji. 

==Plot==
The entire film happens in a very desolate region of China, in the northwestern deserts. A poacher is being arrested by a police officer after capturing a rare falcon, worth 1 million RMB on the black market. A second poacher kills the police officer with his jeep and tells the first poacher to flee, while he stays behind himself to take responsibility. He hires one of the best lawyers in China for his defense, forcing the lawyer to travel a long distance to get to the town. At the trial, the lawyer gets the poacher acquitted by arguing that the crash happened because the police officer was drunk. The lawyer takes the poachers car as collateral and begins a long drive back to Chinas eastern region. While driving, the lawyer is harassed by two people in a truck carrying hay. He throws his lighter at the hay in retaliation, igniting it, and drives ahead.

The second poacher was mad at his car being pawned. He arranges for the first poacher to murder the lawyer on the highway. However, by standing in the middle of the road, pretending that he has car problems in an attempt to trick the lawyer into stopping, the first poacher gets crashed into instead. The lawyer puts him in the backseat, along with his bag, and tries to take him to the nearest rest stop, which is a shanty town, to call for help. He does not realize that that man is the first poacher. When he gets to the shantytown, the first poacher looks like he has died. The lawyer gets scared and decides not to call for help. Instead, he buys gasoline and another lighter. He intends to cover up the crash by cremating the poachers dead body. But it turns out, the first poacher isnt dead, and upon regaining consciousness he holds the lawyer at gunpoint. A girl from the shanty town had stowed away in the trunk, trying to start a new life, and also gets threatened.

The two truck drivers from earlier come upon them, and the poacher kills one and wounds the other, and proceeds to shoot the lawyer non-fatally. He forces the girl to take the car and drive him to a ghost town further on. The falcon, all along, was being held in his bag, and he has scheduled to sell it there to black marketers. The girl crashes the car by accident. She gets rescued by her abusive father and brother, who kill the first poacher. The father tries to take the falcon to the ghost town and sell it for himself, leaving the girl and her brother.

The second poacher, having lost contact with the first, comes upon the lawyer and lets him ride his pickup truck, pretending he will help. He comes upon the girl and her brother and decides to kill the brother, and tries, but fails, to murder the lawyer through carbon monoxide poisoning. He seizes the girl and heads for the ghost town.

The lawyer wakes up and is arrested by a single police officer, but he causes the police car to crash and races to save the girl. The second poacher and the two black marketers murder the girls father at the ghost town, complete their transaction, and try to kill the girl. They are stopped by the lawyer, who approaches and tricks the black marketers into thinking that the second poacher is working with the police. The second poacher is forced to kill both black marketers. He is about to kill the lawyer and the girl, but the surviving truck driver from earlier saves them before getting killed himself. At this point, the lawyer is finally defenseless. He gets dragged by the second poacher into a truck which is holding some barrels, and is forced to watch as he attempts to run over the girl. The second poacher taunts the lawyer, saying that his heroism was in vain. The lawyer chooses at that moment to throw his lighter a second time, igniting the barrels and killing them both.

In the last scene, the girl is in a dance studio for elementary aged children, and she is telling the instructor about the whole sequence of events and how she has been changed. As children come in and the lesson begins, the instructor decides to grant the girls request to work as her assistant, in spite of her not being qualified.

==Cast== Xu Zheng
* Yu Nan
* Huang Bo
* Duo Buji
* Wang Shuangbao
* Ba Duo

==Reception==
Made on a budget of Renminbi|CN¥20 million (US$3.3 million),  the film ended up grossing Renminbi|CN¥258 million (US$42.6 million) at the Chinese box office.  It was in competition for the Golden Bear at the 64th Berlin International Film Festival.   

Elizabeth Kerr of The Hollywood Reporter praised the film as "A bleak and completely engaging Chinese neo-western thriller that works on almost every level." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 