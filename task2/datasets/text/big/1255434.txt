Europa (film)
 
 
{{Infobox film
| name = Europa
| image = Europa (film).jpg
| alt = 
| caption = DVD cover
| director = Lars von Trier
| producer = Peter Aalbæk Jensen Bo Christensen   
| writer = Lars von Trier Niels Vørsel
| starring = Jean-Marc Barr Barbara Sukowa Udo Kier Ernst-Hugo Järegård
| narrator = Max von Sydow
| music = Joachim Holbek
| cinematography = Henning Bendtsen Edward Klosinski Jean-Paul Meurisse
| editing = Hervé Schneid UGC
| Nordisk Film Biografdistribution
| released =  
| runtime = 114 minutes  
| country = Denmark France Germany Poland Sweden Switzerland
| language = English German DKK 28 million
| gross = 
}}
Europa (Zentropa in North America) is a 1991 Danish drama film directed by Lars von Trier. It is von Triers third theatrical feature film and the final film in his Europa trilogy following The Element of Crime (1984) and Epidemic (film)|Epidemic (1987).

The film features an international cast, including the French-American Jean-Marc Barr, Germans Barbara Sukowa and Udo Kier, expatriate American Eddie Constantine, and the Swedes Max von Sydow and Ernst-Hugo Järegård.

Europa was influenced by Franz Kafkas Amerika (novel)|Amerika, and the name of the film was chosen "as an echo" of that novel. 

==Plot== German people Nazi Werwolf|terrorist conspiracy.

==Cast==
 
* Max von Sydow - Narrator
* Jean-Marc Barr - Leopold Kessler
* Barbara Sukowa - Katharina Hartmann
* Udo Kier - Lawrence Hartmann
* Ernst-Hugo Järegård - Uncle Kessler
* Henning Jensen - Siggy
* Erik Mørk - Pater
* Eddie Constantine - Colonel Harris
* Jørgen Reenberg - Max Hartmann
* Benny Poulsen - Steleman
* Erno Müller - Seifert
* Dietrich Kuhlbrodt - Inspector
* Michael Phillip Simpson - Robins
* Holger Perfort - Mr. Ravenstein
* Anne Werner Thomsen - Mrs. Ravenstein
* Lars von Trier - Jew
* Baard Owe - Man With Papers
 

==Style==
  experimental style surreal effect. The voice-over narration uses an unconventional second-person narrative imitative of a hypnotist (e.g. "On the count of ten, you will be in Europa.").
 ironically imitative of film noir conventions.

==Production==
The film was shot throughout Poland (Chojna Cathedral (Marienkirche) and the Chojna Roundhouse) and in Denmark (Nordisk Film studios, Copenhagen and the Copenhagen Dansk Hydraulisk Institut)
 Zentropa Entertainments, is named after the sinister railway network featured in this film, which is in turn named after the real-life train company Mitropa.

==Release==
Europa was released as Zentropa in North America to avoid confusion with Europa Europa (1990).

==Reception==
Europa received largely positive reviews, currently holding an 85% rating on review aggregator website Rotten Tomatoes. 

===Accolades=== Jury Prize, the finger and stormed out of the venue. 

==Home media==
The Criterion Collection released the film on DVD in 2008. The package contained several documentaries on the film and an audio commentary from von Trier.

 
==References==
 

==External links==
*  
*  
 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 