The Legend of Bagger Vance
{{Infobox film
| name           = The Legend of Bagger Vance
| image          = Legend of bagger vance ver2.jpg
| caption        = Theatrical release poster
| alt            =
| writer         = Steven Pressfield (novel)  Jeremy Leven (screenplay)|
| starring       = Will Smith  Matt Damon  Charlize Theron|
| director       = Robert Redford
| producer       = Michael Nozik  Jake Eberts  Robert Redford
| music          = Rachel Portman
| cinematography = Michael Ballhaus
| editing        = Hank Corwin
| studio         = Allied Filmmakers
| distributor    = DreamWorks Pictures (USA)  20th Century Fox (non-USA)
| released       =  
| runtime        = 126 minutes
| language       = English
| country        = United States
| budget         = $60 million
| gross          = $39,459,427 
}}
 2000 American American sports Georgia in 1931. This was the last film appearance of both Jack Lemmon and Lane Smith; Lemmon died in 2001 and Smith in 2005.

On release, the film was attacked by several African American commentators and reviewers for using the "magical negro" as a plot device.          Since the films release, some in the mainstream media have also described the film as flawed and racially insensitive.   

== Plot ==
Promising golfer Rannulph Junuh (Matt Damon) is the favorite son of Savannah, Georgia and noteworthy golf player; Adele Invergordon (Charlize Theron) was his beautiful girlfriend before he went off to war and is from a rich family. While serving as a captain in the US Army during World War I, Junuh is traumatized when his entire company is wiped out in battle. Though he earns the Medal of Honor, he returns to Georgia and lives a shadowy life as a drunk, golf being just a distant memory.
 Bobby Jones (Joel Gretsch) and Walter Hagen (Bruce McGill), the best golfers of the era, with a grand prize of $10,000, at a golf resort her father built as the Depression struck. However, she needs a local participant to generate local interest, so she asks her estranged love Junuh to play.

Junuh is approached by a mysterious traveler carrying a suitcase, who appears while Junuh is trying to hit golf balls into the dark void of night. The man identifies himself as Bagger Vance (Will Smith) and says he will be Junuhs caddy. He then helps Junuh to come to grips with his personal demons and helps him to play golf again.

When the match starts, Jones and Hagen each play well in their distinctive ways, but Junuh plays poorly and is far behind after the first round. With Bagger caddying for him and giving advice, Junuh rediscovers his "authentic swing" in the second round and makes up some ground. In the third round, closes the gap even more. Junuh and Adele also find their romance rekindling.

Late in the final round, Junuh disregards Baggers advice at a crucial point and after that plays poorly. He hits a ball into a forest, where he has a traumatic World War I flashback, but Baggers words help him to focus on golf. Junuh pulls back to a tie with Jones and Hagen, then has a chance to win on the final hole, but calls a penalty on himself when his ball moves after he tries to remove an obstacle.

Seeing from this that Junuh has grown and matured, Bagger decides his golfer doesnt need him any more. Bagger leaves him as mysteriously as he met him, with the 18th hole unfinished. Though losing a chance to win because of the penalty, Junuh sinks an improbable putt and the match ends in a gentlemanly three-way tie. The three golfers shake hands with all of Savannah cheering, and Junuh and Adele get back together.

During the match, Bagger Vance has a young assistant, Hardy Greaves (J. Michael Moncrief), who caddies after Bagger leaves. The beginning of the film features Hardy as an old man (Jack Lemmon) playing golf in the present day.  Hardy experiences a heart attack and loses consciousness.  The story ends with an old Hardy awakening and seeing a never-aging Bagger Vance on the golf course. As Bagger beckons, Hardy follows and the film ends.

===Similarities to the Hindu epic Mahabharata===
The plot is roughly based on the Hindu sacred text the Bhagavad Gita, where the Warrior/Hero Arjuna (R. Junuh) refuses to fight.  The god Krishna appears as Bhagavan (Bagger Vance) to help him to follow his path as the warrior and hero that he was meant to be. This relationship was fully explained by Steven J. Rosen in his book Gita on the Green, for which Steven Pressfield wrote the foreword. 

==Cast==
* Will Smith as Bagger Vance
* Matt Damon as Rannulph Junuh
* Charlize Theron as Adele Invergordon
* Bruce McGill as Walter Hagen Bobby Jones
* J. Michael Moncrief as Hardy Greaves
* Lane Smith as Grantland Rice
* Jack Lemmon (uncredited) as old Hardy Greaves

== Production    ==
Small portions of the exhibition match were set at the Kiawah Island Golf Resort in South Carolina, United States, considered one of the toughest in the country. The final hole in the film was temporary, so the filming did not interfere with the club activities, and cost US$200,000 to build.  However, most of the golf scenes were filmed at Colleton River Plantation, just off Hilton Head Island.  Certain segments of this film were also filmed in Savannah, Georgia, USA and Jekyll Island, Georgia, USA 

==Reception ==

=== Critical response    ===
Reaction from movie critics was mixed. 
Rotten Tomatoes gives the film a score of 43% based on 129 reviews. {{cite web 
| title = Bagger Vance
| url = http://www.rottentomatoes.com/m/legend_of_bagger_vance/
| work = Rotten Tomatoes
| publisher = Flixster 
}}
 

Film critic Roger Ebert, who gave it 3½ stars, said,  "It handles a sports movie the way Billie Holiday handled a trashy song, by finding the love and pain beneath the story. Redford and his writer, Jeremy Leven, starting from a novel by Steven Pressfield, are very clear in their minds about what they want to do. They want to explain why it is possible to devote your life to the love of golf, and they want to hint that golf and life may have a lot in common". {{cite web
| date = November 3, 2000 
| author = Roger Ebert
| url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20001103/REVIEWS/11030302/1023 
| title = Roger Ebert: The Legend Of Bagger Vance 
| publisher = Chicago Sun Times
}}
 
The BBCs George Perry called it a  "sumptuously photographed film" but added that "in spite of being lovely to look at, it is pretentious piffle, although Will Smith shows skill and subtlety in his ludicrous role". {{cite web
| author = George Perry 
| url= http://www.bbc.co.uk/films/2001/02/14/the_legend_of_bagger_vance_2001_review.shtml
| title= The Legend of Bagger Vance (2001)
| publisher= BBC
| date= February 22, 2001
}} 
 
Time Magazine|Time called it one of the most "embarrassing" films of recent years for its treatment of African Americans and the use of a "Magical African-American Friend". 

=== Box office ===
The Legend of Bagger Vance opened at #3 at the U.S. box office, grossing $11,516,712 from 2,061 theaters. {{cite web 
| url = http://www.boxofficemojo.com/movies/?id=legendofbaggervance.htm 
| title = The Legend of Bagger Vance 
| work = Box Office Mojo 
| publisher = Amazon.com 
}}
 
According to the Internet Movie Data Base website (imdb.com), the films total gross came to $30,695,227, far short of its estimated $60 million budget.    

==Soundtrack   ==
The now out-of-print soundtrack to The Legend of Bagger Vance was released on November 7, 2000. It was mostly written by Rachel Portman, except for tracks one ("My Best Wishes"), thirteen ("Bluin’ the Blues") and fourteen ("Mood Indigo"), which were written by Fats Waller, Muggsy Spanier and Duke Ellington, respectively.

; Track list
 
# My Best Wishes (2:27)
# The Legend of Bagger Vance (2:11)
# Savannah Needs a Hero (4:53)
# Bagger Offers to Caddy for Junuh (4:07)
# Bagger & Hardy Measure the Course at Night (2:32)
# The Day of the Match Dawns (3:07)
# Birdie (1:46)
# Junuh Sees the Field (5:11)
# Hole in One (2:30)
# Junuh Comes Out of the Woods (3:55)
# Bagger Leaves (3:12)
# Old Hardy Joins Bagger by the Sea (5:50)
# Bluin’ the Blues (2:27)
# Mood Indigo (3:07)
* Total soundtrack time: 47:15

==Pop culture==
Referenced in the Gilmore Girls season 3 episode, "Haunted Leg."

The film was referenced as a meta-joke in Jay and Silent Bob Strike Back, when Ben Affleck and Matt Damon are shooting a movie-within-a-movie in reference to Good Will Hunting, and Affleck says to Damon (Refrencing The Talented Mr. Ripley (film) as well) "Im sorry I dragged you away from what ever gay serial killers who ride horses and -like to play golf- touchy feely picture you were gonna do this week."

== See also ==
 

==References==
 

==External links==
 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 