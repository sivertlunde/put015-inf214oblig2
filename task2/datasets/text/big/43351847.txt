Adukkan Entheluppam
{{Infobox film
| name           = Adukkan Entheluppam
| image          = Adukkanentheluppamfilm.png
| image_size     =
| caption        = Poster designed by P. N. Menon (director)|P. N. Menon
| director       = Jeassy
| producer       =
| writer         =
| screenplay     = Karthika Shankar Shankar Jose Prakash
| music          = Jerry Amaldev
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}} 1986 Cinema Indian Malayalam Malayalam film, Shankar and Jose Prakash in lead roles. The film had musical score by Jerry Amaldev.     

==Cast== Karthika and director Jeassy with Adukkan Entheluppam team at the shooting location]]
*Mammootty as Srinivasan Nair Shankar as Satheeshan Karthika as Vimala Ragini
*Jose Prakash as Menon
*Sukumari as Bharathi
*M. G. Soman as Williams
*Mala Aravindan as Markose
*Adoor Bhasi as San Diego
*Bahadoor as Peter
*Valsala Menon as Dr. Jameela
*Lalu Alex as Nandakumar Vincent
*Lizzy Lizzy

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Bichu Thirumala.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Onnum onnum randu || K. J. Yesudas || Bichu Thirumala ||
|-
| 2 || Raavinte tholil || K. J. Yesudas, KS Chithra || Bichu Thirumala ||
|-
| 3 || Vasantham thalirthu || K. J. Yesudas, Lathika, Sunanda || Bichu Thirumala ||
|} 

==References==
 

==External links==
*  

 

 
 
 
 


 