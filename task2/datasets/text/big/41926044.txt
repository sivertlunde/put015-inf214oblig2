Maya (1999 film)
{{Infobox film
| name           = Maya
| image          = 
| image_size     =
| caption        = 
| director       = Rama Narayanan
| producer       = Rama Narayanan 
| writer         = 
| screenplay     =  Napolean Nagma Vadivelu
| music          = P. R. G
| cinematography = W R Chandran
| editing        =
| distributor    = 
| studio         = Sri Thenandal Films
| released       = 15 January 1999
| runtime        = 2:26
| country        = India
| language       = Tamil Telugu Kannada
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1999 a Napolean alongside Nagma, while S. P. Balasubrahmanyam plays a supporting role. The venture was simultaneously shot in Tamil, Telugu and Kannada, with the other versions being titled as Gurupoornima and Jayasurya respectively, with scenes overlapping.  The films, which had music composed by P. R. G, opened in January 1999.

==Cast== Napolean
*Nagma
*S. P. Balasubrahmanyam
*Vadivelu/Tennis Krishna Rami Reddy
*Vadivukkarasi
*Girish Karnad Sheela as Jayasurya

==Soundtrack==
The soundtrack was composed by P. R. G.

==Release==
The Tamil and Kannada versions were released in 1999, with the Telugu version released shortly after. 

==References==
 

 

 
 
 
 

 
 
 