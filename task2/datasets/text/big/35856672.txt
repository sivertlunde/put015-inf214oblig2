Tally Brown, New York
{{Infobox film
| name           = Tally Brown, New York
| image          = Tally Brown, New York film poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Rosa von Praunheim
| producer       = Rosa von Praunheim   Joachim von Mengershausen	
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Divine   Taylor Mead   Edward Caton
| music          = Tally Brown   Holly Woodlawn
| cinematography = Edvard Lieber	  	Michael Oblowitz  Rosa von Praunheim	  	Juliana Wang	  	Lloyd Williams
| editing        = Mike Shephard  Rosa von Praunheim,	 	
Rosa von Praunheim	
| studio         = 
| distributor    = 
| released       = 4 May 1979
| runtime        = 97 minutes
| country        = U.S
| language       = English
| budget         = 
| gross          = 
}}

 Tally Brown, New York  is a 1979 documentary film directed, written and produced by Rosa von Praunheim. The film is about the singing and acting career of Tally Brown, a classically trained opera and blues singer who was a star of underground films in New York City and a denizen of its underworld in the late 1960s. 

In this documentary, Praunheim relies on extensive interviews with Brown, as she recounts her collaboration with Andy Warhol, Taylor Mead and others, as well as her friendships with Holly Woodlawn, and Divine. Brown opens the film with a cover of David Bowies ""Heroes" (David Bowie song)|Heroes" and concludes with "Rock n Roll Suicide." The film captures not only Tally Brown’s career  but also a particular New York milieu in the 1970s. {{cite news
 | last = Anderson
 | first = Melissa
 | title = The Films of Rosa von Praunheim at Anthology
 | work =The Village Voice
 | date = June 3, 2009
 | url = http://www.villagevoice.com/2009-06-03/film/the-films-of-rosa-von-praunheim-at-anthology/ 
 | accessdate = 2014-05-05}} 

In the same year of its release, the documentary won the Film Award in Silver at the German Film Awards for Outstanding Non-Feature Film.      

The documentary is also noteworthy for being the first of Von Praunheims many portraits of women, usually aging legendary performers, who have become cult figures among the LGBT community.

==External links==
* 

==Notes==
 

== References ==
*Murray, Raymond. Images in the Dark: An Encyclopedia of Gay and Lesbian Film and Video Guide. TLA Publications, 1994, ISBN 1880707012

 
 
 
 
 
 
 
 
 
 
 

 