The Night of the Sunflowers
 Portuguese drama 3 Goya Award nominations.

==Cast==
*Carmelo Gómez	 ...	Esteban
*Judith Diakhate	 ...	Gabi
*Celso Bugallo	 ...	Amadeo
*Manuel Morón	 ...	Vendedor
*Mariano Alameda	 ...	Pedro
* Vicente Romero	 ...	Tomás
*Walter Vidarte	 ...	Amós
*Cesáreo Estébanez	 ...	Cecilio
*Fernando Sánchez-Cabezudo	 ...	Beni
*Petra Martínez	 ...	Marta
*Nuria Mencía	 ...	Raquel
* Enrique Martínez	 ...	Julián

==Awards and nominations==
===Won===
Cinema Writers Circle Awards
*Best Actor (Carmelo Gómez)
*Best New Artist (Jorge Sánchez-Cabezudo-director)

===Nominated=== Goya Awards
*Best New Actor (Walter Vidarte)
*Best New Director (Jorge Sánchez-Cabezudo)
*Best Screenplay &ndash; Original (Jorge Sánchez-Cabezudo)

 
 
 
 
 