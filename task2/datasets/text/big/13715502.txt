Entrails of a Virgin
{{Infobox film
| name           = Entrails of a Virgin
| image          = Entrails of a Virgin.jpg
| image_size     = 180px
| caption        = Theatrical release poster
| director       = Kazuo Komizu
| producer       = Hiroshi Hanzawa Toshio Satō
| writer         = 
| narrator       = 
| starring       = Saiko Kizuki Naomi Hagio
| music          = Hideki Furusawa
| cinematography = Akihiro Itō
| editing        = Kan Suzuki
| distributor    = Nikkatsu/Rokugatsu Gekijō Synapse Films (NTSC) Japan Shock(PAL)
| released       = May 31, 1986
| runtime        = 72 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 1986 horror film in the "splatter film|splatter-eros" subgenre of pink film.

== Synopsis == softcore sex. Eventually they get picked off one-by-one by a filth-covered "demon" with an unnaturally large penis. Most of the sex scenes are fogging (censorship)|fogged. 

== Cast ==
* Saeko Kizuki – Rei
* Naomi Hagio – Kazuyo
* Megumi Kawashima – Kei
* Osamu Tsuruoka – Itomura
* Daiki Katô – Asaoka
* Hideki Takahashi – Tachikawa
* Kazuhiko Goda – A murderer

== Critical reception ==
In his Asia Shock; Horror and Dark Cinema from Japan, Korea, Hong Kong, and Thailand, Patrick Galloway says that Komizus mix of sex and horror works as a film. He describes the film as "essentially one long fuck-fest interspersed with the occasional impaling, hanging or beheading..." and concludes, "Its all rather sordid and theres a definite misanthropy to the film reminiscent of Henri-Georges Clouzot (Les Diaboliques (film)|Diabolique), but somehow the elements click into place, making for an uncomfortable-yet-cant-look-away film experience." 

== Release ==
Along with its sequel, Entrails of a Beautiful Woman, Entrails of a Virgin became a notorious example of the low-quality blackmarket VHS copies which American enthusiasts traded in the 1980s. These copies usually had no English subtitles. The film was released on DVD first on the Japan Shock label in PAL format, and then by Synapse Films in NTSC. The Synapse release was a restored, widescreen version. 

== Production == softcore pornographic productions. The film became a box-office success.   

== Sequel == Kazuo "Gaira" hardcore Entrails of a Beautiful Woman (1986), as well as Female Inquisitor (1987 in film|1987).

== References ==
 

;Bibliography
*  
*  
*  
*  
*  
*  

== External links ==
*  
*  

 
 
 
 
 
 
 
 

 
 