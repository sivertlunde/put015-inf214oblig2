Smiley Gets a Gun
{{Infobox film
| name     = Smiley
| director = Anthony Kimmins
| image	   = Smiley Gets a Gun FilmPoster.jpeg
| writer   = Anthony Kimmins  Rex Rienits 
| based on = novel by Moore Raymond 
| starring = Sybil Thorndike Chips Rafferty
| producer = Anthony Kimmins
| music    = 
| cinematography = Edward Scaife
| editor = G. Turney-Smith
| studio = Canberra Films London Films
| distributor    = Twentieth Century Fox
| released = May 1958 (UK) December 1958 (Australia)
| runtime  = 90 mins
| country = United Kingdom   United States
| language = English}}
Smiley Gets a Gun is a 1958 Australian film that is the sequel to the 1956 film Smiley (1956 film)|Smiley.

==Synopsis==
A young boy named Smiley desperately wants a gun. A deal is made between him and Sergeant Flaxman that if he gets 8 nicks (marks on a certain tree) for his good deeds he will get a £2 rifle. He has several adventures and is accused to stealing some gold. Smiley runs away but the real thief is caught and Smiley is rewarded with a gun.

==Cast==
 
* Keith Calvert as Smiley Greevins
* Alexander ( Bruce) Thomas as Smiley Greevins on horse
* Bruce Archer as Joey
* Sybil Thorndike as Granny McKinley
* Chips Rafferty as Sergeant Flaxman
* Margaret Christensen as Ma Greevins
* Reg Lye as Pa Greevins Grant Taylor as Stiffy
* Guy Doleman as Mr Quirk
*Leonard Thiele as Mr Scrivens
*Verena Kimmins as Miss MacCowan
*Bruce Beeby as Dr Gasper
*Ruth Cracknell as Mrs Gaspen
*John Fegan as Tom Graham
*Brian Farley as Fred
*Janice Dinnen as Jean Holt
*Barbara Eather as Elsie
*William Rees as Mr Protheroe
*Gordon Chater as Reverend Galbraith
 

==Production==
The novel Smiley had been so popular that author Moore Raymond followed it up with Smiley Gets a Gun in 1947. 

The actor who first played Smiley, Colin Petersen, had moved to England, meaning a replacement had to be found. Anthony Kimmins looked at over 4,000 other applicants before finding Keith Calvert.  Moore Raymond also had returned to England, writing Smiley comicsd for Swift (comics)|Swift Comics.  Kimmins daughter Verena who helped the young actors in the Smiley had a featured role in the film.

Filming took eight weeks towards the end of 1957. Shooting took place at Camden and Pagewood Studios. 

==Release==
The film was less successful than its predecessor and a proposed third film, Smiley Wins the Ashes, was never made. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p226 

==References==
 

==External links==
*  
*  at Oz Movies
 

 
 
 
 
 
 
 


 
 