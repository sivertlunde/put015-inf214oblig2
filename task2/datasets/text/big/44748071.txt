A Novela das 8
{{Infobox film
| name           = A Novela das 8
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Odilon Rocha
| producer       = João Queiroz Filho Justine Otondo
| writer         = 
| screenplay     = Odilon Rocha
| story          = 
| based on       =  
| starring       = Cláudia Ohana Vanessa Giácomo Mateus Solano André Ramiro Paulo Lontra Thaís Müller Aline Fanju Camilla Amado
| narrator       = 
| music          = Tita Lima Rossano Snel Tejo Damasceno
| cinematography = Uli Burtin
| editing        = André Finottti
| studio         = Cria Films Querosene Filmes Geração Conteúdo TC Filmes
| distributor    = Universal Pictures
| released       =  
| runtime        = 108 minutes
| country        = Brazil Portuguese
| budget         = 
| gross          =  
}}

A Novela das 8 (English: Prime Time Soap) is a 2012 Brazilian drama film released on 2011 produced by João Queiroz and directed by Odilon Rocha. The feature is produced by Querosene Filmes, Cria Filmes, Geração Conteúdo and TC Filmes, co-produced and distributed by Universal Pictures. It stars Cláudia Ohana, Vanessa Giácomo, Mateus Solano, André Ramiro between other actors.

Is a tribute to the Brazilian drama, against the backdrop of the novel Dancin Days, Gilberto Braga, who influenced the imagination of most of the Brazilians in terms of fashion, behavior and thinking. The director Odilon Rocha, told the press that "the film recreates the look of the club that appeared on TV, which is in the imagination of most Brazilians who lived through that time and also the youth who knows through novel scenes that can be seen on the internet. The intention is to take viewers to that colorful universe ".

The film debuted to the public at the Festival do Rio in 2011 for later, according to the plans of the director, be presented in international festivals and be released commercially in Brazil, by Universal films, in 30 March 2012.

==Plot==
Brazil, 1978, a group of people live their lives under the dictatorship, the euphoria of disco fever and the fantasy of "Dancin Days", a prime time soap, which fictional drama is set on the homonymous night club in Rio de Janeiro. After a fatal incident, Amanda and Dora (a high-class prostitute addicted to the TV drama, and her "maid") are forced to run away from São Paulo to Rio de Janeiro with the fed Brandão in hot pursuit. While the excitement of visiting the disco "Dancin Days" distracts Amanda from the danger of the situation she is in, Dora gets ready to confront her secret past. In a six degrees of separation manner, their destine will cross with João Paulo, a diplomat who feels like a foreigner in his own country, the revolutionary Vicente and his brother Pedro, and the teenager Caio, who was raised by his grandparents and counts with the support of his friend Mônica as he struggles to be accepted as a gay man. Both youngster are crazy about disco fever and fascinated by the soap "Dancin Days".

==Cast==
* Cláudia Ohana as Dora Dias
* Vanessa Giácomo as Amanda Lima
* Mateus Solano as João Paulo
* André Ramiro as Sergio
* Paulo Lontra as Caio
* Thaís Müller as Mônica
* Aline Fanju as Flávia
* Camilla Amado as Maria
* Otto Jr. as Vicente
* Guilherme Duarte as Pedro

==External links==
*  
*  

 
 
 
 
 
 
 

 
 