The Materassi Sisters
{{Infobox film
| name =   The Materassi Sisters
| image =
| image_size =
| caption =
| director = Ferdinando Maria Poggioli 
| producer =  Sandro Ghenzi 
| writer =  Aldo Palazzeschi  (novel)   Bernard Zimmer  
| narrator =
| starring = Emma Gramatica   Irma Gramatica   Olga Solbelli   Massimo Serato
| music = Enzo Masetti  
| cinematography = Arturo Gallea 
| editing = Ferdinando Maria Poggioli 
| studio =    Società Italiana Cines 
| distributor = Universalcine 
| released =  19 December 1944
| runtime = 72 minutes
| country = Italy Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Materassi Sisters (Italian:Sorelle Materassi) is a 1944 Italian comedy film directed by Ferdinando Maria Poggioli and starring  Emma Gramatica, Irma Gramatica and Olga Solbelli.  The film is an adaptation of the 1934 novel of the same title by Aldo Palazzeschi.

==Cast==
*  Emma Gramatica as Carolina Materassi  
* Irma Gramatica as Teresa Materassi  
* Olga Solbelli as Giselda Materassi  
* Massimo Serato as Remo, il nipote  
* Clara Calamai as Peggy  
* Dina Romano as Niobe, la domestica  
* Paola Borboni as La principessa russa  
* Anna Mari as Laurina  
* Leo Melchiorri as Otello  
* Loris Gizzi as Il sacerdote  
* Ninì Imbornoni as La signora svedese 
* Pietro Bigerna asGiorgio, il prestatore  
* Margherita Bossi as La macellaia 
* Amalia Pellegrini as La domestica della signora svedese  
* Franco Cuppini as Il proprietario del salone automobili 
* Carlo Giustini as Lamante della principessa russa  

== References ==
 

== Bibliography ==
* Reich, Jacqueline & Garofalo, Piero. Re-viewing Fascism: Italian Cinema, 1922-1943. Indiana University Press, 2002.

== External links ==
* 

 
 
 
 
 
 
 
 

 