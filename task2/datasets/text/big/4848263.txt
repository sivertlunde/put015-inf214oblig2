Skinwalkers (2006 film)
 
{{Infobox Film
| name           = Skinwalkers
| image          = Skinwalkers Aug10.jpg
| caption        = Theatrical poster
| director       = James Isaac
| producer       = Dennis Berardi Don Carmody
| writer         = James DeMonaco Todd Harthan James Roday
| starring       = Jason Behr Elias Koteas Rhona Mitra Kim Coates Natassia Malthe Sarah Carter Lyriq Bent
| music          = Andrew Lockington
| cinematography = David Armstrong Adam Kane
| editing        = Allan Lee
| studio         = Skinwalkers DCP Stan Winston Productions Red Moon Productions Constantin Film Lionsgate After Dark Films Freestyle Releasing 
| released       =  
| runtime        = 91 minutes
| country        = United States Canada Germany
| language       = English 
| gross          = $3,269,736 
}}
 Tom Jackson. The film was originally announced for theatrical release on December 1, 2006, but was delayed until August 10. 
 Stan Winston Studio. 

The film was shot at Century Manor in Hamilton, Ontario|Hamilton, Ontario, Canada. To achieve MPAA rating|PG-13 rating, the production cuts several scenes containing graphic violence like Alien vs. Predator—the home media release is the uncut version of the title.

==Plot==
Two packs of werewolves, divided by principles, are signaled by the moon of the coming of an ancient prophecy. A young boy named Timothy (Matthew Knight) approaches his 13th birthday, unaware this marks the time of his transformation. Timothy has been raised by his grandmother Nana (Barbara Gordon), his mother Rachel (Rhona Mitra), his uncle Jonas (Elias Koteas), his cousin Katherine (Sarah Carter) and Katherines boyfriend, Adam (Shawn Roberts). His father is said to be dead.

Rachel and Timothy have been unaware that the rest of the family are "good" werewolves who have guarded Timothy and his secret since birth. They know that Timothy is a "half-blood" who is prophesied to end the curse. But they also know that Timothys power will put him in danger, for there are other werewolves that revel and embrace their blood-lust and are bent on finding and killing the boy. Four of these werewolves are a motorcycle pack—leader Varek (Jason Behr), and cohorts Zo (Kim Coates), Sonya (Natassia Malthe), and Grenier (Rogue Johnston), who use a hawk as an  airborne spy—who track down Timothy in the small town of Huguenot, precipitating the movies extended chase.

Varek discovers the location of Timothy via a video tape which is shown to various "good" werewolves that he is alive and well. Reaching Huguenot, Varek saw Nana and Timothy and proceeds with a gunfight between his pack against the "good" werewolves and various townspeople. Adams father is killed in the gunfight and Nana sacrifices herself to let Timothy and the others escape. Jonas explain the whole situation to both Rachel and Timothy and they are convinced after Jonas and others turn into werewolves at night.

The next day, Timothy faints and is sent to a nearby hospital. Vareks gang infiltrate the hospital and attack Timothy. Grenier is killed by Adam while Katherine is being held hostage by Varek. It is subsequently revealed that Varek was Caleb, Rachels husband and Timothys father, and his transformation was due to him feeding on humans.

After escaping to a safe place, Adam goes off on his own and finds Katherine and brings her back. At sundown, Katherine was discovered to have been forced to feed on humans and kills Adam with his own gun. Just as Katherine is about to attack Timothy, Jonas manages to kill her with a gunshot in the back.

They manage to find their next safe place, with Rachel and Timothy hiding in a steel cage while Jonas sets out to ambush Varek, Zo & Sonya. Zo is killed after being trapped and being dropped down from a height. Sonya tries to attack Rachel and Timothy but is shot by Timothy. Rachel then proceeds to finish Sonya off. Varek then tries to kill Timothy but is stopped by Jonas. They get into a struggle and in a bid to win, Jonas feeds on Vareks arm and knocks Varek unconscious. Taken over by the blood frenzy, Jonas attempts to attack Timothy, but is shot to death by Rachel. Varek wakes up and bites Timothy but the clock chimes midnight. He is then struck by Rachel and knocked to the ground. He then transforms back into a human and to his former self, Caleb.

It is later revealed that Timothy is the cure via his blood and that they travel around, giving the cure to those who want it. They also fill bullets with Timothys blood. The film leaves off with Timothy saying "For some I am salvation, for the others their destruction".

==Characters==
*Jason Behr as Varek—Formerly Caleb, Jonass brother and Rachels husband. Now the leader of the evil werewolves. He turns out to be the father of Timothy. He is cured at the end of the film and reverts to his old normal self.
*Elias Koteas as Jonas—The Alpha of the pack sworn to protect Timothy, and father of Katherine. Like the others, he keeps himself restrained on the full moon and wants their curse to finally end. He is shot and killed by Rachel after trying to attack Timothy towards the end of the film.
*Rhona Mitra as Rachel Talbot—An assumed widow hard-pressed to cope with her frail son Timothys frequent illnesses. She doesnt know her family and community is made up of werewolves.
*Natassia Malthe as Sonja—The Alpha female in Vareks pack, and his mate. She was once human, with a seemingly miserable life until changed by Varek. She is very neurotic, completely loyal to Varek, and obsessed with him. She is shot and killed by Rachel after being weakened by a shot from Timothy.
*Kim Coates as Zo—The second in command in Vareks pack. He is attacked by Jonas during the climax and falls to his death.
*Sarah Carter as Katherine—Jonass daughter and, like the rest of the family and friends, a werewolf as well.  She has a boyfriend named Adam. Towards the end of the film, she turns against and kills Adam and almost attacks Timothy but is shot in the back and killed by Jonas. Tom Jackson as Will
*Matthew Knight as Timothy Talbot—A half-blood, the offspring of Varek and Rachel. He holds the power to end the curse of his family and the entire werewolf race along with him.
*Rogue Johnston as Grenier—One of Vareks henchmen. He is mute and has a heavily scarred face. He is shot and killed by Adam. Barbara Gordon as Nana—Timothys grandmother. She is killed during the firefight between the "bad" and the "good" werewolves.
*Shawn Roberts as Adam—Katherines boyfriend, who is also a lycanthrope. He is shot and killed by Katherine, his girlfriend.
*Lyriq Bent as Doak
*Christine Brubaker as Justine
*Roman Podhora as Ralph
*Wayne Ward as Justines Husband Scott Anderson as Courtney
*Caroline Mangosing as Receptionist
*Ramona Pringle as Nurse Sally
*Everton Lawrence as Paramedic
*Pat Quas as Waitress
*James Kirchner as Motel Clerk
*Jessica Huras as Bar Girl
*Matt Hopkins as Gas Station Attendant
*Jasmin Geljo as Cabin Person #1
*Charmaine Hamp as Cabin Person #2
*David Sparrow as Manny the Butcher
*Carl Marotte as Sheriff John
*Todd Schroeder as Red Neck #1
*Tig Fong as Red Neck #2
*L.J. Vasilantonakis as Bartender
*Wayne Downer as Bar Patron
*Derek Kealey as Grim Reaper
*Wendy Crewson as Female Leader
*Wesley French as Native American Man
*Julian Richings as Sad Looking Man

==Box office==

The film was not widely released, appearing at its peak on 745 screens during the course of its 2  week run.  The film grossed $553,520 on its opening weekend in the US and a total of slightly more than $1 million domestically. The film grossed $2.5 million worldwide.

==Reception==
Skinwalkers had no advance screenings for critics, though Lionsgate did provide DVD screeners to those who asked.  , this Canadian-made action drama sparks an old-fashioned B-movie charge—its welcome-to-the-grindhouse all over again with such delicious conceits as a town of werewolf-citizens and a pistol-packing grandma going it gangsta-style".  Despite the praise given to the creature effects by Stan Winston, the film received mostly negative reviews.

Ultimately, it was not well received.  The film currently maintains a very low rating of 14% at Rotten Tomatoes, with the consensus quote being "Atrociously acted with unoriginal and ineptly-staged action sequences."

==DVD release==
In Canada and worldwide, the film was released in its uncut form in the theaters and on DVD. In the USA however, the film was edited from an R-rating down to a PG-13 for its theatrical and DVD release.

The DVD was released on November 27, 2007. It includes a directors commentary, digital effects comparisons, a previsualization featurette, deleted scenes, the original theatrical trailer, a making of featurette, and additional Lionsgate film trailers to The Monster Squad, Fido, Fangs, and The Descent. There is no "unrated" or directors cut of the film available in the USA.

==References==
 

==External links==
*  
*  
*  
*  
*   at Metacritic
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 