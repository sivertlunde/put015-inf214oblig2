The Battle for Whiteclay
 
The Battle for Whiteclay is a documentary film released in 2008 which chronicles the efforts of Native American activists Frank LaMere (Ho-Chunk|Winnebago), Duane Martin Sr., and Russell Means to end the sale of alcohol in Whiteclay, Nebraska.  Directed and produced by Mark Vasina, the film covers the economy of the approximately one dozen residents and four liquor stores, which sell nearly 5 million cans of beer annually (12,500 cans a day).  The largest populated place nearby is the Pine Ridge Indian Reservation across the border in South Dakota; its residents are largely members of the Oglala Sioux Tribe.

Except for a brief interval, the Oglala Sioux Tribe (OST) has banned the sale and consumption of alcohol on the Pine Ridge Indian Reservation since 1832. A high proportion of residents buy it in Whiteclay and consume it illegally.   They suffer from high rates of alcoholism, diabetes and related health and social problems.  , Sociology Source, 22 November 2012 

Vasina is a documentary filmmaker living in Lincoln, Nebraska, and the former president of the non-profit group Nebraskans For Peace.

The Battle for Whiteclay was awarded Best Political Documentary at the 2009 New York International Independent Film Festival.

==Background==
 
Whiteclay has been active as a Nebraska border post selling alcohol to Oglala Sioux at Pine Ridge Indian Reservation since the buffer zone was removed in 1904 by executive order of President Theodore Roosevelt.

In 1999, after the murders of two young Lakota men at Whiteclay, Oglala Sioux Tribe (OST) and supporting groups, such as Nebraskans for Peace, protested publicly for the state to do something about controlling or shutting down beer sales in the town. They also asked for the county to provide increased law enforcement in the hamlet, which is 22 miles from the seat of rural Sheridan County, Nebraska.  The county sheriff had limited resources to patrol the town. Duane Martin, Sr. and others of the Strong Heart Society of the Oglala Sioux had a blockade within reservation boundaries of the road to Whiteclay in protest.

In 2003 activists including Russell Means (Oglala Sioux and member of the American Indian Movement), Frank LaMere (Ho-Chunk|Winnebago), Duane Martin, Sr. and members of Nebraskans for Peace staged a protest and education conference in Lincoln, Nebraska in coordination with state officials to publicize the crisis in Whiteclay as a humanitarian issue. 

In 2005 the Nebraska Attorney General Jon Bruning and Congressman Tom Osborne proposed a collaborative policing scheme: they had secured federal grant money by which the OST could hire more police, who would be deputized by Nebraska to operate in and around Whiteclay, so that the OST would have more control over policing there. Cecilia Fire Thunder, the first woman elected chief of the OST,  , Rapid City Journal, 28 July 2006, accessed 5 June 2011  and the tribal council approved the proposal in June 2005.  

By May 2007, it appeared as if the OST would lose the $200,000 federal grant to support the extra policing, as it had taken no action to hire police or organize for the program. Tribal officials declined to comment on the matter; at the time, Vasina stated that there was internal tribal conflict over the proposed deputization program, for a variety of reasons.  He expressed satisfaction that Nebraskas politicians had "displayed a real commitment to seeing something would be done,"  but called the arrangement an ineffective approach to the matter that allowed the state to avoid its responsibility for the situation.  , Rapid City Journal, May 2007  

Vasina started working on the documentary to show the OSTs efforts to shut down the liquor stores. He ended up working on the film for five years.

==Related activism==
In 2006 and 2007, activists at Pine Ridge planned to blockade the road leading into the reservation from Whiteclay, to confiscate beer being brought in illegally. The Chief of Police James Twiss encouraged other efforts. Some successful prosecutions of bootleggers have been made by the US Attorney in South Dakota. Carson Walker,     , 13 May 2007, retrieved 26 February 2012. 

In 2009, the Nebraska legislature authorized another study of Whiteclay and the issues; representatives visited the area. The legislature passed bills for increased law enforcement and economic development in Whiteclay, as well as increased treatment for health care for the OST. 
 Creighton Preparatory Omaha created a video, The Hidden Massacre of Whiteclay, Nebraska, incorporating material from Vasinas film, which they have posted on YouTube.   They have also created a Facebook page by that name, as a site for generating activism on behalf of shutting down the liquor operations at Whiteclay.

In December 2010, Sheridan County received a $10,000 grant from the Nebraska state government to cover the additional costs to increase Sheridan County police patrols at Whiteclay. Judi  , head of the Nebraska Indian Commission, said she believed it was a sign of "hope and change" for improving conditions at the Pine Ridge Indian Reservation. 

In February 2012 the Oglala Sioux Tribe filed suit against the beer stores of Whiteclay, beer distributors who served them, and major national beer manufacturing companies; the suit sought damages for health and social costs allegedly arising from their promoting of alcohol sales at Whiteclay.   

==See also==
*American Indian alcoholism

==References==
 

==Further reading==
* , Texas Tech University Press, 2008

==External links==
* 
* , on YouTube
 
 
 
 
 
 
 
 