The Amy Fisher Story
 
 
{{Infobox film
|  name           = The Amy Fisher Story
|  image          = The_Amy_Fisher_Story_DVD.jpg
|  caption        = DVD cover
|  director       = Andy Tennant
|  writer         = Janet Brownell
|  producer       = Andrew Adelson Janet Brownell George W. Perkins
|  starring       = Drew Barrymore
|  released       = 3 January 1993
|  runtime        = 96 min
|  language       = English
|  music          = Michael Hoenig
}} ABC and originally aired on that network; in 1993 it was released on VHS and in 2001 on DVD.   

The movie starred 18-year old Drew Barrymore as Amy Fisher. Anthony John Denison portrayed Joey Buttafuoco. It aired the same night and time as another movie on Amy Fisher starring Alyssa Milano for CBS.  The ABC movie garnered higher TV ratings and critical praise than the CBS version.

==Plot summary==

The story begins in 1992, as Amy Fisher lies in a hospital bed with her mother sitting by her bedside. Earlier, she had attempted to commit suicide, but her parents caught her and took her to hospital. As Amy rests in bed, she thinks back on her life over the last two years and her involvement with Joseph "Joey" Buttafuoco.

In 1991, Amys parents bought her a brand new car for her sixteenth birthday. Amy loved the car, but her parents didnt want her to take advantage of the fact she had a new car by using it whenever she wanted. After an argument with her parents, Amy spends her birthday at a friends house. She had previously gone to a restaurant with them, and her father also didnt like the fact that she was wearing such a revealing outfit (to keep the peace, Amys mother told her outfit was lovely). She flirted with the waiter, by looking at him with the corner of her eye, with a seductive smile on her face.

Amy gets into an accident, crashing her car. Her father takes her to Joey Buttafuocos shop to get it fixed. She flirts with Joey lightly, asking him a lot of questions about his personal life. She begins crashing her car on purpose, using it as an excuse to see Joey again. Eventually, the two begin an affair. He is in his mid-thirties while shes in her teens (a minor). Amy becomes increasingly desperate about her relationship with Joey. She felt strongly that he was the one for her, and even though she knew he was married to his high school sweetheart Mary Jo Buttafuoco, she constantly wanted to spend time with him. When he refused to leave Mary Jo, Amy decided to hire someone to kill her. All her potential accomplices proved unwilling to get the job done, so Amy eventually decides to kill Mary Jo herself. Amy goes to the Buttafuoco house and tells Mary Jo that Joey was cheating on her with Amys younger sister. When Mary Jo expressed disbelief, Amy showed her the T-shirt that Joey had given her, but Mary Jo still didnt believe Amy, saying that Joey gave that shirt to a lot of his customers. As Mary Jo was about to close the door on Amy, she took out her gun and shot Mary Jo in the head. The shot didnt kill Mary Jo, but it left her face partially paralyzed for life. Joey realized that it was Amy who had shot his wife. Mary Jo confirmed this when she had to choose out of a lineup. The reports of the shooting spread through the media and she was given the nickname "Long Island Lolita".  Eventually, in late 1992, Amy was sentenced to five to fifteen years in jail. Joey Buttafuoco was convicted of statutory rape in October 1993 and served six months in prison.   

==Reception==
Camille Paglia dismissed the film, calling it "meandering" and criticizing reviewers for hailing it as the best of the three films that were made about Amy Fisher. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 