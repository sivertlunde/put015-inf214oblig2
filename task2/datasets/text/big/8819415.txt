Rails & Ties
 
{{Infobox film
| name           = Rails & Ties
| image          = Rails and ties.jpg
| caption        = Theatrical release poster
| director       = Alison Eastwood
| producer       = Robert Lorenz Peer Oppenheimer Barrett Stuart
| writer         = Micky Levy
| starring       = Kevin Bacon Marcia Gay Harden Miles Heizer Eugene Byrd Brian Bogulski Bonnie Root Michael Stevens Tom Stern
| editing        = Gary Roach
| distributor    = Warner Bros.
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget       
| gross          = $22,136 
}}
Rails & Ties is a 2007 drama film directed by Alison Eastwood and written by Micky Levy. It tells the story of a young boy and his mentally ill widowed mother who commits suicide in her car by parking on a railroad track. The boy confronts the train engineer who accidentally killed his mother, urging him and his wife to raise him after escaping from an unkind foster mother. The two agree to raise him, however, it is later revealed that his wife is dying of breast cancer. Kevin Bacon portrays the train engineer, Marcia Gay Harden plays his sick wife, and Miles Heizer portrays the boy.

== Plot ==
 
Laura Danner is a mentally ill, single mother who takes illegal drugs and is unable to care for her 10-year-old son Davey. Driven to despair, she decides to commit suicide by driving a car on to a railway track, taking Davey with her. She offers him some tranquillisers beforehand but, unbeknownst to her, he spits them out. His mother drives on to the tracks. As a  train approaches, Davey tries in vain to drag her out of the car, himself jumping clear just in time. Two train crewmen, Tom Stark and Otis Higgs, seeing the car on the tracks ahead, argue about whether an emergency stop will derail the train or not. However, he train  hits and kills the boys mother. Subsequently, the railroad company calls for an internal inquiry and suspends the two drivers.
 foster mother. Later on, after being confined to his room, he escapes by shattering the window. The authorities are alerted and a missing persons search is initiated. The boy obtains train conductor Tom Starks home address. He turns up at the Starks home and berates Tom for accidentally killing his mother in the train crash, but eventually he is placated. Toms wife Megan insists on letting the boy stay although Tom initially disapproves. Caring for Davey gradually helps the couple rebond with each other.

Megan is suffering from breast cancer. Having already undergone a mastectomy, she has decided that she will no longer endure chemotherapy and must accept her inevitable death. The couple have no children, partly due to Megans illness and partly because of Toms job. Tom is unable to deal with his wifes illness and implores her to continue treatment. Meanwhile, the social worker, Susan, hearing that the boy may have sought out the Starks home, arrives at the house and even searches it, suspecting the boy is there, but finds only a visibly sick Megan Stark. The family continues to bond, but the social worker keeps an eye on the family. When she sees them out in the park for a picnic, she decides to call the police, but stops when she realizes that the boy is clearly part of a devoted family.

Megans condition deteriorates and Davey discovers that she is indeed dying. He has a fit, blaming himself not only for Megans death but also for the suicide of his own mother. Tom Stark placates the boy, reassuring him that it is not his fault. A few hours prior to her death, Megan tells a saddened Davey how much she loves him. She dies in her sleep. Some time after the funeral, Tom informs Davey that it is perhaps now time to contact the social worker to see if he can adopt him. The film ends with Tom and Davey approaching Susans office hand-in-hand.

== Cast ==
* Kevin Bacon as Tom Stark
* Marcia Gay Harden as Megan Stark
* Miles Heizer as Davey Danner
* Marin Hinkle as Renee
* Eugene Byrd as Otis Higgs
* Bonnie Root as Laura Danner
* Steve Eastin as N.B. Garcia
* Laura Cerón as Susan Garcia
* Margo Martindale as Judy Neasy
* Kathryn Joosten as Mrs. Brown

==External links==
*  
*  

==References==
 

 
 
 
 
 
 
 
 