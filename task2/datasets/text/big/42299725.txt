Inner Worlds Outer Worlds
 
 

{{Infobox film
| name           = Inner Worlds, Outer Worlds
| image          = Inner_Worlds_Outer_Worlds_Film_Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Daniel Schmidt
| producer       = Daniel Schmidt 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Patrick Sweeney
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2012
| runtime        = 121 minutes
| country        = Canada
| language       = 
| budget         = 
| gross          = 
}}

Inner Worlds, Outer Worlds is a documentary film created by Canadian film maker and meditation teacher Daniel Schmidt. The film was released in 2012.

The film was released for free online. It has been narrated in English, French, Spanish, German and Hindi and there are subtitles for 17 languages. The movie won a number of awards at film festivals, including the Award of Excellence at the Canada International Film Festival.

==Background==
The films creator Daniel Schmidt is a Canadian film maker and is also a musician and meditation teacher.   

Schmidt and his wife Eva Dametto run the Breathe True Yoga Center in Ontario, Canada.  Schmidt studied various forms of meditation before making the film including, the traditions of Buddhism, Taoism and the Yogic traditions of India. He also studied various mystical traditions from various cultures. 

==Production==

The film was created by Schmidt with support from his wife Dametto. Schmidt wrote the screenplay, composed an original music score, created the fractal flame imagery, directed, edited, produced and funded the film. The film drew on his background in philosophy, his experience in the television, music and animation industries, and his personal enthusiasm for meditation.

Eva Dametto is a performing and visual artist, a teacher of chakra and hatha yoga, and an expressive arts practitioner. She was involved in the shaping and editing process and also created and performed some of the mantras in the film.

The film incorporates footage of water vibration from German cymatics researcher Alexander Lauterwasser. The 2012 confirmation of the existence of the Higgs Boson or the "God Particle" was incorporated in the film. The film also includes fractal animation footage from artist Jock Cooper as well as music from the Australian musician Indiajiva.

During the production of the film, Daniel and Eva decided that it would be released for free. 

==Reception==
Inner Worlds, Outer Worlds received positive reviews for its unique take on the universe and a rating of 8.4/10 from 326 reviews on IMDb,    and also received positive reviews in the media. In July 2013 Inner Worlds Outer Worlds was listed on TopDocumentaryFilms.com. 

==Awards==
In early February 2013 after its release, the film was nominated for the Award of Excellence at the Canada International Film Festival.  The film won the Peace Award of Excellence at the International Festival for Peace, Inspiration and Equality  in Indonesia, and also won the Merit Award of Awareness at the Awareness Festival in California. It was the winner of Award of Excellence for the International Film Festival for Spirituality, Religion and Vision in Indonesia, and winner for Best Feature Documentary in the DIY Film Festival, California. In the Moving Images Film Festival in Toronto it won best of the Future World showcase. Inner Worlds Outer Worlds was nominated for the Cosmic Angel Award at the Cosmic Cine Film Festival 2014 in Germany. It was the winner of an Eternal Flame Award Surge Film Festival, Texas. Inner Worlds was selected by a jury of film makers as the winner of the FILMMAKER’S CHOICE (Conscious Art) award in the Spirit Enlightened festival in June 2014.  Inner Worlds movie was an official selection in 19 film festivals worldwide.

==References==
 

 
 
 
 