Ganglamedo
Ganglamedo ( , eng. "Snow Flower") is a 2006 musical movie from Tibet, (original name in Chinese 冈拉梅朵; ganglameiduo), directed by Dai Wei, written by Tashi Dawa. 
The movie is about one of the most popular Tibetan folk songs, Ganglamedo ("Snow lotus"), about two girls and two men. 
The story begins sixty years ago, in Tibet. A local girl, who sings the Ganglamedo, calls herself by same name. She is in love with a Tibetan man, Acuo, and they plan to marry. She disappears the night before the marriage is to take place.

Sixty years later, a Chinese singer, An Yu, comes to Tibet to find her lost voice. She meets young Tibetan man Anzha, who helps her get to Lake Namucuo. One day An Yu meets an old woman, Lamu, and discovers she is Ganglamedo.

==Starring==

* 姜世贞 (Kang Se-Jeong), South Korean actress; as An Yu
* 仁青顿珠 (Renqing Dunzhu), Tibetan actor; as Acuo
* 郑昊 (Zheng Hao); Chinese actor
* 索朗措 (Suo Lang Cuo); Tibetan actress
* 丹增卓嘎 (Dan Zeng Zhuo Ga); Tibetan actress

==Theme Song==

Two most famous Tibetan folk singers
* 亚东 (Ya Dong)
* 索朗旺姆 (Sonam Wangmo)

==External links==
*  
*  
*  


 
 
 