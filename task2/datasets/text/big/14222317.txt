California Dreaming (2007 film)
 
{{Infobox film
| name           = California Dreaming
| image          = California Draming - 2007 Poster.png
| alt            = 
| caption        = 2007 theatrical poster
| director       = Linda Voorhees
| producer       = 
| writer         = Linda Voorhees
| starring       = Lea Thompson Dave Foley Patricia Richardson
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
California Dreaming (also known as Out of Omaha) is a 2007 film written and directed by Linda Voorhees starring Lea Thompson, Dave Foley and Patricia Richardson. It was premiered at the 2007 Omaha Film Festival.

==Plot==
Ginger (Lea Thompson) is a driven real estate agent, eager to have the better things in life, and passionate about realizing a lifelong dream – to revisit Doheny Beach in southern California, which she visited once as a teenager. Her family grudgingly forgoes its annual trip to Branson, Missouri, and grants Ginger her cherished California dream for her fortieth birthday.

The RV trip to the West Coast turns into an odyssey with a dizzying array of psychological shipwrecks and unintended detours. When it appears that things cant get worse, a dog leads the entire ensemble into a maze of cataclysmic events that bring the family closer together, although the family ultimately abandons the trip to California, and just stays at home for "vacation".

==External links==
* 

 
 
 
 
 
 


 