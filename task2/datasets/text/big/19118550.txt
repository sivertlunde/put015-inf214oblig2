Live Like a Cop, Die Like a Man
 
{{Infobox film
| name           = Live Like a Cop, Die Like a Man
| image          = Live-like-a-cop-die-like-a-man.jpg
| caption        = Italian film poster for Live Like a Cop, Die Like a Man
| film name      =  
| director       = Ruggero Deodato
| producers       = {{plainlist|
*Alberto Marras
*Vincenzo Salviani }}
| screenplay     =  Fernando Di Leo
| story          =  {{plainlist|
* Fernando Di Leo
* Alberto Marras
* Vincenzo Salviani }}
| starring       = 
| music          = Ubaldo Continiello 
| cinematography = Guglielmo Mancori 
| editing        = Gianfranco Simoncelli 
| studio         =  
| distributor    =  
| released       =  
| runtime        = 100 minutes 
| country        = Italy 
| language       = 
| budget         = 
| gross          = ₤741,142,540
}}
Live Like a Cop, Die Like a Man ( ) is a 1976 Italian crime film (a polizieschi) directed by Ruggero Deodato and starring Marc Porel.   

Quentin Tarantino said the film is "one of the greatest titles of all time, and it lives up to its name!" 

==Cast==
* Marc Porel - Fred Ray Lovelock - Tony
* Adolfo Celi - The Boss
* Franco Citti - Rudy Ruginski
* Silvia Dionisio - Norma
* Marino Masé - Rick Conti
* Renato Salvatori - Roberto Pasquini, a.k.a. Bibi
* Sergio Ammirata - Sergeant
* Bruno Corazzari - Morandi
* Daniele Dublino - Corrupt police inspector
* Sofia Dionisio - Lina Pasquini (as Flavia Fabiani)
* Tom Felleghy - Major
* Margherita Horowitz - Mona, a hostage woman
* Gina Mascetti - Maricca
* Marcello Monti - 3rd kidnapper
* Claudio Nicastro - Commissioner
* Gino Pagnani - Paul, the dog trainer
* Enzo Pulcrano - Mario, Pasquinis henchman
* Alvaro Vitali - Concierge at Pasquinis building

==Production==
Live Like a Cop, Die Like a Man was based off a screenplay by Fernando Di Leo originally titled Poliziotti si nasce poliziotti si muore (Live Like a Cop, Die Like a Cop).  The film was director Ruggero Deodato only film in the poliziotteschi genre. 

==Release==
Live Like a Cop, Die Like a Man was released on March 11 1976. Curti, 2013. p. 181  It has grossed 741,142,540 lire domestically.  The film was cut on its initial release in Italy. This cut involved a scene with Renato Salvatoris character Bibi who has his men gouge out the eyes of a thug played by Bruno Corazzari, and then crush the eyeball under his feet. 

==Notes==
 

===References===
*  

==External links==
* 
* 

 

 
 
 
 
 
 
 