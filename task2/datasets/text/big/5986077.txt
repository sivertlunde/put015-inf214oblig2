The Bostonians (film)
{{Infobox film
| name           = The Bostonians
| image          = The Bostonians.jpg
| caption        = James Ivory
| producer       = Ismail Merchant
| writer         = Ruth Prawer Jhabvala
| based on       =  
| starring       = Christopher Reeve Vanessa Redgrave Jessica Tandy Madeleine Potter Nancy Marchand Wesley Addy Barbara Bryne Linda Hunt Charles McCaughan Nancy New John Van Ness Philip Wallace Shawn Richard Robbins
| cinematography = Walter Lassally
| editing        =
| distributor    = Almi Pictures (USA)
| released       =  
| runtime        = 122 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
| gross          = $1,009,700 
}} Merchant Ivory novel of New York, London and other cities. Vanessa Redgrave received 1984 Golden Globe and Academy Award nominations for Best Actress, and the movie earned other award nominations for its costume design.

It was filmed in Boston, Marthas Vineyard, Belvedere Castle in Central Park, New York City, and Troy, New York. 

==Cast==
*Christopher Reeve - Basil Ransom
*Vanessa Redgrave - Olive Chancellor
*Jessica Tandy - Miss Birdseye
*Madeleine Potter - Verena Tarrant
*Nancy Marchand - Mrs. Burrage
*Wesley Addy - Dr. Tarrant
*Barbara Bryne - Mrs. Tarrant
*Linda Hunt - Dr. Prance
*Charles McCaughan - Music Hall Police Officer
*Nancy New - Adeline
*John Van Ness Philip - Henry Burrage
*Wallace Shawn - Mr. Pardon

==Awards==

===1985 National Society of Film Critics===
* Won: Best Actress - Vanessa Redgrave

===1985 Golden Globes===
* Nominated: Best Performance by an Actress in a Motion Picture - Drama - Vanessa Redgrave

===1985 Academy Awards===
* Nominated: Best Actress in a Leading Role - Vanessa Redgrave
* Nominated: Best Costume Design - Jenny Beavan, John Bright

===1985 BAFTA Awards===
* Nominated: Best Costume Design - Jenny Beavan, John Bright

==Location==
This film was principally photographed at:
*The Library of the Boston Athenaeum
*Gibson House Museum
*Beacon Hill, Boston
*Harvard University
*Chateau-sur-Mer, Newport, Rhode Island
*Troy Savings Bank Music Hall
*Central Park (including Belvedere Castle), New York City
*Marthas Vineyard

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 