Raffles, the Amateur Cracksman (1925 film)
:For Raffles the Amateur Cracksman (1917), Raffles (1930 film) and Raffles (1939 film).
{{Infobox film
|name= Raffles
|image= Raffles - 1925.jpg
|image_size= 250px
|caption= 1925 theatrical poster
|director= King Baggot Universal Jewel (production company)
|writer= Harvey F. Thew House Peters Walter Long
|music= 
|cinematography= Charles J. Stumar
|editing=  Universal Pictures
|released= May 24, 1925
|runtime= 60 min. (6 reel#Motion picture terminology|reels) United States Silent with English intertitles
|budget= 
|gross= 
}} silent adventure adventure crime crime drama/romance romance film|motion House Peters, Walter Long.
 Directed by produced by Universal Pictures, adapted by Harvey F. Thew from the play by Eugene W. Presbrey and the 1899 novel, The Amateur Cracksman, by Ernest William Hornung|E.W. Hornung. 

==Plot== Raffles (played by House Peters) is an English gentleman with a secret life&mdash;he is the notorious jewel thief known as "The Amateur Cracksman." While sailing from India to England accompanied by his friend, Bunny Manners (played by Freeman Wood), it is rumored that the infamous cracksman is aboard ship. Raffles warns a lady passenger to keep an eye on her necklace, which is stolen soon afterward. Although a search reveals no evidence, the necklace is returned upon reaching London.

Lord Amersteth (played by Winter Hall) and his wife, Lady Amersteth (played by Kate Lester), are having a party at their home and Raffles attends. Another guest, noted criminologist Captain Bedford (played by Fred Esmelton), makes the assertion that a very valuable string of pearls cannot be stolen. Encouraged by this, Raffles steals it.

He has also stolen the heart of Gwendolyn Amersteth (played by Miss DuPont), the daughter of his hosts. Capt. Bedford finally captures him, but he escapes with Gwendolyns help and they run away to be married. Raffles returns the pearls and promises to retire from being a burglar.

==Cast== House Peters as Raffles, the Amateur Cracksman
*Miss DuPont as Gwendolyn Amersteth
*Hedda Hopper as Mrs. Clarice Vidal
*Fred Esmelton as Capt. Bedford Walter Long as Crawshay
*Winter Hall as Lord Amersteth
*Kate Lester as Lady Amersteth
*Freeman Wood as Bunny Manners
*Roland Bottomley as Lord Crowley
*Lillian Langdon as Mrs. Tilliston
*Robert Bolder as Mr. Tilliston

==References==
 

==External links==
* 
*  AFI Catalog of Feature Films

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 