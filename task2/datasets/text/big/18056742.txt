London Dreams
 
{{Infobox film
| name           = London Dreams
| image          = londondreamsfilm.jpg
| caption        = Theatrical release poster
| director       = Vipul Shah
| producer       = Vipul Shah
| story          = Suresh Nair Ritesh Shah
| screenplay     = Suresh Nair
| starring       = Salman Khan   Ajay Devgan Asin Rannvijay Singh Aditya Roy Kapur Om Puri
| music          = Shankar-Ehsaan-Loy
| cinematography = Sejal Shah
| editing        = kunal midtha
| cinematography = Headstart Films UK Limited Blockbuster Movie Entertainers
| released       =  
| runtime        = 152 minutes
| country        = India
| language       = Hindi
}} musical drama film directed by Vipul Shah. The film features Ajay Devgan, Salman Khan and Asin in lead roles. It released on 30 October 2009. The music was by the trio of Shankar-Ehsaan-Loy with lyrics by Prasoon Joshi.

==Plot==
The story revolves around two childhood friends Arjun (played by Ajay Devgn) and Mannu (played by Salman Khan). Arjun wants to become a music star, so he inadvertently asks God for his fathers death. When he and his uncle move to London, Arjun runs away.

Arjun creates a fledgling band with Zoheb and Wasim (Rannvijay Singh and Aditya Roy Kapoor respectively), two brothers who duped their relatives in Pakistan to travel to London in pursuit of their musical aspirations. He also brings aboard Priya (played by Asin), a music enthusiast from a conservative South Indian family. Back in India, Mannu seduces married women and finds himself in debt with the locals.

After paying Mannus debt, Mannu goes to London to join Arjuns band, but becomes more popular with the crowds. Mannu also flirts with Priya.

The band embarks on a three-city tour spanning Paris, Rome and Amsterdam where Arjun deceives a naive Mannu into a rollercoaster ride of promiscuous sex and illicit drugs. He tricks him, gets him addicted to drugs, and then gets him arrested in a car full of them. While pretending to help Mannu, Arjun leaks the drug story to the press. As the three-city tour concludes, the band heads to London to perform at Wembley Stadium in front of an audience estimated at 90,000 (which is of significance since, earlier in the movie, the viewer is told that Arjuns grandfather had failed before a similar audience). Knowing how important this is for Arjun, Mannu tries to give up drugs. But Arjun decides that his success and Mannus total failure are related. He pays a girl to pretend to have oral sex with Mannu, which makes Priya break up with Mannu. In this fragile state, Zoheb pushes Mannu toward drugs again so that he cant come on stage.

In the moments leading up to the stage entrance, Mannu comes to senses and chooses the righteous path and runs to support his mate. But Arjun, who has become incensed with the crowd chanting Mannus name, confesses his envy of Mannus talent and what he did to finish Mannu off. The audience boos Arjun, the band breaks up and a sad Mannu goes back to his village.

Arjuns uncle (Om Puri), advises him to apologize to Mannu. Then it is revealed that after knowing the truth Priya and Mannu reconcile. Also, she marries Mannu and lives with him in his village. In the village, however, Mannu tells him not to apologize saying that it was his fault that he didnt see Arjuns sorrow and Priya also pardons Arjun for his wrong deeds. They get back together and London Dreams becomes a successful band again.

==Cast==
* Salman Khan as Manjeet "Mannu" Khosla
* Ajay Devgn as Arjun
* Asin as Priya
* Rannvijay Singh as Zoheb Khan
* Aditya Roy Kapur as Wasim Khan
* Om Puri as Arjuns uncle

==Production==

 

==Reception==

===Critical response===
London Dreams received mixed to negative reviews from critics. Taran Adarsh praised the performance of the principal cast, while criticizing the climax.  Rajeev Masand gave a scathing review of the movie, describing it as a "frustratingly foolish film about foolish people."   Chandrima Pal of Rediff criticized the background music of the movie, while praising the dialogues, as well as Ajay Devgans performance.  Noyon Jyoti Parasara of AOL said "The last twenty minutes of ‘London Dreams’ take away half its spirit." 

==Soundtrack==
 
The song style is generally rock inspired to match the motifs in the movie.
{{Track listing
| extra_column = Performer(s)
| title1 = Barso Yaaron | extra1 = Vishal Dadlani, Roop Kumar Rathod | length1 = 05:48
| title2 = Man Ko Ati Bhavey Saiyaan | extra2 = Shankar Mahadevan | length2 = 05:14
| title3 = Tapke Masti | extra3 = Feroz Khan punjabi singer from punjab | length3 = 04:29
| title4 = Khanabadosh | extra4 = Mohan | length4 = 04:41
| title5 = Khwab | extra5 = Rahat Fateh Ali Khan, Shankar Mahadevan | length5 = 06:15
| title6 = Yaari Bina | extra6 = Roop Kumar Rathod, Milind Diwadkar | length6 = 04:32
| title7 = Jashn Hai Jeet Ka | extra7 = Abhijeet Ghoshal | length7 = 04:11
| title8 = Shola Shola | extra8 = Zubeen Garg | length8 = 05:31
| title9 = Khanabadosh | note9 = Remix | extra9 = Mohan | length9 = 05:00
| title10 = Man Ko Ati Bhavey Saiyaan | note10 = Remix | extra10 = Shankar Mahadevan | length10 = 05:30
| title11 = Tapke Masti | note11 = Remix | extra11 = Feroz Khan | length11 = 04:20
}}

==References==
 

==External links==
*  

 
 
 
 