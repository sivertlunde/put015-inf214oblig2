Ghostkeeper
 
 

{{Infobox Film
| name     = Ghostkeeper
| image = Ghostkeeper video.png
| caption = Original VHS box cover
| director = Jim Makichuk
| producer = Howard J. Cole
| writer =  Jim Makichuk Doug MacLeod
| starring = Riva Spier Georgie Collins Murray Ord Sheri McFadden
| cinematography = John Holbrook
| music = Paul Zaza
| editing = Stan Cole
| distributor = Badland Pictures New World Pictures Starmaker Video (video release)
| released = December 1981  (US, Canada)   
| runtime = 89 minutes
| country = Canada
| language = English
| budget = $750,000 CAD (Estimated)
}}
 independent Canada|Canadian Rockies who become stranded at an abandoned hotel where an old woman seems to be hiding something. 

Filmed in Banff, Alberta under a tax shelter in December 1980, the film had an unstable financial situation and the filmmakers nearly halted the production mid-way through due to depletion of its budget. The film was given a minuscule theatrical run in Canada and the United States and is consequently little-known among horror film fans, but has attained a cult following over the years.   It was released in VHS format in the 1990s, and released on DVD by Code Red Releasing in April 2012.

==Plot==
The film opens with a title card reading "In the Indian legends of North America, there exists a creature called Windigo... a ghost who lives on human flesh." The audience is then introduced to Jenny  (Riva Spier), Marty (Murray Ord), and Chrissy (Sheri McFadden), who are spending their New Years Eve on a snowmobiling trip in the Rocky Mountains. After talking with a man at a small ski shop, the three decide to go riding before dark, but end up caught in a blizzard. Looming before them is a seemingly abandoned hotel at the top of the snow trail, isolated from tourists and miles away from the skiing area.

The three enter the building to escape the increasingly harsh conditions, and find that the heat is on — there are, however, no working lights. As night falls, they start a fire in a fireplace in the lobby of the hotel, and tell stories and reminisce. Marty decides to go into the hotel kitchen where he finds an old woman (Georgie Collins) lurking. She is brash and apprehensive of their presence, and they learn that she lives in the hotel with two unseen sons; she fails to tell them her name.

Though hesitant, the old lady takes them to their rooms. Marty and Jenny argue in their bedroom, while Chrissy goes down the hallway to take a bath since the plumbing in their rooms is obsolete. While in the bathtub, Chrissy is attacked by one of the old womans sons, and held underwater until she becomes unconscious. A restless Jenny goes to check on Chrissy, but the candle-lit bathroom is empty; Jenny then runs into the old woman, and they speak for a few minutes about the hotel and her sons; she says ambiguously that shes "getting too old for it". Marty then stumbles upon the conversation, and they part ways. Meanwhile, Chrissys attacker carries her to the basement of the hotel, where he slits her throat and then leaves her body in an igloo-like freezer.

Marty and Jenny retire to their rooms, and Jenny awakens in the middle of the night. She leaves her room again, and hears the old woman talking to someone downstairs. She walks to the end of the hallway to the stairwell, and peers down to see the old lady talking to her son, saying, "Its done, isnt it?".

The next morning, Marty goes outside to the snowmobiles to try and start them, but they still fail to turn on. Jenny gets dressed and goes downstairs, where Marty is arguing with the old woman in the kitchen — Chrissy is nowhere to be seen, and he suspects the snowmobiles have been tampered with. Marty goes outside the hotel to an old shed to look for tools, while Jenny stays in the kitchen with the old woman.  The woman offers Jenny tea, which she accepts. Over tea, Jenny inquires about Chrissys disappearance, but the old woman is evasive.
 Native American folklore, which explains that a windigo is often "kept" by an old woman who had the power passed on to her from another.
 human Windigo. The other son comes down to the basement with a chainsaw, and chases Jenny throughout the hotel, up into the attic. Jenny exits through a window onto a small balcony; she manages to push the man over, where he is impaled on an iron fence below.
 possessed and is rambling to himself. He wanders off into the woods, and Jenny returns to the hotel. Meanwhile, the storekeeper from the beginning of the film arrives at the hotel in search of the trio; almost immediately after entering, he is stabbed to death by the old woman. Jenny re-enters the hotel, where she finds the old womans sons body, which has been dragged into the foyer. She finds a shotgun in a storage room, and is confronted by the knife-wielding old woman, who claims she is Jennys dead mother. Jenny shoots her in the chest, killing her.

After shooting the woman, Jenny seems to be overtaken by something; she goes to the basement and visits the windigo in the freezer, saying, "Its all right, Jenny will look after you now." She goes outside and finds Martys lifeless body, but has no reaction to it; "Ill come back for you, Marty", she says, sweetly.

The film ends with Jenny sitting in a lounge chair in front of the fire that night; the old womans voice says in her head, "Youve done good. Youll be fine... Ill look after you, you know I will. Itll be like it always was. Youll see".

==Cast==
*Riva Spier as Jenny
*Murray Ord as Marty
*Sheri McFadden as Chrissy
*Georgie Collins as Ghostkeeper
*Les Kimber as Storekeeper
*Bill Grove as Danny (credited as Billy Grove)
*John MacMillan as Windigo

==Production== Lake Louise and the Banff National Park in Alberta, Canada. Filming began on December 1, 1980, and finished on December 23, 1980.  The films budget was a modest CA$750,000 dollars, thus justifying the film as a highly low budget production. The cast was made up of unknown or locally known actors, and for most of them, Ghostkeeper was their first and final film credit (Georgie Collins was primarily a well-known stage actress in Calgary, and Murray Ord went on to become a successful film producer in later years).

The film was made under a tax shelter, which made its financial situation rather unstable; Jim Makichuk said that he was given the option of halting the films production when the funding began to run low, but he pursued to finish the film anyway—  

According to Makichuk, prior to the depletion of the budget, his original intention was to film a much longer ending, including an extended chase sequence with Spier and the Wendigo creature on the rooftop of the Deer Lodge hotel.   
 Prom Night My Bloody Valentine (1981). Zaza got on board due to the involvement of film editor Stan Cole. In fact, some of the musical themes featured in Ghostkeeper were also used in Prom Night. 

==Release==
Ghostkeeper was released in Canada and the United States in 1981, being distributed by New World Pictures, but did not receive a home video release until the early 1990s. The film was released on VHS in September 1990 through New World Pictures home video branch. Director Jim Makichuk stated in an interview that he was trying to get a DVD released through Netflix with a quality print of the film.   

In April 2012, Ghostkeeper was released for the first time on DVD through Code Red Releasing. The DVD included a commentary with Jim Makichuck, Riva Spier and Murray Ord, an interview with director of photography John Holbook as well as an interview with actress Georgie Collins. The film is presented in 1.78.1 anamorphic widescreen for the first time on video and has been restored from the only known existing film elements.   
==Reception==
 
Critical reception for the film has been mixed-positive.

Terror Trap.com gave the film a positive review stating, "Not for all tastes, the methodically paced Ghostkeeper is an exercise in disciplined mood generation; its bare claustrophobia either works for the viewer or not. But for those who prefer their chills straightforward, pre-sneer and pre-sarcasm, Keeper can be a most rewarding snow trap". 

J.A. Kerswell from Hysteria Lives! gave the film a negative review calling the film "flawed", and "muddled". 
==See also==
*Folklore of the United States
*Wendigo

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 