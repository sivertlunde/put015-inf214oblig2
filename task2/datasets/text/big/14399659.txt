Midnight Eagle
{{Infobox film
| name           = Midnight Eagle
| image          = 
| caption        = 
| director       = Izuru Narushima
| producer       = 
| screenplay     = Yasuo Hasegawa Kenzaburo Iida
| based on       =  
| starring       = Tatsuya Fuji Yoshihiko Hakamada Ken Ishiguro Nao Ōmori Takao Osawa Yuko Takeuchi Hiroshi Tamaki A-Saku Yoshida
| music          = Takeshi Kobayashi Hideo Yamamoto
| editing        = William M. Anderson
| distributor    = Shochiku Company Shochiku Eizo Company Universal Pictures Strand Releasing
| released       =  
| runtime        = 92 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2007 action film directed by Izuru Narushima and written by Yasuo Hasegawa and Kenzaburo Iida, based on the novel by Tetsuo Takashima. Midnight Eagle is the third film directed by Izuru Narushima. 
 Los Angeles. Los Angeles on December 7, 2007.   

==Production==
Executive producers were Keiji Kameyama, Kazutaka Akimoto, Yasuhide Uno, Riki Takano, and Kanjiro Sakura. Producers were Michihiko Umezawa, Masakazu Yoda, Teruo Noguchi, Tomiyasu Moriya, and Kei Fujiki.     The film was shot with the full cooperation of the Japan Ground Self Defense Force, Japan Air Self-Defense Force, and Ministry of Defense.     Mountaineer Hirofumi Konishi was the special advisor for the scenes set in the Northern Alps.  Before shooting started, the crew of the film was given training in snow-covered mountains.  Midnight Eagle is the first mountain film set in Japan since The Precipice in 1958. 

==Plot==
A top secret American forces strategic bomber known as "Midnight Eagle" suddenly vanishes in the Japanese Hida Mountains.

==Cast==
*Takao Osawa as Yuji Nishizaki
*Yuko Takeuchi as Keiko Arisawa
*Hiroshi Tamaki as Shinichiro Ochiai
*A-Saku Yoshida as Major Akihiko Saeki
*Yoshihiko Hakamada as Toshimitsu Fuyuki
*Nao Ōmori as Major Kensuke Saito
*Ken Ishiguro as Tadao Miyata
*Tatsuya Fuji as Prime Minister Watarase

==Critical reception==
The film received mixed reviews by Western critics. As of November 24, 2007 on the review aggregator Rotten Tomatoes, 20% of critics gave the film positive reviews, based on 5 reviews.  On Metacritic, the film had an average score of 48 out of 100, based on 6 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 