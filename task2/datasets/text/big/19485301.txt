Blaavand melder storm
{{Infobox film
| name           = Blaavand melder storm
| image          = Blåvand Melder Storm.jpg
| image size     =
| caption        = Front cover of the Danish DVD
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       =
| writer         = Lau Lauritzen, Jr. Børge Müller Alice OFredericks
| narrator       =
| starring       = Osvald Helmuth
| music          =
| cinematography = Karl Andersson
| editing        = Marie Ejlersen
| distributor    =
| released       = 26 December 1938
| runtime        = 102 minutes
| country        = Denmark
| language       = Danish
| budget         =
| preceded by    =
| followed by    =
}}
Blaavand melder storm is a 1938 Danish thriller film directed by Lau Lauritzen, Jr. and Alice OFredericks.

==Cast==
* Osvald Helmuth as Fiskeskipper Jens Olesen
* Frits Helmuth as Jens Olesen som barn
* Betty Söderberg as Anna Olesen
* Ellen Løjmar as Johanne Olesen
* Lise Thomsen as Inger
* Axel Frische as Fiskeskipper Christian Larsen
* Lau Lauritzen, Jr. as Svend Larsen John Price as Thorsten Larsen
* Johanne Fritz-Petersen Blom as Karen
* Sigurd Langberg as Fiskeskipper Mads Olsen Carl Fischer as Fiskeskipper Claus Mikkelsen
* Carl Heger as Førstemand på Frida
* Erik Malberg as Hjælper på Frida
* Elith Foss as Kokkedreng på Frida
* Gunnar Lauring as Førstemand på Falken
* Thorkil Lauritzen as Kokkedreng på Falken

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 