Saikano
 
{{Infobox animanga/Header
| name = Saikano
| image = Saikano JPN volume 1.jpg
| caption = Cover art of the first Saikano tankōbon featuring lead character Chise
| ja_kanji = 最終兵器彼女
| ja_romaji = Saishū Heiki Kanojo
| genre = Romance novel|Romance, Military, Apocalyptic and post-apocalyptic fiction, Tragedy, Inspirational fiction
}}
{{Infobox animanga/Print
| type = manga
| author = Shin Takahashi
| publisher = Shogakukan
| publisher_en =  
| demographic = Seinen manga|Seinen
| magazine = Big Comic Spirits
| first = May 30, 2000
| last = December 25, 2001
| volumes = 7
}}
{{Infobox animanga/Video
| type = tv series
| title = 
| director = Mitsuko Kase Gonzo
| licensee =  
| network = Family Gekijou
| first = July 2, 2002
| last = September 24, 2002
| episodes = 13
| episode_list = List of Saikano episodes
}}
{{Infobox animanga/Video
| type = ova
| title = Saikano: Another Love Song
| director = Mitsuko Kase
| studio = Studio Fantasia
| licensee =  
| first = August 5, 2005
| last = September 21, 2005
| runtime = 
| episodes = 2
| episode_lists = 
}}
{{Infobox animanga/Video
| type = live film
| title = 
| director = Taikan Suga
| producer = 
| writer = Yukako Shimizu
| music = Makoto Anzai
| studio = Toei Company
| released = January 28, 2006
| runtime = 120 minutes
}}

 
  is a manga, anime, and OVA series by Shin Takahashi, creator of Iihito and Kimi no Kakera. Saikano was originally serialized in Shogakukans Big Comic Spirits magazine.

A live-action movie adaptation was released in Japan on January 28, 2006 with Aki Maeda starring as Chise. 
 English in UK by Manga Entertainment (as "She, the Ultimate Weapon"). The anime series was also licensed by Viz, but is now licensed by Sentai Filmworks. 

Although the city is not mentioned by name in the series, many of the locations used in Saikano can by all probability be found in Otaru, Hokkaidō|Otaru-shi, west of Sapporo. The train station, "Hell Hill",    the Asahi Observation Hill, the view over the harbor, and the school all have similar looking counterparts in the city of Otaru. 

==Plot== exchange diaries flashbacks while reading Chises diary. Chise, a fellow student in his class, declares her love for Shuji at the beginning of the series. However, Chise is very shy and Shuji is insensitive: neither know how to express their feelings very well, but they do indeed have feelings for each other.

One day, while Shuji is shopping in Sapporo, unknown bombers attack the city in broad daylight. He and his friends run for cover, but notice a fast and small flying object shooting down the enemy bombers. Separated from his friends, Shuji wanders through the wreckage—only to stumble upon Chise; here she has metal wings and weapons—apparently grafted onto her body. She tells him she has become the ultimate weapon, without her knowledge or consent, and that she is seen by the Japan Self-Defense Forces (JSDF) as the last hope for defending Japan. In the anime, it is not apparent why Chise was chosen to be the ultimate weapon or why the country is under attack. It was not until the OVA episodes were released that an explanation for Chise being chosen was offered: her body has the highest degree of compatibility with the weapon system.

This story focuses primarily on Chises fading humanity as her condition worsens. The main conflict is within Chise herself; she questions whether or not she is human. Her soul is constantly trying to be a normal girl, while her body succumbs to the devastating effects of the weapon cell within her. Fundamentally important to the plot is the relationship between Shuji and Chise. From this, the resolution of the conflict follows. In the end, she is able to realize who she truly is.

A number of minor characters who do not necessarily know of Chises role in the war have sub-plots that concern everyday people in the context of war: a woman whose husband is constantly away from home, a school boy who joins the army to protect his girlfriend, a girl whose civilian boyfriend is killed in a bombing, and others.

===War===
Both the manga and the anime feature a hotly contested war. Battles are shown through the lives of people on the front, but the diplomatic particulars of the war are not revealed to the audience. A reason is never given as to why the war broke out in the first place.
 Tornado jets, as used by European Air forces, and the attacking army is described as The Combined Forces.

The only weapon of mass destruction observed in both the anime and manga was Chise herself, who by the middle of the story had the power to destroy entire cities and did so on a fairly regular basis. In more than one battle over a Japanese city, Chise simply vaporized the city and most of the people in it. In the OVA, one English speaking soldier from The Combined Forces claims that Chise destroyed his home town.

==Characters==
===Main characters===
; 
: 
:She is the main female character of the story. Chise is a shy, clumsy girl with very little self-esteem and has poor grades in everything except for World History. She was constantly hospitalized in Tokyo during her elementary years, and thus, has very few friends (except for Akemi). She starts dating Shuji, with Akemi’s help, and tries to make her relationship work with him despite his apparently aloof personality. However, Chise is very inexperienced and does not know much about relationships to the point she reads shōjo manga for advice. She was turned into the ultimate weapon against her will and the series revolves around her and her fading humanity. As Chise’s weapon-side starts to take over, her heart stops beating, her body lacks warmth and her sense of taste and touch are dulled, but other senses (especially sight) are accentuated. Her condition worsens and her humanity seems to fade away entirely. In the final episodes of the anime, she appears to be nothing more than a cold, ruthless machine that delights in her growing, destructive powers and killing people without mercy. Her love for Shuji is the last of her humanity. Throughout the story, she tries to come to terms with her body while still trying to convince herself that she is still human. She believes that she is nothing more than a weapon designed to kill. Her boyfriend Shuji, however, is able to help her break free by showing her that she is able to protect the ones she loves, and that only a human can experience the feeling of love. This helps Chise destroy the body in which she was trapped, and she realizes that she is indeed human. This brings to question what is meant by the term "ultimate weapon" used throughout the story. Chises soul had been the only thing that was able to destroy her body, which was claimed to be the most powerful thing in the world.

; 
:  antisocial 17-year-old track team. Shuuji is very unsure of his initial feelings for Chise and feels that their relationship is more trouble than its worth, though his feelings deepen as the story progresses. Not long after the two became much closer, they decided to simply become friends again and leave their closer relationship alone. This gives him time to realize his love for Chise and he eventually wishes to give the relationship another try. However, things get complicated when his first love, Fuyumi, reappears in town. He is the only civilian who knows Chise is the ultimate weapon and promises that he will never divulge her secret. Shuuji is feeling constantly guilty and useless because his irresponsible actions tend to hurt Chise. Later on in the series, he comes to accept his love for Chise and vows to protect her and be by her side at all costs.

; 
:  insecurities about her appearance, always questioning why any man would fall for her. She always comes to Chise’s defense whenever Shuji inadvertently hurts Chise verbally. Akemi reveals to Shuji in the ninth episode of the anime that she loves him. Due to an earthquake, she becomes grievously injured and soon dies as a result.

; 
:  JSDF to protect the girl he loves.

; 
: 
:Regiment leader, Fuyumi’s husband, and the sole survivor of Chise’s first platoon. He’s very much like Shuji in terms of looks and personality and that’s why Chise is initially attracted to him. He treats Chise like an ordinary girl, which is why Chise likes him. Hes one of the characters to be featured in the OVA, where Mizuki, his former commander, was shown to have a crush on him.

; 
:  track team when she was younger and her students called her “Fuyumi-senpai” because of the close age difference. She is always feeling lonely as her husband, Tetsu, is never home since he’s always away in the army. It is soon revealed that Shuji and Fuyumi were once in love. Shuji explains that his love for her was never true; he simply wanted to become physically closer to her. It is only later in the story that Shuji realizes how much he should have cared for her. After he admits his deceitful wrongdoings, Fuyumi says that she had already known all along. She leaves and is never seen again.

; 
: 
:Seen only in the OVA, Mizuki was the prototype for the weapon system which would later be used on Chise. She was an officer who was badly wounded in an attack, but returned to the battlefield because of the prototype weapon system. She also had a crush on Tetsu, her former deputy. She was one of the very few people sympathetic (and empathetic since she, too, was a weapon) towards Chise. Eventually, Mizuki sacrifices herself so Chise can continue on.

===Minor characters===
; 
: 
:The most extroverted guy in Shuji’s circle of friends and Yukaris boyfriend. Take is the first one to get a girlfriend in Shujis group. His girlfriend is named Yukari.

; 
: 
:The most immature guy in Shuji’s circle of friends. Nori is a naive high-school student, who believed that the war would never reach their hometown and they should stop worrying about it. He desperately wants to have a girlfriend, and is slightly jealous of Take for having one.

; 
: 
:Take’s girlfriend. After her boyfriend’s death, she leaves the school. Soon after that, she gets another boyfriend and on the surface, claims she will never love anyone seriously again. Later on in the story, an enemy plane crashes in the mountainside and Yukari and a group of friends go out to look for occupants of the crashed plane. With them, they bring numerous firearms to defend themselves. Yukari stumbles across a dog tag in the snow and picks it up. The enemy who survived the plane crash comes out of hiding, seemingly helpless, with his hands up. Yukari is overwhelmed by emotion and points her gun at the enemy, stating she will never forgive them for the death of her boyfriend Take. She pulls the trigger only to realize the safety is on and begins to cry, whilst still trying to shoot the gun. The enemy pulls out his weapon and points it at her while Yukari turns the safety off on her own gun. They both fire at the same time and Yukari dies while killing the enemy out of anger and vengeance for her dead boyfriend, Take. In the anime, the scene ends showing both bodies on the ground, lifeless and surrounded by blood. It can be argued at this scene suggests that in war, there are no winners. In the preceding episode of the anime, Yukari seems to foreshadow her death. Her words translate as "You could fall in love with someone then end up dying the next day." With the same breath she explains how she feels there is no point in loving someone so seriously. She ended up dying the next day.

; 
: 
:Young soldier on Chise’s former Company (military unit)|company. He admires Chise very much, telling her that her combat skills saved his life. He also appears in the OVA, alongside Tetsu.

; 
: 
:Head scientist in charge of Chise. He is a nervous man that perpetually wipes his forehead with a dark-blue cloth. He also makes an appearance in the OVA.

===Others===
; 
:  insane after witnessing the full extent of her powers. He also appears in the OVA.

; 
: 
:Atsushi’s army mate. He’s a playful, teasing person and gets along well with Atsushi. He dies when his military base is attacked.

; 
: 
:Girl who is part of Chises circle of friends. She was spying behind a locker (along with Akemi, Seiko, and Yukari) when Chise confessed her feelings to Shuji.

; 
:  
:Girl who is part of Chises circle of friends. She was spying behind a locker (along with Akemi, Rie, and Yukari) when Chise confessed her feelings to Shuji.

; 
:  recruit that is seen in the series. She joins the military to avenge her boyfriend’s death.

; 
:  optimistic and track team.

; 
: 
:She is a kind and gentle woman, who always worries over her family.

; 
: 
:A man who cares for his family, though like his son, he is somewhat reserved.

==Media==

===Manga===
  Spanish by Italian by German by French by Éditions Delcourt (publisher)|Delcourt.

The anime follows the manga very closely until the end, where it takes a dramatic turn. In the manga, Chise is portrayed as the one who, after having been almost completely replaced by machine, decides to "liberate" what is left of mankind from its suffering of having to exist on a devastated Earth. In the anime, Chise tries to save the last of the Japanese population from an impending natural disaster coupled with an arriving foreign attack force, but fails. In both, Shuji becomes the last surviving member of humanity. The anime ending is rather disjointed from the rest of the story, where Chise is actively attacking and eliminating population centers around the world.

===One-shots===

;Love Story, Killed

The story is of a 17-year-old Japanese sniper, who hides in the remains of a block of flats and kills enemy soldiers so that he can collect their guns (sans ammunition, just to lay them out on the floor). At the start of the story, he kills a group of enemy soldiers who are about to rape a Japanese girl. While collecting their guns, the sniper is confronted by the girl who picked up one of the guns, but the sniper eventually befriends the girl and takes her back to his room because he wants to have sex with her. It is revealed that he only has five rounds left, with his final round being his good luck charm that he keeps in the pocket, and that he wants to die because it will relieve him from the war. However, after getting to know the girl better and having sex with her, he becomes afraid of death, and when firing on an enemy squadron that came looking for him, he misses and reveals his position. Eventually, he finds himself unable to fire his good luck charm round due to his fear of death. Suddenly the girl appears in the doorway, bringing the guns and ammo of the soldiers he has just killed, saying that he now has the ammunition to kill more soldiers and can keep fighting in the war. Right as the sniper smiles however, three enemy soldiers appear behind the girl, kill the sniper and then rape and kill the girl. The enemy soldiers themselves all die two days later.

; The Last Love Song On This Little Planet

A series by Shin Takahashi which is a collection of OneShots  called "The Last Love Song On This Little Planet". The first, and currently only released, story is of the day Chise asked Shuji to be her boyfriend and their journey home from the place where that happens. Takahashi does not shy away from confronting the awkwardness between the two teenagers and their problems, continuing the trend from his main series "SaiKano"

===Anime===
 
The television series ran for 13 episodes and faithfully adapted all of the chapters in the manga. However, the last TV episode strayed from the mangas continuity and gave an original conclusion to the anime.
 OVA episodes were also released.

; Opening Theme: "Koisuru Kimochi" by Yuria Yato
; Ending Theme: "Sayonara" by Yuria Yato

====Staff====

{| class="wikitable"
|-
! Director
| style="width:180px;"| &nbsp;Mitsuko Kase
|-
! Music
| &nbsp;Takeo Miratsu
|-
! Original Manga
| &nbsp;Shin Takahashi
|-
! Producers
| &nbsp;Shigeru Kitayama &nbsp;Tatsuya Hamamoto
|-
! Chief Animation Director
| &nbsp;Masayuki Sato
|-
! Music producer
| &nbsp;Takeshi Ando
|-
! Original music
| &nbsp;Yuki Kajiura
|-
! Theme songs composition
| &nbsp;Yuria Yato
|-
! Art director
| &nbsp;Junichi Higashi &nbsp;Toshihiro Kohama
|-
! Sound director
| &nbsp;Keiichiro Miyoshi
|-
! Character design
| &nbsp;Minako Shiba &nbsp;Satoko Miyachi &nbsp;Satoshi Ohsawa
|-
! Editing
| &nbsp;Aya Hida &nbsp;Kengo Shigemura
|-
! Music Editor
| &nbsp;Yutaka Gouda
|-
! Color coordination
| &nbsp;Eri Suzuki
|-
! Sound Effects
| &nbsp;Takuya Hasegawa
|-
! 3D Director
| &nbsp;Hiroaki Matsuura
|-
! Hokkaido dialect coach
| &nbsp;Hiromi Konno
|}

===Live-action film===
Numerous differences exist between the live action movie and the anime, due to time restraints. In the movie multiple characters are missing, such as Take, Yukari, Nori, and Akemi’s sister Satomi. Shuji never quit the track team, and Fuyumi was never his middle school teacher-–their relationship never goes past playful flirtation. Tetsu is initially hostile to Chise, and sees her as a monster, but he changes his mind and decides to take care of Chise (in contrast to his immediate embrace of Chise in the anime); also, Tetsu’s death is a very brief affair in the live action movie compared to the anime. In the live action movie Atsushi does not enlist in the army for the sake of Akemi, while in the anime he dies clutching a photo of her at his death. Akemi does not die in Shuji’s arms from an earthquake, rather she dies in a hospital from injuries sustained during an air raid; however she still confesses her love to Shuji. Chise and Shuji do not run away to a harbor city in the live action movie, but to an isolated farm house; here Chise is not shown as becoming progressively inhuman, as she was in the anime. Also, the circumstances of the invasion are clear in the live action movie; the invading countries are doing so due to the creation of Chise herself – they also threaten nuclear attack unless Japan destroys Chise by a certain date. Overall, the live action movie does not actively portray the suffering of a besieged Japan: such medicine and food shortages as are shown in the anime are not present in the live action movie. Importantly, the world is not destroyed in the live action movie.

==References==
 

==External links==
 
*  
*    
*    
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 