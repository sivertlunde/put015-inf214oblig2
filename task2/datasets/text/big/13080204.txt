Kid Speed
{{Infobox film
| name           = Kid Speed
| image          = 
| caption        = 
| director       = Larry Semon Noel M. Smith
| producer       = Larry Semon
| writer         = Leon Lee Larry Semon Noel M. Smith
| starring       = Oliver Hardy
| music          = 
| cinematography = Hans F. Koenekamp
| editing        = 
| distributor    = 
| released       =  
| runtime        = 18 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent comedy film directed by Larry Semon and featuring Oliver Hardy.   

==Cast==
* Dorothy Dwan - Lou DuPoise
* James J. Jeffries - Blacksmith/Tailor
* Oliver Hardy - Dangerous Dan McGraw (as Oliver N. Hardy) Frank Alexander - Mr. Avery DuPoys
* William Hauber - Sheriff Phil ODelfa
* Grover G. Ligon
* Larry Semon - The Speed Kid Spencer Bell - The Speed Kids co-driver (not credited)

==See also==
* List of American films of 1924
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 