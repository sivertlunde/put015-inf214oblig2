Sledgehammer (film)
{{Infobox film
| name           = Sledgehammer
| image          = SledgehammerSlasher.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by InterVision Picture Corp
| film name      = 
| director       = David A. Prior
| producer       = Nicolas T. Kimaz
| writer         = David A. Prior
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Ted Prior   Tim Aguilar   Linda McGill   Sandy Brooke   John Eastman   Janine Scheer   Stephen Wright
| music          = Ted Prior   Marc Adams   Philip G. Slate
| cinematography = Salim Kimaz
| editing        = Ralph Cutter
| studio         = I & I Productions
| distributor    = World Video Pictures, Inc.
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $40,000
| gross          = 
}}

Sledgehammer is a 1983 horror film written and directed by David A. Prior.

== Plot ==

In a remote house, an abusive mother locks her young son in a closet, then goes into another room to meet the man she is cheating on her husband with. The man and woman intend to abandon their respective spouses, but their plans are cut short when an unidentified assailant murders them with a sledgehammer. The bodies of the adulterers are found by the authorities, but the boy was not found, and he is presumed dead or escaped out of the house.

Ten years later, seven friends acquire the house to party in, and travel to it in a van, which they leave with a mechanic. Afterwards, they begin to party and have fun inside the house. When night falls, Chuck convinces the others to participate in séance to try and summon the spirits of the couple that died in the house, so they can learn who killed them. The séance is just a means for Chuck and Joey to prank their friends, but it succeeds in bringing forth a spirit, which appears as translucent-masked man. The spirit hides Joeys body after stabbing him in the neck with a carving knife, and the next day, it murderers Jimmy and Carol while they are having sex, bludgeoning the former with a sledgehammer, and snapping the latters neck.

Discovering the fates of their friends, the remaining four teenagers decide to hold up in the house until morning, at which point they will try to reach the nearest town. When the others fall asleep, John grabs a carving knife and goes off in search of the spirit, and finds the boys skull in a closet, a newspaper mentioning his disappearance, and Carol and Jimmys bodies seated at a table with Satanic imagery painted with blood. John is confronted by the spirit, and tries to fight it off, but is stabbed in the back with a chef knife. The spirit then captures Mary, and is found (as the boy) stabbing her to death by Chuck and Joni. The spirit assumes its adult form, wounds Chuck, and goes after Joni.

Joni fends off the ghost long enough for Chuck to recover, and seemingly kills the spirit with its own sledgehammer. As the sun rises, Joni and Chuck flee from the house, not noticing the killers young form glaring at them from one of the windows.

== Cast ==
 Ted Prior as Chuck
* Linda McGill as Joni
* John Eastman as John
* Janine Scheer as Mary
* Tim Aguilar as Jimmy
* Sandy Brooke as Carol
* Stephen Wright as Joey
* Michael Shanahan as Lover
* Mary Mendez as Mother
* Justin Greer as The Boy
* Doug Matley as The Killer
* Ray Lawrence as The Driver

== Reception ==
 cult movies can go really ought to see for themselves. Horrible in every sense of the word and endlessly entertaining for all the wrong reasons".  DVD Verdict described it as "innocently and consistently incompetent that it is hard not love".   Oh, the Horror! (which gave Sledgehammer the tag "Buy it!") said, "Most will call this crap, but others will call it charming. If youre in the latter category, you will find a lot to like, as stuff like this carries a lot of nostalgic currency. And while that doesnt cover up its obvious flaws (poor acting and a non-existent plot) the feeling it exudes is distinctive".  Hysteria Lives! gave Sledgehammer a two and a half out of five, opening its review with "whilst this early shot-on-video oddity certainly isnt going to win any awards it is cheesy (and even sometimes a little creepy) enough fun to keep most fans of the subgenre entertained". 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 