The Jackhammer Massacre
{{Infobox film
| name           = The Jackhammer Massacre
| image          = JackhammerMassacre.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Plus Entertainment
| film name      = 
| director       = Joe Castro
| producer       = Joe Castro   Steven J. Escobar
| writer         = 
| screenplay     = Joe Castro   Daniel Benton
| story          = Joe Castro
| based on       = 
| narrator       = 
| starring       = Kyle Yaskin   Aaron Gaffey
| music          = Jason Frederick
| cinematography = Nick Saglimbeni
| editing        = Steven J. Escobar
| studio         = Slaughterhouse Cinema
| distributor    = Barnholtz Entertainment
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Jackhammer Massacre is a 2004 horror film written and directed by Joe Castro, and co-written by Daniel Benton.

== Plot ==
 crystal meth. Instead of killing Jack, the cocktail of drugs causes him to have a psychotic break, and drilled by paranoid delusions and a hallucination of Mike, he kills the dealers with an electrical jackhammer that was among the tools in the warehouse machine shop.

When Jacks sister Tori and her girlfriend arrive, Jack having called them earlier to ask for money, Jack locks Tori in a storage room, and murders her girlfriend. After killing his boss, who had stopped by to tell him that the building has been sold, Jack has a series of hallucinations, and passes out on the street. In the morning, Jack wakes up, and murders the realtor and buyer his boss was going to meet with, as the cleaning crew hired by the realtor arrives. Jack kills two members of the cleaning crew, incapacitates one with an injection of drugs, and chases the final member of the group, Sam, through the warehouse, and onto the roof.

When the extension cord of the jackhammer becomes unplugged, Jack resorts to trying to strangle Sam to death, but is stopped by the escaped Tori, who tries to reason with him. When talking to Jack fails, Tori knocks him off the roof with the detached bit of the discarded jackhammer. As Jack lies bleeding in the street, the hallucinatory Mike appears, and yells at him to get up. When Sam and Tori exit the warehouse moments later, they both scream when they notice Jack is gone.

== Cast ==

* Aaron Gaffey as Jack Magnus
* Kyle Yaskin as Mike Fletcher
* Nadia Angelini as Sam
* Trudy Kofahl as Tori Magnus
* Jill Moore as Bobbie
* Bart Burson as Zach
* Evan Owen as Brian
* Desi OBrian as Nelson
* Christopher Michaels as Roger
* John Sarley as Darren
* Joe Haggerty as Borris
* Scott St. James as Taylor
* Staas Yudenko as Victor
* Rob Rotten as Max
* Rachel Rotten as Veronica
* Nick Nyon as Carlos
* Kat Stephens as Lila
* Alex Stone as Junkie #1
* Regina Nicole as Dina
* Wali B. Suhail as Co-Worker #1
* David Ortega as Co-Worker #2
* Kitty as Naked Junkie Girl
* Ivy Villalobos as Little Girl
* Sowilla Henry as Voice of 911 Operator/Darlene
* Steven J. Escobar as Voice of Detective Brannon

== Reception ==

The exploitation film database The Worldwide Celluloid Massacre categorized The Jackhammer Massacre as worthless, criticizing the disappointing gore and acting of the supporting cast, though the site did offer mild praise to the plot, and Aaron Gaffeys performance.  The slasher film blog Vegan Voorhees awarded the film one star out of a possible five, writing "The first half hour attempts to build depth into the story, but as soon as the bodies start dropping it all descends into the usual sloppy gorefest, complete with amateur-night performances". The website concluded "on some level its worth admiring Castros passion for the genre, though you might want to ensure youre on some kind of acid trip of your own if you want to wring much enjoyment from this one". 

Critical Condition called The Jackhammer Massacre "dull and listless" and "a gore-soaked, badly-acted snoozefest with no redeeming value".  A lukewarm review was given by DVD Verdict, which said "I can appreciate what the minds behind The Jackhammer Massacre were trying to do. Yes its highly derivative of pretty much every single slasher movie ever made; but for a low-budget gore romp, the flick does a few noteworthy things that elevate it beyond waste-of-time dreck. Unfortunately, some snails-pacing toward the end, along with a few moronic moments, end up dragging the flick down, ultimately sentencing it to the mediocrity bin". 

A score of two out of five was given by Slasherpool, which deemed it a mediocre slasher and dark horror drama, the websites main criticisms being the uninteresting story and poor acting of the supporting cast, and highlights being Aaron Gaffeys performance and Joe Castros direction on a limited budget.  The Video Graveyard gave the film two and half stars, writing the finale was weak and clichéd, but that overall it is probably Joe Castros best effort, due Aaron Gaffeys above average performance, the effective atmosphere of grime and sleaze, the bloody deaths, and commendable anti-drug message ("even if the mix of exploitation and preaching gets a little bit unagreeable at times"). 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 