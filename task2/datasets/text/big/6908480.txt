The Mysteries of Pittsburgh (film)
{{Infobox film
| name = The Mysteries of Pittsburgh
| image = Mysteries of pittsburgh.jpg
| caption = Promotional film poster
| producer = Michael London Rawson Marshall Thurber Pete Chiarelli Jason Ajax Mercer Thor Benander
| director = Rawson Marshall Thurber 
| screenplay = Rawson Marshall Thurber 
| based on =  
| starring = Jon Foster Sienna Miller Peter Sarsgaard Mena Suvari Nick Nolte Theodore Shapiro Michael Barrett
| editing = Barbara Tulliver 
| released =  
| runtime = 95 minutes
| studio = QED International
| distributor = Peace Arch Entertainment Group
| country = United States
| language = English
}}  Pittsburgh ended in October 2006,    with the film set for release in 2008.  It made its world premiere in January 2008 at the Sundance Film Festival.  Set in 1980s Pittsburgh, the film follows the affairs of two young men with one woman, and later also with each other.

==Cast==
*  gangster who gets caught up in a tangled love triangle with Jane and Cleveland.
*Sienna Miller as Jane Bellwether: Clevelands girlfriend. In the novel, Jane is a minor character but is elevated to leading lady in Thurbers adaptation. homosexual character Arthur Lecomte, one of the novels key love interests.
*Mena Suvari as Phlox Lombardi: A strange girl who works at the book shop, who becomes romantically involved with Art. In the novel, she is one of the main romantic interests along with Arthur, but her role is greatly reduced in the adaptation.
*Nick Nolte as Joe Bechstein: Arts father is a Jewish gangster, who is disappointed with his sons choices and would like him to become a stockbroker.

==History==
Earlier attempts to bring Pittsburgh to the big screen failed, including Chabons own try at adapting the novel.  In early 2000, independent filmmaker Jon Sherman (writer and director of 1996s Breathing Room) adapted Chabons novel into a screenplay, hoping to film it with Jason Schwartzman in the role of Art Bechstein.   By August of the same year, Chabon announced that the project had fallen apart, sadly writing, "Ah, well. Maybe someday." 

Director Rawson Marshall Thurber got hold of the project sometime prior to 2006, when he began filming after convincing Michael Chabon he could make it work. Chabon had seen the script, and was pleased with it. In order to facilitate the film adaptation, Thurber removed the character of Arthur Lecomte "whole cloth" from the original Chabon novel in what he described as "the most glaring change". Lecomte, a gay man for whom Art begins to develop feelings, had to be removed because Thurber felt strongly "that in order for the film to function properly, it needed a more efficient and more cinematic engine — in short, a love triangle." Consequently, many elements of Arthur Lecomte were folded into the character of Cleveland.   

==Controversy==
Cast member Sienna Miller created a minor stir in Pittsburgh when, in a 2006 interview with Rolling Stone, she referred to the city as "Shitsburgh", saying, "Can you believe this is my life? Will you pity me when youre back in your funky New York apartment and Im still in Pittsburgh? I need to get more glamorous films and stop with my indie year."  Miller has been parodied in Pittsburgh media (including one article that was headlined "Semi-famous actress dumps on the Burgh")  and criticized for making what was seen as an unnecessarily disparaging remark, given the special treatment the films cast and crew had been given by the visitors bureau and other city offices. Miller soon apologized and said her remarks were taken out of context. She further stated that she was referring to night time shooting schedule of the production, and that what she had seen of the city was beautiful. 

==Reception== normalized rating average score of 38, based on 14 reviews.    Based on 28 reviews collected by Rotten Tomatoes, The Mysteries of Pittsburgh has an overall approval rating from critics of 7% with an average score of 3.9/10.   
 a film in 2000. Morrow was also critical of Thurbers "timid" take on Arts sexual revelation, and Fosters acting. He described Cleveland as a "perfect role" for Sarsgaard, and was praising of Sarsgaards acting, but felt the characters humour was missing. 

Gay City News felt that the adaptation compromised the "very literary" debut novel, and criticized Fosters "cheap voice-over narration" for failing to add any dimension to whats happening on screen. Reviewer Gary M. Kramer was critical of the film for excising the character of Arthur, compressing the character of Phlox and for bringing supporting characters Jane and Cleveland into the foreground, and was critical of Sarsgaards acting. The review concluded that the film "seems a victim of poor planning and bad timing" and was very distant from what he called "the excellent source novel." 

One reviewer for Movies.com noted that this "makes the 1137th film in which Peter Sarsgaard is called upon to play a smug, condescending, somewhat unbalanced, louche, bisexual jerk" and suspected the actor might become   just to get the cash to free himself to make this)".  
 The Informers, novel of the same name. Noting that, while reviews for most films have been mixed, "the decision to see Pittsburgh on April 10 or Informers on April 24 is completely subjective -- do you like your literary adaptations debauched and nihilistic or wistful and bittersweet?" and the reviewer opined that he would probably be seeing both. 

==References==
 
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 