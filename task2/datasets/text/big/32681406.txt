Whoops Apocalypse (film)
{{Infobox film
| name           = Whoops Apocalypse
| image          = WhoopsDVD.jpg
| image_size     = 215px
| caption        = UK DVD cover
| director       = Tom Bussmann
| producer       = Brian Eastman Andrew Marshall & David Renwick
| starring       = Loretta Swit Herbert Lom Peter Cook
| distributor    = ITC Entertainment (UK)  Metro-Goldwyn-Mayer (USA)
| released       = 1986 UK 1988 United States
| runtime        = 93 min
| country        =   English
}}

Whoops Apocalypse is a 1986 ITC Entertainment film, directed by Tom Bussmann. The film shares the same title as the TV series Whoops Apocalypse, but uses an almost completely different plot from the series.

==Plot==
The beginning of the film parodies the Falklands War when a small British colony is invaded by its neighbour, the fictional country of Maguadora, whose dictator General Mosquera is played by Herbert Lom. The new President of the United States, Barbara Adams, tried to sort out the mess (both countries are hard on communism) but the peace talks are sabotaged by Lacrobat (Michael Richards), the worlds leading terrorism|terrorist. The British, under the leadership of PM Sir Mortimer Chris, send in a task force to seize the islands back. For revenge Mosquera hires Lacrobat to kidnap the British Princess Wendy, in order to hold her to ransom to get the British out.  Sir Mortimer then threatens that unless she is returned in 48 hours, he will release a nuclear strike. So now President Barbara Adams not only has to deal with Mosquera and Lacrobat, she must also deal with Sir Mortimer, and also with the fact that Mosqueara decides to align himself with Russia, and this whole thing could start World War III.

Loretta Swit is Barbara Adams, the first female president. She was only sworn in office when the previous president, an ex circus clown (a parody of Ronald Reagans entertainment career), died after asking a journalist to hit him in the stomach with a crowbar as a test of physical strength (a take on the death of Harry Houdini). For a comical satire, Swit plays the role straight. Adams, while trying to maintain the peace is shown to be incompetent, especially when trying to handle questions from the press. Interestingly, we discover her husband runs a weapons company which hired Lacrobat to start the war in the first place.
 Conservative voters crucifying disloyal Wembley Stadium. 
 SAS squad, Soviet soldier who is hiding nuclear weapons on a Caribbean holiday island. 

Other characters include two tabloid journalists who discover the Communist weapons, but are killed; a rear admiral who is openly homosexual (appropriately enough, since "rear admiral" is also apparently slang for a man who practices anal sex), two security guards who accompany President Adams everywhere (even when shes going swimming) and a former president (a parody of Richard Nixon) who is now in prison and authored the book Commie Bastards I Have Known. 

Although Wendy is rescued, Sir Mortimer ignores Adams pleas to call off the nuclear strike. She then calls the rear admiral (who Lacrobat hypnotised to imagine he was in a burning building when fingers are snapped). He ponders calling off the strike, but when a sailor snaps his fingers, he calls "Fire!", the strike is launched and the film ends.

The film reuses some jokes from the series, like Lacrobats absurd disguises (at one time going by the name Dr. Thesius Lyndon Penis), a dying Soviet leader, a crucifixion sight gag, the president being unable to decipher other peoples technobabble, an insane Prime Minister and an overly macho CIA agent with ludicrously complicated plans. There is an SAS sequence in the series as well, and another gay military character. The following mock news story was used in both as well, "A woman who secured a lock of Frank Sinatras hair twenty years ago today sold it back to him for an undisclosed sum."

==See also==
*The Mouse that Roared
*Dr. Strangelove
*Canadian Bacon
*Wag the Dog
*List of films based on British sitcoms

==External links==
* 



 
 
 
 
 