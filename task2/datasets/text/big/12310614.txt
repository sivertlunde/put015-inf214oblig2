The Monastery: Mr. Vig and the Nun
{{Infobox Film
| name           = The Monastery: Mr. Vig and the Nun
| image          = 
| image_size     = 
| caption        = 
| director       = Pernille Rose Grønkjær
| producer       = Sigrid Dyekjær
| music          = Johan Söderqvist
| cinematography = Pernille Rose Grønkjær
| editing        = Pernille Bech Christensen
| distributor    = Koch-Lorber Films
| released       = 
| runtime        = 84 min.
| country        = Denmark Danish
}} 2006 Danish documentary film directed by Pernille Rose Grønkjær. 

==Plot Summary== Danish estate, Russian orthodox monastery. 

After a visit to Russia to negotiate with the Russian Patriarchate|patriarchate, a delegation of nuns headed by Sister Amvrosija come to Denmark in order to assess whether Vigs castle is fit to serve as a monastery. The nuns approve the castle, but at the same time they demand extensive repairs to it. When the nuns leave again for Russia, Vig sets out to do the repairs by himself. 

The following summer Sister Amvrosija and the other nuns return and Vigs dream seems about to come true. The nuns move into the castle, and slowly they take over the daily work and introduce new routines. 

Vigs life changes: all his life he has lived by himself with no women around, now he has to share his home with the strong-willed Sister Amvrosija and her sister nuns. They demand more and more repairs, as a solution, the, the Russian patriarchate offers to pay for all future repairs to the castle, but on condition that Vig leaves his castle to them in his will. 

Vig has serious doubts: should he leave his castle to the Russian church in his will and thus fulfil his dream of creating a monastery? Or should he keep his castle to himself and continue his lonely life as a bachelor? Vig, who has never known love, is facing many conflicts with the apparently difficult Sister Amvrosija, but she does strangely enough not want to leave him and his castle, even if she will be the last and only nun left.

Today the monastery is run by Sister Amvrosija.  The Russian Orthodox priest Hegumen Theofan, Dean of the St. Alexander Nevsky Parish in Copenhagen, often comes to carry out services.  The Russian Patriarchate and the Hesbjerg Foundation are negotiating the future plans for the monastery, and an agreement is well on its way. 

==Awards==

Joris Ivens Award 
Amersterdam, IDFA 2006

Grand Prix 
Chicago International Documentary Festival 2007

Special Mention 
CPH Dox Copenhagen Documentary Festival 2006

Charles E. Guggenheim Emerging Artist Award 
Durham, Full Frame Documentary Film Festival 2007

Grand Jury Award 
Durham, Full Frame Documentary Film Festival 2007

Honorable Mention 
São Paulo Its All True Int. Documentary FF 2007

FIPRESCI Award for Best Documentary 
Sidney Film Festival 2007

Millennium Award for Best Film 
Warsaw, Planete Doc Review 2007

The Kino Magazine Award 
Warsaw, Planete Doc Review 2007

Audience Award 
Moscow International Film Festival 2007

Best Documentary Nominee 
Film Independent Spirit Awards 2008

Outstanding Achievement in International Nonfiction Feature 
Cinema Eye Honors 2008

Outstanding Achievement in Nonfiction Feature Filmmaking Nominee 
Cinema Eye Honors 2008

Outstanding Achievement in a Debut Feature Nominee 
Cinema Eye Honors 2008

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 