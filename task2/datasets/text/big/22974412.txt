Ambiguous (film)
{{Infobox film
| name = Ambiguous
| image = Ambigous (film).jpg
| image_size =
| caption = DVD cover for Ambiguous (2003)
| director = Toshiya Ueno 
| producer = Nakato Kinukawa Kazuhito Morita Kyōichi Masuko
| writer = Hidekazu Takahara
| narrator = 
| starring = Hidehisa Ebata Noriko Murayama Nikki Sasaki Minami Aoyama
| music = 
| cinematography = Yasumasa Konishi
| editing = 
| distributor = Kokuei
| released = December 9, 2003
| runtime = 63 min.
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2003 Japanese Pink film directed by Toshiya Ueno. It was chosen as Best Film of the year at the Pink Grand Prix ceremony.

==Synopsis==
A diverse group of people, all with troubles in their daily lives, meet online and decide to commit suicide together. Isolated from society at large, they form a physical bond as the appointed time approaches.   

==Cast==
* Hidehisa Ebata: "Hanko" (大友寛司)
* Noriko Murayama: "Maria" (Michiko Sano)
* Nikki Sasaki:  "Screen"  (Mamiko Yoshida)
* Minami Aoyama: "Myū" (Kazuki Aikawa)
* Akio Kurauchi: "Innocent" (Yutaka Matsunaga)
* Rumi Hoshino: Hiromi Shimada
* Yōta Kawase: Yukio Sano
* Suzuka Takaki: Miyuki
* Mika Ogawa: Ami
* Rie Sumiyoshi: Matsunagas mother
* Shirō Shimomoto: Shop owner
* Takeshi Itō: Shop worker
* Kinuta: Fat woman

==Critical reception==
Anglophone pink film scholar Jasper Sharp notes that there are two different audiences for contemporary pink films: The traditional pink theater-goer who is generally interested in seeing sex on the screen, and the devotees of pink cinema represented by such publications as P*G magazine and its website.  As with many of Kokueis pink films, Ambiguous did not prove very marketable for the traditional softcore porn audience, in part because of its downbeat subject matter. However when looked at as a film which happens to include sex scenes—Kokueis approach to the pink genre—Sharp writes Obscene Internet Group "stands as one of the most genuinely insightful and of-the-moment films produced within the Japanese independent sector in its year." 

The readers of P*G magazine showed their approval of the film by awarding it Best Film and giving Hidekazu Takahara the Silver Prize for screenplay.  

==Availability==
Like many pink films, Ambiguous has gone under more than one title. Originally released in theatres as Obscene Internet Group: Make Me Come!!, the film was shown at the Pink Grand Prix under the title Ambiguous, the title under which it was also released on DVD in Japan and internationally. The film has also been released on DVD in Japan as Group Suicide: The Last Supper.    Sacrament released the film as "Ambiguous" on English-subtitled Region 2 DVD on 22 February 2006. 

==Bibliography==

===English===
*  
*  
*  

===Japanese===
*  
*  
*  
*  

==Notes==
 

==External links==
*  

 
 
 
 
 

 
 

 
 
 
 
 