Against All (film)
 
{{Infobox film
| name           = Against All
| image          = 
| caption        = 
| director       = Otakar Vávra
| producer       = 
| writer         = Alois Jirásek Milos Václav Kratochvíl Otakar Vávra
| starring       = Zdenek Stepánek
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Against All ( ) is a 1956 Czechoslovak drama film directed by Otakar Vávra.   

==Cast==
* Zdenek Stepánek as Jan Zizka z Trocnova
* Gustav Hilmar as Ctibor z Hvozdna
* Vlasta Matulová as Zdena
* Bedrich Karen as Provost
* Jan Pivec as Zikmund Lucemburský
* Miroslav Dolezal as Jan Bydlinsky
* Václav Voska as Petr Kanis
* Jana Rybárová as Marta
* Petr Hanicinec as Ondrej z Hvozdna
* Stanislav Neumann as Sakristian
* Jaroslav Vojta as Simon

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 