Sunday Calm
{{Infobox film
| name           = Sunday Calm
| image size     =
| image	=	Sunday Calm FilmPoster.jpeg
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 20th Our Gang short subject released.

==Synopsis==
The gang travels by wagon to go on a picnic with their families. After losing a wheel, the parents put on a new one, and the gang races off with the wagon, leaving the parents behind. They have a picnic without the parents, only to be overrun with insects.

==Cast==

===The Gang===
* Joe Cobb as Joe Tucker
* Jackie Condon as Jackie Tucker
* Mickey Daniels as Mickey McTeeter Jack Davis as Jack Tucker
* Allen Hoskins as Farina
* Ernest Morrison as Ernie
* Leona Levin as Mickeys sister

===Additional cast===
* Richard Daniels as Mr. Tucker
* Helen Gilmore as Mrs. McTeeter
* Clara Guiol as Mrs. Tucker

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 