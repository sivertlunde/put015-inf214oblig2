Thinkalaazhcha Nalla Divasam
{{Infobox film name           = Thinkalaazhcha Nalla Divasam image          = Thinkalaazhcha Nalla Divasam.jpg director       = P. Padmarajan writer         = P. Padmarajan starring       = Mammootty Kaviyoor Ponnamma Karamana Janardanan Nair Srividya producer       = M. Mani music  Shyam
|cinematography = Vasant Kumar editor         = B. Lenin studio         = Sunitha Productions distributor    = New Aroma Release released       =   runtime        = 126 minutes country        = India language       = Malayalam gross          = 
}}
Thinkalaazhcha Nalla Divasam ( ) is a 1985 Malayalam drama film written and directed by P. Padmarajan. It stars Mammootty, Kaviyoor Ponnamma, Karamana Janardanan Nair and Srividya in pivotal roles.    It tells the story of a man who wants to sell his ancestral house and put his mother in an old age home. It won the National Film Award for Best Feature Film in Malayalam.

==Plot==

Janakiyamma ( Kaviyoor Ponnamma) stays in a spacious ancestral house with the help of a distant relative girl and servants who help her take care of her cows and poultry.  Her elder son is Narayanankutty(Karamana Janardanan Nair), married to Ambika(Srividya) both central government servants in Bombay(Mumbai).  They have two girls Sheenu(Kuku Parameswaran) and Meenu.  Her younger son Gopan(Mammootty) is married to Bindu(Unnimary ) and is working in the Middle East.  She also has a deceased daughter (not shown in the movie) and her son is Venu(Ashokan (actor)|Ashokan). 
The story starts with the children and grandchildren arriving at their ancestral house to celebrate the mothers 60th birthday.  Janakiyamma is very happy with everybodys enthusiasm and thinks everyone is genuinely enjoying their stay at the house.  Meanwhile Gopan and Bindu have paid advance for a flat in Bangalore and wants to get their share of the ancestral property(the house in which Janakiyamma lives) to be sold to pay the rest of the amount.  Narayanankutty and Ambika come to know about this and are shown upset at uprooting the mother.  Narayanankutty is helpless and disappointed because they have not yet bought a house for themselves in Bombay and is visibly envious of the prosperity of his brother.  One night they have a visitor Kochu/Kunju(Achankunju) who was their fathers old servile.  He is now well off and has 4 sons in the Middle East.  He offers to buy the property, which upsets Narayanankutty because it is Kochu who is the buyer.  Bindu has bought a TV for Amma(mother) and tries to project to her that Amma is not safe alone in that house.  Janakiyamma overhears Bindu and Ambika discussing about selling the house and waiting for Amma to die. She is heartbroken and accepts the house to be sold.  She also agrees to go to an old age home suggested by Bindu and Gopan.  She refuses to go with Narayanankutty and Ambika. 
She goes to the old age home and passes away the next day. 
Gopan, now with only his family in the house waiting to be sold has nightmares. He feels really guilty of his deeds and loses his peace of mind. He decides not to sell the house his mother loved so much and decides to spend the rest of his life there.

The film depicts how materialistic people become given the circumstances and how their past does not have any value in such peoples lives. 
There is also a side track of on and off love angle between Sheenu and Venu and Gopans girls enjoying the stint in the ancestral house running around with livestock, bathing in ponds etc.  The little girls finally force their father to stay back making his decision to stay in his ancestral house easier. 
Sheenu, brought up in Mumbai initially does not like the relative girl at the house because of her over friendly approach.  She later comes to know in an embarrassing moment from Venu that she understands all the bad words Sheenu used in English because the girl is more educated than her. Padmarajan with his skillful narrative clearly shows how the urban people generally consider all villagers as primitive.

The film gives viewers a clear perspective of the cultural shift that happened in the late 70s to early 90s in India when nuclear families took over from joint families. The film is appreciated for good performances from most of the cast especially Karamana Janardanan Nair.

==Cast==
* Kaviyoor Ponnamma as Janakikutty
* Mammootty as Gopan
* Karamana Janardanan Nair as Narayanankutty
* Srividya as Ambika
* Unnimary as Bindu Ashokan as Venu
* Achankunju as Kunju
* Kukku Parameshwaram as Sheenu
* Ramachandran

==Soundtrack==  Shyam and lyrics was written by Chunakkara Ramankutty. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Panineerumaay || Vani Jairam || Chunakkara Ramankutty || 
|} 

==References== 
 

== External links ==
*  
*  

 
 

 
 
 
 
 