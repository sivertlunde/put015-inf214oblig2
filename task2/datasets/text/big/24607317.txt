A Lesson of Belarusian
{{Infobox film
| name           = A Lesson of Belarusian
| image          = A Lesson of Belorussian poster.jpg
| image_size     = 
| alt            = 
| caption        = Promotional poster
| director       = Mirosław Dembiński
| producer       = Mirosław Dembiński
| writer         = Mirosław Dembiński
| narrator       = 
| starring       = 
| music          = 
| cinematography = Michał Ślusarczyk Maciej Szafnicki
| editing        = Mirosław Dembiński
| studio         = 
| distributor    = 
| released       =  
| runtime        = 53 min
| country        = Belarus/Poland Polish Belarusian Belarusian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 2006 documentary Polish director highly controversial presidential re-election of Alexander Lukashenko on March 19, 2006.  The film has won multiple festival prizes. 

== Plot == Franak Viačorka, Belarusian Popular Front, prepares with his friends for the run-up to the 2006 presidential election. Their activities range from handing out newspapers, organising rock concerts, distributing flyers, composing protest songs and interviewing the opposition candidate, Alaksandar Milinkievič. Aside from the main theme of the elections, it also touches briefly on the incarceration of Franaks father, Vincuk Viačorka, a professor at the banned Belarusian Humanities Lyceum, where Franak and many of his friends were students.
 take to the streets in protest, many of them setting up a tent city in October Square in Minsk. By the night of March 23, riot police are deployed to clear the remaining protesters.
 Belarusian Independence Day riot police clash violently with anti-Lukashenko demonstrators, detaining several, among them Alaksandar Milinkievič. By the end of the film, Franaks father has been released from detention, the battle has temporarily been lost, but the peoples spirit is unquenched. According to Franak, the end is nigh for Lukashenko.

==References==
 

==External links==
* 

 
 
 
 