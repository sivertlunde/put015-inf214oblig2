Life Is Beautiful (2014 film)
{{Infobox film
| name           = Life Is Beautiful
| image          = Life is Beautiful! 2014 poster.jpg
| alt            = 
| caption        = Promotional Poster
| director       = Manoj Amarnani
| producer       = M.A.A. Productions
| distributor    = Shotformats Digital Production 
| released       =  
| country        = India
| language       = Hindi
| budget         =
| gross          = 
}}
 2014 Bollywood film that was directed by Manoj Amarnani, who also starred in the movie.   The film was released in India on 22 August, 2014 and stars Nancy Brunetta as Amarnanis co-star. 
== Synopsis ==

Raj works in Toronto on a temporary visa and is living with his friend Prem, who resents Raj for invading what little living space he has. When Rajs application for permanent residency in Canada is denied, he decides to get married to a complete stranger in hopes of getting approved. The woman, Pia, finds that she has to move in with Raj to make everything look legit, but as time passes the two find themselves becoming friends. However all of Rajs plans are potentially ruined when he meets Linda, a woman who makes him doubt his previous actions.

== Cast ==

* Manoj Amrnani for Raj
* Nancy Brunetta for Linda
* Anokhi Dalvi for Pia
* Parth Naik for Prem
* Master Aaryan for Rahul
* Jayiesh Singh for Rohit
* Sunaina Verma for Meghina
* Arjun Jasuja for Keswani
* Shaibal Datta for Jai

* Sanj Nagu
* Manoj Amarnani 
* Anokhi Dalvi
* Raj Zutshi

== Singers ==

* Udit Narayan
* Alka Yagnik
* Sonu Nigam
* Shreya Ghoshal
* Rahat Fateh Ali Khan
* Richa Tirupati
* Farhad
* Nikhil DSouza

== Reception ==
 The Proposal and What Happens in Vegas and gave the movie two stars. 
== References ==

 

== External links ==

* 
*  

 
 
 

 