A Woman's Face (1938 film)
{{Infobox Film  
| name           = A Womans Face 
| image          = en_kvinnas_ansikte.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Gustaf Molander
| producer       = 
| writer         = Screenplay: Gösta Stevens,  
| starring       = Ingrid Bergman
| music          = Eric Bengtson
| cinematography = Åke Dahlqvist
| editing        = Oscar Rosander
| distributor    = 
| released       = Venice Film Festival: August 1938 Sweden: 31 October 1938 United States: 8 September 1939
| runtime        = 101 min .
| country        = Sweden
| language       = Swedish
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1938 Cinema Swedish drama film directed by Gustaf Molander, based on the play Il etait une fois by Francis de Croisset. The cast includes Ingrid Bergman in the lead as a woman criminal with a disfigured face.
 1938 Venice 1941 by the same title.

==Cast==
* Ingrid Bergman as Anna Holm, aka Anna Paulsson
* Tore Svennberg as Magnus Barring
* Anders Henrikson as Dr. Wegert
* Georg Rydeberg as Torsten Barring
* Gunnar Sjöberg as Harald Berg
* Hilda Borgström as Emma
* Karin Kavli as Mrs. Wegert
* Erik Bullen Berglund as Nyman
* Sigurd Wallén as Miller
* Gösta Cederlund as The Count
* Magnus Kesster as Handsome Herman
* Göran Bernhard as Lars-Erik Barring
* Bror Bügler as Georg Mark

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 