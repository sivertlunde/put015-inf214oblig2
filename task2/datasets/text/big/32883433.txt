Veedu (1988 film)
{{Infobox film
| name           = Veedu
| image          = 
| director       = Balu Mahendra
| producer       = Kaladas
| story          = Akhila Mahendra
| writer         = Balu Mahendra Archana Bhanuchander
| music          = Ilaiyaraaja
| cinematography = Balu Mahendra
| editing        = Balu Mahendra
| studio         = Sri Kala International
| distributor    = 
| released       = 1988
| runtime        = 108 mins.
| country        = India
| language       = Tamil
}} 1988 Tamil Archana as Best Tamil Film. The film was also screened at the "Indian Panorama" of the International Film Festival of India in 1988. Inspite of winning critical acclaim the film failed at the box-office. The film is widely considered to be one of the best films of Balu Mahendra.

==Plot==
Sudha, a 22-year old unmarried woman, lives in an apartment in the Indian city of Madras with her younger sister (Indhu) and grandfather (Murugesan). She is engaged to her her colleague Gopi. The owner of the tenement wants all the families to vacate the place as he intends to construct another building on the same site. Sudhas attempts for searching an alternative house turn out to be unsuccessful as her family could not afford the rents fixed by them. At this juncture, she is advised by Iyengar, one of her colleagues, to start constructing a new house. Though initially hesitant, she decides to build a house after Iyengar motivates her. Sudhas grandfather already owns two construction sites in the outskirts of the city. They decide to construct the house in one piece of the land and sell the other one to raise money. In addition, she also seeks a housing loan and pledges her jewels for money. During this time, Sudha gets introduced to a building contractor who drafts a plan for the house. To get the plan approved they had to bribe a government official. By this time, unexpected rains halt the construction thereby inflating the cost. Though Gopi comes forward to provide financial assistance, Sudha declines it stating that his sisters marriage would get affected because of that.

A kind-hearted Mangamma, a construction worker, helps Sudha in the construction. When she finds that the contractor steals materials from the site, she informs Sudha which in turn leads to the contractor quitting. Mangamma along with her superviser, and Iyengar help Sudha during this time as the construction work resumes. Meanwhile, as the sanctioning of loan gets delayed, Sudha seeks monetary help from her  superior. She becomes even more dejected only to know that he is a womanizer. With all her funds exhausted, she decides to halt the construction. But Gopi offers some assistance and the work continues to progress further. An excited Murugesan visits the site alone on a Sunday and passes away while returning home. Sudha becomes depressed over the situation. When the house is nearing completion, and when Sudha and Gopi appraise each other, an official from the "Metropolitan Water Authority" lands at the site and asks them how can they construct a house in a land which was already acquired by the authority. In the end, Sudha is shown sitting in the court seeking justice.

==Cast== Archana as Sudha
* Bhanu Chander as Gopi
* Chokkalinga Bhagavathar as Murugesan
* Sathya as Mangamma
* Indhu as Indhu
* Senthamarai
* Veera Raghavan
* Ralapalli
* Oru Viral Krishna Rao Chandra Mohan
* Nair Raman

==Production== International Year of Shelter. 

Made on a shoestring budget, the films central theme revolved around the struggle of a middle-class family to build a house. It also marginally focussed on the change in human behaviour and corruption.  The story was credited to Balu Mahendras wife Akhila Mahendra, while he wrote the screenplay, filmed and edited apart from directing the film. The film does not feature any soundtrack.  Ilaiyaraaja, a Balu Mahendra regular, was signed as the composer. On the latters request, he used portions of his composition from the album How To Name It? for the film score.       Most of the sequences were narrated through jump cuts and Montage (filmmaking)|montages, shot mostly using a hand-held camera. 

==Reception and legacy== National Film Best Feature Best Actress for Archana.  The film was among the 16 films to be screened at the Indian Panorama section of the International Film Festival of India in 1988.  In 2002, the film was screened at the "Indian Summer" section of the Locarno International Film Festival. 
 negative of the film is lost.   

==Notes==
 

==References==
 
* 
* 
* }}
* }}
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 