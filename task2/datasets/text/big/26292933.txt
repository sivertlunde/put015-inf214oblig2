Guns Girls and Gangsters
 

{{Infobox film
| name           = Guns, Girls and Gangsters
| image          = Gunsgirlsgangsters.JPG
| image_size     =
| caption        =
| director       = Edward L. Cahn
| producer       = Robert E. Kent Edward Small (executive)
| writer         = Paul Gangelin Jerry Sackheim Robert E. Kent
| narrator       =
| starring       = Mamie Van Doren Gerald Mohr Lee Van Cleef Grant Richards
| music          = Buddy Bregman
| cinematography = Kenneth Peach
| editing        = Fred R. Feitshans Jr.
| distributor    = United Artists
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
}}
Guns, Girls and Gangsters is a 1959 B-movie crime/drama starring Mamie Van Doren.

==Plot==
Chuck Wheeler is released from prison and begins to set up an elaborate heist of an armored truck carrying money from a Las Vegas casino. Chuck enlists the help of nightclub owner Joe Darren as well as Vi Victor, a sensational blonde who is married to Chucks ex-cellmate Mike Bennett. Mike is a very jealous and dangerous man who will not grant Vi a divorce. He escapes from prison just before the armored truck robbery is set to take place and starts to cause havoc when he locates Chuck, Joe and his unfaithful wife.

==Cast==
*Mamie Van Doren as Vi Victor
*Gerald Mohr as Chuck Wheeler
*Lee Van Cleef as Mike Bennett
*Grant Richards as Joe Darren Elaine Edwards as Ann Thomas John Baer as Steve Thomas
*Paul Fix as Lou Largo

==Production==
Robert E. Kent produced the film for Edward Small. Kristin Seen as Challenge: Kaufman Phones Terry Moore; Diamonds Polished for Laage
Scheuer, Philip K. Los Angeles Times (1923-Current File)   21 Nov 1957: C11.   Filming started April 1958. Chance of a Lifetime: Ricky to Be Gunman in Rio Bravo
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   26 Apr 1958: 17. 

==References==
 
*  (credits, film synopsis).

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 

 