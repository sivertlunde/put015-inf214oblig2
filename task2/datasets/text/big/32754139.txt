Interzone (film)
 
{{Infobox film
| name           = Interzone
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Deran Sarafian
| producer       = Joe DAmato
| writer         = Rossella Drudi, James L. Edwards
| screenplay     = Claudio Fragasso, Deran Sarafian
| story          = 
| based on       =  
| narrator       = 
| starring       = Bruce Abbott, Beatrice Ring, Teagan Clive
| music          = Stefano Mainetti
| cinematography = Gianlorenzo Battaglia
| editing        = Kathleen Stratton
| studio         = Filmirage
| distributor    = Star Classics Video, Trans World Entertainment
| released       = 1987  (Bracciano, Rome, Italy)  
| runtime        = UK: 88 min USA: 97 min
| country        = Italy
| language       = English
| budget         = 
| gross          = 
}}
Interzone is a 1987 Italian-American Science fiction|sci-fi/action film directed by Deran Sarafian, with original music composed by Stefano Mainetti, starring Bruce Abbott, Beatrice Ring, and Teagan Clive.  It was produced by Filmirage in Bracciano,   northwest of Rome, and is set in a Apocalyptic and post-apocalyptic fiction|post-apocalyptic future.

The film is plagued by a shoe-string budget, amateurish filming, editing, audio dubbing, terrible acting and dialog. The film was distributed worldwide in VHS format by Trans World Entertainment and Star Classics Video (USA only).

==Plot==
A supernaturally gifted monk, "Panasonic" (Kiro Wehara), is sent on a mission by his dying master, "General Electric," to protect the Interzone, the last fertile region left on a post-apocalyptic Earth, against an invading gang of wasteland raiders.

Along the way, Panasonic is helped by Swan (Bruce Abbott), a roguish road warrior who seeks a rumored treasure hidden within the Interzone, and Tera (Beatrice Ring), an attractive slave girl, who Swan falls in love with. The raiders meanwhile are led by Mantis (Teagan Clive), a female body-builder dominatrix and her sadistic partner Balzakan (John Armstead).

After the defeat of the raiders, Swan locates the treasure which is revealed to be a fallout shelter turned archive of some of mankinds greatest achievements. Within are various items such as books, sculptures and paintings, along with a Panasonic-brand videocassette recorder that plays a final message from those who preserved the artifacts before the apocalypse.

==Cast==
Cast (in credits order):
* Bruce Abbott as Swan
* Beatrice Ring	as Tera
* Teagan Clive as Mantis
* John Armstead as Balzakan
* Kiro Wehara as Panasonic
* Alain Smith as Dwarf
* Franco Diogene as Rat

Rest of cast listed alphabetically:
* Laura Gemser as Panasonics Brothers wife (uncredited)
* Deran Sarafian

==References==
 
 

==External links==
* 

 
 
 


 
 