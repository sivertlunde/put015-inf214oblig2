Coffee Town
{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = Coffee Town 
| image          =
| alt            = 
| caption        = 
| director      = Brad Copeland
| producer       = {{Plainlist|
*Ricky Van Veen
*Kathryn Deen
*Jon Cohen
}}
| writer      = Brad Copeland
| starring       = {{Plainlist|
*Glenn Howerton Steve Little
*Ben Schwartz
*Adrianne Palicki 
*Josh Groban
}}
| original music by        = The Wellspring
| cinematography = Anthony B. Richmond
| editing        = Ned Bastille
| distributor    = Filmbuff (USA) 
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $2,000,000 (estimated)
}}
Coffee Town is a 2013 comedy film. The first feature film from CollegeHumor, it was released on July 9, 2013 on digital platforms and in select theatrical engagements, leading up to a festival premiere on July 27, 2013 at Just For Laughs in Montreal.   

==Plot== Steve Little) and local police officer Gino (Ben Schwartz) – to save his office. In order to thwart the plans of Coffee Towns corporate owners, the trio plan to stage a robbery, creating the illusion of a crime-riddled neighborhood that is not suitable for a bistro. However, standing in Wills way is Sam (Josh Groban), a disgruntled barista with big dreams of being a rock star. Meanwhile, Will tries his best to court his coffee house crush Becca (Adrianne Palicki), an ER Trauma Nurse.   

==Cast==
*Glenn Howerton as Will Steve Little as Chad
*Ben Schwartz as Gino
*Adrianne Palicki as Becca
*Josh Groban as Sam
*Jake Johnson as Wills former roommate
*Josh Perry as Toby, a man with Down Syndrome
*Matthew Riedy as Mr. Ryan, Chads Boss

==References==
 

==External links==
* 
* 
* 
*Coffee Town Official Website  
*Coffee Town Official Trailer  

 
 

 