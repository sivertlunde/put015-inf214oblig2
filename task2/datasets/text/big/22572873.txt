Basanti (2008 film)
 
{{Infobox Film
| name           = Basanti
| image          = 
| image_size     = 
| caption        = 
| director       = Hassan Askari
| producer       = Altamash Butt.
| writer         = Khaakan Haider
| photography  	 = 
| lyrics         = Ahmad Anees
| starring       = Shaan (Pakistan actor)|Shaan, Saima, Shamyl Khan
| music          = Tahir Khan 
| cinematography = 
| editing        = 
| released       =
| runtime        = 
| country        = Pakistan Punjabi
| budget         = 
| domestic gross = 
| preceded_by    = 
| followed_by    =
}}

Basanti is a Punjabi film by Altamash Butt.

==Synopsis==
The movie shows culture and its values, expanding the facts and addressing social evils in our side of the world. The story revolves around a woman who raises her voice against tyranny. Basanti is the story of a very average woman, one who is taken for granted. Basanti is a wall that stands solid against the gales of human wrongdoings.

==Reception==
The film was hold the record the compeletion of a film project in the shortest time, and incredibley it was a hit at the box office.

==Cast==
* Saima
* Shaan Shahid
* Mustafa Qureshi
* Safqat Cheema
* Shamil Khan
* Irfan Khoosat
* Raheela Agha

 
 
 


 