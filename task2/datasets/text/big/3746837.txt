The Comancheros (film)
 
{{Infobox film
| name           = The Comancheros
| image          = Comancheros1961.jpg
| caption        = DVD cover
| director       =  
| producer       = George Sherman
| screenplay     = James Edward Grant Clair Huffaker 
| based on       =  
| starring       = John Wayne Stuart Whitman Ina Balin Lee Marvin Nehemiah Persoff Bruce Cabot
| music          = Elmer Bernstein
| cinematography = William H. Clothier
| editing        = Louis Loeffler
| distributor    = 20th Century Fox
| released       = November 1, 1961  
| runtime        = 105 min. 
| country        = United States
| language       = English
| budget = $4,260,000 
}}
 Western DeLuxe Deluxe CinemaScope Bob Steele, Guinn "Big Boy" Williams and Harry Carey, Jr. in uncredited supporting roles.
 The Adventures of Robin Hood) from finishing the film, Wayne took over as director, though his role remained uncredited. Curtiz died shortly after the film was completed.

==Plot== Texas Ranger Jake Cutter (John Wayne) after a tryst with a mysterious lady, Pilar Graile (Ina Balin). Regret manages to escape, but is subsequently recaptured after a chance encounter with Cutter in a saloon.

In the process of returning Regret to Louisiana, Cutter is forced to join forces with the condemned to fight the "Comancheros", a large criminal gang headed by a former Confederate officer that smuggles guns and whiskey to the Comanche Indians to make money and keep the frontier in a state of violence.  Cutter stops at a ranch owned by a friend when there is a sudden Comanche attack. During the attack Regret hops on a horse and flees, but instead of making a clean getaway he soon returns with a unit of Texas Rangers and the attack is repulsed.

Eventually they infiltrate the self-sufficient Comanchero community at the bottom of a valley in the desert.  Pilar reappears as the daughter of the wheelchair-bound but ruthless leader Graile (Nehemiah Persoff).  After Cutter and the other Texas Rangers defeat both the Comanches and Comancheros, Regret and Pilar leave together for Mexico and Jake rides off into the sunset.

==Production==
  The Diary of Anne Frank and sold the film rights to Fox for $300,000. Clair Huffaker was signed by the studio to adapt it for producer Charles Brackett with Gary Cooper to star. However, Cooper was in ill health and in early 1961 Douglas Heyes was announced as writer and director. John Wayne and Charlton Heston were announced as stars, but Heston dropped out and was replaced by Tom Tryon, then Heyes dropped out and was replaced by Michael Curtiz. Fox had the script rewritten by Waynes regular writer James Edward Grant. Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 75-76 

Whitmans character&mdash;Paul Regret&mdash;was the lead in the novel and Waynes part had to be amplified for the film version. Wellman had envisioned Cary Grant as Regret as he wrote the novel. Gary Cooper and James Garner were originally set to be the leads but Cooper was in ill health and Garner had been blackballed due to a dispute with Jack Warner. 

According to Tom Mankiewicz, who worked on the film as an assistant, Curtiz was often ill during production and John Wayne would take over direction.   

==Cast==
 
* John Wayne as Capt. Jake Cutter
* Stuart Whitman as Paul Regret
* Ina Balin as Pilar Graile
* Nehemiah Persoff as Graile
* Lee Marvin as Tully Crow
* Michael Ansara as Amelung
* Bruce Cabot as Maj. Henry
* Joan OBrien as Melinda Marshall
* Richard Devon as Estevan 
* Jack Elam as Horseface
* Henry Daniell as Gireaux 
* Edgar Buchanan as Judge Thaddeus Jackson Breen Guinn "Big Boy" Williams as Ed McBain (gunrunner)
* Leigh Snowden as  Evie – Blonde in Hotel Room (uncredited) 
* Patrick Wayne as Tobe (Texas Ranger)
 

==Reception==
Bosley Crowther  called the film "so studiously wild and woolly it turns out to be good fun"; according to Crowther, " heres not a moment of seriousness in it, not a detail that isnt performed with a surge of exaggeration, not a character that is credible."   

==Anachronisms==
  Winchester lever Colt Peacemaker Guinn Williams character is said to have stolen rifles from Fort Sill and to have served a sentence in the Yuma Territorial Prison, neither of which became operational until after the Civil War, 1869 and 1876 respectively. The Comanchero leaders status as a former Confederate officer is also anachronistic due to the fact that the Confederate States of America did not exist until the onset of the Civil War in 1861.

==Trivia==
 Rio Conchos, another film which had a remarkably similar plot to this one. In that film (which avoided the anachronisms of this film by being set after the Civil War), Whitmans character was the upstanding figure compelled to work with rogues who either had criminal pasts, or worked on the edge of the law.

==See also==
* John Wayne filmography

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 