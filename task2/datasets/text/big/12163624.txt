Brass Target
{{Infobox film
| name           = Brass Target
| image size     =
| image	=	Brass Target FilmPoster.jpeg
| caption        = John Hough
| producer       = Berle Adams Arthur Lewis
| writer         = Frederick Nolan (novel) Alvin Boretz
| narrator       =
| starring       = Sophia Loren John Cassavetes George Kennedy Robert Vaughn Max von Sydow
| music          = Laurence Rosenthal
| cinematography =
| editing        =
| distributor    = MGM
| released       = 1978
| runtime        = 111 min.
| country        = United States English
| budget         =
| gross          =$5,111,000(domestic)
| preceded by    =
| followed by    =
}} John Hough. It stars John Cassavetes, Robert Vaughn, George Kennedy, Patrick McGoohan and Max von Sydow. 

The film revolves around the actual historical event of Gen. George S. Pattons fatal automobile crash. It suggests it was not an accident but a conspiracy.

==Plot== US Army MP guards are killed in a tunnel using poisonous gas.
 OSS Major Joe De Luca (played by Cassavetes). It seems the robbers used his plan from one of his wartime operations to steal the gold.

This prompts De Luca to start his own investigation. His first stop is to see his old wartime commander, Colonel Mike McCauley (played by McGoohan), who is now living in a requisitioned German castle.

Meanwhile, as the investigation gets closer, the corrupt American officers hire Webber, a professional Assassination|assassin, (played by Von Sydow) to kill Patton in the hope of halting the inquiry.

Soon De Luca meets Mara, a former girlfriend (played by Loren), who can help him find the culprits. But before they can do that, they discover Webber is on their trail and also planning to kill Patton. The pair then race against time across war-ravaged Europe to save the general and catch the villainous officers. However, Webber, posing as an American soldier, kills General Patton in a staged traffic accident. At the precise moment an Army truck collides with Pattons car, Webber fires a rubber bullet, striking Patton the head, killing him. De Luca, however, tracks down the assassin, and at films end, kills him with his own weapon.

==Cast==
* Sophia Loren		: Mara
* John Cassavetes		: Major Joe De Luca
* George Kennedy		: General Patton
* Robert Vaughn		: Colonel Rogers
* Patrick McGoohan          : Colonel Mike McCauley
* Bruce Davison             : Colonel Dawson
* Edward Herrmann           : Colonel Gilchrist
* Max von Sydow             : Professional Assassin Shelley / Martin Webber
* Ed Bishop                 : Colonel Stewart
* Lee Montague              : Lucky Luciano
* Bernard Horsfall          : Shelley
* John Junkin               : Carberry

==Filming locations==
The film was shot on location in Munich, Bavaria|Bavaria, Germany and Switzerland.

The casting director, Munich resident Val Geist, used many US Soldiers from the 66th MI Group who were stationed at the time in Munich, Germany as extras in the film.

==Production Notes==
The film, despite a lukewarm reception on release, is noted for its attention to historical detail in an early post-war Europe. The Cold War had not started but relations with the Russians are shown to be already frosty. There are displaced persons camps, refugees and POWs as well as the remnants of war, such as bombed out buildings and destroyed armored vehicles and equipment. The notion that Patton was assassinated in a staged accident follows the story line of the novel The Algonquin Project upon which the movie was based, and was considered by most to be purely fictional. But a ground-breaking investigative book entitled Target: Patton: The Plot to Assassinate General George S. Patton by Robert K Wilcox, published in 2008, lends credence to the idea.

==Notes==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 