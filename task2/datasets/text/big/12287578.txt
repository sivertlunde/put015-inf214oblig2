Bal-Can-Can
{{Infobox film
| name           = Bal-Can-Can
| image          = BalCanCan2005Poster.jpg
| caption        = Italian poster
| director       = Darko Mitrevski
| producer       = Darko Mitrevski Alessandro Verdecchi Gianluca Curti Loris Curci Robert Naskov
| writer         = Darko Mitrevski
| screenplay     = 
| story          = 
| based on       =  
| starring       = Vlado Jovanovski Adolfo Margiotta Zvezda Angelovska
| music          = Kiril Džajkovski
| cinematography = Suki Medencevic
| editing        = Giacobbe Gamberini
| studio         = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Macedonia Italy
| language       = Macedonian Italian Serbian Bulgarian Croatian Bosnian Albanian English Russian
| budget         = 1,300,000 €
| gross          = 
}}
 transliterated Bal-Kan-Kan) deserter who travels throughout the Balkans as a political immigrant in search of his dead mother-in-law who is wrapped in a carpet.

==Cast==
{| class="wikitable"
|-
! style="background-color:silver;" | Actor
! style="background-color:silver;" | Role
|- 
| Vlado Jovanovski || Trendafil Karanfilov
|-
| Adolfo Margiotta || Santino Genovese
|-
| Zvezda Angelovska || Ruža Karanfilova
|-
| Branko Đurić || Šefket Ramadani
|-
| Seka Sablić || Zumbula
|-
| Toni Mihajlovski || Džango
|- Miodrag Krivokapić || Veselin Kabadajić
|-
| Nikola Kojo || Osman Rizvanbegović
|}

==Release==

===Reception===
The film was mostly praised by critics with some reviewers, such as Dennis Harvey of Variety Magazine, commenting on the film; "Writer-helmer Darko Mitrevski keeps pushing the envelope... The cynical, hallucinatory, modern Pilgrims Progress is a trip, with memorably out-there sequences sure to build a cult rep among adventuresome cineastes."

===Box office===
The film was the highest-grossing film to date in the Republic of Macedonia. It was also released in Russia, United Kingdom, Spain, Turkey, Greece, Ukraine, Serbia, Croatia, Bulgaria, Slovenia, Bosnia and Herzegovina.

==Awards and nominations==
{| class="wikitable"
|-
!Event !! Award !! Winner/Nominee !! Result
|- 27th Moscow International Film Festival    Special Mention of the Film Critics Guild of Russia Darko Mitrevski Won
|- Golden St. George Darko Mitrevski Nominated
|- Motovun Film Festival Propeller of Motovun - From A to A Award, Best Film In the South-East European Region Darko Mitrevski Won
|}

==See also==
List of Macedonian films

==References==
 

== External links ==
*  
* 

 
 
 