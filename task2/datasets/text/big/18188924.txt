The Wild Bees
{{Infobox film
| name           = The Wild Bees
| image          = Wild Bees (film poster).jpg
| image size     =
| alt            =
| caption        = Film poster
| director       = Bohdan Sláma
| producer       =
| writer         = Bohdan Sláma
| narrator       =
| starring       =
| music          = Miroslav Simácek
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2001
| runtime        =
| country        = Czech Republic
| language       = Czech
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Czech film directed by Bohdan Sláma. It was the Czech Republics submission to the 75th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.   

==See also==
 
*Cinema of the Czech Republic
*List of Czech submissions for Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 

 