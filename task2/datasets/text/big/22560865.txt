Oseam (1990 film)
{{Infobox film 
| name         = Oseam
| image        = File:Oseam(1990)_poster.jpg
| caption      = 
| writer       = Jeong Chae-bong 
| starring     = Kim Hye-soo Shim Jae-rim Seo Yeo-jin Jo Sang-geon Choi Jong-won Kim Yong-rim Jo Hyeong-gi Nam Po-dong Song Ok-sook Chun Ho-jin
| director     = Park Chul-soo 
| producer     = Lee Tae-won 
| music        = Lee Jong-gu 
| distributor  = 
| released     =  
| runtime      = 115 minutes
| country      = South Korea
| language     = Korean
| budget       = 
| film name      = {{Film name
 | hangul       =  
 | rr           = Oseam
 | mr           = Oseam}}
}} 1990 South Korean movie directed by Park Chul-soo. It tells the story of two orphans based on a legend in which a five-year-old boy sacrificed himself to open his blinded sisters eyes.  The general theme deals with reconciliation between Buddhism and Catholicism.   

==See also==
*List of Korean language films
*Cinema of Korea
*Contemporary culture of South Korea
*List of Korea-related topics

==References==
 

==External links==
* 
*  at Cine21  

 
 

 
 
 
 