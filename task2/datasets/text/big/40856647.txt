Phone Swap
{{Infobox film
| name               = Phone Swap
| image              = Phone_Swap_Theatrical_Poster.jpg
| alt                = 
| caption            = Theatrical poster
| director           = Kunle Afolayan
| producer           = Kunle Afolayan
| writer         = Kemi Adesoye
| starring       =  
| music          = Truth
| cinematography = Yinka Edward Alfred Chia
| editing        = Yemi Jolaoso
| studio         = Golden Effects Studios
| distributor   =  
| released       =  
| runtime        = 110 minutes
| country        = Nigeria
| language       =  
| budget         =  ₦60 million 
| gross          =
}} romance Comedy-drama|comedy brief from an advertising agency to create a movie that would cut across ages 15 to 45.   

It narrates the story of Mary who works under a very stringent boss and Akin who is very bossy and distants himself from people around him. They both bump into each other at the airport, leading to their phones being swapped. This leads to an exchange in their destinations and the need to help carryout each others assignments. 
 Best Nigerian Film and won the award Achievement in Production Design.   

==Plot==
Mary (Nse Ikpe Etim) is a fashion designer who works for an stringent boss, Alexis, who often takes credit for her designs. Mary is called on the phone by her father in the village about a marital problem involving her sister and her husband. Mary must return to the village as the family will discuss the problem and it is only Mary to which her sister listens. Alexis will not give Mary permission to leave work although Mary insists that she can complete the clothing while away. Alexis is not persuaded. Mary finds out that her boyfriend is married and she breaks up with him. Alexis takes pity on Mary and allows her to take time off from work in order to sort out her problems. Alexis books a flight ticket for Mary so that the dust and dirt of regular ground travel will not mar the clothing that Mary is given permission to take with her while away during that busy time of work.

Akin (Wale Ojo) is always at loggerhead with colleagues at his work, he does not get along with his mother and his girlfriend. His boss is concerned about his recent behaviour, as a result shuts him out of knowing the venue of a company retreat, as many believe he is about to expose the misdeeds of his colleagues so that he can get the position of the Chief Executive Officer|C.E.O. Akin instructs his Assistant, Alex (Hafeez Oyetoro) to find out the venue for the retreat by any means. Akin arrives home one day and sees his girlfriend, Gina (Lydia Forson) drunk and she has also rearranged the living room. Akin gets upset and asks for his house keys. Gina gets angry, gives him the keys and leaves the house cussing him.

Akin heads to the airport and books a flight ticket to Abuja. Mary is also at the airport and bumps into Akin in her rush. Afterwards, a message enters into the phone Akin is holding with the name Alex saying "Enjoy your Flight to Owerri". Thinking the message was his assistant, he quickly goes to book a ticket to Owerri. Mary, in her naivety queues on the counter issuing tickets to Abuja and pays for the ticket. In the Airplane, Akin notices a change in the ringing tone of the phone in his pocket and realizes the phone is not his. He notifies the flight attendant, but it is already too late. Mary asks the flight attendant the duration of her flight to Owerri and she is told the flight is flying to Abuja and not Owerri.

Akin arrives in Owerri and asks for the ticket to Abuja, but he is told theres no flight scheduled for Abuja for the rest of the day. Mary also arrives in Abuja and calls her number through the phone in her hand. She tells Akin she is in Abuja and they both decide to find a way out. Akin asks Mary to go to his mothers house in Abuja, Akin also finds himself in Marys Fathers house which is a very ancient house. Marys sister, Cynthia (Ada Ameh) introduces Akin to their father (Chika Okpala) as Marys boyfriend. Mary is also received happily by Akins mother, Kike (Joke Silva) and she believed Mary is Akins girlfriend. Mary tries to explain to her what happened, but she is too overjoyed to listen. Akin finds it hard to adapt to the life in the Village: He asks the family for a cutlery to eat swallow food and he is laughed at, He is also unable to sleep with the others on bed and goes to sleep on the veranda. He finds it hard to sleep on the Veranda again due to mosquitoes. He gets up and calls Mary instead and they get to discuss many things like the fact that her sisters sin is that she beats her husband and shes accused of pulling her husbands penis this time around. Mary asks Akin to speak to Cynthia on her behalf because she thinks Cynthia will listen to him as Cynthia seems to like him. Akin also asks Mary to go to the companys retreat on his behalf. He tells her that all she needs to do is give the chairman the information on the memory card of his phone. Akin talks to Cynthia and she accepts to behave responsibly at the family meeting and to start behaving responsibly at home in order to preserve the future of her kids. At the meeting she kneels down for the Elders and apologizes to everyone in a sober state and eventually bursts into tears.

Mary gets ready for a party organized by Kike and it appears she has nothing to wear, so she decided to sew the cloth Alexis gave her to work on. Unfortunately, she meets the owner of the dress at the party; in her anxiety to escape, she falls into the swimming pool. She eventually promises the client to make another dress for the client under her own clothing line. Akin and Mary start to call each other often. During a call, Mary tells Akin about Gina calling her and accusing her of snatching her boyfriend. Akin replies by saying "Would you like to be?" and they both get very close from that moment. On the second day of the retreat, Mary got to know the new Chairman is Kike. She tells Mary she acquired the shares to get Akins attention. She used to drink a lot and that ruined her home, thats the main reason Akin doesnt want to have anything to do with her again. Mary tells her that the moment Akin finds out that shes now the Chairman, he will resign, she advises her to call him instead and talk to him. Akins mother listened to the advice and calls Akin. Akin thought it was Mary and tells her not to give the information to the Chairman anymore - He heard his mothers voice and tries to hang up, but his mother starts to apologize for everything she did and he calms down as his mother starts to eulogise him. Mary calls Alexis to tell her the dress got ruined and she informs her that she is resigning.

Akin meets Mary at a Lagos Airport. They both smile to each other and hand back their phone to each other. Akin helps Mary to carry her luggage and she gives an excited look. They both enter Akins car as the credits roll.

==Cast==
 
*Nse Ikpe Etim as Mary Oyenekwe
*Wale Ojo as Akin Cole
*Joke Silva as Kike Cole
*Chika Okpala as Marys Father
*Ada Ameh as Cynthia
*Lydia Forson as Gina
*Chika Chukwu as Hussana
*Hafeez Oyetoro as Alex Ojo
*Chris Iheuwa as Tony
*Charles Billion as Alpha
*Jay Jay Coker as Omega
*Jadesola Durotoye as C.E.O
*Sophia Chioma Onyekwo as Alexis
*Bose Oshin as Mrs Ibekwe
*Christopher John as Cynthias husband
*Toyin Onormor as Tonys wife
*Jara as Seamstress 1
*Biola as Seamstress 2
 

==Production==

===Development=== brief from slapstick comedy, which had become a tradition for the comedy genre in Nollywood.    He also stated at the press screening of the film in Lagos that the choice of a comedy genre is inline with the bid to diversify his production company; the previous two films from Golden Effects Studios had been thrillers, so Phone Swap was to show that his company was not running on a "one-way traffic".     Kemi Adesoye and Afolayan came up with the story idea for the film; with the scripting stage taking a total period of two years.    

Kunle Afolayan noted that Investors were slow in responding to his business plan.  However, the film eventually got financial support from telecommunication companies such as  , Dr Pepper/Seven Up|Seven-up Bottling Company, Berrys Couture, Honeywell Flour Mill and  GlaxoSmithKline|Maclean.    This helped subsidize the budget of the film by thirty   to forty percent. 

===Casting=== Joseph Benjamin Nse Etim Mr and Mrs, a film which was also in production at the time and was also released in March 2012. Jim Iyke was then called to play the role but he was unavailable at the time - shooting Last Flight to Abuja, so Wale Ojo eventually became the new lead actor.      The character of Marys father was slated for Sam loco Efe. The actor, however unfortunately died on 7 August 2011 due to an Asthma attack  before filming began and Chika Okpala was later signed to replace the deceased actor. Sam Loco was honoured by dedicating the film to his memory in the Opening credit of the film.   
 Benue and Igbo dialect Leadership Newspaper stated: "I am not exactly a rookie when it comes to the Igbo Language but until Phone Swap, I never needed to speak Igbo as a native would. I will admit that it was quite challenging, because I had to learn a specific dialect. I am very glad for the experience, though Igbo is a very interesting language and I enjoyed myself immensely in the process of learning it".  
 Etisalat Nigeria, a major competitor in the Nigerian telecommunication market. Afolayan however refused, with the believe that dropping Oyetoro would "kill" the film. As a result, Globacom partially withdrew its investment from the film and Afolayans ambassadorial contract with the company was not renewed. 

===Filming===
Filming began in August;  the film was shot across six weeks in Lagos and the post production took three months. According to Afolayan, the most challenging part of shooting was building the interior of the airplane and the airport scene which was shot for twenty four hours without a break.   At a scene where Mary fell into the pool, Nse had to be pushed into a swimming pool ten times before the cinematographer could get the perfect shot for the scene. 

The cast testified that it was easy working with Kunle Afolayan on set of the film. Ada Ameh stated that Kunle made sure the actors were comfortable and were well attended to. Nse Ikpe Etim said that the filming was fun but very draining for her. 

==Music and Soundtrack==
{{Infobox album
| Name        = Phone Swap - O.S.T
| Type        = Soundtrack
| Artist      = Truth
| Cover       = Phone Swap OST Cover.jpg
| Released    = 17 April 2012
| Genre       = Film soundtrack
| Length      = 16:30
| Label       = Golden Effects Pictures
| Producer    = Truth
| Last album = 
| This album = Phone Swap (2012) The Meeting (2012)
}}

In April 2012, Golden Effects Pictures released the Original Soundtrack of Phone Swap for digital download.    The Soundtrack of Phone Swap consist three original songs composed by Truth, Nodash and Oyinkansola, performed by Truth and Oyinkansola and a previously released classical song by King Sunny Adé. The Music design and score was done by Truth and the use of traditional Nigerian rhythms and drums were predominantly utilized for the film background scores. The songs incorporate chants, fast paced uplifting lyrics and traditional beats, but was downgraded to a very light piece with slower beats in some scenes.  One of the songs "Fate" is an official first single from the music producer, Truth.    

===Track listing===
{{track listing
| headline = 
| extra_column = Singer(s)
| music_credits = no
| total_length = 16:30

| title1 = Life is beautiful
| extra1 = Truth
| length1 = 3:33

| title2 = Fate
| extra2 = Truth
| length2 = 3:29

| title3 = With You
| extra3 = Oyinkansola
| length3 = 4:15

| title4 = Ma jaiye Oni
| extra4 = King Sunny Adé
| length4 = 5:08
}}

==Release== Ramadan celebrations from 18 through 21 August 2012. 

==Reception==

===Critical reception===
The film was met with mostly positive reviews. Nollywood Reinvented commended the cinematography, the subtle comedy and the romance. Though it noted that the film portray an unrealistic situation, it gave a 68% rating and concluded: "One great thing about the movie is the emotional connect it creates between the viewers and the ‘couple’, the viewers and the individual characters, the characters and each other and not just Mary and Akin. At the end of the movie you don’t just feel like you’ve seen another movie. It has a lasting impact that only a few movies can create. It’s one of those movies that will have you coming back to it time after time."  Augusta Okon of 9aija books and movies also praised the cinematography, use of language, production design, editing and soundtrack but talked down on the obvious commercials exhibited in the film. He gave the film a rating of 4 out of 5 stars and concluded: "Phone Swap is incontrovertibly a first class comedy movie in Nollywood, sailing on the seas of dynamism and ingenuity, with its mast of professionalism beating proudly in the air and pointing to one fact…It has raised the movie bar in Nollywood once again!".  Kemi Filani praised the cinematography, editing, character development, scripting and props of the film; she concluded: "Phone Swap is full of laugh out loud moments and shows how the most unlikely people  can adapt to unexpected situations and circumstances. A film I can definitely recommend."    NollywoodForever gave it a Watch Definitely rating and concluded that "...It was funny, it was witty, it was heart warming and more."  Dami Elebe of Connect Nigeria commended the directing and scripting of the film and stated "Without a doubt, this is the movie you are proud to show off. It is a beautifully done film and we can’t wait for another collaboration from them (the lead actors) in the future".   

Andrew Rice of The New York Times commented: "Kunle Afolayan wants to scare you, he wants to thrill you, he wants to make you laugh, but most of all, he would like you to suspend your disbelief — in his plots, yes, which tend to be over the top, but also about what is possible in Africa."  Shari Bollers of Afridiziak stated "All the characters were created to enable the audience to warm upto them and to laugh with and at them. With its switching of dual languages of native and English, it was easy to follow. It was a film that included everyone and warmed us with the humour. Whether Nigerian or not you will find this film funny."  Caitlin Pearson of The Africa Channel stated: "What makes the plot of Phone Swap engaging are the performances from its all-star cast, and a visual range in its cinematography that allows us to see more of Nigeria than any claustrophobic Nollywood film could ever hope to do.     Francis McKay of Flick Hunter gave a 3 out of 4 stars and concluded: "Kunle Afolayan crafted a highly watchable film with a good story and a cast led by two talented actors. The film is a good representation of Nigeria...". 

Kemi Filani commented on the positive portrayal of women in the film: "I love the way women were portrayed. For a change, Nigerian women were strong, smart and beautiful. Although the women in this film are flawed, like every other human being, they were also shown to be inherently good and beautiful."  Dami Elebe commended the acting skills of the two lead actors in the film: "Nse Ikpe-etim executed her role in this movie with precision, perfect pronunciation and class. Wale Ojo is not the typical Nollywood leading man. He is not the hot young boy we always want to just stare at, but the mature fine man we also want to hear. He blended in his role perfectly and showed that he is not a one movie wonder but an actor who will last a lifetime in Nollywood".  Caitlin Pearson commented on Wale Ojos acting skills by stating: "Ojo definitely impresses with his versatility as an actor in taking up this role, and manages to balance the austere qualities of Akin’s character with a sense of compassion". She also commended Hafiz Oyetoros character  by stating "the most funny and entertaining performance is by Hafiz Oyetoro who plays Akin’s assistant Alex. Alex’s character exists in a fine balance between subservient and sneaky that is pure pleasure to watch." 

===Box Office=== John carter. This has been said to be due to the number of showing times given to Phone Swap with at least 6 showing times per day for a theatre. It was also reported that the film had several private screenings for corporate organizations and brands.   Moreover, the film received nominations at the Africa Movie Academy Awards just before its general theatrical release. 

===Awards=== Best Nigerian Film. It eventually won the award for Achievement in Production Design. It received the most nominations at the 2012 Best of Nollywood Awards with ten nominations and won the award for Best Production Set; Nse Ikpe Etim also won the award for Best Lead Actress in an English film.  Phone Swap also got most nominations and most wins at the 2013 Nollywood Movies Awards with a total of twelve nominations and won the awards for Best Movie, Best Actor in a Supporting Role for Hafeez Oyetoro, Best Actress in a Supporting Role for Ada Ameh, Best Cinematography, Best Original Screenplay and Top Box Office Movie of the Year.   It was nominated for eight awards at the 2013 Golden Icons Academy Movie Awards, where Kunle Afolayan won the Best Director category.

 
{| class="wikitable sortable" style="width:100%"
|+Complete list of Awards
|-
!  Award !! Date of ceremony !! Category !! Recipients and nominees !! Result
|- Africa Movie Africa Film Academy   (8th Africa Movie Academy Awards)  22 April 2012 Best Nigerian Film
| Kunle Afolayan
|  
|-
| Achievement in Production Design Pat Nebo
|  
|- Best Actor in a Supporting Role
| Hafeez Oyetoro
|  
|- Best Actor in a Leading Role
| Wale Ojo
|  
|-
| Abuja International Film Festival   (2012 Golden Jury Awards)       September 2012
| Best Film Kunle Afolayan
|  
|-
| Abuja International Film Festival   (2012 Festival Prize)  
| Outstanding Film Directing
|  
|- Best of BON Magazine   (2012 Best of Nollywood Awards)  11 November 2012
| Best Lead Actress in an English film
| Nse Ikpe Etim
|  
|-
| Best supporting Actor in an English film
| Hafeez Oyetoro
|  
|-
| Best Lead Actor in an English film
| Wale Ojo
|  
|-
| Director of the Year Kunle Afolayan
|  
|-
| Movie of the Year
|  
|-
| Screenplay of the Year
| Kemi Adesoye
|  
|-
| Cinematography of the Year
| Yinka Edward
|  
|-
| Movie with Best Sound
| Biodun Oni, Eilam Hoffman
|  
|-
| Best Edited Movie
| Yemi Jolaoso
|  
|-
| Best Production Set
| Pat Nebo
|  
|-
| MultiChoice   (2013 Africa Magic Viewers Choice Awards) 
| 9 March 2013
| Best Movie (comedy) Kunle Afolayan
|  
|- NollywoodWeek Paris   (2013 NollywoodWeek Paris Awards) 
| June 2013
| Public Choice Award
|  
|- Nollywood Movies|Nollywood Movies Network   (2013 Nollywood Movies Awards)  12 October 2013
| Best Movie
|  
|-
| Best Actor in a Leading Role
| Wale Ojo
|  
|-
| Best Actor in a Supporting Role
| Hafeez Oyetoro
|  
|-
| Best Actress in a Supporting Role
| Ada Ameh
|  
|-
| Best Director
| Kunle Afolayan
|  
|-
| Best Editing
| Yemi Jolaoso
|  
|-
| Best Cinematography
| Yinka Edward
|  
|-
| Best Original Screenplay
| Kemi Adesoye
|  
|-
| Best Costume Design
| Titi Aluko
|  
|-
| Best set Design
| Pat Nebo
|  
|-
| Best Sound Track
| Truth
|  
|-
| Top Box office Movie Kunle Afolayan
|  
|- Golden Icons Golden Icons Magazine   (2013 Golden Icons Academy Movie Awards)   19 October 2013
| Best Motion Picture
|  
|-
| Best Director
|  
|-
| Best Cinematography
| Yinka Edward, Alfred Chia
|  
|-
| Best Comedy
| Kunle Afolayan
|  
|-
| Best Actress
| Nse Ikpe Etim
|  
|-
| Most Promising Actor (Best New Actor)
| Wale Ojo
|  
|-
| Best Original Screenplay
| Kemi Adesoye
|  
|-
| Producer of the Year
| Kunle Afolayan
|  
|}

==Home Media== VOD platforms; including OHTV and Ibaka TV.   The film was released on DVD on 15 December 2014.      Afolayan stated in an Interview that the delay in the DVD release was as a result of plans to have an effective distribution framework for the DVD so as to reduce piracy of the film to a minimum level.  The DVD is distributed by G-Media,     and it features bonus content such as "behind-the-scene shots" and "The making of...".  A special edition DVD package, tagged "Kunle Afolayan’s Collection", containing the other two previous films directed by Kunle Afolayan was also released. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 