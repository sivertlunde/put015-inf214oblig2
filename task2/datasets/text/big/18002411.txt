Thenmavin Kombath
{{Infobox film
| name           = Thenmavin Kombath
| image          = Thenmavin Kombath.jpg
| caption        = DVD cover
| director       = Priyadarshan Sreenivasan Sankaradi Sharat Saxena
| writer         = Priyadarshan
| producer       = N. Gopalakrishnan
| cinematography = K. V. Anand
| editing        = N. Gopalakrishnan
| distributor    = Surya Cini Arts Sudev Release
| studio         = Prasidhi Creations
| released       =  
| runtime        = 193 minutes
| country        = India Malayalam
| music          = Original Background Score:  
| budget         =
}} Malayalam musical musical drama film written and directed by Priyadarshan, produced and edited by N. Gopalakrishnan, and starring Mohanlal, Shobana, Nedumudi Venu, Kaviyoor Ponnamma, KPAC Lalitha, Sukumari, Sreenivasan (actor)|Sreenivasan, Sankaradi, and Sharat Saxena. The cinematography was by K. V. Anand, and the score was composed by S. P. Venkatesh, while Berny-Ignatius composed the songs with lyrics written by Gireesh Puthenchery.
 National Film Best Cinematography Best Production Design respectively. Music directors Berny-Ignatius received the Kerala State Film Award for Best Music.  National Film Awards and five Kerala State Film Awards in various categories .
 Hindi as Saat Rang Ke Sapne (1998). And in Kannada as Sahukara (2004).

==Plot==
The story revolves around Manikyan (Mohanlal), Sreekrishnan(Nedumudi Venu) and Karthumbi (Shobana) and the love triangle between them. Manikyan works for Sreekrishnan and Sreekrishnan sees him like a brother. Once when they both are returning from a Mela after shopping, Sreekrishnan sees Karthumbi and gets attracted. But then a fight erupts there and they all have to flee. Sreekrishnan flees alone while Manikyan has to take Karthumbi with him. At night, he flees in the opposite direction and so loses his way. Karthumbi knows the way back but she pretends as if she does not know it and enjoys the fun. Manikyan has to struggle to get out of that place. During that time they fall in love.

Once they are back in Manikyans village, Sreekrishnan proposes her and plans to get married to her. Manikyan can not resist because Sreekrishnan is like an elder brother to him. But Karthumbi opposes. When Sreekrishnan gets to know about this, he gets angry and Manikyan becomes his enemy and he tries to take revenge. Finally Sreekrishnan understands his mistakes and he marries the woman who has been loving him for so long while Manikyan unites with Karthumbi.

==Cast==
*Manikyan – Mohanlal
*Karthumbi – Shobhana
*Sreekrishnan Thampuran – Nedumudi Venu
*Yeshodhamma – Kaviyoor Ponnamma
*Karthu – KPAC Lalitha
*Ginjimood Gandhari – Sukumari Sreenivasan
*Kannayyan – Sankaradi
*Chakkutty – Kuthiravattom Pappu
*Chinnu – Geetha Vijayan
*Thimmayan - Nandhu Sonia   Khadeeja

==Theatrical reception==
The film emerged as a blockbuster at kerala box office becoming the highest grossing Malayalam film of 1994 and subsequently getting a run of 250 days.     

==Soundtrack==
{{Infobox album  
| Name        = Thenmavin Kombath
| Type        = soundtrack
| Artist      = Berny-Ignatius
| Cover       = 
| Released    = 1994 (India)
| Recorded    =
| Genre       = Film soundtrack
| Length      =
| Label       = 
| Producer    =
| Reviews     =
| Last album  = 
| This album  = Thenmavin Kombath   1994
| Next album  = Manathe Kottaram   1994
}}

Berny-Ignatius composed the films songs and Girish Puthenchery wrote the lyrics. It is known to be the most sold soundtrack of the analog cassette era in the history of Malayalam cinema.

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| Track # || Song || Singer(s) || Duration
|-
| 1 || "Nila Pongal" || Malgudi Subha || 3:31
|-
| 2 || "Karutha Penne" || M.G. Sreekumar, K.S. Chitra || 4:47
|-
| 3 || "Maanam Thelinge" || M.G. Sreekumar, K.S. Chitra || 4:04
|-
| 4 || "Kallipoonkuyile" || M.G. Sreekumar || 4:17
|-
| 5 || "Ente Manasinoru"|| M.G. Sreekumar, Sujatha Mohan || 4:11
|}
Re

==Remakes== Hindi by Priyadarshan as Saat Rang Ke Sapne (1998). It was also remade in Kannada as Sahukara with V. Ravichandran.

==Awards== National Film Awards  Best Production Design- Sabu Cyril Best Cinematographer- K. V. Anand

; Kerala State Film Awards  Best Popular Film Best Art Director – Sabu Cyril Best Music Director- Berny-Ignatius  Second Best Actor - Nedumudi Venu Second Best Actress – Kaviyoor Ponnamma

==References==
 

==External links==
*  

 

 
 
 
 
 