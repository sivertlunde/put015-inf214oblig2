Goofy Groceries
 
Goofy Groceries is an animated short film in Warner Bros. Merrie Melodies cartoon series, originally released on March 29, 1941 and re-issued into the "Blue Ribbon Classics" series on April 19, 1947. Bearing a similar premise to such earlier WB shorts as Speaking of the Weather and Have You Got Any Castles? but having a cast inspired by food products instead of magazines or books, the cartoon was written by Melvin Millar, directed by Robert Clampett, and produced by Leon Schlesinger. The animators included Vive Risto, Izzy Ellis, John Carey, and Rod Scribner.

== Synposis ==
The cartoon takes place one winter night, in a grocery store whose owner has just closed the shop. The mascots on the labels of the food products come to life and perform various song and dance numbers.
 Animal Crackers" Horse Radish" Navy Beans" Turtle Soup Roman candle while at one point destroying the bottle of Horse Radish that Jack is riding. Jack sees a box of "Chocolate-Covered A1 Cherries" and snatches the axe on the label amid cheering from an army of chicks, at which point the gorilla shoots the axe with the candle causing it to shrink. As Jack Bunny dons a sheepish grin and backs into a corner, the image of Superman on a box of "Superguy Soap Chips" comes to life at the sight of the gorilla lighting a stick of dynamite with Bunnys cigar. Superman flies up to the gorilla and shouts at him, "Hey, you big ape!" and the gorilla replies "Yeah?" which scares Superman so much that he turns into a helpless, whining baby. Then, as the gorilla is about to destroy Jack Bunny with the dynamite in his hand, a voice calls out "Henry!" (in reference to the opening of The Aldrich Family), causing him to pause and run towards the direction of the voice, saying in a frightened voice "Coming aunt!" (another reference to said opening) while his apparent "aunt" drags him away by his ear, chastising him for his naughty behaviors. Jack Bunny breathes a sigh of relief only to realize hes still holding the dynamite, which explodes leaving him in blackface. After being exploded on, he then concludes the cartoon with a Rochester impression: "My oh my, tattletale gray!"

== Availability ==
The cartoon is available restored, uncut, and uncensored on Disc 2 of the 2005 DVD  .

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 