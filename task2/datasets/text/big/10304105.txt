Machete (film)
 
{{Infobox film name        = Machete image       = Machete poster.jpg alt         =  caption     = Theatrical release poster director    = Robert Rodriguez Ethan Maniquis producer    = Robert Rodriguez Elizabeth Avellan Rick Schwartz writer      = Robert Rodriguez Álvaro Rodriguez starring    = Danny Trejo Steven Seagal Michelle Rodriguez Jeff Fahey Cheech Marin Lindsay Lohan Don Johnson Jessica Alba Robert De Niro music  Chingon
|cinematography = Jimmy Lindsey editing     = Robert Rodriguez Rebecca Rodriguez studio      = Overnight Films Troublemaker Studios Hyde Park Entertainment distributor = Sony Pictures Releasing International (International) released    =   runtime     = 105 minutes  country     = United States language    = English Spanish  budget      = $10.5 million   gross       = $44,093,316 
}}
Machete is a 2010 American action film written, produced, and directed by Robert Rodriguez and Ethan Maniquis. This film is an expansion of a fake trailer that was included in Rodriguezs and Quentin Tarantinos 2007 Grindhouse (film)|Grindhouse double-feature. Machete continues the B movie and exploitation style of Grindhouse,  and includes some of the footage. The film stars Danny Trejo in his first lead role as the title character, and co-stars Robert De Niro, Jessica Alba, Don Johnson, Michelle Rodriguez, Steven Seagal, Lindsay Lohan, Cheech Marin and Jeff Fahey. This was Steven Seagals first theatrical release film in eight years since his starring role in 2002s Half Past Dead. Machete was released in the United States by 20th Century Fox and Rodriguezs company, Troublemaker Studios, on September 3, 2010.  A sequel, Machete Kills, was released on October 11, 2013.

== Plot == Machete Cortez Mexican Federales|Federal. The film opens in Mexico with Machete and his younger partner on a mission to rescue a kidnapped girl (Mayra J. Leal). During the operation, his partner is killed, the kidnapped girl stabs him before being killed herself, and Machete is betrayed by his corrupt Chief to the powerful drug lord, Rogelio Torrez (Steven Seagal), who kills Machetes wife and implies that he will kill his daughter before leaving him for dead.
 Texas State Senator John McLaughlin (Robert De Niro) is sending hundreds of illegal immigrants out of the country. In order to stop this, Booth offers Machete $150,000 to kill McLaughlin. Machete accepts the murder contract after Booth threatens to kill him if he does not.

Machete trains a rifle on McLaughlin from a rooftop during a rally, but before he fires he sees one of Booths henchmen aiming at him. The henchman shoots Machete in the shoulder and then shoots McLaughlin in the leg. It is revealed that Booth orchestrated the attempted assassination as part of a false flag operation to gain public support for McLaughlins secure border campaign. By setting up Machete as the gunman, the conspirators make it appear that an outlaw illegal Mexican immigrant has tried to assassinate the senator, who is known for his tough stance on illegal immigration.

An injured Machete escapes capture by Booth and is taken to a hospital to be treated for wounds, escaping once again from Booths henchmen at the hospital. Agent Sartana Rivera (Jessica Alba), a persistent U.S. Immigration and Customs Enforcement agent, is sent by her superior to find and capture the injured Machete. Machete, with the help of Luz (Michelle Rodriguez), aka Shé, the leader of an illegal immigrant aid movement known as the Network, recruits Padre (Cheech Marin), his "holy" brother. To take revenge on Booth, Machete kidnaps Booths wife and his daughter, April (Lindsay Lohan), after "starring" in an amateur adult film with them. He also collects evidence from Booths house linking McLaughlin and the Mexican drug lord in a major drug trafficking deal. After encountering Machete, Sartana begins to develop an interest in him. Elsewhere, Booth, who is revealed to be working for Torrez, hires a hitman, Osiris Amanpour (Tom Savini), to assist in the hunt for Machete.

Booth and Osiris kill Padre in his church, but do not find his own wife and daughter. Unbeknownst to Booth, the church has cameras. Through the CCTV recordings, the news of McLaughlins corruption and faked assassination is eventually aired on national television. Infuriated, McLaughlin kills Booth and heads back to join Torrez to kill Machete. In order to eliminate the people who double-crossed him, Machete gathers the Mexican Network and leads them to the base of the border vigilantes, led by Von Jackson (Don Johnson).

During this confrontation, the Mexican illegals triumph over the border vigilantes. Jackson tries to escape, but Luz shoots him in the back of the head. Machete fights Torrez, culminating with the drug lord committing seppuku with one of Machetes blades with which he has just been stabbed. April shows up wearing a nuns habit after escaping from the church and shoots McLaughlin after figuring out that he has killed her father.

McLaughlin, injured, manages to escape, but he is later killed by the last remnants of his own border vigilante group, who mistake him for a Mexican. The film ends with Machete meeting with Sartana, who gives him a green card. They kiss and ride off into the night.

Before the credits roll, on the screen it says that Machete will return in Machete Kills and Machete Kills Again.

== Cast == Machete Cortez, "a legendary ex-Federale with a deadly attitude and the skills to match." 
* Robert De Niro as Senator John McLaughlin, a politician campaigning to incite hatred against illegal immigrants. 
* Jessica Alba as Sartana Rivera, "a beautiful Immigrations Officer torn between enforcing the law and doing what is popular in the eyes of her familia."  Alba also portrays Sartanas twin sister in deleted scenes only.
* Steven Seagal as Rogelio Torrez, a drug lord, as well as Machetes former partner with the Federales. 
* Michelle Rodriguez as Luz/Shé, "a taco-truck lady with a revolutionary heart." 
* Jeff Fahey as Michael Booth, "a ruthless businessman with an endless payroll of killers." 
* Cheech Marin as Padre Benicio Del Toro, Machetes brother, "a priest whos good with blessings, but better with guns." 
* Don Johnson as Von Jackson, "a twisted border vigilante leading a small army."  The character was originally known as "Lt. Stillman" and was meant to be Sartanas superior.
* Lindsay Lohan as April Booth, "a socialite with a penchant for guns" and "a nun with a gun." 
* Shea Whigham as Sniper, Booths right-hand man and bodyguard.
* Daryl Sabara as Julio Gilbert Trejo as Jorge
* Ara Celi as Reporter
* Tom Savini as Osiris Amanpour, a hitman hired by Booth to kill Machete.
* Billy Blair as Billy, Von Jacksons henchman. 
* Felix Sabates as Doctor Felix
* Electra and Elise Avellan as Nurses Mona and Lisa 
* Mayra J. Leal as Naked Girl
* Juan Gabriel Pareja as Rico
* Alicia Marek as June Booth, the socialite wife of Michael Booth and Aprils mother.   
* Tito Larriva as Culebra Cruzado Cheryl "Chin" Cunningham as Torrez henchwoman 
* Nimród Antal as Booths bodyguard #1

== Production ==
 

=== Development === The Killer really inspired me to make films that would create that feeling in the Latin arena". 
 federale from DEA have a really tough job that they dont want to get their own agents killed on, theyll hire an agent from Mexico to come do the job for $25,000. I thought, Thats Machete. He would come and do a really dangerous job for a lot of money to him but for everyone else over here its peanuts. But I never got around to making it". 

Instead, during the filming of Rodriguez and Quentin Tarantinos Grindhouse (film)|Grindhouse, Rodriguez shot lobby cards and sequences from parts of the original script in 2006 for a fake trailer featuring Danny Trejo, Cheech Marin, as well as Jeff Fahey. At South by Southwest in March 2007, Rodriguez announced that he would be expanding his trailer for Machete into a feature-length film.  He announced that it would be a bonus feature on the Planet Terror DVD, but the film ended up being produced as a theatrical release.    

During Comic-Con International|Comic-Con International 2008, he took the time to speak about Machete, including such topics as the films status, possible sequels, as well as production priorities.  It was also revealed that he has regularly pulled sequences from it for his other productions including Once Upon a Time in Mexico.

=== Direction ===
Rodriguez had always planned on being the head-director of the film, since he usually uses his " .

=== Casting ===
The films lead role had always been intended for Danny Trejo, as Rodriguez mentioned. The two had met during the filming of Rodriguezs film, Desperado (film)|Desperado. "Nobody really knew about Desperado, yet the local townspeople would flock to see Danny, thinking he was the star of the movie, even though his part was very small", Rodriguez remembers. "He has incredible presence, and I knew I had found Machete. So, I handed him a knife, and told him to start practicing". 

Trejo, Cheech Marin, as well as Jeff Fahey were announced to be reprising their characters in the trailer for the film. Tito Larriva did not reprise his primary role as the sniper who shoots Machete and was replaced by Shea Whigham. Michelle Rodriguez was the first actress to be cast in the film who wasnt in the original trailer. Chris Cooper was approached for the role of Senator McLaughlin. After Cooper turned down the role, Robert De Niro was cast instead. "From the moment you get Robert De Niro in your movie, all the other actors come running", says Robert Rodriguez on De Niros involvement in the film. 

"What I liked about the character was that it was a fun thing to do in a sense of irony and its not taken seriously. Hes not taken seriously on one level so that frees you to have fun without being weighted down by some requirements", says De Niro on playing his character.  Lindsay Lohan was revealed to be cast in the film in July 2009. "Lindsays cool", Rodriguez said of Lohan. "Theres actually a cool part in the movie for her if she takes it".  Lohan portrays April Booth, the socialite daughter of Booth, the man who double-crosses Machete. "April was born into a life of privilege and takes everything she has for granted", says Lohan. "But she undergoes a big change. As an actress, I like pushing the envelope".  Lohan recolored her hair from auburn red to platinum blonde. 

Jonah Hill was originally cast as Julio,  but was replaced by Daryl Sabara, who had worked with Rodriguez, Marin, as well as Trejo on the set of the Spy Kids (franchise)|Spy Kids franchise. Jessica Alba said of her role of Agent Sartana, "My character is an Immigrations Officer and she hunts him (Machete) down, but finds out that he was double-crossed himself. She wrestles with her own right-wing kind of mentality because shes all about enforcing the law and doing what the system tells you". 

"The cast may have sounded bizarre to some people when first announced. But when you watch Machete, you see that the actors fit their roles very well. The eclectic mix really works. Dannys worked in hundreds of movies and probably worked with everyone in Machete at some point. Everyone just loves Danny and appreciated the fact he was finally getting to be the star of his own film. I remember Robert De Niro, who worked with Danny in Heat (1995 film)|Heat telling him that,   is going to be really good for you." 

Regarding the nudity that was present in the film, Rodriguez mentioned that he deliberately set the first scene with a nude woman (actress Mayra Leal) to make the audience think that subsequent scenes show more than they actually do.  Jessica Alba mentioned that for her shower scene, "I had undies on, and I had other stuff on, and they just sort of etched it out in post  . Thats not really me. Im better covered up". 

=== Filming ===
Filming for the fake trailer for Grindhouse began in the summer of 2006, while Rodriguez was also filming Planet Terror. The scenes that Rodriguez shot only consisted of scenes involving Danny Trejo, Cheech Marin, Jeff Fahey, as well as Tito Larriva. Some of the scenes filmed for the trailer were left intact in the film, while others have been re-shot.

Principal photography began on July 29, 2009 in and around the city of Austin, Texas. Lindsay Lohan filmed her role in three days, two in August and one in September.  Principal photography of the film ended on September 24, 2009.

== Release ==

=== Marketing ===
The first trailer was released and attached to Planet Terror in 2007. On July 28, 2010, it was confirmed that Machete would headline the Venice Film Festival held on September 1, 2010, at a special midnight screening, followed by the general theatrical release on September 3.  

The film was released in the U.S. on September 3, 2010,  by 20th Century Fox.  The film appeared on 3,400 screens at 2,670 locations. 

The film was released in Australia on November 11 and in Mexico on November 12, 2010. 

A fake trailer for the film was released on May 5, 2010, through Aint It Cool News. The trailer opened with Danny Trejo saying, "This is Machete with a special Cinco de Mayo message to Arizona", followed by scenes of gunfire, bloodshed, and highlights of the cast.  The fake trailer combined elements of the Machete trailer that appeared in Grindhouse with footage from the actual film,  and implied that the film would be about Machete leading a revolt against anti-immigration politicians and border vigilantes.  According to Fox News, critics of illegal immigration were offended by the contents of the movie trailer.  Production of the film predates the legislation, however. 
 The Expendables.    A red band trailer featuring more gruesome scenes from the film was released on July 23, 2010. 

=== Box office performance ===
{| class="wikitable"
|-
! rowspan=2 | Film
!| Release date
! colspan="3" | Box office revenue
! colspan=2 text="wrap" | Box office ranking
! rowspan=2 style="text-align: center" | Budget
! rowspan=2 style="text-align: center" | Reference
|-
! United States
! United States
! International
! Worldwide
! All-time United States
! All-time worldwide
|-
| Machete
| style="text-align: center" | September 2010
| style="text-align: center" | $26,593,646
| style="text-align: center" | $17,499,670
| style="text-align: center" | $44,093,316
| style="text-align: center" | #2,179
| style="text-align: center" | Unknown
| style="text-align: center" | $10,500,000
| style="text-align: center"| 
|}

Machete opened in 2,670 theaters in the   opened in theaters, totaling $20,916,709.

Machete has made $26,593,646 in the United States as of November 18, 2010.  The film has made an extra $17,499,670 in foreign markets, bringing the gross total of $44,093,316, as of November 18, 2010. 

=== Critical reception ===
{| class="wikitable"
|-
! Film
! Rotten Tomatoes
! Metacritic
! Entertainment Weekly
|-
| Machete
| style="text-align: center" | 72% (178 reviews) 
| style="text-align: center" | 60/100 (29 reviews) 
| style="text-align: center" | B 
|}
 Razzie Award Worst Supporting The Killer Inside Me, Little Fockers and Valentines Day (2010 film)|Valentines Day. 

=== Home media ===
Rodriguez has expressed in an interview that an even more violent directors cut will be released on home media. 

The R-rated theatrical version of Machete was released on DVD and Blu-ray Disc|Blu-ray on January 4, 2011.

On the week ending January 9, 2011, Machete debuted atop the DVD sales chart in America. The DVD sold 691,317 copies in its first week of release. 

In the Blu-ray deleted scenes, Rose McGowan is featured as one of Osiriss henchmen named Boots McCoy; one scene shows her confronting Luz and shooting her through a cat she is holding, hitting her in the eye and seemingly killing her. However, in the film, this action was given to Von Jackson, and it is later revealed Luz is merely blinded. In other deleted scenes Jessica Alba also plays the role of Sis, Sartanas twin sister. McCoy later shows up and slits her throat with a razor blade, thinking she is actually Sartana.

==Sequels==
 
According to Trejo, Rodriguez has scripts for the first of two Machete sequels, tentatively titled Machete Kills and Machete Kills Again.  Both these titles appear in the end credits of the films theatrical release. At the 2011 San Diego Comic-Con, Rodriguez said that both sequels have been greenlit. The trailer for the third film, Machete Kills Again,  precedes the second film as a "Coming Attraction".   Machete Kills was released on October 11, 2013.

== References ==
{{reflist|30em|refs=
   
   
   
   
   
   
   
   
   
   
   
 [http://www.imdb.com/name/nm3046228/ Billy Blair at IMDB 
   
   
   
   
   
   
  , Moviehole, May 14, 2007  
   
   
   
   
   
   
   
   
   
   
   
   
  , Internet Movie Database 
   
  , 67th Venice International Film Festival, September 2010, website. 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
 {{cite web|url=http://www.rottentomatoes.com/m/machete/ |title=Machete (2010) |publisher=Rotten Tomatoes |accessdate=
January 8, 2011}} 
   
   
   
   
 {{cite web
| url = http://www.nme.com/movies/news/machete-sequel-script-has-already-been-written/199871 | title = Machete sequel script has already been written | year = 2010 | publisher = NME | archiveurl = http://www.webcitation.org/5vGM5jsfP | archivedate = 2010-12-26 | accessdate = 2010-12-26 }} 
   
   
   
}}

== External links ==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 