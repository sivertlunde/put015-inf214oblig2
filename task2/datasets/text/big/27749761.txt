Jack O'Lantern (film)
{{Infobox film
| name           = Jack Olantern
| image size     = 
| image	=	Jack OLantern FilmPoster.jpeg
| alt            = 
| caption        = DVD release poster
| director       = Ron McLellen
| producer       = Ron McLellen Tim E. Hayes
| writer         = Ron McLellen
| narrator       = 
| starring       = Dave R. Watkins Tracy Yarkoni Kevin L. Powers Justice Leak Cheri Christian Brian Bradly April Glover
| music          = Ron McLellen
| cinematography = Ron McLellen
| editing        = Ron McLellen
| studio         = Southlan Films
| distributor    = Lions Gate Home Entertainment
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 American 2004 indie horror film written and directed by Ron McLellen, and starring Dave R. Watkins, Kevin L. Powers and Tracy Yarkoni.  First screened at the London FrightFest Film Festival, it was released on DVD in 2005 by Lions Gate Entertainment. 

==Plot==
  Jack O Lantern creature.

==Cast==
* Dave R. Watkins as Jack 
* Kevin L. Powers as Brett
* Tracy Yarkoni as Christine
* Justice Leak as Billy Willis
* Brian Bradley as Josh
* Cheri Christian as Lori
* April Glover as Page
* Joel Hunter as Reverend Willis
* David Chillian as Pauley
* Greg Thompson as Professor Doyle
* Sacha Dzuba as Max
* Michael D Friedman as Trent
* Eric Bomba-Ire as Jamal

==Critical reception==
Jack OLantern was almost universally panned by critics. DVD Talk wrote Jack OLantern is one of the goofiest, silliest, and most obviously inept "do-it-yourself" horror flicks Ive seen in quite some time", offering that one might expect "a no-budget stinker like this populating the 99-cent VHS bin", but that it was "a bit bizarre to see a company like Lions Gate releasing this malformed mass on DVD."  Of plot and cast they wrote, "The acting is hilariously bad, the story is muddled, familiar and boring, the kills are juicy yet infrequent, and the titular beastie? He looks like a goofy, runny, smelly pumpkin. Not too scary, obviously."  They summarized by writing "  is just a boring, derivative, and amateurish mess from beginning to end. When its not ripping off better movies, its meandering around in a barn and grinding to an endless halt."   

DVD Verdict also condemned the film, writing "Jack OLantern fails in almost every category it competes in. Acting, story, production values, gore, and entertainment, all of it—a big fat zilch-o."  They expanded that the films greatest flaw was in its execution and editing, with establishing shots, shot selections, script, plot, and acting also contributing to its failure and compared watching it to being "pruned by a blind toddler". In summarizing, they offered "this flick just isnt fun to watch. Your head will hurt listening to these idiots yammering on about some big secret and yawns will be suppressed during the big down-time between kills…which suck anyway".    

Dread Central compared watching the film to the personal devastation caused to the reviewer by Hurricane Katrina, writing "I watched a new direct-to-video horror movie called Jack OLantern. I’d just like to take this opportunity to ask God a question: Have I not suffered enough?"  The review pointed out the films poor acting, script, filming, editing, and dialogue. They further wrote that the film is "Amateurish on every conceivable level, there is no justification I can think of for this wretched film to have gotten a legit release, certainly not through a name distributor like Lions Gate."    

Conversely, Film Monthly found the film "better than mediocre" and "highly watchable", but noted that this "watchability is tempered by horrible special effects work."  It was offered that many of the films flaws actually created unintentional laughs, and that "the plot somehow manages to be fantastically original and alarmingly derivative all at the same time."  The reviewer summarized by writing "All in all, Jack O’Lantern will offer up the most original derivative plot with special effects cheap enough to be called direct-to-video. It’s not all that bad. It’s nothing great, but it’s certainly not all that bad."   

==References==
 

==External links==
*  

 
 
 
 
 
 
 