Pokémon Heroes
 

{{Infobox film
| name = Pokémon Heroes
| image = Pokemon-heroes-poster-japanese.jpg
| caption = Japanese release poster
| film name      = {{film name
| kanji          = 劇場版ポケットモンスター 水の都の護神 ラティアスとラティオス
| romaji         = Gekijōban Poketto Monsutā Mizu no Miyako no Mamorigami Ratiasu to Ratiosu
| translation    = Pocket Monsters the Movie: The Guardians of Altomare
}}
| director = Kunihiko Yuyama
| producer = Yukako Matsusako Takemoto Mori Choji Yoshikawa
| writer = Hideki Sonoda
| narrator = Unshō Ishizuka
| starring = Rica Matsumoto Mayumi Iizuka Yuji Ueda Megumi Hayashibara Shin-ichiro Miki Fumiko Orikasa Kōichi Yamadera Yuzo Gutch Uno Kanda Yumiko Shaku Masashi Ebara Ikue Ōtani
| music = Shinji Miyazaki
| cinematography = Hisao Shirai
| editing = Toshio Henmi
| studio = OLM, Inc.
| distributor = Toho   Miramax Films  
| released =    
| runtime = 71 minutes
| country = Japan  
| language = Japanese  
| budget = 
| gross = $20,867,919
|}}
Pokémon Heroes: Latios and Latias, commonly referred to as Pokémon Heroes, originally released in Japan as  , is a 2002 Japanese anime film directed by  , Yuji Ueda, Mayumi Iizuka, Megumi Hayashibara, Shin-ichiro Miki and Ikue Ōtani. The English adaptation was produced by 4Kids Entertainment and distributed by Miramax Films (a subsidiary of The Walt Disney Company), and saw a limited release in the United States on May 16, 2003, before being released to video and DVD in January 2004. The English version stars the regular television cast of Veronica Taylor, Eric Stuart, Rachael Lillis and Maddie Blaustein.

Although  ,  , and Pokémon 4Ever).

Pokémon Heroes focuses upon the main characters, Ash, Misty and Brock, traveling through the Johto region once more; the main location of the movie is based on Venice, Italy. The name given to the city in the movie is Alto Mare, meaning "High sea" in Italian. Although it is part of the groups adventure in Johto, the island is a part of the Hoenn Region.

Optimum Home Entertainment re-released the movie on DVD in UK on May 9, 2011   Studio Canal re-released this film along with Pokémon 4Ever on Blu-ray in the UK as a double feature pack on April 2, 2012,  just one day before   came out on DVD on April 3, 2012.

==Plot==

===Camp Pikachu===
The story behind this short film revolves around the Pichu brothers as they attempt to find a train back to the big city. Along the way, they are helped by seven of Ash Ketchums and Misty (Pokémon)|Mistys Pokémon (Pikachu, Cyndaquil, Totodile, Phanpy, Corsola, Togepi, Psyduck) and a Wynaut. Meanwhile, Meowth and Wobbuffet are hiking in the back country, but their journey continuously goes wrong and their paths intertwines with the others.

Along the way, the Pokémon sit around a campfire, where the older Pichu brother, along with Wynaut, scare the younger brother with the help of a Duskull. They also encounter a Volbeat (Pokémon)|Volbeat, who guides them to a mill where they can stay the night.

===Heroes: Latios and Latias===
The main films setting is in the water-themed Johto city of Altomare (which means "High Sea"), based on cities such as Venice, Italy, Amsterdam, Netherlands and Bizerte, Tunisia . The city is watched over by two legendary Pokémon, Latias and Latios. The story behind the two involves an evil Pokémon Trainer using a Kabutops and an Aerodactyl to terrorize the citizens, until the original Latios came to the city, using his powers to drown the evil Pokémon and turning the streets into canals. However, Latios died and his children were left orphans. His soul is within a special jewel, the Soul Dew.
 Brock are Team Rocket are also in the city and decide to follow Annie and Oakley.

The trio later visit Lorenzo, the curator of a local museum, who explains the citys history and also introduces them to the Defence Mechanism of Altomare (DMA). Ash pursues Bianca, and then Latias in disguise, across the city, eventually discovering a hidden garden where Latias and Latios live. When Latios and Bianca threaten to force Ash out, Lorenzo arrives to clear up all the tension. Latias and Latios play with Ash and Pikachu, and later Lorenzo reveals to Ash the Soul Dew. However, Annie and Oakleys spy robot is watching this and later the duo steal the Soul Dew and capture Latios - using both to control the DMA. Latias flees to Ash for help, and later the two and Pikachu race across the gridlocked city to the museum. Using the DMA, Oakley locks down the city and revives the undead Pokémon to capture Latias. After a long chase through the city, Ash and Latias cause the undead Aerodactyl to crash into a building, and are saved from the Kabutops by their friends Pokémon. The heroes arrive at the museum to find the DMA going berserk and Latios seriously injured. After freeing Latios, the Soul Dew fades and turns black, causing the DMA to shut down. When Annie tries to get the Soul Dew, it disappears and the citys water becomes a giant tsunami. Latios and Latias use their psychic powers to stop the tsunami, but Latios sacrifices himself in the process. The water returns to the city, washing away Team Rocket in the process. The undead Pokémon return to their fossil states, and Annie and Oakley get trapped inside the DMA.
 Lawrence III.

==Japanese version==
The Japanese version had a few key differences from the English version.
* Annie and Oakley were not affiliated with Team Rocket.
* The original Soul Dew may or may not have the soul of a Latios inside it at all. The one that appears after Latioss death still does.
* Latioss Japanese voice was not retained in the dub, but Latiass Japanese voice was used in it (though Latiass Japanese singing voice was not retained in the dub).
* The English version edited out the prologue where Annie and Oakley were reciting the legend of Altomare from the book they were about to steal:

A long time ago, Altomare was just a small town. One day, an old couple walking along the beach found two unconscious children lying in the sand. They brought the children home and took care of them. But a dark cloud above Altomare rained down shards of darkness. Anything these shards touched became dark as well. One shard was about to hit the old couple when the children started to glow and all the shards were destroyed. The  children revealed themselves as Latios and Latias. More appeared, one of them carrying the Soul Dew. Their power, combined with the Soul Dews, shone upon the dark cloud, vanquishing it. In gratitude for their help, the two Latios and Latias gave the Soul Dew to the old couple.

==Cast==
{|class="wikitable"
! Character !! Japanese voice !! English voice
|- Satoshi / Ash || Rica Matsumoto || Veronica Taylor
|- Kasumi / Misty || Mayumi Iizuka || Rachael Lillis
|- Takeshi / Brock || Yūji Ueda || Eric Stuart
|-
|  
|-
|   
|-
| Latias || Megumi Hayashibara || Megumi Hayashibara (sounds); Madoka Kimura (singing voice)
|-
| Latios || Masashi Ebara || Megumi Hayashibara
|-
| Musashi / Jessie || Megumi Hayashibara || Rachael Lillis
|-
| Kojiro / James || Shin-ichiro Miki || Eric Stuart
|-
| Zanner/ Annie || Uno Kanda || Megan Hollingshead
|-
| Leon / Oakley || Yumiko Shaku || Lisa Ortiz
|-
| Rossi / Ross || Kōichi Yamadera || Michael Sinterniklaas
|-
| Vongole / Lorenzo || Gutch Yuzo || Wayne Grayson
|-
| Kanon / Bianca || Fumiko Orikasa || Tara Sands 
|-
| Older  
|-
| Younger  
|-
| Nyarth / Meowth || Inuko Inuyama || Maddie Blaustein
|-
| Sounans / Wobbuffet || Yuji Ueda || Kayzie Rogers
|-
| Narrator || Unshō Ishizuka || Rodger Parsons
|}

==Reception==
Pokémon Heroes received generally negative reviews from critics and audiences, earning a Rotten Tomatoes approval rating of 16%.

== References ==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 