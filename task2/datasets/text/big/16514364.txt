Upstream (film)
{{Infobox film
| name           = Upstream
| image          = 
| caption        = 
| director       = John Ford 
| producer       = 
| writer         = Randall Faye Wallace Smith
| starring       = Nancy Nash Earle Foxe
| music          = 
| cinematography = Charles G. Clarke
| editing        = 
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 60 minutes 
| country        = United States  Silent English intertitles
| budget         = 
}}

Upstream is a 1927 American comedy film directed by John Ford. A "backstage drama",    the film is about a Shakespearean actor and a woman from a knife-throwing act. The film was considered to be a lost film,    but in 2009 a print was discovered in the New Zealand Film Archive. 

It is considered to be the first Ford film to show some influence of German director F. W. Murnau, who began working at Fox Studios in 1926. From Murnau, Ford learned how to use forced perspectives and chiaroscuro lighting, which the American director then integrated into his own more naturalistic and direct filmmaking style. 

==Cast==
* Nancy Nash as Gertie Ryan
* Earle Foxe as Eric Brasingham
* Grant Withers as Jack La Velle
* Lydia Yeamans Titus as Miss Hattie Breckenbridge Raymond Hitchcock as Star Boarder
* Emile Chautard as Campbell Mandare
* Ted McNamara as Callahan and Callahan
* Sammy Cohen as Callahan and Callahan
* Judy King as Sister Team
* Lillian Worth as Sister Team
* Jane Winton as Soubrette
* Harry A. Bailey as Gus Hoffman (as Harry Bailey) Francis Ford as Juggler
* Ely Reynolds as Deerfoot

==Recovery==
In 2009 at the invitation of the New Zealand Film Archive, the National Film Preservation Foundation sent consultants Brian Meacham and Leslie Ann Lewis to assess its holdings of long unseen nitrate film prints of American silent films. The cache was found to include astonishing treasures of at least 75 American silent films unknown to exist in the United States, including a complete tinted nitrate print of Upstream and a trailer for another lost John Ford feature, Strong Boy (1929). 

The New Zealand Film Archive turned out to have many American films that had never been shipped back to the United States after they ran in theaters.  The films were supposed to be destroyed at the end of their distribution run, but some were stashed away instead. King, Susan,  , June 7, 2010, Los Angeles Times, retrieved same day   Upstream was considered so important that, unlike other films discovered in the New Zealand archive, it was restored in New Zealand.  

20th Century Fox, a descendant company of the studio that made the movie, supported the preservation of the film in collaboration with the National Film Preservation Foundation and the Academy of Motion Picture Arts and Sciences film archive. Upstream received a "repremiere" at the Academy of Motion Picture Arts and Sciences in September 2010.  and a European screening at Le Giornate del Cinema Muto in Pordenone Italy in October 2010.  Michael Mortilla wrote music for the AMPAS screening, and Donald Sosin for the Pordenone event. 

Only 15 percent of Fords silent films are known to have survived as of 2010. 

==See also==
* List of rediscovered films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 