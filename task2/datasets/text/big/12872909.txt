R.E.M. Live
{{Infobox album
| Name = R.E.M. Live
| Type = live
| Artist = R.E.M.
| Cover = R.E.M. - R.E.M. Live.jpg
| Released =   Ireland
| Genre = Alternative rock
| Length =   English
| Warner Bros.
| Producer = Emer Patten
| Director = Blue Leach
| Last album =   (2006)
| This album = R.E.M. Live (2007)
| Next album = Accelerate (R.E.M. album)|Accelerate (2008)
| Misc = {{Extra chronology
 | Artist = R.E.M.
 | Type = video
 | Last album =   (2006)
 | This album = R.E.M. Live (2007)
 | Next album = This Is Not a Show (2009)
 }}
{{Extra chronology
 | Artist = R.E.M. live album
 | Type = live
 | Last album = Perfect Square (2004)
 | This album = R.E.M. Live (2007) Live from London (2008)
 }}
}}
{{Album ratings
| MC = (65/100)  
| rev1 = Allmusic
| rev1Score =     
| rev2 = The Austin Chronicle
| rev2Score =    
| rev3 = Billboard (magazine)|Billboard
| rev3Score = (positive)   
| rev4 = Entertainment Weekly
| rev4Score = C   
| rev5 = NME
| rev5Score = (7/10)    The Phoenix
| rev6Score =     
| rev7 = Pitchfork Media
| rev7Score = (5.8/10)   
| rev8 = Rockfeedback
| rev8Score =     
| rev9 = Rolling Stone
| rev9Score =    
| rev10 = Spin (magazine)|Spin
| rev10Score = (6/10)   
}}
 
R.E.M. Live is a  .

R.E.M. Live features rare performances of "I Took Your Name" from 1994s Monster (R.E.M. album)|Monster and "Ascent of Man" from Around The Sun as well as the previously unreleased "Im Gonna DJ." A studio version of that song would later appear on the bands 2008 effort, Accelerate (R.E.M. album)|Accelerate.

On September 26, 2007, the band launched R.E.M. Live Zine to solicit reviews from fans and promote the album. 

The DVD has the option for stereo or digital 5.1 surround sound.

==Track listing==
All songs written by Peter Buck, Mike Mills and Michael Stipe except as indicated.
;CD 1
#"I Took Your Name" (Bill Berry, Buck, Mills, Stipe)  (originally on Monster (R.E.M. album)|Monster) &nbsp;– 4:08
#"So Fast, So Numb" (Berry, Buck, Mills, Stipe)  (originally on New Adventures in Hi-Fi) &nbsp;– 4:40
#"Boy in the Well"  (originally on Around the Sun) &nbsp;– 5:16
#"Cuyahoga (song)|Cuyahoga" (Berry, Buck, Mills, Stipe)  (originally on Lifes Rich Pageant) &nbsp;– 4:25
#"Everybody Hurts" (Berry, Buck, Mills, Stipe)  (originally on Automatic for the People) &nbsp;– 6:49
#"Electron Blue"  (originally on Around the Sun) &nbsp;– 4:13
#" ) &nbsp;– 4:26
#"The Ascent of Man"  (originally on Around the Sun) &nbsp;– 4:12 Man on the Moon Soundtrack) &nbsp;– 4:49
#"Leaving New York"  (originally on Around the Sun) &nbsp;– 4:48 Orange Crush" (Berry, Buck, Mills, Stipe)  (originally on Green (R.E.M. album)|Green) &nbsp;– 4:27
#"I Wanted to Be Wrong"  (originally on Around the Sun) &nbsp;– 5:02
#"Final Straw"  (originally on Around the Sun) &nbsp;– 4:10 Imitation of Life"  (originally on Reveal (R.E.M. album)|Reveal) &nbsp;– 3:53 The One I Love" (Berry, Buck, Mills, Stipe)  (originally on Document (album)|Document) &nbsp;– 3:27
#"Walk Unafraid"  (originally on Up (R.E.M. album)|Up) &nbsp;– 5:02 Out of Time) &nbsp;– 4:53

;CD 2
# "Whats the Frequency, Kenneth?" (Berry, Buck, Mills, Stipe)  (originally on Monster (R.E.M. album)|Monster) &nbsp;– 4:06
#"Drive (R.E.M. song)|Drive" (Berry, Buck, Mills, Stipe)  (originally on Automatic for the People) &nbsp;– 5:41
#"(Dont Go Back To) Rockville" (Berry, Buck, Mills, Stipe)  (originally on Reckoning (R.E.M. album)|Reckoning) &nbsp;– 4:39
#"Im Gonna DJ"  (later released on Accelerate (R.E.M. album)|Accelerate) &nbsp;– 2:27 Man on the Moon" (Berry, Buck, Mills, Stipe)  (originally on Automatic for the People) &nbsp;– 6:46

The DVD consists of all of the preceding songs, in the same sequence.
 High Speed Sweetness Follows", The Worst Joke Ever". 

==Personnel==
;R.E.M.
*Peter Buck&nbsp;– guitar background vocals, lead vocals on "(Dont Go Back To) Rockville"
*Michael Stipe&nbsp;– Singing|Vocals, harmonica on "Bad Day"

;Additional musicians
*Scott McCaughey&nbsp;– guitar, keyboards, background vocals drums
*Ken Stringfellow&nbsp;– keyboards, guitar, melodica, background vocals Daniel Ryan of The Thrills&nbsp;– guitar and background vocals on "(Dont Go Back To) Rockville"

==Release history==
The two-CD/DVD version was first released in the United Kingdom on October 15, 2007 and the United States the following day. A triple vinyl LP was released in the UK on January 21, 2008, with the US release on February 19, 2008.

==References==
 

==External links==
* 
* 
*  Flickr pool
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 