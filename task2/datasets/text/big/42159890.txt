So Long Letty (1929 film)
{{infobox film
| title          = So Long Letty
| image          =
| imagesize      =
| caption        =
| director       = Lloyd Bacon
| producer       = The Vitaphone Corporation
| writer         =   (book) Earl Carroll (music, lyrics) Robert Lord (screenplay) Arthur Caesar (screenplay)
| starring       = Charlotte Greenwood
| music          = Louis Silvers
| cinematography = James Van Trees
| editing        = Jack Killifer
| distributor    = Warner Brothers
| released       = October 16, 1929
| runtime        = 64 minutes
| country        = USA
| language       = English
}}
So Long Letty is a 1929 musical comedy directed by Lloyd Bacon and starring Charlotte Greenwood, reprising her role from the 1916 Broadway stage play.    silent So Long Letty in 1920 with Colleen Moore.

At 34:53 into the Pre-Code film, Charlotte Greenwood uses the word "damn", as heard on the Warners On Demand DVD release and broadcast on Turner Classic Movies, an American cable TV channel.

==Cast==
*Charlotte Greenwood - Letty Robbins
*Claude Gillingwater - Uncle Claude
*Grant Withers - Harry Miller
*Patsy Ruth Miller - Grace Miller
*Bert Roach - Tommy Robbins
*Marion Byron - Ruth Davis Helen Foster - Sally Davis
*Hallam Cooley - Clarence de Brie
*Harry Gribbon - Joe Casey
*Lloyd Ingraham - Judge
*Jack Grey - Police Sergeant

==Release and reception==
Film historian Scott Eyman, in his book The Speed of Sound, wrote that So Long Letty, released in late 1929, was part of the wave of more than 70&nbsp;musicals inundating U.S. movie theaters in 1930.  Like most of its genre at the time, it was financially disappointing and "barely broke even", despite the "glorious rowdy Charlotte Greenwood", Eyman said. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 