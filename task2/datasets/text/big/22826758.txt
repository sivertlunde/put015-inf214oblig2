Confucius (2010 film)
 
{{Infobox film name           = Confucius image          = Confucius_film_post.jpg caption        =
| film name = {{Film name| traditional    = 孔子
| simplified     = 孔子
| pinyin         = Kǒng Zǐ
| jyutping       = Hung2 Zi2}}
| director       = Hu Mei
| producer       = Han Sanping Rachel Liu John Shum 
| writer         = Chan Khan He Yanjiang Jiang Qitao Hu Mei Lu Yi Yao Lu
| music          = Zhao Jiping
| cinematography = Peter Pau
| editing        = Zhan Haihong
| studio         = Dadi Century (Beijing) China Film Group distributor    = China Film Group
| released       =  
| runtime        = 115 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = US$18.6 million  
}} biographical drama titular Chinese philosopher. The film was produced by P.H. Yu, Han Sanping, Rachel Liu and John Shum.
 on location in Chinas Hebei province and in Hengdian World Studios in Zhejiang.   

The film was scheduled to screen later in 2009 to commemorate the 60th anniversary of the founding of the Peoples Republic of China, as well as the 2,560th birthday of Confucius himself.  However, the release date was later moved to January 2010.

Funimation released it on 27 March 2012 in English Dub on DVD and Blu-ray.

==Plot==
The film begins with Confucius as an old man, thinking back.  Then we see him in his early 50s, being promoted from Mayor to Minister for Law in his home state of Lu (state)|Lu.  He is confronted with ethical issues after saving a slave-boy who was due to be buried alive with his former master who has just died.  There is a lot of complex politics and war, ending with Confucius being rejected and becoming a wandering scholar.  After many hardships and losses, he is invited back as an old man.  We see him finally preparing the Spring and Autumn Annals, expecting that this book will determine his future influence.

==Cast==
* Chow Yun-fat as Confucius or Kong Qui, also known by the honorary title Zhong Ni.
* Zhou Xun as Nanzi (China)|Nanzi, the royal consort of Wei (Spring and Autumn Period)|Wei.
* Xu Huanshan as Laozi, the sage of Daoism.
* Yao Lu as Duke Lu Dinggong, ruler of Lu.
* Ma Jingwu as Duke Jing of Qi.
* Bi Yanjun as Duke Ling of Wei
* Wang Huichun as Li Chu, a minister in the Qi Court.
* Li Huan as Kuai Kui, crown prince of Wei.
* Chen Jianbin as Ji Sunsi, head of the Jishi Clan and minister in the Court of Lu. Lu Yi as Ji Sunfei, the son of Ji Sunsi.
* Wang Ban as Shu Sunwu, head of the Shushi Clan and minister of the Court of Lu.
* Wu Liansheng as Meng Sunhe, head of the Mengshi Clan and minister in the Court of Lu.
* Kai Li as Lady Qiguan, the wife of Confucius.
* Qiao Zhenyu as Kong Li, son of Confucius.
* Chen Rui as Kong Jiao, the daughter of Confucius. Yan Hui, disciple of Confucius.
* Li Wenbo as Zilu, disciple of Confucius.
* Ma Qiang as Ran Qiu, disciple of Confucius.
* Kan Jinming as Zigong, disciple of Confucius.
* Liu Fengchao as Qi Sigong, a slave in the house of the Jishi Clan.
* Gao Tian as young Qi Sigong
* Liu Yongchen as Gongxi Chi Zeng Shen
* Wang Qingyuan as Zeng Dian, disciple of Confucius.
* Ma Yong as Gongbo Liao Ziyou
* Zigao
* Tang Muchun as Zixia
* Luo Minghan as Ran Yong
* Li Xinru as Nichang
* Gong Jie as Gong Ye
* Zhang Xingzhe as Gongshan Niu, a retainer of the Ji Clan and mayor of Biyi City.
* Dong Ziwu as Yan Zhuoju, Zilus brother in law.
* Gu Yang as Hou Fan
* Huang Wenguang as Ji family servant
* Ji Yongqing as Shen Juxu
* Zhou Jiantao as Yue Shuo

==Awards and nominations==
30th Hong Kong Film Awards
*Nominated – Best Actor (Chow Yun-fat)
*Nominated – Best Cinematography (Peter Pow)
*Nominated – Best Art Direction
*Nomianted – Best Costume Design
*Nominated – Best Original Song (Faye Wong)

==Music==
Faye Wong sang the theme song for the film. Her "soothing and ethereal voice" was considered appropriate for the lofty spirit of the song, "Solitary Orchid" ( ), which is based on an ancient work by Han Yu. Wong, a Buddhist, stated that she recorded the song "for Confucius" as his writings still provide the answers to modern questions. {{Cite web
| date = 8 January 2010
| title =Faye Wong returns for film Confucius
| work = China Central Television
| publisher = China.org.cn
| url =http://www.china.org.cn/video/2010-01/08/content_19203200.htm
| accessdate =2010-04-24
}} 

==Controversies==

===Choice of actors===
After the project was announced, the reaction in China was decidedly mixed. As the film is made in Mandarin, many have expressed concern that Chow, a native of the Cantonese-speaking Hong Kong SAR, will lack the requisite Mandarin language|Mandarin-speaking skills to portray the revered philosopher.  Others were concerned that Chow, a veteran of action and Kung Fu-cinema, would turn Confucius into a "kung-fu hero."  Such concerns were only exacerbated after mainland star Pu Cunxin criticized Hu Meis script as containing inappropriate levels of action and romance for a film based on Confucius life. 

In his review of the movie, Perry Lam of Muse (Hong Kong Magazine)|Muse has criticized Chow for being the least likely actor to play the title role. 

===Kong Jian lawsuit===
In December 2009, more controversy arose when a claimed-direct descendent of Confucius brought suit against the film-makers. After seeing the films trailer, the descendent, Kong Jian, sought to have several scenes deleted from the release of the film and objecting to the intimations that Confucius was romantically attracted to the concubine, Nanzi. 

===Screening===
During the films launch in China, the Hollywood blockbuster Avatar (2009 film)|Avatar was reportedly going to be pulled from nearly 1,600 2-D screens across China, to benefit the wide release of this film.     Instead, Avatar showings continued in the fewer, but more popular 900 3-D screens throughout China, which generated over 64% of the films total ticket sales in China.      The Hong Kong newspaper Apple Daily speculates that the Chinese authorities were worried Avatar had seized the market share from domestic films and noted that many of the vacant cinema slots will be replaced by Confucius,  and the film would be "drawing unwanted attention to the sensitive issue" concerning forced evictions of Chinese homes.   However, Chinas State Administration of Radio, Film and Television responded by stating it was a "commercial decision", and because the "box office performance of the 2D version has not been great."   However, due to low attendance for Confucius, and high demand for Avatar, the Chinese government reversed their decision, and allowed Avatar to remain on some 2-D screens in China.  This choice appeared to be at least partly based on the financial performance of the two films, with Avatar grossing nearly 2.5 times more money per day. 

==DVD release== Region 2. 

==See also==
*Chow Yun-fat filmography
*List of historical drama films of Asia

==References==
 

==External links==
*   
* 
*  at the Chinese Movie Database

 
 
   

 
 
   
 
 
 
 
 
 
 