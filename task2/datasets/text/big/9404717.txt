The Lady Vanishes (1979 film)
 
 
{{Infobox film
| name           = The Lady Vanishes
| image          = The Lady Vanishes 1979.jpg
| image_size     = 200px
| caption        = 1979 theatrical poster.
| director       = Anthony Page
| producer       = Tom Sachs
| based on       =  
| screenplay     = George Axelrod 
| starring       = Elliott Gould Cybill Shepherd Angela Lansbury Herbert Lom Arthur Lowe Ian Carmichael
| music          = Richard Hartley
| cinematography =
| editing        =
| distributor    = 1979
| runtime        = 97 minutes (US) /  95 minutes (UK)
| country        = United Kingdom
| language       = English
| budget         =₤2 million Tom Johnson and Deborah Del Vecchio, Hammer Films: An Exhaustive Filmography, McFarland, 1996 p379  or £2.5 million The lucrative case for believing in yesterday
The Guardian (1959-2003)   18 Dec 1978: 11. 
}} British comedy comedy mystery film directed by Anthony Page. Its screenplay by George Axelrod was based on the novel The Wheel Spins by Ethel Lina White (1876–1944). It stars Elliott Gould as Robert, Cybill Shepherd as Amanda (Iris), Angela Lansbury as Miss Froy, Herbert Lom, Arthur Lowe and Ian Carmichael as Charters and Caldicott.
 1938 film British to US|American.
 Hammer Films for 29 years, until the 2008 film Beyond the Rave. 

==Plot==
In August 1939, a motley group of travellers find themselves in a small hotel in Bavaria, awaiting a delayed train to Switzerland. They include a "much married madcap American heiress", Amanda Metcalf-Midvani-Von Hoffsteader-Kelly, and Robert Condon, a wise-cracking American photographer.
 test match, and Todhunter, an English diplomat larking about with his mistress and Dr. Egon Hartz.

When she wakes up, Miss Froy has vanished. Her fellow travellers deny seeing Miss Froy and declare that she never existed. Amanda begins to doubt her own mental condition. Amanda starts to investigate, joined only by a sceptical Condon. The train stops to pick up a badly burnt and heavily bandaged automobile accident victim. Shortly thereafter, a "Miss Froy" apparently reappears, but it is not her.

The train resumes its journey and Amanda is attacked. Miss Froys broken glasses are found and Condon now believes Amandas story. They surmise that Miss Froy was lured to the baggage car and is being held captive - and that the heavily bandaged accident victim is now Miss Froy. This proves to be the case and Dr. Hartz attempts to drug them - but his wife (disguised as a nun) has not put any drug in their drinks.

At the next station, the train is shifted onto a branch line and only the buffet car and one carriage are left. The train stops and Helmut von Reider, an SS officer (son of Miss Froys former employer), approaches the train, demanding that Miss Froy be surrendered. The passengers refuse and a gunfight ensues. Miss Froy chooses this moment to confess that she is in fact a courier with a vital coded message (she hums a tune to them) that must be delivered to London. She leaves the train and disappears. Condon, Charters and Caldicot contrive to take over the engine and drive the train back to the main line and over the Swiss border.
Back in London at the Foreign Office, the duo attempt to remember the tune she sang, then suddenly they hear someone humming the same tune. It is Miss Froy who managed to escape her captors.

==Cast==
* Elliott Gould - Robert Condon
* Cybill Shepherd - Amanda
* Angela Lansbury - Miss Froy
* Herbert Lom - Doctor Hartz Charters
* Caldicott
* Gerald Harper - Mr Todhunter
* Jenny Runacre - "Mrs" Todhunter
* Jean Anderson - Baroness
* Madlena Nedeva - Nun
* Madge Ryan - Rose Flood Porter
* Rosalind Knight - Evelyn Barnes
* Vladek Sheybal - Trainmaster
* Wolf Kahler - Helmut
* Barbara Markham - Frau Kummer

==Production== Tony Williams of Rank who agreed to finance.   accessed 16 April 2014  Williams had recently agreed to finance a remake of The 39 Steps; he defended the idea of remaking a classic:
 The old films suffer technically against todays. The pace of modern films is much faster. The style of acting is different. Those old actors were marvellous, but if you consult the man in the street, hes more interested in seeing a current artist than someone whos been dead for years.  
"What were competing with here is not the real picture but peoples memory of it," said George Axelrod. "Hitchcocks film had some brilliant things in it, but as a whole picture youd have to admit its pretty creaky. The four or five things people remember from the original receive a homage in our version - which raises the question of when a homage becomes a rip off." 

Axelrod admitted the script was "not like the stuff I normally do, which is two people in and around a bed" but he agreed to do the adaptation because "this picture is actually going to be shown in theatres for actual people to see". Mills, B. (28 January 1979). Movies. Chicago Tribune (1963-Current File). Retrieved from http://search.proquest.com/docview/171835421?accountid=13902  Axelrod had gotten involved in the movie originally by ABC TV who wanted him to write a version of Murder on the Orient Express (1974) - he suggested they buy the rights to Night Train or The Lady Vanishes. He ended up writing three different versions of The Lady Vanishes for ABC TV but none was picked up. Then the rights reverted to Rank Films, who asked Axelrod to work on the movie. 

Among Axelrods changes to the original were setting the new film in 1939 Germany, and altering the hero to a photographer from Life Magazine and the heroine to be a screwball "rompy, Carole Lombard character." Mann, R. (22 October 1978). MOVIES. Los Angeles Times (1923-Current File). Retrieved from http://search.proquest.com/docview/158662943?accountid=13902  The script was constantly rewritten as filming went along. 

George Segal and Ali MacGraw were originally announced for the leads. PRODUCTION INCREASES: Fog Lifts on British Film Industry
Tuohy, William. Los Angeles Times (1923-Current File)   30 Dec 1977: g1. 

==Reception== Time Out notes that "Comparisons are odious, but this remake of Hitchcocks thriller continually begs them by trampling heavily over its predecessor."  The British Film Institute is more critical, calling it "about as witless and charmless as could be conceived". 

Variety magazine notes that the script is "best when dwelling on English eccentricity to make the films most endearing impression...Shepherd and Gould stack up as contrived cliches, characters that jar rather than complement."  Film4s review agrees, writing that the two leads are "ruthlessly upstaged by loveable old coves Arthur Lowe and Ian Carmichael as cricket-mad Charters and Caldicott". It, however, calls it a "watchable remake". 

==References==
 

== External links ==
*  
*  
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 