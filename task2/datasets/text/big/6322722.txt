Broken Bridges
{{Infobox film
| name           = Broken Bridges
| image          = Broken bridges.jpg
| caption        = Promotional poster for Broken Bridges.
| director       = Steven Goldmann
| producer       = Sara Risher Donal Zuckerman
| eproducer      =
| aproducer      =
| writer         = Cherie Bennett Jeff Gottesfeld
| starring       = Toby Keith Kelly Preston Lindsey Haun Burt Reynolds
| music          = Toby Keith
| cinematography = Patrick Cady
| editing        = Maysie Hoy Paramount Classics
| released       =  
| runtime        = English 
| gross          = $252,539
}}
 2006 film starring Toby Keith, Lindsey Haun, Burt Reynolds and Kelly Preston. The film, a music-drama, is centered on a fading country singers return to his hometown near a military base in Kentucky where several young men who were killed in a training exercise on the base were from. He is reunited with his former sweetheart and estranged daughter, who returns to the town as well.

==Plot==

Bo Price (Keith), a down-and-out country singer, has returned home for his brothers funeral following a military training accident.  While there, he reunites with his true love, Angela Delton (Preston), a Miami news reporter who has also returned home for her brothers funeral.  Bo also meets their 16-year-old daughter, Dixie Leigh Delton (Haun), for the first time. Since Bo walked away from Angela while she was still pregnant, Dixie has never met him or his side of the family.  Dixie has experimented with alcohol, but is able to break free with the help of her now-sober father.  With her fathers musical blood running through her veins, Dixie closes the movie by singing a song she wrote at the memorial for the fallen soldiers.

==Critical reaction==
Overall critical reaction was negative.  Most critics cited Toby Keiths wooden acting, as well as the blatant product placement by Keiths sponsor, Ford, as major detriments to the film. The general negative consensus was reflected by the poor box office gross of  $252,539 in four weeks of release.   However, the film has sold over $8 million in DVD sales, and repeats often on Country Music Television|CMT, Country Music Television. 

==Soundtrack==

# Broken Bridges - Toby Keith & Lindsey Haun
# Thinking About You - Fred Eaglesmith
# Crash Here Tonight - Toby Keith
# Broken - Lindsey Haun
# Along for the Ride - Matraca Berg
# Uncloudy Day - Willie Nelson, Toby Keith, & BeBe Winans
# Whats Up With That - Scotty Emerick
# High on the Mountain - Flynnville Train
# The Battlefield - Sonya Isaacs
# Cant Go Back - Toby Keith
# The Waiting Game - Poor Richards Hound
# Big Bull Rider - Toby Keith
# Zig Zag Stop - Toby Keith
# Jacky Don Tucker (Play By The Rules Miss All The Fun) - Toby Keith

==Cast==
* Toby Keith as Bo Price
* Kelly Preston as Angela Delton
* Lindsey Haun as Dixie Leigh Delton
* Tess Harper as Dixie Rose Delton
* Burt Reynolds as Jake Delton
* Willie Nelson as Himself
* BeBe Winans as Himself
* Seth Chalmers as Jerome

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 