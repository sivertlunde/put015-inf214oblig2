Twixt (film)
{{Infobox film
| name           = Twixt
| image          = Twixt_poster.JPG
| caption        = Planned theatrical release poster
| director       = Francis Ford Coppola
| producer       = Francis Ford Coppola
| screenplay     = Francis Ford Coppola
| starring       = {{Plainlist|
* Val Kilmer
* Bruce Dern
* Elle Fanning
* Ben Chaplin 
}}
| music          =  
| cinematography = Mihai Mălaimare, Jr.
| editing        = Robert Schafer
| studio         = American Zoetrope
| distributor    = Pathé
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $368,086   
}}
Twixt is a 2011 horror thriller film written, directed and produced by Francis Ford Coppola starring Val Kilmer and Elle Fanning, The film premiered on September 4, 2011 at the Toronto International Film Festival and was screened at various film festivals in North America, receiving a limited theatrical release in a handful of International markets. The films title, Twixt, refers to the two worlds explored in the film, the dream and the waking worlds.

Twixt was released on Blu-ray Disc and DVD by Fox Home Entertainment on July 23, 2013, and the film marks the on-screen reunion of Val Kilmer and Joanne Whalley.

==Plot synopsis==
The film follows Hall Baltimore ( . This, along with the murders and various other odd features of the town, prompt Hall to announce to his wife that he wants to write a piece based upon the town.

After falling asleep, Hall is shown wandering a dream-like version of the town, where he meets V (Elle Fanning), a young girl by the name of Virginia that is nicknamed "Vampira" due to her strange teeth and braces. V tells Hall that shes a fan of his work but was unable to attend the signing due to the towns clock tower always giving seven different conflicting times. Hall attempts to persuade V to join him at the hotel for a soda, but she refuses to enter the lodging. Despite this, Hall enters the hotel and discovers that it is run by a strange and eccentric couple that talks about daylight savings and the towns history of murder. V appears in the window, only for the female hotel owner to shoo her away. V bites her, which prompts Hall to run outside after V and find her threatening a priest by saying that he "knew what   did". Hall continues to follow V and runs into Edgar Allan Poe, who guides him back to town.

The next morning Hall wakes and inspired, decides that he would like to collaborate with Bobby on his proposed story, which would center around vampires. Bobby invites Hall to his home, which contains a miniature model of a machine that would drive a stake into a vampires heart. Hall is informed that Bobby believes that the machine would be good in the story, which he wants to name The Vampire Executions. Despite Bobbys input, Hall finds himself faced with writers block and accepts several sleeping pills from Bobby in hopes of finding further inspiration from his dreams. Hall succeeds in once again entering the dream version of the town, where he meets Poe again. A series of visions imply that V was molested by the towns priest, who takes in orphans out of fear of them joining Flamingo (Alden Ehrenreich), leader of the people across the lake that are believed to be vampires. In the waking world Halls world begins to unravel as several strange occurrences begin to mirror the experiences Hall had in the dream world, such as Bobby talking about how he believes that teenagers gathering at the towns lake are "evil" and "asking for it". This prompts Hall to look at Bobby strangely, which causes him to knock Hall unconscious.

In the dream world Hall learns that the priest had drugged and murdered all the children in order to keep them from joining Flamingo at the lake. V managed to briefly survive, but was chased down and murdered by the priest. Ashamed of his actions, the priest hangs himself. Hall realizes that his writers block is the result of a form of guilt over his daughters boating accident death, as he had been too drunk to accompany her that morning. Working through his emotions, Hall writes a story where V survives the priests assaults and is rescued by Flamingo. Hall wakes up in his room to find that Bobby has left. He goes to the sheriffs office, only to find that the deputy has been murdered and that Bobby has hanged himself with a note that says "Guilty". Upset, Hall goes down to the morgue to look at the murder victims face, only to discover that it is Vs body and that she had been impaled with a stake. Hall removes the stake from her chest, at which point V awakens and attacks him with a new set of elongated fangs.

The screen then cuts to the publisher setting down a new manuscript and telling Baltimore that he loves the story (implying everything from the first dream sequence up to this point has been the manuscript itself) and sees a whole new series ahead for Baltimore that will make him more popular than ever. Post-film text reveals that the series was popular, LaGranges murder was never solved and that Flamingo was never seen or heard from again.

==Cast==
*Val Kilmer as Hall Baltimore
*Elle Fanning as Virginia "V"
*Joanne Whalley as Denise
*Bruce Dern as Bobby LaGrange
*Ben Chaplin as Edgar Allan Poe
*Don Novello as Melvin
*David Paymer as Sam Malkin
*Alden Ehrenreich as Flamingo
*Lisa Biales as Ruth
*Anthony Fusco as Pastor Allan Floyd
*Ryan Simpkins as Carolyne
*Lucas Rice Jordan as P. J.
*Bruce A. Miroglio as Deputy Arbus
*Tom Waits as the narrator 

==Production==

===Development===

In an interview with The New York Times, Coppola discussed the origins of the film, which he said "grew out of dream   had last year – more of a nightmare" and "seemed to have the imagery of Hawthorne or Poe." He continued:

 But as I was having it I realized perhaps it was a gift, as I could make it as a story, perhaps a scary film, I thought even as I was dreaming. But then some loud noise outside woke me up, and I wanted to go back to the dream and get an ending. But I couldnt fall back asleep so I recorded what I remembered right there and then on my phone. I realized that it was a gothic romance setting, so in fact Id be able to do it all around my home base, rather than have to go to a distant country.  

===Filming and post-production=== Kelseyville and Nice, California|Nice. 

Musician Dan Deacon scored the film.  The films name was changed from Twixt Now and Sunrise to Twixt, and scenes from it were played at the July 2011 San Diego Comic-Con International. 

==Release==
Twixt was screened solely at film festivals in North America as it was a featured film at the November 2011 American Film Market.   The film received a general theatrical release in a handful of international markets. 

==Reception==
Critical reception for Twixt has been predominantly negative and the film currently holds a rating of 29% "rotten" on the review aggregator website Rotten Tomatoes based upon 17 reviews.  Reviewers have criticized the film for being "flimsy" and "  unwatchable".   The Hollywood Reporter panned Coppolas dream sequences in Twixt, commenting that the "young Coppola" could have done more with the scenes while "the present-day   produces only tepid and tired imagery that would not earn high marks in any film school." 

In contrast, Le Monde and Variety (magazine)|Variety gave the film positive reviews,  and Variety praised Twixt as a "disarmingly cheeky, intermittently gorgeous trifle". 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 