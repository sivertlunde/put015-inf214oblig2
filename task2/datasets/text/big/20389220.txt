Modern Boy
 
{{Infobox film
| name           = Modern Boy
| image          = Modern Boy film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | rr             = Modeon boi
 | mr             = Modŏn poi}}
| director       = Jung Ji-woo
| producer       = Kang Woo-suk
| writer         = Jung Ji-woo
| starring       = Park Hae-il Kim Hye-soo
| music          = Lee Jae-jin
| cinematography = Kim Tae-gyeong
| editing        = Eom Yun-ju Wang Su-an
| distributor    = CJ Entertainment
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} 2008 South Korean film.

== Plot == Japanese colonization of his homeland. But things change when he falls in love with bar singer Jo Nan-sil, who turns out to be a member of the Korean independence movement.

== Cast ==
* Park Hae-il ... Lee Hae-myeong
* Kim Hye-soo ... Jo Nan-sil
* Kim Nam-gil ... Shinsuke, Japanese detective
* Kim Joon-bae ... Baek Sang-heo Kim Young-jae ... Okai
* Shin Goo ... Lee Hae-myeongs father
* Joo Seok-tae ... Policeman
* Hong Seung-jin ... Cheol-kwon
* Do Ji-won ... Ishida Yoko

== Release ==
Modern Boy was released in  3,839,780. " ". Box Office Mojo. Retrieved on 25 November 2008. 

==Awards and nominations==
;2008 Blue Dragon Film Awards 
* Best Visual Effects
* Nomination - Best Cinematography - Kim Tae-gyeong
* Nomination - Best Art Direction - Jo Sang-gyeong
* Nomination - Best Music - Lee Jae-jin

;2008 Korean Film Awards
* Nomination - Best Art Direction - Jo Sang-gyeong
* Nomination - Best Music - Lee Jae-jin
* Nomination - Best Sound

;2009 Grand Bell Awards
* Nomination - Best Supporting Actor - Kim Nam-gil
* Nomination - Best Art Direction - Jo Sang-gyeong
* Nomination - Best Costume Design
* Nomination - Best Visual Effects
* Nomination - Best New Actor - Kim Nam-gil

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 