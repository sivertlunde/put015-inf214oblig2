The Lovers of Teruel (film)
{{Infobox film
| name           = The Lovers of Teruel
| image          =
| caption        =
| director       = Raymond Rouleau
| producer       =
| writer         = René-Louis Lafforgue Raymond Rouleau
| starring       = Ludmilla Tchérina
| music          = Mikis Theodorakis
| cinematography = Claude Renoir
| editing        = Marinette Cadix
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         =
}}

The Lovers of Teruel ( ) is a 1962 French musical film directed by Raymond Rouleau. It was entered into the 1962 Cannes Film Festival.   

==Cast==
* Ludmilla Tchérina - Isa
* René-Louis Lafforgue - Barker (as René-Louis Laforgue)
* Milko Sparemblek - Manuel
* Milenko Banovitch - Diego
* Stevan Grebel - Grebelito
* Jean-Pierre Bras - Father
* Antoine Marin - Pablo
* Roberto - Dwarf
* Luce Fabiole
* Jeanne Herviale
* Philippe Rouleau - Lamoureux
* Jean Saudray - Le cycliste
* Bernadette Stern - Lamoureuse

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 