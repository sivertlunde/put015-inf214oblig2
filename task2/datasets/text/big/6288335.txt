Cannon for Cordoba
 
{{Infobox film
|  name           =Cannon for Cordoba 
|  image          = CannonCover.jpg 
  
|  director    = Paul Wendkos 
|  writer      = Stephen Kandel
|  starring    = George Peppard Giovanna Ralli Raf Vallone Pete Duel
|  producer  = Vincent M. Fennelly
|  music          = Elmer Bernstein 
|  cinematography =  Antonio Macasoli
|  editing        =  Walter A. Hanneman
|  distributor = United Artists
|  released    = October, 1970  (United States|USA) 
|  runtime     = 104 min.
|  language    = English
|}}

Cannon for Cordoba was a western film that was released in 1970. Filmed in Spain, the large part of the movie takes place in Mexico in 1912. Directed by Paul Wendkos, it stars George Peppard, Pete Duel, Giovanna Ralli, and Raf Vallone, and features a musical score by Elmer Bernstein.

==Plot== John J. John Russell) in charge of seeing that the cannons will never be used against the American people. Pershing turns to Captain Rod Douglas (George Peppard), instructing him gather a group of men to take part in the dangerous mission into the heart of the Cordobas territory.
 Don Gordon), a soldier who has worked with Douglas before. At the beginning of the film, Harkness has to stand by and watch as his brother is tortured and killed by Cordoba. Douglas ordered him not to step in because they were undercover as sympathizers in the enemy camp and could not afford to make their true intentions known. As a result, Harkness vows vengeance on the captain and will not leave his side until the opportunity presents itself.
 Gabriele Tinti), who holds a personal grudge against Cordoba, approaches him and demands to be part of the operation. He tells Douglas that he knows a woman, Leonora Cristobal(Giovanna Ralli) who, for her own reasons, wishes to see Cordoba dead. If the captain includes him in the mission, she will help them by working her way into Cordoba’s confidence and getting him alone so that he will be vulnerable when they make their move. 

Antonio and Leonora arrive at Cordoba’s camp first. Leonora, who learns that the Mexican government wants to capture Cordoba alive, betrays Antonio and informs the bandit leader of his intentions, hoping that he will reward her for what she has done by allowing her to get closer to him, giving her the opportunity to kill him herself.

When Douglas, Andy, Peter, and Harkness arrive at the camp, posing as sympathizers, they hear of what Leonora has done and decide that they have to act quickly. Douglas starts a fight with one of the Mexican men, so as to be put in jail, where he can help Antonio to escape. That night, Andy, disguised as a Mexican guards, breaks both of the men out of jail so that the operation can proceed. Douglas goes to Cordoba’s room, where he finds him alone with Leonora. She betrayed Antonio but she still did the job she was supposed to do. Meanwhile, Jackson and Peter have turned the cannons on the camp and begin to fire, while Andy and Antonio shoot flares into the buildings. Chaos ensues and the group of men, along with Leonora and their prisoner, Cordoba, attempt to ride out of the camp. Peter, Antonio, and Andy are killed in the process, and Cordoba is wounded.

The next morning, miles away from the camp, the diminished group stops to rest. When Douglas goes off by himself, Harkness sees his opportunity to avenge his brother. He follows the captain, demands that he turn around, and draws his gun. As Douglas walks unflinchingly toward him, however, he is unable to shoot and, instead, punches him. All now forgiven, the two men walk back to where Leonora waits. Cordoba has died from the wound he received the previous night. They are not able to bring him back alive, as the government had wanted, but the cannons were destroyed and their mission is complete.

This film is also seen as a counter revolutionary film that served the Nixon administration war on anti-Vietnam war demonstrators and young radical groups in the US.

==Cast==
*George Peppard....Captain Rod Douglas
*Pete Duel....Andy Rice
*Giovanna Ralli....Leonora Cristobal Don Gordon....Jackson Harkness
*Raf Vallone....General Héctor Cordoba
*Nico Minardos....Peter Gabriele Tinti....Lieutenant Antonio Gutierrez
*John Larch....Warner
*Francine York....Sophia John Russell....John J. "Blackjack" Pershing
*Lionel Murton....Colonel Hammond Hans Meyer....Svedborg
*Richard Pendrey....Adam, Jacksons brother

==Reception==
Arthur Krim of United Artists later did an assessment of the film as part of an evaluation of the companys inventory:
 In 1970 there was a marked change in global acceptance of western and adventure film. The results of films of other companies - for instance Mackennas Gold, Murphys War, The Last Valley - as well as our own - Play Dirty, The Bridge at Remagen - indicated a need for substantial downward revisions in assessing proper budget costs for pictures in this category - even with the so-called big name action stars. This picture falls in that category. If programmed today, it would be considered acceptable only if it could be made at half cost.  

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 