Heartsounds
 
Heartsounds is an  . 1980. ISBN 0-671-24329-2  A 1984 made-for-television movie was based on the book.

==Background==
The book is about Lears husband, Harold Alexander Lear, a   

On her husbands double bypass coronary operation, Martha Lear wrote: 

: "No other surgery affects people in quite this way. For it is unthinkable, finally, that one’s heart should be cut open. It is the one unthinkable cut."

==Film==
The book was made into a     The script was drafted by Fay Kanin, who also was one of the co-producers, along with Fern Field, and the film was directed by Glenn Jordan.  James Garner played the part of Harold Lear, while Mary Tyler Moore played Martha Lear.  It also stars Sam Wanamaker, as Moe Silverman, and Wendy Crewson, as Judy. Norman Lear was the executive producer for the project.  The movie was shot in Toronto, Ontario, Canada, with additional exterior footage shot in New York City, New York.  Toronto stood in for New York City for economic reasons.  The film debuted on ABC September 30, 1984.   	

Garner was nominated for a Golden Globe, for Best Performance by an Actor in a Mini-Series or Motion Picture Made for TV, and for an Emmy, for Outstanding Lead Actor in a Limited Series or a Special. Mary Tyler Moore was nominated for an Emmy, for Outstanding Lead Actress in a Limited Series or a Special. The producers, Norman Lear, Kanin, and Field, were also nominated for an Emmy, for Outstanding Drama/Comedy Special. The casting director, Eve Brandstein, was nominated for an Artios, for Best Casting for TV Miniseries or TV Movie of the Week. ABC won a Peabody Award. 

==Bibliography==
Lear, Martha Weinman. Heartsounds. New York: Simon and Schuster. 1980. ISBN 0-671-24329-2

==References==
 

==External links==
* 
* 
* 
*  at Archive of American Television

 
 
 
 
 
 