Bandhan (1991 film)
 
{{Infobox film
| name           = Bandhan - Marathi Movie
| image          = Bandhan Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Anant Mane
| producer       = Chelaram BhatiaLalchand Bhatia
| screenplay     = 
| story          = 
| starring       = Ajinkiya Dev Nishigandha Wad Ramesh Bhatkar Asha Kale Vasant Shinde
| music          = Anil Mohile
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Bandhan is a Marathi movie released on 5th Match 1991.  The movie is produced by Chelaram Bhatia and Lalchand Bhatia and directed by Anant Mane.  The plot of the movie is based on a woman’s persistence that men are inferior and the extent she would go to prove her point. 

== Synopsis ==
The protagonist of this film Pramila, is a modern girl who harbours hatred towards men. She is the Secretary of a womens awareness group and believes that men marry women to treat them like slaves. One day, Mohan (the hero), attends one of the women’s group meeting with his sister where Pramila is addressing the gathering on the topic of Men. 

The lecture is converted into a debate and Pramila states that women are superior to men. She challenges Mohan that she will do whatever he can! Taking up the challenge he says, “I am willing to marry you, are you ready to do the same?” This causes a commotion among the audience but Pramila accepts the challenge with an intention of teaching him a lesson.

== Cast ==

The cast includes, Ajinkiya Dev, Nishigandha Wad, Ramesh Bhatkar, Asha Kale, Vasant Shinde & Others.

==Producers==
The Bhatia duo: Chelaram Bhatia and Lalchand Bhatia.(Glamour Films)



==Soundtrack==
The music has been provided by Anil Mohile. 

===Track listing===
{{Track listing
| extra_column = Performer(s)
| title1 = Satyavan Savitrichi Katha | extra1 = Jyotsna Hardikar | length1 = 5:08
| title2 = Jhimma Jhimma Pori | extra2 = Anupama Deshpande, Jyotsna Hardikar | length2 = 6:41
| title3 = Majha Sajana | extra3 = Anupama Deshpande,Sudesh Bhosle | length3 = 5:43
| title4 = Naravina Naricha | extra4 = Ravindra Sathe | length4 = 5:46
| title5 = Radu Nako | extra5 = Ravindra Sathe | length5 = 5:25
}}

== References ==
 
 

== External links ==
*  

 
 