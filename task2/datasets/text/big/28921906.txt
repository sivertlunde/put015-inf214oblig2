Roland the Mighty
 {{Infobox film
| name           = Roland the Mighty
| image          = Roland the Mighty-797436429-large.jpg
| image size     =
| caption        =
| director       = Pietro Francisci
| producer       =  
| writer         =  
| starring       = Rik Battaglia   Rosanna Schiaffino
| music          =  Angelo Francesco Lavagnino
| cinematography =  Mario Bava 
| editing        =  
| distributor    =
| released       =  1956
| runtime        =  
| country        =  
| language       = Italian
| budget         =
}} Frankish army as it retreated across the Pyrenees.

This film was directed by Pietro Francisci and written by Ennio De Concini.  Famed Italian horror director Mario Bava was the cinematographer on this film.

==Cast==
* Rik Battaglia as Roland 
* Rosanna Schiaffino as Angélique
* Fabrizio Mioni as Renaud	 
* Lorella De Luca as Aude
* Ivo Garrani as Charlemagne		
* Cesare Fantoni as Agramante 	
* Clelia Matania as Mary
* Mimmo Palmara as Argalie	
* Vittorio Sanipoli as  Ganelon	
* Robert Hundar as Balicante	
* Rossella Como   as 	Dolores		 	
* Pietro Tordi as Ubaldo

==See also==
* list of historical drama films

==External links==
*  

 
 
 
 
 
 
 

 