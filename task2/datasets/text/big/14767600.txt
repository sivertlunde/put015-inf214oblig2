Aashayein
 
 
{{Infobox film
| name=Aashayein
| image          = Aashayein-.jpg
| alt            =  
| caption        = Film poster
| director       = Nagesh Kukunoor
| producer       = Percept Picture Company T-Series
| writer         = Nagesh Kukunoor John Abraham Sonal Sehgal Prateeksha Lonkar Girish Karnad Farida Jalal Ashwin Chitale Anaitha Nair
| music          = Pritam Shiraz Uppal Salim-Sulaiman
| cinematography = Sudeep Chatterjee
| editing        = Apurva Asrani
| studio         = Fuji productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          =     
}} John Abraham and Sonal Sehgal in the lead roles. 

==Plot== John Abraham) is a compulsive gambler and while betting, wins 30&nbsp;million rupees. That night he asks his girlfriend, Nafisa (Sonal Sehgal), to marry him and she agrees. Right after proposing Rahul collapses. The next day he goes to the doctor for a medical test and finds out that he has cancer. Rahul does not know how to handle this situation, in the middle of the night  runs down a street screaming, knowing that he only has months to live. Later, Rahul finds a rehabilitation centre for persons with such incurable diseases. Rahul runs away from his fiancee, leaving back most of the money he earned, for her.
Once Rahul gets into the centre, he meets several people like Parthasarthi (Girish Karnad) who speaks with the help of  a metal tube because of his cancer, Madhu (Farida Jalal) who was a prostitute and an AIDS patient now, Padma (Anaitha Nair) a teenage girl with lot of dreams, but no life to fulfill them, she cant even walk properly, Govinda (Ashwin Chitale) a comic loving kid, who is said to have certain higher capabilities and everybody treats him as a messenger of God.
The relationship between Rahul and each of these persons grows with time. One day Rahul coughs hard and struggles for breath, and he goes to Govinda’s room and falls down there.

Next day when Rahul wakes up, Govinda offers him mangoes and he tells him a story. The story was similar to the story of Rahul’s dreams, in which he imagines himself as Indiana Jones. In the story he is in a mission to free many souls who are in chains and to get the keys of those locks, he needs to find a key of a box, in which all the keys are there. Rahul imagines himself to be Indiana Jones, and Parthasarathy, Madhu etc. as the locked spirits, and Padma as a spirit who motivates Rahul to find the key.

Later, he realises that the key to open the box is his heart, and his love is the key to open all the other chainlocks. He then arranges a beach program with Padma during which, he brings a band troupe that sings about living in the present. Prominent actor Shreyas Talpade appears as the lead singer in this song. After that, Rahul asks everybody to write down their one wish and put it in a pot. Then he and Padma exchanges their wishes. They together fulfill everybody’s wishes, and meanwhile Rahul and Padma were about to be in a physical relationship, but Rahul regrets and rejects. Padma gets angry and throws off her wig to Rahul; she is bald, due to her disease. Padma’s condition gets worse and Rahul looks for her wish pot. He finds that her last wish is to make love with Rahul. Rahul agrees and kisses Padma, and she dies at the moment.

Rahul finds an Indiana Jones costume in his room, which he had wished for and given to Padma. Rahul’s condition gets worse, and Nafisa re-enters the story at that moment. It is revealed that Padma had called Nafisa before her death and asked her to take care of Rahul. Meanwhile, Rahul is in desperate search of some ‘Naksha’ (Treasure Map) which Govinda had told him to find. He then realises the Naksha is near some waterfall, and the closing scene of the movie shows Rahul and Nafisa going in a car, presumably they might have decided to spend the final days of his life near that waterfall.

==Cast== John Abraham as Rahul Sharma – An angry and confused compulsive gambler. Wishes to live the life he has been dreaming of
* Sonal Sehgal as Nafisa – Doting, loving, loyal girlfriend. Wishes that love always wins.
* Anaitha Nair as Padma – She is impetuous, obnoxious, full of life. Wishes to experience love.
* Prateeksha Lonkar as Sister Grace – She will bend her principles for the greater good. Wishes to dedicate her life for the happiness of her inmates.
* Girish Karnad as Parthasarthi – Fun loving, "uncle" to everyone, stupidly stubborn on his principles. Wishes to be reunited with his family.
* Farida Jalal as Madhu – Intelligent, virtuous, loving. Wishes to just lead a normal life and not be ostracised.
* Ashwin Chitale as Govinda – Ten years old, loves comics, mangoes and tells stories. Wishes that everyones life be just like a comic book. Ashwin Chitale is child actor who starred in the Marathi film Shwaas.
* Vikram Inamdar as Xavier
* Sonali Sachdev as Doctor (at Clinic)
* Sharad Wagh as Priest
* Shreyas Talpade as (Special Appearance)

==Production==
Aashayein was shot on locations in Puducherry and Hyderabad, India.  John Abraham had to lose 16 kilos for the film.  The films title is inspired by the song "Aashayein" from Nagesh Kukunoor directed  film Iqbal (film)|Iqbal.

==Tracklist==
{{Infobox album
| name=Aashayein
| Type        = soundtrack
| Artist      =
| Cover       =
| Released    =  
| Recorded    = 
| Producer    = Pritam, Shiraz Uppal, Salim-Sulaiman
| Genre       = Film soundtrack
| Length      = 57:84
| Label       = T-Series
}}
This music is composed by Pritam and Salim-Sulaiman(Ab mujhko jeena), Shiraz Uppal(Rabba).  The music was released on 27 July.

{{Track listing
| extra_column = Singer(s)
| title1 = Mera Jeena Hai Kya | extra1 = Neeraj Shridhar | length1 = 5:53
| title2 = Dilkash Dildaar Duniya | extra2 = Shaan (singer)|Shaan, Tulsi Kumar | length2 = 3:55
| title3 = Rabba | extra3 = Shiraz Uppal | length3 = 4:30
| title4 = Ab Mujhko Jeena | extra4 = Zubeen Garg, Samishka Chandra | length4 = 5:15
| title5 = Shukriya Zindagi | extra5 = Shafqat Amanat Ali | length5 = 4:22
| title6 = Pal Main Mila Jahan | extra6 = Shreya Ghoshal | length6 = 5:29
| title7 = Chala Aaya Pyaar | extra7 = Mohit Chauhan | length7 = 5:18
| title8 = Pal Main Mila Jahan | extra8 = Shankar Mahadevan | length8 = 5:30
| title9 = Mera Jeena Hai Kya | note9 = Remix | extra9 = Neeraj Shridhar | length9 = 6:36
| title10 = Ab Mujhko Jeena | note10 = Remix | extra10 = Zubeen Garg, Samishka Chandra | length10 = 4:28
| title11 = Shukriya Zindagi | note11 = Remix | extra11 = Shafqat Amanat Ali | length11 = 4:29
| title12 = Dilkash Dildaar Duniya | note12 = Remix | extra12 = Shaan, Tulsi Kumar | length12 = 3:34
| title13 = Shukriya Zindagi | note13 = Sad Version | extra13 = Shafqat Amanat Ali | length13 = 1:50
}}

==Critical reception== IANS rated the film 3.5 out of 5 saying, "This is unarguably Kukunoor’s most sensitive and moving work since "Iqbal". It touches the heart".  Rajeev Masand of IBN gave 1.5/5 and commented, "Aashayein is a difficult film to sit through (...) it is in fact a serious test of your patience."  Noyon Jyoti Parasara of AOL rated the film 2 out of 5 and said, "...nothing comes through except disappointment.". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 