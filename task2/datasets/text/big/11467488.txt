Feast of Love
 
{{Infobox film
| name           = Feast of Love
| image          = Feast of love.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Benton
| producer       = Gary Lucchesi Charles Baxter
| starring       = Morgan Freeman Greg Kinnear Radha Mitchell Selma Blair Toby Hemingway Stana Katic and Fred Ward
| music          = Stephen Trask
| cinematography = Kramer Morgenthau
| editing        = Andrew Mondshein
| studio         = GreeneStreet Films Lakeshore Entertainment Metro-Goldwyn-Mayer Revelations Entertainment
| distributor    = Metro-Goldwyn-Mayer
| released       = September 27, 2007
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $5,741,608
}} Billy Burke Charles Baxter, was first released on September 28, 2007, in the United States.

==Plot==
The movie deals with love and its various incarnations, set within a community of friends in Portland, Oregon. Harry Stevenson (Morgan Freeman) narrates about how love can affect ones life.

===Bradley===
 Billy Burke). Though she ends her affair with David to marry Bradley, they ultimately declare they are in love with each other and Diana leaves Bradley, again devastating him. Now twice divorced Bradley suffers a mini-breakdown and stabs himself in the hand. As he is getting stitched up in the hospital he falls for his doctor, Margit (Erika Marozsán). In the films conclusion the two are revealed to marry.

===Oscar and Chloe===

Oscar (Toby Hemingway) is a young man working at Bradleys cafe who soon meets and falls in love with a girl named Chloe (Alexa Davalos). However, Oscar is revealed to be living with his alcoholically abusive father, Bat (Fred Ward). When Chloe visits a fortune-teller (Margo Martindale), she is told that Oscar will die. Chloe, though upset at first, straightens her resolve about her love for Oscar and their future together. Coming home, she urges Oscar that they get married immediately. At the wedding, Chloe reveals to Harry that she is pregnant, and plans to have another baby right after due to Harrys advice of having "two." In the films conclusion everybody gathers for an afternoon in the park. While playing football Oscar collapses; despite an attempt to get him to a hospital, congested traffic interferes, and he dies of a heart defect. Then Bat attempts to avenge his sons death by harming Chloe but Harry scares him off, and then asks Chloe if he and his wife Esther (Jane Alexander) can adopt her as their own.

===Diana and David===

Diana is a successful realtor and has been carrying on an affair with the married David. Though she asks him numerous times to leave his wife, Karen, of 11 years he cannot bring himself to do it. Their relationship becomes even more volatile when Diana begins dating Bradley and falls in love with him. David persists he loves Diana, but unable to leave his wife, Diana marries Bradley and ends their affair. However, their love is later rekindled when Karen discovers her husband was cheating, leaving him. Free at last, David and Diana have an emotional confrontation in the park that ends with a kiss that Bradley sees, fueling their divorce and Bradley stabbing himself. In the films conclusion Diana and David are shown as a public and functionally happy couple.

===Harry and Esther===

Harry and his wife Esther have been married a long time. Harry is a patron at Bradleys cafe and often provides the younger generation with advice on love. However, it is revealed that Harry and Esther are masking their own grief after the death of their adult son, Aaron. Harry reveals the nature of his sons death to Chloe, whom he and Esther grow very close to. Harry has also been struggling with the decision of going back to work as a Professor at a university. In the films conclusion after Oscars death he and Esther offer to adopt a now widowed and pregnant Chloe, who tearfully accepts their offer.

==Cast==
*Morgan Freeman as Harry Stevenson
*Greg Kinnear as Bradley Smith
*Radha Mitchell as Diana Croce Billy Burke as David Watson
*Selma Blair as Kathryn Smith
*Alexa Davalos as Chloe Barlow
*Toby Hemingway as Oscar Gamlen
*Stana Katic as Jenny
*Erika Marozsán as Margit Vekashi
*Jane Alexander as Esther Stevenson
*Fred Ward as Bat Gamlen
*Margo Martindale as Mrs. Maggarolian
*Missi Pyle as Agatha Smith
*Alex Mentzel as Billy Smith
*Shannon Lucio as Janey
*Scott Patrick Green as Young Minister at funeral

==Production== Blue Bridge, the front lawn and Eliot Circle.  Scenes in the Jitters Cafe, owned by Kinnears character, were filmed at the Fresh Pot at the corner of N Mississippi Avenue and Shaver streets in Portland.

==Critical reception==
The film received mixed reviews from critics. The review aggregator Rotten Tomatoes reported that 41% of critics gave the film positive reviews, based on 104 reviews.  Metacritic reported the film had an average score of 51 out of 100, based on 28 reviews. 

Roger Ebert reviewed that this film contains the worst performance of Fred Ward, "no movie can be very good that contains Fred Wards worst performance!"

==Box office performance==
In its opening weekend, the film grossed US$1.7 million in 1,200 theaters in the United States and Canada, ranking #12 at the box office.  It grossed a total of US$5.4 million worldwide – US$3.5 million in the United States and Canada and US$1.9 million in other territories. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 