Keye Luke (film)
{{Infobox film
| image          = KeyeLuke_Poster.jpg
| name           = Keye Luke
| director       = Timothy Tau
| producer       = Timothy Tau
| writer         = Timothy Tau Ed Moy Cynthia San Luis James Huang
| music          = George Shaw (Best Original Score, Asians on Film Festival)
| editing        = 
| studio         = Firebrand Hand Creative
| distributor    = 
| release date   =  
| runtime        = 12 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Keye Luke is a 2012 American short film directed by Timothy Tau, written by Timothy Tau, Ed Moy and Feodor Chin, and produced by Timothy Tau.

==Synopsis== Kato in Secret Agent X-9. Feodor Chin plays Keye Luke, Archie Kao plays Edwin Luke, the brother of Keye Luke, and Kelvin Han Yee plays Lee Luke, Keye Lukes father. Other actors play various real-life historical figures that were Lukes colleagues or family members, such as Elizabeth Sandy playing Lukes caucasian wife, Ethel Davis Luke.

==Cast==
*Feodor Chin as Keye Luke
*Archie Kao as Edwin Luke
*Kelvin Han Yee as Lee Luke
*Mei Melancon as Lotus Long
*Elaine Kao as Marianne Quon/Lai Yee
*Jolene Kim as Suzanna Kim
*David Huynh as Benson Fong
*Britt Prentice as Warner Oland (Charlie Chan segment)
*Robert Factor as Sidney Toler (Charlie Chan segment)
*Burl Moseley as Mantan Moreland (Charlie Chan segment)
*Becky Wu as Florence Ung (Charlie Chan segment)
*James Huang as Victor Sen Yung (Charlie Chan segment)
*Jennifer Chang as Iris Wong (Charlie Chan segment)
*Hedy Wong as Frances Chan (Charlie Chan segment)
*Elizabeth Sandy as Ethel Davis Luke
*Timothy Tau as James Wong Howe Green Hornet segment) Cynthia San Green Hornet segment) Secret Agent X-9 segment) Secret Agent X-9 segment) Secret Agent X-9 segment) Secret Agent X-9 segment) Secret Agent X-9 segment)
*Louie Yee (bulldog actor) as Louie "Guai Low" Luke

==Production==
The film was initially made under a  . 

==Awards and Screenings==
===Awards=== George Shaws music, and also a Honorable Mention award for Best Ensemble Cast from the 2013 Asians on Film Festival.  The film has also been nominated for Best Documentary by the Dragon Con Film Festival. 

===Screenings=== DisOrient Asian American Film Festival of Oregon, the Austin Asian American Film Festival, the ASEAN International Film Festival and Awards in Malaysia, and the 2013 Seattle Asian American Film Festival, where it was the Closing Night film.  

The film also screened at a University of Washington event in May to honor Asian Pacific American Heritage Month, alongside other films such as Aoki (a documentary on activist Richard Aoki by Ben Wang and Michael Chang) and Anna May Wong: In Her Own Words directed by Yunah Hong. 

The film also can be viewed online at DramaFever. 

===Reception=== Twitch  and Slashfilm|/Film. 

The film and filmmaker Timothy Tau were also profiled in Seattle Metropolitan Magazine when the film was the Closing Night film of the 2013 Seattle Asian American Film Festival. 

==Future plans==
Tau has plans on extending the short film into a feature film about Keye Luke and other pioneering Asian American actors of his generation including Sessue Hayakawa, Anna May Wong and Philip Ahn. 

==External Links==
* 
* 
* 
* 
* 
* 
* 

==References==
 

 
 
  
 
 
 
 
 