Death of an Indie Label
{{Infobox film
| name           = Death of An Indie Label
| image          = 
| alt            = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| starring       = Esham James Smith Mastamind
| music          = Esham
| cinematography = 
| editing        =  Reel Life
| distributor    =
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} independent record label Reel Life Productions, founded by James H. Smith and his younger brother, rapper Esham, in Detroit in 1990. The film, which was uploaded onto the labels YouTube channel, features appearances by James Smith, Esham and Mastamind. http://blogs.metrotimes.com/index.php/2011/08/city-slang-eshams-death-of-an-indie-label/             the soundtrack for the film features Seven the General and Poe Whosaine.

== Summary == independent record Life After cannabis and playing Russian roulette while listening to Life After Death.  Esham goes on to influence local artists Kid Rock, Eminem and Insane Clown Posse. James Smith, the CEO of Reel Life Productions, is diagnosed with schizophrenia following a prison sentence. Smiths deteriorating mental state is depicted. Due to the poor living conditions of his apartment, James is evicted by his landlady, and moves in with Esham. 

== Reception ==
The Metro Times describes the film as "compelling viewing". 

== Soundtrack ==
{{Infobox album  
| Name       = Death of an Indie Label
| Type       = mixtape
| Artist     = Esham
| Cover      = Death of an Indie Label.jpg
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = Midwest hip hop
| Length     =   Reel Life
| Producer   = 
| Last album =   (2009)
| This album = Death of an Indie Label (2011)
| Next album = &mdash;
}}
Death of an Indie Label is the third mixtape by Esham. Released in 2011, it is the soundtrack to the documentary film of the same name and features Detroit emcees "Poe Whosaine" & Seven the General  

{{Track listing
| total_length    =

| title1          = Comatose
| title2 = Denouement
| title3 = Reignin Odd Future
| title5 = Orgy Orange
| title6 = Priceless Blessings
| title7 = Cheddaphile 
| note7 = feat. Seven the General
| title8 = Time Machine
| title9 = Sayonara
| title10 = U Aint Got Nuthin On Me 
| note10 = feat. Poe Whosaine
| title11 = Serious Business
| title12 = House 4 Rent
| title13 = Daniel Jordan Skit
| title14 = Beautiful Girl
| title15 = Let The System Bump
| title16 = Another Flight
| title17 = Death Of An Indie Label
| title18 = Amy Winehouse
| title19 = Indubitably
| title20 = Big Thangs
}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 