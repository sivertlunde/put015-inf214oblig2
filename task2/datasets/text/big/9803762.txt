Jab We Met
 
{{Infobox Film |
| name           = Jab We Met
| image          = Jab We Met Poster.jpg
| caption        = Theatrical release poster Imtiaz Ali
| producer       = Dhillin Mehta
| story          = Imtiaz Ali
| screenplay     = Imtiaz Ali
| starring       = Shahid Kapoor Kareena Kapoor
| music          = Songs:  
| editing        = Aarti Bajaj
| cinematography = Natarajan Subramaniam
| distributor    = Shree Ashtavinayak Cine Vision Ltd
| released       = 26 October 2007
| country        = India
| language       = Hindi
| runtime        = 142 min
| budget         =   
| gross          =   
}}
 Hindi romantic Imtiaz Ali. The film, produced by Dhillin Mehta under Shree Ashtavinayak Cinevision Ltd, stars Shahid Kapoor and Kareena Kapoor in their fourth film together with Dara Singh, Pawan Malhotra and Saumya Tandon in supporting roles. 

The film tells the story of a feisty Punjabi girl who is sent off track when she bumps into a depressed Mumbai businessman on an overnight train to Delhi. While attempting to get him back on board when he alights at a station stop, both are left stranded in the middle of nowhere. Having walked out of his corporate business after being dumped by his girlfriend, the man has no destination in mind, until the girl forces him to accompany her back home and then on to elope with her secret boyfriend.
 Kannada and Malayalam language|Malayalam. 

==Plot==
Aditya Kashyap (* Shahid Kapoor) a wealthy industrialist meets his girlfriend at her wedding to another man. Depressed he boards a train without any ticket and while in the train, he bumps into Geet Dhillon(* Kareena Kapoor) who is a talkative Punjabi girl who lives in a world of dreams. Initially Aditya finds Geet very irritating because Geet is continually talking without Aditya showing much interest.

Aditya gets off the train and Geet follows him causing her to lose her luggage. Aditya thinks Geet is indeed stupid because he never told her to get off the train nor does he think she is helping him. He appears to be lost and Geet thinks he may be committing suicide. Geet is now angry that she missed the train and holds Aditya responsible for it. Aditya agrees to help Geet by driving a taxi at a stupendously fast speed to the next train station where the train Geet was in was to stop next. However, Geet misses the train again due to her talkative nature. After this, Geet and Aditya rent a room but is later raided by the police.

Aditya starts to feel good about life again and starts to have fun with Geet and also takes her to her home in Bhatinda where her Punjabi family is very thankful to Aditya. When Geets grandfather(* Dara Singh) sees them both together, he thinks that they are a couple and that they are in love with each other. But no one seems to believe him. Aditya has fallen in love with Geet but is aware that Geet does not love him and instead loves Anshuman(* Tarun Arora). After spending some more time together, Aditya and Geet decide to part ways where Aditya goes back to his business and is now becoming very successful.

Aditya expresses his feelings for Geet by announcing the name of a caller card as Geet on television which Geets family see. By this time it has been 9 months and there has been no news about Geet which comes across as a shock to her relatives because they believe she is with Aditya. When Geets uncle( * Pawan Malhotra) angrily meets Aditya he says Geet is not with her but somehow knows that she is fine but actually he does not know Geets condition as he thought Geet would be with Anshuman. One day Aditya goes to meet Anshuman and discovers that Anshuman had rejected Geet.

Aditya then goes to Manali where he is shocked to see Geet as a depressed and quiet schoolteacher. He brings her back home to Bhatinda to her relatives as promised to her uncle. He takes Anshuman there too as his friend. Geets grandfather again says that they are a couple which is true as Aditya and Geet love each other but Geet thinks she loves Anshuman. All the rituals are performed with Aditya when they should be done with Anshuman but the family does not know about Anshuman. Geet later sees Aditya enjoying bhangra where Anshuman is looking like a complete fool.

Aditya chooses to leave as he still thinks Geet loves Anshuman. After Aditya leaves the field where he, Geet and Anshuman were, Anshuman tells Geet what she needs to tell her family that she loves Anshuman. Upon hearing this, Geet realises that she was unable to tell her mother about Anshuman despite having an opportunity and cannot do so because she realises that she loves Aditya. She runs away from the field only to find Aditya with a kid in her family and hugs him showing that she has finally admitted that she loves Aditya. They get married and it is shown that they have 2 daughters.

== Cast ==
* Shahid Kapoor as Aditya Kashyap
* Kareena Kapoor as Geet
* Dara Singh as Geets grandfather
* Pawan Malhotra as Geets uncle
* Kiran Juneja as Geets mother
* Saumya Tandon as Roop
* Tarun Arora as Anshuman
* Divya Seth as Adityas mother 
* Brijendra Kala as Taxi driver
* Vishal O Sharma as Adityas lawyer

== Production == romantic drama". {{cite web|author=Kumar, Nikhil|title=Full-fledged onscreen romance for Kareena and Shahid
|url=http://www.apunkachoice.com/scoop/bollywood/20070221-4.html|publisher=ApunKaChoice.com|accessdate=21 April 2008}}  When Ali began casting for the film, Kareena Kapoor was the directors first choice to portray the role of Geet Dhillon whilst Shahid Kapoor was cast to play the role of Aditya Kashyap, after Ali met him and discovered the actor was more than a poster boy. On the two lead actors, Ali commented, "I wanted a girl who could look natural in scenes where she misses the train. She had to be without make-up, someone who could be hyper, talkative and interesting but not irritating. I was never a fan of her   and hadnt seen too many of her flicks, but I just knew Kareena was perfect as Geet    , When I met him  , I realised he wasnt the forgettable actor that he comes across in several of his movies. He has gone through a lot in life and that maturity had never been brought out. He was perfect for the role of a young, mature, quiet guy." 
 Punjab and later headed to Shimla  and Manali, Himachal Pradesh,  where the crew filmed a song on the Himalayas  and the Rohtang Pass.  Manish Malhotra served as the costume designer during the filming. The summer shots in Punjab of the haveli were shot in Nabha near Patiala.   While shooting the last schedule of the film in Mumbai, sources had indicated that the lead pair had broken up. Though the media projected it as a publicity stunt for the film, it was later confirmed that the couple had indeed broken up.  The title of the film was decided by a popular vote; moviegoers had a choice between Punjab Mail, Ishq Via Bhatinda (Love via Bhatinda) and Jab We Met.  

To promote the film, the actors appeared separately on different television shows. Kareena Kapoor appeared as guest judge on the singing talent contest Sa Re Ga Ma Pa Challenge 2007 whilst Shahid Kapoor appeared on Amul STAR Voice of India and Jhalak Dikhhla Jaa respectively;     they later appeared together on the television show Nach Baliye.  Apart from this, the producers also painted two full local trains in Mumbai from the Western and Central lines with Jab We Met imagery, where Shahid Kapoor chatted up with fellow passengers and informed them about the movie.  On the night of 23 October 2007, a special preview of the film was organized at Yash Raj Studios in Mumbai for those associated with the film and their families. 

== Release ==

=== Reception ===
The film opened to positive reviews on 26 October 2007 with a fairly good opening of 70% all over India and later increased to 90% during the weekend.  Making a total first week net business of 11.75 crores from 350 cinemas,  the film went into the second week with very strong collections  and saw a massive increase with 40%-50% increment in shows worldwide. 
 Om Shanti Om and Sanjay Leela Bhansalis Saawariya, which resulted in the number of shows for the film being reduced.  Due to popular demand and the poor performance of the latter,
the following week saw an increase in the number of prints for the film across the country,   and resulted in a 50% increase of the films collections.  Completing fifty successful days at the box office on 14 December 2007,  the film received a hit status by BoxOffice India    and continued running successfully in cinemas with greater number of shows than the much later-released, Om Shanti Om. 

As of February 2008, Jab We Met has made a net business of over  ,  and has emerged as one of the biggest hits of the year.   On 30 January 2008, it was announced that to celebrate the success of its 100 days run at the box office, PVR Cinemas in Delhi would have a special screening of the film on Valentines Day. 

Meanwhile, Jab We Met also opened well overseas especially in the U.K., where it was released a day before its worldwide release of 26 October. Debuting at number 10 in the U.K., the film grossed £11,488 on its previews night and £144,525 on its opening weekend, collecting a total of £156,013 from 31 screens.  The film continued to do strong business and garnered excellent collections during its second week, making a two-week total of £325,996.  Over the next five weeks, Jab We Met collected a total of £43,529 from 54 screens   and made a grand total of £424,681. 

=== Reviews ===
Upon release, the film opened to positive reviews. Critics praised the film for its simplicity, its romance, saying that it was "one of the finest romantic films to come out of Bollywood in 2007".     The films direction and performances were particularly appreciated. Taran Adarsh from indiaFM gave the film a 3.5 out of 5 rating saying it is "as refreshing as an ice-cold watermelon juice in scorching heat."  Subhash K. Jha wrote, "...Jab We Met is the kind of cinematic experience that is hard to come by in this day and age of smoky cynicism and borrowed rage."    Rajeev Masand from CNN-IBN, who gave the film 3 stars out of 5, described it as "a film bursting with the kind of lovely little moments thatll bring a smile to your face.    Khaled Mohamed, of the Hindustan Times, gave the film 3 out of 5 stars, saying the film, "is quite a delight, particularly for the chirpy-chirpy-cheep-cheep girl and the retentive, moan-groan boy. Directed with a flair for garnishing even the most abject of circumstances with humour and irony, here’s a feel-cool movie. Wonderful. 

Most critics agreed that the main highlight of the film was the leading pair and Shahid and Kareenas chemistry. Rajeev Masand further explained, "The real magic of this film lies in the performances of its two main leads who seize your attention from the moment they first appear on screen."  Taran Adarsh commented, "Shahid delivers his career-best performance in Jab We Met... Kareena is in top form as well. Jab We Met is a turning point in her career  . Fantabulous -- thats the right word to describe her work this time. The confidence with which she handles the contrasting characterization speaks volumes. This film should do for her what Kuch Kuch Hota Hai (1998) did for Kajol."  Subhash K. Jha approved the chemistry as well, "...there are sparks...and the ones between Kareena and Shahid are so unselfconsciously genuine that you end up looking at the characters rather than the two actors going through a series of brilliantly conceived and energized incidents..."  The Times of India gave the film 4 out of 5 stars concluding, "The film belongs to Shahid and Kareena who pitch in memorable performances and lend a whole new meaning to the boy-meets-girl story."    Moviestalkies.com gave the film 3.5 out of 5 stars and described the movie as "a wonderful film in spite of the fact that its basic premise is extremely simple and has been witnessed several times in the past; in fact there isn’t a single unexpected twist in the tale. However, it is Imtiaz Ali’s treatment and the performances of Shahid Kapoor (undoubtedly his best performance till date) and Kareena Kapoor that transforms this simple script into a romantic film which falls into a class of films like Qayamat Se Qayamat Tak and Kuch Kuch Hota Hai."     Joginder Tuteja from indiaglitz.com gave the film 3.5 out of 5 stars, described it as "Shahid-Kareenas Dilwale Dulhania Le Jayenge|DDLJ", and concluded that "...it would be a pain if any of the two actors do not get awarded and rewarded for their performances..."   

Reviews were favourably directed towards the director, Imtiaz Ali, as well. Indiatimes wrote, "After his much appreciated Socha Na Tha (2004), Imtiaz goes bigger and better and the result is nothing but flying colours"  while Bollyvista.com noted, "The plot of the film has been done quite a few times before in India, as well as internationally. What sets it apart is the excellent writing and direction by Imtiaz Ali."  Movietalkies.com went on to say, "Jab We Met is one of those rare films which is not lifted by its screenplay at all. It is only Imtiaz’s vision of making a simple love story with a breath of fresh air, his ability to repackage something we have witnessed several times before and make it feel new that saves the screenplay. It has been a while since we have seen a fresh love story and we should thank Imtiaz for reminding us that sometimes it not what you say but how well you say it." 

== Legacy ==

  helping Kareena Kapoor board a train. This scene pays homage to the classic train scene from Dilwale Dulhania Le Jayenge]]
Jab We Met remains one of the most popular romantic films in Bollywood. The film helped bring Shahid Kapoor and Kareena Kapoors acting careers to a whole new level. The couple was named onto Bollywood Hungamas list of the top 10 best romantic couples of the decade.  According to Hindi Film News, Shahid Kapoors performance as Aditya Kashyap is ranked number 2 for the finest performance by a Bollywood actor for the decade.  Hindi Film News ranked the scene where Aditya and Geet part ways as number 5 in the critics top ten and number 3 in the popular top ten for the most romantic Bollywood scene of the decade.   The film was also ranked by the same source at number two for the best Bollywood movie of the decade. It was behind only Kaminey, another film that Shahid Kapoor had acted in, but ranked ahead of other popular Bollywood movies like 3 Idiots, Lagaan, and Swades.  Yahoo! Movies India listed the film as one of the Bollywoods top 10 most romantic movies.  The films plot inspired the television show Love U Zindagi.  

Kareena Kapoor, who had before enjoyed only some success in her previous films, had catapulted herself to becoming one of Bollywoods superstars. Her character, Geet Dhillon, has become one of the most popular and recognizable heroines in Bollywood.  She later went on to say about her role, "To play a character like Geet doesnt happen all the time; it just happens. When you begin working on a film, you of course know about the story and your character. However, no one knows how audiences would eventually react to it. Geet did strike a chord with the audiences and became a household name."  Kareena Kapoor, for her performance, was ranked at number 66 in Filmfare Magazines 80 Iconic Bollywood Performances list.  According to Hindi Film News, she was number 5 in the critics top ten and number 4 in the popular top ten for the finest performance by a Bollywood actress for the decade.  She was also named by Rediff as one of Bollywoods most beloved characters. 

== DVD and soundtrack ==
 , where Yeh Ishq Haye song was shot]]
{{Infobox album |  
  Name        = Jab We Met|
  Type        = Soundtrack|
  Artist      = Pritam Chakraborty|
  Cover       = JWM CD Cover.jpg|
  Released    = 21 September 2007 (India)|
  Recorded    = | Feature film soundtrack|
  Length      = 45:01 |
  Label       = T-Series|
  Producer    = Dhillin Mehta|
  Reviews     = |
  Last album  = Bhool Bhulaiyaa  (2007)|
  This album  = Jab We Met (2007)|
  Next album  = Dus Kahaniyaan   (2007)|
}}

=== DVD ===
On 7 December 2007, the film was officially released on DVD in the U.S., U.K., UAE and other international markets. A single disc collectors item in an enclosed box, the DVD contained English, Portuguese, Arabic & Spanish subtitles. {{cite web|author=IndiaGlitz|title=JAB WE MET out on DVDs internationally
|url=http://www.indiaglitz.com/channels/hindi/article/35152.html|publisher=Indiaglitz.com|accessdate=7 December 2007}} 

In India, Jab We Met s DVD was released on 29 January in a single as well as an exclusive twin DVD pack with a price of under Rupee|Rs. 50.  While the films duration is 138 minutes, the additional DVD in a twin pack also has 20 minutes of special features of the films making.

=== Soundtrack ===
Composed by music director, Pritam Chakraborty with lyrics by Irshad Kamil, the films soundtrack was released on 21 September 2007 by lead actress, Kareena Kapoor on the musical show Sa Re Ga Ma Pa Challenge 2007.  Joginder Tuteja from IndiaFM gave the music 3.5 out of 5 stars saying, "In 2007, Pritam may have been come up with good music in number of films   But if there is one album that impresses most after Life in a... Metro and turns out to be most satisfying experience, it is Jab We Met. The album is a perfect example of how to get a quality soundtrack which mixes songs for different segments of audience."  Movietalkies.com rated the album 4 out of 5 stars, describing the album as "certainly one of the better albums that have come out this year ... Pritam has shown a depth and width of imagination in the manner in which he has used so many different genres and mixed them together to create a fascinating journey of music." 

The films soundtrack debuted at number 8  and later jumped up to number 5 during its second week.  Over the next several weeks, the album steadily began climbing up the music charts  and saw the album sales increase after the films release.   During the week of 19 November the album replaced the soundtrack of Om Shanti Om and moved up to number 1  but fell back to number 2 the following week.  Despite competition from the release of newer soundtracks, the album stayed at the top for over nine weeks   and became one of the most successful albums of 2007. 

Jab We Mets soundtrack was featured on Rediff.com|Rediffs and Indiafm.com|IndiaFMs year-end list of 2007s Top 10 Music Albums.   The song, Tum Se Hi, was voted by Hindi Film News as the most romantic Bollywood song   and was voted in at number 7 as the best Bollywood song of the decade. 

{{track listing
|headline=Jab We Met Album: Track listing
|extra_column=Singer(s)
|music_credits=yes
|lyrics_credits=no
|total_length=44:27
|title1=Mauja Hi Mauja
|extra1=Mika Singh
|music1=Pritam Chakraborty
|length1=4:04
|title2=Tum Se Hi
|extra2=Mohit Chauhan
|music2=Pritam Chakraborty
|length2=5:23
|title3=Yeh Ishq Haye
|extra3=Shreya Ghoshal
|music3=Pritam Chakraborty
|length3=4:44
|title4=Nagada Nagada
|extra4=Sonu Nigam & Javed Ali
|music4=Pritam Chakraborty
|length4=3:51
|title5=Aao Milo Chalo Shaan & Ustad Sultan Khan
|music5=Pritam Chakraborty
|length5=5:28
|title6=Aaoge Jab Tum Rashid Khan
|music6=Sandesh Shandilya
|length6=4:25
|title7=Tum Se Hi (Remix)
|extra7=Mohit Chauhan
|music7=Pritam Chakraborty
|length7=4:21
|title8=Yeh Ishq Haye (Remix)
|extra8=Antara Mitra
|music8=Pritam Chakraborty
|length8=4:31
|title9=Mauja Hi Mauja (Remix)
|extra9=Mika Singh
|music9=Pritam Chakraborty
|length9=4:07
|title10=Tum Se Hi (Instrumental)
|extra10=Instrumental
|music10=Pritam Chakraborty
|length10=4:53
}}

== Awards and nominations ==
Below is a complete list showing the awards and nominations Jab We Met received:

{| class="wikitable" style="font-size: 95%;"
|-
! Ceremony
! Category
! Recipients and nominees
! Outcome
|-
|rowspan="9"| Apsara Film & Television Producers Guild Awards   Best Film
| Shree Ashtavinayak Cine Vision Ltd
|  
|- Best Director Imtiaz Ali
|  
|- Best Actor in a Leading Role
| Shahid Kapoor
|  
|- Best Actress in a Leading Role
| Kareena Kapoor
|  
|- Best Music Director 
| Pritam Chakraborty
|  
|- Best Female Playback Singer
| Shreya Ghoshal – "Yeh Ishq Hai"
|  
|-
| Best Dialogue 
| Imtiaz Ali
|  
|-
| Best Screenplay
| Imtiaz Ali
|  
|-
| Best Choreography
| Bosco-Caesar – "Mauja Hi Mauja"
|  
|-
|rowspan="7"| Central European Bollywood Awards 
| Best Film
| Shree Ashtavinayak Cine Vision Ltd
|  
|-
| Best Actor
| Shahid Kapoor
|  
|-
| Best Actress
| Kareena Kapoor
|  
|-
| Best Couple
| Shahid Kapoor & Kareena Kapoor
|  
|-
| Best Male Playback Singer
| Mohit Chauhan – "Tum Se Hi"
|  
|-
| Best Female Playback Singer
| Shreya Ghoshal – "Yeh Ishq Hai"
|  
|-
| Best Story
| Imtiaz Ali
|  
|-
|rowspan="7"| Filmfare Awards   Best Film
| Shree Ashtavinayak Cine Vision Ltd
|  
|- Best Director
| Imtiaz Ali
|  
|- Best Actor
| Shahid Kapoor
|  
|- Best Actress
| Kareena Kapoor
|  
|- Best Music Director
| Pritam Chakraborty
|  
|- Best Female Playback Singer
| Shreya Ghoshal – "Yeh Ishq Hai"
|  
|- Best Dialogue 
| Imtiaz Ali
|  
|-
| HT Café Film Awards 
| Best Actress
| Kareena Kapoor
|  
|-
|rowspan="7"| International Indian Film Academy Awards   Best Movie
| Shree Ashtavinayak Cine Vision Ltd
|  
|- Best Director
| Imtiaz Ali
|  
|- Best Actor
| Shahid Kapoor
|  
|- Best Actress
| Kareena Kapoor
|  
|- Best Female Playback
| Shreya Ghoshal – "Yeh Ishq Hai"
|  
|- Best Story
| Imtiaz Ali
|  
|- Best Dialogue 
| Imtiaz Ali
|  
|-
| Lil Star Awards
| Best Actress 
| Kareena Kapoor
|  
|- National Film Awards  Best Female Playback Singer 
| Shreya Ghoshal – "Yeh Ishq Hai"
|  
|- Best Choreography 
| Saroj Khan – "Yeh Ishq Hai"
|  
|-
| Planet Bollywood Peoples Choice Awards
| Best Actress 
| Kareena Kapoor
|  
|- Rediff Readers Choice Awards
| Best Actress 
| Kareena Kapoor
|  
|-
|rowspan="6"| Screen Awards   Best Actor
| Shahid Kapoor
|  
|- Best Actress
| Kareena Kapoor
|  
|- Best Female Playback
| Shreya Ghoshal – "Yeh Ishq Hai"
|  
|- Best Story
| Imtiaz Ali
|  
|- Best Screenplay
| Imtiaz Ali
|  
|- Best Dialogue
| Imtiaz Ali
|  
|-
|rowspan="6"| Stardust Awards   Best Film 
| Shree Ashtavinayak Cine Vision Ltd
|  
|-
| Hottest Young Filmmaker
| Imtiaz Ali
|  
|- Star of the Year – Male
| Shahid Kapoor
|  
|- Star of the Year – Female
| Kareena Kapoor
|  
|-
| Best Actor (Editors Choice)
| Shahid Kapoor
|  
|-
| New Musical Sensation – Male
| Mika Singh – "Mauja Hi Mauja"  (also for "Ganpat" from Shootout at Lokhandwala) 
|  
|- 
|rowspan="8"| Zee Cine Awards   Best Film
| Shree Ashtavinayak Cine Vision Ltd
|  
|- Best Director
| Imtiaz Ali
|  
|- Best Actor – Male
| Shahid Kapoor
|  
|- Best Actor – Female
| Kareena Kapoor
|  
|- Best Music Director
| Pritam Chakraborty
|  
|- Best Playback Singer – Female
| Shreya Ghoshal – "Yeh Ishq Hai"
|  
|- Best Track of the Year 
| Pritam Chakraborty – "Mauja Hi Mauja"
|  
|- Best Screenplay 
| Imtiaz Ali
|  
|}

== References ==
 

== External links ==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 