X-Paroni
{{Infobox film
| name           = X-Paroni
| image          =X-paroni.JPG
| image size     =
| caption        =
| director       = Uncredited  also writers  : Risto Jarva Jaakko Pakkasvirta Spede Pasanen
| producer       =
| writer         =
| narrator       =
| starring       = Spede Pasanen Jaakko Pakkasvirta Meri Lii
| music          = Henrik Otto Donner
| cinematography =
| editing        =
| distributor    =    Väinän Filmi (Finland) (theatrical) Finnkino (2003) (Finland) (DVD) Finnkino (2003) (Finland) (VHS) Mainostelevisio (MTV3) (Finland) (TV)  1964
| runtime        = 85 min.
| country        = Finland Finnish
| budget         =
| preceded by    =
| followed by    =
}} 1964 Finnish comedy and the debut of Spede Pasanen as a leading male role and debut as a co-director of a full-length film. Similarly to the later Koeputkiaikuinen ja Simon enkelit Pasanen plays a dual-role.

==Plot summary==
The plot concerns a wealthy baron (Pasanen), who is so interested in foreign cultures (particularly Native American), that he is oblivious that people within his own organization are using him to fund a local mafia. While visiting the country-side the baron is mistaken for a lazy but inventive farmer (also Pasanen) who looks exactly like him and the two switch roles by accident. While the reserved baron manages to charm the simple people of the country-side his lookalike cracks down on the corruption within the barons business-monopoly (often spoken of but never elaborated). This eventually leads the mob to attempt to assassinate the baron who then flees to the country-side after learning that he has a doppelganger there as well.

The film features many of the trade-marks of Spedes comedies, such as elaborate if somewhat vague settings, gadgetry and stock-characters like the mobsters, the bungling crook (portrayed by Spedes regular Simo Salminen) and a butler named James.

==External links==
* 
* 

 
 
 
 
 


 
 