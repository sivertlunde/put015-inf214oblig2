Las Aventuras del Capitán Piluso en el Castillo del Terror
{{Infobox film
| name           = Las Aventuras del Capitán Piluso en el castillo del terror
| image          = Aventurascapitanpiluso.jpg
| caption        =
| director       = Francis Lauric
| producer       = Guillermo Teruel
| writer         = Humberto Ortiz
| starring       = Alberto Olmedo  Humberto Ortiz
| music          = Carlos Illiana
| cinematography = Alfredo Traverso
| editing        = Jorge Levillotti
| distributor    =
| released       = 19 December 1963
| runtime        = 90 minutes
| country        = Argentina Spanish
| budget         =
| followed_by    =
}} 1963 black-and-white Argentine family family comedy film directed by Francis Lauric and written by Humberto Ortiz. The film starred Alberto Olmedo and Humberto Ortiz.
==Cast==
*Alberto Olmedo ....  Capitán Piluso
*Humberto Ortiz ....  Coquito
*Lalo Hartich
*Martín Karadagián
*Juan Carlos Barbieri
*Ricardo Carenzo
*Rodolfo Crespi
*José Del Vecchio
*Diego Marcote
*Aldo Mayo
*María Esther Podestá
*Semillita
*Félix Tortorelli
 

==Filming location and premiere date==
The movie was filmed on location in Buenos Aires and premiered there on December 19, 1963.

==External links==
*  

 
 
 
 
 
 

 
 