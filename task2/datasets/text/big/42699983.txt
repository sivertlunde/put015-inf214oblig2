Vietnam Veedu
{{Infobox film
| name           = Vietnam Veedu
| image          = Vietnam Veedu.jpeg
| caption        = 
| director       = P. Madhavan
| producer       = Sivaji Ganesan
| writer         = Vietnam Veedu Sundaram Padmini Nagesh K. A. Thangavelu
| music          = K. V. Mahadevan
| cinematography = P. N. Sundaram
| editing        = R. Devarajan
| studio         = Sivaji Productions
| distributor    = Sivaji Productions
| released       = 1970
| runtime        = 
| country        = India
| language       = Tamil
}}
 1970 Tamil Padmini in lead roles. 
The film, produced by Sivaji Productions, had musical score by K. V. Mahadevan. The movie was based on highly acclaim stage play of Vietnam Veedu by Vietnam Veedu Sundaram, who also writer of the film. The film was super hit at box-office and received cult status. The film was remade in Telugu as Vintha Samsaram.

==Cast==
* Sivaji Ganesan as Prestige Padmanabha Iyer Padmini as Savitri
* Nagesh
* K. A. Thangavelu

==Soundtrack==
The music composed by K. V. Mahadevan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:53
|-
| 2 || Un Kannil Near || T. M. Soundararajan || 03:59
|-
| 3 || Oh My Lady || A. L. Raghavan || 03:29
|-
| 4 || Ulagathile Oruvan || T. M. Soundararajan, Rajalakshmi || 06:29
|}

==References==
 

==External links==
 

 

 
 
 
 
 
 


 