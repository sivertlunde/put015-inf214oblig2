Ullasamga Utsahamga
{{Infobox film
| name = Ullasanga Utsahanga
| image = ULLASAMGA_UTHASAMGA.jpg
| caption = Release poster
| director = A. Karunakaran
| producer = G. S. Ranganath B. P. Somu
| executive producer = Vinay Aryan
| writer = A. Karunakaran  (story & screenplay)   Chintapalli Ramana   (Dialogues ) 
| starring = Late Yasho Sagar Sneha Ullal
| music = G. V. Prakash Kumar
| cinematography = I. Andrew
| editing = Marthand K. Venkatesh
| studio = Amruth Amarnath Arts
| budget =
| released = 18 July 2008
| country = India
| language = Telugu
| runtime =
| gross =
}} Telugu film, starring Yasho Sagar and Sneha Ullal in their debut directed by A. Karunakaran. With music composed by G. V. Prakash Kumar, the film was released on 25 July 2008. It was remade into Kannada as Ullasa Utsaha (2009). It was also dubbed into Malayalam as Ayyo Pavam.

==Cast==
* Yasho Sagar as Aravind
* Sneha Ullal as Dhanalakshmi
* Sri Latha as Heroines Friend Sunil as Instant Investor
* Brahmanandam as Street Secretary Ashok Kumar as Police
* L.B. Sriram as Heroines servant Chandra Mohan as Heros Father
* Dharmavarapu Subramanyam as Car Lift Venu Madhav as Fraud Billionaire
* Suman Shetty as Friend
* Prasad Babu
* Ananth Kavitha as Heroins Step mother
* Kallu Krishna rao as Heroines Family Member
* Sudha as Heros Mother
* Surekha Vani as Heros Sister
* Satya Krishnan as Heros Brothers Wife
* Sattanna as Sisters Husband(S/W Engineer)
* Allari Subhashini as Suman Shettys Mother
* Karrthikeya as Duplicate Balaji

==Plot==
Dhanalakshmi a.k.a. Dhana (Sneha Ullal) is the only daughter of a landlord who has over  50 crore property. As she lost her mother in her childhood, her father marries another woman (Kavitha). However, Dhana faces neglect from her stepmother. After Dhanas father passes away, there is no one to console her and the only friend who showed affection was Balaji. He too leaves her after she completes her schooling. After Dhana grew up, she bequeaths her fathers property and her mother eyes it and wanted to marry Dhana against her wishes. So Dhana escapes from house and takes shelter in her friends house in Hyderabad. Aravind (Yasho Sagar), son of a garage owner, is a vagabond. He comes across Dhanalaxmi and loses his heart to her. However, after a few encounters, Dhana gets caught by her stepmother and when she is about take her away, Aravind helps her from the abduction with the help of police. This makes Dhana to befriend with Aravind and she reveals her childhood friendship and tells Aravind that she is in love with Balaji and cant imagine any other in his place. After refusal, Aravind decides to go to Kolkata in search of a job and at the same time, Dhana also comes to know that Balaji is in Kolkata. They both again meet in train and accidentally Dhana misses the train during the journey. So Aravind helps her reach Kolkata to meet Balaji. In the process, Aravind gets severely hurt in the hands of a criminal, who tries to implicate Dhana in a narcotics case. Later, Aravind takes Dhana to Balajis house in Kolkata. Dhanas stepmother also reaches Kolkata and agrees for Dhanas marriage with Balaji. When the marriage is about to take place, Dhana realises that her stepmother enacted a drama and created a fake Balaji to impress Dhana. Again Aravind comes to her rescue and at that time Dhana realises that she is in love with Aravind and the film ends on a happy note.

==Crew==
* Banner: Amruth Amarnath Arts
* Producer: B. P. Somu, G. S. Rangahanath
* Director: A. Karunakaran
* Story: A. Karunakaran
* Screenplay: A. Karunakaran
* Dialogues: Chintapalli Ramana
* Music Director: G. V. Prakash Kumar
* Cinematographer: I. Andrew
* Art Director: Chinna
* exec producer : vinay aryan Vijay
* Editor: Marthand K Venkatesh
* Lyrics: Anantha Sriram

==Box office==
The movie received positive reviews from both the critics and the audience. Comedy was good.

==Soundtrack==
{{Infobox album|  
 Name = Ullasamga Utsahamga |
 Type = soundtrack |
 Artist = G. V. Prakash Kumar | Feature film soundtrack |
 Label = Aditya Music |
 Last album = Kaalai (2007) |
 This album = Ullasamga Utsahamga (2008) |
 Next album = Kuselan (2008) |
}}

The soundtrack consisted of seven songs composed by G. V. Prakash Kumar|G. V. Prakash. Lyrics were written by Anantha Sriram.
{| class="wikitable" width="50%"
|-
! Song title !! Singers
|-
| "Dhannale Thalli" || Rahul Nambiar
|-
| "Priyatama" || Sonu Nigam, Rahul Nambiar
|- Krish
|-
| "Naa Prema" || Karthik (singer)|Karthik, Harini
|-
| "Lalipata" || V. V. Prasanna
|-
| "Mata Matiki" || Sayanora, Rahul Nambiar, Karthik
|-
| "Chakori" || Vasundhara Das, Benny Dayal
|}

==Award(s)==
* A. Karunakaran won the Nandi Award for Best Screenplay Writer.

==References==
 

 

 
 
 
 
 
 