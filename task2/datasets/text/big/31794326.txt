Bhai Bhai
{{Infobox film
| name           = Bhai Bhai
| image          =
| caption        =
| director       = Nitai Palit
| writer         = Manoj Das
| starring       =  Gaura Ghosh   Parbati Ghosh  Samuel Sahu   Gopal Banerjee  Hemant Das
| producer       = 
| music          = Balakrishna Das   Bhubaneswar Misra
| cinematography = Denen Gupta
| editing        = Biswanath Nayak
| studio         = Dalit Jatisangha Chitram
| released       =  
| country        = India
| runtime        = 151 min Oriya
| website        =
| amg_id         =
}}
 Ollywood / Oriya film directed by Nitai Palit.      The film produced under the banner of Dalit Jatisangha Chitram and got financial assistance from department of Tribal and rural welfare, Government of Orissa,India.   It portrays the problems of untouchability and caste system prevailing at that time in rural India. 

==Synopsis==
In a village there are two sections of people leaving nearby. The upper cast people belongs to Pradhans  and the lower caste belongs to Bauris. Pradhan community are influential and landlords. The Bauri community are cultivation labourers, they work in the lands of Pradhans. Bauris are treated as untouchable.  Suduria is head of the village and is of upper cast. He is board minded and do not believe in untouchability. He has a soft corner for one Nidhi, who belongs to Bauri caste.  Nidhi calls Suduria affectionately as brother. Bana, who is from Pradhan community does not accept the daring of lower community to maintain relationship with the upper caste.  He calls a meeting in the village asks for punishment to Nidhi for maintaining relationship with upper caste. The village people decides to punish Nidhi for his crime and bars him from working in the land and taking water from pond. Though Suduria is against the decision, but remain silent against the voice of Pradhan community. The lower caste Bauri community feel harassed against the decision and decide not to work  in the field of Pradhan community. Without labourers the cultivation work has get stopped and the land become barren. At last Pradhan community realize their fault and accept the relationship of Suduria and Nidhi.

==Cast==
* Gaura Ghosh 
* Gopal Banarjee 
* Parbati Ghosh
* Sahu Samuel
* Chapala Nayak
* Beena Palit
* Hemanta Das
* Lila Dulali

==Box office==
The film is not accepted by the public and ultimately flopped. 

==References==
 

==External links==
 

 
 
 