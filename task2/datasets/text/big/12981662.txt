Marley & Me (film)
{{Infobox film
| name = Marley & Me
| image = MarleyPoster.jpg
| caption = Theatrical release poster
| director = David Frankel
| producer = {{plainlist|
* Karen Rosenfelt
* Gil Netter
}}
| screenplay = {{plainlist|
* Scott Frank
* Don Roos
}}
| based on =  
| starring = {{plainlist|
* Owen Wilson
* Jennifer Aniston
* Eric Dane
* Alan Arkin
}} Theodore Shapiro
| cinematography = Florian Ballhaus
| editing = Mark Livolsi
| studio = {{plainlist| Fox 2000 Pictures
* Regency Enterprises
* Sunswept Entertainment Dune Entertainment III, LLC.
}} Twentieth Century Fox
| released =  
| runtime = 115 minutes
| country = United States
| language = English
| budget = $60 million
| gross = {{plainlist|
* $242.7 million   
}}
}} memoir of John Grogan. The film was released in the United States and Canada on December 25, 2008, and set a record for the largest Christmas Day box office ever with $14.75 million in ticket sales.   

==Plot==
Soon after their wedding, John and Jenny Grogan (Owen Wilson and Jennifer Aniston) escape the brutal Michigan winters and relocate to a cottage in South Florida, where they are hired as reporters for competing newspapers. At The Palm Beach Post, Jenny immediately receives prominent front-page assignments, while at the South Florida Sun-Sentinel, John finds himself writing obituaries and two-paragraph articles about mundane news like a fire at the local garbage dump.

When John senses Jenny is contemplating motherhood, his friend and co-worker Sebastian Tunney (Eric Dane) suggests the couple adopt a dog to see if theyre ready to raise a family. From a litter of newborn yellow Labrador retrievers they select Marley (named after reggae singer Bob Marley), who immediately proves to be incorrigible. They take him to Ms. Kornblut (Kathleen Turner), who firmly believes any dog can be trained, but when Marley refuses to obey commands, she expels him from her class.
 Ireland for a belated honeymoon, leaving the rambunctious dog in the care of a young woman who finds him impossible to control, especially during the frequent thunderstorms that plague the area. Soon after returning from their vacation, Jenny discovers she is pregnant again, and this time she delivers a healthy boy, Patrick. When she has a second son, Connor, she opts to give up her job and become a stay-at-home mom, prompting John to take on a daily column for a pay increase. Due to the crime rate, the couple decides to move to a larger house in the safer neighborhood of Boca Raton, where Marley delights in swimming in the backyard pool.

Although she denies she is experiencing postpartum depression, Jenny exhibits all the symptoms, including a growing impatience with Marley and John, who asks Sebastian to care for the dog when Jenny insists they give him away. She quickly comes to realize he has become an indispensable part of the family and agrees he can stay. Sebastian accepts a job for The New York Times and moves away.

John celebrates his 40th birthday and later goes skinny dipping with Jenny in their swimming pool. Increasingly disenchanted with his job, he decides to accept a position as a reporter with The Philadelphia Inquirer with Jennys blessing, and the family moves to a farm in rural Pennsylvania. John soon realizes that he is a better columnist than reporter and pitches the column idea to his editor. Life is idyllic until the aging Marley begins to show signs of arthritis and deafness. An attack of gastric dilatation volvulus almost kills him, but he recovers. When a second attack occurs, it becomes clear surgery will not help him, and Marley is euthanized with John at his side. The family pay their last respects to their beloved pet as they bury him beneath a tree in their front yard.

==Cast== Johnny "John" Grogan
* Jennifer Aniston as Jennifer "Jenny" "Jen" Grogan
* Eric Dane as Sebastian Tunney
* Alan Arkin as Arnie Klein
* Haley Hudson as Debbie
* Haley Bennett as Lisa
* Kathleen Turner as Ms. Kornblut
* Nathan Gamble as Patrick "Pat" Grogan (Age 10)
** Bryce Robinson as Patrick "Pat" Grogan (Age 7)
** Dylan Henry as Patrick "Pat" Grogan (Age 3)
* Finley Jacobsen as Conor Grogan (Age 8)
** Ben Hyland as Conor Grogan (Age 5)

==Production==
Because the film covers 14 years in the life of the dog, 22 different yellow labradors played the part of Marley (as revealed in the special feature Finding Marley on the DVD).
 West Chester in Pennsylvania. The Irish honeymoon scenes were shot in and around Ballynahinch Castle, Connemara, County Galway, Ireland.
 The Devil Wears Prada. He recorded it with the Hollywood Studio Symphony at the Newman Scoring Stage at 20th Century Fox. 

Dave Barry, Grogans fellow South Florida humor columnist, makes an uncredited cameo as a guest at the surprise party celebrating Grogans 40th birthday. {{cite web
 | last = Grogan
 | first = John
 | title = Dave Barry Does Marley (or should I rephrase that?)
 | work =
 | publisher =
 | date = 21 April 2008
 | url = http://www.johngroganbooks.com/blog/2008/04/dave-barry-does-marley-or-should-i.html
 | doi =
 | accessdate = 3 February 2013}} 

==Release==

===Critical reception===
Marley & Me received mixed to positive reviews from critics. On Rotten Tomatoes the film holds a rating of 63%, based on 138 reviews, with an average rating of 6/10. The sites critical consensus reads, "Pet owners should love it, but Marley and Me is only sporadically successful in wringing drama and laughs from its scenario."   On Metacritic, the film has a score of 53 out of 100, based on 30 critics, indicating "mixed or average reviews". 

Todd McCarthy of Variety (magazine)|Variety said the film is "as broad and obvious as it could be, but delivers on its own terms thanks to sparky chemistry between its sunny blond stars, Owen Wilson and Jennifer Aniston, and the unabashed emotion-milking of the final reel. Fox has a winner here, likely to be irresistible to almost everyone but cats ... Animated and emotionally accessible, Aniston comes off better here than in most of her feature films, and Wilson spars well with her, even if, in the films weaker moments, he shows hes on less certain ground with earnest material than he is with straight-faced impertinence." 

Kirk Honeycutt of The Hollywood Reporter observed that "seldom does a studio release feature so little drama - and not much comedy either, other than when the dog clowns around . . .  hatever Marley wants to be about - the challenges of marriage or the balancing act between career and family - gets subsumed by pet tricks. Dog lovers wont care, and that basically is the audience for the film. From Foxs standpoint, it may be enough . . . Marley & Me is a warm and fuzzy family movie, but you do wish that at least once someone would upstage the dog." 

Roger Ebert of the Chicago Sun-Times called the film "a cheerful family movie" and added, "Wilson and Aniston demonstrate why they are gifted comic actors. They have a relationship thats not too situation comedy|sitcomish, not too sentimental, mostly smart and realistic",  whilst Owen Gleiberman  of Entertainment Weekly graded the film A-, calling it "the single most endearing and authentic movie about the human–canine connection in decades. As directed by David Frankel, though, its also something more: a disarmingly enjoyable, wholehearted comic vision of the happy messiness of family life." 

Steve Persall of the St. Petersburg Times was also very positive, graded the film B and commenting, "Marley & Me practically leaps at viewers like a pound puppy seeking affection, and darn if it doesnt deserve some . . . Things could get mushier or sillier, but Frankel and screenwriters Scott Frank and Don Roos — who usually handle grittier material — decline to play the easy, crowd-pleasing game. Their faith in Grogans simple tale of loyalty among people and pets is unique, and it pays off . . .   isnt extraordinary cinema, but it relates to everyday people in the audience in a way that few movies do without being dull." 

Walter Addiego of the San Francisco Chronicle said, "this love letter to mans best friend will make dog fanciers roll over and do tricks. Its so warmhearted, youll want to run out and hug the nearest big, sloppy mutt."  The praise continued with Carrie Rickey of the Philadelphia Inquirer awarding the film three out of four stars and saying, "Marley and Me operates on the assumption that happiness is a warm tongue bath. And those who endorse this belief will enjoy this shaggy dog story . . . The anecdotal structure does not make for a gripping movie. For one thing, theres no conflict, unless you count the tension between a guy and his untrainable pooch. Yet Marley boasts animal magnetism . . . Mawkish? Sometimes. But often very funny and occasionally very moving." 

The film also had bad reviews though, with Betsy Sharkey of the Los Angeles Times calling it "an imperfect, messy and sometimes trying film that has moments of genuine sweetness and humor sprinkled in between the saccharine and the sadness." 

Peter Bradshaw of The Guardian was unimpressed, awarding the film one out of five stars and commenting, "the relentless gooey yuckiness and fatuous stereotyping in this weepy feelbad comedy gave me the film critics equivalent of a boiling hot nose,"  while Philip French of The Observer said, "the one redeeming feature is the presence as Wilsons editor of that great deadpan, put-on artist Alan Arkin, a comedian who can do a double-take without moving his head."  Further criticism came from Colm Andrew of the Manx Independent who said that "Marley himself is surprisingly one-dimensional" and the ending was over-emotional, going "for the heart-wrenching kind which will always provoke a response, but does so with absolutely no grace". 

Marley & Me was placed #5 in a poll of "20 movies that make men cry". 

===Box office=== Sherlock Holmes).  It earned a total of $51.7 million over the four-day weekend and placed #1 at the box office, a position it maintained for two weeks. The film ended its run having grossed $143,153,751 in the US and $99,563,362 in foreign markets for a total worldwide box office of $242,717,113. 

==Accolades==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipients & Nominees
! Result
|-
| rowspan="5" | 2009
| BMI Film & TV Awards
| BMI Film Music Award Theodore Shapiro ||  
|-
| 2009 Kids Choice Awards|Kids Choice Awards
| Blimp Award
| Jennifer Aniston ||  
|-
| rowspan="3" | Teen Choice Award
| Choice Movie: Bromantic Comedy
| ||  
|-
| Choice Movie Actress: Comedy
| Jennifer Aniston ||  
|-
| Choice Movie Liplock
| Owen Wilson & Clyde ||  
|}

==Home media==
 
  Video Hall of Fame and Marley & Me video contest finalists. The DVD has sold a total of 3,514,154 copies generating $61.41 million in sales revenue.

A prequel to the film,  , was released direct-to-video on June 1, 2011.

==Soundtrack==
{| class="wikitable"
|-
! Song !! Artist !! Written by
|-
| Shiny Happy People || R.E.M. || Bill Berry, Peter Buck, Mike Mills and Michael Stipe
|- One Love || Bob Marley & The Wailers || Bob Marley
|-
| Decepcion || Tech-i-L.A || Danny Osuna and Sara Traina
|-
| Rockin the Suburbs (Over the Hedge Version) || Ben Folds || Ben Folds
|- Jim Soni Sonefeld and Dean Felber
|-
| Cantaloop (Flip Fantasia) || Us3 featuring Rahsaan and Gerard Presencer || Herbie Hancock, Rahsaan Hakeem Kelly, Melvyn Simpson and Geoffrey Wilkinson
|- Lithium || Bruce Lash || Kurt Cobain
|-
| Can We Fix It? || || Paul K. Joyce
|- Rather Be || The Verve || Richard Ashcroft
|- Lucky Man || The Verve || Richard Ashcroft
|- Happy Birthday ||  || Mildred J. Hill and Patty S. Hill
|-
| River Song || Dennis Wilson || Carl Wilson and Dennis Wilson
|}

 Soundtrack references:    

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 