Gate of Hell (film)
{{Infobox film
| name           = Jigokumon
| image          = Jigokumon poster.jpg
| caption        = Original Japanese poster
| director       = Teinosuke Kinugasa
| producer       = Masaichi Nagata
| writer         = Teinosuke Kinugasa
| starring       = Kazuo Hasegawa Machiko Kyō
| music          = Yasushi Akutagawa
| cinematography = Kōhei Sugiyama
| editing        = Shigeo Nishida
| distributor    = Daiei Film
| released       =  
| runtime        = 86 minutes
| country        = Japan Japanese
| budget         = 
}}

  is a 1953 Japanese jidaigeki film directed by Teinosuke Kinugasa. It tells the story of a samurai (Kazuo Hasegawa) who tries to marry a woman (Machiko Kyō) he rescues, only to discover that she is already married to someone else. Filmed using Eastmancolor film, Gate of Hell was both Daiei Films first color film and the first Japanese color film to be released outside Japan.

==Plot==
During a rebellion in 1159 the samurai Morito desires the lady-in-waiting Kesa but shes already married to Wataru. Morito decides to get rid of his rival. He makes Kesa explain to him how he can kill her husband while he sleeps. Kesa provides very precise instructions, yet when Morito follows through on her plan it is Kesa herself who gets killed. Morito understands that Kesa has sacrificed herself because she was determined to save Waturus life and her honour.

==Cast==
 
* Kazuo Hasegawa – Morito Endo
* Machiko Kyō – Lady Kesa
* Isao Yamagata – Wataru Watanabe
* Yatarō Kurokawa – Shigemori
* Kōtarō Bandō – Rokuroh
* Jun Tazaki – Kogenta
* Koreya Senda – Gen Kiyomori
* Masao Shimizu – Nobuyori
* Tatsuya Ishiguro – Yachuta
* Kenjirō Uemura – Masanaka
* Gen Shimizu – Saburosuke
* Michiko Araki – Mano
* Yoshie Minami – Tone
* Kikue Mōri – Sawa
* Ryōsuke Kagawa – Yasutada
* Kunitarō Sawamura – Moritada
 

==Reception== Japan Society sponsored a U.S. release of the film in December 1954, Bosley Crowther reviewed it for The New York Times, calling it a "somber and beautiful presentation of a thirteenth century legendary tale." According to Crowther: 
 "The secret, perhaps, of its rare excitement is the subtlety with which it blends a subterranean flood of hot emotions with the most magnificent flow of surface serenity. The tensions and agonies of violent passions are made to seethe behind a splendid silken screen of stern formality, dignity, self-discipline and sublime esthetic harmonies. The very essence of ancient Japanese culture is rendered a tangible stimulant in this film." 

==Awards== Academy Award New York Film Critics Circle Award for "Best Foreign Language Film". It also won the Golden Leopard at the Locarno International Film Festival.   

==Home video==
In the United Kingdom, the film was released in 2012 on Blu-ray Disc and DVD by the Masters of Cinema;  the next year The Criterion Collection released it in the United States. 

==See also==
*List of jidaigeki films
*List of historical drama films of Asia

==References==
 

==External links==
* 
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 