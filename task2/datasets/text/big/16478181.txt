Kiss or Kill (film)
{{Infobox film
| name           = Kiss or Kill
| image          = Kissorkillposter.jpg
| image_size     =
| caption        = Theatrical release poster Bill Bennett
| producer       = Bill Bennett
| writer         = Bill Bennett
| narrator       =
| starring       = Frances OConnor Matt Day
| sound          = Wayne Pashley
| cinematography = Malcolm McCulloch
| editing        = Henry Dangar
| distributor    = New Vision Films
| released       =  
| runtime        = 96 minutes
| country        = Australia
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 
 thriller about Bill Bennett, and stars Frances OConnor and Matt Day.

==Plot summary==

Kiss or Kill opens with a woman off camera explaining how she has difficulty trusting people, especially men, because of things she saw when she was young. This comment introduces a prologue in which a little girl watches helplessly as a man, presumably her father, appears at the door, douses her mother with petrol and sets her alight.

The young girl, now a woman in her twenties, is Nikki Davies (Frances OConnor). Nikki and her boyfriend, Al Fletcher (Matt Day), are small-time criminals who target married businessmen. Nikki picks up a charmless patent attorney in a bar and accompanies him back to his room where she slips something in his drink. The man passes out, Nikki lets Al into the room and they begin casing it for valuables.

Then things start to go wrong. Al discovers their mark is not merely unconscious but dead. They flee the scene but back at their place the situation gets more complicated. A video in the patent attorneys briefcase features a famous ex-footballer, Zipper Doyle, engaged in pedophilia. Enraged, Nikki leaves an abusive message on the answering machine at Doyles gym. Meanwhile, Al fast-forwards to the next scene in the video. Doyle is with a woman who looks disconcertingly similar to Nikki.

At the break of day, the lovers depart Adelaide, setting out across the Nullabor Plain for Perth, Western Australia|Perth. Soon they are being pursued by Doyle - who wants his tape back - as well as the police. That night they stop at a motel in the middle of nowhere, run by a lonely eccentric by the name of Stan (Max Cullen). Stans attempts at hospitality backfire when Nikkis shirt catches alight over a dinner of fondue. This incident and a sleepwalking episode later that night not only underscore the lovers precarious situation but also serve as reminders of Nikkis horrible past.

At a truck stop the next morning, Nikki overhears that Stan has been murdered. She demands to see Als wallet and finds it stuffed full of cash. Al insists he only robbed Stan. Back on the road, the couple become aware that they are being followed. Al realises its Zipper Doyle and veers off-road. Doyle fires at them and chases after in pursuit. Fortunately for Nikki and Al, Doyles Jaguar cant manage the territory their 4WD can.

Pulling over to get their bearings, they make a bizarre discovery. Adler Jones (Barry Otto)
has been hiding in the boot. While Al is on the verge of attacking Adler, Nikki is persuaded by Adlers offer of help. Adler suggests the couple stay at his place nearby (amusingly located in a former nuclear testing site). When they arrive and meet Adlers wife Bel (Jennifer Cluff), Al acknowledges the apparent wisdom of Nikkis instinct to trust Adler.

The next morning Nikki discovers Adler and Bel dead in their beds, their throats cut. Al appears and knocks Nikki out. Evidently he has decided Nikki must be committing the murders - only semi-consciously in her sleep - and ties her up at a safe-house. It is here that the police finally catch up with the couple. However, after a night in a cell a strange lawyer turns up and organises bail. He drives the bemused lovers to a motel where he instructs them to wait. Suddenly Zipper Doyle appears and forces them at gun-point to get into his car.

By now the police have discovered Doyles participation in a pedophilia ring and are pursuing him as well as Nikki & Al. When Doyle sees a road-block he begins issuing commands to Nikki who is in the drivers seat. Nikki ignores him and drives straight into the road-block. Doyle is killed in the crash, the lovers are only wounded. In a concluding voice-over Nikki explains who was responsible for the murders on the road and how they got off for the death of the patent-attorney (always wipe your prints and only target pedophiles).

==Production==
Bill Bennett originally got the idea to make the film while shooting Backlash at Broken Hill. He was talking with a crew member who started sharpening a Rambo knife and joked that he could easily kill Bennett and no one would ever know. The encounter unsettled Bennett and inspired him to write a story about a relationship where you did not completely know the other person. He worked on this 18 or 20 times over the next ten years but could never get the story right. Then he wrote it in three weeks and was satisfied. 

==Themes==

===Trust and Familiarity===
Nikki and Als relationship is often passionate and at times tender. However, as accumulated mysteries present themselves throughout the narrative, they are forced to question how well they really know each other. Nikki has difficulty accepting Als protestations of innocence regarding Stans death and admonishes him for not telling her he had raided the motels till.

Al, for his part, is aware that Nikki is psychologically volatile. Although he accepts her innocence he does insist that people can do "weird things" in their sleep. This intuition is later lent credence when Nikki relives her mothers murder in her sleep and nearly kills Al in the process. When Adler & Bel are found dead, Al ties Nikki up, apparently for her own benefit as well as his.

Underlying the central question of who is doing the killing are other doubts. Al cant understand why the patent attorney died and interrogates Nikki about how many pills she gave them. In bed at night at Adler and Bels, Al tells Nikki that the woman in Doyles video looked a lot like her.
 trust is developed in three sub-plots. When Nikki and Al arrive at Stans motel, Stan  can be overheard arguing with Adler. At the Jones, Nikki notices a photo of Adler and Bel with Stan and wonders about the dynamic between the three of them. Zipper Doyle is widely admired and respected by the public but is really a pedophile. Finally, even the police who are pursuing Nikki and Al start doubting each other in the very amusing "bacon story" (see The Cops below).

===Childhood and Personality===
As a very young girl, Nikki witnesses her mother being murdered in the most horrible fashion. She states in a voice-over that this has affected her ability to trust people in adult life. The residual effect of the experience is apparent in the way in which she is traumatised when her shirt catches on fire at Stans motel. The suspicion that her sleep-walking is attributable to her mothers murder is confirmed in an episode where she relives in her sleep what she saw as a child. As well as exemplifying how childhood experiences endure in the adult personality, Kiss or Kill also presents the possibility that these experiences can be transcended. Even in the most frightening circumstances, Nikki maintains a largely loving, trusting and intimate relationship with Al, one that ultimately endures.

Nikki is the young victim of violence in the prologue: Zipper Doyle is a perpetrator of violence against children in the films present. Thus Doyles appearance represents, at one level at least, a resurfacing of Nikkis past. On this reading, her flight across the desert with Doyle in pursuit is, amongst other things, a flight from her traumatic past. Ultimately the conflict is resolved when Nikki kills Doyle (and nearly herself too) by driving full-speed into a police road-block. Notably, Nikki does this against the instructions of Doyle, who has a gun at her head.

==Characters==

===Nikki Davies===
Nikki Davies, played by Frances OConnor, is the leading female character in the film. She appears sophisticated, in control and witty at first, however as the film progresses Nikki’s true erratic, nervous personality is revealed. She gives off an air of confidence, wearing short skirts, tight tops with glamorous hair and make up, she chain smokes and her relationship with her boyfriend is overtly sexual. After the death of her mother, an orphaned Nikki turns to a life of petty crime, ending up in a juvenile detention centre, where she meets her future boyfriend, and partner in crime Al Fletcher, played by Matt Day. Although Nikki and Al have been together a long time, and thus know each other very well, there is a strong element of mistrust between them. This becomes particularly apparent as their relationship is put under much strain and doubt with a string of murders following the couple across the desert. Nikki is traumatized by an incident in which her sleeve catches on fire, it is assumed that this provokes memories of her mother’s horrific death and she begins sleepwalking as a result.

===Al Fletcher===
Al Fletcher (Matt Day); tall, masculine, and a criminal. Al is Nikki’s best friend and boyfriend. He is a main character in the story and plays a vital role to the development of Nikki’s character. They met in a juvenile detention centre and have pursued their troubled ways, now partners in petty crime. Al is short tempered, we see this when he attacks the truck driver almost unprovoked. We learn that he is anxious and paranoid, this is demonstrated in the scene where he ties Nikki up, leaving her in the middle of nowhere. From this we learn that Al too has trust issues, and he has perhaps experienced a traumatizing event himself, despite his already harmful relationship with his parents and childhood rebellion. Despite Al’s trust issues his affection towards Nikki is definitely love which he demonstrates this numerous times. For example when Nikki gets burnt on the fondue set at the motel. Al also demonstrates this affection for Nikki through sexual acts, they are almost inseparable. He is not always trustworthy and the audience can, at times, question his loyalty. This is assumed after Al steals the money from Stan and doesn’t tell Nikki about it.

The character of Al reflects Nikki’s incapability to trust men, and her underlining fear of pain. Without Al Nikki’s transformation is incomplete. Although Nikki says that she will never completely trust Al, through what they have been together it can be concluded that he has unconsciously helped Nikki breakthrough some of her childhood damage.

"Matt Days good looks belie a certain undercurrent which he uses to full effect in his character of Al."  Matt Day portrays the character of Al as impulsive, strong willed and troubled. His interpretation of this bruised character is convincing as the audience can se the trauma beyond his hard exterior.

===Stan the Motel Owner===
Stan played by Max Cullen is the owner of the motel that Nikki and Al visit the first night theyre on the run. Stan lives by himself and runs the motel in the middle of the Nullabor desert. He is a small, chubby man, who wears a t-shirts with swirls on it to ward off the spirits. According to Stan, who is evidently deluded, his wife went missing in the caves nearby and never returned. Stan is crazy and clearly eccentric but upon his death is referred to as "a good bloke" by another character in the film. It appears that Stan had an affair with Adler Jones wife. When Nikki and Al arrive at the hotel, a heated conversation between Stan and Adler can be overheard.

===Adler Jones===
Adler Jones (Barry Otto) is a mysterious element to the film kiss or kill. Throughout the movie he is seen as kind hearted, quiet man yet underneath he is a cold killer. He appears strange and somewhat mysterious, due to the little detail revealed about him throughout the film. He is a crucial element to the story, creating the unpredictable plotline. Adler is the mystery link that puts the pieces together after the death of Stan, the drunken motel manager and his wife. Stan mutters in drunken slurs about the ‘unphanthomable tunnels under the desert’, he also informs us of the couple that live out on the abandoned  nuclear testing site.

===The cops===
There are two main detectives which feature in the film Kiss or Kill, Detective Inspector Hummer (Chris Haywood) and Crean (Andrew S. Gilbert). The two detectives experience ups and downs whilst investigating the murder, never quite able to catch Nikki and Al, whom they are chasing on suspicion of murder of Paul, a businessman whom Nikki and Al ripped off. As the detectives chase the protagonists, one step behind, they come across more murdered bodies, who they suspect died at the hands of Nikki and Al as well.

A favourite scene for many reviewers was the comedic scene between the two detectives, nicknamed the “bacon scene”. This scene takes place at a roadside diner, as the detectives have a conversation about how the younger of the two, Crean, doesnt eat bacon. James Berardinelli stated in his review of the film,    the scene begins similar to something from Pulp Fiction, with the conversation being borderline-hysterical. David O’Connell   described the scene as having a classic, wordless punchline which is worth the wait. This scene was also the only scene with fully written dialogue in the film. 

This scene also shows how much the detectives know about each other, or really, how much they dont know about each other, continuing the mistrust theme which runs throughout the film. Crean tells a series of stories about his background, ranging from being adopted by Mossad agents that were killed in a plane crash, to having a child with cerebral palsy, all of which Hummer didnt know. Like other scenes in the movie, mainly those that involve Nikki and Al, the scene shows how they come to a realisation that they actually knew very little about each others personal lives, even though they are with each other every day. 

==Editing==
A jump cut is a cut in film editing in which two sequential shots of the same subject are taken from camera positions that vary only slightly. This type of edit causes the subject of the shots to appear to "jump" position in a discontinuous way. An indecisive cut is cutting from a shot to another shot of a different shot size while framing the same subject and the difference in image size or angle must be decisive.  These methods as well as hand held camera work gives the viewer an insight into the lives of Nikki and Al, as their lives are indecisive  and jumpy, on the edge and ultimately gives what the director calls as a ‘dirty look’. It also lets the audience feel like they can relate to the characters because it is something extra apart from the film itself they can experience with the characters.

Relational editing is the editing of shots to suggest association of ideas between them. For example: The flame unites scene 1 & scene 2, the prologue & the body of the film. Together with hearing Nikki’s voice it confirms that the adult woman we now see was once the young girl who witnessed her mother’s murder. (It also suggests that the residue of that trauma is still with her).  Along with a   (one character is shown looking at another character (often off-screen), and then the other character is shown looking "back" at the first character, presumably looking at each other), cutting between the young Nikki and her mother, Nikki’s experience of this horrible event is emphasised.

The 180 degree rule is a basic guideline in film making that states those two characters (or other elements) in the same scene should always have the same left/right relationship to each other. If the camera passes over the imaginary axis connecting the two subjects, it is called crossing the line. The new shot, from the opposite side, is known as a reverse angle. The 180 degree rule is broken during Kiss or Kill. It is most prominent and significant during the scene located at the dinner, and Nikki confronts Al about the murder of Stan. The rule has been broken and so from the viewer it looks as if Nikki is talking to herself as the camera doesn’t cut to Al. This technique emphasizes Nikki’s internal struggle and her ultimate trust issues.

==Music==
Interestingly there is no music at any point throughout the film. The credits roll by silently, and even the DVD menu uses only ambient sound. Thanks to the frequent jump cuts, the lack of music is rarely even noticeable. The silence in the movie was used by Bennett to emphasize the tense nature of uncomfortable situations. 

==Genre== red herrings to trick the audience. The film is an outback road movie, like many released around the same time and takes place on the outback roads of rural Australia.

Through the main characters, the film also takes on a less cliched lovers-on-the-run aspect. Rather than the usual setup of rebels running from oppression, the movie portrays the detectives chasing them as clever jokers. Al and Nikki are often shown to be flawed, overemotional people. Throughout the movie there are also traces of black humour, usually shown by the detectives. 

==Reception== AFI awards, Grand Prix des Amériques. In 1998 the film received recognition at the Film Critics Circle of Australia awards, winning five out of a possible eight nominated awards including best film.

{| class="wikitable"
|-
!Year
!Award Ceremony
!Category
!Recipients and nominees
!Result
|-
|rowspan="15"|1997 Australian Film Institute Awards
| Best Achievement in Direction Bill Bennett
| 
|-
| Best Achievement in Editing
| Henry Dangar
|  
|-
| Best Achievement in Sound
| Wayne Pashley, Toivo Lember and Gethin Creagh
|  
|-
| Best Film
| Bill Bennett, Jennifer Cluff and Corrie Soeterboek (co-producer)
|  
|-
| Best Performance by an Actor in a Supporting Role
| Andrew S. Gilbert
|  
|- Best Achievement in Cinematography
| Malcolm McCulloch
|  
|- Best Achievement in Costume Design
| Ruth De la Lande
|  
|-
| Best Original Screenplay
| Bill Bennett
|  
|-
| Best Performance by an Actor in a Leading Role
| Matt Day
|  
|-
| Best Performance by an Actor in a Supporting Role
| Chris Haywood
|  
|- Best Performance by an Actress in a Leading Role
| Frances OConnor
|  
|-
|rowspan="3" align="center"| Montréal World Film Festival
| Best actress
| Frances OConnor
|  
|-
| Best Artistic Contribution
| Wayne Pashley (sound)
|  
|-
| Grand Prix des Amériques
| Bill Bennett
|  
|- Chicago International Film Festival
| Best Film
| Bill Bennett
|  
|-
|rowspan="7"| 1998
|rowspan="7" align="center"| FCCA Awards
| Best Actor (Female)
| Frances OConnor
|  
|-
| Best Director
| Bill Bennett
|  
|-
| Best Film
| Kiss or Kill
|  
|-
| Best Supporting Actor (Male)
| Chris Haywood
|  
|-
| Best Actor (Male)
| Matt Day
|  
|-
| Best Supporting Actor (Male)
| Andrew S. Gilbert
|  
|-
| Best Cinematography
| Malcolm McCulloch
|  
|}

==Box Office==
Kiss or Kill grossed $929,021 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  
*  
*  , (including curators notes and a map of the settings).
*    
*  
*  
 
 

 
 
 
 
 
 
 
 