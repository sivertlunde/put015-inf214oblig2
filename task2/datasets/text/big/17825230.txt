My Winnipeg
{{Infobox film
| name           = My Winnipeg
| image          = My winnipeg.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Guy Maddin Michael Burns Phyllis Laing Guy Maddin Jody Shapiro
| writer         = Guy Maddin George Toles (dialogue)
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Guy Maddin Ann Savage Louis Negin Amy Stewart Brendan Cade Wesley Cade
| music          = Jason Staczek
| cinematography = Jody Shapiro
| editing        = John Gurdebeke Documentary Channel Everyday Pictures
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Canada
| language       = English
| budget         = $500,000 Beard, William. Into the Past: The Cinema of Guy Maddin. Toronto: U of Toronto P, 2010. Print. ISBN 978-1442610668  
| gross          = 
}} surrealist mockumentary about Winnipeg, Maddins home town. A New York Times article described the films unconventional take on the documentary style by noting that it "skates along an icy edge between dreams and lucidity, fact and fiction, cinema and psychotherapy." 

My Winnipeg began when Maddin was commissioned by the Documentary Channel, and originally titled Love Me, Love My Winnipeg. 
Maddins producer directed "Dont give me the frozen hellhole everyone knows that Winnipeg is,"  so Maddin cast Darcy Fehr in the role of "Guy Maddin" and structured the documentary around a metafictional plot that mythologizes the city and Maddins autobiography.

==Plot==
Although ostensibly a documentary, My Winnipeg contains a series of fictional episodes and an overall story trajectory concerning the author-narrator-character "Guy Maddin" and his desire to produce the film as a way to finally leave/escape the city of Winnipeg. "Guy Maddin" is played by Darcy Fehr but voiced by Maddin himself (in narration): Fehr appears groggily trying to rouse himself from sleep aboard a jostling train as Maddin wonders aloud "What if?" What if he were able to actually rouse from the sleepy life he lives in Winnipeg and escape? Maddin decides that the only possible escape would be to "film my way out," thus motivating the creation of the "docu-fantasia" already underway.

Maddin then describes Winnipeg in general terms, introducing it to the viewer, noting primarily its location at the junction of the   used to sponsor an annual treasure hunt   required our citizens to wander our city in a day-long combing of the streets and neighbourhoods. First prize was a one-way ticket on the next train out of town." No winners in a hundred years could bring themselves to leave the city after coming to know the city so closely over the course of the treasure hunt. Maddin then posits an alternative explanation for Winnipeggers never leaving Winnipeg: sleepiness. He notes that Winnipeg is the sleepwalking capital of the world, with ten times the normal rate of sleepwalking, and that everyone in Winnipeg carries around the keys to their former homes in case they return while asleep. Winnipeg by-laws require that sleepwalkers be allowed to sleep in their old homes by the new tenants. 
 Ann Savage as his mother) in order to recreate scenes from his childhood memories, excluding his father and himself. The "family" gathers to watch the television show LedgeMan, a fictional drama in which "the same oversensitive man takes something said the wrong way, climbs out on a window ledge, and threatens to jump." His mother, in the next window, convinces him to live. Maddins mother is noted as the star of the show. The film recounts the conditions of the 1919 Winnipeg General Strike, a real-world event with international significance, before returning to the family re-enactments, including Mothers suspicion of Janet Maddin, who hit a deer on the highway but is accused of covering up a sexual encounter. Maddin announces that this, like "everything that happens in   is a euphemism." The film then recounts the citys history of Spiritualism, including a visit by Sir Arthur Conan Doyle in 1923. The film next examines Winnipeg architectural landmarks, including the Eatons building and the Winnipeg Arena, both of which are demolished (while the arena is being destroyed, Maddin becomes the last person to urinate in its washroom). Maddin imagines the arenas salvation by the "Black Tuesdays," a fictional team of hockey heroes "in their 70s, 80s, 90s and beyond," then re-enacts a family scene where Mother is harassed to cook a meal. 

The film recounts a racetrack fire that drove horses to perish in the Red River—the horse heads reappear, ghostly, each winter, frozen in the ice. Further Winnipeg landmarks, including the Golden Boy statue atop the provincial legislative building, the Paddle Wheel restaurant, the Hudsons Bay department store, and the Manitoba Sports Hall of Fame, make appearances in distorted versions of themselves, as does the Sherbrook Pool. The film then recalls If Day (an actual historical event when a faked Nazi invasion of the city was mounted during World War II to promote the sale of war bonds), and a buffalo stampede set off by the mating of two gay bison. Time is now running out for Guy Maddin, who fears he will never leave Winnipeg, since the family re-enactments have failed to free him fully. To accomplish this feat of leaving, Maddin imagines a pinup girl for the 1919 strikes newsletter The Citizen: dreaming up this "Citizen Girl" allows Maddin to leave Winnipeg in her capable hands, guilt-free. The final family re-enactment then involves Maddins brother Cameron, who in real life committed suicide, rationalizing this death calmly in a discussion with Maddins "Mother."

==Cast== Ann Savage as Mother 
Louis Negin as Mayor Cornish 
Amy Stewart as Janet Maddin 
Darcy Fehr as Guy Maddin 
Brendan Cade as Cameron Maddin 
Wesley Cade as Ross Maddin 
Lou Profeta as Himself 
Fred Dunsmore as Himself 
Kate Yacula as Citizen Girl 
Jacelyn Lobay as Gwenyth Lloyd 
Eric Nipp as Viscount Gort 
Jennifer Palichuk as Althea Cornish 

==Release==
A limited theatrical release of My Winnipeg involved live narrators, including Maddin himself, Udo Kier and "scream queen" Barbara Steele. 

The DVD release of My Winnipeg by Seville Pictures, in addition to the feature film, contains a music video titled "Winnipeg" by Andy Smetanka (images) and Paul Copoe (music). The DVD also contains some documentary footage of the films screening at the Royal Cinema in Toronto (on June 18, 2008), where it was  narrated live by Maddin.  The DVD also contains three of Maddins short films: Spanky: To the Pier and Back, Berlin and Odins Shield Maiden.

===Book adaptation=== Ann Savage, and an interview with Maddin conducted by Michael Ondaatje. Maddins publisher offers the book with or without a DVD of the film, distributed by Seville Pictures.

==Critical reception==
My Winnipeg received consistent critical praise. As of October 14, 2012, the review aggregator Rotten Tomatoes reported that 94% of critics gave the film positive reviews, based on 82 reviews.  Metacritic reported "universal acclaim" based on 24 critics (scored 84 out of 100).  

Critic Roger Ebert gave the film a perfect 4/4 star rating, stating of Maddins work generally that "If you love movies in the very sinews of your imagination, you should experience the work of Guy Maddin."  Jonathan Romney began his review by stating that
  This reviews section, youll have noticed, operates a five-tier ratings system, but there are occasions when this just doesnt suffice. Once in a blue moon, you encounter a film so extraordinary that its not enough to award the icon of a woman standing, hands raised in applause. You really need her to be levitating several feet above her armchair, body racked with the transcendental ecstasies of Saint Teresa. Such a film is My Winnipeg, by Canadian film-maker Guy Maddin.   The Hollywood Reporter stated that "Docu-fantasia is too mild a label for My Winnipeg, Guy Maddins simultaneously heartfelt and mocking ode to the hometown he describes as the coldest, most soporific city on Earth," also calling the film "Hilarious for those on Maddins mad wavelength and more varied than his strictly fictional features."  J. Hoberman called the film "Maddins best filmmaking since the nono-dissimilar confessional bargain-basement phantasmagoia, Cowards Bend the Knee." 

===Top Ten lists===
Roger Ebert named My Winnipeg the tenth best film of the decade.  

The film appeared on several other critics’ top ten lists of the best films of 2008.     

*3rd - Richard Corliss, Time (magazine)|TIME magazine 
*4th - Marc Savlov, The Austin Chronicle 
*5th - Rick Groen, The Globe and Mail 
*6th - Marjorie Baumgarten, The Austin Chronicle 
*7th - Liam Lacey, The Globe and Mail 
*10th - Noel Murray, The A.V. Club 

In 2015, the Toronto International Film Festival placed My Winnipeg in the Top 10 Canadian Films of All Time. 

===Guy Maddins My Winnipeg by Darren Wershler===
Darren Wershler, a Canadian avant-garde poet, critic, and assistant professor in the Department of English at Concordia University, has published an academic monograph on My Winnipeg. This book-length work, Guy Maddins My Winnipeg (U of Toronto P, 2010), contextualizes the film in relation to avant-garde literature and art by drawing on media and cultural theory. In Wershlers words, "I argue that Maddins use of techniques and media falls outside of the normal repertoire of contemporary cinema, which requires us to re-examine what we think we know about the documentary genre and even film itself. Through an exploration of the films major thematic concerns - memory, the cultural archive, and how people and objects circulate through the space of the city - I contend that My Winnipeg is intriguing because it is psychologically and affectively true without being historically accurate." 

In the context of its Canadian production, My Winnipegs difference from the documentary genre also marks the film as distinct from the work historically advanced by the National Film Board of Canada. Maddin has called My Winnipeg a "docu-fantasia" and Wershler similarly points out that the films "truth" lies somewhere "in the irresolvable tension created by the gap between documentary and melodrama." 

==Awards==
*2007: Toronto International Film Festival - Best Canadian Feature Film
*2008: San Francisco Film Critics Circle Awards - Best Documentary
*2008: Toronto Film Critics Association Awards - Best Canadian Film
*2009: International Urban Film Festival, Tehran - Best Experimental Documentary 

==References==
 

== External links ==
*   at Cinefantastique Online
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 