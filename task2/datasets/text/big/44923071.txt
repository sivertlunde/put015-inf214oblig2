Hey Bro
{{Infobox film
| name           = Hey Bro
| image          = Hey bro new Poster.jpeg
| alt            = 
| caption        =  
| director       = Ajay Chandhok
| producer       = Vidhi Acharya
| music          = Nitz N Sony
| Lyrics         = Pranav Vatsa
| writer         =  M. Salim
| starring       = Ganesh Acharya Maninder Nupur Sharma  
| cinematography =  Arun Mascarenhas
| editing        = Nitin Madhukar Rokde
| Music Label    =  T-Series
| distributor    =
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Hindi Comedy Action Drama  film directed by Ajay Chandhok, produced by Vidhi Acharya and Music by Nitz N Sony (Nitin Arora & Sony Chandy) Lyrics by Pranav Vatsa. The film stars Ganesh Acharya,  Maninder Singh, Nupur Sharma and Hanif Hilal.  The film was released on March 6, 2015 with mixed to negative reviews.  

==Cast==
* Ganesh Acharya as Gopi  Maninder Singh as Shiv
* Nupur Sharma as Anjali
* Hanif Hilal as Baba
* Indira Krishnan
* Prem Chopra
* Amitabh Bachchan (special appearance in "Birju")
* Akshay Kumar (special appearance in "Birju")
* Hrithik Roshan (special appearance in "Birju")
* Ajay Devgan (special appearance in "Birju")
* Ranveer Singh (special appearance in "Birju")
* Prabhudeva (special appearance in "Birju")
* Avi Khairnar (special appearance)
* Govinda

==Soundtrack== Pranav Vatsa. Artist = Genre = Feature Film Length = Type = Label = Cover =|This Language = Chronology = Last album Border = yes}}{{track listing
| headline = Hey Bro Soundtrack
| extra_column = Singer(s)
| collapsed = no
| title1 = DJ
| extra1 = Sunidhi Chauhan,(Ali Zafar)
| length1 = 02:31
| title2 = Birju
| extra2 = Mika Singh,Udit Narayan,Rap by Ranveer Singh
| length2 = 04:28
| title3 = Bulbul
| extra3 =Shreya Ghoshal,Himesh Reshammiya
| length3 = 04:52
| title4 = Line Laga
| extra4 =Mika Singh, Anu Malik
| length4 = 05:29
| title5 = Hu Tu Tu 
| extra5 =Sonu Nigam, Sivamani
| length5 = 07:06
}}

==Critical reception==

The film was panned by critics.

==References==
 

==External links==
*  

 
 
 


 