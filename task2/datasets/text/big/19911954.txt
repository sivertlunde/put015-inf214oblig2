Novel Romance
{{Infobox Film name = Novel Romance image = Novel Romance poster.jpg caption = Theatrical release poster director = Emily Skopov producer = John Scherer Greg Spence writer = Emily Skopov Eddie Richey starring = Traci Lords Paul Johansson Sherilyn Fenn Mariette Hartley music = Raney Shockne cinematography = Dave Klein editing = Matt Friedman Kenn Kashima distributor = Spotlight Pictures released = World premiere:   runtime = 91 minutes country = United States language = English
|budget = United States dollar|US$1.5 million gross = 
}} 2006 film directed by Emily Skopov in her feature film directorial debut. It stars Traci Lords, Paul Johansson and Sherilyn Fenn. The film was shot in 2004 in Venice, Los Angeles, California, USA, and premiered on October 8, 2006, at the 2nd Annual LA Femme Film Festival.

==Plot==
The film follows Max Normane, a successful literary editor, who wants to have a child alone. She offers to publish Jake Buckley  (Paul Johansson), a struggling writer, in exchange for his sperm for artificial insemination.

== Cast ==
*Traci Lords as Max Normane
*Paul Johansson as Jake Buckley
*Sherilyn Fenn as Liza Normane Stewart
*Mariette Hartley as Marty McCall
*Jacqueline Piñol as Isabelle
*Mikaila Baumel as Four-Year-Old Emma
*Pia Artesona  as Rita Ramirez Tony Lee as Todd

==Festivals==
Novel Romance has been selected to screen at the following film festivals:
*2006 LA Femme Film Festival (October 8, 2006)
*2007 Other Venice Film Festival (March 17, 2007)
*2007 Anthology Film Archives New Filmmakers Series (June 27, 2007)

==Awards==
;LA Femme Film Festival
:2007: won Honorable Mention for Ouststanding Achievement &ndash; Emily Skopov

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 


 
 