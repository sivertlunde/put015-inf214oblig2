Gaslight (1940 film)
{{Infobox film
| name           = Gaslight
| image          = Gaslight 1940 poster.png
| image_size     = 
| caption        = Pre-release poster for trade showing
| director       = Thorold Dickinson
| producer       = John Corfield
| based on       =  
| writer         = A. R. Rawlinson Bridget Boland
| starring       = Anton Walbrook Diana Wynyard
| music          = Richard Addinsell
| cinematography = Bernard Knowles
| editing        = Sidney Cole
| studio     = British National Films
| distributor    = Anglo-American Film Corp.  
| released       =  
| runtime        = 89 minutes   Linked 2014-03-08 
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}

Gaslight is a 1940 British film directed by   

==Plot==
Alice Barlow (Marie Wright) is murdered by an unknown man, who then ransacks her house, looking for her valuable and famous rubies. The house remains empty for years, until newlyweds Paul and Bella Mallen move in. Bella (Diana Wynyard) soon finds herself misplacing small objects; and, before long, Paul (Anton Walbrook) has her believing she is losing her sanity. B. G. Rough (Frank Pettingell), a former detective involved in the original murder investigation, immediately suspects him of Alice Barlows murder.
 
Paul uses the gas lamps to search the closed off upper floors, which causes the rest of the lamps in the house to dim slightly. When Bella comments on the lights dimming, he tells her she is imagining things. Bella is persuaded she is hearing noises, unaware that Paul enters the upper floors from the house next door. The sinister interpretation of the change in light levels is part of a larger pattern of deception to which Bella is subjected. It is revealed Paul is a bigamist. He is the wanted Louis Bauer, who has returned to the house to search for the rubies he was unable to find after the murder.

==Cast==
  
* Anton Walbrook as Paul Mallen 
* Diana Wynyard as Bella Mallen 
* Frank Pettingell as B.G. Rough 
* Cathleen Cordell as Nancy the parlour maid  
* Robert Newton as Vincent Ullswater 
* Minnie Rayner as Elizabeth, the cook 
 
* Jimmy Hanley as Cobb 
* Marie Wright as Alice Barlow 
* Aubrey Dexter as House agent 
* Mary Hinton as Lady Winterbourne 
* Angus Morrison as Pianist 
* Katie Johnson as Alice Barlows maid
 

==Reception== 1944 film starring Charles Boyer, Ingrid Bergman, and Joseph Cotten.  
 Time Out wrote, "Nothing like as lavish as the later MGM version&nbsp;... But in its own small-scale way a superior film by far. Lurking menace hangs in the air like a fog, the atmosphere is electric, and Wynyard suffers exquisitely as she struggles to keep dementia at bay. Its hardly surprising that MGM tried to destroy the negative of this version when they made their own five years later."  

==Gaslight as expression==
The psychological term gaslighting, which describes a form of psychological abuse in which the victim is gradually manipulated into doubting his or her own reality, originated from the play and its two film adaptations.   

==References==
;Notes
 

;Bibliography
*Vermilye, Jerry. The Great British Films Citadel Press, 1978.  pp. 52–54. ISBN 0-8065-0661-X

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 