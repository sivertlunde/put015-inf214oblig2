Mate Bohu Kari Nei Jaa
 
 
{{Infobox film
| name           = Mate Bohu Kari Nei Jaa
| image          = mbknj.jpg
| caption        = Theatrical poster
| director       = Bikash Das
| producer       = Gyana Ranjan Priyadarshan
| writer         = Jyostna Misra Priyadarshan
| Screenplay     = Bikash Das
| starring       = Siddhanta Mahapatra Dipen Mihir Das Malabika Bhatacharya Aishwarya Pathi Aparajita Mohanty Raza Murad 
| music          = Bikash Sukla
| cinematography = Rabindra Behera
| editing        = Chandra Sekhar Mishra
| distributor    =
| released       =  
| runtime        = 
| country        = India
| language       = Oriya
| budget         =
}} Oriyas who are rooted to their culture even after being out of the country for decades. 

== Plot==
The story is based on two families living in Bangkok. 

==Cast==
*Siddhanta Mahapatra		
*Dipen		
*Aishwarya Pathi		
*Malabika Bhatacharya		
*Mihir Das		
*Raza Murad		
*Hadu		
*Aparajita Mohanty		
*Pushpa Panda		
*Asrumochan Mohanty

==Soundtrack==
{| border="4" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #FFFFFF; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#E9AB17" align="center"
! Track !! Song !! Singer(s) !! Composer !! lyric
|-
| 1
| Ete bhala pao nahin
| Suresh Wadkar 
| Bikash Shukla
| Bijay Malla
|-
| 2
| Thiri Thiri Chori Chori
| Abhijeet Bhattacharya|Abhijeet, Kavita Krishnamurthy 
| Bikash Shukla
| Nizam
|-
| 3
| Aame kana tama pain fit nuhan ki,
| Udit Narayan
| Bikash Shukla
| Nizam
|-
| 4
| Agana tora dakuchi mate
| Ira Mohanty
| Bikash Shukla
| Panchanan Nayak
|-
| 5
| Hasibaara michha abhinaya
| Ira Mohanty
| Bikash Shukla
| Panchanan Nayak
|-
| 6
| Gotie chitthi dui thikana 
| Ira Mohanty
| Bikash Shukla
| Panchanan Nayak
|} 

==References==

 

== External links ==
*  

 
 
 

 