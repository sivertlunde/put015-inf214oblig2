Werewolf of London
 
{{Infobox film 
  | name =Werewolf of London
  | image =Werewolfoflondon.jpg
  | caption = Stuart Walker
  | producer = Stanley Bergerman John Colton
  | starring =Henry Hull Warner Oland Valerie Hobson Lester Matthews Spring Byington Clark Williams Lawrence Grant
  | cinematography = Charles J. Stumar
  | music = Karl Hajos
  | editing = Russell F. Schoengarth Milton Carruth
  | studio = Universal Pictures
  | released =  
  | runtime = 75 minutes
  | language = English Language|English, Standard Tibetan|Tibetan, Latin
  | budget = $195,000 Michael Brunas, John Brunas & Tom Weaver, Universal Horrors: The Studios Classic Films, 1931-46, McFarland, 1990 p131 
  }} Jack Pierces The Wolf Man.
 Hollywood mainstream werewolf movie.

==Plot==
Wilfred Glendon (Henry Hull) is a wealthy and world-renowned English botanist who journeys to Tibet in search of the elusive mariphasa plant. While there, he is attacked and bitten by a creature later revealed to be a werewolf, although he succeeds in acquiring a specimen of the mariphasa. Once back home in London he is approached by a fellow botanist, Dr. Yogami (Warner Oland), who claims to have met him in Tibet while also seeking the mariphasa. Yogami warns Glendon that the bite of a werewolf would cause him to become a werewolf as well, adding that the mariphasa is a temporary antidote for the disease.

Glendon does not believe the mysterious Yogami. That is, not until he begins to experience the first pangs of lycanthropy, first when his hand grows fur beneath the rays of his moon lamp (which he is using in an effort to entice the mariphasa to bloom), and later that night during the first full moon. The first time, Glendon is able to use a blossom from the mariphasa to stop his transformation. His wife Lisa (Valerie Hobson) is away at her aunt Etties party with her friend, former childhood sweetheart Paul Ames (Lester Matthews), allowing the swiftly transforming Glendon to make his way unhindered to his at-home laboratory, in the hopes of acquiring the mariphasas flowers to quell his lycanthropy a second time. Unfortunately Dr. Yogami, who is also a werewolf, sneaks into the lab ahead of his rival and steals the only two blossoms. As the third has not bloomed, Glendon is out of luck.

Driven by an instinctive desire to hunt and kill, he dons his hat and coat and ventures out into the dark city, killing an innocent girl. Burdened by remorse, Glendon begins neglecting Lisa (more so than usual), and makes numerous futile attempts to lock himself up far away from home, including renting a room at an inn. However, whenever he transforms into the werewolf he escapes and kills again. After a time, the third blossom of the mariphasa finally blooms, but much to Glendons horror, it is stolen by Yogami, sneaking into the lab while Glendons back is turned. Catching Yogami in the act, Glendon finally realizes that Yogami was the werewolf that attacked him in Tibet. After turning into the werewolf yet again and slaying Yogami, Glendon goes to the house in search of Lisa, for the werewolf instinctively seeks to destroy that which it loves the most.

After attacking Paul on the front lawn of Glendon Manor, but not killing him, Glendon breaks into the house and corners Lisa on the staircase and is about to move in for the kill when Pauls uncle, Col. Sir Thomas Forsythe of Scotland Yard, arriving with several police officers in tow, shoots Glendon once. As he lies dying at the bottom of the stairs, Glendon, still in werewolf form, speaks: first to thank Col. Forsythe for the merciful bullet, then saying goodbye to Lisa, apologizing that he could not have made her happier. Glendon then dies, reverting to his human form in death.

==Sound and make-up== Jack Pierces The Wolf timber wolf, an approach which was never duplicated in any subsequent werewolf film.   Also, at the beginning of the film, the supposed "Standard Tibetan|Tibetan" spoken by villagers in the movie is actually Cantonese; Henry Hull is otherwise just muttering gibberish in his responses to them. 

==Cast==
 
*Henry Hull as Dr. Glendon
*Warner Oland as Dr. Yogami
*Valerie Hobson as Lisa Glendon
*Lester Matthews as Paul Ames
*Lawrence Grant as Sir Thomas Forsythe
*Spring Byington as Miss Ettie Coombes
*Clark Williams as Hugh Renwick
*J.M. Kerrigan as Hawkins
*Charlotte Granville as Lady Forsythe
*Ethel Griffies as Mrs. Whack
*Zeffie Tilbury as Mrs. Moncaster
*Jeanne Bartlett as Daisy
 

==Reception==
  Rialto Theatre before the theatre was torn down and rebuilt in 1935, called the film a "charming bit of lycanthropy"; according to Nugent, the film was   daring young aerialist. Granting that the central idea has been used before, the picture still rates the attention of action-and-horror enthusiasts. It is a fitting valedictory for the old Rialto, which has become melodramas citadel among Times Squares picture houses. 

The movie was regarded at the time as too similar to Dr. Jekyll and Mr. Hyde (1931 film)|Dr. Jekyll and Mr. Hyde with Fredric March, which had been released only three years before, and flopped at the box-office. One striking similarity between the two films is that each time the man becomes a monster, the monster looks markedly more monstrous than the time before, a feature found in Robert Louis Stevensons novella Dr. Jekyll and Mr. Hyde as well as most of the movies based upon it. Additionally, the werewolf in the film acts very human at times, such as his first transformation, where the werewolf dons a hat, scarf and coat.

==Legacy==
 
  Werewolves of London. 
 Walter Harris)   as part of a short-lived series of books based on the classic Universal horror films. The novel is told from the point of view of Wilfred Glendon (whose first name is now inexplicably spelled "Wilfrid") and thus follows an almost entirely different story structure than the film. In particular it has a different ending. Rather than turning into a werewolf, killing Yogami, and then being shot by Sir Thomas, Glendon decides to cooperate with Yogami and they both attempt to control their transformations through hypnotism. However the plan fails, the hypnotist is killed, and Glendon and Yogami both transform and fight to the death. Glendon wins, killing Yogami, and returns to human form afterwards. The novel then ends with Glendon, alive, contemplating using the hypnotists gun to commit suicide rather than go on living as a werewolf.

The second time was in 1985, as part of Crestwood Houses hardcover Movie Monsters series, again based on the old Universal films. This time, the author was Carl Green, and was considerably shorter, followed the plot of the film more closely, and was illustrated extensively with still photos from the movie.

==See also==
*Universal Monsters The Wolf Man
*She-Wolf of London (film)|She-Wolf of London

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 