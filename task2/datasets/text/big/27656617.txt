Haywire (film)
{{Infobox film
| name           = Haywire
| image          = Haywire Poster.jpg
| caption        = Theatrical release poster
| director       = Steven Soderbergh
| producer       = Gregory Jacobs
| writer         = Lem Dobbs
| starring       =   David Holmes
| cinematography = Peter Andrews (a pseudonym for Steven Soderbergh)
| editing        = Mary Ann Bernard (a pseudonym for Steven Soderbergh)
| studio         = Relativity Media
| distributor    = Relativity Media  
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $23 million   
| gross          = $33,372,606 
}}
 thriller film David Holmes.
 MI6 agent, but this is a ruse and she is actually being set up for assassination; Spain and the assassination plot turn out to be connected, and Kane has to unravel the complicated conspiracy against her.

==Plot==
Former Marine Mallory Kane (Gina Carano) goes to a diner in Upstate New York to meet Aaron (Channing Tatum). He tells her to get in his car, but she refuses and they fight. He pulls out a gun, but she disarms and pistol-whips him. Scott (Michael Angarano), a customer in the diner, intervenes and Mallory demands his car keys and that he get in the car. As they flee, she explains who she is and what has happened to her. The flashback sequences are intermixed with scenes of their flight.

Mallory tells Scott that she and Aaron work for a company that handles operations. One week before, the firms director (and Mallorys ex-boyfriend) Kenneth (Ewan McGregor) had attended a meeting in Washington, D.C. arranged by government agent Coblenz (Michael Douglas). Kenneths firm was hired to rescue Jiang (Anthony Brandon Wong), who was allegedly being held hostage in an apartment in Barcelona. Also present at the meeting was Coblenzs Spanish contact, Rodrigo (Antonio Banderas).

Mallory and her team, which includes Aaron, travel to Barcelona and, despite difficulties, succeed in rescuing Jiang and delivering him to Rodrigo.
 MI6 agent Paul (Michael Fassbender) during a mission in Dublin. Mallory agrees and accompanies Paul to a party at Russborough House, where they meet with his contact, Studer (Mathieu Kassovitz). Paul meets with Studer again as Mallory watches from afar. She sees Paul go into a barn and after he leaves, she enters it to find Jiang dead, clutching in his hand a brooch which Kenneth had insisted she wear as a recognition signal for her initial contact with Paul. Mallory realizes she has been set up.

On returning to their room at the Shelbourne Hotel, Paul attacks Mallory and they have a brutal fight; Mallory gets the upper hand and suffocates him near to death with a choke hold, then shoots him point blank in the face. She finds a missed call on Pauls phone and returns it. Kenneth answers and asks if Mallory has been taken care of, before realising who is on the other end. As Mallory leaves the hotel, she evades Kenneths agents, who are tailing her. Heavily armed members of the Garda Emergency Response Unit (ERU) appear and try to arrest her. She escapes after a chase and sneaks onto a ferry to England.

Mallory calls Rodrigo and asks him whether it was he or Kenneth who set her up. Rodrigo calls Coblenz, who then calls Mallory. Coblenz tells Mallory that he has had suspicions about Kenneth for some time. Coblenz then contacts Kenneth and tells him to inform Mallorys father, John Kane (Bill Paxton), of her purported crimes.

Meanwhile, Mallory enters the United States and reaches the diner, expecting to meet Kenneth. Now on the road, Scott and Mallory are captured by the police. Both are taken into custody but the police are ambushed by Kenneths men. Mallory manages to kill one of them and flees with Scott in one of the police cars. She releases Scott and leaves to meet with her father.

Mallory reaches her fathers house in New Mexico before Kenneth, Aaron and two other men arrive to interrogate John about his daughters whereabouts. Aaron receives a photograph on his phone of Jiang lying dead, and it dawns on him that Mallory might have been set up. He tries to press Kenneth for the truth, but Kenneth shoots him and escapes, as Mallory takes out Kenneths other men. Aaron apologizes to Mallory as he dies in her arms.

The following day, Mallory meets with Coblenz, who reveals that he told Kenneth to contact Mallorys father, expecting that Kenneth would go to her fathers house and that she would kill him there. Coblenz also gives her Kenneths present location. Before they part, he offers her a government job, but she says she will respond after she has found Kenneth.

In Mexico, Mallory confronts Kenneth on a beach and they fight. Kenneths foot becomes jammed between rocks. Unable to escape, he reveals that Jiang was a journalist who was being protected in a safe house after having exposed Studers crimes. Knowing that Mallory planned to leave his firm, Kenneth arranged for her to kidnap Jiang and deliver him to Rodrigo, who delivered him to Studer, who killed him. Kenneth then framed Mallory, planning to cut all ties that could lead to him, and convinced Paul that Mallory was a double agent whom he should kill. Mallory leaves Kenneth to drown in the incoming tide.
 conspiracy against her.

==Cast==
* Gina Carano as Mallory Kane
* Michael Fassbender as Paul
* Ewan McGregor as Kenneth
* Bill Paxton as John Kane
* Channing Tatum as Aaron
* Antonio Banderas as Rodrigo
* Michael Douglas as Alex Coblenz
* Michael Angarano as Scott
* Mathieu Kassovitz as Studer
* Eddie J. Fernandez as Barroso
* Anthony Brandon Wong as Jiang
* Tim Connolly as Jason
* Maximino Arciniega as Gomez Aaron Cohen as Jamie
* Natascha Berg as Liliana
* Fergal OHalloran as Hotel Clerk

==Development==
Film development was announced in September 2009  with the title Knockout, later changed to Haywire before production began.  The screenplay was written to be shot in Dublin. The film was shot mostly in Ireland; filming occurred from 2 February 2010 to 25 March 2010 with a budget of approximately $25 million. The first set pictures were released on February 26, 2010. 

==Release==
 

===Critical response===
The film has received generally positive reviews from critics. Review aggregation website Rotten Tomatoes gives the film a score of 80% based on 181 reviews. The sites critical consensus is "Haywire is a fast and spare thriller, with cleanly staged set pieces that immerse you in the action."  It holds a 67/100 on Metacritic, based on 40 critics. 

Claudia Puig of USA Today stated that the film was "a vigorous spy thriller that consistently beckons the viewer to catch up with its narrative twists and turns. Bordering on convoluted, it works best when in combat mode."  Andrew OHehir of Salon.com shared a similar view, saying "Haywire is a lean, clean production, shot and edited by Soderbergh himself and utterly free of the incoherent action sequences and overcooked special effects that plague similarly scaled Hollywood pictures." 

Richard Corliss of Time (magazine)|Time said "Carano is her own best stuntwoman, but in the dialogue scenes shes all kick and no charisma. The MMA battler lacks the conviction she so forcefully displayed in the ring. She is not Haywire s heroine but its hostage."  Keith Uhlich of Time Out New York wrote, "Theres shockingly little thrill in watching Carano bounce off walls and pummel antagonists."  The general publics reception of Haywire has been less positive than that of the critics according to a survey by CinemaScore revealing that audiences rated the film a D+. 

===Box office ===
Haywire was released on January 20, 2012 with an opening weekend gross of $8.4 million,  and has earned $18.9 million in the United States and $32.4 million worldwide. 

===Home media===
Haywire was released on DVD and Blu-ray disc on May 1, 2012. 

It was the first Relativity film to not be released on DVD and Blu-ray by Twentieth Century Fox Home Entertainment.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 