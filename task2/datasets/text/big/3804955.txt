Gold Diggers of 1935
{{Infobox film
|  name           = Gold Diggers of 1935
|  image          = Gold diggers of 1935 poster.jpg
|  image size     = 250px
|  director       = Busby Berkeley
|  writer         = Robert Lord (story) Peter Milne (story & screenplay) Manuel Seff (screenplay)
|  starring       = Dick Powell Adolphe Menjou Gloria Stuart Alice Brady
|  music          = Harry Warren (music) Al Dubin (lyrics)
|  producer       = Robert Lord George Barnes
|  editing        = George Amy First National Pictures
| distributor    = Warner Bros.
|  released       =  
|  runtime        = 95 minutes
|  country        = United States
|  language       = English
|  budget         =
|  gross          =
}}
 s "Lullaby of Broadway" production number from Gold Diggers of 1935]]
 Winifred Shaw, Lullaby of Broadway" production number, which features Shaw singing the song which won Harry Warren (music) and Al Dubin (lyrics) an Academy Award.

Gold Diggers of 1935 was the third film of the Gold Diggers series of movie musicals, after Gold Diggers of Broadway in 1929 (now lost) and Gold Diggers of 1933, a remake of the earlier film.   Both the original and the 1933 film made a great deal of money for Warner Bros., and Gold Diggers of 1935 was an attempt to repeat that success.  It was followed by Gold Diggers of 1937 and Gold Diggers in Paris.

==Plot==
In the resort of Lake Waxapahachie, the swanky Wentworth Plaza is where the rich all congregate, and where the tips flow like wine.  Handsome Dick Curtis (Dick Powell) is working his way through medical school as a desk clerk, and when rich, penny-pinching Mrs. Prentiss (Alice Brady) offers to pay him to escort her daughter Ann (Gloria Stuart) for the summer, Dick cant say no – even his fiancee, Arline Davis (Dorothy Dare) thinks he should do it. Mrs. Prentiss wants Ann to marry eccentric middle-aged millionaire T. Mosley Thorpe (Hugh Herbert), whos a world-renowned expert on snuffboxes, but Ann has other ideas. Meanwhile her brother, Humbolt (Frank McHugh) has a weakness for a pretty face: hes been married and bought out of trouble by his mother several times.
 Grant Mitchell) and the hotel stenographer (Glenda Farrell), whos blackmailing the hapless snuffbox fancier Thorpe.

Of course, Dick and Ann fall in love, Humbolt marries Arline, and the show ends up costing Mrs. Prentiss an arm and a leg, but in the end she realizes that having a doctor in the family will save money in the long run.

==Cast==
*Dick Powell as Dick Curtis
*Adolphe Menjou as Nicolai Nicoleff
*Gloria Stuart as Ann Prentiss
*Alice Brady as Matilda Prentiss
*Hugh Herbert as T. Mosely Thorpe III
*Glenda Farrell as Betty Hawes
*Frank McHugh as Humbolt Prentiss
*Joseph Cawthorn as August Schultz Grant Mitchell as Louis Lampson
*Dorothy Dare as Arline Davis
*Wini Shaw as Winny

==Songs==
The songs in Gold Diggers of 1935 were written by Harry Warren (music) and Al Dubin (lyrics), and the two production numbers were staged by Busby Berkeley.

*"Im Going Shopping with You" – Sung by Dick Powell to Gloria Stuart, this is a montage of scenes of Stuart shopping for everything from lingerie to jewelry, much to the dismay of her penny-pinching mother, Alice Brady.

*"The Words Are in My Heart" – This elaborate Busby Berkeley production number utilized 56 white grand pianos, which were moved around the sound stage by male dancers underneath the piano-shells, dressed in black. TCM   

*"Lullaby of Broadway" – One of the most famous Busby Berkeley numbers is actually a short film-within-a-film, which tells the story of a Broadway Baby who plays all night and sleeps all day.  It opens with a head shot of singer Wini Shaw against a black background, then the camera pulls back and up, and Shaws head becomes the Big Apple, New York City.  As everyone rushes off to work, Shaw returns home from her nights carousing and goes to sleep.  When she awakens, that night, we follow her and her beau (Dick Powell) from club to club, with elaborate large cast tap numbers, until she is accidentally pushed off a balcony to her death. The sequence ends with a return to Shaws head, as she sings the end of the song. Of all the musical numbers Berkeley created in his career, he named this as his personal favorite.   

==Production== Burbank studios acute indigestion.  

The film was Busby Berkeleys first time at the helm of a film as the official director, although he had his own unit at Warners to do the elaborate production numbers he conceived, designed, staged and directed, which were the major elements of the Warners musicals of that period.

==Awards and honors==
Harry Warren and Al Dubin received an 1936 Academy Award for Best Original Song for "Lullaby of Broadway", and Busby Berkely was nominated for Best Dance Direction. 

==See also==
   The Gold Diggers
*Gold Diggers of Broadway  
*Gold Diggers of 1933
 
*Gold Diggers of 1937
*Gold Diggers in Paris
* Pre-Code Hollywood
 

==References==
Notes
 

==External links==
* 
* 
* 
* 


 

 
 
 
 
 
 
 
 
 