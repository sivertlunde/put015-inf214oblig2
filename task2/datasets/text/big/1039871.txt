Enter the Phoenix
 
 
{{Infobox film
| name           = Enter the Phoenix
| image          = Enter-The-Phoenix-poster.jpg
| image_size     =
| caption        =
| director       = Stephen Fung
| producer       = Willie Chan Jackie Chan Solon So
| writer         = Story: Stephen Fung Screenplay: Stephen Fung Law Yiu-Fai Helen To
| narrator       =
| starring       = Eason Chan Daniel Wu Karen Mok Stephen Fung
| music          =
| cinematography = Chow Lin-Yau Poon Hang-Sang
| editing        = Curran Pang
| distributor    = JCE Movies Limited
| released       = Hong Kong: 8 April 2004
| runtime        =
| country        = Hong Kong Cantonese
| budget         =
}} 2004 Cinema Hong Kong film directed by Stephen Fung.  

==Plot==
When gang master Hung died, his two followers Cheung and Chapman To were sent to Thailand to look for his son, Georgie Hung, to succeed him. Georgie, who is gay and lives as a cook with his boyfriend Frankie, is uninterested to continue his fathers work. But his close friend Sam, adored the life of a gangster and took his position instead. Sam and Georgie thus returned to Hong Kong with their identities swapped.

Upon returning, they discovered that Hung had accidentally killed Cheng Chows father and now Cheng Chow, a leading gangster of a friendly gang, led by Chan Wai-Man, is looking to avenge for his fathers death.

Meanwhile, Chan Wai-Man wanted to let his daughter Julie to marry Sam in order to strengthen the bond between the two gangs. Sam initially agrees but later changed his mind when he discovered that Julie is not in love with him.

Finally, Cheng Chow kidnapped both Julie and Sam and forced Georgie to lead his gang on a rescue mission. Georgie, with help from Julie and Sam, defeated Cheng Chow in the final battle and told him to accept the fact that his fathers death was an accident. Then Cheng Chow saved Georgie when one of his men tried to take his life.

Julie and Sam fell in love during the kidnapping and decided to get married while Sam and Georgie took joint leadership of the gang.

==Full Cast==
* Eason Chan - Sam
* Daniel Wu - Georgie Hung Chi Kit
* Karen Mok - Julie Lui
* Chapman To - Chapman To
* Law Kar-ying - Master 8 / Cheung / Father Fight
* Stephen Fung - Cheng Chow
* Yuen Biao - Georgie Hungs father
* Nicholas Tse - Cock Head
* Jackie Chan - Mr. Chan / Client of Julie (cameo) Brian Lee - David Michael Chan - Lui the Gang leader
* Philip Ng - Bo
* Chapman To - Kin Sam Lee - Gay Man
* Sammi Cheng - Head Restaurant Manager (cameo)
* Hayama Go - Max Cheung (Chows Henchman)
* Chan Wai-Man - Lui
* Lee Lik-Chi - Ma Leung
* Glen Chin - Fat Ox
* Maggie Lau - Breeze
* Koey Wong - Precious
* Tenky Kai Man Tin - Assassin
* Courtney Wu - Mr. Ho
* Ankee Leung - Kins thug
* Sam Hoh - Chows thug
* Chu Cho-Kuen (extra)

==See also==
*Jackie Chan filmography

== External links ==
* 
*  

 

 
 
 
 
 
 
 

 