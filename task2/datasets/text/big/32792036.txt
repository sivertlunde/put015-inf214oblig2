Living Dead Girl (short film)
{{Infobox film
| name           = Living Dead Girl
| image          = Living Dead Girl Production Photo.jpg
| caption        = Mark Borchardt & Jon Springer (2003)
|
| director       = Jon Springer
| producer       = Jon Springer, Patrick Chenal III Ted Dewberry Ron Johnson Michele Kurkowski
| screenplay     = Jon Springer
| starring       = Mark Borchardt Nadine Gross Mehrdad Sarlak Charles Hubbell Robert Elliot Harrison Matthews Jennifer Prettyman
| cinematography = Jon Springer
| editing        = Jon Springer
| released       = July 23rd, 2005
| runtime        = 10 minutes
| country        = USA
| language       = English
}}

Living Dead Girl is a low-budget short horror film directed by Jon Springer.    The film stars Mark Borchardt who is best known as the subject of the cult documentary American Movie (1999). "Living Dead Girl" is a silent-film zombie parody with extremely graphic gore effects. 

== Plot ==

A zombie horde overtakes the Mall of America. Meanwhile, we see a man named Tom feverishly barricading the inside of his house, only to have his not-too-bright girlfriend open a window to let some fresh air in.

The ghouls enter and overtake the couple, but only after a valiant attempt by Tom to decimate the invaders with his .357 magnum. During the struggle, Toms last bullet accidentally discharges into Barbaras femoral artery. Barbara bleeds to death in the hallway as Tom is devoured.

The next day, Barbara stumbles through downtown St. Paul as a newly activated zombie. She encounters Jesus Christ on a street corner and bites a huge bloody chunk out of his forearm. Barbara continues on, walking aimlessly through the empty city; she eventually collapses on the sidewalk. Barbaras appearance changes back to normal as she returns to life.

== Cast ==
Mark Borchardt&nbsp;– Jesus 
Nadine Gross&nbsp;– Barbara 
Mehrdad Sarlak&nbsp;– Tom 
Charles Hubbell&nbsp;– Lead Zombie 
Robert Elliot&nbsp;– T-Shirt Zombie 
Harrison Matthews&nbsp;– Redneck Zombie 
Jennifer Prettyman&nbsp;– Chick Zombie

== Release == Fantasia Film Festival in Montreal, Canada.

== See also ==
*List of zombie short films and undead-related projects

== References ==
 

== External links ==
* 
*  on the Internet Movie Database

 
 
 
 
 
 
 
 