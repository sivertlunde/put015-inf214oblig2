Naseeb Apna Apna (1970 film)
{{Infobox film
| name           = Naseeb apna apna
| image          =
| caption        =
| director       = Qamar Zaidi
| producer       = Waheed Murad
| writer         =
| narrator       = Tamanna Saqi
| music          = Lal Mohammad Iqbal
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = approx. 3 hours
| country        = Pakistan
| language       = Urdu
| budget         =
}}
 Pakistani Urdu black-and-white film directed by Qamar Zaidi and produced by Waheed Murad. It cast Waheed Murad, Shabnam, Zamrrud, Tamanna (actress)|Tamanna, Nirala and Saqi. It was a blockbuster musical love story. The songs of the film esp. Aye abr-e-karam... became so popular among the Waheed Murads and Shabnams fans. It proved to be one of the greatest hits in the early stage of Shabnams film career.

== Release ==
NAA was released on April 3, 1970 in Pakistani cinemas. The film completed 11 weeks on main cinemas and 32 weeks on other cinemas in   film.

== Music ==
The music of the film was composed by Lal Muhammad Iqbal and the songs were written by Masroor Anwar. Playback singers are Ahmed Rushdi (he got the Best singer  Nigar award for this film), Mala (Pakistani singer)|Mala, Runa Laila and Irene Parveen.  A list of the songs of the film is as follows:

*Aye abr-e-karam... by Ahmed Rushdi
*Dil tum ko dey diya hai... by Ahmed Rushdi
*Hum se na bigar o larki!... by Ahmed Rushdi Mala
*Mili gul ko khushboo... by Runa Laila
*Aaj nahi to kal is ghar mein... by Irene Parveen

== Awards ==
Ahmed Rushdi received Nigar Award in the best singers category for the song Aye abr-e-karam.... 

==References==
 

== External links ==
*  

 
 
 
 

 