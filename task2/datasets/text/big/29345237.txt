A Fonder Heart
{{Infobox film
| name           = A Fonder Heart
| image          = 
| caption        =  Jim Fitzpatrick
| producer       = James D. Brubaker Jim Fitzpatrick Jim Fitzpatrick
| music          = William Scott Davis
| cinematography = Lux
| editing        = Scott Conrad
| distributor    = MGM
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $7 million
}}
A Fonder Heart is an 2011 drama film directed by Jim Fitzpatrick about a 15 year-old boy, Joey Boone who shares his hospitalisation time with self-made multimillionaire Craig Thomas.

==Background==
The inspiration of the film is based around the true story of Jim Fitzpatricks childhood friend known only as Joey Boone, who died of bone cancer in 1974.

==Plot==
A Fonder Heart is based on a true story about Joey Boone, 15 year-old boy, and Craig Thomas, 59 year-old self-made multimillionaire developer. Joey and Craig became hospital roommates in 1974. Joey believes that he is simply being looked at for a minimal knee surgery due to a basketball injury, while Craig fears the worst...that life and old age have finally caught-up with him, and that he is dying. The two eventually develop empathy for each others personal problems and issues, as well as lifes similar hardships. However it is revealed that it is Joey is the one who is terminally ill and having made a father-son relationship Craig stays out the rest of Joeys days with him.

==Cast==
*Stockard Channing as Paula Silver
*Daryl Hannah as Margaret Boone
*Janine Turner as Nurse Linda
*James Brolin as Craig
*Louis Gossett, Jr. as Glen
*Audrey Landers as Nurse Hope
*Rachel Hunter as Shannon Jim Fitzpatrick as Captain Bligh
*Rebecca De Mornay as Dr. Bach

According to the director Jim Fitzpatricks website the actor William Shatner is set to make a cameo appearance.

==Filming==
These are a list of filming locations used in A Fonder Heart 
*Chicago, Illinois, USA
*Clearwater Beach, Florida, USA
*Clearwater, Florida, USA
*Clearwater-St. Petersburg Airport, Florida, USA
*Indian Rocks Beach, Florida, USA
*Scruggs Harbor, Florida, USA
*Tampa, Florida, USA

==References==
 

 
 
 
 
 