Escort West
{{Infobox film
| name           = Escort West 
| image          = 
| image size     =
| caption        = 
| director       = Francis D. Lyon
| producer       = Robert E. Morrison Nate H. Edwards
| screenwriter   = Leo Gordon Fred Hartsook
| story          = Steven Hayes 
| based on = 
| narrator       =
| starring       = Victor Mature Faith Domergue
| music          = Henry Vars
| cinematography = William H. Clothier Otto Ludwig
| studio = Batjac Productions Romina Productions
| distributor    = 
| released       = 1958
| runtime        = 
| country        = USA English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}}
Escort West is a 1959 Western, first released on Jan. 23, 1959, but sometimes listed as a 1958 release. The movie is set after the U.S. Civil War, when a former Confederate officer, played by Victor Mature, and his daughter help some survivors of an Indian massacre.
 China Doll (1957). 
 Love Me Tender, which filmed its climactic sequence at the same site, known as the "Rocky Hill." 

==Cast==
*Victor Mature as Ben Lassiter  
*Reba Waters as Abbey Lassiter
*Elaine Stewart as Beth Drury 
*Faith Domergue as Martha Drury  Noah Beery Jr. as Lt. Jamison    Harry Carey Jr. as  Trooper Travis
*Slim Pickens as  Corporal Wheeler  
*Ken Curtis as Trooper Burch
*X Brands as Tago
*Roy Barcroft as Sgt. Doyle 
*William Ching as Capt. Howard Poole  John Hubbard as Lt. Weeks Rex Ingram as Nelson Walker 
*Syd Saylor as Elwood Fenniman
*Claire Du Brey as Mrs. Kate Fenniman 
*Chuck Hayward as Indian 
*Charles Soldani as Indian
*Eddie Little Sky as Indian (uncredited)

==References==
 

==External links==
*  at IMDB
*  
* 
 
 
 
 
 
 
 

 