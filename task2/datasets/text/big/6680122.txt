Nijam
{{Infobox film
| name           = Nijam
| image          = Nijam 2003.jpg
| image_size     =
| caption        = Teja
| producer       = Teja
| writer         = Teja
| narrator       = Raasi Tottempudi Gopichand
| music          = R. P. Patnaik
| cinematography =
| editing        = Sankar
| distributor    =
| released       =  
| runtime        = 188 minutes
| country        = India
| language       = Telugu
| budget         =
}} Telugu film Rameshwari won Best Actor Best Supporting Tamil as Oriya starring Anubhab Mohanty and Gargi Mohanty.

== Plot == Jayaprakash Reddy) is a powerful mafia leader whose right hand is Devudu (Gopichand). Devudu has a lover called Malli (Raasi). Reddy - who also like Malli - takes her to the bed, which is not liked by Devudu. In the process, Devudu kills Reddy and he becomes the leader of the mafia gang. Venkateswarlu (Ranganath) is a fire officer. In an incident, Devudu sets a market place afire and Venkateswarlu rescues it with his fire fighting and at the same time Venkateswarlu slaps Devudu, as he continues throwing Kerosene at the market place. Devudu holds grudge against Venkateswarlu and sends him to jail on a framed charge of murder. Seetaram (Mahesh Babu) who is the son of Venkateswarlu tries to rescue his father from the jaws of police. But in the process, everybody starts asking him for money to do the work. At the end of the day, the police officer kills Venkateswarlu. The rest of the story is about how Seetarams mother (Talluri Rameswari) takes revenge on the people responsible for the death of her husband with the help of her son Seetaram. The criminals involved in the crime of his fathers death are killed one after the other by the mother and the son in a planned and scientific manner. This film tells about an innocent boy, who along with his mother undergoes injustice and also loses his father in this process. The mother in this situation plays the role of a trainer and a guide to her son, making him strong physically and emotionally, to fight back.

== Pan-India recognition ==
Nijam was also in the Top 5 movies in consideration for ‘India’s official entry to Oscars’ in the foreign film category. The movie got a Pan-India appeal and was dubbed in Hindi as Meri Adalat. The movie made decent business and is popular in YouTube.

==Cast==
{|class="wikitable"
|-
! Actor/Actress !! Role
|- Mahesh Babu Sitaram
|- Rakshita
|Meera, Sitarams love interest
|- Ranganath (actor)|Ranganath
|Venkateswarlu, Sitarams father
|- Talluri Rameshwari|Rameshwari
|Sitarams mother
|- Tottempudi Gopichand|Gopichand Devudu (Devadaya Sharma)
|- Mantra (actress)|Raasi Malli
|- Jaya Prakash Reddy Sidda Reddy
|- Prakash Raj ACP Khalid
|- Kanta Rao Narayana Rao
|- Rallapalli (actor)|Rallapalli Butcher
|- Vijayachander
|DGP
|- Dharmavarapu Subrahmanyam Traffic constable
|- Amitov Teja Voice of Sitaram
|}

==Awards==
===Won=== Nandi Awards  
* Nandi Award for Best Actor - Mahesh Babu
* Nandi Award for Best Supporting Actress - Rameshwari

;CineMAA Awards  CineMAA Award Gopichand

===Nominated===
;Filmfare Awards South
* Filmfare Award for Best Actor - Mahesh Babu
* Filmfare Award for Best Supporting Actress - Telugu - Taluri Rameswari

;CineMAA Awards
* CineMAA Award for Best Actor - Male - Mahesh Babu
* CineMAA Award for Best Film - Teja

==References==
 

==External links==
*  

 
 
 
 
 
 