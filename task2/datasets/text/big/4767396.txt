Saranggola
{{Infobox film
| name           = Saranggola
| image          =
| caption        = Movie poster
| director       = Gil Portes
| producer       = 
| writer         = Butch Dalisay (story) Butch Dalisay (screenplay)
| starring       = Ricky Davao Lester Llansang Jennifer Sevilla
| music          = 
| cinematography = 
| editing        = 
| distributor    = GMA Films
| released       =  
| runtime        = 87 minutes
| country        = Philippines Filipino / Tagalog
| budget         = 
}}
 Filipino drama film directed by Gil Portes, starring Ricky Davao, Lester Llansang and Jennifer Sevilla and produced by GMA Films and Teamwork Productions. It is a chilling morality tale, showing murder and corruption through the eyes of a child.

==Plot==
Though widowed, ex-cop, Homer, (Ricky Davao) is a kindly father to his ten-year-old son, Rex (Lester Llansang). He is considered to be something of a bully in the poor Manila neighborhood where he lives. After mistaking a child retrieving a snarled kite for a burglar, Homer kills the boy and then hurriedly tries to cover up his error. Unbeknownst to his dad, Rex witnessed the event, too. As the two separately wrestle with their consciences, the neighbors find out and chaos soon ensues.

==Awards and nominations==
The film won several awards at the 1999  ), Best Original Story, Best Screenplay (Gil Portes and Butch Dalisay) and Best Theme Song. It was also nominated for the Golden St. George at the 21st Moscow International Film Festival.    Ricky Davao was also hailed as Best Actor at the 1999 Cinemanila International Film Festival for this film.

In 2000, the film won the FAMAS Best Child Actor Award (Lester Llansang) and was nominated for four more awards (Best Picture, Best Director, Best Screenplay and Best Actor (Ricky Davao)) at the FAMAS Awards. It also won the Gawad Urian Award for Best Actor (Ricky Davao) and was nominated for 5 more awards at the 2000 Gawad Urian Awards. In the same year it won the Original Screenplay of the Year (Gil Portes and Butch Dalisay) and the Actor of the Year (Ricky Davao) and was nominated for 3 more awards at the Star Awards for Movies.

The film was also the Philippines official entry to: the Academy Awards (Oscars); the 2000 Young Artists Award, where it was nominated for Best International Film and Best Performance in an International Film (Lester Llansang); the 2000 Indian Film Festival; and other international film festivals.

==References==
 

==External links==
* 

 
 
 
 
 
 
 