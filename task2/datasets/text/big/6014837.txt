Paying Guest
 
{{Infobox film
| name           = Paying Guest
| image          = Paying Guest.jpg
| image_size     =
| caption        =
| director       = Subodh Mukherjee
| producer       = Sashadhar Mukherjee
| writer         = Nasir Hussain
| narrator       =
| starring       = Dev Anand Nutan
| music          = S.D. Burman
| cinematography =
| editing        =
| distributor    =
| released       =  1957
| runtime        = 157 minutes
| country        = India
| language       = Hindi
| budget         =
}} 1957 Bollywood film directed by Subodh Mukherjee. The film stars Dev Anand and Nutan along with Shobha Khote.

The soundtrack, composed by Sachin Dev Burman, received a degree of popularity and made a lasting impression  . The lyrics were written by Majrooh Sultanpuri. Paying Guest was the second hit film of the team of Mukherjee, Hussain, Dev Anand and S. D. Burman, who had combined two years earlier to make the successful Munimji.

==Cast==
* Dev Anand ...  Ramesh Kumar
* Nutan Behl ...  Shanti
* Gajanan Jagirdar ...  Dayal
* Sajjan ...  Jagat
* Shubha Khote ...  Chanchal
* Gani Dulari ...  Uma
* Rajendra
* Chaman Puri
* Sailen Bose
* Gitanjali
* Master Bapu Yakub ...  Prakash

==Music==
{|class="wikitable"
|-
!Song Title !!Singers !!Time
|-
|"Chand Phir Nikla" Lata Mangeshkar 
|4:43
|-
|-
|"Chhod Do Aanchal Zamana Kya Kahe Ga" Kishore Kumar, Asha Bhosle 
|4:12
|-
|"Chupke Chupke Rukte" Lata Mangeshkar
|2:21
|-
|-
|"Haaye Haaye Ye Nigahen" Kishore Kumar
|4:37
|-
|-
|"Mana Janaab Ne Pukara Nahin" Kishore Kumar
|4:16
|-
|-
|"O Nigahen Mastaana" Kishore Kumar, Asha Bhosle
|3:47
|-
|}

== External links ==
*  

 
 
 
 
 

 