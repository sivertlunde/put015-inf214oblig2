Chicago 10 (film)
 
{{Infobox film
| name           = Chicago 10
| image          = Chicago ten.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Promotional poster
| director       = Brett Morgen
| producer       = Laura Bickford William Pohlad Peter Schlessel Jeffrey Skoll Ricky Strauss Diane Weyermann
| writer         = Brett Morgen Jeffrey Wright
| music          = Jeff Danna
| editing        = Stuart Levy
| studio         = Consolidated Documentaries Participant Productions River Road Entertainment Curious Pictures
| distributor    = Roadside Attractions
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $177,490 
}} animated film Jeffrey Wright in an animated reenactment of the trial based on transcripts and rediscovered audio recordings, making the film fall in the animated documentary genre. It also contains archival footage of Abbie Hoffman, David Dellinger, William Kunstler, Jerry Rubin, Bobby Seale, Tom Hayden, and Leonard Weinglass, and of the protest and riot itself. The title is drawn from a quote by Rubin, who said, "Anyone who calls us the Chicago Seven is a racist. Because youre discrediting Bobby Seale. You can call us the Chicago Eight, but really were the Chicago Ten, because our two lawyers went down with us." 

==Cast==
* Hank Azaria as Abbie Hoffman / Allen Ginsberg David Stahl
* Nick Nolte as Thomas Foran
* Mark Ruffalo as Jerry Rubin
* Roy Scheider as Judge Julius Hoffman
* Liev Schreiber as William Kunstler Richard Schultz Jeffrey Wright as Bobby Seale
* Ebon Moss-Bachrach as Paul Krassner
* Debra Eisenstadt as Mary Ellen Dahl / Waitress Robert Pierson / Arthur Aznavoorian / Police officer
* Leonard Weinglass as himself
* Catherine Curtin as Barbara Callender
* Chuck Montgomery as Lee Weiner

==Production==
On the eve of the Afghanistan War, director Brett Morgen was spurred to create Chicago 10 in response to what he saw as the lack of active American opposition to the war. Morgen wanted to make the anti-Vietnam War protests of the 1960s resonate with contemporary youth, which influenced both the films animation style and its anachronistic soundtrack, the latter of which features modern artists such as Black Sabbath, Rage Against the Machine, the Beastie Boys, and Eminem. The animated courtroom sequences were also informed by Jerry Rubins description of the trial as a "cartoon show."   

==Release== PBS program, Independent Lens, on October 22, 2008. 

===Critical reception===
The film received generally favorable reviews from critics. As of April 11, 2010, the review aggregator Rotten Tomatoes reported that 79% of their critics gave the film positive reviews, based on 77 reviews.  Metacritic reported the film had a weighted average score of 69 out of 100, based on 24 reviews. 

==See also==
* The Chicago 8 (film)
* Chicago Seven

==References==
 

==External links==
*  
*  
*  
*   PBS 
*   at Sundance.org
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 