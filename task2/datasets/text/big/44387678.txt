Aadyathe Anuraagam
{{Infobox film 
| name           = Aadyathe Anuraagam
| image          =  
| caption        = 
| director       = VS Nair
| producer       = MA Sherif
| writer         = VS Nair
| screenplay     = VS Nair Ambika
| music          = Raveendran
| cinematography = Vipin Das
| editing        = G Venkittaraman
| studio         = Remmis Movies
| distributor    = Remmis Movies
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by Raveendran.   

==Cast==
*Prem Nazir
*Sukumari
*Adoor Bhasi Ambika
*MG Soman Ramu

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Madhu Alappuzha and Devadas. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maampoo choodiya makaram || P Jayachandran || Madhu Alappuzha || 
|-
| 2 || Manjakkanikkonna || S Janaki || Madhu Alappuzha || 
|-
| 3 || Puthumulla Poove arimulla poove || K. J. Yesudas || Devadas || 
|-
| 4 || Raagam Anuraagam || K. J. Yesudas, Sujatha Mohan || Devadas || 
|}

==References==
 

==External links==
*  

 
 
 

 