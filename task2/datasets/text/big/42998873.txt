Mamma Ebe
{{Infobox film
 | name = Mamma Ebe
 | image =  Mamma Ebe-591339889-large.jpg
 | caption =
 | director = Carlo Lizzani
 | writer =  
 | starring =  
 | music = Franco Piersanti
 | cinematography =  Romano Albani
 | editing =  Franco Fraticelli 
 | producer =  Giovanni Di Clemente
 | language = Italian  
 }} 1985 drama courtroom drama film directed by Carlo Lizzani. It was entered into the main competition at the 42nd Venice International Film Festival, in which Barbara De Rossi won the Pasinetti Award for best actress.    Based on real events, the film was poorly received by critics.      

== Cast ==

* Berta D. Dominguez: Mamma Ebe  (credited as Cassandra Domenica)
* Stefania Sandrelli: Sandra Agostini
* Barbara De Rossi: Laura Bonetti
* Ida Di Benedetto: Maria Pia Sturla
* Alessandro Haber: Mario Bonetti
* Laura Betti: Lidia Corradi
* Paolo Bonacelli: Don Paolo Monti
* Giuseppe Cederna: Bruno Corradi
* Carlo Monni: Foschi
* Massimo Sarchielli: Oste
* Luigi Pistilli: Roberto Lavagnino
* Maria Fiore: Mara
* Enzo Robutti: Bishop
* Federico Sangirardi Wardal: Don Franco

==References==
 

==External links==
* 

 

 
 
 
  
 
 
 

 
 