Vivahasammanam
{{Infobox film 
| name           = Vivahasammanam
| image          =
| caption        =
| director       = JD Thottan
| producer       =
| writer         = SK Marar S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Sheela Kaviyoor Ponnamma Adoor Bhasi
| music          = G. Devarajan
| cinematography =
| editing        = VP Krishnan
| studio         = Aruna Productions
| distributor    = Aruna Productions
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by JD Thottan . The film stars Prem Nazir, Sheela, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Kannankutty
*Sheela as Gourikkutty
*Kaviyoor Ponnamma
*Adoor Bhasi
*Thikkurissi Sukumaran Nair Prema
*T. S. Muthaiah
*Alummoodan
*K. P. Ummer Meena
*Rani Chandra Sadhana
*T. K. Balachandran
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambarathi Chembrathi || P. Madhuri || Vayalar Ramavarma || 
|-
| 2 || Kaalam Sharathkaalam || A. M. Rajah, Chorus || Vayalar Ramavarma || 
|-
| 3 || Mohabhangangal || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Veenedam Vishnulokam || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Velutha Vaavinekkaal || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 