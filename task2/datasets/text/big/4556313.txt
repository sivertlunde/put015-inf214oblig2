Love on the Run (1936 film)
{{Infobox film
| name = Love on the Run
| image = Loveontherunposter07x.jpg
| image_size = poster
| director = W.S. Van Dyke
| producer = Joseph L. Mankiewicz
| writer = Story:   John Lee Mahin Manuel Seff
| narrator =
| starring = Joan Crawford Clark Gable Franchot Tone
| music =
| cinematography = Olive T. March Frank Sullivan
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 80 minutes
| country = United States English
| budget = $578,000  . 
| gross = $1,862,000 
}}
 Alan Green and Julian Brodie. The film was directed by W.S. Van Dyke and produced by Joseph L. Mankiewicz.  Love on the Run is the seventh of eight cinematic collaborations between Crawford and Gable.

==Synopsis==

Rival London-based American newspaper correspondents Michael Anthony and Barnabas Pells flip a coin to determine who will cover which of two boring assignments. Mike gets the story about millionairess Sally Parkers wedding to fortune-seeking Prince Igor, while Barney takes an interview with aviator Baron Otto Spandermann and his wife Hilda. On the way into the wedding, Mike sees Sally running out of the church and follows her, hoping to get a story. At her hotel, Mike runs into the suspicious Barney, but doesnt tell him what just happened, then sneaks into Sallys hotel room, and telling her that he has admired her for years, suggests that he help her "get away from it all." When the gigolo prince comes to the hotel, Mike slugs him when the prince recognizes him as a reporter, and he and Sally decide to run away, using the baron and baroness flying suits as disguises. Barney chases them to the airport, but is too late and they fly away, though neither is a pilot. Just before they crash land in France, they find a munitions map in a bouquet of flowers intended for the baroness and realize that the aviators are spies. Though Mike has sent a secret cablegram about Sally to his editor Berger in New York, he is even more excited about the spy story. In Paris, after getting money from his paper, Mike and Sally are found by Barney, then are spotted by the baron and baroness. As they flee, Barney comes along until Mike pushes him into the back of the truck they steal and convinces Sally that Barney is a lowlife reporter. By nightfall, they arrive at the Palace of Fontainebleau and sneak in to spend the night. During the evening, they realize that they are in love, and Mike tries to tell her that he is a reporter, but cant. Next morning, Barney finds them again, but Sally doesnt believe him when he accuses Mike of being a reporter, too. Soon, however, an ashamed Mike gives her a newspaper with his byline and she realizes what he has done. He apologizes and tells her he loves her, but she sends him away. When Barney arrives, she says she will give him the greatest story of his career, and they go off to make headlines. A short time later, while they are traveling by train to Nice, Sally realizes that she still loves Mike and wants to go to him, but just then the baron and baroness come into their compartment with guns and demand that Sally give them the map. They search Sally but do not find it and leave her after pushing Barney off the train. When Barney catches up with Mike at a cafe in Paris and tells him what has happened, Mike decides to go to Nice himself to save Sally. In Nice, Mike is lovingly reunited with Sally at her hotel and they go to the train station. In the station, the baroness switches clothes with Sally in the ladies room, then goes with Mike, posing as Sally. The baron then finds Sally and takes her to a restaurant. She tries to contact the police, but when two policemen arrive, they believe the barons story that she stole his plane. The baron then kidnaps Sally and the policemen and takes them to his chateau, where the baroness has Mike bound and gagged, held captive. Having followed Mike and the baroness, thinking that she was Sally, Barney also arrives. Barney at first laughs at Mike and then realizes that he is actually in trouble, and frees him. Using various ruses, Mike, Sally and the policemen eventually overwhelm the baron and baroness and Sally and Mike go off, leaving Barney tied-up, but Mike has a change of heart and returns, once again finding Barney trying to get his story first by using the chateau phone to cable his editor. They finally agree to file a joint byline, and Sally and Mike agree that they will soon be married.

==Cast==

* Joan Crawford as Sally Parker
* Clark Gable as Michael Mike Anthony
* Franchot Tone as Barnabus W. Barney Pells
* Reginald Owen as Baron Otto Spandermann
* Mona Barrie as Baroness Hilda Spandermann
* Ivan Lebedeff as Prince Igor
* Charles Judels as Lieutenant of Police
* William Demarest as Editor Lees Berger
* Donald Meek as Fontainbleau Palace Caretaker

==Reception==

Howard Barnes of the New York Herald Tribune wrote, "A lot of gay nonsense has been strung together....a fantastic and insubstantial narrative, with the result that it is almost continuously amusing and frequently hilarious... Miss Crawford, of the big eyes and flowing hair, turns in a surprisingly volatile and amusing performance as the heiress."
==Box Office==
According to MGM records the film earned $1,141,000 in the US and Canada and $721,000 elsewhere resulting in a profit of $677,000. 
==See also==
* Clark Gable filmography

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 