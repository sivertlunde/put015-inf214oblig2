It Always Rains on Sunday
 
 
{{Infobox film
| name = It Always Rains on Sunday
| image = Rainsonsunday.jpg quad poster
| director = Robert Hamer
| producer = Michael Balcon
| screenplay = Angus MacPhail Robert Hamer Henry Cornelius
| based on =   John McCallum Jack Warner
| music =Georges Auric
| cinematography = Douglas Slocombe
| editing = Michael Truman
| studio = Ealing Studios GDF  
| released =  
| runtime = 92 minutes
| country = United Kingdom
| language = English
| budget =
}} Oxford Dictionary of National Biography,  and Graham Fuller. 

==Plot==
The film concerns events one Sunday (23 March 1947, according to the calendars in the film) in Bethnal Green, a part of the East End of London that was suffering the effects of bombing and post-war deprivation.
 Edward Chapman) John McCallum, who soon afterwards married Withers), jailed some years earlier for robbery with violence, escapes from prison and is discovered by Rose hiding in the familys Air-raid shelter#Anderson shelter|air-raid shelter. He asks her to hide him until nightfall. Rose initially refuses but, clearly still in love with him, eventually allows him to hide in the bedroom she shares with her husband, after the other members of the household have gone out. She then keeps the bedroom locked.

However, it proves extremely difficult to keep the presence of the escapee a secret in such a busy, bustling household – particularly with her former lover intent on seducing her. It is Sunday morning and the lunch must be cooked, the girls admonished for their misdemeanours of the previous night and the husband packed off to the pub out of the way. The strain is intolerable and as the day progresses, the police net closes, after a newspaper reporter interrupts them, as Tommy is about to flee, and soon tips off the police.
 Jack Warner) who has been patiently tracking him. As the film ends, Rose is in hospital recovering, and reconciles with her husband, who then returns alone to their home, under a clear sky.

==Main cast==
 
*Googie Withers as Rose Sandigate Edward Chapman as George Sandigate
*Susan Shaw as Vi Sandigate
*Patricia Plunkett as Doris Sandigate
*David Lines as Alfie Sandigate
*Sydney Tafler as Morrie Hyams
*Betty Ann Davies as Sadie Hyams John Slater as Lou Hyams
*Jane Hylton as Bessie Hyams
*Meier Tzelniker as Solly Hyams, father of Morrie John McCallum as Tommy Swann
*Jimmy Hanley as Whitey
*John Carol as Freddie
*Alfie Bass as Dicey Perkins Jack Warner as Detective Sergeant Fothergill
*Frederick Piper as Detective Sergeant Leech Michael Howard as Slopey Collins
*Hermione Baddeley as Doss-house keeper Nigel Stock as Ted Edwards
*John Salew as Caleb Neesley
*Gladys Henson as Mrs Neesley
*Edie Martin as Mrs Watson
*Vida Hope as Mrs Wallis
*Arthur Hambling as Yardmaster
*Grace Arnold as Landlady
 

==Reception and reputation==
The film was one of the most popular movies at the British box office in 1948. 

In the decades since its release, the reputation of It Always Rains on Sunday has grown from that of a neatly engrossing slice-of-life drama to a film often cited as one of the most overlooked achievements of late-1940s British cinema. Writing in Films in Review in 1987,  : "A fascinating noirish look at life in Londons East End...the scenes between Withers and McCallum are stunningly erotic", while Stephen Garrett of Time Out summed the film up as: ""Absolutely exhilarating! A bleak thriller realised with utter vibrancy, Robert Hamers savoury stew of Londons lower class roils with an emotional brutality and precision that most films don’t dare attempt, let alone achieve." 
 Les Enfants du Paradis." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 