Free Birds
 
{{Infobox film
| name = Free Birds
| image = Free Birds poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Jimmy Hayward
| producer = Scott Mosier 
| screenplay = Jimmy Hayward Scott Mosier 
| story = David I. Stern John J. Strauss
| starring = Owen Wilson Woody Harrelson Amy Poehler
| music = Dominic Lewis 
| cinematography = 
| editing = Chris Cartagena 
| studio = Relativity Media Reel FX Creative Studios
| distributor = Relativity Media
| released =  
| runtime = 91 minutes 
| country = United States
| language = English
| budget = $55 million   
| gross = $110,387,072 
}} 3D Computer buddy comedy film produced by Reel FX Creative Studios, directed by Jimmy Hayward and it stars the voices of Owen Wilson, Woody Harrelson and Amy Poehler.    Originally titled Turkeys,  it was scheduled for 2014, but it was released on November 1, 2013 by Relativity Media. 

==Plot== turkey has pardoned turkey" by the President of the United States and is subsequently taken to Camp David. Although initially hesitant, Reggie soon eases into a routine of doing nothing but enjoying pizza delivered to him by the "Pizza Dude" and watching Mexican telenovelas.
 time machine hunters led native Wild turkeys led by Chief Broadbeak and his two children, Ranger and Jenny.

Broadbeak explains that the turkeys in the area have been forced underground since the settlers came and that they cannot risk fighting back without the settlers taking them. The next day, Broadbeak orders Jake with Ranger to spy on the settlers and Reggie with Jenny to spring all the hunting traps the humans set up. Despite initial hostility, Ranger and Jake find out that the settlers have already begun preparations for Thanksgiving and where they keep their weapons. Meanwhile, Jenny believes in Reggie for lying about being from the future and is impressed with his accidental unorthodox way of springing the traps. However, they are soon intercepted by Standish and Reggie is forced to get her in orbit over the planet aboard S.T.E.V.E., validating everything he said in the process. Reggie asks Jenny to go back to the future with him once everything blows over, but she refuses to leave the flock no matter how much she likes him. Jake then drags Reggie away and tells him he has a plan to attack the settlers. However, Reggie has gotten sick of all his unapproved stories and threatens to leave.

Desperate, Jake tells him that this trip was more about him making up for his failure to save turkey eggs while escaping a turkey-fattening facility when he was young, maintaining that the Great Turkey convinced him to go through with this. While still reluctant to believe what he said, Reggie still goes along with the plan. They manage to use gunpowder to destroy the weapons shack, but Jake inadvertently leaves a gunpowder trail back to the tree the turkeys are hiding under. Standish and his men flush the turkeys out from underground, capturing enough for the feast. Broadbeak sacrifices his life to get the remaining turkeys into the tide pool. Jenny is sworn in as the new chief and orders the remaining turkeys to prepare an attack on the settlers.
 Indians arrive. Inspired, Reggie goes back in time to stop the attack inadvertently trapping Standish in the time stream. Through S.T.E.V.E. and the Pizza Dude, Reggie convinces the settlers and the Indians that pizza is a more acceptable food than turkeys, taking them off the Thanksgiving menu entirely. Reggie stays with Jenny while Jake takes S.T.E.V.E. in order to look for new adventures.

In the mid-credits, Jake returns moments after leaving to Reggie and Jenny. With a chicken and duck in his wings, Jake starts to tell every turkey present about the turducken.

==Cast==
* Owen Wilson as Reggie, a turkey who is pardoned by the President of the United States and is dragged into Jakes plot.   
* Woody Harrelson as Jake, a turkey and the president of the Turkeys Liberation Front.     
* Amy Poehler as Jenny, a turkey and Reggies love interest.    
* Dan Fogler as Governor Bradford  Lesley Nicol as Pilgrim Woman 
* George Takei as S.T.E.V.E., the artificial intelligence of the time machine. 
* Colm Meaney as Myles Standish, a pilgrim hunter. 
* Keith David as Chief Broadbeak, the chief of the native turkeys and Jennys father. 
* Jimmy Hayward as President of the United States, Ranger, Leatherbeak and Hazmats
* Kaitlyn Maher as Presidents daughter 
* Carlos Alazraqui as Amos 
* Josh Lawson as Gus   
* Danny Carey as Danny 
* Dwight Howard as Cold Turkey 
* Carlos Ponce as Narrator, Alejandro
* Robert Beltran as Chief Massasoit
* Scott Mosier as Pizza Dude

==Production== Reel FX and Granat Entertainment launched in 2010 Bedrock Studios (later renamed to Reel FX Animation Studios)    to produce sub $35 million family oriented projects.    Ash Brannon was then set to direct the film,  but in October 2012, when it was announced that Relativity Media would co-finance, co-produce, and distribute the film, Jimmy Hayward took over the directing position.    Originally scheduled for November 14, 2014,    the film was moved up by a year to November 1, 2013, due to the vacant slot left after the delay of Mr. Peabody & Sherman.  In March 2013, the film was retitled to Free Birds.   

==Release==

===Critical response=== normalized rating out of 100 top reviews from mainstream critics, calculated a score of 38 based on 27 reviews, indicating "generally unfavorable reviews". 

Justin Chang of Variety (magazine)|Variety gave the film a negative review, saying "This seemingly innocuous toon fantasy becomes another noxious-but-sanitized exercise in family-friendly cultural insensitivity."    Alonso Duralde of The Wrap gave the film a negative review, saying "Even setting aside the films disregard for time-travel paradoxes and genocide metaphors—trust me, you dont want to wade into either of those—Free Birds just isnt funny."  Stephanie Zacharek of The Village Voice gave the film a negative review, saying "Like so many modern animated features, Free Birds packs too much in; the picture feels cramped and cluttered, and, despite its occasionally manic action, it moves as slowly as a fattened bird waddling toward its doom."  Kate Erbland of Film.com gave the film a 7.6 out of 10, saying "Free Birds is a more than worthy (and weird) holiday diversion for the whole family."  Stephan Lee of Entertainment Weekly gave the film a C, saying "Often, you can point to a middling animated films visuals as its saving grace. But this colonial world, which should feel like an expansive autumnal panorama, feels oddly inert and two-dimensional."  Claudia Puig of USA Today gave the film two and a half stars out of four, saying "The 3-D animated film, the first of the holiday entries, is likable and amusing, if slight."  Chris Cabin of Slant Magazine gave the film one out of four stars, saying "The film is absent of humor and thrills, and accented with designs and color schemes that are equally notable for their lack of risk." 

Sheri Linden of the Los Angeles Times gave the film a negative review, saying "Like the ungainly avian creatures at the center of the herky-jerky adventure, this toon seldom gets off the ground."  Jessica Herndon of the Associated Press gave the film two out of four stars, saying "A solid premiere effort that shows Reel FXs potential to produce quality full-length animation. But the story-line, with its hypothetical constituents, seems a little desperate at times, even for a kiddie film."  Linda Barnard of the Toronto Star gave the film one out of four stars, calling the film "A seasonally pegged 3D cartoon bore that sets the bar so low, it could give a slug a concussion."  Tom Russo of The Boston Globe gave the film two out of four stars, calling the film "A welcome foray into underexploited territory, conceptually at least."  Bill Goodykoontz of The Arizona Republic gave the film two out of five stars, saying "It isnt cute. It isnt really funny. It just kind of is."  Louis Black of The Austin Chronicle gave the film one and a half stars out of five, saying "Free Birds falls flat, despite its good intentions, ideological cuteness, humorous polish, and skillful computer animation. The fine voice talents of the almost-ideal cast are wasted."  Elizabeth Weitzman of the New York Daily News gave the film two out of five stars, saying "Most minor animated movies are so rote that it’s worth acknowledging a strange bird like this cheerfully gonzo kid flick. It’s no masterpiece, but if you’re hoping for a family film that will keep everyone reasonably entertained, this will fly." 

Miriam Bale of The New York Times gave the film a negative review, saying "The concept is inane, and the execution is manic and unoriginal."  Sara Stewart of the New York Post gave the film one and a half stars out of four, saying "Is Hollywood scheming to turn your little ones into strident vegetarians? Could be, but I wish they’d do it with material more inspired than Free Birds, a forgettable—and occasionally borderline offensive—animated tale of turkeys trying to take back Thanksgiving."  David Hiltbrand of The Philadelphia Inquirer gave the film one and a half stars out of four, saying "Free Birds is a stale turkey hash that heaves a lot of ingredients in the oven but never turns on the gas, a frantic attempt to come up with an animated film built around Thanksgiving Day traditions."  Liam Lacey of The Globe and Mail gave the film one and a half stars out of four, saying "The movies animal rights, vegetarian message should go down easily with politically correct parents—at least until they choke on the offensive depiction of 17th-century turkeys as face-painted, headband-wearing native Americans."  Stephanie Merry of The Washington Post gave the film two and a half stars out of four, saying "Finally, theres a movie vegetarian parents can enjoy with their impressionable offspring."  Peter Hartlaub of the San Francisco Chronicle gave the film one out of four stars, saying "In execution, the film is all sidekicks and sight gags, with little story cohesion or purpose." 

Bill Zwecker of the Chicago Sun-Times gave the film a mixed review, saying "No, Free Birds is not (sorry) a turkey of a film. But it doesn’t really soar terribly high either. I only wish the quality of the writing in the earlier parts of the movie had been maintained throughout. If that had been the case, Reggie, Jake and their fellow turkeys just might have been flying high with the eagles—our official national birds."  Michael Rechtshaffen of The Hollywood Reporter gave the film a mixed review, saying "Although it seldom approaches the inspiration of its plucky premise—a pair of turkeys travel back in a time machine to the first Thanksgiving in a bid to scratch the traditional entree off the menu—Free Birds nevertheless manages to avoid being branded a holiday turkey."  Christy Lemire of RogerEbert.com gave the film one and a half stars out of four, saying "Everything about Free Birds feels perfunctory, from its generic title and holiday setting to its starry voice cast and undistinguished use of 3-D." 

===Box office===
Free Birds grossed $55,750,480 in North America, and $54,400,000 in other countries, for a worldwide total of $110,150,480.  In North America, the film opened to number four in its first weekend, with $15,805,237, behind   and Last Vegas.  In its second weekend, the film moved up to number three, grossing an additional $11,112,063.  In its third weekend, the film dropped to number four, grossing $8,106,151.  In its fourth weekend, the film dropped to number five, grossing $5,363,208. 

===Home media===
Free Birds was released on DVD and Blu-ray on February 4, 2014 by 20th Century Fox Home Entertainment. 

==Soundtrack==
{{Infobox album
| Name       = Free Birds
| Type       = Soundtrack
| Artist     = Dominic Lewis
| Cover      =
| Released   = October 29, 2013
| Recorded   = 2013
| Genre      = Film score
| Length     = 64:45
| Label      = Relativity Music Group
| Producer   =
| Chronology = Dominic Lewis film scores
| Last album = 
| This album = 
| Next album =
}}
The films score was composed by Dominic Lewis. The soundtrack was released by Relativity Music Group on October 29, 2013. 

; Track listing
{{Track listing
| all_music       = Dominic Lewis, except as noted
| total_length    =

| title1          = Up Around the Bend
| note1           = performed by Social Distortion
| length1         = 3:22

| title2          = Starry Side Up 
| note2           = 
| length2         = 0:42

| title3          = All the Trimmings
| note3           = 
| length3         = 0:44

| title4          = Turkatory
| length4         = 2:50

| title5          = Poultry Pardon
| length5         = 2:18

| title6          = El Solo Pavo
| length6         = 1:56

| title7          = Turknapped
| length7         = 1:22

| title8          = Shell Shocked
| length8         = 2:30

| title9          = Secret Military Baster
| length9         = 1:56

| title10         = Cranberry Saucer
| length10        = 4:16

| title11         = Eggs-istential Crisis
| length11        = 0:30

| title12         = Killing Two Birds with One Standish
| length12        = 0:52

| title13         = Fly or Get Stuffed
| note13          =
| length13        = 1:51

| title14         = School of Flock
| length14        = 2:38

| title15         = Roast This! 
| length15        = 1:00

| title16         = Tryptophan Traps
| length16        = 0:31

| title17         = Jive Turkey
| length17        = 1:07

| title18         = Lazy Eye View
| length18        = 1:19

| title19         = Birds of a Feather
| length19        = 1:10

| title20         = Great Egg-scape 
| length20        = 2:23

| title21         = Hold on to Your Giblets
| length21        = 4:56

| title22         = The Right Stuffing 
| length22        = 0:39

| title23         = Fowl Play
| length23        = 5:23

| title24         = Laid to Rest 
| length24        = 3:01

| title25         = Cold Turkey
| length25        = 3:54

| title26         = The Great Turkey
| note26          = 
| length26        = 1:20

| title27         = Iratus Aves 
| note27          =
| length27        = 3:48

| title28         = Ocellata Turkeys
| note28          = 
| length28        = 3:19

| title29         = Back in Time
| note29          = performed by Matty B
| length29        = 
}}

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 