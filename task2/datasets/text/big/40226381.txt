Vikingdom
 
 
 
{{Infobox film name           = Vikingdom image          = image size     = caption        = director  Yusry Abdul Halim producer       = Shireen M. Hashim starring       = Dominic Purcell Conan Stevens Craig Fairbrass Natassia Malthe Jon Foo distributor  KRU Films released       =   runtime        = 1hr. 54mins. country        = Malaysia awards         = language       = English budget         = $15.6 million (est.) gross          = $550,796  
}}
 Yusry Abdul Halim and was released in Malaysia and the United States on 12 September 2013 and 4 October 2013 respectively.    

==Plot==
In the midst of time comes the clanging of steel against steel, and in a collision of myth and history, there is the "Vikingdom". Based on Viking folklore and the poems they left, "Vikingdom" is a fantasy, action adventure film about a forgotten king named Eirick. Tasked with the impossible odds to defeat  , his hammer from Valhalla; the Necklace of Mary Magdalene from Midgard|Mitgard; and the Horn from Hel (location)|Helheim. This needs to be accomplished before the sinister event of the Blood Eclipse, or else dire consequences will be faced by all in the realm. 

==Cast==
* Dominic Purcell as Eirick
* Natassia Malthe as Brynna
* Conan Stevens as Thor
* Jon Foo as Yang
* Craig Fairbrass as Sven
* Bruce Blain as Bernard 
* Jesse Moss as Frey
* Tegan Moss as Freyja Patrick Murray as Alcuin
* Creighton Mark Johnson as Raider Captain
* Michael Hagerty-Roach as Yorick 

==Reception==
Critical reception for Vikingdom was predominantly negative and the film holds a rating of 39 on Metacritic (based on 6 reviews) and 43% on Rotten Tomatoes (based on 7 reviews).    

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 