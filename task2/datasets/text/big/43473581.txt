Enne Petha Raasa
{{Infobox film
| name           = Enne Petha Raasa
| image          =
| image_size     =
| caption        =
| director       = Siraaj
| producer       = Rajkiran
| writer         = Rajkiran
| screenplay     = Siraaj Rupini Srividya Vinu Chakravarthy
| music          = Ilayaraja
| cinematography = P. Ganesapandian
| editing        = L. Kesavan
| studio         = Red Sun Art Creations
| distributor    = Red Sun Art Creations
| released       =  
| country        = India Tamil
}}
 1989 Cinema Indian Tamil Tamil film,  directed by Siraj and produced by Rajkiran. The film stars Ramarajan, Rupini (actress)|Rupini, Srividya and Vinu Chakravarthy in lead roles. The film had musical score by Ilayaraja.   

==Cast==
 
*Ramarajan Rupini
*Goundamani
*Srividya
*Vinu Chakravarthy
*Sadhana in Guest Role
*Senthil
*G. Seenivasan
*Ilavarasan
*Durga Sakthivel
*Krishnamoorthy
*Haja Sherif
*Bailwan Ranganathan
*Rajkiran
*Thairvadai Desikan
*Idichapuli Selvaraj
*Vellai Subbiah
*Periya Karuppu Thevar
*Karuppu Subbiah
 

==Soundtrack==
The music was composed by Illayaraja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Chithra || Pirai Sudan || 04:36 
|- 
| 2 || Aasa Vacha Peraiyellam || S. P. Sailaja || Gangai Amaran || 04:37 
|- 
| 3 || Malligai Poo Kathilile || P. Susheela || Gangai Amaran || 04:38 
|- 
| 4 || Ellorakkum Nallavana || Illayaraja || Illayaraja || 03:56 
|-  Mano || Pirai Sudan || 04:36 
|-  Mano || Gangai Amaran || 04:53 
|- 
| 7 || Petha Manasu || Illayaraja || Illayaraja || 04:35 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 