Flashover (film)
 
{{Infobox film
| name = Flashover
| image = Flashover poster.jpg
| director = Paul Medico Sean OConnor
| producer = Amanda McGrady
| starring = April Deacon John Deacon
| released =  
| runtime = 30 minutes
| language = English
| budget = $4,000
}}
Flashover is a 2008 documentary film concerning the release of John Deacon from prison after he was wrongfully convicted of arson in 2005.

==Story==
March 27, 2004 an apartment building in Brockton, MA caught on fire resulting in two injured persons and hundreds of thousands of dollars in damages. A stove in one of the apartments malfunctioned and fed gas back into the ventilation system. The gas poured into the entire room and eventually combusted. Initial investigations revealed traces of flammable liquids soaked in various fabrics within the apartment. An arson investigation began soon after the discovery.

Police targeted two men: Frank Iannetta and John Deacon. Ianetta, the owner and superintendent of the apartment building would often delegate the electrical work to John Deacon. On March 27, John Deacon had been working on the 4th floor installing new outlets in compliance with health and safety regulations.

A week before the fire, Iannetta received a 2nd notice to make a number of repairs or the building would be condemned. The investigators took Iannetta and Deacon in for questioning. Ianetta confessed to setting up the fire to collect insurance money and also claimed that Deacon had sabotaged the stove to feedback into the room.

Frank Ianetta was convicted of conspiracy and sentenced to three years in prison and ten years probation. John Deacon was convicted of arson and conspiracy and was sentenced to nine years, six months in prison and ten years probation.

About a year and a half later, a similar fire occurred in Everett, MA. Investigators concluded that the fire was caused by flashover. Flashover is the simultaneous combustion of anything flammable within one area. Flashover occurs when smoke consumes all of the oxygen within a room. As the oxygen decreases, the temperature increases until it reaches the "fire point," or 600 degrees Celsius.
 
The Everett flashover case resulted in similar cases to be re-examined, including Deacon/Ianettas case. This time, investigators concluded that there was no foul play after all and that flashover had occurred. John Deacons conviction was overturned and Frank Ianetta, who had already been released nine months prior, was taken off probation.

==Production==
On July 8, 2007, April Deacon placed a want ad on craigslist asking for help in making a documentary about her fathers highly publicized and heavily scrutinized release from prison. April felt that the media was treating her father unfairly and wanted to take some action to clear her innocent fathers name.

April and her brother Aden had already begun shooting some introductory scenes with her own Sony handi-cam. A few of these scenes were included in the films final cut and act as an introduction to her fathers story. Within a few days, a small production crew ( ) took on her project. Paul Medico and Sean OConnor served as directors and Amanda McGrady as producer. April also joined as producer and general consultant.
 High Definition  cameras. Principal photography took place over four weeks from July 10- August 7 (including April original footage). Filming took place in and around Greater Boston.

==Controversy==
April Deacon was purportedly given the final approval of the documentary, however, has publicly boycotted the final version of the film claiming that it tarnishes her fathers image.

==Screenings==
The Flashover world premiere occurred on June 13, 2008 at The Boston International Film Festival.  The screening took place at the Loews/AMC Boston Common theater at 6:00PM. The mockumentary won the "Best Short Film" award at the festival.

Flashover also screened on July 26, 2008 at The Woods Hole Film Festival. The screening took place at the Old Woods Hole Fire Station at 7:00PM.

==References==
 

==External links==
*  
*  

 
 
 
 