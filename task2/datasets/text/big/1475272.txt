Bad Boy Bubby
 
 
{{Infobox film
| name           = Bad Boy Bubby
| image          = Bad_boy_bubby.jpg
| image_size     = 220px
| alt            = 
| caption        = Blue Underground DVD cover
| director       = Rolf de Heer
| producer       = Rolf de Heer Giorgio Draskovic Domenico Procacci
| writer         = Rolf de Heer
| starring       = Nicholas Hope Claire Benito Ralph Cotterill Carmel Johnson
| music          = Graham Tardif
| cinematography = Ian Jones
| editing        = Suresh Ayyar
| studio         = South Australian Film Corporation
| distributor    = Roadshow Entertainment
| released       =  
| runtime        = 114 minutes  
| country        = Australia Italy
| language       = English
| budget         = United States dollar|USD$750,000
| gross          = Australian dollar|A$808,789 
}} drama film written and directed by Rolf de Heer. It stars Nicholas Hope and Carmel Johnson. The film became notorious  for pushing the boundaries of good taste with its strong scenes featuring violence, incest and blasphemy amongst other taboo topics.

==Plot==
Bubby is a 35-year-old man who has never set foot outside his mothers dingy apartment in the back of a printing press in an industrial area of Adelaide. In addition to beating and sexually abusing him, she confines him to the apartment, telling him that the air outside is poisonous and telling him he will die if he tries to leave. Bubby eventually escapes, joins up with a rock band, and embarks on a journey of self-discovery and shocking mayhem.

==Cast==
* Nicholas Hope as Bubby
* Claire Benito as Mam
* Ralph Cotterill as Pop
* Carmel Johnson as Angel

==Production background==
Shortly after he had graduated from film school, Rolf de Heer and Ritchie Singer collaborated on the idea of what would eventually become Bad Boy Bubby. For most of the 1980s, de Heer collected ideas and wrote them on index cards. In 1987, he took a hiatus from making Bubby index cards, but in 1989 he resumed work. Sometime between 1989 and 1990, he saw the short film Confessor Caressor starring Nicholas Hope and tracked him down. In 1991, he began work on the actual script.

==Audio and visual innovation== binaural microphones directors of photography to shoot different scenes. Once Bubby leaves the apartment a different director of photography is used for every location until the last third of the film, allowing an individual visual slant on everything Bubby sees for the first time. No director of photography was allowed to refer to the work of the others. 

==Awards==

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards AFI Awards)  AACTA Award Best Film Giorgio Draskovic
| 
|- Domenico Procacci
| 
|- Rolf de Heer
| 
|- AACTA Award Best Direction
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Actor Nicholas Hope
| 
|- AACTA Award Best Cinematography Ian Jones
| 
|- AACTA Award Best Editing Suresh Ayyar
| 
|- Seattle International Film Festival Golden Space Needle Award for Best Director Rolf de Heer
| 
|- Valenciennes International Festival Audience Award
| 
|- Venice Film Festival FIPRESCI Prize
| 
|- Grand Special Jury Prize
| 
|- Special Golden Ciak
| 
|- Golden Lion
| 
|-
|}

==Release== cruelty to a cat.  The film was released on DVD in April 2005 by the Blue Underground company, and a special Two Disc Collectors Edition was also released in June 2005 by Umbrella Entertainment.

===Box office===
Bad Boy Bubby grossed $808,789 at the box office in Australia.   

==See also==
* Cinema of Australia

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 