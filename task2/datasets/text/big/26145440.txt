Sri Venkateswara Mahatyam
{{Infobox film
| name           = Sri Venkateswara Mahatyam
| image          = Sri Venkateswara Mahatyam.jpg
| image_size     =
| caption        =
| director       = P. Pullaiah
| producer       = P. Pullaiah
| writer         = Acharya Atreya
| narrator       = Savitri S. Varalakshmi P. Suri Babu Santha Kumari Gummadi Venkateswara Rao Chittor V. Nagaiah Ramana Reddy
| music          = Pendyala Nageswara Rao
| cinematography = P. L. Roy
| editing        = K. A. Sriramulu
| studio         =
| distributor    =
| released       = 1960
| runtime        =
| country        = India Telugu
| budget         =
}} Telugu Hindu mythological film produced and directed by P. Pullaiah and starring N. T. Rama Rao. The story is based on the story of Lord Venkateswara (Balaji).

==Plot==
Evil forces dominate the world. The wicked prosper, as the good are made to suffer from the vicious impact of the spirit of the times, the Kaliyuga, presided over by Kali Samkhya#10|Purusha, great rishis hold yagyas under the guidance of sage Kasyapa to rid the world of this wickedness. Sage Bhrigu (Gummadi) went to Lord Maha Vishnu (N.T. Rama Rao) to request his help.  He did not notice the sages arrival while enjoying the blissful company of his spouse Sri Lakshmi (Varalakshmi). Furiously, the sage kicked the Lord on the chest. Vishnu without being offended at all pacified the sage and made him realize how anger makes people lose their senses. Sri Lakshmi, however, could not bear all this. In great anger, She left Vaikuntam for the earth below. In search of her, Sri Vishnu too came down to the earth. He took shelter as Srinivasa in the ashram of Vakulamatha (Santha Kumari). Later, however, Srinivasa met Padmavathi (Savitri), daughter of Akashraja and married her.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Nandamuri Taraka Rama Rao || Srimaha Vishnu / Srinivasa
|- Savitri || Padmavathi
|-
| S. Varalakshmi || Srimaha Lakshmi
|-
| Gummadi Venkateswara Rao || Bhrigu Maharshi
|-
| A. V. Subba Rao || Akasa Raju
|-
| Ramana Reddy || Sarabha
|-
| Surabhi Balasaraswati || Sarabhas wife
|-
| Chittor V. Nagaiah || Bavaji
|-
| P. Santha Kumari || Vakula
|-
| Rushyendramani || Dhaaranidevi
|-
| P. Suri Babu || Narada
|-
| A. V. Subba Rao Jr. || Brahma
|-
| Sandhya || Saraswathi Devi
|-
| Lanka Satyam || Yathrikudu
|-
| Vempati Peda Satyam || Maha Siva
|-
| Peketi Sivaram
|-
| Showkar Janaki || Yerukala Saani
|-
| Ghantasala Venkateswara Rao || Guest role in the song Sesha Saila Vaasa
|-
| R. Nageswara Rao||
|-
| Relangi Venkataramaiah||
|-
| Balakrishna||
|-
| Vangara Venkata Subbaiah||
|}

==Crew==
* Director: P. Pullaiah
* Producers: P. Santha Kumari, P. Pullaiah and V. Venkateswarlu
* Production Company: Padamsri Pictures
* Screen adaptation: Acharya Atreya
* Dialogues: Acharya Atreya
* Music Director: Pendyala Nageshwara Rao
* Cinematography: P. L. Roy
* Choreography : Vempati Peda Satyam
* Editing: K. A. Sriramulu
* Art Direction: S. V. S. Ramarao
* Playback singers: S. Janaki, Jikki Krishnaveni, Pithapuram Nageswara Rao, P. Leela, Ghantasala Venkateswara Rao, Madhavapeddi Satyam, P. Susheela, Swarnalatha and Vaidehi

==Songs==
* Chilako Chikkave Eenadu (Cast: Ramana Reddy and Surabhi)
* Edukondala Swamy
* Evaro Atadevaro Aa Navamohanudevaro (Lyrics: Aathreya; Singers: Ghantasala and P. Susheela)
* Gopaala Nandha Gopaala
* Seshasailavasa Srivenkatesha (Lyrics: Aathreya; Singer: Ghantasala) 
* Sri Devini Needu Deverini Sarisaatileni Sowbhagyavatini (Singer: S. Varalakshmi; Cast: NTR and S. Varalakshmi)
* Varala Beramayya Vanaram Beramayya (Lyrics:  ; Cast: S. Varalakshmi)
* Vegaraara Prabho (Lyrics: Aathreya; Singer: Madhavapeddi Satyam)

==Box office==
The film celebrated Silver Jubilee and ran for 175 days. 

==1936 film==
Sri Venkateswara Mahatmyam, titled as Balaji,  was first made by P. Pullaiah under the Famous Films banner in 1936. C. S. R. Anjaneyulu acted as Balaji and Santha Kumari as Padmavathi in the lead roles. The movie was among the successes of the early days of Pullaiah and Shanthakumaris married life.

==References==
 

==External links==
*  
*  
*  

 
 
 
 