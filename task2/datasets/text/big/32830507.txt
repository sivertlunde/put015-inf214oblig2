Career Girl (1944 film)
{{Infobox film
| name           = Career Girl
| image          = Cargirpos.jpg
| image_size     =
| caption        = Original film poster
| director       = Wallace Fox Jack Schwarz (producer)
| writer         = David Silverstein (story) and Stanley Rauh (story) Sam Neuman (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Gus Peterson
| editing        = Robert O. Crandall
| studio         = Producers Releasing Corporation
| distributor    =
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
 Cover Girl.

== Plot summary ==
Kansas City girl Joan Terry has come to New York to conquer Broadway as thousands have before her.  Advised to maintain an appearance of wealth, she has been living in an expensive hotel until she is discovered.  With no offers coming in she moves to an economical womens boarding house full of equally unsuccessful actresses, singers, and dancers.

However when Joan demonstrates her ability in the traditional newcomers show for the residents, the girls recognise her considerable talent and form a corporation to support her until she is discovered and can pay them back from her earnings.

Joan has a further problem when her impatient fiancee, a Kansas City coal mines owner orders her to return home in failure to become his meek housewife.  When she carries on in her plans he arrives in New York to sabotage her aspiring career.

== Cast ==
*Frances Langford as Joan Terry
*Edward Norris as Steve Dexter
*Iris Adrian as Glenda Benton
*Craig Woods as James Blake Linda Brent as Thelma Mason
*Alec Craig as Theodore "Pop" Billings, the Landlord
*Ariel Heath as Sue Collins
*Lorraine Krueger as Ann
*Gladys Blake as Janie
*Charles Judels as Felix Black Charles Williams as Louis Horton
*Renee Helms as Polly
*Marcy McGuire as Louise

== Soundtrack == Tony Romano)
* Frances Langford - "Some Day" (By Morey Amsterdam and Tony Romano)
* Frances Langford - "Blue in Love Again" (Written by Michael Breen and Sam Neuman)
* Frances Langford - "A Dream Came True" (Written by Michael Breen and Sam Neuman)
* Tap danced to by Lorraine Krueger - "Buck Dance" (traditional stop-time tune for tap dance)

== External links ==
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 