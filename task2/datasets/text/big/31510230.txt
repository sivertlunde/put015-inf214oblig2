Nights of Fire
{{Infobox film
| name           = Nights of Fire
| image          = 
| caption        = 
| director       = Marcel LHerbier
| producer       = Ciné-Alliance
| writer         = Marcel LHerbier T.H. Robert
| starring       = Gaby Morlay Victor Francen
| music          = Jean Wiener
| cinematography = 
| editing        = 
| distributor    = 
| released       = 16 April 1937 (France); 22 September 1940 (USA)
| runtime        = 98 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
}}
 French Drama drama film from 1937, directed by Marcel LHerbier, written by Marcel LHerbier, starring Gaby Morlay.   It is also known as "Nights of Fire". 

The scenario is based on a book by Leo Tolstoy.

== Cast ==
* Gaby Morlay: Lisa Andreieva
* Victor Francen: Fedor Andreiev
* George Rigaud: Serge Rostoff
* Madeleine Robinson: Macha
* Sinoël: the man at the jury court
* Mia Slavenska: the ballerina
* Paule Andral: Lisas mother
* Gabriel Signoret: the substitute Bobinine (as Signoret)
* Jeanne Lory: Misses Bobinine
* Odette Talazac: a gipsy
* André Nox: the president
* René Bergeron: an informer
* Jean Toulout: Balichev
* René Génin:  Balichevs client
* Paulette Burguet: the photographer
* Jean Marais: (uncredited)

== References ==
 

== External links ==
*  
*   at the Films de France
*     sur le site DvdToile
*     sur le site ToutleCine

 

 
 
 
 
 
 
 
 


 