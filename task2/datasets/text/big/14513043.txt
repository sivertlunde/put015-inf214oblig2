Eve of Destruction (film)
{{Infobox film
| name = Eve of Destruction
| image = Eve of destruction.jpg
| image_size =
| caption =
| director = Duncan Gibbins David Madden
| writer = Duncan Gibbins Yale Udoff
| starring = Gregory Hines Renée Soutendijk Kurt Fuller Michael Greene John M. Jackson
| music = Philippe Sarde
| cinematography = Alan Hume
| editing = Caroline Biggerstaff Nelson Entertainment Interscope Communications
| distributor = Orion Pictures
| released = January 18, 1991
| runtime = 99 min.
| country = United States English
| budget = $13 million
| gross = $5,451,119 (US)
| preceded_by =
| followed_by =
}} 1991 science United States Dutch actress Renée Soutendijk (in her first U.S. film) with the dual roles as the cyborgs creator Dr. Eve Simmons, and the cyborg Eve herself.

==Plot==
Eve VIII is a military robot created to look and sound exactly like her creator, Dr. Eve Simmons (Soutendijk). When the robot is damaged during a bank robbery, it accesses memories it was programmed with by her creator. The memories used, though, are the dark and angry ones.

The robot is also programmed as a killing machine if anyone tries to stop her mission. Colonel Jim McQuade (Hines) is tasked with stopping the robot. With the help of Dr. Simmons, he tries to outthink the intelligent and emotional killing machine.

==Reception==
Vincent Canby gave a negative review in The New York Times, calling the film "an undistinguished, barely functional action-melodrama."  As of March 2014, the film has a 10% "rotten" score on RottenTomatoes.com. 

===Box Office===

The movie opened with $2.5 million. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 