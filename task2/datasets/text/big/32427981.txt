Dinty (film)
{{Infobox film
| name           = Dinty
| image          = Dinty.jpg
| caption        = Theatrical release poster
| director       = {{plainlist|*Marshall Neilan (dir)
*John McDermott (dir)}}
| producer       = Marshall Neilan
| studio         = Marshall Neilan Productions
| distributor    = Associated First National Pictures
| screenplay     = Marion Fairfax
| story          = Marshall Neilan
| starring       = Wesley Barry
| released       =  
| country        = United States
| editor         = {{plainlist|*Daniel Gray (film ed)
*Bessie Mason (cutter)}}
| language       = Silent film (English intertitles)
}}
 Marjorie Daw, Pat OMalley and Noah Beery.
 Filmmuseum in Amsterdam. 

==Plot== Kate Price) that her husband has been killed in a car accident.

To support herself and her infant son Dinty, Doreen labors night shifts as a scrub woman until, at the age of twelve, Dinty (Wesley Barry) becomes the familys breadwinner by selling newspapers. Doreen, meanwhile, suffers from tuberculosis and gets weaker by the day.
 Marjorie Daw). Dinty, whose work as a newsboy has familiarized him with the Chinese underworld, leads police to Wong Tais hideout and saves the judges daughter from a bizarre death by torture. As Dintys mother has succumbed to tuberculosis, the grateful Judge Whitely adopts Dinty.

==Cast==
 
 
* Wesley Barry – Dinty OSullivan
* Colleen Moore – Doreen OSullivan
* Tom Gallery – Danny OSullivan
* J. Barney Sherry – Judge Whitely Marjorie Daw – Ruth Whitely Pat OMalley – Jack North
* Noah Beery – Wong Tai
* Walter Chung – Sui Lung Kate Price – Mrs. OToole Tom Wilson – Barry Flynn Aaron Mitchell – Alexander Horatius Jones
* Newton Hall – The tough one
* Young Hipp – Wong Tais son
* Hal Wilson

==Production==
In an earlier film, Go and Get It (1920), Barry played a supporting role as a paperboy named "Dinty". Neilan used the character to create a story in a similar vein as a starring vehicle for Barry, who was being groomed by the studio. 

Moore, on loan from Christie Film Company, would sign a lucrative contract with Neilan when production for Dinty was completed.  Anna May Wong appeared in an uncredited role that also led to more work with Neilan; after Dinty, he created a role for her in Bits of Life for which she earned her first screen credit. 

Portions of the film were shot on location in San Francisco including Chinatown and Adolph B. Spreckels mansion.   The end of the film was shot on location on Catalina Island. 

==References==
{{reflist|refs=
   
   
   
   
   
}}

==External links==
*  
*  
*   at Silent Era website

 

 
 
 
 
 