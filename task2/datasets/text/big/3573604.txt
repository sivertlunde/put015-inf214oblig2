Joseph Smith: The Prophet of the Restoration
{{Infobox Film
 | name           = Joseph Smith: The Prophet of the Restoration
 | image          =
 | caption        =
 | director       = T. C. Christensen  Gary Cook
 | producer       = Ron Munns
 | writer         = Gary Cook
 | starring       = Nathan Mitchell  Dustin Harding  Tayva Patch  Rick Macy
 | music          = Merrill Jenson
 | cinematography = T. C. Christensen
 | editing        = Wynn Hougaard
 | distributor    = The Church of Jesus Christ of Latter-day Saints
 | released       = December, 2005
 | runtime        = 60 min
 | language       = English
 | budget         =
|}}
Joseph Smith: The Prophet of the Restoration is a 2005 film that focuses on some of the events during the life of Joseph Smith, founder of the Latter Day Saint movement, which was both filmed and distributed by The Church of Jesus Christ of Latter-day Saints (LDS Church). The film was shown in the Legacy Theater of the Joseph Smith Memorial Building from its opening on December 17, 2005 until early 2015, and opened in several LDS Church visitors centers on December 24, 2005.

The film used 65 mm film   and is currently  being projected digitally. It also took advantage of the new and developing digital intermediate process.  In March 2011, the church released a revised cut of the film, which is available to watch in select visitors centers and online.   

==Story==
 

 
The film begins on June 21, 1844, on a riverboat named Memphis on the Mississippi River. Mary, a recent Mormon convert, has travelled over 4,000 miles to Nauvoo, Illinois, with her father, who wouldnt let her make the trip by herself.  He asks her if she intends to meet Joseph Smith to know that he is a prophet of God. Mary responds that she already knows that Smith is a prophet of God. She says that her father can know too by reading what he wrote.

The movie then shows Marys father beginning to read an issue of Times and Seasons and the Book of Mormon. Significant parts of the life of Smith, with occasional narration from Smith, are presumably from the issue of Times and Seasons shown. 
 the bone in his left leg was seriously infected. Amputation was avoided by an experimental operation to remove the infected parts of the bone.

Next is the family in 1819 collecting maple sap to make syrup. Two cycles of winter are shown amid scenes of family life, attending revivals, reading the Bible and discussions with the local clergy. Narration relates that the time was early in the spring of 1820. The movie and narration relate a brief version of the First Vision.

Three years later, Smith receives a visitation from the Angel Moroni who tells him where to find the golden plates. After finding them, he relates that he has been told that there will be several years of training and preparation before he can take the plates. Shortly thereafter, Josephs eldest brother, Alvin Smith (brother of Joseph Smith)|Alvin, dies. The film has shown the close relationship that Alvin, hyrum Smith|Hyrum, and Joseph had, so the death of Alvin deeply affects Smith. This issue is later revolved with Smiths 1836 vision at the Kirtland Temple, where he sees Alvin in the celestial kingdom, and the doctrine of salvation for the dead is revealed. 
 Emma Hale, against the objections of her parents.
 James and Church of tarred and feathered.  Emma removes what tar she can, and Smith preaches a sermon the next day.

The next scene is in 1836 in Kirtland, Ohio, where a recent convert, a widower with three children, comes to town but has nowhere to live. Smith organizes a group to build them a house. Smith then reveals that God wants them to build a Kirtland Temple|temple. After it is finished and a meeting has been held, Smith and some others see another vision where God accepts the temple. Other personages then appear and confer additional priesthood keys or authorities on Smith and the others present.
 jail in Liberty, Missouri is the longest jail scene in the film. Smith questions God, "How long shall thy hand be stayed?"  It is made clear during this incarceration that Smith is growing discouraged and having doubts about what he is doing. Smith then hears a voice, "My son, peace be unto thy soul; thine adversity and thine afflictions shall be but a small moment; And then, if thou endure it well, God shall exalt thee on high". 

Smith calls several men to travel and preach the gospel. One of these pairs of men is shown preaching, where they encounter Mary and her father. (This is presumably where the story switches from the story being read to what is happening at the movies present time.)

The setting then shifts to Nauvoo, Illinois, in the 1840s, where the Mormons are building a new city. Smith tells the people that God has told him to build another Nauvoo Temple|temple. After quite a bit of the construction has been completed, Smith and Hyrum are in the basement of the temple discussing going to Carthage, Illinois|Carthage. Smith tells Hyrum that if they go, they will not come back. Smith is shown conferring all of the keys and authorities that he holds on apostle Brigham Young.

On June 24, 1844, Smith, Hyrum, and two others mount their horses and ride through the town on the way out of town. During this review, the music to "Praise to the Man" is played in the background while the people the riders pass seem to show on their faces that this is the last time theyll see Smith alive. Meanwhile, Mary and her father have just arrived at the dock and disembarked. They attempt to follow the riders so that they can meet Smith, but they finally give up when they ride out of town past the temple under construction. Marys father then says, "We didnt need to meet him to know that he is a prophet."

Smith and his associates have reported to the jail in Carthage, as requested. They are shown in an upper room of the building when men with guns begin running up the stairs. The men attempt to force their way into the room, but the occupants hold back the door. A shot is fired through the door which hits Hyrum in the face.  Hyrum says, "I am a dead man." Smith is shown attending to Hyrum when he quickly dies. Smith looks up at one of the windows in the room where the panes of glass are being broken by balls fired from outside. The camera (presumably Smiths view) then moves toward the window, the entire pane is broken and the camera view pans slightly upward to where the sun is filtering through the clouds (in the right of the frame). Smiths image is then shown on the left of the frame looking towards where the sun is filtering through the clouds, with the voice of Joseph saying, "Oh Lord, my God."  Then fades to black.

A picture of the Nauvoo Temple is then shown with three sentences praising the work that Smith has done.

==Release==
The   
 Mesa Arizona, Laie Hawaii, Idaho Falls Los Angeles Hamilton New Oakland California, Washington D.C., London England Independence Missouri Cove Fort, Winter Quarters. 

===DVD===
The original film was released on DVD through the churchs Distribution Services on October 1, 2010, as part of the DVD set "Doctrine and Covenants and Church History Visual Resource DVDs".

===2011 revision===
In March 2011, the church released a new version of the film. The revised cut is expected to help those unfamiliar with Smiths story understand it better than the original did.    The revisions to the film included a new narration given by Smiths mother, Lucy Mack Smith, while 40 percent of the film is new content, and it is also five minutes shorter than the original. Because of the level of new content added, some new filming was required.  The revised film is available online, on the churchs YouTube channel—Mormon Messages—and is available for download on their radio service website, the Mormon Channel. The revised film was shown in the Legacy Theater  and at select church visitors centers through early 2015. There are no plans to release the revised edition on DVD since it is available to download. 

==References==
 

==Further reading==
*  
*  

==External links==
 
*   on YouTube from the Mormon Channel
*   on the Mormon Channel
*  
*  

 

 
 
 
 
 
 