Pakal (film)
{{Infobox film
| name           = Pakal
| image          = Pakal(film).jpg
| caption        = Pakal film poster
| alt            =
| director       = M. A. Nishad
| producer       = Santhosh Mathai   Biju John
| writer         = Susmeth Chandroth Prithviraj  Jyothirmayi
| music          = G. Radhakrishnan
| cinematography = Vipin Mohan
| editing        = P. C. Mohanan
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Prithviraj and  Jyothirmayi in the lead role.

== Plot ==
Pakal takes us into the world of farmers, of the high ranges of Wayanad District of Kerala, who are forced to commit suicide following financial problems. The story has a lot of potential with a relevant political issue and there are some political comments too—like hints at the failure of the government machinery and religious institutions, which ought to come to the aid of the peasants. The film opens with the protagonist Nandakumar (Prithviraj Sukumaran|Prithviraj), who is a reporter of the television channel Kerala Today, being sent to Pannakamkudi  to do a regular series on the plight of the farmers of the area. He alights on the mission with his cameraman Abu (Sudheesh) to the high ranges, delivering daily reports in the prime time news about the crop failure, low prices for agricultural products, inability to repay loans issued by banks and cut-throat interest rates charged by private financiers, which eventually force the peasants to commit mass suicides.

Nandakumar and his crew meet 87-year old Kochuthekkethil Kunjappan (Thilakan) in the verandah of the village office. He is one of the early settlers of the region and has been demonstrating before the village office since 14 years seeking justice from the authorities who seize his farmland, branding him a millionaire. And from the directives of the old man and the local priest Abraham Thekkilakkaadan, they meet Thenginthottathil Joseph (T G Ravi) and his family members, who are in the grip of severe financial problems and on the brink of desperation and even suicide. Josephs eldest daughter Merlin (Sreeja Chandran) works for a private exporting company and his second daughter Celine (Jyothirmayi) is a medical student who is currently pursuing her final year house surgeonship. The family and the villagers pin all their hopes on Celine, who will be the first doctor from the village. Joseph who had to borrow loans from four different banks for his daughters educational expenses finds it hard to settle the accounts.

Slowly the media team becomes a part of the village, and their serialized investigative reports on Kerala Today, start to cause many people in the government circles and the local heartless money lender Ummachan (Jagadish), lose their sleep. The crew faces many hardships in the village as many even manhandle the team.  The media management goes a step further and asks Nandakumar to stop reporting on the issue and to return to the city. But he turns down the request and continues there. But as days pass by, the villagers’ plights become more hapless as many more suicides follow, including that of Merlin and Joseph. What follows in the film is how Nandakumar, with the support of Celine and other villagers, finds ways to rescue the villages from the chaos that they face.

==Cast== Prithviraj
* Jyothirmayi
* Sudheesh
* Thilakan
* Jagathy Sreekumar
* Jagadish
* T. G. Ravi
* Shwetha Menon
* Sreeja Chandran
* Seema G. Nair
* Zeenath
* Kanakalatha
* Maya Viswanath
* Anoop
* Ibrahim Vengara

==External links==
*  
*   at Nowrunning.com
*   at Oneindia.in
*   at Indiaglitz.com

 
 
 
 
 