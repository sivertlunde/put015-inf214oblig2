That's What She Said (film)
{{Infobox film
| name = Thats What She Said
| image = 
| image_size = 
| alt = 
| caption = 
| director = Carrie Preston
| producer = Carrie Preston Tai Burkholder Mona Panchal Lucy Barzun Donnelly
| writer = Kellie Overbey
| starring = Marcia DeBonis Anne Heche Alia Shawkat Miriam Shor
| cinematography = William Klayer
| editing = Anita Brandt-Burgoyne
| studio = Daisy 3 Pictures
| distributor = Phase 4 Films
| released =  
| runtime = 84 minutes
| country = United States
| language = English
| budget = $500,000 
}}
Thats What She Said is a 2012 American comedy film written by Kellie Overbey and directed by Carrie Preston.

==Synopsis==
As Bebe prepares for a big date, her best friend Dee Dee offers nothing but cynical barbs. Meanwhile, sex-crazed Clementine wanders into their lives, sparking a wild adventure through the streets of New York City. 

==Cast==
* Marcia DeBonis as Bebe
* Anne Heche as Dee Dee
* Alia Shawkat as Clementine
* Miriam Shor as Rhoda
* Mindy Siegfried as Mary
* Michael Emerson as uncredited background character 

==Release== premiered at the 2012 Sundance Film Festival on January 20, 2012 and proceeded to get a limited release on October 19, 2012.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 

 