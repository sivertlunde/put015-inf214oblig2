Pilgrim's Progress: Journey to Heaven
{{Infobox Film
| name           = Pilgrims Progress: Journey to Heaven
| image          = Pilgrims Progress- Journey to Heaven.jpg
| image_size     = 
| caption        = 
| director       = Danny Carrales
| producer       = 
| writer         = John Bunyan (novel) Danny Carrales (screenwriter)
| narrator       = 
| starring       = 
| music          = Matt Gates
| cinematography = James Burgess
| editing        = James Burgess
| studio         = DRC Films
| distributor    = 
| released       = 2008
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Pilgrims Progress: Journey to Heaven (or simply Pilgrims Progress) is a 2008 Christian film loosely based on John Bunyan’s classic novel The Pilgrims Progress.  It was written and directed by Danny Carrales, and starred Daniel Kruse as Christian.  The film was featured at the Merrimack Valley Christian Film Festival. 

== Plot ==
Set in the modern day, the main character in the film, Christian (Daniel Kruse) is concerned about the well-being of his family after reading a book which says that the city will be destroyed by fire. It becomes a burden for him, but his family and friends reject the warnings in the book. He begins his journey to The Celestial City where he has been told that he will find safety from the coming destruction and relief from his burden.

== Cast ==
*Daniel Kruse as Christian
*Terry Jernigan as Faithful and Apollyon(voice)
*Jeremiah Guelzo as Hopeful
*Hugh McLean as Evangelist
*Reid Dalton as Giant Despair
*Neal Brasher as Envy

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 


 