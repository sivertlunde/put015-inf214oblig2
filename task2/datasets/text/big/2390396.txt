Hell Ride
 
 
 
{{Infobox film
| name           = Hell Ride
| image          = Hell Ride poster.jpg
| caption        = Promotional poster
| director       = Larry Bishop Michael Steinberg Quentin Tarantino
| writer         = Larry Bishop
| starring       = Larry Bishop Michael Madsen Vinnie Jones David Carradine Dennis Hopper
| music          = Daniele Luppi
| cinematography = Scott Kevan
| editing        = Blake West William Yeh
| studio         = Dimension Films
| distributor    = Dimension Films
| released       =  
| runtime        = 83 min.
| country        = United States
| language       = English
| budget         =
| gross          = $390,128 	
}}
Hell Ride is a 2008 American action film written and directed by Larry Bishop and starring Bishop, Michael Madsen, Dennis Hopper, Eric Balfour, Vinnie Jones, Leonor Varela and David Carradine. It was released under the "Quentin Tarantino Presents" banner. The film is a homage to the outlaw biker films of the sixties and seventies.

== Plot ==
Larry Bishop stars as biker Pistolero, (named after the original title for Robert Rodriguez’s Desperado (film)|Desperado) the leader, or "Pres" of the Victors, a Southern California motorcycle gang.  He has two faithful lieutenants, The Gent (Michael Madsen) and Comanche (Eric Balfour).

In 1976, Cherokee Kisum (Julia Jones), the girlfriend of Pistolero (then known as Johnny) is viciously murdered by The Deuce (David Carradine) and Billy Wings (Vinnie Jones), leaders of the archrival gang the Six-Six-Sixers, as a message to the Victors. The Deuce later moves into large scale business efforts, leaving the biker life behind and the Sixers dry up as a gang.  Cherokee Kisum has also hidden away a small fortune from under-the-table drug deals she made behind the Deuces back - the reason for which she is murdered.  The stash of money is intended for her young son, who disappears after her death.

Years later, after The Deuce returns to the area to close up unfinished business and Billy Wings reforms the Sixers in Los Angeles, the rival gang infiltrates the Victors in an attempt to take over their territory. One member from 1976, St. Louie, is murdered in the same manner as Cherokee Kisum.  Bob the Bum, the Victors treasurer, is similarly killed.  Pistolero then begins to make moves to eliminate the Sixers and finally gain his revenge. While loyal bikers are killed by the Sixers, the more treacherous and less faithful Victors try to influence The Gent, Comanche, and Goody Two-Shoes to switch sides - or kill them. Goody Two-Shoes, the Victors only black member, is eventually killed after being located and chased down by Billy Wings.

With the aid of his beautiful "medicine woman" Nada (Leonor Varela) and his old friend and ally Eddie Zero (Dennis Hopper), Pistolero and the remaining Victors try to locate and kill The Deuce, Billy Wings and the Sixers before they themselves are killed.

== Cast ==
* Larry Bishop ... Pistolero/Johnny
* Michael Madsen ... The Gent
* Dennis Hopper ... Eddie "Scratch" Zero
* Eric Balfour ... Comanche/Bix/Sonny Kisum
* Vinnie Jones ... Billy Wings
* Leonor Varela ... Nada
* Michael Beach ... Goody Two-Shoes
* Laura Cayouette ... Dani
* Julia Jones ... Cherokee Kisum
* Francesco Quinn ... Machete
* Allison McAtee ... The Swede
* Cassandra Hepburn ... Maria
* David Carradine ... The Deuce

== Characters ==
Vinnie Jones and David Carradine star as members of the satanic biker gang The 666ers. Dennis Hopper stars as a veteran member of the Victors gang. Leonor Varela plays the mysterious Nada, who prefers to use double entendres and clichés rather than speaking conversationally.

== Production ==
Bishop took extra duties on this film by not only starring in it, but wrote, directed and co-produced with Michael Steinberg and Shana Stein producing and Quentin Tarantino taking on the job of executive producer. This is Bishops modern-day take on those 1960s motorcycle flicks he used to turn out for B-movie masters American International Pictures. It is the project Tarantino inspired Bishop to begin some five and a half years ago, when he told Bishop: "It is your destiny to write, direct and star in a movie". Tarantino also assured Bishop that he would help to produce his film.

== Release ==
Hell Ride premiered at the 2008 Sundance Film Festival. It had a brief theatrical run via Third Rail Releasing before being released to DVD worldwide.

== External links ==
*  
*  
*   at  
*  
*  
*   at BrokenProjector
*   from AMC

 
 
 
 
 
 
 
 
 
 