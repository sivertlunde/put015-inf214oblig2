Christmas in Wonderland
{{Infobox film
| name           = Christmas in Wonderland 
| image          = Christmas in wonderland post.jpg
| caption        = Theatrical release poster James Orr
| producer       = Laurette Bourassa
| writer         = James Orr Wanda Birdsong Shope Jim Cruickshank
| starring       = Matthew Knight Chris Kattan Cameron Bright Preston Lacy Carmen Electra Tim Curry Patrick Swayze
| music          = Terry Frewer
| cinematography = Duane Manwiller
| editing        = Jana Fritsch
| distributor    = Yari Film Group Freestyle Releasing 
| released       =  
| runtime        = 98 minutes
| country        = Canada United States
| language       = espanol
| budget         = 
| gross          = $772,386 
* 
*  
}}
Christmas in Wonderland is a 2007 comedy film about three children who move with their father from Los Angeles to Edmonton|Edmonton, Alberta, where they catch a group of counterfeiters while Christmas shopping.

==Plot==
The Saunders family has just moved from Los Angeles to Edmonton right before Christmas leaving them with no Christmas spirit with the exception of the bright eyed six-year-old Mary. With their mother, Judy, stuck in L.A., it is up to Wayne, the father, and his kids to go to West Edmonton Mall to fulfill the Christmas shopping duties. Things start looking a bit more like Christmas when twelve-year-old Brian and Mary find a large amount of money and launch on a shopping spree across the mall, until the criminals who counterfeited it begin to chase them through the mall.

==Cast==
*Matthew Knight as Brian Saunders
*Chris Kattan as Leo Cardoza
*Cameron Bright as Danny Saunders
*Preston Lacy as Sheldon Cardoza
*Amy Schlagel and Zoe Schlagel as Mary Saunders
*Carmen Electra as Ginger Peachum Matthew Walker as Santa, Mr. Nicholas, Mall Ghost, Kristopher Kringle, and The Old Man With The Walker
*Tim Curry as Gordon McLoosh
*Patrick Swayze as Wayne Saunders
*MacKenzie Porter as Shane
*Marty Atonini as Elliot Block
*Rachel Hayward as Judy Saunders

==Production==
The film is largely set in and mostly filmed at West Edmonton Mall. Secrecy before it was announced led many Edmontonians to wonder why Christmas decorations were still in place long after the Christmas season had passed.

==Release==
The film was exhibited theatrically in Canada in 2007, but in the United States it premiered on television in 2008 as part of ABC Familys 25 Days of Christmas programming block.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 