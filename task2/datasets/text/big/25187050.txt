Greenberg (film)
{{Infobox film
| name           = Greenberg
| image          = Greenberg poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Noah Baumbach
| producer       =  
| writer         = Noah Baumbach
| story          =  
| starring       =  
| cinematography = Harris Savides
| editing        = Tim Streeto James Murphy
| studio         =  
| distributor    = Focus Features
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $25,000,000  
| gross          = $6,153,967
}}
Greenberg is a 2010 American comedy-drama film written and directed by Noah Baumbach. The film stars Ben Stiller, Greta Gerwig, Rhys Ifans and Jennifer Jason Leigh. Greenberg was produced by Focus Features and Scott Rudin Productions, and distributed by Focus Features.
 soundtrack features James Murphy.

==Plot==

Florence Marr (Greta Gerwig) walks the Greenberg family dog, Mahler. She picks up Phillip Greenbergs dry cleaning and heads back to his house, where the entire family is packing for a trip to Vietnam. Phillip (Chris Messina) explains that his brother, Roger, will be staying in the house while they are away, and he asks Florence to help Roger if he needs anything. His wife, Carol (Susan Traylor), confides that Roger has just been released from a hospital after suffering a nervous breakdown. 
Roger is a carpenter and has been hired by Phillip to build Mahler a dog house. It is implied throughout the film that Roger actually does little work, and Carol is sceptical about him actually finishing the project.

When Roger (Ben Stiller) arrives, he calls Florence to ask about the people swimming in the pool. Florence explains that they are neighbors who are allowed to use the pool. She comes to the house to pick up her paycheck and feed Mahler. She has a very brief and awkward conversation with Roger, before offering to pick up groceries for him. He gives her a two-item list, requesting whiskey and ice cream sandwiches.

Rogers friend Ivan Schrank (Rhys Ifans) stops by and invites him to a barbecue at the home of their mutual friend Eric Beller (Mark Duplass). At the party, Roger is uncomfortable, and Eric is visibly hostile towards him. He runs into Beth (Jennifer Jason Leigh), an ex-girlfriend, and she agrees to meet up with him for some drinks. He explains that he is in Los Angeles to simply do nothing for a while. She replies that it is brave for a man in his mid-forties to have no ambition.

Meanwhile, he calls Florence to meet up for a drink. Since as a New Yorker, he does not drive, she picks him up. They stop at her apartment to pick up her purse, which she forgot. Roger comes on to her, and they begin to have sex. Having just come out of a long relationship, Florence stops Roger, not wanting to have meaningless sex.

Eric and Roger have dinner, where Eric vents his anger over the fact that Roger declined a major label recording contract that their band was offered fifteen years ago. Eric marvels over the fact that Ivan will even talk to Roger anymore, given how devastated Ivan was by losing the contract. Roger insists that conforming to the commercial contract would have been impossible for the band.

One day, Roger notices that Mahler is lethargic. He calls Florence to take them to a vet, where they learn that the dog has an auto-immune disease. Roger did not want to get involved with Florence, but as they encounter each other during Mahlers treatment, they keep escalating their relationship, with Florence falling hard for Roger.

When Roger meets up with Beth for drinks, it is her turn to be uncomfortable. She barely remembers their relationship; whereas, he remembers the most minute details from their time together. Roger muses, to Beths astonishment, that they would have probably gotten married and had kids. When Roger asks her out on a date, she says it would be a terrible idea and abruptly asks for the check.

After Florence and Roger finally have sex, they end up in an argument, where he yells at her for always coming back to him, despite the fact that he does not want to get involved. The next day, Roger remorsefully calls her, and Florence is getting drunk in her apartment alone. She confesses that she is due to have an abortion the next day. Roger convinces her to let him take her to the clinic. Since he does not drive, Ivan has to drive Roger and Florence to the clinic. She undergoes  general anaesthesia and stays in the clinic overnight.

Back at the house, Rogers college-age niece, Sara (Brie Larson) has turned up. She is heading to Australia the next morning with her friend, Muriel (Juno Temple), and they throw a house party with dozens of their friends. Roger does drugs with the kids. Ivan shows up and gets into an argument with Roger, where his hurt feelings over the record contract come up. Roger confesses that he had no idea that his personal concerns about the contract would put a stop to the entire deal, and he admits to feeling an immense burden of guilt over it. Both Ivan and Roger bemoan the fact that they have ended up in lives that they did not plan to have; however, Ivan has made peace with his. Before leaving he tells Roger that he knew  all along that he had instituted something Roger never brought up. Having had a similar experience he laments he could have helped him and in disgust says that they never talk about anything good or meaningful.
Dejected, Roger ends up calling Florences phone and leaving her a long voice mail where he confesses that he really likes her.

The next day, Rogers niece invites him to Australia, and he jumps at the invitation. He convinces the neighbors in the pool to take care of Mahler, but on the way to the airport, he changes his mind. Instead, he goes to pick up Florence at the hospital, and they return to her apartment. The film closes as she listens to Rogers voice mail.

==Cast==
* Ben Stiller as Roger Greenberg
* Greta Gerwig as Florence Marr
* Rhys Ifans as Ivan Schrank
* Jennifer Jason Leigh as Beth
* Merritt Wever as Gina Chris Messina as Phillip Greenberg, Rogers brother
* Brie Larson as Sara
* Juno Temple as Muriel
* Mark Duplass as Eric Beller
* Dave Franco as Rich
* Jake Paltrow as Johno

==Production==
Ben Stiller appeared in the staged documentary Im Still Here (2010 film)|Im Still Here in which he approaches Joaquin Phoenix about playing the role of Ivan.

==Soundtrack==
  ]]
 James Murphy.  It is Murphys debut film score, and it includes original compositions credited to Murphy,  his band LCD Soundsystem as well as songs by other artists.  The movie itself contained 25 unique songs, leaving 8 out of the soundtrack. 

; Track listing
  James Murphy: "People"
# Nite Jewel: "Suburbia"
# James Murphy: "Sleepy Baby"
# James Murphy: "Thumbs"
#  "
# James Murphy: "Plenty of Time"
# James Murphy: "Photographs"
# James Murphy: "Gente"
# Galaxie 500: "Strange"
# LCD Soundsystem: "Oh You (Christmas Blues)"
# James Murphy: "Birthday Song"
# James Murphy: "Dear You"
# The Sonics: "Shot Down"
#  "
# James Murphy: "If You Need a Friend"
# James Murphy : "Please Dont Follow Me"
# James Murphy: "Photographs (Piano)"

==Reception==

===Critical response===
Greenberg garnered generally positive critical reception. Review aggregation website Rotten Tomatoes gives the film a score of 75% based on 161 reviews; the film has been "Certified Fresh" with the sites consensus: "Greenbergs title character is harder to like than most, but Ben Stillers nuanced performance and a darkly funny script help take the misanthropic edge off."    
Another review aggregator, Metacritic, assigned the film a weighted average score of 75/100, based on 38 reviews from mainstream critics.   

The film was given a 3 star rating (out of 4 stars) by Peter Travers of Rolling Stone magazine. Travers writes, "Writer-director Noah Baumbach (The Squid and the Whale) walks the fragile line between humor and heartbreak...Even when you laugh, like in the climactic party scene, it hurts."  Roger Ebert gave the film 3 and 1/2 stars (out of four).   

Not all reviews were positive.   was perhaps the most harsh critic of the film: "This is the kind of low-budget movie that attracts respectful reviews, but tiny audiences. Thats because theres virtually no story or character development and the main characters an idiot." 

===Accolades=== Berlin International Film Festival
* Golden Bear Award for Best Film (nominated)
 Gotham Awards
* Best Breakthrough Actor/Actress – Greta Gerwig (nominated)
 Independent Spirit Awards
* Best Feature (nominated)
* Best Male Lead – Ben Stiller (nominated)
* Best Female Lead – Greta Gerwig (nominated)
* Best Cinematography – Harris Savides (nominated)
 National Board of Review Awards
* Top Ten Independent Films

==References==
 

==External links==
 
*  
*  
*  
*  
*  
* Greenberg – full list of the movies music – http://www.extrageographic.org/greenberg-a-full-list-of-all-of-the-music-in-the-film

 
 

 
 
 
 
 
 
 