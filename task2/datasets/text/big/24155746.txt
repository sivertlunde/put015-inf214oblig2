East of Piccadilly
{{Infobox film
| title = East of Piccadilly
| image = Eastofpicpos.jpg
| caption = Original movie poster
| director = Harold Huth
| writer = Gordon Beckles (novel)  J. Lee Thompson Lesley Storm Sebastian Shaw  Niall MacGinnis
| producer = Walter C. Mycroft
| cinematography = Claude Friese-Greene
| music = Marr Mackie
| distributor = Associated British Picture Corporation
| released = 5 July 1941
| runtime = 75 m
| country = United Kingdom
| language = 
| budget =
| gross = 
| awards =
}}   
 British mystery Sebastian Shaw, Henry Edwards, Martita Hunt, Charles Victor and Frederick Piper.  

==Plot==
A series of murders in the West End of London baffle the officers of Scotland Yard and draw the interest of a crime reporter to the case.

==Cast==
*Judy Campbell  ...  Penny Sutton   Sebastian Shaw  ...  Tamsie Green  
*Niall MacGinnis  ...  Joe   Henry Edwards  ...  Inspector  
* George Pughe  ...  Oscar Kuloff  
* Martita Hunt  ...  Ma  
* George Hayes  ...  Mark Struberg   Cameron Hall  ...  George  
* Edana Romney  ...  Sadie Jones  
* Bunty Payne  ...  Tania  
* Charles Victor  ...  Editor  
* Frederick Piper  ...  Ginger Harris
* Bill Fraser ...  Maxie

==References==
 

 
 
 
 
 
 
 
 
 



 
 