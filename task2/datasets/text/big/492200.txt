S.O.B. (film)
{{Infobox film
|  name           = S.O.B.
|  image          = SOBPoster1981.jpg
|  caption        = Theatrical release poster
|  writer         = Blake Edwards Robert Preston Larry Hagman Robert Webber Tony Adams  Blake Edwards
|  studio  = Lorimar Productions
|  director       = Blake Edwards
|  distributor       = Paramount Pictures (theatrical) Warner Bros. (current)
|  music         = Henry Mancini
|  cinematography = Harry Stradling Jr.
|  editing = Ralph E. Winters
|  released   =  
|  runtime        = 122 minutes
|  country      = United States
|  language = English
|  gross         = $14,867,086
}}
 Robert Preston, Larry Hagman, Robert Vaughn, Robert Webber, Loretta Swit, Shelley Winters and, in his last movie appearance, William Holden. 

S.O.B. was produced by Lorimar and originally released in the United States by Paramount Pictures on July 1, 1981.

==Plot== producer who has just made the first major flop of his career, to the dismay of his movie studio, resulting in the loss of his own sanity. Felix attempts suicide four times:

* He attempts to die of carbon monoxide poisoning in his car, only to have it slip into gear and drive through the side of his garage, down a sand dune and into the Pacific Ocean.

* He attempts to hang himself from a rafter in an upstairs bedroom, only to fall through the floor, landing on a poisonous Hollywood gossip columnist standing in the living room below.

* He tries to gas himself in his kitchen oven, but is prevented from carrying out his intent by two house guests with other things on their mind. Thereafter he spends most of the time heavily sedated while his friends and hangers-on occupy his beach house.  The occupation leads to a party which degenerates into an orgy.

* Finally, he tries to shoot himself with a police officers gun but is prevented from doing so by the ministrations of a young woman wearing only a pair of panties.  The experience gives him a brainstorm that the reason for his films failure was its lack of sex.

Felix resolves to save both the film and his reputation. With great difficulty he persuades the studio and his wife Sally Miles, an Academy Award|Oscar-winning movie star with a goody-goody image, to allow him to revise the film into a soft-core pornographic musical in which she must appear Toplessness|topless.  He liquidates most of his wealth to buy the existing footage and to bankroll further production.  If he fails, both he and Sally will be impoverished, at least by Hollywood standards.

At first the studios executives are keen to unload the film onto Felix and move on, but when Sally goes through with the topless scene and the film seems a likely success, they plot to regain control. Using Californias community property laws, they get the distribution and final-cut rights by persuading Sally to sign them over. A deranged Felix tries to steal the movie negatives from the studios color lab vault, armed with a water pistol. He is shot and killed by police who think the gun is real.

Felixs untimely death creates a crisis for his cronies Culley, Coogan and Dr. Finegarten, who plan to give him a burial at sea. They kidnap his corpse, substituting the body of a well-known but underrated character actor who died in the first scene of the movie. Felix gets a Viking funeral in a burning dinghy, while the other actor finally gets the Hollywood sendoff many thought he deserved.

The epilogue later reveals that Felixs revamped film was a huge success, and Sally won another Academy Award for her performance.

===The movie within the movie===
Little is seen of the movie which is the focus of the plot, except for an extended dream sequence and a brief shot close to the end. The title is Night Wind, which provokes the headline "Critics Break Wind" seen on a copy of Variety (magazine)|Variety at the start of S.O.B. after the initial flop. The plot of Night Wind is kept vague; it involves a frigid businesswoman (played by Andrews character) whose inability to love a "male chauvinist" rival executive stems from a childhood trauma that led to her sexual detachment.

The climax of Night Wind is the first scene of S.O.B., an elaborate song and dance sequence set to "Polly Wolly Doodle", in which Julie Andrews wanders through a room full of giant toys (several of which come to life), singing the song while dressed as a tomboy. The implication is that her fathers death caused Andrewss character to renounce childhood and become a cold, frigid person.

A second scene, taking place at the end of the film, has Andrews character arrive at the home of her would-be lover after the dream, where he reveals that he still loves her, "despite everything."
 male chauvinist to being a secret cross dresser. Felix axes the entire song sequence, turning it from a dream to a hallucination "… caused by a powerful aphrodisiac put into her Bosco Chocolate Syrup|Bosco" and replacing the regular version of "Polly Wolly Doodle" with a more haunting version. He has the "toys" dress in more erotic outfits, and includes a carnival barker-type muscle man (portrayed by S.O.B.s choreographer, Paddy Stone), who tempts Andrews character before she rejects him, by way of flashing her breasts.

==Cast==
* Julie Andrews as Sally Miles, the star of Night Wind and the wife of Felix Farmer
* William Holden as Tim Culley, the director of Night Wind and Felixs friend
* Richard Mulligan as Felix Farmer, the producer of Night Wind and the husband of Sally Miles Robert Preston as Dr. Irving Finegarten, Felix and Sallys doctor
* Robert Webber as Ben Coogan, Sallys press agent
* Robert Vaughn as David Blackman, the president of Capitol Studios
* Larry Hagman as Dick Benson, a Capitol Studios executive
* Stuart Margolin as Gary Murdock, Sallys personal secretary and an aspiring producer
* Loretta Swit as Polly Reed, a Hollywood gossip columnist Craig Stevens as Willard, Polly Reeds husband
* Marisa Berenson as Mavis, the actress-girlfriend of David Blackman
* Jennifer Edwards as Lila, a young hitchhiker picked up by Culley
* Rosanna Arquette as Babs, Lilas friend, also picked up by Culley
* Shelley Winters as Eva Brown, Sallys agent
* Robert Loggia as Herb Maskowitz, Sallys lawyer John Lawlor as the Capitol Studios Manager
* John Pleshette as the Capitol Studios Vice-President
* Larry Storch as Swami, Sally Miles spiritual guru, who officiates at the funeral. Paul Stewart as Harry Sandler, Felixs agent
* Ken Swofford as Harold P. Harrigan, a studio security guard
* Hamilton Camp as Lipschitz, an executive at Capitol Color Lab, where the negative of Night Wind is stored. David Young as Sam Marshall, a popular actor
* Benson Fong as Felix and Sallys personal chef
* Charles Lampkin as Felix and Sallys butler
* Joe Penny as Officer Buchwald, a police officer called to the Farmer residence
* Byron Kane as the Funeral Director
* Virginia Gregg as the Funeral Directors Wife
* Erica Yohn as Agnes, the costume designer of Night Wind

==Title==
"S.O.B." (in the film) stands for "Standard Operational Bullshit" and refers to misinformation being the norm.  The acronym also means "sexually oriented business" (if pertaining to strip clubs) and more generally "son of a bitch" (a ruthless person).

A Spanish dub of the film keeps the acronym S.O.B., claiming that it stands for "Sois hOnrados Bandidos" (You Are Honest Crooks). The Argentine title for the movie was changed to Se acabó el mundo (The World is Ended), having no relation to the original title.

Three years later, when Edwards had his name removed from the writing credits of 1984s City Heat, he was billed under the pseudonym Sam O. Brown. (S.O.B.)

==Influences==
When writing the screenplay, Edwards drew upon several of his own experiences. The character of Felix Farmer is a person not unlike Edwards, while actress Sally Miles bears certain similarities to real-life wife Julie Andrews (who plays her).

The story of S.O.B. parallels the experiences of Edwards and Andrews in their infamous, but Academy Award-nominated, failure, Darling Lili. Intended to reveal Andrews heretofore unseen wicked and sexy side, that film had a troubled shoot, went significantly over budget, and was subjected to post-production studio interference. The early 1970s had more bad news for Edwards; he made two more movies, Wild Rovers, a western with William Holden, and The Carey Treatment with James Coburn. Once again, studio interference occurred and both films were edited without Edwards input. Each opened to negative reviews and poor business. Hit hard by these events, Edwards went to Europe to work and to shake off studio interference. The plan worked, leading to successful projects including three "Pink Panther" sequels starring Peter Sellers.

In S.O.B., Andrewss character agrees (with some pharmaceutical persuasion) to "show my boobies" in a scene in the film-within-the-film.  Rooney, Andy. Public Affairs, 2003,  The Sound of Music opening sequence.

==Reception==
Critical opinion was sharply divided.  In a remarkable contradiction, the screenplay was nominated both for a Writers Guild of America Award for Best Comedy Written Directly for the Screen and a Razzie Award for Worst Screenplay.  It was also nominated for a Razzie for Worst Director and a Golden Globe Award for Best Motion Picture - Comedy/Musical.

==Versions==
Several scenes were shot twice, one version for movie theaters and one for television.  The party/orgy was shot for television with no nudity, and the erotic dream sequence was shot in a much milder version than the cinema version.  The most remarkable difference involves a scene where Robert Vaughn, as studio head David Blackman, receives a phone call while in bed with his mistress.  In the television version he is simply seen naked from the waist up.  In the cinema version he gets out of bed wearing a bustier, nylon stockings and other transvestite paraphernalia.

==Home video==
The original video release was made by CBS Video Enterprises in 1982, on both VHS and CED Videodisc, and was later reissued on VHS by CBS/Fox Video in the mid-1980s. Warner Bros. bought ancillary rights in 1989 with their purchase of Lorimar, and the film was released on Laserdisc through Warner Home Video in 1990. Warners released a DVD edition in 2002.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 