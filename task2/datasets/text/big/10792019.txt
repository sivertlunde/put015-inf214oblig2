Sivappu Sooriyan
{{Infobox film
| name           = Sivappu Sooriyan
| image          =
| image_size     =
| caption        =
| director       = Muktha Srinivasan
| producer       = Muktha V. Ramaswamy
| writer         = R. Selvakumar (dialogues)
| story          = R. Selvakumar
| screenplay     =  Radha Silk Smitha
| music          = M. S. Viswanathan
| cinematography = M. Karnan
| editing        = V. P. Krishnan R. Shanmugam
| studio         = Muktha Films
| distributor    = Muktha Films
| released       =  
| runtime        = 139min.
| country        = India Tamil
}}

Sivappu Sooriyan is a Tamil film is directed by V.Srinivasan.  The movie was a box office failure.

==Cast==
 
*Rajnikanth
*Saritha Radha
*Thengai Srinivasan
*Venniradai Moorthy
*Y. G. Mahendran Manorama
*Silk Smitha
*S. R. Sivakami
*V. Gopalakrishnan
*Delhi Ganesh
*Sivachandran
*R. N. Sudarshan in Guest appearance
*Sangili Murugan
*Radha Ravi
*Kathadi Ramamurthy
*Usilimani
*S. Ramarao
*S. V. Shanmugam Pillai
*Ennatha Kannaiya
*Typist Gopu
*Oru Viral Krishna Rao
*M. R. Sulochana
 

==Soundtrack==
The music was composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali || 04.23
|- Vaali || 03.58
|- Vaali || 04.10
|- Janaki || Vaali || 03.58
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 


 