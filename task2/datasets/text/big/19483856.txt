Gaudi Afternoon
{{Infobox film
| name           = Gaudi Afternoon
| image          = Gaudi Afternoon (film).jpg
| alt            = 
| caption        = 
| director       = Susan Seidelman
| producer       = 
| writer         = 
| based on       =  
| starring       = Judy Davis Marcia Gay Harden Lili Taylor Juliette Lewis Christopher Bowen Courtney Jines
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Gaudi Afternoon is a 2001  , Juliette Lewis, Christopher Bowen and Courtney Jines. 

Barbara Wilsons novel is the winner of a British Crime Writers Award for Best Mystery Based in Europe and a Lambda Literary Award.  The story is a high-spirited comic adventure that works issues of sexual politics into a madcap plot. The city of Barcelona is a lively party to the film and books action.

"Gaudi Afternoon" was the Opening Night screening at the 25th San Francisco International Gay and Lesbian Film Festival.  It was selected as the Closing Night film for the New York LGBT Film Festival, the Western and Southern Australian Gay and Lesbian Film Festival, the Miami International Gay and Lesbian Film Festival and the Sydney Women of the World International Film Festival.  It also played at Outfest, Seattle International Film Festival and the Mar Del Plata International Film Festival, among many other venues.

In 2002, the film had a theatrical release in Europe, South Africa, Australia and Japan.  After a short run in New York City, it played on TV and DVD in the US.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 