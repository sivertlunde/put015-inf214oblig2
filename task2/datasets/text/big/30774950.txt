Zeynep's Eight Days
{{Infobox film
| name           = Zeyneps Eight Days
| image          = ZeynepsEightDaysTheatricalPoster.jpg
| alt            =
| caption        = Theatrical Poster
| director       = Cemal Şan
| producer       = Tekin Doğan
| writer         = Cemal Şan
| starring       =   
| music          = Baba Zula
| cinematography = Sarp Kaya
| editing        = Şenol Şentürk
| studio         = Avşar Film
| distributor    = UIP
| released       =  
| runtime        = 123 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
}}

Zeyneps Eight Days ( ) is a 2007 Turkish drama film, written and directed by Cemal Şan, starring Fadik Sevin Atasoy as an isolated woman searching for a stranger she fell in love with at a party. The film, which went on nationwide general release across Turkey on  , was shown in competition at the 44th Antalya Golden Orange Film Festival (October 19–28, 2007). It is the first part of a trilogy of films which includes Dilbers Eight Days (2008) and Alis Eight Days (2009).

==External links==
*  

 
 
 
 
 


 
 