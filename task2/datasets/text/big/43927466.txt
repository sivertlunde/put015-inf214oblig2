23 Blast
{{Infobox film
| name           = 23 Blast
| image          = 23 Blast Poster with MPAA rating.pdf
| caption        =
| director       = Dylan Baker
| producer       =   Misook Doolittle Brent Ryan Green
| writer         = Bram Hoover Toni Hoover
| Based on         = Real-life story of Travis Freeman  Alexa PenaVega Max Adler Kim Zimmer Bram Hoover Becky Ann Baker Dylan Baker Timothy Busfield Fred Thompson
| music          = John Carta
| cinematography = Jay Silver
| editing        = Matt Mayer
| studio         = Touchdown Productions, LLC Toy Gun Films
| distributor    = Ocean Avenue Entertainment 
| released       =   
| runtime        = 98 minutes 
| country        = United States
| language       = English
| budget         = $1 million
| gross          = $549,185   
}}

23 Blast is a 2013 American   teen who loses his sight, but eventually overcomes the challenges of his disability, and continues to live his dream of playing football.   at CBS WKYT; by Rebecca Smith; published October 23, 2014; retrieved October 29, 2014  Travis is portrayed by Mark Hapka in the film.   at USA Today; by Sarah Gearhart; published October 23, 2014; retrieved October 29, 2014 

The film was produced by Touchdown Productions, LLC and   and Becky Ann Baker also starred in the film.

==Plot== depression as mobility coach, center and helps the team turn around their losing season advancing to the state playoffs.  

==Cast==
  in his directorial debut for 23 Blast.]]
* Mark Hapka as Travis, a high school football star suddenly stricken with irreversible blindness.
* Bram Hoover as Jerry Baker, Travis’ best friend and teammate who struggles with alcohol abuse.  Alexa PenaVega as Ashley, Travis childhood friend and high school love interest after he goes blind.  Max Adler as Cameron, who plays Center on the Corbin Redhounds. Cameron rejects the distraction of a blind player, but ends up helping Travis fit in and prosper on the team. 
* Stephen Lang as Coach Farris, Head Coach of the Corbin Redhounds.
* Becky Ann Baker as Patty Wheatley, Travis’ mobility coach.
* Dylan Baker as Larry Freeman, Travis’ Father.
* Kim Zimmer as Mary Freeman, Travis’ Mother.
* Timothy Busfield as Jasper A. Duncan, Corbin High School’s Athletics Director.
* Fred Thompson as Coach Powers, Former Coach of the Corbin Redhounds who picks Coach Farris as his replacement.
* Scott Sowers as Mr. Marshall, Cameron’s Father.
* Isiah Whitlock, Jr. as Dr. Connelly, Travis’ surgeon.
* Crystal Hunt as Molly, Travis’ love interest before he became blind.
* Caleb VanNorstran as young Jerry Baker.
* Frank Russell - As Freddy the backup Quarterback 

 

Director Dylan Baker used his personal network in casting the film. He called several friends to play crucial roles. Stephen Lang was known for playing mean or unlikable people, and was Baker’s first choice to play the coach with the heart of gold, Willard Farris. Originally Bram Hoover was slated to play Travis, but Baker asked him to consider the role of Jerry; when Bram worked on Jerry, he agreed it was the better role for him and he made the switch.  Baker asked wife, Becky Ann Baker, to play redhead, Patty Wheatley, Travis’ mobility coach. Baker had worked with Fred Dalton Thompson, Timothy Busfield and Kevin Cooney, and they all said yes.  Baker needed to audition young actors for the other roles, finding Mark Hapka, Alexa PenaVega and Max Adler on one day in Los Angeles. 

Kim Zimmer was originally slated to play the role of Molly’s mother, which did not survive the final cut of the film. When Baker had trouble finding the right actress to play Travis’ mom, he thought of Kim and asked her to consider the role.  With the real Mary and Larry Freeman frequently on the set, Baker and Zimmer enjoyed their many scenes as “movie - man and wife”.  When Baker added a scene of Travis with his parents in church, he went right to the real Travis Freeman to play the part.  Baker had heard one of Travis’ sermons that convinced him that Travis could play the role.

==Production==

===Development===
23 Blast was inspired by the life of Travis Freeman, a Corbin, Kentucky native. Growing up, Travis always wanted to be a football player for the Corbin Redhounds, his local high school football team. At the age of 9, he was the water boy for the Redhounds and played football during middle school. At the age of 12, Travis began having severe headaches.  His parents took him to a Lexington, Kentucky hospital, where he was diagnosed with viral meningitis, a serious, but not deadly infection.  Travis was given antibiotics and sent home. Soon after, Travis’ eye began to swell because of infection. Travis was rushed to the hospital as the infection began to spread and his head began to swell. Because of the very rare sinus infection, Travis was rushed into surgery. Travis survived, but emerged from surgery blind. 

Travis’ parents vowed to do everything they could to keep him from having to leave Corbin and move to Lexington to attend the school for the blind. Travis adjusted and soon began to live life normally in his home. Being a middle schooler, Travis still had a passion for football. He and his parents approached Coach Willard Farris and asked if Travis could have a support role on the football team. He said “no”, and said that he could play.  Coach Farris strategically assigned Travis the Center position. Just a year after his illness, he played his first game of organized football. When Travis entered high school his coach, Coach Mike Whittaker, assigned him as the third string Center for the Corbin Redhounds. He played all four years of high school and ended his football career in high school.  Travis attended the University of Kentucky, where he was offered a position on the teams support staff. In 1999, the High School Athletic Hall of Fame presented Travis with the first-ever Travis Freeman Award in recognition of his accomplishments. 

===Pre-production===
Toni Hoover approached Dylan Baker’s wife, Becky Ann Baker, about a script she had written for 23 Blast. In the beginning, Dylan and Becky only intended to act in the film, but soon Dylan began to help Toni update the script and cast the project. Eventually, Toni approached Dylan about directing the project and he agreed.   Dylan approached Gary Donatelli,  who had a rich history in television direction and football production, having shot for Monday Night Football for many years.  The three decided to produce the film themselves, and after adding Line Producer, Carrie Holt de Lama, they traveled to Corbin to begin production.

"23 Blast" is the name of a football play, in which the quarterback hands off to the running back, who attempts to go through a hole in the defensive line.    In the 1967 UCLA vs. USC football game, "23 Blast" was the audible run play for O. J. Simpsons 64 yard game-winning touchdown.

===Filming===
23 Blast began shooting on April 2, 2012.  The project was penned under the original title of Sight Unseen.  When the three producers went out for dinner with friends, before they began shooting, a hat collected everyone’s ideas for a title. The table agreed that 23 Blast was the best choice and the name stuck.  The cast filmed on location in Corbin, Kentucky for a little under a month, shooting for 23 days.   Scenes were filmed in the actual places that Travis lived and played football, on the practice field, the stadium, an indoor facility and Corbin High School. Filming was completed one day ahead of schedule.

Because Corbin played its games at night, the producers had to ask all the local football players who portrayed both sides of a game to stay all night, sometimes until early hours of the morning.  Several extras went to the local school, the University of the Cumberlands.  One player, speaking for the group, asked Baker to allow them to leave one morning at 5:30 AM so they would have time to travel to the school and start weight training at 6 AM.  These players never complained, and there were many local extras who came back, again and again, regardless of how cold Corbin got in mid-April at dawn.  When a scene for Travis and his parents at Sunday services was added, the Freemans asked their church, Central Baptist Church, for permission to film there. Many members of the congregation came out to watch Travis’ film debut.

==Themes==
23 Blast is at its heart a slice-of-life story.  As young boys, Travis and Jerry meet on the football field. They are testing themselves against others, and find in their on-the-field talents a bond that extends beyond the game.  When the complexity of adolescence disrupts their lives and jeopardizes that friendship, both boys are forced to grow up.  Life isn’t fair to either of them: Travis loses his vision and Jerry loses his place on the team without his friend to guide and help him.  This theme of identity is visited often in 23 Blast. 
  Rudy and Remember the Titans.  Coach Farris doesn’t have all the answers, but he trusts his own mind and his heart.  When elements of the town question his judgment, he sticks to his decisions to be inclusive instead of exclusive, to care more about the development of his boys as people than as football players. There is also the theme of the whole operating better than its individual parts, just as a team plays much better when it works together against a common foe. 
 
23 Blast deals with sight and blindness on many fronts with a theme of "vision comes from within".   Travis blindly accepts his life and celebrity as a star football player, but when he loses his sight, he discovers vision on a whole new level. The real Travis Freeman has said repeatedly that he believes he will see again, and the film recognizes that in the story of his character. Once the movie ends, Travis realizes that as a blind person he is still the same person he was with sight, he then sees clearly what his purpose is in life and understands what he wants to achieve. The film uses Travis overcoming his fear of failing on the football field as a metaphor for any person who won’t let their disadvantages or bad luck keep them down. The film tries to show that it is not the fall that builds character, but it’s getting up again and again to give it another try.  This was a theme that permeated the production in front of and behind the lens. A line from the film, “How’s that gonna work?” resonated many times, from pre-production in a small town, to the release in 617 theaters nationwide. The story of 23 Blast behind the scenes is a story of overcoming obstacles and odds, persevering and choosing faith over fear. 

==Reception==
23 Blast debuted at the Heartland Film Festival in 2013. It was the winner of the 2013 Heartland Film Festivals Audience choice Award for Narrative Feature.  In 2014, 23 Blast was featured at several private screenings throughout the United States in front of test audiences. 23 Blast released in select theaters on October 23, 2014, and nationwide on October 24, 2014.
 The Huckabee Show, Governor Mike Huckabee said, “If you only see one film between now and the end of the year, go see 23 BLAST.” 
 average score of 4.9/10. The sites consensus reads "The real life story that inspired 23 Blast is undeniably heartwarming — but the movie itself is too predictable and amateurish to recommend."  Metacritic, another review aggregator, assigned the film a weighted average score of 45 out of 100, based on 10 critics, considered to be "mixed or average reviews". 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 