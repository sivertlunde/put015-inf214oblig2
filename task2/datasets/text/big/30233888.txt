Almighty Thor
{{Infobox film
| name           = Almighty Thor
| image          = Almightythor.jpg
| caption        = DVD cover
| director       = Christopher Ray
| producer       =  
| writer         = Erik Estenberg
| starring       =  
| music          = Chris Ridenhour
| cinematography = Alexander Yellen
| editing        = Benjamin Lee Cooper Ron Santiano
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $200,000
| preceded by    =
}}
Almighty Thor is a  , the film follows the young, legendary warrior Thor in his battle against Loki.

== Plot ==
When the demon god Loki (Richard Grieco) wipes off the city of Valhalla to steal the Hammer of Invincibility, only the young hero Thor (Cody Deal) can recover the cities from evil. When Thors father and older brother are killed in a futile attempt to retrieve the hammer from Loki, a Valkyrie named Jarnsaxa (Patricia Velásquez) attempts to train a naïve and inexperienced warrior Thor to fight Loki. This leads them on a short quest from their training camp, to the Tree of Inventory  to collect a sword and shield and then to a small city where Loki attempts to hypnotize the refusing residents into serving as his minions by bringing on a  wipeout with a small army of demon beasts. When Thor is about to be defeated, he must forge his own fate to save the city and reclaim the Hammer of invincibility from Loki once and for all.

==Cast==
*Cody Deal as Thor
*Richard Grieco as Loki
*Patricia Velásquez as Járnsaxa
*Kevin Nash as Odin Baldir
*Chris Himdall
*Rodney Wilson as Hrothgar
*Kristen Kerr as Herja
*Charlie Glackin as Hundig
*Nicole Fox as the Redhead Norn
*Leslea Fisher as the Blonde Norn
*Lauren Halperin as the Brunette Norn
*William Webb as a street punk Tree of Life Guardian

==Release==
Almighty Thor premiered on May 7, 2011, near the time of release of Marvel Studios Thor (film)|Thor,

==Reviews==
Almighty Thor received largely negative reviews from critics.
Reviewing the film for the The A.V. Club, Phil Dyess-Nugent gave "Almighty Thor" a rating
of "D-", taking issue with the films low budget: 
"The film is so underpopulated that most of the awful deaths Loki inflicts go down off-camera; he points his stick or gives a command to his dogs, and then you hear somebody holler, "Argghhhh!!"".
Dyess-Nugent also criticised the acting of the leads and took issue with the
producers decision to shoot the LA scenes in abandoned parking lots: "The comic high point is a fight between Thor and Loki, with the guys spinning around and waving their weapons at each other while keeping one eye peeled for cops who might demand 
to see their filming permit." Dyess-Nugent, Phil.(May 7, 2011). 
" . The A.V. Club. 
The Onion. Retrieved April 29, 2012. 
The Blueprint website review of the film stated, "This brain numbing 80 minutes of constant noise, cheap effects, background music that never once stops and ropey acting will test the patience of even the most hardened B-movie aficionado...Almighty Thor was just one giant headache of a film." Skeates, Andrew, (July 22, 2011), .
Blueprint. Retrieved April 29, 2012. 

==References==
 

==External links==
*  at The Asylum
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 