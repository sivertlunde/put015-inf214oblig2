Chocolate and Soldiers
 
{{Infobox film
| name           = Chocolate and Soldiers
| image          = Chocolate and Soldiers publicity sheet 1938.jpg
| image_size     = 250px
| caption        = Original publicity sheet, 1938
| director       = Sato Takeshi
| producer       = Teppei Himuro
| writer         = Akiko Ishikawa Masaru Kobayashi Noriko Suzuki
| starring       = Kamatari Fujiwara Kiyoshi Hosoi Yuko Ichinose
| music          = Noboru Itô
| cinematography = Kyoji Yoshino
| editing        = 
| distributor    =  
| released       = 30 November 1938 
| runtime        =
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}

  is a 1938 Japanese war film directed by Sato Takeshi. It shows the common Japanese soldier as an individual and as a family man, presenting even enemy Chinese soldiers as brave individuals. It is considered to be a "humanist" film, paying close attention to human feelings both of the soldier and his family.   

==Plot==

A hard-working father is called up and sent to the front. He keeps in touch with his family by writing letters home. These include chocolate wrappers collected from his comrades; his son is collecting the wrappers to redeem for a free box of chocolate. The man volunteers for one brave act, joining a suicide squad. Before he goes, he drinks a toast from a cup given him by his son, and (as happens in other "humanist" films) smiles to indicate his intention to die with his comrades. The son receives the news of his fathers death at the same time as the free chocolates arrive. He swears vengeance; the chocolate company gives him a scholarship. 

==Reception==

The American director Frank Capra said of Chocolate and Soldiers "We cant beat this kind of thing. We make a film like that maybe once in a decade. We havent got the actors."    The film opened the Japan Societys series of Japanese Second World War movies shown in the United States in 1987. 

Cinema theorist Kate Taylor-Jones suggests that along with films like Mud and Soldiers and The Legend of Tank Commander Nishizumi, Chocolate and Soldiers provided "a vision of the noble, obedient and honourable Japanese army fighting to defend the emperor and Japan."   

Joseph L. Anderson observes that the film was "obviously aimed at the home audience".   

==References==
 

==External links==
*  


 
 