GoldenEye
 
 
 
 
{{Infobox film
| name = GoldenEye
| image = GoldenEye - UK cinema poster.jpg Terry ONeill, Keith Hamshere and George Whitear
| starring = {{ubl|
Pierce Brosnan
*Sean Bean
*Izabella Scorupco
*Famke Janssen
*Joe Don Baker}}
| screenplay = {{ubl|
Michael France
*Jeffrey Caine
*Kevin Wade
*Bruce Feirstein}}
| story = Michael France
| based on =  
| director = Martin Campbell
| producer = {{ubl|
Michael G. Wilson
*Barbara Broccoli}}
| cinematography = Phil Méheux
| editing = Terry Rawlings
| music = {{ubl|
Éric Serra
*Bono and The Edge}}
| studio = Eon Productions
| distributor = United Artists|MGM/UA Distribution Company United International Pictures (UK)
| released =  
| runtime = 130 minutes
| country = United Kingdom
| language = English
| budget = $58 million
| gross = $352,194,034
}} MI6 officer James Bond. The film was directed by Martin Campbell and is the first film in the series not to take story elements from the works of novelist Ian Fleming.  The story was conceived and written by Michael France, with later collaboration by other writers. In the film, Bond fights to prevent an arms syndicate from using the GoldenEye satellite weapon against London in order to cause a global financial meltdown.
 M was Robert Brown. GoldenEye was the first Bond film made after the dissolution of the Soviet Union and the end of the Cold War, which provided a background for the plot.

The film accumulated a worldwide gross of US$350.7 million, considerably better than Daltons films, without taking inflation into account.    Some critics viewed the film as a modernisation of the series, and felt Brosnan was a definite improvement over his predecessor.        The film also received award nominations for "Best Achievement in Special Effects" and "Best Sound" from the British Academy of Film and Television Arts.   
 OSS to his estate in Oracabessa, Jamaica.

==Plot==
  James Bond—agent Soviet chemical weapons facility at Arkhangelsk and plant explosive charges. Trevelyan is apparently shot and killed by Colonel Arkady Ourumov, but Bond steals a Pilatus PC-6 Porter and flees from the facility as it explodes.
 crime syndicate, who has formed a suspicious relationship with a Royal Canadian Navy admiral. She murders the admiral to allow Janus to steal his identity. The next day they steal a prototype Eurocopter Tiger helicopter that can withstand an electromagnetic pulse. They fly it to a bunker in Severnaya  in Siberia, where they massacre the staff and steal the control disk for the dual GoldenEye satellite weapons. They program one of the GoldenEye satellites to destroy the complex with an electromagnetic pulse, and escape with programmer Boris Grishenko. Natalya Simonova, the lone survivor, contacts Boris and arranges to meet him in St. Petersburg, where he betrays her to Janus.
 M assigns Cossack clans Nazi forces involvement in his parents deaths. Just as Bond is about to shoot Trevelyan, Bond is shot with a tranquiliser dart, knocking him out.
 Russian Minister of Defence Dimitri Mishkin interrogates them. As Simonova reveals the existence of a second satellite and Ourumovs involvement in the massacre at Severnaya, Ourumov bursts into the room and kills Mishkin. As Ourumov calls for his guards, Bond escapes into the archives with Simonova, where a firefight ensues. Simonova is captured and is dragged into a car by Ourumov. Bond steals a tank  and pursues Ourumov through St. Petersburg to Janus armoured train, where he kills Ourumov as Trevelyan escapes and locks Bond in the train with Simonova. As the trains self-destruct countdown begins, Bond cuts through the floor with his laser watch while Simonova locates Grishenkos satellite dish in Cuba. The two escape just before the train explodes.

Bond and Simonova, now lovers, meet Jack Wade and trade Bonds car for Wades aeroplane. While flying over a Cuban jungle in search of the satellite dish controlling the satellite, Bond and Simonova are shot down. As they stumble out of the wreckage, Onatopp rappels down from a helicopter and attacks Bond. After a struggle, Bond shoots down the helicopter, which snares Onatopp and crushes her to death. Bond and Simonova then watch a lake being drained of water, uncovering the satellite dish. They infiltrate the control station, where Bond is captured. Trevelyan reveals his plan to rob the Bank of England before erasing all of its financial records with the remaining GoldenEye, concealing the theft and destroying Britains economy.

Meanwhile, Simonova programs the satellite to initiate atmospheric re-entry and destroy itself. As Trevelyan captures Simonova and orders Grishenko to save the satellite, Bond triggers an explosion with his pen grenade and escapes to the antenna cradle. Bond sabotages the antenna by jamming the gears, preventing Grishenko from regaining control of the satellite, before turning and fighting Trevelyan. The two end up on the antenna platform,   above the dish. Bond kicks Trevelyan off the side of the platform, but reflexively grabs him by the foot. After a brief and personal exchange, Bond lets go of Trevelyan and he falls to the bottom of the radio dish. The cradle blows up, falling and crushing Trevelyan and Grishenko—the latter of whom is frozen by −200°C liquid nitrogen. Meanwhile, Simonova commandeers a helicopter and flees with Bond, and the couple is then rescued by Wade and a team of United States Marine Corps Force Reconnaissance|Marines.

==Cast== James Bond (007): An MI6 officer assigned to stop the Janus crime syndicate from acquiring "GoldenEye," a clandestine satellite weapon designed and launched by the Soviets during the Cold War.
* Sean Bean as Alec Trevelyan (006) / Janus: Initially another 00 officer and Bonds friend, he fakes his death at Arkhangelsk and then establishes the Janus crime syndicate in the following nine years.
* Izabella Scorupco as Natalya Simonova: The only survivor and eyewitness of the attack of GoldenEye on its own control centre at Severnaya. A skilled programmer, she helps Bond in his mission.
*   lust murderer and Trevelyans henchwoman. A Sadomasochism|sadist, she enjoys torturing her enemies between her thighs.
* Joe Don Baker as Jack Wade: A veteran CIA officer on the same mission as Bond.
* Judi Dench as M (James Bond)|M: The head of MI6.
*  , secretly an agent of Janus who nefariously misuses his authority and position for helping Janus gain access to the GoldenEye.
*   and ex-KGB officer through whom Bond arranges a meeting with Janus (Trevelyan).
* Alan Cumming as Boris Grishenko: A computer programmer at Severnaya secretly affiliated to Janus. Russian Defence Minister Dmitri Mishkin
*   division of the British Secret Service). Llewelyn and Joe Don Baker were the only actors to reprise a role from a previous Bond film, Baker appearing in The Living Daylights and Llewelyn appearing as Q throughout the series until his death.
* Samantha Bond as Miss Moneypenny: Ms secretary. Samantha Bond made her first of four appearances as Moneypenny.
* Minnie Driver as Irina: A Russian nightclub singer and mistress of Valentin Dmitrovich Zukovsky.
* Serena Gordon as Caroline, an MI6 psychological and psychiatric evaluator whom Bond seduces.

==Production==
===Prelude=== James Bond Carlton Hotel John Glen, responsible for the previous five installments of the series. Broccoli listed among the possible directors John Landis, Ted Kotcheff, and John Byrum.  Broccolis stepson Michael G. Wilson contributed a script, and Wiseguy co-producer Alfonse Ruggiero Jr. was hired to rewrite.   Production was set to start in 1990 in Hong Kong for a release in late 1991. 

Dalton would declare in a 2010 interview that the script was ready and "we were talking directors" before the project entered development hell caused by legal problems between Metro-Goldwyn-Mayer, parent company of the series distributor United Artists, and Broccolis Danjaq, owners of the Bond film rights.    In 1990, as MGM/UA was purchased by French-Italian broadcasting group Pathé, Pathé CEO Giancarlo Parretti intended to sell off the distribution rights of the studios catalog so he could collect advance payments to finance the buyout. This included international broadcasting rights to the 007 library at cut-rate prices, leading Danjaq to sue,  alleging the licensing violated the Bond distribution agreements the company made with United Artists in 1962, while negating Danjaq a share of the profits.  The lawsuits were only settled in 1992, and during these litigation delays, Daltons deal with Danjaq expired in 1990. {{cite web|url=
https://variety.com/1994/film/news/dalton-bails-out-as-bond-120067/ |title=Dalton bails out as Bond|author=Cox, Dan|date=April 12, 1994 |work=Variety}} 

===Pre-production and writing===
In May 1993, MGM announced a seventeenth James Bond film was back in the works, to be based on a screenplay by Michael France.  With Broccolis health deteriorating (he died seven months after the release of GoldenEye), his daughter Barbara Broccoli described him as taking "a bit of a back seat" in films production.    Barbara and Michael G. Wilson took the lead roles in production while Albert Broccoli oversaw the production of GoldenEye as a consulting producer, credited as "presenter".  

In an interview in 1993, Dalton said that Michael France was writing the screenplay, due to be completed in January or February 1994.  Despite Frances screenplay being completed by that January, in April 1994 Dalton officially resigned from the role.  

After Michael France delivered the original screenplay, Jeffrey Caine was brought in to rewrite it.  Caine kept many of Frances ideas but added the prologue prior to the credits. Kevin Wade polished the script and Bruce Feirstein added the finishing touches.  In the film, the writing credit was shared by Caine and Feirstein, while France was credited with only the story, an arrangement he felt was unfair, particularly as he believed the additions made were not an improvement on his original version.    Wade did not receive an official credit, but was acknowledged in the naming of Jack Wade, the CIA character he created.

To replace Dalton, the producers cast Pierce Brosnan, who had been prevented from succeeding Roger Moore in 1986 because of his contract to star in the Remington Steele television series. Before negotiating with Brosnan, Mel Gibson and Liam Neeson passed on the role. The actors relatively low $1.2 million salary also allowed the producers to spend properly the $60 million budget lent by MGM.    Judi Dench was cast as M, making GoldenEye the first film of the series featuring a female M. The decision is widely believed to be inspired by Stella Rimington becoming head of MI5 in 1992.   John Woo was approached as the director, and turned down the opportunity, but said he was honoured by the offer.  The producers then chose New Zealander Martin Campbell as the director. Brosnan later described Campbell as "warrior-like in his take on the piece" and that "there was a huge passion there on both our parts". 
 Reflections in a Golden Eye  and Operation Goldeneye, a contingency plan Fleming himself developed during World War II in case of a Nazi invasion through Spain.  

Although only six years since the release of Licence to Kill, world politics had changed dramatically in the interim. With GoldenEye the first James Bond film to be produced since the fall of the Berlin Wall and the collapse of the Soviet Union, the Cold War era which Bond was known to inhabit was brought to an end, and therefore it was doubtful whether the character was still relevant in the modern world.  Much of the film industry felt that it would be "futile" for the Bond series to make a comeback, and that it was best left as "an icon of the past".  The producers even thought of new concepts for the series, such as a period piece set in the 1960s, a female 007, or a Black James Bond. Ultimately, they chose to return to the basics of the series, not following the sensitive and caring Bond of the Dalton films or the political correctness that started to permeate the decade.  However, when released, the film was viewed as a successful revitalisation, and it effectively adapted the series for the 1990s.  One of GoldenEyes innovations was the casting of a female M. In the film, the new M quickly establishes her authority, remarking that Bond is a "sexist, misogynist dinosaur" and a "relic of the Cold War". This is an early indication that Bond is portrayed as far less tempestuous than Timothy Daltons Bond from 1989. 

===Filming=== new studio.    The producers later said Pinewood would have been too small. 
 MI6 headquarters were used for external views of Ms office.  Some of the scenes in St. Petersburg were actually shot in London – the Epsom Downs Racecourse doubled as the airport – to reduce expenses and security concerns, as the second unit sent to Russia required bodyguards.   
 FS La Ministry of Defence over Brosnans opposition to French nuclear weapons testing and his involvement with Greenpeace; as a result, the French premiere of the film was cancelled. 
 BR Mk 1 coaches, all three heavily disguised to resemble a Soviet armoured train.  

===Effects===
 .]]

GoldenEye was the last film of special effects supervisor Derek Meddings, to whom the film was dedicated. Meddings major contribution was miniatures.  It was also the first Bond film to use computer generated imagery. Among the model effects are most external shots of Severnaya, the scene where Janus train crashes into the tank, and the lake which hides the satellite dish, since the producers could not find a round lake in Puerto Rico. The climax in the satellite dish used scenes in Arecibo, a model built by Meddings team and scenes shot with stuntmen in England. 

Stunt car coordinator Rémy Julienne described the car chase between the Aston Martin DB5 and the Ferrari F355 as between "a perfectly shaped, old and vulnerable vehicle and a racecar." The stunt had to be meticulously planned as the cars are vastly different. Nails had to be attached to the F355 tyres to make it skid, and during one take of the sliding vehicles, both cars collided.  The largest stunt sequence in the film was the tank chase, which took around six weeks to film, partly on location in St. Petersburg and partly at Leavesden.  A Russian T-54/55 tank, on loan from the East England Military Museum, was modified with the addition of fake explosive reactive armour panels.  In order to avoid destroying the pavement on the city streets of St. Petersburg, the steel off-road tracks of the T-54/55 were replaced with the rubber-shoed tracks from a British Chieftain tank.  The T-55 Tank used in the film is now on permanent display at Old Buckenham Airport where the East England Military Museum is based. 
 From Russia with Love. Pierce Brosnan and Sean Bean did all the stunts themselves, except for one take where one is thrown against the wall. Brosnan injured his hand while filming the extending ladder sequence, making producers delay his scenes and film the ones in Severnaya earlier. 

The opening   bungee jump at Archangel, shot at the Verzasca Dam in Switzerland and performed by Wayne Michaels, was voted the best movie stunt of all time in a 2002 Sky Movies poll, and set a record for the highest bungee jump off a fixed structure.   The ending of the pre-credits sequence with Bond jumping after the aeroplane features Jacques Zoo Malnuit riding the motorcycle to the edge and jumping, and B.J. Worth diving after the plane – which was a working aircraft, with Worth adding that part of the difficulty of the stunt was the kerosene flying on his face. 
 fall of Communist parties protested against "Socialist symbols being destroyed not by governments, but by bikini-clad women", especially the Communist Party of India, which threatened to boycott the film. 

===Product placement===
 
GoldenEye was the first film bound by BMWs three picture deal,  so the producers were offered BMWs latest Roadster (automobile)|roadster, the BMW Z3. It was featured in the film months before its release, and a limited edition "007 model" sold out within a day of being available to order. As part of the cars marketing strategy, several Z3s were used to drive journalists from a complimentary meal at the Rainbow Room restaurant to GoldenEyes premiere at Radio City Music Hall. 
 convertible Z3 Stinger missiles behind the headlights. The Z3 does not have much screen time and none of the gadgets are used, which Martin Campbell attributed to the deal with BMW coming in the last stages of production.  The Z3s appearance in GoldenEye is thought to be the most successful promotion through product placement in 1995.  Ten years later, The Hollywood Reporter listed it as one of the most successful product placements in recent years. The article quoted Mary Lou Galician, head of media analysis and criticism at Arizona State Universitys Walter Cronkite School of Journalism and Mass Communication, as saying that the news coverage of Bonds switch from Aston Martin to BMW "generated hundreds of millions of dollars of media exposure for the movie and all of its marketing partners." 

In addition, all computers in the film were provided by IBM, and in some scenes (such as the pen grenade scene towards the end), the OS/2 Warp splash screen can be seen on computer monitors.
 Omega Seamaster Quartz Professional watch features as a major plot device several times in the film. It is shown to contain a remote detonator and a laser. This was the first time James Bond was shown to be wearing a watch by Omega, and the character has since worn Omega watches in every subsequent production.  

While chasing Ourumovs car in a battle tank, Bond smashes a truck carrying bottles of Perrier spring water, with the logo prominently displayed. A brand endorsement, it can also be seen as a symbol of capitalism and foreign business in post-Socialist Russia.

===Music===
 
The theme song, "GoldenEye (song)|GoldenEye", was written by Bono and The Edge, and was performed by Tina Turner.  As the producers did not collaborate with Bono or The Edge, the film score did not incorporate any of the theme songs melodies, as was the case in previous James Bond films.  Swedish group Ace of Base had also written a proposed theme song, but label Arista Records pulled the band out of the project fearing the negative impact in case the film flopped. The song was then re-written as their single "The Juvenile". 
 Filmtracks said Serra "failed completely in his attempt to tie Goldeneye to the franchises past."  The end credits song, Serras "The Experience of Love", was based on a short cue Serra had originally written for Luc Bessons Léon (film)|Léon one year earlier.
 John Altman provided the music for the tank chase in St. Petersburg. Serras original track for that sequence can still be found on the soundtrack as "A Pleasant Drive in St. Petersburg".  Serra composed and performed a number of synthesiser tracks, including the version of the James Bond Theme that plays during the gun barrel sequence,  while Altman and David Arch provided the more traditional symphonic music. 

==Release and reception== Prince Charles, followed on 22 November at the Odeon Leicester Square, with general release two days later.  Brosnan boycotted the French premiere to support Greenpeaces protest against the French nuclear testing program, causing the premiere to be abrogated. 

The film earned over $26 million during its opening across 2,667 cinemas in the USA. Its worldwide sales were around the equivalent of $350 million.    It had the fourth highest worldwide gross of all films in 1995  and was the most successful Bond film since Moonraker (film)|Moonraker, taking inflation into account. 
 MPAA and a 12 rating from the British Board of Film Classification|BBFC. The cuts included the visible bullet impact to Trevelyans head when he is shot in the prologue, several additional deaths during the sequence in which Onatopp guns down the workers at the Severnaya station, more explicit footage and violent behaviour in the Admirals death, extra footage of Onatopps death, and Bond giving her a rabbit punch in the car.  In 2006, the film was re-mastered and re-edited for the James Bond Ultimate Edition DVD in which the BBFC cuts were restored, causing the rating to be changed to 15. However, the original MPAA edits still remain. 

===Reviews===
The critical reception of the film was mostly positive. Film review collection website Rotten Tomatoes holds it at an 82% approval rating,    while a similar site, Metacritic, holds it at 65%.    In the Chicago Sun-Times, Roger Ebert gave the film 3 stars out of 4, and said Brosnans Bond was "somehow more sensitive, more vulnerable, more psychologically complete" than the previous ones, also commenting on Bonds "loss of innocence" since previous films.  James Berardinelli described Brosnan as "a decided improvement over his immediate predecessor" with a "flair for wit to go along with his natural charm", but added that "fully one-quarter of Goldeneye is momentum-killing padding." 
 misogynist dinosaur", The Spy Who Loved Me. Jose Arroyo of Sight & Sound considered the greatest success of the film was in modernising the series. 

GoldenEye was also ranked high in Bond-related lists. IGN chose it as the fifth-best movie,  while Entertainment Weekly ranked it eighth,  and Norman Wilner of MSN as ninth.  ET also voted Xenia Onatopp as the sixth-most memorable Bond Girl,  while IGN ranked Natalya as seventh in a similar list. 

However, the film received several negative reviews. Richard Schickel of Time (magazine)|Time wrote that after "a third of a centurys hard use", Bonds conventions survived on "wobbly knees",  while in Entertainment Weekly, Owen Gleiberman thought the series had "entered a near-terminal state of exhaustion."  Dragan Antulov said that GoldenEye had a predictable series of scenes,  and Kenneth Turan of the Los Angeles Times said that the film was "a middle-aged entity anxious to appear trendy at all costs".  David Eimer of Premiere (magazine)|Premiere wrote that "the trademark humour is in short supply" and that "Goldeneye isnt classic Bond by any stretch of the imagination."    Madeleine Williams said that "there are plenty of stunts and explosions to take your mind off the plot." 

===Awards=== BMI Film Award for the soundtrack and the film also earned nominations for Best Action Film and Actor at the Saturn Awards and Best Fight Scene at the MTV Movie Awards.   

==Appearances in other media== 
 
 John Gardner. The book closely follows the films storyline, but Gardner added a violent sequence prior to the opening bungee jump in which Bond kills a group of Russian guards, a change that the video game GoldenEye 007 retained. 

In late 1995, Topps Comics began publishing a three-issue comic book adaptation of GoldenEye. The script was adapted by Don McGregor with art by Rick Magyar. The first issue carried a January 1996 cover date.  For unknown reasons, Topps cancelled the entire adaptation after the first issue had been published, and to date the adaptation has never been released in its entirety. 
 GoldenEye 007, Rare (known Computer and shooters of all time.    It is based upon the film, but many of the missions were extended or modified.   

GoldenEye 007 was modified into a racing game intended to be released for the  , the first game of the James Bond series in which the player does not take on the role of Bond. Instead, the protagonist is an aspiring Double-0 agent Jonathan Hunter, known by his codename "GoldenEye (character)|GoldenEye" recruited by a villain of the Bond universe, Auric Goldfinger.  Except for the appearance of Xenia Onatopp, the game was unrelated to the film, and was released to mediocre reviews.    It was excoriated by several critics including Eric Qualls for using the name "GoldenEye" as an attempt to ride on the success of Rares game.  
 remake of the original GoldenEye 007 game at their E3 press conference on 15 June 2010. The game is a modernised retelling of the original movies story, with Daniel Craig playing the role of Bond. Bruce Feirstein returned to write a modernized version of the script, while Nicole Scherzinger covered Tina Turners theme song. The game was developed by Eurocom and published by Activision for the Wii and Nintendo DS and was released in November 2010. Both Wii and DS versions bear little to no resemblance to the locations and weapons of the original N64 release. In 2011 the game was ported to PlayStation 3 and Xbox 360 under the name GoldenEye 007: Reloaded. 

==See also==
 
* Electromagnetic pulse in fiction and popular culture
* Outline of James Bond
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 