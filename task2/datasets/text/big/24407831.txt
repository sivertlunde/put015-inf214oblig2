Barefoot Gen: Explosion of Tears
 
{{Infobox film name = Barefoot Gen kanji = はだしのゲン 涙の爆発 romaji = Hadashi no Gen Namida no Bakuhatsu}} image = Barefoot Gen 1977 DVD cover.jpg caption = DVD cover director = Tengo Yamada producer = Tengo Yamada Hisako Yamada writer = Keiji Nakazawa starring = Hiroshi Tanaka Mariko Miyagi Kazuhide Haruta Ikumi Ueno Fumiko Abe Shoji Ishibashi Etsuko Ichihara Keiko Takeshita Keishi Takamine cinematography = Setsuo Kobayashi music = Taku Izumi editing =  studio =  distributor = Tengo Yamada released =   runtime = 123 minutes language = Japanese country = Japan budget = 
}}
  is the live-action film of Barefoot Gen Part2  in 1977.

==Plot== the Plot of the main article of Barefoot Gen

==Cast==
* Kazuhide Haruta as "Gen Nakaoka", Barefoot Gen, the protagonist of the story. Hiroshi Tanaka as "Daikichi Nakaoka", Gens father.
* Mariko Miyagi as "Kimie Nakaoka", Gens mother.
* Chizuko Iwahara as "Eiko Nakaoka", Gens elder sister.
* Fumiko Abe as "Tomoko Nakaoka", Gens youngest sister.
* Ikumi Ueno as "Ryuta"
* Shoji Ishibashi as "Seiji Yoshida"
* Etsuko Ichihara as "Kiyo Hayashi"
* Keiko Takeshita as "Mikiko"
* Keishi Takamine as "Denjiro Samejima"

==See also==
* Barefoot Gen
* Barefoot Gen (1976 film) - PART1 (1976)
* Barefoot Gen (1977 film) - PART2 (1977)
* Barefoot Gen (1980 film) - PART3 (1980)
* Barefoot Gen (anime) - PART1 (1983)
* Barefoot Gen 2 (anime) - PART2 (1986)
* Barefoot Gen (TV drama) - (2007)
* Grave of the Fireflies

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 

 