The Blue Veil (1942 film)
{{Infobox film
| name =   The Blue Veil 
| image =
| image_size =
| caption =
| director = Jean Stelli
| producer = Raymond Artus 
| writer =  François Campaux    
| narrator =
| starring = Gaby Morlay   Elvire Popesco   André Alerme   Fernand Charpin
| music = Alfred Desenclos   André Theurer 
| cinematography = René Gaveau   Marcel Grignon
| editing = Michelle David 
| studio =    Compagnie Générale Cinématographique  
| distributor = Consortium du Film
| released =   18 November 1942 
| runtime = 81 minutes
| country = France French
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} remade in 1951.

==Cast==
*    Gaby Morlay as Louise Jarraud 
* Elvire Popesco as Mona Lorenza 
* André Alerme as Ernest Volnar-Bussel  
* Fernand Charpin as Émile Perrette 
* Aimé Clariond as Le juge dinstruction 
* Pierre Larquey as Antoine Lancelot 
* Marcelle Géniat as Madame Breuilly 
* Georges Grey as Gérard Volnar-Bussel 
* Jeanne Fusier-Gir as Mademoiselle Eugénie 
* Renée Devillers as Madame Forneret 
* Denise Grey as Madame Volnar-Bussel 
* Francine Bessy as La jeune danseuse 
* Jean Clarieux as Henri Forneret 
* Pierre Jourdan as Dominique 
* Camille Bert as Le médecin 
* Noël Roquevert as Linspecteur Duval
* Marcel Vallée as Limpressario de Mona Lorenza 
* Jean Bobillot as Julien 
* Mona Dol as Linfirmière-chef 
* Camille Guérini as dAubigny 
* Marthe Mellot as Une commère 
* Primerose Perret as Georgette Volnar-Bucel 
* Michel de Bonnay as Pierre Breuilly  
* Monique Bourdette as Une petite fille  
* Pierre Brulé as Un petit garçon  
* Jean Fay as Limpressario étranger 
* Raymonde La Fontan as Lotte Lorenza 
* Odette Barencey as Une commère  
* Paul Barge 
* Liliane Bert  Paul Demange as Pons   Michel François
* Marcelle Monthil as Une commère
* Noëlle Norman as Madame Berger, la directrice du bureau de placement 
* Jacques Ory as Un jeune garçon  
* Julienne Paroli as La concierge

== References ==
 

== Bibliography ==
* Dayna Oscherwitz & MaryEllen Higgins. The A to Z of French Cinema. Scarecrow Press, 2009.

== External links ==
* 

 
 
 
 
 
 
 
 

 

 