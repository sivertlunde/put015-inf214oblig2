Scandal (2012 film)
{{Infobox film
| name           = Scandal
| image          =
| caption        = 
| director       = Victor Vu
| producer       = Pham Viet Anh Khoa Tran Thi Bich Ngoc
| writer         = Victor Vu Hong Phuc Cao Tan Loc
| screenplay     = 
| story          = 
| narrator       = Van Trang
| based on       =  
| starring       = Van Trang Maya Minh Thuan Khuong Ngoc Duong Hoang Anh Lan Phuong
| music          = Christopher Wong
| cinematography = Nguyen KLinh
| editing        = Victor Vu Thang Vu
| studio         = PS Vietnam Kantana Post Vietcom
| distributor    = Galaxy Studio Saiga Films
| released       =  
| runtime        = 90 minutes
| country        = Vietnam
| language       = Vietnamese
| budget         = 500,000 USD
| gross          = 1,500,000 USD (Vietnam)
}} Vietnamese horror thriller film directed by Victor Vu,  produced by Galaxy Studio and Saiga Films, in association with PS Vietnam, Kantana Post and Vietcom. Scandal was released in Vietnam on October 12, 2012, immediately becoming a critical and box office success. 

== Plot ==
Two actresses compete with each other for roles, allowing ambition and personal jealousy to influence their judgment, resulting in terrifying consequences.

== Cast ==
* Van Trang as Y Linh
* Maya as Tra My
* Minh Thuan as Thien
* Khuong Ngoc as Le Hung
* Lan Phuong as Huong
* Duong Hoang Anh as Hoang Kiet
* Duc Thinh as Dai
* Quyen Loc as Vinh
* Jayvee Mai The Hiep as Tra Mys manager
* Duong Khac Linh as Musician Duong Khac Linh
* Hong Sap as Witch doctor
* Quach Huu Loc as Journalist
* Huu Tien as Theatre director

== References ==
 

== External links ==
*   at the Internet Movie Database
*  
*   
*  
*  
*  

 

 
 
 
 
 
 
 
 


 
 