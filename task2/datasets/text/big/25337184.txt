Afterschool
 After School}}
 
{{Infobox film
| name           = After school
| image          = Afterschool.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Antonio Campos
| producer       = Sean Durkin Josh Mond
| writer         = Antonio Campos
| starring       = Ezra Miller Addison Timlin Jeremy Allen White Michael Stuhlbarg
| music          = Jody Lee Lipes
| cinematography = Antonio Campos
| editing        = Randi Glass Susan Shopmaker
| studio         = BorderLine Films
| distributor    = IFC Films
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Antonio Campos. Filmed at the Pomfret School in Pomfret, Connecticut, Afterschool premiered at the 2008 Cannes Film Festival in the program Un Certain Regard. The film gained an Independent Spirit Award and Gotham Award nomination for Campos and won the Jury Prize for experimental narrative film at the Nashville Film Festival. 

==Plot==
While doing a film project at a private school, internet-video obsessed teenager Robert ( ), are assigned to make a memorial video. The school is not happy with the result and has it re-edited, to make a smoother version.

While making the video, Robert and Amy begin a romantic relationship, wherein they both have sex for the first time in a wooded area. However, it is later hinted that Amy and Robs roommate may be involved romantically, as well. He fights with his roommate, who sold the drugs to the twins, and shouts that he killed the girls. The school questions him about this accusation, and is relieved that Robert says it was not substantiated. Robert is asked to take a leave of absence from the school. Toward the end of the film, we are finally shown the scene of the girls deaths from the front and see Robert pressing his hand over her mouth and nose, smothering her. Later, Robert is shown at the school nurse, taking a daily dose of pills, showing that Robert is now on medication. The film ends as an unseen person with a cell phone videos him while he looks at two pictures of the deceased twins.

==Cast==
* Ezra Miller as Robert
* Addison Timlin as Amy
* Jeremy Allen White as Dave
* Michael Stuhlbarg as Mr. Burke
* Emory Cohen as Trevor
* David Costabile as Mr. Anderson
* Rosemarie DeWitt as Ms. Vogel
* Dariusz Uczkowski as Peter
* Gary Wilmes as Mr. Virgil
* Lee Wilkof as Mr. Wiseman
* Paul Sparks as Detective Eclisse

==Reception==
The film currently holds a 78% Fresh rating on review aggregate website Rotten Tomatoes, with the consensus "Antonio Campos Afterschool is an intelligent, ambitious debut that boasts strong performances and plenty of ideas." 

==References==
 

==External links==
*  
*  
*   at  

 
 
 
 
 
 
 