Under Burning Skies
 
{{Infobox film
| name           = Under Burning Skies
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Stanner E. V. Taylor
| starring       = Blanche Sweet Wilfred Lucas
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent drama film directed by D. W. Griffith and starring Blanche Sweet.   

==Cast==
* Wilfred Lucas - Joe
* Blanche Sweet - Emily
* Christy Cabanne - Emilys Husband
* Alfred Paget - Emilys Husbands Friend
* Edwin August - On Street Frank Evans - In Bar Charles Gorman - In Bar
* Robert Harron - On Street / At Farewell Party
* Charles Hill Mailes - In Bar / At Farewell Party
* Marguerite Marsh - At Farewell Party
* Claire McDowell - A Friend
* W. Chrystie Miller - At Farewell Party
* W. C. Robinson - On Street
* Kate Toncray - The Mother (unconfirmed) Charles West - The Bartender (as Charles H. West)

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 