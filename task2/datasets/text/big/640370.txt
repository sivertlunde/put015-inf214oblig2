Super Size Me
 Supersize Me (Beavis and Butt-head)}}
{{Infobox film
 | name           = Super Size Me
 | image          = Super Size Me Poster.jpg
 | caption        = Theatrical release poster
 | director       = Morgan Spurlock
 | producer       = Morgan Spurlock
 | writer         = Morgan Spurlock
 | starring       = Morgan Spurlock Alexandra Jamieson
 | music          = 
 | cinematography = Scott Ambrozy
 | editing        = Julie "Bob" Lombardi
 | studio         = The Con
 | distributor    = Samuel Goldwyn Films Roadside Attractions
 | released       =  
 | runtime        = 98 minutes
 | country        = United States
 | language       = English
 | budget         = $65,000    
 | gross          = $22,233,808 
}} psychological well-being, and explores the fast food industrys corporate influence, including how it encourages poor nutrition for its own profit.
 megajoules or kcal (the equivalent of 9.26 Big Macs) per day during the experiment. An intake of around 2,500 kcal within a healthy balanced diet is more generally recommended for a man to maintain his weight.  As a result, the then-32-year-old Spurlock gained 24½ lbs. (11.1&nbsp;kg), a 13% body mass increase, increased his cholesterol to 230&nbsp;mg/dL, and experienced mood swings, sexual dysfunction, and fat accumulation in his liver. It took Spurlock fourteen months to lose the weight gained from his experiment using a vegan diet supervised by his then-girlfriend (now ex-wife), a chef who specializes in gourmet vegan dishes.
 obesity throughout Surgeon General tobacco companies applies to fast food franchises whose product is both physiologically addictive and physically harmful.  

The documentary was nominated for an Academy Award for Documentary Feature.    A comic book related to the movie has been made with Dark Horse Comics as the publisher containing stories based on numerous cases of fast food health scares. 

==Content== lbs (84  kilogram|kg).

===Experiment===
Spurlock followed specific rules governing his eating habits:
* He must fully eat three McDonalds meals per day: breakfast, lunch, and dinner.
* He must consume every item on the McDonalds products|McDonalds menu at least once over the course of the 30 days (he managed this in nine days).
* He must only ingest items that are offered on the McDonalds menu, including bottled water. All outside consumption of food is prohibited.
* He must Super Size the meal when offered, but only when offered (i.e., he is not able to Super Size items himself; Spurlock was offered 9 times, 5 of which were in Texas).
* He will attempt to walk about as much as a typical U.S citizen, based on a suggested figure of 5,000 standardized distance steps per day,  but he did not closely adhere to this, as he walked more while in New York than in Houston.

On February 1, Spurlock starts the month with breakfast near his home in Manhattan, where there is an average of four McDonalds locations (and 66,950 residents, with twice as many commuters) per square mile (2.6&nbsp;km²). He aims to keep the distances he walks in line with the 5,000 steps (approximately two miles) walked per day by the average American.
 Double Quarter vomits in the McDonalds parking lot.

After five days Spurlock has gained 9.5 pounds (4.5&nbsp;kg) (from 185.5 to about 195 pounds). It is not long before he finds himself experiencing depression (mood)|depression, and he claims that his bouts of depression, lethargy, and headaches could be relieved by eating a McDonalds meal. His general practitioner describes him as being "addicted". At his second weigh-in, he had gained another 8 pounds (3.5&nbsp;kg), putting his weight at 203.5&nbsp;lb (92&nbsp;kg). By the end of the month he weighs about 210 pounds (95.5&nbsp;kg), an increase of about 24.5 pounds (about 11&nbsp;kg). Because he could only eat McDonalds food for a month, Spurlock refused to take any medication at all. At one weigh-in Morgan lost 1&nbsp;lb. from the previous weigh-in, and a nutritionist hypothesized that he had lost muscle mass, which weighs more than an identical volume of fat. At another weigh-in, a nutritionist said that he gained 17 pounds (8.5&nbsp;kg) in 12 days.
 sex drive during his experiment. It was not clear at the time whether or not Spurlock would be able to complete the full month of the high-fat, high-carbohydrate diet, and family and friends began to express concern.

On Day 21, Spurlock has heart palpitations. His internist, Dr. Daryl Isaacs, advises him to stop what he is doing immediately to avoid any serious health problems. He compares Spurlock with the protagonist played by Nicolas Cage in the movie Leaving Las Vegas, who intentionally drinks himself to death in a matter of weeks. Despite this warning, Spurlock decides to continue the experiment.

On March 2, Spurlock makes it to day 30 and achieves his goal. In thirty days, he has "Supersized" his meals nine times along the way (five of which were in Texas, four in New York City). His physicians are surprised at the degree of deterioration in Spurlocks health. He notes that he has eaten as many McDonalds meals as most nutritionists say the ordinary person should eat in 8 years (he ate 90 meals, which is close to the number of meals consumed once a month in an 8-year period).

===Findings===
 
An end text states that it took Spurlock 5 months to lose 20.1 pounds (9&nbsp;kg) and another 9 months to lose the last 4.5 pounds (2&nbsp;kg). His then-girlfriend Alex, now his ex-wife, began supervising his recovery with her "detox diet", which became the basis for her book, The Great American Detox Diet. 

The movie ends with a rhetorical question, "Who do you want to see go first, you or them?" This is accompanied by a cartoon tombstone, which reads "Ronald McDonald (1954–2012)", which originally appeared in The Economist in an article addressing the ethics of marketing to children. 

A short epilogue was added to the film. Although it showed that the salads can contain even more calories than burgers if the customer adds liberal amounts of cheese and dressing prior to consumption, it also described McDonalds discontinuation of the Super Size option six weeks after the movies premiere, as well as its recent emphasis on healthier menu items such as salads, and the release of the new adult Happy Meal. McDonalds denied that these changes had anything to do with the film.

==Reception== Best Documentary two thumbs At the Movies with Ebert and Roeper. The film overall received positive reviews from other critics, as well as movie-goers, and holds a 93% "Certified Fresh" rating on the film review aggregator Rotten Tomatoes.

Caroline Westbrook  for BBC News stated that the hype for the documentary was proper "to a certain extent", because of its serious message, and that, overall, the films "high comedy factor and over-familiarity of the subject matter render it less powerful than other recent documentaries – but it still makes for enjoyable, thought-provoking viewing." 

===Criticism and statistical notes===
Critics of the film, including McDonalds, argue that the author intentionally consumed an average of 5,000 calories per day and did not exercise, and that the results would have been the same regardless of the source of overeating.  One reviewer pointed out "hes telling us something everyone already knows: Fast food is bad for you."  Robert Davis of Paste (magazine)|Paste implied the film is an example of "how the ignorance of, or willful distortion of, basic scientific methods is used to manipulate public opinion." 

In the comedic documentary reply Fat Head, Tom Naughton "suggests that Spurlocks calorie and fat counts dont add up" and criticizes Spurlocks refusal to publish the Super Size Me food log; The Houston Chronicle reports: "Unlike Spurlock, Naughton has a page on his Web site that lists every item (including nutritional information) he ate during his fast-food month."  The film addresses such objections by highlighting that a part of the reason for Spurlocks deteriorating health was not just the high calorie intake but also the high quantity of sugar relative to vitamins and minerals in the McDonalds menu, which is similar in that regard to the nutritional content of the menus of most other U.S. fast-food chains.  About 1/3 of Spurlocks calories came from sugar. His nutritionist, Bridget Bennett RD, warned him about his excess intake of sugar from "milkshakes and cokes". It is revealed toward the end of the movie that over the course of the diet, he consumed "over 30 pounds of sugar, and over 12 lbs. of fat from their food". 

==Impact==
In the United Kingdom, McDonalds placed a brief ad in the trailers of showings of the film, pointing to the website www.supersizeme-thedebate.co.uk.  The ads stated, "See what we disagree with. See what we agree with."

The film was the inspiration for the BBC television series The Supersizers... in which the presenters dine on historical meals and take medical tests to ascertain the impact on their health. 

==See also==
* Criticism of fast food
* National Weight Control Registry
* New York State Restaurant Association v. New York City Board of Health

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 