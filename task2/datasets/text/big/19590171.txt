Shob Charitro Kalponik
{{Infobox film
| name = Sob Charitro Kalponik
| image = Shob Charitro Kalponik.jpg
| caption  = Film poster for Shob Charitro Kalponik.
| director = Rituparno Ghosh
| writer = Rituparno Ghosh
| starring = Prosenjit Chatterjee Bipasha Basu Paoli Dam  Jisshu Sengupta
| producer =
| music = Raja Narayan Deb Big Pictures
| cinematography =Soumik Haldar
| editing = Arghakamal Mitra
| released =  
| runtime =
| country = India Bengali
}}
Sob Charitro Kalponik, (  "All Characters are Imaginary") also known as Afterword, is a 2009 Bengali film by Rituparno Ghosh. 
{{Cite web|url=http://www.indiaglitz.com/channels/hindi/article/38576.html
|title=IndiaGlitz - Reliance Big Entertainment Feature Films (2008-2010) - Part II - Bollywood Movie News|publisher=www.indiaglitz.com|accessdate=29 October 2008
|last=|first=}}  The film stars Bipasha Basu and Prosenjit Chatterjee. This film is selected for the 30th Durban International Film Festival and won National Film Award for Best Feature Film in Bengali. 09  The film was screened in the Marché du Film section of the 2009 Cannes Film Festival. 

==Plot==
Non-resident Bengali Radhika (Bipasha Basu) marries the thespian poet Indranil Mitra (Prosenjit Chatterjee) to settle in Kolkata. While Indranil continues his surveillance of the surreal world of words, rhythms, rhymes and imaginations Radhika single-handedly pulls out the private and public aspects of conjugal life. Radhika gets wholesome support from their housemaid Priyobala Das (also called Nondor ma). While the apparently irresponsible and introvert Indranil does one menace after other (like quiting his job after getting an award), Radhika stands like a rock to make the family exist materially. But all the reluctance and indifference from Indranil, makes Radhika’s heart oscillate towards Shekhar, her office colleague and Indranil’s biggest admirer. Radhika becomes attracted to Shekhar (Jisshu Sengupta) but can’t abandon the unpredictability and histrionics of her spouse. 

==Cast==
* Prosenjit Chatterjee as Indranil Mitra
* Bipasha Basu as Radhika Mitra
* Jisshu Sengupta as Shekhar
* Paoli Dam as Kajori Roy
* Sohag Sen as Priyobala Das aka Nondor Maa

==References==
 

==External links==
* 

 
 
 

 
 
 
 