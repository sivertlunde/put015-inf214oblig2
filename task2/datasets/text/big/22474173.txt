The Candy Snatchers
{{Infobox Film
| name           = The Candy Snatchers
| image          = The_Candy_Snatchers_Poster.jpg
| image_size     = 
| caption        = 
| director       = Guerdon Trueblood
| producer       = Bryan Gindoff
| writer         = Bryan Gindoff
| narrator       = 
| starring       = Tiffany Bolling   Susan Sennet   Ben Piazza   Brad David   Vince Martorano 
| music          = Robert Drasnin
| cinematography = Robert Maxwell
| editing        = 
| distributor    = Subversive Cinema 1973
| runtime        = 94 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1973 Exploitation exploitation crime crime cult film directed by Guerdon Trueblood.  The film was first released in June 1973 and was unofficially inspired by the kidnapping of Barbara Jane Mackle.    It stars Susan Sennet as a teenager who is kidnapped and held for ransom by three amateur criminals. The Candy Snatchers received a DVD release in 2005 through Subversive Cinema.

Actress Tiffany Bolling has stated that she later came to regret making the film and that she had only done it for a paycheck.  She further commented that "I was doing cocaine...and I didnt really know what I was doing, and I was very angry about the way that my career had gone in the industry...the opportunities that I had and had not been given.... The hardest thing for me, as I look back on it, was I had done a television series, The New People, and so I had a lot of young people who really respected me and... revered me as something of a hero, and then I came out with this stupid Candy Snatchers movie... It was a horrendous experience." 

== Plot ==
Candy Philips (Susan Sennett) is a 16-year-old girl who is kidnapped on her way home from her Catholic school. Her three kidnappers include Eddy (Vince Martorano), his partner Jessie (Tiffany Bolling), and Jessies brother, Alan (Brad David), who bury her alive in a Southern California field. They give her a pipe for air, expecting they will soon gain a ransom from her father. Unbeknownst to them, Candys burial is witnessed by a young mute child, Sean Newton (Christopher Trueblood), who tries to tell his intolerant parents, Dudley (Jerry Butts) and Audrey (Bonnie Boland), about what he saw.

The kidnappers contact Candys step-father, Avery Phillips (Ben Piazza), demanding that he pay her ransom in all the diamonds in the jewelry store he manages. However, Avery does not report the abduction to anyone, including Candys mother Katherine (Dolores Dorn).

The kidnappers dig Candy up and bring her to their hideout. Jessie and Alan intend to remove Candys ear to present to Avery as leverage, but Eddy prevents this. Jessie and Alan visit the morgue, where they bribe the coroner Charlie (Bill Woodard) to remove an ear from a cadaver. Meanwhile, Eddy and Candy bond, with the former promising the latter that he will not let harm come to her. After Jessie and Alan return to the hideout, Alan rapes Jessie, who shows signs of mental illness.

Eddy presents the ear to Avery, who dismisses the threat of killing Candy, as he hopes to obtain two million dollars Candy is set to inherit when she turns twenty-one. Meanwhile, Alan heads to the hideout to kill Candy. Sean infiltrates the hideout, where he discovers and unmasks Candy, who tells him to contact the police. Alan returns to the hideout and rapes Candy. Eddy interrupts the assault and beats Alan. Sean escapes unseen.

Jessie and Alan insist that Candy is killed. In the night, Eddy takes Candy to her grave, promising to return to save her.

The kidnappers visit Katherine, who becomes intoxicated and is seduced by Alan. The kidnappers have Katherine call home Avery, who is having an affair with his employee, Lisa (Phyllis Major). Avery returns home, where he is held at gunpoint by the kidnappers while Alan murders Katherine.

Avery leads the kidnappers to the jewelry store, where he delivers to them the contents of the safe. Avery unsuccessfully attempts to retrieve his snubnosed revolver, but is shot by Alan. Alan attempts to shoot Eddy, who kills him. Eddy and Jessie attempt to escape, but Avery kills Jessie. Avery pursues Eddy to the grave site, where they have a gunfight that ends with Averys death.

Eddy attempts to dig Candy up, but is shot dead by Sean. Sean listens to Candys breathing through a drain pipe. Audrey calls Sean with a cowbell, prompting him to make a trail to his house by scooting down the hill on his backside. A gunshot is heard, followed by the sound of the cowbell dropping.

==Cast==
*Tiffany Bolling as Jessie
*Ben Piazza as Avery
*Susan Sennet as Candy
*Brad David as Alan
*Vince Martorano as Eddy
*Bonnie Boland as Audrey
*Jerry Butts as Dudley
*Leon Charles as Boss
*Dolores Dorn as Katherine
*Phyllis Major as Lisa
*Bill Woodard as Charlie
*Christopher Trueblood as Sean Newton (as Christophe)
*Earl Hansen as Gun Store Owner
*Harry Kronman as Deli Owner
*John Bill as Policeman

==Reception==
Critical reception for the film has been positive and the film currently holds a rating of 80% on Rotten Tomatoes, based on 5 reviews.  DVD Talk gave a positive review for the 2005 DVD release, citing the discs extras as a highlight.  Bloody Disgusting gave a more mixed review and gave the film three out of five skulls. 

== References ==
 

==External links==
*  

 
 
 
 
 
 


 