Dr. Jekyll and Mr. Hyde (1908 film)
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Dr. Jekyll and Mr. Hyde
| image          =
| image_size     =
| caption        =
| director       = Otis Turner (unconfirmed) William N. Selig
| based on       =  
| writer         = George F. Fish Luella Forepaugh
| narrator       =
| starring       = Hobart Bosworth Betty Harte
| music          =
| cinematography =
| editing        =
| distributor    = Selig Polyscope Company
| released       =  
| runtime        = 16 mins. (one reel)
| country        = United States Silent movie
| budget         =
| website        =
}} 1908 Selig silent horror horror film|motion picture starring Hobart Bosworth, and Betty Harte in her film debut.
 Directed by produced by William N. stage play (theatre)|play, which was condensed into a one reel movie short subject|short.

This is the first screen adaptation of Robert Louis Stevensons 1886 novel Strange Case of Dr Jekyll and Mr Hyde.

Critics were enthusiastic, giving Bosworth special mention: "The change is displayed with a dramatic ability almost beyond comprehension."

The film was released seven months after the death of stage actor Richard Mansfield. Mansfield created the part of Jekyll/Hyde in the theater in Dr. Jekyll and Mr. Hyde (play)|Dr. Jekyll and Mr. Hyde, beginning in 1887.

There are no known existing copies of this film.

==Synopsis==
Dr. Jekyll and Mr. Hyde began with the raising of the stage curtain. Dr. Jekyll vows his undying love for Alice, a vicars daughter, in her spacious garden. Suddenly, seized by his addiction to the chemical formula, Jekyll begins to convulse and distort himself into the evil Mr. Hyde. He savagely attacks Alice, and when her father tries to intervene, Mr. Hyde takes great delight in slaughtering him. Later on, Jekyll transforms again, but haunted by visions of the gallows, Mr. Hyde takes a fatal dose of poison, killing both identities. In true theatrical tradition, the curtain then closes.

==External links==
*  at the Internet Movie Database

 

 
 
 
 
 
 
 
 
 
 


 