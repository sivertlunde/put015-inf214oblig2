Kahin Pyaar Na Ho Jaaye (2000 film)
{{Infobox film
| name           = kahin Pyaar Na Ho Jaaye
| image          = Kahin Pyaar Na Ho Jaaye2000.jpg
| writer         = Sanjay Chel
| starring       = Salman Khan Rani Mukerji Jackie Shroff Pooja Batra Raveena Tandon Kashmira Shah
| director       = K. Muralimohana Rao
| producer       = Narendra Bajaj   Shyam Bajaj
| distributor    = Eros Multimedia Pvt. Ltd.
| released       = 17 November 2000
| runtime        = 165 min
| country        = India
| language       = Hindi
| music          = Himesh Reshammiya
| awards         =
| budget         =
}} movie of romantic comedy genre. Bollywood remake of the Hollywood movie The Wedding Singer. It stars Salman Khan and Rani Mukerji. The movie was declared an above average grosser by box office india.

==Synopsis==

The film tells the story of Prem (Salman Khan), who earns his livelihood by performing at weddings and functions. Prem is in love with Nisha (Raveena Tandon), but fate has something else in store for them. Nisha walks out on Prem since she needs the money to cure her ailing brother, who is suffering from cancer. The only way to raise funds, as Nisha sees it, is by marrying a wealthy NRI. Nisha deserts Prem on their wedding day.

Prem is heartbroken. Priya (Rani Mukerji) comes to stay with her cousin Mona (Pooja Batra), an aspiring actress. Mona is Prems neighbor and slowly, Prem starts getting attracted to Priya. But Priya gets engaged to Rahul (Inder Kumar), who, we are later told, is the same guy Nisha was supposed to marry. Priya is at a crossroads since she has also fallen in love with Prem. At this stage, Nisha also returns to Prem. All on a plane to Agra, Prem sings his love a song, which Priya realizes is for her. Prem and Priya end together while Rahul and Nisha end up together.

==Cast==
*Salman Khan ... Prem Kapoor
*Rani Mukerji ... Priya Sharma
*Raveena Tandon ... Nisha
*Jackie Shroff ... Tiger
*Pooja Batra ... Mona
*Mohnish Behl ... Vinod
*Kashmira Shah ... Neelu
*Shakti Kapoor ... Panditji
*Reema Lagoo ... Mrs. Sharma
*Inder Kumar ... Rahul Puglia

==Soundtrack==
{{Infobox album  
| Name = Kahin Pyaar Na Ho Jaaye
| Type = Soundtrack
| Artist = Himesh Reshamiya
| Cover =
| Released = 2000
| Recorded = Feature film soundtrack
| Length =
| Label = Venus Records & Tapes
| Producer = Himesh Reshamiya
| Reviews =
| Last album = Kurukshetra (2000)
| This album = Kahin Pyaar Na Ho Jaaye (2000)
| Next album = Jodi No.1 (2001)
}}
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Title !! Singer(s) !! Length 
|-
| 1
| "Kahin Pyar Na Ho Jaaye"
| Alka Yagnik, Kumar Sanu
| 05:04
|-
| 2
| "Dhin Tara Dhin Tara"
| Kumar Sanu
| 05:09
|-
| 3
| "Aa Meri Life Bana De" Sunita Rao
| 04:55
|-
| 4
|"O Priya O Priya"
| Nitin Mukesh, Kamaal Khan, Alka Yagnik, Kumar Sanu 
| 05:40
|-
| 5
| "Pardesi (I)"
| Alka Yagnik, Sonu Nigam
| 04:55
|-
| 6
| "Sawariya Re O Sawariya"
| Alka Yagnik, Kamaal Khan
| 05:05
|-
| 7
| "Parody"
| Kumar Sanu, Sonu Nigam, Alka Yagnik
| 10:25
|-
| 8
| "Pardesi (II)" 
| Sonu Nigam
| 02:16
|}

==Main characters listing==
Salman Khan is Prem. He is fond of music and plays the role of a singer. He earns his livelihood by performing at parties, shows and weddings. His only ambition in life is to become a well-known singer. He resides with his sister Nilu (Kashmira Shah) and her husband (Mohnish Behl).

Rani Mukerji plays Priya, an independent girl and a videographer, who comes to Bombay from Pune with the intention of making money to cover the cost of her marriage. She wants to fend for herself and make arrangements to meet her wedding expenses so that she does not have to depend on her mother for anything.

Pooja Batra plays Mona who is a model by profession. Her aim is to become a well-known actress. She is Priyas cousin and assists her in achieving her goal of earning money by approaching Prem to allow Priya to join his group as she is a videographer by profession.

Jackie Shroff plays Tiger, Prems friend, who organizes various kinds of shows. He has always been by Prems side in all his good and bad times. He has stood by him in his ambition to make big in the world of music. He falls in love with Mona but she does not respond.
 NRI from America. He comes to Bombay with the intention of getting married. He wants to marry an Indian girl whom he can take back to his parents in America.

==External links==
* 

 
 
 
 