Little Fugitive (2006 film)
{{Infobox film
| name           = Little Fugitive
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Joanna Lipper
| producer       = Nicholas Paleologos
| writer         = 
| screenplay     = Joanna Lipper
| story          = 
| based on       =  
| narrator       = 
| starring       = David Castro, Nicolas Martí Salgado, Justina Machado, Peter Dinklage
| music          = 
| cinematography = 
| editing        = 
| studio         = Little Fugitive LLC, Ruby Slipper Productions
| distributor    = 
| released       =  
| runtime        = 90
| country        = United States English
| budget         = 
| gross          = 
}} remake of jail and their mother work long hours a nursing home.  When Lenny plays a practical joke on Joey that goes too far, Joey soon runs away to Coney Island.

==Cast==
{| class="wikitable sortable"
|-
!Actor
!Role
|- David Castro Joey
|- Nicolas Martí Salgado Lenny
|- Peter Dinklage Sam (father of Lenny and Joey)
|- Justina Machado Natalia (mother of Lenny and Joey)
|- Raquel Castro Destiny
|- Dianne Mazzaro Ava
|- Shortee Red Malcolm
|- Javier Picayo Caleb
|- Brendan Sexton III Frank
|- Rob Lok Magician (as Robert Lok)
|- Sophie Dahl Mermaid model
|- Austin Talynn Carpenter Grace
|- Joseph Mosso
|data-sort-value="Brenner"|Sgt. Brenner
|- Fenton Lawless
|data-sort-value="Ralston"|Sgt. Ralston
|- Lois Smith Social worker 
|}

==Release==
{| class="wikitable sortable"
|-
!Date
!Location
!Category
!Award Ref
|- Seattle International Film Festival||—||—|| 
|- Brooklyn Film Narrative feature||Best editing|| 
|- Newport Beach Film Festival||—||—|| 
|}

==Critical response==
 
The film was well received by critics.  The Economist has described it as “an assured mixture of charm and depth”, L Magazine found it “a lovely ode to childhood and to Brooklyn”. 

==References==
 

===Sources===
 
*   |ref=  }}
*   }}
*   }}
 

==External links==
 
* 
* 
 

 
 
 
 


 