The Falcon in Mexico
{{Infobox film
| name           = The Falcon in Mexico
| image          = Falmexpos.jpg
| image_size     =
| caption        = Film poster
| director       = William Berke
| producer       = Maurice Geraghty
| screenplay     = Gerald Geraghty George Worthing Yates
| starring       = Tom Conway Mona Maris Martha Vickers
| music          = Various
| cinematography = Frank Redman
| editing        = Joseph Noriega RKO Radio Pictures
| released       =  
| runtime        = 70 min.
| country        = United States
| language       = English language|English}}
 Falcon film series|series. The film features many second unit sequences filmed in Mexico.

==Plot summary==
The Falcon helps a woman enter an art gallery late at night to supposedly recover a painting that belongs to her. The Falcon discovers that the woman is the model for the artists painting, however the artist has been dead since 1929, and the body of the owner of the gallery lays murdered on the floor. Wanted by the American police for murder, the Falcon finds the late artists daughter and they travel to Mexico.

==Cast==
*Tom Conway ... Tom Lawrence  
*Mona Maris ... Raquel  
*Martha Vickers  ... Barbara Wade 
*Nestor Paiva ... Manuel Romero  
*Mary Currier ... Paula Dudley  
*Cecilia Callejo ... Dolores Ybarra  
*Emory Parnell ... James Winthrop Lucky Diamond Hughes  
*Pedro de Cordoba ... Don Carlos Ybarra  
*Bryant Washburn ... Artist

==External list==
*  , imdb.com; accessed March 10, 2014.

 

 
 
 
 
 
 
 
 


 