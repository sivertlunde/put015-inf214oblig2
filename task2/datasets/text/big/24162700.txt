A Soldier's Plaything
{{Infobox film name           = A Soldiers Plaything image_size     = image         	= A_Soldiers_Plaything_1930_Poster.jpg caption        = director       = Michael Curtiz producer       = writer         = Perry N. Vekroff  from a story by Viña Delmar narrator       = starring       = Ben Lyon  Harry Langdon Lotti Loder Noah Beery music          = cinematography = Barney McGill editing        = Jack Killifer distributor    = Warner Bros. Pictures released       =   runtime        = 56 minutes (original length: 71 minutes) country        = United States language       = English budget         = gross          = website        =
}}

A Soldiers Plaything  is a 1930 talkie|all-talking comedy-drama film with songs directed by Michael Curtiz. Warner Bros. filmed this simultaneously in 35mm, and in a widescreen process called Vitascope, but it is uncertain whether the Vitascope version was ever released.  The film was planned as a full-scale musical comedy. The majority of the musical numbers of this film, however, were cut out before general release in the United States because the public had grown tired of musicals by late 1930. This accounts for the very short length of the film. The complete film was released intact in countries outside the United States where a backlash against musicals never occurred. It is unknown whether a copy of this full version still exists.

==Synopsis==
The story takes place towards the end of the first World War. Ben Lyon is gambling with some friends. When one of them accuses him of cheating they get into a fight. When Lyon sees his opponent fall down the stairs he assumes he has died. He escapes with his friend, played by Harry Langdon, before the police arrive by joining a parade of men who are enlisting for the army. They end up joining together as Langdon has already made up his mind that he wants to be in the army. They get into trouble with the captain, played by Noah Beery, on numerous occasions leading him to punish them numerous times by making them stable cleaners. When they are stationed at the German town of Koblenz, Lyon meets and falls in love with Lotti Loder, the daughter of an innkeeper. He is unable to propose marriage to her, however, with a murder charging hanging over his head. Eventually, the man he thought he murdered turns up and this allows Lyon to finally marry Loder.

==Cast==
*Ben Lyon - Georgie Wilson
*Harry Langdon - Tim
*Lotti Loder - Gretchen Rittner
*Noah Beery - Captain John Plover
*Fred Kohler - Hank - Poker Player
*Lee Moran - Corporal Brown
*Marie Astaire - Lola Green
*Frank Campeau - Joe - Poker Player Frank Mills - Extra as Doughboy (uncredited)

==Preservation==
Only copies of the cut print released in the United States (without most of the musical numbers) seems to have survived. The complete film, which had a running length of 71 minutes, was released intact in countries outside the United States where a backlash against musicals never occurred. It is unknown whether a copy of this full version still exists. 

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 


 