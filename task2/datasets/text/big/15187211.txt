¡Dispara!
 
{{Infobox film
| name           = ¡Dispara!
| image          = Dispara.jpg
| caption        = Theatrical release poster
| director       = Carlos Saura 
| production Company     =  5 Films S.A
| writer         = Enzo Monteleone    Carlos Saura
| starring       = Francesca Neri  Antonio Banderas  
| music          = Alberto Iglesias
| cinematography = Javier Aguirresarobe
| editor         =  Juan Ignacio San Mateo
| distributor    = 
| released       = 1 October 1993
| runtime        = 108 minutes
| country        = Spain Spanish
| budget         = 
|}}
 Spanish  film directed by Carlos Saura, starring Francesca Neri  and Antonio Banderas. The story is a revenge tragedy.

==Plot==
Marcos, a young reporter, goes to a circus to write a Sunday supplement piece. As he is leaving, the next act is about to start. It involves a woman riding a horse and performing tricks; the presentation ends in shooting balloons from a horse while it is moving. Marcos is taken by the beauty of Ana, the equestrian sharpshooter, and returns to interview her. She invites him to dinner with the troupe. They dance, and then spend the night together. He falls in love with the beautiful horse-riding circus girl. An affair between them ensues; he considers following her around Europe and promises he would follow her to hell. Soon, Marco has to leave to cover a concert in Barcelona. 

Fate intervenes when three young mechanics come to repair circus equipment and the owner gives them complimentary tickets for the show. The trio makes a racket as they watch Ana perform. After the show, they follow Ana to her trailer and brutally rape her. Although she is badly hurt, she decides to take matters into her own hands. Bruised, humiliated, and bleeding, she picks up her rifle and goes to hunt them down. She finds them easily at the mechanic shop where they work. She kills them all and leaves without being clearly identified. Ana, who is bleeding badly, has to visit a doctor who reveals to the authorities that she has been raped. The police initially have no clues about the culprit of the triple homicide, but after interviewing the doctor, they begin to suspect Ana. 

Ana is stopped in a highway by two officers; she panics and kills them too, a decision that she regrets immediately. Marco who goes to Ana’s trailer, finds traces of blood all over the place and he and the authorities go in search of Ana. She finds refuge in a country home where a couple and two small children live. Marcos is responsible for breaking the impasse between Ana and the police, but he arrives too late to help her; the police open fire on her, and she dies in his arms.

==Cast==
*Francesca Neri as Ana
*Antonio Banderas as Marcos
*Eulalia Ramon as Lali
*Coque Malla as rapist

==DVD release== English title dubbing is of poor quality.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 