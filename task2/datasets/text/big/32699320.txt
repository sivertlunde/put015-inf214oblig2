In the Shadow of Kilimanjaro
{{multiple issues|
 
 
}}

{{Infobox film
| name           = In the Shadow of Kilimanjaro
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Raju Patel
| producer       = Jeffrey M. Sneller
| writer         = T. Michael Harry Jeffrey M. Sneller
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = John Rhys-Davies Timothy Bottoms Irene Miracle
| music          = Arlon Ober
| cinematography = Chuy Elizondo
| editing        = Pradip Roy Shah
| studio         = 
| distributor    = Scotti Brothers Pictures Columbia Broadcasting System
| released       =    
| runtime        = 92 minutes
| country        = UK Kenya
| language       = English
| budget         = 
| gross          =
}}

In the Shadow of Kilimanjaro is a 1986 horror film based in Kenya. The film was directed by Raju Patel.

==Plot==
The film, based on a true story, shows the events of an incident that happened in Kenya in 1984. A severe drought happened, and 90,000 starving babboons went on a rampage, killing humans and animals in their way.

==Cast==
*John Rhys-Davies ... Chris Tucker
*Timothy Bottoms ... Jack Ringtree
*Irene Miracle ... Lee Ringtree
*Michele Carey ... Ginny Hansen
*Patrick Gorman ... Eugene Cruz

==External links==
 

 
 
 