The Girl on a Motorcycle
{{Infobox film
| name           = The Girl on a Motorcycle
| image          = The-Girl-on-a-Motocycle.jpg
| image_size     =
| caption        =
| director       = Jack Cardiff
| producer       = William Sassoon
| writer         = André Pieyre de Mandiargues (novel) Ronald Duncan Jack Cardiff Gillian Freeman
| narrator       =
| starring       = Alain Delon Marianne Faithfull Les Reed
| cinematography = Jack Cardiff
| editing        = Peter Musgrave
| distributor    = British Lion Films (UK) Warner Bros. (US)
| released       = October 1968
| runtime        = 91 minutes
| country        = United Kingdom France
| language       = English
| budget         = gross = 626,331 admissions (France) 
| preceded_by    =
| followed_by    =
}} full body suit that Marianne Faithfull wore in the film. 

==Plot== Alsatian bed psychedelic and erotic reveries as she relives her changing relationship with the two men, before crashing into a truck at the films conclusion.

It is based on the novella La Motocyclette by André Pieyre de Mandiargues.  In the medium to distant shots, the rider was the British GP champion Bill Ivy, wearing a blonde wig. Ivy died in a motorcycle accident in July 1969.

==Cast==
* Alain Delon as Daniel
* Marianne Faithfull as Rebecca
* Roger Mutton as Raymond
* Marius Goring as Rebeccas Father
* Catherine Jourdan as Catherine
* Jean Leduc as Jean
* Jacques Marin as Pump Attendant
* André Maranne as French Superintendent
* Bari Jonson as First French Customs Officer
* Arnold Diamond as Second French Customs Officer
* John G. Heller as German Customs Officer
* Marika Rivera as German Waitress
* Richard Blake as 1st Student
* Chris Williams as 2nd Student
* Colin West as 3rd Student

==Reception==
The film was the 6th most popular movie in general release in Britain during 1968. John Wayne-money-spinner
The Guardian (1959-2003)   31 Dec 1968: 3. 
==Home media releases== Anchor Bay. A remastered edition on DVD and Blu-ray Disc was released in 2012 by Redemption Films.

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 