Hardware (film)
 
{{Infobox film
| name           = Hardware
| image          = Hardwareposter.jpg
| alt            = 
| caption        = Theatrical release poster Richard Stanley
| producer       = JoAnne Sellar Paul Trybits
| writer         = Richard Stanley
| based on       =   John Lynch William Hootkins Iggy Pop
| music          = Simon Boswell
| cinematography = Steven Chivers
| editing        = Derek Trigg Unlimited Palace Productions
| distributor    = Palace Pictures   Miramax  
| released       =  
| runtime        = 94 minutes  
| country        = United Kingdom United States
| language       = English
| budget         = £960,000  ($1.5 million)
| gross          = $5,728,953 (US)   
}} science fiction Richard Stanley 2000 AD, the film depicts the rampage of a self-repairing robot in a post-apocalyptic slum.

== Plot ==
A nomad scavenger treks through an irradiated wasteland and discovers a buried robot. He collects the pieces and takes them to junk dealer Alvy, who is talking with Hard Mo Baxter, a former soldier, and Mos friend Shades. When Alvy steps away, Mo buys the robot parts from the nomad and sells all but the head to Alvy. Intrigued by the technology, Alvy begins to research its background. Mo and Shades visit Jill, Mos reclusive girlfriend, and, after an initially distant welcome where Jill checks them with a Geiger counter, Mo presents the robot head as a Christmas gift.  Jill, a metal sculptor, eagerly accepts the head. After Shades leaves, Mo and Jill argue about a government sterilization plan and the morality of having children. Later, they have sex, while being unknowingly watched by their foul-mouthed, perverted, voyeuristic neighbor Lincoln Weinberg via telescope.

Jill works the robot head into a sculpture, and Mo says that he likes the work, but he does not understand what it represents. Frustrated, Jill says it represents nothing and resents Mos suggestion that she make more commercial art to sell. They are interrupted by Alvy, who urges Mo to return to the shop, as he has important news about the robot, which he says is a M.A.R.K. 13. Before he leaves, Mo checks his Bible, where he finds the phrase "No flesh shall be spared" under Mark 13:20, and he becomes suspicious that the robot is part of a government plot for human genocide. Mo finds Alvy dead of a cytotoxin and evidence that the robot is an experimental combat model capable of self-repair; Alvys notes also indicate a defect, a weakness to humidity. Worried, Mo contacts Shades and asks him to check on Jill, but Shades is in the middle of a drug trip and barely coherent.

Back at the apartment, the robot has reassembled itself using pieces of Jills metal sculptures and recharged by draining her apartments power network. It attempts to kill Jill, but she traps it in a room after the apartments doors lock. Lincoln sees the robot close the blinds while trying to peep on Jill, and, after he briefly manages to open the apartment door and sexually harasses her, offers to override the emergency lock that traps them in her apartment. Lincoln dismisses her warnings of a killer robot, and, when he attempts to open Jills blinds so that he can more easily peep on her, the M.A.R.K. 13 brutally kills him. Jill flees into her kitchen, where she reasons that her refrigerator will hide her from the robots infrared vision. She damages the robot before Mo, Shades, and the apartments security team arrive and open fire on it, apparently destroying it.

As Jill and Mo embrace, the M.A.R.K. 13 drags her out a window, and she crashes into her neighbors apartment. Jill races back upstairs to help Mo, who is alone with the M.A.R.K. 13. Overconfident, Mo engages the robot in battle, and it injects him with the same toxin that killed Alvy. Mo experiences euphoria and a series of hallucinations as he dies. After Jill reenters her apartment, the M.A.R.K. 13 sets her apartment doors to rapidly open and close; the security team die when they attempt to enter, and Shades is trapped outside. Jill hacks into the M.A.R.K. 13s CPU and unsuccessfully attempts to communicate with it; however, she discovers the robots weakness and lures the M.A.R.K. 13 into the bathroom. Shades, who has managed to quickly jump through the doors, gives her time to turn on the shower. The M.A.R.K. 13 short circuits and is finally deactivated. The next morning, a radio broadcast announces that the M.A.R.K. 13 has been approved by the government, and it will be mass manufactured.

== Cast ==
* Dylan McDermott as Moses "Hard Mo" Baxter
* Stacey Travis as Jill John Lynch as Shades
* Iggy Pop as Angry Bob
* Carl McCoy as Nomad
* William Hootkins as Lincoln Wineberg, Jr.
* Mark Northover as Alvy
* Paul McKenzie as Vernon
* Lemmy as Water taxi driver

== Production == 2000 AD comic strip.  Other influences include Soylent Green, Damnation Alley, and the works of Philip K. Dick.     Writer-director Richard Stanley had previously made a post-apocalyptic short film when he was a teenager, and Hardware grew out of that film and responses he got from other, unproduced scripts.     Stanley had recently joined a guerrilla Muslim faction in the Soviet war in Afghanistan,  where he shot a documentary.   After he left there, started pre-production of Hardware almost immediately afterward.  Psychic TV was an inspiration for the exaggerated television broadcasts.  Stanley says that the robot does not know that it is committing evil, and it only obeys its programming, which could be likened to a spiritual quest.   The opening scene was shot in Morocco, and the rest of the film was shot in east London.  The film was originally more specifically British, but Miramax insisted on American leads.  Stanley then added a multinational cast to muddy the setting.  Stanley wanted to emphasize themes of fascism and passive acceptance of authoritarianism, as he had recently come from the apartheid regime of South Africa. 

== Release ==
Hardware was originally rated "X" by the MPAA for its gore.   It was later cut to avoid the stigma of a rating associated with pornography.   In the United States, the film debuted at number six.   It grossed $2,381,285 in its opening weekend and had a total domestic gross of  $5,728,953 in 695 theaters. 

Due to its unexpected success, the film was caught up in continual legal issues that prevented its release on DVD for many years. 
Hardware was released on Region 2 DVD and Blu-ray Disc on 22 June 2009.  It was released on Region 1 DVD and Blu-ray Disc on 13 October 2009 by Severin Films. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that the film received positive reviews from 50% of twelve surveyed critics; the average rating was 5.7/10.   On its original release, Hardware received mixed reviews from critics, who cited it as derivative of Alien (film)|Alien and The Terminator.   Owen Gleiberman of Entertainment Weekly rated the film D+ and called it unoriginal, "as if someone had remade Alien with the monster played by a rusty erector set."   Variety (magazine)|Variety wrote, "A cacophonic, nightmarish variation on the postapocalyptic cautionary genre, Hardware has the makings of a punk cult film."   Michael Wilmington of the Los Angeles Times called it a shallow splatter film whose exaggerated bleakness elevates it above the typical techno-thriller.   Vincent Canby of The New York Times described it as a future midnight movie and wrote, "Watching Hardware is like being trapped inside a video game that talks dirty."   Richard Harrington of The Washington Post called it "an MTV movie, a mad rush of hyperkinetic style and futuristic imagery with little concern for plot (much less substance)."  

Despite mixed reviews during original release, Hardware managed to become a cult film.  Ian Berriman of SFX (magazine)|SFX wrote, "Its one of those lovingly crafted movies where ingenuity and enthusiasm overcome the budgetary limitations."   Matt Serafini of Dread Central rated it 4/5 stars and wrote, "Hardware isnt quite the masterpiece that some its most ardent fans have claimed, but its an excellent piece of low-budget filmmaking from an era when low-budget wasnt synonymous with camcorder crap."   Bloody Disgusting rated it 3.5/5 stars and called it "an austere and trippy film" with a narrative that is "a disjointed mess"; however, the films excesses make it a cult film.   Todd Brown of Twitch Film called it "essentially a lower budget, more intentionally punk take on The Terminator" that has an "undeniable ... sense of style".   At DVD Verdict, Daryl Loomis called it slow-paced but stylistic and atmospheric,  and Gordon Sullivan called it "a hallucinatory and violent film" that has an overly detailed, slow-paced beginning.   Writing for DVD Talk, Kurt Dahlke rated it 3/5 stars and called it a "forgotten gem" that "is overwhelmed by style and gore",  and Brian Orndorf called it "an art-house, sci-fi gorefest" that is moody and atmospheric without buckling under its own weight.   Michael Gingold of Fangoria rated it 3/4 stars and wrote, "If the ingredients of HARDWARE are familiar, Stanley cooks them to a boil with a relentless pace and imagery that makes his future a tactile place". 

== References ==
 

== Further reading ==
*  

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 