Power (1986 film)
{{Infobox film
| name           = Power
| image          = Power movie poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Sidney Lumet
| producer       = Reene Schisgal Mark Tarlov Kenneth Utt Wolfgang Glattes
| writer         = David Himmelstein
| starring = {{Plainlist|
* Richard Gere
* Julie Christie
* Gene Hackman
* Kate Capshaw
* Denzel Washington
* E. G. Marshall
* Beatrice Straight
}}
| music          = Cy Coleman
| cinematography = Andrzej Bartkowiak
| editing        = Andrew Mondshein
| studio         = Lorimar Productions
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 111 minutes
| budget         = $16 million
| gross          = $3,800,000 
}}
 American drama film directed by Sidney Lumet and starring Richard Gere. The original screenplay by David Himmelstein focuses on political corruption and how power affects both those who wield it and the people they try to control.
 Outstanding Supporting Worst Supporting Actress.

==Plot summary== Senate seat being vacated by St. Johns friend Sam Hastings (E. G. Marshall).

He comes into conflict with Arnold Billings (Denzel Washington), a public relations expert whose firm Cade has hired.  St. Johns investigation into Cades background prompts Billings to retaliate by bugging St. Johns office phones, flooding the basement of his headquarters, tampering with his private jet, and interfering with his other clients.

These actions force St. John to take a hard look at himself and what he has become and to decide whether his ex-wife Ellen Freeman (Julie Christie) and his former partner Wilfred Buckley (Gene Hackman) are right in believing that his success is due primarily to the exploitation of others.

==Cast==
*Richard Gere ..... Pete St. John 
*Julie Christie ..... Ellen Freeman 
*Gene Hackman ..... Wilfred Buckley 
*Kate Capshaw ..... Sydney Betterman 
*Denzel Washington ..... Arnold Billings 
*E. G. Marshall ..... Sam Hastings 
*Beatrice Straight ..... Claire Hastings 
*Fritz Weaver ..... Wallace Furman 
*Michael Learned ..... Andrea Stannard 
*J. T. Walsh ..... Jerome Cade 
*Matt Salinger ..... Phillip Aarons

==Production==
*Film editing ..... Andrew Mondshein Original Music ..... Cy Coleman    
*Cinematography ..... Andrzej Bartkowiak Production Design ..... Peter S. Larkin  Art Direction ..... William Barclay     Set Decoration ..... Thomas C. Tonery    
*Costume Design ..... Anna Hill Johnstone
 Alburquerque and Vasquez Rocks Natural Area Park in Agua Dulce, California; and Washington, D.C.

===Poster===
The poster for the film is primarily black in color, with a white bar on top that reads, in black letters: 
"More seductive than sex... 
"More addictive than any drug... 
"More precious than gold. 
"And one man can get it for you. 
"For a price." 
Below this is the films title, in all-capital white letters. A small human silhouette is located at the base of the "W" in "Power." On the black field below is written, in red letters against the black: 
"Nothing else comes close."

==Critical reception==
In his review in the New York Times, Vincent Canby described the film as "a well-meaning, witless, insufferably smug movie that . . . suffers from the total lack of a comic imagination." 

In the Chicago Sun-Times, Roger Ebert observed, "Its smart, its knowledgeable, sometimes its funny, occasionally it is very touching, and I learned something from it . . . The movie builds up considerable momentum during its first hour. Theres a sense of excitement, of identification with this man who is being driven by his own energy, ambition and cynicism . . . During the second half of the movie, however, a growing disappointment sets in. Power is too episodic. It doesnt really declare itself to be about any particular story, any single clear-cut issue . . . the movie itself seems to sense that its going nowhere. The climax is a pointless, frustrating montage of images. Its a good montage, but it belongs somewhere in the middle of the movie; it states the problem, but not the solution or even the lack of a solution. The movie seems to be asking us to walk out of the theater shaking our heads in disillusionment, but I was more puzzled than disillusioned." 

==See also==
* List of American films of 1986

==References==
 

==External links==
*  at the Internet Movie Database
*  at Rotten Tomatoes

 

 
 
 
 
 
 
 

 