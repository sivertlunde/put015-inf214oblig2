Steptoe and Son Ride Again
 
{{Infobox Film  |
  name     = Steptoe and Son Ride Again|
  image          = steptoe-and-son-ride-again.jpg|
  producer         = Aida Young | Peter Sykes | Alan Simpson |
  starring       = Wilfrid Brambell Harry H. Corbett |
  music         = Roy Budd Jack Fishman Ron Grainer  |
  cinematography = John Wilcox |
  editing         = Bernard Gribble |
  distributor    = MGM EMI |
  released   =   |
  runtime        = 99 min. |
  country = United Kingdom  |
  language = English |
}}
 Steptoe and Son. Again the film starred Wilfrid Brambell and Harry H. Corbett.

==Plot==
 racing greyhound. Harold reveals to Albert that he purchased this from local gangster and loan shark Frankie Barrow for the £80 plus a further £200 owing on top. Furthermore, he plans to pay a small fortune to keep it fed on egg and steak.  

They eventually have to sell all of their possessions to have one final bet on their dog at the races to try to pay off the money they owe. When their dog loses, they just about lose hope when Albert brings up that he had saved £1,000 in life insurance. Harold then schemes to get the money from his father. They find an old mannequin among their collection of junk and fit it around Alberts body. They then call Dr. Popplewell, a known alcoholic doctor, whos fortunately drunk at the time of seeing Albert and he announces that Albert has died. Harold then brings home a coffin that he has been saving for the inevitable day that his father would actually die.

The next day some old friends of Alberts come to visit and pay their respects to Albert. They announce that they have arranged a funeral for him and this isnt good news for either of the Steptoes. Later one of Alberts friends ask if he could look at him for a final time. Knowing that the coffin is actually full of scrap metal, Harold makes the excuse that his fathers face is all distorted because of a difficult visit to the lavatory which is what caused him to die.
 army in Federation of Malaya|Malaya. Harold asks why he didnt cancel the insurance plan and Alberts only excuse is "I forgot." Harold then has the idea of bringing Albert back to life. However once inside the coffin, Albert falls into a deep sleep and nothing seems to wake him up. Harold tries to wake him several times during the journey to the cemetery, however on the way he is hit in the head by the back door of a removal truck. They decide to take Harold to the hospital and carry on to the funeral without him. At the hospital Harold runs away and gets a taxi to the cemetery.

There Harold accidentally smashes into a tomb and whilst being buried Albert finally wakes up and frightens everyone away. The vicar runs off and meets Harold looking like he himself is one of the undead. Back home, Albert withdraws the insurance plan and receives £876. They pay off their debt and buy a new horse with new riding equipment, but to Alberts horror, Harold invests the rest of the money in a part share of a race horse. He discovers that his partner is called Elizabeth II of the United Kingdom|H.M. Queen...

==Cast==
* Wilfrid Brambell as Albert Steptoe
* Harry H. Corbett as Harold Steptoe
* Diana Dors as Woman in Flat
* Milo OShea as Dr. Popplewell
* Bill Maynard as George Neil McCarthy as Lennie
* Yootha Joyce as Freda
* George Tovey as Percy
* Olga Lowe as Percys wife
* Sam Kydd as Claude
* Joyce Hemson as Claudes wife
* Henry Woolf as Frankie Barrow
* Geoffrey Bayldon as Vicar
* Frank Thornton as Mr. Russell Richard Davies as Butcher
* Peter Brayham as The chicken/pigeon Keeper. (uncredited) Pickfords Removals.

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 