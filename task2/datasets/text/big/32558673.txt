The Cat (2011 film)
{{Infobox film
| name           = The Cat
| image          = The_Cat_(2011_film).JPG
| caption        = Poster
| film name      = {{Film name
| hangul         =   :  
| rr             = Goyangi: Jukeumeul Boneun Du Gaeui Nun 
| mr             = Koyangi: Chugŭmŭl Ponŭn Tu Kaeŭi Nun}}
| director       = Byun Seung-wook 
| producer       = Lee Joon-dong
| writer         = Jang Yun-mi
| starring       = Park Min-young   Kim Dong-wook   Kim Ye-ron
| music          = Lee Jae-jin
| cinematography = Lee In-won
| editing        = Kim Hyeon
| distributor    = Next Entertainment World
| released       =  
| runtime        = 106 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
The Cat ( ; lit. "The Cat: Eyes that See Death") is a 2011 South Korean horror film directed by Byun Seung-wook. The film is about So-yeon (Park Min-young), who works at a small pet-grooming shop called Kitty N Puppy. So-yeon suffers from claustrophobia and starts having apparitions of a ghostly young girl with cat-like eyes (Kim Ye-ron).    

==Plot==
So-yeon works as a groomer in a pet shop, but suffers from claustrophobia due to childhood trauma. A woman comes to the pet shop to collect her Persian cat, Bidanyi. The next day, the woman is found dead in an elevator, but Bidanyi is unharmed. The police investigate, but are unable to determine the cause of her death. So-yeon sees the crime scene and recognizes the woman. Her friend Kim Jun-seok, one of the police officers investigating the murder, gives her Bidanyi to look after.

At home, So-yeon starts to have nightmares of a young girl with cat-like eyes. Later, she accompanies her friend Bo-hee to a run-down animal shelter, where she is going to adopt a cat. Bo-hee chooses a cat nicknamed "Dimwit" by the shelter staff. Alone in the cat room, So-yeon panics when the door suddenly slams shut and locks. She becomes disorientated and coughs up white hairballs, but Bo-hee soon comes for her. At the police station, Jun-seok and his fellow officers watch CCTV footage of the woman who died in the elevator. She is seen staggering and collapsing, and its concluded that she died of a panic attack.

At the pet shop, So-yeon hallucinates seeing several dead cats in a closet. She visits Bo-hee, who is attempting to pamper Dimwit. The distressed Dimwit runs into a dark closet, but when Bo-hee follows him, she is killed by the cat-eyed girl. That night, So-yeon cuts her finger while preparing food for Bidanyi. She stains the sofa with blood, and Bidanyi licks her cut, and then the blood on the sofa, hissing aggressively when So-yeon shouts at him. The next day, she takes Bidanyi to the dead womans husband, but he does not want him. He explains that in the days leading up to her death, his wife claimed to be haunted by a strange little girl. Disturbed, So-yeon leaves Bidanyi in a park, and helps a confused old woman to the police station. At the animal shelter, a staff member euthanizes and cremates a cat, but is pulled inside the furnace by an unseen force and burns to death.

Jun-seok and So-yeon go to the animal shelter, where they find several dead cats and the charred remains of the staff member in the furnace. Returning to the police station, So-yeon and Jun-seok learn that some time ago, there was an infestation of stray cats in the boiler room of an apartment complex. The doors and windows were cemented shut and the cats were left to suffocate. Two weeks later, a video was taken of the workers coming in to remove the dead cats, and So-yeon remarks the similarity of the recent murder victims all being found dead in a small enclosed space.

So-yeon is again approached by the old woman, who is looking for her granddaughter. When So-yeon takes her back to the police station, Jun-seok discovers that the old woman reported her missing granddaughter nine months ago, but her son closed the case. So-yeon escorts the woman back to her apartment - in the same complex where the stray cats lived in the boiler room - and Jun-seok gives her a photo of the granddaughter, who looks exactly like the cat-eyed girl. So-yeon sees the old womans son beating his mother; he goes to confront her for watching but is killed by a horde of cats. 

So-yeon goes to the complexs boiler room and is confronted by more cats. Bidyani appears, and So-yeon follows him to escape, but falls into a large canister. The cat-eyed girl appears and shows So-yeon how she died; she had once played with the cats in the boiler room, and upon hearing of the plans to kill them she attempted to hide them in the canister. While climbing out of it she fell and was paralysed, dying with the cats after the door and windows were cemented. Jun-seok arrives, finding the old woman sitting on the boiler room floor and So-yeon clutching the girls corpse in the canister.

Having conquered her claustrophobia, So-yeon visits her father in a mental hospital, riding in an elevator for the first time without panicking. As she leaves, she and Ju-seok find a kitten underneath their car, and she kindly beckons it to come towards her.

==Cast==
*Park Min-young as So-yeon 
*Kim Ye-ron as Hee-jin
*Kim Dong-wook as Jun-seok
*Shin Da-eun as Bo-hee
*Lee Sang-hee as animal pound doctor
*Jo Seok-hyun as Park Joo-im
*Park Hyun-young as Kim Soon-kyung
*Baek Soo-ryun as grandmother with dementia
*Lee Han-wi as pet shop owner
*Lee Jung-ok as Police chief Lee
*Seo Yi-seok as psychiatrist
*Lee Ji-hyun as veterinarian
*Kim Min-jae as animal rescue staff
*Jo Han-hee as womens association head
*Song Moon-soo as manager
*Lee Jung-gu as asylum doctor
*Kim Gye-seon as asylum receptionist
*Kim Ik-tae as So-yeons father
*Lee Cheol-min as son of demented grandmother Lee Sung-min as Bidans "papa"
*Yoon Ga-hyun as Bidans "mom"

==Release==
The Cat premiered in South Korea on July 7, 2011. 

===Box office===
On its opening week, it grossed   in South Korea, placing it at second place on the weekend box office chart.  It grossed a total of   in South Korea and   in the foreign markets.  

===Critical reception===
Film Business Asia gave the film a 5/10 review, saying it is "almost a paint-by-numbers example of a classic Korean horror...The Cat is a watchable curio and no more." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 