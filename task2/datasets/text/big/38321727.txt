Super Hybrid
{{Infobox film
| name           = Super Hybrid
| image          = 
| caption        = 
| director       = Eric Valette
| producer       = Kevin DeWalt Oliver Hengst Tim Kwok
| cinematography = John R. Leonetti
| starring       = Shannon Beckner Oded Fehr Ryan Kennedy Melanie Papalia
| music          = Thomas Schobel Martin Tillman
| editing        = James Coblentz
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $13 million
| gross          = Unknown
}} horror thriller film about a malicious shape shifting sentient car that devours its victims by tricking them into its cab, sent to a police precinct garage after a terrible accident it stalks the mechanics as it tries to find a way to escape.

==Plot==
On the back streets of Chicago, two would-be car thieves walk past a jet black car (a fourth generation Chevrolet Nova), only to turn back a few seconds later to find a new red sports car (a Chevrolet Corvette Z06). As they climb inside they discover the ignition, the handles and every means of escape have instantly vanished, the windows go black around them and their screams are soon cut off. Driving out to the main street, the black car is demolished by a car flying down the road. The people of the second car are severely injured, but the authorities dont find any passengers in the black car and they have it impounded at a police garage. While there, the vehicle miraculously pieces itself together and kills one of the crewmen, Hector by ramming him into an elevator shaft.

Ray, the garage owner is unable to find Hector and he tells his crew; Gordy, an experienced mechanic with a hearing aid, another mechanic Al, and Bobby, a student whose aunt Tilda got him the job, to go find Hector so they can move cars off the third level. Al and Bobby find the black car, but are unable to find Hector. Tilda arrives and is chewed out by Ray for Bobby studying on the job, she asks Rays secretary Maria if shed seen a necklace that she lost the night before, but Maria doesnt know. Tilda is sent down to help find Hector and comes across the car, which she and Bobby find strange to the touch like it isnt made out of metal and engine, which is completely silent makes hissing noises when they listen to it. After they separate, Al comes across a 1968 Lincoln Continental which he starts to climb into before he is interrupted by Tilda. Suddenly the car comes to life, wrapping a tentacle-like appendage around Al and sucking him into the car, Tilda is unable to free him and is struck by the car as it escapes. Initially Ray and the others think she imagined the entire thing, but when the car comes back, Ray orders the hood opened and a giant snakes head bursts from it, nearly killing the four of them. It goes after Maria who is rescued at the last moment by Tilda; luring the car away from the girls, Gordy is run under the car that strikes an electrical box, killing him and putting the garage on emergency power.

With the lights limited, the group searches for Rays keys to the exit but cant find it, all emergency exits and access points outside are welded shut to prevent drug addicts from breaking in and stealing their equipment. Ray suggests they hunt the car down and kill it, Tilda agrees as she noticed evidence that it is bleeding a strange metallic substance indicating it is still injured from its earlier accident. They work to set up a Burmese tiger trap using welded spikes in the elevator shaft, and a large tarp to trick the car into it. Meanwhile, the car slowly starts making its way down toward them, setting off the alarms to alert the others of its location. The group fashions Molotov cocktails to use against the car, Tilda acts as the bait to draw it into the trap, but is chased down by the car. Maria throws a cocktail at the car, which bounces off and incinerates her by accident and she falls to her death in the pit. Set back by their loss, the three remaining decide to go ahead with the plan. Bobby gets into the police SUV theyd been using and they discover it is a trap. Tilda tries to free Bobby, but it kills him and drives off. Ray and Tilda go after the car and are nearly outsmarted when it double backs on them, Tilda and Ray manage to get it to fall into the pit and Tilda knocks a car from above on top of it, killing it.

Ray reveals he had the keys to the exit the entire time and opens the garage for Tilda to leave, her boyfriend David comes in and examines his destroyed Chevrolet Camaro|Camaro, and she walks off while Ray dials a News station to tell them about what happened. Tilda notices five more cars like the one theyd killed go toward the garage, but seems too desensitized to care, distraught over her loss. Ray, however notices he is surrounded by the cars that all turn their headlights on him, but his fate is unknown.

==Cast==
* Shannon Beckner as Tilda
* Oded Fehr as Ray
* Ryan Kennedy as Bobby
* Melanie Papalia as Maria
* Adrien Dorval as Gordy
* John Reardon as David
* Alden Adair as Hector
* Josh Strait as Al

==Release==
Super Hybrid was released in Australia on August 25, 2011.

==External links==
* 
* 