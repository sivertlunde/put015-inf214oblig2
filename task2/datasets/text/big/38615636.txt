Love, Mother
{{Infobox film
| name           = Love, Mother
| image          = 
| caption        = 
| director       = János Rózsa
| producer       = 
| writer         = Miklós Vámos
| starring       = Dorottya Udvaros
| music          = 
| cinematography = Elemér Ragályi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Love, Mother ( ) is a 1987 Hungarian drama film directed by János Rózsa. It was entered into the 15th Moscow International Film Festival where Dorottya Udvaros won the award for Best Actress.   

==Cast==
* Dorottya Udvaros as Kalmár Juli
* Róbert Koltai as Kalmár Géza
* Sándor Gáspár as Doki
* Kati Lajtai as Kalmár Mari
* Simon Gévai as Peti Kalmár (as Gévai G. Simon)
* Péter Andorai as Csezmiczey
* Ildikó Bánsági as Osztályfõnökn&otilde
* Erika Bodnár as Szomszédasszony
* Zsuzsa Töreky as Szeretõ
* Judit Pogány as Vendég asszony
* Frigyes Hollósi as Vendég férfi

==References==
 

==External links==
*  

 
 
 
 
 
 
 