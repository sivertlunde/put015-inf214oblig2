Brothers Under the Chin
 
{{Infobox film
| name           = Brothers Under the Chin
| image          =
| image size     =
| caption        =
| director       = Ralph Ceder
| producer       = Hal Roach
| writer         = Hal Conklin Al Giebler H. M. Walker
| narrator       =
| starring       = Stan Laurel
| music          =
| cinematography = Frank Young
| editing        = Thomas J. Crizer
| distributor    =
| released       =  
| runtime        = 20 minutes
| country        = United States English intertitles
| budget         =
}}
 1924 silent silent comedy film featuring Stan Laurel.

==Cast==
* Stan Laurel
* Ena Gregory James Finlayson
* Noah Young
* William Gillespie
* Sammy Brooks
* Jack Ackroyd Eddie Baker
* Fred Karno Jr.
* John B. OBrien
* Al Ochs
* George Rowe
* Tonnage Martin Wolfkeil

==See also==
* List of American films of 1924
* Stan Laurel filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 