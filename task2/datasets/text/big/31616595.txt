Money Money (1995 film)
 
 
{{Infobox film
| name           = Money Money
| image          =
| caption        =
| writer         = Uttej  
| producer       = Ram Gopal Varma
| director       = Siva Nageswara Rao   Krishna Vamsi   
| story          = Siva Nageswara Rao
| screenplay     = Siva Nageswara Rao Chinna Jayasudha Renuka Shahane  Surabhi Paresh Rawal Bramhanandam Sri
| studio         = Varma Creations
| cinematography = Rasool 
| editing        = Shankar
| distributor    =
| released       = 1995
| runtime        = 2:32:49
| country        =   India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu film Telugu movie Money (1993 film)|Money. A sequel to the film Money Money, More Money was released in 2011. The film was another Hit at box office.

==Cast==
 
* Jagapathi Babu (Cameo) as Gaayam Durga 
* Urmila Matondkar (Cameo) as Gaayam Chitra
* J.D. Chakravarthy as Chakri Chinna as Bose
* Jayasudha as Vijaya
* Renuka Shahane as Renu
* Surabhi as Sudha
* Kota Srinivasa Rao as Allaudin
* Bramhanandam as Khan Dada
* Paresh Rawal as Subba Rao
* Tanikella Bharani as Manikyam & Gaayam Lawyer Saab (Duelrole)
* Sharat Saxena as Police officer
* Brahmaji as Brahmaji
* Benerjee as Gaayam Benerjee
* Sridhar as Mental Doctor
* Kadambari Kiran as Telephone Booth Owner
* Chinni Jayanth as Peon
* Kallu Chidambaram as Lawyer
* Tarzan as Jaggu
* Uttej as Yadagiri
* Narsingh Yadav as Narsingh
 

==Soundtrack==
{{Infobox album
| Name        = Money Money
| Tagline     = 
| Type        = film Sri
| Cover       = 
| Released    = 1995
| Recorded    = 
| Genre       = Soundtrack
| Length      = 20:51
| Label       = VMG Cassettes Sri
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}
 Sirivennela Sitarama Sastry. Music released on VMG Cassettes Audio Company.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 20:51
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Aarukotla Andhrulu SP Balu
| length1 = 4:21

| title2  = Em Kompa Mano
| length2 = 3:26

| title3  = What A Pity Chitra
| length3 = 4:21

| title4  = Left & Right
| extra4  = Ram Chakravarthy,Radhika
| length4 = 3:57

| title5  = Ooru Vaada 
| extra5  = Ram Chakravarthy,Chitra,Radhika,Swarnalatha 
| length5 = 4:46
}}

==Production==
Krishna Vamsi ghost-directed this film and he preferred his name uncredited in title credits. Ramuji wanted to make a sequel to Money, as no sequel had happened in India. And he wanted Vamsi to direct Money Money. Vamsi expressed his reservations that he did not want to launch himself with this kind of film. Then Ramu told him that he had to direct it. So Vamsi kept a condition that Vamsis name must not be there in the titles (ghost direction). He assured him that if he saw the finished product, he himself would ask for the credit. Then Money Money was started under Vamsis direction. He tried all types of techniques and experimented with it. In that film he shot one song with just four shots (Kota song em kompa munagadoi) and another one with 357 shots (Chakri and Surabhi duet Left and Right endukanta at Golkonda Fort). The Kota song took 6 hours to shoot and the Chakri duet took 6 days. Ramuji scolded Vamsi about his inconsistency. Ramuji gave him a choice to change his mind about the directors name. After Vamsis refusal, Ramuji requested Siva Nageswara Rao to lend his name and it was released. Vamsi detached himself from the film. 

==Others== Hyderabad
 
==References==
 

==External links==
* 
 

 
 
 
 
 
 


 