Honeymoon Hotel (1964 film)
 
{{Infobox film
| name           = Honeymoon Hotel
| image          = Honeymoon Hotel 64.jpg
| alt            =
| caption        =
| film name      =
| director       = Henry Levin
| producer       = Pandro S. Berman, Kathryn Hereford
| writer         = Harvey Bullock
| story          =
| based on       =  
| starring       =  Robert Goulet, Nancy Kwan, Robert Morse, and Jill St. John
| narrator       =
| music          = Walter Scharf
| cinematography = Robert J. Bronner
| editing        = Rita Roland
| studio         =  Metro-Goldwyn-Mayer 
| distributor    =
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Honeymoon Hotel is a 1964 American comedy film, directed by Henry Levin for  Metro-Goldwyn-Mayer. It stars  Robert Goulet, Nancy Kwan, Robert Morse, and Jill St. John. 

==Cast==
*Robert Goulet as Ross Kingsley
*Nancy Kwan as Lynn Hope
*Robert Morse as Jay Menlow
*Jill St. John as Sherry Nugent
*Keenan Wynn as Mr. Sampson
*Anne Helm as Cynthia Hampton
*Elsa Lanchester as Chambermaid Bernard Fox as Room Clerk
*Elvia Allman as Mrs. Sampson
*Sandra Gould as Mabel - Switchboard Operator David Lewis as Mr. Hampton
*Chris Noel as Nancy Penrose
*Dale Malone as Fatso
*Paulene Myers as Hogan - Rosss Secretary

==References==
 

==External links==
*  at the Internet Movie Database

 

 
 
 
 
 
 
 
 