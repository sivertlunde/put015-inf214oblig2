Second Best (film)
 
 
{{Infobox film
| name = Second Best
| director = Chris Menges
| producer = Arnon Milchan Sarah Radclyffe David Cook
| starring = William Hurt Chris Cleary Miles John Hurt Jane Horrocks Alan Cumming
| music = Simon Boswell
| photography = Ashley Rowe
| editing = George Akers
| studio = Regency Enterprises Alcor Films
| distributor = Warner Bros.
| released = 30 September 1994 
| country = United Kingdom
| language = English
}} David Cook, who also wrote the screenplay.

==Plot==
Graham Holt (William Hurt) is a single man aged 42, who attempts to adopt a 10-year-old boy. Graham is a sub-postmaster in Warwickshire, England. James Lennards (Chris Cleary Miles) is a disturbed child brought up in care. Graham wants a son, but James doesnt want another father.

Graham Holts emotional development has been smothered by his protective parents. His mother has died and he has to care for his bed-ridden father who has had a stroke.

James has been shunted from care home to foster home, causing disruption and being unable to relate to women. He only has a vague memory of his mother when he was aged 3, but has a vivid and romantic image of his father. James father is in prison and has spun tales of him being a mercenary in the few times theyve spent together on the run.

As Graham goes through the extended vetting process to be an adoptive parent, he spies James picture in a local journal and immediately selects him as his future son. He then has to attend classes and meet regularly with social workers. Graham and James meet and the embarrassed silences demonstrate Grahams nervousness and James manipulation of the situation. He knows what he has to do and how he has to behave if he is to have any chance of adoption. Graham is not sure how to behave, nor what to say. Asked by his social worker if hes sure he really likes children, he replies in the affirmative.

As the meetings continue, Graham starts to call James Jamie and he doesnt object. But he refuses to answer to Jimmy, the name his father used. Graham and his father were only close for one week in his life, when the two of them went away for a few days when Graham was aged 12. He shows pictures of that trip to James and breaks down and cries.

When James is shown the room that would be his if the "fostering with a view to adoption" went through, he performs a calculated action. He moves towards the man and places his arms around the mans waist. He then hugs him briefly, thus getting their first moment of physical contact over with quickly.

On a trip out in the car, Graham tries to explain that for the relationship to work, they should end up feeling a kind of love for each other. James asks "Would that be the same sort of love you and your father had for each other, that week you spent on holiday?"  Graham has to show that he has a support network and takes James to meet Lynn, a neighbour. James notices that Lynn would like to share her life with Graham and is now jealous. He also grasps that she is trying to guess if he has been sexually abused by Graham and in what manner.

Graham decides to take James camping and the boy helps choose the equipment they need to buy. Its a happy experience and Graham notices the flicker of a look from the boy that he interprets not as love, but as holding out the possibility of a deeper relationship between man and boy. James tells Graham he will need a large sleeping bag, but Graham realises later that this was a ruse when the boy climbs into his sleeping bag at night. Graham is petrified that the human warmth, the never before experienced closeness of another body, albeit a childs body, might excite him sexually. He is also worried that the embrace by an emotionally damaged child might be a re-enactment of some form of seduction he had experienced in the past. Graham experiences an overwhelming desire to shout for joy and return the boys embrace, but he remains still. Graham accepts that had James been a sexually aware child looking for excitement, then he might have been seduced, because of his need and longing for love, and might have "crossed over a line that he could not cross and hope to be his parent". He concludes that James recognised that Graham had to confront the "deadly rivals" of sexuality and the physicality which is not sexual but gives comfort and pleasure.

After a few weekends together, Graham takes James to meet Grahams fathers brother Uncle Turpin (John Hurt). Turpin questions Grahams motives, saying "It is all above board with you and the little lad, isnt it?" Turpin then tells Graham that his father had a homosexual relationship during the war and this explains some of the relationship problems Graham has had with his father.

As the man/boy relationship develops and has its ups and downs, nothing shakes Grahams belief that he is the one person who can make all the difference and that between them, they could change each other. James withdraws into himself as he is unable to meet his father, then his father unexpectedly turns up at the post office, thin and suffering from AIDS. The man is dying, and Graham invites him in and he and James care for him as he lives his last few months.

==Cast==
*William Hurt as Graham Holt
*Chris Cleary Miles as James
*John Hurt as Uncle Turpin
*Jane Horrocks as Debbie
*Alan Cumming as Bernard
*Jake Owen as James age 3

==Critical reception==
It has received 7 out of 10 stars on the Internet Movie Database website  and 6.8 out of 10 on the Rotten Tomatoes site. 

Channel 4 (who rated it 4.6 out of 5) wrote: "The powerful and sometimes crippling relationships between fathers and sons is the inspiration behind Menges worthy drama". 

==Location==
The film was largely made in the small Welsh town of Knighton, Powys|Knighton. {{cite web | title=IMDB |url=http://www.imdb.com/tt0111102/locations
|accessdate=23 November 2009}} 

==References==
 

 
 
 
 
 
 
 
 
 
 