Mark of an Angel
Mark 2008 film, directed by Safy Nebbou. Retitled Angel of Mine for its 2009 English-language DVD release. 

==Plot==
In a hospital Elsa Valentin gives birth to a daughter, but the baby reportedly dies in a fire. Seven years later she is divorcing her husband, and they fight over who gets custody over their 12-year-old son Thomas.

When Elsa collects her son from a party, she sees the 7-year-old girl Lola, who she thinks is her daughter, Lucie. First Elsa keeps this to herself, but she uses every opportunity to see Lola and keep into contact with her.  Elsa begins by approaching Claire, the mother, by feigning interest in buying Claires house, which is on sale, so that she can inspect the house and thus see Lola.  She later encounters Claire and Lola when they go ice-skating, where Elsa and Lola skate together and wind up falling.  At Lolas ballet recital, Elsa stands on the side of the stage behind the curtains to watch her perform; Lola does not mind, as she feels as though Elsa supports her more than Claire does. Claire confronts Elsa with her stalking behavior.   

Elsa reveals to Claire that she believes Lola is her daughter, though Claire refutes it.  When Claire refuses Elsas request for a DNA test, Elsa sneaks into the house to obtain a strand of Lolas hair.  She is caught by Claire and loses the hair.  When Claire tells her husband, Bernard, about the incident, he suggests that they go through with the DNA test as it will prove to Elsa that Lola is not her daughter.  Claire is forced to reveal the truth to Elsa and Bernard: seven years ago, her own daughter died in a fire from smoke inhalation.  At the hospital, she heard another baby crying and saw Elsa lying on the floor.  Believing that Elsa was dead, she took the baby and pretended it was Lola while pretending own dead child as Elsas baby.

Claire and Bernard agree that Lola should be returned to Elsa.  Reunited, Elsa and Lola go for a walk together; though surprised with the decision, the girl does not appear to mind.

==Cast==
*Catherine Frot: Elsa Valentin
*Sandrine Bonnaire: Claire Vigneaux
*Wladimir Yordanoff: Bernard Vigneaux
*Antoine Chappey: Antoine
*Michel Aumont: Alain Valentin
*Michèle Moretti: Colette
*Sophie Quinton: Laurence
*Geneviève Rey-Penchenat: Mme Corlet
*Héloïse Cunin: Lola
*Arthur Vaughan-Whitehead: Thomas
*Zacharie Chasseriaud: Jérémy

==References==
 

==External links==
* 

 
 
 


 