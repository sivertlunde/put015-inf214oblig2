Mad Max 2
 
 
 
{{Infobox film
| name = Mad Max 2
| image = Mad max two the road warrior.jpg
| alt = 
| caption = Theatrical release poster George Miller
| producer = Byron Kennedy
| writer = {{Plainlist|
* Terry Hayes
* George Miller
* Brian Hannant}}
| narrator = Harold Baigent
| starring = {{plainlist|
* Mel Gibson
}} Brian May
| cinematography = Dean Semler
| editing = {{Plainlist|
* David Stiven
* Michael Balson
* Tim Wellburn}} Kennedy Miller Productions
| distributor = Warner Bros.
| released =    
| runtime = 96 minutes  
| country = Australia
| language = English
| budget = Australian dollar|A$4.5 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p81-84 
| gross = {{Plainlist|
* A$10.8 million   
* United States dollar|US$23.7 million   
* $34.5 million  }}
}} George Miller. marauders follows an archetypical "western (genre)|Western" frontier movie motif, as does Maxs role as a hardened man who rediscovers his humanity when he decides to help the settlers.  Filming took part in locations around Broken Hill, in the outback of New South Wales. 
 leather bondage gear-wearing bikers; and its fast-paced, tightly edited and violent battle and chase scenes.

The films comic-book post-apocalyptic/punk style popularised the genre in film and   for Miller;   in 1985, with a fourth film in the series,  , slated for release on 15 May 2015.   

==Plot==
  The supplies supercharged V8 engine|V-8 Pursuit Special, scavenging for food, drink, and gas. His only companions are an Australian Cattle Dog and a rare functioning firearm–a sawn-off shotgun–for which ammunition is very scarce.
 Vernon Wells). Mack semi-truck, Max inspects a nearby autogyro for fuel. Its pilot, the Gyro Captain (Bruce Spence), ambushes Max and manages to capture him briefly before being overpowered. In exchange for his own life, the pilot guides Max to a small oil refinery nearby. Max arrives just as the facility is under siege by a gang of marauders riding a motley collection of cars and motorbikes. The gang leader, known as The Humungus (Kjell Nilsson), tries to convince the refinerys defenders to surrender the facility in exchange for safe passage out of the area.

A group of defenders attempts to break out of the compound, but the marauders capture, torture, and kill all but one of them, who is rescued by Max. Max makes a deal with the mortally-wounded sole survivor: he will bring him back to the compound in exchange for a tank of fuel. The man dies shortly after they enter the facility, and the facility leader, Pappagallo (  semi-truck, which is capable of hauling the tanker trailer that the facility inhabitants use to store the fuel they refine, in exchange for freedom, his vehicle, and as much fuel as he can take with him. The group accepts, but keeps Maxs car to ensure his cooperation. Max sneaks out, joining forces with the Gyro Captain to return to the truck.
 nitrous oxide-equipped car and runs Max off of the road, wrecking his vehicle and severely injuring him. The marauders kill Maxs dog with a crossbow, then attempt to siphon the fuel from the Pursuit Specials tanks, but trigger an explosive booby trap, which kills some of the attackers. Max, left for dead, is rescued by the Gyro Captain as he is trying to crawl back to the refinery.

With no other means of escape and with the refinerys defenders preparing to make their escape, Max insists on driving the repaired truck. He leaves the compound in the heavily-armoured truck, accompanied by a Feral child|"Feral Kid" (Emil Minty) he has befriended and by other inhabitants aboard as defenders. Pappagallo escorts him out in a captured marauder vehicle. The Humungus and most of his warriors pursue the tanker, leaving the remaining inhabitants free to flee the compound in a ramshackle caravan and buses, blowing up the refinery as they leave. Papagallo and the other defenders of the tanker, as well as numerous marauders, are killed during the chase and the Gyro Captain is shot down. Max and the Feral Kid find themselves alone, pursued by the marauders. Wez manages to board the truck and attack Max, but a head-on collision with Humungus car kills both Wez and Humungus. Max loses control of the tanker and it rolls off the road. As the injured Max carries the Feral Kid from the wrecked tanker, he sees not oil, but sand, leaking from the tank.
 oil drums inside their vehicles. With Papagallo dead, the Gyro Captain succeeds him as their chief and leads the settlers to the coast, where they establish the "Great Northern Tribe." Max remains alone in the desert, once again becoming a drifter. Years later, the Feral Kid, now the Northern Tribes new leader(voice by Harold Baigent), reminisces about the legend of the mythical "Road Warrior" (Max) who now exists only in distant memory.

==Cast==
* Mel Gibson as "Mad" Max Rockatansky, a former member of the Australian highway patrol called the Main Force Patrol (MFP). However, after a biker gang kills his family, he leaves the force and hunts down and kills all of the gang members. The trauma transforms him into the embittered, "burnt out...shell of a man". The narration describes him as The Road Warrior, who despite his acerbic nature, elects to assist the settlers in their plan. However once his part is complete, he becomes a drifter once again, choosing not to follow them North.
*   smile with advanced caries"; despite his quirks, however, the Captain proves to be wily and courageous. After the death of Pappagallo, the Gyro Captain succeeds him as the leader of the settlers.   
* Emil Minty as the Feral Kid, a boy who lives in the wasteland near the refinery settlement. His speaks only in growls and grunts. The boy wears shorts and boots made from hide, and defends himself with a lethal metal boomerang which he can catch using an improvised mail glove.  The narration of the opening and closing sequences, provided by Harold Baigent, proves in the closing sequence to be that of the Feral Kid, grown to adulthood by then, and remembering the circumstances of his youthful encounter with "Mad" Max.
* Michael Preston as Pappagallo, the idealistic leader of the settlers in the barricaded oil refinery. Even though the settlers compound is besieged by a violent gang, Pappagallo "...carries the weight of his predicament with swaggering dignity." 
* Virginia Hey as the Warrior Woman, an Amazons|Amazon-like female member of the settlers who initially distrusts Max.
* Kjell Nilsson as The Humungus, the violent, yet charismatic and articulate leader of a "vicious gang of post-holocaust, motorcycle-riding vandals" who "loot, rape, and kill the few remaining wasteland dwellers". Announced by the Toadie as the "warrior of the wasteland, the Lord Humungus,   the ayatollah of rock-and-rollah", The Humungus "malevolence courses through his huge pectorals,   pulses visibly under his bald, sutured scalp."  The Humungus face is never seen, as he wears a hockey goalies mask. In a 1985 interview with Danny Peary, Miller posited that he thought the character "was a former military officer who suffered severe facial burns," and who "might have served in the same outfit as his counterpart, Pappagallo."  . Thefilmist.wordpress.com. Retrieved on 18 November 2011.  Vernon Wells as Wez, a mohawked, leather-clad biker who serves as The Humungus lieutenant in the gang. Vincent Canby, the New York Times reviewer called the Wez character the "most evil of The Humunguss followers...  huge brute who rides around on his bike, snarling psychotically."    In the same Danny Peary interview, Miller states the characters of Wez and Max are near mirror images of each other, with each being chained by the leaders of their respective camps, and who both find themselves spurred on by the death of a loved one somewhere in their past, in Wezs case the relatively recent death of The Golden Youth at the hand of The Feral Kid.  Empire (magazine)|Empire magazine listed Wez as the greatest movie henchman of all time.  stole as a hat and has many automobile badges and hood ornaments on his clothes. His behavior with The Humungus and Wez make him a classic sycophant. Toadie takes pleasure in molesting helpless prisoners, but the gang has little respect for him.
* Arkie Whiteley as The Captains Girl, a beautiful young woman among the settlers who became the Gyro Captains lover.
* Moira Claux as Big Rebecca, a female warrior among the settlers who wields a bow and arrow.
* David Downer as Nathan, a member of the settlers who tries to escape the settlement and is fatally wounded by some of the Humunguss bikers.

==Production== novelisation of Mad Max and, together, they worked on a screenplay for another film, a special effects horror movie. However, after a while, Miller became more interested in doing a sequel to Mad Max, as a larger budget would allow him to be more ambitious. He hired the old Metro Cinema in Kings Cross  , and Brian Hannant came on board as co-writer and second unit director. Miller says that he was greatly influenced by the films of Akira Kurosawa. 

Principal photography took place near Broken Hill over twelve weeks. The original cut was a lot bloodier and more violent but it was cut down heavily by Australian censors. There werent just some shots which were cut but entire scenes and sequences were deleted completely or edited for an "M" rating. When it was submitted to the Motion Picture Association of America|MPAA, two additional scenes (Wez graphically pulling an arrow out of his arm and a close-up shot of him pulling the boomerang out of his dead boyfriends head) were cut down. Although there is a version that includes MPAA cuts, there never was any full uncut version with pre-MPAA cuts included.    

==Release==
When Mad Max was released in 1980 in the United States, it did not receive a proper release from its distributor, American International Pictures. AIP was in the final stages of a change of ownership after being bought by Filmways|Filmways, Inc. a year earlier. AIPs then-current problems affected the release of the film and its box office in the U.S., although Mad Max proved much more successful when released internationally.  Warner Bros. decided to release Mad Max 2 in the United States, but they recognized that the first film was not popular in North America. Although the original Mad Max was becoming popular through cable channel showings, Warner Bros. decided to change the name of its sequel to The Road Warrior. The advertising for the film, including print ads, trailers, and TV commercials, did not refer to the Max character at all, and all shied away from the fact that the film was a sequel. For the majority of viewers, their first inkling of Road Warrior being a sequel to Mad Max was when they saw the black and white, archival footage from the previous film, during the prologue.

The film was a commercial success, earning $3.7 million in rentals in Australia. As The Road Warrior in North America, it was a greater success. The film earned $11.3 million in rentals and $23.6 million in grosses.   , intact for that films American release.

===Critical reception===
Mad Max 2 received universal critical acclaim and is regarded by many as one of the best films of 1981.   The film holds a List of films with a 100% rating on Rotten Tomatoes|100% rating on Rotten Tomatoes as of 9 November 2014.  Film critic Roger Ebert of the Chicago Sun-Times gave the film three-and-a-half stars out of four and praised its "skillful filmmaking," and called it "a film of pure action, of kinetic energy", which is "one of the most relentlessly aggressive movies ever made". While Ebert points out that the film does not develop its "vision of a violent future world ... with characters and dialogue", and uses only the "barest possible bones of a plot", he praises its action sequences. Ebert calls the climactic chase sequence "unbelievably well-sustained" and states that the "special effects and stunts...are spectacular", creating a "frightening, sometimes disgusting, and (if the truth be told) exhilarating" effect.   

In his review for The New York Times, Vincent Canby wrote, "Never has a films vision of the post-nuclear-holocaust world seemed quite as desolate and as brutal, or as action-packed and sometimes as funny as in George Millers apocalyptic The Road Warrior, an extravagant film fantasy that looks like a sadomasochistic comic book come to life".  In his review for Newsweek, Charles Michener praised Mel Gibsons "easy, unswaggering masculinity", saying that "  hint of Down Under humor may be quintessentially Australian but is also the stuff of an international male star".   
 Peckinpah and Sergio Leone|Leone".    Pauline Kael called Mad Max 2 a "mutant" film that was "...sprung from virtually all action genres", creating "...one continuous spurt of energy" by using "jangly, fast editing". However, Kael criticised director George Millers "attempt to tap into the universal concept of the hero", stating that this attempt "makes the film joyless", "sappy", and "sentimental".
 exploitation cinema at its most inventive."

Richard Scheib calls Mad Max 2, "one of the few occasions where a sequel makes a dramatic improvement in quality over its predecessor." He calls it a "kinetic comic-book of a film," an "exhilarating non-stop rollercoaster ride of a film that contains some of the most exciting stunts and car crashes ever put on screen." Scheib states that the film transforms the "post-holocaust landscape into the equivalent of a Western frontier," such that "Mel Gibsons Max could just as easily be Clint Eastwoods tight-lipped Man With No Name" helping "decent frightened folk" from the "marauding Redskins". 

===Awards=== Academy of Best Director, Best Writing, Best Costume Best Actor Best Supporting George Miller Best Foreign Film. The film was also recognised by the Australian Film Institute, winning awards for best direction, costume design, editing, production design and sound. It received additional nominations for the cinematography and musical score. 

===Legacy===
  Mad Max series of films, with their emphasis on dystopian, apocalyptic, and post-apocalyptic themes and imagery, have inspired some artists to recreate the look and feel of some aspect of the series in their work. As well, fan clubs and "road warrior"-themed activities continue into the 21st century. In 2008, Mad Max 2 was selected by Empire Magazine|Empire magazine as one of The 500 Greatest Movies of All Time.  Similarly, The New York Times placed the film on its Best 1000 Movies Ever list.  Entertainment Weekly ranked Mad Max 2 93rd on their 100 Greatest Movies of All Time in 1999, 41st on their updated All-Time 100 Greatest Films in 2013, and the character Mad Max as 11th on their list of The All Time Coolest Heroes in Pop Culture. 

The film now has a permanent legacy in the small town of Silverton which is 25 kilometres from Broken Hill in NSW Australia. The museum was established by Adrian and Linda Bennett and is the only one of its kind in the world. Its location is due to much of the film being made around Silverton on the Mundi Mundi Lookout Road. Scenes were also shot at "the Pinnacles" which is West of Broken Hill and the famous scene where the Interceptor rolls over and is destroyed in a fire ball by Toadies trying to drain the precious fuel was shot on the Menindee Road just out of Broken Hill.

==Soundtrack==
{{Infobox album  
| Name        = Mad Max 2
| Type        = Soundtrack Brian May
| Cover       =
| Released    = 1982
| Recorded    =
| Genre       = Film music
| Length      = 35:08
| Label       = Atlantic Records
| Producer    = Chris Kuchler, Tom Null
| Last album  = Race for the Yankee Zephyr (1981)
| This album  = Mad Max 2 (1982)
| Next album  = Breakfast in Paris (1982)
| Misc        = {{Extra chronology Mad Max
| Type        = Film Mad Max (1979)
| This album  = Mad Max 2 (1982) Mad Max Beyond Thunderdome (1985)
}}}}
{{Album reviews rev1 = AllMusic rev1score =    rev2 = AVForms rev2score =   
}} Brian May. CD on the Varèse Sarabande label, catalog number VCD 47262. The music is presented out of order and sometimes retitled; part of the track titled "Finale and Largo" is actually the main title, "Montage" was written for the truck chase scene (and as such would fit between "Break Out" and "Largo") and the "Main Title" is actually the post-title montage. The sound effects suite that concludes the disc has two cues, "Boomerang Attack" and "Gyro Flight", that do not appear elsewhere on the album (the former is actually presented without any overlaying effects).

The soundtrack begins with the music for the "Montage/Main Title" sequence, which gives the back-story to the descent into war and chaos. The next selections accompany the action-packed sequences as Max and the settlers battle with the gang ("Confrontation"; "Marauders Massacre", "Max Enters Compound"; "Gyro Saves Max"; and "Break Out"). The final tracks include the "Finale and Largo" and the "End Title" music, which is used while the narrator describes the settlers escape to the coast to start a new life. The recording also includes a suite of special effects sounds, such as The Feral Kids "Boomerang Attack"; "Gyro Flight"; "The Big Rig Starts"; "Breakout"; and the climactic effects for "The Refinery Explodes", when the booby-trapped oil refinery turns into a fireball.

{{Tracklist
| headline    = Track listing
| title1      = Montage/Main Title
| length1     = 4:53
| title2      = Confrontation
| length2     = 2:32
| title3      = Marauders Massacre
| length3     = 3:13
| title4      = Max Enters Compound
| length4     = 4:08
| title5      = Gyro Saves Max
| length5     = 3:55
| title6      = Break Out
| length6     = 3:26
| title7      = Finale and Largo
| length7     = 5:06
| title8      = End Title
| length8     = 3:19
| title9      = SFX Suite
| length9     = 4:36
| note9       = Boomerang Attack/Gyro Flight/The Big Rig Starts/Break Out/The Refinery Explodes/Reprise
}}

==See also==
Seven Sisters (oil companies) (reference in the movie to the conspiracy theory)

==References==
 

==External links==
 
*  
*  at Oz Movies
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 