A Chump at Oxford
 
 
{{Infobox film
  | name = A Chump at Oxford
  | image = L&H_Chump_at_Oxford_1940.jpg
  | caption        = Theatrical release poster
  | director =  Alfred J. Goulding
  | producer = Hal Roach Jr.   Hal Roach Felix Adler   Harry Langdon Charlie Hall
  | music = Marvin Hatley
  | cinematography = Art Lloyd
  | editing = Bert Jordan
  | distributor = United Artists
  | released =   streamliner version) 62 41" (extended version)
  | country = United States
  | language = English
  | budget =
}} streamliner featurette at forty minutes long, twenty minutes of footage largely unrelated to the main plot were later added for the European distribution. The longer version is the one most often seen today.

== Plot ==
Stan and Ollie are down to their last six bucks and call a lift to a job agency to find work. A City Water Dept. truck driver offers them a lift and drenches them with water as a joke and leaves them behind. They finally arrive in a badly damaged car that has been towed away. At the job agency a call comes from Mrs. Vandeveer looking for a maid and butler to help at a dinner party she is holding that night. Ollie tells the receptionist they can fill the post and to leave it to them. They arrive and Stan is dressed in drag (clothing)|drag, pretending to be the maid "Agnes". 
 From Soup to Nuts folks; come and get it". Stan is told to take the cocktails; and instead of clearing them away, he drinks them and becomes drunk. Ollie gets the guests to sit down with the men on one side and the women on the other side of the table. Mr. Vandeveer tells Ollie to change the seating arrangement and Ollie begins to move the guests around for a while until Mr. Vandeveer gets impatient and tells them to sit anywhere they like at the table. Mr. Vandeveer then tells the drunken Stan to "serve up the salad without dressing" so Stan takes off his clothes and serves the salad in his underwear. Seeing this, Mr. Vandeveer angrily storms into another room to take a shotgun. Mrs. Vandeveer arrives, having changed her dress, and faints at the sight of Stan. Mr Vandeveer returns, gun in hand, and chases Stan and Ollie out of the house. A single gunshot is heard and Mr. Vandeveer returns, followed by a policeman, who tells him, "why dont you be more careful; you almost blew my brains out?!" When the cop turns to leave, the seat of his pants have a large jagged hole ripped in them, revealing smouldering undershorts.
 reward by suggesting that they could have a job in his bank. When Oliver mentions they would not be much use since he and Stan do not have an education, the bank president expands on their goal to attend night school by saying, "If its an education you want, you shall have the finest education money can buy." He enrolls Stan and Ollie at Oxford University in England, and they depart the U.S. towards Oxford by steamship.

When Stan and Ollie arrive at the university, the snobby undergraduate students, led by the mischievous Johnson (Gerald Rogers) decide to give them the "royal initiation," which involves a number of pranks. They are sent off into a maze in order to get a pass to see the dean and quickly became lost. One of the students (Henry Borden) dresses as a ghost in order to frighten Stan and Ollie, and while they sit on a bench to sleep, the ghosts hand comes through the hedge to help Stan smoke his pipe and cigar (substituting for Stans actual hand).
 dean and gives Stan and Ollie the real Deans quarters to live in. They make themselves at home, only to be confronted by the Dean.  The deans outrage is taken by the boys as "another rib". In their efforts to boot him out,the prank is uncovered, and Johnson is due to expulsion (academia)|expelled. Before this happens, the students decide to run Stan and Ollie out so they cant give evidence against Johnson. The boys are taken to their real quarters where Meredith the valet recognises Stan as Lord Paddington, the "greatest athlete and scholar the university ever had". He says that Lord Paddington had lost his memory when the window fell on his head and wandered from campus. Stan and Ollie dismiss his story as a "dizzy spell".  Hardy explains that he has always known Stan as the dumbest guy he ever met. 

The students arrive and decide to throw Stan and Ollie out the window. Stan and Ollie decide to escape through the window and in doing so the window falls on Stans head, which transforms him back into Lord Paddington. When the students accuse him of "squealing", he becomes angry and his ears wiggle—something that occurs whenever Lord Paddington becomes angry, according to Merediths story—after which he throws all of the students out of the window, into a safety net held out to break the fall of the intended victims, Stan & Ollie. However, Stan does not remember Ollie any longer so he becomes furious when Ollie tells him of his former life and throws Ollie out the window as well.  Even the Dean hurries upstairs only to take a trip out the window into the safety net .

Lord Paddington takes pity on Ollie and employs him to be his personal valet, Lord Paddington even gets a phone call from Albert Einstein, asking to see him about a problem with his Theory of Relativity! That and all the athletic trophies, show the transformed Stan to be now super-human in intellect & body. He calls Ollie by the nickname "Fatty" and criticises him, which makes Ollie so angry he quits his job and storms out. Stan hears students come to cheer him outside and as he looks out of the window it falls on him once again, returning him back to his usual self.  Hardy storms back in, still in a tirade about the way Lord Paddington treated him and stops only when he realizes that Stan is now back to his old self. Friends again, they leave.

== Film production details ==
* The dinner party scenes are those omitted from the original American release and are a partial remake of their 1928 silent film From Soup to Nuts.
* The dinner party ends in the same way as in their 1927 film Slipping Wives.
* As Lord Paddington, Stan Laurel employs an upper-class received pronunciation accent, the only time he affected a voice different from "Stan" on film.

== Cast ==
 
 
* Stan Laurel as Stan / Lord Paddington
* Oliver Hardy as Ollie James Finlayson as Mr. Vanderveer
* Anita Garvin as Mrs. Vanderveer
* Forbes Murray as Banker
* Wilfred Lucas as Dean Williams
* Forrester Harvey as Meredith
* Peter Cushing as Student
* Gerald Rogers as Student Johnson Charlie Hall as Student
* Victor Kendall as Student
* Gerald Fielding as Student
* Eddie Borden as Student Ghost Frank Baker as Deans Servant Herbert Evans
* Evelyn Barlow
* Louise Bates
* Harry Bernard
* Stanley Blystone Tom Costello
* Richard Cramer
 
* Jean De Briac
* Mildred Gaye
* Alec Harford
* Jack Heasley
* Jewel Jordan Robert Kent
* Rex Lease
* Lois Lindsay
* Sam Lufkin
* George Magrill
* James Millican Edmund Mortimer
* Doris Morton
* Edgar Norton
* Vivien Oakland Jack Richardson
* Ronald R. Rondell
* Elmer Serrano
* Al Thompson
* Bobby Tracy
 

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 