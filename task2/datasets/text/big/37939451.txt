Cabin Fever: Patient Zero
{{Infobox film
| name           = Cabin Fever: Patient Zero
| image          = Cabin Fever-Patient Zero.jpg
| alt            = 
| caption        = 
| director       = Kaare Andrews
| producer       = Evan Astrowsky Jaime Pina
| writer         = Jake Wade Wall
| starring       = Sean Astin Jillian Murray Currie Graham Lydia Hearst
| music          = Kevin Riepl
| cinematography = Norm Li
| editing        = Michael P. Mason
| studio         = Film002 Indomina Group
| distributor    = Image Entertainment
| released       =  
| runtime        = 91 min. 99 min. (Uncut)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Cabin Fever franchise and acts as a prequel to the previous two films. The film was shot in the Dominican Republic. It was released on June 26, 2014.

==Plot==
The story centers about the initial outbreak of the virus. On a remote island, an immune patient named Porter is being held in a lab by scientists trying to create a vaccine. Porter forces a lockdown of the station and spreads the virus outside of the lab by infecting a mouse.

When four friends on a bachelor party boat ride land on the island, two of them are infected by swimming in the contaminated water. Searching for help, they make their way to the science lab but find everyone inside infected or dead. The two surviving friends leave the island with Porter. Tricking them into drinking contaminated water and destroying the motor, Porter is the only person to leave the island in the end.

==Cast==
* Sean Astin as Porter
* Currie Graham as Dr. Edwards
* Mitch Ryan as Marcus
* Jillian Murray as Penny
* Ryan Donowho as Dobs
* Brando Eaton as Josh
* Lydia Hearst as Bridgitt
* Solly Duran as Camila
* Claudette Lali as Kate
* Juan Papo Bancalari as Mr. Arias
* Marie Michelle Bazile as Elderly Woman / Witch
* Roberto Linval as Jorge
* Magio Mojica as Captain
* Omar Augusto Luis as Drug Dealer
* Juan Burgos as Driver #1
* Juan Colon as Driver #2
* Jorge Kiko Loubriel as Driver #3
* Jerson Guerrero as Driver #4
* Jorge Manzano as Driver #5
* Dennis Gomez as Driver #6
* Jorge Pinedo as Driver #7
* Amaury De Jesus as Driver #8
* Expedito Lantigua as Driver #9

==Canceled sequel and planned remake==
A fourth entry in the series, titled Cabin Fever: Outbreak, was planned to be filmed back-to-back with this film, but it was eventually scrapped. As of April 2014, it has been reported that the people behind this film are now actively working on a Cabin Fever remake instead of the sequel. 

== Soundtrack ==
{{Infobox album  
| Name       = Cabin Fever: Patient Zero Original Motion Picture Soundtrack
| Type       = soundtrack
| Artist     = Kevin Riepl
| Cover      = Cabin Fever, Patient Zero Soundtrack cover.png
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = 
| Length     =  
| Label      = Sumthing Else Music Works
| Producer   = Kevin Riepl
| Last album = 
| This album = 
| Next album = 
}}

Cabin Fever: Patient Zero Original Motion Picture Soundtrack is the soundtrack album for the movie, released on July 22, 2014 under the record label Sumthing Else Music Works.  The music was composed by Kevin Riepl, who has previously worked with Kaare Andrews on The ABCs of Death.   

===Track listing===
{{Track listing
| collapsed       = no
| headline        = 
| extra_column    = 
| total_length    = 77:47

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          = Cabin Fever
| writer1         = Kevin Riepl
| length1         = 2:05

| title2          = Patient Zero
| writer2         = Kevin Riepl
| length2         = 3:22

| title3          = Just Believer 
| writer3         = Kevin Riepl
| length3         = 1:43

| title4          = Three Blind Mice
| writer4         = Kevin Riepl
| length4         = 3:54

| title5          = Culture Time
| writer5         = Kevin Riepl
| length5         = 2:16

| title6          = Virgin Beach
| writer6         = Kevin Riepl
| length6         = 1:50

| title7          = Swim with the Fishes
| writer7         = Kevin Riepl
| length7         = 2:19

| title8          = Speedy Virus
| writer8         = Kevin Riepl
| length8         = 4:55

| title9          = Its the Bud
| writer9         = Kevin Riepl
| length9         = 1:30

| title10         = Rubs the Lotion On
| writer10        = Kevin Riepl
| length10        = 2:21

| title11         = Cabin Beaver
| writer11        = Kevin Riepl
| length11        = 2:17

| title12         = Looking for Help
| writer12        = Kevin Riepl
| length12        = 1:28

| title13         = A Burning Lotion
| writer13        = Kevin Riepl
| length13        = 1:59

| title14         = Bunker Down
| writer14        = Kevin Riepl
| length14        = 4:37

| title15         = A Lil Blood
| writer15        = Kevin Riepl
| length15        = 1:44

| title16         = Pennyskin
| writer16        = Kevin Riepl
| length16        = 0:59

| title17         = Two Guys, One Door
| writer17        = Kevin Riepl
| length17        = 3:14

| title18         = Dobs Freaks
| writer18        = Kevin Riepl
| length18        = 3:16

| title19         = Infected
| writer19        = Kevin Riepl
| length19        = 4:58

| title20         = Three Blind Guys
| writer20        = Kevin Riepl
| length20        = 3:12

| title21         = Dr. Edwardss Plan
| writer21        = Kevin Riepl
| length21        = 4:26

| title22         = Split Up
| writer22        = Kevin Riepl
| length22        = 0:54

| title23         = To the Beach
| writer23        = Kevin Riepl
| length23        = 2:59

| title24         = Girl Fight!!
| writer24        = Kevin Riepl
| length24        = 4:23

| title25         = Bunker Escape
| writer25        = Kevin Riepl
| length25        = 1:48

| title26         = Porter Patrol
| writer26        = Kevin Riepl
| length26        = 3:53

| title27         = Porter Mouse
| writer27        = Kevin Riepl
| length27        = 2:42

| title28         = Porter Crossing
| writer28        = Kevin Riepl
| length28        = 2:43
}}

==References==
 

==External links==
*  
*   Geexplosion.com
*   El Otro Cine (Spanish)

 
 
 
 
 
 


 