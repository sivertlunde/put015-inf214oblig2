Photograbber
  Pascal Tosi.

== Synopsis ==

Set in 1952, this is a tale about a unique camera that captures reality. When the photos rebel against the photographer (a charming, henpecked toymaker) a battle begins.

== Cast and characters ==

*Michel Crémadès (Marcel)
*Christine Melcer (Carole)
*Catherine Cyler (Marie)

== Technical team ==
 Pascal Tosi
* Produced by : Aton Soumache
* Director of photography : Juan Diego Solanas
* Editing : Sylviane Gozin
* Set decorator : Frank Muller
* Executive in Charge of Production : Jean-Frédéric Samie
* Digital visual effects director : Hugues Namur
* Special Effects : Sparx*

== Awards ==

* Mexico International Film Festival (June 2007) : Best director
* Tahoe-Reno International Film Festival (August 2006) : Best of the festival - New Foreign Director
* ReelHeART International Film Festival (June 2006) : 1st place short film
* Foursite Film Festival (March 2006) : Best Foreign
* Carolina Film and Video Festival (February 2006) : Kodak award for Cinematography
* Fano International Film Festival (October 2005) : First prize video
* Hertfordshire International Film Festival (October 2005) : Best Cinematography
* Festival International du Film - La Paplemühle (October 2005) : Prix du meilleur scénario court métrage
* FAIF Film Festival (October 2005) : Audience award - Best Underground
* Effets stars - Festival international des Effets spéciaux (September 2005) : Prix du jury Graulen - Live SFX
* Festival des nations - Ebensee (June 2005) : Golden bear
* WorldFest-Houston International Film Festival (April 2005) : Platinum Remi award
* ION international short film & animation (October 2004) : ION international short film of the year award

== Official selections ==

* Great Lakes Independent Film Festival USA (September 2006)
* Newport Beach Film Festival USA (April 2006)
* Aarhus Festival of Independent Arts AFIA Denmark (April 2006)
* Belgrade Documentary and Short Film Festival Serbia (April 2006)
* Semana Internacional de Cine Fantastico  Malaga, Spain (March 2006)
* Manchester International Short Film Festival - Kini Film UK (February 2006)
* Festival of European Film on Wheels Turkey (November 2005)
* Starz Denver International Film Festival USA (November 2005)
* Golden Horse Film Festival - Digital Shorts Competition Taiwan (November 2005)
* Los Angeles Latino International Film Festival USA (October 2005)
* Screamfest Horror Film Festival USA (October 2005)
* Lund International Fantastic Film Festival Sweden (September 2005)
* International Short Film Festival in Drama Greece (September 2005)
* Los Angeles International Short Film Festival USA (September 2005)
* European Film Festival Alpinale Austria (August 2005)
* Puchon International Fantastic Film Festival South Korea (July 2005)
* Festival de Huesca Spain (June 2005)
* Athens International Film & Video Festival Ohio, USA (May 2005)
* Fantasporto Portugal (March 2005)
* Santa Barbara International Film Festival USA (January 2005)
* Foyle Film Festival - Seagate Northern Ireland (November 2004)
* Saint Louis International Film Festival USA (November 2004)

== External links ==
*  
*  
*  

 
 
 
 


 
 