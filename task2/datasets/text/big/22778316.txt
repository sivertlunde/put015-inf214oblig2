Arcadia Lost
{{Infobox film
| name           = Arcadia Lost
| image          =
| image size     =
| caption        =
| director       = Phedon Papamichael Jr. James Ivory 
| writer         = David Ariniello
| narrator       =
| starring       = Haley Bennett Carter Jenkins Nick Nolte
| music          = Michael Brook
| cinematography = Phedon Papamichael Jr.
| editing        = Philip Harrison
| studio         = Chambers Productions Top Cut
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Arcadia Lost is a 2010 American drama film directed by Phedon Papamichael Jr. The film both takes place and was filmed in Greece and, specifically, in Poulithra, one of the last to remain, tranquil coastal villages with stone traditional houses in the area of Leonidio.

==Plot==
After a car accident, two teenagers are left stranded in rural Greece. Charlotte has a deep desire to find connection, but hides behind a sullen disposition and her new-found sexual power. Sye, her recent stepbrother, is introverted, hiding behind his camera and caustic wit. As the two wander the dusty roads and staggering beauty of Greece, they come across Benerji, an expatriate American. With no other alternative, they reluctantly accept him as their guide. The three begin an adventurous journey toward the mystic waters on the sacred Mount Parnonas. Their journey takes them through a landscape both ancient and modern. Events force them to confront the truth of their past and the frightening, beautiful reality of their present.

==Cast==
*Nick Nolte as Benerji
*Haley Bennett as Charlotte
*Carter Jenkins as Sye
*Dato Bakhtadze as Gorgo
*Lachlan Buchanan as Raffi
*Renos Haralambidis as hotel manager
*Anthony Burk as cook
*Alex Zorbas as Desmond
*Alexandra Pavlidou as Alisa
*David Ariniello as Charlottes dad James Ivory as wedding photographer
*Nikolas Marmaras as waiter
*Olga Kyprotou as woman in white
*Anastasia Poutsela as old woman
*Arietta Valmas as girl in white
*Vassilis Drossos as truck driver

==External links==
*  
*   
*  

 
 
 
 
 