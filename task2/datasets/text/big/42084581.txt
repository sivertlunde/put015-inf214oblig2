The Interrupted Honeymoon
{{Infobox film
| name =  The Interrupted Honeymoon
| image =
| image_size =
| caption =
| director = Leslie S. Hiscott Herbert Smith 
| writer =  Michael Barringer   Wyndham Brown   Neil Tyfield
| narrator = Jane Carr Jack Hobbs   Claude Hulbert
| music = 
| cinematography = George Stretton
| editing =  British Lion 
| distributor = British Lion
| released = June 1936
| runtime = 72 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jane Carr, Jack Hobbs. It was made at Beaconsfield Studios.  In the film, a couple returning home from a honeymoon in Paris find that their flat has been taken over by their friends.

==Cast== Jane Carr as Greta 
* Helen Haye as Aunt Harriet  Jack Hobbs as George  David Horne as Colonel Craddock 
* Claude Hulbert as Victor 
* Martita Hunt as Nora Briggs 
* Glennis Lorimer as  Edith Hobson 
* Wally Patch as Police Constable 
* Francis L. Sullivan as Alphonse 
* Hugh Wakefield as Uncle John 
* Hal Walters as Valet 
* Robb Wilton as Henry Briggs

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 