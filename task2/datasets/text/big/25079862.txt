One More River
{{Infobox Film
| name           = One More River
| image          = "One_More_River"_(1934).jpg
| image_size     = 
| caption        = 
| director       = James Whale
| producer       = Carl Laemmle R. C. Sheriff
| writer         = John Galsworthy(novel)
| starring       = Diana Wynyard Colin Clive Mrs. Patrick Campbell
| music          = W. Franke Harling
| cinematography = John J. Mescall
| editing        = Ted J. Kent
| studio         = Universal Pictures
| distributor    = Universal Pictures 
| released       = August 6, 1934
| runtime        = 85 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
One More River is a 1934 film directed by James Whale. It was produced and distributed by Universal Pictures and starred Colin Clive, Diana Wynyard and stage actress Mrs. Patrick Campbell in one of her very few films. The film also marked Jane Wyatts screen debut.  It is based on a 1933 novel by John Galsworthy.    
 Universal snapped up the film rights to this best seller and gave the prestigious project to its star director, James Whale.    

Filming from May to July 1934, One More River was one of the first films to be subjected to the exacting censorship of the Production Code Administration, which was brought in that year.  

==Plot== physically and emotionally abusive toward her, and one day she can take no more, and walks out of the relationship. Lady Clare books passage on a ship, where she is befriended by a kind and handsome young man, Tony Croom (Frank Lawton). Though their relationship remains strictly Platonic love |platonic, Tony displays strong feelings for Lady Clare, which are duly noted by a private detective hired by Sir Gerald to keep tabs on his wife. Sir Gerald threatens to paint Lady Clares relationship with Tony in an unflattering light in court, this being a time when divorce was considered scandalous, especially among Englands "Privilege (social inequality)|privileged" classes.  

==Cast==
*Diana Wynyard - Claire Corven
*Colin Clive - Sir Gerald Corven
*Frank Lawton - Tony Croom
*Mrs. Patrick Campbell - Lady Mont
*Jane Wyatt - Dinny Cherrell Reginald Denny - David Dornford
*C. Aubrey Smith - Gen. Charwell
*Henry Stephenson - Sir Laurence Mont
*Lionel Atwill - Brough
*Alan Mowbray - Forsythe
*Kathleen Howard - Lady Charwell
*Gilbert Emery - The Judge
*E. E. Clive - Chayne
*Robert Greig - Blore
*J. Gunnis Davis - Benjy
*Tempe Pigott - Mrs. Purdy

==Reception== The Invisible Man," have fashioned a grand picture out of the late John Galsworthys last novel."  {{cite web|url=http://www.nytimes.com/movie/review?res=9B0DE1D8153FE53ABC4852DFBE66838F629EDE&module=Search&mabReward=relbias%3Aw|title=Movie Review -
  One More River - Diana Wynyard, Frank Lawton and Colin Clive in a Film of John Galsworthys Last Novel. - NYTimes.com|publisher=}} 
 The Invisible Man, and Bride of Frankenstein (1935). Colin Clives first appearance, in a series of four shots that showcase his stiff swagger and superior scowl, is as devastating an entrance as any ever accorded a screen villain. The courtroom sequence is an astounding piece of filmmaking, with Whales elaborately mobile camera accentuating the vastness of the space and setting off the rich contrasts in acting styles among the participants."  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 