The Viking (1931 film)
:For the 1928 Technicolor MGM film, see The Viking (1928 film).
{{Infobox film
| name           = The Viking
| image          = The_Viking_film.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = George Melford Varick Frissell
| producer       = Varick Frissell
| story          = Garnett Weston
| writer         = Garnett Weston (scenario and dialogue)
| narrator       = Wilfred Grenfell Robert Bartlett
| music          = 
| cinematography = Alfred Gandolfi Maurice Kellerman Alexander G. Penrod
| editing        = H. P. Carver
| studio         = James Dixon Williams|J.D. Williams Newfoundland-Labrador Film Company
| distributor    = J.D. Williams
| released       =  
| runtime        = 70 min. Newfoundland United States
| language       = English
| budget         = 
| gross          = 
}}
 1931 Dominion American adventure SS Viking during filming, in which many members of the crew, including producer Varick Frissell, were killed. 

==Plot==
The film is set on the coasts of Newfoundland and focuses on the rivalry between a seal hunter and a local jinx. Worried that the jinx may attempt to steal his girlfriend, the seal-hunter forces the alleged coward to accompany him on an Arctic expedition. They both end up in a hunting party on the ice floes and eventually find themselves stranded. The hunter tries to kill the jinx, but the snow blinds him and he misses. 

Despite the murder attempt, the jinx helps the hunter back to the safety of their ship called The Viking. On recovering his sight, the hunter gains new respect for the jinx and vows that he will beat senseless any man who derides the character of his new friend.

==Production==
American-born producer Varick Frissells previous short films, The Lure of Labrador and The Swilin Racket (also known as The Great Arctic Seal Hunt), gave him interest to make a full-length feature entitled Vikings of the Ice Field. Paramount Pictures put up $100,000 to finance the production while insisting that Hollywood personnel be used.  Frissell hired director George Melford, who went to McGill University in Montreal and had experience in filming Canadian subjects previously.  
 SS Viking Horse Islands. On March 15, 1931, during this second voyage to obtain more footage for the film, Frissell, Alexander Penrod, and 25  or 26 other film crew members were killed in an explosion while trying to film an iceberg. Rist 2001, p.230  Rhodes 2001, p. 95   Some of the survivors made the over-ice trek to the Horse Islands, while some were rescued by other vessels dispatched to the area. 

Despite the fatal accident, the film was completed and released in June 1931. The title was changed from White Thunder to The Viking. A French-language version Ceux du Viking was released in 1932. 

==Reception==
Reviews for the film varied, while the story was generally berated. The New York Times referred to the films story as "sketchy" while the Theater Guild Magazine found the story "melodramatic" finding the screenplay uninteresting in comparison to the cinematography. The Film Daily gave a negative review noting the "weakness" of the story. 

==Notes==
 

===References===
*{{cite book
 | last= Rist
 | first= Peter 
 | title= Guide to the cinema(s) of Canada
 |publisher= Greenwood Publishing Group
 |year= 2001
 |isbn= 0-313-29931-5
 |url=http://books.google.ca/books?id=7uKs4fKOotUC
 |accessdate=October 5, 2009
}}
*{{cite book
 | last= Rhodes
 | first= Gary Don 
 | title= White Zombie: Anatomy of a Horror Film
 |publisher= McFarland
 |year= 2001
 |isbn= 0-7864-0988-6
 |url=http://books.google.ca/books?id=o-TKtRV7jRQC
 |accessdate=October 5, 2009
}}

==External links==
* 
*  (1932 French language version)
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 