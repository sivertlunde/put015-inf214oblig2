1994 Baker Street: Sherlock Holmes Returns
 
 

{{Infobox film
| name           = 1994 Baker Street: Sherlock Holmes Returns
| image          = Sherlock-Holmes-Returns.jpg Kenneth Johnson
| based on       = Characters: Arthur Conan Doyle Anthony Higgins Debrah Farentino Ken Pogue Kerry Sandomirsky Kenneth Johnson
| producer       =
| studio         =
| distributor    =
| released       =  
| runtime        = 96 min.
| cinematography = Ken Orieux
| editing        = David Strohmaier
| country        = United States English
| budget         =
| music          = James Di Pasquale
}} Anthony Higgins Kenneth Johnson, and was broadcast on CBS.

==Plot==
Sherlock Holmes is awakened in modern times from suspended animation as a result of an earthquake. He is aided in his recovery by Dr. Amy Winslow (Debrah Farentino), who lives in Baker Street in San Francisco. Holmes pits his wits against the descendants of the Moriarty family, led by James Moriarty Booth. He is also aided by a new group of Baker Street Irregulars led by Zapper (Mark Adair-Rios).

==Cast== Anthony Higgins - Sherlock Holmes
* Debrah Farentino - Amy Winslow
* Ken Pogue - James Moriarty Booth
* Kerry Sandomirsky - Mrs. Ortega
* Mark Adair-Rios - Zapper

==External links==
* 

 
 

 
 
 
 
 
 
 

 