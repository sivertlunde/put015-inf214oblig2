Czechoslovakia 1968
{{Infobox film
| name           = Czechoslovakia 1968
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Denis Sanders and Robert M. Fresco
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  Charles Bernstein
| cinematography = 
| editing        = Marvin Wallowitz
| studio         = A Film by Sanders/Fresco film makers Denis Sanders Robert M. Fresco
| released       =  
| distributor    = Ocean Releasing
| released       =  
| runtime        = 14:36
| country        = United States
| language       = English
}}
Czechoslovakia 1968  is a 1969 short documentary film about the "Prague Spring", the Russian invasion of Czechoslovakia. The film was produced by the United States Information Agency under the direction of Robert M. Fresco and Denis Sanders and features the graphic design of Norman Gollin.

It won the Academy Award for Documentary Short Subject    and in 1997, was selected for preservation in the United States National Film Registry by the Library of Congress having been identified as "culturally, historically, or aesthetically significant".   

==Controversy== Senate Foreign Attorney General, Justice Department Congress amended the Smith-Mundt Act, based on this event, to explicitly prohibit the domestic dissemination of materials produced by the USIA. The USIA was abolished in 1998.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 