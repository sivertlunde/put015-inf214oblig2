Pudgy Takes a Bow-Wow
{{Infobox Hollywood cartoon|
| cartoon_name = Pudgy Takes a Bow-Wow
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist = Lillian Friedman Myron Waldman
| voice_actor = Mae Questel
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = April 9, 1937
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}
Pudgy Takes a Bow-wow is a 1937 Fleischer Studios animated short film starring Betty Boop and Pudgy the Puppy. It is directed by Dave Fleischer and its producer is Max Fleischer.

==Synopsis==
A marquee advertises "Betty Boop in Person" (and gives her 4 and 7/8 stars) at the theatre. Betty leaves Pudgy in her dressing room while she goes to perform her stage show. Her show involves singing a song, then impersonating a Chinese man and an Italian organ grinder. While shes singing, a kitten comes to the dressing room and Pudgy gets out and begins chasing it. They get onto the stage, and in the orchestra pit, and become part of the act, upstaging Betty.
 

==Notes==
* Most DVD releases cut out the part when Betty Boop impersonates a Chinese man. It cuts right to the cat in the dressing room meowing.
* This episode is in Public Domain.

 
 
 
 
 


 