Filles perdues, cheveux gras
{{Infobox Film
| name           = Filles perdues, cheveux gras
| image          = 
| image_size     = 
| caption        = 
| director       = Claude Duty
| producer       = Bruno Levy
| writer         = Jean-Philippe Barrau Claude Duty Pascale Faure Sergi López Léa Drucker Valmont
| cinematography = Bruno Romiguière
| editing        = Agnès Mouchel
| distributor    = 
| released       = September 10, 2002
| runtime        = 100 minutes
| country        = France
| language       = French
}}

Filles perdues, cheveux gras ( ) is a 2002 French musical comedy-drama film about the crossing paths of three lost young women.

The film was premiered at the Cannes Film Festival on May 17, 2002 in film|2002.

==Plot==

Three lost women cross paths and help each other. Elodie (Olivia Bonamy) wants her daughter back, Natacha (Marina Foïs) wants her cat back, and Marianne (Amira Casar) wants her soul back.

==Cast==

* Amira Casar as Marianne
* Marina Foïs as Natasha
* Olivia Bonamy as Élodie
* Charles Berling as Arnaud Sergi López as Philippe
* Léa Drucker as Coraline
* Esse Lawson as Cindy
* Margot Abascal as Corine
* Evelyne Buyle as Mme Pélissier
* Béatrice Costantini as la patronne de Natasha (Natashas boss)
* Amadou Diallo as Kirikou
* Jean-François Gallotte as Jean-François
* Sylvie Lachat as laide social (social worker)

==Recognition==

Cannes Film Festival 
2002 Grand Golden Rail: Claude Duty

Deauville Film Festival 
2002 Best French Script: Jean-Philippe Barrau, Claude Duty and Pascale Faure

==External links==
*  
* Wikipedia French Page: http://fr.wikipedia.org/wiki/Filles_perdues,_cheveux_gras

 

 
 
 
 
 


 
 