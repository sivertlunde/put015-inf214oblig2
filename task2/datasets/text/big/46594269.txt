Draft:Cocktail Molotov (film)
   
 
{{Infobox film
| name           = Cocktail Molotov
| image          = 
| caption        = 
| director       = Diane Kurys
| producer       = Alexandre Films
| writer         = Diane Kurys
| screenplay     = Diane Kurys
| story          = 
| based on       =  
| starring       = Elise Caron Philippe Lebas Francois Cluzet 
| music          = Yves Simon
| cinematography = Philippe Rousellot
| editing        = Joële Van Effenterre
| studio         = 
| distributor    = Agence méditerranéenne de location de films (AMLF)
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Cocktail Molotov is a 1980 French drama film written and directed by Diane Kurys. It is her second feature after Peppermint Soda. A female coming of age story set during the spring and summer of 1968, the film is not a sequel but can be considered "companion piece" to its predecessor.  It has been called a female take on the male-dominated road movie genre. 

==Plot== social uprising back in Paris. With their money running out and their car stolen, they hitchhike back to find they have missed the excitement. 

==Reception==
Cocktail Molotov did not do as well as Peppermint Soda, Kurys critically acclaimed first feature released three years earlier.   Film studies scholar Carrie Tarr has written that audiences may have been confused by Kurys treatment of May 68 as nearly devoid of protest and politics, instead focusing on an explicitly female personal drama, as opposed to the generally male-centered view of the student revolts. She also notes that Kurys had had to rewrite the script due to budget constraints which made reenacting the barricading of Paris streets impossible, and further cut explicitly political scenes out in the editing process to further emphasize the teenagers story.  Perhaps in a reaction to its lack of political content, Vincent Canbys 1981 review in the New York Times called the film "a nearly perfect example of the kind of French film that apotheosizes middle-class values while pretending to question them".   While Tarr writes that the film does not depict abortion, love triangles, or the subjectivity of the female central character as well as other films,   its autobiographical elements, its pairing of personal narrative with larger, historical events   and other connections with the rest of Kurys filmography mark it as an essential part of her work as auteur. 

==Cast==
* Elise Caron as Anne
* Philippe Lebas as Frederic
* Francois Cluzet as Bruno
* Genevieve Fontanel as Annes mother
* Henri Garcin as Annes Stepfather 
* Michel Puterflam as Annes Father 
* Jenny Cleve as Frederics Mother 
* Armando Brancia as Frederics Father 
* Malene Sveinbjornsson as Annes Little Sister 
* Stefania Cassini  as Anna-Maria
* Frederique Meininger as Doctor
* Patrick Chesnais as Trucker  

==Notes==
 

==References==
  
Foster, Gwendolyn Audrey. Women Film Directors: an International Bio-critical Dictionary. Greenwood Press, Westport, 1995. 
Tarr, Carrie. Diane Kurys. Manchester University Press, New York, 1999. 
Tarr, Carrie.Fifty Contemporary Filmmakers ed. by Yvonne Tasker. Rutledge, New York, 2002.

==External links==
* 

 

 
 
 
 
 
 

 

*
*
*
*

 