5-25-77
{{Infobox film
| name           = 5-25-77
| image          = 
| director       = Patrick Read Johnson
| writer         = Patrick Read Johnson
| producer       = Leigh Jones Fred Roos William Holmes Gary Kurtz
| starring       = John Francis Daley Austin Pendleton Colleen Camp Neil Flynn Steven Coulter
| cinematography = David Blood Ken Seng
| music          =  Alan Parsons David E.Russo
| editing        = Alain K.Blair Kat Thomas
| studio         = Moonwatcher Inc. Sparta Entertainment
| released       = May 25, 2007
| country        = United States
| language       = English
}}
 Star Wars on May 25, 1977. {{cite web | last = DeMara | first = Bruce | title = Star Wars-inspired 5-25-77 a long, long time in the making: Director Patrick Read Johnson brings his nearly finished love letter to his own idealistic filmmaking youth to TIFFs Next Wave Festival. | date = 15 February 2013 | url = http://www.thestar.com/entertainment/movies/2013/02/15/star_warsinspired_52577_a_long_long_time_in_the_making.html
 |publisher=Toronto Star   | accessdate = 2 May 2013  }}  Johnson began funding the project in 2001 and filming took place in 2006. An incomplete "preview cut" was exhibited on May 25, 2007 at Star Wars Celebration|Star Wars Celebration IV  and at the Hamptons International Film Festival in 2008, where it won the Heineken Red Star Award.

The film was first known as 5-25-77; the title was then changed to 77. In 2012, the title was reverted to 5-25-77. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 