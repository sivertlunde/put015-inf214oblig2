Thaali Bhagyam
{{Infobox film
| name =     Thaali Bhagyam
| image =    Thaali Bhagyam.jpg
| caption =
| director =  K. B. Nagabhushanam 
| writer = Aarur Das
| screenplay = Aarur Das
| story = Sundararaman
| starring =  M. G. Ramachandran B. Saroja Devi M. N. Nambiar Nagesh
| producer = Varalakshmi Pictures
| music = K. V. Mahadevan
| cinematography = A. Shanmugam
| editing = M. V. Rajan
| released = 27 August 1966
| runtime = 180 mins
| country = India Tamil
| budget =
}}
 
Thaali Bhagyam is a Tamil film is directed by K. B. Nagabhushanam released in 1966. 

==Plot==

Families of Nallasivam and Murugan (M.G. Ramachandran) are neighbours for generations. Nallasivam is nurturing his wish to give his daughter Valli (B. Saroja Devi) in marriage to Murugan.

Meanwhile, a bride search for Nallasivam is on and Murugan accompanies Nallasivam along with others to see a bride Kamalam. Kamalam thinking Murugan is the groom agrees for the marriage, and finds it is otherwise on the day of the marriage.

In a passionate moment kamalam misbehaves with Murugan which is seen by Namchivayam (M.N. Nambiar). Namchivayam takes this opportunity to blackmail Kamalam and get her do things against Murugan.

How Murugan foils their plans and how finally truth prevails is what the movie is all about.

==Cast==
*M. G. Ramachandran
*B. Sarojadevi
*M. N. Nambiar
*Nagesh
*S. V. Subbaiah
*Chittor V. Nagaiah
*M. N. Rajam
*M. V. Rajamma Manorama
*M. S. S. Bhagyam
*K. S. Parvathy

==Music==
The music composed by K. V. Mahadevan and lyrics written by Vaali (poet)|Vaali.

==References==
 

==External links==
*  

 
 
 
 
 


 