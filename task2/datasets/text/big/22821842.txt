B.F.'s Daughter
{{Infobox film
| name           = B.F.s Daughter
| image          =
| caption        =
| director       = Robert Z. Leonard
| producer       = Edwin H. Knopf
| based on       =  
| writer         = Luther Davis (screenplay)
| starring       = Barbara Stanwyck Van Heflin
| music          = Bronislau Kaper Clifford Vaughan Aaron Harounian
| cinematography = Joseph Ruttenberg George White
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget = $1,745,000  .  gross = $1,910,000 
}}

B.F.s Daughter is a 1948 drama film directed by Robert Z. Leonard and starring Barbara Stanwyck and Van Heflin. It is adapted from John P. Marquands controversial 1946 novel of the same name, but the movie script soft-pedals the controversial elements and is a fairly conventional love story. In the United Kingdom the films title was changed to Polly Fulton, since "B.F." is a euphemism in England for "bloody fool."  

==Plot== Richard Hart), a pleasant, dependable gentleman who has the full approval of her family. Then she meets brash intellectual Tom Brett (Van Heflin), who blames many of the worlds problems on the rich. Tom and Polly heartily dislike each other at first, but she finds him exciting compared to the likable "stuffed shirt" Tasmin. Soon Tom and Polly fall passionately in love and get married.

Tom has a tense relationship with Pollys family from the start. And when he gradually realizes that his in-laws are using their connections to advance his career, he is not grateful but bitter. Polly is painfully torn between her strong-willed husband and her devoted father, whom everyone calls "B.F."

When World War II arrives, Tom takes a high-level civilian position in Washington, doing work that he cannot talk about. He and Polly rarely see each other and begin to lead separate lives. Two wartime developments eventually bring the relationship to a crisis point. Polly hears a rumor that Tom is having an affair. And she is stunned by a news report that Bob Tasmin, now a dashing military officer happily married to Pollys best friend, has apparently been killed on a mission behind enemy lines. As the truth about both situations is revealed, Polly and Tom will finally confront their own problems face to face and learn what they really mean to each other.

==Cast==
* Barbara Stanwyck - Pauline Polly Fulton Brett
* Van Heflin - Tom Brett
* Charles Coburn - Burton F. B.F. Fulton Richard Hart - Robert S. Bob Tasmin III
* Keenan Wynn - Martin Delwyn Marty Ainsley
* Margaret Lindsay - Apples Sandler
* Marshall Thompson - The sailor
* Spring Byington - Gladys Fulton
* Barbara Laage - Euginia Taris
* Thomas E. Breen - Maj. Isaac Riley
* Fred Nurney - Jan (the butler)
==Reception==
The film earned $1,449,000 in the US and Canada and $461,000 elsewhere, recording a loss of $565,000. 
==Radio adaptation==
On December 11, 1950, Lux Radio Theater broadcast a radio adaptation of B. F.s Daughter with Barbara Stanwyck reprising her role in the film. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 