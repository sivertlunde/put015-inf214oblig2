After School Midnighters
 
{{Infobox film
| name           = After School Midnighters
| image          = After_School_Midnighters_Poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Hitoshi Takekiyo
| producer       = 
| writer         = 
| screenplay     = Yōichi Komori
| story          = 
| based on       = 
| narrator       = 
| starring       = Kouichi Yamadera Hiromasa Taguchi Haruka Tomatsu Sakiko Uran Minako Kotobuki
| music          = 
| cinematography = 
| editing        = 
| studio         = Koo-Ki
| distributor    = T-Joy
| released       =   
| runtime        = 95 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2012 Japanese computer animated comedy film directed by Hitoshi Takekiyo.   It released on 25 August 2012 in Japan and was originally based upon a short film that Takekiyo had made for a musical channel.   

The producers of After School Midnighters submitted the film for an Academy Award for Best Animated Feature,  but it was not selected for the final round. The film did go on to gain some attention at the 2013 Fantasia International Film Festival earned a special mention for the Audience Award for Best Animation Feature, but did not win. 

==Plot==
Three elementary school girls, Mako ( /Jean-Pierre Leblan). However the quest is made even more difficult as not everyone wants the girls to succeed and Kunstrijk is hiding his own secret reason for wanting the girls to succeed: the quest is actually to help ensure that he and Goth will remain in their living state forever rather than returning to inanimate objects.

==Cast==
 

===Japanese cast===
*Haruka Tomatsu as Mako
*Hiromasa Taguchi as Goth
*Kouichi Yamadera as Kunstlijk
*Minako Kotobuki as Mutsuko
*Sakiko Uran as Miko
*Chafurin as Bach
*Dai Matsumoto as Beethoven
*Hiroshi Shimozaki as Mr. Fly
*Hiroshi Yanaka as Mozart
*Houchu Ohtsuka as Fred
*Hozumi Gôda as Schubert
*Ikuko Tani as Dunkelheit, Lumière
*Juurouta Kosugi as Pinia
*Mariya Ise as Principal (Girl)
*Shozo Iizuka as Shaburi
*Yuki Kuroda as Sony
*Yuusaku Yara as Michael
 

===French cast===
*Bruno Meyere as Monsieur Kunstlijk
*Caroline Combesas Mako
*Christophe Seugnet as Goth
*Lucille Boudonnat as Miko, Mutsuko
*Cathy Cerda as Dunkelheit, Lumière
*Jacques Albaret as Bach, Pinia
*Jean-Bernard Guillard as Chabris
*Jean-Marco Montalto as Fred, Sonny
*Jean-Pierre Leblan as Mozart
*Loïc Houdré as Michael
*Olivier Angèle as Beethoven
 

==Production==
The character of Kunstlijk was originally created for a dialogue-less short film intended for a music video channel and Takekiyo had planned to film a further series of shorts around the anatomical model.  Producer friends of Takekiyo convinced him to work on a feature-length film instead, which necessitated the creation of a script and the addition of various characters. The film quality also improved from the level seen in the short and Takekiyo stated that he found that "Getting the characters to really perform like actors was both the hardest part and the most important." 

==Reception==
Dread Central gave the film 3 1/2 out of 5 blades, remarking that the "animation seems almost too two-dimensional at times, but it’s so colorful and dynamic that by the halfway point I was too wrapped up in the unfolding storyline to care" and that they aw the movie as a good introduction to the horror genre as a whole. 

==References==
 

==External links==
*   
* 
* 
*   on Vimeo

 
 
 
 
 