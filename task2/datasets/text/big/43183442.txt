Dirty Weekend (2014 film)
{{Infobox film
| name           = Dirty Weekend
| image          = Dirty Weekend 2014 film teaser poster.jpg
| alt            = 
| caption        = Teaser poster
| director       = Neil LaBute
| producer       =  
| writer         = Neil LaBute
| starring       =  
| music          = 
| cinematography = Rogier Stoffers
| editing        = Joel Plotch
| studio         = Horsethief Pictures
| distributor    = Falcon Films K5 International
| released       =
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 drama comedy film written and directed by Neil LaBute.    It will star Matthew Broderick, Alice Eve, Phil Burke, and Gia Crovatin.

==Plot==
Les Moore (Broderick) is a businessman who finds himself delayed in a city where a year earlier a few too many drinks led to an unexpected encounter that has since haunted him. He sets out with his co-worker Natalie Hamilton (Eve) to figure out what really happened that night.

==Cast==
*Matthew Broderick as Les Moore 
*Alice Eve as Natalie Hamilton 
*Phil Burke as Cabbie 
*Gia Crovatin as Dylan 

==Production==
Duncan Montgomery, Tiller Russell, and Joey Stewart produced the film while Neil LaBute directed and wrote the script.  In November 2013, K5 International signed on to distribute world wide.   

Filming took place in Albuquerque, New Mexico in fall 2013.  

==References==
{{reflist|refs=
   }}

==External links==
* 

 

 
 
 
 


 