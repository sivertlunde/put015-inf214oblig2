I Often Think of Piroschka
{{Infobox film
| name = I Often Think of Piroschka  
| image =
| image_size =
| caption =
| director = Kurt Hoffmann
| producer =  Georg Witt
| writer =  Hugo Hartung  (novel)   Per Schwenzen   Joachim Wedekind
| narrator =
| starring = Liselotte Pulver   Gunnar Möller   Wera Frydtberg   Gustav Knuth
| music = Franz Grothe   
| cinematography = Richard Angst   
| editing =  Claus von Boro      
| studio = Georg Witt-Film   Bavaria Film
| distributor = Bavaria Film
| released = 29 December 1955  
| runtime = 96 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} novel of the same title by Hugo Hartung. During a train journey, the writer Andreas nostalgically recalls a holiday trip he had made thirty years before in 1920s Hungary to the Lake Balaton area. While there he had enjoyed his first true romance with the daughter of the local stationmaster. The film is in the heimatfilm tradition which was at its height when the film was released. It was extremely popular, and Pulver became closely identified with her role as the title character Piroschka. 

==Cast==
*  Liselotte Pulver as Piroschka Rácz  
* Gunnar Möller as Andreas  
* Wera Frydtberg as Greta 
* Gustav Knuth as Istvan Rácz  
* Rudolf Vogel as Sandor  
* Adrienne Gessner as Ilonka von Csiky  
* Annie Rosar as Pensionsinhaberin Márton  
* Margit Symo as Etelka Rácz  
* Fritz Hinz-Fabricius as Johann von Csiky 
* Otto Storr as Pfarrer  
* Eva Karsay as Judith  

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 