Messalina, Messalina
 

{{Infobox film
| name           = Messalina, Messalina
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Bruno Corbucci
| producer       = Franco Rossellini
| screenplay     = Mario Amendola Bruno Corbucci
| story          = Bruno Corbucci Mario Amendola
| starring       = Anneka Di Lorenzo Vittorio Caprioli Giancarlo Prete
| music          = Guido De Angelis and Maurizio De Angelis
| cinematography = Marcello Masciocchi
| editing        = Daniele Alabiso
| studio         = Medusa Distribuzione
| distributor    = 
| released       =  
| runtime        = 100 min.
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Messalina, Messalina, also known as Caligula II: Messalina, Messalina, is a 1977 Italian spoof film.

==Background==
The film is not in fact a sequel to the 1979 film Caligula (film)|Caligula. During the time that Caligula spent in post-production, the films co-producer Franco Rossellini wanted to get usage from the $20,000,000 of sets and costumes used in Caligula, fearing that the film would never be released.
The sets and costumes, as noted in the films opening credits, were those from Caligula (film)|Caligula, designed by Danilo Donati and used without his consent.

==Synposis==
The film begins with Messalina (Anneka Di Lorenzo), the wife of Claudius (Vittorio Caprioli), who is now Emperor succeeding Caligula.
Most of the film revolves around Messalinas adulterous behavior, while Claudius remains blissfully unaware. Empress Messalina is having sex every man in Rome except her husband Claudius, a traveler who has come to Rome looking for a good time, and a Roman con-man. It all ends in a comedic and gory bloodbath when Claudius comes home early and finds an orgy taking place in the palace. Claudius and his soldiers kill Messalina and all of the orgy participants.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 