Dodge City (1939 film)
{{Infobox film
| name           = Dodge City
| image          = Dodge City 1939 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Curtiz 
| producer       = Hal B. Wallis
| screenplay     = Robert Buckner
| starring       = {{Plainlist|
* Errol Flynn
* Olivia de Havilland
* Ann Sheridan
}}
| music          = {{Plainlist|
* Max Steiner
* Adolph Deutsch  
}}
| cinematography = Sol Polito 
| editing        = George Amy   Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $1 million 
| gross          = 
}} Western film directed by Michael Curtiz and starring Errol Flynn, Olivia de Havilland, and Ann Sheridan.   Based on a story by Robert Buckner, the film is about a Texas cattle agent who witnesses the brutal lawlessness of Dodge City, Kansas and takes the job of sheriff to clean the town up. Filmed in early Technicolor, Dodge City was one of the highest-grossing movies of the year.

==Plot== longhorn cattle center of the world and wide-open Babylon of the American frontier, packed with settlers, thieves and gunmen—the town that knew no ethics but cash and killing". In particular, it is Jeff Surrett (Bruce Cabot) and his gang who kill, steal, cheat and, generally, control life in Dodge City without ever being brought to justice. As Surrett has installed one of his puppets as sheriff, the other citizens hands are tied when it comes to arresting any of the evildoers.
 Alan Hale), who is prepared to stay with him through thick and thin. Among the settlers are beautiful Abbie Irving (Olivia de Havilland) and her irresponsible brother Lee (William Lundigan), who, drunk, causes a stampede (which eventually kills him) and is shot by Hatton in self-defense. When the group arrive in Dodge City, Hatton is confronted with the full extent of the anarchy which is dictating everyday life there. Asked by anxious citizens—Abbies uncle, Dr. Irving (Henry Travers) among them—to be the new sheriff, Hatton politely declines, saying he is not cut out for this kind of job.
 lynch him right then and there.

In the end, Hatton succeeds in both overwhelming and catching the baddies and winning Abbies heart. Everything has been prepared for a quiet family life in newly civilized Dodge City, but Hatton is asked by Colonel Dodge to clean up Virginia City, Nevada, another railroad town more dangerous than Dodge City had ever been. Understanding how much Wade is needed to settle the West, a loving Abbie heartily suggests she and her new husband join the next wagon train for their new life together.

==Cast==
 
* Errol Flynn as Wade Hatton
* Olivia de Havilland as Abbie Irving
* Ann Sheridan as Ruby Gilman 
* Bruce Cabot as Jeff Surrett
* Alan Hale, Sr. as Rusty
* Victor Jory as Yancy
* Frank McHugh as Joe Clemens
* John Litel as Matt Cole 
* Henry Travers as Dr. Irving 
* Henry ONeill as Col. Dodge 
* William Lundigan as Lee Irving 
* Guinn Big Boy Williams as Tex Baird 
* Bobs Watson as Harry Cole 
* Gloria Holden as Mrs. Cole 
* Douglas Fowley as Munger
* Ward Bond as Bud Taylor
* Clem Bevans as Charley (the barber)

==Reception==
Although Errol Flynn was worried how audiences would accept him in Westerns, the film was a big hit and he went on to make a number of movies in that genre. Tony Thomas, Rudy Behlmer * Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 80-81 
==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 