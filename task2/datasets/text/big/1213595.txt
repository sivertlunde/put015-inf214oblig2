Bagi, the Monster of Mighty Nature
{{Infobox animanga/Header
| name            = Bagi, the Monster of Mighty Nature
| image           =
| caption         =
| ja_kanji        = 大自然の魔獣 バギ
| ja_romaji       = Daishizen no Majū Bagi
| genre           = Drama
}}
{{Infobox animanga/Video
| starring      = Japanese:   Saeko Shimazu
| type            = tv film
| director        = Osamu Tezuka
| producer        = 
| writer          = 
| music           = 
| studio          = Tezuka Productions
| network         = Nippon Television
| released        = August 19, 1984
| runtime         = 
}}
 

  is an anime movie that premiered on the Nippon Television network on August 19, 1984. It was written by Osamu Tezuka as a critique of the Japanese governments approval of recombinant DNA research that year. 

==Synopsis==
Deep in the South American jungle, a Japanese hunter named Ryosuke (Ryo for short), and a local boy named Chico, stalk a monster that has been terrorizing the local countryside.  Ryo, however, is quite familiar with this beast, and the story flashes back to his childhood.

Teenage Ryosuke Ishigami, the delinquent son of a crime reporter and a geneticist, is out with a motorcycle gang when they encounter a mysterious woman.  Some of the rougher members of the gang accost her, and she turns out to be anything but normal, landing the gang with serious injuries.  The gang leader returns to the womans hideout for revenge, but the gang members are torn apart, except for Ryosuke.

The woman, named Bagi, turns out to be a "catgirl|cat-woman" &ndash; a cross between a human and a mountain lion.  She recognizes Ryosuke as the boy who had rescued her and raised her as a kitten.  As Bagi grew and people became suspicious of the precocious "cat", who was able to walk on her hind legs and even learned to write her own name and speak, she escaped and grew to adulthood on her own.

Upon their reunion, Ryosuke and Bagi join forces to find out the truth of her origins.  Ryosukes own mother is found responsible for Bagis creation &mdash; Bagi is a product of recombinant DNA research between human and mountain lion cells.  They then follow Ryosukes mother to South America to confront her about the reason for Bagis existence, but find a far greater peril.  The officials in charge of the laboratories there are creating a strain of rice that has the potential to destroy humanity.  Ryosukes mother sacrifices her life to have Bagi destroy the "Rice Ball" and Ryosuke mistakenly pins the blame on Bagi, vowing revenge.

Meanwhile, Bagi is quickly losing her human traits and becomes extremely feral, attacking any humans that come near.  Ryo catches up to her and stabs her when she attacks, but then finds a hand-written note held in a locket around her neck.  He reads his mothers last words, expressing remorse for being both a bad scientist and a bad mother, and Ryo realizes his mistake and is filled with regret.  He returns to the site the next morning to find Bagis body gone, a set of footprints leading off to the distant mountains.  He prays for Bagi to live on in solitude, far away from mankind.

==Characters (In order of appearance)==
; Ryo / Ryosuke Ishigami biker gang.  He reunites with Bagi on his 15th birthday and becomes a hunter five years later. Bagi was his "pet" when she was not yet fully grown.

; Chiko / Chico
: A Mexican boy who suspects that Bagi killed his father and seeks revenge. He meets Ryo and helps him track down Bagi in the nearby wilderness.  He wears a sombrero and is skilled in the use of bolas.

; Bagi    genetically modified to contain both human and feline DNA (her specific origins are not disclosed in the story).  Bagi and her mother were the only survivors of an animal outbreak at the SuperLife Center, though her mother was eventually hunted down.  Bagi wandered as a kitten until she met Ryo and became his "pet cat".  As she grew and developed human characteristics and intelligence, she was forced to leave and disguised herself as a human girl, searching for more of her own kind until she meets Ryo again.

; Prof. Ishigami
: Ryos mother and a leading research professor at the SuperLife Center.  She believes that anything can be improved with science.  She is responsible for the creation of Bagi and others like her.  After the President coerces her to mass-produce poisonous "rice balls", she is overwhelmed with guilt and arranges to have Bagi destroy the rice balls.  She is killed in the process, and Ryo mistakenly blames Bagi.  Her last letter to Ryo expresses remorse for being a bad scientist and a bad mother.

; The Chief
: The man in charge of the SuperLife Center, who is cunning and ambitious. He has a short, almost comical stature (possibly from dwarfism) with long hair that sticks up in a fashion similar to that of Albert Einstein and a small moustache similar to that of Adolf Hitler. He explains Bagis origins after she and Ryo infiltrate the Center, then is hypnotized into arranging transport for them to South America to meet with Professor Ishigami.

; Colonel Sado knocks Bagi had a strong poison in it. When Ryo tried to escape, Sado attempted to start a sword fight with him. This leads to doing so on a pair of bikes. Sado meets his possible end when he falls off the tower of the Research Lab. It is also possible that he may have survived.

; The President of Monica
: The malevolent president of the North American country where the Cucaracha Research Lab is located, he is rather heavyset and dresses in gaudy attire, including a robe that appears to be made from dyed animal tails. He comes to inspect the work being done at the Research Lab, and then gets the idea to use a poisonous genetically-engineered rice created there to eliminate guerrilla rebels that have been opposing his government for years, as well as anyone else who opposes him. When Prof. Ishigami refuses to cooperate with his plan, he has her killed by his attack dogs. His plans are ultimately defeated thanks to Bagi and Ryo, Bagi escaping the lab with the only samples of the rice while Ryo destroyed the lab.

; Cemen Bond
: The man responsible for keeping Ryo in custody at the Cucaracha Research Lab, known for his remarkable marksmanship. His decisions solely depend on coin flipping. Following Ryos escape from the Research Lab, he is fired and then agrees to teach the boy everything he knows about shooting. He appears to be a loose parody of James Bond, evidenced by his last name and music that plays during most of his appearances.

; Blackout
: A robot sent to kill Bagi. He has a belt on his chest which can increase his mass. He was destroyed when Bagi stole the belt and sliced him in half.

==Distribution elsewhere== English dub has not yet been produced. A crowdfunder for a North American DVD was attempted at Anime Sols, but was not successful.  

==See also==
*Osamu Tezuka
*List of Osamu Tezuka anime
*List of Osamu Tezuka manga
*Osamu Tezukas Star System

==References==
 

 

 
 
 
 
 
 
 
 
 
 