The Walking Dead (1995 film)
 
{{Infobox film
| name           = The Walking Dead
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Preston A. Whitmore II George Jackson Doug McHenry Frank Price
| writer         = Preston A. Whitmore II 
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Allen Payne Eddie Griffin Joe Morton Vonte Sweet Roger Floyd Bernie Mac|
| music          = Gary Chang
| cinematography = John L. Demps Jr.
| editing        = Don Brochu William C. Carruth
| studio         = 
| distributor    = Savoy Pictures
| released       = February 24, 1995
| runtime        = 88 minutes
| country        = 
| language       = English
| budget         = 
| gross          = $6,014,341
}}
 1995 war POW during the Vietnam War in 1972. It opened to poor reviews and low box office receipts. Previews billed it as "the black experience in Vietnam".

The box-office gross was over $6,000,000.00. 

==Plot== NVA soldiers. At morning, they are planning their next move when they are ambushed by more NVA soldiers including tree snipers. After a firefight they manage to kill the soldiers with the help of Cpl. Pippins, who appears from the brush and also begins attacking them. After subduing Pippins they look for the rest of his platoon and discover them murdered and their radio missing. They decide to tie Pippins up and bring him along as they march toward the POW camp.The army men talk about what happened to them before they joined the army. SSgt Barkley was a preacher at a church until he came home to his bedroom seeing his wife in bed having sex with another man, Barkley shoots and kills the man and catches the train out of town. Hoover works for a meat packing place until he got fired for stealing meat. Cole tries to buy a new house, but he is turned down by the real estate agent,because of his credit. Brooks tells his girlfriend that hes joining the army to be like his grandfather, Pippins was working for Ray until he is killed by gangsters and chased in the army line without getting caught. While Cole and Barkley go ahead to survey the area, Hoover and Brooks smoke marajuana and talk about Brooks girlfriend, who just dumped him via the mail. During their break, Pippins escapes and takes Brooks pistol. The four men regroup and head out without Pippins. When they reach the camp, they discover a deranged Pippins holding a Vietnamese woman hostage. After killing the woman in front of them, Pippis turns on them with a gun and is killed by SSgt. Barkley. After seeing the camp is empty, the men realize they are expendable decoys. They radio in and are informed that the Marines cleared the POW camp four hours earlier, and that they have 20 minutes to reach their pickup point before the entire area is bombed. As the men go to leave bombs begin dropping on them. They escape the bombardment and head through the jungle to the landing zone, but are ambushed by more NVA soldiers. Brooks is killed, and Cole and Barkley wounded. Hoover goes back and rescues Barkley and the three men are evacuated by helicopter. During an epilogue, we are told that Cole became a career Marine who retired after 20 years of service, SSgt. Barkley took a job counseling troubled teens in Georgia, and Hoover went back home, married his girlfriend and opened up his own business.

==Cast==
*Allen Payne as Cole Evans
*Eddie Griffin as Pvt. Hoover Brache
*Joe Morton as SSgt. Barkley 
*Vonte Sweet as PFC. Joe Brooks
*Roger Floyd as Cpl. Pippins
*Bernie Mac as Ray
*Ion Overman as Shirley Evans
*Kyley Jackman as Sandra Evans
*Jean-Claude La Marre as Pvt. Earl Anderson (as Jean LaMarre)
*Lena Sang as Barbra Jean
*Wendy Raquel Robinson as Celeste
*Dana Point as Edna
*Doil Williams as Harold
*Damon Jones as 2nd Lt. Duffy
*Kevin Jackson as Deuce

==Soundtrack==
 
{{Infobox album  
| Name        = The Walking Dead
| Type        = soundtrack
| Artist      = Various artists
| Cover       = Walking Dead OST.jpg
| Released    = March 7, 1995
| Recorded    = 1994-1995 Hip hop, Contemporary R&B|R&B
| Length      = 41:32 Motown
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =   
| noprose = yes
}}
The soundtrack album for the film was released on March 7, 1995 by Motown Records. It consists mostly of Motown hits from the 1960s and 1980s, but also includes three newly recorded Hip hop music|hip-hop songs exclusive to the soundtrack. The soundtrack failed to make it to the Billboard charts, but Scarface (rapper)|Scarfaces "Among the Walking Dead" was a minor hit on the R&B and rap charts.

===Track listing===
#"How I Wish"- 4:49 (Queen Latifah)
#" )
#" )
#" )
#"The Tracks of My Tears"- 3:01 (The Miracles)
#" ) Get Ready"- 2:47 (The Temptations)
#"I Heard It Through the Grapevine"- 5:53 (Marvin Gaye)
#" )
#" )
#"Conflict"- 4:33 (Whitehead Bros. and Big Rube)

==References==
 

==External links==
*  
*  

==See also==
* Wallace Terry, who wrote award-winning, true-life accounts of African American soldiers who served in Vietnam

 
 
 
 
 
 
 
 