Appu (1990 film)
{{Infobox film
| name           = Appu
| image          = Appu (1990 film).jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Dennis Joseph
| producer       = G. P. Vijayakumar
| writer         = Sreekumaran Thampi
| narrator       =  Murali  K.R. Vijaya
| music          = {{Plainlist|
* Songs:
* T. Sundararajan
* BGM:
* S. P. Venkatesh
* Lyrics:
* Sreekumaran Thampi
}}
| cinematography = A. V. Thomas
| editing        = K. Sankunni
| studio         = Seven Arts Films
| distributor    = Seven Arts Films
| released       = 1990
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Sunitha and Murali (Malayalam actor)|Murali.

==Plot==
Appukuttan is a poor orphan and honest young man who falls in love with Sarojini. One day Sugandhi, a dancer in Alamelus troop, was found raped and murdered. Appu became a suspect of this murder. With the help of his friends Chandikunju and lawyer Prabhakaran, Appu tries to find the culprits.

==Cast==
*Mohanlal as Appukuttan Sunitha as Sarojini Murali as Gopan Vijayaraghavan as Ramankutty
*Nedumudi Venu as Chandikunju
*K. R. Vijaya
*Captain Raju as Suresh
*Mohan Jose
*Lalu Alex as Adv. Prabhakaran
*K.P.A.C. Lalitha as Alamelu
* Kundara Johny
*M. G. Soman as Police Officer
*Alummoodan as Pushkaran Janardhanan as Janardhana Kaimal Sainuddin as Ramanan
*Balan K. Nair as Abu
*Jagannathan as Thamarakshan

==External links==
*  
* http://popcorn.oneindia.in/title/6302/appu.html

 
 
 
 
 

 
 