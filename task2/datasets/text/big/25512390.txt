Jeevana Jyothi (1975 film)
{{Infobox film
| name           = Jeevana Jyothi
| image          = Jeevana Jyothi.jpg
| image_size     =
| caption        =
| director       = K. Viswanath
| producer       = D. V. S. Raju
| writer         =
| narrator       = Raja Babu Shubha Amol Palekar
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1975
| runtime        =
| country        = India Telugu
| budget         =
}} Sanjog (1985) with Jayaprada and Jeetendra. 

==Plot==
Hyderabad-bred Narain goes to a village, where he meets and falls in love with a village belle, Yashodra. She also is attracted to him, and both get married. After their marriage, both go to live with Narains parents, brother, and sister-in-law, Lalita, and her son, Sonu. Yashodra gets close to Sonu, and starts to spend all her time with him. This raises some concerns with Lalita, which results in some acrimony. Tragically, Sonu passes away, leaving Yashodra devastated and depressed. She gets pregnant and gives birth to a baby girl, but cannot get Sonu out of her mind. Her depression gives way to insanity, as she keeps on seeing Sonu in every child, and as a result she is institutionalized. Narain has taken to alcohol in a big way and drowns his sorrows and frustrations day and night in a drunken stupor. As a result, his daughter is adopted by Lalita and his brother, without knowing who her real parents are. Years pass by, her daughter, Asha, has grown up and is herself a mother of a baby boy, and settled in the U.S.. Asha arrives in India for a visit and the entire family assembles to visit Yashodra. They find her holding a piece of log, covered in a blanket, singing to it as if it where Sonu. It is here that Asha finds out who her real parents are, and it is here that she will be called upon to make the ultimate sacrifice.

==Songs==
*"Ekkada Ekkada Dakkunnano Cheppuko"
*"Endukante Emi Cheppanu"
*"Muddula Maa Baabu"
*"Sinni O Sinni"

==Awards==
Filmfare Awards South
*Filmfare Award for Best Film – Telugu - D. V. S. Raju
* K. Viswanath won 2nd Filmfare Award for Best Director – Telugu
* Shoban Babu won 2nd Filmfare Award for Best Actor – Telugu
* Vanisri won 3rd consecutive Filmfare Award for Best Actress – Telugu

Nandi Awards
*Nandi Award for Best Feature Film in 1975 .
*Nandi Award for Best Actor - Shoban Babu.

==References==
 

==External links==
*  
 

 
 
 
 
 
 


 