Gulliver's Travels (1977 film)
{{Infobox film
| name = Gullivers Travels
| image= Gullivers Travels 1977.jpg
| image size= 
| caption= Film poster
| director=Peter R. Hunt
| producer=Derek Horne (producer) Raymond Leblanc (producer) Josef Shaftel (executive producer) Don Black (lyrics, screenplay) Jonathan Swift (novel)
| narrator=
| starring= Richard Harris Catherine Schell Norman Shelley Rod Taylor
| music= Michel Legrand
| cinematography = Alan Hume
| editing= Ron Pope
| studio= Belvision
| distributor= Arrow Films Sunn Classic Pictures
| released= 
| runtime=77 minutes approx
| country= United Kingdom Belgium
| language=English
| budget=
| gross=
}} novel of the same name by Jonathan Swift. It mixed live action and animation, and starred Richard Harris in the title role.

The opening sequence in live action shows Gulliver announcing his intention to go to sea as a ships surgeon, followed by scenes of a shipwreck. The remainder of the film has Harris on Lilliput and Blefuscu, with the tiny inhabitants created by animation.

At the very end of the film, having escaped by boat from Lilliput, Gulliver encounters one of the giant inhabitants of Brobdingnag, but there is nothing more about his adventures there or in the other lands mentioned in the novel.

==Voice cast==
*Richard Harris as Gulliver
*Catherine Schell as Mary
*Norman Shelley as Father Meredith Edwards as Uncle Michael Bates
*Denise Bryer
*Julian Glover
*Stephen Jack
*Bessie Love
*Murray Melvin
*Nancy Nevinson
*Robert Rietti
*Vladek Sheybal as President of Blefuscu
*Roger Snowden
*Bernard Spear
*Graham Stark
*David Prowse
*Rod Taylor as Reldresal / King of Blefuscu

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 