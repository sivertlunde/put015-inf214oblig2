Stardust in Your Eyes
{{Infobox film
| name           = Stardust in Your Eyes
| image          =
| image_size     =
| caption        =
| director       = Phil Tucker
| producer       = Phil Tucker
| writer         = Slick Slavin
| narrator       =
| starring       = Slick Slavin
| music          = Slick Slavin (song, "My Heart Is Owned and Operated by You")
| cinematography = Jack Greenhalgh
| editing        =
| studio         = Third Dimensional Pictures, Inc.
| distributor    = Astor Pictures
| released       =  
| runtime        = 8 minutes
| country        = US
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Stardust in Your Eyes is a 1953 short subject filmed in 3-D, starring night-club comic Slick Slavin and was originally shot to accompany the 3-D feature Robot Monster.

In the short, Slick tells the audience how their favorite stars will look and sound in 3-D, as done in impressions to his own tune, "My Heart Is Owned and Operated by You."  Slavin does impressions of James Cagney, Ronald Colman, Charles Laughton, James Stewart, Sydney Greenstreet, Peter Lorre, and Humphrey Bogart.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 