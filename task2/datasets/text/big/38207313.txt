Rising Damp (film)
{{Infobox film
| name           = Rising Damp
| image          = "Rising_Damp"_(film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = UK theatrical poster by Tom Beauvais Joseph McGrath
| producer       = Roy Skeggs
| screenplay     = Eric Chappell
| based on       =  
| starring       = {{Plainlist|
*Leonard Rossiter
*Frances de la Tour
*Don Warrington
*Christopher Strauli
*(Richard Beckinsale)
}}
| music          = David Lindup
| cinematography = Frank Watts
| editing        = Peter Weatherly
| studio         = 
| distributor    = 
* ITC Entertainment  (theatrical release) 
* Acorn Media  (DVD-Video) 
| released       =  
| runtime        = 98 minutes
| country        = England
| language       = English
| budget         = 
| gross          = 
}}
 British situation ITV from Joseph McGrath.

In the film, Rupert Rigsby (Leonard Rossiter), the middle-aged landlord of a decrepit townhouse in inner-city London, tries desperately to win the heart of Ruth Jones (Frances de la Tour), one of his tenants.

For her performance as Ruth Jones, Frances da la Tour received an Evening Standard British Film Award in the category of "Best Actress".

==See also==
*Are You Being Served? (film)|Are You Being Served? (film)
*Porridge (film)|Porridge (film)

==Plot==
The Rigsby households fragile romantic alliances are shattered by the arrival of the very suave Seymour. The attractive miss Jones falls head over heels for the newcomer, while he in turn, falls for her savings book. The complex tangles of romance and lust in the house take their farcical twists and turns. 

==Cast==
*Rigsby -	Leonard Rossiter
*Miss Jones -	Frances de la Tour
*Philip Smith -	Don Warrington
*John Harris -	Christopher Strauli
*Charles Seymour -	Denholm Elliott
*Sandra Cooper -	Carrie Jones
*Mr. Cooper -	Glynn Edwards
*Bert -	John Cater
*Alec -	Derek Griffiths
*Italian Waiter -	Ronnie Brody
*Accordionist -	Alan Clare
*Rugby Player -	Pat Roach
*Boutique Assistant -	Jonathan Cecil
*Workman -	Bill Dean

==Critical reception== Time Out wrote, "in which it is demonstrated that moderately droll TV boarding-house sitcoms ought not to be stretched to 98 minutes."  
*David Parkinson wrote in the Radio Times, "the absence of Richard Beckinsale does much to sap the enjoyment of this decent movie version of the enduring television sitcom. Eric Chappell...overwrites to compensate and the film suffers from too many padded scenes and too few hilarious situations. Newcomer Denholm Elliott looks a tad out of place alongside regulars Frances de la Tour and Don Warrington, but he makes a solid foil for the magnificent Leonard Rossiter, who pursues his romantic quest with a seedy chivalry that both disgusts and amuses.". 

==External links==
* 

==References==
 

 
 
 
 
 
 
 


 