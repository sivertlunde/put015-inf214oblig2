Silken Shackles
{{infobox film
| name           = Silken Shackles
| image          =
| imagesize      =
| caption        =
| director       = Walter Morosco Edward Sowders (asst director)
| producer       = Warner Brothers
| writer         = Charles Harris (play) Walter Morosco (adaptation & scenario) Philip Klein (adaptation & scenario)
| starring       = Irene Rich
| music          =
| cinematography = John J. Mescall Bert Shipman (asst cameraman)
| editing        =
| distributor    = Warner Brothers
| released       = May 14, 1926
| runtime        = 60 minutes
| country        = United States Silent (English intertitles)

}}
Silken Shackles is a lost  1926 silent drama film produced and distributed by Warner Brothers. The film was directed by Walter Morosco, the son of theater owner Oliver Morosco and was based on an off Broadway play by Charles Harris. Irene Rich leads the cast. 

==Cast==
*Irene Rich - Denise Lake
*Huntley Gordon - Howard Lake
*Bertram Marburgh - Lord Fairchild
*Victor Varconi - Tade Adrian
*Evelyn Selbie - Tades Mother
*  - Frederic Stanhope
*Kalla Pasha - Tades Father

==References==
 

==External links==
* 
* 
*Alternate lobby posters  ,  

 
 
 
 
 
 
 
 
 


 