A Sweet Scent of Death
 
{{Infobox film
| name           = A Sweet Scent of Death
| image          = A Sweet Scent of Death.jpg
| caption        = Film poster
| director       = Gabriel Retes
| producer       = 
| writer         = Guillermo Arriaga
| starring       = Diego Luna
| music          = 
| cinematography = Claudio Rocha
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

A Sweet Scent of Death ( ) is a 1999 Mexican drama film directed by Gabriel Retes based on the novel of the same name by Guillermo Arriaga who also wrote the screenplay. It was entered into the 21st Moscow International Film Festival.   

==Cast==
* Diego Luna as Ramón
* Laila Saab as Adela
* Karra Elejalde as El Gitano
* Ana Álvarez as Gabriela
* Héctor Alterio as Justino
* Odiseo Bichir as Pascual
* Juan Carlos Colombo as La Amistad
* Alfredo Alfonso as Natalio
* Ignacio Retes as Marcelino
* Edna Necoechea as Francisca

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 