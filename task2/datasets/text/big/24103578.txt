Polikushka (film)
{{Infobox film
| name           = Polikushka
| image          = Polikushka.jpg
| image_size     = 
| caption        = Film poster
| director       = Aleksandr Sanin
| producer       = 
| writer         = Fedor Ozep Nikolai Efros Leo Tolstoy (story)
| narrator       = 
| starring       = Ivan Moskvin Vera Pashennaya
| music          = 
| cinematography = Yuri Zhelyabuzhsky
| editing        = 
| distributor    =  Russ
| released       = 31 October 1922
| runtime        = 50 minutes
| country        = Soviet Union
| language       = Silent film
| budget         = 
}}
 Soviet film story of the same title. The filming was completed in 1919 but the release was delayed till 1922 because of the Russian Civil War.

==Cast==
* Ivan Moskvin - Polikei
* Vera Pashennaya - Akulina, Polikeis wife
* Yevgeniya Rayevskaya - The Lady
* Varvara Bulgakova - The Ladys niece
* Sergei Aidarov - Steward
* Dmitri Gundurov - The gardener
* Sergei Golovin - Dutlov
* A. Istomin - Ilyukha
* Nikolai Znamenskiy - Alyokha
* Varvara Massalitinova - The joiners wife
* Nikolai Kostromskoy - Barman

==External links==
* 

 
 
 
 
 
 
 
 

 