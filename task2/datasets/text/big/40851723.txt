Revolver Rani
 
 
{{Infobox film
| name           = Revolver 
| image          = Revolver_Rani_First_Look_Poster.jpg
| caption        = Theatrical release poster
| director       = Sai Kabir Shrivastav
| producer       = Raju Chadha Crouching Tiger Motion Pictures Nitin Tej Ahuja Rahul Mittra
| Co-Producer    = Dinesh Madan
| screenplay     = Sai Kabir Shrivastav 
| story          = Sai Kabir Shrivastav Zakir Hussain Zeishan Quadri
| music          = Sanjeev Srivastava
| Lyrics         = Puneet Sharma  Shaheen Iqbal
| cinematography = Suhas Gujarathi
| editing        = Aarti Bajaj
| studio         = Wave Cinemas Ponty Chadha Moving Pictures Tigmanshu Dhulia
| distributor    = Wave Cinemas Distribution
| released       =  
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} crime drama Zakir Hussain and Pankaj Saraswat in supporting roles. The film is a satirical and unusual love story set against the backdrop of politics and released on 25 April 2014.

==Plot==

In the hostile territory of Chambal Division|Chambal, the land of rebels, politicians, bullets and blood, you either live by the gun or die by the gun. Revolver Rani is set in this hostile world where there are no friends; only fragile alliances and deadly enemies that aim straight for the head. But sometimes you do not need to take lives to finish your enemies. Sometimes you just need to go for their heart. Revolver Rani is a satirical and unusual love story set against the backdrop of politics. Its about Alka Singh (Kangana Ranaut), the leader of a political party and her obsessive love for Rohan Mehra (Vir Das), a rising star of Bollywood.

The elections in Gwalior have just finished and Alkas reign has come to an end. Her opposition, the Tomar party, have come into power and want to use their new position to take out Alka, but first, they want to hurt her. The Tomars abduct Rohan from Mumbai and take him back to Gwalior with the aim to kill him. They are about to pull the trigger when Revolver Rani turns up, all guns blazing and saves his life.

Revolver Rani has a very unique story at its heart that has only ever been hinted at before in Bollywood. This film will bring that story into the limelight and show the viewers what would happen if a political giant fell in love with a Bollywood star and then had to protect him from her enemies. Its a very authentic satire, littered with dead bodies, love and black comedy that builds up to an iconic climax set in the Shivpuri Bird Sanctuary (Madhya Pradesh) just before the break of dawn. Even if she is deadly, Revolver Rani needs love too!

==Production==
Sai Kabir, who has directed Revolver Rani, said in an Interview, "It was a great experience… we shot the film in Chambal. Its a black comedy and its a political film". Producer Tigmanshu Dhulia says that Revolver Rani is a complete B grade entertainment movie and everybody will like it. 

==Cast==

* Kangana Ranaut as Alka Singh (aka Revolver Rani)
* Vir Das as Rohan Kapoor
* Piyush Mishra as Balli Zakir Hussain as Udaybhan Tomar
* Pankaj Saraswat as Govind Tomar
* Zeishan Quadri as Pilot
* Kumud Mishra as Tomar
* Preeti Sood as Gutki
* Salim Javed as Puneet
* Jami Jafry as Mithilesh Singh
* Zafar Khan as Bheem Singh
* Rahul Gandhi as Jay
* Abhijeet Shetty as Viru
* Pathy Aiyar as CEO of OZO
* Deana Uppal as Nisha
* Nikunj Malik as Zahira(introducing)
* Sanjay Singh as Home Minister
* Mishkka Singh as Payal Parihar

==Soundtrack==
The soundtrack composed by Sanjeev Srivastava received positive reviews. The soundtrack consists of 9 tracks with Asha Bhosle singing a number "Kaafi Nahi Chand" which reflected the style of R D Burman. The lyrics have been penned by Shaheen Iqbal and Puneet Sharma. One does not expect too much from the music of a film with a title like Revolver Rani. Moreover, the composer Sanjeev Srivastava and lyricist Puneet Sharma are new. But after listening to the soundtrack five or six times, you may start liking it. The soundtrack of Revolver Rani is unconventional and largely experimental. It grows on you the more you hear it. Rediff gave 3/5 stars for the soundtrack.

===Track listing===

{{track listing
| extra_column = Singer(s)
| total_length = 

| title1 = Revolver Rani
| extra1 = Usha Uthup
| length1 = 
| title2 = Thaayein Kare Katta
| extra2 = Piyush Mishra
| length2 = 
| title3 = Kaafi Nahi Chaand
| extra3 = Asha Bhosle
| length3 = 
| title4 = Chal Lade Re Bhaiya
| extra4 = Mayur Vyas, Piyush Mishra, Abhishek Mukherjee
| length4 = 
| title5 = Sulgi Hui Hai Raakh
| extra5 = Gorisa, Sanjeev Srivastava
| length5 = 
| title6 = Banna Banni
| extra6 = Rekha Bhardwaj
| length6 = 
| title7 = I Am Brutal
| extra7 = Sanjeev Srivastava
| length7 = 
| title8 = Saawan Ki Aye Hawa
| extra8 = Rahul Gandhi, Garima Aneja
| length8 = 
| title9 = Bol Rahi Hai Payal 
| extra9 = Avi Dutta, Anwesha Datta Gupta
| length9 = 
| title10 = Chanda Ki Katori Hai (Lorie) 
| extra10 = Garima Aneja
| length10 = 
| title11 = Pehle Lohe Ki Chingaari
| extra11 = Sameera, Gorisa, Keka, Manjeera
| length11 = 
| title12 = We Mix You Micheal Jackson
| extra12 = Saleem Javed
| length12 = 
| title13 = Zardozi Lamhe
| extra13 = Moin Sabri
| length13 = 
| title14 = Chanda Ki Katori Hai (Lorie)
| extra14 = Piyush Mishra
| length14 = 
| title15 = Revolver Rani (Reprise)
| extra15 = Usha Uthup
| length15 = 
}}

==Critical reception==
As per Shubha Shetty-Saha of Mid Day The film Revolver Rani  starts with much promise, with a kind of quirkiness and black humour that fits well, but it begins to fizzle out by the end of it. Watch it for Kangna Ranaut and the music. Film critic Subhash K. Jha rated it 3.5 stars and wrote that Revolver Rani is a fiercely original piece of cinema, crafted with compelling concentration and impassioned intuition. As per Jha, Kangana plays like a cross between Uma Thurman in Kill Bill and Seema Biswas in Bandit Queen.   Giving it 3 stars out of 5, Anupama Chopra praised the film in a review for Hindustan Times saying that "what keeps Revolver Rani together are the performances and the sly humour. I particularly enjoyed the two testosterone-filled duffer politicians whose only aim is to kill Alka. If you like uplifting, cheerful cinema, then this isnt the movie for you. But if, like me, you can enjoy bad people doing bad things, then Revolver Rani will be fun."  Harshada Rege from Daily News and Analysis praised Ranauts performance, saying that "she plays the Venice-loving, gun-trotting politician in Chambal with much aplomb. From a vulnerable Rani in Queen (film)|Queen to the mighty Alka Singh in Revolver Rani, the actress sure knows how to make and then, equally easily, break the mould."  Shubhra Gupta from The Indian Express panned the film, saying that the character of Rani in the film was neither believable or relatable, making it "a tiresome Bollywoodesque trudge... It is clear that Kangana Ranaut is trying hard for the sur (mass), but this is not her territory: she gets to that well-judged manic edge only a couple of times, and then slides back. Her hold on the character is slippery, as is her accent, and the shade of her tan." 

==Box office==
This film had a poor run in the Indian Box Office with an occupancy of 15%. The film was a box-office flop.

== References ==
 
 

== External links ==
* 
*  
* 
* 
* 
* 

 
 
 
 
 
 