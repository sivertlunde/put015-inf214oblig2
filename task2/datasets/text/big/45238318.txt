Midnight (1922 film)
{{Infobox film
| name           = Midnight
| image          =
| alt            = 
| caption        =
| director       = Maurice Campbell 	 
| producer       = 
| screenplay     = Harvey F. Thew 
| starring       = Constance Binney William Courtleigh Sidney Bracey Arthur Stuart Hull Herbert Fortier Helen Lynch Edward Martindel
| music          = 
| cinematography = H. Kinley Martin 
| editing        = 
| studio         = Realart Pictures Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Maurice Campbell and written by Harvey F. Thew. The film stars Constance Binney, William Courtleigh, Sidney Bracey, Arthur Stuart Hull, Herbert Fortier, Helen Lynch and Edward Martindel. The film was released on February 19, 1922, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Constance Binney as Edna Morris
*William Courtleigh as William Morris
*Sidney Bracey as Dodd
*Arthur Stuart Hull as George Potter
*Herbert Fortier as Bishop Astor
*Helen Lynch as Grace Astor
*Edward Martindel as Senator Dart
*Jack Mulhall as Jack Dart 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 