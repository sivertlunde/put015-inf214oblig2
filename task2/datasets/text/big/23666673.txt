Black Eagle of Santa Fe
{{Infobox film
| name           =Die Schwarzen Adler von Santa Fe
| image          =Black Eagle of Santa Fe.jpg
| image_size     =
| caption        =
| director       =Alberto Cardone as Albert Cardkiff and Ernst Hofbauer as Ernest Goodman
| producer       = Mario Siciliano, Gunther Raguse, Wolf C. Hartwig Jack Lewis
| narrator       = Joachim Hansen Horst Frank
| music          = Gert Wilden
| cinematography = Hans Jura Eastmancolor, Ultrascope
| editor         = Herbert Taschner
| distributor    = Constantin Film
| released       = 1965
| runtime        = 95 minutes
| country        =West Germany, Italy
| language       = German
| budget         =
}}
 1965 West West German Italian western film directed by Alberto Cardone and Ernst Hofbauer.

==Story==
Ranch workers disguised as soldiers murder Indians in order to stir up trouble with the whites so the rancher can claim their land.

==Cast==
*Brad Harris – Cliff/Clint McPhearson
*Horst Frank – Blade Carpenter Tony Kendall – Chief Black Eagle
*Pinkas Braun – Gentleman Joachim Hansen – Captain Jackson
*Werner Peters – Morton
*Ennio Girolami (as Thomas Moore)	– Tom Howard/Slim James
*Edith Hancke – Cora Morton
*Joseph Egger – Buddy
*Serge Marquand – Brad Howard/Chet BlackyJames
*Olga Schoberová – Lana Miller
*Jacques Bézard (as Jackie Bezard) – Pasqual
* Ángel Ortiz – Sergeant
*Annie Giss	– Madam
*Lorenzo Robledo – courier

==Reception==
Black Eagle of Santa Fe is considered a contemporary homage to the Karl May film adaptations.  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 