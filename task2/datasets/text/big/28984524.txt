The Precinct
{{Infobox film
| name           = The Precinct
| image          = The Precinct.jpg
| alt            =  
| caption        = Film poster
| director       = Ilgar Safat
| producer       = Nariman Mammadov
| writer         = Ilgar Safat
| starring       = Zaza Bejashvili
| music          = 
| cinematography = Mindia Esadze Vako Karchkhadze
| editing        = Ilgar Safat
| studio         = 
| distributor    = 
| released       =  
| runtime        = 116 minutes
| country        = Azerbaijan Azerbaijani Russian Russian
| budget         = 
| gross          = 
}} Best Foreign Language Film at the 83rd Academy Awards,    but it did not make the final shortlist.   

For his performance in an International Feature Film, Teymur Odushev won the award of Young Artist Awards 2011.  

==Cast==
* Zaza Bejashvili as Garib
* Teymur Odushev as young Garib
* Melissa Papel as Sabina
* Vagif Ibrahimoglu as Police chief

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Azerbaijani submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 


 