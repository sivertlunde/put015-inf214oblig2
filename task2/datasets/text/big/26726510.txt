The Rebel Set
{{Infobox film
| name           = The Rebel Set
| image_size     =
| image	=	The Rebel Set FilmPoster.jpeg
| caption        =
| director       = Gene Fowler Jr.
| producer       = Phil G. Giriodi (associate producer) J. William Hayes (executive producer) Kenneth Kessler (supervising producer) Earle Lyon (producer)
| writer         = Bernard Girard (writer) Louis Vittes (writer)
| narrator       =
| starring       = See below
| music          = Paul Dunlap
| cinematography = Karl Struss William Austin Allied Artists
| released       = 1959
| runtime        = 72 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Rebel Set is a 1959 American film directed by Gene Fowler Jr.. It was later featured and riffed on Mystery Science Theater 3000.

== Plot summary ==
Mr. Tucker (Platt), proprietor of a Los Angeles coffee house, hires three down-on-their-luck patrons - out-of-work actor John Mapes (Palmer); struggling writer Ray Miller (Lupton); and George Leland (Sullivan), the wayward son of a movie star - to participate in an armored car robbery to take place during a four-hour stopover in Chicago during the trios train trip from Los Angeles to New York.  Tucker and his henchman Sidney (Glass) fly ahead to set up the robbery, which goes off without a hitch.  However, once back on the train, Lelands greed gets the better of him, but Tucker double crosses the trio, eliminating Leland and Miller, leaving Mapes as the only one left to stop Tucker from getting away with murder - along with the entire haul.

== Cast ==
*Gregg Palmer as John Mapes
*Kathleen Crowley as Jeanne Mapes
*Edward Platt as Mr. Tucker / Mr. T
*John Lupton as Ray Miller
*Don Sullivan as George Leland
*Ned Glass as 	Sidney Horner
*Vikki Dougan as Karen, the waitress
*I. Stanford Jolley as King Invader, beat poet
*Barbara Drew as Mrs. Packard
*Cecil Elliott as Train Gossip
*Grace Field as Train Gossip
*Gene Roth as Conductor, New York train
*Robert Shayne as Lt. Cassidy
*Gloria Moreland as Bali Dancer
*Byron Foulger as Conductor, Chicago train

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 