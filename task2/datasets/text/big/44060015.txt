Madhurikkunna Raathri
{{Infobox film
| name           = Madhurikkunna Raathri
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       =
| writer         = Sreemoolanagaram Vijayan
| screenplay     = Sreemoolanagaram Vijayan
| starring       = Thikkurissi Sukumaran Nair Pattom Sadan Aparna Mala Aravindan
| music          = M. S. Viswanathan
| cinematography = Rangan
| editing        = Balakrishnan
| studio         = Sreeganesh Kalamandir
| distributor    = Sreeganesh Kalamandir
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed PG Vishwambharan. The film stars Thikkurissi Sukumaran Nair, Pattom Sadan, Aparna and Mala Aravindan in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Thikkurissi Sukumaran Nair
*Pattom Sadan
*Aparna
*Mala Aravindan
*Mamatha Meena
*P. K. Abraham Ravikumar
*Vincent Vincent

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ding Dong || P Jayachandran || Yusufali Kechery ||
|-
| 2 || Kuliranu Deham || P Jayachandran || Yusufali Kechery ||
|-
| 3 || Rajani Hemantharajani || P Susheela, P Jayachandran || Yusufali Kechery ||
|-
| 4 || Vishwamohini || Jolly Abraham || Yusufali Kechery ||
|}

==References==
 

==External links==
*  

 
 
 


 