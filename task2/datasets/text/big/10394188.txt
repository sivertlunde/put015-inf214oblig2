The Phantom President
{{Infobox film  
| name           = The Phantom President
| image          = The phantom president.jpg
| caption        =
| director       = Norman Taurog
| producer       =
| writer         = Walter DeLeon Harlan Thompson George F. Worts (novel)
| starring       = George M. Cohan Claudette Colbert Jimmy Durante
| music          = Richard Rodgers Rudolph G. Kopp (uncredited) John Leipold (uncredited) David Abel
| editing        =
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 78 min.
| country        = United States
| language       = English
| budget         =
}}
 1932 film directed by Norman Taurog, and starring George M. Cohan, Claudette Colbert and Jimmy Durante. 

According to Richard Rodgers, George M. Cohan deeply resented having to work with Rodgers and lyricist Lorenz Hart on the film. Cohan was bitter that the type of musical theatre that he had created was now out of fashion, and that it was being supplanted by the more literate and musically sophisticated shows of Rodgers and Hart, among others. During the filming, Cohan would sarcastically refer to Rodgers and Hart as "Gilbert and Sullivan". On the other hand, 5 years later (1937), Cohan starred on Broadway in Rodgers and Harts musical ID RATHER BE RIGHT, as a thinly disguised singing and dancing F.D.R. SO he must have gotten over that resentment. This film and the novel that it was based on are the uncredited sources for the 1993 Kevin Kline film, DAVE.

==Plot==
It tells the fictional story of American presidential candidates, based on the novel by George F. Worts. A colorless stiff candidate for President is replaced in public appearances by a charismatic medicine show pitchman.

==Quotes==
*Prof. Aikenhead: "Blair lacks political charm. Blair has no flair for savoir faire."
*Peeter J. Doc Varney: "Im just trying to figure out which one of us looks the most alike."

==Cast==
*George M. Cohan as Theodore K. Blair/Peeter J. Doc Varney
*Claudette Colbert as Felicia Hammond
*Jimmy Durante as Curly Cooney George Barbier as Boss Jim Ronkton
*Sidney Toler as Prof. Aikenhead
*Louise Mackintosh as Sen. Sarah Scranton
*Jameson Thomas as Jerrido
*Julius McVicker as Sen. Melrose

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 