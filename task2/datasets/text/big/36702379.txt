Shooting High
{{Infobox film
| name           = Shooting High
| image          = Shooting_High_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Alfred E. Green John Stone Magers 2007, p. 158. 
| screenplay     = {{Plainlist|
* Lou Breslow
* Owen Francis
}}
| starring       = {{Plainlist|
* Jane Withers
* Gene Autry
* Marjorie Weaver
}}
| music          = Samuel Kaylin  Ernest Palmer
| editing        = Nick DeMaggio
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 66 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film directed by Alfred E. Green and starring Jane Withers, Gene Autry, and Marjorie Weaver. Written by Lou Breslow and Owen Francis, the film is about a generations-old old feud between two families that is resurrected when the town banker tries to contrsuct a highway through the area bearing a monument to the frontiersman ancestor of one of the families. Magers 2007, p. 159.     Shooting High was Gene Autrys first film away from Republic Pictures, his first as a mere co-star, his first without the comic presence of Smiley Burnette since 1934, and his first playing a character other than himself.

==Plot==
The Carsons and the Pritchards have been feuding in the town of Carsons Corners for generations. The budding romance between Will Carson (Gene Autry) and Marjorie Pritchard (Marjorie Weaver) is now being threatened by the long-standing feud. Margories father, Calvin Pritchard (Frank M. Thomas), is the bank president and mayor of Carsons Corners. Calvin pretends to support Wills courtship of his daughter because he needs to acquire a piece of Carson property for a proposed highway through the area. When Will learns of Calvins true motives, he accuses Marjorie of scheming with her father to steal Carson land. 

The long simmering feud between the Carsons and the Pritchards erupts over Wills accusation. Just as the families renew their bickering, Gabby Cross (Jack Carson), a publicity agent for Spectrum Pictures, arrives in town and offers the townspeople $20,000 to use Carsons Corners as a filming location for a movie he is making about Wild Bill Carson, Wills grandfather and the founder of Carson Corners. Still angered by Wills undermining his highway plan, Calvin refuses Gabbys offer. His youngest daughter, Jane (Jane Withers), suggests a compromise that would allow Spectrum Pictures to use the town as a filming location if the highway proposal were approved by the Carsons.
 Robert Lowery), begins to court Marjorie. Wanting her sister to marry Will, Jane and the sheriff devise a plan to frighten Merritt out of town, telling him a lynch party is after him. After Merritt leaves town, the head of Spectrum Pictures threatens to sue Pritchard for the defection. Gabby suggests giving the part to Will, who agrees on the condition that Pritchard extend the Carson mortgages.

While the movie is being filmed, three gangsters arrive in town. During a bank hold-up scene, the three gangsters put on actors costumes and steal the money from the bank. Learning of the theft, Will pursues the gangsters on horseback, catches them, and brings them back to Carson Corners with the money. Wills heroic actions wins the respect of the Pritchards, as well as Margories respect and hand in marriage. 

==Cast==
* Gene Autry as Will Carson
* Jane Withers as Jane Pritchard
* Marjorie Weaver as Marjorie Pritchard
* Frank M. Thomas as Calvin Pritchard Robert Lowery as Bob Merritt
* Kay Aldridge as Evelyn Trent
* Hobart Cavanaugh as Constable Clem Perkle
* Jack Carson as Gabby Cross
* Hamilton MacFadden as J. Wallace Rutledge
* Charles B. Middleton as Hod Carson
* Ed Brady as Mort Carson
* Tom London as Eph Carson
* Eddie Acuff as Andy Carson
* Pat OMalley as Lem Pritchard
* George Chandler as Charles Pritchard
* Champion as Genes Horse (uncredited)    

==Production==

===Background===
The person responsible for bringing Gene Autry to 20th Century Fox for Shooting High was Jane Withers, at the time the number 6 box office draw in the country. Magers 2007, p. 160.  The thirteen-year-old movie star called Joseph Schenck, then head of 20th Century Fox, directly telling him she wanted to do a film with Gene Autry. Schenck liked the idea, but suspected Republic Pictures would never load out the singing cowboy to another studio. Withers then called the head of Republic Pictures, Herbert Yates: When the studio operator answered, I told her I would like to talk to the head of the studio—(I didnt even know his name at the time). She said, Little girl, a lot of people would like to talk to Mr. Yates, but hes a busy man. I told her, Well, Im a busy girl. My name is Jane Withers, and could you please connect me? The operator screamed, The Jane Withers, the actress? I am sure Mr. Yates would be thrilled to speak with you. Mr. Yates was in an important conference, but she said shed take a note into him; hed definitely want to talk to me. I waited for awhile and finally he came on the phone. Hello, is this Jane Withers? Im Herbert Yates and Im a big fan of yours! I told him I had a terrific idea—I wanted to make a picture with Gene Autry—and he said hed love to borrow me.I had to explain that Mr. Schenck wouldnt loan me and thought he wouldnt loan Gene, but that I had a great idea and that, Honest Injun, I would not take up more than 15 minutes of huis time.He had a board meeting, but I was getting out of school at noon and could meet with him around 2:30. He said hed explain to the others and leave the meeting whenI arrived. True to his word, Mr. yates left the meeting.

I explained that perhaps Fox could loan Republic two or three of their stars in exchange for Gene, since neither studio would loan us to the other, outright. He thought that a good idea, so we called Fox. Mr. Schencks secretary said he was in an important meeting. I told her if she went in and slipped him a note, saying Jane Withers was at Republic in Mr. Yates office, he might come out and talk to us. And he did! I wouldn’t take no for an answer. Not when I knew this would be good for everyone concerned. I was afraid Mr. Schenck would be mad at me, but he wasnt! He thought it a wonderful idea! Mr. Yates told him he had a very determined young lady with a very credible idea! It was like having a baby—it took nine months to put the deal through, but three of Foxs stars were loaned to Republic in exchange for Gene. And, as I thought, the picture was enormously successful! It was one of the biggest box office pictures of the 39-40 season. 

I was happy as a lark to finally get to work with Gene Autry. We remained friends ever since. When he would be on the road, he would carve things and send them to me. I have a little slide ornament he made from the horn of a cow—it is white with 2 turquoise eyes. I have it beside a little gold watch with engraving he gave me, which I havent worn since I was a little girl. I kept the little box he sent it in. The inscription reads For Janie—love and kisses from two-gun Autry. I have three scarves he wore in Shooting High. He got all his costumes after his pictures. I have Genes shirt—embroidered, and a pastel plaid and a brown earth colored one.    }}

===Filming===
Shooting High was filmed November 18 to December 16, 1939.

===Filming locations===
* 20th Century Fox backlot, Calabasas, California, USA
* Santa Susana Pass, Simi Valley, Los Angeles, California, USA
* Corriganville Movie Ranch, Simi Valley, California, USA 

===Stuntwork===
* Foxy Callahan
* Frank McCarroll
* Henry Wills  

===Soundtrack===
* "Wanderers" (Felix Bernard, Paul Francis Webster) by Gene Autry and Jane Withers
* "Shanty of Dreams" (Gene Autry, Johnny Marvin) by Gene Autry and Jane Withers
* "Only One Love in a Lifetime" (Gene Autry, Johnny Marvin, Harry Tobias) by Gene Autry
* "Little Old Band of Gold" (Gene Autry, Charles Newman, Fred Glickman) by Gene Autry
* "On the Rancho with My Pancho" (Harry Akst, Sidney Clare)
* "Bridal Chorus (Here Comes the Bride)" (Richard Wagner)    

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 