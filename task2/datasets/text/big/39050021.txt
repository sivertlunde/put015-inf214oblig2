The Miserables
{{Infobox film
| name   = The Miserables 
| image    =
| image_size    =
| writer         = Alex Lipman Benjamin Taylor
| producer     = Peter Hort, Alex Lipman Ian Hogg
| music          = Anne Chmelewsky
| cinematography = Cenay Said
| editing        = Kieran Waller
| distributor    =
| released       =  
| runtime        = 20 minutes
| country        = United Kingdom
| language       = English
| budget         =
}} Benjamin Taylor and written by Alex Lipman. 
Released in 2010 by University of Westminster, the film was a finalist at The 38th Annual Student Academy Awards in Beverly Hills,  and has been screened at festivals in the UK, USA, China, Russia, Germany and Italy.   The film won Best Drama at the 2011 Royal Television Society Student Awards where the short was considered by the jury to be outstanding. A very good emotional script, had been creatively executed with sensitivity, flair, restraint, and imagination. 

== Plot ==
 Ian Hogg) constructs time machines out of their furniture in his efforts to relive their past and escape the present.  Evelyn is skeptical, but when her doctor insists she is to be moved to a hospice, she joins her husband in remembering their life together and attempts to discuss his life after hers.

== Cast ==

* Maggie Steed - Evelyn Pickleton Ian Hogg - Murray Pickleton Henry Everett - Doctor
* Elizabeth Muncey - Alison Scott Ellis - George

== Awards and nominations ==

{|class="wikitable" 
|-
! Category
! Result
|-
! colspan="2"| Student Academy Awards http://www.oscars.org/press/pressreleases/2011/20110425a.html 
|-
| Best Foreign Film
|  
|- Royal Television Society Student Awards 
|-
| Best Drama
|  
|-
! colspan="2"| International Film and Video Festival of Beijing
|-
| Audience Award
|  
|-
! colspan="2"| International Film Festival
|-
| Best Fiction Film
|  
|-
! colspan="2"| Marthas Vineyard International Film Festival 
|-
| Best Film
|  
|-
! colspan="2"| Screentest Student Film Festival
|-
| Best Film
|  
|-
| Best Drama
|  
|-
| Best Script
|  
|}

== Festival Screenings ==

In addition to receiving various awards and nominations, The Miserables has also been screened at many short film festivals around the world, including the following:
*2011 - Big Sur Film Festival in California, USA. 
*2011 - IULM Film Festival in Milan, Italy.
*2011 - No Limits Film Festival in Sheffield, UK. 
*2011 - Up and Coming Film Festival in Hannover, Germany. 
*2010 - VGIK International Film Festival in Moscow, Russia.

== External links ==

*  
*  

== References ==

 

 
 
 