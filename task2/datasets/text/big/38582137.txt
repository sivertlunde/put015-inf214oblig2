B.A. Pass
 
 

{{Infobox film
| name           = B.A.Pass
| image          = B A Pass Theatrical Poster.jpg
| alt            = 
| caption        = 
| director       = Ajay Bahl
| producer       = Narendra Singh (FilmyBOX) & Ajay Bahl (Tonga Talkies)
| writer         = Ritesh Shah
| based on       =   
| starring       = Shilpa Shukla Shadab Kamal Rajesh Sharma Dibyendu Bhattacharya 
| music          = Alokananda Dasgupta
| cinematography = Ajay Bahl
| editing        = Pravin Angre
| studio         = 
| distributor    = VIP Films
| released       =  
| runtime        = 95 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =    
}}
 Hindi Neo noir film, produced and directed by Ajay Bahl, and starring Shilpa Shukla, Shadab Kamal, Rajesh Sharma, and Dibyendu Bhattacharya in lead roles. It is distributed by Bharat Shahs VIP films banner. The film is based on 2009 short story, The Railway Aunty by Mohan Sikka. 

The movie was earlier scheduled to be released on 12 July 2013 but was eventually released on 2 August 2013.The date was postponed because of a major release, Bhaag Milkha Bhaag, on 12 July 2013.
==Cast==
* Shilpa Shukla as Sarika
* Shadab Kamal as Mukesh Rajesh Sharma as Khanna
* Dibyendu Bhattacharya as Johnny
*Vanya Joshi
* Vijay Kaushik as Phupha ji
* Anula Navlekar as Chotti
* Happy Ranjit as Phd
* Amit Sharma as Amit
* Geeta Aggarwal Sharma as Bua Ji
* Raveena Sharma as Sonu.

==Plot==

The story begins with the death of the parents of Mukesh, a teenager who has just graduated from senior high school. The boys grandfather puts his most unfriendly aunt as care in charge of wealth. Mukesh, meanwhile struggles to get on with his aunts family who refuses to provide him pocket money for his day to day expenses. He tries his best to study during his 1st year B.A. course. He also constantly worries about his two sisters who are put in a girls home. Mukesh develops friendship with Johnny, an undertaker, in Delhis graveyard, over the interest of playing chess. Johnny always tells him that, he has a brother in Mauritius who invites him to live there, but he cannot go as he has no money and that the day he will have enough money in hands, he will pack his bags and leave the country.

Then, Mukesh is introduced to "Sarika" aunty, one of the neighbors, during a get together. He once happens to go to Sarikas home to get apples, after which he is seduced by Sarika who uses his youthful body. She tempts him and also pays him for his services. Mukesh when questioned by his aunt about his new found money replies that he earned it by giving tutions. Sarika introduces Mukesh to many such "hungry" aunts who finally make him a male prostitute. Once, Mukesh gets into a hot scene with Sarika and Sarikas husband suddenly comes to know of this. He complains to Mukesh aunt that Mukesh had been seeking a loan from Sarika and tried to kiss her. This makes Mukeshs aunt throw him out of the house. 

Nowhere to go, Mukesh comes to Johnnys house and stays with him. Sarika refuses to stay in touch  with him and makes sure that none of her female friends who were Mukeshs clients, entertain him. Frustrated, Mukesh asks Johnny to go to Sarika and ask for the money she owes him. Johnny comes back saying that she refused to do so and threatened to call the police. 

Mukeshs sisters are having a hard time at the orphanage misdoings happening everywhere around them. In desperation, Mukesh decides to offer his sexual services even to men. In that process, he gets raped and beaten up by three troublemakers to whom he offers to have sex for money, which they obviously do not pay. 

Mukesh feels betrayed by Sarika who has been the cause for his lifes ruin. He barges into her home and demands money that is due to him. Sarika replies that she gave away his money to Johnny when Mukesh had sent him. Mukesh refuses to believe her words. Meanwhile Ashok(Sarikas husband) arrives home,sees door locked inside. He suspects adultery and threatens to disclose her to public, if she didnt open the door. Sarika draws Mukesh closer and she makes him stab her lightly to create the scene against Mukesh .In a state of shock, Mukesh stabs her three more times till her death in front of her husband and runs away to Johnnys house. 

Meanwhile, Mukeshs two sisters call him and tell him that they have absconded from the orphanage due to ill-treatment from the warden. Mukesh gets up and looks puzzled at house, he cannot find either Johnny or his things. He calls him only to find out that Johnny has left for Mauritius using his money. Looking deceived and frustrated, he receives another call from his sister to pick them up at the railway station. Unable to answer, Mukesh tells them to wait till he arrives. On his way to station, Mukesh is seen by the police. Police chase Mukesh all the way to the top of a buildings balcony. There is no way ahead and the police point a gun at him, making him stop. As the police come near him, he receives a phone call from his sisters again. With no way left, he jumps from the balcony and dies.

==Production==
;Scripting
The screenplay was written by Ritesh Shah, based on a short story, The Railway Aunty by Mohan Sikka, published in 2009 anthology, Delhi Noir edited by Hirsh Sawhney. 

;Casting
Director Ajay Bahl, had two choices for the lead role, Richa Chadda and Shilpa Shukla, whose work he had liked in Khamosh Pani. Shukla had recently done,Chak De! India, and he didnt want to go with a known face, so opted for Chaddha. However, when she was asked to act in Gangs of Wasseypur, he suggested that she do Anurag Kashyaps film, and opted for Shukla instead. 
;Filming
The film was shot by director-cinematographer, Ajay Bahl in the bylanes of Paharganj, Barakhamba Road and Sarai Rohilla area of Delhi, and the principal photography which started in July 2011 in Delhi, after two months devoted to look test, was completed in January 2012. 
;Music
Background score composed by  .
== Location ==
This film is filming mostly in  Dehli city of india
== Ajay Bahl == Queen 2014 starring Kangana Ranaut was successfully hit of the boxoffice .

==Release==
The film was premiered at the 2012 Osians Cinefan Festival of Asian and Arab Cinema in Delhi, it was commercially released all over India on 2 August 2013.   

==Critical reception==
The movie garnered an average rating of close to 3.4

==Awards==
At the 2012 Osians Cinefan Festival of Asian and Arab Cinema, the film won the Best Film Award in the Indian competition section, while debutante Shadab Kamal won the Best Actor award.   Later at the 2012 Montreal World Film Festival the film was part of the First Films World Competition,    where it was nominated for the Golden Zenith for the Best First Fiction Feature Film.

In January 2013, at the first South Asian Film Festival held in Paris it won The Prix Du Public (voted best film by the French Audience). 

*2014   for B.A. Pass-Shilpa Shukla Best Actor in a Negative Role (Female) for B.A. Pass-Shilpa Shukla
*2014   for B.A. Pass-Mohan Sikka

==See also==
* Prostitution in India

== References ==
 

==Bibliography==
* Delhi Noir (Akashic noir series), ed. Hirsh Sawhney. Akashic Books, 2009. ISBN 193335478X.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 