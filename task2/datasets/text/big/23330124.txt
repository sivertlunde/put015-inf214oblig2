Offset (film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Offset
| director       = Didi Danquart
| producer       = Philippe Avril, Andi Huber, Alfred Hurmer, Boris Michalski, Cristian Mungui, Wernfried Natter, Ada Solomon
| writer         = Didi Danquart, Cristi Puiu, Razvan Radulescu
| starring       = Alexandra Maria Lara, Felix Klare, Răzvan Vasilescu
| editing        = Nico Hain
| released       =  
| country        = Romania, France, Germany, Switzerland German
}}

Offset is a 2006 drama film directed and written by Didi Danquart. It was filmed in Bucharest.

==Plot==
The plot focuses on the lives of the soon to be married Stefan (Felix Klare), a German working in Romania for a wealthy and eccentric printing company owner, Nicu Iorga (Răzvan Vasilescu) and his soon-to-be bride Brindusa (Alexandra Maria Lara), who is Nicus secretary. Life seems great for both of them, despite the eminent long affair that Nicu had with Brindusa. All the preparations are made for the wedding, Stefans family arrives, and the  son-to-be bride and groom buy their wedding attire. A newly arrived German (who was set to replace Stefan at the printing company due to a fight with Nicu) Peter Gross (Bruno Cathomas) is invited to the wedding after he becomes friends with both Stefan and Brindusa. Nicu, armed with a gun (and accompanied by several henchmen), crashes the wedding ceremony and threatens everyone present, including Stefans father Ernst (Manfred Zapatka) and Brindusas father Mr. Hergehelegiu. He even punches Peter in the nose, after which he shoots himself and is taken to the hospital. At the hospital, Brindusa tells Stefan that he should go to Germany to pursue a better life, revealing to him that she still has feelings for Nicu. Although it is not stated in the movie, it is assumed Nicu survives the gunshot. The film ends with an enraged Stefan asking Peter about his nose just outside the hospital, after which they go down the road.

==Characters==
*Stefan - Felix Klare
*Brindusa - Alexandra Maria Lara
*Nicu Iorga - Razvan Vasilescu
*Mr Hergehelegiu - Valentin Plătăreanu
*Stefan Fathers Mr Ernst - Manfred Zapatka
*Peter Gross - Bruno Cathomas

==External links==
*  
* http://www.offset-derfilm.de/  

 
 
 
 
 

 