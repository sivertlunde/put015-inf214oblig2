The Velvet Touch
{{Infobox film
| name           = The Velvet Touch
| image          = Velvet touch poster 1948.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster Jack Gage
| producer       = Frederick Brisson
| screenplay     = Leo Rosten Adaptation: William Mercer
| story          = Annabel Ross
| narrator       = Leon Ames Leo Genn Claire Trevor
| music          = Leigh Harline Joseph Walker
| editing        = Roland Gross Chandler House
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Jack Gage. Leon Ames, Leo Genn and Claire Trevor. 

==Plot== Broadway leading lady Valerie Stanton (Russell), accidentally kills her producer and former lover, Gordon Dunning (Ames), during an argument about the direction her career should take. He expects her to sign for his next production, a typical frothy comedy for which he is known, while she wants to star in a revival of Hedda Gabler in order to prove her versatility as an actress. 

Other characters involved in the plot are Valeries new beau Michael Morrell (Genn), supporting actress Marian Webster (Trevor), who is accused of committing Valeries crime, and police Capt. Danbury (Greenstreet), who may know more than he is willing to disclose.

==Cast==
* Rosalind Russell as Valerie Stanton
* Leo Genn as Michael Morrell
* Claire Trevor as Marian Webster
* Sydney Greenstreet as Capt. Danbury Leon Ames as Gordon Dunning
* Frank McHugh as Ernie Boyle
* Walter Kingsford as Peter Gunther
* Dan Tobin as Jeff Trent
* Lex Barker as Paul Banton
* Nydia Westman as Susan Crane
* Theresa Harris as Nancy Russell Hicks as Actor "Judge Brack"
* Irving Bacon as Herbie
* Esther Howard as Pansy Dupont
* Harry Hayden as Mr. Couch

==Reception==
===Critical response===
Film critrc Bosley Crowther thought the plot and its conclusion was too obvious.  He wrote, "Since the murder is prefatory business in this new film which came to the Rivoli yesterday, we are telling no more than youll witness two minutes after the picture begins. The rest is a long and tortuous survey of Miss Russells efforts to elude discovery as the rather obvious murderess and get on with her promising career ... This foregone conclusion of the story is only one of the films weaknesses. The muddiness of the character played by Miss Russell is another one. The role was so randomly written by Leo Rosten that one finds it hard to see any solid personality or consistency in the dame." 

Film critic Dennis Schwartz praised the production and called the film  "A sparkling crime melodrama richly steeped in theatrical atmosphere."  In addition he wrote, "In this solid production, the tension is kept up until the final curtain call as to whether Russell will confess, get caught, or get away with the crime of passion." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 