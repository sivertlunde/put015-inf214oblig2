The Dream (1985 film)
 
{{Infobox film
| name           = The Dream
| image          = 
| caption        = 
| director       = Pieter Verhoeff
| producer       = 
| writer         = Pieter Verhoeff
| starring       = Peter Tuinman
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}
 Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Peter Tuinman as Wiebren Hogerhuis
* Huub Stapel as Inspecteur van politie
* Joke Tjalsma as Ymkje Jansma
* Freark Smink as Pieter Jelsma
* Hans Veerman as Commissaris van politie
* Adrian Brine as Officier van justitie
* Jan Arendsz as Allard Dijkstra

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 