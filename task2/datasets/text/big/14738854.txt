Love at First Bite (1950 film)
{{Infobox Film |
  | name           = Love at First Bite |
  | image          = LoveatFirstBiteTITLE.jpg|
  | caption        = |
  | director       = Jules White Felix Adler|
  | starring       = Moe Howard Larry Fine Shemp Howard Christine McIntyre Yvette Reynard Marie Monteil Al Thompson|
  | cinematography = Rex Wimpy | 
  | editing        = Edwin H. Bryant |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 15 54"
  | country        = United States English
}}

Love at First Bite is the 123rd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges happily reminisce about the meeting of their respective  

While the cement dries, the three doze off. When they come to, all are suffering from hangovers and short memories. Naturally, they cannot figure out how Shemp got into his situation. Realizing they must meet their sweethearts quickly, Moe and Larry load up the tub with sticks of dynamite, which explodes and send the boys flying through the air and onto the docks where their ladies are waiting.

==Production notes== remade in 1958 as Fifi Blows Her Top, using minimal stock footage. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 