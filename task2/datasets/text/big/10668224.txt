The Mad Magician
{{Infobox film
| name           = The Mad Magician
| image          = The mad magician.jpg
| director       = John Brahm
| producer       = Bryan Foy
| writer         = Crane Wilbur Mary Murphy Eva Gabor
| music          = Arthur Lange Emil Newman
| cinematography = Bert Glennon
| editing        = Grant Whytock
| distributor    = Columbia Pictures
| released       = May 1954
| runtime        = 72 min.
| country        = United States
| language       = English
}} 1954 horror film in 3D film|3D, starring Vincent Price and Eva Gabor.

==Plot== Mary Murphy). Patrick ONeal), is asked by her to intervene in the dispute between Mr. Gallico and Mr. Ormond. Mr. Gallico informs the detective that he signed a contract to Mr. Ormonds Illusions, Inc., a magicians trick provider, to invent new tricks, the issue is that Mr. Ormond owns all work created by Mr. Ormond, not just the ones produced for Illusions, Inc., Mr. Gallicos understanding.
 John Emery) arrive, before departing he asks Mr. Gallico to tell his girlfriend where and when to meet for dinner. Mr. Ormond and The Great Rinaldi are shown the buzz-saw illusions inner workings and ruminate on the performance of the trick by The Great Rinaldi and not by Mr. Gallico, angering the tricks inventor. The Great Rinaldi departs leaving Mr. Ormond and Mr. Gallico to discuss their business arrangement; Mr. Ormond dismisses Mr. Gallicos anger by explaining that Mr. Gallico was presented the opportunity to invent under the contract and that Mr. Ormonds wooing of Mr. Gallicos wife Claire (Eva Gabor) was due to her rich needs and Mr. Ormonds ability to provision them, something that Mr. Gallico was never able to do. Incensed, Mr. Gallico attacks Mr. Ormond and forces the businessman into the buzz-saw on functional mode (non-illusion) and decapitates him.  

His crime is almost revealed when Mr. Ormonds severed head is mistakenly taken for a trip with Gallicos assistant Ms. Lee and Mr. Bruce.

Gallico then impersonates Ormond to rent an apartment from Alice Prentiss (Lenita Lane), an author of mystery novels.  Gallico disposes of Ormonds body, but is again forced to murder when his ex-wife Claire discovers the impersonation.  Prentiss comes forth as a witness to the crime, but identifies Ormond as Claires murderer.  The Great Rinaldi again schemes to steal a trick of Gallicos, an illusion involving a crematorium, and ultimately winds up burned to death in the process. Gallico begins impersonating Rinaldi to take over that magicians show.

Meanwhile, Alan Bruce matches the fingerprints of Ormond with those of Rinaldi—since both sets of prints are actually Gallicos, and the novelist Prentiss realizes her boarder, and the murderer, was Gallico and not Ormond. The two, along with the assistant Karen, band together for an ultimate confrontation with Gallico.

==Cast==
{| class="wikitable"
! Actor
! Role
|-
| Vincent Price || Don Gallico / Gallico the Great
|- Mary Murphy || Karen Lee
|-
| Eva Gabor || Claire Ormond
|- John Emery || The Great Rinaldi
|-
| Donald Randolph || Ross Ormond
|-
| Lenita Lane || Alice Prentiss
|- Patrick ONeal || Police Detective Lt. Alan Bruce
|-
| Jay Novello || Frank Prentiss
|-
| Corey Allen || Gus the Stagehand (uncredited)
|}

==External links==
* 

 

 
 
 
 
 
 
 
 