The Bird of Happiness (film)
{{Infobox film
| name           = The Bird of Happiness
| image          = 
| caption        = 
| director       = Pilar Miró
| producer       = Rafael Díaz-Salgado José Luis Olaizola
| writer         = Mario Camus
| starring       = Mercedes Sampietro
| music          = 
| cinematography = José Luis Alcaine
| editing        = José Luis Matesanz
| distributor    = 
| released       = 5 May 1993
| runtime        = 115 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

The Bird of Happiness ( ) is a 1993 Spanish drama film directed by Pilar Miró. It was screened in the Un Certain Regard section at the 1993 Cannes Film Festival.   

==Cast==
* Mercedes Sampietro - Carmen
* Aitana Sánchez-Gijón - Nani
* José Sacristán - Eduardo
* Carlos Hipólito - Enrique Jr.
* Lluís Homar - Fernando
* Daniel Dicenta - Enrique
* Mari Carmen Prendes - Madre
* Jordi Torras - Padre
* Asunción Balaguer - Señora Rica
* Ana Gracia - Chica Agencia
* Eulalia Ramón - Elisa Antonio Canal - Sergio
* Felipe Vélez - Asaltante
* Diego Carvajal - Mauro
* Rafael Ramos de Castro - Ayudante Dirección
* Gabriel Garbisu - Carlos

==References==
 

==External links==
* 

 
 
 
 
 
 


 