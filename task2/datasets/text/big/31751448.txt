Weary Willies
{{Infobox Hollywood cartoon|
| cartoon_name = Weary Willies
| series = Oswald the Lucky Rabbit
| image = Weary Willies.jpg
| caption = A squirrel steals and eats the two hobos egg.
| director = Isadore Freleng
| story_artist =
| animator =
| voice_actor =
| musician =
| producer = George Winkler
| distributor = Universal Pictures
| release_date = August 5, 1929
| color_process = Black and white
| runtime = 8 min English
| preceded_by = Jungle Jingles
| followed_by = Saucy Sausages
}}

Weary Willies is an animated short produced by George Winkler which stars Oswald the Lucky Rabbit. The film is also the penultimate Oswald cartoon created during the Winkler period.

==Plot summary==
Oswald is a penniless vagabond who comes out of one of the trains cars after it makes a stop. As he steps out of the train, Oswald sees a guard passing by. Therefore, he hurries back inside and shuts the door. But because Oswalds baggage (a sack tied to a stick) is left outside, the suspicious guard decides to inspect the train. When the guard opens the door and peeps in, Oswald, coming out from another exit, kicks the officer in and makes a run for it.

Wondering on the countryside, the hungry Oswald sees a homeless bear camping under a tree. The two meet and befriend each other. They then start to fry an egg using the camp fire and other limited equipment that they have. While Oswald and the bear couldnt agree on how they should have the fried egg, a squirrel pops out from the tree and snatches it from them.

The two uneasy friends are left wondering what they should do next. Suddenly, they see roasted chicken on a window sill of a nearby house. The bear tells Oswald to filch it. Oswald is initially reluctant but ultimately agrees after getting strangled by the bear. The rabbit enters the houses yard and approaches the window, only to be chased out of the gate by the resident dog.

Oswald comes up with another idea when he notices the clothesline connects between the gate and the window. The plan works and Oswald gets his hands on the roasted chicken. Upon exiting the gate, he is spotted by a patrolling cop, prompting him to leave quickly. Passing by his friend, Oswald tosses the food to the bear who catches it. The rabbit figures hed rather run to somewhere than to be tormented by the cop.

When the bear is about to take a bite of the roasted chicken, the cop, approaching from behind, swipes it in the blink of an eye. The surprised bear then looks around and notices the cop pointing a gun at him. He too flees. As they walk away together, Oswald and the bear point fingers at each other over not having to eat anything. Meanwhile, the cop carries the roasted chicken and puts it back on the window sill. Just then, the dog, watching from one of the houses corners, thinks another act of pilferage is taking place. In this, the dog chases the cop down the road, passing by Oswald and the bear who are enjoying the incident.

==See also==
*Oswald the Lucky Rabbit filmography

==External links==
* 
*  at the Big Cartoon Database

 

 
 
 
 
 
 