Call Me (film)
{{multiple issues|
 
 
 
}}
{{Infobox Film
| name           = Call Me
| image          = Call me poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Sollace Mitchell
| producer       = Kenneth F. Martel
| writer         = Karyn Kay Sollace Mitchell
| narrator       = 
| starring       = Patricia Charbonneau Stephen McHattie Boyd Gaines Sam Freed Steve Buscemi
| music          = David Michael Frank
| cinematography = Zoltán David
| editing        = Paul Fried
| distributor    = Vestron Pictures
| released       = May 20, 1988
| runtime        = 96 min
| country        =   English
| budget         = 
| gross          = $251,819 (USA)
| preceded_by    = 
| followed_by    = 
}}

Call Me is a 1988 erotic thriller film about a woman who strikes up a relationship with a stranger over the phone, and in the process becomes entangled in a murder. The film was directed by Sollace Mitchell, and stars Patricia Charbonneau, Stephen McHattie, and Boyd Gaines. After its theatrical run, the movie was released on videocassette by Vestron Video. 

==External links==
*  

 
 
 
 
 
 


 
 