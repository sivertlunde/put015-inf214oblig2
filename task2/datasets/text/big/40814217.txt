Some Bride
{{infobox film
| title          = Some Bride
| image          = Viola Dana in Some Bride.jpg
| imagesize      =
| caption        = period advertisement featuring Viola Dana
| director       = Henry Otto
| producer       = Maxwell Karger
| writer         = Alice Duffy (story) Anne Duffy (story) June Mathis (writer) Luther Reed (writer)
| starring       = Viola Dana
| music          =
| cinematography = John Arnold
| editing        =
| distributor    = Metro Pictures
| released       = June 9, 1919
| runtime        = 5 reels
| country        = United States Silent (English intertitles)

}}
Some Bride is a 1919 American silent comedy film directed by Henry Otto and produced and distributed by Metro Pictures. It stars Viola Dana. 

==Plot==
As described in a film magazine,    Patricia Morley (Dana) is a pretty, young bride whose flirtatious ways during their honeymoon at a summer resort keep her husband Henry (Cummings) in a state of constant anxiety. Henrys jealousy is attributed to a strain of Spanish blood, although any husband would be puzzled by Patricias activities. When Patricia, at an old fashioned barn dance, acts out the role of a chicken hatching out of an egg and dances with other men due to Henrys sprained ankle, Henrys wrath blazes up, and he accuses her of being in love with another man and threatens to leave her. He packs his things, goes to New York City, and files for divorce. Patricia, brokenhearted, sends her friend Victoria (Sinclair) to tell Henry that his wife is dying. Patricia goes to the hospital where her hysterical conduct alarms the doctor and nurse. Later the nurse discovers that the bride is bluffing but is serious in her efforts to win back her husband. Henry arrives at the hospital just in time to see his wife acting out the role of a nurse to his divorce attorney Geoffrey Patten (Mason), who had broken his leg two days earlier. Henry again explodes in wrath, but finally makes up with Patricia, and they return to New York City, taking along the lawyer. Additional complications follow, but all ends happily.

==Cast==
*Viola Dana - Patricia Morley
*Irving Cummings - Henry Morley
*Ruth Sinclair - Victoria French
*Billy Mason - Geoffrey Patten
*Florence Carpenter - Jane Grayson
*Jack Mower - Undetermined role

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 


 