The Widow in Scarlet
{{Infobox film
| name           = The Widow in Scarlet
| image          = 
| caption        = 
| director       = George B. Seitz
| producer       = Ralph M. Like
| writer         = 
| starring       = Dorothy Revier Kenneth Harlan
| music          = 
| cinematography = Jules Cronjager
| editing        = Byron Robinson
| distributor    = Mayfair Pictures
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
}}

The Widow in Scarlet is a 1932 American crime film directed by George B. Seitz.   

==Cast==
* Dorothy Revier as Baroness Orsani
* Kenneth Harlan as Peter Lawton-Bond
* Lloyd Whitlock as Mandel
* Glenn Tryon as Spuffy
* Myrtle Stedman as Alice Lawton-Bond
* Lloyd Ingraham as Bradley
* Harry Strang as Hymie
* Hal Price as Eddie
* Arthur Millett as Hanson
* William V. Mong
* Phillips Smalley as Petes pal
* Wilfrid North as Petes pal
* Erin La Bissoniere as Nancy

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 