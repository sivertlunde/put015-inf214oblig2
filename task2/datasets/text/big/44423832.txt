Port of Call (2015 film)
{{Infobox film
| name           = Port of Call
| image          = PortofCall.jpg
| alt            =
| caption        = Teaser film poster
| film name      = {{Film name
| traditional    = 踏血尋梅
| simplified     = 踏血寻梅
| pinyin         = Tà Xuè Xún Méi
| jyutping       = Daap6 Hyut3 Cam4 Mui4 }}
| director       = Philip Yung
| producer       = Julia Chu
| writer         = 
| screenplay     = Philip Yung
| story          = 
| based on       =  Patrick Tam
| narrator       = 
| music          = 
| cinematography = Christopher Doyle
| editing        = Philip Yung Chu Ka-yat
| studio         = Mei Ah Film Production Golden Gate Productions    
| distributor    = Mei Ah Entertainment
| released       =  
| runtime        = 120 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = US$5 million 
| gross          = 
}}
 crime Thriller thriller film Patrick Tam. The film is based on a real murder case where a dismembered corpse of a murdered 16 year-old prostitute girl was found in Hong Kong in 2008.    Port of Call was the closing film at the 39th Hong Kong International Film Festival on 6 April 2015.   

==Cast==
*Aaron Kwok
*Elaine Jin Patrick Tam
*Jessie Li
*Michael Ning
*Jackie Cai
*Maggie Shiu
*Eddie Chan
*Hatou Yeung
*Ellen Li
*Don Li
*Ronny Yuen
*Tam Ping-man
*Noel Leung
*Tai Bo
*Chan Lai-wan

==Production==
Filming for Port of Call began in Hong Kong on 1 September 2014    and wrapped up on 26 September.   In order to prepare his role as a veteran police detective, Aaron Kwok grew a moustache and beard, which, along with his hair,were dyed in grey color, and also had to reduced his gym exercise routines. 

==See also==
*Aaron Kwok filmography

==References==
 

 
 
 
 
 
 
 
 
 
 
 