The Wolf Hunters (1949 film)
{{Infobox film
| name =  The Wolf Hunters
| image =
| image_size =
| caption =
| director = Budd Boetticher 
| producer = Lindsley Parsons 
| writer = James Oliver Curwood   Scott Darling
| narrator =
| starring = Kirby Grant   Jan Clayton   Edward Norris   Helen Parrish
| music = Edward J. Kay  
| cinematography = William A. Sickner 
| editing = Ace Herman 
| studio = Monogram Pictures
| distributor = Monogram Pictures
| released = October 30, 1949
| runtime = 70 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} novel of The Wolf series of ten films featuring Kirby Grant as a Canadian Mountie. 

==Cast==
* Kirby Grant as RCMP Corporal Rod Webb 
* Jan Clayton as Renée 
* Edward Norris as Paul Lautrec 
* Helen Parrish as Marcia Cameron  Charles Lang as J. L. McTavish 
* Ted Hecht as Muskoka 
* Luther Crockett as Supt. Edward Cameron 
* Elizabeth Root as Minnetaki  Chinook as Chinook, Webbs dog

==References==
 

==Bibliography==
* Drew, Bernard. Motion Picture Series and Sequels: A Reference Guide. Routledge, 2013. 
* McGhee, Richard D. John Wayne: Actor, Artist, Hero. McFarland, 1999. 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 