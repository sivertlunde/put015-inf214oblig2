Hot Shots (1956 film)
{{Infobox Film name = Hot Shots image = Hot Shots (1956 film).jpg image_size =  caption = Theatrical release poster  director = Jean Yarbrough producer = Ben Schwalb writer = Jack Townley narrator =  starring = Huntz Hall music =  cinematography = Harry Neumann editing =  distributor =  released =   runtime = 61 minutes country = United States language = English budget =  gross = 
}}

Hot Shots is a 1956 comedy film starring The Bowery Boys. The film was released on December 23, 1956 by Monogram Pictures and is the forty-third film in the series. It was directed by Jean Yarbrough and written by Jack Townley.

==Synopsis==
A spoiled child television star steals Sach and Dukes car. After retrieving the vehicle, the duo "teach the kid a lesson". Television executives, who are disgruntled by the child, are impressed by the duo who are then hired to watch after the boy. The childs uncle/manager is not happy with Sach and Dukes influence over the child so he gets the two fired and then kidnaps the boy for ransom, to cover up his stealing the boys earnings. Sach and Duke then rescue him.

==Cast==

===The Bowery Boys===
*Huntz Hall as Horace Debussy Sach Jones
*Stanley Clements as Stanislaus Duke Coveleskie
*David Gorcey as Charles Chuck Anderson Jimmy Murphy as Myron

===Remaining cast===
*Philip Phillips as Joey Munroe
*Joi Lansing as Connie Forbes
*Queenie Smith as Mrs. Kate Kelly
*Robert Shayne as P.M. Morley
*Mark Dana as George Slater Henry Rowland as Karl
*Isabel Randolph as Mrs. Taylor Ray Walker as Capt. W.K. Wells

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Three" on October 1, 2013.

==External links==
* 
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Fighting Trouble 1956
| after=Hold That Hypnotist 1957}}
 

 
 

 
 
 
 
 
 
 


 