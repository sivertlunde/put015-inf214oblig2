Priyanka (film)
 
 
{{Infobox film
| name           = Priyanka
| image          = 
| image_size     =
| caption        =  Neelakanta
| producer       = Neelakanta
| writer         = P. Kalaimani  (dialogues) 
| screenplay     = Neelakanta Rajkumar Santoshi
| story          = Sutanu Gupta
| starring       =  
| music          = Ilaiyaraaja
| cinematography = B. Kannan
| editing        = B. Lenin V. T. Vijayan
| distributor    =
| studio         = Neelakanta Arts
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1994 Tamil Tamil drama Prabhu playing an important role. The film, produced by Neelakanta, had musical score by Ilaiyaraaja and was released on 27 May 1994. The film is a remake of the critical acclaimed Hindi film Damini – Lightning.   

==Plot==

Priyanka (Revathi) is a straightforward person and daughter of Krishnan (Delhi Ganesh), a post master. Her father Krishnan looks for bride for her elder sister (Sudha) and her. Later, her sister elopes with another man.

Shekar ( ).Shekar reveals his love to his family and they accept it. Then, Shekar gets married with Priyanka. Gokulnath feels betrayed and decides to take revenge on Shekars family. Priyanka moves into his bungalow. One day, Priyanka and Shekhar witness, Shekhars younger brother Vinoth (Raj Sundar) and his friends, raping the young maid-servant Ganga (Seetha).

Afterwards, Ganga, in a serious condition, files a complaint against Vinoth. Ravi (Nizhalgal Ravi), a police officer, compels Priyanka to become the principal witness. To save Vinoth from this odd affair, Shekars family including Priyanka try to smother it. Thereafter, Priyanka decides to become the witness after seeing Ganga in hospital. Ravi is in fact Shekars man and arrests Vinoth. Susila (Manjula Vijayakumar), Shekars mother, orders Priyanka to leave the bungalow while Shekar is abroad. Priyanka decides to live with her sister.

The matter is taken up in court and Sriram appoints Rudrayya (Nassar), a criminal lawyer who has never lost a case. In return, Shekar asks Priyanka to forget everything and to come back home but she refuses. At the court, Priyanka is portrayed as a mentally unstable person by Rudrayya, Shekars family and surprisingly by her father Krishnan. Priyanka is subsequently sent in a mental hospital by a judicial order and Vinoth orders to kill her. Unable to bear the mental torture in the hospital, she escapes and runs into an alcoholic lawyer, Arjun (Prabhu (actor)|Prabhu). Arjun re-opens the rape case. Ganga dies in hospital with a suicide note written by Ravi, but Ganga was an illiterate. What transpires later forms the crux of the story.

==Cast== Prabhu as Arjun
*Jayaram as Shekar
*Revathi as Priyanka
*Jaishankar as Sriram, Sekhars father
*Nassar as Rudrayya
*Nizhalgal Ravi as Ravi
*Captain Raju as Gokulnath
*Venniradai Moorthy
*Delhi Ganesh as Krishnan
*Manjula Vijayakumar as Susila, Sekhars mother
*Seetha as Ganga
*Charle
*Sachu
*Viji Chandrasekhar as Kamini
*Silk Smitha
*Sudha as Priyankas sister
*Raj Sundar as Vinoth
*Oru Viral Krishna Rao Raja as himself (guest appearance)

==Awards==

1994 Film Fans Association Awards
*Best Tamil Actress – Revathi

==Soundtrack==

{{Infobox album |  
| Name        = Priyanka
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1994
| Recorded    = 1994 Feature film soundtrack |
| Length      = 26:57
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1994, features 6 tracks with lyrics written by Vaali (poet)|Vaali, Pulamaipithan and Muhammed Metha.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Durga Durga || K. S. Chithra || 2:59
|- 2 || Jilla Mulukka || Mano (singer)|Mano, K. S. Chithra || 5:10
|- 3 || Nyabagam Illaiyo  (duet)  || Ilaiyaraaja, S. Janaki || 5:00
|- 4 || Nyabagam Illaiyo  (solo)  || Ilaiyaraaja || 2:43
|- 5 || Vanakkuyile Kuyil || S. P. Balasubrahmanyam || 4:56
|- 6 || Vettukili Vetti Vantha || Mano, Swarnalatha || 6:09
|}

==References==
 

 
 

 
 
 
 
 
 
 
 
 