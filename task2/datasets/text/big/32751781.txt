Bush Christmas (1947 film)
 
{{Infobox film name           = Bush Christmas image          = Bush Christmas.jpg caption        = director       = Ralph Smart producer       = Ralph Smart writer         = Ralph Smart narrator  John McCallum starring       = Chips Rafferty John Fernside music          = Sydney John Kay cinematography = George Heath editing        = James Pearson studio         = Childrens Film Foundation|Childrens Entertainment Films distributor    = released       =   running time   = 76 minutes country        = Australia United Kingdom language       = English  budget         = £25,000 
}} British comedy film directed by Ralph Smart and starring Chips Rafferty. It was one of the first movies from Childrens Entertainment Films, later the Childrens Film Foundation.

==Plot==
In the Australian countryside, five children are best friends, including a set of siblings, an English war evacuee, and aboriginal Neza. They boast to three strangers, Long Bill, Jim and Blue, about the mare belonging to the father of one of them. The next day the mare has gone. Suspecting the three men of stealing it, the children set off to recover it.

They discover the horse thieves and harass them by stealing their food and shoes. They get trapped when the thieves trap them in an old ghost town, but are rescued in time.

==Cast==
*Chips Rafferty as Long Bill
*John Fernside as Jim
*Stan Tolhurst as Blue
*Helen Grieve as Helen
*Nick Yardley as Snow
*Morris Unicomb as John
*Michael Yardle as Michael
*Neza Saunders as Neza
*Pat Penny as father
*Thelma Grigg as mother 
*Clyde Combo as Old Jack
*Edmund Allison as policeman

==Production==
Childrens Entertainment Films had been set up by Mary Field for the Rank Organisation to make films to be screened to children in cinema clubs throughout England on Saturday mornings.   

Bush Christmas was originally planned as a serial, but it was then decided to turn it into a feature. 

Several cast members from The Overlanders appear, including Chips Rafferty, John Fernside and Helen Grieve. Grieve was the first choice for her role. Michael and Nick Yardley were brothers who had worked in radio. Neza Saunders came from a mission station near Rockhampton and was discovered by Chips Rafferty. Morris Unicomb was a veteran of stage and radio.    
 Blue Mountains and the Burragorang Valley. 

Post production was completed in Sydney by June 1947.

==Release==
Reviews were positive.   The film was very popular in Britain and Australia  and was seen in 41 countries. 

It was serialised in childrens magazines and a novelisation of the script was published. The movie was also adapted for radio with a young John Meillon. 

Ralph Smart announced plans to make further childrens films in Australia, including a serial about a family living in the outback, but these did not come to fruition. 

Helen Grieve retired from acting to study science.  Child actor Nick Yardley later had his face smashed by a boomerang. 

==See also==
* Bush Christmas 1983 remake

==References==
 

==External links==
* 
*  at Australian Screen Online
*  at  
*  at Oz Movies

 

 
 
 
 
 
 
 
 
 
 
 
 