Carry On Up the Jungle
 
{{Infobox film
| name           = Carry On Up the Jungle
| image          = Carry On Up The Jungle film.jpg
| image_size     =
| caption        = film poster by Renato Fratini
| director       = Gerald Thomas
| producer       = Peter Rogers
| writer         = Talbot Rothwell Charles Hawtrey Joan Sims Terry Scott Kenneth Connor Bernard Bresslaw Jacki Piper Eric Rogers
| cinematography = Ernest Steward
| editing        = Alfred Roome
| distributor    = Rank Organisation
| released       = March 1970 (UK)
| runtime        = 89 mins
| country        = United Kingdom
| language       = English
| budget         = £210,000
}} the series Charles Hawtrey, Joan Sims, Terry Scott and Bernard Bresslaw. Kenneth Williams is unusually absent. Kenneth Connor returns to the series for the first time since Carry On Cleo six years earlier and would now feature in almost every entry up to Carry On Emmannuelle in 1978. Jacki Piper makes the first of her four appearances in the series. This movie is a send-up of the classic Tarzan movies.

==Plot==
In Edwardian England, camp ornithologist Professor Inigo Tinkle (Frankie Howerd) tells a less-than-raptured audience about his most recent ornithology expedition to the darkest, most barren regions of the African wilds in search for the legendary Oozlum bird, which is said to fly in ever decreasing circles until it disappears up its own rear end. Financing the expedition is Lady Evelyn Bagley (Joan Sims) and the team are led by the fearless (and lecherous) Bill Boosey (Sid James) and his slow-witted African guide Upsidaisi (Bernard Bresslaw). Also on the expedition is Tinkles idiotic assistant, Claude Chumley (Kenneth Connor) and June (Jacki Piper), Lady Bagleys beautiful but unappreciated maidservant. The journey does not get off to a good start, with a mad gorilla terrorising the campsite and the travellers realising they have ventured into the territory of the bloodthirsty "Noshas", a tribe of feared Cannibalism|cannibals.

On the first night of the expedition, at dinner Lady Bagley reveals that she has embarked on the journey to find her long-lost husband and baby son who vanished twenty years ago on their delayed honeymoon, whilst out on a walk. Her husband is believed to have been eaten by a crocodile, but she hopes to find her baby son, Cecils, nappy pin as something to remember him by. What the group do not know is watching them from the bushes is named Ug (Terry Scott), the bungling yet compassionate Tarzan-like jungle dweller that wears a loincloth and sandals. Ug has never before seen any other white people, especially a woman. The next day, June stumbles across a beautiful oasis where she saves Ug from drowning and the two begin to fall in love.

That night, Ug wanders into camp and encounters Lady Bagley in her tent (mistaking it to be Junes tent) and she is astonished to see that Ug is wearing Cecils nappy pin, and that Ug is in fact her lost son Cecil. But before they can be reunited, Ug flees in fear and Lady Bagley faints with shock. The next day, the travellers are kidnapped by the Noshas, but manage to bribe their way out of being cannibalised by giving the tribal witch doctor Tinkles pocket watch. Tinkle however delays and promises the witch doctor that their gods will bestow a sign of thanks upon them. Intending rescue, Ug accidentally catapults himself into the Nosha camp and starts a fire. In the chaos, Ug, June and Upsidaisi manage to escape but the enraged Noshas apprehend the other travellers and prepare to kill them.
 Charles Hawtrey) who was taken by the Noshas years ago, but saved and brought to Aphrodisia by the tribal women. Evelyn Bagley is infuriated that he never bothered to search for their missing son and laments she has seen him but has once again lost him. June and Ug are revealed to be living happily together and June is teaching Ug to speak English.

Bill Boosey, Prof. Tinkle and Chumley enjoy the attention given to them by the tribal women, and Tinkle and Chumley are stunned to find that their elusive Oozlum Bird is in fact a sacred animal to the Lubby-Dubby females. It transpires that the Lubby-Dubbies need the menfolk to save themselves from extinction, as no males have been born in Aphrodisia for over a century. The men think their dreams have come true....until Leda makes it clear that the Lubby-Dubby women have no intention of letting them go. Tonka implies that the last man who tried to escape Aphrodisia was murdered by the tribe.

Three months pass and the men now hate the pressures forced on them by Leda, who in turn is outraged that none of the mens "mates" have gotten pregnant. She overthrows Tonka and assumes his place, threatening harm to the men. However Upsidaisi arrives disguised as a woman and says he has brought soldiers to save them. Ug and June also search for their friends and Ug summons a stampede of animals to create chaos and enable the men to get away. During the confusing, Tinkle snatches the Oozlum Bird and the team escape along with Tonka. After the chaos, Leda and her army chase after the men, but are more interested in the trampled soldiers. She says to let the others go not needing them now that they have "some real men." Lady Bagley is reunited with her beloved son and the group return to England. Tinkle unveils his Oozlum Bird to his audience....only to find it vanished up inside itself. June and Ug are happily married with a baby, and live in a treehouse in the suburb whilst Ug goes to work in only a bowler hat and suit.

==Cast==
* Frankie Howerd as Professor Inigo Tinkle
* Sid James as Bill Boosey Charles Hawtrey as Walter Bagley/King Tonka
* Joan Sims as Lady Evelyn Bagley
* Kenneth Connor as Claude Chumley
* Bernard Bresslaw as Upsidasi
* Terry Scott as Ug the Jungle Boy/Cecil Bagley
* Jacki Piper as June
* Valerie Leon as Leda
* Reuben Martin as Gorilla
* Edwina Carroll as Nerda
* Danny Daniels as Nosha Chief
* Yemi Ajibadi as Witch Doctor
* Lincoln Webb as Nosha with girl
* Heather Emmanuel as Pregnant Lubi
* Verna Lucille MacKenzie as Gong Lubi
* Valerie Moore as Lubi Lieutenant
* Cathi March as Lubi Lieutenant
* Nina Baden-Semper as Girl Nosha
* Roy Stewart as Nosha
* John Hamilton as Nosha
* Willie Jonah as Nosha
* Chris Konylis as Nosha

==Crew==
* Screenplay – Talbot Rothwell Eric Rogers
* Production Manager – Jack Swinburne
* Director of Photography – Ernest Steward
* Editor – Alfred Roome
* Art Director – Alex Vetchinsky
* Assistant Editor – Jack Gardner
* Camera Operator – James Bawden
* Assistant Director – Jack Causey
* Continuity – Josephine Knowles
* Make-up – Goeffrey Rodway
* Sound Recordists – RT MacPhee & Ken Barker
* Hairdresser – Stella Rivers
* Costume Designer – Courtenay Elliott
* Dubbing Editor – Colin Miller
* Titles – GSE Ltd
* Producer – Peter Rogers
* Director – Gerald Thomas

==Filming and locations==
* Filming dates – 13 October-21 November 1969

* Maidenhead Library – The location for Professor Tinkles lecture. The building is now demolished but the original site is directly opposite Maidenhead Town Hall, as featured in Carry On Doctor, Carry On Again Doctor and Carry On Behind.

Pinewood Studios was used for both interior and exterior filming.

==Production and casting== Slave Girls (1968)  and more particularly Edgar Rice Burroughs Tarzan series of books and films.

Bernard Bresslaw learned all his native orders in Swahili language|Swahili; however, the "African" extras were of Caribbean origin and didnt understand. But Sid James, who was born in South Africa, recognised it and congratulated him. 
Ross and Collins, The Carry On Companion, B. T. Batsford: London, 1996. ISBN 0-7134-7967-1, p87. 

The storyline is partly referenced in the Christmas Special Carry On, when all the characters sit down for Christmas Dinner and eat the Oozlum bird instead of a traditional Turkey.
 Charles Hawtrey (born November 1914) as Walter Bagley plays the father of Ugg/Cecil Bagley Terry Scott (born May 1927) despite being merely twelve and a half years his senior. Joan Sims (born May 1930) as Lady Bagley plays his mother though she is three years his junior.

==Reception==
The film was among the eight most popular movies at the UK box office in 1970. 

==References==
 

==Bibliography==
*  
*  
*  
*  
* Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (third edition) (2007) (Reynolds & Hearn Books)
*  
*  
*  
*  
*  

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 