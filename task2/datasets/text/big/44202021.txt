It Follows
 
{{Infobox film
| name           = It Follows
| image          = It Follows (poster).jpg
| alt            = Retro Poster
| caption        = Theatrical poster in a retro 80s design
| director       = David Robert Mitchell
| producer       = {{Plainlist|
* Rebecca Green
* Laura D. Smith
* David Robert Mitchell
* David Kaplan
* Erik Rommesmo
}}
| writer         = David Robert Mitchell
| starring       = {{Plainlist|
* Maika Monroe
* Keir Gilchrist
* Daniel Zovatto
* Jake Weary
* Olivia Luccardi
* Lili Sepe
}}
| music          = Disasterpeace
| cinematography = Mike Gioulakis
| editing        = Julio C. Perez IV 
| vfx EP         = Bob Lowery
| studio         = {{Plainlist|
* Animal Kingdom
* Northern Lights Films
* Two Flints
}}
| distributor    = The Weinstein Company|RADiUS-TWC
| released       =  	
| runtime        = 100 minutes  
| country        = United States
| language       = English
| budget         = $2 million
| gross          = $16.4 million 
}}
It Follows is a 2014 American supernatural horror film written and directed by David Robert Mitchell, and starring  Maika Monroe.  The plot follows a girl pursued by a supernatural entity after a sexual encounter.  Filmed in Detroit, Michigan, It Follows debuted at the Cannes Film Festival in 2014 to critical acclaim. It was purchased by The Weinstein Companys subsidiary, RADiUS-TWC, for North American distribution. It received a limited release beginning March 13, 2015, but after a very successful limited release, the film received a wide release on March 27, 2015.

==Plot==
A girl flees her house in fear and drives to the beach where she calls her father. By morning, she has been brutally murdered.

Jay, a college student, goes to see a film with her boyfriend Hugh. While they are waiting in line, Hugh starts talking about how envious he is of young children and how they have innocent, carefree lives. When they are inside the theater, Hugh spots a young woman at the entrance. He points her out to Jay, but she, however, cannot see her. Suddenly, Hugh starts getting afraid and demands to Jay that they leave the theater. On another date, Hugh and Jay have sex in his car, before he incapacitates her with chloroform. She wakes up tied to a wheelchair and Hugh explains that he has passed her a curse through sex. He informs her that an entity which can only be seen by those with the curse will pursue Jay at walking pace. If it catches her, it will kill her and then go after the person who passed it to her. A naked woman emerges from the nearby woods and approaches them, and Hugh drops Jay off at home.

The next day at school, Jay encounters an old woman in a night gown slowly approaching her and rushes to where her sister, Kelly, works. Jays sister and her friends Yara and Paul, who has an obvious crush on Jay, decide to help and spend the night in the same house. Paul investigates a smashed kitchen window but sees no one; Jay sees a half-naked and bloodied woman walking toward her. Jay runs upstairs to the others, who cannot see the entity. When a tall man with gouged-out eyes enters the bedroom, Jay flees the house and rides a bike to a nearby playground, where her friends regroup.

Greg, the neighbor, drives them to Hughs house, now abandoned, where they find a high school picture of him. With the help of the high school, they discover that Hughs real name is Jeff Redmond and trace him to his mothers address where he is living. Jeff informs them he got the curse from a one-night stand and reiterates that Jay has to have sex with someone to get rid of it. They drive to Gregs lakehouse, where Jay learns to fire a gun. The entity, taking multiple guises, eventually catches up and attacks Jay on the lakeshore. She shoots it, only momentarily incapacitating it. Jay flees the house alone in Gregs car but narrowly avoids hitting a truck and instead crashes into a cornfield. She wakes up in the hospital with a broken arm, surrounded by Paul, Yara, Kelly, and Greg.

In the hospital, Greg sleeps with Jay to pass on the curse, and insists he does not believe in it. Later, Jay sees Greg smash the window to his own house and enter. She tries to warn the real Greg on the telephone but he does not answer. She runs into the house and finds the entity in the form of Gregs half-naked mother knocking on his door; it jumps on Greg and has sex with him as he dies.

Jay flees by car and spends the night by the beach. On a beach in the morning, Jay sees three young men on a boat. She undresses and walks into the water. Back home, Paul expresses his feelings about Jay sleeping with Greg and not him; offering to have sex with her.

The group plans to kill the entity by luring it into an abandoned swimming pool and dropping electrical devices into the water to electrocute it. On the way to the pool, Jay sees a nude man ("It") standing on the roof of her house. Jay, waiting in the pool, spots the entity and realizes it has taken the appearance of her father as it throws the devices at her. Firing at an invisible target, Paul accidentally wounds Yara, but shoots the entity in the head, causing it to fall into the pool. As it drags Jay underwater, he shoots it again and Jay escapes. The entity leaves a large cloud of blood but no body is present.

Jay and Paul have sex; afterwards, Paul drives past prostitutes in a seedy part of town. Sometime later, Jay and Paul hold hands and walk down the street while someone follows close behind.

==Cast==
* Maika Monroe as Jay Height
* Keir Gilchrist as Paul
* Olivia Luccardi as Yara
* Lili Sepe as Kelly Height
* Daniel Zovatto as Greg Hannigan
* Jake Weary as Jeff Redmond / Hugh
* Bailey Spry as Annie
* Debbie Williams as Mrs. Height
* Ruby Harris as Mrs. Redmond
* Leisa Pulido as Mrs. Hannigan
* Ele Bardha as Mr. Height
* Ingrid Mortimer as Old Woman in Pajamas
* Alexyss Spradlin as Girl in Kitchen
* Mike Lanier as Giant Man
* Don Hails as Old Naked Man

==Development and production==
 
Writer and director David Robert Mitchell conceived the film based upon recurring dreams he had in his youth about being followed: "I had it when I was very young, the nightmare. I had it several times and I still remember images from it. I didnt use those images for the film, but the basic idea and the feeling I used. From what I understand, its an anxiety dream. Whatever I was going through at that time, my parents divorced when I was around that age, so I imagine it was something to do with that."    The role that sexual transmission plays came later, from Mitchell wanting something that could transfer between people.    Mitchell started writing the film in 2011 while working on a separate film he intended to be his second feature film, however Mitchell struggled with this would be second feature and decided to make It Follows as his next film instead.    While working on the film, Mitchell realized that the concept he was working on was tough to describe and thus refused to discuss the plot when asked what he was working on, reasoning later that "When you say it out loud, it sounds like the worst thing ever."  
 George Romero and John Carpenter as influences on the films compositions and visual aesthetic. 

==Release==
It Follows premiered at the 2014 Cannes Film Festival on May 17, 2014. It was released theatrically in France on February 4, 2015 and in the UK on February 27. It was given a limited release in the US on March 13  and a wide release on March 27  in 1,200 theatres.  The film was released along the USA release in a limited screening on 27 March 2015 in Canada over Toronto based International Sales Agent company Mongrel Media. 

==Interpretations==
It Follows has sparked numerous interpretations from film critics in regard to the source of "it" and the films symbolism.  Director Mitchell stated: "Im not personally that interested in where it comes from. To me, its dream logic in the sense that theyre in a nightmare, and when youre in a nightmare theres no solving the nightmare. Even if you try to solve it."  Some critics have interpreted the film as a parable on AIDS and/or other sexually-transmitted diseases,    the sexual revolution,  and tapping into "primal anxieties" about intimacy. 

Beneath the anxieties about sex (and STDs), the film is also about death, about existential dread in the face of deaths inevitability, and about how we try (and fail) to postpone death through meaningless sex. There are three important quotations in the film that stress the theme of mortality. When Jay is in English class, her teacher is reading from T. S. Eliots "The Love Song of J. Alfred Prufrock," and Yara reads two passages on the imminence of death from Fyodor Dostoevskys The Idiot. 

The films backdrop against the derelict rows of Detroits foreclosed homes, suggests the films supernatural antagonist refers to Marxs theory of Primitive Accumulation or Harveys Accumulation by Dispossession. The wholly white, middle class cast are forced to engage as neo-liberal subjects, choosing to either carry the burden of capital expansion - labouring endlessly to keep ones head above water - or passing it on - to extract value from the labour of others. To accumulate or disposes, in Marxs terms.

==Soundtrack==
The score was composed by Rich Vreeland, better known as Disasterpeace.  It was released on February 2, 2015 over Editions Milan Music in permission of The Weinstein Company with a digital booklet.  The digital version of the album went on sale March 10, 2015.

==Reception==

=== Box office ===
It Follows opened in limited theaters on March 13, 2015 in the U.S. and Canada. It earned $163,453 in its opening weekend from four theaters at an average of $40,863 per theater, making it the best limited opening for a film released in 2015. 

The movie made its international debut in the United Kingdom on February 27, 2015 where it earned $573,290 (£371,142) on 190 screens for the #8 position. The following week, the film dropped two spots to #10 with a weekend gross of $346,005 (£229,927) from 240 screens.

As of April 5, the movie has a domestic gross of $8.9 million and an international gross of $1.6 million for a worldwide total of $10.3 million.   

=== Critical response ===
It Follows received critical acclaim,  with critics praising the acting, plot, score, and old-fashioned scares. Critics also praised Mitchells shift in tone and style, calling it "progressive" and "refreshing" and distant from his earlier works.    It currently holds a 96% "Certified Fresh" rating on review aggregator website  , the film has an 83/100 rating based on 37 critics, indicating "universal acclaim". 
 The Telegraph gave the film five out of five stars and said, "With its marvellously suggestive title and thought-provoking exploration of sex, this indie chiller is a contemporary horror fans dream come true."    Ignatiy Vishnevetaky of The A.V. Club said, "Despite all the fun-to-unpack ideas swirling around Mitchell’s premise, this is first and foremost a showcase for his considerable talents as a widescreen visual stylist, which are most apparent in the movie’s deftly choreographed, virtuoso 360 degree pans." 
Mike Pereira of Bloody Disgusting described the film as a "creepy, mesmerizing exercise in minimalist horror". 

==Sequel==
Following the films success,  Radius-TWC co-president Tom Quinn announced that the studio is looking into a possible sequel.  Quinn has expressed the idea of flipping the concept of the first film around, with Maika Monroes Jay or another protagonist going down the chain to find the origin of "it." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 