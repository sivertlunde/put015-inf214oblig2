The Phantom of Morrisville
 
{{Infobox film
| name           = The Phantom of Morrisville
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Borivoj Zeman
| producer       = 
| writer         = Borivoj Zeman Frantisek Vlcek
| narrator       = 
| starring       = Oldrich Nový
| music          = 
| cinematography = Jirí Tarantík
| editing        = Josef Dobrichovský
| studio         = 
| distributor    = 
| released       = 15 July 1966
| runtime        = 89 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech comedy film directed by Borivoj Zeman. It was released in 1966.

==Cast==
* Oldrich Nový - Drummer Emil / Sir Hannibal Morris
* Kveta Fialová - Lady Clarence Hamilton - Hanibals fiancee
* Jana Novaková - Mabel - Hanibals secretary
* Vít Olmer - Allan Pinkerton
* Waldemar Matuska - Manuel Díaz - adventurer
* Jaroslav Marvan - Inspector Brumpby from Scotland Yard
* Frantisek Filipovský - Doctor Stolly
* Jan Skopecek - Servant John
* Jaroslav Rozsíval - Dixi
* Jaroslav Heyduk - Drummond - called Ruzenka
* Otto Simánek - Miky - called Kuratko (Chicken)
* Vlasta Fabianová - Arabella - the Gray Lady
* Lubomír Kostelka - Ind Abu - guardian tigers Rudolf Deyl - Coroner
* Natasa Gollová - Lady White - shelter owner for incorrigible criminal

==External links==
*  

 
 
 
 
 
 


 
 