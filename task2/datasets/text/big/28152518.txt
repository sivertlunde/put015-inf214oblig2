The Betty Boop Movie Mystery
{{Infobox Film
| name = Betty Boop Movie Mystery (1989)
| image = 
| caption = 
| director = George Evelyn 
| producer = 
| writer = Ali Marie Matheson Michael Bell
| distributor = King Features
| released = 1989
| runtime = 24 minutes
| country = United States
| language = English
}}
 animated special starring Betty Boop.

==Story== Bimbo the musical dog and Koko the Clown. Betty and her friends entertain the customers with a Hawaiian hula revue, the trio are seen entertaining the customers by Diner Dan who owns the diner, he gets very angry and fires Betty and her friends. While searching for a new job they bump into detective Sam Slade, who hires Betty and her pals to go undercover for him as musical detectives to keep an eye on Hollywood movie star Lola DaVilles diamond necklace. The lights go out and Lolas necklace is stolen. Betty is left holding the smoke gun, the police arrest Betty, and she is carted off to jail. Bimbo and Koko break Betty out of jail, then head over to Moolah Studios where they find out that Lolas secretary, Miss Green, was behind the robbery, and that her accomplice was the detective Sam Slade. The pursuit ends on a Busby Berkeley set, Lola DaVille receives her diamond necklace and Sam Slade and Miss Green are both arrested by the police. A singing telegram from Bettys old boss, Diner Dan, pleads Betty and her friends to return to the diner, to which Betty agrees. Betty then finishes the story by singing "You dont have to be star to be star," and says the best place to be is with your friends.

==Cast==
*Melissa Fahn as Betty Boop
*Lucille Bliss
*Hamilton Camp
*Jodi Carlise
*Bill Farmer
*Toby Gleason
*Gregory Jones
*John Stephenson
*Randi Merzon
*Roger Rose
*James Ward

==External links==
*  at Imdb
*  at bcdb

 
 
 
 
 
 
 
 
 
 
 
 


 