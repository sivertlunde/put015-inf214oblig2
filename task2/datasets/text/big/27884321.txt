La Vampire Nue
{{infobox film
| name = La Vampire Nue
| image = Nue,_jean_rollin-1969.jpg
| caption = Original theatrical poster
| director = Jean Rollin
| producer =
| writer = Jean Rollin S.H. Mosti
| starring = Christine François Olivier Rollin Maurice Lemaitre Bernard Musson Jean Aron Ursule Pauly Catherine Castel Marie-Pierre Castel Michel Delahaye
| music = Yvon Gerault
| cinematographer =
| editing =
| distributor = ABC Films
| released =  
| runtime = 90 minutes
| country = France
| language = French
}}
La Vampire Nue (English title: The Nude Vampire)is a 1970 film directed by Jean Rollin and is his second vampire film, it concerns a suicide cult led by a mysterious man known as "The Master". The film was influenced by the 1963 Georges Franju classic Judex (1963 film)|Judex. 

==Plot==
In a strange laboratory men in weird masks take the blood of a naked young woman.  Another woman in an orange night-gown is wandering the streets and is followed by a group of people also in weird masks, the woman comes across a man named Pierre who tries to help her but the masked men corner them and shoot the woman, Pierre escapes unharmed.  The masked men take the woman into a building and the man follows, guests then arrive for some sort of party, but Pierre cant get into the building.  His father is behind it.

He gatecrashes the next party and a woman commits suicide in front of the other guests when a man shows her picture up on a projector, the woman in the orange night-gown appears and drinks the womans blood.  Pierres face then appears on the projector,  The other guests turn Pierre, he escapes and is stopped by a man in a white cape who tells him to go to his fathers office where more mysteries await him.  Pierre goes to his father office and confronts him, he explains that the girl he saw is his protégée, shes an orphan and he was a friend of her family and that she has also got an unknown blood condition that any wounds will heal right away, she is also believed to be a goddess by certain fanatics.  What he is saying is that she is a vampire.  The building they are working in is to find someone with the same condition so that they can find a cure.  The reason for the hoods and masks are that to hide human faces from her, so that she does not know she is different.  They are hiding her from a group of vampires.

The Vampire in the white cape takes the woman and tells Pierre to protect her.  A fight then occurs between the vampires and the humans which later leads to a beach where the woman sees the sunlight for the first time.  They explain that they are not vampires and that one day the human race will all have the power of immortality.

==Cast==
* Christine François
* Olivier Rollin as Pierre Radamante (credited as Olivier Martin)
* Maurice Lemaitre as Georges Radamante
* Bernard Musson as Voringe
* Jean Aron as Fredor
* Ursule Pauly as Solange
* Catherine Castel as Georges Servant (credited as Cathy Tricot)
* Marie-Pierre Castel as Georges Servant (credited as Pony Tricot)
* Michel Delahaye as Grandmaster
* Caroline Cartier as Vampire
* Ly Lestrong
* Pascal Fardoulis as Robert
* Paul Bisciglia as Butler
* René-Jean Chauffard

==Releases==

===VHS===
Released on 25 January 1995 in US by Artemis Entertainment.

===DVD===
Released on 13 November 2007 in the US by Redemption USA with an aspect ratio of 1.58:1, the special feathures include: English theatrical trailer, French theatrical trailer, stills gallery, short film - Amour Jaunes, stills gallery of Amour Jaunes,Blood & Dishonour book traser, La Rose de Fer trailer, Hurt trailer and Black Mass trailer.

Released on 31 January 2005 in the UK by Redemption UK with an aspect ratio of 1.58:1, with optional English audio, the special features include: English theatrical trailer, French theatrical trailer, stills gallery, publicity, video art, filmography, Jean Rollin interview, Devils Nightmare US TV spot, English Nude for Satan trailer, Triple Silence music video.

===Blu-ray===
La Vampire Nue was released in 2012 by Kino Lorber in a five Blu-ray collection, along with La Rose de Fer, Fascination (1979 film)|Fascination, Le Frisson des Vampires, and Lèvres de Sang. 5 other Blu-rays were released later that year.

==References==
 

==External links==
*  

 

 
 
 
 
 