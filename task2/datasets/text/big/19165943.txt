Man with a Gun
{{Infobox film
| name           = Man with a Gun
| image          = "Man_with_a_Gun"_(1958).jpg
| image_size     =
| caption        = Original British quad poster
| director       = Montgomery Tully
| producer       =  Jack Greenwood
| screenplay     = Michael Winner 
| narrator       =
| starring       =  Lee Patterson  Rona Anderson  John Le Mesurier Glen Mason
| music          = Ron Goodwin
| cinematography =  John Wiles
| editing        = Geoffrey Muller
| studio         = Merton Park Studios
| distributor    = Anglo-Amalgamated Film Distributors (UK)
| released       = August 1958	 (UK)
| runtime        = 60 minutes
| country        =
| language       = English
| budget         =
}}
:For the 1955 Robert Mitchum Western film, see Man with the Gun
Man with a Gun is a low budget 1958 British crime film directed by Montgomery Tully from a screenplay by Michael Winner.   It starred Lee Patterson, Rona Anderson and John Le Mesurier. 

==Plot== mob protection scheme responsible for the arson.

==Cast==
*Mike Davies - 	Lee Patterson
*Stella - 	Rona Anderson
*Harry Drayson - 	John Le Mesurier
*Joe Harris - 	Warren Mitchell
*Steve Riley - 	 Glen Mason
*Carlo - 	 Carlo Borelli
*John Drayson - 	Harold Lang
*Supt. Wood - 	Cyril Chamberlain

==Critical reception==
TV Guide called it a "so-so crime story. Despite some fast pacing in the direction, the script is too simplistic for the fare."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 
 