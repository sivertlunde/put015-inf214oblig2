Far from Vietnam
 
{{Infobox film
| name           = Far from Vietnam
| image          = Far from Vietnam.jpg
| caption        = Film poster William Klein Claude Lelouch Agnès Varda Jean-Luc Godard (segment) Chris Marker Alain Resnais (segment)
| producer       = Chris Marker
| writer         = Jean-Luc Godard Chris Marker Jacques Sternberg
| narrator       = 
| music          = 
| cinematography = 
| editing        = Jacques Meppiel
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = France 
| language       = French
| budget         = 
}}
 William Klein, Claude Lelouch, Agnès Varda, Jean-Luc Godard, Chris Marker and Alain Resnais.   

==Cast==
* Anne Bellec
* Karen Blanguernon
* Bernard Fresson as Claude Ridder
* Maurice Garrel
* Jean-Luc Godard as Himself
* Ho Chi Minh as Himself (archive footage)
* Valérie Mayoux
* Marie-France Mignal
* Fidel Castro as Himself (uncredited)

==References==
 

==External links==
* 

{{Navboxes
|title=Links to related articles
|list1=
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 