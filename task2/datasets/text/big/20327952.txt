Beautiful Dreamer (2006 film)
Beautiful Dreamer is a 2006 romantic drama film set during World War II.

==Plot==
Not soon after childhood sweethearts Joe Kelly and Claire get married, U.S. president declares war situation in the country. Joe, who is a Bomber pilot, leaves for the war. He crashes his plane, and is captured by Germany (enemy) troops and loses his memory from a strike on his head after the crash.  Joe is given a fallen comrade’s identity tags and assumes another identity. When Joe is not found in the wreckage, he is assumed missing in action. After two years, he is presumed dead.

Claire and Joes Grandfather, William Kelly, struggle making their livelihood. He tells her to go somewhere she could find something better to do. On a lunch with one of Joes colleagues and his wife, they come across Joes co-pilot, Peter, who informs them that Joe didnt die in the crash. He tells them that he had seen Joe a year after the incident in a truck and but when he called his name, he wasnt sure he heard him. Despite of Grandpas disapproval, Claire leaves to find Joe.

While pursuing her search, Claire reaches Harris and finds Joe at a cafe when her car breaks down. However, she is surprised when he doesnt recognize her and the people around call him Tommy (Thomas Warner). Joe (now Tommy) feels some connection with Claire, but cant understand what it is. Claire calls Grandpa and they go to Dr. Kessler to talk about Joes condition, who suggests them not to try to make him remember anything as it could be fatal for him. He reveals that Joe is suffering from amnesia. He tells Claire to stay close to Joe.

Joe is now a partner of a garage (where they fix planes) with William Martin. Claire takes up the job of a waitress at the Rubys (cafe where she met Joe again). William flirts with Claire when he and Joe go to the cafe for lunch, which Claire ignores. She accepts his invitation to visit his garage, though, in hope to find out something about what happened with Joe. At the garage, Claire feels jealous of a woman called Rachel Thompson who is clearly interested in Tommy (Joe). Through William, Claire finds out a little about what happened with Joe.

Tommy later had an argument with William over a business contract, which he had sighed without Tommys knowledge, due to which they could lose all their business. One night, Tommy dreams of how he got his name (Thomas Warner). Claire goes to garage next day and offers her help. She tells him that she used to work at Kellys Fields and knows somebody who could loan him till he can repay. With her help, they get a B-24 bomber, for a huge cargo they needed to deliver. While Tommy tries to fix the bomber, Claire helps him out with spare part supplies.

On one occasion, Tommy, William, Rachel (and her daughter) go to Rubys for dinner. Claire gets upset, when Rachels daughter asks who is going to marry Rachel. Next day, when Claire is leaving after delivering a few supplies at the garage, Tommy gets a flashback of his childhood. Rachel is clearly unhappy about Tommys acquaintance with Claire and tells him to stay away from her.

On dance night at Rubys, Tommy doesnt show any interest in dancing, so William and Rachel dance together, though later, Tommy agrees to dance with Claire. Tommy leaves reluctantly when he gets flashbacks of his past once again while dancing. Claire follows him. He tries to fly B-24 bomber, the one he had been trying to restore and gets flashbacks again. He recalls how his plane crashed and while saving his fellow soldier, Bobby, he himself got severely injured. Claire tries to help him, but he pushes her away in stress. Heartbroken, Claire decides to leave. She borrows money from Ruby in exchange of her wedding ring.

At the garage, Tommy and William perform a successful test on the restored B-24 bomber, with many town-folks as well as Claire observing. She leaves soon after. While flying, Tommy relives the moments when his plane crashed and he was trying to save Bobby. As he leaves his seat, William is surprised and worried, but manages to land the plane back at the garage. He takes an unconscious Tommy to the hospital.

Claire returns home to Grandpa. Tommy (now Joe again) recalls everything when at hospital. When Claire is out for a walk to the pond where Joe had proposed her, she sees the B-24 bomber fly past over her and runs after it, hoping Joe has returned. Joe shows up reuniting with her and they kiss. He gives Claire her wedding ring and the family is together again with Grandpa joining them.

==Crew==
Director Terri Farley Teruel

==Cast==
Brooke Langton - Claire Kelly  
Colin Egglesfield - Joe Kelly 
James Denton - Dr. Kessler 
Barry Corbin - Grandpa 
Rusty Schwimmer - Jeannie 
William Lee Scott - Will 
Lauren Woodland - Racheal 
Elise Jackson - Sherry

==External links==
* 

 
 
 
 
 
 
 