Mr. Blandings Builds His Dream House
 
{{Infobox film
| name             = Mr. Blandings Builds His Dream House
| image            = Blandings1948.jpg
| caption          = DVD cover
| producer         = Dore Schary Melvin Frank Norman Panama
| director         = H. C. Potter
| writer           = Eric Hodgins (novel) Melvin Frank Norman Panama (screenplay)
| starring         = Cary Grant Myrna Loy
| music            = Leigh Harline
| cinematography   = James Wong Howe
| editing          = Harry Marker
| distributor      = RKO Radio Pictures
| released         =  
| runtime          = 93 minutes English
| country          = United States
}}
 American comedy 1946 novel, illustrated by Shrek! author William Steig.

The film was a box office hit upon its release, and has remained a popular film  through cable television broadcasts and the home video market. Warner Home Video released the film to DVD with restored and remastered audio and video in 2004. In 1986 the novel was adapted for film again for the Tom Hanks, Shelley Long movie The Money Pit, and in 2007 a loose remake of the 1948 film was released under the title Are We Done Yet? 

The house built for the 1948 film still stands on the old Fox Ranch property in Malibu Creek State Park in the hills a few miles north of Malibu. It is used as an office for the Park.  
 

==Plot==
Jim Blandings (Grant), a bright account executive in the advertising business, lives with his wife Muriel (Loy) and two daughters in a cramped New York apartment. Muriel secretly plans to remodel their apartment. After rejecting this idea, Jim Blandings comes across an ad for new homes in Connecticut and they get excited about moving.

Planning to purchase and "fix up" an old home, the couple contact a real estate agent, who uses them to unload "The Old Hackett Place" in fictional Lansdale County, Connecticut. It is a dilapidated, two-hundred-year-old farmhouse. Blandings purchases the property for more than the going rate for land in the area, provoking his friend/lawyer Bill Cole (Douglas) to chastise him for following his heart rather than his head. (Cole narrates the film, smoking a pipe, an apparent nod to the stage manager character in Thornton Wilders Our Town.) 
 Revolutionary War-era, Reginald Denny) to design and supervise the construction of the new home. From the original purchase to the new houses completion, a long litany of unforeseen troubles and setbacks beset the hapless Blandings and delay their moving-in date.

On top of all this, at work Jim is assigned the task of coming up with a slogan for "WHAM" Brand Ham, an advertising account that has destroyed the careers of previous account executives assigned to it. Jim also suspects that Muriel is cheating on him with Bill Cole after Bill slept at the Blandingsess alone in the house with Muriel one night due to a violent thunderstorm.

With mounting pressure, skyrocketing expenses, and his new assignment, Jim starts to wonder why he wanted to live in the country. The Blandingsess maid Gussie provides Blandings with the perfect WHAM slogan, and he saves his job. As the film ends, Bill Cole says that he realizes that some things "you do buy with your heart."

==Reception==
According to Time magazine, "Cary Grant, Myrna Loy and Melvyn Douglas have a highly experienced way with this sort of comedy, and director H. C. Potter is so much at home with it that he gets additional laughs out of the predatory rustics and even out of the avid gestures of a   for small-town audiences, and incomprehensible abroad; but among those millions of Americans who have tried to feather a country nest with city  , it ought to hit the jackpot." 

The film recorded a loss during its initial theatrical release of $225,000. Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 420 
 100 Years...100 Laughs list.

==Cast==
  for film with Cary Grant and Myrna Loy]]
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Cary Grant || James Blandings
|-
| Myrna Loy || Muriel Blandings
|-
| Melvyn Douglas || William "Bill" Cole
|-
| Louise Beavers || Gussie
|- Reginald Denny || Henry Simms
|-
| Jason Robards, Sr. || John Retch
|-
| Lex Barker || Carpenter Foreman
|-
| Connie Marshall || Betsy Blandings
|-
| Sharyn Moffett || Joan Blandings
|-
| Ian Wolfe || Real Estate Agent Smith
|-
| Nestor Paiva || Joe Appolonio
|- Harry Shannon || W.D. Tesander
|-
| Tito Vuolo || Mr. Zucca
|}

==Promotion==
As a promotion for the film, the studio built 73 "dream houses" in various locations in the United States, selling some of them by raffle;  over 60 of the houses were equipped by General Electric, including the ones in the following cities:   

Phoenix, AZ, Little Rock, AR, Bakersfield, CA,
Fresno, CA, Oakland, CA, Sacramento, CA,
San Diego, CA, San Francisco, CA, Denver, CO,
Bridgeport, CT, Hartford, CT, Washington, DC,
Atlanta, GA, Chicago, IL, Indianapolis, IN,
South Bend, IN, Terre Haute, IN, Des Moines, IA,
Louisville, KY, Baltimore, MD, Worcester, MA,
Detroit, MI, Grand Rapids, MI, St. Paul, MN,
Kansas City, MO, St. Louis, MO, Omaha, NE,
Tenafly, NJ, Albuquerque, NM, Albany, NY,
Buffalo, NY, Rochester, NY, Syracuse, NY,
Tarrytown, NY, Utica, NY, Greensboro, NC,
Rocky Mount, NC, Cleveland, OH, Columbus, OH,
Toledo, OH, Oklahoma City, OK, Tulsa, OK, Cedar Hills, OR (near Portland), {{cite web
| url= http://vintageportland.wordpress.com/2012/01/17/mr-blandings-dream-house-1948/
| title= Mr. Blandings’ Dream House, 1948| date= January 17, 2012 
| author= The Oregonian| work= Vintage Portland 
| publisher= vintageportland.wordpress.com| accessdate=2012-11-04
| quote= The Portland-area house was built in the  Cedar Hills area of Beaverton...
on the northwest corner of SW Walker Road and Mayfield Avenue.}} 
Philadelphia, PA, Pittsburgh, PA,
Providence, RI, Chattanooga, TN, Memphis, TN,
Nashville, TN, Amarillo, TX, Austin, TX,
Austin, TX, Dallas, TX, Fort Worth, TX,
Houston, TX, Salt Lake City, UT, Seattle, WA,
and Spokane, WA.
 Worcester and East Natick, Massachusetts; Portland, Oregon; and Ottawa Hills, Ohio. Thousands lined up in front of the house in Ottawa Hills, paying admission to view the house at its opening.   
 Encanto Village). 

In Rocky Mount, North Carolina, the dream house that was built still stands at 1515 Lafayette Avenue. 

Greensboro, North Carolinas dream house was built in the Starmount Forest community. 

==Related works==
The story behind the film began as an April 1946 article written by Eric Hodgins for Fortune (magazine)|Fortune magazine; that article was reprinted in Readers Digest and (in condensed form   ) in Life (magazine)|Life before being published as a novel.   

Irene Dunne played his wife Muriel in the October 10, 1949, Lux Radio Theatre broadcast on CBS (running one hour;   Grants wife  Betsy Drake played Muriel in the June 9, 1950, broadcast on NBCs Screen Directors Playhouse (a 1/2 hour version).

==Remakes==
"The Money Pit" 1986 film starring Tom Hanks and Shelley Long.

"Drömkåken" 1993 Swedish film.
 Are We Done Yet? (a sequel to the 2005 film Are We There Yet?) starring Ice Cube, was released on April 4, 2007.

== References ==
 

==External links==
*  
* http://www.robertabalos.com/2012/01/mr-blandings-builds-his-dream-house.html

 

 
 
 
 
 
 
 
 
 
 