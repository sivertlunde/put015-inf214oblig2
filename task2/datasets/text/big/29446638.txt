World Peace and Other Fourth-Grade Achievements
{{Infobox film
| name = World Peace and Other 4th Grade Achievements
| image =
| caption = Theatrical release poster
| director = Chris Farina
| producer = Chris Farina
| starring = John Hunter (Educator)
| cinematography = Gene Rhodes
| editing = Bill Reifenberger
| distributor = Rosalia Films
| released =  
| runtime = 56 minutes
| country = United States
| language = English
}}

World Peace and Other 4th Grade Achievements is a 2010 documentary film by American filmmaker Chris Farina.  The film is centered on an 8-week, political simulation, The World Peace Game. To win the game, its fourth grade players must peacefully solve the economic, political, and environmental crises of their respective “nation teams”. The film highlights the personalities of the students but focuses their teacher and creator of the game, John Hunter. 

==Content Summary==
In the film, Hunter presents his students with the World Peace Game and the problems associated with attempting to achieve world peace  including national debt, pollution, war, water shortages, and the rights of indigenous peoples. The children, who are divided into “nation teams,” are given roles as world leaders in order to peacefully resolve the problems through negotiation and conversation. Throughout the game Hunter presents the students with daily quotes from Sun Tzu’s, “Art of War”. Through the game, Hunter, encourages the children to think for themselves by stepping out of the way and forcing them to figure problems out themselves.  The film mixes the children’s progress in the game with the life and background of Hunter whose career has been devoted to imparting the Ghandian principle of nonviolence and the importance of taking care of one another. 
==Reception==
The film premiered at the Paramount Theatre in Charlottesville, Virginia on February 21st, 2010. Stuart, Courteney.  . The Hook/  It has been recognized at several film festivals, including SXSW, the Boston Film Festival, the Newport Beach Film Festival, the Hot Springs Documentary Film Festival, and Bergen International Film Festival. The film is currently a finalist for the 2010 Japan Prize. 

In September of 2010, in recognition of this project, John Hunter was named Fellow of the Darden School of Business’ Center for Global Initiatives. 

==Book==
In 2013, a book written by the John Hunter, based on the same project that was documented in the film, and with the same title, was published by Houghton Mifflin Harcourt.  

== References==

 

 
*
*
*
*
==External links==
 
*   from Rosalia Films
*  