May Blossom (film)
{{Infobox film
| name           = May Blossom
| image          =
| alt            = 
| caption        = 
| director       = Allan Dwan
| producer       = Daniel Frohman
| screenplay     = David Belasco
| starring       = Russell Bassett Donald Crisp Marshall Neilan Gertrude Norman Gertrude Robinson
| music          = 
| cinematography = H. Lyman Broening 
| editing        = 
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Allan Dwan and written by David Belasco. The film stars Russell Bassett, Donald Crisp, Marshall Neilan, Gertrude Norman and Gertrude Robinson. The film was released on April 15, 1915, by Paramount Pictures.  

==Plot==
 

== Cast == 
*Russell Bassett as Tom Blossom
*Donald Crisp as Steve Harland
*Marshall Neilan as Richard Ashcroft
*Gertrude Norman as Aunt Deborah
*Gertrude Robinson as May Blossom 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 