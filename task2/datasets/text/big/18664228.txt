Laid to Rest (film)
{{Infobox film
| name           = Laid to Rest
| image          = Laid to rest.jpg
| alt            = 
| caption        = Film poster
| director       = Robert Green Hall
| producer       = Chang Tseng   Bobbi Sue Luther
| writer         = Robert Green Hall Kevin Gage Thomas Dekker Richard Lynch   Lena Headey
| music          = Deadbox   Blackcowboy   Suicidal Tendencies
| cinematography = Scott Winig
| editing        = Andrew Bentler
| studio         = Dry County Films
| distributor    = Anchor Bay Entertainment
| released       =   }}
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Laid to Rest is a 2009  .

== Plot ==

  mortician appears, and is impaled by a man equipped with a chrome skull mask, and a shoulder mounted camera. The girl stabs ChromeSkull in the eye, and runs off while he treats his wound. The girl is picked up by Tucker, who takes her home to his wife Cindy, and promises her that they will go to the sheriffs station in the morning, as Tuckers truck is low on gas, and their phone service has been cut off.

ChromeSkull tracks the girl (who has been nicknamed "Princess") to Tuckers house, and kills Cindy. Tucker and Princess escape in Tuckers truck as ChromeSkull deals with Cindys brother Johnny and his girlfriend, who had come by to check on Cindy. Tucker and Princess seek aid from a recluse named Steven, who uses his computer to email for help, and research ChromeSkull, discovering he is a serial killer whose modus operandi is to send tapes of himself murdering women to the authorities.

The trio go to the police station, and find the sheriff and a deputy dead, and ChromeSkull waiting for them. The group wound ChromeSkulls leg, and drive away, reaching the funeral home. Princess explores the area, and discovers that a nearby barn contains several of ChromeSkulls previous victims. ChromeSkull beheads a still-living captive, knocks Princess out, and places her in a casket. Before ChromeSkull can kill Princess, he is shot by Tucker, who drives away with her and Steven in ChromeSkulls car.

Princess looks at footage on ChromeSkulls camera, which shows he was in league with the mortician, who he killed due to the man becoming a liability. Princess takes the car while Tucker and Steven are removing bodies from the trunk, and is locked inside by ChromeSkull, who uses his cellphone to take control of the onboard computer as he follows Princess to a convenience store. Via paperwork in the car, Princess finds out ChromeSkulls name is Jesse Cromeans, right before he attacks her.

ChromeSkull prepares to kill Princess, but he is out of tapes for his camera, so he coerces her into going into the store to get one. The clerk sees the threatening messages ChromeSkull is sending Princess through a cellphone, goes out to confront him, and has his head blown off when ChromeSkull turns his own shotgun on him. A customer goes to lock the backdoor, and is decapitated by ChromeSkull. Tucker and Steven reach the store, grab ChromeSkulls suitcase on the way in, and replace the glue he uses to keep his mask on with Cyanoacrylate. ChromeSkull cuts the power, and causes Stevens face to explode by injecting Canned tire inflator into his ear. Tucker is stabbed while trying to give Princess enough time to escape with Tommy, the murdered customers friend.

ChromeSkull locks Tommy out of the building, and traps Princess in a freezer. To taunt Princess, ChromeSkull gives her his camera, which reveals she was a drug addicted prostitute he had picked up, and that she lost her memory when ChromeSkull beat her with a bat. Princess smashes the camera in frustration, and while struggling with ChromeSkull, knocks his mask off. ChromeSkull goes to reaffix his mask, unknowingly using the Cyanoacrylate that Steven had replaced his adhesive with. The chemical melts ChromeSkulls face, and as he writhes in agony, Princess bludgeons him with an aluminum bat.

Princess finds her missing persons notice (which Steven had printed off his computer) in Tuckers pocket, and leaves with Tommy. The police arrive, and find the flyer, which Princess had written an explanation on the back of.

== Cast ==
* Bobbi Sue Luther as The Girl/Princess Gemstone Kevin Gage as Tucker Smith
* Sean Whalen as Steven
* Johnathon Schaech as Johnny Thomas Dekker as Tommy
* Nick Principe as ChromeSkull / Jesse Cromeans
* Jana Kramer as Jamie
* Lucas Till as Young Store Clerk
* Seraphine   DeYoung as Bound Girl
* Anthony Fitzgerald as Anthony Richard Lynch as Mr. Jones
* Lena Headey as Cindy Smith

==Production==
 
== Release ==

Laid to Rest received a limited theatrical run in March 2009.   It was released on home video April 21, 2009. 

== Reception ==
 
Dread Central gave the film a four out of five, concluding "Laid to Rest does have some minor issues with pacing at first but really finds its stride about 30 minutes in and just keeps moving along until the very end. What I like here is that Hall was able to create a slasher film with characters we care about but definitely doesnt take anything too seriously either, giving horror fans a movie thats both entertaining and a lot of fun to watch".  DVD Talk awarded three out of five, which said that while Laid to Rest "contrives a lot of convenient coincidences to keep the plot rolling" and was "admittedly lacking in logic" it was still "an entertaining 90 minute thrill ride".  DVD Verdict also responded well to Laid to Rest, writing that it was "an inventive, sick as all get out thrill-ride" and "an entertaining, engaging slasher movie".   Scott Weinberg of Fearnet wrote, "Powerfully gory, peppered with unexpected doses of weird humor, and backed by a colorful cast of familiar faces, Rob Halls Laid to Rest is hardly the most original or trail-blazing terror tale out there -- but its an 80s-style throwback piece that gains a lot of mileage out of very little gas." 

== Sequels ==

The film has been followed by a sequel titled   and an upcoming prequel. 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 