Janji Joni
{{Infobox film name       = Janji Joni image      = Janji Joni.jpg caption    = Theatrical poster director   = Joko Anwar writer     = Joko Anwar starring  Rachel Maryam producer   = Nia Di Nata
|distributor= Focus Films (internationally)
Kalyana Shira Films (in Indonesia) budget     = 3.5 billions rupiah
|released= 27 April 2005 runtime    = 83 minutes country = Indonesia language = Indonesian
|}} 2005 Indonesian romantic comedy film directed by Joko Anwar, starring Nicholas Saputra, Mariana Renata, and Rachel Maryam.

==Plot==
Joni works as a film/reel delivery man in Jakarta. One day, while waiting for the next pick-up, Joni meets a charming girl who is going to catch a film with her boyfriend Otto. The girl would only reveal her name to Joni if he successfully delivers the reels on time. However, things do not turn out well on that day as Joni faces obstacles such as the citys notorious traffic and various people that may disrupt his task.

==Cast==
*Nicholas Saputra&nbsp;– Joni
*Mariana Renata&nbsp;– Angelique
*Rachel Maryam&nbsp;– Voni
*Surya Saputra&nbsp;– Otto
*Sujiwo Tejo&nbsp;– Adam Subandi
*Dwiky Riza&nbsp;– Toni
*Gito Rollies&nbsp;– Pak Ucok
*Fedi Nuril&nbsp;– Jeffrey

==Awards and recognition==
* Best Film 2005 in TEMPO magazine
* Best Movie, MTV Indonesia Movie Awards 2005
* Most Favorite Actor (Nicholas Saputra), MTV Indonesia Movie Awards 2005
* Most Favorite Supporting Actress (Mariana Renata), MTV Indonesia Movie Awards 2005
* Best Director (Joko Anwar), Bali International Film Festival 2005
* SET Awards 2005 (Joko Anwar)
* Best Editing (Yoga Koesprapto), Asia Pacific Film Festival 2005
* Festival Film Indonesia 2005
** Best supporting actor&nbsp;– Gito Rollies
** Penyunting Terbaik&nbsp;– Yoga K. Koesprapto

==External links==
* 
* 

 

 
 
 
 
 
 
 


 
 