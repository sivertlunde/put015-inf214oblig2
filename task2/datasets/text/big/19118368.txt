L'affittacamere
 
{{Infobox film
| name           = Laffittacamere
| image          = Laffittacamere.jpg
| caption        = Film poster
| director       = Mariano Laurenti
| producer       = Galliano Juso
| writer         = Paolo Brigenti Bruno Corbucci
| starring       = Gloria Guida
| music          = Ubaldo Continiello
| cinematography = Franco Vitale Federico Zanni
| editing        = Alberto Moriani
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}
Laffittacamere is a 1976 Italian comedy film directed by Mariano Laurenti and starring Gloria Guida.   

==Cast==
* Gloria Guida - Giorgio Mainardi
* Lino Banfi - Lillino
* Enzo Cannavale - Pasquale Esposito
* Vittorio Caprioli - Onorevole Vincenzi
* Adolfo Celi - Giudice Damiani
* Giancarlo Dettori - Avvocato Mandelli
* Fran Fullenwider - Angela, sorella di giorgia
* Giuseppe Pambieri - Anselmo Bresci
* Luciano Salce - Prof. Eduardo Settebeni
* Marilda Donà - Rosaria, moglie di Damiani
* Giuliana Calandra - Adele Bazziconi, moglie de Settebeni
* Dino Emanuelli - Notaio
* Francesco DAdda - Nipote del notaio
* Vincenzo Crocitti - Paciente in sala operatoria

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 