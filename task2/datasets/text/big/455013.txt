The Boys in the Band
 
{{Infobox film
| name           = The Boys in the Band
| image          = The Boys in the Band-1970 movie poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = William Friedkin
| producer       = Mart Crowley Kenneth Utt Dominick Dunne Robert Jiras
| screenplay     = Mart Crowley
| based on       =   Peter White
| cinematography = Arthur J. Ornitz
| editing        = Gerald B. Greenberg Carl Lerner
| studio         = Cinema Center Films
| distributor    = National General Pictures (1970,original) Paramount Pictures (2008, DVD)
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $5.5 million
| gross          = $3.5 million (US/ Canada rentals) 
}} play of the same title. It is among the first major American motion pictures to revolve around gay characters and is often cited as a milestone in the history of queer cinema.
 Peter White as Alan, Leonard Frey as Harold, Cliff Gorman as Emory, Frederick Combs as Donald, Laurence Luckinbill as Hank, Keith Prentice as Larry, Robert La Tourneaux as Cowboy, and Reuben Greene as Bernard. Model/actress Maud Adams has a brief cameo appearance as a fashion model in a photo shoot segment in the opening montage of scenes.

==Plot==
The film is set in an Upper East Side apartment in New York City in the late 1960s. Michael, a Roman Catholic and recovering Alcoholism|alcoholic, is preparing to host a birthday party for his friend Harold. Another of his friends, Donald, a self-described underachiever who has moved from the city, arrives and helps Michael prepare. Alan, Michaels (presumably straight) old college roommate from Georgetown University|Georgetown, calls with an urgent need to see Michael. Michael reluctantly agrees and invites him to come over.
 flamboyant Interior interior decorator; Hank, a soon-to-be-divorced schoolteacher, and Larry, a fashion photographer, are a couple, albeit one with monogamy issues; and Bernard is an amiable black bookstore clerk. Alan calls again to inform Michael that he isnt coming after all, and the party continues in a festive manner. But, unexpectedly, Alan has decided to drop by after all, and his arrival throws the gathering into turmoil.
 hustler and Emorys "gift" to Harold – arrives. As tensions mount, Alan assaults Emory and in the ensuing chaos Harold finally makes his grand appearance. Michael begins drinking again. As the guests become more and more intoxicated, the party moves indoors from the patio due to a sudden downpour.
 closeted homosexual, begins a telephone game in which the objective is for each guest to call the one person whom he truly believes he has loved. With each call, past scars and present anxieties are revealed. Michaels plan to "Outing|out" Alan with the game appears to backfire when Alan calls his wife, not the male college friend Justin Stewart whom Michael had presumed to be Alans lover. As the party ends and the guests depart, Michael collapses into Donalds arms, sobbing. When he pulls himself together, it appears his life will remain very much the same.

==Cast==
 
* Kenneth Nelson as Michael
* Leonard Frey as Harold
* Cliff Gorman as Emory
* Laurence Luckinbill as Hank
* Frederick Combs as Donald
* Keith Prentice as Larry
* Robert La Tourneaux as Cowboy Tex
* Reuben Greene as Bernard Peter White as Alan McCarthy
* Maud Adams (uncredited) as Photo model
* Elaine Kaufman (uncredited) as Extra/pedestrian

==Production==
Mart Crowley and Dominick Dunne set up the film version of the play with Cinema Center Films, owned by CBS Television. They originally wanted the plays director, Robert Moore, to direct the film but Gordon Stulberg, head of Cinema Center, was reluctant to entrust the job to someone who had never made a movie before. They decided on William Friedkin who had just made a film of The Birthday Party by Harold Pinter which impressed them. 

Friedkin rehearsed for two weeks with the cast. He shot a scene that was offstage in the play where Hank and Larry kiss passionately; the actors who played them were reluctant to do this on film but eventually decided to. Friedkin then cut it in editing feeling it was over-sensationalistic; he says now he wishes he had kept it in. 

The bar scene in the opening was filmed at   in New York City.  According to commentary by Friedkin on the 2008 DVD release, Michaels apartment was inspired by the real life Upper East Side apartment of actress Tammy Grimes. (Grimes was a personal friend of Mart Crowley.) Most of the patio scenes were filmed at Grimes home; the actual apartment interior would not allow for filming, given its size and other technical factors, and so a replica of Grimes apartment was built on the Chelsea Studios sound stage, and that is where the interior scenes were filmed.
 Anything Goes" The Look of Love".

==Critical reception==
The film has a pure 100% rating on Rotten Tomatoes based on 15 reviews. 

Critical reaction was, for the most part, cautiously favorable. Variety (magazine)|Variety said it "drags" but thought it had "perverse interest." Time (magazine)|Time described it as a "humane, moving picture." The Los Angeles Times praised it as "unquestionably a milestone," but refused to run its ads. Among the major critics, Pauline Kael, who disliked Friedkin, was alone in finding absolutely nothing redeeming about it.

Bill Weber from Slant Magazine wrote “The partygoers are caught in the tragedy of the pre-liberation closet, a more crippling and unforgiving one than the closets that remain.”    

Vincent Canby of the New York Times observed, "Except for an inevitable monotony that comes from the use of so many close-ups in a confined space, Friedkins direction is clean and direct, and, under the circumstances, effective. All of the performances are good, and that of Leonard Frey, as Harold, is much better than good. Hes excellent without disturbing the ensemble . . . Crowley has a good, minor talent for comedy-of-insult, and for creating enough interest, by way of small character revelations, to maintain minimum suspense. There is something basically unpleasant, however, about a play that seems to have been created in an inspiration of love-hate and that finally does nothing more than exploit its (I assume) sincerely conceived stereotypes." 

In a San Francisco Chronicle review of a 1999 revival of the film, Edward Guthmann recalled, "By the time Boys was released in 1970 ... it had already earned among gays the stain of Uncle Tomism." He called it "a genuine period piece but one that still has the power to sting. In one sense its aged surprisingly little &mdash; the language and physical gestures of camp are largely the same &mdash; but in the attitudes of its characters, and their self-lacerating vision of themselves, it belongs to another time. And thats a good thing." 

While not as acclaimed or commercially successful as director Friedkins later work, Friedkin considers this film to be one of his favorites. He remarked in the 2011 documentary Making The Boys: "Its one of the few films Ive made that I can still watch." 

==Home media==
The Boys in the Band was released on VHS videocassette Fox Home Entertainment on December 6, 1980. It was later released on laserdisc.
 Pulitzer Prize-winning writer Tony Kushner, and two of the surviving cast members, Peter White and Laurence Luckinbill; and a retrospective look at both the off Broadway 1968 play and 1970 film.

A 2011 documentary, Making the Boys, explores the production of the play and film in the context of its era. 

==Awards and nominations==
Kenneth Nelson was nominated for the Golden Globe Award for New Star of the Year - Actor. The Producers Guild of America Laurel Awards honored Cliff Gorman and Leonard Frey as Stars of Tomorrow.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 