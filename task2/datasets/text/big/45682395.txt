The Breed of the Treshams
{{Infobox film
| name           = The Breed of the Treshams
| image          =
| caption        =
| director       = Kenelm Foss
| producer       = H.W. Thompson
| writer         = Beulah Marie Dix (play) E.G. Sutherland  (play) Kenelm Foss
| starring       = Mary Odette Hayford Hobbs A.B. Imeson
| cinematography = Frank Canham
| editing        =
| studio         = Astra Films
| distributor    = Astra Films
| released       =  
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}} silent adventure film directed by Kenelm Foss and starring Mary Odette, Hayford Hobbs and A.B. Imeson.  During the English Civil War, the Royalists uncover a Roundhead spy.

==Cast==
* Mary Odette as Margaret Hungerford  
* Hayford Hobbs as Hon. Francis Tresham 
* A.B. Imeson as Hon. Clement Hungerford   Charles Vane as Vis. Dorsington 
* Margot Drake as Margaret  Fred Morgan as Capt. Rashleigh   Gordon Craig as Batty  
* Will Corrie as Cpl. Lumsford 
* Philip Hewland as Col. Henry Curwen  
* Nelson Ramsey as Col. Bagshawe 
* Norman Tharp as Capt. Stanhope  
* Farmer Skein as Lord Tresham   Gwen Williams as Mrs. Bagshawe 
* C.R. Foster-Kemp as Child  
* John Martin Harvey as Lt. Rat Reresby

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
  
 
 

 