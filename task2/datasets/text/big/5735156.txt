The Cat and the Canary (1927 film)
{{Infobox film
  | name = The Cat and the Canary
  | image = Thecatandthecanary-windowcard-1927.jpg
  | border = yes
  | caption = original 1927 window card
  | director = Paul Leni 
  | producer = Paul Kohner 
  | writer =  Walter Anthony  
  | screenplay = Alfred A. Cohn   Robert F. Hill  
  | based on =  
  | starring =Laura La Plante Forrest Stanley Creighton Hale Flora Finch
  | music = Hugo Riesenfeld
  | cinematography = Gilbert Warrenton
  | editing = Martin G. Cohn
  | distributor = Universal Pictures
  | released =  
  | country = United States
  | runtime = 82 minutes
  | language = Silent film / English intertitles
  | budget =  
}} silent horror John Willards same name. German Expressionist will 20 years later. Annabelle inherits her uncles fortune, but when she and her family spend the night in his haunted mansion they are stalked by a mysterious figure. Meanwhile, a lunatic known as "the Cat" escapes from an asylum and hides in the mansion.

The film is part of a genre of   in 1939 starring comedic actor Bob Hope and Paulette Goddard.

== Plot ==
  last will and testament remain locked in a safe and go unread until the 20th anniversary of his death. As the appointed time arrives, Wests lawyer, Roger Crosby (Tully Marshall), discovers that a second will mysteriously appeared in the safe. The second will may only be opened if the terms of the first will are not fulfilled. The caretaker of the West mansion, Mammy Pleasant (Martha Mattox), blames the manifestation of the second will on the ghost of Cyrus West, a notion that the astonished Crosby quickly rejects.

 ) is stalked in the night.]]
As midnight approaches, Wests relatives arrive at the mansion: nephews Harry Blythe ( ). If she is deemed insane, the fortune is passed to the person named in the second will. The fortune includes the West diamonds which her uncle hid years ago. Annabelle realizes that she is now like her uncle, "in a cage surrounded by cats."

While the family prepares for dinner, a guard (George Siegmann) barges in and announces that an escaped lunatic called the Cat is either in the house or on the grounds. The guard tells Cecily, "Hes a maniac who thinks hes a cat, and tears his victims like they were canaries!" Meanwhile, Crosby suspects someone in the family might try to harm Annabelle and decides to inform her of her successor. Before he speaks the persons name, a hairy hand with long nails emerges from a secret passage in a bookshelf and pulls him in, terrifying Annabelle. When she explains what happened to Crosby, the family immediately concludes that she is insane.
 hysterics and Joe Murphy). Paul and Annabelle return to her room to search for the missing envelope, and discover that Crosbys body is missing. Paul vanishes as the secret passage closes behind him. Wandering in the hidden passages, Paul is attacked by the Cat and left for dead. He regains consciousness in time to rescue Annabelle. The police arrive and arrest the Cat, who is Charlie Wilder in disguise; the guard is his accomplice. Wilder is the person named in the second will; he hoped to drive Annabelle insane so that he could receive the inheritance.

== Cast ==
* Laura La Plante as Annabelle West
* Creighton Hale as Paul Jones
* Forrest Stanley as Charles Wilder
* Tully Marshall as Roger Crosby
* Gertrude Astor as Cecily
* Flora Finch as Susan
* Arthur Edmund Carewe as Harry 
* Martha Mattox as Mammy Pleasant, housekeeper
* George Siegmann as the Guard
* Lucien Littlefield as Dr. Ira Lazar
* Hal Craig as Policeman 
* Billy Engle as Taxi Driver
* Joe Murphy as Milkman

== Production == German cinema The Cabinet wax figure display at a fair. Richard Peterson, liner notes, The Cat and the Canary (DVD, Image Entertainment, 2005). 
 Gothic horror The Ghost The Monster The Bat The Gorilla Broadway stage plays&mdash;proved successful. Steve Neale, Genre and Hollywood (London: Routledge, 2000), p. 95, ISBN 0-415-02606-7.  

Laemmle turned to John Willards popular play The Cat and the Canary, which centered on an heiress whose family tries to drive her insane to steal her inheritance. Willard hesitated in permitting Laemmle to film his play because, as historian Douglas Brode explains, "that would have exposed to virtually everyone the trick ending, ... destroying the plays potential as an ongoing moneymaker." Nevertheless, Willard was convinced and the play was adapted into a screenplay by Alfred A. Cohn and Robert F. Hill. Douglas Brode, Edge of Your Seat: The 100 Greatest Movie Thrillers (New York: Citadel Press, 2003), p. 32, ISBN 0-8065-2382-4. 

=== Casting ===
The Cat and the Canary features veteran silent film stars  ."  She received a star on the Hollywood Walk of Fame before her death in 1996 from Alzheimers disease. 

 , Gertrude Astor, Creighton Hale, Forrest Stanley, Laura La Plante, and Arthur Edmund Carewe]]
 Irish actor serial The Exploits of Elaine and D. W. Griffiths Way Down East (1920) and Orphans of the Storm (1921).  Hales role in The Cat and the Canary was to provide comedic relief. According to critic John Howard Reid, "He is forever backing into furniture or finding himself in a risqué position under a bed or wrestling with stray objects like falling books or enormous bed-springs." John Howard Reid, These Movies Won No Hollywood Awards (Lulu Press, 2005), p. 39, ISBN 1-4116-5846-9.  Hale had trouble finding a solid career in sound film. Many of his parts were minor and uncredited. 
 Show Boat (1936) and Curse of the Undead (1959) and the television series Alfred Hitchcock Presents, Studio 57, and Gunsmoke. 
 psychiatrist Dr. Ira Lazar who bore an eerie resemblance to Werner Krausss title character in The Cabinet of Dr. Caligari. 

=== Directing ===
As Universal anticipated, director Paul Leni turned Willards play into an expressionist film suited to an American audience. Historian Bernard F. Dick observes that "Leni reduced German expressionism, with its weird chiaroscuro, asymmetric sets, and excessive stylization, to a format compatible with American film practice."  Jenn Dlugos argues that "many stage play movie adaptations   fall into the trap of looking like a stage play taped for the big screen with minimal emphasis on the environment and plenty of stage play overacting."  This, however, was not the case for Lenis film. Richard Scheib notes that "Lenis style is something that lifts The Cat and the Canary up and away from being merely a filmed stage play and gives it an amazing visual dynamism." 

Leni used similar camera effects found in German expressionist films such as The Cabinet of Dr. Caligari to set the atmosphere of The Cat and the Canary. The film opens with a hand wiping cobwebs away to reveal the title credits. Other effects include "dramatic shadows, portentous   Gilbert Warrenton recalled that Leni used a gong to startle the actors. Warrenton mused, "He beat that thing worse than the Salvation Army beat a drum." 

While the film contains elements of horror, according to film historian Dennis L. White it "is structured with an end other than horror in mind. Some scenes may achieve horror, and some characters dramatically experience horror, but for these films conventional clues and a logical explanation, at least an explanation plausible in hindsight, are usually crucial, and are of necessity their makers first concern." 
 The Chinese The Man blood poisoning. 

== Reception and influence ==
 

The Cat and the Canary debuted in New York Citys Colony Theatre on September 9, 1927,   and was a "box office success".  Variety (magazine)|Variety opined, "What distinguishes Universals film version of the ... play is Paul Lenis intelligent handling of a weird theme, introducing some of his novel settings and ideas with which he became identified .... The film runs a bit overlong .... Otherwise its a more than average satisfying feature ...."  A New York Times review expounded, "This is a film which ought to be exhibited before many other directors to show them how a story should be told, for in all that he does Mr. Leni does not seem to strain at a point. He does it as naturally as a man twisting the ends of his mustache in thought."  Nonetheless, as film historian Bernard F. Dick points out, " xponents of Caligarisme, expressionism in the extreme ... naturally thought Leni had vulgarized the conventions  ".  Dick, however, notes that Leni had only "lighten    so they could enter American cinema without the baggage of a movement that had spiraled out of control." Bernard F. Dick, City of Dreams: The Making and Remaking of Universal Pictures (Lexington: University Press of Kentucky, 1997), p. 56, ISBN 0-8131-2016-0. 
 Hitchcock cited it as an influence."   Tony Rayns has called the film "the definitive haunted house movie .... Leni wisely plays it mainly for laughs, but his prowling, Friedrich Wilhelm Murnau|Murnau-like camera work generates a frisson or two along the way.  It is, in fact, hugely entertaining ...."   John Calhoun  feels that what makes the film both "important and influential" was "Lenis uncanny ability to bring out the periods slapstick elements in the storys hackneyed conventions: the sliding panels and disappearing acts are so fast paced and expertly timed that the picture looks like a first-rate door-slamming farce .... At the same time, Leni didnt short-circuit the horrific aspects ...." 

Although not the first film set in a supposed haunted house, The Cat and the Canary started the pattern for the "old dark house" genre.  The term is derived from English director James Whales The Old Dark House (1932), which was heavily influenced by Lenis film, Clarens, Illustrated History of Horror, p. 57.  and refers to "films in which murders are committed by masked killers in old mansions."  Supernatural events in the film are all explained at the films conclusion as the work of a criminal. Other films in this genre influenced by The Cat and the Canary include The Last Warning, House on Haunted Hill (1959), and the monster films of Abbott and Costello and Laurel and Hardy.  

A tinted version produced by David Shepard, film preservationist, was released on both VHS and DVD in 1997 and 2005 by Image Entertainment. The 2005 "Special Edition" contains an original score by Franklin Stover. The original black-and-white version airs infrequently on the cable television network Turner Classic Movies.

==Other film versions==
The Cat and the Canary has been filmed five other times. Rupert Julians The Cat Creeps (1930) and the Spanish language La Voluntad del muerto (The Will of the Dead Man) directed by George Melford and Enrique Tovar Ávalos were the first talkie versions of the play; they were produced and distributed by Universal Pictures in 1930.  Although the first sound films produced by Universal, neither was as influential on the genre as the first film and The Cat Creeps is lost film|lost. 
 The Cat Paramount and acquired the rights to the 1939 version.
 Katten och The Cat Richard Gordon, who explains why he and Metzger made their film version: "Well, it hadnt been done since the Bob Hope version, it had never been done in color, it was a well-known title, had a certain reputation, and it was something that logically could or in fact should be made in England." 

== References ==
 

== Further reading ==
 

* Everson, William K. American Silent Film. New York: Da Capo Press, 1998. ISBN 0-306-80876-5.
* Hogan, David. Dark Romance: Sexuality in the Horror Film. Jefferson, N.C.: McFarland, 1997. ISBN 0-7864-0474-4.
* MacCaffrey, Donald W., and Christopher P. Jacobs. Guide to the Silent Years of American Cinema. Westport, Conn.: Greenwood Press, 1999. ISBN 0-313-30345-2.
* S. S. Prawer|Prawer, S. S. Caligaris Children: The Film as Tale of Terror. New York: Da Capo Press, 1989. ISBN 0-306-80347-X.
* Worland, Rick. The Horror Film: A Brief Introduction. Malden, Massachusetts: Blackwell Publishing, 2007. ISBN 1-4051-3902-1.

 

== External links ==
 
*  
*   at Rotten Tomatoes
*   at Allmovie
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 