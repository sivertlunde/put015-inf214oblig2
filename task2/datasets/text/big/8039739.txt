It Ain't Hay
 
{{Infobox film
| name = It Aint Hay
| image = itainthay.jpg
| caption = Theatrical release poster
| director = Erle C. Kenton
| producer = Alex Gottlieb John Grant
| starring =Bud Abbott Lou Costello Grace McDonald Shemp Howard Cecil Kellaway Eugene Pallette
| music = Harry Revel
| cinematography = Frank Gross
| distributor = Universal Pictures
| released =  
| runtime = 80 min
| country = United States
| language = English
| budget =
}}
It Aint Hay is a 1943 film starring the comedy team of Abbott and Costello. 

==Plot==
Wilbur Hoolihan (Lou Costello) accidentally kills a hack horse owned by King OHara (Cecil Kellaway) and his daughter, Princess (Patsy OConnor) by feeding it candy.  In the hopes of raising enough money to replace it, he and his friend Grover Mockridge (Bud Abbott) visit a gambling parlor.  They are successful in raising the money, but before they can purchase a new horse, a con man swindles Wilbur out of his cash.  

They are informed by some touts that an old horse is available for nothing at one of the tracks.  They visit the track and mistakenly take the wrong horse, a champion by the name of Tea Biscuit.  They present the horse to OHara as a replacement for his deceased horse.
 Samuel Hinds) offers a reward for Tea Biscuit. By this time OHara has taken a fare up to Saratoga. Wilbur and Grover, realizing their error, drive to Saratoga. The three touts also realize that Wilbur and Grover took Tea Biscuit, and trail them hoping to recover the horse and collect the reward. Wilbur and Grover manage to find OHara and hide Tea Biscuit in their hotel room.  But they are hounded by the house detective, Warner (Eugene Pallette), who was tipped off by the touts. Wilbur and Grover head to the race track in time for a big race. Grover makes a deal with Warner: for $100 he will give him the horse Wilbur rides. Grover then uses that money to bet on Tea Biscuit.  Before the race, Wilbur is thrown off Tea Biscuit and lands on Rhubarb. Tea Biscuit, with a real jockey aboard, wins the race. Wilbur ride Rhubarb and loses. Warner and the touts take Wilburs horse, which they believe is Tea Biscuit, to Colonel Brainard for the reward, but it is the wrong horse. Grover holds the only winning ticket on Tea Biscuit, and uses their winnings to buy OHara a real replacement horse.

There is a scene that breaks the   at the time.

==Production==
It Aint Hay is based upon the Damon Runyon story, Princess OHara, which Universal first made into a film in 1935 with Chester Morris. 

Filming of this picture began on September 28, 1942, and lasted until November 11. Lous brother Pat was used as his stunt-double in the headless horseman sequence. 

It was during production, on November 6, that Lous wife, Anne, gave birth to their son, Lou Butch Costello, Jr. 

==DVD release==
It Aint Hay was released on DVD on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection. The films studio-authorized DVD release had been delayed for many years due to legal issues with the estate of Damon Runyon.

==References==
  

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 