Susman (film)
 
{{Infobox film
| name           = Susman
| image          =  
| image_size     = 
| caption        = 
| director       = Shyam Benegal
| producer       = Shyam Benegal
| writer         = Shama Zaidi 
| narrator       = 
| starring       = Shabana Azmi Om Puri Kulbhushan Kharbanda
| music          = Vanraj Bhatia
| cinematography = Ashok Mehta
| editing        = Bhanudas Divakar
| distributor    = 
| released       = 1987
| runtime        = 140 min 
| country        = India
| language       = Hindi
| budget         = 
}}
 1987 Hindi film directed by Shyam Benegal. The film highlighted the struggle of rural handloom weavers in the wake of rapid industrialization.

The film was selected for the Indian Panorama at Filmotsav 1987. Invited to the London Film Festival, the Chicago Film Festival, the Vancouver International Film Festival, the Sydney & Melbourne Film Festivals 1987. 

==Cast==
* Shabana Azmi -  Gauramma
* Om Puri - Ramulu
* Kulbhushan Kharbanda - Narasimha
* Neena Gupta -Mandira
* Mohan Agashe - President of Handloom Cooperative Society
* K.K. Raina
* Annu Kapoor -  Lakshmaya
* Ila Arun
* Anita Kanwar
* Pankaj Kapoor
* Jayant Kripalani
* Satish Kaushik

==References==
 

==External links==
*  

 

 
 
 
 
 

 