The Pursuit of the Phantom
{{Infobox film
| name           = The Pursuit of the Phantom
| image          = 
| alt            = 
| caption        = 
| director       = Hobart Bosworth
| producer       = 
| screenplay     = Hobart Bosworth
| starring       = Hobart Bosworth Rhea Haines Helen Wolcott Courtenay Foote Myrtle Stedman
| music          = 
| cinematography = George W. Hill
| editing        = 	
| studio         = Hobart Bosworth Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

The Pursuit of the Phantom is a 1914 American drama film written and directed by Hobart Bosworth. The film stars Hobart Bosworth, Rhea Haines, Helen Wolcott, Courtenay Foote and Myrtle Stedman. The film was released on September 1, 1914, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Hobart Bosworth as Richard Alden
*Rhea Haines as Aldens sweetheart
*Helen Wolcott as Helen Alden
*Courtenay Foote as Wyant Van Zandt
*Myrtle Stedman as Helen Alden
*Emmett J. Flynn as	Van Zandts son 
*Nigel De Brulier as The Poet

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 