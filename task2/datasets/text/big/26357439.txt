Farsighted for Two Diopters
{{Infobox film
| name = Два Диоптъра Далекогледство ( )
| image = FarsightedForTwoDioptersPoster.jpg
| imagesize =
| caption = the original poster
| director = Petar B. Vasilev
| writer = Mormarevi Brothers
| starring = Georgi Partsalev Dimitar Panov Sashka Bratanova Valentin Gadzhokov Ivan Obretenov
| music = Petar Stupel
| cinematography = Yatsek Todorov SFF / a Film Unite Sredets
| released = 1976
| country = Bulgaria
| runtime = 89 min. Bulgarian
}}

Farsighted for Two Diopters (  / Dva Dioptara Dalekogledstvo) is a 1976 Bulgarian comedy film directed by Petar B. Vasilev and written by Mormarevi Brothers. The film stars Georgi Partsalev, Dimitar Panov, Sashka Bratanova, Ivan Obretenov and Valentin Gadzhokov.

The film represents the clash between the generations in the person of the conservative father Dimo Manchev (Partsalev) and his daughter (Bratanova). The accurate examining of real problems are presented spiced with a lot of humour in the typical of Mormarevi plain language and a touch of nostalgia. It is known that the character of Dimo Manchev was specially written and designed for Partsalev. 

==Plot==
Dimo Manchev (Partsalev), a 50-year-old head of family, has a conservative notion of upbringing and morality. Being familiar with his disposition, the Manchevs daughter Lili (Bratanova) married his sweetheart Plamen (Gadzhokov) in secret. The young couple cast about how to announce the marriage all the more that they are both still students studying in university. The hesitation grows when the father shows strictness even though they present their relation as some university friendship. Lili and Plamen find a cordial welcome by the Lilis grandfather and Manchevs father - Old Pano (Panov). The culmination advances when they are caught in the Old Panos house by the accidentally passing through the village Manchev. Gradually, Dimo Manchev starts accepting their relationship and even become the main (and the only) organizer of the wedding day ceremony. The young couple are horrified at the prospect of some old-fashioned wedding banquet with a great number of guests. They own to having signed and that actually they are already husband and wife. Manchev is devastated but doesnt give up, especially after learning that the Plamens parents were also initially against this marriage. Being respected dentists in a small town, they look down on Manchevs working-class background. He cannot swallow the insult and declares willingness to take charge of the newly married couple. Finally, there is a happy end the more so as a baby is born and the Old Pano words remain: "...Dimo, Dimo, they can be without you, but you cant be without them..."

==Production==
  Whale (1970); The Past-Master (1970))  the prospects for the film were extremely positive. Furthermore, Mormarevi had one more acquaintance here. The cinematographer of Polish origin Yatsek Todorov. They worked together on Exams at Any Odd Time (1974).

 
The title was taken from a Parcalevs character cue: "...And you should listen to me! Why do you think the elderly people become farsighted? Because they start seeing far away. I got directly two diopters..."  P. Kovachev, 50 Golden Bulgarian Films, Zahariy Stoyanov 2008 

  Bistritsa and Zheleznitsa in Pancharevo suburban district. Curiously, the actor Ivan Obretenov had no driving license and couldnt drive at all. So the car was hooked through a draw-bar to the camera-truck.

==Cast==

{| class="wikitable"
|-
! Character
! Actor
! Role
|-
| Dimo Manchev
| Georgi Partsalev
| the conservative father
|-
| Old Pano
| Dimitar Panov
| the grandfather
|-
| Lili
| Sashka Bratanova
| the Manchevs daughter
|-
| Plamen
| Valentin Gadzhokov
| the Lilis sweetheart
|-
| Spiro
| Ivan Obretenov
| Manchevs neighbour and colleague
|-
| Mila
| Valentina Borisova
| Manchevs wife
|-
| Zhorko
| Misho Milchev
| Manchevs little son (Lilis brother)
|-
| Violeta
| Irina Rosich
|
|-
|
| Mariana Alamancheva
| the official
|-
| Yuri Yakovlev
| the lawyer
|-
|
| Kiril Gospodinov
| the taxi driver
|-
|
| Violeta Bahchevanova
|
|}

==Response==
A reported 1,725,810 admissions were recorded for the film in cinemas throughout Bulgaria. 

The film was subsumed among the 50 golden Bulgarian films in the book by the journalist Pencho Kovachev. The book was published in 2008 by "Zahariy Stoyanov" publishing house.

==Notes==
 

==References==
*   
*Pencho Kovachev, 50 Golden Bulgarian Films, Zahariy Stoyanov 2008

==External links==
* 

 
 
 
 