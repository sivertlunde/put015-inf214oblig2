The Tenor – Lirico Spinto
{{Infobox film name           = The Tenor - Lirico Spinto image          =  director       = Kim Sang-man producer       = Kim Jeong-a writer         = Kim Sang-man   Choi Jong-hyeon   Kim Gwan-bin starring       = Yoo Ji-tae   Yusuke Iseya   Cha Ye-ryun music          = Kim Jun-seong  cinematography = Ju Seong-rim editing        = Shin Min-kyung studio         = More In Group distributor    =  released       =   runtime        = 114 minutes country        = South Korea   Japan   Serbia language       = Korean budget         =  gross          =  film name      = {{Film name hangul         = 더 테너 - 리리코 스핀토  hanja          =  rr             = Deo Teneo - Ririko Seupinto mr             = Tŏ Tenŏ - Ririko Sŭpinto}}
}}
The Tenor-Lirico Spinto is a 2014 biopic chronicling the life of world-famous South Korean tenor Bae Jae-chul who performed in numerous European operas, but lost his voice at the peak of his career due to thyroid cancer. However, after enduring a long period of painful rehabilitation, with the help of his wife Yoon-hee as well as his longtime friend and Japanese producer Koji Sawada, Bae regained his singing ability and half his vocal range.  Bae was portrayed by Yoo Ji-tae.      

The film premiered at the 17th Shanghai International Film Festival, and was released in South Korea on December 31, 2014. 

==Cast==
*Yoo Ji-tae as Bae Jae-chul
*Yusuke Iseya as Koji Sawada
*Cha Ye-ryun as Yoon-hee
*Nataša Tapušković

==Production== English lessons.  

Due to budget constraints, it took six years for the film to be completed.  The production crew filmed in Korea, Japan and Serbia for 18 months.   

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 
 
 