Play Dirty
 
{{Infobox film
| name = Play Dirty
| image = Original movie poster for Play Dirty.jpg
| image_size =
| caption = Andre De Toth
| producer = Harry Saltzman screenplay = Melvyn Bragg Lotte Colin story = George Marton
| starring = Michael Caine Nigel Davenport Nigel Green
| music = Michel Legrand
| cinematography = Edward Scaife
| editing = Jack Slade Alan Osbiston (uncredited)
| distributor = United Artists
| studio = Lowndes Productions Limited
| released = 1 January 1969 (UK)
| runtime = 117 minutes
| country = United Kingdom
| language = English
| budget =
}}
 1969 British Andre De SAS in North Africa during World War II. 

==Plot == BP employee British 8th Army. A Popskis Private Army-style colonel (Nigel Green|Green) is told he must have a regular officer to lead one of his units on a dangerous mission to destroy an Afrika Korps fuel depot.

Douglas, who is an expert in fuel pipes and installations, is chosen despite protesting that he is only a port contractor with an honorary commission. Douglass C.O. dismisses his pleas noting that he is wearing a British Army officers uniform and therefore cant disobey the order. So Douglas departs on what seems like a fantasy adventure when he is taken to a Senussi cafe, which is a front for Masters HQ, there Douglas bumps into Cyril Leech. Leech, a convicted criminal rescued from prison by Masters to lead his units sabotage missions, directs Douglas to Masters office. 
 Italian Army patrol. The unit is made up from convicted criminals that have been released to Masters command, these include Greek narcotics smuggler Kafkarides, Tunisian terrorist Sadok, rapist Boudesh, Turkish smuggler Kostas Manou and two homosexual Senussi tribesmen Hassan and Assine. 

Things start to turn sour for Douglas, already Leech is unresponsive and is undermining Douglass orders. It is only through their journey and their run ins with treacherous Arabs, German soldiers and a booby trap laden oasis that the criminals finally accept Douglas. Even though the men will only follow Douglas if Leech goes too, they finally realise their mission was all for nothing when they are betrayed by Blore and Masters, and only Douglas and Leech are left alive. Leech then reveals to Douglas he is still only alive because Masters is paying Leech Pound sterling|£2000 to bring Douglas back alive.

With their mission a failure and the town and the destroyed fuel dump now captured by Montgomerys 8th Army, Douglas and Leech (now disguised as Germans) decide to surrender to the British. Unfortunately, a trigger-happy British soldier opens fire and shoots them both dead before they have a chance to speak. The films infamous ending in which Douglas and Leech were accidentally killed by friendly fire is indicative of the movies cynical, anti-war theme.

==Cast==
*Michael Caine as Captain Douglas – Royal Engineers
*Nigel Davenport as Captain Cyril Leech – serving 15 years in prison for sinking his tramp steamer for the insurance 
*Nigel Green as Colonel Masters Based loosely on Vladimir Popski Peniakoff Shan Hackett was C.O. Special Forces HQ
*Patrick Jordan as Major Alan Watkins – Guards Commando Unit
*Daniel Pilon as Captain Attwood – Blores adjutant
*Bernard Archard as Colonel Homerton  
*Aly Ben Ayed as Sadok
*Enrique Avila as Kafkarides – imprisoned for smuggling arms and explosives into Egypt
*Takis Emmanuel as Kostas Manou
*Scott Miller as Boudesh
*Mohsen Ben Abdallah as Hassan
*Mohamed Kouka as Assine
*Vivian Pickles as a German Nurse

==Production ==
===Casting===
Originally titled Written in the Sand,  the film initially starred Richard Harris - who quit four days into production because of script changes that reduced the size of his role, and a lack of enthusiasm for his co-star Michael Caine. After Nigel Davenport replaced Harris, and Nigel Green replaced Davenport, the films original director René Clément resigned and executive producer André de Toth took over directing the film. 

===Filming===
The film, which was shot on location near Tabernas in Almería, Spain, is notable for several long and involved action sequences that play without principal dialogue.

== External links ==
* 
*    

==References==
 

 
 

 
 
 
 
 
 
 
 