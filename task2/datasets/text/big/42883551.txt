Man on High Heels
{{Infobox film
| name           = Man on High Heels
| image          = File:Man_on_High_Heels.jpg
| director       = Jang Jin
| producer       = Jang Jin
| writer         = Jang Jin
| starring       = Cha Seung-won Oh Jung-se
| cinematography = Lee Seong-jae
| music          = Kim Jung-woo
| editing        = Jang Jin   Yang Dong-yeop
| distributor    = Lotte Entertainment
| studio         = Jangcha & Co.
| country        = South Korea
| language       = Korean
| runtime        = 124 minutes
| budget         = 
| released       =  
| gross          =   
}} noir film written and directed by Jang Jin,  starring Cha Seung-won as a transgender homicide detective.     

==Plot== sex change operation. However, before he has a chance to do so, unexpected crises arise and interfere with his plans. A gang that suffered from the cold, hard steel of Ji-wooks handcuffs are dead set on getting their revenge against him. Ji-wook resigns and tries to go about making his dream a reality, but people close to him get sucked into the revenge plot he finds himself at the center of. When some of those people get killed and a girl named Jang-mi falls into danger, he realizes that he cant stand idly by any longer.  

==Cast==
* Cha Seung-won as Yoon Ji-wook
* Oh Jung-se as Heo Gon
* Esom as Jang-mi
* Song Young-chang as Heo Bul Kim Eung-soo as Squad leader Park
* Ahn Gil-kang as Master Park 
* Go Kyung-pyo as Kim Jin-woo
* Lee Yong-nyeo as Bada
* Lee El as Dodo
* Kim Min-kyo as Man 1
* Min Jun-ho as accountant
* Kim Ye-won as Jung Yoo-ri Lee Eon-jeong as Joo-yeon 
* Oh Ji-ho as Lee Seok
* Park Sung-woong as Prosecutor Hong
* Yoon Son-ha as Jung Yoo-jung
* Lee Moon-soo as taxi driver
* Kim Byung-ok as Dr. Jin
* Lee Hae-young as immigration staff member
* Kim Won-hae as elevator resident

==Box office== Edge of Tomorrow. It opened at sixth place on the box office, drawing 338,663 admissions.  

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 