Boston Blackie Goes Hollywood
{{Infobox film
| name           = Boston Blackie Goes Hollywood
| image          = 
| image size     = 
| caption        =  Michael Gordon
| producer       = Wallace MacDonald
| writer         = Paul Yawitz (original screenplay) Jack Boyle (character) William Wright Constance Worth
| music          = M. W. Stoloff
| cinematography = Henry Freulich
| editing        = Art Seid
| distributor    = Columbia Pictures
| released       =  
| runtime        = 68 min.
| country        = United States
| language       = English
}}

Boston Blackie Goes Hollywood is a 1942 American crime film, fourth of the fourteen Boston Blackie films of the 1940s Columbia Pictures|Columbias series of B pictures based on Jack Boyles pulp-fiction character.

==Plot summary==
 

Boston Blackie (Chester Morris) and his sidekick The Runt (George E. Stone) are called, first to a Manhattan apartment where theres $60,000 waiting in a safe, then to Hollywood, by Bostons old friend Arthur Manleder (Lloyd Corrigan) to bail him out of gangster trouble.  Naturally the police are suspicious and trail him every step of the way.  

== Cast ==
* Chester Morris as Boston Blackie William Wright as Slick Barton
* Constance Worth as Gloria Lane
* Lloyd Corrigan as Arthur Manleder Richard Lane as Inspector John Farraday
* George E. Stone as The Runt
* Forrest Tucker as Whipper
* unbilled players include Lloyd Bridges, Ralph Dunn, Cy Kendall, Cyril Ring and Virginia Sale

==References==
 	

== External links ==
*  
*  
*  

 

 
 
 
 
 