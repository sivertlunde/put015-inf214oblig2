Saan Darating Ang Umaga?
{{Infobox television |
  | show_name = Saan Darating Ang Umaga?
  | image =  
  | caption = 
  | picture_format = 480i SDTV
  | runtime = 25-35 minutes
  | creator = GMA Entertainment TV Group
  | director = Maryo J. de los Reyes
  | executive_producer = Wilma Galvante
  | starring = Yasmien Kurdi  Dion Ignacio  Lani Mercado  Joel Torre and Jacob Rica
  | theme_music_composer = 
  | opentheme = "Saan Darating Ang Umaga?" performed by Yasmien Kurdi 
  | endtheme = 
  | country = Philippines English
  | network = GMA Network
  | first_aired =  
  | last_aired =  
  | num_episodes = 80 episodes
  | website = 
  }} suspense drama series aired on GMA Network. This is the eleventh installment of Sine Novela. The original movie was released in 1983. It aired from November 10, 2008 to February 27, 2009.

From March 5, 2012 until June 20, 2012 GMA Life TV aired its English-dubbed version worldwide.

==Plot==
The eleventh offering of Sine Novela brings us a Maryo J. delos Reyes masterpiece—Saan Darating ang Umaga?

Saan darating ang Umaga? is about the Rodrigo clan led by the powerful patriarch Don Leonardo Rodrigo (Charlie Davao). Don Leonardo is a wealthy businessman who is very proud of his humble beginnings. It was only through hard work and diligence that he was able to achieve all the luxuries in life. Today, he is one of the most respected and well-known businessmen in the real estate business. Because of this, he expects his sons to Dindo Rodrigo (Gary Estrada) and Ruben Rodrigo (Joel Torre) to follow his lead.

Dindo and Ruben are architects working for their fathers company. But it was Ruben who took after his fathers ideals and compassion for the business. Not only is he talented but he is kind-hearted. He is a loving husband to Lorrie Rodrigo (Lani Mercado) and a caring father to his daughter Shayne Rodrigo (Yasmien Kurdi).

Dindo, meanwhile, is the opposite of Ruben. He and his evil wife Agatha Rodrigo (Pinky Amador) are very dependent and still live with Leonardo. Aside from not having a child, Dindo has been rumored for having an extramarital affair with his secretary Marinel "Mylene" Medina (Shirley Fuentes).

When Don Leonardo passes away, he leaves all his wealth as well as the firm to Ruben. But unknown to Ruben, his fathers last will marks the beginning of a series of misfortune in their family.

Ruben and Lorrie have been longing to have another child since their only daughter Shayne is already a teenager. However, they will find out that Lorrie can no longer bear a child, leading them to adopt a young boy named Joel Rodrigo (Jacob Rica).

Joels presence provides an overwhelming happiness to Lorrie and Ruben. But it will be the beginning of Shaynes rebellion when she begins to feel neglected.

Shayne will then meet Raul Agoncillo (Dion Ignacio) who will give her the attention she has been looking for from her parents.

Ruben and Lorrie try to mend back their family. Just when they thought its going to be all right again, tragedy struck. As Ruben died Lorrie became mentally unstable and she blames Joel for her husbands death.

Shayne must then make a painful decision. Should she take Joel back to the orphanage? Especially now she has learned to love the child as her brother.

==Cast==
 

===Main cast===
*Lani Mercado as Lorrie Rodrigo
*Joel Torre as Ruben Rodrigo
*Yasmien Kurdi as Shayne Rodrigo   
*Jacob Rica as Joel Rodrigo
*Dion Ignacio as Raul Agoncillo 
*Andrea del Rosario as Ms. Patricia Bernales

===Supporting cast===
*Gary Estrada as Dindo Rodrigo
*Pinky Amador as Agatha Rodrigo
*Charlie Davao as Don Leonardo Rodrigo
*Shirley Fuentes as Marinel "Mylene" Medina Michikawa / Yaya Rose
*Arci Munoz as Bianca
*Luz Valdez as Yaya Sabel
*Vaness del Moral as Donna

===Guest cast===
*Deborah Sun as Melody Valera / Bea Torralba
*Nicole Dulalia as Young Shayne
*Gay Balignasay as Acy
*Lui Manansala as Olive
*Joseph Bitangcol as Mark

===Production crew===
*Directed by: Maryo J. De Los Reyes
*Writers: Anna Aleta Nadela
*Head Writer: Marides Garbes-Severino
*Creative Consultant: Roy Iglesias
*Executive-in-charge of Production: Wilma Galvante

==Awards and nominations==

|-
| 2009 23rd PMPC Star Awards for TV Best Daytime Drama Series || (shared with the cast) ||  

==Movie==
{{Infobox film
| name= Saan Darating Ang Umaga?
| image=
| caption =
| writer= Fanny Garcia
| starring= Nestor De Villa Nida Blanca Maricel Soriano Raymond Lauchengco
| director= Maryo J. de los Reyes
| producer = 
| music= 
| cinematography = 
| studio= VIVA Films
| distributor= VIVA Films
| released=  
| runtime=
| country = Philippines| English 
| movie_series=
| awards=|
}}

===Cast===

* Nida Blanca  as Lorie
* Nestor De Villa as Ruben
* Maricel Soriano as Shayne
* Raymond Lauchengco as Raul
* Jaypee De Guzman as Joel

===Production crew===
*Directed by: Maryo J. de los Reyes
*Writers: Fanny Garcia VIVA Films

==Theme song==
The theme song of the soap opera was also became a theme song of the movie with the same title originally performed by Raymond Lauchengco.

In 2003, "Saan Darating Ang Umaga?" was used as a theme song of the ABS-CBN primetime soap opera Darating ang Umaga. The theme song was performed by Vina Morales.

==See also==
*Darating ang Umaga
*Budoy
* 
*List of programs broadcast by GMA Network
*Sine Novela
*List of shows previously aired by GMA Network

==References==
 

== External links ==
* 

 
 

 
 
 
 
 
 