One Night in the Tropics
 
{{Infobox film
| name = One Night in the Tropics
| image = onenightinthetropics.jpg
| caption = Ad banner
| director = A. Edward Sutherland
| producer = Leonard Spigelgass Charles Grayson Allan Jones Nancy Kelly Bud Abbott Lou Costello William Frawley Peggy Moran
| music = Jerome Kern
| cinematography =
| editing = Milton Carruth
| distributor = Universal Pictures
| released =  
| runtime = 82 min.
| language = English
| budget =
}}
One Night in the Tropics is a 1940 comedy film noteworthy for being the film debut of Abbott and Costello. The team play minor roles but steal the picture with five classic routines, including an abbreviated version of "Whos On First?" Their work earned them a two-picture deal with Universal Pictures|Universal, and their next film, Buck Privates, made them bona fide movie stars.  Songs in the film were by Jerome Kern. The film is based on a 1914 novel, Love Insurance by Earl Derr Biggers, the creator of Charlie Chan.   
 Lois Wilson. and in 1925 by Universal as The Reckless Age.
==Synopsis== underwritten by a nightclub owner, Roscoe (William Frawley), who sends two enforcers - Abbott and Costello - to ensure that the wedding occurs as planned. Everyone involved in the situation winds up sailing or flying to San Marcos (a fictional South American country), where another complication arises, when Lucky falls for Cynthia. Lucky winds up marrying Cynthia, but Roscoe does not have to pay the $1-million because Steve ends up marrying Mickey.

==Cast== Allan Jones as Jim Moore
*Nancy Kelly as Cynthia Merrick
*Bud Abbott as Abbott
*Lou Costello as Costello
*Robert Cummings as Steve Harper
*Mary Boland as Aunt Kitty Marblehead
*William Frawley as Roscoe
*Peggy Moran as Mickey Fitzgerald
*Leo Carrillo as Escobar
*Don Alvarado as Rodolfo
*Nina Orla as Nina
*Richard Carle as Mr. Moore

==Production==
One Night in the Tropics was filmed from August 26 through September 30, 1940 under the films working title, Riviera.  Riviera was an unproduced Jerome Kern musical from 1937 originally planned for Danielle Darrieux;   
Kerns songs were reused in the film. 

==Promotion==
Just prior to the beginning of production, on August 21, 1940, Jones and Cummings were guests on Abbott and Costellos radio show and promoted the film.

==World premiere==
The film had its world premiere in Costellos home town of Paterson, New Jersey|Paterson, New Jersey on October 30, 1940. 

==Rerelease==
The film was re-released (at 69 minutes) by Realart Pictures in 1950 with The Naughty Nineties and in 1954 with Little Giant. 

==DVD release==
This film has been released twice on DVD.  The first time, on The Best of Abbott and Costello Volume One on February 10, 2004, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 