Limit Up (film)
{{Infobox film
| name         = Limit Up
| image        = Limitup_NancyAllen.jpg
| caption      = theatrical poster
| runtime      = 100 minutes
| genre        = Comedy
| writer       = Richard Martini Luana Anders
| director     = Richard Martini
| producer     = Jonathan D. Krane Nancy Allen Ray Charles Brad Hall Danitra Vance Dean Stockwell Rance Howard Sandra Bogan
| distributor  = MCEG/Virgin
| editing      = Sonny Baskin
| music        = John Tesh
| country      = United States
| language     = English
| released     = November 3, 1989
}}
 Nancy Allen as Chicago commodities trader Casey Falls.  The film was directed by Richard Martini and produced by Jonathan D. Krane. The movie was filmed throughout Chicago and prominently features scenes at The Board of Trade and Wrigley Field.

Rance Howard, Dean Stockwell, blues icon Ray Charles and Saturday Night Live alumni Danitra Vance and Brad Hall round out the cast. Additionally, actress Sally Kellerman makes a cameo appearance as a nightclub singer. John Tesh scored the film.

==Cast== Nancy Allen  as Casey Falls
* Dean Stockwell   as Peter Oak
* Brad Hall   as Marty Callahan
* Danitra Vance   as Nike
* Ray Charles   as Julius
* Rance Howard   as Chuck Feeney
* Sandra Bogan   as Andy Lincoln
*William J. Wolf   as Rusty
*Ava Fabian   as Sasha
*Robbie Martini   as Assistant to Mr. Oak
*Teressa Ovetta Burrell   as Clerk
* Winifred Freedman   as Pit Recorder
*Kellie Joy Beals   as Reporter
*Nicky Blair   as Maitre D

==Critical Response==
The lighthearted romp received a B+ from   noted that director Martini succeeds "in making the commodity-market scenes comprehensible for the layman."   The St. Paul Pioneer Press said Limit Up "cant be faulted for enthusiasm or moxie"  and its "strong equality-of-women-in-the-workplace message is welcome".  Dually, The Chicago Tribune was less complimentary: "the picture is as spunky and good-natured as all get out, but in the absence of any original or substantial ideas, its spunk soon grates."  

==External links==
* 
*  at Facebook

==References==
 

 

 
 
 
 
 
 
 
 
 
 