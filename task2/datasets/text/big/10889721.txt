Ekaveera
{{infobox book |  
| name          = Ekaveera
| title_orig    =
| translator    =
| image         =
| caption =
| author        = Viswanatha Satyanarayana
| illustrator   =
| cover_artist  =
| country       = India Telugu
| series        =
| genre         = Family
| publisher     = Bharathi magazine
| release_date  = 1935
| media_type    = Print
| pages         = 116
| isbn          =
| preceded_by   =
| followed_by   =
}}
Ekaveera ( n novel written by Kavi Samrat Viswanatha Satyanarayana in Telugu language. It was penned between 1929–31 and was published in Bharathi magazine in 1935.  Originally written in Telugu language|Telugu, it has since been translated into various Indian languages.This is the second novel of Viswanadha and it is the only novel written by the poet with his own hand writing. In 1969, a film version was released.

==Telugu film==
{{Infobox film
| name           = Ekaveera
| image          = Ekaveera.jpg
| image_size     =
| caption        =
| director       = C. S. Rao
| producer       = Dronavadjula Lakshmi Narayana  B. A. Sitaram
| writer         = Viswanatha Satyanarayana (story) C. Narayana Reddy (dialogues)
| narrator       = Kantha Rao Jamuna
| music          = K. V. Mahadevan
| cinematography = J. Satyanarayana
| editing        = P. V. Narayana
| studio         =
| distributor    =
| released       = 1969
| runtime        =
| country        = India Telugu
| budget         =
}}
 musical film was produced based on the novel in 1969.

==Plot==

The plot is set up in the late 16th Century revolves around two friends - Puttan Sethupathi and Veerabhupathi. Sethupathi hailing from a royal family falls in love with Meenakshi, an ordinary girl, while Veerabhupathi loves Ekaveera, a young lady from the royal clan in Amba Samudram. However, due to the societal restrictions the dreams of both the couples meet a dead end.

As the destiny would have it, Sethupathis marriage is fixed with Ekaveera while Veerabhupathi gets married to Meenakshi. Both the marriages remain unconsummated due to the lack of feelings between the couples. Overcoming the shock of discovering that his formwer lover has married his best friend, Sethupathi accepts the path of dharma and renounces his feelings for her. He reveals to Veerabhupathi the relationship between him and Meenakshi. Veerabhupathi also accepts the fact and appreciates his friends honesty. Sethupathi then proceeds to express his love towards Ekaveera and embrace her as his wife

But, things go wrong as Sethupathi is made in charge of a strategic fort and asked not to leave the place in anticipation of foreign invasion. Communication sent to Ekaveera by Sethupathi about his changed attitude does not  reach her as the Messenger sent by Sethupathi gets killed.

Meanwhile at Madhurai, in a weak moment, on seeing Ekaveera,  Veerabhupathi advances towards her. As her chastity is consumed by an outsider, Ekaveera proceeds to take her life. Veerabhupathi and Meenakshi also end their lives due to the turn of events. Sethupathi, the follower of dharma is left alone in the world.

===Cast===
{| class="wikitable"
|-
! Actor / Actress !! Character
|-
| Nandamuri Taraka Rama Rao || Sethupati
|-
| Tadepalli Lakshmi Kanta Rao|T. L. Kanta Rao || Veerabhupati
|-
| K.R. Vijaya || Ekaveera
|- Jamuna || Meenakshi
|-
| Mukkamala Krishna Murthy || Maharajah of Puttana kingdom
|-
| Kaikala Satyanarayana || Prince Tirumala Nayudu
|-
| Dhulipala || Father of Veerbhupati
|-
| Nirmalamma || Mother of Ekaveera
|-
| Santha Kumari ||
|-
| Sriranjani ||
|- Rajababu || Bhattu
|-
| Vangara Venkata Subbaiah ||
|}

===Crew===
*Producers: D. L. Narayana and B. A. Sitaram
*Production company: Padma Films
*Director: C. S. Rao
*Story: Viswanatha Satyanarayana (novel)
*Screenplay: V. Chengayya
*Dialogues: Dr. C. Narayana Reddy - His first film as dialogue writer
*Art Directors: Godagonkar and Vaali
*Editing: P. V. Narayana
*Director of Photography: J. Satyanarayana
*Lyrics: C. Narayana Reddy and Devulapalli Krishna Sastry
*Original Music : K. V. Mahadevan and Puhalendi
*Playback singers : Ghantasala Venkateswara Rao, S.P. Balasubramaniam, P. Susheela, Madhavapeddi Satyam and B. Vasantha
*Choreography: Vempati Satyam

===Songs===
There are about 12 songs and poems in the film. 
*Kanipettagalava Maguva (Lyrics: Devulapalli Krishnasastri; Singer: P. Susheela group)
*Kanudammulanu Moosi Kalaganti Okanadu (poem) - (Lyrics:  )
*Krishna Nee Peru Talachina Chalu - (Lyrics:  ).
*Letha Vayasu Kulikindoy (Lyrics:  , S.P. Balasubrahmanyam and others)
*Oka Deepam Veligindi Oka Roopam Velicindi - (Lyrics:   and P. Susheela).
*Oune Cheliya Sari Sari (Lyrics: Devulapalli Krishnasastri; Singer: P. Susheela)
*Prati Raatri Vasantha Raatri (Lyrics:   and S.P. Balasubrahmanyam)
*Thotaloo Naaraju Thongi Choosenu Naadu - (Lyrics:   and Ghantasala (singer)|Ghantasala).
*Vandanamu Janani Bhavani (poem) - (Lyrics:  )
*Ye Parijatammu Leeyagalano Sakhi - (Lyrics:  )
*Yeduru Choochina Valapu Thotalu (poem) - (Lyrics:  )
*Yenta Dooramo Adi - (Lyrics: C. Narayana Reddy; Singers: S.P. Balasubrahmanyam and P. Susheela)

==References==
 

==External links==
*  on IMDb
* 

 
 
 
 
 
 