My Brother's Wedding
{{Infobox film
| name           = My Brothers Wedding Charles Burnett Charles Burnett Gaye Shannon-Burnett Charles Burnett
| starring       = Everett Silas Jessie Holmes Gaye Shannon-Burnett
| editing        = Charles Burnett
| studio         = Milestone Films
| distributor    = Milestone Films  Les Films du Paradoxe (France)
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = $50,000 (estimated)
| gross          = $8,217 (USA)
}} Charles Burnett about a man named Pierce Mundy (Everett Silas) who has low ambitions and no plans for the future. He settles for a job at his parents dry cleaners in South Central Los Angeles after being fired from his previous job. Pierce wastes his days fooling around and wasting time with his childhood friend, Soldier (Ronnie Bell), who was recently released from prison. When his brother Wendell (Monte Easter) plans on marrying a woman, Sonia (Gaye Shannon-Burnett), with a higher social class than he, his disdain for the woman prevents him from making the morally right decision.

== Plot ==
The movie opens with a man playing the harmonica and singing the blues. Pierce is then seen walking down the street when he gets called by a woman to see her sisters baby. Pierce says that he doesnt have time because he has to go to visit Soldiers mother, but goes in anyway. At the house, Pierce asks who the father is and the woman says that he could be the father if he wants. Angered by the comment, Pierce leaves the house and continues his way to Soldiers place.

At the house, Soldiers mother Mrs. Richardson asks Pierce if Soldier would ever act his age and wants Pierce to keep him out of trouble. Pierce promises her by saying that Soldier wrote him a letter that said he would never go back to jail and even asked for a job. Mrs. Richardson asks about his brothers wedding and Pierce tells her about how he doesnt like her because shes rich. Pierce leaves saying that he has to go work at his mothers shop.

At the shop, Mr. Bitterfield comes in asking if Pierces mother, Mrs. Mundy, can mend the rip in his church going pants. When Pierce goes in the back to consult his mother, she says to tell Mr. Bitterfield that they can fix it but to instead throw them in the trash and give Mr. Bitterfield a pair from the unclaimed box. Pierce then gets into a wrestling match with his father. Meanwhile a man comes into the store asking for a job. Mrs. Mundy replies saying there are two grown man in the back that can help her. Turning around to see Pierce and his father still wrestling, the man leaves as does Pierces parents.

Back at home, Pierces mother informs him that his brother, Wendell, and his fiancée, Sonia, are coming over. Pierce says he does not want to go, complaining that Sonia is always bragging about how her family is rich and privileged. She warns Pierce that he has to behave so Sonia can see that he is civilized. She then tells him to go over to the neighborhood elders, Big Momma and Big Daddy, to see if they need anything and to pick up a pot. Once there, Big Daddy gets mad at Pierce for being rude and not saying anything. Pierce explains that he has to get back to the house before Wendell gets there.

Pierce and Sonia get into an argument because Pierce thinks that Sonia had nothing to worry about all her life because she attended charm school. Sonia retaliates saying that she had to worry about things such as grades and if people liked her and her. Sonia also says that charm school taught young girls how to be ladies. They walk into the living room, where Wendell was telling his secretary of the wedding count. Upon hearing that, Pierces mother then turns and asks Pierce when hes going to have a secretary. She says that she put them in church so that when they settled down she would have done her job. Intending that Pierce has not yet accomplished anything, Pierce tells his mother that it is not his fault. This upsets his mother as she tells him that it is his fault because of all the sacrifices that she and their father have given up for them.

The next day, a man comes in looking for the clothes that he had brought in two months ago. They have trouble finding it because they man does not remember what he put his name as. They allow the man to go back and look for it, but Mrs. Mundy states that if he was a good man they would not have to keep track of all his aliases. Angela comes into the store again all dressed up asking Pierce if he would go to prom with her in a couple of year. Seeing Pierces disinterest, she leaves saying that shes going over to Smokey Robinsons tonight and that she should go get dressed. Pierces mother tells him that he needs to go over to Big Momma and Daddys house because Haddie wants to go out. As Pierce leaves, he and his father get into another wrestling match.

With Soldier close to returning home, Pierce goes to a liquor store and asks his friend if he would be willing to give Soldier a job. The friend says that he would give Pierce a job, but he will not give one to Soldier. Having been rejected, Pierce then goes to ask his friend, Bob, telling him that Soldier is getting out of jail. Bob states that it is too bad because a person like Soldier should just stay in jail until he rots. Hearing this, Pierce leaves without even asking and picks up Soldier to take him home. When Mrs. Richardson sees Soldier she starts crying and Pierce consoles her saying that he is here to stay.

Pierce hangs out with Soldier saying that Soldier is with a different girl every time he seems him. They goof around and hang out for the day. While hanging out in an alley, Soldier asks about his friend Lonneil and Pierce tell him that he was killed during an attempt to rob a liquor store. Pierce then says that they are the only two left. The next day Angela comes back and tells Pierce about how she was at Smoke Robinsons last night. She also tells him about how she does not like guys that are too cute. Pierces mom interrupts telling Pierce that he has to go take care of Big Daddy and give him a bath. A man then attempts to rob the shop but backs out when he realized that Mrs. Mundy was on to what he was trying to do. Soldier asks Pierce about his girlfriend, Barbara, and if they had sex or not. Someone hiding in the bushes then leaps out and tries to shoot them but there are no bullets in the gun. They end up chasing the man, but Pierce lets him go.

Pierces mother begs him not to embarrass the family during dinner with the Richardsons. She says that she didnt raise heathens, but Pierce still states that he does not like Sonia because she has never worked for anything in her life. During dinner, Mr. Richardson asks Pierce what his job is. Pierce says that he went to school but did not like that everyone was doing the same thing. He tells him how he used to drive heavy machinery and delivered explosives. He states that he likes to work with his hands and that hes not smart enough to become a lawyer. Pierces mother then says that it would have been nice to have a doctor and a lawyer in the family, but Pierce remarks that they are all crooks. Mr. Richardson says that the real corruption is in politics. Pierce states that the higher up you go, the lower the people you find. When Pierces mother asks Sonia about her trial, Pierce yells at her saying that letting her client go free was wrong because he had killed people. This ends the dinner and they leave with Pierces mother saying how ashamed of him she is.

While Pierce is working at the store, Soldier walks in with a girl and asks Pierce to let him have sex with the girl in the back of the store. Pierces mother goes to church but has to come back because she forgot her prayer book. She ends up walking in on them having sex and freaks out because they also did it on her prayer book. The girl then runs away in embarrassment. The next day, Soldier is waiting in a car for Pierce with another girl and sends Angela to go tell Pierce to hurry up. Angela ends up not telling Pierce that they are waiting and they leave without him.

While Pierce is taking care of Big Momma and Big Daddy, Big Daddy asks Pierce if his friends are saved and believe in God, especially Soldier. That night Soldier gets into a car accident and dies. When Pierce finds out the next day, he runs to Soldiers house where his parents are grieving and the funeral is set for Saturday. Soldiers mom tells Pierce that he was like a son to her. Pierce then feels like it is his job to go find pallbearers for Soldiers funeral. In doing so, he realizes that the funeral is the same day as his brothers wedding. He rushes home to ask his brother and Sonia to change they date, but they refuse because he had never been nice to Sonia. Pierces father then talks to him and tells them that maybe they can change the date of the funeral. With new hope, Pierce goes back to Soldiers house to ask. Once there, Soldiers dad tells him about the stress of the last few days and all the relatives that have flown in for Soldiers funeral. Hearing this, Pierce abandons the idea of asking them to change the date of the funeral and is now left conflicted. As both the wedding and the funeral are starting, Pierce shows up late and tells his mother that is has to go to the funeral. He tells his mother that someone else has to be the best man, but she gets angry and tells him to go sit down. However, Pierce ends up getting a car and drives to Soldiers funeral only to be late and miss it. The movie ends with Pierce sitting at the parking lot of the mortuary with the wedding rings and missing both events.

== Characters ==
Pierce Mundy (Everett Silas) - main character who is a man with no ambitions and spends his days working at his parents dry cleaners in South Central Los Angeles. When he is not working, he is either taking care of the neighborhood elders or wasting time with his childhood friend, Soldier. When the time comes for Pierce to make a crucial decision in his life, he is not morally able to choose the right decision and ends up disappointing his family.

Wendell Mundy (Monte Easter) - Pierces brother who is also a lawyer moving up in the social class order. He is about to marry an upper-middle class woman who Pierce despises because she is wealthier than their family.

Mrs. Mundy (Jessie Holms) - Pierces mother who dominates the household and manages the dry cleaners store. She is always nagging Pierce to get an education and become successful like his brother. She is very proud of her son, Wendell, and is constantly trying to get Pierce to be just like him.

Mr. Mundy (Dennis Kemper) - Pierces father who is very passive and has no major responsibilities in his life. Similar to Pierce, he acts like a child wrestling and fighting with Pierce. He has no influence over his family and cannot even help Pierce make the right decision in life.

Soldier Richardson (Ronnie Bell) - Pierces childhood friend who is constantly in and out of prison. He does not have a job and spends his days fooling around and sleeping with multiple women. He influences Pierce to make the wrong decisions in life and gets them in trouble.

Mrs. Richardson (Sally Easter) - Soldiers mother who looks to Pierce to try to help Soldier become a better person. She sees Pierce as her own son and hopes he can lead Soldier to make the right decisions.
 
Sonia Dubois (Gaye Shannon-Burnett) - Wendells fiancé who Pierce hates because he thinks that she never had to work for anything in her entire life. She and Pierce constantly get into argument about the subject and refuses to change her wedding date because Pierce had never been nice to her.

Mr. and Mrs. Dubois (Sy Richardson and Frances E, Nealy )- Sonias parents who are very wealthy and despised by Pierce.

Angela (Angela Burnett) - neighborhood girl that has a crush on Pierce and is always trying to talk to him.

== History == South Central Los Angeles. In a place where crime and violence are such a big part of everyday life and most people are stuck in their life, Burnett shows the life of an average person through the life of Pierce. {{cite web
  | last = Axmaker
  | first = Sean
  | title = My Brothers Wedding
  | url= http://www.tcm.com/thismonth/article.jsp?cid=184977&mainArticleId=184971
  | accessdate = 2011-02-14 }}   
He spends his days either wasting time with his friends or working a dead end job at his parents dry cleaners with no thoughts of ever leaving. Burnett also depicts a typical household of dominant mothers and passive father as seen in Pierces parents. 
His mother runs the dry cleaning shop while his father, like Pierce, goofs around with him and does whatever his wife tells him to do. Having dropped out of school, Pierce hates the rich and is satisfied with his life for the wrong reasons. He does not realize what it is like to live a well educated and successful life. He sees everyone in the upper and middle class not having worked for anything in their lives. As shown in the conversation with Sonias father, Pierce tells him that he likes doing a working class mans job which includes operating heavy machinery and working with his hands. He also mentions how he believes everyone who goes to college lives the same boring life and that all doctors and lawyers are corrupt. This statement shows how ignorant Pierce is to the good things and the bad things in life reflecting the opinion of most people living in the ghetto. Pierce also spends most of his time taking care of the neighborhood elders instead of taking care of him. This results in the consequence of Pierce not learning to be responsible for his actions and not being able to make the right decisions in life. Burnetts tragicomedy shows the life of African-American living in the ghetto as funny and loving, but in a tragic way because they lack the ambition to get out of the ghetto and lead successful lives. {{cite news
  | last = Scott
  | first = A.O.
  | title = A Rough Poetry of Real Independence
  | date = 2007-09-14
  | url=http://www.nytimes.com/2007/09/14/movies/14brot.html?_r=1
  | accessdate = 2011-02-14
  | work=The New York Times}} 
As a statement in the New York Times states "  making art out of materials and inspirations that lie close to hand. And the result is a film that is so firmly and organically rooted in a specific time and place that it seems to contain worlds.” 

== Black Independent Film ==
My Brothers Wedding is a black independent film rather than a blaxploitation film. Black independent films focus on the many aspects of African American life rather than just the negative side.  However, because they are seen as independent, many of these films never received the publicity they deserved. As was the case with My Brothers Wedding which was not publicly released due to mixed reviews from the original audience. Even though Burnett tried to make these films as personal and as meaningful as possible, many of his works are hard to find on a regular basis. The lack of popularized black films can be acquitted to the fact that Hollywood has been designed to crush the independent film industry.    Another flaw in the black independent film industry is the fact that independent film industries are rarely located in African American neighbors.  Many are instead located in white neighborhoods that usually prefer to watch Hollywoods productions. 

== Production ==
Charles Burnett was never allowed to finish editing the movie before it was sent to the New York film festival for screening by the producers. Films festivals are successful ways to introduce films to the public in hopes of being picked up by a distribution company. {{cite book|last=Litwak|first=Mark|title=Risky Business: Financing & Distributing Independent 
Films|publisher=Silman-James Press|location=Los Angeles|year=2004|pages=133–134|isbn=0-615-29650-5}}  The rough-cut version of the film initially received mixed reviews from the audience and the people of the New York Times. {{cite web
  | title = Milestone Films-My Brothers Wedding
  | url= http://www.milestonefilms.com/movie.php/brother/
  | accessdate = 2011-02-14 }} 
This scared off distributors who  believed it would be unsuccessful and was therefore never released to the wider public audience, who may have had different opinions on the film. Armond White, a film critic, noted that this was “a catastrophic blow to the development of American popular culture.”   Almost twenty five years later when Milestone Films picked up the rights to this film, they allowed Burnett to finally finish editing the film and release it the way he always pictured it to be. 

== Release ==

=== Box office ===

My Brothers Wedding generated $4,294 in revenue during its first weekend on September 16, 2007. The film was screened in only one theater in the U.S. The film went on to earn gross revenue of $8,217. {{cite web
  | title = IMDb- My Brothers Wedding
  | url= http://www.imdb.com/title/tt0087763/
  | accessdate = 2011-02-14 }} 

=== Critical reception ===

The film mostly generated positive reviews from the public. Slant Magazine called this film timeless and necessary even though it did not hold up well as a film. {{cite web
  | last  = Schrodt
  | first = Paul
  | title = My Brothers Wedding
  | url   = http://www.slantmagazine.com/film/review/my-brothers-wedding/3111
  | accessdate = 2011-02-14 }}    
The Village Voice stated it was "A treasure that demands to be unearthed in all its funny-sad tenderness."  Jonathan Rosenbaum of the Chicago Reader said "If a better film has been made about black ghetto life, I havent seen it."  The films did fairly well considering the fact that it was an independent film shown in only one theater across the nation.

== References ==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 