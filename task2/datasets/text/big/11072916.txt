Master of the House
 
{{Infobox film
| name           = Du skal ære din hustru
| image          = Duskalæredinhustru1925.jpg
| image_size     = 190px
| caption        = Poster
| director       = Carl Theodor Dreyer
| producer       = 
| writer         = Carl Theodor Dreyer Sven Rindhom
| narrator       = 
| starring       = Johannes Meyer Astrid Holm
| music          = 
| cinematography = George Schnéevoigt
| editing        = Carl Theodor Dreyer   
| distributor    = Palladium Film 
| released       = 5 October 1925 
| runtime        = 107 minutes
| country        = Denmark Silent
| budget         = 
| gross          = 
}} silent drama film directed and written by acclaimed filmmaker Carl Theodor Dreyer. The film marked the debut of Karin Nellemose. The silent is regarded as a classic by many in Danish cinema and has been released on DVD and Blu-ray.

==Plot==
Viktor Frandsen, embittered by losing his business, is a tyrant at home, constantly criticizes his patient, hard-working wife Ida and their three children. He does not appreciate the effort it takes to maintain a household. While his wife is resigned and browbeaten, his old nanny, nicknamed "Mads" by all, openly defends her.  When Idas mother, Mrs. Kryger, pays a visit, he is very rude to her. Finally, he issues an ultimatum: either Mrs. Kryger and the openly hostile Mads (who regularly helps the family) are gone by the time he returns, or the marriage is over.

Mads orchestrates a plan that will force him to rethink his notions of being the head of a household. With the help of Mrs. Kryger, she persuades a very reluctant Ida to go away for a while and rest, while she sees to Viktor and the children. Then, she institutes a new regime, ordering Viktor to take over many of Idas duties; Viktor obeys, cowed by his memories of the strict discipline she imposed on him when he was a child in her care. Meanwhile, Ida, no longer distracted by her many duties, feels the full misery of her situation and has a breakdown.

As time goes by, Viktor comes to fully appreciate his wife and, as he does love her dearly, longs for her return. When Ida, fully recovered, is finally allowed to come back, Viktor and the children are ecstatic. Idas mother then shows him a newspaper advertisement offering an optometrists shop for sale and gives him a check to buy it.

==Reception==
The film has been evaluated as a "spare, compassionate, and astute social satire". 

==Cast==
*Johannes Meyer as Viktor Frandsen
*Astrid Holm as Ida Frandsen
*Karin Nellemose as Karen Frandsen, their oldest child
*Mathilde Nielsen as "Mads"
*Clara Schønfeld as Alvilda Kryger
*Johannes Nielsen as Doctor
*Petrine Sonne as Laundress
*Aage Hoffman as Dreng, son
*Byril Harvig as Barnet, son
*Viggo Lindstrøm
*Aage Schmidt
*Vilhelm Petersen

==References==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 