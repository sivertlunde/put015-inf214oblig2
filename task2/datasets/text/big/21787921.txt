Asal (film)
 
 
{{Infobox film
| name           = Asal
| image          = Asal.JPG
| alt            = Early publicity poster
| caption        =  Saran
| producer       = Prabhu Ganesan
| writer         = Yugi Sethu Saran
| narrator       = Ajith Kumar Prabhu Sameera Bhavana Suresh Sampath Raj Rajiv Krishna
| music          = Bharathwaj
| cinematography = Prashanth D. Misale Anthony Gonsalves
| studio         = Sivaji Productions
| distributor    = Sivaji Productions Ayngaran International
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Tamil
| budget         =    . Timesofindia.indiatimes.com (31 August 2011). Retrieved on 21 February 2014. 
| gross          = 
Collection         =   
| gross          = 
}} thriller film Bhavana play Anthony Gonsalves.

The film revolves around the feud between the three brothers over property; two brothers, from their fathers first wife on one side with their avarice for all the wealth with the righteous third, from his fathers second marriage, trying to stop the family from breaking down. The feud that exists as an undercurrent in the presence of their father and turns ugly and personal after he passes away. It grows bigger with the two brothers joining in to elbow out the third but he graciously steps aside, only wanting to keep cordial relations. But, the two brothers are just not able to handle the huge wealth and the responsibility that it brings. Their wealth attracts trouble and it is up to the third to come back and save his brothers, against others who strive for the wealth while the crux of the plot revolves around if wealth does disintegrate the family.

 , and abroad in Paris, Dubai and Kuala Lumpur. The film opened worldwide on 550 screens (including 350 screens in India)  following its release on 5 February 2010. The film received generally negative reviews.

== Plot ==
Jeevanandham ( ) from his second wife. Jeevanandham’s favourite is Shiva, who is gutsy and righteous, while the other two are immature and controlled by their uncle, the evil Kali Mamma (Pradeep Rawat), and will do any shady deals.

Sarah (Sameera Reddy) is a cultural attaché at the Indian Embassy in Paris who has a soft spot for Shiva. There is a French police officer, Daniel (Suresh), who constantly hangs around with the family and is a partner in crime.

The bad sons want to deal in drugs and supply arms to terrorists; they work out a strategy to eliminate Shetty (Keli Dorji), who controls the Mumbai underworld, but the old man and Shiva oppose it. After the old mans death, Vicky is kidnapped by Shetty and his gang, who brutally torture him.

To save Vicky, Shiva goes to Mumbai. His local contact there is Mirasi (Prabhu), his father’s best friend. A local girl, Sulaba (Bhavana), falls for our hero, who daringly rescues Vicky with the joker Don Samosa (Yugi Sethu).

Both brothers double crosses Shiva and they shoot Shiva drown him in sea. They torture Sarah and make her to sign as witness that Shiva dies naturally. As per Jeevanandams will the property rights belongs to Shiva . Hence Sam and Vicky attempted to kill Shiva. With the help of Mirasi (Prabhu Ganesan), Sulabha (Bhavana) and Don Samosa all move to France to find the real enemies. Truth is found and revenge taken.

== Cast ==
* Ajith Kumar as Shiva Jeevanandham and Jeevanandham Prabhu as Mirasi
* Sameera Reddy as Sara Bhavana as Sulaba Pillai Suresh as Daniel Dharmaraj
* Sampath Raj as Sam Jeevanandham
* Rajiv Krishna as Vicky Jeevanandham Pradeep Rawat as Kalivardhan
* Yugi Sethu as Don Samsa
* Kelly Dorji as Brijesh Shetty
* Karmanya Sapru as Lee
* Yog Japee
* Surendra Pal
* Balaji K. Mohan

==Production==

===Development=== Vishnuvardhan and Saran leading the race to take over.   
 Saran being signed up as the films director. Ajith Kumar also underwent an appearance change during the early months of 2009 to prepare for his role. The film was eventually launched on 8 April 2009  at Sivaji Ganesan|Sivajis family residence Annai Illam in Chennai.    The film completed its production schedule by January 2010, and post-production and release works began soon after.

===Casting===
Following the announcement that Sivaji Productions, Ajith Kumar and Gautham Menon would come together, other technicians were added to the film. Menons usual collaborator, Harris Jayaraj, was signed on as the music composer. However, after Menons departure, Jayaraj also left the project due to his busy schedule with other films.    After approaching Yuvan Shankar Raja for the job, Saran finally confirmed Bharathwaj as music director.    Asal was Bharathwajs fiftieth film as composer; he had previously teamed up with Saran and Ajith on Kadhal Mannan, Amarkalam and Attagasam. Ajith was under contract to be paid a remuneration of $1.25 million in cash plus a 30 per cent share of the profit from sales of rights of the film. 
 Anthony was Sneha and Bhavana were Sneha was Bhavana was Pradeep Rawat, Keli Dorji, Karen Miao Sapru, Adithya, Suresh,    Sampath Raj,    Surendra Pal    and Yugi Sethu.

=== Filming === pooja ceremony.    Ajith, Saran, Arjun (Prabhus son), Dushyanth (Ramkumars son) and other members of the film were present there.    On 24 November 2009, an important scene for the film was shot at Sivaji Ganesans house in T. Nagar, Chennai. A song sequence featuring Ajith and Bhavana was shot at the AVM Studios on 25 November 2009.    The shooting of the final song was held at Binny Mills (Tambaram). The cast and crew of Asal went to a Middle East country for the remaining song shoot. The Asal unit left Chennai on 26 December and returned after a week.    Shooting was finished on 31 December 2009 at Dubai, where a song sequence was filmed at Zabeel Park.

== Release == Sun TV. The film was given a "U/A" certificate by the Indian Censor Board.

== Reception ==
The movie earned more than was spent on it during the first two weeks of its theatrical run.  The film was declared an average grosser at the box office.

Upon release, the film generally received negative reviews, with critics citing that the film is strictly for Ajith Kumar fans and that it couldnt live up to its expectations, whilst also criticising its Billa (2007 film)|Billa hangover. Sify cited that the film, which "should have been called Billa-2", "falls flat due to lack of proper story and narration", adding, that the film "belongs to Ajith" and "it is a good ride if you keep your expectation meter low".  A reviewer from Behindwoods gave the film 2 out of 5, claiming that this film "will gain no great interest". He adds that "Asal is a complete Ajith centric entertainer with lots of style and sophistication", that "the script is weak and there are other flaws too" and that "Ajith satisfies his fans, but Saran disappoints a bit".  Pavithra Srinivasan from Rediff gave the film 2 out of 5 as well, citing that the film is for "die-hard Ajith fans", who would have "plenty of reasons to rejoice", whilst the others should "leave their brains at home". The reviewer, like Sify, also adds that the film has a "Billa hangover". 

== Soundtrack ==
{{Infobox album  
| Name        = Aasal
| Type        = soundtrack
| Artist      = Bharathwaj
| Label       = Ayngaran Music An Ak Audio
| Cover       = Asal_Audio.JPG
| Alt         = Album cover
| Released    =  
| Recorded    = Jay Jay Studios Chennai   
| Genre       = Film soundtrack
| Length      = 
| Producer    = Bharathwaj
}}
{{Album ratings
| noprose = yes
| rev1 = Behindwoods
| rev1Score =   
| rev2 = Top 10 Cinema
| rev2Score =   
}}
 
The films soundtrack was released on 4 January 2010.  The soundtrack has music by Bharathwaj & Y-Kinz with lyrics by Vairamuthu. The audio distribution rights were given to ANAK Audio.  Think Music, an association of Sathyam Cinemas and Hungama Technology.  The audio was launched at Sivaji Ganesans house in T Nagar, Chennai by Prabhu, one of the producers, and was received by Ajith.  

{{Track listing
| extra_column = Singer(s)
| title1 = Aasal | extra1 = Sunitha Menon | length1 = 04:50
| title2 = Kuthiraikku Theriyum | extra2 = Surmukhi, Sreecharan | length2 = 04:48
| title3 = Tottodaing | extra3 = Mukesh, Janani | length3 = 04:01
| title4 = Yengay Yengay | extra4 = S. P. Balasubrahmanyam | length4 = 04:04
| title5 = Yea Dushyantha | extra5 = Kumaran, Surmukhi | length5 = 05:05
| title6 = Em Thandhai | extra6 = Bharathwaj | length6 = 03:34
| title7 = Yengay Yengay | extra7 = Karthikeyan & Chorus | length7 = 02:57
}}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 