Fast Getaway II
{{Infobox Film |
  name     =Fast Getaway II |
  image          =Fast_Getaway_II_2_Corey_Haim.jpg |
    writer         =Mark Sevi |
  starring       =Corey Haim Cynthia Rothrock Leo Rossi |
  director       =Oley Sassone |
  producer       =Paul Hertzberg Catalaine Knell Russell D. Markowitz |
  distributor    =Cinetel Films Inc. Live Entertainment |
  released   =December 21, 1994 |
  runtime        =95 minutes | David Robbins |
  awards         = |
 language =English |
  budget         = |
}} action comedy/Adventure adventure film, starring Corey Haim, Cynthia Rothrock and Leo Rossi. The film is a sequel to the popular Fast Getaway, released in 1991.

==Plot summary==
Nelson (Haim) having since turned his life around from the days of robbing banks, now runs an insurance business with his girlfriend. His past comes back to haunt him when an ex-partner from the old days frames him for burglary.

When his father, whom Nelson used to rob banks with, gets out of prison, the pair go after the individual who set up Nelson.

==Production notes==
The film was shot in Tucson, Arizona and features a 1994 Mazda RX7 as his getaway car.

==Release history==
Fast Getaway II was released on VHS by Live Entertainment and in Canada by C/FP Video in 1994.

==References==
 
*   IMDB.com. Retrieved December 28, 2005.
*   IMDB.com. Retrieved December 28, 2005.
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 