The Singles Ward
{{Infobox film
| name = The Singles Ward
| image = SinglesWard.jpg
| director = Kurt Hale Dave Hunter John E. Moyer Will Swenson Michael Birkeland Zak Aldridge Lincoln Hoppe Tarance Edwards Michelle Ainge Gretchen Whalley Sedra Santos
| cinematography =
| editing =
| distributor = Halestorm Entertainment
| released =  
| runtime = 102 minutes
| country = United States
| language = English
| music = Cody Hale
| budget = $500,000
| gross  = $1,250,798
}} John E. Moyer based on his own life as a stand up comedian and single member of The Church of Jesus Christ of Latter-day Saints (LDS Church).  Like The R.M., and other films which followed it, The Singles Wards target audience is members of the LDS Church and citizens of Utah.  As such, references to and parodies of the Mormon and Utah subcultures pervade the film and are unlikely to be completely understood by non-Mormons.

The Singles Ward was followed in 2007 by The Singles 2nd Ward.

==Plot==
 stand up routine lampooning the Mormon way of life.  His resistance to the church continues until he falls for Cammie Giles, a member of the local singles Ward (LDS Church)|ward. Suddenly, Jordan finds going to church more appealing.  But is he attending church again just to impress her?

During the course of the movie, Jonathan frequently breaks the fourth wall to narrate events to the audience, much in the style of Ferris Buellers Day Off.

==Selected credits==

===Cast=== Will Swenson, Jonathan Jordan
*Connie Young, Cammie Giles
*Kirby Heyborne, Dalen Martin
*Daryn Tufts, Eldon Coates Michael Birkeland, Hyrum
*Lincoln Hoppe, DeVerl

===Crew=== director
*Kurt John E. writing

===Cameos===
A number of celebrity members of the LDS Church make appearances throughout the movie:
 California Angels, as Brother Angel. NBA player and current Boston Celtics General Manager, plays a Sunbeam Teacher.
*Shawn Bradley, another former NBA player, as an auto mechanic. Steve Young as Brother Niner. The character name is a reference to Youngs career as a quarterback for the San Francisco 49ers. 
*Thurl Bailey, another former NBA player.
*Late actor Gordon Jump.
*Writer, producer, director and actor Richard Dutcher as the neighbor Wes, who knocks on the door while they are watching Dutchers movie Gods Army (film)|Gods Army and declines an invitation to watch it because he was offended by the "toilet scenes".

== Soundtrack == Hymns of The Church of Jesus Christ of Latter-day Saints and the Childrens Songbook. The soundtrack is on Guapo Records.

*"The Church of Jesus Christ" Magstatic Slender
*"There is Sunshine in My Soul Today" Ponchillo
*"Do What is Right" MishMash Rooster
*"Book Pipe Dream
*"In Our Lovely Deseret" Mr. Fusion (band)|Mr. Fusion
*"Keep the Commandments" Mighty Mahogany
*"I Feel My Saviors Love" MishMash
*"We Are All Enlisted" Magstatic
*"Battle Hymn of the Republic" &mdash;Slender
*"Let Us All Press On" Mr. Fusion 
*"When Grandpa Comes" Slender
*"God Be With You Till We Meet Again" Jamen Brooks

==References==
  

==External links==
* 
*  

 

 
 
 
 
 