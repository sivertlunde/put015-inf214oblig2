MLA Mani: Patham Classum Gusthiyum
{{Infobox film
| name           = MLA Mani: Patham Classum Gusthiyum
| image          = MLA Mani Patham Classum Gusthiyum.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sreejith Paleri
| producer       = Joy Mulavanal
| writer         = T. A. Shahid
| screenplay     = 
| story          = Kalabhavan Mani
| based on       = 
| narrator       = 
| starring       = Kalabhavan Mani Lena Abhilash|Lena, Vidya
| music          = Kalabhavan Mani
| cinematography = Sudhi
| editing        = Mentos Antony
| studio         = Ann Maria Film International
| distributor    = 
| released       = April 20, 2012
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Lena and Vidya in the lead roles.  Kalabhavan Mani has also done the music and has sung all the songs in the film.

== Cast ==
* Kalabhavan Mani .... Manikandan Siddique .... Benjamin Martin Lena ....MLA Lakshmi Priya
* Vidya .... Meeenakshi
* Sadhika .... Parvathy Abu Salim
* Babu Namboothiri .... Asanar Mash
* Harisree Ashokan .... Pushkaran Vijayaraghavan .... Varkey
* Shammi Thilakan .... Amir Hussain
* Ambika Mohan as MLA Lakshmi Priyas mother

== Songs ==
{{Track listing
| extra_column    = Singers
| all_lyrics      = Murukan Kattakada
| all_music       = Kalabhavan Mani 
| title1          = Ambalakkulakkadavil
| extra1          = Kalabhavan Mani
| length1         = 
| title2          = Innale Nerathu
| extra2          = Kalabhavan Mani
| length2         = 
}}

== References ==
 

 
 
 


 