The Odd Couple (film)
 
{{Infobox film
| name           = The Odd Couple
| image          = Odd Couple poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Gene Saks
| producer       = Howard W. Koch
| based on       =   
| writer         = Neil Simon
| starring       = {{plainlist|
*Jack Lemmon
*Walter Matthau
*John Fiedler
*Herb Edelman
}}
| music          = Neal Hefti
| cinematography = Robert B. Hauser
| editing        = Frank Bracht
| distributor    = Paramount Pictures
| released       =  
| runtime        = 105 minutes  
| country        = United States
| language       = English
| budget         = $1.2 million
| gross          = $44,527,234   
}}

The Odd Couple is a 1968 American black comedy film written by Neil Simon, based on his play The Odd Couple, directed by Gene Saks, and starring Jack Lemmon and Walter Matthau. It is the story of two divorced men neurotic neat-freak Felix Ungar and fun-loving slob Oscar Madison who decide to live together, even though their personalities clash.
 fourth highest-grossing ABC The television sitcom of the same name, starring Tony Randall as Felix and Jack Klugman as Oscar.

== Plot ==
Felix Ungar (Jack Lemmon) checks into a fleabag hotel near Times Square, Manhattan and attempts to kill himself by jumping out of the window, but he fails to open it and pulls a muscle in his back. Limping back on the street he tries to get drunk at a dance bar and ends up hurting his neck when he throws down a shot. He stands on a bridge, contemplating jumping into the river.

Meanwhile, in the frowzy Upper West Side apartment of divorced sportswriter Oscar Madison (Walter Matthau) on a hot and sticky summer evening, Oscar and his buddies Speed (Larry Haines), Roy (David Sheiner), Vinnie (John Fiedler), and Murray (Herb Edelman) the cop are playing poker and discussing their friend, Felix Ungar, who is unusually late for the game. Murrays wife calls and tells him that Felix is missing. Oscar then calls Felixs wife Frances who says that she and Felix have split up. As they are discussing what to do, and worried that Felix might try to commit suicide, Felix arrives not knowing that his friends already know that his wife has kicked him out of the house.

Felix eventually breaks down crying and his friends try to console him. Oscar then suggests that Felix move in with him, since Oscar has lived alone since he split up with his own wife, Blanche, several months earlier. Felix agrees, and urges Oscar to not be shy about letting him know if he gets on Oscars nerves.
 Mets game on which he is reporting. The two men are shown bowling, shooting pool, and walking the city streets. Felix has a sinus attack, making loud obnoxious noises while seated in a coffee shop. Finally, after Felix drives everyone at the weekly poker game crazy, Oscar convinces Felix to lighten up and join him on a double-date with two English girls who live in the building &ndash; the Pigeon sisters, Cecily (Monica Evans) and Gwendolyn (Carole Shelley),  who actually "coo" when they laugh.

As the date commences, Oscar tries to get Felix to loosen up by leaving him alone for a while in their living room with the two attractive, and somewhat frisky, sisters. Instead, he winds up talking about Frances, and breaks down weeping. When Oscar returns from their kitchen, the Pigeon sisters, one a divorcee, the other widowed, are sobbing as uncontrollably as Felix. Oscar cheers them up and they invite the boys upstairs for what should be a wild night. Instead, Felix, who realizes that he is still too attached to his wife, refuses to go, opting to "scrub the pots and wash his hair" instead. Oscar joins the sisters in their apartment, but winds up spending the night drinking tea and telling them all about Felix.

Furious about Felixs ruining the date, Oscar resorts to giving Felix the silent treatment and torturing him by messing up the apartment as much as possible. Felix retaliates by just being himself, driving Oscar insane with his endless cleaning and neurotic behavior. Eventually, the tension explodes into an argument that results in Oscar demanding that Felix move out. Felix complies, but leaves Oscar with a major-league guilt trip for having abandoned his still-in-need friend.

Feeling awful about throwing Felix out, and not knowing where he has gone, Oscar assembles his poker buddies to search New York City for Felix in Murrays NYPD police car, which hes not supposed to use. After searching for hours, they return to Oscars apartment to find out that Felix has moved in with the Pigeon sisters. Oscar and Felix apologize to each other, and realize that a bit of each has rubbed off on the other, with each being a better person for it. Felix agrees that next Friday night, he will be at Oscars apartment for their poker game. Oscar tells his friends to clean up their mess after the poker game is over, ending the film.

== Cast ==
* Jack Lemmon as Felix Ungar
* Walter Matthau as Oscar Madison
* Herb Edelman as Murray
* John Fiedler as Vinnie
* David Sheiner as Roy
* Larry Haines as Speed
* Monica Evans as Cecily Pigeon
* Carole Shelley as Gwendolyn Pigeon
* Billie Bird as Chambermaid
* Iris Adrian as Waitress
* Angelique Pettyjohn as Go-Go dancer
* Ted Beniades as Bartender
* Bill Baldwin as Sports announcer

== Production ==
The Odd Couple was originally produced for   locations (most notably one at Shea Stadium in Queens, New York).

Oscars poker player friends also made up the cast; they were Roy (David Sheiner), Vinnie (John Fiedler), Speed (Larry Haines) and Murray the Cop (Herb Edelman). The film made its debut at Radio City Music Hall in 1968. It was a hit and earned Neil Simon a nomination for the Academy Award for Writing Adapted Screenplay. The film was also nominated for the Golden Globe Award for Best Motion Picture - Musical or Comedy and Lemmon and Matthau were both nominated for the Golden Globe Award for Best Actor - Motion Picture Musical or Comedy.

The scene at Shea Stadium, which also featured Heywood Hale Broun, was filmed right before a real game between the New York Mets and the Pittsburgh Pirates on June 27, 1967. Roberto Clemente was asked to hit into the triple play that Oscar misses, but he refused to do it and Bill Mazeroski took his place. 

One of the outdoor scenes in the film involved Felix shopping at Bohack, a Maspeth, Queens-based supermarket chain ubiquitous in the New York City area during the mid-20th century. The last Bohack supermarket closed in 1977. 

=== Theme music ===
The award-winning jazz instrumental theme was composed by Neal Hefti. The theme was used throughout the movies sequel, starring Lemmon and Matthau and released 30 years later, and also adapted for the 1970 TV series and used over the opening credits. The song also has seldom-heard lyrics, written by Sammy Cahn. 

== Reception == fourth highest review aggregate website Rotten Tomatoes. 

=== Awards and honors === Academy Award for Writing-Adapted Screenplay
* Frank Bracht was nominated for the Academy Award for Film Editing and for the American Cinema Editors "Eddie" award for Best Edited Feature Film.
* The film was nominated for the Golden Globe Award for Best Motion Picture - Musical or Comedy
* Jack Lemmon and Walter Matthau were each nominated for the Golden Globe Award for Best Actor - Motion Picture Musical or Comedy
* Gene Saks was nominated for the Directors Guild of America Award for Outstanding Directorial Achievement in Feature Film.
* In 2000, the American Film Institute honored The Odd Couple as the 17th greatest American Comedy

=== TV series === The Odd Couple. As the series ended, a cartoon version called The Oddball Couple ran on ABC for a year. Produced by Depatie-Freleng, it had a sloppy dog and a neat cat.

=== Sequel ===
A sequel, The Odd Couple II, reunited Jack Lemmon and Walter Matthau in their original roles and was released 30 years later, breaking the record for the length of time between a movie and its sequel featuring the original cast.

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 