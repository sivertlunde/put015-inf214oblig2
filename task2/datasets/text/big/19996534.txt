Sita Bibaha
{{Infobox film
| name           = Sita Bibaha
| image          = 
| caption        = 
| director       = Mohan Sundar Deb Goswami
| producer       = Mohan Sundar Deb Goswami
| story          = 
| screenplay     = 
| narrator       = 
| starring       = Makhanlal Bannerjee Mohan Sundar Deb Goswami Krishnachandra Singh Prabhavati
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 28 April 1936 
| runtime        = 
| country        = India
| language       = Oriya
}}
Seeta Bibaha (1936) ( ) was the first Oriya film directed by Mohan Sundar Deb Goswami; it presented the marriage of Lord Rama to Sita, based on the epic story Ramayana. The film starred Makhanlal Bannerjee, Mohan Sundar Deb Goswami, Krishnachandra Singh, and Prabhavati. It premiered April 28, 1936 at the Laxmi Talkies of Puri. Sita Bibaha was the first complete Oriya film and an important part of the struggle for a manifestation of Oriya cultural identity in celluloid form. The film was a commercial success. The two-hour movie was subsequently released at Cinema Palace in Cuttack. It also drew numerous crowds at several touring assignments such as the "Radhakishen Chamelia Touring Cinema". The second Oriya film was produced in 1949.   

==Production==
The film was sponsored by the Calcutta-based Kali Film Studio and cost Rs. Thirty thousand(30,000).

Mohan Sundar as the producer then had to seek loans to pay the artists. According to the vouchers signed by the artists, Makhanlal (Ram) had received Rs 120 on 30 October 1935. Adaita Ballabha Mohanty (Laxman) was paid just Rs 35 as a conveyance fee. Actress Miss Prabhabati (Sita) received the highest among the lot, Rs 150.

The total expenditure incurred to make the first commercial movie in the history of Odisha was Rs 29,781 and 10 anna. The picture was subjected to Bengal Board of Film Certificate and the police commissioner of Lal Bazaar awarded it the censor certificate. 

==Performance==
The performance of Narasingha Nandasharma as the boatman as well as Makhanlal Banerjee as Sri Ram were well received. Fourteen songs encapsulated in the movie amidst mythological interpretations were written by Mohan Sundar and Nandasharma. All singers for the movie were from Orissa and Mohan Sundar maintained traditional music of the land in the cinema. The settings of the songs and dances were carefully chosen.

Rishi Biswamitra arrives at the court of King Dasharatha to seek help of Ram and Laxman to combat the Asuras. A group song envelopes the durbar. Both the princes follow Biswamitra to an Ashram where a jagnya is underway. Tadaka and Subahu who then arrive to disrupt have to face the wrath of Rama|Ram. Tadaka dies while Subahu flees. A contented Biswamitra then welcomes the brother duo to Rishi Goutams Ashram. A major welcome song is then played in the cinema: "Ei je Ashrama Dekhuchha Sr Ram, Ei je banabaasa."

Post welcome, another song is sung in praise of Ram, "Debanka Tarane, Danuja Marane." Among other songs in the film are "Apurba Kumari Tripura Sundari" as an ode to Janakis beauty. Miss Prava in role of Sita, likewise, sings "Kali dekhithili madhura chaliki" which is jovially joined in by Sitas Sakhis who sing "Kahniki go priya sakhi?"

It was the arrival of the first complete Oriya film. And it was a commercial success too. The two-hour-long movie was released in Lakshmi Theatre, Puri and subsequently at Cinema Palace in Cuttack. It also brought in numerous crowds at several touring assignments like the ones conducted by "Radhakishen Chamelia Touring Cinema". The second Oriya film was produced in 1949. 

==Cast and crew==
* Director: Mohan Sundar Deb Goswami
* Play Writer: Kamapal Mishra
* Dialogues :Adweta Mohanty
* Sound Arrangements:Haricharan Mohanty
* Cast:Mohan Sundar Dev Goswami, Makhanlal Banarjee, Krushnachandra Singh, Ramachandra Lal, Narasingha Nanda, Adweta Mohanty, Prabhabati Devi, Radharani, Laxmipriya and Parbati were the artists in lead role

==References==
 

==External links==
*  

 
 
 
 
 