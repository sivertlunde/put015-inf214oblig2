Sacred Silence
{{Infobox film
| name           = Sacred Silence
| image_size     = 
| image	=	Sacred Silence FilmPoster.jpeg
| caption        =  Antonio Capuano
| producer       =  Antonio Capuano
| narrator       = 
| starring       = Fabrizio Bentivoglio 
| music          = 
| cinematography = Antonio Baldoni
| editing        = 
| distributor    = 
| released       = 1996
| runtime        = 
| country        =  Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1996 Italy|Italian Antonio Capuano pederastic relationship Napolitan street boy, and the domination of daily life in Southern Italy by the Camorra.  The title translates as Pianese Nunzio, 14 in May and the movie was released in the USA with the title Sacred Silence.

== Plot ==

Don Lorenzo Borrelli (Fabrizio Bentivoglio) is a priest in a poor neighborhood in Naples where Mafia killings are a daily occurrence and most young people see organized crime as a way to earn respect. Don Borelli tries as best as he can to persuade the adolescents that the Camorra is at odds with Catholicism, but has to learn that nothing will change as long as their parents silently accept the Mafia supremacy.

Borrellis personal life centers on his relationship with a 13-year-old choir boy, Nunzio Pianese (Emanuele Gargiulo), who is not only strikingly handsome but also a very talented musician. Nunzio plans on becoming a priest as well, as the easy life of a priest without worries about the future appeals to him.
 child molestation charge is a convenient way to get rid of the incendiary Don Lorenzo and try to get the local authorities to investigate. Meanwhile, Nunzio begins to doubt if he should stay his course or give in to the pressure to denounce Don Lorenzo.

== Cast ==

* Fabrizio Bentivoglio: Don Lorenzo Borrelli
* Emanuele Gargiulo: Nunzio Pianese
* Rosaria De Cicco: Rosaria
* Teresa Saponangelo: Anna Maria Pica
* Tonino Taiuti: Cuccarini 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 
 