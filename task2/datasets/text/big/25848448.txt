Tora-san Loves an Artist
{{Infobox film
| name = Tora-san Loves an Artist
| image = Tora-san Loves an Artist.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Keiko Kishi
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 107 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka Tora-san Goes French  is a 1973 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Keiko Kishi as his love interest or "Madonna".  Tora-san Loves an Artist is the twelfth entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
Tora-san watches the family shop while the rest of his family takes a vacation to Kyushu. 
An old friend introduces Tora-san to his sister Ritsuko, and he promptly falls in love with her. She is an artist and has no time for Tora-san.       

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Keiko Kishi as Ritsuko Yanagi
* Takehiko Maeda as Fumihiko Yanagi
* Chieko Baisho as Sakura
* Tatsuo Matsumura as Ryūzō Azuma
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hayato Nakamura as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Chishū Ryū as Gozen-sama

==Critical appraisal==
Stuart Galbraith IV calls Tora-san Loves an Artist a "solid" entry in the series, which is "alternately sweet and touching, funny and biting."  The German-language site molodezhnaja gives Tora-san Loves an Artist three and a half out of five stars.   

==Availability==
Tora-san Loves an Artist was released theatrically on December 16, 1973.  In Japan, the film has been released on videotape in 1995, and in DVD format in 2008. 

==References==
 

==Bibliography==

===English===
*  
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 


 