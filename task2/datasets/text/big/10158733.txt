Ploy
 
 
{{Infobox film
| name           = Ploy
| image          = Ploy movie poster.jpg
| caption        = The Thai movie poster
| director       = Pen-Ek Ratanaruang
| producer       = Chareon Iamphungphorn Kiatkamon Iamphungporn Wouter Barendrecht Michael J. Werner
| writer         = Pen-Ek Ratanaruang
| starring       = Lalita Panyopas Pornwut Sarasin Apinya Sakuljaroensuk Porntip Papanai Ananda Everingham Thaksakorn Pradabpongsa
| music          = Hualampong Riddim Koichi Shimizu
| cinematography = Chankit Chamnivikaipong
| editing        = Patamanadda Yukol
| distributor    = Five Star Production Fortissimo Films
| released       =  
| runtime        = 105 minutes
| country        = Thailand
| language       = Thai
| budget         = $2 million   
| gross          = $436,809  . Boxofficemojo. Retrieved 2011-12-18. 
}} 2007 Cinema Thai film written and directed by Pen-Ek Ratanaruang. The film premiered during the Directors Fortnight at the 2007 Cannes Film Festival.    {{cite web  | last = Leffler   | first = Rebecca   | title =
Wide range of views on Fortnight sked  | publisher = Hollywood Reporter  | date = archiveurl = archivedate = 2007-06-12}} 

The drama film stars Thai actress Lalita Panyopas in a story of a middle-aged married couple who question their relationship after seven years. Ananda Everingham is featured in a supporting role as a bartender.
 sex scenes censorship concerns cinemas in Thailand when it opened there on June 7, 2007. {{cite web  | last = Rithdee   | first = Kong | title =
Scenes from a marriage  | publisher = Bangkok Post  | date =
2007-06-08  | url = http://www.bangkokpost.com/topstories/topstories.php?id=119300 | accessdate = 2007-06-08 }} (Bangkok Post articles are archived after seven days for subscribers only)  The uncensored version of the film was shown in Thailand at the 2007 Bangkok International Film Festival.   

==Plot==
The death of a relative brings a Thai American|Thai-American couple, Wit and Dang, back to Bangkok for the first time in many years. Arriving at Bangkoks Suvarnabhumi Airport at around 5 a.m., they check in to a hotel in the city. Suffering from jet lag, Dang wants to sleep, but her husband Wit is restless and heads down to the hotel bar to buy some cigarettes. While unpacking their luggage, Dang finds a small paper with a phone number of a woman named Noy, and she is immediately suspicious.

In the hotel bar, Wit meets Ploy, a young woman who says she is waiting for her mother to arrive from Stockholm. The two bond over coffee and cigarettes, and Wit then invites Ploy to come up to the hotel room, where she can take a shower and rest while she awaits her mother. Up at the room, Dang opens the door and sees Ploy. To the girl, Dang is pleasant and friendly, but to her husband, she is seething with anger and jealousy. She has bouts of suspicion, followed by hallucinations / nightmares. Fitfully, the three people try to sleep. Dang and Wit discuss their seven-year marriage, and wonder why the love has gone out of it.

Meanwhile, a hotel maid, Tum, has stolen a guests suit from the hotels dry cleaners. She takes it up to room 609 and hangs it in the closet. Nut, the silent, weary bartender, enters the room and puts on the suit. He then finds Tum hiding in the shower, and proceeds to kiss her and caress her, and the couple starts having a sexual relationship.

Back in Wit and Dangs room, Dang becomes more upset, and decides to leave. Wit wakes up to find Dang gone, and then he and Ploy sit and smoke cigarettes and talk about love and relationships. They discuss how married life begins to lose its sheen eventually and think of options to keep the flame lit.

Ploy eventually leaves the hotel room, when Wit falls asleep. Out of curiosity, she examines the room in which she thinks the maid and the bartender had sex. She apparently has a dream about the entire episode.

Dang finds a coffee shop, where she takes a seat with a cup of coffee and pours some vodka in it from a small bottle she took from her hotel rooms mini-bar. She is noticed by Moo, who is using his laptop computer. He recognizes Dang from her days as a famous television soap opera actress and invites her back to his home for some more drinks. Dang, who has already had quite a bit to drink, eventually throws up. Moo, spikes her last drink and tries to get closer to her. When Dang turns down his advances, he attacks and rapes her, although she puts up a fight. When Dangs life is in danger, they end up playing a game of hide and seek that would end with one of them dead.

The film ends with Wit and Dang leaving Thailand, as Dang says nothing about her ugly encounter with the rapist, Moo; when Wit sees her back at the hotel, he is happy to have Dang back with him. He places his hand over Dangs in the taxi, and sincerely tells her he loves her, which she had earlier expressed longing to hear. Dang smiles, and rests her head on Wits shoulder lovingly as they exit Thailand to return to their home in the United States.

==Cast and characters==
* Lalita Panyopas as Dang: A former actress, still recognized and well known in Thailand, she has been married for seven years to Wit.
* Pornwut Sarasin as Wit: A Thai restaurateur whos lived in the United States for a number of years, and was widowed when he met Dang.
*  , she says she is 19 years old and is waiting for her mother to arrive from Stockholm. She has a black eye. Wit first spots her in the hotel bar with a young man whos passed out at a table, whom Wit assumes is Ploys boyfriend.
* Porntip Papanai as Tum: A hotel maid who steals a customers suit from the hotel dry cleaners.
* Ananda Everingham as Nut: The weary, silent bartender, he meets Tum in room 609, and puts on the suit she stole, and engages in some erotic role-playing with Tum.
* Thaksakorn Pradabpongsa as Moo: An antiques dealer who meets Dang in a coffee shop and invites her to his apartment for drinks.

==Production==

===Origins===
 
Director Pen-Ek Ratanaruang said he was inspired to write the screenplay after talking with a relative who had returned to Bangkok after some years in the United States. “My cousin had a restaurant in the U.S. and one day she came back to Thailand for a funeral,” Pen-Ek said. “But she didnt have a home here, so she had to stay in a hotel. I met her at the funeral and it struck me as rather odd that this Thai person had to stay in a hotel room when she came to Thailand. It inspired me to start writing a script about a Thai couple in a hotel room."   

The films proposal was submitted in 2006 to the Pusan Promotion Plan at the Pusan International Film Festival.      Budgeted at around US$2 million,   principal photography began in February 2007 in Bangkok. The locations included hotels and restaurants along Sukhumvit Road, as well as Suvarnabhumi Airport, making Ploy the first Thai feature film to feature Bangkoks new international airport. "Most of the action takes place in a hotel room. So we built our own room on the rooftop of a hotel, and it looks exactly like a real hotel room," Pen-Ek said in the films production notes. 

In March 2007, the Hong Kong/Amsterdam-based film financing and distribution company, Fortissimo Films, announced it would be executive producer of the film along with Thailands Five Star Production. Fortissimo takes care of international sales for the film, while Five Star handles distribution in Thailand.   

From the start, producers sought to make Ploy a smaller-scale film than Pen-Eks previous two films, Last Life in the Universe and Invisible Waves, which "were high-budget productions with international collaboration," said executive producer Chareon Iamphungporn.  The previous two films had also been scripted or co-scripted by Thai writer Prabda Yoon. Chareon said he wanted Pen-Ek to return to his screenwriter-director roots and  "come up with a project that had the personal warmth of his early films like Ruang Talok 69|6ixtynin9 and Monrak Transistor." 

The film reunited Pen-Ek with Thai cinematographer Chankit Chamnivikaipong, who had photographed his first three films, Fun Bar Karaoke, 6ixtynin9 and Monrak Transistor, before Pen-Ek collaborated with Christopher Doyle on Last Life in the Universe and Invisible Waves.

===Casting===
 
 
Ploy was a reunion for Pen-Ek and actress Lalita Panyopas, who is well known in Thailand for her roles in soap operas and a string of films in the 1980s. She starred in Pen-Eks 1999 crime comedy-drama Ruang Talok 69|6ixtynin9.

Pornwut Sarasin, who portrays the husband Wit, was a first-time actor. Forty-eight years old at the time of shooting, he is a vice president for Thai Namthip, the distributor of Coca-Cola in Thailand, and is well known in Thai high-society circles.

Ananda Everingham, who plays Nut the bartender, had worked with Pen-ek on a short film Twelve Twenty, commissioned for the 2006 Jeonju International Film Festival. He shares erotic scenes with model-actress Porntip Papanai, who plays the maid, Tum. She had a major role in Pen-eks 2001 musical-comedy, Monrak Transistor, portraying the singer, Dao.

For the title character, Ploy, Pen-Ek chose Apinya Sakulcharoensuk, who was 16 years old at the time of shooting. "Shes a godsend," the director said of the actress. "Its as if I could sense that the Ploy character is out there somewhere in the real world, and then she materializes in the form of this girl. Apinya was born to be Ploy."

Apinya had her hair styled into an Afro, a decision made by costume designer Visa Khongka, to make the character of Ploy look older and unique. 

==Reception==
The film premiered on May 21, 2007 during the Directors Fortnight at the 2007 Cannes Film Festival.    (Bangkok Post articles are archived after seven days for subscribers only)  Critical reaction was mixed. Russell Edwards, writing for the film industry trade publication Variety (magazine)|Variety, was negative, criticizing the film for its "glacial pace" and saying it was "too flimsy and false to truly engage."   

Lee Marshall, writing for another trade journal,  , erotic, funny and emotionally perceptive."     

"Ploy imposes its own unhurried rhythm but then rewards its viewers for their indulgence, and within the arthouse niche that it will inevitably inhabit this could turn out to be a strong seller," Marshall wrote. 

In an interview at Cannes, Pen-Ek reacted to the Variety and Screen Daily reviews, saying, "One is so bad and one is so good. Theyre equally not true." 

Todd Brown, writing from Cannes for the website Twitchfilm.net, said Ploy is a logical step in the directors evolution, continuing on themes that were explored in Last Life in the Universe and Invisible Waves. "Ploy is essentially a lucid dream, a film that takes place in that odd in-between state when you cannot be sure whether you are sleeping or awake and there are seemingly sure pointers that would have you believe both. It is a film about people dislocated and relationships formed while others are breaking down badly," Brown wrote. "Ploy is a beautiful, thoughtful, meditative film, one that requires more effort from its audience than does Last Life in the Universe but one that the patient will find no less rewarding."   

Kong Rithdee, critic for the Bangkok Post, said "Ploy is a finely tuned, mature piece of filmmaking that discusses adult themes with honesty and amused attention to the tiny details that define the shifting phases of a marriage. Ploy takes us closer to the characters than the director did in his last two outings ... and despite its dreamy episodes of hot-breath lovemaking, the movie is anchored in the sense of social realities more than his non-fans might care to observe." 

===Censorship issues=== sex between Thai films censorship code that prohibits such depictions.

Pen-Ek said he didnt like directing the sex scenes. "Its not fun at all. I dont know how to direct two people making love! And its not a good experience when they had to do it over and over again for 12 hours." 

In describing the scenes in his review, Russell Edwards of Variety called them "compellingly erotic", but sarcastically added that the scenes "indicate that cinemas gain has truly been pornographys loss." 

Todd Brown of Twitchfilm.net said the sex scenes might harm the films chances in Thailand, saying "Thailand is notorious for the uneven application of its censorship laws but graphic nudity is pretty much always frowned upon." 

Ahead of the films opening in Thailand, the countrys Board of Censorship ordered eight cuts for scenes it deemed obscene and inappropriate, mostly the lovemaking scenes.  The director film editing|re-edited the film, cutting some scenes and adding others not included in the Cannes version, so the length is unchanged. The version shown in Cannes is unlikely to be seen in Thailand.  

"Once again a good movie has been butchered by the powers-that-be, who believe their judgment is superior to ours," Bangkok Post film critic Kong Rithdee said in his review,  referring to another film that had run afoul of Thai censors earlier in the year, Syndromes and a Century, by Apichatpong Weerasethakul. That film was pulled from release by the director, who refused to make the cuts ordered.

For Ploy, the cuts have little effect on the film as whole, Rithdee said, though the contrast is less stark between the bartenders and maids steamy sex scenes and the chilly arguments of the married couple. 

The uncensored version of the film was screened in the main competition at the 2007 Bangkok International Film Festival. 

===Box office=== Thailand cinemas on June 7, 2007, but proved to be no match for the Thai comedy film, Nhong Teng kon maha hia, which was released the previous weekend and even topped a Hollywood release, Oceans Thirteen.
 baht (about US$171,000) in Bangkok cinemas, compared to the $2.28 million after two weeks of earnings by Nhong Teng kon maha hia.

Five Star Production had hoped the film would do better, based on the popularity of leading actress Lalita Panyopas, positive reviews from Cannes and word-of-mouth buzz.

"We expected a little more with Ploy," Co-producer Aphiradee Iamphungphorn was quoted as saying by Variety. "It is difficult to predict the market these days. Some people may have second thoughts whether they should see the film with the name Pen-Ek Ratanaruang as a director. Many still have this image that he only makes difficult movies."   

===Festivals and awards===
*2007 Cannes Film Festival, Directors Fortnight, world premiere
*2007 Bangkok International Film Festival, Golden Kinneree nominee for best international film
*2007 Osians Cinefan Festival of Asian and Arab Cinema, FIPRESCI award winner   
*2007 Toronto International Film Festival   
*2007 San Sebastián International Film Festival 
   
  
* 2008 Deauville Asian Film Festival
* 2008 Silk Screen Asian American Film Festival

==References==
 

==External links==
 
 
 
* 
* 
* 
*  at Five Star Production
*  at Five Star Production

 

 
 
 
 
 
 