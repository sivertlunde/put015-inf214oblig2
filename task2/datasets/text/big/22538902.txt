Lunatics: A Love Story
 
{{Infobox Film
| name           = Lunatics: A Love Story
| image          = Lunatics_Poster.jpg
| caption        = VHS Artwork
| director       = Josh Becker
| producer       = Sam Raimi Bruce Campbell Robert Tapert
| writer         = Josh Becker
| starring       = Ted Raimi Deborah Foreman Bruce Campbell George Aguilar Brian McCree
| distributor    = Renaissance Pictures SVS/Triumph Home Video
| released       = February 1991 February 21, 1992 July 29, 1992
| runtime        = 87 min.
| country        = United States English
| music          = Joseph LoDuca
| editing        = Kaye Davis
| cinematography = Jeffrey Dougherty
| awards         = 
| budget         = $650,000
| gross          = 
| followed_by    = 
| preceded_by    =  
|}}
 1991 comedy romance film with neo-noir (especially List of film noir#Psycho noir|psycho-noir) connections written and directed by Josh Becker, starring Ted Raimi, Deborah Foreman and Bruce Campbell. The film tells the story of a young, paranoid aspiring poet who, after an accidental phone conversation with a seemingly sweet woman, is forced to overcome his worries in order to win her heart. The film’s music was composed by Joseph LoDuca, and was edited by Kaye Davis.

==Plot==
In a rough area in Los Angeles, an aspiring poet has spent six months without leaving his apartment because of his obsessive delusions concerning cruel doctors, rappers, and spiders. Meanwhile, a woman who appears to curse things by wanting to help is dumped by her boyfriend and finds herself flat broke on the streets of LA. Soon she runs into a local gang. Due to a telephone glitch, our hero calls her at a phone booth trying to dial a "talk line" and invites her to his place. There they are forced to aid each other in overcoming their particular problems. 

==DVD Release==

Sony Pictures has yet to announce any plans to release the film onto DVD. Because of this, the original VHS release of the film has gone up for as much as $40 online. However, a DVD-R of the film has been released onto Josh Beckers website for the time being.

==Cast==
*Deborah Foreman as Nancy
*Ted Raimi as Hank 
*Bruce Campbell as Ray
*George Aguilar as Comet
*Brian McCree as Presto

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 


 