A Strange Role
{{Infobox film
| name           = A Strange Role
| image          = 
| caption        = 
| director       = Pál Sándor (director)|Pál Sándor
| producer       = 
| writer         = Pál Sándor Zsuzsa Tóth
| starring       = Margit Dajka
| music          = 
| cinematography = Elemér Ragályi
| editing        = Éva Kármentő
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}
 Best Foreign Language Film at the 50th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Margit Dajka - Öreg primadonna
* Irma Patkós - Kegyelmes Asszony
* Carla Romanelli - Olasznő
* Dezső Garas - Reményi / Glück úr / Fényképész
* Sándor Szabó (actor)|Sándor Szabó - Wallach doktor
* Endre Holmann - Galambos Sarolta / Kövesi János (as Holman Endre)
* Hédi Temessy - Ágota kisasszony
* Ildikó Pécsi - Mesternő
* Mária Lázár - Füsthajú nő
* Ági Margittay - Ambrusné (as Margitai Ági)
* Erzsébet Kútvölgyi - Zsófi nővér
* András Kern - Ács István
* György Simon - Lajos bácsi
* Georgiana Tarjan - Reményi Margitka (as Györgyi Tarján)
* Márk Zala - Különítményes tiszt

==See also==
* List of submissions to the 50th Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 