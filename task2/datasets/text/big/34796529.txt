Uliyin Osai
 
{{Infobox film
| name           = Uliyin Osai
| image          = Uliyin Osai Poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Ilavenil
| producer       = S. P. Murugesan
| writer         = M. Karunanidhi
| narrator       =
| starring       =Vineeth Keerthi Chawla
| music          = Ilayaraja
| cinematography =
| editing        = 
| studio         = 
| released       = 4 July 2008
| runtime        =
| country        = India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Indian Tamil Tamil Period Drama written by Karunanidhi and directed by Ilavenil, starring Vineeth in the lead role, with Keerthi Chawla.

==Plot==
The story is set in 1005 AD.

==Cast==
*Vineeth as Iniyan
*Keerthi Chawla as Chamundi
*Akshaya as Muthunagai
*Sarath Babu as Raja Raja Chola I Manorama as Azhagi
*Kovai Sarala as Sokki
*Ganja Karuppu as Soodamani
*Thalaivasal Vijay as Manikandan
*Bala Singh as Brammarayar
*Suja Varunee

==Reviews==
The movie opened with a promising note but mostly got negative reviews. 

==Awards==
Karunanidhi received the Tamil Nadu State award in 2008 as best dialogue writer.  
Kovai Sarala received the Tamil Nadu State award in 2008 for best comedian.   

==References==
 

 

 
 
 
 
 


 