Popeye the Sailor Meets Ali Baba's Forty Thieves
{{Infobox Hollywood cartoon|
| cartoon_name = Popeye the Sailor Meets Ali Babas Forty Thieves Popeye Color Features
| image = Popeye the Sailor Meets Ali Babas Forty Thieves.jpg
| caption =
| director = Dave Fleischer
| animator = Willard Bowsky   George Germanetti   Orestes Calpini
| voice_actor = Jack Mercer   Mae Questel   Gus Wickie   Lou Fleischer
| musician = Sammy Timberg   Sammy Lerner   Tot Seymour   Vee Lawnhurst
| producer = Max Fleischer
| studio = Fleischer Studios
| distributor = Paramount Pictures
| release_date = November 26, 1937
| color_process = Technicolor
| runtime = 17 min (two reels)
| preceded_by = Proteck the Weakerist
| followed_by = Fowl Play English
}} Popeye Color Feature series, produced in Technicolor and released to theatres on November 26, 1937 by Paramount Pictures.  It was produced by Max Fleischer for Fleischer Studios, Inc. and directed by Dave Fleischer. Willard Bowsky was head animator, with musical supervision by Sammy Timberg. The voice of Popeye is performed by Jack Mercer, with Mae Questel as Olive Oyl, with Lou Fleischer as J. Wellington Wimpy and Gus Wickie as Abu Hassan. It should also be noted that Popeye is enlisted in the US Coast Guard|U.S. Coast Guard and did not enlist in the Navy until 1941 in the Fleischer short The Mighty Navy.

==Plot== pipe as a blowtorch.
 Tabletop dimension|3D background process), Popeye sneaks past the guards and attempts to free Olive and Wimpy. He confronts Abu Hassan and demands that he give the Forty Thieves stolen jewels back to the people. He is apprehended and thrown into a shark pit. Just before being eaten by a shark, Popeye tangles the sharks teeth together, and the shark goes back down into the water. Popeye then produces his spinach, opening it by commanding the can "open sez me!" Now superpowered, Popeye defeats Abu Hassan, and all forty of the Thieves (counting them as he does so). The Thieves and Hassan are chained and made to drag a cart filled with the stolen jewels, Popeye, Olive, and Wimpy, back to town, where the townspeople await them with open arms. Popeye turns to Olive and sings, as the film irises out: 
"I may be a shorty, But I licked the Forty. Im Popeye the Sailor man! (toot-toot)"

==Release and reception== Snow White and the Seven Dwarfs. It made full use of Fleischer Studioss multiplane camera, which they had been experimenting with for some time.  Disney had just released The Old Mill, their first 3-D cartoon, and were advertising their upcoming Snow White as multiplanal as well. As such, advertising for "Forty Thieves" accented the fact that it was 3-dimensional. It was released just weeks before the seasonal Los Angeles premiere of Snow White and was essentially the only animated competition for the feature.

The short was the second of the three Popeye Color Specials, which were over sixteen minutes each, three times as long as a regular Popeye cartoon, and were often billed in theatres alongside or above the main feature. Today, this short and the other two Popeye Color Specials,  .

==External links==
 
* 
  
*  at the Big Cartoon Database
* 
*  in Flash and iPod formats.
 
 

 
 
 
 
 
 