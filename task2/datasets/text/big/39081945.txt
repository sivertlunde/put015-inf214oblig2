I Don't Luv U
 

{{Infobox film
| name           = I Dont Luv U
| image          = I Dont Luv U.Jpeg
| caption        = Movie Poster
| director       = Amit Kasaria
| producer       = Dr Anil Kumar Sharma
| writer         = Amit Kasaria
| starring       = {{Plainlist|
* Ruslaan Mumtaz
* Chetna Pande
* Murali Sharma
* Jass Bhatia 
* Ragesh Asthana
}}
| studio         = Amrapali Media Vision
| released       =  
| music          =  Aman- Benson & Amit Kasaria
| country        = India
| language       = Hindi
}}
I Dont Luv U is a 2013 Bollywood romantic drama film, starring Ruslaan Mumtaz and Chetna Pande in the lead roles. The film is produced by Amit Kasaria under the banner of Amrapali Media Vision. The film was released on 17 May 2013 in India. 

==Story==
Yuvaan (Ruslaan Mumtaz) is a friendly 19 year old college boy. The story begins with Yuvaan falling from a balcony with a diary, and being taken to the hospital. An inspector comes for investigation and finds out that he is the same guy who was there in the mms scandal . He starts thinking that if Yuvaan wanted to commit suicide then why with the diary . Then he starts reading the diary. The diary begins when Yuvaan meets Aayra (Chetna Pande), an NRI, and is attracted to her. One day Yuvaan asks Aayra whether they can become a couple as both of them are single or can even be friends but Aayra refuses. Yuvaan turns and walks away when he sees one of his friends fighting, and he stops the fight.  His friend is shattered and says that the girl he loved was cheating on him. After that, Yuvaan tries cheering his friend up and doesnt see Aayra much.  Seeing Yuvaan caring so much for his friend, Aayra starts liking Yuvaan and becomes friends with him. One day due to some problem Aayra and Yuvaan dont talk to each other, although both are eager to. At the end of the day, both meet and say each other how much they had missed each other.  Aayra starts loving Yuvaan and writes all this in her diary. One day, as nobody else is in her house, she calls Yuvaan, and he excitedly goes to her house where they start taking pictures. Yuvaan by mistake keeps his video recorder in his cellphone on and keeps it on the desk as he goes near Aayra. Yuvaan kisses Aayra, and zips her dress. Aayra cries since she didnt know he would do that, and Yuvaan zips her dress back.  Aayra hugs Yuvaan. The inspector reads this and is shocked he says that some how the story which has been shown is incorrect . The actual thing is something else and he has to find it out . He goes to the hospital where Yuvaan is admitted and the doctor tells that he is serious and only one doctor can save him who is going to Shimla for doctors meeting . The inspector brings the doctor to the hospital and the doctor does Yuvaans operation . By this time the inspector asks the actual story of what happened when Yuvaan returned from Aayras home . Amey says that the next day Yuvaan, Amey, Vibhu and Lovely planned of going out and having fun, but before leaving they met Aayra who gave him her diary in which she had written how she really feels for him . Lovely takes Yuvaans phone from him and doesnt give him back . When they return from the trip Amey got a call from a friend about the mms of Aayra and Yuvaan which had been leaked in the whole college and even the media know about it . Yuvaan gets arrested and when he comes back he tries finding a way to remove Aaryas name from all this mess as he really loves her, so he takes her diary and falls off the balcony . Later Yuvaans mobile falls from Lovelys pocket and the mms is seen by a media reporter and they use it just for the TRP .It goes on air because of the media . All the public take Yuvaans side and pray for his fast recovery . After a month Yuvaan gets alert and goes to meet Aayra with the inspector . He says her how much he loves her and asks for forgiveness, Aayra is broken due to the news forgives him but says as his sorry wont bring her life back and says that it wont matter as she is going abroad next week . There is a news in which Yuvaan is being interviewed and is asked that the whole country has forgiven him but did Aayra do ? He says he doesnt know but the only thing he knows is he loves Aayra a lot and will always love her . Aayras dad leaves India but leaves Aarya in India and both Aayra and Yuvaan are shown together .

==Cast==

* Ruslaan Mumtaz as Yuvan
* Murali Sharma
* Chetna Pande as Aayra
* Ragesh Asthana
* Jass Bhatia as Lovely
* Ravi Khemu
* Paramjyot Singh

==Soundtrack==
The soundtrack album has eleven songs and The music is composed by music duo Aman Pant-Benson Baby

===Track Listings===
{| class="wikitable"
|-
! No. !! Title !! Singer(s) !! Duration
|-
| 1 || I Dont Luv U || Neuman Pinto, Monali Thakur || 4:02
|-
| 2 || Ishq ki #@@ ki || Mika Singh || 3:13
|- Shaan || 3:47
|-
| 4 || Mere Khuda || Javed Ali, Hamsar Hayat || 5:11
|-
| 5 || Kuchh Hone Ko Hai || Raman Mahadevan, Joi Barua || 3:52
|-
| 6 || Jaane Kaise Do I Luv U || Linda M Johny || 3:36
|-
| 7 || Mohe Apne Hi || Fareed Hasan || 4:27
|-
| 8 || Mission Tadofier || Siraj Khan || 2:28
|-
| 9 || I Dont Luv U (Club Version) || Neuman Pinto, Rob C, Monali Thakur || 4:18
|-
| 10 || Ishq Ki (Club version) || Mika Singh || 3:36
|}

==References==
 

==External links==
*  

 
 
 