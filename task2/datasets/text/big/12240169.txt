Female Convict 701: Scorpion
{{Infobox film
| name           = Female Prisoner 701: Scorpion 女囚701号/さそり
| image          = Female Convict Scorpion 1972.jpg
| image size     = 220px
| caption        = DVD cover
| image_size     =
| director       = Shunya Itō 
| producer       =
| writer         = Fumio Konami Hirō Matsuda
| starring       = Meiko Kaji Rie Yokoyama
| music          = Shunsuke Kikuchi
| cinematography = Hanjiro Nakazawa
| editing        = Osamu Tanaka
| distributor    = Toei Company
| released       = August 25, 1972 (Japan)
| runtime        = 87 minutes France: 83 minutes
| country        = Japan Japanese
| budget         =
| gross          =
| based_on = A comic by Toru Shinohara
}}

  Also known as Female Prisoner 701: Scorpion  is a film made by   and  , and has also been remade several times.

==Plot==
 Japanese mafia orchestrate a plan in which Matsushima will succumb to an "accidental" death in prison. They enlist the help of Katagiri, pulling on her ties to both Sugimi and the mafia, and quickly set their plan in motion.

Matsushima is attacked in the shower but defends herself, wounding the attacker. She is punished by being held bound by ropes in solitary confinement. A group of trustees including Katagiri torments her. One of them torments her by pouring hot soup on her. Matsushima is able to trip the trustee and make her spill the vat of hot soup over herself, causing horrible burns. Matsushima is forced to dig dirt holes for two days and nights straight. She kills a woman who attempts to attack her during this digging by tripping her and breaking her neck. Matsushima is hung and tied from the ceiling while being beaten by her fellow prisoners.

After a riot, Matsushima escapes and kills all the Yakuza and Sugimi with a dagger. The film ends with Matsushima walking alone back in prison.

== Cast ==
* Meiko Kaji - Nami Matsushima (aka Matsu the Scorpion)
* Rie Yokoyama - Katagiri
* Yayoi Watanabe - Yukiko Kida
* Yōko Mihara - Masaki
* Akemi Negishi - Otsuka
* Keiko Kuni - Nemoto
* Yumiko Katayama - Kito
* Emi Jo - Morikawa
* Isao Natsuyagi - Tsugio Sugimi
* Fumio Watanabe - Warden Goda
	
==Notes==
 

==References==
 

==Sources==

*  
*  

==External links==
*  
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 