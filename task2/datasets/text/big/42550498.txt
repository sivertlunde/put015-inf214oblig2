Yemaatrathe Yemaaraathe
{{Infobox film
| name = Yemaatrathe Yemaaraathe
| image = YemaatratheYemaaraathe.jpg
| caption = 
| director = V.C. Guhanathan
| producer = United Cine Technicians
| Lyrics= Gangai Amaran
| dialogues= 
| screenplay=  Archana Sumithra Sumithra Anuradha Anuradha Nizhalgal Ravi Chandrabose
| cinematography = 
| editing = 
| released = 1985
| country = India
| language = Tamil
}}
 1985 Tamil Tamil  Indian feature directed by Archana and Sumithra in the lead roles. 

== Cast ==

*Vijayakanth Archana
*Sumithra Sumithra
*Anuradha Anuradha
*Nizhalgal Ravi
*Suruli Rajan

==Soundtrack== 
{{Infobox album  
| Name        =  Yemaatrathe Yemaaraathe
| Type        =  soundtrack Chandrabose
| Cover       =  
| Caption     = 
| Released    =  1985
| Recorded    =  
| Genre       =  
| Length      =   Tamil
| Label       =  EMI-The Gramaphone Company of India Ltd.
| Producer    =  
| Reviews     =  
| Compiler    =  
| Misc        =  
}}

* "Pudikkaiyile.." - P. Jayachandran, SP Sailaja
* "Ila Maalai Nilavo.." - SP Balasubramanyam, SP Sailaja & Chorus
* "Naana Vambukku.." - T. M. Soundararajan & Chorus
* "Engal Thamizhinam.." - Malaysia Vasudevan
* "Iru Kannil.." - Malaysia Vasudevan, Vani Jairam

==References==

 

==External links==
*  

 
 
 
 
 


 