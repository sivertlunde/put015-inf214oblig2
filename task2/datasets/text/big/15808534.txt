Perils of the Wild
 
{{Infobox film
| name           = Perils of the Wild
| image          = Perilsofthewild.jpg
| caption        = Theatrical poster Francis Ford
| producer       = 
| writer         = Isadore Bernstein William Lord Wright Johann David Wyss
| starring       = Joe Bonomo Margaret Quimby
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 15 episodes 
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 adventure film Francis Ford. The film is considered to be lost film|lost.     This serial was based on The Swiss Family Robinson by Johann David Wyss. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut   
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 322
 | chapter = 13. The Classics "You Say What Dost Thou Mean By That? and Push Him Off the Cliff"
 }} 

==Cast== Joe Bonomo as Frederick Robinson. Bonomo performed his own stunts.  He fractured his leg and received an injured sacroiliac during filming. 
* Margaret Quimby as Emily Montrose
* Jack Mower as Sir Charles Leicester Alfred Allen as Captain William Robinson
* Eva Gordon as Frau Mitilla Robinson
* Jack Murphy as Jack Robinson
* Howard Enstedt as Ernest Robinson
* Francis Irwin as Francis Robinson William Dyer as Black John
* Albert Prisco as Tonie
* Fanny Warren as Bonita (as Fannie Warren)
* John Wallace as Pirate
* James Welsh as Pirate Philip Ford as Pirate (as Phil Ford)
* Sammy Gervon as Pirate
* Boris Karloff

==See also==
* List of film serials
* List of film serials by studio
* List of lost films
* Boris Karloff filmography

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 