Apache Drums
{{Infobox film
| name           = Apache Drums
| image          = Apache Drums Poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Hugo Fregonese
| producer       = Val Lewton
| screenplay     = David Chandler Harry Brown
| based on       = "Stand at Spanish Boot"
| narrator       = 
| starring       = Stephen McNally Coleen Gray 
| music          = Hans J. Salter
| cinematography = Charles P. Boyle
| editing        = Milton Carruth
| studio         = 
| distributor    = Universal International Pictures#Universal-International|Universal-International
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $1.4 million (US rentals) 
}} Western directed by Hugo Fregonese and produced by Val Lewton.  The drama features Stephen McNally, Coleen Gray, and Willard Parker.  The film was based on the novel Stand at Spanish Boot, by Harry Brown.   Apache Drums was the last film Val Lewton produced before his death. 

==Plot==
A notorious gambler is thrown out of a small town named Spanish Boot, but he quickly returns when he discovers the town is threatened by the Mescalero Apaches led by Chief Victorio.

==Cast==
* Stephen McNally as Sam Leeds
* Coleen Gray as Sally
* Willard Parker as Mayor Joe Madden
* Arthur Shields as Rev. Griffin
* James Griffith as Lt. Glidden
* Armando Silvestre as Pedro-Peter
* Georgia Backus as Mrs. Keon
* Clarence Muse as Jehu
* Ruthelma Stevens as Betty Careless
* James Best as Bert Keon
* Chinto Guzman as Chacho
* Ray Bennett as Mr. Keon

==Reception==

===Critical response===
When the film was released The New York Times gave the film a mixed review and wrote, "Apache Drums is tense and exciting fare when its green and red-painted Indians, yelping and keening, ride to attack or literally bite the dust with authentic thuds. When it is loquaciously appraising its principals, it is, to quote one of them, kind of dull and tame." 

Recently, film critic Dennis Schwartz reviewed the film favorably, writing, "Its the kind of effective kickass B western where the cavalry comes in the nick of time to rescue the white folks from the attacking Indians. Director Hugo Fregonese (Untamed Frontier) gives a nod to Lewtons eye for detail and shadowy photography...David Chandler turns in a crisp screenplay thats always tense and filled with exciting action sequences except when he keeps things too chatty, which tamps down the narrative with a dull soap opera romantic feud...Pretty darn good stuff for such a modest western, showing that it takes all kinds to be brave and that the worst situation might bring out the best in a man." 
 The Men of Harlech)." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 