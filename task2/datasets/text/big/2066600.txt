Coffy
{{Infobox film
| name        = Coffy
| image       = Coffy.jpg
| writer      = Jack Hill
| starring    = Pam Grier Booker Bradshaw Robert DoQui William Elliott Allan Arbus Sid Haig
| director    = Jack Hill
| producer    = Robert Papazian
| distributor = American International Pictures
| released    =  
| runtime     = 91 min.
| country     = United States
| language    = English
| budget      = $500,000 
| music       = Roy Ayers
| gross       = $2 million  (rentals)   Samuel Z Arkoff & Richard Turbo, Flying Through Hollywood By the Seat of My Pants, Birch Lane Press, 1992 p 202 
}} written and directed by American filmmaker black female vigilante played by Pam Grier. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 40 

The films tagline in advertising was "They call her Coffy and shell  cream  you!"   

==Plot==
Nurse "Coffy" Coffin (Pam Grier), seeks revenge for her younger sisters getting hooked on drugs and having to live in a rehabilitation home, a product of the drug underworld, mob bosses and chain of violence that exists in her city. The film opens with Coffy showing her vigilante nature by killing a drug supplier and dealer. She does this without getting caught by using her sexuality as an attractive and athletic woman willing to do anything for a drug fix. 

She lures the men to their residences, which gives her the privacy to kill them both. After the killings, Coffy returns to her regular job at a local hospital operating room, but is asked to leave when she is too jumpy when handing tools to the surgeon. 

The film introduces Coffy’s  police friend Carter (William Elliott), who used to date Coffy in their younger years. Carter is portrayed as a straight-shooting officer who is not willing to bend the law for the mob or thugs who have been bribing many officers at his precinct. Coffy doesnt believe his strong moral resolve until two hooded men break into Carters house while shes there and beat Carter severely, temporarily crippling him. This enrages Coffy, giving her further provocation to continue her work as a vigilante, killing those responsible for harming Carter and her sister.

Coffys boyfriend Howard Brunswick (Booker Bradshaw) is a city councilman who appears to be deeply in love with Coffy at the beginning of the film. Coffy admires Brunswick for his body as well as his use of law to solve societal problems. She is very happy when he announces his plan to run for United States Congress|Congress, and his purchase of a night club. The two share a passionate love scene in the first part of the film.

Coffys next targets are a pimp named King George (Robert DoQui), who is supposedly one of the largest providers of prostitutes and illegal substances in the city, and Mafia boss Arturo Vitroni (Allan Arbus). 

Coffy questions and abuses a former patient of hers who was a known drug user to gain insight into the type of woman King George likes and where he keeps his stash of drugs. This is the first scene where Coffy brutalizes another woman and shows no remorse because the former patient is using drugs again and thus a societal deviant. Coffy quickly goes to a resort posing as a Jamaican woman looking to work for King George.

George is quickly interested in her exotic nature and asks her to come with him back to his house to experience Coffy himself first. One of the prostitutes returns from a far away job and gets disgruntled and jealous when seeing George taking such a liking to Coffy. At a party later that day Coffy and the other prostitutes get into a massive brawl, which entices mob boss Vitroni and he demands that he have her tonight. 

Coffy prepares herself to murder Vitroni and just when she is about to shoot, is overtaken by his men. She lies and tells Vitroni that King George ordered her to kill him, which makes Vitroni order George to be murdered. Vitronis men kill George by dragging him through the streets by a noose. 

Coffy then discovers her clean-cut boyfriend is actually corrupt when shes shown to him at a meeting of the mob and several police officials. He denies knowing her other than as a prostitute and Coffy is sent to her death. Once again, Coffy uses her sexuality to seduce her would-be killers. They try injecting her with drugs to sedate her, but she had switched these out for sugar earlier. Faking a high, she kills her unsuspecting hitmen with a pointed metal wire she fashioned herself and hid in her hair.
 carjacks a vehicle to escape. Coffy drives to Vitronis house, murders him, and then goes to Brunswicks to do the same. He pleads forgiveness and just as she is about to accept, a naked white woman comes out of the bedroom. At this, Coffy shoots Brunswick in the groin. The film then closes with Coffy being satisfied at having avenged her sister and Carter.

==Production== American International Pictures head of production, Larry Gordon, lost the rights to the film Cleopatra Jones after making a handshake deal with the producers. Gordon subsequently approached Hill to quickly make a movie about an African American womans revenge and beat Cleopatra Jones to market.  Hill wanted to work with Pam Grier with whom he had worked on The Big Doll House (1971). The film ended up earning more money than Cleopatra Jones and established Grier as an icon of the genre.

Coffy is notable in its depiction of a strong female lead (a capable nurse), something rare in the genre at the time, and also in its then-unfashionable anti-drug message. It was remade in 1981, with an all-white cast, as Lovely But Deadly.

==Cast and roles==
* Pam Grier - Coffy
* Booker Bradshaw - Howard Brunswick
* Robert DoQui - King George
* William Elliott - Carter, a police officer
* Allan Arbus - Arturo Vitroni
* Sid Haig - Omar, a henchman of Vitroni Barry Cahill - McHenry, a police officer
* Lee de Broux - Nick, a police officer
* Ruben Moreno - Ramos
* Lisa Farringer - Jeri, one of King Georges girls
* Carol Locatell - Priscilla (credited as Carol Lawson)
* Linda Haynes - Meg, one of King Georges girls
* John Perak - Aleva
* Mwako Cumbuka - Grover, Sugarmans henchman
* Morris Buchanan - Sugarman
* Bob Minor - Studs

==Soundtrack==
 

==Reception==
The movie received a mixed reception.  Foxy Brown Friday Foster, and Sheba, Baby (both 1975). 
 Jackie Brown in 1997, a film with clear inspiration from films like Coffy and Foxy Brown. Tarantino said of the film poster: "Not only is it a great image of Pam Grier, its got great type — its the epitome of a great exploitation poster...and every version of it in foreign countries rocked"..   

==Release on DVD & HD==
* In 2003 it was released on DVD,  and rereleased on DVD on December 6, 2005 as part of the Vibe magazine|Vibe Fox In A Box collection.  
* In 2010 it was digitized in High Definition (1080i) and broadcast on MGM HD.

==See also==
*List of American films of 1973
*List of female action heroes

==References==
 

==External links==
* 
*  
*  at Trailers From Hell

 

 
 
 
 
 
 
 
 
 
 
 
 