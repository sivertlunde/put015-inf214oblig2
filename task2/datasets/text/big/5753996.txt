She (1935 film)
 
{{Infobox film
| name           = She
| image          = She (1935).jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Lansing C. Holden Irving Pichel
| producer       = Merian C. Cooper
| writer         = 
| screenplay     = Dudley Nichols Ruth Rose
| story          = 
| based on       =   
| narrator       =  Helen Gahagan Randolph Scott Helen Mack Nigel Bruce Gustav von Seyffertitz
| music          = Max Steiner
| cinematography = J. Roy Hunt
| editing        = 
| studio         = 
| distributor    = RKO Pictures
| released       =  
| runtime        = 102 min (original theatrical release) 94 min. (1949 re-release)
| country        = 
| language       = 
| budget         = 
| gross          =
}}
 

She is a  ,   and Wisdoms Daughter. The film reached a new generation of moviegoers with a 1949 re-release.
 Helen Gahagan, Randolph Scott and Nigel Bruce.
 John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

In 2006, Legend Films and Ray Harryhausen colorized the film as a tribute to Cooper. The colorized trailer for She premiered at the 2006 Comic-Con International|Comic-Con. 

She was considered a lost film for many years until an original print, stored in silent film star Buster Keatons garage, was turned over to film distributor Raymond Rohauer for preservation.   
 The Last Days of Pompeii.
 Kino Video. 

==Characters and story==
Leo Vincey (Randolph Scott) is called from America to the familys ancestral estate in England where his dying uncle John Vincey (Samuel S. Hinds) and Horace Holly (Nigel Bruce) convince him that their ancestor, also named John Vincey (also played by Scott) found the fountain of youth 500 years ago.

Following the route outlined in an old journal, Leo and Holly travel through frozen wastes, as a guide named Tugmore and his daughter, Tanya (Helen Mack) join them on their quest. They stumble upon the ancient city of Kor, where they are attacked by cannibals but are saved by She Who Must Be Obeyed (Helen Gahagan) and her Minister Billali (Gustav von Seyffertitz).

She believes that Leo is the reincarnation of her lover, John Vincey and vows to make him immortal like herself to rule this shangri-la in eternal youth. Tanya warns Leo that nothing human can live forever. At the end, She asks Leo to step into the Flame of Life with her, so that they can become immortal. When Leo hesitates, She offers to step in first. Rather than renewing her youth, She ages hundreds of years, becomes a withered mummy-like creature and dies. Leo, Holly and Tanya then safely make their escape.

==Differences from the novel==
The film version conflates characters, events and locations from all three books:
* The location of Ayeshas realm is a free adaptation from the second novel. In the film, it is located in the extreme north of the Russian Arctic, whereas in the original novel   the city of Kor is located in eastern Africa, in the caldera of an extinct volcano. The second novel, Ayesha (novel)|Ayesha has a reincarnated Ayesha living in a remote valley in the Himalayas, just south of Tibet, inhabited by descendents of a lost troop of Greek soldiers from the time of Alexander the Great. 
* In the books, Ayesha is so beautiful that she must be veiled at all times. The film skirts this (although Ayesha is veiled in her first on-screen appearance), and it does not mention all of the supernatural powers the books ascribe to her. 
* The film does not mention any of the Egyptian, African, or Arabian back story from the books.
* The character of Tanya does not appear in any of the books; rather, she is a composite of the other rivals for Leos affections: the Amahagger maiden Ustane from  , the Khania Atene from Ayesha (novel)|Ayesha and, Inez, the daughter of the trader Robertson, from She and Allan. In the books, Ayesha directly claims that Atene is the reincarnation of her ancient Egyptian rival, the Princess Amenertas. The fate of Tanya and the ending are different from the books.

==Cast== Helen Gahagan as She (Who Must Be Obeyed)
*Randolph Scott as John Vincey and Leo Vincey
*Nigel Bruce as Professor Horace Holly
*Helen Mack as Tanya Dugmore
*Gustav von Seyffertitz as  Billali, Shes mortal Governor

==Home media==
Legend Films release 
*Picture Format: 1.33:1 (1080p 24fps)  
*Soundtrack(s): English (Dolby Digital 2.0 Dual Mono)
*Extras (Blu-ray):
**Things to Come (1936) in colorized and black & white versions
**Commentary by Ray Harryhausen and Mark Vaz on She
**Interviews with Ray Harryhausen    
**Colorization Process with Ray Harryhausen    
*Extras (DVD): The Most Dangerous Game (1932) in colorized and black & white versions
**Ray Harryhausen on the Importance of a Movie Score (2:31)
**James V. DArc, Curator of the Merian C. Cooper Papers, BYU (4:30) 
**John Morgan , Composer, on Max Steiner (7:15)

==See also==
*She (novel)
*She (1965 film)
*She (1982 film)

==References==
 

==External links==
*  
* 
*  
*  on Escape (radio program)|Escape: July 11, 1948

 
 

 
 
 
 
 
 
 
 
 
 
 
 