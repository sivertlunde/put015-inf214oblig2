Rhythm of the Islands
{{Infobox film
| name           =Rhythm of the Islands
| image          =
| image_size     =
| caption        =
| director       = Universal Pictures
| writer         =
| narrator       =
| starring       = Jane Frazee
| music          =
| cinematography =
| editor       = Universal Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}}

Rhythm of the Islands is a 1943 American film starring Jane Frazee. It is also known as Isle of Romance.

==Plot Summary==

Two ambitious guys from Brooklyn, Tommy Jones and Eddie Dolan, get the idea of buying a tropical island in the South Pacific to exploit as a tourist paradise. They go there and start rounding up customers on the beach, to bring to native shows they set up with the help of the locals.

The plan is to turn the idea into a tourist trap so they can sell it for a good profit, go back to the U.S., and pay back the $500 they borrowed from Eddies fiancée, Susie Dugan.

They are in luck, because a Manhattan millionaire, Mr. Holton, visits the island with his family, and offers to buy the island for the sum of $10,000. But before the deal goes through, a tribe of cannibals also arrive on the island, spreading terror and capturing all the natives. In the middle of all this, Susie comes to the island to marry Eddie. Together with the Holtons, she is captured by the cannibals.

Since Tommy thinks the whole invasion is part of the show, he befriends the cannibal chief, Nataro. At the same time, Eddie becomes romantically interested in a beautiful native girl, Luani.

Mr. Holtons daughter Joan falls in love with Tommy, but when she finds out about his friendship with their captor Nataro, her feelings cool off and instead she blames him for everything thats happened to her family. When Tommy hears this, he persuades Nataro to release his captives. The condition for their release is that they all must immediately leave the island and never return.

However, when they are released, they refuse to leave the island, and an argument over the ownership of the island ensues between Mr. Holton and Susie. Nataro comes to the rescue, and reunites Eddie and Susie as a couple. He also reconciles Tommy with Joan and both couples are married in a native cannibal ceremony there on the island. After this they are all allowed to stay on the island for as long as they wish. 

==Cast==
 Allan Jones as Tommy Jones, also known as "Chief Atamu"
*Jane Frazee as Joan Holton
*Andy Devine as Eddie Dolan
*Ernest Truex as Mr. Holton
*Marjorie Gateson as Mrs. Holton
*Mary Wickes as Susie Dugan Burnu Acquanetta as Luani
*Nestor Paiva as Nataro John Maxwell as Marco
*Maceo Anderson as Abercrombie The Step-Brothers as Abercrombies assistants
*The Lester Horton Dancers

==External links==
*  
*  

==References==
 

 

 
 
 
 
 
 
 
 
 
 


 