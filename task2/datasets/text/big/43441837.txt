Judge Mujrim
{{Infobox film
| name            = Judge Mujrim
| image           = 
| caption         = 
| director        = Jagdish A. Sharma
| producer        = Jayant B. Shah
| Writer          = 
| screenplay      = S.Khan
| starring        = Jeetendra Sunil Shetty Ashwini Bhave
| music           = Bappi Lahiri
| cinematography  = Raj Kumar Khatri
| editing         = Hussain A. Burmawala
| distributor     =
| released        = September 12, 1997
| runtime         = 138 mins
| language        = Hindi
| country         = India
| budget          = 
| gross           = 
}}
Judge Mujrim is a 1997 Bollywood Action film directed by Jagdish A Sharma. The film starred Jeetendra, Sunil Shetty, Ashwini Bhave in lead roles. 

==Plot== 
Judge Pratap Sinha (Jeetendra) is a renowned judge who does not pass judgement by sitting down on the judges chair but he himself investigates and bring the criminals to their fate. This worshiped of law has a wife Sujata (Sujata Mehta) who is a very famous lawyer. He also has a sister Ashwini (Ashwini Bhave) who is a brave police officer. The citys Mafia Don D.V.Ms (Kiran Kumar) brother Jaggi commits a gruesome murder of a lady journalist Bharti. He is arrested and tried by judge Pratap Sinha who passes a death sentence on Jaggi. One day while judge Pratap was going to the court he sees Sunil (Sunil Shetty) stabbing one man to death. With Pratap as witness he tells Ashwini to arrest Sunil, who gets shocked because Sunil is her lover. The court sentences Sunil to death. In jail Sunil meets notorious criminal Mangal (Mukesh Khanna), who is Pratap Sinhas enemy. Before Sunil is hanged, D.V.M. tells judge Pratap that Sunil is innocent and that D.V.M. himself had trapped Sunil. Judge Pratap is stunned as if a lightening had struck him. To save the respect of the law Pratap has to break the law. He runs away with Sunil from jail. Behind judge Pratap and criminal Sunil, are the full police force from one side and on the other is D.V.Ms henchmen. Did Sunil prove his innocence? What happened to D.V.M. in the end?

==Cast==
* Jeetendra as Justice Pratap Sinha 
* Sunil Shetty as Sunil / Dhaga
* Ashwini Bhave as Inspector Ashwini Sinha
* Sujata Mehta as Mrs. Sujata Sinha 
* Johnny Lever as Havaldar Amar Lokhande
* Mukesh Khanna as Mangal Singh
* Kiran Kumar as D.V.M
* Ashok Saraf as PA Natwar
* Surendra Pal
* Vikas Anand
* Ayesha Jhulka
* Kunika
* Archana Puran Singh

==References==
 

==External links==
*  at the Internet Movie Database

 
 
 

 