A Merry Mix Up
{{Infobox Film |
  | name           = A Merry Mix-Up |
  | image          = Merrymixup57.jpg|
  | caption        =  Columbia Pictures made an error by listing the Stooges as "Shemp (who had died), Larry and Joe," omitting Moe entirely. |
  | director       = Jules White Felix Adler| | Ruth Godfrey White Suzanne Ridgeway Harriette Tarler Diana Darrin Frank Sully |
  | cinematography = Irving Lippman |  Harold White |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 15 51"
  | country        = United States
  | language       = English
}}

A Merry Mix-Up is the 177th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges play three sets of identical triplets, born one year apart. All nine brothers lose track of each other after World War II, unaware that they are all living in the same city. One set (Moe, Larry and Joe) is single, one (Max, Louie and Jack) is married, and the other (Morris, Luke and Jeff) is engaged.

Trouble brews when the engaged set of brothers decided to celebrate at a local nightclub. Before they arrive, the unmarried set show up, followed by the fiancees of their brothers. The ladies start hugging and kissing the unsuspecting brothers. Within minutes, the wives of the married brothers show up, thinking their husbands are cheating on them. Hilarity ensues when the nightclub waiter (Frank Sully) walks in and sees all nine brothers simultaneously.

==Production notes==
The shot featuring all nine brothers standing side by side took careful planning to expose just right, giving the effect of three Moes, Larrys and Joes. To achieve this, each Stooge had to stand behind a specific marker before each shot was taken. For the final exposure, director Jules White suspected that Larry Fine was standing behind the wrong marker when compared to the previous two exposures.
 
Larry knew White was wrong, and went to great lengths to prove it. Luckily, Larry prevailed, and saved the studio from having to reshoot thousands of dollars worth of exposures. 

When everything is sorted out between the girls, Moe suggests they all celebrate.  In the background, a viewer can see one of the doubles that played Joe.  As the girls and the doubles leave the room, a careful viewer can easily spot one of the doubles for Moe due to his slight jumping up and down.  The final "Larry" can be easily spotted due to his unusual smile as he leaves the room.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 