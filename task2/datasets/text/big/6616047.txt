Gang (film)
{{Infobox film
| name = Gang
| image = Gang_(film)_dvd.jpg
| image_size = 
| caption =  Mazhar Khan
| producer = Mazhar Khan Bharat Shah
| writer = 
| narrator = 
| starring = Jackie Shroff Nana Patekar Kumar Gaurav Jaaved Jaffrey Juhi Chawla
| music = Anu Malik R.D. Burman
| cinematography = 
| editing = 
| distributor = 
| released = 14 April 2000
| runtime = 
| country = India
| language = Hindi
| budget = 
| preceded_by = 
| followed_by = 
}} Mazhar Khan. The film began production in the early 1990s and was delayed for many years. The director Mazhar Khan died two years before the films release. The song Chhod Ke Na Jaana is composed by R.D.Burman and rest by Anu Malik.

==Plot==
Four friends, Gangu (Jackie Shroff), Abdul (Nana Patekar), Nihal (Kumar Gaurav) and Gary (Jaaved Jaffrey)- which forms the word G.A.N.G, get together to start their business, but their roots are built on friendship and trust. They succeed in their criminal goals, although Gangu is arrested and sentenced to jail for five years. Before going to jail, he asks them to promise to go straight, to which they all agree.

When Gangu is released, he is pleased to find that Abdul is now driving a taxi, his mother is well looked after, and that Nihal and Gary have also started doing business. It is when Gangu meets his sweetheart, Sanam (Juhi Chawla), and proposes marriage, that he learns that all is not well in their world.

==Soundtrack==

R.D.Burman composed only one track, Chhod Ke Na Jaana, sung by Asha Bhosle, for the movie. But since the movie faced late release and his death, Mazhar Khan roped in Anu Malik for the composition. It can be noted that Anu Malik used some bits of the background score composed by Burman, of the movie, Caravan (1971 film)|Caravan for the song Dil Hai Bechain in the late composers remembrance.

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Music
|-
| 1
| "Meri Payal Bole"
| Anu Malik, Alka Yagnik, Sunidhi Chauhan
| Anu Malik
|-
| 2
| "Ye Karo Ye Nahin"
| Abhijeet Bhattacharya|Abhijeet, Hariharan (singer)|Hariharan, Jolly Mukherjee
| Anu Malik
|-
| 3
| "Dil Hai Bechain"
| Kumar Sanu, Sadhana Sargam
| Anu Malik
|-
| 4
| "Kyon Hum Tum Miley"
| Kumar Sanu, Anu Malik & Alka Yagnik, 
| Anu Malik
|-
| 5
| "Aaj Tu Maang Le"
| Abhijeet Bhattacharya|Abhijeet, Hariharan (singer)|Hariharan, Roop Kumar Rathod &  Jolly Mukherjee
| Anu Malik
|-
| 6
| "Chhod Ke Na Jaana"
| Asha Bhosle
| R.D. Burman
|}

==External links==
*  
*  

 
 
 

 