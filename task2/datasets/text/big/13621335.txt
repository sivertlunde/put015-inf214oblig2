Streets of Gold (film)
{{Infobox film |
  name=Streets of Gold |
  image= Streets of Gold Film Poster.jpg|
  caption= Theatrical release poster| Richard Price Tom Cole |
  starring=Klaus Maria Brandauer Adrian Pasdar Wesley Snipes Ángela Molina Elya Baskin John Mahoney |
  director=Joe Roth |
  producer=Joe Roth Harry J. Ufland |
  music=Jack Nitzsche | cinematography = Arthur Albert |
  distributor=20th Century Fox |
  released=November 14, 1986 |
  runtime=95 min. |
  language=English |
  country=United States |
  movie_series=|
  awards= |
  budget=|
  gross=$2,546,238 (US) 
}} 1986 American dramatic film directed by Joe Roth starring Klaus Maria Brandauer, Wesley Snipes and Adrian Pasdar.

== Plot ==
Alik is an immigrant from the Soviet Union, but is not allowed on the Soviet national team because he is Jewish. One day he meets two young amateur boxers named Roland Jenkins and Timmi Boyle and begins to coach them.

== DVD release ==

20th Century Fox has yet to announce any plans to release the film onto DVD.

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 