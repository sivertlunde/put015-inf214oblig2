Corruption (1933 film)
{{Infobox film
| name           = Corruption
| image          =
| image_size     =
| caption        =
| director       = Charles E. Roberts
| producer       = William Berke (producer)
| writer         = Charles E. Roberts (original story and screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Robert E. Cline Arthur Cohen
| distributor    =
| released       = 19 June 1933
| runtime        = 67 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Corruption is a 1933 American film directed by Charles E. Roberts.

The film is also known as Double Exposure in the United Kingdom.

==Plot summary==
 

==Cast==
*Evalyn Knapp as Ellen Manning
*Preston Foster as Tim Butler
*Charles Delaney as Charlie Jasper
*Tully Marshall as Gorman
*Warner Richmond as Regan
*Huntley Gordon as District Attorney Blake
*Lane Chandler as Assistant District Attorney King
*Natalie Moorhead as Sylvia Gorman
*Mischa Auer as Volkov
*Jason Robards, Sr. as Police Commissioner
*Gwen Lee as Mae
*Sidney Bracey as Dr. Robbins
*Kit Guard as Pat
*Fred Kohler Jr. as Bud Nick Thompson as Tony

==Soundtrack==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 