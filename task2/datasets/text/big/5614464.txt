Milenge Milenge
 
 
{{Infobox film
| name           = Milenge Milenge
| image          = Milenge Milenge.jpg
| caption        = Theatrical release poster
| director       = Satish Kaushik
| producer       = Boney Kapoor Surinder Kapoor
| writer         = Shiraz Ahmed
| screenplay     =
| story          =
| based on       =  
| starring       = Shahid Kapoor Kareena Kapoor
| music          = Songs:  
| cinematography = Sethu Sriram
| editing        = Sanjay Verma
| studio         =
| distributor    = Eros International
| released       =  
| runtime        =
| country        = India
| language       = Hindi
}}

Milenge Milenge (We Will Meet, We Will Meet) is a 2010 Bollywood romantic drama film.  Largely based on the 2001 film Serendipity (film)|Serendipity,  the movie is directed by Satish Kaushik and stars Shahid Kapoor and Kareena Kapoor in their fifth film together after Imtiaz Alis Jab We Met (2007).    Other members from the cast include Satish Shah, Aarti Chhabria, and Delnaaz Paul.

==Plot==
Priya Malhotra (Kareena Kapoor) is an orphan who hopes to have a family of her own and keeps a diary outlining her dreams and the type of man she wants to meet – someone who does not drink, smoke or tell lies. She is skeptical when her friend Honeys (Delnaaz Paul) aunt, a card reader, Sunita Rao (Kirron Kher), predicts that she will go to a foreign land and find the love of her life in seven days. She is pleasantly surprised when she is selected to go to a Youth Festival in Bangkok.

That is where Immy (Shahid Kapoor) enters. Immy is a complete opposite to what Priya wants in a guy. He smokes, drinks, and lies. Due to his bad habits, Immy is being chased by security and runs into Priyas hostel room. Before he leaves, he sees Priya and falls in love with her. He takes her diary and escapes. He then pretends to be the total guy Priya wants to be with, and the two start a relationship. Soon enough, Priya spots her diary in his room, and realises he had stolen her diary and acted to be like her dream man. She breaks up with him, and leaves the country to go Delhi and forget about her past.

After she reaches the airport, Immy also arrives, and explains to her that destiny wants them together. Priya does not believe him and therefore challenges him that if destiny did want them together, they would both find them again in future. In order to prove it, she asks Immy to write his name and phone number on a note and uses the same note to buy a Numerology book, in which she writes her own name and phone number and further she sells it in market at second hand rate.

If she receives the same note again and if Immy finds that book with her name and number on it, then it will prove that they love each other and its destiny that wants them together. Three years later, Immy is engaged to Sofiya (Aarti Chhabria) and Priya is engaged to Jatin. However, a week before the marriage, both of them land up in Delhi again looking for each other.

==Cast==
* Shahid Kapoor as Amit Immy Kapoor
* Kareena Kapoor as Priya Malhotra
* Aarti Chhabria as Sofiya
* Kirron Kher as Sunita Rao
* Sarfaraz Khan as Ashish
* Delnaaz Paul as Honey
* Satish Shah as Trilok Kapoor
* Himani Shivpuri as Mrs. Gandhi
* Satish Kaushik as Rosan Saputra

==Production==
In 2004, the cast began shooting in places like Delhi and Dubai, and later continued to shoot at Pathways World School.  They were also supposed to shoot in Thailand, but Kapoor asked for a delay to attend the premiere of Dil Maange More, thus avoiding the 2004 Indian Ocean tsunami that destroyed the hotel that the cast and crew had been scheduled to stay in.  The movie was expected to hit screens on 23 December 2005, but due to some financial and casting problems, it failed to do so.   Salman Khan was signed on to do an important extended guest appearance for the film but opted out because of differences with the films lead pair (Shahid Kapoor and Kareena Kapoor). 

After the lead pair broke up, sources indicated that they wouldnt come together to dub for the film. However, the director, Satish Kaushik questioned "...why should Shahid and Kareena have any problems dubbing it?"    Producer Boney Kapoor further noted that "...perhaps it was destined that the most romantic film featuring Shahid and Kareena would come after their relationship."  In December 2007, sources indicated that the film was scheduled to release the day after Valentines Day (15 February 2008) but it was again delayed.  On 1 April 2008, the director announced that Shahid Kapoor had begun dubbing for the film whilst Kareena Kapoor would begin after she returned from her overseas trip.  In February 2009, it was reported that both the actors had finally completed dubbing for the movie. 

==Reception==
===Critical reception===
Upon release, Milenge Milenge received mixed to negative reviews from critics. Rajeev Masand of CNN-IBN gave the film a rating of 1.5 out of 5 and described the film as "regressive, senseless, and packed with plot-holes the size of craters".  Taran Adarsh of Bollywood Hungama rated it 2/5 and said, "Milenge Milenge has the charismatic lead pair, who are very popular with the youth, as its USP. But the problem is its dated look. Having taken a long time to reach the theatres, it will have to rely on a solid word of mouth to lure the audiences into cineplexes".  Mayank Shekhar of the Hindustan Times gave it 1.5/5 saying, "Years since, while the films main pair may not be friends anymore, have probably knocked each other off their Facebook pages, trashed old kissing pics, tucked away the sweet SMSes, they can’t quite disown a new release that celebrates them still. Well. Sucks. I guess!".  Gaurav Malani of Indiatimes gave the film 2/5 and stated, "Watch it only if you are just interested to see Kareena Kapoor when the term size zero wasn’t coined. Else Milenge Milenge doesn’t score too much above zero". 

===Box office===
The film opened to a poor opening  and collected Rs. 25000&nbsp; in its theatrical run.  . Bollywood Hungama  It was declared a super flop film at the box office.

==Soundtrack==
{{Infobox album
| Name = Milenge Milenge
| Type = Soundtrack
| Artist = Himesh Reshammiya
| Released =  
| Genre = Film soundtrack
| Length = 47:46
| Label = T-Series
| Producer = Boney Kapoor
| Last album = Radio (2009 film)|Radio  (2009)
| This album = Milenge Milenge (2010)
| Next album = Kajraare  (2010)
}}
{{track listing
| extra_column    = Artist(s)
| title1          = Milenge Milenge
| extra1          = Alka Yagnik and Jayesh Gandhi
| length1         = 04:46
| title2          = Kuch To Bakee Hai
| extra2          = Himesh Reshammiya
| length2         = 03:55
| title3          = Ishq Ki Galee
| extra3          = Rahat Fateh Ali Khan and Jayesh Gandhi
| length3         = 05:15
| title4          = Tum Chain Ho
| extra4          = Sonu Nigam, Alka Yagnik and Suzanne DMello
| length4         = 05:11
| title5          = Milenge Milenge
| extra5          = Himesh Reshammiya and Shreya Ghoshal
| length5         = 05:07
| title6          = Yeh Hare Kaanch Ki Choodiyan
| extra6          = Alka Yagnik
| length6         = 04:43
| title7          = Kuch To Bakee Hai (Bright Mix)
| extra7          = Himesh Reshammiya
| length7         = 04:00
| title8          = Ishq Ki Galee (Dance Mix)
| extra8          = Rahat Fateh Ali Khan and Jayesh Gandhi
| length8         = 03:40
| title9         = Kuch To Bakee Hai (Dark Mix)
| extra9         = Himesh Reshammiya
| length9        = 03:59
| title10         = Tum Chain Ho (Unplugged)
| extra10         = Vinit Singh
| length10        = 04:12
}}

==References==
 

== External links ==
*  
*  

 
 
 
 