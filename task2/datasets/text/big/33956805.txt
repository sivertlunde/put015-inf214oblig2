Kazhukan
{{Infobox film 
| name           = Kazhukan
| image          =
| caption        =
| director       = AB Raj
| producer       =
| writer         = K. Balachander AB Raj (dialogues)
| screenplay     = AB Raj Shubha Sukumaran Jagathy Sreekumar
| music          = M. K. Arjunan
| cinematography = NA Thara
| editing        = K Sankunni
| studio         = Kalaranjini Films
| distributor    = Kalaranjini Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by AB Raj. The film stars Jayan, Shubha (actress)|Shubha, Sukumaran and Jagathy Sreekumar in lead roles. The film had musical score by M. K. Arjunan.    Sukumaran plays the antagonist. The film was a remake of the 1978 Kannada/Tamil bilingual Thappida Thala / Thappu Thalangal directed by K. Balachander.

==Cast==
 
*Jayan as Velu Shubha as Malathi
*Sukumaran as Gopi
*Jagathy Sreekumar
*Sreelatha Namboothiri
*Paul Vengola
*Bahadoor
*Ceylon Manohar
*Kundara Johny
*Philomina Priya
*Varalakshmi
*Vazhoor Rajan
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chandanakkulirchoodivarum || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Enthinee jeevithavesham || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Thaalam thettiya raagam || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  
 
 
 
 
 
 