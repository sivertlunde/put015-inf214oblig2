Tarzan and the Lost Safari
{{Infobox film 
| name           = Tarzan and the Lost Safari 
| image          = Tarzan and the Lost Safari poster.jpg
| caption        = Tarzan and the Lost Safari movie poster Bruce Humberstone		
| producer       = John Croydon
| writer         = Montgomery Pittman Lillie Hayward
| based on       =  
| starring       = Gordon Scott Robert Beatty Yolande Donlan Betta St. John
| music          = Clifton Parker
| cinematography = C.M. Pennington-Richards
| editing        = Bill Lewthwaite	 MGM 
| released       =  
| runtime        = 86 min.
| country        = United States
| language       = English 
| budget         = $2,315,000  . 
}}
 Jane does not appear in this motion picture.

==Plot== Opar under Chief Ogonooro (Orlando Martins). The Oparians desire the strangers as sacrifices for their lion god. She is recovered by Tarzan and hunter Tusker Hawkins (Robert Beatty), whose advances Diana rebuffs. Secretly, however, Hawkins is in league with the Oparians, and plans to sell the castaways to the natives for a fortune in ivory. Tarzan, rightly suspecting Hawkins untrustworthiness, exposes his treachery. Now openly in league with the natives, the hunter helps them take the white party captive in Tarzans absence. The ape man returns to save them before the sacrifice can take place, aided by his chimpanzee ally Cheeta, who sets fire to the native village. He then leads them to the safety of a nearby settlement. Hawkins meets his fate at the hands of the Oparians, to whom Tarzan has signaled his double-dealing by a creative use of jungle drums.

==Cast==
* Gordon Scott as Tarzan  
* Robert Beatty as Tusker Hawkins  
* Yolande Donlan as Gamage Dean  
* Betta St. John as Diana Penrod  
* Wilfrid Hyde-White as Doodles Fletcher (as Wilfrid Hyde White)  
* George Coulouris as Carl Kraski  
* Peter Arne as Dick Penrod  
* Orlando Martins as Oparian Chieftain Ogonoore

==Notes==
The film contains more echoes of the original Burroughs novels than usual in a Tarzan movie, including the ape mans allusions to his origin (which follows Burroughs version), and the use of Opar, though reducing the romantic lost city described by Burroughs to a generic native village. Tarzan, while retaining the customary film characterization of an inarticulate simpleton, displays considerable shrewdness and resource, foreshadowing the restoration of Burroughs original concept of an intelligent, multitalented ape man in later movies.
==Reception==
According to MGM records the film earned $915,000 in the US and Canada and $1.4 million elsewhere, resulting in a profit of $432,000. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 