Radha Kalyanam
 
{{Infobox film
| name           = Radha Kalyanam
| image          =
| image_size     =
| caption        = Bapu
| producer       =
| writer         = Mullapudi Venkata Ramana K. Bhagyaraj
| narrator       = Chandra Mohan Kanta Rao
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| studio         = Saradhi Studios
| distributor    =
| released       = 7 November 1981
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Radha Kalyanam  is a 1981 Telugu drama film written by Mullapudi Venkata Ramana and directed by Bapu (artist)|Bapu. It is considered to be one of the best Telugu movies by critics.  It was a remake of a Tamil movie Antha Ezhu Naatkal (Those 7 Days) directed by K. Bhagyaraj. 

It was remade in Hindi as Woh Saat Din in 1983 starring Anil Kapoor, Padmini Kolhapure and Naseeruddin Shah.

==Plot==
The movie is a triangle love story between the lover, the heroine and her husband. Radha (Raadhika) is a typical girl from a lower-middle-class family. She falls deep in love with the new tenant upstairs Palghat Madhavan (Chandramohan). Madhavan, aspires to be a great music director, but struggles to get by. He is attracted to Radha.

Dr. Anand (Sarath Babu), a widower, is forced to marry Radha to fulfill the wish of his dying mother. He fully intends to keep his promise of returning Radha to her rightful owner and partner, Madhavan, after hearing to Radhas reason behind attempting suicide on their first night. Events lead to conflict in her mind between the husband and lover. Whom should Radha choose and why?

==Cast== Chandra Mohan ...  Palghat Madhavan
* Raadhika ...  Radha
* Sarath Babu ...  Dr. Anand
* Tadepalli Lakshmi Kanta Rao
* Raavi Kondala Rao
* Pushpalata
* Sakshi Ranga Rao
* Radhabai

==Soundtrack==
* "Bangaru Bala Pichuka"
* "Chetiki Gajulla" SP Balasubramanyam, Jyothirmayi
* "Kalanaina Kshanamaina"
* "Palaghat Madhavan"
* "Yemmogudo"

==References==
 

==External links==
*  

 
 
 
 
 
 


 