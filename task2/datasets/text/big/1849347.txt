The Truth About Jane and Sam
 
 
{{Infobox film
| name           = The Truth About Jane and Sam 真心話
| image          = Poster jane.jpg
| image_size     = 200px
| caption        = Theatrical poster for The Truth About Jane and Sam.
| director       = Derek Yee
| producer       = Catherine Hun
| writer         = Derek Yee
| narrator       =  Wei Wei James Lye
| music          = Ken Lim Zhang Renshu Li Min Dawn Lim
| cinematography = Feng-Zhen Feng Kwok-Man Keung   
| editing        = Chi-Leung Kwong
| distributor    = 
| released       =  
| runtime        = 100 min.
| country        = Hong Kong Singapore English Standard Mandarin  Cantonese
| budget         = United States Dollar|US$1 million
}}
The Truth About Jane and Sam ( : 真心话) is a 1999 Hong Kong film co-produced by Hong Kongs Film Unlimited and Singapores Raintree Pictures.  Directed by Hong Kong director Derek Yee, the movie stars Singapore actress Fann Wong and Taiwanese male singer Peter Ho.

==Plot==
Sam, a fresh graduate from Singapore, works as a journalist in Hong Kong to gain wider exposure in life. He chances upon Jane, an intriguing mainland girl who lives a wayward, depraved life on the streets of Hong Kong.  Because of her previous experiences in life, she is highly distrustful of men and spends her days smoking, taking drugs and booze. Sam, from a rich Singapore family, finds his social view broadened as he spends time with Jane in an effort to capture a good feature story. What started out as fascination over her for a cover story develops into a heart-warming love story. The two fall in love but their love is tested under the harsh light of societal comparisons.

== Cast ==
* Fann Wong as Jane
* Peter Ho as Sam
* Cheng Pei-pei as Sams mother
* Chin Kar-lok as Janes brother
* Joe Cheung as Sams father Wei Wei as Janes grandmother
* James Lye as The man in the pet shop
* Deborah Sim as The ice-cream seller

== Box office ==
* The film grossed SGD$78,261 in Singapore on the sneak preview weekend, making it the top film of the weekend, a rarity for a Hong Kong film in Singapore.  (Hong Kong films have suffered declining cinema attendance rates since the mid-90s.)  The media widely attributed it to the presence of Fann Wong, a leading Singaporean actress, in the lead role.
* At the end of its five-week run in Singapore, it had grossed SGD$1,057,318 - making it the top Hong Kong film of the year in Singapore.
* In Hong Kong, the film grossed over HKD$5 million.

==See also==
* List of Hong Kong films

== Nominations ==
* 19th Hong Kong Film Awards Best New Performer Award (Fann Wong)

== External links ==
* 

 

 
 
 
 