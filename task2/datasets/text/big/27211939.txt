Blood Out
 
{{Infobox film
| name           = Blood Out
| image          = Blood Out.jpg
| alt            =
| caption        =
| director       = Jason Hewitt
| producer       = Michael Arata Jason Hewitt Carsten H.W. Lorenz
| writer         = John A. OConnell Jason Hewitt
| starring       = Luke Goss Vinnie Jones Val Kilmer 50 Cent AnnaLynne McCord Tamer Hassan
| music          = Jermaine Stegall
| cinematography = Christian Herrera
| editing        = Ezra Gould Matt Hathcox Donald Ray Washington
| studio         = Films In Motion Voodoo Production Lionsgate Grindstone Media
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Blood Out is a 2011 American action thriller film released direct-to-video. The film is written by Jason Hewitt and John A. OConnell, starring Val Kilmer and Luke Goss,  and is Hewitts directorial debut. 

== Plot ==
Small town Cop Michael Savion (Luke Goss) gets stone-walled by big city detectives led by Hardwick (50 Cent) when he asks them to investigate his younger brothers death. After his brothers death he swears vengeance in a flashback where he fires his gun in the rain over his dead body. His brother David (Ryan Donowho), a gangster, was murdered by members of his own gang for having wanted to quit the gang lifestyle in order to marry Gloria (a gang members sister), and for being suspected of ratting out his gangs activities... Savion takes off his badge and goes vigilante to seek justice for the death of his brother, and crosses paths with Arturo (Val Kilmer), the leader of an international ring involved in human trafficking.

==Cast==
* Luke Goss as Michael
* Tamer Hassan as Elias
* AnnaLynne McCord as Anya
* Vinnie Jones as Zed
* 50 Cent as Hardwick
* Val Kilmer as  Arturo
* Ed Quinn as Anthony
* Ryan Donowho as David
* Ambyr Childers as Medic
* Michael Arata as Detective
* Jesse Pruett as Methhead
* Bobby Lashley as Hector
* Stephanie Honore as Gloria

==Production==
Filming began May 10, 2010  on locations in Baton Rouge, Louisiana. 

Vinnie Jones appears in the film. Despite reports of an earlier conflict with actor Tamer Hassan, both were quite amicable on and off the set. 

Former WWE Wrestler Bobby Lashley stars in the film.

As an unusual method of "clearing his head" on set after "flubbing" an occasional line of dialog, Val Kilmer would briefly break into dog-like barks and howls. 
 Lionsgate U.K. acquired distribution right for the UK,    and has announced tentative plans for an end-of-year domestic release of the film. 

==Soundtrack==
The film was scored by Jermaine Stegall, and features the single "Hells Gates" by Colin C. Allrich under his Slighter alias. Also T.Lee from DesFly Entertainment song called "Im The Shit.

==Release==
The film was released on DVD and Blu-ray in America on April 26, 2011. 

== References ==
 

   

   

     

   

   

   
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 