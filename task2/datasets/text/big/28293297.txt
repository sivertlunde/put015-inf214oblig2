The Amazing Mrs. Holliday
{{Infobox film
| name           = The Amazing Mrs. Holliday
| image          = Poster of The Amazing Mrs. Holliday.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Bruce Manning
| producer       = Bruce Manning
| writer         = {{Plainlist|
* Boris Ingster (adaptation)
* Leo Townsend (adaptation)
}}
| screenplay     = {{Plainlist|
* Frank Ryan
* Hans Jacoby
}}
| story          = Sonya Levien
| starring       = {{Plainlist|
* Deanna Durbin
* Edmond OBrien
* Barry Fitzgerald
}}
| music          = {{Plainlist|
* Hans J. Salter
* Frank Skinner
}}
| cinematography = Elwood Bredell
| editing        = Ted J. Kent
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Amazing Mrs. Holliday is a 1943 American comedy drama film produced and directed by Bruce Manning and starring Deanna Durbin, Edmond OBrien, and Barry Fitzgerald.       Based on a story by Sonya Levien, the film is about a young idealistic missionary who smuggles a group of Chinese war orphans into the United States posing as the wife of a wealthy commodore who went missing after their ship was torpedoed and sunk. After safely sequestering the orphans in the commodores family mansion, her plans start to unravel when she falls in love with commodores grandson and the commodore himself turns up alive and well. Originally intended as Durbins dramatic debut, Universal insisted on adding songs. The original director of the film was Jean Renoir, and though most of his completed footage was retained, final directorial credit was given to Bruce Manning, the films producer.  The Amazing Mrs. Holliday received an Academy Award nomination for Best Score.    

==Plot==
A young idealistic schoolteacher named Ruth Kirke (Deanna Durbin) is transporting a group of war orphans from South China to Calcutta when their steamship Tollare is torpedoed and sunk in the Pacific. Along with sailor Timothy Blake (Barry Fitzgerald), they are the only passengers to survive the enemy attack. They are picked up by the steamship Westonia and taken to San Francisco, where immigration officials inform Ruth that the orphans will be held until a $500 bond is posted for each child.
 Harry Davenport), the wealthy owner of their sunken cargo ship, who perished during the torpedo attack. When they appeal for financial assistance for the orphans, the commodores family refuses. Desperate to help the children, Timothy tells the commodores family that Ruth and the commodore were married aboard the Tollare before it was attacked. With the childrens future at stake, Ruth reluctantly goes along with the deception.

Ruth, Timothy, and the eight orphans move into the Holliday mansion, where they soon meet the commodores grandson, Thomas Spencer Holliday III (Edmond OBrien). When a sceptical Tom questions Ruth about how she became his grandmother, Ruth explains that her Christian mission was destroyed in a Japanese bombing raid, and that she was sent south with eight European children, entrusted with their safety. Along the way, they encountered a dying Chinese woman, and Ruth agreed to care for her child as well. Moved by her personal story and her beautiful singing voice, Tom is soon smitten with the young woman.

After learning that she, as the commodores "widow", will inherit his vast shipping fortune, and faced with pressure from the family and press, Ruth gathers the children and attempts to sneak away during the night, but Tom discovers them. Wanting to end the deception, Ruth confesses to Tom that she smuggled the orphans aboard the commodores ship, believing it was headed to Calcutta. During the voyage, they were discovered by the commodore who promised to help Ruth get the orphans into the United States, even if it meant adopting them. After their ship was torpedoed, Ruth and Timothy put the children into a lifeboat—losing only one child, a boy named Pepe—and were later picked up by another steamship. Angered by the deception, Tom insists that Ruth stay and continue the charade until the publicity about her "marriage" dies down, after which he will care for the orphans at the mansion once she leaves.

In the coming days, as she watches Tom caring for the children, Ruth falls in love with him. When the childrens immigration papers finally arrive, Ruth prepares to leave as promised, despite her feelings for Tom and the children. Later at the station, while Ruth waits for her train to Philadelphia, Tom arrives at Timothys request, unaware that Ruth is preparing to leave. Timothy lies to Tom, telling him that the stranger sitting next to her is her fiancée—intending to make Tom jealous and prevent her from leaving. The ploy works, as Tom congratulates the stranger on his upcoming marriage. In the ensuing commotion, Tom escorts Ruth away from the train station and they return to the mansion.

Sometime later, at a China relief ball held at the mansion, Ruth sings an aria, Puccinis Vissi darte, to the assembled guests while Tom looks on with loving admiration. By now they have expressed their love for each other. Suddenly, the commodore steps forward, having been rescued along with Pepe following the torpedo attack. Knowing what Ruth has done for the children, he plays along with the deception, telling the guests how happy he is that fate spared his "dear wife". Afterwards the commodore tells Ruth that hell marry her for real and raise the orphans as his own children, unaware she is in love with Tom. The commodores plans change when he learns that Ruth and Tom are in love. Addressing the guests, the commodore confesses that he and Ruth were never really married, but that in three days she is going to become Mrs. Holliday—Mrs. Tom Holliday—the wife of his grandson.

==Cast==
 
 
* Deanna Durbin as Ruth Kirke Holliday
* Edmond OBrien as Tom Holliday
* Barry Fitzgerald as Timothy Blake
* Arthur Treacher as Henderson Harry Davenport as Commodore Thomas Spencer Holliday Grant Mitchell as Edgar Holliday
* Frieda Inescort as Karen Holliday
* Elisabeth Risdon as Louise Holliday
* Jonathan Hale as Ferguson
* Esther Dale as Lucy
* Gus Schilling as Jeff Adams
* John F. Hamilton as Dr. Kirke
 
* Christopher Severn as Orphan
* Vido Rich as Orphan
* Mila Rich as Child
* Teddy Infuhr as Orphan
* Linda Bieber as Child
* Diane DuBois as Child
* Charles Trowbridge as Immigration Officer
* Irving Bacon as Ticket Agent
* Philip Ahn as Major Ching
* Vangie Beilby as Railroad Passenger (uncredited)
* George Chandler as Butler (uncredited)
* Yvonne Severn as Child (uncredited)    
 

==Production==

===Directorial change===
According to a story in the Hollywood Reporter published on August 7, 1942, Jean Renoir was being replaced as director after forty-seven days of shooting. According to the story, the French director was fired because of his slow filming pace—he was reportedly ten weeks behind schedule. According to Renoir, he left the film because of recurring pain caused by an old World War I leg injury.   

===Soundtrack===
* "Mong Djang Nu (A Chinese Lullaby)" (Traditional, English Translation by Rosalyda Chang)
* "The Old Refrain" (Fritz Kreisler, Alice Mattullath) by Deanna Durbin
* "Mighty Lak a Rose" (Ethelbert Nevin, Frank L. Stanton) by Deanna Durbin
* "Vissi darte" from Tosca (Giacomo Puccini, Giuseppe Giacosa, Luigi Illica) by Deanna Durbin
* "Rock-a-bye Baby" (Effie I. Canning, Chinese Translation by Rosalyda Chang)   

==Reception==
In his review on Allmovie, Craig Butler wrote that the film is "undeniably heartwarming and definitely has charm, even if it veers over into sentimentality."    According to Butler, the film could have turned out much better had the original director, Jean Renoir, seen the project through to its conclusion.
 
Butler praises Barry Fitzgerald for his "fine comic relief" and Edmond OBrien who "handles the romantic element with aplomb."  Butler concludes that Durbin is in good voice and her "pleasant personality, sweet looks, and engaging way with a line build up considerable good will among viewers." 

==Awards and nominations==
* 1944 Academy Award nomination for Best Music, Scoring of a Dramatic or Comedy Picture (Hans J. Salter, Frank Skinner)  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 