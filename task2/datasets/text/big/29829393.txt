10 Years (film)
 
{{Infobox film
| name           = 10 Years
| image          = Ten Years poster.jpg
| caption        = Theatrical release poster Jamie Linden
| producer       = {{Plainlist|
*Marty Bowen
* Reid Carolin
*Wyck Godfrey
* Channing Tatum
}}
| writer         = Jamie Linden
| starring       = {{Plainlist|
* Lynn Collins
* Channing Tatum
* Rosario Dawson
* Brian Geraghty
* Ari Graynor
* Oscar Isaac
* Ron Livingston
* Justin Long
* Anthony Mackie
* Kate Mara
* Max Minghella Jenna Dewan-Tatum
*Kelly Noonan
* Aubrey Plaza
* Scott Porter
* Chris Pratt
* Aaron Yoo
 
}}
| music          = Chad Fischer
| cinematography = Steve Fierberg
| editing        = Jake Pushinsky
| released       =  
| studio          = {{Plainlist|
*Boss Media
* Temple Hill Entertainment Iron Horse
}}
| distributor    = Anchor Bay Films
| runtime        = 100 minutes
| country        = United States
| language       = English
| gross          = $203,373
}}
10 Years is a 2011 American   including Channing Tatum, Justin Long, Kate Mara, Chris Pratt, Scott Porter, Brian Geraghty, Anthony Mackie, Rosario Dawson, Oscar Isaac, Lynn Collins, Max Minghella, Kelly Noonan, Juliet Lopez and Jenna Dewan. The film was released on September 14, 2012, in select theaters.

==Plot==
Jake (Channing Tatum), and his girlfriend Jess (Jenna Dewan) arrive at his high school
friends house owned by married couple, Cully (Chris Pratt) and Sam (Ari Graynor). There, some of
Jakes friends  start to arrive including: best buddies Marty (Justin Long) and AJ (Max Minghella),
musician Reeves (Oscar Isaac) and Scott (Scott Porter) together with his wife, Suki (Eiko Nijo).
The guys then start driving to their high school reunion venue. Other friends from their
high school arrive including: Garrity (Brian Geraghty) along with his wife Olivia (Aubrey Plaza)
and Garritys best friend Andre (Anthony Mackie). Meanwhile, a reclusive woman Elise (Kate Mara)
arrives at the reunion alone. She tries to welcome the guest only to be ignored by the party planners, 
Anna (Lynn Collins) and Julie (Kelly Noonan).

The night proceeds as each couple is faced with both their past selves and present.  Some have changed while others stayed the same; Cully attempts to apologize for his high school behavior towards the nerds he used to know but he only reverts to his old self with each drink he takes, embarrassing his wife and all others present.

Marty and AJ, now successful in their respective careers, attempt to impress and woo the prom queen Anna, the girl who has everything.  After she turns them down they attempt childish revenge only to learn that none of them has a perfect life.  Instead all three must learn to reconnect and forgive themselves for past faults.

Elise connects with Reeves throughout the night, revealing feelings he has had for her that ended up as inspiration for his popular song Never Had, and she must decide if she is going to run away again.

Jake, meanwhile, has been deciding the right time to propose to Jess, but he must come to terms with feelings he had for his high school sweetheart, Mary (Rosario Dawson), and the way their relationship ended.

The film ends with Jake and Jess at a diner with friends, happy to relive past moments and looking forward to tomorrow.

== Cast ==
* Channing Tatum  as Jake Bills
* Jenna Dewan as Jess
* Justin Long  as Marty Burn
* Kate Mara  as Elise
* Chris Pratt  as Cully
* Scott Porter as Scott
* Rosario Dawson as Mary
* Lynn Collins as Anna
* Oscar Isaac as Reeves
* Ari Graynor as Sam
* Brian Geraghty as Garrity Liamsworth
* Max Minghella as AJ
* Anthony Mackie as Andre Irine
* Nick Zano as Nick Vanillo
* Juliet Lopez as Becky
* Kelly Noonan as Julie
* Aubrey Plaza as Olivia
* Eiko Nijo as Suki
* Ron Livingston as Paul
* Aaron Yoo as Peter Jung
* Frantz Durand as Frantz
 
 

==Production==
A short film titled Ten Year was produced in 2011 by Channing Tatum to attract financing for the feature film. 
 Jamie Linden. The film was shot in New York, California and New Mexico starting in January 2011. 

== Release ==
The film had a red carpet premiere at the Toronto International Film Festival. {{Citation  | title = Ten Year TIFF premiere photos  | url = http://www.digitalhit.com/galleries/40/595/  | year = 2011  | author = Lambert, Christine
 | journal = DigitalHit.com  | accessdate = 2012-04-05 }} 

==References==
 

== External links ==
*  
*  
*   at hitfilmindirizle.net

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 