Immortal (2004 film)
 

{{Infobox film
| name           = Immortal
| image          = Immortel (ad vitam) movie poster.jpeg
| caption        = 
| director       = Enki Bilal
| producer       = Charles Gassot
| writer         = Enki Bilal (comic books, scenario & adaptation and dialogue)   Serge Lehman (script) 
| starring       = Linda Hardy Thomas Kretschmann Charlotte Rampling
| music          = Sigur Rós Goran Vejvoda
| cinematography = Pascal Gennesseaux
| editing        = Véronique Parnet
| studio          = Duran Entertainment
| distributor    = First Look Pictures (U.S.) Ciby 2000 (Australia)
| released       =   
| runtime        = 102 minutes
| country        = France
| language       = French English 
| budget         = €22,1 million 
| gross          = $6,251,029 
|}}
Immortal ( , directed by Enki Bilal and loosely based upon his comic book La Foire aux immortels (The Carnival of Immortals). It was one of the first major films (along with Casshern (film)|Casshern and Sky Captain and the World of Tomorrow) to be shot entirely on a "digital backlot", blending live actors with computer generated surroundings. The French video game studio Quantic Dream helped produce much of the cinematics.

== Plot ==
 , left, and Linda Hardy in a scene from Immortel (Ad Vitam).]]
The film takes place in New York City in the year 2095 where genetically altered humans live side by side with unaltered men and women, and where Central Park has been mysteriously encased in an "intrusion zone" where people who attempt to enter are instantly killed. A strange pyramid has appeared over the city; inside, the gods of ancient Egypt have judged Horus, one of their fellow gods, to cease his immortality.

In the city below, Jill, a young woman with blue hair is arrested. Not completely human, her tissues appear to be no more than a few months old according to an examining physician, although her physical form is already that of an adult. She does also possess a number of secret powers, including one that enables her to procreate with gods, though she knows nothing of this. Horus is given a limited time to interact with the humans of New York and procreate. During his search for a host body, Horus encounters Nikopol, a rebel condemned to 30 years of hibernation who escapes his prison, due to a mechanical accident, one year early.

Horus has been unsuccessful in attempting to take over the bodies of other humans; due to an incompatibility with the genetic alterations humans have undergone, the host bodies self-destruct while attempting to accommodate a god. Nikopols body is acceptable as it has been frozen in prison/storage and not undergone the genetic changes causing the rejections. Horus takes partial control of Nikopols body and starts looking for a woman he can mate with to provide him a son before his death sentence is carried out. When Horus/Nikopol discovers Jill, they become entangled in a web of murder and intrigue.

== Cast ==
* Linda Hardy as Jill Bioskop
* Thomas Kretschmann as Alcide Nikopol
* Charlotte Rampling as Elma Turner
* Frédéric Pierrot as John
* Thomas M. Pollard as Horus
* Yann Collette as Froebe
* Derrick Brenner as Jonas

Also starring: Olivier Achard, Corinne Jaber, Barbara Scaff, Joe Sheridan, Jacquelyn Toman, Jean-Louis Trintignant.

== Trivia == Barbara Weber-Scaff), except for one piece of dialogue delivered in her French.

== References ==
 
 

== External links ==
 
*  
*  
*  
 
 

 

 
 
 
 
 
 
 
 
 