My Wife's Family (1941 film)
 

{{Infobox film
| name           = My Wifes Family
| image          =
| caption        =
| director       = Walter C. Mycroft
| producer       = Walter C. Mycroft
| writer         = Clifford Grey  Norman Lee
| based on       = the play by Fred Duprez (story by Harry B. Linton and Hal Stephens)
| starring       = Charles Clapham  John Warwick  David Thomlinson Patricia Roc Charles Williams
| cinematography = Walter J. Harvey 
| editing        = 
| studio         = Associated British Picture Corporation
| distributor    = Pathé Pictures International (UK)
| released       = July 1941	
| runtime        = 82 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} British domestic comedy film directed by Walter C. Mycroft and starring Charles Clapham, John Warwick, David Tomlinson and Patricia Roc. 
 remake in 1956. http://www.allmovie.com/movie/my-wifes-family-v103441 

== Plot ==
A farce concerning the attempts of a naval officer to avoid a visit from his wifes overbearing mother-in-law, and cope with a former girlfriend at the same time. 

==Cast==
*Doc Knott -	Charles Clapham
*Jack Gay -	John Warwick
*Peggy, his Wife -	Patricia Roc
*Noah Bagshott -	Wylie Watson
*Sally the Maid -	Peggy Bryan
*Rosa Latour -	Chili Bouchier
*Mrs Bagshott -	Margaret Scudamore
*Policeman -	Leslie Fuller
*Second Maid -	Davina Craig
*Irma -	Joan Greenwood
*Willie -	David Tomlinson

== Critical reception ==
*TV Guide wrote, "every old joke you never wanted to hear repeated is packed into this dusty, but still mildly amusing comedy." 
*Allmovie gave the film two out of five stars, and referred to the film as an "old-joke-filled farce." 

== References ==
 

== External links ==
*  

 
 
 
 
 


 
 