My Name Is Anna Magnani
{{Infobox film
| name           = My Name Is Anna Magnani
| image          = 
| caption        =
| director       = Chris Vermorcken
| producer       = Jacqueline Pierreux
| screenplay     = Chris Vermorcken
| based on       = 
| starring       = 
| music          = Willy De Maesschaelck
| cinematography = {{plainlist|
* Rufus Bohez
* Romano Scavolini
* Gianfranco Transunto
}}
| editing        = {{plainlist|
* Eva Houdova
* Yves Van Herstraeten 
}}
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = {{plainlist|
* Belgium
* Italy
}}
| language       = {{plainlist|
* French
* Italian
}}
| budget         = 
| gross          = 
}}
My Name Is Anna Magnani ( ) is a 1980 documentary film written and directed by Chris Vermorcken. It is about the life and career of Italian actress Anna Magnani. The film features appearances and interviews by Federico Fellini, Leonor Fini, Claude Autant-Lara, Giulietta Masina, Marcello Mastroianni, and Franco Zeffirelli, among others. 

My Name Is Anna Magnani received the André Cavens Award for Best Film given by the Belgian Film Critics Association (UCC). 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 
 
 