Looks and Smiles
{{Infobox film
| name           = Looks and Smiles
| image          =
| image size     =
| caption        =
| director       = Ken Loach
| producer       = Raymond Day Irving Teitelbaum
| writer         = Barry Hines
| narrator       = Graham Green
| music          =
| cinematography = Chris Menges
| editing        = Stephen Singleton
| distributor    =
| released       = 12 September 1981
| runtime        = 104 minutes
| country        = United Kingdom
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} 1981 cinema British drama film directed by Ken Loach. It is based on the novel of the same name, written by Barry Hines. The film was entered into the 1981 Cannes Film Festival, where Loach won the Young Cinema Award.   

==Plot==
A disadvantaged young man tries to get by in Margaret Thatchers England.

==Cast== Graham Green - Michael Mick Walsh
* Carolyn Nicholson - Karen Lodge
* Tony Pitts - Alan Wright
* Roy Haywood - Phil
* Phil Askham - Mr. Walsh
* Pam Darrell - Mrs. Walsh
* Tracey Goodlad - Julie
* Patti Nicholls - Mrs. Wright
* Cilla Mason - Mrs. Lodge
* Les Hickin - George Arthur Davies - Eric Lodge Deirdre Costello - Jenny (as Deidre Costello)
* Jackie Shinn - Gatekeeper
* Christine Francis - Careers Officer Rita May - Receptionist

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 
 
 
 

 
 