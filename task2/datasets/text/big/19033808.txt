Pan Tadeusz (film)
 
{{Infobox film
| name           = Pan Tadeusz
| image          = PanTadeusz1999.jpg
| caption        = Promotional movie poster for the film
| director       = Andrzej Wajda
| producer       = Lew Rywin
| writer         = Andrzej Wajda Jan Nowina-Zarzycki
| based on   =  
| starring       = Bogusław Linda Alicja Bachleda Grażyna Szapolowska Andrzej Seweryn Michał Żebrowski Marek Kondrat
| music          = Wojciech Kilar
| cinematography = Paweł Edelman
| editing        = Wanda Zeman Anita Wandzel
| distributor    = Heritage Films Canal+Polska Les Films du Losange
| released       =  
| runtime        = 147 minutes
| country        = Poland France
| language       = Polish
| budget         = $5 million (estimated) 12,5 million zloty
| preceded_by    =
| followed_by    =
}} eponymous epic poem by Polish poet, writer and philosopher Adam Mickiewicz (1798–1855).  As in the poem, conflict between the Soplica and Horeszko families serves as a backdrop for discussion of issues of Polish national unity and the struggle for independence.

==Historical background== noble families who live in the Russian-controlled part of Poland: the Horeszko family, who ardently favor Polish independence, and the Soplica family, who support Russia.

==Synopsis== invasion of Russia, the prospect of which heartens Poles yearning for liberation. But more immediately, the characters in Pan Tadeusz are feuding among themselves.

At odds are two families: the Soplicas and the Horeszkos. Their differences arise from a bloody night when the dashing Jacek Soplica (who was earlier rejected as a suitor for the old Count Horeszkos daughter), takes advantage of a Russian assault on the Counts castle to kill him. At that moment, the Counts faithful warden, Gervazy, vows vengeance for his masters death. Gervazy will not forgive and forget that in 1792, the last household lord of the Horeszkos was killed by Jacek Soplica and as a result, the latter was rewarded with the formers castle by the Russian colonizers.

20 years later, matters remain unresolved. Judge Soplica (Andrzej Seweryn), Jacek Soplicas brother, (who now lives in the castle of Count Horeszko), is locked in a lawsuit over the castle. A relative of the old murdered Count, young Count Horeszko (Marek Kondrat) has just arrived on the scene, as has 20-year-old Tadeusz Soplica (Michal Zebrowski), the Judges nephew. He is soon smitten with the innocent Zosia (Alicja Bachleda), the teenage ward of his manipulative aunt, Telimena (Grazyna Szapolowska).

Preaching insurrection among the people is Priest Robak (Boguslaw Linda), who carries more than a few secrets under his cowl. 
Robak informs the Poles who are living in Lithuania that Napoleon is marching against the Russians and will be crossing the nearby Niemen River. Naturally, the Poles get intensely worked up over this news as they abhor their Russian overlords.

In the meantime, aunt Telimena, who is in charge of raising 14-year old Zosia, begins a relationship with Tadeusz. This relationship does not please the rest of the family who expect Tadeusz to marry Zosia.

More reports arrive of the approach of Napoleons army. It is said that Polish horsemen are coming with the French and will cross the Niemen. At this time Tadeusz finds out that his father Jacek is still alive and that it was he who sent Priest Robak to his uncle to secure the marriage of Tadeusz and Zosia. Through this marriage, Jacek wishes to make amends for his past sins by restoring the land back to the Count. However, aunt Telimena (who is in love with Tadeusz herself) secretly wishes for Zosia to marry the wealthy Count.

When the Count attends a banquet given by the Soplicas family, Gervazy (the old Counts faithful warden) wreaks havoc by bringing up the old family dispute (namely Jacek Soplica killing old Count Horeszko). The Count and Tadeusz agree to settle their dispute with a duel. Meanwhile, it is revealed to the judge that Father Robak is actually Jacek Soplica. To take vengeance on the Soplica family, the Count and Gervazy head to the village of Dobrzyn to recruit some of the gentry to help destroy them. Vengeance is combined with the goal of starting an insurrection against the Russians. The recruited gentry along with the Count put the Soplicas under house arrest while Gervazy and his forces settle in the castle and make it the headquarters of the Count. The Russian soldiers intervene and capture all the rebels and make them prisoner. Nonetheless, the Soplicas supply weapons and free the rebels, which ultimately allows both Poles and Lithuanians to come together to fight the Russians. In the struggle, Jacek Soplica personally saves the lives of both the Count and Gervazy, for which the two men forgive Jacek Soplica for his past sins. Climactically, the Poles and Lithuanians win the battle, but many will have to leave their homes to avoid the wrath of the Russians.

As news is received that Napoleon has declared war on Russia, the Count and Tadeusz, forgetting their promised duel, head off to join the French troops marching against the Russians. As the story of Pan Tadeusz approaches the end, Count Horeszko and Tadeusz Soplica return as soldier heroes and both families (Soplicas and Horeszkos) celebrate and rejoice in peace as Tadeusz is betrothed to Zosia. 

The film ends, as it began, with many of the protagonists, now emigres in Paris, listening to Adam Mickiewicz as he reads from his poem about the homeland to which they cannot return.

==Cast==
*Boguslaw Linda (Jacek Soplica   alias Priest Robak, Bernardyn, father of Tadeusz Soplica, brother of Judge Soplica)
*Michal Zebrowski (Tadeusz (Thaddeus) Soplica  , son of Jacek Soplica, 20 year old nephew of Judge Soplica, in love with Zosia)
*Alicja Bachleda-Curus (Zosia Horeszko  , 14 year old orphan raised by Telimena)
*Grazyna Szapolowska (Telimena  , a distant relative of the Soplicas and of the Horeszkos, guardian of Zosia Horeszko)
*Andrzej Seweryn (Judge Soplica, younger brother of Jacek Soplica)
*Marek Kondrat (Count Horeszko, a distant relative of the Horeszko family and the rightful owner of the castle)
*Daniel Olbrychski (Gervazy  , the Warden, formerly a servant of the Horeszko family)
*Krzysztof Kolberger (Adam Mickiewicz)
*Sergei Shakurov (Rykow)
*Jerzy Bińczycki (Maciej Królik-Rózeczka)
*Jerzy Trela (Podkomorzy)
*Jerzy Gralek (Wojski)
*Marian Kociniak (Protazy)
*Piotr Gasowski (Rejent)
*Andrzej Hudziak (Asesor)
*Marek Perepeczko (Maciej Chrzciciel)

==Critical reception==
Adam Mickiewiczs Pan Tadeusz is widely considered the foremost text in the Polish literary canon, and held as Polands national epic.  Building off the popularity of its source text, the film was hugely popular in Poland.

==Box Office performance==
Pan Tadeusz was an overwhelming commercial success, but only in its domestic market.  With more than 6 million tickets sold to its screenings in Poland, it was significant in allowing for the unprecedented domination of Polish box-office by domestic productions, an exception in the history of the late 20th century Polish and also European cinema. Pan Tadeusz did not do what Wajda’s other films managed in the past; it was not successful internationally. While Wajda’s Neo Realist trilogy of Pokolenie (A Generation) (1954), Kanal (film)|Kanał (Canal) (1957), and Popiół i diament (Ashes and Diamonds) (1958) was quoted as inspiration by, for instance, Martin Scorsese, and Wajda’s diptych, Człowiek z marmuru (Man of Marble) (1976) and Człowiek z żelaza (Man of Iron) (1981), were hailed by Western European critics as among the better films from beyond the Iron Curtain, Pan Tadeuszs success was largely confined to Poland.

Pan Tadeusz played in eastern Europe during the latter half of 1999, featured at the Berlin Film Festival, and endured a limited, albeit financially unsuccessful, run in the US early in 2000. 

==Soundtrack by Wojciech Kilar==

The Polish film composer, Wojciech Kilar, wrote the score for Pan Tadeusz in addition to scores for over 100 films, including. Kilar as better known internationally for his scores in Bram Stokers Dracula, Death and the Maiden (film), The Portrait of a Lady (film), The Pianist (2002 film), the The Truman Show and The Ninth Gate. Kilar is also internationally known for his epic Exodus, which is famous as the trailer music from Schindlers List. 

==Academy Award submission and Awards== Best Foreign Language Film submission at the 72nd Academy Awards, but did not manage to receive a nomination. 
Pan Tadeusz did however win awards at The Polish Film: Eagle award 2000. It won Best Film score, Best actress, Best Cinematography, Best Sound, Best Editing and Best Production Design.  Andrzej Wajda won a Life Achievement Award at the same ceremony. Mr. Wajda also won an honorary Academy Award (Oscar statuette) in the same year for five decades of extraordinary film direction.

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 