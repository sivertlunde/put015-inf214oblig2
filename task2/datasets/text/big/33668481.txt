Flight (2012 film)
{{Infobox film
| name           = Flight
| image          = Flight film poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Robert Zemeckis
| producer       = {{Plainlist|
* Walter F. Parkes
* Laurie MacDonald
* Steve Starkey
* Robert Zemeckis
* Jack Rapke }}
| writer         = John Gatins
| starring       = {{Plainlist|
* Denzel Washington
* Don Cheadle
* Kelly Reilly
* John Goodman
* Bruce Greenwood
* Brian Geraghty
* Tamara Tunie
* Melissa Leo }}
| music          = Alan Silvestri Don Burgess
| editing        = Jeremiah ODriscoll
| studio         = {{Plainlist| Parkes + MacDonald Prods.
* ImageMovers }}  
| distributor    = Paramount Pictures
| released       =  
| runtime        = 138 minutes   
| country        = United States
| language       = English
| budget         = $31 million 
| gross          =  $161.8 million 
}}
Flight is a 2012 American drama film directed by Robert Zemeckis. The film stars Denzel Washington as Whip Whitaker, an airline pilot who miraculously crash-lands his plane after it suffers an in-flight mechanical failure, saving nearly everyone on board. Immediately following the crash, he is hailed a hero, but an investigation soon leads to questions that puts the captain in a different light.
 Best Actor (Denzel Washington) and Best Original Screenplay (John Gatins).

==Plot== Orlando hotel room. He pilots SouthJet Flight 227 to Atlanta which experiences severe turbulence at takeoff. Copilot Ken Evans (Geraghty) takes over while Whip discreetly mixes vodka in his orange juice and takes a nap. He is jolted awake as the plane goes into a steep dive. Unable to regain control, Whip is forced to make a controlled crash landing in an open field and loses consciousness on impact.

Nearby, heroin addict Nicole Maggen (Reilly) is on the verge of being evicted. After visiting a drug dealer, she overdoses and has to be wheeled out of the house by a paramedic. SouthJet Flight 227 flies overhead in the midst of its emergency landing.

Whip awakens in an Atlanta hospital with moderate injuries and is greeted by his old friend Charlie Anderson (Greenwood), who represents the airlines pilots Trade union|union. He tells Whip that he managed to save 96 out of 102 people on board, losing two crew members and four passengers, and that his copilot is in a coma. Whip sneaks away for a cigarette in the stairwell and meets Nicole, who is recovering from her heroin overdose in the same hospital. The next morning, his friend and drug dealer Harling Mays (Goodman) picks him up from the hospital.

Having retired to his late fathers farm, he meets Charlie and attorney Hugh Lang who explain that the National Transportation Safety Board (NTSB) performed a drug test while he was unconscious, and it showed that Whip was intoxicated during the flight. The results could send him to prison on alcohol, drug, and manslaughter charges. Hugh promises to get the toxicology report voided on technical grounds. Meanwhile, Whip visits and soon becomes intimate with Nicole, but Whips drinking habits clashes with Nicoles attempts to stay off drugs. Later at a crew members funeral, he attempts to persuade a flight attendant who survived to tell the NTSB that he was sober on the flight.

Whip hears on the news that co-pilot Ken Evans has come out of his coma and pays him a visit. He learns that Evans has a slim chance of regaining his ability to walk unaided and probably wont be able to pilot an airplane again, yet Evans has no intentions of telling the NTSB that he knew Whip was drunk. Nicole, who was staying with Whip, decides to leave him after he was unable to break his habits. Hounding Whip, the media catches him drunk when he spontaneously drove to the home of his ex-wife and son, both of whom resent him. He drives to Charlies house begging to stay until the NTSB hearing, vowing not to drink. The night before the hearing, Charlie and Hugh move Whip to a guarded hotel room to prevent him from drinking. Although his minibar contains no alcohol, he finds the door to the adjacent room unlocked and raids its minibar instead.
 elevator assembly jackscrew was the primary cause of the crash. She commends Whip on his valor and skill, explaining that no other pilots were able to land the plane in trial simulations of the crash. She then reveals that two empty vodka bottles were found in the trash of the plane, but none were served to passengers. She goes on to say that two of the crew members toxicology reports were positive for alcohol, but one was excluded from the hearing and the other was from a deceased crew member, Trina. Block asks Whip if he believes Trina drank it. Rather than lie and permanently taint Trinas good name, Whip admits that it was him, and that he flew intoxicated. He also admits hes intoxicated at that moment.

Thirteen months later, an imprisoned Whip tells a support group of fellow inmates that hes glad to be sober and doesnt regret doing the right thing. Whip is seen looking at pictures of Nicole, family, and friends on the wall of his cell, along with greeting cards congratulating him on being sober for a year. He is working to rebuild his relationship with his son, who visits to talk with him about a college application essay on "the most fascinating person that Ive never met." His son begins by asking, "Who are you?" As a plane flies overhead, Whip replies, "Thats a good question."

==Cast==
{{multiple image
 
| align     = right
| direction = vertical
| footer    = Denzel Washington and  Kelly Reilly in Paris at the films French premiere, January 2013.
| width     = 
 
| image1    = Denzel Washington 2013.jpg
| width1    =  
| alt1      = 
| caption1  =

 
| image2    = Kelly Reilly 2013.jpg
| width2    =  
| alt2      = 
| caption2  =

}}
* Denzel Washington as William "Whip" Whitaker, Sr.
* Don Cheadle as Hugh Lang
* Kelly Reilly as Nicole Maggen
* Bruce Greenwood as Charlie Anderson
* John Goodman as Harling Mays
* Melissa Leo as Ellen Block
* Tamara Tunie as Margaret Thomason
* Nadine Velazquez as Katerina Márquez
* Brian Geraghty as Ken Evans
* Peter Gerety as Avington Carr 
* Garcelle Beauvais as Deana Coleman
* Justin Martin as Will Whitaker, Jr.
* James Badge Dale as Gaunt Young Man
* Piers Morgan (cameo appearance)

==Production==
Robert Zemeckis entered negotiations to direct in April 2011,    and by early June had accepted, with Denzel Washington about to finalize his own deal.  It marked the first time Zemeckis and Washington worked together on a motion picture.

By mid-September 2011, Kelly Reilly was in negotiations to play the female lead,  with Don Cheadle,    Bruce Greenwood,  and  John Goodman  joining later in the month, and Melissa Leo and James Badge Dale in final negotiations.  Screenwriter John Gatins said in early October 2011 that production would begin mid-month.  Flight was largely filmed on location near Atlanta, Georgia, over 45 days in November 2011.    The films relatively small budget of $31 million, which Zemeckis later calculated was his smallest budget in inflation-adjusted dollars since 1980, was due to tax rebates from Georgia and from Zemeckis and Washington having waived their customary fees. 

Gatins explained in a 2012 interview with the Los Angeles Times that the dramatic fictional crash depicted in Flight was "loosely inspired" by the 2000 crash of Alaska Airlines Flight 261,  which was caused by a broken jackscrew. That crash had no survivors. The airplane in Flight, a two-engine T-tail jet airliner, appears to be from the same model family as Alaska Airlines 261, a variant of the MD-80. Many elements are lifted from the accident into the film, such as the cause of the accident, segments of the radio communication and the decision to invert the airplane.
 Devil in a Blue Dress), John Goodman (Fallen (1998 film)|Fallen), and Bruce Greenwood (Déjà Vu (2006 film)|Déjà Vu).

==Reception==
===Release===
Flight opened in 1,884 theaters across North America on November 2, 2012. In its first week, the film ranked second in the domestic box office grossing $24,900,566 with an average of $13,217 per theater. Flight earned $93,772,375 domestically and $68,000,000 internationally for a total of $161,772,375, well above its $31 million production budget. 

===Critical response===
 	 		
Flight received mostly positive reviews from critics. The film has an approval rating of 78% based on a sample of 219 critics on Rotten Tomatoes.   Flixster  The site’s consensus states "Robert Zemeckis makes a triumphant return to live-action cinema with Flight, a thoughtful and provocative character study propelled by a compelling performance from Denzel Washington."
Metacritic gives the film a weighted average score of 76 out of 100 based on reviews from 40 critics. 
 Best Picture, he later noted that he felt it deserved to be. Entertainment Weekly wrote, "Denzel Washington didnt get an Oscar nod for nothing: His performance as an alcoholic airline pilot ensnared by his own heroics is crash-and-burn epic." 

The film received some criticism from airline pilots who questioned the films realism, particularly the premise of a pilot being able to continue flying with a significant substance-abuse problem.  The Air Line Pilots Association in an official press release dismissed the film as an inaccurate portrayal of an air crew and stated that "we all enjoy being entertained, but a thrilling tale should not be mistaken for the true story of extraordinary safety and professionalism among airline pilots."  An airline pilot also commented that "a real-life Whitaker wouldnt survive two minutes at an airline, and all commercial pilots—including, if not especially, those whove dealt with drug or alcohol addiction—should feel slandered by his ugly caricature."  . The Daily Beast (November 18, 2012). Retrieved July 13, 2013.  The pilot also criticised the portrayal of the relationship between copilot and captain, the decision of Whitaker to increase speed dangerously in a storm, and the ultimate dive and crash landing of Whitakers aircraft. 

====Top ten lists====
 
* 3rd&nbsp;– Steven Rea, Philadelphia Inquirer
* 3rd&nbsp;– Rafer Guzmán, Newsday
* 6th&nbsp;– Roger Ebert, Chicago Sun-Times
* 7th&nbsp;– Richard Roeper, RichardRoeper.com
* 7th&nbsp;– Kyle Smith, New York Post
* 8th&nbsp;– Brian Tallerico, Hollywood Chicago Michael Phillips, Chicago Tribune
* 9th&nbsp;– Owen Gleiberman, Entertainment Weekly
 

===Awards and nominations===
{| class="wikitable"
|+ List of awards and nominations
! Award
! Category
! Subject
! Result
|- 85th Academy Academy Award Academy Award Best Writing, Original Screenplay John Gatins
| 
|- Academy Award Best Performance by an Actor in a Leading Role Denzel Washington
| 
|- AACTA Awards AACTA International Best International Actor Denzel Washington
| 
|- Art Directors Art Directors Guild Award Art Directors Excellence in Production Design for a Contemporary Film Nelson Coates
| 
|- Black Reel Black Reel Award Black Reel Best Film
|Flight
| 
|- Black Reel Best Actor Denzel Washington
| 
|- Black Reel Best Supporting Actress Tamara Tunie
| 
|- Black Reel Best Ensemble The Cast of Flight
| 
|- Broadcast Film Critics Association Award Broadcast Film Best Actor Denzel Washington
| 
|- Broadcast Film Best Original Screenplay John Gatins
| 
|- Chicago Film Critics Association Award Chicago Film Best Actor Denzel Washington
| 
|- Chicago International Film Festival
|Founders Award Robert Zemeckis
| 
|-
|Dallas-Fort Worth Film Critics Association Award Best Actor Denzel Washington
| 
|- Golden Globe Award Golden Globe Best Actor – Motion Picture Drama Denzel Washington
| 
|- Hollywood Film Festival Spotlight Award Kelly Reilly
| 
|- NAACP Image Award NAACP Image Outstanding Motion Picture
|Flight
| 
|- NAACP Image Outstanding Actor Denzel Washington
| 
|- NAACP Image Outstanding Supporting Actor Don Cheadle
| 
|- Outstanding Writing in a Motion Picture John Gatins
| 
|- National Board of Review Spotlight Award John Goodman, also for Argo (2012 film)|Argo, ParaNorman, and Trouble with the Curve
| 
|- Online Film Online Film Critics Society Award Online Film Best Actor Denzel Washington
| 
|- Palm Springs Palm Springs International Film Festival Award Director of the Year Robert Zemeckis
| 
|- Satellite Award Satellite Award Best Actor – Motion Picture Denzel Washington
| 
|- Satellite Award Best Supporting Actor – Motion Picture John Goodman
| 
|- Best Screenplay, Original John Gatins
| 
|- Best Visual Effects Jim Gibbs, Kevin Baillie, Michael Lantieri and Ryan Tudhope
| 
|- Satellite Award Best Editing Jeremiah ODriscoll
| 
|- Best Sound (Editing & Mixing) Dennis Leonard Dennis Leonard, Dennis Sands, Randy Thom and William Kaplan
| 
|- Screen Actors Guild Award Screen Actors Outstanding Lead Actor Denzel Washington
| 
|-
|rowspan=2|St. Louis Gateway Film Critics Association Awards 2012|St. Louis Gateway Film Critics Association Best Actor Denzel Washington
| 
|- Best Scene (favorite movie scene or sequence) The plane crash
| 
|- Visual Effects Visual Effects Society Outstanding Supporting Visual Effects in a Feature Motion Picture
|
| 
|- Washington DC Area Film Critics Association Award Best Actor Denzel Washington
| 
|- Writers Guild of America Award Writers Guild Best Original Screenplay John Gatins
| 
|}

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 