True Grit (2010 film)
{{Infobox film
 | name = True Grit
 | image = True Grit Poster.jpg
 | alt =
 | caption = Theatrical release poster Joel Coen Ethan Coen
 | producer = Joel Coen Ethan Coen Scott Rudin
 | screenplay = Joel Coen Ethan Coen
 | based on =  
 | narrator = Elizabeth Marvel
 | starring = Jeff Bridges Matt Damon Josh Brolin Barry Pepper Hailee Steinfeld
 | music = Carter Burwell
 | cinematography = Roger Deakins
 | editing = Joel Coen Ethan Coen  (as Roderick Jaynes)
 | studio = Skydance Productions Mike Zoss Productions Scott Rudin Productions Amblin Entertainment  (uncredited) 
 | distributor = Paramount Pictures
 | released =  
 | runtime = 110 minutes
 | country = United States
 | language = English
 | budget = $38 million 
 | gross = $252.2 million 
}} western directed, adaptation of novel of filmed in Reuben J. "Rooster" Cogburn, along with Matt Damon, Josh Brolin, and Barry Pepper.
 Best Director, Best Adapted Best Actor Best Actress Best Art Best Cinematography, Best Costume Best Sound Best Sound Editing. The film was released on Blu-ray and DVD on June 7, 2011.

== Plot == Deputy U.S. Rooster Cogburn (Bridges), whom the sheriff had described as being the "meanest". The taciturn, one-eyed Cogburn rebuffs her offer, not believing she has the reward money to hire him. She raises the money by aggressively horse-trading with Colonel Stonehill, who did business with her father.
 Texas Ranger LaBoeuf (Damon) arrives in town on the trail of Chaney for the murder of a Texas State Senator. LaBoeuf proposes to team up with Cogburn, who knows the Choctaw terrain where Chaney is hiding, but Mattie refuses his offer. She wishes Chaney to be hanged in Arkansas for her fathers murder, not in Texas for killing the senator. Mattie also insists on traveling with Cogburn to search for Chaney and secure her $50 payment, but Cogburn later leaves without her, having gone with LaBoeuf to apprehend Chaney.
 birching Mattie with a stick, but Cogburn eventually stops him. After a dispute over their respective service with the Confederate States of America, Cogburn ends their arrangement and LaBoeuf leaves. Later, while pursuing the Pepper gang that Chaney is reportedly traveling with, the two meet a trail doctor who directs them to an empty dugout for shelter. There they find two outlaws, Quincey and Moon, and interrogate them. Soon Moon, fearful of losing his leg, acquiesces by divulging what he knows to Cogburn, Quincey then fatally stabs Moon, and Cogburn shoots Quincey dead. Before dying, Moon says Pepper and his gang will be returning later that night.

Just before they arrive, LaBoeuf arrives at the dugout and is confronted. Cogburn, hiding on the hillside with Mattie, shoots two gang members and LaBouef, but Pepper escapes. The next day, Cogburn gets in a drunken argument with the wounded LaBoeuf, who departs once again. While getting water from a nearby stream, Mattie encounters Chaney. She shoots and wounds him, but he survives and drags her back to Ned, who forces Cogburn to leave by threatening to kill her. Being short a horse, Ned leaves Mattie with Chaney, ordering him not to harm her or he will not get paid after his remount arrives.

Once alone, Chaney disobeys Ned and tries to knife Mattie. LaBoeuf suddenly appears and knocks Chaney out, explaining that he rode back when he heard the shots, and he and Cogburn devised a plan. They watch from a cliff as Cogburn takes on the remaining members of Neds gang, killing two  and wounding Ned, before his horse is shot and falls, trapping Cogburns leg. Before Pepper can kill him, LaBoeuf snipes Pepper from roughly four hundred yards away with his Sharps Rifle. Chaney however regains consciousness and knocks LeBoeuf unconscious with a rock. Mattie seizes LaBoeufs rifle and shoots Chaney in the chest. The recoil also knocks her into a deep pit. Cogburn soon arrives, but Mattie is bitten by a rattlesnake before he can get to her. Cogburn cuts into her hand to suck out as much of the venom as he can, and then rides day and night to get Mattie to a doctor, carrying her on foot after Blackie her horse collapses from exhaustion, finally making his way to Bagbys store.
 Wild West show in which he now performs. She arrives, only to learn that Cogburn died three days earlier. She has his body moved to her family cemetery. Standing over Cogburns grave, she reflects on her decision to move his remains, and about never having married. She also reveals that she never saw LaBoeuf again, though she would like to, and observes that "time just gets away from us".

== Cast ==
 
  Reuben J. "Rooster" Cogburn Texas Ranger LaBoeuf
* Josh Brolin as Tom Chaney
* Barry Pepper as "Lucky" Ned Pepper
* Hailee Steinfeld as Mattie Ross
* Domhnall Gleeson as Moon (the Kid)
* Bruce Green as Harold Parmalee Ed Lee Corbin as Bear Man (Dr. Forrester)
* Roy Lee Jones as Yarnell Poindexter
* Paul Rae as Emmett Quincy
* Nicholas Sadler as Sullivan
* Dakin Matthews as Colonel Stonehill
* Elizabeth Marvel and Ruth Morris as 40-year-old Mattie  Sheriff
* Jake Walker as Judge Isaac Parker
* Don Pirl as Cole Younger
* James Brolin as Frank James (uncredited cameo)
* Jarlath Conroy as The Undertaker
* J. K. Simmons as Lawyer J. Noble Daggett (voice only; uncredited)
 

== Adaptation and production ==

The project was rumored as far back as February 2008;  however it was not confirmed until March 2009. 
 Ethan Coen the novel 1969 version.

 

Mattie Ross "is a pill," said Ethan Coen in a December 2010 interview, "but there is something deeply admirable about her in the book that we were drawn to," including the Presbyterian-Protestant ethic so strongly imbued in a 14-year-old girl. Joel Coen said that the brothers did not want to "mess around with what we thought was a very compelling story and character". The films producer, Scott Rudin, said that the Coens had taken a "formal, reverent approach" to the Western genre, with its emphasis on adventure and quest. "The patois of the characters, the love of language that permeates the whole film, makes it very much of a piece with their other films, but it is the least ironic in many regards". 

Open casting sessions were held in Texas in November 2009 for the role of Mattie Ross. The following month, Paramount Pictures announced a casting search for a 12- to 16-year-old girl, describing the character as a "simple, tough as nails young woman" whose "unusually steely nerves and straightforward manner are often surprising".  Steinfeld, then age 13, was selected for the role from a pool of 15,000 applicants. "It was, as you can probably imagine, the source of a lot of anxiety", Ethan Coen told The New York Times. "We were aware if the kid doesnt work, theres no movie".   
 Granger and trailer was released in September; a second trailer premiered with The Social Network.

True Grit is the first Coen brothers film to receive a Motion Picture Association of America#The five ratings|PG-13 rating since 2003s Intolerable Cruelty for "some intense sequences of western violence including disturbing images".

For the final segment of the film, a one-armed body double was needed for Elizabeth Marvel (who played the adult Mattie). After a nationwide call, the Coen brothers cast Ruth Morris – a 29-year-old social worker and student who was born without a left forearm.    Morris has more screen time in the film than Marvel. 

== Soundtrack ==
 

== Reception ==
=== Critical reception ===
{| class="wikitable" style="width:99%;"
|-
! Film
! Rotten Tomatoes
! Metacritic
|-
| True Grit
| style="text-align: center; "| 96% (259 reviews) 
| style="text-align: center; "| 80/100 (41 reviews) 
|}

The film received critical acclaim.   gave the film an average score of 80/100 based on 40 reviews from mainstream critics, indicating "generally favorable reviews".  Total Film gave the film a five-star review (denoting outstanding): "This isnt so much a remake as a masterly re-creation. Not only does it have the drop on the 1969 version, its the first great movie of 2011". 

Roger Ebert awarded 3.5 stars out of 4, writing, "What strikes me is that Im describing the story and the film as if it were simply, if admirably, a good Western. Thats a surprise to me, because this is a film by the Coen Brothers, and this is the first straight genre exercise in their career. Its a loving one. Their craftsmanship is a wonder", and also remarking, "The cinematography by Roger Deakins reminds us of the glory that was, and can still be, the Western." 

The Los Angeles Times critic Kenneth Turan gave the film 4 out of 5 stars, writing, "The Coens, not known for softening anything, have restored the originals bleak, elegiac conclusion and as writer-directors have come up with a version that shares events with the first film but is much closer in tone to the book... Clearly recognizing a kindred spirit in Portis, sharing his love for eccentric characters and odd language, they worked hard, and successfully, at serving the buoyant novel as well as being true to their own black comic brio."   
 Time Magazine named Hailee Steinfelds performance one of the Top 10 Movie Performances of 2010, saying "She delivers the orotund dialogue as if it were the easiest vernacular, stares down bad guys, wins hearts. Thats a true gift". 

Rex Reed of The New York Observer criticized the films pacing, referring to plot points as "mere distractions ... to divert attention from the fact that nothing is going on elsewhere". Reed considers Damon "hopelessly miscast" and finds Bridges performance mumbly, lumbering, and self-indulgent. 

Entertainment Weekly gave the movie a B+: "Truer than the John Wayne showpiece and less gritty than the book, this True Grit is just tasty enough to leave movie lovers hungry for a missing spice." 

The US Conference of Catholic Bishops review called the film "exceptionally fine" and said " mid its archetypical characters, mythic atmosphere and amusingly idiosyncratic dialogue, writer-directors Joel and Ethan Coens captivating drama uses its heroines sensitive perspective – as well as a fair number of biblical and religious references – to reflect seriously on the violent undertow of frontier life. " 

=== Box office ===

{| class="wikitable" style="width:100%;"
|-
! rowspan="2" | Film
! colspan="1" | Release date
! colspan="3" | Box office revenue
! colspan="2" text="wrap" | Box office ranking
! rowspan="2" style="text-align: center; "| Budget
! rowspan="2" style="text-align: center; "| Reference
|-
! North America
! North America 
! Other territories 
! Worldwide
! All time United States
! All time worldwide
|-
| True Grit
| style="text-align: center; "| December 22, 2010
| style="text-align: center; "| $171,050,328
| style="text-align: center; "| $79,880,786
| style="text-align: center; "| $250,931,114
| style="text-align: center; "| #168
| style="text-align: center; "| #327
| style="text-align: center; "| $38,000,000
| style="text-align: center; "| 
|}
 No Country for Old Men, which earned $74.3 million. True Grit was the only mainstream movie of the 2010 holiday season to exceed the revenue expectations of its producers. Based on that performance, The Los Angeles Times predicted that the film would likely become the second-highest grossing western of all time when inflation is discounted, exceeded only by Dances with Wolves.  On Thursday, December 23, 2010, it opened to #3 behind Little Fockers and Tron: Legacy. On Friday, December 24, 2010, it went up to #2 behind Little Fockers. On Friday, December 31, 2010 it went up to #1 and then on January 1, 2011, it went back to #2 until January 3, 2011. It stayed #1 until January 14 and then went down to #3 behind The Green Hornet and The Dilemma. On February 11, 2011, it went down to #9 behind Justin Bieber: Never Say Never, Just Go With It, Gnomeo and Juliet, The Eagle, The Roommate, The Kings Speech, No Strings Attached, and Sanctum. It closed in theaters on April 28, 2011. True Grit took in an additional $15 million in what is usually a slow month for movie attendance, reaching $110 million.    According to Box Office Mojo, True Grit has grossed over $170 million domestically and $250 million worldwide as of July 2011.
 Rob Moore attributed the films success partly to its "soft" PG-13 rating, atypical for a Coen brothers film, which helped broaden audience appeal. Paramount anticipated that the film would be popular with the adults who often constitute the Coen brothers core audience, as well as fans of the Western genre. But True Grit also drew extended families: parents, grandparents, and teenagers. Geographically, the film played strongest in Los Angeles and New York, but its top 20 markets also included Oklahoma City; Plano, Texas; and Olathe, Kansas.      

=== Home media ===

The film was released on DVD and Blu-ray on June 7, 2011. 

== Awards ==
 
 Best Actor Best Supporting Best Director, Best Adapted Best Cinematography, Best Art Best Costume Best Makeup, Best Score. The ceremony took place on January 14, 2011. 
 Best Actress The ceremony took place on January 30, 2011. 
 Best Actor Best Actress Best Adapted Best Cinematography, Best Production Best Costume Best Cinematography.

It was nominated for ten  ,   

== References ==
 

== External links ==
 

*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 