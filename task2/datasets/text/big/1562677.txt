The Chipmunk Adventure
{{Multiple issues|
 
 
}}
 
{{Infobox film
| name = The Chipmunk Adventure
| image = Chipmunkadventure1987.jpg
| alt = 
| caption = Theatrical release poster
| director = Janice Karman Ross Bagdasarian
| writer = {{Plainlist|
* Ross Bagdasarian
* Janice Karman}}
| starring = {{Plainlist|
* Ross Bagdasarian
* Janice Karman
* Dody Goodman
* Ken Sansom
* Nancy Cartwright}}
| music = Randy Edelman
| editing = Tony Mizgalski
| studio = Bagdasarian Productions
| distributor = The Samuel Goldwyn Company
| released =  
| runtime = 78 minutes
| country = United States
| language = English
| gross = $6.8 million 
}} animated Musical musical Adventure adventure comedy Alvin and Ross Bagdasarian, stars the voices of Karman, Bagdasarian, and Dody Goodman.

==Plot==
  David Seville the Chipmunks, Simon and Theodore are Miss Miller Brittany argue over which would win an actual race around the world. Diamond smugglers Claudia and Klaus Furschtein overhear the conversation and approach the children, telling them that they will provide them with the means for a real race around the world by hot air balloon, with the winner receiving $100,000.
 INTERPOL agents, sent by Inspector Jamal, who has been informed by Claudia and Klaus butler, Mario, of the Furschteins operation. The agents silently follow behind as the Chipmunks and Chipettes travel to many exotic locations. Both teams meet up in Athens, where Alvin and Brittany begin bickering, each claiming they can "out-rock and roll" the other. What ensues is a large-scale musical number set in a nearby temple before they part ways in their separate balloons, with Dave and the two INTERPOL agents nearly spotting them.

The Chipettes then travel on to Egypt, where they are captured and brought to a young Arabian prince.  The prince falls in love with Brittany and, oblivious to the diamond smuggling, agrees to return the valuable dolls to Jamal but declares that he is keeping the girls, and making Brittany his bride. Despite being showered with gifts, including a baby penguin, the Chipettes are desperate to make an escape. Still unaware of the contents of their dolls, they secretly try to retrieve them - only to find them guarded by snakes. Brittany and Jeanette "charm" the snakes by singing "Getting Lucky", then grab the dolls and escape just as Eleanor arrives carrying a small cooler.

Meanwhile, the Chipmunks are camping in a jungle near a native village while taking a shortcut. Theodore senses something is wrong; Simon and Alvin dont believe him. Upon waking up the next morning, they find Theodore missing. While trying to find him, Alvin and Simon are taken captive by some local natives. They soon learn that Theodore is being worshipped as the natives "Prince of Plenty". Alvin and Simon are quickly forced to wear loincloths and are made into Theodores personal servants.

Meanwhile, the Chipettes learn that Eleanors cooler contains not food, but the baby penguin, whom Eleanor feels needs to be returned to its parents in Antarctica. The girls soon decide to head to Antarctica and sing the song "My Mother". However, Claudia is alerted to the girls actions by her henchmen, and concludes that they must have discovered the diamonds and are trying to escape with them. She orders her henchmen to get the dolls back.  As the girls deliver the young penguin to its parents, they are attacked by Claudias henchmen. The Chipettes wrestle with the men as they attempt to steal the dolls, and a doll tears as the henchman falls from the balloon. Grateful to the Chipettes, the penguins snowball the henchmen into submission as the girls escape. They then notice the torn Chipette doll lying on the floor of the balloon; Jeanette picks it up and diamonds fall out of it.  Opening a Chipmunk doll, they find it full of dollar bills, and realize that the whole race was just a set-up to deliver the diamonds. Realizing that the Chipmunks are likely in danger as well, they head off to find them.

Alvin and Simon, meanwhile, are forced to search a nearby cave for mushrooms, with Alvin growing increasingly fed-up with the situation. Inside the cave, they find a series of ideograms on the walls, which Simon is able to translate -thanks to his vast intellect. The ideograms tell of the Prince of Plenty being sacrificed on a full moon, which is that night. Before long, all three Chipmunks are tied to stakes by the natives, hanging precariously above a pit full of crocodiles. In a desperate attempt to delay their own deaths, they sing "Wooly Bully," which pleases the natives and lasts just long enough for the Chipettes to rescue them.

Together in the Chipettes balloon, the kids return to Los Angeles, where they are met at the airport by Claudia and Klaus. After a high-energy chase through the baggage claim (as the Chipettes sing Diamond Dolls), the two groups are forced into a car under the false threat of Miss Millers being kidnapped. Dave is arriving home to the airport at the same time, and hears Alvins cries for help. Inspector Jamal runs up, identifying himself, and he and Dave begin to make a chase. While it at first appears that Claudia and Klaus will escape, they are accidentally run off the road by Miss Miller as she is heading to the airport to pick Dave up, who happens to drive up the wrong way towards them by accident. The diamond smugglers are arrested, and Brittany and Alvin argue over who really won the race, but Dave breaks up the argument and guides them to Miss Millers car. Dave tells Alvin that he and the Chipmunks and the Chipettes have a lot of explaining to do. Alvin repeatedly protests about being entitled to the promised prize money, causing Dave to yell "ALVIN!" as the film ends.

==Voice cast== Ross Bagdasarian as Dave Seville, Alvin and Simon
* Janice Karman as Theodore, Brittany, Jeanette, and Eleanor
* Dody Goodman as Miss Miller
* Ken Sansom as Inspector Jamal, a detective for the INTERPOL
* Nancy Cartwright as the Arabian Prince
* Anthony De Longis as Klaus Furschtein, a European diamond smuggler
* Susan Tyrrell as Claudia Furschtein, another European diamond smuggler and Klaus sister
* Frank Welker as Sophie, the Furschteins pet Yorkshire Terrier puppy / Baby penguin

==Production== The Black Cauldron in 1985 had led to the layoff of a number of Disney animators, whom Bagdasarian promptly hired to work on his film. The result was extremely high-caliber animation.

Bagdasarian and his wife Janice Karman decided to finance the project themselves, having generated so much revenue from the Alvin and the Chipmunks TV series. Their decision to work with several overseas studios led to major production delays. By late 1986, production had fallen far behind schedule, and a shortage of time and money resulted in major cuts being made to the film.

==Music==
The film score of The Chipmunk Adventure was composed by Randy Edelman and performed by the Royal Philharmonic Orchestra; Edelman also contributed songs to the film. Several songs throughout the film were performed by both the Chipmunks and the Chipettes. On April 1, 2008, the soundtrack was re-released as a bonus CD with the films DVD.

;Track listing
# "Chipmunk Adventure Theme" - Royal Philharmonic Orchestra
# "I, Yi, Yi, Yi, Yi (I Like You Very Much)|I, Yi, Yi, Yi, Yi/Cuanto le Gusta" - The Chipmunks
# "Off to See the World" - The Chipmunks and the Chipettes
# "Weekend in France, Italy, England, Amsterdam, Greece..." - David Seville and the Chipmunks (heard instrumentally in the film)
# "The Girls and Boys of Rock and Roll" - The Chipmunks and the Chipettes
# "Flying with the Eagles" - The Chipmunks and the Chipettes (a shortened instrumental version of the chorus heard as part of a longer piece of music during the start of the race)
# "Getting Lucky" - The Chipettes
# "Mexican Holiday" - The Chipmunks (heard instrumentally in the film)
# "My Mother" - The Chipettes
# "Wooly Bully" - The Chipmunks
# "Diamond Dolls" - The Chipettes

;Songs heard in the film but not on the album
# "Underwaterture" - Randy Edelman. Lengthy piece of music recorded for the underwater sequence. Unreleased on record.
# "Come On-a My House" - Dody Goodman. Unreleased on record. Sung briefly by Miss Miller. So Serious" single, four months ahead of the films release. Unavailable in the U.S. until 1990 in its inclusion on that bands first box set, Afterglow (box set)|Afterglow. Witch Doctor" - Dody Goodman. Unreleased on record. Sung briefly by Miss Miller.

==Release==
The film was promoted one year before its release, at the 1986 Cannes Film Festival.   

Though initially scheduled for Christmas 1986,  The Chipmunk Adventure opened on May 22, 1987 through The Samuel Goldwyn Company and Bagdasarian Productions. With an opening weekend take of $2,584,720, it ultimately grossed $6,804,312 in North America alone.    

===Critical reception===
In his Family Guide to Movies on Video, Henry Herx deemed the film a "charming, lighthearted diversion for the younger set", and remarked that it resembled "a musical revue of pop tunes".   
 Siskel and Ebert condemned the film, saying it would inspire kids to "trick their parents and run away from home" and that some parents left theaters rather than let their children be exposed to the chipmunk misbehavior.  Both also criticized the plot and the chipmunks squeaky, high-pitched voices.

===Home media=== Paramount Home Alvin and Alvin and the Chipmunks Go to the Movies. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 