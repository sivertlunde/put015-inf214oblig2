Authors Anonymous
{{Infobox film
| name           = Authors Anonymous
| image          = Authors Anonymous film poster.jpg
| caption        = Film poster
| director       = Ellie Kanner
| producer       = Ellie Kanner Hal Schwartz
| writer         = David Congalton
| starring       = {{Plainlist|
* Kaley Cuoco Chris Klein
* Tricia Helfer
* Jonathan Banks Jonathan Bennett
* Teri Polo
* Dennis Farina
}}
| music          = Jeff Cardoni
| cinematography = Tobias Datum
| editing        = Stephen Myers
| studio         = Bull Market Entertainment Forever Sunny Productions Starz Digital
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 comedy American Chris Klein, Jonathan Bennett, Starz Digital VOD release date of March 18, 2014 before its theater release on April 18, 2014.      

==Summary==
When several dysfunctional and unpublished writers accept young Hannah into their clique, they dont expect her overnight success. 

Hannah, who has rarely even read a book, let alone written one, not only manages to land a literary agent to represent her, she cashes in on a deal to turn her first manuscript into a Hollywood film. The support of her weekly writers group, Authors Anonymous, turns to resentment.

Colette Mooney receives rejection letters galore from agents and publishers. Her husband, optician Alan, speaks ideas into a hand-held recorder all day long, but never acts on them. Henry Obert has writers block, as well as a huge crush on Hannah, while a Tom Clancy wanna-be, John K. Butzin, resorts to self-publishing in a delusional quest to become a best-selling author, helped by a young hardware store employee named Sigrid who believes in him.

In time, Hannah realizes that maintaining a relationship with these people is next to impossible, but does what she can to at least encourage Henry to begin writing again.

==Cast==
* Kaley Cuoco as Hannah Rinaldi  Chris Klein as Henry Obert 
* Tricia Helfer as Sigrid Hagenguth
* Jonathan Banks as David Kelleher Jonathan Bennett as William Bruce 
* Teri Polo as Colette Mooney 
* Dennis Farina as John K. Butzin 
* Dylan Walsh as Alan Mooney 
* Charlene Amoia as Eudora
* Meagen Fay as Maureen
*Robb Skyler as Dr. Xiroman
*Diane Robin as Lois Pepper

==Production==

===Pre-production=== Jonathan Bennett and Kaley Cuoco also served as executive producers to the film.   

===Filming===
The film was shot in and around Los Angeles during August 2012.  It includes one of Dennis Farinas last performances before his death in July 2013. 

==References==
 

==External links==
* 

 
 
 
 