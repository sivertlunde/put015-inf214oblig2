Spider-Man 2
 
 
{{Infobox film
| name           = Spider-Man 2
| image          = Spider-Man 2 Poster.jpg
| caption        = Theatrical release poster
| director       = Sam Raimi
| producer       = {{Plain list|
* Avi Arad
* Laura Ziskin
}}
| screenplay     = Alvin Sargent
| story          = {{Plain list|
* Alfred Gough
* Miles Millar
* Michael Chabon
}}
| based on       =   
| starring       = {{Plain list|
* Tobey Maguire
* Kirsten Dunst
* James Franco
* Alfred Molina
* Rosemary Harris
* Donna Murphy
 
}}
| music          = Danny Elfman
| cinematography = Bill Pope
| editing        = Bob Murawski
| studio         = Marvel Enterprises Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 127 minutes
| country        = United States 
| language       = English
| budget         = $200 million   
| gross          = $783.8 million 
}} of the same name. Tobey Maguire, Kirsten Dunst, and James Franco reprise their respective roles as Peter Parker/Spider-Man, Mary Jane Watson, and Harry Osborn.

Set two years after the events of Spider-Man, the film focuses on Peter Parker struggling to manage both his personal life and his duties as Spider-Man, while Doctor Octopus|Dr. Otto Octavius (Doctor Octopus) becomes diabolical following a failed experiment and his wifes death. He uses his mechanical tentacles to threaten and endanger the lives of New York Citys residents. Spider-Man must stop him from annihilating the city.
 Best Fantasy Best Director for Raimi. The films success led to Spider-Man 3, released in 2007.

==Plot==
  Peter Parker struggles to balance his crime-fighting duties as Spider-Man with the demands of his normal life. Estranged from both love interest Mary Jane Watson and best friend Harry Osborn, who intends to seek revenge on Spider-Man for his father Norman Osborn|Normans death, Peter additionally discovers his Aunt May is facing foreclosure.
 robotic tentacle arms with artificial intelligence while conducting his research. When a power spike causes an experiment to destabilize rapidly, Octavius stubbornly refuses to shut the experiment down, leading to disastrous consequences: his wife is killed, the neural inhibitor chip which keeps the arms from influencing his mind is destroyed and the arms are fused to his spine. Spider-Man arrives and shuts down the experiment before it can do any further damage.

At a hospital, doctors prepare to surgically remove Octavius arms and harness, but the arms, having developed sentience from the inhibitor chips destruction, spring to life and attack the medical crew, killing most of them. Upon regaining consciousness and seeing the carnage, Octavius escapes and hides at a harbor. The arms convince him to retry the experiment. To fund it, Octavius — now called Doctor Octopus by the Daily Bugle — robs a bank.
 John Jameson, son of Bugle chief J. Jonah Jameson. Peter suffers an emotional breakdown causing him to believe hes lost his powers. He abandons his Spider-Man identity and returns to his normal life while trying to reconcile with Mary Jane.
 Aunt May that his Uncle Bens death some time ago was his fault. May forgives him, but when his 9-year-old neighbor learns of Spider-Mans disappearance and the subsequent rising crime rate in New York City, Peter becomes concerned.

Octavius needs tritium to fuel his reactor and goes to Harry to demand it. Harry initially refuses because the experiment threatens to level the city, but he eventually agrees in exchange for Spider-Man and tells him that Peter, who is supposedly good friends with Spider-Man, is the key to finding him. However, Harry tells Octavius not to harm Peter. Octavius finds Peter, tells him to find Spider-Man, and abducts Mary Jane. Peter realizes his powers are restored due to the trauma of seeing Mary Jane kidnapped, dons his costume again after stealing it from the Bugle.
 R train. Octavius disables the controls and jumps off. Spider-Man stops the train before the track ends. When he faints from exhaustion, the passengers carry him into one of the cars. He comes to and realizes his mask is off, but the passengers are so grateful they vow not to reveal what he looks like. Octavius returns, demanding Spider-Man, and subdues the passengers. After knocking out Spider-Man, Octavius delivers him to Harry.

After giving Octavius the tritium, Harry prepares to kill Spider-Man, only to be shocked to see it is really Peter. Peter convinces him greater things are at stake, and Harry reveals Octavius location. Spider-Man arrives at the doctors waterfront laboratory and tries to rescue Mary Jane discreetly. One of Octavius tentacles senses him, and they fight. Spider-Man ultimately subdues Octavius, reveals his identity, and convinces Octavius to let go of his dream for the greater good. Octavius finally commands the tentacles to obey and drowns the fusion reactor, along with himself, in the Hudson River. Mary Jane discovers Spider-Mans true identity and feelings, as well as why they cannot be together. Spider-Man returns Mary Jane to John and leaves.

Harry is visited by a vision of his father, pleading for Harry to avenge his death. Refusing to hurt Peter, Harry shatters the mirror, revealing a secret room containing the Green Goblins equipment. On her wedding day, Mary Jane abandons John at the altar and runs to Peters apartment. She admits her true feelings for Peter. They kiss, hear a police chase, and she encourages him to respond as Spider-Man.

==Cast== Peter Parker / Spider-Man
:A superhero, a Columbia University physics student and photographer for the Daily Bugle. Juggling these separate lives means he briefly gives up his responsibilities as a superhero in a moment of adversity. When Maguire signed on to portray Spider-Man in 2000, he was given a three-film contract.  While filming Seabiscuit (film)|Seabiscuit in late 2002, Maguire suffered injuries to his back and Sony was faced with the possibility of recasting their lead.  Negotiations arose to replace Maguire with Jake Gyllenhaal, who at the time was dating Kirsten Dunst, who portrayed Mary Jane Watson. However, Maguire recovered and was able to reprise his role, with a salary of $17&nbsp;million.   
* Kirsten Dunst as Mary Jane Watson
:A friend Peter Parker has loved since he was a child, yet he gave up the chance of being with her due to his obligations as a superhero.
* James Franco as Harry Osborn
:Oscorps leader and Norman Osborns son who holds a resentment against Spider-Man over his fathers death.
* Alfred Molina as Doctor Octopus|Dr. Otto Octavius / Doctor Octopus
:A scientist and Peters role model who goes insane after his failure to create a self-sustaining fusion reaction. Octavius is bonded with his handling equipment, four artificially intelligent mechanical tentacles. Molina was cast as Octavius in February 2003 and immediately began physical training for the role.  Raimi had been impressed by his performance in Frida and also felt he had the physicality.  Molina only briefly discussed the role and was not aware that he was a strong contender,  and was excited, being a big fan of Marvel Comics.  Although he wasnt familiar with Doc Ock, Molina found one element of the comics that he wanted to maintain, and that was the characters cruel, sardonic sense of humor.  May Parker
:Ben Parkers widow and Peters aunt.
* J.K. Simmons as J. Jonah Jameson
:The miserly chief of the Daily Bugle who carries a personal vendetta against Spider-Man, whom he considers a criminal.
* Donna Murphy as Rosalie Octavius
:Otto Octavius wife and assistant John Jameson
:J. Jonah Jamesons son, Mary Janes fiancé and a national hero.
* Dylan Baker as Lizard (comics)|Dr. Curt Connors
:One of Peters college physics professors. He is a colleague of Octavius. Norman Osborn / Green Goblin King Hamlet haunting his son to avenge him. 
* Mageina Tovah as Ursula Ditkovich
:The daughter of the landlord of Peters apartment.

As with the previous film, Bruce Campbell has a cameo appearance as an usher who refuses to let Peter enter the theater for arriving late to Mary Janes play, thus causing a rift in their relationship. Spider-Man co-creator Stan Lee portrays a man on the street who saves a woman from falling debris during a battle between Spider-Man and Doctor Octopus. Scott Spiegel portrays a man who attempts to eat some pizza Spider-Man is delivering, only to have it webbed from his hands. Joel McHale portrays the teller in the bank who refuses Aunt Mays loan. Hal Sparks portrays an elevator passenger who has a conversation with Spider-Man. Donnell Rawlings portrays the New Yorker who exclaims that Spider-Man "stole that guys pizzas". Emily Deschanel portrays the receptionist who tells Parker she is not paying for the late pizza. Elizabeth Banks portrays Betty Brant, one of the Bugle staff and J. Jonah Jamesons secretary. Daniel Dae Kim plays an assistant of Doctor Octavius working in his laboratory. Aasif Mandvi portrays Mr. Aziz, the pizza store owner who later dismisses Parker. Vincent Pastore portrays a train passenger who tells Doctor Octopus that he has to get past him to get to Spider-Man; Joey Diaz portrays a similar passenger. Vanessa Ferlito portrays one of Mary Janes co-stars. Joy Bryant has a cameo appearance as a spectator that witnesses Spider-Man in action. John Landis plays one of the doctors who operates on Doctor Octopus. Phil LaMarr portrays a train passenger who is most easily seen to the left of Spider-Man (the viewers right) while the hero uses webbing to slow the train down. Greg Edelman portrays Dr. Davis, the doctor at the University, that Peter Parker sees to talk about the loss of his superpowers.

==Production==

===Development===
{{multiple image
 | align     = right
 | direction = vertical
 | header    =
 | header_align = left/right/center
 | header_background =
 | footer    =
 | footer_align = left/right/center
 | footer_background =
 | width     =
 | image1    = Spideygivesup.jpg
 | width1    = 240
 | caption1  = Peter Parker gives up being Spider-Man.
 | image2    = SpiderManNoMore.jpg
 | width2    = 240
 | caption2  = Panel of "Spider-Man No More!" which Raimi replicated for the film. Art by John Romita Sr.
}} the Lizard Black Cat as villains.    On May 8, 2002, following Spider-Man (2002 film)|Spider-Mans record breaking $115&nbsp;million opening weekend, Sony Pictures announced a sequel for 2004.  Entitled The Amazing Spider-Man, after the characters main comic book title,    the film was given a budget of $200&nbsp;million  and aimed for a release date of May 7, 2004. The following month, David Koepp was added to co-write with Gough and Millar. 

In September 2002,   put a $10&nbsp;million price on Spider-Mans head, causing the citys citizens to turn against him.  Producer Avi Arad rejected the love triangle angle on Ock, and found Harry putting a price on Spider-Mans head unsubtle. 

Raimi sifted through the previous drafts by Gough, Millar, Koepp and Chabon, picking what he liked with screenwriter Alvin Sargent.    He felt that thematically the film had to explore Peters conflict with his personal wants against his responsibility, exploring the positive and negatives of his chosen path, and how he ultimately decides that he can be happy as a heroic figure.  Raimi stated the story was partly influenced by Superman II, which also explored the titular hero giving up his responsibilities.  The story is mainly taken from The Amazing Spider-Man No. 50, "Spider-Man No More!" It was decided that Doc Ock would be kept as the villain, as he was both a visually interesting villain who was a physical match for Spider-Man, and a sympathetic figure with humanity.  Raimi changed much of the characters backstory, however, adding the idea of Otto Octavius being a hero of Peter, and how their conflict was about trying to rescue him from his demons rather than kill him. 

===Filming===
  the Loop in Chicago during two days in November 2002. The crew bought a carriage, placing sixteen cameras for background shots of Spider-Man and Doc Ocs train fight.  Principal photography began on April 12, 2003 in New York City. The crew moved on May 13 to Los Angeles,  shooting on ten major sets created by production designer Neil Spisak. After the scare surrounding his back pains, Tobey Maguire relished performing many of his stunts, even creating a joke of it with Raimi, creating the line "My back, my back" as Spider-Man tries to regain his powers.  Even Rosemary Harris took a turn, putting her stunt double out of work. In contrast, Alfred Molina joked that the stunt team would "trick" him into performing a stunt time and again. 

Filming was put on hiatus for eight weeks, in order to build Doc Ocks pier lair. It had been Spisaks idea to use a collapsed pier as Ocks lair, reflecting an exploded version of the previous lab and representing how Octavius life had collapsed and grown more monstrous,  evoking the cinema of Fritz Lang and the film The Cabinet of Dr. Caligari.  Filming then resumed on that set, having taken fifteen weeks to build, occupying Sonys Stage 30. It was   by   long, and   high, and a quarter-scale miniature was also built for the finale as it collapses.  Filming was still going after Christmas 2003. 

A camera system called the Spydercam was used to allow filmmakers to express more of Spider-Mans world view, at times dropping fifty stories and with shot lengths of just over   in New York or   in Los Angeles. For some shots the camera would shoot at six frames per second for a faster playback increasing the sense of speed. Shots using the Spydercam were pre-planned in digital versions of cities, and the cameras movement was controlled with motion control, making it highly cost-effective. The camera system was only used in the previous film for the final shot. 

===Effects===
Although roughly the same, costume designer James Acheson made numerous subtle changes to Spider-Mans costume. The colors were made richer and bolder, the spider emblem was given more elegant lines and enlarged, the eye-lenses were somewhat smaller, and the muscle suit underneath was made into pieces, to give a better sense of movement. The helmet Maguire wore under his mask was also improved, with better movement for the false jaw and magnetic eye pieces, which were easier to remove. 

To create   June 2007: p. 30–31. 

Edge FX was only hired to do scenes where Octavius carries his tentacles. CGI was used for when the tentacles carry Octavius: a   high rig held Molina to glide through his surroundings, with CG tentacles added later.    The CG versions were scanned straight from the practical ones.  However, using the practical versions was always preferred to save money,  and each scene was always filmed first with Edge FXs creations to see if CGI was truly necessary. Completing the illusion, the sound designers chose not to use servo sound effects, feeling it would rob the tentacles of the sense that they were part of Octavius body, and instead used motorcycle chains and piano wires. 

==Release==

===Home media===
The film was initially released on DVD as a two-disc special edition on November 30, 2004. It was available in both anamorphic widescreen and Pan-and-Scan "fullscreen", as well as a Superbit edition and in a box-set with the first film. There was also a collectors edition including a reprint of The Amazing Spider-Man #50. 

An extended cut of the film, with eight minutes of new footage, was released as Spider-Man 2.1 on DVD and Blu-ray Disc on April 17, 2007 and on October 30, 2007. In addition to the new cut, the DVD also included new special features not on the original release, as well as a sneak preview of Spider-Man 3. 

The film was released on   as part of Sonys Blu-ray Essentials Collection.  The Spider-Man series was re-released on Blu-ray with a different audio transfer on June 12, 2012. 

==Reception==

===Box office=== Harry Potter and the Prisoner of Azkaban. Spider-Man 2 is the twenty-third highest grossing film in the U.S. and Canada. 

===Critical reception=== average score normalized rating out of 100 to reviews from mainstream critics, calculated an average score of 83, based on 41 reviews.    The film was placed at   411 on Empire (film magazine)|Empire magazines top 500 movies list. 

Chicago Tribunes Mark Caro stated that Alfred Molina was a "pleasingly complex" villain, and the film as a whole "improves upon its predecessor in almost every way."  Kenneth Turan, of the Los Angeles Times, gave the film 4 out of 5 stars, and concurred with Caro when he stated, "Doc Ock grabs this film with his quartet of sinisterly serpentine mechanical arms and refuses to let go."  Roger Ebert, who had given the first film two and a half stars, gave Spider-Man 2 a perfect four out of four stars, calling it "The best superhero movie since the modern genre was launched with Superman (1978 film)|Superman (1978)", and praising the film for "effortlessly   special effects and a human story, keeping its parallel plots alive and moving."  He later called it the fourth best film of 2004."  IGNs Richard George felt "Sam Raimi and his writing team delivered an iconic, compelling version of Spider-Mans classic foe... We almost wish there was a way to retroactively add some of these elements to the original character." 

Conversely, J. Hoberman, of The Village Voice, thought the first half of the film was "talky bordering on tiresome", with the film often stopping to showcase Raimis idea of humor.  Charles Taylor believed, "The scripts miscalculation of Peters decision feeds into the pedestrian quality of Raimis direction and into Maguires weightlessness...   simply does not suggest a heroic presence", and suggested that "Dunst appears to be chafing against strictures she cannot articulate." 

===Awards and nominations===
  Academy Award Best Visual Best Sound Kevin OConnell, Best Sound Best Actor, Best Director, Best Fantasy Best Special Best Writer, Best Supporting Best Music. Special Visual AFI listed top 10 100 most 100 greatest American films. 

{| class="wikitable plainrowheaders"  style="text-align:center; width:99%;"
|+
! scope="col" | Award
! scope="col" | Date of ceremony
! scope="col" | Category
! scope="col" | Recipients and nominees
! scope="col" | Outcome
|-
| rowspan="3"| Academy Awards  February 27, 2005 Best Sound Editing
| Paul N.J. Ottosson
|  
|- Best Sound Mixing Kevin OConnell, Greg P. Russell, Jeffrey J. Haboush and Joseph Geisinger
|  
|- Best Visual Effects
| John Dykstra, Scott Stokdyk, Anthony LaMolinara and John Frazier
|  
|- American Film Institute Awards  2005
! scope="row" style="text-align:center"| Movie of the Year
| Spider-Man 2
|  
|- BMI Film and TV Awards 
| May 18, 2005
! scope="row" style="text-align:center"| BMI Film Music Award
| Danny Elfman
|  
|-
| rowspan="3"| British Academy Film Awards  February 12, 2005 Best Achievement in Special Visual Effects
| John Dykstra, Scott Stokdyk, Anthony LaMolinara and John Frazier
|  
|-
! scope="row" style="text-align:center"| BAFTA Award for Best Sound
| Paul N.J. Ottosson, Kevin OConnell, Greg P. Russell and Jeffrey J. Haboush
|  
|-
! scope="row" style="text-align:center"| Orange Film of the Year
| Spider-Man 2
|  
|-
| rowspan="2"| Broadcast Film Critics Association Awards  January 10, 2005
! scope="row" style="text-align:center"| Best Family Film
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Best Popular Movie
| Spider-Man 2
|  
|-
| Cinema Audio Society Awards 
| February 19, 2005
! scope="row" style="text-align:center"| Outstanding Achievement in Sound Mixing for Motion Pictures
| Joseph Geisinger, Kevin OConnell, Greg P. Russell and Jeffrey J. Haboush
|  
|-
| rowspan="2"| Empire Awards     March 13, 2005
! scope="row" style="text-align:center"| Best Actor
| Tobey Maguire
|  
|-
! scope="row" style="text-align:center"| Best Director
| Sam Raimi
|  
|-
| Golden Trailer Awards 
| May 25, 2004
! scope="row" style="text-align:center"| Summer 2004 Blockbuster
| Spider-Man 2
|  
|-
| Hugo Awards 
| August 7, 2005
! scope="row" style="text-align:center"| Best Dramatic Presentation – Long Form
| Spider-Man 2
|  
|-
| London Critics Circle Film Awards  February 9, 2005
! scope="row" style="text-align:center"| British Supporting Actor of the Year
| Alfred Molina
|  
|-
| rowspan="3"| MTV Movie Awards  June 4, 2005
! scope="row" style="text-align:center"| Best Action Sequence
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Best Movie
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Best Villain
| Alfred Molina
|  
|-
| rowspan="4"| Peoples Choice Awards 
| rowspan="4"| January 9, 2005
! scope="row" style="text-align:center"| Favorite Motion Picture
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Favorite On-Screen Chemistry
| Kirsten Dunst and Tobey Maguire
|  
|-
! scope="row" style="text-align:center"| Favorite Sequel
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Favorite Villain Movie Star
| Alfred Molina
|  
|-
| rowspan="8"| Satellite Awards  December 17, 2005
! scope="row" style="text-align:center"| Best Actor in a Supporting Role, Drama
| Alfred Molina
|  
|-
! scope="row" style="text-align:center"| Best Cinematography
| Bill Pope and Anette Haellmigk
|  
|-
! scope="row" style="text-align:center"| Best DVD Extra
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Best Film Editing
| Bob Murawski
|  
|-
! scope="row" style="text-align:center"| Best Original Score
| Danny Elfman
|  
|-
! scope="row" style="text-align:center"| Best Overall DVD
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Best Sound (Editing & Mixing)
| Kevin OConnell, Greg P. Russell, Jeffrey J. Haboush, Joseph Geisinger, Paul N.J. Ottosson and Susan Dudeck
|  
|-
! scope="row" style="text-align:center"| Best Visual Effects
| John Dykstra, Scott Stokdyk, Anthony LaMolinara and John Frazier
|  
|-
| rowspan="8"| Saturn Awards  May 3, 2005
! scope="row" style="text-align:center"| Best Fantasy Film
| Spider-Man 2
|  
|-
! scope="row" style="text-align:center"| Best Actor
| Tobey Maguire
|  
|-
! scope="row" style="text-align:center"| Best Supporting Actor
| Alfred Molina
|  
|-
! scope="row" style="text-align:center"| Best Director
| Sam Raimi
|  
|-
! scope="row" style="text-align:center"| Best Writer
| Alvin Sargent
|  
|-
! scope="row" style="text-align:center"| Best Music
| Danny Elfman
|  
|-
! scope="row" style="text-align:center"| Best Special Effects
| John Dykstra, Scott Stokdyk, Anthony LaMolinara and John Frazier
|  
|-
! scope="row" style="text-align:center"| Best DVD Special Edition Release
| Spider-Man 2
|  
|-
| rowspan="6"| Visual Effects Society Award 
| rowspan="6"| February 16, 2005
! scope="row" style="text-align:center"| Best Single Visual Effect of the Year
| John Dykstra, Lydia Bottegoni, Dan Abrams and John Monos
|  
|-
! scope="row" style="text-align:center"| Outstanding Compositing in a Motion Picture
| Colin Drobnis, Greg Derochie, Blaine Kennison and Ken Lam
|  
|-
! scope="row" style="text-align:center"| Outstanding Created Environment in a Live Act on Motion Picture
| Dan Abrams, David Emery, Andrew Nawrot and John Hart
|  
|-
! scope="row" style="text-align:center"| Outstanding Performance by an Actor or Actress in a Visual Effects Film
| Alfred Molina
|  
|-
! scope="row" style="text-align:center"| Outstanding Special Effects in Service to Visual Effects in a Motion Picture
| John Frazier, James D. Schwalm, James Nagle and David Amborn
|  
|-
! scope="row" style="text-align:center"| Outstanding Visual Effects in a Visual Effects Driven Motion Picture
| John Dykstra, Lydia Bottegoni, Anthony LaMolinara and Scott Stokdyk
|  
|-
| rowspan="3"| World Stunt Awards 
| rowspan="3"| September 25, 2005
! scope="row" style="text-align:center"| Best Overall Stunt by a Stunt Man
| Chris Daniels and Michael Hugghins
|  
|-
! scope="row" style="text-align:center"| Best Specialty Stunt
| Tim Storms, Garrett Warren, Susie Park, Patricia M. Peters, Norb Phillips, Lisa Hoyle, Kevin L. Jackson and Clay Donahue Fontenot
|  
|-
! scope="row" style="text-align:center"| Best Work with a Vehicle
| Tad Griffith, Richard Burden, Scott Rogers, Darrin Prescott and Mark Norby
|  
|}

==Legacy==
Despite the many super-hero movies which have followed after it, Spider-Man 2 still regularly tops rankings as one of the best-loved of the genre.        In 2012, Ask Men wrote, "This is the high-water mark for Spider-Man movies, and good luck to anyone who wants to top it." 

In 2013, Screen Crush wrote,"Sam Raimis second outing with the web-slinging hero is as perfect as superhero movies get, nailing everything thats great about its hero without sacrificing the unique tone established by the first film. How exactly does Raimi pull off a movie thats simultaneously goofy, melancholy, romantic, frightening, melodramatic, crazily intense and emotionally fulfilling? Some kind of cinematic alchemy, apparently."  Forbes described it as "Not just one of the greatest sequels, but one of the best films of the genre, period." 

In 2014, Yahoo Movies! wrote, "Raimis best superhero movie still takes the cake." 
 The Dark Knight at 94% on rottentomatoes.com.
In 2007, Entertainment Weekly named it the   21 greatest action film of all time. 

==Sequel==
 

On 13 March 2007, Sony released a teaser trailer suggesting a new film in the Spider-Man trilogy. On 18 March 2007, Sony officially announced a sequel, titled Spider-Man 3.

==References==
 

==External links==
 
* 
* 
* 
* 
* 
*  at Marvel.com

 
 
 
 
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 