Ab Tak Chhappan 2
{{Infobox film
| name           = Ab Tak Chhappan 2
| image          = Ab_Tak_Chhappan_2_Official_Poster.jpg
| caption        = Theatrical Release Poster
| director       =Aejaz Gulab
| producer       =Raju Chada Gopal Dalvi
| writer         = Nilesh Girkar
| starring       = Nana Patekar Ashutosh Rana Vikram Gokhale Gul Panag
| music          = Amal Mallik
| cinematography = Siddharth More
| editing        = Vinay Chauhan  Abhijith Kokate
| studio         =Alumbra Entertainment
| distributor    =Wave Cinemas Ponty Chadha (worldwide)
| released       = 27th February 2015
| runtime       = 105 minutes 
| country        = India Hindi Marathi Marathi
| budget         = 
| gross          =    
}}
Ab Tak Chhappan 2 (English: So Far Fifty Six II) is a 2015 Indian crime film produced by Raju Chada and Gopal Dalvi. The film is directed by Aejaz Gulab and scripted by Nilesh Girkar. The film stars Nana Patekar in the lead role. It also stars Gul Panag, and Ashutosh Rana. The film is a sequel to Ab Tak Chhappan. The film was released on 27 February 2015 and met with mixed to positive reviews.   The story revolves around Inspector Sadhu Agashe (Nana Patekar) from the Mumbai Encounter Squad.  It is inspired by the life of Police sub-Inspector with Mumbai Police force Daya Nayak. 

==Plot==
Ab Tak Chhappan 2 opens to encounter specialist Sadhu Agashe (Nana Patekar) proclaiming what he did was right. The film then goes into flashback, where we are shown what happened a few months back. Away from Mumbai, with a few cases pending against him, Sadhu now leads a peaceful life in his village (Goa) with his son, mourning the loss of his wife. He cooks, lives in a house by the river and listens to his son playing the piano. However, his retired phase comes to a halt, when ex-police commissioner (Mohan Agashe) convinces Sadhu to get back on the job on the home ministers (Vikram Gokhale) request to tackle Mumbais escalating crime scene.

Sadhu is hesitant but his son talks him into accepting the offer, which brings him back to Mumbai. While Sadhus encounter squad is happy to see him, Thorat (Ashutosh Rana), Sadhus junior is not too pleased with the decision as he hopes to become the chief. Sadhu senses the rivalry and makes it clear to Thorat that he will have to follow orders.

The squad begins their mission by reconnecting with their sources and targeting gangsters of two gangs (Rauf and Rawale). Meanwhile crime reporter Shalu Dixit (Gul Panag) becomes a regular visitor at Sadhus residence, since she wants to finish the book on encounter cops, which her journalist father couldnt complete, since he was shot by a gangster.

As the encounter squad goes on a shooting spree, Sadhu starts getting threats from unknown parties. Soon, Sadhus son is targeted by the gangsters and is killed. Rawale informs Sadhu that Rauf has done it. Sadhu nabs Rauf. He initially denies being the culprit but later accepts it. However, he also warns Sadhu that Rawale has been assigned the job of killing the righteous CM (Dilip Prabhawalkar) by someone, who Sadhu trusts a lot. Before Rauf could give away the name, he gets shot by Thorat. The latter says he was ordered to do so. It is soon announced that Rawale would be returning to Mumbai and may join politics soon. Both Sadhu and Shalu suspects that the home minister (Vikram Gokhale) is involved in the CMs assassination and was the one who took Rawales help to plot the murder.

Sadhu confronts the home minister and the later admits it, saying his political career wouldnt have taken off in the presence of the CM, who didnt favour him and his devious activities. He also dares Sadhu to expose him, having a firm opinion that no one would believe Sadhu.

Sadhu decides to give up but Shalu tells him that she has found evidence against the home minister, a video proof of his meeting with Rawale but before she could pick up the CD, she gets shot by Thorat, who is now revealed to be Jagirdars Henchman. Sadhu arrives at nick of time and shoots Thorat dead before he can kill Shalu.

Sadhu goes to an event, where the home minister is to pay tribute to the late CM. Sadhu steps on stage, requests he be given an opportunity to speak. After appreciating the HM, he quickly moves towards him and stabs a sharp pen into his shoulder, thus killing him.

The film continues at present time, and it is revealed that Aman is alive, his death was merely faked to eliminate the underworld. Commissioner Pradhan leaves after interrogating Sadhu, and promises him that he will take care of Aman. An epilogue is shown where we are told that Sadhu was given life imprisonment for also killing Rawale in same jail where both were imprisoned together.

==Cast==
*Nana Patekar as Inspector Sadhu Agashe
*Gul Panag as Reporter Shalu Dixit
*Tanmay Jahagirdar as Aman,Sadhu Agashes son
*Mohan Agashe as Ex Commissioner Pradhan
*Govind Namdev as Police Commissioner Bhandare
*Raj Zutshi as Rawle, Brother of Zameer (Antagonist from First Film).
*Vikram Gokhale as Home Minister Jagirdar
*Dilip Prabhavalkar as Chief Minister Anna Saheb
*Ashutosh Rana as Suryakant Thorat, Sadhus Immediate Junior
*Revathi as Mrs Sadhu Agashe ( Footage from first film)
*Pradeep Kabra as Rawles Henchman assigned to kill Sadhu Agashe.

==Critical reception==

The film received negative reviews from critics.

==References==
 

==External links==
 
*  
* 

 
 
 
 


 