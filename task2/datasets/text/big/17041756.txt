Freelance (2007 film)
 

{{Infobox Film
| name           = FreeLance
| image          = FreeLancePoster.jpg
| caption        = Theatrical poster
| director       = Drew Sawyer
| producer       = Jason Clairy   Jason Fordham
| writer         = Sean Mann
| starring       = Bob Kunkel   Sean Mann   Jackson Williamson
| music          = Robb Stregowski
| cinematography = Robert Mallin   Drew Sawyer
| editing        = Drew Sawyer   Sean Mann
| released       = 2007
| runtime        = 63 minutes
| country        = United States
| language       = English
}}
FreeLance is a 2007 low-budget independent film directed by Drew Sawyer and written by Sean Mann and Drew Sawyer.
It was filmed in Rome, Georgia  and premiered at the Rome International Film Festival.  Later that year, it showed at the Atlanta Underground Film Festival.

==Plot==

Elliot Guilespie (Bob Kunkel) is a smalltown twentysomething with aspirations of stardom in television news as his alter-ego, Lance Windchaser.  He idolises the local TV news anchor,
Rod Reel (Tim Hensely), who is intermittently shown reporting on the spree of a local serial killer known as the Rubiks Cube Killer, or "RCK".

Elliots home life is less than ideal; he still lives with his mother and never knew his father, who was killed in a "freak hospital accident". Compounding Elliots frustrating
life is Roy-Henry Ringold (Sean Mann), who terrorized Elliot in high school, but is now a recovering alcoholic and desperately seeks his forgiveness. Roy-Henrys quest for forgiveness is conflicted with the fact that he is sleeping with Elliots mother and feels compelled to act as a father figure to the aimless Elliot. As if that werent enough, Elliot works for Roy-Henrys landscaping company.
 depressed family man who vents to an unwilling Elliot about the horrors of his hen-pecked homelife.

After a disastrous attempt at handling the filming and the reporting simultaneously, Elliot
enlists Toby as his cameraman. Desperate for friendship and an opportunity to escape his
horrible marriage, Toby happily agrees and the two begin a whirlwind escapade of [[freelance
journalism]] marked largely by failure, ridicule, and numerous injuries.

Things start to look up for Elliot when Roy-Henry is killed in a hit and run accident. Elliot is overjoyed to be free of Roy-Henrys smothering presence, but his newfound happiness is quickly shot down by his idol Rod Reel, who mercilessly ridicules Elliots work. Elliot becomes unhinged as he realises his only shot at making the news may be to actually "make the news". Soon after his epiphany, Elliot is shown reporting suspiciously quickly on several acts of anti-Rod Reel vandalism. Further complicating things, Elliot falls in love with a prostitute (Nichole Harrison) he meets while riding along on Tobys taxi runs. Believing Elliot is interested in her services, she gives him her card. When Elliot goes to her place to see her again, he is surprised to find she has committed suicide to escape from the shame of her past misdeeds, of which, prostitution is the least troublesome. Finally snapping, Elliot decides to use her body to fake the next RCK killing and be the first to report it, thus showing up Rod Reel and proving his worth as a reporter. His plans are thwarted as Toby arrives and witnesses Elliot arranging the gruesome tableau and, believing his friend to be a killer, calls the police. The film ends with a blood soaked Elliot being dragged away into a squad car as Rod Reel ironically reports live on the scene of Elliots capture as the "RCK Killer".

==Cast==
*Bob Kunkel as Elliott Gillespie / Lance Windchaser
*Sean Mann as Roy Henry Ringold
*Jackson Williamson as Toby
*Tim Hensely as Rod Reel
*Nichole Harrison as Escort
*Deboria Arrant as Priest
*Trip Barnes as Intern
*Corly Bryant as Intern
*Mike Burton as Pineapple Man
*Bee Chidsey as Brew Stealer
*Laura Evans as Intern
*Julie Anne Franklin as Jogger
*Jon Ingram as Country Gentleman
*Prerry Lucas as Brew Stealer
*Rebecca Maddox as Mourner / Cookie Lady
*Sarah Pruett as Intern
*James Schroeder as Park Avenger
*Melissa Smathers as Mrs. Guillespe
*Linda Smith as Cat Lady
*Mike Smith as Big Hole Man
*Jamie Steinbrugge as Brew Stealer
*Yokenon Zoberist as Brew Stealer
*Yoram Zoberist as Brew Provider

==Production==
 Atlanta soundman Roy Clements, known for his previous work on Space Ghost Coast to Coast and Aqua Teen Hunger Force.  Thanks to his expertise and studio time donated toward the films sound production, the actual production value of FreeLance ended up being closer to $25,000. 

==Reception==
FreeLance was well received by audiences and critics, who commended its combination of satire
and slapstick. 
It was awarded the Best Local Feature award at the Atlanta Underground Film Festival. 

==Soundtrack==
*"Hold On To Me"
**Performed by Little Country Giants

*"More Than Everything To Me"
**Performed by Little Country Giants

*"Bottle Fight"
**Performed by The Greedy Robots

*"Night Drive to Commerce"
**Performed by The Greedy Robots

*"A.M. 180"
**Performed by Grandaddy. Written and Produced by Jason Lytle. Courtesy of V2 Records

*"Tribute to a Dead Hooker"
**Written and performed by Robb Stregowski

*"Ala-non-allujah"
**Written and performed by Robb Stregowski

*"Windchaser Theme"
**Written and performed by Robb Stregowski

*"Making The News"
**Written and performed by Robb Stregowski

*"Mammoth Ride"
**Written and performed by Jumble

==References==
 

==External links==
* 
*http://freelancemovie.com/

 
 
 
 
 
 