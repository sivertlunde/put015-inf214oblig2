Rancho Deluxe
 
 
{{Infobox film
| name           = Rancho Deluxe
| image          = Rancho deluxe.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Frank Perry
| producer       = Elliott Kastner
| writer         = Thomas McGuane
| narrator       =
| starring       = Jeff Bridges Sam Waterston Elizabeth Ashley Clifton James Slim Pickens
| music          = Jimmy Buffett
| cinematography = William A. Fraker
| editing        = Sidney Katz
| distributor    = United Artists
| released       =  
| runtime        = 93 min
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Rancho Deluxe is a contemporary western film that was directed by Frank Perry and released in 1975.  Jeff Bridges and Sam Waterston star as two cattle rustlers in modern-day Livingston, Montana who plague a wealthy ranch owner, played by Clifton James.
 Richard Bright, Elizabeth Ashley and Slim Pickens as the aging detective Harry Beige hired to find the rustlers.

Jimmy Buffett contributed the music, and performed "Livingston Saturday Night" with alternate lyrics within the film in a scene set at a country/western bar.

The script was by novelist Thomas McGuane.

==Plot==
Jack McKee and Cecil Colson are a couple of young, restless rustlers. Jack has turned his back on his wealthy family and his wife. Cecil is a Native American. Together, more out of boredom than anything else, they have begun rustling cattle, cutting them up with a chainsaw and paying bills with fresh meat in lieu of cash.

Equally bored are wealthy Montana rancher John Brown and his wife, Cora. They once ran a beauty parlor in Schenectady, New York, but now they have bought up most of the land in this corner of Montana. Cora is so bored that she tries to catch the eye of her husbands dim ranch hands, Burt and Curt but she cant seem to work up much interest on their part.

The rustling of his livestock lights a fire under Brown, who sends Burt and Curt up in a helicopter to try to catch the thieves in the act. Jack and Cecil continue to single out Browns cattle, even kidnapping his $50,000 prize bull, Basehart of Bozeman Canyon, for ransom.

Brown decides to call upon Henry Beige, said to be the scourge of rustlers everywhere. A legendary stock detective who once served time on a prison farm for rustling, Beige turns out to be a feeble old fool who doesnt seem to be interested in anything except watching TV and being waited on hand and foot by his niece, Laura, who is almost sickeningly sweet.

Jack and Cecil are feeling cocky, so much so that when Burt and Curt figure out that they must be the rustlers, Jack and Cecil bribe them into a scheme to steal a semi-truck full of John Browns cattle.

Curt, however, has fallen head over heels in love with the luscious Laura, even though she still mistakenly calls him Burt. She is nowhere near as innocent as she seems, as she proves in a sexual encounter in the woods. Burt intends to use his rustling profits to take an expensive vacation in Mexico, but Curt has chosen to propose marriage to Laura.

Henry Beiges ineptitude and uninterest in identifying the rustlers is infuriating to Brown, who angrily fires him. A distressed Laura explains to Curt that she needs to take care of her uncle and therefore will be leaving with him, unable to marry Curt.

Curt decides to help Henry catch the rustlers instead. Henry proceeds to do exactly that, making a show of it before the towns citizens. Burt and Curt are also arrested, Curt coming to realize that Lauras sweetness and love for him was all an act.

Henry Beige comes to Brown to say goodbye, nonchalantly accepting his payment because he says hes in it now simply for the sport. Brown can see now that Henry is shrewd, not doddering at all, and Laura is a sexy, all-business woman, not innocent in any way.

Jack and Cecil end up sent to the Montana State Prison Ranch at Deer Lodge, presumably the same prison where Henry Beige served time in his youth. They spend their days on horseback, seemingly no more or less bored than they had been before. The final scene shows the two rustlers riding under a sign reading "Rancho Deluxe".

==Cast==
*Jeff Bridges as Jack McKee
*Sam Waterston as Cecil Colson
*Elizabeth Ashley as Cora Brown
*Clifton James as John Brown
*Slim Pickens as Henry Beige
*Charlene Dallas as Laura Beige
*Harry Dean Stanton as Curt Richard Bright as Burt
*Patti DArbanville as Betty Fargo
*Maggie Wellman as Mary Fargo
*Bert Conway as Wilbur Fargo
*Joe Spinell as Mr. Colson, Cecils father

==Reaction==
The film was described as a form of "parody Western" by critic Richard Eder in his Nov. 24, 1975 New York Times review. "It is so cool that it is barely alive," he wrote of the films general tone. Among the positive elements: "Slim Pickens for once has a strongly written comic role. He plays it with great effect, and Charlene Dallas, as his sluttish assistant, is almost as good."

Roger Ebert of the Chicago Sun-Times gave Rancho Deluxe only one-and-a-half out of four possible stars. He wrote: "I dont know how this movie went so disastrously wrong, but it did."

==See also==
*Rancho Deluxe (soundtrack)|Rancho Deluxe (soundtrack)

==External links==
* 

 

 
 
 
 
 
 
 
 