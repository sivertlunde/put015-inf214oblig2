Carne (film)
{{Infobox film
| name           = Carne
| image          = 
| caption        = 
| director       = Gaspar Noé
| producer       = 
| writer         = Gaspar Noé
| starring       = Philippe Nahon Blandine Lenoir
| music          = 
| cinematography = Dominique Colin
| editing        = Lucile Hadžihalilović
| studio         = Les Cinémas de la Zone
| distributor    = Action Gitanes
| released       =  
| runtime        = 40 minutes
| country        = France
| language       = French
| budget         = 
}} I Stand Alone.

==Plot==
A nameless horse butcher, whose wife left him soon after their mute daughter was born, operates his own business while trying to raise the daughter. Despite the fact that she has become a teenager, the butcher continues to wash her like a baby, and struggles to resist the temptation of committing incest. On the day of the daughters menstruation, the butcher misinterprets the situation and assumes that she has been raped by a worker, whom he immediately seeks out and stabs as revenge. The butcher is imprisoned for the assault and is forced to sell his butcher shop and apartment.

==Cast==
* Philippe Nahon as the butcher
* Blandine Lenoir as the butchers daughter
* Frankye Pain as the butchers mistress
* Hélène Testud as maid

==Production== 16 mm 35 mm.   

==Release==
The film premiered in the short film section of the 1991 International Critics Week in Cannes.  It won the top prize in its section, as well as the Georges Sadoul Prize and the Prix Très Special.  Carne was eventually given a theatrical release in France and started a trend of theatrical distribution for films with similar length. 

==Anecdote==

In the fourth episode of the second season of the TV show The Wire, Johnny Weeks tells to Bubbles the beginning of the movie Carne.

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 

 