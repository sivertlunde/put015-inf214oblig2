Aar Paar
 
{{Infobox film
| name           = Aar-Paar
| image          = Aarpaar.jpg
| image size     =
| caption        =
| director       = Guru Dutt
| producer       = Guru Dutt
| writer         = Abrar Alvi  (dialogue)  Nabendu Ghosh  (screenplay)  Majrooh Sultanpuri (lyrics)
| narrator       = Johnny Walker Shakila Jagdeep
| music          = O.P. Nayyar
| cinematography = V.K. Murthy
| editing        = Y. G. Chawhan
| distributor    =
| released       = 1954
| runtime        =
| country        = India
| language       = Hindi, Urdu
| budget         =
| preceded by    =
| followed by    =
}} 1954 Indian Johnny Walker, Shyama, Shakila (actress)|Shakila, Jagdeep, Jagdish Sethi, Bir Sakuja, Rajendra, Amir Banu, Rashid Khan, M.A. Latif.

==Plot==
Kalu is a taxi-driver in Mumbai, India. He has two women who love him and would like to marry him. Kalu first wants to establish himself and become rich, before he can even think of marriage.

==Cast==
*Shyama as Nikki
*Guru Dutt as Kalu
*Jagdish Sethi as Lalaji Johnny Walker as Rustom
*Jagdeep as Elaichi Sandow Shakila as Dancer
*Bir Sakhuja
*Rajendra
*Amir Banu
*Rashid Khan
*M.A. Lateef

==Songs==
All songs composed by O.P. Nayyar.
{| class="wikitable"
|-
! Song !! Singer(s)
|-
| "Sun Sun Sun Sun Zaalima"
| Geeta Dutt and Mohammed Rafi
|-
| "Ja Ja Ja Ja Bewafa"
| Geeta Dutt
|-
| "Babuji Dheere Chalna"
| Geeta Dutt
|-
| "Ye Lo Main Haari Piya"
| Geeta Dutt
|-
| "Hoon Abhi Main Jawaan Aye Dil"
| Geeta Dutt
|-
| "Mohabat Karlo Ji Barlo"
| Geeta Dutt and Mohammed Rafi
|-
| "Na Na Na Na Na Na Tauba Tauba"
| Geeta Dutt and Mohammed Rafi
|-
| "Kabhi Aar Kabhi Paar"
| Shamshad Begum
|}

==External links==
* 

 
 
 
 

 