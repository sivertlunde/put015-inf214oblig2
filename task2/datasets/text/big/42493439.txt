Daring Lover
{{Infobox film
| name = Daring Lover
| image =Daring Lover.jpg
| director = Badiul Alam Khokon
| writer =  Komol Sorkar
| story = Boopathy Pandian
| producer = S M Film International
| studio = BFDC
| starring = Shakib Khan Apu Biswas Misa Sawdagar
| music = Ali Akram Shuvo
| cinematography =
| editor =
| distributor    = S M Film International
| released =  
| runtime        =
| country = Bangladesh
| language       = Bengali
| budget =
| gross =
}} Dhallywood film directed by Badiul Alam Khokon. starring Shakib Khan and Apu Biswas in lead roles, while Misa Sawdagar, Probor Mitro and Rehana Jolly play other pivotal roles.    This is a remake of 2006 Tamil language comedy film Thiruvilaiyaadal Aarambam. Upon release, the film received massive response and is said to contribute to record breaking sales in some renowned theaters. 

==Plot==
Raja (Shakib Khan) is a brilliant man who wants to become a Realtor, much to the chagrin of his father (Probir Mitro), who doesnt support his goal. Eventually Raja wins the heart of a rich girl, Priya (Apu Biswas), sister of a business tycoon and villainous Guru (Misha). He realizes his ambition in real estate. The Guru wants him to drop his sister; Raja in return demands money. The villain gives him a check from a dubious bank. However, Raja gets his money as he blackmails the heroines brother, threatening to expose some photos. The villain also plans an IT raid on Rajas companies. However, that plan backfires. Then Guru tries to get his sister married to another, but the hero marries her. The story ends with Raja returning all the money taken from the Guru, who finally accepts the marriage.

==Cast==
* Shakib Khan as Raja
* Apu Biswas as Priya
* Misa Sawdagar as Priyas brother
* Probor Mitro as Rajas father
* Rehana Jolly as Rajas mother
* Ilish Kobra
* Ratan as Rajas friend

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 