Shishira (film)
{{Infobox film
| name           = Shishira
| image          =
| caption        =
| director       = Manju Swaraj
| producer       = B Mahadev and BT Manju
| writer         =
| narrator       = Prema
| music          = Ajaneesh Lokhanath
| cinematography = Suresh Babu
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Kannada
| budget         =
| gross          =
}} Prema in an important role. 

==Cast==
* Yashas Prema
* Meghana
* Santhosh

==Soundtrack==
{{Infobox album
| Name        = Shishira
| Type        = Soundtrack
| Artist      = Ajaneesh Lokhanath
| Cover       =
| Released    = 2009
| Recorded    =
| Genre       = Film Soundtrack
| Length      =
| Label       =
| Producer    = B.MAHADEV
| Last album  =
| This album  =
| Next album  =
}}

Ajaneesh Lokhanath has scored the music.  
{|class="wikitable"
! No. !! Song title !! Singers !! Lyrics || Length
|-
| 1 || "Elliruve Hegiruve" || K. S. Chithra || K. Kalyan || 04:57
|-
| 2 || "Tumbu Hunnimeyali" || K. S. Chithra || Krishnegowda || 03:45
|-
| 3 || "Vidyege Dasanagu" || Ajaneesh Lokanath, Chaitra H. G. || Nagathihalli Chandrashekar || 05:25
|-
| 4 || "Ello Ello" || Vijay Yesudas || Manju Swaraj || 03:39
|-
| 5 || "Kamana Bille"|| Baby Harshika, Baby Rakshita Bhaskar || Krishnegowda || 04:42
|-
| 6 || "Kamana Bille"(female) || Lakshmi Manmohan || Krishnegowda || 04:41
|-
|}

==Reception==
The film got mostly positive reviews from the media.  The director has been able to extract a fine performance from new face Yashas and has been greatly helped by the special effects team. The background score of first-time music director Ajneesh Lokanath and camerawork of Suresh Babu helps the director handle the subject well. Actress Meghna too came up with a good performance. Prema, who returns to the film industry after a while, suits the role perfectly. The tall villain Santosh is quite frightening in the film.

==References==
 

 
 
 


 