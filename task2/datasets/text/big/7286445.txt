The Witches (1966 film)
 
 
 
 
{{Infobox film
| name = The Witches
| image = The Witches poster.jpg
| caption = Theatrical release poster
| director = Cyril Frankel
| producer = Anthony Nelson Keys
| writer = Peter Curtis aka Norah Lofts
| screenwriter = Nigel Kneale
| based on = The Devils Own (novel)
| starring = Joan Fontaine Kay Walsh Alec McCowen Ann Bell Ingrid Boulting (billed as Ingrid Brett)
| music = Richard Rodney Bennett Arthur Grant
| editing = Chris Barnes James Needs
| studio = Hammer Film Productions
| distributor = Associated British-Pathé   20th Century Fox  
| released = 21 November 1966 (London) February 1967 (United States)
| runtime = 90 min.
| country = United Kingdom
| language = English
}} 1966 United British horror Hammer Films. It was adapted by Nigel Kneale from the novel The Devils Own by Norah Lofts, under the pseudonym Peter Curtis. It was directed by Cyril Frankel and starred Joan Fontaine, Alec McCowen, Kay Walsh, Ann Bell, Ingrid Boulting (billed as Ingrid Brett), Gwen Ffrangcon Davies and Rudolph Walker. This was the final big-screen film role for Fontaine.

==Plot==
 
A British schoolteacher (Joan Fontaine) travels to Africa to work as a missionary. She has a nervous breakdown after being exposed to witchcraft during a rebellion led by witch doctors.  Returning to England to recover, she is hired by wealthy siblings Alan and Stephanie Bax (Alec McCowen and Kay Walsh) to become head teacher of the small private school in their rural village.

Gwen soon detects a sinister undercurrent beneath the pleasantries of the village life, starting with Alans admission that he is not really a priest. Soon more suspicious events occur, including the reappearance of a missing doll without its head.

She also becomes suspicious of the way the villagers are treating Linda Rigg, a 14-year-old girl (Ingrid Boulting). Her investigations point to witchcraft.

==Cast==
*Joan Fontaine as Gwen Mayfield
*Kay Walsh as Stephanie Bax
*Alec McCowen as Alan Bax
*Ann Bell as Sally Benson
*Ingrid Boulting as Linda Rigg (as Ingrid Brett) John Collin as Dowsett
*Michele Dotrice as Valerie Creek
*Gwen Ffrangcon-Davies as Granny Rigg (as Gwen Ffrangcon-Davies)
*Duncan Lamont as Bob Curd
*Leonard Rossiter as Dr. Wallis Martin Stephens as Ronnie Dowsett
*Carmel McSharry as Mrs. Dowsett
*Viola Keats as Mrs. Curd
*Shelagh Fraser as Mrs. Creek
*Bryan Marshall as Tom

==Production==
  Bray in Elstree and Martin Stephens, John Collin, score was by Richard Rodney Bennett. 

In a later magazine interview Nigel Kneale said he was dissatisfied with the way the film had turned out. Personally he found modern black magic practitioners to be fairly risible and he had intended to poke fun at the idea of an English coven. His blackly comic touches were removed by the production team, who wanted the film to be entirely serious.

==Critical reception==
 
Variety (magazine)|Variety called the film "routine entertainment".  The Hammer Story: The Authorised Biography of Hammer Films called the film "unsettling, though compromised by a hysterical climax", writing, "when The Witches strikes the right balance it ultimately succeeds as an engrossing thriller, even if it ultimately disappoints as Hammer horror." 

As of 2013, The Witches currently holds a three star rating (5.8/10) on IMDb and 40% maximum approval on Rotten Tomatoes.

==References==
 

; Sources

* 

==External links==
* 
* 
* 
* 
*  is available for free download at the Internet Archive

 
 

 
 
 
 
 
 
 
 
 
 
 