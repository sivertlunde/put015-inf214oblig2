French Film
 
{{Infobox Film
| name           = French Film
| image          = French Film.jpg
| director       = Jackie Oudney
| producer       = Rachel Connors Arvind Ethan David Judy Counihan Jonny Persey Aschlin Ditta Michael Riley (film producer)
| writer         = Aschlin Ditta
| starring       = Anne-Marie Duff Hugh Bonneville Victoria Hamilton Douglas Henshall Eric Cantona
| music          = 
| cinematography = 
| editing        = 
| distributor    = Slingshot Productions
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
}} Waterloo station and the BFI Southbank. 

==Plot==
Two relationships in North London simultaneously draw to a close. Jed, a journalist is living with his long-term girlfriend, Cheryl, with the couple regularly seeing Jeds best friend Marcus and his girlfriend, Sophie. Compared to Marcus and Sophie, Jed and Cheryls relationship is tired and dysfunctional which Jed tries to put right by suggesting marriage. When he asks her, Cheryl turns him down because of the lack of romantic chemistry in their relationship. In the meantime, Jed is preparing to interview the pretentious French film writer/director, Thierry Grimandi.  Grimandi, who lives and works on the basis that he considers himself lucky "firstly because I am French, secondly because I make movies, and thirdly because I understand love" is shown regularly to give us the benefit of his advice.

Jed and Cheryl turn to a relationship counseller but when asked if he loves Cheryl, Jed cant bring himself to say "Yes", and instead covers up by saying of course, he "loves her TO BITS".  Cheryl is unimpressed as Jed cannot declare his love without adding a qualification, and as they begin to drift apart, she rediscovers the pleasures of being more independent. Jed looks to his friend, Marcus for help and for advice from Grimandi who tries to school him in the French philosophy of love.  The scenes as Jed travels around London on his bicycle cleverly give the city an appearance of Paris.  In the meantime, Marcus meets an old girlfriend, keen on travel, rediscovering his youth and finding that the flame between them burns as brightly as ever.  He resolves to leave Sophie and go away with his old flame.

Jed has long liked Sophie without realising that he was falling in love with her and even when Marcus and his new love leave London on the Eurostar to Paris, Jed lacks the courage to tell Sophie of his true feelings, even though she is aware of them herself. We see Jed undertaking the on-stage interview with Thierry Grimandi, for which he has been preparing and only then does he begin to understand his own feelings as he listens to the film directors pontifications.  Jed rushes off to see Sophie, who greets him warmly, whilst we see Cheryl enjoying her new independence.  We are left to assume that all live happily ever after.......

==Reception==
The film received an overall positive rating on the Rotten Tomatoes film review site, achieving 53% positive reviews.  Reviews to the film were mixed, but many were negative. The Daily Mail described the film as "plodding and predictable - frightfully English, and not in a good way".  The Daily Telegraph compared the dramatic function Cantona plays in the film as a man who reveals "some hidden truths - insights and wisdom denied to most men" to a similar role in the Ken Loach film Looking for Eric released at the same time. 

==References==
 

== External links ==
*  

 
 
 
 
 