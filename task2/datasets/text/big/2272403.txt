Out to Sea
{{Infobox film
| title          = Out to Sea
| image          = Out to sea poster.jpg
| caption        = Theatrical release poster
| director       = Martha Coolidge
| writer         = Robert Nelson Jacobs
| starring       = Jack Lemmon Walter Matthau Rue McClanahan Gloria DeHaven Dyan Cannon Elaine Stritch Brent Spiner John Davis David T. Friendly David Newman
| cinematography = Lajos Koltai
| editing        = Anne V. Coates
| studio         = Davis Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $30,716,901
| awards         =
}}
 1997 romantic David Newman.

==Plot summary== Holland America dance hosts and must sleep in a cramped cabin in the bowels of the ship.

Ruled over by tyrannical, control-freak cruise director Gil Godwin ("a song and dance man raised on a military base"), they do their best, despite Charlies not actually being able to dance. Each meets a lady of interest. One is the luscious heiress Liz LaBreche, whose wealth attracts Charlie every bit as much as the rest of her does. The other is lovely widow Vivian, who is under the impression that Herb is really a doctor, not a dancer. After finally telling her the truth, Herb soon finds himself quite attracted to Vivian, and eventually the feeling becomes mutual. However, because Herb still pines for the day Susie will suddenly come back to him, this conflicts his very strong feelings for Vivian, leading him to eventually stand her up on the day that they were supposed to view the rare solar eclipse together.

Herb decides against starting a new relationship with Vivian until Charlie, who also reminds Herb that Susie was his sister, long before she was Herbs wife, gets the main point across, that Susie would never want Herb to spend the rest of his life completely alone and unhappy, because as Charlie says, even though Susie is gone forever, Herb is still alive (Charlie: "Will you stop using Susie as a safety net?" Herb: "Wait a minute. Who in the hell are you to tell me...?" Charlie: "She was my sister before she was your wife. And if she were here now, shed tell ya the same thing." Herb: "Ah, but she is not here now, is she Charlie? Shes gone." Charlie: "Thats right Herb, but youre not.").

By the time Charlie literally drags ship owner Mrs. Carruthers across the dance floor, the boys arent sure if they will find true love or need to abandon ship.

==Main cast==
* Jack Lemmon - Herb Sullivan
* Walter Matthau - Charlie Gordon
* Dyan Cannon - Liz LaBreche
* Gloria DeHaven - Vivian
* Brent Spiner - Gil Godwin
* Elaine Stritch - Mavis LaBreche
* Hal Linden - Mac Valor
* Donald OConnor - Jonathan Devereaux
* Edward Mulhare - Cullen Carswell
* Rue McClanahan - Ellen Carruthers

==Reception==
The movie received an unenthusiastic review from Janet Maslin in the New York Times. She described the film as a "weak but genial romp." She credits Brent Spiner as a funny "scene-stealer" and says that Ms DeHaven is "almost as pretty" in this film as she was in Charlie Chaplins 1936 Modern Times, and says that Donald OConnors dancing "draw  a well-deserved round of applause."   However, film critics Gene Siskel and Roger Ebert gave it a "Two Thumbs Up" recommendation on their weekly show.  

The film currently holds a 38% rating on Rotten Tomatoes based on 21 reviews. 

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 