Scouts vs. Zombies
 
{{Infobox film
| name           = Scouts vs. Zombies
| image          =
| alt            =
| caption        =
| film name      =
| director       = Christopher B. Landon
| producer       = {{plainlist|
* Andy Fickman
* Betsy Sullenger
* Todd Garner
* Bryan Brucks}}
| writer         = {{plainlist|
* Emi Mochizuki
* Carrie Evans
* Lona Williams}}
| starring       = {{plainlist|
* Tye Sheridan
* Logan Miller
* Joey Morgan
* Halston Sage
* Sarah Dumont
* Patrick Schwarzenegger}}
| makeup effects       = Tony Gardner and Alterian, Inc.
| music          = Matthew Margeson
| cinematography = Brandon Trost
| editing        = Jim Page
| studio         = {{plainlist|
* Broken Road Productions
* Brucks Entertainment
* Oops Doughnuts Productions}}
| distributor    = Paramount Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Scouts vs. Zombies is an upcoming 2015 American horror film directed by Christopher B. Landon and written by Emi Mochizuki, Carrie Evans and Lona Williams. The film stars Tye Sheridan, Logan Miller and Joey Morgan. The movies makeup effects are being handled by Tony Gardner and his company, Alterian, Inc. The film is scheduled to be released on October 30, 2015 by Paramount Pictures.

== Plot ==
Three Scouts who, on the eve of their last camp out, discover the true meaning of friendship when they attempt to save their town from a zombie outbreak.

== Cast ==
* Tye Sheridan as TBA
* Logan Miller as TBA
* Joey Morgan as TBA
* David Koechner as Scout Leader Rogers
* Cloris Leachman as Ms. Fielder
* Patrick Schwarzenegger as Jeff
* Halston Sage as Kendall
* Sarah Dumont as Denise

== Production ==
On May 30, 2014, Paramount Pictures set the film for a March 13, 2015 release date.    On October 14, 2014, the film was pushed back to October 30, 2015. 

=== Filming ===
The principal photography of the film began on May 8, 2014 in Los Angeles.  

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 