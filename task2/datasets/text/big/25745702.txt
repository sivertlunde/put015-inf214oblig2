Falling Up (film)
{{infobox film
| name       = Falling Up
| image      = Falling Up.jpg
| alt        =
| caption    =
| director   = David M. Rosenthal (director)
| producer   = Craig Conolly , Joseph M. Smith , Juan A. Mas , Rich Cowan, Alex Hank, Peter Kellner, Matt Luber, Richard Salvatore, Phoebe Stephens 
| writer     = David M. Rosenthal (director) Joseph M. Smith
| narrator   = Joseph Cross Sarah Roemer Snoop Dogg Rachael Leigh Cook Claudette Lali Joe Pantoliano Mimi Rogers Annette OToole
| music          = Mark Mothersbaugh
| cinematography = Joseph Gallagher
| editing        = Conor ONeill
| studio        =
| distributor  = Anchor Bay (US & UK) Gryphon (Australia)
| released    =  
| runtime    =
| country   = United States
| language = English
| budget  =
| gross  =
}}
Falling Up (known in Australia as The Golden Door) is a 2009 romantic comedy film that was released direct-to-video in late 2009 or early 2010 depending on region.

==Plot== Joseph Cross), who suddenly finds himself the head of his family, after the death of his father, takes a leave from school to support his mother, getting a job as a New York doorman. Soon one of the buildings residents, Scarlett Dowling (Sarah Roemer), takes notice of him and soon they become romantically attracted to each other, much to the annoyance of her mother (Mimi Rogers).

==Cast== Joseph Cross as Henry OShea
* Sarah Roemer as Scarlett Dowling
* Snoop Dogg as Raul
* Rachael Leigh Cook as Caitlin O Shea
* Claudette Lali as Mercedes
* Joe Pantoliano as George
* Mimi Rogers as Meredith
* Annette OToole as Grace O Shea Daniel Newman as Jake Weaver
* Samuel Page as Buck
* Frankie Shaw as Gretchen
* Gordon Clapp as Colin OShea
* Jim Piddock as Phillip Dowling
* Peter Jason as John OShea
* Ajay Naidu as Paco
* Maria Cina as Mrs. Silverman

==Production==
 

==Home media== Showtime network, the film has a TV-14 rating for American television. {{cite web
 |url=http://www.sho.com/site/schedules/product.do?episodeid=136258&seriesid=0&seasonid=0
 |title=Falling Up: Program Details
 |publisher=Showtime |accessdate=2010-08-23
}} 
 M advisory. {{cite web
 |url=http://www.ezydvd.com.au/item.zml/807738
 |title=Golden Door, The (807738)
 |publisher=ezydvd.com.au |accessdate=2010-08-23
}} 

Falling Up was released on Region 1 DVD in January 2010 without an MPAA rating. The Region 2 DVD was released in March 2010 with a 12 certificate. {{cite web
 |url=http://www.amazon.co.uk/dp/B002T5QMBU/
 |title=Falling Up (DVD) (2009)
 |publisher=Amazon.co.uk |accessdate=2010-08-23}} 

Reviewer Amy Longsdorf wrote that although the film is predictable, "this modest charmer has a surprising delicacy and old-fashioned humanity" and is worth watching at home. {{cite news |url=http://www.northjersey.com/arts_entertainment/80976652_New_on_DVD_this_week.html
 |title=New on DVD this week
 |author=Amy Longsdorf
 |date=2010-01-08 |work=The Record
 |publisher=NorthJersey.com
 |accessdate=2010-01-09}} 

==Locations==
The exterior of the apartment building used in the movie is at 1215 5th Avenue, between 101st and 102nd Streets, just above Mt. Sinai Hospital. The Maine Monument at the entrance to Central Park at Merchants Gate is also featured in one dog walking scene in the movie.

==References==
 

==External links==
*  
*  
*   at Anchor Bay

 

 
 
 
 
 
 
 
 