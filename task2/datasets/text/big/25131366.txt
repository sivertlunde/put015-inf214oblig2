Sevasadanam
{{Infobox film
| name           = Sevasadanam
| image          = Sevasadhanam_1938film.jpg
| image_size     = 
| caption        = M. S. Subbulakshmi and S. Varalakshmi in Sevasadanam
| director       = K. Subramaniam|K. Subramanyam 
| producer       = K. Subramanyam
| writer         = Premchand K. Subramanyam
| narrator       = 
| starring       = M. S. Subbulakshmi F.G. Natesa Iyer S. G. Pattu Iyer S. Varalakshmi
| music          = Papanasam Sivan
| cinematography = Sailen Bose Ellappa Rama Rao
| editing        = Dharam Veer Singh
| distributor    = 
| released       =  
| runtime        = 210 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Tamil film directed by K. Subramaniam|K. Subramanyam .  It is one of the early Tamil films to be set in a contemporary social setting and to advocate reformist social policies. This was the first film for M. S. Subbulakshmi.               

==Production==
After the success of Balayogini (1937), director Subramanyam was encouraged to make more socially oriented films. In 1938 he decided to make a film version of Premchands novel Bazaar-e-Husn. While Balayogini was about the travails of widows, Sevasadanam dealt with domestic abuse, prostitution and womens liberation. Subramnyam wrote the screenplay himself and made the film under his Madras United Artists Corporation Banner.   The completed film was 18,900 feet in length with a run time of 210 minutes.  

==Plot==
Sevasadanam (lit. The house of Service) tells the story of an abused wife Sumathi (M. S. Subbulakshmi) who is driven out of her house by husband and into prostitution. Later she reforms her ways and devotes her life to running an institution for the children of prostitutes.

==Cast and crew==
*M. S. Subbulakshmi - Sumathi
*F.G. Natesa Iyer -Eashwara Iyer
*S. Varalakshmi
*S. G. Pattu Iyer
*Kumari Kamala
*"Jolly" Kittu Iyer
*Jayalakshmi
*Ram Pyari
*Premchand - Story
*K. Subramaniam|K. Subramanyam - Screenplay and Director
*Papanasam Sivan - Music, Lyrics
*Rajagopal Iyer - Music, Lyrics
*Sailen Bose - Cinematographer
*Ellappa - Cinematographer
*Ramarao - Cinematographer
*K. R. Sharma - Art Director
*Dharam Veer Singh - Art Director 

==Reception==
Sevadanam was released on 2 May 1938. It was a critical and commercial success.  Ananda Vikatan favourably reviewed the film on 8 May 1938:

  }}

As was the case with Balayogini, conservative Hindus were upset with Sevasadanam.   

The veteran Marxist leader N. Sankaraiah, has described Seva Sadhanam as an "unusual film" for choosing the subject of marriages between young girls and old men (which had social sanction). According to him, the film successfully broughout the "sufferings of the girl" and the "mental agony of the aged husband". Sankariah particularly appreciated  F.G. Natesa Iyers performance in the role of the old man, which he said " was impressive". Tamil film critic and historian Aranthai Narayanan observes in his book Thamizh Cinemavin Kathai (The Story of Tamil Cinema) that "Seva Sadhanam proved a turning point in the history of Tamil cinema. In the climax, the aged husband, now a totally changed man, was shown as casting aside with utter contempt his `sacred thread, which symbolises his Brahmin superiority. It came as a  stunning blow to the then Brahmin orthodoxy." 

==See also==
* Tamil films of the 1930s

==References==
 

==External links==
*  
* http://www.dhingana.com/maa-ramanan-song-sevasadanam-tamil-oldies-399aa31

 
 
 
 
 