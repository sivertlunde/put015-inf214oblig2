Eye of the Needle (film)
{{Infobox film
| name           = Eye of the Needle
| image          = Eye of the Needle.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Richard Marquand Stephen J. Friedman 
| screenplay     = Stanley Mann
| based on       =  
| starring       = {{Plainlist|
* Donald Sutherland
* Kate Nelligan
}}
| music          = Miklós Rózsa
| cinematography = Alan Hume
| editing        = 
| studio         = Kings Road Entertainment
| distributor    = United Artists
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $17,583,634
}} novel of the same title by Ken Follett, the film is about a German spy in England during World War II who discovers vital information about the upcoming Normandy landings|D-Day invasion. In his attempt to return to Germany with the information, he travels to the isolated Storm Island off the coast of Scotland to rendezvous with a U-boat, but his plans are thwarted by a young woman resident. 

==Plot== German spy nicknamed "the Needle" because of his preferred method of assassination, the stiletto. He is a coldly calculating Psychopathy#Sociopathy|sociopath, emotionlessly focused on the task at hand, whether the task is to signal a U-boat or to gut a witness to avoid exposure.

In England, he has obtained critical information on the Allies of World War II|Allies invasion plans. Trying to make his way to Nazi Germany|Germany, he is stranded by a fierce storm on Storm Island, occupied only by a woman named Lucy (Kate Nelligan), her disabled husband David, their son, and their shepherd, Tom. A romance develops between the woman and the spy, due to an estrangement between Lucy and her husband, whose accident has rendered him emotionally crippled as well.

When David discovers their guests true identity, a struggle ensues, ending with the Needle throwing him off a cliff. Lucy realizes that her lover has been lying after she chances upon her husbands dead body. "The Needle" must get to Toms radio  to report to his superiors the exact location of the D-Day invasion.  Lucy is the Allies last chance. He is reluctant to harm her, but she has no such qualms and shoots him as he tries to escape in a boat. 

Additional footage tells of Fabers activities four years before, and of Davids accident, while another ending finds Lucy receiving help from British Intelligence.

==Cast==
*Donald Sutherland as Henry Faber
*Kate Nelligan as Lucy Rose Stephen MacKenna as Lieutenant 
*Christopher Cazenove as David Rose
*Philip Martin Brown as Billy Parkin
*George Belbin as Lucys Father 
*Faith Brook as Lucys Mother 
*Barbara Graley as Davids Mother 
*George Lee as Constable 
*Arthur Lovegrove as Peterson 
*Colin Rix as Oliphant 
*Barbara Ewing as Mrs. Garden  Patrick Connor as Inspector Harris
*David Hayman as Canter 
*Ian Bannen as Inspector Godliman  John Bennett as Kleinmann
*Sam Kydd as Lock Keeper  John Paul as Home Guard Captain
*Bill Nighy as Squadron Leader Blenkinsop
*Jonathan and Nicholas Haley (twins) as Joe (David and Lucys son)

==Production==
The Storm Island scenes were shot over eight weeks on the Isle of Mull in the Inner Hebrides.    Some of the location filming was shot at Blackbushe Airport Yateley.

==Reception==
Roger Ebert "admired the movie," stating it "resembles nothing so much as one of those downbeat, plodding, quietly horrifying, and sometimes grimly funny war movies that used to be made by the British film industry, back when there was a British film industry." 

==Anachronisms==
The DKW Munga vehicle shown on the island was not built until the 1950s. The enclosed-cabin helicopter that is briefly shown toward the end of the film is also an anachronism.

==References==
 

==External links==
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 