Second Chance (1950 film)
{{Infobox film
| name           = Second Chance
| image          = 
| alt            = 
| caption        = 
| director       = William Beaudine
| producer       = Paul F. Heard
| writer         = Robert Presnell Sr.
| starring       ={{Plain list| 
*Ruth Warrick John Hubbard
*Hugh Beaumont David Holt
*Pat Combs
*Ellye Marshall
*John Holland
*Joan Carroll
*John Marston
*Jameson Shade
}}
| music          = Louis Forbes
| cinematography = Marcel Le Picard
| editing        = Albrecht Joseph
| studio         = Protestant Film Commission
| distributor    = Religious Film Association
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} English war war drama drama romance John Hubbard, David Holt in the lead roles.

== Cast ==
*Ruth Warrick as Emily Dean John Hubbard as Ed Dean
*Hugh Beaumont as Pastor Dr. Emory David Holt as Jimmy Dean
*Pat Combs as Dick Dean
*Ellye Marshall as Irene
*John Holland as Dr. Matthews
*Joan Carroll as Nurse Eva
*John Marston as Harry Decker
*Jameson Shade as Mr. Lewis
*Fay Kern as Mrs. Lewis

==Home video release==
A DVD version of the film was released in 2008 by Alpha Video Distributors.

==References==
 

== External links ==
* 

 
 
 
 


 
 