The Shop Around the Corner
{{Infobox film
| name           = The Shop Around the Corner
| image          = The Shop Around the Corner - 1940- Poster.png
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Ernst Lubitsch
| producer       = Ernst Lubitsch
| writer         = 
| screenplay     = {{Plainlist|
* Samson Raphaelson
* Ben Hecht  
}}
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Margaret Sullavan
* James Stewart
* Frank Morgan
}}
| music          = Werner R. Heymann
| cinematography = William H. Daniels
| editing        = Gene Ruggiero
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Hungarian play Parfumerie by Miklós László.     Eschewing regional politics in the years leading up to World War II, the film is about two employees at a gift shop in Budapest who can barely stand each another, not realizing theyre falling in love as anonymous correspondents through their letters.  The Shop Around the Corner is ranked #28 on AFIs 100 Years... 100 Passions, and is listed in Times All-Time 100 Movies.    In 1999, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."    The supporting cast included Joseph Schildkraut, Sara Haden, Felix Bressart, and William Tracy.   

==Plot==
Alfred Kralik (James Stewart) is the top salesman at a gift shop in Budapest owned by the high-strung Mr. Hugo Matuschek (Frank Morgan). Kraliks coworkers at Matuschek and Company include his friend, Pirovitch (Felix Bressart), a kindly family man; Ferencz Vadas (Joseph Schildkraut), a two-faced womanizer; and Pepi Katona (William Tracy), an ambitious, precocious delivery boy. One morning, Kralik reveals to Pirovitch that hes been corresponding anonymously with an intelligent and cultured woman whose ad he came across in the newspaper.
 Ochi Chërnye" when opened. Kralik thinks its a bad idea. Although annoyed with Kraliks stubbornness, Matuschek is reluctant to ignore his judgment. After their exchange, Klara Novak (Margaret Sullavan) enters the gift shop looking for a job. Kralik tells her there are no openings, but when she is able to sell one of the cigarette boxes (as a candy box), Mr. Matuschek hires her.

As Christmas approaches, Kralik is preparing to finally meet his mystery correspondent for a dinner date. Planning to propose if the date works out, Kralik requests a raise from Mr. Matuschek, who has not been in a good mood for months. Forced to put up with the pesky Miss Novak—the two simply cannot get along—Kralik is grateful that his anonymous correspondent is nothing like her. He admits to Pirovitch that he is nervous about meeting this "most wonderful girl in the world" for the first time.

Kraliks planned meeting is interrupted when Mr. Matuschek demands that everyone stay after work. He and Kralik argue when Kralik mentions his previous engagement. Later Kralik is called into Mr. Matuscheks office—and is fired. No one in the shop understands Mr. Matuscheks actions; they do not know that Mr. Matuschek suspects Kralik of having an affair with his wife. Later, Mr. Matuschek meets with a private investigator who informs him that his suspicions were correct, that his wife is having an affair with one of his employees—Ferencz Vadas. Pepi returns to the shop just in time to prevent the distraught Mr. Matuschek from committing suicide.

Meanwhile, Kralik arrives at the Cafe Nizza, where he discovers that his mystery woman, with the red carnation as planned, is in fact Klara Novak. Despite his disappointment, Kralik goes in and talks with her, pretending he is there to meet Pirovitch. In his mind, Kralik tries to reconcile the cultured woman of his letters with his annoying coworker—secretly hoping that things might work out with her. But concerned that Kraliks presence will spoil her first meeting with her "far superior" mystery correspondent, she calls Kralik a "little insignificant clerk" and asks him to leave, and he does.

Later that night, Kralik goes to the hospital to visit Mr. Matuschek. After apologizing for his behavior, Mr. Matuschek offers him a job as manager of Matuschek and Company, gives him the keys to the shop, and asks him to dismiss Vadas quietly. Kralik dismisses Vadas, loudly and publicly, pushing Vadas across the shop floor into a pile of cigarette boxes. Grateful to Pepi for saving his life, Mr. Matuschek promotes the errand boy to clerk. The next day, Miss Novak calls in sick after her mystery man failed to show. That night, Kralik visits her at her apartment, where she reveals her problem to be "psychological". During his visit, she receives a letter from her correspondent and reads it in front of Kralik (who wrote the letter).

Two weeks later, on Christmas Eve, Matuschek and Company achieves record sales. A grateful Mr. Matuschek gives everyone their bonuses and sends them home early. Then, feeling lonely, he tries to get someone to have dinner with him, but all the employees have other plans. Finally, Rudy, the new errand boy who lives alone in the city, agrees. Kralik and Miss Novak, now alone in the shop, talk about their planned dates for the evening and Miss Novak reveals that she had a crush on Kralik when they first met, back when she was "foolish and naive". After pretending to have met Miss Novaks mystery man—whom he claims is overweight, balding, and unemployed—Kralik puts a blue carnation in his lapel and finally reveals to Miss Novak that he is in fact her mystery correspondent—her "dear friend"—and they kiss.

==Cast==
 
* Margaret Sullavan as Klara Novak
* James Stewart as Alfred Kralik
* Frank Morgan as Hugo Matuschek
* Joseph Schildkraut as Ferencz Vadas
* Sara Haden as Flora Kaczek
* Felix Bressart as Pirovitch
* William Tracy as Pepi Katona
* Inez Courtney as Ilona Novotny
* Charles Halton as Detective
* Charles Smith as Rudy 
* Sarah Edwards as Customer
* Edwin Maxwell as Doctor 

==Adaptations==
The Shop Around the Corner was dramatized in two separate half-hour broadcasts of The Screen Guild Theater, first on September 29, 1940 with Margaret Sullavan and James Stewart, second on February 26, 1945 with Van Johnson and Phyllis Thaxter. It was also dramatized as a one-hour program on Lux Radio Theater s June 23, 1941 broadcast with Claudette Colbert and Don Ameche.

==Remakes== musical remake, In the Good Old Summertime (1949), which stars Judy Garland and Van Johnson.

The 1963 Broadway musical She Loves Me was also inspired by the play and the film.

The plot element of two people who dislike each other while developing an anonymous romance by correspondence is used in the film Youve Got Mail (1998) with Tom Hanks and Meg Ryan, in which one of the protagonists owns a bookstore named "The Shop Around The Corner." Screen credit is given to Miklós László for Parfumerie.

==References==
;Notes
 
;Citations
 

==External links==
 
*  
*  
*  
*  
*   Movie: A Journal of Film Criticism, Issue 1, 2010
Streaming audio
*   on Screen Guild Theater: September 29, 1940
*   on Lux Radio Theater: June 23, 1941

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 