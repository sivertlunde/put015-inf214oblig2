French Kiss (1995 film)
{{Infobox film
| name        = French Kiss
| image       = French Kiss film.jpg
| caption     = Theatrical release poster
| director    = Lawrence Kasdan
| producer    = {{Plainlist|
* Tim Bevan
* Eric Fellner
* Kathryn F. Galan
* Meg Ryan
}} Adam Brooks
| starring    = {{Plainlist|
* Meg Ryan
* Kevin Kline
* Jean Reno
* Timothy Hutton
}}
| music       = James Newton Howard
| cinematography = Owen Roizman
| editing     = Joe Hutshing
| studio      = {{Plainlist|
* PolyGram Filmed Entertainment
* Working Title Films
}}
| distributor = 20th Century Fox
| released    =  
| runtime     = 111 minutes
| country     = United States
| language    = English French
| budget      =
| gross       = $101,982,854
}} Adam Brooks, the film is about a woman who flies to France to confront her straying fiancé and gets into trouble when the charming crook seated next to her uses her to smuggle a stolen diamond necklace. French Kiss was filmed on location in Paris and Cannes, France.   

==Plot==
Kate (Ryan) is a fastidious and wholesome history teacher living in Canada with her fiancé, Charlie (Hutton), a doctor. While waiting for her Canadian citizenship to come through, Kate has been busy planning their wedding and the purchase of their first house, complete with a white picket fence. He urges her to accompany him to Paris for an upcoming business trip, but she declines due to her fear of flying and her general intolerance for cheese, secondhand smoke, and the French.

A few days later Kates plans for the future are crushed when Charlie, in a drunken phone call to her, informs her that he has fallen in love with a beautiful French "goddess" named Juliette (Anbeh) and that he will not be returning. Determined to win him back, Kate boards a flight to Paris, despite her fear of flying, and is seated next to a crude Frenchman, Luc Teyssier (Kline), whose every word during the seven-hour flight seems to annoy her. He is a thief who is smuggling a vine cutting and an expensive stolen diamond necklace into Paris hoping to use both to start his own vineyard. Despite the uncomfortable and sarcastic banter throughout the flight, Kate, with the help of a few drinks, is able to tolerate her "rude" and "hygiene deficient" seating partner long enough to arrive safely in Paris. Before deboarding, however, Luc sneaks the vine and necklace into Kates bag, knowing she will not be searched at customs, and then offers her a ride into the city.

In the terminal, Luc is spotted by Inspector Jean-Paul Cardon (Reno) who insists on giving him a ride during which he searches his bag. Jean-Paul well knows of Lucs vocation, but feels "protective" of him, as he once saved his life. Meanwhile, Kate makes it on her own to the Four Seasons Hotel George V|Hôtel George V where she encounters new levels of French sarcasm and rudeness from the concierge. While waiting in the lobby to confront Charlie, she meets a petty thief named Bob (Cluzet). When she finally sees Charlie and Juliette kissing in a descending elevator, Kate faints, and in the commotion Bob steals her bag. Luc arrives and greets exiting Bob, an acquaintance, as he enters. He finds Kate and revives her, then realizes what Bob has done, so he steals a car, and together he and Kate track down Bob and the missing bag.

Upset at having lost all her money and her passport (which Bob has already sold), Kate argues with Luc and they go their separate ways. Kate learns from Charlies family back home that he and Juliette are headed south to Cannes to meet her parents before their wedding. Meanwhile, after realizing the necklace is still in Kates bag, Luc tracks her down, offers to help her "win back Charlie", and together they board a train to Cannes. At the same time, Bob has disclosed to Jean-Paul that Luc has the necklace. Along the way, Luc attempts to search her bag but is unsuccessful. After lactose intolerant Kate samples some of the 452 official government cheeses of France, she becomes sick and they get off the train at Lucs hometown of La Ravelle in Paulhaguet. They stay at his family home, surrounded by a beautiful vineyard where Kate learns about his past and how he gambled away his vineyard birthright to his brother in a single hand of poker. In her momentary absence, he finally searches Kates bag, but comes up empty, and he is depressed. She has realized he is a schemer, but also learns that he knows a lot about wine, and has dreams of buying land for his own winery. They grow closer and he volunteers to coach her on how to get Charlie back. As they leave his town Kate discloses to him that she, in fact, has the necklace and will return it to him.

At Cannes, the two stay (platonically) in one room at the Carlton Hotel, using a stolen credit card proffered by Luc, and they grow even closer. Kate finally confronts Charlie in front of Juliette on the beach, but her attitude about the situation has changed somewhat. To make Charlie jealous, Luc pretends to be Kates lover, and the deception works.

Jean-Paul approaches Kate and urges her to convince Luc to return the necklace anonymously to avoid jail. Luc, who is planning to sell the necklace at Cartier S.A.|Cartier, agrees to Kates "new plan" to have her sell the necklace, as that would be the safer thing to do.

At dinner, Charlie apologizes to Kate and later tries to seduce her in her room, but she rejects his advances, realizing she no longer wants him, as she is now in love with Luc. Meanwhile, in an effort to "ensure victory" for her, Luc starts to romance an all-too-willing Juliette, but she walks out after he calls her "Kate" by mistake while theyre in bed.

The following morning, Kate tells Luc that Charlie wants her back, but quickly leaves the room, saying, "Cartier is waiting". She returns the necklace to Jean-Paul and purchases a Cartier check for $45,782 with her own savings to create the illusion that she sold it. After giving the check to Luc, she leaves for the airport pretending to meet Charlie. Just after, Jean-Paul approaches Luc and reveals the charade and all that Kate has done for him, as Charlie and Juliette reconnect in view of the both of them. Luc rushes to the airport, boards the plane, and confesses that hes in love with her and wants her to stay with him. Sometime later, he and Kate embrace each other in their beautiful new vineyard.

==Cast==
 
 
* Meg Ryan as Kate
* Kevin Kline as Luc Teyssier
* Timothy Hutton as Charlie
* Jean Reno as Inspector Jean-Paul Cardon
* François Cluzet as Bob
* Susan Anbeh as Juliette
* Renee Humphrey as Lilly
* Michael Riley as M. Campbell
* Laurent Spielvogel as Concierge
* Victor Garrivier as Octave
* Elisabeth Commelin as Claire
* Julie Leibowitch as Olivia
* Miquel Brown as Sgt. Patton
* Louise Deschamps as Jean-Pauls Girl
* Olivier Curdy as Jean-Pauls Boy
* Claudio Todeschini as Antoine Teyssier
* Jerry Harte as Herb
* Thomasine Heiner as Mom
* Joanna Pavlis as Monotonous Voiced Woman
* Florence Soyez as Flight Attendant
* Barbara Schulz as Pouting Girl
* Clément Sibony as Pouting Boy
  Adam Brooks as Perfect Passenger
* Marianne Anska as Cop #1
* Philippe Garnier as Cop #2
* Frédéric Therisod as Cop #3
* Patrice Juiff as French Customs Official
* Jean Corso as Hotel George V Desk Clerk
* François Xavier Tilmant as Hotel Waiter
* Williams Diols as Beach Waiter
* Mike Johns as Lucien
* Marie-Christine Adam as Juliettes Mother
* Jean-Paul Jaupart as Juliettes Father
* Fausto Costantino as Beefy Doorman
* Jean-Claude Braquet as Stolen Moto Owner
* Dominique Régnier as Attractive Passport Woman
* Ghislaine Juillot as Jean-Paul Cardons Wife
* Inge Offerman as German Family
* Nicholas Hawtrey as German Family
* Wolfgang Pissors as German Family
* Nikola Obermann as German Family
* Alain Frérot as Old Man
* Dorothée Picard as Mrs. Cowen
* Jean Allain as Mr. Cowen
 

==Production==

===Casting===
The lead role of Luc was originally written for Gérard Depardieu, but Kevin Kline accepted the role when Depardieu was not available for the film.

===Filming locations===
French Kiss was filmed primarily in Paris, Valbonne in the Alpes-Maritimes départements of France|département in the Provence-Alpes-Côte dAzur régions of France|région of southeastern France, and Cannes. 
* American Embassy, 2 Avenue Gabriel, Paris 8, Paris, France (exterior)
* Avenue des Champs-Élysées, Paris 8, Paris, France (where Kate phones her once future mother-in-law)
* Basilique du Sacré-Coeur, Montmartre, Paris 18, Paris, France
* Canadian Embassy, 35 Avenue Montaigne, Paris 8, Paris, France (embassy)
* Cannes, Alpes-Maritimes, France
* Chateau Val Joanis, Pertuis, Vaucluse, France (grape harvest scenes)
* Grande Pharmacie de la Place Blanche, 5 Place Blanche, Paris 9, Paris, France
* Hôtel George V Quatre Saisons, 31 Avenue George V, Paris, France (Charlies hotel, where Kate loses her bag)
* La Tour dAigues, Vaucluse, France
* Meyrargues, Bouches-du-Rhône, France (station scenes)
* Musée du Louvre, Paris 1, Paris, France (driving scenes)
* Palais de Chaillot, Paris 16, Paris, France (where Kate says "screnched?")
* Paris Studios Cinéma, Billancourt, Hauts-de-Seine, France (studio)
* Paris, France
* Place des Abbesses, Paris 18, Paris, France (where Kate and Luc discuss his "little problem")
* Rue Feutrier & rue Paul Albert, Paris 18, Paris, France (street scenes)
* Rue des Rosiers, Paris 4, Paris, France (where Luc drives down a narrow, winding cobblestoned street)
* Tour Eiffel, Champ de Mars, Paris 7, Paris, France (establishing shots)
* Valbonne, Alpes-Maritimes, France
 Hotel George V in Paris, where Kate has her encounters with the supercilious concierge. The hotel lobby was used for the scene where Kate was robbed by Bob the pickpocket. Other scenes around Paris include numerous shots of the Eiffel Tower, the Louvre Pyramid, and a phonebooth near the Arc de Triomphe where Kate calls her now former mother-in-law. A number of street scenes were shot on the Rive Droite. The corner in Montmartre where rue Paul Albert and rue Feutrier meet was used for the scene where Luc throws money on the sidewalk. Scenes were also filmed at the American Embassy and the Canadian Embassy. The final scene shot in Paris was at the Gare Saint-Lazare train station where Luc is chased by police inspector Jean-Paul Cardon (Jean Reno) while trying to board a train south to Cannes. This is a minor mistake because the train to Cannes does not leave from this station but rather from Gare de Lyon.
 Auvergne régions of France|région of south central France.
 Carlton Hotel Cartier boutique on the next corner.

===Music soundtrack=== Someone Like You" by Van Morrison (4:06)
# "La Vie en Rose" by Louis Armstrong (3:22)
# "Les Yeux Ouverts" by The Beautiful South (3:33)
# "Via Con Me" by Paolo Conte (2:36)
# "I Love Paris" by Toots Thielemans (1:38) Zucchero (5:12)
# "I Love Paris" by Ella Fitzgerald (4:57)
# "Verlaine" by Charles Trénet (3:10)
# "Cest Trop Beau" by Tino Rossi (2:31) La Mer" by Kevin Kline (3:44)
# "I Want You" (Love Theme from French Kiss) (2:04)
# "Les Yeux de Ton Père" by Negresses Vertes (3:57)   

In the film, items 2, 10, and 1 are played in that order over the end scene and closing credits.

==Reception==

===Critical response===
French Kiss received mixed reviews upon its release. In his review in the San Francisco Chronicle, Mick LaSalle wrote that director Lawrence Kasdan "takes what could have been a fluffy comedy with lots of plot complications and picturesque scenery and instead puts his focus on the important things: on the characters played by Ryan and Kline and how they happen to be feeling." LaSalle also applauded Kasdans sense of subtle comedy:
 
LaSalle found Klines performance "extraordinary" and that he not only perfected the accent but the "speech rhythms and the manner as well." LaSalle also praised Ryans comic timing, which "continues to delight."   

In her review in The Washington Post, Rita Kempley, giving it a mixed review, wrote that the film "isnt as passionate as the title suggests—in fact, its facile—but Ryan and Kevin Kline, as her attractive opposite, are irresistible together." Kempley applauded the acting performances:
  }}

In his review in the Chicago Sun-Times, a disappointed Roger Ebert wrote, "The characters in this movie may look like adults, but they think like teenagers." Although he acknowledged that the film was not without its charms—Paris and Cannes being "two of the most photogenic cities on earth"—Ebert wrote, "Klines Frenchman is somehow not worldly enough, and Ryans heroine never convinces us she ever loved her fiance in the first place."   
 Wyatt Earp."   

Review aggregation website Rotten Tomatoes gives the film a score of 45% based on reviews from 22 critics.   

===Box office===
The film earned $38,896,854 in the United States and an additional $63,086,000 in international markets for a total worldwide gross of $101,982,854.   

===Awards and nominations===
  
* 1996 American Comedy Award Nomination for Funniest Actor in a Motion Picture (Kevin Kline)
* 1996 American Comedy Award Nomination for Funniest Actress in a Motion Picture (Meg Ryan)

==Remakes== Mon Mane Na by Sujit Guha. A Telugu film Dongata directed by Kodi Rama Krishna and starring Jagapathi babu and Soundarya was also inspired by French Kiss. 

==Notes== Serious Moonlight (2009), wherein he similarly portrayed a cheating husband and she the wronged wife, battling to get him back.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 