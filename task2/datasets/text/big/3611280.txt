The Housemaid (1960 film)
{{Infobox film 
| name            = The Housemaid
| image           = Housemaid 1960 Poster.jpg
| caption         = Theatrical poster to The Housemaid (1960)
| director        = Kim Ki-young 
| producer        = Kim Ki-young
| writer          = Kim Ki-young Kim Jin-kyu Lee Eun-shim Ju Jeung-ryu Um Aing-ran 
| music           = Han Sang-gi
| cinematography  = Kim Deok-jin
| editing         = Kim Ki-young
| distributor     = Kuk Dong Seki Trading Co.
| released        =  
| runtime         = 108 minutes
| language        = Korean
| country         = South Korea
| budget          = 
| film name      = {{Film name
 | hangul          =  
 | hanja           =  
 | rr              = Hanyeo
 | mr              = Hanyŏ }}
}} 1960 black-and-white Korean film. Kim Jin-kyu. remade in 2010 by director Im Sang-soo.

== Plot ==
The film is a domestic horror thriller telling of a familys destruction by the introduction of a sexually predatory femme fatale into the household. A piano composer has just moved into a two-story house with his wife and two children. When his pregnant wife becomes exhausted from working at a sewing machine to support the family, the composer hires a housemaid to help with the work around the house. The new housemaid behaves strangely, catching rats with her hands, spying on the composer, seducing him and eventually becoming pregnant by him.
 rat poison.

The film ends with the composer reading the story from a newspaper with his wife. The narrative of the film has apparently been told by the composer, who then all smiles warns the film audience that this is just the sort of thing could happen to anyone.

== Cast == Kim Jin-kyu as Dong-sik Kim (the husband/father)
* Ju Jeung-ryu as Mrs. Kim (the wife/mother)
* Lee Eun-shim as Myung-sook (the housemaid)
* Um Aing-ran as Kyung-hee Cho (the factory worker who takes piano lessons)
* Ko Seon-ae as Seon-young Kwak (the factory worker who commits suicide)
* Ahn Sung-ki as Chang-soon Kim (the son)
* Lee Yoo-ri as Ae-soon Kim (the daughter)
* Kang Seok-je
* Na Jeong-ok

==Critical appraisal==
In 2003, Jean-Michel Frodon, editor-in-chief of Cahiers du cinéma, wrote that the discovery of The Housemaid by the West, over forty years after the films debut, was a "marvelous feeling—marvelous not just because one finds in writer-director Kim Ki-young a truly extraordinary image maker, but in his film such an utterly unpredictable work".

Comparing the director to Luis Buñuel, Frodon wrote Kim is "capable of probing deep into the human mind, its desires and impulses, while paying sarcastic attention to the details". He called The Housemaid "shocking", noting that "the shocking nature of the film is both disturbing and pleasurable". Frodon pointed out that The Housemaid was only one early major film in the directors career, and that Kim Ki-young would continue "running wild through obsessions and rebellion" with his films for decades to come. 

==References==
 

==Bibliography==
*  
*  
*  
*  
*  
*  
*  

== External links ==
*  
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 