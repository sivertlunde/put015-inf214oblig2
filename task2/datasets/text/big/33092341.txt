Babu (1975 film)
{{Infobox film
| name           = Babu
| image          = 
| image_size     =
| caption        =
| director       = K. Raghavendra Rao
| producer       = A. L. Kumar Athreya    (dialogues and lyrics)   
| narrator       = Lakshmi 
| music          = K. Chakravarthy
| cinematography = Vincent
| editing        = 
| studio         =
| distributor    =
| released       = 1975
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
 Lakshmi in key roles.

==The Plot==
The film is a love story between two couples and woven with strong family sentiments like objection from elders.

==Cast==
* Sobhan Babu
* Vanisri Lakshmi
* Murali Mohan
* Gummadi Venkateswara Rao

==Soundtrack==
* Ayyababoy Adiripoyindi (Singers: S. P. Balasubrahmanyam and P. Susheela)
* Ennenni Vampulu Ennenni Sompulu Nakunnavemo Rendu Kannulu (Singers: S. P. Balasubrahmanyam and P. Susheela)
* Naa Sneham Pandi Premai Nindina Cheliya Ravela (Lyrics: Aatreya (playwright)|Athreya; Singer: S. P. Balasubrahmanyam)
* Oka Janta Kalisina Tarunana Jeganta Mrogenu Gudilona (Singers: V. Ramakrishna and P. Susheela)
* Oyamma Entalesi Siggochindi Siggochi Mogamenta Muddochindi (Lyrics: Athreya; Singer: P. Susheela)

==Boxoffice==
The film ran for more than 100 days in four centers (Hyderabad, Rajahmundry, Visakhapatnam and Vijayawada) in Andhra Pradesh. 

==References==
 

==External links==
*  

 
 
 
 
 


 