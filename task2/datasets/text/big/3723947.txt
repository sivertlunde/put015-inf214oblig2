Ocean's Thirteen
 
{{Infobox film
| name           = Oceans Thirteen
| image          = Oceans13Poster1.jpg
| director       = Steven Soderbergh
| producer       = Jerry Weintraub
| writer         = Brian Koppelman  David Levien
| starring       = George Clooney  Brad Pitt Matt Damon Andy García Don Cheadle  Bernie Mac  Ellen Barkin  Al Pacino David Holmes
| cinematography = Steven Soderbergh
| editing        = Stephen Mirrione Jerry Weintraub Productions Section Eight Productions Village Roadshow Pictures Warner Bros. Pictures
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = $85 million 
| gross          = $311,312,624
}}
 comedy heist film directed by Steven Soderbergh and starring an ensemble cast. It is the third and final film  in the Soderbergh series (Oceans Trilogy) following the 2004 sequel Oceans Twelve and the 2001 film Oceans Eleven, which itself was a remake of the 1960 Rat Pack film Oceans 11.  All the male cast members reprise their roles from the previous installments, but neither Julia Roberts nor Catherine Zeta-Jones return.

Al Pacino and Ellen Barkin joined the cast as their new targets.
 Las Vegas and Los Angeles, based on a script by Brian Koppelman and David Levien. {{cite web
 | url = http://www.comingsoon.net/news/movienews.php?id=13812
 | title =  Oceans 13 to Start on July 21
 | accessdate = July 14, 2006
}}  The film was screened for the Out of Competition presentation at the 2007 Cannes Film Festival.    It was released on June 8, 2007, in the United States {{cite web
 | url = http://www.themovieinsider.com/m3254/oceans-thirteen/
 | title =  Movie Insider: Oceans Thirteen (2007)
 | accessdate = July 14, 2006
}}  and in several countries in the Middle East on June 6. {{cite web
 | url = http://www.bahraincinema.com
 | title =  Bahrain Cinema Company homepage
 | accessdate = June 6, 2007
}}  

== Plot ==
Reuben Tishkoff is conned by Willy Bank, his former business partner, by being forced to sign over the ownership rights of the new hotel-casino they were building together, "The Bank". Reuben suffers a heart attack and becomes bedridden. Daniel Ocean offers Bank a chance to set things right, given his long history in Las Vegas and the fact that he "shook hands with Frank Sinatra|Sinatra," though Banks refuses. To avenge Reuben, he gathers his partners-in-crime and plans to ruin Bank on the opening night of the hotel.
 Five Diamond Award, which all of Banks previous hotels have won. Saul Bloom poses as the reviewer of the board, while the real reviewer is treated horribly during his stay by Oceans associates and the staff on their payroll. Next, they plan to rig the casinos slot machines and games to force a payout of more than $500&nbsp;million in winnings, forcing Bank to cede control of the casino to the board. This requires defeating "The Greco Player Tracker," a state-of-the-art artificial intelligence system that ensures that all winnings are legitimate by measuring the players biometric responses for authenticity. 
 magnetron to disrupt the Greco. Oceans team acquires one of the giant drills used to bore the Channel Tunnel to simulate an earthquake under the hotel on opening night to ensure that the Greco shuts down. The drill breaks, forcing them to approach Terry Benedict, their previous antagonist, to fund the purchase of a second drill. As Benedict has a grudge against Bank, he offers Ocean the funds only if they also steal four necklaces Bank bought representing the four Five Diamond Awards, now on display in a secured case at the top of the hotel.

On opening night, FBI agents have been informed that machines have been rigged by Livingston Dell, and have identified him. This was intended, resulting in the card-shuffling machines being replaced by Nagel, as Livingston was unable to effectively rig them. Basher distracts Bank long enough to allow Virgil and Turk Malloy to change the groups FBI records, including their names and appearances, to prevent being identified alongside Livingston.

Linus Caldwell seduces Banks assistant to gain access to the display and switch the diamonds with fakes. He is interrupted by the lead FBI agent, who explains the diamond theft to Banks assistant. Linus is arrested, but as they exit in the elevator, the lead agent is revealed to be his father, Robert Bobby Caldwell, who is in on Oceans plan. As they exit to the roof of the hotel for extraction via helicopter, they are caught by François "The Night Fox" Toulour, whom Benedict had ordered to intercept the diamonds. Linus gives the diamonds to Toulour, who escapes by parachuting off the hotel, but after departing discovers that he holds only the fakes; Ocean, who had been aware of Toulours presence, had arranged to extract the entire display case from the hotel.

The plan continues as expected. With "The Greco" disrupted, and guests leaving the hotel with their massive winnings, Bank realizes his ruin. Ocean lets him know that they did everything for Reuben. Bank cannot get revenge as he cannot prove that Ocean did anything illegal. The group uses the money they made off with to buy property north of the Las Vegas Strip for Reuben. To punish Benedict, Ocean donates his $72 million portion of the take to charity, forcing Benedict to publicly admit his philanthropy via television appearances. As the group disperses, Rusty ensures that the real Five Diamond reviewer, who suffered numerous discomforts during his stay at the hotel, is compensated by allowing him to win the jackpot on a rigged slot machine at the airport.

== Cast ==

=== Oceans Thirteen ===
# George Clooney as Danny Ocean
# Brad Pitt as Rusty Ryan Linus Caldwell/Lenny Pepperidge Frank Catton Reuben Tishkoff Virgil Malloy Turk Malloy Livingston Dell Basher Tarr/Fender Rhodes
# Shaobo Qin as List of Oceans Trilogy characters#The Amazing Yen|"The Amazing" Yen/Mr. Weng Saul Bloom/Kensington Chubb Terry Benedict Roman Nagel
 Tess Ocean Isabel Lahiri due to script issues,  their absence being explained by Danny, who repeatedly states, "Its not their fight."

=== Others === Willy Bank Abigail Sponder
* Vincent Cassel as List of Oceans Trilogy characters#François Toulour|François Toulour FBI Agent Robert "Bobby" Caldwell Debbie
* Five Diamond Award reviewer.
* Julian Sands as Greco Montgomery
* Angel Oquendo as Ortega, a guard Denny Shields The Bruiser", casino chips
* Oprah Winfrey as herself
* Bernie Yuman as himself
* Noureen DeWulf as one of the casino game show expo women

== Reception ==

=== Box office ===
The film did well on its first weekend, reaching the top spot at the North American box office. Despite being opened in 250 more theaters than Oceans Twelve, it had a slightly weaker opening weekend than the former, pulling in $36&nbsp;million, compared with Twelve  $39&nbsp;million opening weekend. {{cite news
 | title = "Oceans Thirteen" steals No. 1 spot at box office
 | publisher = Yahoo! Entertainment News
 | date = June 10, 2007
 | url = http://news.yahoo.com/s/nm/20070610/film_nm/boxoffice_dc_1
 | accessdate = June 10, 2007}}    {{cite news
 | last= Douglas
 | first = Edward
 | title =  The Summer Box Office Gets All Wet
 | publisher = Box Office Mojo
 | date = June 10, 2007
 | url = http://www.comingsoon.net/news/movienews.php?id=20925
 | accessdate = June 10, 2007}} 
By the end of December 2007,  Oceans Thirteen had generated $311.4&nbsp;million in box office revenue worldwide. 

=== Critical reception=== New York, David Edelstein wrote, "As the plotting gets knottier, his technique gets more fluid—the editing jazzier, the colors more luscious, the whip-pans more whizbang. Its all anchored by Clooney, looking impudent, roguish, almost laughably handsome." {{cite news
| last = Edelstein
| first = David
| title = What Happens in Vegas… New York
| date = June 3, 2007
| url = http://nymag.com/movies/reviews/32866/
| accessdate = June 18, 2008}}  Manohla Dargis, in her review for The New York Times, wrote, "Playing inside the box and out,     has learned to go against the grain while also going with the flow. In Oceans Thirteen he proves that in spades by using color like Kandinsky and hanging a funny mustache on Mr. Clooneys luscious mug, having become a genius of the system he so often resists." {{cite news
| last = Dargis
| first = Manohla
| authorlink = Manohla Dargis
| title = They Always Come Out Ahead; Bet on It
| work = The New York Times
| date = June 8, 2007
| url = http://movies.nytimes.com/2007/06/08/movies/08ocea.html
| accessdate = June 18, 2008}}  However, Roger Ebert wrote, in his review for the Chicago Sun-Times, "Oceans Thirteen proceeds with insouciant dialogue, studied casualness, and a lotta stuff happening, none of which I cared much about because the movie doesnt pause to develop the characters, who are forced to make do with their movie-star personas." {{cite news
| last = Ebert
| first = Roger
| authorlink = Roger Ebert
| title = Oceans Thirteen
| work = Chicago Sun-Times
| date = June 7, 2007
| url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20070606/REVIEWS/706060301
| accessdate = June 18, 2008}}  Peter Bradshaw, in his review for The Guardian, wrote, "Sometimes we go to split-screen, and sometimes - whooaaa! - two of the split-screen frames are funkily showing the same thing. It is all quite meaningless. As if in an experimental novel by BS Johnson, the scenes could be reshuffled and shown in any order and it would amount to the same thing. There is no human motivation and no romance." {{cite news
| last = Bradshaw
| first = Peter
| title = Oceans Thirteen
| work = The Guardian
| date = June 8, 2007
| url = http://film.guardian.co.uk/News_Story/Critic_Review/Guardian_review/0,,2097605,00.html
| accessdate = June 18, 2008
| location=London}} 

==Home video release==
 
Oceans Thirteen was released on DVD in November 2007. 

== References ==
 

== External links ==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 