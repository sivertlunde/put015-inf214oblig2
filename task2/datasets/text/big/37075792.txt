Huck and Tom
:Not to be confused with the 1995 film Tom and Huck.
{{Infobox film
| name = Huck and Tom
| image = File:Huck and Tom Poster.jpg
| caption = Film poster
| director = William Desmond Taylor
| produced by = Jesse L. Lasky
| writer = Mark Twain (stories) Julia Crawford Ivers Robert Gordon George Hackathorne Edythe Chapman Frank Lanning Clara Horton Helen Gilmore Antrim Short Jane Keckley
| music = 
| cinematography = Homer Scott
| editing = 
| distributor = Famous Players-Lasky Co. Paramount Pictures
| released = May 13, 1918
| runtime = 
| country = United States
| language = English
| budget = 
| gross = 
}} Tom Sawyer, a successful adaptation also directed by Taylor.

==Cast==

*Jack Pickford: Tom Sawyer
 Robert Gordon: Huck Finn

*George Hackathorne: Sid Sawyer

*Alice Marvin: Mary Sawyer
 
*Edythe Chapman: Aunt Polly
 
*Frank Lanning: Injun Joe

*Clara Horton: Becky Thatcher
 
*Tom Bates: Muff Potter

*Helen Gilmore: Widow Douglas

*Antrim Short: Joe Harper
 
*Jane Keckley: Mrs. Thatcher
 John Burton: Judge Thatcher

==Release== Variety called it "acceptable" and Photoplay described it as "not so fascinating, being an unbelievable mixture of boyish fancy and Brady melodrama." 

==References==
 

==External links==
*  
 
 

 
 
 
 
 
 
 
 
 


 