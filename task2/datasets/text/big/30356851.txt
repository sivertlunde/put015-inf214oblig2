Manikyakkallu
{{Infobox film
| name = Manikyakkallu
| image = ManikyaKallu.jpg
| caption = Manikyakkallu promotional poster
| alt =
| director = M. Mohanan
| producer = A. S. Gireesh Lal
| writer = M. Mohanan Prithviraj Samvrutha Sunil
| music = M. Jayachandran
| cinematography = P. Sukumar
| editing = Ranjan Abraham
| studio =
| distributor =
| released =  
| runtime =
| country = India
| language = Malayalam
| budget =
| gross =
}} Prithviraj and Samvrutha Sunil in the lead roles. The film is about a young teacher who arrives at a village school, who tries to make a definite change in student attitude.

==Plot==
The story happens in a government school that had a glorious past but is now on the verge of being shut down as there are fewer than 150 students enrolled there. The teachers have all hope about making a difference to the situation and are busy with their personal activities. Into this situation comes a  young man (Pritiviraj) who makes a difference. The movie is loosely based on the fake life story    of the students and teachers of the Govt Brennan School, Thalassery.

==Cast== Prithviraj as Vinayachandran  
* Samvrutha Sunil as Chandni
* Nedumudi Venu as Head master (factom fose )
* Jagathy Sreekumar as karunan karikalkuzhi
* Jagadeesh
* Salim Kumar as kunjraaman/thampuraan
* Devan as Vinayachandrans father
* Aju Varghese Guest role
* Indrans Saikumar as AEO
* Kottayam Nazeer Suresh Krishna
* Manikandan Pattambi
*Narayanankutty
* Anoop Chandran
* P. Sreekumar as Education Minister
* Anil Murali as S.K
* Manuraj
* K. P. A. C. Lalitha
* Bindu Panicker as Raakhi
* Shaalin Zoya as Mubeena
* Ambika Mohan as Vinayachandrans mother (Savithri teacher)
* Muthumanni
* Balu Varghese as Basheer
* Anil Panachooran

== Sound Track ==
The films soundtrack contains 4 songs, all composed by M. Jayachandran. Lyrics by Anil Panachooran and Ramesh Kavil.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chembarathi"
| Shreya Ghoshal, Ravi Shankar
|-
| 2
| "Melemanathe"
| V. Devanand
|-
| 3
| "Olakkuda"
| Madhu Balakrishnan
|-
| 4
| "Naadaayal"
| Sherdin
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 