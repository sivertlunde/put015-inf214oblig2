Sunny (2011 film)
 
{{Infobox film
| name           = Sunny
| image          = Sunny2011filmposter.jpg
| caption        = Theatrical poster
| director       = Kang Hyeong-cheol
| producer       = Ahn Byeong-ki    Ahn In-ki
| writer         = Kang Hyeong-cheol
| starring       = Shim Eun-kyung   Kang So-ra   Yoo Ho-jeong   Jin Hee-kyung
| music          = Kim Jun-seok
| cinematography = Lee Hyeong-deok
| editing        = Nam Na-yeong
| studio         = Toilet Pictures   Aloha Pictures
| distributor    = CJ Entertainment
| film name      = {{Film name
| hangul         =  
| rr             = Sseoni
| mr             = Ssŏni}}
| released       =  
| runtime        = 134 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
}}
Sunny ( ) is a 2011 South Korean film about a middle-aged woman who tries to fulfill her friends dying wish of reuniting their group of high school friends. The film alternates between two timelines: the present day where the women are middle-aged, and the 1980s when they were in high school. It is the second film by writer-director Kang Hyeong-cheol, who previously directed Scandal Makers (2008).  

Released on 4 May 2011, Sunny was the first film of that year to sell five million tickets in South Korea,  and became the second highest grossing Korean film by years end.   , it is the List of highest-grossing films in South Korea|all-time 13th best-selling in South Korean history. Kang Hyeong-cheol and Nam Na-yeong won Best Director and Best Editing, respectively, at the Grand Bell Awards.   Actress Kang So-ra won several awards for her role as the teenage Ha Chun-hwa. 

==Plot==
 

Im Na-mi (Yoo Ho-jeong), a wealthy housewife and mother, does her daily routine. While things look perfect on the outside (wonderful home, generous husband, beautiful daughter), she is depressed about her life. When she washes her face, she sees wrinkles on her skin. When she asks her husband to visit her mother at the hospital, he replies by giving her money to buy luxury bags, and her daughter expresses similar indifference and annoyance. In the mornings, Na-mi eats breakfast alone while father and daughter head to work and school, respectively. She looks outside and notices a group of high school girls who are walking and laughing.   

After visiting her mother, Na-mi passes a patients room with the sign "Ha Chun-hwa," and thinks about her high school life. She asks her chauffeur to take her to the all-girls high school she attended in Seoul. As she walks toward the school, a high school girl bumps into her and apologizes respectfully. Shortly after, a second girl bumps into her, but she is not as courteous. She wears a bright, yellow sweater, in fact, all of the girls are now wearing colorful sweaters and bright pants, as the setting is now the 1980s with Na-mi (Shim Eun-kyung) as a teenager. 

At class, the girls are dusting records and admiring posters of American actors. Many of the girls are wearing American athletic shoe brands. The teacher enters and introduces Na-mi. The students make fun of her country accent, and she becomes embarrassed of her shoes and clothing. After the teacher leaves, two girls sit around timid Na-mi, and bully her. As their remarks turn vicious, a large Adidas bag hits one of them on the head. The girl turns around, ready to lash out, but when she sees who it is, she immediately apologizes, and returns to her seat.

The girl who threw the bag is Ha Chun-hwa ( ) is a bright student who wants to become a writer; she will hit anyone who messes with her friends. Ryu Bok-hee (Kim Bo-mi) has dreams of becoming Miss Korea; she carries a small hand mirror and makes faces to herself.    Jung Su-ji (Min Hyo-rin) is a quiet, mysterious beauty; whenever she speaks to Na-mi, it is always with disdain. At one point in the film, Na-mi confronts Su-ji and learns that Su-jis stepmother is from Jeollado, the same city Na-mi is from, making Su-ji automatically biased against Na-mi. The two make up after drinking soju, and crying out their frustrations together.  Na-mi is accepted into their group as their seventh member, after she unexpectedly proves herself against a rival group from a different school when she uses her diabetes as a front for spirit possession. Chun-hwa suggests naming their group; they settle on "Sunny," after a night-time radio DJ responds to their letter on air. During this time Na-mi meets Oh Joon-ho (Kim Shi-hoo), a friend of Jang mis brother. She is instantly enamored with him. Throughout the movie there are flashbacks of the time the two spent together as he becomes Na mis first love.
 terminal cancer with two months to live. Na-mis husband calls, and informs her that he is on a two-month business trip. As Na-mi leaves, she promises she will visit often; Chun-hwa then tells her she would like to see Sunny reunited one more time before she dies. 

Na-mi hires a private detective to find the members of Sunny. Jang-mi (Go Soo-hee) is struggling as a life insurance sales agent. The foul-mouthed Jin-hee (Hong Jin-hee) married rich, but her husband cheats, and she pretends to be ladylike. Geum-ok (Lee Yeon-kyung) is unemployed and living in a cramped apartment with her overbearing sister-in-law, her sister-in-laws husband, and a newborn. After her mothers salon went bankrupt, Bok-hee (Kim Sun-kyung) had resorted to prostitution; her daughter lives at an orphanage.  The detective notes that Su-ji has been exceptionally difficult to find; he recommends posting a newspaper ad. Na-mi also ask the detectives to search for Joon-ho. Eventually he is found and Na mi goes to visit him. While on her way to see him, she flashes back to the time the group of friends went on a trip together. While on the bus Na-mi draws a portrait of Joon-ho; she later goes in search of him with the intention of giving him the drawing. When she finds him, she is shocked to see him and Su-ji kiss. She leaves in tears and never gives him the picture. Now as an adult, she goes to the record shop Joon-ho owns and sees Joon-hos son (who looks exactly like the younger Joon-ho). She then gives the now-older Joon-ho (Lee Geung-young) the drawing, and by doing so she is able to let go of her first love.

Chun-hwa passes away before the group manages to get together, but by finding each other, the women rekindle their passion for life and enjoy each others company. At one point Chun-hwa, Na-mi, Jang-mi and Jin-hee get together to get revenge on the group of girls who are bullying Na-mis daughter. At Chun-hwas funeral, Sunny (minus Su-ji), is reunited, but not every woman knows about each others present struggles. As they are about to leave, Chun-hwas lawyer (Sung Ji-roo) walks in and asks them if they are Sunny. He reads Chun-hwas will, which bequeathes that Na-mi will be the leader of Sunny. Jin-hee is given the position of vice-president; she looks disappointed because she expected something monetary. To that, the lawyer explains, "You are already rich" from Chun-hwa. He then reads that, for Jang-mi, Chun-hwa had bought life insurance from her, in the names of all the members of Sunny. Jang-mi is elated that she will finally be number one in her sales for that month. To Geum-ok, Chun-hwa offers her a position at her publishing company, with a chance to become executive manager if she doubles her sales. Chun-hwa leaves Bok-hee a paid-for apartment, so that she may live with her daughter. And after she finishes rehab, she will also receive the ground floor of Geum–oks building, with a large sum of money, so she can open a hair salon.

After the conclusion of the reading, per Chun-hwas last wish the women reprise their high school choreography by dancing to "Sunny (song)|Sunny" in front of Chun-hwas funeral picture. As they celebrate, Su-ji (Yoon Jung) makes a surprise appearance. The film ends with flashbacks to their teenage selves.

==Allusions==
The flashback scenes juxtaposed the fun and silly, drama-filled lives of high school students with the Gwangju Democratization Movement that took place in May 1980. In the film, Na-mis brother is a university student who participates in the protests. The scenes where Sunny fights the rival gang are backgrounded with the violent clash between the protestors and the military.
 Nike and Boney Ms 1976 cover of Bobby Hebbs song "Sunny (song)|Sunny."

==Cast==
 
 
;Present
*Yoo Ho-jeong as Im Na-mi
*Jin Hee-kyung as Ha Chun-hwa
*Go Soo-hee as Kim Jang-mi
*Hong Jin-hee as Hwang Jin-hee
*Lee Yeon-kyung as Seo Geum-ok
*Kim Sun-kyung as Ryu Bok-hee
*Baek Jong-hak as Na-mis husband
*Ha Seung-ri as Ye-bin, Na-mis daughter
*Jung Suk-yong as Jong-ki, Na-mis older brother 
*Lee Joon-hyuk   as Owner of private detective agency
*Lee Geung-young as Oh Joon-ho
*Kim Shi-hoo as Joon-hos son
*Kim Ji-kyung as Jin-hees husband
*Joo-ho as Insurance company manager
*Cha Tae-hyun as Model of insurance company
*Kim Joon-ho as Private detective
*Sung Ji-roo as Chun-hwas lawyer
*Yoon Jung as Jung Su-ji
 
;1980s
*Shim Eun-kyung as Im Na-mi
*Kang So-ra as Ha Chun-hwa
*Kim Min-young as Kim Jang-mi
*Park Jin-joo as Hwang Jin-hee
*Nam Bo-ra as Seo Geum-ok
*Kim Bo-mi as Ryu Bok-hee
*Min Hyo-rin as Jung Su-ji
*Kim Shi-hoo as Oh Joon-ho
*Kim Young-ok as Na-mis grandmother
*Jung Won-joong as Na-mis father
*Kim Hye-ok as Na-mis mother
*Park Young-seo as Jong-ki, Na-mis older brother 
*Chun Woo-hee as Sang-mi Kim Ye-won as Leader of rival gang "Girls Generation"
*So Hee-jung as Homeroom teacher
*Kim Won-hae as Student liaison teacher
*Park Hee-jung as Young-jin
*Han Seung-hyun as Jang-mis older brother
*Kang Ji-won as Su-jins stepmother
*Kang Rae-yeon as Jong-kis girlfriend
*Yang Hee-kyung as Jang-mis mother
 

==Songs==
The soundtrack of this film is mostly composed of songs from the Sunny Original Motion Picture Score Album produced by music director Joon-Seok Kim to express the emotional status of the characters in the film. The soundtrack also includes a mix of Korean and Western pop music of the 1980s to bring reminiscent feel to the general plot. The Western pop songs were added to signify the Western "fad" that swept over the students at the time in Korea. The soundtrack helped the movie to be better resonated with the audience allowing Joon-Seok Kim to win the Best Music Award the year of the movie release.

{| class="wikitable"
! Song || Artist || Album
|-
| Girls Just Want to Have Fun || Cyndi Lauper || Shes So Unusual
|- Sunny || Boney M (Original by Bobby Hebb) || Take the Heat off Me
|-
| Reality || Richard Sanderson || La Boum
|- Joy || Best of JOY
|-
| In My Dreams (꿈에) || Duck-Bae Cho 조덕배 || 조덕배 2nd Album
|-
| I See (보이네) || Nami || Nami 4th Alum
|-
| Time Travel (시간여행) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| SUNNY || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Like Mom, Like Daughter || Joon-Seok Kim|| Sunny Original Motion Picture Score
|-
| Shadowing Stealthily || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Blind with Love || Joon-Seok Kim (Original: Nami) || Sunny Original Motion Picture Score
|-
| Sunny vs. Girls Generation (소녀시대) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| The Realization of a Just Society || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Journey to Find Friends (친구를 찾아) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Dreaming (꿈꾸던 소녀) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| A Little Girl || Joon-Seok Kim (Original: Nami) || Sunny Original Motion Picture Score
|-
| Memory Train (추억의 기차) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Old Promise (오래된 약속) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Close to My Friend || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| The Last Gift (마지막 선물) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Reunion (오랜만의 재회) || Joon-Seok Kim || Sunny Original Motion Picture Score
|-
| Round and Round (빙글빙글) || Joon-Seok Kim (Sung by actors in film) || Sunny Original Motion Picture Score
|-
|}

==Release==
The film was released on 4 May 2011 in South Korea.  It also received a limited release in the United States in July 2011, screening in Los Angeles, Torrance, California|Torrance, New York City, New Jersey, Chicago, Virginia, Washington D.C., Seattle, Texas and Hawaii.  

===Film festivals===
The film has been shown in film festivals worldwide:
{| class="wikitable"
|-
! Event  !! Location !! Event Dates !! Category/Remarks
|-
| 16th Busan International Film Festival || Busan, South Korea  || 6–14 October 2011 || Korean Cinema Today: Panorama *Directors Cut
|-
| 6th Korean Film Festival in Paris || Paris, France  || 11–18 October 2011 || Opening Night Film *European Premiere
|-
| 13th Mumbai Film Festival || Mumbai, India || 13–30 October 2011 || World Cinema
|-
| 6th London Korean Film Festival  || London, England || 4–10 November 2011 || Contemporary Korean Cinema
|-
| 10th New York Korean Film Festival || New York City, United States || 24–26 February 2012 || 
|-
| 7th Osaka Asian Film Festival  || Osaka, Japan || 9–18 March 2012 || Special Screenings
|-
| 2nd San Diego Asian Film Foundation Spring Showcase || San Diego, United States || 19–26 April 2012 || Opening Night Film
|-
| 14th Udine Far East Film Festival  || Udine, Italy || 20–28 April 2012 || Opening Night Film
|-
| 16th Fantasia International Film Festival  || Montreal, Canada || 19 July–9 August 2012 || 
|-
| 3rd Korean Film Festival in Australia  || Sydney, Australia || 22–28 September 2012 || Closing Night Film
|-
|}

==Reception==

===Box office===
In 2011, the movie sold 7,375,110 tickets, and grossed   ( ), making it the years second highest grossing Korean film and fourth highest grossing overall film in South Korea.       At the end of the movies run, it had sold 7.38 million admissions, with an additional 90,555 from a directors cut.  

===Awards and nominations===
{| class="wikitable"
|- style="background:#b0c4de; text-align:center;"
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="22"| 2011
|   5th Mnet 20s Choice Awards
| Hot Movie Star
| Kang So-ra
|  
|-
|   20th Buil Film Awards
| Best New Actress
| Kang So-ra
|  
|-
| rowspan="9"|   48th Grand Bell Awards
| Best Film
| Sunny
|  
|-
| Best Director
| Kang Hyeong-cheol
|   
|-
| Best Supporting Actress
| Chun Woo-hee
|   
|-
| Best New Actress
| Kang So-ra
|  
|-
| Best Screenplay
| Kang Hyeong-cheol
|  
|-
| Best Planning
| Ahn Byeong-ki, Lee Anna
|  
|-
| Best Editing
| Nam Na-yeong
|  
|-
| Best Costume Design
| Chae Kyung-hwa
|  
|-
| Best Music
| Kim Jun-seok
|  
|- 32nd Blue Dragon Film Awards
| Best Film
| Sunny
|  
|-
| Best Director
| Kang Hyeong-cheol
|  
|-
| Best Supporting Actress
| Chun Woo-hee
|  
|-
| Best New Actress
| Kang So-ra
|  
|-
| Best Screenplay
| Kang Hyeong-cheol
|  
|-
| Best Art Direction
| Lee Yo-han
|  
|-
| Best Music
| Kim Jun-seok
|  
|-
| Technical Award
| Nam Na-yeong (editing)
|  
|-
| rowspan="2"|   19th Korean Culture and Entertainment Awards
| Grand Prize (Daesang) for Film
| Sunny
|  
|-
| Best New Actress
| Min Hyo-rin
|  
|-
|   4th Style Icon Awards
| Content of the Year
| Sunny
|  
|-
| rowspan="5"| 2012
|   3rd KOFRA Film Awards  
| Best Director
| Kang Hyeong-cheol
|  
|- 48th Baeksang Arts Awards
| Best Film
| Sunny
|  
|-
| Best Actress
| Shim Eun-kyung
|  
|-
| Best New Actress
| Kang So-ra
|  
|-
| Most Popular Actress
| Kang So-ra
|  
|-
|}

== Unofficial remakes ==
Hong Kong television series Never Dance Alone, which aired on TVB in 2014, is reportedly inspired by this movie. 

==Notes==
 

==References==
 

==External links==
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 