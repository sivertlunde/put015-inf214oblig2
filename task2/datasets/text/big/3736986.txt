Carry On Spying
 
 
 
{{Infobox film
| name = Carry On Spying
| image = Carry On Spying.jpg
| caption = film poster by Tom Chantrell
| director = Gerald Thomas
| producer = Peter Rogers
| writer = Talbot Rothwell Sid Colin Charles Hawtrey Eric Barker Dilys Laye Eric Rogers
| cinematography = Alan Hume
| editing = Archie Ludski
| studio = Peter Rogers Productions/ Anglo-Amalgamated
| distributor = Anglo-Amalgamated/ Associated British Picture Corporation|Warner-Pathé
| released =  
| runtime = 87 minutes
| country = United Kingdom
| language = English
| budget = £148,000
}}
 the series Charles Hawtrey and Jim Dale are present. Bernard Cribbins makes the second of his three Carry On appearances (although it would be 28 years before he returned in Carry On Columbus). Eric Barker appears for his third entry (his final appearance would be in Carry On Emmannuelle 14 years later). Dilys Laye returns after her debut in Carry On Cruising.  Carry On Spying is the last Carry On film shot in black and white.

==Plot== Secret Service Charles Hawtrey), to find the formula.

The Agents are hot on the trail, chasing the villains across the world. Their pursuit takes them to Vienna, and to Algiers. Upon the way they encounter the STENCH agents, the Fat Man and Milchmann (who stole the formula disguised as a milkman). Unfortunately the agents lack of experience results in their contact agent, Carstairs (Jim Dale), being floored in an encounter with the Fat Man, and they also encounter the mysterious Lila (Dilys Laye), whom they are uncertain if they can trust.

==Cast==
* Kenneth Williams as Desmond Simkins (codename Red Admiral)
* Barbara Windsor as Daphne Honeybutt (codename Brown Cow) Charles Hawtrey as Charlie Bind (codename Yellow Peril)
* Bernard Cribbins as Harold Crump (codename Blue Bottle)
* Jim Dale as Carstairs
* Eric Barker as The Chief
* Richard Wattis as Cobley
* Dilys Laye as Lila
* Eric Pohlmann as The Fat Man
* Victor Maddern as Milchmann
* Judith Furse as Dr Crow
* John Bluthal as The head waiter
* Renée Houston as Madame Tom Clegg as Doorman
* Gertan Klauber as Code clerk
* Norman Mitchell as Native policeman
* Frank Forsyth as Professor Stark
* Derek Sydney as Algerian gent
* Jill Mai Meredith as Cigarette girl
* Angela Ellison as Cloakroom girl
* Hugh Futcher as Bed of nails native
* Norah Gordon as Elderly woman
* Jack Taylor as Thug
* Bill Cummings as Thug
* Anthony Baird as Guard
* Patrick Durkin as Guard
* Virginia Tyler as Funhouse girl
* Judi Johnson as Funhouse girl
* Gloria Best as Funhouse girl
* Audrey Wilson as Amazon guard
* Vicky Smith as Amazon guard
* Jane Lumb as Amazon guard
* Marian Collins as Amazon guard
* Sally Douglas as Amazon guard
* Christine Rodgers as Amazon guard
* Maya Koumani as Amazon guard

==Crew==
* Screenplay – Talbot Rothwell & Sid Colin Eric Rogers Geoffrey Parsons Eric Rogers
* Associate Producer – Frank Bevis
* Art Director – Alex Vetchinsky
* Director of Photography – Alan Hume
* Editor – Archie Ludski
* Camera Operator – Godfrey Godar
* Assistant Director – Peter Bolton
* Unit Manager – Donald Toms
* Continuity – Penny Daniels
* Hairdressing – Biddy Chrystal
* Sound Editor – Christopher Lancaster
* Sound Recordists – CC Stevens & Bill Daniels
* Costume Designer – Yvonne Caffin
* Make-up – WT Partleton
* Producer – Peter Rogers
* Director – Gerald Thomas

==Production== From Russia with Love.

The film pokes fun at various spy movies, James Bond being the least of them. They include The Third Man (coincidentally, Eric Pohlmann – who played The Fat Man – also had a minor part in The Third Man), and Casablanca (film)|Casablanca. One or two of Crows female assistants wear hairstyles similar to that of Modesty Blaise, whose adventures had started in the London Evening Standard the previous year.

==Filming and locations==

* Filming dates – 8 February-13 March 1964

Interiors:
* Pinewood Studios, Buckinghamshire

==Bibliography==
*  
*  
*  
*  
* Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (third edition) (2007) (Reynolds & Hearn Books)
*  
*  
*  
*  
*  

==References==
 
 

==External links==
*  
*   at The Whippit Inn
*  
*   Empire

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 