The Brigand of Kandahar
 
 
{{Infobox film
| name           = The Brigand of Kandahar
| image_size     =
| image	         = "The_Brigand_of_Kandahar"_(1965).jpg	
| caption        = UK theatrical poster
| director       = John Gilling
| producer       = Anthony Nelson Keys
| writer         = John Gilling (original story and screenplay)
| narrator       = Ronald Lewis  Oliver Reed  Duncan Lamont
| music          = Don Banks
| cinematography = Reginald H. Wyer (as Reg Wyer)
| editing        =  Tom Simpson
| studio         = Hammer Films 
| distributor    = Columbia Pictures (US)
| released       = 9 August 1965 (UK) November 1965 (US)	
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} British adventure Ronald Lewis, Oliver Reed and Duncan Lamont.  A mixed-race British officer is thrown out of his regiment when he is accused of cowardice in action. He then joins some tribesmen rebelling against the British.  It was one of a number of films made during the era that had native rebels as their heroes.  It was filmed with extensive footage from Zarak.

==Cast== Ronald Lewis - Case
* Oliver Reed - Ali Khan
* Duncan Lamont - Colonel Drewe
* Yvonne Romain - Ratina Katherine Woodville - Elsa
* Glyn Houston - Marriott
* Sean Lynch - Rattu Walter Brown - Hitala
* Inigo Jackson - Captain Boyd
* Jeremy Burnham - Captain Connelly
* Joe Powell - Colour Sergeant
* Henry Davies -  Crowe
* Caron Gardner - Serving Maid
* John Southworth - Barlow

==Critical reception== Time Out wrote, "India in 1850 provides the backdrop (supposedly, at least, since papier mâché rocks and rural England are much in evidence) for a routine military adventure";  while TV Guide noted the "battle climax is fast, well staged, and entertaining."  

==Bibliography==
* Monk, Claire & Sargeant, Amy. British historical cinema: the history, heritage and costume film. Routledge, 2002.
* Richards, Jeffrey. Visions of Yesterday. Routledge & Kegan Paul, 1973.

==References==
 

==External links==
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 