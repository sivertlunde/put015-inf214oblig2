One: The Movie
 
  independent Documentary documentary that surveys beliefs on the meaning of life, culminating with the view that "we are all one". The movie was created and directed by Michigan filmmakers Scott Carter, Ward M. Powers and Diane Powers, and featured interviews with Deepak Chopra, Robert Thurman, Thich Nhat Hanh, Jaggi Vasudev, and others.

It was originally released in movie theaters in  . 

==Content==
  spiritual and Hare Krishna, Native American spirituality, Catholicism, Protestantism, and Christian fundamentalism). ONE also explores contemporary themes of war, conflict, terrorism, peace, global change, social responsibility, and environmental concerns. Prior to editing the movie, the filmmakers consulted with American philosopher Ken Wilber, founder of the Integral Institute.

The 20 questions asked of participants were: Why is there poverty and suffering in the world?
# What is the relationship between science and religion?
# Why are so many people depressed?
# What are we all so afraid of? war justifiable?
# How would God want us to respond to aggression and terrorism?
# How does one obtain true peace?
# What does it mean to live in the present moment?
# What is our greatest distraction?
# Is current religion serving its purpose? after you die?
# Describe heaven and how to get there?
# What is the meaning of life?
# Describe God? quality humans possess?
# What is it that prevents people from living to their full potential?
# Non-verbally, by motion or gesture only, act out what you believe to be the current condition of the world.
# What is your one wish for the world?
# What is wisdom and how do we gain it?
# Are we all One?

Participants included Deepak Chopra, Ram Dass, Thich Nhat Hanh (Thay), Thomas Keating, Barbara Marx Hubbard, Riane Eisler, Robert Thurman, Jaggi Vasudev, Llewellyn Vaughan-Lee, Mantak Chia, the late Bhakti Tirtha Swami, Richard Rohr, Sister Chan Khong, Hassan Al-Qazwini, the late Wayne Teasdale, Rabbi Arnie Sleutelberg, and Muruga Booker. 

==International theatrical release==
 
*In April 2007, (subtitled in Spanish language|Spanish) was released in Mexico by Arthouse Movies. German was released in several cities in Germany by TAO Cinemathek.
*In July 2007, released in Brazil
*Opened in theaters in Austria in September 2007.

==Film festival presence==
 
*Official Selection of the 2005 Berlin One World International Human Rights Festival
*Official Selection of the 2005 Hawaii International Film Festival
*Official Selection of the 2005 Global Peace Film Festival
*Official Selection of the 2005 Hong Kong Exploring Consciousness Film Festival
*Official Selection of the 2005 South African Exploring Consciousness Film Festival
*Official Selection of the 2005 Waterfront Film Festival
*Award Winner of the 2005 East Lansing Film Festival

==Soundtrack==
*ONE: The Movie Original Musical Score and Theme by Omar Jon Ajluni
*"Ya Ya" by Paully Moonbeam
*"Choir OM" by the Center for Spiritual Living
*"Evolution" by Simple Wisdom
*"Slo OJ" by Chapstik
*"Washed Away" by Nooh Michael Fitzpatrick Lish McDuff, Chien-Tai, and Anthony Stoops
*"Awakening" by Aaron Nicholson
*"Walk a Mile" in My Shoes by M-Pact

==See also==
* New Thought
* The Secret (2006 film)
* The Secret (book)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 