Loving Jezebel
{{Infobox film
| name           = Loving Jezebel
| image          = Loving Jezebel poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Kwyn Bader 
| producer       = David Lancaster 
| writer         = Kwyn Bader 
| starring       = Hill Harper David Moscow Laurel Holloman Nicole Ari Parker Sandrine Holt Lysa Aya Trenier
| music          = Tony Prendatt 
| cinematography = Horacio Marquínez 	
| editing        = Tom McArdle 
| studio         = BET Lancaster Productions Starz! Encore Entertainment
| distributor    = Universal Focus
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $72,190 
}}

Loving Jezebel is a 1999 American comedy film written and directed by Kwyn Bader. The film stars Hill Harper, David Moscow, Laurel Holloman, Nicole Ari Parker, Sandrine Holt and Lysa Aya Trenier. The film was released on October 27, 2000, by Universal Focus.

==Plot==
 

== Cast ==
*Hill Harper as Theodorous
*David Moscow as Gabe
*Laurel Holloman as Samantha
*Nicole Ari Parker as Frances
*Sandrine Holt as Mona
*Lysa Aya Trenier as June
*Andre B. Blake as Necco 
*Jean-Christophe Emo as Francois
*Lawrence Gilliard, Jr. as Walter 
*John Doman as Pop Melville
*Phylicia Rashad as Alice Melville
*Justin Pierre Edmund as Little Theodorous
*Elisa Donovan as Salli
*Heather Gottlieb as Nina Clarise
*Diandra Newlin as Nikki Noodleman
*Faith Geer as Mrs. Harp
*Barry Yourgrau as Mr. Leone
*Eugene Ashe as Man in Car
*Makeda Christodoulos as Israeli Girl
*Abigail Revasch as Israeli Girl
*Judah Domke as Customer
*Jason Hefter as Customer
*Gregory Grove as Waiter
*Angel Brown as Rita
*Ray Frazier as Skip
*Crystal Rose as Beth
*Johnny X Rook as Steve

==Reception==
Loving Jezebel received negative reviews from critics. On Rotten Tomatoes, the film has a rating of 33%, based on 27 reviews, with a rating of 4.8/10. The sites critical consensus reads, "The script is cliched and badly written. The female characters arent believable, and the movie is not funny."  On Metacritic, the film has a score of 33 out of 100, based on 12 critics, indicating "generally unfavorable reviews". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 