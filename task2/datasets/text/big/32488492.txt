Sweet Kitty Bellairs (1916 film)
{{infobox film
| name           = Sweet Kitty Bellairs
| image          = Sweet Kitty Bellairs.jpg
| imagesize      =
| caption        = James Young
| producer       = Adolph Zukor Daniel Frohman
| writer         = James Young (scenario)
| based on       =   Tom Forman 
| cinematography = Paul P. Perry
| editing        =
| distributor    = Famous Players-Lasky Paramount Pictures
| released       =  
| runtime        = 50 mins.
| country        = United States Silent English English intertitles
}}
 1916 American silent romantic James Young.  

==Cast==
*Mae Murray - Kitty Bellairs Tom Forman - Lord Verney
*Belle Bennett - Lady Julia
*Lucille Young - Lady Barbara Flyte Joseph King - Sir Jasper
*James Neill - Colonel Villers Lucille Lavarney - Lady Maria
*Horace B. Carpenter - Captain Spicer
*Robert Gray - Captain OHara

==Other adaptations== in 1930 as a sound musical comedy filmed in Technicolor.

==See also==
*List of lost films

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 