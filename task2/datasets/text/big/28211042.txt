Attenberg
 
{{Infobox film
| name           = Attenberg
| image          = Attenberg.jpg
| caption        = Film poster
| director       = Athina Rachel Tsangari
| producer       = Maria Hatzakou Yorgos Lanthimos Iraklis Mavroidis Athina Rachel Tsangari Angelos Venetis
| writer         = Athina Rachel Tsangari
| starring       = Ariane Labed Vangelis Mourikis Evangelia Randou Yorgos Lanthimos
| music          = 
| cinematography = Thimios Bakatatakis
| editing        = Matt Johnson Sandrine Cheyrol
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Greece
| language       = Greek
| budget         = 
}} Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Plot==
Marina, a sexually inexperienced 23 year old woman, lives with her terminally-ill architect father, Spyros, in an industrial Greek town by the sea.
 Sir David Suicide and the sex education lessons given to her by her friend Bella.

Despite her sexual inexperience, Marinas relationships show warmth and thought.  Spyros, contemplative as he approaches death, shares with her how he believes, "Man has designed ruins with mathematical accuracy..." referring to the destiny of most architecture, eventually.  But then cynically, he reflects that " We (Greece) went from sheep to bulldozers...

When a stranger comes to town, Marina has her first sexual relationship with him. She is secretive.  Telling first Spyros and later Bella.  Spyros asks of course, "If you do not want me to meet him, why are you telling me about him?"

As Spyros come closer to death, Marina asks Bella to sleep with her father, as a favor for a dying man, whom she duly obliges. Meanwhile, Marina begins a sexual relationship with the stranger.

The film reaches its conclusion after Spyross passing, where the last scenes are of Bella and Marina scattering his ashes in the sea.

==Cast==
* Ariane Labed as Marina
* Vangelis Mourikis as Spyros
* Evangelia Randou as Bella
* Yorgos Lanthimos as the Engineer

==Reception==
 |accessdate=4 September 2011|date=2010-09-10}}  Peter Bradshaw characterised the film as "an angular, complex, absorbing and obscurely troubling movie".   

==Promotion==
A promotional picture for the film, where the tongues of two women meet, was censored on   profile for Attenberg. 

==Awards==
{| class="wikitable"
! Event
! Category
! Nominee
! Result
|- 67th Venice Venice Film Festival          Coppa Volpi for Best Actress Ariane Labed
| 
|- Golden Lion Athina Rachel Tsangari
| 
|- Lina Mangiacapre Award Athina Rachel Tsangari
| 
|- Whistler Film Festival      New Voices Award for Best International Feature Athina Rachel Tsangari
| 
|- Thessaloniki International Film Festival      Special Jury Award - Silver Alexander Athina Rachel Tsangari
| 
|- Angers European First Film Festival     
|"Mademoiselle Ladubay" Best Actress Prize Ariane Labed
| 
|- Mexico National University International Film Festival (FICUNAM)         Silver Puma for Best Director Athina Rachel Tsangari
| 
|- Audience Choice Award Athina Rachel Tsangari
| 
|- Buenos Aires International Festival of Independent Cinema      Best Director Athina Rachel Tsangari
| 
|- International Womens International Womens Film Festival      Best Feature Award Athina Rachel Tsangari
| 
|- Hellenic Film Academy Awards      Best Actress Ariane Labed
| 
|- ERA New New Horizons Film Festival      Grand Jury Prize Athina Rachel Tsangari
| 
|- Romanian International Film Festival      Best Film Award Athina Rachel Tsangari
| 
|- European Parliament Film Prize      LUX Prize Athina Rachel Tsangari
|Runner-up
|- AFI FEST      Special Jury Prize Athina Rachel Tsangari
| 
|-
|}

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Greek submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 