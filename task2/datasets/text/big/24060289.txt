Laughing at Life
{{Infobox film
| name           = Laughing at Life
| image_size     =
| image	=	Laughing at Life FilmPoster.jpeg
| caption        =
| director       = Ford Beebe
| producer       = Nat Levine
| writer         = Ford Beebe (story) Prescott Chaplin (screenplay) Thomas J. Dugan (screenplay)
| narrator       =
| starring       = See below
| music          = Tom Galligan Ernest Miller
| editing        = Joseph Kane Ray Snyder
| distributor    =
| released       = 12 July 1933
| runtime        = 71 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Laughing at Life is a 1933 American film directed by Ford Beebe.

== Plot summary ==
Easter, a soldier of fortune and gunrunner, leaves his family behind escaping from the authorities and an American detective named Mason. His globe hopping escape leads him finally to South America, where he is hired to organize a band of revolutionaries, unaware that they plan to eliminate him when his job is done. Here, also, he encounters his own son, on track to waste his own life in pursuits similar to Easters.

== Cast ==
*Victor McLaglen as Dennis P. McHale / Burke / Captain Hale
*Conchita Montenegro as Panchita
*William "Stage" Boyd as Inspector Mason Lois Wilson as Mrs. McHale
*Henry B. Walthall as President Valenzuela
*Regis Toomey as Pat Collins / Mc Hale Ruth Hall as Alice Lawton
*Guinn Big Boy Williams as Jones
*Dewey Robinson as Smith
*Ivan Lebedeff as Don Flavio Montenegro
*Mathilde Comont as Mamacita
*Noah Beery as Hauseman
*J. Farrell MacDonald as The Warden
*Tully Marshall as Stone
*Henry Armetta as Fruit Vendor
*Edmund Breese as Cabinet Officer
*Frankie Darro as Chango
*Otis Harlan as Businessman
*Buster Phelps as Young Pat Denny McHale
*Arthur Hoyt as Businessman Pat OMalley as Detective Agency Official William Desmond as Military Cabinet Officer
*Lloyd Whitlock as WWI Commanding Officer
*Philo McCullough as Capt. Valdez
*George Humbert as Beverage Vendor

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 