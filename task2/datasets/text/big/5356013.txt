Mann (film)
 
{{Infobox film
| name           = Mann
| image          = Maan.jpeg
| caption        = Dvd cover
| director       = Indra Kumar
| producer       = Indra Kumar Ashok Thakeria
| screenplay     = Aatish Kapadia
| starring       = Aamir Khan Manisha  Koirala Sharmila Tagore Neeraj Vora Rani Mukerji  (Special Appearance)
| music          = Sanjeev Darshan
| editing        = Hussain Burmawala
| cinematography = Basha Lal
| distributor    = Maruti International
| released       = July 9, 1999
| runtime        = 160 min.
| country       = India
| language       = Hindi
| budget         =
| gross  =  
|}}
 Tollywood titled Ravoyi Chandamama  starring  Akkineni Nagarjuna, Anjala Zhaveri, Keerthi Reddy and Jagapati Babu.

==Plot==
Dev Karan Singh (Aamir Khan), a casanova and ambitious painter deep in debt, agrees to marry Anita (Deepti Bhatnagar), the daughter of Singhania (Dalip Tahil), a rich tycoon. Priya (Manisha Koirala), a music teacher for children, is engaged to Raj (Anil Kapoor), whom she has agreed to marry because he had helped her when she was in need. Priya and Dev meet on a cruise and fall in love. However, due to their already being engaged to other people, they agree to work everything out and meet in 6 months on Valentines Day to get married.

During the 6 months, Dev breaks off his engagement with Anita and starts working hard. Driven by his love, he creates and auctions beautiful paintings, and becomes very successful. On the other hand, Priya realizes that leaving Raj will be wrong and sadly writes a letter to Dev, explaining everything. When Raj gets the letter instead, he supports Priya and convinces her to go to Dev. Things take a bad turn when, on her way to meet him, Priya gets hit by a car and gets her legs amputated. Dev, who waited all night for Priya, believes she has rejected him, as he does not know of her accident. Priya forbids Raj from telling Dev, not wanting to become a burden on him. However, both Dev and Priya still love and pine for each other.

Though heartbroken, Dev continues his career and goes on to become a famous painter. One day, Priya attends his art exhibition and wishes to buy a painting of his, a sentimental picture of her speaking to his beloved grandmother, whom Dev had introduced to her during the cruise. Dev had said that he would not sell the painting as it was only for Priya, but upon hearing that the girl who wanted it understands the emotion behind the picture and is disabled, he tells the host of the exhibition to give the painting to her for free.

One day, he visits Priya to give her an anklet that his grandmother, who recently died, had wished for Priya to have when she became Devs bride. Initially unaware of Priyas condition, Dev realizes the truth when he sees the painting in another room. The two embrace tearfully and he assures Priya that he will love her no matter what. Dev and Priya then get married and live happily ever after.

==Cast==
* Aamir Khan as Dev Karan Singh
* Manisha Koirala as Priya Verma
* Sharmila Tagore as Devs grandmother
* Anil Kapoor as Raj
* Deepti Bhatnagar as Anita Singhania
* Dalip Tahil as Pratap Rai Singhania
* Neeraj Vora as Nattu
* Satyen Kappu as Rajs uncle
* Anant Mahadevan as Creditor
* Paresh Ganatra
* Rani Mukerji ... Special Appearance in the song Kali Nagin Ke Jaisi Zulfain

==Box office==
It opened with a verdict of "Good", and at the end of its run it received a verdict of "Hit", though it has a cult following of both the film and its music.

==Casting==
Originally, Aishwarya Rai was offered the lead role of Priya and after she had declined due to prior date commitments, Manisha Koirala was roped in. Interestingly, the movie Hum Dil De Chuke Sanam starring Aishwarya was released at the same time and became an unexpected hit.

==Soundtrack==
The music for the movie was composed by the composer duo Sanjeev Darshan while lyrics were by Sameer (lyricist)|Sameer.

===Track listing===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Note(s)
|-
| 1
| "Chaha Hai Tujhko"
| Udit Narayan, Anuradha Paudwal
| Lifted from song "Etho Oru Paattu" from the Tamil movie Unnidathil Ennai Koduthen (1998)
|-
| 2
| "Kehna Hai Tumse"
| Udit Narayan, Hema Sardesai
| Lifted from song "Liquid" by Jars of Clay (1995)
|-
| 3
| "Kali Nagin Ke Jaisi"
| Udit Narayan, Kavita Krishnamurthy
| Composition from song "Ya Rayah" by Algerian singer Rachid Taha (1997).
|-
| 4
| "Khushiyan Aur Gham"
| Udit Narayan, Anuradha Paudwal Italian Pop pop group Ricchi & Poveri (1981)
|-
| 5
| "Kyon Chhupate Ho"
| Udit Narayan, Anuradha Paudwal
|
|-
| 6
| "Mera Mann Kyon Tumhe Chahe"
| Udit Narayan, Alka Yagnik
|
|-
| 7
| "Nasha Yeh Pyar Ka"
| Udit Narayan
|  Composition from the 1983 Italian pop song "LItaliano" by Toto Cutugno.
|-
| 8
| "Tinak Tin Tana"
| Udit Narayan, Alka Yagnik
| Lifted from song "Yang Sedang Sedang Saja" by Malaysia singer Iwan
|-
| 9
| "Tumhare Baghair Jeena Kya (Dialogue)"
| Aamir Khan
| Dialogue
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 