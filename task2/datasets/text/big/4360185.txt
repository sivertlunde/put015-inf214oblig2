The Fox and the Hound 2
 
{{Infobox film
| name        = The Fox and the Hound 2 
| image       = The Fox and the Hound 2 Coverart.png
| writer      = Roger S.H. Schulman 
| director    = Jim Kammerud 
| producer    = Ferrell Barron
| distributor = Buena Vista Pictures Walt Disney Home Entertainment 
| released    =  
| starring    = Reba McEntire Patrick Swayze Jonah Bobo Harrison Fahn Jeff Foxworthy Vicki Lawrence Stephen Root 
| runtime     = 69 minutes
| language    = English
}}
The Fox and the Hound 2 is a 2006 direct-to-video followup to the 1981 Disney animated film The Fox and the Hound. It was produced by DisneyToon Studios, directed by Jim Kammerud, and features the voice talents of Patrick Swayze and Reba McEntire. The story of the film takes place during the youth of Tod and Copper, in which Copper is tempted to join a band of singing stray dogs. It is the last direct-to-video film released by Disney before Disney released their own movies on the Blu-ray Disc format.

==Plot==
Best friends Tod, a   from the Grand Ole Opry will be at the fair.

Cash and Dixie then get into an argument, and Dixie walks off before their performance, forcing them to go on stage without her. During the show, Copper sings along, and Cash invites the pup up on stage to sing with them. The musical number is a success. Cash invites Copper to join the band, which he does after Tod lies that he is a stray dog|stray. Copper spends the entire day with Cash, forgetting his promise to watch the fireworks with Tod. Dixie finds Tod and sympathizes with his feelings of abandonment. During their conversation, Tod lets it slip that Copper isnt a stray, and Dixie hatches a plan to get Copper out of the band with Tods help. Tod lures Coppers owner, Slade, to the fair in a wild chase. The chase leads to widespread mayhem in the fair, and the Singin Strays performance is sabotaged right in front of the talent scout, Mr. Bickerstaff. After finding out that Copper has an owner, Cash fires Copper from the band and returns home with Slade. Granny Rose and the rest of the members of the band feel sorry for Copper about this and the band therefore breaks up. Tod tries to apologise to Copper for ruining everything and is brought home by his owner, Widow Tweed. Along the way, Tweed narrowly misses being hit by the talent scouts car, and Bickerstaffs hat flies off and lands on Tod.

The following day, Tod and Copper admit their mistakes and are friends again. Hoping to amend for his doings, Tod gives Bickerstaffs hat to Copper, who uses it to track down the talent scout at a local diner. Tod tricks Cash and Dixie into thinking the other is in trouble, and the entire band end up meeting up at the diner. Copper convinces the band the importance of harmony, and The Singin Strays howl a reprise of their song "Were in Harmony", attracting the attention of the talent scout. Impressed with the band, he arranges for the dogs to perform at the Grand Ole Opry. The film ends with Copper choosing to leave the band and be best friends with Tod again.

==Voice cast==
* Jonah Bobo as Tod
* Harrison Fahn as Copper
* Patrick Swayze as Cash
* Reba McEntire as Dixie
* Jeff Foxworthy as Lyle
* Vicki Lawrence as Granny Rose
* Rob Paulsen as Chief
* Jim Cummings as Waylon & Floyd
* Stephen Root as Winchell P. Bickerstaff (the Talent Scout)
* Russi Taylor as Widow Tweed
* Jeff Bennett as Amos Slade

==Soundtrack==
{{Infobox album 
| Name        = The Fox and the Hound 2 Soundtrack Album 
| Type        = Soundtrack 
| Artist      = Various Artists 
| Cover       = foxhoundsound.jpg 
| Released    = November 21, 2006 
| Recorded    = 2005&ndash;2006 
| Genre       = Country music|Country, Country pop Walt Disney
}}

{{Album ratings
| rev1      = Allmusic
| rev1Score =   
}}
 bluegrass writers Nashville by Disney according to the music supervisor Kimberly Oliver, and Matt Walker Senior VP, DisneyToon Studios. Background music score composer instrumental songs Joel McNeely. Bluegrass music for setting the moods of scenes, performed by several famous bluegrass performers.  

===Track listing===
{{Track listing
| extra_column    = Performer(s)
| writing_credits = yes
| title1   = Friends for Life
| writer1  = Marcus Hummon
| extra1   = One Flew South
| length1  = 4:00
| title2   = Nashville 7 Mike Marshall
| extra2   = 
| length2  = 1:41
| title3   = Were in Harmony
| writer3  = Will Robinson
| extra3   = Chip Davis
| length3  = 1:29
| title4   = Hound Dude
| writer4  = Will Robinson
| extra4   = Josh Gracin
| length4  = 2:17
| title5   = Depressed Dixie
| writer5  = Joel McNeely
| extra5   = 
| length5  = 1:28
| title6   = Good Doggy, No Bone!
| writer6  = Marcus Hummon
| extra6   = Reba McEntire
| length6  = 3:13
| title7   = Sticky Hound Puppy
| writer7  = Joel McNeely
| extra7   = 
| length7  = 2:48
| title8   = Blue Beyond Gordon Kennedy and Blair Masters
| extra8   = Trisha Yearwood
| length8  = 3:08
| title9   = We Go Together
| writer9  = Marcus Hummon
| extra9   = Little Big Town with The Singin Strays
| length9  = 3:09
| title10  = You Know I Will Gordon Kennedy
| extra10  = Reba McEntire
| length10 = 3:21
| title11  = Sad Puppy
| writer11 = Joel McNeely
| extra11  = 
| length11 = 2:16
| title12  = Were in Harmony (Finale) 
| writer12 = Will Robinson
| extra12  = The Singin Strays with Copper
| length12 = 1:53
}}

==References==
 

==External links==
 
*  
*  
*  
*   Official Disney DVD site
*  
*   Animated News & Views interview
*   on Allmusic

 

 
 
 
 
 
 
 
 
 
 
 
 