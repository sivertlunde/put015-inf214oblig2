Lützow's Wild Hunt
Lützow’s German silent silent war film.

==The song== Theodor Körner, Wars of Liberation. It was set to music by Carl Maria von Weber and became very popular.

The song praises the deeds of the Free Corps that became an essential part of Germany’s national identity in the 19th century due to its famous members. Besides Körner, “Turnvater” Friedrich Ludwig Jahn, the famous poet Joseph von Eichendorff, the inventor of the kindergarten Friedrich Fröbel, and Eleonore Prochaska, a woman who had dressed as a man in order to join the fight against the French, served in the Corps.

{| class="wikitable"
!German
!English 
|-
| 1. Was glänzt dort im Walde im Sonnenschein?
Hör’s näher und näher brausen.
Es zieht sich herunter in düsteren Reihn
Und gellende Hörner schallen darein,
Erfüllen die Seele mit Grausen.
Und wenn ihr die schwarzen Gesellen fragt:
Das ist Lützows wilde, verwegene Jagd! 
| 1. What glistens there in the forest sunshine?
Hear it roaring nearer and nearer.
It comes down this way in dark rows,
And blaring horns sound in it,
And fill the soul with terror.
And if you ask the black fellows:
That is Lützow’s wild daredevil hunt. 
|-
| 2. Was zieht dort rasch durch den finsteren Wald
Und streifet von Bergen zu Bergen?
Es legt sich in nächtlichen Hinterhalt,
Das Hurra jauchzt, die Büchse knallt,
Es fallen die fränkischen Schergen.
Und wenn ihr die schwarzen Jäger fragt
Das ist Lützows wilde, verwegene Jagd! 
| 2. What moves quickly there through the dark forest
And streaks from mountains to mountains?
It settles down for a night ambush,
The Hurrah rejoices and the gun bangs,
The French bloodhounds fall.
And if you ask the black hunters:
That is Lützow’s wild daredevil hunt. 
|-
| 3. Wo die Reben dort glühen, dort braust der Rhein,
Der Wütrich geborgen sich meinte.
Da naht es schnell mit Gewitterschein
Und wirft sich mit rüstigen Armen hinein
Und springt an das Ufer der Feinde.
Und wenn ihr die schwarzen Schwimmer fragt:
Das ist Lützows wilde, verwegene Jagd! 
| 3. Where the grapes glisten there, there roars the Rhine,
The scoundrel thought himself hidden.
Then it approaches quickly, looking like a thunderstorm,
And throws itself in with vigorous arms,
And springs onto the enemy’s riverbank.
And if you ask the black swimmers:
That is Lützow’s wild daredevil hunt. 
|-
| 4. Was braust dort im Tale die laute Schlacht,
Was schlagen die Schwerter zusammen?
Wildherzige Reiter schlagen die Schlacht,
Und der Funke der Freiheit ist glühend erwacht
Und lodert in blutigen Flammen.
Und wenn ihr die schwarzen Reiter fragt:
Das ist Lützows wilde, verwegene Jagd! 
| 4. Why roars there in the valley the loud battle,
Why do the swords strike one another?
Wild-hearted riders attack the fight,
And the spark of freedom has awakened, glowing,
And smolders in bloody flames.
And if you ask the black riders:
That is Lützow’s wild daredevil hunt. 
|-
| 5. Was scheidet dort röchelnd vom Sonnenlicht
Unter winselnde Feinde gebettet?
Es zucket der Tod auf dem Angesicht,
Doch die wackeren Herzen erzittern nicht,
Das Vaterland ist ja gerettet!
Und wenn ihr die schwarzen Gefall’nen fragt:
Das ist Lützows wilde, verwegene Jagd. 
| 6. What departs there, rattling, from the sunlight,
Put to bed among whimpering enemies?
Death twitches across the face;
Yet bold hearts do not waver,
For the fatherland is indeed saved!
And if you ask the black fallen ones:
That was Lützow’s wild daredevil hunt. 
|-
| 7. Die wilde Jagd und die deutsche Jagd
Auf Henkersblut und Tyrannen!
D’rum, die ihr uns liebt, nicht geweint und geklagt!
Das Land ist ja frei, und der Morgen tagt,
Wenn wir’s auch nur sterbend gewannen.
Und von Enkel zu Enkel sei es nachgesagt:
Das war Lützows wilde, verwegene Jagd. 
| 7. The wild hunt, and the German hunt,
Upon hangmen’s blood and tyrants!
Therefore, those who love us, no weeping and lamenting;
For the land is free, and morning dawns,
Even if we only won this by dying!
And from grandchildren to grandchildren be it said:
That was Lützow’s wild daredevil hunt. 
|-
|}

The tune was adopted as the regimental march of the 1st Surrey Rifles, a Volunteer unit of the British Army. 

==The movie==

{{Infobox film
| name           = Lützows Wild Hunt
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald 
| producer       = Richard Oswald Theodor Körner (poem)   Max Jungk
| narrator       = 
| starring       = Ernst Rückert   Arthur Wellin   Mary Kid   Paul Bildt
| music          = Gustav Gold
| editing        =
| cinematography = Ewald Daub
| studio         = Richard-Oswald Film
| distributor    =  Richard-Oswald Film 
| released       = 21 February 1927
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| 
}}
 German silent silent war French during Theodor Körner.

===Cast===
* Ernst Rückert as Theodor Körner 
* Arthur Wellin as Major von Lützow 
* Mary Kid as Toni Adamberger, Schauspielerin am Burgtheater 
* Paul Bildt as Napoleon, Kaiser der Franzosen 
* Wera Engels as Eleanore Prochaska, ein Bürgermädchen 
* Gerd Briese as Graf von Seydlitz 
* Sig Arno as Franz I, Kaiser von Österreich  
* Leopold von Ledebur as Goethe 
* Albert Steinrück as Beethoven 
* Friedrich Kühne as Fürst Metternich 
* Harry Nestor as Friedrich Wilhelm III, König von Preußen 
* Robert Hartberg as Erzherzog Karl von Österreich 
* Carl Zickner as Fouché, Minister von Frankreich 
* Eduard von Winterstein as Blücher  Paul Marx as Hardenberg 
* Eugen Jensen as Freiherr vom Stein 
* Josef Karma as Direktor des Burgtheaters 
* Hugo Döblin as Burgtheaterfaktotum 
* Emil Sondermann as Schmierendirektor 
* Theodor Burghardt

==References==
 

==Bibliography==
* Anon, A War Record of the 21st London Regiment (First Surrey Rifles), 1914–1919, 1927/Uckfield: Naval & Military, 2003, ISBN 1-843426-19-6.

* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 