Big Fat Liar
{{Infobox film
| name = Big Fat Liar
| image = Big Fat Liar film.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Shawn Levy Mike Tollin Marie Cantin Dan Schneider
| story = Dan Schneider Brian Robbins
| starring = Frankie Muniz Paul Giamatti Amanda Bynes
| music = Christophe Beck
| cinematography = Jonathan Brown
| editing = Stuart Pappé Kimberly Ray
| studio = Tollin/Robbins Productions|Tollin/Robbins 21 Laps Entertainment
| distributor = Universal Pictures
| released =  
| runtime = 88 minutes
| country = United States
| language = English
| budget = $15 million
| gross = $52,970,014
}}
 Dan Schneider and Brian Robbins, and starring Frankie Muniz, Paul Giamatti, and Amanda Bynes. The film involves a 14-year-old pathological liar named Jason Shepherd (Muniz), whose creative writing assignment is stolen by an arrogant Hollywood producer named Marty Wolf (Giamatti), who plans to use it to make the fictional film of the same name.

==Plot==
Jason Shepherd (Frankie Muniz) is a 14-year-old chronic liar living in the fictional town of Greenbury, Michigan who is deceiving and misleading his way out of trouble. Jason tries to get out of writing his 1000 word essay by making up a lie, but he gets caught. He is given three hours to submit his paper, otherwise he will fail English and go to summer school. Jason accidentally forgets his report in a limousine of Marty Wolf (Paul Giamatti). Wolf initially attempts to give it back to Jason, but when he sees that it is excellent, he decides to keep it for himself.

Jason realizes his paper is missing, and neither his parents, Harry and Carol (Michael Bryan French and Christine Tucci), or his Literature teacher, Ms. Phyllis Caldwell (Sandra Oh), believe him and he is sent to summer school. Later, after a horrible and disgusting first day at summer school, Jason and his best friend, Kaylee (Amanda Bynes), find out that Wolf has plagiarized his paper and is turning it into a film. Jason and Kaylee fly to Los Angeles to confront Wolf. Upon their arrival, they trick limo driver and struggling actor Frank Jackson (Donald Faison) into giving them a ride to Wolfs studio, where Jason tricks its receptionist, Astrid Barker (Rebecca Corry), into letting him speak with Wolf. Jason sneaks into Wolfs office to tell him to call his dad and tell him that he stole the story from Jason, but instead Wolf "accidentally" burns Jasons paper and removes him and Kaylee from his office. Angered, Jason and Kaylee plan to inconvenience Wolf until he admits to having stolen Big Fat Liar.  Frank, having discovered their true identities, joins them in their plan, as he has had a troubled history with Wolf. After gathering information about Wolfs treatment of his employees, Jason and Kaylee sabotage Wolf by going as far as dyeing his skin blue and his hair orange; sending him to a childs birthday party, where he is mistaken for a clown and beaten up by the party-goers, and modifying the controls to his car.
 Universal Pictures, Marcus "Marc" Duncan (Russell Hornsby). After another fictional film, Whitaker and Fowl, proves to be a critical and box-office failure, Duncan loses confidence in Wolf and threatens to pull production for Big Fat Liar. Jason approaches Wolf and agrees to help in exchange for his confession to having stolen the story. Guided by Jason, Wolf makes a successful presentation which convinces Duncans wife, Shandra (Chris Ott), to green-light Big Fat Liar, but Duncan warns Wolf that any mistakes will cause Universal to pull funding for it and end his career. However, Wolf betrays Jason again and kicks him out from his hiding place. Wolfs assistant, Monty Kirkham (Amanda Detmer), grows tired of his abuse and decides to help Jason and Kaylee to expose him. Jason and Kaylee rally all of Wolfs employees and devise a plan to stop him once and for all. As Wolf heads to the studio to begin shooting, many of his employees cause him to be late through multiple mishaps. As Wolf finally arrives to the studio, he witnesses Jason kidnapping his stuffed monkey, Mr. Funny-Bones. After a chase across the studio, Wolf is tricked into confessing that he stole Jasons story and Duncan, disgusted that Wolf would steal from a young boy, immediately fires him from Universal due to his actions. Jason thanks Wolf for teaching him the importance of telling the truth. After escaping from Wolf and reuniting with his parents, Jason finally re-establishes his trust with them.

In the epilogue, Universal later reproduces Big Fat Liar, utilizing the talents and skills of all those whom Wolf had abused, and the film is released in theaters, with Jason being credited during the closing credits for writing his original story. Meanwhile, Wolf, having been stripped of his career, declares bankruptcy and begins his new job as a clown, where he is kicked in the testicles by Darren, the birthday boy and the son of The Masher (Brian Turk), a monster truck driver whom he had earlier insulted.

==Cast==
* Frankie Muniz as Jason Shepherd, a 14-year-old pathological liar and slacker who, despite his slacker personality, is actually very intelligent
* Paul Giamatti as Marty Wolf, an arrogant Hollywood producer and compulsive liar who, unlike Jason, does not care how his lies affect other people
* Amanda Bynes as Kaylee, Jasons best friend and girlfriend
* Donald Faison as Frank Jackson, a limo driver and struggling actor who helps Jason and Kaylee in their mission to get Wolf to admit the truth
* Russell Hornsby as Marcus "Marc" Duncan, Wolfs boss and president of Universal Studios
* Amanda Detmer as Monty Kirkham, Wolfs long suffering assistant
* Michael Bryan French and Christine Tucci as Harry and Carol Shepherd, Jasons parents
* Sandra Oh as Ms. Phyllis Caldwell, Jasons English teacher
* Alex Breckenridge as Janie Shepherd, Jasons irresponsible older sister
* Rebecca Corry as Astrid Barker, the dog-loving receptionist at the Wolf Pictures office Urkel
* Lee Majors as Vince, an aging but nevertheless qualified stunt director
* Sean OBryan as Leo
* Amy Hill as Jocelyn Davis
* John Cho as Dustin Dusty Wong, the director
* Taran Killam as Bret Callaway. He is a skateboard punk who consistently bullies Jason, and also has a crush on Kaylee.
* Jake Minor as Aaron
* Kyle Swann as Brett
* Sparkle as Grandma Pearl, Kaylees senile grandmother
* Chris Ott as Shandra Duncan, Duncans wife
* Kenan Thompson, Dustin Diamond, Shawn Levy, Corinne Reilly, and Bart Myer as Wolf party guests
* Brian Turk as The Masher, a monster truck driver
* John Gatins as George, a tow truck driver
* Don Yesso as Rocko Malone

==Filming== Flash Flood Pasadena and Whittier, California.

==Soundtrack==
{{Track listing
| headline        = 
| writing_credits = yes Come on Come on
| writer1         = Smash Mouth
| length1         = 2:33
| title2          = Conant Gardens
| writer2         = Slum Village
| length2         = 3:03
| title3          = Me Myself and I
| writer3         = Jive Jones
| title4          = I Wish
| writer4         = Hairbrain
| length4         = 3:11
| title5          = Eye of the Tiger Survivor
| length5         = 4:29
| title6          = Hungry Like the Wolf
| writer6         = Duran Duran
| length6         = 3:41
| title7          = Blue (Da Ba Dee)
| writer7         = Eiffel 65
| length7         = 4:40
| title8          = Diablo
| writer8         = Triple Seven
| title9          = Disco Inferno
| writer9         = The Trammps
| length9         = 10:54
| title10        = Party Time
| writer10       = The Grand Skeem
| length10      = 3:32
| title11       = Backlash
| writer11       = The Grand Skeem
| title12       = Where ya at
| writer12       = The Grand Skeem
| title13       = Mind Blow
| writer13       = Zion-1
| title14       = Right Here Right Now
| writer14       = Fatboy Slim
| title15       = Move It Like This
| writer15       = Baha Men
| length15      = 3:51
}}

==Reception==
Big Fat Liar received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 43%, based on 92 reviews, with the sites consensus stating "Though theres nothing that offensive about Big Fat Liar, it is filled with Hollywood cliches and cartoonish slapstick, making it strictly for kids."  On Metacritic, the film has a score of 36 out of 100, based on 24 critics, indicating "generally negative reviews". 

On the positive side, Ebert and Roeper gave the film "Two Thumbs Up". Some critics praised the film as energetic and witty; others called it dull and formulaic. Critic David Palmer gave the film a 7/10, stating that it is a fun movie for people who love the behind the scenes of making movies, and "not awful considering its a kids film".

==Box office== Collateral Damage ($15.1 million). The movie would go on to gross $40.4 million domestically and $4.6 million in other countries for a total of $52.97 million,  more than tripling the $15 million budget.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
  
 
 
 
 
 
 
 