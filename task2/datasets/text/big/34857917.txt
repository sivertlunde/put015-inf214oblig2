Tears of April
 
{{Infobox film
| name           = Tears of April
| image          = 
| alt            = 
| caption        = 
| director       = Aku Louhimies
| producer       = Aleksi Bardy
| writer         = Jari Rantala
| based on       =  
| starring       = Samuli Vauramo Pihla Viitala Eero Aho
| music          = Pessi Levanto
| cinematography = Rauno Ronkainen
| editing        = Benjamin Mercer
| studio         = Helsinki-filmi
| distributor    = FS Film
| released       =  
| runtime        = 
| country        = Finland
| language       = Finnish
| budget         = €1.5 million 
| gross          = €274,079 (domestic) 
}} Finnish war drama film directed by Aku Louhimies. Based on the novel Käsky by Leena Lander, the film is set in the final stages of the Finnish Civil War. 

== Cast  ==
* Samuli Vauramo as Aaro Harjula
* Pihla Viitala as Miina Malin
* Eero Aho as Emil Hallenberg
* Eemeli Louhimies as Eino
*   as Martta
* Riina Maidre as Beata Hallenberg
* Sulevi Peltola as Konsta
* Oskar Pöysti as Paasonen

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 
 