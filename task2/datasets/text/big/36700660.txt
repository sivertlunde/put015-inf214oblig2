Terang Boelan
 
{{Infobox film
| name           = Terang Boelan
| image          = Terang Boelan p311.jpg
| image_size     = 
| border         = yes
| alt            = A black-and-white poster Batavia
| director       = Albert Balink
| producer       = 
| screenplay     = Saeroen
| story          = 
| starring       = {{plainlist|
*Rd Mochtar
*Roekiah
*Eddie T. Effendi
*Kartolo
}}
| music          = Ismail Marzuki
| cinematography = {{plainlist| Joshua Wong Othniel Wong
}}
| editing        = 
| studio         = Algemeen Nederlandsch Indisch Filmsyndicaat
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies   (now Indonesia) 
| language       = Malay Indonesian
| budget         = 
| gross          = 
}} Indonesian for native audiences and included keroncong music, which was popular at the time, and several actors from Balinks previous work Pareh (1936).
 Malay audiences lost since at least the 1970s.

==Plot== Terang Boelan" for Rohaya, and they agree to elope. The following day, Rohaya and Kasim escape from Sawoba Island to Malacca, where Kasim begins work at a drydock and Rohaya keeps busy as a housewife. They discover that Kasims old friend, Dullah (Kartolo), has lived in Malacca for some time.

Their life together is interrupted when Musa, who is revealed to be an opium dealer, discovers them. While Kasim is away at work, Rohayas father (Muhin) comes and takes her back to Sawoba. Kasim, having discovered Musas deeds, also returns to Sawoba and rallies the villagers to his side by telling them of Musas opium dealings. He and Musa begin fighting. When it appears Kasim may lose, he is saved by Dullah, who had followed him back to Sawoba. The villagers and Rohayas father agree that Kasim and Rohaya should be together, as they are truly in love.  and  .}}

==Background== native audiences because of their action sequences.  The Teng Chuns dominance was an effect of the Great Depression and changing market trends. The Great Depression had led to the Dutch East Indies government collecting higher taxes and cinemas selling tickets at lower prices, ensuring that there was a very low profit margin for local films. As a result, cinemas in the colony mainly showed Hollywood productions, while the domestic industry decayed.  The Teng Chun was able to continue his work only because his films often played to full theatres. 

In an attempt to show that locally produced, well-made films could be profitable, the Dutch journalist Albert Balink, who had no formal film experience,  produced Pareh (Rice) in 1935 in collaboration with  the ethnic Chinese Wong brothers (Othniel and Joshua),  and the Dutch documentary filmmaker Mannus Franken.  The film cost 20&nbsp;times as much as an average local production,  in part because of Balinks perfectionism, and was ultimately a failure. The Indonesian writer and cultural critic Armijn Pane wrote that Pareh had performed poorly with native audiences as it was seen as looking at them through European eyes.  Pareh bankrupted its producers,  and enabled The Teng Chun to dominate the industry&nbsp;– although with less traditional stories&nbsp;– for a further two years. 

==Production==
 ,   1947; the Wongs collaboration with Balink on Terang Boelan was their second, after Pareh.]] Batavia (now Jakarta). Although this new establishment focused mainly on newsreels and documentaries, on 1&nbsp;January 1937 ANIF announced that it would produce several feature films, one of which was Terang Boelan. 

The story for Terang Boelan was written by Saeroen, a reporter with the newspaper Pemandangan who had close connections to the theatrical community, shortly after the domestic release of the American-produced Dorothy Lamour vehicle The Jungle Princess (1936), which served as an inspiration.  The Indonesian film historian Misbach Yusa Biran wrote that this gave Terang Boelan stylistic and thematic similarities to the earlier film.  The Indonesian film critic Salim Said also recognised such similarities, describing Terang Boelan as reflecting the "jungle princess" works popular at the time.  Saeroen named the fictional island on which Terang Boelan takes place "Sawoba" after the crew: Saeroen, Wong, and Balink. 

Production had begun by February 1937, under Balinks direction and with the Wongs as cinematographers, only to be interrupted by the relocation of ANIFs offices. Filming had begun by May of that year.  Sources conflict as to whether Franken was involved: Biran wrote that Franken had been left in charge of the studios documentaries,  while the American film scholar Karl G. Heider recorded Franken as co-directing the film.  As opposed to The Teng Chun, who aimed his films at lower-class audiences, Balink aimed his film at educated native Indonesians, attempting to show them not from a European perspective but as they viewed themselves.  According to Said, this arose as a reaction to  }} failure and resulted in a less ethnological approach.  Terang Boelan was shot in black-and-white using highly flammable nitrate film at Cilincing in Batavia, Merak Beach in Banten, and Tanjong Katong in Singapore.  The use of nitrate film may have been a factor in the films later loss. 
 Portuguese influences); because Mochtars voice was ill-suited to the task, the musician Ismail Marzuki&nbsp;– who also composed the films score&nbsp;– sang while Mochtar lip synced. 

==Release and reception== University of Newcastle in Australia, notes that the film continued a trend of "Indonesianisation", or the application of a national (Indonesian) understanding to borrowed concepts; for Terang Boelan this indigenisation process involved the inclusion of "exotic local settings" and keroncong music. Such adaptations of foreign films had arisen several years earlier and continued long after Terang Boelan  release. 
 RKO Radio Pictures, the film was screened in British Malaya, where it was advertised as "the first and best Malay musical" and earned 200,000 Straits dollars (then equivalent to US$ 114,470  ) in two months.  Terang Boelan proved to be the most successful production in the area until Krisis (Crisis) in 1953, released after the Netherlands recognised Indonesias independence in 1949. 
 Indo man named J.J.W. Steffens,  suggested that ANIFs management preferred works of non-fiction as a more intellectual medium. Disappointed by the companys reaction, Balink left the Indies and emigrated to the United States in March 1938.  Terang Boelan  cast left ANIF not long afterwards and, after briefly touring Malaya, joined Tans Film.  They made their first film for Tans, Fatima (1938 film)|Fatima, in 1938. Mochtar, who soon married fellow Terang Boelan actress Soekarsih, continued to be cast as Roekiahs lover; the two were a popular screen couple until Mochtar left Tans in 1940 over a wage dispute. 

==Legacy==
  and Rd Mochtar (pictured in Siti Akbari) continued to be cast as lovers until 1940.]] films released Japanese occupation Run Run Malay audiences, established Malay Film Productions in Singapore, where it became one of the more successful production houses. 

Heider considered Terang Boelan one of the two most important cinematic works from the Dutch East Indies during the 1930s; Balinks earlier film Pareh was the other. He notes that Terang Boelan "set the tone for popular Indonesian cinema", a tone that remained dominant into the 1990s.  Biran considered the film a turning point in the history of Indonesian cinema, showing the possibilities of the medium and serving as a catalyst for further development.  Said concurred, describing the film as a milestone in Indonesias history because of the widespread formula it introduced.  The repeated use of Terang Boelan  formula has been criticised. The director Teguh Karya, for instance, denounced films that used it without building on it, leaving the formula "undeveloped and static". 

Terang Boelan is considered Lost film|lost,  as are most domestic productions from the era.  writes that all Indonesian films from before 1950 are lost. However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and   writes that several Japanese propaganda films have survived at the Netherlands Government Information Service.}} The Filipino film historian and director Nick Deocampo noted that productions made with nitrate film&nbsp;– such as Terang Boelan&nbsp;– burned easily and were thus easily lost, but suggested that copies of the film may have survived until the 1970s.  In a 1991 publication Said, Heider, and the American translator John H. McGlynn expressed hope that a copy of the film might be lying around in someones attic or closet in Indonesia or the Netherlands. 

==See also==
*List of lost films

==Explanatory notes==
 

==Footnotes==
 

==Works cited==
 
*{{cite news
 |title=Albert Balink
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011070854%3Ampeg21%3Ap003%3Aa0076
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=3
 |date=14 March 1938
 |publisher=Kolff & Co.
 |accessdate=22 January 2013
 |ref= 
}}
*{{cite web
 |title=Albert Balink
 |url=http://www.jakarta.go.id/jakv1/encyclopedia/detail/782
 |language=Indonesian
 |publisher=Jakarta City Government
 |work=Encyclopedia of Jakarta
 |accessdate=24 July 2012
 |archivedate=24 July 2012
 |archiveurl=http://www.webcitation.org/69NvTik1w
 |ref= 
}}
*{{cite book
 |url=http://books.google.ca/books?id=YWHjTT3FEycC
 |title=Sejarah Kecil "petite histoire" Indonesia
 |trans_title=A Short History of Indonesia
 |language=Indonesian
 |isbn=978-979-709-428-7
 |last=Anwar
 |publisher=Kompas
 |year=2004
 |location=Jakarta
 |first=Rosihan
 |ref=harv
}}
*{{cite journal
  |doi=10.1017/S0022463409990257
  |ref=harv
  |title=Film Melayu: Nationalism, Modernity and Film in a pre-World War Two Malay Magazine
  |date=February 2010
  |last=Barnard
  |first=Timothy P.
  |journal=Journal of Southeast Asian Studies
  |volume=41
  |issue=1
  |url=http://journals.cambridge.org/action/displayAbstract?fromPage=online&aid=6881324&fileId=S0022463409990257
  |publisher=McGraw-Hill Far Eastern Publishers
  |location=Singapore
  |issn=0022-4634
  |pages=47–70
}}
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |location=Jakarta
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite book
 |title=Lost Films of Asia
 |editor1-last = Deocampo
 |editor1-first =  Nick
 |publisher=Anvil
 |location=Manila
 |year=2006
 |isbn=978-971-27-1861-8
 |ref=harv
}}
*{{cite news
 |title=Een Film in Wording: Interessante avondopnamen.
 |trans_title=A Film in Making: Interesting Night Shots
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010285943%3Ampeg21%3Ap006%3Aa0121
 |work=De Indische Courant
 |location=Surabaya
 |page=6
 |date=3 June 1937
 |accessdate=22 January 2013
 |ref= 
}}
*{{cite book
 |title=Ismail Marzuki: Musik, Tanah Air, dan Cinta
 |trans_title=Ismail Marzuki: Music, Homeland, and Love
 |language=Indonesian
 |isbn=978-979-3330-36-5
 |last1=Esha
 |first1=Teguh
 |last2=Alhaziri
 |first2=Wasmi
 |last3=Fauzi
 |first3=Muhammad
 |last4=Donald W.
 |first4=Sabu
 |last5=Sigarlaki
 |first5=Erwin R.
 |year=2005
 |ref=harv
 |publisher=LP3ES
 |location=Yogyakarta
}}
*{{Cite journal
 |title=Foreign Exchange
 |date=8 June 1938
 |work=The New York Times
 |location=New York
 |accessdate=24 November 2012
 |url=http://select.nytimes.com/gst/abstract.html?res=F40F17F7395C1B7A93CBA9178DD85F4C8385F9
 |ref= 
}}  
*{{cite book
 |url=http://books.google.ca/books?id=k3HTdu1HuWQC
 |title=Malaysian Cinema, Asian Film: Border Crossings and National Cultures
 |isbn=978-90-5356-580-3
 |author1=van der Heide
 |first1=William
 |year=2002
 |ref=harv
 |location=Amsterdam
 |publisher=Amsterdam University Press
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=qWklvbDlYc4C
 |title=Reclaiming Adat: Contemporary Malaysian Film and Literature
 |isbn=978-0-7748-1172-9
 |author1=Khoo
 |first1=Gaik Cheng
 |year=2006
 |ref=harv
 |publisher=University of British Columbia Press
 |location=Vancouver
}}
* {{cite web
  | title =Kredit Lengkap
  |trans_title=Full Credits
  |language=Indonesian
  |work=Filmindonesia.or.id
  |publisher=Konfidan Foundation
  | url =http://filmindonesia.or.id/movie/title/lf-t012-37-272359_terang-boelan/credit
  | accessdate =12 August 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/69rBsyoce
  |archivedate=12 August 2012
  }}
*{{cite news
 |title=Maleische Muzikale Film Terang Boelan
 |trans_title=Malay Musical Film Terang Boelan
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010285352%3Ampeg21%3Ap006%3Aa0112
 |work=De Indische Courant
 |location=Surabaya
 |page=6
 |date=19 April 1937
 |accessdate=22 January 2013
 |ref= 
}}
*{{cite web
 |title=Pareh
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-p005-35-063498_pareh#.UAyscqDE_Mw
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=23 July 2012
 |archiveurl=http://www.webcitation.org/69Mlnc59M
 |archivedate=23 July 2012
 |ref= 
}}
*{{cite book
 |title=Profil Dunia Film Indonesia
 |trans_title=Profile of Indonesian Cinema
 |language=Indonesian
 |last=Said
 |first=Salim
 |publisher=Grafiti Pers
 |location=Jakarta
 |year=1982
 |oclc=9507803
 |ref=harv
}}
*{{cite book
 |title=Cinema of Indonesia: Eleven Indonesian Films, Notes & Synopses
 |last1=Said
 |first1=Salim
 |last2=McGlynn
 |first2=John H.
 |author2-link=John H. McGlynn
 |last3=Heider
 |first3=Karl G
 |author3-link=Karl G. Heider
 |publisher=Festival of Indonesia Foundation
 |location=New York
 |year=1991
 |isbn=978-1-879578-00-5
 |ref=harv
}}
*{{cite news
 |title=Terang Boelan
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011070854%3Ampeg21%3Ap003%3Aa0076
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=3
 |date=9 December 1937
 |publisher=Kolff & Co.
 |accessdate=22 January 2013
 |ref= 
}}
*{{cite web
 |title=Terang Boelan
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-t012-37-272359_terang-boelan
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=24 July 2012
 |archiveurl=http://www.webcitation.org/69OO8lfqA
 |archivedate=24 July 2012
 |ref= 
}}
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 