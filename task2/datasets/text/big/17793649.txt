Trance (1998 film)
{{Infobox film
| name           = Trance
| image          = Eternal cover.jpg
| caption        = DVD cover for the Trance
| director       = Michael Almereyda
| producer       = {{Plainlist|
* Mark Amin
* David L. Bushell
* Andrew Fierberg}}
| writer         = Michael Almareyda
| starring       = {{Plainlist|
* Alison Elliott
* Jared Harris
* Christopher Walken}}
| music          = Simon Fisher-Turner
| cinematography = Jim Denault
| editing        = Tracy Granger Steve Hamilton
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $4 million
| gross          =
}} 1998 horror The Mummy, although it isnt. It was directed and written by Michael Almereyda. The films score features music by Mark Geary. It premiered on Toronto Film Festival, and was released as direct-to-video in the USA, UK and many parts of the world including Argentina, Germany, Spain, Azerbaijan, Russia and many more.

== Plot ==
Nora (Elliott) is a young American woman of Irish origin who suffers from alcoholism. She and her husband Jim (Harris) are coming home from a night of drinking when Nora has an accident, tumbling down the stairs of their New York apartment building. Nora survives the fall, but is soon visited by headaches, nosebleeds and hallucinations. comes to life and attempts to steal the body, soul and identity of her hapless descendant.

== Cast ==
* Alison Elliott as Nora / Niamh
* Rachel ORourke as Alice
* Jared Harris as Jim
* Jeffrey Goldschrafe as Jim, Jr.
* Christopher Walken as Uncle Bill Ferriter
* Lois Smith as Mrs. Ferriter
* Sinead Dolan as Noras mother
* Raina Feig as Young Nora Jason Miller as The Doctor
* Paul Ferriter as Joe
* David Geary as Noras Father

== Critical reception ==
Trance received mixed reviews from critics. Film review aggregate website Rotten Tomatoes gave the film a "rotten" score of 33% based on 10 reviews.  For David Nusair (from Reel Film Reviews) film is "just silly and not the least bit scary". Mark R. Leeper said for film "This is the kind of film you used to see in the 60s from small studios like Tigon" and also he said "Once it gets going it is entertaining but it would be hard to claim it is actually a good film."  

== Awards and nomination ==
{| class="wikitable"
! colspan="5" align="center"|Sitges - Catalonian International Film Festival
|-
! width="33"| Year
! width="200"| Nomination
! width="100"| Result
! width="200"| Category
|-
| align="center" rowspan="2"| 1998
| align="center"| Jared Harris
| align="center"| Won
| align="center"| Best Actor
|-
| align="center"| Michael Almereyda
| align="center"| Nominated
| align="center"| Best Film
|-
|}

== See also ==
*Christopher Walken filmography
* 

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 