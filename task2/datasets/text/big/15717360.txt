Three Smart Girls Grow Up
{{Infobox film
| name           = Three Smart Girls Grow Up
| image          = Three Smart Girls Grow Up poster.jpg
| caption        = Theatrical release poster
| director       = Henry Koster
| producer       = Joe Pasternak
| screenplay     = {{Plainlist|
* Felix Jackson
* Bruce Manning
}}
| starring       = {{Plainlist|
* Deanna Durbin
* Nan Grey
* Helen Parrish
}} Frank Skinner
| cinematography = Joseph A. Valentine
| editing        = {{Plainlist|
* Bernard W. Burton
* Ted J. Kent
}}
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} musical comedy film directed by Henry Koster and starring Deanna Durbin, Nan Grey, and Helen Parrish.    Written by Felix Jackson and Bruce Manning, the film is about three sisters who believe life is going to be easy now that their parents are back together, until one sister falls in love with anothers fiance, and the youngest sister plays matchmaker.  Durbin and Grey reprise their roles from Three Smart Girls, and Parrish replaces Barbara Read in the role of the middle sister. 

==Cast==
* Deanna Durbin as Penelope Penny Craig
* Nan Grey as Joan Craig
* Helen Parrish as Katherine Kay Craig
* Charles Winninger as Judson Craig
* Nella Walker as Mrs. Craig
* Robert Cummings as Harry Loren
* William Lundigan as Richard Watkins
* Ernest Cossart as Binns, the butler
* Felix Bressart as a music teacher Charles Coleman as Henry 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 