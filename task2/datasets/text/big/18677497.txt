Totò cerca pace
 
{{Infobox film
| name           = Totò cerca pace
| image          = Totò cerca pace.jpg
| caption        = Film poster
| director       = Mario Mattoli
| producer       = Alfredo De Laurentiis
| writer         = Emilio Caglieri Ruggero Maccari Mario Mattoli Vincenzo Talarico
| starring       = Totò
| music          = Carlo Savina
| cinematography = Riccardo Pallottini Leo Cattozzo
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Totò cerca pace is a 1954 Italian comedy film directed by Mario Mattoli and starring Totò.   

==Plot==
Two widowers decide to get married but their decision is continually hampered by their grandchildren, who are just interested interested in their inheritance.

==Cast==
* Totò as Gennaro Piselli
* Ave Ninchi as Gemma Torresi Piselli 
* Enzo Turco as Pasquale Paolo Ferrari as Cousin Celestino
* Isa Barzizza as Cousin Nella Caporali
*Nino Vingelli as the waiter
*Vincenzo Talarico as the  lawyer
*Gina Amendola: Adele
*Ughetto Bertucci as a witness
*Mario Castellani as a witness
*Renzo Biagiotti as Oscar 
*Salvo Libassi as the anonymous phone caller

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 