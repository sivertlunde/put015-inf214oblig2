School Dance (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name       = School Dance
| image       = 
| caption      = Theatrical Film Poster
| producer    = Nick Cannon Michael Goldman
| director     = Nick Cannon
| writer   = Nick Cannon Nile Evans
| starring      = Bobbe J. Thompson The Ranger$ Luenell Mike Epps Katt Williams Kristina DeBarge George Lopez Wilmer Valderrama
| music         =  Geoff Zanelli
| cinematography     =  
| editing        = Erik C. Anderson
| studio      = NCredible Entertainment
| distributor  = Lionsgate
| runtime      = 
| released  =  
| country = United States
| language     = English
| budget        = 
}}
School Dance is a 2014 American comedy-drama musical film directed, written, and produced by Nick Cannon. The film stars  Bobbe J. Thompson, The Ranger$, Mike Epps, Luenell, Kristina DeBarge, Katt Williams, George Lopez, and Wilmer Valderrama The film was released on July 2, 2014 in select theaters, Video on demand|VOD, and Digital HD. This is also Cannons directoral feature film debut.

==Plot==
The film opens with a news broadcast about a teenager named Jason Jackson (Bobbe J. Thompson) being shot outside of the Monte Vista High School dance lock-in. Then Jason tells the story from the beginning, in which it starts with him trying to get in the most popular dance clique in school, The Ranger$ (Langston Higgins, Julian Goins, Dashawn Blanks) but they say he has to pass the initiation of getting a pair of panties from one of the Sweet Girls by midnight. Jason is also trying to get his crush, Anastacia (Kristinia DeBarge) to notice him, but all she remembers him is when he peed on her in elementary school.

Meanwhile, Day Day, one of The Ranger$ and Jasons older cousin owes Anastacias eldest brother, Junior, $2,000 by midnight due to Day Days father, Darren (Katt Williams) telling Junior that he would pay him after losing in dominoes. Jason gives Anastacia a poem he wrote about her as her homework in which the teacher makes her read it in front of the class. Anastacia then gives Jason her number so they can write a song together sometime.

Another subplot of the film is two police officers, Officer Peniss and Lagney are chasing down the New Boyz, who are on their way to the lock-in but get caught with weed brownies and grape juice which Officer Lagney consumes.

At the school dance lock-in, Jason, Anastacia, The Ranger$, and Sweet Girls play "7 Minutes in Heaven" but when it comes to Jason and Anastacia she tells him that she already knew his plan. Then they compete in a talent show to win $2,000 but when it comes to The Ranger$ performance, Jason finally breaks his fear and then raps to help them win the money, but the Sweet Girls win. At midnight, Jason and The Ranger$ meet the Eses in the parking lot and are about to be killed until the wannabe gangsters (Kevin Hart, Lil Duval) start a drive-by shooting and Jason then saves Anastacia but is shot. Jason is sent to the hospital but Anastacia goes with him, becomes Jasons girlfriend, and gives Flaco the $2,000 prize money so they wont kill Day Day.

The film ends with Mamma Tawanna (Luenell) coming into the parking lot with a gun, yelling who shot Jason and breaks the fourth wall showing Nick, the producers and the crew then shouting to the audience.

==Cast==
*Bobbe J. Thompson as Jason Jackson
*Julian Goins as Julian
*Langston Higgins as Langston
*Dashawn Blanks as Day Day 
*Luenell as Mamma Tawanna
*Mike Epps as Principal Rogers
*George Lopez as Oscar
*Katt Williams as Darren
*Wilmer Valderrama as Flaco
*Kristinia DeBarge as Anastacia
*Lil Duval as Bam-Bam
*Affion Crockett as Coach Fontaine
*Amber Rose as MaryWanna
*Efren Ramirez as El Matador
*Patrick Warburton as Prairie Puff Man
*Kevin Hart as OG Lil Pretty Thug (uncredited)
*New Boyz
*Melissa Molinaro as Tequila
*Kayla Collins as Big Booty Becky
*Jim Breuer as Officer Lagney
*Vivian Kindle as Mrs. Johnson
*Jessica Kirson as Officer Peniss
*Nick Cannon as Super Sizer/Juicy

==Home Media==
School Dance was released on DVD and Blu-ray on October 7, 2014.

==External links==
*  

 
 