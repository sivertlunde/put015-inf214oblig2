Predictions of Fire
{{Infobox Film
| name = Predictions of Fire
| image = Predictions_of_Fire.gif Michael Benson
| writer   = Michael Benson
| starring = Laibach (band) Peter Mlakar Slavoj Žižek
| released = 2 October 1996
| language = English
}} American filmmaker Michael Benson about the Neue Slowenische Kunst.

==Synopsis== SR Slovenias SFR Yugoslavia marked the first spark in the Yugoslav Wars that defined the first chapter of the post-cold war era. Using an inventive combination of reportage, dramatization, archival footage, animation and miniatures, Predictions of Fire is a revealing study of the controversial and internationally acclaimed Slovenian arts collective Neue Slowenische Kunst|NSK, as seen through the lens of 20th century Central European history. Shot in Ljubljana, Moscow, New York, Belgrade, and Athens, this visually arresting film offers a portrait of a culture suspended between East and West. By documenting Neue Slowenische Kunst|NSK, Predictions of Fire holds a mirror up to Europe and the world, analyzing the way nations are brought into conformity with ideology.

The film won the National Film Board of Canadas Best Documentary Award at the 1996 Vancouver International Film Festival. The jury issued a statement: "Predictions of Fire is intellectual dynamite. It explodes the icons and myths of communism and capitalism. Out of the shattered history of Slovenia, this film constructs a new way of looking at art, politics, and religion."
 Laibach emerged SR Slovenia, Yugoslav constituent Mute Records label, went on to shock the rest of the world as well. Laibach was soon joined by a painting group, IRWIN, and theater group, Scipion Nasice Sisters Theatre, at the helm of one of the most ambitious and cutting-edge arts collectives in the world. Modeled after a socialist state bureaucracy, and calling themselves Neue Slowenische Kunst (New Slovenian Arts, or NSK), these three groups became the titular heads of a micro-state within the independent republic of Slovenia. NSK recently began issuing its own passports and opened embassies and consulates in Moscow, Berlin, Ghent, Florence, and in the US.
 Godardian sensibility to show how politics invades every facet of artistic creation and how integral ideology is to the understanding of the structure and signification of images... An extremely rich tapestry of historical events and their mythic implications in both art and politics unfolds onscreen." 

==See also== NSK
*Laibach Laibach
*Slavoj Žižek

==References==
 

==External links==
* 

 
 
 
 
 