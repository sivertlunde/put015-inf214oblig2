Bye, See You Monday
 
{{Infobox film
| name           = Bye, See You Monday
| image          = 
| caption        = 
| director       = Maurice Dugowson
| producer       = 
| writer         = Jacques Dugowson Maurice Dugowson Roger Fournier
| starring       = Miou-Miou
| music          = 
| cinematography = François Protat
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Canada France
| language       = French
| budget         = 
}}

Bye, See You Monday ( ) is a 1979 French-Canadian drama film directed by Maurice Dugowson. It was entered into the 12th Moscow International Film Festival.   

==Cast==
* Miou-Miou as Nicole
* Carole Laure as Lucie Leblanc
* Claude Brasseur as Arnold Samson
* David Birney as Frank
* Gabriel Arcand as Georges, le barman
* Alain Montpetit as Jack Davis Frank Moore as Robert Lanctôt
* Denise Filiatrault as Dame de compagnie aérienne
* Renée Girard as Mere de Lucie
* Andrée Pelletier as Soeur de Lucie
* Murray Westgate as Pere de Lucie
* Raymond Cloutier as Hector, le beau-frère

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 