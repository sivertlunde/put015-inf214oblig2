Great Mazinger vs. Getter Robo
 
{{Infobox film
| name           = Great Mazinger vs. Getter Robo
| image          = Great Mazinger tai Getter Robot (1975).jpg
| image size     = 
| alt            = 
| caption        = Screenshot of the opening title.
| director       = Masayuki Akehi
| producer       = Chiaki Imada
| writer         = Keisuke Fujikawa
| screenplay     = 
| story          =
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Toei Doga, Dynamic Production
| distributor    = 
| released       = 
| runtime        = 30 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Toei based crossover between canon to either of the two series.
 Arabic countries, Grande Mazinga contro Getta Robot G in Italy and Gran Mazinger contra Getter Robo in Spain.

==Story== Boss to lure the monster there where it eats Boss Borot in the process. The combined heroes almost defeat Gilgilgan until the UFO returns and allows itself to be eaten to cause its own creation to grow to its final form. During the intense battle the heroes manage to find its weak points and destroy Gilgilgan. Afterward they promise to be friends from then on.

===Gilgilgan===
The primary antagonist of the movie, Gilgilgan is a tortoise-like bioweapon from outer space used to conquer Japan primarily by living on a diet of nothing but metal and slowly grows larger into two other forms. It possesses an extremely thick hide that prevents most attacks from harming it and for attacks it can fire yellow eye beams and green poison from its mouth that acts like a very strong acid. In his second form Gilgilan adds the ability to move underwater with the powers of his first form still intact. Once it eats its creator Gilgilgan will change into its third form which appears more like a demon where it can fly and its powers include finger lasers, bladed wings that can also fire energy beams, and has three whip-like tails.

Gilgilgan appears in various Super Robot Wars as well as a Banpresto original fourth form called Mecha Gilgilgan based on its third form that serves as the final boss in the first Super Robot Wars game and appears in other titles with the other forms. Mecha Gilgilgans powers primarily consist of its ultra sharp claws, its wing beams, and very powerful waves of gravity from its body.

==Staff==
*Production: Toei Doga, Dynamic Production
*Original work: Go Nagai, Ken Ishikawa, Dynamic Production
*Director: Masayuki Akehi
*Scenario: Keisuke Fujikawa
*Planning: Ken Ariga, Kenji Yokoyama
*Producer: Chiaki Imada
*Animation director: Kazuo Komatsubara
*Assistant director: Yuji Endo
*Music: Michiaki Watanabe, Shunsuke Kikuchi
*Art director: Tomo Fukumoto
*Cast: Akira Kamiya (Ryo Nagare), Junji Yamada (Hayato Jin), Keiichi Noda (Tetsuya Tsurugi), Toku Nishio (Musashi Tomoe), Hiroshi Otake (Boss), Kazuko Sawada (Shiro Kabuto), Kosei Tomita (Dr. Saotome), Rihoko Yoshida (Michiru Saotome), Yumi Nakatani (Jun Hono), Hidekatsu Shibata (Kenzo Kabuto)

==See also==
*Great Mazinger
*Getter Robo
* 

==External links==
* 
*    at allcinema
*  at Animemorial
*  at Toeis corporate website
*    at the EncicloRobopedia website

 

 
 

 
 
 
 
 
 