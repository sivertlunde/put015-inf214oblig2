Asu Mare
{{Infobox film
| name           = Asu Mare
| image          = File:Asu-Mare-officialPoster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Ricardo Maldonado Carlos Alcántara
| writer         = Carlos Alcántara
| starring       = Carlos Alcántara Ana Cecilia Natteri Gisela Ponce de León Emilia Drago
| music          = José San Miguel Carlos San Miguel
| cinematography = 
| editing        = 
| studio         = Tondero Films
| distributor    = 
| released       =   
| runtime        = 
| country        = Peru
| language       = Spanish
| budget         = 700 000 USD 
| gross          = 10.700.000 USD 
}}
 Carlos Alcántara, starring himself.     Directed by Ricardo Maldonado and produced by Tondero Films, the film premiered on April 11, 2013 nationwide. 

This film brings together most of the actors from the Peruvian sitcom Pataclaun (1997—99).   

== Plot ==
The story follows the adventures of Carlos Alcántara on his way to fame from his childhood in the "Unidad Vecinal Mirones". It is a recreation of his youth and experiences with his mother.
In The beginning he explains how he started to admire music but later realized that he couldnt sing. So he later tried being an actor but got framed in the process. Later on he finds himself down on his luck but later a young boy who was a street performer gives him his clown nose and its from there that plans to get some acting lessons. Later he and a couple of close friends make a team known as Pataclaun that becomes a great hit in Peru. From there he gets to be well known around the country and gets fame. In the end he thanks his mom for all the support she gave him in the good and bad times.

== Cast == Carlos Alcántara as himself
*Ana Cecilia Natteri as Isabel "Chabela" Vilar
*Gisela Ponce de León as Isabel "Chabela" Vilar (young)
*Emilia Drago as Emilia
*Dayiro Castañeda as Carlos Alcántara (child)
*Santiago Suárez as Carlos Alcántara (young)
*Andrés Salas as Culicich
*Anahí de Cárdenas as Emilias friend
*Franco Cabrera as Carloss friend

=== Guest appearances ===
*Gisela Valcárcel as herself
*Tatiana Astengo as Marujita
*Carlos Carlín as janitor
*Wendy Ramos as gypsy
*Johanna San Miguel as Socorro
*Gonzalo Torres as professor
*Katia Condos as lady at the party

=== Other appearances ===
*Juan Manuel Ochoa as Monfu
*Mario Velásquez as swindler
*Carlos Cano as Carloss uncle
*Daniel Marquina as soldier
*Jossie Lindley as Emilias mom
*Carolina Cano as girl at the beach
*Ana Rosa Liendo as teacher
*Carlos Cabrera as Ferrando
*Katia Palma as Carloss aunt

== References ==
 

== External links ==
*  

 
 
 