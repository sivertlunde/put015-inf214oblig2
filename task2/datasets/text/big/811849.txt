Once a Thief (1991 film)
 
 
 
{{Infobox film name           = Once a Thief image          = Onceathiefwooposter.jpg caption        = Theatrical release poster director       = John Woo producer       = Terence Chang Linda Kuk writer         = John Woo Janet Chun Clifton Ko starring       = Leslie Cheung Chow Yun-fat Cherie Chung music          = Lam Manyee cinematography = Poon Hang-sang William Yim editing  David Wu studio         = Golden Princess Film Production Milestone Films distributor    = Golden Princess Amusement released       =   runtime        = 103 minutes country        = Hong Kong language       = Cantonese English French
|}} 
Once a Thief ( ) is a 1991 Hong Kong crime film written and directed by John Woo. The film was released in the Hong Kong on 2 February 1991.

==Plot==
The story is of three orphans and their two father figures. They are taken in by both a wealthy crime boss which leads to their close friendship, and a kind police officer. Nevertheless, the trio grows up learning high-tech methods of theft and specialize in stealing treasured paintings. After a heist in France goes awry, Red Bean Pudding is thought to be dead and James takes his place as Red Beans lover. However, Pudding returns in a wheelchair, and the group begins planning their next heist for themselves, fall out of favor with Chow, and various complications and gun battles ensue.

== Cast ==
* Leslie Cheung as James
* Chow Yun-fat as Red Bean Pudding
* Cherie Chung as Red Bean
* Kenneth Tsang as Chow
* Chu Kong as Godfather
* Bowie Wu as Stanley Wu
* John Dang Yat-gwan as Young Red Bean Pudding
* Tong Ka-fai as Young James
* Leila Tong as Young Red Bean
* Declan Wong as Magician Henchman David Wu as Auctioneer

== Release == HKD in Hong Kong. 
 The Killer.

== Reception ==
Scott Tobias of The A.V. Club wrote that Woos style makes up for the films implausibility and lack of logic.   Kevin Lee DVD Verdict wrote that the film has "a lack of focus and an uneven tone".   Chris Gould of DVDactive.com rated the film 5/10 and wrote that that there was too much slapstick.   J. Doyle Wallis of DVD Talk rated it 2.5/5 stars and called it "an odd and annoyingly silly entry during   most creative period." 

== See also == Once a of the same name which ran from 1997 to 1998. Both films were directed by John Woo.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 