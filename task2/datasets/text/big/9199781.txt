This Is England
 
 
 
{{Infobox film
| name           = This Is England
| image          = This is england film poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Shane Meadows
| producer       = Mark Herbert
| writer         = Shane Meadows Paddy Considine (uncredited)  Stephen Graham Jo Hartley Andrew Shim Vicky McClure Joseph Gilgun Rosamund Hanson
| music          = Ludovico Einaudi Danny Cohen
| editing        = Chris Wyatt
| studio         = Warp Films FilmFour Productions
| distributor    = Optimum Releasing
| released       =  
| runtime        = 102 minutes  
| country        = United Kingdom
| language       = English
| budget         = £1.5 million 
| gross          = £5 million  
}} West Indian white nationalists, which led to divisions within the skinhead scene. The films title is a direct reference to a scene where the character Combo explains his nationalist views using the phrase "this is England" during his speech.

==Plot== new wave style.
 sociopathic tendencies, English nationalist racist views, and attempts to enforce his leadership over the other skinheads. This leads the group to split, with young Shaun, the belligerent Pukey, and Gadget, who feels bullied by Woody for his weight, choosing Combo over Woodys apolitical gang. 
 white nationalist meeting. After Pukey expresses doubt over their racist and nationalistic politics, Combo throws him out of the group and sends him back to Woody. The gang then engages in racist antagonism of, among others, shopkeeper Mr. Sandhu, an Indian man who had previously banned Shaun from his shop.
 cannabis from Milky, the only black skinhead in Woodys gang. During a party, Combo and Milky bond while intoxicated, but Combo becomes increasingly jealous when Milky shares details of his many relatives and comfortable family life. Enraged, Combo beats Milky into a coma while Banjo holds down Shaun, who watches in horror. Shaun and Combo later take Milky to a nearby hospital.
 Saint George Flag into the sea.

==Cast==
* Thomas Turgoose as Shaun Field   Stephen Graham as Andrew "Combo" Gascoigne
* Jo Hartley as Cynthia Field
* Andrew Shim as Michael "Milky"
* Vicky McClure as Lorraine "Lol" Jenkins
* Joseph Gilgun as Richard "Woody" Woodford
* Rosamund Hanson as Michelle "Smell" Andrew Ellis as Gary "Gadget"
* Perry Benson as Ronald "Meggy" Megford
* George Newton as Banjo
* Frank Harper as Lenny Jack OConnell as "Pukey" Nicholls
* Kriss Dosanjh as Mr. Sandhu
* Kieran Hardcastle as Kes
* Chanel Cresswell as Kelly Jenkins
* Danielle Watson as Trev
* Sophie Ellerby as Pob
* Michael Socha as Harvey

==Soundtrack==
{{Infobox album  
| Name        = This Is England Soundtrack
| Type        = soundtrack
| Artist      = various artists
| Cover       = 
| Cover size  = 
| Released    = 23 April 2007
| Recorded    =
| Genre       = Rock music|Rock, ska, Britpop, reggae, jazz rock
| Length      =
| Label       = Commercial Marketing
| Producer    =
| Reviews     = 
| Chronology  = Shane Meadows film soundtracks Dead Mans Shoes (2004)
| This album  = This Is England (2006) Somers Town (2008)
}}

# "54–46 Was My Number" – Toots & The Maytals
# "Come On Eileen" – Dexys Midnight Runners
# "Tainted Love" – Soft Cell
# "Underpass/Flares" (Film dialogue) Gravenhurst
# "Cynth / Dad" (Film dialogue)
# "Morning Sun" – Al Barry & The Cimarons
# "Shoe Shop" (Film dialogue)
# "Louie Louie" – Toots & The Maytals
# "Pressure Drop" – Toots & The Maytals
# "Hair in Cafe" (Film dialogue)
# "Do the Dog" – The Specials
# "Ritornare" – Ludovico Einaudi
# "This Is England" (Film dialogue)
# "Return of DJango" – Lee "Scratch" Perry & The Upsetters
# "Warhead" – UK Subs
# "Fuori Dal Mondo" – Ludovico Einaudi
# "Since Yesterday" – Strawberry Switchblade
# "Tits" (Film dialogue)
# "The Dark End of the Street" – Percy Sledge
# "Oltremare" – Ludovico Einaudi
# "Please Please Please Let Me Get What I Want" (The Smiths cover) – Clayhill
# "Dietro Casa" – Ludovico Einaudi
# "Never Seen the Sea" – Gavin Clark (of Clayhill)

;Additional music from the film includes Pomp and Circumstance March No 1 in D. OP 39/1" (Edward Elgar) – performed by Royal Philharmonic Orchestra
# "Maggie Gave a Thistle" – Wayne Shrapnel and The Oi Stars
# "Lets Dance" – Jimmy Cliff

==Production== St Anns, Lenton, Nottingham|Lenton, and The Meadows, with one section featuring abandoned houses at the former airbase RAF Newton, outside of Bingham, Nottinghamshire.  The opening fight was filmed at Wilsthorpe Business and Enterprise College, a secondary school in Derbyshire.  Additional scenes such as "the docks" were filmed in Turgooses home town of Grimsby, which is also the opening scene for This is England 86, episode one.

Turgoose was 13 at the time of filming.  Turgoose had never acted before, had been banned from his school play for bad behaviour, and demanded £5 to turn up for the films auditions.  The film was dedicated to Turgooses mother, Sharon, who died of cancer on 29 December 2005; while she never saw the film, she saw a short preview. The cast attended her funeral.

===Setting=== the Midlands. Although much of the film was shot on location in Nottingham, a number of scenes feature the towns docks, which precludes this inland city being the setting for the action. Similarly, the accents of the main characters are drawn from a wide geographical area.

==Reception==
On 5 January 2008, the review aggregator Rotten Tomatoes reported that 93% of critics gave the film positive reviews, based on 82 reviews.  Metacritic reported the film had an average score of 86/100, based on 23 reviews — indicating "universal acclaim".  This made it the tenth best reviewed film of the year. 

The film appeared on several US critics top ten lists of 2007; it was third on the list by Newsweek  s David Ansen, seventh on the list by The Oregonian  s Marc Mohan, and ninth on the list by  Los Angeles Times  Kevin Crust. 

In Britain, director Gillies Mackinnon rated the film the best of the year  and David M. Thompson, critic and film-maker, rated it third.  The film was ranked fourteenth in The Guardian s list of 2007s Best Films  and fifteenth in Empire (magazine)|Empires Movies of the Year.

===Accolades=== 2007 British Academy Film Awards. It also won the Best Film category at the 2006 British Independent Film Awards, Turgoose winning the Most Promising Newcomer award.

==TV miniseries==
In 2010, a spin-off series set three years after the film, This Is England 86, was shown on Channel 4. A sequel to the series set two and a half years later, This Is England 88, was broadcast in December 2011. A third installment, This Is England 90, was originally due in late 2012, but in July 2012, Meadows announced that the production had been put on hold in order for him to complete his documentary about The Stone Roses,  and the actors were still waiting for confirmation as to when filming would start. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   – Interview with Stephen Graham about This Is England

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 