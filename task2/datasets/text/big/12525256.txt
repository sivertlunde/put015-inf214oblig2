Dyesebel
 
{{Infobox comics character
 
| character_name = Dyesebel 
| image          =  
| imagesize      = 
| caption        =  The original depiction of Dyesebel created by Mars Ravelo.
| publisher      = 
| debut          = 
| creators       = Mars Ravelo
| alter_ego      = 
| full_name      =
| species        = Half-human/Half-mermaid
| homeworld      =  
| alliances      = Darna
| partners       = Fredo 
| supports       = 
| aliases        =  
| powers         = Able to live on or under the sea.
| cat            = 
| subcat         = 
| hero           = Super
| villain        = Dyangga 
| sortkey        = Dyesebel
| addcharcat#    = 
}}
 mermaid character comic book cinema and List of Philippine television shows|television.  It was first serialized in Pilipino Komiks in 1952-53, and was later adapted into the big screen that same year. The film was produced under Manuel Vistan Productions, Inc. and released through Premiere Productions. It was directed by  Gerardo Gerry De Leon.  

Dyesebel first appeared in the pages of Pilipino Komiks during the 1950s, then in Kampeon Komiks (Champion Comics) in 1975. The story has been adapted into five films and a spin-off, throughout the timeline from 1953 to 1996. Among those who took on the role in the big screen are Edna Luna (1953), Vilma Santos (1973), Alma Moreno (1978), Alice Dixson (1990), and Charlene Gonzales (1996). On the television, the iconic mermaid is portrayed by Marian Rivera (2008) and Anne Curtis (2014).

==Fictional character history==

Dyesebel is unique among the merfolk for she is the only mermaid born of human parents from the surface world. A mermaid from birth, she is shunned by the superstitious due to her physical form.

The story of Dyesebel begins when an amnesiac mermaid-turned-human named Lucia fell in love with a human. They got married and had a beautiful baby, but instead of a human baby she delivered a mermaid. The child is called "Dyesebel" . Although surprised by their daughters strange form, Lucia and her husband Tino promise to love their child no matter what. But in no time at all, their neighbors discovered the couples secret. When the community is hit with a series of catastrophes, the town believes that the anomaly is caused by Dyesebel; an accusation that leads to Tinos death. After her father is killed by an angry, superstitious mob, Dyesebel is taken to the ocean by her mother where she is sent into exile since her chances for survival are greater there than on land. Unknown to Lucia is that in the heart of the sea, Dyesebels adventures and misfortunes will begin. It is within the confines of the waters that Dyesebel will lead a new life-a journey that would take her to the depths of the sea, as well as to chaos of the city. 

One day the young girl-mermaid got lost in the ocean and found by Banak who adopts her and takes her to their kingdom where she is welcomed into their community. Years later, a jealous mermaid named Dyangga makes Dyesebels life miserable among the denizens of the sea which leads Dyesebel to try returning to the surface world. She meets a kind, handsome young man named Fredo who falls in love with her and protects her, but then she is kidnapped by Fredos jealous former girlfriends (Betty) new boyfriend and displayed at the circus. She is subsequently rescued by Fredo and is brought back to the ocean where she is again told that although Fredo doesnt want to see Dyesebel go, hed rather let her go than see her hurt by evil men on the surface world. Dyesebel discovers that she can also be a human being by visiting Amafura to get a secret necklace which makes her human; but a price must be made, for her to sacrifice her true possession, in order to gain something special. This time around, Dyesebel returns with a magic conchshell that transforms her into a full human form and returns to Fredo where they marry. 

==Adaptations==

===Films===

====Dyesebel (1953)====

The first actress to play Dyesebel on the big screen was Edna Luna in a 1953 film made by Gerry de Leon for Manuel Vistan Production/Premiere Production.

In the Philippines, the lore of Dyesebel began with the story of a girl born to a mermaid-obsessed Filipino mother. Whilst  pregnant, Lucia (Dyesebels mother) obsessively looked at a collection of mermaid pictures from a calendar which eventually affected the infants form when it was born with a fishtail instead of normal human lower extremities. Tino (Dyesebels father), angered by his childs deformity, wanted to abandon the child, only to be hindered by a bolt of lightning during a typhoon. To avoid the stigma of having a mermaid child, the couple decided to leave their village and relocate where they can raise Dyesebel in secret, hidden from prying eyes. The mermaid, Dyesebel, eventually met other mermaids while spending time by the seashore, and through her sea adventures, she comes into contact with Dyangga, a sea-witch who had the power to transform sea-creatures into humans. Dyesebel fell in love with Fredo (Jaime de la Rosa), a human. But after her secret of being a mermaid was exposed, it prompted Betty (Carol Varga), the jealous former girlfriend of Fredo, to kidnap Dyesebel and place her  in a carnival. Dyesebel was later saved by Fredo. Dyangga eventually transformed Dyesebel into a permanent female human with normal lower extremities.    , retrieved on: August 8, 2007 

Anak ni Dyesebel (1964)

In 1964, eleven years after, Director Gerardo de Leon directed a sequel to his first Dyesebel. He reunited the cast of the blockbuster 1953 movie Dyesebel in a sequel based on Mars Ravelos Anak ni Dyesebel (Liwayway #4, 1963-1964). The movie introduced Alona (played by Eva Montes who also played Darna in the 1965 movie Darna at ang Babaing Tuod), the daughter of Dyesebel (Edna Luna) and Fredo (Jaime dela Rosa).

====Dyesebel at Ang Mahiwagang Kabibe (1973)====

The 1973 Dyesebel film ("Dyesebel at Ang Mahiwagang Kabibe") was directed by Emmanuel H. Borlaza, and was closer to Mars Ravelos heroine mermaid. The character inhabits an undersea kingdom of mermaids, cast out from the land of humans due to the belief that mermaids are the cause of misfortune. Dyesebel became attracted to a male human being and swore to find any means to be transformed into normal human, in order to be with the man that her heart desired.   , retrieved on: August 8, 2007 

In this film, Vilma Santos played the role of Dyesebel making her the first actress to play both Darna (four times) and Dyesebel, both classic Mars Ravelo creations. Romeo Miranda played the role Fredo, her human lover. Perhaps this is the best one that depicts the mermaid riding on a giant seahorse, fighting against a giant Octopus, being helped by electric eels to fight the behemoth, mermaids galore, and Dyesebels "Magic Conch-shell" that turns her into a human.

====Sisid, Dyesebel, Sisid (1978)====

In 1978s "Sisid, Dyesebel, Sisid" made by Sampaguita Pictures, Dyesebel (Alma Moreno) was a mermaid born to a rich couple. The husband claimed his wife had an affair with a merman. Still loving and accepting, they moved to their beach house where Dyesebel was kept in a wheelchair covered with a blanket to hide her tail. It was only her parents and her nanny who knew her identity as a mermaid. As she grew up into a lady mermaid (still on shore) the family hires Fredo (renamed David, played by Matt Ranillo III) and his dad as their gardener. Fredo and Dyesebel fell in love with each other.

After Dyesebel revealed herself to Fredo, they decided to get married (with Dyesebel wearing a wedding gown in a fish tail). Shortly after they got married, Dyesebels parents died in a plane crash. Then, Dyesebel discovered that Fredo is a womanizer. In full despair, she heard  the ocean calling her after a very long time, and she joined the mermaid kingdom. Trying to be happy, she admitted that she still missed Fredo, and asked Banak (Nova Villa) how to be become human. She led her to Dyangga (Bella Flores), a human octopus. With the help of Maro (Matt Ranillo III in a dual role) — a merman friend who killed Dyangga, she got the magical pearl on her head that gave her two legs. She became a human for an hour or so, only to break up with Fredo and decided to be a mermaid forever, and lived in the ocean where she was deprived from since birth.

====Dyesebel (1990)====

Perhaps the most memorable Dyesebel film to date, this version is common that makes Dyesebel of what it is now. It was the first Dyesebel to swim underwater, to shot real underwater scenes, to use the "orange-colored" fish tail (which is commonly used now), to show the combination of Dyesebels life underwater water and on land, and to tell a realistic story line on love and relationships.

A couple found a wounded and stranded pregnant mermaid on shore, who died after giving birth. Being childless, the couple adopted the mermaids daughter and named her Dyesebel, From Dyesebels childhood through her teenage years, she and her family moved from place to place to get away from angry mob. The teen Dyesebel (Carmina Villaroel) starts to ask questions why shes a mermaid and shows signs of wanting to become a human. It was at this time that they decide to let Dyesebel go into the sea with the other mermaids (not shown on screen) where she has the chance to encounter with Edward (Robert Ortega) as a teen. Fast forwarding to the present day where Dyesebel is now a lady (Alice Dixson). She then sees Edward (Richard Gomez) again as an adult, then searches Banak, a sea witch this time, to help her acquire human legs through a magic shell. However, it was only temporary. The shell has to be within her reach for a day or else shell have her tail back.

She finds shelter by Marina (Malou de Guzman) and Iday (Judy Ann Santos) who later on discovers her identity but still remain true to her. She also found Edwards wallet washed from the shore. Still having Edwards wallet that was misplaced on the water Dyesebel and her friends look for Edward and becomes a famous model by a friend of Edwards. Dyesebel and Edward finally meet and become lovers. However, Malou (Edwards girlfriend, played by Nadia Montenegro) tries to break their relationship. At the same time, Dyesebel has a hard time keeping her identity a secret to Edward. Until she finally admits it to him, and shows him her true self, where they broke up for sometime. Edward realizes he cannot live without her, they are about to become closer and Malou accidentally finds out, and exposed Dyesebels mermaid identity in Edwards birthday celebration. The media and the government took interest on her, as a display on the countrys underwater museum. Edward rescued her, and Malou discovered Dyesebels magic shell, steals it and breaks it. However, it only turned Dyesebel into a human permanently, and Malou into a mermaid permanently.

====Dyesebel (1996)====

In the Dyesebel 1996 film version, Charlene Gonzales played Dyesebel with Matthew Mendoza as Fredo. It was directed by Emmanuel H. Borlaza who also directed the 1973 Dyesebel film starring Vilma Santos.

Dyesebel (Charlene Gonzales) is unique among the merfolk for she is the only mermaid born of human parents from the surface world. A mermaid from birth, she is shunned by the superstitious due to her physical form. After her father is killed by an angry, superstitious mob, Dyesebel is taken to the ocean by her mother where she is sent into exile since her chances for survival are greater there than on land. The young girl-mermaid is found by Banak (Gloria Diaz) who adopts her and takes her to their kingdom where she is welcomed into their community. Years later, a jealous mermaid named Dyangga (Maritoni Fernandez) makes Dyesebels life miserable among the denizens of the sea which leads Dyesebel to try returning to the surface world. She meets a kind, handsome young man named Fredo (Matthew Mendoza) who falls in love with her and protects her, but she is kidnapped by Fredos jealous former girlfriends new boyfriend and displayed at the circus. She is subsequently rescued by Fredo and is brought back to the ocean.

====Dyesebel: The Movie (2015)====

An upcoming film version will be produced by Star Cinema, and will be distributed by Mars Ravelo Komiks Characters, Inc. It will be released on the fourth quarter of 2015 in theatres nationwide, and a possible entry to the 2015 Metro Manila Film Festival. The film will feature Anne Curtis reprising her role as Dyesebel, and the whole cast will also reprise their roles. As opposed to a remake of Dyesebel (2014 TV series) preceding it, this film version serves as a reboot of Dyesebel and intends to be a more faithful adaptation of the original Komiks written by Mars Ravelo. It will be the 6th Dyesebel film, after Charlene Gonzales Dyesebel in 1996, and the 2nd Mars Ravelo Komiks film produced by Star Cinema, after Angel Locsins upcoming 2015 Darna: The Movie.

===Television series===

====Mars Ravelos Dyesebel (2008)====
 

The first TV series adaptation of Dyesebel was broadcast on GMA Network in 2008. It originally aired on April 28, 2008 and ended on October 17, 2008, completing 125 episodes.   It is top billed by Marian Rivera and Dingdong Dantes in the lead roles.  It was internationally aired on GMA Pinoy TV, which became a huge success to viewers abroad.

The story of Dyesebel begins when an amnesiac mermaid-turned-human named Lucia (Jean Garcia) fell in love with a human. They got married and had a beautiful baby, but instead of human baby she delivered a mermaid. The child is called Dyesebel (Marian Rivera). Although surprised by their daughters strange form, Lucia and her husband Tino (Wendell Ramos) promise to love their child no matter what. In no time at all, their neighbors discovered the couples secret. When the community is hit with a series of catastrophes, the town believes that the anomaly is caused by Dyesebel; an accusation that leads to Tinos death and Lucia to offer her only daughter to the sea, where she would belong. Unknown to Lucia is that in the heart of the sea, Dyesebels adventures and misfortunes will begin. It is within the confines of the waters that Dyesebel will lead a new life &mdash; a journey that will take her to the depths of the sea, as well as to chaos of the city. Dyesebel discovers that she can also be a human being by visiting Amafura (Rufa Mae Quinto) to get a secret necklace which makes her a human; but a price must be made, for her to sacrifice her true possession, to gain something special.

====Mars Ravelos Dyesebel (2014)====
 

In 2014, the second TV series adaptation was broadcast on   as Fredo, Sam Milby as Liro, and Andi Eigenmann as Betty.  It is the second time where Anne Curtis takes the role of a mermaid, after her lead role as a goddess (who can transform into a mermaid, a harpy, or a centaur) in the 2008 TV series, Dyosa. The story follows the journey of a young mermaid named Dyesebel, as she discovers the world above the ocean and locks in a fierce love triangle.

"A young mermaid, Dyesebel, takes on a journey to trace her origins until she finds out that she is the daughter of a merman and a human. Disowned by the sea for being the child of Tino who was the prince of the sea and the human Lucia, she starts with her adventure and explores the human world where she will meet Fredo. But this journey will be marked by a realization that she is not accepted by both worlds. And as a war between man and the underworld appears imminent, Dyesebel will choose to leave those that she loves."
— Official IWanTV! Synopsis  

==On-screen actresses and actors==
 
In the films, Dyesebel was portrayed by Filipino actresses: Edna Luna, Vilma Santos-Recto, Alma Moreno, Alice Dixon, and Charlene Gonzales-Muhlach. In television, she was portrayed by Marian Rivera on 2008 and Anne Curtis on 2014.  , (Notes: According to the Mars Ravelo official website, the first Dyesebel film was in 1953 and that the latest movie version was in 1996; not 1950 and 1990 as indicated in IMDb.com, respectively), retrieved on: August 13, 2007  She was also personified by the actress Ara Mina in a cameo appearance on Mars Ravelos Darna (2005 TV Series).

=== Official list of actresses who played Dyesebel ===
{| class="wikitable" 
|- 
! No. || Actress || Role || Title || Year
|- 
| 1||Edna Luna|| Dyesebel  || Dyesebel || 1953
|- Vilma Santos-Recto|| Dyesebel|| Dyesebel at Ang Mahiwagang Kabibe || 1973
|-
| 3||Alma Moreno|| Dyesebel ||Sisid, Dyesebel, Sisid || 1978
|- 
| 4||Alice Dixson|| Dyesebel|| Dyesebel || 1990
|- Charlene Gonzales-Muhlach|| Dyesebel || Dyesebel || 1996
|- Dyesebel || Mars Ravelos Dyesebel (2008 TV series) || 2008
|- Dyesebel || Mars Ravelos Dyesebel (2014 TV series) || 2014
|}

Young Dyesebel
*Carissa in Sisid, Dyesebel, Sisid (1978).
*Carmina Villaroel in Dyesebel (1990).
*Charina Scott in Dyesebel (1996).
*Kirsten Jane Sigrist in Dyesebel (2008 TV series).
*Ashley Sarmiento in Dyesebel (2014 TV series).

Cameo Appearance
*Ara Mina in Mars Ravelos Darna (2005 TV Series).

=== Official list of actors who played Fredo ===
{| class="wikitable" 
|- 
! No. || Actor || Role || Title || Year
|- 1953
|- 1973
|- 1978
|- 1990
|- Fredo ||Dyesebel||1996
|- 2008
|- 2014
|}

Young Fredo
*Arkin Luciano Magalona in Dyesebel (2008 TV series).
*Giacobbe Whitworth in Dyesebel (2014 TV series).

===Trivia===
 
*Dyesebel is the most popular and most loved mermaid in the Philippines.
*Edna Luna is the first Dyesebel in 1953. She also reprised her role in 1964s Anak ni Dyesebel with Jaime dela Rosa as Fredo and Eva Montes as Alona, Dyesebels daughter.
*Vilma Santos is the second Dyesebel, and the first actress who played two of Mars Ravelos komiks characters, Darna and Dyesebel respectively.
*Marian Rivera is the second actress who played both Darna and Dyesebel.
*Alice Dixson was the first Dyesebel to use the "orange-colored" fish tail, which is commonly used now. The first three Dyesebel actresses used bluish-silver fish tails much like a dolphin. In the teaser trailer of Dyesebel 2014 remake, Anne Curtis was seen wearing pink fish tail; however, it is confirmed that she also used orange fish tail. 
*Anne Curtis is the 7th official Dyesebel. She is the oldest actress to play the role at age 29. Edna Luna was 19 when she played Dyesebel; Vilma Santos was also 19, while Alma Moreno was 18. The others were a little older when assigned the mermaid role: Alice Dixson was 20, Charlene Gonzales was 22, and Marian Rivera was 23. 
*Carmina Villaroel and Ara Mina also played Dyesebel only in a teen and cameo appearances respectively, making them excluded from the official list of actresses who played the role.

==Collected editions==
{| class="wikitable"
|-
! Title !! Material collected
|-
|  
| Pilipino Komiks #1 (1952);   (October 25, 1952);   (March 14, 1953);   (June 20, 1953)
|-
| Anak ni Dyesebel
| Liwayway   (1963-1964)
|-
|}

==See also==
*Panday (comics)
*Zuma (comics)
*Philippine mythical creatures

==References==
 

==External links==
*  
*  at MarsRavelo.Tripod.com
* 
* 

*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 