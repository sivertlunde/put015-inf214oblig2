The Fifth Wheel (film)
{{Infobox film
 | name = The Fifth Wheel 
 | image = The Fifth Wheel (film).jpg
 | caption =
 | director = Giovanni Veronesi
 | writer =   
 | starring =   Elisa
 | cinematography = Fabio Cianchetti
 | editing =   
 | producer =  
 }} Italian comedy-drama film directed by Giovanni Veronesi. It was the opening film at the 2013 Rome Film Festival.   

== Cast == 

* Elio Germano: Ernesto
* Alessandra Mastronardi: Angela
* Ricky Memphis: Giacinto
* Sergio Rubini: Fabrizio Del Monte
* Virginia Raffaele: Mara
* Alessandro Haber: il Maestro
* Ubaldo Pantani: Toscano
* Francesca Antonelli: Agnese
* Maurizio Battista: zio Alberto
* Francesca DAloja: Donna Giulia
* Massimo Wertmüller: padre di Ernesto
* Elena Di Cioccio: Giuliana
* Luis Molteni: Cocco
* Dalila Di Lazzaro: signora veneta

==References==
 

==External links==
* 
  
 
 
  
 
 
 
   
  
 