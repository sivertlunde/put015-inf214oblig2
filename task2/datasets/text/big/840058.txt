42nd Street (film)
{{Infobox film
| name           = 42nd Street
| image          = Forty-second-street-1933.jpg
| image_size     =
| caption        = Theatrical Poster
| director       = Lloyd Bacon Busby Berkeley (musical numbers)
| producer       = Darryl F. Zanuck Hal B. Wallis (assoc.) (both uncredited)
| writer         = Bradford Ropes (novel) Rian James James Seymour Whitney Bolton (uncredited)
| starring       = Warner Baxter Ruby Keeler Dick Powell Ginger Rogers
| music          = Harry Warren (music) Al Dubin (lyrics)
| choreography   = Busby Berkeley
| cinematography = Sol Polito Thomas Pratt Frank Ware
| distributor    = Warner Bros.
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $439,000 (est.)
| gross          = $2,250,000   accessed 19 April 2014  
}}
 
42nd Street is a 1933 American Warner Bros. musical film directed by Lloyd Bacon with choreography by Busby Berkeley. The songs were written by Harry Warren (music) and Al Dubin (lyrics), and the script was written by Rian James and James Seymour, with Whitney Bolton (uncredited), from the novel of the same name by Bradford Ropes (who helped with the film).
 list of best musicals.

==Plot==
 
It is 1932, the depth of the Depression, and noted Broadway producers Jones (Robert McWade) and Barry (Ned Sparks) are putting on Pretty Lady, a musical starring Dorothy Brock (Bebe Daniels). She is involved with wealthy Abner Dillon (Guy Kibbee), the shows "angel" (financial backer), but while she is busy keeping him both hooked and at arms length, she is secretly seeing her old vaudeville partner, out-of-work Pat Denning (George Brent).
 Stock Market Crash. He must make his last show a hit, in order to have enough money to retire on.

Cast selection and rehearsals begin amidst fierce competition, with not a few "casting couch" innuendos flying around. Naïve newcomer Peggy Sawyer (Ruby Keeler), who arrives in New York from her home in Allentown, Pennsylvania, is duped and ignored until two experienced chorines, Lorraine Fleming (Una Merkel) and Ann "Anytime Annie" Lowell (Ginger Rogers), take her under their wing. Lorraine is assured a job because of her relationship with dance director Andy Lee (George E. Stone); she also sees to it that Ann and Peggy are chosen. The shows juvenile lead, Billy Lawler (Dick Powell), takes an immediate liking to Peggy, as does Pat.
 Tom Kennedy) to rough him up. That plus her realization that their situation is unhealthy makes Pat agree to not to see each other for a while, and he gets a stock job in Philadelphia.

Rehearsals continue for five weeks to Marshs complete dissatisfaction until the night before the shows opening in Philadelphia, when Dorothy breaks her ankle. By the next morning Abner has quarreled with her and wants Julian to replace her with his new girlfriend, Annie. She, however, tells him that she cant carry the show, but the inexperienced Peggy can. With 200 jobs and his future riding on the outcome, a desperate Julian rehearses Peggy mercilessly (vowing "Ill either have a live leading lady or a dead chorus girl") until an hour before the premiere.

Billy finally gets up the nerve to tell Peggy he loves her; she enthusiastically kisses him. Then Dorothy shows up and wishes her luck, telling her that she and Pat are getting married. The show goes on, and the last twenty minutes of the film are devoted to three Busby Berkeley production numbers: "Shuffle Off to Buffalo", "(Im) Young and Healthy", and "42nd Street".

The show is a hit. As the theater audience comes out Julian stands in the shadows, hearing the comments that Peggy is a star and he (Marsh) does not deserve the credit for it.

==Note==
In the original Bradford Ropes novel Julian and Billy are lovers. Since same-sex relationships were unacceptable by the moral standards of the era, the film substituted a romance between Billy and Peggy.

==Cast==
{{multiple image
|align=right
|direction=vertical
|width=125
|header=
|image1=DanBaxtCred42ndSt1933Trailer.jpg
|image2=KeelBrenCred42ndSt1933Trailer.jpg
|image3=PowRogCred42ndSt1933Trailer.jpg
|image4=MerkKibCred42ndSt1933Trailer.jpg
}}
* Warner Baxter as Julian Marsh
* Bebe Daniels as Dorothy Brock
* George Brent as Pat Denning
* Ruby Keeler as Peggy Sawyer
* Guy Kibbee as Abner Dillon
* Una Merkel as Lorraine Fleming
* Ginger Rogers as Ann Lowell
* Ned Sparks as Barry
* Dick Powell as Billy Lawler
* Allen Jenkins as Mac Elroy, the stage manager
* Edward J. Nugent as Terry, a chorus boy
* Robert McWade as Jones
* George E. Stone as Andy Lee
* Toby Wing as Blonde in "Young and Healthy" Number
 George Irving Charles Lane. Al Dubin and Harry Warren, who actually wrote the films songs, made cameo appearances as the songwriters.

==Production==
 
The film was Ruby Keelers first one, and the first time that choreographer Busby Berkeley and songwriters Harry Warren and Al Dubin had worked for Warner Bros.  Director Lloyd Bacon was not the first choice to direct – he replaced Mervyn LeRoy when LeRoy became ill. LeRoy was dating Ginger Rogers at the time, and had suggested to her that she take the role of "Anytime Annie".  

Actors who were considered for lead roles when the film was being cast include Warren William and Richard Barthelmess for the role of Julian Marsh, eventually played by Warner Baxter; Kay Francis and Ruth Chatterton instead of Bebe Daniels for the role of Dorothy Brock; Loretta Young as Peggy Sawyer instead of Ruby Keeler; Joan Blondell instead of Ginger Rogers for Anytime Annie; Glenda Farrell for the role of Lorraine, played by Una Merkel, and 
Frank McHugh instead of the diminutive George E. Stone as Andy, the dance director. IMDb   

The film began production on 5 October 1932 and shot for 28 days at the Warner Bros. studio in Burbank, California. The total cost of making it has been estimated to be $340,000–$439,000.  TCM   

{{multiple image
|direction=vertical
|width=220
|header=
|image1=DanceNo1in42ndSt1933Trailer.jpg
|image2=DanceNo2in42ndSt1933Trailer.jpg
}}

==Musical numbers==
All songs have music by Harry Warren and lyrics by Al Dubin. 

* "Youre Getting To Be A Habit With Me" – sung by Bebe Daniels ( )
* "It Must Be June" – sung by Bebe Daniels, Dick Powell and the chorus
* "Shuffle Off to Buffalo"   – sung and danced by Ruby Keeler and Clarence Nordstrom, with Ginger Rogers, Una Merkel and the chorus
* "Young and Healthy" – sung by Dick Powell and the chorus
* "Forty-Second Street" – sung and danced by Ruby Keeler, and sung by Dick Powell ( )

Also, a "Love Theme," written by Harry Warren, is played under scenes between Ruby Keeler and Dick Powell, and Bebe Daniels and George Brent. It has no title or lyrics, and is unpublished.

==Reception and legacy== Strand Theatre, and went into general release two days later, becoming one of the most profitable ones of the year, bringing in an estimated gross of $2,300,000. It received Academy Award nominations for Best Picture and Best Sound Recording for Nathan Levinson, and was named one of the 10 Best Films of 1933 by Film Daily. TCM     

Mordaunt Hall of The New York Times called the film "invariably entertaining" and, "The liveliest and one of the most tuneful screen musical comedies that has come out of Hollywood". 

The New York World-Telegram described it as "A sprightly entertainment, combining, as it did, a plausible enough story of back-stage life, some excellent musical numbers and dance routines and a cast of players that are considerably above the average found in screen musicals." 

"Every element is professional and convincing," wrote Variety (magazine)|Variety. "Itll socko the screen musical fans with the same degree that Metro-Goldwyn-Mayer|Metros pioneering screen musicals did." 
  John Mosher of The New Yorker called it "a bright movie" with "as pretty a little fantasy of Broadway as you may hope to see," and praised Baxters performance as "one of the best he has given us," though he described the plot as "the most conventional one to be found in such doings." 

Warner already had a follow-up of sorts, Gold Diggers of 1933, in production before the films release, and the success of both films permitted a higher budget and more elaborate production numbers in Warners next follow-up, Footlight Parade.

By the time Busby Berkeley died in 1976, the film had become revered as the archetypal backstage musical, the one that "gave life to the clichés that have kept parodists happy," as critic Pauline Kael wrote. 

==Awards and honors==
 

; Academy Award nominations   
* Best Picture
* Best Sound – Nathan Levinson

;American Film Institute recognition
* 2004 – AFIs 100 Years... 100 Songs:
** "42nd Street," # 97
* 2005 – AFIs 100 Years... 100 Movie Quotes:
** "Sawyer, youre going out a youngster, but youve got to come back a star!," # 87
* 2006 – AFIs Greatest Movie Musicals – # 13

==See also==
* 42nd Street (musical)|42nd Street, the Broadway musical 42nd Street, the actual street in New York City

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 