Oru Oodhappu Kan Simittugiradhu
{{Infobox film
| name           = Oru Oodhappu Kan Simittugiradhu
| image          = 
| image_size     =
| caption        =
| director       = S.P. Muthuraman
| producer       = S.Sankaran Ramya Cine Arts
| writer         = Pushpa Thangadurai
| narrator       =
| starring       = Kamal Haasan, Vijayakumar (actor)|Vijayakumar,  Sujatha (actress)|Sujatha,  Vijaya geetha,  T.K.veerasami,  Vijayalakshmi
| music          = V. Dakshinamoorthy
| cinematography = Babu
| editing        = R.Vittal
| distributor    =
| released       = 4 June 1976
| runtime        =
| country        = India Tamil
| budget         =
| gross          =  30 lakh
| preceded_by    =
| followed_by    =
| website        =
}}

Oru Oodhappu Kan Simittugiradhu is a Tamil language film starring Kamal Haasan in the lead role of the protagonist, Ravi. This movie is based on a novel by the same name written by Pushpa Thangadorai.

==Plot==
Ravi (Kamal) lives next door to Radha (Sujatha) and both fall in love with each other. Ravi’s friend challenges him that he can make Radha fall for him in one week. Unable to prove himself right, the friend attempts to molest Radha. Ravi arrives at the scene and a fight ensues with Ravi killing his friend unintentionally. Ravi is sentenced to life sentence and he requests Radha to forget him and carry on with her life.

Due to Gandhi’s birthday, Ravi gets a pardon after spending six years in prison. Film begins with Ravi traveling back to his hometown. Ravi happens to meet Radha who in turn spurns him and asks him not to interfere with her life!
Apparently Radha is already married to Vijayakumar!

Unable to forget her, Ravi lives a lonely and monotonous life. Radha enters hs workplace and pleads with him to forget her and start his life anew. Ravi says that it is impossible unless Radha spends one whole day with him like as if they were married! “Don’t mistake me,” he pleads, “I will not even touch you.” He explains that he had built a lot of dreams on living together and this “one day” business will satisfy his “hunger.”

Now Radha is overcome with emotions and is unable to forget Ravi!!!
She now requests that there is only one solution………!

Radha requests Ravi to come over to her house and pick her up that very night. Together they will run away and live together Ravi turns up Radha writes a long letter for her husband and then leaves the house. What happens then forms the story.

==Soundtrack==
The lyrics for the songs are written by Kannadasan, Kumaradevan, P.Palani Samy 

{|class="wikitable"
! Song !! Singer !! Duration
|---
| Aandavan Illa Ulagamithu
| Vani Jayaram
| 4:21
|---
| Nalla Manam Vaazhga
| Yesudas
| 4:03
|---
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 


 