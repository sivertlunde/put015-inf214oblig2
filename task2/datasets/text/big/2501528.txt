Airborne (1993 film)
{{Infobox film
| name           = Airborne
| image          = Airborne93poster.jpg
| image size     =
| caption        = Theatrical release poster Rob Bowman
| producer       = Bruce Davey Stephen McEveety
| screenplay     = Bill Apablasa
| story          = Stephen McEveety
| narrator       = Jack Black
| music          = Stewart Copeland
| cinematography = Daryn Okada
| editing        = Harry B. Miller III
| studio         = Icon Productions
| distributor    = Warner Bros. Pictures
| released       = USA: September 17, 1993 Australia: March 31, 1994
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = United States dollar|USD$2,600,000 
| gross          = $2,850,263 (US)   
| preceded by    =
| followed by    =
}}
 Jack Black.

==Plot==
Mitchell Goosen (McDermott) is a teenager from California who loves to surf and rollerblade.  His zoologist parents are given the opportunity for grant work in Australia for six months. Eager to accompany his parents to the surf-friendly shores of the South Pacific, he is dismayed to find out that he will not be joining them and instead will be living with his aunt and uncle in Cincinnati, Ohio, so as to finish the remainder of his high school semester. He arrives in the midst of a winter storm to a blue-collar Midwestern city, quickly coming to the realization that this is far from the free-spirited beach atmosphere that he has been accustomed to. To add to his disillusion he meets his cousin Wiley (Green), who at first glance is an awkward teenager and whose parents lifestyle and demeanor, though warm and hospitable, is a bit old-fashioned.
 fish out hockey players who chastise Mitchell for his easy going "Maharishi" philosophy and "California" appearance. These antagonists include Jack (Conrad), Augie (Black), Snake (Vargas), Rosenblatt, and the Banduccis. With an upcoming hockey game against the rival "preppy|preps", Wiley and subsequently Mitchell are asked to fill-in for two students, who are caught putting a laxative into one of their teachers coffee (never shown in the film). Mitchell inadvertently scores a goal for the preps, cementing his disdain by the hockey players, and in particular Jack, who proceeds to tackle Mitchell while still on the ice, concussing him and leaving him unconscious for what appears to be hours. Over the course of the next few weeks, Mitchell and Wiley are harassed relentlessly, eventually culminating with a dream Mitchell wakes to which convinces him to peacefully confront the situation.

During the interim, Mitchell falls in love with Nikki (Powell). During a double with his cousin and Nikkis friend Gloria, Blane physically confronts Mitchell who is only saved when Jack arrives to stand up for Nikki who, as it turns out, is also Jacks sister. Mitchells dream comes to fruition when he decides to proactively join Jack and his ice hockey brethren for a street hockey game against the preps. Mitchell embarrasses Blane, endearing a change of heart from his teammates. Snake, Augie, the Banduccis, and the rest of the hockey team, with the notable exception of Jack, solicit Mitchells help and rollerblading expertise in a race down a harrowing street route termed Devils Backbone against the preps. It is agreed upon that the first team with three members crossing the finish line will be deemed the winner. An aggressive and athletic Snake reaches the finish first for Mitchells team, but two preps swiftly follow suit. Needing only one more person to win and with Blane in sight of the end, he decides to barrel into Mitchell but poorly times his attack and instead lands in the waters below. This leaves Jack and Mitchell in clear sight of the finish line, as they approach in tandem victory to the cheers of their awaiting schoolmates, and kisses of respective love interests. Mitchell has finally earned the respect of Jack and his friends, and he is lifted on the shoulders of a cheering crowd as the movie ends.

==Cast==
* Mitchell - Shane McDermott
* Wiley - Seth Green
* Nikki - Brittney Powell
* Jack - Chris Conrad
* Aunt Irene - Edie McClurg Patrick OBrien
* Augie - Jack Black
* Gloria - Alanna Ubach
* Snake - Jacob Vargas
* Blane - Owen Stadele Chris Edwards
* Tony Banducci - Daniel Betances
* Mark Banducci - David Betances
* Ice Skating / Roller Hockey Extra - Travis Hafer
* Supermac - Christian McVeigh

==Box office==
The film was released in 982 theaters. It made $2,850,263 domestically, and $1,262,239 in its opening weekend. 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 