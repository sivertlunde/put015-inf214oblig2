L'aube à l'envers
 
{{Infobox film
| name           = Laube à lenvers
| image          = 
| caption        = 
| director       = Sophie Marceau
| producer       = Philippe Carcassonne Philippe Jacquier
| writer         = Sophie Marceau
| starring       = Judith Godrèche Jerzy Gralek Anna Nehrebecka
| music          = Stanislas Syrewycz
| cinematography =  
| editing        = Marie-Sophie Dubus
| distributor    = 
| released       =  
| runtime        = 10 minutes
| country        = France
| language       = French
| budget         = 
}}

Laube à lenvers is a 1995 French short film written and directed by Sophie Marceau and starring Judith Godrèche, Jerzy Gralek, and Anna Nehrebecka.    It was screened in the Un Certain Regard section at the 1995 Cannes Film Festival.   

==Plot==
A young woman comes home to a half-empty apartment, and she feels alone. An older man walks through the corridors of a half-empty airport, and he feels alone and sad. He takes a photograph out of his wallet, tears it in two and drops it on the moving walkway. Both are torn in two. A girl in Paris is alone with a cat. A man arrives in Warsaw, and a woman is there to meet him. She drives him to his parents home. An accident, a murder—nothing alters the imperturable course of life. 

==Cast==
* Judith Godrèche
* Jerzy Gralek
* Anna Nehrebecka
* Pawel Burczyk
* Maciej Maciejewski
* Danuta Szaflarska  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 