Pussyfoot (film)
{{Infobox film|
 name = Pussyfoot |
 image =  Pussyfoot_film.jpg|
 caption=Theatrical poster |
 director = Dusan Sekulovic |
 producer = Dusan Sekulovic |
 writer = Dusan Sekulovic |
 starring = Irwin Pelkalvski Jeffrey Coyne Nikoleta Sekulovic Lael Logan Michael De Nola Shana Chanel|
 movie_music = George Ilijin|
 released = 2008 |
 runtime = 90 minutes |
 language = English |
  music = |
 awards = |
 budget = USD 75,000     
}} 2008 comedy film directed by Dusan Sekulovic.

==Synopsis==
The film tells the story of a bristly-faced resident Expatriate|expat, Irwin, and his friends as they experience their second coming-of-age in New York City. For Irwin, there are “women” and then there are “girls”: women want to get married and girls just want to have fun. For the single Anny, there are three types of men: married, gay and idiots. Polish polka bars, confusing self-help books and a mythical hooker who never sleeps with her clients confound the friends’ American journey. 

==Cast==
The cast includes: 
* Atul Singh
* Chiara de Luca
* Christian Georgescu
* Irwin Pelkalvski
* Jason Bittle
* Jeffrey Coyne
* Lael Logan
* Lex Alexander
* Lora Grillo
* Michael De Nola
* Nikoleta Sekulovic
* Paula Panzarella
* Ramona Ganssloser
* Shana Chanel
* Vinny Muffoletto

==Awards and recognition==
The film screened at the 2008 Fort Lauderdale International Film Festival,  was a 2008 Moondance International Film Festival Finalist  and was named the Audience Award Feature Film at the 2008 Chashama Film Festival. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 