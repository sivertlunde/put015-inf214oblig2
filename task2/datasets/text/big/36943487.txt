Killjoy 3
{{Infobox film
| name        = Killjoy 3
| image       =  
| director    = John Lechago
| producer    = Charles Band
| writer      = John Lechago
| starring    = Trent Haaga Victoria De Mare Jessica Whitaker Michael Rupnow Spiral Jackson Oliva Dawn York Al Burke Tai Chan Ngo and Darrow Igus
| music       = Michael Sean Colin
| distributor = Full Moon Features
| released    = December 14, 2010
| budget      =
| runtime     = 78 min.
| country     = United States
| language    = English
}}
Killjoy 3 (also known as Killjoys Revenge) is a 2010 slasher film and sequel to Full Moons hit urban horror movie,   and Demonic Toys 2.  There are three deleted scenes from this movie that are shown in a recap montage in Killjoy Goes to Hell. These scenes go by real quick in the film, so the only way to see them is to play the montage slow motion and analysis each scene closely. The first scene shows the shaman outside the living room window as Sandie and Rojer clean up, but when Rojer senses his presence and turns, the shaman is gone. The second one takes place shortly after that, where Rojer is making advances towards Sandie in bed, but she turns him down. The third one occurs when Killjoy tries to get the mirror to work, where he calls technical services, and appears to get into a heated argument with the operator, but Batty calms him down. Why these scenes were cut out or not left in a deleted scenes section is unknown.

==Plot==
Some time passes and Killjoy is once again called, this time through a blood pact. Immediately he resorts to using the blood spilled by his summoner to create three underlings, which he dubs new evil clowns, Punchy, Freakshow and Batty Boop. However the man does not name a victim for Killjoy, leaving the scene without doing so. This causes Killjoy and his posse to vanish and return to their world.
  Zilla inspects the mirror on his own, whereby he is transported to Killjoy (film series)|Killjoys world. Killjoy stages a boxing match between Zilla and Punchy which nearly kills Zilla, however his friends discover his physical body and successfully resuscitate him, rescuing his consciousness from Killjoys world. Furthermore, a barrier has been placed over the house, trapping the group indoors.
  giant mallet by Killjoy and Zilla suggests Punchy take this opportunity to strike back against Killjoy, who slays him for his insolence. 
 
The professor finally enacts his plan to say the name Killjoy originally went by in antiquity, in an effort to subdue him. He also reveals himself to be the father of Michael, whose soul Killjoy exploited before destroying. The professor chose not to name a victim while initially summoning Killjoy because the target of his revenge was ultimately Killjoy himself. The clown applauds the professor for his deviousness in using both himself and the students alike to achieve his revenge. Killjoy proclaims that the souls he consumes become a part of him, and the spirit of Michael appears, consoling his father. With the professors guard down, Killjoy slays him as well by smashing him with the giant mallet. The two survivors, Sandy and Zilla, resort to laughter to quell the clown, but Zilla is killed when Killjoy taunts them while actually trying to be humorous. Sandy continues to laugh at Killjoy while shouting his original name, which incapacitates him long enough for her to return to the mirror and be transported to her world. Killjoy then explodes in a fit of innards. The Magic mirror disappeared from the wall. Sandy is shown to be committed for insanity, having not stopped laughing since the ordeal, and under the suspicion of murdering her friends and the professor.

==Cast==

===Evil clowns===
*Trent Haaga as Killjoy The Demonic Clown
*Al Burke as Punchy the clown
*Tai Chan Ngo as Freakshow the mime clown
*Victoria De Mare as Batty Boop the sexy female clown

===Humans from Earth===
*Darrow Igus as The Professor
*Spiral Jackson as Zilla
*Quentin Miles as Michael
*Michael Rupnow as Rojer
*Jessica Whitaker as Sandy
*Olivia Dawn York as Erica

==References==
 

==External links==
* http://www.imdb.com/title/tt1603314/

 

 
 
 
 
 
 