Gaali Maathu
{{multiple issues|
 
 
 
}}
{{Infobox film|
| name = Gaali Maathu
| image = 
| caption =
| director = Dorai Bhagwan
| writer = T. R. Subba Rao|Ta. Ra. Su (novel) Lakshmi  Kokila Mohan
| producer = Dorai Bhagawan
| music = Rajan-Nagendra
| cinematography = B. C. Gowrishankar
| editing = P. Bhaktavatsalam
| studio = Anupam Movies
| released = 1981
| runtime = 130 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada Drama drama film Bhagwan duo. Lakshmi and Kokila Mohan in lead roles. 

The film was a musical blockbuster with all the songs composed by Rajan-Nagendra considered evergreen hits.

== Cast ==
* Jai Jagadish  Lakshmi  Mohan
* K. S. Ashwath 
* Hema Chowdhary Leelavathi
* Uma Shivakumar
* Sampath
* Mysore Lokesh

== Soundtrack ==
The music of the film was composed by Rajan-Nagendra with lyrics by Chi. Udaya Shankar.  All the songs composed for the film were received extremely well.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 = Naanenu Neenenu
| extra1 = S. P. Balasubramanyam
| music1 = 
| length1 = 
| title2 = Bayasade Bali Bande
| extra2 = S. P. Balasubramanyam, S. Janaki
| music2 = 
| length2 = 
| title3 = Nammura Santheli
| extra3 = Nagendra, Renuka
| music3 =
| length3 = 
| title4 = Omme Ninnannu 
| extra4 = S. Janaki
| music4 =
| length4 = 
| title5 = Nagisalu Neenu
| extra5 = S. Janaki
| music5 =
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 