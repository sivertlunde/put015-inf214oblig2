Appunni
{{Infobox film
| name           = Appunni
| image          = Appunni film.jpg
| image size     = 
| alt            = 
| caption        = Promotional Poster designed by Kitho
| director       = Sathyan Anthikad
| producer       = Ramachandran
| writer         = V. K. N. Menaka Bharath Gopi Mohanlal Sukumari
| music          =  
| cinematography = Anandakuttan
| editing        = G. Venkitaraman
| studio         = Revathi Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
Appunni is a 1984 Malayalam romantic comedy film written by V. K. N. and directed by Sathyan Anthikad. The film is based on V. K. N.s story Premavum Vivahavum. The lead roles are played by Nedumudi Venu, Menaka (actress)|Menaka, Bharath Gopi, Mohanlal and Sukumari.   

==Plot==
The film explores the complicated love triangle between the characters played by Nedumudi Venu, Mohanlal and Menaka. Appunni and Ammukutty are lovers from childhood. Ammukuttys father Ayyappan Nair arranges her marriage with a rich school teacher, Menon Mash. Ammukutty is happy to break her relationship with Appunni and get married to the rich and modern Menon Mash. 

However, Menon Mash cannot arrive on time for the marriage. Ayyappan Nair, who thought that Menon Mash might have cheated his daughter, conducts Ammukkuttys marriage with Appunni. But, Menon Mash arrives late night, and now Ayyappan Nair wants Ammukutty to marry Menon Mash himself. Shaked by the behaviour of her father and Menon Mash, Ammukkutty closes the door in front of them and decides to live with Appunni.

==Cast==
* Nedumudi Venu as Appunni Menaka as Ammukutty
* Mohanlal as Menon Mash
* Bharath Gopi as Ayyappan Nair
* Sankaradi as Adhikari
* Bahadoor as Hajiyar
* Sukumari as Malu
* Oduvil Unnikrishnan as Kurup Mash
* Kuthiravattam Pappu as Karunakaran
*Meenakumari as Menons Mother
* Kuttyedathi Vilasini

==Soundtrack==
The music was composed by Kannur Rajan and lyrics was written by Bichu Thirumala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kinnaaram tharivalayude || Vani Jairam || Bichu Thirumala ||
|-
| 2 || Thoomanjin thulli || K. J. Yesudas || Bichu Thirumala ||
|}

==References==
 

==External links==
*  

 
 
 
 

 