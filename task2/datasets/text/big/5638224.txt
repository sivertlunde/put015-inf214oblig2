The Born Losers
 
 
{{Infobox film
| name           = Born Losers
| image          = Bornlosersmp.jpg
| caption        = Film poster by Reynold Brown
| director       = Tom Laughlin (actor)|T. C. Frank (Laughlin) Don Henderson Tom Laughlin
| writer         = Elizabeth James
| narrator       =
| starring       = Tom Laughlin Elizabeth James Jeremy Slate
| music          = Mike Curb
| cinematography = Gregory Sandor
| editing        = John Winfield
| distributor    = American International Pictures
| released       =  
| runtime        = 113 min.
| country        = United States
| language       = English
| budget         = $400,000 Hollywoods Only Woman Producer a Born Winner
Clifford, Terry. Chicago Tribune (1963-Current file)   20 Aug 1967: g13. 
| gross          = $36 million 
}}
 action film Indian United Green Beret Vietnam veteran American Indians.  In 1968 he decided to introduce the Billy Jack character in a quickly written script designed to capitalize on the then-popular trend in motorcycle gang movies. The story was based on a real incident from 1964 where members of the Hells Angels were arrested for raping five teenage girls in Monterey, California|Monterey, California.

==Plot==
Billy Jack is introduced as an enigmatic, half-Indian Vietnam War veteran who shuns society, taking refuge in the peaceful solitude of the California Central Coast mountains. His troubles begin when he descends from this unspoiled setting and drives into a small beach town named Big Rock (Morro Bay). A minor traffic accident in which a motorist hits a motorcyclist results in a savage beating by members of the Born Losers Motorcycle Club. The horrified bystanders (including Laughlins wife, Delores Taylor, and their two children in cameo roles) are too afraid to help or be involved in any way. Billy Jack jumps into the fray and rescues the man by himself. At this point the police arrive and arrest Billy for using a rifle to stop the fight. (The irony here is that, unknown to Billy, the motorist is the one who starts the fight by inexplicably insulting one of the bikers.) 

The police throw Billy in jail and fine him heavily for discharging a rifle in public. He is treated with suspicion and hostility by the police. Meanwhile, the marauding bikers terrorize the town, rape four teenage girls (Jane Russell plays the mother of one of the girls), and threaten anyone slated to testify against them.  One of the girls, played by Susan Foster, later recants, saying she willingly gave herself to the biker gang. (Foster would go on to play a larger supporting role in Billy Jack.) 

Co-scriptwriter Elizabeth James plays Vicky Barrington, a bikini-clad damsel-in-distress who is twice abducted and abused by the gang. The second time, she and Billy are kidnapped together.  After Billy is brutally beaten, Vicky agrees to become the gangs sexually compliant "biker mama" if they release Billy. At the police station, Billy is unable to get help from the police or the local residents and must return to the gangs lair to rescue Vicky by himself.
 Springfield rifle, captures the gang, shoots the leader (Jeremy Slate) between the eyes, and forces some of the others to take Vicky, whos been badly beaten, to the hospital.  As the police finally arrive, Billy abruptly rides away on one of the gangs motorcycles. 

The anti-authority sentiment continues up to the end when a police deputy accidentally shoots Billy in the back, mistaking him for a fleeing gang member. He is later found, nearly dead, lying by the shore of a lake. He is placed on a stretcher and is flown to the hospital in a helicopter as Vicky and the sheriff give him a salute.

==Production== Seal Beach, Huntington Beach, Rancho Palos Morro Bay, and other coastal locales.  The bikers lair in Seal Beach was once owned by silent film star Rudolph Valentino as a hideaway from Hollywood.

According to Laughlins DVD audio commentary, filming was completed in just three weeks on an operating budget of $160,000.  To cut costs, a stunt scene of a biker crashing into a pond was taken from American Internationals 1966 comedy The Ghost in the Invisible Bikini.  The movie was not released until 1968.

Laughlin ran out of money during post production, but showed the film to American International Pictures who bought out the original investors and gave Laughlin $300,000 to finish it. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p252 

The film was commercially successful, and resulted in Laughlin being able to raise the funds to make its sequel,   was released. 

==Reception==
Although highly successful at the box office, critical response was generally negative.  Film critic Leonard Maltin criticized Laughlins films for "using violence as an indictment of violence." 

In 1967 the film earned an estimated $2,225,000 in North American rentals. 

The movie was re-released by AIP in 1974 following the success of Billy Jack. AIP issued ads which proclaimed "THE ORIGINAL BILLY JACK IS BACK!", which led to a lawsuit from Laughlin. Losers Ad Results in Suit
Murphy, Mary. Los Angeles Times (1923-Current File)   28 June 1974: f24.  Following Laughlins lawsuit, the advertising for the re-release of "Born Losers" was changed.  All newspaper advertising had to include the disclaimer "This is a Re-release" to make viewers aware that the film was not the film Billy Jack. 

== The banned Hungarian version == Hungarian actors, with Zoltán Latinovits, Gábor Agárdy, Gyula Szabó, or Éva Almási.  The film was shocking and very popular in Hungary, the tickets sold out near the cash of the Pushkin Cinema in Budapest. Three days later, the film was banned in Hungary. Forty years later, the film again prevalent in amateur downloader circles and in the YouTube. 

==References==
 
*Weiner, Mike,  Motorcycle News, Review of "The Born Losers" 
*White, Rusty, Entertainment Insiders "1967 films : The Born Losers"
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 