The Scorpion King
 
 

{{Infobox film
| name           = The Scorpion King
| image          = The Scorpion King poster.jpg
| caption        = Theatrical release poster
| director       = Chuck Russell
| producer       = Sean Daniel James Jacks Kevin Misher Stephen Sommers
| story          = Stephen Sommers   Jonathan Hales
| screenplay     = Stephen Sommers   William Osborne   David Hayter The Rock Kelly Hu Bernard Hill Grant Heslov Peter Facinelli Steven Brand Michael Clarke Duncan
| editing        = Greg Parsons Michael Tronick
| music          = John Debney
| cinematography = John R. Leonetti World Wrestling Entertainment Alphaville Films
| distributor    = Universal Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States Germany Belgium
| language       = English
| budget         = $60 million 
| gross = United States $91,047,077 Worldwide: $165,333,180 
}} Dwayne "The Rock" Johnson, Kelly Hu, Grant Heslov, and Michael Clarke Duncan. It is a prequel/spin-off to The Mummy (franchise)#Stephen Sommers series (1999-2008)|The Mummy trilogy, and follows the story of Mathayus the Scorpion King, the character featured in The Mummy Returns.
 The Mummy and The Mummy Returns, and reveal Mathayus origins and his rise to power as the Scorpion King. The name is a reference to a historical king of the Protodynastic Period of Egypt, King Scorpion.

==Plot==
"Before the time of the pyramids," a horde from the East invades the ancient world, led by the ruthless Memnon, who by their law, is king for being their greatest warrior. His many victories come from the help of a sorcerer who predicts the outcomes of battles, leaving only a few free tribes to oppose him.

Mathayus, his half-brother Jesup, and friend Rama, the only three true remaining Akkadians, are hired by King Pheron of the last free tribes to kill Memnons sorcerer for twenty blood rubies, upsetting Pherons son Takmet, as it is the last of their treasury, and Nubian King Balthazar, who dislikes the Akkadians. The Akkadians manage to sneak into Memnons camp, but are ambushed by Memnons guards, having been tipped off by Takmet, who killed Pheron and defected to Memnons side. Jesup and Rama are hit by arrows, but Mathayus manages to sneak into the sorcerers tent, where he sees that the visionary is actually a sorceress, Cassandra. Mathayus is then captured and meets Memnon himself, who brutally executes Jesup in front of him, and is about to kill Mathayus too. Cassandra, however, tells Memnon that the gods wish Mathayus to survive the night, and to defy them would incur their wrath. Memnon has Mathayus buried to his neck in the desert to be devoured by fire ants, but manages to escape with help from a horse thief, Arpid.

Deciding to finish his mission and avenge his brother, Mathayus sneaks into Memnons stronghold, Gomorrah, and manages to enter Memnons palace with help from an urchin. He briefly meets Memnons court magician, Philos, who hides him and then directs him to the courtyard where Memnon is training. Mathayus tries to shoot Memnon from the watchtower, but is forced to save the urchin from having his hand amputated by shooting the axe out of Takmets hand, alerting the guards to his presence. Mathayus only barely manages to escape Gomorrah, abducting Cassandra along the way, knowing that Memnon will come for her. Cassandra tries to escape from Mathayus and even tells him that she has been Memnons prisoner since she was a child. Sympathetic, Mathayus allows her the choice of leaving, but warns her of worse dangers and that she is likely safer with him. However, Memnon sends his right-hand man Thorak and a group of guards to kill Mathayus and retrieve Cassandra, but Mathayus manages to slay them all single-handedly with the aid of a sandstorm. With his dying breaths, Thorak manages to stab Mathayus in the leg with a scorpion blood-laced arrow. Cassandra, however, uses her magic to save Mathayuss life. As an insult, Mathayus sends Thoraks blood-stained pendant to Memnon.

Mathayus, Arpid and Cassandra then run into Philos, who fled Memnons palace and has perfected an explosive powder he was working on. However, they are ambushed by the rebels, now under the rule of Balthazar. Though Mathayus defeats Balthazar in a fight and earns his grudging respect and sanctuary, Cassandra has a vision of Memnon and his army slaughtering the entire rebel camp. She informs Mathayus and tells him of a prophecy Memnon once heard: when the moonlight reaches the House of Scorpio, the King on High will become the invincible Scorpion King, and Memnon believes himself to be the one destined to become the Scorpion King. Furthermore, she informs Mathayus that if he faces Memnon, he will most likely die, but Mathayus assures her that he will make his own destiny and they make love.

The next morning, however, Cassandra returns to Memnon in order to stall him and possibly kill him. Mathayus, with help from Balthazar, Arpid, Philos and the army of rebels, launches an all-out assault on Memnons stronghold, facing Memnon personally before he can kill Cassandra, while Balthazar confronts and kills Takmet, avenging Pheron, and takes on the full force of Memnons forces alone. The battle rages on until Mathayus is shot by a guard as in Cassandras vision. As Memnon takes his place in the House of Scorpio to become the Scorpion King, Cassandra kills the guard while Mathayus retrieves his bow, pulls the arrow out of his back and uses it to shoot the exhausted Memnon, sending him off the edge of the roof just as Philos and Arpid use the explosive powder to destroy the palaces foundation stone, bringing down the bulk of Memnons forces. Memnon is consumed by the flames as he falls to his death. With the battle over, the remnants of Memnons army bow before Mathayus, who is hailed as the Scorpion King.

In the aftermath, Mathayus and Balthazar share a good-natured farewell as the latter returns to his own kingdom. Cassandra tells Mathayus that she sees a period of peace and prosperity coming, but subtly warns him that it will not last forever. Undeterred, Mathayus decides that they will make their own destiny.

==Cast== The Rock Mathayus
*Steven Brand as Memnon
*Kelly Hu as Cassandra
*Grant Heslov as Arpid
*Bernard Hill as Philos
*Michael Clarke Duncan as Balthazar
*Peter Facinelli as Prince Takmet
*Sherri Howard as Queen Isis
*Ralf Möller as Thorak
*Branscombe Richmond as Jesup
*Roger Rees as King Pheron
*Joseph Ruskin as Tribal Leader
*Tyler Mane as Barbarian Chieftain

==Reception==

===Critical response===
 
*The Scorpion King holds a 41% rating on  
| publisher = Flixster}}  

* 
| publisher = Amazon.com| accessdate = 2010-06-07 }} 

*Roger Ebert, a film critic of the Chicago Sun-Times, gave the film 3 out of 4 stars, writing "A wise move, too, because The Scorpion King is set thousands of years before the Pyramids, so property values in Gomorrah were a good value for anyone willing to buy and hold." 

*James Berardinelli of ReelViews gave the film two stars (out of four), saying: "Its possible to make an engaging action/adventure picture of this sort, but The Scorpion King isnt it." 
 , ReelViews   

*Dennis Harvey of Variety (magazine)|Variety gave a positive review, saying the film "rouses excitement mostly from stuntwork and thesp agility rather than CGI excess." {{cite news | date = April 18, 2002| url = http://www.variety.com/review/VE1117917485.html Variety }} 

*Nathan Rabin of  
| publisher = The Onion }}  

*Owen Gleiberman of Entertainment Weekly gave the film a score of C+, calling it "plodding and obvious" but adding that The Rock "holds it together." 

*Jonathan Foreman of the New York Post gave a negative review, saying that The Scorpion King "has none of the qualities &mdash; epic sweep, relative originality and heartfelt bloodthirstiness &mdash; that made Conan the Barbarian (1982 film)|Conan so trashily entertaining." 

===Box office===
   
The Scorpion King grossed $12,553,380 on its opening day and $36,075,875 in total over the weekend, from 3,444 theaters for an average of $10,475 per venue, and ranking at #1 at the box office. It then dropped 50 percent in its second weekend, but remained at #1, earning another $18,038,270. The film closed on June 27, 2002, with a total domestic gross of $91,047,077, and an additional $74,286,103 internationally, for a total worldwide gross of $165,333,180, against a budget of $60 million, making it a moderate box office success.   

==Soundtrack==
{{Infobox album
| Name        = The Scorpion King
| Type        = Soundtrack
| Longtype    = 
| Artist      = Various Artists
| Cover       = The Scorpion King soundtrack.jpg
| Cover size  =
| Caption     =
| Released    =  
| Recorded    = Heavy metal, alternative metal, alternative rock, nu metal, post-grunge, hard rock
| Length      = 01:00:41
| Language    = Universal
| Producer    =
| Compiled by =
| Chronology  =
| Last album  =
| This album  =
| Next album  =
| Misc        =
}}
{{Album ratings rev1 = Allmusic rev1score =   
}}
;Track listing
  I Stand Alone" by Godsmack Tweaker Remix)" by P.O.D.
# "Break You" by Drowning Pool
# "Streamline" by System of a Down Creed
# "Yanking Out My Heart" by Nickelback
# "Losing My Grip" by Hoobastank Flaw
# "Iron Head" by Rob Zombie feat. Ozzy Osbourne
# "My Life" by 12 Stones
# "Along the Way" by Mushroomhead
# "Breathless" by Lifer
# "Corrected" by Sevendust Injected
# Breaking Point
# "Glow" by Coal Chamber

==Video games==
The film spawned two video games:   for the  , in which Cassandra is abducted by the ruthless sorcerer Menthu and his lackey, the witch Isis (not to be confused with Queen Isis from the film), prompting Mathayus to undergo a quest to uncover the legendary Sword of Osiris and use it to defeat Menthu and Isis once and for all and rescue Cassandra.

==Prequel and sequels==
Following the films release, there were initial plans for a sequel with Johnson to return as Mathayus, but these plans eventually fell through and the project was shelved. A direct-to-video prequel,  , was released in 2008 with  , was released in 2012 with Victor Webster as Mathayus and Billy Zane as the villain, King Talus.
 WWE female wrestler Eve Torres joined the cast.  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 