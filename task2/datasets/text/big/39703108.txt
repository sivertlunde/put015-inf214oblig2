Scenic Route (film)
{{Infobox film
| name           = Scenic Route
| image          = Scenic Route film.jpg
| alt            = 
| caption        = film poster
| director       = Kevin Goetz Michael Goetz
| producer       = Scott Freeman Brion Hambel Paul Jensen Luke Rivett
| writer         = Kyle Killen
| starring       = Josh Duhamel Dan Fogler
| music          = Michael Einziger
| cinematography = Sean ODea
| editing        = Kindra Marra
| studio         = Anonymous Content Best Medicine Productions
| distributor    = Vertical Entertainment
| released       =   |2013|08|23|ref2=   }}
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} South by Southwest Film Festival and was released on August 23, 2013.
== Plot ==
Mitchell and Carter, old friends who have begun to drift apart, agree to go on a road-trip.  Along the way, their truck breaks down in the California desert.  Mitchell is relieved when a motorist stops and offers help, but Carter admits he has sabotaged their truck in order to give them time to reconnect and force a dialogue between them.  Carter, who has remained idealistic and continued with an unsuccessful writing career, believes that Mitchel has sold out, and Mitchell, who gave up his rock band for a corporate job, believes that Carter remains stuck in adolescent rebellion.  They dismiss the helpful motorist after Carter replaces the part he removed from the engine.  However, after switching off the engine, the truck refuses to start, and tensions continue to rise as the former friends argue and blame each other.  In a moment of reconciliation, Carter helps Mitchell to realize one of his dreams and gives him a crude Mohawk haircut.  The two later come to blows over an argument, and Mitchell believes that he has accidentally killed Carter.  As a stricken Mitchell drags Carters body to a shallow grave, Carter wakes up in a daze and freaks out.

Thinking that Mitchell was going to bury him alive, Carter flees from their impromptu camp, despite Mitchells protestations.  Carter later returns during the freezing cold night, and they settle into a truce.  Starving and ill-prepared for the deserts temperature extremes, the two decide to give up on waiting for help, and they attempt to cross through the desert on foot.  As they pass through an abandoned town, Mitchells cell phone starts ringing.  They excitedly answer the call and are soon rescued.  In a rapid montage, Mitchell goes back to his roots as a rock musician, Carter moves into a guest house provided by Mitchell and his wife, Carter and Mitchells wife come to respect each other, and Carter and Mitchell each experience professional success and personal growth.  However, Mitchell calls Carter to voice his suspicions of their too-perfect dénouement, and suggests that it is a shared delusion experienced by the pair as they die in the desert, Carter denies this possibility. The final scene of the film shows Mitchell lying in bed, with images from the desert while a faint cellphone sound is ringing.

== Cast ==
* Josh Duhamel as Mitchell
* Dan Fogler as Carter
* Miracle Laurie as Joanne
* Christie Burson as Annie
* Peter Michael Goetz as Old man
* Jamie Donovan as Tow truck driver
* Ethan Maher as Cole
* Bob Schwartz as Office Worker

== Production ==
Stars Duhamel and Fogler were drawn to the film because they could relate to the characters.  Duhamel also wanted to do a film that was thematically darker than his previous work.   The directors wanted a character-driven, dialog-heavy project as their first project.  The desert shots took twelve days, and the Los Angeles shots took another three. 

== Release ==
Scenic Route premiered at SXSW on March 8, 2013.   It opened theatrically on August 23, 2013, in New York, Los Angeles, and other cities. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 62% of 21 surveyed critics gave it a positive review; the average rating was 5.8/10.   Metacritic rated it 34/100 based on ten reviews.   Olie Coen of DVD Talk rated it 3/5 stars and said of the leads that "they both held their own in what must have been difficult roles to pull off", though he found the film to be "good, not great".   Gordon Sullivan of DVD Verdict called it "a decent little thriller mixed with a character study."   Noah Lee of Film Threat rated it 2.5/5 stars and wrote, "Its beautifully shot, has awesome performances from its two leads but just ultimately is marred by its inconsistent storytelling and brutally awful ending."   Scott Bowles of USA Today rated it 3/4 stars and called it gripping but rushed and lacking subtlety.   John DeFore of The Hollywood Reporter said that "the film is less harrowing than it intends to be", though the second half is more successful once it becomes more concerned with survival than a less believable character study.   Inkoo Kang of the Los Angeles Times called the film contrived and uninspired.   G. Allen Johnson of the San Francisco Chronicle rated it 2/5 stars and also called the film uninspired and predictable.   Scott McDonald of The A.V. Club rated it D- and called it "a two-hander featuring dull, unsympathetic twits bickering for an hour and a half".   Leah Churner of the Austin Chronicle wrote that the film "is sometimes funny, sometimes shocking, but rarely unpredictable." 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 