The Last Straw (film)
{{Infobox film
| name=The Last Straw
| image=The-Last-Straw.jpg
| caption = Film poster
| director=Jon Monday
| starring=Charles Bukowski
| producer=Jon Monday
| distributor= mondayMEDIA
| runtime= 74 minutes
| language=English
}}
The Last Straw is a film documenting the very last live poetry reading given by Charles Bukowski, even though he lived and wrote for another 14 years. The reading was given at The Sweetwater, a music club in Redondo Beach, California on March 31, 1980. It is produced and directed by Jon Monday for mondayMEDIA.

==Plot synopsis==
In March 1980, Takoma Records was about to re-release an audio recording on vinyl of a Charles Bukowski live poetry reading given in San Francisco years earlier. As part of the promotion of the album release, Bukowski agreed to give a new live reading, even though he hated doing them. Jon Monday, then General Manager of Takoma Records, video taped the event. 

But by later that year Bukowskis book royalties and movie advances provided him enough of a living that he no longer had to do readings.

The Redondo Beach reading turned out to be the very last poetry reading Bukowski ever gave. The video recording of that night stayed in an archive until 2008 when mondayMEDIA entered into an agreement with Bukowskis widow Linda to release it on DVD.

Bukowskis readings were known for their riotous back and forth with the audience and this recording shows this in full color. Each poem is set between a tense dialog - with Bukowski giving and taking insults and threats with members of the audience. It ends with the prophetic words "This reading is over".

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 


 