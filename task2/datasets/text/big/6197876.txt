The Toxic Avenger Part II
{{Infobox film
| name           = The Toxic Avenger Part II
| image          = TheToxicAvenger2.jpg
| image_size     = 200px
| alt            =
| caption        = Theatrical release poster Michael Herz
| producer       = Michael Herz Lloyd Kaufman Jeffrey W. Sass
| writer         = Gay Partington Terry
| screenplay     = Lloyd Kaufman
| story          = Lloyd Kaufman
| based on       =  
| starring       = Ron Fazio John Altamura Phoebe Legere
| music          = Barrie Guard
| cinematography = James London
| editing        = Michael Schweitzer
| distributor    = Troma Entertainment Lorimar
| released       =  
| runtime        = 96 minutes 103 minutes  
| country        = United States
| language       = English Japanese
| budget         = $2.3 million
| gross          = $792,966
}} comedy horror film released by Troma Entertainment. It was directed by Lloyd Kaufman and features The Toxic Avenger in an adventure to Japan to meet his father. The film has received cult status among a new audience almost a generation after it was first released. Go Nagai makes a cameo appearance and the film is also the debut of actor/martial artist Michael Jai White and musician/composer/performance artist Phoebe Legere.

==Synopsis==
  original film, The Toxic Avenger (Ron Fazio) is lured to Tokyo, Japan by the evil corporation Apocalypse Inc. So while the Toxic Avenger is fighting crime in Tokyo, Apocalypse Inc. spreads evil in Tromaville and it is up to the famous hideously deformed superhero of superhuman size and strength to stop the abominable Apocalypse Inc. from attacking both Tokyo and Tromaville.

==Cast==
* Ron Fazio as The Toxic Avenger / Apocalypse Inc. Executive
* John Altamura as The Toxic Avenger
* Phoebe Legere as Claire
* Rick Collins as Apocalypse Inc. Chairman
* Lisa Gaye as Malfaire Michael Herz, uncredited voice) as Big Mac Bunko
* Mayako Katsuragi as Masumi
* Tsutomu Sekine as News caster
* Jessica Dublin as Mrs. Junko
* Jack Cooper as Big Mac Junko
* Don Eckhart as Kid riding bike in opening scene
* Fernando Antonio as Apocalypse Inc. Executive
* Paul Borghese as Lou Sipher: Apocalypse Inc. Executive
* Sylvester Covin as Apocalypse Inc. Executive
* William Decker as Apocalypse Inc. Executive
* Joe Fleishaker as Apocalypse Inc. Executive
* Mark Fucile as Apocalypse Inc. Executive
* Marc Allan Ginsberg as Apocalypse Inc. Executive
* Sal Lioni as Apocalypse Inc. Executive
* Doug McDonald as Apocalypse Inc. Executive
* Benny Nieves as Apocalypse Inc. Executive
* Kariim Ratcliff as Apocalypse Inc. Executive
* Michael Jai White as Apocalypse Inc. Executive
* Susan Whitty as Apocalypse Inc. Executive
* Jeremiah Yates as Apocalypse Inc. Executive
* Dan Snow as Cigar Face Sumo wrestler / Voice of fish salesman

==Versions==
The Unrated Directors cut version released on DVD by Troma Entertainment is actually missing almost all scenes of gore. The full version with an extra 10 minutes of gore is currently only available in the Tox Box DVD set which is now out of print.

The Japanese and German releases are the full uncut version.

The 2008 Complete Toxic Avenger 7-disc DVD set includes the uncut version and is the same disc found in the Tox Box. However, there are reports of the heavily cut version occasionally being included in the Complete Toxic Avenger. This may be because nowhere on the disc art or DVD do the words Directors Cut appear. The unedited "Directors Cut" can be told apart from the edited version by checking the feature running time, or by listening to the audio commentary by director Lloyd Kaufman. The commentary track is different for both versions, and the director mentions which cut is shown.
 High Definition. 

==Reception==
The Toxic Avenger Part II currently holds a rare 0% on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 