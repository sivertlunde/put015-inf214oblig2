The Finishing Line
 

{{Infobox film
| name           = The Finishing Line
| image          = 
| caption        = 
| director       = John Krish
| producer       = James Ritchie
| writer         = Michael Gilmour John Krish
| music          =
| cinematography = 
| distributor    = British Transport Films
| released       = 1977
| runtime        = 
| country        = United Kingdom
| followed by    = 
}}
The Finishing Line is a short film produced in 1977 by  , written by Krish and Michael Gilmour, and produced by James Ritchie. It was filmed in the vicinity of the then-closed Watton-at-Stone railway station, Hertfordshire.

==Plot==

The film begins with a headmasters voice telling a large group of children he knows they have been playing on the railway with a  young boy sitting on a railway bridge wall. As he ponders on his thoughts, he considers the idea of holding an Olympic Games-style sports event on the railway line. The rest of the film shows his imagined idea of what would happen, with children being split into four competitive teams to take part in different activities often carried out by young people trespassing on the railway. Three "games" are held, in which the children are challenged to break through the fence surrounding the railway line, play "chicken" with the trains and throw things at passing trains. Each time we see the tragic consequences of these activities, such as one scene where a drivers face is shredded by broken glass after a child throws a brick through the train window. The final task is for the children to run through a tunnel, but after they enter, we see a train approaching. Only four children cross the end of the tunnel, each of them injured terribly. One boy who crosses the finish line collapses as the overhead speaker announces the final results. The film finishes as a group of adults appear and go into the tunnel to carry out the bodies of the dead and injured children, which are then laid out in a long line along the railway track. The camera pans out to show all the dead and bloodied children along the track before returning to the boy sitting on the railway bridge wall, who seems to be reconsidering the idea.

==External links==
* 
* British Transport Films:  
*  

 
 
 
 
 
 
 