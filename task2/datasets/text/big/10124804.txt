Night Riders (1981 film)
{{Infobox film| name = Night Riders
  | director = Martin Hollý
  | screenplay = Tibor Vichta
  | cinematography = František Uldrich
  | starring = Radoslav Brzobohatý Michal Dočolomanský Soňa Valentová Leopold Haverl
  | runtime = 88 min. Slovak and English and Czech
 1981
}}

Night Riders is a 1981 Slovak film.

==Synopsis==
Shortly after World War I and creation of the new Czechoslovak Republic, two war veterans are confronted in a small village in the north of Slovakia at the border with Poland. For one of the protagonists - Marek Orban (Michal Dočolomanský), this village is his home. As it is isolated and lacks job opportunities, he persuades the inhabitants to emigrate to America. The villagers have to sell all the cattle to be able to buy the boat tickets, but it still is not enough. Marek Orban has to smuggle horses to Poland.

The second protagonist - customs officer Halva (Radoslav Brzobohatý) arrives from Prague to protect the borders and the law of the new republic. Both men of honor and principle, they are led to conflict which results in a death of villagers and customs officers.
The absurdity of this tragedy is underlined at the end of the film when the border is moved, the village becomes a part of Poland and in the background we can see Marek Orban smuggling horses from Poland to Poland.

==References==
 
*  
*  
*  


 
 
 
 
 
 
 