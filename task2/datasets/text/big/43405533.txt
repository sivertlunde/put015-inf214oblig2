Deivapiravi
{{Infobox film
| name = Deivapiravi
| image =
| caption =
| director = Krishnan-Panju
| producer = Kamaal Brothers
| writer = K. S. Gopalakrishnan Padmini S. S. Rajendran K. A. Thangavelu M. N. Rajam
| music = R. Sudarsanam
| cinematography = S. Maruthi Rao
| editing = S. Panjabi
| studio = Kamaal Brothers
| distributor = AVM Productions
| released =   
| country = India
| language = Tamil
}}
 Indian Tamil Tamil film, All India Certificate of Merit for the Third Best Feature Film. 

==Cast==
*Sivaji Ganesan Padmini
*S. S. Rajendran
*K. A. Thangavelu
*M. N. Rajam
*M. S. Sundari Bai
*C. Lakshmi Rajyam

==Soundtrack==
The music composed by R. Sudarsanam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anbale Thediya || C. S. Jayaraman || Udumalai Narayana Kavi ||
|-
| 2 || Ivar Kaana Avar || Jamuna Rani || Kavi Rajagopal ||
|-
| 3 || Kaalai Vayasu || Jamuna Rani || Thanjai N. Ramaiah Dass ||
|-
| 4 || Kattadathukku || S. C. Krishnan, L. R. Eswari || K. S. Gopalakrishnan ||
|- Kannadasan ||
|-
| 6 || Mounam Ennum Ragam || T. M. Soundararajan, P. Susheela ||
|-
| 7 || Nil Nil Nil Ilam Thendrale || T. M. Soundararajan, P. Susheela ||
|-
| 8 || Poovai Oru Poo || T. M. Soundararajan, P. Susheela ||
|-
| 9 || Silar Siripar || T. M. Soundararajan ||
|-
| 10 || Thaara Thaara || Jamuna Rani, M. S. Rajeswari || Thanjai N. Ramaiah Dass  ||
|-
| 11 || Thannaithane || C. S. Jayaraman || Udumalai Narayana Kavi ||
|-
| 12 || Vayasu Pennai || T. M. Soundararajan, Jamuna Rani || A. Maruthakasi ||
|}

==References==
 

==External links==
*  

 

 
 
 
 
 


 