Boys Life (film)
 
{{Infobox film
| name           = Boys Life
| image          =
| image size     =
| caption        =
| director       = Robert Lee King Raoul OConnell
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1995
| runtime        = 91 min.
| country        = U.S.A. English
| budget         =
| preceded by    =
| followed by    =
}}
Boys Life is a compilation of three 30-minute short subject films about being gay in America.

A high school student in "Pool Days" finds excitement in his health club job. In "A Friend of Dorothy," a college freshman hopes his handsome roommate is also gay. And in "The Disco Years," Tom is crushed when the tennis player with whom he had a fling leaves him for a girl and joins an openly homophobic crowd.

==See also==
* List of American films of 1995
* Boys Life 2
* Boys Life 3
*  
* Boys Life 5
* Boys Life 6

==External links==
* 

 
 
 
 
 
 
 

 
 