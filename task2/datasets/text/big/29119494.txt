Shevri
{{Infobox film
| name           = Shevri
| image          = Shevri.jpg
| caption        = 
| director       = Gajendra Ahire
| producer       = Neena Kulkarni
| writer         = Gajendra Ahire
| starring       = Neena Kulkarni    Mohan Agashe   Mukta Barve   Dilip Prabhavalkar
| music          = 
| cinematography = Imtiaz Bargir
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}} Marathi film made in 2006. The film is directed by Gajendra Ahire and produced by Neena Kulkarni.   

==Plot==
Vidya Barve, is a divorced, working woman,sharing a rented flat with a room mate in Mumbai. Our film deals with one single night which Vidya is forced to spend on the streets of this city. As she ambles on the deserted roads waiting for morning, shes frightened, lonely, angry, confused. She keeps recalling her relationships as they stand today - Her estranged husband, her teenaged son staying with her mother in small town Nasik, her boss, her colleague, her room mate. Alternating between her encounters during this night and her past, the film finally sees a visibly confident Vidya coming to terms with her lot. And that is when dawn breaks... The title Shevri suggests an insignificant wisp of cotton, which is Vidya. An ordinary woman with ordinary dreams, leading a life which is now out of the ordinary for her. Her coming to terms with her state today and becoming present to her life is what gives an essence to the film. 

“Shevri is an interesting film because of its innovative and intelligent cinematic narration by effective use of flashbacks” - Jury comment at the Pune International Film Festival 2007 where it won the best International Marathi Film Award. 

==Cast==
Following table shows details of Cast for Shevri.
{| class="wikitable"
|- 
! Actor 
! Role 
|- 
| Neena Kulkarni
| Vidya Barve
|-
| Ravindra Mankani
| Neena Kulkarnis Husband
|-
| Mita Vashisht
| Maya 
|-
| Mukta Barve
| Upset girl on the street 
|-
| Mohan Agashe
| Neena Kulkarnis Boss
|-
| Dilip Prabhavalkar
| Neena Kulkarnis office colleague
|-  
| Shivaji Satam
|
|-
| Uttara Baokar
|  
|-
| Saksham Kulkarni 
| 
|}

==Awards==
* 2007 - National Film Award for Best Feature Film in Marathi

==References==
 

==External links==
*  

 
 

 
 
 
 


 