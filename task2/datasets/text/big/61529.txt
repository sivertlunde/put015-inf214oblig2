Lost Horizon (1937 film)
{{Infobox film
| name           = Lost Horizon
| image          = 1937LostHorizonPoster.jpg
| caption        = Original poster
| director       = Frank Capra
| producer       = Frank Capra
| screenplay     = Robert Riskin
| based on       =   John Howard H.B. Warner
| music          = Dimitri Tiomkin Joseph Walker Elmer Dyer
| editing        = Gene Havlick Gene Milford
| distributor    = Columbia Pictures
| released       =  
| runtime        = 132 minutes 210 minutes (original cut)
| country        = United States
| language       = English
| budget         = $2 million 
}}
 1933 Lost novel of James Hilton.

The film exceeded its original budget by more than $776,000, and it took five years for it to earn back its cost. The serious financial crisis it created for Columbia Pictures damaged the partnership between Capra and studio head Harry Cohn, as well as the friendship between Capra and screenwriter Riskin, whose previous collaborations had included Lady for a Day, It Happened One Night, and Mr. Deeds Goes to Town. 

==Plot== Foreign Secretary, writer, soldier and diplomat Robert Conway (Ronald Colman) has one last task in 1935 China: to rescue 90 Westerners in the city of Baskul. He flies out with the last few evacuees, just ahead of armed revolutionaries.
 Sam Jaffe).
 paleontologist Alexander Thomas Mitchell) John Howard), and Maria (Margo (actress)|Margo), another beautiful young woman they find there, are determined to leave.

Conway eventually has an audience with the High Lama and learns that his arrival was no accident. The founder of Shangri-La is said to be hundreds of years old, preserved, like the other residents, by the magical properties of the paradise he has created, but is finally dying and needs someone wise and knowledgeable in the ways of the modern world to keep it safe. Having read Conways writings, Sondra believed he was the one; the Lama had agreed with her and arranged for Conways abduction. The old man names Conway as his successor and then peacefully passes away.

George refuses to believe the Lamas fantastic story and is supported by Maria. Uncertain and torn between love and loyalty, Conway reluctantly gives in to his brother and they leave, taking Maria with them, despite being warned that she is much older than she appears. After several days of grueling travel, she becomes exhausted and falls face down in the snow. When they turn her over, they discover that she had become extremely old and died. Her departure from Shangri-La had restored Maria to her true age. Horrified, George loses his sanity and jumps to his death.

Conway continues on and eventually meets up with a search party sent to find him, although the ordeal has caused him to lose his memory of Shangri-La. On the voyage back to England, he remembers everything; he tells his story and then jumps ship. The searchers track him back to the Himalayas, but are unable to follow him any further. Conway manages to return to Shangri-La.

==Comparison to the novel== Manchu princess Lo-Tsen is the basis of the Maria character. There is no Sondra in the novel, though Conway does feel a languid attraction to Lo-Tsen. Mallinson, Conways younger, discontented vice-consul rather than his brother, persuades Conway to leave with him and Lo-Tsen. Mallinsons fate is not revealed, and it is implied that Lo-Tsen brings a sick Conway to a hospital before dying of old age. There, he is found, not as a result of a massive search, but simply by chance by an acquaintance.

==Cast==
* Ronald Colman as Robert Conway
* Jane Wyatt as Sondra Bizet
* H.B. Warner as Chang Sam Jaffe as High Lama John Howard as George Conway
* Edward Everett Horton as Alexander P. Lovett Thomas Mitchell as Henry Barnard Margo as Maria
* Isabel Jewell as Gloria Stone David Clyde as Club Steward
* David Torrence as Prime minister
* Hugh Buckler as Lord Gainsford
* Val Duran as Talu
* Milton Owen as Fenner
* Richard Loo as Shanghai airport official
* Willie Fung as Bandit leader
* Victor Wong as Bandit leader

==Production==
Frank Capra had read the James Hilton novel while filming It Happened One Night, and he intended to make Lost Horizon his next project. When Ronald Colman, his first and only choice for the role of Robert Conway, proved to be unavailable, Capra decided to wait and made Mr. Deeds Goes to Town instead. 
 documentary about the Himalayas, was in black and white, he was forced to change his plans.  In 1985, Capra, Sr. claimed the decision to film in black and white was made because Technicolor#Three-strip Technicolor|three-strip Technicolor was new and fairly expensive, and the studio was unwilling to increase the films budget so he could utilize it. 

  Lucerne Valley, Ojai Valley, Sierra Nevada Westlake Village, adding the cost of transporting cast, crew, and equipment to the swelling budget. McBride 1992, p. 353. 
 Sam Jaffe performing the High Lamas monologues, then reshot the scenes twice, once with Walter Connolly because it was felt Jaffes makeup was unconvincing and he looked too young for the role. A total of 40 minutes of footage featuring the High Lama eventually was trimmed to the 12 that appeared in the final cut. Filming took one hundred days, 34 more than scheduled. The films final cost, including prints and promotional advertising, was $2,626,620, and it remained in the red until it was reissued in 1942. 
 Santa Barbara Lake Arrowhead, and remained in seclusion there for several days. He later claimed he burned the first two reels of the film, an account disputed by Milford, who noted setting the nitrate film on fire would have created a devastating explosion. McBride 1992, p. 362. 
 roadshow attraction, with only two presentations per day and tickets sold on a reserved-seat basis. Because the box office returns were so low, the studio head deleted an additional 14 minutes before the film went into general release the following September. Due primarily to the cuts made without his approval, Capra later filed a lawsuit against Columbia, citing "contractual disagreements," among them, the studios refusal to pay him a $100,000 semi-annual salary payment due him. A settlement was reached on November 27, 1937, with Capra collecting his money and being relieved of the obligation of making one of the five films required by his contract. In 1985, the director claimed Cohn, whom he described as the "Jewish producer," trimmed the film simply so theaters could have more daily showings and increase the films chance of turning a profit. 

==Reception==
Frank S. Nugent of The New York Times called it, "a grand adventure film, magnificently staged, beautifully photographed, and capitally played." He continued,

  here is no denying the opulence of the production, the impressiveness of the sets, the richness of the costuming, the satisfying attention to large and small detail which makes Hollywood at its best such a generous entertainer. We can deride the screen in its lesser moods, but when the West Coast impresarios decide to shoot the works the resulting pyrotechnics bathe us in a warm and cheerful glow." In conclusion, he observed, "The penultimate scenes are as vivid, swift, and brilliantly achieved as the first. Only the conclusion itself is somehow disappointing. But perhaps that is inescapable, for there can be no truly satisfying end to any fantasy... Mr. Capra was guilty of a few directorial clichés, but otherwise it was a perfect job. Unquestionably the picture has the best photography and sets of the year. By all means it is worth seeing.  

Nugent later named it one of the 10 best films of the year. 

The Hollywood Reporter called it "an artistic tour de force ... in all ways, a triumph for Frank Capra." McBride 1992, p. 366. 

Less enthusiastic was Otis Ferguson, who in his review for National Board of Review Magazine observed, "This film was made with obvious care and expense, but it will be notable in the future only as the first wrong step in a career that till now has been a denial of the very tendencies in pictures which this film represents."  Joseph McBride in a later biography notes that Capras emphasis on theme rather than people was evident in the film; he also considered the film a financial "debacle."  

==Awards and nominations==
Stephen Goossons elaborate sets won him the Academy Award for Best Art Direction, and Gene Havlick and Gene Milford shared the Academy Award for Best Film Editing.   oscars.org. Retrieved: August 9, 2011. 
 The Hurricane. Charles C. Coleman, nominated for the Academy Award for Best Assistant Director, lost to Robert Webb for In Old Chicago. This was the last year an Oscar was awarded in this category. 

;American Film Institute (AFI) Lists
* AFIs 100 Years...100 Movies – Nominated   afi.com. Retrieved: June 30, 2011. 
* AFIs 100 Years of Film Scores – Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition) – Nominated 
* AFIs 10 Top 10 – Nominated Fantasy Film 

==Later releases and remakes==
In 1942, the film was re-released as The Lost Horizon of Shangri-La. A lengthy drunken speech delivered by Robert Conway, in which he cynically mocks war and diplomacy, had already been deleted in the general release version. Capra felt the film made no sense without the scene,  and in later years film critic Leslie Halliwell described the missing 12 minutes as "vital".  They were restored years later.

In 1952, a 92-minute version of the film was released. It aimed to downplay features of the utopia that suggested Communist ideals, a sensitive point after a Civil War in China resulted in the ascension of Mao Zedongs Communist Party in that country in 1949.
 AFI initiated missing film footage were replaced with a combination of publicity photos of the actors in costume taken during filming and still frames depicting the missing scenes. 
 remake of Mary Poppins, My Fair The Sound of Music.

==Adaptations to other media==
Lost Horizon was adapted as a radio play starring Ronald Colman and Donald Crisp for the September 15, 1941 broadcast of Lux Radio Theater. Colman reprised his role again for the November 27, 1946 broadcast of Academy Award Theater and the July 24, 1948 broadcast of Favorite Story.

In 1946, Ronald Colman also made a three-record, 78 RPM album based on the film for American Decca Records. The score for the album was by Victor Young. 

Another radio adaptation starring Herbert Marshall was broadcast on December 30, 1948 on Hallmark Playhouse.
 stage musical Broadway in 1956, but closed after only 21 performances.  It was staged for a 1960 Hallmark Hall of Fame television broadcast.

Author Harlan Ellison alludes to the film in a 1995 television commentary for the program Sci-Fi Buzz, wherein he laments what he perceives as a prevailing cultural illteracy. 
 Megan in California. 

==Digital restoration==  digital restoration of the film was done by Sony Colorworks, The digital pictures were frame by frame digitally restored at Prasad Corporation to remove dirt, tears, scratches and other artifacts. The film was restored to its original look.  

==DVD release== Columbia TriStar Home Video released the restored version of the film on Region 1 DVD on August 31, 1999. It has an English audio track and subtitles in English, Spanish, Portuguese, Georgian, Chinese and Thai. Bonus features include three deleted scenes, an alternate ending, a commentary about the restoration by Charles Champlin and Robert Gitt, and a photo documentary with narration by film historian Kendall Miller.

A Region 2 DVD including the same bonus features (plus the original theatrical trailer) was released on February 26, 2001. It has audio tracks in English, French, German, Italian and Spanish and subtitles in English, Spanish, German, French, Italian, Hindi, Portuguese, Turkish, Danish, Icelandic, Bulgarian, Swedish, Hungarian, Polish, Dutch, Arabic, Finnish, Czech and Greek.

==See also==
* List of incomplete or partially lost films

==References==

===Notes===
 

===Bibliography===
 
* Capra, Frank. Frank Capra, The Name Above the Title: An Autobiography. New York: The Macmillan Company, 1971. ISBN 0-306-80771-8.
* Halliwell, Leslie. Halliwells Hundred: A Nostalgic Choice of Films from the Golden Age . New York: Charles Scribners Sons, 1982. ISBN 0-684-17447-2.
* Joseph McBride (writer)|McBride, Joseph. Frank Capra: The Catastrophe of Success. New York: Touchstone Books, 1992. ISBN 0-671-79788-3.
* Michael, Paul, ed. The Great Movie Book: A Comprehensive Illustrated Reference Guide to the Best-loved Films of the Sound Era. Englewood Cliffs, New Jersey: Prentice-Hall Inc., 1980. ISBN 0-13-363663-1.
* Poague, Leland. The Cinema of Frank Capra: An Approach to Film Comedy. London: A.S Barnes and Company Ltd., 1975. ISBN 0-498-01506-8.
* Scherle, Victor and William Levy. The Films of Frank Capra. Secaucus, New Jersey: The Citadel Press, 1977. ISBN 0-8065-0430-7.
* Wiley, Mason and Damien Bona. Inside Oscar: The Unofficial History of the Academy Awards. New York: Ballantine Books, 1987. ISBN 0-345-34453-7.
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
*   at Virtual History
Streaming audio
*   on Lux Radio Theater: September 15, 1941
*   on Theater of Romance: February 6, 1945 Academy Award Theater: November 27, 1946
*   on Favorite Story: July 24, 1948
*   on Hallmark Playhouse: December 30, 1948
*   on Theater of Romance: June 5, 1954

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 