A Place in the Sun (film)
{{Infobox film
| name           = A Place in the Sun
| image          = A Place in the Sun (film) poster.gif
| caption        = original film poster
| director       = George Stevens
| producer       = George Stevens Michael Wilson Harry Brown
| based on       =    
| starring       = Montgomery Clift Elizabeth Taylor Shelley Winters
| music          = Franz Waxman
| cinematography = William C. Mellor
| editing        = William Hornbeck
| distributor    = Paramount Pictures
| released       =  
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $2,295,304
| gross          = $7,000,000
}}
 An American Tragedy, in 1931.
 Harry Brown Michael Wilson, and stars Montgomery Clift, Elizabeth Taylor, and Shelley Winters; its supporting actors included Anne Revere, and Raymond Burr.  

The film was a critical and commercial success, winning six Academy Awards and the first ever Golden Globe Award for Best Motion Picture – Drama. In 1991, A Place in the Sun was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot summary==
George Eastman (Montgomery Clift), the poor nephew of rich industrialist Charles Eastman (Herbert Heyes), arrives in town following a chance encounter with his uncle while working as a bellhop in a Chicago hotel. The elder Eastman invites George to visit him if and when he ever comes to town, and the ambitious young man takes advantage of the offer. Despite Georges family relationship to the Eastmans, they regard him as something of an outsider, but his uncle nevertheless offers him an entry-level job at his factory. George, uncomplaining, hopes to impress his uncle (whom he addresses as "Mr. Eastman") with his hard work and earn his way up. While working in the factory, George starts dating fellow factory worker Alice Tripp (Shelley Winters), in defiance of the workplace rules. Alice is a poor and inexperienced girl who is dazzled by George and slow to believe that his Eastman name brings him no advantages.

Over time, George begins a slow move up the corporate ladder, into a supervisory position in the department where he began. He has submitted recommendations on improving production in his department, which finally catch the attention of his uncle, who invites him to their home for a social event. At the party, George finally meets "society girl" Angela Vickers (Elizabeth Taylor), whom he has admired from afar since shortly after arriving in town, and they quickly fall in love. Being Angelas escort thrusts George into the intoxicating and carefree lifestyle of high society that his rich Eastman kin had denied him. When Alice announces that she is pregnant and makes it clear that she expects George to marry her, he puts her off, spending more and more of his time with Angela and his new well-heeled friends. An attempt to procure an abortion for Alice fails, and she renews her insistence on marriage. George is invited to join Angela at the Vickerss holiday lake house over Labor Day weekend, and excuses himself to Alice, saying that the visit will advance his career and accrue to the benefit of the coming child.
 
George and Angela spend time at secluded Loon Lake, and after hearing a story of a couples supposed drowning there, with the mans body never being found, George hatches a plan to rid himself of Alice so that he can marry Angela.

Meanwhile, Alice finds a picture in the newspaper of George, Angela, and their friends, and realizes that George lied to her about his intentions for wanting to go to the lake. During a dinner which is attended by the Eastman and Vickers families, George appears to be on the verge of finally advancing into the business and social realm that he has long sought. However, Alice phones the house during the dinner party and asks to speak with George; she tells him that she is at the bus station, and that if he doesnt come to get her, shell come to where he is and expose him. Visibly shaken, he contrives an excuse to the families that he must suddenly leave, but promises Angela he will return. The next morning, George and Alice drive to City Hall to get married but they find it closed for Labor Day, and George suggests spending the day at the nearby lake; Alice unsuspectingly agrees.

When they get to the lake, George acts visibly nervous when he rents a boat from a man who seems to deduce that George gave him a false name; the mans suspicions are aroused more when George asks him whether any other boaters are on the lake (none are). While they are out on the lake, Alice confesses her dreams about their happy future together with their child. As George apparently takes pity on her and, judging from his attitude, decides not to carry out his murderous plan, Alice tries to stand up in the boat, causing it to capsize, and Alice drowns.

George escapes, swims to shore, and eventually drives back up to the Vickerss lodge, where he tries to relax but is increasingly tense. He says nothing to anyone about having been on the lake or about what happened there. Meanwhile, Alices body is discovered and her death is treated as a murder investigation almost from the first moment, while an abundant amount of evidence and witness reports stack up against George. Just as Angelas father approves Angelas marriage to him, George is arrested and charged with Alices murder. Though the audience knows that the planned murder in fact turned into an accidental drowning, Georges furtive actions before and after Alices death condemn him. His denials are futile, and he is found guilty of murder and sentenced to death in the electric chair. Near the end, he confesses to the priest in his cell that, although he did not kill Alice, he didnt act as to save her, during the few dramatic moments when he could have, because he was thinking of Angela. Then the priest states that, in his heart, it was murder. The film ends with Angela visiting George in prison, saying that she will always love him, and with him slowly marching towards his execution.

==Cast==
 
 
* Montgomery Clift as George Eastman
* Elizabeth Taylor as Angela Vickers
* Shelley Winters as Alice Tripp
* Anne Revere as Hannah Eastman
* Keefe Brasselle as Earl Eastman
* Fred Clark as Bellows, defense attorney
* Raymond Burr as Dist. Atty. R. Frank Marlowe
* Herbert Heyes as Charles Eastman
 
* Shepperd Strudwick as Anthony Tony Vickers
* Frieda Inescort as Mrs. Ann Vickers
* Kathryn Givney as Louise Eastman
* Walter Sande as Art Jansen, Georges Attorney
* Ted de Corsia as Judge R.S. Oldendorff
* John Ridgely as Coroner
* Lois Chartrand as Marsha
* Paul Frees as Rev. Morrison
 
All primary cast members are deceased.

==Reception==
The film earned an estimated $3.5 million at the US and Canadian box office and earned critical acclaim in 1951.  

Upon seeing the film, Charlie Chaplin called it the greatest movie ever made about America. 

The films acclaim did not completely hold up over time.  Reappraisals of the film find that much of what was exciting about the film in 1951 is not as potent in the 21st century.  Critics cite the soporific pace, the exaggerated melodrama, and the outdated social commentary as qualities present in A Place in the Sun that are not present in the great films of the era, such as those by Alfred Hitchcock.  The performances however by Clift, Taylor and Winters continue to receive praise.    

Many still consider the film to be a classic too; it was featured on the AFIs 100 Years…100 Movies list of 1998, and the AFI 100 Years…100 Passions list of 2002, while the film holds a strong 75% rating on Rotten Tomatoes,  and in 2013 the British Film Institute rereleased the picture across the United Kingdom due to its significant merit. 

==Awards and nominations==
 
 

===Academy Awards===
;Wins: Best Cinematography, Black-and-White (William C. Mellor) Best Costume Design, Black-and-White (Edith Head) Best Director (George Stevens) Best Film Editing (William Hornbeck) Best Original Score (Franz Waxman) Best Writing, Screenplay (Michael Wilson and Harry Brown)

;Nominations: Best Actor in a Leading Role (Montgomery Clift) Best Actress in a Leading Role (Shelley Winters)  Best Picture
 

===Other honors===
;American Film Institute recognition
* AFIs 100 Years...100 Movies - #92
* AFIs 100 Years...100 Passions - #53
* AFIs 100 Years...100 Movie Quotes:
** "I love you. Ive loved you since the first moment I saw you. I guess maybe I’ve even loved you before I saw you." - Nominated 
* AFIs 100 Years of Film Scores - Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 

;Golden Globe Award
* Best Picture (Drama)

;Directors Guild of America Award
* Best Director
 Cannes Film Festival
* In competition (1951)   
 

==References==
 

==External links==
 
*  
*   at Rotten Tomatoes
*  
*   at TV Guide
*   at Filmsite.org

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 