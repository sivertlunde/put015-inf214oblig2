The Union (2011 film)
{{Infobox Film
| name        = The Union
| image       = The Union (2011 film).jpg
| director    = Cameron Crowe
| writer      = Cameron Crowe
| producer    = Cameron Crowe
| starring    =  
| distributor = 
| released    =  
| runtime = 
| language    = English
| country = United States
| budget      = 
}} The Union.

==Synopsis==
In November 2009, filmmaker Cameron Crowe began filming a behind-the-scenes look at the creation of the album The Union, a collaboration between musicians Elton John and Leon Russell, who hadnt spoken to one another in 38 years prior to beginning work on the album. In addition, the documentary offers a rare glimpse into the process John goes through to create and compose his music.
 Robert Randolph, Don Was and a 10-piece gospel choir who all contribute to the album, which is produced by award-winning producer T-Bone Burnett.  Musician Stevie Nicks and Johns long-time lyricist Bernie Taupin also appear. 

On March 2, 2011, the documentary was announced to open the 2011 Tribeca Film Festival. 

In June 2011, HBO announced it had acquired US TV rights and would begin airing the documentary in January 2012. 

== References ==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 

 