Mister Big (1943 film)
Mister Big is a 1943 musical directed by Charles Lamont, starring Donald OConnor, Gloria Jean and Peggy Ryan. The film features the song "Rude, Crude, and Unattractive".

This was the final of seven, successive B movie musicals that Universal Pictures cast with Donald O’Connor after he signed in 1941. When his popularity soared prior to release, the studio added $50,000 in musical numbers and distributed it as an A.   His subsequent films were all As.  

== Plot ==

In a school for the dramatic arts called the Davis School of the Theatre, run by Jeremy Taswell, Donald (Donald OConnor) has written a musical comedy for the school play.  But the owner of the school, Mrs. Davis (Florence Bates)(whose niece, played by Gloria Jean, secretly has a crush on Donald), disapproves and insists that they do a play by Sophocles instead. The students manage to change the play into a musical while Mrs. Davis is traveling to New York. Their triumph is short lived, however, for Mrs. Davis comes back from New York on opening night. At first she becomes quite angry at the students, but changes her tune when a Broadway producer agrees to finance the show and produce it in New York.

==Cast==
*Gloria Jean- Patricia
*Donald OConnor- Donald
*Peggy Ryan- Peggy
*Robert Paige- Johnny Hanley
*Elyse Knox- Alice Taswell
*Samuel S. Hinds- Jeremy Taswell
*Florence Bates- Mrs. Mary Davis
*Mary Eleanor Donahue- Muggsy

==Songs==
*Serenade (Joy) performed by Gloria Jean
*Soliloquy performed by Donald OConnor and chorus
*This Must Be a Dream performed by Ray Eberle
*Niagara performed by the Jivin Jacks and Jills, featuring Donald OConnor and Peggy Ryan
*Moonlight and Roses performed by Gloria Jean
*All the Things I Wanta Say performed by Donald OConnor and Peggy Ryan
*Kitten with My Mittens Laced performed by the Jivin Jacks and Jills
*Rude, Crude, and Unattractive performed by Donald OConnor, Peggy Ryan, and the Jivin Jacks and Jills
*Were Not Obvious performed by Donald OConnor and Gloria Jean
*Thee and Me performed by Donald OConnor, Peggy Ryan, and Gloria Jean
*Come Along, My Mandy performed by Donald OConnor and Peggy Ryan, sung by the Ben Carter Choir
*Well Meet Again performed by the Ben Carter Choir
*Egyptian Boogie performed by the Jivin Jacks and Jills
*Hi, Character performed by Donald OConnor and Peggy Ryan
*The Spirit Is in Me sung by Gloria Jean, performed by the Jivin Jacks and Jills, featuring Peggy Ryan

== External links ==
*  
*  

 

 
 
 
 
 
 

 