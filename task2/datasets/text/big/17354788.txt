Between the Lines (2008 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name = Between the Lines: The True Story of Surfers and the Vietnam War
| image =
| alt =  
| caption =
| director = Scott Bass Ty Ponder
| producer = Ty Ponder
| writer = Scott Bass John Milius
| starring = Jerry Anderson Pat Farley Howard Fisher Herbie Fletcher
| music = Terry Palmer
| cinematography = Troy Page
| editing = Troy Page
| studio =
| distributor =
| released =  
| runtime =
| country = United States
| language = English
| budget =
| gross =
}}

Between the Lines is a 2008 documentary film that examines the Vietnam War and its effects on the surf culture.

==Synopsis==
Pat Farley, a heavy combat vet, volunteered for service in Vietnam and was discharged with a full psychiatric pension. Conversely, Brant Page evaded the draft and fled to the Hawaiian Islands where he was pursued and eventually arrested by the FBI. While following the lives of these two surfers the film chronicles the impact of the Vietnam War on the surfing lifestyle.

==Production==
The film utilizes archival footage, still images, home movies and interviews. This documentary film includes archival images and film including The China Beach Surf Club, Surfing in Vietnam, combat footage and surfers around the world during the Vietnam War era.

Between the lines documents and explores some of the concepts depicted in the fictional films Apocalypse Now and Big Wednesday. All of the men interviewed are surfers who either entered the military and went to Vietnam or evaded the draft.

The surfers interviewed include the primary characters; combat veteran Pat Farley and draft evader Brant Page, as well as Jerry Anderson,   Jim Cowell (KIA 05-31-68), Howard Fisher, Herbie Fletcher, Drew Kampion, Tom Luker, Mark Martinson, Greg Murray,Greg Samp, Ron Sizemore, and Rick Thomas. Some of the music was written by Hot Water Music co-lead man, Chuck Ragan.
The film is narrated by John Milius.

==External links==
* 
* 

 
 
 
 
 
 


 