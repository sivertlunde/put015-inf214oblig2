Struck by Lightning (2012 film)
 
{{Infobox film
| name = Struck by Lightning
| image = Struck_By_Lightning_-_Theatrical_Poster.jpeg
| image_size = 215px
| alt = 
| caption = Official one-sheet
| director = Brian Dannelly Rob Aguire Chris Colfer
| writer = Chris Colfer
| starring = Chris Colfer Allison Janney Christina Hendricks Sarah Hyland Carter Jenkins Brad William Henke Rebel Wilson Angela Kinsey Polly Bergen Dermot Mulroney
| music = Jake Monaco
| cinematography = Bobby Bukowski
| editing = Tia Nolan Permut Presentations
| distributor = Tribeca Film
| released =  
| runtime = 90 minutes 
| country = United States
| language = English
| budget = $1,300,000   
| gross = $28,378   
}}
Struck by Lightning is a 2012 coming-of-age comedy-drama film written by and starring Chris Colfer, also based on his novel, and directed by Brian Dannelly. After high school senior Carson Phillips is struck by lightning and killed in his high-school parking lot, he recounts the way he blackmailed his classmates into contributing to his literary magazine.
 world premiere on Demand and was released theatrically on January 11, 2013. The film was released on DVD and Blu-ray Disc|Blu-ray on May 21, 2013.   

==Plot==
High school senior Carson Phillips (Chris Colfer) is randomly struck and killed by lightning and the rest of the movie is a flashback of his life. He desires to go to Northwestern University in order to one day become the youngest editor for The New Yorker, but so far is having limited success in making those dreams happen.

One day during a writers club meeting, Malerie Baggs (Rebel Wilson) comes in to get advice about her short stories. Carson tells her not to give up, and says that she cant find the ideas, the ideas have to find her. He later goes to a student council meeting run by head cheerleader Claire Matthews (Sarah Hyland). Claire, as well as everyone in student council, resort to ignoring him due to his constant objections to their ideas. While Sheryl is picking up several prescriptions, she makes idle small talk with the cheerful new pharmacist April (Christina Hendricks), who is six months pregnant. Carson also visits his grandmother (Polly Bergen), the first person who encouraged his writing, but she doesnt know who he is due to Alzheimers disease. She tells him her grandson used to be so happy and full of life, but was now his own personal rain cloud.
 Graham Rogers) making out in a bathroom stall. Nicholas, horrified, begs Carson not to tell anyone since his rich family would disapprove of his homosexuality. Carson agrees on the condition that they will contribute to the paper until they both graduate. April and Neal visit Neals lawyer to straighten out some legal issues. April, who had no knowledge about Carson or Sheryl, storms out when she learns Neal is still legally married, and has a child she never knew about. Carsons guidance counselor (Angela Kinsey) informs him that one way to improve his chances of getting into Northwestern is to submit a literary magazine in order to show he can inspire others to write. He gets permission to start the magazine from his conservative principal (Brad William Henke). He announces at a school assembly that all entries will be taken, but later finds the submission box filled with nothing but trash. Carson tells Malerie the real reason behind their participation in the paper, and she reveals that she caught Claire having sex with Coach Walker (Charlie Finn), who is also the older brother of her boyfriend, Justin (Robbie Amell). Malerie goes on to say how everyone in their school probably has an embarrassing secret that they wouldnt want to get out.

Sheryl has another run-in with April when April recognizes Carsons name on the anti-depressant prescription. She then comes home to Neal and demands to meet his son. During the homecoming parade, Carson is forced to pull the writers club float himself after the cheerleaders take the car they were assigned. Feeling more humiliated than ever, Carson decides to blackmail several of his peers into writing for the literary magazine. He tricks yearbook president Remy Baker (Allie Grant) into sending him a dirty picture that could ruin her reputation. Together, he and Malerie blackmail fellow school paper members Dwayne (Matt Prokop) for bringing marijuana to school, and goth girl Vicki (Ashley Rickards) for taking BDSM-style pictures that her church-going parents would disapprove of. Carson additionally discovers that supposed foreign exchange student Emilio (Roberto Aguire) is from San Diego, knows only rudimentary Spanish, and is using his faux-exotic charm to seduce women.
 extorts agreements that everyone must offer something to put in the magazine if they want their secrets to stay quiet. Carson also tells Claire and Coach Walker that they must have each cheerleader and football player submit something as well, as a way to make the issue more popular among the student body.

After school, Carson gets an unexpected call from his father. Neal tells him about April, the baby, and how he wants them to get together soon. When he has dinner with Neal and April, Carson realizes that his father is trying to make himself sound like he has been more present in Carsons life, which quickly results into an argument and Carson storming out. Later, during a student council meeting with the principal, Carson opposes a ban on clothing logos in school. As punishment, the principal revokes all off-campus privileges for students.

During another encounter a the pharmacy, Sheryl explains to April that she gave her husband everything, and was therefore left with nothing when he decided she wasnt enough for him and that just like April, she had a kid to keep him around, but that didnt change anything. Meanwhile, Carson completes the literary magazine, but it flops due to the backlash from the student body. Carson also learns that he was accepted into Northwestern, but since he never confirmed his admission, he must wait to reapply and go to community college in the meantime. Carson assumes his letter was lost in the mail, but after telling his mother, Sheryl admits she threw away his acceptance letter to protect him from her perceived "reality" that his dreams will probably never come true.

While Carson and Malerie pack up the unread literary magazines, Carson asks her why she likes to film everything. Malerie replies that "It doesnt matter if youre stuck in the past or if youre trying to forget the past, what matters is what you do in the present and I try to soak it up." As Malerie is leaving, she asks Carson if he thinks theyre friends. He says theyre best friends. Carson suddenly decides that theres one story left to write; his own. His voice-over explains how he finally realized that, despite everything he went through, he successfully got the other students to write for the literary magazine, so for the first time in his life, he was truly happy. He is then shown going outside as the thunder starts.

It takes over three days for people to find Carsons body. Many people, including the principal and the students who hated him, come to the funeral. Its shown that Carson did leave an impression on everyone he knew. Malerie takes over as president of the writers club, while Sheryl visits her mother in assisted care.

==Cast==
* Chris Colfer as Carson Phillips,  , Celebuzz, July 19, 2011, accessed August 12, 2011.  the head of the schools writing club. He is disliked, belittled and insulted by his peers, mostly for his sarcastic and dry personality that he gained after his parents divorce.
* Allison Janney as Sheryl Phillips,  Carsons alcoholic and pill-popping single mother.
* Christina Hendricks as April,  Carsons fathers pregnant fiancée.
* Sarah Hyland as Claire Mathews,  , About.com, July 18, 2011, accessed August 12, 2011.  the head cheerleader and class president.
* Carter Jenkins as Nicholas Forbes,  a wealthy, closeted gay student.
* Brad William Henke as Principal Gifford,  the school principal.
* Rebel Wilson as Malerie Baggs,  Carsons best friend.
* Angela Kinsey as Ms. Sharpton,  the guidance counselor.
* Polly Bergen as Grandma,  Carsons grandmother whos suffering from Alzheimers disease.
* Dermot Mulroney as Neal Phillips,  Carsons absent father.
* Allie Grant as Remy Baker,  , SlashFilm, July 18, 2011, accessed August 12, 2011.  the editor of the school yearbook.
* Ashley Rickards as Vicki Jordan,  an apathetic goth girl.
* Robbie Amell as Justin Walker,  the captain of the football team.
* Charlie Finn as Coach Colin Walker, the football coach and Justin’s brother.
* Roberto Aguire as Emilio Lopez    
* Matt Prokop as Dwayne Michaels,  , VictoriaAdvocate, July 20, 2011, accessed August 12, 2011.   Graham Rogers as Scott Thomas, the drama club president. 
* Adam Kolkin as Young Carson Phillips.
* Taylor Clarke-Pepper as Dog Walker (uncredited)
* Michael Van London as Caretaker (uncredited) He also wrote and sang three songs in the film: "Sound of an Amp", "Downtown" and "Feel Love" © Michael Van London Recordings ASCAP 

==Reception==
Struck by Lightning has received mixed reception, holding a Metacritic score of 41, and a Rotten Tomatoes score of 30%. The most common complaint from reviewers was the fact that Colfers character came off as too arrogant and cynical, and didnt manage to be likeable.       Many reviews agreed that the dialogue was witty and funny and that the cast gave strong performances. Many critics also praised Colfers debut as a screenwriter.    
 Seize the day, kids. There are some real stresses in the fellows life, many caused by his mess of a drunken single mom (an affecting Allison Janney)." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 