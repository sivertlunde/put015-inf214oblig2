Stalingrad: Dogs, Do You Want to Live Forever?
{{Infobox film
| name = Stalingrad: Dogs, Do You Want to Live Forever?
| image =
| alt =
| caption =
| director = Frank Wisbar
| producer = Alf Teichs
| writers = Fritz Wöss
| screenplay = Frank Wisbar, Frank Dimen, Heinz Schröter
| narrator = Joachim Hansen
| music = Herbert Windt
| cinematography = Helmut Ashley
| editing = Martha Dübber
| studio = Deutsche Film Hansa
| distributor =
| released = 7 April 1959
| runtime = 97 minutes
| country = Germany
| language = German
| budget =
| gross =
}}
Dogs, Do You Want to Live Forever (  film, directed by Frank Wisbar and based on the eponymous novel by Fritz Wöss. The movie revolves around the Battle of Stalingrad. The title is vaguely drawn from Friedrich the Greats words when he saw his soldiers fleeing at Battle of Kolín|Kolin: "You cursed rascals, do you want to live forever? "

==Plot summary==
The young Wehrmacht Lieutenant Wisse is sent to serve as a liaison officer with the Romanian Army at Stalingrad. His new commander, Major Linkmann, is a rigid officer who looks down on their allies, contrary to Wisse. Soon after his arrival, the Red Army encircles the Germans, and they retreat into the city of Stalingrad. During the flight, Linkmann tries to abandon them. Wisse convinces a sergeant to ignore the orders obliging him to burn the supply depot, thus procuring provisions for the soldiers. In Stalingrad, he is again put under Linkmanns command. The tensions between them soar while their soldiers become desperate with hunger, as the 6th Army is ordered to hold on. Wisse is nearly captured by the Soviets, but a Russian woman whom he once helped leads him back to the German lines. Eventually, Linkmann tries to surrender by himself, but is shot dead by two of his own men. When the 6th Army commander, Field Marshal Friedrich Paulus, orders the remaining German troops in Stalingrad to surrender, Wisse and his soldiers are sent to a POW camp.

==Select cast== Joachim Hansen as Wisse Wilhelm Borchert as Friedrich Paulus
*Wolfgang Preiss as Linkmann Carl Lange General von Seydlitz Karl John as Hermann Hoth
*Sonja Ziemann as Katia

==References==
 

==External links==
*  on the IMDb.

 
 
 
 
 
 
 


 