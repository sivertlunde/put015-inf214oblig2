Corn Island (film)
 
{{Infobox film
| name           = Corn Island
| image          = Corn Island film poster.jpg
| caption        = Film poster
| director       = Giorgi Ovashvili
| producer       = Guillaume de Seille, Nino Devdariani, Sain Gabdullin, Eike Goreczka, Christoph Kukula	, George Ovashvili, Karla Stojáková
| writers         = Giorgi Ovashvili, Roelof Jan Minneboo, Nugzar Shataidze
| starring       = İlyas Salman
| music          = Iosif Bardanashvili	
| cinematography = Elemér Ragályi
| editing        = Sun-min Kim
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Georgia
| language       = Georgian
| budget         = 
}} Best Foreign Language Film at the 87th Academy Awards,    making the January Shortlist.    The film won the Crystal Globe prize at the Karlovy Vary International Film Festival.   

==Cast==
* İlyas Salman as Old Man
* Mariam Buturishvili as Girl

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Georgian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 