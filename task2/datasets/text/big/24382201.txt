Life Begins at Forty (1935 film)
{{Infobox film
| name           = Life Begins at Forty
| image_size     =
| image	=	Life Begins at Forty FilmPoster.jpeg
| caption        = George Marshall
| producer       = Sol M. Wurtzel William M. Conselman (uncredited) Dudley Nichols (uncredited)
| narrator       = Richard Cromwell George Barbier Rochelle Hudson
| music          =
| cinematography =
| editing        =
| distributor    = Fox Film Corporation
| studio         = Fox Film Corporation
| released       = March 22, 1935
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 Richard Cromwell. It is based on the non-fiction self-help book Life Begins at 40 by Walter B. Pitkin.

==Cast==
*Will Rogers as Kenesaw H. Clark
*Richard Cromwell as Lee Austin George Barbier as Col. Joseph Abercrombie
*Rochelle Hudson as Adele Anderson
*Jane Darwell as Ida Harris
*Slim Summerville as T. Watterson Meriwether
*Sterling Holloway as Chris Thomas Beck as Joe Abercrombie
*Roger Imhof as Pappy Smithers
*Charles Sellon as Tom Cotton
*John Bradford as Wally Stevens
*Ruth Gillette as Mrs. Cotton

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 