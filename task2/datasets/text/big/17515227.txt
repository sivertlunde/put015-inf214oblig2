The Revenge of Tarzan
{{Infobox film name        = The Revenge of Tarzan image       = The Revenge of Tarzan (1920) - Ad 1.jpg image_size  = 210px caption     = Ad with original title The Return of Tarzan based on    =   writer      = Robert Saxmar starring    = Gene Pollar Karla Schramm Estelle Taylor Armand Cortes Franklin B. Coates director    = Harry Revier George M. Merrick producer    = Samuel Goldwyn Edgar Rice Burroughs studio      = Numa Pictures Corporation distributor = Goldwyn Pictures released    =   runtime     = 90 minutes language  Silent (English intertitles)
}}     silent adventure film, originally advertised as The Return of Tarzan, and the third Tarzan film produced.  The film was produced by the Great Western Film Producing Company, a subsidiary of Numa Pictures Corporation. The film was sold to Goldwyn Pictures for distribution. 

The film was written by Robert Saxmar (based on the 1915 novel The Return of Tarzan by Edgar Rice Burroughs), and directed by Harry Revier and George M. Merrick. It was released on May 30, 1920.

==Synopsis== Jane are traveling to Paris to help his old friend Countess de Coude, who is being threatened by her brother, Nikolas Rokoff. Rokoff has Tarzan tossed overboard. He survives, comes ashore in North Africa, and goes to Paris to search for Jane.

In Paris, Tarzan reunites with his old friend Paul DArnot, who informs him that Jane was taken to Africa.

Tarzan returns just in time to save Jane from a lion attack, and soon defeats Rokoff and his henchmen.

==Cast==
*Gene Pollar as Tarzan
*Karla Schramm as Jane
*Estelle Taylor as Countess de Coude, Tarzans ally
*Armand Cortes as Nikolas Rokoff, a villain
*Franklin B. Coates as Paul DArnot, Tarzans old friend
*George Romain as Count de Coude

==Production notes==
The production filmed on location in New York, Florida, and Balboa, California.
 The Son Brenda Joyce are the only two actresses who have portrayed Jane opposite two different Tarzans.

Gene Pollar, a former firefighter, made no other films, and returned to his old job after the film was completed.

Outside of the United States, the film is known by its working title, The Return of Tarzan. The title was changed for its American release in July 1920.  The film is currently lost film|lost.

==See also==
*List of lost films

==References==
 

==Bibliography==
*Essoe, Gabe. Tarzan of the Movies (Citadel Press, 1968)

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 