Grace Quigley
{{Infobox film
| name           = Grace Quigley
| image          = Grace-Quigley-1985.jpg
| image_size     =
| caption        = movie poster
| director       = Anthony Harvey
| producer       =
| writer         = A. Martin Zweiback
| narrator       =
| starring       = Katharine Hepburn Nick Nolte
| music          = John Addison
| cinematography = Larry Pizer
| editing        = Robert M. Reitano
| distributor    =
| released       =  
| runtime        = 102 minutes (Premiere) 87 minutes (Theatrical) 94 minutes (revised cut)
| country        =   English
| budget         = $5 million   accessed 6 February 2015 
}}
Grace Quigley, also titled The Ultimate Solution of Grace Quigley, is a 1985 film starring Katharine Hepburn and Nick Nolte. The plot centers around an elderly woman who decides not to wait around to die of old age, but hires a hit man to kill her. The film is noted for being Hepburns last leading role in a movie for the big screen, as well as the last appearance, stage or otherwise, of Walter Abel.

In addition to the version of the film that was released in 1985, at least two other versions are known to exist: the original cut, which premiered at the Cannes Film Festival in 1983, running 102 minutes; and the alternate version, The Ultimate Solution Of Grace Quigley, assembled by the writer, A. Martin Zweiback, running 94 minutes.  The latter version is considered superior by some critics. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 