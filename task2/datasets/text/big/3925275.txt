The One and Only, Genuine, Original Family Band
{{Infobox film
| name           = The One and Only, Genuine, Original Family Band
| image          = One-and-only-genuine-original-family-band-poster.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Michael OHerlihy
| producer       = Bill Anderson
| screenplay     = Lowell S. Hawley
| based on       =   John Davidson Jack Elliott
| cinematography = Frank V. Phillips
| editing        = Cotton Warburton Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 110 min.
| country        = U.S.A.
| language       = English
| budget         = 
| gross          = $2,250,000 (US/ Canada) 
| preceded by    = 
| followed by    = 
}} Walt Disney 1888 presidential election, the film portrays the musically talented Bower family, American pioneers who settle in the Dakota Territory.
 John Davidson head the cast. Kurt Russell is also featured, and, in a bit part, Goldie Hawn makes her big-screen debut.

==Production== frontier life in the Black Hills.
 Robert Sherman reportedly did so under protest, believing the subject matter too mundane to be made into a feature-length musical film.
 John Davidson his father.

==Plot== 1888 convention. Electoral College reversal and the presidency. Before he leaves office, Cleveland grants statehood to the two Dakotas, along with two Democrat-voting territories, evening the gains for both parties.  The Dakotans, particularly the feuding young couple, resolve to live together in peace.

==Songs==
"The One and Only, Genuine, Original Family Band" The film opens with Grandpa conducting all ten members of the Bower family, each playing a different musical instrument.  Practicing in their barn, the family dances among the animals and hay, boasting of their unique talents and versatility.

"The Happiest Girl Alive" Alice expresses her intense emotions over receiving her latest letter from suitor Joe Carder.

"Lets Put It Over with Grover" The Bowers perform this Grover Cleveland campaign song to a representative from the Democratic National Committee. 

"Ten Feet off the Ground" Ecstatic at the prospect of performing at the National Convention, the family band engages in an impromptu celebration.  They sing about the feeling which only music can bestow, figuratively lifting them "Ten Feet off the Ground". (This was one of two songs from the film covered by Louis Armstrong later in 1968.)

"Dakota" Joe Carder entices local Missouri families, singing about the marvels of the Dakota Territory. ("Dakota" is similar in style to the title song of the Oklahoma! and was once considered as a candidate for "state song" for South Dakota.)

"Bout Time" Joe Carder expresses his devotion to Alice, telling her its "Bout Time" they were engaged, she responds in kind, and the two sing this duet. (This song was covered by Louis Armstrong and was later featured in the 2005 film, Bewitched (2005 film)|Bewitched.)
  drummer boy Civil War, school house that they too can stand their ground and make a difference.
 Wide Missouri.

"Oh, Benjamin Harrison" The Republicans in town have their own campaign song; they sing their praise for Benjamin Harrison, who is "far beyond comparison."

The original cast soundtrack was released on Buena Vista Records in stereo (STER-5002) and mono (BV-5002).  Disneyland Records released a second cast album with studio singers and arrangements by Tutti Camarata, with both mono (DQ-1316) and stereo (STER-1316) versions.  Neither the soundtrack or the second cast album have been released on CD or to iTunes.

==Cast==
*Walter Brennan - Renssaeler Bower
*Buddy Ebsen - Calvin Bower John Davidson - Joe Carder
*Lesley Ann Warren - Alice Bower
*Janet Blair - Katie Bower
*Kurt Russell - Sidney Bower
*Steve Harmon - Ernie Stubbins Richard Deacon - Charlie Wrenn
*Wally Cox - Wampler
*Debbie Smith - Lulu Bower
*Bobby Riha - Mayo Bower
*Smith Wordes - Nettie Bower
*Heidi Rook - Rose Bower
*Jon Walmsley - Quinn Bower
*Pamelyn Ferdin - Laura Bower
*John Craig - Frank
*William Woodson - Henry White
*Goldie Hawn (as Goldie Jeanne Hawn) - Giggly Girl
*Jonathan Kidd - Telegrapher

==Theatrical Release==

The film premiered at Radio City Music Hall in New York City. Originally intended to run 156 minutes, the Music Hall requested 20 minutes of cuts. Disney responded by cutting the film to 110 minutes. Among the cuts were Westerin , sung by Calvin, and I Couldnt Have Dreamed it Better, sung by Katie. The Sherman Brothers and producer Bill Anderson objected, but the studio heads told them the cuts would be just for the Music Halls engagement. Robert B. Sherman pointed out that the Music Hall is where New York film critics screen these films, arguing that the cuts weakened the characters dramatic motivation. He also predicted that those cuts would result in negative reviews.
 The Wonderful World of Disney in two parts on January 23 and January 30, 1972.   

As of 2014, Disney has made no attempt at a reconstruction of the originally intended cut, but sheet music of the two cut songs was included in the book Disneys Lost Chords, Volume 2.

==Home video==
While a planned 1979 MCA DiscoVision release with the catalog number D18-513 was cancelled, the film was released on videotape in 1981 and on laserdisc in 1982.    

After 20 years of unavailability, the film was released on DVD on July 6, 2004. Though the transfer was not in the original aspect ratio, it included an audio commentary from Richard M. Sherman, Lesley Ann Warren and John Davidson and a 12-minute making-of featurette featuring all three.

==Literary sources==
* Van Nuys, Laura Bower (1961). The Family Band : from the Missouri to the Black Hills, 1881-1900. Pioneer Heritage Series, vol. 5.  Lincoln: University of Nebraska Press.
*  . Santa Clarita: Camphor Tree Publishers, pgs. 148-149.
* Gheiz, Didier (2009). Walts People - Volume 8. Xlibris Corporation, pgs. 203, 206-207, 247.
* Schroder, Russell (2008). Disneys Lost Chords Volume 2. Robbinsville, North Carolina: Voigt Publications, pgs. 17-25.

==External links==
*  
*  
*  
*   on UltimateDisney.com
*  , New York Times 1968 movie review
*  , Time magazine 1968 movie review
*  , Keystone Area Historical Society
*   on CastAlbums.org

==References==
 

 

 
 
 
 
 
 
 
 