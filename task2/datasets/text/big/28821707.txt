Elles et Moi
Elles et Moi is a two-part film directed by Bernard Stora. The languages spoken in the film are French and Catalan.

== Synopsis ==
January 1939. The fall of Barcelona declares the defeat of the Spanish Republicans. Five hundred thousand of them choose exile. Arriving in France, the men were disarmed and interned in camps, and their families are moved by the government to makeshift camps, many in Ardeche. "Elles et moi" follows the fate of the Esteva family during those terrible months and the five years of war that will follow.

While Lluis refuses to accept defeat and dreams of a future victory, Pilar seeks above all to survive and raise her children Isabel and Igniacio. She knows that this new country will be theirs for a long time and despite the difficulties, she tries to integrate. Sixty years later, Isabel Esteva, having become a world famous fashion designer, remembers her troubled times.

== Technical Details ==
* Genre: Drama
* Time 1 () () st part: 119 min
* Duration 2 ((e)) part: 103 min
* Digital High Definition - 5.1 sound
* Country:   and  
* Director: Bernard Stora
* Original Screenplay: Bernard Stora and Leny Bernard
* Dialogues: Bernard Stora

== Cast ==
 
* Ariadna Gil : Pilar Esteva
* Astrid Berges-Frisbey : Isabel Esteva, the daughter of Pilar and Lluís
* Danielle Darrieux : Isabel Esteva, at 80
* Montserrat Carulla : Esperanza, the mother of Pilar
* Julie Depardieu : Alice Brunetti
* Jean-Pierre Marielle : Emile de Montellier
*  , mother of Armand
* Guillaume Gallienne : Robert
* Bernard Blancan : Rafael
* Luis Rego : Jo Morales
 

== External links ==
*     
*     
*     
*     

 
 


 