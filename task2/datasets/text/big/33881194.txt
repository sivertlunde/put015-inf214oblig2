Mortal Sin (film)
 
{{Infobox film
| name           = Mortal Sin
| image          = 
| caption        = 
| director       = Miguel Faria, Jr.
| producer       = Gustavo Dahl Miguel Faria, Jr.
| writer         = Miguel Faria, Jr.
| starring       = Fernanda Montenegro
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}
 Best Foreign Language Film at the 43rd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Fernanda Montenegro as Fernanda
* José Lewgoy as José
* Renato Machado as Renato
* Anecy Rocha as Anecy
* Rejane Medeiros as Rejane
* Suzana de Moraes as Suzana
* Marina Montini
* Ivan Pontes

==See also==
* List of submissions to the 43rd Academy Awards for Best Foreign Language Film
* List of Brazilian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 