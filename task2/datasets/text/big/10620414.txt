Dharam Karam
{{Infobox Film
| name           = Dharam Karam
| image          =Dharamkaram.jpg 
| image_size     =
| caption        = 
| director       = Randhir Kapoor
| producer       = Raj Kapoor
| writer         = Prayag Raj
| narrator       = 
| starring       = Raj Kapoor Randhir Kapoor Rekha Dara Singh
| music          = Rahul Dev Burman
| cinematography = Taru Dutt
| editing        = Shankar Hurde
| studio         = 
| distributor    = R.K. Films Ltd. Yash Raj Films
| released       = 1 December 1975
| runtime        = 159 mins
| country        = India
| language       = Hindi
| budget         = 
}}

Dharam Karam is a 1975 Hindi film produced by Raj Kapoor and directed by Randhir Kapoor, who also star as father and son in the film, respectively. The film also stars Rekha, Premnath and Dara Singh. The music is by R.D. Burman and the lyrics by Majrooh Sultanpuri, who received a Filmfare nomination as Best Lyricist for the hit song "Ek Din Bik Jayega."  The song is played several times during the film, with playback singing by  Mukesh (singer)|Mukesh, Kishore Kumar, and Sushma Shrestha.  Of the three of them, only Mukesh received a Filmfare nomination as Best Male Playback Singer for the song.   According to one source, the film performed "below average" at the box office. 

== Plot ==
Shankar is a hoodlum who lives in a shanty hut with his pregnant wife, Kanta, and makes a living as a career criminal. He prays to Lord Shiv that if he is blessed with a male child, he will ensure that the child does not take to his path, but instead grows up to a decent and honest human being. His wife does give birth to a baby boy, and Shankar loots the ill-gotten gains of another hoodlum named J.K.. A furious J.K. hunts down Shankar in an attempt to abduct his son, but Shankar takes his child and switches him with one belonging to renowned stage artist, Ashok Kumar. Shankar gets into a scuffle with J.K. and his men, killing one of them, getting arrested, tried in Court, and being sentenced to 14 years in jail. Kanta passes away, while Dharam is left in the care of a wrestler, Bhim Singh, and a mid-wife, Ganga. Dharam is taught to be a hoodlum, but wants to focus on becoming a singer, while Ranjit has taken to alcohol, gambling, and a life of crime under J.K. himself. After his discharge, Shankar finds to his delight out that Ashok has brought up Ranjit and both are stage actors. Then his world descends into chaos when he finds out that Ranjit is in fact Dharam, while his very son, Ranjit has taken to a life of crime. Angered at Dharam, he beats him up and asks him to be a hoodlum like himself, he also beats up Ranjit and asks him to obey Ashok and follow in his footsteps. Watch as things spiral out of control when a vengeful J.K. abducts Shankar and holds him hostage - the ransom - the dead body of Ashok - at any and all costs - and the person chosen to carry out this task is none other than Dharam!!

== Cast ==
*Raj Kapoor &ndash; Ashok Bonga Babu Kumar
*Randhir Kapoor &ndash; Dharam/Ranjit
*Rekha &ndash; Basanti
*Prem Nath &ndash; Shankar Dada
*Dara Singh &ndash; Bhim Singh
*Pinchoo Kapoor &ndash; J.K.
*Narendra Nath &ndash; Ranjit A. Kumar
*Urmila Bhatt &ndash; Ganga
*Alka &ndash; Neena Master Satyajeet &ndash; Young Dharam
*Baby Pinky &ndash; Young Basanti
*Master Sailesh &ndash; Young Ranjit A. Kumar
*Rajan Kapoor
*Aarti
*Subhash

==References==
 

== External links ==

*  

 

 
 
 
 
 