Trial and Error (1997 film)
 
{{Infobox Film  |
  name          = Trial and Error |
  image         = Trial and error poster.jpg|
  caption       = Theatrical release poster|
  producer      = Jonathan Lynn Gary Ross |
  director      = Jonathan Lynn |
  writer        = Sara Bernstein Gregory Bernstein |
  starring      = Michael Richards Jeff Daniels Charlize Theron Austin Pendleton Rip Torn Jennifer Coolidge |
  distributor   = New Line Cinema  |
  released      = May 30, 1997 |
  runtime       = 98 minutes |
  language      = English |
  budget        = $25 million  |
  gross         = $14,598,571 |
  }}

Trial and Error is a 1997 American romantic comedy film about an attorney and his friendly actor, who takes his place in court to defend the bosss hopelessly guilty relative. It stars Michael Richards, Jeff Daniels and Charlize Theron.

== Plot ==
Charlie Tuttle (Jeff Daniels) is a partner in a successful law firm, Whitfield and Morris. His boss and future father-in-law sends him to Paradise Bluff, Nevada, to request a continuance in a mail-fraud case involving a distant relative who is believed to be guilty, and is very likely to be adjudged guilt (law)|guilty. But the timing of the trip conflicts with Charlies bachelor party. After Charlie drives from California to Paradise Bluff, he is unexpectedly greeted by his best man, actor Richard Rjetti, (Michael Richards) who is determined to show his friend a great time prior to his wedding.
 knocked out prescribed painkillers prison for perpetrating and conspiring to perpetrate a fraud upon the court.
 rules of banned from reentering the courthouse. Later, Richard and Charlie devise a communication system involving a baby monitor and morse code|morse-code sounding of Charlies Jaguar Cars|vehicles horn, heard through an open window, to instruct Richard as to which type of objection to use.
 attractive waitress Billie Tyler (Charlize Theron) who causes Charlie to rethink his impending wedding to his shrill, self-absorbed fiancée, Tiffany (Alexandra Wentworth). Richard becomes involved with the prosecutor, Elizabeth (Jessica Steen) against whom he ultimately finds due process for "his" client.

==Reception==
Despite Richards extreme popularity on Seinfeld, and Daniels success as a comic actor in Dumb and Dumber, Trial and Error failed to be a big hit with critics and audiences alike. Most critics and audiences compared it to director Jonathan Lynns far more critically and commercially successful courtroom comedy My Cousin Vinny with Joe Pesci and Oscar winner Marisa Tomei, since this film possesses a very similar plot. Austin Pendleton, who played defense attorney John Gibbons in My Cousin Vinny, is cast in this film again by director Lynn as the judge, a role that was played by Fred Gwynne in the earlier film. Trial and Error earned a very low box office income of about $13 million domestically. Despite this, film critics Roger Ebert and Leonard Maltin gave positive reviews to the film both rewarding it 3 stars out of a possible four.

== Cast ==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor|Actor/Actress Role
|-
| Michael Richards
| Richard Rjetti
|-
| Jeff Daniels
| Charlie Tuttle
|-
| Charlize Theron
| Billie Tyler
|-
| Jessica Steen
| Elizabeth Gardner
|-
| Austin Pendleton
| Judge Paul Z. Graff
|-
| Rip Torn
| Benjamin Gibbs
|-
| Alexandra Wentworth
| Tiffany Whitfield
|-
| Jennifer Coolidge
| Jacqueline "Jackie" Turreau
|-
| Lawrence Pressman
| Whitfield
|-
| Dale Dye
| Dr. German Stone
|-
| Max Casella
| Dr. Brown
|-
| McNally Sagal
| Charles Assistant
|-
| Kenneth A. White
| Hank Crabbit
|-
| Keith Mills
| Buck Norman
|-
| Zaid Farid
| Bailiff
|}

==External links==
*  
*  
*  

==Notes==
 
 

 
 
 
 
 
 
 
 