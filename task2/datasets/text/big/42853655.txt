We Make Music
{{Infobox film
| name = We Make Music
| image =
| image_size =
| caption =
| director = Helmut Käutner 
| producer =  
| writer =  Manfried Rössner  (play)   Hans Effenberger    Erich Ebermayer   Helmut Käutner 
| narrator =
| starring = Ilse Werner   Viktor de Kowa   Edith Oß   Grethe Weiser
| music = Peter Igelhoff   Adolf Steimel  
| cinematography = Jan Roth   
| editing =  Helmuth Schönnenbeck      
| studio =  Terra Film 
| distributor = Deutsche Filmvertriebs 
| released = 8 October 1942
| runtime = 95 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
We Make Music (German:Wir machen Musik) is a 1942 German musical comedy film directed by Helmut Käutner, starring Ilse Werner, Viktor de Kowa and Edith Oß.  

==Plot==
It is a revue film, loosely based on the stage work Karl III. und Anna von Österreich by Manfried Rössner. Karl Zimmermann, a composer whose idols are Johann Sebastian Bach and classical composers, dreams of being successful with his own opera and composes popular music for fun, but does not try to distribute it for reasons of honor. His wife, Anni Pichler is a popular singer and secretly sells his popular songs so that they can make a living. When he finally completes his opera and it is rejected by the public, he realizes that his true talent is composing popular music and not dishonorable.

==Cast==
*  Ilse Werner as Anni Pichler  
* Viktor de Kowa as Paul Zimmermann  
* Edith Oß as Trude, Trompeterin  
* Grethe Weiser as Monika Bratzberger  
* Georg Thomalla as Franz Sperling  
* Rolf Weih as Peter Schäfer  
* Ilse Buhl as Altsaxofonistin  
* Sabine Naundorff as Tenorsaxofonistin  
* Hilde Adolphi as Posaunistin  
* Gertrud Leonhardt as Gitarristin  
* Eva Gotthardt as Bassistin  
* Kurt Seifert as Hugo Bratzberger  
* Victor Janson as Direktor Pröschke 
*Lotte Werkmeister as Karls Zimmerfrau Frau Zierbarth  
* Helmuth Helsig  as Schornsteinfeger 
* Ewald Wenck as Gasmann Knebel 
* Wilhelm Bendow as Theaterinspizient   Klaus Pohl as Bühnenportier in der Oper  
* Sonja Kuska as Musikschülerin Barbara  
*  Otto Braml as Vorsitzender der Prüfungskommission 
* Curt Cappi as Kellner Neumann 
* Friedrich Wilhelm Dann as Zugschaffner
* Hanne Fey as Sekretärin des Musikverlags 
* Robert Forsch as Musikverlagsdirektor Mehmann 
* Karl Hannemann as Direktor der Opernbühne 
* Sonja Kuske as Barbara, Musikschülerin 
* Karin Luesebrink as Musikstudentin 
* Artur Malkowsky  as Opernsänger bei der Premiere 
* Maria von Höslin as Peters Braut 
* Helga Warnecke as Musikstudentin 
* Gertrud Seelhorst
* Hansi Würbauer 
* Sabine André

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 