His Private Secretary
 
{{Infobox film
| name           = His Private Secretary 
| image          = His Private Secretary FilmPoster.jpeg
| caption        = Film poster
| director       = Phil Whitman
| producer       = Al Alt  D.J. Mountan
| writer         = Lewis D. Collins Jack Natteford
| starring       = Evalyn Knapp John Wayne
| music          = 
| cinematography = Abe Scholtz
| editing        = Bobby Ray
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = English
| budget         = 
}}

His Private Secretary is a 1933 American comedy film starring Evalyn Knapp and John Wayne.

==Plot==
Dick Wallace, portrayed by a 26-year-old John Wayne, has to prove to the Preachers daughter, his own Dad, his old friends, and himself that he isnt just an irresponsible playboy. Fortunately, his new love, Marion does a good job of convincing them. The question is whether or not it is true.

==Cast==
* Evalyn Knapp as Marion Hall
* John Wayne as Dick Wallace
* Reginald Barlow as Mr. Wallace
* Alec B. Francis as Rev. Hall
* Arthur Hoyt as Little
* Natalie Kingston as Polly
* Patrick Cunning as Van, Pollys Brother
* Al St. John as Garage Owner Tom Hugh Kidder as Jenkins, the Butler
* Mickey Rentschler as Joe Boyd

==See also==
* John Wayne filmography

==External links==
* 
* 

 
 
 
 
 
 
 
 