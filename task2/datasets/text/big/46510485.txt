Cómo sobrevivir a una despedida
{{Infobox film
| name           = Cómo sobrevivir a una despedida
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Manuela Moreno
| producer       = 
| writers        = Susana López Rubio Manuela Moreno Nuria Valls
| screenplay     = 
| story          = 
| based on       = 
| starring       = Celia de Molina Natalia de Molina Úrsula Corberó Brays Efe María Hervás
| narrator       = 
| music          = 
| cinematography = Bet Rourich
| editing        = Antonio Frutos Atresmedia Cine
| distributor    = DeAPlaneta
| released       =  
| runtime        = 
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}

Cómo sobrevivir a una despedida ("How to survive a hen party") is a 2015 Spanish comedy film directed by Manuela Moreno and starring sisters Celia and Natalia de Molina and Úrsula Corberó.

==Plot== Christian Grey-like interns that only allow them to buy new clothes at the sales and dating guys who are allergic to commitment. Never ones to give up in the face of adversity, they decide to set up a bachelorette party for the more responsible member of the gang, Gisela (Celia de Molina), in Gran Canaria when she is about to become the first of the group to get married. 

==Cast ==
*Natalia de Molina as Nora
*Úrsula Corberó as Marta
*Celia de Molina as Gisela
*María Hervás as Tania
*Brays Efe as Mateo
*Jim Arnold as Roman
*Javier Bódalo as Jonathan
*Daniel Pérez Prada as Lucas
*Roger Berruezo as Rai
*José Lamuño as Yago
*Miki Nadal as the talent show host
*Emma Bunton as herself

==References==
 

 
 
 
 
 