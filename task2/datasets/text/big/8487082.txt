Safeguarding Military Information
{{Infobox film
| name = Safeguarding Military Information
| image = Safeguarding Military Information VideoCover.jpeg
| image_size =
| caption =
| director =
| producer = Academy of Motion Picture Arts and Sciences
| writer = Preston Sturges
| narrator =
| starring = Walter Huston Eddie Bracken
| music =
| cinematography =
| editing =
| distributor = War Activities Committee of the Motion Picture Industry
| released = January 16, 1942
| runtime = 9 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Safeguarding Military Information was a short propaganda film produced by the Academy of Motion Picture Arts and Sciences in 1942 in film|1942.

==Description==
The film opens with a dramatic explosion of a ship by two undercover saboteurs and then fades into a written "Thoughtlessness Breeds Sabotage" message. A short vignette comes next with a sailor with his girlfriend on the telephone at a bar. As he placates her suspicions by telling her that he is sailing to Hawaii on the USS Navajo at 10:30 pm. A man with a radio disguised as a hearing aid is sitting next to him, and he sends the message to his confederates speaking in code with an enemy submarine, which blows up the ship.

Walter Huston then appears as a military instructor briefing a class about military security, and narrates a short vignette about service men in a bowling alley and how they confront a stranger asking questions about military equipment.

Probably the most powerful segment is the last, in which a woman at a grocery store, tells the grocer about her son George taking a train to the West Coast, unaware of the person standing behind her. The film fades into a newspaper room, and an editor hurriedly ordering a re-write after receiving a telephone call. When the headline is shown, it does not show the information that the woman gave out, but that 200 servicemen died in a train explosion. The womens face is shown superimposed on the newspaper saying, "No, not George!"

== See also ==
*List of Allied propaganda films of World War II

== External links ==
* 
* 

 

 
 
 
 

 