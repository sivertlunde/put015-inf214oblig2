The Devil's Hairpin
{{Infobox film
| name           = The Devils Hairpin
| image          = 
| caption        = 
| director       = Cornel Wilde
| producer       = Cornel Wilde
| based on       = 
| screenplay     = 
| starring       = Cornel Wilde
| music          = 
| cinematography = 
| editing        = 
| studio         = Theodora Productions
| distributor    = Paramount Pictures
| released       = 4 October 1957
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $1 million (US) 
}}
The Devils Hairpin is a 1957 feature film about car racing, filmed in Technicolor and VistaVision, written and directed by Cornel Wilde, who also stars.

==Plot==
Nick Jargin retired from auto racing undefeated. He is continually goaded by Mike Houston, a sportswriter, to come out of retirement and challenge the top racer of the day, Tony Botari, particularly after egotistically saying in an interview that Botari has no real competition now that hes out of the sport.

Nicks girlfriend is Kelly James, a health club instructor. Kelly wants to be married, and when a reluctant Nick introduces her to his mother, Mrs. Jargin wants nothing to do with him, blaming Nick for a racing accident that seriously injured her other son, Johnny.

Kelly is even urged by Nicks mother to leave him. She gives him an ultimatum, marry her or else. He declines, so she goes back to former boyfriend Danny Rhinegold, who now runs Botaris racing team.

In the 100-lap race that takes them along rural roads, Nick takes the lead, with his brother Johnnys help on the crew. Botari is nearly in an accident in the dangerous "Devils Hairpin" turn, so Nick slows down to help Botari steer clear of it. A self-sacrificing gesture is rare for him, so after the race, Kelly accepts when Nick finally proposes to her.

==Cast==
* Cornel Wilde as Nick Jargin
* Jean Wallace as Kelly James
* Mary Astor as Mrs. Jargin
* Arthur Franz as Danny Rhinegold Morgan Jones as Chico Martinez
* Gerald Milton as Houston
* Paul Fix as Doc

==References==
 

==External links==
*  at IMDB

 

 
 
 
 

 