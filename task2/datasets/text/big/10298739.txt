Son of the Gods
{{Infobox film name = Son of the Gods image = SonoftheGods1930.jpg producer  = Frank Lloyd director = Frank Lloyd writer = Bradley King based on the novel by Rex Beach starring = Richard Barthelmess Constance Bennett Frank Albertson music = cinematography = Ernest Haller (Technicolor) editing = distributor = First National Pictures released =   runtime = 93 minutes language = English country = United States
}}

Son of the Gods (1930) is an talkie|all-talking Pre-Code Hollywood|pre-code romantic drama film with Technicolor sequences, produced and released by First National Pictures, a subsidiary of Warner Bros.. It was adapted from the novel of the same name by Rex Beach. Richard Barthelmess and Constance Bennett star as a couple in love who have a falling-out when she discovers that, though he looks Caucasian, he is actually Chinese.

==Synopsis==
Sam Lee (Barthelmess) is the son of a Chinese merchant, Lee Ying, in San Franciscos Chinatown. He is tolerated in white dominated social circles because of his wealth. One day, he leaves college after being insulted by three racist white girls who thought they were too good to be seen going out with a "dirty yellow Chinaman". He takes a tour around the world and ends up in the Riviera where he is introduced to Allana Wagner (Bennett). 

Allana falls madly in love with Sam and refuses to her anything about his background. Sam is afraid to pursue a relationship with her until she tells him that she used to date a man from India and that she is open to interracial relationships. Sam then becomes Allanas boyfriend and everything goes well for a while. One day, however, Allana discovers that Sam is Chinese and she flies into a rage and lashes him in public with her riding crop. 

Heartbroken, Sam now returns home, only to find that his father is on his deathbed. Sam now discovers that his parents were both white and that he had been orphaned as a child. Allana, unable to quench her love for Sam, returns to the United States in order to find him. She declares her love for Sam before he has a chance to tell her that he is indeed white. The film ends with the lovers happily reunited.

==Cast==
*Richard Barthelmess as Sam Lee
*Constance Bennett as Allana Wagner
*Anders Randolf as Wagner, Allanas father
*E. Alyn Warren as Lee Ying, Sams father Claude King as Bathurst
*Frank Albertson as Kicker
*King Hou Chang as Moy
*Mildred Van Dorn as Eileen
*Barbara Leonard as Mabel

==Preservation==
The film only survives in black and white. One reel was originally in Technicolor, but no color prints seem to have survived. This reel takes place in Chinatown and tells us story of how Barthelmess was orphaned and adopted by the Chinese man he had thought was his father. Current prints present this originally color sequence in sepia-tone.

==See also==
*List of early color feature films

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 