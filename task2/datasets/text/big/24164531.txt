Broadway Scandals
 
{{Infobox film
| name           = Broadway Scandals
| caption        =
| image	         = Broadway Scandals FilmPoster.jpeg
| director       = George Archainbaud
| producer       = Harry Cohn
| writer         = Norman Houston Howard J. Green Gladys Lehman (scenario)
| starring       = Sally ONeil Jack Egan 
| music          = Harry Jackson
| choreographer  = Rufus LeMaire
| editing        = Leon Barsha Ben Pivar
| studio         = Columbia Pictures Corporation
| distributor    = Columbia Pictures
| released       =    	
| runtime        = 73 minutes
| country        = United States English
| budget         =
| gross          =
}}

Broadway Scandals, aka Scandalo di Broadway (Italy) is a 1929 American black and white musical film.   

==Cast==
* Sally ONeil - Mary
* Jack Egan - Ted Howard
* Carmel Myers - Valeska
* J. Barney Sherry - Le Claire John Hyams - Pringle Charles C. Wilson - Jack Lane
* Doris Dawson - Bobby
* Wild Bill Elliott - George Halloway
* Charles Willis Lane

==Songs==
* "Does An Elephant Love Peanuts?"
::Music and Lyrics by James F. Hanley
::Sung by Jack Egan
::Danced by Jack Egan and Sally ONeill
::Copyright 1929 by Shapiro, Bernstein & Co. Inc.

* "What Is Life Without Love?"
::Sung by Jack Egan
::Music and Lyrics by Jack Stone, Fred Thompson, & Dave Franklin
::Copyright 1929 by Irving Berlin Inc.

* "Would I Love To Love You (Id Love To)"
::Sung by Jack Egan
::Words and Music by Dave Dreyer and Sidney Clare
::Copyright 1929 by Irving Berlin Inc.

* "Can You Read in My Eyes"
::Music and lyrics by Sam Coslow

* "Loves the Cause of All My Blues"
::Music and lyrics by Joe Trent and Charles Daniels

* "Rhythm of the Tambourine"
::Music and lyrics by David Franklin

* "Kickin the Blues Away"
::Music and lyrics by David Franklin and James F. Hanley.

==Reception==
Photoplay Magazine was unenthusiastic in its review of Broadway Scandals: "If this picture appeared six months ago, it would have looked better, for it is a late entrant in the line of love stories back of the theater curtain." Egan and Myers did well in their roles, while "Sally ONeil tries hard." 

==References==
 

==External links==
*  
*  
*   by New York Times
*  

 

 
 
 
 

 