Leafie, A Hen into the Wild
 
{{Infobox film name = Leafie, A Hen into the Wild image = Leafie_A_Hen_Into_the_Wild_poster_Korean.jpg film name = {{Film name hangul =       rr = Madang-eul Naon Amtak mr = Madang-ŭl Naon Amt‘ak}} caption = Korean poster director = Oh Sung-yoon producer = Shim Jae-myung Lee Eun Son Gwang-ik Kim Seon-gu writer = Na Hyun   Kim Eun-jung  based on =   starring = Moon So-ri  Yoo Seung-ho Choi Min-sik music = Lee Ji-soo cinematography = Lee Jong-hyuk editing = Kim Hyeong-ju Kim Jae-bum Kim Sang-bum studio = Myung Films distributor = Lotte Entertainment released =   runtime = 93 minutes country = South Korea language = Korean budget =   gross =   
}} hen as she raises an adopted duckling. It made box office history by drawing over 2.2 million viewers, the largest audience for a home-grown animated film in South Korea.

==Source material==
The film is based on a well-respected and extremely popular childrens book authored by Hwang Sun-mi. The novel was first released in South Korea in 2000, and sold more than 1 million copies domestically. It has been sold to nine countries including France, Poland, Japan, China, Vietnam, Thailand and Italy. The Polish-translated version of the novel was named the "Best Book of the Year 2012" and "Best Book of Spring 2012" (Najlepsza książka na wiosnę 2012) by Granice.pl, a renowned literary organization in Poland.  It was translated into English by Chi-young Kim for Penguin Books under the title The Hen Who Dreamed She Could Fly, and the 144-page novel was published on November 26, 2013.     

==Plot== eggs even though they are her own. Dreaming of having her own young, she plans to escape the farm. While escaping, a one eyed weasel known as One-Eye threatens Leafie, but Wanderer (a mallard duck, 나그네 "Nagnae") helps her to escape. Leafie makes friends with Wanderer, but the guard duck leaves without a word. Leafie goes back to the Farm to join the group of farm animals who reside outside the egg farm in the yard, but Rooster argues with Leafie and refuses to allow her into his flock, wanting her to return laying eggs.

With nowhere else to go, Leafie sneaks out of the Farm and arrives in the wild, where she meets Mr. Otter, who heard about Leafie and helps her find a place to live on Wanderers behalf. The now wild hen meets Wanderer again but discovers he has a mate. That night, One-Eye kills Wanderers mate, and after hearing the commotion, Leafie enters the nest and finds a single egg. Wanderer accepts Leafie to take care of the egg for a few days, while one night fighting off One-Eye. Wanderer instructs Leafie to take his unborn child to the everglades, and she will understand in time why. That night One-Eye returns, and Wanderer engages her in a fight to the death. Leafie witnesses the battle but Wanderer is defeated and killed by the weasel, leaving Leafie alone and depressed.

After Wanderers death, the egg hatches into a baby duckling, who thinks Leafie is its mother. Leafie cares for the duckling and names him "Chorok head" or "Greenie" (초록머리, 초록 in the movie). The hen heads to a nearby glade where there are many water birds and took most of her time caring for Greenie, who grew up and learned how to swim from Mr. Otter. Later on Greenie tries to make friends with Mandarin Ducks but they make fun of Leafie, who they think is crazy. Growing up, a teenage Greenie becomes interested in flying, and Mr. Otter enlists the help from a Bat and an Owl to help Greenie learn how to fly. Meanwhile, Leafie discovers that the local water birds doesnt like her nor accept her in their society and becomes depressed. She then meets up with Greenie, who has been teased by the other ducks and blames Leafie for being an outcast. After discovering that they are indeed both different, Greenie leaves Leafie, feeling that he doesnt need her anymore.

Hearing about this, and the mentioning of Wanderer from Leafie, Mr. Otter tells her about Wanderer being the guard duck of his flock and how he crippled his wing while fighting One-Eye, who lost one of her eyes in the struggle. Wanderer was then captured by the farmer and held within the yard, but managed to escape sometime later. Greenie comes across the Farm after hearing about it from three ducks who live there. After arriving in the farm with the group, Greenie is attacked by the owner of the farm, who ties him down with a string in order to clip his wings. Chirpie, a friend of Leafies, sees this and goes to warn Leafie about Greenie, and the hen quickly rushes to the farm to save him.

To distract the farmer, Leafie, with the help of Mr. Otter, releases all of the hens from the chicken farm and she reunites with Greenie when Mr. Otter frees him by biting off the string that held him down. Rooster confronts Leafie before she could escape with Mr. Otter and Greenie, and in a short scuffle, all of the animals discover Roosters comb is fake and forces him to work for them. Leafie escapes the farm with Greenie and Mr. Otter, and both Leafie and Greenie reconcile after Mr. Otter departs. The two are suddenly confronted by One-Eye, who then hunts down Greenie to eat him. Despite Leafies intervention, the weasel managed to pin Greenie down on an old tree, which breaks off and sends the two plummeting over a cliff. Believing Greenie to be dead, Leafie mourns, but Greenie escaped certain death by successfully learning how to fly while One-Eye managed to save herself by grabbing hold of a ledge on the cliff.

A flock of ducks soon come during the autumn season, and Leafie realized what Wanderer meant when he instructed her to take his egg to the everglades. An adult Greenie goes to meet the flock and learns about an upcoming contest to decide who will be the new guard duck of the flock. The ducks flee from Greenie after seeing the human string that was still wrapped around his leg from his past encounter with the farmer. Leafie gives Greenie her support and reassures him to participate in the contest after taking a portion of the human string from his leg just in time. Greenie returns to the flock and announced that he wants to be in the contest as well, quickly forming a rivalry with another duck named Red Head.

During the race, Greenie recalls the words of his mentors, and successfully wins the contest, becoming the new guard duck of the flock. After the competition, Leafie finds a nest of baby weasels, to which she begins to take care of. While Rooster is finding a home with Mr. Otter after escaping from the farm, Greenie tries to find Leafie to say his goodbyes but comes across One-Eye once again and saves a female duck from the hungry carnivore. Their brief scuffle lands them in front of Leafie and the nest of baby weasels, and One-Eye pins down Greenie, preparing to kill him. Leafie attempts to help Greenie, but One-Eye threatens her to not come near. During the confrontation, the baby weasels Leafie had been caring for were revealed to be One-Eyes offspring. This makes Leafie realize that the weasel who killed Wanderer and his mate, is now killing to provide food for her kits. After agreeing to not harm the weasel kits in exchange for letting Greenie go, Leafie and Greenie were allowed to escape. Before they leave, Leafie sees how One-Eye, who is malnourished due to the rough winter season, is unable to produce milk to feed her kits.

The flock of ducks prepare to leave the everglades, and Leafie and Greenie say their heartfelt goodbyes to one another before Greenie departs with his flock, now able to set out and see the world. Soon afterwards, One-Eye comes. Leafie, feeling that shes done everything she can for Greenie and knowing that One-Eye is unable to produce milk from malnutrition, allows herself to be killed and eaten, so that the baby weasels will not starve.

==Characters==
Leafie/Yipsak/Sprout/Daisy (Voiced by Moon So-ri)
:The leading character Leafie exhibits strength. She doesnt settle for the fate she is given but accepts challenges and welcomes adventure. It is shown that Leafie tends to speak loudly, a trait that Greenie inherits. She was originally stuck in an egg farm, but she escapes by playing dead. While in a pit filled with dead chickens, the One-eyed Weasel appears and attacks Leafie to eat her, when Wanderer shows up, and saves her. Leafie immediately falls in love with him, but this quickly changes when she finds out he already has a mate. Before Wanderer died, he told her to take his unborn child to the everglades. When she asks why, he says "you will find out in time." She then raises Greenie despite numerous difficulties.

Greenie/Chorok Head/Green-Top/Willy (Voiced by Han Shin-jeong & Yoo Seung-ho)
:Greenie is a male wild Mallard duck, and is the son of Wanderer, but is adopted by Leafie after Wanderer dies. As a teenager, Greenie had a difficult time making friends with the other ducks, because his mother is a chicken. Because of this, he considered leaving Leafie, but after the scuffle at the chicken farm, Greenie accepts Leafie as his mother. Thanks to Mr. Otter, a bat, and an owl, Greenie grows up to be a strong duck and a good flier, and, thanks to this, he wins a contest held by the ducks to become a "guard duck". Greenie noticeably inherits Leafies trait of being loud.

Wanderer/Nagnae/Straggler/Wilson (Voiced by Choi Min-sik)
:Wanderer is a Mallard and a former guard duck. Guard ducks help defend their flocks from predators. He helped protect Leafie from the Weasel at the beginning of the film, which leads to Leafie falling in love with him, but this quickly changes when she finds out he already has a mate. He lived with his wife in a brier patch until the one-eyed weasel killed them both. They left behind one egg, which Leafie hatched, and later named the duckling Greenie. Before Wanderer died, he told Leafie to go to the everglades with his unborn child. When she asks him why, he says "you will find out in time." Mr. Otter tells Leafie that, when Wanderer was a guard duck, he got into a fight with the weasel, One-Eye. In the end, he couldnt use his right wing to fly anymore, but, in return, the weasel loses sight in one of her eyes.

Rooster (Voiced by Um Sang-hyun)
:Rooster is the king of the chicken flock who is a pompous and egotistical rooster on the chicken farm that lives freely in the yard. Even though Leafie admires him greatly, Rooster refuses to allow Leafie to join his flock and tells her to go back to laying eggs. Near the middle of the film, it is discovered that his comb is a fake, which then leads the animals of the barn to make him work for them. He then leaves, after seeing Greenie racing Red Head, and goes to find a home with Mr. Otter.

Mayor or Mr. Otter (Voiced by Park Chul-min)
:Mr. Otter is the local real estate agent who runs the wildlife community. He helps Leafie survive in the wild by helping her find a place to live and introducing her son to mentors, such as a bat and an owl who teach Greenie how to fly. Before Greenie leaves with the flock, he tells Mr. Otter to watch over his mother.

One-eyed Weasel (Voiced by Kim Sang-hyeon)
:One-Eye is portrayed as the antagonist in the story, as she persists in hunting Leafie and the ducks for food. She is a predator feared by those who live in the brier patch and everglades, and only eats live prey and those that catches her interest. During her first fight with Wanderer, she lost an eye during the struggle, but managed very well without it afterwards. However, at the end of the story, it is revealed that she is not actually evil, just trying to provide food for her family as she became a mother too.

Red Head/Ace (Voiced by Sa Seong-ung)
:Red Head is a rival wild duck that competed with Greenie to be a guard duck. However, after the competition, he good-naturedly becomes friends with Greenie.

Chirpie (Voiced by Jeon Suk-gyeong)
:A high-pitched talking Sparrow who is friends with Leafie and Greenie. He always visits Leafie at the farm sometimes, When he saw Greenie being attacked by the farmer he and his fellow sparrow friends distracted the farmer. He possess a dislike towards Rooster who always chases his flock off.

Owl (Voiced by Seo Seung-won)
:Owl helped Greenie how to fly.

Bat (Voiced by Hong Beom-gi)
:Bat helped Greenie to do some flying tricks, but required the duck to hang upside down to begin his training, to which Greenie found impossible. He also possess a dislike towards Mr. Otter, who suggested the bat to live in a cave, but it drips constantly with water.

Wanderers Mate (Voiced by Kim Ji-hye)
:A female white duck who lived with Wanderer and was Greenie`s mother until the One-eyed Weasel killed her.

Bully (Voiced by )
:A tough strong duck who competed in the race against Greenie.

Blade (Voiced by )
:A duck who participated in the guard duck race and tried to blind Greenie with sand but crashed into a tree during the race.

The Farmer (Voiced by )
:A elderly-like man who owns the chicken farm, He often does his job picking up the dead chickens from their cages, putting them in his wheelbarrow and dumping them in a pit of dead chickens. He often captures wild ducks and keeps them in his farm just like he did to Wanderer after crippling his wing in the aftermath of his battle against One-Eye (but managed to escape). Later in the film he caught Greenie after he entered the farm and tied a string on him and attaching it to his wheelbarrow, grabbed scissors and attempted to clip his wings, Luckily Leafie came to the rescue and attacked the farmer, but only to throw her in the chicken house. Chirpie and his fellow flock distracted the farmer and after his chickens went free, He was confused and couldnt get to Greenie and never harmed his wings, After that he is never seen again for the rest of the film. He is the only human character in the film.

Dol, Mee, Sol and Toe
:Four small white ducks who are clumsy and weird. Dol and Toe are twins and Mee and Sol are twins. They all like singing songs about themselves and like following the leader. After discovering Roosters comb was a fake, Mee took it and played around with it.

The Dog
:A ferocious dog who is very aggressive against strangers, but if petted or scratched on the back, he will leave the stranger alone.

The Hens
:Two hens who live freely in the yard. One of the hens has children of their own while the other one is still incubating their eggs. They have a strong dislike of Leafie and the other hen hates her children being near her. The two hens were later shocked when they discover Roosters comb was a fake.

==Production==
Director Oh Sung-yoon struggled for more than twenty years as an animator under adverse economic circumstances before finally debuting with this feature film.  Originally titled YIPSAK - A Chicken Wild, the movie took Myung Films six years (three years for pre-production including scriptwriting, and one and a half year for storyboard) and   ( ) to produce.   

Shim Jae-myung (also known as Jaime Shim), the head of Myung Films, which co-produced Leafie in conjunction with the local animation studio Odolttogi, said that it was her experience with major motion pictures that made her want to produce animated films that could compete with those from Hollywood and Japan. "And as a woman and mother, the plot touched my heart," she said. "Many people were doubtful about whether the film would be a success, but I believed in the power of the novel."  

When asked what they did to distinguish their film from films by the world’s major studios, director Oh said he and the crew focused on making the visual effects as beautiful as possible. "Most of my staff and I majored in painting, and we chose to make the film Animation#2D animation|two-dimensional, so that the entire product looks like a beautiful picture," Oh said. Though the sharply drawn foreground characters have an international look, the gentler backgrounds seem typically Korean in their use of landscape and flora (with the Upo wetlands in the south of the country inspiring the everglades in which much of the action takes place). "In addition to that, we have many quality animators on staff who draw for Pixar and Disney in Korea." Disney, Pixar and DreamWorks often hire small- and medium-sized Korean animation studios to work on their illustrations.    

==Soundtrack==
The soundtrack is composed by Lee Ji-soo and was released in 2011, consisting of 22 tracks.
*1. The Farm
*2. Attacked by the Weasel
*3. First Meeting 
*4. Leafie, Into the Wild
*5. Duel of Death
*6. Greenie Hatches
*7. Into the Woods
*8. The Everglade
*9. Humoresque
*10. Waltz of Happiness
*11. The Guard Ducks Anthem
*12. Finding Greenie
*13. Escape from the Farm...Again
*14. The Weasel Returns
*15. Autumn/The Flock Returns
*16. Mommy Will Be Watching
*17. Flying Race
*18. Flying Race (Victory)
*19. Mothers Love
*20. I Wonder...Will I Ever Fly?
*21. Changing of the Guard
*22. Melody of the Wind

==Box office== Wonderful Days (2003), Oseam (2003), Aachi & Ssipak (2005), and Yobi, the Five Tailed Fox (2006) which all performed dismally at the box office. No domestic animated film had ever achieved 1 million viewers.     

Undaunted by the knowingly discouraging prognoses from industry insiders, Leafie went on to rake in more than 2.2 million tickets, while recouping its production budget in just four weeks.      It has become South Koreas most successful animated film since the countrys first feature-length cartoon, A Story of Hong Gildong (1967).   

==Awards== Best Animated Feature Film at the 2011 Asia Pacific Screen Awards held in Australia. 

For her contribution to the popularization of Korean animation, producer Shim Jae-myung (a.k.a. Jaime Shim) CEO of Myung Films won a Special Mention at the 2011 Korean Association of Film Critics Awards.  

==Distribution rights==
Racking up numerous sales at the American Film Market, the film was picked up by Toronto-based 108 Media Group for distribution in English-speaking countries including Canada, U.S., Australia, U.K. and New Zealand. Leafie also sold to German-speaking territories via Ascot Elit and Brazil’s Conquest Filmes. It has so far secured deals for 46 countries around Asia, Europe and the Middle East after going on sale at Busan’s Asian Film Market, Cannes’s MIPCOM and Rome’s Business Street. 

It became the first Korean animated film to play at Chinese theaters, opening at 3,000 screens, which is over one-third of the countrys total. Clearly elated at the films warm reception by the Chinese media, director Oh said he hoped this breaks new ground as investors traditionally judge projects based on their appeal to viewers in English-speaking countries. "Its time to change the standard," he said. "In the future, Ill focus more on the cultural values of the animation rather than giving too much emphasis to the business aspect." 

==Stage adaptation==
A stage play based on the book and film ran from June 22 to September 2, 2012 at the COEX Art Hall in Seoul. 

==References==
 

==External links==
*   
* 
* 

 
 
 
 
 
 
 
 
 