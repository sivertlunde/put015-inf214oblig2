The Cowboy's Flute
{{Infobox film
| name = The Cowboys Flute
| image = CowboysFlute.jpg
| caption =
| imdb_rating =
| director = Te Wei, Qian Jiajun
| producer =
| writer =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1963
| runtime = 20 mins
| country = China
| language = none
| budget =
| preceded_by =
| followed_by =
| mpaa_rating =
| tv_rating =
}} Chinese animated short film produced by Shanghai Animation Film Studio under the master animator Te Wei.  It is also referred to as The Cowboys Flute, The Cowherds Flute, The Buffalo Boy and his Flute.

==Background==
The film does not contain any dialogues, allowing it to be watched by any culture.  The animation is essentially Chinese painting in motion,  with a heavy emphasis on the flute melody.

==Story== water buffalo, and falls asleep in a tree.  Soon he is dreaming that he has lost his buffalo. The dream sequence is delightfully whimsical, beginning with falling leaves that turn into butterflies and gradually lead the cow herder to a beautiful mist-filled valley. Here the buffalo refuses to budge from his hiding spot, leaving the cow herder to find an alternate musical solution to his problem.

==DVD==
The DVD has been re-released under the Chinese Classic Animation Te Wei Collection set.

==Awards==
More than a decade later, the film would win an award at the Odense International Film Festival in 1979.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 