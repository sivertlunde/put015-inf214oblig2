Cry Woman
{{Infobox film
| name           = Cry Woman
| image          = Cry_Woman.jpg
| caption        = 
| director       = Liu Bingjian
| producer       = Jason Chae Ellen Kim Michel Reilhac Deng Ye
| writer         = Liu Bingjian Deng Ye
| starring       = Liao Qin Wei Xingkun Zhu Jiayue Li Longjun
| music          = Dong Liqiang Xu Wei Ying Zhou
| distributor    = Mirovision
| released       =  
| runtime        = 90 minutes
| country        = China
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 哭泣的女人
| fanti          = 哭泣的女人
| pinyin         = Kū qì de nǔ rén}}
}} Chinese film Men and Women, Cry Woman was not given permission to screen in China. 

Cry Woman stars the Beijing Opera star Liao Qin in her first film role. 

Despite being banned in China, the film was screened at several international venues, including the Karlovy Vary International Film Festival held in the Czech Republic  and in the Un Certain Regard section at the 2002 Cannes Film Festival.   

== Plot ==
Wang Guixiang (Liao Qin) and her husband (Li Longjun) are a migrant couple living in Beijing. Wang is eking out a career selling pirated DVDs when disaster strikes and police confiscate her DVD stocks, and her husband is arrested after getting into a fight over a mahjong game. Forced to return to Guizhou, she meets an old boyfriend (Wei Xingkun), who suggests she take a job as a professional mourner. Surprisingly, Wang finds herself very good at her new job as a "cry woman" and soon discovers that her talents are very much in demand.

== Cast ==
* Liao Qin
* Li Longjun
* Wei Xingkun
* Wen Qing
* Zhu Jiayue

== References ==
 

== External links ==
* 
* 
*  at the Chinese Movie Database
*    (French release)

 
 
 
 
 
 
 


 
 