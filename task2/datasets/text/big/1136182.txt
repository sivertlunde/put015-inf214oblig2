The Secret Garden (1993 film)
 
{{Infobox film
| name           = The Secret Garden
| image          = Secretgarden1993.jpg
| image_size     = 250px
| alt            = 
| caption        = Theatrical release poster
| director       = Agnieszka Holland
| producer       = Fred Fuchs Tom Luddy Fred Roos
| screenplay     = Caroline Thompson
| based on       =   John Lynch
| music          = Zbigniew Preisner
| cinematography = Roger Deakins Jerzy Zielinski
| editing        = Isabelle Lorente
| studio         = American Zoetrope 
| distributor    = Warner Bros. Family Entertainment
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $31,181,347
}} British Drama drama fantasy John Lynch novel of the same name by Frances Hodgson Burnett. 

==Plot synopsis==
 
The recently-orphaned Mary Lennox travels from her home in India to her uncle Archibald Cravens hundred-room house, Misselthwaite Manor, in Yorkshire, England. Mary, materially spoiled but emotionally neglected by her late parents who had been killed in an earthquake, is rather unpleasant and unhappy in her new surroundings. Martha, a Yorkshire girl working as a maid, and her brother Dickon Sowerby, a boy who can "talk" to animals, befriend and help her to heal and grow. She discovers her deceased aunts secret garden, which has been locked for ten years and enlists Dickon to help her bring it to life.

Hidden away in the gloomy house is Marys cousin Colin, who has been treated all his life like a fragile, sickly invalid. This exaggeration has augmented what smaller problems he did have, turning him into a demanding, short-tempered, helpless boy. Mary, defying the orders of Mrs Medlock (who runs Misselthwaite), discovers Colin and is taken aback by his difficult nature, but reaches out to him anyway. Soon Colin, Mary, and Dickon all spend their time in the secret garden. They perform "magic"; barefoot, they  make a large bonfire and play a flute, dance round it, and chant, while Colin asks the magic to send his father. Colin learns to walk and gets quite well, which his father Archibald discovers upon his return to Misselthwaite.

==Cast==
* Maggie Smith as Mrs. Medlock
* Kate Maberly as Mary Lennox
* Andrew Knott as Dickon Sowerby
* Heydon Prowse as Colin Craven John Lynch as Lord Archibald Craven (Colins Father)
* Irène Jacob as Mrs. Lennox (Marys Mother) / Lilias Craven (Colins Mother)  Colin Bruce as Major Lennox (Marys Father)
* Laura Crossley as Martha Sowerby
* Walter Sparrow as Ben Weatherstaff

==Production==
Yorkshires imposing Allerton Castle stood in for most of the exterior shots of Misselthwaite Manor, and some of the interior was also used. Fountains Hall was also used for part of the Manors exterior. 

Interiors of the former Midland Grand Hotel were used for filming as well, notable the scenes on the grand staircase.

Holland was already internationally famous as a director before the making of the film; in making this film she continued to work outside of Poland. 

==Soundtrack== Winter Light.  Opera star Sarah Brightman and the youngest member of Celtic Woman, Chloë Agnew covered this song for their albums; Brightmans Classics (Sarah Brightman album)|Classics and Agnews Walking In The Air.

The soundtrack, released by Varèse Sarabande, contains the original score. 

==Reception==
Since its 1993 release, the film has garnered exceptionally positive reviews and currently holds an 85% "fresh" approval rating on the Rotten Tomatoes website, based on 40 reviews. 

According to Box Office Mojo, the film has a domestic gross of $31,181,347. 

==Awards and nominations==
Award wins Los Angeles Film Critics Association Award for Best Music – (Zbigniew Preisner)
Award nominations BAFTA Award for Best Actress in a Supporting Role - (Maggie Smith)

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 