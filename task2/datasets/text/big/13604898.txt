Jungle Woman
 
{{Infobox film
| name           = Jungle Woman
| image          = Junglewomanposter.jpg
| image size     =
| director       = Reginald Le Borg
| writer         = Henry Sucher Bernard Schubert
| starring       = Evelyn Ankers J. Carrol Naish Samuel S. Hinds Lois Collier Milburn Stone Douglass Dumbrille Acquanetta
| producer       =
| cinematography = Jack MacKenzie
| distributor    = Universal Pictures
| released       =  
| runtime        = 61 minutes
| language       = English
| budget         =
}}
Jungle Woman is a 1944 horror film released by Universal Pictures. The movie features Evelyn Ankers, J. Carrol Naish, Samuel S. Hinds, Lois Collier, Milburn Stone, Douglass Dumbrille, and Acquanetta. This is, in effect, a sequel to Captive Wild Woman and was followed by The Jungle Captive.

==Plot==
The film begins with shadowy images on a wall showing a man being attacked by what appears to be a woman. He produces a needle from his pocket and administers its contents to his assailant. She falls to the ground, apparently dead. 

The scene shifts to the coroner’s inquest into the death of Paula Dupree (Acquanetta), allegedly at the hands of Dr. Carl Fletcher (J. Carrol Naish). Induced to reluctantly tell his story by the coroner (Samuel S. Hinds) and district attorney (Douglas Dumbrille), Fletcher admits that he killed Paula. Yet, he also offers that there is more to the tale than what is on the surface. 

Via flashbacks, it is learned that Fletcher was present on the opening night of the Whipple Circus and witnessed Cheela the Gorillas heroic act that saved the life of Fred Mason (Milbern Stone). After her supposed demise, the doctor acquires the body of the beast. He detects a heartbeat, and revives the animal. Fascinated by the creature, he purchases the late Dr. Walters’ estate, hoping to find the records of his experiments. 

Apparently having no need of more glandular material or another brain, Cheela returns to human form. She is at first unable to speak and Fletcher diagnoses her condition as being due to shock. However, upon the arrival of his daughter Joan (Lois Collier) and her fiancée Bob Whitney (Richard Davies), the girl suddenly becomes verbal identifying herself as Paula. 

Enamored with Bob, Paula’s jealous streak resurfaces. During a moonlight canoe ride, an unseen attacker capsizes Joan and Bob. Discussing the episode with Dr. Fletcher, they believe that one of the other patients Willie (Edward M. Hyans, Jr.) is responsible. Unbeknownst to them, Willie has already paid the supreme price for his constant harassment of Paula. 

Later, meeting with Bob in private, Paula shows him bruises on her shoulder, injuries she claims were inflicted by Dr. Fletcher. Meanwhile, the caretaker for the estate has made the doctor aware of the vicious killing of his dog and a flock of chickens. Armed with the broken lock to the henhouse, Dr. Fletcher confiscates Paula’s perfume bottle as well. After he returns to his study, Paula makes an attempt on his life. Just as he pushes her to the floor, Bob enters the room. Misunderstanding what he has seen, he takes Paula to another doctor for an examination.

Dr. Fletcher has the lock and perfume bottle analyzed by a fingerprint expert, learning that although different in size, the prints do indeed match. He is now convinced that Paula and Cheela are one and the same. 

Bob has Paula examined by Dr. Meredith (Pierre Watkin), who does detect mental instability and extreme physical strength. When told she is already under the care of a doctor, he admonishes the young man to return her at once to her attending physician. 

Arriving back at Crestview, Dr. Fletcher is met with the news that Willie’s mangled body has been found. He confides in Joan what he knows about Paula, and expresses his concern for Bob. 

Paula and Bob return as well, and the latter finally tells her that he is going to marry Joan, which visibly angers her. Joan rushes to meet him as Paula disappears in the shadows. Learning the truth about the girl, he sends his fiancée to her cabin while he goes to the aid Dr. Fletcher. 

After a search of Paula’s room fails, Dr. Fletcher tells Bob to check the rest of the house. Brandishing a hypodermic needle filled with a sedative, the physician heads outside to continue his search. 

Paula pursues Joan through the woods to the cottage. Attempting to gain entrance, she hears the approaching Dr. Fletcher. She attacks him, and in the struggle, he accidentally administers a fatal overdose.

The flashbacks conclude and the D.A. expresses his disbelief. The Coroner parades the group, including the jury, to the morgue to re-examine the body of Paula Dupree. She is found to have reverted in death to the form of a half-human, half-ape monster. Dr. Fletcher is exonerated.

==Cast==
* Acquanetta
* Evelyn Ankers
* J. Carrol Naish
* Samuel S. Hinds
* Lois Collier
* Milburn Stone
* Douglass Dumbrille
* Richard David
* Nana Bryant
* Pierre Watkin
* Christian Rub
* Alec Craig
* Eddie Hyans Tom Keene
* Clyde Beatty

==Sequel==
Jungle Woman was later followed by The Jungle Captive.

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 