Everyday Sunshine: The Story of Fishbone
 
 
{{Infobox film
| name           = Everyday Sunshine: The Story of Fishbone
| image          = 
| caption        = 
| directors      = Lev Anderson Chris Metzler
| producers      = Lev Anderson Chris Metzler Norwood Fisher Angelo Moore 
| narrator       = Laurence Fishburne
| music          = Fishbone
| editing        = Jeff Springer   
| distributor    = Pale Griot Film
| released       =  
| runtime        = 107 minutes      
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Everyday Sunshine: The Story of Fishbone is an award-winning    2010 documentary film about the U.S. alternative rock band Fishbone.       Co-produced  and co-directed by Lev Anderson and Chris Metzler,       and narrated by actor Laurence Fishburne,     Everyday Sunshine premiered during the 2010 Los Angeles Film Festival, on 19 June 2010.       The film was released by Pale Griot Film,  and was made to coincide with Fishbones 25th  anniversary.     It was released on iTunes in February 2012 to coincide with Black History Month. 

==Synopsis== rock documentary Norwood Fisher George Clinton, animated Flashback flashbacks in the visual style of Fat Albert and the Cosby Kids. 

==Reception==
The film was critically acclaimed.  At   it has a 69/100 score, based on 16 reviews.    It won the Best Documentary award at the 2011 DC Independent Film Festival,  and was a Best Feature Documentary nominee at the 2011 Guam International Film Festival.   

==References==
 

==External links==
*  
*  
*  
*  
*   at The New York Times

 

 
 
 
 
 