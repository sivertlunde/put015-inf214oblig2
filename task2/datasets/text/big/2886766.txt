Kickboxer (1989 film)
{{Infobox film
| name           = Kickboxer
| image          = Kickboxer_poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Mark DiSalle David Worth
| producer       = Mark DiSalle
| screenplay     = Glenn A. Bruce
| story          = Mark DiSalle Jean-Claude Van Damme
| starring       = Jean-Claude Van Damme Dennis Alexio Dennis Chan
| music          = Paul Hertzog
| cinematography = Jon Kranhouse
| editing        = Wayne Wahrman Kings Road Entertainment
| distributor    = The Cannon Group
| released       =  
| runtime        = 103 minutes  
| country        = United States
| language       = English
| budget         = $1.5 million 
| gross          = $14,697,005   
}} cult classic and one of the definitive works in Van Dammes movie career as a martial artist. It was so successful that it spawned several sequels. 

==Plot== Bangkok to paralyzed from the waist down. However, Eric is carried outside and left on the street without any help, until Winston (Haskell Anderson), a retired American special forces member, intervenes and agrees to take the brothers to the hospital.

Furious, Kurt vows to avenge his brother and defeat Tong Po in the ring. Though reluctant at first, Winston eventually tells him about Xian Chow (Dennis Chan), a locally famous trainer living in a remote area of Thailand. Upon locating Xian, Kurt is able to convince him to train him in the art of Muay Thai ("Thai boxing"). While training, Kurt attempts to foil the operations of a group of Thai mobsters led by Freddy Li, who continuously threaten and steal money from Xians niece, Mylee (Rochelle Ashana). After Kurt makes short work of the thugs in a bar fight, Xian is able to arrange a match with Tong Po on Kurts behalf. It is determined that they will fight in the "ancient way", in which both fighters wrap their hands in hemp rope, which is then coated in resin and dipped in broken glass to make them deadly weapons.

In the days leading up to the match, Mylee is beaten and raped by Tong Po while Eric is kidnapped by Freddy Lis henchmen for the purpose of blackmailing Kurt into losing the fight. To save his brothers life, Kurt is instructed to go the distance with Po before losing the match. He endures a torturous beating, but fortunately, Xian and Winston are able to locate and rescue Eric before the fight concludes. With his brother free from danger, Kurt manages to find a second wind and ultimately defeat Tong Po and also kicks Freddy Li.

==Cast==
* Jean-Claude Van Damme as Kurt Sloane
* Dennis Alexio as Eric Sloane
* Dennis Chan as Xian Chow
* Michel Qissi as Tong Po
* Ka Ting Lee as Freddy Li
* Rochelle Ashana as Mylee Haskell Anderson as Winston Taylor
* Richard Foo as Tao Lin
* Ricky Liu as Big Thai man
* Africa Chu as Messenger
* Joann Wong as Tao Lius wife
* Louel Pio Roda as Lexls husband
* Mathew Cheung as Surgeon

==Production== ruins of Ayutthaya city in Thailand. Principal photography lasted 56 days on location in Bangkok, Thailand, between June 28 and August 23, 1988.

==Soundtrack==
A soundtrack containing songs from the movie was released featuring songs from soundtrack specialist Stan Bush. The score for the movie was composed by Paul Hertzog. The full score was remastered and released in 2006 by Perseverance Records in limited quantity.

The track listing is as follows on the 2006 full score CD.
# "To the Hospital / Well See" (01:15)
# "Groceries" (01:47)
# "Very Stupid" (00:45)
# "Tai Chi" (02:55)
# "First Kiss" (00:53)
# "Stone City" (02:34)
# "Second Stone" (00:53)
# "Hospital" (02:21)
# "Palm Tree" (00:30)
# "Advanced Training" (01:49)
# "Ancient Voices" (02:08)
# "Mylee Is the Way" (01:32)
# "Warriors" (00:45)
# "Buddhas Eagle" (01:01)
# "Kidnap" (01:01)
# "Youve Done It Before" (01:45)
# "Downstairs" (00:54)
# "Round One" (02:12)
# "Round Two" (01:36)
# "The Hook" (01:32)
# "Round Three" (01:32)
# "The Eagle Lands" (04:02)

The 2006 official score release does not include a previously released version of the score track titled "Buddhas Eagle" which was released on the Best of Van Damme Volume 2 Compilation CD.

On July 2, 2014, an expanded version of the 2006 album was released by Perseverance Records. This album contained the remastered original 22 tracks plus 9 vocal performances that previously had only been available in Germany. 

==Box office and reception==
Kickboxer is considered a box office success, as it grossed $14,697,005 in the domestic box office  based on a $1.5 million budget.

Chris Willman of the Los Angeles Times called the film "egregiously dull" and a contender for one of "the dumbest action pictures of the year", citing its "jarring shifts in tone, insurmountable plot implausibilities, rampant racial stereotyping, superfluous nudity and inhuman amounts of comically exaggerated violence". Willman also questioned the manner in which characters seem to recover from serious injuries and major trauma. 

Chris Hicks of the  . In addition to stating that the ending was predictable, Hicks also dismissed Van Damme as "little more than a low-budget Arnold Schwarzenegger Wannabee" whose attempts at acting were in vain. 

There is currently no consensus on the review aggregator website Rotten Tomatoes, though it has a 65% "fresh score" generated by 50,913 users. 

The internationally acclaimed theater work and film Your brother. Remember? by Zachary Oberzan transplants the plot of Kickboxer to examine a true-to-life brother story, using 20 year old film footage of the Oberzan brothers re-enacting Kickboxer, and re-re-enacting these scenes very precisely in the present day.  Filmmaker/actor Oberzan also re-enacts the infamous improvised monologue of JCVD (film), suggesting a universality between the sufferings of our heroes and ourselves. 

==Home media== Region 1. On 6 January 2003, DVD was released by Prism Leisure Corporation at the United Kingdom in Region 2.

==Sequels==
The film spawned several sequels.  Despite Van Damme not returning, the film series between parts two and four continues the ongoing battles between the Sloan family - expanded to include third brother David Sloan, essayed by Sasha Mitchell - and Tong Po.  The fifth entry is related in name only.

*  
*  
*  
*  

A new film titled The Kickboxer: City of Blood  which stars Sasha Mitchell, Dennis Chan, Clare Kramer and Julie Estelle and is directed by Albert Pyun.  

==Remake/reboot==
 
Film is currently in production. 

==References==
 

==External links==
*  
*  
*  
*    

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 