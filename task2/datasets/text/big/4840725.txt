Yawara!
 
{{Infobox animanga/Header name = Yawara! image =   caption = The cover of Yawara volume 1. genre = Comedy, Genre fiction#Romance|Romance, Sports
}}
{{Infobox animanga/Print type = manga author = Naoki Urasawa publisher = Shogakukan demographic = Seinen manga|Seinen magazine = Big Comic Spirits first = 1986 last = 1993 volumes = 29 volume_list = 
}}
{{Infobox animanga/Video type = live film director = Kazuo Yoshida producer = Yukio Sakamoto Hitoshi Ogura Toshimine Kobayashi Hiro Oda writer = Ikuo Sekimoto music = Ken Yajima Kaname Kato studio = Toho Doga Mycal Group released = April 15, 1989  runtime = 97 Minutes
}}
{{Infobox animanga/Video type = tv series title = Yawara! A Fashionable Judo Girl! director = Hiroko Tokita producer = Masao Maruyama Michihiko Suwa Satoshi Suzuki writer = Hiroyuki Kawasaki Toshiki Inoue Yoshimasa Takahashi Yoshiyuki Suga music = Hideharu Mori Yasuo Uragami studio = Madhouse Kitty Films network = Yomiuri TV (Nippon Television Network System|NNS) licensee = {{english anime licensee
 | NA = AnimEigo (2006-2012)
}} first = October 16, 1989 last = September 21, 1992 episodes = 124 episode_list = List of Yawara! episodes
}}
{{Infobox animanga/Video type = ova title = Yawara! Soreyuke Koshinuke Kizzu director = Hiroko Tokita producer = Masao Maruyama Michihiko Suwa Tatsuya Mukai writer = Toshiki Inoue music = Yasuo Uragami studio = Madhouse Kitty Films released = August 1, 1992 runtim = 65 minutes
}}
{{Infobox animanga/Game developer = Sofix publisher = Sofix genre =  platforms = PC Engine Super CD-ROM² released = October 1, 1992
}}
{{Infobox animanga/Game title = Yawara! 2 developer = Sofix publisher = Sofix genre =  platforms = Super CD-ROM² released = September 23, 1994
}}
{{Infobox animanga/Video type = film title = Yawara! Special - Zutto Kimi no Koto ga director = Morio Asaka producer = Masao Maruyama Michihiko Suwa Toshio Nakatani writer = Toshiki Inoue music = Hideharu Mori studio = Madhouse Kitty Films released = July 19, 1996 runtime = 110 minutes
}}
 

Yawara! (also stylized as YAWARA!) is a Japanese manga series by Naoki Urasawa which ran in Big Comic Spirits from 1986 to 1993. In 1990, it received the 35th Shogakukan Manga Award for general manga.    It was adapted in 1989 by Toho as a live action movie directed by Kazuo Yoshida, starring Yui Asaka in the main role and singing the main theme, "Neverland." 
 Yomiuri TV Madhouse studio, aired on Japanese television contemporary with Kittys Ranma 1/2 but achieved higher ratings than Ranma 1/2, despite the latter series being more well-known outside Japan. AnimEigo licensed the TV series for North American distribution in August 2006.  However, as of April 2010 AnimEigo has been unable to license the remaining episodes of the TV series for North American distribution. 

==Story== 1992 Olympic Games. Because of the pressure from her grandfather, she has a generally bad attitude about judo, avoiding it as much as she can. However, as the manga continues, she begins to understand why her grandfather loves judo, and she begins to appreciate it more.

==Characters==
; 
: 
:Yawara is a girl who aspires to an ordinary life but who is forced to practice judo by her grandfather. She is always having romantic daydreams; her biggest dream being to find a boyfriend. (Inokuma means "Pig Bear"; Yawara is the same Chinese as "ju" in "judo.") Although she has a lot supporters and very close close friends such as Fujiko, sometimes she acts selfishly and does not appreciate their commitment toward her (e.g., almost failing to help Fujiko in her All-Japan Selection match).

; 
: 
:Jigoro is a 7th-dan Judo Master (though he often inflates this to 8- or even 9-dan) and five-time National Champion. His passion for judo is rivaled only by his love for Yawara and food (particularly sweets). He has great expectations for Yawaras judo career and is constantly pushing her to do her best and focus primarily on judo. His motto is "Judo was not built in a day." He does work on the side as a bone-setting doctor. He is a bit of a media hound as he often tries to hijack publicity events for Yawara to highlight his own life and achievements. He is often overbearing toward Yawara and is not above using mean tricks to get his way, either to thwart her goals and desires or to force her to do what he wants.

;Tamao
:Tamao is Yawaras mother. She is rarely home as she searches all over Japan for her husband Kojiro. Unlike Jigoro, she does not pressure Yawara to do judo, but she does not detest judo either because it was through judo that she met Jigoro. She tells Yawara that there are other ways to be strong other than judo and that its possible to be a judoka and be feminine as well.

;Kojiro
:Kojiro is Yawaras father. He disappeared soon after winning the 1974 Japan Judo Championship in his debut as an unknown judoka, supposedly training in secrecy in extreme environments (for 20 years?) without letting his family know his whereabouts.

; 
: 
:Sayaka is an extremely spoiled daughter of one of Japans richest families. She has never failed to dominate any sport shes cared to try. When Yawara shows her up, she decides to stop at nothing to defeat her and becomes her rival. She has a false tooth, which is a sore point for her, and one that Yawara keeps accidentally bringing up. She becomes Yawaras rival not only in judo but also for Shinnosukes affection.

; 
: 
:Shinnosuke is one of the finest judo coaches in all of Japan; he idolizes Jigorou and has been hired by Honami. Despite suffering from stage fright, he is an unbelievable babe-magnet and incorrigible womanizer thanks to his good looks and ability to flatter women with ease and superficial caring.

; 
: 
:A reporter working for a sports paper (Daily Every Sport), Matsudas becomes convinced Yawara is going to be the next great sports superstar in Japan. He is conflicted by his feelings for Yawara and his goal of making her into a judo superstar.

; 
: 
:A cameraman working for a sports paper, Kamoda seems to have a knack for taking "the perfect shot" for headlines. He never refuses any offer of food.

;Hanazono
: 
:Hanazono, the large, overly emotional captain of the judo club at Yawaras high school, becomes devoted to Yawara when she comes to his attention as a skilled judoka. He has a crush on Yawara and tries to be her chief protector, until he meets Fujiko and the two bond over their mutual enthusiasm for Yawara. He is later trained by Jigoro and becomes his colleges strongest judo player.

;Yuki Tohdoh
:Yuki is a heavyweight judoka who lost to Yawara in the latters "unofficial" debut, though Yawara was trying to lose the match. She outweighs Yawara by 20kg (44 lbs) and looks, talks, and eats like a sumo wrestler.

; 
: 
:Jody is the previous years heavyweight judo world champion (>72kg division); she is a Canadian who comes to challenge Yawara to a match part way into the series, becoming a friendly rival and regular character.

;Kim Yonsky
:Kim is the South Korean "secret weapon" whose judo incorporates Korean close-in wrestling. She and Sayaka have a judo match in which they come to an exhausting draw and pronounce mutual hatred.

;Anna Teleshikova
:Teleshikova is Soviet/Russian judo star who looks like a platinum robot with a butch haircut. She kicks Jody Rockwell in their first match and severely injures Jodys leg. Her habit is to study tapes intensively to figure out her opponents weak point, which she attacks relentlessly.

;Belkins
:Belkins is the Belgian judo champion who is also a model, known as the Judo Queen. She loses to Yawara in the semifinals match at the Judo World Cup and crowns Yawara the "New Queen."

;Yuutenji 
:Yuutenji is the judo coach for Sakai College, reputed to be the best in judo. He has been scheming with Jigoro to recruit Yawara.

;Kaga Kuniko
:Kuniko is a new photographer for the Daily Every Sport. She wears glasses, flaunts her big breasts with revealing clothing, and uses her ability to cry hysterically on command to get her way. She develops a crush on Matsuda and falsely pretends to be his "girlfriend" in front of others, repeatedly plotting to ruin the relationship between Matsuda and Yawara.

;Ito Fujiko
:Fujiko is a very tall student (180cm, a little over 511") at Mitsuba Womens College whose goal is to experience a "once-in-lifetime youth" and meet a wonderful man. She is somewhat gloomy and sensitive about her height, which she inherits from both sides of her family. She is not popular with men (until she meets Hanazono) and often ends up drinking alone at social gatherings. She dedicated her youth to ballet dancing until she grew too tall. She becomes Yawaras best friend at college. She organizes Mitsubas first judo club to help restore Yawaras passion for judo. Under Jigoros training, she becomes an excellent judoka herself due to her ballerina training, height, and hard work, but lacks confidence and psychological toughness as a competitor. The latter part of the show features her development as a judoka as much as Yawara.

;Komiya Yukari
:Komiya is a short-haired student at Mitsuba Womens College. She is flirtatious and a veteran dater. Her stated goal is hook a rich man for marriage. She is friends with Yawara and Fujiko but never joins the Mitsuba judo team.

;Minamda Yoko
:Yoko, aka "Paddy-field," joins the Mitsuba team out of frustration after being repeatedly dumped by men (13 when she joined; 19 when she graduated). She uses "(mans name) you jerk!" as a battle cry when she fights. She becomes a police officer after judo.

;Kikage Kyoto
:Nicknamed "Kyon-kyon" (the shadow), she is a small sickly woman whos usually "invisible" in gatherings. She weighs only 36kg (about 79lb). She joins Mitsuba judo to get stronger in health. Although weak, shes also brave and great with details by keeping notes on opponents and is able to absorb Yawaras instructions even without being taught any real moves by Jigoro. 

;Yoshinagawa Sayuri
:Sayuri joins Mitsuba judo to lose weight. Although not skilled, she often is able to use her weight to pin down opponents ("Thank God Im Fat!") and can take great punishment. She believes that each match reduces her weight by a few kg. People often mispronounce her surname.

;Oda Mari
:Nicknamed "Marilyn," she is the least serious of the Mitsuba judo club and is mostly seen preening for the camera. Often attracting mens attentions and sometimes molesters, she joins judo to fend them off. She is buxom and wears makeup and earrings to matches, causing her to be disqualified. She aspires to be an actress.

==Media==
===Manga===
The series was published in Big Comic Spirits from 1986 to 1993.

The series was collected into 29 volumes between June 1, 1987 and December 1, 1993.   A 19 volume edition was then released between July 17, 1998 and March 16, 1999.  

===Anime===
 

Yawara! was adapted into an anime series by Kitty Films. The series was broadcast on Yomiuri TV between October 16, 1989 and September 21, 1992. A total of 124 episodes were produced. 

The series was licensed for release in North America by Animeigo. Preorders were opened in early 2008.  A box set of the first 40 episodes was released on October 31, 2008  and the boxset went out of print on August 31, 2012. 

==Reception==
The significance of the 1992 Barcelona Olympics in the story is that in the real world, this was the first time that Womens Judo would be a full competition event and would thus see the awarding of the first Olympic gold medal for Womens Judo. 

Yawara! was very popular in Japan, so when real life Japanese teenager Ryoko Tamura won a silver medal for judo at the 1992 Barcelona Olympics, she was seen as a real-life Yawara (her age, stature, and ability all being strikingly similar to those of the fictional character) and promptly nicknamed "Yawara-chan".  She was still known by this name eight years later,  indicating perhaps the enduring popular recognition of the series as well as that of Ryoko Tamura.

==References==
 

==External links==
*  
*   
*   
*  
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 