The Forgotten Pistolero
{{Infobox film
| name = The Forgotten Pistolero
| image = The-forgotten-pistolero-poster.jpg
| caption =
| director = Ferdinando Baldi
| writer = Leonard Mann Luciana Paluzzi
| music = Roberto Pregadio
| cinematography = Mario Montuori
| editing = Eugenio Alabiso
| producer = Manolo Bolognini
| distributor =
| released =   	
| runtime = 91 minutes
| awards =
| country = Italy
| language = Italian
| budget =
}} 1969 Cinema Italian Spaghetti western written and directed by Ferdinando Baldi. The movie is a western adaptation of the Greek myth of Orestes, subject of three famous drama-plays by Aeschylus, Sophocles and Euripides.   Ulrich P. Bruckner puts it among the "most interesting and most touching spaghetti westerns of the late sixties". 

== Plot ==
When he returns home from war the Mexican general Juan Carrasco is killed by the lover of his wife Anna. The victims children run away with their nanny but fifteen years later they come back for revenge. Anna and Tomas want to have them killed but their henchmen fail them. It turns out Anna is not the real mother of the dead generals children.

== Cast == Leonard Mann: Sebastian Carrasco 
* Luciana Paluzzi: Anna Carrasco 
* Peter Martell: Rafael Garcia  
* Alberto de Mendoza: Tomas 
* Pilar Velázquez: Isabella Carrasco 
* Piero Lulli: Francisco 
* Luciano Rossi: Juanito 
* José Suárez: General Juan Carrasco 
* Barbara Nelly: Conchita
* Enzo Fiermonte: Friar 
* José Manuel Martín: Miguel

==Releases== The Unholy Four.

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 