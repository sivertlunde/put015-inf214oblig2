Afternoon Delight (film)
 
{{Infobox film
| name           = Afternoon Delight
| image          = Afternoon Delight poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Jill Soloway
| producer       =  
| writer         = Jill Soloway
| starring       = Kathryn Hahn Juno Temple Josh Radnor Jane Lynch
| music          = Craig Wedren
| cinematography = Jim Frohna
| editing        = Catherine Haight
| art direction       = Jean-Paul Leonard 72 Productions Rincon Entertainment
| distributor    = Film Arcade
| released       =  
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $174,496 
}}
Afternoon Delight is a 2013 American comedy-drama film written and directed by Jill Soloway.  The film stars Kathryn Hahn, Juno Temple, Josh Radnor, and Jane Lynch.

==Plot==
Rachel (Kathryn Hahn) is a mother living in an unhappy life, frustrated by the roles of being a stay-at-home mom and not having had sex with her husband Jeff (Josh Radnor) for months. She visits her therapist, Lenore (Jane Lynch) but is unable to find any help in her advice. 

Looking to spice up their relationship, they go to a strip club, where Rachel sees McKenna (Juno Temple). Jeff buys her a private lap dance from McKenna; Rachel finds out that McKenna is only 19. But afterwards, Rachel and Jeff continue not having sex. 

Rachel follows a mobile coffee hut on Twitter, where she goes to buy a drink. At the hut, she sees McKenna and they start talking. She introduces herself and they become friends, having coffee together regularly. One day, Rachel finds McKenna thrown out of her residence, and now homeless, so she invites her to stay at her large house. While Jeff is less than happy, Rachel does not feel that she can kick her out, as she feels she can help McKenna get out of being a stripper. She finds out that McKenna is a sex-worker, who has clients she sees regularly.

Rachel starts teaching McKenna to nanny her young son Logan. When Rachel is frustrated at a school event, she asks McKenna if she can go with her to see her client, Jack. When there, she watches the two of them have sex, and is horrified by what she sees. 

When asked by a friend if McKenna can babysit, Rachel changes her mind and says she doesnt want her to. McKenna is upset by this, as she went through a lot of effort buying things for the girls party. While the women are out, and all the men are at Jeff and Rachels house, McKenna comes in and starts acting provocatively. She ends up sleeping with one of Jeffs friends, but his wife and Rachel walk in on them. McKenna is thrown out of their house.

Rachel tells Jeff that she wants out of this life, and out of her head, which he takes to mean he should leave. At a visit to Lenore, Rachel comforts Lenore when she starts crying and telling her how her partner had left, saying "I dont want to start all over again." That night, Rachel goes to Jeff (he is staying in the garage of one of his friends) and they reconcile, being happier than ever.

One day while driving, Rachel sees McKenna on the street, and starts to stop but changes her mind. She tells her friend that she had nothing to say to her.

The film ends with Rachel and Jeff being happy together, and Rachel playing with her son Logan.

==Cast==
* Kathryn Hahn as Rachel
* Juno Temple as McKenna
* Josh Radnor as Jeff
* Jane Lynch as Lenore
* Jessica St. Clair as Stephanie
* Michaela Watkins as Jennie
* Josh Stamberg as Matt
* John Kapelos as Jack
* Keegan Michael Key as Bo
* Annie Mumolo as Amanda

==Release== premiered at the 2013 Sundance Film Festival.    The film was given a limited release on August 30, 2013.   

==Reception==
Afternoon Delight garnered mixed to positive reviews; from 69 critics, it holds a 6.2 rating, a 67% "fresh" rating on review aggregator website Rotten Tomatoes.  On Metacritic, based on 21 critics, the film has a 48/100 rating, signifying "mixed or average reviews". 

===Accolades=== Directing Award (United States, Drama) at the 2013 Sundance Film Festival on January 26, 2013.

Filmmaker Quentin Tarantino included Afternoon Delight on his list of the Top Ten Films of 2013. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 

 