Chora Chittha Chora
{{Infobox film
| name = Chora Chittha Chora
| image = 
| caption =
| director = Subrahmanyam
| writer = K. Raghavendra Rao Malavika   Srinivasa Murthy 
| producer = Sa. Ra. Govindu
| music = V. Ravichandran
| cinematography = G. S. V. Seetharam
| editing = Shyam
| studio = Thanu Chitra
| released =  
| runtime = 132 minutes
| language = Kannada
| country = India
| budget =
}} romantic drama drama film Malavika in the leading roles.  
 Telugu film Allari Priyudu (1993) directed by K. Raghavendra Rao. The film was dubbed in Tamil as Yaarukku Mappillai Yaaro. The music was composed by Ravichandran to the lyrics of K. Kalyan.

== Cast ==
* V. Ravichandran 
* Namrata Shirodkar Malavika 
* Srinivasa Murthy
* Umashri
* Kashi
* Balaraj Sumithra
* Tennis Krishna
* Mandya Ramesh
* Bank Janardhan
* Shivaram
* Sarigama Viji
* Mimicry Dayanand

== Soundtrack ==
The music was composed by V. Ravichandran and lyrics were written by K. Kalyan and Shree Chandru.  A total of 8 tracks have been composed for the film and the audio rights brought by Jhankar Music.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Hey Dosth Helo Dosth
| extra1 = Badri Prasad, L. N. Shastry, Ravishankar Gowda
| lyrics1 = K. Kalyan
| length1 = 
| title2 = Malle Sooji Malle
| extra2 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics2 = K. Kalyan
| length2 = 
| title3 = O Igo Illondu Manaside
| extra3 = K. J. Yesudas, K. S. Chithra
| lyrics3 = K. Kalyan
| length3 = 
| title4 = Cheluvamma Chandamama
| extra4 = S. P. Balasubrahmanyam
| lyrics4 = Shree Chandru
| length4 = 
| title5 = Cheluvamma Premadamma
| extra5 = L. N. Shastry
| lyrics5 = Shree Chandru
| length5 = 
| title6 = Kachaguliya Kannavane
| extra6 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics6 = Shree Chandru
| length6 = 
| title7 = Dillu Dillu Seridaga
| extra7 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics7 = Shree Chandru
| length7 = 
| title8 = Hello Hello
| extra8 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics8 = Shree Chandru
| length8 = 
|}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 

 