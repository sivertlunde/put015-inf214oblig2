Raging Phoenix
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Raging Phoenix
| image          =
| caption        = 
| director       = Rashane Limtrakul
| writer         = Sompope Vejchapipat
| starring       = Yanin Vismistananda Kazu Patrick Tang
| released       =  
| country        = Thailand
| language       = Thai
}} Yanin "Jeeja" Vismistananda, in her second film performance. It is directed by Rashane Limtrakul, with martial arts choreography by Panna Rittikrai.

==Plot==

Deu (Jija Yanin) meets three masters of drunken martial arts—"Dog Shit", "Pig Shit", and Sanim, when they foil an attempt to abduct her.  She convinces them to train her in their martial arts style, and learns that they have come together to defeat the Jaguar Gang, who abduct "special" young women.  She falls for Sanim while he trains her—but learns that he is still in love with Pai—his fiancée who was abducted by the Jaguar Gang three years before, during what was supposed to be their wedding.

After rescuing more women using her newfound skills, Deu is twice tested by the until now hidden fourth member of the team—"Bull Shit".  She learns that The Jaguar gang only abducts women with a certain "smell", a smell that Deu has, which they use to make a special perfume—and that the secret of her new kung fu is not simply drunkenness, but emotional pain.  However, Kee Ma (Dog Shit) is a "Sniffer", and is able to find the smell. To locate out the Jaguar Gangs hideout, Deu is used as a decoy. It works too well, as Deu is abducted but the team fails to find the hideout where she was taken.

Deu, having been drugged, tries to escape and finds out the reason why the Jaguar gang abducts these specific women. The womens tears have a chemical substance which is able to improve health. Deu is then caught, but not before knocking some vials of the tears onto the floor which causes them to break. Kee Ma is able to smell the tears and shows the others into the hideout where they have a large battle ensues.

Sanim tries to take Deu away from the fighting but instead finds Pai. Before they are able to take Pai away, they meet the leader of the Jaguar Gang, London (Roongtawan Jindasing). They attempt to escape from London with the drugged and limp Pai, but London corners them near a sequence of rope bridges, and the real fight begins.  Finally, London is temporarily defeated, and Sanim and Pai are left dangling over the edge of a bridge with Deu holding onto both of them. Sanim then sacrifices himself for Pai and lets go of Deus hand. London recovers and takes both Deu and Pai to where the rest of the team is being beaten by two Jaguar Members. As they are knocked out, Deu rouses herself using the pain of Sanims passing, and fights with London and the other two members in turn, defeating each of them.

==Cast== Yanin "Jeeja" Vismistananda as Deu
*Kazu Patrick Tang as Sanim
*Nui Saendaeng as Kee Moo (Pig Shit)
*Sompong Lertwimonkaisom as Kee Ma (Dog Shit)
*Boonprasayrit Salangam as Kee Kwai (Bull Shit)
*Jindasee Roongtawan

==Martial art==
The main fighting style for the film, Meyraiyuth, is a fictional drunken style of B-boying infused Muay Thai which strongly resembles Capoeira.

==Reception==

The movie received mostly positive praise.  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 