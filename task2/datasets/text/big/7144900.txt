Making Friends (1936 film)
{{Infobox Hollywood cartoon|
| cartoon_name = Making Friends
| series = Betty Boop
| image = 
| caption =
| director = Dave Fleischer
| story_artist = 
| animator = Hicks Lokey Myron Waldman 
| voice_actor = Mae Questel 
| musician = Sammy Timberg (uncredited)
| producer = Max Fleischer 
| distributor = Paramount Pictures
| release_date = December 18, 1936
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}
Making Friends is a 1936 Fleischer Studios animated short film starring Betty Boop.

==Synopsis==
Pudgy the pup takes Betty Boops advice (Go Out and Make Friends With the World) to heart and befriends various wild animals.
 

==Notes==
* This is the first cartoon in which Bettys hair is not parted and will be like that to the end of the series.

 
 
 
 
 


 