Temptation Harbour
 
 
{{Infobox film
| name           = Temptation Harbour
| image          = Temptation Harbour (film).jpg
| image_size     = 200px
| caption        = Temptation Harbour
| director       = Lance Comfort
| producer       = Victor Skutezky
| writer         = Rodney Ackland, Frederick Gotfurt
| starring       = Robert Newton, Simone Simon, William Hartnell
| music          = Mischa Poliansky
| cinematography = Otto Heller
| editing        = Lito Carruthers
| distributor    = Pathe Pictures
| released       = 1947 United Kingdom 27 March 1949 (USA)
| runtime        = 91 minutes
| country        = United Kingdom English
| budget         =
| gross          = £132,235 
}}
 British black and white crime/drama film directed by Lance Comfort, released in 1947 based on the novel Newhaven-Dieppe by Georges Simenon.  The film was made at Welwyn Film Studios.

==Synopsis==
A signalman on a quay sees a fight between two men. One of the men is deliberately pushed into the water and the signalman cannot save him, but decides to keep his suitcase which later finds is full of banknotes with a value of £5000. 

===Cast list===
:(in credit order)
*Robert Newton as Bert Mallinson
*Simone Simon as Camelia
*William Hartnell as Jim Brown
*Marcel Dalio as Insp. Dupré
*Margaret Barton as Betty Mallinson
*Edward Rigby as Tatem
*Joan Hopkins as Beryl Brown
*Kathleen Harrison as Mabel
*Leslie Dwyer as Reg
*Charles Victor as Gowshall
*Irene Handl as Mrs. Gowshall
*Wylie Watson as Fred
*John Salew as CID Inspector George Woodbridge as Mr. Frost
*Kathleen Boutall as Mrs. Frost

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 

 