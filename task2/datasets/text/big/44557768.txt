Aadaalla Majaka
{{Infobox film
| name           = Aadaalla Majaka
| image          = 
| caption        =  Vikram Sivaranjani Ooha Betha Sudhakar
| director       = Muthyala Subbaiah
| writer         = Muthyala Subbaiah
| producer       = K. Padmavathi
| cinematography = K. V. Ramanan
| editing        = Madhu
| dialogues      = 
| music          = Raj Bhaskar
| distributor    = 
| studio         = 
| released       = 20 October 1995
| runtime        = 136 mins Telugu
| country        = India
}}
 Telugu romantic Ooha and Ali in Sudhakar plays a supporting role.

== Cast == Vikram as Vikram Ooha as Bhanurekha Sudhakar
* Ali
* Brahmanandam
* Ajay Rathnam

==Release== Simran added to the film.

== Soundtrack ==
{{Infobox album
| Name        = Aadaalla Majaka
| Lontype     = 
| Type        = Soundtrack
| Artist      = Raj Bhaskar
| Producer    =  Feature film soundtrack Telugu
| Last album  = 
| This album  = 
| Next album  = 
}}

The soundtrack album was composed by Raj Bhaskar. 

Tracklist
{{track listing
| extra column    = Singer(s)
| lyrics_credits  = 
| total_length    =

| title1       = Auntylu Auntylu
| length1      =

| title2       = Meesum Puttina 
| length2      =

| title3       = Mudduala Maa Chinnayya

| title4       = Mukkala Mukkabula

| title5       = Pacha Pachani Pavada
}}

== References ==
 

 

 
 
 
 


 