Freejack
 
 
{{Infobox film
| name        = Freejack 
| image       = Freejack.jpg
| director    = Geoff Murphy
| producer    = Ronald Shusett Stuart Oken
| screenplay  = Steven Pressfield Ronald Shusett Dan Gilroy
| based on    = Immortality, Inc. by Robert Sheckley
| story       = Steven Pressfield Ronald Shusett
| starring    = Emilio Estevez Mick Jagger Rene Russo Anthony Hopkins Jonathan Banks David Johansen Trevor Jones
| editing     = Dennis Virkler
| cinematography = Amir Mokri   Morgan Creek
| distributor = Warner Bros.
| released    =  
| runtime     = 110 minutes
| language    = English
| budget      = $30 million (estimated)   
| gross       = $17,129,000 (USA)
}} science fiction action film directed by Geoff Murphy, starring Emilio Estevez, Mick Jagger, Rene Russo and Anthony Hopkins. Upon its release in the United States, the film received mostly negative reviews. The story was adapted from Immortality, Inc., a 1959 novel by Robert Sheckley. Aside from the most basic elements – the journey of a modern man into a future where everything is for sale, and the presence of a "spiritual switchboard" in which souls are suspended – the cyberpunk plot bears little resemblance in tone or content to Sheckleys story, where discovery of scientific proof of the afterlife altered societys views of the sanctity of life.

==Plot== dystopic future, most people suffer from poor physical health as a result of rampant drug use and environmental pollution, making them unattractive as replacement bodies.

In 1991, professional Formula One racer Alex Furlong (Emilio Estevez) is about to die in a spectacular crash witnessed by his fiancé Julie Redlund (Rene Russo) and agent Brad Simmons (David Johansen). Just as the crash occurs, bonejackers from 2009 use a time displacement device to snatch him from the car and into a 21st-century laboratory. When Furlongs captors - led by mercenary Victor Vacendak (Mick Jagger) - are ambushed by a hit squad, Furlong escapes into the unfamiliar streets of a dystopian, post-disaster Manhattan. Having no idea what happened, he tries to find Julie, but she no longer lives at her 1991 address. He takes shelter at an old church, where a sympathetic (and well-armed) nun (Amanda Plummer) explains what has happened to him. She also provides him with clothing, food, and a pistol and sends him on his way.

Furlong finds Brad, but Brad immediately sells him out to Vacendak, getting killed in the process. He then finds Julie at her new apartment, but she calls security, believing him to be "the bastard who stole   body." When Vacendak and his men shows up, she realizes that Furlong is actually her Furlong. She goes to her boss Ian McCandless (Anthony Hopkins), president of McCandless Corp., to try and help Furlong escape the city. McCandless promises that he will help secure Alexs escape. However, it is a trap — and Vacendak and his men try again to capture Furlong, but he manages to fight off the mercenaries and hold Vacendak at gunpoint. Furlong demands that he be told who wants his body; Vacendak finally tells him that McCandless himself wants it. Because Furlong saved his life earlier, Vacendak gives him a chance to escape and Julie steals an armored vehicle used by the mercenaries.

Furlong and Julie manage to evade the mercenaries and McCandless personal police force. But they find they also have to deal with the private guards of McCandless corporate executive officer Mark Michelette (Jonathan Banks), who desperately wants McCandless job. His men were the ones responsible for the ambush that let Furlong escape in the first place. Furlong pretends to take Julie hostage and negotiates with Michelette. Furlong hopes they dont know about his past relationship with Julie. But, it turns out that Michelette saw footage of Julies grief after the 1991 accident. He immediately fires Julie on the spot. Furlong and Julie escape the meeting, hoping to flee through the lobby, but they are thwarted by a gunfight between the security guards and mercenaries.

Their elevator takes them to the penthouse, where McCandless mind is stored. A hologram of McCandless explains why he wants Furlongs body — so he could show his love for Julie. Apologizing, he offers to let Furlong run the company while pretending to be McCandless. Just then Vacendak arrives and McCandless reveals he was only stalling for time. Furlong, forced into the transfer machine, protests "You dont need a new body, you need a new soul, and your machine cant give you that!" Just as the transfer process begins, Michelette stumbles in, wounded from fighting Vacendaks soldiers. In the confusion, Julie grabs a soldiers pistol and shoots the processing crystal used by the transfer computers. The transfer results are deemed inconclusive since no one knows if McCandless controls Furlongs body. McCandless scientists cannot tell and conclude that McCandless will have to identify himself. Michelette agrees, revealing that only McCandless will know his own personal I.D. code — a code that Alex wouldnt know. Vacendak receives a handheld computer which displays McCandless code. He instructs "Furlong" to tell him what it is.

Alex responds by saying the code slowly, and Vacendak asks him to continue. Michelette concludes that the transfer was completed and tries to kill Furlong, but is gunned down by Vacendak and his men. Furlong remarks about how well he feels in his "new" body, before ordering Julie to dress more appropriately.

Sometime later, Furlong and Julie go for a drive. Vacendak stops them as the car leaves the estate. It turns out that the transfer was not complete after all; Furlong got McCandless secret number wrong, though Vacendak went along with it. He simply waited until Furlong made a mistake: McCandless did not know how to drive. Vacendak admonishes Julie that "youll have to coach him better than that", then leaves while Furlong and Julie speed away.

==Cast==
{| class="wikitable"
|-
!Actor
!Character
|- Emilio Estevez
| Alex Furlong
|- Mick Jagger
| Victor Vacendak
|- Rene Russo
| Julie Redlund
|- Anthony Hopkins
| Ian McCandless
|-
| Jonathan Banks
| Mark Michelette
|-
| Grand L. Bush
| Boone
|-
| David Johansen
| Brad
|}

==Production==
The role of Julie Redlund was originally going to be played by Linda Fiorentino, but due to scheduling conflicts, she dropped out and Rene Russo signed on to replace her.

==Reception==
The film received negative reviews from critics and currently holds a 15% rating at Rotten Tomatoes based on 15 reviews.    

==Home video releases==
Freejack was released on VHS in 1992,  with a DVD release following in 2002. 

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 