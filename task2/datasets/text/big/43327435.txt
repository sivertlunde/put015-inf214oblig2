The Trapp Family in America
{{Infobox film
| name           = The Trapp Family in America
| image          = Die Trapp-Familie in Amerika Poster.jpg
| alt            = Poster showing Ruth Leuwerik as Maria with New York City in the background
| caption        = German theatrical release poster
| director       = Wolfgang Liebeneiner
| producer       = {{Plainlist|
* Heinz Abel
* Ilse Kubaschewski
* Utz Utermann
}}
| screenplay     = Herbert Reinecker
| based on       =  
| starring       = {{Plainlist|
* Ruth Leuwerik
* Hans Holt
* Josef Meinrad
* Adrienne Gessner
}}
| music          = Franz Grothe
| cinematography = Werner Krien
| editing        = Margot von Schlieffen
| studio         = Divina-Film
| distributor    = Gloria
| released       =  
| runtime        = 103 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}}
The Trapp Family in America ( ) is a 1958 West German comedy drama film directed by Wolfgang Liebeneiner and starring Ruth Leuwerik, Hans Holt, and Josef Meinrad. It is a sequel to the 1956 film The Trapp Family. Reimer and Reimer 2010, p. 188.  The films art direction was by Robert Herlth.

==Plot==
The von Trapps have left Austria and are now in the United States.  But the Land of Unlimited Possibilities turns out to be anything but for our hapless heroes.  Though the American public has demonstrated countless times, that theyll pay anything to hear German folk songs and other pop songs, the von Trapps on the verge of being penniless and suicidal, thanks to Father Wasner, whos determined to teach Americans to appreciate great church music ... no matter how much his "cultural mission" pushes the von Trapps to starvation.  Only the insistence of paying patrons that they drop the holy roller music and the guffaws of the audience abandoning their shows finally convinces Maria, that its time to start entertaining the paying public and give Palestrina a rest. Eventually they receive critical acclaim and a large following for their music. Later, They purchase a farm in Vermont and decide to remain in America.

==Cast==
* Ruth Leuwerik as Baronin von Trapp
* Hans Holt as Baron von Trapp
* Josef Meinrad as Dr. Wasner
* Adrienne Gessner as Mrs. Hammerfield
* Michael Ande as Werner von Trapp
* Knut Mahlke as Rupert von Trapp
* Ursula Wolff as Agathe von Trapp
* Angelika Werth as Hedwig von Trapp
* Monika Wolf as Maria von Trapp
* Ursula Ettrich as Rosemarie von Trapp
* Monika Ettrich as Martina von Trapp
* Wolfgang Wahl  as Patrick
* Peter Esser as Mr. Hammerfield
* Till Klockow as Bronx-Lilly
* Holger Hagen as Mr, Harris

==References==
===Citations===
 

===Sources===
 
*  
*  
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 


 