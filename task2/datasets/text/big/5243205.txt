Kismet (1943 film)
 
 
{{Infobox Film
| name           = Kismet
| image          = kismet1943film.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Gyan Mukherjee
| producer       = Bombay Talkies
| writer         = Gyan Mukherjee
| narrator       = 
| starring       = Ashok Kumar Mumtaz Shanti Shah Nawaz Anil Biswas Kavi Pradeep (lyrics)
| cinematography = R.D.Pareenja
| editing        = 
| distributor    = 
| released       =  
| runtime        = 143 min.
| country        = India
| language       = Hindi
| budget         = Rs 10&nbsp;million.
| gross          = Rs 632&nbsp;million
}}
Kismet is a 1943 Indian film, written and directed by Gyan Mukherjee and produced by Bombay Talkies during the second world war period,  while it was in a succession battle between Devika Rani and Sashadhar Mukherjee after owner Himanshu Rais death. The film is one of the biggest hits in the history of Hindi cinema. 

The film came with some bold themes for the first time in Indian Cinema showing an anti-hero character and an unmarried girl getting pregnant.

==Synopsis==
Shekhar Ashok Kumar is a pickpocket and con man, who is released from jail after serving his third sentence. It is at once evident that he has no intention of mending his ways, as he relieves a pickpocket of his catch, which happens to be an old, priceless watch.

Shekhar goes to a fence (David Abraham Cheulkar|David), where he meets the original pickpocket, Banke (V.H. Desai). Impressed by Shekhars prowess, Banke makes him a special offer: he is a small time thief currently working in the house of a very rich man, who has a considerable fortune stashed away in the safe at his house. Banke, who does not have the expertise to break the lock, asks Shekhar if he would be interested in helping him out. Uninterested in the plan, Shekhar leaves.

As he steps out of the fences establishment, Shekhar bumps into the original owner of the watch (P.F. Pithawala), an old man desperate for money, who intended to sell the watch and raise the money to see a live performance by Rani (Mumtaz Shanti). Out of compassion Shekhar takes him to the theatre. There the old man points out a prosperous looking man called Indrajit (Mubarak). It turns out that Rani is the daughter of this old man, who was once a rich man and the owner of that theatre. Indrajit was once his employee.

Fortune (kismet) turned things around and today the old man is indebted to Indrajit, from whom he is on the run. The moment the show ends, he flees, but not before hes seen in the company of Shekhar by Rani, from whom he has fled in shame and remorse. Unknown to them, the tormentor Indrajit is himself a tormented man, after his elder son Madan ran away after a fight with his authoritarian father several years ago.

Due to a combination of circumstances, Shekhar ends up staying in Ranis house as a paying guest. There he discovers that she is struggling with a limp, thats affecting her ability to perform on stage. The limited means is a great problem for Rani, the sole bread earner in the house, having to support her younger sister Leela (Chandraprabha). To make things worse, she is constantly troubled by the ruthless Indrajit, who threatens to turn her out of the house if she cannot repay the next instalment of the money payable by her father.

Things take a turn for the worse when Rani discovers that Leela is pregnant out of wedlock with her lover Mohan (Kanu Roy), who happens to be the son of Indrajit. Shekhar, who is falling in love with Rani, decides to help her out. Desperate to raise funds for her cure, he takes up Bankes offer to break the vault of his employer, who is none other than Indrajit. The attempted robbery goes awry, but Shekhar escapes, dropping behind the chain he always wears. Indrajit immediately recognises that as that of his long lost son Madan.

Desperate to get back his son, Indrajit organises a live programme featuring Rani, knowing that his love for the young lady will impel Shekhar to come. Rani father turns up at the theatre, as does Indrajits entire family. As expected, Shekhar turns up at the theatre, where he is immediately recognised as Indrajits long lost son Madan.  Delighted to find his beloved son, Indrajit immediately turns a new leaf, cancelling his former boss debts and asking him for the hand of both his daughters for his sons.

==Reception==

Kismet was severely criticised for glorifying crime and portraying a criminal in good light. Despite the criticism, the movie shattered all box office records, becoming the first Indian movie to gross 10&nbsp;million (10&nbsp;million) at the box office. It ran for 187 continuous weeks at Roxy Cinema in Calcutta, a record that stood for 32 years.

The song दूर हटो दुनियावालों, हिन्दुस्तान हमारा हैं (go away foreigners, India is ours), which slipped past the censors, was an immensely popular song in the mid 40s, coming as it did less than 6 months after Mahatma Gandhi called for the Quit India Movement. Although superficially addressed to the Germans and Japanese (with whom the British rulers were at war then), the patriotic overtones became at once evident to contemporary audiences. At screenings of Kismet, the reels would be rewound and the song played multiple times on public demand. The unprecedented popularity of the song forced the lyricist Kavi Pradeep to go underground to avoid being arrested by the British authorities for sedition.

==Legacy==

Kismet was the first Hindi movie to have an anti-hero as its protagonist. It was also the first ever Hindi movie to use the theme of pregnancy out of wedlock, a recurring theme in Hindi movies all the way to the 1990s.
 Waqt (1965), Yaadon ki Baraat (1973) and Amar Akbar Anthony (1977).

The unprecedented success of Kismet established Ashok Kumar as the first ever superstar of Hindi cinema. He remained the most popular actor in Hindi cinema until the early 50s.

Kismet marks the on screen debut of famous 60s comedian Mehmood Ali|Mehmood, who played the role of Madan in his childhood avtar.
 boy friend featuring Shammi Kapoor, Dharmendra and Madhubala was a remake of Kismet.

In the 1968 movie Padosan, when asked to think of a romantic song, Bhola immediately remembers the door hato duniyawalo song.

==Cast==
* Ashok Kumar   as  Shekhar & Madan
* Mumtaz Shanti   as  Rani
* Shah Nawaz   as  Inspector Saahab
* Mubarak   as  Indrajeet Babu David   as  the fence
* Chandraprabha   as  Leela (Ranis sister)
* Kanu Roy   as  Mohan (Leelas lover)
* V.H.Desai   as  Baanke
* P.F. Pithawala   as  Ranis father Baby Kamala   as  as Rani (Childhood) Mehmood   as  Madan (Childhood)
*  Moti

==Soundtrack== Anil Biswas introduced full chorus for the first time in Hindi cinema.  The film gave memorable hits like the patriotic, Door Hato O Duniyawalon Hindustan Hamara hay, the sad Ghar Ghar Mein Diwali and a soothing lullaby, Dheere Dheere Aa. The last was a duet between Amirbai Karnataki and Ashok Kumar, which added to the success of the film that is still known as one of his finest works. 
# Aaj Himalay Ki Choti Se – Door Hato Ai Duniya Walo, Singer: Ameerbai Karnataki, Khan Mastana
# Ab Tere Siwa Kaun Mera Krishan Kanhaiya,             Singer: Ameerbai Karnataki
# Ai Duniya Bata – Ghar Ghar Me Diwali Hai,            Singer: Ameerbai Karnataki
# Dhire Dhire Aa Re Badal, Mera Bulbul Sau Raha Hai,   Singer: Ameerbai Karnataki
# Dhire Dhire Aa Re Badal, Mera Bulbul Sau Raha Hai,   Singer: Ameerbai Karnataki, Ashok Kumar
# Ham Aisi Qisamat Ko, Ek Din Hansaaye,                Singer: Ameerbai Karnataki, Arun Kumar
# Papihaa Re Mere Piyaa Se Kahiyo Jaay,                Singer: Parul Ghosh
# Tere Dukh Ke Din Phirenge, Zindagi Ban Ke Jiye Jaa,  Singer: Arun Kumar

===Door hato O Duniya walon===
In the patriotic song, Door hato O Duniya walon, Hindustan hamara hay ("Step away, People of the World,  .   But the hidden meaning got through to the people and backed by  , London, 15 December 1998. 

==Reception==
The film went on to become a major success, at a theatre in Calcutta it ran for three years, and gave Indian cinema its first title of superstar, Ashok Kumar.  According to the numbers, it has been given the status of All-Time Blockbuster. In the decade of the 1940s, this movie made the most money. Its net gross came to Rs. 10 million in 1943, which in todays date is equivalent of Rs. 632&nbsp;million.  This record was beaten in 1949 by Barsaat (1949 film)|Barsaat.

===Box office===
Presenting some bold themes for the first time in Bollywood in the 40s – an anti-hero and a single pregnant girl, for instance – this movie ran for 3 years. One of the earliest superhits of Bollywood, this movie also introduced double-role acting for the first time through its first superstar, Ashok Kumar.  Box Office India reported that the film collected a nett gross of   (which amounts to   if adjusted for inflation) and declared the film as an "All Time Blockbuster". 

==See also==
* List of highest-grossing Bollywood films

==References==
 

==External links==
 
*  
*  

 
 
 
 
 