Aika tappaa
  
{{Infobox film
| name           = Aika tappaa
| image          = Aikatappaa.jpg
| caption        = DVD cover
| director       = Timo Puustinen
| producer       = Timo Puustinen
| writer         = Timo Puustinen
| starring       = 
| music          = Jukka Korhonen
| cinematography = Panu Räsänen
| editing        = Jukka Korhonen
| distributor    = Coppersky Oy
| released       = 31 July 2005
| runtime        = 94 minutes
| country        = Finland Finnish
| budget         = 
| followed_by    = 
}}
 2005 Finland|Finnish horror Thriller thriller directed and written by Timo Puustinen.

The film premiered on 31 July 2005 in Finland.

==Plot==
The film is about a killer and murder.

==Cast==
*Marko Airama ....  Janitor (1935)
*Kirsi Ekonen ....  Eyewitness
*Atte Hämäläinen ....  Saku Pitkänen
*Mari Hänninen ....  Milla Jurvonen
*Kristian Hohkavaara ....  researcher Salo
*Jäntti Jussi ....  P.J
*Kirsi Kolehmainen ....  Mother Ivanov (1935)
*Maria Kukko ....  Miia
*Ismo Kuorttinen ....  police officer Lindström
*Emilia Laitinen ....  waitress in Nightlife
*Silja Lakisuo ....  eyewitness in harbor
*Satu Lasanen ....  Kaveri Nightlifessa
*Hanna Liinoja ....  Mikkos mother
*Tapio Liinoja ....  police chief
*Hannu Lintukoski ....  Aki Kallio
*Elina Lintulahti ....  nurse
*Olli Loukola ....  fisher (1935)
*Jari Määttä ....  police
*Mika Mäkkeli ....  Mikael Mäkinen
*Laura Malmberg ....  Mikkos sister
*Matias Malmivaara ....  Mikko Kallio
*Juha-Pekka Manninen ....  senior crime detective
*Henri Marjanen ....  Lähetti
*Elina Nurminen ....  waitress in Nightlife
*Elina Otsala ....  Nina
*Laura Otsala ....  Ronja
*Henri Paavola ....  Joonas (police)
*Ari Pelkonen ....  Juha Kokko
*Matti Poutiainen ....  Topias Ivanov (1935)
*Sari Pöyhönen ....  Mikon työkaveri
*Kiia-Frega Prepula ....  Laura Siekkinen
*Jarkko Pulkkinen ....  Markus
*Teuvo Puumalainen ....  researcher Sulkula
*Matti Sulin ....  eyewitness in harbor
*Tanja Venäläinen ....  police
*Jere P. Vilkkinen ....  manager of the car repair shop

==See also==
*List of Finnish films

==External links==
*  

 
 
 
 
 


 
 