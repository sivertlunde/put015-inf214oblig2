Shakuntala (1943 film)
{{Infobox film
| name           = Shakuntala
| image          = Shakuntala_1943.jpg
| image size     =
| caption        = Film Poster of Shakuntala
| director       = V. Shantaram
| producer       = V. Shantaram
| writer         = Diwan Sharar  (screenplay) 
| based on       =   
| narrator       = Chandra Mohan V. Shantaram Ameena
| music          = Vasant Desai
| cinematography = V. Avadhoot
| editing        =
| studio         = Rajkamal Kalamandir
| distributor    = National Finance of India, Ltd., Delhi, India
| released       =  
| runtime        = 122 minutes
| country        = India
| language       = Hindi
| budget         =
| preceded by    =
| followed by    =
}} 1943 costume drama film based on the Mahabharat episode of Shakuntala, directed by V. Shantaram.    It was the first film made under the newly formed Rajkamal Kalamandir banner that Shantaram had started. It was the first film to be shown commercially in US.    Adapted from the Shakuntala of Kalidas the screenplay was by Diwan Sharar. Music was composed by Vasant Desai  with lyrics by Diwan Sharar and Ratan Piya.     Chandra Mohan, Ameena, Shantaram, Zohra and Nana Palsikar.   

Shakuntala was an adaptation of Kālidāsas  Sanskrit drama Abhijñānaśākuntalam (Of Shakuntala who is recognized by a token) and appreciated "worldwide".       The film initially sticks to the traditional version of Kalidas in the representation of Shakuntala, but later follows a "transformation" in the form of "empowerment of women" in Shakuntalas role, which is attributed to a critique of the play by Bankim Chatterjee.    

==Plot== Chandra Mohan), Bharata and several years pass without the return of Dushyanta who has lost his memory and has no recollection of Shakuntala. The ring he has given her is lost in the river and swallowed by a fish. Dushyanta turns her away when Shakuntala goes to the court. Later when Dushyanta recovers his memory Shakuntala refuses to go with him but both are finally united.

==Cast==
* Kumar Ganesh as Bharat
* Jayashree as Shakuntala
* Chandra Mohan as King Dushyanta
* Shantaram as Priyamvad
* Ameena
* Madan Mohan
* Zohra as Menaka
* Nana Palsikar
* Vilas
* Raja Pandit
* Shantarin
* Vidya

==Review==
Shakuntala was the first Indian film to be shown in the US.     The New York Times of 1947 stated that "Shakuntala has a charm entirely its own". Calling it a "fairy-tale"  the reviewer praised the background, and commented on the "unabashed naïveté of acting of the entire cast", and the "crudely rich musical score" but called it "a sturdy screen promise".    The film has been cited as a "major hit" and was shown at a theatre in India for continuous 104 weeks.    

==Award==
The film was nominated for the Grand International Award at the 1947 Venice Film Festival.

==Soundtrack==
The film was composed by Vasant Desai, who had earlier provided background music in Shantaram’s films. This was Desais first independent music venture and continued to have a long association with Shantarams films.    The lyricists were Diwan Sharar and Ratan Piya. The singers were Jayashree, Zohrabai Ambalewali, Parshuram and Amirbai Karnataki.    

===Songlist==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| Jhooloongi Jhooloongi Jeevan Bhar
| Jayashree
| Ratan Piya
|-
| 2
| Pyari Pyari Ye Sukhad Maatrubhoomi Apni
| Jayashree, Zohrabai Ambalewali, Parshuram
| Ratan Piya
|-
| 3
| Chand Sa Nanha Aaye
| Jayashree, Zohrabai Ambalewali
| Ratan Piya
|-
| 4
| Kamal Hai Mere Saamne
| Jayashree
| Deewan Sharar
|-
| 5
| Tumhe Prasann Yun Dekh Ke
| Jayashree, Zohrabai Ambalewali, Parshuram
| Ratan Piya
|-
| 6
| Ek Prem Ki Pyasi
| Amirbai Karnataki
| Ratan Piya
|-
| 7
| Mere Baba Ne Baat Meri
| Jayashree
| Ratan Piya
|-
| 8
| Kisi Kanya Ko
| Vasant Desai
| Deewan Sharar
|-
| 9
| Jeevan Ki Naav Na Doley
| Jayashree
| Deewan Sharar
|-
| 10
| Sukh Bhara Kare Jeevan
| Amirbai Karnataki
| Ratan Piya
|-
| 11
| Byah Rachaye Chup Chup
| Zohrabai Ambalewali
| Deewan Sharar
|-
| 12
| Na Jaane Kahan Ka Yeh Jaadoo
|
|
|-
| 13
| Mere Birah Ki Rain
|
|
|}

==References==
 

==External links==
* 
*  at Surjit Singh - Shakuntala songs

 

 
 
 
 
 
 