Charlotte's Web (2006 film)
 
  
{{Infobox film name = Charlottes Web image = Charlottes Web 2006.jpg caption = Theatrical release poster director = Gary Winick producer = Jordan Kerner screenplay = Susannah Grant Karey Kirkpatrick based on =   narrator = Sam Shepard starring = Andre Benjamin Reba McEntire Kathy Bates Robert Redford music = Danny Elfman cinematography = Seamus McGarvey editing = Susan Littenberg Sabrina Plisco studio = Kerner Entertainment Company Walden Media Nickelodeon Movies distributor = Paramount Pictures released =   runtime = 97 minutes language = English budget = $85 million  . Box Office Mojo. Retrieved September 4, 2010.  gross = $144,877,632
}}
 of the The K Entertainment Company, and Nickelodeon Movies. The screenplay is by Susannah Grant and Karey Kirkpatrick, based on Whites book.
 1973 cel-animated version produced by Hanna-Barbera for Paramount Pictures.

==Plot==
One spring, on a farm in Somerset County, Maine, Fern Arable (Dakota Fanning) finds her father about to kill the runt of a litter of newborn pigs. She successfully begs him to spare its life. He gives it to her, who names him Wilbur and raises him as her pet. To her regret, when he grows into an adult pig, she is forced to take him to the Zuckerman farm, where he is to be prepared as dinner in due time.

Charlotte A. Cavatica ( , an orb-weaver spider with the scientific name Araneus cavaticus.
 egg sac. She cannot return home because she is dying. Wilbur tearfully says goodbye to her but manages to take her egg sac home, where hundreds of offspring emerge. Most of the young spiders soon leave, but three, named Joy, Aranea, and Nellie, stay and become Wilburs friends.

==Cast==

===Live-action actors===
*Dakota Fanning as Fern Arable Kevin Anderson as John Arable
*Beau Bridges as Dr. Dorian
*Louis Corbett as Avery Arable
*Essie Davis as Mrs. Arable, Ferns mother
*Siobhan Fallon Hogan as Edith Zuckerman
*Gary Basaraba as Homer Zuckerman
*Nate Mooney as Lurvy

===Voice actors===
*Dominic Scott Kay as Wilbur the Pig
*Julia Roberts as Charlotte A. Cavatica, the Spider
*Steve Buscemi as Templeton the Rat
*John Cleese as Samuel the Sheep
*Oprah Winfrey as Gussie the Goose
*Cedric the Entertainer as Golly the Gander Cow
*Reba McEntire as Betsy a Cow
*Robert Redford as Ike the Horse
*Thomas Haden Church as Brooks a Crow
*André Benjamin as Elwyn a Crow
*Abraham Benrubi as Uncle a large Pig
*Sam Shepard as Narrator

==Production== The Trumpet became its own company in late 2004.
 Heidelberg in Melbourne, Australia at Heidelberg West Football Clubs football ground.

Visual effects are by Rising Sun Pictures, Fuel International, Proof (studio)|Proof, Rhythm and Hues Studios, Digital Pictures Iloura and Tippett Studio. The visual effects supervisor for the film as a whole was John Berton, who noted that a live action version of Charlottes Web has become much more practical in recent years due to advances in technology.  Winick "was adamant" that Charlotte and Templeton (the films two entirely computer-generated characters) should be realistic and not stylized, although they did give Charlotte almond-shaped eyes.  John Dietz, visual effects supervisor for Rising Sun Pictures, notes that there was a debate over whether to give her a mouth, and that in the end they decided to have her chelicerae move in what he describes as being almost like a veil, as if there were a mouth behind it. 

==Release== Over the Hedge and Cars (film)|Cars among other films. It was released in Australia on December 7, 2006 and in the United States and Canada on December 15, 2006.  The U.S. and Canadian release date matches that of 20th Century Foxs Eragon (film)|Eragon, another film with fantasy elements and a young protagonist. The scheduled release date in the UK is February 9, 2007. 

===Home media===
Charlottes Web was released on DVD on April 3, 2007 in the United States and Canada and May 28 in the United Kingdom. It was released on Blu-ray on March 29, 2011 along with The SpongeBob SquarePants Movie.

==Reception==
Reviews were generally positive, especially with respect to Dakota Fannings portrayal of Fern. The film currently holds a 78% "Certified Fresh" rating at Rotten Tomatoes.  Michael Medved gave Charlottes Web three and a half stars (out of four) calling it "irresistible" and "glowing with goodness".  Medved also said that Dakota Fannings performance was "delightfully spunky".  Entertainment Weeklys Owen Gleiberman complains that the film is "a bit noisy" but applauds the director for putting "the book, in all its glorious tall-tale reverence, right up on screen." He goes on to say that "What hooks you from the start is Dakota Fannings unfussy passion as Fern." 

Colm Andrew of the Manx Independent gave the film 6/10, saying that the main problem was "the ultra-cute characterisation of Wilbur, resulting in half the audience rooting for his demise" although overall it was "a competent retelling of a classic story that wont offend". 

The film was awarded a 2006 Critics Choice Award for Best Family Film in the live-action category,  and Fanning won the Blimp Award for Favorite Movie Actress at the 2007 Kids Choice Awards.

The film debut in third place at the box office with only $11 million. The film performed very well after spending 14 weeks in theaters for a total of $82 million, $61 million elsewhere, for a total of $144 million before closing on March 22, 2007. 

==Soundtrack==
{{Infobox album    Name        = Charlottes Web Type        = Film score Artist      = Danny Elfman Cover       = Charlottes Web Soundtrack.jpg Released    = 2006 Recorded    = Genre       = Length      = Label       = Producer    = Reviews     = Last album  = Serenada Schizophrana (2006) This album  = Charlottes Web (2006) Next album Standard Operating Procedure (2008)
}}
 opening ceremonies of her hometown Vancouver Winter Olympics. A CD compilation of "Music Inspired by the Motion Picture" was issued on December 12, 2006.

==Video game==
  PS2 and Personal computer|PC.

==References==
 

== External links ==
 
*  
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 