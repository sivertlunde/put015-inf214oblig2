Vidaaya
{{Infobox film
| name           = Vidaaya
| image          =
| director       = P. Sheshadri
| producer       = Basantkumar Patil Amruta Patil
| writer         = P. Sheshadri
| screenplay     = 
| starring       = Lakshmi Gopalaswamy Suchendra Prasad H. G. Dattatreya Pratham
| music          = Raju
| cinematography = SabhaKumar
| editing        = B. S. Kemparaju
| studio         = Basant Productions
| released       = 
| country        = India Kannada
}}
 Kannada film written and directed by P. Sheshadri  starring Lakshmi Gopalaswamy, Suchendra Prasad, H. G. Dattatreya and Pratham in lead roles.

==Plot==
Vasuki is a father of two children, who is in a permanent vegetative state, after seriously hurt in an accident.  Meera his wife, who has been opposing from the beginning the request of “euthanasia” from her husband, having lost any hope to live, at last submits to her husband’s persuasion and agrees to apply in the court for passive euthanasia. But, in the media, this matter takes totally a different color and she will be depicted as “a woman set to kill her own husband”. Vasu will be denied of his request, by the court as per the existing law. Though Vasu is disappointed by this verdict, Meera feels happier. For further treatment, now by nature therapy as suggested by his mother, Vasu is taken to a place near his hometown. A stay near his hometown by the seashore nurtures a hope in his life. 
In the nature clinic Meera is drawn closer to an aged Colonal. Having seen hundreds of deaths in the closest vicinity, now despite seeing another death dancing in a nascent form, being oblivious to whatever has happened to him, showing all the zeal in life, strengthens Meera’s will.  She begins to feel as if Vasu is getting recouped.
At the same time Supreme Court again calls for an open discussion on euthanasia.  And almost at the same time condition of Vasu suddenly begins to deteriorate. What happens next is the anxious question… 

== Cast ==
* Lakshmi Gopalaswamy
* Suchendra Prasad
* H. G. Dattatreya
* Pratham

==References==
 

 
 
 