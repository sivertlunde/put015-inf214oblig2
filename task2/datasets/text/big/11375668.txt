Anokha Milan
{{Infobox film
| name           = Anokha Milan
| image          =
| image size     =
| caption        =
| director       = Jagganath Chatterjee
| producer       = R. J. Vazirani
| writer         =
| narrator       =
| starring       = Dharmendra Dilip Kumar
| music          = Salil Choudhury
| cinematography =
| editing        = Waman B. Bhosle
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| preceded by    =
| followed by    =
}}
Anokha Milan in a 1972 Hindi film produced by R. J. Vazirani and directed by Jagganath Chatterjee. The film stars Dharmendra, Pronati Ghosh and Dilip Kumar. The films music is by Salil Choudhury.

==Plot==
Tara Sen lives in a small town in India with her parents, a younger brother, sister, and an elder brother who lives in Calcutta. She is friendly with Ghanshyam, affectionately called "Ghana", and often spends time with him near the river bank. When her brother returns home he brings along a friend named Bijesh, who takes a liking to Tara and molests her. He is asked to leave the very next day, but the news spreads like wildfire, resulting in her parents to decide to get her married immediately, in vain though as no one wants to come forward to marry her. During the Devi Pooja ceremonies, people crowd around the Sen household and spread vicious rumors about Tara, Ghana does not take kindly to this, a fight ensues, and a man is killed. The police are summoned, Ghana is arrested and held in prison. Sen attempts to reason with the local Police Inspector Badal Gupta, known for womanizing and being an alcoholic, who agrees to withdraw all charges provided Tara marries him. Sen is reluctant, but Tara readily accepts, and their marriage takes place. Taras husband does not give up on his bad habits, neither does he fulfill his promise of releasing Ghana. One day an argument ensues between Tara and him, followed by a struggle, blows are exchanged, with Tara hitting him on the head with an axe. He is hospitalized, Tara is arrested, but when he regains consciousness he refuses to press charges, then subsequently succumbs to his injuries, Tara is sentenced to prison. When she finds out that Ghana is doing time in Andamans, she requests for a transfer there, which is granted. When she reaches Andamans, she is told by the Warden that the only way she can meet with Ghana is by marrying him. But Ghana is old-fashioned and refuses to marry Tara on grounds that she is wealthy and he a lowly servant. Tara now has an option - return home and live alone for the rest of her life, or alternatively kill herself.

==Cast==
* Brahm Bhardwaj	 ...	Voice
* Abhi Bhattacharya	 ...	Anant Sen
* Dharmendra    	 ...	Ghanshyam "Ghana"
* Pranoti Ghosh 	 ...	Tara Sen / Tara Gupta (as Pronoti)
* Dilip Kumar   	 ...	Warden
* Raj Mehra     	 ...	Voice
* Shivraj        	 ...	Voice

==External links==
* 

 
 
 
 

 