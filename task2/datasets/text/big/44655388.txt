Winning a Widow
{{infobox film name = Winning a Widow image = Winning_Widow_Wiki.jpg caption = Advertising published in The Moving Picture World, Vol 12, p. 1102 director = Sidney Olcott producer = Kalem Company writer = Gene Gauntier starring = Gene Gauntier Jack J. Clark distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 1000 ft
| country = United States language = Silent film (English intertitles) 
}}

Winning a Widow is a 1912 American silent film produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott with Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier - The Widow
* Jack J. Clark - Jim White

==Production notes==
The film was shot in Luxor, Egypt.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 


 