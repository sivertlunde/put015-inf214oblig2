Against the Wall (2010 film)
 
{{Infobox film
| name           = Against the Wall
| image          = Against the Wall.jpg
| caption        = 
| director       = David Capurso
| producer       = Jeff Haber
| writer         = Jeff Haber
| starring       = Russ Russo Sarah Ahlgren Tammy McNeill
| music          = John Plenge
| cinematography = Tim Naylor
| editing        = Karlyn Michelson
| studio         = 
| distributor    = Indie Rights, Go Digital, G4TV
| released       =  
| runtime        = 9 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Breakout style of video game play.  The short was accepted into 5 festivals, nominated for two Maverick Movie Awards, and given multiple online distribution deals, including appearances on iTunes and the G4TV website.

==References==
 
* http://www.g4tv.com/videos/54039/against-the-wall-a-g4-films-presentation/
* http://www.filmthreat.com/reviews/27467/

==External links==
* 
* 

 
 
 


 
 