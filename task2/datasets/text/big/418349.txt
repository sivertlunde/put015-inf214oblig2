Lancelot du Lac (film)
{{Infobox film
| name           = Lancelot du Lac
| image          = Lancelot du Lac film poster .jpg
| caption        = Film poster
| writer         = Robert Bresson
| starring       = Luc Simon Laura Duke Condominas Humbert Balsan Vladimir Antolek-Oresek Patrick Bernhard
| director       = Robert Bresson
| producer       = Jean-Pierre Rassam Francois Rochas
| cinematography = Pasqualino De Santis
| editing        = Germaine Artus
| music          = Philippe Sarde
| studio         = Mara Films Laser Productions Office de Radiodiffusion Télévision Française Gerico Sound Gaumont
| released       = May 1974 (1974 Cannes Film Festival|Cannes) 26 September 1974 (France)
| runtime        = 85 min.
| country        = France
| language       = French
| budget         = 
|}} 1974 film Round Table fall apart. It is based on Arthurian legend and medieval romances, especially the Lancelot-Grail cycle, and the works of Chrétien de Troyes.

In common with Bressons later films, the cast was composed of amateur actors, several of whom did not appear in any other film. Bressons direction demanded a purposeful lack of emotion in the acting style, and reduced or eliminated the fantastical elements of the Grail legend. This unglamorous depiction of the Middle Ages emphasizes blood and grime over fantasy.

==Plot==
King Arthur has sent out 100 knights to retrieve the Holy Grail. Arthur is dismayed when it turns out that the mission was futile and 70 knights have died in its course. Among those who have returned is the Queens lover Lancelot. Soon Lancelot again takes part in a Tournament (medieval)|tournament. There he gets injured. While Lancelot seeks recovery in his own castle, Arthur learns about his wifes affair and, heavily agitated by Mordred, he puts Queen Guinevre in prison. With Lancelots help she breaks out. Arthur starts immediately a campaign against the castle where the lovers were looking for shelter. During the siege Mordred Lancelet happens to kill his old mate Gawain. Driven by sorrow he tries to end the fight and wants to negotiate a treaty with King Arthur. When he witnesses how Mordred commits an attempt on Arthur he joins without hesitating the side of the king. In the end Arthur and Lancelot and all the other knights of the legendary round table are slaughtered.

==Selected cast== Lancelot du Lac La Reine (The Queen)
* Humbert Balsan as Gauvain (Gawain) Le Roi (The King)
* Patrick Bernhard as Mordred Lionel
* Charles Balsan   
* Christian Schlumberger   
* Joseph-Patrick Le Quidre   
* Jean-Paul Leperlier   
* Marie-Louise Buffet   
* Marie-Gabrielle Cartron   
* Antoine Rabaud   
* Jean-Marie Becar   
* Guy de Bernis

==Production==
The film was shot from the end of June to the start of September 1973 in Noirmoutier-en-lÎle. 
It was shot on 35 mm color film with an aspect ratio of 1.66 : 1. 

==Release==
The film premiered at the 1974 Cannes Film Festival in May of 1974, followed by its theatrical release in France on 26 September 1974. It had its television premiere in West Germany on 4 May 1974. 

==Reception==
===Critical response===
The film was well-received among critics and audiences, currently holding a 94% "fresh" rating on Rotten Tomatoes based on 16 reviews  as well as a 7.1 rating on imdb. 

===Accolades=== FIPRESCI Prize at the 1974 Cannes Film Festival.   

==See also==
* List of films based on Arthurian legend

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 