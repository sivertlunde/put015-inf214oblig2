Manon des Sources (1986 film)
 
{{Infobox film
| name           = Manon des Sources
| image          = Manon des Sources ver2.jpg
| writer         = Claude Berri Gérard Brach
| starring       = Yves Montand Daniel Auteuil Emmanuelle Béart Hippolyte Girardot
| director       = Claude Berri
| producer       = Pierre Grunstein Alain Poiré
| music          = Jean-Claude Petit  
| cinematography = Bruno Nuytten
| editing        = Hervé de Luze Geneviève Louveau
| distributor    = Pathé Distribution (EU) Orion Classics (USA)
| released       =  
| runtime        = 113 minutes
| language       = French
| budget         = 
| gross          = 56.4 million €
| country        = Italy France Switzerland
}}
Manon des Sources ( ; released in North America as Manon of the Spring) is a critically acclaimed and commercially successful 1986 French language film. Directed by Claude Berri, it is the second of two films adapted from the 1966 two-volume novel by Marcel Pagnol, who wrote it based on his own earlier film of the same title. It is the sequel to Jean de Florette.

==Plot== spring there.

After seeing her bathe nude in the mountains, Ugolin develops an interest in Manon. When he approaches her, she seems disgusted by his vileness and almost certainly by the memory of his involvement in her fathers downfall. But Ugolins interest in Manon becomes obsessive, culminating in sewing a ribbon from her hair onto his chest. At the same time, Manon becomes interested in Bernard, a handsome and educated schoolteacher recently arrived in the village. As a small child, Manon had suffered the loss of her father, who died from a blow to the head while using explosives in an attempt to find the water source. César and Ugolin then bought the farm cheaply from his widow—Manons mother—and unblocked the spring. Manon witnessed this as a child. The two men profited directly from his death.
 source of the spring that supplies water to the local farms and village. To take her revenge on both the Soubeyrans and the villagers, who knew but did nothing, she stops the flow of water using the iron-oxide clay and rocks found nearby.

The villagers quickly become desperate for water to feed their crops and run their businesses. They come to believe that the water flow had been stopped by some Providence to punish the injustice committed against Jean. Manon publicly accuses César and Ugolin, and the villagers  admit their own complicity in the persecution of Jean. They had never accepted him, as he was an outsider and was physically deformed. César tries to evade the accusations, but an eye-witness, a poacher who was trespassing on the vacant property at the time, steps forward to confirm the crime, shaming both César and Ugolin. Ugolin makes a desperate attempt to ask Manon for her hand in marriage, but she rejects him. The Soubeyrans flee in disgrace. Rejected by Manon, Ugolin commits suicide by hanging himself from a tree, apparently ending the Soubeyran line.

The villagers appeal to Manon to take part in a religious procession to the villages fountain, hoping that acknowledging the injustice will restore the flow of water to the village. With the assistance of Bernard, Manon unblocks the spring in advance, and the water arrives at the village at the moment that the procession reaches the fountain. Manon marries Bernard. She is last seen very pregnant and leaving a church service on Christmas Eve with her husband.

Meanwhile, César has been broken by his nephews suicide. Delphine, an old acquaintance of his, returns to the village and tells him that Florette, his sweetheart from that period, had written to him to tell him she was carrying their child. Receiving no reply from him, she had tried to abort it. Florette left the village, married a blacksmith from nearby Créspin, and the child was born alive but a hunchback.

César, away on military service in Africa, never received her letter and did not know that she had given birth to his child.  In a cruel twist of fate, Jean, the man he drove to desperation without having met him, was the son he had always wanted. Devastated, and lacking the will to live any longer, César dies quietly in his sleep. In a letter he leaves his property to Manon, whom he recognises as his natural granddaughter and the last of the Soubeyrans.

==Cast==
* Emmanuelle Béart as Manon
* Yves Montand as César Soubeyran/Le Papet
* Daniel Auteuil as Ugolin
* Hippolyte Girardot as Bernard Olivier
* Margarita Lozano as Baptistine
* Yvonne Gamy as Delphine
* Gabriel Bacquier and Eve Brenner as singers at the wedding

==Box office ==

The film was a success. 

==Awards==
* César Award for Best Actor - Daniel Auteuil, 1987
* César Award for Best Actress in a Supporting Role – Emmanuelle Béart, 1987

==References==

 

== External links ==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 