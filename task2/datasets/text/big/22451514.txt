Talk to Me (2006 film)
{{Infobox film
| name           = Talk to Me
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Mark Craig
| producer       = Mark Craig
| writer         = Mark Craig
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Mark Craig Steve Alexander
| cinematography = Ken Morse
| editing        = Dan Haythorn
| studio         = Stopwatch Productions British Documentary Film Foundation
| released       = 2006 
| runtime        = 23 minutes United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 2006 United British documentary film by filmmaker Mark Craig.                       The film won Best Short Doc upon its debut at the Boulder International Film Festival in 2006. 

==Synopsis==
The film follows Craigs relationships over a twenty-year period using answer phone tapes and photos of the time. The recordings were originally kept as a sort of diary though this eventually developed into the film.    

==Production==
 
 

==Release==
The film screened in 2007 at the Ashland Independent Film Festival in Ashland, Oregon,  and in June that year at the National Media Museum in Bradford, West Yorkshire, England,   It was previously available on More 4 and 4od, however, the film is now available only on DVD with some of the original soundtracks removed due to copyright reasons.

==Reception==
The Daily Telegraph wrote that Mark Craigs use of onscreen photographs of his various callers from over a 20+ year period was a "brilliant collage" and "so inventive that it aspired to the condition of drama".  They lauded the film, writing "The cleverness of this work was that it gave a complete portrait not only of the callers, but also of Mark  ", and that it "conveyed a real sense of non-communication and of lifes dramas."      

===Awards and nominations===
* 2006, won Best Short Doc at Boulder International Film Festival    Sheffield International Documentary Festival   
* 2007, won Special Mention for Best Short Doc at Hot Docs Canadian International Documentary Festival 

==References==
 

==External links==
*  
*  

 
 
 
 
 