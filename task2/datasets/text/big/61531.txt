Stage Door
 
{{Infobox film
| name           = Stage Door
| image          = Stage Door (1937).jpg
| caption        = Stage Door theatrical poster James Anderson (assistant)
| producer       = Pandro S. Berman
| screenplay         =Morrie Ryskind Anthony Veiller
| based on       =  
| starring       = Katharine Hepburn Ginger Rogers Adolphe Menjou
| music          = Roy Webb
| cinematography = Robert De Grasse William Hamilton RKO Radio Pictures
| distributor    = RKO Radio Pictures   Radio Pictures Ltd  
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $952,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  gross = $1,762,000 
}}
Stage Door is a 1937 RKO film, adapted from the play by the same name, that tells the story of several would-be actresses who live together in a boarding house at 158 West 58th Street in New York City. The film stars Katharine Hepburn, Ginger Rogers, Adolphe Menjou, Gail Patrick, Constance Collier, Andrea Leeds, Samuel S. Hinds and Lucille Ball. Eve Arden and Ann Miller, who became notable in later films, play minor characters.

The film was adapted by Morrie Ryskind and Anthony Veiller from the play by Edna Ferber and George S. Kaufman, but the plays storyline and the characters names were almost completely changed for the movie, so much so in fact that Kaufman joked the film should be called "Screen Door".

==Plot==
Terry Randall ( , just as fellow resident Linda Shaw (Gail Patrick) has from her relationship with influential theatrical producer Anthony Powell (Adolphe Menjou). In truth however, Terry comes from a very wealthy, upper class, Midwest family. Over the strong objections of her father, Henry Sims (Samuel S. Hinds), she is determined to try to fulfill her dreams on her own. In the boarding house, Terrys only supporter is aging actress Catherine Luther (Constance Collier), who appoints herself Terrys mentor.

When Powell sees Jean dancing, he decides to dump Linda. He arranges for Jean and her partner Annie (Ann Miller) to get hired for the floor show of a nightclub he partly owns. He then starts dating Jean, who, despite her initial reluctance, starts falling for the man.

Meanwhile, well-liked Kay Hamilton (Andrea Leeds) had a great success and rave reviews in a play the year before, but has had no work since, and is running out of money. She clings desperately to the hope of landing the leading role in Powells new play, Enchanted April. She finally gets an appointment to see Powell, only to have him cancel at the last minute. She faints in the reception area, the result of malnutrition and disappointment. Seeing this, Terry barges into Powells private office and berates him for his callousness.  As a result, the other boarding house residents start to warm to the newcomer.

Terrys father secretly finances Enchanted April on condition that Terry be given the starring role, hoping she will fail and return home. Powell invites Terry to his penthouse to break the news. When Jean shows up unannounced, Terry sees the opportunity to save her friend from the philandering Powell. She pretends that Powell is trying to seduce her. It works. However, it makes things uncomfortable around the boarding house. Terrys landing of the plum part breaks Kays heart. 

The totally inexperienced Terry is so woodenly bad during rehearsals that Powell tries desperately to get out of his contract with Sims. On opening night, after she learns from Jean that the depressed Kay has committed suicide, Terry decides she cannot go on. Catherine tells her that she must, not just for herself, but also for Kay. She does, and gives a heartfelt performance. She and the play are a hit, much to the chagrin of her father, who is in the audience. At her curtain call, Terry gives a speech in tribute to her dead friend, and Terry and Jean are reconciled. The play remains a success after months, but Terry continues to board at the Footlight Club. A newcomer shows up looking for a room.

==Cast==
{| class="wikitable" Katharine Hepburn  as Terry Randall Ginger Rogers  as Jean Maitland
|- Adolphe Menjou  as Anthony Powell Gail Patrick  as Linda Shaw
|- Constance Collier  as Catherine Luther Andrea Leeds  as Kay Hamilton
|- Samuel S. Hinds  as Henry Sims Lucille Ball  as Judy Canfield, who eventually gives up acting and marries Milbanks
|}
* Franklin Pangborn as Harcourt, Powells butler
* William Corson as Bill
* Pierre Watkin as Richard Carmichael
* Grady Sutton as "Butch"
* Frank Reicher as Stage Director
* Jack Carson as Mr. Milbanks, a lumberman from Seattle who takes Jean to dinner
* Phyllis Kennedy as Hattie
* Eve Arden as Eve
* Ann Miller as Annie
* Margaret Early as Mary Lou
* Florence Reed (uncredited)

==Production==
 
*The writers listened to the young actresses talking and joking off set during rehearsals and incorporated their style of talking into the film. Director Gregory La Cava also allowed the actresses to ad lib during filming. The Lake (1934),  the play for which Dorothy Parker panned Hepburns performance as "running the gamut of emotions from A to B."

==Reception== Regal on 31 December 1937:
 

Prior to this film, Hepburns last four movies had flopped commercially. However, as a result of the positive response to her performance in Stage Door, RKO immediately cast Hepburn opposite Cary Grant in the screwball comedy Bringing Up Baby (1938).

Stage Door made a small profit of $81,000. 

==Similarities to the play==
The movie has almost nothing to do with the play, except in a few character names, such as Kay Hamilton, Jean Maitland, Terry Randall, Linda Shaw, and Judith Canfield. In the play, Terry Randall is from a rural family whose father is a country doctor, and Jean Maitland is actually a shallow girl who becomes a movie star. However, Kay Hamilton does commit suicide, but for completely different reasons and not on an opening night.
 Academy Awards==
;Nominations 
*   Best Director: Gregory La Cava Best Supporting Actress: Andrea Leeds
*  , Anthony Veiller

==Home media==
After Kay commits suicide, there is a brief shot of her grave as part of the montage of the success of the play, which was once edited out on all TV showings and is not on the VHS release.  The shot was restored for the DVD and is now included in TV showings of the restored version.

==TV adaptation==
*On April 6, 1955, a 60-minute version of the play, written by Gore Vidal, aired on the CBS Television series The Best of Broadway.

==References==
 
* Dooley, Roger, From Scarface to Scarlett: American Films in the Thirties

==External links==
 
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 