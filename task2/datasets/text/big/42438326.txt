Lab Rats (film)
{{Infobox film
| name           = Lab Rats
| image          = Lab_Rats_Film_Poster.jpg
| alt            = 
| caption        =  Sam Washington
| producer       = Amy Salko Robertson Louis Rosenberg Sam Washington Louis Rosenberg Daniel Fraser Stephanie Blacker Howard Ward Georgia Slowe Lucy Evans
| music          = 
| cinematography = 
| editing        = 
| studio         = Outland Pictures
| distributor    = 
| released       =  
| runtime        = 27 minutes
| country        = United Kingdom
| language       = 
| budget         = 
| gross          = 
}} Sam Washington. Starfish Award at the Moondance International Film Festival in 2011.  

In 2013 Lab Rats was licensed by Frostbite Pictures for development as an internet web-series. After completion but before distribution, the series won a Best Series award at LA Web Series Festival in 2014 and is scheduled for distribution later this year. 

==Plot==
To earn extra money, two university students, Zac and Cindy, get jobs as physiological test subjects. Neither knew that they would be used as pawns by the professors running the tests whom a using the two students to fight through a divorce.

==Awards==
{| class="wikitable" style="text-align: center"
|----- bgcolor="#94bfd3"
| colspan=4 align=center | Awards for Lab Rats
|----- bgcolor="#94bfd3" 
!width="90"|Year
!width="250"|Association
!width="300"|Award Category
|-
| 2009
| Moondance International Film Festival
| Best Short Screenplay
|-
| 2009
| Worldfest-Houston International Film Festival
| Best Short Screenplay
|-
| 2011
| Moondance International Film Festival
| Best Short Film

|-
| 2011
| San Louis Obispo International Film Festival
| Audience Choice Award
|-
| 2011
| Silicon Valley Film Festival
| Best Short Film 
|-
| 2011
| Maverick Movie Awards
| Nominated for Best Picture, Best Director, Best Screenplay 
|-
| 2011
| Ventura Film Festival
| Best Short Film
|-
| 2011
| Los Angeles Comedy Festival
| Best Foreign Short
|-
| 2014
| Vancouver Web Fest
| Official Selection
|-
| 2014
| Hollyweb Festival
| Official Selection
|-
| 2014
| LA Web Series Festival
| Official Selection
|}

==References==
 

==External links==
*  
*  

 

 
 


 