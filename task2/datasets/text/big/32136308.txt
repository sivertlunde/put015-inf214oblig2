Five and Ten
{{Infobox film
| name           = Five and Ten
| image          =
| image_size     =
| caption        =
| director       = Robert Z. Leonard (uncredited)
| producer       = Marion Davies Robert Z. Leonard
| writer         = Fannie Hurst (novel) A. P. Younger Edith Fitzgerald (dialogue continuity)
| narrator       = Leslie Howard
| music          =
| cinematography =
| editing        =
| studio         = Metro-Goldwyn-Mayer Cosmopolitan Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 88-89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Leslie Howard as the man she loves, though he marries someone else. It is based on the Fannie Hurst novel of the same name. 

==Cast==
*Marion Davies as Jennifer Rarick
*Leslie Howard as Berry Rhodes Richard Bennett as John G. Rarick
*Irene Rich as Jenny Rarick
*Douglass Montgomery as Avery Rarick (as Kent Douglass)
*Mary Duncan as Muriel Preston

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 

 