The Cost of a Kiss
{{Infobox film
| name           = The Cost of a Kiss
| image          =
| caption        =
| director       = Adrian Brunel
| producer       = 
| writer         = H. Fowler Mear Edward Cooper
| music          =
| cinematography = 
| editing        = 
| studio         = Mirror Films
| distributor    = Mirror Films
| released       = February 1917 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Edward Cooper. It marked the feature film debut of Brunel who went on to become a leading British director of the 1920s. It was the only film produced by Mirror Films, a company set up by Brunel and the screenwriter H. Fowler Mear.

==Cast==
* Bertram Wallis as Lord Darlington
* Marjorie Day Edward Cooper
* Thomas Canning
* R. Van Cortlandt
* Joan Marshall
* Cino Conti
* Ethel Griffies
* Noel Grahame
* Gordon Begg
* A.V. Bramble
* Babs Brunel

==Bibliography==
* Murphy, Robert & Brown, Geoff & Burton, Alan. Directors in British and Irish cinema: A Reference Companion. BFI, 2006.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 