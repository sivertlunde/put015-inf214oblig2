Akseli and Elina
{{Infobox film
| name           = Akseli and Elina
| image          = 
| caption        = 
| director       = Edvin Laine
| producer       = Mauno Mäkelä
| writer         = Väinö Linna Edvin Laine
| starring       = Aarno Sulkanen
| music          = 
| cinematography = Olavi Tuomi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 137 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

Akseli and Elina ( ) is a 1970 Finnish drama film directed by Edvin Laine. It was entered into the 7th Moscow International Film Festival.    The film is based on the third volume of Väinö Linnas novel trilogy Under the North Star. Its a sequel to the 1968 film Here, Beneath the North Star which was based on the two first volumes of the trilogy.

==Cast==
* Aarno Sulkanen as Akseli Koskela
* Ulla Eklund as Elina Koskela
* Risto Taulo as Jussi Koskela
* Anja Pohjola as Alma Koskela
* Mirjam Novero as Anna Kivivuori
* Kauko Helovirta as Otto Kivivuori
* Esa Saario as Janne Kivivuori
* Rose-Marie Precht as Ellen Salpakari
* Matti Ranin as Lauri Salpakari
* Maija-Leena Soinne as Aune Leppänen
* Jussi Jurkka as Siukola
* Ilkka Keltanen as Opettaja Pentti Rautajärvi
* Martti Järvinen as Ilmari Salpakari

==References==
 

==External links==
*  

 
 
 
 
 
 


 