Strange Culture
{{Infobox film
| name      = Strange Culture
| image     = Poster of the movie Strange Culture.jpg
| starring  = Tilda Swinton Thomas Jay Ryan Peter Coyote Josh Kornbluth
| director  = Lynn Hershman Leeson
| producers = Lynn Hershman Leeson Steven Beer Lise Swenson
| runtime   = 75 minutes
| released  =  
| country   = United States
| language  = English
| music     = The Residents
| awards    =
| budget    =
}}
Strange Culture is a 2007 American documentary film directed by Lynn Hershman Leeson. It stars Tilda Swinton and Thomas Jay Ryan.

It premiered January 19, 2007 at the 2007 Sundance Film Festival.

==Synopsis==
The film examines the case of artist and professor Steve Kurtz, a member of the Critical Art Ensemble (CAE). The work of Kurtz and other CAE members dealt with genetically modified food and other issues of science and public policy. After his wife, Hope, died of heart failure, paramedics arrived and became suspicious when they noticed petri dishes and other scientific equipment related to Kurtzs art in his home. They summoned the FBI, who detained Kurtz within hours on suspicion of bioterrorism.

As Kurtz could not legally talk about the case, the film uses actors to interpret the story, as well as interviews with Kurtz and other figures involved in the case. Through a combination of dramatic reenactment, news footage, animation, and testimonials, the film scrutinizes post-September 11, 2001 attacks|9/11 paranoia and suggests that Kurtz was targeted because his work questions government policies. At the films close, Kurtz and his long-time collaborator Dr. Robert Ferrell, former chair of the Genetics Department at the University of Pittsburgh Graduate School of Public Health, await a trial date.

 , the Buffalo Prosecutor has declined to reopen the case within the 30 day window in which he was allowed to do so.  So, Steve Kurtz is free.

==See also==
* USA PATRIOT Act
* Freedom of speech
* Civil rights

==External links==
*  
*  

 
 
 
 
 

 