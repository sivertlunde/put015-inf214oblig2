Addicted to Love (film)
{{Infobox film
  | name = Addicted to Love
  | image = Addicted to love poster.jpg
  | caption = Theatrical release poster
  | director = Griffin Dunne
  | producer = Jeffrey Silver Robert Newmyer Robert Gordon
  | starring = Meg Ryan Matthew Broderick Kelly Preston Tchéky Karyo Maureen Stapleton Remak Ramsay
  | music = Rachel Portman Andrew Dunn
  | editing = Elizabeth Kling
  | studio = Miramax Films Outlaw Productions
  | distributor = Warner Bros.
  | released = May 23, 1997
  | runtime = 100 minutes
  | country = United States
  | language = English
  | gross = $34,673,095
}}
 Robert Palmers Addicted to Love".

==Plot==
Two pairs of lovers play out a comedy of errors, in which Maggie (Ryan) and Sam (Broderick), an astronomer, try several unethical and nasty tricks to break apart the envied union of their respective former partners, Anton (Karyo) and Linda (Preston).

Good-natured astronomer Sam is devastated when the love of his life, Linda, leaves him for a suave Frenchman named Anton. He therefore does what every other normal dumpee would do: he goes to New York and sets up house (and a camera obscura) in the abandoned building opposite his ex-girlfriends apartment, intent on winning her back and waiting until she decides to leave her current lover. What Sam does not count on is being joined several weeks later by ultra hip tomboy Maggie, a photographer and motorcyclist, who is determined to get revenge on Anton, her ex-fiance. Mutually hostile at first, the two of them eventually join forces in an attempt to separate the couple and ruin Antons life. However, complications ensue when Sam and Maggie start falling for each other. 

==Critical reviews==
The film received mixed reviews. Chicago Sun-Times film critic Roger Ebert panned it as immature, implausible and imbecilic, but still gave it two stars out of a possible four. 

He did not go as far as the Los Angeles Times   Kevin Thomas, who called it creepy and said:

  riot. }}

Addicted to Love currently holds a 57% rating on Rotten Tomatoes.

==Reception==
The advertisements did warn viewers that it would be darker than what Ryan and Broderick are usually associated with, using the taglines "A comedy about lost loves and last laughs" and "A comedy about two people who are getting off on getting even."  However, the film only managed to take $34,673,095 gross at the box office,  several million less than either Ryan  or Brodericks averages. 

==Release==
The film, marking actor Griffin Dunnes directorial debut, was released on May 23, one week before the highly competitive Memorial Day weekend in the United States.

==Box office==
The film opened at #2 at the North American box office making $11.4 million  . 

==Locations==
While the majority of the filming took place where it was set, in the Greenwich Village area of New York City, some shooting was done in Centreville, Delaware and Swarthmore, Pennsylvania. 

==References==
 

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 