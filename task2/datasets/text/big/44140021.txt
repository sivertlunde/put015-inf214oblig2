Penn Simham
{{Infobox film
| name           = Penn Simham
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       = S R Enterprises
| writer         =
| screenplay     = Anuradha Kuthiravattam Pappu Silk Smitha
| music          = Guna Singh
| cinematography =
| editing        =
| studio         = SR Enterprises
| distributor    = SR Enterprises
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by Crossbelt Mani and produced by S R Enterprises. The film stars Ratheesh, Anuradha (actress)|Anuradha, Kuthiravattam Pappu and Silk Smitha in lead roles. The film had musical score by Guna Singh.   

==Cast==
*Ratheesh Anuradha
*Kuthiravattam Pappu
*Silk Smitha

==Soundtrack==
The music was composed by Guna Singh and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaasha Swapnamo || KS Chithra, Jolly Abraham || Sreekumaran Thampi || 
|-
| 2 || Ayyayyo || KS Chithra || Sreekumaran Thampi || 
|-
| 3 || Pachappattu Saari || KS Chithra || Sreekumaran Thampi || 
|-
| 4 || Ponnurukki Poomalayil || KS Chithra, Jolly Abraham || Sreekumaran Thampi || 
|-
| 5 || Sukham Sukham || KS Chithra || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 