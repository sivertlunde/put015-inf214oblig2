Agyaat
 
 
{{Infobox film
| name = Agyaat - Film
| image = Agyaat Movie Poster.jpg
| image_size =
| caption = Theatrical poster
| director = Ram Gopal Varma
| producer = Ronnie Screwvala Ram Gopal Varma
| writer = Nilesh Girkar Puneet Gandhi
| starring = Nitin Kumar Reddy Priyanka Kothari Gautam Rode
| music = Imran Bapi Tutul
| cinematography = Surjodeep Ghosh
| editing = Nipun Gupta
| distributor = UTV Motion Pictures
| released =  
| runtime = 130 minutes
| country = India Telugu
| budget = 5 crore 
}} Telugu as Adavi.  The film is shot at Sigiriya jungle in Sri Lanka. 

==Plot==
A film unit goes for a shoot in a forest, but breaks down deep in the jungle. Resulting in a four-day delay, the 9-member cast and crew of a Bollywood movie, including leading lady Aasha, and her egoistic co-star, Sharman Kapoor; decide to relax and take an outing with their guide, Setu. They end up at a picturesque spot and decide to spend the night there - not realizing they will soon be stalked and brutally killed by a seemingly invincible, unseen and unknown beast and/or an extraterrestrial entity.

==Cast==
*Gautam Rode - Sharman
*Nitin Kumar Reddy - Sujal
*Priyanka Kothari - Aasha
*Ravi Kale - Rakka
*Ishrat Ali - Moorthy
*Harvey Rosemeyer - Jj
*Kali Prasad Mukherjee - Shakky
*Rasika Dugal - Sameera
*Joy Fernandes - Setu
*Ishteyak - Laxman

==Music==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Duration !! Notes
|-
| Jai Shiv Bum Shambu
| Runa Rizvi & Bonnie Chakraborty
| 5:06
| Song starring Gautam Rode and Priyanka Kothari
|-
| 
| Bhaven, Banjotsana & Earl
| 4:34
| Song starring Nitin Kumar Reddy and Priyanka Kothari
|-
| Koobsurat
| Shweta Pandit & Vickeey B. Joshi
| 4:41
| Song starring Nitin Kumar Reddy and Priyanka Kothari (Not included in the film)
|-
| Sunsakte Ho
| Shweta Pandit
| 3:49
| Background piece
|-
| Jungle Jugle
| Jankee & Imran
|  3:43
| Promotional Song starring Priyanka Kothari
|-
| Na Koi
| Keka Ghoshal & Bapi Tutul
| 3:50
| Background piece
|-
| Kiss Mix
| Bhaven, Banjotsana, Earl & Jankee
| 3:43
| Remix
|-
|}

==Reception==
Upon release, the film received mixed reviews. Mihir Fadnavis of India.com, who gave the film 1 and a half star out of 5, noted that "Theres no way Agyaat isnt a major career embarrassment for director Ram Gopal Varma, and a possible career stopper for Nisha Kothari." 

Rajeev Masand of CNN-IBN gave 2/5 (Average) for director Ramgopal Varmas Agyaat; Masand states "At a time when special effects and digital tricks can be used to create just about anything, heres a reminder that what really scares  us is the stuff we cant see". 

Nikhat Kazmi of Times of India gave 2/5 and noted "It neither thrills nor chills with its repetitive running-for-life sequences and its absolutely infuriating end which offers no explanation whatsoever for the scurrying and screaming". 

Subhash K Jha (IBOS) gave 2.5/5 noting "Jungle fever has never been more contagious. Every corner of the Sri Lankan jungle as shot with mesmeric skill by the cinematographer Surjodeep Ghosh is filled with danger.". 

Taran Adarsh gave this movie a rating of 3/5 saying "Ramgopal Varma thinks differently. This time too, the maverick film-maker defies the stereotype and takes to the dense forests of Sri Lanka to meet an unknown force" and explains further "But lets clear the misconception at the very start. Agyaat is no supernatural thriller (like Phoonk) or a horror fest (like Bhoot). This ones more of an adventure thriller that doesnt scare you at all, but makes you a participant in a pulse-pounding journey". 

==References==
 

==External links==
*  
*  

 

 
 
 
 