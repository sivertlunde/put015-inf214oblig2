The Clones of Bruce Lee
 
 
{{Infobox film
 | name = The Clones of Bruce Lee 蛇形三傑
 | director = Joseph Kong Nam Gi-Nam
 | producer = Cheung Chung-Lung Han Sang-Hoon
 | starring = Bruce Le  Dragon Lee Bruce Lai Bruce Thai Bolo Yeung Jon T. Benn  Chiang Tao
 | studio = Dae Yang Films Co., Ltd Film Line Enterprises
 | distributor =   Newport Releasing   Ambassador Film Distributors
 | released =  
 | runtime = 81 min South Korea Hong Kong
 | language = Cantonese
 }}

The Clones of Bruce Lee (Korean: 蛇形三傑 Death Penalty on Three Robots, Taiwan:  複製人李小龍：三龍怒火, Hong Kong: 神威三猛龍) is one of the most notorious exploitation films in cinematic history.  It gathers together several of the Lee imitators who sprang up after Bruce Lees death (including Bruce Le and Dragon Lee), as well as performers from the real Lees films and other veterans of the Hong Kong movie industry.  It has been called "The Mount Rushmore of Bruceploitation films".

==Synopsis==

Immediately after the death of  ), Bruce Lee 2 (Bruce Le), and Bruce Lee 3 (Bruce Lai).  They are trained in martial arts by Bolo Yeung and Chiang Tao. The mission of the clones is to fight crime in Southeast Asia.

Bruce Lee 1 goes undercover as an actor for a corrupt, gold-smuggling producer who plans to have him die on camera.  Meanwhile, the other clones go to Thailand where they meet up with Chuck (Bruce Thai), a local SBI agent who is not a clone but who also resembles Bruce Lee. They have been assigned to kill Dr. Ngai, a mad scientist who is plotting to take over the world with his army of bronze automatons: men whose skin turns to metal when they are injected with Ngais special formula.

The clones successfully complete their missions and return to Hong Kong. But Professor Lucas, disgruntled because he feels he was not properly rewarded by the SBI for creating the clones, pits them against one another. The professors female assistants stop the three clones from fighting amongst themselves, and Lucas sends out a small army of men to dispatch the clones. By the end of the film, Bruce Lee 3 has been killed--but so have all of Professor Lucass henchmen (including the two kung-fu instructors, both defeated by Bruce Lee 1). Bruce Lee 2 finishes off Lucass personal bodyguard and the professor is arrested.

==Reaction==

On the website  , Mark Pollard sums up much of the appeal of the film, writing: " Clones of Bruce Lee is the Plan 9 from Outer Space of Hong Kong cinema. Its the very definition of bad filmmaking and sleazy exploitation. Expect to witness mad scientists bent on world domination, death rays, bronzemen, and naked women frolicking on the beach. Despite some decent kung fu action, particularly with Bolo, its so bad youll laugh or turn it off."  

A review on the website   finds it less enjoyable: "Although the Lee-alikes are in superb shape, and certainly know their Lee chops, there is a deadening sameness about the fight scenes that eventually robs the movie of all joy; this flick is, after all, 95% fight scenes. Ive always preferred the swordplay-oriented kung fu movies- the more weapons, and the more bizarre, the better. Lets face it, the numerous styles notwithstanding (and there are several on display), there are only so many ways to aim a blow at your opponent and only so many ways to block that blow. Without a good fight coordinator, like Lee himself, Jimmy Wang Yu, Samo Hung or a host of others, after forty-five minutes of the same fight over and over, each subsequent fight becomes the kinetic equivalent of white noise: your mind more or less goes on vacation."  

==Trivia==

*Contains the only fight scene between Bruce Le and Dragon Lee.
*Jon T. Benn, who plays Professor Lucas and is familiar to Bruce Lee fans as the mob boss from Way of the Dragon, recently appeared as an American businessman in Jet Lis film Fearless (2006 film) | Fearless.
*One of several appearances in the Bruceploitation subgenre for Bolo Yeung, who won fame for his portrayal of the muscular villain Bolo in Bruce Lees Enter The Dragon.

==External links==
*  
*  
*   &ndash; The Ultimate Guide To Bruce Lee Exploitation Cinema
*   at Korean Movie Database
*   at Korean Powerhouse

 
 
 
 
 
 
 
 
 