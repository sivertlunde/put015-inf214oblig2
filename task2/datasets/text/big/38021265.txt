Prem Kahani (2009 film)
{{Infobox film
| name           = Prem Kahani
| image          = Kannadapremkahani.png
| image_size     =
| caption        = Promotional Poster
| director       = R. Chandru
| producer       =
| writer         =
| narrator       =
| starring       =  
| music          = Ilayaraja
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Kannada
| budget         =
| gross          =
}}

Prem Kahani is an Indian Kannada film directed by R. Chandru, starring Ajay Rao and Sheela (Tamil actress)|Sheela. The films music was composed by Illayaraja. The movie released in September 2009.

==Plot==

The story revolves around a slum dweller and a rich girl.

==Soundtrack==
{|class="wikitable" width ="70%"
! Song name !! Singers
|-
| "Rangu Rangu" || Ilayaraja, Shreya Ghoshal
|-
| "Yaarivanu Yaarivanu" || Sadhana Sargam
|-
| "Yaarivalu Yaarivalu" || Sriram Parthasarathy
|-
| "Badavara Manege" || Sadhana Sargam
|-
| "Shrungaara Bangaara" || Shreya Ghoshal
|-
| "Kogile Koogu Baa" || Bela Shende
|-
| "Giliya Mariyondu" || Ilayaraja
|-
| "Hodadavanae" || Tippu (singer)|Tippu, Rahul Nambiar
|-
| "Nannavale" || Rahul Nambiar
|-
|}

==References==
*http://bellitere.com/sad-kahani-of-prem-kahani.html
*http://movies.rediff.com/review/2009/sep/11/south-kannada-movie-review-prem-kahani.htm

 

 
 
 
 


 