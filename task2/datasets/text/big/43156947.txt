Flashback Memories 3D
{{Infobox film
| name           = Flashback Memories 3D
| image          = 
| alt            = 
| caption        = 
| director       = Tetsuaki Matsue
| producer       = Junji Takane
| writer         = 
| starring       = 
| music          = 
| cinematography = Tomonori Watanabe
| editing        = Daisuke Imai
| studio         = 
| distributor    = 
| released       =   
| runtime        = 72 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 3D music documentary film directed by Tetsuaki Matsue. It was released on 19 January 2013 in Japan. 

==Cast==
*Goma
*Kosuke Tsuji
*Kenta Tajika
*Kyoichi Shiin

==Reception==
The film was in competition at the 25th Tokyo International Film Festival, on October 2012.  It opened the 14th Cinemanila International Film Festival, on 5 December 2012  and also the 1st Helsinki Cine Aasia, on 14 March 2013.  It was also shown at the 13th Japanese Film Festival Nippon Connection in June 2013. 

Mark Adams, on Screen Daily, said the film was "more art installation than formal feature documentary". 

===Accolades===
It was chosen as the 5th best film at the 23rd Japan Film Professional Awards,    as the 8th best Japanese film of the year by film magazine Eiga Geijutsu  and as the 10th best Japanese film of the year by film magazine Kinema Junpo. 
{| class="wikitable sortable" width="90%"
|- style="background:#ccc; text-align:center;"
! Award
! Date
! Category
! Recipients and  nominees
! Result
|-
| rowspan="1"| Tokyo International Film Festival  October 2012
| Audience 
| Flashback Memories 3D
|  
|-
| rowspan="1"| Japan Film Professional Awards  June 2014
| Special
| Tetsuaki Matsue
|  
|}

==References==
 

==External links==
*   
* 

 
 
 
 
 

 
 