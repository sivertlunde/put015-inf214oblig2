Unholy Partners
{{Infobox film
| name           = Unholy Partners
| image          = Unholy Partners poster.jpg
| image_size     =
| caption        =
| director       = Mervyn LeRoy Samuel Marx
| writer         = Earl Baldwin Bartlett Cormack
| narrator       = Edward Arnold Marsha Hunt David Snell George Barnes
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         =
}} Edward Arnold, Marsha Hunt. It was directed by Mervyn LeRoy and released by Metro-Goldwyn-Mayer.

== Synopsis ==
A newspaper reporter, Bruce Corey, returns from the war to New York City. After reporting to his job at his old newspaper, Corey finds that his old editor doesnt like his new ideas.

Corey and his war correspondent friends start their own tabloid style newspaper which will feature "the news before it happens." Corey gambles with a mob boss and wins the money to start up his paper, the New York Mercury, an instant success.

However, because of stories that may implicate the newspapers silent partner in a number of crimes, Corey finds himself and his staff threatened and even the targets of gunfire. Corey finally kills the mob boss and flees the country on a plane that is attempting a trans-Atlantic flight. The plane crashes and he is killed.

==Cast==
*Edward G. Robinson as Bruce Corey Edward Arnold as Merrill Lambert
*Laraine Day as Miss Cronin Marsha Hunt as Gail Fenton
*William T. Orr as Thomas Jarvis
*Don Beddoe as Michael Reynolds

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 