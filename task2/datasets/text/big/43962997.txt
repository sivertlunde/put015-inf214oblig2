The City and the Dogs
 
{{Infobox film
| name           = The City and the Dogs
| image          = 
| caption        = 
| director       = Francisco José Lombardi
| producer       = 
| writer         = Mario Vargas Llosa
| starring       = Alberto Isola
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 135 minutes
| country        = Peru
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Alberto Isola as Mayor Garrido Gustavo Bueno as Lt. Gamboa / Teniente Gamboa Luis Álvarez as El Coronel
* Juan Manuel Ochoa as The Jaguar / El Jaguar
* Eduardo Adrianzén as The Slave / El Esclavo
* Liliana Navarro as Teresa
* Miguel Iza as Arrospide
* Pablo Serra as The Poet / El Poeta
* Jorge Rodríguez Paz as The General / El General

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Peruvian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 