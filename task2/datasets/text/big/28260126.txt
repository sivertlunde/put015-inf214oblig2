Mesrine (1984 film)
{{Infobox film
| name           = Mesrine
| image          = Mesrine 1984 poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical poster
| director       = André Génovès
| producer       = Gérard Croce
| writer         = André Génovès
| starring       = Nicolas Silberg Caroline Aguilar Gérard Sergue
| music          = Jean-Pierre Rusconi
| cinematography = Jean-Claude Couty
| editing        = Martine Rousseau
| studio         = G.R. Productions
| distributor    = Parafrance Films
| released       =  
| runtime        = 110 min.
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Mesrine is a 1984 French film written and directed by André Génovès. A biographical film based on the life of Jacques Mesrine, it focuses on the eighteen months following his escape from La Santé Prison in May 1978 until his death in November 1979. Nicolas Silberg stars as the eponymous lead character.

== Cast ==
* Nicolas Silberg as Jacques Mesrine
* Caroline Aguilar as Sylvia Jeanjacquot
* Gérard Sergue as François Besse
* Michel Poujade as Le commissaire Broussard
* Louis Arbessier as Lelievre
* Claude Faraldo as Charlie Bauer
* Jean-Pierre Pauty as Tillier

== Reception == Little White 2008 film starring Vincent Cassel. Benefield criticized Génovès "bland direction" and "lack of imagination", likening the film to a television production rather than a cinematic release, and noted that "the film fails to come to terms with Mesrine’s controversial glamour and celebrity". 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 

 
 