Aarti (film)
{{Infobox film
| name = Aarti
| image =Aarti62.jpg
| caption = DVD cover
| director = Phani Majumdar
| producer = Tarachand Barjatya
| writer = Vishwamitter Adil Prafulla Desai S. Kumar
| narrator =
| starring =Ashok Kumar Meena Kumari Pradeep Kumar Shashikala Roshan Majrooh Sultanpuri (lyrics)
| cinematography = Pandurang Naik
| editing = G.G. Mayekar
| distributor = Rajshri Productions
| released = 1962
| runtime = 156 mins
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}

Aarti (  Bollywood film directed by Phani Majumdar and produced by Tarachand Barjatya. The film stars Meena Kumari in the title role of Aarti, with Ashok Kumar, Pradeep Kumar and Shashikala appearing in pivotal roles. The film is based on Prafulla Desai, a play written by Prafulla Desai.

==Plot==
An unemployed young man named Deepak (Pradeep Kumar) saves Aarti Gupta (Meena Kumari), a hardworking and dedicated doctor, from drowning, and the two eventually fall in love with each other, although she is already engaged to Dr. Prakash (Ashok Kumar). Although Aartis father firmly disapproves of this relationship, Deepak and Aarti marry.
She moves in with him and his family, which includes his brother, Niranjan (Ramesh Deo), his sister-in-law, Jaswanti (Shashikala), their three children, and Deepaks father (Jagirdar).
That leaves the humiliated Prakash determined to get Aarti back, whatever he may have to do. He manages to bring a discord in their marital life, so much so that Deepak asks Aarti to leave, and she returns home to her father. This is followed by Deepak having a serious accident, when Prakash is the only surgeon who can operate on him. He agrees to operate on him only if Aarti promises to come back to him and forget her husband.

==Cast==
*Ashok Kumar ... Dr. Prakash
*Meena Kumari ... Aarti Gupta
*Pradeep Kumar ... Deepak
*Shashikala ... Jaswanti
*Rajendra Nath ... Jivan
*Keshto Mukherjee ... Johnny
*Jagirdar ... Deepaks dad
*Peace Kanwal ... Kumar (subsequently known as Mehmood)
*Chandrima Bhaduri ... Sarlas mom
*Ramesh Deo ... Niranjan

==Awards== Bengal Film Journalists Association acknowledged Aarti as the ninth-best Indian film of the year, and gave it three additional competitive awards. 
Nominations are listed below.
*Nominated, Filmfare Best Actress Award - Meena Kumari
*Won, Filmfare Best Supporting Actress Award - Shashikala
*Won, BFJA Award for Best Actress (Hindi) - Meena Kumri
*Won, BFJA Award for Best Supporting Actress (Hindi) - Shashikala
*Won, BFJA Award for Best Audiography (Hindi) - R.G. Pushalkar

==Music==
{{Infobox album|
| Name = Aarti
| Type = Album Roshan
| Cover =
| Released =   1962 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Sa Re Ga Ma Roshan
| Reviews =
| Last album = Soorat Aur Seerat (1962)
| This album = Aarti (1962)
| Next album = Dil Hi To Hai (1963)
}}

The soundtrack of the film contains 8 songs. The music is composed by Roshan (music director)|Roshan, with lyrics authored by Majrooh Sultanpuri

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)
|-
|1.
| "Aapne Yaad Dilaya" Mohammed Rafi, Lata Mangeshkar
|-
|2.
| "Ab Kya Misal Doon" Mohammed Rafi
|-
|3.
| "Baar Baar Tohe Kya" Mohammed Rafi, Lata Mangeshkar 
|-
|4.
| "Bane Ho Ek Khak Se" Lata Mangeshkar
|-
|5.
| "Kabhi To Milegi" Lata Mangeshkar
|-
|6.
| "Na Bhanwara Na Koi Gul" Mohammed Rafi, Asha Bhosle
|-
|7.
| "Tere Bin Lage Na Jiya" Mohammed Rafi, Lata Mangeshkar
|-
|8.
| "Woh Teer Dil Pe Chala" Mohammed Rafi, Asha Bhosle
|}

==References==
 

==External links==
* 
* Watch   on YouTube (Official Link)
 
 
 
 
 