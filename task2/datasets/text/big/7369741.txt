Basketball Jones featuring Tyrone Shoelaces
 
  
{{Infobox single  
| Name = Basketball Jones featuring Tyrone Shoelaces
| Cover = Basketball Jones single.jpg
| Cover size =
| Caption =
| Artist = Cheech and Chong
| Album = Los Cochinos
| A-side =
| B-side = Dont Bug Me
| Released = September 1973
| Format =
| Recorded =
| Genre = Comedy rock, funk, psychedelic soul
| Length = 4:04
| Label = Ode Records
| Writer =
| Producer =
| Certification =
| Last single = Sister Mary Elephant
| This single = Basketball Jones Featuring Tyrone Shoelaces
| Next single = Earache My Eye
| Misc =
}}
"Basketball Jones featuring Tyrone Shoelaces" is a song by Cheech and Chong that first appeared on the 1973 album Los Cochinos.
 Wide World of Sports interview conducted by a character named "Red Blazer."  

"Jones" is slang for "craving" or "addiction." A "Basketball Jones" would thus refer to loving basketball so much that it overtakes all other thoughts.
 Tom Scott. The Blossoms and Michelle Phillips (from The Mamas & the Papas) performed vocals as cheerleaders on the track.

The song was released as a single in September 1973 and reached #15 on the Hot 100, becoming the only spoof to peak higher than the corresponding original.  It was backed with "Dont Bug Me", also from Los Cochinos.  To coincide with the graffiti artwork from the albums cover, both sides of the single feature the Ode label covered with graffiti. {{cite AV media notes
| title = Los Cochinos ©#!!*
| titlelink = Los Cochinos
| others = Cheech y Chong
| year = 1973
| type = CD
}} 

Some notes on the recording of the track, taken from the accompanying booklet to "Where Theres Smoke, Theres Cheech & Chong":
&nbsp;... Cheech sings, and Tommy plays piano-thats all it was at first.  In Cheechs words, "George Harrison and those guys were in the next studio recording, and so Lou (Adler) just ran over there and played (it for him).  They made up the track right on the spot."  "That was a wild session," Lou Adler recalls.  "I probably called Carole (King) and told her to come down, but with Harrison and (Klaus) Voorman- I didnt call and say come in and play.  Everyone happened to be in the A&M studios at that particular time, doing different projects.  It was spilling out of the studio into the corridors."  Other members of the ad-hoc all-stars included Nicky Hopkins (piano), Tom Scott (sax), Billy Preston (organ), Jimmy Karstein (drums) and Jim Keltner (percussion).

The song was covered by Barry White and Chris Rock in the 1996 film Space Jam. Rock performed the higher-pitched vocals and shouted lines.
 season 22 episode of The Simpsons A Midsummers Nice Dream.

==Animated film==
 
Basketball Jones is a 1974 animated short film based on the Cheech and Chong song. The cartoon was created to promote the songs release in the United States. It is about a teenager named Tyrone Shoelaces and his love of basketball.  It was rumoured for many years that Ralph Bakshi created the short, when in fact it was designed by animator Paul Gruwell. 
 alley cats, men in business suits, a mountaintop guru, aides to recently "impeached" Richard Nixon (who himself just looks and stares), The Singing Nun, and all four members of The Beatles (former Beatle George Harrison himself plays guitar on the track) sing along to the song.

"Basketball Jones" was originally seen in theaters in early 1974, before showings of The Last Detail at select screens.    It can be seen during the 1974 film California Split, directed by Robert Altman, although its use in the film prevented California Split from being released on VHS or Laserdisc due to Columbia Pictures refusal to pay royalties for the song. Altman later removed the song (but not the cartoon) from the film so it could be released on DVD.
 Tunnel Vision. The film is perhaps best known for being featured in the 1979 Hal Ashby film, Being There, where Peter Sellerss character, Chauncey Gardiner watches the cartoon in a limousine.

==Credits==
* Cheech Marin - Tyrone Shoelaces (voice)
* George Harrison - Guitar
* Klaus Voorman - Bass
* Jim Karstein - Drums
* Jim Keltner - Percussion
* Carole King - Electric Piano
* Nicky Hopkins - Piano Tom Scott - Saxophone
* Billy Preston - Organ
* Cheerleaders:
** Darlene Love  Fanita James Jean King
** Michelle Phillips 
** Ronnie Spector 
* Horny Guys
** George Bohanon  Dick "Slyde" Hyde 
** Paul Hubison 
* Sister Mary Elephant
* Santana Street School 6th Grade Class

==References==
 

 
 
 
 
 
 
 
 
 