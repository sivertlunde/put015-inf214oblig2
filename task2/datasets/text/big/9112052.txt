Babes in Arms (film)
{{Infobox film
| name           = Babes in Arms
| image          = Babes-in-Arms-1937.jpg
| caption        = film poster
| director       = Busby Berkeley
| producer       = Arthur Freed
| screenplay     = Jack McGowan Kay Van Riper
| based_on       = Babes in Arms (stage musical) by Richard Rogers & Lorenz Hart John Meehan Florence Ryerson Annalee Whitmore Edgar Allan Woolf
| starring       = Mickey Rooney Judy Garland
| music          = Songs: Arthur Freed (lyrics) & Nacio Herb Brown (music); Richard Rodgers (music) & Lorenz Hart (lyrics);  Harold Arlen (music) & E. Y. Harburg (lyrics); Roger Edens (music & lyrics) et al.
| cinematography = Ray June Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $745,341 Eyman, Scott. Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 328   . 
| gross = $3,335,000  
| preceded by    =
| followed by    =
}}
 of the Betty Jaynes.

==Plot== Betty Jaynes) sing "You Are My Lucky Star" and "Broadway Rhythm," but Joe says no to their going. So Mickey proposes the kids put on a show, and Don Brice (Douglas McPhail) sings "Babes in Arms" as they march and make a bonfire. Joe dismisses Mickey.
 Margaret Hamilton) and her son Jeff Steele (Rand Brooks) from military school complain to Judge Black (Guy Kibbee) about the Vaudeville kids, but he wont take them from their homes. In a drugstore Mickey and Patsy meet movie star Baby Rosalie Essex (June Preisser), but Mickey gets in a fight with Jeff. Mickey tells Judge Black that his parents show flopped. The judge gives Mickey thirty days to pay damages. Don and Molly sing "Where or When" with an orchestra of children. Mickey has a date with Baby and dines in her house. Mickey wants Baby in the show, which needs $287. She offers to pay it. Mickey smokes a cigar and leaves sick.

Mickey tells Patsy that Baby has to play the lead because of the money. Baby shows how limber she is. Mickey directs rehearsal with Baby and Don, imitating Clark Gable and Lionel Barrymore. Patsy sees Mickey kiss Baby. Mickey tries to stop Patsy from leaving. On the train Patsy sings "I Cried for You." Patsy goes to a theater to see her mother (Ann Shoemaker). Patsy says that Mickey is putting on a show to keep the kids out of an institution. Patsys mother tells Patsy not to quit her show.

Babys father takes her out of the show, and Mickey asks Patsy to go on. In the show Patsy sings "Daddy Was a Minstrel Man." Mickey and Patsy put on black face and sing a medley with Don. Patsy sings "Im Just Wild About Harry," but a storm drives the audience away. Mickey learns that his father quit theater and got an elevator job. Mrs. Steele says the children must report and gives Joe the paper. Mickey gets a letter from producer Maddox (Henry Hull), who liked the show and produces it. As hidden Mickey listens, Maddox asks bitter Joe to teach the youngsters in the show. Mickey introduces the show by singing "Gods Country," which the company contrasts to fascism. Mickey and Patsy satirize FDR and Eleanor and dance.

==Cast==
  
{|
|-
!  !!
|- Mickey Rooney as Mickey Moran
|- Judy Garland as Patsy Barton
|-
|}
 
*Charles Winninger as Joe Moran
*Guy Kibbee as Judge John Black
*June Preisser as Rosalie Essex
*Grace Hayes as Florrie Moran Betty Jaynes as Molly Moran
*Douglas McPhail as Don Brice
*Rand Brooks as Jeff Steele
*Leni Lynn as Dody Martin Margaret Hamilton as Martha Steele 
*Lelah Tyle as Mrs. Brice
 

Cast notes:
*Cliff Edwards appears in a clip from The Hollywood Revue of 1929 of the song Singin in the Rain Charles King appears in a clip from The Broadway Melody

==Production==
The movie was written by Jack McGowan, Kay Van Riper and Annalee Whitmore (uncredited). It was directed by Busby Berkeley — the first film directed  in its entirety at M-G-M by the noted choreographer    — and produced by Arthur Freed.  
 Broadway script vaudevillian parents Good Morning" The Wizard Words and Music. Garland also sang "Johnny One Note" in the same picture.

Filming of Babes in Arms began on May 12, 1939, soon after Garland and Hamilton had finished filming The Wizard of Oz, and was completed on July 18, 1939.
 Franklin and Eleanor Roosevelt; this was edited from the film after FDRs death.  It was thought to be lost, but was discovered on a 16 millimeter reel and restored in the 1990s.

Musical numbers were recorded in stereophonic sound but released to theaters with conventional monaural sound. Recent home video releases feature some of the original stereo recordings. 

The film premiered on October 13, 1939.

==Reception==
The film was one of the ten biggest hits of the year.  According to MGM records it earned $2,311,000 in the US and Canada and $1,024,000 elsewhere resulting in a profit of $1,542,000. 
 Best Music, Scoring by Roger Edens and Georgie Stoll.

==Home video== Strike Up the Band, as well as a fifth disc containing bonus features on Rooney and Garland. {{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title = The Mickey Rooney  & Judy Garland Collection
  | work =
  | publisher =Amazon.com
  | date =
  | url =http://www.amazon.com/Mickey-Rooney-Garland-Collection-Broadway/dp/B000RT99FG/ref=pd_bbs_sr_1/103-3573852-9060615?ie=UTF8&s=dvd&qid=1182367875&sr=1-1
  | format =
  | doi =
  | accessdate =  2008-01-04}} 

==See also==
* List of American films of 1939

==References==
 

==External links==
 
* 
*  
*  
* 
*  on Screen Guild Theater: November 9, 1941

 
 
 

 
 
 
 
 
 
 
 
 
 
 