Homeless for the Holidays
{{Infobox film
| name           = Homeless for the Holidays
| image          = Homeless for the Holidays.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = George A. Johnson
| producer       = George A. Johnson Karen Johnson
| writer         = George A. Johnson
| narrator       = Harley Akers
| starring       = Matt Moore  Crystal Dewitt Hinkle  Cole Brandenberger   Gabrielle Phillips  Brad Stine
| music          = Matthew Wayne Murray	
| cinematography = Tyler Black
| editing        = Axil Ramikus
| studio         = Breathe Motion Pictures
| distributor    =  Bridgestone Multimedia Group
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $30,000
| gross          = $25,180
}}
Homeless for the Holidays is a 2009  .  It is currently in limited release theatrically, including a special screening sponsored by The Dove Foundation at Loeks Theatres, Inc.|Celebration! Cinema in Grand Rapids, Michigan. 

==Plot==
Jack Baker is a self-made executive who lives an upper-middle-class life&ndash;until he loses his job, and finds himself working at a burger restaurant to make ends meet. To make things worse, ends are not being met, and, if something doesnt change soon, his family could lose everything by Christmas.

==Cast==
*Matt Moore as Jack Baker
*Crystal Dewitt-Hinkle as Sheryl Baker
*Cole Brandenberger as Adam Baker
*Gabrielle Phillips as Michelle Baker
*Brad Stine as a Supermarket Manager
*David Sisco as Wesley

==References==
 

==External links==
*  
*  
*   at The New York Times

 
 
 
 
 
 


 