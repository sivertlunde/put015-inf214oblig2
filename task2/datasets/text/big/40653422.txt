Ezhu Sundara Rathrikal
{{Infobox film
| name = Ezhu Sundara Rathrikal
| image = Ezhu_Sundara_Rathrikal.jpg
| image_size = 
| caption = Promotional Poster
| director = Lal Jose
| producer = Rathish Ambat Prakash Varma Jerry John Kallatt James Albert
| narrator = Dileep Rima Kallingal  Murali Gopy
| music = Prashant Pillai
| cinematography =Pratheesh Varma
| editing = Ranjan Abraham
| Producers: Rathish Ambat, Jerry Jhon, Prakash Varma
| studio =  Small Town Cinema LJ Films
| distributor = LJ FILMS & TRICOLOR ENTERTAINMENTS
| released =   
| runtime = 
| country = India
| language = Malayalam
| budget   = 6.70 cr
| gross    =  
}}

Ezhu Sundara Rathrikal (English:Seven beautiful nights) is Malayalam film directed by Lal Jose and starring Dileep (actor)|Dileep, Rima Kallingal,Parvathy Nambiar and Murali Gopy in the lead roles. The film narrates the sequence of events that follow a bachelor party. 

==Plot==
Aby is an ad-film maker. After a failed romance, he has remained a bachelor. His family has been on the lookout for suitable girls for sometime now and finally he agrees to tie the knot. Seven days before the wedding, he hosts a bachelor party. The events that follow are what the film is about.

== Cast == Dileep as Aby
* Rima Kallingal as Sini Alex
* Murali Gopy as Tyson Alex
* Parvathy Nambiar as Ann
* Tini Tom as Franco
* Harishree Ashokan as Abid
* Suraj Venjaramoodu as Prem Raj
* Vijayaraghavan (actor) as City Police Commissioner Varghese
* Sreejith Ravi as Christy Perera
* Sreenivasan as Himself
* Sreejith Vijay
* Sekhar Menon as Vivek
* Arun 
* Ramu as Abys father (Mathew) 
* Anil Scene Stealer Rajgopal as Baijuraj
* Shiju as Alexs friend
* Majeed as Adv. George John
* Praveena as Dr. Daisy Franco
* Suja Menon as Viveks wife
* Ambika Mohan as Aby Mathews mother(Susanamma)
* Surabhi as Jayalakshmi
* Krishnaprabha as Manjusha
* Deepika Mohan as Principals wife

==Production==
 Dileep was Dileep for James Albert after the blockbuster hit Classmates.

==Reception==

The film got mixed reviews from the audience.

==Soundtrack==
{{Infobox album 
| Name = Ezhu Sundara Rathrikal
| Artist = Prashant Pillai
| Type = Soundtrack
| Language = Malayalam
| Label = East Cost Audios
}}


{{tracklist
| headline     =
| extra_column = Singer(s)
| total_length =
| title1       = Koode Irikkaam
| length1 = 5:03
| extra1       = Haricharan, Gayathri
| title2       = Nakshathram Pol
| length2 = 4:03
| extra2       = Preethi Pillai
| title3       = Orkaathe Mayaathe
| length3 = 4:55
| extra3       = Karthik, Charan Raj, Gowri Lakshmi
| title4       = Pettidaam Aarum
| length4 = 3:24
| extra4       = Aalaap Raju, Sankar Sharma 
}}

==References==
 

==External links==
*  


 
 
 