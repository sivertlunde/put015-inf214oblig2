Sønnen fra Amerika
 
{{Infobox film
| name           = Sønnen fra Amerika
| image          = 
| caption        = 
| director       = Jon Iversen
| producer       = Henning Karmark
| writer         = Jon Iversen Paul Sarauw Johan Skjoldborg
| narrator       = 
| starring       = Peter Malberg
| music          = 
| cinematography = Henning Bendtsen
| editing        = Edith Schlüssel
| distributor    = ASA Film
| released       =  
| runtime        = 85 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Sønnen fra Amerika is a 1957 Danish family film directed by Jon Iversen and starring Peter Malberg.

==Cast==
* Peter Malberg - Niels Pind Nielsen
* Poul Reichhardt - Jens Nielsen
* Emil Hass Christensen - Squier Karl Kristian Duue
* Lisbeth Movin - Ingrid
* Sigrid Horne-Rasmussen - Mine Holm
* Kjeld Petersen - The Vet
* Holger Juul Hansen - Teacher Jesper Jensen
* Anna Hagen - Mads
* Preben Kaas - Preben
* Louis Miehe-Renard - Under Manager Palle Reamussen
* Knud Heglund - Grocer Peter Mikkelsen
* Helga Frier - Alma Mikkelsen
* Vera Tørresø - Lise Mikkelsen Poul Petersen - Lawyer Viggo Lange
* Preben Lerdorff Rye - A Worker Henry Nielsen - Member of the Parish Council
* Bjørn Puggaard-Müller - Antique dealer Sejersen
* Lise Thomsen - Daughter at inn
* Henry Lohmann - Waiter at inn
* Marie Brink - Karen
* Carl Ottosen

==External links==
* 

 
 
 
 
 
 


 