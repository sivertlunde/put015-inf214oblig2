The More the Merrier
{{Infobox film
| name           = The More the Merrier
| image          = The More the Merrier - poster.jpg
| image_size     =
| caption        = theatrical poster
| producer       = George Stevens
| director       = George Stevens
| based on       =    Frank Ross Robert W. Russell
| starring       = Jean Arthur Joel McCrea Charles Coburn
| music          = Leigh Harline
| cinematography = Ted Tetzlaff Otto Meyer
| distributor    = Columbia Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $878,000 Dick 1993, p. 160. 
| gross          =
}}
 Frank Ross, Robert Russell, from "Twos a Crowd", an original story by Garson Kanin (uncredited).

This film was remade in 1966 as Walk, Dont Run, with Cary Grant, Samantha Eggar and Jim Hutton.

==Plot==
During World War II, retired millionaire Benjamin Dingle (Charles Coburn) arrives in Washington, D.C. as an adviser on the housing shortage and finds that his hotel suite will not be available for two days. He sees an ad for a roommate and talks the reluctant young woman, Connie Milligan (Jean Arthur), into letting him sublet half of her apartment. Comedy ensues when the two clumsily get in each others way while arising and preparing for work. Connie makes things work by keeping to an exacting schedule, including eating breakfast and leaving for work at precise times.  Then Dingle runs into Sergeant Joe Carter (Joel McCrea), who has no place to stay while he waits to be shipped overseas. Dingle generously rents him half of his half.

When Connie finds out about the new arrangement, she orders them both to leave, but she is forced to relent because she has already spent the mens rent money and cannot refund it. Joe and Connie are attracted to each other, though she is engaged to high-paid bureaucrat Charles J. Pendergast (Richard Gaines). Connies mother married for love, not security, and Connie is determined not to repeat her mistake. Dingle happens to meet Pendergast at a business luncheon and does not like what he sees. He decides that Joe would be a better match for his landlady.

 
One day, Dingle goes too far, reading aloud to Joe from Connies private diary, including her thoughts about Joe. When she finds out, she demands they both leave the next day. Dingle accepts full blame for the incident, and Connie allows Joe to stay the few more days before he is to ship out to Africa. Joe gives Connie an expensive suitcase as a surprise.  They go out to dinner, and have a romantic and affectionate talk. She tells him he may stay there his final night before leaving.

Due to a nosy teenage neighbor, Joe is taken in for questioning as a suspected spy for the Japanese, and Connie is brought along as well. When Dingle and Pendergast show up to vouch for them, it comes out that Joe and Connie are living in the same apartment, despite her engagement to Pendergast. They are eventually released, but the story reaches a reporter. Dingle advises the young couple to get married to avoid a scandal and then to have it annulled later. They follow his advice and wed. However (as Dingle had foreseen), Connies attraction to Joe overcomes her prudence.

==Cast==
As appearing in The More the Merrier, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: August 24, 2013. 
 
* Jean Arthur as Constance Milligan
* Joel McCrea as Joe Carter
* Charles Coburn as Benjamin Dingle
* Richard Gaines as Charles J. Pendergast
* Bruce Bennett as FBI Agent Evans
* Frank Sully as FBI Agent Pike
* Clyde Fillmore as Senator Noonan
* Stanley Clements as Morton Rodakiewicz
* Jean Stevens as Dancer (listed as Peggy Carroll)
 

==Production== The Talk of the Town (1942). Steffen, James.   Turner Classic Movies. Retrieved: August 24, 2013. 

Under fire at Columbia for turning down too many projects, Jean Arthur and her husband Frank Ross invited a friend, Garson Kanin, to create a vehicle for Arthur, and they paid him out of their own pocket. Kanins "Twos a Crowd", with Robert W. Russell, co-writing, received Harry Cohns go-ahead. Other titles considered included, "Washington Story", "Full Steam a Head", "Come One, Come All" and "Merry-Go-Round", which actually tested best with audiences. Testy Washington officials objected to the title and plot elements that suggested "frivolity on the part of Washington workers". The More the Merrier was finally approved as the title. 

==Reception==
  characterizes it as "a delightful and effervescent comedy marked with terrific performances" and praises Coburn as "nothing short of superb, stealing scene after scene with astonishing ease". 
Time Out Film Guide notes, "Despite a belated drift towards sentimentality, this remains a refreshingly intimate movie." 

It currently has a 100% fresh rating on the Rotten Tomatoes website, certifying it as fresh. 

===Awards=== Best Actress Best Director, Best Picture, Best Writing, Best Writing, Screenplay.

==DVD release==
This film is available on Region 1 (USA/Canada).

==References==
Notes
 

Citations
 

Bibliography
 
* Dick,  Bernard. The Merchant Prince of Poverty Row: Harry Cohn of Columbia Pictures. Lexington, Kentucky : University Press of Kentucky, 1993. ISBN 978-0-8-1319-323-6.
* Harrison, P. S. Harrisons Reports and Film Reviews, 1919-1962. Hollywood, California: Hollywood Film Archive, 1997. ISBN 978-0-91361-610-9.
* Maltin, Leonard.  Leonard Maltins Movie Encyclopedia. New York: Dutton, 1994. ISBN 0-525-93635-1.
* Oller, John. Jean Arthur: The Actress Nobody Knew. New York: Limelight Editions, 1997. ISBN 0-87910-278-0.
* Sarvady, Andrea, Molly Haskell and Frank Miller. Leading Ladies: The 50 Most Unforgettable Actresses of the Studio Era. San Francisco: Chronicle Books, 2006. ISBN 0-8118-5248-2.
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 