28 Hotel Rooms
{{Infobox film
| name           = 28 Hotel Rooms
| image          = 28 Hotel Rooms poster.jpg
| image_size     = 200px
| director       = Matt Ross
| producer       = Lynette Howell Louise Runge Samantha Housman
| screenplay     = Matt Ross
| starring       = Chris Messina Marin Ireland
| cinematography = Doug Emmett
| editing        = Joseph Krings
| studio         = Mott Street Pictures OneZero Productions Silverwood Films
| distributor    = Oscilloscope Laboratories
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| gross          = $18,869   
}}
28 Hotel Rooms is an American feature film written and directed by Matt Ross and starring Chris Messina and Marin Ireland. It is Matt Ross first feature film.

==Plot==
A novelist and a corporate accountant conduct an affair over a period of several years, meeting only when they are each traveling for work in a city far from their homes. The film takes a minimalist approach: it consists entirely of scenes between the two of them in hotel rooms. 

==Cast==
* Marin Ireland as woman
* Chris Messina as man
* Robert Deamer as bartender
* Brett Collier as bar patron

==Production==
Matt Ross original idea was to make an intimate film about a relationship that would primarily focus on characters rather than plot. He stated that "the genesis of this movie came out of conversations I had with Chris Messina". 
 Mildred Pierce. The film was edited during the next couple of months and it was determined that certain additional scenes were needed to help further define the relationship between the two characters. These last remaining scenes were shot in New York over the course of about another week.  Filming took in total about 18 days.

Before and during the shooting of the film, Ross encouraged the actors to collaborate in its development, resulting in more than 49 hours of material, some of it scripted and some improvised. During postproduction, many different complete versions of film were created, with scenes in different orders, different plots, and different beginnings or endings, before one version was selected as final. 

==Critical reaction==
Variety found it neither dramatically nor intellectually stimulating despite good chemistry between the lead actors.  Slant gave it 1.5 out of 4 stars.  The Village Voice felt that the characters werent properly fleshed out, and that the films stripped-down, minimalist approach prevented a sense of getting insight into their lives.  The New York Times was also unimpressed, praising the quality of the acting but noting a lack of any dramatic tension drama or passion in the characters relationships with each other.    Rotten Tomatoes has given the film a score of 38%.  Metacritic has given it a score of 50%. 

==References==
 

==External links==
*  at the Internet Movie Database
*  at the Rotten Tomatoes

 