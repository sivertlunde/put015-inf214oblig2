Zebraman
 
{{Infobox film name           = Zebraman image          = Zebraman.jpg
| film name = {{Film name| kana = ゼブラーマン
| romaji = Zeburāman}} caption        = director       = Takashi Miike producer       = writer         = Kankurō Kudō starring       = music          = Kōji Endō cinematography = Kazunari Tanaka editing        = Yasushi Shimamura distributor    = Toei Company released       =   runtime        = 115 minutes country        = Japan language       = Japanese budget         = gross          = $3,070,596 
}}

  is a 2004 film directed by  , was released in 2010 featuring the addition of Masahiro Inoue of Kamen Rider Decade to the cast. 

==Plot==
It is 2010. A failure as a 3rd grade teacher and a family man, Shinichi Ichikawa lives with his cheating wife, his teenage daughter who dates older men, and his son who is bullied because of his fathers presence in the school. Escaping from everyday life, Shinichi secretly dresses up nightly as "Zebraman", the title character from an unpopular 1970s tokusatsu TV series he watched as a child before it was canceled after the seventh episode. As a result of meeting a wheelchair-using transfer student named Shinpei Asano, also a fan of Zebraman, Shinichi not only regains his love for teaching but also develops feelings for the boys mother. At the same time, a rash of strange crimes and murders have been occurring around the school at which Shinichi teaches. On his way to Shinpeis house in his costume to give him a surprise, Shinichi fights a crab-masked serial killer whom he defeats when he starts exhibiting actual superpowers. Confronting more criminals who are possessed by a strange slime-based alien force, Shinichi learns that the Zebraman series was actually a cautionary prophecy of an actual alien invasion written by the schools principal, revealed to be an alien who refused to go through with the invasion and attempted to keep his kind from getting out from below the school before they kill him off and attack in full fury. Though he knows how the show ends, Shinichi defies his predestined fate as he is the only person who can stop the aliens from taking over the Earth. As a result, when the aliens emerge from the ground, the government informs the United States, which will perform an airstrike on the aliens. Realizing this, Shinichi learns of his powers and defeats the aliens.

==Cast==
*Show Aikawa as Shinichi Ichikawa/Zebraman
*Kyoka Suzuki as Kana Asano/Zebra Nurse
*Naoki Yasukochi as Shinpei Asano
*Atsuro Watabe as Oikawa
*Koen Kondo as Segawa
*Makiko Watanabe as Yukiyo Ichikawa
*Yui Ichikawa as Midori Ichikawa
*Yoshimasa Mishima as Kazuki Ichikawa
*Ren Osugi as Kuniharu Kuroda
*Teruyoshi Uchimura as Ippongi
*Akira Emoto as Kitahara the Crab Man
*Ryo Iwamatsu as Kanda
*Yu Tokui as Pyromaniac
*Yoji Boba Tanaka
*Arata Furuta - Eggplant Vendor
*Kumiko Aso as Clerk
*Yoshihiko Hakamada
*Miyako Kawahara
*Hideki Sone
*Masayuki Fukushima
*Satoru Hamaguchi Midoris boyfriend
*Hiroshi Watari - 1978 Zebraman

==Other credits==
*Produced by
**Shigeyuki Endō - planner
**Kumi Fukuchi - planner
**Akio Hattori - producer
**Takashi Hirano - executive producer: TBS
**Mitsuru Kurosawa - executive producer: Toei/Tôei
**Makoto Okada - producer
*Production Design: Akira Sakamoto
*Sound Department: Yoshiya Obara - sound
*CGI producer: Misako Saka
*Lighting Director: Seiichirô Mieno

==Manga==
The movie screenplay was adapted into a five volume manga by Reiji Yamada. The manga told its own story, focusing on the relationship the main character has with his two children. Unlike the movie, Zebraman never gains any powers, he is just a man in a suit.

==References==
 

==External links==
* 
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 