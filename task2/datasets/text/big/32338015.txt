Transformers: Age of Extinction
 
{{Infobox film
| name           = Transformers: Age of Extinction
| image          = Transformers Age of Extinction Poster.jpeg
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Bay
| producer       = {{Plainlist| 
* Don Murphy
* Tom DeSanto
* Lorenzo di Bonaventura
* Ian Bryce
}}
| writer         = Ehren Kruger
| based on       =  
| starring       = {{Plainlist| 
* Mark Wahlberg
* Stanley Tucci
* Kelsey Grammer
* Nicola Peltz
* Jack Reynor
* Sophia Myles
* Li Bingbing
* Titus Welliver
* T. J. Miller
}}
| music          = Steve Jablonsky
| cinematography = Amir Mokri
| editing        = {{Plainlist| 
* William Goldenberg Roger Barton
* Paul Rubell
}}
| studio         = {{Plainlist|
* di Bonaventura Pictures
* Hasbro China Movie Channel   
* Jiaflix Enterprises }}
| distributor    = Paramount Pictures
| released       =   
| runtime        = 165 minutes  
| country        = United States China   
| language       = English 
| budget         = $210 million    
| gross          = $1.091 billion 
}}
Transformers: Age of Extinction is a 2014   and a soft reboot of the franchise, and takes place five years later, after the Decepticon invasion of Chicago. Like its predecessors, the film is directed by  . The film features an entirely new cast of human characters and is the first in the series to feature the Dinobots. Returning Transformers include Optimus Prime, Bumblebee, Ratchet, Leadfoot, and Brains.  The film was released on June 27, 2014, in IMAX and 3D film|3D. 
 Worst Picture Worst Prequel, Worst Director Worst Supporting tenth highest-grossing film of all time.

A fifth installment is set for a 2017 release. However, the fifth film will not see Bay returning as director. Wahlberg is set to reprise his role.

==Plot==
 

Sixty-five million years ago, a race of aliens called the "Quintessons|Creators" begin wiping out most life on Earth. In the present, somewhere in the Arctic, geologist Darcy Tirrel discovers dinosaur corpses covered in a strange metal.

Five years after the  , humanity has grown wary and fearful of the Transformers. Cemetery Wind, an elite CIA black ops unit, is formed by paranoid agent Harold Attinger and team leader James Savoy to hunt down the remaining Decepticons. However, they have also been secretly hunting down the Autobots, believing all Transformers to be a threat, despite the Autobots officially being granted sanctuary by the government. With help from the Cybertronian bounty hunter Lockdown (Transformers)|Lockdown, they ambush and brutally kill Ratchet (Transformers)|Ratchet. However, their primary target is Optimus Prime, whom Lockdown personally wants alive.
 Crosshairs - in a desert. Cade hacks into a spy drone he took from the Texas attack, learning of a technology firm called KSI (Kinetic Solutions Incorporated) in league with Cemetery Wind, and they decide to infiltrate KSI HQ in Chicago, with Optimus vowing to kill Attinger for his crimes and actions against Autobots.
 Brains to decode dead Transformers minds and utilize their data for human-created Transformers. Joshua shows Darcy his prized creation Galvatron, created using data from Megatrons severed head. The Autobots storm the facility, but Joshua stops them, proclaiming the Autobots are no longer needed now that they can create their own Transformers. Disillusioned, Optimus and the Autobots decide to leave.

Forced by Attinger, Joshua launches Galvatron and Stinger (a man-made Transformer similar to Bumblebee) to pursue the Autobots. Optimus fights Galvatron before Lockdown wounds him, capturing him and Tessa on his ship. He explains that the beings he calls the Creators want Optimus back. Before leaving Earth, Lockdown gives Cemetery Wind a Seed, giving the Autobots time to board the ship. Cade and Shane save Tessa and escape with Bumblebee while the others save Optimus and escape on a detachable ship. Optimus and Brains reveal that Galvatron is Megatron reborn and has manipulated KSI in order to steal the Seed and create more Decepticons. Cade warns Joshua, who retreated to Beijing to use production facilities there, about Galvatron and Attinger. Joshua backs off his deal with Attinger while Galvatron activates himself and infects all the KSI prototypes, turning them into the new Decepticon army. Joshua flees with the Seed to Hong Kong, with Attinger and Galvatron chasing him.

When the Autobots try to retrieve the Seed, their ship is shot down by the Decepticons, leaving the Yeagers, Hound and Bumblebee to fight. After Cade kills Savoy in a fist fight, the Decepticons leave the Autobots outnumbered. Knowing this, Optimus releases and tames the legendary warriors known as the Dinobots. With their help, they defeat the Decepticons while Bumblebee kills Stinger at the battles climax. However, Lockdown returns with a magnetic weapon to reclaim Optimus and the Dinobots. Optimus destroys it and fights Lockdown. Cade tries to help, but is held at gunpoint by Attinger, who is angered by Cades choice to side with the Autobots. Optimus saves Cade by killing Attinger, but is immediately impaled by Lockdown with his sword to a wall. As Cade and Bumblebee hold off and distract Lockdown, Tessa and Shane use a tow truck to pull the sword out of Optimus, who then kills Lockdown. They finish off the remaining Decepticons as a retreating Galvatron vows to fight Optimus another day. Optimus sets the Dinobots free but decides to confront the Creators, knowing the Seed must be kept away from Earth. He requests the Autobots to protect Cade and his family before flying into space with the Seed, sending a message to the Creators that he is coming for them.

==Cast==
 

===Humans===
 
* Mark Wahlberg as Cade Yeager, a single father and struggling inventor, who discovers Optimus Prime and is drawn into the Transformers war.   
* Stanley Tucci as Joshua Joyce, the arrogant head of KSI who wants to build his own Transformers.  
* Kelsey Grammer as Harold Attinger, a paranoid CIA agent who created the Cemetery Wind to eliminate all Transformers from Earth. 
* Nicola Peltz as Tessa Yeager, Cades daughter who is secretly dating Shane and has a straining relationship with Cade.    
* Jack Reynor as Shane Dyson, Tessas boyfriend and an Irish race car driver.    
* Titus Welliver as James Savoy, field leader of the Cemetery Wind working for Attinger.   
* Sophia Myles as Darcy Tirrel, Joshuas geologist assistant. {{cite web|url=http://www.thewrap.com/movies/column-post/sophia-myles-join-mark-wahlberg-
michael-bays-transformers-4-exclusive-89661 |title=Sophia Myles to Join Mark Wahlberg in Michael Bays Transformers 4 (Exclusive) |publisher=Thewrap.com |date=2013-05-06 |accessdate=2014-07-13}}   
* Li Bingbing as Su Yueming (苏月明 Sū Yuèmíng), owner of the Chinese factory used by KSI to build their artificial Transformers.   
* T. J. Miller as Lucas Flannery, Cades best friend and a mechanic. 
* James Bachman as Gill Wembley, a scientist working for Joshua. Thomas Lennon as Greg, the White House Chief of Staff. Charles Parnell as the CIA Director.
 Michael Wong was cast as a Hong Kong police officer.  Han Geng had a cameo, singing and playing the guitar in a parked car that is magnetized by Lockdowns ship.    General Motors Vice President of Design Edward T. Welburn had a cameo appearance as a KSI executive.  Director Michael Bay also cameoed in the film as the driver of the truck where Optimus Prime and Bumblebee destroy during their fight against Galvatron.

===Transformers===

====Autobots==== Marmon 97 Western Star 4900 Phantom Custom semi-trailer truck. Optimus has since lost his faith in humanity after the ambush that caused his injury and even considers leaving the planet until Cade convinces him to reconsider his faith in humanity.                1967 Camaro 2014 Chevrolet Camaro concept.    Oshkosh Defense Medium Tactical Vehicle.   
* Ken Watanabe voices Drift (Transformers)|Drift, an Autobot tactician and a former Decepticon who transforms into a black and blue 2013 Bugatti Veyron Grand Sport Vitesse and a helicopter.    2014 Chevrolet Corvette C7 Stingray.    Wrecker who transforms into an armored version of the #42 Earnhardt Ganassi Racing Target car. 
* Reno Wilson voices Brains (Transformers)|Brains, a former Decepticon drone turned Autobot. 

====Dinobots==== Tyrannosaurus Rex.  
* Scorn (Transformers)|Scorn, the Dinobots demolition specialist who transforms into a mechanical three-sailed Spinosaurus.  
* Slag (Transformers)|Slug, the savage destroyer amongst the Dinobots who transforms into a mechanical spiked and bestial Triceratops.  
* Strafe (Transformers)|Strafe, a Dinobot who specializes in assault infantry and transforms into a mechanical two-headed and two-tailed Pteranodon.    

====Decepticons====
* Frank Welker voices Galvatron, a human-made Transformer who becomes possessed by Megatrons mind, thus making him a new body for the Decepticon leader. He molecularity transforms into a black and grey 2014 Freightliner Argosy cab over trailer truck. He has been secretly manipulating the human governments into creating their own Transformers for him to control so he can steal the Seed for himself and rebuild the Decepticon army.      
* Stinger, a human-made Transformer inspired by Bumblebee who becomes a Decepticon spy and sabotage specialist. He molecularly transforms into a red and black 2013 Pagani Huayra.    
* Junkheap, a human-made Transformer who becomes a Decepticon under Galvatrons control. He molecularly transforms into a Isuzu Giga garbage truck of Waste Management, Inc.  Shockwave who become Decepticons under Galvatrons control.
* Traxes, mass-produced human-made drones who become Decepticons under Galvatrons control. They molecularly transform into different colored Chevrolet Traxes. 

====Others==== Mark Ryan voices Lockdown (Transformers)|Lockdown, an intergalactic bounty hunter, aligned with neither side, who transforms into a grey 2013 Lamborghini Aventador LP 700–4 Coupe, is working with Attinger to capture Optimus Prime for the humans in exchange for the Seed from him, being provided unlimited access to all of Attingers CIA resources. Lockdown is working for unknown alien life forms whom he claims are the creators of the Transformers.   He has a group of alien humanoid mercenaries and a pack of techno-organic wolves called Steeljaws under his command. The Creators, an unknown race of aliens whom Lockdown claims to be the Transformers creators. They are seen very briefly in the films opening when they use multiple Seeds to cyberform Earth, exterminating the dinosaurs. Years later, Lockdown was hired to apprehend Optimus Prime for unknown reasons.

==Production==

===Development===
During production for Dark of the Moon, Shia LaBeouf and Bay confirmed that they would not return for a fourth installment of the franchise.  Roland Emmerich, Joe Johnston, Jon Turteltaub, Stephen Sommers, Louis Leterrier and David Yates were rumored to replace Bay.  Jason Statham was rumored to star in the fourth installment. Hasbro CEO Brian Goldner revealed that he was able to announce the film as he was talking with Steven Spielberg, Bay and Paramount. 
There were rumors that the fourth and fifth installment would be shot Back to back film production|back-to-back with Statham as the lead role, which he and Bay denied.  Spielberg hoped Bay would return for a fourth installment. 
 China Movie Channel and Jiaflix Enterprises would international co-production|co-produce the film with Paramount.  

On September 1, 2013, Fusible revealed three possible subtitles for the film, which were Last Stand, Future Cast, and Apocalypse. On September 2, TFW 2005 revealed one last possible title, Age of Extinction.   On September 3, 2013, Paramount released an official teaser poster for the film, revealing the title to be Transformers: Age of Extinction. 

===Casting===
In November 2012,  , Nicola Peltz, Gabriella Wilde, and Margaret Qualley were all considered for the role of the daughter, while Luke Grimes, Landon Liboiron, Brenton Thwaites, Jack Reynor, and Hunter Parrish were all considered for the boyfriend. The leads are contracted for three films.    In January 2013, Reynor was cast as the boyfriend,  and in March 2013, Nicola Peltz was cast as Wahlbergs daughter. 

Peter Cullen reprises his role as the voice of Optimus Prime.  Glenn Morshower stated in September 2012 that he would appear in the next two films, reprising his role of General Morshower, but Morshower announced in May 2013 that he would not be able to appear in the new films due to a scheduling conflict.  In April 2013, Bay revealed that actor Stanley Tucci had joined the cast.    On May 1, 2013, actor Kelsey Grammer was cast as the lead human villain named Harold Attinger.  On May 6, 2013, actress Sophia Myles was cast in a major role.  That same month, Chinese actress Li Bingbing and comedian T. J. Miller joined the cast.  On July 14, 2013, Bay announced that Han Geng had joined the cast.  That same month, Titus Welliver also joined the cast. 

===Filming=== C7 Corvette Lockdown were Bois Blanc Islands amusement park were partially restored as props for the film. 

===Incidents===
On October 17, 2013, while filming in Hong Kong, Bay was assaulted by two brothers surnamed Mak, who demanded a payment of HK$100,000 (US$12,900). The elder brother had also assaulted three police officers during the incident. Both brothers and a third man surnamed Chan were arrested on suspicion of assault, with the younger Mak also charged on suspicion of blackmail.  The Mak brothers pleaded guilty to both charges in February 2014. The two brothers were incarcerated, with the prosecutor citing that the case had attracted a great deal of media attention and affected Hong Kongs image. 

===Post-production===
Industrial Light & Magics VFX supervisor Scott Farrar who supervised on all four films in the Transformers franchise was brought in to render the visual effects. He said the film contains about 90 minutes (of the 165 minutes) of visual effects.  Farrer said that it was the biggest project that he ever got involved in which consisted of the largest crew of his career, noting that there were over 500 crews working in different facilities.   

There were 9 different formats used in the film which included IMAX film, IMAX digital, single-frame anamorphic film, GoPros, crashcams, Red cameras on 3Ality sterio 3D gigs and red cameras for 2D. 

==Music==
 

Like its predecessors, Steve Jablonsky composed the films score, marking his sixth film collaboration with director Michael Bay, four of them which were Transformers films. The films score were praised by critics and fans. The score album sold more than 15,000 units worldwide. 

Skrillex worked on sound design for the film, having said that he was creating "the craziest Skrillex sounds I could ever make" and mentioned working on sounds for the Dinobots.  
 Battle Cry", that was implemented in key parts of the film by Bay. Imagine Dragons also worked with Steve Jablonsky and Hans Zimmer to contribute additional music to the films score.   
 Until Its Gone" is included in the video game soundtrack of the movie. 

On June 30, 2014, an EP was released on iTunes, featuring four tracks from the soundtrack score. 

==Release== world premiere in Hong Kong on June 19, with a live concert by Imagine Dragons. 

===Marketing=== The Walking Dead. Another shortened version of the teaser trailer was aired during the 2014 MTV Movie Awards.

Chevrolet aired a commercial at the New York International Auto Show featuring General Motors vehicles with clips from the film, along with putting them on display. 
 The Voice on May 12, 2014.

DeNA and Hasbro teamed up to construct an official mobile video game for the film. The game was first announced on May 13, 2014, though, the title is still in active development.  Also on this date, Oreo launched a marketing campaign to promote the film.  This included a television commercial where a boy gives a wounded Optimus Prime an Oreo cookie to continue the fight. 

An exclusive theatrical trailer debuted on May 15 on iTunes Movie Trailers at 12:01 AM Pacific Standard Time.    On May 21, 2014, two television spots appeared online, both containing new footage from the film.    The films viral campaign updated on May 22, showcasing all-new posters and realistic news reports of the damage done to Chicago from the third film.    Three more television spots, all sporting new footage, appeared online on May 30, 2014.   

Imagine Dragonss single for the film officially released online on June 2, 2014. 
 Twin Cities the Canadian The Late Show with David Letterman on June 9, 2014. During his visit, the very first clip from the film debuted, showcasing Grammers character and Wahlbergs in a heated argument. During the first commercial break for the show, a brand new television spot aired.  On June 10, 2014, two television spots appeared online, both containing extensive new footage from the film. 

Three more television spots appeared online on June 14, 2014, containing new footage of the Dinobots transforming and of Lockdown speaking.   On June 17, a brand new television spot aired on Comedy Central containing new footage.  Another television spot appeared online on June 18, sporting new footage as well. 

===Video games===
 
In February 2014, Transformers: Rise of the Dark Spark, developed by Edge of Reality, published by Activision was announced as a companion to the film. It was released in June 2014 for Microsoft Windows, Nintendo 3DS, PlayStation 3, PlayStation 4, Wii U, Xbox 360, and Xbox One.   

  Rovio and Hasbro announced Angry Birds Transformers. The game has Transformers movie designs on two of the characters.

==Reception==
===Box office===
====Worldwide====
 
Despite being panned by critics, Transformers: Age of Extinction was a box office success like its  . It grossed $245,439,076 in the United States and $845,966,021 in other territories, for a worldwide total of $1,091,405,097.  Furthermore, it was the only film of 2014 to earn over $1 billion at the box office worldwide.  Calculating in all expenses,  , as well as the 2014 in film|highest-grossing film of 2014, the second highest-grossing film in the Transformers film series, the eleventh highest-grossing film of Paramount (domestically).  It is the second film in the Transformers installment to earn over $1 billion following Dark of the Moon  and the nineteenth film overall.

==== North America ====
In the U.S. and Canada, Transformers: Age of Extinction is the fifth highest-grossing film of 2014.  It was released on June 27, 2014 in across 4,233 theaters in North America. It earned $8.75 million from Thursday late-night run, which was the fifth biggest of 2014.  On Friday the film grossed an additional $31.25 million bringing its total day gross to $41.6 million, which includes $10.7 million from IMAX theaters. In its opening weekend the film earned $100,038,390  setting an opening record of 2014 (overtaken by   with $121.9 million),  which is the fourth-highest opening for Paramount,  and the fourth-highest for a film released in June behind   ($108 million).  The opening-weekend audience was evenly split among those under and over the age of 25, with 58%, male, 64%, and 27% under 18 years old. {{cite web| url = http://www.forbes.com/sites/scottmendelson/2014/06/28/box-office-transformers-4-scores-41m-friday-for-likely-100m-debut/ | title = 
Box Office: Transformers 4 Scores $41.6M Friday For Likely $100M+ Debut | first = Scott | last = Mendelson | work = Forbes | date = June 28, 2014 | accessdate = June 29, 2014}}  The film remained at the summit for two consecutive weekends before being overtaken by Dawn of the Planet of the Apes in its third weekend. It also cross the $200 million mark in its third weekend becoming the fifth film of 2014 to do so.   The film closed down its theatrical run on October 9, 2014    and earned a total of $245,439,076, making it the fifth highest-grossing film of 2014 but the lowest-grossing in the Transformers film series.    

==== Outside North America ====
Outside North America, it is the highest-grossing film of 2014,  and the sixth-highest grossing film.  Transformers: Age of Extinction was released internationally on more than 10,152 screens.    It set an international opening record earning $202.1 million from 37 foreign markets which is the eight-largest overseas openings of all time and the highest of 2014 (previously held by  ) of which $16.6 million came from IMAX showings.  Its biggest opener outside the U.S. was in   ($92.46 million).   The film set an all time IMAX opening record with nearly $10 million.  After five days of its release, the fourth Transformers movie surpassed its North American run with $134.5 million.   In Russia the film opened at number one with $21.7 million from 1,100 screens which is the second largest in the territory for which 3D accounted for 80% of the total gross. It earned $2.6 million from IMAX screens.  
 The Amazing Spider-Man ($5.7 million) and has so far earned $10.22 million at the Indian box office.     

Age of Extinction earned $95.8 (down 52%) million in its second week from 37 territories. In China, the film earned an additional $50.9 million in its second weekend for a total of $212.8 million. In only 10 days of its release, it became the List of highest-grossing films in China|highest-grossing film in China with $222.74 million, thus overtaking Avatar (2009 film)|Avatar  s previous record.    Adding to the films revenue and popularity were product placements of Chinese brands edited into the movie specifically for Chinese audiences. 

The film topped the box office outside North America for four consecutive weekends despite coinciding with the 2014 FIFA World Cup before being overtaken by Dawn of the Planet of the Apes in its fifth weekend. 

Transformers: Age of Extinction once became the List of highest-grossing films in China|highest-grossing film in China, with $301–$320 million         }} in revenue surpassing 2009s record set by Avatar ($204 million),    until it was surpassed by Furious 7 in 2015 with $323 million.  It is also the first movie in China to gross more than $300 million at the box office.  

At the end of its theatrical run outside North America, the film earned $845,966,021 which is 77.5% of its total gross. The highest revenue came from China ($301 million), Russia ($45.2 million), South Korea ($43.3 million), Germany ($38.2 million), Mexico ($33.5 million) and the UK ($33.1 million).  It became the highest-grossing international film of 2014 with over $845.3 million. 

===Critical reception===
As of December 2014, based on 179 reviews collected by the  , the film has a score of 32 out of 100, based on 37 critics, indicating  "generally unfavorable reviews".   In CinemaScore polls, users gave the film an "A-" on an F to A+ scale compared to the "B+" that the original film had scored and "A" for both the second and third movie. 

<!-- Professional reviews only&nbsp;— no reviews from YouTube-based critics such as Jeremy Jahns, Chris Stuckmann, Schmoes Know, etc.!

Haha, I was just thinking of Jeremys review when I read this notification :D -->
  movie."  Peter Travers of Rolling Stone gave the film zero out of four stars, calling it "the worst and most worthless Transformers movie yet."  Kyle Smith of the New York Post gave the film one-and-a-half out of four stars, commenting that "This series was never good, but it was once fun, or at least flashy. Now that its gears have gone rusty, it’s time for an Alien vs. Predator-style rethink."  A. O. Scott of The New York Times said in his review that: "The story is scaffolding for the action, and like every other standing structure it is wrecked in a thunderous shower of metal, glass, masonry and earth." 

Clarence Tsui of   masquerading as character development, its a toss-up if thats better or worse than seeing clattering collections of   junk." 

===Home media=== digital download through iTunes and Google Play on September 16, 2014.

==Controversy==
===Authenticity over North American box office opening===
The $100 million opening for Transformers: Age of Extinction is disputed within the industry.  According to Rentrak—which has a direct line into the vast majority of theaters in the United States and Canada to track actual ticket sales—about 4,100 of the 4,233 theaters playing the film generated $95.9 million. The projected total from the Rentrak sales data would put the opening three-day weekend gross at around $97.5 million. For Transformers to have crossed the $100 million threshold, it would have needed to gross more than the nation-wide average in the theaters not tracked by Rentrak. Some media outlets have elected to go with the Rentrak figure.  

==Accolades==
 
{| class="wikitable" style="width:99%;"
|-
! Award !! Category !! Winner/Nominee !! Result
|- 2014 Teen Choice Awards
| Choice Movie: Villain
| Kelsey Grammer
|  
|-
| Choice Movie: Breakout Star
| Nicola Peltz
|  
|-
| Choice Summer Movie
| Transformers: Age of Extinction
|  
|-
| Choice Summer Movie Star
| Mark Wahlberg
|  
|- Golden Trailer 2014 Golden Trailer Awards
| Best Summer 2014 Blockbuster Trailer
| Paramount Pictures and Creative Buzz Industries
|  
|-
| Best Summer Blockbuster 2014 TV Spot
| Paramount Pictures and Creative Buzz Industries
|  
|- 2014 Hollywood Film Awards
| Best Visual Effects of the Year
| Scott Farrar
|  
|- 2014 Houston Film Critics Society Awards
| Worst Film
| Transformers: Age of Extinction
|  
|- 2015 Annie Awards
| Outstanding Achievement, Animated Effects in a Live Action Production
| Michael Balog, Jim Van Allen, Rick Hankins, and John Hansen
|  
|- 2015 Satellite Awards Best Visual Effects
| John Frazier, Patrick Tubach, Scott Benza, and Scott Farrar
|  
|- Best Sound (Editing & Mixing)
| Erik Aadahl, Ethan Van Der Ryn, and Peter J. Devlin
|  
|- 2015 Golden Raspberry Awards Worst Picture
| Transformers: Age of Extinction
|  
|- Worst Director
| Michael Bay
|  
|- Worst Supporting Actor
|  , and Think Like a Man Too) 
|  
|- Worst Supporting Actress
| Nicola Peltz
|  
|- Worst Screenplay
| Ehren Kruger
|  
|- Worst Prequel, Remake, Rip-off or Sequel
| Transformers: Age of Extinction
|  
|- Worst Screen Combo
| Transformers: Age of Extinction
|  
|}

==Sequels== The Gambler, Walhberg confirmed that he will return for another installment.  Peter Cullen (who voiced Optimus Prime in the film series) commented about Transformers 5 and a possible sixth installment stating that both films will go back to more of their original roots.  According to his Facebook and Twitter page, composer Steve Jablonsky has said that his involvement for the sequel is still undetermined. In March 2015, it was reported that the studio was in talks with Akiva Goldsman to write the fifth film.   In April 2015, Hasbro CEO Brian Goldner confirmed Goldsmans involvement, announcing that Goldsman would be "leading a group of writers to really create a strategic plan around Transformers". Goldner also stated that he "would expect the sequel to Transformers: Age of Extinction to happen in 2017." 

==See also==
 
* List of films featuring drones

==References==
 
 
 

==External links==
 
 

*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 