Fanny Hill (1964 film)
{{Infobox film
| name           = Fanny Hill
| image_size     = 
| image	=	Fanny Hill FilmPoster.jpeg
| caption        = 
| director       = Russ Meyer Albert Zugsmith (Uncredited)
| producer       = Artur Brauner Albert Zugsmith
| writer         = John Cleland (Novel) Robert Hill
| narrator       = 
| starring       = Letícia Román Miriam Hopkins Ulli Lommel
| music          = Erwin Halletz	
| cinematography = Heinz Hölscher
| editing        = Alfred Srp
| distributor    = Gloria Filmverleih AG (West Germany) Pan World (U.S.)
| released       =  
| runtime        = 85 min (Cut version) 96 min (Germany) 104 min (U.S.)
| country        = United States
| language       = English
| budget         = 
| gross          =
| website        = 
}} novel of the same name.

==Partial cast==
* Letícia Román as Fanny Hill
* Miriam Hopkins as Mrs. Brown
* Ulli Lommel as Charles
* Chris Howland as Mr. Norbert
* Helmut Weiss as Mr. Dinklespieler
* Karin Evans as Martha  
* Alexander DArcy as Admiral  
* Christiane Schmidtmer as Fiona

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 