Parker (2013 film)
 
{{Infobox film
| name           = Parker
| image          = Parker 2013 Movie Poster.png
| caption        = Theatrical release poster
| director       = Taylor Hackford
| producer       =  
| screenplay     = John J. McLaughlin
| based on       =  
| starring       = Jason Statham Jennifer Lopez 
| music          =  
| cinematography = J. Michael Muro Mark Warner
| studio         =  
| distributor    = FilmDistrict
| released       =  
| runtime        = 118 minutes  
| country        = United States
| language       = English
| budget         = $35 million
| gross          = $48.5 million
}}
 crime Thriller thriller film Parker novel, written by Donald Westlake under the name Richard Stark.
 Emma Booth).

Parker marked a departure in Hackfords career, as he had hoped to make it his first film noir. The film, produced on a "mid-30s" budget, was conceived following Westlakes 2008 death, when producer Les Alexander secured the rights to it.
 film critics comedic relief. It grossed $48.5 million worldwide at the box office. 

== Plot == Parker (Jason Statham) is a professional thief, specializing in planning big robberies. His mentor Hurley (Nick Nolte) asks him take charge of a job with a crew he doesnt know, consisting of Melander (Michael Chiklis), saving Parker a parking spot, Carlson (Wendell Pierce), Ross (Clifton Collins Jr.), and Hardwicke (Michah Hauptman). The job, taking the gate money from the Ohio State Fair, is successful, but Hardwicke ignored instructions, resulting with needlessly killing a man in a fire set as a distraction.

Parker, disgusted with the crews unprofessional standards, refuses to participate in another robbery that could net them millions. Needing his share of the loot to finance the bigger job, and fearing he might take revenge, the crew decides to shoot Parker and leave him to die alongside a road. Hardwicke was to finish him off, shooting ground, he is found by a family of tomato farmers who take him to the hospital, where he subdues a male nurse, steals his uniform, and escapes. He then robs a check-cashing store, shooting one proprietor in the leg and stealing a womans car.
 Emma Booth). She narrowly escapes and goes into hiding at a Fish Camp. Hurley, who is Claires father, is worried and suggests Parker run away with her, but he refuses, completely intent on revenge against Melander.

Obtaining in Palm Beach a false ID as an Ecuador oil family immigrant Texan named Daniel Parmitt, looking for a place to live. Leslie Rodgers (Jennifer Lopez) is a depressed, unsuccessful real-estate agent living with her mother (Patti LuPone), struggling financially after a divorce. She is thrilled when Parker (as Parmitt) appears to become interested in her properties because she is desperate for a commission.

Leslie soon becomes suspicious when Parker shows interest only in a house that a man named Rodrigo recently purchased and is remodelling. In reality, Rodrigo is Melander, staying in the house with the crew in anticipation of a $50 million jewelry auction they plan to rob. Parker returns to plant his guns, find their weapons, disabling firing pins.

Leslie eventually finds out that Parker is using a fake identity. She offers her knowledge of the area in exchange for a commission. He considers it only after making her strip to show she isnt wearing a wire. Together, they plan to steal the jewels from Melander after he robs them from the auction. Leslie makes a pass at Parker, but he remains distant, though obviously attracted to her.
 Palm Beach Sheriffs Deputy Jake Fernandez (Bobby Cannavale) arrives with questions for Leslie after learning that she was in business with Daniel Parmitt. She is shocked when she discovers a bloody Parker hiding in her house with her mothers permission. At Parkers request, she contacts Claire, who comes to stitch up his wounds. The subsequent encounter makes it clear to Leslie that Claire is the woman in his life.

The crew successfully steals the jewels at the auction. They swim back to the house, where Parker has already arrived. Worried that Parker might need help, Leslie makes her way to the house and begins snooping around the garden. She is found and taken inside, where the crew abuse and question her, assuming she and Parker are working together. Parker kills Ross when he goes outside by stabbing him in the neck; others begin to panic.

Melander finds Parker and a fight ensues. Carlson starts to molest Leslie, but she shoots him multiple times with a gun she noticed under the table that Parker had planted. Parker, in spite of his wounds, is able to kill Melander. All members of the crew end up dead. Parker and Leslie arrange for the jewels to be hidden and for her to receive her cut. She tells him she knows now she never had a chance to be with him, and they part ways—Parker showing some regret as she leaves.

Six months later, Parker goes to Chicago and kills the syndicate boss who hired Kroll to kill him. A year later, Leslie receives two hefty boxes in the mail containing several million dollars. In the final scene before the credits, the tomato farmers who saved Parkers life are talking to somebody about how they got all this money that changed their lives. They credit the stranger they rescued, who they think was an angel sent to test them.

== Cast ==
*Jason Statham as Parker 
*Jennifer Lopez as Leslie Rodgers 
*Michael Chiklis as Melander 
*Wendell Pierce as Carlson   
*Clifton Collins Jr. as Ross 
*Bobby Cannavale as Jake Fernandez 
*Patti LuPone as Ascension  Carlos Carrasco as Norte 
*Micah A. Hauptman as August Hardwicke    Emma Booth as Claire 
*Nick Nolte as Hurley  
*Daniel Bernhardt as Kroll   
*Dax Riggs as Himself

== Production ==

===Conception ===
  stated that Parker, although an anti-hero, has a likable quality.]] Parker had The Hunter, Point Blank (1967) and Payback (1999 film)|Payback (1999), among others.  Despite these films, Westlake always refused to let any of them use the characters name, saying hed only allow that if theyd agree to adapt all the novels. In 2008, following Westlakes death, his wife Abby, having been contacted by Les Alexander, a television producer who was a longtime acquaintance of Westlakes, agreed to sell the rights to one Parker novel (including the right to use Parkers name), with the option of several more being adapted later if the first film was successful.  Alexander hired a friend of his named John McLaughlin to write the screenplay for Parker, and then director Taylor Hackford became involved.  When the film opened, Taylor Hackford said in an interview that he didnt think Westlake would have agreed to let Parkers name be used under these circumstances. 

Hackford directed the film, and Steven Chasman, Hackford, Alexander,   about what led him to Parker, Hackford stated "I’m a fan of Donald Westlake. I really think he’s a fabulous writer … very unique in the area of crime because his Parker series". Hackford was attracted to Parker because he was a "strange character" and "Psychopathy#Sociopathy|sociopath" who, at the same time, isnt a sociopath, describing him as "compelling".   

=== Pre-production and casting ===
On April 18, 2011, Justin Kroll of  .  

  was cast as the films female lead character named Leslie, an insider who helps Parker during a heist.]]
 Emma Booth also co-starred in Parker.     In the novel Flashfire, Leslie wasnt of Cuban descent. However, Hackford cast Lopez in the role and decided to re-write her as Cuban, hiring Italian-American LuPone to play her "domineering" mother. 

=== Filming ===
According to executive producer Nick Meyer, Parker was produced on a "mid-30s" budget range, which he described as "pretty good", "given the caliber of the movie".  The Times-Picayune s Mike Scott reported on June 23, 2011 that Parker would film in New Orleans for seven weeks starting July 18. Scott noted that filming in New Orleans was "good news" for the local film industry because it came "at a time that has historically seen a slowdown in major productions, due both to the oppressive heat and the arrival of hurricane season."    Playbill later confirmed that production for the film had begun on August 4, 2011, in New Orleans.  Filming briefly moved to Baton Rouge, Louisiana from August 5–9. 
 Red Epic digital cameras anamorphic lenses. 

Statham, who is a former diver for the British Olympic Team, performed all of Parkers stunts in the film. In one scene, Statham jumped out of the window of a fast-moving car for his character to escape being shot; this stunt was considered "really dangerous" and Hackford said he was "nervous when he went out that window" five or six times before the scene was finished.  In another scene, Statham had to hang off a buildings balcony. The actor said he took a "real beating" from these scenes. He credited this to wearing a wire, which got in the way of filming and made things feel "restricted", because they ripped up his arms.  In January 2012, filming for Parker concluded in Miami and also in Columbus,Ohio.  

== Marketing ==
Originally, Parker was set to be released on October 12, 2012.  However, the release date was moved back due to strong competition it would have faced at the box office from other films released around that time including Gangster Squad and Here Comes the Boom; the former of which later had its release date moved to January due to the 2012 Aurora shooting.  Matt Goldberg of the website Collider noted that it would have probably lost to these films if it had been released that October.  Boxoffice (magazine)|Boxoffice listed the pros of the film release, which were Stathams "consistent" performance at the box office and Lopezs appearance which "could help the film expand a bit beyond Stathams usual audience".  It also listed the cons, which are Lopezs presence that might "turn off some of Stathams usual audience" as well as heavy competition from multiple other films.   

The films first promotional poster was unveiled on October 1, 2012.  Its theatrical trailer was released on October 4, 2012.    Collider commented that despite this being a slightly different film than what Statham is known for, the trailer "still has its share of clichés".  Joblo.com|Joblos Paul Shirley said "Its got a lot of the usual Statham action goodies", but with "source material and stellar cast" it has potential to be a theatrical hit.  Simon Reynolds of Digital Spy noted the pairing of "tough guy" Statham and "global superstar" Lopez to be "unlikely" but said Parker promises to "serve up some meaty action thrills".  On January 3, 2013, Digital Spy unveiled another promotional poster for Parker. 

== Box office == box office bomb.     By the end of its 70 day North American release, Parker had grossed $17.6 million at the box office, placing it at the low end of Stathams wide release crime/action vehicles.  It finished in 118th place on the 2013 domestic release box office chart.  The film was released on Blu-ray and DVD in the United States on May 21, 2013.   It was not one of the Top 100 selling DVDs of 2013, grossing a total of $11,274,235 on DVD and Blu-ray.   

== Critical response == film critics. Based on 100 reviews compiled by Rotten Tomatoes, the film has a "Rotten" rating from critics, with 40% positive reviews. "A thoroughly generic and convoluted heist movie" was the sites summation of the critical consensus.    Metacritic gave the film a score of 46 out of 100 based on 21 reviews from mainstream critics, which signifies "mixed or average" reviews.    The studio required critics attending a press screening sign an agreement that none of their reviews would appear in print before the film opened. 

John Semley of Slant Magazine was not receptive to Parker, panning its "painfully slapdash script",    although the Miami Herald s Connie Ogle felt that while it was a "stretch" with "absurdities", Statham "turns out to be a good choice to play the taciturn thief."  Christy Lemire, film critic for the Associated Press, felt that Statham is "not exactly pushing himself outside his comfort zone", and Lopez is "here to provide some comic relief as the wide-eyed fish out of water."    Alonso Duralde of The Wrap called the film a "bore" considering Stathams potential as an action star,  and Joe Morgenstern of The Wall Street Journal was also negative, saying that the film set a "tin standard" for crime thrillers. 
 Emma Booth). Nonetheless, Berardinelli gave Parker a generally positive review, summarizing its action scenes as "crisply directed, brutal, and invigorating." 

Other critics panned Parker and its action sequences to be predictable and generic. Writing for The A.V. Club, Josh Modell said the films beginning was "fairly strong", although the action "gets more predictable as it meanders toward its conclusion."  Peter Howell of the Toronto Star said the film started off "promisingly" but ended "predictable",  while the Montreal Gazette s Bill Brownstein panned the film by stating, "Much gunplay and bloodletting ensues. The body count is high. Intrigue is low."  Lisa Schwarzbaum of Entertainment Weekly s review was along the same lines, calling it "unremarkably generic" and "insanely bloody." 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 