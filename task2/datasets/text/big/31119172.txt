Makkal Mahatmyam
{{Infobox film
| name = Makkal Mahatmyam
| image = Makkal Mahatmyam.gif
| caption = CD Cover
| director = Paulson
| writer = Siddique-Lal Dialogues: Robinnath Mukesh Saikumar Saikumar Innocent Innocent Jagadish Vaishnavi
| producer = Shamli International
| music = S. Balakrishnan (composer)|S. Balakrishnan
| cinematography =
| studio = Shamli International
| distributor =
| released =  
| runtime =
| country = India
| language = Malayalam
| budget =
}}

Makkal Mahatmyam is a 1992 Malayalam comedy film directed by Paulson,  written by Siddique-Lal, and starring Mukesh (actor)|Mukesh, Saikumar (Malayalam actor)|Saikumar, Innocent (actor)|Innocent, Jagadish, Vaishnavi, K. P. A. C. Lalitha, Zeenath, Sukumari and Suchitra.      

==Plot==
The film is about the fights between two stepbrothers, Madhavankutty and Krishnankutty. The film takes a turn when Madhavankutty is accused in the murder of Pisharady Mash, an alcoholic.

==Cast== Mukesh as Madhavankutty Saikumar as Krishnankutty Innocent as Kuruppu Maash
* Jagadish as Manikantan, Madhavankuttys friend Vaishnavi as Radhika
* K. P. A. C. Lalitha as Kunjulakshmi Teacher, Kuruppu Maashs first wife and Madhavankuttys mother
* Zeenath as Saraswathi Amma, Kuruppu Maashs second wife and Krishnankuttys mother
* Sukumari as Subhadramma Suchitra as Ammu, Krishnankuttys sister
* Mamukkoya as Andru, peon Krishnan Kutty Nair as Pisharady Mash

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 