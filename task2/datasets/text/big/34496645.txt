The Bachelor's Club
{{Infobox film
| name           = The Bachelors Club
| image          =
| caption        =
| director       = A.V. Bramble
| producer       = 
| writer         = Israel Zangwill (novel)   Eliot Stannard
| starring       = Ben Field Ernest Thesiger Mary Brough   Sydney Fairbrother
| music          = 
| cinematography = 
| editing        = 
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       = 1921 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British silent silent comedy film directed by A.V. Bramble and starring Ben Field, Ernest Thesiger and Mary Brough.  It was based on the 1891 novel The Bachelors Club by Israel Zangwill.

==Cast==
* Ben Field - Peter Parker
* Ernest Thesiger - Israfel Mondego
* Mary Brough - Mrs. Parker
* Sydney Fairbrother - Tabitha
* Arthur Pusey - Paul Dickray
* Margot Drake - Jenny Halby James Lindsay - Eliot Dickray
* Sidney Paxton - Caleb Twinkletop
* A.G. Poulton - Edward Halby
* Arthur Cleave - Warlock Combs
* Dora Lennox - Israfels Sweetheart
* Jack Denton - Mandeville Brown
* Alice De Winton - Dowager

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 
 