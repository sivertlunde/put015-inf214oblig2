Pan Twardowski (1921 film)
{{Infobox film
| name           = Pan Twardowski 
| image          =
| image_size     = 
| caption        = 
| director       = Wiktor Biegański
| producer       = Józef Szwajcer
| writer         = Wiktor Biegański   Mieczysław Szerer   Adam Zagórski
| narrator       = 
| starring       = Bronisław Oranowski    Wanda Jarszewska   Antoni Nowara-Piekarski   Maria Krzyżanowska
| music          = 
| editing        = 
| cinematography = Stanisław Sebel
| studio         = Polfilma 
| distributor    = 
| released       = February 1921
| runtime        = 
| country        = Poland
| language       = Silent   Polish intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Polish silent silent fantasy Polish government to make the film in an effort to foster a greater sense of Polish national identity - particularly in the ethnically mixed Upper Silesia.  It is one of many films based on the legend of Pan Twardowski.

==Cast==
* Bronisław Oranowski as Twardowski 
* Wanda Jarszewska as Mrs. Twardowska 
* Antoni Nowara-Piekarski as Iwan IV Groźny 
* Maria Krzyżanowska as Królowa nimf 
* Mila Kamińska as Ulubienica cara 
* Antoni Siemaszko as Stary bojar 
* Władysław Grabowski as Młody Bojar 
* Stanisław Bryliński as Diabeł 
* Paweł Dydek-Dudziński as Pokurcz 
* Władysław Lenczewski   
* Stanisława Umińska  
* Bruno Winawer   
* Zofia Żukowska

==References==
 

==Bibliography==
*Skaff, Sheila. The Law of the Looking Glass: Cinema in Poland, 1896–1939. Ohio University Press, 2008.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 