The Heart of the Bear
{{Infobox film
| name           = The Heart of the Bear  ( ) 
| image          = 
| caption        = 
| director       = Arvo Iho
| producer       = Kristian Taska
| writer         = Nikolai Baturin
| starring       = 
| music          = Peeter Vähi
| cinematography = Rein Kotov
| editing        = Sirje Haagel, Arvo Iho
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Estonia
| language       = Estonian
| budget         = 
| gross          = 
}}
The Heart of the Bear ( ) is a 2001 Estonian drama film directed by Arvo Iho. It was Estonias submission to the 74th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.         It was also entered into the 24th Moscow International Film Festival.   

==Cast==
* Rain Simmul as Nika / Nganasan
* Dinara Drukarova as Gitya
* Ilyana Pavlova as Emily
* Külli Teetamm as Laima
* Lembit Ulfsak as Simon
* Nail Chaikhoutdinov as Tolkun
* Arvo Kukumägi as Venjamin
* Galina Bokashevskaya as Katherine

==References==
 

== External links ==
*  

 
 
 
 
 
 
 


 
 