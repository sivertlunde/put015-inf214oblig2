Private Lessons (2008 film)
{{Infobox film
| name           = Private Lessons
| image          = 
| caption        = 
| director       = Joachim Lafosse
| producer       = Jacques-Henri Bronckart
| writer         = Joachim Lafosse François Pirot
| based on       = 
| starring       = 
| music          = 
| cinematography = Hichame Alaouie
| editing        = Sophie Vercruysse
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Belgium France
| language       = French
| budget         = 
| gross          = 
}}
Private Lessons ( ) is a 2008 Belgian drama film directed by Joachim Lafosse. It was written by Lafosse and François Pirot. It was screened in the Directors Fortnight section at the 2008 Cannes Film Festival on May 19.  It was nominated for seven Magritte Awards and was awarded two: Best Actor for Jonathan Zaccaï and Most Promising Actress for Pauline Étienne. 

==Cast==
* Jonas Bloquet as Jonas
* Jonathan Zaccaï as Pierre
* Yannick Renier as Didier
* Claire Bodson as Nathalie
* Pauline Étienne as Delphine
* Anne Coesens as Pascale
* Johan Leysen as Serge

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 
 