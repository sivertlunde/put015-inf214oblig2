Effie Gray (film)
{{Infobox film
| name           = Effie Gray
| image          = Effie poster small.jpg
| alt            = 
| caption        = Film poster, featuring Fanning with Millais painting Ophelia (painting)|Ophelia behind her.
| director       = Richard Laxton
| producer       = Andreas Roald Donald Rosenfeld
| writer         = Emma Thompson
| starring       = Dakota Fanning Emma Thompson Greg Wise Tom Sturridge
| music          = Paul Cantelon  Andrew Dunn
| editing        = Kate Williams
| studio         = Sovereign Films
| distributor    = Metrodome Distribution (UK)   Adopt Films (US)
| released       =  
| runtime        = 108 minutes  
| country        = United Kingdom
| language       = English
| budget         = $11 million 
| gross          = $368,609 
}} biographical drama film directed by Richard Laxton.
 Euphemia "Effie" Gray (Dakota Fanning) and Pre-Raphaelite artist John Everett Millais (Tom Sturridge). Emma Thompson also appears in the film as Elizabeth Eastlake.

The films initial release was delayed by lawsuits alleging that the script, written by Emma Thompson, was plagiarised from earlier dramatisations of the same story. The cases were won by Thompson. 

==Plot==
In a pre-credit sequence Effie Gray is seen walking through a garden speaking about a fairy story in which a girl married a man with wicked parents. After the credits, the marriage of Effie to John Ruskin in Perth, Scotland is seen. The couple travel to London to stay with his parents. Effie soon begins to feel isolated, especially as she is repeatedly belittled by Johns mother. Her distress is compounded by the fact that her husband shows no interest in consummating the marriage and refuses to discuss the subject.

At the Royal Academy of Arts, John and Effie attend a dinner at which there is heated debate about the new Pre-Raphaelite movement in art, which John supports. John convinces Sir Charles Eastlake, the president of the academy, to allow the young artists to exhibit their pictures. Effie attracts the attention of Sir Charles wife, Elizabeth. When the Eastlakes visit the Ruskins, Elizabeth sees how distressed Effie is in the repressive atmosphere of the Ruskin family.
 The Stones of Venice. But when they get there, John busies himself studying the many historic monuments of the city, leaving Effie in the company of Rafael, a young Italian. Effie enjoys the city life, but is distressed when Rafael tries to seduce her. Her husband seems oblivious to the situation.
 his portrait. In Scotland, Everett befriends Effie, and becomes increasingly disturbed by Johns dismissive attitude to his wife. He is deeply embarrassed when John leaves the two of them alone together for several nights when he visits Edinburgh. Effie and Everett fall in love. When John returns he says Effie must come back with him to London. Everett convinces her to take someone she trusts with her and to explore the options for divorce. Effie brings her sister Sophy, claiming that Sophy wants to see the capital.

In London, Effie visits Elizabeth Eastlake. She tells her she is still a virgin and that John has told her he was disgusted by her body on their wedding night. Elizabeth advises her to seek legal advice. Effie is examined by a doctor, who confirms her virginity. Her lawyer tells her the marriage can be annulled. Effie leaves for Scotland, supposedly to accompany her sister, but really to leave John forever. Before she leaves London, she visits Everett, but only communicates with him via her sister. Everett says he will wait for her. Ruskins family is horrified when Effies lawyer calls round with a notification of annulment proceedings on the grounds of Johns impotence. As she travels back north, Effie smiles.

==Cast== Euphemia "Effie" Gray Lady Eastlake
* Greg Wise as John Ruskin
* Tom Sturridge as John Everett Millais
* Robbie Coltrane as Doctor
* Julie Walters as Margaret Cox Ruskin
* Claudia Cardinale as Viscountess
* David Suchet as John James Ruskin
* Derek Jacobi as Travers Twiss Sir Charles Eastlake
* Russell Tovey as George
* Riccardo Scamarcio as Rafael Sophy Gray

==Lawsuits== The Countess which had been positively received,  and ran Off-Broadway for 634 performances during the 1999/00 season.  Murphy also penned an unproduced screenplay on the same topic.  The matter was decided in Emma Thompsons favour in March 2013.   Murphy appealed the ruling,   but the Second Circuit rejected Murphys appeal. 

The release date was put back to October 2013, but the film was withdrawn from the Mill Valley Film Festival in California at which it was to be premiered under the title Effie Gray.  The film was finally released in October 2014. 

==Reception==
Effie Gray has received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 41%, based on 70 reviews, with an average rating of 5.6/10. The sites critical consensus reads, "Effie Gray benefits from its strong cast, elevating a period drama that doesnt strike quite as many narrative sparks as it could."  On Metacritic, the film has a score of 54 out of 100, based on 24 critics, indicating "mixed or average reviews". 

Mark Kermode said that the film "intelligently dramatises the prison-like nature of Effie’s status while struggling to engage us in what is essentially a non-relationship...we have a handsome but rather inert portrait of a suffocating social milieu in which it is left to Thompson herself to inject vibrant relief as the independently minded Lady Eastlake."  Tim Robey in The Telegraph said that "There are clever and sensitive touches right through, and a moving ending. But Fanning seems wholly uncomfortable, and not always intentionally. Shes meant to be playing a trapped Pre-Raphaelite muse, frequently ill and/or sedated, but moons her way through the film seeming mostly dazed and confused."  

David Sexton, in contrast, praised Fannings performance as "remarkably good", but objected to the caricatured portrait of Ruskin and what he called the "Everyday Feminism" of the portrayal of Effie as a victim.   Stephen Dalton in The Hollywood Reporter was unflattering, calling the film "an exquisitely dreary slice of middlebrow armchair theater which adds little new to a much-filmed story. Despite a lurid plot involving sex scandal, family dysfunction and proto-feminist revolt, the end result is depressingly conventional and deadeningly tasteful...yet another surface-level rehash of Victorian costume-drama clichés." However, Fannings "wounded, emotionally conflicted performance" was praised. 

==References==
{{Reflist|refs=
  , No. 11-CIV-7087 (JPO) (S.D.N.Y. Dec. 18, 2012) (248 KB PDF, accessed 17 January 2013) 
   
   
   
}}

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 