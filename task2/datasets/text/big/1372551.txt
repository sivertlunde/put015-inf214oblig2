London to Brighton
 
 
{{Infobox film
| name           = London to Brighton
| image          = Londontobrightonposter.jpg
| image_size     =
| caption        =
| director       = Paul Andrew Williams
| producer       = Wellington Films
| writer         = Paul Andrew Williams Johnny Harris Sam Spruell Chloe Bale Alexander Morton
| music          = Laura Rossi
| cinematography =
| editing        =
| distributor    =
| released       =
| runtime        = 85 mins
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
London to Brighton is a 2006 award-winning British film.   IMDb.com  The film was written and directed by Paul Andrew Williams.

==Plot==
The film opens with a woman and child, Kelly and Joanne, bursting into a London toilet. Joanne is crying and Kelly has a black eye. Eventually Kelly gets them on a train to Brighton, and it is clear they are running from someone.
 runaway who taste for underage girls. Kellys pimp, Derek, bullies her into complying, but it all goes horribly wrong, and the old mobster is killed, presumably by one of the girls. The older mans son, Stuart, then forces Derek to find the girls. The film follows the duos flight from London in the wake of what has happened.

Arriving initially in Brighton, Kelly visits her friend Karen and tries to earn enough money through prostituting herself to help Joanne afford the train to Devon, where the childs grandmother lives. The two are eventually tracked down by her pimp and his associate and taken to meet Stuart at a secluded field. Upon arrival, Kellys pimp and associate are made to dig two graves, presumably for the girls. However, Stuart decides that the girls are the victims in this episode and decides instead to kill Kellys pimp and associate. The film ends with Kelly and Joanne arriving at Joannes grandmas house in Devon. Kelly watches from a distance as the girl and the grandmother hug, then turns away.

==Cultural references==
* The film is seen as belonging to the crime-thriller genre, but it also plays on the idea of social realism with the themes of child prostitution and the runaway youth.

==Main cast==
* Lorraine Stanley as Kelly
* Georgia Groome as Joanne
* Sam Spruell as Stuart Allen
* Alexander Morton as Duncan Allen Johnny Harris as Derek
* Chloe Bale as Karen
* Nathan Constance as Chum

==Critical reception==
The film received mostly very positive reviews from critics. The British magazine produced for homeless people, The Big Issue, called it "The best British film of the century,"  while The Guardian dubbed it "the best British film of the year." 

In July 2008, the review aggregator Rotten Tomatoes reported that 69% of critics gave the film positive reviews, based on 22 reviews.  Metacritic reported the film had an average score of 55 out of 100, based on 7 reviews. 

==Awards and nominations==
The film won a British Independent Film Award for Best Achievement in Production. The director also won several awards at various film festivals. Paul Andrew Williams won the Golden Hitchcock award at the Dinard Festival of British Cinema, the New Directors Award at the Edinburgh International Film Festival, Best Feature Film at the Foyle Film Festival, and a Jury Prize at the Raindance Film Festival. 

==References==
 

==External links==
* 
* 
* 
* 
* 
*  guardian.co.uk, 30 April 2007
*  Times Online, 30 November 2006
*  BBC - Movies, 24 November 2006
*  Timeout London, 29 November 2006
*  YouTube
*Vidiyum Munn Vidiyum Munn - Tamil Movie - Inspired by London to Brighton

==Publications==
* Stella Hockenhull, "An Aesthetic Approach to Contemporary British Social Realism: London to Brighton", Film and Romantic special issue, Jeffrey Crouse (ed.), Film International, Vol. 7, No. 6, December 2009

 

 
 
 
 
 
 
 
 