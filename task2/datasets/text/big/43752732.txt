An Honest Liar
{{Infobox film
| name           = An Honest Liar
| image          = AnHonestLiarPoster.png
| alt            = 
| caption        = Theatrical release poster
| director       =  Justin Weinstein Tyler Measom
| writer         = Justin Weinstein Tyler Measom Greg OToole
| producer       = Tyler Measom Justin Weinstein
| starring       = James Randi Alice Cooper Bill Nye Adam Savage Penn & Teller Michael Shermer
| music          = Joel Goodman
| cinematography = Tyler Measom Justin Weinstein
| editing        = Greg OToole
| studio         = Left Turn Films Pure Mutt Productions Part2 Filmworks
| distributor    = Abramorama
| released       =    
| runtime        = 93 minutes  . An Honest Liar. Retrieved September 5, 2014. 
| country        = United States
| language       = English
| budget         = Undisclosed 
| gross          = 
}}
An Honest Liar is a 2014 biographical  . Retrieved September 5, 2014.   . Rotten Tomatoes. Retrieved September 5, 2014. 
 Hot Docs, AFI Docs Festival, where it won the Audience Award for Best Feature. It is scheduled for wide release in February 2015.

==Cast== stage magician and escape artist, turned scientific skeptic investigator       known for his public exposés of faith healers, psychics and other promoters of pseudoscientific and paranormal claims.    The film focuses on his life as an investigator, and his relationship with his long-time boyfriend, José Alvarez.
* .  Science educator and television host, best known as the host of the Disney/PBS childrens science show Bill Nye the Science Guy.
*Adam Savage - Industrial design and special effects designer/fabricator, known as one of the co-hosts of the Discovery Channel television series MythBusters and Unchained Reaction.    illusionists and entertainers
*Michael Shermer - Science writer, historian of science, founder of The Skeptics Society, and Editor in Chief of its magazine Skeptic (U.S. magazine)|Skeptic. 
*José Alvarez - Performance artist who posed as a channeller known as "Carlos" on Australian television, in a hoax arranged by Randi. He is also Randis live-in boyfriend of 25 years, whose real name is revealed in the course of filming to be Deyvi Peña. 
*Richard Wiseman - Professor of the Public Understanding of Psychology at the University of Hertfordshire in the United Kingdom.    magician who works primarily with Playing card|cards.
*Ray Hyman - Professor Emeritus of Psychology at the University of Oregon in Eugene, Oregon,  and a noted critic of parapsychology.  Steve Shaw - Mentalist known by the stage name Banachek,  who posed as a psychic in a paranormal research project at Washington University in St. Louis.
*Michael Edwards - Actor who posed as a psychic in a paranormal research project at Washington University in St. Louis. 
*Uri Geller - Israeli illusionist, television personality, and self-proclaimed psychic, known for his trademark television performances of spoon bending and other supposed psychic effects. Geller famously failed to perform his feats under controlled conditions during a 1973 appearance The Tonight Show with Johnny Carson, after Randi supervised the staff of that program on the proper handling of the materials used in the performance to prevent cheating.

== Synopsis ==
An Honest Liar documents James Randis early life as a carnival-bound refugee from Toronto who early on, dedicated himself to learning every trick performed by Harry Houdini, and even improving on some of them. In one of his feats as an escape artist, Randi frees himself from a straitjacket while being hung upside down by his ankles over Niagara Falls.

Age and concerns over the danger of his profession and his health lead him to retire from that occupation and seek out not only a new career, but a crusading obsession that makes him a pop cultural fixture by the 1970s: As a scientific skeptic investigator and challenger to pseudoscientific and paranormal claims, which leads him to expose the deceit behind religious faith healers, psychics, and other con artists who publicly exploit the public. Randi becomes a recurring guest on The Tonight Show with Johnny Carson, and makes appearances on TV shows such as Happy Days and in rock music artist Alice Coopers 1973 Billion Dollar Babies tour, where Randi decapitates Cooper at the end of each performance.
 controls over Steve Shaw pose as mentalists in a Washington University in St. Louis study that confirmed Geller as an actual psychic.
 come out at age 81, and how Alvarez, at the time of filming, had recently been discovered to be living under a false identity, which leads to legal ramifications for the couple.

==Production==
In 2012 producers Tyler Measom and Justin Weinstein visited James Randi at his home in Plantation, Florida to express interest in filming a documentary about his life. To illustrate their bona fides to him, they gave him copies of their previous documentaries. Randi comments, "When I saw the product that hey had turned out, I thought to myself, These are the guys. These are the guys that I think I can trust with my life story." 

The film was funded in part via a campaign  . Retrieved September 5, 2014.  The film is produced through Left Turn Films, Pure Mutt Productions, and Part2 Filmworks by Tyler Measom and Justin Weinstein, who also directed, and written by Weinstein, Measom and Greg OToole. Toole also edited the film.   The films music is produced by  .  It is distributed by Abramorama. 

==Reception==
===Release=== Hot Docs film festival.  It was also screened at the June 2014 AFI Docs Festival in Silver Spring, Maryland and Washington, D.C., where it won the Audience Award for Best Feature.  Its wide releases was March 6, 2015. Catsoulis, Jeannette (March 5, 2015).  . The New York Times. 

===Reception===
The film holds a 93% score on the review aggregator website Rotten Tomatoes, based on 15 reviews.  Geoff Pevere, reviewing the film for The Globe and Mail, gave the film three out of four stars, calling it "aptly seductive", though he called into question whether the methods Randi used in the case of the Australian hoax were a form of dubious deceit themselves, stating, "The ultimate question in An Honest Liar is whether it’s possible to know so much about the method behind the magic without being fooled into believing your own act." 

David Rooney, reviewing the film for The Hollywood Reporter, thought the film "intriguing", but felt the transition from Randis investigations to the revelations about Alvarez were too abrupt, and the conclusions lacking full coherence, and summarized the film as "a compelling magic act that loses focus in the big finish when the cloak gets whisked away." 

The film was a Critics Pick of The New York Times, for which reviewer Jeannette Catsoulis called the film a "jaunty, jovial portrait with a surprising sting in its tail". Catsoulis also called the development of Randi and Alvarezs legal problems to be "moving". 

==Awards==
*Audience Award for Best Feature, 2014 AFI Docs Festival 

==References==
 

==External links==
* 
* 
* . Indiewire.
*Higginbotham, Adam (November 7, 2014).  . The New York Times.
* . The Huffington Post. April 18, 2014.

 
 
 
 
 
 
 
 
 
 
 