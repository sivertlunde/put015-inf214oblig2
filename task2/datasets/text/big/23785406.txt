One Nation Under God (2009 film)
{{Infobox film
| name           = One Nation Under God
| image          = ONUG poster.jpg
| director       = Will Bakke
| producer       = Michael B Allen
| writer         = Will Bakke
| starring       = Michael B Allen Will Bakke Lawson Hopkins Austin Meek
| music          = We Are The City Johnny Stimson Sleeperstar various other artists
| editing        = Will Bakke
| sound          = Steven Samuels
| distributor    = Riot Studios
| released       =  
| runtime        = 93 minutes 
| country        = United States
| language       = English
}}
One Nation Under God is a 2009 American   since it premiered on April 11, 2009,  and was released on DVD August 31, 2009. 

==Plot==
Austin, Lawson, Michael, and Will are four college-aged Christians who have grown up in the bubble of Christianity. They realize that their faith is more religion and less relationship. Because they have been in the rut of mindless faith, they decide to expand their views on God, the world, and eternity by traveling by car around the United States and Canada for the summer.
 Las Vegas. Portland and Glacier National Park, Yellowstone, Chicago, and Toronto. The last leg of the journey includes Boston, New York City, Philadelphia, Washington D.C., Atlanta, and New Orleans.

Along the way, they meet all kinds of people including beach bums, hippies, suburbanites, a clown, and a congresswoman, who each give them a unique perspective of eternal things. The trip is narrated with commentary by the four guys in a studio, and the footage cuts back and forth from studio to trip throughout the movie. As they come to the end of their journey, they conclude by stating that the most important thing they learned from the experience is to "never stop asking questions."

==Cast==
* Michael B Allen
* Will Bakke
* Lawson Hopkins
* Austin Meek

==Music==
The soundtrack for the film has not yet been released for sale, but the film contains music from the following artists. 

* We Are the City
* Sleeperstar
* Johnny Stimson
* Wheeler Sparks
* Austin Pitzer
* Jillian Edwards
* Whitney Whyte
* The Heart Is a Lonely Hunter
* Stars Shapes Make
* Two Bicycles
* Lunden McGill
* La Vérité
* Trey Duck
* The Review

==Reception==
Reception for this film has been somewhat positive. In an interview with Gary Cogill, award-winning film critic for WFAA, Mr. Cogill mentions that the film is "selling out wherever   go."  He also calls One Nation Under God "...an exhilarating, often funny, very enlightened new documentary...One Nation Under God has a wicked sense of humor. Its slightly irreverent, and its right on the mark."  The movie currently has a user rating of 6/10 at IMDb.  

==Distribution==
In April 2009, Michael B Allen, cast member and producer of the film, stated that those involved were "looking for a distributor."   However, the film is now planned to be distributed under a newly created production/distribution company called "Riot Pictures."  DVDs were released August 31, 2010, and are available for purchase online through The Riot Studios Official Site. The production studio was renamed and officially launched as Riot Studios in January 2011.

==References==
 

==External links==
*  
*  
*   in One Magazine

 
 
 
 
 
 