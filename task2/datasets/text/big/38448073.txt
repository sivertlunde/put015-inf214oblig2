A Small Town Idol
{{infobox_film
| name           = A Small Town Idol
| image          =A Small Town Idol (1921) - Ad 2.jpg
| image_size     =190px
| caption        =Ad for the film with Phyllis Haver, Dot Farley, and Ben Turpin
| director       = Mack Sennett Erle C. Kenton
| producer       = Mack Sennett
| writer         = 
| starring       =
| music          =
| cinematography = Ernie Crocket Perry Evans J. R. Lockwood
| editing        = Allen McNeil Associated First National
| released       = February 13, 1921
| runtime        = 70 minutes; 7 reels
| country        = United States
| language       = Silent (English intertitles)

}} Associated First Down on the Farm. Sennett and Erle C. Kenton directed.    This was Ramon Novarros seventh film, where he was billed as Ramon Samaniegos.

Copies of the film exist in archives such as Gosfilmofond.

==Plot==
Sam (Turpin) leaves town after being falsely accused of a crime and becomes a film star in Hollywood working with actress Marcelle Mansfield (Prevost). He returns to his home town hailed as a hero where one of his films is shown in the theater. A rival, who wants Sams girlfriend Mary (Haver), frames Sam for an apparent murder of Marys father. Just as the townspeople are about to lynch Sam, Mary arrives with word that her father is fine, and the two are reconciled.

==Cast==
*Ben Turpin - Sam Smith James Finlayson - J. Wellington Jones
*Phyllis Haver - Mary Brown
*Bert Roach - Martin Brown
*Al Cooke - Joe Barnum Charles Murray - Sheriff Sparks (*as Charlie Murray)
*Marie Prevost - Marcelle Mansfield
*Dot Farley - Mrs. Smith
*Eddie Gribbon - Bandit Chief
*Kalla Pasha - Bandie Chiefs Rival
*Billy Bevan - Director George OHara - Cameraman
*John J. Richardson - 
*Louise Fazenda -
*Lige Conley - (*as Lige Crommie)

 
uncredited performers
*Heinie Conklin - Jester in movie
*Ramon Novarro - Dancer (*as Ramon Samaniegos)
*Jane Allen - Bit
*Andy Clyde - Bit
*Harry Gribbon - Bit
*Floy Guinn - Bit
*Mack L. Hamilton - Bit
*Harriet Hammond - Bit
*Mildred June - Bit
*Fanny Kelly - Bit
*Marvin Loback - Bit
*Don Marion - Bit
*Kewpie Morgan - Bit
*Derelys Perdue - Dancer
*Gladys Whitfield - Bit

==References==
 

==External links==
 
* 
* 

 

 
 
 
 