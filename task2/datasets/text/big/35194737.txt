9 Nelalu
 
{{Infobox film
| name           = 9 Nelalu
| image          =
| caption        =
| director       =Kranthi Kumar
| producer       =
| writer         = Vikram Soundarya
| music          = V. S. Udhaya
| cinematography =
| editing        =
| distributor    =
| studio         =
| released       = 11 January 2001
| runtime        = 135 minutes
| country        = India
| language       = Telugu
| budget         =
}} Vikram in the lead roles. The film opened in 2001 to positive reviews from critics. The film was later dubbed and released in Tamil as Kanden Seethaiyai.  The film was premiered retrospective at the Toronto Film Festival in Canada. The director shot the film in just 15 days. 

==Plot==
The film revolves around Savitri (Soundarya), an innocent simple and loving orphan, and Surendra (Vikram (actor)|Vikram), an intelligent computer programmer. When Savitri has a marriage arranged with a drunkard, Surendra stops this alliance and graciously agreed to marry her.

However, trouble starts when Surendras brainchild, a virtual reality program on temples of South India is sabotaged, and he is accused of selling out to a rival company. Disillusioned by the sudden turn of events, he crashes into a truck and sustains a serious head injury that requires a major operation.

Savitri finds herself deserted by all her well-wishers when it comes to financial help, and her husbands life hangs in the balance. A lady doctor suggests that she become pregnant through artificial insemination for a rich man, whose wife is impotent, in exchange for monetary remuneration. Savitri agrees and the rest of the film is based on the social stigma attached to artificial insemination and surrogate motherhood.

==Cast== Vikram as Surendra
* Soundarya as Savitri

==Production==
A. R. Rahman was initially signed on to score the films music, but he later opted out owing to the films small budget.  The film was premiered retrospective at the Toronto Film Festival in Canada. The director shot the film in just 15 days. 

==Release==
The film opened to very positive reviews with a critic noting that "in short, this is cinema at its best" and that "the script is smooth flowing and engrossing" and that "it leaves you with a thought and a sense of pride in Indian cinema. Whether it will go down well with the masses is doubtful as there are no songs, action or cheap comedy, which might be a bitter pill to swallow for most". In regard to performances, the critic notes that Soundarya "excels in her role and that her expressions, body language and dialogue delivery fit her role like a glove" and that "Vikram has given a controlled performance as the troubled, confused, yet loving husband." 

==References==
 

==External links==
*  

 
 
 

 