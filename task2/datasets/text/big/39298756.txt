Desingu Raja
{{Infobox film
| name           = Desingu Raja
| image          = Desingu Raja Poster.jpg
| caption        = Promotional poster
| director       = Ezhil
| producer       = Madhan
| writer         = Ezhil N. Rajasekhar (dialogues)
| starring       = Vimal Bindu Madhavi
| music          = D. Imman
| cinematography = Sooraj Nalluswamy
| editing        = Gopi Krishna
| studio         = Escape Artists Motion Pictures
| distributor    = Olympia Movies
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          =   20 crore
}} Tamil film directed by Ezhil starring Vimal and Bindu Madhavi.  The film is a full-length comedy set in two villages which are perpetually at conflict with one another.    The film has been extensively shot in Thanjavur, Kumbakonam and Chennai.  The film was released on August 23, 2013 to mixed - positive reviews and success at the box office.  

==Plot==
Idhayakkani (Vimal), who hails from the village of Killioru, is a firm believer in non-violence. The ongoing feud between his family and the family of Seena Thana from the neighbouring village of Pulioru is a source of constant worry for him.

Though their rivalry began on a minor issue, many lives were sacrificed over the years.

Idhayakkani’s father is killed by Seena Thana. Angered by the death of his son, Idhayakkani’s grandfather retaliates by killing Seena Thana’s son.

So Seena Thana vows to kill Idhayakkani.

Meanwhile, Idhayakkani meets Thamarai (Bindu Madhavi), Seena Thana’s daughter, at a temple festival and falls deeply in love. Chaos ensues when both realise who their families are and the respective sets of parents and villagers object to their romance. Idhayakani marries Thamarai at a temple and Seenathana gets killed by henchmen of Idhayakanis grandfather. Thus Thamarai develops hatred towards Idhayakani for her fathers death.

According to Panchayat, Thamarai goes to Idhayakanis home with her family in order to kill Idhayakani but they fail in their attempts.

When Chittappa hears that Thamarai is pregnant he is determine to kill her child. But Surya (Soori), Thamarais uncle tries to give honey instead of poison to Thamarai which makes her safe.

In the climax, Killiooru organises Kabaddi match in Puliooru. Chittappa arrives at the place by kidnapping Thamarai and her mother insteads Kaushik, Idhayakanis uncle was kidnapped instead of Thamarai it turns out that Surya had saved her. Chitappa arrives to kill Idhayakani but the presence of Cheergirls makes him undergo a change of heart.He then dances to the tune of Gangnam Style and accepts Idhayakani wholeheartedly.

==Cast==
* Vimal as Idhayakani
* Bindu Madhavi as Thamarai Soori as Suriya
* Singampuli as Koushik
* Ravi Mariya as Chittappa
* Charle
* Aadukalam Naren as Idhayakanis father
* Vinu Chakravarthy as Idhayakanis grandfather
* Vanitha Krishnachandran as Thamarais mother
* Vadivukkarasi
* Susan George
* Singamuthu
* Chaams
* Gnanavel as Cheena Thaana Muktha Bhanu Special appearance in song "Nelavattam"
* Venniradai Moorthy Special appearance in song "Pom Pom" Lollu Sabha Swaminathan Special appearance in song "Pom Pom"

==Production== MGR fan. Muktha Bhanu danced for one song along with Vimal and the group dancers which was shot in Thiruvarur. 

Sify reported that the film was nearly 50% complete in December 2012.  In April 2013 a song sequence featuring Vimal and Bindu Madhavi was shot for almost ten days in a set worth 15 lakhs.  In May 2013 the crew shot a song sequence for 10 days with Vimal and Bindu Madhavi in a set resembling a fruit godown. 

==Soundtrack==
{{Infobox album
| Name        = Desingu Raja
| Longtype    = to Desingu Raja
| Cover       = Desingu Raja Audio Cover.jpg
| Caption     = Front Cover
| Type        = soundtrack Feature film soundtrack
| Artist      = D. Imman
| Producer    = D. Imman
| Label       = Sony Music India Tamil
| Last album  = Kumki (film)|Kumki   (2012)
| This album  = Desingu Raja   (2013)
| Next album  = Varuthapadatha Valibar Sangam   (2013)
}}

After Manam Kothi Paravai, D. Imman collaborates with Ezhil for second time and his first collaboration with Vimal. The soundtrack contains 5 songs which was written by Yugabharathi. Single track "Ammadi Ammadi" was released in May 25, 2013.  The audio was released in 19 June 2013 at Suryan FM Radio station.   Behindwoods wrote:"Folksy and well produced".  Yaarume Kekkave Illa was not included in the film.

Tracklist
{{track listing
| extra_column  = Singer(s)
| all_lyrics   = Yugabharathi

| title1      = Ammadi Ammadi
| extra1      = Shreya Ghoshal
| length1     =

| title2      = Oru Ora Ora Paarvai
| extra2      = Balram
| length2     =

| title3      = Nelaavattam Nethiyile
| extra3      = Harini, P. Unni Krishnan
| length3     =

| title4      = Yaarume Kekkave Illa
| extra4      = S. P. Balasubramaniam
| length4     =

| title5      = Pom Pom
| extra5      = Vijay Prakash
| length5     =

| title6      = Ammadi Ammadi (Karaoke Version)
| extra6      = Instrumental
| length6     =

| title7      = Oru Ora Ora Paarvai (Karaoke Version)
| extra7      = Instrumental
| length7     =

| title8      = Nelaavattam Nethiyile (Karaoke Version)
| extra8      = Instrumental
| length8     =

| title9      = Yaarume Kekkave Illa (Karaoke Version)
| extra9      = Instrumental
| length9     =

| title10     = Pom Pom (Karaoke Version)
| extra10     = Instrumental
| length10    = 
}}

==Release== Zee Thamizh. The film was given a "U" certificate by the Indian Censor Board. The film was supposed to release on July 15 and got postponed to August 9 but due to release of Thalaivaa, it got postponed and finally released on August 23.   The film released in 300+ screens across TN. 

==Box Office==
The film grossed   8+ crore at the box office.

==Critical reception==
Rediff wrote: Desingu Raja received generally negative reviews from critics"The unconvincing storyline may not hold your attention, also some unfunny comic elements in the film will not certainly keep you entertained throughout".  Behindwoods wrote:"On the whole, Desingu Raja doesn’t have much going for it as this blend of comedy, romance and family drama is one that has been done to death many times".  Deccan herald wrote:"Its an unambitious film on a mission to entertain its viewers with full-length comedy. Honestly, it doesnt succeed in its mission because the forced humour does not even works for a few minutes, but after a while it really tests your patience".  Times of India wrote:"You will not like this film if you are looking for a rural comedy that feels fresh and funny".  Hindu wrote:"Desingu Raja is about the union of a couple from feuding villages, but it’s really a cautionary tale about making movies with no jokes and no script". 

==References==
 

==External links==
*  

 

 
 
 
 