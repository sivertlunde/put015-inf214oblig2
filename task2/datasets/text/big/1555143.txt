Dolphins (2000 film)
{{Infobox film
| name           = Dolphins
| image          =
| caption        =
| director       = Greg MacGillivray
| producer       =
| writer         = Tim Cahill Stephen Judson
| narrator       = Pierce Brosnan
| starring       = Sting
| cinematography = Greg MacGillivray Brad Ohlund
| editing        = Stephen Judson Chris Palmer IMAX Corporation
| released       =  
| runtime        = 39 min.
| country        = United States English
| budget         =
}}
 Chris Palmer serving as executive producer, this feature follows a few scientists studying dolphins (chiefly Kathleen Dudzinski, Dean Bernal, and Alejandro Acevedo-Gutiérrez, as well as Louis Herman and Bernd Wursig) as they work to learn more about dolphins.  The main focus is on research into dolphin communication and dolphin intelligence|intelligence, along with some exploration of feeding habits and human interaction. Several species of dolphins are shown, primarily the bottlenose dolphin, the dusky dolphin, and the Atlantic spotted dolphin. Dolphins is narrated by Pierce Brosnan with music by Sting (musician)|Sting.

==Track listing==
{{Tracklist
| collapsed       = no
| extra_column    = Performer
| writing_credits = yes
| title1          = I Need You Like This Hole in My Head Sting
| extra1          = Sting
| length1         = 2:51
| title2          = Sea of Light
| writer2         = Sting / Steve Wood
| extra2          = Steve Wood
| length2         = 4:21
| title3          = Fill Her Up
| writer3         = Sting
| extra3          = Sting
| length3         = 5:36
| title4          = When Dolphins Dance
| writer4         = Sting
| extra4          = Steve Wood
| length4         = 5:21
| title5          = Ghost Story
| writer5         = Sting
| extra5          = Sting
| length5         = 5:28
| title6          = First Dive
| writer6         = Sting / Steve Wood
| extra6          = Steve Wood
| length6         = 6:18
| title7          = Bubble Rings
| writer7         = Sting / Steve Wood
| extra7          = Steve Wood
| length7         = 5:17
| title8          = When We Dance
| writer8         = Sting
| extra8          = Sting
| length8         = 5:47
| title9          = On the Island
| writer9         = Steve Wood
| extra9          = Steve Wood
| length9         = 3:16
| title10         = Dolphins of the World
| writer10        = Steve Wood
| extra10         = Steve Wood
| length10        = 5:12
| title11         = Rendezvous
| writer11        = Sting
| extra11         = Steve Wood
| length11        = 5:06
}}

==References==
 

==Further reading== Chris Palmer: Shooting in the Wild: An Insiders Account of Making Movies in the Animal Kingdom, Sierra Club Books, 2010

==External links==
*   
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 


 