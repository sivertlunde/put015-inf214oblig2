A Busy Day
{{Infobox film
| name           = A Busy Day
| image          = A Busy Day.jpg
|image_size=180px
| caption        = film scene
| director       = Mack Sennett
| producer       = Mack Sennett
| writer         = 
| starring       = Charlie Chaplin
| music          =  Frank D. Williams
| editing        = 
| distributor    = Keystone Studios Mutual Film
| released       =  
| runtime        = 6 minutes
| country        = United States
| language       = English
| budget         = 
}}
 
A Busy Day is a 1914 short film starring Charlie Chaplin and Mack Swain.

==Plot==
In A Busy Day, a wife (played by an energetic Charlie Chaplin) becomes jealous of her husbands interest in another woman during a military parade. On her way to attack the couple, the wife interrupts the set of a film, knocking over a film director and a police officer.  Finally, the husband pushes the wife off of a pier and she falls into the harbor.

The film contains a judicious amount of kicking and slapping.

==Cast==
* Charles Chaplin as Wife
* Mack Swain as Husband
* Phyllis Allen as The Other Woman
* Mack Sennett as Film director Billy Gilbert as Police officer

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 