Back Street (1941 film)
{{Infobox film
| name           = Back Street
| image          = BackStreet1941.jpg
| caption        = Theatrical release poster Robert Stevenson
| producer       = Bruce Manning Frank Shaw  (associate producer)  
| writer         = Bruce Manning Felix Jackson Fannie Hurst (novel)
| starring       = Charles Boyer   Margaret Sullavan Frank Skinner 
| cinematography = William H. Daniels
| editing        = Ted J. Kent
| distributor    = Universal Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Robert Stevenson. 1932 film novel and the 1932 film version very closely, in some cases reproducing the earlier film scene-for-scene. It is a sympathetic tale of an adulterous woman and the man she loved.
 Best Music Frank Skinner).

Margaret Sullavan so much wanted Charles Boyer to play her leading man that she gave up her top billing in order to persuade him to play this unsympathetic role.

== Plot summary ==
The film is set in the early 1900s. It tells the story of a pretty and independent young woman, Ray Smith, who lives in Cincinnati. She has many suitors, none of whom she takes seriously. One day she meets an extremely charming and handsome banker named Walter Louis Saxel, and they fall immediately into a strong attraction, which for her is real love. After a few days of closeness she is shocked when he tells her he is already engaged to someone else. Nonetheless the two of them very nearly marry one another on an impulse, but they are prevented from doing so by arbitrary external forces. 

After five years, they meet once again, by chance, in New York City. The banker is now married with two children (Richard and Elizabeth) and is extremely successful in his career, but Ray and he still share the same strong attraction. Ray loves him so much that she gives up her career in dress design and becomes his kept mistress, seeing him only when it is convenient for him. Walter keeps up the appearance of a "happy marriage" and never considers divorcing his wife, whose father is his boss at the banking company. 

Rays loyalty to Walter collapses only once, when he fails to contact her after he has been on an extended trip to Europe with his wife. Ray goes back to Ohio and agrees to marry Curt, an attractive and good-hearted man who proposed to her many times in their youth. However, Walter travels to Ohio to find her, and is able to persuade her to return with him.

Once Walters children reach adulthood they understand who Ray is, and they despise her. People in Walters social circle also point condemning fingers at Ray, who suffers all this with patience and fortitude.

In old age, dying of a stroke in his grand home, Walters last faltering word is to Ray, on the phone. She dies not long afterwards in her apartment.

== Cast ==
* Charles Boyer as  Walter Louis Saxel
* Margaret Sullavan as  Ray Smith Richard Carlson as  Curt Stanton
* Frank McHugh as  Ed Porter
* Tim Holt as  Richard Saxel
* Frank Jenks as  Harry Niles
* Esther Dale as  Mrs. Smith
* Samuel S. Hinds as  Felix Darren Peggy Stewart as  Freda Smith
* Nell ODay as  Elizabeth Saxel
* Kitty ONeil as  Mrs. Dilling
* Nella Walker as  Corinne Saxel
* Cecil Cunningham as  Mrs. Miller
* Marjorie Gateson as  Mrs. Adams
* Dale Winter as  Miss Evans

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 