I Killed the Count
 
{{Infobox play
| name       = I Killed the Count
| image      = 
| image_size = 
| caption    = 
| writer     = Alec Coppel
| characters = 
| setting    = London
| premiere   = 10 December 1937
| place      = Whitehall Theatre, London
| orig_lang  = English
| subject    = 
| genre      = 
}}

I Killed the Count is a 1937 play by Alec Coppel. Its success launched Coppels career. 

The play was produced on Broadway in 1942. 

==Film adaptation==
{{Infobox film
| name           = I Killed the Count
| image          = 
| caption        = 
| producer       = 
| director       =  
| writer         = 
| starring       = Ben Lyon
| music          = 
| cinematography = 
| editing        = 
| studio  = 
| distributor    =  
| released       = 1939
| runtime        = 
| country        = United Kingdom
| language       = English 
| budget         = 
}}
 directed by Frederic Zelnik and starring Ronald Shiner as Mullet, Ben Lyon, Syd Walker, Terence De Marney, Barbara Blair and Athole Stewart.
 produced by Grafton Films.

===Synopsis===
Cockney comedian Syd Walker plays it more or less straight as Scotland Yard Detective Inspector Davidson, at present trying to determine who murdered the much-hated Count Mattoni (Leslie Perrins). The dilemma isnt that the Detective is suffering from a lack of witnesses. In fact, four different people come forth to confess to the killing – each of them with plenty of motive and opportunity.

===Cast===
* Ben Lyon – Bernard Froy
* Syd Walker – Detective Inspector Davidson
* Terence De Marney – Detective Sergeant Raines
* Barbara Blair – Renée la Lune
* Athole Stewart – Lord Sorrington
* Antoinette Cellier – Louise Rogers
* Leslie Perrins – Count Mattoni David Burns – Diamond
* Ronald Shiner – Mullet
* Aubrey Mallalieu – Johnson
* Kathleen Harrison – Polly
* Gus McNaughton – Martin

===Release===
Originally released in the US by Grand National Films Inc. in 1939, I Killed the Count was reissued the following year as Who is Guilty? by Monogram Pictures in America. 

==Other versions==
A second adaptation I Killed the Count was made by the BBC in 1948. The play was also adapted as a three-parter on TVs Alfred Hitchcock Presents  and on Australia radio in 1941. 

==References==
 

==External links==
*  
*  
*  
*  
*   at AusStage

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 