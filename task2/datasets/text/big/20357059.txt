And Now the Screaming Starts!
{{Infobox film
| name           = And Now the Screaming Starts!
| image          = Screamstarts.jpg
| image_size     =
| caption        = United States cinematic release poster
| director       = Roy Ward Baker
| producer       = Max Rosenberg Milton Subotsky Gustave Berne
| writer         = Roger Marshall (screenplay) David Case
| narrator       = Patrick Magee Stephanie Beacham
| music          = Douglas Gamley
| cinematography = Denys Coop
| editing        = Peter Tanner Tony Curtis (addl) Amicus
| distributor    = Fox-Rank
| released       = April 27, 1973
| runtime        = 91 mins
| country        = British
| language       = English
| budget         = $500,000 Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 102-109 
| gross          =
| preceded_by    =
| followed_by    =
}}
 gothic horror anthology or "portmanteau" films.
 Patrick Magee, Stephanie Beacham and Ian Ogilvy, and was directed by Roy Ward Baker. The large gothic house used in the film is Oakley Court, near Bray village, which is now a four star hotel.

==Plot==
In 1795, two newlyweds move into the stately mansion of the husband, Charles Fengriffen (Ogilvy). The bride, Catherine Fengriffen (Beacham), falls victim to a curse placed by a wronged servant on the Fengriffen family and its descendants.

==Cast==

* Peter Cushing as Dr. Pope
* Herbert Lom as Sir Henry Fengriffin Patrick Magee as Dr. Whittle
* Stephanie Beacham as Catherine Fengriffin
* Ian Ogilvy as Charles Fengriffin
* Geoffrey Whitehead as Woodsman / Silas
* Guy Rolfe as Lawyer Maitland
* Rosalie Crutchley as Mrs Luke
* Gillian Lind as Aunt Edith
* Janet Key as Bridget

==Reception==
The film received a lukewarm reception in Britain and America on its release. In the UK, And Now the Screaming Starts! went out on a double bill with the American horror film, Dr Death, Seeker of Souls. Jonathan Rosenbaum of Monthly Film Bulletin praised Denys Coops camerawork and the acting performances, yet felt the film never quite realised its potential.  A. H. Weiler reviewing the work in The New York Times commended Cushings contribution, deeming it superior to the rest of the casts, although considered its plot contrived.  Mark Burger, reviewing a home video release for the Winston-Salem Journal in 2002, similarly noted the strong cast though found the muddled screenplay led to a merely "watchable" film. 

==Citations==
{{Reflist|colwidth=30em|refs=
 Burger, Mark (17 May 2002). "Video View". Winston-Salem Journal: p. 3. 
 Rosenbaum, Jonathan (1974). "Feature Films". Monthly Film Bulletin, 41:480/491: p. 243. 
 Weiler, A H (28 April 1973). "Screen: A Creepy Legend". The New York Times:  p. 21. 
}}

==References==
 

==Further reading==
* Chibnall, Steve; Petley, Julian (ed.) (2005). British Horror Cinema. Oxford: Taylor & Francis, pp.&nbsp;132–134. ISBN 0-203-99676-3 Manchester Univ. Press. ISBN 0-7190-6354-X

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 

 