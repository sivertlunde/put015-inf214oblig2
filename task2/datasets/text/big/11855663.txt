Peppermint Soda
{{Infobox film
| name           = Peppermint Soda
| image          = Diabolo menthe.jpg
| caption        = Diabolo menthe.  Poster design by Floch.
| director       = Diane Kurys
| producer       = Serge Laski
| writer         = Diane Kurys Alain Le Henry
| starring       = Eléonore Klarwein Odile Michel Yves Simon
| cinematography = Philippe Rousselot
| editing        = Joële Van Effenterre Gaumont 
| released       =  
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 1977 French film directed by Diane Kurys. This autobiographical film was her directorial debut, and it won the Prix Louis-Delluc. The high school where the film takes place is the Lycée Jules-Ferry in Paris, France.

== Plot ==
In 1963, thirteen-year-old Anne and fifteen-year-old Frederique are two Jewish sisters whose parents are separated. They live in Paris with their mom and spend their summer holidays at the beach with their father, around whom they both feel awkward. They attend a strict school. 

Anne is on the threshold of adolescence, and not much of an achiever and anti-social. She tries to find out what is going on around her by eavesdropping on her mother and sister. Her school grades are terrible and she has to deal with unfair teachers; on one occasion she is caught plagiarising an essay her sister wrote a year before. She also has her first period and is learning to talk to boys, and to sort things out with her mom after getting in trouble. 
 Algerian question and banning the atomic bomb|bomb. She has a crush on someone much older than herself, and is beginning to feel the first serious pangs of love. She also experiences her first friend who runs away, and her first loss of friendship over values.

== Cast ==
*Eléonore Klarwein as Anne
*Odile Michel as Frederique
*Anouk Ferjac as Mme. Weber
*Michel Puterflam as M. Weber
*Yves Rénier as Philippe
*Robert Rimbaud as M. Cazeau
*Marie Véronique Maurin as Muriel
*Corinne Dacla as Pascale Coralie Clément as Perrine
*Valérie Stano as Martine
*Jacqueline Boyer as Mlle. Petitbon
*Anne Guillard as Sylvie
*Véronique Vernon as Evelyne
*Dora Doll as Gymnastics teacher
*Françoise Bertin as French teacher
*Dominique Lavanant as The maths professor

== External links ==
* 

 

 
 
 
 
 
 
 
 
 

 
 