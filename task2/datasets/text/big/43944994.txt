Post Mortem (1982 film)
{{Infobox film 
| name           = Post Mortem
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = Pushparajan
| writer         = Pushparajan Dr Pavithran (dialogues)
| screenplay     = Dr Pavithran
| starring       = Prem Nazir Mammootty Sukumaran Balan K Nair
| music          = KJ Joy
| cinematography = KB Dayalan
| editing        = K Sankunni
| studio         = Rajapushpa
| distributor    = Rajapushpa
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by Pushparajan. The film stars Prem Nazir, Mammootty, Sukumaran and Balan K Nair in lead roles. The film had musical score by KJ Joy.    The film was remade in Tamil as Vellai Roja and in Telugu as S.P. Bhayankar.

==Cast==
*Prem Nazir
*Mammootty
*Sukumaran
*Balan K Nair
*Jalaja
*Janardanan
*Kuthiravattam Pappu
*Sathyakala

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Makkathe Panimathi Pole || Unni Menon, Chorus || Poovachal Khader || 
|-
| 2 || Raaja pushpame rithu raaja pushpame || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 
 
 