It Runs in the Family (2003 film)
{{Infobox film
| name           = It Runs in the Family
| image          = It runs in the family.jpg
| caption        =
| director       = Fred Schepisi
| producer       = Michael Douglas Marcy Drogin Jesse Wigutow - Associate Producer - Joel Douglas - Executive Producer - Kerry Orent Fred Schepisi
| writer         = Jesse Wigutow Diana Douglas Rory Culkin Bernadette Peters
| music          = Moose Charlap Paul Grabowsky Charles F. Sweeney Jr. Ian Baker
| editing        = Kate Williams Buena Vista Pictures  (International) 
| released       =  
| runtime        = 109 min.
| country        = United States
| awards         =
| language       = English Hebrew Spanish
| budget         =
| gross          = $8.2 million
}}

It Runs in the Family is a  , his son Michael Douglas, and Michaels son Cameron Douglas, who play three generations of a family.

Diana Dill, real-life mother to Michael Douglas and ex-wife of Kirk, plays the wife of Kirk in the film.

== Plot ==
The story involves a highly successful New York City family, each with its set of problems, and highlights the difficulties of the father-son relationship.

Mitchell Gromberg is trying to deal with health problems resulting from a stroke. (Kirk Douglas himself suffered a stroke in 1996.) His son, Alex, works as a lawyer in the firm that his father founded, but is questioning the usefulness of his work and his place in the family. Alexs son, Asher, does not take college seriously and seems lost. The youngest son Eli, while extremely intelligent, is entering a difficult pre-adolescent time, while being socially awkward.

When Alex indulges in a thoughtless and careless brief romantic fling and his psychologist wife Rebecca discovers it, his marriage is threatened. Asher is discovered with illegal drugs. But major life crises, although devastating, bring the family together in a familial show of support, respect, and love.

==Cast==
 
*Michael Douglas as Alex Gromberg
*Kirk Douglas as Mitchell Gromberg
*Cameron Douglas as Asher Gromberg
*Diana Dill as Evelyn Gromberg
*Bernadette Peters as Rebecca Gromberg
*Rory Culkin as Eli Gromberg
*Michelle Monaghan as Peg Maloney
*Geoffrey Arend as Malik
*Sarita Choudhury as Suzie
*Irene Gorovaia as Abby Staley
*Annie Golden as Deb
*Mark Hammer as Stephen Gromberg
*Audra McDonald as Sarah Langley
*Josh Pais as Barney

==Production==
In his role as producer, Michael Douglas suggested his mother (Diana Dill), Rory Culkin, and Bernadette Peters for their roles. Fred Schepisi noted that they were originally considering Sigourney Weaver for the part of Michaels wife. "Bernadette   was a really nice balance, playing straighter than you’d usually see her play..." 
 The In-Laws, grossed $20,453,431. 

==Reception==
Critic Steven Holden wrote in The New York Times that the movie is a "surprisingly complex and subtle portrait", and "Besides its laudable reluctance to tie up loose ends, the most courageous thing about It Runs in the Family is its refusal to try to make you love its aggressive, strong-willed characters." 

Most reviews, as tallied by rottentomatoes.com, were unfavorable or mixed; the movie has a "Rotten" rating of 28%, with the sites consensus stating "Despite its gimmick casting, the movie ultimately goes nowhere." Roger Ebert wrote: "But the movie is simply not clear about where it wants to go and what it wants to do. It is heavy on episode and light on insight, and although it takes courage to bring up touchy topics it would have taken more to treat them frankly." 

==See also==

*List of films featuring diabetes

==References==
 

==External links==
* 
*  from Rotten Tomatoes
* 
*  August 21, 2003

 

 
 
 
 
 
 
 
 
 
 