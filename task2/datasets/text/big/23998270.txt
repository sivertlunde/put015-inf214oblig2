It Happened One Sunday
{{Infobox film
| name           = It Happened One Sunday
| director       = Karel Lamac
| writer         = Frederic Gotfurt Victor Skutezky
| starring       = Robert Beatty Barbara White Marjorie Rhodes Kathleen Harrison Moore Marriott
| music          = 
| cinematography = Basil Emmott
| editing        = 
| distributor    = Pathé film
| released       = 1944 
| runtime        = 97 min
| country        = United Kingdom
| language       = English
}} British romantic romantic comedy film directed by Karel Lamac and starring Robert Beatty, Barbara White, Marjorie Rhodes, Kathleen Harrison and Moore Marriott.  In the film, an Irish servant girl working in Liverpool mistakenly believes that she has a secret admirer working at a hospital, and while seeking him out accidentally meets and falls in love with a serviceman there. She spends the rest of the day around Liverpool with him and they eventually decide to marry.  The film was based on the play She Met Him One Sunday by Victor Skutezky. 

==Cast==
* Robert Beatty as Tom Stevens
* Barbara White as Moya Malone
* Marjorie Rhodes as Mrs. Buckland
* Ernest Butcher as Mr. Buckland
* Judy Kelly as Violet
* Irene Vanbrugh as Mrs. Bellamy
* Kathleen Harrison as Mrs. Purkiss
* Moore Marriott as Porter
* C.V. France as Magistrate
* Marie Ault as Madame
* Brefni ORorke as Engineer

==References==
 

 
 
 
 
 
 
 


 
 