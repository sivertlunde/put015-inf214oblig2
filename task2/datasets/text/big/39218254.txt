Albeli
{{Infobox film
| name           =Albeli
| image          = 
| image_size     = 
| caption        = 
| director       = R. C. Talwar 
| producer       = Talwar Productions
| writer         = J. S. Casshyap
| narrator       = 
| starring       = Ramola Satish
| music          = G. A. Chisti
| cinematography = G. K. Mehta
| editing        = 
| distributor    = 
| released       = 1945
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Albeli is a Bollywood film. It was released in Bollywood films of 1945|1945.       The film was directed by R. C. Talwar for Talwar Productions. It starred Ramola, Satish, Hiralal, Sunder and Rooplekha.    The music was composed by Ghulam Ahmed Chishti|G. A. Chisti    with lyrics written by Shanti Swaroop and Chishti.

==Cast==
*Ramola (actress)|Ramola: Street girl
*Satish: Gypsy boy
*Hiralal: Rajasaheb
*Rooplekha: Raja’s daughter Sunder
*Manorama Manorama

==Soundtrack==
G. A. Chisti composed music for Indian films till 1949 after which he shifted to Pakistan where he continued to provide music for a large number of films. Chisti was also the co-lyricist for Albeli along with Shanti Swaroop. Munawwar Sultana (singer) and Zeenat Begum sang some of the songs for this film. There are eleven credited songs in the film and these were released by Columbia Records.    

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
|Bekason Ki Bebasi Ko "
| Munawwar Sultana
|-
| 2
| "Hum Apne Liye"
| 
|-
| 3
| "O Babu Ji Le Lo"
|  
|-
| 4
| "Do Saajan Ki Aur Do Apni"
| 
|-
| 5
| "Koi Le Lo Saiyan"
| Zeenat Begum
|-
| 6
| "Tu Taar Baja Main Gaati Hoon"
| 
|-
| 7
| "Bane Na Koi Ghar Janwai"
| 
|-
| 8
| "Kaliyon Ka Ras Pee Le"
| 
|-
| 9
| "Jawani Pe Rang"
|
|-
| 10
| "Tod Na Dena Dil"
|
|-
| 11
| "Chaman Ke Angoor"
| Zeenat Begum
|-
|}

  
==References==
 

==External links==
*  

 
 
 


 