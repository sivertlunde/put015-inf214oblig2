Silvester Home Run
{{Infobox Film
| name = Silvester Homer Run
| image =
| caption = Movie poster
| director = Sebastian Bieniek
| producer = Hartmut Bitomsky
| writer = Sebastian Bieniek
|´starring = Michael Kausch, Hannes Wegener, Lennie Burmeister, Sabine Wegner
| cinematography = Manuel Kinzer
| editing = Sebastian Bieniek
| runtime = 13 mins
| country = Germany German
| budget = 3000 €
}}
 German black-and-white short film produced 2008 by the Deutsche Film- und Fernsehakademie Berlin. It was written and directed by Sebastian Bieniek.

==Plot==
A story about people who can not communicate with each other. A father who can not talk with his son. A son who can not talk with his friend. And a mother who talks about everything, but doesnt say anything important.

==Festivals==
2008
*Montreal World Film Festival (focus on world cinema)
*Iris Prize (international competition)
*Lesbisch Schwule Filmtage Hamburg (Made in Germany)
*Mar del Plata Film Festival (international competition)
*Mix Brasil (international competition)

==External links==
*  
* 
* 
* 
*  
*  
*   Bafta

 
 
 
 
 
 

 