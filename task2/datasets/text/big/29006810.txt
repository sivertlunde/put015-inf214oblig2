The Blacks (film)
 
{{Infobox film
| name           = The Blacks
| image          = 
| caption        = 
| director       = Goran Dević Zvonimir Jurić
| producer       = Ankica Jurić-Tilić
| writer         = Goran Devic Zvonimir Juric
| starring       = Ivo Gregurevic
| music          = 
| cinematography = Branko Linta
| editing        = Vanja Sirucek
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Croatia
| language       = Croatian HRK 3.7 million 
}} Best Foreign Language Film at the 83rd Academy Awards,    but it did not make the final shortlist.   

==Cast==
* Ivo Gregurević as Ivo
* Krešimir Mikić as Barišić
* Franjo Dijak as Franjo
* Rakan Rushaidat as Darko
* Nikša Butijer as Saran
* Emir Hadžihafizbegović as Lega
* Stjepan Pete as File
* Saša Anočić as Vozač

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Croatian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 
 