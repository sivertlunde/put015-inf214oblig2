The Passion of Darkly Noon
{{Infobox film
| name           = The Passion of Darkly Noon
| image          = Darklynoon.jpg
| image_size     = 
| caption        = DVD cover for the film
| director       = Philip Ridley
| producer       = Dominic Anciano
| writer         = Philip Ridley
| narrator       = 
| starring       = Brendan Fraser Ashley Judd Viggo Mortensen
| music          = Nick Bicât
| cinematography = John de Borman
| editing        = Les Healey
| distributor    = Seville Pictures
| released       =  
| runtime        = 100 minutes
| country        = Belgium Germany United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1995 film written and directed by Philip Ridley, starring Brendan Fraser in the title role, and co-starring Ashley Judd and Viggo Mortensen.

==Cast==
*Brendan Fraser... Darkly Noon
*Ashley Judd... Callie
*Viggo Mortensen... Clay
*Loren Dean... Jude Lou Myers... Quincy
*Grace Zabriskie... Roxy

==Plot==
Darkly Noon (Fraser) is a young man who has spent his entire life as a member of an ultraconservative Christian cult. After a violent altercation that results in the dissolution of the cult and the death of Darklys parents, a disoriented Darkly wanders into a forest in the Appalachian region of North Carolina and is rescued from exhaustion by a coffin transporter named Jude (Loren Dean) and his friend Callie (Ashley Judd).
 mute boyfriend who builds the coffins Jude sells, returns home after being away for a few days.

When Darkly encounters Clays mother, Roxy (Grace Zabriskie), his internal conflicts grow even stronger. Roxy despises the relationship between Clay and Callie, and tells Darkly that Callie is a witch bent on destroying Roxys family.

Finally, in the films climax (narrative)|climax, Darklys rage boils over. Having wrapped himself in barbed wire and armed with one of Clays chisels, he bursts into Callie and Clays house, intent on murdering the couple, whom he discovers having sex. After a scene of horrific destruction, Darkly is finally tamed by Callies confession that she loves him. Unfortunately for Darkly, Jude arrives, rifle in hand, to rescue Callie and Clay. Jude shoots Darkly, who laments, "Who will love me now?" as he lies dying.

== Title source ==
Darkly received his unusual name from a passage in the bible, (1 Corinthians 13), "Now we see through a glass, darkly...".

== Critical reception==
Entertainment Weekly called The Passion of Darkly Noon "an unintended comedy with a scorcher of an ending", citing poor acting, over-the-top dialogue and implausible plot twists.  Conversely, Fangoria magazine praised the film effusively, citing especially the performance of Brendan Fraser. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 