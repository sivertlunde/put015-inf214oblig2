Jodi Breakers
 
 
{{Infobox film
| name           = Jodi Breakers
| image          = JodiBreakersFirstLook.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Ashwini Chaudhary
| producer       = Rajesh Rajilal
| story          = Ashwini Chaudhary
| screenplay     = Ashwini Chaudhary
| starring       = Bipasha Basu R. Madhavan Omi Vaidya Dipannita Sharma
| music          = Salim-Sulaiman
| cinematography = 
| editing        = Biren Jyoti Mohanty
| distributor    = Prasar Visions Pvt. Ltd.
| released       =   
|   country        = India
| language     =  Hindi
| budget         =  
| gross  =      
}} 2012 Hindi romantic comedy film directed by Ashwini Chaudhary. The film features R. Madhavan and Bipasha Basu in the leading roles with Omi Vaidya, Dipannita Sharma and Milind Soman in supporting roles. The film had been previously titled as Shaadi Fast Forward but was later changed to Jodi breakers.  It was released on 24 February 2012 to negative reviews from critics however opened to mixed response at the box office.

==Synopsis==
Sid (R. Madhavan), a bachelor works as a divorce managing officer. He meets Sonali (Bipasha Basu), who joins him in his business of splitting couples and they earn well in the process. Together, they are known as the Jodi (Couple) Breakers. Sometimes, they secretly make husband and wife fight with each other, that will result in their divorce. This happens with Marc (Milind Soman) and Maggie (Dipannita Sharma). But when Marc finds out the reason that got him and Maggie divorced, hell breaks loose.

==Cast==
* R. Madhavan as Sid Khanna
* Bipasha Basu as Sonali Agnihotri
* Omi Vaidya as Nainsukhbhai Chamanlal Patel
* Dipannita Sharma as Maggie
* Milind Soman as Marc
* Mrinalini Sharma as Ira Helen as Madonna
* Tarana Raja Kapoor as Dr. Isha
* Mazhar Sayed

== Release ==

=== Controversy ===
Jodi Breakers faced controversy when the Indian distributors of the 2010 French film Heartbreaker (2010 film)|Heartbreaker alleged that the story of Jodi Breakers was plagiarised from Heartbreaker. The makers of Jodi Breakers offered to let the Heartbreakers team read the entire script, provided they agree to sign a Non Disclosure Agreement (NDA), which the Heartbreakers team was unwilling to sign. 

==Reception ==

===Critical reception=== DNA India gave the movie 2 out of 5 stars, mentioning "Leave all expectations behind before entering the hall and you might (urgency redefined) walk out with a smiling face."  Mansha Rastogi of Nowrunning gave the movie 2 out of 5 stars, noting "Jodi Breakers fails to make up for a good rom-com. Watch the film only if you are a fan of Maddy and Bips."  Savera R Someshwar of Rediff.com gave the movie 1 out of 5 stars, saying that "Jodi Breakers could have been a good romance or a good comedy but its neither. The film is seriously flawed."  Komal Nahta of Koimoi.com also gave the movie 1 in a scale of 5 stars, adding that "Jodi Breakers is a dull show. It will not be able to break even.".   Faisal Saif of Global Movie magazine gave the film 2 out of 5 stars and pointed "Nothing great to write about except performances". 

==Soundtrack==
{{Infobox album
| Name        = Jodi Breakers
| Type        = soundtrack
| Artist      = Salim-Sulaiman
| Cover       = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      =
| Label       = T-Series
| Producer    = Salim-Sulaiman
| Last album  = Ladies vs Ricky Bahl  (2011)
| This album  = Jodi Breakers (2012)
| Next album  = Heroine (2012 film)|Heroine (2012)
}}
The soundtrack of Jodi Breakers, released by T-Series on 17 January 2012, featured music composed by Salim-Sulaiman with lyrics penned by Irshad Kamil and Shabbir Ahmed. 

{{track listing
| headline  = Tracklist
| extra_column    = Singer(s)
| total_length    = 
| all_lyrics      = 
| lyrics_credits  = no
| title1          = Kunwara
| lyrics1         = 
| extra1          = Salim Merchant
| length1         = 4:24
| title2          = Bipasha
| lyrics2         = 
| extra2          = Shrraddha Pandit, Shadab Faridi
| length2         = 4:42
| title3          = Darmiyaan
| lyrics3         = 
| extra3          = Shafqat Amanat Ali, Clinton Cerejo
| length3         = 5:55
| title4          = Mujhko Teri Zaroorat Hai
| lyrics4         = 
| extra4          = Salim Merchant, Shadab Faridi, Shrraddha Pandit
| length4         = 5:25
| title5          = Jab Main Tumhare Saath Hoon
| lyrics5         = 
| extra5          = Salim Merchant, Benny Dayal, Shilpa Rao
| length5         = 5:12
| title6          = Darmiyaan (Reprise)
| length6         = 
| extra6          = Shreya Ghoshal
| length6         = 2:44
| title7          = Bipasha (Remix)
| lyrics7         = 
| extra7          = Salim Merchant, Shrraddha Pandit
| length7         = 4:41
| title8          = Mujhko Teri Zaroorat Hai (Remix)
| lyrics8         = 
| extra8          = Rahat Fateh Ali Khan, Shrraddha Pandit
| length8         = 4:41
}}

===Reception===
The album received mixed to positive reviews. Joginder Tuteja of Bollywood Hungama gave the music an overall rating of three and half out of five saying "Jodi Breakers has all in it to be one of the more popular albums at the very beginning of the year. Salim-Sulaiman along with Irshad Kamil and Shabbir Ahmed have done their job and now it is up to the makers to make the most out of it and promote it well enough to enhance the musics reach."  Rumnique Nannar of Bollyspice concluded"Jodi Breakers is an a good album but it is let down by two duds that fail rise above the humdrum lyrics." 

==References==
 

==External links==
*  
*  
*   at Bollywood Hungama

 
 
 
 