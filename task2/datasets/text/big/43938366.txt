My Beloved Dearest
 
{{Infobox film
| name           = Sayang DiSayang (My Beloved Dearest)
| image          = 
| caption        = 
| director       = Sanif Olek
| producer       = Sanif Olek
| writer         = Sanif Olek Gene Sha Rudyn
| starring       = Rahim Razali Aidli Mosbit
| music          = Imran Ajmain
| cinematography = M Senthilnathan Vincent Wong
| editing        = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Singapore
| language       = Malay Bahasa Indonesia
| budget         = 
}}

My Beloved Dearest ( ) is a 2013 Singaporean drama film directed by Sanif Olek. 

The film tells the story of the fractious relationship between Murni, a homesick Indonesian caregiver slash domestic helper and her crabby and disabled older Singaporean master, a widower named Park Harun who sits on his wheelchair all day by the sliding glass door where the curtains are shut and seemingly waiting for someone to rescue him from his loneliness and helplessness. Park Harun resists all basic conversation with Murni and rudely expresses his dislike of the food that she serves him every day, including the traditional Nusantara dish Sambal Goreng. 

The film began its principal photography in 2009 but was canned until 2013 due to lack of funds. Other than featuring the quintessential Nusantara dish, Sambal Goreng, it features evergreen Nusantara songs such as Sijali-jali, Pesan Kakek, Seroja, Main Tali and Sayang Disayang by Zubir Said, sung live by the films cast. Imran Ajmains original composition, Hanya Menari from his first album, Dengan Secara Kebetulan was used as the films main soundtrack. It is also notable that this film is Singapores first, local-made, Malay-language film since its independence.
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.     

In November 2013 the film won the Best Asian Film (Jury Prize) at the SalaMindanaw International Film Festival in the Philippines. 

In 2014 it was the closing film for the Southeast Asian Film Festival  and opening film for the Phnom Penh International Film Festival.  It was an Official Selection at the Jogja-NETPAC Asian Film Festival,  the Hawaii International Film Festival where it was showcased in the festivals New Frontiers section,  the Edmonton International Film Festival,  the Luang Prabang Film Festival,  the Barcelona International Film Festival  and showcased at the Southeast Asian Screen Academy.  It also represented Singapore at the Asia Pacific Screen Awards but was not nominated.

Other awards include the Mexico International Film Festivals Best Musical award.   Best Feature at the 12th Royal Bali International Film Festival.

==Cast==
* Rahim Razali as Harun
* Aidli Mosbit as Murni
* Asnida Daud as Siti
* Hashimah Hamidon as Minah
* Rafaat Hj Hamzah as Rosli
* Norsiah Ramly
* Keatar HM
* Shah Iskandar 
* Khalid Baboo

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Singaporean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 