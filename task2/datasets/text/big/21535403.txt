Miss April (1958 film)
{{Infobox film
| name           = Miss April
| image          = 
| caption        = 
| director       = Göran Gentele
| producer       = 
| writer         = Göran Gentele
| starring       = Gunnar Björnstrand
| music          = 
| cinematography = Karl-Erik Alberts
| editing        = Wic Kjellin
| distributor    = 
| released       = 3 November 1958
| runtime        = 98 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}

Miss April ( ) is a 1958 Swedish comedy film directed by Göran Gentele. It was entered into the 1959 Cannes Film Festival.   

==Cast==
* Gunnar Björnstrand - Marcus Arwidson
* Lena Söderblom - Maj Bergman
* Jarl Kulle - Osvald Berg
* Gaby Stenberg - Vera Stenberg
* Douglas Håge - Chorus master
* Hjördis Petterson - Mrs. Berg
* Meg Westergren - Anna
* Lena Madsén - Siri
* Olof Sandborg - Head of the Opera
* Sif Ruud - Mrs. Nilsson
* Birgitta Valberg - Ms. Holm, secretary
* Per Oscarsson - Sverker Ek
* Sven Holmberg - Malmnäs
* Björn Gustafson - Director at the opera
* Georg Skarstedt - Stage Manager
* Bengt Eklund - Hink
* Börje Mellvig - Prosecutor
* Hans Strååt - Baecke
* Tord Stål - Judge
* Kurt Bendix - Opera Conductor

==References==
 

==External links==
* 

 
 
 
 
 


 
 