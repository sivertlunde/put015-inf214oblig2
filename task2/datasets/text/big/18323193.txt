Utamaro and His Five Women
{{Infobox film name           = Utamaro and His Five Women image          = caption        = director       = Kenji Mizoguchi producer       = writer         = Yoshikata Yoda starring       = Minosuke Bandō Kinuyo Tanaka Kōtarō Bandō Hiroko Kawasaki Toshiko Iizuka music          = cinematography = Minoru Miki editing        = Shintarō Miyamoto distributor    = Shochiku released       = 17 December 1946 runtime        = 106 min. country        = Japan awards         = language  Japanese
|budget         = preceded_by    =
}}
 American occupation.

==Theme== floating world - he painted also idyllic outdoor scenes, Yoshiwara festivals and drinking bouts, bathers and shell-divers, as well as erotica. The film dramatically presents this sense of range, and openness to lifes variety, and contrasts the old official court-approved style of painting, called kano with the new, dynamic form of painting known as ukiyo-e (literally:paintings of the floating world). 

==Synopsis==
The story is set in Edo (now Tōkyō) in Japan.
 high shoes) along an avenue of cherry trees. Koide, called Seinosuke by his woman (Kotaro Bando), an artist/samurai apprenticed to a Kanō school|Kanō master, leaves the parade and visits a print shop where he sees  a woodcut print by Utamaro that boasts  of ukiyo-e s superiority to the official style. Enraged, he  goes to a tea-house to find Tsutaya Jūzaburō, the owner of the print shop, to express his displeasure. Word is leaked to Utamaro to avoid the tea-shop, but instead he goes there directly to investigate. Koide then challenges him to a duel. Utamaro counter-challenges him with a different kind of duel––a painting contest.  

Utamaro is declared the winner by Koide, who then acknowledges Utamaros superiority.

==Production history== Allied occupation of Japan which followed World War II. At the time, film production was overseen by representatives of the Occupation forces, and Jidaigeki (period films) like Utamaro were rarely made, as they were seen as being inherently nationalistic or militaristic.   

===The Film as Autobiography===
Though Utamaro and His Five Women is based on the life of Kitagawa Utamaro, it is frequently seen as being an autobiographical work.   

In her article on the film for the Australian film journal  , who worked with him (more precisely, for him) for 20 years, claimed in his memoirs that in the script for this film he was almost unconsciously drawing a portrait of Mizoguchi through Utamaro. The equation Utamaro=Mizoguchi has been irresistible to most critics as the two artists did have a lot in common. Both of them worked in a popular mass-produced medium operated by businessmen, and chafed under oppressive censorship regimes; both frequented the pleasure quarters and sought the company of geishas; but, most significantly, they both achieved fame for their portraits of women. In a highly charged scene in this film, Utamaro paints, directly on the back of a beautiful courtesan, a sketch that is later tattooed into her skin. One could say that this creative act (and the passion the artist displays in executing it) literalises the fact that both artists achieved fame on the backs of women – relying on them to arouse and express themselves, emotionally and aesthetically."  

==References==
 

==External links==
*  at Senses of Cinema
*  

 

 
 
 
 
 
 
 
 
 
 