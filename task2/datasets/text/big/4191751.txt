Def Jam's How to Be a Player
{{Infobox film 
| name        = How to Be a Player 
| image       = How to Be a Player (DVD cover).jpg
| writer      = Mark Brown Demetria Johnson  Anthony Johnson Max Julien Beverly Johnson Gilbert Gottfried and Bernie Mac 
| producer    = Todd R. Baker Mark Burg Preston L. Holmes Russell Simmons
| music       = Darren Floyd Def Jam Pictures
| distributor = Gramercy Pictures
| released    =  
| runtime     = 93 min.
| language    = English
| budget      = 
| gross       =
}} directed by written by Mark Brown and Demetria Johnson.
 How to Foxy Brown featuring Dru Hill.

==Plot outline==
Drayton "Dray" Jackson ( ). But, Drays sister, Jenny, (Natalie Desselle-Reid) and her friend Katrina (Mari Morrow) strategize to invite all the women Dray has been playing to the same party that he is attending. He begins to make the moves on his sisters friend, almost succeeding until the one girl he really has true feelings for shows up at the door. 

==Cast==
*Bill Bellamy —  Drayton "Dray" Jackson
*Natalie Desselle-Reid — Jenny Jackson
*Lark Voorhies — Lisa
*Mari Morrow — Katrina (Jennys Friend)

*Pierre Edwards — David
*Bernie Mac — Buster
*Jermaine Huggy Hopkins — Kilo Anthony Johnson — Spootie
*Max Julien — Uncle Fred
*Beverly Johnson — Robin
*Gilbert Gottfried — Tony the Doorman
*Stacii Jae Johnson — Sherri
*Elise Neal — Nadine
*J. Anthony Brown — Uncle Snook
*Amber Smith — Amber
*Marta Cunningham - C.C.

==Soundtrack==
{|class="wikitable"  Year
!rowspan="2"|Album Peak chart positions Certifications
|-
!width=40| Billboard 200|U.S. 
!width=40| Top R&B/Hip-Hop Albums|U.S. R&B 
|- 1997
|How How to Be a Player 
*Released: August 5, 1997 Def Jam 7
|align="center"|2
|
*Recording Industry Association of America|US: Gold
|-
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 