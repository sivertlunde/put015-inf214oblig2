Las Abandonadas
{{Infobox film
| name = Las Abandonadas
| image = LasAbandonadasPoster.jpg
| caption = Theatrical release poster
| director = Emilio Fernández
| producer = Felipe Subervielle
| writer = Emilio Fernández Mauricio Magdaleno
| starring = Dolores del Río Pedro Armendáriz Víctor Juco Paco Fuentes Fanny Schiller Maruja Grifell
| music = Manuel Esperón
| cinematography = Gabriel Figueroa
| editing = Gloria Schoemann
| distributor = Films Mundiales
| released = 1945
| runtime = 103 minutes
| country = México
| language = Spanish
| budget =
}}
Las Abandonadas ("The Abandoned") is a Mexican film of 1945, directed by Emilio Fernández, starring Dolores del Río and Pedro Armendáriz.

==Plot==
Margarita Pérez,a young and pregnant woman abandoned by her fiance has to deal with a society that 
is cruel with unmarried mothers.

== Curiosities ==
The movie have one of the greatest and expensive wardrobes of the Mexican Cinema, designed by Armando Valdes Peza. It is a sample of the deep devotion that the director and the photographer felt for Dolores del Río.

== External links ==
* 

 

 
 
 
 
 
 

 