Land Raiders
{{Infobox film
| name           = Land Raiders
| image          = Landraidpos.jpg
| caption        = Original film poster
| director       = Nathan Juran
| producer       = Charles H. Schneer
| writer         = 
| narrator       = 
| starring       = Telly Savalas
| music          = Bruno Nicolai
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film directed by Nathan Juran. It stars Telly Savalas and George Maharis and was filmed in Spain and Hungary. 

==Plot==
Vince Cardens hatred for Indians has caused an estrangement from his brother Paul and disappointment from wife Martha. A wagon train joined by Paul is attacked by braves in retaliation for a raid by Vince, and the only survivor besides Paul is a woman, Kate Mayfield, returning home from her education back East.

Vince organizes one more attack, pitting him against not only the Indians but his own brother.

==Cast==
*Telly Savalas as Vince Cardenas
*George Maharis as Paul Cardenas
*Arlene Dahl as Martha Cardenas
*Janet Landgard as Kate Mayfield
*Guy Rolfe as Major Tanner
*Paul Pacerni as Carney and H.P. Pacerni

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 