Al otro lado
 
{{Infobox film
| name           = Al Otro Lado
| image          = Alotroladoposter.jpg
| caption        = 
| director       = Gustavo Loza
| producer       = Elisa Salinas
| writer         = Gustavo Loza
| screenplay     = 
| story          = 
| based on       =  
| starring       = Adrian Alonso Carmen Maura Jorge Milo Vanessa Bauche
| music          = Hector Ruiz Quintanar
| cinematography = Gerónimo Denti Patrick Murguia Serguei Saldívar Tanaka
| editing        = Roberto Bolado Juan Fernández
| studio         = 
| distributor    = Imcine
| released       =  
| runtime        = 90 minutes
| country        = 
| language       = Spanish
| budget         = 
| gross          = 
}}
 Mexican film directed by Gustavo Loza.  The film follows the story of three children, one from Mexico, Cuba, and Morocco, as they attempt to deal with realities of immigration in their societies.  The film won three awards at the Lleida Latin-American Film Festival in 2006, including Best Film and the Audience Award; Vanessa Bauche won best actress at the festival for her performance. Mexican candidate for the Academy Award for Best Foreign Language Film at the 78th Academy Awards, but was not selected as one of the final five nominees.

== Plot ==

=== Mexico ===
Priciliano, the first boy introduced in the story, has a very strong bond with his father. At the beginning of the film, his father tells him a story about a princess who was kidnapped by a conquistador and commits suicide in the lake he and his friend have been playing in. He is trying to teach his son the importance of respecting the dead, a very prevalent theme in Mexican culture. However, Priciliano is struck with grief when his father leaves his family to go find work "al otro lado" (on the other side). He is particularly upset because he is about to celebrate his ninth birthday, which his father will be missing. Frustrated, he attempts to cross the river to the other side with his friend and nearly drowns but is rescued by the ghost of the princess his father told him about. An older man in the community tells Priciliano that it is important to stay with his family because they need him, and the boy accepts his role as "the man of the house" in his fathers place and begins to learn how to live everyday life without him.

=== Cuba ===
The second boy introduced in the story, Ángel, is ashamed of his mother, who is promiscous. He was told before the story began that his father is a famous person and lives "al otro lado." Desperate for a sense of pride, he and his friend attempt to cross the ocean to find his father. Sadly, his friend drowns in the ocean and Ángel miraculously escapes. He doesnt have the courage to tell his friends mother of what happened and feels extremely guilty. His grandfather notices something is wrong and urges his mother to talk to him. He tells his mother a little of what happened in the ocean and why they were there, and his mother figures out the rest and tells him that his father knows nothing of Ángels existence. But tells him that he is all that she and his grandfather have and vice versa. Ángel learns to be content in his situation and the cinematography suggests that his friends death is no longer a burden to him, but a memory to carry with him.

=== Morocco ===
Fátima is a girl who runs away from home to find her father, bringing only a picture of him with her. She is intercepted, though, by slave traders, who offer to take her across to Spain on their boat. Once the traders arrive, a woman who works with them refuses to send Fátima to work because Fátima is too young and they have enough girls anyway. This woman, who does not speak Arabic, takes Fátima in and eventually softens enough to drive Fátima to Málaga and help her find her father. The father is with a Spanish woman, but once confronted with his daughter, he leaves the other woman to return Fátima home. The movie ends with a shot of the two of them in front of their home village.

== External links ==
*  

 
 
 
 
 