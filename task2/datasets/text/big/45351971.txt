Digging for Fire
 
{{Infobox film
| name           = Digging for Fire
| image          = 
| alt            = 
| caption        = 
| director       = Joe Swanberg
| producer       = Joe Swanberg Alicia Van Couvering Jake Johnson
| writer         = Joe Swanberg Jake Johnson
| starring       = Jake Johnson Rosemarie DeWitt Orlando Bloom Brie Larson Sam Rockwell Anna Kendrick Mike Birbiglia
| music          = Dan Romer Ben Richardson
| editing        = Joe Swanberg
| studio         = Lucky Coffee Productions The Orchard Sony Pictures Worldwide
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
Digging for Fire is a 2015 American dramedy directed by Joe Swanberg.  It is also co-written by Swanberg and Jake Johnson, Johnson also serves as a producer alongside Swanberg and Alicia Van Couvering.   It stars Johnson, Rosemarie DeWitt, Orlando Bloom, Brie Larson, Sam Rockwell, Anna Kendrick and Mike Birbiglia.
 The Orchard Sony Pictures Worldwide acquired international distribution rights. 

==Synopsis==
The film follows the discovery of a bone and a gun which sends a husband and wife, each full of doubts about their future and anxiety about the present, on separate adventures over the course of a weekend.

==Cast==
*Jake Johnson
*Rosemarie DeWitt
*Orlando Bloom
*Brie Larson
*Sam Rockwell
*Anna Kendrick
*Mike Birbiglia
*Sam Elliot
*Judith Light
*Ron Livingston
*Melanie Lynskey
*Jenny Slate Jane Adams
*Chris Messina
*Tim Simons

==Reception==
The film has been met with positive reviews from critics, on the review site Rotten Tomatoes the film currently holds an 86% approval rating, based on 7 reviews.   On Metacritic, the film has a score of 70 out of 100, indicating "generally favorable reviews."

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 