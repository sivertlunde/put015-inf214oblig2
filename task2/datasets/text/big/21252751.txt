Maddalena (1954 film)
 
{{Infobox film
| name           = Maddalena
| image	         = Maddalena FilmPoster.jpeg
| caption        = Film poster
| director       = Augusto Genina
| producer       = Giuseppe Bordogni
| writer         = Carlo Alianello Pierre Bost Madeleine Masson de Belavalle Alessandro De Stefani Augusto Genina Giorgio Prosperi
| starring       = Märta Torén
| music          = Ennio Morricone
| cinematography = Claude Renoir
| editing        = Giancarlo Cappelli
| distributor    =
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Maddalena is a 1954 Italian drama film directed by Augusto Genina. It was entered into the 1954 Cannes Film Festival.   

==Cast==
* Märta Torén - Maddalena
* Gino Cervi - Don Vincenzo
* Charles Vanel - Giovanni Lamberti
* Jacques Sernas - Giovanni Belloni
* Folco Lulli - The Herdsman
* Angiola Faranda - The Herdsmans Daughter

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 



 