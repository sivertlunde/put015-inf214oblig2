Things Are Tough All Over
 
{{Infobox film
| name           = Things Are Tough All Over
| image          = Things Are Tough All Over (1982).jpg
| caption        = Theatrical release poster
| director       = Thomas K. Avildsen Howard Brown
| writer         = Tommy Chong Cheech Marin
| starring = {{plainlist|
* Cheech Marin
* Tommy Chong
}}
| music          = Gaye Delorme Bobby Byrne
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 90 min.
| country        = United States
| awards         =
| language       = English
| budget         = $7 million
| gross          = $21,134,374
}}
 fullscreen versions of the film on the same disc.

==Plot==
The movie opens with Cheech and Chong driving a limo through the desert. Chong, who has decided to stop doing drugs for a while, is talking about rock and roll, and Cheech is falling asleep, but Cheech is narrating over whats happening. He says that "things are tough all over" and that hes going to tell their story.
 French girlfriends, who are more in love with the stoners.
 Las Vegas. stoners to drive the limousine to Las Vegas, telling them that theyre sending them on a "rock tour."
 strapped for cash, the man at the gas station takes a piece of the car as payment. With that idea, Cheech and Chong find themselves driving across the country, selling parts and pieces of the car for gas, food, and supplies. Soon, their car becomes a wreck and looks messed up, but Cheech and Chong continue to sell parts to get by. While out in the middle of the deserts, they decide to pick up a hitchhiker, who turns out to be none other than Donna (Evelyn Guerrero), Cheechs girlfriend (from Cheech & Chongs Next Movie and Nice Dreams). The two decide to take Donna in their messed-up limo to the nearest gas station. However, Donna is traveling with dozens of Mexicans, so the stoners end up driving all the Mexicans and Donna to the nearest gas station. To pay for gas, Cheech and Chong give the old man that runs the place a chair from the limo—which unknown to them is stuffed with the Arabs money.

Cheech and Chong deliver the messed-up limousine to the Arabs other oil plant in the desert, to find no one there. With no other transportation or money, Cheech and Chong set out on foot into the desert. They wander into the burning deserts, suffering the Nevada heat, and trying to get cars to stop- they remain unsuccessful. Eating peyote to survive and singing to pass the time, Cheech and Chong do their best to get through the desert, though they believe theyll die from the heat.

Back in Chicago, the Arabs find out that Cheech and Chong have delivered what remains of the car without any money in it. After deciding to kill them, the Arabs fly out to Nevada in their private plane and set out by car into the desert. The Arabs meet the old man at the gas station and learn that Cheech and Chong have been around, and set out into the deserts; their car breaks down, leaving the Arabs to wander through the Nevada deserts and get lost. Meanwhile, while walking through the desert, Cheech and Chong are picked up by the Arabs French girlfriends, who take them to an abandoned motel in the middle of the desert. The French girlfriends have sex with the stoners, and are (unknown to them) on a hidden camera film. Afterwards, the French ladies leave in their car, leaving the stoners stranded in the middle of nowhere yet again. Meanwhile, the Arabs are having the same problem, looking for Cheech and Chong in the middle of the desert, having no idea where to go.
 porno theater with the murder-happy Arabs on their tail. In the theater, the Arabs see the showing of the hidden camera film of the stoners having sex with the Arabs girlfriends. While the Arabs watch, inspired, Cheech and Chong escape. The stoners ditch the womens clothes and set out on foot to leave Las Vegas. The next day, as Cheech and Chong walk out of the city, a car pulls up, and Cheech and Chong get in, to find the Arabs and their French girlfriends. At first, Cheech and Chong are terrified and try to escape, but Mr. Slyman reveals that, instead of killing them, the Arabs have decided to cast the duo in porn films and launder the money through the enterprise.

A happy ending, with a narrating Cheech reminding us that "hey, things are tough all over."

==Cast==
 
*Cheech Marin as Cheech, Mr. Slyman and Narrator
*Tommy Chong as Chong "Man" and Prince Habib
*Evelyn Guerrero as Donna
*John Steadman as Old Timer George Wallace as The Champ
*Toni Attell as Cocktail Waitress
*Mike Bacarella as Cop in Laundromat
*Billy Beck as Pop
*Senta Moses as Kid in Laundromat
*Don Bovingloh as Maitre D
*Richard "Moon" Calhoun as Drummer
*Jennifer Condos as Bass Player
*John Corronna as St. Louis Biker
*Aaron Freeman as Cop in Ghetto
*Mike Friedman as Car Rental Agent
*Maya Harman as Belly Dancer
*Vanaghan S. Housepian as Henchman
*Garth Shaw as Tourist 

Comedians Rip Taylor, Dave Coulier and Ruby Wax have brief appearances in the restaurant scene. The Arabs French girlfriends are played by Rikki Marin and Shelby Chong.

==Soundtrack==
The movie begins with the song Johnny B. Goode by Chuck Berry.

==References==
 

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 