The Derby Stallion
 
 

{{Infobox film
| name           = The Derby Stallion
| image          = The Derby Stallion Poster.jpg
| director       = Craig Clyde
| producer       = Tonja Walker Kevin Summerfield
| writer         = Kimberly Gough
| starring       = Zac Efron Bill Cobbs William R. Moses Tonja Walker Rob Pinkston Crystal Hunt Michael Nardelli Colton James
| music          = Billy Preston
| cinematography = John Gunselman
| studio         = Tonja Walker Productions
| distributor    = Echo Bridge Entertainment
| released       = June 30, 2005
| runtime        = 98 minutes
| country        = United States
| language       = English
}}
The Derby Stallion is a 2005 film starring Zac Efron.

==Plot==
 White girl named Julie, who taught him to ride.

At a game, Patrick is distracted by his teammate Chucks (Rob Overton) older sister Jill (Crystal Hunt) Patrick and knocked out by the pitch. Patricks father asks him about his fall, and Patrick confronts him about pushing him into baseball. Patrick visits Houston after the game, and Houston tells him more stories. This time about Julies death when she told Houston she was going to marry him. Patrick feels sorry and asks Houston to train him in steeplechase. Houston agrees, but Patricks parents are appalled when he asked them for permission. His mother thinks it is too dangerous, and his father is concerned about Patrick taking riding lessons from a drunk black man. When they agree, Houston buys Patrick a horse named Rusty and tells Patrick to take care of him till he says to stop.

Patrick meets again with Chuck and Jill and asks them to join him at Houstons barbecue; he also invites his parents. There, his parents begin to trust Houston more. Patrick starts to learn to ride Rusty as Jill tends to Houstons garden. One day, the rich town bully and steeplechase champion, Randy, destroys Houstons vegetable/fruit stand; Houston warns him to stop. Houston then starts to collapse while Randy leaves and Patrick arrives. Jill calls 9-1-1, but Houston refuses to go to the hospital and dies the next day, leaving Patrick devastated. Patrick drops out of the steeplechase race and stops taking care of Rusty.
 racing silks and a letter. Patrick decides to enter the race and gets there just in time. Jill kisses Patrick before he leaves and sets up. Patrick, along with strong Rusty and Houstons voice in his head, wins race. In the end, Patrick lays his trophy on Houstons grave, along with Houstons hat and his harmonica.

==Cast==
*Bill Cobbs as Houston Jones
*Zac Efron as Patrick McCardle
*Crystal Hunt as Jill Overton
*William R. Moses as Jim McCardle
*Tonja Walker as Linda McCardle
*Rob Pinkston as Chuck Overton
*Colton James as Donald
*Michael Nardelli as Randy Adams
*Billy Preston as Will Gentry
*Isabella Davidson as Tammy McCardle
*Abrianna Davidson as Annie McCardle

==External links==
*  
*  

 
 