Mother's Joy
 
{{Infobox film
| name           = Mothers Joy
| image          = 
| caption        = 
| director       = Ralph Ceder
| producer       = Hal Roach
| writer         = H. M. Walker
| starring       = Stan Laurel
| music          = 
| cinematography = Frank Young
| editing        = Thomas J. Crizer
| distributor    = 
| released       =  
| runtime        = 20 minutes
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 silent comedy film starring Stan Laurel.   

==Cast==
* Stan Laurel - Magnus Dippytack/Basil Dippytack, his son
* Ena Gregory - Maid
* Mae Laurel - Miss Flavia de Lorgnette James Finlayson - Baron Buttontop
* Jack Ackroyd - Attorney McFumble
* William Gillespie - Houseguest
* Helen Gilmore - Dippy
* George Rowe - Waiter Charlie Hall - Houseguest
* Charles Lloyd
* Earl Mohan
* Laura Roessing
* Glenn Tryon
* Tonnage Martin Wolfkeil

==See also==
* List of American films of 1923
* Stan Laurel filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 