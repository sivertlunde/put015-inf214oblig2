Sweet Jesus, Preacherman
{{Infobox film
| name           = Sweet Jesus, Preacherman
| image          = Sweet Jesus, Preacherman poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Henning Schellerup 
| producer       = Daniel Cady 
| writer         = John Cerullo M. Stuart Madden Abbey Leitch  William Smith Michael Pataki Tom Johnigarn Joe Tornatore Damu King
| music          = Horace Tapscott
| cinematography = Paul Hipp
| editing        = Warren Hamilton Jr. 	
| studio         = Capitol Cinema Entertainment Pyramid Inc.
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 William Smith, Michael Pataki, Tom Johnigarn, Joe Tornatore and Damu King. The film was released on May 25, 1973, by Metro-Goldwyn-Mayer.  

==Plot==
Holmes is a hitman who has nailed one victim after another. Having iced a large number of them, he is sent by his boss Martelli
to infiltrate a section of the black quarter of the inner city. To do this, he becomes Reverend Lee, a Baptist preacher who comes to the local church to preach. Finding that other thugs are there, he decides to take the entire section for himself.

== Cast ==
*Roger E. Mosley as Holmes / Lee William Smith as Martelli
*Michael Pataki as State Senator Sills
*Tom Johnigarn as Eddie Stoner
*Joe Tornatore as Joey
*Damu King as Sweetstick
*Marla Gibbs as Beverly Solomon
*Sam Laws as Deacon Greene
*Phil Hoover as George Orr
*Paul Silliman as Roy
*Chuck Lyles as Detroit Charlie
*Norman Fields as Police Captain
*Della Thomas as Foxey
*Amentha Dymally as Mrs. Greene
*Patricia Edwards as Marion Hicks
*Chuck Douglas Jr. as Lenny Solomon
*Vincent LaBauve as Bobby Thompson
*Chuck Wells as Eli Stoner
*Betty Coleman as Maxine Gibbs
*Lou Jackson as Randy Gibbs
*Lillian Tarry as Mother Gibbs
*T.C. Ellis as Earl Saunders
*Lee Frost as 1st Policeman
*Joanne Bruno as Widow Foster 
*K.D. Friend as Funeral Minister 
*Gordon James as Restaurant Hood
*Billy Quinn as Sweetsticks Bodyguard

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 