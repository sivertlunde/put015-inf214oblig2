These Wilder Years
{{Infobox film
| name           = These Wilder Years
| image_size     =
| image	         = These Wilder Years FilmPoster.jpeg
| caption        = Roy Rowland
| producer       = Jules Schermer Frank Fenton
| narrator       =
| starring       = James Cagney Barbara Stanwyck Walter Pidgeon
| music          = Jeff Alexander
| cinematography = George J. Folsey
| editing        = Ben Lewis
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $1,257,000  . 
| gross          = $877,000 
}} Roy Rowland, and starring James Cagney and Barbara Stanwyck. It is the story of a businessman who tries to find the illegitimate son he gave up to an orphanage many years ago.

These Wilder Years marked the first onscreen pairing of Hollywood stars Cagney and Stanwyck. 

==Plot==
 
Lonely middle-aged businessman Steve Bradford (James Cagney) returns to his old town in hopes of finding the son he fathered 20 years earlier. Choosing his career over marriage and family, he got a girl pregnant and she gave the baby up for adoption. He goes to an orphanage run by Ann Dempster (Barbara Stanwyck) to find out information about his son. 

==Cast==
* James Cagney as Steve Bradford
* Barbara Stanwyck as Ann Dempster
* Walter Pidgeon as James Rayburn
* Betty Lou Keim as Suzie
* Don Dubbins as Mark
* Edward Andrews as Mr. Spottsford
* Basil Ruysdael as Judge
* Grandon Rhodes as Roy Oliphant Will Wright as Old Cab Driver
* Lewis Martin as Dr. Miller
* Dorothy Adams as Aunt Martha Dean Jones as Hardware Clerk
* Herb Vigran as Traffic Cop
* Michael Landon as Boy in Poolhall

==Box office==
According to MGM records the film earned $572,000 in the US and Canada and $305,000 elsewhere, resulting in a loss of $600,000. 

==References==
 	

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 