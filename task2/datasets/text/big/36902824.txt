The To Do List
 
{{Infobox film
| name           = The To Do List
| image          = The To Do List film.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Maggie Carey 
| producer       = {{Plainlist|
* Jennifer Todd 
* Brian Robbins 
* Sharla Sumpter Bridgett 
* Mark Gordon
* Tom Lassally
* Greg Walter
}}
| writer         = Maggie Carey 
| starring       = {{Plainlist|
* Aubrey Plaza
* Johnny Simmons
* Bill Hader
* Scott Porter
* Alia Shawkat 
* Rachel Bilson
* Christopher Mintz-Plasse
* Andy Samberg
* Donald Glover
* Connie Britton
* Clark Gregg
}}
| music          = Raney Shockne
| cinematography = Doug Emmett
| editing        = Paul Frank
| studio         = {{Plainlist | The Mark Gordon Company
* 3 Arts Entertainment
}}
| distributor    = CBS Films
| released       =  
| runtime        = 104 minutes  
| country        = United States
| language       = English
| budget         = $1.5 million    
| gross          = $3.9 million    
}}
The To Do List is a 2013 American comedy film, released on July 26, 2013.    Written and directed by Maggie Carey in her feature film directorial debut, the film stars Aubrey Plaza, Christopher Mintz-Plasse, and Rachel Bilson. The film is about a recent high school graduate (Plaza), who feels she needs to have more sexual experiences before she starts college. 

==Plot==
Brandy Klark (Aubrey Plaza), from Boise, Idaho, is an overachieving but socially awkward teenager who graduates as the valedictorian of her high school in the summer of 1993. After the ceremony, Brandys two best friends, Wendy (Sarah Steele) and Fiona (Alia Shawkat), take Brandy to a party, where she gets drunk for the first time. At the party, Brandy makes out with a college boy she has a crush on named Rusty Waters. Since the room was dark, he mistook Brandy for someone else and when he realizes who she is, he rejects her. Brandy blames herself for her lack of sexual experiences and resolves to learn all about sex over the summer to prepare for college. She decides that her end-of-summer goal will be to have sex with Rusty to complete her "To Do List".
 turd out of the pool (which Brandy assumes her co-workers are playing a prank on her, based on the Baby Ruth joke from the film Caddyshack, so she takes a bite of it only to find that it really was feces). Embarrassed, she pushes Willy into the pool as revenge and learns that he does not know how to swim, despite managing a swimming pool. She agrees to teach him how to swim in exchange for ending the hazing.

Brandy gets advice from her sister, Amber (Rachel Bilson), and her mother (Connie Britton), as well as her two best friends, while her father (Clark Gregg), a judge and devout conservative, is uncomfortable with the talk of sex, especially since his wife is more sexually experienced than him. Using this information, she makes a "to do list" of sexual acts to learn and perform. As the summer progresses, Brandy has several sexual encounters with Cameron and other boys, all while trying to catch Rustys eye. However, Cameron believes that he and Brandys sexual encounters are a sign of her romantic interest so he begins to fall in love with her. He is crushed after discovering her list and realizing he was just part of her "mission". Meanwhile, Willy catches Brandy, Wendy, Fiona, and adult members of a male grunge band messing around in the pool after-hours. After a scolding, Brandy is sent home where she is confronted by Cameron over the list. Cameron goes away in a huff and Brandy starts to cry. An ex-boyfriend of Wendys comes over to comfort her and the two hook up.

When Wendy and Fiona come over to watch Beaches (film)|Beaches, they discover Brandy’s list and see the list of boys with whom she had experimented. They get upset after finding out that Brandy hooked up with one of their ex-boyfriends and leave angrily. They declare that Brandy has failed to put "hoes before bros," and call her a slut.

Brandy finally gets close to Rusty when they vandalize a rival pool, but they get caught because of Brandy leaving her bra, with her name on it, at the pool. Willy is then forced to fire her. Brandy asks out Rusty, and he takes her to popular make-out spot to have sex. However, the sex is brief and terrible, which disappoints Brandy. She sees her father and mother in the Dodge Caravan next to them having sex causing her to freak out and demand that Rusty take her home immediately.

Meanwhile, Willy goes to the Klark house to stop Brandy from having sex with Rusty, but is met at the door by Amber, who immediately seduces him. When Rusty and Brandy arrive home, a jealous Cameron is there to meet him with a sucker-punch, and they fight until Brandy breaks it up. She compliments each one on their good qualities, then apologizes sincerely to Cameron for using him and offers her own view of sex.

Afterward, she seeks out Wendy and Fiona to apologize to them. She sings "Wind Beneath My Wings" at Wendys door, and the two girls eventually join in, forgiving her. Brandy meets up with Willy at the pool, who offers her his job if she comes back next summer as he has decided to quit and follow the Grateful Dead while Jerry Garcia is still alive.

In the fall, Brandy and Cameron meet again at Georgetown University where they have both matriculated. Brandy apologizes to Cameron once more and they agree to try dating. They have sex in her dorm room and Brandy finally achieves orgasm, the last thing on her list, right as her father walks in on them.

==Cast==
* Aubrey Plaza as Brandy Klark
* Johnny Simmons  as Cameron Mitchell
* Bill Hader  as Willy Mclean
* Scott Porter as Rusty Waters
* Alia Shawkat  as Fiona Forster
* Rachel Bilson  as Amber Klark
* Christopher Mintz-Plasse  as Duffy
* Andy Samberg  as Van King
* Connie Britton  as Jean Klark
* Clark Gregg as Judge George Klark
* Donald Glover as Derrick Murphy
* Sarah Steele as Wendy
* Adam Pally as Chip
* Jack McBrayer as Hillcrest Pool manager

==Production==
The script was originally entitled The Handjob. The film was rejected by studios, but ended up on the "blacklist" of most popular unpublished scripts of 2010. A live table reading of the script at the Austin Film Festival led CBS Films to produce the project.   

==Reception==

===Box office===
During the films opening weekend, The To Do List earned $1,579,402 from 591 theaters, opening in 15th place. This was below expectations, the Los Angeles Times predicted an opening weekend of $2–3 million,    and CBS Films was expecting $2 million.  

===Critical response=== review aggregation website Rotten Tomatoes based on 86 reviews. 

Plazas role as Brandy Klark was praised by film critic Alan Scherstuhl, who writes for The Village Voice, that "unlike for the female characters in previous sex comedies, sex for her is a straight-up choice, something she offers or refuses according to no agenda but her own," but notes that "theres something dispiriting about   junky look, indifferent pacing, and sketch-comedy characterization." 

On a more critical front, Rafer Guzman of Newsday gave the film one out of four stars, calling it "a fake feminist comedy that pays lip service to female empowerment but inadvertently makes sex seem both demeaning and meaningless," as well as "Vulgar, cynical and rarely funny."  Paul Doro of the Milwaukee Journal Sentinel criticized the film for its extreme raunchiness, which he found to be unneeded; he named the film "an absurdly profane comedy that spends its entire time serving up filthy jokes and then trying to top them". 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 