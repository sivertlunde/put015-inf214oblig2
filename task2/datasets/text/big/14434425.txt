Ex-Lady
{{Infobox film
| name           = Ex-Lady
| image          = Ex-Lady film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Florey
| producer       = Darryl F. Zanuck
| writer         = David Boehm
| starring       = Bette Davis Gene Raymond Frank McHugh Monroe Owsley
| music          = Leo F. Forbstein
| cinematography = Tony Gaudio
| editing        = Harold McLernon
| studio         = Vitaphone Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 67 minutes
| country        = United States English
| budget         = $93,000
| gross          = 
}}

Ex-Lady is a 1933 American comedy film directed by Robert Florey. The screenplay by David Boehm is based on an unproduced play by Edith Fitzgerald and Robert Riskin. It is a lightweight and modern version of a drawing room comedy. The film was made before the Motion Picture Production Code was in force, and it is risqué: in subject matter (people having affairs without shame), in staging (double beds) and in the fairly revealing negligees that Bette Daviss character wears.

==Plot== New York immigrant father Adolphe (Alphonse Ethier), whose Old World views spur him to condemn their affair. They form a business partnership, but financial problems at their advertising agency put a strain on the marriage and Don begins seeing Peggy Smith (Kay Strozzi), one of his married customer|clients. Convinced it was marriage that disrupted their relationship, Helen suggests they live apart but remain lovers. When Don discovers Helen is dating his business rival, playboy Nick Malvyn (Monroe Owsley), he returns to Peggy, but in reality his heart belongs to his wife. Agreeing their love will help their marriage survive its problems, the two reconcile and settle into domestic bliss.

The plot is unusual for its time in that Helen is not denigrated for her beliefs about marriage and Don is not depicted as being a cad.    In addition, although they are sleeping together and unmarried, neither is concerned about the possibility of children, and certain dialog could suggest that they are using birth control. 

==Cast==
* Bette Davis as Helen Bauer
* Gene Raymond as Don Peterson
* Kay Strozzi as Peggy Smith
* Monroe Owsley as  Nick Malvyn
* Ferdinand Gottschalk as Herbert Smith
* Alphonse Ethier as Adolphe Bauer
* Frank McHugh as Hugo Van Hugh
* Claire Dodd as Iris Van Hugh

==Production==
The Warner Bros. film was a remake of the Barbara Stanwyck vehicle Illicit (1931 film)|Illicit, released two years earlier. 
 20th Century Fox to become 20th Century Fox.
 What Ever Happened to Baby Jane? includes a scene from Ex-Lady as an example of former child star Jane Hudsons failure to achieve screen success as an adult due to her lack of talent.

==Critical reception==
The New York Times in a contemporary review from 1933, described the film as "an honestly written and truthfully enacted picture of the domestic problems which harass two persons in love with one another". 

In contrast, a more recent review in TV Guide called it a "lame little melodrama notable chiefly for being the first film to have Bette Davis name above the title". 

 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 
 
 