Interview with the Vampire: The Vampire Chronicles
{{Infobox film
| name           = Interview with the Vampire: The Vampire Chronicles
| image          = InterviewwithaVampireMoviePoste.JPG
| image_size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Neil Jordan
| producer       = David Geffen Stephen Woolley
| screenplay     = Anne Rice
| based on       =  
| starring       = Tom Cruise Brad Pitt Antonio Banderas Stephen Rea Christian Slater Kirsten Dunst
| music          = Elliot Goldenthal
| cinematography = Philippe Rousselot
| editing        = Mick Audsley Geffen Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 122 minutes  
| country        = United States
| language       = English
| budget         = $60 million   Retrieved May 30, 2013 
| gross          = $223.7 million 
}} romantic horror Lestat (Cruise) Louis (Pitt), beginning with Louis de Pointe du Lac|Louis transformation into a vampire by Lestat in 1791. The film chronicles their time together, and their turning of a twelve-year-old girl, Claudia (The Vampire Chronicles)|Claudia, into a vampire. The narrative is framed by a present day interview, in which Louis tells his story to a San Francisco reporter. The supporting cast features Christian Slater, Kirsten Dunst, and Antonio Banderas.
 Oscar nominations Best Art Best Original Best Supporting Actress for her role in the film.

==Plot==
In modern-day San Francisco, reporter Daniel Molloy interviews Louis de Pointe du Lac, who claims to be a vampire.
 Spanish Louisiana in 1791, at age 24, by the vampire Lestat de Lioncourt. Louis suffered from a death wish following the deaths of his wife and child; Lestat offered Louis the death he craved, but Louis asks to live instead. Lestat turns Louis and teaches him how to live as a vampire. As part of his education, Lestat informs Louis that some vampires, as an extension of the Dark Gift of vampirism, can develop the capacity to read the thoughts of others. At first, Louis rebels against hurting humans, drinking animal blood instead. He finally succumbs and kills his faithful house slave Yvette. Guilt ridden, he tries to kill himself by setting fire to his house; but Lestat rescues him and they flee.

Wandering the streets of New Orleans, amidst an outbreak of Plague (disease)|plague, Louis finds a sick child in a house with her dead mother. He bites the girl, Claudia (The Vampire Chronicles)|Claudia, whom Lestat later transforms into a vampire "daughter," to discourage Louis from leaving him. Lestat teaches Claudia to live as a vampire and prey on humans. As thirty years pass, Claudia becomes a sadistic killer and closely bonded to Louis and Lestat. But, when she realizes she will never grow up, she is furious with Lestat. She tricks him into drinking the blood of twin boys she killed by overdosing them with laudanum, knowing that blood from a corpse is fatal to vampires. This weakens him, and she slits his throat. Claudia and Louis dump Lestats body in a swamp; but he returns, having drained the blood of swamp creatures to survive. Lestat attacks them, but Louis sets him on fire and is able to flee to Paris with Claudia.
 Santiago and Armand (The Vampire Chronicles)|Armand; Armand invites Louis and Claudia to his coven, the Théâtre des Vampires, where they witness Armand and his coven dispatching a terrified human woman before an unsuspecting human audience. Louis makes inquiries about vampires and their origins, of which Armand is mostly evasive in his answers. Santiago, having read Louis thoughts about Lestat, warns them that the only crime a vampire can commit is killing another vampire.

Claudia rightly accuses Louis of wanting to leave her and join Armand. She demands he turn a human woman, Madeleine, to be her new protector, and he reluctantly complies, feeling he has sacrificed the last of his humanity. As punishment for Lestats murder, the Parisian vampires abduct all three; they imprison Louis in a metal coffin. When freed by Armand the next night, he learns Claudia and Madeleine have been turned to ash by the sun, having been trapped in an airshaft by the coven. He returns to the Theatre and avenges Claudia and Madeleine by burning the vampires in their theatre as they sleep and bisecting Santiago with a scythe. Armand arrives in time to help him escape and once again offers him a place by Armands side. Louis once again refuses, knowing that Armand choreographed Claudias demise to have Louis all to himself, and he leaves Armand for good.
 Gone with the Wind, and Superman (1978 film)|Superman. In 1988, he returns to New Orleans and finds Lestat, a mere shadow of his former self. Lestat asks Louis to rejoin him, but Louis rejects him and leaves.

At this point, Louis concludes the interview, claiming that his experiences have resulted in his becoming the "very spirit of preternatural flesh; detached, unchangeable, empty." Molloy is shocked by this statement and openly declares his desire to have had Louis experiences as a vampire. He asks Louis to transform him. Louis is immediately outraged by Molloys complete disregard for the pervasive suffering caused by vampirism outlined in the interview. Louis bodily lifts Molloy up and pins him against the ceiling. In the next instant, Louis vanishes. Molloy hurriedly runs to his car and drives away, feeling happy with his interview as he plays it through the cassette player. Just then, Lestat appears and its strongly hinted that he strangely seems to know everything that went on between Louis and Daniel. Lestat then attacks him and takes control of the car. Revived by Molloys blood, he shows strong signs of flirting and then offers a dying Molloy "the choice   never had" as they drive off into the San Francisco night, taking out the cassette and turning on the radio, which is playing a Guns N Roses cover of The Rolling Stones "Sympathy for the Devil."

==Cast==
* Tom Cruise as Lestat de Lioncourt
* Brad Pitt as Louis de Pointe du Lac
* Christian Slater as Daniel Molloy Claudia
* Armand
* Santiago
* Domiziana Giordano as Madeleine
* Thandie Newton as Yvette
* George Kelly as Dollmaker
* Sara Stockbridge as Estelle
 A Room with a View, he was rejected and the role was given to Tom Cruise. This was initially criticized by Anne Rice, who said that Cruise was "no more my vampire Lestat than Edward G. Robinson is Rhett Butler" and the casting was "so bizarre; its almost impossible to imagine how its going to work". Nevertheless, she was satisfied with Cruises performance after seeing the completed film, saying that "from the moment he appeared, Tom was Lestat for me" and "that Tom did make Lestat work was something I could not see in a crystal ball."
 Closer to the Truth. 

Originally, River Phoenix was cast for the role of Daniel Molloy (as Anne Rice liked the idea), but he died four weeks before he was due to begin filming. When Christian Slater was cast in his place as Molloy, he donated his entire salary to Phoenixs favorite charitable organizations.  The film has a dedication to Phoenix after the end credits.

==Release==
===Box office===
Interview with the Vampire: The Vampire Chronicles opened on November 11, 1994. Opening weekend grosses amounted to $36.4m, placing it in the number one position at the US box office.  In subsequent weeks it struggled against Star Trek Generations and The Santa Clause. Total gross in the United States was $105 million, while the total including international gross was $224 million, with an estimated budget of $60 million. 

===Critical reception===
 
The film received positive reviews among film critics. Review aggregate website   and the Chicago Sun-Times  Roger Ebert was tempered by poor reviews in The Washington Post and Time magazine.    

===Accolades=== Razzie Award for Worst Screen Couple for Pitt and Cruise, tied with Sylvester Stallone and Sharon Stone in The Specialist.

===Home media===
The film was released on DVD on June 6, 2000    and on Blu-ray Disc on October 7, 2008. 

==Soundtrack==
The soundtrack was written by Elliot Goldenthal and received an Oscar nomination for Best Original Score.

==Sequel== an adaptation for the third book in the series, The Queen of the Damned, was produced and distributed once again by Warner Bros. Tom Cruise and Brad Pitt did not reprise their roles as Lestat and Louis. Many characters and important plotlines were written out of the film, which actually combined elements of The Vampire Lestat with The Queen of the Damned. The film was negatively received by critics, and Rice dismissed it completely as she felt the filmmakers had "mutilated" her work. During pre-production, Rice had pleaded with the studio not to produce a film of the book just yet as she believed her readers wanted a film based on the second book in the series, The Vampire Lestat. Rice was refused the cooperation of the studio. 

In February 2012, the fourth book in The Vampire Chronicles, The Tale of the Body Thief, entered a stage of development with Brian Grazer and Ron Howards film production company, Imagine Entertainment. It was reported that screenwriter Lee Patterson was going to pen the screenplay. However, Rices own son, Christopher, apparently had drafted a screenplay based on the novel that was met with praise from those involved in the developmental stage. Rice later confirmed that creative differences that were beyond those involved resulted in the dismissal of the project in April 2013. 

In August 2014 Universal Pictures acquired the rights to the entire Vampire Chronicles. Alex Kurtzman and Roberto Orci have been named as producers and Christopher Rice, Anne Rices own son, has already written a screenplay based on one of the novels.    

==See also==
*Vampire film

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 