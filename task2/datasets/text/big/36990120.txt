Fantasm Comes Again
 
{{Infobox film
| name           = 
| image          = 
| image size     =
| caption        = 
| director       = Colin Eggleston (as "Eric Ram")
| producer       =Antony I. Ginnane
| writer         = Ross Dimsey (as "Robert Derriere")
| narrator       =
| starring       = 
| music          = John Mol
| cinematography = Vince Monton
| editing        = Tony Patterson
| studio = Australian International Film Corporation
| distributor    = Filmways
| released       = 26 December 1977
| runtime        = 94 mins
| country        = Australia English
| budget         = $85,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 322  Beilby, Peter and Scott Murray, ‘Antony I. Ginnane’, Cinema Papers, January/February 1979 p176-177 
| gross = 
| preceded by    =
| followed by    =
}}
Fantasm Comes Again is a 1977 soft core pornographic film from Australia. It was a sequel to Fantasm (1976) and was the first feature directed by Colin Eggleston.

==Plot==
Journalist Libbie is taking over the "Dear Collette" sex advice column at her newspaper from veteran reporter Harry, who is retiring. Over the course of one night, Harry talks Libbie through a series of letters from their readers. They include:
*rape in a drive in theatre
*teacher-student sex in a gym
*lesbian seduction in a barn
*a suburban orgy
*incest
*sex in a pool
*sex in a library
*oral sex on the road
*a threesome in an elevator
*sex between monks and a nun.

==Cast==
*Angela Menzies-Wills as Libbie
*Clive Hearne as Harry
*Uschi Digart as Leslie
*Rick Cassidy as Mr Bates
*Liz Wolfe as Rita
*Rosemarie Bern as Cindy
*Urias S. Cambridge as Bob
*Rainbeaux Smith as Carol
*Peter Kurzon as Ted
*Lois Owens as Alice
*Mike Stapp as rapist
*Michael Barton as Miss Peabody
*Suzy A. Star as Penny
*Dee Dee Levitt as Bianca

==Production==
The linking scenes between the two journalists were shot in Australia, and the sex scenes were filmed over 12 days in Los Angeles by an Australian director and cinematographer, using American porn stars.  

The budget was larger on the sequel in an attempt to attract a bigger audience; in contrast with the original, where only one of the ten stories was lip sync and the rest in voice over, all the stories in Fantasm Comes Again had lip sync dialogue. Ginnane later thought this was a mistake as it distracted from what was on screen.

==Reception==
The film sold well around the world but was not as popular as Fantasm at the Australian box office.  Ginnane blamed the fact by the time it was released there was a glut of sex films on the market and the delay caused by censorship hold ups. (In 1980 David Stratton called it "the most censored of the new Australian films." David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p250 ) By 1979 the film had not yet broken even but Ginnane was confident that would be the case. 

Ginnane later said that he felt Colin Eggleston was just as competent a director as Richard Franklin but thought his sense of humour was different "and perhaps it didnt suit the material as well". 

==References==
 
==External links==
*  at IMDB
*  at Oz Movies
 
 
 
 
 
 
 
 