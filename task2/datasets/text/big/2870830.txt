Brother's Keeper (1992 film)
{{Infobox film
| name           = Brothers Keeper
| director       = Joe Berlinger and Bruce Sinofsky
| starring       = Delbert Ward Lyman Ward Roscoe Ward Bill Ward
| released       =  
| runtime        = 104 min.
| music          = Molly Mason, Jay Ungar
| language       = English
| genre          = Documentary
| studio         = Wellspring Media Inc.
| rating         = Not Rated
| gross          =  $1,305,915 
}} documentary directed Maysles brothers, who had formerly employed Berlinger and Sinofsky. 

The film contrasts two groups of society; people from rural areas and those from larger cities. It also exhibits how the media flocked to the town to cover the story.

This movie displays two completely opposite views of the Ward brothers. One opinion is that of the locals, who defend the Ward brothers as simple country folk. The other is that of the press, who stereotype the brothers as uneducated hicks.

The Ward sister was not featured in this film because of her death in the 1980s. However, her daughter Pat makes an appearance in the DVDs special features.

==The Ward brothers==
In a rural farming community near Syracuse, New York, four brothers lived in a dilapidated house.  William Jay (1925-1990), Adelbert (known as Delbert), Lyman, and Roscoe Ward were barely literate, had no formal education, and farmed land that had been in their family for generations.

==Williams death==
William Ward, who had been ill for years, was found dead one morning.  Delbert was accused of killing him, perhaps by smothering. The prosecutions theory at trial was that Delbert had performed a mercy killing in order to put William out of his misery after a period of severe headaches and declining health. As the film progresses, it is revealed that during the coroners examination of Williams body, semen was found on clothing and on Williams leg, leading to the suggestion that Delbert had killed William in an episode of "sex gone bad." The film never follows up on this media sensation.

Delbert Ward was acquitted at trial, largely because the lack of any physical evidence and that the New York State Police violated Delberts rights by coercing a confession (which he later retracted) and by having him sign a written statement which he could not understand due to illiteracy.

==The brothers fates==
Delbert Ward died at age 66 at Bassett Hospital in Cooperstown on August 6, 1998. 

Roscoe Ward died at age 88 on June 23, 2007.  

Lyman Ward died at age 85 in Utica, New York on 15 August 2007.  

==Film title==
The title Brothers Keeper comes from the Book of Genesis in the Bible. When God asks Cain where his brother Abel is, Cain replies, "I know not; am I my brothers keeper?" (Genesis 4:9 KJV)

==Awards==
*Directors Guild of America, USA: DGA Award for Outstanding Directorial Achievement in Documentary/Actuality
*Kansas City Film Critics Circle Awards: KCFCC Award for Best Documentary
*National Board of Review, USA: NBR Award for Best Documentary
*New York Film Critics Circle Awards: NYFCC Award for Best Documentary
*Sundance Film Festival: Audience Award for Documentary

==Nominations==
*Sundance Film Festival: Grand Jury Prize for Documentary

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 