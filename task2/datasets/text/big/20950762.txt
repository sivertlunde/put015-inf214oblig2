Antoine and Antoinette
{{Infobox film
| name           = Antoine and Antoinette
| image          =
| caption        =
| director       = Jacques Becker
| producer       =
| writer         = Jacques Becker Maurice Griffe Françoise Giroud
| starring       = Roger Pigaut
| music          =
| cinematography = Pierre Montazel
| editing        = Marguerite Renoir
| distributor    =
| released       =  
| runtime        = 78 minutes
| country        = France
| language       = French
| budget         =
}}
Antoine and Antoinette ( ) is a 1947 French comedy film directed by Jacques Becker. It was entered into the 1947 Cannes Film Festival.   

==Cast==
* Roger Pigaut as Antoine Moulin
* Claire Mafféi as Antoinette Moulin
* Noël Roquevert as Mr. Roland
* Gaston Modot as civil servant
* Made Siamé as the shopkeepers wife
* Pierre Trabaud as Riton
* Jacques Meyran as M. Barbelot
* François Joux as bridegroom
* Gérard Oury as customer
* Charles Camus as the shopkeeper
* Émile Drain as father-in-law
* Annette Poivre as Juliette

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 