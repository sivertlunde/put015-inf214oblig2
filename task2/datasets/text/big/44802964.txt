Familien Gyldenkål
{{Infobox film
| name = Familien Gyldenkål
| image = Familien Gyldenkål (The Goldcabbage Family) 1975 poster.jpg
| caption = Poster by Aage Lundvald
| director = Gabriel Axel
| writer = Poul-Henrik Trampe
| starring = Axel Strøbye, Kirsten Walther
| music = Søren Christensen
| cinematography = Henning Christiansen
| editing =
| producer = Just Betzer   
| distributor =
| released =  
| runtime = 92   
| awards = 
| country = Denmark
| language = Danish
| budget =
}}
Familien Gyldenkål (English: The Goldcabbage Family) is a 1975 Danish comedy film directed by Gabriel Axel.   
 Bent Christensen.   

==Cast==
*Axel Strøbye
*Kirsten Walther
*Birgitte Bruun
*Martin Miehe-Renard
*Karen Lykkehus
*Bertel Lauring
*Ove Sprogøe
*Lily Broberg
*Jens Okking
*Karl Stegger
*Brigitte Kolerus
*Helle Merete Sørensen
*Bjørn Puggaard-Müller
*Otto Brandenburg
*Lisbet Dahl
*Claus Ryskjær
*Hans Christian Ægidius
*Tommy Kenter
*Hardy Rafn
*Benny Hansen
*Ebba With
*Jens Brenaa Poul Thomsen
*Gyda Hansen Ernst Meyer
*Søren Rode
*Karl Gustav Ahlefeldt

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 