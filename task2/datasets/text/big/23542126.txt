Sukeban Deka the Movie 2: Counter-Attack from the Kazama Sisters
{{Infobox film
| name = Sukeban Deka the Movie 2: Counter-Attack  from the Kazama Sisters
| image = Sdekatwo.jpg
| caption = Theatrical poster Hideo Tanaka
| producer = Masaharu Nakasone Osamu Tezuka Asao Tsunoda
| writer =
| starring = Yui Asaka Yuka Onishi Yuma Nakamura Kosuke Toyoharu Masaki Kyomoto
| music =
| cinematography =
| editing =
| distributor = Toei Company Magnolia Home Entertainment
| released =  
| runtime =
| country = Japan
| language = Japanese
| budget =
}}

Sukeban Deka the Movie 2: Counter-Attack from the Kazama Sisters (スケバン刑事 風間三姉妹の逆襲) is a live action Japanese film that was released in 1988. The movie closely follows the manga series Sukeban Deka which was written and illustrated by Shinji Wada. The movie also stars the only returning actresses, Yui Asaka, who was the same actress for the TV series. Being a sequel to Sukeban Deka The Movie, the movie is followed by a third movie, entitled Yo-Yo Girl Cop in the year 2006.

==Synopsis== juvenile crime bureau and refuses to undertake undercover missions for them. It is soon discovered that Sekin’s actual goal is to overthrow the Japanese government, and the Kazama sisters have to stop him.

==Cast==
* Yui Asaka
* Yuka Onishi
* Yuma Nakamura
* Kosuke Toyoharu
* Masaki Kyomoto

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 

 