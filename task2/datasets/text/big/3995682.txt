Chinese Coffee
{{Infobox Film name            = Chinese Coffee image           = Chinese Coffee film.jpg caption         = director        = Al Pacino producer        = James Bulleit Anne DAmato Michael Hadge Larry Meistrich John Mollura Robert Salerno writer          = Ira Lewis starring        = Al Pacino Jerry Orbach Susan Floyd Ellen McElduff music           = Elmer Bernstein cinematography  = Frank Prinzi editing         = Michael Berenbaum Pasquale Buba Noah Herzog distributor     = Chal Productions released        =	 runtime         = 99 minutes country         = United States language        = English Released        =
|}}

Chinese Coffee is a one-act play, written by Ira Lewis.  

Chinese Coffee premiered at the Circle in the Square Theatre on Broadway in 1992.    Actor Al Pacino was cast as the plays struggling writer, Harry Levine. 

The play was later adapted into a 2000 independent film, starring Al Pacino and Jerry Orbach. It was released in New York as part of the Tribeca Film Festival. The film, which was also written by Lewis and directed by Pacino, was introduced by Robert De Niro during the open ceremony. 

==Film==
Al Pacino directed the 2000 film adaptation of Chinese Coffee, in which he also starred opposite Jerry Orbach.  Ira Lewis, who wrote the original play, also penned the screenplay for the film. 

The film adaptation was released in New York as part of the Tribeca Film Festival. Shot almost exclusively as a one-on-one conversation between the two main characters, it chronicles friendship, love, loss, and humor of daily life. After years of withholding it, Pacino allowed it to be released on June 19, 2007 as a part of a three-movie boxed set called Pacino: An Actors Vision.

Howard Shore reportedly originally composed the score to the film, before Elmer Bernstein was hired to replace him. 

==Plot==
Harry Levine (Pacino) is a struggling writer (barely) ekeing out a living as a doorman &mdash; that is, until he is fired. Desperate for money, he pays a visit to his friend Jake Manheim (Orbach), an arts photographer, to collect an old debt. After Jake says he doesnt have the money, the two engage in an all-night conversation about their respective art, past and present loves, and the directions their lives are heading. The play and film are set in Greenwich Village circa 1982.

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 


 