Border Vigilantes
{{Infobox film
| name           = Border Vigilantes
| image          = Border Vigilantes poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Derwin Abrahams 	
| producer       = Harry Sherman 
| screenplay     = J. Benton Cheney  William Boyd Russell Hayden Andy Clyde Frances Gifford Victor Jory Ethel Wales Morris Ankrum
| music          = John Leipold
| cinematography = Russell Harlan
| editing        = Robert B. Warwick Jr. 
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Russell Hayden, Andy Clyde, Frances Gifford, Victor Jory, Ethel Wales and Morris Ankrum. The film was released on April 18, 1941, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*Russell Hayden as Lucky Jenkins
*Andy Clyde as California Carlson
*Frances Gifford as Helen Forbes
*Victor Jory as Henry Logan
*Ethel Wales as Aunt Jennifer Forbes
*Morris Ankrum as Dan Forbes
*Tom Tyler as Henchman Yager
*Hal Taliaferro as Henchman Big Ed Stone 
*Jack Rockwell as Hank Weaver
*Britt Wood as Lafe Willis 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 