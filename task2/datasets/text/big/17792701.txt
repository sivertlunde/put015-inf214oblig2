Day of Atonement (film)
Day of Atonement (original French title:Le Grand Pardon II) is a 1992 127-minute longer sequel to film Le Grand pardon, film directed by  , Florida, USA and France.

==Plot==
Raymond Bettoun is released after 10 years in prison to come to his family in Miami, FL for his grandson Alexanders bar-mitzvah. His son, Maurice is a banker. Raymond soon finds out Maurice in laundering drug money. 
Raymonds nephew Roland lives in an island with his assistant Joseph. He is pulling the strings, stealing from Maurices bank. Rolands brother Eli is planning to go away with Roland after he testifys again Maurice later on in the film.

Maurice comes into contact with drug lord Pasco Meisner who wants to be part of the same mob family as him. But Maurices plan goes horribly wrong when members of his real family & drug family are all being killed off or kidnapped by Pasco & his assistant Verdugo, all when Roland too is trying to get Maurice killed to inherit the money from Maurices bank.

==Cast==
*Roger Hanin.........Raymond Bettoun Richard Berry.......Maurice Bettoun
*Jennifer Beals......Joyce
*Christopher Walken..Pasco Meisner
*Jill Clayburgh......Sally
*Gerard Darmon.......Roland Bettoun
*Alexandre Aja.......Alexander Bettoun
*Jean-Francois Stevenin.....Eric Lemmonier
*Jean-Claude de Goros.....Sammy
*Jim Moody.....Danny Williams
*Christian Fellat.....El Verdugo

==Soundtrack==
The soundtrack to the film was recorded by Italian composer Romano Musumarra. The CD soundtrack was released in France for a limited time.

===Track listing===

1. Le Grand Pardon
2. Little Havana
3. Yir Yir Ibon
4. Viviane
5. Alton Road
6. Khemdati
7. Saruba
8. Flash Back
9. Dope Rever
10. Moon Ray
11. Ya Oummie Ya Oummi
12. Pasaumes

==External links==
*  

 
 
 
 
 
 
 
 
 


 