Inside the Lines
{{Infobox film
| name           = Inside the Lines
| image          = InsideTheLinesDVDCover.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover for home release of film
| director       = Roy Pomeroy   
| producer       = William LeBaron Roy Pomeroy 
| screenplay     =John Farrow Ewart Adamson 
| based on       =   
| narrator       = 
| starring       = Betty Compson Ralph Forbes Mischa Auer 
| music          = Roy Webb 
| cinematography = Nicholas Musuraca   
| editing        = George Marsh Ann McKnight  RKO Radio Pictures   
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 72 minutes 
| country        = United States
| language       = English
| budget         = 
}}
Inside the Lines is a 1930 Pre-Code Hollywood|Pre-code talking film spy drama starring Betty Compson, Ralph Forbes and Mischa Auer. Directed by Roy Pomeroy (who also was the associate producer) from a screenplay by John Farrow and Ewart Adamson, which in turn was based on the 1915 Broadway play of the same name by Earl Derr Biggers.  This version is a remake of the 1918 silent version, also with the same name.  This film exists in the public domain after failure to renew the copyright after 27 years.

==Plot==
Jane Gershon is engaged to Eric Woodhouse, living in Germany prior to the onset of World War I.  When the war breaks out, they are forced to separate, but are reunited months later in Gibraltar, at the British fortress there.  Both are supposedly German spies with orders to destroy the British fleet, anchored in the harbor.

Not fully trusting either of them, the German government has sent another agent, the Hindu Amahdi, to ensure that their sabotage plans are carried out.  Both Jane and Eric believe the sincerity of the other as a German agent.  When it appears that Janes attempt to destroy the fleet is uncovered, to save her, Eric takes the blame and seemingly commits suicide.  However, when Ahmadi uncovers the truth that Jane is really a double agent for the British government, he attempts to go through with the sabotage.  When he is about to kill Jane due to her treachery, Eric reappears and kills him instead, saving Janes life.  When Jane discovers that Eric is also a British double agent, they are happily reunited.

==Cast==
*Betty Compson - Jane Gershon
*Ralph Forbes - Eric Woodhouse
*Montagu Love - Governor of Gibraltar
*Mischa Auer - Amahdi
*Ivan F. Simpson - Capper (*billed Ivan Simpson)
*Betty Carter - Lady Crandall Evan Thomas - Major Bishop
*Reginald Sharland - Archie
*Wilhelm von Brincken - chief, Secret Service

(cast list is per AFI database) 

==Notes==
The play of the same name, on which this screenplay was based, was produced in 1915 at the Longacre Theatre. 

This film is a remake of the 1918 silent version, also titled Within the Lines, which was directed by David H. Hartford, and starred Lewis Stone and Marguerite Clayton, based on a screenplay by Monte M. Katterjohn.  The silent version was produced by Delcah Photoplays, Inc. and Pyramid Film Corporation, and distributed by the World Film Company.   
 public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

==See also==
*List of films in the public domain in the United States

==References==
 

==External links==
*  
*  at TCMDB
*  at IMDB
*  at Allmovie
*  available for free download at  

 
 
 
 
 
 
 