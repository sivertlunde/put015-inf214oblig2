Der var engang en vicevært
{{Infobox film
| name = Der var engang en vicevært
| image = Dervarengangenvicevært.jpg
| image_size =
| caption = Movie poster
| director = Lau Lauritzen Jr. Alice OFredericks
| producer =
| writer = Lau Lauritzen Jr. Børge Müller
| narrator =
| starring = Osvald Helmuth
| music = Willy Kierulff Karl Andersson
| editing = Marie Ejlersen
| distributor = ASA Film
| released = 26 December 1937
| runtime = 100 minutes
| country = Denmark language = Danish
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 1937 Denmark|Danish family film directed by Lau Lauritzen Jr. and Alice OFredericks and written by Børge Müller. The film stars Osvald Helmuth and Connie Meiling .

==Cast==
* Osvald Helmuth ... Vicevært Christensen
* Connie Meiling ... Connie
* Victor Borge ... Komponist Bøegh ... credited as Victor Borge|Børge Rosenbaum
* Sigurd Langberg ... Direktør Haller
* Asta Hansen ... Asta Haller
* Poul Reichhardt ... Paul
* Inger Stender ... Den lyse Inger
* Lulu Ziegler ... Sangerinden Lulu
* Sigfred Pedersen ... Digteren Sigfred
* Thorkil Lauritzen... Onkel Aksel
* Erika Voigt ... Tante Eulalia
* Bjørn Spiro ... Sagfører Birck Lau Lauritzen
* Tove Wallenstrøm ... Girl being picked up in the road

==External links==
*  
*  

 
 

 
 
 
 
 
 
 


 
 