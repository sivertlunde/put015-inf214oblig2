The Happy Hooker Goes to Washington
{{Infobox film
| name           = The Happy Hooker Goes to Washington
| image          = Happy Hooker Washington.jpg
| image_size     = 
| alt            = 
| caption        = Movie Poster
| director       = William A. Levey
| producer       = William A. Levey
| writer         = Bob Kaufman
| story          = Barney Cohen George Hamilton
| music          = Don Elliott
| cinematography = Robert Caramico
| editing        = Larry Marinelli
| studio         = 
| distributor    = Cannon Films
| released       =    
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 The Happy Hooker, which was released in 1975.  Joey Heatherton replaced Lynn Redgrave as the lead character of Xaviera Hollander.

The films tagline was, "She served her country... the only way she knew how!"

Joe E. Ross, Larry Storch and Rip Taylor are among the celebrities who make cameos.

==Plot== Hollander is called to testify in front of the United States Congress.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Xaviera Hollander
|- George Hamilton || Ward Thompson
|-
| Ray Walston|| Sen. Sturges
|- Jack Carter || Sen. Caruso
|-
| Phil Foster || Sen. Krause
|- David White || Sen. Rawlings
|-
| Louisa Moritz || Natalie Nussbaum
|-
| Billy Barty || Little Man
|-
| Harold Sakata || Wong
|-
| Edy Williams || Professor Simmons
|-
| Larry Storch || Robby Boggs
|-
| Will Hutchins || Randall Petersdorf
|-
| Cisse Cameron || Miss Goodbody
|}

==Additional information==
This film was also released under the following titles:
*En Washington los senadores están calientes - Spain
*Happy Hooker Vai a Washington - Brazil
*Xaviera Washingtonissa - Finland

==External links==
*  
*  
*List of American films of 1977

 
 
 
 
 
 
 
 


 