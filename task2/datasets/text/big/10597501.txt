Ming Ming
 
 
 
{{Infobox film
| name           = Ming Ming
| image          = Ming Ming poster.jpg
| caption        = Promotional poster for Ming Ming
| director       = Susie Au
| producer       = Philip Lee Barbie Tung Louisa Wei Jeff Chang
| music          = Veronica Lee Anthony Wong Yiu Ming
| cinematography = Fisher Yu Cheung Ka-Fai
| distributor    = Polybona Films
| released       =  
| runtime        = 105 minutes
| country        = Hong Kong
| language       = Cantonese, Standard Chinese|Mandarin, Shanghainese
| budget         =
}} 2006 Hong Jeff Chang. A debut feature-length film by Au, an experienced music video director, Ming Ming premiered on 14 October 2006 during the Pusan Film Festival and was released in cinemas in Hong Kong and Mainland China on 26 April 2007. 

==Cast==
* Zhou Xun as Ming Ming / Nana (double role)
* Daniel Wu as D
* Tony Yang as Tu Jeff Chang as Brother Cat
* Kristy Yang

==Plot==

Fiery Ming Ming (Zhou Xun) has always been the kind to take responsibility for her actions. When she meets D (Daniel Wu) at a boxing ring, the two soon become lovers.

D tells Ming Ming he would go to Harbin if he had $5 million. Taking him at his word, Ming Ming goes to Brother Cat and asks him for the money. When he demurs, she steals it, along with a secret box; she manages to fight off the other gang members by incredible prowess with black flying beads, which projected at enough speed, can be deadly.

Brother Cat is furious she has taken the box; and send his associates to find her.

 As she is running away, Ming Ming bumps into an acquaintance, Tu, and passes him the money and tells him to run. Tus special skill is to run very fast. She also bumps into Nana (Zhou Xun in a double role) who coincidentally is also in love with D, while escaping.

 Mistaking Nana for Ming Ming, Tu grabs her hand and the two of them escape to Shanghai in search for D. Nana knows Tu has got the wrong person, but the lure of the $50 million is too strong. Meanwhile, Ming Ming keeps herself hidden with the box, using her superb fighting skills to protect the two from a distance.

 Failing to find D, all Ming Ming and Nana have is a secretive voicemail message left by him.

 Finally, they realise that the secret that D is looking for is also connected to the box they hold. Just what is this secret...

== Release ==
Ming Ming premiered during the  , 28 March 2007. Retrieved on 11 April 2007.  According to distributor Polybona Films, the film grossed over Renminbi|¥10 million (approximately US dollar|US$1.3 million) in total box office from Hong Kong and Mainland China within five days of release. 

==Critical reception==
Ming Ming opened to mixed to negative reviews. Perry Lam of Muse (Hong Kong magazine)|Muse magazine writes, There is little doubt that Susie Au has an enormous gift for creating visual images, but somebody should have told her that a movie director is someone who tells a story with images and dialogue. 

== Notes ==
 

== External links ==
*  
*  
* Lovehkfilm.com Review  

 
 
 
 