North Dallas Forty
{{Infobox film name = North Dallas Forty image = North-dallas-forty-poster-1.jpg
|caption=Promotional poster for North Dallas Forty writer = Peter Gent Ted Kotcheff Frank Yablans Nancy Dowd (uncredited) starring = Steve Forrest G. D. Spradlin Dabney Coleman Savannah Smith Boucher director = Ted Kotcheff producer = Frank Yablans distributor = Paramount Pictures released =   runtime = 119 min. budget =  gross = $26,079,312  country = United States language = English
}}

North Dallas Forty is a 1979 film drama starring Nick Nolte, Mac Davis, and G. D. Spradlin set in the world of American professional football. It was directed by Ted Kotcheff and based on the best-selling novel by Peter Gent. The screenplay was by Kotcheff, Gent, Frank Yablans and Nancy Dowd (uncredited). This was the first film role for Davis, a popular recording artist.

==Plot== professional football team based in Dallas, Texas named the North Dallas Bulls,   which closely resembles the Dallas Cowboys.

Though considered to possess "the best hands in the game", the aging Elliott is struggling to stay competitive and relies heavily on painkillers. Elliott and popular quarterback Seth Maxwell (Davis) are outstanding players, but they also characterize the recreational drug use|drug-, sex-, and alcohol-fueled party atmosphere of NFL teams of that era. Elliott wants only to play the game, retire, and own a home with his girlfriend Charlotte (Dayle Haddon), who appears to be financially independent, and has no interest whatsoever in football. 
 coach (Spradlin) who turns a blind eye to anything that his players may be doing off the field or anything that his assistant coaches and trainers condone to keep those players in the game. The Coach is focused on player "tendencies", a quantitative measurement of their performance, and seems less concerned about the human aspect of the game and the players. As one player (John Matuszak) finally erupts to a coach (Charles Durning): "Every time I call it a game, you call it a business. And every time I call it a business, you call it a game."
 Steve Forrest). When they also drag Charlottes name into it, Elliott, convinced that the entire investigation is merely a pretext to force him off the team, quits the game of football for good.

==Behind the Scenes==

Part drama, comedy, and satire, North Dallas Forty is widely considered a classic sports film, giving insights into the lives of professional athletes.  

Based on the semi-autobiographical novel by Peter Gent, a Cowboys wide receiver in the late 1960s, the films characters closely resemble real-life team members of that era, with Seth Maxwell often compared to quarterback Don Meredith, B.A. Strother to Tom Landry, and Elliott to Gent. Of the story, Meredith said, "If Id known Gent was as good as he says he was, I would have thrown to him more." 

==Cast==
* Nick Nolte as Phil Elliott
* Mac Davis as Seth Maxwell
* G.D. Spradlin as B.A. Strother
* Dayle Haddon as Charlotte Caulder
* Bo Svenson as Joe Bob Priddy
* John Matuszak as O.W. Shaddock Steve Forrest as Conrad Hunter
* Dabney Coleman as Emmett Hunter
* Charles Durning as Coach Johnson
* Marshall Colt as Art Hartman
* Savannah Smith Boucher as Joanne Rodney

==Reviews== Fun with The Apprenticeship of Duddy Kravitz.
In her review for The New York Times, Janet Maslin wrote, "The central friendship in the movie, beautifully delineated, is the one between Mr. Nolte and Mac Davis, who expertly plays the teams quarterback, a man whose calculating nature and complacency make him all the more likable, somehow."  Time (magazine)|Time magazines Richard Schickel wrote, "North Dallas Forty retains enough of the original novels authenticity to deliver strong, if brutish, entertainment".  Newsweek magazines David Ansen wrote, "The writers -- Kotcheff, Gent and producer Frank Yablans -- are nonetheless to be congratulated for allowing their story to live through its characters, abjuring Rocky-like fantasy configurations for the harder realities of the game. North Dallas Forty isnt subtle or finely tuned, but like a crunching downfield tackle, it leaves its mark." 

However, in his review for the Globe and Mail, Rick Groen wrote, "North Dallas Forty descends into farce and into the lone man versus the corrupt system mentality deprives it of real resonance. Its still not the honest portrait of professional athletics that sport buffs have been waiting for."  Sports Illustrated magazines Frank Deford wrote, "If North Dallas Forty is reasonably accurate, the pro game is a gruesome human abattoir, worse even than previously imagined. Much of the strength of this impression can be attributed to Nick Nolte ... Unfortunately, Noltes character, Phil Elliott, is often fuzzily drawn, which makes the actors accomplishment all the more impressive."  In his review for the Washington Post, Gary Arnold wrote, "Charlotte, who seemed a creature of rhetorical fancy in the novel, still remains a trifle remote and unassimilated. Dayle Haddon may also be a little too prim and standoffish to achieve a satisfying romantic chemistry with Nolte: Somehow, the temperaments dont mesh." 

==Differences from the Novel== most popular sport in the United States as the metaphorical central focus. Recurring scenes of television and radio news reporting violent crimes, war and environmental destruction are scattered throughout various scenes, but left out in the same scenes recreated in the movie. Throughout the novel there is more graphic sex and violence, as well as drug and alcohol abuse without the comic overtones of the film, for instance the harassment of an unwilling girl at a party played for laughs in the movie is a brutal near-rape at an orgy in the novel.

At the end of the novel there is a shocking twist ending in which Phil returns to Charlotte to tell her hes left football and presumably to continue his relationship with her on her ranch, to find she and a black friend (who is not in the movie) have been regular lovers, unbeknownst to Phil, and that they have been violently murdered by Charlottes ex-husband (also left out of the movie), who has been stalking her throughout the novel.  

==References==
 

==See Also==
Any Given Sunday

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 