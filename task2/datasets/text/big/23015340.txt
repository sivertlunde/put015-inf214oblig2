The Poet (film)
{{Infobox film
| name = The Poet
| image = The Poet VideoCover.jpeg
| caption = 
| director = Damian Lee
| producer = Lowell Conn Damian Lee
| writer = Jack Crystal
| starring = Nina Dobrev Colm Feore Roy Scheider Kim Coates Daryl Hannah
| music = Zion Lee Steve Raiman
| cinematography = David Pelletier
| editing = Joseph Weadick Noble House Film & Television
| distributor = Image Entertainment (USA) American World Pictures (AWP) (non-USA)
| released =  
| runtime = 100 minutes
| country = Canada
| language = English
| budget =
}} CAD $11 million. {{cite web 
| url = http://www.imdb.com/title/tt0904127/business
| title = Box office / business for The Poet (2007) 
| accessdate = 2009-07-06
| publisher = Internet Movie Database}} 
It was released in the United States as Hearts of War. {{cite web 
| url = http://www.imdb.com/title/tt0904127/releaseinfo
| title = Release dates for The Poet (2007)
| accessdate = 2009-07-06
| publisher = Internet Movie Database}} 

== Synopsis ==
At the dawn of World War II, a rabbis daughter and a disenchanted German soldier fall in love and are separated by the war. They struggle on a perilous journey to find one another.

Rachel, a young Jewish woman (Nina Dobrev) headed home runs into a snow storm and falls unconscious only to be rescued by Oscar Koenig (Jonathan Scarfe), an undercover German officer stationed in Poland. Over the next few days, Oscar nurses Rachel back to health and in the process the two fall in love, bonding over the poetry that Oscar writes. Meanwhile, German soldiers infiltrate Poland and destroy Rachels village, killing Rachels family. Oscar helps Rachel and Rachels fiancé, Bernard (Zachary Bennett), escape into the woods, but he refuses to accompany them, despite Rachels pleas. Oscar goes back to his daily routine, scouting for his father, General Koenig (Kim Coates), whom Oscar has a rough relationship because of their differing opinions on the war. Oscar seeks comfort in his memories of Rachel, and in his mother (Daryl Hannah), who shares his disenchantment with the war, and encourages him to search for his lost love.

Bernard and Rachel escape into the mountains, when an unknown illness overtakes Rachel. They find a farm that is hiding other Jews, and discover that Rachel is pregnant with Oscars child. Regardless of the situation that hes been placed in, Bernard marries Rachel anyway. On the day of their wedding, hidden in the basement of the farmhouse, German soldiers attack and kill everyone inside but Bernard and Rachel somehow escape and head once more into the woods.

Soon Oscar is sent to the Russian border that is occupied by the Germans, along the road Oscar, Bernard and Rachel unknowingly cross paths, and Rachel delivers the baby in the woods. Rachel and Bernard reach the German camp at the border before Oscar, and pick up jobs, Bernard helps clean up the camp and Rachel becomes a singer/prostitute, trying to survive the war. Meanwhile, Oscar has been fighting the Russian partisans, trying to get to the camp. When he arrives in the camp and reunites with Rachel and she tells him about their son, and Oscar begs her to leave with him and they can start and make a new life with their son, Rachel leaves him to retrieve their son. In the meantime Bernard, playing chess in their tent with a German soldier when he is called out to clean and another German soldier takes his place in the game but he loses because he cannot concentrate with the baby crying. In frustration at losing, he lashes out at the baby, killing him, then flees. Rachel return to the tent a few moments after Bernard and they find the baby dead, they attack and kill the German soldier and flee the camp without telling Oscar. 
They run into the Russian partisans that Oscar had fought earlier, with Oscar in pursuit. Oscar captures one of the Russian partisans and Rachel and Bernard are recruited to kill Oscar. Seeing that its Oscar, Bernard tells Rachel to run to him despite what the Russians do, and Bernard attacks them while Rachel runs to Oscar. Bernard dies in the process and Oscar kills the others. Rachel and Oscar are reunited once more, and Oscar promises to take care of Rachel as Bernard dies. The film ends with Rachel and Oscar grieving over Bernards body.

== Cast ==
*Jonathan Scarfe as Oscar Koenig
*Nina Dobrev as Rachel
*Zachary Bennett as Bernard
*Kim Coates as General Koenig
*Colm Feore as Colonel Hass
*Roy Scheider as Rabbi
*Daryl Hannah as Marlene Koenig
*Miriam McDonald as Willa

== Reception ==
The Poet received generally poor reviews. Jay Seaver of eFilmCritic said "Youre not supposed to laugh at movies like "The Poet"   Once the audience is snickering, youve failed.   The story   is supposed to be grand and tragic". {{cite web 
| url = http://efilmcritic.com/review.php?movie=16647
| title = Poet, The
| date = 2007-07-28
| accessdate = 2009-07-06
| work = Efilmcritic.com
| last = Seaver
| first = Jay}}   Similarly, Steve Power of DVD Verdict concluded "There are much better films out there that cover similar ground, and Hearts of War is best avoided." {{cite web
| url = http://www.dvdverdict.com/reviews/heartsofwar.php
| title = DVD Verdict Review — Hearts of War
| date = 2009-05-07
| accessdate = 2009-07-06
| work = DVDVerdict.com
| last = Power
| first = Steve}} 

==References==
 

==External links==
*  

 

 
 
 
 
 
 