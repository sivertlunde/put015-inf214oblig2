Do Jasoos
{{Infobox Film
| name           = Do Jasoos
| image          = Do Jasoosfilm.jpg
| image_size     = 
| caption        = Vinyl Record Cover
| director       =Naresh Kumar
| producer       =Naresh Kumar
| writer         = 
| narrator       = 
| starring       = Raj Kapoor Rajendra Kumar music          = Ravindra Jain
| cinematography = 
| editing        = Anand Apte
| distributor    = 
| released       = 1975
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

Do Jasoos is a 1975 Hindi film. Produced and directed by Naresh Kumar, the film stars Raj Kapoor, Rajendra Kumar, Aruna Irani, Prem Chopra, Farida Jalal and Asit Sen. The music is by Ravindra Jain.

== Plot ==

Dharamchand and Karamchand, (Raj Kapoor and Rajendra Kumar) are two detectives who are on the trail of a millionaires missing daughter(Bhavana Bhatt). In their efforts to save the girl, who is an eye-witness to a murder, they fall headlong into a series of comic situations because 
the photograph they have is that of the friend of Bhavana.Eventually they also succeed in busting a smugglers gang. A rollicking comedy, starring Raj Kapoor and Rajendra Kumar whore being seen together in very different roles, after an intense love story like Sangam. The film is directed by Naresh Kumar and has music by Ravindra Jain. The supporting cast includes Shailendra Singh and Prem Chopra.

== External links ==
*  

 
 
 

 