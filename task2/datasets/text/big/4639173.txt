Cabaret Balkan
{{Infobox film
| name           = Cabaret Balkan
| image          = Cabaret Balkan.jpg
| image size     = 
| alt            = 
| caption        = International poster
| director       = Goran Paskaljević
| producer       = Goran Paskaljević Antoine de Clermont-Tonnerre
| writer         = Dejan Dukovski (play) Goran Paskaljević Filip David Zoran Andrić
| narrator       = 
| starring       = Miki Manojlović Nebojša Glogovac Dragan Nikolić Bata Živojinović Mirjana Karanović Bogdan Diklić
| music          = Zoran Simjanović
| cinematography = Milan Spasić
| editing        = Petar Putniković
| studio         = StudioCanal
| distributor    = StudioCanal   (worldwide theatrical) 
| released       =  
| runtime        = 102 minutes
| country        = Yugoslavia
| language       = Serbian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Cabaret Balkan is a 1998 Serbian film directed by Goran Paskaljević starring Miki Manojlović and Nebojša Glogovac. Its original Serbian language title is Буре барута (Bure baruta) which means Powder Keg. It was released in English speaking countries under the title of Cabaret Balkan, with the official reason for the name change being that Kevin Costner had already registered a film project under the title Powder Keg.  The film received a number of distinctions, including a FIPRESCI award at the Venice Film Festival in 1998. It was based on a play by the same title by Dejan Dukovski.

==See also==
* 1998 in film
* Cinema of Serbia
* List of Serbian films

== References ==
 

==External links==
 

 
 
 
 
 
 
 
 


 
 