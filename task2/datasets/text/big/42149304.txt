Chicken Party
{{Infobox film
| name           = Chicken Party
| image          = Chicken Party.jpg
| caption        = 
| director       = Tate Taylor Will Roberts Steven Rogers Tate Taylor
| writer       = Tate Taylor Peter Adams Tony Morales
| cinematography = Al Satterwhite
| editing        = David Kirchner
| studio = Harbinger Pictures
| distributor    =
| released       = October 14, 2003
| runtime        = 29 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Chicken Party is a 2003 short film directed by Tate Taylor.

==Cast==
*Octavia Spencer as Laqueta Mills
*Allison Janney as Barbara Strasser 
*Tate Taylor as Luke Smith
*Melissa McCarthy as Tot Wagner

==Production==
Chicken Party was filmed in Los Angeles, California. 

==Reception==
Chicken Party won five awards. 

==References==
 

==External links==
*  

 

 
 
 

 