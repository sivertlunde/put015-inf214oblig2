Boys and Girls (2000 film)
 
{{Infobox film
| name           = Boys and Girls
| image          = Boys and girls poster.jpg
| image size     =
| caption        = Promotional poster for Boys and Girls
| director       = Robert Iscove
| producer       = Jay Cohen Lee Gottsegen Murray Schisgal Andrew Miller
| narrator       =
| starring       = Freddie Prinze, Jr. Claire Forlani  Jason Biggs Amanda Detmer Heather Donahue
| music          = Stewart Copeland
| cinematography = Ralf Bode
| editing        = Casey O. Rohrs
| studio         = Dimension Films
| distributor    = Miramax Films 
| released       = June 16, 2000
| runtime        = 94 minutes
| country        = United States English
| budget         = $35,000,000  
| gross          = $25,850,615  
| followed by    =
}}
Boys and Girls is a romantic comedy film that was released in 2000, directed by Robert Iscove. The two main characters, Ryan (played by Freddie Prinze, Jr.) and Jennifer (Claire Forlani) meet each other initially as adolescents, and later realize that their lives are intertwined through fate.

==Plot==
Jennifer Burrows and Ryan Walker meet as 12-year-olds aboard an airplane, and are immediately at odds; she is a free spirit, while he is deliberate and serious. Several years later, Ryan is mascot to his high school, while Jennifer is elected Homecoming Queen of hers. During the halftime ceremony between the two schools, Ryan is chased by the rival mascots and loses his mascot head, only to find it run over by Jennifers ceremonial car. Jennifer later finds Ryan and tries to console him about his costume. The two part ways once more, realizing they are too different.

A year later, Ryan and Jennifer are students at UC Berkeley. Ryan is in a steady relationship with his high school sweetheart, Betty, and Jennifer is having a fling with a musician. Ryan meets his roommate Hunter (aka Steve), a self-described ladies man with countless elaborate (and unsuccessful) ploys for sleeping with women. Jennifer moves in with her best friend Amy after she and her boyfriend break up. Ryan and Amy start going out, and he renews his friendship with Jennifer, even after Amy has her "breakup" with him for her. They take walks, console each other over break-ups, and gradually become best friends. Jennifer even talks Ryan into dating again, as he starts seeing a girl named Megan.

One night, in a cynical mood towards love, Jennifer breaks down and Ryan tries to console her. To their equal surprise, the two make love. Afraid of commitment, Jennifer says that sleeping together was a mistake, and that they should pretend it never happened. Hurt and lovesick, Ryan breaks up with Megan and withdraws into his studies. As months pass, Jennifer graduates and readies herself to travel to Italy. She encounters Ryan, who she has not seen since their night together, at a hilltop overseeing the Golden Gate Bridge. Ryan confesses his feelings towards her, but soon realizes that she does not feel the same way. He wishes her well in Italy, and leaves.

On the shuttle to the airport, Jennifer passes the same hilltop where the two used to spend time together and realize she indeed loves Ryan. She immediately races back to her apartment and finds Amy frantically getting dressed to greet her. Steve confidently strolls out of Amys bedroom and tells Jennifer that Ryan is heading back on a plane to Los Angeles. While waiting for departure, Ryan hears Jennifer confess her love for him in Latin. After a brief convincing, and feeling the wrath of a flight attendant, the two rekindle their romance where they first met — on an airplane.

==Cast==
*Claire Forlani as Jennifer Burrows
*Freddie Prinze, Jr. as Ryan Walker
*Jason Biggs as Hunter / Steve
*Amanda Detmer as Amy
*Heather Donahue as Megan
*Alyson Hannigan as Betty Brendon Ryan Barrett as Young Ryan Walker
*Raquel Beaudene as Young Jennifer Burrows
*David Smigelski as Homecoming King
*Blake Shields as Homecoming Knight
*Gay Thomas as N.Y. Flight Attendant
*Renate Verbaan as Farting Super Model

==Reception==

The film garnered mainly negative reviews, holding an 11% rating from Rotten Tomatoes,  with seven positive reviews and 54 negative.

==Box office==
The film opened at #6 at the North American box office, making approximately $7 million USD in its opening weekend. 

==References==
 

==External links==
*  
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 