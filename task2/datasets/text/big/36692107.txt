Swathi Muthu
{{Infobox film
| name        = Swathi Muthu
| image       = Swathi Muthu.jpg
| writer      = K. Vishwanath Meena Kishan Shrikanth
| director    = D. Rajendra Babu
| producer    = Sarovar Sanjeev
| distributor =
| released    =  
| runtime     =
| music       = Rajesh Ramanath
| language    = Kannada
| country     = India
| budget      =
}} Kannada film Meena in the lead roles. 
 Telugu film Swati Mutyam (1986) which starred Kamal Haasan and Raadhika in lead roles. 

==Plot==
Sudeep plays the role of Shivanna, an autistic person, living in a village with his grandmother. All he is good at is obeying what his trusted grandmother says. In an attempt to do good to Lalitha (Meena Durairaj|Meena), a destitute young widow with a 5-year old son, the simpleton Shivanna marries her during an auspicious village festival.

The villagers are shocked and try to harm him as his act goes against traditional societal norms. In the meantime, Shivannas beloved grandmother dies. Lalitha wants to save her new husbands life and so moves with him and her son to a city in search of better prospects. With support from a few of their acquaintances, the family of three settle down.

Life for them, thereafter, looks good and promising. Lalitha begins to appreciate Shivannas innocence and warmth. She grows closer to him and eventually bears him a son. Years pass, the children grow up, and then Lalitha breathes her last. Shivanna lives with her memories for a long time. At the end, he is seen leaving his old house in the company of his children and grandchildren, carrying with him a tulsi plant which symbolizes his memories of Lalitha.

==Cast==
* Sudeep as Shivanna Meena as Lalitha
* Kishan Shrikanth as Krishnamurthy

==Soundtrack==
 
{|class="wikitable"
|-
! Sl No. !! Song Title !! Singer(s) !! Lyrics
|-
| 1 || "Sri Chakradharige" || K. S. Chitra || V. Nagendra Prasad
|-
| 2 || "Maangalya" || K. S. Chitra || V. Nagendra Prasad
|-
| 3 || "Andada Chandada" || Rajesh Krishnan, K. S. Chitra|| V. Nagendra Prasad
|-
| 4 || "Malagiruva" || Rajesh Krishnan, K. S. Chitra || V. Nagendra Prasad
|-
| 5 || "Amma Dharma" || Rajesh Krishnan, K. S. Chitra|| V. Nagendra Prasad
|-
| 6 || "Laali" || Rajesh Krishnan, K. S. Chitra || V. Nagendra Prasad
|- Rajesh Krishnan, K. S. Chitra || V. Nagendra Prasad
|-
| 8 || "Suvvi Suvvi" || Rajesh Krishnan, K. S. Chitra || V. Nagendra Prasad
|}

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Filmfare Awards South
| 
| 
|-
|align="center"|
;Film Fans Association Awards
| 
| 
|-
|align="center"|
;Hello Gandhinagara Awards
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|}
Filmfare Awards South Best Actor - Kannada - Sudeep   Best Actress Meena  Filmfare Awards South 2003:
*  
* {{cite web|url=http://www.chitraloka.com/awards/2917-film-fare-awards-2004-sudeep-on-a-hattrick.html|title=
Film Fare Awards 2004 – Sudeep On A Hattrick|publisher=chitraloka.com|date=5 June 2004   }}
*  
*   

Film Fans Association Awards
* Best Actor - Kannada - Sudeep   

Hello Gandhinagara Awards
* Special Award - Sudeep  Hello Gandhinagara Awards:
*  
*  
*   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 