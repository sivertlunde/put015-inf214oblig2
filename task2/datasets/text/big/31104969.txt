Black Box BRD
{{Infobox film
| name           = Black Box BRD
| image          = Black Box BRD.jpg
| image size     = 
| alt            = 
| caption        = German release theatrical poster
| director       = Andres Veiel
| producer       = Pepe Danquart Erich Lackner Mirjam Quinte
| writer         = Andres Veiel
| starring       = 
| music          = Jan Tilman Schade
| cinematography = Jörg Jeshel
| editing        = Katja Dringenberg
| studio         = 
| distributor    = Warner Home Video
| released       =  
| runtime        = 102 minutes
| country        = Germany
| language       = German
| budget         = 
}}
 2001 German West German politics of the 1970s and 1980s, a period marked by turmoil and the highly publicized activities of the left-wing terrorist group known as the Red Army Faction (RAF).   

The film focuses on the lives and deaths of Alfred Herrhausen, a prominent banker and chairman of the Deutsche Bank who was assassinated by the RAF in 1989, and Wolfgang Grams, member of RAF who was a suspect in the attack on Herrhausen and who later shot himself in the head while being chased by the German police in 1993. A number of relatives, friends, and colleagues of both men were interviewed for the film.

In Germany the film was released in May 2001, and then re-released in September 2002. It was screened at a number of festivals and won several awards in 2001 and 2002, including the 2002 German Film Award for Best Documentary Film and the 2001 European Film Award.   

In the United States the film was shown on Public Broadcasting Service|PBS, retitled as Black Box Germany.

==Awards==
{| class="wikitable" style="font-size:95%"
|- style="text-align:center;"
! style="background:#B0C4DE;" | Year
! style="background:#B0C4DE;" | Awarding Body
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Nominee
! style="background:#B0C4DE;" | Result
|-
|  2001 
| European Film Awards
| Best Documentary
| Andres Veiel 
|  
|-
|  2002
| Bavarian Film Awards  Best Documentary 
| Andres Veiel 
|  
|-
|  2002
| German Film Awards Best Documentary
| Zero Film GmbH 
|  
|-
|}

==References==
 

==External links==
*   
* 
* 

 
 
 
 
 
 
 

 