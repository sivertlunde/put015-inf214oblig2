Solemn Communion
{{Infobox film
| name           = Solemn Communion
| image          = 
| caption        = 
| director       = René Féret
| producer       = René Féret Michelle Plaa
| writer         = René Féret
| starring       = Christian Drillaud
| music          = 
| cinematography = Jean-François Robin
| editing        = Vincent Pinel
| distributor    = 
| released       = 27 April 1977
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
}}

Solemn Communion ( ) is a 1977 French comedy film directed by René Féret. It was entered into the 1977 Cannes Film Festival.   

==Cast==
* Christian Drillaud - Young Charles Gravet
* Claude Bouchery - Jules Ternolain
* Isabelle Caillard - Young Josette Dauchy
* Patrick Fierry - Young François Dauchy
* Véronique Silver - Josette Dauchy at 40
* Marcel Dalio - Old Charles Gravet
* Myriam Boyer - Léone Gravet
* Manuel Strosser - Julien III Gravet
* André Marcon - Lucien Gravet
* Barbara Lemaire
* Pierre Ascaride
* Sylviane Nicolas
* Valentine Albin
* Pierre Olivier Such
* Marief Guittier - Julie Ternolain at 25
* Claude-Emile Rosen - Honore Dauchy
* René Féret - Julien I Gravet at 30
* Judith Guittier
* Andrée Tainsy - Charlotte
* Roland Amstutz - Raoul LHomme, le fils naturel de François
* Monique Mélinand - Julie Ternolain at 45
* Vincent Pinel - Leon Gravet
* Ariane Ascaride - Palmyre
* Yveline Ailhaud - Marie
* Eric Lebel - Julien I Gravet as child
* Richard Barbier
* Mireille Rivat
* Jean-Pierre Agazar
* Armelle Galliès
* Jean-Marc Tran
* Nathalie Baye - Jeanne Vanderberghe
* Gérard Chaillou - Marcel Dauchy
* Yves Reynaud - Gaston Gravet
* Fabienne Arel - Mathilde Ternolain
* Philippe Léotard - Jacques Gravet
* Jany Gastaldi - Lise Paulet-Dauchy at 20
* Monique Chaumette - Lise Paulet-Dauchy at 40
* Guillaume Lebel - Julien II Gravet
* Pierre Pernet
* Martine Kalayan
* Paul Descombes - Julien I Gravet at 50
* Jenny Clèves
* Olivier Caillard - Armand Gravet
* Michel Amphoux
* Jean-Claude Perrin
* Guillaume Bec
* Suzy Rambaud
* Pierre Forget - François Dauchy at 50
* Dominique Arden
* Serge Reggiani - Récitant chanteur (voice)
* Mathilde Azouvi - Une enfant
* Rémi Carpentier
* Jacqueline Gaillard
* Alain Chevalier
* Gérard Cruchet
* Guy Dhers
* Sophie Féret
* Philippe Gorgibus
* André Guittier
* Bernard Mounier
* François Nadal
* Philippe Nahon
* Francine Olivier
* Abbi Patrix
* Julie Pernet
* Mathieu Pernet
* Denise Péron
* François Pinel
* Iris Schmidt
* Jean-David Sultan - Un enfant
* Liliane Tylbor
* Emile Decotty - Laccordéoniste

==References==
 

==External links==
* 

 
 
 
 
 


 
 