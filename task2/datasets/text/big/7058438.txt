Silence of the North
{{Infobox Film
| name           = Silence of the North
| image          = 
| image_size     = 
| caption        = 
| director       = Allan King
| producer       = 
| writer         = 
| narrator       = 
| starring       = Tom Skerritt Gordon Pinsent Ellen Burstyn
| music          = 
| cinematography = 
| editing        = 
| distributor    =  1981
| runtime        = 94 min.
| country        = Canada English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Silence of the North is a 1981 autobiography|semi-autobiographical  Canadian film of author Olive Frederickson, taken from the book of the same name. The film stars Tom Skerritt as her first husband Walter Roemer who was killed, Gordon Pinsent as her second husband John Frederickson, and Ellen Burstyn as Olive. All three main actors were nominated for Genie Awards, as was the director, Allan King.

Originally, the movie was set to share the title of its country music theme song, "Comes a Time" performed by Lacy J. Dalton and composed by Neil Young, a Canadian artist once part of Crosby Stills Nash and Young, but at the last minute, the title of the book was retained for the film. 
 SelecTV picking it up 18 months later for its evening lineup, allowed the film to develop a cult following which remains to this day.

==External links==
* 

 
 
 
 
 
 