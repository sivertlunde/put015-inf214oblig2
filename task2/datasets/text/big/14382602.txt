Tie Xi Qu: West of the Tracks
{{Infobox film
| name           = Tie Xi Qu: West of the Tracks
| image          = TieXiQu.jpg
| film name = {{Infobox name module
| traditional    = 鐵西區
| simplified     = 铁西区
| pinyin         = Tiěxī Qū  	
| translation    = district west of the railroad
}}
| caption        =  Wang Bing Wang Bing Zhu Zhu
| writer         = 
| narrator       = 
| starring       = 
| music          =  Wang Bing Wang Bing Adam Kerdy
| distributor    =
| released       = 2003
| runtime        = 551 min. China
| Mandarin Chinese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}	
 Chinese documentary Wang Bing. Over 9 hours long, the film consists of three parts, "Rust," "Remnants" and "Rails."
 Tiexi district, an area that was once a vibrant example of Chinas socialist economy. With the move towards other industries, however, the factories of Tiexi have all begun to be closed down, and with them, much of the districts worker-based infrastructure and social constructs.

== Plot ==

=== "Rust" ===
The first portion, "Rust" follows a group of factory workers in three state-run factories: a smelting plant, an electric cable factory and a sheet metal factory. Workers at all three face sub-standard equipment, hazardous waste, and lack of safety precautions. Perhaps even worse, with the declining need for such heavy industry, the factories also face a constant lack of raw materials, leaving the workers idle and concerned for their future.

=== "Remnants" ===
The second part, "Remnants" follows the families of many of the workers in an old state-run housing block, "Rainbow Row." In particular, Wang focuses on the teenage children who concern themselves with their own lives but must also cope with their inevitable displacement as Tie Xis factories continue to close down.

=== "Rails" ===
The third part, "Rails" narrows its focus to a single father and son who scavenge the rail yards in order to sell raw parts to the factories. With the factories closing however, their future suddenly becomes uncertain.

== See also ==
*List of longest films by running time

== References ==
 

== External links ==
* 
* 
* 
* 

 
 
 
 
 
 
 


 
 