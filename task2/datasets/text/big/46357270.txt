Drunk Stoned Brilliant Dead (film)
 
 
{{Infobox film
| name           = Drunk Stoned Brilliant Dead
| image          = Drunk_Stoned_Brilliant_Dead_Poster.jpg
| caption        = Film poster
| director       = Douglas Tirola
| producer       = {{plainlist|
* Susan Bedusa
* Douglas Tirola
}}
| writer         = {{plainlist|
* Mark Monroe
* Douglas Tirola
}}
| starring       = 
| music          = Bryce Jacobs
| cinematography = Sean Price Williams
| editing        = {{plainlist|
* Joseph Hrings
* G. Jesse Martinez
}}
| studio         = 4th Row Films
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Sean Kelly, Jerry Taylor, and Ed Subitzky in beige.]]

Drunk Stoned Brilliant Dead: The Story of the National Lampoon is a 2015 American   by Rick Meyerowitz, which was also about National Lampoon, the film is not actually based on the book.   

The film contains archival material as well as new interviews with staff members and other notable figures who were fans of the magazine.  The film premiered at the 2015 Sundance Film Festival  and the New York premiere was at the 2015 Tribeca Film Festival in New York City on 16 April. 

==Poster==
The poster for the film was drawn by Rick Meyerowitz, and is deliberately reminiscent of the original poster that Meyerowitz drew for the 1978 comedy Animal House, starring John Belushi.  In the new film poster, instead of the fictional Delta Tau Chi House, the building portrayed is the National Lampoon magazines headquarters, which were at 635 Madison Avenue in Manhattan.

==Cast== Christopher Buckley, Chris Miller, Sean Kelly, Michael Gross, Janis Hirsch, Chris Cerf, Peter Kleinman, Judith Belushi Pisano, Rick Meyerowitz, Ivan Reitman, Bruce McCall, Al Jean, Beverly DAngelo, Ed Subitzky, Sam Gross, Jerry Taylor, and many others, and archival footage of staffers and Lampoon performers including John Belushi and Gilda Radner, as well as Doug Kenney and Michael ODonoghue. 

==Reception==
Ben Kenigsberg of Variety (magazine)|Variety reviewed the film positively, calling it "generous and briskly entertaining" and "a real non-fiction crowdpleaser". He also comments that director Douglas Tirola "pulls it off with style   skillfully presenting a huge amount of material, including animations rendered after the style of Lampoon artwork." 

Paula Bernstein of Indiewire interviewed director Douglas Tirola, who commented, "What I liked about the Lampoon was there was no going too far." 

John DeFore of The Hollywood Reporter called it "a lively, very entertaining look at the Lampoon s unlikely empire". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 