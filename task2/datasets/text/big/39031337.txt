Kan Pesum Vaarthaigal
 
 
{{Infobox film
| name           = Kan Pesum Vaarthaigal
| image          = Kan Pesum Vaarthaigal.jpg
| caption        = Film poster
| director       = R. Balaji
| producer       = R. Saravanakumar
| writer         = R.Saravana Kumar Mirchi Senthil Iniya Livingston
| music          = Shamanth
| cinematography = Nagakrishnan
| editing        = 
| studio         = Sri Balaji Cine Creations
| distributor    = 
| released       =  
| country        = India Tamil
| runtime        = 
| budget         = 
| gross          = 
}}
 Mirchi Senthil and Iniya.  The film was released on 22 March 2013 and centers upon a young man who rethinks his life after his father attempts suicide.    The film was filmed in Chennai and Singapore, with most of the filming taking place in Singapore. 

==Plot== Mirchi Senthil is a young unemployed man living in Thiruvarur who enjoys hanging out with his friends. His father (Livingston) is unhappy with his sons irresponsible lifestyle and attempts suicide. Mahesh then begins to search for a job, and with the help of his friend, Appu Kutty (Muruga Dass), finds a job in Singapore. He meets Janani (Iniya), who is a Singapore citizen, and attempts to marry her in order to obtain citizenship. but the truth was that she was not a Singapore citizen.

==Cast== Mirchi Senthil as Mahesh
* Iniya as Janani
* Aadukalam Murugadoss as Appukutty Livingston
* Rajendran
* Jangiri Madhumitha as Nimmi

==Reception==
IBN Live wrote that the film was "a good attempt, but following just four characters is a bit tiring at times."    The Hindu issued a feed back on how the subject is perfect from the talent adoption history of Singapore from R.Saravana Kumar who reacted to the review of Udhav Naig from The Hindu said that it was a "classic example of how easily things can go wrong in a film, when two producers, who have largely ignored the need to research their subject matter, come together. The review came within hours of the movie was released. However there is no proof for Udhav Naigs statement that the writer has largely ignored the research . The Singapore talent adoption history shows that they are continuously on the look out for talent from overseas. Thus the review conflicts from the fact. "    Malini Mannath from The New Indian Express said "The film has a lyrical title and some appealing visuals of Singapore. But not sufficient enough to make the celluloid trip worthwhile!". 



==References==
 

==External links==
*  

 
 
 