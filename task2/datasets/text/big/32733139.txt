Lifeu Ishtene
 
{{Infobox film
| name = Lifeu Ishtene
| image = Lifeu Ishtene.jpg Pawan Kumar
| producer =   Pawan Kumar
| starring =  
| music = Mano Murthy
| cinematography = Sugnana
| editing =
| studio =  
| distributor =
| released =  
| runtime = 130 minutes
| country = India
| language = Kannada
| budget =
| gross =  6 crores    
}}
 dark comedy-drama Kannada film Pawan Kumar.  It features Diganth, Sindhu Lokanath and Samyukta Hornad in the lead roles. The music is directed by Mano Murthy (which is his 25th movie)  and the music album was released in July 2011 with lyrics being penned by Jayanth Kaikini along with Yograj Bhat  who has also co-produced the film.

The film was released in over 40 theaters across Karnataka on 9 September 2011 to a collection of over  6 crores  in three weeks and received good critical reviews.  

==Plot==
Vishaal (Diganth) is a careless youngster who does not have any set goals in life. In contrast, his friend Shivu (Sathish Ninasam) is serious and advises Vishaal to reform himself. But Vishaal, whose parents allow him a lot of freedom, finds his own way to deal with his love affairs. He falls in love with Nandini (Sindhu Loknath) and wants to marry her. Nandini wants Vishaal to take up a job as it will help her discuss the marriage proposal with her father. He thinks she is the one for him, but only till the point where he has to choose between his music career and her. At this point they break up.

Later Vishaal meets television anchor Rashmi (Samyukta Horanadu) who is ready to marry him but Vishaal realises he wishes to be alone. Meanwhile, his best friend Shivu falls ill and the film ends with both of them video-conferencing about life in general. 

==Production==
Director Pawan Kumar had previously worked as an Assistant Director for Yograj Bhats recent films like Manasaare and Pancharangi which were well received by critics. The title lifeu ishtene was taken from one of the most popular Kannada songs in 2010 from Pancharangi movie.  Three weeks after the successful show of the film, Pawan Kumar said he is planning a sequel to the film which is tentatively titled Wifeu Ishtene which will "show how marriage is a bad idea for the character that Diganth played in Lifeu Ishtene."

==Cast==
* Diganth as Vishaal, an MBA student
* Sindhu Lokanath as Nandini
* Samyukta Hornad 
* Ramya Barna
* Sathish Ninasam as Shivu - Vishaals close friend
* Srinivas Prakash 

==International release== Bay Area, the screening is scheduled for 14, 15, 16, 22 and 23 October at Serra Theaters.  

==Online Release==
The movie got released online to curb piracy and to reach out to NRI audience.The film released in two versions. While the high resolution one is priced at five dollars, the low resolution version for two dollars and fifty cents. 

==Reception==
Overall, the film garnered very good reviews from critics. Newspapers like Bangalore Mirror and Times of India both rated the film 4/5 stars.       The film is noted for its story, screenplay and dialogues - all composed by the director Pawan Kumar. 

Magazine Just Femme has praised the films just portrayal of feminine characters by saying that "the women in the film are real and they are not the usual whitened, straight-haired, fake-boobed, dolled-up women who constantly forgive the men around them."  The Times of India summed it by calling it a film for the Generation Y|GenNext which is a "brilliant directorial debut by Pawan Kumar who has infused freshness in the story combined with excellent script, lively narration and not a single dull moment in the story with punchy dialogues that reminds of a Yograjbhat touch." 

==Soundtrack==
{{Infobox album
| Name        = Lifeu Ishtene
| Type        = Soundtrack
| Artist      = Mano Murthy
| Cover       = Lifeu Ishtene audio cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 21 July 2011
| Recorded    =  Feature film soundtrack
| Length      = 26:34 Kannada
| Label       = Alpha Digitech
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Lifeu Ishtene music album, composed by Mano Murthy, was released on 21 July 2011. The rights for the album was purchased by Ashwini Audio House. The music director Mano Murthy of Mungaru Male fame has introduced two new singers Renju and Ankitha Pai for this film.

During the audio release ceremony, lyricists Jayanth Kaikini and Yograj Bhat spoke not on the significance of the song but when and under what situation it was written and recorded. Director Duniya Soori|Soori, who is a close friend of Yograj Bhat, was also present during the audio release. 

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 26:34
| title1 = Yaarige Healona | extra1 = Chetan Sosca | lyrics1 = Yograj Bhat | length1 = 4:45
| title2 = Ninna Gungalli | extra2 = Sonu Nigam | lyrics2 = Jayanth Kaikini | length2 = 4:23
| title3 = Junior Devadasa | extra3 = Hemanth Kumar, Ananya Bhagath | lyrics3 = Pawan Kumar | length3 = 4:12
| title4 = Maayavi Maayavi | extra4 = Sonu Nigam, Shreya Ghoshal | lyrics4 = Yograj Bhat | length4 = 4:45
| title5 = Kanasina Hosaputa | extra5 = Rajesh Krishnan | lyrics5 = Jayanth Kaikini | length5 = 4:16
| title6 = Hrudaya Jaaruthide | extra6 = Rengith Raghav (Renju) , Ankita Pai | lyrics6 = Yograj Bhat | length6 = 3:04
| title7 = Kanasina Hosaputa (bit) | extra7 = Rajesh Krishnan | lyrics7 = Jayanth Kaikini | length7 = 1:09
}}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 