Let's Go Navy!
{{Infobox film
| name           = Lets Go Navy!
| image          = Lets Go Navy!.jpg
| image_size     =
| caption        = Theatrical poster
| director       = William Beaudine
| producer       = Jan Grippo Bert Lawrence (add. dialogue)
| narrator       =
| starring       = Leo Gorcey Huntz Hall  David Gorcey William Benedict
| music          = Edward J. Kay
| cinematography = Marcel LePicard William Austin
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Lets Go Navy! is a 1951 comedy film starring The Bowery Boys. The film was released on July 29, 1951 by Monogram Pictures and is the twenty-third film in the series.

==Plot==
A local charity has raised sixteen hundred dollars and entrusted the boys with it.  They are then robbed of the cash by two men dressed as sailors.  Believing them to be real sailors, and in order to catch them, they enlist in the Navy under fake names.  They spend a year at sea, but cannot locate the thieves.  However, Sach is able to win two thousand dollars gambling and the boys return to the Bowery.  It is there that they are robbed by the same two men, but with the Navy captain helping, they are able to capture the crooks.  They return to the navy office to receive their commendations, but are mistakenly re-enlisted!

==Production==
This is the final Bowery Boys film to feature Buddy Gorman; beginning with the next film in the series, Bennie Bartlett rejoined the group. It is also the last one produced by Jan Grippo, who left the series after his wife died. 

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*William Benedict as Whitey
*David Gorcey as Chuck
*Buddy Gorman as Butch

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Allen Jenkins as Chief Petty Officer Mervin Longnecker
*Tom Neal as Joe
*Charlita as Princess Papoola
*Richard Benedict as Red Paul Harvey as Lieutenant Commander O. Tannen
*Jonathan Hale as Captain
*Emory Parnell as Police Sergeant Mulloy
*Douglas Evans as Lieutenant Smith
*Frank Jenks as Shell game sailor

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Two" on April 9, 2013.

==References==
 

==External links==
* 
* 
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Ghost Chasers 1951
| after=Crazy Over Horses 1951}}
 

 
 

 
 
 
 
 
 
 


 