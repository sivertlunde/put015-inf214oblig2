Mohre
  
{{Infobox film
| name           = Mohre
| image          = Mohre.jpg
| image_size     = 
| caption        = DVD cover
| director       = Raghuvir Kul
| producer       = Appa Dandekar
| lyricist         = Shakti Kumar
| narrator       = 
| starring       = Nana Patekar Madhuri Dixit Sadashiv Amrapurkar Anupam Kher Alok Nath
| music          = Vanraj Bhatia
| cinematography = Ishan Arya
| editing        = Das Dhaimade
| distributor    = 
| released       = 1987
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Mohre is a 1987 Indian  . This film also starred Sadashiv Amrapurkar, Anupam Kher among others.

==Plot==
Abdul is a truck driver with alcohol and depression problems so much so that he would like to end his life. One day another truck driver shows him a newspaper advertisement that offers help for suicide-ridden youth. Abdul decides to try them out and travels all the way to the countryside. Once there he is joined by four other youth, including a printing press proof-reader, Prakash Raikar; a young woman, Maya, who had been molested and hates men, as well as two other males, which include Sunil, who is gay. They meet with the person who placed the advertisement, a wheelchair-using former Indian Army Major Vishwas Sawant and his assistant, Vasu Mudaliar. Little do they know that they have been specially chosen to die by the duo, who are not who they claim to be but are actually escaped convicts by the names of Jagga and Badri.

== Cast ==
*Nana Patekar		...	Abdul
*Madhuri Dixit		...	Maya
*Sadashiv Amrapurkar	...	Badrinath Badri Chawla / Vasu Mudaliar
*Anupam Kher		...	Jagmohan Jagga Sharma / Major Vishwas Sawant
*Shreechand Makhija		...	Makhija
*Alok Nath			...	Father Braganza
*Nilu Phule			...	Social Worker
*K.K. Raina			...	Prakash Raikar
*Jayshree T.		...	Tawaif

==External links==
 

 
 
 

 