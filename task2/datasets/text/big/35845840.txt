Ghost on Air
{{Infobox film name           = Ghost on Air image          =  caption        = Ghost on Air director       = Cheng Ding An producer       = Tay Hoo Wee writer         = Cheng Ding An / Dennis Chew starring       = Dennis Chew Gan Mei Yan Eunice Olsen Rei Poh Samuel Chong Sheena Chan music          = Ken Chong cinematography = Lim Beng Huat editing        = WideScreen Media distributor  Shaw Organisation released       =   runtime        =   country        = Singapore language       = Mandarin budget         = Singapore dollar|$1 Million gross          = Singapore dollar|$800 Thousand
}}



{{multiple issues|
 
 
 
 
}}
Ghost on Air is a Singapore horror film directed by Cheng Ding An and stars Y.E.S. 93.3FM DJ Zhou Chong Qing (Dennis Chew). The film is released island-wide in Singapore on the 17th May 2012 and is slated for release in Malaysia in June 2012.

==Synopsis==
Ping Xiao, a two time "Most Popular DJ" award recipient feels he has been robbed of his potential next award when a younger DJ Pauline steals limelight and his successful morning show slot. Desperate to get it back, he battles recurring memories of his deceased horror novelist girlfriend, to bring to his listeners the sensational "Ghost on Air" show&mdash;a radio program that shares horror stories. The show is a hit, and with each painful memory he recalls and a step closer to regaining his fame, strange and unexplainable occurrences begin to happen around him. Ping Xiao then learns a dark secret&mdash;the ghost stories he told were all true, and he could be part of the next one.

==Cast==
*Zhou Chong Qing (Dennis Chew) as Ping Xiao, popular radio personality who has fallen grace after a much published scandal
*Gan Mei Yan as Jia Li, Ping Xiaos late girlfriend
*Samuel Chong as Albert, Ping Xiaos superior
*Eunice Olsen as Pauline, rival radio DJ to Ping Xiao
*Choo Siew Feng as Mang Po, owner of Shophouse 14
*Rei Poh as Ya Nan, son of Mang Po
*Sheena Chan as Ya Ru, Jia Lis best friend
*William Lawandi as Guo Ming, fellow DJ
*Clarissa Kok as Nana, fellow DJ
*Lee Shan Hui as Ai Xi, a 14 year old deceased schoolgirl

==Production==
Director Cheng Ding An and Zhou Chong Qing met in 2008 during the promotion of Kallang Roar the movie. The two instantly found a spark, and had been actively on the lookout for an opportunity where the two can come together to create an innovative new project. After numerous coffees, it struck upon them to construct a brave new wave of terror that came from the stories of a Radio DJ. After a strenuous few months of research and drafting tonnes of story drafts by the director’s company Merelion Pictures, the story of Ghost on Air was finally ready. The film went into principal photography in December 2011 and was completed by the start of January 2012. It cost about 1 million in budget.

==References==
 

==External links==
 

 
 