The Out-of-Towners (1999 film)
 
 
{{Infobox film
| name           = The Out-of-Towners
| image          = Outoftownersmp.jpg
| caption        =
| director       = Sam Weisman Robert Evans David Madden Teri Schwartz Marc Lawrence
| starring       = Steve Martin Goldie Hawn John Cleese
| music          = Marc Shaiman John Bailey
| editing        = Kent Beyda
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $75 million
| gross          = $28,544,120 (USA)
}} 1999 comedy a 1970 film by the same name; the original version, written by Neil Simon, starred Jack Lemmon and Sandy Dennis.

==Plot==
Henry and Nancy Clark are a couple living in a quiet Ohio town. Married for 27 years, their last child has left home and Nancy is suffering from "empty nest" syndrome. Unbeknownst to her, Henry has lost his job due to corporate downsizing and has an interview in New York. Nancy sneaks on the plane with him and they begin a disastrous series of misadventures. Their plane is rerouted to Boston, their luggage is lost, they are mugged at gunpoint and their daughter has used their credit card to the point where it has reached its limit. They are thrown out of their hotel by a pompous manager (John Cleese) who also indulges in cross dressing. Forced to live by their wits on the street, the couple find themselves caught up in a robbery, chased by the police through Central Park and also finding renewed love between them. In the end, Henry aces his job interview and the two begin a new life together in "The Big Apple".

===Comparisons to the 1970 film===
There are subtle differences from the original, such as the couples credit card being declined at the hotel (instead of their room being given away as in the original), but the script is largely faithful to the 1970 version. The remake also has the addition of John Cleese as Mr. Mersault, the hotel manager, a character similar to the one Cleese played in the television series Fawlty Towers. Most of the differences are due to the characterizations of Hawn and Martin, who are both mainly comedic actors with very individual styles (the 1970 film had Jack Lemmon and Sandy Dennis playing their roles very seriously, largely the source of many of the films laughs). Consequently, the 1999 film stands alone as a Hawn and Martin collaboration, rather than as a comparable remake of an already successful film. Some compared this film to Planes, Trains and Automobiles that Martin had previously acted in. The films are similar in that they show a series of disasters on a trip.

==Cast==
 
*Steve Martin ... Henry Clark
*Goldie Hawn ... Nancy Clark
*Mark McKinney ... Greg
*Oliver Hudson ... Alan Clark
*Valerie Perri ... Stewardess
*Steve Mittleman ... Passenger
*Randall Arney ... Passenger
*Carlease Burke ... Airline Representative
*William Duell ... Lost Baggage Clerk
*J.P. Bumstead ... Boston Cab Driver
*Peggy Mannix ... Sweeper Woman
*Anne Haney ... Woman in Bathroom
*Charlie Dell ... Janitor on Train
*Jordan Baker ... Rental Car Clerk
*Tom Riis Farrell ... Andrew Lloyd Webber
*Dani Klein ... Michelle
*John Cleese ... Mr. Mersault
*Daniel Parker ... Desk Clerk
*Cynthia Darlow ... Desk Clerk
*Alyson Palmer ... Shoplifter
*Elizabeth Ziff ... Shoplifter
*Diane Cheng ... Korean Grocer
*Christopher Durang ... Paranoid Man
*Mo Gaffney ... Paranoid Woman
*Mary Testa ... Dominatrix
*Monica Birt ... Supermodel
*John Elsen ... Deli Guy
*Babo Harrison ... Well Dressed Woman
*Josh Mostel ... Dr. Faber
*Gregory Jbara ... Edward
*Amy Ziff ... Edwards Friend
*Cynthia Nixon ... Sheena
*French Napier ... Sexaholic
*Joseph Maher ... Mr. Wellstone
*Constance McCashin ... Mrs. Wellstone
*Steve Bean ... Gregs Friend
*James Arone ... Room Service Waiter
*Philip Earl Johnson ... Hotel Security Man
*Ernie Sabella ... Getaway Driver
*Jack Willis ... Robber
*John Pizzarelli ... Bandleader Mayor Rudolph Giuliani ... Himself
*Scotty Bloch ... Florence Needleman
*Chris McKinney ... Arresting Cop
*Joe Grifasi ... Arresting Cop
*Jerome Preston Bates ... Prisoner #1 Jack McGee ... Sergeant Jordan
*Jacinto Taras Riddick ... Prisoner #2
*L.B. Fisher ... Howard the Bellman
*Janna Lapidus ... Central Park Woman
*T. Scott Cunningham ... Paul
*Mandy Siegfried ... Receptionist
*Jenn Thompson ... Lisa Tobin
*John Gould Rubin ... Bill
*Christopher Duva ... Barry the Bellman
*Arthur French ... Cab Driver
*Jessica Cauffiel ... Susan Clark
 

==Reception==
The movie was a disappointment critically and commercially. It has a 23% rating on the Rotten Tomatoes website,  with Roger Ebert commenting that the movie "was not a proud moment in the often-inspired careers of Martin and Hawn."  Most of the negative reviews point to Cleese as the only redeeming factor of the film.

==See also==
* List of American films of 1999

==References==
 

==External links==
*  at the Internet Movie Database
*  at Rotten Tomatoes
*  at Box Office Mojo

 

 
 
 
 
 
 
 