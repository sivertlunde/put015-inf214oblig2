A Home of Your Own
{{Infobox Film
  | name = A Home of Your Own
  | image = A home of your own dvd.jpg
  | director = Jay Lewis
  | writer = Jay Lewis Johnny Whyte
  | producer = Bob Kellett
  | starring = Ronnie Barker Bernard Cribbins Richard Briers
  | released = 1964
  | runtime = 45 minutes British Lion
  | country = United Kingdom
}} 1964 British comedy film which is a brick-by-brick account of the building a young couple’s dream house. From the day when the site is first selected, to the day – several years and children later – when the couple finally move in, the story is a noisy but wordless comedy of errors as the incompetent labourers struggle to complete the house. It may well have been inspired by the success of Bernard Cribbins classic song of the same vein from two years earlier, "Right Said Fred".

In this satirical look at British builders, many cups of tea are made, windows are broken and the same section of road is dug up over and over again by the water board, the electricity board and the gas board.

Ronnie Barker’s put-upon cement mixer, Peter Butterworth’s short-sighted carpenter and Bernard Cribbins’ hapless stonemason all contribute to the ensuing chaos.

==Cast==
*Ronnie Barker as the Cement Mixer
*Richard Briers as the Husband
*Peter Butterworth as the Carpenter
*Bernard Cribbins as the Stonemason
*Bill Fraser as the Shop Steward
*Norman Mitchell as the Foreman Ronnie Stevens as the Architect
*Fred Emney as the Mayor

Also starring
*Janet Brown
*Gerald Campion
*Bridget Armstrong George Benson
*Douglas Ives
*Jack Melford
*Thelma Ruby
*Tony Tanner
*Aubrey Woods
*Helen Cotterill
*Barrie Gosney
*Harry Locke
*Thorley Walters
*Henry Woolf
 

== External links ==
*  
* 

 

 
 
 
 
 


 