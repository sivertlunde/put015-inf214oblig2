Kudumbini
{{Infobox film
| name = Kudumbini
| image =
| caption =
| director = PA Thomas J. Sasikumar
| producer = PA Thomas
| writer = KG Sethunath Kanam EJ (dialogues)
| screenplay = Kanam EJ
| starring = Prem Nazir Sheela Kaviyoor Ponnamma Adoor Bhasi
| music = LPR Varma
| cinematography = PB Mani
| editing = KD George
| studio = Thomas Pictures
| distributor = Thomas Pictures
| released =   
| country = India Malayalam
}}
 1964 Cinema Indian Malayalam Malayalam film, National Film Awards.

==Cast==
  
*Prem Nazir 
*Sheela 
*Kaviyoor Ponnamma 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*KK Aroor
*Muthukulam Raghavan Pillai 
*Sreemoolanagaram Vijayan
*PA Thomas Meena 
*Panjabi
 

==Soundtrack==
The music was composed by LPR Varma and lyrics was written by Abhayadev. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ambilimaaman Pidicha || K. J. Yesudas || Abhayadev || 
|- 
| 2 || Enthelaam Kadhakal || P. Leela || Abhayadev || 
|- 
| 3 || Kanninu Kanninu || Zero Babu || Abhayadev || 
|- 
| 4 || Karayaathe Karayaathe || P. Leela || Abhayadev || 
|- 
| 5 || Olathil Thulli || P. Leela || Abhayadev || 
|- 
| 6 || Swapnathin Pushparadhathil || K. J. Yesudas, P. Leela || Abhayadev || 
|- 
| 7 || Vedanayellaamenikku || P. Leela || Abhayadev || 
|- 
| 8 || Veedinu Ponmani || CO Anto || Abhayadev || 
|}

==References==
 

==External links==
*  

 

 
 
 


 