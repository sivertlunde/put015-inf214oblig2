Frankenstein Meets the Space Monster
{{Infobox film
| name           = Frankenstein Meets the Space Monster
| image          = Frankenstein Meets the Space Monster.jpg
| caption        = Film poster for Frankenstein Meets the Space Monster
| director       = Robert Gaffney
| producer       = Stanley P. Darer Alan V. Iselin Robert McCarty George Garrett John Rodenbeck
| starring       = James Karen Marilyn Hanold Lou Cutell Robert Reilly
| cinematography = Saul Midwall 
| editing        = Lawrence C. Keating
| music          = Ross Gaffney
| distributor    = Futurama Entertainment Corp.
| released       =  
| runtime        = 79 min. 
| language       = English
| budget         = $60,000 (estimated)
}}

Frankenstein Meets the Space Monster (1965) is a science fiction cult film, directed by Robert Gaffney and starring Marilyn Hanold, James Karen, and Lou Cutell.  It was filmed in Florida and Puerto Rico in 1964 

The film was released in the United Kingdom as Duel of the Space Monsters. It is also known as Frankenstein Meets the Space Men, Mars Attacks Puerto Rico, Mars Invades Puerto Rico, and Operation San Juan. Released by the Futurama Entertainment Corp., it was released on DVD by Dark Sky Films in 2006. In the United States, it was initially released on a double bill with Curse of the Voodoo. The film tells the story of a robot who combats alien invaders. Despite the title, neither Victor Frankenstein|Dr. Frankenstein nor Frankensteins monster appear in the film.

==Plot== android Colonel Frank Saunders (Robert Reilly), causing it to crash in Puerto Rico. Franks electronic brain and the left half of his face are damaged after encountering a trigger-happy Martian and his ray gun. Frank, now "Frankenstein", described by his creator as an "astro-robot without a control system" proceeds to terrorize the island. A subplot involves the martians abducting bikini clad women.

The titles space monster refers to the radiation-scarred mutation Mull brought along as part of the alien invasion force. The Frankenstein android and Mull confront one another at the climax.

==Cast==
*Marilyn Hanold as Princess Marcuzan
*James Karen as Dr. Adam Steele (as Jim Karen)
*Lou Cutell as Dr. Nadir
*Nancy Marshall as Karen Grant David Kerman as Gen. Bowers
*Robert Reilly as Col. Frank Saunders / Frankenstein
*Robert Alan Browne as Martian Crewmember (uncredited)
*Robert Fields as Reporter (uncredited)
*Bruce Glover as Martian Crewmember / The Monster (uncredited)
*Susan Stephens as Blonde Surf-bather (poster girl) (uncredited)

==Reception==
The film was ranked #7 in the 2004 DVD documentary The 50 Worst Movies Ever Made.   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 