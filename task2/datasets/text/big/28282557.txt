Son of Pakistan
 
{{Infobox film
| name           = Son of Pakistan
| image          =
| alt            =  
| caption        = Film poster
| director       = Jarar Rizvi
| producer       = Chaudhry Hameed
| writer         = Muhammad Tariq Meera Shamil Ghulam Mohiuddin Sana Nawaz
| music          = Naveed Wajid Ali Naushad.
| cinematography = 
| editing        = 
| studio         = New Vision Productions Geo Films
| released       =  
| runtime        = 
| country        = Pakistan
| language       = Urdu
| budget         = 
| gross          =
}} Ghulam Mohiuddin in the lead roles.

The film is shot in several cities of Pakistan, while special shots made in Royal Park, Lahore.

==Premise==
The story is set in Pakistan against the backdrop of the War on Terror and the prevalent consequences of the war that have stigmatised Pakistan as a country that breeds and indoctrinates terrorism. The filmmakers intended Son of Pakistan to enlighten the masses and the world at large about Pakistan and its people against the backdrop of the War on Terrors consequences. The filmmakers intended to counter a negative prejudice about Pakistan and its people as terrorists &mdash; and to show their perspective that the people of Pakistan follow a peaceful way of life.

The movie tells the story of a dutiful police officer S.P. Islam who comes from a patriotic upbringing as does his brother. Son of Pakistan depicts uncertainty in the concept of patriotism while taking the protagonists perspective of compassion towards his fellow citizens when he cannot be certain that everyone shares his beliefs. In a place of uncertainty being an enforcer of law where he comes across a potential terrorist every day, he cant trust anyone and has only himself to trust.

==Cast== Ghulam Mohiuddin
*Babar Ali
*Shamil Khan
*Meera
*Sana Nawaz
*Sila Hussain
*Amit Bhola Hansda

==Filming==
This film was shot in the cities of Pakistan; it was also shot in Kallar Kahar Daman-e-koh Islamabad and farm house of Lahore and different locations of Lahore.

==Budget==
Director and producer Jarar Rizvi claims that this movie is the most expensive to be made in Lollywood &mdash; making it false that film is made in 3 crore the film cost its producer around 85 lacs.

==Release==
The film was released on December 16, 2011.

===Reception===
Son of Pakistan mostly faced negative reviews. A critical review published in the Express Tribune stated "Son of Pakistan disappoints."  There are some sources who praises the film,  especially the director and producer for their courageous effort.

===Box office===
The film opened to an occupancy of 30% - 40%. It was released after a long delay and got just few cinemas for release. Son of Pakistan got a poor start but managed to do good business. The critics are rating it as a "Below Average" movie.

==Soundtrack==
{{Track listing
| extra_column = Performer(s)
| title1 = Son of Pakistan Title Song | length1 = 3:18 | extra1 = Ameer Ali
| title2 = Bhangra Punjabi Bha Zara | length2 = 4:35 | extra2 =  Saima Jahan, Ameer Ali, Nadeem Abbas, Amir Ghulam Abbas
| title3 = Kaali Kaali Tere Ankhein | length3 = 5:20 | extra3 = Ahmed Jahanzeb
| title4 = Hum Tum Sa Mila | length4 = 3:01 | extra4 = Ameer Ali, Saima Jahan
| title5 = Bibi Sheerini | length5 = 1:42 | extra5 = Saima Jahan
| title6 = Toona Toona | length6 = 2:47 | extra6 = Maria Shaukat
| title7 = Yeh Mausam | length7 = 2:53 | extra7 = Saima Jahan, Nadeem Abbas
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 