Athlete (film)
 
{{Infobox film
| name = Athlete
| image = ATHLETE Movie Poster The Race Begins 407x603.jpg
| caption = "The Race Begins" promotional poster
| starring =  
| director = Dave Lam
| editing = Dave Lam
| producer = Dave Lam
| music = Moby
| distributor = Gare du Nord Pictures LLC
| released =  
| runtime = 95 minutes
| language = English
| country = United States
}}

Athlete (also styled as ATHLETE) is a 2010 sports documentary film directed, edited and produced by Dave Lam that examines the popularity of endurance sports through the profiles of four individuals –&nbsp;a cancer survivor, a blind senior citizen and twin sisters&nbsp;– who compete in marathons and triathlons. The film was released on DVD and video on demand on March 9, 2010.      

== Athletes profiled ==
The films principal subjects are:    survivor of Hodgkins lymphoma and member of Team in Training member;
* Artie Elefant, a 61-year-old athlete who is blind due to retinitis pigmentosa; member of the Achilles Track Club;
* Carrie Neveldine and Kellie Smirnoff, 35 year-old twins training for their first Ironman triathlon.

== Production ==
Filming was on location in the United States, beginning in September 2006 and lasting 12 months. The locations featured are:  
* New York City; Syracuse and Lake Placid, New York;
* Long Branch, New Jersey;
* Miami and Jacksonville, Florida;
* Raleigh-Durham, North Carolina|Raleigh-Durham and Boone, North Carolina;
* Lake Winnipesaukee, New Hampshire;
* Washington, D.C.

== Races ==
Races featured in the documentary: 
* New York City Marathon
* New Jersey Marathon
* Miami Marathon
* Ironman Lake Placid Triathlon
* Mighty North Fork Triathlon
* Tupper Lake Tinman Triathlon
* Finger Lakes Triathlon
* New York City Triathlon
* Rochester Autumn Classic Duathlon
* Achilles Hope & Possibility 5-Mile Run/Walk
* Five Boro Bike Tour

== Cameos ==
Source: 
*Lance Armstrong, former  seven-time Tour de France champion and cancer survivor; Chris Carmichael, Lance Armstrongs coach and founder of Carmichael Training Systems;
*Paula Radcliffe, womens marathon world-record holder and three-time New York City Marathon champion;
*Frank Shorter, marathon legend and 1972 Summer Olympics gold medalist;
*Dick Traum, founder of the Achilles Track Club; hematologist and oncologist at the Duke University Medical Center.

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 