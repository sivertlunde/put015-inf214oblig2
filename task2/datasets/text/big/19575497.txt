Dark Country
{{Infobox film
| name           = Dark Country
| image          = Dark_Country.jpg
| caption        = Official poster
| director       = Thomas Jane
| producer       = Patrick Aiello Ashok Amritraj 
| writer         = Tab Murphy
| starring       = Thomas Jane Lauren German Ron Perlman Eric Lewis
| cinematography = Geoff Boyle
| editing        = John Lafferty Robert K. Lambert
| studio         = Stage 6 Films Hyde Park Films
| distributor    = Stage 6 Films Sony Pictures Home Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $5 million
| preceded_by    = 
| followed_by    = 
}} psychological crime thriller directed by and starring Thomas Jane in his directorial debut. It  also stars Lauren German and Ron Perlman.   

==Plot==
Newly weds Dick (Thomas Jane) and Gina (Lauren German) decide to head across the Nevada desert for their honeymoon, driving at night to beat the heat. Before they head off, a stranger warns Dick to be careful, as couples have been known to get lost, and to stick to the Interstate. Shortly afterward, the couple realize they are heading the wrong way and turn off the highway onto another road. Dick turns off the car headlights to drive by starlight and Gina masturbates herself to orgasm as they head across the desert. Eventually Dick turns the lights back on, immediately swerving to avoid a figure in the middle of the road. Investigating, they find a man severely injured from a car accident. Unable to get a phone signal, they decide to drive him to a hospital themselves, only for the road to come to a sudden end a few miles ahead.

During the drive, the couple argue and the injured man awakens with a scream. He asks Gina for a cigarette, advises her to leave her husband and becomes increasingly erratic, finally attempting to strangle Dick and almost causing the car to crash. Gina stops the car and the two men tumble out, continuing to fight until Dick beats the stranger to death with a rock. Dick convinces his wife they need to dispose of the body, and together they bury it in a shallow grave. While she fills the hole, Dick finds a revolver in her handbag. Soon after, they arrive at a rest area where several cars are parked. They tidy themselves up and argue until Dick discovers he lost his watch while they were burying the stranger. Refusing to go back, Gina waits at the rest stop with the gun while Dick returns to find his watch. Arriving at the site where they hid the body, Dick finds the grave empty. Gunshots ring out across the desert and Dick races back to the rest stop to find Gina is missing. Nearby, he stumbles onto a womans grave and realizes that the other cars are rusted and covered with dust.

In a panic, he flees, almost colliding head-on with a deputy sheriff (Ron Perlman). In the back of the police car, he rides with the deputy to a crime scene, where police are excavating murder victims from a mass grave surrounded by abandoned vehicles. The deputy explains that this was where the rest area had been 30 years before. Dick recognizes the spot as the location where he buried the stranger. As he watches on from the back of the police car, a deputy exhumes Ginas body and finds Dicks watch. Dick kicks his way out of the patrol car and escapes in one of the nearby vehicles, leading the squad of police in a chase across the desert.

Driving with the lights off, he loses them, only to lose control and roll the car. Thrown clear of the wreck, he is then almost run down by another car before he passes out. Some time later, Dick wakes to find himself in the back of his own car, listening to himself and Gina argue, and realizes that he was the mysterious stranger that he fought with and murdered earlier in the evening.

==Cast==
* Thomas Jane as Dick / Bloodyface 
* Lauren German as Gina
* Ron Perlman as Deputy Thompson 
* Con Schell as Bloodyface Double
* Chris Browning as Stranger
* Rene P. Mousseux as Crime Scene Trooper

==Production==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | I called Mel Gibson and he talked to me on the phone for an hour, and said that when he was getting ready to direct and star in his first film, he was nervous and he called Clint Eastwood. Clint Eastwood talked to Mel for a long time and told Mel that he was really nervous and he called Don Siegel, who had directed Clint in a bunch of movies, and Don told Clint, Don’t sell yourself short. Spend as much time on yourself—your own shots—as you do on every other actor, on every other aspect of production. Be careful, because you’re in the movie you have permission to just do one or two takes on yourself and quickly move on—but you need to spend as much time on yourself for your film to work.   
|-
| style="text-align: left;" | —Thomas Jane on getting directing advice.
|}

The idea for making Dark Country came after Jane had read the short story by the films writer Tab Murphy.  After working for a year on the story, Jane and Murphy brought it to Lionsgate, who purchased the script. Upon learning Janes intention to shoot the film 3D, Lionsgate backed out of the deal and allowed Jane and Murphy to take the script to Sony Pictures, whose home video division were looking for content for their new line of 3D televisions that were soon to be released.  
 The Outer Limits and old issues of EC Comics comic books. 
 

Before filming began, Jane, together with his storyboard artist, story boarded every shot, approaching the film like they were doing a live-action graphic novel. Wanting to have as many graphic novel elements Jane brought on-board comic artist Tim Bradstreet to work as the visual consultant and production designer in addition to Berni Wrightson, who provided the designs for the character Bloodyface,  and Ray Zone as the 3D supervisor.  Jane chose to do the film in 3D as a way to prove to the filmmaking community that you could create a low-budget film in 3D and have it turn out looking great. 

The 25-day shoot took place in Albuquerque, New Mexico.  The film was shot in both 3-D high definition and 2-D high-def, with the intention of a limited theatrical run in 3D.  The 3D for Dark Country was done using two Silicon Imaging (SI) HD heads capturing at 2K resolution, the cameras were built by Hector Ortega and Stephen Pizzo of Element Technica,  and supervised by Geoff Boyle, Max Penner, Tim Thomas and Paradise FX in California. The small size of the cameras allowed for more fluid camera movements compared to the cameras previously used to shoot 3D films. 

For the filming of scenes in 3D, Jane wished for the effect to vary in intensity and be impactful. To help in that, Jane devised a color-coded system for his cameraman to know how he wanted the 3D to be visually in any particular scenes.     

==Release==
Dark Country was released on DVD on October 6, 2009 in North America by Sony Pictures Home Entertainment and Stage 6 Films. On April 1, 2011, French distributor Metropolitan Filmexport released a Blu-ray Disc#Blu-ray 3D|Blu-ray 3D edition of the film. 

A graphic novel adaptation of Dark Country by Swiss artist Thomas Ott was released on May 30, 2012, by Raw Studios through comiXology as a digital download. 

===Critical reception===
Upon its release Dark Country received mixed reviews, though many reviewers praised Janes acting and directing style. David Ray Carter of   gave the film a positive review stating, "With Dark Country, his directorial debut, he manages to pump a ton of noir-ish atmosphere into what could have been a by the numbers thriller. Sure you may figure out the big secret pretty quickly, I did, but Dark Country is a visually intense thriller that keeps you watching." 

Conversely David Nusair of Reel Film Reviews gave the film 1 out of 4 stars, stating that "Its clear right from the opening frames that Jane is going for a hyper-stylized throwback to the film noir thrillers of the 1950s, yet, despite his best efforts, the first-time filmmaker is simply unable to transform the two protagonists into figures worthy of the viewers interest and sympathy. Janes reliance on visuals that are almost astonishingly garish." And concluding that "Dark Country s pervasive lack of compelling elements cements its place as a misfire of impressively epic proportions (and this is to say nothing of the laughably nonsensical twist ending, which would seem like a stretch within a David Lynch film)."  Jeff Allard of Shock Till You Drop stated of the films problems "If the same story were told in less time, it mightve worked better. This is a half-hour or hour-long anthology episode at best, not a feature. Even with a brief 88 minute running time, Dark Country feels like its taking excessively long to get to its climax and by the end, what should be an ironic wallop is more of a shoulder shrug." While ultimately praising the performances of Jane and German, he concluded that "Dark Country is a noble effort that takes too many wrong turns and ends up getting lost. For fans that find themselves jonesing for a Twilight Zone-esque tale or some noir atmosphere, however, your mileage may vary." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 