Fury at Furnace Creek
{{Infobox film
| name           = Fury at Furnace Creek
| image          = Fury at Furnace Creek Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = H. Bruce Humberstone
| producer       = Fred Kohlmar
| writer         = {{Plainlist|
* Charles G. Booth
* Winston Miller
}}
| story          = David Garth
| starring       = {{Plainlist|
* Victor Mature
* Coleen Gray
* Glenn Langan
* Reginald Gardiner
}}
| music          = David Raksin
| cinematography = Harry Jackson
| editing        = Robert L. Simpson
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by H. Bruce Humberstone and starring Victor Mature, Coleen Gray, Glenn Langan, and Reginald Gardiner.   

==Plot==
Troops are massacred at a Furnace Creek fort in 1880 after an army captain, Walsh, cites orders forcing him to abandon a wagon train. Apache Indians hid inside the wagons to gain access to the fort.

General Blackwell is blamed for the incident and court-martialed. Denying that he sent any such order, the generals heart gives out on the witness stand. No written evidence of the order is presented.

One of his sons, Rufe, a captain from West Point, travels west to find out what happened. His brother, Cash, reads of their fathers death in a Kansas City newspaper and also heads toward Furnace Creek in search of answers.

Using an alias, Cash learns that Capt. Walsh has become a drunkard. A mining boss, Leverett, is impressed by the stranger in town and hires him, not knowing Cashs real name or intent. Rufe arrives in town and also assumes a false identity.

Cafe waitress Molly Baxter, whose father was killed at the fort, still considers General Blackwell the man to blame. But the real villain is Leverett, who bribed Walsh and organized the Apache raid. A guilty conscience causes Walsh to write a confession. Leverett sends one of his henchmen to do away with Walsh, but the confession is found by Cash.

Rufe is framed, arrested and tried, but escapes. Cash gives him the confession and tells him to take it to the Army as proof. Wounded in a gunfight with Leverett but victorious, Cash recovers and reads in the paper about the proof of General Blackwells innocence.

==Cast==
 
* Victor Mature as Cash Blackwell
* Coleen Gray as Molly Baxter
* Glenn Langan as Rufe Blackwell
* Reginald Gardiner as Captain Walsh
* Albert Dekker as Leverett
* Fred Clark as Bird
* Charles Kemper as Peaceful Jones
* Robert Warwick as Gen. Fletcher Blackwell
* George Cleveland as Judge
* Roy Roberts as Al Shanks
* Willard Robertson as Gen. Leads
* Griff Barnett as Appleby  

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 