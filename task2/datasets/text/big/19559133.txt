Città violenta
{{Infobox film
| name           = Città violenta
| image          = Città violenta.jpg
| image_size     = 
| caption        = Cover of the DVD release
| director       = Sergio Sollima Harry Colombo George Papi
| writer         = Massimo De Rita (story) Gianfranco Galligarich (screenplay)  Lina Wertmüller
| narrator       = 
| starring       = Charles Bronson  Jill Ireland  Telly Savalas  Michel Constantin
| music          = Ennio Morricone
| cinematography = Aldo Tonti
| editing        = Nino Baragli
| distributor    = International Co-Productions United Artists
| released       =  
| runtime        = 100 min   109 min (uncut)
| country        = Italy/France Italian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} thriller with a plot of hitman revenge.  

== Plot == assassin Jeff Heston (Charles Bronson) and mistress Vanessa (Jill Ireland) pursued mercilessly while holidaying in the Virgin Islands. Jeff is shot and left for dead, while Vanessa runs off with his shooter and former business associate Coogan. After his release from prison on a framed murder charge, Jeff tracks the pair to New Orleans. However, after taking revenge on his betrayer and reuniting with Vanessa, Jeff is blackmailed by the very crime boss (Telly Savalas) who framed him, took Vanessa as his mob wife, and who is now intent on having him join his organisation.  When Jeff refuses, he is hunted through an unforgiving city only to discover that his real enemy is closer than he realised.

== Release and reception == Death Wish The French Connection "by staging its engine-revving, pedestrian-dodging antics not on the wide streets of American cities but, rather, the narrow, winding pathways (and, in one case, staircases) of a Caribbean island."   

== Influence == 

Danish film director, Nicholas Winding Refn has cited Violent City as one his favorite Italian genre films. 

American film director Quentin Tarantino used samples from the films Ennio Morricone score in his 2012 film Django Unchained.  

== See also ==
*Le Samouraï, a 1967 French crime drama which influenced Città violenta
* New Orleans in fiction

== References ==
 

== External links ==
* 
*  
 

 
 
 
 
 
 
 
 
 
 
 