Jump! (film)
{{Infobox film
| name           = Jump
| image          = Jump! Film Poster.jpg
| image_size     =
| caption        = 
| director       = Joshua Sinclair Ryan James Joshua Sinclair
| narrator       = 
| starring       = Ben Silverstone Patrick Swayze Martine McCutcheon
| distributor    = LBW Media
| released       =  
| runtime        = 118 minutes
| country        = United Kingdom  Austria
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Jump! is a 2008 United Kingdom|British-Austrian drama film written and directed by Joshua Sinclair. It starred Ben Silverstone, Patrick Swayze and Martine McCutcheon.  It was loosely based on the real-life Halsman murder case. The film was premiered on the 2009 Jewish Film Festival in June 2009. Swayze was unable to attend due to pancreatic cancer. 

==Synopsis==
Set in Austria, in 1928, with the spectre of Nazism on the rise, a young Jew, Philippe Halsman, is accused of patricide after his fathers death during a hike through the Alps. His strained relationship with his father, and the apparent evidence that he has been struck on the head with a rock, point towards the sons guilt. Halsman is put on trial, in Innsbruck, where his case is taken up by one of the countrys leading lawyers.

==Cast==
* Ben Silverstone - Phillippe Halsman 
* Patrick Swayze - Richard Pressburger 
* Martine McCutcheon -  Liuba Halsman 
* Heinz Hoenig - Morduch Halsman 
* Anja Kruse - Ita Halsman 
* Heinz Trixner - Emil Groeschel 
* Christoph Schobesberger - Siegfried Hohenleitner  Richard Johnson - Judge Larcher 
* Wolfgang Fierek - Leopold Zipperer 
* Adi Hirschal - Dr. Stein 
* Alf Beinell - Franz Eicher 
* Christian K. Schaeffer - Johan Weiler
* Erik Jan Rippmann - Josef Eder 
* Cornelia Albrecht - Marilyn Monroe 
* Bernd Jeschek - Josef Weil

==References==
 

== External links ==
*  

 
 
 
 
 
 
 


 
 