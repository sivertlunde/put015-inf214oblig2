Milky Way (2000 film)
 
{{Infobox film
| name           = Milky Way
| image          = Mlijecni_put.jpg
| caption        = 
| director       = Faruk Sokolović
| producer       = Šuhreta Duda Sokolović
| writer         = Faruk Sokolović  Almir Imširević  Edina Kamenica
| starring       = Žan Marolt  Gordana Boban  Dragan Bjelogrlić  Selma Alispahić
| music          = Zlatan Fazlić
| cinematography = Stipo Svetinović
| scenography    =
| editing        = Bojan Filipović
| studio         = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Bosnia and Herzegovina
| language       = Bosnian
| budget         = 
| gross          = 
}} Bosnian film directed by Faruk Sokolović.            

==Cast==
*Žan Marolt - Mujo Hrle
*Gordana Boban - Sena
*Dragan Bjelogrlić - Ale
*Selma Alispahić - Anka
*Davor Janjić - Josip
*Nada Đurevska - Fata
*Ante Vican - Stjepan
*Ada Sokolović - Dalila
*Hana Sokolović - Denis
*Ivo Gregurević - Službenik
*Nebojša Veljović - Prodavac

==References==
 

==External links==
* 

 
 
 
 
 

 