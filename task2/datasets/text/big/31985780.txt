Deiva Magan
 
{{Infobox film
| name           = Deiva Magan
| image          = Deiva Magan.jpg
| image_size     =
| caption        =
| director       = A. C. Tirulokchandar
| producer       = Periyanna
| writer         = Aarur Das (dialogues)
| screenplay     = A. C. Tirulokchandar
| story          = Dr. Nihar Gupta
| narrator       =
| starring       =  
| music          = M. S. Viswanathan
| cinematography = Thambu
| editing        = B. Kandasamy
| distributor    = Shanthi Films
| released       =   
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
 Bale Pandiya. Jayalalitha as submitted by India in contest for the Academy Award for Best Foreign Language Film.    The film had a song "Deivamae Deivamae" sung by T. M. Soundararajan which became very popular.  Sivajis performance in this film was well acclaimed.     The film was adapted from Bengali play Ulka by Dr. Nihar Ranjan Gupta. 

==Plot==
Sivaji Ganesan portrays three roles: an insecure father, a timid son and an angst-ridden twin brother.  Sivaji Ganesan, a lawyer with a scarred face, gets married to Pandari Bai. The couple lead a happy life and Pandari Bai gives birth to twin brothers, a son with a scarred face (Also Ganesan) like his father and a brother (Also Ganesan). Their father unable to tolerate his eldest sons face leaves him under the custody of another person and tells his wife that the first child is dead. As years pass by, the father becomes a justice and his younger son, a happy-go-lucky college-going timid son, falls in love with J. Jayalalithaa|Jayalalithaa. In the meanwhile, the elder son comes to know about his family and tries to meet his mother and brother but he is asked by his father not to reveal his identity to them. M. N. Nambiar, a man once punished by the father, kidnaps the younger son in order to take revenge on him. In the end, the elder son rescues his brother, killing M. N. Nambiar. He succumbs to his injuries and dies on the lap of his mother (Pandari Bai). This film was the first Tamil film recommended for an Oscar.

==Cast==
* Sivaji Ganesan
* Chittor V. Nagaiah
* Pandari Bai Jayalalitha
* Major Sundarrajan
* M. N. Nambiar

==Production== Muthuraman which failed at the box office, "Deivamagan" was the second adaptation of the play.  Sivaji who was impressed with the plot decided to make the film and made his friend Periyanna to produce the film.  The film was initially titled as "Uyiroviyam" before finalising "Deivamagan". 

The film involved the characters of father and his two sons which would be tough to be acted by the same person so the director wanted two young actors to play the characters of two sons. However Sivaji accepted the challenge and acted in the role of both sons, bringing a difference in body language and mannerisms to each role. 

==Remakes==
The film was dubbed in Telugu as Koteeswaralu. The film was remade in Hindi as Bairaag (1976) with Dilip Kumar. 
 Vikram which was earlier titled as Deiva Magan  was thought to be the remake of this film, however the director denied it by saying the story of both these films are no way connected with each other and eventually the new films title was changed as Deiva Thirumagal. 

==Soundtrack==
{{Infobox album  		
| Name =	Deiva Magan	
| Type =	soundtrack	
| Longtype =		
| Artist =	M. S. Viswanathan	
| Cover =		
| Cover size =		
| Caption =		
| Released =	 	
| Recorded =		
| Genre =		
| Length =		  Tamil	
| Label =		
| Producer =		
| Reviews =		
| Compiler =		
| Misc =		
}}		
		
{{tracklist	
| collapsed =	
| headline =	
| extra_column =	Singers
| total_length =	
	
| all_writing =	
| all_lyrics =	Kannadasan
| all_music =	M. S. Viswanathan
	
| writing_credits =	
| lyrics_credits =	
| music_credits =	
	
| title1 =	Deivame Deivame Nandri Solven
| note1 =	
| writer1 =	
| lyrics1 =	
| music1 =	
| extra1 =	T. M. Soundararajan
| length1 =	
	
| title2 =	Kadal Malar Kootam
| note2 =	
| writer2 =	
| lyrics2 =	
| music2 =	
| extra2 =	T. M. Soundararajan
| length2 =	
	
| title3 =	Anbulla Nanbare
| note3 =	
| writer3 =	
| lyrics3 =	
| music3 =	
| extra3 =	T. M. Soundararajan
| length3 =	
	
| title4 =	Kaathalikka Katrukollungal
| note4 =	
| writer4 =	
| lyrics4 =	
| music4 =	
| extra4 =	T. M. Soundararajan, P. Susheela
| length4 =	
	
| title5 =	Kangal Pesuthamma
| note5 =	
| writer5 =	
| lyrics5 =	
| music5 =	
| extra5 =	P. Susheela
| length5 =	

| title6 =	Kettadhum Koduppavane Krishna
| note6 =	
| writer6 =	Kannadasan
| lyrics6 =	
| music6 =	
| extra6 =	T.M. Soundararajan
| length6 =	3.56
}}

==Release==
Tamil magazine Ananda Vikatan said in its review, "A film with multiple roles of same actor without any confusion which itself a milestone, Makers have tried to built a hall with just one pillar and that was Sivaji Ganesan!. 


==See also==
* List of submissions to the 42nd Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

==Bibliography==
* 

 
 

 

 
 
 
 
 
 
 
 