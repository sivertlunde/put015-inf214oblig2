Miracolo italiano
{{Infobox film
| name = Miracolo italiano
| image = Miracolo italiano.jpg
| caption = 
| director = Enrico Oldoini
| writer = Enrico Oldoini Liliana Betti Giovanna Caico Bernardino Zapponi 
| starring =Renato Pozzetto
| music =   Manuel De Sica
| cinematography = Giorgio Di Battista
| editing =  Raimondo Crociani
| language = Italian
}}  1994 Cinema Italian Anthology anthology comedy film directed by Enrico Oldoini.    

== Cast ==
* Renato Pozzetto: Fermo Pulciani/ Passenger of the Ferry 
* Nino Frassica: Toti/ Ernesto
* Ezio Greggio: Manuel Rodriguez/ Marcello Troiani
* Giorgio Faletti: Teodoro Pautasso, aka Teo
* Maria Amelia Monti: Lucia Baggioni
* Leonardo Pieraccioni: Saverio
* Anna Falchi: Maria
* Nadia Rinaldi: Adelaide
* Claudia Koll: Maria Carla
* Daniela Conti: Rosalia
* Athina Cenci: Teos mother-in-law/ Saverios mother 
* Carlo Monni: Nedo
* Novello Novelli: Grandfather of Saverio
* Sergio Bini Bustric:  Brother of Saverio
* Tony Sperandeo: Deputy Locafò
* Gianfranco Barra: Deputy Nania
* Cecilia Dazzi: Vanessa
* Carlotta Natoli: Samantha 
* Dario Bandiera: the Taxi Driver
* Enrico Brignano: Michele 
* Remo Remotti: Fermos father
* Francesco Benigno: Carmelo

==References==
 

==External links==
* 

 
 
 
 
 


 
 