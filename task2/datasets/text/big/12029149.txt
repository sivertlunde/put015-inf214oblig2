Mano Po 5: Gua Ai Di
{{Infobox film
| name           = Mano Po 5: Gua Ai Di
| image          = ManoPo52006Poster.jpg
| caption        = Philippines poster
| director       = Joel Lamangan
| producer       = Roselle Monteverde-Teo
| writer         = Roy C. Iglesias
| screenplay     = 
| story          = 
| based on       =  
| starring       = Lorna Tolentino, Richard Gutierrez, Angel Locsin
| music          = Von de Guzman
| cinematography = 
| editing        = 
| studio         = Regal Multimedia
| distributor    = Regal Films
| released       =  
| runtime        = 
| country        = Philippines Tagalog English English Min Nan
| budget         = 
| gross          = 
}}
 2006 Filipino Filipino actors Angel Locsin as Charity and Richard Gutierrez as Nathan. Nathan is a veterinarian with playboy tendencies who falls in love with Charity, a full blood Chinese who always follows tradition.  In order to woo not only Charity but also her family, Nathan tries to follow and adapt Chinese culture, but with hilarious results.

Unlike its earlier predecessors and the following installment  , Mano Po 5 is rather more of a romantic comedy-drama than the purely drama genre.

The film stars   finalist Christian Bautista also appeared in his first film at the height of his successful musical career.

==Cast==
*Richard Gutierrez as Nathan
*Angel Locsin as Charity
*Christian Bautista as Timothy/Felix Yan
*Lorna Tolentino as Yolanda
*Tirso Cruz III as Williamson
*Ketchup Eusebio as Emerson
*Michelle Madrigal as Kate
*Jacklyn Jose
*AJ Dee as Anderson
*Joyce So
*Boots Anson-Roa
*Aria Clemente as Young Charity
*Enzo Sison as Young Timothy

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2006
| rowspan="5" align="left"| Metro Manila Film Festival  Best Cinematography
| align="center"| Charlie Peralta
|  
|- Best Production Design
| align="center"| Egay Littua
|  
|- Best Musical Score
| align="center"| Von De Guzman
|  
|- Best Sound Recording
| align="center"| Ditoy Aguila
|  
|- 1st Runner-up Best Float
| align="center"| Mano Po 5: Gua Ai Di
|  
|}

==References==
 

==External links==
* 

 
 
 
 
 

 
 