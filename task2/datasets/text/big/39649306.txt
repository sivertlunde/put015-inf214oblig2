Lola Montez, the King's Dancer
{{Infobox film
| name           = Lola Montez, the Kings Dancer
| image          = 
| image_size     = 
| caption        = 
| director       = Willi Wolff 
| producer       = 
| writer         = Paul Merzbach
| narrator       = 
| starring       = Ellen Richter   Arnold Korff   Fritz Kampers   Frida Richard
| music          = 
| editing        =
| cinematography = Arpad Viragh
| studio         = Ellen Richter Film  UFA
| released       = 28 December 1922
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent historical historical drama distributor Universum Film AG|UFA. 
 Lola Montez had been released in 1919, starring Leopoldine Konstantin.

==Cast==
* Ellen Richter as Lola Montez 
* Arnold Korff as Ludwig I, König von Bayern 
* Fritz Kampers as Lieutenant Nussbaum 
* Frida Richard as Zigeunerin 
* Georg Alexander as Studiosus Ludwig von Hirschberg 
* Georg Baselt as Von Pechmann, Polizeipräsident von München 
* Arthur Bergen as Madras, Zigeuner 
* Hugo Döblin as Gouverneur von Barcelona 
* Heinrich George as Don Miguel 
* Max Gülstorff as Journalist Beauvallion 
* Leonhard Haskel as Pillet, Direktor der Pariser Oper  Hans Junkermann as Hoftheaterintendant 
* Friedrich Kühne as Staatsrat von Berks 
* Robert Scholz as Louis Napoleon 
* Julia Serda as Königin Therese 
* Toni Tetzlaff as Primaballerina 
* Hans Heinrich von Twardowski as Studiosus Friedemann 
* Ernst Pittschau as Studiosus Peisner 
* Fritz Beckmann   
* Gustav Botz   
* Rudolf Del Zopp   
* Wilhelm Diegelmann  
* Julius Falkenstein   
* Maria Forescu   
* Carl Geppert   
* Martha Hoffmann   
* Rudolf Meinhard-Jünger   
* Albert Patry   
* Herbert Paulmüller   
* Hermann Picha   
* Emil Rameau   
* Fritz Richard    Fritz Schulz   
* Mizzi Schütz   
* Alfred Walters   
* Kurt Wolowsky

==References==
 

==Bibliography==
* Kreimeier, Klaus. The UFA Story: A Story of Germanys Greatest Film Company 1918-1945. University of California Press, 1999.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 