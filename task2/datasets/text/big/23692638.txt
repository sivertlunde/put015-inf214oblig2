Sankham
{{Infobox film
| name = Sankham
| image = Sankham Poster HD.jpg
| caption = Siva
| producer =
| writer = Gopichand Trisha Trisha   Sathyaraj
| music = Thaman
| cinematography =
| editing =
| released =  
| studio =
| distributor =
| released =
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
}} Telugu Action Action film, starring Tottempudi Gopichand|Gopichand, Trisha Krishnan, Sathyaraj amongst others, directed by Siva (director)|Siva. Produced by J Bhagavan, J Pulla Rao, and music composed by Thaman. It was a below average grosser on its release in 2009. 

The film was dubbed in Hindi as Phir Ek Most Wanted and in Tamil as Sivappu Saamy.

==Plot==
Chandu (Gopichand) is brought up by his uncle (Chandra Mohan) in Australia. Mahalakshmi (Trisha) stays in Australia along with her uncle. Chandu is fond of martial arts and he never gets a chance to exhibit them in real situations. Mahalakshmi is another martial arts freak. After a few misunderstandings they fall in love. Mahalakshmi is forced to come back to her home town in Rayalaseema. Chandu comes in search of her. Meanwhile Sivaiah (Satyaraj) and his opponent Pasupathi (Kota Srinivasa Rao) belong to two neighboring villages. They have longtime enmity. The crux of the movie is how Chandu and Mahalakshmi are related to Siavaiah and Pasupathi. 

==Reception==

Sankham received mixed reviews from critics. Idlebrain rated it 2.5/5, calling it a clichéd film.   123telugu rated it 3.25/5, along with stating that "Shankam can be watched once as it does meet the minimum audience expectation of watching a timepass movie." 

==References==
 

==External links==
* 
* 
 

 

 
 
 
 
 


 
 