Lucky and Zorba
{{Infobox film
| name           = Lucky and Zorba   La gabbianella e il gatto
| image          = LA GABBIANELLA E IL GATTOposter.jpg
| caption        = Original theatrical poster
| director       = Enzo DAlò
| producer       = Vittorio Cecchi Gori   Rita Rusic
| screenplay     = Enzo DAlò   Umberto Marino
| story          = Luis Sepúlveda
| starring       = Carlo Verdone   Antonio Albanese   Melba Ruffo   Luis Sepúlveda David Rhodes
| cinematography = 
| editing        = Rita Rossi
| studio         = Lanterna Magica
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Italy
| language       = Italian,English(Dub)
| budget         =
| gross          =
}}

Lucky and Zorba ( ; literally "The Little Seagull and The Cat") is a 1998 Italian traditional animation film directed by Enzo DAlò, based on The Story of A Seagull and The Cat Who Taught Her To Fly by Luis Sepúlveda. The movie was dubbed in english and aired on Toon Disney during the early 2000s.

==Plot==
In the coasts of Hamburg in Germany, a petrol ship sinks leaving a lot of petrol in the sea. The next day a seagull flock starts looking for fish in the sea, they dive in and stay there until there leader spots the petrol flood, he warns the rest of the flock, but one of the seagulls named Kengah doesnt hear it and gets dirtied by the petrol. She survives the accident but has trouble with flying. She flies over the city until she falls on a womans garden, right on top of her cat Zorba. Being disgusted by the petrol taste Zorba refuses to eat her. Kengah asks him three promises that he must do if she doesnt survive. The first one is that when she lays her egg he must not eat it, the second one is that he must take care of it until it hatches, and third is that he would teach the newborn how to fly. 

Zorba promises despite his hesitations, then he goes to find his friends to try and help save Kengah. Zorba gets his friends but when they arrive to save the seagull its too late. Under her wing they find her egg, so Zorba tells them about the promise and the cats decide to help him by giving him some instructions (found in encyclopedias in a nearby abandoned museum) on taking care of the egg. Zorba then forces himself to gently sit on the egg and hatch it. 

Word soon spreads about a cat hatching a birds egg, until it reaches the ears of Zorbas love interest Bubulina and the towns cats arch-enemy Great Big Rat, who after hearing the news of the cat-egg makes a plan to make all the towns cats his servants. The egg soon hatches and the cats decide to name the newborn Lucky. Lucky lives with the cats believing to be a cat herself. Her belief soon disappears when YoYo, a red kitten jealous of Lucky because of all the attention and advantages she gets, tells her that shes a bird and that her adoptive father wants to eat her. 

Lucky runs away and gets captured by Great Big Rats minions.  The cats look for her all over the town until they found out that Great Big Rat has her captured.  The cats build a big cheese and hide in it (a trick they learned from the Trojan Horse). YoYo however goes alone into the sewers and stops the rats before they can eat Lucky, but both of them end up captured. The cats cheese arrives just in time as the cats jump out and rescue Lucky and YoYo right before Great Big Rat could kill them. The cats throw Great Big Rat and his sidekick into the canals. 

Zorba and his friends then decide to teach Lucky how to fly. Lucky fails to successfully fly until Zorba decides to teach her to fly as a seagull mother would. For this Zorba asks Bubulinas owner, a little girl named Nina, to take them to a very high tower where Lucky could jump from the top and, according to her instincts, be able to fly. Lucky tells Zorba that she loves him, calling him by his name for the first time, and he tells her that he loves her, too. The plan succeeds and Lucky starts flying. The Great Big Rat sees the commotion and becomes enraged that his plan failed. Before she leaves she (Lucky) grabs YoYo and brings him to Zorba. Lucky then says good-bye to Zorba. YoYo calls Lucky his little sister, before Lucky gives her first seagull call and joins a flock of seagulls.

== Music == David Rhodes. The songs So volare and Canto di Kengah are sung by Spagna, Non sono un gatto by Leda Battisti, Siamo gatti by Samuele Bersani, Duro lavoro and Noi siamo topi by Gaetano Curreri and Antonio Albanese.

==Awards==
The film won a special Nastro dArgento and the audience award at the Montréal International Childrens Film Festival. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 