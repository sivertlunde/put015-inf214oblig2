Ved verdens ende
{{Infobox film
| name     = Ved verdens ende
| image    =
| director = Tomas Villum Jensen
| producer = 
| writer   = 
| starring = Nikolaj Lie Kaas   Birgitte Hjort Sørensen   Nikolaj Coster-Waldau
| cinematography = Jan Richter-Friis 
| music = 
| country = Denmark
| language = Danish
| runtime = 95 min
| released =  
}}

Ved verdens ende (At Worlds End) is a 2009 Danish action film directed by Tomas Villum Jensen.

== Cast ==
* Nikolaj Lie Kaas – Adrian Gabrielsen
* Birgitte Hjort Sørensen – Beate
* Nikolaj Coster-Waldau - Severin Geertsen 
* Nicolas Bro – Mikael Feldt
* Søren Pilmark – Consul
* Ulf Pilgaard – Werner Gabrielsen
* Birthe Neumann - Bitten Gabrielsen

== External links ==
* 

 
 


 