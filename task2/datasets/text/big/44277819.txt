Cuatro en la frontera
{{Infobox film
| name           =Cuatro en la frontera
| image          =Cuatro en la frontera.jpg
| image_size     =
| caption        =
| director       =Antonio Santillán
| producer       =Ignacio F. Iquino, Marius Lesoeur 
| writer         =Joaquín Algars and Nené Cascallar
| narrator       =
| starring       =
| music          =Ricardo Lamotte de Grignón
| cinematography =Pablo Ripoll
| editor       =Juan Luis Oliver 
| studio    =IFI Producción S.A., Sopadec
| released       = 5 May 1958
| runtime        =91 minutes
| country        = Spain Spanish
| budget         =
}}

Cuatro en la frontera is a 1958 Spanish film directed by Antonio Santillán.    Written
by Joaquín Algars and Nené Cascallar, it stars Claudine Dupuis, Danielle Godet, and Estanis González.

==Cast==
*Claudine Dupuis as Isabelle
*Danielle Godet as Olivia
*Estanis González as Estanis González 		
*Frank Latimore as Javier Miguel Ligero 
*Armando Moreno as Roca José Sancho 
*Adriano Rimoldi 			
*Julio Risca 		
*Gérard Tichy as Julio

==References==
 

==External links==
*  

 

 
 
 
 
 