Foolish (film)
 
{{Infobox film 
| name = Foolish
| image          = Foolish movie.jpg|
| image_size     =
| caption        = Theatrical release poster Dave Meyers
| producer       = Master P
| writer         = Master P
| starring       = Master P Eddie Griffin  Andrew Dice Clay  Amy Peterson Lisa Coleman Wendy Melvoin
| cinematography = Steve Gainer
| editing        = Anna Celada Chris Davis Artisan Pictures No Limit Films
| released       = September 1999
| runtime        = 97 minutes
| language       = English
| gross          = $6,033,999   
}}
 Dave Meyers No Limit Films second theatrical release after I Got the Hook Up.

==Plot==
Quentin "Fifty Dollah" Waise (Master P) is involved in a crime ring that earns him good money but worries his grandmother (Marla Gibbs), who dotes on him and encourages him to follow a more righteous path. Fifty Dollahs brother Miles "Foolish" Waise (Eddie Griffin), whose grandmother Odetta (Marla Gibbs) gave him the nickname, is an aspiring comedian, but his inability to get his career going convinces his older sibling hes wasting his talents. The movie pays homage to several of Griffins idols, such as Redd Foxx, Robin Harris and Sammy Davis, Jr. who appear as feet under restroom stalls while he prepares to perform. 

His idols inspire Foolish to do well in his shows, which are widely attended and scheduled last to keep the bar customers drinking, but he has home trouble with his girlfriend and their son, and after the death of his grandmother, seems unable either to keep a gig or to move on. Fifty Dollah tries to give him the push he needs and tries to get his own life in order, but is distracted both by problems with criminal leader Eldorado Ron (Andrew Dice Clay) and by a painful love triangle with his brother and the girl they both like (Amie) (Amy Petersen).

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Master P || Quentin "Fifty Dollah" Waise 
|-
| Eddie Griffin || Miles "Foolish" Waise 
|-
| Amy Petersen || Desiree
|-
| Frank Sivero || Giovanni 
|- Daphne Duplaix || Clarisse
|-
| Jonathan Banks || Numbers
|-
| Andrew Dice Clay || El Dorado Ron
|-
| Sven-Ole Thorsen || Paris
|-
| Marla Gibbs || Odetta Waise
|-
| Traci Bingham || Simone 
|-
| Bill Nunn || Jimmy Beck
|-
| Clifton Powell || Everette Washington
|-
| Anthony Johnson (actor)|A.J. Johnson || Himself
|-
| Traequon Tolbert || Miles Waise, Jr.
|}

==Soundtrack==
 
A soundtrack containing hip hop music was released on March 23, 1999 by No Limit Records. It peaked at #32 on the Billboard 200|Billboard 200 and #10 on the Top R&B/Hip-Hop Albums.

==References==

 

==External links==
* 
* 

 

 
 
 
 
 
 
 