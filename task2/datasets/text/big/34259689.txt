Manasukkul Mathappu
{{Infobox film
| name           = Manasukkul Mathappu
| image          = Manasukkul.jpg
| caption        =
| director       = Robert-Rajasekhar
| producer       = D. Saraswathi
| writer         = Prasanna Kumar
| story          = Priyadarshan
| screenplay     = Robert-Rajasekhar
| starring       = Prabhu Ganesan Saranya Ponvannan Sarath Babu Senthamarai
| music          = S.A. Rajkumar
| cinematography = Robert-Rajasekhar
| editing        = R.T. Annadurai
| distributor    = One Land Arts
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
}}
 Tamil drama One Flew Over the Cuckoos Nest.
 Mohanlal starrer Malayalam film Thalavattam, which was directed by Priyadarshan. 

==Plot==
Shekar (Prabhu Ganesan|Prabhu) becomes mentally ill after his girlfriend Anita (Lizy (actress)|Lizy) dies because of an electric short circuiting accident during a rock concert. Shekar is admitted into an institution managed by  Nagaraj (Senthamarai).

With the help of a young doctor Seema (Saranya Ponvannan), who is Nagarajs daughter, and an old friend Vijay (Sarath Babu), Shekar slowly regains his memory and mental equilibrium. He and Seema soon fall in love. But Nagaraj has already arranged Seemas marriage with someone else, so he opposes the lovers.

When Nagaraj finds that Seema and Shekar are adamant, he performs a surgery (lobotomy) on Shekar that puts him into a state of coma. Vijay feels that death would be preferable to vegetative life and kills Shekar. He confronts Nagaraj and confesses to the euthanasia. He then tries to kill Nagaraj for ruining Shekars life, but Seema suddenly kills her father before Vijay can, and she loses her mental equilibrium. She is admitted into the same institution as a patient.

==Cast==
*Prabhu Ganesan as Shekar
*Saranya Ponvannan as Seema
*Sarath Babu as Vijay Lizy as Anita
* Senthamarai as Nagaraj

==See also==
* Kyon Ki, Hindi remake of Thalavattam

==References==
 

 
 
 
 
 
 
 

 