Darling (1965 film)
 
 
{{Infobox film name            = Darling image           = Darling322.jpg image_size      = 225px caption         = film poster director        = John Schlesinger producer        = Joseph Janni writer          = Frederic Raphael starring        = Julie Christie Laurence Harvey Dirk Bogarde music           = John Dankworth cinematography  = Kenneth Higgins editing  James Clark distributor     = Anglo-Amalgamated released        =   runtime         = 128 minutes language        = English country         = United Kingdom budget          = £400,000  (estimated)  gross           = $12,000,000
}}

Darling is a 1965 British drama film written by Frederic Raphael, directed by John Schlesinger, and starring Julie Christie with Dirk Bogarde and Laurence Harvey.
 Best Picture Best Director. Best Original Best Costume Design.

==Plot==
Diana Scott (Julie Christie) is a beautiful, bored young model married to Tony Bridges (Trevor Bowen). One Day, Diana meets Robert Gold (Dirk Bogarde), a literary interviewer/director for television arts programmes, by chance when she is spotted on the street by his roving film crew and interviewed by him about young peoples views on convention. Diana is invited to watch the final edit in the TV studio and there their relationship starts. After liaisons in bleak hotel rooms they leave their spouses (and, in Roberts case, children) and move into an apartment.

As a couple, they become part of the fashionable London media/arts set. Initially, Diana is jealous when Robert sees his wife (Pauline Yates) while visiting his children, but she quickly loses this attachment when she mixes with the predatory males of the media, arts and advertising scene, particularly Miles Brand (Laurence Harvey), a powerful advertising executive for the "Glass Corporation" who gets her a part in a trashy thriller after she has sex with him. The bookish Robert prefers the quiet life; it is he who now becomes jealous, but increasingly detached, depressed and lonely.

Diana attends a high-class charity draw for world hunger for which she is the face. The event, adorned by giant images of African famine victims, is at the height of cynical hypocrisy and bad taste, showing Dianas rich white set, which now includes the establishment, playing at concern, gorging themselves, gambling and generally behaving decadently.

Already showing signs of stress from constantly maintaining the carefree look demanded by the false, empty lifestyle to which she has become a prisoner, Diana becomes pregnant, and has an abortion.

She flies to Paris with Miles for more jet-set sophistication. She finds the wild party, beat music, strip dance mind game, cross dressing and predatory males and females vaguely repellent and intimidating, but Diana holds her own, gaining the respect of the weird crowd when she taunts Miles in the game. On her return to London, Robert calls her a whore and leaves her, for which she is not emotionally prepared. Ironically, Miles casts her as "The Happiness Girl" in the Glass Corporations advertising campaign for a chocolate firm.

On location at a palazzo near Rome, Diana smiles in her medieval/Renaissance costume and completes "The Happiness Girl" shoot. She is much taken with the beauty of the building and the landscape and gets on well with the Prince, Cesare (José Luis de Vilallonga), who owns the palazzo. With the gay photographer Malcolm (Roland Curram) who has created her now famous look and who is the only person who has shown her any real understanding and friendship, Diana decides to stay on in Italy. They stay in a simple house by a small harbour in Capri. Diana flirts half-heartedly with Catholicism. They are visited by Cesare, who arrives in a huge launch, invites them on board and proposes to Diana. Cesare is widowed and has several children, the oldest of whom is about the same age as Diana. Diana politely declines his proposal, but Cesare leaves the offer open.

Diana returns to London, and still living in the flat she shared with Robert, has a party with Miles and other assorted media characters.. Robert has aged. Soon disillusioned with Miles and the vacuous London jet set, Diana flirts with the Catholic Church again. Impulsively, she flies out to Italy and marries the Prince, which proves to be ill-considered. Though waited on hand and foot by servants, she is almost immediately abandoned in the vast palazzo by Cesare, who has gone to Rome, presumably to visit a mistress.

Diana flees to London to Robert, who, taking advantage of her emotional vulnerability, charms her into bed and into what she thinks is a stable long-term relationship. In the morning, in self-disgust, he tells her that hes leaving her and that he fooled her only as an act of revenge. He reserves a flight to Rome, packs her into his car and takes her to Heathrow airport to send her back to her life as the Princess Della Romita. At the airport, Diana is hounded by the press, who address her reverentially as Princess. She boards the plane to leave.

==Cast==
* Julie Christie as Diana Scott
* Dirk Bogarde as Robert Gold
* Laurence Harvey as Miles Brand
* José Luis de Vilallonga as Prince Cesare della Romita
* Roland Curram as Malcolm
* Basil Henson as Alec Prosser-Jones
* Helen Lindsay as Felicity Prosser-Jones
* Marika Rivera as Paris party woman Alex Scott as Sean Martin
* Brian Wilde as Basil Willett
* Pauline Yates as Estelle Gold
* Trevor Bowen as Tony Bridges, first husband
* Carlo Palmucci as Curzio
* Peter Bayliss as Lord Alex Grant
* Georgina Cookson as Carlotta Hale
* Hugo Dyson as Walter Southgate
* Tyler Butterworth as William Prosser-Jones
* Vernon Dobtcheff as Christopher Greatorex, art critic
* James Cossins as Basildon, charity MC
* Jane Downs as Julie
* Zakes Mokae as Black Man at Party
* John Woodvine as Customs Officer

==Production and reputation==
 , and   in London.  
 New York mod fashion David Thomson writes that the film "deserves a place in every archive to show how rapidly modishness withers. Beauty is central to the cinema and Schlesinger seems an unreliable judge of it, over-rating Christie and rarely getting close enough to the action to make a fruitful stylistic bond with it".  Leonard Maltins Film Guide describes it as a "trendy, influential 60s film – in flashy form and cynical content".  Tony Rayns though, in the Time Out Film Guide, is as damning as Thomson. For him, the film is a "leaden rehash of ideas from Godard, Antonioni and Bergman", although with nods to the "Royal Court school", which "now looks grotesquely pretentious and out of touch with the realities of the life-styles that it purports to represent." 

==Box office== theatrical rentals. 

==Awards and honours==
{| class="wikitable sortable" style="font-size: 90%;"
|-
| Group|| Award|| Recipient|| Result
|- 38th Academy Awards| Academy Awards||  Academy Award for Best Actress || Julie Christie ||  
|- Julie Harris ||  
|-
| Academy Award for Writing Original Screenplay || Frederic Raphael ||  
|-
| Academy Award for Best Picture || Joseph Janni ||  
|-
| Academy Award for Best Director || John Schlesinger ||  
|- 19th British BAFTA Awards|| BAFTA Award for Best British Actress || Julie Christie ||  
|-
| BAFTA Award for Best Actor in a Leading Role| BAFTA Award for Best British Actor || Dirk Bogarde ||  
|-
| BAFTA Award for Best Production Design| BAFTA Award for Best British Art Direction || Ray Simm ||  
|-
| BAFTA Award for Best Screenplay| BAFTA Award for Best British Screenplay || Frederic Raphael ||  
|- BAFTA Award for Best Film | BAFTA Award for Best British Film || John Schlesinger ||  
|-
| BAFTA Award for Best Cinematography || Kenneth Higgins ||  
|- 23rd Golden Globe Awards| Golden Globe Awards || Golden Globe Award for Best English - Language Foreign Film ||  ||  
|- Golden Globe Award for Best Director || John Schlesinger ||  
|- Golden Globe Award for Best Actress - Motion Picture Drama || Julie Christie ||  
|- 18th Directors Guild of America Awards| Directors Guild of America || Directors Guild of America Award for Outstanding Directing - Feature Film || John Schlesinger ||  
|- Laurel Awards || Laurel Awards| Laurel Award for Top Female Dramatic Performance || Julie Christie ||  
|- Mexican Cinema  Journalists || Silver Goddess for Best Foreign Actress || Julie Christie ||  
|-
| 4th Moscow International Film Festival| Moscow International Film Festival || 4th Moscow International Film Festival| Moscow International Film Festival Grand Prix || John Schlesinger ||  
|- National Board of Review Awards 1965| National Board of Review || National Board of Review Award for Best Director || John Schlesinger ||  
|- National Board of Review Award for Best Actress || Julie Christie ||  
|- National Board of Review Award for Best Film ||  ||  
|- 1965 New York Film Critics Circle Awards| New York Film Critics Circle || New York Film Critics Circle Award for Best Film || ||  
|- New York Film Critics Circle Award for Best Director || John Schlesinger ||  
|- New York Film Critics Circle Award for Best Actress || Julie Christie ||  
|-
| rowspan="2"| Writers Guild of Great Britain || Writers Guild of Great Britain| Writers Guild of Great Britain Award for Best British Comedy Screenplay || Frederic Raphael ||  
|-
|Writers Guild of Great Britain| Writers Guild of Great Britain Award for Outstanding British Original Screenplay || Frederic Raphael ||  
|-
|}
Source:  

==See also==
* BFI Top 100 British films

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 