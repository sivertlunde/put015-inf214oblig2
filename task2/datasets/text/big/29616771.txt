Kallan Pavithran
{{Infobox film name           = Kallan Pavithran image          = director       = P. Padmarajan writer         = P. Padmarajan starring       = Nedumudi Venu Adoor Bhasi Bharath Gopi producer       = M. Mani music  Shyam
|cinematography = Vipin Das editor         = Madhu Kainakari studio         = Sunitha Productions distributor    = released       =   runtime        = country        = India language       = Malayalam gross          =
}}
Kallan Pavithran ( ,  ) is a 1981 Malayalam film written and directed by P. Padmarajan. It stars Nedumudi Venu, Adoor Bhasi and Bharath Gopi. The film is about a thief who meets a crooked merchant in the town and an incident occurring afterward changing his entire life. The film is based on Padmarajans pre-published short story of the same name.This was also the first super hit film of Padmarajan as a director.

==Plot==
The profession Pavithran is involved in earned him the title ‘Pavitran the Thief’, even if the thefts he gets involved were of petty nature. It was as the result of Pavithran’s effort to get rid of the title he had with his name that he visited Mamachan the mill owner. Pavithran’s request for a job in the mill was turned down by Mamachan. But a theft that occurs in Mamachan’s house changes the fate of Pavithran’s life forever.

==Cast==
* Nedumudi Venu as Pavithran
* Adoor Bhasi as Merchant / Trader
* Bharath Gopi as Maamachan
* Beena as Damayanthi
* Subhashini as Damayanthis sister
* Devi as Pavithrans first wife
* Prem Prakash as Taxi Driver
* Bhaskara Kurup as Sub-Inspector 
== External links ==
*  

 

 
 
 
 
 


 