In the Middle of the Night (film)
{{Infobox film
| name     = In the Middle of the Night
| image    =
| director = Erik Balling
| producer = 
| writer   = Kim Larsen Henning Bahs Erik Balling
| starring = Kim Larsen   Erik Clausen
| cinematography =
| music = 
| country = Denmark
| language = Danish
| runtime = 131 min
| released =  
}}

In the Middle of the Night ( ) is a 1984 Danish drama film directed by Erik Balling.

== Cast ==
* Kim Larsen - Benny
* Erik Clausen - Arnold Jensen
* Birgitte Raaberg - Susan Himmelblå
* Holger Boland - Tusindfryd
* Buster Larsen - Charles
* Frits Helmuth - J.O. Kurtzen
* Poul Bundgaard - Kai Buhmann
* Judy Gringer - Rita
* Ove Sprogøe - Susans father

== External links ==
* 
* 

 
 


 