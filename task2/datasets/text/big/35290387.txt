City of My Dreams (film)
 
{{Infobox film
| name           = City of My Dreams
| image          = 
| caption        = 
| director       = Ingvar Skogsberg
| producer       = Olle Hellbom Olle Nordemar
| writer         = Per Anders Fogelström Ingvar Skogsberg
| starring       = Eddie Axberg
| music          = 
| cinematography = John Olsson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 168 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Eddie Axberg as Henning Nilsson
* Britt-Louise Tillbom as Lotten Blom
* Kjell-Hugo Grandin as Tummen
* Gunilla Larsson as Matilda
* Åke Wästersjö as Skräcken
* Märta Dorff as Johanna
* Berit Gustafsson as Malin Peter Lindgren as Storsäcken
* Fylgia Zadig as Storsäckens Wife
* Mona Seilitz as Storsäckens Daughter
* Gertrud Widerberg as Augusta

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 