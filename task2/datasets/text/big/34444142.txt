War Witch
 
 
{{Infobox film
| name           = War Witch
| image          = Rebelle (2012 film).jpg
| caption        = Film poster
| director       = Kim Nguyen Pierre Even Marie-Claude Poulin Kim Nguyen
| writer         = Kim Nguyen
| starring       =  
| music          = 
| cinematography = Nicolas Bolduc
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Canada
| language       = French, Lingala
| budget         = 
| gross          = $70,544   
}}

War Witch ( ) is a 2012 Canadian drama film written and directed by Kim Nguyen. It was primarily filmed in the Democratic Republic of the Congo.   The film was in competition for the Golden Bear at the 62nd Berlin International Film Festival in February 2012.          At Berlin, Rachel Mwanza won the Silver Bear for Best Actress.    She also won the award for Best Actress at the 2012 Tribeca Film Festival.   

The film was Canadas entry in the Best Foreign Language Film category at the 85th Academy Awards,        and became one of the five nominees.    Mwanza was granted a visa to allow her to attend the Academy Awards.   
 Best Picture, Best Director, Best Actress Best Supporting Outstanding International Motion Picture.   

==Plot==
During a civil war in sub-saharan Africa, a 12-year-old girl named Komona is abducted by rebel forces to become a child soldier. She is given a gun and told to choose between shooting her parents, who huddle in front of her, or watching them come to a much more painful end via machete. With tears in her eyes, she shoots them. Then, with other captives, she is whisked by motorised canoe up the river and led into a forest where training exercises immediately commence. New recruits are regularly beaten and face near-starvation.

Komonas salvation is her imagination. Stimulated by “magic milk,” a hallucinogen found in sap, she has visions of ghosts in the trees (actors in white body paint, their eyes blank), including those of her parents, who warn her of the enemy’s proximity. She narrowly escapes an ambush after the ghosts alert her to danger.

Word of her supernatural gifts gets back to the Great Tiger (Mizinga Mwinga), who summons her to his side and dubs her his protective “war witch.”  It is here that Komona learns of Coltan mining as the primary source of funds backing the war lord.

With her only friend, Magician (Serge Kanyinda), a slightly older albino boy, who introduced her to the sap, she flees the rebel army. The films middle section is an idyll during which Magician asks Komona to marry him. She refuses unless he catches her a white rooster, and his comical quest for this elusive prize, which is reputed not to exist, is a respite from the rest of the film’s horrors.

For a time, the couple stay with Magician’s uncle, Butcher (Ralph Prosper), who witnessed acts against his family that were so barbarous that Komona refuses to describe them. This peaceful section is buoyed by sparkling fragments of African pop music.

Their euphoria is short-lived. The Great Tiger, needing his war witch, dispatches his soldiers to find Komona, and she is dragged back into the forest, where she becomes the sexual slave of a hateful commander. She wreaks an excruciating revenge via castration.

Komonas ultimate desire is to return to her village and bury her parents’ remains so that her child, by the Commander but named after Magician, will not grow up cursed.

==Cast==
* Rachel Mwanza as Komona
* Alain Lino Mic Eli Bastien as Commandant-rebelle
* Serge Kanyinda as Magicien
* Mizinga Mwinga as Grand Tigre Royal
* Ralph Prosper as Boucher
* Jean Kabuya as School camp coach
* Jupiter Bokondji as Royal Tiger sorcerer
* Starlette Mathata as Komonas mother
* Alex Herabo as Komonas father
* Dole Malalou as Coltan dealer
* Karim Bamaraki as Biker

==Accolades==
{| class="wikitable" rowspan=5; style="text-align: center; background:#ffffff;"
!Year!!Association!!Category!!Recipient(s)!!Result!!Ref.
|- 45th NAACP NAACP Image 45th NAACP Outstanding International Motion Picture||War Witch|| || 
|}

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Canadian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 