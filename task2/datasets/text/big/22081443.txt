The Dumbfounded King
{{Infobox film
| name           = El rey pasmado
| image          = EL REY PASMADO.jpg
| image size     =
| alt            = 
| caption        = Film poster
| director       = Imanol Uribe
| producer       = 
| writer         = Joan Potau Gonzalo Torrente Ballester
| narrator       = 
| starring       = Gabino Diego Juan Diego Maria Barranco Javier Garruchaga
| music          = José Nieto (composer)|José Nieto
| cinematography = 
| editing        = Teresa Font
| studio         = 
| distributor    = 
| released       = 1 November 1991
| runtime        = 106 minutes
| country        = Spain/France/Portugal Spanish
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Spanish comedy-historical film directed by Imanol Uribe and written by Joan Potau and Gonzalo Torrente Ballester. The screenplay was based on Torrentes novel Crónica del rey pasmado.

==Cast==
*Gabino Diego as The King
*Laura del Sol as Marfisa
*Joaquim de Almeida as Father Almeida
*María Barranco as Lucrecia Juan Diego as Father Villaescusa
*Fernando Fernán Gómez as Grand Inquisitor
*Javier Gurruchaga as Valido
*Anne Roussel as The Queen
*Carme Elías as Abesse

==Plot== Philip IV Elisabeth of France (Anne Roussel)
 monastery of San Plácido and achieves his goal.

Meanwhile, the Count-Duke of Olivares (Javier Gurruchaga) fears that he could being punished by God because he fails to have children with his wife, so he gets advice from Villaescusa, who informs him that the pleasure he and his wife obtain when performing the sexual act is to be blamed for the infertility. The "divinely inspired" solution proposed by Villaescusa is that the Earl and his wife copulate in the choir of the church of San Plácido (where by coincidence are also very nearly the kings) in front of the choir nuns. At the end of this sexual encounter the Count-Duke of Olivares receives two letters which informs him of the successful arrival of the Indian fleet to Cadiz and the victory of Spanish troops in Flanders. Villaescusa says that the happy ending is due to the sacrifices they all have passed, but the Count Duke replies that by the date of the letters it could be seen that the fleet had arrived in Cadiz two days ago "just the day King went whores". The Count-Duke sends Villaescusa to Rome with a sealed letter asking to not let him go until he has changed his attitude.

==Critic== Carlos Aguilar in his Guía del cine español says that the film has some flaws, such as the different treatment of historical characters, but these are outweighed by good realization and a good cast.  Critics also noted the close resemblance of Gabino Diego and Gurruchaga with Philip IV and the Count-Duke of Olivares respectively. 
 Renaissance palace of the Marquis of Santa Cruz in Viso del Marqués (Ciudad Real) and the Monastery of Uclés (Cuenca).

==Awards and nominations==

===Won=== Goya Awards
**Best Actor in a Supporting Role (Juan Diego)
**Best Costume Design (Javier Artiñano)
**Best Makeup and Hairstyles (Romana González and Josefa Morales) Best Original Score (José Nieto)
**Best Production Design (Félix Murcia)
**Best Production Supervision (Andrés Santana)
**Best Screenplay &ndash; Adapted (Joan Potau and Gonzalo Torrente Malvido)
**Best Sound (Ricard Casals and Gilles Ortion)

===Nominated=== Goya Awards
**Best Actor in a Leading Role (Gabino Diego)
**Best Actor in a Supporting Role (Javier Gurruchaga)
**Best Actress in a Supporting Role (María Barranco) Best Cinematography (Hans Burman) Best Director (Imanol Uribe) Best Film

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 