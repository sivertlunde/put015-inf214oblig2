Sons of the Desert
 
{{Infobox film
| name           = Sons of the Desert
| image          = L&H_Sons_of_the_Desert_1933.jpg
| caption        = theatrical release poster
| director       = William A. Seiter
| producer       = Hal Roach
| story          = Frank Craven
| screenplay     = Byron Morgan
| starring       = Stan Laurel Oliver Hardy Charley Chase Mae Busch
| music          = William Axt George M. Cohan Marvin Hatley Paul Marquardt ODonnell-Heath Leroy Shield Frank Terry
| cinematography = Kenneth Peach
| editing        = Bert Jordan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 64 46"
| language       = English
| country        = United States
| budget         = 
}}

Sons of the Desert is a 1933 American film starring Laurel and Hardy, and directed by William A. Seiter. It was first released in the United States on December 29, 1933 and is regarded as one of Laurel and Hardys greatest films. In the United Kingdom, the film was originally released under the title Fraternally Yours.

In 2012, the film was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry. 

== Plot ==
The film begins at a meeting of the Sons of the Desert, a fraternal lodge (styled to resemble the Ancient Arabic Order of the Nobles of the Mystic Shrine|Shriners) of which both Stan Laurel and Oliver Hardy are members. The organization will be holding its annual convention in Chicago in a week and all members have to take an oath to attend. Stan is reluctant to take the oath, but Oliver goads him into it. Later, on the way home, Stan explains to Oliver his reluctance to take the oath; he is worried that his wife will not let him go to the convention. Oliver tries to reassure Stan his wife has no choice but to let him go because he took a sacred oath. When they get home and Stan accidentally brings up the subject of the convention, however, it turns out Olivers wife will not let him go as they had already arranged a mountain trip together (which Oliver had forgotten about). Olivers attempts to exert his authority over his wife only results in him getting several pieces of crockery thrown at him.

Unwilling to go back on the oath that he swore, Oliver feigns illness to get out of the trip with his wife. Stan arranges for a doctor (actually a veterinarian) to prescribe an ocean voyage to Honolulu, with their wives staying home (Oliver is well aware how much ocean voyages disagree with his wife). Stan and Ollie go to the convention, with their wives none the wiser. Of course, they have a close call while drinking with a fellow conventioneer when as a practical joke their friend "Charley" calls his sister in Los Angeles who turns out to be Mrs. Hardy! However, nothing comes of this. But then, as usual in such a film, fate cruelly closes in. While Stan and Ollie are en route home from Chicago, their supposed ship arriving from Honolulu sinks in a typhoon and the wives head to the shipping companys offices to find out any news about the survivors. Stan and Ollie discover what has happened only after returning home and looking at the newspaper while their wives were out.
 cinema to calm their rattled nerves...and what should they see but a newsreel of the convention in which their husbands are prominently featured! Furious at being deceived, they challenge one another to test the character of their husbands, to see whose is the truthful one. As for the husbands, their camping in the attic is interrupted loudly enough as to attract the attention of the wives and they manage to flee out of sight, escaping to the rooftop. Stan wants to go back home and confess to his wife but Oliver threatens, "If you go downstairs and spill the beans, Ill tell Betty that I caught you smoking a cigarette!"

They are about to make their way to a hotel to spend the night, but are stopped by a policeman who manages to get their home addresses from them thanks to Stan. Upon walking into the house, they tell the wives about the shipwreck. Then, when asked about how the pair of them had managed to get home a day before the rescue ship carrying the survivors was due, their story begins to unravel; they say they jumped ship and "hitch-hiking|ship-hiked" their way home. Ollie still insists his story is true; "Its too farfetched not to be the truth!" But Stan eventually breaks down and tearfully confesses, despite Olivers previously mentioned threat. Stan tells his wife about the smoking too, and for his honesty ends up wrapped in her dressing gown on the sofa, sipping wine and eating chocolates, despite having been marched out of Ollies house at gunpoint. Olivers wife proceeds to break all the crockery over his head. This battle is heard, but not actually shown. Stan returns from next-door to compare notes, sees Hardy sitting in the wreckage and tells Ollie that his wife said that "honesty is the best politics!" Stan puffs on a once-forbidden cigarette, and then goes out the door singing "Honolulu Baby". Ollie vengefully hurls a pot at his head, upending him. 

==Cast==
* Stan Laurel as Stanley
* Oliver Hardy as Oliver
* Charley Chase as Charley
* Mae Busch as Mrs. Hardy
* Dorothy Christy as Mrs. Laurel
* Lucien Littlefield as the Veterinarian
* Ty Parvis as the sailor in Honolulu Baby song and dance 
* Charita as the lead Hawaiian hula dancer (Charita Alden) 

==Soundtrack==
* "Honolulu Baby" – written by Marvin Hatley|T. Marvin Hatley 
* "Auld Lang Syne" – written by Robert Burns
* "We Are the Sons of the Desert" – written by Marvin Hatley 
* "Tramp! Tramp! Tramp!" – music and lyrics by George Frederick Root

==Awards and honors==
American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs – #96
* 2005: AFIs 100 Years... 100 Movie Quotes:
** #60 – "Well, heres another nice mess youve gotten me into!"

===Nominations===
* 1998: AFIs 100 Years...100 Movies
* 2007: AFIs 100 Years...100 Movies (10th Anniversary Edition)

==Similar films==
* Sons of the Desert is a partial remake of 1928s We Faw Down.
* Another Laurel and Hardy Short film called Be Big! is very much the same storyline as this feature. Stan and Ollie are invited to a party with a club that the two attend. However, they are about to take their wives on holiday for the weekend. Ollie pretends to be ill and makes sure the wives still go, the boys will meet them there the next day, and Stan stays to look after him. They get changed into their uniforms which causes many problems such as Stan putting on Ollies boots by mistake. However, in Be Big, Stan is initially unaware of the party at their club until Ollie fills him in, and then tells him his sick act was a charade.

==Legacy==
The international Laurel and Hardy society The Sons of the Desert takes its name from this feature film. The title was also used as the name of a country group and the Danish comedy quartet "Ørkenens sønner", the literal translation of the movies title. The comedy group uses the basic theme of a fraternal organization, and their stage costumes are identical to the ones used in the movies organization. Even their theme song is a translation of the one from the movie. 

Furthermore, the movies plot, itself recycled and revised from We Faw Down and Be Big!, may very well have been the inspiration for The Cosby Show episode "Off to See The Wretched". Like Stan and Ollie, Vanessa and her friends finagle a long-distance road trip to a rock concert fooling their parents, but everything that can go wrong does, including a twist of fate that destroys their alibi (a fire right next door to one of Vanessas friends house where they were supposed to be staying), and the parents eventually discover the real truth.

Another example was an inspiration for The Flintstones episode "The Buffalo Convention". Like Stan & Ollie, Fred & Barney fake an illness ("dipsy-doodle-itis") in which the doctor (whos really a plumber) can send them off by themselves, away from their wives. Unfortunately, a so-called "talking dodo bird" that Fred gave Wilma as a present, a pet, proves to really be a talking bird...who knows everything about Fred and Barneys ploy, and so must also be a stoolpigeon! They get rid of the bird and go to their convention in Frantic City, but the wives happen to find the bird...and discover it really can talk, spilling the beans about Fred and Barneys game! This was a running gag—other Flintstone Episodes of Fred and Barney faking illness to get away from their wives-and failing as usual—are "The Flintstone Flyer" & "Peek A Boo camera"

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 