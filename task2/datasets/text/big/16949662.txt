Comme elle respire
 
{{Infobox film
| name           = ...Comme elle respire
| image          =
| image_size     =
| caption        =
| director       = Pierre Salvadori
| producer       = Philippe Martin Gérard Louvin (co-producer)
| writer         = Danièle Dubroux  Pierre Salvadori Marc Syrigas
| narrator       =
| starring       =
| music          = Camille Bazbaz
| cinematography = Gilles Henry
| editing        = Hélène Viard
| studio         =
| distributor    = Les Films du Losange
| released       =  
| runtime        = 106 minutes
| country        = France
| language       = French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
...Comme elle respire (La Menteuse est amoureuse, English: White Lies)  is a French comedy film directed by Pierre Salvadori, released in French films of 1998|1998.

==Synopsis==
Is mythomania an evil trait, or a sickness? Jeanne, played by Marie Trintignant, can never tell the truth for more than two minutes. She fears that reality is too much for her. And Guillaume Depardieu, who plays the role of her lover, discovers her mythomania, but stays in love with her.

==Technical detail==
* Year of production : 1997

==Starring==
* Marie Trintignant :  Jeanne
* Guillaume Depardieu : Antoine
* Jean-François Stévenin : Marcel

==External links==
 
*  
*  
*   at AlloCiné  

 

 
 
 
 
 
 


 
 