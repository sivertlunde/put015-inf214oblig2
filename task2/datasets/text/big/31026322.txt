El río y la muerte
{{Infobox film
| name           =El río y la muerte
| image          = El Rio y la muerte poster.jpg
| image size     =
| caption        =
| director       = Luis Buñuel
| producer       = J. Ramón Aguirre, Armando Orive Alba	
| writer         = Luis Alcoriza,  Luis Alcoriza
| narrator       = Jaime Fernández
| music          = Raúl Lavista
| cinematography = Raúl Martínez Solares
| editing        = Jorge Bustos	 
| distributor    = 
| released       = 30 June 1955 (Mexico) 
| runtime        = 91 min
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1955 Mexico|Mexican film. It was written by Luis Alcoriza and directed by Luis Buñuel.

==Cast==

* 	 Columba Domínguez	 ...	Mercedes
* 	 Miguel Torruco	 ...	Felipe Anguiano
* 	 Joaquín Cordero	 ...	Gerardo Anguiano Jaime Fernández	 ...	Romulo Menchaca
* 	 Víctor Alcocer	 ...	Polo Menchaca
* 	 Silvia Derbez	 ...	Elsa
* 	 José Elías Moreno	 ...	Don Nemesio
* 	 Carlos Martínez Baena	 ...	Don Julian
* 	 Alfredo Varela	 ...	Chinelas (as Alfredo Varela Jr.)
* 	 Miguel Manzano	 ...	don Anselmo
* 	 Manuel Dondé	 ...	Zósimo Anguiano
* 	 Jorge Arriaga	 ...	Filogonio Menchaca
* 	 Roberto Meyer	 ...	Doctor
* 	 Chel López	 ...	El asesino
* 	 José Muñoz (actor)|José Muñoz	 ...	don Honorio


==External links==
*  

 

 
 
 
 
 
 

 