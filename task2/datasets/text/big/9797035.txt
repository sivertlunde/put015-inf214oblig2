When the Heart Sings
{{Infobox film
| name           = When the Heart Sings
| image          =
| image_size     =
| caption        = Richard Harlan
| producer       = 
| writer         = Juan José Piñeiro   Rafael Solana    Rodolfo M. Taboada
| narrator       =
| starring       = Hugo del Carril   Aída Luz   José Olarra   Felisa Mary
| music          =Alejandro Gutiérrez del Barrio 
| cinematography =Roque Funes
| editor       =
| studio        = Establecimientos Filmadores Argentinos 
| distributor    =Establecimientos Filmadores Argentinos 
| released       = 6 August 1941
| runtime        = 82 minutes
| country        = Argentina Spanish
| budget         =
}} musical drama film directed by  Richard Harlan (director)| Richard Harlan and starring  Hugo del Carril, Aída Luz and José Olarra.  A man from a wealthy background meets and marries an actress despite fierce opposition from his family.

==Cast==
* Hugo del Carril as Martín
* Aída Luz as Lucy
* José Olarra as Don Olegario
* Felisa Mary as Doña Angélica
* Oscar Valicelli as Pedrito
* Adrián Cúneo as Coco
* María Esther Gamas as Emma
* Julio Scarcella as Di Paula
* Vicky Astori as Gloria Norton
* Eva Guerrero as Mangacha
* Joaquín Petrosino as Juancho
* Fausto Fornoni as Garrido
* Agustín Barrios (actor)|Agustín Barrios as Federico
* Julio Renato as El franciscano
* Alberto Terrones as Roncales
* King Wallace as El cuidador
* Emilio Fuentes as El ilusionista
* Warly Ceriani

== References ==
 

== Bibliography ==
* Rist, Peter H. Historical Dictionary of South American Cinema. Rowman & Littlefield, 2014. 

==External links==
*  

 
 
 
 
 
 
 
 
 
  

 