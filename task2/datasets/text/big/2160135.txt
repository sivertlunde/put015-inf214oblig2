Tombstone, the Town Too Tough to Die
{{Infobox film
| name           = Tombstone, the Town Too Tough to Die
| caption        =
| image	         = Tombstone, the Town Too Tough to Die FilmPoster.jpeg
| director       = William C. McGann
| producer       = Harry Sherman
| writer         = Albert S. Le Vino (screenplay) Edward E. Paramore Jr. (screenplay) Charles Reisner (story) Dean Riesner (story) Richard Dix Edgar Buchanan Frances Gifford Don Castle Rex Bell
| music          = Gerard Carbonara
| cinematography =
| editing        = Carroll Lewis Sherman A. Rose
| distributor    = Paramount Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| awards         = English
| budget         =
}}
 Western film Richard Dix gunfight at the OK Corral also features Rex Bell as Virgil Earp and Victor Jory as Ike Clanton.

==Plot==
Set in the legendary town of Tombstone, Arizona, the plot centers on former gunslinger Wyatt Earp, who helps the sheriff round up criminals. Earp becomes a lawman after he sees an outlaw accidentally kill a child during a showdown. Earps brothers and Doc Holliday help him take on the outlaw and his gang. More trouble ensues when the sheriff becomes involved with the gang. Earp manages to get them on robbery charges and the situation finally culminates at the infamous Gunfight at the O.K. Corral|O.K. Corral.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|- Richard Dix || Wyatt Earp
|-
| Edgar Buchanan || Curly Bill Brocious
|-
| Frances Gifford || Ruth Grant
|-
| Don Castle || Johnny Duanne
|-
| Rex Bell || Virgil Earp
|-
| Kent Taylor || Doc Holliday
|-
| Clem Bevans || Tadpole Foster
|-
| Victor Jory || Ike Clanton
|}

==External links==
*  

 
 
 
 
 
 
 
 


 