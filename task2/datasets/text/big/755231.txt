Mo' Better Blues
{{Infobox film
| name = Mo Better Blues
| image = Mo Better Blues; Movie poster.jpg
| caption = Theatrical release poster
| director = Spike Lee
| producer = Spike Lee
| writer = Spike Lee
| starring = Denzel Washington Spike Lee Wesley Snipes Giancarlo Esposito Robin Harris Joie Lee Bill Nunn John Turturro Dick Anthony Williams Cynda Williams Bill Lee
| cinematography = Ernest Dickerson
| editing = Samuel D. Pollard
| studio = 40 Acres and a Mule Filmworks Universal Pictures
| released =  
| runtime = 129 minutes
| country = United States
| language = English
| budget = $10 million
| gross = $16,153,600 (USA) (sub-total)
}}
 musical comedy-drama Malcolm X, He Got Game and Inside Man). The film was released five months after the death of Robin Harris and is dedicated to his memory.

==Plot==
The film begins with a scene set in Brooklyn, New York in 1969.  A group of four boys walk up to Bleek Gilliams brownstone and ask him to come out and play baseball with them. Bleeks mother insists that he continue his trumpet lesson, to his chagrin.   His father becomes concerned that Bleek will grow up to be a sissy, and a family argument ensues.  In the end, Bleek continues playing his trumpet, and his friends go away.

The next scene brings us to the present (over twenty years later), with an adult Bleek (Denzel Washington) performing on the trumpet at a busy nightclub with his jazz band, The Bleek Quintet (Jeff "Tain" Watts, Wesley Snipes, Giancarlo Esposito and Bill Nunn). Giant (Spike Lee, one of his boyhood friends from the previous scene and current manager of Bleeks band), is waiting in the wings, and advises him to stop allowing his saxophone player Shadow Henderson (Snipes) to grandstand with long solos.

The next morning Bleek wakes up with his girlfriend, Indigo Downes (Joie Lee). She leaves to go to class, while he meets his father by the Brooklyn Bridge for a game of catch, telling him that while he does like Indigo, he likes other women too and is not ready to make a commitment. Later in the day while he is practicing his trumpet, another woman named Clarke Bentancourt (Cynda Williams) visits him. She suggests he fire Giant as his manager; he suggests that they make love (which he refers to as "mo’ better"). She bites his lip and he becomes upset about it, saying, "I make my living with my lips", as he examines the bleeding bottom lip.

Giant is with his bookie, betting on baseball. Then he goes to the nightclub and argues with the doormen about what time to let the patrons into the club. He meets Bleek inside with the rest of the band, except for the pianist, Left Hand Lacey (Esposito), who arrives late with his French girlfriend and is scolded by Giant. Later Giant goes to the club owners’ office, points out how busy the club has been since Bleek and his band began playing there, and unsuccessfully attempts to renegotiate their contract.

Giant meets his bookie (Rubén Blades) the next morning, who is concerned that Giant is going too deep into debt. Giant shrugs it off, and places several new bets. He then stops off at Shadows home to drop off a record. Shadow confides in him that he is cheating on his girlfriend. This leads to the next scene where Bleek is in bed with Clarke, and she asks him to let her sing a number at the club with his band, to which he declines.

Bleek and Giant are fending off requests from the other members of the band, especially Shadow, for a raise due to the bands success at the club. Bleek goes to the club owners to see about more money, which they refuse, reminding him that it was Giant who locked him into the current deal.

That night at the club, both Clarke and Indigo arrive at the club to see Bleek. They are wearing the same style dress, which Bleek had purchased for them both. Bleek attempts to work it out with each girl, but they are both upset with him over the dresses, and though he sleeps with them each again they leave him (after he calls each of them by the others name). However, tension rises with Shadow, who has feelings for Clarke.

Bleek and Giant go for a bike ride, where Bleek insists to Giant that he do a better job managing and bringing in money. Giant promises to do so, then asks Bleek for a loan to pay off his gambling debt. Bleek declines, and later Giant is apprehended by two loan sharks (Samuel L. Jackson and Leonard L. Thomas) who demand payment. Giant can’t pay and gets his fingers broken. Later Giant tells Bleek that he fell off his bike on the ride home, but Bleek does not believe him. Giant asks the other band members for money and Left loans him five hundred dollars. When loan sharks stake out Giants home, he goes to Bleek for a place to stay. Bleek agrees to help him raise the money, but fires him as manager.

Bleek misses both his girlfriends, leaving messages for each, but Clarke has begun a new relationship with Shadow. Bleek finds out about it, and fires Shadow after the two fight backstage before a gig. The loan sharks track Giant down at the club before Bleek can come up with the money, take him outside and beat him while Bleek plays. Bleek goes outside to intervene, and gets beaten as well.  Additionally, one loan shark (Samuel L. Jackson) takes Bleeks own trumpet, and smacks him across the face with it.  This not only puts Bleek in the hospital, but it also permanently injures his lip, making him unable to continue playing trumpet.

Months later, Bleek reunites with Giant, who has gotten a job as a doorman and stopped gambling. He drops in to see Shadow and Clarke, who are now performing together with the rest of Bleeks former band. Shadow invites him on stage, and they play together. Bleek still has scars to his lips, and is unable to play correctly. He walks off the stage, gives his trumpet to Giant, and goes directly to Indigos house. She is angry with him because she hasn’t heard from him in over a year. She tries to reject him, but agrees to take him back when he begs her to save his life.

A montage flashes through their wedding, the birth of their son, Miles, and Bleek teaching his son to play the trumpet.  In the final scene of the movie, Miles is ten years old, and wants to go outside to play with his friends.   Indigo wants him to finish practicing his trumpet lessons.  However, unlike the (almost identical) opening scene in the beginning of the film, Bleek relents and allows his son to leave and play with friends. This final scene uses exactly the same dialogue as the first, with changes only in the delivery of the dialogue. A major part of the films themes is cause and effect and fate. In letting Miles stop practicing which Bleek never did, then maybe his life will lead to an alternate, and ultimately happier, conclusion.

==Cast==
* Denzel Washington — Bleek Gilliam
* Spike Lee — Giant
* Wesley Snipes — Shadow Henderson
* Joie Lee — Indigo Downes
* Cynda Williams — Clarke Bentancourt
* Giancarlo Esposito — Left Hand Lacey
* Bill Nunn — Bottom Hammer
* Jeff "Tain" Watts — Rhythm Jones
* Dick Anthony Williams — Big Stop Williams
* John Turturro — Moe Flatbush
* Nicholas Turturro — Josh Flatbush
* Robin Harris — Butterbean Jones
* Samuel L. Jackson — Madlock
* Leonard L. Thomas — Rod
* Charlie Murphy — Eggy Coati Mundi — Roberto

==Soundtrack==
{{Infobox album  
| Name        = Mo Better Blues
| Type        = soundtrack
| Artist      = Branford Marsalis Quartet and Terence Blanchard
| Cover       = Mo Better Blues; Soundtrack cover.jpg
| Released    = 31 July 1990
| Recorded    =
| Genre       = Jazz
| Length      = 37:20
| Label       = Sony Music
| Producer    =
}}
The soundtrack to the film was composed and played by Branford Marsalis Quartet and Terence Blanchard.

In 1991, the soundtrack was nominated for a Soul Train Music Award for Best Jazz Album, but lost to saxophonist Najee for "Tokyo Blue".

# "Harlem Blues" (vocals: Cynda Williams) 4:50
# "Say Hey" 3:18
# "Knocked Out the Box" 1:35
# "Again, Never" 3:54
# "Mo Better Blues" 3:40
# "Pop Top 40"	(vocals: Denzel Washington, Wesley Snipes) 5:40
# "Beneath the Underdog" 5:07 Gang Starr, Kenny Kirkland, Robert Hurst) 4:48
# Harlem Blues (Acapulco version)" (vocals: Cynda Williams) 4:46

==Anti Defamation League controversy==
For his portrayal of Jewish nightclub owners Moe and Josh Flatbush, Lee drew the ire of the Anti Defamation League, Bnai Brith, and other such Jewish organizations. The Anti-Defamation League claimed that the characterizations of the nightclub owners "dredge up an age-old and highly dangerous form of anti-Semitic stereotyping", and "...disappointed that Spike Lee – whose success is largely due to his efforts to break down racial stereotypes and prejudice – has employed the same kind of tactics that he supposedly deplores." 

Lee eventually responded in an editorial in   from writing these roles for African-Americans". Lee argues that even if the Flatbush brothers are stereotyped figures, their "10 minutes of screen time" is insignificant when compared to "100 years of Hollywood cinema...   a slew of really racist, anti-Semitic filmmakers". According to Lee, his status as a successful African-American artist has led to hostility and unfair treatment: "Dont hold me to a higher moral standard than the rest of my filmmaking colleagues... Now that young black filmmakers have arisen in the film industry, all of a sudden stereotypes are a big issue... I think its reaching the point where Im getting reviewed, not my films."

Ultimately, however, Lee refuses to apologize for his portrayal of the Flatbush brothers: "I stand behind all my work, including my characters, Moe and Josh Flatbush... if critics are telling me that to avoid charges of anti-Semitism, all Jewish characters I write have to be model citizens, and not one can be a villain, cheat or a crook, and that no Jewish people have ever exploited black artists in the history of the entertainment industry, thats unrealistic and unfair." 

==See also==
* List of American films of 1990

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 