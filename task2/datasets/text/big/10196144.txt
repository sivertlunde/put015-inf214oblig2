The Children of Huang Shi
{{Infobox film
| name = The Children of Huang Shi
| image = Children of huang shi ver2.jpg
| caption = US theatrical poster
| director = Roger Spottiswoode
| producer = Arthur Cohn Wieland Schulz-Keil Jonathon Shteinman
| writer = Jane Hawksley James MacManus
| narrator = 
| starring = Jonathan Rhys-Meyers Radha Mitchell Chow Yun-fat Michelle Yeoh Guang Li
| music = David Hirschfelder
| cinematography = Zhao Xiaoding
| editing = Geoffrey Lamb
| distributor =
| released =  
| runtime =125 minutes
| country = Australia, China, Germany Japanese
| budget = 
| gross = $7,782,470  
}} Chinese 2008 George Hogg and the sixty orphans that he led across China in an effort to save them from conscription during the Second Sino-Japanese war.

==Plot summary==
George Hogg (Jonathan Rhys-Meyers) is a young British journalist from Hertfordshire in England. In 1938, during the early days of the Japanese occupation of China, he sneaks into Nanjing, China, by pretending to be a Red Cross aid worker. Arriving in Nanjing, Hogg witnesses and photographs the poverty, ruins, and corpses on the streets.  He proceeds to write a daily journal about his findings when he is interrupted by the sounds from outside. Upon peering outside the window, Hogg witnesses Japanese soldiers round up Chinese refugees and proceed to massacre the group. He anxiously takes photos of this event by the window. Later at night, Hogg is captured by the Japanese while photographing them committing atrocities. He is about to be executed when Chen Hansheng (Chow Yun-fat), a Chinese communist resistance fighter, saves him. While hiding in the rubble with Hansheng, Hogg witnesses the execution of two of his colleagues by the Japanese. Overwhelmed by shock, he inadvertently reveals their presence. A firefight ensues, and Hogg is wounded. He wakes up to Lee Pearson, (Radha Mitchell), checking on his wounds and discovers he has been brought to a rebel camp. With nowhere to go for now, Hansheng tells Hogg, on Lees suggestion, to rest at an orphanage housing 56 young boys and only an aged grandmother to take care of them. However, on the night of arrival, he is called out by one of the boys to a strange location and is savagely attacked with sticks by the orphans.  Thankfully, Lee arrives just in time and threatens to abandon the boys, leaving them without medical supplies or food. Lee explains to Hogg that she runs the orphanage and drops by from time to time with supplies.

The next day, at Lees insistence, Hogg helps her to convince the boys that the treatment of lice by flea powder does not hurt. Lees demonstration of the treatment on a naked Hoggs in the middle of the courtyard manages to convince the boys and they all promptly accept treatment. However, Lee asks Hogg to take care of the boys and states that she will be leaving for two months from March to May. Lee also leaves Hogg and the orphans with a donkey. However, Hogg replies that he has no intention to stay at the orphanage, but instead go to the front lines to write and spread the word about the war. As Hogg is leaving, he spots the grandmother looking down at Hogg  and reflects on his short memory at the orphanage. Reluctantly, he returns to take care of the children. Over the course of the next few days Hogg gains the boys respect by repairing the lighting, cleaning up the old school (which is the orphanage), and being their teacher. However, as for food, the grandmother had previously shown Hogg with a handful of maggot-infested rice, that there was close to nothing to feed the boys. 

Hogg makes a trip to town with one of the boys to seek a well-known and wealthy lady, Mrs. Wang,(Michelle Yeoh), with a business deal in mind. Aware that Mrs. Wang only wanted to deal with cash transactions, Hogg still proposed to Mrs. Wang that hed be able to provide her with vegetables if she supplied him with food and seeds for now. (A later scene would reveal that the war made Mrs. Wang flexible and compassionate toward others and therefore privately willing to barter without cash) Mrs. Wang tested Hogg to see if he possessed the agricultural knowledge so by asking him to identify certain seeds. Hogg passes the test easily and returns with the boy to the orphanage leading his donkey full of food and seeds. He starts to plow the land beside the orphanage and with the help of one of the orphans, successfully grows a flourishing vegetable garden along with beautiful and tall stalks of sunflowers.

Fleeing from the nationalists who want to conscript the boys into their army to fight the Japanese, they make a three-month journey across the snow-bound Liu Pan Shan mountains to safety on the edge of the Mongolian desert, the first 900&nbsp;km on foot. To their relief, for the last part of the journey they are supplied with four trucks.

At the destination they are supplied with a building that they turn into a new orphanage. In 1945 Hogg dies of tetanus. This was foreshadowed by Lee, when she had described the horrors of the disease to him earlier.

The film features the Rape of Nanking  and the Three Alls Policy|Sankō Sakusen,  and ends with a few brief interview snippets with some of the surviving orphans.

==Cast== George Hogg
*Radha Mitchell as Lee Pearson
*Chow Yun-fat as Chen Hansheng
*Michelle Yeoh as Mrs. Wang
*Guang Li as Shi-Kai
*Lin Ji as Horse Rider Matt Walker as Andy Fisher
*Anastasia Kolpakova as Duschka
*Ping Su as Eddie Wei
*David Wenham as Barnes

==Critical reception== New York Times gave the film an overall positive review, praising the acting and its "realistic depiction of war-ravaged China". 

The film has been criticized for ignoring the role of Rewi Alley, a Communist New Zealander celebrated in Chinas revolution. Conversion of the nurse played by Radha Mitchell from a New Zealander (Kathleen Hall, associated with Alley) to an American also received negative attention.  The omission of Alley in particular has been called a blatant misrepresentation by at least one critic. 

==Box office performance==
The film grossed around $7.4 million worldwide,  including $1.6 million in China and Spain, and $1 million in the US and Australia. 

==References==
 

==External links==
*  – official site
* 
* 
* 
* 
* 
*James MacManus (scriptwriter). " ".  Times Online. February 12, 2007.

 

 
 
 
 
 
 
 
 
 
 
 
 
 