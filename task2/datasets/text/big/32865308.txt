Death Rite
{{Infobox film
| name           = Death Rite
| image          = Death Rite.jpg
| image_size     = 
| alt            = 
| caption        = French theatrical release poster
| director       = Claude Chabrol
| producer       = Tarak Ben Ammar Jean Boujnah Tablouti Temini  
| screenplay     = Claude Chabrol Paul Gégauff
| narrator       = 
| starring       = Franco Nero Stefania Sandrelli
| music          =   
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| studio         = Carthago Films S.a.r.l. Maran Film Mondial Televisione Film
| distributor    = S.N. Prodis 
| released       = 12 May 1976
| runtime        = 94 minutes
| country        = France Italy West Germany
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Death Rite ( ) is a 1976 film co-written and directed by Claude Chabrol.

==Plot== magician Vestar (Fröbe) meets the dark Edouard (Rochefort).  Heading to the hotel, Vestar has a vision of a woman being murdered in the desert. Edouard, a member of the leisure class, decides to use his influence to make the dream become a reality. Also staying at the resort are Sadry, returning (Nero) who has come home to visit his dying mother, and his annoying wife Sylvia (Sandrelli). Also there is Martine (von Weitershausen), an ex-lover of Sabry who would like to get back together with him. The marriage is further strained when Sylvia finds the two of them together. It appears that the prophesied murder has something to do with Sylvia. Specific details from Vestars prediction about her death are used by Edouard to make it happen, although in fact his interference alters the results. Sadry comes to terms with her tensions and anger as events build toward the inevitable.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Franco Nero ||  Sadry  
|-
| Stefania Sandrelli ||  Sylvia  
|-
| Jean Rochefort || Edouard 
|-
| Gert Fröbe ||  Vestar   
|-
| Gila von Weitershausen ||  Martine   
|}

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 

 