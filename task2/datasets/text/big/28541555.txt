Irány Mexikó!
{{Infobox film
| name           = Irány Mexikó!
| image          = 
| image_size     = 
| caption        = 
| director       = Éva Zsurzs
| producer       = 
| writer         = Gedeon Viktor   József Romhányi
| narrator       = 
| starring       = Gyula Bodrogi Ági Voith Kálmán Latabár
| cinematography = György Czabarka
| music          = 
| distributor    = Magyar Film Iroda
| released       = 21 September 1968
| runtime        = 79 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Hungarian musical film directed by Éva Zsurzs and starring Gyula Bodrogi, Manyi Kiss and Kálmán Latabár.

==Main cast==
* Gyula Bodrogi ...  Gergely
* Ági Voith ...  Teri
* Kálmán Latabár ...  Csoró
* Kálmán ifj. Latabár ...  Sanyi
* Anikó Felföldi ...  Lenke
* Manyi Kiss ...  Tilda Eszter Tamási ... Herself
* Anna Bélaváry ... Soloist
* Mária Patocska ... Soloist
* András Szigeti ... Kerékpáros (as Schwetz András)
* Endre Várhelyi ... Torma Ernő, Teri apja
* István Egri ... TV-s vezető
* Sándor Siménfalvi ... Edzőtábor portása
* Ádám Szirtes ... Rendőr örnagy
* László Nemere
* János Gálcsiki
* László Mezei
* András Kósa ... Fiatal operatör
* Pál Somogyvári
* Györgyi Telessy
* Gedeon Viktor
* János Pagonyi
* István Balázs (actor)|István Balázs
* Márta Bakó
* István Nagy (actor)|István Nagy
* Mária Toldy ... Herself
* János Koós ... Himself

==External links==
* 

 
 
 
 
 
 
 
 

 
 