The Wanderer in Bulgar
{{Infobox film
| name           = The Wanderer in Bulgar
| image          = The_Wanderer_in_Bulgar_002.png
| caption        = The screenshot of The Wanderer in Bulgar.
| director       = Vladislav Chebitarev
| producer       =
| writer         = Vladislav Chebitirev
| starring       = Venera Ganieva Alfred Kamilevsky Damir Siraciev Tatar Academic Opera and Ballet Theater Artists
| music          = Räşid Kalimullin
| cinematography = Mönir Zalyuşev
| distributor    =
| released       = 1989
| runtime        = 30 min.
| country        =   USSR Tatar
| budget         =
}}

The Wanderer in Bolghar|Bulgar aka The Minstrel in Bolghar|Bulgar ( ) is a 30 minutes Tatar rock opera written and directed by Vladislav Chebitarev, music by Räşid Kalimullin based on İldar Yüzievs libretto for  classical opera Cuckoos Cry.   It was filmed in Kazan Television Studio under Gosteleradio of USSR.

==Plot== Bulgar city, the sacred place of their ancestors. While climbing up and down the ruined towers and minarets The Wanderer begins to see some flashbacks, symbolizing the return to the roots and historical identity. So the journey back to the past and up to the nowadays begins, his every step followed by different musical illustration.

==Cast==

=== Main Characters ===
*Venera Ganieva,
*Damir Siraciev as guest,
*Alfred Kamilevsky,

===Others===
Tatar Academic Opera and Ballet Theater Artists:
*Vladimir Yakovlev,
*Marat Gimatutdinov,
*Igor Zhukov,
*Konstantin Zakharov,
*Alexander Barmin,
*Anatoly Petrov,
*Ramil Gafiatullin,
*Ruald Sidayev,
*Ruslan Butayev,
*Ilya Migachev,
*Nadejda Magdeyeva,
*Farida Galeyeva,

*Dinara Bikbova,
*Kamil Kamalov,
*Kamil Fäyzrahmanov,
*Dmitry Pivovarov,
*Dmitry Rytov,
*Alfia Chebotareva,
*Zinaida Yakovleva,
 . ]]

===Vokals by===
*Venera Ganieva,
*Zölfät Xakim
*Damir Siraciev as the guest
*Rafael Sähäbiev
*Rawil İdrisov
*Räşid Kalimullin

===Music Performed by===
*ZMC (Zapiski Mertvogo Cheloveka)
*Alexander Ivanovs Band

===Choreography by===
*Vladimir Yakovlev

==References and notes==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 
 