The War Room
 
{{Infobox film
| name           = The War Room
| image_size     =
| image	=	The War Room FilmPoster.jpeg
| caption        =
| director       = Chris Hegedus D. A. Pennebaker
| producer       = R. J. Cutler Wendy Ettinger Frazer Pennebaker
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        = Chris Hegedus D. A. Pennebaker
| studio         =
| distributor    = October Films
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} Bill Clintons campaign for 1992 presidential election. It is frequently cited as one of the greatest documentaries ever made. 

==Production==

===Background=== 1992 Democratic primaries, filmmakers D.A. Pennebaker and Chris Hegedus requested permission from the Bill Clinton Campaign to film its progression. The Clinton Campaign agreed, and Pennebaker and Hegedus were allowed to film Communications Director George Stephanopoulos as well as Lead Strategist James Carville; they were given limited access to Bill Clinton.

===Locations===
At the start of filming, the film team was embedded with the Clinton Campaign in New Hampshire for that New Hampshire Democratic primary, 1992|states Democratic primary. During the onset of the campaign, the film crew traveled around the state with the Bill Clinton Campaign.

After the surprise Clinton second place finish in the New Hampshire primary, the crew filmed mostly in Little Rock, Arkansas, home to the Clinton campaigns national headquarters. As the film focused in on Carville and Stephanopoulos, the film crew saw no need to travel outside of Little Rock as both were present in the city for much if not all of the primary and general election campaigns. 

Over a time span of four months filming, Pennebaker and Hegedus only shot about 35 hours of film. Essentially, over four months they were only allowed to film less than 2 days of activity in the Clinton War Room. 

===People===
The following people were primarily documented during the film:
*Bill Clinton, 1992 Presidential Candidate and 42nd Governor of Arkansas
*George Stephanopoulos, 1992 Bill Clinton Communications Director
*James Carville, 1992 Bill Clinton Lead Strategist
 Harold Ickes, Bush deputy DNC rival Jerry Brown.

==Synopsis==
The film follows Stephanopoulos and Carville at first during the New Hampshire Primary and then mostly in Little Rock, Arkansas at Clinton Campaign Headquarters. The film follows several key 1992 Campaign events such as the Clinton Campaigns attack on " ", the Gennifer Flowers scandal, the New Hampshire primary upset and others as they played out inside of the Clinton 1992 Campaign.

The documentary uses many media headlines from the day, including media coverage of the election and other news stories such as Ross Perots presidential exit and re-entrance, among other topics.

==2008 Democratic Primary controversy==
In late April 2008, a clip from the film was released on YouTube, purporting to show former Clinton administration official (and supporter of then-presidential candidate Hillary Clinton) Mickey Kantor saying to James Carville and George Stephanopoulos, "Look at Indiana, wait, wait – look at Indiana. 42-40. It doesn’t matter if we win. Those people are shit. Excuse me." Another erroneous interpretation of the clip alleged that Kantor said, "How would you like to be a worthless white nigger?". 

On May 2, 2008, Kantor claimed that the footage had been doctored,  and shortly thereafter, D.A. Pennebaker claimed that Kantor had actually said "Those people must be shitting in the White House."  The doctored footage and false allegations against Kantor were discussed in the Return of The War Room, a 2008 sequel to The War Room.

==Reception==
The film received near universal acclaim by critics. Rotten Tomatoes gives it a 94% Fresh Rating.  As it was only screened at few locations, Yahoo reports it earned $21,446 at the box office. 

===Awards===
*66th Academy Awards Best Documentary (1994)    

*National Board of Review of Motion Pictures
:Won Special Recognition in Filmmaking (1996)
:Won Award for Best Documentary (1993)

==Influences== The Ides of March cast watched The War Room to “get their bearings” on their characters and life on the campaign trail. 

==Home Video==
The film was released as a special edition DVD and Blu-ray by the Criterion Collection on March 20, 2012. 

==References==
 

==External links==
* 
*  
* 
*  at  

 

 
 
 
 
 
 
 
 
 
 