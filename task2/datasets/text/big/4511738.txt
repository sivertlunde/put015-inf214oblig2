Heart and Souls
 
 
 
{{Infobox film
| name           = Heart and Souls
| image          = HeartandSouls.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ron Underwood Nancy Roberts Sean Daniel 
| writer         = Brent Maddock S. S. Wilson Gregory Hansen Erik Hansen 
| starring       = Robert Downey, Jr. Charles Grodin Alfre Woodard Kyra Sedgwick Elisabeth Shue Tom Sizemore David Paymer
| music          = Marc Shaiman
| cinematography = Michael W. Watkins
| editing        = O. Nicholas Brown 
| studio         = Alphaville Stampede Entertainment Universal Pictures
| released       =  
| runtime        = 104 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $16,581,714
}} fantasy comedy film directed by Ron Underwood and starring Robert Downey, Jr. as Thomas Riley, a businessman recruited by the souls of four deceased people - his guardian angels from childhood - to help them rectify their unfinished lives, as he is the only one who can communicate with them.

==Plot== afraid of the stage and quits his audition. A waitress named Julia regrets turning down her boyfriend Johns marriage proposal and leaves her job to seek him out. A small-time thief named Milo Peck unsuccessfully attempts to retrieve a collection of vintage stamps that he had stolen from a young boy. However, the bus driver, Hal, has a serious accident, killing himself and everyone on board.
 next life, but the souls of the four passengers become the guardian angels of the boy, Thomas Reilly, and can be seen only by him. Seven years later, the boys parents and teachers begin to worry about his obsession with these "imaginary friends". After realising their presence is harming Thomas, the quartet decide to become invisible also to him.

Thirty-four years later, in 1993, Hal returns with his bus and prepares to take them to the next life. The quartet learns from Hal that they had all those years with Thomas to resolve the problems they left behind, as he is their corporeal form. After convincing Hal to buy some more time for them to rectify their unfinished lives, they return to Thomas, who is now a tough businessman and indecisive in his relationship with girlfriend Anne, and ask him for his help.

Thomas reluctantly agrees and, through a series of hilarious mishaps, the lost souls are freed: Milo by returning the stolen stamps, Harrison by facing his fears and singing to a live audience, Penny by discovering the fates of her children and Julia by encouraging Thomas to repair his relationship with Anne, as she was never able to do the same with John. In the end, Thomas becomes a better man and he dances with Anne as four stars twinkle in the night sky, symbolizing that Penny, Julia, Harrison and Milo are finally at peace.

==Cast==
* Robert Downey, Jr. as Thomas Reilly 
** Eric Lloyd as 7-year-old Thomas Reilly
* Charles Grodin as Harrison Winslow 
* Alfre Woodard as Penny Washington 
* Kyra Sedgwick as Julia 
* Elisabeth Shue as Anne 
* Tom Sizemore as Milo Peck 
* David Paymer as Hal the Bus Driver 
* Bill Calvert as Frank Reilly
* Lisa Lucas as Eva Reilly
* Richard Portnow as Max Marco
* B. B. King as Himself
* Wren T. Brown as Sgt. Wm. Barclay
* Kurtwood Smith (uncredited) as Patterson
* Chloe Webb (uncredited) as Patient in Psychiatric Ward

==Production==
The film was shot on-location in San Francisco, California. 

==Reception==
Heart and Souls received mixed reviews from critics, as it currently holds a 55% rating on Rotten Tomatoes based on 22 reviews. 

==Accolades==
*Saturn Award (Best Actor in a Leading Role) - Robert Downey Jr.

==See also==
* List of ghost films

==References==
{{Reflist|refs=
   }}

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 