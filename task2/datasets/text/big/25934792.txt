Wings over Honolulu
{{Infobox film
| name           = Wings over Honolulu
| image          =
| caption        =
| director       = H. C. Potter
| writer         =
| narrator       =
| starring       = Wendy Barrie
| music          =
| cinematography = Joseph A. Valentine
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 78 min.
| country        = United States
| language       = English
}}

Wings over Honolulu is a 1937 film directed by H. C. Potter.

The cinematographer Joseph A. Valentine earned a nomination for the Academy Award for Best Cinematography for his work on this military romance, with Wendy Barrie as a young woman who finds it difficult to adjust to the life of a Naval aviators wife.

== Cast ==
* Wendy Barrie as Lauralee Curtis
* Ray Milland as Lt. Samuel Gilchrist
* William Gargan as Lt. Jack Furness
* Kent Taylor as Greg Chandler
* Polly Rowles as Rosalind Furness
* Samuel S. Hinds as Admiral Furness
* Mary Philips as Mrs. Hattie Penletter
* Clara Blandick as Evie Curtis
* Margaret McWade as Nellie Curtis
* Louise Beavers as Mamie
* Joyce Compton as Caroline
* unbilled players include Granville Bates and Milburn Stone

==External links==
* 

 

 
 
 
 
 
 
 
 


 