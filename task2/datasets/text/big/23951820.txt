Man from the Restaurant
{{Infobox film
| name           = Man from the Restaurant
| image          = 
| image_size     = 
| caption        = 
| director       = Yakov Protazanov
| producer       = 
| writer         = Yakov Protazanov Oleg Leonidov Ivan Shmelyov (story)
| narrator       = 
| starring       = Michael Chekhov
| music          = 
| cinematography = Anatoli Golovnya Konstantin Vents
| editing        = 
| distributor    =  Mezhrabpomfilm
| released       = 12 June 1927 (USSR) 4 January 1930 (USA)	
| runtime        = 62 minutes
| country        = Soviet Union Russian intertitles)
| budget         = 
}}
 Soviet film directed by Yakov Protazanov based on the story by Ivan Shmelyov. The main role was written for Ivan Moskvin, but he was changed for Chekov because of illness.

==Cast==
* Michael Chekhov	
* Vera Malinovskaya	
* Ivan Koval-Samborsky	
* Mikhail Narokov	
* Mikhail Klimov
* Andrey Petrovsky
* K. Alekseyeva
* Mikhail Zharov
* Raisa Karelina-Raich
* Sofya Yakovleva
* Stepan Kuznetsov	
* Mark Prudkin

==References==
* .
* .

==External links==
* 

 

 
 
 
 
 
 

 