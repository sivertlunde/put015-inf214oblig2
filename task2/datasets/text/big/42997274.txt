Don't Give a Damn
 
 
 
{{Infobox film
| name           = Dont Give a Damn
| image          = DontGiveaDamn.jpg
| alt            = 
| caption        = VCD cover
| film name      = {{Film name
| traditional    = 冇面俾
| simplified     = 冇面俾
| pinyin         = Mǎo Miàn Bǐ
| jyutping       = Mou2 Min5 Bei2 }}
| director       = Sammo Hung
| producer       = Sammo Hung
| writer         = 
| screenplay     = Sze To Cheuk Hon Philip Kwok Chung Wai Hung
| story          = 
| based on       = 
| starring       = Sammo Hung Yuen Biao Takeshi Kaneshiro Kathy Chow
| narrator       = 
| music          = Frankie Chan
| cinematography = Lau Hung Chuen
| editing        = Chun Yu Bojon Films Long Shong Pictures (HK) Mandarin Films
| released       =  
| runtime        = 95 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$5,085,770   
}}
 Hong Kong action film produced and directed by Sammo Hung and starring Hung, Yuen Biao, Takeshi Kaneshiro and Kathy Chow.

==Plot==
Police inspector Pierre Lau (Sammo Hung) and his young superior Tang Chuen Shek (Takeshi Kaneshiro) are ordered to cooperate with customs officer Rambo Wong (Yuen Biao) to foil a drug trade. During the operation, they arrest Nakamura, a major drug lord and seize the largest bulk of drugs since the start of Hong Kong. Japanese businessman Yamamoto (Kelvin Siu) is the mastermind of the Nakmura Organization. Due to the large amount of drugs being confiscated and with nothing left, Yamamoto desperately decides to order his underling Siu Lung (Collin Chou) to hire some thugs from America to help him snatch the drugs at the police station to destroy the evidence so Yamamoto would not face any charges and restore the collapse Nakamura Organization.

On the other hand, Yau Ching (Kathy Chow) and Pierre are like-minded colleagues, while May (Annabelle Lau), who has a secret crush on Pieree, is not his cup of tea. At the same time, Rambo has admired Pierres subordinate Anna (Eileen Tung) after their first meet and often forces Pierre to be their matchmaker.
 SDU members and go snatch the drugs from the evidence room. However, they were penetrated by Shek  and after battling alongside Pierre against the thugs, the thugs managed to escape after one of the thugs Sean (Shawn Patrick Berry) was arrested.

After this, Yamamoto orders the thugs to leave Hong Kong, who refuse to do so since Bobby (Robert Samuels), Seans older brother, refuses to leave without his brother. When Yamamotos bodyguards attempt to kill them, the thugs managed to overpower them and kill them. The thugs then kidnap Ching and forces Pierre to free Ching, where he will get Ching and the drugs back in exchange. Pierre decides to take matters to his own hands and Rambo and Shek agrees to help him, while Yamamoto also decides to take back the drugs and kill the thugs.

Pierre confronts the thugs with holding Rambo, who is dressed and painted to resemble Sean, hostage to exchange for Ching and the drugs while Shek, dressed and painted to resemble Sean, takes another route to rescue Ching. But hell breaks loose no so long later and Yamamoto also arrives with Siu Lung and two white bodyguards. After several long battles, where Pierre defeats Bobby, who was later killed by Lung, kills Lung and Yamamoto, Rambo killing the two bodyguards with help of two thugs Timmy (Timmy Hung) and Cheung (Jimmy Hung), who decide to turn straight and be witnesses, and Shek engaging in a gunfight with the thugs and bodyguards, Pierre is reunited with Ching.

==Cast==
 
*Sammo Hung as Pierre Lau
*Yuen Biao as Rambo Wong Yuk Man
*Takeshi Kaneshiro as Inspector Tang Chuen Shek
*Kathy Chow as Yau Ching
*Eileen Tung as Anna
*Annabelle Lau as May
*Timmy Hung as Timmy
*Collin Chou as Siu Lung
*Jimmy Hung as Cheung
*Kelvin Wong as Yamamoto
*Dion Lam as Yamamotos bodyguard
*Eddie Maher as Yamamotos bodyguard
*Habby Heske as Yamamotos bodyguard
*Roy Filler as Yamamotos bodyguard
*Wong Chan as Commissioner Wong
*Robert Samuels as Bobby
*Shawn Patrick Berry as Sean
*Charlie Wong as Charlie
*Jackie Lee as Jackie
*Yvonne Yung as Sun Flower (cameo)
*Natalis Chan as Deranged cop (cameo)
*Lau Kar-wing as Statement cop (cameo)
*Bryan Leung as No-nonsense cop (cameo)
*Melvin Wong as Rambos superior officer (cameo)
*Michael Miu as CID (cameo)
*Tommy Wong as CID (cameo)
*Wan Chi Keung as CID on the bus (cameo)
*Wan Yeung-ming as CID on the bus (cameo)
*Blackie Ko as Stupid robber on the bus (cameo)
*Richard Ng as Old man on the bus (cameo)
*Cheung Kwok Keung as Superman (cameo)
*Chin Siu-ho as Spider Man (cameo)
*Teddy Yip as Senior Officer Yip (cameo)
*Wu Ma as Arrested hawker (cameo)
*Peter Chan as Rapist at scrapyard (cameo)
*Billy Lau as Fainted cop (cameo)
*Wong Hoi Yiu
*Simon Cheung
 

==Production== China Drama Academy together, and members of the Seven Little Fortunes, since 1988s Dragons Forever. However, due to obligations on Rumble in the Bronx, Chan was forced to pull out and was replaced by Takeshi Kaneshiro.   

==Box office==
The film grossed HK$5,085,770 at the Hong Kong box office during its theatrical release from 17 February to 2 March 1995 in Hong Kong. 

==See also==
*Sammo Hung filmography
*Yuen Biao filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 