Secrets of the Lone Wolf
 
{{Infobox film
| name           = Secrets of the Lone Wolf
| image          = 
| caption        = 
| director       = Edward Dmytryk
| producer       = Jack Fier
| writer         = Stuart Palmer Louis Joseph Vance
| starring       = Warren William
| music          = 
| cinematography = Philip Tannura
| editing        = Richard Fantl
| distributor    = 
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
}}

Secrets of the Lone Wolf is a 1941 American crime film directed by Edward Dmytryk.   

==Cast==
* Warren William as Michael Lanyard Ruth Ford as Helene de Leon Roger Clark as Paul Benoit
* Victor Jory as Dapper Dan Streever
* Eric Blore as Jamison the Butler
* Thurston Hall as Inspector Crane
* Fred Kelsey as Detective Sergeant Wesley Dickens
* Victor Kilian as Colonel Costals
* Marlo Dwyer as Bubbles Deegan
* Lester Sharpe as Deputy Duval (as Lester Scharff)
* Irving Mitchell as Mr. Evans, financier John Harmon as Uptown Bernie, alais Bernard the Steward
* Joe McGuinn as Bob McGarth, Private Detective

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 