Kannukku Kannaga
{{Infobox film
| name           = Kannukku Kannaga
| image          = 
| image_size     = 
| caption        = 
| director       = S. Dhayalan
| producer       = Henry
| writer         = S. Dhayalan
| starring       =   Deva
| cinematography = Thangar Bachan
| editing        = K. Thanigachalam
| distributor    = 
| studio         = Pankaj Productions
| released       = 26 October 2000
| runtime        = 130 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 2000 Tamil Tamil drama Vindhya and Raja in Deva and was released on 26 October 2000.    

==Plot==

Dharma (Murali (Tamil actor)|Murali) and Devi (Devayani (actress)|Devayani) are sibling and are very close. Devi (Devayani (actress)|Devayani)  and Arun (Raja (Tamil actor)|Raja) fall in love but Selvi cannot reveal her love to her kindly brother. Dharma, knowing her love, accepts for the marriage although the astrologer (Charuhasan) says that, if his sister gets married with Arun and they had a male child, Dharma will die definitely. Few months later, they have a male child called Vijay (Master Vasanth) and Dharmas death countdown begins.

==Cast==
 Murali as Dharma Devayani as Devi Vindhya as Selvi Raja as Arun
*Master Vasanth as Vijay
*Vadivelu as Velu
*Anu Mohan as Aruns father
*Charuhasan as the astrologer
*Mayilsamy 
*Kumarimuthu as Perumal

==Soundtrack==

{{Infobox album |  
|  Name        = Kannukku Kannaga
|  Type        = soundtrack Deva
|  Cover       = 
|  Released    = 2000
|  Recorded    = 2000 Feature film soundtrack
|  Length      = 24:14
|  Label       = Star Music Deva
|  Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 2000, features 5 tracks with lyrics written by Vaali (poet)|Vaali, Muthulingam, Kalidasan and Nandalala.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration 
|-  1 || Anandam Anandam || P. Unni Krishnan || 4:59
|- 2 || Computer Graphic Pennukullae || Anuradha Sriram || 4:30
|- 3 || Deva || 5:09
|- 4 || Sama Kulir Adikkuthu || Krishnaraj, S. Janaki || 4:17
|- 5 || Thulli Thulli Mazhaithulli || Sujatha Mohan || 5:20
|}

==Reception==

Savitha Padmanabhan of hindu.com said that : "This domestic drama suffers because of lack of originality and exaggerated sentiments".  

==References==
 

 
 
 
 