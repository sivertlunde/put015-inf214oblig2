Goal! (film)
 
 
 
{{Infobox film
| name         = Goal!: The Dream Begins 
| image        = GoalPoster.jpg
| caption      = U.S. theatrical poster
| director     = Danny Cannon
| producer     = Mike Jefferies Matt Barrelle Mark Huffam
| screenplay   = Dick Clement Ian La Frenais Mike Jefferies
| story        = Mike Jefferies Adrian Butchart
| starring     = Kuno Becker Alessandro Nivola Marcel Iureş Stephen Dillane
| music        = Graeme Revell Michael Barrett
| editing      = Chris Dickens
| studio       = Touchstone Pictures Buena Vista Pictures
| released     = 30 September 2005 (UK) 12 May 2006 (US)
| runtime      = 118 minutes
| language     = English
| budget       = $33 million 
| gross        = $27,610,873 	 
}}
Goal! (also known as Goal! The Dream Begins in the United States) is a 2005 film directed by  , was released in February 2007. The third instalment,  , was released straight to DVD in June 2009.

==Plot== football professionally. Newcastle United GMC truck to allow them to work for themselves. His dream is not lost though, as his grandmother sells off her jewellery to buy him a ticket to England.
 substitute in The Sun, causing anger from the manager. At the same time, Santiagos friend, Jamie, suffers a career-ending injury that only causes him additional grief.
 free kick, which Gavin gives to Santiago. Santiago, with the hopes and prayers of the whole city of Newcastle resting on his shoulders, scores, and Newcastle win 3–2. Glen reveals to Santiago that his grandmother is trying to call. She mentions that his father did watch his first match against Fulham, after learning this from a fellow supporter (a cameo by Brian Johnson, lead singer from AC/DC, who was born and raised in Newcastle). Santiago shouted to Glen that his father saw him play and was proud of him before he died. Glen replies: "Hes probably watching you right now." The film happily ends with Santiago shedding tears of joy while embracing his dream.

==Cast==
* Kuno Becker as Santiago Muñez
* Alessandro Nivola as Gavin Harris
* Stephen Dillane as Glen Foy
* Marcel Iures as Erik Dornhelm
* Anna Friel as Roz Harmison
* Tony Plana as Santiagos father
* Kieran OBrien as Hughie McGowan
* Sean Pertwee as Barry Rankin	
* Míriam Colón as Mercedes Muñez
* Cassandra Bell as Christina
* Alejandro Tapi as Júlio
* Kate Tomlinson as Val
* Arvy Ngeyitlala as Tom
* Zachary Johnson as Rory
* Jorge Cervera as Cesar

===Cameo appearances===
{{columns
|col1=
*   Zinedine Zidane &nbsp; &nbsp;}}
*   David Beckham
*   Ronaldo Petit
*   Hugo Viana
*   Hélder Postiga Ricardo
*   Raúl González
*   Iker Casillas Roberto Carlos
*   Alan Shearer
*   Martin Tyler
*   Shay Given
*   Sven-Göran Eriksson
*   Lee Bowyer
*   Jermaine Jenas
*   Kieron Dyer
*   Titus Bramble
|col2=
*   Brian McBride James Adams
*   Carlos Bocanegra
*   Tomasz Radzinski
*   Stephen Carr
*   Jean-Alain Boumsong}}
*   Michael Chopra
*   Rafael Benítez
*   Milan Baroš
*   Vladimír Šmicer
*   Laurent Robert
*   Celestine Babayaro
*   Frank Lampard
*   Joe Cole
*   Steven Gerrard
*   Emile Heskey
*   Thomas Gravesen
*   William Gallas
*   John Arne Riise
|col3=
*   Jamie Carragher
*   Igor Bišćan
*   Dave Hughes
*   Patrick Kluivert
*   Jiří Jarošík
*   Rob Lee Pablo García
*   Shola Ameobi
*   James Milner
*   Thierry Henry
*   Sergio Ayala
*   Rafael Márquez
*   José Antonio Reyes
*   Fredrik Ljungberg
* Howard Webb, as referee in final match (Newcastle vs Liverpool)
}}

==Soundtrack==
The soundtrack contains "Playground Superstar", the track that marked the full return of alternative rock group Happy Mondays. A video was also made to promote the soundtrack.

The soundtrack was released on Oasis (band)|Oasis Big Brother label and contains three Oasis songs unavailable elsewhere, including the exclusive Noel Gallagher song "Who Put the Weight of the World on My Shoulders?".
 Cast No Morning Glory" for inclusion on the soundtrack.

#"Playground Superstar" – Happy Mondays  (exclusive track)  Oasis  (exclusive track) 
#"Leap of Faith" – Unkle featuring Joel Cadbury  (exclusive track) 
#"Human Love" – Dirty Vegas
#"Morning Glory" (Dave Sardy Mix) – Oasis  (exclusive track)  The Bees
#"Cast No Shadow" (Unkle Beachhead Mix) – Oasis  (exclusive track) 
#Score: "Thats That" – Graeme Revell
#"Club Foot" – Kasabian
#"Look Up" – Zero 7
#"Wet!Wet!Wet!/Keith n Me|Wet! Wet! Wet!" – Princess Superstar
#"Blackout" – Unkle
#"Will You Smile Again for Me" – ...And You Will Know Us by the Trail of Dead
#Score: "Premiership Medley" – Graeme Revell

==Reception==
 
 
The film received mixed reviews. Rotten Tomatoes gave the film a score of 44% based on reviews from 80 critics, with an average score of 5.2/10.  Other reviews came from Variety.com,  BBC Film,  and UEFA Perspective. 

Despite its mediocre critical reviews, the film has gained major popularity among sports fans, especially football supporters, as it is one of the few movies dedicated to this sport. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 