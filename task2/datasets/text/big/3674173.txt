The Thief (1952 film)
{{Infobox film
| name           = The Thief
| image          = The Thief (1952 film) poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Russell Rouse
| producer       = Clarence Greene
| screenplay     = Clarence Greene Russell Rouse
| starring       = Ray Milland
| music          = Herschel Burke Gilbert
| cinematography = Sam Leavitt Chester W. Schaeffer
| studio         = Harry Popkin Productions
| distributor    = United Artists
| released       =   
| runtime        = 86 minutes
| country        = United States
| language       =
| budget         =
| gross          = $1 million 
}}
The Thief is a 1952 American black-and-white Cold War film noir spy film, directed by Russell Rouse and starring Ray Milland. Its the third in a series of six classic film noir productions scripted by Rouse and his writing partner Clarence Greene. The film is unusual because there is no principal actor dialogue spoken. 

The principal characters are not "fleshed-out", as they might be in a more conventional film, as this film is more about "trade-craft", not as much about the principal characters and their respective personalities. Not withstanding that observation, the film certainly depends upon the exceptionally strong dramatic performance by Ray Milland, and the equally strong music performance by composer/conductor Herschel Burke Gilbert. The film is nearly fully orchestrated by Gilbert.

==Plot== Atomic Energy Commission in Washington, D.C.. But Fields is also a spy working for an unnamed foreign power.
 Minox camera, and passes these through a vast network of foreign-power "couriers"  to New York City, and thereafter overseas to an enemy country (implied by the final couriers planes destination of "Cairo",  certainly a thinly-veiled reference to "the East", without actually naming the Soviet Union). The latest canister of microfilm which Fields sends out is intercepted by authorities after a courier is killed in a freak traffic accident in Manhattan, with the undeveloped microfilm canister in his hand. The FBI develops the microfilm, analyzes its contents and constructs a list of probable suspects within the AEC, one of whom is the "custodian"  of the subject document, and who is initially interrogated.

The custodian having apparently been cleared of espionage charges, Fields and his immediate AEC colleagues have all come under suspicion by the FBI, and agents are assigned to "tail" each one, but it quickly becomes apparent that Fields is the "prime suspect".  Fields case officer becomes aware of this and sends him a "flash message", in a Western Union telegram, to destroy all his "spy-craft" apparatus and to leave immediately for a "safe house"  in New York City.

Now scared and paranoid, Fields stays overnight in the safe house, a cheap hotel, waiting for a "signal" (as usual, throughout this film, three rings on the phone, a hang-up, followed immediately by three rings, followed by another hang-up)  from his case officer on the hotels hall phone.
 observation deck, Fields meets his contact, Miss Philips. The alert FBI agent spots Fields and pursues Fields who climbs even higher, reaching the 102nd-floor observation deck, and, finally, the Empire State Building#Above 102nd floor|spire—where, perhaps as an Homage (arts)|homage, King Kong had famously met his doom several decades before—Fields fights off the agent, causing the agent to plummet to his death. Fields exits the building with money and false identity documents, his "escape",  which will get him out of the country, incredibly enough, also to "Cairo", but he has been shaken by the sight of the dead agent, and feels remorse.

Fields finally breaks down after realizing what he has done, destroys his escape, and surrenders to the FBI the next day.

==Cast==
* Ray Milland as Allan Fields (Nuclear physicist/spy for the Soviet Union) case officer)
* Harry Bronson as Harris (FBI agent)
* Rita Vale as Miss Philips (Soviet agent/courier)
* Rex OMalley as Beal (Soviet agent/courier)
* Rita Gam as the Girl (MacGuffin#Broader use|MacGuffin)
* John McKutcheon as Dr. Linstrum
* Joe Conlin as Walters

==Reaction==

===Critical response===
When the film was released, A. W. Weiler, the film critic at The New York Times gave the film a good review, writing, "Clarence Greene and Russell Rouse, an enterprising pair of film artisans, are trying to prove that some movie yarns are better seen than heard. Their effort is a successful tour de force. For, generally speaking, theirs is a spy melodrama in which language would appear to be redundant ...  aside from its novelty, The thief has its fair share of attributes. The fine photography of cinematographer Sam Leavitt, whose cameras have captured the lights of actual, and familiar, locations in Washington and New York, contributes strongly to the tensions of the hunt. The musical score by Herschel Gilbert is insidiously suggestive in creating atmosphere as well as indicating the emotions of the principals. And, above all, Russell Rouse, who also directed, has gotten a sensitive and towering performance from Ray Milland in the title role." 

The staff at Variety (magazine)|Variety magazine reviewed the film positively.  They wrote, "This has an offbeat approach to film story-telling (a complete absence of dialog), a good spy plot and a strong performance by Ray Milland. The film is not soundless. The busy hum of a city is a cacophonous note, a strident-sounding telephone bell plays an important part and, overall, there’s the topnotch musical score by Herschel Burke Gilbert, sometimes used almost too insistently to build a melodramatic mood and in other spots softly emphasizing and making clear the dumb action of the players." 
 The Oscar) Red Scare thriller set in New York City ... What we get is a tense mood piece through the excellent dark visuals delivered by cinematographer Sam Leavitt. It shows a lonely and alienated unsympathetic man on-the-run, who is trapped in a shadowy world of chaos but is not fleshed out in his  character so we never become concerned with his plight as a human interest story." 

===Accolades===
{| class="wikitable" width="75%"
|- 
! Year !! Award/Category !! Recipient !! Result
|- bgcolor="#dbdbdb"
| colspan=4 align="center"| Academy Awards
|-
| 1953 || Best Music, Scoring of a Dramatic or Comedy Picture || Herschel Burke Gilbert || Nominated
|- bgcolor="#dbdbdb"
| colspan=4 align="center"| Golden Globe Awards
|-
| 1953 || Best Cinematography - Black and White || Sam Leavitt  || Nominated
|- bgcolor="#dbdbdb"
|-
| 1953 || Best Motion Picture - Drama ||   || Nominated
|- bgcolor="#dbdbdb"
|-
| 1953 || Best Motion Picture Actor - Drama || Ray Milland   || Nominated
|- bgcolor="#dbdbdb"
|-
| 1953 || Best Screenplay || Clarence Greene, Russell Rouse    || Nominated
|- bgcolor="#dbdbdb"
|-
| 1953 || Most Promising Newcomer - Female || Rita Gam || Nominated
|- bgcolor="#dbdbdb"
|}

==References==
 

== External links ==
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 