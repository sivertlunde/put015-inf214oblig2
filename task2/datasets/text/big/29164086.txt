The Devil Is Driving
{{Infobox film
| name           = The Devil Is Driving
| image          = The Devil Is Driving poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Benjamin Stoloff
| producer       = Charles R. Rogers
| screenplay     = Frank Mitchell Dazey P.J. Wolfson Allen Rivkin Louis Weitzenkorn  Lois Wilson Alan Dinehart Dickie Moore
| music          = Karl Hajos John Leipold
| cinematography = Henry Sharp 
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
The Devil is Driving is a 1932   gave the film a mixed review upon its release. Hall, Mordaunt.  , The New York Times, December 16, 1932, accessed October 13, 2010. 

==Plot== drifter with a chronic gambling problem. Despite his flaws he is beloved by his family.  Gabbys brother-in-law Beef gets Gabby work as a mechanic at the Metropolitan Garage. The shop is a front to a stolen car ring. His brother-in-law Beef, who is otherwise honest, is aware of this. One day, Gabby is sent to pick up Silver, Jenkinss girl friend, whose car has broken down. Both Gabby and Silver start a relationship, after which Silver leaves Jenkins. During a getaway one of car thiefs hits Gabbys nephew Buddy, who is in the street driving a toy car. The driver makes it to the garage, and Buddy receives treatment at a hospital. A witness points out the car to Gabby, and he understands its the car that drove into the garage to be repainted. He investigates and discovers a piece of Buddys little car in the wheel of the stolen car. When he confronts Beef, Beef gets drunk and confronts Jenkins and the head of the stolen car ring. They kill Beef, making his death look accidental. Photographer Bill Jones gives Gabby a photograph of Beef in the car before the accident, which shows Beef was already dead. Silver and Gabby confront Jenkins. The criminals drive away, but die in a car crash. With the hoodlums out of the way, Gabby marries Silver.

==Cast==
*Edmund Lowe as Gabby Denton
*Wynne Gibson as Silver
*James Gleason as Beef Evans Lois Wilson as Nancy Evans
*Alan Dinehart as Jenkins 
*Dickie Moore as Buddy Evans
*George Rosener as The Dummy
*Guinn "Big Boy" Williams as Mac 

==Reception==
  reprinted in The New York Times, accessed October 12, 2010. 

==Notes==
 

==References==
*Doherty, Thomas Patrick. Pre-Code Hollywood: Sex, Immorality, and Insurrection in American Cinema 1930-1934. New York: Columbia University Press 1999. ISBN 0-231-11094-4

==External links==
* 

 
 
 
 
 
 