Rosolino Paternò, soldato...
{{Infobox film
 | name =  Rosolino Paternò, soldato...
 | image =  Rosolino Paternò, soldato....jpg
 | caption =
 | director =  Nanni Loy
 | writer = Agenore Incrocci Furio Scarpelli
 | starring =  Nino Manfredi Jason Robards Peter Falk Martin Landau
 | music =  Carlo Rustichelli
 | cinematography =  Tonino Delli Colli
 | editing =Ruggero Mastroianni
 | producer = Dino de Laurentiis Cinematografica
 | distributor = Allied Artists Pictures
 | released =   
 | runtime =
 | awards =
 | country = Italy
 | language = English
 | budget =
 }} Italian comedy film. The film stars Martin Landau, Jason Robards, and Peter Falk. In the title role is Nino Manfredi.

==Plot==
Sicily, 1943, before the Allied assault in July. A pair of American commandos parachute inside the island for a secret mission. They are asked to check on the German weapons and positions before the assault. Along the way, they meet a wounded Italian prisoner of war. He agrees to help them complete their mission, provided there are no bumps in the road and no Germans thrown in.

== Cast ==
* Nino Manfredi as Rosolino Paternò
* Jason Robards as Sam Armstrong
* Martin Landau as Joe Mellone
* Peter Falk as Peter Pawney
* Scott Hylands as Reginald Wollington
* Milena Vukotic as Rosolinos wife
* Frank Latimore as the American Lieutenant
* Anthony Dawson as the Italian General
* Slim Pickens as General Maxwell
* Lorenza Guerrieri as Vincenzina Puglisi
* Renzo Marignano
* Mario Maranzana
* Orso Maria Guerrini

==Production==
The film was filmed in Licata and Agrigento. It was completely shot in English, with great difficulty for the lead actor Nino Manfredi due to his lacking of English language skills.  The English-language version took its title from the American anacronym
during the war used for situational conditions before any sanctioned mission by the military: Military slang|SNAFU, meaning Situation Normal, All Fouled Up.

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 


 
 