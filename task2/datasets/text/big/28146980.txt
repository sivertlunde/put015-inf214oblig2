Spoken Word (film)
{{Infobox film
| name           = Spoken Word
| image          = spoken-word.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Victor Nuñez
| producer       =  
| writer         =  
| starring       =  
| music          = Michael Brook
| cinematography = Virgil Mirano
| editing        =  
| studio         =  
| distributor    = Variance Films
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Spoken Word is a 2009 drama directed by Victor Nuñez and stars Kuno Becker, Ruben Blades, Miguel Sandoval and Persia White. 

The writers include William T. Conway and Joe Ray Sandoval. The movie was produced by Karen Koch and William T. Conway. The film opened in NYC at Big Cinemas Manhattan 1 on July 23, 2010 and played in Los Angeles at Laemmles Sunset 5 on July 30, 2010.

==Plot==
A Latino San Francisco spoken word artist returns home to Santa Fe, New Mexico to reconnect with his dying father and brother, finding himself losing his "voice" as he spirals downward back into the dysfunctional life of drugs and violence he had left behind. 

==Cast==
*Kuno Becker as Cruz Montoya
*Ruben Blades as Senior
*Miguel Sandoval as Emilio
*Persia White as Shae
*Chris Ranney as Bartender

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 


 