Der 10. Mai
 
{{Infobox film
| name           = Der 10. Mai
| image          = 
| caption        = 
| director       = Franz Schnyder
| producer       = Franz Schnyder
| writer         = Franz Schnyder Arnold Kübler William Michael Treichlinger
| starring       = Hans Gaugler
| music          = 
| cinematography = Konstantin Irmen-Tschet
| editing        = Hans Heinrich Egger
| distributor    = 
| released       = 18 October 1957
| runtime        = 91 minutes
| country        = Switzerland
| language       = German
| budget         = 
}}

Der 10. Mai is a 1957 Swiss drama film directed by Franz Schnyder. It was entered into the 8th Berlin International Film Festival.   

==Cast==
* Hans Gaugler - Fritz Steiner
* Linda Geiser - Anna Marti
* Therese Giehse - Ida Herz
* Yvette Perrin - Jeannette Perrin
* Heinz Reincke - Werner Kramer Fred Tanner - Albert Widmer
* Hermann Wlach - Julius Herz

==Plot==
  start the Battle of France and invade the Netherlands, Belgium and Luxembourg.

Change of location to German-Swiss border area: Early in the morning the armed ticket inspector Emil Tschumi  controls tracks next to the border. He surprises Werner Kramer, who is a German refugee. Kramer swims through the Rhine and arrives Switzerland. Tschumi is a good-natured, old man, who receives the repairman to his small house. There he offers to dry the wet clothes. The young German alleges that he is followed by the Gestapo, because he had spoken with a colleague about foreign radio stations. Tschumi has sympathy for Kramer, but border officers will expel Kramer, so Tschumi gives him advice to get away. 

On 10 May 1940 troops march along the border of Switzerland. The Swiss Federal Council is afraid of a sneak attack and orders the mobilization. Now the Swiss border gets sharply guarded. An old German refugee is located at a border station and he hasn’t got documents and Swiss francs is going to buy a ticket for inland. Werner Kramer is at the same place and he is shocked, because the old German is hauled off by the swiss border police. With the aid of a truck driver  Kramer leaves the border area. Suddenly the truck driver abandons Kramer. In spite of being nowhere, he reaches Zurich, where his old friend of his childhood, Anna Marti lives. She works as a tailoress at fashion store Perrin.

Anna advises him to beg the family of influence, named Hefti for help. That family he knows because of early times. But they are already gone to inland, like as many wealthy people.  They had fled because in the alps they thought they’re safety  of a potential invasion of German. Anna gives him  permission to live in a mansard. He always fears that somebody could denounce, discover him and then he will be expel ed by police. Especially Anna’s brother-in-law Albert Widmer is not sympathetic to him. His biggest fear is Albert Widmer, because he frankly says his opinion. The next day he expresses the desire that he wants to give up the hide-and-seek and to turn himself in to the police sergeant Grimm.       

==References==
 

==External links==
* 

 
 
 
 
 
 
 