The Cat Who Walked by Herself
 
{{Infobox Film
| name = The Cat Who Walked by Herself
| image =
| caption = 
| imdb_rating = 
| director = Ideya Garanina
| producer = Ideya Garanina
| writer = Ideya Garanina Mariya Solovyova Valentina Ponomaryova Yelena Sanayeva Georgi Burkov Nikolay Karachentsov Inna Churikova Nogon Shumarov Anna Kamenkova Nikolai Burlyayev
| music = Sofya Gubaydulina
| cinematography = 
| editing = Nadezhda Treshchyova
| distributor = 
| released = 1988 (USSR)
| runtime = 70 min Soviet Union
| language = Russian
| budget = 
}} Soviet animation|animated The Cat that Walked by Himself". Like the earlier Soviet animated feature Adventures of Mowgli, the film retains the dark, primal tone of Kiplings work. Includes in itself almost all types animation technologies.

It not the only Soviet screen version of this fairy tale: in 1968 the director Aleksandra Snezhko-Blotskaya at the same studio released other animated film "The Cat Who Walked by Himself" made in the drawn equipment with lasting 20 minutes.

==Plot==
The film is largely based on the short story, but goes off on many digressions.  

Two parents put their child in his crib for the night and leave the room.  The child starts crying, and the Cat comes into the room to keep him company.  The child grabs her tail, and the Cat angrily reminds him that they agreed a thousand years ago that he would not do that.  Upon seeing that the child doesnt remember, the Cat sighs and decides to tell him the story from the beginning. The story starts when the planet was young and life on Earth is starting with dinosaurs.

==Creators==
 
 
{| class="wikitable"
|-
! !! English !! Russian
|-
| Director-producer
| Ideya Garanina
| Идея Гаранина
|-
| Scenario 
| Ideya Garanina Mariya Solovyova
| Идея Гаранина Мария Соловьёва
|-
| Art Director
| Nina Vinogradova
| Нина Виноградова
|-
| Animators
| Aleksandr Gorlenko Violetta Kolesnikova Aleksandr Panov Yuriy Batanin Vladimir Shevchenko Anatoliy Abarenov Olga Panokina Akop Kirakosyan Yelena Gavrilko Tatyana Molodova Lidiya Mayatnikova
| Александр Горленко Виолетта Колесникова Александр Панов Юрий Батанин Владимир Шевченко Анатолий Абаренов Ольга Панокина Акоп Киракосян Елена Гаврилко Татьяна Молодова Лидия Маятникова
|-
| Camera Operator
| Aleksandr Vikhanskiy
| Александр Виханский
|-
| Executive Producer
| Grigoriy Khmara
| Григорий Хмара
|-
| Composer
| Sofya Gubaydulina
| Софья Губайдулина
|-
| Sound Operators 
| Vladimir Vinogradov Sergei Karpov
| Владимир Виноградов Сергей Карпов
|-
| Script Editor
| Raisa Frichinskaya
| Раиса Фричинская
|-
| Puppets & Decorations
| Marina Chesnokova Natalia Grinberg Vladimir Abbakumov E. Belova Mikhail Koltunov Aleksandr Maksimov O. Potanin Pavel Gusev Vladimir Alisov Anna Vetyukova Svetlana Znamenskaya Nikolay Zaklyakov Liliana Lyutinskaya V. Platonov Nina Moleva Oleg Masainov Natalia Barkovskaya
| Марина Чеснокова Наталия Гринберг Владимир Аббакумов Э. Белова Михаил Колтунов Александр Максимов О. Потанин Павел Гусев Владимир Алисов Анна Ветюкова Светлана Знаменская Николай Закляков Лилиана Лютинская В. Платонов Нина Молева Олег Масаинов Наталия Барковская
|-
| Sculptor
| S. Aseryants
| С. Асерьянц
|-
| Voice Actors  Valentina Ponomaryova (Woman) Yelena Sanayeva Georgi Burkov Nikolay Karachentsov Inna Churikova Nogon Shumarov (Man) Anna Kamenkova Nikolai Burlyayev
| Валентина Пономарёва Елена Санаева Георгий Бурков Николай Караченцов Инна Чурикова Ногон Шумаров Анна Каменкова Николай Бурляев
|-
| Editor Nadezhda Treshchyova
|Надежда Трещёва
|}

==Home video==
The film is available on DVD in the collection Золотая коллекция мультфильмов 9 ("Golden Collection of Cartoons 9"), a PAL 2003 Russian release.  It also includes the films Barankin, Be a Man!,  Inchgirl,  The Pot of Porridge,  How the Cat Fought with Mice,  and Wings, Legs and Tails  (total running time: 142 minutes).

The film has also been released on DVD by the company Krupnyy Plan. 

==See also==
*History of Russian animation
*List of animated feature-length films
*List of stop-motion films

== Notes ==
 

==External links==
*  at Animator.ru
* 
*   


 
 
 
 
 
 
 
 
 
 
 