A.K. (film)
{{Infobox film name           = A.K. image          = caption        = director       = Chris Marker producer       = Serge Silberman writer         = Chris Marker starring       = Akira Kurosawa Chris Marker Tatsuya Nakadai music          = Tôru Takemitsu cinematography = Frans-Yves Marescot editing        = Chris Marker distributor    = released       =   runtime        = 75 minutes country        = France language       = French budget         =
}}
A.K. is a 1985 French documentary film directed by Chris Marker about the Japanese director Akira Kurosawa. Though it was filmed while Kurosawa was working on Ran (film)|Ran, the film focuses more on Kurosawas remote but polite personality than on the making of the film. The film is sometimes seen as being reflective of Markers fascination with Japanese culture, which he also drew on for one of his best-known films, Sans Soleil.  The film was screened in the Un Certain Regard section at the 1985 Cannes Film Festival.   

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 


 
 