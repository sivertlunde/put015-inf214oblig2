Janapriyan
{{Infobox film
| name           = Janapriyan
| image          = Janapriyan.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Boban Samuel
| producer       = Mammen John Reena M John
| writer     = Krishna Poojapura Bhama Sarayu Sarayu Lalu Alex
| music          = Rinil Gowtham
| cinematography = Pradeep Nair
| editing        = V. T. Sreejith
| studio         = Spot Light Visions
| distributor    = Kalasangam Release
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Malayalam romantic Bhama in the lead roles.    The film was released on May 20, 2011, with highly positive reviews from audiences.

==Plot==

The film starts off with Vaishakan (Manoj K Jayan) a man aspiring to be a film director, telling his dreadful story to a film producer named Achayan (Jagathy Sreekumar). He gets told from Achayan that Vaishakans story is not worthy of being a film produced by him.

Then he goes to his full-time job as an office worker in a subdivision of a revenue district, where he gets informed by the head (Lalu Alex) of the complaints of him not submitting files said by the collector.
Vasishakan gets told that he will receive a memo by the head, in which he insists to get laid off so he can get more time focusing on his direction and films. Also, that he is only in the office because of his father who told him to take up his job in the office and died.
There on everyone refuses to hear his film script, they taunt him for not being able for his lack of sincerity in his office job and his failure in getting towards his ambition as a director.
People even protest in front of the office, for him to be laid off. To make a fair decision, the head announces to him and the impacted people, that he shall take a five-year leave without allowance for better employment within the country.
After this, it shows the jovial life Priyadarshan (Jayasurya), a village simpleton, whose daily work consists of doing all the work around the rubber farm, fruits and vegetables farm and giving the load of fruits and vegetables to local stores. He also teaches in a nearby high school, tuitions two elementary school-goers and supplies his familys basic needs.
After coming home from his works, Priyadarshan is disappointed in hearing from his mother that there are problems marrying his younger sister, due to his father committing suicide because he couldnt pay back his loans and tells his mother that he will pay off all those loans.
He then receives the employment letter on being temporarily appointed at the Taluk Office replacing Vaishakan on a leave vacancy and is overjoyed.
Priyadarshan does his good deeds from the start like cleaning the office, which is rather dirty and shows the whole office that he is nothing but an optimistic merry man, who does all of his work sincerely. Priyadarshan merges into the city landscape in no time, and its busy inhabitants find his ways amusing. He then meets his humorous room mate in his lodge Kannappan (Salim Kumar).  
Priyadarsan talks a lot that too in a peculiar slang, is highly idealistic and falls in love with Meera (Bhama), who lives in a house next to his room in a lodge where he is staying in the city. He loves her as he thinks that Meera is the maid there, but actually she is the daughter of a wealthy businessman. At the office one day Vaishakan comes in saying he wants to cancel his dreams of being a director and resume his job there. Priyadarshan becomes upset and goes to see Vaishakan and helps him get a producer.Priyadarshan is recommended by Meera, who is in love with him and his innocence. He makes changes in Vaishakans script by making it heart touching about a father and son who live in a poor household. 
Vaishakan quickly befriends Priyadarshan because with his help he got Achayan to sign the film to produce it.
There on Priyadarshan and Meera fall deeply in love and meet quite often. One evening Meeras father (Devan) come home buying Revathy (Sarayu) his friends daughter and Meera some dresses and she comes across a Saree. Meeras father informs her that his friends son and family is coming over to see Meera for a proposal. Meera gets worried and refuses to get married, then Revathy tells Meeras father about Priyadarshan, in which he is not happy about. There becomes a conflict between Priyadarshan and Meeras father.

Priyadarshan gets upset and goes to his office asking some of them to come to Meeras house and show Meeras father that he has people who cares for him too. (Priyadarshan still believes Meera is a poor maid at that house.)
There the head and Kannappan go to Meeras house, the head compliments about Priyadarshan to Meeras father believing that he is the house owner (not her father).
The head says that it isnt right to marry off the maid to a wealthier man to Meeras father, Here Meeras father says that Meera is her daughter not the maid, and Priyadarshan didnt believe it at first thinking it was a drama to not give her to him until Meera admits it. Priyadarshan hearing this tears up then says "you were cheating me, werent you?" and then apologizes to Meeras father. While Priyadarshan is about leave he says in tears that he has never lied to anyone and that he doesnt have many dreams but marrying Meera was the only dream in his life, which has been destroyed to the head.
He leaves saying that he wont come to disrupt you again to Meeras father.
There is a confusion drama in the end happening with Vysakhan falling for Revathy, who is actually from a poor household. Vaishakan comes to Meeras house believing that Meeras father was actually Revathys father, but realizing she isnt he leaves and meets Achayan. Vaishakan questions Achayan about if he asked Priyadarshan to direct the film, Achayan replies saying the yes that was true. There shows a flashback With Priyadarshan and Achayan, in which Achayan says that he knew that the script that was presented to him was Priyadarshans story and Achayan offers some money in advance for Priyadarshan to do the film. Also, Achayan says that Malayalam Cinema needs directors with good will, Priyadarshan replies if he takes this money and do this film. Hell have the money to pay back his fathers loan but he wouldnt be a person with good will if he takes away the film Vaishakan intended on doing. Vaishakan realizes Priyadarshans pure heart and tears up for thinking that he would take the film. 
There it shows Priyadarshan leaving to his home in Thonakkad. While there he gets annoyed by comments hinting that he cheated the girl by the local people there.
While reaching home he sees Revathy, was the one saying that she is his girl. He then tells everyone this is not his girl, then he realizes that he didnt fall in love with Meera the poor maid, but Meera the person within. 
He sees a crowd of people and one man says that we all know that she isnt your girl, and they make way for Meera, her father, Vaishakan, the Head and Achayan. 
The head says that you fell in love with her thinking that she was poor but she loved you knowing that your poor, so that shows she is in love with you more than you are in with her.
Then Priyadarshan and Meera are seen going back to the city (showing that they got married) and there Vaishakans car and production crew passes by, Vaishakan and Achayan wave at them and they wave back as the film ends.

==Cast==
* Jayasuriya as Priyadarshan
* Manoj K Jayan as Vaishakan
* Bhama as Meera Sarayu as Revathy
* Lalu Alex
* Salim Kumar as Kannappan Devan as Meeras father
* Bheeman Raghu
* Jagathy Sreekumar as Achayan
* Sreelatha Namboothiri
* Chembil Ashokan as Vaishakans father
* Reshmi Boban as Vaishakans sister

== Production ==
Boban Samuel directed the film, that was produced Mammen John, Reena M John, which had the story, screenplay, dialogues by Krishna Poojapura, with music by R. Gautham, Pradeep Nair did the cinematography, editing was by V. T. Sreejith, Manu Jagadh did the art direction, designs by Jissen Paul and stills by Mahadevan Thampy.
The film was distributed by Kalasangham Release.

Shoot started on 26 January 2011 at Kochi and Thodupuzha.

==Soundtrack==
{{Infobox album|  
  Name        =  Janapriyan
| Artist      = R. Gautham
| Type        = soundtrack
| Image       = 
| Cover       = 
| Released    =  12 May 2011 (India)
| Recorded    = 2011 Feature film soundtrack
| Length      = 13:38
| Label       = 
| Music       = R. Gautham
| Producer    = Mammen John, Reena M John
  }}
The soundtrack of the film was released on 12 May 2011. It had music scored by R. Gautham. Lyrics were provided by Santhosh Varma, Manju Vellayani. There was no special launch for the audio release and the music were given straight through the audio sellers.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Duration !! Lyrics
|-
| "Erivenal " Jyothsna
| 4.35
| Santhosh Varma, Manju Vellayani 
|-
| "Nanmakalerum "
| K. J. Yesudas
| 4.33
| Santhosh Varma, Manju Vellayani 
|-
| "Pookkaithe "
| Madhu Balakrishnan
| 4.29
| Santhosh Varma, Manju Vellayani 
|}

==References==
 

==External links==
*  
*   at Metromatinee.com

 
 
 