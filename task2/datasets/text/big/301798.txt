Dirty Harry
 
 
{{Infobox film name           = Dirty Harry image          = Dirty harry.jpg image_size     = 225px border         = yes caption        = Theatrical release poster by Bill Gold director       = Don Siegel producer       = Don Siegel screenplay     = Harry Julian Fink R.M. Fink Dean Riesner John Milius  (uncredited)  story          = Harry Julian Fink R.M. Fink Jo Heims  (uncredited)  starring  Andy Robinson Harry Guardino Reni Santoni John Vernon music          = Lalo Schifrin cinematography = Bruce Surtees editing        = Carl Pingitore studio         = Malpaso Productions distributor    = Warner Bros. released       =   runtime        = 102 minutes country        = United States language       = English budget         = $4 million gross          = $35,976,000   
}}
Dirty Harry is a 1971 American  action film produced and directed by Don Siegel, the first in the Dirty Harry (film series)|Dirty Harry series. Clint Eastwood plays the title role, in his first outing as San Francisco Police Department (SFPD) detective Harry Callahan (character)|"Dirty" Harry Callahan. The film drew upon the actual case of the Zodiac Killer as the Callahan character seeks out a similar vicious psychopath. 
 The Enforcer in 1976, Sudden Impact in 1983 (directed by Eastwood himself), and The Dead Pool in 1988.

In 2012, the film was selected for preservation in the National Film Registry by the Library of Congress for being "culturally, historically, and aesthetically significant." 

==Plot== Andy Robinson) SFPD Harry Inspector Harry chief of Mayor (John Fillmore District.
 Potrero Hill.

Assigned a rookie partner, Chico Gonzalez (Reni Santoni), Callahan complains that he needs someone experienced because his partners keep getting injured or worse. When Scorpio kills a young black boy from another rooftop, the police believe the killer will next pursue a Catholic priest. Callahan and Gonzalez wait for Scorpio near a Catholic church where a rooftop-to-rooftop shootout ensues, with Callahan attempting to snipe Scorpio with a .458 Winchester Magnum rifle. Scorpio escapes, killing a police officer disguised as a priest.
 buries alive Mount Davidson. Scorpio brutally beats Callahan and tells him hes going to kill him and let the girl die anyway; Gonzalez comes to his partners rescue but is wounded. Callahan stabs Scorpio in the leg, but the killer escapes without the money. Gonzalez survives his wound, but decides to resign from the force at the urging of his wife.
 Running out demands a lawyer, Callahan tortures the killer by standing on the wounded leg. Scorpio confesses, but the police are too late to save the girl.
 searched Scorpios warrant and are inadmissible, and the District Attorney (Josef Sommer) has no choice but to let Scorpio go. Outraged, Callahan warns that Scorpio will kill again because of the excitement that killing gives him. Callahan proceeds to follow Scorpio on his own time. To thwart Callahan, Scorpio pays to have himself beaten by a thug, then claims the inspector is responsible. Despite Callahans protests, he is ordered to stop following Scorpio.
 kidnaps a school bus load of children and demands another ransom and a plane to leave the country. The Mayor again insists on paying but when offered to deliver the ransom again, Callahan angrily refuses. Callahan, fed up with the way the city is handling the situation with Scorpio, instead pursues Scorpio without authorization, jumping onto the top of the bus from a railroad trestle. The bus crashes into a dirt embankment and Scorpio flees into a nearby rock quarry, where he has a running gun battle with Callahan. Scorpio keeps running until he spots a young boy (Andy Robinsons stepson Steve Zachs in real life) sitting near a pond, and grabs him as a hostage.

The inspector feigns surrender, but fires, wounding Scorpio in his left shoulder. The boy runs away and Callahan stands over Scorpio, gun drawn. The inspector reprises his "Do you feel lucky, punk?" speech. Scorpio lunges for his gun, Callahan kills him instantly. As Callahan watches the dead body, he takes out his inspectors badge. After contemplating what will happen to him as a result of his actions, he hurls the badge into the water before walking away.

==Cast== SFPD Homicide Harry Callahan Andy Robinson as Scorpio SFPD Homicide Lt. Al Bressler SFPD Homicide Inspector Chico Gonzalez
* John Vernon as The Mayor of San Francisco Chief of Police SFPD Homicide Inspector Frank "Fatso" DiGiorgio
* Woodrow Parfrey as Jaffe
* Josef Sommer as District Attorney William T. Rothko
* Albert Popwell as Bank Robber
* Lyn Edgington as Norma Gonzalez
* Ruth Kobart as Marcella Platt (School Bus Driver)
* Lois Foraker as Hot Mary
* William Patterson as Bannerman
* Craig Kelly as Reineke

==Production==

===Development=== Harry Julian Harry Callahan, determined to stop Travis, a serial killer, by any means at his disposal. Hughes, p. 49.  McGilligan (1999), p. 205.  The original draft ended with a police sniper, instead of Callahan, taking out Scorpio. Another earlier version of the story was set in Seattle, Washington. Four more drafts of the script were written.

Although Dirty Harry is arguably Clint Eastwoods signature role, he was not a top contender for the part. The role of Harry Callahan was offered to   and Burt Lancaster.  Mitchum dismissed this totemic role as "a piece of junk, and Marlon Brando."    In Dick Lochtes article, "Just One More Hangover: A Vodka-Soaked Afternoon with Robert Mitchum", he writes:
 Mitchum always got "those prices" in those days. "Somebody says, We really want you to do this script. And I say, Id need an awful lot of money in front to do that one. And that never seems to be a problem. The less I like the script, the higher my price. And they pay. They may pay in yen, but they pay. Not that Im a complete whore, understand. There are movies I wont do for any amount. I turned down Patton (film)|Patton and I turned down Dirty Harry. Movies that piss on the world. If Ive got $5 in my pocket, I dont need to make money that fucking way, daddy. 
Robert Mitchums brother John Mitchum would appear in three of the Dirty Harry films as Callahans sometimes partner Frank DiGiorgio.

When producer Jennings Lang initially could not find an actor to take the role of Callahan, he sold the film rights to ABC Television. Although ABC wanted to turn it into a television film, the amount of violence in the script was deemed too excessive for television, so the rights were sold to Warner Bros. Eliot (2009), p. 134. 

Warner Bros. purchased the script with a view to cast Frank Sinatra in the lead. Sinatra was 55 at the time and since the character of Harry Callahan was originally written as a man in his mid to late 50s (and Eastwood only then 41), Sinatra fit the character profile. Initially, Warner Bros. wanted either Sydney Pollack or Irvin Kershner to direct.  Kershner was eventually hired when Sinatra was attached to the title role. But when Sinatra eventually left the film, so did Kershner. 

John Milius was asked to work on the script when Sinatra was attached, along with Irvin Kershner as director. Milius claimed he was requested to write the screenplay for Frank Sinatra in three weeks. 

 I was the young hot guy there, then. The new thing at Warner Bros. ... John Calley called me in and he said, "We have this meeting with Frank Sinatra. We have to have a script to show him, and we dont really have it, nothings any good. Were going to do this, this is what were going to do. Can you do this in three weeks?" I said, "I dont recommend it, but it can be done." And I remember that was one of the first movies where I made them give me a gun. I had this gun in mind, I knew where this gun was. I made them give me a $2,000 gun, I remember. I had to have the gun, and they said, "Well send for the gun." I said, "No, you dont understand. If I dont have the gun today, when the gun comes here, Ill have to stop everything just to look at it for a whole day, and that will slow everything up." So they sent a limo for the gun, or a station wagon or something, for the gun. Brought the gun over, a wonderful gun. Unfortunately, I traded it off over the years. I looked at the gun for the rest of the day, then I started the thing and wrote it in 21 days. And thats Dirty Harry.  

Terrence Malick wrote a draft of the film dated November 1970 (John Milius and Harry Julian Fink are also named as co-writers) in which the shooter (also named Travis) was a vigilante who killed wealthy criminals who had escaped justice. Malick, Milius & Fink, Dirty Harry November 1970 Script.  Malicks ideas formed the basis for the sequel, Magnum Force, though with a group of vigilante motorcycle cops instead of a single shooter.

Details about the film were first released in film industry trade papers in April, September and November 1970 with Frank Sinatra attached as Harry Callahan and Irvin Kershner listed as director and producer with Arthur Jacobson acting as associate producer. 
 The French Connection the same year, giving the same reason. Believing the character was too "right-wing" for him, Newman suggested that the film would be a good vehicle for Eastwood. Eliot (2009), p. 133. 

The screenplay was initially brought to Eastwood’s attention around 1969 by Jennings Lang. Warner Bros offered him the part while still in post-production for his directorial debut film Play Misty for Me. By December 17, 1970, in a Warner Brothers studio press release it was announced that Clint Eastwood would star in as well as produce the film through his company, Malpaso.

Eastwood was given a number of scripts, but he ultimately reverted to the original as the best vehicle for him. Richard Schickel commentary, Dirty Harry Two-Disc Special Edition DVD.  In a 2009 MTV Interview, Eastwood said, "So I said, Ill do it, but since they had initially talked to me, there had been all these rewrites. I said, Im only interested in the original script." Looking back on the 1971 Don Siegel film, he remembered, "  everything. They had Marine snipers coming on in the end. And I said, No. This is losing the point of the whole story, of the guy chasing the killer down. Its becoming an extravaganza thats losing its character. They said, OK, do what you want. So, we went and made it."   

Eastwood also agreed to star in the film only on the provision that Don Siegel direct. Siegel was under contract to Universal at the time, and Eastwood personally went to the studio heads to ask them to "loan" Siegel to Warner. The two had just completed the movie The Beguiled (1971). 
 Andy Robinson. pacifist who deplores the use of firearms. In the early days of principal photography, Robinson would reportedly flinch in discomfort every time he was required to use a gun. As a result, Siegel was forced to halt production briefly and sent Robinson for brief training in order to learn how to fire a gun convincingly. 

Director John Milius owns one of the actual Model 29s used in principal photography in  Dirty Harry and Magnum Force.  As of March 2012, it is on loan to the National Firearms Museum in Fairfax, Virginia, and is on display in the Hollywood Guns gallery. 

===Principal photography===
Glenn Wright, Eastwoods costume designer since Rawhide, was responsible for creating Callahans distinctive old-fashioned brown and yellow checked jacket to emphasize his strong values in pursuing crime.  Filming for Dirty Harry began in April 1971 and involved some risky stunts, with much footage shot at night and filming the city of San Francisco aerially, a technique which the film series is renowned for.  Eastwood performed the stunt in which he jumps onto the roof of the hijacked school bus from a bridge, without a stunt double. His face is clearly visible throughout the shot. Eastwood also directed the suicide-jumper scene.

The line, "My, thats a big one," spoken by Scorpio when Callahan removes his gun, was an Ad libitum|ad-lib by Robinson. The crew broke into laughter as a result of the double entendre and the scene had to be re-shot, but the line stayed.
 High Noon. Eliot (2009), p.138  Eastwood initially did not want to toss the badge, believing it indicated that Callahan was quitting the police department. Siegel argued that tossing the badge was instead Callahans indication of casting away the inefficiency of the police forces rules and bureaucracy.  Although Eastwood was able to convince Siegel not to have Callahan toss the badge, when the scene was filmed, Eastwood changed his mind and went with the current ending. 

===Filming locations===
One evening Eastwood and Siegel had been watching the San Francisco 49ers in the Kezar Stadium in the last game of the season and thought the eerie Greek amphitheater-like setting would be an excellent location for shooting one of the scenes where Callahan encounters Scorpio. McGilligan (1999), p. 206 

In San Francisco, California
* 555 California Street, The Bank of America Building
* California Hall, 625 Polk Street (formerly the California Culinary Academy)
* San Francisco City Hall, 1 Dr. Carlton B. Goodlett Place
* Hall of Justice – 850 Bryant Street Forest Hill Station Holiday Inn Chinatown, 750 Kearny Street - rooftop swimming pool   in the opening scenes. It is now the Hilton - Chinatown. 
* Kezar Stadium – Frederick Street, Golden Gate Park
* Dolores Park, Mission District Mount Davidson Washington Square, 666 Filbert Street  Washington Square, North Beach  Washington Square  Washington Square  Washington Square 
* Big Als strip club, 556 Broadway
* Roaring 20s strip club, 552 Broadway
* North Beach, San Francisco

In Marin County, California Larkspur Landing Shopping Center and Larkspur Shores Apartments,  north of the Larkspur Ferry Terminal  
* Greenbrae, California
* Mill Valley, California

In Los Angeles County, California
* Universal Studios Hollywood&nbsp;— San Francisco Street (Hot dog café / Bank robbery sequence)

==Music==
The soundtrack for Dirty Harry was created by composer   and the Bullitt soundtrack, and who had previously collaborated with director Don Siegel in the production of Coogans Bluff (film)|Coogans Bluff and The Beguiled, both also starring Clint Eastwood. Schifrin fused a wide variety of influences, including classical music, jazz, psychedelic rock, along with Edda DellOrso-style vocals, into a score that "could best be described as acid jazz some 25 years before that genre began." According to one reviewer, the Dirty Harry soundtracks influence "is paramount, heard daily in movies, on television, and in modern jazz and rock music."  

==Release==

===Critical reception=== Feminists in particular were outraged by the film and at the 44th Academy Awards protested outside the Dorothy Chandler Pavilion, holding up banners which read messages such as "Dirty Harry is a Rotten Pig". McGilligan (1999), p. 211 
 fascist moral Philippine police force ordered a print of the film for use as a training film.  
 the greatest Vanity Fair also included the film on their lists of the 50 best movies.  
 review aggregate Best Motion Picture. 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  Dirty Harry was listed at 78th place on this list. 

===Box office performance=== world premiere Loews Theater fifth highest-grossing film of 1971, earning an approximate total of $36 million in its U.S. theatrical release,  making it a major financial success in comparison with its modest $4 million budget. 

===Home media=== Warner Home Video owns rights to the Dirty Harry series. The studio first released the film to VHS and Betamax in 1979. Dirty Harry (1971) has been remastered for DVD three times&nbsp;— in 1998, 2001 and 2008. It has been repurposed for several DVD box sets. Dirty Harry made its high-definition debut with the 2008 Blu-ray Disc. The commentator on the 2008 DVD is Clint Eastwood biographer Richard Schickel.  The film, along with its sequels, has been released in High Definition, on various Digital distribution services, including the iTunes Store. 

==Legacy== 100 series 100 Years... 100 Years 100 Years of Film Scores. 

==Real-life copycat crime and killers==
The film supposedly inspired a real-life crime, the Faraday School kidnapping.  In October 1972, soon after the release of the movie in Australia, two armed men (one of whom coincidentally had the last name Eastwood) kidnapped a teacher and six school children in Victoria, Australia|Victoria. They demanded a $1 million ransom. The state government agreed to pay, but the children managed to escape and the kidnappers were subsequently jailed. 

On September 1981, a case occurred in  , was buried alive in a box fitted with ventilation, lighting and sanitary systems to be held for ransom. The girl suffocated in her prison within 48 hours of her abduction because autumn leaves had clogged up the ventilation duct. 27 years later, a couple was arrested and tried for kidnapping and murder on circumstantial evidence. According to the Daily Mail, the couple were inspired by the film Dirty Harry, in which "Scorpio" Davis kidnaps a girl and places her in an underground box.  This case was also dealt with in the German TV series Aktenzeichen XY ... ungelöst.

==Influence==
  four sequels.
 Black Cobra, The Mask British virtual band Gorillaz also titled a song called Dirty Harry on their album Demon Days.
The film can also be counted as the seminal influence on the Italian tough-cop films, Poliziotteschi, which dominated the 1970s and that were critically praised in Europe and the U.S. as well.
 Star Wars fame. The poll surveyed approximately two thousand film fans.  However, the only appearances of the Model 29 in the movie are in the close-ups: Any time Eastwood actually fired the revolver, he was shooting a Smith & Wesson Model 25 in .45 Long Colt.  The reason was in 1971 .45 caliber 5-in-1 blank cartridges were readily available, while .44 caliber blanks did not exist. As the Model 25 and Model 29 are both built on the Smith & Wesson N frame, visually they are almost indistinguishable.

In 2010, artist James Georgopoulos included the screen-used guns from Dirty Harry in his popular Guns of Cinema series.  

The 2013 satirical comedy film InAPPropriate Comedy parodied the character Dirty Harry with a fake trailer called "Flirty Harry", in which Flirty Harry was played by Adrien Brody.

==See also==
* Zodiac Killer in popular culture
* Vigilante film

==References==
Notes
 

Bibliography
*  .
*  
*  
*  

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 