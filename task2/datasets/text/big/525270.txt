Kill Bill Volume 2
{{Infobox film
| name           = Kill Bill Volume 2
| image          = Kill bill vol two ver.jpg
| caption        = Theatrical poster
| director       = Quentin Tarantino
| producer       = Lawrence Bender
| writer         = Quentin Tarantino
| story          = Quentin Tarantino Uma Thurman  (character The Bride) 
| starring       = Uma Thurman David Carradine Lucy Liu Vivica A. Fox Michael Madsen Daryl Hannah Gordon Liu The RZA Robert Richardson
| editing        = Sally Menke
| studio         = A Band Apart
| distributor    = Miramax Films
| released       =  
| runtime        = 136 minutes
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $152.1 million
}}

 
 martial arts action film written and directed by Quentin Tarantino. It is the second of two volumes that were released several months apart. It was originally scheduled for a single theatrical release, but was divided into two films with its running time being over four hours. Kill Bill Volume 1 was released in late 2003, and Kill Bill Volume 2 was released in early 2004.
 The Bride", a former member of an assassination team who seeks revenge on her ex-colleagues who massacred members of her wedding party and tried to kill her.

==Plot==
The pregnant Bride and her groom rehearse their wedding. Bill, the Brides former lover, father of her child, and leader of the Deadly Viper Assassination Squad, arrives unexpectedly. On Bills orders, the Deadly Vipers kill everyone at the wedding, but the Bride survives and swears revenge.
 Vernita Green Budd (Michael Elle Driver buries her alive.
 death blow that Mei refuses to teach his students; the technique will supposedly kill any opponent when he or she takes five steps. Bill takes the Bride to Meis temple to be trained by him. Mei ridicules her and makes her training a torment, but she gains his respect. In the present, the Bride uses Meis martial arts techniques to break out of the coffin and claw her way to the surface.

Elle arrives at Budds trailer and kills him with a black mamba hidden with the money for his sword. She calls Bill and tells him that the Bride has killed Budd, and that Elle has killed the Bride. She uses the Brides real name: Beatrix Kiddo. As Elle exits the trailer, Beatrix ambushes her and they fight. Elle, who was also taught by Pai Mei, reveals that she poisoned him in retribution for plucking out her eye. Beatrix plucks out Elles remaining eye and leaves her screaming in the trailer with the black mamba.

Beatrix tracks Bill to the Mexican countryside and discovers that their daughter B.B. (Perla Haney-Jardine) is alive and well, now aged four. She spends the evening with Bill and B.B. After Beatrix puts B.B. to bed, Bill shoots Beatrix with a dart containing truth serum and interrogates her.

Years earlier, Beatrix is on a mission to assassinate Lisa Wong. As she discovers she is pregnant with Bills child, she is confronted in her hotel room by Karen Kim, an assassin sent by Lisa Wong to kill Beatrix. When Karen learns Beatrix is pregnant, they agree to abort their missions and go home.

In the present, Beatrix tells Bill that she left him and the Deadly Vipers to give their child a better life. She disables Bill with the Five Point Palm Exploding Heart Technique, which Mei secretly taught her. Bill makes his peace with her, takes five steps and dies. Beatrix leaves with B.B. to start a new life.

==Cast==
  The Bride / Beatrix Kiddo (Black Mamba): A former member of the Deadly Viper Assassination Squad who is described as "the deadliest woman in the world". She is targeted by her former allies in the wedding chapel massacre, and falls into a coma. When she awakens four years later, she embarks on a deadly trail of revenge against the perpetrators of the massacre. Bill (Snake Charmer): The former leader of the Deadly Viper Assassination Squad. He is also the former lover of Beatrix and the father of her daughter. He is the final and eponymous target of Beatrixs revenge.
* Lucy Liu as List of Kill Bill characters#O-Ren Ishii / Cottonmouth|O-Ren Ishii (Cottonmouth): A former member of the Deadly Viper Assassination Squad. She later becomes "Queen of the Tokyo Underworld". She is the first of Beatrixs revenge targets. Vernita Green (Copperhead): A former member of the Deadly Viper Assassination Squad. She later becomes a homemaker living under the false name Jeannie Bell. She is the second of Beatrixs revenge targets. Budd (Sidewinder): A former member of the Deadly Viper Assassination Squad and brother of Bill. He later becomes a bouncer living in a trailer. He is the third of Beatrixs revenge targets. Elle Driver (California Mountain Snake): A former member of the Deadly Viper Assassination Squad. She is the fourth of Beatrixs revenge targets. Pai Mei: An immensely powerful and extremely old martial arts master. Bill, Beatrix, and Elle all train under him. Esteban Vihaio: A retired pimp. He was the first of Bills “father figures”. Beatrix comes to him asking for Bills whereabouts.
* Bo Svenson as  Reverend Harmony: The minister who was to officiate at Beatrix and Tommys wedding.
* Jeannie Epper as  Mrs. Harmony: Reverend Harmonys wife.
* Samuel L. Jackson as List of Kill Bill characters#Rufus|"Rufus": The organist who was to perform at Beatrix and Tommys wedding. Larry Gomez: The abusive manager of the strip club at which Budd works.
* Sid Haig as Jay: A bartender at the strip club where Budd works.
* Perla Haney-Jardine as List of Kill Bill characters#B.B.|B.B.: The daughter of Beatrix and Bill. She is raised by her father while her mother is comatose.
* Helen Kim as List of Kill Bill characters#Karen Kim|Karen: An assassin sent to kill Beatrix. Her attack comes moments after Beatrix learns that she is pregnant. Tommy Plympton: Beatrixs fiancé who is killed in the wedding chapel massacre.
* Laura Cayouette as Rocket: An employee at the strip club who orders Budd to clean up a flooded toilet.
* Clark Middleton as Ernie: The man assisting Budd in burying Beatrix alive.
* Stephanie L. Moore, Shana Stein, and Caitlin Keats as Joleen, Erica, and Janeen, Beatrixs best friends who are present at the wedding rehearsal.
* Lawrence Bender (uncredited) as Hotel clerk

==Music==
 
As with Tarantinos previous films, Kill Bill features an eclectic soundtrack comprising many musical genres. On the two soundtracks, music ranges from country music to selections from the Spaghetti Western film scores of Ennio Morricone. Bernard Herrmanns theme from the film Twisted Nerve is whistled by the menacing Elle Driver in the hospital scene. A brief, 15-second excerpt from the opening of the Ironside (TV series)|Ironside theme music by Quincy Jones is used as the Brides revenge motif, which flares up with a red-tinged flashback whenever shes in the company of her next target.  Instrumental tracks from Japanese guitarist Tomoyasu Hotei figure prominently, and after the success of Kill Bill they were frequently used in American TV commercials and at sporting events. The end credits are driven by the rock and roll version of "Malagueña Salerosa", a traditional Mexican song, performed by "Chingon (band)|Chingon", Robert Rodriguezs band.

==Release==

===Theatrical release===
  The Punisher. Volume 2 s opening weekend gross was higher than Volume 1 s, and the equivalent success confirmed the studios financial decision to split the film into two theatrical releases.  Volume 2 attracted more female theatergoers than Volume 1, with 60% of the audience being male and 56% of the audience being men between the ages of 18 to 29 years old. Volume 2 s opening weekend was the largest to date for Miramax Films aside from releases under its arm Dimension Films. The opening weekend was also the largest to date in the month of April for a film restricted in the United States to theatergoers 17 years old and up, besting Life (1999 film)|Life s 1999 record. Volume 2 s opening weekend was strengthened by the reception of Volume 1 in the previous year among audiences and critics, abundant publicity related to the splitting into two volumes, and the DVD release of Volume 1 in the week before Volume 2 s theatrical release. 

Outside of the United States and Canada, Volume 2 was released in 20 territories over the weekend of  , 2004. It grossed an estimated   and ranked first at the international box office, ending an eight-week streak held by The Passion of the Christ.  Volume 2 grossed a total of   in the United States and Canada and   in other territories for a worldwide total of  . 

===Home release===

In the United States, Volume 2 was released on DVD and VHS on August 10, 2004.

In a December 2005 interview, Tarantino addressed the lack of a special edition DVD for Kill Bill by stating "Ive been holding off because Ive been working on it for so long that I just wanted a year off from Kill Bill and then Ill do the big supplementary DVD package."   

The United States does not have a DVD boxed set of Kill Bill, though box sets of the two separate volumes are available in other countries, such as France, Japan and the United Kingdom. Upon the DVD release of Volume 2 in the US, however, Best Buy did offer an exclusive box set slipcase to house the two individual releases together.   
 High Definition on Blu-ray Disc|Blu-ray on September 9, 2008 in the United States.

===The Whole Bloody Affair===
Tarantino announced at the 2008  .  This was verified to be the original print that premiered at the Cannes Film Festival in 2003,    before the decision was made to split the film into two parts due to the roughly four-hour length.

==Critical reception==

Kill Bill Volume 2 received positive reviews from film critics, with many praising its direction and saying Tarantino had matured as a filmmaker. For Volume 2, review aggregator Rotten Tomatoes gives the film a score of 84% based on reviews from 230 critics and reports a rating average of 7.7 out of 10. It reported the consensus, "Talkier and less action-packed than Vol. 1, Kill Bill Vol. 2, nevertheless, delivers the goods for those expecting a satisfying conclusion to this two-parter."  At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 83 based on 42 reviews. 

Roger Ebert celebrated the films, saying "Put the two parts together, and Tarantino has made a masterful saga that celebrates the martial arts genre while kidding it, loving it, and transcending it.... This is all one film, and now that we see it whole, its greater than its two parts."  In 2009, he placed the film on his twenty best films of the decade list. 

===Accolades=== Best Actress Best Supporting Actor nomination in the same year. Kill Bill Volume 2 was placed in Empire (film magazine)|Empire Magazines list of the "500 Greatest Films of All Time" at number 423 and the Bride was also ranked number 66 in Empire magazines "100 Greatest Movie Characters". 

{| class="wikitable" 
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|- 10th Empire Awards
|- Empire Award Best Film
|Kill Bill Volume 2
| 
|- Empire Award Best Actress Uma Thurman
| 
|- Empire Award Best Director Quentin Tarantino
| 
|- Sony Ericsson Scene of the Year
|"The Bride" versus "Elle" sequence
| 
|- 62nd Golden Globe Awards Golden Globe Best Actress – Drama Uma Thurman
| 
|- Golden Globe Best Supporting Actor David Carradine
| 
|- 2005 MTV Movie Awards MTV Movie Best Movie
|Kill Bill Volume 2
| 
|- MTV Movie Best Female Performance Uma Thurman
| 
|- MTV Movie Best Fight Uma Thurman vs. Daryl Hannah
| 
|- Golden Satellite 2004 Satellite Awards
|- Satellite Award Best Film-Drama
|Kill Bill Volume 2
| 
|- Satellite Award Best Actress - Motion Picture Drama Uma Thurman
| 
|- Satellite Award Best Supporting Actor – Drama David Carradine
| 
|- Satellite Award Best Supporting Actress – Drama Daryl Hannah
| 
|- 31st Saturn Awards
|- Saturn Award Best Action/Adventure Film
|Kill Bill Volume 2
| 
|- Saturn Award Best Actress Uma Thurman
| 
|- Saturn Award Best Supporting Actor David Carradine
| 
|- Saturn Award Best Supporting Actress Daryl Hannah
| 
|- Saturn Award Best Younger Actor/Actress Perla Haney-Jardine
| 
|- Saturn Award Best Director Quentin Tarantino
| 
|- Saturn Award Best Screenplay Quentin Tarantino
| 
|}

==Possible sequel==
In April 2004, Tarantino told Entertainment Weekly in April 2004 that he was planning a sequel: 
 

At the   about Bill and his mentors, and another origin starring the Bride.  

Details emerged around 2007 about two possible sequels, Kill Bill Volumes 3 and 4. According to the article, "the third film involves the revenge of two killers whose arms and eye were hacked by Uma Thurman in the first stories." The article adds that the "fourth installment of the popular kung fu action films concerns a cycle of reprisals and daughters who avenge their mothers deaths". 

At the 2009 Morelia International Film Festival, films, Tarantino stated that he intended to make a third Kill Bill film.  The same month, he stated that Kill Bill 3 would be his ninth film, and would be released in 2014.  He stated that he wanted ten years to pass after the Brides last conflict, to give her and her daughter a period of peace. 

In December 2012, Tarantino stated: "I dont know if theres ever going to be a Kill Bill Vol. 3. Well see, probably not though." 

==See also==
* Zoë Bell Lady Snowblood
* Kill Buljo
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   Article from CGSociety.org

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 