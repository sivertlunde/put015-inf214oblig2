Pleasant Moments
 
{{Infobox film
| name           = Pleasant Moments
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Věra Chytilová
| producer       = 
| writer         = Věra Chytilová
| narrator       = 
| starring       = Jana Janeková
| music          = 
| cinematography = Martin Strba
| editing        = Jirí Brozek
| studio         = 
| distributor    = 
| released       = 21 September 2006
| runtime        = 113 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech drama film directed by Věra Chytilová. It was released in 2006.

==Cast==
* Jana Janeková - Hana
* Jana Krausová - Eva
* Bolek Polívka - Dub (as Boleslav Polívka)
* David Kraus - Pavel
* Igor Bares - Karel
* Martin Hofmann - Petr
* Miroslav Hájek - Honzík
* Ivana Milbachová
* Jirí Ornest - Benda
* Katerina Irmanovová - Bendová
* Barbora Hrzánová - Sára (as Bára Hrzánová)

==External links==
*  

 

 
 
 
 
 
 