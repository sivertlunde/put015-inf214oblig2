The Battle of Waterloo (film)
 
{{Infobox film name     = The Battle of Waterloo image    = Battle of Waterloo 1914 ad.jpg director = Charles Weston writer   =  producer = {{plainlist|
* Ernest Batley
* Will Barker
* Vivian Ross
}} starring = George Foley released =   runtime  = 86 minutes language = Silent budget   =   country  = United Kingdom music    =
 |
}} the eponymous battle ahead of its centenary. Hailed  as the "first British epic film", The Battle of Waterloo was much longer and more costly than contemporary films but went on to great commercial and critical success. Though the film was shown in theaters around the world, all copies were thought lost until 2002, when about 22 minutes of the hour-and-a-half production were rediscovered at the British Film Institute archives. Since then, two reels and a fragment have been compiled, representing about half the completed film.

== Inspiration == vignettes of a single subject, films evolved to include multiple scenes, locations and actors. In 1910, Barker Motion Photography released Henry VIII, a 30-minute recreation of the Shakespearean play.  The success of this movie, (the first lengthy production by a British film studio) and similar foreign productions encouraged British and Colonial Films to produce its own feature film. The company, based in East Finchley, London, had already created several short films and documentaries, but The Battle of Waterloo was its longest production to date.

== Production ==
To direct the film, British and Colonial Films hired American director Charles Weston. To raise money for the production, John Benjamin McDowell, one of the founders of British and Colonial, remortgaged the company for  . Weston chose to film the production in Irthlingborough, Northamptonshire, a place the Duke of Wellington reportedly said reminded him of the terrain around Waterloo, Belgium.   bbc.com. 7 June 2013. Retrieved 3 September 2014.  Hundreds of local residents were used as extras. Some were paid, while others volunteered. There were so many volunteers that two shoe factories in the town had to close for lack of workers.   Subsequent advertisements indicated the movie contained 2,000 soldiers, 116 scenes, 1,000 horses and 50 cannons. Paget, George Charles Henry Victor. A History of British Cavalry: Volume 4, 1899-1913. Pen and Sword, 1993. pp. 486 
 12th Lancers Weedon Barracks. More than 100 horses came from the London stables of Thomas Tilling, which at the time was the biggest supplier of horsepower in London. The regimental historian recorded, "An accommodating American made the rounds of all the pubs at night to pay for drinks. The fact that Napoleon could not ride and that a sergeant in the regiment appropriated Wellingtons boots nearly prevented the film being made and C Sqn from taking part in the most exciting, best paid and least painful battle of the regiments long history." 

Despite the complexities of the production, filming was completed in just five days, and the resulting edited film encompassed about   of film over five reels. McKernan, Luke.  , 1996. Retrieved 3 September 2014. 

== Reception ==
Commercially, the film returned John Benjamin McDowells investment many times over. British display rights alone were sold for  , and international display rights earned the company even more.  Critically, the film received mixed reviews. Bioscope, a British film journal, praised British and Colonials effort to preserve a portion of British history with a British production. It tempered this praise by noting that the film recreated scenes from the battle "from the point of view of an ordinary soldier in the thick of the battle," but there was almost no dramatic or human interest. 

The film was popular enough that a parody, Pimples Battle of Waterloo, was hurriedly put into production and released later that year. 

After the success of The Battle of Waterloo, British and Colonial continued to produce longer films. After the start of World War I, many of the same filmmakers who produced Waterloo were put to work on propaganda films, the most famous of which is The Battle of the Somme (film)|The Battle of the Somme.

The original prints of The Battle of Waterloo were made on nitrate film and have since been destroyed by irreversible nitrate decomposition. Only fragments survive in the British Film Institute archive. The entire film was thought lost until 2002, when 22 minutes were rediscovered in the archive. Additional fragments have been compiled, the equivalent of about two and a half reels of film. 

== References ==
* Turvey, Gerry. "The Battle of Waterloo (1913): The First British Epic," in Burton and Porter (eds) The Showman, the Spectacle and the Two-Minute Silence: Performing British Cinema before 1930. Trobridge: Flicks Books, pp.&nbsp;40–47.
* "The Battle of Waterloo": British History Reconstructed by Britons, Bioscope. 3 July 1913, pp.&nbsp;51.
* Our Poster Gallery: "The Battle of Waterloo", Bioscope, 14 August 1913. pp.&nbsp;24.

== Notes ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 