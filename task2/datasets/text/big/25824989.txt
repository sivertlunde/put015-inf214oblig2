Jeevan Baator Logori
{{Infobox film
| name           = Jeevan Baator Logori
| image          = jeevan_baator_logori.jpg
| alt            =  
| caption        = Film poster of Jeevan Baator Logori
| director       = Timothy Das Hanche  Lohit Dutta (Assistant)
| producer       = Phukan Konwar  Purnananda Gogoi  Beauty Baruah
| writer         = Timothy Das Hanche
| starring       = Nipon Goswami  Kapil Bora  Hiranya Deka Suchandra Chandra  Moloya Goswami  Mahika Sharma
| music          = Biman Baruah  Arup Dutta  Timothy Das Hanche
| cinematography = Sibanan Baruah
| editing        = Debankur Borgohain
| studio         = 
| distributor    = Hills Motion Pictures Association
| released       =  
| runtime        = 118 minutes
| country        = India
| language       = Assamese
| budget         = 15 Lacs
| gross          = 41 Lacs
}}
Jeevan Baator Logori ( ) is an Assamese language feature film directed by Timothy Das Hanche under the banner Hills Motion Pictures Association of Diphu.        The film is set in the rural and urban areas of Assam and shot at Cinemascope at 35mm format.  

==Synopsis==
The film shows the journey of a young man from his college days to his professional life as a businessman. Problems like unemployment and agricultural underproduction comes as subplots.  Jeevan Baator Logori focuses on some significant aspects of our society like chaotic education system, young generation becoming self-reliant through agriculture or through proper acquisition of some other benevolent schemes. It carries a strong message for the young generation to preserve and uplift the traditional values while accepting modernity. 

==Plot==
Pabitra Baruah is a principal of a high school. He is living a happy and contented life with his caring wife Prafulli and a pair of studious twin children Pritom and Tarali. As a leading villager, Pabitra always stands for the moral values in running the school affairs as well as his daily life. Pritom and Tarali, as expected, top their matriculation and leave for Guwahati for higher studies. But the proud parents do not realise that their children left them forever.
 MNC in Mumbai, gets married to the daughter of his boss and settles down there. Torali, too, after completion of her higher studies in the U.S., gets married to an American and settles there. Both of them are so busy and pre-occupaid with their own life they even could not come back home at the death of their mother.

Dinanath Bora is a farmer with his teacher wife Nirmali and children Mohen and Malati. He possesses strong faith and believes in work culture and is always busy with his agricultural activities. The only thing the couple is worried about is their children’s negligence towards studies. Mohen is always busy rehearsing his Bhaona and Malati with singing and dancing around with her folks. The children fail their matriculation. But the parents shows them other fruitful means to shape their lives. Mohan with a financial loan buys a tractor and gets fully involved in agricultural activities. Malati with other girls from the village forms a financial Self-help group (finance)|self-help group, undergoes training on fruit processing and preservation and opens up a big shop of pickle, jam, jelly and other food materials, etc. in the village. For Pabitra Barua, the neighboring people become the source of succour and an integral part for the rest of his life. 

==Reception==
The film was released through satellite using UFO Moviez technology simultaneously in 25 theatres in Assam. Its box office success came as a refreshing change for the film industry, which had not seen a commercial success for a long time.     On November 15, 2009, at 3 pm, the film was screened at Xankardev Bhawan, Qutub Institutional Area, New Delhi. 

==Casts==
*Nipon Goswami 
*Bishnu Kharghoria
*Arun Nath
*Moloya Goswami
*Beauty Baruah
*Hiranya Deka
*Atul Pasani
*Kapil Bora
*Parineeta (Mumbai)
*Suchandra Chandra (Kolkata)
*Biki
*Raag Oinitom
*Shyamanitka Sarma
*Asha Bordoloi
*Devananda Saikia
*Kishore Choudhury
*Dilip Goswami
*Rup Goswami
*Julen Bhuyan
*Dudul Baishya
*Mahika Sharma
*Narendra Chandra Sutradhar
*Dinesh Das (Guest Appearance)
*Ananya Pasani (Guest Appearance)

==See also==
*Jollywood

==References==
 

==External links==
* 
* 

 
 
 