Udayam Kizhakku Thanne
{{Infobox film 
| name           = Udayam Kizhakku Thanne
| image          =
| caption        =
| director       = P. N. Menon (director)|P. N. Menon
| producer       = VN Kochaniyan
| writer         = Thikkodiyan
| screenplay     = Thikkodiyan
| starring       = Kaviyoor Ponnamma Pattom Sadan Sukumaran Balan K Nair
| music          = K. J. Yesudas
| cinematography = K Ramachandrababu
| editing        = Ravi
| studio         = Sithara Films
| distributor    = Sithara Films
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by P. N. Menon (director)|P. N. Menon and produced by VN Kochaniyan. The film stars Kaviyoor Ponnamma, Pattom Sadan, Sukumaran and Balan K Nair in lead roles. The film had musical score by K. J. Yesudas.   

==Cast==
 
*Kaviyoor Ponnamma
*Pattom Sadan
*Sukumaran
*Balan K Nair
*Bhaskara Kurup
*Janardanan
*Kunjandi
*Ravi Menon
*S. P. Pillai
*Santha Devi Sujatha
*Sumithra Sumithra
*Umesh
 

==Soundtrack==
The music was composed by K. J. Yesudas and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Madamilakithullum || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Oh My Sweetie || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Thaaraapadhangale || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Thaaraapadhangale || P Susheela || Sreekumaran Thampi || 
|-
| 5 || Thendi Thendi Thengiyalayum || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 