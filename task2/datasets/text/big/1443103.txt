The Professionals (1966 film)
{{Infobox film
| name           = The Professionals
| image          = Movie poster for "The Professionals".jpg
| caption        = original film poster by Howard Terpning
| director       = Richard Brooks
| producer       = Richard Brooks
| writer         = Richard Brooks Frank ORourke (novel)
| starring       = Burt Lancaster Lee Marvin Claudia Cardinale Robert Ryan Woody Strode Jack Palance Ralph Bellamy
| cinematography = Conrad L. Hall
| music          = Maurice Jarre
| editing        = Peter Zinner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $19,537,346 
}} western starring Burt Lancaster, Lee Marvin, Claudia Cardinale, Robert Ryan, and Woody Strode. The supporting cast includes Jack Palance and Ralph Bellamy and the film was written and directed by Richard Brooks, whose screenplay was based upon the novel A Mule for the Marquesa by Frank ORourke.

The movie received three Academy Award nominations and an enthusiastic critical reception.

==Plot==
In the latter period of the Mexican Revolution, Rancher and Oil Tycoon J.W. Grant (Ralph Bellamy) hires four men, who are all experts in their respective fields, to rescue his kidnapped wife, Maria (Claudia Cardinale) from Jesus Raza (Jack Palance), a former Mexican Revolutionary leader turned bandit.
 horse wrangler scout and traditional Apache skills, particularly with a bow and arrow, and is also a crack shot with a rifle. Fardan and Dolworth, having both fought under the command of Pancho Villa during the first Mexican Revolution both speak Spanish and know the roads and terrain in Mexico. They both had served with Raza under General Villa and have a high regard for him as a soldier. But as hard and cynical professionals, they have no qualms about killing him now.
 Mexican border, the team tracks the bandits to their hideout. They bear witness as Mexican federal soldiers on a government train are attacked, defeated, and massacred by Razas small army. The professionals follow the captured train to the end of the line and retake it from the bandits. Some move on to the bandit camp and observe Raza and his followers — including a buxom female officer, named Chiquita (Marie Gomez), in Razas command. At nightfall Fardan, Dolworth, and Sharp, stage an attack on Razas compound using explosives to simulate a Mexican Army artillery barrage. Ehrengard stayed behind at the train to prepare their departure.  Then Fardan and Dolworth infiltrate Razas private quarters but they are  stopped from killing him by Maria, the kidnapped wife. "Amigo," Dolworth concludes, "weve been had."

Fardan and company do what they are being paid for and escape with Grants wife. Back at the train, the men find that it has been retaken by the bandits.  Ehrengard, who had been captured by the bandits, warns them of the ambush set by the bandits.  Ehrengard is wounded in the ensuing gun battle.  After a shootout, they retreat into the mountains, first on the train and then by horseback. They are hotly pursued by Raza and his men. The professionals evade capture by using explosives to bring down the prepared walls of a narrow canyon, thus blocking Razas  and the bandits path and delaying their pursuit. It is then revealed that they had not rescued his kidnapped wife but Razas willing mistress. Grant "bought" Maria for an arranged marriage only for her to escape and return to her "true love", Captain Jesus Raza, in Mexico.

But as Raza and his group of bandits relentlessly pursue the northbound retreating professionals, Dolworth fights the bandits in a rearguard action to allow the other professionals with Maria to escape to the United States border.  In a hit and run battle Dolworth kill all of Razas men, save Raza and Chiquita. The bandit leader Raza is wounded in the leg. Raza, along with fellow bandit Chiquita, attempts a last ditch attack on Dolworth.    But she is shot and mortally wounded by Dolworth in the gunfight. Dolworth, himself narrowly escapes death as she points a pistol at Dolworths head and pulls the trigger on the empty revolver, as she lies dying in Dolworths arms. Weakened by loss of blood, Raza falls of his horse and is captured by Dolworth.

The other professionals, with Maria reach the U.S. border to be met by Grant and his own men.  They wait just north of the river in an empty building to rest and wait for Dolworth.  Sharp and Ehrengard seem to be in no hurry to return Maria to Joe Grant.  Just as they had given up Dolworth for dead, Bill arrives at the border with a string of captured bandit horses and Raza in tow.  Bill tells Rico that as far as he is concerned, Grant is the kidnapper.  Just then Grant arrives with several of his men.  He tells Fardan that their contract has been satisfactorily concluded, even before Maria is safely handed over to him.

As Maria tends the wounded Raza on the ground, Grant callously turns to one of his men and says, "Kill him." But before the man can shoot, the gun is shot out of his hand by Dolworth who tells Grant he has not earned the right to kill a man like Raza. The four professionals brandish their pistols and rifles, and then step in to protect Maria and Raza. Maria, apparently, never was kidnapped from Grant by Raza after all.  She left Grant to return to Raza of her own free will. The ransom demand was a ruse to obtain money to fund the Revolution. Grant always knew that the kidnapping was a farce.  He never had any intention of paying the ransom. The professionals collect the wounded Raza, load him onto a wagon and, with Maria at the reins, send both back to Mexico.  Dolworth tells Grant that they both made a bad deal. That Grant loses his wife and the professionals their reward.

Grant calls Fardan a bastard, to which Fardan retorts: "Yes, sir, in my case an accident of birth. But you, sir, you are a self-made man." The professionals gather their horses, mount up, and follow the departing wagon to Mexico.

==Cast==
* Burt Lancaster ...  Bill Dolworth
* Lee Marvin ...  Henry Rico Fardan
* Claudia Cardinale ... Mrs. Maria Grant
* Robert Ryan ...  Hans Ehrengard
* Woody Strode ...  Jake Sharp
* Jack Palance ...  Jesus Raza
* Ralph Bellamy ...  Joe Grant
* Joe De Santis ...  Ortega
* Rafael Bertrand ...  Fierro
* Jorge Martínez de Hoyos ...  Goatkeeper
* Marie Gomez ...  Chiquita
* José Chávez (actor)|José Chávez ... Revolutionary Carlos Romero ...  Revolutionary
* Vaughn Taylor ...  Money-Delivering Banker

==Production notes==
 
During the filming of a scene where Maria attempts to escape through a canyon wired with dynamite, Cardinales stunt double was badly injured. Cardinale, who had never ridden a horse before, performed the stunt herself in the final cut.
 Las Vegas. The Pioneer Club.

Portions of the film were shot in the  )  The railway scenes were filmed on Kaiser Steels Eagle Mountain Railroad.  The steam locomotive seen in the movie currently resides on the Heber Valley Railroad.

Burt Lancaster and Lee Marvin did not get along during filming due to Marvins alcoholism, which was making him unreliable and difficult at the time. Marvins behavior was infuriating Lancaster, who prided himself as being a consummate actor who always showed up on time and ready to work. Director Richard Brooks, who had worked with Lancaster before, felt the need to intervene because he feared that Lancaster, a former circus acrobat and fearsome physical specimen, was going to "take Lee Marvin by the ass and throw him off that mountain". 
 Oscar for Cat Ballou (1965) and the year before his World War II smash hit The Dirty Dozen (1967). Marvins other most famous role, as Liberty Valance in John Fords The Man Who Shot Liberty Valance opposite James Stewart and John Wayne, had been filmed in 1962.
 The Killers, Elmer Gantry.

==Award & nominations== Best Director Best Writing Best Cinematography.
 Golden Screen Doctor Zhivago, You Only Live Twice) in 1967.

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 