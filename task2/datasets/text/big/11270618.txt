Dr. Kotnis Ki Amar Kahani
{{Infobox film
| name           = Dr Kotnis ki Amar Kahani (The Journey of Dr. Kotnis)
| image          = Dr_Kotnis_ki_Amar_Kahani_poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = V. Shantaram
| producer       = Rajkamal Kalamandir
| writer         = Khwaja Ahmad Abbas  V.P. Sathe
| narrator       = 
| starring       = V. Shantaram Jayashree music          = Vasant Desai
| cinematography = V. Avadhoot
| editing        = 
| distributor    = 
| released       = 1946
| runtime        = 124 min./ 100 min. (ENG)
| country        = India English
| budget         = 
}} 1946 Cinema Indian film in Hindi English and Japanese invasion in World War II.

==Overview==

The film was based on the story And One Did Not Come Back, by Khwaja Ahmad Abbas, which is itself based on the heroic life of Dr. Dwarkanath Kotnis, played by V. Shantaram in the film.

Dr. Kotnis was sent to China during the Second World War to provide medical assistance to the troops fighting against the Japanese invasion in Yenan province.
 Chinese girl Ching Lan, played by Jayashree. His main triumph was curing a virulent plague, but later he was captured and released by a Japanese platoon. Eventually he succumbed to the plague himself.

== Lead Cast ==

* Rajaram Vankudre Shantaram ...  Dwarkanath Kotnis 
* Jayashree ...  Ching Lan

== Rest of the cast listed alphabetically ==

* Keshavrao Date ...  Dr. Kotnis Father 
* Pratima Devi ...  Dr. Kotnis mother (as Pratimadevi) 
* Prof. Hudlikar ...  Dr. Atal 
* Jankidas ...  Dr. Mukerjee (as Janki Dass) 
* Baburao Pendharkar ...  General Fong 
* Rajasree ...  Dr. Kotniss son 
* Salvi (caste)|Salvi ...  Dr. Cholkar 
* Ulhas ...  Dr. Basu 
* Master Vinayak ...  Bundoo

==Poster and artwork==
The Poster and artwork were designed and executed by the noted Calendar artist S. M. Pandit through his Studio S. M. Pandit.  

==See also==
* Dwarkanath Kotnis

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 