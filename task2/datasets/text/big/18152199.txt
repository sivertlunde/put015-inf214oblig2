Ice from the Sun
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Ice from the Sun
| image          =
| image_size     =
| caption        =
| director       = Eric Stanze
| producer       = Jeremy Wallace
| writer         = Eric Stanze
| narrator       =
| starring       = DJ Vivona Ramona Midgett Jason Christ Tommy Biondo
| music          =
| cinematography =
| editing        = Eric Stanze
| distributor    =
| released       = 1999
| runtime        =
| country        =
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Ice from the Sun is a 1999 independently produced horror film directed by Eric Stanze, and is St. Louis-based production company Wicked Pixel Cinemas second feature film. The film had a theatrical release in 2001.

== Plot ==
The concept of Ice from the Sun creates a third presence that disrupts the balance between Heaven and Hell. The Presence is a supernatural entity that rules a separate dimension surrounded by ice, where he brings imprisoned humans for eternal torture. The Presence had once been a human, but now commands omnipotent powers.  A young woman who is in the midst of committing suicide is recruited by a coalition of angels and devils to infiltrate The Presence’s ice world as one of his prisoners for torture. Her goal is to get The Presence to recall his blocked memory of his human origins, which would allow his icy domain to melt and enable the angels and demons to destroy their common enemy.   

== Production and reception ==
Ice from the Sun was produced in the St. Louis, Missouri metropolitan area with a cast of local actors. Stanze used 193 rolls of Super 8 film (a total of 9,650 feet) to shoot the film.

The film was released on DVD in 1999 by SRS Cinema, and it received strong reviews in the horror film media. Carl Lyon, writing for Monsters at Play, called the film “a deftly executed, relentless nightmare, unapologetic in its brutality or its experimental nature,”  while Mac McIntire, writing for DVD Verdict, stated the film was “recommended for die hard horror fans looking for something way off the beaten path.”  In 2000, Ice from the Sun won three awards at the B-Movie Film Festival.

In 2001, the film received a theatrical release, where it was met with mixed reviews. Maitland McDonagh, writing for TV Guide Online, praised the film for being “often imaginative and surprisingly accomplished within the limits of its ultra-low budget.”  However, Stephen Holden of The New York Times complained “it is the kind of film that only a certain breed of cinematic cultist could tolerate.”  Also, Michael Atkinson, writing in The Village Voice, stated the film was “a furiously pointless punk-gore loogie that resets the bar on how wretched an amateur indulgence can be and still garner public screen time merely on the impression of being ‘transgressive.’” 

The film was re-released in 2005 by Image Entertainment.

== References ==
 

== External links ==
* 
*  at Rotten Tomatoes
* 

 
 
 