Myra Breckinridge (film)
 
{{Infobox film
| name = Myra Breckinridge
| image = Myrabreckposter.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Michael Sarne
| producer = David Giler Robert Fryer
| writer = Michael Sarne David Giler
| based on =  
| starring =  Mae West John Huston Raquel Welch Rex Reed Farrah Fawcett John Phillips Richard Moore
| editing = Danford B. Greene
| distributor = 20th Century Fox
| released =  
| runtime = 94 minutes
| country = United States
| language = English
| budget = $5.385 million Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History (The Scarecrow Filmmakers Series). Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-8108-4244-1. p256 
| gross = $4 million (US/ Canada) 
}} novel of the same name. The film was directed by Michael Sarne, and featured Raquel Welch in the title role. It also starred John Huston as Buck Loner, Mae West as Leticia Van Allen, Farrah Fawcett, Rex Reed, Roger Herren, and Roger C. Carmel. Tom Selleck made his film debut in a small role as one of Leticias "studs". Theadora Van Runkle was costume designer for the film, though Edith Head designed Wests costumes. 

Like the novel, the picture follows the exploits of Myra Breckinridge as she goes to Hollywood to turn it inside out; also in the story are a former Hollywood siren named Leticia and Myras alter ego, Myron, who originally was a man before he became Myra.
 worst films ever made.    

==Cast==
* Mae West as Leticia Van Allen
* John Huston as Buck Loner
* Raquel Welch as Myra Breckinridge
* Rex Reed as Myron Breckinridge
* Farrah Fawcett as Mary Ann Pringle
* Roger C. Carmel as Dr. Randolph Spencer Montag
* Roger Herren as Rusty Godowski
* George Furth as Charlie Flager, Jr.
* Calvin Lockhart as Irving Amadeus
* Jim Backus as Doctor
* John Carradine as Surgeon
* Andy Devine as Coyote Bill
* Grady Sutton as Kid Barlow
* Robert Lieb as Charlie Flager, Sr.
* Skip Ward as Chance
* Kathleen Freeman as Bobby Dean Loner
* B.S. Pully as Tex
* Buck Kartalian as Jeff
* Monte Landis as Vince
* Tom Selleck as Stud
* Toni Basil as Cigarette Girl
* Dan Hedaya as Hospital Ward Patient
* William Hopper as Judge Frederic D. Cannon

==Production==
Filming was laden with controversy due to Michael Sarne being granted complete control over the project. Sarne quickly went over budget due to his unorthodox techniques, which included spending up to seven hours at a time by himself, "thinking," leaving the cast to wait around on set for him to return so that filming could commence.    Additionally, Sarne spent several days filming tables of food for a dream sequence which, in addition to being non-essential to the plot, appears in the film for only a few seconds.  After the failure of this film, he was never asked by an American studio to direct another film. Upon learning that Sarne was now working at a pizza restaurant, Gore Vidal is said to have commented that this was proof of Gods existence. 

There were also reports of conflicts between Raquel Welch and Mae West, who came out of retirement to play the Leticia character.     Furthermore, some 1940s- and 1950s-era film actors who appeared in Myra Breckinridge were upset that footage from their old films was inserted into the movie to punctuate some of the gags and the films climactic rape sequence. After the film was previewed in San Francisco, the White House demanded that footage from the 1937 film Heidi (1937 film)|Heidi, featuring Shirley Temple, be removed due to Temples role as a United States ambassador. Loretta Young also successfully sued to have footage of herself removed from the film.  Commenting on this, Rex Reed, who costarred and was then a columnist, said, "This was a film where the lawsuits really flew".

==Rating== X rating to be released by 20th Century Fox in 1970 (the other being Beyond the Valley of the Dolls).  In 1978 the studio submitted a cut version running 91 minutes to the MPAA, and the film was re-classified with an R rating. Both versions are available on the DVD, though the uncut print is now considered unrated.

==Reception== Miami News, "I now nominate Myra Breckinridge as the worst movie ever made...nothing can touch
it for tastelessness and boredom".  The film is also cited in the book The Fifty Worst Films of All Time.  Gore Vidal disowned the film, calling it "an awful joke".   Film historian Leonard Maltin gave the film a BOMB (his lowest possible score). In his movie guide he states the film "tastelessly exploits many old Hollywood favorites through film clips." He also calls the film "as bad as any movie ever made."

Due to the films adult themes, it has rarely been shown on television, though in recent years, the film has aired on   format.  Since its release, it has developed a cult following. 

==See also==
* List of films considered the worst

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 