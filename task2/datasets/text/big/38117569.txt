Aata Pita
{{Infobox film
| name           = Aata Pita
| image          = Aata Pita Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Uttung Shelar
| producer       = Twenty First Century Entertainment
| screenplay     = 
| story          =  Ashwini
| music          = Sanjeev Kohli
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Everest Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Aata Pita is a Marathi film, released on December 10, 2010. The film has been produced by Twenty First Century Entertainment and directed by Uttung Shelar. The plot of the film revolves around the centric character “Ashu” who embarks on a journey to pursue his dream of becoming a writer. The film unravels a tale that questions the dominance of destiny or karma on our lives, a story that portrays through very real and amusing characters, a very subtle narration of the reliance we human beings have on our own fate and on our own actions. 

== Synopsis ==
Ashutosh Pawar aka Ashu (Bharat Jadhav) is a common man working as a clerk in the water department of Mumbai’s municipality. In his absolute mundane existence he has always nurtured a dream to be a writer and write his very own story.

Despite being a voracious reader and having read almost all of Marathi literature; all he has managed to write so far is only the first line of his short story. He thus decides to seek help from his professor, a renowned writer and his guide Prof. V.V.Deshpande (Satish Pulekar) who advises him that in order to move ahead with a story the writer primarily needs to have a central character on whom the story is based on. 

Thus following his Professor’s advice, Ashutosh sets out on his search for his central character & after several disastrous attempts, comes across Mr. Nandkumar Deshmukh aka Nandu (Sanjay Narvekar), an ambitious and flamboyant young man working as a loan recovery officer in a multinational bank.

Ashu begins to follow Nandu and locks him as his central character. Since Ashu has taken the professors advice to observe his character intensly, he lands up at Nandu’s house under the pretext of being new in Mumbai and having no place to stay!!

However Nandu soon finds out Ashu’s reality but when he reads a rough sketch of the story line written by Ashu, he is shocked. It’s a known fact that your life is a story. But what if a story was your life?  Ashu is extraordinarily accurate about Nandu.

Amidst all the chaos and confusion does Ashu manage to complete his story that started it all – entitled Aata Pita – a tale that questions the dominance of destiny or karma on our lives....a story that portrays through very real and amusing characters, a very subtle narration of the reliance we human beings have on our own fate and on our own actions.

== Cast ==
*Sanjay Narvekar as Nandkumar Deshmukh aka Nandu
*Bharat Jadhav as Ashutosh Pawar aka Ashu
*Satish Pulekar as Prof. V.V.Deshpande 
*Ashwini & Others

==Soundtrack==
The music is directed and composed by Sanjeev Kohli.

===Track listing===
{{Track listing
| title1 = Aata Pita Title Track | length1 = 5:28
}}

== References ==
 
 

== External links ==
*  
*  
*  

 
 