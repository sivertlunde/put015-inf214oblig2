American Guerrilla in the Philippines
{{Infobox film
| name           = American Guerrilla in the Philippines
| image          = American Guerrilla in the Philippines.jpg
| image size     =
| caption        = original film poster
| director       = Fritz Lang
| producer       = Lamar Trotti
| writer         = Ira Wolfert, Iliff David Richardson (book) Lamar Trotti (screenplay)
| narrator       = Jack Elam
| starring       = Tyrone Power Micheline Presle
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = November 8, 1950
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2,275,000 (US rentals) 
}}
American Guerrilla in the Philippines (released as I Shall Return in the UK) is a 1950 war film starring Tyrone Power as a U.S. Navy ensign stranded by the Japanese occupation of the Philippines in World War II. Based on the story of Iliff David Richardson, it was directed by Fritz Lang and filmed on location.

==Plot==
In April 1942 in the Philippines, an American motor torpedo boat is destroyed by Japanese planes. The survivors, among them Ensign Chuck Palmer (Tyrone Power), make their way ashore on Cebu. Their commander orders them to split up. Chuck pairs up with Jim Mitchell (Tom Ewell) and reaches Colonel Benson on Leyte, only to be told that he has been ordered by General Douglas MacArthur to surrender his forces soon.
 Air Corps soldiers in a desperate, but unsuccessful attempt to sail to Australia. When the boat founders, the crew is rescued by Miguel (Tommy Cook), a member of the Filipino resistance. The Americans evade capture and Chuck eventually meets Jeanne again, as well as her husband Juan (Juan Torena), a secret supporter of the resistance movement.

Chuck is ordered to stay in the Philippines to help set up a network to gather intelligence on the Japanese. Later, Juan is beaten to death in front of Jeanne in an attempt to find out where the guerrillas are hiding out. Jeanne joins the resistance and is reunited with Chuck at Christmas 1943. They begin to fall in love.
 liberation of the Philippines is underway. The Japanese leave to face this greater threat.

==Cast==
*Tyrone Power as Ensign Chuck Palmer
*Micheline Presle as Jeanne Martinez (as Micheline Prelle)
*Tom Ewell as Jim Mitchell
*Robert Patten as Lovejoy (as Bob Patten)
*Tommy Cook as Miguel
*Juan Torena as Juan Martinez
*Jack Elam as The Speaker
*Robert Barrat as General Douglas MacArthur

==Production== Iliff "Rich" PT 34.  After it was sunk by the Japanese, Richardson and a dozen fellow Americans attempted to sail a native outrigger to Australia, but the boat was sunk in a storm. He eventually joined the Philippine guerrilla forces, setting up a radio network to keep the various bands in touch with each other and Allied forces in Australia. After the liberation of the Philippines, Richardson dictated his memoirs to war correspondent Ira Wolfert, who published them in 1945 as An American Guerilla in the Philippines. The book became a Book-of-the-Month Club selection and was published in condensed form in the March 1945 issue of Readers Digest. 
 Henry King, John Payne Catalina Island.    The end of the war led Zanuck to shelve all films with a World War II theme.

In 1950, American Guerrilla in the Philippines was the first American film made on location in the Philippines in color.  Fritz Lang was assigned the project, but said it was the least favorite of all his films.  Though the story was well received in the Philippines, there was a protest that Filipino actors were left out of the credits, with the studio placing their names later.   Zanuck, who also worked on the script and edited it,  rushed the films completion to tie in with the start of the Korean War. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 