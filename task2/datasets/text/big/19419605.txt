Strongroom (film)
{{Infobox film
| name           = Strongroom
| image          = "Strongroom"_(film).jpg
| image_size     = 
| caption        = Brazilian poster
| director       = Vernon Sewell
| producer       = Guido Coen Richard Harris
| narrator       = 
| starring       = Derren Nesbitt Colin Gordon  Ann Lynn
| music          = Johnny Gregory
| cinematography = Basil Emmott
| editing        = John Trumper
| studio         = Theatrecraft
| distributor    = Bryanston Films (UK)
| released       = May 1962 (UK)
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| website        = 
| amg_id         = 
}} 1962 British bank manager, and cashier are locked in a safe, but the criminals are forced to return to the bank to release them before the police arrive.  

==Cast==
* Derren Nesbitt as Griff
* Colin Gordon as Mr. Spencer
* Ann Lynn as Rose Taylor
* Keith Faulkner as Len
* W. Morgan Sheppard as Alec
* Hilda Fenemore as Charlady
* Diana Chesney as Charlady Jack Stewart as Sergeant McIntyre
* Colin Rix as P.C. Harper
* Ian Colin as Creighton
* John Chappell as John Musgrove
* Pamela Conway as Secretary
* Colin Tapley as Haynes
* Kevin Stoney as Police Sergeant
* Duncan Lewis as Mortuary Attendant

==Critical reception==
*The Radio Times wrote, "director Vernon Sewell sets up this improbable story quite neatly, but its hardly breathless entertainment."  
*TV Guide noted "a suspenseful, taut crime drama."  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 
 