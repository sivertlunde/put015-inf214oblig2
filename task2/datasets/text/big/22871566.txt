Soundless Wind Chime
 
 
{{Infobox film name           = Soundless Wind Chime image          = SWCPoster.jpg caption        =  producer       = Jacqueline Liu   Liliane Ott   Min Li Marti   Philip Delaquis   Stefan Zuber  director  Hung Wing Kit writer         = Hung Wing Kit starring       = Bernhard Bulling   Lu Yulai   Wella Zhang   Li Wai Foon   Marie Omlin   Hannes Lindenblatt   Wong Siu Yin music          = cinematography = Yue Shi   Alex Shi Yue editing        = Martina Ziesack distributor    = TLA Releasing released       =   runtime        = 100 minutes country        = Hong Kong Switzerland China language       = Swiss German   English   Mandarin   Cantonese budget         = 
}} Hung Wing Kit ( ), starring Lu Yu Lai and Bernhard Bulling.  It was a 2009 Nominee for the Berlin International Film Festivals Teddy Award.

==Plot==
Soundless Wind Chime centers around a new immigrant to Hong Kong from China, Ricky (Lu Yu Lai), who works as a delivery boy while living with his prostitute aunt (Wella Zhang).  He is pickpocketed by a Swiss thief, Pascal (Bernhard Bulling) who is in an abusive relationship with his con artist boyfriend (Hannes Lindenblatt).  Deciding to leave him, Pascal has a chance encounter with Ricky and the two begin a romantic relationship.  The couple struggles through good times and bad, forcing them to determine if their relationship is based on love or dependence on one another.  Several years later, Ricky searches Switzerland for signs of Pascal, eventually encountering Ueli (also played by Bulling), a timid antique store owner who looks the same as Pascal, but who has a vastly different personality.  As Ricky and Uelis relationship deepens, the truth of Pascal and Rickys relationship is unraveled as the film progresses through glimpses of the present and the past.

==Accolades==
In the Turin GLBT international Film Festival, Italy, the film has won the Audience Award, Nuovi sguardi (Best New Director), and a Special Jury Mention for Best Feature Film in Competition.

This film has also won the Best Direction, and Best Actor (Lu Yulai) awards in the  

In Canada, Soundless Wind Chime has won the best international festure in the  , and the Jury Award For Best Feature Film in the  .

The film was featured in the 59th Annual Berlin International Film Festival and was an official nominee for the 2009 "Teddy Award."

==Cast==
* Lu Yu Lai - Ricky
* Bernhard Bulling - Pascal/Ueli
* Marie Omlin - Sister
* Gilles Tschudi - Father
* Ruth Schwegler - Mother
* Wella Zhang - Auntie
* Li Wai Foon - Restaurant Owner
* Wong Siu Yin - Popo
* Hannes Lindenblatt - Marcus
* Jackie Leung - Singing Angel

==Music==
The film features an original score produced by Claudio Puntin and Insa Rudolph performed by Sepiasonic and also contains music by artist Justin McGrath as well as the song "Achoo Cha Cha" by classic Chinese singer and actress Grace Chang.

==See also==
* Speechless (2012 film)|Speechless, a 2012 gay-themed film with a similar atmosphere, and which also features, as its main theme, an affair between two young men, one Chinese and the other, Western. Unlike Soundless Wind Chime  , the films dialogue is mostly in Mandarin (Standard Chinese|Putonghua), the national language of China.
* List of Chinese films of 2009
* List of lesbian, gay, bisexual or transgender-related films
* List of lesbian, gay, bisexual, or transgender-related films by storyline

==External links==
*  
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 