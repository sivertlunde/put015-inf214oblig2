Rocky Road to Dublin (film)
{{Infobox Film
| name           = Rocky Road to Dublin
| image          = RRTD.JPG
| image_size     = 
| caption        = 
| director       = Peter Lennon
| producer       = 
| writer         = 
| narrator       = 
| starring       = Seán Ó Faoláin Conor Cruise OBrien John Huston Father Michael Cleary
| music          = 
| cinematography = Raoul Coutard
| editing        = 
| distributor    =  1967
| runtime        = 69 min.
| country        = Ireland
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Rocky Road to Dublin is a 1967 documentary film by Irish-born journalist Peter Lennon and French cinematographer Raoul Coutard, examining the contemporary state of the Republic of Ireland, posing the question, "what do you do with your revolution once youve got it?" It argues that Ireland was dominated by cultural isolationism, Gaelic and clerical traditionalism at the time of its making.

==Outline==
A brief sketch of Irish history since the Easter Rising of 1916 is drawn, in which the hopes of the revolutionary founders of the Irish Free State for a republican society are dashed. The writer Seán Ó Faoláin argues that what emerged was a society of "urbanized peasants" without moral courage who observed a self-interested silence in a "constant alliance" with an "obscurantist" and "uncultivated church".

The film covers the continuing dominance of the Anglo-Irish aristocracy with scenes of hunting and equestrian events. The countrys lack of independence from American foreign policy is touched on; Conor Cruise OBrien is critical of Irelands following of American policy in the United Nations General Assembly since 1957, and argues that Ireland should follow Swedens more independent lead.
 contraceptive pill in the correspondence columns of his newspaper, and reflects on the emerging difficulties of censorship. He points out the difficulties of the older generation adapting to change, while people under thirty are more positive. Students at Trinity College, Dublin though, are critical of the Irish media, including The Irish Times.

A policy of the 500,000 strong Gaelic Athletic Association, finally dropped in 1971, is discussed. This was for the suspension of members found to have watched or participated in the foreign, or more precisely the English, sports of Soccer, Cricket, Rugby and Hockey. The ban applied to clubs whose activities included these pastimes.

The emigration of Irish writers is viewed as the countrys most "notorious" export because of state Censorship in the Republic of Ireland|censorship. A roll call of native and international writers whose work had been banned is accompanied by church bells rung as at a funeral. A member of the censorship appeal board is interviewed. Professor Liam Ó Briain (1888–1974) contrasts Ireland with Britain where he sees the complete loss of faith, moral values and the abolition of sin. He recounts that a friend, Jimmy Montgomery, while film censor, had to make decisions "for a while" on sound films before the equipment to hear the soundtrack had arrived. Ó Briain though, does recognise the difficulties of sustaining faith in the context of scientific and philosophical developments.
 Christian Brothers catechism on the effects of original sin. Irish schools were single sex only. For this reason, contact between adolescents and young of both sexes for the middle classes is restricted to the tennis club dance halls where strict decorum is observed. Irish women are depicted as passive, always waiting for men to make contact with them. The old ways there are contrasted with a much more modern working class venue.

The "close involvement" of Irish politicians with the clergy is seen as "not so much a villainous conspiracy as a bad habit". Before the Second Vatican Council the role of the clergy was "not to disturb the simple faith of the people". Subsequently, the hierarchy "reluctantly" allowed some relaxation. Father Michael Cleary, posthumously the subject of a scandal, is shown singing a secular song "Chattanoogie Shoe Shine Boy" in a womens hospital ward. At a wedding reception he is an active participant. In an interview, he sees this show of camaraderie as a means of reaching the young, and showing himself to be "on their wavelength". He insists the clergy is not against sex, and that celibacy is a problem for the priest; "personally" he would wish to be married and have a family, but their absence is a sacrifice he makes as a priest.
 safe period. At Sacrament of Penance (Catholic Church)|confession, the priest tells her to sleep separately from her husband rather than participate in his sin. They do however stop their method of contraception, and the woman Miscarriage|miscarries. Though she remains a practicing Catholic, priests and doctors are seen as solely being on the side of men.

The clerical and political orthodoxy outlined is viewed as being about to change. Though he is critical of "pop orchestras" and "jazz bands", the censor Ó Briain, wishes the emerging world well and hopes it will develop its own traditions. The film ends on a freeze frame of two boys in a boat, one of them is looking straight at the camera.

==Production==
Peter Lennon, then a junior feature writer for The Guardian in Paris, was told by friends in his week back in Dublin covering the theatre festival, that Ireland was changing: "censorship was a thing of the past they told me."  No one paid any attention to the clergy, it was claimed. Gaining permission from his newspaper to write a series of articles about the situation in Ireland, he found this was not so. The articles gained much notice lasting over a year, and he was able to make the film when his friend, Victor Herbert, who had made a fortune selling mutual funds, agreed to finance it.
 Week End.  At the time, Lennon chose to use the cameraman because Coutard was the only person he was aware able to shoot in an informal way. 
 reversal film stock used, and partly because Lennon thought that monochrome was more in keeping with the tone of the film. 

==Response in Ireland== Cork Film Festival, but it was rejected on the grounds that it had already been shown in the capital. It was only when the film was chosen to represent Ireland at the 1968 Cannes Film Festival that the Cork organisers were embarrassed into changing their decision, but gave it a lunchtime screening on the day that all the media covering the Festival were invited, "to a free oyster-and-Guinness lunch in Kinsale, 20 miles away."  Lennon organised his own screening in Cork the following day, the resulting furore leading to a single Dublin cinema giving it a seven-week run to packed audiences.
 Paris students strike, leading to a furious debate with Lennon and other filmmakers present. Despite this, screenings were organised for the same students, as well as striking workers besieged in the Renault plant.

The de facto ban in Ireland remained in place for more than thirty years - barring sporadic private screenings in the 1990s - until the film was restored in 2004 by the Irish Film Board and Loopline Films. At the same time, a new documentary, The Making of Rocky Road, was made by Paul Duane, examining the history of Rocky Road, and including BBC footage of Coutard and Lennon shooting Rocky Road in 1967,  and previously unseen film of the events in Cannes in 1968. Both films were released on DVD (Region 0 PAL) in 2005.

==External links==
* 
* 

==References==
 

 
 
 
 
 
 
 