Lady Hunter: Prelude To Murder
 
 
{{Infobox film
| name           = Lady Hunter: Prelude To Murder
| image          = 
| image_size     =
| caption        =
| director       = Takashi Miike
| producer       = Shochiku Company
| writer         = Shinjirō Ishihama (Story), Tatsuo Eguchi (Scenario)
| narrator       =
| starring       = Yoshie Kashiwabara, Naomi Morinaga
| music          = Tomio Terada 
| cinematography = Shigeru Komatsubara
| distributor    =
| released       = 1991, December 21
| runtime        = 79 min.
| country        = Japan
| language       = Japanese
| budget         =
}}

Lady Hunter: Prelude To Murder (レディハンター 殺しのプレュード, Redi Hantā: Koroshi No Pureryūdo) is a 1991 Japanese action film directed by Takashi Miike. This is the very first film shot by Takashi Miike even though the film has been released after  Toppū! Minipato tai - Aikyacchi Jankushon, a film that was shot later, but released before.

==Plot==
Saeko, a female former soldier is forced to come out of her retirement to stop the kidnapping of a young boy who is being used a political pawn.

==Cast==
*Yoshie Kashiwabara
*Naomi Morinaga
*Kōsuke Morita
*Isao Murata

==References==
 

 

 
 
 
 

 