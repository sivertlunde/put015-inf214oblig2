Samasthanam
{{Infobox film
| name           = Samasthanam
| image          = Samasthanam DVD cover.svg
| image_size     =
| caption        = DVD cover
| director       = Raj Kapoor (Tamil film director) | Raj Kapoor
| producer       = M. Kajamydeen K. Ayisha
| writer         = Raj Kapoor
| starring       =   Deva
| cinematography = B. Balamurugan
| editing        = V. T. Vijayan
| distributor    =
| studio         = 
| released       = 27 September 2002
| runtime        =
| country        = India
| language       = Tamil
| preceded_by    =
| followed_by    =
| website        =
}}
 2002 Tamil Sarath Kumar, Devayani and Abhirami in lead roles. The film, produced by  M. Kajamydeen and K. Ayishawas, had musical score by released on 27 September 2002.    

==Plot==
 Sarath Kumar) and Surya (Suresh Gopi) are inseparable friends. They are so close to each other that Thiru selects a bride for Surya and Surya for Thiru. Their friendship is carried over from their grandfathers and fathers who were thick friends. Thiru brings up Suryas daughter, because Thiru has no children. Shankara (Ashish Vidyarthi) wants to separate them and does all that he could to separate them.
Suryas sister becomes pregnant due to a love affair with Thirus relative. On the eve of her wedding, Divya (Devayani (actress)|Devayani), Thirus wife, helps her run away with the guy she loves. This breaks the friendship between the two as Suryas mother (Vadivukkarasi) poisons his ears against Thiru. As expected, Shankara is behind this. Finally in the climax, the two destroy Shankara and resolve their differences, and unite in the end.

==Cast==
 Sarath Kumar as Thiru and Thirus father
*Suresh Gopi as Surya Devayani as Divya Abhirami as Aisha
*Ashish Vidyarthi as Shankara
*Vadivukkarasi as Suryas mother
*Manivannan as Parama
*Goundamani Senthil
*Chandrasekhar Chandrasekhar as Sundarapandian, Suryas father Raj Kapoor as Eswaramoorthy, Shankaras father
*Vinod Kishan as Shankara (child)
*Sabitha Anand
*Rishi Ramji
*Vijay Sarathi

==Soundtrack==

{{Infobox album |  
  Name        = Samasthanam|
  Type        = soundtrack | Deva |
  Cover       = |
  Released    = 2002 |
  Recorded    = 2002 | Feature film soundtrack |
  Length      = 32:25 |
  Label       = | Deva |  
  Reviews     = |
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 2002, features 6 tracks with lyrics written by P. Vijay and Na. Muthukumar.    

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Lyrics !! Duration
|- 1 || Stella Maris || Silambarasan Rajendar || rowspan=2|P. Vijay || 5:38
|- 2 || Srinivas || 6:16
|- 3 || Penne Penne || Unnikrishnan, Harini, Tippu (singer)|Tippu, Ganga || Na. Muthukumar||  6:23
|- 4 || Eswaraa Eswaraa || Krishna Raj, Tippu || rowspan=3|P. Vijay|| 5:46
|- 5 || Koththamalli || Silambarasan Rajendar || 6:10
|- 6 || Malaraai Malaraai ||  Unnikrishnan || 2:12
|}

==References==
 

 
 
 
 