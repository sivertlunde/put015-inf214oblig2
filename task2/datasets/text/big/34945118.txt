Rana's Wedding
 {{Infobox film
| name           = Ranas Wedding
| image          = Ranas_Wedding.jpg
| caption        = Poster
| director       = Hany Abu-Assad
| writer         = Ihab lamy and Liana Badr
| Editor         = Denise Janzee
| starring       = Clara Khoury,Khalifa Natour and Ismael Dabbagh
| released       =  
| runtime        = 86 minutes
| country        = Palestine
| music          = Mariecke Van Der Linden and Bashar Abd Rabou
| Funding        = Palestinian film foundation
| cinematography = Brigit Hillenius Arabic
| producer       = Bero Bayer and George Ibrahim
| distributor    = Arab Film Distribution
}}
 romance and dark comedy genres which left the audience astonished by this unique approach, that was able to portray, (through the couples marriage drama)  a vivid image of a Palestinians daily struggle, towards living a somewhat normal life.

==Plot==
The Film depicts a 17-year old girl called Rana that is subjected to a crucial life-changing decision at a very young age. Because her father decides to leave Palestine and relocate to Egypt, due to the chaotic working and living conditions, to try and maintain a decent living to support his family. She wakes up one day and finds a letter from her father informing her about his difficult decision adding to it, he also involves her in his plans and provides her with two choices, http://articles.boston.com/2004-03-12/ae/29207514_1_ihab-lamey-liana-badr-clara-khoury  either to travel with him to Egypt and carry on with her education there under his watchful eye, or stay in Palestine and get married to make sure that someone is guarding her in his absence. Although the options seem fairly reasonable, there is a twist, her father will only allow her to marry one of the men that he has mentioned on a list with the letter, because they are the most reputable and trustworthy men in Jerusalem, as well as, she has to take that critical decision just 10 hours before her father departs from Palestine. http://www.popmatters.com/pm/review/ranas-wedding 

Rana is left shocked and disappointed with the options, after reading the letter she decides to run away from home in search of her lover Khalil that her father does not approve of because of his career, therefore, does not include him in the list of people she can marry.  Rana desperately searches for Khalil leaving no home, family or friend unasked about his whereabouts to inform him about her critical situation. As she searches for him she is faced with many challenges along the way from Israeli soldiers, road blocks and physical conflicts between Palestinian and Israeli soldiers. http://movies.nytimes.com/movie/review?res=9C04E7DE1739F931A1575BC0A9659C8B63  After hours of searching she finally finds his location and rushes to him, finding him in the theatre, working on directing one of his plays.

At last she finds him and tells him everything she has been through the past few hours, and confidently asks him for his hand in marriage, rushing him to take a decision in order to find a sheikh to marry them before her father sets off. Together they set out on an even longer journey to reach the sheikh and her father to approve of their marriage. They were able to find the sheikh and drove him to her father’s house to convince him of the marriage, that he could not object to without any rightful excuse according to Islamic law, the sheikh supported Ranas decision and told her father that the marriage must proceed. http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20040130/REVIEWS/401300304  Her father giving into her wishes unwillingly approves, Rana and Khalil then set out on another extremely tiresome mission, getting their papers and preparing themselves for their wedding ceremony. http://www.filmthreat.com/reviews/4693/ 

At the end tension rises when the wedding is held at Ranas fathers house while the sheikh has not shown up to do the ceremony and her dad is getting ready to leave. As she waits restlessly she finds out that the sheikh is stuck at a road block. Her father gets furious and waits no longer, ordering her to come with him in her wedding dress, leaving for Egypt. Riding in the car with her father and her family they pressure and persuade her father to go to the road block for his daughter, finally agreeing they arrive at the road block. When her father and Khalil read their vows in the car, her dad sympathetically gives her away to the man she loves, ending the love story with a heart-warming celebration on the streets of Jerusalem. 
                                  
The film ends with the words of Palestinian poet, Mahmoud Darwish:
{{quote|text=Here on the slopes before sunset and at the gun-mouth of time,
Near orchards deprived of their shadows, 
We do what prisoners do, 
What the unemployed do: 
We nurture hope.|sign=Mahmoud Darwish}} 

==Cast==
Acting Credits http://movies.nytimes.com/movie/265460/Rana-s-Wedding/details                                                             
{| class="wikitable"                                           
! Actor !! Character Name                                    
|-
| Clara Khoury || Rana
|-
| Khalifa Natour || Khalil
|- Ramzy 
|-
| Zuher Fahoum  || Father
|-
| Bushra Karaman || Grandmother
|}

Production Credits
{| class="wikitable"
|-
!  Position  !!  Name
|-
| Director || Hany Abu-Assad
|-
| screenplay ||  Liane Badr
|-
| Screenplay || Ihab Lamey
|-
| Producer ||  Bero Beyer
|-
| Producer || George Ibrahim 
|-
|  Associate Producer || Mohammad Rachid
|-
| story by ||  Liane Badr
|-
| D.O.P || Brigit Hillenius
|-
| Editor || Denise Janzee
|-
| Music|| Mariecke van der Linden
|-
|  Music|| Bashar Abd Rabbou
|-
|  Costume Designer || Hamada Atallah
|-
| Sound Design || Mark Wessner
|}

==Production==
The Film was shot in  . Hany Abu-Assad in an interview with journalist Sabah Haider, disucsses the challenges he faced when producing the film "For sure the intifada influenced the production because at the end of the film, reality is stronger than fiction. The occupation, the checkpoints — you don’t want them to interfere with your story but the ugliness of occupation influenced the look of film. As much as you might not want occupation to influence the making of the film, at the end it does influence it. "

==Critical Reception==
Box Office

According to the website  , in 2003 Ranas Wedding was ranked out of the top 50 wedding genre films produced in that year, and received a world ranking of 1108 in  .

==Reviews==

The film Ranas Wedding was reviewed by many notable newspapers, websites and critics and overall received pleasing reviews.
* Ranas Wedding was reviewed by Stephen Holden for the The New York Times in 2003, discussing the plot and providing the film with great support to increase viewers interest towards this unique story. 
* It was reviewed by Al Bawaba   2002, who identifyied it as one of the first Palestinian films to have an impact on the Arabic films being screened at Cannes and was able to increase the Arab presence in the festival. 
* The website Metacritic  http://www.metacritic.com/movie/ranas-wedding  presents a large number of critics stating their opinion about the film and providing it with great reviews. 
* An article written by Janice Page in 2004 found at Boston.com article collection, evaluates and supports the film with a great review. 
* About.com, rated Ranas Wedding, 3.5 stars out of 5, based on a review written by Jürgen Fauth, supporting the movie and evaluating its plot. 
* Rotten Tomatoes, provides positive and negative reviews on the film giving the film an overall rating of 93% on the tomato meter. 
* IMBD users rate movie as a 6.7 out of 10  
* Amazon.com includes editorial reviews written by Roger Ebert for the Chicago Sun-Times and Ann Hornaday, for the Washington Post  

==Awards and Nominations==
Awards
* Won Grand Prize in Cologne  , and Clara Khoury as best actress in (2002). http://www.imdb.com/title/tt0305229/awards 
* Clara Khoury won best actress from Marrakech International Film Festival in (2002). 
* Won the Golden Antigone award from Montpellier Mediterranean Film Festival in (2002)  
* Won the Golden Anchor award from Haifa International Film Festival in (2003)  
* Won the Nestor Almendros for courage in film making, Human Rights Watch International Film Festival in (2003)  
Nominations
* Nominated for the Golden star award from Marrakech International Film Festival in (2002) 
* Nominated for the Golden Alexander Award by the Thessaloniki Film Festival in (2002) 
* Nominated by Buenos Aires International Festival of Independent Cinema for Best Film in (2003)  
* Nominated for Golden Dolphin award by Festróia - Tróia International Film Festival in (2003)  

==Legacy==
 
* Rana’s Wedding is considered to be the first film that approaches and show cases the Palestinian-Israeli conflict in a novel way, leaving viewers astonished by this unique approach, because the film did not incorporate the common styles of films that discuss life circumstances in Palestine.  In which they would always focus on tragic plots that contain similar content of war, murder, hunger and destruction. Instead Rana’s Wedding portrayed all these events but in an indirect and compelling way, through a love story, and amazingly did not reduce the intensity of the Palestinian Israeli conflict. Through a plot that was able to portray how people still try to live and try to create an everyday normal life, despite the numbers that die in Palestine each day. People still laugh, hope, worship, love, work, care for one other, sing and dance, and finally try to have a normal wedding that’s not so normal to many of us.  Rana’s Wedding is just filled with pure heart felt emotions and through the movements of the couple one is able to witness all the different events that take place in Jerusalem, both the joyful and heart-breaking. A film that truly makes Palestinian see hope in a new day and find bliss  under all the destruction they see everyday. 

* Is one of the first few Arabic films to be screened at Cannes Film Festival and more importantly one of the earliest Palestinian films to reach Cannes and gain a great deal of popularity, success and outstanding reviews. Affecting greatly the Arabic cinema and the Arab presence at Cannes by opening doors to many Palestinian and Arabic film directors to take on the same path and reach Cannes film festival. 

==See also==
*Paradise now
*Cinema of Palestine
*Cinema Jenin
*Palestinian National Theatre
*Al-Kasaba Theatre

== References ==
 

== External links ==
* 
* 
* 
* 
* 

 

 

 
 