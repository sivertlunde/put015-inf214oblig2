Tsuribaka Nisshi (film)
{{Infobox film
| name           = Tsuribaka Nisshi
| image          = 
| caption        = 
| film name = {{Film name| kanji          = 釣りバカ日誌
| romaji         = Tsuribaka Nisshi }}
| director       = Tomio Kuriyama
| producer       = Shizuo Yamanouchi
| writer         = 
| starring       = Toshiyuki Nishida Rentarō Mikuni
| music          = Mari Nakamoto 
| cinematography = Kosuke Yasuda
| editing        = 
| studio         = 
| distributor    = Shochiku
| released       =  
| runtime        = 93 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 1988 Japanese comedy film directed by Tomio Kuriyama. It was released on 24 December 1988. It is the first film in Tsuribaka Nisshi series. Most of this series are released as one of double feature such as Otoko wa Tsurai yo. The origin is   , a fishing manga by Jūzō Yamasaki (story) and Kenichi Kitami  since 1979.

The film focuses on salaryman Densuke Hamasaki (a.k.a. Hama-chan), whom his supervisor Sasaki has dubbed the "Fishing Baka (fool)|Baka" because of his passion for fishing. 

==Cast==
* Toshiyuki Nishida as Densuke Hamazaki
* Rentaro Mikuni as Ichinosuke Suzuki
* Eri Ishida as Michiko Hamazaki
* Kei Tani
* Mami Yamase

==Awards==
* 17th   - Rentarō Mikuni 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 

 