The Heart of Maryland
 
{{Infobox film
| name           = The Heart of Maryland
| image          =
| caption        = Ross Lederman (assistant director)
| producer       = Warner Brothers
| writer         = C. Graham Baker (scenario)
| based on       =  
| starring       = Dolores Costello Jason Robards, Sr.
| cinematography = Hal Mohr
| editing        =
| distributor    = Warner Brothers
| released       =   reels 5,868 feet
| country        = United States
| language       = Silent film (English intertitles) Vitaphone (music and effects)
}} The Heart of Maryland performed on Broadway.

This film is the last silent version of the oft filmed Victorian story with versions having been produced in 1915 and 1921. An incomplete print of the film is at the Library of Congress.      

==Cast==
*Dolores Costello - Maryland Calvert
*Jason Robards, Sr. - Maj. Alan Kendrick
*Warner Richmond - Captain Fulton Thorpe
*Helene Costello - Nancy
*Carroll Nye - Lloyd Calvert
*Charles Edward Bull - Abraham Lincoln
*Erville Alderson - Maj. Gen. Kendrick
*Paul Kruger - Tom Boone
*Walter Rodgers - Gen. U.S. Grant
*Jim Welch - Gen. Robert E. Lee
*Orpha Alba - Mammy
*Myrna Loy - Mulatta
*Harry Northrup - Gen. Joe Hooker
*Nick Cogley - Eli Stanton
*Lew Short - Allan Pinkerton
*Leonard Mellon - Young Stewrt
*Madge Hunt - Mrs. Abraham Lincoln
*Charles Force - Col. Lummon Francis Ford - Jefferson Davis
*Ruth Cherrington - Mrs. Gordon
*S. D. Wilcox - Gen. Winfield Scott

==See also== The Heart of Maryland (1915) The Heart of Maryland (1921)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 