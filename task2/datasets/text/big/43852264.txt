Scattergood Meets Broadway
{{Infobox film
| name           = Scattergood Meets Broadway
| image          = Scattergood Meets Broadway poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Christy Cabanne
| producer       = Jerrold T. Brandt 
| screenplay     = Michael L. Simmons Ethel B. Stone 	
| story          = Michael L. Simmons 
| starring       = Guy Kibbee Mildred Coles William "Bill" Henry Emma Dunn Frank Jenks Joyce Compton Bradley Page
| music          = Dimitri Tiomkin
| cinematography = Jack MacKenzie
| editing        = John Sturges
| studio         = Pyramid Productions
| distributor    = RKO Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Scattergood Meets Broadway is a 1941 American comedy film directed by Christy Cabanne and written by Michael L. Simmons and Ethel B. Stone. It is the sequel to the 1941 film Scattergood Pulls the Strings. The film stars Guy Kibbee, Mildred Coles, William "Bill" Henry, Emma Dunn, Frank Jenks, Joyce Compton and Bradley Page. The film was released on August 22, 1941, by RKO Pictures.   

==Plot==
 

== Cast ==
*Guy Kibbee as Scattergood Baines
*Mildred Coles as Peggy Gibson
*William "Bill" Henry as David Drew
*Emma Dunn as Mirandy Baines
*Frank Jenks as J. J. Bent
*Joyce Compton as Diana Deane
*Bradley Page as H. C. Bard
*Chester Clute as Quentin Van Deusen
*Morgan Wallace as Reynolds
*Carl Stockdale as Squire Pettibone Charlotte Walker as Elly Drew
*Paul White as Hipp
*Don Brodie as Waiter
*Herbert Rawlinson as The Governor 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 