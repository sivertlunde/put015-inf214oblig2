Nenu
 

{{Infobox Film 
|  name            = Nenu 
|  image           = Nenu 2004 Poster.jpg
|  imdb_id         =  Selva Raghavan Veda Abhishek
|  director        = E. Sathibabu
|  producer        = Mullapudi Brahmanandam
|  editing         =  
|  distributor     = Sri Rajya Lakshmi Combines Vidyasagar
|  released        = April 15, 2004 Telugu
|  country         = India
|  budget          = 
}}
 Tamil blockbuster movie Kaadhal Kondein, which starred Dhanush and Sonia Agarwal in lead roles. The film was a flop at the box office but received good critical acclaim, mainly for Allari Nareshs performance as Psychotic Lover.

==Plot==
Vinod (Allari Naresh), who has grown up under the care of a church father (Paruchuri Venkateswara Rao), is an introvert but a genius. He is forcibly sent to a college in Vizag by the father but is a complete misfit in class. Though shunned by the rest of his class, Divya (Veda Archana|Veda) becomes his friend and he gradually warms up to her too. His feelings soon turn into love but he realizes that Divya considers him as only a friend. But he is unwilling to let her go. Meanwhile Vinod learns that Divya is in love with another classmate, Abhi (Abhishek).

Divyas father is enraged on learning about her love. He shuts her up and prevents her from contacting anyone. But Vinod comes and meets her on the pretext of getting some old clothes for himself to wear. Pitied by Vinod, her father allows him. But Vinod uses the chance and escapes with Divya. He convinces her that she will meet Abhi at the Jungle.

Vinod has set up a secret place in that jungle for executing his plan of wooing Divya. He makes her stay with him, while convincing her by talking about the never-impending Abhis arrival. On one such day, he reveals his miserable past, when he was made to work for paid labour after being orphaned at an early age. He revolts against the oppression one day against the illegal child labour in vogue at his place. Promptly he is beaten black and blue for his profanity. Moreover he also loses his girlfriend to rapists in that place, who also kill her. Somehow he manages to escape from them and seeks refuge in the place of a church father.

Divya is really touched by his past. Incidentally the police and Abhi arrive at the place. While Vinod was away to get some food, they try to make Divya understand that Vinod was a psychopath. Yet Divya scoffs at their claims, citing his gentlemanly behavior over the days she was put up alone with him. Vinod, learning that the police have arrived at the scene, begins to indulge in mass violence. He opens fire, killing a police constable. Forcing them out of their hideout, he manages to evade the police Inspector and Abhi and successfully brings Divya back to their original place of stay.

Divya soon identifies the tiger out of the cows skin. Vinod pleads with her, telling her that all he wanted in his life was her presence with him. But Divya called him a friend and stated her inability to accept him as her partner for life. Meanwhile Abhi regains consciousness and comes back to attack Vinod and rescue his girlfriend. A violent fight follows, where Vinod defies his puny self and treats Abhi with disdain. The fight culminates with Vinod, Abhi and Divya teetering at the edge of a slippery cliff.

While Divya clutches a tree bark tightly, Vinod and Abhi slip out and barely manage to hold either of her hands. Divya is forced to a situation where she needs to choose between her boyfriend and friend. Abhis pleas notwithstanding, Divya doesnt have the heart to kill Vinod. The epic of a cliffhanger finally ends with Vinod smiling wryly at Divya and letting go of her hands himself. He falls to his death into the abyss.

==Cast==
* Allari Naresh as Vinod Veda as Divya
* Abhishek as Aadi
* Paruchuri Venkateswara Rao as Church Father
* Kota Srinivasa Rao
* CVL Narasimha Rao
* Shankar Melkote
* Chalapathi Rao Jenny
* Rajiv Kanakala

==Soundtrack==
Audio of this film was released in a function arranged at Padmalaya today evening at 7:30 pm. Venkatesh was invited as chief guest. Other guests of the function were M Shyam Prasad Reddy, KS Rama Rao, KC Sekhara Babu, Arjuna Raju, EVV Satyanarayana, Paruchuri Venkateswara Rao and Sirivennela. Chalapati Rao anchored this event. Surya Music has bought the audio rights. Venkatesh released the audio and gave the first cassette to hero Naresh and heroine Veda. The audio received positive response. 

==References==
 

==External links==
* 

 
 