Under Your Hat
 
{{Infobox film
| name =  Under Your Hat
| image =
| image_size =
| caption =
| director = Maurice Elvey
| producer =Ivor McLaren    Jack Hulbert
| writer =  Rodney Ackland    Anthony Kimmins    Arthur Macrae   Geoffrey Kerr   Jack Hulbert
| narrator =
| starring = Jack Hulbert   Cicely Courtneidge   Austin Trevor   Leonora Corbett
| music = Lew Stone 
| cinematography = Mutz Greenbaum    
| editing = Edward B. Jarvis 
| studio = Grand National Pictures  British Lion
| released =  
| runtime = 79 minutes
| country = United Kingdom English
| budget =
| gross =
}} comedy spy film directed by Maurice Elvey and starring Jack Hulbert, Cicely Courtneidge and Austin Trevor. 

==Production== independent production made at Isleworth Studios. It was based on a popular stage musical starring Hulbert and Courtneidge, a husband-and-wife team who had made a series of successful comedy films during the 1930s. The sets were designed by art director James A. Carter. 

==Synopsis==
The film is set in pre-Second World War England where a leading film star and his wife attempt to recover a secret carburetor stolen by enemy agents.

==Cast==
* Jack Hulbert as Jack Millett 
* Cicely Courtneidge as Kay Millett 
* Austin Trevor as Boris Vladimir  
* Leonora Corbett as Carole Markoff  
* Cecil Parker as Sir Jeffrey Arlington   Anthony Hayes as George   Charles Oliver as Carl  
* H.F. Maltby as Colonel Sheepshanks   Mary Barton as Mrs. Sheepshanks  
* Glynis Johns as Winnie 
* Myrette Morven as Miss Stevens  
* Roddy Hughes as Film Director  
* John Robinson and The Rhythm Brothers as Themselves 
* Don Marino Baretto as Band Leader 
* Paul Sheridan as Minor role Charles Eaton as Minor role
* Paul Henreid as Minor role
* Terry-Thomas as Party Guest

==References==
 

==Bibliography==
* Murphy, Robert. Realism and Tinsel: Cinema and Society in Britain, 1939-1949. Routledge, 1992.

==External links==
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 