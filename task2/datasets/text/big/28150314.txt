A Time for Loving
 
{{Infobox film
| name           = A Time for Loving
| image          = 
| caption        = 
| director       = Christopher Miles
| producer       = Nat Cohen Dimitri De Grunwald
| writer         = Jean Anouilh
| screenplay     = 
| story          = 
| based on       =  
| starring       = Joanna Shimkus Mel Ferrer Britt Ekland Philippe Noiret
| music          = Michel Legrand
| cinematography = Andréas Winding
| editing        = Henri Lanoë
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

A Time for Loving is a 1971 British comedy-drama film directed by Christopher Miles and starring Britt Ekland, Joanna Shimkus and Mel Ferrer.  The film depicts several stories surrounding an apartment in Paris, and the various people who occupy it over the years. It was also released as Room in Paris.

==Cast==
* Joanna Shimkus - Joan McLaine
* Mel Ferrer - Doctor Harrison
* Britt Ekland - Josette Papillon
* Philippe Noiret - Marcel Dutarte-Dubreuilh
* Susan Hampshire - Patricia Robinson Mark Burns - Geoff Rolling
* Lila Kedrova - Madame Olga Dubillard
* Robert Dhéry - Leonard
* Michel Legrand - Monsieur Grondin
* Didier Haudepin - Fils de la concierge
* Ophelie Stermann - Simone, Young girl
* Jany Holt - Mme. Dutarte-Dubreuilh, Marcels mother
* Eléonore Hirt - Héloïse Dutarte-Dubreuilh
* Lyne Chardonnet - La fille du bar
* Gilberte Géniat - La concierge

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 