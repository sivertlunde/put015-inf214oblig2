Lakshmi Kataksham
{{Infobox film
| name           = Lakshmi Kataksham
| image          = Lakshmi Kataksham.jpg
| image_size     =
| caption        =
| director       = B. Vittalacharya
| producer       = Pinjala Subba Rao
| writer         = 
| narrator       =
| starring       = N.T. Rama Rao K.R. Vijaya Rajasree Kaikala Satyanarayana M. Prabhakar Reddy M. Balayya 
| music          = S. P. Kodandapani
| cinematography = 
| editing        = 
| studio         =
| distributor    =
| released       = 1970
| runtime        =
| country        = India Telugu
| budget         =
}}

Lakshmi Kataksham or Laxmi Kataksham ( ) is a 1970 Telugu musical film directed by B. Vittalacharya and starring N. T. Rama Rao and K. R. Vijaya.

The lyrics are written by Dr. C. Narayana Reddy and music score provided by S. P. Kodandapani. Some of the songs are romantic and sung by Ghantasala Venkateswara Rao and P. Susheela. The song Sukravarapu Poddu Sirini Viduvoddu provides some ancient Hindu principles to keep Money in our Home.

==Plot==
The story revolves around procuring Lakshmi Bhandagaram. Both the evil forces and kind people fight to procure. Finally Kulavardhanudu (NTR), who got Padmarekha on his palm achieves it. He saves his parents and lover Hemamalini (KR Vijaya) from the control of Prachandudu (Satyanarayana).

==Cast==
{| class="wikitable"
|-
! Actor/Actress !! Character
|-
| N. T. Rama Rao || Kulavardhanudu
|-
| K. R. Vijaya || Rani Hemamalini
|-
| Kaikala Satyanarayana || Prachandudu
|-
| M. Prabhakar Reddy ||
|-
| Hemalatha ||
|-
| M. Balayya || Vinayadandudu
|-
| Rajasree ||
|-
| Mikkilineni ||
|-
| Balakrishna ||
|}

==Crew==
* Director : B. Vittalacharya
* Producer : Pinjala Subba Rao
* Production company : P. S. R. Pictures
* Music Director : S. P. Kodandapani
* Lyrics : C. Narayana Reddy, Kosaraju Raghavaiah and Chillara Bhavanarayana Rao
* Playback singers : Ghantasala Venkateswara Rao, P. Susheela, S. Janaki, L. R. Eswari

==Soundtrack==
There are about 13 songs and poems in the film. 
* Ammammammomo Telisindile Guttu Telisindile (Lyrics: C. Narayana Reddy; Singers: Ghantasala Venkateswara Rao and P. Susheela; Cast: NTR and KR Vijaya)
* Andala Bommanu Nenu Chelikada (Singer: L. R. Eswari)
* Dhanyosmi Dhanyosmi Trailokya Mata (Dandakam by Chillara Bhavaranayana Rao; Singer: Ghantasala Venkateswara Rao; Cast: NTR)
* Gata Suvignana Prakasammu Marala Kalpinchitivi Talli (Lyrics: Chillara Bhavanarayana Rao; Singers: Ghantasala and S. Janaki)
* Jaya Jaya Mahalakshmi Jaya Mahalakshmi
* Kilakila Bullammo Kiladi Bullammo (Lyrics: Kosaraju Raghavaiah; Singers: Ghantasala and S. Janaki; Cast: NTR and Rajasri)
* Naa Vayasu Sumagandham Naa Manasu Makarandam (Singer: P. Susheela)
* Naadu Gurudevu Karyarthinavuchu Nedu Vachitini (Lyrics: Chillara Bhavanarayana Rao; Singer: Ghantasala)
* Ponna Chettu Maatuna Poddu Vaalipotundi (Singer: S. Janaki)
* Raa Vannela Dora Kanniyanu Chera (Lyrics: C. Narayana Reddy; Singers: P. Susheela and Ghantasala; Cast: KR Vijaya and NTR)
* Sakala Vidyamayee Ghana Sharadendu Ramya (Lyrics: Chillara Bhavanarayana Rao; Singer: Ghantasala)
* Sukravarapu Poddu Sirini Viduvaddu (Lyrics: Chillara Bhavaranayana Rao; Singer: S. Janaki)
* Swagatham Swagatham Kshatrava Janajaita Swagatham (Lyrics: C. Narayana Reddy; Singers: S. Janaki and P. Leela)

==References==
 

==External links==
*  

 
 
 