List of Malayalam films of 1960
 

The following is a list of Malayalam films released in 1960.
{| class="wikitable"
|- style="background:#000;"
! colspan=2 | Opening !! Sl. No. !!Film !! Cast !! Director !! Music Director !! Notes
|-
| rowspan="1" style="text-align:center; background:#ffa07a; textcolor:#000;"| M A R 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 29
|valign="center" |  1  Umma || Thikkurissy Sukumaran Nair, B. S. Saroja || Kunchacko || M. S. Baburaj ||
|-
| rowspan="2" style="text-align:center; background:#dcc7df; textcolor:#000;"| S E P 
| rowspan="2"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 3
|valign="center" |  2 
| Poothali || Thikkurissi Sukumaran Nair || P. Subramaniam || Br Lakshmanan ||
|-
|valign="center" |  3  Seetha || Prem Nazir, Kushalakumari || Kunchacko || V. Dakshinamoorthy ||
|-
| rowspan="2" style="text-align:center; background:#d0f0c0; textcolor:#000;"| D E C 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 3
|valign="center" |  4 
| Sthreehridayam || Pappukutty Bhagavathar, T. S. Muthaiah, Ambika || JD Thottan || LPR Varma ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 23
|valign="center" |  5 
| Neeli Saali || Boban Kunchacko, Bahadoor, Vilasini || Kunchacko || K. Raghavan ||
|}

 
 
 

 
 
 
 
 