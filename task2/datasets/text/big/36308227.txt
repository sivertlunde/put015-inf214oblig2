Aarohanam
 
{{Infobox film
| name           = Aarohanam
| image          = 
| caption        = 
| director       = Lakshmy Ramakrishnan
| producer       = A.V._Anoop|A. V. Anoop
| story          = Lakshmy Ramakrishnan
| screenplay     = Lakshmy Ramakrishnan
| starring       = Viji Chandrasekhar Jayaprakash Uma Padmanabhan Rajee Vijayasarathy K
| cinematography = N. Shanmugasundaram
| editing        = Kishore Te.
| studio         = AVA Productions Monkey Creative Labs
| distributor    = JSK Film Corporation
| released       =  
| runtime        = 90 minutes
| language       = Tamil
| subtitle by    = rekhs
| country        = India
| budget         = 
}}
Aarohanam ( ) is 2012 Tamil drama film directed by actress-turned-director Lakshmy Ramakrishnan. It stars Viji Chandrasekhar, Jayaprakash, Uma Padmanabhan and Rajee Vijayasarathy among others. According to the director, the film revolves around a missing mother, who suffers from bipolar disorder. The film received mostly positive reviews, with some critics calling it the best Tamil film of the year.  

==Cast==
* Viji Chandrasekhar as Nirmala
* Jayaprakash
* Sampath Raj
* Veeresh as Senthil
* Jai Queheni as Selvi
* Uma Padmanabhan as Jay
* Rajee Vijayasarathy as Sandy
* Kavithalaya Krishnan
* G. Marimuthu
* Abhinay as Sandys Security
* Preethi

==Production==
In early February 2012, reports claimed that character actress Lakshmy Ramakrishnan would direct her maiden feature film,  although Lakshmy had begun filming in December 2011 already.  Titled as Aarohanam, it was said to be based on a "series of amusing events that take place on a single day"; the film would follow a 17-year-old boy and his sister who are in search of their missing mother, played by television artiste and actress Sarithas sister, Viji Chandrasekhar. Lakshmy along with her friends, actresses Uma Padmanabhan and Rajee Vijayasarathy, initially wanted to make a "desi Hangover kind of a film" about spirited women.  When developing the third character in the film, she realized that she had penned a story about that person in 2008 already. She reworked the script and made that character the lead, while also deciding to Co produce the film.  Lakshmy said that she had written 45 versions of the screenplay for Aarohanam.  She further stated that the film was initially started with the intention to screen it at film festivals only, but that eventually it turned into a "commercial" film. 

Character actors Jayaprakash does a cameo and  Sampath Raj and director Marimuthu were selected to essay pivotal supporting roles,  while newcomers Veeresh and Jai Queheni would play the missing mothers son and daughter, respectively.  Veeresh was then studying editing in his final year at the MGR Film Institute, Taramani and got the opportunity when he approached director Lakshmy to join her as an assistant director.  The entire film was shot with a Canon EOS 5D digital camera; it was completed in 20 days. 

==Music==
{{Infobox album
| Name = Aarohanam
| Type = Soundtrack K
| Cover  = Aarohanam Audio Cover.jpg
| Caption = Front Cover
| Released = 19 June 2012 Feature film soundtrack
| Length =  Tamil	
| Label = Sony Music Entertainment
| Producer = K
| Reviews =
| Last album = Yuddham Sei (2011)
| This album = Aarohanam (2012)
| Next album = Mugamoodi (2012)
}}
 single track to coincide with Mothers day 2012.  The entire album was launched on 19 June 2012 at Prasad Studios in Chennai;  directors K. Balachander and K. S. Sethumadhavan released the audio.  "Thappattam", a club number, was sung by Lakshmys daughter Sharadha along with the composer. 

{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 
| all_lyrics      = Subbu
| title1          = Thappaattam
| extra1          = Sharadha R., Ranjini Chander, Harish
| length1         = 
| title2          = Indha Vaan Veli
| extra2          = Anand Aravindakshan
| length2         = 
| title3          = Dhishai Ariyaadhu
| extra3          = Vandana Srinivasan
| length3         = 
| title4          = Indha Vaan Veli – Flute Version
| extra4          = Instrumental
| length4         = 
| title5          = I Won’t Give Up
| extra5          = Instrumental
| length5         = 
| title6          = Ode To Motherhood
| extra6          = Instrumental
| length6         = 
}}

==Critical reception==
Aarohanam opened to mostly positive reviews from critics. Vivek Ramz from "in.com" gave the film 4.5 out of 5 and said that its a must watch experience and added that Its a unique masterpiece of cinema and one couldnt afford to miss this film at any cost!  N. Venkateswaran from The Times of India gave the film 4 out of 5 stars and claimed that it "could easily be the best Tamil film of the year" and that "it tackles a very important issue — mental illness — with a sensitivity that is rare to Tamil cinema, and does so without being judgmental about the main characters and their actions". The reviewer further mentioned that "a lot of recent films have had incorrect portrayals of mental illness and bipolar disorder, and Aarohanam helps in dispelling the darkness among the audiences", terming it a "must see for all lovers of good cinema".  Sifys critic wrote that "director Lakshmy Ramakrishnan can be proud of her maiden venture Aarohanam which slowly but beautifully unfolds and reaches its crescendo in the climax", further praising her for "making such a film with a neat message and at the same time not preachy".  Rediff gave 3 out of 5 stars and noted that Lakshmy Ramakrishnan "may have made her film more like a fictionalised documentary, and stumbled at a few places, but by and large, the message she delivers is a valid one".  Haricharan Pudipeddi from nowrunning.com cited: "If you miss Aarohanam, then you may have missed most likely the best Tamil film of the year".  Indiaglitz.com claimed that a film "with such a knot needs to be welcomed for it marks a shift from the regular mainstream cinema".  Baradwaj Rangan, too, lauded the film: "After a movie-going season filled with crushing big-budget disappointments, it’s wonderful to watch Aarohanam. The director Lakshmy Ramakrishnan has a bracingly uncomplicated approach to filmmaking, where it’s all about writing a solid script and etching out memorable characters and, finally, casting the right people". 

On the contrary, Malini Malnath from The New Indian Express panned the film, calling Aarohanam "at the most, a stepping stone for the filmmaker. It may serve as a valuable experience gained and a reference for how she could craft a more coherent screenplay, bringing in more clarity and focus on her narration". The critic further wrote: "The director should have done her homework before taking up such a serious issue as a concept. Both the script and treatment are amateurish".  Lakshmy Ramakrishnan, in return, clarified that "enough research had been done before locking the characterisation" and that several doctors the film was shown to were "delighted with the way the disorder is portrayed". 

==Awards==

{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards Best Actress in a Supporting Role Viji Chandrasekhar
| 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 