Texas Chainsaw 3D
{{Infobox film
| name           = Texas Chainsaw 3D
| image          = TheTexasChainsawMassacre3DPoster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = John Luessenhop
| producer       = Carl Mazzocone Adam Marcus Debra Sullivan Adam Marcus Debra Sullivan	 
| based on       =   Tremaine Neverson Tania Raymonde Thom Barry Paul Rae Bill Moseley  John Frizzell 
| cinematography = Anastas N. Michos
| editing        = Randy Bricker
| studio         = Twisted Pictures Nu Image Millennium Films  Mainline Pictures Lionsgate
| released       =  
| runtime        = 92 minutes  
| country        = United States
| language       = English
| budget         = $20 million 
| gross          = $47,241,945 
}} Adam Marcus. Tremaine Neverson, original 1974 film. The story centers on Heather, who discovers that she was adopted after learning of an inheritance from a long-lost grandmother. She subsequently takes a road trip with her friends to collect the inheritance, unaware that it includes her cousin, Leatherface, as well. Filming began in the summer of July 2011, with Kirsten Elms and Luessenhop providing rewrites to the script. Texas Chainsaw 3D was released on January 4, 2013.

==Plot== the original Tremaine Neverson), and two other friends, Nikki (Tania Raymonde) and Kenny (Keram Malicki-Sánchez), travel to Newt to collect her inheritance. Along the way, the group picks up a hitchhiker named Darryl (Shaun Sipos).

When they arrive, the Sawyer family lawyer, Farnsworth (Richard Riehle), gives her the keys to the family house along with a letter from Verna. Excited about the property she now owns, Heather and her friends look through the house, decide to stay the night, and immediately set off to buy supplies and food, trusting Darryl to stay behind and look after the house. Darryl begins looting it and believes he will find valuables in a locked room in the cellar; however, upon entering the locked room he is bludgeoned by Leatherface. Heather and her friends return and discover the house has been ransacked, but choose to let it go. As Kenny prepares dinner he finds the cellar, where Darryl was killed, and Leatherface impales him on a meat hook. Heather finds a decomposing body upstairs and runs to find her friends, but she is knocked unconscious by Leatherface. She wakes up in Leatherfaces room, but manages to escape to the graveyard. Hearing the screams and chainsaw, Ryan and Nikki draw Leatherfaces attention, while Heather gets their van and picks up her friends.

In the ensuing chase, Leatherface saws through one of the vans tires, causing it to crash and subsequently kill Ryan. Heather escapes and makes her way into a carnival, but Leatherface escapes the police as they patrol the grounds. Sheriff Hooper (Thom Barry) realizes that Leatherface survived the fire and is still alive; Mayor Hartman sends Officer Marvin (James MacDonald) to the Sawyer house to kill Leatherface against Hoopers orders. While looking for Leatherface, Marvin is startled by Nikki and accidentally shoots her, before being killed by Leatherface himself. While at the station, Heather learns of what the townspeople did to her family and flees. She is soon caught by Hartmans son, Deputy Carl Hartman (Scott Eastwood), who ties her up at the long-abandoned Sawyer slaughterhouse to lure Leatherface. Once there, Leatherface finds out that Heather is his cousin and frees her, but is attacked by Mayor Hartman and another officer, Ollie (Ritchie Montgomery). At first Heather begins to flee, but after hearing her cousin being beaten she decides to help him, killing Ollie with a pitchfork and giving Leatherface his chainsaw, which he uses to force Mayor Hartman into a meat-grinder. Afterward, Heather and Leatherface return home, where Heather reads the letter from Verna which tells her Leatherface will protect her as long as she takes care of him. Realizing Leatherface is the only family she has left, Heather decides to stay with him. 

In a post-credits scene, Gavin and Arlene show up at the mansion to visit Heather, intending on greedily splitting her assets. As they wait in front of the door, Leatherface answers with his chainsaw in hand.

==Cast==
* Alexandra Daddario as Heather Miller: The film follows Heather, who is travelling through Texas with her boyfriend Ryan to collect an inheritance which, unbeknownst to her, includes her cousin Leatherface.  
* Dan Yeager as Leatherface: Luessenhop stated that he picked Yeager because he felt a sense of "menace" after witnessing Yeagers 66" frame, "farm boy arms", and "brooding brow" stand "quiet and circumspect". He claimed he could no longer think of another actor afterward.   Tremaine Neverson as Ryan: Heathers boyfriend, who accompanies her on the trip through Texas.   
* Scott Eastwood as Deputy Carl Hartman: Town deputy and Burts son.  
* Tania Raymonde as Nikki: She is described as a "small town girl with an attitude", and the best friend of Heather.   
* Shaun Sipos as Darryl: A hitchhiker who catches a lift with Heather and her friends; Darryl "knows more than he lets on". 
* Keram Malicki-Sánchez as Kenny: Ryans friend 
* Thom Barry as Sheriff Hooper: Town sheriff 
* Paul Rae as Mayor Burt Hartman: Town mayor and Carls father. 
* Richard Riehle as Farnsworth: The Sawyer family lawyer. 
*  , who portrayed Drayton in the 1974 film and its sequel, who died in 2003. Director Luessenhop chose Moseley because he felt that he could portray the same "essence" that Siedow brought to the character.  Moseley previously portrayed Chop Top in The Texas Chainsaw Massacre 2. 
* Marilyn Burns as Verna Carson: Burns portrayed Sally Hardesty in the original Texas Chain Saw Massacre. 
* John Dugan as Grandpa Sawyer: Dugan is reprising his role as "Grandpa" from the 1974 film. 
* Gunnar Hansen as Boss Sawyer: Hansen last portrayed Leatherface in the 1974 original film.  
* David Born as Gavin Miller. Foster Father to Heather Miller.
* Sue Rock as Arlene Miller.  Foster Mother to Heather Miller

==Production== Adam Marcus were brought in to write the script; Kirsten Elms and Luessenhop worked on rewrites and script polishing.    Texas Chainsaw 3D had to be re-cut before release, as it received an Motion Picture Association of America film rating system|NC-17 rating due to excessive gore during its initial submission to the Motion Picture Association of America|MPAA. 

==Reception==

===Critical reviews===
Based on 73 reviews collected by Rotten Tomatoes, Texas Chainsaw 3D received an average 19% overall approval rating, with an average rating of 3.5/10; general consensus is that the film is making a "bold move" in trying to turn Leatherface into a "horror anti-hero", but ultimately is nothing more than "ugly and cynical" in its attempt.  On Metacritic, the film received a score of 31 out of 100, indicating "generally unfavorable reviews", based on 17 reviews.  IGN editor Eric Goldman wrote, "A few fun 3D-aided jump-scares aside, Texas Chainsaw 3D is a generic and laughable attempt to follow the original."  The film received a CinemaScore of "C+", with 63% of moviegoers being under the age of 25.   

===Box office===
On its opening night, Texas Chainsaw 3D took first place, earning approximately $10,200,000 at the North American box office.  The film ultimately took first place for the entire weekend, making $21,744,470.  As of March 2013, the film has made $39,093,317 worldwide. 
 

===Home media=== UltraViolet digital copy of the film along with multiple commentaries, an alternate opening, the theatrical trailer and a half-dozen making of behind-the-scenes featurettes. 

==Merchandise==
In March 2015 Hollywood Collectibles released a 20-inch Leatherface action figure, based on Dan Yeager potrayed figure. 

==References==
 

==External links==
* 
* 
* 
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 