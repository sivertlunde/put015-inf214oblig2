Intimacy (film)
{{Infobox film 
| name           = Intimacy 
| image          = Intimacy.jpg 
| director       = Patrice Chéreau
| producer       = Patrick Cassavetti Jacques Hinstin Charles Gassot
| screenplay         =Anne-Louise Trividic Patrice Chéreau
| based on       = Intimacy (novel)|Intimacy by Hanif Kureishi
| narrator       =  Alastair Galbraith Susannah Harker Timothy Spall
| music          = Eric Neveux
| cinematography = Francois Gedigier
| editing        = Karen Lindsay-Stewart
| distributor    = Empire Pictures Inc.
| released       = 23 November 2001
| runtime        = 119 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  $405,094 
}}
Intimacy is a 2001 British film directed by Patrice Chéreau, starring Mark Rylance and Kerry Fox.
 pop songs novel of unsimulated fellatio dubbed version features voice actors Jean-Hugues Anglade and Nathalie Richard.

The film has been associated with the New French Extremity. Quandt, James, "Flesh & Blood: Sex and violence in recent French cinema", ArtForum, February 2004   Access date: July 10, 2008. 

== Plot ==
Jay (Rylance) is a bartender who abandoned his family, because his wife lost interest in him and their relationship.

Now living alone in a decrepit house, he has casual weekly sex with an anonymous woman (Fox), whose name he doesnt know. At first, their relationship is purely physical, but he eventually falls in love with her.

Wanting to know more about her, Jay follows her across the streets of London to the grey suburbs where she lives.  He then follows her to a pub theatre where she is working as an actress in the evenings. Jay learns that her name is Claire, and has a husband (Timothy Spall) and a son.  Subsequently it is made clear to Jay that Claire will not leave her family. They meet for a final time, and have sex with an intimacy that has been missing during the illicit sex sessions of their previous encounters.

==Cast==
*Mark Rylance - Jay 
*Kerry Fox - Claire 
*Susannah Harker - Susan, Jays wife  Alastair Galbraith - Victor 
*  Philippe Calvario  - Ian 
*Timothy Spall - Andy 
*Marianne Faithfull - Betty 
*Fraser Ayres - Dave  Michael Fitzgerald - Bar owner 
*Robert Addie - Bar owner

==Reception==
Intimacy was placed at 91 on Slant Magazines best films of the 2000s. 

== Awards ==
Intimacy won the Golden Bear for Best Film and the Silver Bear for Best Actress (Kerry Fox) at the Berlin Film Festival in 2001.

==See also==
*List of mainstream films with unsimulated sex#Films with non-simulated sexual activity

== References ==
 

== External links ==
* 

 
 

 
 
 
 
 
 
 
 
 
 