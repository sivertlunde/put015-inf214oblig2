The Night Evelyn Came Out of the Grave
{{Infobox film
| name           = The Night Evelyn Came Out of the Grave  (La notte che Evelyn uscì dalla tomba) 
| image          = The-night-evelyn-came-out-of-the-grave.jpg
| image_size     = 
| caption        = 
| director       = Emilio Miraglia
| producer       = Antonio Sarno
| writer         = Massimo Felisatti Fabio Pittorru Emilio Miraglia
| narrator       = 
| starring       = Anthony Steffen Marina Malfatti Erika Blanc Giacomo Rossi-Stuart
| music          = Bruno Nicolai
| cinematography = 
| editing        = 
| distributor    =  1971 (Italy)
| runtime        = 103 min.
| country        = Italy Italian
| gross          =  
}}
 1971 Italy|Italian giallo film directed by Emilio Miraglia.  It was released in 1971 in film|1971.

==Plot==
Alan (Anthony Steffen) is a wealthy aristocrat who has just been released from a mental institution following the death of his wife, redheaded Evelyn. Having caught Evelyn making out with an unknown man prior to his institutionalization, the psychotic Alan begins luring redheaded strippers and prostitutes to his home to torture and kill them, as a means to deal with his grief and inability to get revenge on his deceased wife. 

Alan attends a séance in which the medium contacts Evelyn, causing Alan to faint. Alans cousin (and only living heir) Farley offers to move into the mansion to take care of him. Farley takes him to a strip club and Alan takes home Susan (Erika Blanc), one of the strippers at the club who disappears after barely escaping with her life. Afterwards, Farley believes that Alan would be cured of his instability if he replaces Evelyn with a new bride that resembles her. On Farleys advice, Alan moves to London to get away from his home and marries Gladys (Marina Malfatti), another redhead. 

Gladys finds herself being haunted by strange goings on at her new home and being shunned by Evelyns brother and Alans invalid aunt, whom Alan has taken in as staff at his mansion. Gladys tells Alan her suspicions that Evelyn faked her death to escape Alan and run away with her lover. Alans mental state continues to unravel as Evelyns brother and Alans aunt are each killed by a mystery killer and when he sees a zombified Evelyn beckoning to him from her tomb, he breaks down completely. 

When Alan is taken away for permanent institutionalization, Gladys and Farley celebrate as they had been trying to push Alan back into insanity, with Farley supposedly impersonating Evelyn, so that Farley would gain control of Alans fortune and estate. After they toast their success, Susan, the stripper that Alan had taken home, appears. Farley reveals that Susan was the one impersonating Evelyn and that the champagne Gladys is drinking has been poisoned. Farley stands by as Gladys attacks and kills Susan before succumbing to the poison.

However, Alan appears along with Richard, the doctor who treated him for his first breakdown. Alan had suspected he was being manipulated and had faked his most recent breakdown to lure out the conspiracy against him, after Alan discovered proof that Farley killed Evelyn after she refused to leave Alan for him. Farley tries to escape and in the ensuing fight, a bag of sulfur-based fertilizer falls into the nearby pool. The pool turns to acid and Farley falls in, horribly burning him. Pulling him out of the pool, Farley is arrested and Alan manages to get away with his crimes.

==Release==
The film was released theatrically in its native Italy in August 1971.

The film was released theatrically in the United States by Phase One in June 1972.

The film was released theatrically in the United Kingdom as The Night She Rose From The Tomb.

On VHS and DVD, the film has been released by various labels over the years with questionable legitimacy to the rights and subpar, truncated presentations.  

NoShame DVD released the officially licensed film remastered, widescreen and uncut on DVD in 2006 as part of the Emilio Miraglia Killer Queen Box Set.

The film was released on Blu-ray disc for the first time by Australian distribution company Gryphon Entertainment on 20 June, 2013. 

== Critical reception ==

Allmovie wrote "The Night Evelyn Came Out of the Grave might make acceptable fodder for giallo fans but isnt as memorable as its reputation suggests." 

== References ==
 

==External links==
*   at  
* 
*  

 
 
 
 
 
 
 
 
 

 
 