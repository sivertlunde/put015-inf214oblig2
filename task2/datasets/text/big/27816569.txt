Blessed (2008 film)
{{Infobox film
| name           = Blessed
| image          = Blessed poster.jpg
| caption        = Theatrical release poster
| director       = Mark Aldridge
| producer       = Mary Aldridge
| writer         = Mark Aldridge Gary Lewis Lil Woods
| music          = Mike Moran
| cinematography = Steve Weiser
| editing        = Nick Arthurs
| studio         = STORM pictures
| distributor    = G2 Pictures
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} Gary Lewis, Lillian Woods. Scottish island of Eilean Iarmain.

==Plot==
The film begins with a man named Peter who is living alone on an island as a lighthouse keeper. His only contact is with a sailor called Howie who brings him food supplies once every few months. It is quickly revealed that until recently he had a well-paid job, and a wife named Lou and two daughters. His family were drowned, however, in a freak storm on a boating holiday that Peter was meant to be on. This causes him to withdraw from society and he is rendered unable to speak. He sticks to a regular but lonely routine until one day he discovers that a lifeboat containing a little girl of about seven called Charlotte has landed on the island. Charlotte refuses to leave the island until her father comes to pick her up, so she stays with Peter. After just over a week, he finds out from a radio broadcast that she is the only survivor from a sinking ship, and that the girls father is dead. It is at this point he becomes able to speak again. During this time the two have formed a bond, and so when Charlotte refuses to leave, he decides that she can stay. This is possible as no-one else knows that she is alive. At the end of the film Peter has to persuade his food supplier to keep quiet about the little girls presence on the island so she is not taken away by the authorities, he agrees and the film ends showing the viewer several "family snapshots" of an adult Charlotte and an older Peter, thus implying that she remained with him for the rest of her childhood.

==Cast==
*James Nesbitt as Peter
*Natascha McElhone as Lou Gary Lewis as Howie
*Lil Woods as Charlotte

==External links==
*  
* http://www.film4.com/reviews/2008/blessed
* http://uk.movies.yahoo.com/b/Blessed/index-7451308.html
* http://www.rollcreditsonline.com/reviews/dvdreviews/827-blessed-james-nesbitt-dvd-review.html
* http://www.stalepopcorn.co.uk/reviews/dvd-reviews-r2/dvd-review-r2-blessed/

 
 
 