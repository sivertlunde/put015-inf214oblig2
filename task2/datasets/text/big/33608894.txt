Blood River (film)
{{Infobox film
| name           = Blood River
| image          = BloodRiver2009film.jpg
| caption        = 
| director       = Adam Mason
| writer         = Adam Mason Simon Boyes
| starring       = 
| cinematography = Stuart Brereton
| editing        = Adam Mason
| producer       = Timothy Patrick Cavanaugh Mary Church Patrick Ewald Lee Librado Adam Mason
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| music          = 
| studio         = Epic Pictures Group
| budget         = 
| gross          = 
}}

Blood River is a 2009 psychological thriller film written by Simon Boyes and Adam Mason and directed by Adam Mason. It follows a newlywed couple’s relationship during a chance encounter with a mysterious drifter in a deserted ghost town.

==Plot==
Clark and Summer, a young married couple, are on their way to see Summers parents. Their car has a blowout and, as they do not have a spare tire, the couple decides to walk onwards and find help. Upon arriving at a small collection of buildings, they realise that the town is abandoned. It isnt long before a drifter in a cowboy hat comes along. The stranger introduces himself as Joseph, and immediately overshadows Clark with his personality and style. That evening, around a campfire, the three talk: Summer shows Joseph a photo of her older son, Ben, who is Clarks stepson, while Joseph talks about how unlike Clark - who is a slave to society — he is himself, being beholden to no one but God. God is great god is good

The next morning, Joseph suggests that he and Clark walk back to the couples car and siphon off the gas, then continue on to Josephs truck and leave the area. Summer wants to accompany them, but Joseph and Clark both say that in her delicate condition, she should remain. Joseph pulls out a revolver and teaches Summer how to use it, touching her in an intimate way. Clark is obviously furious, but he and Joseph still set off together. On the way, Joseph continually mocks Clark for his weaknesses, claiming to know everything about him. Clark is convinced that Joseph is a fanatical, religious hippie. When they arrive, Joseph tells Clark that everything he is about to go through is his own fault, because of his sins, then disappears. Clark opens the trunk of his car to find Bens rotting body, buzzing with flies. He is horrified, then realises that he has left Summer alone and rushes back.

Meanwhile, Summer has found a room in one of the abandoned buildings with photographs on the wall, including her photo of Ben, with a cross scratched over his face. She turns to find that Joseph is in the room with her. She attempts to shoot him, but the gun he gave her is unloaded. Joseph subdues her and uses a razor to carve a cross into her forehead, saying that she will have the mark forever and will remember what happened there. She begs to know why he is doing this and he claims that it is Gods will. At that moment, Clark arrives and attacks Joseph, beating him to the ground.

When Joseph regains consciousness, he is tied to a chair. Clark demands to know how they can escape the town, but Joseph wont say. Clark takes a pair of pliers and cuts off one of Josephs fingers in order to make him talk. Finally Joseph admits that he is an avenging angel, sent to punish the wicked for their sins. He suggests that Clark tell Summer what was in the trunk of their car but Clark says it was nothing. Joseph then agrees to help them as long as he can first show them something down by the river. Just past the water tower, there is a field of rough, wooden crosses, marking graves. Summer asks if Joseph killed all those people but he claims that he never killed anyone; all did it to themselves and every one of them deserved it.

Further into the makeshift graveyard, there are three empty graves. Joseph again exhorts Clark to tell Summer what his sin is. When Clark refuses, Joseph loads the gun and hands it to Summer, reiterating that she need only cock it and squeeze the trigger. Receiving no response to Josephs repeated question, Summer assumes it has something to do with Ben and finally shoots Clark. When she asks what she has done to deserve this pain, Joseph tells her that her sin is apathy: she knew something was wrong but did nothing about it.

Joseph raises his hand to the sky and murmurs a prayer, and his finger reappears. His face is no longer wounded and bloody. Leaving Summer at the graveyard with the gun, he walks off. Summer attempts to shoot herself, but there had been only one bullet in the chamber.

==Cast==
* Dillon Borowski as Benny
* Ian Duncan as Clark
* Sarah Essex as Inn Keeper
* Andrew Howard as Joseph
* Tess Panzer as Summer

==Release==
Blood River debuted on 17 April 2009 at the Atlanta Film and Video Festival and opened in other film festivals on the dates given below.

{| class="wikitable sortable"
|- style=background:#ccccff
! Region
! Release date
! Festival
|- United States
| style="background: #f7f8ff; white-space: nowrap;" |  Atlanta Film and Video Festival
|- United Kingdom
| style="background: #f7f8ff; white-space: nowrap;" |  Dead by Dawn 
|- United States
| style="background: #f7f8ff; white-space: nowrap;" |  Visionfest 
|- Canada
| style="background: #f7f8ff; white-space: nowrap;" |  Fantasia Festival 
|- United States
| style="background: #f7f8ff; white-space: nowrap;" |  Maelstrom International Fantastic Film Festival 
|- Spain
| style="background: #f7f8ff; white-space: nowrap;" |  Sitges Film Sitges International Fantastic Film Festival 
|}
 

===Home release===
Blood River was released for online viewing or download  and on DVD  on 19 July 2010.

==References==
{{Reflist|refs=
   

   

   

   

   

   

   
}}

== External links ==
*  
*  
*  

 
 
 
 
 
 