The Other Side of the Bed
{{Infobox film  name = The Other Side of the Bed image =The Other Side of the Bed movie poster.jpg caption =  director = Emilio Martínez Lázaro  writer = David Serrano starring = Ernesto Alterio, Paz Vega, Guillermo Toledo  producer = Tomás Cimadevilla  distributor = Lions Gate Entertainment  budget = released =   runtime = 114 minutes language = Spanish
|}}
 2002 film directed by Emilio Martínez Lázaro.

==Main cast==
*Ernesto Alterio &ndash; Javier
*Paz Vega &ndash; Sonia
*Guillermo Toledo &ndash; Pedro
*Natalia Verbeke &ndash; Paula
*Alberto San Juan &ndash; Rafa

==Synopsis==
Sex, love, lies, bed-hopping and mistaken identities abound in this pop musical-comedy set in Madrid. The gorgeous Paula breaks up with her boyfriend Pedro in order to continue her affair with Javier. The immature Javier however, is unwilling to break up with his current girlfriend Sonia, or confess to their affair to Pedro, who happens to be his best friend.

==External links==
* 

 
 
 
 
 


 
 