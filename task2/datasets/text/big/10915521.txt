Sword Stained with Royal Blood (1982 film)
 
 
{{Infobox film name = Sword Stained with Royal Blood image = Sword Stained with Royal Blood (1982 film).jpg image_size = 200px caption = DVD cover director = Chang Cheh producer = Shaw Brothers Studio story = Louis Cha screenplay = Chang Cheh narrator = starring = Philip Kwok Lu Feng Chiang Sheng Wang Li Wen Hseuh-erh Lung Tung-sheng music = cinematography = editing = distributor = released = 1982 runtime = country = Hong Kong language = Mandarin
|budget =  gross =  preceded_by =  followed_by = 
}} novel of Louis Cha. The film was one of the rarest Venom martial arts film available, and has been digitally remastered and released by Celestial Pictures.

==Plot==
The story begins with the exile of a young boy who grows up to meet his destiny as a great warrior. After his father, a Han loyalist and hero, is betrayed by the Emperor and sentenced to death, Yuan Chengzhi is spirited away to the reclusive master of the Lung Yau school of martial arts. Having grown into a righteous young man of considerable martial skill, Yuan sets out on his own. He discovers the hideout of a long-dead martial arts master known as Golden Snake Xia Xueyi and lays claim to his buried martial arts manual, sword, and collection of darts. He also discovers the whereabouts of a lost treasure and instructions to deliver a portion of it to a certain woman. Cheng-chih sets out to find her in order to honor the dead mans wishes and ends up meeting a spoiled and not-so-cleverly disguised young woman posing as a man named Wen Qingqing. Its enough to fool the naive Yuan Chengzhi, who befriends Wen after she takes a liking to him. She brings him into her household, which is home to a wealthy clan of martial artists known for their mastery of the Five Element Array. Yuans stay grows unsettling, first when jealous quarreling sparked by his presence erupts between Wen and her cousin. Things get a lot more complicated when a trio of angry martial artists storm the household and accuse Wen of theft. It turns out that they are members of Yuans school and just as the situation threatens to turn into a full-scale battle, he intercedes in order to find a peaceful solution. As a result of his intervention, Yuans skills draw the attention of the master of the house, who recognizes the kung fu techniques of his arch-enemy. As hidden truths about the Wen clan and their dark part are revealed through flashbacks, Yuan finds himself forced to fight their infamous Five Element Array in order to complete his quest and escape in one piece.

==Cast==
*Philip Kwok as Yuan Chengzhi
*Wen Hsueh-erh as Wen Qingqing
*Lung Tien-hsiang as Xia Xueyi
*Ching Li as Wen Yi
*Chiang Sheng as Wen Zheng
*Wang Li as Wen Fangshan
*Lu Feng as Wen Nanyang
*Yu Tai-ping as Wen Beiyang
*Chu Ke as Wen Fang
*Chao Kuo as Cui Ximin
*Siao Yuk as Mei Jianhe
*Zhang Chiung-yu as Sun Zhongjun
*Chu Tit-wo as Huang Zhen
*Kwan Fung as Wen Fangda
*Jim Sum as Rong Cai
*Wang Hua as Mute
*Li Shouqi as Mu Renqing
*Wang Ching-ho as Taoist Musang
*Ng Hong-sang as Wen Fangshi
*Keung Hon as Wen Fangwu
*Lui Hong as Wen Yis mother

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 