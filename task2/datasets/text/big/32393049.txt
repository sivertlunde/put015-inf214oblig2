Willie's Magic Wand
{{Infobox film
| name           = Willies Magic Wand
| image          = Willies Magic Wand (1907).webm
| image_size     = 
| caption        = 
| director       = Walter R. Booth
| producer       = 
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Charles Urban Trading Company
| distributor    =
| released       =  
| runtime        = 2 mins 45 secs extent
| country        = United Kingdom Silent
| budget         =
}}
 1907 UK|British short  silent comedy film, directed by Walter R. Booth, featuring a young boy terrorising the household with his fathers magic wand. Similar to "earlier trick films The Haunted Curiosity Shop and Undressing Extraordinary (both 1901)," this is, according to Michael Brooke of BFI Screenonline, "essentially a series of   special-effects set pieces," however, "the print in the National Film and Television Archive is incomplete, omitting amongst other things a come-uppance where Willie is punished for his misdemeanours by being turned into a girl, thus depriving him of more than one magic wand." A clip from the film is featured in Paul Mertons interactive guide to early British silent comedy How They Laughed on the BFI website.         

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 