Starter for 10 (film)
 
 
{{Infobox film
| name           = Starter for 10
| image          = Starter for ten.jpg
| image_size     = 215px
| alt            =
| caption        = US release poster Tom Vaughan
| producer       = {{Plain list | 
* Tom Hanks
* Pippa Harris
}} David Nicholls
| starring       = {{Plain list | 
* James McAvoy
* Alice Eve
* Rebecca Hall
* Dominic Cooper
}}
| music          = Blake Neely
| cinematography = Ashley Rowe
| editing        = Heather Persons
| studio         = {{Plain list | 
* Playtone
* HBO Films
* BBC Films
* Neal Street Productions
}} Picturehouse  
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = $8,287,000 
| gross          = $1,736,394 
}} Tom Vaughan David Nicholls, Starter for Ten. The film stars James McAvoy as a university student who wins a place on a University Challenge quiz team. It premiered at the Toronto Film Festival in September 2006, and was released in the UK and Ireland on 10 November 2006, and in Canada and the US on 23 February 2007.

==Plot==
In 1985, Brian Jackson is a first-year university student and information sponge. Since his working-class childhood in Southend-on-Sea, Brian has loved the TV quiz show University Challenge, whose famous catchphrase—"Your starter for 10"—gives the film its title. Soon after arriving at Bristol University, Brian joins their University Challenge team and promptly falls for his glamorous teammate Alice, though he may be more compatible with the politically conscious Rebecca. Brian finds himself caught between his highbrow new milieu at Bristol and the culture of his straight-talking friends and family from home.

  

==Cast==
* James McAvoy as Brian Jackson
* Alice Eve as Alice Harbinson
* Rebecca Hall as Rebecca Epstein
* Dominic Cooper as Spencer
* James Corden as Tone
* Simon Woods as Josh
* Catherine Tate as Julie Jackson
* Elaine Tan as Lucy Chang
* Charles Dance as Michael Harbinson
* Lindsay Duncan as Rose Harbinson
* Benedict Cumberbatch as Patrick Watts
* Mark Gatiss as Bamber Gascoigne
* James Gaddas as Mr. Jackson
* John Henshaw as Des

==Production==
Despite being set at Bristol University, the main quad and Gustave Tuck lecture theatre of  . Brians home is shown as a quaint seaside cottage, supposedly located in Westcliff-on-Sea. These scenes were actually filmed in the small village of Jaywick, near Clacton-on-Sea, with the real Westcliff bearing little resemblance to its onscreen depiction. Clacton Pier was used for the scenes set on the pier at Southend-on-Sea.

==Reception==
The film received positive reviews, with an 89% rating on Rotten Tomatoes based on 75 reviews; the sites consensus states: "Starter For 10 is a spirited coming-of-age tale that remains charming and witty even as it veers into darker territory. The unique setting of a quiz show makes the film wittier than your average romantic comedy." 

==Soundtrack== Ace of The Young Ones, filmed in Bristol, that included the University Challenge spoof.

==References==
 

==External links==
*  
*  
*  
*  
*  
*   on Allmusic.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 