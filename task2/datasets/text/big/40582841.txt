Lady of the Dynasty
 
{{Infobox film
| name           = Lady of the Dynasty
| film name      =  |traditional= |pinyin=Wángcháo de Nǚrén Yáng Guìfēi|jyutping=Wong4ciu4  dik1 Neoi5jan4 Jeong4 Gwai3fei1}}
| image          = ladyofthedynasty.jpg
| alt            =
| caption        = Poster
| director       = Cheng Shiqing
| producer       = 
| screenplay     = 
| story          = 
| based on       = 
| starring       = Fan Bingbing Leon Lai Wu Chun Joan Chen
| music          = Hou Yong
| editing        = 
| studio         = China Film Group Corporation Chunqiuhong Cultural Investment Company Tristone Entertainment Inc.
| distributor    = 
| released       =   
| runtime        =
| country        = China
| language       = Mandarin
| budget         = $20 million 
| gross          =
}} epic Romance romance war film directed by Cheng Shiqing (writer of Codename Cougar) and featuring Fan Bingbing, Leon Lai, Wu Chun, Joan Chen, Ning Jing. The film also had a director group including Zhang Yimou and Tian Zhuangzhuang.  The film is scheduled for release on December 2015. 

==Premise==
A love story between Yang Guifei and Li Longji, also known as Emperor Xuanzong of Tang.

==Cast==
*Fan Bingbing as Yang Guifei
*Leon Lai as Emperor Xuanzong of Tang Li Mao, Prince of Shou
*Joan Chen as Consort Wu
*Ning Jing

==Production==
In March 2009, Yang Guifei officially announced, then the productiuon companies included a British company. In April 2011, the film was renamed as Tang Crisis, but when the British production company withdrew its investment, the film turned into a Chinese-Japanese co-production, and confirmed Kwak Jae-yong as the director. In October 2011, at the Busan International Film Festival ,it was reported that the film renamed as Yang Guife and confirmed that stars Fan Bingbing and Leehom Wang. 

In January 2012, the film began to shoot. Japanese actor Shun Oguri played Yang Guifeis former husband in this film.  But the concepts between the director and the production company were different, the production company intended to replace the director Kwak Jae-yong, who resigned lately. It was reported that the production company said Kwak refused to shoot the film according to its requirements which emphasized a traditional Chinese understanding of the Tang dynasty. 

In March 2012, Tian Zhuangzhuang participated in this film, but his position was unknown.   while Japanese investors announced the divestment.

In August 2013, after a year-long script modifications, the film planned to restart.  However, only Fan Bingbing remains on the casting list.  The films launch event was held in Beijing on Sunday, September 1, 2013.   The shoot period lasted four months, ending on 31 December 2013, according to Fans weibo.

==References==
 

==External links==
* 
* 
*    on Sina Weibo

 
 
 
 
 
 
 
 
 
 
 
 
 
 