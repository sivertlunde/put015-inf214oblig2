The Arrows of Robin Hood
{{Infobox film
| name           = The Arrows of Robin Hood
| image          =
| image_size     =
| caption        =
| director       =
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Riga Film Studio 1975
| runtime        =
| country        = Soviet Union
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
  Soviet 1975 1975 film Sergei Tarasov.

2 soundtracks exist for the movie. In 1975 Vladimir Vysotsky wrote and performed 7 ballads, 6 of them were included in the final version. However a recommendation by Goskino editorial board called them inadequate for a romantic adventure; the real reason being conflicts with Vysotsky. In 1976 new songs were performed by Aija Kukule and Viktors Lapčenoks, lyrics by Lev Prozorovsky, music by Raimonds Pauls, this version was released in the cinemas. 4 of Vysotskys songs were later used in 1982 film The Ballad of the Valiant Knight Ivanhoe also directed by Tarasov, set in the same time and place and using some of the same characters. In the 1990s the film was successfully re-released with the 1975 soundtrack. The DVDs also have the 1975 soundtrack.

==Cast== Robin Good
* Regīna Razuma
* Vija Artmane
* Eduards Pāvuls
* Hariy Shveiyts
* Algimantas Masyulis
* Yuri Kamornyj
* Juris Strenga
* Ints Burāns
* Mirdza Martinsone
* Yanis Plesums
* Mārtiņš Vērdiņš
* Nikolay Dupak
* Ivars Kalniņš

==External links==
*  

 
 
 
 
 
 
 


 
 