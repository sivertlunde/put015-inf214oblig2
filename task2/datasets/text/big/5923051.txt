International Velvet (film)
{{Infobox film
| name           = International Velvet
| image          = International Velvet - Poster.jpg
| image_size     = 250
| caption        = Theatrical poster
| director       = Bryan Forbes
| producer       = Bryan Forbes
| writer         = Bryan Forbes
| based on       =  
| starring       = Tatum ONeal Nanette Newman Anthony Hopkins Christopher Plummer Jeffrey Byron Sarah Bullen Richard Warwick
| music          = Francis Lai
| cinematography = Tony Imi
| editing        = Timothy Gee
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 127 min.
| country        = United States
| language       = English
}}
 National Velvet. The film stars Tatum ONeal, Christopher Plummer, Anthony Hopkins and Nanette Newman.  The film received mixed reviews. International Velvet was partly filmed at Birmingham University, England.

==Plot== Olympic Three Day Event helping Great Britain win the team competition. She falls in love with an American competitor and moves back to America with him. At the conclusion of the film she gives her Olympic gold medal to her Aunt Velvet. Sarah introduces her fiance by saying: "Scott, I would like you to meet my parents".

==Cast==
* Tatum ONeal as Sarah Brown 
* Christopher Plummer as John Seaton 
* Anthony Hopkins as Captain Johnson 
* Nanette Newman as Velvet Brown 
* Peter Barkworth as Pilot 
* Dinsdale Landen as Mr. Curtis 
* Sarah Bullen as Beth 
* Jeffrey Byron as Scott Saunders 
* Richard Warwick as Tim 
* Daniel Abineri as Wilson 
* Jason White as Roger 
* Martin Neil as Mike 
* Douglas Reith as Howard 
* Dennis Blanch as Policeman 
* Norman Wooland as Team Doctor 
* Susan Jameson as T.V. Interviewer 
* Brenda Cowling as Alice

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 