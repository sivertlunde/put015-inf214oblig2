A Moment of Romance II
 

{{Infobox film
| name           = A Moment of Romance II
| image          = AMomentofRomanceII.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 天若有情II之天長地久
| simplified     = 天若有情II之天长地久
| pinyin         = Tiān Ruò Yǒu Qíng Èr Zhī Tiān Cháng Dì Jiǔ
| jyutping       = Tin1 Jeok6 Jau5 Cing4 Ji6 Zi1 Tin1 Ceong4 Dei6 Gau2}} Benny Chan
| producer       = Johnnie To
| writer         = 
| screenplay     = Susan Chan
| story          = 
| based on       =  Anthony Wong
| narrator       = 
| music          = William Hu
| cinematography = Ardy Lam Patrick Jim
| editing        = Ma Chung-yiu
| studio         = China Entertainment Films Paka Hill Films
| distributor    = Newport Entertainment
| released       =  
| runtime        = 88 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$9,146,482
}}
 Hong Kong Benny Chan and starring Aaron Kwok and Jacklyn Wu. It is the second installment of the A Moment of Romance trilogy, featuring a new storyline. The film is followed by a final installment, A Moment of Romance III (1996), with producer Johnnie To taking the helm as director and features the return of Andy Lau, the star of the first insatllment.

==Plot==
Celia (Jacklyn Wu) illegally enters Hong Kong from mainland China and works as a prostitute in order to earn money to save her younger brother from prison. While working for the first time, Celia witnesses the murder of a triad leader and gets framed for the murder. As triad members are chasing her, she is rescued by Frank (Aaron Kwok), a member of a biker gang. Frank came from a wealthy but broken family and always feels depressed. However, he later becomes inspired by Celias upbeat personality and the two of them fall in love. Celia then leaves Frank in order to avoid bringing him into trouble with the triads, but Frank unconditionally sacrifices his life to save her.

==Cast==
*Aaron Kwok as Frank Cheung
*Jacklyn Wu as Celia
*Roger Kwok as Jack Anthony Wong as Dinosaur
*William Ho as Brother Hung Kui (cameo) Paul Chun as Mr. Cheung (cameo)
*Kwan Hoi-san as Bill
*Hon Yee-sang as Paul
*Ng Wui as Uncle Man
*Tse Wai-kit as Hairy
*Cheung Tat-ming as Celias brother (cameo)
*Cheung Wing-cheung as Pauls thug
*Cindy Yip as bar girl
*Lee Siu-kei as loan shark
*Alan Mak as cop
*Johnny Wong as illegal bike racer
*Simon Cheung as police segreant
*Tse Chi-wah as Mr. Cheungs servamt
*Alan Ng as Bills superior

==Music==

===Theme song===
*Even If Lovers (就算是情人)   / If I Cannot Be With You This Life (如果今生不能和你一起)  
**Composer: William Hu
**Lyricist: Lin Xi
**Singer: Aaron Kwok

===Insert theme===
*I Would Just Wait For You If I Will Reborn (再生亦只等待你)
**Composer: William Hu
**Lyricist: Siu Mei
**Singer: Aaron Kwok

*With You in My Life (在我生命中有了你)   / Warm Fate (驟暖的緣份)  
**Composer: William Hu
**Lyricist: Siu Mei
**Singer: Aaron Kwok, Jacklyn Wu

==Reception==

===Critical===
Compared the ist previous insatllment, the film did not fare as well with critics.   writes "While the first film at least had some thematic and dramatic justification for its tragic resolution, this one just seems to follow a similar pattern to get a rise out of this audience." 

===Box office===
The film grossed HK$9,146,482 at the Hong Kong box office during its theatrical run from 10 to 30 June 1993 in Hong Kong.

==See also==
*Aaron Kwok filmography
*Johnnie To filmography
*List of biker films

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 