Morya (film)
{{Infobox film
| name           = Morya - Marathi Movie
| image          = Morya Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Avdhoot Gupte
| producer       = Avdhoot Gupte Atul Kamble
| screenplay     = 
| story          = 
| starring       = Santosh Juvekar Chinmay Mandlekar Pari Telang Spruha Joshi Dilip Prabhawalkar Ganesh Yadav Pushkar Shrotri Sunil Ranade 
| music          = Avadhoot Gupte
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Morya is a Marathi movie and released on 19 August 2011.  Directed and produced by Avdhoot Gupte along with Atul Kamble. The movie revolves around the Ganeshotsav festival and amount of political influence involved in the celebration.

== Synopsis ==
In 1893 Lokmanya Balgangadhar Tilak gave a public form to the celebration of the festival of Ganpati which made it genuinely a peoples festival. He saw in the festival, a way of uniting people for a common cause with the aim to bring about political consciousness under the guise of a religious celebration, with freedom for India being the ultimate goal. 

In the metro city of Mumbai, there are two adjoining chawls, ‘GaneshChawl’ and ‘KhatavChawl’.  Being situated in prime locations they have been celebrating Ganeshotsav with splendour and devotion for the past 40 years. Being arch rivals that they are, each competes with the other every year and do their utmost, to prove they’re THE BEST. However, this year will be their last opportunity as the entire locality is being purchased by a builder for re-development, and the following year they would be only one big community during the Ganesh Festival. Each faction pulls out all stops to prove their supremacy. 

Huge Donations, Fabricated Stories,Marketing through Press & Media, Lavani Performances, Celebrity visits are done, until things take an ugly turn. The humble beginning of this sacred celebration takes a diverse path.

== Cast ==

The cast includes Santosh Juvekar, Chinmay Mandlekar, Pari Telang, Spruha Joshi, Dilip Prabhawalkar, Ganesh Yadav, Pushkar Shrotri, Sunil Ranade & others.

==Soundtrack==
The music is produced by Avadhoot Gupte and co-directed by Amit Sonmale.

===Track listing===
{{Track listing
| title1 = Morya Title Song | length1 =
| title2 = Aarti | length2 =
| title3 = Dev Chorala Slow Version | length3 =
| title4 = Dev Chorla | length4 =
| title5 = Email Kaal Internet Var Kela | length5 =
| title6 = Govinda Re | length6 =
| title7 = Hey Lambodar | length7 =
| title8 = Utsavatla Raja Utsav | length8 =
}}

== References ==
 
 

== External links ==
*  
*  
*  
*  

 
 