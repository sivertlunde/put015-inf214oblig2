Jeeva (2014 film)
{{Infobox film
| name           = Jeeva
| image          = 
| alt            =  
| caption        = 
| director       = Suseenthiran Vishal Arya Arya Suseenthiran R. Madhie Rajeevan S. Shanmugam Atlee Kumar
| writer         = Santhosh  (dialogues) 
| screenplay     = Suseenthiran Arun Balaji
| story          = Suseenthiran Vishnu Sri Divya
| music          = D. Imman
| cinematography = R. Madhi
| editing        = Anthony L. Ruben
| studio         = The Next BiG Film Vennila Kabadi Team The Show Vishal Film Factory
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| gross          = 
}} Tamil sports Vishnu and Sri Divya in the lead cast, while Lakshman Narayan plays a supporting role. Its music was composed by D. Imman, cinematography was handled by R. Madhi and editing was by Anthony L. Ruben, while dialogue was written by Santhosh. The film opened to positive reviews from critics in September 2014.

==Plot==
This film is about an young aspiring Cricketer Jeeva, who dreams of playing for the India national cricket team one day. The films begins with Jeeva, sitting in a park bench and starts to narrate his life story. He is a lower middle class boy,who from the very young age is interested in cricket. He sees Sachin Tendulkar as his idol. Though his father initially does not support him, he later starts to do so on the request of his friend. Jeeva grows up and becomes a part of the school team. He excels in his game and turns out to be a star player. Seeing his performances, a local cricket club offers him a chance to join and train with them,for which his father disagrees saying that his academic performance is poor due to cricket and he may not get a job in future. In between Jeeva falls in love with his neighbour girl and they are broken up when their family comes to know about this. Jeeva starts to drink due to this heartbreak. So to make him concentrate on good deeds again, Jeevas father agrees to let him join the cricket club.

Jeeva becomes a sensation at the club, scoring good runs in every game.His opening Partner Ranjith initially has ego clashes with Jeeva, but later they become close friends and produce great performances.They both develop and their team starts to enter higher division competitions.Then comes the tournament that selects the players for the Tamil Nadu Ranji Trophy team and as expected both Jeeva and Ranjith get selected. But the real trouble starts after joining the Team. The Tamil Nadu team mostly comprises players from a particular community and since Jeeva and Ranjith are from a different community, they are sidelined for most of the games.They are given chances in tough conditions where its hard to score more runs.But still they manage to put up a decent performance. Jeeva even once gets applauded by Irfan, the Captain of the Rajasthan team, who is a veteran national team member.But getting dropped in the upcoming games affects their average and their subsequent chances of getting into the team for future games. Angered by being rejected from the team, Ranjith storms into the Association office and blasts at the chairman for showing partiality towards a particular community and leaves with a heavy heart. Both Jeeva and Ranjith feel that their cricketing career is over. Ranjith commits suicide and Jeeva is heartbroken. In the meantime, Jeevas school time crush Jenny meets him back and they start loving again. Her Father even agrees for their marriage and promises a job for Jeeva, provided he converts to Christianity and quits Cricket. Jeeva initially agrees but immediately tells  Jenny that he cannot live without Cricket. Jeeva returns to training and he practices even harder waiting for a magic to happen that will revive his cricketing career.

And the magic happens after sometime, when the his Coach gets a call from the Rajasthan CPL(ipl) team franchise, to offer Jeeva a chance to play in the upcoming season of CPL. Jeeva comes to know that it was Irfan, the Rajasthan Team Captain who applauded him during the Ranji Trophy match, has suggested his name for the CPL. Jeeva is awestruck. He is extremely happy as his coach tells that if he performs well in the CPL, he can directly enter the National team. Jeeva Runs home with tears of joy.

The story comes back to the present, where Jeeva actually gives interview to TV channels, as a star player of the national cricket team. He narrates his experience on his debut CPL match, where he smashes the very first ball he faces for a Sixer, and he puts up a great performance time and again which seals his place in the national team. Jeeva ends his interview by saying that "in other countries, players lose by playing; But only in India, the players lose even without  getting a chance to participate".

==Cast==
  Vishnu as Jeeva
* Sri Divya as Jenny Soori as David
* Lakshman Narayan as Ranjith
* Sanyathara as Preethi Charlie as Arul Pragasam
* Madhusudhan Rao as Partha Sarathy
* T. Siva as Jennys father
* Marimuthu as Jeevas father
* Sanjay Bharathi as DCA Team cricketer
* Udhayabhanu Maheswaran as Raghavan Arya as the CPL cameraman (cameo appearance)
* Natarajan Subramaniam in a cameo appearance in the song "Oru Rosa" Surabhi in a cameo appearance in the song "Oruthi Mele"
 

==Production==
Soon after wrapping up production works of Rajapattai, Suseenthiran announced in October 2011 that he would shortly start his next venture featuring Vishnu (actor)|Vishnu. The film titled Veera Dheera Sooran was said to be an original story, to be made simultaneously in Tamil and Telugu.  In early 2012, it was reported that director Pandiraj would write the films dialogues.  However the director opted to concentrate on other projects before starting the film. He subsequently began a different project with Vishnu and production work on the film began again in August 2013.  Sri Divya was signed on to play a college girl in the film after the director was impressed with her performance in Varuthapadatha Valibar Sangam.  Surabhi (actress)|Surabhi, who debuted in Ivan Veramathiri was signed as another heroine.  She had to opt out later due to conflicting schedules.   Vinoth Kishan was initially signed to play a supporting role, but was later replaced by Lakshman Narayan, who had played the lead role in Bharathirajas Annakodi (2013). The title of the film was officially announced to be Jeeva in January 2014.

==Soundtrack==
{{Infobox album
| Name        = Jeeva
| Longtype    = to Jeeva
| Type        = Soundtrack
| Cover       = 
| Caption     = Cover art
| Genre       = Film soundtrack
| Artist      = D. Imman
| Producer    = D. Imman Tamil
| Sony Music
| Released    = 11 September 2014
| Recorded    = 2014
| Last album  = Oru Oorla Rendu Raja   (2014)
| This album  = Jeeva   (2014)
| Next album  = 
}}
The films score and soundtrack are composed by D. Imman under Sony Music Indias label. The film notably had songs lyrics written by Vairamuthu and his two sons Madhan Karky and Kabilan Vairamuthu, becoming the first such album to have the trio from the same family.  A launch event was held on 11 September 2014. 

; Tracklist
{{track listing
| extra_column  = Singer(s)
| all_lyrics    = 
| total_length  = 32:01

| title1     = Ovvundraai Thirudugiraai
| extra1     = Karthik (singer)|Karthik, Bhavya Pandit
| length1    = 04:45

| title2     = Oru Rosa
| extra2     = Anthony Daasan, Pooja AV
| length2    = 04:09

| title3     = Oruthi Maelae
| extra3     = Abhay Jodhpurkar, El Fé Choir
| length3    = 04:23

| title4     = Sangimangi
| extra4     = Nivas, Malavika & Tha Mystro
| length4    = 04:44

| title5     = Engae Ponaai
| extra5     = S. P. Balasubrahmanyam
| length5    = 03:53

| title6     = Ovvundraai Thirudugiraai (Karaoke)
| extra6     = 
| length6    = 04:45

| title7     = Oruthi Maelae (Karaoke)
| extra7     = 
| length7    = 04:21

| title8     = Netru Naan (Short)
| extra8     = D. Sathyaprakash
| length8    = 01:40
}}

==Release==
Actors Vishal Krishnas Vishal Film Factory and Arya (actor)|Aryas The Show People banner bought Tamil Nadu theatrical rights of the film in September 2014.  The satellite rights of the film were sold to STAR Vijay. The film was released on September 26, 2014

==Reception==
The film was released to positive reviews. The Times of India gave the film 3 stars out of 5 and wrote, "Suseenthiran pads up the film with the tropes that the genre needs. Though it has its heart in the right place the problem with Jeeva is that it is uneven. Cricket takes a backseat every time the director decides to focus on the love angle, taking some fizz out of the film".  The Hindu wrote, "A film has stayed true to its soul if it replays in your mind long after you’ve left the theatre. In most parts, Jeeva does that".  Indo-Asian News Service gave 3 stars out of 5 and wrote, "Jeeva is gripping with a moving second half, but not as inspiring as Nagesh Kukunoors Bollywood film Iqbal. Suseenthirans desperate act to portray his film as a commercial entertainer and not as a sports-drama doesnt go down too well. It somehow distracts you from the films core subject and thats a big letdown. If only these crucial comprises were handled with care, the film wouldve been highly satisfying".  Sify wrote, "Jeeva is sure to provide inspiration to many youngsters with similar cricket dreams and aspirations...it is a nice entertaining film, and another feather on the director’s cap".  Rated 3.25 out of 5 by Cinemalead-" Commendable cricketer!"  

==References==
 

==External links==
*  

 

 
 
 
 
 
 