Ding Dong Doggie
 
{{Infobox Hollywood cartoon|
| cartoon_name = Ding Dong Doggie
| series = Betty Boop
| image = 
| caption =
| director = Dave Fleischer
| story_artist =  Thomas Johnson 
| voice_actor = Mae Questel 
| musician = 
| producer = Max Fleischer 
| distributor = Paramount Pictures
| release_date = July 23, 1937
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}
Ding Dong Doggie is a 1937 Fleischer Studios animated short film starring Betty Boop.

==Synopsis==
Pudgy the Pup is impressed by a dalmatian fire dog he sees out his window. Against Betty Boops orders, Pudgy accompanies the fire dog to a fire at a general store. Pudgy tries to help out, but the fire takes on an (animated) life of its own and gets the better of Pudgy. In the end, Pudgy is happy to run back home to Betty Boop.
 

 
 
 
 
 


 