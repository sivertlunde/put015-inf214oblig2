Current (film)
{{Infobox film
| name           = Current
| image          = Current 2009 DVD.jpg
| alt            =  
| caption        = 
| director       = Surya Pratap
| producer       = 
| writer         = Srinivasa Rao Chinthalapudi
| screenplay     = Surya Pratap
| starring       = {{plainlist|
*Sushanth
*Sneha Ullal
*Raghu Babu
*Tanikella Bharani
*Brahmanandam
*Jhansi
*Lakshman Rao Kondavalasa
*Melkote
*Charan Raj
*Telangana Sakuntala
*Shafi
*Shakeela
*Sudha 
}}
| music          = Devi Sri Prasad
| cinematography = Vijaya Kumar C.	 	
| editing        = Martand K. Venkatesh	 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 2:17:05
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}
Current is a 2009 romantic comedy drama Telugu film directed by Surya Pratap. It stars Sushanth and Sneha Ullal in the Lead. Devi Sri Prasad provided the Music while Vijay Kumar C. Provided the Cinematography. Marthand K. Venkatesh handled the Editing Department. It was declared as a Super Hit at the Box Office.

==Plot==
Sushshu   is a happy-go-lucky college student whose philosophy of life is summed up as Forget Yesterday, Enjoy Today & Worry Not about Tomorrow which keeps him happy but worries his parents to no end. A transfer brings the entire family to Hyderabad where he flips for his college classmate Sneha on day one. A determined Sushanth woos her successfully and we are treated to a couple of songs in the bargain. Now, Snehas philosophy of life runs totally opposite of Sushanth and very soon reality sets in. His attitude ruins his marriage prospects with Sneha and they end up separating. The second half of the movie is all about how they try to fall out of love while making compromises to their philosophies. As expected, it ends well with both accepting that Love is above everything else in life!!! 

==Cast==
* Sushanth
* Sneha Ullal
* Raghu Babu
* Tanikella Bharani
* Brahmanandam
* Jhansi
* Shankar Melkote
* Charan Raj
* Telangana Shakuntala
* Shafi
* Sudha
* Kondavalasa Lakshmana Rao

==Soundtrack==
{{Infobox album
| Name = Current
| Caption = Album cover
| Type = Soundtrack
| Artist = Devi Sri Prasad
| Cover =
| Released =  
| Recorded = 2009 Feature film soundtrack
| Length =  Telugu
| Label = Aditya Music
| Producer = Devi Sri Prasad
| Last album = Kanthaswamy (film)|Kanthaswamy (2009)
| This album = Current (2009) Arya 2 (2009)
}}
The audio was Launched on 9 June 2009. The special guests for the Launch were Nagarjuna, Seenu Vytla and Sekhar Kammula along with Surya Pratap, Chintalapuri Srinivasa Rao, Devi Sri Prasad, Bhaskarabhatla Ravi Kumar and Ramajogaiah Sastri.  The Soundtrack was Received well.
{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = You Are My Love Story
| lyrics1 = Bhaskarabhatla
| extra1 = Devi Sri Prasad
| length1 = 
| title2 = Rekkalu
| lyrics2 = Bhaskarabhatla
| extra2 =  Sagar & Ranina Reddy
| length2 = 
| title3 = Ammayilu Abbayilu
| lyrics3 = Ramajogayya Sastry
| extra3 = Franco & Andrea Jeremiah
| length3 = 
| title4 = Current
| lyrics4 = Ramajogayya Sastry
| extra4 = Benny Dayal
| length4 = 
| title5 = Atu Nuvve Itu Nuvve
| lyrics5 = Ramajogayya Sastry
| extra5 = Neha Bhasin
| length5 = 
}}

==References==
 

==External links==
* 

 
 
 


 
 