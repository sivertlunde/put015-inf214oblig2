Born to Battle (1935 film)
{{Infobox film
| name           = Born to Battle
| image_size     = 
| image	=	Born to Battle FilmPoster.jpeg
| caption        = 
| director       = Harry S. Webb
| producer       = Harry S. Webb (associate producer) Bernard B. Ray (producer)  (uncredited) Oliver Drake (story) Rose Gordon (continuity) Carl Krusada (dialogue)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = J. Henry Kruse
| editing        = Frederick Bain
| distributor    = William Steiner
| released       = 1935
| runtime        = 63 minutes 55 minutes (American Alpha Video print)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 western produced by Bernard B. Ray and Harry S. Webb and directed by Webb for Reliable Pictures.

== Plot ==
"Cyclone" Tom Saunders, a free-spirited cowboy, is hired by a ranchers association to look into a series of cattle thefts, for which they suspect a pair of "nesters". When Saunders discovers that the nesters are an old man and his pretty young daughter and could not be the rustlers, he begins to suspect that Nate Lenox, a bullying ranch foreman, might have something to do with it.

== Cast ==
*Tom Tyler as "Cyclone" Tom Saunders
*Jean Carmen as Betty Powell
*Earl Dwire as George Powell
*Julian Rivero as Pablo Carranza
*Nelson McDowell as Lem "Blinky" Holt William Desmond as John Brownell Richard Alexander as Nate Lenox Charles King as Jim Larmer Ralph Lewis as Justice Hiram McClump
*Ben Corbett as Deputy

== External links ==
* 
* 

 
 
 
 
 
 
 


 