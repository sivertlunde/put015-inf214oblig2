1,778 Stories of Me and My Wife
{{Infobox film
| name           = 1,778 Stories of Me and My Wife
| image          = 1778 Stories of Me and My Wife movie poster.jpg
| image size     = 
| alt            = 
| caption        = Film poster advertising this film in Japan
| writer         = Ritsuko Hanzawa
| screenplay     = 
| story          = Taku Mayumura
| narrator       = 
| starring       = Tsuyoshi Kusanagi, Yuko Takeuchi
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| director       = Mamoru Hoshi
| producer       = 
| distributor    = Toho
| released       =  
| runtime        = 139 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

  is a 2011 Japanese film based on the true story of the science  fiction writer Taku Mayumura.    It was directed by Mamoru Hoshi, and stars actor Tsuyoshi Kusanagi and actress Yuko Takeuchi. 

1,778 Stories of Me and My Wife was released in Japanese cinemas on 15 January 2011.   

==Cast==
* Tsuyoshi Kusanagi as Sakutaro    
* Yuko Takeuchi as Setsuko  
* Ren Osugi as Setsukos doctor  
* Shosuke Tanihara as Takizawa  
* Michiko Kichise as Takizawas wife  
* Tai Kageyama (陰山泰)   as Nimi  
* Jun Fubuki as Setsukos mom  
* Yasuto Kosuda (小須田康人)   as a newspaper bill collector  
* Kazuyuki Asano (浅野和之)    
* Sumie Sasaki (佐々木すみ江)    

==References==
 

==External links==
*    
*  

 
 
 
 
 


 