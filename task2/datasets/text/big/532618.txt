Scary Movie 2
 
{{Infobox film
| name = Scary Movie 2
| image = ScaryMovie2.jpg
| alt = 
| caption = Theatrical release poster
| director = Keenen Ivory Wayans
| producer = Eric L. Gold Shawn Wayans Marlon Wayans Bob Weinstein Harvey Weinstein
| writer = Shawn Wayans Marlon Wayans Alyson Fouse Greg Grabianski Dave Polsky Michael Anthony Snowden Craig Wayans
| based on =   Chris Masterson Kathleen Robertson David Cross James Woods Tim Curry Tori Spelling Chris Elliott  Steven Bernstein Richard Pearson Peter Teschner
| studio = Dimension Films Brillstein Entertainment Partners#Brillstein-Grey Entertainment|Brillstein-Grey Entertainment Gold/Miller Productions Wayans Bros. Entertainment Brad Grey Pictures Miramax Films
| released =  
| runtime = 82 minutes  
| country = United States
| language = English
| budget = $45 million 
| gross = $141.2 million 
}}
Scary Movie 2 is a 2001 American parody film and the second film of the Scary Movie (film series)|Scary Movie franchise. Though part of the first Scary Movie s tagline read "...No sequel", this films tagline compensated by adding "We lied".
 thriller films, The Exorcist, The Haunting, The Amityville The Changeling, Hannibal (film)|Hannibal, Hollow Man, and The Legend of Hell House.
 Marlon and Shawn Wayans, and the last to be directed by Keenen Ivory Wayans.

Some of the original working titles were Scary Sequel and Scarier Movie.

==Plot== The Exorcist, Megan Jason Voorhees becomes possessed by the spirit of Hugh Kane, the previous owner of the House. Two priests, Father McFeely and Father Harris visit the house. After McFeely pays a trip to the toilet, they both attempt to drive Hughs ghost out, but the exorcism does not go as planned, resulting in a chain of projectile vomiting and various instances of pedophilia. Finally, McFeely responds to an insult towards his mother by shooting Megan.

One year later, Cindy Campbell, Brenda Meeks, Ray Wilkins, and Shorty Meeks are at college, trying to live new lives following the events of the previous film (although most of them actually died in the first film, Brenda claims in a deleted scene that her death was a near-death experience). Cindy and Brenda get tagged by a socially maladjusted girl, Alex. Ray, still confused about his sexuality, has two new male friends, Tommy and Buddy.
 haunted mansion called Hell House (the same house involved in the opening scene) using the clueless teens. Meanwhile, Buddys advances are spurned by Cindy, who is recovering from her previous relationship. When Cindy is the first to arrive at Hell House, she encounters a vulgar parrot, and the caretaker, Hanson, who has a badly malformed hand. Later that evening, the group, including sexy newcomer Theo, sit down for dinner. Unfortunately, everybody loses their appetite due to Hansons repulsive antics.
 joint and munchies and lets him escape. 
 uses a collection of random objects in the room to somehow produce a Caterpillar tractor, which she drives through the wall.
 bra and underwear. Eventually, Dwight and the teens regroup, and agree to use Cindy as bait to lure Kane into a device that will destroy him. Just as they activate the device, they realize Cindy is still standing on it. Ray springs to action and saves her. The plan succeeds, freeing the group from the houses curse.

Two months later, Cindy and Buddy are in a relationship and go out for a walk. However, Buddy disappears without notice when Cindy discovers Hanson at the hot dog stand. As Cindy backs away in fear, Hanson pursues her, and, in the midst of screaming, gets struck by a red car driven by Shorty similar to the scene in the first film.

==Cast==
 
* Anna Faris as Cindy Campbell
* Regina Hall as Brenda Meeks
* Shawn Wayans as Ray Wilkins
* Marlon Wayans as Shorty Meeks Chris Masterson as Buddy
* Kathleen Robertson as Theo
* David Cross as Dwight Hartman
* James Woods as Father McFeely
* Tim Curry as Professor Oldman
* Tori Spelling as Alex 
* Chris Elliott as Hanson
* Andy Richter as Father Harris
* Richard Moll as Hugh Kane (Hell House Ghost)
* Veronica Cartwright as Mrs. Voorhees
* Natasha Lyonne as Megan Voorhees
* James DeBello as Tommy Beetlejuice as Shortys brain Matt Friedman (voice) as Polly the Parrot  Vitamin C (voice) as herself
 

==Soundtrack== Vitamin C interrupts her to tell her to stop. Later in the film, "Let Me Blow Ya Mind" plays on a radio as Theo attempts to seduce Dwight and "Smack My Bitch Up" is heard during Cindy, Brenda and Theos fight against the possessed Hanson. "Ride wit Me" is the first song featured during the end credits, followed by "So Erotic" and "When Its Dark".
 Hello Dolly" – Jerry Herman
# "Shake Ya Ass" – Mystikal
# "Tubular Bells" – Mike Oldfield
# "Smack My Bitch Up" – The Prodigy Vitamin C
# "U Know Whats Up" – Donell Jones
# "So Erotic" – Casey Crown featuring J Dee
# "Ride wit Me" – Nelly featuring City Spud
# "Insane in the Brain" – Cypress Hill
# "Evel Knieval" – Deadly Avenger

Other songs in the film: Oleander
* "Fever (Little Willie John song)|Fever" – Richard Marino and His Orchestra
* "Killer Bee" – Meeks Eve featuring Gwen Stefani
* "Skullsplitter" – Hednoize
* "If I Had No Loot" – Tony! Toni! Toné! History Repeating" – Propellerheads featuring Shirley Bassey
* "When Its Dark" – Trace featuring Neb Luv
* "Givin My Dick Away" – Trace
* "Sorry Now" – Sugar Ray

==Parodies== mystery genres. 
 
* The films central parody is The Legend of Hell House. The Exorcist. 
* The walking scene on Campus with Cindy & Brenda references to the 90s cult classic "Clueless (film)|Clueless". Thir13en Ghosts.
* Hollow Man – The equipment which the group employs to fight an invisible enemy (thermal goggles, smoke, and so on) and use of a defibrilator to escape from a freezer room. House on Haunted Hill – The labyrinthian basement, weapons with limited ammo, and Professor Oldman being lured to his death.
* Charmed – Hugh Kanes vanquishing is the same as many of the vanquishings the sisters do on the show.
* Brendas reaction to the walking skeleton could be taken as commentary on the films use of imagery no longer considered scary today.
* In the scene in which Hanson removes the top of Shortys head and he then said "Hello Cindy", is similar to a scene in Hannibal (film)|Hannibal.
* The scene where Ray and his friend read their back tattoos to each other repeatedly references a scene of Dude, Wheres My Car?.
* What Lies Beneath is parodied in a scene where Cindy seduces the professor in the kitchen, and then Ray suddenly appears in the same dress.
* The scene in which a clown hides under Rays bed and then pulls him underneath, the marijuana plant growing large and coming to life and Alex being dragged across the walls of the bedroom are all parodies of scenes from Poltergeist (1982 film)|Poltergeist.
* In the sequence where an invisible ghostly presence penetrates Alex and then has sex with her, heavily references Dracula (1992 film)|Dracula and The Entity
*   – Hanson sings "God is in his holy temple" and the use of Kane as the name of the evil spirit. Stephen Kings film adaptation of It (novel)|It.
* The Rocky Horror Picture Show – Hanson is a parody of the character, Riff Raff. Tim Curry played the role of Dr. Frank-N-Furter in The Rocky Horror Picture Show.
* Cindy, Brenda and Theo fighting Hanson in the style of Charlies Angels (film)|Charlies Angels. Furthermore, Tori Spellings character is named after Lucy Lius character (Alex Munday) from the same film, in which also Tim Curry co-starred.
* Save the Last Dance – Shorty teaches Cindy how to be "black".
*   – The wheelchair duel between Dwight and Kane parodies the motorcycle sequence between Ethan and Sean.
* MacGyver – Cindy uses everyday items to build a mini-bulldozer and escape the refrigerator.
* Paulie  – A foul-mouthed Blue-crowned Parakeet|blue-crowned parrot named Polly appears throughout the movie . The Amityville Horror – Reverend McFeely tries to bless the house and ends up with flies all over him, only for the scene to change and reveal he is actually sitting and straining into a toilet, resulting in explosive diarrhea. The two red eyes in the dark looking through the window from the movie also appear in the scene.
* Buddy hands Cindy a book titled "Harry PotHead", a clear reference to Harry Potter.  The Changeling Nike commercial. 
* Dawn of the Dead – Ray has on a suit similar to Peters SWAT team suit.
* The dialogue between Cindy and Buddy in the freezer, when she is giving him a handjob has been taken from Titanic (1997 film)|Titanic.
* The use of the quote "do you feel lucky, punk?" from Dwight is a parody of Dirty Harry.
* Twister (1996 film)|Twister – When Cindy is fighting Hanson and creates the tornado, which has various objects and even a cow blowing around inside it.
* Scanners – Hansons head explodes.
* Rocky – Cindys fight with the cat (editing style, blows, unseen cameras flashing and the cats triumphant raising of its "fists").
* Weakest Link – When the parrot says "You are the weakest link. Goodbye" after Alex gets knocked out by the chandelier.
* The skeleton chasing Cindy sequence is based on Wishmaster (film)|Wishmaster. Urban Legend because the opening featured a woman singing along to the radio badly.
* The scene where Dwight and Hanson are trading insults about each others disability is based on the scene from Wild Wild West where Jim West and Arliss Loveless trade insults on Wests race and Lovelesss disability.
* The scene when Cindy is fighting with the cat and the name of the house is from the film The Legend of Hell House.
* Ray carrying the paraplegic Dwight on his back is reminiscent of Chewbacca attaching the destroyed C-3PO to a harness on his back in The Empire Strikes Back.
* Cindy having flashbacks while reading the diary and learning about the dead wife is a parody of the 1988 film Night Screams.
 
* Charlies Angels -  where Cindy, Brenda and Theo fighting against Hanson, just like Natalie(Cameron Diaz), Dylan(Drew Barrymore) & Alex(Lucy Liu) against the thin man(Crispin Glover)

==Reception==
===Box office=== the fifth film was released twelve years later.    

===Critical response===
Despite its box office success, the film received mostly negative reviews. Achieving a rotten 15% rating on Rotten Tomatoes  and a score of 29% on Metacritic. Metacritic. 

==See also==
* List of ghost films

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 