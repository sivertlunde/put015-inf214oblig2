The Groomsmen
{{Infobox film
| name = The Groomsmen
| image = The Groomsmen.jpg
| caption =
| director = Edward Burns
| producer = Edward Burns Philippe Martinez Aaron Lubin Margot Bridger
| writer   = Edward Burns
| starring = Edward Burns John Leguizamo Matthew Lillard Donal Logue Jay Mohr Brittany Murphy Jessica Capshaw Spencer Fox
| music    = Robert Gary  Howard Drossin P.T. Walkley  
| cinematography = William Rexer II
| editing  = Jamie Kirkpatrick
| distributor =  Bauer Martinez Entertainment
| released =  
| runtime  = 93 minutes
| language = English
| budget   = $3 million 
}}
The Groomsmen is a 2006 comedy film written and directed by Edward Burns.  It opened in New York City and Los Angeles on July 14, 2006.  Filming took place at many locations on City Island, New York.

==Plot==
A groom and his four groomsmen wrestle with issues such as fatherhood, homosexuality, honesty and growing up in the week leading up to his wedding.

Paulie (Burns), a self-supporting writer, is making plans for his marriage to Sue (Murphy), his girlfriend who is in her 5th month of pregnancy.  In real life, Burns then-girlfriend, supermodel Christy Turlington, was also five months pregnant when they married in June 2003.  Christy inspired Burns to rework the manuscript for this movie, which he hadnt worked on in many months.  

Paulie is strongly advised by his older brother Jimbo (Logue) to not go through with the wedding. Jimbo, who runs a struggling business, is envious of Paulie, partly because his own childless marriage is unraveling.

T.C. (Leguizamo), who left the neighborhood without explanation eight years earlier, returns for the wedding. Apparently, before leaving, T.C. had stolen a Tom Seaver baseball card from Paulies cousin Mike (Mohr). Mike still harbors such resentment over the loss that he immediately starts a fight with T.C. Later, T.C. hesitantly reveals that he abruptly left the neighborhood because hes gay and that he stole Mikes card because, even though they were best friends, he hated him for his constant verbal gay bashing.

The neighborhood bar is owned by Dez (Lillard), who is married with two children and is the most content and functional member of the gang. He is continually trying to "get the band back together". He has even pushed his own sons into learning the guitar and is seen riding them to become better.

Jessica Capshaw has a small role in this film.  Her stepfather, Steven Spielberg, directed Edward Burns in 
Saving Private Ryan.

==Cast==
* Edward Burns as Paulie
* Jessica Capshaw as Jen
* Spencer Fox as Jack
* John Leguizamo as T.C.
* Matthew Lillard as Dez
* Donal Logue as Jimbo
* Jay Mohr as Mike (Cousin of Paulie & Jimbo)
* Brittany Murphy as Sue
* Heather Burns as Jules
* John F. ODonohue as Pops (Uncle of Paulie & Jimbo)
* Joseph OKeefe as Roman Catholic Priest
* Joe Pistone as Topcat
* Kevin Kash as Strip Club MC
* Amy Leonard as Crystal
* Arthur J. Nascarella as Mr. B
* Shari Albert as Tina
* John Mahoney as Father of Paulie & Jimbo (cut from film- seen in bonus footage only)

==Box office==
The film garnered mixed reviews and only managed a limited release of 26 theaters in the US.  It grossed less than one million dollars foreign and domestic.

==Reception==
The Groomsmen received mixed reviews, earning a Rotten Tomatoes approval rating of 52%.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 