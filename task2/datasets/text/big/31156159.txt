Vithukal
{{Infobox film 
| name           = Vithukal
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = P Bhaskaran
| writer         = MT Vasudevan Nair
| screenplay     = MT Vasudevan Nair Madhu Sheela Sukumari Kaviyoor Ponnamma
| music          = Pukazhenthi
| cinematography = SJ Thomas
| editing        = K Sankunni
| studio         = Aradhana Movies
| distributor    = Aradhana Movies
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by P. Bhaskaran and produced by P Bhaskaran. The film stars Madhu (actor)|Madhu, Sheela, Sukumari and Kaviyoor Ponnamma in lead roles. The film had musical score by Pukazhenthi.   

==Cast==
  Madhu as Unnikrishnan
*Sheela as Sarojini
*Sukumari as Sarada
*Kaviyoor Ponnamma as Ammini
*Adoor Bhasi as Eravan Nair
*Sankaradi as Achuthan Nair
*Kannan
*Adoor Bhavani as Amma
*Baby Indira
*Balakrishna Menon
*K. P. Ummer as Chandran
*N. Govindankutty as Raghavan
*Nambiar
*Raghava Menon
*Vanchiyoor Radha as Madhavi
*Renuka
*P. R. Menon
 

==Soundtrack==
The music was composed by Pukazhenthi and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Apaarasundara Neelaakaasham || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Gopuramukalil || S Janaki || P. Bhaskaran || 
|-
| 3 || Ingu Sookshikkunnu || K. J. Yesudas || P. Bhaskaran || 
|-
| 4 || Maranadevanoru || K. J. Yesudas || P. Bhaskaran || 
|-
| 5 || Yaathrayaakkunnu Sakhi Ninne || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 