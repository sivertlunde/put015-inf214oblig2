The Earth Dies Screaming
 
 
{{Infobox Film
| name           = The Earth Dies Screaming
| image          = "The_Earth_Dies_Screaming"_(1965).jpg
| image_size     = 
| caption        = 
| director       = Terence Fisher
| producer       = {{plainlist|
* Robert L. Lippert
* Jack Parsons
}}
| writer         = Harry Spalding (as Henry Cross)
| narrator       = 
| starring       = {{plainlist|
* Willard Parker
* Virginia Field
* Dennis Price
}}
| music          = Elisabeth Lutyens
| cinematography = Arthur Lavis
| editing        = Robert Winter
| studio         = Lippert Films
| distributor    = Twentieth Century Fox 
| released       =  
| runtime        = 62 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
The Earth Dies Screaming is a 1965 British science fiction film directed by Terence Fisher, and starring Willard Parker, Virginia Field, Dennis Price, Vanda Godsell,  Thorley Walters, David Spenser, and Anna Palk. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 129-132 

==Plot summary==
After a mysterious gas attack which kills off most of the Earths population, a few survivors gather at a country inn to figure out a plan for survival. However, the gas attack is only the first step in an alien invasion, in which groups of killer robots stalk the streets, able to kill anyone with the a mere touch of their hands. The groups members find additional weaponry in a nearby drill hall, but the robots continue their campaign of terror, which only increases when their victims rise from the dead as zombies, eager to kill anyone who might try to stop them. Yet despite frictions within the group -- and the birth of a baby, which further complicates matters -- most of the members survive, and head to a nearby airport, where they commandeer a plane and fly towards an unknown destination, where perhaps additional survivors await their arrival.

==Production== Location filming scored by the avant-garde Elisabeth Lutyens, whose father, Edwin Lutyens, designed Manor House Lodge in Shere, a small property which features prominently at several points in the film. 

==Reviews==
Wheeler Winston Dixon wrote about the films use of silence:

 

Writing in The Zombie Movie Encyclopedia, academic Peter Dendle cited the film as "an obvious precursor to Night of the Living Dead. 

==In popular culture==
  The Wrecker Village of the Damned to portray a train wreck and plane crashing.

The Earth Dies Screaming was used in 1983 as the inspiration (and title) for an obscure Atari 2600 video game.  The game is set in space, and has you shooting down satellites and fighter ships. 

UB40 had a Top 10 hit in the UK in 1980 with a song with the same name as the film, but the songs subject matter was a post-nuclear holocaust.

Tom Waits has a song called "Earth Died Screaming" on his 1992 album Bone Machine. The song and name were inspired by the title of the film, despite Waits never actually seeing it.

Earth Dies Screaming is the name of an experimental band in Austin, Texas.  Contrary to popular belief, the band actually takes their name from the 1983 Atari video game. 

The title is referenced by a Dalek in the Doctor Who episode "Victory of the Daleks" who says "The Earth Will Die Screaming".

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 