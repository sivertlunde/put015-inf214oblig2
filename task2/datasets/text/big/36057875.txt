Moovendhar
{{Infobox film
| name           = Moovendhar
| image          = 
| image_size     = 
| caption        =  Suraj
| producer       = N. Vishnuram
| screenplay     = Suraj
| story          = K. Selva Bharathy
| starring       =  
| music          = Sirpy
| cinematography = RM. Ramanath Shetty
| editing        = B. S. Vasu Saleem
| distributor    = 
| studio         = Ganga Gowri Production
| released       = 12 January 1998
| runtime        = 150 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1998 Tamil Tamil comedy Sarath Kumar Devayani in lead roles. The film, produced by N. Vishnuram, had musical score by Sirpy and was released on 12 January 1998 to negative reviews.     

==Plot==
 Sarath Kumar), his father Poochi (S. V. Ramadas)  and his grandfather Nagappan (M. N. Nambiar) are short-tempered persons who beat the villagers for a simple quarrel. Only, Manimarans mother, Sivagami (Lakshmi (actress)|Lakshmi) can control them. Uma (Rajeswari), Manimarans sister, gets married with a man of another village. Manimaran falls in love with Vaidehi (Devayani (actress)|Devayani), a Brahmin girl, and gets married with her without her permission. In a fight against Manimaran, the antagonist becomes a blind in one eye and decides to take revenge on Manimaran. When Manimaran drives his pregnant sister, the antagonist stops him but Manimaran beats the antagonist and his henchmen. Uma comes too late at the hospital and dies with her baby. Vaidehi leaves their house because of her husbands behavior who brings Umas death. Manimaran changes his behavior and tries to convince Vaidehi to come back home.

==Cast==
 Sarath Kumar as Manimaran Devayani as Vaidehi
*Rajeswari as Uma Lakshmi as Sivagami
*Manivannan as Velu Nayakan
*Anandaraj as
*M. N. Nambiar as Nagappan
*S. V. Ramadas as Poochi
*Delhi Ganesh as Vaidehis father
*Anu Mohan as Dharmidasan Monica as Umas sister Manorama in a guest appearance

==Soundtrack==

{{Infobox album |  
| Name         = Moovendhar
|  Type        = soundtrack
|  Artist      = Sirpy
|  Cover       = 
|  Released    = 1998
|  Recorded    = 1997 Feature film soundtrack
|  Length      = 
|  Label       = Five Star Audio
|  Producer    = Sirpy
|  Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sirpy. The soundtrack, released in 1998, features 7 tracks with lyrics written by Arivumathi, Pazhani Bharathi and Thavasimani.   
 Hariharan
* Chera Nenna - Mano, Sujatha
* Kumudam Pol - Hariharan
* Nenja Thirandhu - Malaysia Vasudevan
* Singakutty - Mano
* Sokku Sundari - Krishnaraj

==Reception==
Sureshbabu of indolink.com gave 1.5 out to 5 and described the film as "not worth it". 

==References==
 

 

 
 
 
 