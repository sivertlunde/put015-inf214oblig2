Anyarude Bhoomi
{{Infobox film 
| name           = Anyarude Bhoomi
| image          =
| caption        =
| director       = Nilambur Balan
| producer       = Unmachithra
| writer         = UA Khader
| screenplay     = UA Khader
| starring       = Chowalloor Krishnankutty Nilambur Balan Amina Kozhikode Sharada
| music          = A. T. Ummer
| cinematography = Ashok Chowdhary
| editing        =
| studio         = Unmachithra
| distributor    = Unmachithra
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by Nilambur Balan and produced by Unmachithra. The film stars Chowalloor Krishnankutty, Nilambur Balan, Amina and Kozhikode Sharada in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Chowalloor Krishnankutty
*Nilambur Balan
*Amina
*Kozhikode Sharada
*Kunjandi
*Lalithasree
*Mamukkoya
*Master Anwar
*Nilambur Ayisha
*Zeenath
*Vijayalakshmi
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kodi chenthaamarappoo || Peeru Muhammed || Bichu Thirumala || 
|-
| 2 || Manushyamanassaakshikal || Bichu Thirumala || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 