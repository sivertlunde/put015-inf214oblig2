The Life Line
{{Infobox film
| name           = The Life Line
| image          = The Life Line 1919 newspaper ad.jpg
| image_size     =180px
| caption        = Newspaper advertisement.
| director       = Maurice Tourneur
| producer       = Maurice Tourneur Charles E. Whittaker Jack Holt   Wallace Beery   Lew Cody   Tully Marshall
| music          =
| cinematography = 
| editing        = 
| studio         = Maurice Tourneur Productions
| distributor    = Famous Players-Lasky Corporation
| released       = 1919
| runtime        = 60 minutes
| country        = United States
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Jack Holt, Wallace Beery and Lew Cody. The picture was based on the play The Romany Rye by the British playwright George R. Sims. The film is set amongst the criminal classes in the slums of London.

==Cast== Jack Holt as Jack Hearne, the Romany Rye
* Wallace Beery as Bos
* Lew Cody as Phillip Royston
* Tully Marshall as Joe Heckett
* Seena Owen as Laura
* Pauline Starke as Ruth Heckett

==Bibliography==
* Waldman, Harry. Maurice Tourneur: The Life and Films. McFarland, 2008.

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 