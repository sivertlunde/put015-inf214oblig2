Kadalpalam
thumbnail
{{Infobox film
| name           = Kadalpalam
| image          = 
| image size     =
| caption        =
| director       = K. S. Sethumadhavan
| producer       = M. O. Joseph
| writer         = K. T. Mohammed
| narrator       = Sathyan Jayabharathi Sheela
| music          = Devarajan
| cinematography = Melli Irani
| editing        = M. S. Mani
| studio         = Manjilas Films
| distributor    = 
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Kadalpalam is a 1969 Malayalam language film directed by K. S. Sethumadhavan. Sathyaneshan Nadar|Sathyan, Prem Nazir, Jayabharathi, Sheela, Bahadoor and Adoor Bhasi star in the lead.  The film is an adaptation of the very popular professional stage play of the same name authored by the renowned dramatist K. T. Mohammed which is an adaptation of 1940 Hindi film Prem Nagar. The film was critically well received and was a box office hit. It is probably best remembered for the performance by Sathyan, who received the first ever State Award for his performance. 

==Plot==
The story revolves round the family of Narayana Kaimal (Sathyaneshan Nadar|Sathyan), a leading advocate who loses his eyesight. Kaimal is a strict disciplinarian and always tries to impose his thoughts and principles on his children and fellow beings. This creates a conflict with others. Kaimals elder son Raghu (Again Sathyan) dislikes the obstinate nature of his father leading to constant conflicts between the two. Kaimals younger son Prabhakaran (K. P. Ummer) who is an advocate and daughter Geetha (Jayabharathi) try to obey their father, but at times they only pretend to obey him. Kaimal now sees the world through the eyes of his faithful servant Appu (Bahadur).

Prabhakaran is in love with Sarala (Sheela), daughter of Kaimals servant Sreedharan Pillai (Sankaradi). Geetha is in love with Murali (Prem Nazir), the adopted son of Kadeeja (Adoor Bhavani), who lives in the neighbourhood. Appu is warned against telling anything about the love affairs to Kaimal. Raghu believes that they lost their mother at very young age only because of the arrogance of their father. Raghu is married to Prashanthi (Vijayachandrika) who was his fathers choice.

Raghu shows his protest by his rude behaviour towards his wife. Kaimal demands to give back the tile factory which he had given to Raghu. But Raghu refuses to part with the factory. Kaimal files a court case against his son.

Geetha marries Murali and Raghu supports them. Kaimal regains his eyesight and is thunderstruck to know about the happenings that took place while he was blind. He decides to pretend to be blind so as to know more about his children. Only Appu is aware that Kaimal is now not blind. Kaimal plots to stop the marriage of Prabhakaran. He offers partnership of the factory to the manager Nanukuttan Nair (Adoor Bhasi) if he could help conduct the marriage of his son, who is a police inspector, with Sarala. Nair agrees to carry out the crooked plan.

The verdict of the court goes against Kaimal. But Prabhakaran hides this from his father. Prabhakaran does not allow Geetha to meet Kaimal and when Kadeeja come and raises voice for justice, Kaimal hits her. Murali attacks Kaimal and now all come to know that Kaimal is not blind. Prabhakaran comes to know about the crooked plan of his father. All the children turn against Kaimal. Unable to stand the setback and the thought that he had failed in his attempts to control his children with an iron hand he suffers a heart attack and dies.

==Cast==
  Sathyan as Narayana Kaimal Sathyan as Raghu
*Jayabharathi as Geetha
*K. P. Ummer as Prabhakaran
*Prem Nazir as Murali
*Sheela as Sarala
*Adoor Bhasi as Nanukkuttan Nair
*Sankaradi as Sreedharan Pillai
*Adoor Bhavani as Khadeeja
*Bahadoor as Appu
*Kuttyedathi Vilasini
*N. Govindankutty
*Thodupuzha Radhakrishnan
*Vijayachandrika as Prasanthi
 

==Songs==
The film had four songs penned by Vayalar Ramavarma and set to tune by Devarajan. S. P. Balasubrahmanyam debuted in Malayalam with this film. It was R. K. Shekhar who recommended S.P.B. to Devarajan. Coincidentally, S.P.B.s most of the famous songs came when he associated with Shekhars son A. R. Rahman.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Kadalum Marukadalum || SP Balasubrahmanyam || Vayalar Ramavarma ||
|-
| 2 || Inne Pol || K. J. Yesudas, B Vasantha || Vayalar Ramavarma ||
|-
| 3 || Kasthoorithailamittu || P. Madhuri, Chorus || Vayalar Ramavarma ||
|-
| 4 || Ujjayiniyile || P. Leela || Vayalar Ramavarma ||
|}

==Awards==
; 1st Kerala State Film Awards 
* Best Dialogues: K. T. Mohammed
* Best Actor: Sathyan
* Best Music Director: Devarajan
* Best Lyrics: Vayalar Ramavarma
* Best Singer: P. Leela

==References==
 

 
 
 