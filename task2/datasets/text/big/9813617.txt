Shiloh 2: Shiloh Season
{{Infobox Film name = Shiloh 2: Shiloh Season image = Shiloh 2.jpg caption = DVD cover
| director = Sandy Tung
| studio = Legacy Releasing
| distributor = Warner Bros.
| released =   
| runtime = 96 minutes
| country = United States 
| language = English
| gross = $58,946
}}
Shiloh Season is a 1999 family/drama film about a boy trying to defend his dog from a man who is constantly under the influence. It is a sequel to the 1996 Shiloh (film)|Shiloh.

==Plot==
Judd is upset that Shiloh (his former hunting dog) now belongs to Marty. In the first film, Marty did hard labor to get Shiloh from Judd. Judd starts drinking heavily and almost runs Marty and Shiloh off the road with his truck. Marty encounters Judd several times, including one key moment when Judd thinks he heard Marty on his property, threatens him and holds his gun and goes to find him but he is too drunk. Later, Judd crashes his truck into the creek while driving drunk. Shilohs loud barking gets Martys attention, and he finds Judd. Judd is taken to the hospital and returns home sometime later. Marty wants to be nice with Judd and makes donations (bread and meat). Marty then starts writing Judd letters, telling him stories about Shiloh. Judd reads them and starts to bond with Marty. In the end, Marty decides he wants to take Shiloh with him to visit Judd. Marty and Judd start to become friends after Judd tells him that it was Shiloh who really saved him and for the first time ever, Shiloh allows Judd to pet him. Judd starts to gets close with his dogs and he lets them come inside when it starts to rain at the end.

== Cast ==
* Michael Moriarty as Ray Preston Scott Wilson as Judd Travers
* Zachary Browne as Marty Preston
* Ann Dowd as Louise Preston
* Caitlin Wachs as Dara Lynn Preston
* Rachel David as Becky Preston
* Rod Steiger as Doc Wallace
* Bonnie Bartlett as Mrs. Wallace
* Marissa Leigh as Samantha Wallace
* Joe Pichler as David Howard
* Frannie as Shiloh
* John Short as Mr. Howard

==Reception==
Review aggregation website Rotten Tomatoes reports that 68% of critics have given the film a positive review based on 19 reviews, with an average score of 6.7/10.

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 


 