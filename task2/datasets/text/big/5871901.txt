Creator (film)
{{Infobox Film
| name           = Creator
| image          = Creatorposter.jpg
| caption        = Theatrical release poster
| director       = Ivan Passer Stephen J. Friedman
| writer         = Jeremy Leven
| starring = {{Plainlist|
* Peter OToole
* Mariel Hemingway
* Vincent Spano
* Virginia Madsen
* David Ogden Stiers
}}
| music          = Sylvester Levay
| cinematography = Robbie Greenberg Bill Phillips (supervising editor), Jimmy Ling, Hal Sanders (editors), Michael A. Warner (assistant editor)
| distributor    = Universal Pictures
| released       = September 20, 1985
| runtime        = 107 min.
| country        = United States English
| budget         = 
| gross          =  $5,349,607 
| 
}}
Creator is a 1985 film directed by Ivan Passer, starring Peter OToole, Vincent Spano, Mariel Hemingway, and Virginia Madsen. It is based on a book of the same title by Jeremy Leven.

==Plot==

 

==Cast==
* Peter OToole: Dr. Harry Wolper
* Mariel Hemingway: Meli
* Vincent Spano: Boris Lafkin
* Virginia Madsen: Barbara Spencer
* David Ogden Stiers: Dr. Sid Kuhlenbeck
* John Dehner: Paul
* Kenneth Tigar: Pavlo
* Rance Howard: Mr. Spencer
* Ellen Geer: Mrs. Spencer
* Eve McVeagh: Woman with monkey (final screen appearance)
* Karen Kopins: Lucy William Bassett : Dr. Sutter

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 