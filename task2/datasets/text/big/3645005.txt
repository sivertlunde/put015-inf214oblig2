Revenge of the Pink Panther
{{multiple issues|
 
 
}}
{{Infobox film
| name = Revenge of the Pink Panther
| image = Revenge_of_the_pink_panther_ver3.jpg
| image_size =
| caption = Theatrical release poster
| director = Blake Edwards
| producer = Blake Edwards Ron Clark Blake Edwards
| story = Blake Edwards
| starring = Peter Sellers Dyan Cannon Robert Webber Tony Beckley Paul Stewart  And Herbert Lom| music = Henry Mancini Leslie Bricusse (songwriter)
| cinematography = Ernest Day Alan Jones
| studio = Sellers-Edwards Productions Jewel Productions Limited
| distributor = United Artists
| released =  
| runtime = 104 minutes
| country = United Kingdom United States
| language = English
| budget =
| gross = $49.5 million 
}}
Revenge of the Pink Panther is the sixth film in The Pink Panther film series. Released in 1978, it was the last entry released during the lifetime of Peter Sellers, who died in 1980. It is also the last entry to be distributed solely by United Artists, which merged with Metro-Goldwyn-Mayer in 1981. The opening credits were animated by DePatie-Freleng Enterprises.

== Plot ==
Philippe Douvier (Robert Webber), a major businessman and secretly the head of the French Connection, is suspected by his New York Mafia drug trading partners of weak leadership and improperly conducting his criminal affairs. To demonstrate otherwise, Douviers aide Guy Algo (Tony Beckley) suggests a show of force with the murder of Chief Inspector Jacques Clouseau (Peter Sellers). Unfortunately for Douvier, his first attempt at bombing him fails; and the subsequent attempt by Chinese martial artist Mr. Chong (an uncredited appearance by the founder of American Kenpo, Ed Parker) is thwarted when Clouseau successfully fights him off, believing him to be his (Clouseaus) valet Cato (Burt Kwouk) who has orders to keep his employer alert with random attacks. That night, Douvier poses as an informant to lure Clouseau into a trap, but the Chief Inspectors car and clothes are stolen by transvestite criminal Claude Russo (Sue Lloyd), who is killed by Douviers men. Subsequently Douvier and the French public believe Clouseau is dead and, as a result of this assumption, Clouseaus former boss, ex-Commissioner Charles Dreyfus (Herbert Lom), is ordered released from the lunatic asylum to perform the investigation.

In Russos clothes and insisting on his true identity, Clouseau is taken to the asylum himself (featuring a cameo by Andrew Sachs, whose character apparently believes himself to be Hercule Poirot), but escapes into Dreyfus room, who faints from the shock of seeing Clouseau alive. Clouseau manages to disguise himself as Dreyfus and is taken home by operative François (André Maranne). At home, Clouseau finds Cato, who, despite having turned Clouseaus apartment into a Chinese-themed brothel, is relieved to see he survived and the two plan revenge on the sponsor of Clouseaus assassination. Meanwhile, Dreyfus is assigned to read an elegy at Clouseaus funeral by the police chiefs wife, on pain of his own discharge. During the recital, Dreyfus cannot control his laughter at the praise of Clouseau, but disguises his reaction by pretending to shed tears. When Clouseau surreptitiously reveals himself at the funeral, however, Dreyfus faints into the burial pit while the master of ceremonies completes the oration.
 Paul Stewart) in Hong Kong, but again faints when he beholds Clouseau.

Clouseau, Cato, and Simone follow Douvier to Hong Kong in disguise, unaware that Dreyfus has also arrived. There, Clouseau impersonates Scallini, while Simone distracts the real Scallini; but the plan goes awry when one of Scallinis men spots Douvier leaving their hotel with a stranger and Clouseau exposes his own disguise. A car chase begins, ending in a crash at the Hong Kong docks, where Dreyfus recognizes Clouseau and pursues him into a firework warehouse. Inside, Dreyfus mistakenly sets the fireworks off and the resulting explosions sow chaos among all the participants, which eventually leads to the arrests of Douvier and Scallini. Clouseau is awarded for their arrest by the President of France, and he and Simone spend an evening together.

== Cast ==
* Peter Sellers as Inspector Jacques Clouseau
* Herbert Lom as Chief Insp. Charles Dreyfus
* Dyan Cannon as Simone Legree
* Robert Webber as Philippe Douvier Paul Stewart as Julio Scallini
* Burt Kwouk as Cato Fong
* Tony Beckley as Guy Algo
* Robert Loggia as Al Marchione
* André Maranne as Sgt. François Chevalier
* Graham Stark as Prof. Auguste Balls
* Alfie Bass as Fernet
* Sue Lloyd as Claude Russo
* Danny Schiller as Cunny
* Douglas Wilmer as Police Commissioner
* Ferdy Mayne as Dr. Paul Laprone
* Valerie Leon as Tanya
* Ed Parker as Mr. Chong
* Adrienne Corri as Therese Douvier
* Henry McGee as Officer Bardot
* Andrew Sachs as Hercule Poirot
* Julian Orchard as Hospital Clerk
* John Bluthal as Guard at Cemetery
* Rita Webb as Woman at Window

== Production == The Pink Inspector Clouseau Richard Williams Studio). They later went on to produce the animated television series The All-New Pink Panther Show. Trail of the Pink Panther and Curse of the Pink Panther would be animated by the retitled Marvel Productions.

==Cancelled sequel ==
Romance of the Pink Panther was a Pink Panther film that Sellers was working on, and willing to make without Edwards, before Sellers fatal heart attack. UA considered recasting the role before convincing Blake Edwards to return to the series. Edwards chose to replace Clouseau with a new character rather than replace Sellers as Clouseau and to utilize outtakes from The Pink Panther Strikes Again to set up a transitional film (Trail of the Pink Panther) with new linking footage shot on the set of the new film (Curse of the Pink Panther).

== Soundtrack ==
The theme music, and much of the soundtrack from this entry in the series, draw heavily from the disco trends of the late 1970s. The theme itself was reworked to include a more dancy bassline, electric piano, and guitar solo.

== References ==
 

== External links ==
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 