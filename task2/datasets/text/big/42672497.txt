The Ape (2005 film)
{{Infobox film
| name           = The Ape
| image          = The Ape (2005 film).jpg
| caption        = 
| director       = James Franco
| producer     = James Franco Vince Joviette Stephen Kayo David Klein Zak Knutson
| screenplay  = James Franco Merriwether Williams
| based on    = 
| starring       = James Franco Brian Lally
| music          = Eric Stratmann David Klein
| editing        = Scott Mosier
| distributor   = RabbitBandini Productions
| released     =  
| runtime       = 92 minutes
| country       = United States
| language    = English
| budget        = 
| gross          = 
}}
The Ape is a 2005 American comedy film starring James Franco in his directorial debut, and also serves as a writer and executive producer.

== Plot ==
Human resources drone and put-upon family man Harry imagines he could be the next Dostoyevsky if he could just get a little peace and quiet. When he moves into his own apartment to craft his masterpiece, his solitude is broken by an unexpected roommate-a foul-mouthed, Hawaiian shirt-wearing gorilla (Brian Lally), eager to share his opinions on life, love, and animal magnetism. 

==Cast==
*James Franco - Harry Walker
*Brian Lally - The Ape
*Allison Bibicoff - Cathy
*Stacey Miller - Beth
*Vince Jolivette - Steve
*Nori Jill Phillips - Judy
*Danny Molina - Raoul
*David Markey - Flies With Eagles

== Release ==
The film was released in the USA on June 18, 2005

==Critical response==
The film holds a 4.6 rating at imdb.com.  The website variety.com called it "self-indulgent." 

== See also ==
* 2005 in film

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 