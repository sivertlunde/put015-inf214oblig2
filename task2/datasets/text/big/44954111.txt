Love Letters (1917 film)
{{Infobox film
| name           = Love Letters
| image          = 
| alt            = 
| caption        = 
| director       = Roy William Neill
| producer       = Thomas H. Ince
| screenplay     = Ella Stuart Carson Shannon Fife
| starring       = Dorothy Dalton William Conklin Dorcas Matthews Thurston Hall Hayward Mack William Hoffman
| music          = 
| cinematography = John Stumar
| editing        = 
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Roy William Neill and written by Ella Stuart Carson and Shannon Fife. The film stars Dorothy Dalton, William Conklin, Dorcas Matthews, Thurston Hall, Hayward Mack and William Hoffman. The film was released on December 24, 1917, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Dorothy Dalton as Eileen Rodney
*William Conklin as Raymoond Moreland
*Dorcas Matthews as Eleanor Dare
*Thurston Hall as John Harland
*Hayward Mack as Robert Maxwell
*William Hoffman as Amos

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 