List of horror films of 2012
 
 
A list of horror films released in 2012 in film|2012.

{| class="wikitable sortable" 2012
|-
! Title
! Director
! class="unsortable"| Cast
! Country
! class="unsortable"| Notes
|-
!   |  
|   || || }}|| 
|-
!   | 2-Headed Shark Attack
|   || Brooke Hogan, Lauren Perez, Carmen Electra || }} ||  
|-
!   | 3AM 3D Focus Jeerakul, Toni Rakkaen, Chakhrit Yamnam|| }}|| 
|-
!   | 9・9・81 Patitta Attayatamavitaya, Setthasitt Limkasidej, Thiti Vetchabul || }}|| 
|-
!   | 12/12/12 (film)|12/12/12
| Jared Cohn|| Sara Malakul Lane, Jesus Guevara, Steve Hanks  || }}|| 
|-
!   |  
| Various || photoJoshua Diolosa, Erik Audé || }}|| 
|-
!   | American Mary Katharine Isabelle, Tristan Risk || }} || 
|-
!   | Awake in the Woods
| Nicholas Boise|| Theresa Galeani, Keith Collins, Madelaine Kemp|| }}|| 
|-
!   | Bait (2012 film)|Bait
|   || Xavier Samuel, Sharni Vinson, Adrian Pang || }} || 
|-
!   |  
|   || Stephen Moyer, Mia Kirshner, Allie MacDonald ||  }}|| 
|-
!   |  
|   || Nansi Aluka, Christopher Denham, Stephen Kunken || }}|| 
|-
!   | Berberian Sound Studio
|  || Toby Jones, Antonio Mancino, Fatma Mohamed|| }}|| 
|-
!   | Bhoot Returns
|   || || }}|| 
|-
!   |  
|   || Drew Rausch, Rich McDonald, Ashley Wood || }}|| 
|-
!   | Blood Stained Shoes
|   || Ruby Lin, Kara Hui, Monica Mok ||  }} ||   
|-
!   | Bloody Bloody Bible Camp
| Vito Trabuco || Reggie Bannister,  , Ron Jeremy||   ||  
|-
!   | Bunshinsaba (2012 film)|Bunshinsaba Mei Ting, Wu Chao || }}|| 
|-
!   |  
|   || Kristen Connolly, Chris Hemsworth, Anna Hutchison || }} ||  
|-
!   | Chernobyl Diaries
|   || Jesse McCartney, Jonathan Sadowski, Devin Kelley  || }}||  
|-
!   |  
|   || Emma Fitzpatrick, Josh Stewart, Christopher McDonald  ||  }} ||  
|-
!   | Citadel (film)|Citadel
|   || Aneurin Barnard, Amy Shiels, Wunmi Mosaku|| }}|| 
|-
!   | Cockneys vs Zombies Horror comedy 
|-
!   | Cross Bearer
|  ||J.D. Brown, Isaac Williams, Natalie Jean|| }}|| 
|-
!   | Dark Flight Peter Knight, Patcharee Tubthong ||  }} ||  
|- Dark Shadows
|   || Johnny Depp, Michelle Pfeiffer, Helena Bonham Carter ||  }} ||  
|-
!   | Dead Sushi
|   || Rina Takeda, Shigeru Matsuzaki, Kentaro Shimazu||  }}||  
|-
!   |  
|   || He Dujuan, Zak Di, Wang Yi|| }}||  
|-
!   | Decay (film)|Decay
|  || Zoë Hatherell, Tom Procter, Stewart Martin-Haugh, Sara Mahmoud, William P.Martin|| }}||  
|-
!   |  
|   || Fernanda Andrade, Simon Quarterman, Evan Helmuth ||  }}||  
|-
!   | Dracula 3D
|   || Thomas Kretschmann, Asia Argento, Marta Gastini ||  }}   ||   
|-
!   | Excision (film)|Excision
|  || Annalynne McCord, Traci Lords, Ariel Winter|| }}|| 
|-
!   | Found (2012 film)|Found Louie Lawless, Phyllis Munro, Ethan Philbeck || || 
|-
!   | Foreclosure (film)|Foreclosure
|  || Bill Raymond, Michael Imperioli, Spencer List|| }}|| 
|- Ghost Day
| Thanit Jitnukul, Titipong Chaisati, Sorathep Vetwongsatip || Apisit Opasaimlikit, Pimradapa Wright, Padong Songsang ||  }}|| 
|-
!   | Grabbers
|   || Richard Coyle, Ruth Bradley, Russell Tovey ||  }}|| 
|-
!   | Grave Encounters 2
|   || Richard Harmon, Leanne Lapp, Dylan Playfair ||  }}|| 
|-
!   | Harpoon
|   || Monika Mok, Hu Bing, Wang Shuangbao ||  }} ||  
|-
!   | Haunted Poland
|   || Ewelina Lukaszewska, Pau Masó, Irene Gonzalez || ||  
|- Horror Stories Kim Sun, Kim Gok|| || }}|| 
|- John Dies at the End Horror comedy 
|-
!   | Killer Kart James Feeney||Ray Bouchard, Christine Alicia Rodriguez, Elly Schaefer|| || 
|-
!   |   Ernest Thomas, Jeff Daniel Phillips|| }}|| 
|-
!   | Lovely Molly
|   || Alexandra Holden, Johnny Lewis ||  }} ||  
|-
!   | Maniac (2012 film)|Maniac
|  || Elijah Wood, Megan Duffy, Nora Arnezeder|| }} || 
|-
!   | Modus Anomali Rio Dewanto, Hannah Al Rashid, Izzi Isman|| }}|| 
|-
!   | Nightmare (2012 film)|Nightmare Fiona Sit, Huang Xuan, Zhou Chuchu|| }}|| 
|-
!   | No One Lives Luke Evans Luke Evans, Adelaide Clemens, Lee Tergesen|| }}|| 
|-
!   |  
|   || Caity Lotz, Kathleen Rose Perkins, Haley Hudson||  }} ||  
|-
!   | Painless (film)|Painless Tomas Lemarquis, Juan Diego, Alex Brendemühl|| }}  ||  
|-
!   | Paranormal Activity 4
|  , Ariel Schulman || Kathryn Newton||  }} ||  
|-
!   | Piranha 3DD Matthew Bush, Chris Zylka ||  }} ||  
|-
!   |  
|   || Jeffrey Dean Morgan, Kyra Sedgwick, Natasha Calis||  }} ||  
|-
!   | Prometheus (2012 film)|Prometheus
|   || Noomi Rapace, Michael Fassbender, Charlize Theron ||  }} || Science fiction horror 
|-
!   |  
|   || Arisu Ozawa, Asami Sugiura|Asami, Yui Aikawa ||  }} ||  
|-
!   | Raaz 3
|   || Bipasha basu, Imran Hashmi, Esha Gupta||   ||  
|-
!   |  
|   || Diego Martín, Leticia Dolera, Javier Botet || }}||  
|-
!   | Sadako 3D
|   || ||  }} ||   
|-
!   | Shackled (2012 Indonesian film)|Shackled
|   || Abimana Aryasatya, Imelda Therinne, Laudya Chintya Bella || }} ||  
|-
!   |  
|   || Adelaide Clemens, Sean Bean, Radha Mitchell|| }}  ||   
|- Silent House
|  , Laura Lau || Elizabeth Olsen ||  }} ||   
|- Silent Night
|   || Jamie King, Donal Logue, Malcolm McDowell ||  }} ||  
|-
!   | Sinister (film)|Sinister
|   || Ethan Hawke, Vincent DOnofrio, James Ransone||  }} ||  
|-
!   | Smiley (2012 film)|Smiley
|   || Caitlin Gerard, Melanie Papalia, Shane Dawson||  }} ||  
|-
!   | Stitches (2012 film)|Stitches
|   || Ross Noble, Tommy Knight ||  }} ||  
|-
!   | Storage 24
|   || Noel Clarke, Colin ODonoghue, Antonia Campbell-Hughes ||  }} ||  
|-
!   | ThanksKilling 3
|   || Dan Usaj, Joe Hartzler, Marc M. ||  }} ||  
|- The Battery
|   || Niels Bolle, Adam Cronheim, Alana OBrien ||  }} ||  
|-
!   |  
|   || Dingdong Dantes, Joey Marquez, Lovi Poe|| }}|| 
|-
!   |  
|  , Bjorn Stein || Kate Beckinsale, Stephen Rea, Michael Ealy ||  }} ||  
|-
!   | V/H/S
|  , David Bruckner, Ti West, Glenn McQuaid, Joe Swanberg || Joe Swanberg, Adam Wingard, Sophia Takal || }} ||  
|-
!   |  
|   || Daniel Radcliffe, Ciarán Hinds, Janet McTeer ||  }}  ||  
|-
|}

==References==
 

 
 

 
 
 
 