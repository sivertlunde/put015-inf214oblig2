Masquerade (1965 film)
 
 
{{Infobox film
| name           = Masquerade
| image          = Masquerade 1965 poster.jpg
| image_size     = 220
| caption        = 1965 Theatrical Poster
| director       = Basil Dearden
| producer       = Michael Relph
| writer         = William Goldman Victor Canning (novel)
| narrator       =
| starring       = Cliff Robertson Jack Hawkins Phillip Green
| cinematography = Otto Heller
| editing        =  John D. Guthridge
| studio         = Michael Relph Productions (as Novus Films Limited)
| distributor    = United Artists
| released       = 23 August 1965
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} comedy thriller film directed by Basil Dearden based on the 1954 novel Castle Minerva by Victor Canning. It stars Cliff Robertson and Jack Hawkins and was filmed in Spain. 

==Synopsis==
An Arab heir plots his own kidnapping in a desperate bid for peace in the Middle East.

==Cast==
* Cliff Robertson as David Frazer
* Jack Hawkins as Colonel Drexel
* Marisa Mell as Sophie
* Michel Piccoli as George Sarrassin
* Bill Fraser as Dunwoody Charles Gray as Benson  
* John Le Mesurier as Sir Robert  
* Felix Aylmer as Henrickson  
* Ernest Clark as Minister  
* Tutte Lemkow as Paviot  
* Keith Pyott as Gustave

==Production notes==
Rex Harrison was originally meant to star but he dropped out and Cliff Robertson was hired to replace him. The film was the first screen credit for William Goldman who had been hired to Americanise the diaologue for Robertson (Robertson had just commissioned Goldman to adapt Flowers for Algernon into a screenplay). 

* Location scenes filmed in Spain and the Middle East. 
* Working title: The Shabby Tiger. Also known as Operation Masquerade.

==References==
 

==External links==
* 

 
 

 
 
 
 
 