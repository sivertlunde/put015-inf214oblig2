The Gate of Youth (1981 film)
{{Infobox film
| name           = The Gate of Youth
| image          = The Gate of Youth 1981 DVD cover.jpg
| caption        = DVD cover
| director       = Kinji Fukasaku Koreyoshi Kurahara
| producer       =
| writer         = Tatsuo Nogami
| narrator       =
| starring       =
| music          = Hako Yamazaki
| cinematography = Toru Nakajima  Hanjiro Nakazawa
| editing        =
| distributor    =
| released       = 1981
| runtime        =
| country        = Japan
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
}}
  is a 1981 film directed by Kinji Fukasaku and Koreyoshi Kurahara.

It is based on a story by Hiroyuki Itsuki that was originally serialized in the magazine Shukan Gendai in 1969-70. The same story inspired a 1975 film, also titled  , as well as three separate television productions in 1976-77 (Tokyo Broadcasting System|TBS), 1991 (TV Tokyo), and 2005 (TBS).  

==Cast==
*Bunta Sugawara as Shigezo Ibuki
*Keiko Matsuzaka as Tae Ibuki
*Kōichi Satō as Shisuke Ibuki
*Kaoru Sugita as Orie Maki
*Tomisaburo Wakayama as Ryugoro Hanawa
*Koji Tsuruta as Yabetora
*Tsunehiko Watase as Kanayama
*Junichi Ishida as Hirano
*Ryunosuke Kaneda as Hanezawa
*Kazuo Kato as Owner
*Nenji Kobayashi as Chota
*Aoi Nakajima as Kimiyo Hirai
*Taeko Shinbashi as Masae Maki
*Kantaro Suga as Ichimura
*Saburô Tokitô as Haruo

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 

 