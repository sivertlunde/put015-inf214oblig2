Under the Raven's Wing
{{Infobox Film
| name = Under the Ravens Wing
| director = Susan Adriensen
| producer = Susan Adriensen (Blue Eyed Productions) & Brian Jude
| writer = Susan Adriensen
| starring = Kimberly Amato Coy DeLuca Kamilla Sofie Sadekova Jessica Palette
| editing = Susan Adriensen
| released = October 26, 2007
| country = United States
| language = English
}}
 2007 psychological thriller starring Scream Queens). 2008 and The Queens International Film Festival in 2008 in film|2008. Under the Ravens Wing was shot with the Panasonic AG-HVX200 in the state of New Jersey. On January 15, 2008, BumsCorner.com named Under the Ravens Wing the best film of 2007, earning the Golden Fishwrap Award.  Distributed in the US on all media in 2009 by Medicine Show Cinema.   

==Plot==
A cocky unseen filmmaker (Coy DeLuca) documents three young women about a murder they committed. Raven, (Kimberly Amato) the ringleader, believes in serene dimensions beyond this world and has the power to send you there. After all, she doesnt call it murder. She and her two minions, Angel (Kamilla Sofie Sadekova) and Jessie (Jessica Palette) call it Transcendence and its the greatest gift to give. These girls want to spread their philosophy and this Director is just the man to do it. His desire to make a movie keeps him involved as he documents and recreates the girls glorious murder night.

The complicated lives of these three women unfold freely before the camera. However, without their knowledge, the Director has his own agenda and the documentary project starts to become about seduction and voyeurism. The girls stories appall yet captivate the Director as he finds himself becoming entangled in their grand plan. But Raven tries to control the production by keeping everyone in her psychological grip. Ultimately, the production goes terribly wrong and a power struggle ensues with the Director fearing for his life.

==Criticism and Ratings==
Under the Ravens Wing premiered at Full Moon Horror Film Festival to positive fan response. It has continued to receive rave reviews for Susan Adriensens development of the docu-style character study.  Kimberly Amato also continues to garner positive reviews for her portrayal of Raven, the lost and misguided lead. Theron Neal of Scream TV states "Under the Ravens Wing is a striking film that fascinates...", and Jane Rose of Pretty Scary adds "(it) uses an interesting story structure and varied, visually compelling style."

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 