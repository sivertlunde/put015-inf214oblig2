Black Cat (1991 film)
 
 
{{Infobox film
| name           = Black Cat
| image          = Black-cat-1991-poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Stephen Shin
| producer       = Stephen Shin
| writer         = Lam Wai-lun Chan Bo-shun Lam Tan-ping
| based on       =  
| starring       = Jade Leung Simon Yam Thomas Lam
| music          = Danny Chung
| cinematography = Lee Kin-keung
| editing        = Wong Wing-ming
| studio         = D & B Films Co. Ltd.
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$ 11,088,210
}}

Black Cat ( ) is a 1991 Hong Kong action film directed and produced by Stephen Shin. The film stars Jade Leung as Catherine who accidentally kills a truck driver. After escaping trial, she is captured by medics who insert a "Black Cat" chip into her brain putting her under the complete control of the American CIA. The CIA makes her into a new CIA agent known as Erica.
 Disney purchased the rights for an American remake. Critics compared the film unfavorably to Nikita. Jade Leung won the award for Best Newcomer at the 11th Hong Kong Film Awards for her role in the film. A sequel titled Black Cat 2: The Assassination of President Yeltsen was released in 1992.

==Production==
Director Stephen Shin originally wanted to make the film a straight remake of Luc Bessons film Nikita (film)|Nikita but stopped after rights to a remake in the United States were purchased by Disney.  For the role of Catherine, unknown model Jade Leung was cast. Black Cat was filmed on locations in Hong Kong, Japan and Canada. Logan, 1996. p.169 

==Release==
The film was released in Hong Kong on 17 August 1991.    The film grossed a total of HK$ 11,088,210.  The film received a sequel in 1992 titled Black Cat 2: The Assassination of President Yeltsen. 

==Reception== La Femme Nikita" 

==Notes==
 

===References===
* * {{cite book
| last      = Logan
| first     = Bey 
| title     = Hong Kong Action Cinema
| publisher = Overlook Press
| year      = 1996
| isbn      = 0879516631
| url       = http://books.google.ca/books?id=eZQEF-3pvz8C
}}

==See also==
 
* Hong Kong films of 1991
* List of action films of the 1990s

==External links==
*  

 
 
 
 
 
 
 