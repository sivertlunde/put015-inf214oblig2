Dastan (1950 film)
{{Infobox film
| name           = Dastan
| image          = Dastan1950.jpg
| image_size     = 
| caption        = 
| director       = Abdul Rashid Kardar
| producer       = 
| writer         = S.N. Banerjee
| narrator       =  Veena 
| music          = Naushad    
| cinematography =  Dwarka Divecha    
| editing        = Iqbal G.G. Mayekar    
| distributor    = 
| released       = 1950
| runtime        = 122 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1950 Bollywood Murad and Shakila (actress)|Shakila.   

The story, a tragic melodrama, was a narrative in flashback.    It was inspired from the film Enchantment (1948 film)|Enchantment (1948) directed by Irving Reis.    Cited as "one of the biggest commercial hits" by Patel, Suraiyas acting was stated to have "over-shadowed" that of Raj Kapoor.   

==Cast==
*Suraiya ...  Indira 
*Raj Kapoor ...  Raj  Veena ...  Rani  Suresh ...  Ramesh  Al Nasir ...  Kundan 
*Pratima Devi ...  Rameshs mom (as Protima Devi)  Murad ...  Father 
*S.N. Banerjee ...  Shambhu Dada (as Banerji) 
*Laxman  (as Lakshman) 
*Surendra  (as Surinder)  Shakila
*Baby Anwari   
*Swartha   
*Krishna Kumar

==Soundtrack==
The music was composed by Naushad and the lyricist was Shakeel Badayuni. The singers were Suraiya and Mohammed Rafi.    Some of the notable numbers from this film included Suraiyas "Aa Ra Ri", "Yeh Mausam Aur Yeh Tanhai", "Ae Sham Tu Bata" and "Naam Tera Hai Zuban Par". 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Ta Ra Ri Ra Ra Ri
| Suraiya
|-
| 2
| Aaya Mere Dil Mein Tu
| Suraiya
|-
| 3
| Ye Mausam Aur Ye Tanhai
| Suraiya
|-
| 4
| Ae Shama Tu Bata
| Suraiya
|-
| 5
| Nainon Mein Preet Hai
| Suraiya
|-
| 6
| Mohabbat Badha Kar Juda Ho Gaye
| Suraiya
|-
| 7
| Naam Tera Hai Zabaan Par
| Suraiya
|-
| 8
| Dil Dhadak Dhadak Dil Phadak Phadak
| Suraiya, Mohammed Rafi
|-
| 9
| Dil Ko Teri Tasveer Se
| Suraiya, Mohammed Rafi
|}

==References==
 

==External links==
*  

 

 
 
 
 
 


 