Tales from Earthsea (film)
{{Infobox film
| name = Tales from Earthsea
| image = Gedo6sn.jpg
| caption = Japanese release poster
| director = Gorō Miyazaki Toshio Suzuki  Tomohiko Ishii
| based on =  
| screenplay = Gorō Miyazaki  Keiko Niwa
| story = Hayao Miyazaki (concept)
| editing = Takeshi Seyama
| music = Tamiya Terashima
| starring = Bunta Sugawara  Junichi Okada  Aoi Teshima  Yuko Tanaka
| studio = Studio Ghibli
| distributor = Toho (Japan) Walt Disney Pictures (International) Madman Entertainment (Australia)
| released =   
| runtime = 115 minutes
| language = Japanese
| budget =$22 million
| gross = $68,673,565  
}}
  is a 2006 Japanese animated fantasy film directed by Gorō Miyazaki and produced by Studio Ghibli.

The film is based on a combination of plots and characters from the first four books of  , The Tombs of Atuan, The Farthest Shore, and Tehanu; however, the films title is named from the collection of short stories, Tales from Earthsea, made in 2001. The plot was "entirely different" according to the author Ursula K. Le Guin, who told director Gorō Miyazaki, "It is not my book. It is your movie. It is a good movie", although she later expressed her disappointment with the end result. 

==Plot==
 
A war galley is caught in a storm at sea. The ships weatherworker is distressed to realize he has lost the power to control the wind and waves, but is more so when he sees two dragons fighting above the clouds, during which one is killed by the other—an unprecedented and impossible occurrence.
 King of wizard Root tells the tale of how dragons and men were once "one", until people who cherished freedom became dragons, and men chose possessions; and of his fears of how the lands plight is because of the weakening of the worlds "Balance". The King has little time to ponder on this before he is killed in a dark corridor by a young boy who is revealed to be his son Arren. The young prince steals his fathers sword and flees the palace.

Later, the Archmage Sparrowhawk (Ged) arrives at the desert, where he witnesses Arren being pursued by Gray wolf|wolves. He rescues Arren, who later agrees to accompany Sparrowhawk on his journey and travels to the city of Hort Town, where vendors scam people and slavers sell and trade slaves. Arren, after earning a cloak from Sparrowhawk, who buys it at a cloak vendors stand, almost eats some Hazia from a Hazia dealer. Sparrowhawk stops him and tells him that Hazia is a drug that makes people hallucinate and then eventually die if they continue to eat it. Sparrowhawk returns to the inn while Arren goes to explore the town alone. Arren suddenly becomes scared, as if something is following him. While fleeing in fright, Arren sees a young girl with a burn scar on her face named Therru, fleeing from slavers and their leader, Hare. Although Arren saves her by fighting off the slavers, he also shocks her with his indifference to the life of any person, even his own, causing Therru to not trust him.

Later in the evening Arren falls asleep by the port and is captured by Hare and the slavers. Hare thinks Arrens sword is worthless junk and tosses it in the sea. Arren is rescued from the slavers caravan by Sparrowhawk. Together they travel to a farm where Therru is looked after by a woman named Tenar, an old friend of Sparrowhawk. Despite Therrus distrust towards Arren, Tenar welcomes the young prince with open arms.

Hare reports back to his master Lord Cob about the slaves escape, and almost pays with his life for the loss, until he tells Cob that it was Cobs rival Sparrowhawk who freed the slaves. Cob orders him to bring Sparrowhawk to him at the castle. Back at the farm, Arren learns more about Sparrowhawk, Tenar and Therrus lives. Sparrowhawk tells Arren that he is looking into the cause of the upset of the Balance. He soon leaves the farm to return to Hort Town and investigate further. While there he discovers Arrens sword has been recovered and is on sale at a merchants stall. Sparrowhawk is then confronted by Hare, but transforms his face to disguise himself. When the slavers leave, he changes back and buys the sword.
 true name", Lebannen, to control him. Sparrowhawk, on the way back to the farm, encounters Therru, who freed herself, and gives her Arrens sword, telling her to stay home and give it to Arren if he returns. He goes to the castle to save Tenar but instead finds Cob, whom he confronts after evading Hare and the slavers. Sparrowhawk learns that Cob is causing the worlds balance to collapse by opening the door between life and death to try and gain eternal life. After Sparrowhawk tries to warn Cob of the dangers of upsetting the Balance, Cob tells him he is "above nature." Cob then sends Arren out to kill Sparrowhawk, but Sparrowhawk stops him. As Sparrowhawk frees Arren from Cobs control, he tells Arren that death is natural and that no one can live forever causing Arren to realize what he almost did. Sparrowhawk is then captured by Hare and the slavers, as his power is weakened within the stronghold of Cobs castle. Sparrowhawk is then thrown into the same dungeon cell Tenar is locked in.

Meanwhile, Therru sees a copy of Arren and follows him to the castle, where he reveals he is the light within Arren. The reason Arren is afraid of his double is due of his fear of death and that Arren is going insane due to the darkness in his heart and due to the worlds Balance collapsing. He tells Therru his true name and says while he cannot go into the castle, he will be with her at all moments. Inside the castle, Therru learns of Sparrowhawk and Tenars sunrise execution by eavesdropping on Hare and the slavers conversation. Therru then finds Arren, full of guilt and hopelessness, and brings hope back to him by telling him that life is beautiful and that death is part of the circle of life. She then tells Arren his true name and confides in him her own true name, Tehanu. They both rush to rescue Sparrowhawk and Tenar from Cob, Hare and the slavers, who are about to throw them off a high tower as part of the execution. After fighting and scaring off Hare and the slavers, Arren confronts Cob, but Cob tries to kill him with a "Summoning Spell," but he fights back and then finally unsheathes his sword, revealing its magical nature. Arren then cuts off Cobs hand. With his hand and staff now gone, Cob can no longer use his magic powers. With his magic almost gone Cob begins to age rapidly and turns into a grotesque old man. He reattaches his severed hand and reveals that he wants eternal life because he doesnt want to die and that he wants revenge on the wizards of Earthsea and Roke for banishing him for trying to control the dead and become Archmage himself. Cob then captures Therru and flees to the highest tower on the castle, with Arren in hot pursuit. Cornering Cob, Arren tries to explain what he learned about life and death from Therru and Sparrowhawk to Cob, but Cob refuses to listen and using the last of his magic, strangles Therru to death. However, she does not die as she has eternal life, and instead becomes a dragon. Therru kills Cob and rescues Arren from the collapsing castle tower that Cob destroyed on trying to prevent Arren from advancing.

Sparrowhawk and Tenar leave the castle. Tenar wonders where Arren and Therru have gone. Sparrowhawk reassures her that they have Therrus dragon wings to guide them. Therru and Arren meanwhile land at a field where Therru changes back into a human. Arren tells Therru he will leave for home to repent for his crime, but will come back to see her some day.

In the epilogue, after Arren and Therru reunite with Sparrowhawk and Tenar, Arren and Sparrowhawk reconcile for fighting earlier. All four of them then pitch in to finish the farm chores and spend some time together like a family. Eventually, Arren and Sparrowhawk depart for Enlad, bidding Therru and Tenar goodbye. Therru then looks up to see dragons peacefully flying in the sky, indicating that the worlds balance is back to normal as the Japanese writing meaning "The end" appears before fading to black and the end credits.

==History==
This feature film from Studio Ghibli is the first anime film adaptation of any part of the Earthsea series. In the past, many directors, including Hayao Miyazaki, {{cite web
|url=http://mag.udn.com/mag/dc/storypage.jsp?f_ART_ID=32822
|title=Article about the anime by Shuffle Alliance, a Taiwan anime club
|accessdate=2006-06-18
}}  had tried to adapt the Earthsea cycle for film, but were disapproved by the author herself.  When Le Guin first heard of Miyazakis interest in adapting her work, she had not seen any of his films and associated animation with the output of Disney; as such, she turned down his request.
 Oscar for his film Spirited Away, Hayao Miyazaki received Le Guins approval but was busy directing Howls Moving Castle (film)|Howls Moving Castle.  Studio Ghibli head Toshio Suzuki decided that Hayaos son Gorō Miyazaki, who was advising on the film, should be given his first directing job for the adaptation. Hayao was dissatisfied with the decision, thinking that Gorō lacked the necessary experience. They reportedly did not speak to one another during production of the film, however Hayao later acknowledged his sons work upon its first preview." 

==Voice cast== wizard of Earthsea, known as the Archmage. He travels around, searching for answers on why the worlds balance is collapsing. He eventually learned that his nemesis, Cob, is behind this and when he got captured at the end, Arren and Therru rescue him. Prince Arren/Lebannen, a prince of Enlad. Due to the worlds balance collapsing, he is going insane from the darkness in his heart and is also afraid of death that he is being pursued by an unknown presence. In the end, Sparrowhawk and Therru help him overcome the darkness and his fear of death and eventually, help save Earthsea. He even eventually falls for Therru. Blaire Restaneo in the English Dub) as Characters in Earthsea|Therru/Tehanu, a first-degree burn scarred girl who is the same age as Arren. At first, she doesnt trust Arren because she thinks he doesnt care about life. Eventually, after Arren explained about his fears, she warms up to him and eventually falls for him. In the end, she is revealed to have eternal life and has the ability to change into a dragon and back, which resulted her in defeating Cob with her fiery breath.
* Jun Fubuki (Mariska Hargitay in the English Dub) as Characters in Earthsea|Tenar, a woman who is Sparrowhawks old friend. She used to be a priestess at the Tombs of Atuan, but Sparrowhawk guided her to freedom from that place. She acts as Therrus only parent and welcomes Arren into her home with open arms. In the end, after getting captured by Cobs minions along with Sparrowhawk, Arren and Therru rescue her and Sparrowhawk during the final battle. Kaoru Kobayashi King of Enlad. He cares for his kingdom and is worried about the conditions. He is later killed by Arren. Queen of Enlad. She is a strict woman and during the Kings concerns, she scolded two maids for interrupting her husbands work when they expressed their concerns for Arren, because she believes Arren is old enough to take care of himself.
* Yūko Tanaka (Willem Dafoe in the English Dub) as Characters in Earthsea|Cob, the main antagonist of the film. He is responsible for causing the worlds balance to go out of control by opening the door between life and death in order to gain eternal life. In the end, Arren and Therru defeated him and the worlds balance returned to normal.
* Teruyuki Kagawa (Cheech Marin in the English Dub) as Hare, Cobs minion head slaver. He is very loyal to Cob and is cowardly at times. In the end, Arren fought and scared him off during the final battle and he and the slavers leave the castle in fear. Whatever happened to them afterwards is unknown.
* Mitsuko Baisho (Kat Cressida in the English Dub) as a Cloak Vendor, a middle-aged woman who sells cloaks.
* Takashi Naito (Jess Harnell in the English Dub) as Hazia Dealer, a man who tried to bribe Arren into eating some Hazia when Sparrowhawk interfered.

==Trailer== NTV on 23 February 2006 (the day the trailer was completed. {{cite web
|url=http://www.nausicaa.net/miyazaki/earthsea/blog/blog23.html
|title=Translation of Gorō Miyazakis Blog (page 23)
|accessdate=2006-05-30
}} ) Theo Le Guin, Ursula K. Le Guins son, viewed the Japanese trailer and said this of it: "The images are really beautiful. The song too, its not like something from Hollywood, but felt really like Ghibli." {{cite web
|url=http://www.nausicaa.net/miyazaki/earthsea/blog/blog32.html
|title=Translation of Gorō Miyazakis Blog (page 32)
|accessdate=2006-05-30
}}  The trailers were made by Keiichi Itagaki, who had been responsible for trailers for all of the other Ghibli films up until then.

==Score== multichannel hybrid Carlos Núñez was a key collaborator on the soundtrack, contributing his ocarina, whistle and Galician gaita (bagpipe) to 11 of the 21 tracks. Newcomer singer, Aoi Teshima, sang in 2 of the tracks. A follow-up album, "Melodies from Gedo Senki", was released in 17 January 2007 and included unreleased Gedo Senki OST tracks and new tracks by Núñez. Its release code is SICP-1151 and its ASIN is B000HT1ZLW.  

==Reception==
The film reached No.1 at the Japanese Box Office on its opening week with a gross of over 900 million   to second place and became the number one movie in the country for five weeks,  until it was pushed out of the top spot when   was released.  The movie went on to be the #4 top grossing movie for the year in Japan.

Ursula K. Le Guin, the author of the Earthsea Series, gave a mixed response to the film in her review on her website. Le Guin commended the visual animation in the film but stated that the plot departed so greatly from her story that she was "watching an entirely different story, confusingly enacted by people with the same names as in my story." She also praised certain depictions of nature in the film, but felt that the production values of the film were not as high as previous works directed by Hayao Miyazaki, and that the films excitement was focused too much around scenes of violence. Her initial response to Goro Miyazaki was "It is not my book. It is your movie. It is a good movie".    However, she stated that the comment disclosed on the movies public blog did not portray her true feelings about the films vast departure from original stories; "taking bits and pieces out of context, and replacing the storylines with an entirely different plot..." 

Le Guins mixed opinion of the film is indicative of the overall reception of the film, particularly in Japan. In Japan, the film found both strong proponents and detractors. Many of the opinions can best be summed up in a response to Le Guins comments on her website, that the weak points of the film were the result of "when too much responsibility was shouldered by someone not equipped for it".   
 The Girl Who Leapt Through Time)  and was selected in the Out of Competition section at the 63rd Venice Film Festival. 
Rotten Tomatoes Tomatometer shows a rating of 42%. 

==International releases== Disney to receive a PG-13 rating. It is also the second Studio Ghibli film to receive this rating after Princess Mononoke. The DVD release date was March 8, 2011.

The film was released in selected UK cinemas on August 3, 2007, in both subtitled and English dubbed versions. The film was not released as widely as previous Ghibli movies, playing to 23 venues across the nation and making an unremarkable £23,300.     Reviews were generally positive, but received mixed reviews when it was compared to the past Ghibli films. Radio Times suggested that it "lacks the technical sheen and warm sentimentality of some of Ghiblis earlier films", {{cite web
|url=http://www.radiotimes.com/servlet_film/com.icl.beeb.rtfilms.client.simpleSearchServlet?frn=47068&searchTypeSelect=5
|title=Radio Times Film Review: Tales from Earthsea
|accessdate=2007-09-11
}}  while the Daily Mirror called it "ploddy, excruciatingly slow" and not in the same league as the work of Hayao Miyazaki. {{cite web
|url=http://www.mirror.co.uk/showbiz/entertainment/movies/2007/08/03/tales-from-earthsea-89520-19554356/
|title=Daily Mirror: Tales from Earthsea
|accessdate=2007-09-11 Empire magazine said it was "well worth watching"    while The Guardian called it "An engaging piece of work"   
DVD distributor Optimum Releasing released an English dubbed and subtitled, region 2 DVD for the UK market on January 28, 2008.  To mark the release, HMV ran frequent sponsor credits for the DVD, as well as a prize competition, on the AnimeCentral channel. 
 Perth over the following months. It was notable that unlike previous Studio Ghibli releases, only a subtitled version was seen in cinemas. A 2-disc DVD was released on September 12, 2007 by Madman Entertainment, this time featuring both the English and Japanese versions. 

In Spain, Tales from Earthsea (Cuentos de Terramar) premiered only in Madrid and Barcelona in two small theaters on December 28, 2007, only in a Japanese version with subtitles (an odd theatrical release compared to previous Ghibli movies). A single DVD and a special 2-disc DVD were released on March 12, 2008 by Aurum Producciones|Aurum, this time with a Spanish soundtrack included.

==Adaptations==
A manga adaptation of the film has been published in Japan. 

==References==
 

==Further reading==
*  
*  
*  
*  
*  
*  
*  

==External links==
*    
*    
*  
*  
*  
*  
*  
*  
*   at Metacritic
*  
*   information at Nausicaa.net
* Gorō Miyazakis    
* Translation of Gorō Miyazakis  
*   with producer Toshio Suzuki
*   at Ursula K. Le Guins Web site
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 