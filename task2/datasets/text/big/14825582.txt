Strangers When We Meet (film)
{{Infobox film
| name           = Strangers When We Meet
| image          = Poster of the movie Strangers When We Meet.jpg
| image_size     =
| caption        =
| director       = Richard Quine
| producer       = Richard Quine Bryna Productions Richard Quine Productions
| writer         = Evan Hunter
| narrator       =
| starring       = Kirk Douglas Kim Novak Ernie Kovacs Barbara Rush
| music          = George Duning
| cinematography = Charles Lang
| editing        = Charles Nelson
| studio         = Bryna Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States English
| budget         =
| gross          = $3,400,000 (US/ Canada) 
}} 1960 drama film about two married neighbors who have an sexual affair|affair. The movie was adapted by Evan Hunter from his novel of the same name and directed by Richard Quine. The film stars Kirk Douglas, Kim Novak, Ernie Kovacs, Barbara Rush, and Walter Matthau.  
 Beverly Hills, Santa Monica, and Malibu, California|Malibu.

==Plot== Los Angeles architect who is married with two kids. He has a very bright wife, Eve. She is ambitious for him, but he wants to do work more imaginative than the commercial buildings hes been designing. He meets with Roger Altar, an author, to discuss building a house that will be an "experiment" and something Coe wants to do more of, something original.

Maggie Gault is one of his neighbors whose son is friends with his. She tells Larry she has seen some of his previous houses and thinks that the more unconventional houses are the best.  This encouragement is what he needs from his wife but hasnt been able to get.

Both Larry and Maggie grow dissatisfied in their marriages. Larrys wife is too hard-headed and practical and Maggies husband isnt interested in having sex with her. So they have an affair that involves meeting in secret. They both know what theyre doing is wrong, and they are devoted to their children.

Felix Anders is a neighbor who snoops around and finds out about their affair. His leering and insinuations make Larry realize the risks hes taking. He tells Maggie that they shouldnt see each other for a while. Felix, in the meantime, makes a play for Larrys wife. In a way, Felix is a personification of the tawdriness of Larry and Maggies affair.

After her near-rape by Felix, Eve wises up and realizes that Larry has been unfaithful. She confronts him. They agree to stay together and move to Hawaii, where Larry has been offered a job to design a city.

Altars house is finished but still empty. Maggie drives up to take a look at it. Larry shows up and they talk about how they can never be together. Larry wishes he and Maggie could live in the house and if they did, he would dig a moat around it and never leave it. Maggie says she loves him.

The contractor for the house shows up and thinks Maggie is Larrys wife. They both take a moment to savor the irony of his remark and Maggie drives away.

==Cast==

* Kirk Douglas as Larry Coe
* Kim Novak as Margaret Gault
* Ernie Kovacs as Roger Altar
* Barbara Rush as Eve Coe
* Walter Matthau as Felix Anders
* Virginia Bruce as Mrs. Wagner
* Kent Smith as Stanley Baxter
* Helen Gallagher as Betty Anders
* John Bryant as Ken Gault
* Sue Ane Langdon as Daphne

==Reception==
  called the movie: "pure tripe".  "Unvaried strangulated hush", is how film critic Stanley Kauffmann, in The New Republic, described Novaks diction.  Craig Butler at Allmovie says that Douglas "seems a little out of place", and that the screenplay is "predictable". 

==References==
 

==External links==
* 
* 
* 
* 
*Tonguette, Peter. -  . - The Film Journal
*  at Trailers from Hell

 

 
 
  
 
 
 
 
 
 
 
 