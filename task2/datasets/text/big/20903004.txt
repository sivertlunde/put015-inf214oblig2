Shareef Budmaash
{{Infobox film
| name           = Shareef Budmaash
| image          = Shareef Budmaash.jpg
| image_size     = 
| caption        = 
| director       = Raj Khosla
| producer       = 
| writer         =
| narrator       = 
| starring       =Hema Malini
| music          =R.D.Burman
| lyrics          =Anand Bhakshi  
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1973
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1973 Bollywood action thriller film directed by Raj Khosla. The film stars Dev Anand, Hema Malini, Shatrughan Sinha and Ajit in pivotal roles.

==Cast==
*Dev Anand ... Rocky / Inspector Ramesh / Sustram
*Hema Malini ... Seema 
*Ajit ... Ranjit 
*Jeevan ... Diwan Saheb 
*Trilok Kapoor    Helen ... Carmen / Rita 
*Bhagwan ... Ranjits henchman 
*Jankidas ... Prisoner 
*D.K. Sapru ... Lawyer  Sudhir ... Ajit

==Soundtrack==
The songs are composed by legendary R.D.Burman and written by Anand Bakshi
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Neendh Churake"
| Asha Bhosle Kishore Kumar
|-
| 2
| "Mohabbath Baazi Jeetengi"
| Asha Bhosle
|-
| 3
| "Main Nikal Jaoonga"
| Kishore Kumar
|-
|-
| 4
| "Tere Sau Deewane"
| Kishore Kumar
|-
| 5
| "Meri Mohabbat Mein"
| Lata Mangeshkar
|}

==External links==
*  

 
 
 
 
 


 
 