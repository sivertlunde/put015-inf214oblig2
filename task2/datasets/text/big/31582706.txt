Wrong Number (film)
 
 
{{Infobox Film
| name   = Wrong Number
| image    = Wrong Number.jpg
| image_size  = 200px
| caption     = VCD Cover
| director      = Motin Rahman
| producer    = Faridur Reza Sagar (Impress Telefilm Ltd.)
| writer         = Pronabh Bhatto Riaz Shrabanti Abdul Qader Nasir Rahman Dolly Johur Tushar Khan Amol Bosh Tahsina Taskina
| music          = Ahmed Imtiaz Bulbul Ayub Bachchu S I Tutul Nachiketa (India)
| cinematography = Mostafa Kamal
| editing        = Amzad Hossain
| distributor    = Impress Telefilm Limited
| released       = 25 March 2004
| runtime        = 144 Min.
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
| imdb_id        = 
}}
Wrong Number also ( ) is a Bangladeshi Bengali language film.  The film released on 25 March 2004 in all over Bangladesh. The film directed by Motin Rahman. And produced-distributed by Impress Telefilm Limited. Stars Riaz (actor)|Riaz, Shrabanti, Abdul Qader, Nasir Rahman, Dolly Johur, Tushar Khan, Amol Bosh, Tahsina, Taskina and many more. 

==Synopsis== Riaz ).The story advances through a series of humorous events.

==Cast== Riaz as Abir
* Shrabanti as Athoi
* Abdul Qader as Boro Bhai
* Nasir Rahman as
* Dolly Johur as Athois Unt
* Tushar Khan as Athois Father
* Amol Bose as Abirs Father
* Amin Tushar as Abirs Servant
* Tahsina as Athois College Friend
* Taskina as Athois College Friend

==Crew==
* Director: Motin Rahman
* Producer: Faridur Reza Sagar (Impress Telefilm Ltd.)
* Story: Pronabh Bhatto
* Music: Ahmed Imtiaz Bulbul, Ayub Bachchu, S I Tutul and Nachiketa
* Lyrics: Ahmed Imtiaz Bulbul
* Composed: Ayub Bachchu, Imon Shaha, S I Tutul and Nachiketa
* Background Sound: S I Tutul
* Cinematography: Mostafa Kamal
* Editing: Amzad Hossain
* Distributor: Impress Telefilm Limited

==Technical details==
* Format: 35 MM (Color)
* Running Time: 144 Minutes Bengali
* Country of Origin: Bangladesh
* Date of Theatrical Release: 25 March 2004
* Year of the Product: 2003
* Technical Support: Bangladesh Film Development Corporation (BFDC)

==Music==
{{Infobox album
| Name = Wrong Number
| Type = soundtrack
| Cover = 
| Artist = Ayub Bachchu, Imon Shaha, S I Tutul and Nachiketa
| Released = 2004 (Bangladesh)
| Recorded = 2004
| Genre = Films Sound Track
| Length =
| Label = 
| Producer = CD PLUS
}}
Worng Number films music directed by the Popular composers Ayub Bachchu, Imon Shaha, Tutul and Indian singer Nachiketa composed the music for the movie while Kanakchampa, Asif, Panthakanai, Nachiketa, Udit Narayan, Ferdous Ara and Mehreen  lent their voice for the playbacks.

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Tracks !! Titles !! Singers !! Performers 
|- 1
|Ami Amar Bhitor Thekey Nami Mahreen
|Shrabanti
|- 2
|Preme Poroche Mon Kanak Chapa Shrabanti
|- 3
|Tumi Jodi Chao Ekbar Nachiketa
|Riaz Riaz
|- 4
|Chikon Komor Amar Shakila Zafar and Kumar Bswajit  Riaz (actor)|Riaz, Shrabanti and Abdul Qader
|- 5
|Megh Kalo oi Chokhr Taray Udit Narayan, Ferdous Ara   Riaz (actor)|Riaz and Shrabanti 
|-
|}

==References==
 

==External links==
* 
*  

 
 
 
 
 
 