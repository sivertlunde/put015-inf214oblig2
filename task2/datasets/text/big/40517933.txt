Super Speed (1925 film)
{{infobox film
| title          = Super Speed
| image          =
| imagesize      =
| caption        =
| director       = Al Rogell
| producer       = Harry Joe Brown
| writer         = John Grey (screenwriter)|J. W. Grey(story) Henry Roberts Symonds(aka J.W. Grey)(story)
| starring       = Reed Howes Mildred Harris
| music          =
| cinematography =
| editing        =
| distributor    = Rayart Pictures
| released       = April 2, 1925
| runtime        = 5 reels
| country        = USA
| language       = Silent film(English intertitles)

}}
Super Speed is a 1925 silent film comedy directed by Al Rogell and starring Reed Howes and Mildred Harris.  

It survives at the Library of Congress. 

==Cast==
*Reed Howes - Pat OFarrell
*Mildred Harris - Claire Knight
*Charles Clary - Warner Knight
*Sheldon Lewis - Stanton Wade George A. Williams - Dad Perkins (*billed as George Williams)
*Martin Turner - Zeke, Pats Valet

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 