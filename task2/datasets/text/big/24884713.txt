The Adventures of Sherlock Holmes and Dr. Watson (film)
The Soviet film adaptation of Arthur Conan Doyles stories about Sherlock Holmes. It is the second film (episodes 3-5) in The Adventures of Sherlock Holmes and Dr. Watson film series directed by Igor Maslennikov.

The film is based on three stories by Conan Doyle - "The Adventure of Charles Augustus Milverton", "The Adventure of the Final Problem", "The Adventure of the Empty House".

==Part 1: The King of Blackmailers== Mycroft has much better ability to deduction, though he almost doesnt use it.

Shortly afterwards, Holmes and Watson are invited by Mycroft to the Diogenes Club, the club for the most unsociable men of London, where even talking is banned. There, Mycroft asks Sherlock to help young Lady Eva Blackwell. She is blackmailed by one of the most villainous men of London, Charles Augustus Milverton. The latter demands £7,000 for the letters, which would cause a scandal that would end Lady Evas marriage engagement. Sherlock agrees to help her.

The same day Milverton himself shows up at 221B. He knows about Holmes task and refuses to get the letters back, unless being paid seven thousands. 
Sherlock understands that he has to steal these letters from Milverton. Watson joins him. After learning the plan of blackmailers house and his daily routine from his housemaid, the two burgle Milvertons headquarters at night. They find the letters in his safe, but have to hide behind the curtain for Charles Augustus suddenly appears in the room. He has a meeting with a supposed maidservant offering to sell letters that would compromise her mistress.

The woman is actually Lady Huksley, whose broken-hearted husband died when she wouldnt pay Milverton and he revealed her secret. Now Lady Huksley revenges, killing the blackmailer with a revolver. She takes the note he read from his dead hand and leaves. Holmes and Watson destroy Lady Evas letters and run away from Milvertons servants. But they leave many clues to the police: Sherlocks blood (he cut himself with glass) and Watsons shawl and boot.

The next morning, Inspector Lestrade visits 221B. He investigates Milvertons murder and asks detective for assistance. Luckily, Lestrade cant even imagine to suspect Holmes or Watson, and leaves after Sherlocks refuse.

In the end, Holmes and Watson go to Lady Huksleys house. Sherlock asks for the note which Lady has taken and which is signed by the "M" letter, made up of four crossed swords. She pretends to know nothing about that, but later sends the note to detective by mail.

The note is encrypted. Holmes starts to decrypting it, looking forward for a new puzzle.

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 
 