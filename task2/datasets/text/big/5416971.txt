Down (film)
{{Infobox film
| name           = Down 
| image          = TheShaftDVD.jpg
| director       = Dick Maas
| producer       = Laurens Geels
| writer         = Dick Maas James Marshall  Naomi Watts Eric Thal Michael Ironside  Edward Herrmann  Ron Perlman  Dan Hedaya  David Gwillim
| music          = Paul M. van Brugge
| cinematography = Marc Felperlaan
| editing        = Bert Rijkelijkhuizen
| distributor    = Buena Vista International 
| released       =  May 20, 2001
| runtime        =  110 minutes
| country        =  United States Netherlands
| language       = English
}} James Marshall and Naomi Watts. The film is also known as The Shaft, which is the name used for the United States DVD release. The film is a remake of the 1983 Dutch film De Lift (The Elevator), which was also directed by Maas.

Watts plays the role of pushy journalist Jennifer Evans, and Marshall is Mark, an elevator repairman and former Marine. Directed by Dick Maas, the movie was mainly filmed in the Netherlands, although the crew briefly visited New York and the District of Columbia as well for exterior shots.

The film premiered at the Cannes Film Festival in May 2001, but was not released on video in the United States until two years later.  

Produced before the September 11, 2001, attacks, the film makes several references to the possibility of terrorists attacking New York City and even specifically about Osama bin Laden. 

==Plot== James Marshall), they determine that nothing is wrong with the elevators, a large part being Jeffs inability to actually admit there is something wrong (he states throughout his scenes that the computer controlling the elevators has absolutely no defects).

A short time later, a blind man and his guide dog disappear in the building.  The two guards from the beginning of the film discover the dogs corpse hanging from its collar on a shaft support. The discovering guards head is caught between the elevator doors. He is decapitated a short while later, his partner too horrified to help him. Once again, the METEOR executives find nothing wrong with the elevators. Evans interviews Newman, who sarcastically states "Nine people out of ten make it out of an elevator alive." Evans places this in her report, causing a large controversy over his statement by his boss, Mitchell (Ron Perlman) and the police. During the same day, a roller skater is sucked into an elevator in the parking garage and shot from the 86th floor of the building to his death. The roller skaters death is explained to media as suicide.

Evans visits Newman and shows him a tape of the roller skaters death. She points out the time it normally takes for the elevator to go up 87 floors would take about 40 seconds to a minute. However, the elevator ascended the floors in less than two seconds, thus noting that there is definitely something wrong. When they try to show the tape to Jeff, he refuses to watch it and leaves abruptly. Instead, they go to Evans office and look up a man named Gunther Steinberg (Michael Ironside), who had been experimenting with organic reproducing computer chips using dolphin brains. However, the project had gone disastrously wrong and Steinberg was fired. Later the next morning Milligan (Edward Herrmann), who remains suspicious of the elevators throughout the film, discovers Jeffs corpse in an elevator shaft. When Jennifer and Mark arrive, they are shocked to hear that the police have concocted a story that has Jeff being a terrorist and being behind the incidents and assure the public that the threat is over. Later during the day, an elevator cab flies to top floor at such a speed that the floor flies off and all the people in it are killed. This event reaches the President and is seen as an act of terrorism. 

A terrorism unit is assembled at the building to get any further terrorists out of the building.  Meanwhile, Jennifer and Mark discover a recent suicide could be linked to the incidents, as his extremely superstitious widow believes his soul has returned to punish others. Jennifer and Mark enter the building to discover and stop the threat once and for all.  During the entry, Jennifer is taken into custody posing as a METEOR executive.  During her first attempt to prove this fraud, she receives a phone call from a friend to explains that while Steinbergs time working on the "fucking chips" as they are called in the movie, has been revoked, Steinberg continued to work on the project except not with dolphin brains. Eventually, Mitchell abandons Steinberg for fear of his own reputation being ruined. Mark manages to get into the Millennium Building and discovers a large bio-chip in the form of a brain in an elevator shaft. It is assumed that this brain is alive and controlling the elevators. He attempts to destroy using a screwdriver, but this attempt fails when it sends a flaming elevator down to kill him. Mark barely escapes while the elevator kills a SWAT officer who was barely out of the elevator shaft before he was sliced in half from his waist down with the upper half of his body sliding across the floor. Marshall gets a hold of a stinger missile launcher and is about to destroy the organ when Steinberg intervenes, threatening him. Jennifer appears, having escaped custody and frees Marshall. As Mark tries to destroy the organ a third time, the police enter, giving Steinberg the opportunity to hold Jennifer hostage. Jennifer manages to escape thanks to Steinberg being unable to recognize one of his superiors. Steinberg is grabbed by the elevator shaft cables and pulled in, along with Mark. At the last second, Jennifer kicks the stinger launcher to Mark, who proceeds to destroy the organ. Steinbergs mutilated corpse falls seconds later.

Some time later, Mark and Jennifer leave a hospital where they find themselves trapped in an elevator.  However, it proves to be a ruse for Mark to make a romantic overture toward Jennifer. The film then cuts out to Aerosmith|Aerosmiths song "Love in an Elevator".

==Reception==
Currently, there are not enough reviews on Rotten Tomatoes to equal a score. Out of five reviews, four are considered "rotten". If there would have been a score, however, it would have a 20% "Rotten" rating.  Common criticisms include that the film was not gripping enough, having a weak storyline, having generally unrealistic deaths, weak dialogue and unconvincing special effects.

The film was not well received in the Netherlands and was seen as comparing poorly to De Lift, considered to be one of the strongest Dutch horror films of all time, whilst Down was regarded as a typical American B-film. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 