The Grand Babylon Hotel (1920 film)
{{Infobox film
| name           = The Grand Babylon Hotel 
| image          =
| caption        =
| director       = E.A. Dupont 
| producer       = 
| writer         = Hanns Kräly
| starring       = Hans Albers   Max Landa   Karl Falkenberg
| music          = 
| cinematography = Charles Paulus
| editing        = 
| studio         = Stern-Film  
| distributor    = 
| released       = 6 February 1920 
| runtime        = 
| country        = Germany
| awards         =
| language       = Silent   German intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent mystery film directed by E.A. Dupont and starring Hans Albers, Max Landa and Karl Falkenberg. The films title is drawn from the 1902 novel The Grand Babylon Hotel by Arnold Bennett. 

==Cast==
* Hans Albers
* Max Landa
* Karl Falkenberg 
* Toni Grünfeld 
* Hanni Weisse
* Maria Zelenka

== References ==
 

==Bibliography==
* Hunter, Jefferson. English Filing, English Writing. Indiana University Press, 2010.

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 