Luck (film)
 
 
{{Infobox film
| name=Luck
| image            = Luck Hindi Movie Poster.jpg
| image_size       = 185px
| caption          = Theatrical release poster
| director         = Soham Shah
| producer         = Dhillin Mehta
| story            = Rensil DSilva Soham Shah
| screenplay       = Rensil DSilva Soham Shah Imran Khan Shruti Haasan Danny Denzongpa Ravi Kishan Chitrashi Rawat Rati Agnihotri
| music            = Salim-Sulaiman
| cinematography   = Santosh Thundiyil
| editing   = Biren Jyoti Mohanty
| distributor      = Shree Ashtavinayak Cine Vision Ltd Viacom 18 Motion Pictures
| released         =  
| runtime          = 141 minutes
| country          = India
| language         = Hindi
| budget           =  
}}
 thriller film Imran Khan and Sanjay Dutt; the other significant parts, and fellow members of the gang were played by  Danny Denzongpa, Ravi Kishan and Chitrashi Rawat, prominent actors from the Indian film industry. The camera of the film was handled by Santosh Thundiyil, whilst composer duo Salim-Sulaiman recorded the musical score.

The film released to Indian audiences on 24 July 2009 & was critically panned.

==Plot==

Luck is the story of kingpin Karim Musa (Sanjay Dutt), who is a very lucky person seeing the story of his life. He remained the sole survivor when a mosque collapsed in Maharashtra when he was just a child of nine months and was amongst very few survivors of another incident at the age of 12. When he was 14, he challenged three friends to jump from a four-storied building and remained alive, though with some injuries, and won. He started gambling at 19 and since then tried out his luck in different places of the world and became known as Musa Bhai. To him gambling and drug smuggling have become old ways to get money, his latest interest is investment on peoples luck.
 Imran Khan), is abducted by Lakhan Tamang (Danny Denzongpa), but instead of taking, he gives Ram 100,000 rupees and explains that Ram is very lucky as Tamang bought lottery tickets for two years without any success until he took tickets from Ram both of which won him 200,000 rupees. He then offers Ram 200&nbsp;million rupees for which Ram has to gamble with him for 20 days. After Ram agrees, Musa Bhai and Tamang create a game, getting lots of lucky people to play, and the luckiest ones win them money.

Later, along with Ram, Tamang hires another lucky man, Major Jawan Pratap Singh (Mithun Chakrabarty), who is a soldier and a lucky one as he fought in many battles and faced numerous bullets none that could kill him. He is in need of money for the operation of his wife, Sheela (Rupa Ganguly). Tamang visits Pakistan where he sees Shortcut (Chitrashi Rawat), a 16-year old girl, who is also known to be lucky as any camel she rides wins the race and observes her winning one despite her camels half-broken leg. Tamang pays four lakh rupees to "export" her to South Africa. Tamang then invites the sole-criminal, Raghav (Ravi Kishan) to the game. Raghav was put in Tihar Jail for raping many women, and also killing them, but when he was hanged, he survived, and since the Indian law doesnt allow one person to be hanged twice, he frees from Tihar Jail.

All competitors arrive in Cape Town. There, Ram meets another lucky girl (Shruti Haasan). Her name is revealed to be Ayesha and Tamang says she took part in the game once before. In their first game, all competitors shoot the person to their right at the same time. All the real unlucky ones die, though the real lucky ones survive. Later that night, while everyone is eating at a dining table, Raghav goes to Ayeshas room, and starts flirting with her, though Ayesha pays no attention and leaves. The second game is where all of the lucky ones take parachutes and jump off from a helicopter. Among the unlucky is Ayesha whose parachute doesnt open but she is saved by Ram, who developed a liking towards her, and afterwards, they start getting closer. In the third game, everyone is left in a hollow tank with one of everyones hand locked with handcuffs and in front of them are 300 keys and one each will open the handcuffs and added to their trouble are the sharks that swim around them as they start opening the handcuffs as the tank sinks. Only one dies, though the most jolly girl, whom everyone loved, Shortcut is attacked by a shark. The shark bites her leg, but since Ram cant bear to see it, he jumps into the water, and saves the harmed Shortcut (who is then taken to the hospital). Later on, it is revealed that due to her leg, Shortcut will not be taking part in any of the games any more.

Before the fourth and final game, Raghav tries to kill Ayesha, who then runs away to Musa and Tamang, where it is revealed that Ayesha is really dead and it is Natasha (her twin-sister) playing, taking Ayeshas character whilst seeking revenge on Musa, for murdering her sister, though Musa finds out about this, and uses her for the game. In the final game, Ram, on whom the bid is the highest ever, has to save Natasha before the train shes tied to hits an oil tanker while Major Singh will help him and Raghav will shoot at Ram and the other survivors try to stop Ram. On the train, Ram starts to fight Raghav, but once Raghav is knocked out on the floor, Ram goes to save Natasha, but Raghav gets up, then interrupted by Major Singh, who to save Natasha and Ram, jumps onto Raghav. However, Raghav pushes Major Singh down to the back of the train, which later explodes. Raghav then goes to Ram, who is currently untying Natashas hands, though Raghav attacks Ram from the back, but when the oil tank reaches right in front of the train, Ram and Natasha jump off, and Raghav dies. When Ram and Natasha realise what has happened, before they lose hope, Major Singh is seen on the floor getting up, who explains that he jumped off the train a second before his side of the train exploded. The three survivors are then approached by Musa and Tamang, who claim that they won 200&nbsp;million rupees each due to Ram.

In the climax, Ram stakes his reward and challenges Musa to a game where two pistols are dug in coal the trains carrying and one has to find one and shoot the other. Musa finds both but hands one to Ram and both shoot at the same time and hit each other, Ram is shot at the place of his heart and Musa survives as he is hit at his shoulder. Though, it is shown that Rams heart is at his right side (dextrocardia), very rare for a human, rather than left and eventually wins back his reward. Then, Shortcuts leg is fixed by a prosthesis, Major Singh saves his wife and maintains his title "Lucky Major Jawar Pratap Singh" and Ram and Natasha become life partners.

==Cast== Imran Khan as Ram Mehra
* Shruti Haasan as Ayesha / Natasha
* Mithun Chakraborty as Major Jawaar Pratap Singh
* Sanjay Dutt as Karim Moussa
* Danny Denzongpa as Lakhan Tamaang
* Ravi Kissen as Raghav Raghuvaran
* Rati Agnihotri as Rams Mother
* Chitrashi Rawat as Shortcut
* Snita Mahey as Angela (Introducing)
* Snehal Dabi as Jiten
* Daya Shankar Pandey as Passport Agent
* Roopa Ganguly as Sheila (Major Jawar Pratap Singhs wife)
* Kota Srinivasa Rao as Swami

==Soundtrack==
{{Infobox album|  
  Name        = Luck |
  Type        = Album |
  Artist      = Salim-Sulaiman |
  Cover       = |
  Released    = 28 July 2008|
  Recorded    = | Feature film soundtrack |
  Length      = 41:30|
  Label       = T-Series|
  Producer    = Salim-Sulaiman |

  Last album  = 8 x 10 Tasveer (2009) |
  This album  = Luck (2009) |
  Next album  = Aashayein (2009) |
}}  {{Album ratings
| rev1      = Rediff Music Reviews
| rev1Score =     
|rev2=Bollywood Hungama|rev2Score=    |}}
The soundtrack album has nine songs, eight composed by duo Salim-Sulaiman and one by guest composer Hamza Mirza. The lyrics are primarily penned by Shabbir Ahmed and Anvita Dutt Guptan. The audio of the film released nationwide on 28 June 2009, a month prior to the release.      The album, notably features a song sung entirely by the lead actress Shruti Haasan who made her debut with the film.


===Track listing===
{{tracklist
| headline        = Tracklist   
| music_credits   = no
| extra_column    = Artist(s)
| title1          = Luck Aazma
| extra1          = Sukhwinder Singh, Satya Hinduja
| length1         = 4:37
| title2          = Khudaya Ve
| extra2          = Salim Merchant
| length2         = 5:23
| title3          = Jee Le
| extra3          = Shruti Pathak, Naresh Kamath
| length3         = 4:27
| title4          = Aazma&nbsp;— Luck Is The Key
| extra4          = Shruti Haasan, Clinton Cerejo
| length4         = 5:30
| title5          = Laaga Le
| extra5          = Anushka Manchanda, Robert Bob Omulo
| length5         = 4:14
| title6          = Khudaya Ve (Radio Mix)
| extra6          = Salim Merchant, Sulaiman Merchant
| length6         = 5:19
| title7          = Luck Aazma (Remix)
| extra7          = Sukhwinder Singh, Satya Hinduja
| length7         = 4:08
| title8          = Jee Le (Remix)
| extra8          = Shruti Pathak, Naresh Kamath
| length8         = 4:05
| title9          = Khudaya Ve (Remix)
| extra9          = Salim Merchant
| length9         = 3:42
}}

==Box office==
Luck was released worldwide on 1,110 prints in 1300 screens. In India, it was released on 976 prints (343 physical prints and 633 digital prints) in 1,150 screens and in 134 prints on 150 screens in overseas. The action-thriller Luck fetched a very good start at single screens of most circuits. The 12 noon shows in Uttar Pradesh|U.P., Punjab, India|Punjab, Bihar, Rajasthan, Gujarat and Maharashtra were in the 70% – 80% range. But the 3&nbsp;pm shows were almost houseful everywhere. The opening at multiplexes, however, was around 40%. Mumbai was affected due to the high tide scare on Friday but soon rose up on Saturday and Sunday. Meanwhile, the film collected DHS280,000 on its opening day in UAE.

In 4 weeks, the film collected   in India, meanwhile collecting   in the UK and   in United States. 

==References==
 

==External links==
*  

 
 
 
 
 