I'll Get Him Yet
{{Infobox film
| name           = Ill Get Him Yet
| image          = Ill Get Him Yet (1919) - 1.jpg
| caption        = scene from the film
| director       = Elmer Clifton Leigh R. Smith (assistant)
| producer       = New Art Film Company
| writer         = Harry Carr (screen story)
| starring       = Dorothy Gish
| cinematography = John Leezer Lee Garmes
| editing        =
| distributor    = Famous Players-Lasky Paramount Pictures
| released       = May 18, 1919
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} lost  1919 American comedy film starring Dorothy Gish and directed by Elmer Clifton. It was produced by D. W. Griffith under his production unit New Art Film. Paramount Pictures distributed the film.  

==Cast==
*Dorothy Gish - Susy Faraday Jones
*George Fawcett - Bradford Warrington Jones
*Richard Barthelmess - Scoop McCready
*Ralph Graves - Harold Packard
*Edward Peil, Sr. - Robert Hamilton
*Porter Strong - William Craig
*Wilbur Higby - unknown role

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 