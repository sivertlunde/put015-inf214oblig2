A Cheerful Gang Turns the Earth

{{Infobox film
| name           = A Cheerful Gang Turns the Earth
| image          = A cheerful gang turns the earth.jpg
| alt            = 
| caption        = 
| film name      = Film poster
| director       = Tetsu Maeda
| producer       = 
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = 
| cinematography = Hideo Yamamoto
| editing        = Mototaka Kusakabe
| studio         = 
| distributor    = Shochiku Company
| released       =  
| runtime        = 92 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2006 comedy heist film directed by Japanese director Tetsu Maeda, based on a novel by Kōtaro Isaka.   

==Plot==
The film is about a group of four people who form a gang to put romance back into bank robbery. The film is light-hearted in tone, and makes impressive use of Computer-generated imagery|CGI.

==Cast==
Cast members include: Takao Osawa (as Naruse, boss and "human lie detector"), Kyoka Suzuki (as Yukiko, driver and "human clock"), Shota Matsuda (as Kuon, the pickpocket), Kōichi Satō (as Kyōno, the coffee shop owner), and Koji Okura (as Jimichi). 
 
* Takao Osawa as Naruse
* Kyoka Suzuki as Yukiko
* Shota Matsuda as Kuon
* Kōichi Satō as Kyono
* Arata Furuta as Tanaka
* Ren Ohsugi as Kunimoto
* Suzuki Matsuo as Kamoda
* Ken Mitsuishi as Asakura
* Rosa Kato as Shoko
* Sakichi Sato
* Hoka Kinoshita as Hayashi
* Tomohiro Miura as Shinichi
* Yuichiro Nakayama as Akajima
* Koji Okura as Jimichi
* Eisuke Shinoi as Urushibara
* Ryo Iwamatsu
* Yasuhi Nakamura
* Shou Ohkura
* Takashi Okuda
* Kyisaku Shimada
* Kaori Sunaga
* Ellie Toyota
 

==References==
 

==External links==
*  
*   as archived October 30, 2008 at Wayback Machine
*  

 
 
 
 
 
 
 