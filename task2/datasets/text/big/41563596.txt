Holy Ghost People (2013 film)
{{Infobox film
| name           = Holy Ghost People
| image          = Holy Ghost People.jpg
| alt            =
| caption        =
| director       = Mitchell Altieri
| producer       = {{plainlist|
* Joe Egender
* Kevin Artigue
* Phil Flores
* Mitchell Altieri
* Jeffrey Allard
}}
| writer         = {{plainlist|
* Kevin Artigue
* Joe Egender
* Mitchell Altieri
* Phil Flores
}}
| starring       = {{plainlist|
* Emma Greenwell
* Brendan McCarthy
* Cameron Richardson
* Roger Aaron Brown
* Donald Patrick Harvey
* Joe Egender
}} Kevin Kerrigan
| cinematography = Amanda Treyz
| editing        = {{plainlist|
* Mitchell Altieri
* Brett Solem
}}
| studio         = {{plainlist|
* San Francisco Independent Cinema
* Found & Lost
* Indie Entertainment
* Butcher Brothers
}}
| distributor    = XLrator
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Holy Ghost People is a 2013 American psychological thriller directed by Mitchell Altieri and written by Kevin Artigue, Joe Egender, Altieri, and Phil Flores. It stars Emma Greenwell as a woman who goes in search of her missing sister, who has joined an isolated religious group.

== Plot ==
After her sister Liz goes missing, Charlotte recruits Wayne, an ex-Marine, to help her locate Liz. Their search ends at a Pentecostal church headed by the charismatic Brother Billy. As Wayne becomes attracted to the church, details emerge that Liz may have been held against her will.

== Cast ==
* Emma Greenwell as Charlotte
* Joe Egender as Brother Billy Brendan McCarthy as Wayne
* Cameron Richardson as Sister Sheila
* Roger Aaron Brown as Brother Cole
* Donald Patrick Harvey as Brother Sherman

== Production == Holy Ghost People served as partial inspiration. The film was shot in Tennessee at Camp Nakanawa after the filmmakers scouted locations in several other states.   In an interview, Altieri said he wanted to "get back to   roots and tell a gritty American tale."  Real snakes were used.   Egender prepared for his role by watching videos of snake-handlers, whom he described as "charming and funny and charismatic. They are performers." 

== Release ==
Holy Ghost People premiered at the 2013 SXSW, where it was picked up for distribution by XLrator.   XLrator released it on video-on-demand on February 18, 2014, and it had a limited theatrical release three days later.   For the theatrical release, the film was revised to address concerns raised in the festival screenings, such as excessive narration.  The film is set for an Video on demand release on Hulu on 15 March 2015, as part of XLrator Media series Macabre. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 40% of five critics gave the film a positive review; the average rating was 5.4/10.   J. Hurtado of Twitch Film wrote, "A film is only as good as its ending, and Holy Ghost People really had me going up until the ludicrous finale that conjures memories of some other great films, just rehashed and with less style."   Dennis Harvey of Variety (magazine)|Variety also criticized the "underwhelming payoff", though he said the film is atmospheric and has a "promising buildup".   Jim Harrington of the San Jose Mercury News called it "a relentless psychological thriller that features a solid script and even better acting."   Heather Wixson of Dread Central rated it 3.5/5 stars and called it "an eerie exploration of power and religion" that is "an often hypnotic and surreal journey".   Joshua Starnes of Shock Till You Drop rated it 6/10 stars and wrote, "The production is slick and the film is beautifully shot, but it’s missing that spark to become more."   Evan Dickson of Bloody Disgusting rated it 3/5 stars and wrote that the film is "a well shot piece of work with an intriguing premise and several amazing performances", but the ending "teeters dangerously close" to going off the rails.   Scott Weinberg of Fearnet called it "easily   most complete, cohesive, and compelling thriller yet."   Samuel Zimmerman of Fangoria rated it 2/5 stars and criticized the films narration, which he states wrecks viewers engagement with its redundancy.   Inkoo Kang of the Los Angeles Times wrote the film "boasts great performances and urgent, intimate camera work" but "makes nasty beasts of the very people Adair strived to humanize".  Kang concludes, "Long before its feeble, drawn-out ending, its clear no miracle can cure this films many frailties." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 