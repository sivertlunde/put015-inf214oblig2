The Ultimate Gift
{{Infobox film
| name           = The Ultimate Gift
| image          = Theatrical poster of film, The Ultimate Gift, 2007.jpg
| caption        = Theatrical release poster
| director       = Michael O. Sajbel
| producer       = Rick Eldridge Jim Van Eerden
| writer         = Novel:  
| starring       = Drew Fuller Bill Cobbs Abigail Breslin Brian Dennehy James Garner  
| runtime        = 117 minutes
| music          = Mark McKenzie
| cinematography = Brian Baugh
| release        =  
| country        = United States
| language       = English
| studio         = Fox Faith
| distributor    = 20th Century Fox Porchlight Entertainment
| budget         = $9 million
| gross          = $3,438,735   
}}
The Ultimate Gift is a 2006 American drama film directed by Michael O. Sajbel from a screenplay written by Cheryl McKay, which is in turn based on the best selling novel by Jim Stovall.

It stars Drew Fuller, Bill Cobbs, Abigail Breslin, Brian Dennehy, and James Garner, and was released on March 9, 2007 in the United States and Canada.   The films DVD sales were quite high in relation to its theatrical receipts and it continues to be a success in DVD sales and on television. 

== Plot ==
When his rich grandfather, Howard "Red" Stevens ( ), and his secretary Miss Hastings (Lee Meriwether) attempt to guide Jason along the path his grandfather wishes him to travel. But this is a hard task for the guides, because Jason is a spoiled under-achiever who never had to genuinely work. For instance, to carry out the first task he has to fly to Texas, and naturally assumes he has a first-class seat, and is annoyed when he discovers he has a coach class seat, complaining to the steward "Dont you know who I am?"

On his return after completing the first task in Texas, everything he values is suddenly taken away from him – luxury apartment, his restored muscle car, and all his money – and he is left homeless. His trendy girlfriend, Caitlin (Mircea Monroe), ditches him when his credit card is rejected at a fancy restaurant, and she is mortified when he asks her if she could pay the bill. After his mother (Donna Cherry) tells her son she cannot help him as part of the agreement, he miserably wanders the city; for the first time truly alone.

It is while sleeping in a park that he encounters a woman, Alexia (Ali Hillis), and her outspoken daughter Emily (Abigail Breslin). Jason befriends the two, but then tells them that he needs them to come to the attorneys office and confirm themselves as his "true friends" in order to pass the "gift of friends" assignment. But afterward, Jason walks away and ignores Emilys request to see him again, convincing Alexia and Emily that Jason is too self-centered to be a real friend. However, soon afterward Jason accidentally discovers that Emily is suffering from leukemia, and sees this family as a chance to develop a strong bond with someone. Indeed, the emergence of his "true" self is the key theme of the film.

From that point onwards he tries as best he can to help Emily have a great life while it lasts. Emily also engineers and encourages a romance between Jason and her mother. After Jason has worked through the twelve gifts in twelve months, he finally comes to see his grandfather as more than a dead billionaire who he believes disliked him. One of his tasks requires him to travel to Ecuador and study in a library his grandfather built to help the people there. This also forces him reluctantly to confront his resentment over the death of his father there, but he ends up making a dangerous trip into the mountains to see where it happened. Jason is informed by his guide that his father committed suicide upon becoming disillusioned with his destiny. The two find themselves held hostage by militants for several weeks, until Jason manages to ensure their mutual escape. He returns to America and discovers that Emilys condition has accelerated. As compensation for his lost time, he arranges for one of his initial supervisors, Gus the ranch-hand, to host a belated Christmas celebration at his home for them.

Upon completing his twelve tasks, and enduring this and other "harsh conditions", Jason is given a sum of $100 million to do with whatever he pleases, and all of his property is returned to him. His former girlfriend, knowing that he has regained his wealth, makes an attempt to win him back with the offer of sexual intercourse, but he disgustedly walks out on her. 

With his inheritance, Jason chooses to build a hospital, called Emilys Home (named after Emily), for patients with deadly diseases and their families, which also includes a church (knowing that Emily was often in the hospital chapel).  But then before the building begins, Emily dies.  Alexia and Jason pray for her at the chapel. After the groundbreaking for Emilys Home, he is brought back to the law firm for one more meeting. He had exceeded the expectations of his dead grandfather, and receives one final "gift" of over $2 billion, rewarding Jason not only for his completing the tasks, but for using the $100 million to help others. That night, Jason is seen sitting on a bench in the park, when Alexia joins him. He thanks her for the help that she and her daughter gave him. Then they kiss as a butterfly, representing Emily, is shown flying around them.

== Cast ==
* Drew Fuller as Jason Stevens
* Bill Cobbs as Theophilus Hamilton
* Abigail Breslin as Emily Rose
* Brian Dennehy as Gus
* James Garner as Howard "Red" Stevens

== Production and release ==
=== Production costs and underwriting ===
The film was financed with $14 million from the Stanford Financial Group,       wealth management firm based in Houston. Stanford showed the film to prospective clients at private screenings and according to an executive of the firm, they were able to track a number of multimillion-dollar relationships that resulted because of the film.   

=== Advanced screenings ===
 
Because of the philanthropic message of the film, charities in a handful of communities sponsored advance screenings of The Ultimate Gift to coincide with National Philanthropy Day 2006. Among such regional screenings was one in Richmond, Virginia, organized in partnership by The Community Foundation Serving Richmond and Central Virginia Bon Secours Health Care Foundation, Richmond Jewish Foundation and the Association of Fundraising Professionals. Professor Paul Schervish of Boston College was among those in attendance for the event. Another pre-screening took place on February 22, 2007, in Cape Girardeau, Missouri, under the auspices of the United Way of Southeast Missouri. In addition, organizations such as Bernhardt Wealth Management of McLean, Virginia have held private screenings for their clients. Bernhardt Wealth Management hosted such a screening on February 24.

== Reception ==
=== Critical response ===
The Ultimate Gift was given mixed reviews from film critics. "Though The Ultimate Gift avoids religious speechifying, its dramatically inert with flat direction" says Rotten Tomatoes. The Ultimate Gift has a 32% overall approval (59 reviews with a 5.1/10 average critic rating).  On Metacritic, the film has a user score of 8.9 out of 10 based on 25 reviews, and a critic metascore of 49 out of 100. 

The New York Times  reviewer said, "Reeking of self-righteousness and moral reprimand,   is a hairball of good-for-you filmmaking.....  he movies messages are methodically hammered home."    Christianity Today felt the film warranted 3.5 out of 4 stars and called it "lovingly crafted ... but never manages to build up much mystery, suspense, tension, or narrative steam."  Joe Leydon of Variety (magazine)|Variety magazine was favorably impressed and noted that "discussions of faith and God are fleeting, almost subliminal — without stinting on the celebration of wholesome family values."  William Arnold of the Seattle Post-Intelligencer wrote: "Its sincerity, optimism and air of open-minded tolerance go down well, and it makes a nice change-of-pace." He lauded its "tight and often compelling" screenplay, sparkling dialogue and "first-rate" production values. 

The Washington Post and The Hollywood Reporter both thought highly of Breslins performance as the young girl calling her "captivating" and referring to her "charm," respectively.     Indeed, it was a common theme for both those who recommended the film and those who did not to praise Breslins role. 

=== Box office and home media ===
The Ultimate Gift opened quietly with receipts of $1.2 million on its first weekend. As a result, many theaters dropped the film, causing a drastic slide in screens and ticket sales.  As of May 6, the movie had grossed a total of just over $3.4 million. 

DVD sales were $9.55 million in the first two months following its release. 

== Soundtrack ==
Mark McKenzie wrote the films incidental music. At the films climax, "Something Changed" is highlighted, a song composed by Contemporary Christian Music-singer Sara Groves.  Other songs include "Gotta Serve Somebody" by Bob Dylan, "The Thrill is Gone" by B.B. King, and "Crazy" by Patsy Cline. 

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  

 
 
 
 
 