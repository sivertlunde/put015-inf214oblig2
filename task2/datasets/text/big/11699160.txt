The Card Player
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Card Player
| image          = Ilcartaio.jpg
| caption        = Italian theatrical poster
| director       = Dario Argento
| producer       = Dario Argento Claudio Argento
| writer         = Jay Benedict Phoebe Scholfield
| screenplay     = Dario Argento Franco Ferrini
| story          = Dario Argento Franco Ferrini Liam Cunningham Silvio Muccino    
| music          = Claudio Simonetti 
| cinematography = Benoît Debie
| editing        = Walter Fasano
| distributor    = Medusa Produzione
| released       = 2 January 2004
| runtime        = 103 min.
| country        = Italy
| language       = Italian English
| budget         = Italian lira|€2,000,000  (estimated) 
| gross          = €2,713,882  (Italy; as of 18 January 2004) 
}}
 Liam Cunningham and is Argentos second giallo feature of the decade (following Sleepless (2001 film)|Sleepless).

The film features a brief role by Fiore Argento, the directors eldest daughter. She had previously appeared in her fathers films Phenomena (film)|Phenomena and Demons (film)|Demons.

== Plot ==
 
The film centers around a serial killer known as "The Card Player", who is kidnapping young women in Rome. Using a webcam set-up, the killer challenges the police by forcing them to play hands of Internet poker. If the police lose, the kidnapped victim is tortured and murdered on-screen. When a British tourist is among the girls murdered, policeman John Brennan (Cunningham) is assigned the case and quickly teams up with Italian detective Anna Mari (Rocca). The duo have their work cut out for them when the Police Chiefs daughter (Argento) becomes the killers latest kidnapping victim.

==Cast==
 
*Stefania Rocca as Anna Mari
*Liam Cunningham as John Brennan
*Silvio Muccino as Remo 
*Adalberto Maria Merli as the Police Commissioner
*Fiore Argento as Lucia Marini 
*Cosimo Fusco as Berardelli 
*Mia Benedetta as Francesca

== Critical reception ==
 set piece, but for those even remotely familiar with Argentos canon, theres the feeling that its all been done before – and handled with much more style and confidence." 

== References ==

 

== External links ==

*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 