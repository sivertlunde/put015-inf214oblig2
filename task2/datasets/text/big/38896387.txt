Conflict of Wings
{{Infobox film
| name           = Conflict of Wings
| image          = "Conflict_of_Wings"_(1954).jpg
| image_size     = 
| caption        = Original lobby card
| director       = John Eldridge
| producer       = Herbert Mason
| writer         = John Pudney 
| based on = novel by Don Sharp 
| narrator       = 
| starring       = John Gregson Muriel Pavlow Kieron Moore   Niall MacGinnis Philip Green Arthur Grant   Moray Grant
| editing        = Lito Carruthers
| studio         = Group 3 Films British Lion
| released       = 30 March 1954
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Conflict of Wings is a 1954 British drama film directed by John Eldridge and starring John Gregson, Muriel Pavlow and Kieron Moore.  It is based on a novel by Don Sharp.  The film was made at Beaconsfield Film Studios and on location in Norfolk. The film sets were designed by art director Ray Simm. The film was re-titled for the American market as "Fuss Over Feathers".

==Synopsis==
A small Norfolk village is outraged when it is discovered that the local RAF base plans to use a nearby island as a practice target before going on service overseas. A struggle of wills begins between the military and the villagers, who resort to a variety of ways to prevent damage to the historic island.

==Cast==
* John Gregson as Cpl. Bill Morris 
* Muriel Pavlow as Sally 
* Kieron Moore as Squadron Leader Parsons
* Niall MacGinnis as Harry Tilney
* Harry Fowler as Leading Aircraftman Buster 
* Guy Middleton as Adjutant
* Sheila Sweet as Fanny Bates 
* Campbell Singer as Flight Sgt. Campbell
* Frederick Piper as Joe Bates
* Russell Napier as Wing Cmdr. Rogers 
* Bartlett Mullins as Soapy
* Edwin Richfield as Smother Brooks 
* Margaret Withers as Mrs Tilney 
* Howard Connell as Flight Officer, Flying Control 
* Beryl Cooke as Miss Nelson 
* Tony Doonan as Range Corporal John Gale as Range Leading Aircraftman 
* Brian Harding as 1st Pilot 
* Barbara Hicks as Mrs. Thompson 
* Humphrey Lestocq as Squadron Leader Davidson 
* Charles Lloyd-Pack as Bookie 
* William Mervyn as Mr Wentworth 
* Brian Moorehead as 3rd Pilot
* Hugh Moxey as Mr. Ruddle
* Dorothea Rundle as Mrs. Trotter
* Harold Siddons as Flight Lt. Edwards
* David Spenser as Corporal, Flying Control
* Peter Swanwick Sergeant, Working Party
* Guy Verney as 2nd Pilot
* Gwenda Wilson as Miss Flew George Woodbridge as Man in charge of Pumping-Station

==References==
 

==Bibliography==
* Harper, Sue & Porter, Vincent. British Cinema of the 1950s: The Decline of Deference. Oxford University Press, 2003.

==External links==
*  BFI
 

 
 
 
 
 
 
 
 
 
 


 