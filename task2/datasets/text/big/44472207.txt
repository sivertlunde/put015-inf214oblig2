Absolute Quiet
{{Infobox film
| name           = Absolute Quiet
| image          = 
| alt            = 
| caption        = 
| director       = George B. Seitz
| producer       = John W. Considine Jr. 	
| screenplay     = Harry Clork 
| story          = George F. Worts 
| starring       = Lionel Atwill Irene Hervey Raymond Walburn Stuart Erwin Ann Loring Louis Hayward
| music          = Franz Waxman
| cinematography = Lester White 
| editing        = Conrad A. Nervig
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Absolute Quiet is a 1936 American drama film directed by George B. Seitz and written by Harry Clork. The film stars Lionel Atwill, Irene Hervey, Raymond Walburn, Stuart Erwin, Ann Loring and Louis Hayward. The film was released on April 24, 1936, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Lionel Atwill as G.A. Axton
*Irene Hervey as Laura Tait
*Raymond Walburn as Governor Pruden
*Stuart Erwin as Chubby Rudd
*Ann Loring as Zelda Tadema
*Louis Hayward as Gregory Bengard
*Wallace Ford as Jack
*Bernadene Hayes as Judy
*Robert Gleckler as Jasper Cowdray		
*Harvey Stephens as Barney Tait
*J. Carrol Naish as Pedro Matt Moore as Pilot Robert Livingston as Co-Pilot 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 