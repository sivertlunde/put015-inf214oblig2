Fortune Is a Woman
{{Infobox film
| name           = Fortune Is a Woman
| image          = "Fortune_is_a_Woman"_(1957).jpg
| image_size     =
| caption        = British quad poster
| director       = Sidney Gilliat
| producer       = Sidney Gilliat   Frank Launder
| writer         = Winston Graham (novel)   Val Valentine    Sidney Gilliat   Frank Launder
| narrator       =
| music          = William Alwyn Gerald Gibbs
| editing        = Geoffrey Foot
| starring       = Jack Hawkins   Arlene Dahl   Dennis Price
| distributor    = Columbia Pictures
| released       = 15 April 1957
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
Fortune Is a Woman is a 1957 British crime film directed by Sidney Gilliat and starring Jack Hawkins, Arlene Dahl and Dennis Price. Its plot concerns an attempted insurance fraud goes badly wrong.  In the United States, it was released as She Played With Fire.

==Plot==
London insurance investigator Oliver Branwell goes to Louis Manor to probe a recent fire. Tracey Moreton lives there with his wife Sarah and his mother. He introduces them to Oliver, as well as his neighbor and cousin, Clive, and describes a valuable painting that was lost in the blaze. Tracey isnt aware that Sarah and Oliver were once romantically involved.

Months later, on another case, Oliver meets a woman, Vere Litchen, who has a painting that appears to be the one allegedly destroyed. The next time he and Sarah speak, she asks questions about  the best way to start a fire.

When he later sneaks into the manor, Oliver finds the dead body of Tracey as well as flames that are about to engulf the house. Sarah is beneficiary of her husbands insurance policy but Oliver remains suspicious of her involvement.

Veres rich fiance, Croft, who gave her the painting, is shown a photo of Sarah and denies she was the woman who sold it to him. A relieved Oliver proposes to Sarah and they leave on a honeymoon. Now the police begin to consider him a suspect.

Blackmailed by a shadowy figure, Oliver and Sarah follow him and discover that Clive was the schemes mastermind. And that the dead mans mother, Mrs. Moreton, had accidentally caused Traceys death, believing that her son was about to start a second fire.

==Cast==
* Jack Hawkins - Oliver Branwell
* Arlene Dahl - Sarah Moreton Branwell
* Dennis Price - Tracey Moreton
* Violet Farebrother - Mrs Moreton Ian Hunter - Clive Fisher
* Malcolm Keen - Old Abercrombie
* Geoffrey Keen - Young Abercrombie
* Patrick Holt - Fred Connor John Robinson - Berkeley Reckitt
* Michael Goodliffe - Detective Inspector Barnes
* Martin Lane - Detective Constable Watson
* Bernard Miles - Mr Jerome
* Christopher Lee - Charles Highbury
* Greta Gynt - Vere Litchen John Phillips - Willis Croft
* Patricia Marmont - Ambrosine
* Rosalind Knight - Minor role

==References==
 

==External links==
 

 
 
 
 
 
 


 