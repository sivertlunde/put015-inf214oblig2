Schizo (1976 film)
{{Infobox film
| name           = Schizo
| image          = "Schizo" U.S.film poster.jpg
| caption        = U.S. poster (1977) Pete Walker Pete Walker David McGillivray John Fraser
| music          = Stanley Myers
| cinematography =  Peter Jessop
| editing        = 
| distributor    =
| released       = 11 November 1976 (UK)
| runtime        = 109 min
| country        = United Kingdom
| language       = English
| budget         = 
}} Pete Walker and starring Lynne Frederick.  

==Plot==
When figure skater Samantha Gray (Lynne Frederick) married to Englands businessman Alan Falconer (John Leyton), a sinister man from her past; William Haskin (Jack Watson)started stalking her, making Samantha depressed. Then the murder start. 

==Critical reception==
 Time Out wrote: "Walker and writer David McGillivrays most ambitious project to date attempts to shake off the low-budget horror/exploitation tag with a move into more up-market psychological suspense. If the formula is threadworn - a trail of victimisation, sexual paranoia, and murder in the wake of the heroines wedding - at least some effort is made to locate it (rich middle class London). But things collapse disastrously in the second half. Caught between sending itself up and taking itself seriously, the film ends closer to the silliness of Francis Durbridge than to the menace of Alfred Hitchcock." 

==Cast==
*Lynne Frederick	 ...	Samantha Gray
*John Leyton	... 	Alan Falconer
*Stephanie Beacham	... 	Beth John Fraser	... 	Leonard Hawthorne Jack Watson	... 	William Haskin
*Queenie Watts	... 	Mrs. Wallace
*Trisha Mortimer	        ... 	Joy
*John McEnery	... 	Stephens (uncredited) Paul Alexander	... 	Peter McAllister
*Robert Mill	... 	Maitre Diana King	... 	Mrs. Falconer
*Colin Jeavons	... 	Commissionner
*Victor Winding	... 	Sergent Raymond Bowers	... 	Manager
*Pearl Hackney	        ... 	Lady at Seance
*Terry Duggan	... 	Editor
*Lindsay Campbell	... 	Falconer
*Wendy Gilmore	... 	Samanthas Mother
*Primi Townsend	... 	Secretary
*Victoria Allum	        ... 	Samantha as Child

==Production==
The film was made in London in 1976.

== References ==

 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 