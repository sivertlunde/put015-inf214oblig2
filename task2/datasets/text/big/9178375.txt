Barnacle Bill (1941 film)
{{Infobox film
| name           = Barnacle Bill
| image size     =
| image	         = Barnacle Bill FilmPoster.jpeg
| caption        =
| director       = Richard Thorpe
| producer       = Milton H. Bren
| writer         = Jack Jevne Hugo Butler
| story          = Jack Jevne
| narrator       =
| starring       = Wallace Beery Marjorie Main
| music          = Bronisław Kaper Lennie Hayton
| cinematography = Clyde De Vinna
| editing        = Frank Hull
| distributor    = Metro-Goldwyn-Mayer
| released       = April 7, 1941
| runtime        = 90 minutes
| country        = United States English
| budget         =
| preceded by    =
| followed by    =
}}

Barnacle Bill is a 1941 feature film starring Wallace Beery and Marjorie Main.  The screen comedy was directed by Richard Thorpe. Barnacle Bill was the second of seven MGM films costarring Wallace Beery and Marjorie Main. 

==Plot summary== fisherman Bill San Pedro harbor, aggravating ship chandler Pop Cavendish (Donald Meek) and Pops spinster daughter Marge (Marjorie Main), who would like to marry Bill even though he has welched on paying his debts for years. Pop tries to have Bills boat attachment (law)|attached, but cannot because Bill has craftily listed the boats ownership in the name of his daughter Virginia, whom he has not seen since she was a baby. Meanwhile, reefer ship-owner John Kelly (Barton MacLane) has a monopoly and intimidates local fishermen into accepting less than market value for their fish. Marge tells Bill he is just the man to stand up to Kelly, but Bill would rather fish for swordfish, which bring a higher price (and thus require less work to earn beer money) with his partner, Pico (Leo Carrillo). His daughter Virginia (Virginia Weidler), now twelve, is brought to meet Bill by her Aunt Letty (Sara Haden) and asks to stay with him, even though Letty thinks he is an unfit father. Bill likes Virginia, but doesnt want the responsibility of raising a child, so he convinces Marge to let her live ashore with her.
 Gloucester fishing scuttles his boat after he passes out.
 stows away as cook to keep Bill honest. Bill arrives at the fishing grounds as Kelly is again trying to intimidate the fishermen and gives Kelly his money back. Kelly and his gang sneak aboard the fish-laden Were Here to scuttle her, but Pop discovers the invaders. Bills makeshift crew capture the gang and put them to work to successfully weather a bad storm. Virginia and the telescope are waiting back at San Pedro. where Bill and a suddenly bashful Marge wed.

==Cast==
* Wallace Beery as Bill Johansen
* Marjorie Main as Marge Cavendish
* Leo Carrillo as Pico Rodriguez
* Virginia Weidler as Virginia Johansen
* Donald Meek as Pop Cavendish
* Barton MacLane as John Kelly
* Connie Gilchrist as Mamie
* Sara Haden as Aunt Letty William Edmunds as Joe Petillo 
* Don Terry as Dixon 
* Alec Craig as MacDonald Charles Lane as Auctioneer (uncredited)

==Notes==
Wallace Beery and Marjorie Main costarred in the following seven films together: 
* Wyoming (1940 film)|Wyoming (1940)
* Barnacle Bill (1941)
* Jackass Mail (1942)
* The Bugle Sounds (1942)
* Rationing (1944 film)|Rationing (1944) Bad Bascomb (1946) Big Jack (1949)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 