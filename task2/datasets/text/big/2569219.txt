Cover Girl (film)
{{Infobox film
| name           = Cover Girl
| image          = Covergirlmp.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Charles Vidor
| producer       = Arthur Schwartz
| writer         = Story: Erwin S. Gelsey Adaptation: Marion Parsonnet Paul Gangelin Screenplay: Virginia Van Upp Uncredited: John H. Kafkr
| starring       = Rita Hayworth Gene Kelly
| music          = Score:   Henry E. Pether Songs-Lyrics Ira Gershwin E.Y. Harburg Fred W. Leigh
| cinematography = Allen M. Davey Rudolph Maté
| editing        = Viola Lawrence
| distributor    = Columbia Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Cover Girl is a 1944 American Technicolor musical film starring Rita Hayworth and Gene Kelly.  The film tells the story of a chorus girl given a chance at stardom when she is offered an opportunity to be a highly paid cover girl.  The film was directed by Charles Vidor, and was one of the most popular musicals of the war years.

Primarily a showcase for Rita Hayworth, the film has lavish modern and 1890s costumes, eight dance routines for Hayworth, and songs by Jerome Kern and Ira Gershwin, including the classic "Long Ago (and Far Away)".

==Plot==
Rusty (Rita Hayworth), a chorus girl working at a nightclub run by her boyfriend Danny McGuire (Gene Kelly), is given a chance for stardom by the wealthy magazine editor John Coudair (Otto Kruger), who years earlier had been in love with her grandmother, Maribelle Hicks. Offered an opportunity to be a highly paid cover girl, Rusty would faithfully remain with her nightclub act if only Danny would ask her. He doesnt want to stand in her way, so he picks an argument to send her packing. Rusty becomes a star on Broadway after appearing in a musical produced by Coudairs wealthy friend, Noel Wheaton (Lee Bowman), and decides to get married to Wheaton. At the last second she leaves the wedding and reunites with Danny. 

== Cast ==
*Rita Hayworth as Rusty Parker and Maribelle Hicks
*Gene Kelly as Danny McGuire
*Phil Silvers as Genius
*Otto Kruger as John Coudair
*Eve Arden as Cornelia "Stonewall" Jackson
*Lee Bowman as Noel Wheaton
*Jess Barker as young John Coudair
*Edward Brophy as Joe, Oyster Cook

Cast notes
*The film features cameo appearances by Jinx Falkenburg and Anita Colby as themselves, and an appearance by Shelley Winters, early in her career, as one of the young autograph hounds. 
*In one of Hollywoods most unique reprise roles, Kelly played Danny McGuire again—36 years later—in 1980s Xanadu (film)|Xanadu.

==Musical numbers==
Cover Girl marked the first film collaboration of Jerome Kern and Ira Gershwin. 

*"The Show Must Go On" (Kern - music, Gershwin - lyrics)
*"Whos Complaining?" (Kern, Gershwin)
*"Sure Thing" (Kern, Gershwin)
*"Make Way For Tomorrow" (Kern, Gershwin, E.Y. Harburg - lyrics)
*"Put Me to the Test" (Kern, Gershwin)
*"Long Ago (and Far Away)" (Kern, Gershwin)
*"Poor John" (Henry E. Pether - music, Fred W. Leigh -lyrics)
*"Alter-Ego Dance" (Kern)
*"Cover Girl (That Girl on the Cover)" (Kern, Gershwin)

==Production==
Columbia Pictures  originally wanted to used Warner Bros. star Dennis Morgan for Cover Girl, but when Kellys project at MGM, Dragon Seed, was postponed, MGM extended their loan of Kelly to Columbia, allowing this film to be made with him.  Columbias production head, Harry Cohn, was initially opposed to having Kelly do the film, but producer Schwartz nevertheless obtained him, promising Kelly that he would be able to choreograph, which MGM had not allowed him to do. 

Columbia gave Kelly almost complete control over the making of this film, and many of his ideas contributed to its lasting success. He removed several of the soundstage walls so that he, Hayworth, and Silvers could dance along an entire street in one take. He also used trick photography so that he could dance with his own reflection in the sequence "Alter-Ego Dance", achieved using   

The film was Hayworths fourth musical: the first two she had done opposite Fred Astaire.  Hayworths singing voice was dubbed by Martha Mears. 

Cover Girl was Columbias first   

==Awards and honors== Academy Award best musical Best Art Best Cinematography, Best Original Best Sound, Recording (John Livadary).   

==References==
Notes
 

==External links==
* 
* 
* 
* 
* 
*   by Ned Scott

 

 

 
 
 
 
 
 
 
 
 
 
 
 