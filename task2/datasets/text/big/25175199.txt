Red Heat (1985 film)
{{Infobox film
|name=Red Heat
|image=Red Heat film.jpg
|director=Robert Collector
|writer=Robert Collector Gary Drucker 
|producer=
|starring=Linda Blair Sylvia Kristel
|released= 
|language=English
|country= gross = 37,933 admissions (France) 

}}

Red Heat is a 1985 women in prison film starring Linda Blair and Sylvia Kristel. 

==Synopsis==
Christine Carlson (Linda Blair), an American college student, travels to West Germany to visit her fiancee Mike (William Ostrander), who is serving in the US Army there.  She tries to convince him to marry her promptly, but he chooses to delay marriage in order to re-enlist.  Distraught by Mikes decision, Christine takes a late-night walk where she witnesses a kidnapping by the East German Stasi and gets kidnapped herself as well.  She is transported to the East Zone, where she is brutally interrogated by the Stasi, forced to admit to false charges of espionage, and thrown into a womens prison with the common criminals, including the gang leader Sofia (Sylvia Kristel), who is the prisoners "top bitch" and has de facto control of the entire prison population.  Sofia takes pleasure in brutally tormenting and harassing Christine, until the latter loses her patience and fights Sofia in a no-holds-barred brawl.  Meanwhile, Mike is determined to free his beloved, and tries to get the US Army and the West German BND to help him.  At first they refuse, but ultimately they reluctantly go along with his plans and help him free Christine.

==Cast==
Linda Blair—Christine Carlson 
Sylvia Kristel—Sofia  
Sue Kiel—Hedda  
William Ostrander—Mike  
Elisabeth Volkmann—Einbeck  
 —Ernst  
Herb Andress—Werner  
Barbara Spitz—Meg  
Kati Marothy—Barbara  
Dagmar Schwarz—Lillian  
Sissy Weiner—Uta  
Norbert Blecha—Kurt  
Sonja Martin—Evelyn  
 —Eva  
John Brett—Roger 
Michael Troy—Howard  
 —Lecture  
 —Limmer  
Fritz von Friedl—BND Agent

==Notes== the 1988 movie of the same title starring Arnold Schwarzenegger and Jim Belushi.

==References==
 

==External links==
* 

 
 
 
 


 