Robin Hood of Monterey
{{Infobox film
| name =  Robin Hood of Monterey
| image =
| image_size =
| caption =
| director = Christy Cabanne 
| producer =Jeffrey Bernerd 
| writer =  Bennett Cohen    Gilbert Roland
| narrator =
| starring = Gilbert Roland   Chris-Pin Martin   Evelyn Brent
| music = Edward J. Kay 
| cinematography = William A. Sickner 
| editing = Roy V. Livingston 
| studio = Monogram Pictures
| distributor = Monogram Pictures 
| released = September 6, 1947
| runtime = 55 minutes
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Robin Hood of Monterey is a 1947 American adventure film directed by Christy Cabanne and starring Gilbert Roland, Chris-Pin Martin and Evelyn Brent. The film was part of the long-running Cisco Kid series produced by Monogram Pictures.  The Cisco Kid travels to Monterey, California (then part of Mexico), where he clears the son of an old friend of a charge of murder.

==Cast==
*  Gilbert Roland as The Cisco Kid  
* Chris-Pin Martin as Pancho  
* Evelyn Brent as Maria Belmonte Sanchez  
* Jack La Rue as Don Ricardo Gonzales  
* Pedro de Cordoba as Don Carlos Belmonte 
* Donna Martell as Lolita 
* Travis Kent as Eduardo Belmonte  
* Thornton Edwards as El Capitan  
* Nestor Paiva as The Alcalde  
* Ernie Adams as Pablo  Fred Cordova as Henchman   Ray Jones as Henchman 
* Bob McElroy as Rurale  
* Alex Montoya as Juan  
* George Navarro as Card player 
* Artie Ortego as Alcaldes carriage driver  
* Julian Rivero as Doctor Martinez  
* Felipe Turich as Jose - Sentry / Servant Bob Woodward as Pedro

==References==
 

==Bibliography==
* Baugh, Scott L. Latino American Cinema: An Encyclopedia of Movies, Stars, Concepts, and Trends. ABC-CLIO, 2012.

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 ]

 