The Butcher's Wife
{{Infobox film
| name = The Butchers Wife
| image = ButcherWifePoster.jpg
| caption = Theatrical poster Terry Hughes
| producer = Lauren Lloyd Wallis Nicita
| writer = Ezra Litwak Marjorie Schwartz
| starring = Demi Moore Jeff Daniels
| music = Michael Gore Steven Jae Johnson
| cinematography = Frank Tidy
| editing = Donn Cambern
| distributor = Paramount Pictures
| released =  
| runtime = 107 minutes
| country = United States
| language = English
| gross = $9,689,816
}}
The Butchers Wife is a 1991 romantic comedy film, in which a clairvoyant woman (Demi Moore) thinks that shes met her future husband (George Dzundza), who she has seen in her dreams and is a butcher in New York. They marry and move to the city, where her powers tend to influence everyone she meets while working in the shop. Through her advice, she helps others and eventually finds the true man of her dreams.

==Plot==
As a clairvoyant, Marina awaits signs from beyond that her true love, whoever he may be, is waiting for her, somewhere. When New York butcher Leo Lemke shows up on the tiny North Carolina island of Ocracoke, where Marina lives, she is convinced that he is the man predestined to be her husband. After the wedding, Marina moves into Leos blue-collar neighborhood, where she successfully commiserates with such eccentrics as withdrawn teenager Eugene, frustrated singer Stella Keefover, unlucky-in-love actress Robyn Graves, over analytical psychiatrist Dr. Alex Tremor, and frustrated dress shop clerk Grace. But what Marina fails to grasp about her powers is that she can see the future of strangers far more clearly than her own, and love is unpredictable no matter how many ways you have to look for it.

The film makes use of several phenomena that can be described as occult portents that meeting a love match is imminent or occult tools to help strengthen, seal or bring about love, luck and happiness.  These include the sudden "finding" of a ring that would serve as a wedding band, falling stars with twin tails, zig-zagged rainbows and found objects symbolizing a change in the finders path that will cause it to cross with their beloved.  It also popularizes, as the character of Alex Tremor calls it, "a corruption" of a section of Symposium_(Plato)|Platos Symposium regarding soul mates, referred to in the movie as "split aparts".  In one scene, Dr. Tremor notes an aspect of Marinas visions, and Marina says:  "Women have been burned for less."  This is a reference, of course, to the famous witch burnings in Europe and America between the 15th and 19th centuries:  the most famous of which being the Salem Witch Trials.  Lastly, it also references common-use bastardizations of voodoo practices, such as "mojo" bags (or gris gris) and the use of chickens or toads.

==Cast==
* Demi Moore as Marina Lemke
* Jeff Daniels as Dr. Alex Tremor
* George Dzundza as Leo Lemke
* Mary Steenburgen as Stella Keefover
* Frances McDormand as Grace
* Margaret Colin as Robyn Graves
* Max Perlich as Eugene
* Miriam Margolyes as Gina
* Christopher Durang as Mr. Liddle
* Luis Avalos as Luis
* Helen Hanft as Molly
* Elizabeth Lawrence as Grammy DArbo
* Diane Salinger as Trendoid

==Formats==
It was released on VHS and later on DVD, then was out of print on both formats.  To date no DVD re-release date has been announced. Currently available on Netflix Instant Streaming.

==Reception==
The film holds a 21% rotten rating at website Rotten Tomatoes. 

== Awards and nominations ==

=== 1991 Golden Raspberry Awards ===
One nomination:
* Worst Actress (Demi Moore) 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 