Eddie and the Cruisers II: Eddie Lives!
{{Infobox Film
| name = Eddie and the Cruisers II: Eddie Lives!
| image = EddieAndTheCruisers.jpg
| caption = VHS cover art
| director = Jean-Claude Lord
| producer = William Stuart Denis Heroux Stephane Reichel
| writer = Charles Zev Cohen Rick Doehring
| starring = Michael Paré Marina Orsini Bernie Coulson Matthew Laurance John Cafferty Marty Simon Kenny Vance Leon Aronson
| cinematography = Rene Verzier
| editing = 
| distributor = Scotti Brothers Pictures
| released = August 18, 1989
| runtime = 104 min.
| country = Canada/United States
| language = English
}}
 cult classic Eddie and the Cruisers, and despite being a failure at the box office, has since joined its predecessor as a cult favorite. It is directed by Jean-Claude Lord, and based on literary characters created by author P. F. Kluge. Michael Paré and Matthew Laurance reprise their roles as Eddie Wilson and Sal Amato, respectively. The film is marketed with the tagline "The legend. The music. The man."

Director Lord and several members of the films supporting cast (Marina Orsini, Vlasta Vrána, and Mark Brennan) had previously worked together on the French-Canadian television series Lance et Compte. Additionally, cast members Harvey Atkin and Kate Lynch had earlier co-starred in the 1979 film Meatballs (film)|Meatballs.

==Plot summary==
Satin Records, which twenty years prior had rejected rock and roll band Eddie and the Cruisers last album A Season in Hell, launches an "Eddie Lives!" campaign to make more money from his image in a publicity stunt, despite them not believing he is alive. The record label re-releases the bands first album, which becomes an even bigger hit than it had on its first release. As seen in the first film, the "lost recordings" from Season in Hell are released and become yet another hit album. In fact, Eddie Wilson is living, and he had simply slipped away following the car crash in which he was believed killed. By that stage in his life, he simply wanted to leave music behind him. The newly generated constant spotlight surrounding his supposed death angers the reclusive rocker, by now living in Canada as a construction worker under the name Joe West.

When Eddie gets involved in a struggling street band, its not long before his passion for music—not to mention his desperate anger—resurfaces, and a decision must be made to determine Eddies fate once and for all. Eddie ("Joe West") interacts with a band at a local bar and challenges their ability to play.  He accepts an invitation to play and quickly dazzles the audience and the band.  Eddie abruptly leaves the stage after feeling the presence of Wendall Newton while Hilton plays a brief sax solo.  After unrelenting attempts by local guitarist Rick Diesel, Eddie finally begins to play again. The two circulate throughout Montreal, selecting musicians for a new band, Rock Solid. Despite Eddie fighting the progress of the band every step of the way, Rock Solid begins to tour and wows audiences everywhere. Their popularity closely mirrors Eddies former success with Eddie and the Cruisers, and he begins to see the similarities.
 flashbacks to his former life. His anger and his pride peak when lead guitarist Rick Diesel calls a woman whom they had met at gig. She wants the band to audition for an upcoming music festival. Eddies anger, for a second time, is soothed by sax player Hilton Overstreets cool demeanor and Ricks fast talking. Eddie caves into the bands desire to do the largely public venue that Eddie fears gravely. Eddie, as usual, agrees under the condition that they lock themselves away in a cabin, where there are "no distractions" so that they can get back to the music. The band is beginning to lose its momentum, and Eddies wrath is at its peak. After breaking his guitar into pieces and storming off, Eddie is confronted again by Hilton, except this time Eddie is called out by his real name. In what is probably one of the movies defining moments, Hilton says, "I knew who you were from the moment I heard you play. The way a man plays—hes born with it; like fingerprints," suggesting that Eddie has no choice but to be the musician Eddie Wilson and not Joe West the construction worker.

All the while, Satin Records has been upping the ante for anyone who can provide proof that Eddie lives. An expert, a few scenes before, had proved that the legendary Bo Diddley had played on the mystery tapes before the death of Cruisers sax player Wendell Newton, as well as Eddies unfortunate meeting with the river. However, having been in seclusion for over a month, Rick does not know about the mounting tension surrounding the mystery of Eddies whereabouts, and decides to send a tape to Satin Records along with a note that contains the line, "I have a band and my singer sure sounds a lot like Eddie Wilson."

After successfully auditioning for the Music Festival, Eddie suddenly has doubts. His life, his face, is about to be made very public. In desperation, he turns to his longtime friend and confidant Sal Amato. On a beach in Jersey, Sal and Eddie quickly and angrily hash out twenty years of grief. Sal, being the type of man who he is, just has to know where these "so-called-mystery tapes were recorded". Eddie takes Sal back to the old abandoned church where in 1963, he and former sax player Wendell Newton had a jam session with a large group of black musicians, Bo Diddley being the most well known name among them. Eddie confesses that the whole affair made him feel inadequate due to what appeared to be a lukewarm reaction from the people whom Eddie had aspired to be like. Sal, in one of his deeper moments, reveals a simple truth to Eddie: in short, its not about setting the world on fire, its about playing the music. Armed with that sentiment in mind, Eddie returns to Montreal, primed, shaved and ready to go.

However, as fate would have it, Ricks earlier moment of vanity has paid off, and the two top dogs of Satin Records appear just before Rock Solid is about to take the stage. Being confronted by one of the two men who once told Eddie his music was not fit to be released, sends Eddie into a momentary lapse of judgement where he goes looking for another bridge to drive off. His girlfriend, Diane, confronts him and convinces him that even though the world will know who Joe West really is tomorrow, Eddie still has today all to himself. Eddie decides to take the stage once again, and after getting pumped up by the first song, "Running Through the Fire", Eddie introduces his band once more, except this time, Eddie willingly proclaims, "and me. Im Eddie Wilson."

==Cast==
* Michael Paré - Eddie Wilson / Joe West
*  Michael "Tunes" Antunes as Wendell Newton
* Marina Orsini - Dianne Armani
* Bernie Coulson - Rick Diesel
* Matthew Laurance - Sal Amato
* Anthony Sherwood - Hilton Overstreet
* David Matheson - Stewart Fairbanks
* Mark Holmes - Quinn Quinley
* Harvey Atkin - Lew Eisen
* Paul Markle - Sexy Charlie Tanzie

Special guest appearances by:
* Larry King
* Martha Quinn
* Bo Diddley

==Filming==
The film was quickly made in just over 30 days from March to mid-April 1989. The concert scenes for the finale and Eddies big return onstage were filmed on April 24, 1989 in Las Vegas Paradise, Nevada at the Thomas & Mack Center
in between sets of Bon Jovis headlining "New Jersey" tour (after Skid Rows opening set) concert.

==Reception==
The film was released on only 402 screens nationwide on the weekend of August 18, 1989, just grossing $536,508. It was quickly pulled from the theaters. Once again, as with the original film, critics heavily panned the film and the performances of Pare and Orsini, and also was very critical of the producers (the Scotti Brothers) for being more concerned about selling soundtrack albums for the film than promoting the film itself. 

==Soundtrack release==
The soundtrack was released on December 13, 1990.

==Video releases==
The film was released on VHS and videodisc in the US on January 18, 1990. The DVD was released in 1998, then in 2008 as a combo pack with the first film. 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 