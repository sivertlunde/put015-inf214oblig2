Aszparuh
{{Infobox Film
| name=Aszparuh
| image=681 glory of khan vhs-us.jpg
| director=Ludmil Staikov
| writer= Vera Mutafchieva
| starring=Stoyko Peev   Antoniy Genov   Vanya Tsvetkova   Đoko Rosić   Anya Pencheva   Petar Slabakov
| producer=
| distributor=
| released= 
| runtime=92 minutes
| country=Bulgaria
| language=Bulgarian
| budget=
}}
 Khan Asparuh medieval Bulgarian Best Foreign Language Film at the 55th Academy Awards, but was not accepted as a nominee. 

The director is Ludmil Staikov, with Stoyko Peev playing Asparuh, Antoniy Genov playing Velisarius and Vanya Tsvetkova Pagane.

In 1984 the film was internationally released as 681 AD: The Glory of Khan in a 92-minute English-language edited version, down from the original 4½ hours. The version has received much criticism for not presenting the entire plot well and focusing on certain aspects, thus changing the whole feel of the production.

==Cast==
* Stoyko Peev as Khan Asparukh of Bulgaria
* Antony Genov as Velisarius
* Vassil Mihajlov as Khan Kubrat
* Vania Tzvetkova as Pagane
* Stefan Getsov as The High Priest of Tangra
* Georgi Cherkelov as  Velisarius father
* Iossif Surchadzhiev as Constantine IV

==See also==
* List of historical drama films
* List of submissions to the 55th Academy Awards for Best Foreign Language Film
* List of Bulgarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 

 
 