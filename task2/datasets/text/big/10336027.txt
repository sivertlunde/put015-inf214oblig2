The Swan Princess: Escape from Castle Mountain
 
{{Infobox film
| name = The Swan Princess: Escape from Castle Mountain
| image = The Swan Princess II- Escape from Castle Mountain VideoCover.jpeg Richard Rich
| producer = Jared F. Brown Richard Rich
| screenplay = Brian Nissen
| story = Brian Nissen Richard Rich
| starring = Michelle Nicastro Douglas Sills Jake Williamson
| music = Lex de Azevedo
| cinematography = Tom Sheppard
| editing = James Koford
| studio = Nest Family Entertainment Rich Animation Studios
| distributor = Warner Bros. Family Entertainment
| released =  
| runtime = 71 minutes
| country = United States
| language = English
| budget = 
| gross = $273,644 (USA)
}}

The Swan Princess: Escape from Castle Mountain (alternatively known as The Swan Princess: The Secret of the Castle in European countries) is an 1997 American animated musical-fantasy film and the   (1998),   (2014).

Most of the main voice cast of the previous film has been replaced, save for Michelle Nicastro, who reprises her role as Odette and this time did her own singing, and Steve Vinovich, who reprises his role as Puffin. This sequel features recycled footage from the previous film, most notably in the flashbacks during "The Magic of Love" sequence. Musically, the film also samples the soundtrack of the previous film, such as during the scene where Clavius is changing Queen Uberta into various animals; the instrumental of "Practice, Practice, Practice" can be heard. Overseas production for all three films was by Hanho Heung-Up Co., Seoul, South Korea.

==Plot== wizard Clavius. Clavius was the partner of Rothbart, villain of the previous film; the pair conquered the Forbidden Arts together until Rothbart drove Clavius underground after betraying their partnership. Clavius now wants to claim the magical orb of the Forbidden Arts that is located somewhere in Swan Lake castle, which has become the new home of Derek and Odette. Clavius has Knuckles to perform acts of vandalism in the kingdom that keep Derek busy and makes him neglect both Odette and Uberta.

On Ubertas birthday, she is abducted by Clavius, who wants to use her as leverage. When Derek sets out to rescue his mother, Clavius sneaks into Swan Lake castle where he locks Odette in a tower and then goes after the orb himself. Bridget, who was once Rothbarts accomplice but has joined the side of good, recognizes Clavius and knows that he is after the Forbidden Arts. She takes Speed, Puffin, and Jean-Bob into the catacombs under the castle where they find the orb first. After claiming the orb, they race back upstairs and free Odette. Odette knows now that Derek is heading into a trap, but Puffin cannot fly because of his tail is injured, so she convinces Bridget to use the orb to change her into a swan. Once transformed, Odette flies off to warn Derek. Clavius stumbles upon the remaining group and a chase ensues throughout the castle. Clavius eventually obtains the orb, and locks Bridget and the animals in the watery dungeon, although they later escape.

Elsewhere, Odette reaches Derek in time to save him from a pit of quicksand. Racing back to the castle, they see Clavius escaping in his hot-air balloon, from which Speed, Puffin and Jean-Bob are secretly clinging to in the hopes of being able to regain the orb. Derek and Odette follow the balloon to Clavius volcano lair. Knuckles tries to stop them, and after a fight, Knuckles falls into the lava pool beneath the volcano.

Clavius celebrates his regaining the Forbidden Arts again, but Derek arrives and the animals free Uberta from her prison. During the fight, Jean-Bob jumps on Clavius head to stop him from delivering a killing blow to Derek, and Jean-Bob is killed when he is thrown off. Derek gets his hands on the orb, and the group rushes to escape in Clavius balloon. Clavius tries to stop them, and during the struggle the orb is dropped, which causes a massive explosion and volcanic eruption which kills Clavius while the others escape.

Later, everyone is at Swan Lake, waiting for the moon to rise on Odette, who is waiting on the surface with Jean-Bob on her wing. When the moonlight touches Odette, she is transformed back to her human form and Jean-Bob is revived. The gang celebrate their victory and Ubertas fiftieth birthday. The next day, a royal guest arrives at the castle, but Derek asks Rogers to take care of it, as he wishes to spend the day with Odette. The two kiss, enjoying their time together alone at last.

==Cast==
* Michelle Nicastro - Princess Odette
* Douglas Sills - Prince Derek
* Jake Williamson - Clavius
** Michael Lanning provided the singing voice for You Gotta Love It but not the ending rap.
* Christy Landers - Queen Uberta
* Donald Sage MacKay - Jean-Bob the frog Doug Stone - Speed the turtle
* Steve Vinovich - Puffin
* Joseph Medrano - Lord Rogers
* James Arrington - Chamberlain
* Joey Camen - Knuckles
* Owen Miller - Bromley
* Rosie Mann - Bridget the Hag Wolf

==Musical numbers==
* The Magic of Love
* Thats What You Do for a Friend
* You Gotta Love It
* Far Longer Than Forever (end credits)
* No Fear (rap)

==Release==
The film had a limited theatrical release in July 18, 1997 and performed poorly, only achieving a total domestic gross of $273,644.    Two months later in September 1997 the film was released on video. In 1999 it was included in a   set containing all three The Swan Princess movies with a bonus a sing-a-long disc.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 