Los Olvidados
 
{{Infobox film name            = Los Olvidados image           = Los-Olvidados-Poster.jpg caption         = director        = Luis Buñuel producer        = Óscar Dancigers writer          = Luis Alcoriza Luis Buñuel starring        = Alfonso Mejía Stella Inda Miguel Inclán Roberto Cobo music  Gustavo Pittaluga cinematography  = Gabriel Figueroa editing         = Carlos Savage distributor     = Koch-Lorber Films released        = December 9, 1950 (Mexico) runtime         = 80 min. language        = Spanish budget          =
}} Spanish for 1950 cinema Mexican film directed by Luis Buñuel.   

Óscar Dancigers, the producer, asked Buñuel to direct this film after the success of the 1949 film El Gran Calavera. Buñuel already had a script ready titled ¡Mi huerfanito jefe! about a boy who sells lottery tickets. However, Dancigers had in mind a more realistic and serious depiction of children in poverty in Mexico City.

After conducting some research, Jesús Camacho and Buñuel came up with a script that Dancigers was pleased with. The film can be seen in the tradition of social realism, although it also contains elements of surrealism present in much of Buñuels work.
 Best Director award at the 1951 Cannes Film Festival.   

==Cast==
* Stella Inda as Pedros Mother
* Miguel Inclán as Don Carmelo, the blind man
* Alfonso Mejía as Pedro
* Roberto Cobo as "El Jaibo"
* Alma Delia Fuentes as Meche
* Francisco Jambrina as the principal of the rural school
* Jesús Navarro as Juliáns father
* Efraín Arauz as "Cacarizo"
* Jorge Pérez as "Pelón"
* Javier Amézcua as Julián
* Mário Ramírez as "Ojitos" ("Little Eyes"), the lost boy
* Ernesto Alonso as Narrator (uncredited)

==Plot== juvenile jail blind street musician. They fail at first, but later track him down, beat him, and destroy his instruments.

With the help of Pedro, El Jaibo tracks down Julián, the youngster who supposedly sent him to jail. El Jaibo puts his arm in a fake sling and hides a rock in it. El Jaibo confronts Julián, who denies that he reported him to the police. Julián refuses to fight El Jaibo because it wouldnt be a fair fight with El Jaibos arm broken. As Julián starts to walk away, El Jaibo hits him in the head with the rock. He then beats Julián to death and takes his money. El Jaibo warns Pedro not to report the crime, and since he shares Juliáns money with Pedro, Pedro is an accomplice to the murder.
 pesos bill. Pedro accepts the offer, but as soon as he leaves, he encounters El Jaibo, who steals it. Pedro then tracks down El Jaibo and fights him. The fight ends in a stalemate, but Pedro announces to the crowd that it was El Jaibo who killed Julián. El Jaibo flees, but the blind man has heard the accusation and tells the police.

Pedro tracks El Jaibo down once again, and El Jaibo kills Pedro. While fleeing, El Jaibo runs into the police. As El Jaibo tries to run away, the police shoot and kill him. Meche and her grandfather find Pedros body. Not wanting to attract the police, they dump his body down a garbage-covered cliff. On their way, they pass Pedros mother, who, though once unconcerned with her disobedient child, is now searching for him.

== Alternate Ending ==

In the year 2002, it was announced that an alternate ending for Los Olvidados (labeled "the happy ending") was discovered at the Film Warehouse of the National Autonomous University of Mexico, and it would be restored digitally in order to show it to the public. On July 8, 2005, it was re-screened with the alternate ending on a few selected venues and included in subsequent DVD releases {{cite web
  | last = La Jornada
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Restrenan en pantalla grande Los olvidados, con final inédito
  | work = 
  | publisher =
  | date = 8 July 2005
  | url = http://www.jornada.unam.mx/2005/07/08/a10n1esp.php
  | format = 
  | doi = 
  | accessdate = 3 October 2011 }} 
. {{cite web
  | last = ABC.es
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Los Olvidados vuelve a la vida en DVD, con final alternativo
  | work = 
  | publisher =
  | date = 12 January 2004
  | url = http://www.abc.es/hemeroteca/historico-01-12-2004/abc/Espectaculos/los-olvidados-vuelve-a-la-vida-con-la-edicion-en-dvd-de-un-final-alternativo_963727110684.html
  | doi = 
  | accessdate = 3 October 2011 }} 

At the International Cinematographic Festival in Saltillo, Coahuila, Mexico, on February 3, 2011, the last surviving member of the cast, Alfonso Mejia (Pedro), introduced the alternative ending to the film. {{cite web
  | last = Vanguardia
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Un olvidado en Saltillo
  | work = 
  | publisher =
  | date = 3 February 2011
  | url = http://www.vanguardia.com.mx/unolvidadoensaltillo-643875.html
  | doi = 
  | accessdate = 3 October 2011 }} 

According to Mejia, Buñuel was "pressured by the censorship in México, and urged  to film an alternative ending, a conventional ending, to maintain the image of a progressive Mexico, where no one was poor or illiterate" (you can view the interview  ).

The alternative ending begins with El Jaibo and Pedro fighting on an abandoned warehouse. Pedro pushes El Jaibo from the roof, where he falls to his death. Pedro frisks the body for the money El Jaibo stole from him (in contrast to the original ending, where Pedro is murdered by El Jaibo). Pedro returns to the farm school with the money that the principal entrusted to him.

== Analysis ==
Thematically, Los Olvidados is similar to Buñuels earlier Spanish film, Las Hurdes. Both films deal with the never-ending cycle of poverty and despair.  Los Olvidados, is especially interesting because although “Buñuel employed … elements of Italian neorealism,” a concurrent movement across the Atlantic Ocean marked by “outdoor locations, nonprofessional actors, low budget productions, and a focus on the working classes,” Los Olvidados is not a neorealist film (Fernandez, 42).  “Neorealist reality is incomplete, conventional, and above all rational,” Buñuel  wrote in a 1953 essay titled "Poetry and Cinema."  “The poetry, the mystery, all that completes and enlarges tangible reality is utterly lacking.” (Sklar, 324)  Los Olvidados contains such surrealistic shots as when “a boy throws an egg at the camera lens, where it shatters and drips” or a scene in which a boy has a dream in slow-motion (Sklar, 324). The surrealist dream sequence was actually shot in reverse and switched in post-production.

==Reception==
Many critics have proclaimed Los Olvidados a masterpiece. It currently holds a 94% score on the website Rotten Tomatoes based on 29 reviews. {{cite web
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Rotten Tomatoes
  | work = 
  | publisher =
  | date = 
  | url = http://www.rottentomatoes.com/m/los-olvidados-1950/
  | format = 
  | doi = 
  | accessdate = 3 October 2011 }}  It was inscribed on UNESCO|UNESCOs "Memory of the World" Register in 2003 in recognition of its historical significance.

==References==
 
*Fernandez, Walter, Jr.  “A Directory of Dynamic Directors: Luis Buñuel.” Cinema Editor Fourth Quarter 2005: 42-43.
*Sklar, Robert.  Film: An International History of the Medium.   : Thames and Hudson,  .

==External links==
*    at the cinema of Mexico site of the ITESM  
*  
*   at  

 

 
 
 
 
 
 
 
 