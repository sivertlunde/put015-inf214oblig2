Nǃai, the Story of a ǃKung Woman
{{Infobox Film
| name           = Nǃai, the Story of a ǃKung Woman
| image          = 
| image_size     = 
| caption        = Screenshot from Nǃai, the Story of a ǃKung Woman John Marshall
| producer       = 
| writer         =  John Marshall
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Documentary Educational Resources 1980
| runtime        = 59 min.
| country        = U.S.A. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
  ethnographic filmmaker John Marshall.
 PBS and is distributed by Documentary Educational Resources. It provides a broad overview of Juǀʼhoan life, both past and present, and an intimate portrait of Nǃai, a Juǀʼhoan woman who in 1978 was in her mid-thirties. Nǃai tells her own story, and in so doing, the story of Juǀʼhoan life over a thirty-year period.

"Before the white people came we did what we wanted," Nǃai recalls, describing the life she remembers as a child: following her mother to pick berries, roots, and nuts as the season changed; the division of giraffe meat; the kinds of rain; her resistance to her marriage to ǀGunda at the age of eight; and her changing feelings about her husband when he becomes a healer. As Nǃai speaks, the film presents scenes from 1950s that show her as a young girl and a young wife.

The uniqueness of Nǃai may lie in its tight integration of ethnography and history. While it portrays the changes in Juǀʼhoan society over thirty years, it never loses sight of the individual, Nǃai. The film is credited with the introduction of the diological structure, whereby both the voices of the filmmaker and the subject are woven together to tell the story. It is also credited as the first ethnographic film to recognize the influence of modernity on the ǃKung people.

Marshall complied the footage of for Nǃai over the course of 27 years. Marshall shot over 353,000 feet of color film during his expeditions into the Nyae-Nyae region. The footage of Nǃai as a young girl, including her wedding ceremonies, was recorded in 1951.

The film contains a scene from the filming of The Gods Must Be Crazy, with the actual, revealing words of the Bushmen involved translated.

Awards:
* Cine Golden Eagle
* American Film Festival, Blue Ribbon
* International FIlm and Television Festival of New York
* Grand Prize, Cinema du RjeZ, Paris
* News Coverage Festival, Luchon, France

==References==
*{{cite web|url=http://der.org/films/nai-kung-woman.html
|accessdate=2007-02-15
|title=John Marshall: N!ai, the Story of a !Kung Woman
|work=www.der.org}}

*{{cite web|url=http://www.nefilm.com/news/archives/05june/marshall.htm
|accessdate=2007-03-21
|title=Remembering John Marshall
|author=Apley, Alice; Tames, David
|work=New England Film}}

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 
 