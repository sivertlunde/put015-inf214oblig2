Aasra
{{Infobox film
| name           =Aasra 
| image          = 
| image_size     =
| caption        =  Satyen Bose
| producer       = 
| writer         = 
| narrator       = 
| starring       = Mala Sinha  Biswajeet
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1966
| runtime        = 
| country        = India Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Satyen Bose. The film stars Mala Sinha, Biswajeet, Balraj Sahni, Jagdeep and Nirupa Roy. The films music is by Laxmikant Pyarelal.

==Plot==
Amar Kumar is an eligible bachelor and his parents are looking to match him up. Amar meets Roopa but is more attracted her cousin, Shobha. The girl is treated as a house servant in Roopas househould. When Amars feelings become public, Roopa beats Shobha. Frightened, she keeps away from Amar. The man travels abroad for several months; again Shobha is beaten. She escapes from the abusive household and is taken in by Amars parents. There is a twist, though, she is pregnant and the identity of the father is unclear.

==Cast==
* Mala Sinha as Shobha 
* Biswajeet as Amar Kumar 
* Balraj Sahni as Surendra Nath Kumar 
* Jagdeep as Harish 
* Nirupa Roy as Maya Kumar

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Daiya Ri Daiya Yashoda Maiya"
| Lata Mangeshkar
|-
| 2
| "Mere Soone Jeevan Ka Aasra Hai Tu"
| Asha Bhosle
|-
| 3
| "Neend Kabhi Rehti Thi Ankhon Mein"
| Lata Mangeshkar
|-
| 4
| "Sajana Kidhar Sari Ratiyan"
| Lata Mangeshkar, Usha Mangeshkar
|-
| 5
| "Shokhiyan Nazar Mein Hain"
| Mohammed Rafi
|-
| 6
| "Tu Kaun Ho Bataon"
| Lata Mangeshkar
|}

==External links==
*  

 
 
 
 


 