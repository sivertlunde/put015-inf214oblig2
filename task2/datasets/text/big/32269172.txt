Samundar
{{Infobox film
| name = Samundar
| image =
| writer =
| starring = Sunny Deol Poonam Dhillon Navin Nischol
| director = Rahul Rawail
| producer = Mushir-Riaz
| music = Rahul Dev Burman
| lyrics =
| released = September 3, 1986
| language = Hindi
}}

Samundar is an Indian film directed by Rahul Rawail and produced by Mushir-Riaz released in 1986. The movie stars Sunny Deol, Poonam Dhillon, Amrish Puri, Navin Nischol.  

==Cast==
* Navin Nischol ... Surajbhan
* Sunny Deol ... Ajit
* Poonam Dhillon ... Anjali
* Amrish Puri... Raiszada Narsingh
* Anupam Kher... Rajeshwar Nath / Girija Shankar
* Paresh Rawal ... Hansukh

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ae Zindagi"
| Kishore Kumar, Lata Mangeshkar
|-
| 2
| "Us Din Mujhko Bhool Na Jana"
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Ye Kori Kanwari"
| Kishore Kumar
|-
| 4
| "Ae Sagar Ki Laheron"
| Kishore Kumar, Lata Mangeshkar
|-
| 5
| "Range Mehfil"
| Asha Bhosle
|-
| 6
| "Tum Dono Ho Kitne Achhe"
| Bhavna Datta
|}

==References==
 

==External links==
*  

 
 
 
 
 

 