Mr. North
:For the alternative rock band from Dublin, Ireland see Mrnorth.
{{Infobox film
| name           = Mr. North
| image          =
| image_size     =
| alt            =
| caption        =
| director       = Danny Huston
| producer       = John Huston Steven Haft Skip Steloff Tom Shaw
| screenplay     = John Huston James Costigan
| based on       =  
| starring       = Anthony Edwards Robert Mitchum Lauren Bacall Harry Dean Stanton Anjelica Huston Mary Stuart Masterson Virginia Madsen
| music          = David McHugh
| cinematography = Robin Vidgeon
| editing        = Roberto Silvi
| studio         = Heritage Entertainment Inc. Showcase Productions International
| distributor    = The Samuel Goldwyn Company
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,221,366   (limited release)
}}
Mr. North is a 1988 American Comedy-drama|comedy-drama film starring Anthony Edwards, based on the 1973 novel Theophilus North by Thornton Wilder.

Directed by Danny Huston, the film became a family project; produced by John Huston, it also stars Anjelica Huston, Dannys future wife Virginia Madsen, and Allegra Huston.

==Plot==
In 1920s Newport, Rhode Island, Theophilus North (Anthony Edwards) is an engaging, multi-talented, middle-class Yale graduate who spends the summer catering to the wealthy families of the city. He becomes the confidant of James McHenry Bosworth (Robert Mitchum), and a tutor and tennis coach to the families children. He also befriends many from the citys servant class including Henry Simmons (Harry Dean Stanton), Amelia Cranston (Lauren Bacall), and Sally Boffin (Virginia Madsen).

Complications arise when some residents begin to ascribe healing powers to the static electricity shocks that Mr. North happens to generate frequently. Despite never claiming any healing or medical abilities, he is accused of quackery, and must, with the help of those he has befriended, defend himself.

In the end, Mr. North accepts a position of leadership at an educational and philosophical academy founded by Mr. Bosworth, and begins a romance with Bosworths granddaughter Persis.

==Cast==
* Anthony Edwards as T. Theophilus North
* Robert Mitchum as James McHenry Bosworth
* Lauren Bacall as Amelia Cranston
* Harry Dean Stanton as Henry Simmons
* Anjelica Huston as Persis Bosworth-Tennyson
* Mary Stuart Masterson as Elspeth Skeel
* Virginia Madsen as Sally Boffin
* Tammy Grimes as Sarah Baily-Lewis David Warner as Dr. Angus McPherson
* Hunter Carson as Galloper Skeel
* Christopher Durang as YMCA clerk
* Mark Metcalf as George Skeel
* Katharine Houghton as Mrs. Skeel
* Thomas H. Needham as Judge Nicholas Catwalader
* Richard Woods as Willis
* Harriet Rogers as Tante Liselotte
* Layla Summers as Nadia Denby
* Lucas Hall as Joseph Denby
* Thomas-Laurence Hand as Luther Denby
* Linda Peterson as Mrs. Denby
* Cleveland Amory as Mr. Danforth
* Christopher Lawford as Michael Patrick Ennis III

==Production== Eugene Lee was the Production Designer.

Originally John Huston was to play James McHenry Bosworth; but just after filming began, the illness that eventually killed him forced his withdrawal.  He was quickly replaced by family friend Robert Mitchum. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 