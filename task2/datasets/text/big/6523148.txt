Vanaja (film)
{{Infobox film
| name           = Vanaja
| image          = Vanaja poster.jpg
| image size     = 220px
| caption        = Theatrical poster
| director       = Rajnesh Domalpalli
| producer       = Latha R. Domalapalli
| writer         = Rajnesh Domalpalli
| starring       = Mamatha Bhukya
| music          = Indira Amperiani Bhaskara S. Narayanan
| cinematography = Milton Kam
| editing        = Rajnesh Domalpalli Robert Q. Lovett
| distributor    = Emerging Pictures (US)
| released       =  
| runtime        = 111 minutes
| country        = India United States
| language       = Telugu with English subtitles
}} Indian Parallel art house shoestring budget using a cast of non-professional first-timers for two and a half months.

The film stars   in Southern India. She learns Kuchipudi, a classical Indian dance form, while being employed at a local landladys house. All seems to be going well for her until sexual chemistry evolves between her and the landladys son, and this eventually leads her being raped by him. The ensuing pregnancy disrupts her simple life, and she must choose how to deal with the child.
 Best First Best Cinematography Telugu with subtitles in English language|English.

== Plot == soothsayer predicting to Vanaja that she will be a great dancer. With permission from her father, she goes to work in the house of the local landlady Rama Devi with the hope of learning Kuchipudi. While she is employed as a farmhand, she is entrusted with tending the chickens. When she gets caught playfully chasing them, she lies to conceal her pranks. Her vivaciousness and spunk soon catch the landlady’s eye. To keep her out of trouble, Rama Devi soon promotes her to a kitchen underhand where she meets Rama Devi’s cook, the old, crusty and extremely loyal Radhamma (Krishnamma Gundimalla).
 ashta chamma (a leisurely game in rural towns of Andhra Pradesh) against the landlady. Knowing that losing isn’t the mistress’s forte, she deliberately gives up her game. This gesture, in turn, eventually secures her the landlady’s mentorship, first in music and then in dance. Vanaja excels at these art forms and seems to be on a steadily ascending path until the arrival of Shekhar (Karan Singh), the landladys 23-year-old son, from the United States. Shekhar is a handsome, muscular young man who is running for an office in the local government. Sexual chemistry is ignited between Shekhar and Vanaja (still a minor at 15) when flirtation and sexual innuendo bloom.
 lower caste. In the end Rama Devi and Shekhar gain possession of the child, who will grow up to be an upper caste boy.

== Cast ==
* Mamatha Bhukya as Vanaja
* Urmila Dammannagari as Rama Devi
* Ramachandriah Marikanti as Somayya
* Krishnamma Gundimalla as Radhamma
* Karan Singh as Shekhar
* Bhavani Renukunta as Lacchi

== Production ==
=== Background ===
Director Rajnesh Domalpalli graduated with Bachelors and Masters degrees in electrical engineering in 1984 and 1986. {{cite news|date=2007-08-28 |accessdate=2008-02-16 |url=http://www.indiewire.com/people/2007/08/indiewire_inter_101.html
|title=indieWIRE interview: Vanaja Director Rajnesh Domalpalli |work=indieWIRE |archiveurl=http://web.archive.org/web/20080226183823/http://www.indiewire.com/people/2007/08/indiewire_inter_101.html |archivedate=2008-02-26}}  While working on his Bachelors degree at the Indian Institute of Technology Bombay, he wrote short stories. One of his stories, The Dowry, was twice selected for broadcast by BBC World Service while he was in graduate school.    During schooling, he was introduced to south Indian classical music, especially on the veena, and followed this up with years of training on the vocals. 

While he was working as a software engineer in Silicon Valley, California, he pursued filmmaking and graduated with a Master of Fine Arts degree from Columbia University. 
 class distinction and conflict that continue to infuse our society and culture even today."  Referring to its emphasis, he said the film was also about "fading institutions of folk art, old buildings that are collapsing, things which we should be protecting&nbsp;— which are a part of our heritage."    Speaking of the need for preservation of Indian culture and heritage, he said that making the film was an opportunity to emphasize the Indian folk arts, too. 

=== Filmmaking ===
With the early version of the script being ready at the end of his fourth semester, Domalpallis initial intent was to find financing for the film in India and then in the United States. However, he could not find financing. Domalpalli decided to select non-professionals and train them in a year. In the United States, producers voiced similar concerns over the marketability of the film and what they referred to as "its lack of cohesion." Beside this, Domalpallis inexperience in filmmaking added to their concerns.    Recalling initial hurdles, Domalpalli later said, "it was only when I showed my professors a rough cut of the film, and they approved, that purse strings finally came loose."  It eventually constituted the thesis for his Masters degree. 
 southern India) scene at the beginning of the film, Domalpalli said:
 If you talk to people who perform a Burrakatha, you will see a huge difference in the way the older generation performs the art vis-à-vis the way the way the younger generation performs it... This has happened because of the advent of television. Burrakatha is a long-format art form. The point we are making is that if we dont protect these folk arts, they will be on their way out.  

The producers faced a stiff challenge in securing a rural bungalow to serve as the landladys mansion. Even after obtaining a building as the best possible fit in the coastal town of Bobbili, infestation of snakes and bats posed a problem for the crew. In addition, the building was not strong enough to support the filming equipment. Under these circumstances, the makeshift production designers refurbished the building with space for chicken coops and goat pens as required by the script. To make the space look inhabited, local help was sought for trampling the ground with their livestock. The local people wanted to act as extras in exchange for providing farming tools, bullock carts and other material. As a result, the crew had to ensure that these extras didnt look into the camera during filming. 

 "Given the rural nature of the story, and the tendency of most local acting to lean towards the theatrical, it was clear that non-actors drawn from hutments, labor camps and the vast Indian middle class were the right choice," said Domalpalli, referring to his choice of casting.  In addition, the inclusion of Radhammas character was to bring a natural feel for the film. Her behavior such as the way she "sits, stands, moves, grunts and groans&nbsp;— that is the way people from a village talk and behave... You would immediately recognize a person who served you breakfast... That makes a point."  

Referring to the challenges faced in casting, he said that while they were canvassing local people for auditions, they were warding off rumors that they were after the peoples kidneys.  When placing a newspaper advertisement for the landladys character did not seem viable, they advertised for household help instead. Upon seeing this, Urmila Dammannagari turned up for the interview with Domalpalli. Inadvertently, their conversation veered toward the film and the real motive behind the advertisement became evident. Though initially shocked and despite the fact that she had to commute   from her house to the location, she took up the role.  Professionally Krishnamma Gundimalla, who played the role of Radhamma, carried bricks on her head as a construction worker. On the other hand, Ramachandriah Marikanti, who plays Vanajas fisherman father, was a municipal sweeper and worked as a security guard. 

For casting Vanaja and Lacchi, he said that they met approximately 2500 children, interviewed about 260, and finally selected two from a shortlist of five to play the roles of Vanaja and Lacchi.  Parents of these children were distrustful of them because their wards would have to frequently commute to Domalpallis house for acting lessons for at least a year. Domalpalli felt that "to convince people to devote so much of their childrens time and energy was hard enough, but to find the right combination of intelligence, commitment and talent as well was probably the steepest cliff they had to climb."  While visiting schools for identifying the child cast, Mamatha Bhukya at first was not selected because her hair was short. But after she sang a song on Mahatma Gandhi, she was selected for the role. Srinivas Devarakonda, a disciple of well-known Kuchipudi guru Vempati Chinna Satyam,  taught Mamatha the classical dance form for a year in the basement of Domalpallis house.   Due to this sustained effort, she altered her ambitions from becoming a doctor to an actress and a Kuchipudi dancer.  Bhavani Renukunta was chosen for Lacchis role after an interview at the Hyderabad office of Varija Films, the company that handled the publicity. 
 aspect ratio, he was shocked to look through the Super 16mm lens the day they commenced the shoot. However, Kam helped him to stay composed during the production. 
 mode or rāga known as "Behag".

Domalpalli used "janapada geetalu" in the film; folk songs that are rarely heard. To record these songs for the film, Domalpalli and his crew traveled to towns and villages in rural Andhra. 

== Release and reception == Dolby Digital 5.1 Surround, widescreen and NTSC format.  While reviewing it, Jeffrey Kauffman from DVD Talk observed that though most of the film was made using natural lighting situations, the color and saturation quality was excellent. Further, the reviewer was quite favourable about the use of exotic sounds right from birds to the instruments. 

=== Special screenings and awards ===
On 11 September 2006, Vanaja first premiered at the Toronto International Film Festival.   The same year, it won an Honorable Mention for the Golden Starfish Award at the Hamptons International Film Festival.  It won the Best Narrative Film award at the Indo-American Arts Council Film Festival.  Following that, it was showcased at the International Film Festival of India  and the International Film Festival of Kerala  by the end of 2006.

At the films screening at the 2007 Berlin Film Festival, it won a standing ovation from the audience, bringing Mamatha Bhukya to tears and emotionally affecting Rajnesh Domalpalli.  The film won the Best First Feature award at this festival.   The same year, Vanaja received a special international jury prize at the Cairo International Film Festival,  Best Feature at the Memphis International Film Festival,   Best International Film at the Sacramento International Film Festival,   and a Platinum award at the WorldFest-Houston International Film Festival.   It received a special mention for the grand jury prize at the Indian Film Festival of Los Angeles,  the Miloš Macourek Award in special recognition for a feature film for youth at the Zlín International Film Festival,  an Achievement Award at the Newport Beach Film Festival,   and a special jury prize for Best Production Design at the RiverRun International Film Festival.  It won the prize for Best Cinematography at the Rhode Island International Film Festival,  the first prize in the live-action feature film category at the Chicago International Childrens Film Festival,  and the Camério Meilleur Long Métrage/Starlink Aviation award at the Carrousel international du film de Rimouski.  In addition, the film won also won an award for the best live-action film at the International Young Audience Film Festival.  Bhukya won the best actress award at the Asian Festival of First Films.  It was chosen as one of 13 "key films" when the Locarno International Film Festival focused on India in 2011.   

=== Reviews ===
The film received an overall positive response from critics and was particularly noted for the theme and for Mamatha Bhukyas performance. Roger Ebert described Bhukya as "a natural star, her eyes and smile illuminating a face of freshness and delight."    Writing about Vanaja, he added that "there are the glorious colors of saris and room decorations, the dazzle of dance costumes and the dusty landscape that somehow becomes a watercolor by Edward Lear, with its hills and vistas, its oxen and elephants, its houses that seem part of the land. In this setting, Domalpalli tells his story with tender precision, and never an awkward moment."  Ebert listed it among the top five foreign films of 2007.  According to Laura Kern from The New York Times, the film "is a coming-of-age tale that is engrossing, if slightly overlong, and absolutely timeless, unfolding against an antiquated class system that sadly stands firm in rural areas of India to this day." 

A review in Variety (magazine)|Variety called it "a film that touches the heartstrings as it brings home the cruel class distinctions that poison Indian societ," and said it "is more than a childrens film, despite revolving around a central character of 14. Its social message, linked to the story of a poor farm girl who aspires to be a dancer, never feels forced, and the moral issues it depicts are realistically complex."    Commenting on its commercial feasibility, the review concluded that "while that might not translate into obvious box office potential, art house appeal is there for distributors willing to seek out a market."  The Chicago Tribune wrote:

 Its a touching, believable, often funny but ultimately sad tale of how one class can take advantage of another, even in the guise of patronizing benevolence. Though sometimes shifting abruptly in time, Vanaja is an arresting story of modern-day hardship and class exploitation, recalling Charles Dickens as well as Western fairy-tale lore. Domalpallis settings are ultra-real in detail and color, from the crude, almost feudal deprivations of Vanajas dirt-floor background to the stately rituals and autocratic entitlement of the well-to-do.  

Speaking of Bhukyas performance, the San Francisco Chronicle writes, "Bhukya delivers an entrancing and natural performance, deftly balancing both the wide-eyed childishness of a young girl with the dawning awareness of lifes darker possibilities. Shes also an accomplished dancer, which she proves at several points in the film." It adds, "can this wonder-filled film truly be not only Domalpallis first feature, but originally part of a thesis submission at Columbia University? Both in the films writing and direction, Domalpalli displays maturity, wisdom and a loving sense of visual and character detail."  Marc Savlov of The Austin Chronicle proclaimed, "director Domalpalli, who, with his debut feature, turned in what may well be the best Columbia University masters thesis ever."  The Hartford Courant also claimed like the Chicago Tribune that the film would remind the Western audience of Charles Dickens, and further stated that it "... gives a detailed sense of place and shows a mastery of story telling. The themes of fate and class resonate and the work of the amateur players is remarkably moving." 
 Time Out. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 