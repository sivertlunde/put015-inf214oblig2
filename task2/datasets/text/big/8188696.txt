Dorm (film)
{{Infobox film
| name           = Dek hor
| image          = Dormmovieposter.jpg
| caption        = The Thai movie poster.
| director       = Songyos Sugmakanan
| producer       = Yodphet Sudsawad
| writer         = Chollada Teaosuwan Vanridee Pongsittisak Songyos Sugmakanan
| starring       = Charlie Trairat Chintara Sukapatana
| music          = Chumpol Sepswadi
| cinematography = Niramon Ross
| editing        = Pongsatorn Kosolpothisup GTH
| released       =  
| runtime        = 110 minutes
| country        = Thailand
| language       = Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 2006 Cinema Thai horror film|horror-drama film.

==Plot==
In Thailand, young Ton Chatree (Trairat) is sent to a boarding school by his father to get good grades and not tell his mother about his father having an affair. Once in the school, Ton feels like an outcast and misses his family and friends. His new schoolmates tell ghost stories about a boy who died in the school swimming pool and a young pregnant woman who committed suicide.  The stories frighten him, thereby exacerbating Tons difficulties adjusting to the school. However, Ton becomes close friends with another lonely boy, Vichien (Chienthaworn), who Ton later discovers is the boy who drowned, and his death repeats every night. Ton finds a way to help his friend rest in peace.

At the same time, Ms. Pranee (Sukapatana), the school administrator, is deeply troubled by Vichiens death because she believes incorrectly that Vichien committed suicide and that it was partly her fault.  At the end of the movie, Ton tells Pranee the truth, that Vichiens death was an accident and that Pranee should not blame herself.

==Cast==
* Charlie Trairat as Ton
* Chintara Sukapatana as Ms. Pranee
* Sirachuch Chienthaworn as Vichien
* Suttipong Tudpitakkul as Tons father
* Jirat Sukchaloen as Peng
* Thanabodin Sukserisup as Doc Nui
* Pakasit Pantural as Pok
* Nipawan Taweepornsawan as Tons mother

==Reception==

===Box office, critical reception===
The film was screened at the 2006 Bangkok International Film Festival.  It opened in wide release in Thailand on February 23, 2006, and was the No. 1 film that weekend, earning nearly US$544,000.  The film has had theatrical releases in Singapore and Malaysia and at other film festivals, including the Pusan International Film Festival.

It received praise from critics for the performances of the child actors and Chintara Sukapatana, as well as for its color-drained photography and the production design of the old boarding school.  

== Production == 2003 hit Thai film Fan Chan, which also starred Charlie Trairat. The film was critically acclaimed in Thailand, where it won more honors than any other film, including best picture from the Bangkok Critics Assembly.

===Awards===
* 2007 Berlin Film Festival – Crystal Bear Award for best film in Generation Kplus competition, awarded by 11-member childrens jury. 
* 2007 Fajr International Film Festival – Crystal Simorgh for best director in the Spiritual Films Competition. 
* Thailand National Film Association Awards  , ThaiCinema.org, 2007-02-28.  
**Best editing
**Best art direction Golden Doll Awards 
** Best actor (Charlie Trairat)
** Best actress (Chintara Sukapatana)
** Best script Bangkok Critics Assembly 
** Best picture
** Best director (Songyos Sugmakanan)
** Best script
** Best cinematography Starpics Awards 
**Best picture
**Best supporting actress (Chintara Sukapatana)
**Best script
**Best editing Kom Chad Luek Awards 
**Best supporting actor (Sirachuch Chienthaworn)
**Best supporting actress (Chintara Sukapatana)

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 