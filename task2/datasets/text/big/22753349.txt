Backbone (1975 film)
{{Infobox film
| name           = Backbone
| image          =
| caption        =
| director       = Vlatko Gilić
| producer       = Svetlana Jovanović Dan Tana
| writer         = Vlatko Gilić
| starring       = Dragan Nikolić
| music          =
| cinematography = Branko Ivatovic Zivorad Milic
| editing        = Leposava Aleksic Olga Skrigin
| distributor    =
| released       = 1975
| runtime        = 91 minutes
| country        = Yugoslavia
| language       = Serbo-Croatian
| budget         =
}}

Backbone ( ), is a 1975 Yugoslavian drama film directed by Vlatko Gilić. It was entered into the 1977 Cannes Film Festival.   

==Cast==
* Dragan Nikolić - Pavle Gvozdenović
* Predrag Laković - Pepi
* Mira Banjac
* Milutin Butković
* Stanislava Janjic - Ana
* Seka Sablić - (as Jelisaveta Sabljic)
* Nada Skrinjar - Bolesnica
* Miroljub Leso
* Neda Spasojevic
* Djordje Jelisic
* Renata Ulmanski - Woman from kafana
* Sladjana Matovic - Jelena
* Dragomir Felba Danilo "Bata" Stojković - Man from kafane
* Jelena Cvorovic - (as Jelena Radovic)
* Jelka Utornik
* Taško Načić

==References==
 

==External links==
* 

 
 
 
 
 


 