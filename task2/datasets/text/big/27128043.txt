The Optimists (film)
{{Infobox film
| name           = The Optimists
| image          = The Optimists poster.jpg
| image size     = 
| alt            = 
| caption        = Promotional poster
| director       = Goran Paskaljević
| producer       = Goran Paskaljević Lazar Ristovski
| writer         = Goran Paskaljević Vladimir Paskaljević
| narrator       = 
| starring       = Lazar Ristovski Bojana Novakovic Petar Božović Nebojša Glogovac Aleksandar Simić
| cinematography = Milan Spasić
| editing        = Petar Putniković
| studio         = Nova Film
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Serbia
| language       = Serbian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
The Optimists ( ) is a 2006 Serbian black comedy film directed by Goran Paskaljević. The film, presented as five unrelated narrative sequences, was inspired by Voltaires satirical work Candide.   The Optimists features an ensamble cast of Serbian actors, with Lazar Ristovski appearing in all five storylines.

The film had its premiere at the 2006 Toronto International Film Festival (TIFF)  and was subsequently screened at several other film festivals earning multiple awards. Ristovski was named Best Actor at the 51st Valladolid International Film Festival while the film won the Golden Spike award as the best film of the festival also earning the Youth Jury Award. The Optimists was included as part of a retrospective exhibition on director Goran Paskaljević at the Museum of Modern Art in 2008. 

==Plot==
A village has been almost destroyed by a large flood. A hypnotist (Ristovski) comes to the village and speaks to the destitute inhabitants. Free of charge, the hypnotist offers to lift the villagers spirits through hypnosis. The villagers doubt the hypnotists noble motive and he is accused of an apparent theft. The police arrest, beat and interrogate him.

A sleazy businessman (Tihomir Arsić) takes a young woman (Bojana Novaković) in his employ to a rural area and he rapes her there. The girls father (Ristovski) is upset and wants to kill the man. The businessman is powerful and the father is afraid of getting fired so the father, instead, ends up greatly apologizing to the businessman suggesting that the assault was the daughters fault and that it inconvenienced the businessman.

Ilija (Viktor Savić) is a young man whose father Ratomir (Ristovski) recently died. Ilija takes the money that was saved for his fathers funeral and he quickly loses it all while gambling. He meets an old lady (Mira Banjac) who was recently diagnosed with a terminal illness and who has had a lucky streak on the slot machines ever since her diagnosis. Ilija joins her in hopes of rejoicing with her success.

Pera (Ristovski), the owner of a large slaughterhouse, calls a doctor (Nebojša Glogovac) to his home complaining that his 12-year-old son (Nebojša Milovanović) is giving him a heart attack. The 12-year-old is kept locked up in a room because he feels overly dedicated to the family business, so much so that he slaughters every animal he sees. The doctor realizes that he is expected to treat the son, not the slaughterhouse owner.

A con man posing as a faith healer (Ristovski) approaches a group of people with various disabilities and illnesses. He offers, for a fee, to take them by the busload to a spring that has magical healing powers where they will be cured. The group boards the bus and arrives at the destination. Once there, the con man abandons the group and leaves the site. The group, having realized theyre being abandoned, does not do much to stop the bus from leaving because theyre still determined to get the full benefit of the springs healing waters.

==Reception==
===Critical response===
Dan Fainaru of  , September 15, 2006. Accessed April 27, 2010. 

Reviewing the film for  , September 15, 2006. Accessed April 27, 2010. 

===Awards===
* Cinéma Tout Ecran (2006) 
** The FIPRESCI International Critics Award
** Best Production - Goran Paskaljević
** Young Jury Mention

* 51st Valladolid International Film Festival 
** Golden Spike
** Best Actor - Lazar Ristovski
** Youth Jury Award

* 23rd Festroia International Film Festival 
** Audience Award

==See also==
* Cinema of Serbia
* 2006 in film
* List of Serbian films

==References==
 

==External links==
*  

 

 
 
 
 
 