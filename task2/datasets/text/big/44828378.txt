Sankalpam
{{Infobox film
| name           = Sankalpam
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| screenplay     = A. M. Rathnam
| producer       = A. M. Rathnam
| director       = A. M. Rathnam Gautami
| Koti
| cinematography = Navakanth
| editing        = Cola Bhaskar
| studio         = Sri Surya Movies
| released       =  
| runtime        = 138 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 English meaning 1995 Telugu Gautami in the leading roles, with Prakash Raj and Jaya Sudha in pivotal roles. The music was composed by Saluri Koteswara Rao|Koti. The film is Prakash Rajs debut in Telugu language. The film was considered a flop at the box office.      

==Plot==
The entire story is based around the construction of the Annapurna Biscuit Factory by the young & energetic Krishna Murthy (Jagapathi Babu) inspired by his willpower and determination.

==Cast==
{{columns-list|3|
*Jagapathi Babu as Krishna Murthy  Gautami as Rukmini
*Prakash Raj as Gaddapalugu Chenchu Ramayah
*Jaya Sudha as Municipal Commisioner Vyjayanthi Satyanarayana  Gummadi as Prakasam / Chinna Chandra Mohan as Sivaprasad
*Brahmanandam as Jagam Sudhakar as Madhu
*Babu Mohan as Guravaiah
*Dharmavarapu Subramanyam as M.R.O AVS as Pollution Officer
*Narra Venkateswara Rao as Municipal Engineer
*Chalapathi Rao as Abbulu Rallapalli as Commedy Wizard
*P. L. Narayana as Ranganayakulu
*Gokina Rama Rao as Minister
*Bheemiswara Rao as Chief Secretary
*Brahmaji as Babji
*Sudha as Suseela
*S. Varalakshmi as Mathaji
*Athili Lakshmi as Kantham
*Annuja
*Seeta as Subhadra
*Sunitha
*Padma
*Divya
*Vidya
*Manisha
}}

==Soundtrack==
{{Infobox album
| Name        = Sankalpam
| Tagline     = 
| Type        = film Koti 
| Cover       = 
| Released    = 1995
| Recorded    = 
| Genre       = Soundtrack
| Length      = 23:55
| Label       = Supreme Music Koti
| Reviews     =
| Last album  = Rikshavodu   (1995)
| This album  = Sankalpam   (1995)
| Next album  = Aayanaki Iddaru   (1995)
}}
 Koti and it was released on Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 23:55
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Acchatlo Muchatlo
| lyrics1 = Bhuvanachandra SP Balu, Chitra
| length1 = 4:52

| title2  = Chinnaari Manasuku  Sirivennela Sitarama Sastry
| extra2  = SP Balu,Chitra
| length2 = 4:53

| title3  = Dheem Thanakku
| lyrics3 = Bhuvanachandra
| extra3  = SP Balu,Chitra
| length3 = 5:09

| title4  = Kurisindi Vaana
| lyrics4 = Vaddepalli Krishna
| extra4  = SP Balu,SP Pallavi
| length4 = 5:00

| title5  = Mettaga Hattuko
| lyrics5 = Bhuvanachandra 
| extra5  = SP Balu,Chitra
| length5 = 4:01
}}

==Others== Hyderabad

==References==
 

 
 
 


 