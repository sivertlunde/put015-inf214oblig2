Paradise in Harlem
{{Infobox film
| name           = Paradise in Harlem
| image_size     = 
| image	=	Paradise in Harlem FilmPoster.jpeg
| caption        = 
| director       = Joseph Seiden
| producer       = 
| writer         = Frank L. Wilson (story) Vincent Valentini (writer)
| narrator       = 
| starring       = 
| music          = Lucky Millinder Juanita Hall  (uncredited) 
| cinematography = Charles Levine Don Malkames
| editing        = 
| distributor    = 
| released       = 
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Paradise in Harlem is a 1939 American musical comedy-drama film written by Frank L Wilson and directed by Joseph Seiden. It was first shown in 1939 starring Frank H Wilson. It was released by Jubilee Production Co.

== Plot summary ==
An actor sees a mob execution and is run out of town by the aforesaid mob members.

== Cast ==
*Frank H. Wilson as Lem Anderson
*Mamie Smith as Madame Mamie
*Norman Astwood as Rough Jackson
*Edna Mae Harris as Doll Davis
*Merritt Smith as Ned Avery
*Francine Everett as Desdemona Jones
*Sidney Easton as Sneeze Ancrum
*Babe Matthews as Laura Lou
*Lionel Monagas as Matt Gilson
*Madeline Belt as Acme Delight
*Herman Green as Ganaway
*Percy Verwayen as Spanish
*George Williams as Runt
*Alec Lovejoy as Misery
*Lucky Millinder as Himself - Bandleader
*Juanita Hall as Singer in Audience

== Soundtrack ==
* Lucky Millinder with band & chorus - "I Gotta Put You Down" (Written by Lucky Millinder)
* Mamie Smith - "Lord, I Love that Man"
* Edna Mae Harris and Lucky Millinder - "Harlem Serenade" (Written by Vincent Valentini)
* Sidney Easton and Babe Matthews - "How DYou Figure Ill Miss You?"
* Mamie Smith and The Alphabetical Four - "Harlem Blues" (Written by Perry Bradford)
* Babe Matthews - "Why Am I so Blue?" (Written by Joe Thomas )
* Juanita Hall, Singers, Francine Everett, Frank C. Wilson and Babe Matthews - Gospel version of "Othello" (Written by Juanita Hall)

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 