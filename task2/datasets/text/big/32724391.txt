Second Show
 
 
 
{{Infobox film
| name           = Second Show
| image          = Second show film.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Srinath Rajendran
| producer       = AOPL Entertainment Private Limited
| writer         = Vini Vishwa Lal Baburaj Sunny Rohini
| Avial
| cinematography = Pappu
| editing        = Praveen K. L. N. B. Srikanth
| studio         = AOPL Entertainment Private Limited
| Online Promoters        = PrimeGlitz Media,Sreenu sree (srs international group)
| distributor    = AOPL Entertainment Private Limited (Kerala) Fox Star Studios (rest of India and abroad) 
| online media partner = PrimeGlitz Media
| released       =     
| runtime        = 126 minutes
| country        = India
| language       = Malayalam
| budget         =      
| gross =  
}}
 Malayalam action action crime film directed by Srinath Rajendran and
written by Vini Vishwa Lal. The film, set in the backdrop of criminal gangs of varied varieties in Kerala, tells the story of young Hari who rises from an illicit sand miner to a smuggling baron in a short time. It stars Dulquer Salmaan, Gauthami Nair, Sunny Wayne, Baburaj (actor)|Baburaj, and Sudesh Berry. 

==Plot==
The film proceeds as the flashback of an ex-convict Hari (Dulquer Salmaan) narrated to a man he meets at a bus stop on a rainy night. It is revealed that Hari (whose nickname is Lalu) is a poor young man who works for the local sand mafia as a driver. He is friends with Kurudi (Sunny Wayne) whose real name is Nelson Mandela PP. One day while at work, Kurudi draws Lalu to a fight leaving the sand smugglers without a driver that causes them to get caught by the police. Now jobless, Lalu and Kurudi join the group of Chaver Vavachan (Baburaj), who is a financier, and take up the job of retrieving vehicles for non-repayment of loans.

Chaver Vavachan is the younger brother of a notorious former goon Chaver Antony who dies due to a cracker burst by a young lad. Vavachan swears revenge and, after a long wait, finds the lad (who is now a man) at a hospital and bursts crackers on him. The injured however happens to be one of the henchmen of an influential smuggler, Vishnubuddhan(Sudesh Berry) who, enraged, kills Vavachan. Impressed by his courage, Vishnubuddhan takes Lalu into his gang. During one of Vishnubuddhans deals, Lalu is betrayed by one of his friends and Vishnubuddhan mistakes Lalu for making away with the goods. Vishnubuddhans henchmen severely thrash Lalu, forcing him to flee. He however returns as a smuggler in his own right and starts overshadowing Vishnubuddhan. Angered by this, Vishnubuddhan kills Kurudi and Lalu avenges his friends death by killing Vishnubuddhan and taking over his business. However, Lalu is eventually caught by the police and sent to prison.

Back in the present, as Lalu finishes his narration, he sees Vishnubuddhans son sitting in a car parked nearby. Realising it to be a trap, Lalu becomes alert but the man to whom Lalu has just narrated his story shoots him. Before leaving the man reveals that he was actually sent by Vishnubuddhans son to kill Lalu. The final scene shows Lalu lying badly wounded but his hand moves slightly, revealing that his chapter is not yet over.

==Cast==
* Dulquer Salmaan as Hari Lal (Lalu)
* Gauthami Nair as Geethu (Geethanjali)
* Sunny Wayne as Kurudi (Nelson Mandela P. P.) Baburaj as Chaver Anthony/Chaver Vavachan Rohini as Lalus Mother (Devaki)
* Sudesh Berry as Vishnu Budhan Kunjan as Janardanan (Lalus uncle)
* Mithun Nair as Sidharth Budhan
* Bibin Perumbillikunnel as Abu
* Anil Anto as Neerali
* Murali Krishna as Sethu
* Ratheesh as Ummar
* Aneesh Gopal as Vikadan
* Baiju Varghese as Babu
* Sam as Sunil
* Vijay Kumar as Sachi
* Noora Michael as Surabhi
* Sundar as Abhi
* Joby as Kochu
* Sreekumar Kozhikode as S.I. George
* Jayaraj Kozhikode as Ramettan
* Kottayam Bose as Moopan
* Dominin as Interview
* Bibin as Sajan George
* Sidhu R. Pillai as Shyam

==Production== Avial band Baburaj is also appearing in supporting roles. The Project Designed by Shaz Shabeer Strikers & Crew.

==Music==
{{tracklist
| collapsed       = no Avial and Nikhil Rajan
| extra_column    = Artist(s)
| title1          = Adipidi
| extra1          = Nikhil Rajan
| length1         = 3:30
| title2          = Ee Ramayana
| extra2          = Sooraj Santhosh, Janani Madan
| length2         = 4:19
| title3          = Ee Ramayana
| extra3          = Janani Madan
| length3         = 4:19
| title4          = Ayyo Avial
| length4         = 4:27
| title5          = Thithithara Avial
| length5         = 3:29
| title6          = Arambath Avial
| length6         = 4:22
| title7          = Swapnam
| extra7          = Naresh Iyer
| length7         = 4:43
| title8          = Swapnam
| extra8          = Jakes Bejoy
| length8         = 4:43
}}

==Critical response==
Nowrunning.com rated the movie 2.5 out of 5 and said "Srinath Rajendrans film does have a head-spinning quality to it that leaves you dazed at times, but remains entertaining and witty to the hilt".  IndiaGlitz.com rated the movie 6.5 out of 10 by saying "Overall, Second show  though with its share of little negatives, is an honest effort that deserves to be encouraged". 

Rediff.com gave the movie 2 out of 5 stars and said the movie was disappointing.   Sify.com in its review said that "Ultimately, its an above average film, at best time-pass viewing. Watch it with no expectations and if you’re ready to think unconventional, this film could be a nice option." 

==References==
 

==External links==
*  
*  



 
 
 
 
 
 
 
 