The Legend of the Titanic
 
{{Infobox film
| name = The Legend of the Titanic
| image = Titanic_Animated.jpg
| image_size = 190px
| caption = DVD cover art
| director = Orlando Corradi Kim J. Ok
| producer = ITB Spain Hollywood Gang Productions USA
| writer = Celelia Castaldo Loris Peota
| narrator =
| starring = Jane Alexander Francis Pardeilhan Gregory Snegoff Sean Patrick Lovett
| music = John Sposito (Gianni Sposito)
| cinematography =
| editing = Emanuelle Fogelietti
| studio = ITB Hollywood Gang Productions Mondo TV
| distributor = Mondo TV (Italy) Adriana Chiesa Enterprises (Worldwide, All Media) AFM Filmverleih GmbH (Germany, Theatrical) Laser Paradise (Germany, DVD)
| released =  
| country = Italy
| runtime = 84 minutes Italian
| budget =
| gross =
}}

The Legend of the Titanic ( ,  ) is a 1999 Italian animated film directed by Orlando Corradi and Kim J. Ok. The film is a very loose adaptation of the RMS Titanic sinking and featured several fantasy elements such as anthropomorphic animals.  

The Legend of the Titanic was followed by a 2004 sequel titled Tentacolino.

==Plot==
  RMS Titanic.

In April 1912, Conners was a young sailor mouse on the Titanics maiden voyage from Southampton, England to New York. He is in charge of taking account for the mice who are making the trip. A young mouse from Brazil named Ronny who enjoys playing soccer befriends Conners and Conners falls in love with Ronnys sister Stella.
  gypsies dancing at the dock and happily watches them. A gypsy man named Don Juan is dancing with his dog Smiley. He notices Elizabeth and sends Smiley to see her. When Elizabeth takes off one of her gloves to pet Smiley, Smiley snatches it and takes it back to Juan. Juan looks at Elizabeth and the two instantly fall in love. The Titanic then sets off to sea from Southampton on her first, and only, voyage.
 ventilation system. They are appalled by the way Elizabeth is being treated badly and decide to help her. When Elizabeth goes to the bow of the ship one night, some dolphins talk to her due to some magic Moonlight|moonbeams. The dolphins jump very high out of the water and seem to Levitation|levitate. They tell her of Maltraverss evil scheme. Maltraverss manservant Geoffreys spies on Elizabeths activities and uses a special whistle at the stern of the ship to call the criminal shark named Mr. Ice and use him for causing destruction.

Conners and Ronny introduce themselves to Elizabeth and offer to help her. Listening to their advice, Elizabeth tells her father she doesnt want to marry Maltravers. He listens to his daughter and tells her that he will never force her to do something she doesnt want to do. Meanwhile, Smiley tries to look for Elizabeth to cheer up Juan. After he unsuccessfully tries to find her in the ships ballroom, he meets Conners and Ronny, who agree to help arrange a meeting and dance for Elizabeth and Juan. The meeting goes according to schedule and Elizabeth and Juan dance and kiss together. 

Elizabeth tells her father that she wants to marry Juan, and he agrees, but Elizabeths stepmother is furious. When Elizabeth tells her stepmother that she should marry Maltravers, she storms off in anger, despite the fact that they both flirt constantly (this might just be the fact that theyre scheming together). Elizabeths stepmother and Maltravers decide to do things drastically now, as its clear that Elizabeth will not marry him. They decide to sink the Titanic using the help of Mr. Ice and his gang of criminal sharks. Maltravers prepares to send news to his whaling ships by Telegraphy|telegraph, and the mice decide to chew apart the wires to stop it from being sent. 

Ice and his gang of sharks decide to let an iceberg sink the Titanic. They fool an octopus named Tentacles with a dogs nose into heaving an iceberg to the surface of the ocean by concealing the plan in the form of a bet to see who can throw ice the farthest. On the Titanic, the Duke is forced to sign the whaling rights to Maltravers and his stepwife at Coercion#Physical|gunpoint. Maltravers and his entourage escape the Titanic in a Lifeboat (shipboard)|lifeboat. 
 Captain Edward Smith. 

The mice realize that because they have cut the telegraph wires, the ship cant send out a call for help. They enlist the help of another mouse friend of theirs, named Camembert, to help repair the wires. They are unable to do it, but Camembert gets the idea to attach the halves of the cut wire to his moustache. They do so, and Camembert somehow dies because of this. 

Elizabeth and Juan manage to save her father, as he was tied up in a chair by Maltravers, and manage to put him on a lifeboat.

Tentacles notices that the Titanic is breaking in two and decides to rise out of the water, wrapping himself around the ship to keep it together. Several whales and dolphins arrive to help with the rescue. Elizabeth, Juan, Conners, Ronny, and Smiley jump off the now-perpendicular stern and jump into the water, and they are saved by a whale as well. Once everyone on the ship has been saved, the Titanic finally sinks, taking Tentacles with it and seemingly killing him, as he was still heroically holding on to it. In the morning, the passengers are taken aboard the RMS Carpathia. Conners mournfully remembers the heroes of the disaster, Camembert and Tentacles. 

Maltravers and his entourage are seemingly lost at sea, without Mr. Ice to help them and without Maltravers message having been sent. 

The Carpathia arrives in New York and disembarks the passengers. Elizabeth and Juan are married, as are Conners and Stella. Suddenly, everyone gets great news. Everyone runs to the Brooklyn Bridge and sees a huge flotilla of whales and dolphins in the harbor. It is revealed that the wonderful news and celebrating are because Tentacles and Camembert have both survived and are well. Everyone is happy and dances in the harbor, congratulating Tentacles for saving everyone in the disaster.

The film ends with old Conners and Stella back in modern-day New York and Conners telling his grandchildren that whales are still hunted.

==Cast==
*Gregory Snegoff – Maltravers / Mr. Ice
*Francis Pardeilhan – Don Juan
*Jane Alexander – Elizabeth
*Anna Mazzotti – Ronnie / Stella / Dolphin
*Sean Patrick Lovett – Top Connors / Camembert
*Teresa Pascarelli – Rachel Nick Alexander – Bob (uncredited)
*Frank von Kuegelgen – The Captain (uncredited)
*John Stone - (uncredited) Keith Scott - Geoffreys / Dolphin (uncredited)

==Sequel==
A sequel for the movie, Tentacolino, was made.

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 