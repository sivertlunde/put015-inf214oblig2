The Great Riviera Bank Robbery
 

The Great Riviera Bank Robbery (1979 in film|1979), also known as Dirty Money, is a motion picture  written and directed by Francis Megahy. Main actors Ian McShane, Warren Clarke, Stephen Greif, and Christopher Malcolm.

==Synopsis==

The Great Riviera Bank Robbery is based on a real-life event in 1976. A group of professional criminals team up with a fascist terrorist group known as "The Chain" to steal 15 million dollars worth of tourist money from a bank in a French resort town. 

==Miscellanea==

Two movies were made at the same time that both dealt with the same subject—a true life event of ex-firebrands using the sewers to rob a bank.

The French version, made by Jose Giovanni, (Les Egouts du Paradis = Sewers of Paradise) kept the heros real name but is rather listless. The "hero" becomes a nice guy, some kind of Arsene Lupin, who visits the old ladies in the hospital.

The English version, which features a better lead (Ian McShane billed as "Brain") and a more honest approach. The characters are fascists (anti communist) and their paramilitary activities are not passed over in silence—in the French attempt, all they show is weapons in the thieves den in the country.
When they left the vault, a parting message was written on the wall, which said; "we came with no malice; but went in peace".

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 

 