Truth about Kerry
 

{{Infobox film
| name           = Truth about Kerry
| image          = Truth about Kerry.jpg
| alt            = 
| caption        = 
| director       = Katherine Torpey
| producer       = Shaun OSullivan
| writer         = Shaun OSullivan Katherine Torpey
| starring       = Stana Katic Jessica Dean Darren Keefe
| music          = Ciaran Hope 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Truth about Kerry is a 2011 suspense film. It was written by Shaun OSullivan and Katherine Torpey, directed by Katherine Torpey, produced by Shaun OSullivan, Katie Torpey & Tony Zanelotti, and stars Stana Katic, Jessica Dean and Darren Keefe. The film was shot in County Kerry, Ireland, and in San Francisco, California, USA. The film is currently going on post production, and is scheduled to be released in the Film Festival

== Plot ==
Kerry Carlson goes to Ireland with her boyfriend, Hunter, to visit his childhood friend, Patrick. Two weeks later, Kerry is found dead on a beach. Her mysterious death has destroyed the world of her best friend Emma, who was supposed to go on the trip with her but canceled at the last minute. A few weeks after her death, Emma starts having nightmares about Kerry.

Emma travels to a remote village in Ireland to investigate the mysterious circumstances surrounding her best friend Kerry’s death. Distraught by constant dreams and haunting visions of Kerry, Emma refuses to believe that the death was an accident. After arriving in the small, fishing village where Kerry died, Emma realizes the death was never really investigated and the locals dont want to talk to her.  Her quest, to find out what really happened, targets one local villager named Patrick, who she believes may have been fatally obsessed with Kerry. 

Desperate to prove that Patrick was involved, it isn’t long before Emma believes that the village may be hiding more secrets than she thought, and that foul play may have led to Kerry’s death… or murder.

What Emma discovers is nothing compared to what audiences discover when the truth about Kerry’s death is finally revealed at the movie’s shocking conclusion.

== Cast ==
* Stana Katic as Emma
* Jessica Dean as Kerry
* Darren Keefe as Patrick
* Paul Hardiman as Joseph
* Ryan King as Daniel
* Rick Yudt as Hunter

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 