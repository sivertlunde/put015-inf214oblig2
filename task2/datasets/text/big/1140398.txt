Blue Thunder
 
{{Infobox film
| name           = Blue Thunder
| image          = Blue Thunder.jpg
| caption        = Theatrical release poster
| director       = John Badham
| producer       = Gordon Carroll Phil Feldman Andrew Fogelson
| writer         = Dan OBannon Don Jakoby
| starring = {{plainlist|
* Roy Scheider
* Warren Oates
* Candy Clark Daniel Stern
* Malcolm McDowell
}}
| music          = Arthur B. Rubinstein
| cinematography = John A. Alonzo
| editing        = Edward M. Abroms Frank Morriss
| studio         = Rastar 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $11 million  
| gross          = $42,313,354
}}
 action thriller the same Blue Thunder lasted 11 episodes in 1984. 

== Plot == Daniel Stern). The two patrol Los Angeles at night and give assistance to police forces on the ground.
 forthcoming Olympic games. With powerful armament, stealth technology that allows it to fly virtually undetected, and other accoutrements (such as infrared scanners, powerful microphones and cameras, and a U-Matic VCR), Blue Thunder appears to be a formidable tool in the war on crime.

But when the death of city councilwoman Diane McNeely turns out to be more than just a random murder, Murphy begins his own covert investigation. He discovers that a subversive action group is intending instead to use Blue Thunder in a military role to quell disorder under the project codename THOR (for "Tactical Helicopter Offensive Response"), and is secretly eliminating political opponents to advance their agenda.

Murphy suspects the involvement of his old wartime nemesis, former United States Army Colonel F.E. Cochrane (Malcolm McDowell), the primary test pilot for Blue Thunder. Murphy and Lymangood use Blue Thunder to record a meeting between Cochrane and the other government officials which would implicate them in the conspiracy. After landing, Lymangood secures the tape and hides it, but is captured upon returning to his home, interrogated, and killed while trying to escape. Murphy steals Blue Thunder and arranges to have his girlfriend Kate (Candy Clark) retrieve the tape and deliver it to the local news station, using the helicopter to thwart her pursuers.
 fighters are Hughes 500 helicopter, and after a tense battle, Murphy is able to shoot him down by executing a 360° loop through use of Blue Thunders turbine boost function. Murphy then destroys Blue Thunder by landing it in front of an approaching freight train, having deemed the tactical helicopter too dangerous to be used by anyone else.

==Cast==
* Roy Scheider as Officer Frank Murphy
* Malcolm McDowell as Colonel F.E. Cochrane
* Warren Oates as Captain Jack Braddock
* Candy Clark as Kate Daniel Stern as Officer Richard Lymangood
* Paul Roebling as Icelan 
* David Sheiner as Fletcher 
* Joe Santos as Montoya Anthony James as Grundelius 
* Ed Bernard as Sgt. Short 
* Jason Bernard as Mayor 
* Mario Machado as Himself 
* James Murtaugh as Alf Hewitt 
* Pat McNamara as Matusek  Jack Murdock as Kress 
* Clifford A. Pellow as Allen

==Production==
Co-writers Dan OBannon and Don Jakoby began developing the plot while living together in a Hollywood apartment in the late 1970s, where low-flying police helicopters awoke them on a regular basis. Their original script was a more political one, attacking the concept of a police state controlling the population through high-tech surveillance and heavy armament. They sought and received extensive script help from Captain Bob Woods, then-chief of the Los Angeles Police Department|LAPDs Air Support Division.

The first draft of the screenplay for Blue Thunder was written in 1979 and featured Frank Murphy as more of a crazy main character with deeper psychological issues, who went on a rampage and destroyed much of Los Angeles before finally falling to F-16s.  Filmed on location in Los Angeles during the early months of 1980, this was one of Warren Oatess last films before his death on April 3, 1982, which occurred during post-production nearly two years after principal filming had ended, and it is dedicated to him. He made one movie and one TV episode before and after filming during 1981-1982 that were released after Blue Thunder.
 Pavilions supermarket. 

Malcolm McDowell, who portrayed antagonist F. E. Cochrane, was intensely afraid of flying, and not even his then wife Mary Steenburgen could persuade him to overcome his phobia. In an interview for Starlog in 1983, Badham recalled, "He was terrified. He used to get out and throw up after a flight." McDowells grimaces and discomfort can be seen during the climactic battle between Murphy and Cochrane in the film. Steenburgen commented to filmmakers afterward, "I dont know how you got him up there, I cant even get him in a 747!" Donner, Greg.   Blue Thunder. Retrieved: April 10, 2012. 

===Blue Thunder helicopter===
  of Disneys Hollywood Studios, Florida]]
 

Designer Mickey Michaels invented the helicopters used in the film after reviewing and rejecting various existing designs. The helicopter used for Blue Thunder was a French-made Aérospatiale Aérospatiale Gazelle|SA-341G Gazelle modified with bolt-on parts and an Boeing AH-64 Apache|Apache-style canopy (aircraft)|canopy.  Two modified Gazelle helicopters, a Hughes 500 helicopter, and two F-16 fighter aircraft were used in the filming of the movie.  The helicopters were purchased from Aérospatiale by Columbia Pictures for $190,000 each and flown to Cinema Air in Carlsbad, California where they were heavily modified for the film. These alterations made the helicopters so heavy that various tricks had to be employed to make it look fast and agile in the film. For instance, the 360° loop maneuver Murphy performs at the end of the film, which catches Cochrane so completely by surprise that he is easily shot down by Murphys gunfire and killed, was carried out by a radio controlled model. 

==Reception==
Blue Thunder was released on May 13, 1983, and was the #1 ranked movie in the United States in its opening weekend, taking $8,258,149 at 1,539 theaters. It overtook Flashdance as the #1 movie that weekend. The movie was ranked #2 in its second and third weekends. Overall in the United States, it took $42,313,354 from 66 days on release. Blue Thunder was released in West Germany on February 5, 1983, before its United States release, then released worldwide between June–September 1983. Its UK release was August 25, 1983. It was released in East Germany and South Korea in 1984. Its international box office takings are unknown. The movie made $21.9 million in video rentals in the United States also. 

Blue Thunder received positive reviews, with an 84% rating on Rotten Tomatoes.  Variety (magazine)|Variety called it "a ripsnorting live-action cartoon, utterly implausible but no less enjoyable for that." 

==Cultural references== Daniel Sterns TV series, expurgated as "Just Another Frustrated Observer".

The Galaxie 500 song "Blue Thunder" takes its name from frontman Dean Warehams car, which was named after the helicopter.

==Video game== Worlds of Wonders short lived Action Max game system.  Using footage from the film, the player plays the pilot of the Blue Thunder helicopter as he tries to prevent the World Peace Coalition from being attacked by a terrorist organization.

==Remake==
Sony is developing a remake of Blue Thunder focusing on drone technology, with Dana Brunetti and Michael De Luca as producers, and Craig Kyle as writer. 

==See also==
* List of films featuring surveillance
* Airwolf
* List of American films of 1983
*  

==References==

===Notes===
 

===Bibliography===
 
* Farmer, James H. Broken Wings: Hollywoods Air Crashes. Missoula, Montana: Pictorial Histories Pub Co., 1984. ISBN 978-9999926515.
 

== External links ==
*  
*  
*  
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 