Steal (film)
{{Infobox film
| name           = Steal
| image          = StealFilm.jpg
| caption        =
| director       = Gérard Pirès
| producer       = Eric Altmeyer Nicolas Altmeyer Michael Cowan Jason Piette
| writer         = Mark Ezra  adapted by Gérard Pirès
| starring       = Stephen Dorff Natasha Henstridge Bruce Payne Steven Berkoff Clé Bennett Karen Cliche Steven McCarthy Alain Goulem Andy Gray
| cinematography = Tetsuo Nagata
| editing        = Véronique Lange
| distributor    =
| released       =  
| runtime        = 83 minutes
| country        = Canada France United Kingdom
| language       = English
| budget         = $15,000,000 (estimated)
}} 2002 action adventure film starring Stephen Dorff, Natasha Henstridge, Bruce Payne and Steven Berkoff. It was directed by Gérard Pirès and written by Mark Ezra and Gérard Pirès.

==Plot== skating and snowboarding. Led by Lieutenant Macgruder (Bruce Payne) The group evades capture from the police, but an anonymous individual seems to know who they are and threatens to inform the police unless they undertake a robbery for him. Enter the Mafia, represented by underworld enforcer Surtayne (Steven Berkoff), who instructs the group to work for them also or they will all be killed. Slim becomes romantically involved with Karen (Natasha Henstridge), (a  detective who distrusts Macgruder), and to save her and his friends escape from the threat of the anonymous man and the Mafia, Slim concocts a daring robbery.

==Cast==
* Stephen Dorff as Slim
* Natasha Henstridge as Karen
* Bruce Payne as Lt Macgruder
* Steven Berkoff as Surtayne
* Cle Bennett as Otis
* Karen Cliche as Alex
*Alain Goulem as Pandelis
*Andreas Apergis as Nixdorfer
*Tom McCamus as Jerry
*Andy Bradshaw as Sargeant Garret (as Anderson Chet Bradshaw)
*David Gow as Charlie / Brinks
*Jamie Orchard as Newsreader
*Stephen Spreekmeester as Eddie / Brinks
*Mariusz Sibiga	 as Police officer / Karens street

==Reception==
The film has a rating of 29% on the film review website Rotten Tomatoes  and 5.3 out of 10 on IMDB. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 