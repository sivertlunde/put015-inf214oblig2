So Long Letty
 
{{Infobox film
| name           = So Long Letty
| image          = So long letty.jpg
| image_size     = 
| caption        = 
| director       = Al Christie
| producer       = Al Christie
| writer         = 
| narrator       = 
| starring       = T. Roy Barnes Walter Hiers Grace Darmond Colleen Moore
| music          = 
| cinematography = 
| editing        = 
| distributor    = Christie Film Company
| released       = October 20, 1920
| runtime        = 
| country        = United States Silent (English intertitles)
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

So Long Letty is a 1920 silent American comedy film directed by Al Christie, and starring Grace Darmond, T. Roy Barnes, and Colleen Moore. So Long Letty was an adaptation of a popular stage comedy/musical of the same name.

==Story==
Harry Miller (Barnes) is a party boy who loves the cabaret scene and nights on the town while his wife Grace is a homebody, distressed by her husbands errant ways. Their neighbors are the opposite. Tommy Robbins (Walter Hiers) likes domestic life and home cooking while his wife Letty (Darmond) is devoted to the wild life. Harry and Tommy hatch a plan to solve their problems; that they divorce their wives and swap. The wives overhear the plan and go along with the suggestion, though following a plan of their own. They suggest a week-long trial period of platonic marriage, during which the wives do all they can to make their new potential mates miserable. In the end the husbands are happy with who theyve married.

==Remake== version stars Charlotte Greenwood in the titular role. Greenwood was the star of the original 1916 Broadway play.

==Bibliography==
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).

==External links==
 
*  

 
 
 
 
 
 
 
 


 