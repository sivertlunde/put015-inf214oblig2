Convicts 4
{{Infobox Film
| name           = Convicts 4
| image          = Convicts 4.jpg
| image_size     =
| caption        = 
| director       = Millard Kaufman
| producer       = A. Ronald Lubin
| based on       =  
| screenplay     = Millard Kaufman
| starring       = Ben Gazzara Stuart Whitman Vincent Price Rod Steiger
| music          = Leonard Rosenman
| cinematography = Joseph F. Biroc George White
| distributor    = Allied Artists Pictures Corporation
| released       = September 15, 1962
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
}}

Convicts 4 is a 1962 prison film drama starring Ben Gazzara and directed by Millard Kaufman.    The film is a fictionalized version of the life of death row convict John Resko, who wrote his autobiography: Reprieve.

== Plot summary ==
It is Christmas and John Resko (Ben Gazzara) wants to give his baby daughter a new teddy bear. He goes, without money, into a shop and tries to get the shopkeeper to give it to him saying he will pay him later. The shopkeeper refuses, Resko grabs a gun he saw in the till and points it at the man. The shopkeeper lunges at Resko and is shot. Resko is condemned to the electric chair at the age of eighteen.

Pardoned by the governor at the last minute, Resko is sentenced to Dannemora Prison, where he has difficulty adjusting to life behind bars. It becomes even less bearable after hearing that his wife (Carmen Phillips) has left him and that his father (Jack Kruschen) has died.

Resko does long stretches in solitary confinement. But he is befriended eventually by fellow convicts like Iggy (Ray Walston) and Wino (Sammy Davis Jr.) who help him to pass the time. When he takes up art as a hobby, Reskos work is seen by an art critic, Carl Carmer (Vincent Price), who believes him to have promise.

In 1949, after 18 years in prison, Resko is released. His daughter (Susan Silo) and granddaughter are waiting when he gets out.

==Cast==
* Ben Gazzara as John Resko 
* Stuart Whitman as Principal Keeper 
* Ray Walston as Iggy 
* Vincent Price as Carl Carmer 
* Rod Steiger as Tiptoes 
* Broderick Crawford as Warden 
* Dodie Stevens as Reskos Sister 
* Jack Kruschen as Reskos Father 
* Sammy Davis Jr. as Wino 
* Naomi Stevens as Reskos Mother 
* Carmen Phillips as Connie Resko 
* Susan Silo as Cathy (as an adult) 
* Timothy Carey as Nick 
* Roland La Starza as Duke 
* Tom Gilson as Lefty
* Jack Albertson as Art teacher

==References==
 

== External links ==
*  
*   
*  
*  

 
 
 
 
 
 
 
 
 
  
 