Stand By (film)
{{infobox film
| name = Stand By
| image = Stand By Hindi Film.jpeg
| caption = Poster
| director = Sanjay Surkar
| producer = Prakash Choube Sagar Choube
| story =
| writer =
| editor = Rajesh Rao
| cinematography = Sanjay Jadhav
| starring = Siddharth Kher Sachin Khedekar
| music = Aadesh Shrivastava
| studio = BRC Productions
| country = India
| language = Hindi
}}

Stand By is a 2011 Bollywood film directed by late Sanjay Surkar and produced by Sagar Choube and Prakash Choube. The film stars Siddharth Kher, Sachin Khedekar, Dalip Tahil and Avtar Gill in pivotal roles.

==Plot==
Story about two football players Rahul Narvekar and Shekhar Verma who shares a passion for football but neverthless the friends turns foes after the politics in the game of soccer arises.

==Cast==
* Siddharth Kher as Shekhar Verma
* Dalip Tahil as JP Verma (Shekhars dad)
* Adinath Kothare as Rahul Narvekar
* Sachin Khedekar as Damodar Narvekar (Rahuls dad)
* Avtar Gill as Shrivastava

==External links==
*  
*   at Bollywood Hungama

 
 
 
 
 
 


 