Faith like Potatoes
 
 
{{Infobox film
| name           = Faith Like Potatoes 
| image          = Faith Like Potatoes.jpg
| image_size     = 
| caption        = 
| director       = Regardt van den Bergh
| producer       = Frans Cronje Kirstin Barwise (line producer)
| writer         = 
| narrator       =  Sean Michael
| music          = Grant Innes McLachlan
| cinematography = Dave Aenmey
| editing        = Ronelle Loots
| distributor    = Affirm Films
| released       =  
| runtime        = 116 min.
| country        = South Africa Zulu English English
| budget         = 
| gross          = 
}}
Faith Like Potatoes is a 2006 South African biographical drama film based on the 1998 book written by Angus Buchan, "Faith Like Potatoes."  It is written and directed by Regardt van den Bergh, and stars Frank Rautenbach, Jeanne Wilhelm, Hamilton Dhlamini, and Sean Michael Cameron. The film follows Buchan and his family’s move from Zambia to South Africa and chronicles his Christian faith throughout that time. 

==Plot==

When Angus Buchan, a white Zambian farmer of Scottish origin, emigrates to escape political unrest and worrying land reforms, he looks south for a better life. With nothing more than a trailer in the untamed bush, and help from his Zulu foreman, Simeon Bhengu, the Buchan family struggles to settle in their new homeland. Faced with ever mounting challenges, hardships and personal turmoil, Buchan quickly spirals down into a life consumed by anger, fear and destruction. Finally, his wife convinces him to attend a local church, where the religious testimony of other farmers influence his decision to give his life to Jesus Christ. His outlook takes a complete turnaround, and supernatural occurrences begin to happen when Angus prays in faith. He begins giving his testimony in different towns, and eventually gathers thousands of people in Kings Park Stadium for a time of unified prayer for the nation and for the land.
Traditionally a maize and cattle farmer, Buchan decides to plant potatoes. Scientists had warned the farmers not to plant that season unless they had irrigation. Because of the unprecedented drought, planting potatoes would be a massive risk. Believing he is led by the Lord, he plants potatoes in the dry dust. When harvest time comes, there is a crop of giant potatoes.

==Production==
Faith Like Potatoes was produced by Frans Cronje from Global Creative Studios. Most of the filming took place at Shalom Farm, the famous Buchan farm in Kwa-Zulu Natal.

==Release and reception== 
The movie opened in cinemas in South Africa on 27 October 2006. 

Faith Like Potatoes was shown at several small film festivals including the Fort Lauderdale International Film Festival and the Australian International Film Festival. It was especially well received by Christian organizations, and won the audience choice award at the 2006 Sabaoth International Film Festival held in Milan, Italy.

Ted Baehr, chairman of the Christian Film and Television Commission, described the film as "Fireproof (film)|Fireproof" on steroids. 

==Soundtrack==
A soundtrack consisting of the songs played in this movie has been released, with the score by Grant Innes McLachlan. The title song for the movie, A New Day, was written and performed by South African artist Joe Niemand. 

==Home Media==
Faith Like Potatoes was released on DVD, December 2006 in South Africa, as well as in Australia, Italy, Germany and the UK. Sony Pictures released the DVD in April 2009 in North America and worldwide in July 2009. 

==References==
 

==External links==
* 
* 
*  at the  

 
 
 
 
 
 
 
 
 
 
 