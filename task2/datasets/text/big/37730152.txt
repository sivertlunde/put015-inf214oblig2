The Hidden Face (film)
{{Infobox film
| name     = The Hidden Face
| image    =
| director = Andrés Baiz
| producer = 
| writer   = 
| starring = Quim Gutiérrez   Clara Lago   Martina García
| cinematography = 
| music = Federico Jusid
| country = Colombia
| language = Spanish
| runtime = 97 minutes
| released =  
}}

The Hidden Face ( ) is a 2011 Colombian thriller film directed by Andrés Baiz. An official Bollywood remake of the film, titled Murder 3, was made in 2013.

==Story==
The Hidden Face is a psychological thriller that displays and analyzes the basic instincts, feelings of guilt, betrayal and fear that love can bring. The film opens with Adrián (Quim Gutierrez), a young orchestra conductor, viewing a video of his girlfriend, Belén (Clara Lago) telling him she is leaving him. Adrián becomes distraught. While drinking away his sorrows at a bar, he meets Fabiana (Martina Garcia) and they have a relationship where Fabiana moves into the house that Adrián was sharing with Belén. Adrián becomes a suspect in the disappearance of Belén, however, the investigators can find no evidence of Adriáns involvement in Beléns disappearance.

It is revealed that the house is owned by a German lady who shows Belén a secret room built to hide her husband just in case someone came to look for him because he was a former Nazi SS officer. The room is self-contained and sealed off from sound.

It is also shown that Belén, jealous of Adriáns relationship with one of his violinists, Verónica, decided to pretend she is leaving him. She creates the video saying she is leaving as she hides in the secret room. The room has some one way mirrors where she can observe Adriáns reaction. When she decides he has had enough she looks for the key and realizes she lost the key and is now trapped in the room with no way to contact Adrián.

Fabiana finds the key to the secret room, but she doesnt know what it is used for. Fabiana eventually figures out that Belén is trapped in the house because Belén is able to communicate through tapping on the pipes in the secret room. As Fabiana is ready to open the door, she pauses and decides not to rescue Belén because she might lose Adrián.

Fabiana struggles with her decision, but decides to open the door and check on Belén because she cant get a response from her. Also, one of the investigators gave Fabiana pictures of Adrián and Verónica and she herself feels the pangs of jealously. As Fabiana is checking on Belén laying in a bed in the secret room, Belén surprises Fabiana and knocks her out and leaves Fabiana locked in the room. Belén decides to leave the house. She leaves the key to the secret room on a bed for Adrián to find and leaves a picture of the two of them taped to the mirror that acts as the door to the secret room. The final scene shows Belén sitting on the beach alone and Fabiana trapped inside hoping to be rescued.

It was also revealed that one of the police investigators, who is still not convinced of Adriáns innocence in Beléns disappearance, is a former boyfriend of Fabiana, and he warns Adrián that if anything should happen to Fabiana he will kill Adrián. At the end of the movie the audience is thus left to wonder not only if Fabiana will eventually die in the hidden room, but whether Adrián will also be killed because of her disappearance.

== Cast ==
* Quim Gutierrez - Adrián
* Clara Lago - Belén
* Martina García - Fabiana
* Marcela Mar - Verónica
* Juan Alfonso Baptista - Agente de policía
* Alexandra Stewart - Emma

==References==
 

==External links==
*  

 
 
 
 
 