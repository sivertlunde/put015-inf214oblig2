Jail Yatra
{{Infobox film
| name           = Jail Yatra
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Bhappi Sonie
| producer       = Pravin Bali Vijay Maria
| writer         = 
| screenplay     = Sachin Bhowmick
| story          = 
| based on       =  
| narrator       = 
| starring       = Ashok Kumar Vinod Khanna Reena Roy
| music          = Rahul Dev Burman
| cinematography = Jal Mistry	
| editing        = M.S. Shinde
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}} Indian Bollywood film directed by Bhappi Sonie. It stars Ashok Kumar, Vinod Khanna and  Reena Roy in pivotal roles.

==Cast==
* Ashok Kumar...Ramnath Verma
* Vinod Khanna...Raju Verma
* Reena Roy...Shanu
* Nirupa Roy...Radha Verma
* Amjad Khan...Kuldeep Anwar Hussain...Police Inspector
* Heena Kausar...Roopa
* Dhumal_(actor)|Dhumal...Constable at the beach

==Soundtrack==

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bachna Raja Ji"
| Kishore Kumar
|-
| 2
| "Nahin Lagta Hai Dil"
| Lata Mangeshkar
|-
| 3
| "Yeh Jo Nazar Humari"
| Kishore Kumar, Lata Mangeshkar
|}
==External links==
* 

 
 
 
 
 
 

 