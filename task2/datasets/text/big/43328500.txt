Marika (film)
{{Infobox film
| name = Marika 
| image = 
| image_size =
| caption =
| director = Viktor Gertler
| producer = János Smolka 
| writer =   István Zágon  (play)   Armand Szántó    Mihály Szécsén
| narrator =
| starring = Pál Jávor (actor)|Pál Jávor   Lia Szepes   Zita Perczel   József Juhász
| music = József Radó 
| cinematography = István Eiben 
| editing =  
| studio = Budapest Film 
| distributor = 
| released =11 February 1938 
| runtime = 80 minutes
| country = Hungary Hungarian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} comedy drama film directed by Viktor Gertler and starring Pál Jávor (actor)|Pál Jávor, Lia Szepes and Zita Perczel. The film is based on a play by István Zágon, which was later adapted as the German film Marili. The sets were designed by Márton Vincze. After his wife dies, Orbán Sándor adopts his stepdaughter Marika. However, once she grows into a woman she falls in love with him.

==Cast==
*   Pál Jávor (actor)|  Pál Jávor as Orbán Sándor  
* Lia Szepes as Marika  
* Zita Perczel as Ella  
* József Juhász as Karády  
* Lidia Beöthy as Ella barátnõje  
* Zoltán Hosszú as János, komornyik  
* Béla Mihályffi as Színházigazgató  
* Ferenc Pethes as Tóni  
* Lajos Köpeczi Boócz as Kerényi,színiigazgató 
* István Falussy as Színész  
* Gusztáv Harasztos as Színházi szakember  
* Rezsö Harsányi as Gróf Kápolnay 
* Valéria Hidvéghy as Szobalány  
* Aladár Sarkadi 
* Irén Sitkey as Nusi, cukrászdáslány 
* Sándor Solymossy as Súgó  
* Lajos Ujváry as Színész  
* Anna Zöldhelyi as Kerényiné, Julia 

== External links ==
* 

 
 
 
 
 
 
 
 
 

 