Romanoff and Juliet (film)
 
{{Infobox film
| name           = Romanoff and Juliet
| image          = Poster of the movie Romanoff and Juliet.jpg
| caption        = Film poster
| director       = Peter Ustinov Walter Thompson
| writer         = Peter Ustinov
| starring       = Peter Ustinov
| music          = Mario Nascimbene
| cinematography = Robert Krasker
| editing        = Renzo Lucidi studio = Pavla Productions
| distributor    = Universal International
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
}}
 same name, which was itself based on Romeo and Juliet by William Shakespeare, released by Universal Pictures. Peter Ustinov wrote the screenplay, directed, and starred in the film. It co-starred John Gavin as Igor and Sandra Dee as Juliet.

==Plot==
A vote in the United Nations is deadlocked. The deciding vote goes to the obscure European republic of Concordia, whose President abstains because he does not understand the issues at play.

Juliet Moulsworth is in love with a young man called Freddie. She goes to live in Concordia, where her father is ambassador. She falls in love with Igor Romanoff, the son of the Russian ambassador to Concordia.

==Cast==
*Peter Ustinov as The General 
*Sandra Dee as Juliet Moulsworth 
*John Gavin as Igor Romanoff 
*Akim Tamiroff as Vadim Romanoff 
*Tamara Shayne as Evdokia Romanoff
*Alix Talton as Beulah 
*Rik Van Nutter as Freddie  John Phillips as Hooper Moulsworth  Peter Jones as Otto
*Tamara Shayne as Evdokia Romanoff  
*Suzanne Cloutier as Marfa Zlotochienka

==Production==
Ustinov first performed the play in 1956. Ustinov Directing Self in Own Film: Romanoff and Juliet Starts; an Interlude With Tina Louise
Scheuer, Philip K. Los Angeles Times (1923-Current File)   09 June 1960: A13.   The film was shot in Italy. 

==Awards==
Ustinov was nominated for the Golden Bear at the 11th Berlin International Film Festival    and the Directors Guild of America Award for Outstanding Directorial Achievement in Motion Pictures.

==References==
 

==External links==
*  
*  at Sandradeefans.com

 
 

 
 
 
 
 
 
 
 
 
 