Beauty (2011 film)
 
 
{{Infobox film
| name           = Beauty
| image          = Beauty film.jpg
| caption        = Theatrical release poster
| director       = Oliver Hermanus
| producer       = Didier Costet
| writer         = 
| starring       = Deon Lotz Charlie Keegan
| music          = Ben Ludik
| cinematography = Jamie Ramsay
| editing        = George Hanmer
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = South Africa
| language       = Afrikaans English
| budget         = 
}}
 Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Plot== sex encounters with other white and married men. 

At his daughters wedding party, he meets Christian once more (Charlie Keegan), a handsome young man, and He quickly becomes obsessed with Christian. François starts chasing Christian, learning everything he can about him. 

Eventually, his attraction for the young guy turns into hatred that seems poised to explode into violence.

==Cast==
* Deon Lotz as François
* Roeline Daneel as Anika
* Sue Diepeveen as Marika
* Charlie Keegan as Christian
* Albert Maritz as Willem
* Michelle Scott as Elena

==Critical reception== Empire Magazine also praised Lotz, and in his three out of five stars review summarized his verdict as "Despite that title, theres an ugly power to this study of obsession and anger." 

==Awards==
Beauty was awarded the Queer Palm Award at the 2011 Cannes Film Festival.  Actor Deon Lotz received a special mention in the 2011 Zurich Film Festival for his performance in the film. 

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of South African submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 