Chastity Bites
{{Infobox film
| image          = Chastity Bites.jpg
| alt            =
| caption        =
| director       = John V. Knowles
| producer       = Lotti Pharriss Knowles
| writer         = Lotti Pharriss Knowles
| starring       = Allison Scagliotti Francia Raisa Eddy Rioseco Greer Grammer Amy Okuda Louise Griffiths
| music          = Voodoo Highway
| cinematography = Justin Thomas Ostensen
| editing        = Phillip J. Bartell
| studio         = Weirdsmobile Productions
| distributor    = Grand Entertainment Group Gravitas Ventures
| released       =   }}
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Chastity Bites is a 2013 comedy-horror film written by Lotti Pharriss Knowles and directed by John V. Knowles.

== Plot ==
 
A feminist blogger attempts to save her friends from Elizabeth Báthory, who poses as an abstinence counselor in a high school.

== Cast ==
* Allison Scagliotti as Leah
* Francia Raisa as Katharine
* Eddy Rioseco as Paul
* Chloë Crampton as Kelly
* Greer Grammer as Nicole
* Sarah Stouffer as Britney
* Lindsey Morgan as Noemi
* Amy Okuda as Ashley
* Louise Griffiths as Liz Batho / Elizabeth Báthory 

Director Stuart Gordon appears in a cameo.

== Release ==
Chastity Bites premiered on June 1, 2013, at the Dances With Films film festival.   It was released on video on demand on November 1, 2013,  and on DVD on February 11, 2014. 

== Reception ==
Scott Hallam of Dread Central rated it 2/5 stars and wrote that despite "some fun moments", the film fails to live up to its potential.   Patrick Cooper of Bloody Disgusting rated it 3/5 stars and called it "a solid horror-comedy" that comes across as a TV show episode.   Elias Savada of Film Threat rated it 3/5 stars and called it a "delightfully cheesy horror comedy".   Gordon Sullivan of DVD Verdict called it a feminist film that favors comedy over horror. 

== References ==
 

== External links ==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 