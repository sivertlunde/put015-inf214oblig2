My Blood Runs Cold

{{Infobox film
| name           = My Blood Runs Cold
| image          = File:My_Blood_Runs_Cold_Poster.jpg
| caption        = Theatrical Poster.
| director       = William Conrad
| producer       = William Conrad
| writer     = John Mantley FILMLAND EVENTS: Mule for Marquesa Bought by Columbia
Los Angeles Times (1923-Current File)   25 Aug 1964: D7.   based on = story by John Meredyth Lucas
| narrator       =  Barry Sullivan
| music          = 
| cinematography = 
| editing        = 
| studio         = Warner Bros
| distributor    = Warner Bros 
| released       = 1965
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross = 
}}
My Blood Runs Cold is a 1965 American film starring Troy Donahue with Joey Heatherton as his love interest. It was directed by William Conrad. It was the second of three thrillers Conrad made for Warner Bros.
==Plot==
A young woman falls in love with a man who may be insane. The young man, Ben Gunther, believes he and the young woman, Julie Merriday, are the reincarnations of lovers from an earlier time. Ben convinces Julie of his beliefs and encourages her to run off with him. 

==Production==
The movie was a considerable change of pace for Donahue. It was shot on the Monterey Peninsula in late 1964. Trend: Are Movies Going to the Cats?: Chad, Jeremy Meet Beatles; Troy Donahue Going Psycho
Scheuer, Philip K. Los Angeles Times (1923-Current File)   12 Oct 1964: D18.   Troy Is Playing New Kind of Role
Los Angeles Times (1923-Current File)   02 Nov 1964: D22.  
==Reception==
===Box Office===
The film was profitable. Conrad Sees Green in Future
Thomas, Kevin. Los Angeles Times (1923-Current File)   06 July 1966: c9.  
===Critical===
The Washington Post called the film "woolly" and full of plotholes. A Fate Worse Than Insanity
By Richard L. Coe. The Washington Post, Times Herald (1959-1973)   10 Sep 1965: B37.   The New York Times called it a "wordy, bloodless little Warner chiller."  My Blood Runs Cold Brings Troy Donahue to the Palace
Thompson, Howard. New York Times (1923-Current file)   25 Mar 1965: 42.   "A blah Troy Donahue and a bad script spoil the show" said the Los Angeles Times. My Blood Runs Cold Taps Vein of Banality
Thomas, Kevin. Los Angeles Times (1923-Current File)   02 Apr 1965: D14.  
==References==
 
==External links==
* 
*  at TCMDB
*  at New York Times
*  at Cinema Retro
 
 
 