Jack Straw (1920 film)
{{infobox film
| name           = Jack Straw
| image          =
| imagesize      =
| caption        =
| director       = William C. deMille
| producer       = Adolph Zukor Jesse Lasky Elmer Harris (scenario)
| starring       = Robert Warwick Carroll McComas
| cinematography = L. Guy Wilky
| editing        =
| distributor    = Paramount Pictures
| released       = March 14, 1920
| runtime        = 5 reels; 4,707 feet
| country        = United States
| language       = Silent film(English intertitles)
}} John Drew and a young Mary Boland. In 1926 Paramount attempted a remake of this film called The Waiter from the Ritz which was begun and/or completed but never released. James Cruze directed and Raymond Griffith starred however the film if completed is now lost film|lost. The 1920 film survives at the Library of Congress.   

==Plot==
Based upon a review of the plot in a film publication,  Jack Straw (Warwick) is an iceman who becomes a waiter to be closer to the girl (McComas) he is interested in. Later, to impress her, he impersonates an Archduke from Pomerania. A Count from Pomerania (Brower) who is the ambassador arrives and learns of the long-missing son of royalty. The girls mother (Ashton) learns of the trick being played by Jack. Just when Jack is exposed as being a fraud, it turns out that he is the genuine article. The girls mother then gladly announces her daughters engagement to Jack.

==Cast==
*Robert Warwick - Jack Straw
*Carroll McComas - Ethel Parker Jennings Charles Ogle - Mr. Parker Jennings
*Irene Sullivan - Mrs. Wanley
*Monte du Mont - Ambrose Holland
*Frances Parks - Rose
*Lucien Littlefield - Sherlo
*Robert Brower - Count of Pomerania
*Sylvia Ashton - Mrs. Parker Jennings

unbilled
*Mayme Kelso - unknown role

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 