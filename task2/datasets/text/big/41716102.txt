The Mole Song: Undercover Agent Reiji
{{Infobox film
| name           = The Mole Song: Undercover Agent Reiji
| image          = The Mole Song.jpg
| alt            = 
| caption        = 
| director       = Takashi Miike
| producer       = Juichi Uehara Misako Saka Shigeji Maeda
| writer         = Kankuro Kudo
| based on       =   Takashi Okamura Riisa Naka Takayuki Yamada Yusuke Kamiji
| music          = Koji Endo
| cinematography = Nobuyasu Kita
| editing        = Kenji Yamashita OLM Shogakukan Toho
| distributor    = Toho
| released       =   
| runtime        = 130 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = US$20,392,349
}}
  is a 2013 Japanese film directed by Takashi Miike and based on the manga series Mogura no Uta. It was released in Japan on February 15, 2014. 

==Cast==
*Toma Ikuta as Reiji Kikukawa
*Shinichi Tsutsumi as Masaya Hiura  Takashi Okamura as Issei Nekozawa
*Riisa Naka as Junna
*Takayuki Yamada
*Yusuke Kamiji as Kenta Kurokawa

==Reception==
By March 30, the film had earned US$20,392,349 at the Japanese box office. 

The Hollywood Reporter s Deborah Young gave the film a positive review, calling it "an irresistible cops and yakuza romp".  Jay Weissberg of Variety (magazine)|Variety said the film "will be right up the alley of Takashi Miikes many fans". 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 