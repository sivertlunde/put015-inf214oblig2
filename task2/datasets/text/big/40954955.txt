Badsha the King
{{Infobox film
| name           = Badsha the King
| image          = Badsha The King 2004 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Saurav Chakraborty
| producer       = Suman Kumar Das
| writer         = 
| starring       =
| music          = Ashok Bhadra Snehasish Chakraborty
| cinematography = 
| editing        = 
| studio         = Burima Chitram
| distributor    = 
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}} Bengali film directed by Saurav Chakraborty and produced by Suman Kumar Das under the banner of Burima Chitram. The film features actors Prosenjit Chatterjee and Koel Mallick in the lead roles. Music of the film has been composed by Ashok Bhadra and Snehasish Chakraborty.   

== Cast ==
* Prosenjit Chatterjee
* Koel Mallick
* Mrinal Mukherjee
* Laboni Sarkar
* Dulal Lahiri
* Subhasish Mukhopadhyay
* Anamika Saha Rajesh Sharma
* Locket Chatterjee
* Shantilal Mukherjee

== Soundtrack ==
{{Infobox album  
| Name       = Badsha The King
| Type       = Soundtrack
| Artist     = Ashok Bhadra, Snehasish Chakraborty
| Cover      = 
| Alt        = 
| Released   = 2004
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = 
| Producer   = 
| Chronology = Ashok Bhadra
| Last album = Agni (2004)
| This album = Badsha The King (2004)
| Next album = Coolie (2004 film)|Coolie (2004)
| Misc       = {{Extra chronology 
 | Artist     = Snehasish Chakraborty
 | Type       = Soundtrack 
 | Last album = Mon Jake Chay (2004)
 | This album = Badsha The King (2004)
 | Next album = Anurag (2004)
 }}
}}

Ashok Bhadra and  Snehasish Chakraborty composed the film score of Badsha The King.

=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = 
| total_length    = 22:19
| title1          = Amader Ei Jibone
| length1         = 4:05
| title2          = Ei Mone Premer Kobita 
| length2         = 4:33
| title3          = Likhechi Amar Moner Kobita
| note3           = Performed by Shreya Ghoshal and Abhijeet Bhattacharya
| length3         = 4:56
| title4          = Sathi Eto Bhalobasa
| note4           = Performed by Shreya Ghoshal and Udit Narayan
| length4         = 4:24
| title5          = Tomar Choyay Eto Agun
| length5         = 4:21
}}

== References ==
 

 
 
 


 