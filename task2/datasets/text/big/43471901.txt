Puthu Paatu
{{Infobox film
| name           = Puthu Paatu
| image          =
| image_size     =
| caption        =
| director       = Panju Arunachalam
| producer       = Ilayaraja
| writer         = Panju Arunachalam
| screenplay     = Panju Arunachalam Suma Rajeev Rajeev
| music          = Ilayaraja
| cinematography = Rajarajan
| editing        = B. Lenin V. T. Vijayan
| studio         = Ilaiyaraaja Creations
| distributor    = Ilaiyaraaja Creations
| released       =  
| country        = India Tamil
}}
 1990 Cinema Indian Tamil Tamil film, Suma and Rajeev in lead roles. The film had musical score by Ilayaraja.  

==Cast==
 
*Ramarajan
*Vaidegi Suma
*Rajeev Rajeev
*Goundamani
*Senthil
*Senthamarai
*Ranjan
*Vaathiyar Raman
*A. Sukantala
*Vijaya Chandrika
*Devi
*Nizhalgal Ravi in Special Appearance
*Ramya Krishnan in Special Appearance Vaishnavi in Special Appearance
*Sivaraman
*Nagaraja Chozhan
*Karuppu Subbiah
*Vellai Subbiah
*Cenchi Krishnan
*M.L.A. Thangaraj
*Thideer Kanniah
*Periya Karuppu Thevar
*Kuchi Babu
*Balu
*Boopathi Raja
*Jagadeesan
*Kaalai
*Nethaji
 

==Soundtrack==
The music was composed by Illayaraja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Enga Ooru Kadhala || Illayaraja, Asha Bhosle || Gangai Amaran || 04:38
|-
| 2 || Bhoomiye Enga || Mano (singer)|Mano, S. Janaki || Panju Arunachalam || 04:50
|- Chitra || Gangai Amaran || 04:24
|- Chitra || Vaali || 04:29
|- Chitra || Vaali || 00:46
|- Chitra || Vaali || 00:35
|-
| 7 || Ich Libe Dich || S. Janaki || Gangai Amaran || 03:13
|- Vaali || 04:32
|- Chitra ||  || 00:51
|-
| 10 || Senjanthu ||  || || 00:57
|}

==References==
 

==External links==
*  

 
 
 
 
 


 