The Tell-Tale Heart (1941 film)
 
{{Infobox film
| name           = The Tell-Tale Heart
| image          = TellTaleHeartTitleScreen.JPG
| caption        = Title screen
| director       = Jules Dassin
| producer       = 
| writer         = Doane R. Hoag Based on the short story by Edgar Allan Poe
| starring       = Joseph Schildkraut Roman Bohnen
| music          = Sol Kaplan
| cinematography = Paul Vogel
| editing        = Adrienne Fazan
| studio         =  MGM
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 short story of the same title by Edgar Allan Poe.
 directorial debut after working as an assistant to Alfred Hitchcock and Garson Kanin.  It is typical of the short film adaptations of literary classics studios produced to precede the feature film during the 1930s and 1940s.  

==Plot==
After years of being subjected to verbal and emotional abuse by his master, a young weaver decides to murder him. Before the elderly man dies, he predicts his killer eventually will succumb to an overwhelming sense of guilt and betray himself.

Shortly after the mans death, the weaver begins to hear various sounds - a ticking clock, a dripping faucet, and rain falling into a metal pan outside the window - that convince him he can hear his victims heart still beating beneath the floorboards where he buried him. When two deputy sheriffs appear at the house the following day, he confesses to his crime to clear his tortured conscience.  

==Cast==
*Joseph Schildkraut as Young Man
*Roman Bohnen as Old Man
*Oscar OShea as First Deputy Sheriff Will Wright as Second Deputy Sheriff

==Critical analysis==
In an article about Jules Dassin written the week of his death, Time (magazine)|Time film critic Richard Corliss called The Tell-Tale Heart "possibly the very first movie to be influenced by Citizen Kane&nbsp;... This short film&nbsp;... is positively a-swill in Orson Wellesian tropes: the crouching camera, the chiaroscuro lighting, the mood-deepening use of silences and sound effects."  

==DVD release==
The film is a bonus feature on the Region 1 DVD box set The Complete Thin Man Collection, released by Warner Home Video on August 2, 2005.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 