Dolma (film)
 
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Dolma
| image          = Poster of short film Dolma.jpg
| caption        = Original film poster 
| director       = Jim Sanjay
| writer         = Jim Sanjay
| starring       = Deden, Kelsang, Rinchen Tashi
| music          = C Rajesh
| cinematography = Bernard David
| editing        = 
| studio         = Shepherd India Media Private Limited
| distributor    = 
| released       =  
| country        = India Tibetan
| budget         = $10,000
}} Tibetan childrens Tibetan Settlement Camp in Bylakuppe 85&nbsp;km from Mysore in Karnataka. All the actors and voices used in this film are Tibetan. Deden plays the main character of a small innocent girl named Dolma.

== Plot ==
The central character of this film is a small innocent girl named Dolma who wrote a secret letter to God, and wants to deliver it. However, she has a problem. She does not know the address of God. She sets out to find Gods address, first approaching her family members. Her busy mom insists that she finish her homework and concentrate on school rather than this kind of unnecessary conversation. Her father orders her to pay attention to her mothers advice. Grandfather advises her to work on her goodness before she can even think of sending a letter to God. Sister totally ignores her.

Distracted by the letter and her desire to send the letter to God, she misses her school bus. She is disappointed and now has a whole day to think about what to do with the letter. She meets a Muslim mullah, a Buddhist monk and a Church pastor. They all fail to give her Gods address. Her patience gives away. She breaks into crying. And then, the story reaches its climax. Does she ever deliver her letter to God? To find out this you need to watch the film.

==References==
 
* http://www.thehindu.com/arts/cinema/god-this-is-dolma/article4126587.ece
* http://timesofindia.indiatimes.com/videos/entertainment/regional/tamil/Dolma--Short-film-Press-meet/videoshow/17217528.cms
* http://chennaionline.com/video/movies/Dolma-Short-Film-Launch/3802.col
* http://www.pr4india.com/dolma---tibetan-film-made-in-india_24800.htm
* http://www.indiaglitz.com/channels/tamil/Events/37782.html
* http://pluzmedia.in/galleries/kollywood/66989/dolma-short-film-press-meet-pictures/2.htm
* http://www.indiaglitz.com/channels/tamil/flashslide.asp?cid=37782&category=3