Road to Singapore
 
{{Infobox film
| name           = Road to Singapore
| image          = RoadToSingapore 1940.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Victor Schertzinger
| producer       = Harlan Thompson
| screenplay     = {{Plainlist| Frank Butler
* Don Hartman
}}
| story          = Harry Hervey
| starring       = {{Plainlist|
* Bing Crosby
* Bob Hope
* Dorothy Lamour
}}
| music          = Victor Young
| cinematography = William Mellor
| editing        = Paul Weatherwax
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.6 million 
}}

Road to Singapore is a 1940 American comedy film directed by Victor Schertzinger and starring Bing Crosby, Bob Hope, and Dorothy Lamour. Based on a story by Harry Hervey, the film is about two playboys trying to forget previous romances in Singapore, where they meet a beautiful woman. Distributed by Paramount Pictures, the film marked the debut of the long-running and popular "Road to …" series of pictures spotlighting the trio, seven in all.

==Plot==
Josh Mallon (Bing Crosby) and Ace Lannigan (Bob Hope) are best friends and work aboard the same ship. As their ship returns to the US after a long voyage, they see all the other sailors being mistreated by their wives and girlfriends, and the two friends pledge never to get involved with women again. Unfortunately, this vow is tested almost immediately. First, Ace is confronted by the family of a former lover, Cherry, who insist he marry her. Then Josh, who is the son of rich shipping magnate (Charles Coburn), has to fend off his fiancee, Gloria (Judith Barrett), and his fathers wishes that he settle down and take over the family business.

Things get worse when Josh and Ace get caught up fishing and turn up late for a party to celebrate Joshs engagement. Glorias hostile drunken brother starts a fistfight and a news reporter takes photographs that cause a scandal. Josh and Ace flee to Hawaii and then head for Singapore.

However, the pair only get as far as the island of Kaigoon before their money runs out. They rescue Mima (Dorothy Lamour), an exotic local (but not native) from her abusive dance-partner, Caesar (Anthony Quinn), and she moves into their hut. Soon Mima is running the two mens lives, much to their chagrin. The trio try to make money in several different ways, including trying to sell a spot remover that is so bad it dissolves clothes.

When Joshs father finally locates his wayward son, he and Gloria fly out to bring Josh back to face his responsibilities. The resentful Caesar leads them to where Ace, Josh and Mima are enjoying a local feast. By this point, both Josh and Ace have fallen in love with Mima. She is heartbroken to learn that Gloria is Joshs fiancee.

Ace proposes to Mima, but before she can accept, Josh returns. The two friends almost come to blows over Mima, but then decide that she should choose between them. Mima picks Ace. Josh boards an ocean liner with Gloria and his father.

Meanwhile, Caesar informs the local police that Ace is on the island illegally. Ace is arrested when he cannot produce a passport, but manages to escape. He and Mima flee aboard a ship, but Ace comes to realize that Mima really loves Josh.

When Joshs ship docks at a tropical port, a passenger complains about a terrible spot remover that disintegrated his suit jacket. Josh realizes that Ace and Mima must be on the island. When he finds them, Ace tells his best friend that Mima really loves him.

==Premiere== Paramount Theatre in New York City. Tommy Dorseys orchestra (with Frank Sinatra) highlighted the accompanying stage show.  

==Running gags==
The "Road to …" series of films had several running gags that appeared in nearly every movie. Most of these originated in Road to Singapore. These include:

* Pat-a-cake – Ace and Josh play patty-cake as a distraction before starting a fistfight
* References to Bings waistline (in this movie, Crosby himself pokes fun at his spare tire)
* Confidence tricks – the two main characters are usually con-men, although in this movie it is not their starting profession

==Cast==
* Bing Crosby as Josh Mallon V
* Dorothy Lamour as Mima
* Bob Hope as Ace Lannigan
* Charles Coburn as Joshua Mallon IV
* Judith Barrett as Gloria Wycott
* Anthony Quinn as Caesar Jerry Colonna as Achilles Bombanassa

==Production==
According to Hope biographer Raymond Strait, the project which became Road to Singapore was first offered to Fred MacMurray and Jack Oakie (under the working title of Road to Mandalay), and after they declined, to George Burns and Gracie Allen (as Beach of Dreams), with a second male lead to be determined. They also declined.  (Burns is quoted as saying that Gracie "thought the whole thing was silly.")  At this point, Paramount decided to pair Crosby with Hope, and to take advantage of the screen popularity of Lamour, who had already made several lucrative pictures with a "South Seas" theme.  Crosby and Hope had recently appeared live together in Hollywood beforehand, amazing onlookers with how smoothly they worked together, but their audience didnt realize that theyd briefly performed together on the vaudeville stage years earlier, getting a few routines down pat. 
 Frank Butler and Don Hartman and directed by Victor Schertzinger, much of the material was ad libbed by Hope and Crosby or surreptitiously contributed by their own writing staffs (including Sid Kuller and Ray Golden).

This was the only installment of the series in which Hope was billed third, under Dorothy Lamour.  After this picture, the billing order remained Bing Crosby, Bob Hope, and Dorothy Lamour until The Road to Hong Kong more than two decades later, in which Lamour was replaced with Joan Collins and relegated to a smaller role when Crosby insisted on a younger leading lady.  Hope fought to get her into the picture, albeit in a reduced capacity.
 

==Soundtrack== Johnny Burke and Victor Schertzinger) by Bob Hope and Bing Crosby
* "The Moon and the Willow Tree" (Burke and Schertzinger) by Dorothy Lamour
* "Sweet Potato Piper" (Burke and James V. Monaco) by Bing Crosby, Dorothy Lamour, and Bob Hope
* "Too Romantic" (Burke and Monaco) by Bing Crosby and Dorothy Lamour
* "Kaigoon" (Burke and Monaco) by the chorus

==Copyright==
 
As a result of EMKA, Ltd.s acquisition of the pre-1950 Paramount library (which includes this and the following three "Road" pictures) and the later transfer of rights to the fifth and sixth films to FremantleMedia and Columbia Pictures Television, Paramount would end up losing the rights to all the "Road" pictures it originally produced (the last film, The Road to Hong Kong, was produced and released by United Artists, who retain the rights to the film to this day).

The copyright to Road to Singapore was renewed in a timely manner by EMKA. Originally registered for copyright as LP9497 with a declared publication date of March 22, 1940, the continuation of copyright was contingent upon renewal between the 27th and 28th anniversaries of that date. Renewal occurred March 31, 1967, number R407858. Although the film opened a week prior to the publication date, the renewal is still timely even if the earlier date were considered publication date. Renewal was filed by EMKA, Ltd., today part of NBC Universal Television Distribution, so thus Universal Studios now handles theatrical and home video distribution. The copyright is now scheduled to run until 95 years after the publication date (2035). The film has not entered the public domain.

==Reception== radio show.

==References==
;Citations

 
;Bibliography
 
* Strait, Raymond (2003). Bob Hope: A Tribute. New York: Pinnacle Books, ISBN 0-7860-0606-4.
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 