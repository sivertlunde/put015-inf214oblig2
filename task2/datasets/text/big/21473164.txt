Ready? OK!
{{Infobox Film
| name           = Ready? OK!
| image          = DVD Cover Art for Ready? Ok! Film.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = James Vasquez Mark Holmes
| writer         = James Vasquez
| starring       = Michael Emerson Carrie Preston Lurie Poston John G. Preston Kali Rocha Tara Karsian Sandra Ellis-Troy Sam Pancake
| music          = Lance Horne
| cinematography = Elizabeth Santoro
| editing        = James Vasquez
| studio         = Daisy 3 Pictures
| distributor    = Wolfe Releasing
| released       =      
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
}}
Ready? OK! is a 2008 comedy film written, edited, and directed by James Vasquez, and produced by Daisy 3 Pictures. 

==Plot== wrestling team, Joshua strives to be on the cheerleading squad. When students are told to come to school dressed as an influential role model, Joshua chooses Maria von Trapp. The film explores Andreas internal battle of wanting her son to be normal, and embracing his individuality and accepting him how he is.  

==Cast==
* Michael Emerson as Charlie New
* Carrie Preston as Andrea Dowd
* Lurie Poston as Joshua Alexander "Josh" Dowd
* John G. Preston as Alex Dowd
* Kali Rocha as Halle Hinton
* Tara Karsian as Sister Vivian
* Sandra Ellis-Troy as Emily Dowd
* Sam Pancake as Mabel
* Stephanie DAbruzzo as Normal Heights vocalist

==Production==
The film is loosely based on a short film of the same name. The short film is available on the DVD, First Out 3.

==Awards==
* Best US Narrative Feature Film: 2008 FilmOut San Diego
* Best Actress (Carrie Preston): 2008 FilmOut San Diego
* Outstanding Emerging Talent (James Vasquez): 2008 FilmOut San Diego 2008 NewFest NYC
* Best Male Feature Film: 2008 North Carolina GLFF
* Best Narrative Feature Film (Jury Award): 2008 Seattle Lesbian & Gay Film Festival

==Release==
* FilmOut San Diego - April 17, 2008
* Q Cinema Fort Worth Gay and Lesbian International Film Festival - June 1, 2008 2008 NewFest NYC - June 7, 2008
* Frameline Film Festival - June 24, 2008
* Cinequest Film Festival - March 6, 2009

Ready? OK! premiered on television on November 15, 2009 on the cable LGBT-related channel Logo (TV channel)|Logo.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 