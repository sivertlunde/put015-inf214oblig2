The Unknown Known
 
 
{{Infobox film
| name           = The Unknown Known
| image          = The Unknown Known poster.jpg
| caption        = Theatrical release poster
| director       = Errol Morris
| producer       = 
| writer         = 
| starring       = Errol Morris
| music          = Danny Elfman
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $301,604   
}}
 Secretary of Defense Donald Rumsfeld, directed by filmmaker Errol Morris.

It premiered at the Telluride Film Festival on August 29, 2013. The film was screened in the main competition section at the 70th Venice International Film Festival.        It has an 84% "Certified Fresh" rating on the critical review aggregation website Rotten Tomatoes.   

In the review for Philly.com, Tirdad Derakhshani gives the film two out of fours stars and states, "Morris tries to hold Rumsfeld to account for the blunders, cover-ups, and atrocities critics say were committed during his watch.... Yet we get little in response from Rumsfeld but a demonstration of his cunning at parrying, dodging, and twisting queries."   

==Cast==
* Donald Rumsfeld – (interviewee) himself
* Kenn Medeiros – younger Donald Rumsfeld / Secret Service
* Errol Morris – (interviewer, voice) himself

==References==
 

==External links==
*  
* 
* 
*  
*  
*   at RogerEbert.com

 

 
 
 
 
 
 
 
 
 

 