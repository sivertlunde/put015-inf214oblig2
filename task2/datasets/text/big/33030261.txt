Around the Boree Log
 
 
{{Infobox film
| name           = Around the Boree Log
| image          = 
| image_size     =
| caption        = 
| director       = Phil K. Walsh
| producer       = 
| writer         = Phil K. Walsh
| based on       = poems of Patrick Joseph Hartigan
| narrator       =
| starring       = Molly ODonohue 
| music          =
| cinematography = Lacey Percival
| editing        = 
| studio = Phil K. Walsh Productions
| distributor    = 
| released       = 25 September 1925 
| runtime        = 7,100 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Around the Boree Log is a 1925 Australian silent film adapted from the poems of "John OBrien" (Patrick Joseph Hartigan). It tells stories of a priests life around the 1870s in the Goulburn area. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 128.  

Unlike many Australian silent films, a copy of it survives today.

==Plot==
A priest reads from the book of poems by John OBrien and recalls his earlier life in the country. He remembers travelling hawkers, his first school, a bishop inspection, childhood romance, and the marriage of a girl to another man.

==Cast==
*Molly ODonohue as Laughing Mary

==Production==
The movie was shot on location in the New South Wales bush, mostly at the Wollondilly River area near Goulburn, in early 1925.  The director had previously worked as an assistant on While the Billy Boils (1921) and would direct The Birth of White Australia (1928).  He also tried to make a film of the novel Love Blind but was unable to raise finance. 

Most of the cast were anonymous people who lived in the area.   

==Release==
The film met with resistance from distributors who felt it was Roman Catholic propaganda.  It was also criticised for having little plot and consisting mostly of a travelogue of scenery and incidents in the country. 

However it did screen throughout Australia. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
* 

 
 
 
 
 


 