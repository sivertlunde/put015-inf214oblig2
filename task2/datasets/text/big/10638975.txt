Prem Pujari
{{Infobox film
| name           = Prem Pujari
| image          = Prem Pujari.jpg
| image_size     = 
| caption        = 
| director       =Dev Anand
| producer       =Dev Anand
| writer         =Dev Anand
| narrator       = 
| starring       = Dev Anand Waheeda Rehman Shatrughan Sinha Ashok Kumar Madan Puri
| music          = S. D. Burman   Neeraj (Lyrics)
| cinematography = Fali Mistry
| editing        = Babu Shaikh
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
}}
 1970 Bollywood film produced, directed and written by Dev Anand for Navketan films.    The movie stars Anand, Waheeda Rehman, Shatrughan Sinha, Prem Chopra, Madan Puri, Amrish Puri. It has several popular songs composed and directed by S. D. Burman.   

==Plot summary==

Ramdev Bakshi (Dev Anand) is a peaceful, quiet man who is only interested in wildlife and nature. He is sent then to the Indo-Chinese border in the army by his father, a war hero. Here he refuses to lift his gun and attack. He is then court-martialled. Later he is captured by the Chinese and humiliated, but when a beautiful Chinese agent promises him a better future by working and spying for her (against India), he agrees but foils their wicked plans by spying over them. Later the Indo-Pakistani War of 1965 breaks out. He joins in the action to show what he is made of by killing Pakistani soldiers hiding in the bushes in an Indian village in the Khemkaran sector.

==Cast==
*Dev Anand ...  Ramdev Bakshi / Peter Andrews / Yik Tok
*Waheeda Rehman ...  Suman Mehra
*Shatrughan Sinha ...  Pakistani Army Officer
*Zaheeda ...  Rani Chang / Mrs. Andrews / Mrs. Yuk Tok
*Prem Chopra ...  Bilkis Mohammed (Billy)
*Ashok Kumar ... Kulbhushan Nasir Hussain ...  Retd. Subedar Major Durgaprasad Bakshi
*Madan Puri ...  Chang
* Sajjan ...  Sumans maternal uncle
* Amrish Puri ... Henchman in church of Spain. Sachin ...  Sunder
*Siddhu ...  Chamanlal

==Location==
The film was shot in Switzerland and has Grimsel Hotel in one of the songs. Some part of movie was also shot in Bihar near a famous tourist place Rajgir at India. Here Shatrughan Sinha was picked to play his part from the gathered public. the prem pujari also shoot at astagaon near shirdi ahmednagar district of maharastra.

==Music== Navketan favourite song.   

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)

|-
| 1
| "Rangeela Re Tere Rang Mein"
| Lata Mangeshkar
|-
| 2
| "Shokhiyon Mein Ghola Jaye "
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Phoolon Ke Rang Se"
| Kishore Kumar 
|-
| 4
| "Taqat Watan Ki Humse HaI"
| Mohammed Rafi, Manna Dey 
|-
| 5
| "Prem Ke Pujari Hum Hai"
| S D Burman 
|-
| 6
| "Yaaron Neelam Karo Susti "
| Kishore Kumar, Bhupinder Singh 
|-
| 7
| "Dungi Tenu Reshmi Rumal "
| Lata Mangeshkar 
|}

==Reception==
While Prem Pujari did not do well at the box office. However, the soundtrack, which became a success was termed as "superlative" and as a "brilliant score" by author Khagesh Dev Burman. The songs are known for the purity of their Urdu and Hindi languages and are regarded as "evergreen" classics.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 