The Double-D Avenger
 American comedy film by William Winckler. {{cite book
|title= Doug Pratts DVD: Movies, Television, Music, Art, Adult, and More!, Volume 1
|last= Pratt
|first= Douglas
|authorlink= Douglas Pratt
|coauthors=
|year= 2001
|publisher= UNET 2 Corporation
|location=
|isbn= 1-932916-00-8
|page= 357
|pages=
|url= http://books.google.com/books?id=DTUw1SDQECoC&pg=PA357&dq=The+Double-D+Avenger&hl=en&ei=d6FzTOzGDYGBlAfurpjJCA&sa=X&oi=book_result&ct=result&resnum=2&ved=0CC4Q6AEwAQ#v=onepage&q=The%20Double-D%20Avenger&f=false}}  

This film, first released in fall 2001, written, produced and directed by cult filmmaker William Winckler, is a campy spoof of "Wonder Woman" about a costumed superwoman who uses her super bust to fight crime.  
 Haji (Faster Pussycat! Kill! Kill!) and Raven De La Croix (Up! (1976 film)|Up!).

The story focuses on big busty Chastity Knott (Natividad) who must use her new amazing abilities as the super-stacked costumed crime fighter, The Double-D Avenger, to stop villainous bikini bar owner Al Purplewood (Larry Butler) and his sexy, murderous strippers. The movie is full of live-action comic-book action, sexual innuendo, Russ Meyer-inspired camera angles focusing on the female characters breasts, and other "off color" verbal and visual gags. Remarkably, there is no nudity in the picture. 

Variety critic Dennis Harvey gave the film a very positive review noting its "a cheerfully silly ode to larger-than-life femininity."

The fun film runs 77 minutes, in color, and is suggested for audiences over 18. It has been a
success in the U.S. theatrically and on video and DVD, and is also a hit internationally, with a French-language version in France ("Double D Avenger Les Appas De La Justiciere") and a Japanese-language version in Japan ("The Double-D Avenger / MegaPie Oba Ranger"). A second DVD version was released by Elite Entertainment, titled "Joe Bob Briggs Presents The Double-D Avenger," which features audio commentary by famous drive-in movie critic Joe Bob
Briggs. A special DVD two-pack has also been released by Elite, "A Double Dose of Joe Bob Briggs," featuring the Joe Bob commentary version of "The Double-D Avenger" packaged with Joe Bobs commentary on "Jesse James Meets Frankensteins Daughter."

Key Cast & Crew include: Kitten Natividad, Haji, Raven De La Croix, G. Larry Butler ("Frankenstein Vs. the Creature From Blood Cove"), Bill Winckler, Forrest J Ackerman, Mimma Mariucci, Sheri Dawn Thomas, Lunden De Leon.  Cinematographer/Editor Raoul J. Germain Jr., Writer/Producer/Director William Winckler. A William Winckler Productions, Inc., production.

== References ==
 
*  

 
 
 