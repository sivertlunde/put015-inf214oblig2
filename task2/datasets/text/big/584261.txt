Carry On at Your Convenience
 
 
{{Infobox film| name = Carry on at Your Convenience
  | image = Carry-On-At-Your-Convenience.jpg
  | caption = Carry on at Your Convenience (DVD)
  | director = Gerald Thomas
  | producer = Peter Rogers
  | writer = Talbot Rothwell Charles Hawtrey Joan Sims Hattie Jacques Bernard Bresslaw Kenneth Cope Jacki Piper Eric Rogers
  | cinematography = Ernest Steward
  | editing = Alfred Roome
  | distributor = Rank Organisation
  | released = December 1971
  | runtime = 90 minutes
  | language = English
  | country = United Kingdom
  | budget = £220,000
  }} Charles Hawtrey, Joan Sims, Hattie Jacques and Bernard Bresslaw. It features Kenneth Cope in the first of his two Carry on appearances.

==Plot== union representative football match. Sid Plummer (Sid James) is the site foreman bridging the gap between workers and management, shrewdly keeping the place going amid the unrest.
 Charles Hawtrey) has included a bidet in his latest range of designs, but W.C. objects to the manufacture of such "distasteful" items. W.C. wont change his stance even after his son, Lewis (Lew) Boggs (Richard OCallaghan), secures a large overseas order for the bidets. It is a deal that could save the struggling firm, which W.C. has to admit is in debt to the banks.

Vics dim stooge Bernie Hulke (Bernard Bresslaw) provides bumbling assistance in both his union machinations and his attempts to woo Sids daughter, factory canteen worker Myrtle (Jacki Piper). She is torn between Vic and Lew Boggs, who is something of a playboy but insists he loves her.

Sids wife is Beattie (Hattie Jacques), a lazy housewife who does little but fuss over her pet Budgerigar|budgie, Joey, which refuses to talk despite her concerted efforts. Their neighbour is Sids brassy and lascivious co-worker Chloe Moore (Joan Sims). Chloe contends with the endless strikes and with her crude, travelling salesman husband Fred (Bill Maynard), who neglects her and leaves her dissatisfied. Chloe and Sid enjoy a flirtatious relationship and are sorely tempted to adultery|stray. Unusually for Sid James, his character is a faithful husband, albeit a cheeky and sorely tempted one.
 horseraces – bookie after making several payouts.

The strikers finally return to work, but it is only to attend the annual works outing, a coach trip to Brighton. A good time is had by all with barriers coming down between workers and management, thanks largely to that great social lubricant, alcohol. W.C. becomes intoxicated and spends the day – and it seems the night – with his faithful, adoring secretary, Miss Hortense Withering (Patsy Rowlands). Lew Boggs manages to win Myrtle from Vic Spanner, giving his rival a beating, and the couple elope. After arriving home late after the outing and with Fred away, Chloe invites Sid in for a cup of tea. They fight their desires and ultimately decide not to have the tea fearing that neighbours might see Sid enter Chloes home and get the wrong idea.

At the picket lines the next day, Vic gets his comeuppance – partly at the hands of his mother (literally, as she spanks him in public) – and the workers and management all pull together to produce the big order to save the firm.

==Cast==
*Sid James as Sid Plummer
*Kenneth Williams as WC Boggs Charles Hawtrey as Charles Coote
*Hattie Jacques as Beattie Plummer
*Joan Sims as Chloe Moore
*Bernard Bresslaw as Bernie Hulke
*Kenneth Cope as Vic Spanner
*Jacki Piper as Myrtle Plummer
*Richard OCallaghan as Lewis Boggs
*Patsy Rowlands as Hortence Withering
*Davy Kaye as Benny
*Bill Maynard as Fred Moore
*Renée Houston as Agatha Spanner
*Marianne Stone as Maud
*Margaret Nolan as Popsy
*Geoffrey Hughes as Willie
*Hugh Futcher as Ernie
*Simon Cain as Barman
*Amelia Bayntun as Mrs Spragg
*Leon Greene as Chef
*Harry Towb as Film doctor
*Shirley Stelfox as Bunny waitress
*Peter Burton as Hotel manager
*Julian Holloway as Roger
*Anouska Hempel as New canteen girl
*Jan Rossini as Hoopla girl
*Philip Stone as Mr Bulstrode

==Crew==
*Screenplay – Talbot Rothwell Eric Rogers
*Production Manager – Jack Swinburne
*Art Director – Lionel Couch
*Editor – Alfred Roome
*Director of Photography – Ernest Steward
*Camera Operator – James Bawden
*Make-up – Geoffrey Rodway
*Continuity – Rita Davidson
*Assistant Director – David Bracknell
*Sound Recordists – Danny Daniel & Ken Barker
*Hairdresser – Stella Rivers
*Costume Designer – Courtenay Elliott
*Set Dresser – Peter Howitt
*Assistant Art Director – William Alexander
*Dubbing Editor – Brian Holland
*Titles – GSE Ltd
*Processor – Rank Film Laboratories
*Toilets – Royal Doulton Sanitary Potteries
*Assistant Editor – Jack Gardner
*Producer – Peter Rogers
*Director – Gerald Thomas

==Filming and locations==

*Filming dates – 22 March-7 May 1971

Interiors:
* Pinewood Studios, Buckinghamshire

Exteriors:
* Brighton – Palace Pier. The West Pier in Brighton was used two years later for Carry On Girls.
* Brighton – Clarges Hotel. The same location was also used in the later Carry On Girls.
* Pinewood Studios. The studios wood storage area was used as the exterior of WC Boggs factory
* Pinewood Green, Pinewood Estate. Sid Plummer house and the Moores house
* The Red Lion, Shreding Green, Buckinghamshire
* Odeon Cinema, Uxbridge. (demolished in September 1984)
* Heatherden Hall, Pinewood Studios
* Black Park Country Park, Iver Heath, Buckinghamshire

==Behind the scenes== Bless This House.  In the next film Carry On Matron (1972) his character was preoccupied with thieving, but made odd suggestive comments to nurses (including one played by Jacki Piper, who played his daughter in this film). Sids girl-chasing persona was fully reinstated for subsequent films. It is one of the few films in the series where several script lines take on a semi-serious tone and are not played entirely for laughs. 

==See also==
*Prague Philharmonic, Gavin Sutherland conducting. The carry on album: music from the films : London, England : ASV, p1999. LCCN 00300982

==References==
 

==Bibliography==
* 
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
* 
* 
* 


 
 

 
 
 
 
 
 
 
 
 
 