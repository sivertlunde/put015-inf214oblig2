Alfonsina
{{Infobox film
| name           = Alfonsina
| image          = AlfonsinaAmeliaBence.jpg
| caption        = Screenshot
| director       = Kurt Land
| producer       = Kurt Land
| writer         = José María Fernández Unsáin Alfredo Ruanova
| starring       = Amelia Bence Guillermo Murray
| music          = Tito Ribero
| cinematography = Alfredo Traverso
| editing        = Vicente Castagno
| distributor    = 
| released       = 1957
| runtime        =  Argentina
| Spanish
| budget         = 
| followed_by    = 
}} Argentine biographical film directed by Kurt Land and written by José María Fernández Unsáin. The film stars Amelia Bence as the poet Alfonsina Storni and actor Guillermo Murray.

==Plot==
In a movie of slow-pace, the film opens with a wide shot of waves breaking ominously on the seashore, accompanied by Alfonsinas poetry. Another scene has Alfonsina looking into a fish-bowl and saying "I wonder what it would be like to live under the sea".

There is also an important scene where Alfonsina announces the news of her illness performed in a very different way from Hollywood film.

==Other cast==
 ]]
*Amelia Bence as  Alfonsina Storni
*Alberto Berco
*José De Angelis
*Dorita Ferreyro
*Domingo Mania
*Alejandro Rey
*Marcela Sola

==Overview==
Amelia Bence stars as the Argentine poet and journalist Alfonsina Storni was an Argentine poet and journalist who made a successful career in the rough macho world of reporting.

Kurt Land was originally from Vienna, so he has a romantic atmosphere somewhat similar to Ernst Lubitsch. This style of movie-making was dated even in 1957, but Alfonsina shows style in the voice over editing of the poetry reading.

==Release and acclaim==
The film premiered in 1957.  It was one of the first and few Argentine films shot in widescreen CinemaScope.

==External links==
*  

 
 
 
 
 

 
 