The Mummy's Shroud
 
 
{{Infobox film
| name           = The Mummys Shroud
| director       = John Gilling
| image          = The-Mummys-Shroud-poster.jpg
| producer       = Michael Carreras
| writer         = John Gilling Anthony Hinds John Phillips David Buck Maggie Kimberly Elizabeth Sellars
| music          = Don Banks Arthur Grant Chris Barnes
| studio         = Hammer Film Productions
| distributor    = Associated British Picture Corporation|Warner-Pathé (UK) 20th Century Fox (US)
| released       =  
| runtime        = 90 min
| country        = United Kingdom
| language       = English
| budget         =
}}
 DeLuxe colour horror film made by Hammer Film Productions which was directed by John Gilling.
 John Phillips, Stuntman Eddie Powell (Christopher Lees regular stunt double) played the Mummy, brought back to life to wreak revenge on his enemies. The uncredited narrator in the prologue, sometimes incorrectly assumed to be Peter Cushing, is British actor Tim Turner. 
 The Mummy (1959), continued with The Curse of the Mummys Tomb (1964), and ended with Blood from the Mummys Tomb (1971). It was the last to feature a bandaged mummy - the final film contained no such character.

It was the final Hammer production to be made at Bray Studios, the companys home until 1967, when its productions moved to Elstree Studios and occasionally Pinewood Studios|Pinewood.

== Plot ==
The Mummys Shroud is set in 1920 and tells the story of a team of archaeologists who come across the lost tomb of the boy Pharaoh Kah-To-Bey (Toolsie Persaud). The story begins with a flash back sequence to Ancient Egypt and we see the story of how Prem (Dickie Owen), a manservant of Kah-To-Bey, spirited away the boy when his father (Bruno Barnabe) was killed in a palace coup and took him into the desert for protection. Unfortunately, the boy dies and is buried.
 John Phillips) finding the tomb. They ignore the dire warning issued to them by Hasmid (Roger Delgado), a local Bedouin about the consequences for those that violate the tombs of Ancient Egypt and remove the bodies and the sacred shroud. Sir Basil is bitten by a snake just after finding the tomb. He recovers, but has a relapse after arriving back in Cairo. 

Preston takes advantage of this and commits him to an insane asylum, to take credit for finding the tomb and Princes mummy himself. Meanwhile, after being placed in the Cairo Museum, the mummy of Prem is revived when Hasmid chants the sacred oath on the shroud. The mummy then proceeds to go on a murderous rampage to kill off the members of the expedition, beginning with Sir Basil after he escapes from the asylum. One by one, those who assisted in removing the contents of the tomb to Cairo are eliminated by such grisly means as strangulation, being thrown out of windows, and having photographic acid thrown in their face. Greedy Stanley Preston, the real villain of the piece, after repeated attempts to evade the murder investigations and flee for his own safely, is murdered in a Cairo sidestreet by the avenging mummy. All ends happily thanks to the intervention of remaining members of the party, Stanleys son Paul Preston (David Buck) and Maggie Claire de Sangre (Maggie Kimberly), who succeed in destroying the Mummy in a very dramatic and beautifully staged finale.

==Cast==
* André Morell as Sir Basil Walden (as Andre Morell) John Phillips as Stanley Preston
* David Buck as Paul Preston
* Elizabeth Sellars as Barbara Preston
* Maggie Kimberly as Claire (as Maggie Kimberley)
* Michael Ripper as Longbarrow Tim Barrett as Harry
* Richard Warner as Inspector Barrani
* Roger Delgado as Hasmid
* Catherine Lacey as Haiti
* Dickie Owen as Prem
* Bruno Barnabe as Pharaoh
* Toni Gilpin as Pharaohs Wife
* Toolsie Persaud as Kah-to-Bey
* Eddie Powell as The Mummy

== Critical reception ==
 
AllRovi|Allmovies contemporary review of the film was unfavourable: "The Mummys Shroud is a standard issue spook show that recycles elements from the previous two mummy titles   without any of their atmosphere, imagination or suspense." 

==In other media==
The film was adapted into a 12-page comic strip for the December 1977 issue of the magazine House of Hammer (volume 2, # 15, published by Top Sellers Limited).  It was drawn by David Jackson from a script by Donne Avenell.  The cover of the issue featured a painting by Brian Lewis  depicting a scene from the movie.

== References ==
 

== External links ==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 