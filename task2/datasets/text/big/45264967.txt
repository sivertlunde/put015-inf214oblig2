Discount (film)
{{Infobox film
| name           = Discount 
| image          = 
| caption        =
| director       = Louis-Julien Petit
| producer       = Liza Benguigui Philippe Dupuis-Mendel 
| writer         = Samuel Doux Louis-Julien Petit
| starring       = Olivier Barthelemy   Corinne Masiero  Pascal Demolon Sarah Suco MBarek Belkouk Zabou Breitman
| music          = Chkrrr 
| cinematography = David Chambille	 
| editing        = Nathan Delannoy Antoine Vareille 
| studio         = Elemiah   France 3 Cinéma   Orange Studio
| distributor    = Wild Bunch Distribution
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = $3,3 millions
| gross          = $1,910,332 
}}

Discount is a 2014 French comedy-drama film directed by Louis-Julien Petit. 

== Cast ==
* Olivier Barthelemy as Gilles 
* Corinne Masiero as Christiane  
* Pascal Demolon as Alfred  
* Sarah Suco as Emma 
* MBarek Belkouk as Momo 
* Pablo Pauly as Hervé 
* Zabou Breitman as Sofia Benhaoui  
* Francesco Casisa as Francesco 
* Hafid F. Benamar as Abril
* Yves Verhoeven as Le capitaine
* Xavier Robic as Le chef de réseau
* Gaëlle Gauthier as Lassistante du chef de réseau

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 
 