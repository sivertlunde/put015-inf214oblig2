Summer in February
{{Infobox film
| name           = Summer in February
| image          = Summer in February poster.jpg
| image_size     = 250
| caption        = Theatrical release poster
| director       = Christopher Menaul
| producer       = {{plainlist|*Jeremy Cowdrey
*Pippa Cross
*Janette Day}} Jonathan Smith
| based on       =  
| starring       = {{plainlist|*Dominic Cooper 
*Emily Browning
*Dan Stevens
*Hattie Morahan}}
| music          = Benjamin Wallfisch Andrew Dunn
| editing        = {{plainlist|*Chris Gill
*St. John ORorke}}	
| studio         = {{plainlist|*CrossDay Productions Ltd.
*Apart Films
*Marwood Pictures}}
| distributor    = Metrodome Distribution
| released       = 
| runtime        = 100 minutes   
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £74,898
}} romantic drama Jonathan Smith adapted the screenplay from his 1995 eponymous novel. The film stars Dominic Cooper, Emily Browning, Dan Stevens, Hattie Morahan and Nicholas Farrell and focuses on the early 20th century love triangle between British artist Alfred Munnings, his friend Gilbert Evans and Florence Carter-Wood. It was released in the United Kingdom on 14 June 2013. 

==Plot==
Set in Cornwall in the early 20th century, Summer in February focuses on a group of Bohemian artists called the Lamorna Group, which include Alfred Munnings (Dominic Cooper), Laura Knight (Hattie Morahan) and Harold Knight (Shaun Dingwall). The group is at the centre of the real life love triangle between Alfred, his friend Gilbert Evans (Dan Stevens) and the girl they both loved, Florence Carter-Wood (Emily Browning). 

==Production==

===Filming=== Holywell and Porthcurno provided "a dramatic setting" for a horse-race sequence and a beach party scene respectively.  Producer Jeremy Cowdrey explained "We could have filmed it anywhere in the world but we were determined to do it here, where it all happened. Its a true story and, because its about a Bohemian artists colony, the exciting thing is to recreate it, splash Cornwall and bring the county alive." 

===Music=== The Jewish Classic FMs Sam Pittis Wallfisch said that it was clear to him from the start that the score had to be something emotional and thematic. He also wanted to capture the beauty of the location where the film is set and tell stories through the music.  

The score is played by the London Chamber Orchestra and features piano solos by Yuja Wang.    Wallfisch thought Wang would be perfect to play on the score and he approached her with some of the pieces he had composed.  Wang agreed to come aboard and during the recording sessions, Wallfisch was so impressed with her playing that he wrote her a solo piano suite of all the main themes.  The score to Summer in February was released by Decca Records in the UK on 24 June 2013. 

The track listing for the album is as follows: 

{{Track listing
| collapsed       = yes
| headline        = 
| writing_credits = 
| title1          = Lamorma
| length1         = 3:06
| title2          = Mirror
| length2         = 2:18
| title3          = The Races
| length3         = 1:34
| title4          = Painting
| length4         = 4:51
| title5          = Proposal
| length5         = 1:30
| title6          = Gilberts Theme
| length6         = 1:27
| title7          = Florences Theme
| length7         = 2:36
| title8          = AJs Request
| length8         = 1:06
| title9          = Unaccountable
| length9         = 2:14
| title10         = Wedding
| length10        = 1:19
| title11         = Cyanide
| length11        = 3:20
| title12         = Florences Hut
| length12        = 1:56
| title13         = Art And Life
| length13        = 2:18
| title14         = Final Kiss
| length14        = 4:59
| title15         = The Storm
| length15        = 3:15
| title16         = Aftermath
| length16        = 1:56
| title17         = Gilbert Returns
| length17        = 3:49
| title18         = Sirens Lullaby
| length18        = 2:50
| title19         = Epilogue: Morning Ride
| length19        = 4:03
}}

==Reception==

===Box office===
Summer in February entered the box office chart at number 11 after grossing £74,898 in its opening weekend for a per-cinema average of £1,170 in 64 cinemas. 

===Critical response=== review aggregation rating average of 5.1 out of 10.  Stella Papamichael from Digital Spy gave the film three out of five stars and commented "The film itself is no masterwork, but it has a certain irresistible undertow."    Papamichael thought that Menaul and Smith seemed unsure when it came to the scenes in between documented incidents and called the dialogue "serviceable".  Anna Smith, writing for Empire (film magazine)|Empire, also awarded the film three stars and she stated "While the melodrama occasionally grates, this works as a raw romance and an intriguing glimpse of a bold and brash artist ahead of his time."  Total Films Tom Dawson gave Summer in February a mixed review, saying "Though it struggles to transcend its Sunday-TV feel, Christopher Menauls film boasts sturdy turns from its three leads, while the outdoor lensing is a breath of fresh air." 

Chris Tookey from the Daily Mail gave the film one star and opined Summer in February as "a plodding, sub-Merchant Ivory costume drama, not terribly convincing even though its based on a true story, about a romantic triangle in Edwardian Cornwall"  Michael Hann from The Guardian writes "Proof that truth is duller than fiction comes with this tale of real events in the Lamorna artists colony in Cornwall in the months before the first world war."    He also thought Brownings role was underwritten.  During her review, Tara Brady from The Irish Times, thought the film was like "Downton Abbey stripped of charm and lobotomised."  The Independents Anthony Quinn felt that the film "struggles to rise above the blandness of a Sunday teatime serial,",    where "sudden bursts of drama fizzle like damp fireworks".  Derek Malcolm from the London Evening Standard lamented that the film "seems to be much ado about nothing very much, despite the pleasing performances and scenery,"    jibing that his fathers "painted horse, if only he could speak, could probably tell   more about Munnings the artist than Summer in February does." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 