End of Watch
 
{{Infobox film
| name           = End of Watch
| image          = End of Watch Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = David Ayer
| producer       = David Ayer Matt Jackson John Lesher Nigel Sinclair
| writer         = David Ayer
| starring       = Jake Gyllenhaal Michael Peña Anna Kendrick Natalie Martinez America Ferrera Frank Grillo David Harbour 
| music          = David Sardy
| cinematography = Roman Vasyanov
| editing        = Dody Dorn
| studio         = StudioCanal Exclusive Media   Crave Films   CECTV Films  Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor    = Open Road Films
| released       =  
| runtime        = 110 minutes  
| country        = United States
| language       = English, Spanish
| budget         = $7.5 million
| gross          = $53 million
}} drama thriller thriller film written and directed by David Ayer. It stars Jake Gyllenhaal and Michael Peña as Brian Taylor and Miguel Zavala, two Los Angeles Police Department officers who work in South Central Los Angeles. The film focuses on their day-to-day police work, their dealings with a certain group of gang members, and their personal relationships.

Ayer, who had written several police procedural films previously, wanted End of Watch to focus more on the friendship between partners and honest police work rather than corruption. Gyllenhaal, Peña, and other cast members underwent an intensive training program to prepare for their roles as police officers. Filming took place in Los Angeles in August 2011 with a budget of United States dollar|$7.5 million.

End of Watch premiered on September 8, 2012 at the Toronto International Film Festival and was released in American theaters on September 21, grossing over $53 million at the box office. The film was well received by critics and received a number of accolades, including two Independent Spirit Award nominations.

==Plot== South Central Los Angeles. Taylor, a former United States Marine Corps|Marine, is recording their police activities for a film project.

Upon responding to a public disturbance call, Tre, a Bloods gang member, yells racist insults at Zavala, who responds by accepting a fight. Zavala beats and arrests him, but wins Tres respect for not charging him with assault. Later that night, Tre and his fellow Bloods gang members are attacked by a group of Sureños in a drive-by shooting and one of Tres friends is killed. The next day, the officers respond to a noise complaint at a house party full of Sureños, where Taylor encounters a gang member named Big Evil, whose truck he later finds is filled with ornately-decorated firearms and a large amount of money.
 Medal of ICE agents arrive, one agent informs the officers that the house is tied to the Sinaloa Cartel, and strongly urges them to "lay low" due to possible reprisals. Around this time, Taylor begins dating a woman named Janet, and Zavalas wife Gabby gives birth to their first child.

One night, Taylor and Zavala respond to a call from their fellow officer, Sook, and find her partner Van Hauser with a knife through his eye. He leads them to Sook, who is being savagely beaten nearby. After arresting the culprit, the officers learn that Van Hauser is not returning to patrol and Sook is leaving the force. Taylor marries Janet, and at their wedding Zavala tells Taylor that, should anything happen to him, he will take care of Janet. The next day, the officers perform a welfare check on an elderly woman. In her house, they discover drugs, dismembered corpses, and a message from the cartel. Unbeknownst to them, a cartel member has "Contract killing|green-lit" the officers, and the gangsters from the earlier drive-by begin following them.

Shortly after Janet gets pregnant, the officers are baited into chasing a reckless driver into an apartment complex, where they are ambushed by the same group of Latino gang members. They fight their way into an alley, where Taylor is shot in the chest. As Zavala desperately attends to his partner, the assassins arrive and shoot him several times in the back, killing him. Police backup eventually arrives and the gangsters are killed after refusing to surrender.

Taylor survives, having been shielded by Zavalas body. At Zavalas funeral, Taylor tries to deliver a eulogy, but only manages to say a few words: "He was my brother." In a flashback to the day of the shooting, Zavala recounts to Taylor a story from his teens before the two receive the call from dispatch.

==Cast==
 
* Jake Gyllenhaal as Brian Taylor
* Michael Peña as Miguel "Mike" Zavala
* Natalie Martinez as Gabby Zavala
* Anna Kendrick as Janet Taylor
* Frank Grillo as Sarge
* America Ferrera as Orozco
* Cody Horn as Davis
* David Harbour as Van Hauser Cle Sloan as Tre
* Shondrella Avery as Bonita
* Kristy Wu as Sook

 

==Production==
{{multiple image
| footer    = Jake Gyllenhaal (left) and Michael Peña (right) undertook five months of intensive training to prepare for their roles in the film.
| image1    = Jake Gyllenhaal Toronto International Film Festival 2013.jpg
| alt1      = Jake Gyllenhaal
| width1    =  
| image2    = Michael Peña Labor Cesar E. Chavez Memorial Auditorium (cropped).jpg
| alt2      = Michael Peña
| width2    =  
}}
David Ayer, who wrote and directed End of Watch, grew up in South Central Los Angeles and has had numerous friends in the LAPD.       He had written several films previously about police officers in Los Angeles, but while these depicted rogue and corrupt officers, he wanted to feature honest, ethical police work in End of Watch.  In contrast to his previous works, Ayer wanted to focus on the friendship between Taylor and Zavala and "have all the cop stuff drop away and become secondary to the chemistry of these guys".  Ayer wrote the screenplay over six days in December 2010.    Jaime FitzSimons, a longtime friend of Ayer and a former police officer with the LAPD, served as the films technical advisor, and his experiences from working in Los Angeles inspired several plot points of the film.        

Jake Gyllenhaal was the first to be cast in the film; after receiving the script, he read it in an hour and immediately contacted Ayer.   Michael Peña was cast shortly after, following a string of auditions.  He and Gyllenhaal did not bond immediately but gradually became close friends over the process of training and filming.  Gyllenhaal and Peña undertook five months of intensive training under the guidance of FitzSimons to prepare for their roles—this included 12-hour ride-alongs with multiple Greater Los Angeles Area law enforcement agencies up to three times a week, as well as training in hand-to-hand combat, police tactics and weapons.   On his first ride-along, Gyllenhaal witnessed a murder during a drug bust.  Tactical training was also given to the other actors playing police officers, including David Harbour, America Ferrera, Cody Horn, and Frank Grillo.  
 found footage style and traditional photography.    Most scenes were captured by four cameras simultaneously:   these included a handheld camera operated by Gyllenhaal, cameras clipped to Gyllenhaal and Peñas vests, and dashboard footage from their patrol car.   Some scenes were shot entirely by Gyllenhaal.  An alternate ending of the film was shot where both of the main characters died, but Ayer ultimately chose to retain the original ending. 

==Release==
 ]]
The world premiere of End of Watch was held on September 8, 2012 at the Toronto International Film Festival.  It was originally scheduled to be released theatrically on September 28, 2012,  but the release was later moved to September 21. 

===Box office=== word of mouth.  The film was initially released in 2,730 theaters and expanded to 2,780 locations in its second week of release.    On December 7, the film was given a nationwide re-release in 1,259 theaters  shortly after it received two Independent Spirit Award nominations.  On the first weekend of its re-release, it grossed $752,000. 

After a total of 119 days, the film ended its American theatrical run in January 2013 with a gross of $41 million.  It grossed $12 million from other territories, making a worldwide total gross of $53 million.   

===Home media===
End of Watch was released on DVD and Blu-ray Disc|Blu-ray on January 22, 2013.  This release included 40 minutes of deleted scenes and an audio commentary of the film recorded by Ayer.  In the United States, the film has grossed $15.6 million from DVD sales and $9.2 million from Blu-ray sales, making a total of $24.8 million. 

==Reception==
  
End of Watch received positive reviews from critics, who praised Ayers direction and the performances of Gyllenhaal and Peña. The  , the film received a score of 68 out of 100, based on 37 critics, which indicates "generally favorable reviews". 
 New York  Bilge Ebiri found the film largely unrealistic and Ayers direction "serviceable at best", he wrote that "Ayer and his cast appear to have so convincingly nailed the way these characters talk and act that you might not even notice the film slipping from workaday grit into out-and-out myth." 
 buddy cop genre. Peter Debruge of Variety (magazine)|Variety wrote that "Like a knife in the eye, End of Watch cuts past the cliches of standard police procedurals" and praised Ayer for depicting the LAPD as "an honorable and efficient organization of people working together".    Entertainment Weekly  Lisa Schwarzbaum, who gave the film an A-, described it as "one of the best American cop movies Ive seen in a long time   also one of the few Ive seen that pay serious attention to what cop life feels like, both on and off duty".  In a review for The Globe and Mail, however, Rick Groen opined that the focus on "saintly" police officers was less interesting than Ayers "trademark grit and authenticity".   

The performances of Peña and Gyllenhaal were also praised by critics. Peter Debruge commended the realism that the two actors brought to their roles, saying, "Gyllenhaal and Peña so completely reinvent themselves in-character. Instead of wearing the roles like costumes or uniforms, they let the job seep into their skin."  The Los Angeles Times  Betsy Sharkey applauded the chemistry between the two lead actors, as well as their individual performances, writing, "As good as Gyllenhaal is in this, Peña nearly steals the show."  In a review for USA Today, Claudia Puig commended Gyllenhaal for "giv  his best performance since Brokeback Mountain" and Peña for "shin  with charisma".  Roger Ebert highlighted End of Watch as "one of the performances of   career" and praised the performances given by the supporting cast, including Natalie Martinez and Anna Kendrick. 

An aspect of the film criticized in numerous reviews was its handheld camerawork and cinematography. Richard Corliss wrote for Time (magazine)|Time that the found footage style of cinematography "borders on the ludicrous" and that "the tactic fatally substitutes photo realism for fauxto realism".  Similarly, The Washington Post  Michael OSullivan found the aesthetic gimmicky, overused, and "an unnecessary distraction from the story".  On the other hand, Amy Biancolli of the San Francisco Chronicle felt that although the cinematography was inconsistent, "its used to deepen its main characters" and "lends the film a lively intimacy". 
 minorities as gang members. Manohla Dargis of The New York Times pointed out that "almost all of   committed by the black and mainly brown people",  while The Globe and Mail  Rick Groen criticized "the scripts penchant for overdemonizing the ghettos black residents". 

==Awards and nominations==
 
{| class="wikitable sortable" width="95%"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards and nominations
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Nominee(s)
! Result
|-
| Critics Choice Movie Awards  January 10, 2013 Best Actor in an Action Movie
| Jake Gyllenhaal
|  
|-
| rowspan="3"| Golden Trailer Awards 
| rowspan="3"| May 3, 2013
| Best Independent TV Spot
| Open Road Films, Aspect Ratio
|  
|-
| Best Action
| Open Road Films, Ignition Creative
|  
|-
| Best Foreign Action Trailer
| Tobis Film
|  
|-
|rowspan="2"| Independent Spirit Awards {{cite web|url=http://www.nytimes.com/movies/movie/465745/End-of-Watch/awards|title=
End of Watch (2012) – Awards|work=The New York Times|accessdate=May 16, 2014}}  February 23, 2013 Best Supporting Male
| Michael Peña
| 
|- Best Cinematography
| Roman Vasyanov
|  
|- Key Art Awards  October 24, 2013
| Best Audio/Visual Technique
| Open Road Films, Aspect Ratio
|  
|-
| Best Trailer – Audio/Visual
| Open Road Films, Ignition Creative
| 
|- London Film Festival 
| October 20, 2012
| colspan="2"|  Best Film 
|  
|- MTV Movie Awards  April 14, 2013
| Best Latino Actor
| Michael Peña
| 
|-
| National Board of Review  January 8, 2013
| colspan="2"| Top Ten Independent Films
|  
|-
| Zurich Film Festival 
| September 29, 2012
| colspan="2"| Best International Feature Film
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 