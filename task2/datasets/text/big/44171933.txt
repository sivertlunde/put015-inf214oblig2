Hifajat
  }}

 
{{Infobox film
| name = Hifajat
| image =
| caption = Theatrical poster
| film name = 
 |nepali= हिफाजत
| director = Gyanendra Deuja
| producer = Dijendra sakhya   Chabi raj ojha
| writer = Pradeep Bhardwaj
| starring = Rekha Thapa Aryan Sigdel
| music = 
| cinematography = Bishnu Kalpit
| editing = 
| distributor = Guna films
| released =  
| runtime = 138 min
| country = Nepal Nepali
| budget =
| gross =
}}
 Nepali film released on 10 April 2010. The film is a combination of drama and comedy.

==Characters==
* Rekha Thapa as Kriti
* Aryan Sigdel as Aryan
* Aayush regmi as Arjun

==Story==
The film characterizes Rekha Thapa as Kriti who is a spoiled brat estranged from a rich business father. Kriti seems to be courageous enough to start problems in her college. She is also famous in her college as a caring and loving friend for all students. Once her father hires an ex-army tough guy Arjun (Aayush Regmi) as her bodyguard for which she rebels with her father. She also makes a thief Aryan (Aryan Sigdel) as her friend so that she can protest with her rich business tycoon father. But Aryan also has his own secret of living due to which he betrays Kriti and leaves her in danger. After which Arjun promises to take care of Kriti till she does not unite with Aryan who is Kritis first love.

The story then surrounds around the difficulties and problems Kriti has to tackle for the uniting with Aryan. The search for Aryan becomes the adventure of Kriti in the track full of dangers and risks. During the search, Kriti has to face with twists and turns which ultimately challenges Kritis View of living life and also changes her way of thinking about love and life. 

==References==
 


 