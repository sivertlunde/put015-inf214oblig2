The Enforcer (1951 film)
{{Infobox film
| name           = The Enforcer
| image_size     =
| image          = The Enforcer 1951.JPG
| alt            =
| caption        = Theatrical release poster
| director       = Bretaigne Windust
| producer       = Milton Sperling
| screenplay     = Martin Rackin
| narrator       =
| starring       = Humphrey Bogart Zero Mostel Ted de Corsia Everett Sloane
| music          = David Buttolph
| cinematography = Robert Burks
| studio         = United States Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2 million (US rentals) |
}}
The Enforcer (aka Murder, Inc.) is an American 1951 black-and-white film noir co-directed by Bretaigne Windust and an uncredited Raoul Walsh, who shot most of the films suspenseful moments, including the ending.  The production, largely a police procedural, features Humphrey Bogart and based on the Murder, Inc. trials.

==Plot==
The action is set in an unnamed American city and is told mainly in Flashback (literary technique)|flashback, and flashbacks within flashback.

===The Terrified Witness=== ADA Martin Ferguson (Humphrey Bogart) reminds him that he himself faces plenty of charges that could "burn you a dozen times". Ferguson is bound and determined to get Mendoza "in the electric chair" and stresses to Rico that Mendoza will "die, hes got to die, and youre going to kill him."

After yet another attempt on his life, Rico gives his bodyguards the slip and tries to escape by reaching the fire escape on the eighth floor of the building, but he falls off the ledge and is killed on impact when he hits the courtyard.

Rico was the only evidence Ferguson had against Mendoza who will walk away in the morning as a free man. However he believes that something else came up in the course of the investigation that might make the case if only he could remember it. He and police Captain Nelson (Roy Roberts) decide to go through the evidence hoping that something will come up.

===The Original Investigation===
The case began when a man called James "Duke" Malloy (Michael Tolan) burst into a police station and claimed to have killed his girlfriend, under pressure from others. At the crime scene, which is out in the countryside, the police find an empty grave. Malloy, overcome with grief, bitterly explains that his girlfriend was a "contract" and a "hit", terms which mean nothing to the officers. He later commits suicide in his cell.

Ferguson, the ADA in charge of homicide, is brought in on the case. Malloy only had convictions for petty crimes, not murder, but checking on his associates leads the investigators to "Big Babe" Lazick (Zero Mostel). When Ferguson threatens to jail his wife and put his little son into care, Lazick confesses that he is part of a "troop" (a group of killers) operating under the orders of Joe Rico who gets requests to commit murders over the telephone from a third party. The gang uses terms like "contract" (a request to commit murder) and "hit" (the actual killing) in case of others listening in. The killers get a regular salary (even if they go to jail), their families are looked after if anything goes wrong and bonuses are paid for actual killings. Only Rico knows who the top boss is.

The killers carry out murders for profit, the idea being that they are hired to kill someone at the request of someone else (the persons spouse or business partner, for example). The killer will have no motive for committing the crime and thus will not be suspected by the police, while the client with the motive will have a perfect alibi. Furthermore, the client has to keep contributing money in case of exposure.
 cab driver, witnessed the murder of John Webb, a café owner.

The police eventually find a mass grave filled with dozens of bodies. As the authorities close in on them, the gang begins to break up. Some go into hiding, fearing for their lives as others are killed by other members from out of town. Rico himself is hiding in a farm with his last remaining accomplices. He calls his boss whose answers do not reassure him. Rico pretends to go to town for a contract but instead parks his car behind some bushes. He later witnesses his accomplices being killed by other killers sent by the boss.

Rico contacts Ferguson. In return for being spared the death penalty, he offers to testify against his boss, Albert Mendoza. Rico first met Mendoza when the latter tried to interfere in a  ) and his daughter. Mendoza and Rico got away, but years later Vetto recognized Mendoza as a cab fare and was murdered before he could go to the police.

===Desperate Hunt=== admissible in court. In it, Rico describes Vettos daughter as having "big blue eyes", whereas the body of Nina Lombardo (assumed to be Angela Vetto) had brown eyes. On the other hand her roommate, Teresa Davis, does have blue eyes. Ferguson concludes that Nina was pointed out as Dukes contract by mistake. Teresa told the police that Nina was Angela Vetto as a hint: to get them on the trail of the killers without getting involved herself; she even tried to leave town, but Ferguson warned her against it.

However, from Ninas photo, Mendoza has come to the same conclusion and, through his attorney, sends two of his remaining men after the real Angela Vetto. Ferguson and Nelson arrive at her house to learn that she has gone shopping. The streets are too crowded for them to find her, so Ferguson uses a music stores street-side loud speakers to warn her that her life is in danger and to contact him at the store. Angela does so and Ferguson sets off to meet her, followed by the killers. In the subsequent shoot-out, Ferguson kills one of the gangsters and the other is arrested. He then escorts Angela Vetto to testify against Mendoza and put him in the chair.

==Cast==
* Humphrey Bogart as ADA Martin Ferguson
* Zero Mostel as Big Babe Lazick
* Ted de Corsia as Joseph Rico
* Everett Sloane as Albert Mendoza
* Roy Roberts as Capt. Frank Nelson
* Michael Tolan as James Duke Malloy
* King Donovan as Sgt. Whitlow Bob Steele as Herman
* Adelaide Klein as Olga Kirshen
* Don Beddoe as Thomas OHara
* Tito Vuolo as Tony Vetto John Kellogg as Vince Jack Lambert as Tom Philadelphia Zaca
* Patricia Joiner as Teresa Davis / Angela Vetto (uncredited)

==Production== Broadway director, fell seriously ill during the beginning of shooting, so Raoul Walsh was brought in to finish the film. Walsh refused to take the credit, calling it Windusts work.

This was Bogarts last film for Warner Bros., the studio that had made him a star. Warner only distributed the film. It was produced by United States Pictures, and is now owned by Republic Pictures, a division of Paramount Pictures.

==Background== Kefauver hearings, that terms like "contract" (a deal to commit a murder) and "hit" (the actual killing itself) first came into the public knowledge. The gangsters used such codes in case of eavesdroppers or phone tappings by the police.

Bogarts ADA Martin Ferguson is based on Burton Turkus, who led the prosecutions of several members of the Murder, Inc. gang. The Aurum Film Encyclopaedia - Gangsters, edited by Phil Hardy, published in 1998 by Aurum.  His book on the case was published at about the same time the film was released.

Ted de Corsias Joe Rico was probably inspired by Abe Reles. Like Rico, Reles was about to testify against a major crime lord but, although under heavy police guard, was found dead after falling out of the Half Moon Hotel in Coney Island on November 12, 1941. It has never been established for sure if Reles death was murder, accident, or suicide.

==Reception==

===Critical response===
When the film was released the staff at Variety (magazine)|Variety magazine praised director Windust, writing, "The film plays fast and excitingly in dealing with Humphrey Bogart’s efforts to bring the head of a gang of killers to justice. The script uses the flashback technique to get the story on film, but it is wisely used so as not to tip the ending and spoil suspense ... Bretaigne Windust’s direction is thorough, never missing an opportunity to sharpen suspense values, and the tension builds constantly." 

===Noir analysis===
Film critic Dennis Schwartz questioned if the film should be labeled as film noir, writing, "The crime film tells for the first time in film how a mob works and its use of terms such as contract, hit, and finger man. It is shot in a semi-documentary style and looked more like a crime caper movie than the film noir category most film critics have classified it under." 

==References==
 

==External links==
*  
*  
*  
*   informational site and DVD review (includes images)
*  

 
 
 
 
 
 
 
 
 
 