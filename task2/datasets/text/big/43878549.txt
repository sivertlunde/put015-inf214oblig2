Partners in Time
{{Infobox film
| name           = Partners in Time
| image          = Partners in Time poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Nigh
| producer       = Ben Hersh 
| screenplay     = Charles E. Roberts 
| starring       = Chester Lauck Norris Goff Pamela Blake John James Teala Loring Danny Duncan
| music          = John Leipold Lucien Moraweck 
| cinematography = Jack MacKenzie
| editing        = S. Roy Luby
| studio         = Jack Votion Productions Inc.
| distributor    = RKO Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Partners in Time is a 1946 American comedy film directed by William Nigh and written by Charles E. Roberts. The film stars Chester Lauck, Norris Goff, Pamela Blake, John James, Teala Loring and Danny Duncan. The film was released on April 25, 1946, by RKO Pictures.   

==Plot==
 

== Cast ==
*Chester Lauck as Lum Edwards
*Norris Goff as Abner Peabody
*Pamela Blake as Elizabeth Meadows
*John James as Tim Matthews
*Teala Loring as Janet Smith
*Danny Duncan as Grandpappy Spears / Constable Spears
*Grady Sutton as Cedric Weehunt
*Dick Elliott as Squire Skimp
*Phyllis Kennedy as Abagail
*Ruth Lee as Miss Martha Thurston
*Charles Jordan as Gerald Sharpe
*Ruth Caldwell as Josie 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 