The Beast with a Million Eyes
{{Infobox film
| name            = The Beast with a Million eyes
| image           =Poster of the movie The Beast with a Million Eyes.jpg
| image size       = Theatrical release poster by Albert Kallis
| director        = David Kramarsky Lou Place Roger Corman (uncredited)
| producer        = David Kramarsky Charles Hanawalt (associate director)
| writer          = Tom Filer Paul Birch Lorna Thayer Dona Cole
| music           = John Bickford
| cinematography  = Everett Baker
| editing         = Jack Killiferart
| special effects = Paul Blaisdell
| studio          = San Mateo Productions
| distributor     = American Releasing Corporation
| released        = 1955
| runtime         = 75 minutes
| country         = USA
| awards          = English
| budget          = $33,000 McGee 1996,  pp. 24–27.  
}}

The Beast with a Million Eyes (aka The Unseen) is a   and Samuel Z. Arkoff.   The film was released in 1955 by American Releasing Corporation which later became American International Pictures. 

==Plot==
The isolated Kelley family struggle with their small farm in a bleak landscape. A mysterious plane crash happens nearby. Then wild and domesticated animals and finally their handyman turn on the family and attack. It turns out a space alien (the beast of the title) has taken over the minds of the lesser animals, working its way up to controlling humans as part of a plan to conquer the world. In the end, the family bond together, unite against the alien and their love conquers all.

==Cast==
  Paul Birch as Allan Kelley
* Lorna Thayer as Carol Kelley
* Dona Cole as Sandy Kelley
* Dick Sargent as as Richard Sargeant)(Larry)
* Leonard Tarver as "Him"
* Bruce Whitmore (voice only) as The Beast
* Chester Conklin as Old Man Webber
 

==Production== The Fast and the Furious (1955) and Five Guns West (1955). Smith 2014, pp. 18–19.  Only $29,000 remained to make the film for Pacemaker Productions. The tiny budget meant music in The Beast with a Million Eyes, credited to "John Bickford", is actually a collection of public-domain record cues by classical composers Richard Wagner, Dimitri Shostakovich, Giuseppe Verdi, Sergei Prokofiev, and others, used to defray the cost of an original score or copyrighted library cues.  

Studio publicist James H. Nicholson had come up with a title and ad treatment that had exhibitors signed on before seeing the finished film. When Sam Arkoff of ARC received The Beast with a Million Eyes he was unhappy that it did not even feature a beast, implicit in the title. Paul Blaisdell responsible for the special effects, was hired to create a space ship and alien for $200. Notably, the Art Director was Albert S. Ruddy, who would later win two Best Picture Academy Awards for "The Godfather" (1972) and "Million Dollar Baby" (2004). Smith 2009, p. 21. 
 Indio and the Coachella Valley, California.  Corman shot 48 pages of interiors in two days at a studio in La Ciegna. 
The Beast with a Million Eyes was a non-union film originally titled The Unseen with Lou Place set to direct. After one days filming, the union threatened to shut down the film unless everyone signed with the Guild. Roger Corman, who was producing, took over the film as director and replaced the cinematographer with Floyd Crosby; however Corman took no official credit.  Another version of the story has Corman allocating directing duties to Dave Kramsarsky, his associate director on Five Guns West. 

==Reception==
Film historian Leonard Maltin called The Beast with a Million Eyes, "Imaginative though poorly executed sci-fi melodrama with desert setting; a group of people is forced to confront an alien that can control an unlimited number of animals, hence the title." He further described the film as, "(an) early Roger Corman production (that) features Paul Blaisdells first movie monster."  In 2007 Metro-Goldwyn Mayer distributed The Beast with a Million Eyes as part of its Midnight Movies catalog on a double-feature DVD shared with The Phantom from 10,000 Leagues (1955). 

==References== 
Notes
 

Bibliography
 
* Lentz, Harris M. III. Science Fiction, Horror & Fantasy Film and Television Credits, Vol. 1.  Jefferson, North Carolina: McFarland & Company, 1983. ISBN 978-0-7864-0952-5.
* McGee, Mark. Faster and Furiouser: The Revised and Fattened Fable of American International Pictures. Jefferson, North Carolina: McFarland & Company, 1996. ISBN 978-0-7864-0137-6.
* Smith, Gary A. American International Pictures Video Guide. Jefferson, North Carolina: McFarland & Company, 2009. ISBN 978-0-7864-3309-4.
* Smith, Gary A. American International Pictures: The Golden Years. Albany, Georgia: Bear Manor Media, 2014. ISBN 978-1-5939-3750-8.
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 