The Centrifuge Brain Project
{{Infobox film
| name           = The Centrifuge Brain Project
| image          = The Centrifuge Brain Project DVD cover.jpg
| alt            =
| caption        = DVD cover art
| film name      = 
| director       = Till Nowak
| producer       = Till Nowak
| writer         = Till Nowak
| screenplay     = 
| story          = 
| based on       =  
| starring       = Leslie Barany as Dr. Nick Laslowicz
| narrator       = 
| music          = Siriusmo
| cinematography = Ivan Robles Mendoza
| editing        = Till Nowak
| production companies = FrameboX Digital
| distributor    = Kurz Film Agentur Hamburg
| released       =  
| runtime        = 7 minutes
| country        = Germany
| language       = English
| budget         = 
| gross          =  
}}

The Centrifuge Brain Project is a 2011 German short mockumentary fantasy film written and directed by Till Nowak.     The film incorporates computer-generated imagery to create seven real-seeming fictional amusement park rides                   used in a faux documentary film about the construction of physics-defying rides intended for use in research efforts to improve human cognitive function.     Nowak was inspired to create the project when visiting an amusement park in 2008.   

==Background==

===Art installation=== looped video Prototyp Museum honorary mention, juried runner Seoul Biennale 7th edition of Media City Seoul.          Most recently it was presented at the Cité des Sciences et de lIndustrie in Paris as part of L Art Robotique through January 2015.         

===Short film=== Chief Engineer improv from the scripted lines to have them seem as if given in a real interview. Nowak related that "Barany was perfect for the role, because he turned out to be a very good liar." 

Nowak had the monologue concept in his head for a while; the script was written just two days before filming. Nowak stated,  I had no technical reference for the short film. I created the manipulated amusement rides and the techy talk just out of my own scientific humor. They are a mix of real physics, absurdity and deliberate contradictions. The goal was to create the biggest possible mistake, but still make it sound serious and convincing.    Filming took two days – one day in an actual amusement park and one day in a laboratory – and editing took two months.      
 Filmfestival Münster. Kaohsiung Film Unlimited Film La Rochelle International Film Festival, and had its television debut December 21 on Yleisradio in Finland. After being published to YouTube in January 2013, the film received more than 3.3 million views.     An unreleased alternate version has interview questions becoming so accusatory that Dr. Laslowicz becomes angry and walks away, cancelling the interview.  

==Film synopsis==
Dr. Nick Laslowicz (Leslie Barany) speaks toward the discovery of how playground merry-go-rounds increase creative activity in children and discusses the investigation of centrifugal force on human development to expand the human mind. He explains how his company, the Institute for Centrifugal Research (ICR), officially doubts the generally accepted laws of physics and has developed tests of human mental endurance disguised as amusement rides. He then describes how ICR developed a series of experiments as rides, created to test and expand a subjects mental growth. The first was the 1977 prototype 6 G-force Vertical Centrifuge which self-destructed in 1978.
 globular centrifuge in 1982.  The second was 1985s 96-seat, 2.3 G-force Layer cake|"Wedding Cake Centrifuge", so named because of how its four platforms were layered one above the other.

In 1991, ICR shifted its concentration to height, and developed the  2844-seat, 1-G "High Altitude Conveyance" (HAC), which initially confused riders who were unaware the ride took fourteen hours. ICR learned that passengers would suffer from boredom on rides that were too long, specially for those passengers who had fallen asleep, missed disembarking, and had to ride an additional fourteen hours. In 2005, a redesign of the HAC added toilets and oxygen masks. Dr. Laslowicz explains that to deal with the boredom issue, in 1993, ICR created the 18-seat, 1.1 to 3.6 G-force "Expander", as a ride with an interactive component. Finding this created brain activity, in 1996 ICR created the  126-seat, 2.7 G-force "Dandelion" to simulate the prenatal experience of an embryo.

In 2003, ICR crated the 10,000 horsepower 172-seat, 9-G "Steam Pressure Catapult" (SPC) to add a level of uncertainty which resulted in riders re-evaluating their own goals and aspirations. The last ride created was 2005s 12-seat, 17 G-force "Centriductor Schwingmaschine".

==Reception== National Geographic The Creators New York and Buenos Aires".   
 San Diego Comic-Con 2012, calling it an "amazing short film", and expanding "out of all the films they ended up showing this was my favorite, and I can guarantee its going to entertain the hell out of you!"     First Showing wrote of the 14th Annual Animation Show of Shows at Comic-Con 2013 and labeled the film "incredible" and "worth watching", expanding that of the films screened, "one that really tickled our fancy was a live-action faux documentary called The Centrifuge Brain Project. The film follows the studies of pseudo-mad scientist Dr. Nick Laslowicz and his wild amusement park prototypes, brought to life by some impressive visual effects work."     TV QC wrote that the film is "a fun and especially brilliant documentary".   

GB Times writes that with having seen his own "film hundreds of times in different movie theaters, sometimes with ten people, other times with 500 people in the audience,"  Till Nowak loves to watch audience reactions as they watch the film, and is bemused that "some people have perceived it as a real documentary of real amusement parks."   He has stated,  there are actually still people, especially if they see it on the internet, that really think everything is real. To me that is super interesting because the film is also about our reception of media, how we believe everything, how media can manipulate us. I had never expected that a lot of people would believe the whole film. I thought okay, maybe the first half, but then... For me as a filmmaker and my filmmaker friends, it is very obvious. But people who are not working with the media, it is very surprising for me, how much they believe. Sometimes it is a bit shocking – but also an honor and a compliment because it means that the film was convincing,   and that to "amusement park enthusiasts it feels almost like a pity that these rides are not real."  He explains,  the bizarre thing is that if it would be possible, they would also exist. The only reason these things do not exist in this crazy world that we live in is that they are physically impossible. The constructions I did in this film, they would collapse. They are just not logical. The forces do not work right like the gravity and centrifugal force, and everything is a deliberate mistake that I made to illustrate this. But people would build it if it was possible.    

==Partial awards and nominations==

===The Experience of Fliehkraft=== Honorary mention at Ars Electronica in Linz    Juried runner up at Siggraph 2011. 

===The Centrifuge Brain Project=== Filmfestival Münster     San Sebastián Horror and Fantasy Film Festival    Exground Filmfest 
* 2012, Won Kurosawa Award for creative excellence at 24FPS International Short Film Festival   
* 2012, Won Audience Award at Alcalá de Henares, Alcine 
* 2012, Nominated for Le Cristal dAnnecy at Annecy International Animated Film Festival    Jury Award for Best Short Film at Aspen Shortsfest  Filmfest Dresden   
* 2012, Won Youth Jury Award Honorable Mention for National Competition at Filmfest Dresden   German Short Film Awards    Audience Liberté Hamburg International Short Film Festival 
* 2012, Won Russian Film Clubs Federation Award and a First Place award for Best Short Film at Moscow International Film Festival       
* 2012, Won Audience Award at Filmfest München 
* 2012, Won Honorable Mention for Short Film at International Short Film Festival Oberhausen    Regensburg Short Film Week
* 2012, Won Kurosawa Award for creative excellence at 24FPS International Short Film Festival 
* 2012, Won Best Producer at Abu Dhabi Film Festival   

==References==
 

==External links==
*   at the Internet Movie Database
*  
*   official Facebook page

YouTube videos
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 