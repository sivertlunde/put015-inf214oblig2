My Bloody Valentine 3D
{{Infobox film
| name           = My Bloody Valentine 3D
| image          = Bloodyvalentine3dfinal.jpg
| caption        = Theatrical release poster
| director       = Patrick Lussier
| producer       = Jack L. Murray
| screenplay     = Zane Smith Todd Farmer John Beaird  }}
| story = Stephen Miller  
| starring = Jensen Ackles Jaime King Kerr Smith Betsy Rue Kevin Tighe Megan Boone Edi Gathegi
| music = Michael Wandmacher
| cinematography = Brian Pearson
| editing = Patrick Lussier Cynthia Ludwig
| distributor = Lionsgate Lionsgate
| released =  
| runtime = 101 minutes
| country = Canada United States
| language = English
| budget = $14 million {{cite web|title=My Bloody Valentine
|url=http://www.the-numbers.com/movies/2009/MBVAL.php}} 
| gross = $102.7 million (worldwide) $21.6 million (domestic DVD sales) 
}} of the 3D theatrical Lionsgate to generally mixed reviews but nevertheless a box office success. It was released on DVD and Blu-ray Disc|Blu-ray on May 19, 2009.

==Plot== Harry Warden (Richard John Walters), who survived by killing the other miners with a pickaxe, allowing himself to breathe. Tom Hanniger (Jensen Ackles), the mine owners son, was blamed for the mine disaster because he forgot to vent the methane lines and caused a cave-in, but also Harry Warden for killing the miners.
 Tom Atkins) arrives, finding multiple mutilated bodies many of which have been severed in half and the heart of a nurse inside a candy box. Burke and his partner come to the conclusion that Warden had awoken from his coma and, realizing they dont have much time left, they try to figure out where hes headed. Meanwhile, at the abandoned mine shaft that was the site of the disaster, a party is in full swing, attended by many teens, including Axel Palmer (Kerr Smith), his girlfriend, Irene (Betsy Rue), Tom Hanniger and his girlfriend, Sarah (Jaime King). Tom was initially reluctant to go, but he goes anyway. He leaves Sarah and goes back to his car to pick up some beers before going inside.

Sarah goes alone and gets lost looking for Axel and Irene. She runs across a teen and a few seconds later, he is stabbed through his eye. She is confronted by Warden in full miners garb, carrying a bloody pickaxe, and she flees, finding several dead bodies. Axel grabs her and they, along with Irene, hide from the killer. Warden eventually sees the trio, and they run out of the mine, meeting Tom as he comes in. Warden hits Tom with the pickaxe, injuring him, while the other three run for the car. Sarah tries to go back for Tom but Warden throws a pickaxe into the windshield of Axels truck, just an inch away from stabbing Sarah in the eye. Axel, realizing he has no other choice starts up his truck and leaves Tom behind. Warden turns his attention towards Tom, who runs back into the mine in an attempt to escape from him. Before Tom can be killed, Burke and his partner arrive and shoot Warden, who makes his getaway back into the mine. Tom also appears shocked by the events.

Ten years later, in 2008, Toms father, from whom Tom has been estranged, dies and Tom inherits the mine. Tom returns to town after his fathers funeral to sell the mine, having vanished since the incident; it was unknown where he was, since he was devastated by the events and unable to see Axel and Sarah after they had abandoned him in the mine to die. Axel is now sheriff and married to Sarah and they have a son, but he is cheating on her with Megan (Megan Boone), who works with Sarah at Sarahs grocery store.

After Megan and Axel have sex at his familys old house in the woods, Megan gives Axel a Valentines Day card and a box of candy and tells him that she is pregnant with his child. Meanwhile, at the motel where Tom is staying, Irene is having sex with a local trucker named Frank (Todd Farmer). After they finish, Frank reveals that he has been filming her. Irene goes out completely naked and angry about the events, drawing a gun on him. After he reveals that the gun is empty, she throws it and hits him, but he ignores her. Just as he enters the truck, he is impaled with a pickaxe through the head by a man in full miner garb, face completely covered by his mask. The killer chases Irene through the motel, but she hides under the bed. The noise draws Selene (Selene Luna), the midget motel owner, into  the room, and the miner kills her by impaling her whole body through the head on the lights above. Irene makes a noise and is discovered by the killer, who removes the bed mattress and stabs her to death. Axel is called to the scene of the crime, and finds Irene cut open, her heart missing; he also discovers Toms name on the register of people staying at the motel.

He returns to his office, where he sees a video of the miner following Irene. Axel and his partner Martin talk about how it cant be Warden, despite the rumors. Then Axel receives a candy box with Irenes heart inside, just like Warden had done with the nurse ten years ago. Meanwhile, Tom is visiting his newly inherited mine. In the mine, the killer traps Tom in a cage while William "Red" Kirkpatrick (Jeff Hochendoner) is calling for Ben, who is an old friend of the Hannigers. Warden proceeds to murder Red and a group of miners show up seconds later. Suspicion falls on Tom, despite the fact that he was locked in a cage the entire time. At the hospital afterwards, Axel asks Burke what happened to the killer. Burke says that he and Ben Foley (Kevin Tighe), the mine manager, killed and buried Warden. But when Axel, Tom, and Sarah accompany them to the burial spot, Wardens body is no longer there. Later on, Foley is at his house when he hears a noise. He goes outside with a shotgun to find nobody there, but when he goes back inside he is attacked by Warden, who knocks him down and impales his head with the pickaxe. The police find his body at the burial spot with his chest cut open. Axel, angry about the events, orders a police guard on his home to protect his son.

Meanwhile, Sarah is with Megan at the store when Warden arrives. He chases them through the store, but they lock themselves in the office. The window is locked, but Megan finds the key while Sarah is blocking the door with a table. Just as Warden breaks a hole in the door and unlocks it, Megan goes through the window and Sarah, who is right behind her, turns around and notices that Warden has disappeared from the office door. She quickly tries to pull Megan back into the office, knowing that Warden is going to the outside window but shes too late, as Warden arrives outside and grabs Megan. Sarah runs back through the store and encounters Axel outside. They find Megans mutilated body in the alley with the message "Be Mine 4 Ever" scrawled in blood on the wall above. Sarah ponders why Megan was killed when she had no relation to the mine. Sarah comes to the conclusion that the killer was trying to get at Axel, and she admits she knew about their affair. Axel then sends her to the hospital. Meanwhile, at Axel and Sarahs home, their son Noah is watching TV with his babysitter Rosa (Joy de la Paz), while Deputy Ferris (Karen Baum) is in the squad car watching the premises. Suddenly, Warden arrives and impales Rosa with the pickaxe and throws her into the washing machine, scalding her. Burke arrives and warns Ferris about Wardens presence.

Ferris goes to check the house, while Burke is at the porch. Ferris finds Noah and warns him to hide, and she finds Rosas mutilated body in the washing machine. She warns Burke, but he is killed by Warden. Meanwhile, Sarah is in the hospital and Tom calls her and convinces her that Warden is not the killer. He tells her that he needs her to trust him as he has found something he needs to show her. On the road Tom tries to convince Sarah that Axel is the killer. Axel calls Sarah while she and Tom are in the car; Axel tells her that Tom has been in a mental hospital for the last seven years since Harry Wardens rampage and that he is the killer.

Shocked, Sarah attempts to get Tom to drive her home and, when he refuses, she grabs the wheel and crashes Toms car. Tom is knocked out momentarily and Sarah escapes. She calls Axel and he instructs her to go to his fathers old house. When she enters she finds hundreds of candy heart boxes and her old picture with Tom. The killer, confirming that he is not Warden, appears. Sarah distracts him and runs into the mine shaft where the original murders took place. She runs into Axel and steals his gun. Pointing the gun at Axel she asks him about the hundreds of candy boxes. He seems confused and blames Tom. Tom then appears and Sarah points the gun at him too. Both men accuse the other of being the killer.

Tom gives himself away when he points out that the message over Megans dead body ("Be Mine 4 Ever") was the same as what Megan wrote to Axel in the Valentine card, revealing that he knows Megan is dead which is information that only Axel and Sarah knew. Tom begins to hallucinate when what seems to be Warden appears in the mine with them. Tom tries to warn Sarah but Axel points his flashlight into the area that Tom is pointing to and shakes his head no, she tells him that nothing is there. Warden walks right up to Tom then disappears. A montage then plays out all the murders in the film again, revealing Tom to be the true killer. Sarah tells Tom that Harry isnt there with them, to which Axel replies "Oh, hes here, arent you Harry? You living inside Tom ?" Tom turns to him, smirks, and says, "Oh, Im right here". Axel is infuriated and grabs the pickaxe, rushing at Tom. The two fight, Axel gains the upper hand and knocks Tom onto the ground, but Tom injures him with the pickaxe. Tom runs off and Sarah and Axel try to hide. Tom comes and finds them, smashing out lights with the pickaxe as he goes. As each light goes out, Tom flashes between being him, and being the miner. Sarah shoots Tom through his side and the bullet hits a gas tank, causing an explosion that causes the mine to collapse.

Rescue teams enter the mine, searching for the survivors. One lone rescuer finds Tom, barely conscious and covered in debris and planks of wood. He tells Tom not to worry, help is coming, when Tom suddenly grabs his pickaxe and impales the rescuer through his mask. Sarah and Axel get out, and Axel is taken away in an ambulance while Sarah is taken by Martin. Then a miner holding his injured side is seen leaving the mine. It is revealed to be Tom as he takes off the mask and walks away.

==Cast==
* Jensen Ackles as Tom J. Hanniger
* Jaime King as Sarah Palmer
* Kerr Smith as Axel Palmer
* Betsy Rue as Irene
* Edi Gathegi as Deputy Martin Tom Atkins as James "Jim" Burke
* Kevin Tighe as Ben Foley
* Megan Boone as Megan
* Karen Baum as Deputy Ferris
* Joy de la Paz as Rosa
* Marc Macaulay as Marc Riggs
* Todd Farmer as Frank the Trucker
* Selene Luna as Selene
* Jeff Hochendoner as William "Red" Kirkpatrick Harry Warden/The Miner
* David Theodor Blatt (uncredited) as hospital security guard

==Production==
The film was shot in South Western Pennsylvania, taking advantage of the states tax incentives for film productions as well as the topographical and architectural versatility of the Pittsburgh Metro area. Filming began on May 11, 2008 in  . Retrieved on January 14, 2008. 
 Ross Township was used as a location for one scene,  and a house on Hulton Road in Oakmont, Pennsylvania|Oakmont, a suburb of Pittsburgh, Pennsylvania|Pittsburgh, was also used as a location. 
 digitally in 4K resolution. Red One from Red Digital Cinema Camera Company, and the SI-2K Digital Cinema Camera by Silicon Imaging as digital cameras. Max Penner, the films stereographer, found these lighter and smaller cameras easier to use. 

==3D aspect== RealD technology and to have a wide release (1,000 locations) in 3D-enabled theaters.   The film was also available in 2D for theaters that were not equipped to process digital 3D technology.

==Reception== rating average of 5.8 out of 10.  However, its reception was relatively better than the original 1981 film, which only had 40% of positive reviews. On Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 51 based on 11 reviews. 

Joe Leydon of   (1982)". He added, in spite of the "state-of-the-art 3-D camera trickery, which helmer Patrick Lussier shamelessly exploits to goose the audience with cheap thrills and full-bore gore, My Bloody Valentine is at heart an unabashedly retro work, reveling in the cliches and conventions of the slasher horror pics that proliferated in the early 1980s". {{cite journal | last = Leydon | first = Joe | title = My Bloody Valentine Variety  | date = January 16, 2009 | url = http://www.variety.com/review/VE1117939353.html?categoryid=31&cs=1 | accessdate = January 26, 2009 }} 

Mark Olsen of the Los Angeles Times said, the implemented 3-D technology enables "startling effects, but after a while the minor thrill of the trick is gone. Advances in digital technology have allowed the filmmakers to largely avoid the physical headaches that are perhaps the biggest hallmark of the cyclical attempts at 3-D moviemaking". He added, "wooden performances by forgettable, generic actors -- again, just like in the original -- dont aid in making things any less leaden", concluding My Bloody Valentine 3D is "just good enough to not be annoying". 

Jeannette Catsoulis of The New York Times said, "the creaky screenplay (by Todd Farmer and Zane Smith) is mercilessly at odds with the directors fine sense of pacing. From the moment you duck a flying mandible and gaze, mesmerized, at a severed hand oozing two inches from your nose, youll be convinced that the extra dimension was worth seeking out. A strange synergy of old and new, My Bloody Valentine 3D blends cutting-edge technology and old-school prosthetics to produce something both familiar and alien: gore you can believe in". 

Clark Collis of Entertainment Weekly graded the film a C+ and said that it "starts in spectacular fashion. But what really leaps out at you about My Bloody Valentine 3-D is its lack of imagination".  Frank Scheck of The Hollywood Reporter felt, "While the concept of adding 3-D to the horror genre is hardly new ... Patrick Lussiers film is the most accomplished example. The 3-D effects come fast and furious, rendered with a technical skill and humor that gives this otherwise strictly formulaic slasher picture whatever entertainment value it possesses." He added, "the three leads actually manage to invest their roles with some depth, but the real acting treats come courtesy of veteran character actors Kevin Tighe and Atkins, whose presence provides a comforting bridge to horror films past. Special mention must also be made of supporting actress Betsy Rue, a real trouper who treats the target male audience to one of the longest and most unabashedly gratuitous full-frontal nude scenes in horror film history". 

===Box office===
On its 4-day opening weekend, the film grossed $24.1 million, ranking #3 for the weekend, behind   at #1.  In its second weekend, the movie grossed estimated $10.1 million, ranking number 6 at the domestic box office.  The film grossed $51,545,952 in the United States and Canada, and $49,188,766 in other markets for a worldwide total of $100,734,718.   

==Home media==
My Bloody Valentine 3D was released on DVD and Blu-ray Disc|Blu-ray on May 19, 2009 and has grossed in excess of $19.7 million,  with DVD sales and theater gross revenue totaling over $119.9 million.

Both home release versions have both a standard 2D version and the 3D version on the same disc using seamless branching.  However, a special Blu-ray version was also created specifically for online rental chains like Netflix and Blockbuster LLC|Blockbuster. 

On October 5, 2010, Lionsgate Home Entertainment released My Bloody Valentine 3D on Blu-ray 3D which requires a 3D-capable HDTV, 3D Blu-ray player and 3D glasses. The disc also includes a 2D version of the film and all bonus materials included in the 2D Blu-ray version released after the films initial theater run.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 