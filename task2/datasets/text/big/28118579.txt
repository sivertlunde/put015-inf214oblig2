Hollywood on Parade No. A-8
{{Infobox Film
| name           = Hollywood on Parade No. A-8
| director       = 
| starring       = Bela Lugosi Bonnie Poe Eddie Borden Rex Bell
| distributor    = Paramount Pictures
| released       = 10 March 1933
| runtime        = 10 min (One-Reel film) 
| country        = USA
| language       = English
}}

Hollywood on Parade No. A-8  (1933) is an Paramount Pictures short starring Bela Lugosi and Betty Boop.

==Plot summary==
The waxwork figure of Eddie Borden comes to life and introduces various stars from the Hollywood Hall of Fame. and tells the audience about the various stars such as Clara Bow. Clara Bows husband, Rex Bell, suggests that Eddie get it on with Betty Boop. Betty asks Eddie to accompany her in a rendition of "My Silent Love." Count Dracula, who is played by Bela Lugosi, comes to life, gets Betty Boop in the Clinch fighting|clinch, bends over her menacingly and sensually at the same time, and utters: "Boop! You have Booped your last boop!"

==Cast==
*Eddie Borden as Himself
*Bonnie Poe as Betty Boop
*Bela Lugosi as Dracula
*Rex Bell as Himself

==Soundtrack==
*"My Silent Love"
:Sung by Bonnie Poe

==Controversy==
Betty is portrayed by singer-actress Bonnie Poe who was one of several actresses who voiced the star in the animated Betty Boop cartoons. the controversy comes via Helen Kane, the “original Boop-Oop-a-doop girl,” a popular singer who capitalized on her novel coquettish voice to become an on-stage hit in the late 1920s through early 1930s. Helen Kane sued over the Impersonation of her as Betty Boop. any such appearance as Betty Boop would have negated her claim.

==External links==
* 

 
 
 
 
 
 
 
 
 

 