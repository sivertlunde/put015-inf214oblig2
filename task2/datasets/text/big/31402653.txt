Kumari (1952 film)
 
{{Infobox Film
| name           = Kumari குமாரி
| image          = Kumari 1952.jpg
| director       = R. Padmanaban
| writer         = Ku. Sa. Krishnamurthi (story)  S. N. Santhanam (dialogue) Sriranjani Madhuri Devi Serukulathur Sama 
| producer       = R. Padmanaban
| distributor    = 
| music          = K. V. Mahadevan
| cinematography = 
| editing        = 
| released       = 11 April 1952
| runtime        = 
| budget         = 
| gross          =  Tamil
}}

Kumari ( ) is a 1952 Tamil-language film starring M. G. Ramachandran, and Sriranjani (junior)|Sriranjani, in the lead roles.

== Plot ==

The story begins with a princess meeting with an accident and her subsequent rescue by a young commoner. The princess and the commoner soon fall in love and wish to get married. But the king and queen plan to marry off the princess to the queens brother.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran ||
|- Sriranjani ||
|-
| Madhuri Devi ||
|-
| Serukulathur Sama ||
|-
| T. S. Durairaj ||
|-
| Pulimoottai Ramasamy ||
|-
| K. S. Angamuthu ||
|-
| C. T. Rajakantham  ||
|}

==Crew==
* Producer: R. Padmanaban
* Production Company: La R.Batmanabin Rajeshwari
* Director: R. Padmanaban
* Music: K. V. Mahadevan
* Lyrics: Ku. Sa. Krishnamurthi & T. K. Sundhara Vathiyar
* Story: Ku. Sa. Krishnamurthi 
* Screenplay: 
* Dialogues: S. M. Santhanam
* Art Direction:
* Editing: 
* Choreography: 
* Cinematography: T. Marconi 
* Stunt:
* Dance:

==Soundtrack==
The music was composed by K. V. Mahadevan.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Azhiyaadha Kaadhal Vaazhvil Anaiyaadha Jothiyaai || A. M. Rajah || || 
|- 
| 2 ||  ||  || ||
|- 
| 3 ||  ||  || ||
|- 
| 4 ||  ||  || ||
|- 
| 5 ||  ||  || ||
|- 
| 6 ||  ||  || ||
|- 
| 7 ||  ||  || || 
|- 
| 8 ||  ||  || || 
|- 
| 9 ||  ||  || || 
|}

==References==
* 
 

 
 
 
 
 


 