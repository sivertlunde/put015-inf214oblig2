Neverwas
{{Infobox film
| name = Neverwas
| image = Neverwas (2005).jpg
| caption =
| director = Joshua Michael Stern
| producer = Sidney Kimmel Greg Shapiro
| writer = Joshua Michael Stern
| starring = Ian McKellen Aaron Eckhart Brittany Murphy Nick Nolte Jessica Lange
| music = Philip Glass
| cinematography = Michael Grady
| editing = Steven Kemper
| studio = Kingsgate Films Legacy Filmworks Sidney Kimmel Entertainment
| distributor = Neverwas Productions
| released =  
| runtime = 108 minutes
| country = United States Canada
| language = English
| budget =
}} fantasy drama straight to DVD in 2007.

==Plot==
Zach Riley (Aaron Eckhart) is a psychiatrist who leaves a job at a prestigious university to take up a job at the privately run mental institution, Millwood, belonging to Dr. Reed (William Hurt). What he doesnt reveal at the time of his appointment is that this was the very place where his novelist father, T.L. Pierson (Nick Nolte), spent many years of his life as he battled chronic Depression (mood)|depression. T.L. later wrote a popular childrens classic, Neverwas, about a child (based on young Zach himself) who enters a secret world to free a captive king. T.L. later committed suicide; Riley, who found the body, has always partly blamed himself for his fathers death.
 schizophrenic patient, Gabriel Finch (Ian McKellen), and soon realizes that Finch sees himself as the captive king. As he listens to Finch and resumes his acquaintance with childhood friend Maggie Paige (Brittany Murphy) he realizes that more things link him to the book—and also to Finchs recovery – than he ever thought. T.L.s novel was based on Finchs stories, told to him by Finch while both were patients in the hospital. Finch believes that the book is a sort of oracle confirming his personal reality, and that Riley is the boy hero. Riley comes to see himself this way in a sense, as he discovers that Finchs "hallucination" concerns actual places and events.   

==Cast==
* Ian McKellen as Gabriel Finch
* Aaron Eckhart as Zach Riley
* Brittany Murphy as Maggie Paige
* Nick Nolte as T.L. Pierson
* Alan Cumming as Jake
* Vera Farmiga as Eleanna
* Bill Bellamy as Martin Sands
* Michael Moriarty as Dick
* Jessica Lange as Katherine Pierson
* William Hurt as Dr. Peter Reed

==Production==

===Development=== Miramax Home Entertainment for its home media release.  Owing to the theme of a fairy tale which is based on real events, the film has often been compared with Finding Neverland (2004). 

===Filming===
Principal photography took place in September 2004 in Vancouver, British Columbia, Canada.  

==References==
 

==External links==
*  
*   Variety
*  
*  
*  

 
 

 
 
 
 
 
 
 