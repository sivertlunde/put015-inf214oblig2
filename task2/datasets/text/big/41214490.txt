Rangi's Catch
 
 
{{Infobox film
 | name = 
 | image = 
 | caption = 
 | director = Michael Forlong 
 | producer =  Michael Forlong
 | writer =  Michael Forlong 
 | starring = Temuera Morrison Ian Mune 
 | music = 
 | cinematography = 
 | editing = 
 | distributor =
 | released = 1986 
 | runtime = 72 min. (theatre)   128 min. (TV series)  English
 | budget =
 | gross = 
 }}
Rangi’s Catch is a 1973 British film for children set and shot in New Zealand, with one (Rangi) played by a young Temuera Morrison in his first role. Originally made with eight episodes for television; edited and cut for theatical release.

==Plot==
Four children on a remote sheep station in the South Island of New Zealand hear of the escape of two convicts, and realise that the crooks are responsible for burgling their house while they were swimming. They pursue the crooks, and despite being detained by the police help catch the crooks and their stolen money hidden in a cave, so are rewarded. They return to their idyllic rural existence

==Cast==
* Temuera Morrison ... Rangi
* Andrew Kerr .... Johnny Murray 
* Kate Forlong .... Jane Murray 
* Vernon Hill .... Hemi 
* Ian Mune .... Jake, crook 
* Michael Woolf .... Bill, crook 
* Don Selwyn .... Mr Rukuhia 
* Hannah Morrison .... Mrs Rukuhia 
* Peter Vere-Jones Mr Murray 
* Christine Elsdon .... Mrs Murray

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p57 (1997, Oxford University Press, Auckland) ISBN 019 558336 1

<!-- ==External links==
*  
*    -->

 
 
 
 
 
 
 
 
 
 


 
 