Prince Avalanche
{{Infobox film
| name           = Prince Avalanche
| image          = Prince Avalanche Official Poster.jpeg
| caption        = Theatrical release poster
| director       = David Gordon Green
| producer       = {{Plain list |
*Lisa Muskat
*Derrick Tseng
*Craig Zobel
*James Belfer
*David Gordon Green
 
}}
| screenplay     = David Gordon Green
| based on       =  
| starring       = {{Plain list |
*Paul Rudd
*Emile Hirsch
}}
| music          = {{Plain list |
*Explosions in the Sky
*David Wingo
}}
| cinematography = Tim Orr
| editing        = Colin Patton
| studio         = Dogfish Pictures
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $725,000
| gross          = $442,313 
}}
 Either Way (Á annan veg). The film was shot in Bastrop, Texas, after the Bastrop County Complex fire.

==Plot==
In 1988, an odd pair of sorts, meditative and stern Alvin (Paul Rudd) and his girlfriends brother, Lance (Emile Hirsch), dopey and insecure, leave the city behind to spend the summer in solitude repainting traffic lines down the center of a country highway ravaged by wildfire. As they sink into their job in the remarkable landscape, they learn more than they want to about each other and their own limitations. An unlikely friendship develops through humor and nasty exchanges, leading to surprising affection.

==Cast==
* Paul Rudd as Alvin
* Emile Hirsch as Lance
* Lance LeGault as Truck Driver
* Joyce Payne as Lady

==Production== Either Way, the script for Prince Avalanche consisted of roughly 65 pages – about 30 pages short of an average feature-length screenplay.  From development onward, the film was fast tracked to completion. "We really didn’t have time for proper or traditional development," said Gordon Green. "We had the idea in February of 2012, we were filming in May, and sound mixing in July. It was an unusually tight production schedule."    Paul Rudd joked to an Entertainment Weekly interviewer, "I found the biggest challenge of working on this was trying to stifle my Alpha (ethology)|alpha-male  ." 

The films entire production was done in secret; it was only announced to the public after completion in June 2012. This was at the request of the director, David Gordon Green, who wanted to get back to his independent roots after his last three films were completed by a major film studio.    Principal photography for Prince Avalanche began in May 2012 and lasted for 16 days in Bastrop State Park. Because of its scale, the film was shot with a small 15-person film crew.  While already mid-shoot, the film crew came across Joyce Payne, a resident of the area whose home was destroyed in the fire.  Fascinated by her story, Gordon Green included her in the film. "It wasn’t scripted at all; it was something very real for her that we documented," said Gordon Green. "It ended up being pivotal. I couldn’t imagine the movie without it now." 

==Release== premiered at the 2013 Sundance Film Festival on 20 January.    Then it had its international premiere in competition at the 63rd Berlin International Film Festival on 13 February 2013    where David Gordon Green won the Silver Bear for Best Director.    It subsequently screened within such U.S. festivals as South by Southwest and Maryland Film Festival.

==Reception==
Prince Avalanche received positive reviews and has a rating of 83% on Rotten Tomatoes based on 105 reviews with an average score of 7 out of 10. The consensus states: "A step back in the right direction for director David Gordon Green, Prince Avalanche shambles amiably along with a pair of artfully low-key performances from Paul Rudd and Emile Hirsch." 

==Soundtrack==
{{Infobox album  
| Name       = Prince Avalanche
| Type       = Soundtrack
| Artist     = Explosions in the Sky & David Wingo
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = Soundtrack
| Length     =  
| Label      = Temporary Residence
| Producer   = 
| Last album = 
| This album = 
| Next album = 
| Misc        = 
}}
{{Album ratings
|MC=72/100 
|rev1=Allmusic
|rev1Score=  
|rev2=Alternative Press
|rev2Score=  
|rev3=Clash (magazine)|Clash
|rev3Score=7/10 
|rev4= Consequence of Sound
|rev4score= C+ 
|rev5=Drowned in Sound
|rev5Score=8/10 
|rev6=Exclaim!
|rev6Score=6/10 
|rev7=NME
|rev7Score=6/10 
|rev8=Pitchfork Media
|rev8Score=6.5/10 
|rev9=Popmatters
|rev9Score=7/10 
|rev10=Rolling Stone
|rev10Score=  
}}
The soundtrack was scored by Explosions in the Sky and David Wingo. 

{{tracklist
|-
|title1=Fires
|length1=1:29
|-
|title2=Theme from Prince Avalanche
|length2=2:22
|-
|title3=Dear Madison
|length3=1:47
|-
|title4=Passing Time
|length4=1:51
|-
|title5=Rain
|length5=1:08
|-
|title6=Alone Time
|length6=4:59
|-
|title7=Hello, Is This Your House?
|length7=4:10
|-
|title8=Cant We Just Listen to the Silence
|length8=1:35
|-
|title9=Wading
|length9=1:38
|-
|title10=Dear Alvin
|length10=1:21
|-
|title11=The Lines on the Road that Lead You Back Home 
|length11=2:00
|-
|title12=An Old Peasant Like Me
|length12=3:47
|-
|title13=Join Me on My Avalanche
|length13=3:29
|-
|title14=The Adventures of Alvin and Lance
|length14=1:50
|-
|title15=Send Off
|length15=4:09
}}

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 