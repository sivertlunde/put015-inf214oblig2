Something Remote
{{Infobox film
| name           = Something Remote
| image          = Something Remote logo small.png
| caption        = Webisode logo
| director       = Alex Laferriere
| producer       = Nick Allain Steve DiTullio
| writer         = Alex Laferriere Rebecca Davis Sarah Neslusan Matt Heron Duranti
| music          = Talco
| cinematography = Alex Laferriere
| editing        = Alex Laferriere John Selig
| studio         = Broken Wall Films
| distributor    = IndieFlix
| released       =  
| runtime        = 92 minutes (original cut) 86 minutes (Plus Edit)
| country        = United States
| language       = English
| budget         = United States dollar|US$150
}}
 independent comedy film written and directed by Alex Laferriere, and produced by Nick Allain and Steve DiTullio.  Starring C/J Haley, Rick Desilets, and John Selig, it follows three overeducated, television|TV-addicted roommates trying to stay together as an ex-girlfriend tries to tear them apart.  Something Remote was the first feature-length film by Broken Wall Films.
 Gen Con Indy 2010 and the 2010 Wanderings Film Festival.   

==Plot==

 

==Cast==
*C/J Haley as Neil Trembley: An office drone who just wants to get home and hang out with his roommates.  He spends his days working a mindless, dead-end job he is unsatisfied with, and is just coming out of a long-term, apparently one-sided relationship.  He has difficulty making decisions for himself, and relies on his best friends and roommates Mat and Erik.  George Aldrich was originally cast as Neil, but after he left the production Haley was cast on the first day of filming.   
* ".  While he puts up with Erik, Mat has no patience for Neils ex-girlfriend Lisa, constantly berating her while Neil is present and trying to prevent Neil from talking to her.  Desilets is the only actor retained from the original Sofa King Kilroy sketch comedy show, and is the nephew of comedian Dane Cook. 
*John Selig as Erik Goulding: Neils other, internet-addicted roommate.  Arguably more obsessed with the internet than with television, Erik only tends to watch TV so that he can spend time hanging out with Neil and Mat.  He is very non-confrontational, and does what he can to keep everyone together and happy.  Unlike Mat, Erik actually likes Lisa, and tries to help Neil reconcile with her. Rebecca Davis as Lisa: Neils psycho ex-girlfriend.  Going out of her way to make Neils life miserable, she refuses to leave his apartment.  She is fed up with dealing with the three guys, and is back for something Neil took from her.
*Sarah Neslusan as Shannon: One of Neils friends, and girlfriend of Skott.  Arriving at the apartment to go on a double-date with Neil and Lisa, she quickly finds herself at odds with Mat.  She is fairly amicable, however, with Neil and Erik.  Neslusan was the first actor to audition at the casting call, and the role of Shannon (and a substantial portion of the full script) was written into the film specifically for her after her audition for the part of Lisa.    
*Matt Heron Duranti as Skott: A friend of Neils, dating Shannon.  One of the few real friends the three roommates have, he does what he can to defuse arguments before they get worse, usually to no avail.  He is whipped by Shannon, and tends to wind up standing between her and Mat.

The cast also includes Sammi Lappin as "Abby", a random passerby hired to pretend to be Neils new girlfriend (whose actual name remains a mystery), Hunter Giles as Homeless Al, a hobo hired to do the same thing for Lisa, and Jeffrey Desautels as Bruce, one of Lisas coworkers.  Director Alex Laferriere and producer Nick Allain have a brief cameo as Neils boss and bosss underling, respectively.  Assistant producer Steve DiTullio also appears in the film as Nelsin, a passerby with the ability to turn his surroundings into a kung fu movie.

==Production==
{{Infobox television
| show_name            = Something Remote
| image                =  
| caption              = Something Remote webisode logo
| genre                = Comedy Satire
| creator              = Alex Laferriere
| writer               = Alex Laferriere
| director             = Alex Laferriere
| starring             = C/J Haley Rick Desilets John Selig Matt Heron Duranti Sarah Neslusan Talco
| opentheme            = "Tortuga"
| country              =   English
| num_seasons          = 1
| num_episodes         = 11
| producer             = Nick Allain Steve DiTullio
| editor               = Alex Laferriere
| location             = Worcester, Massachusetts
| cinematography       = Alex Laferriere
| camera               = Single-camera
| runtime              = 1-9 minutes
| company              = Broken Wall Films
| channel              = SomethingRemote.com (via Viddler) HDTV 1080i
| audio_format         = Stereophonic
| first_aired          =  
| last_aired           =  
| status               = Concluded Something Remote (film)
| website              = http://www.somethingremote.com/
| production_website   = http://www.brokenwallfilms.com/
}}

In December 2007, Worcester Polytechnic Institutes sketch comedy troupe Kilroy Sketch Comedy put on a show called  .  The shows "omniscript", written by Kilroy member Caleb Wrobel with edits and continuity added by Alex Laferriere, focused on roommates Neil, Mat, and Erik as they dealt with Neils ex-girlfriend Lisa trying to break into the apartment.  The show became a 17-minute stageplay broken up by television sketches, in a similar manner to that of the film.  Several Kilroy members would eventually work on Something Remote.

Auditions were held on June 3, 2008, bringing in several dozen local actors trying out for the film.  At the first readthrough, the original cast saw George Aldrich as Neil, Rick Desilets reprising the role of Mat, John Selig as Erik, Rebecca Davis as Lisa, Sarah Neslusan as Shannon, Matt Heron Duranti as Skott, Sean Cusick as Homeless Al, and Jeffrey Desautels as Bruce.  At the time, no one had been cast as "Abby".  Due to unknown conflict, Aldrich dropped out of the project, and was replaced on the first day of filming by Chris "C/J" Haley.  Sean Cusick also dropped out, and was replaced by Hunter Giles, and "Abby" was soon given to Sammi Lappin.

Filming began on July 1, 2008 in director Alex Laferrieres actual apartment at 7 Goulding St. in Worcester, Massachusetts.  Alex and his roommates Neal Humphrey, Matt Lowe, and Eric Kolodziecjzak (the namesakes and inspirations for the characters of Neil, Mat, and Erik) had to live with a fully equipped set in their living room for 40 days while the movie was shot.  The portion of the script involving Skott and Shannon (nearly one sixth of the full script) was filmed entirely on one day.

Because of Worcesters unpredictable weather (in fact, during the filming it rained nearly every day for six weeks), and to allow for late filmings, posters were used to cover the windows.  This was later said to be a nod to the movie Clerks, which was a major inspiration for Something Remote.  Additionally, to maintain continuity on-set, all of the on-table props were fixed in place with strips of Velcro.  While much of the apartments geography is used accurately in the film, it should be noted that the door used to leave the apartment in the film actually leads to the side of the apartment building, whereas the front door to the building leads to a door near the television.

Filming on-set wrapped on August 10, 2008, ending with the "pie shot" used as a stinger after the films credits.  External shots and sketch filming continued as late as October 26, 2008, the day before several film festival submission deadlines.  The final film length came to 92 minutes.  While the film failed to be accepted into thirteen film festivals, Broken Wall Films held a premiere screening event at the Elm Draught House Cinema on January 9, 2009, primarily to friends and family of the cast and crew.

Laferriere and Selig have recently re-edited the film, fixing color and audio issues, as well as trimming the fat on the film itself.  This "Plus Edit" clocked in at 86 minutes.

==Cultural references==
Due to the pop culture nature of Something Remote, the film is littered with cultural references, both veiled and explicit.

==Reception==
Initial reactions to the film were mixed.  While local newspapers had positive reviews,    over a dozen film festivals rejected Something Remote.  In the summer of 2009, Broken Wall Films began "touring" Something Remote to various colleges, gaining momentum and favorable responses.  After entering into the Silk City Flick Fest and competing against films with budgets over 1000 times as large, Something Remote was awarded "Best Feature" and "Funniest Flick".
 Gen Con Indy 2010. 

==DVD release==
The Something Remote film DVD was officially released on September 14, 2009, and is available from the SomethingRemote.com store.  It also became available via IndieFlix on February 10, 2010.

==Web series==
In an effort to advertise the films DVD release, Broken Wall Films produced Something Remote, a weekly web television series of the same name as the feature-length film.  The first episode was webcast on June 28, 2009, on Something Remotes official site via Viddler, as well as the following day on a dozen online video sharing websites, with new webisodes scheduled every Sunday. The show followed roommates Neil Trembley, Mat Highland, and Erik Goulding as they struggled to live together in peace after their television ― the one thing they had in common ― was stuck on the Spanish Home Shopping Network.

The series was written and directed by Alex Laferriere and produced by Nick Allain and Steve DiTullio through Broken Wall Films. The Something Remote website also featured an RSS feed for video podcasting purposes. Something Remote: The Complete First Season was released on September 21, 2009.
 remote is broken, leaving them stuck on the Spanish Home Shopping Network.  While all this is going on, Neil encounters friction, both with his friends Mat and Erik, and with his girlfriend Lisa.  The conflict inevitably leads to Neil and Lisa breaking up (on ambiguous terms) and Neil stealing her oversized remote, leading into the plot of the film.

The series featured C/J Haley, Rick Desilets, John Selig, Matt Heron Duranti, and Sarah Neslusan continuing in their roles as Neil, Mat, Erik, Skott, and Shannon.  It also featured Broken Wall Films veteran Dustin Deren as the oafish Officer Red, Steve Harwick as Steev (a caricature of himself), and Jim Perry (the owner and operator of the Elm Draught House Cinema) as himself.

In the pilot episode, "Something Broken", Erik challenges Mat and Neil to a blogging competition: the blog with the most views after one year receives $50 from each of the losers.  They all accept and begin blogging to win the hundred dollars.  In "Something Popping", Erik mentions that someone on his blog claimed that "corn in the microwave is a no-no idea".  In "Something Lost in Time", Mat attempts to convince Erik that he has over 50,000 views on his blog, while in actuality it had received less than 3,500 on the episodes release.  Neils girlfriend Lisa had begun posting replies on Neils blog, interacting with Mat and other posters.  These interactive blogs are accessible through Something Remotes official website.  Unregistered users can view videos and posts but must create an account to make comments, which are currently moderated.

Something Remote ran again on   with new content organized and produced by Rick Desilets.  Along with the original webisodes, interviews with Laferriere,     Allain,     DiTullio,  Haley,  Selig,  Duranti,    and Neslusan  were released, as well as a short titled "Mantequilla Magica"    and the theatrical trailer for Command & Conquer 3: The Forgotten.     A sneak peek at the feature-length film was released the week after series finale.    

== Episodes ==
{| class="wikitable" width="100%" style="text-align:center;"
! colspan="7" style="background: #FF9900;"| Something Remote episodes
|-
! style="background: #FFCC7F;" width="5%"| #
! style="background: #FFCC7F;" width="25%"| Title
! style="background: #FFCC7F;" width="5%"| Length
! style="background: #FFCC7F;" width="30%"| Featuring
! style="background: #FFCC7F;" width="15%"| Initial Release
! style="background: #FFCC7F;" width="15%"| Re-release
! style="background: #FFCC7F;" width="5%"| Prod. Code
|-
| 1
|  
| 5:13
| 
| June 28, 2009
| May 16, 2010
| SR201
|-
| 2
|  
| 2:31
| 
| July 5, 2009
| May 23, 2010
| SR203
|-
| N/A
|  
| 1:13
| Brandon Vogel, Jon Zoll, Sarah Lofgren
| 
| May 26, 2010
| SR212
|-
| 3
|  
| 3:43
| Dustin Deren
| July 12, 2009
| May 30, 2010
| SR206
|-
| 4
|  
| 4:52
| 
| July 19, 2009
| June 6, 2010
| SR204
|-
| 5
|  
| 3:44
| 
| July 27, 2009
| June 13, 2010
| SR208
|-
| 6
|  
| 3:10
| Alex Laferriere
| August 2, 2009
| June 20, 2010
| SR207
|-
| 7
|  
| 2:53
| 
| August 9, 2009
| June 27, 2010
| SR205
|-
| 8
|  
| 6:07
| Steve Harwick, Jim Perry
| August 17, 2009
| July 4, 2010
| SR209
|-
| 9
|  
| 3:51
| Matt Heron Duranti, Steve DiTullio, Jon Zoll
| August 23, 2009
| July 11, 2010
| SR210
|-
| 10
|  
| 8:52
| Matt Heron Duranti, Sarah Neslusan, Dustin Deren
| August 30, 2009
| July 18, 2010
| SR211
|-
| 11
|  
| 1:47
| 
| September 6, 2009
| July 25, 2010
| SR202
|}

== References==
 

== External links ==
* 
* 
*  at the Internet Movie Database
* 
* 

 
 
 
 
 
 