Capulina Speedy González
{{Infobox film
| name           = Capulina Speedy González
| image          = Capulina_Speedy_Gonzalez_movie_poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release lobby card
| director       = Alfredo Zacarías
| producer       = Alfredo Zacarías
| writer         = Alfredo Zacarías
| screenplay     =
| story          =
| based on       =  
| narrator       = 
| starring       = Gaspar Henaine Leonorilda Ochoa Xavier Loyá
| music          = Ernesto Cortázar
| cinematography = Javier Cruz
| editing        = Federico Landeros
| studio         = Estudios América, S.A.
| distributor    = 
| released       =   
| runtime        = 93 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
Capulina Speedy González is a 1970 Mexican Western (genre)#Film|western-comedy film starring Gaspar Henaine in the title role and Leonorilda Ochoa with Víctor Alcocer and Julián de Meriche.

Directed, produced, and written by Alfredo Zacarías, the film revolves around the hilarious mishaps of a Mexican courier working for an American post office who is framed by two U.S. army deserters-turned-bandits.

==Cast==
*Gaspar Henaine as Capulina Espiridión González 
*Leonorilda Ochoa as Rosita Smith  
*Xavier Loyá as Timothy 
*Jorge Rado as Sheriff 
*John Kelly as Sergeant
*Víctor Alcocer as Priest 
*Nathanael León as Bartender   
*Conjunto "Los Tonchis"
*Jorge Casanova as Mailman
*Julián de Meriche as Judge
*Juan Garza as Sheriffs assistant 
*Jesús Gómez as Courier
*Víctor Almazán as Townsman

==External links==
* 

 
 
 
 
 
 
 


 
 
 