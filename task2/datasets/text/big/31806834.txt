What's New About Love?
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Whats New About Love?
| image          = Cartaz para O Que Há De Novo No Amor?.jpg
| caption        = 
| director       = Mónica Santana Baptista Hugo Martins Tiago Nunes Hugo Alves Rui Santos Patrícia Raposo
| producer       = Maria João Sigalho
| screenplay     = Mónica Santana Baptista Hugo Martins Tiago Nunes Hugo Alves Rui Santos Patrícia Raposo Octávio Rosado
| based on       =
| starring       = Joana Santos Miguel Raposo David Cabecinha João Cajuda Nuno Casanovas Inês Vaz
| music          = Peixe:Avião Os Golpes Os Velhos Samuel Úria
| cinematography = Daniel Neves
| editing        = Rui Santos
| studio         = Rosa Filmes
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        =  
| language       = Portuguese
| budget         = €500 hundred
}} Portuguese feature film in six segments, directed by Mónica Santana Baptista, Hugo Martins, Tiago Nunes, Hugo Alves, Rui Santos and Patrícia Raposo, produced by Rosa Filmes.

==Cast==
* Joana Santos as Rita
* Miguel Raposo as Edu
* David Cabecinha as João
* João Cajuda as Marco
* Nuno Casanovas as Samuel
* Inês Vaz as Inês
* Ângelo Rodrigues as Rafael
* Diana Nicolau as Maria
* Sofia Peres as Lígia
* João Pedro Silva as Tiago
* Joana Metrass as Vera
* Raquel André as Sofia
* Sónia Balacó as Bárbara

== Soundtrack ==
Includes songs by the alternative Portuguese rock bands  , besides some songs composed by Hugo Alves, one of the directors. The singer-songwriter Samuel Úria performs in the film, playing two of his songs: Barbarella e Barba Rala and Não Arrastes o Meu Caixão. 

== Reception ==
Whats New About Love? was awarded the TAP prize for the best Portuguese feature film at the 2011 edition of the IndieLisboa film festival.    The film had its international premiere as part of the official selection of the 19th Raindance Film Festival in London. The film had its South American premiere at the São Paulo International Film Festival, and was also present in other Portuguese-language festivals, like the Santa Maria da Feira Luso-Brazilian Film Festival and the Toronto Portuguese Film Fesival.

== External links ==
*  
*  

==References==
 

 
 
 