The Emperor and the Golem
{{Infobox film name           = The Emperor and the Golem image          =
 |caption= writer         = {{Plainlist|
* Jan Werich
* Jiří Brdečka
}} starring       = {{Plainlist|
* Jan Werich
* Jiří Plachý
* Zdeněk Štěpánek
* Bohuš Záhorský
* František Filipovský
* Marie Vášová
* Nataša Gollová
* František Černý
}} director       = {{Plainlist|
* Martin Frič
* Jiří Krejčík
}} producer       =  music          = Julius Kalaš cinematography =  editing        =  distributor    =  released       = 1951 (Czechoslovakia) 1955 (USA) runtime        = 86 + 69 min or 112 min budget         =  country        = Czechoslovakia language       = Czech
}} Czech History|historical scenes cut color (not common for Czechoslovak films in that period) because of the international release.

The film is set during the reign of Rudolf II, Holy Roman Emperor. It is one of the best known film with Jan Werich (performing a dual role of Emperor and baker).

==Plot==
  from the film]]

Rudolf is obsessed with finding the Golem. He and his inventor Scotta later accidentally stumble upon him while performing a magic ritual (not as its result). But he does not have the Shem and cannot awake him.

It first follows the aging and eccentric Rudolf II, who refuses to hear out ambassadors and falls into fits, destroying vases and dishes. The conflict between him and his brother Matthias, Holy Roman Emperor|Matthias, is demonstrated here. Edward Kelly in his palace and shows him around.

He keeps an alchemist laboratory in his palace, where generally either swindlers or fools work (one desires to materialize dark, another hits an anvil with a hammer, examines it with a magnifying glass and says hes trying to fission an atom in order to change it into gold). There is also a character who, when the Emperor asks him of his doings, always begins to babble in a senseless language (in the end, it is revealed that he has been making "slivovitz|slivovice", a well known Czech plum brandy).

One character, Alessandro Scotta (called Honza Skoták), appears repeatedly: instead of experimenting, he cooks sausages. He also approaches the emperor with a cleaning product, with which he immediately cleans the floor, making it slippery and causing another person to slip, to the amusement of the Emperor.

The movie then follows a baker, Matew, Matěj (also played by Werich, but as a younger man than Rudolf) who is confronted with angry people who want rolls but cannot receive them, because they need to be "especially baken" for the emperor. He is later imprisoned in the dungeons for spreading rolls to poor people.

Later on, Kelly reveals his homunculus Sirael, to whom the Emperor wishes to teach everything of our world (not knowing that she is a plain country girl, acting after coercion by Kelly). She and Matěj communicate through vents between Kellys room and the dungeons.

Later the Emperor takes part in what is supposed to make him young again by Alessandro Scottas drink of youth (which is actually a mix of strong alcoholic drinks and morphium). After waking up, he sees Matěj, who escaped the dungeons meanwhile, mistaking him for him-younger-self in the mirror. Then he and his entourage ride on a carriage to the countryside to remind themselves of the sins of their misspent youth.
 rejuvenation to have worked out, hold him for the emperor. Matěj gets into an embarrassed scene with the emperors "consort" (something like a guest, only longer staying and generally arguing with Rudolf). He believes he killed her and puts her on top of a two-story bed.

A sphere of intrigue (shown rather humorously) between the emperors councilors is shown and all basically want the shem, which a hound is spotted to have.
Matěj (as the emperor) sits as a table with Tycho de Brahe and talks about planets and as the astronomer demonstrates the moving of planets according to Nicolaus Copernicus|Koperník with the wine grails, he mixes a wine grail with poison, arranged by the councilors. They are stunned, unknowing which grail is the poisoned one, take theirs on Matějs demands.

The situation escalates, as the Golem is awakened and destroys the general, who wanted to use him to conquer the whole world. The Emperor returns and chaos is at large.

==Cast==
* Jan Werich as Rudolf II / pekař Matěj
* Marie Vásová as Countess Katharina Strada
* Nataša Gollová as Kateřina aka Sirael
* Bohuš Záhorský as Lang, komoří
* Jirí Plachý as Edward Kelley
* Zdeněk Štěpánek as Rusworm, maršál
* František Filipovský as Court astrologer
* František Černý as Scotta
* Václav Trégl as Emperors servant
* Vladimír Leraus as Hungarian delegate
* Miloš Nedbal as Court doctor
* Bohuš Hradil as Tycho de Brahe (as B. Hradil)
* František Holar as Commander of guard (as F. Holar)

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
  
 