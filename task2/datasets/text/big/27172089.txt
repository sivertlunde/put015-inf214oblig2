Annabel Takes a Tour
{{Infobox film
| name           = Annabel Takes a Tour
| image          =
| image size     =
| alt            =
| caption        =
| director       = Lew Landers
| producer       = Lou Lusty
| writer         = Joe Bigelow (story) Olive Cooper (writer)
| narrator       =
| starring       = Jack Oakie Lucille Ball
| music          = Robert Russell Bennett
| cinematography = Russell Metty
| editing        = Harry Marker
| studio         =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 comedy directed by Lew Landers, starring Lucille Ball and Jack Oakie. Annabel (Lucille Ball) is on a promotional tour and as a publicity stunt, leaks a story that she is having a romantic fling with a famous romance novelist. 

==Plot==
 

==Cast==
* Jack Oakie as Lanny Morgan
* Lucille Ball as  Annabel Allison
* Ruth Donnelly as Josephine Jo
* Bradley Page as Howard Webb, Chief of Wonder Pictures
* Ralph Forbes as Viscount Ronald River-Clyde
* Frances Mercer as Natalie Preston
* Donald MacBride as Thompson, RR Conductor
* Alice White as Marcella, Hotel Manicurist
* Chester Clute as Pitcarin, Rodney-Marlborough Hotel Manager
* Jean Rouverol as Laura Hampton
* Clare Verdera as Viscountess River-Clyde
* Edward Gargan as Longshoreman at dance
* Pepito  as Poochy the Accordion Player

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 