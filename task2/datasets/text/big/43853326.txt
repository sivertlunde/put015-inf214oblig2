The Great Gildersleeve (film)
{{Infobox film
| name           = The Great Gildersleeve 
| image          = 
| alt            =
| caption        =  Gordon Douglas William Dorfman (assistant)
| producer       = Herman Schlom
| writer         = Jack Townley Julien Josephson
| starring       = Harold Peary Jane Darwell
| music          = C. Bakaleinikoff
| cinematography = Frank Redman
| editing        = John Lockert RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 Gordon Douglas. radio series created by Leonard L. Levinson, which ran from 1941 to 1950, this was the first of four films in the Gildersleeves series produced and distributed by RKO Radio Pictures. The screenplay was written by Jack Townley and Julien Josephson, and the film stars Harold Peary and Jane Darwell.

==Cast==
* Harold Peary as Throckmorton P. Gildersleeve
* Jane Darwell as Aunt Emma Forrester
* Nancy Gates as Marjorie Forrester
* Charles Arnt as Judge Horace Hooker
* Freddie Mercer as Leroy Forrester
* Thurston Hall as Governor John Stafford
* Lillian Randolph as Birdie Lee Calkins
* Mary Field as Amelia Hooker
* George Carleton as Frank Powers

==References==
 

 

 
 
 
 
 
 
 
 
 

 