Beauty and the Barge (1937 film)
{{Infobox film
| name = Beauty and the Barge Henry Edwards
| producer = Julius Hagen
| based on =  
| starring = Gordon Harker Judy Gunn Jack Hawkins George Carney
| music = W.L. Trytel 
| editing = Michael C. Chorlton
| cinematography = Sydney Blythe   William Luff
| studio = Twickenham Studios
| distributor = Associated British 
| released =  
| runtime = 77 minutes
| country = United Kingdom
| language = English
}}
 Henry Edwards Beauty and the Barge by W. W. Jacobs.

==Cast==
* Gordon Harker - Captain Barley
* Judy Gunn - Ethel Smedley
* Jack Hawkins - Lieutenant Seton Boyne
* George Carney - Tom Codd
* Margaret Rutherford - Mrs Baldwin
* Ronald Shiner - Augustus
* Michael Shepley - Hebert Manners
* Margaret Yarde - Mrs Porton
* Sebastian Smith - Major Smedley
* Margaret Scudamore - Mrs Smedley
* Ann Wemyss - Lucy Dallas

==References==
 

==Bibliography==
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 