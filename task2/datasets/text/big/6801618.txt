The Planet (film)
 
{{Infobox Film
| name           = The Planet
| image          = The Planet 2006 film.jpg
| image_size     = 
| caption        = There is only one planet and it has its limits
| director       = Johan Söderberg, Michael Stenberg, Linus Torell
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = David Österberg, Johan Söderberg
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2006
| runtime        = 82 minutes
| country        = Sweden, Norway, Denmark
| language       =English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Swedish documentary film on environmental issues, released in 2006. The film was made by Michael Stenberg, Johan Söderberg and Linus Torell for the big screen and was shot in the English language to reach an international audience. It includes interviews with 29 environmental scientists and experts including Dr. Stephen Peake,  Herman Daly, Lester Brown, Gretchen Daily, Mathis Wackernagel, Norman Myers, Jill Jäger, George Monbiot, Robert Costanza, Will Steffen, and Jared Diamond.

At 8pm GMT on 21 March 2007, as part of the OXDOX:MK documentary film festival, it became the first ever simultaneous round the world screening of a film. After the screening, a panel of leading environmental experts answered questions from around the world from the Berrill Lecture Theatre at The Open University, England.

==TV adaptation==
The TV adaptation consists of four episodes of 50–60 minutes:
* Part 1: The Earth System
* Part 2: Natures Resources
* Part 3: Humankind and Nature
* Part 4: Choices and Consequences

== Awards ==
*Winner, Best Documentary – Italian Environmental Film Festival, 2007

==External links==
*  at Sveriges Television|svt.se - includes the TV adaptation available for viewing (with Swedish subtitles)
* 
* 

 
 
 
 
 
 


 
 