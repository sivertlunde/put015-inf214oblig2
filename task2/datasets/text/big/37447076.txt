Kaashh
{{Infobox film
| title          = Kaashh
| image          = 
| caption        = 
| director       = Sujith-Sajith
| producer       = Sunil
| writer         = Sujith-Sajith Vamanapuram Mani
| starring       = Rajeev Pillai Bineesh Kodiyeri Vineeth Kumar Bazil
| music          = Sandeep Pillai
| cinematography = S. B. Prijith
| editing        = Vivek Harshan
| studio         = Oh My God Cinemas
| distributor    = Shenoy Cinemax
| runtime        = 
| released       =  
| language       = Malayalam
| gross          = 
| country        = India
}}
Kaashh is a 2012 Malayalam crime comedy film written and directed by debutant filmmakers Sujith and Sajith. The film is about the abduction of an industrialists daughter by a team of four friends.

==Cast==
* Rajeev Pillai as Sarath 
* Bineesh Kodiyeri
* Vineeth Kumar Innocent as Devasiya
* Mamukkoya as Manager
* Geetha Vijayan
* Lakshmipriya
* Suraj Venjarammoodu as a trickster
* Dharmajan as the pizza boy
* Jayan as Jayasankar
* Tini Tom as Karunan
* Chembil Asokan as the moneylender
* Leenna Panchaal as Jayasankars daughter

==Critical reception==
The film received mixed to negative reviews upon release. Veeyen of Nowrunning.com rated the film   and said, "Not all novel ideas have a desirable impact on the viewer, as Kaashh proves beyond doubt. This could have been a fun ride perhaps, but as it is, the entertainment value that the film offers is abysmally low."  Paresh C. Palicha of Rediff.com rated the film   and stated, "Kaashh has some glaring gaps in the narrative" and is "an average film despite having a young team of actors who have done their best." 
Review: Kaashh is just an above average fare if you want to relax 2 hours

==References==
 

 
 
 
 
 


 
 