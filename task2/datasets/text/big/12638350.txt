Luxury Car (film)
{{Infobox film
| name           = Luxury Car
| image          = Luxury Car poster.jpg
| image_size     = 
| caption        = French one-sheet Wang Chao
| producer       = Sylvain Bursztejn Mao Yonghong Zhou Weisi
| writer         = Wang Chao Tian Yuan Huang He
| music          = Xiao He
| cinematography = Liu Yonghong
| editing        = Tao Wen
| distributor    = France: Celluloid Dreams 
| released       = 2006 Cannes Film Festival|Cannes: May 22, 2006 
| runtime        = 88 minutes
| country        = China/France
| language       = Mandarin
| budget         = 
}} 2006 film directed by Wang Chao. The film was produced by Chinas Bai Bu Ting Media and Frances Rosem Films, and in association with Arte France Cinéma.

Luxury Car premiered on May 22, 2006 at the 2006 Cannes Film Festival, where it won the side competition for the Prix un certain regard.    

== Plot ==
The film follows an old man from the countryside who goes to Wuhan to search for his missing son, who his dying wife has requested to see one last time. In Wuhan, he meets his daughter, a karaoke bar escort who introduces him to an old police officer who offers to help. It soon becomes apparent to both the father and the policeman that the mobsters running the daughters karaoke bar and the sons disappearance are linked.

== Cast == Tian Yuan, as Li Yanhong, a young woman from the countryside who has moved to the city in search of a better life. She finds herself, however, working as a prostitute in a karaoke bar instead.
*Wu Youcai, as Li Qiming, Li Yanhongs father, an elderly schoolteacher who has come to Wuhan in search of his son.
*Li Yiqing, as a policeman, an aging detective who has agreed to help Li Yanhong in his search Huang He, as He Ge the owner of the Karaoke bar and Li Yanhongs "boyfriend" Li Li, as A Li, another prostitute and Li Yanhongs roommate.

== Production history ==
=== Story === Day and Night (2004). The concept for the film and its focus on parents who have become disconnected from their children, came about, according to Wang, after he discovered that his mother had contracted cancer.  Wang has also stated what his socially conscious intentions were behind the film:
 

=== Location ===
The film was shot and takes place almost entirely within the city of Wuhan in central Chinas Hubei province. Director Wang Chau noted that Wuhan represents to him, a "very typical Chinese city," in particular its mix of urban and rural, modern and traditional.  

The main cast in addition, were all locals from the city, including the actors playing the father, the police officer and the daughter. 

== Reception == Tian Yuan in the role of the daughter, but overall found the film "predictable and narratively uninspired." 

On the other hand, Variety (magazine)|Varietys Derek Elley showered only praise on the film, emphasizing the performances of Tian and Wu, and noting that the film is "tightly written and beautifully played."   

== References==
 

== External links ==
* 
* 
* 
*  at the Chinese Movie Database

 
 

 
 
 
 
 
 
 
 