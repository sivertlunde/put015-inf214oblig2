By Touch
{{Infobox film
| name           = By Touch
| image          = PrzezDotyk.jpg
| image_size     = 
| caption        = VHS cover
| director       = Magdalena Lazarkiewicz
| producer       = 
| writer         = Magdalena Lazarkiewicz Ilona Lepkowska
| narrator       = 
| starring       = 
| music          = Zbigniew Preisner
| cinematography = Krzysztof Pakulski
| editing        = Ewa Pakulska
| distributor    = 
| released       = September 24, 1986 (Poland)
| runtime        = 79 mins.
| country        = Poland Polish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1986 Poland|Polish film directed by Magdalena Lazarkiewicz and written by Lazarkiewicz and Ilona Lepkowska. It was released in Poland on September 24, 1986.

==Cast==
*Barbara Chojecka
*Tadeusz Chudecki as Priest
*Maria Ciunelis as Teresa Jankowska
*Zuzanna Helska
*Wieslaw Kowalczyk
*Izabela Laskowska
*Antoni Lazarkiewicz as Tomek
*Milogost Reczek as Doctor
*Teresa Sawicka as Doctor
*Krzysztof Stelmaszyk as Adam, Annas husband
*Grazyna Szapolowska as Anna
*Irena Szymkiewicz
*Jerzy Trela as Professor
*Wanda Weslaw-Idzinska
*Maria Zbyszewska

==Awards==
In 1986, By Touch won the Grand Prix at the Créteil International Womens Film Festival and the award for Best Cinematography at the Polish Film Festival. {{cite web
  | title =Awards for Przez dotyk
  | publisher =Internet Movie Database
  | url =http://www.imdb.com/title/tt0089848/awards
  | accessdate =2007-11-08  }} 

==References==
 

==External links==
* 
*  

 
 
 
 

 
 