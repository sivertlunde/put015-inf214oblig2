Caiçara (film)
 
{{Infobox film
| name           = Caiçara
| image          = Caiçara film.jpg
| caption        = Theatrical release poster Tom Payne John Waterhouse
| producer       = Alberto Cavalcanti
| writer         = Alberto Cavalcanti Adolfo Celi Ruggero Jacobbi
| starring       = Eliane Lage Abílio Pereira de Almeida Carlos Vergueiro Mário Sérgio (actor)|Mário Sérgio
| music          = Francisco Mignone
| cinematography = H. E. Fowle
| editing        = Oswald Hafenrichter
| studio         = Vera Cruz
| distributor    = Universal Filmes Marte Filmes
| released       =  
| runtime        = 92 minutes
| country        = Brazil 
| language       = Portuguese
}}
 Tom Payne, John Waterhouse. 

==Cast==
* Eliane Lage as Marina
* Abilio Pereira de Almeida as José Amaro
* Carlos Vergueiro as Manuel
* Mário Sérgio (actor)|Mário Sérgio as Alberto
* Maria Joaquina da Rocha as Felicidade
* Adolfo Celi as Genovês
* Vera Sampaio
* Célia Biar
* Renato Consorte
* Luiz Calderaro
* José Mauro de Vasconcelos
* Zilda Barbosa as Diretora do orfanato

==Production==
It was shot between March and September 1950 in Ilhabela, São Paulo. 

==Reception==
It was nominated for the Grand Prize of the Festival at the 1951 Cannes Film Festival.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 