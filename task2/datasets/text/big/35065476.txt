Bheema Theeradalli
{{Infobox film
| name           = bheema theeradalli
| image          =
| caption        = Theatrical release poster
| director       = Om Prakash Rao
| producer       = Anaji Nagaraj Pranitha
| music          = Abhiman Roy
| cinematography  = Anaji Nagaraj
| released        =    
| runtime         =
| language        = Kannada
| country         = India
| budget          =   8 crore
}}
 action film directed by Om Prakash Rao and produced-shot by cinematographer Anaji Nagaraj. The film stars Duniya Vijay and Pranitha Subhash in the lead roles. Controversial Excise minister in the State Cabinet of Karnataka Renukacharya makes his acting debut in a special appearance role.   

Composer Abhiman Roy has composed the score and soundtrack. Duniya Vijay plays a  role who is a dalit warrior(chandappa harijan)  in this film. The film released across the cinema halls on 6 April 2012.  The story is based on the life and times of dreaded dacoit Chandappa Harijana. 

==Production==
The films shooting began on the auspicious day of Ugadi in 2011.  It is based on the character ‘Chandya’,one of the members of a dreaded gang who adds a human touch to his work.

The plot is based on real life incidents.Actor Vijay of Duniya fame was roped in with full swing for the action role.Pranitha, Prajwal Bopaiah, Doddanna, Umasri, Shobaraj are others who make up the cast.Anaji Nagaraj,a reputed cameraman and producer in the Kannada film industry was happy with the film making news before the release.The film has been marketed by Prasad of "Sammarth Ventures".

==Cast==
* Duniya Vijay Pranitha
* Doddanna
* Sharath Lohitashwa
* Prajwal Bopaiah
* Umashree
* Shobaraj
* Renukacharya in a special appearance
* Suchhendra Prasad

==Title Controversy==
Chandappa was entangled in a controversy due to its previous title Bheema Theeradalli. The films title apparently enraged the Korma community members in Karnataka. The community members felt that the film-maker has criticized their community. Thus Korma community members had complained against the director Om Prakash Rao. The community members have asked for an apology from the director. However the director dismissed all allegations terming that the film has nothing to do with the community as such. Later,the title was changed to Chandappa due to the protests the previous name generated. 

==Accolades==

{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards SIIMA Award Best Actor Duniya Vijay
| 
|- SIIMA Award Best Actress Pranitha Subhash
| 
|- SIIMA Award Best Actress in a Supporting Role Umashree
| 
|- SIIMA Award Best Actor in a Negative Role Sharath Lohitashwa
| 
|-
|}

==References==
 

 
 
 
 