The Rage of Paris
{{Infobox film
| name           = The Rage of Paris
| image          = The Rage of Paris Poster.jpg
| image_size     =
| caption        =
| director       = Henry Koster
| producer       = Buddy G. DeSylva  Henry Koster (uncredited) 
| writer         = Bruce Manning   Felix Jackson
| story          = Bruce Manning   Felix Jackson
| narrator       =
| starring       = Frank Skinner Charles Henderson
| cinematography = Joseph A. Valentine
| editing        = Bernard W. Burton
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 78 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

The Rage of Paris is a 1938 American comedy film made by Universal Pictures. The movie was directed by Henry Koster, and written by Bruce Manning and Felix Jackson. It won the Venice Film Festival for Special Recommendation.

==Cast==
*Danielle Darrieux as  Nicole de Cortillon 
*Douglas Fairbanks Jr. as  James Jim Trevor 
*Mischa Auer as  Mike Lebedovich 
*Louis Hayward as  Bill (Jerome) Duncan 
*Helen Broderick as  Gloria Patterson  Charles Coleman as  Wrigley Trevors Butler 
*Samuel S. Hinds as  Mr. Duncan 
*Nella Walker as  Mrs. Duncan  Harry Davenport as  Pops, Caretaker

==External links==
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 


 