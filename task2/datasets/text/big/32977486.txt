Raaz 3D
 
 
{{Infobox film
| name           = Raaz 3
| image          = Raaz_3d_movie_poster.jpg
| caption        = Theatrical release poster
| director       = Vikram Bhatt
| producer       = Mahesh Bhatt  (Presenter)  Mukesh Bhatt
| cinematography = Pravin Bhatt Stereographer Michael Flax
| writer         = Shagufta Rafique
| narrator       = Emraan Hashmi
| editing         = Kuldip K. Mehan Rashid Khan Background Score Raju Khan
| starring       = Bipasha Basu Emraan Hashmi Esha Gupta
| studio         = Vishesh Films
| distributor    = Fox Star Studios
| released       =   
| runtime        =
| country        = India
| language       = Hindi
| budget         =   
| gross          =  (worldwide)}}

Raaz 3 (also known as Raaz 3: The Third Dimension) is a  , which itself was a sequel to Raaz which starred Bipasha Basu and  ,  , Murder 3 and Aashiqui 2, each of which had nothing to do with their respective prequels, but somehow fell into the same genre following a similar story.

The film released on 7 September 2012 with paid previews on 6 September 2012 to mixed reception while Basus role was highly praised by critics. The film garnered overwhelming box office collections and was declared a "Super Hit" at the box office.    It emerged as a major commercial success in India and is the highest grossing film in the Raaz trilogy as well as the highest grossing horror film ever. The film is also the first 3D super hit of India and the second highest grossing film of Vishesh Films.It was also called a 3D Blockbuster in the Indian media. The film is also currently the highest grossing film of Emraan Hashmi and Esha Gupta, beating Murder 2 and Jannat 2, respectively.

==Plot==
The story revolves around Shanaya (Bipasha Basu), an actress at the height of her success. She gets offered the best roles and wins all the awards she is nominated for. In addition, she also has a passionate affair with a handsome director named Aditya (Emraan Hashmi). This all ends when a younger actress, Sanjana (Esha Gupta) makes her film debut. In place of Shanaya, Sanjana comes into spotlight and begins to snatch away her awards. Suddenly, Shanaya seems forgotten and everybody only wants to work with Sanjana. Soon enough, Shanaya sees her career fading away and her envy slowly becomes craziness. She begins to turn to black magic and makes it her goal in life to destroy Sanjanas career and make her the pain of loss.

She seeks help of her uncle sonu (Sunil Dhariwal), a tantrik who helps her to practice black magic and calls upon an evil entity, Tara Dutt (Manish Chowdhary). She asks him to make Sanjanas life a living hell. Tara agrees to help haunt and torture Sanjana until she becomes suicidal. He asks Shanaya to give her a black poison through a person she trusts. Shanaya seduces Aditya to do it for her and whenever he feels guilty, she makes him accept again. Aditya, whose sympathy for Sanjana has turned into love, leaves Shanaya and refuses to listen her any more. Now Shanaya must work on her own to defeat the power of love.

The film delivers the message of – "Some people forget everything and run behind mere fame that gets destroyed by time. They do not know that the only thing that remains is the love that we have not just for ourselves, but also for our co-beings. That when we strive, not for ourselves, but for others, that Raaz (secret) is referred to as Life."

==Cast==
* Bipasha Basu as Shanaya Shekhar
* Emraan Hashmi as Aditya Arora
* Esha Gupta as Sanjana Krishna
* Manish Chowdhary as Tara Dutt (Ghost)
* Sunil Dhariwal as Sonu
* Mohan Kapoor as Doctor
* Jagat Rawat as Tantrik

==Production==

===Casting===
The original choice for the female lead of the film was Jacqueline Fernandez, but she had later dropped out due to not wanting to establish herself as a Bollywood sex symbol, as she was uncomfortable with a scene where cockroaches attack her and she had to be topless in front of 300 people. Fernandez was later replaced with Jannat 2 debutant, Esha Gupta. Bipasha Basu(main female lead and antagonist) will be playing the role of an actress who is a "prisoner of fame". Actor Dino Morea and actress Kangna Ranaut, who were a part of Raaz (2002 film)|Raaz and Raaz - The Mystery Continues respectively are not a part of the film.Emraan Hashmi will be playing the male lead.

===Filming===
The film began shooting on 29 January 2012, director Vikram Bhatt explained how confident he is that the film should be completed sometime in Summer 2012 and that Raaz 3 is amongst his most important films.

==Promotion==

===Trailer launch===
The Raaz 3 trailer was originally scheduled to be attached with the prints of Rohit Shettys Bol Bachchan on 6 July 2012. However, it was delayed multiple times due to censoring. The theatrical trailer was officially launched on 30 July 2012 for the media, as confirmed by actress Bipasha Basu. Despite being widely speculated that it would be released to the public also, this was proven false. Later, the theatrical trailer was unveiled for the public as it was released online a day later on 31 July 2012. The theatrical trailer will also be in theatres on 3 August 2012 with Pooja Bhatts Jism 2, as they are both under the Vishesh Films banner.

===Marketing===
Raaz 3 was promoted on the Sony TV show C.I.D. (India TV series)|C.I.D. Bipasha Basu and Esha Gupta promoted the film by making an appearance on a special episode of CID, which aired on 2 September 2012. 

Although, the film was originally planned to release on 31 August 2012 alongside Barfi! and Joker (2012 film)|Joker, it was later delayed to avoid clashing with any other big film. Raaz 3 was banned for release in United Arab Emirates|UAE, although is being kept under review for the adult content included in the film.  

==Soundtrack==
{{Infobox album
| Name        = Raaz 3D
| Type        = Soundtrack Rashid Khan
| Cover       =
| Released    = 4 August 2012 Feature film Indian conventional Gujarati folk, punjabi folk, qawwali
| Length      =
| Recorded    = 2011–2012
| Label       =
| Producer    = Jeet Ganguly, Rashid Khan Hindi
| Last album  =
| This album  =
| Next album  =
}}
Films music is composed by Jeet Gangulli & Rashid Khan and lyrics are penned by Rashid Khan, Sanjay Masomm, Kumaar.

==Tracklist== Rashid Khan.
 
{| class="wikitable"
|-
! Track No !! Song !! Singer !! Music Composer
|-
| 1 || Deewana Kar Raha Hai || Javed Ali || Rashid Khan
|-
| 2 || Zindagi Se || Shafqat Amanat Ali Khan || Jeet Ganguly
|- KK || Jeet Ganguly
|-
| 4 || Oh My Love || Sonu Nigam & Shreya Ghoshal || Jeet Ganguly
|-
| 5 || Kya Raaz Hai || Zubeen Garg & Shreya Ghoshal || Jeet Ganguly
|-
| 6 || Khayalon Mein || Shreya Ghoshal || Jeet Ganguly
|}

==Release==
Raaz 3D released in India and overseas on 7 September 2012. Raaz 3D is Indias second-ever 3D horror movie. The movie releaseed in stereoscopic 3D, Imax 3D and 3D worldwide. There are more 3D prints of the movie than its 2D version. Raaz 3D is the biggest release for an Emraan Hashmi film as it released on over 2000 screens in India, including in its 3D format.  This movie is Emraan Hashmis first ever 3D movie. The movie received an Adult Certificate from the Censor Board of India without any cuts. 

===Critical reception===
The movie received mixed reviews from critics. Critics pointed out that the only saving grace of the movie was Bipasha.
Taran Adarsh gave it 3.5/5 stars, commenting "If you are an enthusiast of supernatural thrillers/horror movies, RAAZ 3 should be on your list of things to do and watch this weekend. Go, get ready to be spooked!" Bollywood3 awarded it 3.25/5 stars and wrote," On the whole, Raaz 3 is one movie which has everything going its way. Gritty screenplay, awesome songs and mature performances are sure shot plus points. On the other hand it would be interesting to see how family audience, who love watching popcorn entertainment respond to this film. To those who love edge of the seat horror thrillers, Raaz 3 is one film which is highly recommended. Go buy yourself a ticket and experience the thrill in 3D."  Madhureeta Mukherjee of The Times Of India gave it 3/5 stars while writing, "For all those who want to move over from the Ramsay Bros... go watch Raaz 3 in 3D, at your own risk. But dont take it to your grave." Ananya Bhattacharya of Zee News gave it 3/5 stars and stated that"Watch Raaz 3 for Bipasha and its 3D factor."Independent Bollywood gave 3 out of 5 stars and quoted "Some real original scares and chills this time with memorable performances."  IBNLive also gave it 3/5 stars calling it a good horror film.

Subhash K. Jha of IANS gave 2 out of 5 stars saying, "Its Bipasha who holds together the feverish proceedings. She delivers a full-bodied gutsy performance."  Udita Jhunjhunwala of businessofcinema.com said, "Bhatt gets the rhythm of the suspense-building wrong, going all too rapidly from grotesque decapitation to a kiss and song scene. It does not help that the acting is also at different levels. Most of the burden to convince rests on Basus fit shoulders – and she does. You do believe that she is menacing and wicked."
 

==Box office==

===India=== 2012 for a Bollywood release.  Raaz 3 had a huge first week where it had collected  .    Raaz 3 collected   nett in its second weekend taking its ten-day collection to   nett.    It had collected   in its second week and   in its third week to make a total of   domestically.     The film ultimately grossed   domestically  and   worldwide.  Box Office India declared the film as a Super Hit. 

===Overseas===
Raaz 3 did not do as well overseas, as it approximately collected around US$1 million ( 55&nbsp;million) plus in ten days. The film was not released in the UAE, which has been noted to have hit it hard, as it could have been the films best International market and added another $250,000 plus in revenue. Raaz 3 was declared average by box office India.     Hossing However,it became the highest grossing Emraan Hashmi film in the overseas.

==Sequel==
A fourth film in the series, titled  , is currently in pre-production. The film is supposed to be an official remake of the American film The Omen.   

==References==
 

==External links==
*  
 

 

 
 
 
 
 
 
 
 