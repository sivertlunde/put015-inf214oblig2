Piccadilly Third Stop
{{Infobox film
| name           = Piccadilly Third Stop
| image          = "Piccadilly_Third_Stop"_(1960).jpg
| caption        = Original British quad poster
| director       = Wolf Rilla
| producer       = Norman Williams
| writer         =  Leigh Vance (and story) John Crawford Mai Zetterling Philip Green
| cinematography = Ernest Steward
| editing        = Bernard Gribble
| studio         = Ethiro-Alliance Sydney Box| Sydney Box Associates
| distributor    = J. Arthur Rank Film Distributors  (UK)
| released       = 6 September 1960	(London)  (UK)
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} 1960 Cinema British thriller John Crawford.  A wealthy playboy hires a gang of criminals to help him steal £100,000.

==Plot==
Crook Dominic Colpoys-Owen (Terence Morgan) has his eye on the loot inside an embassy in London, when foreign ambassadors daughter Seraphina (Yoko Tani) unwittingly reveals her father, away on business, has left big money behind in the safe. Colpoys-Owen works his smooth-talking charm on the innocent girl, who becomes so infatuated that she agrees to help his gang with their plan. This involves a robbery accessed from the London Underground on the embassy in Knightsbridge.

==Cast==
* Terence Morgan as Dominic Colpoys-Owen
* Yoko Tani as Seraphina Yokami John Crawford as Joe Pready
* Mai Zetterling as Christine Pready
* William Hartnell as Colonel
* Dennis Price as Edward
* Ann Lynn as Mouse
* Charles Kay as Toddy
* Doug Robinson as Albert
* Gillian Maude as Brides Mother
* Trevor Reid as Brides Father
* Ronald Leigh-Hunt as Police Sergeant
* Tony Hawes as Harry Prentice
* Clement Freud as Chemmy dealer
* Judy Huxtable as Angela Vaughan

==Critical reception==
Allmovie called it a "fast-paced, standard crime story" ;  while the Radio Times called it a "plodding low-budget thriller."  

==External links==
* 

==References==
 

 
 
 
 
 
 


 
 