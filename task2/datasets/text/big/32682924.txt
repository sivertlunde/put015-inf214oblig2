Greencastle (film)
{{Infobox film
| name           = Greencastle
| image          = Greencastle the Film Poster.jpg
| alt            = Greencastle the Film Poster
| caption        = Theatrical release poster
| director       = Koran Dunbar
| producer       = Hans Scharler
| screenplay     = Koran Dunbar
| story          = Koran Dunbar Hans Scharler
| starring       = Koran Dunbar Doua Moua Nikki Estridge Aurelius Dunbar Quevaughn Bryant Jeannie Malone Christopher James Raynor
| music          = John Cimino Jonathan Austin Jonathan Austin
| studio         = Rages 2 Riches Triahn Entertainment
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Greencastle is an American drama film directed by, written by, and starring Koran Dunbar. The story follows Poitier Dunning, a single father who works as an Assistant Manager at a small town pet shop, as he enters a "quarter-life crisis" impelled by a recent tragedy. Greencastle intertwines lives of loneliness and disconnection, fatefully leading Poitier toward an unexpected and sublime awakening.

==Cast==
*Koran Dunbar as Poitier Dunning
*Aurelius Dunbar as Julian Dunning
*Nikki Estridge as Leslie Davis
*Quevaughn Bryant as Poitiers Father Ralph Mauriello as Rocco Mazzagatti
*Doua Moua as Nu Vang
*Christopher James Raynor as Rick
*Hans Scharler as Roy Baker, District Dean

==Production== Maryland Theater in Hagerstown, MD on March 31, 2012. Jonathan Austin is the cinematographer for the film.

==References==
 

==External links==
*  

 
 
 

 