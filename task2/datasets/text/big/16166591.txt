Azhagana Naatkal
{{Infobox film
| name           = Azhagana Naatkal
| image          =
| caption        =
| director       = Sundar C.
| producer       = S. T. Selvan
| writer         = Boopathy Pandian  (dialogues) 
| screenplay     = Sundar C.
| story          = Priyadarshan (story base) Cheriyan Kalpakavadi (uncredited) Karthik Rambha Rambha Mumtaj
| producer       = S. D. Selvan
| cinematography = Prasad Murella Deva
| editing         = P. Sai Suresh
| released       =  
| runtime        = 145 minutes
| language       = Tamil
| country        = India
}}
 Karthik and Rambha in Senthil in pivotal roles and featured music composed by Deva (music director)|Deva. The film released on 7 December 2001 to average reviews from critics. The film is a remake of the Malayalam film Minnaram.  

==Cast== Karthik as Chandru Rambha as Indhu
*Mumtaj as Rekha
*Goundamani as Ranjith
* Jothi Meena as Sheela Senthil
*Pandiarajan
*Manivannan
*Delhi Ganesh Rajeev
*Ponnambalam Ponnambalam

==Plot==
Indu and Chandru (Ramba and Karthik) fall in love. Indu does the disappearing act and Chandru gets engaged to Rekha (Mumtaz). Indu re-appears after three years with a little girl, who she claims is Chandrus daughter. Chandru vehemently denies it and finding Indus presence an embarrassment tries to ferret out the truth. After many scenes of forced comedy, we learn that the girl is the illegitimate daughter of Chandrus much-married brother, who had seduced Indus sister. After his brothers futile attempt to play the villain, Chandru latches on to Indu fast enough. And with no apology to Rekha. But it is not really Chandrus fault. For Rekha had done the disappearing act from the scene early enough without any explanation either.

==Production== Simran had originally signed on to play the lead role in the film, before she was replaced by Rambha. This is a tamil remake of 1994 Malayalam movie Minnaram. 

==References==
 

==External links==
* 

 

 
 
 
 
 


 