KT (film)
{{Infobox film name           = KT image          =  director       = Junji Sakamoto producer       = Lee Bong-su Yukiko Shii writer         = Haruhiko Arai Eisuke Nakazono starring       = Kōichi Satō Kim Kap-soo Choi Il-hwa  music          = Park Ji-woong cinematography = Norimichi Kasamatsu  editing        = Kim Hyeong-ju distributor    =  released       =   runtime        = 130 minutes country        = South Korea language       = Korean Japanese budget         =  gross          = 
}}
KT is a 2002 Japanese-South Korean film directed by Junji Sakamoto with a screenplay by Haruhiko Arai. It is based on the kidnapping of Kim Dae-jung by agents of Park Chung-hee in August 1973 while on a trip to Tokyo. He was released in Seoul after five days.

==Plot==
The story is centered on Masuo Tomita, a Japanese intelligence officer who helped arrange for South Korean agents to kidnap and try to kill Park Chung-hees enemy Kim Dae-jung, who was in exile in Tokyo. Tomita went along with the plan to save a South Korean teacher Lee Jeong-mi, whom he loved.

==Cast==
* Kōichi Satō ... Masuo Tomita
* Kim Kap-soo ... Kim Chang-won
* Choi Il-hwa ... Kim Dae-jung
* Yoshio Harada ... Akikazu Kamikawa
* Michitaka Tsutsui ... Kim Kap-soo
* Yang Eun-yong ... Lee Jeong-mi
* Kim Byeong-se ... Kim Jung-won
* Teruyuki Kagawa ...Haruo Satake
* Akira Emoto ... Hiroshi Uchiyama
* Kyoko Enami ... Kab-Soos Mother
* Noboru Hamada ... Minister
* Masahiro Komoto
* Akaji Maro ... Susumu Kawahara
* Ken Mitsuishi ...Yu Chun-seong
* Ryūshi Mizukami
* Nana Nakamoto ... Toshiko Takashima
* Hirochi Oguchi ... Shoichi Tsukada
* Gō Rijū ... Hong Seong-jin
* Kenji Sahara... Minister
* Ken Utsui
* Hōka Kinoshita
* Akira Hamada

==External links==
*  
*  

 

 
 
 
 
 
 


 
 