Private Romeo
{{Infobox film
| name = Private Romeo
| image =  Private-romeo-film-by-alan-brown.jpg
| caption = Theatrical release poster Alan Brown
| producer = Agathe David-Weill, Kevin Ginty and Alan Brown  (producers)    Alli Maxwell  (co-producer)   Matt Janes, Glen Trotiner  (associate producers)  Alan Brown (film writer) 
| based on   =    Matt Doyle
| music = Nicholas Wright
| cinematography = Derek McKane
| editing = 
| studio  = 
| distributor = 
| released =  
| runtime = 
| country = United States
| language = English
| budget =  
| gross = 
}} Alan Brown gay love Matt Doyle. The film was Browns take on Dont ask, dont tell, the official United States policy on gays serving in the military from December 21, 1993, to September 20, 2011 (the law was repealed after the production of the film).

The scenes of the cadet school and the lessons of the play "Romeo and Juliet" are filmed in desaturated colors, gray, khaki, and pale, whereas the scenes depicting the actual Shakespearean scenes are shown in saturated colors.

The film project was previously known as "McKinley"  and as "The Shakespeare Project".

Brown was inspired to write the script after seeing Joe Calarcos play Shakespeares R&J in 2008 wherein four senior students in blazers, discover Shakespeares Romeo and Juliet and become intrigued by it while reciting excerpts, and act out key scenes from the play intermingled with their own personal realities.  All four play various roles in a minimalist stage setting.

==Cast==
The cast was made up of 8 actors playing 11 characters. Many other characters from the original play were left out. 
*Seth Numrich as Sam Singleton (Romeo) Matt Doyle as Glenn Mangan (Juliet)
*Hale Appleman as Josh Neff (double role Mercutio and Capulet) Charlie Barnett as Ken Lee (Prince Escalus)
*Adam Barrie as Adam Hersh (Father Laurence)
*Chris Bresky as Omar Madsen (Nurse)
*Sean Hudock as Gus Sanchez (double role Capulets Wife and Benvolio)
*Bobby Moreno as Carlos Moreno (double role of Capulet and Tybalt)

==Reception==
The film was singled out by critics for being "hugely adventurous and highly liberated".  It was a New York Times Critics Pick. 

==Awards==
In 2011, the film won the Grand Jury Prize for "Outstanding Actor in a Feature Film" The prize was co-won by Seth Numrich, Chris Bresky, Matt Doyle, Hale Appleman, Sean Hudock, Adam Barrie, Bobby Moreno, Charlie Barnett at Outfest, the Los Angeles Gay and Lesbian Film Festival. The award credited the actors for "bringing fresh life to a timeless love story and infusing each moment with a 21st century immediacy that balances naked passion with longing and delivering it all with brilliant coherence".

==References==
 

==External links==
*  
*  

 

 
 
 
 
 