It's a Wise Child (film)
{{Infobox film
| name           = Its a Wise Child
| image          = 
| alt            = 
| caption        =
| director       = Robert Z. Leonard
| producer       = Marion Davies Robert Z. Leonard
| screenplay     = Laurence E. Johnson 	
| starring       = Marion Davies Sidney Blackmer James Gleason Polly Moran Lester Vail
| music          = 
| cinematography = Oliver T. Marsh 
| editing        = Margaret Booth
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Its a Wise Child is a 1931 American comedy film directed by Robert Z. Leonard and written by Laurence E. Johnson. The film stars Marion Davies, Sidney Blackmer, James Gleason, Polly Moran and Lester Vail. The film was released on April 11, 1931, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==	 
*Marion Davies as Joyce Stanton
*Sidney Blackmer as Steve
*James Gleason as Cool Kelly
*Polly Moran as Bertha
*Lester Vail as Roger Baldwin
*Marie Prevost as Annie Ostrom
*Clara Blandick as Mrs. Stanton
*Robert McWade as G. A. Appleby
*Johnny Arthur as Otho Peabody
*Hilda Vaughn as Alice Peabody Ben Alexander as Bill Stanton
*Emily Fitzroy as Jane Appleby 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 