The Kid Rides Again
{{Infobox film
| name           = The Kid Rides Again
| image          = The Kid Rides Again.jpg
| image_size     = 190px
| caption        =
| director       = Sam Newfield
| producer       = Sigmund Neufeld (producer)
| writer         = Fred Myton (story and screenplay)
| narrator       =
| starring       = See below
| music          = Leo Erdody
| cinematography = Jack Greenhalgh
| editing        = Holbrook N. Todd
| distributor    =
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Kid Rides Again is a 1943 American film directed by Sam Newfield.

== Cast ==
*Buster Crabbe as Billy the Kid
*Al St. John as Fuzzy Q. Jones
*Iris Meredith as Joan Ainsley
*Glenn Strange as Henchman Tom Slade Charles King as Vic Landeau, Henchman
*I. Stanford Jolley as Mort Slade
*Edward Peil Sr. as John Ainsley Ted Adams as Sundown Sheriff
*Slim Whitaker as Texas Sheriff

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 