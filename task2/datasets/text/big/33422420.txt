Miss Stone (film)
 

{{Infobox film
| name = Miss Stone
| image =
| caption = Poster
| director = Živorad Mitroviḱ
| producer =
| screenplay =  Gorgi Abadjiev, Popov Trajče
| story =
| based on =
| starring = Nikola Avtovski, Damevski Darko, Dragi Kostovski, Vladimir Medar, Milchin Elias, Todor Nikolovski, Ocokoljic Dragan, Prlichko Petre
| music = Ivan Rupnik
| cinematography =
| editing = Vojislav Bjenjaš
| studio = Vardar Film
| distributor =
| released = 1958
| runtime = 97 minutes Macedonia
| Macedonian
| budget =
| gross =
| followed_by =
}} Macedonian 1958 1958 list historical film, directed by Žika Mitrović|Živorad Mitroviḱ and tells the story about the Miss Stone Affair.

==Synopsis==
The film is set in 1901, and is based on events surrounding the growing Macedonian resistance to the Ottoman Empire. Resistance groups known as "Komiti" show the readiness of the Macedonian people to fight for freedom. At the same time different religious missions are trying to expand their influence among the people. Thessaloniki is the center of the American Protestant mission in Macedonia. The Internal Macedonian Revolutionary Organization (ВМРО Внатрешна Македонска Револуционарна Организација) abducted U.S. Protestant missionary Miss Stone (Olga Spiridonovikj) led by Yane Sandanski (Ilija Milcin) and his unit. The group demanded 25 pounds of gold to release her. They wanted her to sign the demand. She flatly refuses, but after going through several battles with Turkish forces, she stands on the side of guerrillas and signs the letter. The gold is eventually provided.

== References ==
 
*  
*  

==External links==
*  

 
 
 
 


 