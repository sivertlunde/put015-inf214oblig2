D-Day -1
 
{{Infobox film
| name = D-Day -1
| image =
| caption =
| director =
| producer = Army Air Forces
| writer =
| narrator =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1945
| runtime = 17 minutes USA
| language = English
}}
D-Day - 1 was a short propaganda film produced shortly before the end of the Second World War to boost the 7th war bond drive. It focused on the experiences of American paratroopers and gliders who went into Normandy prior to the sea borne invasion.

Opening with a statement by Gen. Barney M. Giles, reminding the audience that the war cant be won without money to pay for armaments and equipment, the film turns to an overview of Operation Overlord and the necessity for breaking the Atlantic wall. However, the emphasis soon turns to the men stationed in England, and their life while training and waiting for the inevitable invasion. Finally, the big day arrives and paratroopers are flown in over northern France in advance of the actual invasion. Their tasks included blowing up bridges, securing airfields, cutting communication lines, and other clandestine operations.

The film ends by reminding the audience that many of the men they have just seen have given their lives in defense of freedom, and urges them to buy war bonds.

==See also==
*List of Allied propaganda films of World War II

==External links and reference==
*  

 
 


 