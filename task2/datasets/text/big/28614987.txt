My Dear Muthachan
{{Infobox film
| name           = My Dear Muthachan
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Sathyan Anthikad
| producer       = Joy Thomas Sreenivasan
| narrator       = 
| starring       = Thilakan Madhurima Jayaram Johnson
| cinematography = Vipin Mohan
| editing        = K. Rajagopal   
| studio         = 
| distributor    = 
| released       = 1992
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}  Sreenivasan and Master Tarun in lead roles, and Murali (Malayalam actor)|Murali, Sreenivasan, Innocent (actor)|Innocent, Urvashi (actress)|Urvashi, K.P.A.C. Lalitha, Jayaram and Philomina in major supporting roles.

==Plot==
Meera and her younger siblngs hires an ex-army officer posed as their grandfather to find out who is behind their parents deaths.

==Cast==
*Thilakan as  Parameswaran / Major K. K. Menon
* Madhurima(debut) as Meera Baby Jomol as Maya Tarun as Manu
*Baby Renju as Manju Murali as  Kuriachan, GM of the company
*Jayaram as  Parthasarathi Innocent as  Sub Inspector K. P. Adiyodi Sreenivasan as  Dinakaran / Babu Raj Janardhanan as  Adv. Ananthan Urvashi as  Clara
*K.P.A.C. Lalitha as  Shantha
*Mammukoya as  Punnoose
*Philomina as  Kunjamma
*Sukumari as  Parthasarathis Mother
*Oduvil Unnikrishnan as  Factory Worker
*Jose Pellissery as Lawyer at the courthouse

== Soundtrack ==
The films soundtrack contains 3 songs, all composed by Johnson and Lyrics by Bichu Thirumala.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Cheppadikkaaranalla"
| C. O. Anto, K. S. Chitra, Minmini, Jancy
|-
| 2
| "Raathri Than Kaikalil"
| K. S. Chitra, Chorus
|-
| 3
| "Randu Poovithal Chundil Virinju"
| K. J. Yesudas
|}

==External links==
*  

 
 
 
 
 
 


 