A Girl in Black
{{Infobox film
| name           = A Girl in Black
| image          = To Koritsi me ta Mavra.jpg
| image_size     = 
| caption        = The film poster
| director       = Michael Cacoyannis
| producer       = Hermes Film
| writer         = Mihalis Kakogiannis
| narrator       = 
| starring       = Ellie Lambeti Dimitris Horn Giorgos Foundas Eleni Zafeiriou Stephanos Stratigos Notis Peryalis Nikos Fermas Anestis Vlahos Thanasis Veggos
| music          = Arghyris Kounadis
| cinematography = Walter Lassally
| editing        = Emilios Proveleggios
| distributor    = 
| released       = 1956
| runtime        = 100 minutes
| country        = Greece
| language       = Greek
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Greek dramatic film by the Cypriot director Michael Cacoyannis starring Dimitris Horn and Ellie Lambeti. The film takes place on the Greek island of Hydra island|Hydra, where two Athenian visitors become entangled in local feuds after one of them falls in love with a local girl.

It was one of the first Greek films to achieve international recognition (Golden Globe award).

==Cast==
*Ellie Lambeti as Marina
*Dimitris Horn as Pavlos
*Giorgos Foundas as Hristos
*Eleni Zafiriou	as Froso
*Stephanos Stratigos as Panagis
*Notis Peryalis	as Antonis
*Anestis Vlahos	as Mitsos
*Thanassis Veggos as policeman

==Awards==
The film was among the six Best Foreign Language Film award winners of the 14th Golden Globe Awards.  It was also nominated for a Golden Palm Award at the 1956 Cannes Film Festival.   

==References==
 

== External links ==
*  

 

 
 
 
 
 

 