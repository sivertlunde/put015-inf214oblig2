Wuthering Heights (1998 film)
 
{{Infobox film
| name           = Wuthering Heights
| image          = Wuthering Heights 1998.jpg
| image size     = 200px
| alt            = 
| caption        = 
| director       = David Skynner  
| producer       = Louise Berridge Jo Wright (executive producer) Rebecca Eaton (executive producer)
| writer         = Neil McKay  Emily Brontë (book)
| screenplay     = 
| story          =  novel  by Emily Brontë
| narrator       = 
| starring       = Robert Cavanah Orla Brady Sarah Smart Warren Bennett
| cinematography = 
| editing        = 
| studio         = London Weekend Television WGBH-TV ITV 
| released       = 5 April 1998 (UK) 18 October 1998 (USA)
| runtime        = 112 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =
}}
 1998 British ITV on 5 April, 1998 in the United Kingdom and released by WGBH-TV on 18 October, 1998 in the United States. 

The films tagline is Two hearts that beat as one.

==Cast== Heathcliff
*Peter Davison as Joseph Lockwood Cathy Earnshaw
*Tom Georgeson as Joseph
*Matthew Macfadyen as Hareton Earnshaw
*Sarah Smart as Catherine Linton
*Kadie Savage as young Cathy Earnshaw
*Ken Kitson as Mr. Earnshaw
*Flora Montgomery as Isabella Linton Ian Shaw as Hindley Earnshaw
*Crispin Bonham-Carter as Edgar Linton
*David Maybrick as Gaddick
*Catherine Chesire as Frances Earnshaw
*Polly Hemingway as Nelly Dean

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 
 