Lord of the Universe
 
{{Infobox film
| name           = Lord of the Universe
| image          = Lord of the Universe video cover.jpg
| image_size     = 
| border         = yes
| caption        = 1991 VHS edition
| director       = {{plainlist|
* Top Value Television
*Michael Shamberg
}}
| producer       = {{plainlist|
* David Loxton
* Top Value Television
}}
| writer         =
| music          =
| cinematography =
| editing        = {{plainlist|
* John. J. Godfrey
* Wendy Appel
}}
| studio         = Top Value Television
| distributor    = Subtle Communications
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         = US$30,000 
}}
 Hare Krishna.
 Top Value independent video documentary shown on national public television.
 Alfred I. du Pont/Columbia University Award in Broadcast Journalism. The documentary received a negative review in the New York Post, and positive reviews in The New York Times, The Boston Globe, the Los Angeles Times, and the Chicago Sun-Times. The San Francisco Bay Guardian wrote that the TVTV team had improved since their previous work but wanted them to move on to more challenging subjects.

==Content==
The documentary chronicles Guru Maharaj Ji, the Divine Light Mission, his followers and anti-Vietnam War activist Rennie Davis at "Millennium 73", an event held at the Houston Astrodome in November 1973.          Rennie Davis, a follower of Guru Maharaj Ji, was one of the spokespersons and speakers at the  "Millennium 73" event. His speech is featured in the documentary. 

Abbie Hoffman appears as a commentator in the documentary and addresses some points raised in Davis speech, stating: "Its rather arrogant of Rennie to say that he has found God and has his Telex number in his wallet."  The TVTV crew interviewed different "premies", or followers of Prem Rawat, throughout the film, and one teenage boy is shown stating: "Before I came to the Guru I was a freak, smoking dope and dropping out – and my parents were happier then than they are with this."  In a later part of the film, a loudspeaker voice announces: "Those premies who came in private cars can leave now. Those who came in rented buses can stay and meditate until further notice."    Adherents of other belief systems also appear in the documentary, including a born-again Christian who criticizes devotees for "following the devil", and a Hare Krishna follower.   

A separate storyline is seen concurrently through the coverage of the "Millennium 73" event, involving a man named Michael who has come to Houston, Texas, to receive Teachings of Prem Rawat|"Knowledge" from Maharaj Ji.  Once Michael has received the "Knowledge", he defends the secrecy behind the rituals.  Michaels experiences are contrasted in the documentary with interviews with "ex-premies" or former followers of Maharaj Ji, recounting their initiation and later disillusionment with Maharaj Jis teachings.  One of them says that after receiving "Knowledge" from Maharaj Ji, he was told that this free gift required lifetime devotion and donations of "worldly goods". 
 decorative garlands, while a band called "Blue Aquarius" plays his theme song.   The stage is decorated with glitter and neon lights, and Maharaj Jis brother performs rock music songs.   Abbie Hoffman gives a final comment in the documentary, stating: "If this guy is God, this is the God the United States of America deserves."   

==Production==
The documentary was produced by Top Value Television (TVTV) in association with TV Lab, and was primarily directed by Michael Shamberg.       TVTV had received initial funding for the documentary through a small grant from the Stern Foundation, and an additional promise from the Corporation for Public Broadcasting.   David Loxton arranged a post-production budget of USD|USD$4,000, and the total production costs for the documentary amounted to $36,000 – about forty-five percent of the average costs for a PBS film production at the time.   Several camera crews used  &nbsp;in (13&nbsp;mm) black and white portapaks and followed Maharaj Ji and his group across the United States for six weeks.    The TVTV production team debated whether to include the secret techniques of Maharaj Ji in the documentary and finally decided that it was vital to disclose these practices in the piece.   They chose to have an ex-premie divulge these practices rather than use a narration, but they were fearful of potential repercussions, which never came.   TVTV member Tom Weinberg found a man who demonstrated meditation techniques in the documentary, which he described as being the "Knowledge".   Producer Megan Williams stated that TVTV crew members empathized with the experiences of Maharaj Jis followers, because there was very little age difference between them and the TVTV production team. Nevertheless, many in the crew of TVTV felt superior to these "lost souls" describing the followers as "gurunoids". 

At the end of filming, eighty-two hours of tape were edited to the final fifty-eight minute documentary piece.   TVTVs team utilized graphics, live music, and wide angle lens shots.    Stop-action sequences where quotations flash on the screen were also used for effect.   The production was the first Portapak video documentary made  for national television, and the "first program originally made on  &nbsp;in (13&nbsp;mm) video tape to be broadcast nationally".       Lord of the Universe was also: "The first independent video documentary made for national broadcast on public television." {{cite book page = 70 | isbn = 0-89774-102-1}}   The trailer was originally broadcast on WNET Channel Thirteen television.   Lord of the Universe was shown to a national audience in the United States on February 2, 1974, broadcast on 240&nbsp;stations of the Public Broadcasting Service.        It aired a second time on July 12, 1974.  Later TVTV productions broadcast on public television included Gerald Fords America, and a 1975 program on Cajuns The Good Times Are Killing Me.    

In 1989, the documentary was included in an exhibition at the  , and  | url =http://cruzcat.ucsc.edu:2082/search/aWNET+(Television+station+:+New+York,+N.Y.)/awnet+television+station+new+york+n+y/-3,-1,0,B/frameset&FF=awnet+television+station+new+york+n+y+television+laboratory&4,,13%20Cruzcat%20Catalog%5D,%20%5B%5BUniversity%20of%20California,%20Santa%20Cruz%5D%5D,%20The%20Lord%20of%20the%20Universe
  | accessdate =  2007-11-07}}   The documentary was screened in August 2006 at The Centre for Contemporary Arts in Glasgow, Scotland, as part of the Camcorder Guerilla cinema programme. 

==Reception==

===Reviews=== title =Participatory TV Goes to Guru Gala |work=Chicago Sun Times
  | date = March 16, 1974}}   Electronic Arts Intermix described Lord of the Universe as "a forceful expose on the sixteen-year-old Guru Maharaj Ji and the national gathering of his followers at the Houston Astrodome".   Michael Blowen of The Boston Globe wrote that the documentary "captures the absurdity of Millennium 73", and that "The desperation of flower people alienated from politics is both touching and hilarious as they offer hope for eternal life to other converts."   

Dick Adler of the Los Angeles Times gave the documentary a positive review, writing: "The Lord of the Universe doesnt really take sides, which doesnt mean its a bland hour trying to please everybody.  Its considerable bite comes first from the material TVTV so carefully gathered and there from the artfully wise frame in which it chose to present it."     Deirdre Boyle wrote in Art Journal that the piece was "the zenith of TVTVs guerrilla-TV style".  According to Boyles Subject to Change: Guerrilla Television Revisited, as in all TVTV tapes, everyone in the documentary comes across as foolish, describing the productions sarcasm as the "ultimate leveler" using equal irony  "both with the mighty and the lowly". 

Katy Butler wrote in the San Francisco Bay Guardian that the TVTV style had improved since their previous work:  "This show has fewer interjections from TVTV personnel, fewer moments that drag, more technological razzle-dazzle (color footage, slow motion, stop motion, tight and rapid cutting)."  However, she described Guru Maharaj Ji and his entourage as "an easy target" and wrote that "anybody can look like a fool when a smartass wide angle lens distorts their face, and teenage ex-dopers who think a fat boy is God dont stand a chance".  Butler wished that TVTV would move on to more challenging subjects for their future work.  Bob Williams of the New York Post called the documentary a "deplorable film" and "flat, pointless, television".   He wrote: "The hour-long program was remiss in not providing some small examination of the available box-office take of the goofy kid guru, much less telling prospective contributors how it got involved in spending how much of its foundation grants and viewer subscription money in such a questionable venture without more inquisitive journalistic endeavor, or ignoring gurus."   

A review in The Oakland Tribune described the film as "a fascinating hour documentary on the gurus three-day happening at the Houston Astrodome", and commented that the event was "deftly captured by the mobile video cameras of TVTV, a group of talented young tapemakers".  John J. OConnor of The New York Times described TVTVs work as "a terrific documentary" and complimented the team on the visual results of the piece: "After TVTV superbly dissected the guru, his holy family and his followers, more objective viewers might have chosen to laugh, cry, or throw up."  In a later piece by OConnor in 1975, he wrote that TVTV "gained a respectable measure of national success with The Lord of the Universe". 

===Accolades===
The documentary received the 1974 "Alfred I. du Pont/Columbia University Award in Broadcast Journalism"   ( , and he raised additional funds that helped TVTV to produce five more programs, including Gerald Fords America.   

==Credits==
{| class="wikitable"
|-
| Production || Wendy Appel, Skip Blumberg, Bill Bradbury, John Brumage, Steve Christiansen, Paul Goldsmith, Stanton Kaye, John Keeler, Anda Korsts, Harry Mathias, Doug Michels, Tom Morey, Rita Ogden, Tom Richmond, Van Schley, Jodi Sibert, Elon Soltes, Akio Yamaguchi 
|-
| Editors || Wendy Appel, Hudson Marquez, Rita Ogden, Allen Rucker, Michael Shamberg, Elon Soltes
|-
| Producer || David Loxton. A TVTV production in association with the TV Lab at WNET/Thirteen. also: (Hudson Marquez, Allen Rucker, Michael Shamberg, Tom Weinberg, and Megan Williams)
|-
| Supervising Engineer/Videotape Editor || John J. Godfrey
|-
|}

==References==
 

==Further reading==
 
* 
* 
* 
* 
* 
* 
* 
 

==External links==
 
* 
* 
* , Media Burn Independent Video Archive (mediaburn.org)
* , Creative Commons License, Internet Archive

 
 

 
 
 
 
 
 
 
 
 
 
 