The 24th Day
 
{{Infobox film
| name           = The 24th Day
| image          = The24thDay.jpg
| alt            =  
| caption        = DVD release cover
| director       = Tony Piccirillo
| producer       = Nick Stagliano  Liliana Lovell
| writer         = Tony Piccirillo (play)
| screenplay     = 
| story          = 
| based on       = the play by the same name by Tony Piccirillo
| starring       = James Marsden Scott Speedman Sofía Vergara
| music          = Kevin Manthei
| cinematography = J. Alan Hostetter
| editing        = Aaron Mackof
| studio         = 
| distributor    = Warner Bros.
| released       =    
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The 24th Day is a 2004 film starring Scott Speedman and James Marsden. The film is based on a play of the same name, written by Tony Piccirillo, who also directed the film.

==Plot== bar and then proceed to Toms apartment together.  While there, Dan realizes that he had been in that same apartment before.  Five years earlier, Dan and Tom had a one night stand there.  According to Tom, that encounter with Dan was his first and only homosexual experience.  Some years later, Toms wife is found to be HIV positive.  Despondent after receiving this diagnosis from her doctor, Toms wife drives through a red traffic light and is killed in an ensuing collision.  Subsequent to these events, medical tests reveal that Tom is also HIV positive.  Tom blames himself for passing HIV on to his wife and, in turn, he blames Dan for passing the virus on to him.  Reasoning that Dan, ultimately, is to blame for his wifes death, Tom devises a plan to exact revenge.  He holds Dan hostage, keeping him bound and gagged to a chair in his apartment. He draws blood from Dan in order to conduct a test to determine Dans HIV status.  If Dans test results are positive for HIV, Tom vows to kill Dan.  If the results are negative, Tom agrees to release Dan unharmed. In the end, Tom returns to the apartment and lets Dan go. As Dan is leaving, Tom asks him when he had last been tested. A few moments later, he reveals that Dans test was, in fact, positive. He decided to let Dan go because he realized that his positive status was the result of his choices which he couldnt blame on anyone else. The screen fades with Dan standing in Toms doorway in shock.

==Cast==
* James Marsden as Dan
* Scott Speedman as Tom
* Sofía Vergara as Isabella
* Barry Papick as Mr. Lerner
* Charlie Corrado as Officer #1
* Jarvis W. George as Officer #2
* Scott Roman as Bartender
* Jeffrey Frost as Dans Assistant
* Jona Harvey as Marla
* Thea Chaloner as Wife
* Brian Campbell as Blondie

==Home media== region 1 DVD was released August 31, 2004.

==See also==
* List of American films of 2004

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 