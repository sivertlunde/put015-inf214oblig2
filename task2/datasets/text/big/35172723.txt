The Man with a Broken Ear
{{Infobox film
| name           = Lhomme à loreille cassée
| image          =
| caption        =
| director       = Robert Boudrioz
| producer       = 
| writer         = Robert Boudrioz 
| starring       = Thomy Bourdelle, Alice Tissot and Jacqueline Daix
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = France 
| awards         = French
| budget         = 
| preceded_by    =
| followed_by    =
}}
 French drama film directed by Robert Boudrioz and starring Thomy Bourdelle, Jacqueline Daix and Alice Tissot. It was an adaptation of the 1862 novel by Edmond About.

==Cast==
* Thomy Bourdelle - Le Colonel Fougas 
* Jacqueline Daix - Clémentine 
* Alice Tissot - Mademoiselle Sambucco 
* Jim Gérald - Le capitaine des pompiers 
* Gustave Hamilton - Le docteur Renaud 
* Christiane Arnold - Madame Renaud 
* Boris de Fast - Garok 
* Jacques Tarride - Léon Renaud

==External links==
* 

 
 
 
 
 
 
 


 