Screw Loose
 
 
{{Infobox film
| name           = Svitati
| image          = Svitati_poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Ezio Greggio
| producer      = Ezio Greggio Whitney R. Hunter Massimo Santorsola
| writer         = Rudy De Luca
| starring       = Ezio Greggio Mel Brooks Julie Condra
| distributors    = Columbia TriStar Home Video  Medusa Distribuzione
| released       =  
| country        = Italy
| language       = English
| released = 1999
| runtime =98 min
}}

Svitati (or Screw Loose) is a 1999 Italian comedy film produced by Atmosphere Film S.r.l and Wolf Pictures, directed by and starring Ezio Greggio, written by Rudy De Luca and Steve Haberman, and also starring Ezio Greggio, Mel Brooks and Julie Condra. It was released in 15 February 1999. 

== Plot ==
Bernardo Puccini (Ezio Greggio), visits the Italian natural food company, owned by his wealthy but hot-tempered father Guido Puccini (Gianfranco Barra) for a work as a food examiner and flavour developer. He is delighted by the taste of a new cream cheese he had asked, which contains no sugar, fat and preservatives. However, it displeases Guido for the lack of sugar, and angrily aruges with his son for additional chemical additives instead of natural substitutes. He then receives a heart attack and is taken to the hospital, along with his old-aged doctor, Dr. Caputo (John Karlsen).

In the intensive care unit, Guido recites to Bernardo his history during World War II where the statue of Virgin Mary fell on him and pinned, but was saved by an American soldier named Jake Gordon (Mel Brooks). He tells his son to deliver Jake to him as his last request for a half of his business, and Bernardo unwillingly accepts the request. Sofia (Randi Ingerman), his girlfriend who receives plastic surgery, questions about the request that it doesnt deserve to take the job, but he pursues to fulfill his fathers will.

When he arrives at Los Angeles, California, he visits the mental hospital where Jake Gordon is located, who was discovered as delusional from a day after the war he had experienced. Nevertheless, Dr. Barbara Collier (Julie Condra) and her father Dr. Hugo (Robert Dawson) refuses him because Jake is unpredictable to be released. After that, he sneaks to make Jake remember that Jake had saved his father, and to break him free. They are able to escape the hospital after they push the mental patients on wheelchairs as a diversion to gate guards, but are discovered by Dr. Barbara and Dr. Hugo, who orders her to retrieve him back.
 sedative pills. Bernardo unknowningly doses it, causing him to become perplexed in arrival terminal. They are soon found by Dr. Barbara Collier, and Jake steals Bernardos wallet (which he falsely thinks that Bernardo Puccini photo is him) and quickly escapes to Monte Carlo, Monaco. After he is recovered, Bernardo confronts Dr. Collier that he should bring Jake to his father to fulfil the request, but Collier wants him to return to the mental hospital when Dr. Hugo holds her responsibility for letting the patient go.

Bernardo and Collier arrive in Monte Carlo, and enter the hotel where unidentified Jake had checked in. Bernardo replies the lobby his own name that Jake used from his purloined wallet. The lobby tells them that he is out to the beach, and Bernardo finds him covering the topless female sunbathers with hand towels. He removes them, but accidentally includes a sensitive, aged sunbather, who calls the police due to his perversion. After he was released from prison by Collier, Bernardos twitching sensation is increasing, and he proves to her that having Jake with him turns him insane every time Jake causes trouble. In the diner, Collier advices him to stay calm so he will be fine. When he hears the piano tune and is annoyed by twitches, he rushes for it and finds Jake. When Jake finds Collier, he departs the building, causing Bernardo to chase after him, but causing the police to follow them.

Bernardo is feebly pinned down by the statue of Virgin Mary, which Jake reminds him about the incident. They finally sidestepped from police chase, but Jake is handcuffed by Collier who demands to take him with her. Bernardo does not surrender and sets up the trap, installing a fake corridor in the airport, which turns out to be a truck carrying Jake away. But again he was arrested. In the police station, Bernardo madly retells what Jake had been abusing him and what Jake had performed compulsive acts all along. Dr. Collier feels remorse about his sanity, so she allows him and Jake to depart back to Italy.

As Bernardo and Jake meet Guido, whose heart condition is recuperating, Guido strangles Jake for revenge because of the death of his wife. Bernardo breaks them apart, and Jake states his reason of vengeance that he had married Jiovana who happens to be Guidos wife, but realized that she is a he. Sofia approaches with her newly surgicated body parts, and tells the police to arrest Jake for his insanity. Dr. Collier intervenes and declares Jake a "free man", so he is released. Bernardo scolds Sofia for her abnormal femininity and her plastic surgery obsession, and dumps her. He then persuades his father to take over the factory. Guido asks to call over Dr. Caputo, but his son realizes that he is dead. Bernardo later falls in love with Dr. Collier while Jake enjoys his marching dance with other patients.

Later, Bernardo becomes Colliers husband and Jakes assistant, but then pretends to dislike the cream cheese the way Guido did. He visits Jake in the office, and grows headache. Jake uses a "relocating pain" again by hitting Puccinis leg with a golf club. Bernardo runs away while Jake tries to finish the "treatment".

== Cast ==
*Ezio Greggio – Bernardo Puccini
*Mel Brooks – Jake Gordon
*Julie Condra – Dr. Babara Collier
*Gianfranco Barra – Guido Puccini
*Randi Ingerman – Sofia
*Joshn Karlsen – Dr. Caputo
*Enzo Iacchetti – the Factory Guard
*Robert Dawson – Dr. Hugo
*Sofia Milos – the woman in airport

== Production ==

 

== Reception ==

 

== References ==
 

== External links ==
* 

 
 
 
 
 
 