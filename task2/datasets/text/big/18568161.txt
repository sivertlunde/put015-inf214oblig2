Halo (1996 film)
{{Infobox film
| name           = Halo
| image          = Halo (1996 film).jpg
| image_size     =
| caption        =
| director       = Santosh Sivan
| producer       = Childrens Film Society, India
| writer         = Santosh Sivan
| narrator       =
| starring       = Benaf Dadachandji, Bulang Raja, Viju Khote, Mukesh Rishi, Tinnu Anand
| music          = Ranjit Barot
| cinematography = Santosh Sivan
| editing        = Kanika Myer, Bhara J.
| distributor    =
| released       = 7 February 1996
| runtime        = 92 min
| country        = India
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Halo is a 1996 Indian film directed by Santosh Sivan. It tells the story of Sasha ( Benaf Dadachandji ) who is searching for her lost puppy in the streets of Mumbai and the variety of people that she meets. 

==Awards==
*1996: National Film Award for Best Childrens Film 
*1996:National Film Award – Special Jury Award / Special Mention (Feature Film)-Benaf Dadachandji
*2001: Filmfare Critics Award for Best Movie

==Plot==
Halo revolves around a seven-year-old girl, Sasha (Benaf Dadachandji), who is in quest of her lost puppy. Having lost her mother in childhood, she yearns for mother’s love and always feels lonely even though, there is a doting father in Rajkumar Santoshi. During vacation, when all other kids are busy playing, she sits silently and doesn’t even eat properly. So, the gluttonous servant fabricates a story that a miracle will happen in form of a Halo. There comes a street dog and Sasha believes it to be the miracle, the God sent Halo. She adopts it and names it Halo. Now her life revolves around it. She sleeps, she drinks, she eats with it. Her father doesn’t object. One day Halo is lost. Sasha is terribly upset. The quest of her lost puppy takes her through the terrifying streets of Mumbai to the neurotic editor of a newspaper, for smuggling, a police commissioner (Mukesh Rishi) and a colorful gang of street urchins.

==Public Viewing==
The movie was first relayed on Childrens Day in 1996 on Doordarshan.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 

 
 
 
 
 


 