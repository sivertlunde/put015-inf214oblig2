Social Register (film)
{{Infobox film
| name = Social Register
| image = Social register.JPG
| imagesize =
| caption = Theatrical release poster
| director = Marshall Neilan Harold Godsoe
| producer = William C. deMille  Marshall Neilan
| writer = James Creelman Clara Beranger Grace Perkins
| starring = Colleen Moore Charles Winninger Pauline Frederick Alexander Kirkland
| photography = Merritt Gerstad
| music = Nathaniel Shilkret
| Production Company = Associated Film Productions Columbia Pictures Corp.
| distributor = Columbia Pictures Corporation
| released =  
| country = United States
| language = English
| runtime = 71 minutes
}}
 John Emerson.

==Story==
Patsy Shaw (Moore) upsets a stuffy party at the home of wealthy Mr. Henry Breene (John Miltern) by stealing a necktie to win the scavenger hunt at Robert Benchleys party across the street. Charlie Breene (Alexander Kirkland), Henrys spoiled son, comes to retrieve his tie and becomes infatuated with Patsy.

Three months later, Charlie gives Patsy a valuable diamond bracelet, which she reluctantly accepts. Back at her apartment, Lester Trout (Ross Alexander), a saxophone player, convinces Patsy that he is ill and has lost his job. Patsy gives Lester the bracelet as a loan, against the advice of her friends, who are suspicious of him.

Charlies mother, Mrs. Henry Horace "Maggie" Breene (Pauline Frederick), fearful of her sons relationship with a plebeian, arranges a party hoping Patsys lack of social graces will ruin Charlies affection for her. Finding the party boring, Patsy goes to the bar, where after a few drinks, she begins to entertain the guests who are slowly wandering in to escape the dull affair. Maggie, relying on the family patriarch, Uncle Jefferson Breene (Charles Winninger), to end the romance, announces his arrival, but she is aghast when Patsy fondly hugs the old man whom she knows as "Jonesie," the friend of one of her roommates. Maggie then asks family attorney Albert Wiggins (Garvie) to end the romance, no matter the cost. Wiggins pays Lester to tell Patsy that he is ill and in need of her help. Charlie is informed about what Patsy did with the bracelet and accuses her of being unfaithful. Lester convinces Patsy to marry him, but as soon as they wed, she finds Wiggins check for $5000 and realizes the truth. Patsy tears the check up, just as Charlie arrives with Jonesie, who arranges their reconciliation. Having only been married to Lester for ten minutes, Charlie has Wiggins arrange Patsys annulment.

==Cast==
 
*Colleen Moore - Patsy Shaw
*Charles Winninger - "Jonesie," also known as Uncle Jefferson Breene
*Pauline Frederick - Mrs. Henry Horace "Maggie" Breene
*Alexander Kirkland - Charlie Breene
*Robert Benchley - Himself
*Ross Alexander - Lester Trout
*Margaret Livingston - Gloria
*Olive Olsen - Ruth
*John Miltern - Mr. Henry Breene
*Roberta Robinson - Kay Hans Hansen - Chris
*Edward Garvie - Albert Wiggins
*Ramona - Ramona
*Frey & Bragiotti - Piano duo
*Georgette Harvey - Lulu

==Songs==
*"Honey Dear" (music by Ford Dabney and Con Conrad, lyrics by Edward Heyman)
*"I Didnt Want to Love You" (music by Conrad, lyrics by Ned Washington)
*"Why Not" (music by Conrad, lyrics by Heyman)

==Background==
After several years of semi-retirement from Hollywood, Colleen returned to the film industry. The character she played in this film, while still working class (as they were in her other films for Neilan, like Dinty (film)|Dinty and Her Wild Oat) was more sophisticated and adult than the women she had played at the height of her fame in the late 1920s. Neilan was a heavy drinker and over the years his reputation as a director was eroded by his drinking and his disdain for authority. This was one of his last major films.

Some scenes were filmed on location in New York City.

A print of this film has long been held by the Library of Congress.

==References==
* http://movies.nytimes.com/movie/110822/The-Social-Register/overview
* Colleen Moore, Silent Star, Doubleday, 1968.
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).

==External links==
* 

 

 
 
 
 