Bomber (1982 film)
{{Infobox film
 | name = Bomber
 | image =  Bomber (1982 film).jpg
 | caption =
 | director =  Michele Lupo
 | writer =  Marcello Fondato, Francesco Scardamaglia
 | starring =   Bud Spencer, Jerry Calà
 | music =  Guido & Maurizio De Angelis
 | cinematography =  
 | editing =	Eugenio Alabiso
 | producer = Elio Scardamaglia
 | distributor =
 | released = 1982
 | runtime = 99 min
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} 1982 comedy film directed by Michele Lupo, featuring Bud Spencer and Kallie Knoetze.

==Plot==
Bud Graziano said "Bomber" is a former heavyweight boxing champion who retired to private life on a ship. Jerry Calà is instead a Lombard punter who finds himself always in trouble and one day gets really big. In fact, he unwittingly sets himself against a group of thugs who answer only to the orders of a new boxing champion who is making the rounds in the area with his victories. Even Bomber collides later with this sample name Rosco. So Bomber, suddenly awakened his passion for boxing, opening a gym for amateurs and begins to train the promising young George. Below Bomber organizes meetings which also participates Rosco which, being too strong, knocks the young George. After so many other battles, Bomber is on the verge of losing everything he has slowly and painstakingly put together and so decides to confront himself in combat Rosco.

==Cast==
*Bud Spencer: Bud Graziano aka Bomber
*Jerry Calà: Jerry
*Stefano Mingardo: Giorgio Desideri aka Giorgione
*Kallie Knoetze: Rosco Dunn
*Gegia: Susanna
*Nando Paone:Ossario
*Valeria Cavalli: Giorgiones girlfriend
*Bobby Rhodes:Samuel Newman
*Rik Battaglia
*Nello Pazzafini
*Angela Campanella

==External links==
* 

 
 
 
 
 
 


 
 