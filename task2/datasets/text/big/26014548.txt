Insan
 

 
{{Infobox film
| name           = Insan
| image          = Insaan 2005.jpg
| director       = K. Subash
| producer       = Sujit Kumar
| music          = Himesh Reshammiya 
| writer         = Krishna Vamsi
| starring       =Akshay Kumar  Ajay Devgan  Tusshar Kapoor Esha Deol Koena Mitra Lara Dutta Laila Mehdin Sharat Saxena
| label          = T-Series
| company        = D.M.S. Films
| released       =  
| country        = India Hindi
| genre          = Drama, War
}}

Insan ( ;  ) is a Bollywood film, released in January 2005. The film has a star cast including Akshay Kumar and Ajay Devgan in male lead  and Tusshar Kapoor, Esha Deol, Lara Dutta and Koena Mitra in the pivote roles. It is a remake of 2002 Telugu film Khadgam starring Meka Srikanth|Srikanth, Prakash Raj and Ravi Teja.

==Story==
The film revolves the story of Police Officer Rathod (Ajay Devgn) who is on a mission to eliminate a notorious terrorist Azhar (Rahul Dev) who killed his 
wife (Koena Mitra). Now story moves on Amjad (Akshay Kumar) who is an auto rickshaw driver & struggler Avinash (Tusshar Kapoor) who live their lives in very normal way. Amjad wants to marry Heena (Esha Deol). But her parents want her to marry a rich man. So Amjad plans to win her parents heart by a faux that is Amjad plans a drama with Avinash and his friend to play fake baddies & they involve Rathod to arrest & win Heenas heart. The plan goes awry as real baddies appears but Amjad overpowers them leading to the Heenas parents to marry Amjad. On the other hand Azhar is planning a bomb blast on train with passengers. Rathod learns that Azhar is none other than Amjads younger brother. Rathod beats Amjad in a mosque where Azhar takes refuge thinking that Amjad is trying to save him. Now Azhar calls Rathod to release his men and Amjad (who was arrested) & demands a helicopter for escaping. Rathod goes according to the plan where Amjad learns that his brother is a terrorist. Amjad pleads Azhar to leave the innocent people but Azhar denies it by framing it to do Jihad. A big fight goes on during blast which Rathod and Amjad kills the Azhars army. Just Azhar about to attack Rathod, Amjad shoots him down.

==Cast==
* Akshay Kumar  as  Amjad Khan
* Ajay Devgn  as  Ajit Rathod
* Tusshar Kapoor  as  Avinash
* Lara Dutta  as  Meghna
* Esha Deol  as  Heena
* Koena Mitra  as  Sonali Rathod
* Laila Mehdin  as  Indu
* Sharat Saxena  as  Heenas father
* Rahul Dev  as  Azhar, Amjads brother and Terrorist
* Asrani  as  Film Director
* Himani Shivpuri  as  Indus mother
* Laxmikant Berde  as  Laxman
* Vivek Vaswani  as  Agarwal
* Archana Puran Singh  as  Rajjo Beena  as  Amjads mother
* Viju Khote  as  Real Estate Agent
* Kader Khan
* Mac Mohan

==Box Office==
The film did average business at the box office, where it ran for three weeks. It made a total gross of around Rs.11 crores. 

==Soundtrack==
{{Track listing
| headline        = Songs
| extra_column    = Playback Sameer
| all_music       = Himesh Reshammiya

| title1 = Chunri
| extra1 = Alka Yagnik, Udit Narayan

| title2 = Chunri
| note2  = Instrumental
| extra2 =  

| title3 = Is Tarah Deewane
| extra3 = Kunal Ganjawala, Sunidhi Chauhan

| title4 = Khwahish
| extra4 = Alka Yagnik, Sonu Nigam

| title5 = Rabba Mere Rabba
| extra5 = Alka Yagnik, Udit Narayan

| title6 = Rabba Mere Rabba
| note6  = Sad	
| extra6 = Alka Yagnik

| title7 = Rain Rain
| extra7 = Shaan (singer)|Shaan, Sunidhi Chauhan
}}

==References==
 

==External links==
*  
*  

 
 