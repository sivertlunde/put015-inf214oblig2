Christopher Columbus: The Discovery
 
 
{{Infobox film
| name          = Christopher Columbus: The Discovery
| image         = Christopher columbus the discovery.jpg
| caption       = Theatrical release poster John Glen
| screenplay    = John Briley Cary Bates Mario Puzo
| story         = Mario Puzo
| starring      = Marlon Brando Tom Selleck Georges Corraface Rachel Ward Robert Davi Benicio del Toro 
| producer      = Alexander Salkind Ilya Salkind 
| music         = Cliff Eidelman
| editing       = Matthew Glen
| cinematography= Alec Mills
| distributor   = Warner Bros.
| released      = 21 August 1992
| runtime       = 120 minutes
| language      = English
| country       = United States United Kingdom Spain
| budget        = $45 million 
| gross         = $8,251,071
}} John Glen. the voyage Columbus to the New World in 1492.

Its behind-the-scenes history involved an elaborate series of financial mishaps, which later brought about an emotional falling-out between both Alexander and Ilya; indeed, as a frustrated Alexander would later lament, in an interview with the Los Angeles Times, "I know, after this, that Ill never make movies again." 

The film was released for the 500th anniversary of Columbus voyage.  

==Plot==
The titular Genoese navigator overcomes intrigue in the court of King Ferdinand and Queen Isabella of Spain and gains financing for his expedition to the West Indies, which eventually leads to the discovery of the Americas.

==Cast==

*Marlon Brando received top billing for his supporting part as Tomás de Torquemada King Ferdinand V
*Georges Corraface as Christopher Columbus. He replaced Timothy Dalton, who was originally chosen to play the title role. Queen Isabella I. She replaced Isabella Rossellini, who was originally chosen to play the female lead.
*Robert Davi as Martín Pinzón
*Catherine Zeta-Jones as Beatriz Enriquez de Arana
*Oliver Cotton as Harana
*Benicio del Toro as Alvaro Harana
*Simon Dormandy as Bives
*Michael Gothard was cast as the Inquisitors spy, but also as a potential replacement for the role of Tomás de Torquemada in case Brando did not show up for filming. Brando did indeed miss the first day of filming, and Gothard took over the role. However, Tom Selleck told the director that without Brando, he would quit the film. Word apparently got out, for Brando was on the set the next day, and assumed the role of Tomás de Torquemada, with Glen reshooting the scene. 
*Branscombe Richmond as Indian Chieftain
* Christopher Chaplin as Escobedo

== Production ==
  and Licence to Kill.

==Reception==
The film was not a commercial success, debuting at #4,   but ultimately bombed, grossing $8 million against its $45 million budget.

Critically, the film received mostly negative reviews,   with a rotten 7% rating on Rotten Tomatoes based on 29 reviews.  Brandos performance in particular was singled out as his "worst". 

==Awards== John Glen.

==Home video==
The film was released on VHS format from Warner Home Video. It has not been released on DVD in North America, but is available in other format regions on DVD.

==See also==
 
*  , another big budget, all-star epic about Columbus also released in 1992.
* Carry On Columbus – A comedy-film about Columbus released in 1992.
* The Magic Voyage, an animated film about Columbus also released in 1992.

==References==
 

== External links ==
*  
*   at Rotten Tomatoes
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 