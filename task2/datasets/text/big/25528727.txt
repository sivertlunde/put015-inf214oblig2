The Two Faces of War
 
The Two Faces of War (As duas faces da guerra in Portuguese) is a documentary shot in Guinea-Bissau, Cape Verde and Portugal that includes a series of interviews and testimonies of people who lived through the period of the anti-colonial war and liberation in Guinea-Bissau. This documentary, directed by Diana Andringa and Flora Gomes, sets the tone for a debate around the themes of reconciliation and historical memory in the post-conflict period of the Portuguese colonial war.

==Synopsis==
The documentary consists of interviews with veterans and leaders from Portugal, Guinea-Bissau and Cape Verde. Those countries lived through the conflict spanning from 1963 to 1974, a conflict between the PAIGC (African Party for the Independence of Guinea-Bissau and Cape Verde) and Portuguese troops.

In 1995, Diana Andringa, the director of the film, visited the town of Geba as a reporter and there she found a stone half-destroyed in the name of the Portuguese soldiers killed on African soil. This was the starting point for this work. Along with Flora Gomes, the second director, the two directors produced a documentary which is the result of two points of view of  Portugal and Guiné, about one of the bloodiest conflicts suffered during the Portuguese Colonial War.

For six weeks, Diana Andringa and Flora Gomes traveled through the regions of Mansoa, Geba, and guilegar in Guinea-Bissau,Cape Verde and Portugal where they collected the testimonies of people who lived through the colonial war.

Throughout the documentary we see the homage to Amílcar Cabral, founder of the PAIGC. The testimonies show the magnitude of Cabral who, despite being in the middle of the conflict between the two countries, has never ceased to feel to the Portuguese people as something of their own.

According to Amilcar, there existed between the two sides a complicity that went beyond the war. "Were not fighting against the Portuguese people, but against colonialism," words that show how many of the Portuguese colonies were joined in solidarity with revolutionary movements for independence. It was the case of PAIGC in Guinea-Bissau and Cape Verde, the MPLA (Popular Movement for the Liberation of Angola) in the case of Angola and FRELIMO (Front for the Liberation of Mozambique) in the case of Mozambique. Therefore, it is no accident that it was in Guinea where the captains movement (Capitães de Abril in Portuguese) grew, leading to the Carnation Revolution of April 25, 1974.

The war resulted in two victories: the independence of Guinea-Bissau and Cape Verde and democracy in Portugal. It is this "paired adventure" that Andringa and Flora want to tell through the voices of those who lived through the conflict. Diana Andringa and Flora Gomes are the narrators of the documentary. The soundtrack is made up of music from Portuguese, Guinea and Cape Verde from the featured period.

==Historical background==

=== The Estado Novo (New State) ===

The European colonies always expressed a certain resistance to the presence of the colonial powers. This feeling intensified in the 20th century where the first and second world wars implemented a strong nationalist sentiment in the colonized peoples.

Moreover, the large emerging powers of World War II, the United States of America and the Soviet Union, supported the formation of nationalist resistance groups. It is in this context that the Bandung Conference, held in 1955 gives voice to the colonies seeking an alternative to the bipolarity that confronted those two countries.
The U.S. and the former USSR were therefore keen to legitimize the claims of the colonies, either to maintain a balance in international relations, or for their own benefit.

In Portugal, the   from a possible confrontation between East and West, and secondly, the responsibility for the maintenance of sovereignty over its colonies. Yet the Portuguese governors of the time chose to join the NATO and the consequent desire of an alliance with the winners.

This integration of Portugal in NATO formed a military elite that became indispensable in the conduct of the Portuguese Colonial War. This established a series of conflicts between the military structure and political power.

In March 1961 in Portugal there was an attempt against the state conduct by Major General Botelho Moniz. This marked the beginning of the break, and the origin of a certain distrust of the arrangements for the maintenance of a single command center, facing the threat of confrontation with armed force. This led to rupture between the three staffs: the Army, Air Force and Navy.

The Estado Novo considered the independence movements as terrorist forces and camps as part of Portugal, and therefore never recognized the existence of a war.

From the Portuguese point of view, the fighting in Guinea-Bissau began in July 1961 when guerrillas of the Liberation Movement of Guinea (MLG) launched attacks on the villages of Santo Domingo, near the northwest border with Senegal. From the Guinean point of view, the clashes began in January 1963 when the African Party for the Independence of Guinea and Cape Verde (PAIGC), launched an attack on the Moncada Tite, south of Bissau, in the Corubal River.

The attacks quickly spread throughout almost the entire territory of Guinea, growing in intensity to the surprise of the Portuguese who considered themselves to be of greater military force.

The theater of war in Guinea was led by two men of strong personality. On the Portuguese side, General António Spínola and from Guinean side was Amílcar Cabral the president of PAIGC. With the decisions of the former, the Portuguese forces gained ground in 1968 and 1972 and managed to maintain the situation and sometimes take action to confirm the positions.

One can say that the Portuguese troops took in Guinea a defensive stance in order to control the actions of the PAIGC. To this end, Portugal manipulated public opinion through propaganda in favor of their troops, which profoundly affected the highest levels of the hierarchy of the party of Guinea. However, the situation rapidly changed position, favoring thus the position of the colony.

Anti-aircraft missiles forced the Portuguese troops to reassess the war effort. Marcelo Caetano fought with Spinola, exempting him from the position of governor who came to be occupied by Bettencourt Rodrigues on 21 September 1973. Three days later, the PAIGC declared the independence of the new state in Medina of Boe.

===The PAIGC===
The PAIGC had its genesis as the African Party for Independence (PAI) and was founded on September 19, 1956 by Amílcar Cabral, in the company of Aristides Pereira, Luís Cabral, Fernando Fortes de Almeida, and Julius Elisee Turpin.

At first it was a non official party and was only legalized four years later when it acquired its first office in Guinea Conakry.

In November 1957 the founders of the PAIGC participated in a meeting in Paris on the development of the struggle against Portuguese colonialism.

In January 1960 in Tunis  the second Conference of African peoples was held, which Cabral and his colleagues attended.

Later that year in London for the first time in an international conference Portuguese colonialism was discussed. This climate of denunciation gave stabilization to the PAIGC and they initiated the training of militants and the expansion into the country at the same time as requesting the support of bordering countries. The Peoples Republic of China  was the first to contribute, giving them training and ideological preparation. In 1961 was the turn of the Kingdom of Morocco to support the young party and so began the armed struggle with Portugal in 1962.

Killed on 20 January 1973, the founder of the PAIGC did not survive to see the freedom of Guinea. However, he went down in history as one of the most important chiefs among nationalists in the former colonies. Amílcar Cabral is the essence of the doctrine of the party.

==Bibliography==
*Afonso, Aniceto, e Carlos Gomes. Guerra Colonial (ISBN 9789724611921)
*Carlos Azeredo|Azeredo, Carlos. Trabalhos e Dias de um Soldado do Império (ISBN 9789722621427)
*Alpoim Calvão|Calvão, Alpoim. De Conakry ao MDLP
*Felgas, Hélio. Guerra na Guiné
*Fraga, Luís Alves de.  Força Aérea na Guerra em África
*Marinho, Luís de. Operação Mar - Verde: Um documentário para a história
*Mateus, Dalila Cabrita. A PIDE/DGS na Guerra Colonial 1961-1974
*Monteiro, Saturnino. Batalhas e Combates da Marinha Portuguesa (vol. VIII) (ISBN 9725622960)
*Mourão, Piçarra. Guiné Sempre,
*Pereira, Aristides. Uma luta, um partido, dois países
*Vaz, Nuno Mira. Guiné 1968 e 1973 - Soldados uma vez, sempre soldados!

==External links==
*  

 
 
 
 
 
 
 
 