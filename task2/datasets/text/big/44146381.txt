Enne Njan Thedunnu
{{Infobox film
| name           = Enne Njan Thedunnu
| image          =
| caption        =
| director       = P Chandrakumar
| producer       = P Ramachandran
| writer         = Sylam Aluva
| screenplay     = Madhu Shubha Shubha Sukumari Kaviyoor Ponnamma
| music          = A. T. Ummer
| cinematography = Vasanth Kumar
| editing        = G Venkittaraman
| studio         = RR International
| distributor    = RR International
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar and produced by P Ramachandran. The film stars Madhu (actor)|Madhu, Shubha (actress)|Shubha, Sukumari and Kaviyoor Ponnamma in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
  Madhu
*Shubha Shubha
*Sukumari
*Kaviyoor Ponnamma
*Jose Prakash
*Rugmini
*Aranmula Ponnamma Baby Sumathi
*Kanakadurga
*Kedamangalam Ali
*Kuthiravattam Pappu
*P. K. Abraham
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maayaa Prapanchangal || K. J. Yesudas || Bichu Thirumala || 
|-
| 2 || Pularikal Paravakal || P Jayachandran, Vani Jairam || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 