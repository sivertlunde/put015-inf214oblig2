Witchcraft (1964 film)
{{Infobox film
| name           = Witchcraft
| image          = Witchcraft poster 01.jpg
| caption        = Theatrical release poster
| director = Don Sharp
| starring = Lon Chaney Jr.
| producer = Robert L. Lippert Jack Parsons
| distributor = 20th Century-Fox
}}
Witchcraft is a 1964 British black and white horror film. It was directed by Don Sharp from a script by Harry Spalding. It stars Lon Chaney Jr. in one of his last proper acting roles.

==Plot== David Weston), fall in love and plan to marry regardless of the families objections.

The Laniers, headed by Bill Lanier (Jack Hedley), are building developers who now own part of the old Whitlock Estate. Without their knowledge and following the instructions of Lanier manager Myles Forrester (Barry Linehan) a bulldozer overturns headstones and churns up graves in the old Whitlock Cemetery. At night, when all is quiet an exhumed grave opens and Vanessa Whitlock (Yvette Rees), the witch buried centuries ago, rises from the grave. Together with Morgan Whitlock (Lon Chaney Jr.), they use their witchcraft and one by one, the Laniers meet with various fatal accidents.

Tracy Lanier (Jill Dixon) follows her future sister-in-law Amy into the family crypt only to see the risen Vanessa and her witches conducting black magic rites. Tracy is captured and while either in a trance or drugged tied up to be used as a human sacrifice. Looking for Tracy, Bill and Todd go to the Whitlock crypt. They find and rescue Tracy. Todd returns to the crypt for Amy, who has now joined the rest of her family and embraced Satanism. He is captured by the witches. Vanessa threatens Todd and reverting to good, just in time to save her beloved, Amy pours burning oil over her ancient ancestor. The fire kills not only all the witches but Amy as well. Todd escapes and along with his surviving family stands and watches the Whitlock estate burn to the ground ending the 300-year-old nightmare.

==Cast==

*Lon Chaney Jr. as Morgan Whitlock
*Jack Hedley as Bill Lanier
*Jill Dixon as Tracy Lanier
*Viola Keats as Helen Lanier
*Marie Ney as Malvina Lanier David Weston as Todd Lanier
*Diane Clare as Amy Whitlock
*Yvette Rees as Vanessa Whitlock
*Barry Linehan as Myles Forrester
*Victor Brooks as Inspector Baldwin
*Marianne Stone as Forresters secretary
*John Dunbar as Doctor
*Hilda Fenemore as Nurse

==Crew==

*Don Sharp - Director
*Arthur Lavis - Director of Photography
*Len Harris - Camera Operator
*Harry Spalding - Screenwriter
*Robert L. Lippert - Producer
*Jack Parsons - Producer
*Robert Winter - Film Editor
*Clive Smith - Film Editor
*Carlo Martelli - Music Composer
*Philip Martell - Music Conductor
*George Provis - Art Director
*Harold Fletcher - Makeup
*Joyce James - Hairdresser
*Jean Fairlie - Wardrobe
*Spencer Reeve - Sound Editing
*Buster Ambler - Sound Recording
*Clifton Brandon - Production Manager
*Renee Glynne - Continuity
*Barbara Allen - Production secretary Lippert Films - Production Company
*Twentieth Century Fox Film Corporation - Distribution Company
==Production==
The film was shot over 14 days. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 124-128 
==Release==

Witchcraft was released in the UK in March 1964 and in the USA in September of that year.
Witchcraft was finally released on Region 1 DVD along with Devils of Darkness as part of the Midnite Movies range of classic and cult horror films in 2007.

Note that it is not a part of or to be confused with the Witchcraft (1988 film)|Witchcraft thirteen movie series that began in 1988.
==References==
 
==External links==
* 

 

 
 
 
 
 
 