Aazhakadal
 
{{Infobox film
| name           = Aazhakadal
| image          = Aazhakadal.jpg
| caption        = Film DVD cover
| alt            =
| director       = Shaan
| producer       = P. Ramachandran
| writer         = T. A. Razak (Story)  Subodh Cherthala (Screenplay)
| starring       = Kalabhavan Mani  Shruthi Lakshmi
| music          = Mohan Sitara
| cinematography = Saloo George
| editing        = Sunny Jacob
| studio         = Pooja Films
| distributor    = Paduva Films
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Aazhakadal is a 2011 Malayalam film directed by Shaan, featuring Kalabhavan Mani and Sruthi Lakshmi in the lead roles.

==Plot==
Aazhakadal tells the story of Kunjumon (Kalabhavan Mani), who returns to the beach after 16 years, after serving a sentence for murdering his father Anthony Tharakan (Saikumar (Malayalam actor)|Saikuamr). He returns and become the leader of the fishermen and fights the local seafood king Vakkachan (Vijayaraghavan (actor)|Vijayaraghavan) with the support of his fathers old friends. He also gets love and support from his childhood friend Kochurani (Sruthi Lakshmi). In the end he finds out that Paulachan (Shammi Thilakan) was the man behind all his misery and the murder of his father. He avenges his death and rescues the fishermen.

==Cast==
* Kalabhavan Mani as Kunjumon
* Sruthi Lakshmi as Kochurani
* Shammi Thilakan as Paulachan Vijayaraghavan as Vakkachan Saikuamr as Anthony Tharakan
* Sabitha Anand as Rosanna
* Lakshmipriya as Indira
* Meghanathan as Kuttappai
* Ajayan as Monachan
* Bindu Panicker as Thresamma
* Harisree Ashokan as Eenasu
* Sivaji Guruvayoor as Pappichayan

==External links==
* 

 
 
 
 
 


 
 