I'm Losing You (film)
{{Infobox film|
| name = Im Losing You
| image = Im Losing You cover.jpg
| caption = Theatrical release poster
| director = Bruce Wagner
| screenplay = Bruce Wagner
| based on  =  
| produced = Pamela Koffler Christine Vachon
| starring = Andrew McCarthy
| cinematography = Gordon Willis
| music = Georges Delerue
| editing = Janice Hampton Lionsgate
| distributor = Lionsgate 
| released =  
| runtime = 81 minutes
| budget = 
| gross = $13,996  ( )
| language = English
| country = United States
}} 1996 novel of the same name. 

==Plot==
A melodrama of a wealthy Los Angeles family - and the journey each one begins after a death in the family. The title of the film refers not only to the loss of life and love, but to a phrase used by most Angelenos while talking on cellular phones; Im Losing You... "Im Losing You" follows the path of each character after a cataclysmic event: the death of Berties young daughter Tiffany, in an "accident". The family comes closer together in the wake of such an event, seeking to recover from a blow that has driven each one to near madness.

==See also==
* List of American films of 1998

==References==
 

==External links==
*  at the Internet Movie Database

 
 
 
 
 
 
 