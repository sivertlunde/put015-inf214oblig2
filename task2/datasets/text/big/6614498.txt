Bloodfist VI: Ground Zero
{{Infobox film
| name = Bloodfist VI: Ground Zero
| image =
| image size =
| caption =
| director = Rick Jacobson
| producer =
| writer = Brendan Broderick Rob Kerchner
| narrator = Don Wilson Marcus Aurelius Michael Blanks Anthony Boyer />Cat Sassoon
| music =
| cinematography =
| editing =
| distributor =
| released = January 31, 1995
| runtime = 86 minutes
| country = United States
| language = English
| budget =
}}
 Don Wilson, Marcus Aurelius, Michael Blanks and Anthony Boyer. It was directed by Rick Jacobson and written by Brendan Broderick and Rob Kerchner.

==Plot==
Air Force courier Nick Corrigan is sent to deliver a message to a nuclear missile base in a remote corner of the Midwest. Unbeknownst to him, however, a gang of terrorists have taken over the base in the hopes of launching the missiles at all of the nations largest cities. In a panic, they lock Nick in the base, thinking to keep him from interfering with their plans. Little do they know, however, that Sgt. Corrigan is a former Special Forces soldier who is more than capable of shutting them down single-handedly.

==Criticism==
In his book Reel Bad Arabs, Jack Shaheen lists this as one of the most Anti-Arabism|Anti-Arabist films produced by the United States film industry. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 
 