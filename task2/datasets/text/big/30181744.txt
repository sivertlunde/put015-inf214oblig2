The Flight Before Christmas
 
{{Infobox film
| name        = The Flight Before Christmas
| image       = The Flight Before Christmas.jpg
| caption     = 
| director    = Michael Hegner Kari Juusonen 
| producer    = Hannu Tuomainen Petteri Pasanen
| writer      = Mark Hodkinson Marteinn Thorisson Hannu Tuomainen
| starring    = Olli Jantunen Hannu-Pekka Björkman Vuokko Hovatta Vesa Vierikko Jussi Lampi Risto Kaskilahti
| music       = Stephen Mckeon
| studio      = Anima Vitae A. Film A/S Pictorion Magma Animation  Magma Films
| distributor = Telepool (Munich) The Weinstein Company (US)
| released    =  
| runtime     = 80 minutes 45 minutes (TV Version)
| language    = Finnish
| country     = Finland Denmark Germany Ireland
| budget      = €6,100,000 
| gross       = $21,822,495 
}}

The Flight Before Christmas ( , aka Niko & The Way to the Stars) is a 2008   was released in October 2012 in Finland.

==Plot==
Niko, a young reindeer, was told by Oona, his mother, that his father is one of the "Flying Forces", Santas flying sled reindeer. Niko dreams of joining his dad as a flying reindeer but he is unable to fly. While trying to fly with the encouragement of Julius, a flying squirrel who takes on the role of a mentor and father figure, the other young reindeer teased Niko. To avoid further teasing, Niko and his friend Saga leave their protected valley so Niko can practice without any disruptions. Niko gets spotted by two of the wolves, and escapes to his herd in panic, not thinking about the repercussions. While the herd is fleeing the valley, Niko overhears others talking of how his actions have damaged the herd. He decides to leave the herd in an attempt to find his father and Santas Fell. 

When Niko is discovered missing, Julius chooses to look for Niko as he can search without leaving a trail as fresh snow is falling. Once he finds him, Julius cannot convince Niko to return to the herd and reluctantly joins him in the search for Santas secret location. Meanwhile, Essie, a lost pet poodle, stumbles upon the wolf pack and is about to be eaten, but suggests to Black Wolf the idea of killing Santas Flying Forces reindeer. Essie is considered Black Wolfs good luck charm for this idea and is spared, but is also forced to join the pack on this grim plan. Niko and Julius discover Wilma, a weasel, stuck in a small tree branch and rescue her. She is not pleased because she feels she has to save the lives of Niko and Julius before she can go on with her life. Julius and Niko get lost and separated in a sudden blizzard and Niko wakes under a pile of snow, unnoticed by the wolf pack that is nearby. Niko overhears Black Wolf resolving to kill Santa as well and take his place and bring death instead of presents to boys and girls. When Niko is discovered, he flees and finds Julius, but the two are cornered between rock cliffs. 

Wilma shows up in the overhanging snowdrifts above and sings a song that destabilizes the snowdrifts, starting an avalanche. Hopping on to Niko, she steers the reindeer past many dangers by manipulating his antlers. After saving their lives, she is free to go but corrects Julius on the directions to Santas home due to previously working there as a singer. Niko convinces Wilma to guide them while Black Wolf and his pack are hot on their trail. Niko tries to cross a dangerous river by flying with Wilmas help, but fails once he looks down and is reminded of his fear of heights. Thinking they are dead, the wolves head for Santas home. At the same time, Julius and Wilma save Niko from going over a high waterfall. 

While waiting for Niko to recover, Julius tells how his flying squirrel family was taken by wolves so Niko is his son now until he finds his real father. Once they arrive at Santas place, Niko is almost hit by the Flying Forces while standing on the runway. Niko tells them of Black Wolfs plan but they doubt a wolf will ever make it to Santas secret valley. They go in to celebrate before heading out on Christmas Eve when Wilma sings them a song that asks them the identity of Nikos father. Nobody admits to being his father but they decide to do a "flying test" to see if Niko has the genetics to fly to prove if this claim is true or not. Julius is worried after almost losing Niko at the river and his scream distracts Niko so he falls yet again. After this the wolves end up getting in and the scared Flying Forces reindeer lose their power of flight. Black Wolf is determined to get Niko and ends up chasing him up a tall tree despite the efforts of Wilma and Julius to distract him. 

Julius recruits a reindeer with his head in a present box (so can fly) with a jab with an icicle. The other reindeer are cornered on the runway by the other wolves who are bowled over by the blindly flying reindeer. Julius then convinces the reindeer that they can fly, allowing them to save Niko using the sleigh. Black Wolf attaches himself onto the sleigh until Julius dislodges the part he is holding on to. In order to save Julius from falling, Niko jumps off the sleigh, only to realize that he can fly. Niko, backed up by the Flying Forces, send the wolves fleeing when they realize Black Wolf had fallen to his death. Santa invites Niko to join the crew but after Julius leaves to tell his mother and the herd that Niko is all right, Niko chooses to stay with his mother and with Julius, but will visit his reindeer father, (who turns out to be Prancer).

==Production==
The animation took place in three countries; initial animation production and effects were produced in Finland, Germany and Denmark. Rendering and Lighting took place in Finland, and much of the post-production was carried out in Ireland. The American version of the film re-casts Norm MacDonald as Julius and Emma Roberts as Wilma.

===English cast===
* Andrew McMahon as Niko
* Norm Macdonald as Julius
* Sam Gold as Specs/Black Wolf/Smiley
* Prudence Alcott as Saga
* Katie Leigh as Essie
* Emma Roberts as Wilma
* Paul Tylak as Prancer
* Susan Slott as Oona

===Finnish cast===
* Olli Jantunen as Niko
* Hannu-Pekka Björkman as Julius
* Vuokko Hovatta as Wilma
* Vesa Vierikko as Musta Susi
* Jussi Lampi as Räyskä
* Risto Kaskilahti as Rimppa
* Minttu Mustakallio as Essie
* Dennis Schmidt-Fob as Prancer
* Elina Knihtila as Oona

==Reception==
The Flight Before Christmas currently only has one review at Rotten Tomatoes, not enough to give it an aggregated score. The review, which is from Common Sense Media, gives the film 3 out of 5 stars, and the disclaimer: "Wolves will scare younger kids in this holiday feature."  However, it is implied that the film has received mostly mixed reviews by critics and audiences.

==Home media==
The film was released on DVD and Blu-Ray in most European territories, and was released as a direct-to-video film in North America on DVD.

==Soundtrack==
The original score was composed by Stephen McKeon.

* Happy Christmas! – Performed by Vuokko Hovatta
* Flying Forces Rock – Written by Stephen McKeon
* The Way to the Stars – Written by Stephen McKeon, performed by Sean Dexter

==Sequel==
The sequel,   was first released in 12 October 2012 in Finland. The sequel follows Niko, who must deal with his mother getting re-married and gaining a stepbrother named Jonni. When Jonni is kidnapped by eagles, Niko flies off to rescue him. He is joined by an old, near-blind reindeer named Tobias, the former leader of the Flying Forces. But also standing in Nikos way is White Wolf, Black Wolfs sister, who leads the eagles and wants revenge on Niko for her brothers death. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 