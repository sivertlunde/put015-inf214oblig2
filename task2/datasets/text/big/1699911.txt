Turn Left, Turn Right
 
 
{{Infobox film
| name           = Turn Left, Turn Right
| image          = Turn Left Turn Right.jpg traditional = 向左走·向右走 jyutping = Heung joh chow, heung yau chow}}
| image_size     = 
| caption        = 
| director       = Johnnie To Wai Ka-Fai
| producer       = Johnnie To Wai Ka-Fai Daniel Yun
| writer         = Wai Ka-Fai Yau Nai-Hoi Au Kin-Yee Yip Tin-Shing
| narrator       = 
| based on       = A Chance of Sunshine by Jimmy Liao
| starring       = Takeshi Kaneshiro Gigi Leung
| music          = Ben Cheung Chung Chi Wing
| cinematography = Cheng Siu-Keung
| editing        = Law Wing-Cheong Milky Way Image Company
| distributor    = Warner Bros. (Peoples Republic of China|China)
| released       =   
| runtime        = 96 min 
| country        = Hong Kong 
| film location  =  , Taipei
| language       = Cantonese
| budget         = 
| gross          = $2,083,193 
}}
 
 2003 joint Hong Kong-Cinema Singaporean romance film, filmed in Taipei, Taiwan. Produced and directed by Johnnie To and Wai Ka-Fai, the film stars Takeshi Kaneshiro and Gigi Leung. The story is based on the illustrated book A Chance of Sunshine by Taiwanese author Jimmy Liao, who makes a cameo appearance with his wife and daughter in the film. It is also the first Chinese language|Chinese-language Asian film ever from produced and distributed by Warner Bros.

The original graphic novel was first published in 1999. The characters in the novel were not given names, both characters were only refereed to as "him" and "her". Chinese title of the book translates to "Turn Left, Turn Right", A Chance of Sunshine is the original English title of the book, on the newer published editions the English title is shown as "Turn Left, Turn Right". The book consists of a series of detailed illustrated images, with a small amount of text in poetry style. The film has managed to include every single image in the book with a high level of accuracy, although some are fleeting.

==Plot==
The story tells of two people who live in buildings right next to each other, separated only by a wall, and are always near each other but cant seem to find one another.

John Liu (Takeshi Kaneshiro) is a violinist who works from job to job. During one of his gigs, he meets June, who offers to give him a ride home. However she flirts aggressively with John during the ride which frightens him. To get away from her he jumps out of her car and makes a run for his apartment building. The next day he sees her waiting for him on the left corner of his building which makes him avoid going to the left of his building.

Eve Choi (Gigi Leung) is a translator for a book publishing company. Her job is to translate foreign books into Chinese. She is given the daunting task of translating a horror novel into Chinese. At night while translating the horror novel she gets frightened and thinks her home is haunted. The next day she notices a scary-looking tree to the right of her apartment building and, frightened, she refuses to go to the right of her building.

John and Eve meet by accident at the park fountain, when he helps her pick up her papers that have fallen into the fountain. They find out that they had met each other when they were younger at an amusement park, when both their schools had organized a field trip to the same place. Then, Eve had asked John for his telephone number, but the two never got in touch because she left her school bag on the train and lost Johns phone number. The two laugh about the past and exchange phone numbers without asking for each others name before parting quickly because of a coming rainstorm.

That night, both are happy and excited to have met each other again, but both also came down with the flu because of the earlier rainstorm. The flu medication John has on hand has expired and he decides not to take it. Eve is overjoyed and forgets to take her medication. The next day the two are devastated to find out that the phone numbers they had exchanged are unreadable due to the paper getting wet during the rainstorm. Both can only make out a few numbers and vainly call several phone numbers at random hoping to get through to one another.

One of the numbers they called is a restaurant that Ruby (Terri Kwan) works at as a waitress and delivery person. Both John and Eve, ill and not wanting to leave their home because theyre afraid that they would miss the other persons phone call, decides to order delivery from Rubys restaurant. Ruby falls in love with John at first sight when she arrives at his home to make her delivery. Seeing the smudged pieces of paper on both John and Eves table, Ruby soon finds out that the two are trying to find each other. To distract John from the apartment located in the next building Ruby tells him an old lady lives in that apartment.

Johns and Eves flu become so severe that they are both taken to the hospital, John by Ruby and Eve by an ambulance. They both meet Dr. Hu (Edmund Chen), but Dr. Hu happens to be a former University classmate of Eves, who has a crush on her. He tells Eve that it must be fate that they met each other again and proceeds to check her into the hospital to get the best care possible, while discharging John from the hospital, changing his mind only when Ruby argues with him about how severe Johns illness is. Both John and Eve give their home keys to Ruby and Dr. Hu to set up voicemail at their home in case the other person calls.

Once John and Eve are discharged from the hospital they find out that Dr. Hu and Ruby had literally moved into each of their apartments, but both make it clear that they are not interested in them since they already love someone else. Heartbroken, drunk and sobbing to each other Dr. Hu and Ruby decide to get together. Taking revenge on John and Eve for breaking their hearts, they send pictures to them showing how many places they had missed finding each other. John and Eve find Dr. Hu and Ruby to talk about the pictures they had received in the mail. Ruby gives Eves phone number to John telling him its her number to test if she was ever in his heart and Dr. Hu does the same to Eve, but they do not call.

John and Eve are frustrated about not being able to find each other and decide to take jobs abroad. On the day they are to leave Taiwan, an earthquake strikes, destroying the wall that separates their apartment. Both finally find each other. 

==Cast==

=== Main cast ===
*Takeshi Kaneshiro as John Liu
*Gigi Leung as Eve Choi
*Edmund Chen as Dr Hu
*Terri Kwan as Ruby

=== Supporting cast ===
*Lam Suet as Restaurant Manager (Johns boss at the Italian restaurant)
*Benz Hui as George (Eves boss) 
*Four Tse Liu-Shut as Music producer
*Wong Tak-Chi as Eves landlord
*Wong Lei as Johns landlady
*Beatrice Hsu as June
*Klaus Bru as Ghoul

=== Cameo ===
*Jimmy Liao and his family as family by the water fountain

=== Cantonese dubbing ===
Kaneshiro and Leung did their own Cantonese voice dubbing. Edmund Chen and Terri Kwan characters voices were dubbed by other people in the Cantonese version.
*Chan Suk Yee as Dr Hu
*Angela Tong as Ruby

==Filming locations== Songshan District of Taipei. 
*Fujing Street Shin Kong Mitsukoshi Department Store Taipei
*Fat Angelos Italian Restaurant – Ximending  
*Sweetme Hotspring Resort – Beitou  Warner Village
*Core Pacific City
*MRT Beitou Station
*Taipei City Hospital Yangming Branch Daan District
*Ximending
*Xinyi, Taipei|Xinyi, Taipei City Hall Plaza, Shin Kong Mitsukoshi

==Soundtrack==
{{Infobox album  
| Name        = Turn Left, Turn Right Original Soundtrack
| Type        = soundtrack
| Artist      = Various artistes
| Cover       = TurnLeftTurnRightOST(CD+VCD).jpg
| Released    = 15 August 2003
| Recorded    = 
| Language    = Cantonese
| Genre       = Cantopop
| Label       = Warner Music Hong Kong
}}
Turn Left, Turn Right Original Soundtrack (CD+VCD) (向左走、向右走 – 電影原聲大碟) was released on 15 August 2003 for the Hong Kong release of the film only by Warner Music Hong Kong. It features 22 tracks and a bonus VCD. Two different album covers were released for the same album. One showing only Kaneshiro and the other only Leung, both are at the same scene searching for someone amongst a crowd of people. Songs featured on the soundtrack are Cantonese version of each song, Mandarin version of the soundtrack was not released. "Encounter 遇見" by Stefanie Sun is not featured on the soundtrack since "At The Carousel 迴旋木馬的終端" by Gigi Leung is the Cantonese version of the same song. "Encounter 遇見" can be found on Stefanie Suns 2003 album "The Moment 關鍵時刻".

{{tracklist
| collapsed       = no
| extra_column    = Singer(s)
| total_length    =

| title1          = Shoulder To Shoulder
| note1           = 擦身而過 
| extra1          = Gigi Leung (narration) & Instrumental
| length1         = 1:57

| title2          = Two of Us
| note2           = 兩個人的幸運
| extra2          = Gigi Leung
| length2         = 3:47

| title3          = The Cries of the Ghoul 
| note3           = 死神的呼喚
| extra3          = Instrumental
| length3         = 1:29

| title4          = The Ghoul Cries Again 
| note4           = 死神再呼喚
| extra4          = Instrumental
| length4         = 1:10

| title5          = Violin Concerto OP. 47
| note5           = 琴弦上的鴿子
| extra5          = Instrumental 
| length5         = 0:47

| title6          = Edward Elgar Six Very Easy Pieces,NO.6
| note6           = 艾爾加六首給小提琴的簡易之歌(第六首)
| extra6          = Instrumental
| length6         = 0:41

| title7          = She Is 763092... 
| note7           = 她是784533... 
| extra7          = Instrumental
| length7         = 5:51

| title8          = He Is 763092...
| note8           = 他是763092...
| extra8          = Instrumental
| length8         = 2:58

| title9          = Signs And Signal
| note9           = 時機尚未成熟 
| extra9          = Instrumental
| length9         = 1:38

| title10         = Whats Your Phone Number?
| note10          = 交換電話號碼
| extra10         = Instrumental
| length10        = 2:24

| title11         = Broken Line
| note11          = 斷了線 
| extra11         = Gigi Leung (narration) & Instrumental
| length11        = 2:04

| title12         = A Lonely Christmas Eve
| note12          = 這個寂寞的平安夜
| extra12         = Instrumental
| length12        = 1:04

| title13         = At The Hospital
| note13          = 同病相憐 
| extra13         = Instrumental
| length13        = 0:24

| title14         = The Kiss 
| note14          = 
| extra14         = Instrumental
| length14        = 0:52

| title15         = A Song for a Girl Ive Lost
| note15          = 給一位失散了的女孩
| extra15         = Gigi Leung (narration) & Instrumental
| length15        = 0:49

| title16         = So Close Yet So Far...
| note16          = 這麼近又那麼遠... 
| extra16         = Instrumental
| length16        = 1:49

| title17         = At The Carousel
| note17          = 迴旋木馬的終端
| extra17         = Gigi Leung
| length17        = 3:29

| title18         = Revisit Edward Elgar Six Very Easy Pieces,NO.6
| note18          = 重提艾爾加六首給小提琴的簡易之歌(第六首)
| extra18         = Gigi Leung/Takeshi Kaneshiro (narration) & Instrumental
| length18        = 1:04

| title19         = If Only You Can Hear Me
| note19          = 高呼至沙啞
| extra19         = Instrumental
| length19        = 2:21

| title20         = At The Carousel
| note20          = 迴旋木馬的終端
| extra20         = Gigi Leung/Takeshi Kaneshiro (narration) & Instrumental
| length20        = 3:22

| title21         = Turn Left, Turn Right
| note21          = 向左走 向右走
| extra21         = Gigi Leung
| length21        = 3:47

| title22         = Lost My Shadow
| note22          = 沒有影子的人
| extra22         = Instrumental
| length22        = 2:27
}}

=== Bonus VCD ===
{{tracklist
| collapsed       = no
| writing_credits = no
| lyrics_credits  = no
| music_credits   = no

| title1          = Movie clip 1
| note1           = 電影精華片段 1

| title2          = Movie clip 2
| note2           = 電影精華片段 2

| title3          = Fortunately, Two of Us MV
| note3           = 兩個人的幸運 MV

| title4          = At The Carousel MV
| note4           = 迴旋木馬的終端 MV
}}
 

==Awards==
{| class="wikitable"
! style="width:50px;"| Year
! style="width:240px;"| Ceremony
! style="width:500px;"| Category
! style="width:60px;"| Result
|- 2004 
|align="center" rowspan=4| Hong Kong Film Award
|align="center"| Best Art Direction  Bruce Yu Ka-On 
|  
|- Best Costume & Make Up Design  Steven Tsang, Stephanie Wong 
|  
|- Best Original Film Score  Chung Chi Wing, Ben Cheung 
|  
|- Best Original Film Song  "Turn Left Turn Right" – Music: Peter Kam, Lyrics: Lin Xi, Performed by: Gigi Leung 
|  
|- 2003 
|align="center" rowspan=7| Golden Horse Awards
|align="center"| Best Supporting Actress  Terri Kwan 
|  
|- Best Adapted Screenplay   Wai Ka-Fai, Yau Nai-Hoi, Au Kin-Yee, Yip Tin-Shing 
|  
|- Best Art Direction  Bruce Yu Ka-On 
|  
|- Best Makeup & Costume Design  Steven Tsang, Stephanie Wong 
|  
|- Best Original Film Score  Chung Chi-Wing, Ben Cheung Siu-Hung 
|  
|- Best Original Film Song  "At The Carousel" – Music: Chet Lam, Lyrics: Albert Leung, Performed by: Gigi Leung 
|  
|- Best Visual Effects  Stephen Ma Man-Yin 
|  
|}

==Poem==
The poem recited by the main female protagonist of this film is ""Love at First Sight" ("Miłość od pierwszego wejrzenia") by famous Polish poet, essayist and translator Wisława Szymborska (She was awarded the 1996 Nobel Prize in Literature). The film used the translation of Walter Whipple. Another popular translation of the poem is by Stanisław Barańczak and Clare Cavanagh.

== References ==
 

==External links==
* 
*  
* , translated by Stanisław Barańczak and Clare Cavanagh
* , translated by Walter Whipple
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 