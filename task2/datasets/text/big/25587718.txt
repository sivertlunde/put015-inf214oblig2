Whom God Forgives
{{Infobox film
| name           = Whom God Forgives
| image          = Whom God Forgives.jpg
| caption        = Film poster
| director       = José María Forqué
| producer       = Ángel Monís
| writer         = José María Forqué Alfonso Sastre Natividad Zaro
| starring       = Francisco Rabal
| music          = 
| cinematography = Cecilio Paniagua
| editing        = Julio Peña
| distributor    = 
| released       = 7 May 1957
| runtime        = 92 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 Silver Bear Extraordinary Prize of the Jury award.   

==Cast==
* Francisco Rabal - Juan Cuenca
* Luis Peña - Andrés
* Alberto Farnese - Pedro
* Isabel de Pomés - Rosario
* Luisella Boni
* José Marco Davó - (as Marco Davo)
* José Sepúlveda
* Barta Barri - (as Barta Barry)
* Josefina Serratosa
* Santiago Rivero Ricardo Canales
* Antonio Puga
* Valeriano Andrés
* Rafael Calvo Revilla
* Alfonso Muñoz
* Salvador Soler Marí
* Casimiro Hurtado
* Adela Carboné

==References==
 

==External links==
* 

 
 
 
 
 