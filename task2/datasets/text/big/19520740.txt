Exodus (2007 Hong Kong film)
 
 
{{Infobox film
| name           = Exodus
| image          = 
| alt            = 
| caption        = 
| film name      = 出埃及記
| director       = Pang Ho-Cheung
| producer       = Pang Ho-Cheung
| writer         = Cheuk Wan-chi
| screenplay     = 
| story          = 
| based on       =  
| starring       = Simon Yam
| narrator       = 
| music          = Gabriele Roberto
| cinematography = Charlie Lam
| editing        = Stanley Tam
| studio         = 
| distributor    = 
| released       = 2007
| runtime        = 94 min
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}} Hong Kong film directed by Pang Ho-Cheung.

==Cast and roles==
* Simon Yam - Tsim Kin-Yip
* Annie Liu - Ann
* Nick Cheung - Kwan Ping-Man
* Irene Wan - Pun Siu-Yuen
* Maggie Shiu - Fong
* Candice Yu - Anns mother
* Pal Sinn - Anns father
* Chim Sui-man
* GC Goo-Bi
* Gordon Lam

==Critical reception==
The film opened to generally very positive reviews with the Hong Kong press. Perry Lam, in Muse (Hong Kong magazine)|Muse, praised Simon Yams performance, claiming the film featured the best acting that the prolific and still under-recognized Yam has done. 

==References==
 

==External links==
*  
*  

 

 
 
 
 


 