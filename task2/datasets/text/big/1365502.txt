Teacher's Pet (1958 film)
{{Infobox film 
| name           = Teachers Pet 
| image          = Teachers Pet 1958 Fr.jpg
| caption        = 
| director       = George Seaton 
| producer       = William Perlberg George Seaton
| writer         = Fay Kanin Michael Kanin  Nick Adams 
| music          = Roy Webb
| cinematography = Haskell B. Boggs
| editing        = Alma Macrorie
| distributor    = Paramount Pictures 
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English 
| budget         = 
}} 1958 American romantic comedy film starring Clark Gable and Doris Day. It was directed by George Seaton and co-starred Gig Young and Mamie Van Doren.

==Plot==
Journalism professor Erica Stone asks journalist James Gannon to speak to her night-school class.  He turns down the invitation via a nasty letter to her. His managing editor, however, orders him to accept the assignment. He arrives late to find Professor Stone reading aloud his letter and mocking him in front of her class.

Humiliated, he decides to join the class as a student in order to show up Professor Stone and get his own back by posing as a wallpaper salesman named Jim Gallagher.  The professor is somewhat intrigued by this charming older man, whom she finds an exceptional student.  Gannon continues his ruse and becomes attracted to Stone. He finds he has to contend with Dr. Pine, as well as his own girlfriend, Peggy DeFore, a nightclub singer (Mamie Van Doren).  When Stone discovers Gannons deception, she immediately calls off their relationship. Dr. Pine convinces her to give Gannon another chance.

By the end of the film, both Jim and Erica have come to understand, and partially adopt, the others point of view.

==Cast==
*Clark Gable as James Gannon &ndash; city editor for a large metropolitan newspaper, with no education past the 8th grade, who is convinced that formal education is “a waste of time” for anyone who would like to get into the newspaper business and that experience in the workplace is the key to success.
*Doris Day as Erica Stone &ndash; journalism instructor at a local university with whom Gannon falls in love.
*Gig Young as Dr. Hugo Pine &ndash; a worldly and attractive psychologist who has "more degrees than a thermometer" and Gannons (perceived) rival for Stones affections.
*Mamie Van Doren as Peggy DeFore &ndash; nightclub singer and Gannons girlfriend. Nick Adams as Barney Kovac &ndash; copy boy at Gannons paper who idolizes him.

==Awards and nominations==
*Academy Award for Best Supporting Actor (nomination) - Gig Young Academy Award for Best Original Screenplay (nomination) - Fay Kanin and Michael Kanin

==See also==
* List of American films of 1958

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 