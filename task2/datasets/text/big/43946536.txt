Thriveni
{{Infobox film 
| name           = Thriveni
| image          =
| caption        =
| director       = A Vincent
| producer       = CV Sridhar
| writer         = Thoppil Bhasi
| screenplay     = Thoppil Bhasi Sathyan Sharada Sharada Kaviyoor Ponnamma
| music          = G. Devarajan
| cinematography =
| editing        = G Venkittaraman
| studio         = Chithralaya Films
| distributor    = Chithralaya Films
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film, Sharada and Kaviyoor Ponnamma in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir as Shivaraman Sathyan as Joy Muthalali Sharada as Thankamma
*Kaviyoor Ponnamma as Parvathy
*KPAC Lalitha as Janaki
*Adoor Bhasi as Purushu
*Thikkurissi Sukumaran Nair as Padmanabhan
*Sankaradi
*Alummoodan
*Paravoor Bharathan

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaithappuzhakkaayalile || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Kezhakku Kezhakkoraana || PB Sreenivas, Latha Raju || Vayalar Ramavarma || 
|-
| 3 || Paamaram Palunku Kondu || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Sangamam Sangamam || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Sangamam Sangamam (Pathos) || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 