Side Street (1950 film)
{{Infobox film
| name           = Side Street
| image          = SideStreetposter.jpg
| alt            =
| caption        = Theatrical release poster
| producer       = Sam Zimbalist
| director       = Anthony Mann
| screenplay     = Sydney Boehm
| story          = Sydney Boehm Paul Kelly James Craig Jean Hagen
| music          = Lennie Hayton
| cinematography = Joseph Ruttenberg
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         =$935,000  . 
| gross          = $777,000 
}} crime film noir/police procedural directed by Anthony Mann. 

The motion picture was filmed on location throughout New York City and culminated in one of the first modern car chases, prior to 1968s Bullitt. Much of the story is set in the vicinity of the long-demolished Third Avenue El, a favorite location of the few films made in the city during that era.

The cast features Farley Granger and Cathy ODonnell for the second and last time; their earlier film was the noted noir They Live By Night (1948).

==Plot== Paul Harvey) extorted the James Craig).
 bartender Nick Paul Kelly) of the New York Police Department investigates the murder. Both Lorrison and Backett are interviewed, names found in her "love diary." After the birth of his child, Joe decides to try to return his ill-gotten gain, but Backett suspects a trap and refuses the offer. Backett instead sends Garsell and a taxi cab driver accomplice to grab Joe and recover the cash. Joe is able to escape after they discover that Drumman has substituted a nightgown in the package and gone into hiding with the money.

Joe looks for Drumman, but Garsell finds the bartender first, strangles him, and recovers the money. Joe confesses the original theft to Ellen, who urges him to turn himself in, but he finds himself a suspect in Drummans murder. He tries to track down the source of the money to clear himself, even as Capt. Anderson methodically pursues both men as suspects, Garsell for Lucille and Joe for Drumman. Joe locates Garsells girlfriend, singer Harriette Sinton (Jean Hagen), but she betrays him to Garsell. Garsell plans to murder Joe and strangles Harriette to eliminate her as a witness. Capt. Anderson is hot on their heels and a chase ensues through the early morning streets of New York. Garsell kills his partner and forces Joe to drive, but Joe deliberately crashes the taxi to end the nightmare. When Garsell tries to run out of the car, he is shot by the police and killed. Joe is carried out of the car by the police and taken into the ambulance. Ellen arrives just before he leaves, and they embrace before he goes to the hospital. Joe survives the accident, and the police are able to learn the truth so Joe is cleared.

==Cast==
* Farley Granger as Joe Norson
* Cathy ODonnell as Ellen Norson James Craig as George Garsell Paul Kelly as Capt. Walter Anderson
* Jean Hagen as Harriette Sinton Paul Harvey as Emil Lorrison
* Edmon Ryan as Victor Backett
* Charles McGraw as Det. Stan Simon
* Edwin Max as Nick Drumman
* Adele Jergens as Lucille Colner
* Harry Bellaver as Larry Giff, cabdriver
* Whit Bissell as Harold Simpson, chief teller
* John Gallaudet as Gus Heldon, bar owner
* Esther Somers as Mrs. Malby, Ellens mother
* Harry Antrim as Mr. Malby, Ellens father
* Ben Cooper as the young man at the drycleaners
* King Donovan as Detective Gottschalk

==Reception==
According to MGM records the film made $448,000 in the US and Canada and $323,000 elsewhere, resulting in a loss to the studio of $467,000. 

===Critical response===
When the film was screened in New York City in 2006 as part of Film Forums festival devoted to the "B Noir" films of the 1940s and 1950s, film critic Ed Gonzalez for Slant (magazine)|Slant magazine, reviewed the film and found he liked the pictures mise en scène and screenplay, writing, "...Side Street is a triumph of visual savvy and moral exactitude—-a scurrying spectacle of dog-cat-and-mouse throughout the veiny streets of New York City. The Big Apple comes alive via a nervy mix of photojournalistic shots of people on the move and hieratic compositions that give the squeeze to Farley Grangers Joe Norton..." 

Critic Nathan Gelgud said, "Because its an Anthony Mann movie, Side Street is similarly interested in detail, as well as great action sequences and even greater locations. The best stuff is inside a bar where Farley Granger leaves a bundle of stolen money.  The scenes in the bar are the ones that come immediately to mind when you think of Side Street because the details are spot-on, and Mann constructs the place with the depth of the academy frame he’s so good at utilizing." 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 
 
 