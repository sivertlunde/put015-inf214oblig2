I Loved a Woman
{{Infobox film
| name           = I Loved a Woman
| image          = I Loved a Woman 1933 poster.jpg
| image_size     = 220
| caption        = 1933 Theatrical Poster
| director       = Alfred E. Green
| producer       = Henry Blanke
| writer         = Edgar Wallace
| based on       = David Karsner (novel)
| screenplay     = Charles Kenyon Sidney Sutherland
| narrator       =
| starring       = Kay Francis Edward G. Robinson Genevieve Tobin
| music          = Bernhard Kaun
| cinematography = James Van Trees
| editing        = Herbert Levy
| studio         = First National Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 90 min.
| country        = United States English
| budget         =
| gross          =
}} drama directed by Alfred E. Green, starring Kay Francis, Edward G. Robinson, and Genevieve Tobin.  

==Plot==
John Hayden (Robinson), owner of a Chicago meat-packing company, falls in love with a beautiful opera singer (Francis).

==Cast==
* Edward G. Robinson as John Mansfield Hayden
* Kay Francis as Laura McDonald
* Genevieve Tobin as Martha Lane Hayden
* Robert Barrat as Charles Lane (Credits), Phineas D. Lane (in Film)
* Murray Kinnell as Davenport
* Robert McWade as Larkin
* J. Farrell MacDonald as Shuster
* Henry Kolker as Mr. Sanborn
* George Blackwood as Henry Walter Walker as Oliver
* Henry ONeill as Mr. Farrell
* E. J. Ratcliffe as Theodore Roosevelt
* William V. Mong as Bowen

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 