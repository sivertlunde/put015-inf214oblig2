Tragic Hunt
{{Infobox film
| name           = Tragic Hunt
| image          = Cacciatragica.jpg
| image_size     = 
| caption        =  
| director       = Giuseppe De Santis
| producer       = 
| writer         = Giuseppe De Santis Corrado Alvaro Michelangelo Antonioni Umberto Barbaro Carlo Lizzani Gianni Puccini Cesare Zavattini
| narrator       = 
| starring       = Vivi Gioi Massimo Girotti Carla Del Poggio Andrea Checchi
| music          = Giuseppe Rosati
| cinematography = Otello Martelli
| editing        = Mario Serandrei
| distributor    = 
| released       = November 4, 1947 (Italy) October 21, 1948 (U.S.)
| runtime        = 90 Min
| country        = Italy Italian
| budget         = 
}}
 1947  Italian  drama film directed by Giuseppe De Santis.

Future filmmakers Michelangelo Antonioni and Carlo Lizzani co-wrote the script.

==Plot== Second World War, in Emilia-Romagna, Italy, a cooperative has been founded by peasants. War has destroyed the country. A group of bandits, with former Nazi-collaborator Daniela, known as Lili Marlene (Vivi Gioi), holds up the truck where the money of the cooperative is travelling. All the peasants search for the thieves in a tragic hunt.

==Cast==

*Vivi Gioi: Daniela Lili Marlene
*Andrea Checchi: Alberto
*Carla Del Poggio: Giovanna
*Massimo Girotti: Michele
*Vittorio Duse: Giuseppe
*Checco Rissone: Mimì
*Umberto Sacripante:  The lame man 
*Folco Lulli: A farmer
* 
*Eugenia Grandi: Sultana
*Piero Lulli: The driver 
*Ermanno Randi: Andrea 
*Enrico Tacchetti: The accountant
*Carlo Lizzani: The veteran holding a speech

==Awards==
It won two Nastro dArgento as Best Director and Best Supporting Actress (Vivi Gioi).

== External links ==
*  
 

 
 
 
 
 
 
 
 
 

 
 