Fist of Fear, Touch of Death
{{Infobox_Film| name = Fist of Fear, Touch of Death
| producer = Terry Levene|
| director = Matthew Mallinson |
| writer = Ron Harvey Matthew Mallinson |
| starring = Bruce Lee Fred Williamson Adolph Caesar Ron van Clief Bill Louie  Aaron Banks|
| cinematography = John Hazard 
| editing = Jeffrey D. Brown Matthew Mallinson
| distributor = Troma Entertainment 
| released = 1980
| runtime = 90 min.
| language = English
}} 

Fist of Fear, Touch of Death, also known as The Dragon and the Cobra, is a 1980 martial arts film set at the "1979 World Karate Championships" at Madison Square Garden that will supposedly determine the "successor" to Bruce Lee. The film is hosted by Adolph Caesar. Bruce Lee was deceased before the film went into production, and any footage featuring Lee was taken from earlier films or television appearances. It is considered to be an exploitation film, exploiting Bruce Lees popularity, and the mystique surrounding his death.

==Synopsis==

TV reporter Adolph Caesar is outside Madison Square Garden before the start of a martial arts tournament that will apparently determine the "successor" to the legacy of Bruce Lee. He interviews martial arts promoter Aaron Banks, who says that Lee was actually killed by a kung fu move called "The Touch of Death." Banks describes the move as being effective in "three to four weeks." The segment contains a sequence of flashbacks to Bruce Lee ostensibly supporting Banks assertion. In one, a poorly dubbed Lee states "the secret to karate is power, internal power from the ear!" 

From inside Madison Square Garden, Caesar discusses the competitors (including Bill Louie, who, while in the ring, apparently pokes an opponents eyes out and flings them into the audience). He talks about the legacy of Bruce Lee, and shows what he describes as "interview footage" he did with Lee shortly before his death; the next scene is footage of Lee from the TV show Longstreet (TV series)|Longstreet with new dialogue awkwardly dubbed in. Then, Caeser flashes back to earlier in the day, where action star Fred Williamson seen having to traverse through a number of obstacles to get to the tournament while being repeatedly mistaken for Harry Belafonte. Next, Ron Van Clief is also profiled and interviewed. Van Clief is then seen saving a woman from being raped in a New York park.
 throwing star.

After Caesar announces the conclusion of "The Bruce Lee Story," the film transitions back to Madison Square Garden, where a number of performers are showcased. Caesar interviews Fred Williamson, who denounces the idea of a contest to determine Bruce Lees successor.

The grande finale is devoted to a two-round kickboxing match, in which Louis Neglia reigns victorious. Adolph Caesar concludes the film with a final thought.

==Criticism==

Fist of Fear, Touch of Death is routinely lambasted by fans of martial arts movies for its complete ignorance not only of the facts of Bruce Lees life, but of its apathy towards the culture of China (karate and samurais are constantly referred to as being Chinese, even though they are actually Japanese). Fans are also usually disappointed to learn that Bruce Lee had no actual involvement in this film (it was released 7 years after his death), despite the fact that his face is invariably plastered on posters and DVD boxes for the film.

A typical reaction comes from the website "Movies in the Attic": "I cant even explain how mind-bogglingly awful Fist Of Fear Touch Of Death is. The worst part is how it insults the intelligence of the viewer who has even mildly enjoyed a Bruce Lee movie. We hear Bruce Lees grandpa was a samurai.(The fact that samurais were from Japan and Bruce Lee is Chinese notwithstanding) Just adds to the overall stupidity. Oh and then we get footage of Bruce Lee explaining to his mother that he beats people up because of his tradition of his samurai grandfather, what makes this movie so unbelievably bad is that this footage is shot like a soap opera and is then spliced in with a bad samurai movie. Actually I take that back the samurai movie maybe indeed good but taken in this direction it just shows adds up to the overall futile surroundings. Williamson is wasted, the tournament footage is lackluster and the Bill Louie cameo is just unbelievably stupid. This is without a doubt the worst movie Ive ever seen."  

On his Bruceploitation website, "Keith" calls the film the worst Bruceploitation movie of all time. 

== External links ==
*  

 
 
 