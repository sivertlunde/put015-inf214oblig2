Rettai Vaalu
{{Infobox film
| name           = Rettai Vaalu
| image          =
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Desika
| producer       = S. K. Jaya Ilavarasan
| writer         =  
| screenplay     =
| story          =  
| Dialogue       =  Akhil Saranya Nag Kovai Sarala Thambi Ramaiah
| music          = V. Selvaganesh
| cinematography = 
| editing        = 
| studio         = Pranav Productions
| distributor    = 
| released       =  
| country        = India
| language       = Tamil
}}
 Tamil film Akhil and Saranya Nag in the lead roles, and was released in September 2014.

==Cast== Akhil
*Saranya Nag
*Kovai Sarala
*Thambi Ramaiah
*Pasanga Sivakumar
*Sairamani
*Benven Prabhakaran

==Production==
Desika made his directorial debut with the film, after having apprenticed under Thambi Ramaiah, Sairamani and Sakthi Chidambaram for fifteen years. He approached real estate businessman Jaya Ilavarasan with the script and succeeded in convincing him to make his first film. The team began filming scenes across Chennai in September 2012 under the title of Vaalu.  During production, the film faced a legal tussle over the title with the makers of the Silambarasan starrer Vaalu.  The Producers Council concluded that Desikas team had not registered the title and subsequently the film used Rettai Vaalu instead. 

The music for the film was composed by V. Selvaganesh and the lyrics were written by Vairamuthu. In June 2013, the team held an audio launch function where the first CD was released by Vairamuthu and received by producers R. B. Choudary and Kothanda Ramaiah|Keyar. 

==Release==
The film had a limited release across Tamil Nadu on 19 September, owing to the presence of bigger budget films at the box office. 

==References==
 

 
 
 
 
 


 