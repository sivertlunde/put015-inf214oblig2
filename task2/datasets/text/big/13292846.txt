Backstage (1988 film)
 
 
{{Infobox film
| name           = Backstage
| image          = 
| caption        = 
| director       = Jonathan Hardy
| producer       = Peter Boyle Geoff Burrowes
| writer         = Jonathan Hardy Frank Howson John D. Lamond
| based on       =  
| starring       = Laura Branigan Michael Aitkens Noel Ferrier Rowena Wallace
| music          = Bruce Rowland
| cinematography = Keith Wagstaff
| editing        = Ray Daley
| studio         = Burrowes Film Group
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Australia
| language       = English
| budget         = A$7 million 
| gross          = A$8,000 (Australia) 
}}
 Breaker Morant.

==Plot==
The plot centred on American pop singer Kate Lawrence (Branigan) wanting to embark on a career as an actress. The only job she can find is playing the lead role in an Australian theatre production of The Green Year Passes. The hiring of an American causes conflict with her Australian cast and crew, and the chagrin of theatre critic Robert Landau with whom she has an affair.

==Production==
In 1981 Frank Howson set up a company, Boulevard Films, with a view to making movies. He wanted to make a film on Les Darcy, Something Great, and collaborated with Jonathan Hardy on the script. They could not secure financing but Hardy showed Howson some other scripts he had written, including Backstage. Backstage had originally been meant to be directed in 1982 by John Lamond starring Max Phipps, Jill Perryman and Steve Tandy but the film did not eventuate. 

Howson and Hardy decided to work on Backstage together, with the story being relocated to the music world. Howson got Laura Branigan interested in the lead. The Burrowes Film Group needed to make another film before the end of the financial year and offered to raise the budget. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p196-197 

Frank Howson original thought that the involvement of the Burrowes Film Group would be limited but found they wanted to have more creative control. "I knew very clearly what audience it should be made for", said Howson, "but all of a sudden I found myself dealing with production by committee. To even make the smallest decision required everyone sitting around the table." 

Matters reached an impasse when Howson and Burrowes fought over who would compose the music. Burrowes fired Howsons composer, John Capek, and replaced him with Bruce Rowlands, then Burrowes bought Boulevard out of the film. 

Filming started 7 March 1986.

==Release== Variety conceded the film was "well crafted in every department". David Stratton called the film " neither funny nor romantic, and will do little for the careers of anyone involved in it." 

Branigan also performed several songs in the film.

The funding of the film was investigated in an episode of ABCs Four Corners. 

==Cast==
*Laura Branigan – Kate Lawrence
*Michael Aitkens – Robert Landau
*Noel Ferrier – Mortimer Wynde
*Rowena Wallace – Evelyn Hough
*Kym Gyngell – Paarvo

==See also==
*Cinema of Australia

==References==
 

==External links==
*  .
 
 
 

 
 
 
 
 