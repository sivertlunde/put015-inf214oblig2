Towelhead (film)
{{Infobox film
| name = Towelhead 
| image = Towelheadposter.jpg
| image_size = 215px
| alt = 
| caption = Promotional poster Alan Ball 
| producer = Alan Ball Ted Hope Steven M. Rales
| screenplay = Alan Ball
| based on =   Peter MacDissi Summer Bishil
| music = Thomas Newman
| cinematography = Newton Thomas Sigel
| editing = Andy Keir Scott Rudin Indian Paintbrush This is that
| distributor = Warner Independent Pictures
| runtime = 124 minutes
| released =  
| country = United States
| language = English French Arabic Spanish
| budget = 
| gross = $675,662 
}} Alan Ball novel of world premiere at the Toronto Film Festival on September 8, 2007 under the name Nothing is Private. The film, like the book, touches on issues of sexual awakening, privacy, and race.

==Plot== sexual awakening Iraq the next morning, he has sex with Jasira. When Rifat finds one of Mr. Vuosos adult magazines at his house, he beats Jasira, and she seeks refuge at the home of Melina (Toni Collette) and her husband, Gil, neighbors that were aware of Mr. Vuosos inappropriate behavior from the beginning. Eventually, Jasira reveals that she was raped by Mr. Vuoso, and he is arrested.

==Cast==
* Aaron Eckhart as Travis Vuoso 
* Toni Collette as Melina
* Maria Bello as Gail Monahan
* Peter Macdissi as Rifat Maroun
* Summer Bishil as Jasira Maroun
* Matt Letscher as Gil Hines
* Chase Ellison as Zack Vuoso
* Carrie Preston as Mrs. Vuoso
* Lynn Collins as Thena
* Shari Headley as Mrs. Bradley
* Irina Voronina as "Snow Queen" Centerfold Randy Goodwin as Mr. Bradley

==Critical reception==
Towelhead received mixed reviews from critics; Rotten Tomatoes reports that 48% of critics have given the film a positive review, based on 114 reviews with an average rating of 5.3 out of 10 with the consensus that "This story of politics, race and sexual awakening has moments that pack a punch, but overall, Towelhead never quite achieves the nuance of helmer Alan Balls television work."  The film also holds a score of 57 out of 100 on Metacritic based on 31 reviews. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 