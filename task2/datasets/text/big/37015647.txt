Picasso's Face
{{Infobox film

| name           = Picassos Face
| image          = Picassos Face Locandina.jpg
| caption        = 
| director       = Massimo Ceccherini
| producer       = 
| writer         =  Massimo Ceccherini  Giovanni Veronesi
| starring       =  Massimo Ceccherini  Alessandro Paci 
| music          = Riccardo Galardini
| cinematography = Giovanni Canevari 
| editing        =  Alessio Doglione
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 2000 comedy film written, directed and starred by Massimo Ceccherini.      

==Plot==
Massimo Ceccherini is looking for a plot for his new movie: the suggestions of the people around him are all the same and they indicate "less vulgarity" and "more gags".

==Cast==

* Massimo Ceccherini as Himself
* Alessandro Paci as  Himself
* Marco Giallini as  Producer
* Bianca Guaccero as  Samantha
* Pietro Fornaciari as  Fernando Capecchi
* Chiara Conti as  Stefania Nobili
* Yuliya Mayarchuk as  Ucraine Girl
* Giovanni Veronesi as  Priest
* Vincenzo Salemme as  Himself
* Andrea Balestri as  Pinocchio / Himself
* Christian Vieri as  Ivan Drago

== References ==
  

 

 