No Highway in the Sky
{{Infobox film
| name           = No Highway in the Sky
| image          = No Highway in the Sky.jpg
| image size     =
| alt            =
| caption        =
| producer       = Louis D. Lighton
| director       = Henry Koster
| writer         = Alec Coppel Oscar Millard R. C. Sherriff
| based on       =  
| starring       = James Stewart Marlene Dietrich Glynis Johns Jack Hawkins
| music          = Malcolm Arnold
| cinematography = Georges Périnal
| editing        = Manuel del Campo
| studio         =
| distributor    = 20th Century Fox
| released       =  
| runtime        = 98 min.
| country        = United Kingdom English
| budget         =
| gross          = $1,150,000 (US rentals)  
}}
 1951 United British disaster film directed by Henry Koster and starring James Stewart and Marlene Dietrich. The film is based on the novel No Highway by Nevil Shute, and was one of the first films that involved a potential aircraft crash. Although the film follows Shutes original novel closely, No Highway in the Sky notably omits references to the supernatural that had been contained in the original novel, including the use of automatic writing to resolve a key element in the original novels story.

==Plot== Farnborough to investigate the crash of a "Reindeer" airliner in Newfoundland and Labrador|Labrador, Canada. He theorizes the crash occurred because of a structural failure in the tail caused by sudden metal fatigue. To test the theory in his laboratory, an airframe is continuously shaken in eight-hour daily cycles.
 Gander Airport in Newfoundland and Labrador |Newfoundland, an inspection clears the aircraft to continue on, he decides to take drastic action to stop the flight. Honey raises the undercarriage while the aircraft is still on the ground, lowering the aircraft to its belly and damaging it. Shocked by the act, some of his colleagues demand that he be declared insane to discredit his theory.

Teasdale and flight attendant Marjorie Corder (Glynis Johns) both take a liking to Honey and Elspeth, who is lonely and isolated from her schoolmates. Teasdale speaks on his behalf to Honeys superiors, while Corder, seeing that he is decent but disorganized, decides to accept his offer of marriage.

During a hearing in which his sanity is questioned, Honey resigns but continues trying to prove that his theory is sound. In the laboratory, the time he predicted for a structural failure passes without anything untoward happening. The Reindeer he disabled, however, is repaired, but after a test flight, the tail breaks off. Shortly afterward, the same thing happens to the test airframe in the lab, and Honey discovers that he failed to include temperature as a factor in his calculations.

==Cast==
  
* James Stewart as Theodore Honey
* Marlene Dietrich as Monica Teasdale
* Glynis Johns as Marjorie Corder
* Jack Hawkins as Dennis Scott
* Janette Scott as Elspeth Honey
* Elizabeth Allan as Shirley Scott
* Ronald Squire as Sir John, Director
* Jill Clifford as Peggy, Stewardess
* Niall MacGinnis as Captain Samuelson, Pilot (uncredited)
* Kenneth More as Dobson, Co-Pilot (uncredited)
 
* Dora Bryan as Rosie, Barmaid (uncredited)
* Felix Aylmer as Sir Philip (uncredited)
* Maurice Denham as Major Pearl (Tour guide) (uncredited)
* Wilfrid Hyde-White as Fisher, Inspector of Accidents (uncredited)
* John Lennox as Farnborough Director (uncredited)
* Bessie Love as Aircraft passenger (uncredited)
* Arthur Lucas as Farnborough Director (uncredited) 
* Pete Murray as Peter, the Radio Operator (uncredited)
* Hugh Wakefield as Sir David Moon, Airline President (uncredited)
 

==Production==
 
The first writer who worked on the script was R.C. Sheriff. The novel was then assigned to producer Buddy Lighton who hired Oscar Millard to do the screenplay. Millard says he spent six months writing the script without ever looking at a Sheriff draft. In London, the producer Buddy Lighton hired Alec Coppel to re-write some scenes based at the Farnborough Aircraft Establishment. 
 Boscombe Down. 

The November 1950 Hollywood Reporter news items noted while the film was in production, Stewart underwent an emergency appendectomy in London.   Turner Classic Movies. Retrieved: 17 October 2014. 

==Reception==
Reviews of No Highway in the Sky were decidedly mixed. Bosley Crowther of The New York Times had a favorable review, noting the films "... sly construction of an unusual plot and wry suspense." Crowther, Bosley.
  The New York Times, 22 September 1951. Retrieved: 3 January 2011.  In a later appraisal, reviewer Dennis Schwartz opined: "American military war hero pilot James Stewart plays the eccentric Yank scientist working for a British airline, and gives one of his better and more pleasing performances as someone kindhearted but a bit daffy. ... The one-dimensional characters add no emotional depth, especially when the awkward romance is tossed onto the airplane drama; but Stewart plays a likable character that translates into a rather genial pic with much appeal." 

Three years after the film and six years after Nevil Shutes original novel (No Highway) there were two fatal crashes of the worlds first jet passenger airliner, the de Havilland Comet. Investigation found that metal fatigue was the most likely cause of both accidents, albeit in the fuselage and not the tail.   

==Adaptations in other media==
On 21 April 1952, before a live studio audience. Stewart and Dietrich, along with a full cast, reprised their roles in an adaptation of No Highway in the Sky on the CBS Lux Radio Theatre. Steffen, James.   Turner Classic Movies. Retrieved: 17 October 2014.  

The central element of No Highway in the Sky, of a concerned airline passenger having unique knowledge of an imminent danger, taking drastic action to eliminate it and then being regarded as insane, is comparable to that of The Twilight Zone episode "  scene in the later anthology movie is that the character of John Lithgow, like that of James Stewart, is portrayed as an engineering expert. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* 
* 
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 