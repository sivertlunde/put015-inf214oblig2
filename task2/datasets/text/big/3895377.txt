Dirty Love (film)
{{Infobox film
| name           = Dirty Love
| image          = Dirty Love.jpg
| director       = John Mallory Asher
| producer       = John Mallory Asher BJ Davis Rod Hamilton Kimberley Kates Michael Manasseri Jenny McCarthy Trent Walford Jim Cantelupe
| writer         = Jenny McCarthy
| starring       = Jenny McCarthy Eddie Kaye Thomas Carmen Electra Victor Webster
| music          = D. A. Young
| cinematography = Eric Wycoff
| editing        = Warren Bowman
| distributor    = First Look Pictures and DEJ Productions
| released       =      
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $9 million
| gross          = $36,099
}}

Dirty Love is a 2005 film written by and starring Jenny McCarthy and directed by John Mallory Asher. At the time of filming McCarthy and Asher were married; they divorced the month the film was released.  The film heavily plays off McCarthys reputation for toilet humor. The film made $36,016 at the box office, worldwide.

==Plot==
Rebecca (Jenny McCarthy), a struggling photographer, finds her model boyfriend Richard (Victor Webster) in bed with another woman. He also destroys all of her camera equipment. Her life falls apart, and she alternates between desire for revenge upon him, sexual promiscuity and abandonment of all hope of love.
 Guillermo Díaz) ecstasy and has a fetish for fish. She attempts to make Richard jealous by taking a director, who is reminiscent of Woody Allen, to a runway show, but he ends up vomiting on her breasts in front of everyone.

Ultimately Rebecca realizes she should focus her energy on being with someone who truly loves her, and that turns out to be John (Eddie Kaye Thomas), her nerdy but caring best male friend who has been supportive of her through the entire ordeal.

==Cast==
* Jenny McCarthy as Rebecca Sommers
* Eddie Kaye Thomas as John
* Carmen Electra as Michelle López
* Victor Webster as Richard
* Kam Heskin as Carrie Carson
* Deryck Whibley as Tony
* Steve Jocz as Steve The Drummer
* Kathy Griffin as Madame Belly
* David ODonnell as Jake
* Lochlyn Munro as Kevin Jessica Collins as Mandy
* Sum 41 as Themselves

== Critical Response ==
Dirty Love holds a 4% "Rotten" rating on the review aggregator website Rotten Tomatoes, based on 28 reviews summarized as, "The laugh-free Dirty Love is a comedy dead zone — its aggressively crude and shoddily constructed."  Film critic Roger Ebert gave the movie a zero star rating and said it was the third worst movie of 2005. In his written review of the film, he stated, "Here is a film so pitiful, it doesnt rise to the level of badness. It is hopelessly incompetent."   

==Awards== Worst Actress (Jenny McCarthy), and Worst Screenplay (written by McCarthy). It was also nominated for Worst Supporting Actress (Carmen Electra) and Worst Screen Couple, (Jenny McCarthy "& ANYONE
Dumb Enough to Befriend or Date Her").   

==Soundtrack==
The film features Sum 41 as a guest artist, and they perform "No Reason", the main song on the soundtrack.

==See also==
* List of American films of 2005

==References==
 

==External links==
*  
*  
   

{{Succession box
| title=Golden Raspberry Award for Worst Picture
| years=26th Golden Raspberry Awards
| before=Catwoman (film)|Catwoman
| after=Basic Instinct 2
}}
 

 
 
 

 
 
 
 
 
 