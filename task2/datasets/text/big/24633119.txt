The Other (1999 film)
 
{{Infobox film
| name           = The Other
| image          = The Other (1999 film).jpg
| caption        = Film poster
| director       = Youssef Chahine
| producer       = 
| writer         = Youssef Chahine Khaled Youssef
| starring       = Nabila Ebeid
| music          = 
| cinematography = Mohsen Nasr
| editing        = Rashida Abdel Salam
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Egypt France
| language       = Arabic
| budget         = 
}}

The Other ( , Transliteration|translit.&nbsp;El akhar,  ) is a 1999 French-Egyptian drama film directed by Youssef Chahine. It was screened in the Un Certain Regard section at the 1999 Cannes Film Festival.   

Lebanese soprano Majida El Roumi sang "Adam W Hanan", an Egyptian song included in the film.

==Cast==
* Nabila Ebeid as Margaret
* Mahmoud Hemida as Khalil
* Hanan Tork as Hanane (as Hanane Turk)
* Hani Salama as Adam
* Lebleba as Baheyya
* Hassan Abdel Hamid as Dr. Maher
* Ezzat Abou Aouf as Dr. Essam
* Ahmed Fouad Selim as Ahmed
* Amr Saad as Omar
* Ahmed Wafik as Fathallab
* Edward Said as Himself
* Hamdeen Sabahi as Chief editor
* Tamer Samir as Morcy

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 