That Uncertain Feeling (film)
{{Infobox film
| name           = That Uncertain Feeling
| image          = That Uncertain Feeling.gif
| image_size     = 200px
| caption        = Theatrical poster
| director       = Ernst Lubitsch
| producer       = Ernst Lubitsch Sol Lesser (uncredited)
| based on       =  
| writer         = Walter Reisch Donald Ogden Stewart
| narrator       =
| starring       = Merle Oberon Melvyn Douglas Burgess Meredith
| music          = Werner R. Heymann George Barnes William Shea
| distributor    = United Artists
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 
That Uncertain Feeling is a 1941 comedy film directed by Ernst Lubitsch and starring Merle Oberon, Melvyn Douglas and Burgess Meredith. The film is about the bored wife of an insurance salesman who meets an eccentric pianist and seeks a divorce. The screenplay by Walter Reisch and Donald Ogden Stewart was based on the 1880 French play Divorçons by Victorien Sardou and Émile de Najac. The film was a failure at the box office. 

==Plot==
At the suggestion of one of her friends, Jill Baker (Merle Oberon) visits psychoanalyst Dr. Vengard (Alan Mowbray) for her intermittent hiccups, which appear when she gets nervous or irritated. He soon has her questioning her previously happy marriage to her business executive husband Larry (Melvyn Douglas).

In Vengards waiting room one day, Jill meets a very odd and individualistic pianist, Alexander Sebastian (Burgess Meredith). He considers himself the best in the world when playing for a single listener, but has trouble performing in front of a large audience. She eventually invites him to an important dinner for Larrys prospective insurance buyers. When Larry realizes that Jill is infatuated with Sebastian, he gives her a friendly divorce, in which Larry is represented by a lawyer named Jones (Harry Davenport) whose secretary is Sally Aikens (Eve Arden).

Jill gets engaged to Sebastian, but after she learns that Larry is seeing an attractive woman, she realizes that she still loves her ex-husband. When she tries to reconcile with him, he pretends that Sally Aikens, Jones secretary, is in the other room. His deception is revealed when Sally enters the apartment while he is in the next room breaking a dinner date, supposedly with the distraught Sally (her supposed cries of anguish voiced by Larry). Jill and Larry get back together, and the hiccups vanish forever.

==Cast==
*Merle Oberon as Jill Baker
*Melvyn Douglas as Larry Baker
*Burgess Meredith as Alexander Sebastian
*Alan Mowbray as Dr. Vengard
*Olive Blakeney as Margie Stallings, Jills friend Harry Davenport as Jones, Larrys lawyer
*Sig Ruman as Mr. Kafka, Larrys prospective client
*Eve Arden as Sally Aikens
*Richard Carle as The Butler

==Award and honors== Academy Award for Best Music, Scoring of a Dramatic Picture.

==See also==
*Lets Get a Divorce (1918) Kiss Me Again (1925)

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 