Hero (2002 film)
 
 
{{Infobox film
| name = Hero
| image = Hero poster.jpg
| caption = Chinese theatrical release poster
| film name = {{Film name| traditional = 英雄
 | simplified = 英雄
 | pinyin = Yīngxióng
 | jyutping = Jing1 Hung4}}
| director = Zhang Yimou
| producer = Zhang Yimou
| writer = Feng Li Bin Wang Zhang Yimou Tony Leung Maggie Cheung Chen Daoming Zhang Ziyi Donnie Yen
| music = Tan Dun
| cinematography = Christopher Doyle
| editing = Angie Lam CFCC Elite Group Enterprises Zhang Yimou Studio Beijing New Picture Film
| distributor = Miramax Films   Beijing New Picture Film (China) EDKO Film (Hong Kong)
| released =  
| runtime = 1 hr 39 min (99 min)   1 hr 33 min (93 min) (China) 1 hr 47 min (107 min) (extended) (China) 1 hr 29 min (89 min) (TV) (Turkey) 1 hr 20 min (80 min) (unapproved cut)
| country = China Hong Kong
| language = Mandarin
| budget = $31 million 
| gross = $177.4 million 
}} King of Qin in 227 BC.

Hero was first released in China on 24 October 2002. At that time, it was the most expensive project  and the highest-grossing motion picture in Chinese film history.  Miramax Films owned the American market distribution rights, but delayed the release of the film for nearly two years. It was finally presented by Quentin Tarantino to American theaters on 27 August 2004.

==Plot== Qin states capital city to meet the king of Qin, who has survived an attempt on his life by the assassins Long Sky, Flying Snow, and Broken Sword. Because of the assassination attempt, no visitors are to approach the king within 100 paces. Nameless claims that he has slain the three assassins and he displays their weapons before the king, who allows the former to sit closer to him and tell him his story.
 Zhao state, where he pitted them against each other until Snow killed Sword and was herself slain by Nameless. As the tale concludes, the king expresses disbelief and accuses Nameless of staging the duels with the assassins, who surrendered their lives to allow him to gain the kings trust and take the kings life.

Nameless admits that he is a native of the Zhao state and that his family was killed by Qin soldiers; he also confesses that he defeated Sky without killing him and asked Snow and Sword to cooperate by faking a duel as well. Sword had waited for Nameless on his way to Qin after his false duel with Snow.  He told Nameless that the only way to achieve peace was to unite the states under a common dynasty, namely that of Qin, which alone had the ability to do so, thus revealing why Sword gave up his earlier assassination attempt.

The king, affected by the tale and by Swords understanding of his dream to unify China, ceases to fear Nameless. He tosses his sword to Nameless and examines a scroll drawn by Sword.  The king understands that it describes the ideal warrior, who, paradoxically, should have no desire to kill. When Nameless realizes the wisdom of these words, he abandons his mission and spares the king.

When Snow learns that Sword convinced Nameless to forgo the assassination, she furiously challenges Sword to a fight and unintentionally kills him when he chooses not to defend himself so that she would understand his feelings for her. Overwhelmed with sorrow, Snow commits suicide. Urged by his court, the king reluctantly orders Nameless to be executed at the Qin palace. He understands that in order to unify the world, he must enforce the law to execute Nameless as an example to the world. As the film ends, Nameless receives a heros funeral and a closing text identifies the king as Qin Shi Huang.

==Cast== Shaolin Temple.
 Tony Leung as Broken Sword ( ): Broken Sword and Flying Snow are the only assassins to ever infiltrate the kings palace, killing hundreds of his personal guard and very nearly the king himself before halting at the last moment. Of all the assassins, Broken Sword is the only one whom Nameless considers his equal in swordsmanship.

*Maggie Cheung as Flying Snow ( ): A skilled assassin, Flying Snow is Broken Swords lover and his equal as a swordsman. She has vowed revenge upon the King for killing her father in battle. When Broken Sword convinces Nameless to abandon the assassination attempt on the king, Flying Snow kills him and later herself.

* .

*Donnie Yen as Long Sky ( ): an accomplished spearman, Sky is the first to be "defeated" by Nameless, who takes Skys broken spear as proof of his defeat to the king. He is the least-seen assassin in the film.

*Zhang Ziyi as Moon ( ): Broken Swords loyal apprentice, skilled in using twin swords.

==Box office== HK $15,471,348 in its first week. Its final gross of HK $26,648,345 made it one of the top films in Hong Kong that year. On 27 August 2004, after a long delay, Hero opened in 2,031 North American screens uncut and subtitled. It debuted at #1, grossing US $18,004,319 ($8,864 per screen) in its opening weekend. The total was the second highest opening weekend ever for a foreign language film; only The Passion of the Christ has opened to a better reception.  Its US $53,710,019 North American box office gross makes it the fourth highest-grossing foreign language film and 15th highest-grossing martial arts film in North American box office history.  The total worldwide box office gross was US $177,394,432.

==Critical response==
The film received extremely favorable reviews, scoring 95% at Rotten Tomatoes  and 84 at Metacritic, indicating "universal acclaim".  Roger Ebert called it "beautiful and beguiling, a martial arts extravaganza defining the styles and lives of its fighters within Chinese tradition."  Richard Corliss of Time (magazine)|Time described it as "the masterpiece", adding that "it employs unparalleled visual splendor to show why men must make war to secure the peace and how warriors may find their true destiny as lovers."  Michael Wilmington of the Chicago Tribune called it "swooningly beautiful, furious and thrilling" and "an action movie for the ages."  Charles Taylor of Salon.com took an especially positive stance, deeming it "one of the most ravishing spectacles the movies have given us".  Nevertheless, there were several film critics  who felt the film had advocated autocracy and reacted with discomfort. The Village Voices reviewer deemed it to have a "cartoon ideology" and justification for ruthless leadership comparable to Triumph of the Will. 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  Hero was listed at 77th place on this list. 

==Political meaning and criticism== totalitarian and government of the Peoples Republic of China. These critics  argued that the ulterior meaning of the film was triumph of security and stability over liberty, analogous to the "Asian values" concept that gained brief popularity in the 1990s. 

The films director, Zhang Yimou, purportedly withdrew from the 1999 Cannes Film Festival to protest similar criticism.  Zhang Yimou himself had maintained that he had absolutely no political points to make. 

===Translation of "Tianxia"===
There has been some criticism of the film for its American-release translation of one of the central ideas in the film: Tiānxià ( ) which literally means "  (which took place just before the movie was filmed) the themes of universal brotherhood and "peace under heaven" may indeed be interpreted more globally, and taken to refer to peace in "the world."  The phrase was later changed in television-release versions of the film.

==Miramax release== Japanese weapon, in the North American promotional poster, which was both anachronistic and culturally misplaced. The United States version of the DVD, with Mandarin, English, and French soundtracks, was released on 30 November 2004.

==Awards and recognition== Best Foreign 2003 Academy Awards but lost to Nowhere in Africa (Germany).
*Zhang Yimou won the Alfred Bauer Prize at the Berlin International Film Festival in 2003 for his work in Hero.
*The National Society of Film Critics awarded Zhang Yimou their Best Director award.
*The New York Film Critics Circle recognized cinematographer Christopher Doyle with its award for Best Cinematography. The Aviator.
*The Online Film Critics Society awarded Hero Best Cinematography and Best Foreign Language Film.
*Hero received seven Hong Kong Film Awards in 2003, including Best Cinematography, Best Art Direction, Best Visual Effects, and Best Sound. The movie was also nominated for seven other awards, including Best Picture, Best Screenplay, Best Actress, Best Song, and Best Director.
*Hero won joint Best Film at the Hundred Flowers Awards in 2003.

==Music== Tony Leung. weiqi courtyard scene is a guqin. The guqin music for that scene was performed by Liu Li.

==See also==
*List of historical drama films

==References==
{{Reflist|30em|refs=
 
  {{cite web
    | url=http://www.boxofficemojo.com/movies/?id=hero02.htm
    | title=Hero at BoxOfficeMojo.com
  }} 
 
  {{cite news
    | title = Foreign Language – Box Office History
    | url = http://www.the-numbers.com/movies/series/ForeignLanguage.php
    | accessdate = 2009-01-13
  }} 
 
  {{cite news
    | url = http://www.boxofficemojo.com/genres/chart/?id=martialarts.htm
    | title = Action – Martial Arts
    | work = BoxOfficeMojo.com
    | accessdate = 2009-01-13
  }} 
 
  {{cite web
    | url = http://www.rottentomatoes.com/m/hero/
    | title = Hero at Rotten Tomatoes
  }} 
 
  {{cite web
    | url = http://www.metacritic.com/video/titles/hero2004?q=hero
    | title = Hero (2004) at Metacritic
  }} 
 
  {{cite news
    | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20040826/REVIEWS/408260304/1023
    | first = Roger
    | last = Ebert
    | publisher = Chicago Sun-Times
    | title = Hero
  }} 
 
  {{cite news
    | url=http://www.time.com/time/magazine/article/0,9171,1101040823-682258,00.html
    | work=Time
    | title=Men, Women and Fighting
    | date=15 August 2004
    | accessdate=4 May 2010
| first=Richard
| last=Corliss}} 
 
  {{cite news
    | first = Michael
    | last = Wilmington
    | url = http://www.helloziyi.us/Articles/hero-review.htm
    | work = Chicago Tribune
    | title = Hero
    | accessdate = 2010-08-24
  }} 
 
  {{cite web
    | url = http://dir.salon.com/story/ent/movies/review/2004/08/27/hero/index.html
    | title = "Hero" – Salon.com 
  }} 
 
  {{cite news
    | url = http://www.villagevoice.com/film/0434,hoberman2,56140,20.html
    | title = Man With No Name Tells a Story of Heroics, Color Coordination
    | author = J. Hoberman
    | work = Village Voice
  }} 
 
  {{cite web
    | url = http://en.wikisource.org/wiki/Zhang_Yimou_withdraws_from_Cannes
    | title = Zhang Yimou withdraws from Cannes
    | work = Wikisource
  }} 
 
  {{cite news
    | url=http://film.guardian.co.uk/interview/interviewpages/0,6737,1375277,00.html
    | work=The Guardian
    | location=London
    | title=Im not interested in politics
    | first=Geoffrey
    | last=MacNab
    | date=17 December 2004
    | accessdate=4 May 2010
  }} 
 
  {{cite web
    | url = http://www.eye.net/eye/issue/issue_09.30.04/film/mediumcool.html
     | title = Getting lost in translation
    | first = Jason
    | last = Anderson
    | date = 2004-09-30
    | accessdate = 2010-08-24
    | work = Eye Weekly archiveurl = archivedate = 2006-05-21}} 
 
  {{cite web
    | url = http://www.youtube.com/watch?v=C1hF8RzPtYY
    | title = "Cause: The Birth Of Hero" Documentary
  }} 
 
  {{cite news
    | url = http://www.wired.com/news/digiwood/0,1412,61554,00.html
    | title = Slideshow: Studio Warns Kung Fu Site  Wired
    | date=15 December 2003
  }} 
 
  {{cite book
    |last=Smith
    |first=Jim
    |title=Tarantino
    |publisher=Virgin Books
    |year=2005
    |location=London
    |pages=202
    |isbn=0-7535-1071-5
  }} 
 
  {{cite web
    | url = http://www.yesasia.com/us/1002392618-0-0-0-zh_TW/info.html
    | title = Hero soundtrack CD track list at YesAsia.com
  }} 
 
  {{cite web
    | url = http://www.filmtracks.com/titles/hero.html
    | title = FilmTracks.com: Hero
  }} 
}}

==External links==
 
*  
*  
*  
*  
*  
*  
*   at LoveHKFilm.com
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 