The Dognapper
 
 
{{Infobox Hollywood cartoon cartoon name=The Dognapper
|series=Mickey Mouse
|image=The Dognapper.jpg image size=
|alt=
|caption=Mickey and Donald David Hand   
|producer= story artist=
|narrator= voice actor=Walt Disney Clarence Nash
|musician= Dick Lundy Bill Roberts Bob Wickersham Marvin Woodward layout artist= background artist= Walt Disney Productions
|distributor=United Artists release date=  (USA)}}  color process=Black-and-white
|runtime=8 minutes
|country=United States
|language=English preceded by=Mickey Plays Papa followed by=Two-Gun Mickey
}} 1934 animated Walt Disney Pegleg Pete dognaps Fifi, David Hand and features the voices of Walt Disney as Mickey and Clarence Nash as Donald. 

The Dognapper was Donalds third film and was the first adventure story to feature both Mickey and Donald. This was the second of only three B&W cartoons to feature Donald Duck (the other two being Orphans Benefit and Mickeys Service Station). Because the color of Donalds feet doesnt show in black and white, his feet were black in these cartoons.

==Plot== Minnie Mouses Peg Leg Pete. A radio transmission detailing the suspects get-away car is heard by police officer Mickey Mouse and his sidekick Donald Duck. The pair soon see Peg Leg Pete speed by in the car and they chase after him, Mickey driving a motorcycle and Donald riding in a sidecar. Despite Petes evasive maneuvers he is unable to escape from Mickey and Donald, and they eventually follow him to his hideout in an abandoned sawmill.

Inside, Pete chains Fifi to the wall and grabs a submachine gun. Mickey and Donald follow Pete into the sawmill and hold him at gunpoint. The film is filled with various gags showing the two law men as bumbling and incompetent, yet at every turn they are able to stay ahead of Pete, but not capture him.

Finally, while Mickey and Donald are standing on a log, Pete turns on a circular saw positioned to cut the log. Mickey and Donald run to stay ahead of the blade, but eventually the end of the log comes and the saw blade shakes loose and spins out of control. The blade proves a threat for all three of the characters, but eventually Pete gets his peg leg caught in the center hole and brings it to a stop. Mickey and Donald restrain him with a corset, and in the final scene Mickey and Donald march Pete off to jail with the now-free Fifi angrily barking at him.

==Release history==
*1934 &ndash; Original theatrical release
*c. 1992  &ndash; Mickeys Mouse Tracks, episode #28 (TV)
*c. 1992  &ndash; Donalds Quack Attack, episode #54 (TV) The Ink and Paint Club, episode #1.32: "Goin to the Dogs" (TV)
*2002 &ndash;   (DVD)

==Notes==
 
*  at the Disney Film Project
* 
* 
 

 
 
 
 
 
 
 