Colorado Sunset
{{Infobox film
| name           = Colorado Sunset
| image          = Colorado_Sunset_Poster.jpg
| caption        = Theatrical release poster
| border         = yes
| director       = George Sherman
| producer       = Harry Grey (uncredited)
| screenplay     = {{Plainlist|
* Betty Burbridge
* Stanley Roberts
}}
| story          = {{Plainlist|
* Luci Ward
* Jack Natteford
}}
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* June Storey
}}
| music          = Raoul Kraushaar (supervisor) 
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
}} Western film directed by George Sherman and starring Gene Autry, Smiley Burnette, and June Storey. Written by Betty Burbridge and Stanley Roberts, based on a story by Luci Ward and Jack Natteford, the film is about a singing cowboy and his buddies who discover that the ranch they bought is really a dairy farm—and worse, its subject to intimidation from a protection racket that prevents dairy products from safely reaching the market.   

==Plot==
Tired of traveling around the country performing their music, singing cowboy Gene Autry (Gene Autry) and his Texas Troubadors decide to purchase a cattle ranch and settle down. When they arrive at the ranch purchased for them by Frog Millhouse (Smiley Burnette), they cannot believe that the herd consists of milkcows rather than the cattle they had anticipated.

Soon they find themselves in the middle of a dairy war in which various farmers trucks are being hijacked and destroyed in an attempt to drive them out of business. The town veterinarian, Dr. Rodney Blair (Robert Barrat), suggests that the Hall Trucking Company is behind the raids and proposes the establishment of a protective association. No one suspects that Blair and deputy sheriff Dave Haines (Buster Crabbe) are in fact the real masterminds behind the sabotage. When Gene vetoes Blairs idea of a protective association, the doctor directs his men to attack Genes ranch, sending a secret code over the radio station owned by Hainess unsuspecting sister Carol (June Storey).
 Jack Ingram), one of Blairs men, and turns him over to Sheriff George Glenn (William Farnum). Soon after, Blair arrives at the jail, kills the sheriff, and frees his henchman. Suspecting that Blair and Haines are involved in the raids, Gene accepts decides to run for sheriff against Haines, and he wins. Gene then convinces the ranchers to contract with the Hall Trucking Company. When he discovers Blairs secret radio messages, he tricks Dr. Blair and his men into an ambush in which the milk trucks are overturned, and the hijackers are caught. Gene and his men emerge victorious in the dairy war.

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* June Storey as Carol Haines
* Barbara Pepper as Ginger Bixby
* Buster Crabbe as Dave Haines
* Robert Barrat as Dr. Rodney Blair
* Patsy Montana as Patsy
* The Texas Rangers as Texas Troubadors
* Purnell Pratt as Mr. Hall
* William Farnum as Sheriff George Glenn
* Kermit Maynard as Cyrus Drake Jack Ingram as Henchman Clanton
* Elmo Lincoln as Dairyman Burns
* Frankie Marvin as Ranch Hand
* Slim Whitaker as Exploding Cigar Recipient (uncredited)
* Champion as Genes Horse (uncredited)   

==Production==

===Stuntwork===
* Bill Yrigoyen (stunt double for Gene Autry) 

===Filming locations===
* Corriganville, Ray Corrigan Ranch, Simi Valley, California, USA 
* Keen Camp, State Highway 74, Mountain Center, San Jacinto Mountains, California, USA   

===Soundtrack===
* "Colorado Sunset" (Con Conrad, L. Wolfe Gilbert) by Gene Autry, June Storey, and Cowboys at the end 
* "On the Merry Old Way Back Home" (Walter G. Samuels) by Gene Autry, Smiley Burnette, and The Texas Rangers
* "Cowboys Dont Milk Cows" (Smiley Burnette) by Smiley Burnette and The Texas Rangers
* "I Want to Be a Cowboys Sweetheart" (Patsy Montana) by Patsy Montana and The Texas Rangers
* "Poor Little Dogie" (Gene Autry, Johnny Marvin, Fred Rose) by Gene Autry (guitar and vocal)
* "Beautiful Isle of Somewhere" (John S. Fearis, Jessie B. Pounds) by Gene Autry and Townsfolk at the funeral
* "Autrys the Man – Vote for Autry" by Smiley Burnette and The Texas Rangers during electioneering
* "Autrys the Man – Vote for Autry" (Reprise) by Smiley Burnette and townsfolk after the election
* "Seven Years with the Wrong Woman" (Bob Miller) by Gene Autry (guitar and vocal)   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 