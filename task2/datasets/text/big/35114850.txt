Flores de otro mundo
{{Infobox film
| name           = Flores de otro mundo
| image          = Flores de otro mundo film poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster 
| director       = Icíar Bollaín
| producer       = Santiago García de Leaániz  Enrique González Macho
| writer         = Icíar Bollaín   Julio Llamazares
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = José Sancho   Luis Tosar   Lissete Mejía  
| music          = Pascal Gaigne
| cinematography = Teo Delgado
| editing        = Ángel Hernández Zoido	 	
| studio         = La Iguana   Alta Films
| distributor    = 
| released       = 28 May 1999
| runtime        = 105 minutes
| country        =  Spain
| language       = Spanish
| budget         = 
| gross          = 
}}

Flores de otro mundo ( )  is a 1999 Spanish film, written and directed by Icíar Bollaín and starring José Sancho, Luis Tosar and Lisete Mejía. Benavent, Cine Español de los Noventa , p. 263  The plot follows three women who arrive to a village in rural Spain looking for love and to start a new life. Benavent, Cine Español de los Noventa , p. 264  Very well received, the film won the International Critics Week Grand Prix  award at the Cannes Film Festival. 

== Plot ==
A group of women arrive by bus to a small town in the depopulated central Province of Guadalajara, where there is a scarcity of single women and economic opportunities. The men have organized an annual party to get to know available ladies with the hopes of falling in love and finding a wife. Women hope to find stability, companionship, immigration papers or a combination of the three.

The group of women include: Marirrosi, a divorced nurse from Bilbao who is lonely and looking for love; Patricia, a young woman with two children from the Dominican Republic who has been in Spain illegally and having difficulties to find decent work; and (joining later) Milady, a young black Cuban who wants to see the world at any cost. They come to the town filled with eager men but not lots of opportunities other than farming. Patricia settles with Damian, a hardworking farmer who lives with his mother, Gregoria. Looking to find stability for her young children and a hard worker herself, Patricia tries in vain to find common ground with her stern mother-in-law, who seems to reject her because of her skin color and the fact that her son is no longer under her control. Patricia, with more time and experience in Spain, befriends the newcomer Milady. Milady settles with Carmelo an older Spanish man who wooed her in Havana. Carmelo tries to impress her with a big television and modern kitchen, but what she really wants to do is go to discos and have fun. She is in no way ready to settle down. Her dream of a life in Europe is very different from his fantasy of life with a gorgeous young wife. Milady is flirty and does not love Carmelo. Wanting to see the sea in Valencia, she takes a ride from a truck driver. When she comes back to town, the jealous Carmelo gives her a beating. Milady wishes she could leave him, but she has few friends and no real place to go.

Marirrosi, the oldest of the three women, has a grown up son and is tired of her job in Bilbao. She finds love with Alfonso, a gentle soft spoken gardener. However, she is unwilling to give up city life for fields and cows, while he is unwilling to even consider living in a city. Unable to resolve their differences, she eventually breaks up with him.

At the farm, Patricia receives the visit of some of her friends and this creates confrontation with Gregoria, but Damian’s comes to Patricia’s defense and the relation between the two women improves. However the father of Patricia’s two children reappears in her life wanting to take advantage of her steadier situation. In a frank talk with Damian, Patricia admits that if she could have found work and been able to take care for herself either in the Dominican Republic or Spain, she probably wouldnt have married him. But now that she is there, she realizes that she wants to be his companion and stable love. Damian and Patricia have an argument after her honest revelation. He believes that she is taking advantage of him and he tells her to leave with her children. At the last minute, persuaded by his mother, Damian stops them from leaving and he and Patricia reconcile. Later on, we see Patricia’s daughter making her first communion in the local church.

Milady captures the attention of Oscar, a young man infatuated with her. He helps her escape the town but in a stop in a hotel she also leaves him, taking the road on her own. Carmelo deals alone with his disappointment of Milady’s sudden departure. 

Alfonso is devastated when he receives Marirrosi’s letter breaking up with him. At the village’s bar Alfonso gives some advice to the local single men. Another bus, full of hopeful single women, is arriving in town.

== Cast==
* José Sancho  as Carmelo	
* Luis Tosar  as Damián
* Lissete Mejía  as Patricia	
* Chete Lera  as Alfonso
* Marilyn Torres as Milady
* Elena Irureta as Marirrosi
* Amparo Valle as Gregoria
* Rubén Ochandiano  as Oscar
* Ángela Herrera as Lorna
* Doris Cerdá  as Daisy
* Chiqui Fernández as Aurora
* Carlos Kaniowsky as Felipe
* Isabel de los Santos as Janay
* Antonio de la Torre as truck driver

==DVD release==
Flores de otro mundo (flowers from another world) was released on DVD in the United States on October 18, 2005. The film is in  Spanish  with English subtitles. There are no extras features.

==Notes==
 

==Bibliography==
*Benavent, Francisco Maria. Cine Español de los Noventa. Ediciones Mensajero,2000.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 