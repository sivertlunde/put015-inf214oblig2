Gehrayee
 
{{Infobox Film
| name           = Gehrayee
| image          = gehrayee-dvd-cover.jpg
| image_size     = 
| caption        = 
| director       = Vikas Desai Aruna Raje
| producer       = N. B. Kamat
| writer         = Vijay Tendulkar Vikas Desai Aruna Raje
| narrator       = 
| starring       = Padmini Kolhapure, Sriram Lagoo, Anant Nag, Indrani Mukherjee
| music          = Laxmikant Pyarelal
| cinematography = Barun Mukherjee
| editing        = Vikas Desai Aruna Raje
| distributor    = 
| released       =   9 September 1980
| runtime        = 
| country        =   India Hindi
}}Gehrayee (  directed by Vikas Desai and Aruna Raje based on a script by Vijay Tendulkar, Desai and Raje, and starring Padmini Kolhapure, Sriram Lagoo, Anant Nag and Indrani Mukherjee with Amrish Puri in a guest appearance. The film was produced by N. B. Kamat.

==Plot==
Chennabasappa (Shriram Lagoo) is a successful high-collar manager of a reputed firm in Bangalore and lives with his very spiritual and rather docile wife Saroja (Indrani Mukherjee), son Nandish (Anant Nag) and daughter Uma (Padmini Kolhapure). Chennabasappa wants to build a house for his family in Bangalore and desperately needs money. He decides to sell his plantation spread across several acres in his ancestral village to a soap company. The plantation has been looked after by Baswa for many years. Baswa is Chennabasappas loyal servant. Upon knowing Chennabasappas intentions, Baswa becomes agitated as he considers Chennabasappas act to be something close to the rape of ones mother. In his view, the piece of land is like ones mother (sign of fertility) that Chennabasappa sold for money.

Chennabasappa is a rationalist, who is more like an atheist and doesnt believe on anything thats beyond sensory perception. He is a hard-headed fellow who is bossy at both office and home. As the story progresses, we see Uma behaving strangely. She would speak something of Chennabasappas unspeakable and totally unknown dark past. During one such revelation, the family members come to know that Chennabasappa had seduced Baswas wife while he was a teenager. Baswas wife got pregnant and jumped into a well to save herself from taunts.

Chennabasappa tries every medication and treatment that would bring Uma back to normal, but nothing works out. Unfortunately the family also becomes the target of several fake exorcists, who start milking them with their evasive talks but do no good to hapless Uma. In one such instance, the family is fooled by a Tantrik Puttachari (Amrish Puri), who actually tries to harness Umas virginity to resurrect his own devil. However his plans fail when Nandish interrupts in between and saves Uma.

Finally the family finds peace in the hands of a mighty but sane Tantrik Shashtri (Sudhir Dalvi), who discovers the roots of evil in Chennabasappas house itself, a spell cast lemon and an ugly voodoo doll. Shashtri orders the soul inside Umas body to reveal its identity and we come to know that the unholy spirit was actually sent to Uma by a village Tantrik whom Baswa paid for this heinous act. Uma turns to normal after few days.

A vengeful Nandish decides to go man to man with Baswa and reaches his ancestral village. He comes to know the Baswa died a few days back. Nandish begs a local Tantrik to help him out meet Baswas spirit as he is all set to find answers to his questions.

==Cast==
* Sriram Lagoo as Chennabassapa
* Indrani Mukherjee as Saroja
* Anant Nag as Nandu
* Padmini Kolhapure as Uma
* Ramkrishna as Rama
* Reeta Bhaduri as Chenni
* Amrish Puri as Tantrik Puttachary
* Sudhir Dalvi as Tantrik
* Kumar Sahu as Sabi

== External links ==
 

 
 
 
 
 
 