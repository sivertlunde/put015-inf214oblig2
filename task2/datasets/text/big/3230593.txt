O Fantasma
{{Infobox film
| name           = O Fantasma The Phantom
| image          = 
| image_size     = 
| caption        = 
| director       = João Pedro Rodrigues
| producer       = Amândio Coroado
| writer         = Alexandre Melo José Neves
| narrator       = 
| starring       = Ricardo Meneses Beatriz Torcato Andre Barbosa
| music          = 
| cinematography = Rui Poças
| editing        = Paulo Rebelo João Pedro Rodrigues
| distributor    = 
| released       =  
| runtime        = 90 min.
| country        = Portugal Portuguese
| budget         = 
| preceded_by    = 
| followed_by    =  Portuguese explicit gay-themed film directed by João Pedro Rodrigues and produced at the independent production company Rosa Filmes. 

==Synopsis==
Young and handsome Sergio (Ricardo Meneses) works the night shift as a trash collector in Lisbon. He is uninterested in his pretty female co-worker Fatima, who displays an avid interest in him, so instead Sergio roams the city. Eventually Sergio becomes fascinated with a sleek motorcycle and its arrogant owner - a young man totally indifferent to Sergio. Sergios surfacing desires unleash his darkest impulses, sending him down a dangerous path of violence, depravity, and degradation.

==Reception== 2000 Venice International Film Festival. It won the prize for best feature film in the New York Lesbian & Gay Film Festival and the Entrevues Film Festival. Ricardo Meneses was nominated for the 2001 Portuguese Golden Globe award for best leading actor.

==External links==
*  
*  

==References==
 

 

 
 
 
 
 
 
 
 
 
 


 
 