Billy Budd (film)
 
 
{{Infobox film
| name           = Billy Budd
| image          = Billy budd poster.jpg
| image_size     = 250px
| caption        = Original film poster
| based on       =    
| screenplay     = Peter Ustinov Robert Rossen DeWitt Bodeen
| starring       = Terence Stamp Robert Ryan Peter Ustinov
| director       = Peter Ustinov
| producer       = Peter Ustinov
| cinematography = Robert Krasker
| music          = Antony Hopkins
| editing        = Jack Harris (film editor)
| studio         = Anglo Allied Harvest Films Allied Artists
| released       = 12 November 1962
| runtime        = 123 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
 Ustinov as Captain Vere. Stamp was nominated for an Academy Award for Best Supporting Actor, and received a Golden Globe Award for Most Promising Male Newcomer. The film was nominated for four British Academy of Film and Television Arts|BAFTAs.

==Plot== impresses a crewman "according to the Rights of War" from the merchant ship The Rights of Man. The new crewman, Billy Budd (Terence Stamp), is considered naive by his shipmates, and they attempt to indoctrinate him in their cynicism. But Budds steadfast optimism is impenetrable, as when he is asked to critique the horrible gruel the crew must eat, he offers "Its hot. And theres a lot of it. I like everything about it except the flavor."  The crew discovers Budd stammers in his speech when under anxiety.

Though Budd manages to enchant the crew, his attempts at befriending the brutal master-at-arms, John Claggart (Robert Ryan), are unsuccessful. Claggart is cruel and unrepentant, a man who believes he must control the crew through vicious flogging; savaging them before they can prey on him. He reveals his mistrust for humanity when Budd confronts him about his discipline.

Budd: "Its wrong to flog a man. Its against his being a man."

Claggart: "The sea is calm you said. Peaceful. Calm above, but below a world of gliding monsters preying on their fellows. Murderers, all of them. Only the strongest teeth survive. And whos to tell me its any different here on board, or yonder on dry land?"

Claggart orders Squeak (Lee Montague) to find means of putting Budd on report and to implicate him in a planned mutiny. He then brings his charges to the Captain, Edwin Fairfax Vere (Peter Ustinov). Although Claggart has no reason to implicate Budd in the conspiracy, Budd becomes a target because Billy represents everything that Claggart despises: humility, innocence, and trust in humanity. Vere summons both Claggart and Budd to his cabin for a private confrontation. When Claggart makes his false charges that Budd is a conspirator, Budd stammers, unable to find the words to respond, and he strikes Claggart, killing him with a single blow.

Captain Vere assembles a court-martial. Vere and all the other officers on board are fully aware of Budds simplicity and Claggarts evil, but the captain is also torn between his morality and duty to his station. Vere intervenes in the final stages of deliberations (which are in full support of Budd). He argues the defendant must be found guilty for even striking Claggart, Budds superior, not to mention killing him. His arguments to pursue the letter of the law succeed, and Budd is convicted.

Condemned to be hanged from the ships yardarm at dawn the following morning, Budd takes care to wear his good shoes. At Budds final words, "God bless Captain Vere!", Vere crumbles, and Billy is subsequently hoisted up and hanged on the ships rigging. At this point the crew is on the verge of mutiny over the incident, but Vere can only stare off into the distance, the picture of abdication, overtaken by his part in the death of innocence. Just as the crew is to be fired upon, a French vessel appears and commences cannon fire on the Avenger, and the crew eventually returns fire. In the course of battle a piece of the ships rigging falls on Vere, killing him in an act of poetic justice. The ships figurehead is also shot off while a narrator tells of Budds heroic sacrifice.

==Cast==
* Terence Stamp as Billy Budd
* Robert Ryan as John Claggart, Master dArms Post Captain
* Melvyn Douglas as The Dansker, sailmaker Paul Rogers as Philip Seymour, 1st Lieutenant John Neville as Julian Radcliffe, 2nd Lieutenant
* David McCallum as Steven Wyatt, Gunnery Officer Ronald Lewis as Enoch Jenkins, maintopman
* Lee Montague as Squeak, Mr. Claggarts assistant
* Thomas Heathcote as Alan Payne, maintopman
* Ray McAnally as William ODaniel, maintopman Robert Brown as Talbot
* John Meillon as Neil Kincaid, maintopman
* Cyril Luckham as Hallam, Captain of Marines
* Niall MacGinnis as Captain Nathaniel Graveling

==Production==
Not commonly a director of films, Ustinov also produces and co-stars in the feature. His dedication to the film appears to emanate from his identification with the characters in the story. He said, "I am an optimist, unrepentant and militant. After all, in order not to be a fool an optimist must know how sad a place the world can be. It is only the pessimist who finds this out anew every day." 
On the novel itself, Melville had been writing poetry for 30 years when he returned to fiction with "Billy Budd" in late 1888. Still unfinished when he died in 1891, it was forgotten. Melvilles biographer accidentally stumbled upon it when going through a trunk of the writers papers in his granddaughters New Jersey home in 1919. Melvilles widow worked to help complete it, and it was finally published in 1924. Over the years other unsatisfactory versions were published, but it wasnt until Melvilles original notes were found that the definitive version was ultimately published in 1962. Ironically, this movie version, made in Europe and England, was released the same year.

It is famous as the last black and white film by Allied Artists. It did poorly due to competition from color films. 

==In other media== Carmela dismisses the notion that the book contains homosexual subtext by stating "I saw the movie...with Terence Stamp."

==References==
 

== External links ==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 