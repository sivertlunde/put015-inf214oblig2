Hurry Sundown (film)
{{Infobox film
| name           = Hurry Sundown
| image          = Hurry_sundown_moviep.jpg
| image_size     = 
| alt            = 
| caption        = Original poster
| director       = Otto Preminger
| producer       = Otto Preminger
| writer         = Horton Foote Thomas C. Ryan Based on the novel by K.B. Gilden  
| narrator       = 
| starring       = Michael Caine Jane Fonda Faye Dunaway Diahann Carroll John Phillip Law
| music          = Hugo Montenegro
| cinematography = Loyal Griggs Milton R. Krasner 
| editing        = Louis R. Loeffler Tony de Zarraga James D. Wells
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 146 minutes
| country        = United States
| language       = English
| budget         = $3,785,000 
| gross          = 
}}

Hurry Sundown is a 1967 American drama film produced and directed by Otto Preminger. It stars Jane Fonda and Michael Caine. The screenplay by Horton Foote and Thomas C. Ryan is based on the 1965 novel of the same title by K.B. Gilden, a pseudonym for married couple Katya and Bert Gilden.

==Plot==
In 1946, bigoted, draft-dodging Henry Warren and his wife Julie Ann, owners of a Northern canning plant, are determined to purchase a large tract of uncultivated farmland in rural Georgia (U.S. state)|Georgia. Two plots remain beyond their grasp, one owned by Henrys cousin Rad McDowell and his wife Lou, the other by black farmer Reeve Scott, whose mother Rose had been Julies mammy. Neither man is interested in selling his land, and they form a partnership to strengthen their claim to it, which infuriates Henry.

When Rose suddenly dies, Henry tries to persuade his wife to charge Reeve with illegal ownership of his property, but local teacher Vivian Thurlow searches the towns records and uncovers proof that Reeve legally registered the deed to his land. Julie, upset with Henrys treatment of their mentally challenged son, decides to leave him and drops her suit against Reeve.

With the help of Ku Klux Klansmen, Henry dynamites the levee above the farms, and Rads oldest child drowns in the ensuing flood, much to Henrys dismay. Rather than admit defeat, Rad and Reeve decide to rebuild their decimated property with the assistance of their neighbors.

==Cast==
* Michael Caine ..... Henry Warren
* Jane Fonda ..... Julie Ann Warren
* Diahann Carroll ..... Vivian Thurlow
* Beah Richards ..... Rose Scott
* Robert Hooks ..... Reeve Scott
* Faye Dunaway ..... Lou McDowell
* John Phillip Law ..... Rad McDowell
* Luke Askew ..... Dolph Higginson George Kennedy ..... Sheriff Coombs
* Burgess Meredith ..... Judge Purcell
* Madeleine Sherwood ..... Eula Purcell
* Frank Converse ..... Reverend Clem De Lavery
* Robert Reed ..... Lars Finchley
* Jim Backus ..... Carter Sillens

==Production== Ingo and, fully expecting it to be another Gone with the Wind, purchased the film rights to the novel for $100,000 eight months prior to its publication. He initially intended to adapt it for a four-and-a-half-hour epic film that would be shown twice-a-day at what would be the highest price scale in the history of American film exhibition, with a top admission of $25 on Friday and Saturday nights. When the book sold a mere 300,000 copies, Preminger decided a less grandiose project might be in order. 
 reader and was familiar with the type of material his employer found appealing. 
 Gene Callahan suggested his home state of Louisiana might be a viable alternative, since the unions there were governed by the more liberal one in Chicago. Baton Rouge and its environs were selected, and Callahans crew began planting cornfields, erecting shanties, and constructing a dam and reservoir containing 17.5 million gallons of water. 

From the start, Preminger and his cast and crew encountered strong resistance from the locals, who resented having a film featuring a biracial friendship made in their midst. Tires were slashed, some actors received telephoned death threats, and a burning cross appeared on one of the sets at 3:00am. The manager of the hotel where everyone was housed advised Preminger mixed bathing would not be permitted in the swimming pools, but grudgingly agreed to designate one "interracial" when the director threatened to vacate the premises and not pay the bill. Eventually armed state troopers were called in to guard the hotel wing where everyone was staying, making them feel as if they were under house arrest. Problems were encountered even in New Orleans, where a racially mixed group was refused admission to Brennans restaurant. Matters came to a head when a convoy of cars and trucks returning to the hotel through a heavily wooded area one evening became the target of a volley of sniper gunfire. Robert Hooks later recalled, "All of us were convinced that we were surrounded by some of the dumbest and meanest people on the face of the earth, to say nothing of being the most cowardly."  

Midway through filming, Preminger had to replace cinematographer Loyal Griggs with Milton R. Krasner when Griggs seriously injured his back. He later banned screenwriter Ryan from the set for talking to Rex Reed for an article published in The New York Times. In it Reed characterized the director as an autocrat who was losing his grip, quoted Michael Caine as saying, "Hes only happy when everybody else is miserable," and claimed Griggs had been fired by Preminger "in a moment of uncontrolled fury." Griggs demanded and received a retraction from the Times.  

Preminger greatly regretted casting Faye Dunaway, with whom he clashed on a regular basis. She felt the director didnt know "anything at all about the process of acting." She resented having him yell at her in public and commented, "Once Ive been crossed, Im not very conciliatory." After filming was completed, she sued Preminger to win her release from the five-film contract she had signed with him. An out-of-court settlement was reached in March 1968. Dunaway later admitted, "It cost me a lot of money to not work for Otto again . . . I regretted paying him   I thought he was awful."  

==Critical reception==
The movie opened to negative reviews from those who felt that Otto Preminger made a film that was not in tune with the problems of the contemporary South. It was also criticized for its out-of-date racial stereotyping and tasteless attitude towards sexuality. 

Roger Ebert of the Chicago Sun-Times called the film "a frustrating case, not good but not particularly bad, with a smokescreen of controversy surrounding it and obscuring its real faults. The trouble with this film . . . is not that its racist and tasteless, but that its naive and dull." 
 pulp fiction" and "an offence to intelligence." 

Time (magazine)|Time observed, "Obviously, Hurry Sundown was intended as a paean to racial justice, but Producer-Director Otto Preminger chooses strange ways to display his big brotherhood. One sequence shows Negro sharecroppers singing a white-eyed hallelujah number reminiscent of those 40s films that pretended to liberalize but patently patronized. Two hours of such cinematic clichés make the viewer intolerant of everyone in the film, regardless of race, creed or color." 

Variety (magazine)|Variety said, "Otto Preminger has created an outstanding, tasteful but hard-hitting, and handsomely-produced film . . . Told with a depth and frankness, the story develops its theme in a welcome, straight-forward way that is neither propaganda nor mere exploitation material." 

Time Out London stated, "The Preminger flair which made The Cardinal so enjoyable, despite its hackneyed script, seems to have deserted him in this lumbering melodrama, put together with the sort of crudely opportunistic style which alternates scenes of the rich folks parading in a stately mansion with shots of the poor sitting down to their humble fare while thumping mood music makes sure you get the point." 

Channel 4 noted, "Preminger wears a liberal heart on his sleeve and then blows his nose on it as heavy-handed sentimentality and nobility dominate this story . . . God, sex, class, guilt, moralizing and Negro spirituals are all thrown into the stew, and youll come away feeling that although its worthy in its ideals, it could have done with a touch less overblown melodrama." 

The Legion of Decency gave the film a "C", "Condemned" rating citing the portrayal of Blacks and portrayal of sex. 

==Awards and nominations== BAFTA Award for Most Promising Newcomer to Leading Film Roles for her performances in this and Bonnie and Clyde.  She was nominated for the Golden Globe Award for New Star of the Year – Actress but lost to Katharine Ross in The Graduate.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 