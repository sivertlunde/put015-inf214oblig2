Run & Jump
{{Infobox film
| name           = Run & Jump
| image          = Run & Jump.jpg
| director       = Steph Green
| producer       = Tamara Anghie Martina Niland
| writer         = Ailbhe Keogan Steph Green
| composer      = Sebastian Pille
| starring       = Maxine Peake Edward MacLiam  Will Forte   Ruth McCabe
| studio         = Samson Films in co-production with Bavaria Pictures 
| distributor    = Sundance Selects (USA) Senator Film  (Germany)
| released       =  
| runtime        = 102 minutes
| country        = Ireland
| language       = English
}}
Run & Jump is a 2013 Irish comedy-drama film starring Maxine Peake, Edward MacLiam, Ruth McCabe and Will Forte. It is directed by Academy Award nominee Steph Green and produced by Tamara Anghie. Run & Jump is a romance film, a voyeuristic reveal of changing family dynamics and a story of physical and emotional recovery.

==Plot==
Conor (Edward MacLiam), a highly skilled woodworker and carpenter, suffers a stroke that caused him to spend a month in a coma and several months in rehab. The stroke leaves him mentally challenged and much changed as he returns home to his wife Vanetia (Maxine Peake) and two children 

Vanetia tries to keep her family together in the wake of Conor’s changed childlike condition. American doctor and neuroscientist Ted Fielding (Will Forte) has received a grant to accompany Conor to his home and stay with the family to document Conor’s recovery and his unfolding interactions with his family. Dr Fielding’s grant funds provide a source of funds to the family as Conor is now unable to produce commercially viable pieces and is compulsively obsessed with manufacturing wooden spheres. Vanetia struggles with the invasiveness of living under Teds ever present video camera and scientific observations.  

Eventually, Vanetia comes to appreciate Dr Fieldings adult company. Dr Fielding’s natural uptight and formal nature begins to soften in the presence of Vanetias free-spirited, fun-loving and optimistic personality and his video cam increasingly captures footage of Vanetia at work about the house. The movie centers on the tensions that emerge between Vanetia and Dr Fielding, Vanetia and her now-childlike husband and Vanetia and her two children. Unfolding events trigger a deadline for Vanetia to make some significant family choices for how and with whom she will live in this new post-stroke environment.

==Cast==
* Maxine Peake as Vanetia
* Edward MacLiam as Conor
* Will Forte as Dr. Ted Fielding
* Ruth McCabe as Conor’s mother
* Sharon Horgan as Tara

==Production==
Run & Jump was shot in County Wicklow and County Kerry in Ireland.

==Reception==
Upon its world premiere at Tribeca Film Festival on April 20, 2013, Run & Jump was met with mostly positive reviews. It has played to international audiences in Germany, Italy, Spain and Brazil (Oct 31, 2013). It was selected as the Best Breakthrough Feature at the Boston Irish Film Festival and screened at that festival on Mar 20, 2014.  Run & Jump has secured distribution at Sundance Selects. 

==References==
  

==External links==
*  

 
 
 
 
 
 
 
 
 