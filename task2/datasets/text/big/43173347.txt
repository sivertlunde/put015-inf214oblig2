The New Gentlemen
 
{{Infobox film
| name           = Les Nouveaux Messieurs
| image          = LesNouveauxMessieurs.jpg
| caption        = Theatrical release poster
| director       = Jacques Feyder
| producer       = 
| writer         = {{Plain list|
* Charles Spaak
* Jacques Feyder}}
| based on     =  
| starring       = {{Plain list|
* Gaby Morlay
* Albert Préjean Henry Roussell}}
| music          = 
| cinematography = {{Plain list|
* Georges Périnal
* Maurice Desfassiaux}}
| editing        = 
| studio        = {{Plain list|
* Films Albatros
* Sequana Films}}
| distributor    = Armor Films
| released       =  
| runtime        = 135 minutes
| country        = France
| language       = Silent (French intertitles)
| budget         = FF 2 million
}}

Les Nouveaux Messieurs ("the new men") is a 1929 French silent film directed by Jacques Feyder. It is a satirical comedy, whose initial release in France was delayed for several months because of objections to its portrayal of the French parliament.

==Cast==
* Gaby Morlay as Suzanne Verrier
* Albert Préjean as Jacques Gaillac Henry Roussell as the Comte de Montoire-Grandpré

==Plot==
Suzanne Verrier, a dancer of mediocre talent at the Paris Opéra, is the mistress of an elderly aristocrat and conservative politician, the Comte de Montoire-Grandpré, who uses his influence to assist her career. She begins a romance with Jacques Gaillac, a left-wing young electrician who is also a committed union organiser.  Gaillac is elected to Parliament and become a minister, leaving him little time for his relationship with Suzanne. When the leftist government abruptly falls, Montoire-Grandprés party returns to power, and he arranges to have Gaillac appointed to a post abroad. Suzanne reluctantly returns to the protection of Montoire-Grandpré.

==Production== MGM to work in Hollywood, but before he left, he agreed to one further film in France for Alexandre Kamenkas Albatros company which was in economic difficulties.  Feyder was offered complete artistic freedom on the project and chose to adapt a recent stage comedy which combined romantic drama with political satire. Lenny Borger, The Russian Emigrés in Paris: 1923-1928;  . pp. 24-27.  

Les Nouveaux Messieurs by Robert de Flers and Francis de Croisset had opened in Paris in February 1925, with Gaby Morlay in the leading role, and it became the biggest success of the season, running for over 500 performances. Feyder and Charles Spaak made an adaptation which sought to dispense with the verbal humour of the play but to translate it into visual terms. The film uses relatively few intertitles and those are employed mainly to convey information. A number of technical devices are used (superimposition of images, "flous" or blurring of the image, speeded-up motion) often to achieve comic effect. 
 Chambre des Deputés, a stylish Parisian town house, and rehearsal areas at the Paris Opera|Opéra).   One of the assistant cameramen working with Georges Périnal was the 22-year-old Marcel Carné. 

==Reception==
The film was ready for a press screening at the end of November 1928 and it met with a mainly favourable reaction from the critics. Dimitri Vezyroglou, "Le politique et son image: les Nouveaux Messieurs, le cinéma français et le politique à la fin des années 20", in Jacques Feyder (1895: numéro hors série). Paris: 1998. pp. 136-137.  One review commented that it was a likable satire of the world of politics which was so amusing that nobody could take offense at it. 

However members of the government rapidly became anxious about the film, not so much because of its satire which was actually more restrained than the successful stage play had been, but rather for its portrayal of the Parliament. Some politicians felt that their fictional equivalents were made up to resemble them rather too realistically; and in one scene a député falls asleep during a debate in the Chamber and dreams happily that the other members have all been transformed into ballerinas in tutus. In this climate of controversy before the end of December the film was refused a distribution licence, for "damaging the dignity of Parliament and ministers".  

By this time Jacques Feyder had left France to take up his job in the United States, and protracted negotiation about the films future continued for several months between the production company and the government. Eventually some cuts were agreed and the film was finally released in Paris in April 1929.   It enjoyed some success in France and was then distributed also in a number of European countries. (A Berlin newspaper voted it the best film of 1929.) However this success, limited also by the arrival of talking pictures, came too late to save the fortunes of the Albatros company which was forced to cease production. 

A correspondent for The New York Times later in the year explained the persistent sensitivities in France: "The fall of the Briand Government last month perhaps gave an extra fillip, in the mind of Paris, to Les Nouveau Messieurs, since the story concerns the collapse of a Cabinet and the substitution of an extreme Radical Ministry. At the houses where it is showing now a careful foreword is thrown on the screen, asserting that no characters are drawn from life and even explaining that the interior of the Chamber of Deputies, where some of the drama and much of the comedy of the picture occur, was constructed at the Feyder studios."

He went on to give a warm appraisal of the films qualities: "Any audience might find the film amusing, but it is so Parisian and so appropriate to these recent days of political crisis here that it is having an exceptional welcome.... With this story actors and director have done remarkably well. It is brightly told and excellently filmed. The photography is modernistic in tone, but always successfully so.... Gaby Morlay, rowdy and dainty by turn, serious and merry, has a picture well to her taste and capacities, which are infinite." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 