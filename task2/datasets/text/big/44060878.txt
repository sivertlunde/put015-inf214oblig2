Samayamaayilla Polum
{{Infobox film
| name           = Samayamaayilla Polum
| image          =
| caption        =
| director       = UP Tomy
| producer       = UP Tomy
| writer         = UP Tomy
| screenplay     = UP Tomy Ambika K. P. Ummer
| music          = Salil Chowdhary
| cinematography =
| editing        =
| studio         = Ajantha Arts Productions
| distributor    = Ajantha Arts Productions
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Ambika and K. P. Ummer in lead roles. The film had musical score by Salil Chowdhary.   

==Cast==
*Prakash
*Urmila Ambika
*K. P. Ummer
*Jagathy Sreekumar
*Mala Aravindan
*Kaviyoor Ponnamma
*Mallika Sukumaran
*Kuthiravattam Pappu

==Soundtrack==
The music was composed by Salil Chowdhary and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Devi Devi || K. J. Yesudas || ONV Kurup ||
|-
| 2 || Mayilukalaadum || K. J. Yesudas, Sabitha Chowdhary || ONV Kurup ||
|-
| 3 || Onnaamthumbi Nee || P Susheela || ONV Kurup ||
|-
| 4 || Shyaama Meghame || K. J. Yesudas || ONV Kurup ||
|}

==References==
 

==External links==
*  

 
 
 


 