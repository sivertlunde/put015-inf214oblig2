Bombhunters
 

{{Infobox film
| name           = Bombhunters
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Skye Fitzgerald
| producer       = Skye Fitzgerald
| writer         = 
| narrator       = 
| starring       = 
| music          = William Campbell 
| cinematography = Skye Fitzgerald
| editing        = Chris G. Parkhurst
| studio         = Spin Film
| distributor    = Indigo Film & Television
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 2006 documentary film produced and directed by Skye Fitzgerald, that documents the effects of unexploded ordnance on Cambodian people, both within their homeland and in the US.     

==Production==

===Funding===
Including a Fulbright Scholarship, 
{{cite journal
|last=Han
|first=Edward J.
|date=August 3, 2004
|title=Local filmmaker receives Fulbright Scholarship for landmine documentary
|journal=The Asian Reporter
|publisher=Pacific Northwest Newsweekly|volume=14
|issue=32
|url=http://www.bombhunters.com/pdfs/AsianReporter.pdf
|accessdate=August 2, 2009}}  
{{cite news
|url=http://www.bombhunters.com/pdfs/StatesmanJournal.pdf
|title=Gervais teacher receives Fulbright scholarship
|last=Lynn
|first=Capi
|date=July 12, 2004
|publisher=Statesman Journal
|accessdate=2009-08-02}}  the project was funded by the Sundance Institute and the United States Institute of Peace, 
{{cite news
|url=http://www.bombhunters.com/pdfs/ORarticle.pdf
|title=Desperation intersects with residue of destruction in Bombhunters documentary
|last=Verzemneiks
|first=Inara
|publisher=The Oregonian
|accessdate=2009-08-02}}  
{{cite web
|url=http://www.filmbaby.com/films/2319|title=Film Overview
|publisher=Filmbaby
|accessdate=2009-08-02}}  including grants from The Office of Weapons Removal and Abatement in the U.S. Department of States Bureau of Political-Military Affairs. 
{{cite news
|url=http://www.accessmylibrary.com/coms2/summary_0286-16140755_ITM
|title=More Grants for Humanitarian Mine Action
|date=July 28, 2006
|publisher=U.S. Department of State
|work=accessmylibrary.com
|accessdate=2009-08-02}} 

===Filming===
Based upon his 2001 trip to Cambodia, 
{{cite news
|url=http://www.bombhunters.com/pdfs/PhnomPen.pdf
|title=Deliberate handling of UXO leading cause of casualties
|last=Woodd
|first=Richard
|date=January 14–17, 2005
|publisher=Phnom Penh Post
|accessdate=2009-08-02}}  Skye Fitzgerald, a graduate of Eastern Oregon University, spent 6 months in Cambodia and Vietnam creating the documentary Bombhunters. 
{{cite news
|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=EOOR&p_multi=EORB&p_theme=eoor&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=11621619D7910138&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM
|title=EOU graduate prepares to showcase documentary
|date=January 16, 2006
|work=newsbank.com
|publisher=East Oregonian
|accessdate=2009-08-02}}  
{{cite news
|url=http://www.eou.edu/ua/news/documents/BombHunters4.pdf
|title=On the trail of the Bomb Hunters
|last=Hancock
|first=Laura
|year=2005
|work=eou.edu
|publisher=Eastern Oregon University
|accessdate=2009-08-02}} 

===Music===
Music was used from the popular Canadian post-rock band Godspeed You! Black Emperor.  The band allowed songs from Yanqui U.X.O. to be used, stating that while they didnt normally allow their music to be used in films, they could align with the social nature of the film.

==Purpose==
This film project documents the effects of UXO* on Cambodian people, both within their homeland and in the US. In particular, Bombhunters documents villagers efforts throughout rural Cambodia as they seek out UXO and attempt to render it safe for sale to the scrap metal industry in order to survive. 

==Release==
When the film had its premiere in November 2005 in Portland Oregon.   This film has been showcased in many film festivals across the globe including; Hot Springs Documentary Film Festival, Rhode Island International Film Festival, Palm Beach International Film Festival, 
{{cite news
|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=PA&p_theme=pa&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=1110D0D3A53E5CE0&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM
|title=2006 PALM BEACH INTERNATIONAL FILM FESTIVAL
|date=April 16, 2006
|work=newsbank.com
|publisher=Palm Beach Daily News
|accessdate=2009-08-02}}  Planet in Focus Environmental Film Festival,  Seattle Human Rights Film Festival, and Northwest Film & Video Festival. 
{{cite web
|url=http://www.wweek.com/editorial/3253/8190/
|title=The 33rd NW Film & Video Festival tells a wide range of tales
|last=Walling
|first=James
|date=November 8, 2006
|publisher=Willamette Week
|accessdate=2009-08-02}} 

In addition to broadcast on US public television the film has aired on the History Channel Asia, Free Speech television in the US and on select national channels in Europe, including national broadcast on TV2 in Sweden.

==Response==
Since completion, the film has been credited by the US Department of State with influencing national legislation in Cambodia that has led to a 50% drop in UXO tampering casualties. 

==References==
 

==External links==
*  at the Internet Movie Database
* 
* 

 
 
 
 
 
 
 