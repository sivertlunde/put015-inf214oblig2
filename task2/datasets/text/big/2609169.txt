The Pink Panther (1963 film)
{{Infobox film
| name           = The Pink Panther
| image          = Pink panther63.jpg
| caption        = Theatrical release poster by Jack Rickard
| director       = Blake Edwards
| producer       = Martin Jurow
| screenplay     = {{Plain list|
* Maurice Richlin
* Blake Edwards
}}
| starring       = {{Plain list|
* David Niven
* Peter Sellers
* Robert Wagner
* Capucine
* Brenda De Banzie
* Colin Gordon
* Fran Jeffries
* Claudia Cardinale
}}
| music          = Henry Mancini
| cinematography = Philip Lathrop
| editing        = Ralph E. Winters
| studio         = Mirisch Company
| distributor    = United Artists
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $10.9 million  }} 
}}
 cartoon character animated by DePatie-Freleng Enterprises.

==Plot==
As a child, Princess Dala receives a gift from her father, the Shah of   in the world. This  . (As the camera moves in, this image comes to life and participates in the opening credits.) When Dala is a young woman, rebels seize power in Lugash and then demand possession of the jewel, but the exiled princess refuses to hand it over.

Several years later, Dala (Claudia Cardinale) relaxes on holiday at an exclusive ski resort in Cortina dAmpezzo. Also staying is a noted British playboy, Sir Charles Lytton (David Niven), who leads a secret life as a jewel thief called "The Phantom" and has his eyes on the Pink Panther. His unwitting American playboy nephew, George (Robert Wagner), follows his uncle to the resort, also hoping to steal the jewel and blame it on the Phantom.

On the Phantoms trail is French police inspector Jacques Clouseau (Peter Sellers) of the Sûreté, who doesnt know his wife Simone (Capucine) is the paramour of Charles and helper in the Phantoms crimes. Clouseau is so clueless and clumsy that while several theft attempts are made at a fancy-dress party, he looks everywhere but the right place. Meanwhile, Simone dodges her husband while trying to avoid George, who has grown enamored of her, and aid Charles, who has grown enamored of Dala and is ambivalent about carrying out the theft.

During a costume party, Sir Charles and his nephew attempt to steal the diamond, only to find the jewel already missing from the safe. In spite of himself, the buffoonish inspector discovers the two in the act, resulting in a car chase throughout the town streets. Despite all odds, Sir Charles and his accomplice George are captured when all the vehicles collide with one another.

Later, Simone informs Dala that Charles wished to call off the theft, and asks her to help in his defense. Dala then reveals that it was she herself who stole the diamond to avoid deportation back to Lugash, and that she has a plan to save Sir Charles from prison. At the trial, Charles and Georges convictions seem inevitable when the defense calls as their lone witness a surprised Clouseau. The barrister asks a series of questions that suggest Clouseau himself could be the Phantom; an unnerved Clouseau pulls out his handkerchief, from which drops the jewel, promptly rendering him unconscious from shock.

As Clouseau is driven away to prison, he is mobbed by a throng of enamored women. Watching from a distance, a regretful Simone expresses fears he will rot in prison; Sir Charles reassures her that when the Phantom strikes again, Clouseau will be exonerated. Sir Charles, Simone, and George drive away to continue their life of crime as Dala leaves to return to her country. Meanwhile, in the police car, the officers express their envy that Clouseau is now the object of affection of young women everywhere. As they ask him with obvious admiration how he committed so many robberies, Clouseaus mood gradually changes: "Well, you know . . . it wasnt easy." The film ends by showing the panther, as a traffic warden, getting run over by the car carrying Clouseau and attempting to chase after it. He pulls himself together and closes the film, holding a "The End" title card

==Cast==
 
* David Niven as Sir Charles Lytton Jacques Clouseau
* Robert Wagner as George Lytton
* Capucine as Simone Clouseau
* Claudia Cardinale (Gale Garnett, uncredited voice) as Princess Dala
* Brenda De Banzie as Angela Dunning
* Colin Gordon as Tucker
* John Le Mesurier as Defense attorney
* James Lanphier as Saloud
* Guy Thomajan as Artoff
* Michael Trubshawe as Felix Townes
* Riccardo Billi as Aristotle Sarajos
* Meri Welles as Monica Fawn Martin Miller as Pierre Luigi
* Fran Jeffries as Ski Lodge singer
 

==Production==
The film was "conceived as a sophisticated comedy about a charming, urbane jewel thief, Sir Charles Lytton" (played by Niven); Peter Ustinov was "originally cast as Clouseau, with Ava Gardner as his faithless wife in league with Lytton."    After Gardner backed out&mdash;the Associated Press reported in November 1962 it was because The Mirisch Company wouldnt meet all her demands &mdash;Ustinov also left the project, and Blake Edwards then chose Sellers to replace Ustinov.  Janet Leigh turned the lead female role down as she would have been away from the United States too long. 

The film was initially intended as a vehicle for Niven, as evidenced by his top billing.  As Edwards shot the film, employing multiple takes of improvised scenes, it became clear Sellers, originally considered a supporting actor, was stealing the scenes and resulted in his continuation throughout the films sequels. When presenting at a subsequent Oscar Awards ceremony, Niven requested his walk-on music be changed from the "Pink Panther" theme, as "that was not really my film." 
 Foreign Correspondent (1940).
 Meglio Stasera (It Had Better Be Tonight)" while she danced provocatively around a fireplace.

==Reception==
The movie was a popular hit earning estimated North American rentals of $6 million.  

Bosley Crowther of The New York Times wrote "seldom has any comedian seemed to work so persistently and hard at trying to be violently funny with weak material"; he called the script a "basically unoriginal and largely witless piece of farce carpentry that has to be pushed and heaved at stoutly in order to keep on the move."  Variety (magazine)|Variety was much more positive, calling the film "intensely funny" and "Sellers razor-sharp timing ... superlative." 

In a 2004 review of "The Pink Panther Film Collection", a DVD collection that included The Pink Panther, The A.V. Club wrote: 
 "Because the later movies were identified so closely with Clouseau, its easy to forget that he was merely one in an ensemble at first, sharing screen time with Niven, Capucine, Robert Wagner, and Claudia Cardinale. If not for Sellers hilarious pratfalls, The Pink Panther could be mistaken for a luxuriant caper movie like Topkapi (film)|Topkapi, which is precisely what makes the movie so funny. It acts as the straight man, while Sellers gets to play mischief-maker." 

The film was selected in 2010 to be preserved by the Library of Congress as part of its National Film Registry.      

The film received 90% on the popular review site Rotten Tomatoes. 

;American Film Institute Lists
* AFIs 100 Years...100 Movies&nbsp;– Nominated 
* AFIs 100 Years of Film Scores&nbsp;– #20

==Soundtrack album==  soundtrack album 100 Years of Film Scores.

:All songs written by Henry Mancini, except where noted.
 
 
;Side one
# "The Pink Panther Theme"&nbsp;– 2:35
# "It Had Better Be Tonight (Meglio Stasera)" (Instrumental)&nbsp;– 1:44
# "Royal Blue"&nbsp;– 3:09
# "Champagne and Quail"&nbsp;– 2:45
# "The Village Inn"&nbsp;– 2:34
# "The Tiber Twist"&nbsp;– 2:47
 
;Side two
# "It Had Better Be Tonight (Meglio Stasera)" (Vocal) (Henry Mancini - Johnny Mercer)&nbsp;– 1:56
# "Cortina"&nbsp;– 1:52
# "The Lonely Princess"&nbsp;– 2:25
# "Something for Sellers"&nbsp;– 2:45
# "Piano and Strings"&nbsp;– 2:34
# "Shades of Sennett"&nbsp;– 1:22
 

==References==
Notes
 

Further reading
*  

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 