Eyyvah Eyvah
{{Infobox film
| name           = Eyyvah Eyvah
| image          = EyvahEyvahFilmPoster.jpg
| image size     = 190px
| caption        = Film poster
| director       = Hakan Algül
| producer       = Necati Akpinar
| writer         = Ata Demirer
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = Besiktas Kültür Merkezi
| distributor    = Uip filmcilik
| released       =  
| runtime        =  
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $12,011,619
}}

Eyyvah Eyvah is a 2010 Turkish comedy film, directed by Hakan Algül, which stars Ata Demirer as a young clarinet player who travels to Istanbul in search of his estranged father. The film, which went on nationwide general release across Turkey on  , is one of the highest-grossing Turkish films of 2010 and was followed by the sequel Eyyvah Eyvah 2 (2011).

==Production==
The film was shot on location in Istanbul and Çanakkale, Turkey.   

== Plot ==
Hüseyin (Ata Demirer) is a young man living with his grandparents in a village in Turkey’s Thracian region. Two things are of great importance in Hüseyin’s life: his clarinet and his fiancee. However, one day Hüseyin is forced to go to İstanbul and leave behind his beloved village. In the big city, Hüseyin will receive the biggest support from his clarinet and later from a bar singer called Firuzan (Demet Akbağ). Firuzan, who storms İstanbul’s night clubs with her songs, already leads a very colorful and highly complicated life, which, with Hüseyin’s inclusion, gets all the more colorful with comedy and action.

==Release==
The film opened across Germany on   and across Turkey and Austria on   at number one in the Turkish box office chart with an opening weekend gross of $1,597,709.   

{| class="wikitable sortable" align="center" style="margin:auto;"
|+ Opening weekend gross
|-
!Date!!Territory!!Screens!!Rank!!Gross
|-
|  
| Turkey
| 350
| 1
| $1,597,709
|-
|  
| Germany
| -
| -
| -
|-
|  
| Austria
| 6
| 18
| $30,622
|}

==Reception==

===Box Office===
The film was number one at the Turkish box office for four weeks running and has made a total gross of $12,011,619. 

==References==
 

==External links==
*  
*   interview with Todays Zaman
*  

 
 
 
 
 
 
 