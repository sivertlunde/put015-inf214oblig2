Chelsea Girls
 
{{Infobox film
| image          = Chelseagirlsposter.jpg
| name           = Chelsea Girls
| writer         = Ronald Tavel Andy Warhol Ondine Gerard Malanga Eric Emerson Mary Woronov Mario Montez Ingrid Superstar International Velvet
| director       = Andy Warhol Paul Morrissey
| producer       = Andy Warhol
| cinematography = Andy Warhol Paul Morrissey (uncredited)
| released       =  
| runtime        = 210 min. (approx.)
| Filmed         = Summer 1966 – fall 1966
| movie_language = English
| country        = United States
| budget         = $3,000 (approx.)
| music          = The Velvet Underground
}} experimental underground split screen, accompanied by alternating soundtracks attached to each scene and an alternation between black-and-white and color photography. The original cut runs at just over three hours long.  
 Chelsea Girl. Chelsea Girls", Francis Bacon. 

==Production==
According to script-writer Ronald Tavel, Warhol first brought up the idea for the film in the back room of Maxs Kansas City, Warhols favorite nightspot, during the summer of 1966. In Ric Burns documentary film Andy Warhol, Tavel recollected that Warhol took a napkin and drew a line down the middle and wrote B and W on opposite sides of the line; he then showed it to Tavel, explaining, "I want to make a movie that is a long movie, that is all black on one side and all white on the other." Warhol was referring to both the visual concept of the film, as well as the content of the scenes presented.
 International Velvet and Eric Emerson. According to Burns documentary, Warhol and his companions completed an average of one 33-minute segment per week.
 Ondine (right) in the final scene of Chelsea Girls. This still comes from the 2003 Italian DVD print of the film.]]

Once principal photography wrapped, Warhol and co-director Paul Morrissey selected the twelve most striking vignettes they had filmed and then projected them side-by-side to create a visual juxtaposition of both contrasting images and divergent content (the so-called "white" or light and innocent aspects of life against the "black" or darker, more disturbing aspects.) As a result, the 6½ hour running time was essentially cut in half, to 3 hours and 15 minutes. However, part of Warhols concept for the film was that it would be unlike watching a regular movie, as the two projectors could never achieve exact synchronization from viewing to viewing; therefore, despite specific instructions of where individual sequences would be played during the running time, each viewing of the film would, in essence, be an entirely different experience.
 Ondine as he called himself, as well as a segment featuring Mary Woronov entitled "Hanoi Hannah," one of two portions of the film scripted specifically by Tavel.

Notably missing is a sequence Warhol shot with his most popular superstar Edie Sedgwick which, according to Morrissey, Warhol excised from the final film at the insistence of Sedgwick herself, who claimed she was under contract to Bob Dylans manager, Albert Grossman, at the time the film was made. Sedgwicks footage has been used in another Warhol film "Afternoon."

==Cast==
The cast of the film is largely made up of persons playing themselves, and are credited as so:
 
 
*Brigid Berlin as herself (The Duchess)
*Nico as herself Ondine as himself (Pope)
*Ingrid Superstar as herself
*Randy Bourscheidt as himself
*Angelina Pepper Davis as herself
*Christian Aaron Boulogne as himself
*Mary Woronov as Hanoi Hannah
*Ed Hood as himself
*Ronna as herself International Velvet as herself
 
*Rona Page as herself
*Rene Ricard as himself
*Dorothy Dean as herself
*Patrick Fleming as himself
*Eric Emerson as himself
*Donald Lyons as himself
*Gerard Malanga as Son
*Marie Menken as Mother
*Arthur Loeb as himself
*Mario Montez as Transvestite
 

==Critical reception==
  in color photography on the left side, and black & white photography on the right.]]Although the film garnered the most commercial success of Warhols films, reaction to it was mixed. In the UK it was refused a theatrical certificate in 1967 by the BBFC.

Roger Ebert reviewed the film in June 1967, and had a negative response to it, granting it one star out of four. In his review of the film, he stated "...what we have here is 3½ hours of split-screen improvisation poorly photographed, hardly edited at all, employing perversion and sensation like chili sauce to disguise the aroma of the meal. Warhol has nothing to say and no technique to say it with. He simply wants to make movies, and he does: hours and hours of them."  

Kenneth Baker of the San Francisco Chronicle reviewed the film in honor of its screening in the San Francisco Bay Area in 2002, and gave the film a positive review, stating "The tyranny of the camera is the oppression The Chelsea Girls records and imposes. No wonder it still seems radical, despite all we have seen onscreen and off since 1966." 

Jonathan Rosenbaum also gives the film positive review, stating that "the results are often spellbinding; the juxtaposition of two film images at once gives the spectator an unusual amount of freedom in what to concentrate on and what to make of these variously whacked-out performers." 

TV Guide reviewed the film in December 2006, granting it four stars, calling it "fascinating, provocative, and hilarious" and "a film whose importance as a 1960s cultural statement outweighs any intrinsic value it may have as a film." 
 aggregator Rotten Tomatoes has the film ranked as 67% "fresh", or positive, out of nine collected reviews. 

==Availability==
===Home video=== screen tests, which have since been released on DVD)  have never seen home video releases in the United States. In Europe, however, a handful of Warhols films were released on DVD, including a short-lived DVD print of Chelsea Girls which was available in Italy for some time. This Italian DVD print, which is the films only official home video release, was released on September 16, 2003. 

===Museum screenings===
While the film is unavailable for personal purchase, it is often screened at art museums, and has been shown at The Museum of Modern Art  (which owns a rare print of the film reels) as well as The Andy Warhol Museum in Pittsburgh, Pennsylvania. The film was screened in San Francisco for the first time in nearly twenty years at Castro Theater in April 2002.  A screening was also held May 21, 2010 at the Seattle Art Museum.  Screened at the Varsity Theater in Chapel Hill, NC on November 18th 2010 by The Ackland Art Museum and The Interdisciplinary Program in Cinema of The University of North Carolina at Chapel Hill.  A screening was done at the High Museum of Art in Atlanta, GA on November 5, 2011 as a part of their Masters of Modern Film series. 

==See also==
* Andy Warhol filmography
* Hotel Chelsea
* Reality films
* Arthouse cinema

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 