Half Ticket
{{Infobox Film
| name = Half Ticket
| image = Half Ticket.jpg
| image_size = 
| caption = 
| director = 
| producer = 
| writer = 
| narrator =  Pran
| music = Salil Choudhury
| cinematography = 
| editing = 
| distributor = 
| released = 1962
| runtime = 
| country = India
| language = Hindi
| budget = 
}} Helen and Pran (actor)|Pran.

==Synopsis==

Vijay (Kishore Kumar) is the good-for-nothing son of a rich industrialist, who becomes bored of his father’s constant railing and the efforts to marry him off, with the intention of getting him “settled” in life.  So Vijay walks out of his home and decides to leave for Bombay and start life afresh in the big city, but he doesn’t have enough money to get himself a ticket!

Vijay gets a burst of inspiration from a plump child called Munnah, who is waiting in line with his mother (Tun Tun), and decides to pass himself off a child in order to get the eponymous half-ticket.
 mule for a diamond smuggler (Pran (actor)|Pran) without his knowledge.  On the train, Vijay also meets Rajnidevi (Madhubala) and falls in love with her.

The rest of the film follows Vijays exploits as he avoids capture by the diamond smuggler and his girlfriend (Shammi (actress)|Shammi), romances Rajnidevi while avoiding her auntie-ji (Manorama (Hindi actress)|Manorama), and reunites with his father.

The film is based on the Hollywood movie Youre Never Too Young.

Some hilarious scenes:

Vijay at Interview- prompted for "MASKA" (Flattery).

Dinner at Vijays home, called by his father to Board of Directors.

Would-be Father-in-Law comes to see Vijay as a bridegroom for her daughter.

==Cast==

* Kishore Kumar.... Vijaychand vald Lalchand vald Dhyanchand vald Hukumchand alias Munna / Vijays Mother
* Madhubala.... Rajnidevi / Asha
* Pran (actor)|Pran.... Notorious Thief Raja Babu aka Chacha
* Shammi.... Lily Raja Babus Mistress Manorama .... Ashas Aunty Helen .... Stage Dancer in the song Woh ek Nigah (Special Appearance)
* Pradeep Kumar .... Special Appearance
* Moni Chaterjee .... Seth Lalchand Vijays Father
* Tun Tun .... Real Munnas Mother

==Soundtrack==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| Chaand Raat Tum Ho Saath
| Lata Mangeshkar and Kishore Kumar
| 
|-
|Aankhon Mein Tum Geeta Dutt and Kishore Kumar
|
|-
|Chill Chill Chilla Ke Kishore Kumar Picturized on Kishore Kumar as he is dressed as Munna.
|-
|Woh Ek Nigah Kya Mili Lata Mangeshkar and Kishore Kumar Picturized on Helen and Kishore Kumar with Pran (actor)|Pran.
|-
|Aake Seedhi Lagi Dil Pe Kishore Kumar and Kishore Kumar Picturized on Pran with Kishore in drag.
|-
|Are Lelo Ji Lelo Kishore Kumar
|
|-
|Are Wah Re Mere Malik Kishore Kumar
|
|}

==External links==
*  
*  

 
 
 


 