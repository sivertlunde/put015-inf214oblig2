Inland with Sturt
 
{{Infobox film
| name           = Inland with Sturt
| image          = 
| image size     = 
| caption        = 
| director       = Hugh McInnes
| producer       = 
| writer         = 
| based on       =  Grant Taylor Rod Taylor
| music          = 
| cinematography = 
| editing        = 
| distributor    = Film Australia
| prod. company  = 
| released       = 1951
| runtime        = 20 mins
| country        = Australia
| language       = 
| budget         = 
}}
 Murrumbidgee and Murray Rivers. The re-enactment was part of Australias 1951 Commonwealth Jubilee Celebrations, commemorating 50 years of Federation.
 Grant Taylor Duntroon played the men who travelled with them. 

The expedition took several weeks, travelling from Sydney to Adelaide. The crew travelled by road to the town of Maude, New South Wales|Maude, then entered the Murrumbidgee River in a whaleboat and went by water to Goolwa, after which they drove to Adelaide. All the time they wore 1830 period costume.

An extensive support crew accompanied the whaleboat from land, including Talbot Duckmanton and a team from ABC radio who would present a nightly report. The trip was also covered extensively by local media and seen by an estimated 300,000 people along the way. 
 Charles Tingwell, pulled out to act in Kangaroo (1952 film)|Kangaroo (1952). 

==Cast== Grant Taylor as Captain Charles Sturt
*Rod Taylor as George Macleay
*Pat Trost
*Ian Gilmore
*Roy Pugh
*Captain Jim Laughlin
*Ron Wells
*Brien Forward

==References==
 

==External links==
* 
*  and   at The Duntroon Society Newsletter November 2000 and April 2001.
*  at National Film and Sound Archive
*  at The Rod Taylor Site

 
 
 

 