To levende og en død (film)
{{Infobox film
| name           =To levende og en død 
| image          = 
| image size     =
| caption        =
| director       = Gyda Christensen   Tancred Ibsen
| producer       = 
| writer         = Sigurd Christiansen (novel)   Tancred Ibsen
| narrator       =
| starring       = Hans Jacob Nilsen   Unni Torkildsen   Jan Vaage   Toralf Sandø
| music          = Carsten Carlsen
| cinematography = Olle Comstedt
| editing        =
| distributor    = 
| released       = 6 September 1937
| runtime        =
| country        = Norway Norwegian
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Norwegian thriller To levende og en død by Sigurd Christiansen. A post office worker is left wrestling with his conscience following a robbery at his workplace.

==Cast==
* Hans Jacob Nilsen - Erik Berger, postfunksjonær 
* Unni Torkildsen - Helene Berger 
* Jan Vaage - Knut, deres sønn 
* Toralf Sandø - Lydersen, postfunksjonær 
* Lauritz Falk - Rognås, bankfunksjonær 
* Hans Bille - Postmesteren 
* Hilda Fredriksen - Pensjonatvertinnen 
* Joachim Holst-Jensen - Engelhardt 
* Thoralf Klouman - Postdir. 
* Guri Stormoen - Ekspiditrisen  Axel Thue - Politifullmektig 
* Einar Vaage - Kvisthus 
* J. Barclay-Nitter - En postfullmektig 
* Elsa Sandø - Fru Lydersen 
* Knut Jacobsen - Doktoren 
* Kolbjørn Brenda - En postfunksjonær 
* Marie Hedemark - Pensjonær 
* Ingse Vibe - Pensjonær 
* Signe Ramberg - Pensjonær 
* Alfred Solaas - Pensjonær

==Bibliography==
* Soila, Tytti & Söderbergh-Widding, Astrid & Iverson, Gunnar. Nordic National Cinemas. Routledge, 1998.

==External links==
* 

 
 
 
 
 
 
 
 


 