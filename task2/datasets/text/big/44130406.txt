Komaram (film)
{{Infobox film 
| name           = Komaran
| image          =
| caption        =
| director       = JC George
| producer       = Prabhakaran Thattiriyattu
| writer         = JC George
| screenplay     = JC George
| starring       = Jayan Mammootty Nedumudi Venu Beena
| music          = Kottayam Joy
| cinematography = G Kutty
| editing        = K Narayanan
| studio         = Thathiriyattu Films
| distributor    = Thathiriyattu Films
| released       =  
| country        = India
| language       = Malayalam Language|Malayalam}}
 1982 Cinema Indian Malayalam Malayalam film, directed by JC George and produced by Prabhakaran Thattiriyattu. The film stars Jayan, Mammootty, Nedumudi Venu and Beena in lead roles. The film had musical score by Kottayam Joy.   

==Cast==
 
*Jayan
*Mammootty
*Nedumudi Venu
*Beena Sreenivasan
*Kalaranjini
*Balan K Nair
*CC Varghese
*Jalaja
*Kundara Johny
*Lalithasree
*Mala Aravindan
*Raji
*Ravi Menon Seema
 

==Soundtrack==
The music was composed by Kottayam Joy and lyrics was written by Trichur Biju. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aamala keri || Vani Jairam || Trichur Biju || 
|-
| 2 || Sheethala sharathkaala Sandhyayil || K. J. Yesudas || Trichur Biju || 
|}

==References==
 

==External links==
*  

 
 
 

 