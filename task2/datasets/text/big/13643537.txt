My Father
{{Infobox film
| name           = My Father
| image          = My Father (2007).jpg
| caption        = Poster for My Father (2007)
| film name      = {{Film name
| hangul         =    
| hanja          = 
| rr             = Mai padeo
| mr             = Mai p‘adŏ}}
| director       = Hwang Dong-hyuk
| producer       = Seok Myeong-hong Choe Yun
| writer         = Hwang Dong-hyuk Kim Yeong-cheol Daniel Henney
| music          = Gang Ho-jeong
| cinematography = Choe Hyeon-gi
| editing        = Lee Sang-min Ham Sung-won
| distributor    = Cineline II Co. Ltd.
| released       =  
| runtime        = 109 minutes
| country        = South Korea
| language       = Korean English
| budget         = 
| gross          = $6,063,158 
}}
My Father ( ) is a 2007 South Korean film. The film, which is based on a true story, is about an adopted son who is searching for his biological parents in South Korea. During his search he meets his real father, a condemned murderer on death row. Daniel Henney plays the lead role of James, who works as a volunteer in the United States armed forces in Korea. He asks questions of why his father is on death row and finds out things that he always wanted to know. Then he finds more and more truths unravel about his father and his life.  

The release of the film inspired controversy because the family of the fathers victims did not support its production. In its first week on release it topped the South Korean box office sales charts. 

The adopted son on whom the story is based is Aaron Bates, a licensed insurance broker for his familys insurance brokerage, Insurance Services of America. He lives and works in Arizona and is raising two boys of his own with his wife. 

==Cast== Kim Yeong-cheol... Hwang Nam-cheol
* Daniel Henney... James Parker
* Ahn Suk-hwan... Jang Min-ho
* Richard Riehle... John Parker 
* Ilene Graff...  Nancy Parker
* Kim In-kwon... Shin Yo-seob
* Choi Jong-ryul... Moon Shin-bu
* Jeon Guk-hwan... Kim
* Lee Sang-hee... Park
* Son Jin-hwan... Thief
* Bae Ho-geun... Hyeon-shik
* Bak Gyeong-geun... Haeng Sang-nam

==References==
 

==External links==
* http://www.myfather2007.co.kr/
*  
*   
*  

 
 
 


 