Dom (film)
Dom Polish for House) is a 1958 Polish short film directed by Walerian Borowczyk and Jan Lenica. The short combines live action with various animation techniques, such as stop motion, cut-out animation and pixilation.

==Plot==
A woman (played by Borowczyks wife Ligia Branice) has a series of surreal, dream-like hallucinations and encounters within the confines of a lonely apartment building. Some of these bizarre occurrences include various abstract objects appearing in a room, two men engaging in fencing and martial arts, a man entering and leaving a room repeatedly, and a living wig destroying several items on a table. The film ends with the woman passionately kissing a male mannequins face before it crumbles to pieces.

==Awards== The Violinist. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 