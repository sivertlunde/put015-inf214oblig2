Secretary (2002 film)
 
{{Infobox film
| name           = Secretary
| image          = Secretarymovpost.jpg
| caption        = Theatrical release poster
| director       = Steven Shainberg
| producer       = Andrew Fierberg Amy Hobby Steven Shainberg
| screenplay      =  Steven Shainberg
| story          = Erin Cressida Wilson
| based on       =  
| starring       = James Spader Maggie Gyllenhaal
| music          = Angelo Badalamenti
| cinematography = Steven Fierberg
| editing        = Pam Wise
| studio         = Twopoundbag Productions Double A Films SloughPond Co. Lionsgate
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $4 million
| gross          = $9.3 million
}} erotic romance film directed by Steven Shainberg and starring Maggie Gyllenhaal as Lee Holloway and James Spader as E. Edward Grey. The film is based on a short story from Bad Behavior by Mary Gaitskill,  and explores the relationship between a sexually dominant man and his submissive secretary.

==Plot==
Lee Holloway (Maggie Gyllenhaal), the socially awkward and emotionally sensitive youngest daughter of a dysfunctional family, adjusts to normal life after having been hospitalized following an incident of dangerous Self-injury|self-harm. She learns to type, and begins to work as a secretary for an eccentric attorney, E. Edward Grey (James Spader), who hires her despite her stilted social skills, unprofessional appearance and her scoring higher than anyone hes ever interviewed, therefore being highly overqualified. Edward explains that its dull work and they dont use computers; however, Lee remarks that she is okay with these conditions. 
 typos and sexually aroused by her submissive behavior. After he confronts her about her propensity for self-injury and commands that she never hurt herself again, the two embark on a BDSM relationship. Lee experiences a sexual and personal awakening, and she falls deeply in love. Edward, however, displays insecurity concerning his feelings for Lee, and he shows shame and disgust over his sexual habits. During this period of exploration with Edward, Lee has also been attempting to have a more conventional boyfriend in Peter (Jeremy Davies), even engaging in lukewarm sex with him. After a sexual encounter in Edwards office, he fires Lee. 

After Lee is fired from her job, Peter proposes to Lee, who reluctantly agrees to marry him. However, while trying on her wedding gown, she leaves and runs to Edwards office where she declares her love for him. Edward, still uncertain about their relationship, tests Lee by commanding her to sit in his chair without moving her hands or feet until he returns. Lee willingly complies. Hours pass, as several family members and acquaintances individually visit Lee to alternately attempt to dissuade or encourage her while Edward watches from afar, completely taken by Lees compliance, and his resulting sexual arousal. Because of Lees inability to leave the office, she has gained news coverage from the media, which they believe to be a Hunger strike. After three days, Edward returns to the office and takes Lee to a room upstairs where he bathes and nurtures her. The pair marry and happily continue their BDSM|dominant-submissive relationship.

==Cast==
* James Spader as E. Edward Grey, Lees employer and sexually dominant lover
* Maggie Gyllenhaal as Lee Holloway, the eponymous submissive secretary
* Jeremy Davies as Peter
* Lesley Ann Warren as Joan Holloway
* Stephen McHattie as Burt Holloway
* Jessica Tuck as Tricia OConnor
* Patrick Bauchau as Dr. Twardon
* Amy Locane as Lees sister
* Oz Perkins as Jonathan
* Michael Mantell as Stewart
* Sabrina Grdevich as Allison
* Ezra Buzzington as Typing teacher

==Production==
 
Many changes were made from Mary Gaitskills original short story, which was significantly expanded and given greater depth in order to be made into a feature-length film. Lines of dialogue were changed; Lees statement, "Im so stupid" became a fantasy-sequence cry of "Im your Secretary," which the director thought far more "celebratory." Shainberg, Steven (2004), audio commentary to Secretary.  Additionally, the ending of the story was changed to give a more positive outcome to the relationship. Steven Shainberg stated that he wished to show that BDSM relationships can be normal and was inspired by the film My Beautiful Laundrette, which he feels normalized gay relationships for audiences in the 1980s.  Gwyneth Paltrow was originally cast as Lee Holloway, but for unknown reasons, Maggie Gyllenhaal replaced her. 

A central component to the film, the office spaces of Edward and Lee, took form after two years of planning by Shainberg and production designer Amy Danger, who had collaborated with Shainberg on several projects. John Calhoun. (2002, October). Spank You Very Much. Entertainment Design, 36(10), 8-10. Retrieved April 1, 2011, from Research Library Core. (Document ID: 204894041).  The desire to have the office feel homemade and express Edwards interest in the growing of plants led Danger to juxtapose a natural decor in the office with a predominantly artificial outside world.  Speaking of her choices Danger compares the office with the rest of the films locations: "All the materials I used   were natural: natural wood, bamboo, ironwork ... If I wasnt using natural materials, it was natural colors, like   the botanical wallpaper."  In contrast, "everything   was fake ... I covered Lees house in plastic sheeting, and used artificial, manufactured colors." 
Although the interior sets were carefully constructed, the filmmakers did face some location-related challenges. Notably, in one instance the filmmakers accidentally obtained shooting rights for the wrong park. Gyllenhaal encouraged them to hastily shoot the required park scene anyway, without permission, while crew members distracted the local police. 

Speaking about Secretarys tone and atmosphere, Danger says "With this S&M material, we could go into a dark place... Steve and I wanted the total opposite: that the nature of this relationship freed   to be their natural selves."  Because of this atmosphere, Danger says "Everybody kept saying, When are we going back to the office? It was funny, because the rooms werent any smaller in the house, and it wasnt any more difficult to shoot. It was because you wanted to be in that space." 

==Distribution==
The film was initially screened at several 2002 film festivals and had its domestic theatrical release on September 20, 2002, and in various foreign markets in 2003 and 2004. 

==Home release== region 1 DVD was released on April 1, 2003. In the UK, a version by Tartan Video was released on January 5, 2004, followed by a budget edition by Prism Leisure on February 7, 2005. A UK Blu-ray Disc release was scheduled for September 13, 2010. 
 trailer and TV spots, Curricula Vitae" and an audio commentary by director Steven Shainberg and writer Erin Cressida Wilson. 

==Reception==

===Critics===
The film was generally received positively by critics. As of May 2011, it has a rating of 74% on Rotten Tomatoes based on 141 reviews.  Many critics noted the films original take on themes of sadomasochism, with Roger Ebert saying that the film "approaches the tricky subject...with a stealthy tread, avoiding the dangers of making it either too offensive, or too funny".  Aint It Cool News commented: "Perhaps there is something bold about saying that pain can bring healing as long as its applied by the right hand, but even that seems obvious and even normal thanks to  ." 

===Box office===
Secretary grossed $4,059,680 domestically and $5,244,929 internationally, to a total of $9,304,609.

==Awards and Nominations==
Secretary was nominated for a number of awards and won several, with numerous wins for Maggie Gyllenhaals breakthrough performance.

{| class="wikitable sortable"
|- Year
!align="left"|Ceremony Category
!align="left"|Recipients  Result
|- 2002
|7th 7th Satellite Awards Best Actress - Musical or Comedy Maggie Gyllenhaal
| 
|- 9th Empire Awards Best Actress Maggie Gyllenhaal
| 
|- 18th Independent Spirit Awards Best Feature
|Secretary
| 
|- Best Female Lead Maggie Gyllenhaal
| 
|- Best First Screenplay Erin Cressida Wilson
| 
|- 60th Golden Globe Awards Best Actress - Musical or Comedy Maggie Gyllenhaal
| 
|- 2002 National Society of Film Critics Awards Best Actress Maggie Gyllenhaal
| 
|- 2003 MTV Movie Awards Best Breakthrough Female Performance Maggie Gyllenhaal
| 
|- Boston Society of Film Critics Awards 2002 Best Actress Maggie Gyllenhaal
| 
|- Central Ohio Film Critics Association Best Actress Maggie Gyllenhaal
| 
|- Chicago Film Critics Association Awards 2002 Most Promising Performer Maggie Gyllenhaal
| 
|- Florida Film Critics Circle Awards 2002 Pauline Kael Breakout Award Maggie Gyllenhaal
| 
|- Gotham Independent Film Awards 2002 Breakthrough Performer Maggie Gyllenhaal
| 
|- Las Vegas Film Critics Society Awards 2002 Best Actress Maggie Gyllenhaal
| 
|- Locarno International Film Festival Golden Leopard Award Steven Shainberg
| 
|- National Board of Review Awards 2002 Breakthrough Performance - Female Maggie Gyllenhaal
| 
|- Online Film Critics Society Awards 2002 Best Actress Maggie Gyllenhaal
| 
|- Best Breakthrough Performance Maggie Gyllenhaal
| 
|- Phoenix Film Critics Society Awards 2002 Best Actress Maggie Gyllenhaal
| 
|- Best Newcomer Maggie Gyllenhaal
| 
|- San Diego Film Critics Society Awards 2002 Best Actress Maggie Gyllenhaal
| 
|- Sundance Film Festival Special Jury Prize - Originality
|Secretary
| 
|- Toronto Film Critics Association Awards 2002 Best Actress Maggie Gyllenhaal
| 
|- Vancouver Film Critics Circle Awards 2002 Best Actress Maggie Gyllenhaal
| 
|- Washington D.C. Area Film Critics Association Awards 2002 Best Actress Maggie Gyllenhaal
| 
|}

==Soundtrack==
 –style cover art for the Secretary soundtrack album]]

The films  s "Im Your Man" and Lizzie Wests "Chariots Rise". 

The song "Chariots Rise" was changed slightly for the film, with the lyric "what a fool am I, to fall so in love" changed to "what grace have I, to fall so in love". 

; Track listing
All tracks by Angelo Badalamenti unless otherwise stated.

# "Im Your Man" – Leonard Cohen
# "Main Title"
# "Feelin Free"
# "Snow Dome Dreams"
# "Bathing Blossom"
# "Seeing Scars"
# "Loving to Obey"
# "Office Obligations"
# "The Loving Tree"
# "Orchids"
# "Secretarys Secrets"
# "Chariots Rise" – Lizzie West

==See also==
* Sadism and masochism in fiction
* Nudity in film

==References==
 

==Further reading==
*  

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 