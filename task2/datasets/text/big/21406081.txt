The Woman Cop
{{Infobox film
| name           = The Woman Cop
| image          = La-femme-flic-poster.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Yves Boisset
| producer       = Alain Sarde
| writer         = Yves Boisset Claude Veillot
| narrator       = 
| starring       = Miou-Miou Jean-Marc Thibault Jean-Pierre Kalfon François Simon (actor)|François Simon
| music          = Philippe Sarde
| cinematography = Jacques Loiseleux
| editing        = Albert Jurgenson
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
The Woman Cop ( ) is a 1980 French film directed by Yves Boisset.

==Cast and roles==
* Miou-Miou - Inspector Corinne Levasseur
* Jean-Marc Thibault - Commissaire Porel
* Leny Escudéro - Diego Cortez
* Jean-Pierre Kalfon - Backmann, the director of the MJC
* François Simon (actor)|François Simon - Doctor Godiveau
* Alex Lacast - Inspector Simbert
* Niels Arestrup - Dominique Allier, the photographer
* Henri Garcin - Le procureur
* Philippe Caubère - Abbot Henning
* Roland Amstutz - M. Muller
* Roland Bertin - Substitut Berthot
* Roland Blanche - Inspector Roc
* Stéphane Bouy - Commissaire Bonnard
* Philippe Brizard - Juge dinstruction in the South
* Gérard Caillaud - Juge dinstruction in the North

==External links==
*  

 
 
 
 
 
 
 
 