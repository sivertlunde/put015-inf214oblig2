The Revolutionary
{{Infobox film
| name           = The Revolutionary
| image          = The Revolutionary.jpg
| image_size     =
| alt            = 
| caption        =  Paul Williams
| producer       = Edward R. Pressman
| writer         = Hans Koning (novel & screenplay)
| narrator       = 
| starring       = Jon Voight Seymour Cassel Robert Duvall
| music          = Michael Small
| cinematography = Brian Probyn
| editing        = Henry Richardson
| studio         = 
| distributor    = United Artists
| released       = 15 July 1970 (USA)
| runtime        = 100 min.
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1970 film Paul Williams.  The screenplay was written by Hans Koning (credited as Hans Koningsberger), based on his novel of the same name.  It was the film debut for actor Jeffrey Jones. 

==Plot==
The film deals with campus life and protests during the Vietnam War.

==Main cast==
{| class="wikitable" border="1"
|-
! Actor
! Role
|-
| Jon Voight || A
|-
| Seymour Cassel || Leonard II
|-
| Robert Duvall || Despard
|-
| Alan Tilvern || Sid
|-
| Jennifer Salt || Helen
|-
| Lionel Murton || Prof
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 