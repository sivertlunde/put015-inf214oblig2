At the Circus
{{Infobox film
| name           = At the Circus
| image          = Marxcricus.jpg
| caption        =
| director       = Edward Buzzell
| producer       = Mervyn LeRoy
| writer         = Irving Brecher Kenny Baker
| music          = Harold Arlen
| cinematography = Leonard M. Smith
| editing        = William H. Terhune
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| country        = United States
| runtime        = 87 minutes
| language       = English
| budget         =
}}
 circus from Kenny Baker.

==Plot== strongman (Nat football players James Burke) who is trying to take over the Wilson Wonder Circus. Jeff Wilsons girlfriend, Julie Randall (Florence Rice), performs a horse act in the circus. In the animal car on the circus train, Goliath and Atom knock out Jeff Wilson (Kenny Baker) and steal $10,000, which Jeff owes Carter. But they unintentionally leave a cigar as evidence.

Tony (Chico) summons Groucho, as attorney J. Cheever Loophole, to handle the situation. Loophole caves in when he sees the muscular Goliath, and gets nowhere with Little Professor Atom. His attempts to get a cigar from Atom are foiled by Chico, who each time offers to give him one of his own. When Harpo sneezes and knocks the furniture over in Atoms tiny room, the midget threatens to sue. Loophole, ever ready to solicit business, offers his business card, which Atom accepts. In order to help Wilson, he first tries to get the hidden money from Carters moll, Peerless Pauline (Eve Arden), but fails. Tony and Goliaths assistant Punchy (Harpo), search Goliaths stateroom on the circus train for the money, but are unsuccessful.

Loophole later calls upon Jeffs wealthy aunt, Mrs. Dukesbury (Margaret Dumont), and tricks her into paying $10,000 for the Wilson Wonder Circus to entertain the Newport 400, instead of a performance by an orchestra conducted by a Frenchman named Jardinet (Fritz Feld). The "400 of Newport" are delighted with the circus; when Jardinet arrives, Loophole, who also delayed Jardinet by implicating him in a dope ring, disposes of the conductor and his orchestra by having them play on a floating bandstand down at the waters edge.
 mooring rope while the orchestra plays the Prelude to Act Three of Wagners Lohengrin (opera)|Lohengrin, serenading the waves. Meanwhile, Carter and his henchmen try to burn down the circus, but are thwarted by loophole, Tony and Punchy, along with the only witness to the robbery - Gibralter the gorilla (Charles Gemora), who also retrieves Wilsons ten thousand dollars.

==Cast==
*Groucho Marx as attorney J. Cheever Loophole
*Harpo Marx as Punchy
*Chico Marx as Antonio Tony Pirelli
*Florence Rice as Julie Randall Kenny Baker as Jeff Wilson
*Margaret Dumont as Mrs. Susanna Dukesbury
*Eve Arden as Peerless Pauline James Burke as John Carter
*Nat Pendleton as Goliath the Strongman
*Jerry Marenghi as Little Professor Atom
*Fritz Feld as Jardinet
*Barnett Parker as Whitcomb
*Charles Gemora as Gibraltar the Gorilla

== Production notes ==
  and Eve Arden in the trailer for At the Circus]]
Buster Keaton worked on the film as a gag man. His career was on the downside and he was forced to work for scale. His complex and sometimes belabored gags did not work well with the Marx Brothers brand of humor, and was a source of friction between the comedian and the group.  When Groucho called Keaton on the inappropriateness of his gags for the Marx Brothers, Keaton responded, "Im only doing what Louis B. Mayer|Mr. Mayer asked me to do. You guys dont need help." 

Hundreds of girls applied for the film, with eighteen finally chosen after "rigid tests." They had to be expert ballet dancers, have good singing voices, and they had to be able to prove all this by doing a toe-dance on a cantering bareback horse, while singing in key. Four of the girls were former circus riders. Several of the other girls had ridden in rodeos, either professionally or as amateurs, and the rest had been riding most of their lives. 
 Show Boat. Universal Pictures studio and served as the companys president until 1946. Show Boat proved to be a financial success and, had the loan not been called for repayment until after the films release, the Laemmles would have been able to repay the loan and retain ownership of their film production company.
 Go West.

One of Grouchos stories about the film concerned the fake gorilla skin that an actor wore. During production, the skin was switched from a gorilla to an orangutan, which perplexed some viewers who would ask Groucho about it if they happened to meet him.

==Musical numbers==
* "Step Up And Take A Bow"
*"Lydia the Tattooed Lady|Lydia, the Tattooed Lady"
*"Two Blind Loves"
*"Swingali" Blue Moon"
*"Beer Barrel Polka"

==Reception==
The Nov. 11, 1939 Ottawa Citizen described the film as "a veritable riot of hilarity" and "possibly the nuttiest of the films that Groucho, Chico and Harpo have perpetuated." 

==References==
 

==External links==
 
* 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 