For His Son
 
{{Infobox film
| name           = For His Son
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Emmett C. Hall Charles West
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent drama film directed by D. W. Griffith and starring Blanche Sweet. The film was shot in Fort Lee, New Jersey when Biograph Company and other early film studios in Americas first motion picture industry were based there at the beginning of the 20th century.    A print of the film survives today.   

==Cast==
* Charles Hill Mailes - The Father, a Physician Charles West - The Son
* Blanche Sweet - The Sons Fiancée William Bechtel - In Office
* Dorothy Bernard - The Secretary
* Christy Cabanne - One of the Sons Friends / At Soda Fountain (as W. Christy Cabanne) Edward Dillon - At Soda Fountain
* Edna Foster - At Soda Fountain
* Robert Harron - At Soda Fountain
* Dell Henderson - In Office
* Grace Henderson - The Landlady Harry Hyde - One of the Sons Friends
* J. Jiquel Lanoe - At Soda Fountain
* Alfred Paget - In Office
* Gus Pixley - At Soda Fountain
* W. C. Robinson - At Soda Fountain
* Ynez Seabury - At Soda Fountain (as Inez Seabury)
* Kate Toncray - At Soda Fountain

==See also==
* List of American films of 1912
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 
*   on YouTube
 

 
 
 
 
 
 
 
 
 
 
 

 