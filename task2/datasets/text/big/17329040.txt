Extreme Justice (film)
{{Infobox film
| name           = Extreme Justice
| image          = Extreme justice.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Mark L. Lester
| producer       = Frank Sacks
| writer         = Frank Sacks Robert Boris
| narrator       =
| starring       = Lou Diamond Phillips Scott Glenn Chelsea Field
| music          = David Michael Frank
| cinematography = Mark Irwin
| editing        = Donn Aron
| distributor    = Trimark Pictures
| released       =  
| runtime        = 96 min.
| country        = United States English
| budget         =
| gross          =
}}
Extreme Justice is a 1993 action film|action-thriller film. The film was directed by Mark L. Lester, and stars Lou Diamond Phillips, Scott Glenn, and Chelsea Field.

==Plot== LAPD unit designed to track and shut down high profile criminals. Jeff discovers that the group in actuality functions more like an unofficially sanctioned death squad of rogue cops who are given wide latitude when it comes to dealing with criminals. Although their stated mission is to surveil criminals and arrest them while in the act of committing a crime, the squad will often resort to using brutality and murder to accomplish their goals.

This causes Jeff to question the purpose of the squad, and he begins to see them as being more of a harm to society then a positive force for justice. When he tries to bring evidence of the squads abuse of power, he learns that the squad is protected by well-connected and very influential people who already know and condone the squads methods. Jeffs former teammates in the squad begin to suspect that Jeff has turned on them and decide to take measures to eliminate him before he can expose their activities to the public.

==Cast==
* Lou Diamond Phillips  as Detective Jeff Powers
* Scott Glenn  as Detective Dan Vaughn
* Chelsea Field  as Kelly Daniels
* Yaphet Kotto  as Detective Larson
* Andrew Divoff  as Angel
* Richard Grove  as Lloyd
* William Lucking  as Cusak
* L. Scott Caldwell  as Devlin
* Larry Holt  as Reese
* Daniel Quinn  as Bobby Lewis (Surfer)
* Thomas Rosales Jr.  as Chavez (as Tom Rosales)
* Ed Frias  as Herrera
* Jay Arlen Jones  as Nash
* Adam Gifford  as Speer
* Jophery C. Brown  as Vince
* Stephen Root  as Max Alvarez
* Sonia Lopes  as Rosa Rodrigues
* Ed Lauter  as Captain Shafer

==External links==
*  

 

 
 
 
 
 
 


 