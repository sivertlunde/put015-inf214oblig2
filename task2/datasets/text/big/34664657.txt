Four Women of Egypt
{{Infobox film
| name           = Four Women of Egypt
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Tahani Rached
| producer       = Éric Michel
| writer         = Tahani Rached
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =   
| music          = Jean Derome
| cinematography =  
| editing        =  
| studio         = National Film Board of Canada
| distributor    = 
| released       =  
| runtime        = 
| country        = Canada
| language       = French Arabic
| budget         = 
| gross          = 
}}
Four Women of Egypt (original French title Quatre femmes dÉgypte) is a 1997 Canadian-Egyptian documentary film by Tahani Rached. The film revolves around four female friends from Egypt with opposing religious, social, and political views in modern-day Egypt. The film was highly acclaimed and won several awards in documentary film festivals.   

==Cast==
Wedad Mitry has been a lifelong journalist. A student activist, she was the only woman elected to the Student Union at Cairo University in 1951. That same year she joined the Womens Popular Resistance Committee (founded by the feminist Saiza Nabarawi).   

Safinaz Kazem, a journalist, theater critic and writer, is the author of many books. In the 1960s she was a graduate student in the United States-in Kansas, Chicago, and New York. She is the ex-wife of the Egyptian poet Ahmed Fouad Negm and the mother of the political activist and writer Nawara Negm. 

Shahenda Maklad was active in student and nationalist movements, running as a candidate in parliamentary campaigns. She continues her tireless fight for peasants rights and other populist causes. Her husband, Hussein Salah was politically assassinated on 30 April 1966 in the town of Kamshish. 
 
Amina Rachid is a committed leftist, was born into the old upper class, the granddaughter of Ismail Sidki (a former prime minister). She completed her studies in Paris where she was active in the Arab Student Association in France and worked for several years at the French National Centre for Scientific Research. Her political commitment brought her back to Egypt where she teaches French literature at Cairo University. 

==Plot==
The film opens with four middle aged women walking on a bridge at the barrages south of Cairo, Egypt. The four women speak throughout the film about Egypt, its politics, culture, and Islam, the most popular religion in the country. They connect the politics and ideologies of past and present with their own experience. The four women are friends and were born under the colonial occupation of Egypt. They share their memories that span five decades throughout the film. The four women were political prisoners under the regime of Anwar Sadat.   

==Awards==
The film tied with   by Kasim Abid for the Audience Honorary award for Best Documentary in the Arab Screen Independent Film Festival which took place from April 15 to 18 1999, London - England. It also won the Best Documentary Film - with a cash prize of (£2000) in the same festival. 

The film also won the Public Award in the International Documentary Film Festival which took place from November 15 to 23 1997, Odivelas - Portugal. The film was also awarded the Grand Prize for feature-length documentary in the Category Films presented in video format in the same festival. 

==References==
 

==External links==
*  
* Watch   at the National Film Board of Canada

 
 
 
 
 
 
 
 
 
 
 