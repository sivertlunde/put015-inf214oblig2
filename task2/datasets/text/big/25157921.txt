Free Willy: Escape from Pirate's Cove
 
  }}
{{Infobox film
| name           = Free Willy: Escape from Pirates Cove
| image          = Free Willy4.jpg
| image_size     = 190px
| caption        =
| director       = Will Geiger
| producer       =  
| screenplay     = Will Geiger
| story          = Cindy McCreery
| narrator       =
| starring       =  
| music          = Enis Rotthoff
| cinematography = Robert Malpage
| editing        = Sabrina Plisco
| studio         =  
| distributor    = Warner Premiere
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Free Willy: Escape from Pirates Cove is a 2010 direct-to-video family film and the fourth and final installment in the Free Willy series. It stars Beau Bridges and Bindi Irwin. The film was released on DVD and Blu-ray Disc|Blu-ray on 23 March 2010 in the United States. It was released on 2 August in the United Kingdom and Ireland.  The film is not connected in any way to the first three movies and is considered to be a reboot of the series.

==Plot==
Kirra is sent to stay with Gus in his run-down amusement park in Cape Town after her father was hospitalized for six weeks. Upon arrival, Kirra is met at the airport by Mansa, one of her grandpas employees. Kirra is initially unhappy about leaving her father, but she eventually makes friends with a boy named Sifiso.

After a fierce storm, a baby male orca was separated from his pod, and became stranded in a lagoon on Guss property. The next morning, Kirra discovers the animal and names it Willy. Willy proves to be a big hit amongst the park visitors. However, Kirra is concerned about Willy, who is not eating due to stress. Kirra sets herself a mission to take care of it. Kirra tries very hard to get Willy to eat a fish, and finally succeeds after talking to it about her parents.
 echolocation skills and is unable to survive without his pod, thus making it unsuitable for rehabilitation. Not one to give up, Kirra does much researching on how to train Willy to use his echolocation skill, despite being told that there is no known method to do so. She then tries to feed Willy blindfolded, but fails many times.

Rolf, desperate for Willy, hatches a plot to poison Willy to get Gus to sell it at a cheaper price. After his plan fails, Rolf denies all knowledge of the plan, though he offers to buy Willy again.

Kirra then camps out by the lagoon to keep Willy calm after the failed plot. Willy suddenly wakes her up, pulls her into the water, and lets her ride him. After that, Kirra and Willy become a double-act at the park, attracting many reporters and cameramen. The money earned from this publicity is then used to fund the fishes needed for Willys echolocation training. After many tries, Willy learns to use his echolocation, and manages to catch live fish swimming in the lagoon. In the meantime, Rolf returns to tempt Gus with a lower offer for Willy. Faced with a mounting food bill, Gus agrees to sell Willy to Rolf at 500,000 dollars, and insists the exchange take place after Kirras departure.

Mansa makes an underwater recording device to record Willy’s sounds, hoping to use the sounds to locate Willys pod. Despite working hard for days, they do not produce any results. One day, Sifiso invites Kirra to go to his Uncle Rudys safari park to take her mind off Willy. On their way back, they see a billboard advertising Willy as a new attraction to Rolfs theme park. The pair then hurries back, but was too late as Gus had already signed the agreement to sell Willy. Kirra is heartbroken, and makes Gus promise to make sure that Rolf takes good care of Willy. Later, when Kirra goes down to the lagoon, she sees Willy’s pod. However, Gus does not believe her account.

Kirra and Sifiso go to seek Uncle Rudys help with their plan to put Willy back into the ocean. However, he is not around, but the two steal a crane truck and drive it back to Pirates Cove. Later, Gus agrees to help them get Willy back into the ocean if his pod can be found. Kirra and Sifiso then head for the harbour with Willy, while Gus and Mansa stay to distract Rolf, who was on his way over. Eventually they find Willys family, and Willy is reunited with them. Kirra later jumps into the water to bid Willy farewell.

On the day of Kirras departure, she says goodbye to Mansa and Sifiso at the airport, and agrees to come back next summer. As she gets on the plane, Gus wipes away a tear. Kirra smiles as she takes a final look on Cape Town. Meanwhile, Willy and his pod swim off into the ocean depths.

==Cast==
* Bindi Irwin as Kirra
* Beau Bridges as Gus, Kirras maternal grandfather.
* Siyabulela Ramba as Sifiso
* Bongolethu Mbutuma as Mansa
* Stephen Jennings as Rolf Woods, the owner of a rival theme park to Pirates Cove.
* Kevin Otto as Dr. Sam Cooper, Kirras father and a veterinarian. He was injured in an unfortunate accident while tending to the animals.
* Darron Meyer as the doctor of Kirras father.
* Jeanne Neilson an air hostess.
* Robert Spencer as the nerdy boy.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 