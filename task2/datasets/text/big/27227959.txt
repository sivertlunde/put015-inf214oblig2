Rockabye (1986 film)
{{Infobox Film
| name           = Rockabye
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Michaels
| producer       = Jack Grossbart Marty Litke
| writer         = Laird Koenig (novel)
| narrator       = 
| starring       = Valerie Bertinelli Rachel Ticotin Jason Alexander Jimmy Smits Charles Bernstein John Lindley
| editing        = Jim Benson
| studio         = 
| distributor    = CBS
| released       = January 12, 1986
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Rockabye is a 1986 television film|made-for-TV crime drama film, starring Valerie Bertinelli, Rachel Ticotin, Jason Alexander, and Jimmy Smits.

==Plot==
Stranded in New York City due to missing a bus caused by a delay of a plane, recently divorced mother Susannah Bartok (Valerie Bertinelli) is attacked and maced outside Macys in Manhattan, and her 2-year-old son gets kidnapped. After she unsuccessfully pleads to the police, who feel indifferent about the case, newspaper reporter Victoria Garcia (Rachel Ticotin) helps the young mother in finding her son. Susannah, desperate to find her son, initially rejects Victorias help because she is realistic about the possible fate of her boy, though convinced that the police are not doing their job quickly enough, she allows Victorias help. Victoria redirects Susannah to a psychic called Christopher Zellner (Roderick Cook), who believes that her son Sonny is dead. Susannah refuses to believe him, and continues her intense and exhausting search. After putting a photo of her son in the newspaper, several witnesses report to the police, but they are all frauds, annoying Lt. Ernest Foy (Jason Alexander). During their search, they discover an underground black market ring, selling young children.

==Cast==
*Valerie Bertinelli as Susannah Bartok
*Rachel Ticotin as Victoria Garcia
*Jason Alexander as Lt. Ernest Foy Ray Baker as Donald F. Donald
*Roderick Cook as Christopher Zellner
*Jonathan Raskin as Sonny

==External links==
* 

 
 
 
 
 
 
 
 
 


 