Return to Macon County
{{Infobox film
| name           = Return to Macon County
| image          = Poster of the movie Return to Macon County.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Richard Compton
| producer       = Elliot Schick Samuel Z. Arkoff  (executive producer) 
| writer         = Richard Compton
| narrator       =
| starring       = Nick Nolte Don Johnson Robin Mattson Eugene Daniels Robert Viharo Devon Ericson
| music          = Robert O. Ragland
| cinematography = Jacques R. Marquette
| editing        = Corky Ehlers
| distributor    = American International Pictures
| released       = September 3, 1975
| runtime        = 
| country        = United States English
| budget         = $800,000  (estimated) 
| gross          = SEK 1,146,410  (Sweden) 
| preceded by    =
| followed by    = 
}} 1975 sequel 1974 drive-in classic Macon County Line. This film was written and directed by Richard Compton, who was also responsible for the earlier film.

Set in 1958, the film stars then little-known actors Nick Nolte (as Bo) and Don Johnson (as Harley). They play friends who are heading to California to enter a drag race.

==Plot==
Bo is the driver and Harley is the mechanic. They stop at a roadside diner to eat and meet Junelle (Robin Mattson). Junelle, while attractive, is in a world of her own. After having an altercation with a customer, she is rescued by Bo and Harley.

The hot-rodding friends find that Junelle (with suitcase in hand) wants to travel with them. Their adventure on the road with Junelle turns dangerous after a misunderstanding at a grocery store, where Junelle is trying to raise funds for Bo and Harleys entrance fee to the drag race. Sgt. Whittaker (Robert Viharo) becomes obsessed with catching them, which leads to tragic results.

==External links==
* 

 
 
 
 
 
 
 
 
 


 