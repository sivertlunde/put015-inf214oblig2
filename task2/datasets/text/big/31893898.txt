The Deep Blue Sea (2011 film)
 
 
{{Infobox film
| name           = The Deep Blue Sea
| image          = The Deep Blue Sea (2011 film) poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Terence Davies Sean OConnor Kate Ogborn
| screenplay     = Terence Davies
| based on       =  
| starring       = Rachel Weisz Tom Hiddleston Simon Russell Beale Harry Hadden-Paton
| music          = Samuel Barber
| cinematography = Florian Hoffmeister
| editing        = David Charap Film4 UK Film Council Artificial Eye
| distributor    = Music Box Films   
| released       =  
| runtime        = 98 minutes  
| country        = United Kingdom
| language       = English
| budget         =
| gross          = $1,126,525   
}} romantic drama The Deep Sean OConnor and Kate Ogborn.

Filming began in late 2010 and it was released in the United Kingdom in 2011, the year of Rattigans centenary. It was released in the United States in 2012 by distributor Music Box Films.    

==Plot==
In 1950, Hester Collyer, the younger wife of High Court judge Sir William Collyer, has embarked on a passionate affair with Freddie Page, a handsome young former RAF pilot troubled by his memories of the Second World War. Freddie throws Hesters life in turmoil, as their erotic relationship leaves her emotionally stranded and physically isolated. For Freddie, that tumultuous mix of fear and excitement that was once in his life seems to be no longer present.

The majority of the film takes place during one day in Hesters flat, a day on which she has decided to commit suicide. Her attempt fails and as she recovers, the story of her affair and her married life is played out in a mosaic of short and sporadic Flashback (narrative)|flashbacks. We soon discover the constraints of Hesters comfortable marriage, which is affectionate but without sexual passion.
 affair is discovered she leaves her life of comparative luxury and moves into a small dingy London flat with Freddie. Hesters new lover has awakened her sexuality, but the reckless, thrill-seeking Freddie can never give her the love and stability that her husband gave her. Yet to return to a life without passion would be unbearable. The film takes its title from her dilemma of being caught between the Devil and the deep blue sea – two equally undesirable situations. 

==Cast==
* Rachel Weisz as Hester Collyer
* Tom Hiddleston as Freddie Page
* Simon Russell Beale as Sir William Collyer
* Harry Hadden-Paton as Jackie Jackson
* Ann Mitchell as Mrs Elton
* Sarah Kants as Liz Jackson
* Karl Johnson as Mr. Miller
* Barbara Jefford as Collyers mother
* Oliver Ford Davies as Hesters father

==Critical reception==
The film was released to highly positive reviews from critics. At Rotten Tomatoes, the film holds a rating of 78%, based on 130 reviews and an average rating of 6.1/10.    It also has a score of 82 on Metacritic based on 30 reviews.   
 2012 New York Film Critics Circle Awards and also won the Best Actress Award from the Toronto Film Critics Association in the same year.

In December 2012, Weisz was nominated as Best Actress in the 70th Golden Globe Awards. The film was also chosen as one of the Top Ten films of the year by The New York Times and The Los Angeles Times. The film has found a largely appreciative audience and critical success in the United States with Weiszs performance named as film performance of 2012 by David Edelstein of New York (magazine)|New York magazine.

==Accolades==
{| class="wikitable" style="font-size: 95%;" 
|- align="center"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Outcome
|- BFI London 55th BFI London Film Festival 
| 12–27 October 2011 Best Film
|The Deep Blue Sea
|  
|- Evening Standard 39th Evening Standard British Film Awards    6 February 2012 Best Actress Rachel Weisz
|  
|- London Film 33rd London Film Critics Circle  19 January 2012 British Actress of the Year Rachel Weisz
|  
|- Supporting Actor of the Year Simon Russell Beale
|  
|- 2012 New York Film Critics Circle Awards
| 3 December 2012 Best Actress Rachel Weisz
| 
|- 70th Golden Globe Awards
| 13 January 2013 Best Actress in a Motion Picture – Drama Rachel Weisz
| 
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 