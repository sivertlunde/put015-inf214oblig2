Shukriya: Till Death Do Us Apart
{{Infobox film|
  name           = Shukriya: Till Death Do Us Apart |
image=Shukriya,_Till_Death_Do_Us_Apart.jpg|
  caption        = Dvd Cover for Shukriya: Till Death Do Us Apart |
  director       = Anupam Sinha |
  writer         = Jeetu Arora Anubhav Sinha |
  starring       = Anupam Kher Aftab Shivdasani Shriya Saran Indraneil Sengupta |
  producer       = Vibha Bhatnagar Meenu Bachan |
  Line producer  =  Jai Prakash Upadhyay
  released       =  |
  runtime        = |
  language       = Hindi |
  music          = Himesh Reshammiya Vishal Dadlani Devendra Yogendra Raju Rao|
  cinematography = Rajeev Shrivastav|
  editing        = |
}} 2004 film directed by Anupam Sinha, starring Anupam Kher, Aftab Shivdasani, Shriya Saran and Indraneil Sengupta. It is based on the film Meet Joe Black starring Brad Pitt.

==Plot==
Thirty years ago Karam Jindal together with his widowed mom, Gayatri, and wife, Sandhya, had immigrated to London, England. Shortly thereafter Gayatri gets cancer and tragically passes away. Then, Sandhya gives birth to two daughters, Anjali and Sanam. The Jindals accumulate wealth, and are now one of the wealthiest families in London. Anjali gets married to Akash, while Sanam is on the look-out for her beau. With Karams 60th birthday coming up, Anjali is busy with preparations for a grand party, while Sanam has already started with her make-up. Karam hopes to get Sanam married to Yash, his employee, who is like a son to him. Add to that is the inauguration of the "Gayatri Jindal Cancer Hospital" which is to be done on the same day. 

With the preparations under way, Karam brings home a young man, Rohan "Ricky" Verma, to live with them for a few days. Sanam has already met him and is quite friendly with him. She confides in her mom that she would like to marry Rohan, and her mom indicates that she approves of him. They get a shock when Karam vehemently opposes any alliance with Rohan, and refuses to divulge the reason(s). For it is only Karam who knows that Rohan is not who he claims to be - for he is Death himself - accompanying Karam during his last four days on Earth.

== Cast ==
*Anupam Kher as  Karam Jindal
*Aftab Shivdasani as  Rohan "Ricky" Verma
*Shriya Saran as  Sanam K. Jindal
*Indraneil Sengupta as  Yash
*Rati Agnihotri as  Sandhya K. Jindal
*Rana Jung Bahadur as  Uday R. Jindal

== Music ==
Two songs in this film are given by Himesh Reshammiya. A version of "Sohniye", originally by Juggy D and Rishi Rich, also appears in the film.

===Track list===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aankhon Aankhon Mein"
| KK (singer)|KK, Sunidhi Chauhan
|-
| 2
| "Maine Poochha Kudrat Se"
| Kumar Sanu, Anuradha Paudwal
|- 
| 3
| "Tumhe Jitna Bhulate Hain"
| Sonu Nigam
|- 
| 4
| "Ni Sohniye"
| Sonu Nigam, Udit Narayan, Alka Yagnik, Shreya Ghoshal
|- 
| 5
| "Dil Ai Dil"
| Kumar Sanu
|-
| 6
| "Leti Hai Yeh Zindagi"
| Sonu Nigam
|- 
| 7
| "Maine Dil Mein Chupaya"
| Udit Narayan, Alka Yagnik
|-
| 8
| "Shukriya (Theme)"
| Instrumental
|-
| 9
| "Kya Haal Hai Mere Is Dil Ka"
| Alka Yagnik, Udit Narayan 
|}

== External links ==
*  

 
 
 
 