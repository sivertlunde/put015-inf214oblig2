Karl Hess: Toward Liberty
{{Infobox film
| name           = Karl Hess: Toward Liberty
| image          = 
| caption        = 
| director       = Roland Hallé Peter W. Ladue
| producer       = Roland Hallé Peter W. Ladue
| writer         = Peter W. Ladue Kevin Burns
| narrator       = Kevin Burns 
| starring       = Karl Hess
| music          = Keith Martin Don Wilkins, Berklee School of Music 
| cinematography = John Hoover
| editing        = Loren Miller
| sound          = Charlie Lew
| distributor    = Direct Cinema 
| released       =  
| runtime        = 26 minutes 
| country        = United States 
| language       = English
| budget         = 
}} short documentary 1981 for Documentary Short Subject.    The film was produced at Boston Universitys College of Communications, School of Broadcasting and Film, Graduate Film Program. Several students, faculty and others made a substantial contribution to creating the film.  

==Background==
During the late 60s, Cinema Verite or Direct cinema became the new wave in documentary film making. Boston filmmaker Fred Wiseman released “Titicut Follies” (1967), which took us inside a Massachusetts mental institution, and “High School” (1968), where we became the proverbial fly on the wall in an American secondary school.  Like other Verite filmmakers, Wiseman’s approach was to shoot hundreds of hours of footage and shape the story in the editing room. 

The same new technology that enabled French New Wave dramatic films was adopted by American documentary filmmakers. Small (for the day) hand held cameras and greatly improved 16mm film stocks eliminated the need for placing the camera on a tripod and washing a scene with intense lighting. Until that point, the size and bulk of film making equipment had limited the chance of capturing real life and real human behavior. 

These films were a revelation. For the first time we were experiencing “real life” on film. No recreations, no staging.  Cinema Verite was both voyeuristic and insightful.  It was the world’s first Reality TV.  

From the audience’s perspective, the camera in Verite took on an invisible presence within the scene.  But was it invisible to the participants? How does the camera impact  the reality of the moment? Does the editing and post-production process as well as the need to tell compelling stories mean that objectivity is not possible?  Can we actually experience real life? Do we want to? This was the debate that took place at the Graduate Film Program that helped shape Karl Hess: Toward Liberty. 

While Verite had a claim on reality, TV documentaries required balance. In part, this was driven by the FCC’s Fairness Doctrine, but more so by the reluctance of the network to offend any audience. 

==Production==
In deliberate contrast to Direct Cinema and traditional documentaries, Toward Liberty wouldn’t pretend to be a slice of life or balanced. It would be transparent to the audience that this was one man’s – one very interesting man’s – take on America. Let a thousand voices speak and truth wins.  

While they would remain true to the man, Roland and Peter had no intention of becoming an invisible presence in the film. Working with hours of transcripts and footage, they shaped the story back in Boston, along with Editor Loren Miller. Their strategy was to give Karl a forum to speak his mind, building a storyline from interviews in which he leaped from one topic to another, and then periodically pop up to make a comment on Karl’s views. Their original intent was for Karl to be the only voice in the film, but during editing they decided to include opening narration to introduce Karl.   

One scouting trip to West Virginia, two production trips to West Virginia, one to Washington D.C, and too many months later they were ready to run a check print. It’s hard to imagine today, with thousands of effects at our finger tips, that Peter and Roland couldn’t even see a dissolve until a print was struck. 

People either loved or hated the film.  It either won first pace at festivals or didnt make the cut. Standing in the back of at the first screening, they heard laughter and when the lights went up, intense conversations. Like filmmakers everywhere all they could see were the little things they wished they’d done differently.  But as the film made the festival circuit, it became clear that audiences were enchanted by Karl. He was an authentic American character. If the film preserves the memory of Karl, it’s done its best.

From the screening notes from the AMPAS Documentary Retrospective Oscars Docs, Part Three, Linwood Dunn Theater, October 1, 2007  

==Karl Hess==
The press editor of Newsweek magazine when was 26, Karl Hess became the voice behind Republican conservatives including Dwight Eisenhower, Richard Nixon and Barry Goldwater. When Goldwater was defeated, the conservative leadership was ostracized from the party. Karl became curious about the New Left, the antiwar movement, local politics and economics, and alternative technology.   

On his long strange journey Karl had gone from mainstream media to White House insider to being arrested on the Capital steps during an antiwar demonstration. There was a time when he was both writing Goldwater’s newspaper column and active in the New Left.   

For Karl, being a national player was not nearly as interesting as having a real impact at a neighborhood level.  After working in D.C.’s Adams-Morgan neighborhood, Karl disappeared from the Washington scene.  A stone’s throw from his old life, Karl and his wife Theresa settled near Martinsburg, West Virginia in their hand-built, energy-efficient, earth-insulated home. Hess was fascinated by the possibilities of appropriate technology to empower communities and individuals. In the late 1970s and early 1980s, Karl taught a summer-long course at Goddard College. This hands-on experience included building and operating solar and wind energy systems, aquacultural and organic farming.   

His energy, his life experience, his take on the times, and most of all his sense of humor made Hess an ideal subject for a documentary.

==Awards==
* 1980 - Maya Dern Award, Boston University
* 1980 - FOCUS Student Film Festival, Best Film
* 1980 - AMPAS Student Film Award, Best Documentary
* 1981 - Academy Award: Best Documentary Short Subject
* 1981 - CINE Golden Eagle
* 1981 - Governors Award, State of Massachusetts
 
==Cast==
* Karl Hess - Himself (also archive footage)
* Kevin Burns - Narrator (voice)
* Barry Goldwater - Himself (archive footage)
* Adolf Hitler - Himself (archive footage)
* Lyndon Johnson - Himself (archive footage) (uncredited)

==References==
 

==External links==
* 
* :  Toward Liberty
* :  Acceptance speech of Roland Hallé and Peter W. Ladue, Producers for Karl Hess: Toward Liberty

 

 
 
 
 
 
 
 
 
 
 
 