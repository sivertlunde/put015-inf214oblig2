Tarzan and the Jungle Boy
{{Infobox Film
| name           = Tarzan and the Jungle Boy
| image          = Tarzan and the Jungle Boy (movie poster).jpg
| writer         = Stephen Lord
| based on       =   Mike Henry Rafer Johnson Aliza Gur Steve Bond Robert Gordon Robert Day
| music          = William Loose
| cinematography = Ozen Sermet
| editing        = Milton Mann Reg Browne
| studio         = Banner Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 mins.
| language       = English
}}
 Mike Henry Robert Day, Robert Gordon. It was released in May 1968.   

==Synopsis==
At home in Africa, Tarzan assists Myrna, a photojournalist, and Ken, her associate, in their search for Erik Brunik, a thirteen-year-old boy lost in the jungle since he was seven years old.  Tarzan is assisted by his friend Buhara, whose brother Nagambi does not wish the boy found, and attempts to kill him before Tarzan saves the day.

==Selected Cast== Mike Henry as Tarzan
* Rafer Johnson as Nagambi, villain who hinders Tarzans search for the Jungle Boy
* Aliza Gur as Myrna, photojournalist searching for Erik
* Steve Bond as Erik Brunik, the missing Jungle Boy
* Ron Gans as Ken, Myrnas associate
* Ed Johnson as Buhara, ally to Tarzan, brother of Nagambi

==Production notes==
The movie was filmed on location in Brazil and along the Amazon River immediately after production of the previous film, Tarzan and the Great River.

All three of Mike Henrys Tarzan films were completed before the first (Tarzan and the Valley of Gold) was released in 1966.

The roles of opposing brothers Nagambi and Buhara were played by real life brothers Rafer and Ed Johnson.

Citing exhaustion and unsafe work conditions (Henrys jaw was bitten by the chimpanzee that was playing Cheeta, named Dinky in Tarzan and the Great River previously) Mike Henry bowed out of the Tarzan (NBC series)|Tarzan television series (for which he had been signed to play the lead) and sued Sy Weintraubs Banner Productions.  The case was settled for an undisclosed sum.  Ron Ely replaced him as Tarzan.

==References==
 
Essoe, Gabe. Tarzan of The Movies, 1968, published by The Citadel Press.

==External links==
*  
*  
*  

 

 
 
 
 
 