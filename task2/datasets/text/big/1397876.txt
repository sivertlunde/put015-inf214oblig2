Spawn (film)
{{Multiple issues|
 
 
}}
 
{{Infobox film
| name           = Spawn
| image          = spawnmovieposter.jpg
| alt            = 
| caption        = Original release poster
| director       = Mark A.Z. Dippé
| producer       = Clint Goldman
| screenplay     = Alan B. McElroy
| story          = Alan B. McElroy Mark A.Z. Dippé
| based on       =  
| starring       = Michael Jai White John Leguizamo Martin Sheen Nicol Williamson Theresa Randle D. B. Sweeney Melinda Clarke Frank Welker
| music          = Graeme Revell
| cinematography = Guillermo Navarro
| editing        = Rick Shaine Michael N. Knue Todd Busch
| studio         = Todd McFarlane Entertainment Pull Down Your Pants Pictures
| distributor    = New Line Cinema
| released       =  
| runtime        = 96 minutes  
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $87,840,042 
}} comic book Al Simmons, a soldier/assassin who is killed and resurrected as Spawn, a reluctant, demonic leader of Hells army. Spawn eventually refuses to lead Hells army in the war against Heaven and turns against evil all together. The film co-stars John Leguizamo as Violator (comics)|Clown/The Violator, Als demonic guide and the films antagonist; and Nicol Williamson as Als mentor Cogliostro. Martin Sheen, Theresa Randle, D. B. Sweeney, Melinda Clarke and Frank Welker (as the voice of Malebolgia) also star.

Spawn was released in the United States on August 1, 1997. It was the first film to feature an African American portraying a major comic book superhero, followed by Steel (1997 film)|Steel in the same month on the 15th.  This was Williamsons final film appearance before his death on December 16, 2011.

==Plot==
 , he will be able to return to Earth to see his fiancée, Wanda Blake. Simmons accepts the offer and is returned to Earth.
 The Violator who acts as a guide, setting Simmons onto the path to evil. He also meets a mysterious old man named Cogliostro - a fellow Hellspawn who freed his soul and now fights for Heaven. Wynn has become a high-class weapons dealer and has developed the ultimate biological weapon, Heat 16. During a reception, Simmons attacks Wynn, kills Jessica, and escapes, instinctively using Spawns strange armor.

Following Simmons attack, the Violator convinces Wynn to have a device attached to his heart that will trigger the worldwide release of Heat 16 should his vital signs flatline. The device is supposedly a safeguard against assassination attempts, but Malebolgia actually wants Simmons to kill Wynn and trigger the apocalypse. Spawn confronts the Violator, who turns into his demonic form and beats him down. Cogliostro rescues him and teaches him how to use his necroplasm armor before Simmons learns that Clown and Wynn are going to kill Terry.

Meanwhile, Terry has just finished emailing a fellow newsman who sent him evidence exposing Wynn. After the transmission, Cyan enters the room, with Wynn right behind her. Wynn destroys Terrys computer and takes the family hostage. When Spawn arrives, he ends up almost killing Wynn, despite his warning that his death will launch the Heat 16 bombs. Only after realizing that Wynns death would ultimately mean the death of Cyan does Spawn relent. Instead, he extracts the device from Wynns body before destroying it. His plans foiled, Clown draws Spawn and Cogliostro into Hell, where Spawn tells Malebolgia that he will never lead his army. He engages in a short battle with the Violator before escaping with Cogliostro just before they are overwhelmed and return to Earth. The Violator follows and there is a final battle between him and Spawn, ending with Spawn severing the demons head with his chains. The Violators head taunts the group and says that he will return before melting and going back to Hell. Wynn is arrested and Spawn, realizing there is no place for him in Wandas world anymore, dedicates himself to justice rather than succumbing to his lust for vengeance.

==Cast== Al Simmons/Spawn
* John Leguizamo as Violator (comics)|Clown/Violator
* Martin Sheen as Jason Wynn
* Nicol Williamson as Cogliostro Wanda Blake-Fitzgerald
* D. B. Sweeney as Terry Fitzgerald
* Melinda Clarke as Jessica Priest
* Frank Welker as the voice of Malebolgia
* Sydni Beaudoin as Cyan Fitzgerald
* Miko Hughes as Zack
* Michael Papajohn as Glen, Zacks father

Hughes was a Spawn enthusiast, professing that "  getting all the comics and action figures since theyve been coming out." 

===Cameos===
* At the government-hosted gala, a red-headed woman with Spawn symbol-shaped earrings, played by Laura Stepp, can be seen. This cameo is generally considered to be a nod to the angelic Hellspawn hunter Angela (comics)|Angela. 
* Todd McFarlane makes a cameo appearance as one of the bums running away during the alley fight between Spawn and the Violator. 
* Detectives Sam Burke and Twitch Williams are shown taking Wynn into custody near the end of the film. 

==Production==
Todd McFarlane had offers for a film adaptation of Spawn (comics)|Spawn by Columbia Pictures just as the comic started in 1992, but the deal would fall out as the artist felt the studio was not giving him enough creative control.    McFarlane sold the Spawn film rights to New Line Cinema for $1 in exchange for creative input and merchandising rights.    New Line president Michael DeLuca, a comic book collector himself, expressed interest in having "a character that has as established an audience as Spawn", while declaring the chances of success deemed on an adaptation that "maintains a PG-13 rating but retains its darkness." 
 Steve Spaz comic book also wrote many episodes of the Todd McFarlanes Spawn animated series. 

Michael Jai White found appeal in the story of Al Simmons, which he described as "the most tragic character Ive encountered in any cinematic production", while providing the challenge of making audiences sympathize with a government assassin who comes back from hell. White had endure two to four hours of make-up appliance, that included a full glued-on bodysuit, eye-irritating yellow lenses, and a mask that restricted his breathing.  The actor declared his long-time experience with martial arts helped him go through the uncomfortable prostethics, that required "strong will and unbreakable concentration." 

Spawn was originally green-lighted at $20 million. Every test of the visual effects lead New Line to increase the budget, culminating in $40 million, a third of which were spent on the effects. The shooting schedule was only 63 days, with a week cut so Goldman could lend $1 million for John Growers Santa Barbara Studios to develop the digital Hell sequences.  The visual effects shot count raised from 77 shots to over 400, created by 22 companies in the United States, Canada and Japan.  ILM had the majority of work, 85 shots at the cost of $8.5 million, requiring 70 people and nearly 11 months to complete the work. The effects had the films difficult sequences, including the Violator, Spawns digital cape, and some of Spawns transformations.   More than half the effects shots were delivered about two weeks before the films debut. 

==Differences from the comic==
 
Although based on the comic book series, some details were changed for the theatrical version of Spawn. Terry Fitzgerald, Al Simmons best friend in his former life in the comics, is a black man who is played by a white man, D. B. Sweeney, in the film. Todd McFarlane explained that this was done by the studio to avoid having too many black leads and creating a perception the film was aimed at just a black target audience.  In the comics, Cyan is Terrys daughter, introduced in the third issue as being roughly eighteen months old. In the movie, Al is Cyans father instead. In the film, Wanda was revealed to be engaged to Al prior to his death whereas in the comic the two were married. The comic had Al hitting Wanda while the film did not.

In the comic, Al Simmons murderer was Chapel (comics)|Chapel, a character created by Rob Liefeld for the comic Youngblood (comics)|Youngblood. In the film, Jessica Priest, a character created for the film, takes Chapels place in the story.  Priest was later introduced into the comics and was retconned to be Als murderer.  In the film, Simmons worked for an agency called A6, while in the comic book he worked for the Central Intelligence Agency|CIA.

The nature of Spawns powers and allies are different. Cogliostro, for example, while revealed to be Cain in the comics, is portrayed as an assassin for the church in the fifteenth century, who has forsaken most of his Spawn-based powers apart from the blade attached to his right wrist. While Cogliostro warns Spawn that he will die if his powers are drained, no reference is ever made to Spawn possessing a "counter" like in the comics.

==Release==
The original cut for Spawn earned an Motion Picture Association of America film rating system|R-rating from the Motion Picture Association of America, leading the producers to tone down the violence and profanity to get a PG-13.  The original version was released on DVD as the "Directors Cut". 

===Box office=== Air Force One. For its second weekend, the film remained at the third spot, dropping 54.7%, and grossing $8,949,953.  The film was considered a modest box-office success; based on a $40 million budget, it grossed $54,870,174 domestically and $32,969,867 overseas for a worldwide total of $87,840,042.   

===Critical response===
The film received negative reviews from film critics. According to  .  One of the few positive reviews was from Roger Ebert, who awarded the film 3½ out of 4 stars. He ended his review with, "As a visual experience, Spawn is unforgettable." 

===Accolades===
  Best Make-up. Sitges - Catalonian International Film Festival, Spawn was nominated for Best Film; the film was also nominated for & won the Best Special Effects award.

===Home media===
There are two versions of the film, the PG-13 version and the R-rated directors cut. Both versions are available on VHS while only the R-rated directors cut is available on DVD and later on Blu-ray Disc|Blu-ray, which was released on July 10, 2012. 

==Sequel==
A  . 

In 2007, plans were made for McFarlane Funding to make a new Spawn film, scheduled for release in 2008.  The film may simply be called Spawn, according to Home Media Magazine.  While a guest on the Scott Ferrall show on Sirius radio, a caller asked if he had any plans to do the sequel. He said "Its coming out no matter what. Even if I have to produce, direct and finance it myself, its going to come out." 

It was announced in August 2009 that McFarlane had officially begun writing the screenplay for a new movie based on the character. "The story has been in my head for 7 or 8 years," McFarlane said. "The movie idea is neither a recap or continuation. It is a standalone story that will be R-rated. Creepy and scary." He added that "the tone of this Spawn movie will be for more older audience. It’s not going to be a giant budget with a lot of special effects; it’s going to be more of a horror movie and a thriller movie, not a superhero one.  Like the film   says hes "aggressively pursuing the Spawn reboot. 

In August 2013, McFarlane hopes to start shooting in 2014, with the studio wanting his script in by December.  On October 10, 2013, McFarlane revealed to Assignment X that he wants the film to be an R-rated supernatural thriller without the superhero elements associated with Spawn. 

==Soundtrack==
{{Infobox album  
| Name        = Spawn: The Album
| Type        = soundtrack
| Artist      = Various
| Cover       = spawnsoundtrackcover.jpg
| Released    = July 29, 1997
| Recorded    =  experimental
| Length      =  Sony
| Producer    = Various
| Misc        = {{Singles
  | Name          = Spawn: The Album
  | Type          = Soundtrack
  | single 1      = (Cant You) Trip Like I Do
  | single 1 date = October 7, 1997
  | single 2      = Long Hard Road Out of Hell
  | single 2 date = November 11, 1997
  }}
}}
{{Album ratings rev1 = AllMusic rev1score =    rev2 = Entertainment Weekly rev2score = (A) 08/08/1997 rev3 = Rolling Stone rev3score =   08/21/1997
}} rock bands Marilyn Manson, electronic producers Incubus and hip hop-infused Judgment Night Gold for selling over 500,000 copies in America.   

There are several limited editions of the soundtrack. A limited US version featuring different cover artwork plus the bonus track "This Is Not a Dream" (UK Mix) by Apollo 440, Morphine (band)|Morphine, DJ Spooky, Butthole Surfers and DJ Greyboy an Australian version featuring yet another cover (with the same image as on Spawn #39 and the marquee of Spawn: In the Demons Hand) plus the bonus track; and a Japanese version with the same cover art as the Australian, including a bonus disc containing three remixes as well as the extra track 15. The McFarlane Collectors Club made an LP release available to members featuring the standard album art and a translucent red disc. 

;Track listing Filter & The Crystal Method&nbsp;– 4:28 Marilyn Manson & Sneaker Pimps&nbsp;– 4:21 Orbital & Kirk Hammett&nbsp;– 3:45
# "Kick the P.A." - Korn & The Dust Brothers&nbsp;– 3:21
# "Tiny Rubberband" - Butthole Surfers & Moby&nbsp;– 4:12 For Whom the Bell Tolls (The Irony of It All)" - Metallica & DJ Spooky&nbsp;– 4:39
# "Torn Apart" - Stabbing Westward & Josh Wink|Wink&nbsp;– 4:53 Skin Up Pin Up" - Mansun & 808 State&nbsp;– 5:27
# "One Man Army" - The Prodigy & Tom Morello&nbsp;– 4:14
# "Spawn Again|Spawn" - Silverchair & Vitro&nbsp;– 4:28
# "T-4 Strain" - Henry Rollins & Goldie&nbsp;– 5:19 Incubus & DJ Greyboy&nbsp;– 3:22
# "No Remorse (I Wanna Die)" - Slayer & Atari Teenage Riot&nbsp;– 4:16
# "A Plane Scraped Its Belly on a Sooty Yellow Moon" - Soul Coughing & Roni Size&nbsp;– 5:26 When Worlds Collide" - Powerman 5000&nbsp;- 5:30                         
;Bonus Tracks                                                                                                                          Morphine
                                            
===Chart positions===
{| class="wikitable sortable"
!Chart (1997)
!Peak position
|-
 
|-
 
|-
 
|-
 
|-
 
|-
 
|-
 
|-
 
|}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 