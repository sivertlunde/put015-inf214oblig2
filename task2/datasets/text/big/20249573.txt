The Blue Hills
{{Infobox Film
| name           = Sinimäed  ( ) 
| image          = Raimo-jf5erand-sinime4ed-28200629-b70b-1995.jpg
| image_size     = 
| caption        = 
| director       = Raimo Jõerand
| producer       = Kiur Aarma
| writer         = Kiur Aarma, Eerik-Niiles Kross, Raimo Jõerand 
| narrator       = 
| starring       = 
| music          = Ardo Varres
| cinematography = Manfred Vainokivi
| editing        = 
| distributor    = 
| released       = 8 May 2006
| runtime        = 59 minutes
| country        = Estonia
| language       = Estonian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 2006 Cinema Estonian film directed by Raimo Jõerand. It is a documentary film about the 1944 Battle of Tannenberg Line. Based upon the war diaries of Paul Maitla, the film tells the story of this epic battle and the young Estonian men who had to choose between two totalitarian powers. The film received special mention at the 10th Tallinn Black Nights Film Festival.   

==Cast==
*Mait Malmsten – Narrator (voice)
*Taavi Teplenkov – Col. Maitla (voice)

==References==
 

==External links==
* ) }}

 
 
 
 
 
 
 


 
 