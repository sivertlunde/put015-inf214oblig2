Body and Soul (1931 film)
{{Infobox film
| name           = Body and Soul
| image          = Body and Soul poster.jpg
| image size     = 200px
| caption        = Theatrical poster
| director       = Alfred Santell William Fox
| based on       =   and A. E. Thomas}}
| writer         = Jules Furthman
| narrator       =
| starring       = Charles Farrell Elissa Landi Humphrey Bogart Myrna Loy
| music          = Peter Brunelli  
| cinematography = Glen MacWilliams
| editing        = Paul Weatherwax
| studio         = Fox Film Corporation
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Body and Soul (1931) is an action and drama film directed by Alfred Santell. The story, adapted from the stage play Squadrons by Elliott White Springs and A.E. Thomas, depicts Royal Air Force pilots in World War I. The film stars Charles Farrell, Elissa Landi, Humphrey Bogart and Myrna Loy.  

==Plot== shoot down an enemy balloon for his first flight with the squadron. Secretly, Mal joins him aboard the aircraft and when Jim is killed in the air battle, his friend manages to complete the mission and make it look like the dead pilot was a hero. 

At the base, Jims wife Carla (Elissa Landi) is mistaken for "Pom Pom," his mistress. Mal falls in love with Carla and when Alice Lester (Myrna Loy), the real "Pom Pom", appears, she finds out that Tap is about to fly a mission. Lester is a German spy whose information sent to the enemy, results in Tap being killed. When Mal realizes that Carla is Jims widow and not his mistress, he sets off on another mission, with the hope that he will return to his true love.

==Cast==
* Charles Farrell as Mal Andrews
* Elissa Landi as Carla  
* Humphrey Bogart as Jim Watson
* Myrna Loy as Alice Lester ("Pom Pom")
* Don Dillaway as Tap Johnson
* Crauford Kent as Major Burke
* Pat Somerset as Major Knowls
* Ian MacLaren as General Trafford-Jones
* David Cavendish as Lieutenant Meggs (Dennis DAuburn)
* Douglas Dray as Zane
* Harold Kinney as Young
* Bruce Warren as Sam Douglas

==Production==
 
Body and Soul began location shooting on November 29, 1930, at the Russell Movie Ranch in Agoura, California, with a modicum of flying.  A Travel Air 4000 biplane that had been flown in Hells Angels (film)|Hells Angels (1930), disguised as a British World War I fighter aircraft, was the only actual aircraft acquired for the production.  A combination of flying sequences, matched to sound stage process shots at the studio where the Travel Air was again used, completed the aerial scenes. Orriss 2013, p. 59.  Production wrapped up on January 2, 1931. 

==Reception==
The main attraction of Body and Soul was in the drama and the introduction of Elissa Landi to North American audiences. In  , March 14, 1931. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Maltin, Leonard. Leonard Maltins Movie Encyclopedia. New York: Dutton, 1994. ISBN 0-525-93635-1.
* Orriss, Bruce W. When Hollywood Ruled the Skies: The Aviation Film Classics of World War I. Los Angeles: Aero Associates, 2013. ISBN 978-0-692-02004-3.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 