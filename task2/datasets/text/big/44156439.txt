Une histoire banale
{{Infobox film
| name           = Une histoire banale
| image          = 
| caption        =
| director       = Audrey Estrougo
| producer       = Audrey Estrougo   Lauren Grall 
| writer         = Audrey Estrougo 
| starring       = Marie Denarnaud   Marie-Sohna Conde   Oumar Diaw   Renaud Astegiani
| music          = James "BKS" Edjouma
| cinematography = Guillaume Schiffman Julien Malichier	 
| editing        = Céline Cloarec 
| studio         = Six Onze Films   Les Canards Sauvages
| distributor    = Damned Distribution
| released       =  
| runtime        = 82 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Une histoire banale is a 2014 French drama film written, produced and directed by Audrey Estrougo. It tells the story of Nathalie, an ordinary young woman who works in the health care industry. However her life takes an abrupt turn during an encounter with a co-worker on one fateful evening. The film was made with an €8,000 budget that was raised through crowdfunding. Filming took place in Estrougos flat over a period of three weeks. 

== Cast ==
* Marie Denarnaud as Nathalie 
* Marie-Sohna Conde as Sonha
* Oumar Diaw as Wilson  
* Renaud Astegiani as Damien 
* Vincent Londez as Calixte  
* Steve Tran as Steve 
* Frédéric Duff-Barbé as the Police Lieutenant
* Naidra Ayadi as Nathalies colleague
* Benjamin Siksou as the man in the washroom
* Nicolas Gob as Fabrice
* Aurore Broutin as the girl in the street
* Gladys Gambie as the dance teacher

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 