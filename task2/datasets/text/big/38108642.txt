Destruction Force
{{Infobox film
 | name = Destruction Force
 | image = Destruction Force.jpg
 | caption =
 | director = Stelvio Massi
 | writer =
 | starring =
 | music = Bruno Canfora
 | cinematography = Franco Delli Colli
 | editing =
 | producer =
 | distributor =
 | released =
 | runtime =
 | awards =
 | country =
 | language =
 | budget =
 }} Italian poliziottesco-comedy film directed by Stelvio Massi. It is the second film in which Tomas Milian plays the character of Monnezza after Free Hand for a Tough Cop (Il trucido e lo sbirro).       

The participation of Milian was due to an old contract he was forced to fulfill;  originally intended as a special appearance, the role of Milian was expanded to make it appear as the star of the film, and Milian obtained in exchange to construct his character as he wanted and to write (uncredited) his dialogue lines.  The film was shot in three weeks, but Milian filmed his part in just five days. 

== Cast ==
*Luc Merenda: Commissioner Ghini
*Tomas Milian: Sergio Marazzi, aka "Er Monnezza"
*Massimo Vanni: Marchetti
*Elio Zamuto: Belli
*Franco Citti: Lanza
*Mario Brega:Quaestor Alberti 
*Imma Piro: Agnese Rinaldi

==References==
 

==External links==
* 

 
 
 
 


 
 