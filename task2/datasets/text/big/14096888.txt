The Price of Sugar
  Vicini family to stifle efforts to change the situation.

While the documentary highlights the efforts of Father Christopher Hartley to bring medicine, education, and human rights to Haitian workers, it also shows the widespread resentment of his actions held by Dominican people.

==Critical reception==

The documentary won the audience award at the 2007 South by Southwest Film Festival.

On November 19, 2007, The Price of Sugar was named by the Academy of Motion Picture Arts and Sciences as one of 15 films on its documentary feature Oscar shortlist. 

The documentary did not make the nomination list for the Oscar Documentary Feature category. 

==Defamation lawsuit== First Circuit Court of Appeals, the Vicini family "later winnowed the number of allegedly defamatory statements down to seven".  The Appeals Court upheld a judgment from a lower court that the Vicini brothers were "public figures under the circumstances". The brothers thus must prove that the filmmakers made false depictions and knew about it. If they had been private figures, as the plaintiffs had unsuccessfully tried to prove, the filmmakers could have been liable for publishing information without verifying its truth. The appeals court sent the case back to the lower court to decide if the filmmakers have to hand over a report that they prepared to obtain insurance coverage for the film. After that, the lower court can determine whether information shown in the film was false and, if it was the case, if the filmmakers knew about it. 

==Improvement of living conditions==
According to a report by NPR, the living conditions of Haitian workers depicted in the film have improved to some extent soon after the film. New houses with electricity and water have been built together with rural clinics. The guards no longer carry guns, and the Haitians can leave the plantation.   Though, recent service workers from the U.S. who have traveled to the La Romana region of the Dominican Republic to partner with The Good Samaritan Hospital in providing relief efforts to the bateyes report different findings from what NPR reports.  In the network of more than 200 bateyes around La Romana, more than 50% have undrinkable water.   Most do not have electricity.  Guards are often seen with firearms.  While Haitians are allowed to leave the plantations, they lack the financial resources and/or relationships to do so.  Malnutrition and dehydration are the number one causes of death among children.

== See also ==
*The Sugar Babies The Last Mountain, a 2011 film by Haney and Grunebaum

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 