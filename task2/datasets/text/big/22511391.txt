Mr. Sleeman Is Coming
 
{{Infobox film
| name           = Mr. Sleeman Is Coming
| image          = 
| image_size     = 
| caption        = 
| director       = Ingmar Bergman
| producer       = Henrik Dyfverman
| writer         = Hjalmar Bergman
| starring       = Bibi Andersson
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 43 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}

Mr. Sleeman Is Coming ( ) is a 1917 one-act play by the Swedish author Hjalmar Bergman. The main character is an orphaned young woman who is about to be married off to an unappealing but rich old man, Mr. Sleeman, at the instigation of her aunts who have taken charge of her. Bergman infuses the situation with overtones of rueful pessimism concerning life in general.
 Swedish television. In 1957, Ingmar Bergman (no relation to the author) directed the first TV adaptation.   

==Cast==
* Bibi Andersson – Anne-Marie
* Jullan Kindahl – Mrs. Mina
* Yngve Nordwall – Mr. Sleeman
* Max von Sydow – The hunter
* Naima Wifstrand – Mrs. Gina

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 