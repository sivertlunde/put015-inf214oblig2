Love from a Stranger (1937 film)
{{Infobox film
| name           = Love from a Stranger
| image          = Love From a Stranger movie poster.jpg
| caption        = 1937 US Theatrical Poster
| director       = Rowland V. Lee
| producer       = Harry E. Edington (associate producer) Max Schach (producer)
| writer         = Agatha Christie (story Philomel Cottage) Frank Vosper (play) Frances Marion
| narrator       = 
| starring = Ann Harding Basil Rathbone Binnie Hale Bruce Seton
| music          = Benjamin Britten
| cinematography = Philip Tannura
| editing        = Howard ONeill
| studio         = Trafalgar Films
| distributor    = United Artists
| released       = January 1937   18 April 1937 (US)
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 play of the same name by Frank Vosper. In turn, the play was based on the 1924 short story Philomel Cottage, written by Agatha Christie.
 independent Trafalgar Films at Denham Studios near London.  It is also known by the alternative title A Night of Terror in the United States

== Cast ==
*Ann Harding as Carol Howard
*Basil Rathbone as Gerald Lovell
*Binnie Hale as Kate Meadows
*Bruce Seton as Ronald Bruce
*Jean Cadell as Aunt Lou
*Bryan Powley as Doctor Gribble
*Joan Hickson as Emmy
*Donald Calthrop as Hobson
*Eugene Leahy as Mr. Tuttle

== Reception == Public Enemy. A woman in front of me let out a scream like a steamship siren at this point in the first performance. That scream was the natural voice of criticism testifying to the films success." 

The Scotsman of June 22, 1937 started off its review by saying, "Suspense is cleverly created and sustained in this film version of the late Frank Vospers play." The reviewer continued, "The suspicion that she has married a murderer is cunningly built up; his homicidal mania, strangely mixed up with greed and sadism, is made plausible and eerily convincing; and the closing sequence, in which the wife, sensing his murderous intention, seeks frantically, almost despairingly, for some escape, achieves dramatic suspense of an intensity only occasionally encountered on the screen. Much of the effect is due to the acting. Ann Harding brings a strong, yet restrained emotion to her part, even when it trembles of the verge of melodramatic insanity, and Basil Rathbone terrifyingly combines sensitiveness and insanity in a polished and persuasive performance." 

==References==
 

==Bibliography==
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 