Thieves (1996 film)
{{Infobox film
| name           = Thieves (Les Voleurs)
| image          = Le voleurs.jpg
| caption        = Theatrical release poster
| director       = André Téchiné
| producer       = Alain Sarde
| writer         = André Téchiné  Gilles Taurand   Michel Alexandre
| starring       = Daniel Auteuil Catherine Deneuve    
| music          = Philippe Sarde
| cinematography = Jeanne Lapoirie
| editing        = Martine Giordano
| distributor    = 
| released       =  
| runtime        = 117 minutes
| country        = France 
| language       = French
| budget         = 8.400.000 Euro sign|€    
| gross          =$17,145,357  
}}
Thieves ( ) is a 1996 French drama film directed by André Téchiné, starring Daniel Auteuil, Catherine Deneuve and  . Marshall, André Téchiné, p. 157   The plot follows a cynical police officer, who comes from a family of thieves, and a lonely philosophy professor, both romantically involved with a self-destructive petty criminal. With a puzzling structure, the story is told through a series of flashbacks presented from four different perspectives.

== Plot ==
Justin, a ten-year-old boy living in a small town in the Rhône-Alpes region, is awaken in the middle of the night. His mother, Mireille is crying and his widower grandfather, Victor, tells the boy that his father, Ivan, is dead. The next day, Alex, Justin’s uncle and Ivan’s younger brother, arrives for the funeral accompanied by a young woman, Juliette. Justin notice the tension between the adults. He takes his fathers gun and hides it. The story flashes backward and forward to explain what led up to Ivan’s death. 

Alex, who has rebelled against his family by becoming a cop, works in the deprived La Duchere district of Lyon. He begins with his encounter, a year before Ivan’s death, when he first meets Juliette, who is brought to his office when she was arrested for shoplifting. He lets her off. He sees her again one day when he visits the sleazy nightclub run by his older brother, Ivan. Ivan is a crime boss and runs a popular club with a drag revue, funding his business through a stolen-auto ring headed up by Jimmy, who is Juliettes brother. The encounter between the siblings on opposite sides of the law is tense, they barely get along. Ivan is happy to show Alex that he is doing much better than him on the wrong side of the law. After their second encounter Alex and Juliette start a tortured affair beginning to meet regularly for sex. He is not in love with her, and their relationship is complicated by the young womans emotional and sexual involvement with Marie, a professor of philosophy. Alex spies on Marie. Ivan hears about Juliette’s relationship with Alex and threatens her. Ivan and his gang, including Juliette, plan the theft of cars in a railway deposit.

The heist at the railway marshaling yard goes disastrously wrong when Ivan is shot dead. Juliette, who had been coerced into participating, against Jimmy’s wishes, is distraught by Ivan’s death.  Emotional imbalance and self-destructive, Juliette tries to commit suicide twice, first while staying with her brother and later while visiting Marie, after Ivan’s death. Marie has her interned in a psychiatric hospital to recuperate. Alex asks Marie for the address of Juliette’s clinic, but she has disappeared. In search of Juliette, Marie visits her housing estate apartment. After Ivan’s cremation, Alex makes an effort to connect with his nephew without much success. On a local fair, Justin threatens Alex with a gun. Justin learns the truth about his father’s death from his grandfather Victor.

Marie visits Alex, who tells her Juliette was involved in the heist and that he is trying to find her. Alex and Marie develop an uneasy friendship as they bid, from a distance, to protect Juliette. Later Alex visits Marie and finds she has become a recluse, writing a book about Juliette. He tells her Juliette is safe because the police are looking for a boy. Later Alex hears that Marie has committed suicide as he receives her manuscript and the tapes of Juliette’s voice in the post. Alex goes to see Juliette in Marseilles, but leaves her alone when he sees she is well from a distance. She has built a new life for herself and works in a bookshop. Justin has taken after his father and grandfather and does not like his uncle. By contrast, Justin gets along very well with Jimmy. Jimmy decides to marry Ivans widow, Mireille.

==Cast==
* Daniel Auteuil as Alex
* Catherine Deneuve as Marie Leblanc
*   as Juliette Fontana
* Benoît Magimel as Jimmy Fontana
* Didier Bezace as Ivan
* Julien Rivière as Justin
* Ivan Desny as Victor
* Fabienne Babe as Mireille
* Régis Betoule as Régis
* Pierre Perez as Fred
* Naguime Bendidi as Nabil

== Reception ==
The film premiered in May 1996 at the 49th Cannes Film Festival.    It was released in France on 21 August 1996  and in the United States on 25 December that same year.
 
Thieves received widespread praised from film critics. It is generally considered, with My Favorite Season and Wild Reeds, among Téchiné’s best films. Marshall, André Téchiné, p. 1   Review aggregate Rotten Tomatoes  reports that 82% of 11 critics have given the film a positive review, holding an average score of 7/10.      

On line film critic James Berardinelli called the film  "strong and keenly-insightful ".  Roger Ebert from the Chicago Sun-Times wrote that "Téchiné involves us in a subtle, gradual process of discovery; each piece changes the relationship of the others. He is so wise about these criminals, he makes the bad guys in most American films look like cartoon characters." 
 Mike Clark  commented that in Thieves “Deneuve gives one of her best performances ever, though shes not exactly the movies focus. Of the many pleasures here, one is trying to guess just who the dramatic focal point is".  Janet Maslin in the New York Times reported that in the film "Téchiné explores the intricate connections among these characters with a somber, penetrating wisdom that is the films main reward."  Jonathan Rosenbaum from Chicago Reader called the film "a Masterpiece" 

The film received five nomination to the Cesar Awards, including the categories of Best Film, Best Director, Best Actress (Catherine Deneuve), Best Young Actor (Benoît Magimel) and Best Young Actress (Laurence Côte). It won only in the last category. In addition, Benoît Magimel received the Prix Michel Simon as Best Actor for his performance in Thieves.

==Productiom==
Thieves was directed by André Téchiné in the style of film noir. Its fragmented structure has been compared to a William Faulkners novel, particularly to The Sound and the Fury (1929). Marshall, André Téchiné, p. 52   The narrative — consisting of a prologue, five sections, and an epilogue — leaps around in time and switches between the viewpoints of Justin, Alex, Marie, and Juliette. The Sound and the Fury unfolds over four days, likewise, switching narrative perspectives. Both The Sound and the Fury and Thieves also have a central female character who is loved obsessively by two of the narrators: Faulkner’s novel has Caddie, who is loved by her brothers Benjy and Quentin, and Téchiné’s film has Juliette, who is the love interest of both Alex and Marie. Marshall, André Téchiné, p. 53   The divided time structure emphasize the interlapping relationships between the characters. Marshall, André Téchiné, p. 54 

===Music===
The original score for the film was written by  , The Archies  and Mozart. Marshall, André Téchiné, p. 149  
* Douha Alia Cheb Mami
* Baadini Rachid Taha
* Sous le soleil de Bodega Les Négresses Vertes
* Sugar Sugar The Archies
* The Magic Flute Wolfgang Amadeus Mozart
* Tonite Les Rita Mitsouko
* La Valse Petite La Tordue 

==Home media == Sony Pictures French with English subtitles. Les Voleurs  is also available in Region 2 DVD.

==References==
 

==Further reading==
*Marshall, Bill. André Téchiné, Manchester University Press, 2007, ISBN 0-7190-5831-7

==External links==
* 

 

 
 
 
 
 
 
 
 