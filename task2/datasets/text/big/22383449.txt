The Married Couple of the Year Two
 
{{Infobox film
| name           = The Married Couple of the Year Two
| image          = The Married Couple of the Year Two.jpg
| caption        = Film poster
| director       = Jean-Paul Rappeneau
| producer       = Alain Poiré
| writer         = Daniel Boulanger Maurice Clavel Jean-Paul Rappeneau Claude Sautet
| narrator       = Jean-Pierre Marielle
| starring       = Jean-Paul Belmondo
| music          = Michel Legrand
| cinematography = Claude Renoir
| editing        = Pierre Gillette
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = France Italy Romania
| language       = French
| budget         =  gross = 2,822,567 admissions (France) 
}}

The Married Couple of the Year Two ( ) is a 1971 French comedy film directed by Jean-Paul Rappeneau. It was entered into the 1971 Cannes Film Festival.   
The title is a reference to “The Soldiers of Year II”, the conscripts raised by the Levée en masse in 1793 to defend the republic against foreign invaders.

==Plot==
Having killed a noble too friendly with his wife Charlotte, Nicolas Phillibert flees from France to South Carolina, where he does well and wants to marry a rich man’s daughter. To do so, he will first have to return to France and get a divorce. On landing at Nantes in 1793, the Reign of Terror is raging and he is arrested by the revolutionaries. Taken to a republican ceremony in the cathedral, he saves the life of a royalist girl, Pauline, and escapes with her to an isolated castle. There he finds Charlotte, claiming to be a widow, with Pauline’s brother Henri. A prince arrives from London to organise resistance in the War in the Vendée|Vendée and is struck by Charlotte, who was told by a gypsy that she would become a princess. She admits that she is married to Nicolas, so the prince has him drugged and carried into Nantes city hall to get a divorce. Put back on his ship for America, Nicolas’ divorce certificate blows overboard. Diving into the Loire, he swims ashore to find Charlotte again, but she has left with the prince for neutral Germany. Pursuing her across France in the throes of the Austrian invasion, he catches her at the frontier. Fifteen years later, Nicolas is made a prince by Napoleon and the gypsy’s prediction comes true.

==Cast==
* Jean-Paul Belmondo as Nicolas Philibert
* Marlène Jobert as Charlotte
* Laura Antonelli as Pauline
* Michel Auclair as Prince
* Julien Guiomar as Representative Mario David as Requiem
* Charles Denner as Traveller
* Georges Beller as Simon
* Paul Crauchet as Public Prosecutor
* Marc Dudicourt as Le chauve
* Patrick Préjean as Saint-Aubin Sim as Lucas
* Pierre Brasseur as Gosselin
* Sami Frey as Marquis de Guérandes (as Sami Frei)
* Jean Barney as Lorateur républicain

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 