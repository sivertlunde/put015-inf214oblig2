Melodies Old and New
{{Infobox film
| name           = Melodies Old and New
| image          =
| caption        = Edward Cahn
| producer       = Metro-Goldwyn-Mayer
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       = David Snell
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 53"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 203rd Our Gang short (204th episode, 115th talking short, 116th talking episode, and 35th MGM produced episode) that was released.

==Plot==
Just as they did in Ye Olde Minstrels, the gang prevails upon old-time minstrel impresario Uncle Wills to help them stage a fund-raising musical show. Highlights include the ensemble number "When Grandma Wore a Bustle", the barbershop-quartet set piece "Songs of Long Ago", and the grand finale "Dances Old and New". The kids are unable to post the profits because Mickey has allowed most of the audience to enter for free, but Uncle Wills comes to the rescue once again.   

==Cast==
===The Gang===
* Janet Burston as Janet Mickey Gubitosi (later known as Robert Blake) as Mickey
* Billy Laughlin as Froggy
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* Walter Wills as Uncle Walt Wills

===Dancers and audience members===
Lavonne Battle, Shiela Brown, Shirley Jean Doble, Donna Jean Edmondson, Eddie Ehrhardt, James Gubitosi, Dwayne Hickman, Dickie Humphries, Robert Morris, Kay Tapscott, Frank Lester Ward, Patricia Wheeler

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 