The Shade (1998 film)
{{Infobox film
| name           = The Shade
| image          =
| caption        = Theatrical poster
| director       = Raphael Nadjari
| producer       = Geoffroy Grison Francesca Feder
| writer         = Raphael Nadjari
| starring       = Richard Edson Lorie Marino
| music          = John Surman
| cinematography = Laurent Brunet
| editing        = Tom Donahue
| released       = March 1, 2000 (France)
| country        = United States
| language       = English
}}

The Shade is Raphael Nadjaris debut feature. This is a modern adaptation of Fyodor Dostoyevskys A Gentle Creature, and takes place in contemporary New York City.

==Plot==
The film tells the story of a Jewish middle aged pawnbroker who meets a mysterious woman who will become his wife without their truly knowing each other. The film begins with Simon, alone in his apartment with the corpse of his wife, Anna, who has just committed suicide. In his grief, he remembers the first time he met her, a year ago when she walked into his pawnbrokers shop in Spanish Harlem. Mysterious Anna, who seems to come from nowhere, impresses solitary Simon with her beauty, and he proposes to her on their first night out. They then enter into a passionate relationship that will lead her to death.

==Awards and nominations==
* 1999 Cannes Film Festival - Official Selection, Un Certain Regard   
* Bergamo Film Festival (1999) - Audience Award

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 