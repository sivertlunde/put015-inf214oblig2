Linda Sara
{{Infobox film
| name           = Linda Sara
| image          =
| director       = Jacobo Morales
| producer       = Blanca Silvia Eró
| writer         = Jacobo Morales Daniel Lugo Johanna Rosaly Adamari López
| music          = Pedro Rivera Toledo
| cinematography = Milton Graña
| editing        = Alfonso Borrell
| production_company = Cinesí, Inc.
| distributor    =
| released       = 1994
| runtime        = 107 min
| country        = Puerto Rico
| awards         =
| language       = Spanish
| budget         =
| gross          =
}} Puerto Rican film directed by Jacobo Morales and starring singer Chayanne and former Miss Universe Dayanara Torres. The film was released in 1994 in film|1994.

==Cast==
* Dayanara Torres - Young Sara
* Chayanne - Young Alejandro Daniel Lugo - Gustavo
* Johanna Rosaly - Sofía
* Jacobo Morales - Pablo
* Jorge Luis Ramos - Mario
* Joan Amick - Sara
* Adamari López - Tita
* Benjamín Morales - Don Alejandro
* Jorge Javier Melani - Tavito

==See also==
* Cinema of Puerto Rico
*List of Puerto Ricans in the Academy Awards

==External links==
*  at the Internet Movie Database

 
 
 
 
 


 
 