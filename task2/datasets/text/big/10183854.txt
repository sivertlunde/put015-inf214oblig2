Head of the Family
{{Infobox film
| name           = Head of the Family
| image          = 
| image_size     =
| caption        = 
| director       = Charles Band (as Robert Talbot)
| producer       = Charles Band Kirk Edward Hansen
| screenplay     = Benjamin Carr
| based on       = 
| narrator       =
| starring       = Blake Adams Jacqueline Lovell J.W. Perra Bob Schott Alexandria Quinn
| music          = Richard Band Steven Morell
| cinematography = 
| editing        = 
| distributor    = Full Moon Entertainment
| released       =   (USA video premiere)
| runtime        = 82 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
 1996 b-movie black comedy released by Full Moon Features. It concerns a Southern couple who blackmail a family of mutants to get money and revenge.

==Plot==
Howard (Gordon Jennison Noice) is the meanest nastiest thug in town, a  ); and, one is extremely intelligent. The whole family is run by the one who has super intelligence, the head of the family from the title, Myron (J.W. Perra). Little more than a giant head with hands in a wheelchair, Myron psychically controls his other siblings, but seeks more. When idiotic locals fall for his trap, he experiments on their brains, trying to find a normal body to house his superior intellect. Lance blackmails the Stackpools with their secret, getting them to kill Howard and demanding $2,000 a week in cash. (The Stackpools are rich in oil and coal among other things) Eventually Myron tires of Lances bottom-feeding, and captures him and Loretta, to get them to destroy the evidence of their secret. To force Lances hand, he puts Loretta in a mock play of Joan of Arc in the basement, complete with a burning at the stake. The dumb strong one, seeing the pretty girl in trouble, carries her off before she can be hurt, and burns the house down. With the Stackpools and Lance dead, the ever scheming Loretta realizes that the big dumb one is the heir to the family riches. She marries him inheriting all the Stackpool fortunes.  The ending, however, suggests that Myron is still alive and is controlling the dumb one again....

==Sequel==
On June 25, 2008, director Charles Band announced that he would be making a sequel entitled Bride of the Head of the Family.  The sequel had originally been intended to be made in the late 1990s, but the plans for the film fell through.  This version never came into fruition.

==See Also==

* The Invisible Maniac
* Castle Freak


==External links==
* 

 

 
 
 
 
 

 