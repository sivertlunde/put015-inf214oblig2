La liceale al mare con l'amica di papà
{{Infobox film
 | name = La liceale al mare con lamica di papà
 | image = Liceale_mare.jpg
 | caption =
 | director = Marino Girolami
 | writer =  Gino Capone
 | starring = Renzo Montagnani
 | music =      Ubaldo Continiello 
 | cinematography = Federico Zanni     
 | editing =  	   
 | producer = Camillo Teti
 | released       = 10 December 1980
 | runtime        = 90 minutes
 | country        = Italy
 | language       = Italian
 }} 1980 commedia sexy allitaliana film directed by Marino Girolami and starring Renzo Montagnani. Although presented as the latest installment in the commercially successful Liceale series,  the film despite its title and the token blonde high school girl character, has little to no resemblance to earlier films starring Gloria Guida.

==Plot== Apulian coast of the Adriatic. Nevertheless, Massimo seems to find a solution. His raunchy daughter Sonia (Sabrina Siani) has failed in her Italian compensation exam, and he plans to present Laura as a nun who will give Sonia private lessons to re-take her exam. On the other hand, there are two dim-witted criminals, Terenzio (Alvaro Vitali) and Fulgenzio (Gianni Ciardo), who are planning to kidnap one of the Castaldi family for good ransom money.

==Cast==
*Renzo Montagnani: Massimo Castaldi
*Marisa Mell: Violante Castaldi
*Alvaro Vitali: Terenzio
*Gianni Ciardo: Fulgenzio
*Cinzia De Ponti: Laura
*Sabrina Siani: Sonia Castaldi
*Andrea Brambilla: Arcibaldo
*Lucio Montanaro: Gustavo

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 