The Tartars
{{Infobox film
| name           = The Tartars
| image          = The Tartars (film).jpg
| image size     =
| caption        = Original film poster
| director       = Richard Thorpe
| producer       = Riccardo Gualino
| writer         = 
| based on       = 
| narrator       =
| starring       = Victor Mature Orson Welles Renzo Rossellini
| cinematography = Amerigo Gengarelli
| editing        = 
| studio         = Lux Film
| distributor    = MGM
| released       = 1961
| runtime        = 
| country        = Italy English
| budget         = 
| gross = 
}}
The Tartars/I Tartari is a 1961 Italian/Yugoslavian international co-production film starring Victor Mature and Orson Welles. 

==Cast==
*Victor Mature  ...  Oleg Burundai
*Liana Orfei  ...  Helga
*Arnoldo Foà  ...  Ciu Lang
*Luciano Marin  ...  Eric
*Bella Cortez  ...  Samia
*Furio Meniconi  ...  Sigrun
*Folco Lulli  ...  Togrul

==Reception==
According to MGM records the movie made a profit of $34,000.  .  Orson Welles enunciation has been praised while Victor Mature has been considered a miscast for not having the looks of an archetypical Viking. 

==Biography==
* 

==References==
 

==External links==
*  at IMDB

 
 
 
 
 
 
 
 
 
 


 