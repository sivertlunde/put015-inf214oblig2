Life Without Soul
{{Infobox film
| name           = Life Without Soul
| image          =
| caption        =
| director       = Joseph W. Smiley
| producer       = John I. Dudley
| writer         = Jesse J. Goldburg
| narrator       =
| starring       = Percy Standing George De Carlton
| music          =
| cinematography =
| editing        =
| distributor    = Ocean Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
| website        =
}}

Life Without Soul (1915 in film|1915) is a horror film, directed by Joseph W. Smiley and written by Jesse J. Goldburg. This film is an adaptation of Mary Shelleys Gothic novel Frankenstein. The film is about a doctor who creates a soulless man. In the end, it turns out that a young man has dreamed the events of the film after falling asleep reading Mary Shelleys novel.

This version is considered a lost film and the second film version of Frankenstein.  The first version was the Edison Manufacturing Companys 12-minute short film Frankenstein (1910 film)|Frankenstein (1910), written and directed by J. Searle Dawley.

==Production==

This full-length film (broken into five parts), was produced by the Ocean Film Corporation and featured English-born actor Percy Darrell Standing wearing little to no make-up as the Brute Man. The story is about the Brute Man killing the sister of his creator (Dr. William Frawley) on her wedding night. Frawley pursues his creation across Europe finally killing him by shooting him. Frawley then dies of exhaustion.
A framing device reveals that the story is being read from a book.
The film was reissued in 1916 by the Raver Film Corporation with added scientific documentary footage detailing the reproduction methods of fish. 

== Cast ==
* Percy Standing as The Creation (aka Brute Man)
* George De Carlton as Frankensteins Father 
* Lucy Cotton as Elizabeth Lavenza 
* Pauline Curley as Claudia Frawley 
* Jack Hopkins as Henry Claridge
* David McCauley a Victor Frawley, as a child 
* Violet De Biccari as Elizabeth, as a child 
* William A. Cohill as Dr. William Frawley

==See also==
*List of lost films

==References==
 

==External links==
*  at Allmovie
*  at the Internet Movie Database

 

 
 
 
 
 
 
 
 
 
 


 