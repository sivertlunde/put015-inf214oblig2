Intimate Strangers (2004 film)
 Intimate Stranger}}
{{Infobox film image              =Intimate Strangers poster.jpg name               =Intimate Strangers director           =Patrice Leconte producer           =Alain Sarde writer             =Jerome Tonnerre Patrice Leconte starring           =Sandrine Bonnaire Fabrice Luchini Michel Duchaussoy music              =Pascal Estève cinematography     =Eduardo Serra editing            =Joëlle Hache Paramount Classics  (USA) |
| released       =   executive producer = Christine Gozlan runtime            =103 minutes language           =French gross              =$7,120,939
}}
Intimate Strangers ( ) is a 2004 French film directed by Patrice Leconte. It was shown in Competition at the 2004 Berlin Film Festival.
 Paramount Classics acquired the United States distribution rights of this film and gave it a limited United States|U.S. theatrical release on July 30, 2004; this film went on grossing $2.1 million in the United States theaters,  which is considered a good result for a foreign language film.  Ruth Vitale (who was the president of Paramount Classics at that time) was pleased with this films performance in the United States market. 

==Plot==
On her initial appointment seeking psychiatric counseling, a troubled woman mistakenly enters a tax accountants office, and an unusual relationship develops.

==Cast==
* Sandrine Bonnaire as Anna
* Fabrice Luchini as William
* Michel Duchaussoy as Dr. Monnier
* Anne Brochet as Jeanne
* Gilbert Melki as Marc
* Laurent Gamelon as Luc
* Hélène Surgère as Mrs. Mulon

==Hollywood remake==
Paramount Pictures (the parent studio of Paramount Vantage/Classics) is developing a Hollywood remake version of this film, with Hilary Swank attached to star. 

==References==

 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 