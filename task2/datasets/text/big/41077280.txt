Control (2013 film)
 
 
{{Infobox film
| name           = Control
| image          = Control 2013 poster.jpg
| alt            = 
| caption        = 
| director       = Kenneth Bi
| producer       = Wang Zhonglei   Stephen Fung   Henry Fong   Stephen Lam
| writer         = Kenneth Bi
| starring       = Daniel Wu   Yao Chen   Simon Yam   Leon Dai   Ady An   Shao Bing
| music          = 
| cinematography = Roman Jakobi
| editing        = 
| studio         = Sil-Metropole Organisation   Huayi Brothers   Celestial Pictures   Media Asia Films   Le Vision Pictures
| distributor    = Sil-Metropole Organisation
| released       =   
| runtime        = 90 minutes
| country        = China   Hong Kong
| language       = Mandarin
| budget         = 
| gross          = $7,020,000 
}}
Control (Chinese: 控制) is a 2013 Chinese-Hong Kong film directed by Kenneth Bi and starring Daniel Wu, Yao Chen, Simon Yam, Leon Dai, Ady An and Shao Bing. 

==Cast==
* Daniel Wu as Mark
* Yao Chen as Jessica
* Simon Yam as Tiger
* Leon Dai as Devil
* Ady An as Mimi
* Shao Bing as Sam

==References==
 

==External links==
*  

 

 
 
 
 

 