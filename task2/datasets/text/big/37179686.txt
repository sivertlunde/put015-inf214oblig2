The Leopardess
{{infobox film
| name           = The Leopardess
| image          =
| imagesize      =
| caption        =
| director       = Henry Kolker
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Katharine Newlin Burt (story) J. Clarkson Miller
| starring       = Alice Brady
| music          =
| cinematography = Gilbert Warrenton
| editing        = Thomas J. Geraghty
| distributor    = Paramount Pictures
| released       = March 23, 1923
| runtime        = 5-6 reels (5,621 feet)
| country        = United States Silent (English intertitles)
}} 1923 American silent South South Seas melodrama film produced by Famous Players-Lasky and distributed by Paramount Pictures.

The film was directed by Henry Kolker, starred Alice Brady in her next to last silent film, and is now thought to be a lost film.   

==Cast==
*Alice Brady - Tiare
*Edward Langford - Captain Croft
*Montagu Love - Scott Quaigg Charles Kent - Angus McKenzie
*George Beranger - Pepe (*George Andre Beranger)
*Marguerite Forrest - Evoa
*Glorie Eller - Mamoe

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 