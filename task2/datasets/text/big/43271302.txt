The Galapagos Affair
 

{{Infobox film
| name = The Galapagos Affair: Satan Came to Eden
| image = GalapagosAffairPoster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Daniel Geller Dayna Goldfine
| producer = Jonathan Dana Daniel Geller Dayna Goldfine Celeste Schaefer Snyder
| writer = Daniel Geller Dayna Goldfine Celeste Schaefer Snyder
| music = Laura Karpman
| cinematography = Daniel Geller
| editing = Bill Webber
| studio = Geller/Goldfine Productions
| distributor = Zeitgeist Films
| released =  
| runtime = 120 minutes
| country = United States
| language = English
| budget = 
| gross = $247,159 (USA) 
}}

 
The Galapagos Affair: Satan Came to Eden is a 2013 feature-length documentary directed by Daniel Geller and Dayna Goldfine. It is about a series of unsolved disappearances on the Galapagos island of Floreana in the 1930s among the largely European expatriate residents at the time. The voice cast includes Cate Blanchett, Sebastian Koch, Thomas Kretschmann, Diane Kruger, Connie Nielsen, Josh Radnor and Gustaf Skarsgård.   

==Release==
The film premiered at the 40th Telluride Film Festival on September 2, 2013. It was an official selection of the Hamptons International Film Festival on October 12, 2013 and Berlin International Film Festival on February 10, 2014.   It opened theatrically in the US on April 4, 2014. 

==Critical reception==
The Galapagos Affair: Satan Came to Eden received generally positive reviews from critics. The review aggregator Rotten Tomatoes reported that 82% of critics gave the film positive reviews, based on 49 reviews.  Metacritic reported the film had a weighted average score of 67 out of 100, based on 20 reviews. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 