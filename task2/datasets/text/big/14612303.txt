Mail Order Bride (1964 film)
{{Infobox film
| name           = Mail Order Bride
| image_size     = 
| image	=	Mail Order Bride FilmPoster.jpeg
| caption        = 
| director       = Burt Kennedy
| producer       = Richard E. Lyons
| writer         = Van Cort (story) Burt Kennedy
| narrator       = 
| starring       = Buddy Ebsen Keir Dullea Lois Nettleton
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $700,000 Wayne Will Play Harms Way Role: Hell Portray Adm. Torrey; Ebsen Film Setting Records
Hopper, Hedda. Los Angeles Times (1923-Current File)   17 Feb 1964: C16.  
| gross          = $1,250,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
}}
Mail Order Bride is a 1964 western film starring Buddy Ebsen, Keir Dullea and Lois Nettleton, directed by Burt Kennedy. An old man who pressures the wild son of a dead friend into marrying a mail-order bride in an attempt to settle him down.

==Plot==
Retired lawman Will Lane promises to look after a dying friends son. He is given the deed to the mans Montana ranch and instructed not to let the friends son, Lee Carey, have it until Lee gives up his immature ways.
 Kansas City with a six-year-old son.

Lee agrees to marry her, with ranch hand Jace as his best man, but assures Annie that their marriage will be in name only, with no other marital obligations. Lane learns that Jace has been stealing cattle. Lee refuses to believe it until Jace proposes they rustle together and leave the ranch in ruins.

But after a fire is set by Jace with the woman and boy still inside the house, Lee rescues them and comes to his senses. He now has a family and vows to rebuild the ranch, so Lane hands him the deed and rides home to Kansas City.

==Cast==
*Buddy Ebsen as Will Lane
*Keir Dullea as Lee Carey
*Lois Nettleton as Annie Boley
*Warren Oates as Jace
*Barbara Luna as Marietta
*Paul Fix as Sheriff Jess Linley
*Marie Windsor as Hanna
*Denver Pyle as Preacher Pope

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 


 