Trenchcoat (film)
{{Infobox film
| name = Trenchcoat
| image = Trenchcoatkidder.jpg
| director = Michael Tuchner Jeffrey Price Peter S. Seaman
| starring = {{plainlist|
* Margot Kidder
* Robert Hays
* David Suchet
* Gila von Weitershausen
* Ronald Lacey
}}
| cinematography = Tonino Delli Colli Charles Fox
| editing = Frank J. Urioste
| producer = Jerry Leider Walt Disney Productions Buena Vista Distribution
| released =  
| runtime = 91 min
| country = United States
| language = English
| budget =
| gross = $4,304,286 (US)
}}

Trenchcoat is a 1983 American action/comedy film starring Margot Kidder and Robert Hays.

== Synopsis ==
The aspiring mystery writer Mickey Raymond (Margot Kidder) travels to Malta to research her first novel and falls in love with a handsome, mysterious American (Robert Hays). She is drawn into a conspiracy both fuelled by her vivid imagination and real-world drama. David Suchet plays a local police official who seemingly is one step behind events. Raymonds odd luck leads her to become embroiled in an international plutonium smuggling ring and comedic chaos ensues.

== Cast ==
* Margot Kidder as Mickey Raymond 
* Robert Hays as Terry Leonard 
* Gila von Weitershausen as Eva Werner
* Daniel Faraldo as Nino Tenucci
* Ronald Lacey as Princess Aida
* John Justin as Marquis De Pena
* Leopoldo Trieste as Esteban Ortega
* Jennifer Darling as Laurie 
* Kevork Malikyan as Arab 
* Vic Tablian as Achmed

== Release == Never Cry Wolf and Dragonslayer are widely regarded as the films that led to the launch of Touchstone Pictures.

The film was released on March 11, 1983 at movie theatres. It was released on VHS and Betamax in 1983.

As of March 22, 2011, Trenchcoat was available on iTunes and Amazon.com for digital rental, with the Walt Disney logo being attached to the film for the first time since its theatrical release.

==Reception==
The film was a box office failure, earning a total of $4,304,286 domestically. Siskel and Ebert named it one of the "Stinkers of 1983". It garnered a 3% fresh rating on Rotten Tomatoes.

== External links ==
*   
*  

 

 
 
 
 
 
 
 
 
 


 