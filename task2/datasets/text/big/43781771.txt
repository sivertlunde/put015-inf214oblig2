Ratha Thilagam
{{Infobox film
| name           = Ratha Thilagam இரத்தத் திலகம்
| image          =
| image_size     =
| caption        =
| director       = Dada Mirasi
| producer       = Panchu Arunachalam P. V. Krishnan
| writer         = Kannadasan P. C. Ganesan Thyagan
| screenplay     = Dada Mirasi
| story          =  Savitri Pushpalatha Manorama
| music          = K. V. Mahadevan
| cinematography = Jagirdhar
| editing        = R. Devarajan
| studio         = National Movies
| distributor    = National Movies
| released       =   
| runtime        = 142 mins
| country        = India Tamil
}}
 1963 Tamil Savitri in lead roles. 
The film, produced by National Movies, had musical score by K. V. Mahadevan and was released on 14 September 1963. 

==Cast==
*Sivaji Ganesan Savitri
*Pushpalatha
*Nagesh Manorama
*Kannappa
*S. R. Janaki
*Ennatha Kannaiya
*Gundu Karuppaya
*Shanmugasundaram
*Senthamarai

==Crew==
*Director: Dada Mirasi
*Producer: Panchu Arunachalam & P. V. Krishnan
*Production Company: National Movies
*Music: K. V. Mahadevan
*Lyrics: Kannadasan
*Story: 
*Screenplay: Dada Mirasi
*Dialogues: Kannadasan, P. C. Ganesan & Thyagan
*Cinematography: Jagirdhar
*Editing: R. Devarajan
*Art Direction: M. Azhagappan
*Choreography: Desmond & S. M. Rajkumar
*Stunt: Sarangan
*Audiography: T. S. Rangasamy

==Soundtrack==

The music composed by K. V. Mahadevan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 02:55
|-
| 2 || Happy Birthday || L. R. Eswari || 03:22
|-
| 3 || Oru Koppaiyile || T. M. Soundararajan || 02:33
|-
| 4 || Pani Padarntha || T. M. Soundararajan || 06:06
|-
| 5 || Pasumai Niraintha || T. M. Soundararajan, P. Susheela || 03:49
|- Manorama || 01:56
|-
| 7 || Thazhampoove || T. M. Soundararajan, P. Susheela || 05:53
|-
| 8 || Vaadai Kaatramma || L. R. Eswari || 03:27
|-
|}

==References==
 

==External links==
*  
*  
*  


 
 
 
 
 


 