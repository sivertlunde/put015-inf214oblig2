Pagpag: Siyam na Buhay
 
{{Infobox film
| name           = Pagpag: Siyam na Buhay
| image          = Pagpag_film.jpg
| caption        = Theatrical movie poster
| director       = Frasco S. Mortiz
| producer       = Charo Santos-Concio Malou Santos Lily Monteverde Roselle Monteverde-Teo
| writer         = Joel Mercado
| screenplay     = Joel Mercado
| starring       = Kathryn Bernardo Daniel Padilla Shaina Magdayao Paulo Avelino
| cinematography = David Abaya
| editing        = Jerrold Tarog 
| music          = Francis Concio 
| studio         =  
| distributor    = Regal Films Star Cinema
| released       =  
| runtime        = 105 minutes
| country        = Philippines
| language       = Filipino
| budget         = 
| gross          = Php 170.5 million(FIGURES FROM MMDA)
   
}} horror and suspense film Metro Manila Film Festival and stars Kathryn Bernardo and Daniel Padilla.   The movie follows a group of teenagers that are terrorized by an evil spirit. 

The film revolves around the traditional Filipino belief that one should never go home directly after visiting a wake since it risks bringing evil spirits or the deceased to ones home.  One should first shake off (in Filipino, pagpág) whatever curse there may be to avoid being trailed by the soul of the dead. 

== Plot ==
Zarina (Empress) visits the funeral of her boyfriend Joseph Maurice (Enchong Dee) who hung himself. She is driven away by the boys mother, insisting her son died because of her. When she arrives home, her sister asks her if she did the "pagpag" but she replies that she doesnt believe in it. Afterwards, she is suddenly haunted by the ghost of her dead boyfriend.

Cedric (Daniel Padilla), his friends; Hannah (Michelle Vito), Ashley (Miles Ocampo), Justin (CJ Navato) and Rico (Dominic Roque) enter a horror house. Hannah is assaulted by one of the men in the haunted house and Cedric fights for her. At home, Cedrics father scolds and grounds him and takes Cedrics fake keys for the car. While his Dad gave orders to their maid, Cedric escapes and drives the highways until he almost runs over a girl, Leni (Kathryn Bernardo). They fight and Leni unknowingly loses her charm bracelet in Cedrics car.

Cedric and his friends party on a hill. Cedric and Hannah have a heated argument which ends in Cedric revealing that he doesnt actually have feelings for Hannah and is not ready for a committed relationship, which prompts her to throw a rock at Cedric, wounding his shoulder. Cedric loses control and drives up until morning where they come upon the funeral of Roman (Paulo Avelino) with his wife, Lucy (Shaina Magdayao), in despair. They soon discover that the funeral was arranged by Leni, Dencio (Janus del Prado) and Marcelo (Marvin Yap). After the funeral, Marcelo is seen sweeping the room when Eva (Matet de Leon) suddenly scolds him as it was a bad superstition. All of the unwanted guests violate a superstition including: Hannah, who dropped tears on the coffin when she saw Cedric holding Lenis hand; Rico, who wiped the coffin with a handkerchief; Justin, who stole bread from the funeral; Ashley for looking at her reflection in the mirror; Cedric, by attending the funeral with a wound; Dencio, who stole the money below the coffin and Leni and everyone else who did not do the "Pagpag," an act of dusting off oneself after a funeral to avoid evil spirits following you home.

At home, Lenis stepbrother Mac-Mac (Clarence Delgado) sees apparitions and draws nine figures with a number 8. Marcelo who was sweeping outside is killed by a malevolent entity. Dencio blames Leni for not doing the Pagpag and tells her that the spirit is coming back for the rest of them. He proceeds to tell her the story of how Roman, the husband of Lucy, died. Roman and Lucy once had a child who unfortunately died. Roman made a pact with the demons to bring back his sons life, but on the consequence that he kill nine people in order to do so, and he willingly obliged. After committing his sins, he found his son well and alive inside his coffin. One night, the people from the barrio tried to burn Romans house down because they knew he was working for the devil. This act led to the loss of Roman and his son, Emmanuel lives.

Meanwhile, Hannah is planning to commit suicide when she suddenly sees a shadow which she thought it was Cedrics, which she tries to follow. After a while, her phone suddenly rings and it turns out to be Cedric telling her that he is on his way there. Hannah panics and a demonic form gouges her eyes out and throws her into the pool, killing her. Cedric witnesses the event and is blamed for what happened. One by one, Cedrics friends are killed by the unknown entity they brought back with them from the funeral. Rico is killed when the soul from his handkerchief throws him against a wall in a gyms shower room, which he first thought was Justin and says that he is really gay. Cedric consults Justin and Ashley and insists them to burn the clothes they wore so they did.

Cedric decides to go back to the house of Lucy so he can talk to Eva (Lucys older sister). While on his way, he sees Leni in the market and decided to follow her. While in the market, Leni suddenly sees the ghost of Roman which caused her to run and panic. Cedric saw Leni panicking causing him to worry about her and chase after her. Leni blacks out while being chased and Cedric rescues her before Romans ghost catches her. Cedric brings Leni to his car and drives. Leni woke up and was alarmed she ordered Cedric to stop the car. After the car stopped she and Cedric talked, they both find out they have the same goal and decided to team up.

When they found the coffin, the body is not there anymore. They searched the house and saw three human figures in the wall drew in blood. Leni goes to find Mac-Mac in the church and Dencio, together with Eva also arrives to warn them and tells the group that Mac-Mac is really Emmanuel. She tells them that during the fire, she went there to save the baby in order for him to stay away from the demonic lives of his parents.

When Eva tells Cedric and Leni that the body is in their old house, they went to find it. In the church, Mac-Mac was then trapped inside. Eva is killed when she is sucked inside a tomb. Meanwhile, Ashley and Justin had done what Cedric asked them to do, they went inside Ashleys house, Justin told Ashley he will leave for a while to make a sandwich in the kitchen which caused Ashley to panic and to be scared. To overcome her fear she took pictures of herself but while she was taking pictures she saw one of the images has the face of the ghost. She is soon thrown to a mirror then falls down. Ashley is impaled by a falling chandelier and Justin, who was running away in the streets calling Cedric about it was crushed by two trucks, getting sandwiched in between.

In the church, Dencio is trapped inside his car when he plans to save Mac-Mac in the church. The spirit taunts him and he tries to give back the money he stole. He rolls down the windows and tries to get out but is ultimately killed after his neck being impaled by a shattered glass of a car window. When Mac-Mac is able to get out from the church, he sees the spirits of the recently killed victims warning them that Cedric and Leni are both in danger. He then follows Romans spirit on the way to the house.

Meanwhile, when Cedric and Leni find the old house, they realize that it was Lucy who sent her husbands spirit to murder the victims until he obtains nine lives for him to be alive. Mac-Mac rushes in to save them but he is caught by Lucy and threatens to kill him. Before she does, they tell Lucy the truth about Emmanuel. The entity tries to kill Leni and Cedric, but Lucy intervenes and was impaled by a girder. After Roman finally reanimates from the dead, he rushes to Lucy, who reveals to him that she was pregnant which killed their second child and explain to him that their son is still alive before she dies from her wound. Enraged of her death, Roman attempts to kill Leni before being stabbed by Cedric.

In the aftermath, Cedric arrives in the hospital to visit Leni and Mac-Mac. Within seconds of having fun, they see the apparitions of Lucy and Roman smiling at them eerily.

== Cast ==
*Kathryn Bernardo as Leni
*Daniel Padilla as Cedric
*Shaina Magdayao as Lucy
*Paulo Avelino as Roman
*Matet de Leon as Eva
*Janus del Prado as Dencio
*Miles Ocampo as Ashley
*Clarence Delgado as Mac-Mac / Emmanuel
*CJ Navato as Justin
*Michelle Vito as Hannah
*Dominic Roque as Rico
*Dominic Ochoa as Paul
*Enchong Dee as Joseph Maurice
*Empress Schuck as Zarina
*Manuel Chua as SPU2 Manlajas
*Eric Froilan Nicolas as SPU4 Garcia
*Marvin Yap as Marcelo
*Ian Jocson as Yan Yan

== Reception == PEP gave a mostly favorable review for Pagpag, praising Padillas acting while stating that his character was "not fully developed as it had to be sacrificed to spotlight his being a romantic lead to his onscreen partner Kathryn Bernardo."  Rappler also gave the film a positive review and expressed that it was a "surprisingly violent film by Metro Manila Film Festival standards" but commented that it "only adds to the fun of the film." 

== Accolades ==
{| class="wikitable"
|- style="background:#ccc;"
| Award Giving Body || Award || Recipient(s) || Result
|- 39th Metro Manila Film Festival
|-
| Best Picture || Pagpag: Siyam Na Buhay ||  
|-
| Best Actor || Daniel Padilla ||  
|-
| Best Actress || Kathryn Bernardo ||  
|-
| Best Director || Frasco Santos Mortiz ||  
|-
| Best Supporting Actor || Janus Del Prado ||  
|-
| Best Supporting Actress || Shaina Magdayao ||  
|-
| Youth Choice Award || Pagpag: Siyam Na Buhay ||     
|-
| Best Festival Make-Up || Mountain Rock Productions & Leslie Lucero ||   
|-
| Best Visual Effects || Blackburst Inc. ||  
|-
| Best Child Performer || Clarence Delgado ||  
|-
| Best Sound Recording || Arnel Labayo ||  
|-
| Best Original Story || Joel Mercado ||  
|-
| Mavshack Male Star of the Night || Daniel Padilla ||  
|- 2014 Young Critics Circle Awards || Best Film || Frasco Mortiz ||  
|-
| Best Achievement Film Editing || Jerrold Tarog ||  
|}

== References ==
 

== External links ==
*  (in Tagalog)
* 
* 
* 
* 

== See also ==
*List of ghost films

 
 
 
 
 
 
 