Licensed to Kill (1965 film)
{{Infobox film
| name           = Licensed to Kill
| image          = Licensed.jpg
| caption        = original British film poster
| director       = Lindsay Shonteff
| producer       = James Ward   Alistair Films Howard Griffiths Tom Adams Karel Stepanek Veronica Hurst Peter Bull John Arnatt
| music          = Herbert Chappell
| cinematography = Terry Maher
| editing        = Ron Pope
| distributor    = Embassy Pictures
| released       = July 1965
| runtime        = 96 minutes
| country        = United Kingdom English
| gross          = $1.2 million (est. US/ Canada rentals) 
}}
 1965 superspy Tom Adams as British secret agent Charles Vine. It was directed and co-written by Lindsay Shonteff. Producer Joseph E. Levine picked it up for American and worldwide distribution and reedited it under the title The Second Best Secret Agent in the Whole Wide World. 
 Tinker Tailor Soldier Spy.

==Plot== Tom Adams), a former mathematician, as a bodyguard and assassin|exterminator.

==Cast== Tom Adams as Charles Vine
* Karel Stepanek as Henrik Jacobsen
* Peter Bull as Masterman
* John Arnatt as Rockwell
* Francis de Wolff as Walter Pickering
* Felix Felton as Tetchkinov
* Veronica Hurst as Julia Lindberg
* Judy Huxtable as Computer Center Girl
* Carol Blake as Crossword Puzzle Girl
* Claire Gordon as Hospital Doctor
* George Pastell as Russian Commissar

==Aspects of production==
Based on the success of the film, Columbia Pictures offered director Shonteff a five picture contract, but they disagreed over conditions. 
 Howard Griffiths  emigrated to Australia where he wrote extensively for Australian television series such as the spy series Hunter (Australian Crawfords TV series)|Hunter (1967), and police shows Division 4, Homicide (Australian TV series)|Homicide, and Blue Heelers. 

== The Second Best Secret Agent in the Whole Wide World ==
  pram of twins changing to a pre-credit scene. Levine engaged songwriters Sammy Cahn and Jimmy Van Heusen to write a title song performed by Sammy Davis Jr and arranged and conducted by Claus Ogerman over the credits with the new title. The American release then eliminated scenes of Francis de Wolff talking to John Arnatt about seeking Bond for the assignment, and Vine in bed with a girl and a crossword puzzle giving double entendre clues. The American release also eliminates much of the dialogue about the anti gravity device, called "Regrav" that makes the denouement of the film less comprehensible.

The American publicity for the film echoed the "Number 2, but tries harder" advertising of the Avis Rent a Car System prevalent at the time. Levine launched a November 1965 nationwide 100 word essay contest to be titled "the most unforgettable second best secret agent I have known". 

==Sequels==
What Eon Productionss reaction was to the blatant imitation is not known, but Shonteff was missing from the two Vine sequels starring Tom Adams:

* Where the Bullets Fly (1966) (directed by Warwick Films and Hammer Films director John Gilling) that was also released by Embassy Films
* Somebodys Stolen Our Russian Spy/O.K. Yevtushenko (1969) a film shot in Spain instead of the usual UK location, that languished in a film laboratory until 1976. 

Shonteff later made three spy films with the hero named "Charles Bind":

* Number One of the Secret Service (1970) starring Nicky Henson
* Licensed to Love and Kill aka The Man from S.E.X. (1979) starring Gareth Hunt
* Number One Gun (1990) starring Michael Howe.

==See also==
* Outline of James Bond
* Eurospy film

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 