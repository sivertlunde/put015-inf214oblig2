Names in Marble (film)
{{Infobox film
| name           = Names in Marble    
| image          = Nimed marmortahvlil (2002).jpg
| caption        = 
| director       = Elmo Nüganen
| producer       = Kristian Taska
| writer         = Elmo Nüganen Kristian Taska
| screenplay     = 
| story          = 
| based on       =  
| starring       = Priit Võigemast Indrek Sammul Hele Kõre
| music          = Margo Kõlar
| cinematography = Sergei Astakhov
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 95 minutes
| country        = Estonia Finland
| language       = Estonian EEK
| gross          = 
}} the novel of the same name written by Albert Kivikas in 1936 about the Estonian War of Independence fought in 1918-1920.

==Cast==
* Henn Ahas – Priit Võigemast
* Ants Ahas – Indrek Sammul
* Marta – Hele Kõre
* Käsper – Alo Kõrve
* Tääker – Anti Reinthal
* Mugur – Ott Sepp
* Miljan – Mart Toome
* Martinson – Karol Kuntsel
* Kohlapuu – Ott Aardam
* Karakull – Guido Kangur Peter Franzén
* Captain – Jaan Tätte
* Battalion commander – Martin Veinmann

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 