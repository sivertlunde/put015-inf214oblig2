9 Dead Gay Guys
 
{{Infobox film
| name           = 9 Dead Gay Guys
| image          = 9DeadGayGuys.png
| image_size     =
| caption        = DVD Cover
| director       = Lab Ky Mo
| producer       = Andrew Melmore
| writer         = Lab Ky Mo
| narrator       = Brendan Mackey
| starring       = Brendan Mackey Glen Mulhern
| music          = Resident Filters  Stephen Parsons
| cinematography = Damien Elliott
| editing        = Chris Blunden
| distributor    = TLA Releasing
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $26,377
}}
9 Dead Gay Guys is a 2002 British comedy film by director Lab Ky Mo starring Brendan Mackey and Glen Mulhern and released by TLA Releasing.

==Cast==
*Glen Mulhern as Kenny
*Brendan Mackey as Byron
*Steven Berkoff as Jeff
*Michael Praed as The Queen
*Vas Blackwood as Donkey-Dick Dark Fish as Old Nick
*Leon Herbert as Nev
*Simon Godley as Golders Green
*Carol Decker as Jeffs Wife
*Raymond Griffiths as The Desperate Dwarf
*Abdala Keserwani as Dick-Cheese Deepak
*Karen Sharman as The Iron Lady

== Reception== John Waters tradition, takes place in such a cartoonish, good-natured universe its hard to imagine anyone taking offence."   When the film went on national sale it generally found favor with the general public and especially the gay and lesbian community with many positive reviews coming from Amazon. 

Due to the films seemingly controversial subject matter the film could only secure a limited release, and subsequently made only $26,377 at the box office. In the films initial sole theater, the film grossed $3,462 in the opening week. 

The film won two major awards. The first was the 2002 Audience Award for Best Feature Film at the Dublin Gay and Lesbian film festival. The second award was the Festival Prize at the Montreal Just For Laughs comedy film festival. 

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 