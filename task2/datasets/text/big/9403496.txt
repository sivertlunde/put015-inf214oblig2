The Holy Girl
{{Infobox film
| name           = The Holy Girl
| image          = holygirlposter.jpg
| caption        = Theatrical release poster
| director       = Lucrecia Martel
| producer       = Lita Stantic
| writer         = Lucrecia Martel
| starring       = Mercedes Morán Carlos Belloso Alejandro Urdapilleta María Alche
| music          = Andres Gerszenzon
| cinematography = Félix Monti
| editing        = Santiago Ricci Lita Stantic Producciones
| distributor    = HBO Films
| released       =  
| runtime        = 106 minutes
| country        = Argentina Italy Spain Netherlands
| language       = Spanish
| budget         = 
|}}
The Holy Girl ( ) is a 2004 Argentinian drama film directed by Lucrecia Martel.  The picture was executive produced by Pedro Almodóvar, Agustín Almodóvar, and Esther García.  It was produced by Lita Stantic. The film features Mercedes Morán, María Alche, Carlos Belloso, Alejandro Urdapilleta, Julieta Zylberberg, among others.

==Plot==
The film takes place in the small town of La Ciénaga, at the Hotel Termas, a dilapidated Argentine hotel, during a medical conference. Two young teenage girls, Amalia (María Alché) and her best friend Josefina (Julieta Zylberberg), begin to explore their new sexuality and, at the same time, have Catholic religious passion. Amalia lives with her attractive divorced mother (Mercedes Morán), who owns the hotel, and her uncle Freddy (Alejandro Urdapilleta). During this time, in Amalias mind, spiritual and sexual impulses are seeming to converge.

One day, in the midst of a large crowd watching the performance of a musician playing the theremin, Dr. Jano (Carlos Belloso), a participant in the conference and hotel guest, rubs up sexually against Amalia. She is upset but takes his inappropriate action as a sign that her Catholic faith has given her a mission: to save Dr. Jano from such inappropriate behavior. Afterward, the object of Amalias desire becomes the married middle-aged doctor and she begins to spy on him. Amalias story is partly about an adolescent girls discovery of her sexual vulnerability and the sexual power she possesses.

==Cast==
* Mercedes Morán as Helena
* María Alche as Amalia
* Carlos Belloso as Dr. Jano
* Alejandro Urdapilleta as Freddy
* Julieta Zylberberg as Josefina
* Mía Maestro as Inés
* Marta Lubos as Mirta
* Arturo Goetz as Dr. Vesalio
* Alejo Mango as Dr. Cuesta
* Mónica Villa as Madre de Josefina
* Leandro Stivelman as Julian
* Manuel Schaller as Thermin player

==Background==
The screenplay of the film was written by director Lucrecia Martel.

The picture while not exactly autobiographical was based on Martels memories. Martel said, "The film isnt strictly autobiographical, but what I put in it is my personal experience in life, my memories. When I was in my teens, I was a very religious person. I thought I had a special relationship with God, or anything that was up there. Now, I dont believe in miracles, but I do believe in the emotion you feel in front of a miracle - the emotion of something unexpected revealed to you." 

As part of the way she uses the camera the film has few establishing shots or transition shots because she makes the case it physically separates a space from its moment in the film. 

Filming location 
The film was shot entirely in Salta, in the Salta Province, Argentina.  The director/screenwriter was born in Salta.

==Distribution==
The film first opened in Argentina on May 6, 2004.  It was selected for competition and featured internationally at the 2004 Cannes Film Festival on May 16.   

The film was also shown at various film festivals, including: the Karlovy Vary Film Festival, the Toronto Film Festival, the Helsinki International Film Festival, the London Film Festival, the Hong Kong International Film Festival, and the Reykjavik International Film Festival.

In the United States it was presented at the New York Film Festival on October 10, 2004, and the Seattle International Film Festival on May 20, 2005. Fine Line Features gave it a limited US theatrical release on April 29, 2005.

==Critical reception==
A.O. Scott, film critic for The New York Times, called the film an "elusive, feverish and altogether amazing second feature..." He also liked Martels artistic directorial approach to films, and wrote, "Her visual style is similarly oblique, as she frames her characters through half-opened doors, at odd angles and in asymmetrical close-ups. To a degree that is sometimes disorienting, Ms. Martel explores the mysteries of the senses. They are our instruments for knowing ourselves, each other and the world, but they also mislead us, bringing pain, pleasure and confusion in equal measure." 

Kevin Thomas, critic for the Los Angeles Times, wrote, "  reveals the style, insight and confidence that are the marks of a major director."  He also said of director Martel, "  a subtle artist and a sharp observer, Martel manages a large cast with an ease that matches her skill at storytelling, within which psychological insight and social comment flow easily and implicitly." 

Film critic Ruth Stein also credits director Martel for capturing the mood of the film, and wrote, "Martel is especially good at capturing a claustrophobic environment, and she wisely leaves ambiguous the question of the doctors complicity in Amalias frenzied state. He fails to recognize her when she starts stalking him -- an indication of the randomness of the act done to the accompaniment of a theremin." 

The film currently holds a 77% approval rating at Rotten Tomatoes, with 43 of 56 reviews being favorable.

==Awards==
Wins
* Clarin Entertainment Awards: Clarin Award; Best Director, Lucrecia Martel; Best New FIlm Actress, Julieta Zylberberg; 2004.
* São Paulo International Film Festival: Critics Award, Honorable Mention, Lucrecia Martel; 2004.

Nominations Cannes Film Festival: Golden Palm, Lucrecia Martel; 2004.
* Argentine Film Critics Association Awards: Silver Condor; Best Cinematography, Félix Monti; Best Costume Design, Julio Suárez; Best New Actress, María Alche; Best Supporting Actress, Julieta Zylberberg; 2005.

==References==
 

==External links==
*  
*  
*  
*  
*   at cinenacional.com  
*   reviews at Metacritic
*    

 
 
 
 
 
 
 
 
 
 
 
 