Shark! (film)
 
 
{{Infobox film
| name = Shark!
| image =Poster of the movie Shark!.jpg
| caption =
| director = Samuel Fuller
| producer = José Luis Calderón Mark Cooper Skip Steloff
| writer = Samuel Fuller John Kingsbridge Arthur Kennedy Manuel Alvarado Carlos Barry Silvia Pinal
| music = Rafael Moroyoqui
| cinematography = Raúl Martínez Solares
| editing = Carlos Savage
| distributor = Troma Entertainment
| released =  
| country = United States
| runtime = 92 minutes
| language = English
| budget =
}} American action film directed by Samuel Fuller and starring Burt Reynolds. The film was based on the Victor Canning novel His Bones are Coral with the original screenplay written by Ken Hughes.  When Fuller joined the project, he rewrote the script and retitled it Caine. {{cite book
|last=Dombrowski
|first=Lisa
|title=The films of Samuel Fuller: if you die, Ill kill you!
|url=http://books.google.com/books?id=TVd4b-w-bE0C
|accessdate=12 April 2011
|date=31 March 2008
|publisher=Wesleyan University Press
|isbn=978-0-8195-6866-3
|page=177}}  

==Plot== gunrunner who becomes stranded in a small port in the Red Sea. He meets a seductive woman who propositions him to dive into shark-infested waters off the coast for scientific research. However, when Caine realizes the woman and her partner are actually treasure hunters, the action starts to heat up both above and below the water.

==Production==
During production in Mexico in 1967, one of the films stuntmen was attacked and killed on camera by a shark that was supposed to have been sedated. When the production company used the death to promote the film, (even retitling the film to Shark!)  Fuller, who had been arguing with the producers on several major issues relating to the film, quit the production.

When Samuel Fuller finally saw the version that was released to theaters, he said it was so badly butchered he demanded the producers take his name off it.  The producers refused.

The film was re-released as Man-Eater  by Legacy Entertainment Inc. in 2003.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 