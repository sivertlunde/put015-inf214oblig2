Ring: Kanzenban
{{Infobox film
| name               = Ring: Kanzenban
| image              = KANZENBANVHSB.jpg
| writer             = Jôji Iida Taizo Soshigaya
| starring           = Katsunori Takahashi Ayane Miura Yoshio Harada Akiko Hinagata
| director           = Chisui Takigawa
| producer           = Ryunosuke Endou
| music              = Yoshihiro Ike
| cinematography     = Kazumi Iwata
| editing            = Ichiro Chaen Fuji Television Amuse Video
| released           = Japan: Television: August 11, 1995 Home video: November 24, 1995 (VHS), July 26, 1996 (Laserdisc)
| country            = Japan
| language           = Japanese
}}
Ring: Kanzenban (リング 完全版) is a 1995 Japanese film that predates all other films in the Ring cycle. It is based on the book Ring (Suzuki novel)|Ring by Koji Suzuki. Unlike that of the later film series the characters remain largely unchanged and in comparison other films made of this novel it is the most accurate in relation to the original text.

==Plot==
Newspaper reporter-turned-proofreader Kazuyuki Asakawa reluctantly enlists the help of a former contact by the name of Ryuji Takayama to break the curse of a cursed videotape.

==Cast==
*Katsunori Takahashi as Kazuyuki Asakawa
*Yoshio Harada as Ryûji Takayama
*Ayane Miura as Sadako Yamamura
*Kyôko Dônowaki as Shizuko Yamamura
*Mai Tachihara as Shizuka Asakawa
*Akiko Hinagata as Tomoko Ôishi
*Maha Hamada as Mai Takano
*Tomorowo Taguchi as Jôtarô Nagao
*Tadayoshi Ueda	as Kei Yamamura

==Release==

===Japan=== Fuji Television on August 11, 1995 under the title Ring. This version never had a commercial release.

The film was released on VHS on November 24, 1995, and on Laserdisc on July 26, 1996, under the title Ring: Kanzenban (literally: Ring: The Complete Edition). This version contained more nudity and extra short scenes.

== Link ==
* 

 

 
 
 
 


 
 