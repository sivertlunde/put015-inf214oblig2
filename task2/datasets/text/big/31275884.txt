The Colonel (2006 film)
The Colonel ( ) is a 2006 French-Belgian film directed by Laurent Herbiet based on the novel by Francis Zamponi.

== Plot ==
France, 1993. The retired Colonel Raoul Duplan is shot in his home. As the police is baffled, the young army officer Galois leads the investigation. Shortly thereafter, she receives a letter containing some diary pages of a lieutenant who served since 1955 in the Algerian war under the command Duplan and disappeared in 1957 under mysterious circumstances.
Every day Galois receives a continuation of the diary in which Rossi describes in detail his ambivalent relationship to Duplan and his dirty methods. Rossi had to witness even torture and public executions. Eventually Galois recognizes, who shot Duplan.

== Cast ==
* Olivier Gourmet : Colonel Duplan
* Robinson Stévenin : Lieutenant Guy Rossi
* Cécile de France : Lieutenant Galois
* Charles Aznavour : Père Rossi
* Bruno Solo : Commissaire Reidacher
* Eric Caravaca : René Ascensio
* Guillaume Gallienne : Le sous-préfet
* Georges Siatidis : Le capitaine Roger
* Thierry Hancisse : Commissaire Quitard
* Jacques Boudet : Le sénateur-maire 

== External links ==
*  

 
 
 
 

 