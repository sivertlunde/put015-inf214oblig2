Vetri Kodi Kattu
{{Infobox film
| name           = Vetri Kodu Kattu
| image          = Vetri Kodi Kattu DVD Cover.jpg
| image_size     =
| caption        = DVD Cover Cheran
| producer       = Shivashakti Pandian Cheran
| narrator       = Murali Parthiban Meena Vadivelu Manorama Malavika Malavika Charle Anand Raj Deva
| cinematography = Priyan
| editing        = B. Lenin V. T. Vijayan
| studio         = Shivashakti Movie Makers
| distributor    = Shivashakti Movie Makers
| released       = June 30, 2000
| runtime        = 168 minutes
| country        = India
| language       = Tamil
| preceded_by    =
| followed_by    =
}}
 Tamil film directed by Cheran starring Manorama and Malavika

==Plot==
Sekhar (  and Parthiban) are neck-deep in poverty and they want to send their eldest family member to Dubai to earn good amount of money for their families. Both Muthuraman and Sekhar along with many dreaming to go to Dubai give money to a middleman to buy them passports and visas. Everyone goes to the Chennai airport only to find that they were duped by the middleman. Sekhar and Muthuraman prefer not to go home as their loved ones might be heart-broken to know the fact. Hence, both of them agree that while Muthuraman goes to Sekhars house saying that he was sent by Sekhar and vice versa. Back at home, an educated Muthuraman finds that though Sekhars family land is unfit for agriculture; it can be used for grazing of cows. Hence Muthuraman manages to secure loan to buy a couple of cows and help the family earn a big profit; while Sekhar finds that Valli, Muthuramans wife (Meena Durairaj|Meena), is good at cooking and he suggests her to start a business in cooking. In this way, both families succeed in their efforts. Before the climax, Valli was pregnant and admitted to a hospital in Coimbatore and Sekhar asks Muthuraman to come there. At the right time, Pazhani (Charle), one of the persons who was cheated by the middleman, finds that the cheat has come into Coimbatore too in another disguise and he informs this to Muthuraman and Sekhar when he finds them. In order to prevent any more people to be fleeced by the middleman, the duo set out to bash him. At the right time; the police arrives and he promises them that the cheated men will be given passports and visas to Dubai. However; Muthuraman and Sekhar say that they do not want to go to Dubai or any other country as they became prosperous in their motherland due to their self-confidence.

==Cast==

*Murali (Tamil actor)|Murali as Sekar
*Parthiban as Muthuraman
*Meena Durairaj|Meena as Valli
*Manorama (Tamil actress)|Manorama as Sekars Mother
*Malavika (Tamil actress)|Malavika as Amudha
*Vadivelu as Sudalai
*Charle as Pazhani
*Dhamu - (Special Appearance) in the song Sirippu Varudhu Sirippu Varudhu
*Kovai Sarala - (Special Appearance) in the song Sirippu Varudhu Sirippu Varudhu
*Pandu (actor)|Pandu - (Special Appearance) in the song Sirippu Varudhu Sirippu Varudhu

==Awards==
The film won the National Film Award for Best Film on Other Social Issues in 2000.

==Box office==
*The film grossed $500,000 at the box office ( 2.5 crores )

==Soundtrack==

{{Infobox album |  
| Name        = Vetri Kodi Kattu
| Type        = soundtrack Deva
| Cover       = 
| Released    = 2000
| Recorded    =  Feature film soundtrack |
| Length      = 
| Label       = The Best Audio Deva
| Reviews     = 
}}

The soundtrack features five songs which is composed by Deva (music director)|Deva. Karupputhan Enakku was a huge hit that time.
 Deva
* Thillele Thillele - Shankar Mahadevan, Krishnaraj
* Valli Valli - Mano (singer)|Mano, K. S. Chitra
* Karupputhan Enakku pidicha colouru - Anuradha Sriram
* Latcha Latchama - Shankar Mahadevan

==Production==
This film was Cherans third and last combo with Murali and second with Parthiban. Thangar bachan was replaced by Priyan as cinematographer. The shooting for the film was held at places like Gopichettypalayam, Dindugal and Palani among other places. 

==Reviews==
Indolink wrote:"Vetrikkodi Kattu is your average, predictable, clean, and reasonably engaging tamil movie fare".  Chennaionline wrote:"Vettrikkodi Kattu is a well-intentioned film and fairly entertaining too. It may not be as neatly scripted and as good as Cherans first two films. But it proves that he is on the right track".  Subash of oocities.org wrote, "Wonderful story, JUST DONT PREACH SO MUCH, Cheran." 

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 