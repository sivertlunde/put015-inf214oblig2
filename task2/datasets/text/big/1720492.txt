The Squid and the Whale
{{Infobox film
| name = The Squid and the Whale
| image = Squid and the whale.jpg
| caption = Theatrical release poster
| director = Noah Baumbach Peter Newman
| writer = Noah Baumbach
| starring = Jeff Daniels Laura Linney Jesse Eisenberg Owen Kline William Baldwin Anna Paquin
| music = Britta Phillips Dean Wareham
| cinematography = Robert D. Yeoman
| editing = Tim Streeto Sony Pictures American Empirical Pictures
| distributor = Samuel Goldwyn Films
| released =  
| runtime = 81 minutes
| country = United States
| language = English
| budget = $1,500,000
| gross = $11,098,131
}}

The Squid and the Whale is a 2005 American   diorama housed at the American Museum of Natural History, which is seen in the film. The film was shot on Super 16mm, mostly using a handheld camera.
 Academy Award nomination for Best Original Screenplay. The film received six Independent Spirit Award nominations and three Golden Globe nominations. The New York Film Critics Circle, Los Angeles Film Critics Association and the National Board of Review voted its screenplay the years best.

==Plot summary== Prospect Park from their home in Park Slope, Brooklyn.

As the parents set up a schedule for spending time with their children, Walt and Frank can hardly imagine that things could get more combative between their parents. They do, however, as Joan begins dating Ivan (William Baldwin), Franks tennis instructor, and Bernard starts sharing his new house with Lili (Anna Paquin), one of his students. Meanwhile, the two boys begin taking sides in the battle between their parents, with Walt taking after his father and Frank siding with his mother.

Along with the trouble both boys exhibit verbally with their parents, they also show internal struggles and very different ways of handling the stress of their parents divorce.  Walts most obvious cry for help is when he performs and claims to have written Hey You (Pink Floyd song)|"Hey You" by Pink Floyd at his schools talent show.  After Walt wins first place and receives praise from his family and friends, his school realizes that he did not write the song.  At this point, the school calls Bernard and Joan in to discuss Walts issues, which are mainly fabricating his accomplishments.  It is decided that Walt should meet with a school psychologist.  Meanwhile, Frank exhibits his own internal turmoil by repeatedly masturbating at school.  He also begins to frequently drink beer and speak in a way that emulates Ivans mannerisms.
 giant squid and whale exhibit at the American Museum of Natural History; the exhibit scared him as a small child so he would often close his eyes whenever they went by the exhibit at the museum.

After a heated argument between Bernard and Joan over custody, Bernard collapses on the street outside their home and is taken to the hospital. Bernard asks for Walt to stay by his side, but Walt now finally fed up with his fathers selfish ways instead runs to visit the squid and whale. The film concludes with him pondering over the exhibit and his past.

==Cast==
* Jeff Daniels as Bernard Berkman
* Laura Linney as Joan Berkman
* Jesse Eisenberg as Walt Berkman
* Owen Kline as Frank Berkman
* Anna Paquin as Lili
* William Baldwin as Ivan
* Halley Feiffer as Sophie Greenberg
* David Benger as Carl
* Adam Rose as Otto
* Peter Newman as Mr. Greenberg
* Peggy Gormley as Mrs. Greenberg Greta Kline as Greta Greenberg

==Reception==
The Squid and the Whale was met with near-universal critical acclaim. It scored 82 out of 100 on Metacritic  according to 37 critics and 93% on Rotten Tomatoes.
 Ebert & Roeper, both critics praised the film and gave it a "two thumbs up" rating. Premiere (magazine)|Premiere critic Glenn Kenny praised the film, writing, "Its a rare film that can be convincingly tender, bitterly funny, and ruthlessly cutting over the course of fewer than 90 minutes. The Squid and the Whale not only manages this, it also contains moments that sock you with all three qualities at the same time." Time (magazine)|Time critic Richard Corliss wrote, "The Squid and the Whale is domestic tragedy recollected as comedy: a film whose catalog of deceits and embarrassments, and of love pratfalling over itself, makes it as (excruciatingly) painful as it is (exhilaratingly) funny."

The English Indie folk band Noah and the Whale takes its name from a combination of the directors name (Noah Baumbach) and the films title. 

==Awards and nominations== Academy Award nomination for Best Original Screenplay
* Won awards for best dramatic direction and screenwriting at the Sundance Film Festival
* Six Independent Spirit Award nominations. Best Feature, Best Director (Baumbach), Best Screenplay (Baumbach), Best Supporting Male (Jesse Eisenberg), Best Female Lead (Laura Linney) and Best Male Lead (Jeff Daniels)
* Three Golden Globe nominations (Best Motion Picture - Musical or Comedy, Jeff Daniels for Best Performance by an Actor in a Motion Picture - Musical or Comedy and Laura Linney for Best Performance by an Actress in a Motion Picture - Musical or Comedy)
* Won Los Angeles Film Critics Association award for Best Screenplay
* Awarded Best Screenplay by the National Board of Review
* Won New York Film Critics Circle award for Best Screenplay
* Won Best Screenplay and Best Actress (Laura Linney) at the 2005 Toronto Film Critics Association Awards Grand Prix of the Belgian Syndicate of Cinema Critics
* Won AARP Movies for Grownups Award (The "Golden Barcalounger") for Best Actor Jeff Daniels  

==Home media==
The film was released on DVD on March 21, 2006 by Sony Pictures. The DVD includes a 45-minute commentary with director Noah Baumbach, another 40-minute commentary with Baumbach and Phillip Lopate, cast interviews, and trailers.
 Running With Scissors". All extras were dropped for the Blu-ray release. 

==Soundtrack==
The soundtrack features two songs by Loudon Wainwright III and one by Kate and Anna McGarrigle. It reuses Tangerine Dreams "Love on a Real Train", from Risky Business, for the scenes of Franks sexual awakenings. Other contemporary popular music is played in the background of scenes, such as The Cars "Drive (The Cars song)|Drive" and Bryan Adams "One Night Love Affair". "Figure Eight", from Schoolhouse Rock, is used as both an instrumental and a vocal.
 Hey You" diegetic performances by Jesse Eisenberg and Owen Kline, are used. Baumbach originally wanted to use The Whos "Behind Blue Eyes" instead but he could not secure the rights.

;Track listing Britta Phillips & Dean Wareham
# "Courting Blues" - Bert Jansch John Phillips
# "Lullaby" - Loudon Wainwright III Kate & Anna McGarrigle
# "The Bright New Year" - Bert Jansch
# "Drive" - The Cars
# "Lets Go" - The Feelies
# "Figure Eight" - Blossom Dearie
# "Come Sing Me a Happy Song to Prove We All Can Get Along the Lumpy, Bumpy, Long & Dusty Road" - Bert Jansch Hey You " - Pink Floyd (Performed by Dean Wareham) Britta Phillips & Dean Wareham Street Hassle" - Lou Reed
# "The Swimming Song" - Loudon Wainwright III

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 