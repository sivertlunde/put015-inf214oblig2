Peter Voss, Thief of Millions (1946 film)
{{Infobox film
| name           = Peter Voss, Thief of Millions 
| image          = 
| image_size     = 
| caption        = 
| director       = Karl Anton 
| producer       = Kurt Ulrich
| writer         = E.G. Seeliger (novel)   Felix von Eckardt   Karl Anton
| narrator       = 
| starring       = Viktor de Kowa   Else von Möllendorff   Karl Schönböck   Hans Leibelt
| music          = Werner Schmidt-Boelcke   Friedrich Schröder
| editing        = Johanna Meisel
| cinematography = Eduard Hoesch
| studio         = Tobis Film   DEFA
| distributor    = Sovexport-Film 
| released       = 27 September 1946
| runtime        = 91 minutes
| country        = Germany German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} comedy crime Peter Voss, Thief of Millions by E.G. Seeliger.

==Main cast==
* Viktor de Kowa as Peter Voss
* Else von Möllendorff as Polly Petterson
* Karl Schönböck as Bobby Dodd
* Hans Leibelt as Van Gelder
* Kurt Seifert as Petterson
* Fritz Kampers as Fritz Mohr
* Georg Thomalla as Max Egon Flipp
* Werner Stock as Dodds Sekretär
* Gustav Bertram as Sam Parker

==References==
 

==Bibliography==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 