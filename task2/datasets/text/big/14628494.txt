The Trail of the Lonesome Pine (1916 film)
 
{{Infobox film
| name           = The Trail of the Lonesome Pine
| image          = The Trail of the Lonesome Pine (1916) 1.jpg
| caption        = Thomas Meighan and Charlotte Walker
| director       = Cecil B. DeMille
| producer       = Jesse L. Lasky
| story          = Cecil B. DeMille
| based on       =   Charlotte Walker
| cinematography = Alvin Wyckoff
| editing        = Cecil B. DeMille
| distributor    = 
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}} silent drama same name.

==Preservation status==
A copy of the 1916 film survives in the archives of George Eastman House. 

==Cast== Charlotte Walker as June Tolliver
* Thomas Meighan as Jack Hale
* Earle Foxe as Dave Tolliver
* Theodore Roberts as Judd Tolliver
* Milton Brown as Tolliver Man
* Hosea Steelman as Tolliver Man

==Other adaptions== released in in 1936, which stars Sylvia Sidney and Fred MacMurray.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 