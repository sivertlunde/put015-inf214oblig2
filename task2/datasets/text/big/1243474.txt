Twenty Minutes of Love
 
{{Infobox film
| name = Twenty Minutes of Love
| image =Twenty Minutes of Love (1914).webm
| caption =
| director = Joseph Maddern Charlie Chaplin
| producer = Mack Sennett
| writer = 
| starring = Charlie Chaplin Minta Durfee Edgar Kennedy Gordon Griffith Chester Conklin Josef Swickard Hank Mann 
| music = Frank D. Williams
| editing =
| distributor = Keystone Studios
| released =  
| runtime = 7 minutes at 14 frame/s
| language = Silent film English (Original titles)
| country = United States
| budget =
}}
Twenty Minutes of Love is a 1914 American comedy silent film made by Keystone Studios. The film is widely reported as Charlie Chaplins directorial debut; some sources name Joseph Maddern as the director, but generally credit Chaplin as the creative force.   

==Cast==
*Charles Chaplin – Pickpocket
*Minta Durfee – Woman
*Edgar Kennedy – Lover
*Gordon Griffith – Boy
*Chester Conklin – Pickpocket
*Josef Swickard – Victim
*Hank Mann – Sleeper 

==See also==
*Charlie Chaplin filmography

==References==
{{reflist|refs=
   
   
   
   
}}

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 