Goyokin
{{Infobox film
| name           = Goyokin
| image_size     =
| image	=	Goyokin FilmPoster.jpeg
| caption        = A poster with one of the films alternative titles: Steel Edge of Revenge
| director       = Hideo Gosha
| producer       = Sanezumi Fujimoto
| writer         = Hideo Gosha Kei Tasaka
| narrator       =
| starring       = See text.
| music          = Masaru Sato
| cinematography = Kozo Okazaki
| editing        = Michio Suwa
| distributor    = Toho
| released       = 1 May 1969
| runtime        = 124 min
| country        = Japan
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
}}

  is a 1969 jidaigeki film co-written and directed by Hideo Gosha. Set during the late Tokugawa era, the story follows a reclusive ronin who is trying to atone for past transgressions. In 1975 it was remade as a Western film, entitled The Master Gunfighter.

==Plot==
Magobei Wakizaka is a samurai for the Sabai clan. A nearby island, Sado, Niigata|Sado, boasts a rich gold mine which provides plentiful riches  for the Tokugawa clan. When one of the gold ships sink, the local farmers recover some of the gold, intending to return it to the Tokugawa clan. However, Magobeis clan master, Rokugo Tatewaki, takes the gold and slaughters the farmers so they cannot report the gold stolen. Magobei is appalled. He promises not to report Rokugo to the shogunate in exchange for Rokugo s promise to never do so again.

However, three years later, assassins sent by Rokugos retainer, Kunai, come for Magobei, who is living in Edo. He realizes that Rokugo intends to steal more gold and slaughter more innocents. So Magobei returns to Sabai to face his former master. Rokugo hires another ronin, Samon Fujimaki, to kill Magobei, but Magobei eventually wins him over. Also, along the way, Magobei meets a young woman, Oriha, who survived the original slaughter. She and her brother, Rokuzo, join him on his way to Sabai.

At Sabai they learn that Rokugo intends to move a bonfire, which serves as a warning to passing ships against dangerous rocks, so that a gold ship will hit the rocks and sink. After recovering the gold, Rokugo intends to slaughter the peasants who help him in this endeavor. The combined efforts of Magobei, Samon, Oriha, and Rokuzo result in the correct bonfire being lit, the fake bonfire being put out, and the innocent peasants lives being saved. Thus the gold-bearing ship evades the rocks. In a final showdown, amid falling snow, Magobei slays Rokugo, but is wounded by one of Rokugo s throwing knives.

==Cast==
* Tatsuya Nakadai as Magobei Wakizaka
* Kinnosuke Nakamura as Samon Fujimaki
* Tetsurō Tamba as Rokugo Tatewaki, Magobeis childhood friend, brother-in-law, and clan master
* Yoko Tsukasa as Shino, Magobeis sister and Rokugos wife
* Ruriko Asaoka as Oriha
* Isao Natsuyagi as Kunai
* Ben Hiura as Rokuzo
* Kunie Tanaka as Hirosuke

==References==
 

==External links==
*  
*  
*  
*  
*     at the Japanese Movie Database


 

 
 
 
 
 
 
 
 
 
 
 
 