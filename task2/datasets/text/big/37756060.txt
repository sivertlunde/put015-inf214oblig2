Soodhu Kavvum
 
 

{{Infobox film
| name           = Soodhu Kavvuum
| image          = Soodhu kavvum.jpg
| alt            = Soodhu Kavvum Poster
| caption        =
| director       = Nalan Kumarasamy
| producer       = C. V. Kumar
| writer         =
| story          = Nalan Kumarasamy Srinivas Kavenayam
| screenplay     = Nalan Kumarasamy
| starring       = Vijay Sethupathi Sanchita Shetty
| music          = Santhosh Narayanan costume designer =Reshma Kunhi
| cinematography = Dinesh Krishnan
| editing        = Leo John Paul
| studio         = Thirukumaran Entertainment
| distributor    = Studio Green
| released       =  
| runtime        = 138 mins
| country        = India
| language       = Tamil
| budget         =     
| gross          =
}} Tamil comedy thriller film directed by Nalan Kumarasamy of Naalaya Iyakunar fame.  It features Vijay Sethupathi and Sanchita Shetty in the lead roles. The film was released on 1 May to universal acclaim from critics.  The films main concept is about how baloney has engulfed peoples day-to-day life and modern society. It became a critical and financial success and is set to be remade in various Indian languages.    

==Plot==

Friends Kesavan (Ashok Selvan), Sekar (RJ Ramesh Thilak) and Pagalavan (Bobby Simha) meet Das (Vijay Sethupathi), a middle-aged man with an imaginary girlfriend named Shalu (Sanchita Shetty) who does low-profile kidnappings for a living. Broke, the trio decide to become his assistants. Das follows a Five Rules of kidnapping (which Das spells and writes as "kednapping"), which makes kidnapping easier to do. One day, they kidnap a young boy and successfully obtain ransom money from his father Nambikkai Kannan. Nambikkais contractor brother has been arrested for attempted bribery by State Minister Gnanodayam (M. S. Bhaskar), a disciplined politician who plays by the book. Impressed with Das kidnapping skills, Nambikkai asks him to kidnap the ministers son Arumai Pragasam (Karuna Karan) as revenge and offers to pay Das up to  .

The next day, the four men set out to kidnap Arumai but are astonished to see him get kidnapped by another group. They kidnap Arumai from the other kidnappers and discover that the first kidnapping was staged by Arumai himself to extort money from his father. Arumai manages to convince Das and his men to collude with him to obtain ransom money from his father. The group demand   from the minister and receive the money

An argument over splitting the cash arises between Arumai and the rest during which their van loses control and falls from a bridge. Arumai runs away with all the money. The minister seeks the assistance of Encounter Specialist Bramma, a brutal and merciless cop to hunt down the kidnappers. Arumai returns to his house and hides the money in his room. Das devices a plan to kidnap Arumai again to retrieve the money. They accidentally meet Arumai and successfully kidnap him again. But Das lets Arumai go free, after making him promise that he should not tell anything about them to the police. Arumai also promises to return their share of the money. Bramma finds that Arumai staged his own kidnapping and uses this information to threaten Arumai into testifying against the Das gang.

Arumai tells the court that the Das gang did not kidnap him and they are acquitted. A furious Bramma takes the gang to a remote location and brutally beats them and raises his department gun to kill them. Instead of shooting them with the officially issued gun, he goes out and retrieves from his Police Jeep an illegal homemade gun that he had previously seized from a crook. When he inserts that gun behind his back in his pants, the rusty gun which misfires into Brammas buttocks, allowing Das and others to escape. Arumais father breaks in and takes the money bag to the Chief Minister, who provided the ransom money. When he opens the bag, the minister is shocked to find it filled with newspapers instead of cash. Arumai had transferred the cash to another bag and gives the Das gang their share.

The Chief Minister calls Arumai to his office and asks him to stand as a candidate in the upcoming elections in lieu of his father, who never brought much income to the party due to his refusal to be corrupt. The Chief Minister praises Arumais shrewdness and believes he can rake in substantial income for the party. Arumai wins the election and appoints Sekar and Kesavan as his Personal Advisers. Pagalavan becomes an actor. Das continues his kidnapping business with new band of young men. They kidnap a woman who looks exactly like Shalu, but later only he knows that she is a Ministers Daughter, which means he has broken the 1st rule of Kidnapping again.

==Cast==
* Vijay Sethupathi as Das
* Sanchita Shetty as Shalu/Shalini Gupta
* Radha Ravi as the Chief Minister
* M. S. Bhaskar as Gnanodayam
* Ashok Selvan as Kesavan
* Bobby Simha as Pagalavan
* Ramesh Thilak as Sekhar Karunakaran as Arumai Pragasam
* Yog Japee as K. Bramma
* Aruldass as Rowdy Doctor/Dass brother
* Sivakumar as Nambikkai Kannan
* "Boys" Rajan as AC Manikandan
* Radha as Arumai Pragasams mother
* Kannayiram
* Gaana Bala in the song Kaasu Panam
* Karthik Subbaraj as jaguars owner

==Soundtrack==
{{Infobox album
| Name       = Soodhu Kavvum
| Longtype   = to Soodhu Kavvum
| Type       = Soundtrack
| Artist     = Santhosh Narayanan
| Cover      =
| Released   =
| Recorded   = 2013 Feature film soundtrack Tamil
| Label      = Think Music
| Producer   =
| Last album = Pizza (2012 film)|Pizza   (2012)
| This album = Soodhu Kavvum   (2013)
| Next album =     (2013)
}}

The soundtrack album and background music was scored by Santhosh Narayanan.

Tracklist 
{{tracklist
| extra_column = Singer(s)
| total_length =

| title1       = Come Na Come
| extra1       = Ganesh Kumar B, Chinna
| length1      = 03:56

| title2       = Mama Douser
| extra2       = Andrea Jeremiah
| length2      = 03:21

| title3       = Ellam Kadanthu Pogumada
| extra3       = Koavai Jaleel
| length3      = 02:42

| title4       = Sudden Delight
| extra4       = Rob Mass
| length4      = 02:34

| title5       = Sa Ga
| extra5       = Divya Ramani
| length5      = 01:44

| title6       = Kaasu Panam
| extra6       = Gaana Bala, Andony Dasan
| length6      = 02:27
}}

==Release== Ethir Neechal and Moondru Per Moondru Kadhal. 

===Critical reception===
Soodhu Kavvum opened to highly positive reviews. Baradwaj Rangan from The Hindu stated "Nalan Kumarasamy’s Soodhu Kavvum is a demonstration of what’s possible when movies are made for the sheer joy of making movies. There isn’t a single calculated moment, something cynically aimed to satisfy this segment of the audience or that one. Everything is organic, the events rooted in a nutty story and sprouting through a brilliant screenplay".  S. Saraswathi from Rediff gave 3.5 stars out of 5 and wrote "Soodhu Kavvum is an engaging film, with ingenious characters and entertaining situations" and called it a "must-watch".  N Venkateswaran from The Times of India gave 4 out of 5 stars and wrote "Nalan Kumarasamy establishes himself as a director to watch out for in this laugh riot of a debut movie. Carrying off a dark comedy is no mean task, but Nalan hits the target right in his first attempt. His writing is crisp, the lines are down to earth and funny, the characters well-etched and the screenplay has no dull moments".  Sify wrote "Soodhu Kavvum works big time due to smart writing and perfect characterisation. Final verdict on Soodhu Kavvum is that it is a gutsy great film. It is one of the best films to have emerged out of Kollywood in a long, long time".  Cinemalead wrote "Soodhu Kavvum is definitely a new attempt in Tamil cinema, go for it." 

Malini Mannath from The New Indian Express wrote "Engaging screenplay, deft narration, well-etched characters and twists and humour generated at unexpected moments, make Soodhu Kavvum a wacky jolly fun ride".  Behindwoods gave 3 stars out of 5 and said "Soodhu Kavvum’s scoring area is definitely its characterizations as Nalan Kumarasamy offers each of the central characters a back story that’s unique and more importantly contributing to the character’s present circumstance".  Haricharan Pudipeddi gave 3 stars out of 5 and stated "Soodhu Kavvum an entertaining watch".  Vivek Ramz from In.com|In rated the film 3.5 out of 5 and wrote "Soodhu Kavvum is a total laugh riot! 

Sudhish Kamath later picked Soodhu Kavvum as one of five films that have redefined Tamil cinema in 2013, writing, "This film is a joy to watch, full of laughs and unpredictable situations with great wit, dark humour and satire. Writer-director Nalan Kumarasamy, the winner of the first season of Nalaya Iyakkunar, is one of the most exciting filmmakers of our times with his ability to turn a cliché on the head".  The film also featured in Indo-Asian News Service|IANSs list of 10 best southern films of 2013, who called it "unarguably the funniest film of the year".  Sify  and Rediff  listed the film in the year-end top Tamil films lists too. The film was selected for screening in the Zurich Film Festival, being the only Tamil film of 2013 to be screened there. 
 

==Box office==
Soodhu Kavvum grossed   in Chennai theatres on its first weekend.  In its opening weekend it grossed   in US.  The film had collected    in two weeks in Tamil Nadu, according to IANS.  In June 2013, IANS reported that it had earned   totally. 

==Remakes==
After the films release, PVP Cinema purchased the Telugu remake rights in July 2013.  The film has been remade in Telugu as Gaddam Gang with Dr. Rajasekhar. The Kannada remake rights were bought by Rockline Venkatesh.  In October 2013, it was announced that Rohit Shetty will remake the film in Hindi.  It was reported to be remade in Malayalam too. 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 