Her Majesty the Barmaid
{{Infobox film
| name           = Her Majesty the Barmaid
| image          = 
| image_size     = 
| caption        = 
| director       = Joe May 
| producer       = Joe May
| writer         = Rudolph Bernauer   Adolf Lantz   Rudolf Österreicher 
| narrator       = 
| starring       = Käthe von Nagy   Francis Lederer   Otto Wallburg   Gretl Theimer
| music          = Walter Jurmann   Bronislau Kaper
| editing        = 
| cinematography = Otto Kanturek
| studio         = Deutsche Lichtspiel-Syndikat 
| distributor    = Standard-Filmverleih 
| released       = 9 January 1931
| runtime        = 102 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy film directed by Joe May and starring Käthe von Nagy, Francis Lederer and Otto Wallburg. It premiered on 9 January 1931. 

==Main cast==
* Käthe von Nagy as Lia Török 
* Francis Lederer as Fred von Wellingen  
* Otto Wallburg as Othmar von Wellingen - Freds Bruder 
* Gretl Theimer as Elli von Wellingen - seine Tochter 
* Alexandra Schmitt as Großmama 
* Adele Sandrock as Großtante Henriette 
* Szöke Szakall as Bela Török / Lias Vater 
* Ralph Arthur Roberts as Baron Schwapsdorf 
* Kurt Gerron as Hornberg 
* Walter Steinbeck as Hannemann 
* Tibor Halmay as Friedrich Hempel 
* Leo Monosson as Barsänger 
* Lina Woiwode as Frau von Lingenfeld 
* Paul Henckels as Standesbeamter 
* Gerhard Bienert as Werkmeister 

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 

 
 
 
 
 
 