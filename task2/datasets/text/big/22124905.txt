Sniffles and the Bookworm
 
Sniffles and the Bookworm is a 1939 Merrie Melodies cartoon from Warner Bros. Studios. Directed by Chuck Jones, this was the third cartoon with Sniffles the mouse. This cartoon, however, was more in line with the "books come to life" entries from around this period.

== Plot synopsis ==

Mother Goose characters come to life late at night in a bookshop, serenading Sniffles the mouse, and his bookworm friend (in his first appearance) with the swing song "Mutiny in the Nursery" by Johnny Mercer and Harry Warren, until the Frankenstein monster intrudes.

== Edited versions ==

The Turner "dubbed version" replaces the 1938 ending music with the 1941 ending music.

== External links ==
* 

 
 
 
 


 