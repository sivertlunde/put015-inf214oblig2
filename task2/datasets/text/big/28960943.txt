Redline (2009 film)
{{Infobox film
| name           = Redline
| image          = Redline (2009 film) poster.jpg
| caption        = Japanese theatrical release poster
| director       = Takeshi Koike
| producer       = {{Plainlist|
* Kentarō Yoshida
* Yukiko Koike}}
| screenplay     = {{Plainlist|
* Katsuhito Ishii
* Yōji Enokido
* Yoshiki Sakurai}}
| story          = Katsuhito Ishii
| starring       = {{Plainlist|
* Takuya Kimura
* Yū Aoi
* Tadanobu Asano}}
| music          = James Shimoji
| cinematography =
| editing        = Madhouse
| distributor    = Tohokushinsha Film
| released       =   |2010|10|9|Japan}}
| runtime        = 102 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}} Madhouse and sound directs.    The film is set in the distant future, where a man known as JP takes on great risks for the chance of winning the titular underground race.

After a total of seven years in production, Redline was intended to premiere at the 2009 Annecy International Animated Film Festival and follow Summer Wars, Mai Mai Miracle, and Yona Yona Penguin as the fourth and final feature film Madhouse planned to release between summer 2009 and spring 2010. However, further delays resulted in the delay of its world premiere, pushed back a few months to August 14, 2009, at the Locarno International Film Festival and its Japanese release to fall 2010.

==Plot== canine humanoids, Ducks ass|ducks ass-hairstyle wearing "Sweet" JP races in the planets Yellowline car race, a final elimination race, despite two interplanetary wars, to the most popular race in the galaxy, the Redline. The crowd is watching the race on an outdoor giant monitor screen in the desert at the Water Stop. His mechanic, Joshua "Frisbee" Flathead, is watching the televised race with a mafia boss. The gambling boss is concerned that JP might attempt to win the fixed race, but Frisbee reassures him that JP needs bail bond funds from the pay-off. JP attempts to win anyway while Frisbee resorts to a remote detonator transmitter hidden in his palm, which the mob boss doesnt notice, causing JPs TransAM20000 to explode, making the Crab Sonoshee Sea hovercraft, piloted by the female Sonoshee "Cherry Boy Hunter" McLaren, the winner.

While recuperating in a planet Dorothy hospital, Frisbee tells JP hes off the hook with his Bail bondsman|bondsman. JP initially turns down the money but a crowd of reporters storms the hospital room, where JP learns that he has been voted by popular demand for Redline, following the dropout of two qualifiers due to the selected location of the race, Roboworld. A planet dominated by zealous, purist, militant cyborgs whose President has threatened on interstellar television to hang all involved with the Redline mothership if they appear out of hyperspace over their planet with their hundreds of television satellites, exposing their secretive weapons build-up.

Inside DEST Tower on Roboworld, the President asks Secretary of Defense Titan for a report about ships landing on Roboworlds moon, EUЯPSS (pronounced Europass), a de-militarized zone that Roboworld signed away to refugees in the M-3 Nebula Federation, making it off limits to their worlds troops. The President obtains an oath from Colonel Volton to fight to the death to protect Roboworld and the M-3 Nebula.

While the racers stay on EUЯPSS, and as the days count down to REDLINE under constant media coverage, Frisbees alien junk dealer, Old Man Mole, wants to put weapons on the to-be-rebuilt TransAM, to which JP refuses, despite the conditions that hell be facing and the presence of weapons on his competitors crafts. Old Man Mole also protests the presence of Frisbee on their team and his insistence on using a rare, uncontrollable, hyper-powered engine, but eventually gives into JPs convincing.

JP finds Sonoshee McLaren in the slow hours of the Oasis Restaurant. Shinkai, formerly of the Roboworld army, shows up at the Oasis when racer Little Deyzuna, a recently AWOL subordinate of Col. Volton, attacks Shinkais partner, Trava. The Redline favorite, Machinehead, a large cyborg, appears and attacks Deyzuna, who in turn is retrieved by Col. Volton. Machinehead confronts Volton for being in the de-militarized zone. Volton reminds the racer the Redline event will be repelled by the army and police before leaving the damaged restaurant.

Fireworks and bookmaker agent parties break out on the Roboworld surface, which are raided by deadly android police robots. Miners on Roboworld use their suits to sabotage the military bases power station while the Race Commission, which promotes gambling, hires Earth-native racing partners Lynchman and Johnny Boya to sabotage the Orbital Disintegration Cannon which Sec. Titan plans to destroy the Redline mothership with the instant it comes out of hyperspace.

While in hyperspace, the Princess on the Redline ship from the planet Supergrass marks the course on a military base with a pair of scout vessels. The starting line at the north Knock-out Tower and the finish line to the south, east of DEST Tower. Unaware of the sabotage against the Orbital Disintegration Cannon, the President realizes the racers are on the planet once it fails and sends his troops en masse at the racers. The President becomes increasingly desperate as the racers evade them and approach the mine-laden Zone XXXXXXX (pronounced, Seven X), lair of a secret, illegal, biological weapon named Funky Boy, which awakens from its stasis as the racers and the rogue miners converge on it. With Funky Boy awakening and subsequent destruction of the base coinciding with the orbital cannon coming back online, the President orders fire on it. The explosion takes out most of the racers while JP rights his car off its side. Sonoshee, her vehicle ruined, agrees to rides with him to the finish line, as Funky Boy regenerates but is taken out by Col. Volton, who merges with yet another bioweapon.

Frisbee watches the race on televisions parked atop a mesa in the mob bosss giant Cadillac hovercraft where he revives the same Yellowline deal. Frisbee, however, refuses to detonate his charge this time, wanting to see his friend win the big one for once. He is attacked by mobsters for his decision, but is saved by Old Man Mole, who discovered Frisbees sabotage, and kills the mob boss and his lackeys.

As the race nears the finish line, Machinehead and JP race to the southern finish line, boosted by their rare Steamlight boosters. As they come to a desperate, neck-and-neck finish, a drunk Old Man Mole accidentally detonates Frisbees remote detonator, propelling JP and Sonoshee towards the finish without a vehicle. The boost is enough and JP wins by the length of his pompadour. As the racers rest at the end of the race, amidst the ruins of Roboworld, JP and Sonoshee float back down to the ground, declaring their love for each other.

==Cast==
* JP -  
* Sonoshee McLaren -  
* Frisbee -  
* Boiboi -  
* Bosbos -  
* Inuki Boss -  
* Gori-Rider -  
* Todoroki -  
* Miki -  
* Trava -  
* Shinkai -  
* Secretary of Defense Titan -  
* Colonel Volton -  
* Roboworld President -  
* Machine Head -  
* Old Man Mole -  
* Lynchman -  
* Johnny Boy -  
* Little Deyzuna -  

==Development and release== Madhouse  and directed by Takeshi Koike, who also wrote the storyboard and served as unit director and animation director. The films producers were Yukiko Koike and Kentarō Yoshida. Masahiro Fukushima served as executive producer. The task of writing the script was shared by Yoji Enokido, Katsuhito Ishii, and Yoshiki Sakurai. The films character designs, original and otherwise, were done by Katsuhito Ishii, who also served as one of the films sound directors, the other being Youji Shimizu. The films music was composed by James Shimoji. 

According to Tim Maughan of Anime News Network, Redline was released several years later than originally planned. Its development took seven years and used 100,000 hand-made drawings,    which Maughan notes is all the more unusual as it is Koikes directorial debut. 
 Annecy 2010 in the category "Feature Films out of competition".  During September, it was shown in Australia and New Zealand as part of Reel Anime 2010.  Redline was shown in Austin, Texas on September 23,  and in Edinburgh, Scotland, on October 15 as part of the Scotland Loves Anime film festival.    At the 2010 Nantes Utopiales Sci-Fi festival, the film received the special mention of jury and public.

The film opened in Japan on October 9, 2010. Coinciding with this, the film was shown in a San Francisco-based Viz Cinema theater from October 8–14, 2010. Distribution in North America is handled by Manga Entertainment. 

==Soundtrack==
An official soundtrack to the film by   was released under GBC label on October 6, 2010,  and contains 42 tracks.  It ranked 207th on Oricons album chart. 
{{tracklist
|collapsed=yes
|headline=Track listing: Redline Original Soundtrack title1   =Yellow Line title2   =Inuki title3   =Redline Title title4   =Boys Memory title5   =Winner March title6   =Roboworld TV title7   =TV Show title8   =Roboworld title9   =Europass title10  =Mogura Oyaji title11  =Oasis title12  =And Its So Beautiful (feat. Kitty Brown) title13  =Shinkai title14  =Machine Head title15  =Capture Operation title16  =Let Me Love You (feat. Veronica Torraca-Bragdon) title17  =Get The Stones (feat. Andrew O. Jones) title18  =Crab Sonoshee title19  =彼のシフトはブンブンブン (feat. Super Boins) title20  =Lynch Man & Johnny Boya title21  =Redline News title22  =Gori-Rider title23  =Miki & Todoroki title24  =Put-up Guy title25  =Red Angels title26  =Three-point Decomposition Cannon title27  =Tension title28  =Chatter Void title29  =Volton Unit title30  =Vertical Drop title31  =Moniter Room   title32  =Sand Biker title33  =Spinning Car title34  =Trouble title35  =Semimaru title36  =Gangster  title37  =Flying Finger title38  =Funky Boy title39  =Redline title40  =Exceed Limit title41  =Dead Heat title42  =Redline Day (feat. Rob Laufer)
}}
 

==Reception==
Tim Maughan of Anime News Network describes the film as "something very special, very different, and insanely exhilarating." In particular he praises the films director, saying that "Koike has managed to make all this chaos believable." He goes on to say that "Redline is animation not only at its best, but also largely animation for animations sake." Although Maughan says some may dislike its techno soundtrack and "minimal plot," he calls Redline "the most insanely exciting, visually exhilarating anime film youve seen in decades." 

Thomas Zoth of Mania.com comments that while the film does not provide "a deep plot or unique premise," it still "demands to be seen." He said that if Scott Pilgrim vs. the World was "an assault on the senses ... Redline is a declaration of war." Zoth went on to praise the film for its "imaginative creatures and clever designs", to compare Koikes work to that of Quentin Tarantino, and to compliment the film for its "soundtrack, with its memorable high-energy tracks that greatly complement the chaos onscreen." He concludes by saying that if "the life of the industry has been drained and replaced with rote, cookiecutter shows-by-committee, Redline shows a path out." 

Nicolas Penedo of the French magazine Animeland describes it as the "Paris-Dakar revisited à la Ōban Star-Racers",  a remake of Hanna-Barbera Productions|Hanna-Barbera Wacky Races with arts inspired from Jack Kirby comics and know-how, rhythm and energy inherent of the best Japanese anime movies.  The reviewer praises the animation quality as breathtaking,    and declares that Takeshi Koike made an homage to comics and films of the 70s and 80s.  On the negative, he notes the lack of scenario stating "Dont hope to find any scenario in Redline". 

Kwenton Bellette and Peter Martin reviewed Redline for Twitch Film. Bellette describes it as a "truly out of this world experience", "Speed Racer on crack" and praised the supporting characters for being memorable and the background galaxy made of different races and creeds to be very solid.  Martin expresses that "it feels like every centimeter of every frame is filled with some kind of kinetic color or action or bit of business, making it an experience that is sure to overload the senses" and asserts that writer Katsuhito Ishii succeeds at making a feature-length anime as insane as his film Funky Forest. 

Jon Liang of UK Anime Network comments that "A sense of the cool and outrageous is seeped into every pore of the design, ... exaggerated is an understatement here", and notes that the films "cinema-quality smooth animation" makes "even the most alien of things move naturally and the sense of speed that is often achieved is frequently mind-blowing." He remarks that "the visuals and over-the-top action will most likely overload sensitive brain cells," but concludes by calling Redline "an incredibly exciting cinematic experience that doesnt take itself at all seriously." 

==References==
{{reflist|2|refs=
   
   
   
   
   
   
   
   
   
   
   
   
   
}}

==External links==
*    
*  
*  
*  -(ANN)
*  -(ANN)
* 
 

 
 
 
 
 
 
 
 
 
 
 