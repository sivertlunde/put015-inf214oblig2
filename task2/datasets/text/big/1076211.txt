The Englishman Who Went Up a Hill But Came Down a Mountain
{{Infobox film
| name           = The Englishman Who Went Up a Hill But Came Down a Mountain
| italic title   = force
| image          = Englishman who went up a hill but came down a mountain.jpg
| caption        = Theatrical release poster
| director       = Christopher Monger
| producer       = Bob Weinstein Harvey Weinstein Sarah Curtis Sally Hibbin Robert Jones Scott Maitland Paul Sarony
| writer         = Ifor David Monger Ivor Monger Christopher Monger
| starring       = Hugh Grant Ian McNeice Tara FitzGerald Colm Meaney Kenneth Griffith
| music          = Stephen Endelman
| cinematography = Vernon Layton
| editing        = David Martin
| distributor    = Miramax Films
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| gross          = $10,904,930
}}
The Englishman Who Went Up a Hill But Came Down a Mountain is a 1995 British film with a story by Ifor David Monger and Ivor Monger, written and directed by Christopher Monger. It was entered into the 19th Moscow International Film Festival    and was screened in the Un Certain Regard section at the 1995 Cannes Film Festival.   

The film is based on a story heard by Christopher Monger from his grandfather about the real village of Taffs Well (Ffynnon Taf in Welsh language|Welsh), Rhondda Cynon Taff, Wales and its neighbouring Garth Hill. Due to 20th century urbanisation of the area, it was filmed in the more rural Llanrhaeadr-ym-Mochnant and Llansilin in Mid Wales.

==Plot== Welsh village of Ffynnon Garw ("Rough Fountain" or "Rough Spring" in Welsh language|Welsh) to measure its "mountain" &ndash; only to cause outrage when they conclude that it is only a hill because it is slightly short of the required 1000 feet in height. The villagers, aided and abetted by the wily Morgan the Goat and the Reverend Mr Jones (who after initially opposing the scheme, grasps its symbolism in restoring the communitys war-damaged self-esteem), conspire with Morgan to delay the cartographers departure while they build an earth cairn on top of the hill to make it high enough to be considered a mountain.

==Cast==
* Hugh Grant as Reginald Anson
* Ian McNeice as George Garrad
* Tara FitzGerald as Betty from Cardiff
* Colm Meaney as Morgan the Goat
* Ian Hart as Johnny Shellshocked
* Robert Pugh as Williams the Petroleum 
* Kenneth Griffith as the Reverend Robert Jones
* Ieuan Rhys as Sgt Thomas

==Reception== Waking Ned Kirk Jones. The Garth, and the Pentyrch History Society and the local community council have erected a notice on the mountain to explain its real historical significance. 

==Welsh language==
One of the most obscure jokes in the film occurs when a mechanic is asked about a nondescript broken part he has removed from a car, and replies "Well I dont know the English word, but in  " or "thingamajig."  However, this is made obvious in the novel on which the film is based. 

==See also==
*Mynydd Graig Goch, a member of the Moel Hebog group of summits. This is a Snowdonia hill that became a mountain in September 2008 when it was measured by three Welshmen with GPS equipment and found to be 30 inches taller than was thought, thereby exceeding the height required to classify it as a 2000-ft mountain by six inches.  

==References==
 

==External links==
 
*  
*  
*  
*   – Backsights Magazine (Surveyors Historical Society), originally published in Professional Surveyor, Nov./Dec. 1998

 
 
 
 
 
 
 
 
 
 