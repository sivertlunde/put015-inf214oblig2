Napoleon at Saint Helena
{{Infobox film
| name           = Napoleon at Saint Helena
| image          = 
| image_size     = 
| caption        = 
| director       = Lupu Pick
| producer       = Peter Ostermayr   Ottmar Ostermayr   Serge Sandberg   Lupu Pick
| writer         = Abel Gance   Willy Haas   Lupu Pick
| narrator       = 
| starring       = Werner Krauss   Hanna Ralph   Albert Bassermann   Philippe Hériat
| music          = Willy Schmidt-Gentner
| editing        = 
| cinematography = Robert Baberske   Ludwig Lippert   Fritz Arno Wagner   Friedrich Weinmann    
| studio         = Peter Ostermayr Produktion
| distributor    = Deutsche Lichtspiel-Syndikat
| released       = 7 November 1929
| runtime        = 100 minutes
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} silent historical Atlantic island of Saint Helena following his defeat at Battle of Waterloo|Waterloo. 

==Cast==
* Werner Krauss as Napoleon 
* Hanna Ralph as Madame Bertrand 
* Albert Bassermann as Hudson Lowe - Gouverneur von St. Helena 
* Philippe Hériat as General Bertrand 
* Louis V. Arco as Graf Montholon 
* Suzy Pierson as Madame Montholon 
* Hermann Thimig as General Gourgaud 
* Paul Henckels as Baron Las Cases 
* Georges Péclet as Kammerdiener Marchand 
* Theodor Loos as Hauptmann Pionkowski 
* Erwin Kalser as Dr. OMeara  Hugh Douglas as Captain Maitland 
* Fritz Odemar as Leutnant Nichols 
* Eduard von Winterstein as General Blücher 
* Albert Florath as Louis XVIII 
* Fritz Staudte as Talleyrand 
* Max Kaufmann as Kaiser Franz II von Österreich 
* Camillo Kossuth as Fürst Metternich 
* Alfred Gerasch as Tsar Alexander I 
* Philipp Manning as Lord Holland 
* Günther Hadank as Lord Wellington 
* Karl Etlinger as Buchhändler Simon 
* Jaro Fürth as Dr. Arnott 
* Ernst Rotmund as Oberst Reed 
* Magnus Stifter as Admiral Cockburn 
* Lilli Weiss as Kind 
* Matti Prinz as Kind 
* Martin Kosleck   
* Hermann Böttcher   
* Franz Schafheitlin   
* Stella Harf 
* John Mylong  
* Arthur von Klein   
* Petra Unkel

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: Jewish Presences in German and Austrian Film, 1910-1933. Berghahn Books, 2007.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 