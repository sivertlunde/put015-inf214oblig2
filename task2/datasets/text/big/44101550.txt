Choodatha Pookal
{{Infobox film 
| name           = Choodatha Pookal
| image          =
| caption        =
| director       = MS Baby
| producer       = V Gopalakrishnan Nair Vijayan (dialogues) Vijayan
| Kalpana Lakshmi Lakshmi Ratheesh T. R. Omana
| music          = KJ Joy
| cinematography = Vipin Das
| editing        = G Murali
| studio         = VGN International
| distributor    = VGN International
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film,  directed by MS Baby and produced by V Gopalakrishnan Nair. The film stars Kalpana (Malayalam actress)|Kalpana, Lakshmi (actress)|Lakshmi, Ratheesh and T. R. Omana in lead roles. The film had musical score by KJ Joy.   

==Cast==
  Kalpana
*Lakshmi Lakshmi
*Ratheesh
*T. R. Omana Shankar
*Zarina Wahab
*Vijay Menon
*Prathapachandran
*Sukumaran
*Balan K Nair Kunchan
*Master Vimal
 

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Poovachal Khader and ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Innen Kinaavil || Vani Jairam || Poovachal Khader || 
|-
| 2 || Manjanippoovin || P Susheela || Poovachal Khader || 
|-
| 3 || Oduvilee Sishirathin || K. J. Yesudas || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 