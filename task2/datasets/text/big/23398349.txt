Sankat City
{{Infobox film
| name = Sankat City
| image = Sankat City Movie Poster.jpg
| caption = Theatrical Poster Pankaj Advani
| producer = MoserBaer Entertainment Ltd. and 7 Entertainment Ltd. Pankaj Advani
| starring = Kay Kay Menon Anupam Kher Rimi Sen Chunky Pandey Dilip Prabhawalkar
| music = Ranjit Barot
| cinematography = Chirantan Das
| editing =
| distributor = 
| released =  
| runtime = 120 minutes
| country = India
| language = Hindi
| budget = 
}} Hindi movie Pankaj Advani starring Kay Kay Menon.  The film was released in India on July 10, 2009.

==Plot== Mercedes Rs. 10 million inside, unaware that the car belongs to a vicious gangster/loan shark called Faujdaar (Anupam Kher). They try to sell the stolen Mercedes to Suleman Supari (Rahul Dev), a hitman, who recognizes the car and notifies his friend Faujdaar. Faujdaar sends his henchman with Guru to get back the cash. 

Meanwhile, Ganpat has hidden cash in a safe place. After an accident, he loses his memory. Angered by the chain of events, Faujdaar gives Guru three days to return the money.

==Cast==
*Kay Kay Menon - Guru
*Anupam Kher - Faujdar
*Rimi Sen - Mona
*Chunky Pandey - Sikandar Khan and Sheshaiyya (Dual role)
*Dilip Prabhawalkar - Ganpat
*Rahul Dev - Suleman Supari Yashpal Sharma - Pachisia
*Hemant Pandey - Filip Fattu
*Virendra Saxena - Godman
*Shrivallabh Vyas - Sharafat
*Manoj Pahwa - Gogi Kukreja

==Critical reception==
Sankat City received generally positive reviews from film critics as a dark comedy.   

== Nominations in 2010 ==
*  Nominated for Most  Promising Debut Director, 55th Filmfare Awards. 
*  Nominated for Best Director and Best Film; Searchlight Awards - Max  Stardust Awards 2010.
*  Nominated for Best Screenplay, Most Promising Debut Director, and Best Ensemble Cast;  Nokia 16th Star Screen Award.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 