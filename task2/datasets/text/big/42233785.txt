Creep (2014 film)
{{Infobox film
| name           = Creep
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Patrick Brice
| producer       = Patrick Brice, Mark Duplass, Christopher Donlon   Jason Blum
| writer         = Patrick Brice, Mark Duplass
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Mark Duplass, Patrick Brice
| music          = Kyle Field, Eric Andrew Kuhn
| cinematography = 
| editing        = Christopher Donlon
| studio         = Blumhouse Productions
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Creep (originally titled Peachfuzz) is a 2014 comedy horror film directed by Patrick Brice, based on a script written by Brice and Mark Duplass. The movie, which is Brices directorial debut, had its world premiere on March 8, 2014 at South by Southwest and stars Brice as a man that answers a cryptic Craigslist ad. 

Duplass has confirmed plans for a sequel, stating "We’re in the process of selling the movie right now and there will definitely be more Creep." 

==Synopsis==
Aaron (Patrick Brice) is an optimistic videographer that decides to come work for Josef (Mark Duplass) after answering his ad on Craigslist. All Aaron has to do is record Josef throughout the day and remain discreet about the entire setup. Josef tells Aaron that hell be recording a series of videos for his unborn son, as hes suffering from a terminal illness and will never be able to see him grow up. While Josef seems strange, the money is too good for Aaron to pass up and he agrees to the task. However as the day progresses Josef becomes increasingly strange and Aaron finds it difficult to tell whether or not some of the things Josef is saying or doing are truly jokes or actually a sign of true danger and mental instability.

==Cast==
*Mark Duplass as Josef
*Patrick Brice as Aaron

==Production==
Brice and Duplass originally began working on Creep under the working title Peachfuzz, but chose to rename the film as the titles relevance came later in the movies plot and they did not want viewers to "spend the first half hour trying to figure out why the movie is called Peachfuzz and   pay attention to the very intricate details".    The two built the movie from a series of conversations they had with one another and decided to refine Creep while they were filming, which enabled them to film and screen portions of the film to see what would or wouldnt work on camera.  As a result the film had multiple alternate end scenarios and Duplass stated that there were "10 to 12 permutations of each scene". 

Of the creative process for his character, Josef, Duplass explained "We were interested in the psychological profile of this very, very strange person. We were very interested in how you meet people and don’t quite understand what’s up, but you start to get signs. For us that was intense eye contact, lack of personal space, oversharing, maybe a little bit too much love here and there. But, for me, there’s something wrong with both of these guys. Deeply. This concept of, “who is the creep in this scenario?”"   

==Reception==
Critical reception for Creep was mostly positive.   The Hollywood Reporter and Indiewire both gave the film positive reviews,  and Indiewire noted that although the film had its flaws, that they mostly worked in Creeps favor.  Variety (magazine)|Variety remarked that Creep "could have been more effective if Duplass’ performance were a shade more ambiguous, and the audience had a chance to at least fleetingly believe Josef might be telling the truth" but that "Despite the blatancy of his character’s ulterior motives, Duplass scores a considerable impact by making the most of the aforementioned plot twists."  In contrast, Shock Till You Drop panned the movie overall, stating that "Creep might work for those dont regularly digest horror films, but for the hardened fan, this is a film that spins its wheels all too often and feels like an exercise in self-indulgence." 

==References==
 

==External links==
*  

 
 
 
 