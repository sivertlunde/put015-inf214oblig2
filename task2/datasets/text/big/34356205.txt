Shut Up and Play the Hits
{{Infobox film
| name           = Shut Up and Play the Hits
| image          = Shut Up and Play the Hits.jpg
| alt            =
| caption        = DVD cover
| director       = Dylan Southern Will Lovelace James Murphy Terry Felgate
| starring       = LCD Soundsystem Chuck Klosterman
| cinematography = Reed Morano
| editing        = Mark Burnett
| distributor    = Pulse Films
| released       =  
| country        = United Kingdom
| runtime        =
| language       = English
}} James Murphy over a 48-hour period, from the day of the bands final gig at Madison Square Garden to the morning after the show.    The film also features intermittent segments from an extended interview between Murphy and pop culture writer Chuck Klosterman.  The film premiered at the Sundance Film Festival on January 22, 2012, and was released in the USA for one night only on July 18, 2012.    UK showings were held on 4 September 2012.

James Murphy performs a duet with comedian and musician Reggie Watts for one song during the show. Several members of the band Arcade Fire provide backing vocals during a performance of "North American Scum". The films title is a reference to the moment Win Butler of Arcade Fire shouts "shut up and play the hits" as James introduces the song.
 James Murphy and Nancy Whangs appearances in the Soulwax film Part of the Weekend Never Dies). Comedian Donald Glover can also be seen dancing in the crowd and making a face for the camera.

The film was released to Blu-ray and DVD on October 9, 2012 in the US and 8 Oct, 2012 in the UK, with a digital release on the bands official Facebook page soon afterwards.

A relatively complete audio recording of this concert was released in April 2014, entitled  .

==Reception==
The film was met positively by critics and fans alike, particularly for its concert coverage and direction. On Rotten Tomatoes the film holds a rating of 88%, based on 25 reviews.  On Metacritic the film has a score of 72 out of 100, indicating "generally favorable reviews".  Henry Barnes of The Guardian wrote that it is a "gorgeously shot concert film that cuts repeatedly between the near-desperate abandon of the show and Murphy, in his apartment the following day, wandering around in his pants and petting the dog."  

In November 2012, Shut Up and Play the Hits won the "Best Live Music Coverage" category of the UK Music Video Awards.   

==Track list==
#  Dance Yrself Clean
#  Drunk Girls
#  I Can Change
#  Time To Get Away
#  Get Innocuous!
#  Daft Punk Is Playing At My House
#  Too Much Love
#  All My Friends
#  Tired
#  45:33 Part One
#  45:33 Part Two
#  Sound of Silver (intro)
#  45:33 Part Four
#  45:33 Part Five
#  45:33 Part Six
#  Freak Out/Starry Eyes
#  Us v Them
#  North American Scum
#  Bye Bye Bayou (Alan Vega cover)
#  You Wanted A Hit
#  Tribulations
#  Movement
#  Yeah (outro)
#  Someone Great
#  Losing My Edge
#  Home
#  All I Want
#  Jump Into The Fire (Harry Nilsson cover)
#  New York I Love You, But Youre Bringing Me Down 

==References==
 

==External links==
*  
*  

 

 
 
 
 


 
 