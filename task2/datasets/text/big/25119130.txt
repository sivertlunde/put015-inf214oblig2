The Men of Sherwood Forest
 
 
{{Infobox film
| name           = The Men of Sherwoof Forest
| image_size     = 
| image    	 = "The_Men_of_Sherwood_Forest"_(1954).jpg
| caption        = 
| director       = Val Guest
| producer       = Michael Carreras
| writer         = Allan MacKinnon
| narrator       =  Don Taylor Reginald Beckwith Eileen Moore
| music          = Doreen Carwithen
| cinematography = Walter J. Harvey
| editing        = James Needs
| studio         = Hammer Films (UK)
| released       = 1954 (UK)
| runtime        = 77 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1954 UK|British Don Taylor, Reginald Beckwith, Eileen Moore and David King-Wood. The film follows the exploits of Robin Hood and his followers.    Doreen Carwithen wrote the score for the film.

==Plot== Richard the Lionheart is taken prisoner in Germany. Disguised as a troubadour, Robin Hood builds a plan to take out it of this tight spot. Unfortunately, he is captured. The Merry Men then have to fulfil a double mission: find Robin Hood and save the King.

==Cast== Don Taylor - Robin Hood
* Reginald Beckwith - Friar Tuck
* Eileen Moore - Lady Alys
* David King-Wood - Sir Guy Belton
* Douglas Wilmer - Sir Nigel Saltire Harold Lang - Hubert
* Ballard Berkeley - Walter
* Patrick Holt - King Richard
* Wensley Pithey - Hugo
* Leslie Linder - Little John
* John Van Eyssen - Will Scarlett

==Crew==

* Director: Val Gest
* Assistant director: Jack Sangster 
* Screenplay by Allan MacKinnon
* Photography: Walter J. Harvey, B.S.C
* Music by Doreen Carwithen
* Musical director: John Hollingsworth
* Art director: J. Elder Wills
* Editor: James Needs
* Production manager: Jimmy Sangster
* Produced by Michael Carreras
* Sound recording: Sid Wiles and Ken Cameron
* Continuity: Renee Glynne
* Camera operator: Len Harris
* Make-up: Phillip Leakey
* Hairdresser: Monica Hustler
* Costume designer: Michael Withaker
* Wardrobe mistress: Molly Arbuthnot
* Production company: Hammer Film Productions
* Country: England
* Aspect ratio: 1.37 : 1 - Eastmancolor - Mono (RCA Sound Recording)
* Runtime : 77 mn
* Release date: 17 November 1954
* Produced at Bray Studio, England

==Critical reception== ITV series starring Richard Greene," concluding "Val Guest directs with little feel for the boisterous action, but its a tolerable frolic all the same" ;   while TV Guide wrote, "this low-budget swashbuckler is good fun for the undiscriminating."  

==References==
 

 
 

 
 
 
 
 
 
 
 


 
 