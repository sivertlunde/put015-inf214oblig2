Friday the 13th Part VII: The New Blood
{{Infobox film name            = Friday the 13th Part VII: The New Blood image           = friday7.jpg caption         = Theatrical poster director        = John Carl Buechler producer        = Frank Mancuso, Jr. Iain Paterson Barbara Sachs writer          = Manuel Fidello Daryl Haney starring  Kevin Blair Susan Blu Terry Kiser Kane Hodder music           = Harry Manfredini Fred Mollin cinematography  = Paul Elliott editing         = Maureen OConnell Martin Jay Sadoff Barry Zetlin distributor     = Paramount Pictures released        =   runtime         = 88 minutes country         = United States language  English
|budget          = $2.8 million  (estimated)  gross           = $19.1 million  (domestic) 
|}}

Friday the 13th Part VII: The New Blood is the seventh installment in the original   and precedes  .

==Plot==
Shortly after the events of the  , seven-year-old Tina Sheppard witnesses her father, John Sheppard, abusing her mother, Amanda Sheppard, and runs out onto the lake in a boat. When John tries to retrieve and apologize to her, Tinas latent telekinetic powers awaken and she accidentally collapses the dock on him, causing him to drown. Ten years later, after having been shuffled around from hospital to hospital, Tina and Amanda return to the lake at the request of her doctor Dr. Crews in order to face her fear and trauma over the death of her father. She meets Nick, who organized a surprise birthday party for his cousin Michael and he becomes smitten with her, much to the chagrin of Melissa, a spoiled socialite who has her eye on him.

Crews tries to incite Tina to use her telekinetic powers through constant persuasion and manipulation, though under the guise of psychiatric care, he plans to exploit Tinas gifts. After a particularly disturbing confrontation with Dr. Crews that night, Tina runs out to the docks and believes she senses her fathers presence in the lake. She uses her powers to resurrect him, but accidentally frees Jason Voorhees from his imprisonment. Tina faints at the sight of him, but her sighting is shrugged off as a delusion by Crews. Meanwhile, Jason kills birthday boy Michaels girlfriend Jane with a spike to the neck, then Michael himself by stabbing it into his back. He also kills a nearby couple camping, by punching Dans heart out and bashing his girlfriend when she tries to hide in her sleeping bag. Nick invites Tina to meet the party guests and Amanda allows it, to allow Tina some semblance of a normal life. She meets the plucky Robyn, shy Maddy, stoner David, writer Eddie, and quarrelling lovers Ben and Kate. Tina experiences a realistic vision of Michael being killed and runs back to the house. She sees a spike in the porch, but when Dr. Crews goes out to find it they find nothing, leaving Tina to think she is losing her mind. The next morning, everyone is disappointed that Michael doesnt show up, and Tina tells Nick about her experience with her father as well as being in mental institutions. Melissa later exploits this knowledge to make fun of Tina, who snaps Melissas pearl necklace with her abilities. That night, two other party guests; Ivy League Russell, is killed with an axe to the face and his girlfriend Sondra is dragged under the surface of the lake and is drowned as Jason moves into the area. He then disrupts Ben and Kates makeup sex, killing Ben by crushing his head when he ventures out of the vehicle, then shoving a party horn into Kates eye. Maddy inadvertently ends up outside and discovers Russells body, she runs to a nearby shed where her throat is slashed by a sickle.

Amanda discovers a video documenting Tinas powers and realizes Crews true motives. The ensuing argument forces Tina to run away in Amandas car. She sees a vision of Amanda being killed by Jason and crashes before running off into the woods on her own. She meets with Nick in the woods and they discover Michaels body. Meanwhile, Jason cuts the power in the teens house and kills David who comes downstairs for food. Robyn goes into Maddys room and discovers Davids severed head before she is thrown from the window and killed. Eddie is shunned by Melissa who was using him as a means to make Nick jealous and he goes downstairs. After Melissa leaves the house, Eddie is killed when Jason rams a knife into his neck. Meanwhile, Tina and Nick return to her cabin and Nick leaves to try and gather everyone. Amanda and Dr. Crews venture into the woods after finding the car when Jason suddenly attacks them; Crews uses Amanda as a human shield and she is killed. After finding Tina returning to the woods and failing to warn her away, Dr. Crews is killed when Jason bisects him with a tree saw. Tina discovers her mothers body, then discovers Kate and Sondras bodies before meeting Jason, instigating a battle between mind and matter.

Using her powers to keep Jason at bay, She strikes him with tree branches, and electrocutes him. She then lures him through the teens house, discovering Davids severed head. As impervious to harm as Jason is, Tina is able to hold him at bay with her powers, slamming objects into him and and she eventually sends the porch roof down on his head and believes him to be dead as she returns to Nick and Melissa, who had gone next door. Sickened by the stories, Melissa storms out the door, but encounters Jason who slams an axe into her skull. Upstairs, Tina causes a light to shatter in Jasons face, sending him down into the stairwell. He recovers and knocks Nick out, but Tina forces him off him by crushing his head in the mask until it snaps, revealing his horrible visage. She then hangs him, and drops him into the basement before checking on Nick and is suddenly dragged down into it. She attacks him with nails, douses him with gasoline and lights it with the furnace, causing an inferno. Nick recovers and he and Tina run from the house before it explodes. Jason returns however and knocks Nick out, leaving Tina defenseless. With the last ounce of her strength, she calls on her father, who appears from below the pier and wraps Jasons chains around him, dragging him into the water before Tina passed out.

The next morning in the aftermath, Tina and Nick are loaded into an ambulance. When Nick asks where Jason was, Tina simply responds "We took care of him." Meanwhile, one of the officers discover Jasons split hockey mask, but the lake itself appears calm.

==Cast==
* Kane Hodder as Jason Voorhees Tina Shepard
* Kevin Spirtas as Nick
* Susan Blu as Amanda Shepard
* Terry Kiser as Dr. Crews Melissa
* Elizabeth Kaitan as Robin William Butler as Michael
* Staci Greason as Jane
* Jon Renfield as David
* Jeff Bennett as Eddie  Sandra
* Diana Barrows as Maddy
* Larry Cox as Russell
* Craig Thomas as Ben
* Diane Almeida as Kate
* John Otrin as Mr. Shepherd Jennifer Banko as Young Tina

==Production== Nightmare on film was eventually made possible when New Line bought the rights to the Friday the 13th series, but did not see release until 2003.
 Bay Minette. This film marks the first of four appearances by Kane Hodder as Jason, the only actor to ever reprise the role. Although C. J. Graham, who had portrayed Jason in Part VI, was initially considered, Hodder was ultimately chosen based on his work in the film Prison, for which The New Bloods director, Carl Buechler had worked on as the special effects makeup artist. In that movie, Hodder filmed a scene in which his character—a prisoner executed in the electric chair—rises from the grave; Hodder himself had suggested to Buechler that he have maggots coming out of his mouth during the scene to heighten the effect of decomposition, and went on to film the sequence with live maggots spilling out of his mouth. Buechler remembered Hodders commitment to the part when casting The New Blood, and chose Hodder over Graham. Graham expressed disappointment, as he had hoped to reprise the role of Jason and make himself synonymous with the character, as Boris Karloff had with Frankensteins monster, but ultimately expressed satisfaction with Hodders portrayal and said that he bore no ill will about not being asked to return. Hodder would go on to make cinematic history for the longest uninterrupted on-screen controlled burn in Hollywood history. For the scene in which Tina causes the furnace to shoot flames at Jason, Hodder was actually set on fire by an apparatus rigged so that the ignition could be captured on film (as opposed to being edited in later with trick photography). Hodder was on fire for a full forty seconds, a record at the time.
 blood spurt; Dans original death had Jason ripping out his guts; Amanda Shepards death originally showed Jason stabbing her from behind, with the resulting blade going through her chest and subsequent blood hitting Dr. Crews; Dr. Crewss death showed Jasons tree-trimming saw violently cutting into his stomach, sending a fountain of blood and guts in the air; Melissas original death had Jason cleaving her head in half with an axe with a close-up of her eyes still wriggling in their sockets. The boxed set DVD release of all of the films and the single deluxe edition have all these scenes available as deleted scenes in rough workprint footage, however the deluxe edition features more additional footage than the boxed set.

The narration in the prologue of the film (spoken by Walt Gorney) is as follows:
:Theres a legend round here. A killer buried, but not dead. A curse on Crystal Lake. A death curse. Jason Voorheess curse. They say he died as a boy, but he keeps coming back. Few have seen him and lived. Some have even tried to stop him. No one can. People forget hes down there... waiting.

==Box office and reception==

===Release=== The Serpent Bad Dreams, The Blob, Phantasm II, and Childs Play (1988 film)|Childs Play. Ultimately, the film would go on to gross a total of $19.2 million at the U.S. box office, placing it at number 53 on the list of the years top earners.

===Critical response===
The film received negative reviews from critics. John Carl Buechler, the director, who also created the special make-up effects for the film, is credited with creating "the definitive Jason" in the audio commentary of the film from the series DVD box set. The film is later mentioned in the novels American Psycho by Bret Easton Ellis and The Time Travelers Wife by Audrey Niffenegger. It currently holds a 25% rating on review aggregator Rotten Tomatoes, based on 17 reviews.

==Soundtrack==
On  , BSX records released a limited edition CD of Fred Mollins Friday the 13th Part VII and VIII scores. 

==References==
 

2. R.I.P. Susan Jennifer Sullivan (Melissa, Friday The 13th Part 7: The New Blood)
- See more at: http://www.fridaythe13thfranchise.com/2012/11/rip-susan-jennifer-sullivan-melissa.html#sthash.LTjZv0O4.dpuf
This is sad news to pass along as it has been speculated about for many years now, but the good folks producing the Crystal Lake Memories documentary have confirmed Susan Jennifer Sullivans passing. They have found the obituary for the late actress, which is posted below, and it shows that Susan was lost to the world on August 10, 2009. Susan was such a memorable part of Friday The 13th Part 7: The New Blood that fans still talk about her performance and lasting impression on the franchise almost 25 years later. This writer only wishes we could have had a chance to hear her stories about her time filming The New Blood and find out more about what she did in life after acting. - See more at: http://www.fridaythe13thfranchise.com/2012/11/rip-susan-jennifer-sullivan-melissa.html#sthash.LTjZv0O4.dpuf
From The Boston Herald SULLIVAN Susan J., in Randolph, formerly of West Roxbury, August 10, 2009, age 46. Beloved wife of Ed K. Taylor. Devoted daughter of the late Charles A. (BPD) and Carol J. (Marcus) Sullivan. Loving sister of Karen L. Albergo of Randolph, John of Maynard, Stephen of Hanson and David of Taunton. Also survived by 21 nieces, nephews and 1 grandniece.  - See more at: http://www.fridaythe13thfranchise.com/2012/11/rip-susan-jennifer-sullivan-melissa.html#sthash.LTjZv0O4.dpuf

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 