Frenchman's Creek (film)
{{Infobox film
| name           = Frenchmans Creek
| image          = Frenchmans Creek poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Mitchell Leisen
| producer       = Buddy G. DeSylva
| writer         = Daphne Du Maurier (novel) Talbot Jennings
| narrator       =
| starring       = Joan Fontaine Arturo de Córdova Basil Rathbone
| music          = Victor Young George Barnes
| editing        = Alma Macrorie
| distributor    = Paramount Pictures
| released       =
| runtime        =
| country        = United States English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 novel (about English woman French pirate), Clair de Lune as the love theme for the film.
 Charles II Cornish region of England. 

Fontaine was under contract to independent producer to David O. Selznick, who only produced a few films each year.  Typically, he loaned out his contract players and director Alfred Hitchcock (who had a contract with Selznick from 1940 to 1947) to other studios.  In this case, Fontaine was loaned to Paramount for this lavish production.  She later complained about her work with director Leisen and some of her costars. 

Although the film has not been released on DVD, it has been shown on American Movie Classics.

==Cast==
* Joan Fontaine - Dona St. Columb
* Arturo de Córdova - Jean Benoit Aubrey
* Basil Rathbone - Lord Rockingham
* Nigel Bruce - Lord Godolphin
* Cecil Kellaway - William
* Ralph Forbes - Harry St. Columb
* Harald Maresch - Edmond (as Harald Ramond)
* Billy Daniel - Pierre Blanc (as Billy Daniels)
* Moyna Macgill - Lady Godolphin
* Patricia Barker - Henrietta David James - James Charles Coleman - Thomas, the footman David Clyde as Martin, the coachman

==Awards== Best Art Direction (Hans Dreier, Ernst Fegté, Sam Comer).   

==See also==
* The Wicked Lady, a British film made a year later and telling a similar story but with very different sensibilities.

==References==
 

==External links==
* 
* 
*  on Theater of Romance: May 22, 1954

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 