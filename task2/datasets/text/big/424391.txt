Stardust Memories
 
{{Infobox film
| name            = Stardust Memories
| image           = Stardust_memories_moviep.jpg
| image_size      =
| alt             =
| caption         = Theatrical release poster
| director        = Woody Allen Jack Rollins
| writer          = Woody Allen Tony Roberts
| cinematography  = Gordon Willis
| editing         = Susan E. Morse
| distributor     = United Artists
| released        =  
| runtime         = 88 minutes  
| language        = English
| budget          = $10 million
| gross           = $10,389,003
}} black and white and is reminiscent of Federico Fellinis 8½ (1963), which it parodies.

The film was nominated for a Writers Guild of America award for Best Comedy written directly for screen. Allen denies that this film is autobiographical and has expressed regret that audiences interpreted it as such.  "  thought that the lead character was me," the director is quoted as saying in Woody Allen on Woody Allen  . "Not a fictional character but me, and that I was expressing hostility towards my audience. That was in no way the point of the film. It was about a character who is obviously having a sort of nervous breakdown and, in spite of success, has come to a point in his life where he is having a bad time."

==Plot==
The film follows famous filmmaker Sandy Bates, who is plagued by fans who prefer his "earlier, funnier movies" to his more recent artistic efforts, while he tries to reconcile his conflicting attraction to two very different women: the earnest, intellectual Daisy and the more maternal Isobel. Meanwhile, he is also haunted by memories of his ex-girlfriend, the unstable Dorrie.

==Cast==
* Woody Allen as Sandy Bates
* Charlotte Rampling as Dorrie ("She was just right for that part," the director is quoted as saying in Woody Allen on Woody Allen  . "I mean, she is so beautiful and so sexy and so interesting. She has an interesting neurotic quality.")
* Jessica Harper as Daisy
* Marie-Christine Barrault as Isobel Tony Roberts as Tony Daniel Stern as Actor
* Amy Wright as Shelley
* Helen Hanft as Vivian Orkin
* John Rothman as Jack Abel
* Anne De Salvo as Sandys Sister
* Leonardo Cimino as Sandys Analyst
* Sharon Stone as Pretty Girl on Train (Of the train window through which she blows a kiss during the opening sequence at the beginning of the film, Stone remarked, "I gave it my best shot to melt that sucker."  ) Jack Rollins as a Studio Executive (Rollins and his partner Charles H. Joffe produced all of Allens films from 1969 to 1993, including Stardust Memories.) Three Little Words" 25th Anniversary Playmate, as Tonys Girlfriend
* Brent Spiner as a Fan in Lobby
* film critic Judith Crist as a Cabaret Patron
* Irwin Keyes as a Fan Outside Hotel
* Bonnie Hellman as a Fan Outside Hotel
* Cynthia Gibb as a Young Girl Fan (credited as "Cindy Gibb")
* Annie Korzen as Woman in Ice Cream Parlor (credited as "Anne Korzen") James Otis as a UFO Follower
* Alice Spivak as Nurse at Hospital
* Armin Shimerman as a member of the Eulogy Audience
* Laraine Newman as Film Executive (uncredited)
* Louise Lasser as Sandys Secretary (uncredited)   

==Themes==
The conflict between the maternal, nurturing woman and the earnest, usually younger one, is a recurring theme in Allens films. Like many of Allens films, Stardust Memories incorporates several jazz recordings including those by such notables as Louis Armstrong, Django Reinhardt, and Chick Webb. The films title alludes to the famous take of "Stardust (song)|Stardust" recorded in 1931 by Armstrong, wherein the trumpeter sings "oh, memory" three times in succession. However, it is the master take that plays in the movie during the sequence where Sandy is remembering the best moment of his life: looking at Dorrie while listening to Armstrongs recording of the song. 

The film deals with issues regarding religion, God, and philosophy; especially existentialism, psychology, symbolism, wars and politics.  It is also about realism, relationships, and death.  It refers to many questions about the meaning of life. It also ruminates on the role that luck plays in life, a theme Allen would revisit in Match Point.

==Production==
;Filming locations
* Asbury Park, New Jersey, USA
* Belmar, New Jersey, USA
* Deal, New Jersey, USA
* Hoboken, New Jersey, USA
* Neptune City, New Jersey, USA
* Ocean Grove, New Jersey, USA   
 Ocean Grove Great Auditorium and the Methodist Episcopal Conference Center and Concert Hall in New Jersey. Most of the interiors, including the bedroom scenes, were shot in a vacant Sears Roebuck building, but the crew also recreated a vintage train at Filmways Studio in Harlem. To reproduce the movement of a rail car, the whole train was mounted on jacks and gently jostled back and forth."

==Reception==
The film sharply divided both audiences and critics, with many Allen fans proclaiming it his best picture or among his worst.   In October 2013, the film was voted by the Guardian (newspaper)|Guardian readers as the eighth best film directed by Woody Allen. 
 the very guy who had asked him for his autograph earlier in the day… This is what happens with celebrities: one day people love you; the next day they want to kill you."

==Box office== profit after foreign revenue was taken into account. 

==Soundtrack==
* "Hebrew School Rag" (Dick Hyman) by Dick Hyman Just One of Those Things" (Cole Porter) by Dick Hyman
* "Youd Be So Easy to Love" (Cole Porter) by Dick Hyman
* "Tropical Mood Meringue" (Sidney Bechet) by Sidney Bechet
* "Ill See You in My Dreams (song)|Ill See You in My Dreams" (Isham Jones and Gus Kahn) by Django Reinhardt
* "Tickletoe" (Lester Young) by Lester Young with Count Basie and His Orchestra Three Little Words" (Harry Ruby, Bert Kalmar) by The Jazz Heaven Orchestra
* "Aquarela do Brasil|Brazil" (Ary Barroso, S.K. Russell) by Marie Lane
* "Palesteena" (J. Russel Robinson and Con Conrad) by The Original Dixieland Jazz Band Body and Soul" (Edward Heyman, Robert Sour, Johnny Green, and Frank Eyton) by Django Reinhardt
* "Night on Bald Mountain" (Modest Mussorgsky) by Vienna State Opera Orchestra
* "If Dreams Come True" (Irving Mills, Edgar M. Sampson, and Benny Goodman) by Chick Webb Just One of Those Things" (Cole Porter) by Dick Hyman
* "Youd Be So Easy to Love" (Cole Porter) by Dick Hyman
* "One OClock Jump" (Count Basie) by The Jazz Heaven Orchestra
* "Sugar (Maceo Pinkard song)|Sugar" (Maceo Pinkard and Sidney D. Mitchell)
* "Sweet Georgia Brown" (Ben Bernie, Kenneth Casey and Maceo Pinkard)
* "Moonlight Serenade" (Glenn Miller) by Glenn Miller
* "Stardust (song)|Stardust" (Hoagy Carmichael, Mitchell Parish) by Louis Armstrong   

==References==
 

== Further reading ==
*  

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 