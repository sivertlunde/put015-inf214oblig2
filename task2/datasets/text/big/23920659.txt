Housefull (2010 film)
 
 
{{Infobox film
| name           = Housefull
| image          = Housefull Poster.jpg
| caption        = Theatrical release poster Sajid Khan
| producer       = Sajid Nadiadwala
| writer         = Anvita Dutt (dialogue)
| story          = Sajid Nadiadwala
| screenplay     = Milap Zaveri Sajid Khan Vibha Singh
| starring       = Akshay Kumar Arjun Rampal Ritesh Deshmukh Deepika Padukone Lara Dutta Jiah Khan
| music          = Shankar-Ehsaan-Loy cinematography  = Vikas Sivaraman editing         = Rameshwar S. Bhagat
| studio         = Nadiadwala Grandson Entertainment ShowMan Pictures Eros Entertainment
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Hindi
| budget         =     
| gross          =     
}} Sajid Khan, starring Akshay Kumar as the protagonist and Deepika Padukone, Lara Dutta along with the late Jiah Khan as the female leads. Arjun Rampal, Ritesh Deshmukh and Boman Irani also play important roles in the film, whilst Malaika Arora Khan and Jacqueline Fernandez make special appearances. This would be Jiah Khans final film before her death in 2013.  The film was shot over several months in London and Italy. Produced by Sajid Nadiadwala, the film released at more than 750 theatres on 30 April 2010 in India.    It received negative reviews upon release, but was nonetheless a success at the box office. Housefull film series.

The story has also been noted to be copied from Kamal Haasans 1998 Tamil movie Kaathala Kaathala.

==Plot== Telugu girl Sandy (Deepika Padukone).

Sandy originally thinks Aarush is a pervert, but when Aakhri Pasta (Chunkey Pandey), the owner of Italys biggest hotel where Aarush was spending his honeymoon with Devika, tells Sandy that Aarush is a widower, she befriends him and slowly falls in love with him. What she doesnt know, is that Aakhri Pasta was only joking. She then finds out his wife is alive and leaves him, but when he tries to make her understand, she accepts him.

The only way Aarush can get married to Sandy is by convincing her elder brother, Major Krishna Rao (Arjun Rampal), a strict Indian Military Intelligence officer who loves and is over-protective of Sandy. Meanwhile, Hetal lies to her father, Batuk Patel (Boman Irani), that Bob owns a mansion and they have a child. The four rent a mansion to make him believe that Bob owns it, but Batuk accidentally believes that Aarush is married to Hetal, and Bob is the cook. Then, Krishna turns up, much to Aarushs horror, leading to more lies.

For half of the term, the four must pretend that the mansion is owned by Bob, and the rest must pretend that Aarush owns it. An overly suspicious Krishna sees Bob and Hetal working at the casino and uses a lie detector to find the truth. After Sandy objects, Krishna apologises and agrees to Aarush & Sandys marriage. The whole family is invited to the Royal Palace, to see Krishna get rewarded by the Queen of England. While this is happening, 2 workers are meant to be installing air conditioning gas for the place, however instead they accidentally supply the hall with laughing gas causing everyone to break into an outburst of laughter. During this laughter, the truth is let out but no one seems to be in the right state of mind.

The 2 workers meanwhile stop the gas from spreading – everyone comes back to their senses. Aarush is still telling Krishna the truth, leading Krishna to break off Sandys engagement. However, after Sandy tearfully pleads with him, Krishna lets the two get married. In the end, Hetal and Bob live together with Batuk Patel, and Aarushs luck changes for the better.

== Cast ==
* Akshay Kumar as Aarush
* Deepika Padukone as Saundarya "Sandy" Bhagayalaxmi Venkateshwari Basapa Rao
* Lara Dutta as Hetal Patel
* Ritesh Deshmukh as Babu "Bob" Rao
* Jiah Khan as Devika
* Arjun Rampal as Major Krishna Rao
* Boman Irani as Batuk Patel Daisy Irani as Batuk Patels Mother
* Lillete Dubey as Zulekha Bano
* Chunkey Pandey as Aakhiri Pasta
* Randhir Kapoor as Kishore Samtani
* Malaika Arora Khan as Pooja
* Jacqueline Fernandez as Dhanno (Special appearance in song "Aapka Kya Hoga")
* Vindu Dara Singh as Security Guard
* Suresh Menon as Santa Singh
* Manoj Pahwa as Banta Singh

==Reception==

===Critical response===
Housefull generally received negative reviews from critics. Review aggregator website ReviewGang rated the film 3.5/10.  On website Rotten Tomatoes, the film holds a 43% rotten rating. 

Anupama Chopra of NDTV said that it was impossible to take the characters seriously, and gave the movie a rating of 2/5.    gave it 2/5 and said "Housefull is a slap on your senses as the film sticks to the slapstick genre in every literal sense.  There are some genuinely funny moments in the second half esp. Boman Irani sleepwalking through his portrayal of Gujarati Sholay. Unfortunately it fails to salvage the harm caused by the bland, boring and banal first half. Housefull ends up being a painful experience."   

In a rare positive review, Taran Adarsh of Bollywood Hungama gave the film a rating of 4/5, saying "Housefull is a complete laugh-riot, and is an entertainer all the way, targeted at the masses". 

===Box office===
 
Upon release, the film had a good opening weekend of Rs. 410&nbsp;million in India.  This gave Akshay Kumar his biggest opening till date, beating Singh Is Kinng.  Housefulls opening weekend was the second-highest opening weekend collection of all time, behind 3 Idiots.  By the end of the first week, the film collected Rs 498&nbsp;million, the third-highest first week collection – behind 3 Idiots and Ghajini. It also broke the 2010 record of biggest first week, previously held by My Name Is Khan.  Housefull debuted in the United Kingdom at number two and collected Rs 18&nbsp;million on 55 screens in its opening weekend, while in the United States, the film collected Rs. 28&nbsp;million on 82 screens in its opening weekend.   After six weeks, the film netted Rs. 1.81&nbsp;billion.   The film was declared hit by Box Office India. 
Houseful 2010 total box office grossing crossed 1&nbsp;billion mark

==Home media==
Housefull was released in Blu-ray Disc|Blu-ray and DVD in October 2010.    The films DVD went on to become the first Bollywood film to release on a 3-Disc edition, as the first disc included the film, the second disc included the video commentary by Sajid Khan, Ritesh Deshmukh, Lara Dutta, Boman Irani and Chunkey Pandey, while the third disc included a documentary. 

==Awards and nominations==
;6th Apsara Film & Television Producers Guild Awards
*Nominated –  Apsara Award for Best Performance in a Comic Role – Riteish Deshmukh 
 2011 Zee Cine Awards
Nominated  Best Director – Sajid Khan Best Actor – Akshay Kumar Best Music – Shankar Ehsaan Loy Best Supporting Actress – Lara Dutta Best Comedian – Riteish Deshmukh
*Zee Cine Award for Best Track of the Year – "Aapka Kya Hoga (Dhanno)"

;IIFA Awards
Won Best Performance in a Comic Role – Riteish Deshmukh

;Stardust Awards
Won Star of the Year – Male – Akshay Kumar Best Supporting Actor – Arjun Rampal

==Controversies== Tamil film, Kaathala Kaathala starring Kamal Hassan, Prabhudeva, Rambha (actress)|Rambha, and Soundarya — filed a complaint with the Tamil Film Producers Council alleging that producer Sajid Nadiadwala and director Sajid Khan had remade his film into Hindi without proper permission. 

In his complaint, Thenappan said, "I was shocked when I watched the Hindi film as nearly 60 per cent of the scenes from Kadhala Kadhala are there." He also stated that he was in talks with Nadiadwala about selling the remake rights more than a year ago and the deal did not go through.    

The film also faced controversy due to the remixed version of the song "Apni Toh Jaise Taise," originally from the 1981 film Laawaris directed by Prakash Mehra, music by Kalyanji Anandji. The Calcutta High Court restrained producer Sajid Nadiadwala from the cinematic use of the song. 

==Soundtrack== Sameer and Amitabh Bhattacharya.    The song "Aapka Kya Hoga" was a remake of the song of the same name from the 1981 movie Lawaaris.

{{Infobox album 
| Name = Housefull
| Type = soundtrack
| Artist = Shankar-Ehsaan-Loy
| Cover =
| Released =  
| Recorded =
| Genre = Film soundtrack
| Length = 30:32
| Label = T-Series
| Producer = Shankar-Ehsaan-Loy
| Last album = Hum Tum Aur Ghost (2010)
| This album = Housefull (2010)
| Next album = Tere Bin Laden (2010)
}}

===Tracklist===
{{Track listing
| extra_column = Performer(s)
| title1 = Oh Girl Youre Mine
| extra1 = Loy Mendonsa, Tarun Sagar, Noman Jalil
| length1 = 3:56
| title2 = Papa Jaag Jaayega
| extra2 = Neeraj Shridhar, Amitabh Bhattacharya, Ritu Pathak
| length2 = 3:17
| title3 = I Dont Know What To Do
| extra3 = Shabbir Kumar, Sunidhi Chauhan
| length3 = 3:20
| title4 = Aapka Kya Hoga (Dhanno)
| extra4 = Mika Singh, Sunidhi Chauhan, Shankar Mahadevan, Noman Jalil, Arun Ingle
| length4 = 5:08
| title5 = Loser
| extra5 = Amitabh Bhattacharya, Sachin Pandit and Vivienne Pocha
| length5 = 3:17
| title6 = Oh Girl Youre Mine
| note6 = O Boy What A Girl Mix
| extra6 = Loy Mendonsa
| length6 = 3:39
| title7 = Papa Jaag Jaayega
| note7 = Insane Insomaniac Mix
| extra7 = Neeraj Shridhar, Amitabh Bhattacharya
| length7 = 3:44
| title8 = I Dont Know What To Do
| note8 = Shabbirs Sexy 70s Mix
| extra8 = Shabbir Kumar, Sunidhi Chauhan
| length8 = 4:03
}}

===Reception===
 
The soundtrack met with positive reviews. Abid of Glamsham said  "Summing up, the album is another fun ride as we encounter one chartbuster after another, and hats off to the trio Shankar-Ehsaan-Loy, who have once again shown that they have the potential to compose as per the demand of the makers and the script" and gave the album 4 stars. 

Samir Dave of Planet Bollywood gave 7.5 stars out of 10, stating "SEL deliver a fun n frothy soundtrack that serves up a whole heaping spoonful of happiness."  Joginder Tuteja of Bollywood Hungama gave the album 3.5 stars and stated "Housefull is a fun album and never takes itself too seriously to turn into a landmark affair that would be remembered for years to come. The songs are meant to be entertaining enough to fit in well with the films narrative and not allow any full moment whatsoever to come in."    Rudhir Barman of Sampurn Wire awarded the album 3.5 out of possible 5 stars saying "The fast paced journey that everything is taking currently, it’s the survival of what works today. In that aspect, Housefull works and ensures that there won’t be any dull moment once the songs play on screen." 

==Sequel==
See article; Housefull 2
 John Abraham, Asin Thottumkal, Jacqueline Fernandez, Zarine Khan, Shreyas Talpade and Shazahn Padamsee. It also stars veteran actors such as Rishi Kapoor, Mithun Chakraborty and Johnny Lever in supporting roles. Furthermore, the film was titled Housefull 2 and released on 6 April 2012 getting mixed response from critics. The sequel was more successful than the prequel and was declared a Blockbuster at the box office, while some had to say that Housefull had much more sense.

==See also==
*Housefull 2
*Housefull 3

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 