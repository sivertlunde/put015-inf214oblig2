Death Drums Along the River
{{Infobox film
  | name           = Death Drums Along the River
  | image          = Death Drums Along the River DVD cover.jpg
  | image_size     = 190px
  | caption        = DVD cover
  | director       = Lawrence Huntington
  | producer       = Harry Alan Towers
  | writer         = Harry Alan Towers Lawrence Huntington Nicolas Roeg
  | narrator       = 
  | starring       = Richard Todd Marianne Koch Albert Lieven Walter Rilla
  | music          = Sidney Torch
  | cinematography = Robert Huke
  | editing        = 
 | studio = Hallam Productions Constantin Film
  | distributor    = Constantin Film   Planet Films  
  | released       =  
  | runtime        = 83 minutes
  | country        = United Kingdom West Germany South Africa
  | language       = English
  | budget         = 
  | gross          = 
}}
 1935 film based on the novel, but placed in a totally different story. Filmed on location in South Africa, it features Richard Todd and Marianne Koch leading a cast of British, German and South African actors.

==Plot==
In an unnamed small British colony on the west coast of Africa, somewhat resembling The Gambia, two policemen patrolling a wharf sight a sack of peanuts dropped by stevedores. As the sack breaks the workers discover a pouch in it that is quickly grabbed by a man who then runs away. The policemen chase him and he eventually kills one of them before disappearing. Police Commissioner Sanders (Todd) questions Pearson (Bill Brewer), a suspected criminal, but finds no information.
 American journalist (Robert Arden) who wishes to visit the clinic to do a story, and also turns out to be an old friend of Pearson.

Commissioner Sanders begins to suspect the clinic as a location for smuggled diamonds from across the border.

==Main cast==
* Richard Todd as Commissioner Harry Sanders
* Marianne Koch as Dr. Inge Jung
* Albert Lieven as Dr. Weiss
* Walter Rilla as Dr. Schneider
* Jeremy Lloyd as Hamilton
* Robert Arden as Hunter
* Vivi Bach as Marlene
* Bill Brewer as Pearson
* Simon Sabela as Bosambo

==Sequel==
Todd would reprise the role of Harry Sanders in the follow-up film, Coast of Skeletons (1965).

==Production== West German financing from Constantin Film and a cast of West German actors.   The film has very little to do with Edgar Wallaces Sanders of the River except the name of Sanders and the boat Zaire (which is now a small motorboat rather than a steamer). The film was marketed in Germany as one of the then popular Edgar Wallace series of films.  
 The Hellions and was attempting to produce a film of Ian Flemings The Diamond Smugglers. 
 1935 British film has been replaced by a standard detective story involving murder and diamond smuggling. The fictional colony is not given a name, but in the spirit of the times Dr Jung asks Sanders what hell do when it is granted independence.  Sanders replies hell stay on "if theyll have me".

==Locations== Zululand and Lake St. Lucia. 

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 