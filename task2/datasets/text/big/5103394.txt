Kanoon
 
 
 
{{multiple issues|
 
 
}}

 
{{Infobox film
| name           = Kanoon
| image          = Kanoon film poster.jpg
| caption        = Film poster
| director       = B. R. Chopra
| producer       = Anano B. R. Chopra Sarup Singh
| eproducer      =
| aproducer      =
| writer         = Akhtar ul Iman|Akhtar-Ul-Iman C. J. Pavri Nanda
| music          = Salil Choudhury
| cinematography = M. N. Malhotra
| editing        = Pran Mehra Ramlal Krishnan Sachdeva
| distributor    =
| released       = 1960
| runtime        = 150 min.
| country        = India
| awards         =
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Kanoon ( : قانون ; Title translation: The Law) is a 1960 Indian Hindi film directed by Baldev Raj Chopra|B.R. Chopra. The film stars Rajendra Kumar, Nanda (actress)|Nanda, Ashok Kumar, Mehmood Ali|Mehmood, Shashikala, Jeevan and Om Prakash. The film presents a case against capital punishment, arguing that witnesses may be genuinely deceived, and their consequent inadvertently tendered false testimony may lead someone wrongly to the gallows.

The film was a courtroom drama of a murder case, where the judges prospective son-in-law (Rajendra Kumar) is the defence lawyer in a case of murder for which he suspects his would-be father-in-law. The film was Indias second songless talkie.  The first one was Andha Nal, a Tamil movie.

==Plot synopsis==
Kalidas (Jeevan) is presented before court for the murder of Ganpat. He pleads guilty, but claims that the court can do him no harm as he has already served a sentence for the murder of the same man. An emotionally overcharged Kalidas asks judge Badri Prasad (Ashok Kumar) what gives law the right to deprive an innocent man of something it cannot return him, before collapsing and dying.

The shocking incident makes its way to the press and becomes a matter of hot debate in the city. Two judges, Mr. Jha and Mr. Savalkar (Iftekhar in a guest appearance), discuss the case. Badri Prasad, who is well known for never having awarded a death sentence, has a friendly argument with Jha, which leads to a wager that it is possible for someone to get away scot-free with murder.

In the meanwhile, romance is blossoming between Badri Prasads daughter Meena (Nanda (actress)|Nanda) and advocate Kailash Khanna (Rajendra Kumar), one of the rising stars in the legal fraternity and Badri Prasads protégé. The young couples visit to a ballet is rather unremarkable, except for the surreptitious appearance of Ashok Kumar, who is seen by the viewer romancing an unknown lady (Shashikala) in a private box. Incidentally the murdered man Ganpat is her husband. She married a rich man during her first husbands (Ganpat) lifetime, and inherited his property. This was an illegal marriage, and Dhaniram was blackmailing her, as he was privy to this information.

Badri Prasads son Vijay (Mehmood Ali|Mehmood) is heavily indebted to a local money lender Dhaniram (Om Prakash) who, having obtained the formers signature on a blank piece of paper, threatens to have his entire property confiscated. Afraid to face his stern father with the truth, Vijay pleads with Kailash to intercede with the money lender. The later agrees to do so, despite initial reluctance.

Kailash drops in at the money lenders place to have a word with him. Their exchange is interrupted by the arrival of Badri Prasads look-alike. Kailash does not want to be seen with Dhaniram, so he hides in a sideroom, instructing Dhaniram not to disclose to the judge that he had come there.

Dhaniram receives the unexpected guest through an already open door. Kailash watches in horror from the inner room, as the unscheduled visitor (Ashok Kumar) stabs his host to death. Unsure what to do, Kailash walks away. Unfortunately, a petty thief Kaalia (Nana Palsikar), who comes in with the intention of burglary, is apprehended at the scene of the crime and presented in Badri Prasads court. He is shown being caught by Sub-Inspector Das (Jagdish Raj), and Hawildar Ram Singh. Kaalias hands are completely drenched in blood.

Torn between his loyalty to his mentor and future father-in-law, on the one hand, and his moral duty to save an innocent man, on the other, Kailash resolves to defend the accused, while at the same time, avoiding bringing out in public the painful truth. What ensues is an absorbing psychological thriller with an unexpected end.

==Cast==
* Rajendra Kumar  as  Advocate Kailash Khanna
* Ashok Kumar  as  Judge Badri Prasad Nanda  as  Meena Prasad, Badri Prasads daughter Mehmood  as  Vijay Prasad, Badri Prasads son
* Manmohan Krishna  as  Prosecuting Attorney
* Jagdish Raj  as  Sub-Inspector Das Jeevan  as  Kalidas
* Nana Palsikar  as  Kaalia
* Iftekhar  as  Judge Savalkar
* Om Prakash  as  Dhaniram
* Mool Chand  as  Advocate
* Nazir Kashmiri
* Shashikala
* Shubha Khote
* Balwant Singh
* Devan Sharad
* Kailash Sharma
* Ravi Kant

==Murder and the Forensics==
There are interesting forensic details to be noted in the murder. The Badri Prasad look-alike who commits the murder comes from the main door, stabs Dhaniram in the stomach, switches off the light and goes back quietly through the same door.

Much later, Kaalia a petty thief, ascends to the first floor residence of Dhaniram, through a pipe and then through the open window. The lights are off. His steps fall on spilt milk. The milk got spilt by a black cat which jumps on the glass of milk, (after the murder, when Kailash Khanna comes out from the adjoining room and is trying to contemplate what happened and why, a cat is shown running around. In fact this alerts Kailash to run away from the scene himself).

Public Prosecutor Kailash Khanna enters Dhanirams house exactly at 11.00&nbsp;pm on 30 June. This is proved by two points (i) When Meena asks Kailash to go to Dhanirams house to collect the incriminating paper from him, Kailash informs her that he had to attend Bar Associations dinner that day. So he would not be free before 10.30 or perhaps 11.00&nbsp;pm. Later when he is shown walking towards Dhanirams house, a clock in the background can be heard chiming. One can easily count 11 chimes from the moment Kailash is walking towards Dhanirams house, till Dhaniram opens the door (chiming stops after that). So the action starts at 11.00&nbsp;pm.

The police surgeons (Forensic experts) report says that Dhaniram died between 11.30&nbsp;pm and 12.00 midnight (on the night of 30th).

Kaalia is caught descending the pipe hurriedly at 00.08&nbsp;am on 1 July.

==Weaknesses in Prosecution reasoning==
* The prosecuting attorney (Manmohan Krishna) asserts in the beginning of their case, that Dhaniram was sleeping (because the lights of his house were found off). However the main door of the house was open (Badrinaths lookalike had come and gone through that, and Dhanirams corpse could not have possibly closed the door back). The question arose, why the door was open at that untimely hour (12 midnight). The prosecution then changes the story and says that Dhaniram was awake at that time, and that explained the open main door. But he was trapped by Kailash immediately by indicating that if Dhaniram was indeed awake, the lights should have been on! Indeed there were two contrasting things at the scene of crime – lights were off, indicating the owner of the house was sleeping and the main door was open, indicating the owner of the house was awake. In fact, the door was opened (and never closed after that) by Dhaniram when Kailash Khanna went to his house to take back the blank signed paper written by Vijay (Mehmood Ali|Mehmood). At the time Kailash Khanna knocks at his house, Dhaniram is shown preparing himself to drink a glass of milk (he never gets to drink that milk, and it is the milk in that glass, which gets spilt later, perhaps by a rat, on which Kaalias footprints are later imprinted). He opens the door, and feeling ingratiated at having such an honourable guest, offers a glass of sherbet to Kailash. During their talks, Kailash peeps out of window, and sees Badrinath (in fact Badrinaths lookalike) coming towards the door. Kailash does not want to be seen with Dhaniram, so he hides in a sideroom. Badrinaths lookalike is received by Dhaniram, through an already open door (he had never closed the door after Kailash had entered). Without saying anything, the newcomer stabs him in the abdomen quietly, switches off the lights and goes away, without closing the door behind him. Kailash sees all this through the side room. Later he also escapes from the house, without closing the door. This leaves the crime scene with two contrasting situations – a switched off light and an open door. This contrasting situation was never explained satisfactorily by the prosecution. The best it could say was that both door and lights were open when Kaalia came in through the pipe. Kaalia murdered Dhaniram and then switched off the lights. But Kailash quite rightly points out, that if Kaalia had indeed switched off the lights, the switch board should be smeared with blood (as Kaalias hands were full of blood when he was caught). No such blood was found on the switchboard. This allowed Kailash to make a deep dent in the story of the prosecution. The persons attending the court are shown admiring Kailashs reasoning.

==Weaknesses in Defence reasoning==
The defence represented by Kailash Khanna too has several fatal weaknesses, although these were never brought to the notice of the court by the prosecution side. Here are some:

* Kaalia is shown being caught by Subinspector Das at 11.55 pm. The time is noted by Das by his own wrist watch. In India, generally a murder case is committed to trial months or even years after the murder. During trial, Kailash proves that Dass watch is slower by 13 minutes (an unlucky number). He asks Das the time. Das consults the very same wrist watch he was wearing at the time of murder and tells the court that the time is 3.55 pm. At that very same time, the courts watch is showing 4.08 pm, proving that Dass watch was 13 minutes slower. By this fact, Kailash proves that Kaalia was caught at 12.08 am (on 1 July) rather than on 11.55 pm on 30 June thus conveniently bringing him out of suspicion. It is incomprehensible that the subinspector kept his watch slower by the same amount over the months it took the case to come to trial.

* Kailash reasons that the Police surgeon mentioned in his postmortem report that the murder took place between 11.30 and 12.00. This quite possibly includes 12.00 midnight also. And if this time is taken as the true time of murder, Kaalias arrest at 12.08 can be easily explained. But Kailash takes the ploy of taking an arithmetic mean of the two extremes of time mentioned in the report and assumes (without objection from the prosecution), that the murder took place at 11.45&nbsp;pm (on 30 June). He then goes on to reason that Kaalia should have been with the corpse for a full 23 minutes (he was caught at 00.08&nbsp;am on 1 July) with his hands smeared in blood. This is obviously unlikely. And thus Kaalia cannot be the murderer. Strangely this defective reasoning is never challenged by the prosecution.

* The prosecution story is that Kaalia went through the pipe to Dhanirams house (through an open window) and tried to steal something. Dhaniram who was sleeping at that time woke up, tried to apprehend Kaalia. During the struggle milk got spilt (in fact it was already spilt when Kaalia came in). Kaalia stabbed Dhaniram and rushed back to the window to get down, and in the process his foot prints were imprinted on the spilt milk. Kailash could have easily punched holes in this story, by pointing out that the footprints were directed inwards (towards the centr of the room where Dhanirams corpse was lying). According to the prosecution story, the footprints should have been directed outwards (towards the window, from where he eventually came out). This interesting fact was never brought to the courts notice. Furthermore, when Kaalia came in, he was tiptoeing (to avoid anyone listening to his footsteps). This led to two successive footprints being very near. When a person is running (as the case should have been when Kaalia ran out of the room after supposedly murdering Dhaniram), the two successive footprints should be much farther. This fact was also never brought to the notice of the court.

==Trivia==
* The year of Dhanirams (Om Prakash) death is not mentioned in the film, but it can be derived from the contents of the film. He may have been murdered on the night of 30 June 1959. This is because on the next day of the murder, when Badri Prasads daughter Meena (Nanda (actress)|Nanda), is serving breakfast to her father, the radio announces in Hindi in the background – "Aaj Budhwar hai aur July ki pehli Taarikh"  . The film was made in 1960. Before this year, 1959 is the nearest year, where Wednesday falls on 1 July.

* The wager between Badri Prasad (Ashok Kumar) and Justice Jha, that it is possible to murder someone without being sentenced, occurs only 2 days earlier – on 28 June. When Kailash Khanna towards the end of the film presents Badri Prasads diary to the court, he mentions this date.

==Awards and Nominations==
 
|- 1960
| B. R. Chopra   
| National Film Award for Best Feature Film in Hindi Certificate of Merit
|  
|- 9th Filmfare 1962
| B. R. Chopra 
| Filmfare Award for Best Director
|  
|-
| Nana Palsikar
| Filmfare Best Supporting Actor Award
|  
|-
| C.J. Pavri
| Filmfare Award for Best Story
|  
|-
| B. R. Chopra (for B. R Films)
| Filmfare Award for Best Film
|  
|}

==References==
 

==External links==
*  
 
 

 
 
 
 
 