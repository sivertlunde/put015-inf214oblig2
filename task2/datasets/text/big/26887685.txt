Annigoni: Portrait of an Artist
{{Infobox film
| name           = Annigoni: Portrait of an Artist
| image          = Annigoni_Portrait_of_an_Artist.jpg 
| caption        = Film Poster
| director       = Stephen Smith
| producer       = {{Plainlist|
*Richard Bond
*Stephen Smith
*Duilio Ringressi
*Amy C. Barnes}}
| writer         = Stephen Smith
| music          = Stefano Burbi
| cinematography = Duilio Ringressi
| cast           = {{Plainlist|
*Valerio Orlandini
*Angelo Alberi
*Benedetto Annigoni
*Giovanni Arcidiacono
*Rossella Annigoni
*Silvestro Pistolesi
*Lorenzo Fatichi
*Antonio Ciccone
*Alvaro Tortorelli
*Michael John Angel
*Nando Bernardini
*Alfredo Cifariello
*Romano Stefanelli
*Paolo Galli
*Lamberto Ringressi
*Palmiro Meacci
*Bruna Nardi
*Ezechiele Galiasso
*Lucio Nugnes
*Rabhi Karim}}
| released       =  
| runtime        = 96 minutes
| country        = Italy, Canada
| language       = English, Italian English subtitles
}}

Annigoni: Portrait of an Artist is a 1995 documentary film about the life and times of Italian portrait painter Pietro Annigoni.

==Production==
Principal photography began in October 1990 in Florence, Italy, and lasted four years. Annigoni: Portrait of an Artist had its World Premiere on October 23rd 1995, at the historic and largest movie theatre in Toronto, The Bloor Cinema. It played to sold out audiences during its entire week long run. It was followed by the Italian premiere in Florence, Italy. The Television Broadcast Premiere was on BRAVO! on March 3rd 1996.     

The film begins with a panoramic shot of dark rain clouds gathering over the city of Florence. The voice over of Pietro Annigoni strains from beyond, "I signed my work using the cipher C followed by three crosses representing the via cruicis, the hard road to the cross which the artist must travel." The story that follows is based on Annigonis Diary|diaries, documenting his work, his travels, his loves but mostly his thoughts on life and art. 

== Major film locations ==
* Convent of San Marco, Firenze
* Palazzo Venerosi-Pesciolini, Firenze Santo Spirito, Firenze
* Chiesa di San Lorenzo, Firenze
* Florence Academy of Art, Firenze
* Cattedrale di Santa Maria del Fiore, Firenze
* Convento del Bosco ai Frati, San Piero a Sieve Padova
* Chiesa di San Martino, Castagno dAndrea Italia
* Mount Falterona, Castagno dAndrea Italia.
* La Stazione di Sante Maria Novella, Firenze
* Ghislieri College, Pavia
* Montecatini, Italia
* Rainbow Studios Firenze, Italia
* Toronto, Ontario, Canada

==Original music soundtrack==
Original music from Annigoni: Portrait of an Artist, performed in concert from Florence on YouTube. 
 musical score for the film. His orchestra, "The Orchestra da Camera Nova Harmonia di Firenze" was created for the film and now continues as Mr. Burbis orchestra. A sample of the soundtrack of Stefano Burbi: Suite dalle musiche per il film "Annigoni: Portrait of an Artist" soundtrack can be found below in external links.

Burbi describes his inspiration for Annigoni as, "music of memory and nostalgia, it is melancholic because we remember, like Annigoni, a paradise lost".

==Awards and honors==
* Best Musical Score - 1996 Hot Docs International Film Festival 
* Award of Merit - Arts & Humanities - 1996 Chicago International Film Festival 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 