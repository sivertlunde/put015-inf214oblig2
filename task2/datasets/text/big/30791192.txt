Aparichita
{{Infobox film
| name = Aparichita, ಅಪರಿಚಿತ
| image = Scene_from_Aparichita.jpg
| caption = A scene from the movie Aparichita starring Suresh Helblikar and Shobha Kashinath
| producer = Satyanarayana, Kashinath, Dattaatreya, Dodayya
| released = 26th May 1978
| runtime =
| language = Kannada L Vaidyanathan
| lyrics = Ramdas Naidu
| cinematography = B C Gowrishankar
| studio = Shree Gayathri Arts Vasudeva Rao
}}

Aparichita ( ) is a 1978 Kannada, suspense oriented, thriller movie directed by Kashinath (actor)|Kashinath, starring Suresh Heblikar, Sobha and M. V. Vasudeva Rao. It is a directorial debut movie of Kashinath (actor)|Kashinath. The music was composed by noted composer L. Vaidyanathan. The movie was critically acclaimed.

==Soundtrack==
{| class="wikitable"
|-
! Sl.No
! Song
! Lyricist
! Artist
|-
| 1
| Ee Naada Anda
| P R Ramdas Naidu 
| S P Balasubramanyam
|-
| 2
| Savi Nenapugalu Beku
| P R Ramdas Naidu 
| Vani Jayaram
|-
|}

==Awards==
Karnataka State Film Awards Kashinath
* Best Child actor - Master Prakash

==External links==
* 
* 

 
 
 
 
 
 


 