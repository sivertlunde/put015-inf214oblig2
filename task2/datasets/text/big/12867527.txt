Sivi
{{Infobox film
| name           = Sivi
| image          = Sivi_VCD_cover.jpg
| caption        = VCD cover
| director       = K. R. Senthil Nathan
| Art Director   = B.Sai Kumar
| producer       = K. Sundar Yogi Jayashree Rao Anuja Iyer
| writer         = K. R. Senthil Nathan Dharan
| cinematography = P. S. Sanjay
| editing        = Sasikumar
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
}} Tamil horror film directed by K. R. Senthil Nathan and starring Yogi Srinivasan|Yogi, Jayashri Rao and Anuja Iyer,  directed by K. R. Senthil Nathan. A remake of the 2004 Cinema of Thailand|Thai-thriller, Shutter (2004 film)|Shutter, Sivi opened in September 2007 to positive reviews. 

==Plot==
It begins when Krishna (Yogi Srinivasan|Yogi) a young photographer and his girlfriend Sona (Jayashri Rao) accidentally run down a young woman on their drive home after a nights party on a deserted stretch of East Coast Road, near Chennai. They decide to leave the dead body and drive away. Later Krishna discovers something strange when he finds a mysterious shadow that appears on the pictures he takes with his camera. Ever since the accident, Krishna has been experiencing shoulder and neck pains.

He thinks thats just a bad picture, but then he realizes that there is something much more sinister behind the shadow on the picture frame and the extremely unsettling dreams. Unable to cope, they start investigating the phenomenon of the ghost appearing on the photographs, which leads to a discovery about Krishnas past, and a possible clue to the identity of their ghostly nemesis. As Sona goes to the college and starts taking pictures of the college and the library, Sona had found the girl that was hit on the road and was in the pictures, a young shy girl named Nandhini (Anuja Iyer|Anuja). As Krishnas friends have also committed suicide, by jumping off the buildings.

Flashback reveals that Krishna had once dated Nandhini, as the relationship ended with Krishna dumping her and Nandhini cutting herself and. As she continues to haunt Krishna and Sona, they go and visit her mother, where it is revealed that there is a coffin near the shed with Nandhinis body inside in her mothers house and revealed that she committed suicide and her mother could not bear to cremate the body. Krishna is haunted by the girl, and ends up been thrown off a fire escape. Sona releases that, Krishnas friends had raped Nandhini and revealed the Krishna had taken the photos of the rape, so Sona leaves him. Still haunted by  Nandhini, Krishna begins to take pictures around the apartment to find the Nandhini, as he throws the camera, the Polaroid takes by itself, as its revealed that the mysterious neck pains were all from Nandhini sitting on his shoulders, as Krishna is thrown off the apartment. Sona visits Krishna, as door swings the reflection shows the Nandhini still sitting on his shoulders.

==Cast== Yogi as Krishna
*Jayashree Rao as Sona
*Anuja Iyer as Nandhini
*Funky Shankar as Sara
*Uma Padmanabhan Chitti Babu Aarthi
*Benjamin

==Production==
In February 2007, director Senthilnathan, who had apprenticed under S. J. Surya, announced that he would make a horror film titled Sivi and would be in charge of the screenplay and dialogues, apart from direction. It was a remake of the 2004 Cinema of Thailand|Thai-thriller, Shutter (2004 film)|Shutter. Yogi Srinivasan|Yogi, the grandson of late Thengai Srinivasan, was selected to play his second lead role after Azhagiya Asura while it was revealed that newcomers Jayashree Rao and Anuja Iyer would also play pivotal parts. Dharan (music director)|Dharan, who shot to fame with Parijatham, was announced as music director for the film. 

==Release==
The film received generally positive reviews, with The Hindu stating that the "film scares in parts" and that "Yogi has performed his role convincingly, while Jayasri Rao needs to improve. Anuja Iyer as the ghost has done a good job".  Indiaglitz.com labelled the film as a "welcome relief", noting that "the movie is technically rich and the star cast give their best on screen makes it work. But giving the movie its true color is Dharans top-class background score. His good re-recording helps sustain the momentum of the movie and wraps audience to their seats."  Behindwoods.com stated "the film has a bright chance of becoming a commercial success", mentioning that "debutant director Senthil Nathan has diligently worked hard and the film stands a testimony to his efforts. A bright future is in store for this talented director. Seamless and taut editing by Sasikumar spices up the terror quotient of the film."  Reviewers from Sify.com cited that "Yogi as Krishna is one of the most impressive actors, one has seen in recent times. The director has been able to give an eerie look and feel to the film",  while Rediff.com said "it is the climax though, that packs a punch, and worth all the minutes of waiting, watching and wondering. An end that fits perfectly with the tone of the story, incidentally tying up with the title of the movie as well. A pat on the back to the editor for some slick work." 
 Unnaipol Oruvan, while music director Dharan (music director)|Dharans career has been staggered.

==Soundtrack==
{{Infobox album|  
  Name        = Sivi
|  Type        = Soundtrack Dharan
|  Cover       =
|  Released    = 29 July 2007
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = Parijatham (2006)
|  This album  = Sivi (2007)
|  Next album  = Laadam (2009)
}}
The soundtrack of the film was composed by Dharan (music director)|Dharan. Union Minister G. K. Vasan released the audio cassette of Sivi at a function held in Chennai on 29 July 2007 with attendees including directors Ameer Sultan|Ameer, Subramaniam Siva and Tamil Film Producers Council president Rama Narayanan.  The soundtrack became critically acclaimed with a reviewer noting that Dharan "has oodles of talent, and he definitely knows the pulse of todays generation".  La. Rajkumar wrote lyrics for two of the films songs, while Dr Burn wrote his own lyrics for his rap portions. 

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Theme Music
| extra1          = Subiksha
| length1         =
| title2          = O Nenje
| extra2          = Benny Dayal, Dr. Burn, Dharan (music director)|Dharan, Swetha
| length2         = 
| title3          = Maayavi Neeya
| extra3          = Krish (singer)|Krish, Haricharan, Shruthi
| length3         = 
| title4          = Rap Theme
| extra4          = Bob, Ranjith (singer)|Ranjith, Subiksha
| length4         = 
| title5          = O Nenje (2)
| extra5          = Benny Dayal, Dr. Burn, Dharan (music director)|Dharan, Swetha
| length5         = 
| title6          = Neruppum
| extra6          = Ranjith (singer)|Ranjith, Sunitha Sarathy
| length6         = 
}}

==Remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" Shutter (2004) Shutter (2008) Click (2010) (Hindi cinema|Hindi)
|-
| Ananda Everingham || Yogi Srinivasan || Joshua Jackson || Shreyas Talpade
|-
| Natthaweeranuch Thongmee || Anuja Iyer || Rachael Taylor || Sadha
|-
|}

==References==
 

 
 
 
 
 
 