Saradaga Kasepu
{{Infobox film
| name = Saradaga Kasepu
| image = Saradaga Kasepu.jpg
| caption = Theatrical Release Poster
| writer = Sankaramanchi (story) Padala Sivasubramanyam (Dialogues)
| starring = Allari Naresh Madhurima Srinivas Avasarala Ahuti Prasad
| director = Vamsy
| cinematography = Loki
| producer = M. L. Padma Kumar Chowdary
| editing = Basva Paidireddy
| distributor = Sri Keerthi Creations
| country = India
| released = 17 September 2010
| runtime =
| language = Telugu Chakri
| awards =
| budget = 
| gross = 
}}
 Telugu film starring Allari Naresh, Madhurima and Srinivas Avasarala in the Lead roles directed by Vamsy. The Movie was released on September 17, 2010.This film has shades of Malayalam movie "Mazha Peyyunnu Maddalam Kottunnu" directed by Priyadarshan.

==Plot==
Ranga Babu (Allari Naresh) is a driver whose childhood friend is Srinivas (Srinivas Avasarala). Srinivas is from a rich family and he lives in the US. The story takes a turn when Srinivas comes to India for getting married and his parents (Jeeva, Sana) want him to tie the knot to Raja Raos (Ahuti Prasad) daughter Manimala (Madhurima) who lives in a different place. However, Srinivas has a condition that he must know the girls character. For this, he switches places with Ranga Babu. As expected, misunderstandings and confusions arise. What happens from there forms the rest of the story.

==Cast==
* Allari Naresh as Ranga Babu
* Srinivas Avasarala as Srinivas
* Madhurima as Manimala
* Ahuti Prasad as Raja Rao Jeeva as Rangas dad

==References==
 

 

 
 
 


 