Fontamara (film)
{{Infobox film
| name           = Fontamara
| director       = Carlo Lizzani
| producer       = Piero Lazzari   Edmondo Ricci 
| story = Ignazio Silone (novel)
| writer         = Carlo Lizzani
| screenplay     = Carlo Lizzani   Lucio De Caro
| based on       =  
| starring       = Michele Placido   Imma Piro   Antonella Murgia   Ida Di Benedetto
| music          = Roberto De Simone
| cinematography = Mario Vulpiani
| editing        = Franco Fraticelli
| released       =  
| runtime        = 139 minutes
| country        = Italy
| language       = Italian
}} novel of the same name by Ignazio Silone.

It stars Michele Placido in the role of Berardo Viola and Antonella Murgia as Elvira.  
 Best supporting Actress in 1981 for her supporting role as Maria Rosa.

==Plot==
In 1929 in Abruzzo, in the mountain villages of Aielli and Fontamara, near Avezzano, the people are subjugated by the arrival of a team of fascists. The militiamen prove hostile, preventing citizens from obtaining supplies necessary to survive the imminent winter. The small Berardo is shocked by the rape of his mother by a fascist officer, and is forced to go to Rome, meditating revenge. Ten years later Berardo in fact, now that he has become large, goes back from Rome in Abruzzo, and discovers that the country is still victim of abuse of fascism. The young communist tries to attack squadrons, but is murdered.

==Cast==
*Michele Placido - Berardo Viola
*Ida Di Benedetto - Maria Rosa
*Imma Piro - Maria Grazia
*Antonella Murgia - Elvira

==External links==
* http://www.filmscoop.it/film_al_cinema/fontamara.asp

==References==
 

 
 

 
 
 
 
 


 