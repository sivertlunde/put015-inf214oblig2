Puffball (film)
{{Infobox film
| name=Puffball
| image= Puffball2007.jpg
| caption=
| screenplay = Dan Weldon
| based on = Puffball (novel)|Puffball by Fay Weldon
| starring=Kelly Reilly Miranda Richardson Rita Tushingham Donald Sutherland Leona Igoe Declan Reynolds
| director=Nicolas Roeg
| producer=Julie Baines Dan Weldon Ben Woolford
| distributor= Yume Pictures (2008) (UK)
| runtime=120 min. English
| movie_series_label=
| movie_series=
| music= 
| budget= £7,000,000 (estimated)
}} 2007 supernatural novel by Fay Weldon adapted by her son Dan Weldon. The film was partially funded through the UK Film Councils New Cinema Fund.

The film had its premiere at the 2007 Montreal Film Festival. The film was later released in Canada on October 28, 2007, and saw a limited release in the United States on 29 February 2008.

==Plot synopsis==

Liffey (Kelly Reilly), an ambitious young architect, moves to an isolated and eerie Irish valley to build a modern piece of architecture. The ruined cottage and land that she will use are a gift from her fiancé Richard, and is the former home of Molly, the elderly matriarch of a farming family living on the other side of the woods. Molly’s daughter is Mabs (Miranda Richardson), herself mother of three girls and wife to farmer Tucker. Now in her forties, Mabs wants another baby, a boy to inherit the farm - or perhaps she just wants to be pregnant again.

Mabs and Molly use black magic to give Mabs a baby boy. But Liffey becomes pregnant after sleeping with both Richard and Tucker, causing everybody to believe that her baby is Tuckers. Mabs seeks Mollys help again to kill Liffeys baby with magic, making it look like a miscarriage. Mabss elder daughter helps Liffey fight the spell. In the end, Mabs is finally pregnant with her own child, a boy.

==External links==
* 
* 
*   Official United Kingdom Distributors

 

 
 
 
 
 
 
 
 
 


 