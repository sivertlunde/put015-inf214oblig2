Relics: Einstein's Brain
 
{{Infobox Film |
name           = Einsteins Brain |
writer         = Kevin Hull |
starring       = Kenji Sugimoto |
director       = Kevin Hull |
producer       = BBC Films |
released       = 1994 |
runtime        = 65 min.| English |
}}
 Japanese professor Kenji Sugimoto in his search for Albert Einsteins brain. It is produced by BBC Films.

== Summary ==

This documentary is introduced by a set of titles informing the viewer that Albert Einsteins brain was extracted after his death in 1955 and that it was donated to the Princeton Medical Center. We then meet Kenji Sugimoto, professor of mathematics and science history at the Kinki University of Osaka. In broken English, he describes what Einstein means to him:

"Einstein teaches me about love as well as science. Passion, love and science. I love Albert Einstein."

Thus, he embarks on a pilgrimage to Princeton to find the legendary cerebrum. Once there, he learns that the brain has been misplaced, and the film documents his subsequent travels across the United States to recover it. The last person known to handle the item is Thomas Stoltz Harvey, a man that proves difficult to find. One lead sends Sugimoto to the Albert Einstein College of Medicine in New York, where a former associate of Harveys, Dr. Harry Zimmerman, informs the unlikely pilgrim that the man he is seeking in fact is dead.

Sugimoto next tracks down Einsteins granddaughter by adoption, Evelyn Einstein. She tells him she has reason to believe she actually is biologically related to Einstein, and has been in dialogue with an institute to compare her DNA to that of the late scientists brain. The brain sample used for this was sent from Harveys residence in Lawrence, Kansas, giving Sugimoto a possible lead to the brains current whereabouts.

Once in Kansas, it appears Zimmerman was misinformed; Thomas Harvey is still very much alive. Sugimoto finally finds the brain (which is stored in three jars in a closet), and even acquires a small sample to bring back to Japan. He celebrates by singing karaoke in a local bar, and closes the documentary with a few more contemplations around his idol:

"I am born in Nagasaki two years after bomb. Einstein is made responsible for the bomb, but I do not blame him. I still love Albert Einstein."

== Themes == idol worship, catharsis and the dichotomy of scientific progress.

== The question of veracity == narrativization of material found in documentaries, very little actually indicates forgery.

Kai Michels article " Wo ist Einsteins Denkorgan?" ("Where is Einsteins Brain?"), published by Die Zeit in December 2004, shows just how easy it is to assume the film is a forgery. This article revolves around professor Michael Hagner of ETH Zürich, who after showing a group of students the film in question informs them that this is all fiction and that Kenji Sugimoto is a character (arts)|character. But after a phone call to a colleague he is informed that Sugimoto in fact is real, and that truth in fact is stranger than fiction. Or as Hagner himself puts it, "Nichts ist absurder als die Realität".

The documentary is lent further credibility by Michael Paternitis 2000 book Driving Mr. Albert: A Trip Across America With Einsteins Brain, where the author tells the story of how he chauffeured Dr. Harvey across the US to deliver the brain to Evelyn Einstein. His path crosses with several persons who appeared in Einsteins Brain, including director Kevin Hull and Evelyn Einstein, and at one point he even travels to Japan and meets Sugimoto, who proudly shows off his brain sample and invites him out to a night of karaoke. If the story of Sugimoto and Harvey is a hoax, it is an elaborate one.

==See also==
*Albert Einsteins brain

== External links ==
* 
*Kai Michel:  . Die Zeit, December 2012
* 

 
 
 
 
 
 