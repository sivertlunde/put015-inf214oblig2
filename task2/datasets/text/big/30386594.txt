Tsugaru Folk Song
{{Infobox film
| name           = Tsugaru Folk Song
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Kōichi Saitō (film director)|Kōichi Saitō
| producer       = 
| writer         = 
| screenplay     =
| story          =
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1973
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
  is a 1973 Japanese film directed by Kōichi Saitō (film director)|Kōichi Saitō. The story is about the search for the basis of Japanese national identity, and the escape of lovers to the wonderfulness of nature. {{cite book|title=Kodansha encyclopedia of Japan
|page=275|publisher=Kodansha|year=1982}} 

==Awards== Kinema Junpo Award
*Won: Best Film
*Won: Best Director for Kōichi Saitō (film director)|Kōichi Saitō Best Actress for Kyôko Enami

28th Mainichi Film Award  Best Film

==References==
 

==External links==
*  
*  at the University of California, Berkeley Art Museum & Pacific Film Archive

 
{{Navboxes title = Awards list =
 
 
}}

 
 
 
 


 