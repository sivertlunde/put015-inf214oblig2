Vijaypath
 
{{Infobox Film |
| name     = Vijaypath |
| writer         = Talat Rekhi (story) |
| image =  Vijaypath.jpg
| caption        = Dvd Cover
| starring       = Ajay Devgan,  Tabu (actress)|Tabu, Danny Denzongpa ,  Gulshan Grover,  Reema Lagoo |
| director       = Farouq Siddique |
| producer       = Dhirajlal Shah |
| Genre          = Action |
| released       = July 29, 1994 |
| distributor    = Time Magnetic Pvt. Ltd.
| language       = Hindi |
| music          = Anu Malik |
| awards         = Filmfare Best Debut Award
| budget         = |
|
}} Tabu and Danny Denzongpa
 Tabu after her death.

==Plot==
The family of Justice Saxena is extremely happy until his younger brother Inspector Rajesh Saxena (Suresh Oberoi) arrests a notorious gangster, Bhawani Singh (Anant Jog). Justice Saxena sentences Bhawani Singh to death.
Bhawani Singhs brother, Dilawar Singh (Danny Denzongpa), avenges his brothers death, and he kills the judge as well as his driver Shanker. Dilawar Singh  also tries to kill their sons. Driver Shankers son, Karan (Ajay Devgan) loses his eyesight while saving Justice Saxenas son Babloo.

Justice Saxenas wife, the two children and Rajesh Saxena leave the city. Dilawar Singh finds them and kills the Judges wife and throws Babloo out of the running train.

Before dying, Babloo donates his eyes to Karan. Karan vows that he will not remove his goggles till he confronts Dilawar Singh.

==Cast==
* Ajay Devgan: Karan
* Danny Denzongpa: Dilawar Singh
* Tabu (actress)|Tabu: Mohini Mona Face over Surgery
* Suresh Oberoi: Inspector Rajesh Saxena
* Reema Lagoo: Mrs. Saxena
* Gulshan Grover: Shakti B. Singh
* Vikas Anand: Judge Saxena
* Ram Mohan
* Aparajita: Mohinis mom
* Anant Jog: Bhawani Singh Salim
* Gurbachchan Singh
* Silk Smitha

==Soundtrack==
The soundtrack album of the movie composed by Anu Malik topped the charts in 1994. Apart from the side-splitting "Ruk Ruk Ruk Arey Baba" sung by Alisha Chinai, the mushy romantic number "Raah Mein Unse Mulaqaat" rendered by Kumar Sanu and Alka Yagnik can be heard playing on FM broadcasting radio and stereo even today. The other songs, too, chipped in to the popularity of the album.  The album brought off rank 537 in the "Best Albums of 1994" and was in the place 5214 in the "Best Albums of the 1990s". 

===Tracklist===
{| class="wikitable"
|-
! Title !! Singer(s) 
|-
| "Sagar Sang Kinare Hai"
| Kumar Sanu & Alka Yagnik
|-
|"Ruk Ruk Ruk Aare Baba Ruk"
| Alisha Chinai
|-
|"Raah Mein Unse Mulaqat"
| Kumar Sanu &  Alka Yagnik
|-
|"Aayiye Aapka Intezaar Thaa"
| Kumar Sanu & Sadhna Sargam
|-
|"Kal Saiya Ne Aisi Bowling Ki "
| Alisha Chinai
|-
|"Seene Mein Dil Hai"
| Kumar Sanu & Sadhna Sargam
|-
|"Ladke Aaj Ke Ladke"
| Anu Malik & Poornima (Sushma Shrestha)
|}

==Awards==
===Filmfare Awards===
;Won: Tabu

;Nominated:
* Filmfare Best Villain Award - Danny Denzongpa
* Filmfare Best Female Playback Award - Alisha Chinoy
* Filmfare Best Female Playback Award - Alka Yagnik

==References==
 

== External links ==
*  

 
 
 