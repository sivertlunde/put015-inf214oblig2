I Vicerè (film)
{{Infobox film
 | name = I Vicerè
 | image = I Vicerè (film).jpg
 | caption =
 | director = Roberto Faenza
 | writer = Federico De Roberto (novel) Roberto Faenza Francesco Bruni Andrea Porporati
 | starring = Lando Buzzanca Alessandro Preziosi Cristina Capotondi
 | music = Paolo Buonvino
 | cinematography =  Maurizio Calvesi 
 | editing =  Massimo Fiocchi
 | producer = Elda Ferri
 | distributor = 01 Distribution
 | released = 2007
 | runtime = 120 min 180 min (directors cut)
 | awards =
 | country = Italy
 | language =  Italian
 | budget =
 }}
 the novel with the same name written by Federico De Roberto. For his performance Lando Buzzanca won the Globo doro for best actor.  The film also won four David di Donatello awards    and two Silver Ribbons. 

== Plot ==
Uzeda, a Sicilian family faithful to the Bourbon kings, with the Unification of Italy in 1861 and the rise to power of Giuseppe Garibaldi and Vittorio Emanuele II, begins to lose his power, although Sicily remains in the hands of Prince Giacomo (Giacomo Uzeda).

== Cast ==
*Alessandro Preziosi: Consalvo
*Lando Buzzanca: Principe Giacomo
*Cristiana Capotondi: Teresa
*Guido Caprino: Giovannino
*Assumpta Serna: Duchessa Radalì
*Magdalena Grochowska: Donna Isabella
*Sebastiano Lo Monaco: Duca Gaspare 
*Lucia Bosè: Ferdinanda

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 