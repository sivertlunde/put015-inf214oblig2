Squareheads of the Round Table
{{Infobox Film |
  | name           = Squareheads of the Round Table
  | image          = SuqareheadsroundLOBBY48.jpg
  | caption        = 
  | director       = Edward Bernds
  | writer         = Edward Bernds Harold Brauer
  | cinematography = Allen G. Siegler 
  | editing        = Henry DeMond 
  | producer       = Hugh McCollum 
  | distributor    = Columbia Pictures 
  | released       =  
  | runtime        = 18 06" 
  | country        = United States
  | language       = English
}}

Squareheads of the Round Table is the 106th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are troubadours in medieval times. The villainous Black Prince has designs on marriage to Elaine, the princess. She however is in love with Cedric, the blacksmith. The Stooges try to intervene for Cedric by serenading Elaine; the music is the sextet from Donizettis opera "Lucia di Lammermoor". The lyrics serve as a secret conversation between Elaine and the foursome that she will leave her shade up when the "coast is clear" however, the group is spotted by the black Prince who calls for the guards. the group escapes but after the king is informed he orders the princess to stay in his room for the night (where shell be better guarded) and hell sleep in the princess room. The stooges unaware of the switch send Shemp to bring the princess however, he mistakes the king for her. After they fail to escape the king orders them imprisoned and to be executed the next morning. Elaine secretly sends them a "loaded" loaf of bread and the stooges use the tools to escape. This leads to a chase with the guards. While hiding, Moe overhears the Black Prince plotting with a co-conspirator to murder the king. The Stooges save the day by disguising themselves as knights and revealing the plot to Elaine, They distract the guards by to Stephen Foster’s "Old Folks at Home", thus allowing Elaine to free Cedric. Finally, the king realizes the plot and jails the Black Prince and his fellow plotter. Elaine is allowed to marry Cedric, and they all live happily ever after.

==Production notes== remade in Fiddlers Three and The Hot Scots, Squareheads of the Round Table was filmed on the existing set of the feature film The Bandit of Sherwood Forest. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion; Comedy III Productions, Inc., ISBN 0-9711868-0-4 

==Quotes==
*King: "My daughter marry a ‘smith?!"
*Shemp: "Take it easy, King; millions of women marry Smiths every year!"

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 

 