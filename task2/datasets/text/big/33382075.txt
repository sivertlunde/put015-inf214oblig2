Treevenge
{{Infobox film 
| name           = Treevenge
| image          = 
| alt            = 
| caption        = 
| director       = Jason Eisener
| producer       = Rob Cotterill
| writer         = Rob Cotterill Jason Eisener Glenn Matthews  Kristin Slaney
| music          = 
| cinematography = 
| editing        = 
| studio         = Yer Dead Productions
| distributor    = Yer Dead Productions
| released       =  
| runtime        = 16 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 Glenn Matthews, and Kristin Slaney. The theme song to the film Cannibal Holocaust is played during the opening title.

==Synopsis==
 sentient Christmas trees. After being hacked down and shipped to homes, they are subject to "humiliation" by humans, who decorated them and make them stand in their living rooms. The Christmas trees have had enough, and go on to massacre and kill an entire town as part of their uprising.

==Awards and nomination==
*Audience Award for Best Short Film -- New York City Horror Film Festival
*Best Local Film -- The Coast Magazine
*Audience Award for Best Short Film -- Toronto After Dark Film Festival
*Audience Award for Best Short Film -- Fantasia Film Festival
*Best Editing -- Atlantic Film Festival
*Best Short Film -- Fantastic Fest Online
*Audience Award for Best Short Film -- San Francisco Independent Film Festival
*Best Short Film -- Rue Morgue Magazine
*Honorable Mention -- Sundance Film Festival

==See also==
*Hobo with a Shotgun

==External links==
* 
*  at Yer Dead Production website
* 

 
 
 
 
 
 
 
 
 
 