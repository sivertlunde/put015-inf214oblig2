Sector 236 – Thor's Wrath
{{Infobox film
| name           = Sector 236 – Thors Wrath
| image          = 
| alt            =  
| caption        = 
| director       = Björne Hellquist Robert Pukitis
| producer       = Björn Ericson Lars Lundgren
| writer         = Björne Hellquist
| starring       = A.R. Hellquist Daniel Janzen Tina Ericksson Fredrik Dolk Lars Lundgren Jan Johansen Fredrik Blom
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 90 minutes
| country        = Sweden
| language       = Swedish
| budget         = 300.000 kr
| gross          = 
}}
 Planet of the Apes and Thriller - A Cruel Picture. 

==Plot==
A platoon of Swedish Mountain Rangers goes missing in the remote military sector 236 - nicknamed since the Viking age Thors Wrath.  The Swedish government sends captain Palmquist and a special force to investigate. Joining them is the American agent and scientist Johnson. Meanwhile a group of teenagers goes to hike in the area. Their phones suddenly lose reception and the compass stops working. Soon, they are killed off one by one by an unseen force. At the headquarters, the Swedish Colonel Stag realises that the American has lied about what he believes to hide in sector 236. He sets out to save his soldiers from walking into a bloodbath. 

==Cast==
*A.R. Hellquist - Captain Hellquist
*Daniel Janzen - John
*Tina Ericksson - Ylva
*Björn Ericson - Steven
*Rickard Green -Micheal
*Fredrik Dolk -  Col. Stag
*Lars Lundgren - Agent Johnson
*Natasha Semenets - Pia
*Tintin Anderzon - Karin Westin Jan Johansen - Agent Roth
*Viktoria Hedberg - Sofie
*Daniela Miteska - Lt. Luthman
*Åke Jansson - General Rickert
*Mikael Bergåse - Patrik
*Hippe Man - Johan Gunnarsson
*Emma Sundstedt - The Creature
*Magnus G. Bergström - Torsten Brinkel
*Magnus Dahl - Soldier 1
*Ulrika Forberg - Solder 2 Fredrik Blom - Soldier 3

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 