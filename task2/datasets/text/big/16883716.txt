The Last Debate
{{Infobox Film |
  name         = The Last Debate |
  image        = The Last Debate.jpg |
  caption      = |
  writer       = Jim Lehrer, Jon Maas |
  starring     = James Garner Peter Gallagher Audra McDonald Marco Sanchez|
  director     = John Badham |
  producer     = |
  studio       = Scott Free Productions | Showtime |
  released     = 2000 |
  runtime      = | English |
    budget       = |
}} 2000 political television film directed by John Badham, based on the book by journalist and writer Jim Lehrer, with a teleplay by Jon Mass,   and starring James Garner and Peter Gallagher.

==Synopsis==

A televised presidential debate has a conspiracy behind it.

==Cast==
 
* James Garner as Mike Howley
* Peter Gallagher as Tom Chapman
* Audra McDonald as Barbara Manning
* Donna Murphy as Joan Naylor
* Marco Sanchez as Henry Ramirez
* Dorian Harewood as Brad Lily
* Jack Turpin as Michael Riley
* Bruce Gray as Governor Paul L. Greene Stephen Young as Richard Meredith
* Lawrence Dane as Sidney Robert Mulvane
* Djanet Sears as Nancy Dewey
* Peter Donaldson as Jeff Grayson
* John Badham as Don Beard
* Leslie Carlson as Pat Tubbs
* Maggie Huculak as Gwyn Garrison
* Martin Doyle as Jim Weaver
* Brenda Robins as Carol Reynolds Colin Fox as Joshua L. Simpson
* Judah Katz as Bob Lucas Robin Ward as Mark Southeran
* Don Ritchie as Church Hammond
* Nicky Guadagni as Sam Minter
* Patrice Goodman as Roz Weisberg
* Barry Flatman as Ned Cannon
* Shelley Peterson as Joyce Meredith
* Annabelle Torsein as Alison Meredith
* Lynn Vogt as Ellen Greene
* Eamon Zekkon as TV Director Barbara Gordon as Lorraine Hampstead
* Nancy Harewood as Bonnie Kerr
* Ann Marin as Female Reporter
* Vanessa Vaughan as Grace Dickins Doug Murray as Jeff Field
* Martin Roach as Room Service Waiter
* Denis Akiyama as TV Makeup Artist
* Bryan Renfro as Steve Harrington
* Sharron Matthews as Barbara Fan
* Chuck Campbell as Bartender
 

==References==
 

==External links==
*  

 

 

 

 
 
 
 