Bubble Gum (film)
{{Infobox film
| name= Bubble Gum
| image= Bubble Gum (2011).jpg
| alt=
| caption= 
| director= Sanjivan Lal
| producer= Sushma Kaul Ravi
| writer= Sanjivan Lal Rohit Gahlowt
| starring=Sachin Khedekar Tanvi Azmi Sohail Lakhani Delzad Sanjay Hirale Apoorva Arora
| music= Bapi Tutul Haffu
| cinematography=Anshul Chobey
| editing= Suresh Pai
| sound= Manas Choudhury
| released=  
| country=India
| language=Hindi
}}
Bubble Gum  is a 2011 Hindi film directed by Sanjivan Lal. The film was released on 29 July 2011 and upon release it got critical appreciation.

The film was among the 5 films you shouldve watched in 2011  by Rajeev Masand. The film was also listed among the best films of 2011 by Hindustan Times.

==Cast==

*Sachin Khedekar - Mukund Rawat
*Tanvi Azmi - Sudha Rawat
* Sohail Lakhani - Vidur
* Delzad Sanjay Hirale - Vedant
* Apoorva Arora - Jenny
* Suraj Kumar - Ratan Singh
* Ganesh Yadav - L F Rebello
* Sita Singh - Mrs. Rebello

==Plot==
BUBBLE GUM is a coming of an age story of family and for families....set in Industrial town of 1980 Jamshedpur - an era when there was no TV, Internet, mobile and face book! It is a fun, frothy and warm coming of an age story of a fourteen-year-old Vedant. He has just stepped out of childhood and has not yet stepped into adulthood. It is while going through turbulent phase of life in his academically critical year of Standard 9 first love or rather infatuation happens to him and sends his relationship with his family and his world upside down finally leading him to discover the meaning of love, life and relationship in a positive way!

Its a story that is highly relatable and identifiable as we all have been 14 & 15. We all have gone through that phase of life! Its a film which promises to take each of us down the memory lane... a film that promises to make everyone nostalgic about their childhood!

==Critical reception==
Bubble Gum achieved critical acclaim among the Indian critics. Mayank Shekhar of Hindustan Times awarded the film 3 out of 5 stars, saying that "the pic is a sweet, rare, candid personal piece". 

Taran Adarsh of Bollywood Hungama states that "Bubble Gum has its heart in the right place. A film that arrives with zilch expectations, but succeeds in taking you back in time when life was simpler and sweet. Recommended!". 

==References==
 

== External links ==
*  
* http://www.coolage.in/2013/10/01/an-enlightening-colloquium-by-sanjivan-lal-director-and-film-mak/

 
 
 