Tortoise in Love
{{Infobox film
| name           = Tortoise in Love
| image          = Tortoise in Love.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Guy Browning
| producer       = Steffan Aquarone
| writer         = Guy Browning
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Tom Mitchelson
* Alice Zawadzki
* Tom Yates
* Mike Kemp
* Steven Elder
* Anna Scott
* Lesley Staples
* Duncan Armitage
}}
| cinematography = 
| music          = Geoff Cottrell
| editing        = 
| studio         = 
| distributor    = Immense Productions    United Kingdom|ref2= }}
| runtime        = 84 minutes 
| country        = United Kingdom
| language       = English 
| budget         = £500,000 cash & services rendered   
| gross          = 
}} crowd sourced from the village of Kingston Bagpuize and the neighbouring village of Southmoor.   

== Plot ==
Tom, a gardener at Kingston Bagpuize House falls in love with Anya, a Polish au pair. Agonisingly slow at making a move to win the heart of Anya, the whole village came together to help speed things up.

== Production == Rural Development Programme for England  towards the total budget of £250,000 in cash. In addition, the residents provided another £250,000 in services rendered. In return, they received shares in the film, with any profits going towards village improvement projects. 

Filmed and set in the village of Kingston Bagpuize, the filming took place over a six weeks period with Kingston Bagpuize House and the local pub serving as backlot. While the main cast and key members of the production crew are professionals, the rest of the cast and crew were all provided for by the village residents and businesses. The local Womens Institutes provided the catering, hair salon provided the hair and make up, and a local composer Geoff Cottrell    the film score, which won Best Original Score at the Idyllwild Film Festival in 2013. All of the non-local based personnel were housed in local homes. 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 