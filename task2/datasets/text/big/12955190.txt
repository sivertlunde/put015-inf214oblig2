Hungry Hearts (1916 film)
 
{{Infobox film
| name           = Hungry Hearts
| image          = 
| image size     = 
| caption        = 
| director       = Will Louis
| producer       = Louis Burstein
| writer         = 
| narrator       = 
| starring       = Oliver Hardy
| music          = 
| cinematography = 
| editing        = 
| distributor    = Vim Comedy Company
| released       =  
| runtime        = 
| country        = United States  English intertitles
| budget         = 
}}
 silent comedy short film, filmed in Jacksonville, Florida by Vim Comedy Company, and featuring a young Oliver Hardy.

==Cast==
* Oliver Hardy - Plump (billed as "Babe Hardy")
* Billy Ruge - Runt
* Ray Godfrey - A Model
* Edna Reynolds - A Widow
* Bert Tracy - Art Connoisseur

==See also==
* List of American films of 1916
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 

 