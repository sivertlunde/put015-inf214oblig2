Naksha
{{multiple issues|
 
 
}}

{{Infobox Film 
| name     = Naksha 
| image          = Naksha.jpg 
| caption        = Movie poster for Naksha 
| director       = Sachin Bajaj
| producer       = Akshay Bajaj
| writer         = Milap Zaveri Tushar Hiranandani
| narrator       = Anil Kapoor machaan pochaan
| starring       = Sunny Deol Vivek Oberoi Sameera Reddy Jackie Shroff
| music          = Pritam
| cinematography = Vijay Arora
| editing        = Sanjay Sankla
| costume design = Simple Kapadia
| distributor    = Om Films Pvt. Ltd.
| released       = 8 September 2006
| runtime        = 126 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2006 Bollywood film. The film is directed by Sachin Bajaj and stars Sunny Deol, Vivek Oberoi, Sameera Reddy and Jackie Shroff.

The role of Ria was initially offered to Amrita Rao but she declined the offer as she was filming for Vivah.

== Synopsis ==
Youngster Vicky ( ) from a river rafting accident. Bali and his men capture Vicky and Veer, and slaughter the pygmies. Bali reveals that the map describes the location of a powerful device: the armour and ear rings of the mythical warrior Karna (of the Mahabharata). This armour will make the wearer invincible and all powerful. Vicky and Veer escape. Vicky and Veer arrive at the final destination, only to find that Bali has beaten them to it. Endowed with divine strength, the evil Bali easily overpowers Vicky and Veer and prepares to kill them. Vicky and Veer exploit the flaw to defeat Bali and restore the armour. They escape the temple just in time before it comes crashing down, thus sealing it off forever.

==Cast==
{| class="wikitable"
|-
! Actor/Actress !! Role
|- Sunny Deol|| Veer Malhotra
|- Vivek Oberoi|| Vicky Malhotra
|- Sameera Reddy|| Riya
|- Jackie Shroff|| Bali
|}

==Soundtrack==

The music was composed by Pritam. The lyrics were written by Sameer apart from the song U n I which was penned by Mayur Puri.

== Response ==

Junta avoided the movie, primarily due to the showcasing of Vivek & Sam, as the leads; the semi-old guns still did the job - Sunny Boy & Jackie Man. Taking a closer look, ignoring the small little vices and the movie holds a charm.

=== Critical ===
The movie received extremely poor reviews by most critics. The films flawed story and performances were especially criticized. The sets and action, however, drew praise. As Lala Amarnath would say, "Not Bad, Not Too Bad At All"

===Box office===
The film recovered its costs, despite being panned by the critics, which yet again proves the point that India is still a democracy, at least as far as the movies` go - majority wins.

==External links==
*  
*  

 
 
 