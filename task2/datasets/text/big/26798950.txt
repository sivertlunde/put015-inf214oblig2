Perder es cuestión de método
{{Infobox film
| name = Perder es Cuestión de Método 
| image = Cartel perder es cuestion de metodo 0.jpg
| caption = Perder es cuestión de método promotional poster
| director = Sergio Cabrera
| producer = Sergio Cabrera  Florina Lemaitre
| writer = based on the book of Santiago Gamboa
| starring = Daniel Gimenez Cacho  Martina Garcia   Cesar Mora   Carlos Benjumea   Jairo Camargo  Victor Mallarino   Saín Castro
| music = 
| studio = Caracol Television
| released =  
| runtime = 107 min.
| country = Colombia 
| language = Spanish
}}
Perder es Cuestión de Método ( ) is a 2005 film directed by Sergio Cabrera, and starring Daniel Gimenez Cacho, Cesar Mora, Martina Garcia, Victor Mallarino and Sain Castro.

A grim history, but also human and fun, led by Colombian Sergio Cabrera (Golpe de estadio, La Estrategia del Caracol), the movie exposes the corruption rampant in the South American country - and the unholy nexus between Politicians, Land Developers, the mafia and the Military Police.

Based on the novel "Method of Losing" by Santiago Gamboa ( the quote is from Juan Sepulveda : "Defeat is often a Question of Method," which in the movie is put into the mouth of the ageing and psychotic mentor of the journalist, Guzman ).

The novel was adapted by screenwriter  ) plays the journalist Victor Silampa and Martina Garcia (Golpe de Estadio) plays the 16-year-old prostitute Quica, while Cesar Mora, in a role worthy of an Oscar-nomination, plays Sancho Panza to the journalists Don Quixote, as Estupinan - probably the most human and sympathetic of all the characters - representing the Colombian "Everyman".

== Plot == Impaled corpse. A journalist named Victor Silampa (Gimenez Cacho) is writing an article a la Peter Gay, about bourgeouise attitudes about Romantic Love through the 17th to 19th centuries. Silampa receives a call from the Police-Colonel Aristophanes Moya (Benjumea) who calls him to the station immediately notifying him of the crime. Moya suggests an exchange of favors (Silampa had a criminal record), whereby the journalist would actively investigate the case while Moya will provide him with protection if, in addition, the journalist will "ghost-write" for him a speech for a slimming non-conformist Evangelical Christian sect, originating in Memphis, called "The Last Supper".

Esquilache is being followed by Abuchija and Estupiñan as he arrives in his official car at an apartment where he confronts Barragan - who is extremely agitated and hysterical, and pulling a gun, alternately levels it at the Councilman and his own temple. He has discovered that Esquilache was complicit in the impalement of Pereira Antunez and this is the impaled. The Councilman cannot hide his despising his nieces husband, and in a sudden reversal, Barragan who has been putting the gun into his mouth and to his temple, shoots Esquilache and is killed by the lawyer. 

Moya, sitting at his desk, recounts for Silampas benefit the polices closing report - "the Official Version" ! Silampa is enraged at the neatly self-serving concoction of Truth that Moya is pandering but helpless in front of the Colonels power. The Colonel reminds him that in fucking Quica he was having sex with a minor, and that the body of Tiflis henchman Morsita was found stabbed to death in Quicas apartment - and if Silampa is going to be exceedingly particular about "justice" - Moya requests Silampa to forget about justice, to go along and publish "the official version" as his journalistic effort and "win the Simon Bolivar Prize for Journalism" so that his girlfriend will return to him upon his getting famous, if not rich - and also to finish the speech for "The Last Supper" and deliver it to him. Silampa leaves impotent, chastened and hemmorhoidal and surprised when sees Vargas Vicuña and Susan Caviedes goes free.

== Cast ==
*Daniel Gimenez Cacho as Victor Silampa: A journalist who, through an exchange of favors with the police solved the case of a body impaled on the territories of a lake near Bogota.
*Cesar Mora as Emir Estupiñan:A man searching for his brother alive or dead, which ends up being a victim of this crime.
*Martina Garcia as Maria "Quica" Gomez Castañol: A prostitute who has a relationship with Silampa almost falls for her even as a minor and ends involved in the events.
*Victor Mallarino as Councilan Marco Tulio Esquilache: A Bogota councilman seeking control of the land of the lake and is an accomplice of the impalement of the landowner Pereira Antunez.
*Carlos Benjumea as Coronel Aristofanes Moya: An obese police colonel investigating the case of impalement. He has a close friendship with Silampa and this research gives a speech in return for a sect of thinning.
*Jairo Camargo as Angel Vargas Vicuna: A civil engineer who seeks control of land in the lake, is complicit in Esquilache and Tiflis and who impaled Pereira Antunez.
*Humberto Dorado as Heliodoro Tiflis: An emerald and entrepreneur eccentric owner of a hotel and bar "Lolitas" who inherited of Pereira Antunez the land from the lake.
*Sain Castro as Emilio Barragan: A lawyer, husband of the niece of Esquilache. Deed completely legalize reasons for whoever controls the land of the lake.
*Gustavo Angarita as Guzman: An ezquisofrenic former journalist mentor of Silampa.

== External links ==
*  

 
 
 