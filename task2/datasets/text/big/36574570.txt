Missing Pieces (film)
{{Infobox film
| name           = Missing Pieces
| image          = Missing_Pieces_(1992)_theatrical_release_poster.jpg
| caption        = Theatrical release poster.
| director       = Leonard B. Stern
| producer       = Bill Carraro Aaron Russo
| writer         = Leonard B. Stern
| narrator       =
| starring       = Eric Idle Robert Wuhl Lauren Hutton Janice Lynde James Hong
| music          = Marvin Hamlisch
| cinematography = Peter Stein
| editing        = Evan A. Lottman
| distributor    = Universal Pictures
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}

  Leonard Stern. Eric Idle plays a former greeting-card writer whose possible inheritance causes him great distress. 
==Plot==

Eric Idle and Robert Wuhl star as a mismatched couple, Wendel and Lou in this mystery film|mystery-comedy caper. Characters include a one-handed kingpin,a lawyer who suffers from dwarfism, and twin brothers, one a crazed photographer and the other a mild-mannered antique dealer. One of Wendels many former foster parents, Mr Hu, dies, and his lawyer (the dwarf) gives Wendel his inheritance. A riddle. Wendel and Lou (his cello-playing best friend) soon realise the riddle isnt Hus heirloom, just the key to finding Wendels inheritance. But they are pursued by several unfavourable characters, almost all of whom want them dead. 

==Cast==
* Eric Idle - Wendel
* Robert Wuhl - Lou Wimpole
* Lauren Hutton - Jennifer
* Bob Gunton - Mr. Gabor
* Richard Belzer - Baldesari
* Bernie Kopell - Dr. Gutman
* Kim Lankford - Sally
* Donald Gibb - Hurrudnik
* Leslie Jordan - Krause
* Louis Zorich - Ochenko
* Don Hewitt - Scarface
* John de Lancie - Paul / Walter Thackary
* James Hong - Chang
* Janice Lynde - Marion
* Mary Fogarty - Mrs. Callahan (as Mary Fogerty)

==Reception==
The film opened in theatres on 17 July 1992, and was available on DVD from 20 August 1996. Also on HBO Video.

==External links==
 

 
 
 
 
 
 
 
 