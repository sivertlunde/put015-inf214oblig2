L'Enquête Corse
 

LEnquête Corse (English: The Corsican investigation), known as The Corsican File in its US release, is a 2004 French comedy film directed by Alain Berbérian. It is based on the comic book of the same name, one of the stories from the Jack Palmer series by René Pétillon.    
 Gaumont and Legend, and written by Christian Clavier and Michel Delgado.  The story follows the escapades of private investigator Jack Palmer, a pseudonym of Rêmi Francois, trying to locate a man living on the island of Corsica who is to inherit a property from a Will (law)|will.

Nicolas Jouenne of Le Figaro said, "And you must admit that the two actors stick to their characters perfectly! Although it has not adopted the unmistakable look of Jack Palmer, Christian Clavier is shown in a relatively convincing interpretation while retaining far from his usual bidding a bit boring. Facing him, Jean Reno turns out perfect in solitary independence leader and blood, in addition to the Corsican accent!"  , Le Figaro, 21 October 2008.  (Machine translation of the original)

==Cast==
* Christian Clavier as Rémi François / Jack Palmer
* Jean Reno as Ange Leoni
* Caterina Murino as Léa
* Didier Flamand as Dargent
* Pierre Salasca as Matéo
* Eric Fraticelli (credited as Pido) as Figoli
* Alain Maratrat as De Vlaminck
* François Orsoni as Balducci
* Nathanaël Maïni as Grappa
* Albert Dray as Le capitaine de gendarmerie
* Daniel Delorme as Doumé
* Guy Cimino as Borgnoli
* Jo Fondacci as Diazep
* Philippe Guerrini as Le Marseillais
* Tzek as Bruno

==References==
 

 
 
 
 
 
 
 
 
 
 

 
 