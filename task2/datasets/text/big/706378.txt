Blue in the Face
{{Infobox film
| name           = Blue in the Face
| image          = Blue in the face ver1.jpg
| caption        = Promotional movie poster
| director       = Paul Auster Wayne Wang
| producer       =
| writer         = Paul Auster Wayne Wang
| narrator       =
| starring       = Harvey Keitel Victor Argo Giancarlo Esposito Roseanne Barr Michael J. Fox Lily Tomlin Mira Sorvino Lou Reed Mel Gorham Jim Jarmusch Malik Yoba
| music          = John Lurie
| cinematography = Adam Holender Harvey Wang
| editing        = Maysie Hoy Christopher Tellefsen
| distributor    = Miramax Films
| released       = October 13, 1995
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $2 million
| gross          = $1,268,636
}}
Blue in the Face is a 1995 comedy directed by Wayne Wang and Paul Auster. It stars Harvey Keitel, Victor Argo, Giancarlo Esposito, Roseanne Barr, Michael J. Fox, Lily Tomlin, Mira Sorvino, Lou Reed, Mel Gorham, Jim Jarmusch,and Malik Yoba.

Blue in the Face was filmed over a five-day period as a follow-up to Wangs 1995 movie Smoke (film)|Smoke. During production of Smoke, Keitel and the others ad-libbed scenes in-character between takes and a sequel was made using this improvised material.

Lily Tomlin was nominated for an American Comedy Award as "Funniest Supporting Actress in a Motion Picture" for her performance in this picture.

Blue in the Face features songs by late slain Tejano Queen, Selena.  The bilingual duet with David Byrne, "Gods Child (Baila Conmigo)" is used as sound track.

==Plot==
The film once again centers on the Brooklyn Cigar Store and manager Auggie (Harvey Keitel), although most of the other characters are different. The store owners frustrated wife Dot (Roseanne) is one of them, and one of the plotlines follows her attempts to seduce Auggie. Madonna, Michael J. Fox, Lily Tomlin, and Lou Reed (as himself) also put in appearances. Blue In The Face was shot without a complete script and presents a unique combination of distinctive performances, oddball characters, improvisations, and raffish scenes.

==Cast==
*Harvey Keitel... Augustus "Auggie" Wren
*Victor Argo... Vinnie
*Keith David... Jackie Robinson
*Giancarlo Esposito... Tommy Finelli
*Michael J. Fox... Pete Maloney
*Mel Gorham... Violet
*Jared Harris... Jimmy Rose
*Jim Jarmusch... Bob
*Madonna (entertainer)|Madonna... Singing Telegram Girl
*Lou Reed... As Himself
*Roseanne Barr... Dot
*Mira Sorvino... Young Lady
*Lily Tomlin... Waffle Eater
*Malik Yoba... Watch Man
*RuPaul|Rupaul... Dance Leader
*Selena... Sound Track

==Reception==

The movie received mixed reviews.   

== References ==
 

==External links==
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 