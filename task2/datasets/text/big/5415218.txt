Autism Every Day
{{Infobox film
| name           = Autism Every Day
| image          =
| imagesize      = 250px
| caption        = 
| director       = Lauren Thierry
| producer       = Eric Solomon Lauren Thierry
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = APM Music
| cinematography = Francisco Aliwalas Chris Meagher
| editing        = Brian Dileo Christine Dupree
| studio         = 
| distributor    = 
| released       =  
| runtime        = 13 minutes (2006 debut) 46 minutes (Sundance version)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Autism Every Day is a 2006 documentary film sponsored by Autism Speaks, and produced by Lauren Thierry, Jim Watkins and Eric Solomon.    It follows mothers with autistic children which consists mainly of interviews with the mothers.

A 13 minute version of Autism Every Day debuted at a fundraiser named "A New Decade for Autism" in New York City on May 9, 2006,  and made its mainstream debut on Don Imus show on MSNBC the following day.   It was selected by the Sundance Institute as a special screening film at the 2007 Sundance Film Festival.     A 7-minute version of the film can be found on Autism Speaks YouTube Channel. 

The New York Times said, "While the filmmakers capture hope, love and determination, the documentary also reveals the unrelenting stress and occasional despair in rearing children with autism."  The New York Observer said the film was a  
"short documentary film ... about the lives of mothers of autistic kids. The film consists mainly of interviews with mothers (and scenes of them with their autistic children), mothers whose lives have been utterly transformed. The situation of these mothers is just unrelieved, unrelenting." 
 

According to Stuart Murray, author of Representing Autism: Culture, Narrative, Fascination, disability rights advocates criticized the film for categorizing the disorder as "one of problems and difficulties, especially for parents", while ignoring the positive aspects.      

One interview in the film that drew significant controversy was that of Alison Tepper Singer who contemplated driving off a bridge with her autistic daughter Judie Singer.  Some have drawn a connection between Alison and Karen McCarron, a woman who murdered her autistic daughter on May&nbsp;13, 2006, 4 days after the films debut. 

==See also==
*List of films about Autism
*Autism spectrum disorders in the media
* 
*Dads in Heaven with Nixon
*The Horse Boy
* 

== References ==
 

==External links==
*   - Autism Every Day video
* 


 
 
 

 
 
 
 
 
 
 
 
 