Passport to Pimlico
 
 
{{Infobox film 
 | name = Passport to Pimlico
 | image = Passport to Pimlico film.jpg
 | caption = Original UK cinema poster
 | director = Henry Cornelius 
 | writer = T. E. B. Clarke
 | starring = Stanley Holloway Margaret Rutherford Barbara Murray 
 | producer = Michael Balcon 
 | distributor = General Film Distributors (UK)
| studio = Ealing Studios
 | budget =
 | released =  
 | runtime = 84 minutes
 | language = English 
 | country = United Kingdom
}}
Passport to Pimlico is a 1949 British comedy film made by Ealing Studios and starring Stanley Holloway, Margaret Rutherford and Hermione Baddeley. It was directed by Henry Cornelius.

The script was written by T.E.B. Clarke and demonstrated his usual logical development of absurd ideas. Some scenes in which the residents are refused passage out of their district into London by the authorities, and rely on supplies thrown over the dividing wall by well-wishers, were very topical because the film was made during the Berlin blockade.
 Second World War, when the maternity ward of Ottawa Civic Hospital was temporarily declared extraterritorial by the Canadian government so that, when Princess Margriet of the Netherlands was born there, she would not lose her right to the throne. 

The film was screened at the 1949 Cannes Film Festival, but not entered into the competition.   

==Plot== Edward IV Charles VII ("the Rash"), the last Duke of Burgundy, when he sought refuge there several centuries ago after being presumed dead at the Battle of Nancy. As the charter had never been revoked, Pimlico is legally part of Burgundy. Local policeman P.C. Spiller (Philip Stainton) is shocked to realise "Blimey! Im a foreigner!"

The British government has no legal jurisdiction and requires the Burgundians to form a representative committee according to the laws of the long-defunct dukedom before negotiating with them. Ancient Burgundian law requires that the Duke himself appoint a council. Without one, all seems lost - until a young man from Dijon (Paul Dupuis) steps forward and proves that he is the heir to the dukedom. He duly forms a governing body; one of its members is the shrewd shopkeeper Arthur Pemberton (Stanley Holloway), designated Burgundys Prime Minister.

Very quickly, Burgundy (followed soon after by the rest of London) realises that it is not subject to Rationing in the United Kingdom|post-war rationing and other bureaucratic restrictions, and the district is quickly flooded with entrepreneurs, crooks, and eager shoppers. A noisy free-for-all ensues, which Spiller, the Chief (and only) Constable of Burgundy, finds himself unable to handle. Then the British authorities close the "border" with barbed wire. Having left England without their passports, the bargain hunters have trouble returning home - as one policeman replies to an indignant woman, "Dont blame me Madam, if you choose to go abroad to do your shopping."  

The Burgundians decide that two can play this game and stop an underground train dead in its tracks. "The train is now at the Burgundy frontier", explains an agent of the newly formed customs and excise department. They proceed to ask the passengers if they have anything to declare. 

The infuriated British government retaliates by breaking off negotiations. Burgundy is cut off, like the western sectors of post-war Berlin, and the residents are invited to "emigrate" to England. But the Burgundians are "a fighting people" and, though the children are evacuated, the adults stand fast. As Mrs. Pemberton (Betty Warren) puts it, "Weve always been English and well always be English; and its precisely because we are English that were sticking up for our right to be Burgundians!"

Pimlico is cut off from electricity, food and water (though there is plenty of gin and crisps). The water problem is solved by a covert raid late one night, refilling the reservoir with hoses attached to the nearest fire hydrant on the British side of the border. Unfortunately, the food supply is spoiled when the cellar where it is being stored becomes flooded, and it appears that the Burgundians are beaten. Just in time, three Burgundian youngsters learn about this crisis and toss food across the border, setting an example for sympathetic Londoners; they begin throwing food parcels across the barrier in an improvised "airlift", echoing the one that ended the Berlin blockade. Soon, others get into the act. A helicopter drops a hose to deliver milk. Even swine are parachuted in.

Meanwhile, the government comes under public pressure to resolve the problem. It becomes clear to the bumbling British diplomats assigned to find a solution, Gregg (Basil Radford) and Straker (Naunton Wayne), that defeating the Burgundians would be no easy task, so they negotiate. The sticking point turns out to be the disposition of the unearthed treasure. At last, the local bank manager-turned-Burgundian Chancellor of the Exchequer (Raymond Huntley) hits upon a novel solution: "A Burgundian loan to Britain!"
 Big Ben strikes the hour of reunification, the Burgundians realise they truly are back in England when a torrential downpour sends the temperature plummeting and everyone scurrying for cover.

==Cast==
  
*Stanley Holloway as Arthur Pemberton
*Betty Warren as Connie Pemberton
*Barbara Murray as Shirley Pemberton
*Paul Dupuis as Sébastien de Charolais, Duke of Burgundy John Slater as Frank Huggins
*Jane Hylton as Molly Reed
*Raymond Huntley as Mr. W.P.J. Wix
*Philip Stainton as P.C. Spiller
*Roy Carr as Benny Spiller
*Sydney Tafler as Frederick Albert Fred Cowan
*Nancy Gabrielle as Mrs. Cowan
*Malcolm Knight as Monty Cowan
*Hermione Baddeley as Edie Randall
*Roy Gladdish as Charlie Randall
*Frederick Piper as Jim Garland Charles Hawtrey as Bert Fitch
*Margaret Rutherford as Professor Hatton-Jones
*Stuart Lindsell as Coroner
 
*Naunton Wayne as Straker
*Basil Radford as Gregg
*Gilbert Davis as Bagshawe
*Michael Hordern as Inspector Bashford
*Arthur Howard as Bassett
*Bill Shine as Captain
*Harry Locke as Sergeant
*Sam Kydd as Sapper
*Joe E. Carr as Dave Parsons
*Lloyd Pearson as Fawcett
*Arthur Denton as Customs Official
*Tommy Godfrey as Bus Conductor James Hayter as Commissionaire
*Masoni as Conjurer
*Fred Griffiths as Spiv
*Grace Arnold as Woman in underground
*Paul Demel as Central European
 

===Uncredited===
  
*Winston Churchill as himself (archive footage)
*Clement Attlee as himself (archive footage) Michael Craig
*E.V.H. Emmett as the voice of the newsreel commentator
 
*Bernard Farrel
*Richard Hearne as Nighttime drunk on bicycle
*Arthur Lovegrove as Tough Man on Underground Train
*Frank Phillips as the voice of the radio announcer
 

==Location==
The outdoor scenes were actually shot about a mile away in Lambeth and not in Pimlico. A set was built on a large Second World War bombsite just south of the Lambeth Road at the junction of Hercules Road. This has now been built on by 1960s municipal flats; however, the buildings on the junction of Hercules Road and Lambeth Road can still be recognised from the film as can the railway bridge going over Lambeth Road, particularly from the scenes where food is thrown to the "Burgundians".

==Radio adaptation==
A BBC Radio 4 adaptation was broadcast on 20 January 1996.

==Legacy==
The film inspired the   (1958–1983), particularly through its basic premise that a small part of the nations capital is suddenly found to be a separate microstate.
 German film East German town joining West Germany. Many problems that eventually plagued the actual reunification 20 years later were accurately predicted in the film.

The 1989 Scrooge McDuck story "His Majesty, McDuck" by Don Rosa also shares some similarities with the film.

==See also==
*Micronations

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 