Uncle Sam (film)
{{Infobox film
| name           = Uncle Sam
| image          = UncleSamSlasher.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Blue Underground
| film name      = 
| director       = William Lustig
| producer       = George G. Braunstein
| writer         = Larry Cohen
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = Mark Governor
| cinematography = James A. Lebovitz
| editing        = Bob Murawski
| studio         = A-Pix Entertainment
| distributor    = Solomon International Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $2,000,000
| gross          = 
}}

Uncle Sam is a 1996 horror film directed by William Lustig, and written by Larry Cohen.

== Plot ==

In Kuwait, a military unit uncovers an American helicopter downed by friendly fire at least three years ago. As the wreckage is inspected, Master Sergeant Sam Harper, one of the burnt bodies within, springs to life and kills a sergeant and a Major (United States)|major, and returns to an inert state after muttering, "Dont be afraid, its only friendly fire!"
 Twin Rivers, Independence Day. patriotic young desecrated an American flag.
 congressman is psychopath who physically and sexually abused them, and only joined the military so he could get a "free pass" to kill people.

Jody is told by Barry, another boy who has established an unexplained mental link with Sam, that the undead Sam is responsible for the deaths. With help from Sams old mentor Jed, the boys goes to Jodys house, where they find the lecherous sergeant who had dropped Sam off dead, and stuffed inside Sams coffin. Realizing that Sam will probably go after Louise, the boys and Jed go to her home, where Sam blames Jed (who had told him tales of how glorious combat was) for his current state. Jed retorts by yelling, "You never fought for your country! You just killed for the love of killing!"

Jeds gun proves ineffective against Sam, so he and Louise go to get Jeds cannon while Jody (who Sam claims is the reason he came back) keeps Sam occupied. Jody lures Sam outside, and Jed blasts him with the cannon and destroys him in flames at Louises house. The next day, Sally watches as Jody burns all of his war-themed toys.

== Cast ==
 William Smith as Major
* David "Shark" Fralick as Master Sergeant Sam Harper
* Christopher Ogden as Jody Baker
* Leslie Neale as Sally Baker
* Bo Hopkins as Sergeant Twining
* Matthew Flint as Deputy Phil Burke
* Anne Tremko as Louise Harper
* Isaac Hayes as Sergeant Jed Crowley
* Timothy Bottoms as Donald Crandall
* Tim Grimm as Ralph
* P.J. Soles as Madge Cronin
* Tom McFadden as Mac Cronin
* Zachary McLemore as Barry Cronin
* Morgan Paull as Mayor
* Richard Cummings Jr. as Dan
* Robert Forster as Congressman Alvin Cummings
* Frank Pesce as Barker
* Jason Adelman as Jesse Colbert
* Laura Alcalde as Park Mother
* Raquel Alessi as Girl Student
* Abby Ball as Rick
* Stanton Barrett as Clete
* Mark Chadwick as Willie on Stilts
* Chris Durand as Sergeant
* Taylor Jones as Boy Student
* Desirae Klein as Barbeque Girl
* Jason Lustig as Undertaker
* Joseph Vitare as Kuwaiti Captain

== Reception ==
 slasher flick" that "does a fine job of bringing the pain while we celebrate our independence" even though it "kind of plods along" and "none of it really makes too much sense". 

A review by DVD Verdict described the film as "a sluggish, shoddily produced horror/comedy" that was "a by-the-numbers turd that sports embarrassing child actors, C-level stars slumming for a paycheck (oh P.J. Soles, how far youve tumbled...) and a level of suspense that rivals clipping your toenails in a well lit room".  Uncle Sam was also derided by The A.V. Club, which wrote "Incoherent as social satire and perfunctory and routine as a horror film, Uncle Sam is every bit as lazy and uninspired as the Maniac Cop films that preceded it". 

== References ==

 

== External links ==

*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 