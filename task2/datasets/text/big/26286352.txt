I Killed That Man
{{Infobox film
| name           = I Killed That Man
| image          =
| image_size     =
| caption        =
| director       = Phil Rosen Frank King Maurice King (producer)
| writer         = Henry Bancroft (writer) Leonard Fields (story) David Silverstein (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Harry Neumann
| editing        = Martin G. Cohn
| distributor    = King Brothers Productions Monogram Pictures
| released       = 1941
| runtime        = 71 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

I Killed That Man is a 1941 American film directed by Phil Rosen that was a remake of his 1933 film The Devils Mate.

== Plot summary ==
The film begins with a disparate group of individuals in a room; some are gambling, some are drinking coffee, others are chatting as if at a party.  Curtains are removed from the windows to reveal bars.  The group is in a prison to witness the execution of hard core murderer Nick Ross (Ralf Harolde).  Ross is told there has been no commutation of his execution.  Having never given a confession, Ross proceeds to tell the crowd of the true events of what happened as his benefactor promised him his release; but then falls over dead, the victim of a poison dart.

District Attorney Roger Phillips (Ricardo Cortez) conduct a search of the occupants of the room and together with his girlfriend crime reporter Geri Reynolds  (Joan Woodbury) and his young receptionist Tommy George P. Breakston solve the murder and the criminal mastermind behind Ross.

== Cast ==
*Ricardo Cortez as Roger Phillips
*Joan Woodbury as Geri Reynolds Pat Gleason as Bates
*George Pembroke as Lowell King
*Iris Adrian as Verne Drake
*Herbert Rawlinson as Warden
*Ralf Harolde as Nick Ross
*Jack Mulhall as Collins
*Vince Barnett as Drunk Gavin Gordon as J. Reed John Hamilton as District Attorney
*Harry Holman as Lanning
*George P. Breakston as Tommy

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 