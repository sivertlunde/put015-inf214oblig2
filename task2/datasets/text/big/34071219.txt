The Soul of Broadway
{{infobox film
| name           = The Soul of Broadway
| image          =
| imagesize      =
| caption        =
| director       = Herbert Brenon
| writer         = Herbert Brenon (scenario)
| story          = Herbert Brenon
| starring       = Valeska Suratt
| music          =
| cinematography = Phil Rosen
| editing        =
| distributor    = Fox Film Corporation
| released       = October 18, 1915  (United States)  February 9, 1919  (Re-release) 
| country        = United States
| language       = Silent (English intertitles)
}}
 silent crime drama film produced and distributed by the Fox Film Corporation and directed by Herbert Brenon. Popular vaudeville performer Valeska Suratt starred in the film which was also her silent screen debut. The Soul of Broadway is now considered Lost film|lost.  It is one of many silent films that were destroyed in a fire at Foxs film storage facility in Little Ferry, New Jersey in July 1937. 

==Plot==
As described in a film magazine,  La Valencia (Suratt), a stage beauty, has ensnared a young man who steels in order to shower her with the luxuries that she demands. He is caught and, after serving a 5-year term, emerges from prison a gray haired man. La Valencia comes across him again, and her passion revives. She seeks to ensnare him again, but now he is married and his old life has no charms for him. Desperate, she then threatens to reveal his past to his wife, which leads to a terrific climax.

==Cast==
*Valeska Suratt - Grace Leonard, aka La Valencia William E. Shay - William Craig
*Mabel Allen - June Meredith
*Sheridan Block - Frederick Meredith
*George W. Middleton - Monty Wallace
*Jane Lee - Graces Daughter
*Gertrude Berkeley - Stage Actress (*Gertrude Berkeley was the mother of Busby Berkeley)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 