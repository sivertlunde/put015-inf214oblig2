List of Malayalam films of 1965
 

The following is a list of Malayalam films released in 1965.
{| class="wikitable"
|- style="background:#000;"
! colspan=2 | Opening !! Sl. No. !!Film !! Cast !! Director !! Music Director !! Notes
|-
| rowspan="1" style="text-align:center; background:#ffa07a; textcolor:#000;"| J A N 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 14
|valign="center" |  1  Sathyan || K. Padmanabhan Nair/WR Subba Rao || PS Divakar ||
|-
| rowspan="2" style="text-align:center; background:#dcc7df; textcolor:#000;"| F E B 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 3
|valign="center" |  2  Ambika || M. S. Mani (editor)|M. S. Mani || MS Baburaj ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 18
|valign="center" |  3  Ambika || P. Bhaskaran || K. Raghavan ||
|-
| rowspan="2" style="text-align:center; background:#d0f0c0; textcolor:#000;"| M A R 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 5
|valign="center" |  4  Odayil Ninnu National Film Award for Best Feature Film
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 12
|valign="center" |  5 
| Kadathukaran || Sathyan (actor)|Sathyan, Sheela || M. Krishnan Nair (director)|M. Krishnan Nair || MS Baburaj ||
|-
| rowspan="5" style="text-align:center; background:#ffa07a; textcolor:#000;"| A P R 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 9
|valign="center" |  6 
| Porter Kunjali || Prem Nazir, Sheela || J. Sasikumar || MS Baburaj ||
|-
| rowspan="2"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 10
|valign="center" |  7 
| Inapravukal || Satyan, Prem Nazir || Kunchacko || Dakshinamoorthy ||
|-
|valign="center" |  8  Madhu || P. Subramaniam || G. Devarajan ||
|-
| rowspan="2"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 30
|valign="center" |  9 
| Muthalali || Prem Nazir, Sheela || M. A. V. Rajendran || Pukazhenthi ||
|-
|valign="center" |  10 
| Kalyana Photo || Madhu (actor)|Madhu, Adoor Bhasi || JD Thottan || K. Raghavan ||
|-
| rowspan="3" style="text-align:center; background:#dcc7df; textcolor:#000;"| M A Y 
| rowspan="2"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 7
|valign="center" |  11  Madhu || NN Pisharady || MS Baburaj ||
|-
|valign="center" |  12 
| Kuppivala || Prem Nazir, Sukumari || SS Rajan || MS Baburaj ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 28
|valign="center" |  13 
| Thankakudam || Prem Nazir, Sheela || SS Rajan || MS Baburaj ||
|-
| rowspan="1" style="text-align:center; background:#d0f0c0; textcolor:#000;"| J U N 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 4
|valign="center" |  14 
| Rosy (film)|Rosy || Prem Nazir, Kaviyoor Ponnamma || P. N. Menon (director)|P. N. Menon || KV Job ||
|-
| rowspan="1" style="text-align:center; background:#ffa07a; textcolor:#000;"| J U L 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 10
|valign="center" |  15  Sharada || M. Krishnan Nair (director)|M. Krishnan Nair || MS Baburaj ||
|-
| rowspan="3" style="text-align:center; background:#dcc7df; textcolor:#000;"| A U G 
| rowspan="2"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 19
|valign="center" |  16  Madhu || Ramu Kariat || Salil Chowdhury ||
|-
|valign="center" |  17  Madhu || Ramu Kariat || Salil Chowdhury ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 28
|valign="center" |  18  Madhu || GK Ramu || MS Baburaj ||
|-
| rowspan="4" style="text-align:center; background:#d0f0c0; textcolor:#000;"| S E P 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 2
|valign="center" |  19  Madhu || J. Sasikumar || PS Divakar ||
|-
| rowspan="2"  style="text-align:center; background:#edefff; textcolor:#000;"| 3
|valign="center" |  20  Sharada || RS Prabhu || BA Chidambaranath ||
|-
|valign="center" |  21 
| Kattupookkal || Madhu (actor)|Madhu, Adoor Bhasi || K Thankappan || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 7
|valign="center" |  22 
| Kathirunna Nikah || Prem Nazir, Sheela || M. Krishnan Nair (director)|M. Krishnan Nair || G. Devarajan ||
|-
| rowspan="4" style="text-align:center; background:#ffa07a; textcolor:#000;"| O C T 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 8
|valign="center" |  23 
| Kochumon || Prem Nazir, Sheela || K Padmanabhan Nair || Alleppey Usman ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 9
|valign="center" |  24 
| Bhoomiyile Malakha || Prem Nazir, Sukumari || P. A. Thomas || Jaya Vijaya, MA Majeed, PS Divakar ||
|-
| rowspan="2"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 22
|valign="center" |  25 
| Daaham || Sathyan (actor)|Sathyan, Sheela || KS Sethumadhavan || G. Devarajan ||
|-
|valign="center" |  26  Dakshinamoorthy || National Film Award for Best Feature Film
|-
| rowspan="3" style="text-align:center; background:#dcc7df; textcolor:#000;"| N O V 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 13
|valign="center" |  27  Sathyan || Kunchacko || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 20
|valign="center" |  28 
| Pattuthoovaala || Madhu (actor)|Madhu, Sheela || P. Subramaniam || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 26
|valign="center" |  29  Sathyan || S R Puttanna || Baburaj ||
|-
| rowspan="3" style="text-align:center; background:#d0f0c0; textcolor:#000;"| D E C 
| rowspan="2"  style="text-align:center; background:#edefff; textcolor:#000;"| 24
|valign="center" |  30  National Film Award for Best Feature Film
|-
|valign="center" |  31  Madhu || J. Sasikumar || MS Baburaj ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 31
|valign="center" |  32 
| Sarppakavu || Madhu (actor)|Madhu, Sukumari || JD Thottan || MS Baburaj ||
|}

 
 
 

 
 
 
 
 