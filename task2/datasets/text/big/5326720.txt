The Actors
 
{{Infobox film
| name = The Actors
| image = Actors_moviep.jpg
| caption =
| director = Conor McPherson Redmond Morris
| writer = Conor McPherson
| starring = Michael Caine Dylan Moran Lena Headey Michael Gambon Miranda Richardson Michael McElhatton Abigail Iversen Aisling OSullivan Ben Miller Simon Delaney Alvaro Lucchesi
| music = Michael Nyman
| cinematography = Seamus McGarvey
| editing = Emer Reynolds
| distributor = Miramax Films
| released =  
| runtime = 91 minutes
| country = Ireland
| language = English
| budget =
}}
The Actors is a 2003 film written and directed by Conor McPherson and starring Dylan Moran and Michael Caine. In supporting roles are Michael Gambon, Miranda Richardson and Lena Headey .

The Actors is a contemporary comedy set in Dublin. It follows the exploits of two mediocre stage actors as they devise a plan to con a retired gangster out of £50,000. The gangster owes the money to a third party, whom he has never met. The actors take advantage of this fact by impersonating this unidentified third party, and claiming the debt as their own. To pull it off they enlist Morans eerily intelligent nine-year-old niece, who restructures the plan each time something goes wrong.
 Richard III Ian McKellens production.
 Olympia Theatre, and it is noteworthy for featuring the famous glass awning over the entrance which has since been destroyed in a traffic accident. The glass awning has since been rebuilt to its full former glory.

== Reception ==
Empire magazine gives the film 2/5, describing it as "Based on an idea by Neil Jordan, The Actors had the potential to be gut-achingly funny. But instead it ends up raising a few paltry smiles." 

== Soundtrack ==
{{Infobox album  
| Name        = The Actors
| Type        = soundtrack
| Longtype    =
| Artist      = Michael Nyman, Conor McPherson, Fionnula Flanagan
| Cover       = Nymanactors.jpg
| Cover size  = 200
| Caption     =
| Released    = May 19, 2003
| Recorded    = minimalism
| Length      =
| Language    = English
| Label       = EMI Records
| Director    =
| Producer    =
| Compiler    =
| Chronology  = Michael Nyman
| Last album  =   2003
| This album  = The Actors 2003
| Next album  =   2004
}}
# Act One - The Michael Nyman Band and Moss Hall Junior Choir
# Star Of The Sea - Laura Cullinan, Bebhinn Ni Chiosain, Cliona Ni Chiosain & Niamh H Reynolds
# Zinc Bar Walk - The Michael Nyman Band
# A Certain Party - The Michael Nyman Band
# House On Fire - The Michael Nyman Band
# Act Two - The Michael Nyman Band
# Could This Be Love? - Lena Headey & Dylan Moran
# Mary Directs Tom - The Michael Nyman Band
# Unas Waltz - Una Ni Chiosain/Conor McPherson
# Rope Trick - The Michael Nyman Band
# Dolores On The Beach - The Michael Nyman Band
# Return To The Scene Of The Crime - The Michael Nyman Band
# Seems So Long - Cathy Davey
# Wheelchair Chase - The Michael Nyman Band
# Tubbetstown Elegy - The Michael Nyman Band
# Lovely Morning - Cathy Davey
# Mrs OGrowny Appears - The Michael Nyman Band
# Zinc Piano - The Michael Nyman Band
# Final Act - The Michael Nyman Band

==References==
 

==External links==
*  
*   at Rotten Tomatoes

 
 

 
 
 
 
 
 
 