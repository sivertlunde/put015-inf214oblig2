Blackmail (1939 film)
{{Infobox film
| name = Blackmail
| image = Blackmail (1939 film) poster.jpg
| caption = Theatrical release poster
| director = H.C. Potter
| producer = Fred T. Gallo
| writer = Endre Bohem (story) and Dorothy Yost (story) David Hertz (screenplay) and William Ludwig (screenplay)
| starring = Edward G. Robinson Ruth Hussey Gene Lockhart David Snell Edward Ward
| cinematography =
| editing =
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 81 minutes
| language = English
| budget =
}}
Blackmail is a 1939 crime drama film starring Edward G. Robinson and was directed by H.C. Potter.

==Plot==
John Ingram is a very successful oil-field firefighter and a family man.  All is going so well, hes even bought his own oil well in hope of striking it rich.  His greatest fears are realized, however, when a man, William Ramey, from his secret past sees Ingram in a newsreel and shows up looking for a job.

Ramey attempts to blackmail Ingram, who had run from a chain gang years ago and started a new life under an assumed name.  After a shady deal is made, Ingram is tricked and Ramey turns him into authorities, who return him to a chain gang.  Ramey subsequently becomes a very rich man.

When Ingram finds out about the success of the man who betrayed him, he plans a daring escape in an attempt to return home and get revenge.

==Cast==
*Edward G. Robinson as John R. Ingram, an alias of John Harrington
*Ruth Hussey as Helen Ingram
*Gene Lockhart as William Ramey
*Bobs Watson as Hank Ingram Guinn Big Boy Williams as Moose McCarthy (as Guinn Williams) John Wray as Diggs
*Arthur Hohl as Rawlins
*Esther Dale as Sarah

==External links==
*  

 

 
 
 
 
 


 