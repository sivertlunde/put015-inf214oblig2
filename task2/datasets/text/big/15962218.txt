Anzio (film)
{{Infobox film
| name           = Anzio
| image          = Anzio 1968.jpg Frank McCarthy
| director       = Edward Dmytryk Duilio Coletti
| producer       = Dino De Laurentiis
| studio         = Dino de Laurentiis Cinematografica Columbia Pictures
| screenplay     = HAL Craig
| based on       =  
| starring       = Robert Mitchum Robert Ryan Peter Falk Earl Holliman
| music          = Riz Ortolani
| cinematography = Giuseppe Rotunno Peter Taylor
| distributor    = Columbia Pictures
| released       =   
| runtime        = 117 minutes
| country        = Italy, USA
| language       = English
| budget         =
| gross          = $1,400,000 (US, Canada) 
}} Italian and American co-production, about Operation Shingle, the 1944 Allied seaborne assault on the Italian port of Anzio in World War II. It was adapted from the book Anzio by Wynford Vaughan-Thomas, who had been the BBC war correspondent at the battle.

The film stars Robert Mitchum, Peter Falk, and a variety of international film stars, who mostly portray fictitious characters based on actual participants in the battle. The two exceptions were Wolfgang Preiss and Tonio Selwart, who respectively played Field Marshal Albert Kesselring and General Eberhard von Mackensen. The film was made in Italy with an Italian film crew and produced by Italian producer Dino De Laurentiis; however, none of the main cast were Italian, nor were there any major Italian characters. The film was jointly directed by Edward Dmytryk and Duilio Coletti.

In the English-language version, Italians are portrayed speaking their native language, but in scenes involving the German military commanders, these speak English to each other.

==Plot==
After meeting a general, war correspondent Dick Ennis (Robert Mitchum) is assigned to accompany US Army Rangers for the upcoming attempt to outflank the tough enemy defenses.  The amphibious landing is unopposed, but the bumbling American general, Mark W. Clark (Robert Ryan) is too cautious, preferring to fortify his beachhead before advancing inland.  Ennis and a Ranger drive in a jeep through the countryside, discovering there are few Germans between the beachhead and Rome, but his information is ignored.  As a result, the German commander, Kesselring, has time to gather his forces and launch an effective counterattack.
 Port Cros during Operation Dragoon, the invasion of southern France. ) Ennis survives to publicly question the competence of the Allied commander.

==Cast==
 
* Robert Mitchum as Dick Ennis, war correspondent (based on Ernie Pyle)
* Peter Falk as Corporal Jack Rabinoff (based on Sergeant Jake Wallenstein) Mark Clark)
* Earl Holliman as Technical Sergeant Abe Stimmler
* Mark Damon as Private Wally Richardson Arthur Kennedy as Major General Jack Lesley (based on John P. Lucas)
* Wolfgang Preiss as Generalfeldmarschall Albert Kesselring
* Reni Santoni as Private Movie
* Joseph Walsh as Private Doyle Thomas Hunter as Private Andy
* Giancarlo Giannini as Private Cellini
* Wayde Preston as Colonel Hendricks (based on William O. Darby)
* Arthur Franz as Major General Howard (based on Lucian Truscott) Anthony Steel as Major-General Marsh Patrick Magee as Major-General Starkey
 

==Response==
The movie opened to mixed reviews in the US; many felt it didnt work as well as Dmytryks early war films. The New York Times film review was generally dismissive, and describes the film as "a very ordinary war movie with an epic title, produced by Dino De Laurentiis, the Italian producer... who thinks big but often produces small".   In contrast, Chicago Sun-Times critic Roger Ebert had a more favourable opinion of the film, described it as "a good war movie and even an intelligent one". 

==Production== Jack Jones. Luigi Scaccianoce was the production designer.

Peter Falk thought that the script he read was clichéd and wanted off the film.  At the last minute, Dino DeLaurentiis put Falks name above the title billing and gave him his choice of writer for his characters dialogue.  Falk stayed and wrote his lines himself.  The production saw DeLaurentiis bring in for the first time another actor who made a debut, Giancarlo Giannini, who would later do international films and would work with director Lina Wertmüller.

==References==
 

==External links==
*  
*  
*  .
*  .

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 