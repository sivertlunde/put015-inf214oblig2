Sith Apprentice
{{Infobox film
| name = Sith Apprentice
| image = SithPosterSmall.jpg
| director = John E. Hudgens
| producer = John E. Hudgens
| writer = John E. Hudgens Lowell Cunningham Denny Humbard
| starring = Robert Alley Robert E. Bean Kristen Caron Patrick McCray
| distributor = Z-Team Productions Atomfilms
| released =  
| runtime = 12 min
| language = English
| budget = $1000
}}
 Men in spoof of The Apprentice, with Emperor Palpatine filling the Donald Trump role in his search for a new apprentice. The final candidates in the film are Darth Vader, Darth Maul, Count Dooku, and Jar Jar Binks.
 The Apprentice, there are several other notable targets, including swipes at The Princess Bride, Monty Python and the Holy Grail, Dracula (1958 film)|Dracula, The Lord of the Rings, and in the films standout sequence, Darth Vader takes to the stage, dancing Riverdance-style with a squad of stormtroopers. 

At one point in the film, Vader cuts off Dookus head and hands in a scene staged similarly to one in   where Anakin, not yet Vader, cuts off Dookus hands and then proceeds to cut off his head. However, Sith Apprentice was written and filmed months before Revenge of the Sith premiered. 
 Official Star Wars Fan Film Awards.  In August 2010, Time (magazine)|Time magazine listed it as one of the Top 10 Star Wars fanfilms. 

==Cast==
* Robert Alley ... Darth Sidious
* Robert E. Bean ... Darth Vader
* Kristen Caron ... Darth Maul
* Patrick McCray ... Count Dooku
* Brandon Alley and James W. Williams... JarJar Binks
* Brian Boling ... Boba Fett
* Uncredited actor ... Jango Fett
* Amy Earhart ... Pink Five
* Heather Harris ... The FemTrooper
* John E. Hudgens ... The voice of Darth Vader
* Denny Humbard ... The Reluctant Jedi
* John Mailen ... The voice of JarJar Binks
* Sarah Mailen ... Emperors Advisor #1
* Jeff McClure ... Emperors Advisor #2
* Ziggy McMillan ... Darth Maul (voice)
* Tom Ott ... Imperial Admiral
* Christine Papalexis ... Yoda
* Stephen Stanton ... The voice of Yoda

==References==
 

==External links==
*   - News, behind-the-scenes info, and more
*  
*  
*  

 
 
 
 


 
 

 