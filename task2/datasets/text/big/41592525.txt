Vingt Mille Lieues sur la Terre
{{Infobox film
| name           = Vingt Mille Lieues sur la Terre
| image          = 
| caption        = 
| director       = Marcello Pagliero
| producer       = 
| writer         = Leonid Zorin, Sergey Mikhalkov, Semyon Klebanov, Michel Cournot
| starring       = Léon Zitrone  Yuri Belov  Nikolay Savin  Tatiana Samoilova  Jean Rochefort  Jean Gaven  Valentin Zubkov  Evgeny Burenkov
| music          = Nikita Bogoslovsky
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 
| country        = USSR/France
| awards         = 
| language       = Russian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
«Vingt mille lieues sur la terre» (20,000 Leagues Across the Land or "Léon Garros Is Looking for His Friend",  ) is 1960 Soviet-French feature film by Marcello Pagliero.

The film premiered in the USSR was held January 13, 1961, and in France was January 18, 1961.

== Plot ==
During the Second World War and the Leon Garros and Boris Vaganov escapes from a Nazi concentration camp. After 15 years as a journalist Leon with buddies visited the Soviet Union to make a report and casually find Boris. But Boris is not in Moscow, and for the sake of meeting a friend Garros have to overtake a car half the country... Together with foreigners is traveling translator Nikolai, who turn to looking for Natasha, his brothers runaway bride.

== Actors ==
* Léon Zitrone - Léon Garros, journalist
* Yuri Belov - Nikolay Savin, translator
* Tatiana Samoilova - Natasha, singer
* Jean Rochefort - Fernand
* Jean Gaven - Gregoire
* Valentin Zubkov - Alexey Savin, polar aviator and Natashas groom
* Evgeny Burenkov - Boris Vaganov
* Lyudmila Marchenko - elevator woman in the hotel "Ukraine"
* Antonina Maksimova - chairman of the kolkhoz
* Vladimir Ivashov - Fyodor, a young worker in Bratsk
* Valentina Kutsenko - Olga
* Nina Nikitina - mother of Fyodor
* Margarita Zharova - maid
* Alexei Vanin - theatrically
* Tamara Yarenko - Claudia, hotel receptionist
* Zinovy Gerdt - voiceover

== Links ==
*  
*  

 
 


 