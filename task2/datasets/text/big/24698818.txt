Bendu Apparao R.M.P
{{Infobox film
| name           = Bendu Apparao RMP
| image          = Bendu Apparao RMP Poster HD.jpg
| alt            =  
| caption        =
| director       = E V V Satyanarayana
| producer       = D. Ramanaidu
| writer         = Veligonda Srinivas   (Dialogues ) 
| Screenplay     = E V V Satyanarayana
| starring       = Allari Naresh Kamna Jethmalani Koti
| cinematography = V Jayaram
| editing        = Gautham Raju
| studio         = Suresh Productions
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
Bendu Apparao RMP is a 2009 Telugu comedy film starring Allari Naresh and  Kamna Jethmalani in the lead roles. The film was directed by E V V Satyanarayana and produced by D. Ramanaidu.  

The film was made on a reasonable budget of 3 crores and was released with 35 prints on 16 October all over AP and it generated instant hit talk.  The film continued to dominate the box office and completed a successful 50 day run. This is the only  movie has struck the right chord with the audiences across theaters as one of the most entertaining films after the unbeaten box office success Magadheera. 

==Plot==
Bendu Appa Rao (Allari Naresh) is a RMP doctor in a tiny village in East Godavari. Though he does not have much knowledge about medicines, he uses his brain to come out of critical situations. He earns money by all means as his sister faces torture in the hands of her husband (Krishna Bhagavaan), as he could not pay the entire dowry to him. He maintains close relations and friendship with everyone in the village, which include a barber (Uttej), a tailor (Srinivasa Reddy), a washer man (L.B. Sriram) and a postman (Kondavalasa Lakshmana Rao).

Raja (Ahuti Prasad) is a landlord and Padma Priya (Kamna Jethmalani) is his only daughter. Padma falls in love with Appa Rao and the latter too loses his heart to her. Once, Appa Rao and his friends, while returning to village after watching a movie, find a grievously injured person called Sivaji. Before breathing his last, he pleads with Appa Rao to hand over Rs 15 lakh to his family. Despite lot of pressure from his friends to share the money among themselves, Appa Rao makes many efforts to hand over the money to Sivajis kin.

On learning that Sivajis parents are no more and sister had left the country after marrying a person, Appa Rao agrees to share the money between the tailor, barber and himself. He gives them Rs 2 lakh to each to settle their debts; he too takes a part of the money to settle his sisters life. He utilizes the balance amount to construct a school building in the village and names it after Sivaji. However, Appa Rao makes everyone believe that they got a prize in lottery.

Accidentally, Sivajis parents and sister reach the village and Appa Rao comes to know the truth. He feels guilty as he had already spent the money. So, he mortgages his house to a local trader (Jeeva (Telugu actor)|Jeeva) and spends it for the bypass surgery of Sivajis father. He also promises to marry Sivajis sister. But on the day of marriage, the President of the village (Chalapathi Rao) tells to Gayathri that Appa Rao killed Sivaji and robbed the Money and wants to marry Gayathri to avoid future legal problems. Gayathri believes it and Appa Rao is beaten severely. But Raju and the Local Trader reveal the truth and save Appa Rao. Padma is married to Appa Rao in the climax as Gaythri understands their Love.

==Cast==
 
* Allari Naresh as Bendu Appa Rao
* Krishna Bhagavaan as Appa Raos brother-in-law
* Kamna Jethmalani as Padma
* Meghana Raj as Gayathri
* Ahuti Prasad as Raju/Padmas father
* Raghubabu as Rajus Servant
* L.B. Sriram as Samaram (cock) feeder.
* Dharmavarapu Subramanyam as Donga Doctor
* Suman Setty as compounder to hero
* Telangana Shakuntala as Heroines Grandma
* Srinivasa Reddy as Tailor
* Uttej as Barber Ali as Dubai Broker
* Chalapathi Rao as Village sarpanch
* Kondavalasa Lakshmana Rao as Postman
* Ananth as Permanent Guest to Raju.
* Ravi prakash as Shivaji (Gayathris brother) jeeva as money lender/sweet shop owner
 

==Crew==
* Director: E V V Satyanarayana
* Screenplay: E V V Satyanarayana Koti
* Editor: Gautham Raju
* Dialogue: Veligonda Srinivas
* Cinematographer: V Jayaram

==Soundtrack==
 
{{Infobox album
| Name = Bendu Apparao RMP
| Type = Soundtrack
| Artist = Koti
| Released = 14 September 2009
| Label = Aditya Music
| Producer = Koti
}}
{{tracklist
| extra_column = Singer(s)
| writing_credits = yes
| title1 = Adire Adharama
| writer1 = Vanamali
| extra1 = Tippu, Harini
| title2 = Em Roopura Ori Nayana
| writer2 = Chandrabose
| extra2 = Tippu (singer)|Tippu, Harini
| title3 = Manchamesi Veturi
| Kalpana
| title4 = Nagadhi Naguni
| writer4 = Ramajogayya Sastry
| extra4 = Mano (singer)|Mano, Chorus
| title5 = Sarejaha
| writer5 = Anantha Sriram
| extra5 = Geetha Madhuri
| title6 = Sukumari Chinnadi Chandrabose
| extra6 = Karthik (singer)|Karthik, Nitya Santhoshini
}}

==See also==
* Telugu films of 2009

==References==
 

==External links==
*  
 

 
 
 
 