Perspective (film)
{{Infobox film
| name           = Perspective
| image          = 
| caption        = 
| director       =   B. P. Paquette  
| producer       =   Jason Ross Jallet B. P. Paquette
| writer         =   B. P. Paquette
| narrator       =
| starring       =   Stéphane Paquette Patricia Tedford Pandora Topp
| music          =   Daniel Bedard
| cinematography =   Ivan Gekoff
| editing        =   Ernest Riffe
| studio         =   Nortario Films
| distributor    =   Nortario Films
| released       =  
| runtime        =   
| country        =   Canada
| language       =   English
| budget         =
| gross          =
}}
Perspective is a 9-chapter episodic drama film from Canada written and directed by B. P. Paquette and starring Stéphane Paquette, Patricia Tedford, and Pandora Topp in a love triangle.              The first three of the nine chapters, titled, respectively, Chapter 1: Salt & Soda (2012), Chapter 2: Chris and Other Beards (2013), and Chapter 3: Hush, hsuH (2014), have been completed. 

==Production==
Subtitled Variations on a Love Triangle in 9 Chapters, and inspired by the renown documentary Up Series, Perspective is unique in that it is a feature-length fiction film in progress, and will continue to evolve until 2020. Every year a new chapter of the film will be presented exclusively at Cinéfest until the project is complete. Each chapter will run approximately 10 to 20 minutes, with the completed film expected to be approximately 90-120 minutes. The time lapse between chapters will be integrated into the drama.    

At the premiere of each additional chapter, the preceding chapter(s) are replayed. Thus far, the first three chapters, titled, respectively, Chapter 1: Salt & Soda (2012), and Chapter 2: Chris and Other Beards (2013), and Chapter 3: Hush, hsuH (2014), have been completed and presented. 

The film features music and sound design (the onscreen credit states "soundscapes") by Daniel Bédard, production design by Joseph Kabbach, and cinematography by Ivan Gekoff.

An accomplished educator, filmmaker B. P. Paquette is using Perspective as a teaching tool for his film production students in the Motion Picture Arts program at Thorneloe University (federated with Laurentian University). In this context, Perspective offers students a unique opportunity to work on, and observe first hand, a professional production.    

==Festival recognition==
The first three of the nine chapters that compose Perspective have each premiered at Cinefest in, respectively, 2012, 2013, and 2014.       

==Theatrical release==
Perspective will be released commercially in theatres across Canada when it is completed in 2020.

==See also==
* Antoine Doinel – In five films, French filmmaker François Truffaut followed the fictional life of Antoine Doinel (played by Jean-Pierre Léaud) - beginning in 1959, with following films for 1962, 1968, 1970 and 1979. 
* The Children of Golzow – a series of documentary films following the lives of several people from 1961 to 2007. British children since 1964, when they were seven years old.
* Boyhood (film)|Boyhood - a 2014 feature film by American filmmaker Richard Linklater that follows a fictional character, played by the same actor over a period of 12 years.
* Everyday (film)|Everyday - British film directed by Michael Winterbottom about five years in the lives of a prisoner and his family, filmed between 2007 and 2012.

==References==
 

 
 
 
 
 
 