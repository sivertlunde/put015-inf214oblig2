National Treasure: Book of Secrets
{{Infobox film name = National Treasure: Book of Secrets image = Book of secrets post.jpg caption = Theatrical release poster director = Jon Turteltaub producer = Jon Turteltaub Jerry Bruckheimer screenplay = Cormac Wibberley Marianne Wibberley story = Cormac Wibberley Ted Elliott starring = Nicolas Cage Diane Kruger Justin Bartha Jon Voight Harvey Keitel Ed Harris Bruce Greenwood Helen Mirren music = Trevor Rabin cinematography = John Schwartzman Amir Mokri editing = David Rennie studio = Walt Disney Pictures Jerry Bruckheimer Films  Junction Entertainment   Saturn Films distributor = Walt Disney Studios Motion Pictures released =   runtime = 124 minutes country = United States language = English budget = $130 million  gross = $457.4 million 
}} National Treasure and is the second part of the National Treasure (franchise)|National Treasure franchise. It was directed by Jon Turteltaub, produced by Jerry Bruckheimer and released by Walt Disney Pictures. The film stars Nicolas Cage, Diane Kruger, Justin Bartha, Jon Voight, Harvey Keitel, Ed Harris, Bruce Greenwood, and Helen Mirren.

It was stated in the first films commentary that there were no plans for a sequel, but due to the first films impressive box-office performance, earning $347.5 million worldwide, a sequel was given the go-ahead in 2005. It took 38 days of release for the sequel to out-gross the original.

The film premiered in New York City on December 13, 2007, and was first released in Korea and Taiwan on December 19, 2007. It was then released in Australia and the Middle East on December 20, 2007. The film opened in the United States, Canada, Japan, Spain, and Italy on December 21, 2007. It was released in Germany and The Netherlands on January 24, 2008, and in the United Kingdom and Denmark on February 8, 2008.

==Plot==
  assassinate Abraham South and tosses the diary pages into a fire after OLaughlen shoots him. OLaughlen manages to save a partial page from the fireplace before escaping. Thomas dying words to Charles are "the debt that all men pay".

142 years later in modern day, three years after the events of the first film, Benjamin Gates (Nicolas Cage), Thomas great-great-grandson, is retelling Thomas story at a Civilian Heroes conference. He and his father Patrick Gates (Jon Voight) are publicly accosted by Mitch Wilkinson (Ed Harris), a black market dealer who has come into possession of the Booth diary page, which appears to link Thomas to Booth and Lincolns assassination. Ben is determined to solve the puzzle on the page to restore Thomas legacy.

Meanwhile, Agent Peter Sadusky (Harvey Keitel) of the FBI finds it suspicious that a black market dealer like Mitch would suddenly come forward with a rare Civil War artifact. Ben meets up with his technology-savvy friend Riley Poole when his Ferrari just got impounded by the IRS because of an unexpected debt. Ben requests his ex-girlfriend Abigail Chase (Diane Kruger) to access the diary page, and after several hours of examination they discover the cipher on the diary page.
 scale model of the Statue of Liberty on the Île aux Cygnes by Édouard Laboulaye who was a Freemason. In Paris, the inscription points to the two Resolute desks, one of which is in Buckingham Palace, the other in the White House. Ben is unaware that Mitch is following in his footsteps, using an illegal clone of Patricks cell phone to intercept messages. In London, Ben discovers that Patrick has sent Abigail to help. They sneak into the Queens private living quarters and find that the Resolute desk acts as a puzzle box, revealing a hidden compartment with a centuries-old plank of wood with indecipherable hieroglyphics on it. Ben then photographs it with a traffic light camera before Mitch and his men steal it after a car chase through London. Back in Washington D.C, Patrick indicates the plank leads to Cíbola, the City of Gold. Reading a book about Native American history, tells how explorers tried to find Cíbola after its first discovery in 1527 but couldnt find it.
 President (Bruce Greenwood) himself, since its for Presidents eyes only. Ben asks Sadusky if he can get the Book for him, but Sadusky tells Ben that the only way to see it is if he gets elected President. Desperate to find evidence of Cibola, Ben sets his friends on a quest to kidnap the President.
 President Calvin Coolidge found the other plank in 1924 and destroyed it after taking a photo (which is still in the Book). He also ordered Gutzon Borglum to carve Mount Rushmore to hide the landmark indicated on the plank in order to protect Cíbola. As Ben and his team elude the FBI, he sends an image of the plank to Patrick to give to Emily to decode, unaware that Mitch has intercepted this. Mitch forces Emily to lie to Patrick about the inscriptions meaning, but gives him a non-verbal cue of this falsehood.

Ben, Patrick, Abigail and Riley arrive at Mount Rushmore, prepared to deal with Mitch, but find that Mitch has brought Emily along, using her as a hostage to ensure Ben follows the final clues on an old letter sent from Queen Victoria to Confederate general Albert Pike. Ben finds the secret entrance, and after avoiding several traps, the group arrives at Cíbola, the City of Gold. Mitch apologizes to Ben for damaging his family name. As they explore, they accidentally cause the cave to flood with water from the lake above. The only means of escape is through a drainage door that must be held open during the escape and closed afterwards, trapping the one controlling the door since it cant stay open by itself. Mitch initially forces Ben to sacrifice himself, while threatening to cut Abigails throat but after a surge of water, Mitch finds himself holding the door open. He requests that part of this discovery be in his name. Ben and his friends escape safely while Mitch stays behind and drowns.

Ben and his friends are met by Sadusky and taken to a nearby military base. Ben is cleared of kidnapping by the President himself who claims that Ben rescued him after the tunnel closed accidentally. Thomas Gates name is cleared and he is called a hero of the Civil War. Sometime later, Cíbolas discovery is announced with Ben, his team, and Mitch given credit for its discovery. Patrick and Emily, happily working together, eagerly take to the task of analyzing Cíbola once it is drained of water. Ben and Abigail rekindle their broken relationship. Riley arrives home to find that the President has given his Ferrari back. The only mystery remaining is what the President asked Ben to decipher on Page 47 of the Book, which Ben says was a life-changing message.

==Cast== Benjamin Franklin "Ben" Gates
* Justin Bartha as Riley Poole, Bens best friend
* Diane Kruger as Dr. Abigail Chase, Bens colleague and girlfriend
* Jon Voight as Patrick Henry Gates, Bens father
* Helen Mirren as Dr. Emily Appleton-Gates, Bens mother
* Ed Harris as Mitch Wilkinson, a black market dealer, and Bens nemesis 
* Harvey Keitel as FBI Special Agent Sadusky
* Armando Riesco as FBI Special Agent Hendricks
* Alicia Coppola as FBI Special Agent Spellman Albert Hall as Dr. Nichols President of the United States
* Ty Burrell as Connor, Abigails new boyfriend who works at the White House

Randy Travis makes a cameo appearance, playing himself. Small supporting parts are played by Joel Gretsch and Billy Unger as Gates ancestors Thomas Gates and Charles Carroll Gates; Christian Camargo as John Wilkes Booth; and Zachary Gordon as a boy who gets into a heated argument with Gates over a Lincoln conspiracy.

==Reception==

===Critical response===
National Treasure: Book of Secrets has received mixed reviews from critics.  , September Dawn and Transformers (film)|Transformers), but lost both categories to Eddie Murphy for Norbit.

The film was nominated for Best Movie at 2008 MTV Movie Awards, but lost to Transformers (film)|Transformers.

===Box office===
The film opened at number one with $16,739,339 on its first day,  and $44,783,772 its first weekend,    the third largest Christmas opening.  It reached the $100 million mark in eight days,  half the time it took the first film.  It stayed at number one for 17 days before dropping to number two,  and grossed $457,364,600 worldwide, making it the ninth highest grossing film of 2007.    It was also broadcast on STAR Movies and Fox Movies Premium. 

==Novelizations==
Disney Press published an official novelization of the screenplay titled National Treasure 2: Book of Secrets The Junior Novel on November 6, 2007.  Parts of the story in the novel version differ slightly from what was actually filmed, owing to changes being made in the screenplay prior to and during production. For example, in the novel, Ben and Abigail photograph the wooden plank found hidden in the Queens desk and leave it behind, with the car chase following. But in the movie, they take the plank with them on the chase.

Also published on the same day as the official novelization was a companion youth novel Changing Tides: A Gates Family Mystery by Catherine Hapka. Its story is set in England in the year 1612 and is the first in a series of planned historical novels about the Gates family. The epilogue from Changing Tides is included at the back of the National Treasure book.  The second youth novel by Hapka, Midnight Ride: A Gates Family Mystery, was published on March 8, 2008. 

==Home video==
National Treasure: Book of Secrets was released on DVD, Universal Media Disc|UMD, and Blu-ray Disc on May 20, 2008  (June 2, 2008 in the UK).   In the opening weekend, 3,178,631 DVD units were sold, bringing in $50,826,310 in revenue.  As of August 2009, 5,873,640 DVD units have been sold, generating revenue of $93,132,076.  This does not include Blu-ray Disc sales or DVD rentals. 

The film has been retitled National Treasure 2: Book of Secrets for all three releases. The films official website has also been changed accordingly.

A special edition, called the "National Treasure Presidential Edition", contains National Treasure and National Treasure 2: Book of Secrets inside a letter book which is a replica of the Presidents secret book from National Treasure 2: Book of Secrets. 

==Soundtrack==

The soundtrack to National Treasure: Book of Secrets was released on December 18, 2007.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 22:37 

| title1          = Page 47
| length1         = 2:39
| extra1          = Trevor Rabin

| title2          = Cibola
| length2         = 5:16
| extra2          = Trevor Rabin

| title3          = Spirit of Paris
| length3         = 2:20
| extra3          = Trevor Rabin

| title4          = City of Gold
| length4         = 2:13
| extra4          = Trevor Rabin

| title5          = So!
| length5         = 1:46
| extra5          = Trevor Rabin

| title6          = Bunnies
| length6         = 2:03
| extra6          = Trevor Rabin

| title7          = Gabby Shuffle
| length7         = 1:52
| extra7          = Trevor Rabin

| title8          = Franklins Tunnel
| length8         = 4:28
| extra8          = Trevor Rabin

}}

== Production ==
 
Many scenes of historic locations were filmed on location, including the scenes at Mount Vernon and Mount Rushmore.      Filming at Mount Rushmore took longer than initially scheduled, due to inclement weather and the decision to change the setting of additional scenes to the area around Mount Rushmore to take advantage of Black Hills backdrop. 

==Sequel== Disney has already registered the domains for NationalTreasure3DVD.com and NationalTreasure4DVD.com.  Though the second film ended with the question about page 47 of the Presidents book of secrets, Turteltaub responded in a press interview that the idea was not set in stone as the basis for National Treasure 3.

In October 2013, filmmaker Jon Turteltaub confirmed to Collider that the studio, himself, producer Jerry Bruckheimer and the actors all want to do the third film: "We want to do the movie, Disney wants to do the movie. Were just having the damnedest time writing it. Ill bet that within two years, well be shooting that movie. Id say were about half-way there. Its not only writing a great historical mystery, but weve gotta write something that has nothing to do with anything weve done before. The goal is to always have an original sequel, as silly as that sounds. We really want to make sure that the third one doesn’t just feel like a repeat of the first one, or one too many". 

== See also ==
* Abraham Lincoln assassination
* Playfair cipher

== References ==
 

== External links ==
 
*  
*   for the DVD and Blu-ray Disc
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 