The Base 2: Guilty as Charged
{{multiple issues|
 
 
}}
 adventure film The Base.

Tagline: The Army has one simple rule... kill or be killed.

==Plot==
Two dozen U.S. soldiers are missing without a trace and the President wants answers. Lt. John Murphy is sent undercover to find them. But what hes about to discover is an underground military court where those judged guilty by fraud. Despite, it didnt do anything wrong only serving up the country name are forced into a lethal sport of hunting - and they are the prey after they have killed the psychopathy Lieutenant cut down the Ear for the collection to at the black market betting system that has its tentacles in every U.S. army base, it will take more than surviving the game and bringing down the sadistic ringleader to stop the body count from rising.

==Cast==
* Antonio Sabato Jr.: Lt. John Murphy/Sgt. Hawks
* James Remar: Lt. Col. Strauss
* Duane Davis: Goose
* Yuji Okumoto: Davis
* Melissa Lewis: Lee
* Elijah Mahar: Willetts
* Emilio Rivera: Pvt. Alberto Ramirerz
* Johnny Urbon: Lt. Daniel Zach
* William Jones: Lt. Col. Joe Serano
* Deron McBee: Cletus
* Randy Mulkey: Col. Howard
* Gary Cervantes: Sgt. Hugo Ramirez (as Carlos Cervantes)
* Robert Crow: Gen. Gray
* Bob Rudd: Gen. Walsh
* Dean Hayman Mason: MP
* John Ashker: Soldier #1
* Danny Wynands: Soldier #2
* Christopher Caso: Hunted Soldier

 
 
 