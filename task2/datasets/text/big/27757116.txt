The Amorous Milkman
{{Infobox film
| name = The Amorous Milkman
| image = "The_Amorous_Milkman"_(1975).jpg 
| image_size  =
| caption = UK 1-sheet poster by Tom Chantrell
| director = Derren Nesbitt
| producer = Derren Nesbitt
| writer =  Derren Nesbitt
| narrator = 
| starring = Diana Dors Brendan Price Julie Ege
| music = Roger Webb
| cinematography = James Allen (as Jim Allen) Russell Lloyd
| studio  =  
| distributor =  
| released = January 1975	
| country = United Kingdom
| runtime = 86 minutes
| language = English
| budget = 
| preceded_by = 
| followed_by = 
}}
The Amorous Milkman is a 1975 British comedy film directed by Derren Nesbitt and starring Julie Ege, Diana Dors and Brendan Price.  The plot follows a young milkman who enjoys a number of adventures with bored women on his round. One version of the poster showed a self-satisfied cat licking its lips above the tagline, "If your pussy could only talk." 

==Cast==
* Julie Ege as Diana
* Diana Dors as Rita
* Brendan Price as Davey
* Alan Lake as Sandy
* Donna Reading as Janice
* Nancie Wait as Margo
* Bill Fraser as Gerald
* Roy Kinnear as Sergeant
* Ray Barrett as John
* Fred Emney as Magistrate
* Patrick Holt as Tom
* Anthony Sharp as Counsel
* Megs Jenkins as Iris
* Arnold Ridley as Cinema Attendant
* Sam Kydd as Wilf
* Janet Webb as Vera
* Hugo Keith-Johnston as Hippy in the Nightclub (uncredited)

==Critical reception==
The Radio Times wrote, "Brendan Price does his best to rattle his pintas with panache. The most significant thing about this bawdy trash is what it says about the state of the British film industry at the time - its sad that this was the only worthwhile work Diana Dors, Roy Kinnear and other talented actors could find";  while Sky Movies wrote, "much in the vulgar mode of dozens of Confessions of a Window Cleaner|Confessions, Adventures of a Taxi Driver|Adventures and Up Pompeii (film)|Up sex romps of the Seventies, this one-man project (actor Derren Nesbitt wrote, produced and directed it) is a touch above that level, if only because its girls are at least sexy and its veteran cast is full of names who have seen better films and better days." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 