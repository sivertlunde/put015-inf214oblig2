The Day the Fish Came Out
{{Infobox film
| name           = The Day the Fish Came Out
| image          = TheDaytheFishcamout.jpg
| image_size     =
| caption        = Cover of the films novelisation
| director       = Michael Cacoyannis
| producer       = Michael Cacoyannis
| writer         = Michael Cacoyannis
| narrator       =
| starring       = Tom Courtenay Colin Blakely Sam Wanamaker
| music          = Mikis Theodorakis
| cinematography = Walter Lassally
| editing        = Vassilis Syropoulos
| distributor    = Twentieth Century-Fox
| released       = October 2, 1967 (U.S.)
| runtime        = 109 min.
| country        = Greece / U.K. English
| budget         =$875,000 
| preceded_by    =
| followed_by    =
}}
 
 British comedy film directed and written by Michael Cacoyannis who also designed the films costumes. The film stars Tom Courtenay, Colin Blakely and Sam Wanamaker.

==Plot summary== actual incident; a B-52G Stratofortress collided with a KC-135 Stratotanker over Palomares, Almería|Palomares, Spain on January 17, 1966, and four B-28 FI 1.45-megaton-range nuclear bombs aboard the B-52 were briefly lost.  In a title sequence shot by Maurice Binder, a chorus of Spanish Flamenco dancers explains why the film has moved from Spain to Greece.
 Greek resort island of Karos is forever changed when atomic bombs are dropped there by a NATO plane rapidly losing power.  Life on the island is so bleak that the inhabitants stage a mass exodus on news that Denmark has opened Greenland to Greek emigration.  The pilots drop their payload - which includes atomic weapons, but also a mysterious package called simply "Container Q" - over land, because they are under orders not to drop at sea.  The hapless pilots bail out and reach the island with no equipment or means to contact their headquarters, and only their underwear.  Lacking resources, money to buy food (or pay for a long distance call to base) or even their clothes, the pilot and navigator of the lost bomber scour the island like vagabonds.  Unknown to the pilots, the Americans have already deployed their own operation - a team of agents disguised as resort developers.  The pilots are clueless as to the fact that American agents are also on the island searching for their cargo.

Meanwhile the island is suddenly filled with clamoring, hedonistic tourists who believe the developer is going to build the best resort in the area first.  Unknown to all of them, a poor farmer and his wife find Container Q and, presuming it holds some treasure, they try to open it.  Unsuccessful at first - because Container Q is virtually impregnable - the farmer eventually steals a device that sprays acid that will eat through anything.  Expecting gold, the farmer and his wife instead find strange-looking rocks.  The Americans are eventually led back to the farmers, but not before the panicking farmers ditch Container Q into the sea, and the rocks into a cistern that provides water to the rest of the island.  The contents of Q - presumably highly toxic - thus contaminate all the water used by the islands inhabitants.

By nightfall, as tourists revel, the waters surrounding Karos are dotted with the bodies of dead and dying fish.  The Americans sent to recover the lost payload of the stricken jet realize that they are too late.  The pilot and the navigator, having saved enough small change to call home, are shocked to be booted from the long distance phone in the post office by the American developers.  Too late, the pilots realize that the developers are Americans.  The revellers continue dancing wildly as a voice from a PA system vainly pleads for their attention, presumably to warn them of their imminent end.

==Cast==
*Tom Courtenay as The Navigator
*Colin Blakely as The Pilot
*Sam Wanamaker as Elias
*Candice Bergen as Electra Brown
*Ian Ogilvy as Peter
*Dimitris Nikolaidis as The Dentist
*Nicolas Alexios as Goatherd
*Patricia Burke as Mrs. Mavroyannis
*Paris Alexander as Fred Arthur Mitchell as Frank
*Marlena Carrer as Goatherds Wife
*Tom Klunis as Mr. French William Berger as Man in Bed
*Kostas Papakonstantinou as Manolios
*Dora Stratou as Travel Agent
*Alexander Lykourezos as Director of Tourism
*Tom Whitehead as Mike
*Walter Granecki as Base Commander
*Dimitris Ioakeimidis as Policeman
*James Connolly as Tourist
*Assi Dayan as Tourist
*Robert Killian as Tourist
*Derek Kulai as Tourist
*Alexis Mann as Tourist
*Raymond McWilliams as Tourist
*Michael Radford as Tourist
*Peter Robinson as Tourist
*Grigoris Stefanides as Tourist
*Peter Stratful as Tourist
*Kosta Timvios as Tourist
*Herbert Zeichner as Tourist

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 