Pokémon Apokélypse
 
{{Infobox film
| name = Pokémon Apokélypse
| image = Apokelypse-poster.jpg
| released =  
| director =  
| screenplay =  
| producer =  
| editing = Nicholas Porteous
| music = David Mesiha
| runtime = 2:50
| country = Canada
| language = English
}} anime series. Intended as a fan-response to "the common trend of dark and gritty reboots of popular franchises", the film was meant to give a mature spin on Pokémon and be in the same vein as work seen on parody website CollegeHumor. The creators initially intended to reveal the film at the Vancouver Anime Evolution convention, it was not shown due to technical difficulties. Instead on September 14, 2010, it revealed online via a teaser segment distributed through emails sent to various media outlets, which presented itself as a recording of a "secret movie trailer screening" for a film in development. The full film followed shortly thereafter, posted online on September 20.
 Rocket Industries. Grand Theft Auto and Dragon Ball Z, the producers were surprised by fan reaction to the material. Despite praises for the content, they have stated they have no intention to produce a full-fledged film.
 The Escapist and Game Informer also heavily praised the production, with the former lamenting that such a concept would never officially appear from Nintendo.

==Synopsis== trailer for Brock (Kial Giovanni (David Rocket Industries, Misty (Rebecca Strom). Deciding to keep them safe, Ash accepts the bribe, and Pikachu is nearly killed in the match.

Quickly regretting his actions, Ash revisits Giovanni and returns the bribe money, declaring himself out of the league. Enraged, Giovanni declares Ash and his friends dead, and sends his minions Jessie and James|Jessie, James (Julia Lawton and Gharret Patrick Paon) and Meowth to carry out the retribution. As a result, Oak is killed, Pikachu and Misty are assaulted, and Brock is tortured. With the help of Pikachu, Brock and Misty, Ash goes after Giovanni and aims to take down Rocket Industries, declaring he will not stop until he has "caught them all".

==Cast==
* Lee Majdoub as Ash Ketchum
* Ikue Ohtani as Pikachu (Voice) Brock
* Misty
* Jessie
* James
* Giovanni
* Richard Toews as Professor Oak
* Sahaj Malhotra as Nurse Joy
* Katherine Atkinson as Officer Jenny 
* Jason Lee Fraser as Team Rocket Grunt
* Tanner McColman as Team Rocket Grunt
* Taylor Enobuc James as Team Rocket Grunt
* Derek Cheng as Team Rocket Grunt

==Development and production== Grand Theft Auto, and Majdoub suggesting a film based upon Dragon Ball Z. After another member of the group suggested Pokémon as a possibility, they focused on that concept instead. Kial quickly wrote a script for the short film, while Majdoub and executive film producer Innes contributed additional material to flesh it out.  Originally intended to be a film in the vein of CollegeHumors parody productions, more scenes were added and visual effects improved due to Kials love for the series and his desire for the film to be "more and more". Planning of the film took four months, while filming and production took a year and a half. Varying crew sizes were used for the four different filming periods with the majority of filming occurring within one weekend.    Majdoub noted he was surprised at the number of people that came forth for auditions that were not actors, but fans of the series. 

The films tone was inspired by a discussion with editor Nicholas Porteous about a review of Ang Lees film Hulk (film)|Hulk, in which a producer theorized that the film was seen as a commercial failure "because it wasn’t dark and gritty, like Batman Begins". Natale found the idea that Begins succeeded solely due to its tone insulting to its director Christopher Nolan, and joked with Majdoub on the popularity of dark series reboots, pondering what series would be good for a live-action adaptation. Settling on Pokémon due to its undertones of animal rights abuse, Majdoub wanted to approach the film from a completely mature standpoint. Displeased with the current trend of video game to film adaptations, they wished to "see how far we could take it without being absolutely ridiculous".  A variety of locations were worked in to give the completed project a proper trailer feel. The title Pokémon Apokélypse was chosen as nod to Apocalypse Now, due to the similarity of the shooting ratios, with only ten percent of the footage actually used. Though several scenes were shot to add more content to the film, others using dialogue and one-liners from the anime series were purposefully shot in longer scenes then cropped down, in order to ensure the lines did not feel forced and to give a sense that "here is a world beyond what is shown in the trailer". 
 CGI aspects of the film, however any further production would be up to Natale. Natale in turn added that while he was grateful for the positive reviews, he had no intention to do further films based on the Pokémon franchise.   

==Critical reception==

===Initial reaction===
Reaction to the films preview segment was mixed.  . 

===Post-release===
In contrast, the full film has been well received by the media. GamesRadar praised the idea of the film, stating that if produced it would be "the best movie since Piranha 3D" and calling it "good enough for grown-ups".  In a further discussion via their Pokémon Monday cast, the three editors involved praised the film as "very well done". While one editor stated disdain for the appearance of the various Pokémon in the film, they agreed that in any live-action adaptation the creatures would "always look weird", and that for a film with a lack of budget they were done very well. They also praised the attention of detail, and praised David Quasts portrayal of Giovanni, as well as the presentation of recurring anime villains Jessie, James and Meowth. Editor Carolyn Gudmundson added that she was uncomfortable with the grittiness of the production, though praised it for presenting out the inherent problem of the concept behind the games and its similarity to dog fighting. 

The Escapist shared similar sentiments, calling the film "amazing" and stating their desire to see the mock-trailer expanded into a full film despite it being contrary to Nintendos vision of the series; "Why cant anything this cool ever come through official channels?"  Joystiq commented that it was "long, long overdue" and called the completed results delightful, questioning why such a project had not been properly attempted years prior.  Kotaku added that while the "leaked" segment hid the flaws in the films effects, it hurt the film by hiding the "hammy charm" of the completed project.  Though Game Informer criticized the original segment, they heavily praised the full film, describing it as "what the franchise could look like if Peter Jackson, Martin Scorsese, and Jerry Bruckheimer all teamed up".  Topless Robot stated that while the original preview had nearly sabotaged the final productions appeal, they called it "awesome" and described it as taking "Pokémon battles to its inevitable, cock-fighting end".  Cinemablend stated that while the content was out of place with Nintendos presentation of the series, they found the finished product still "pretty cool", further praising the effects and the approach taken towards the material. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 