Gertrud (film)
{{Infobox film
| name           = Gertrud
| image          = Gertrudposter.jpg
| caption        = Theatrical release poster
| director       = Carl Theodor Dreyer
| producer       = Jørgen Nielsen
| screenplay     = Carl Theodor Dreyer Grethe Risbjerg Thomsen (poems)
| based on       =  
| starring       = Nina Pens Rode Bendt Rothe Ebbe Rode Baard Owe Axel Strøbye
| music          = Jørgen Jersild
| cinematography = Henning Bendtsen
| editing        = Edith Schlüssel
| studio         = Palladium
| distributor    = Film-Centralen-Palladium
| released       =  
| runtime        = 116 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}} the 1906 play of the same name by Hjalmar Söderberg. The title role of Gertrud Kanning is played by Nina Pens Rode, with Bendt Rothe as her husband, Gustav Kanning, and Baard Owe as her lover, Erland Jansson. 

Gertrud was Dreyers final film. It is notable for its many long takes, which include a 9 minute, 56 second take of Gertrud and her ex-lover, Gabriel, talking about their pasts.

==Plot==
Gertrud, a former opera singer in Stockholm in the early 20th century, is married to the lawyer and politician Gustav Kanning. Gertrud tells her husband that he has become more in love with his career and status than with her. She also tells him that she has met another man who loves her more than anything else, and that she therefore prefers him to her husband and wants a divorce.

Gertrud meets her lover, the promising young pianist Erland Jansson, in a park. The two go to Janssons house. Gertrud tells him how devoted she is to him. In the evening Gustav goes to pick Gertrud up at the opera where she had said she would be, but cant find her. The next evening the Kannings attend a dinner party at the house of the poet Gabriel Lidman, with whom Gertrud has had a relationship in the past. Gertrud greets her friend Axel Nygren who attends the same party. Gustav confronts Gertrud about the opera, and demands one last night with her before the separation. Lidman tells Gertrud that he had met Jansson at a party where he had bragged about Gertrud as his latest conquest.

When Gertrud meets with Jansson the next day she tells him that she wants to go away with him and leave everything else behind. He tells her that he cannot, because he is expecting a child with another woman. Lidman makes an attempt to persuade Gertrud to leave with him instead, but without success; when Lidman and Gertrud were a couple, just like Kanning, he had valued his career above her. Kanning makes a last attempt to persuade Gertrud to stay with him, even allowing her to keep her lover at the same time. The attempt fails and Gertrud moves alone to Paris to study psychology.

Thirty years later, Gertrud, together with Nygren, looks back at her life. She says that love is the only thing that means anything in life. She is now alone because of her refusal to compromise on that position, but does not regret anything.

==Cast==
* Nina Pens Rode as Gertrud
* Bendt Rothe as Gustav Kanning
* Ebbe Rode as Gabriel Lidman
* Baard Owe as Erland Jansson
* Axel Strøbye as Axel Nygren
* Vera Gebuhr as Kannings Housekeeper
* Lars Knutzon as Student

==Production==
This was Dreyers last film and his first since Ordet in 1955. In the nine-year period between films he had attempted to make films based on Euripides Medea, William Faulkners Light in August, and wrote treatments based on Henrik Ibsens Brand (play)|Brand, August Strindbergs Damascus and Eugene ONeills Mourning Becomes Electra. He also worked on his long planned but never realized film about the life of Christ.  According to Carl Theodor Dreyer, he had considered adapting two Hjalmar Söderberg works in the 1940s, the 1905 novel Doctor Glas and the 1906 play Gertrud (play)|Gertrud. None of the projects were realised at the time. The Gertrud project was revived when Dreyer read a 1962 monograph by Sten Rein called Hjalmar Söderbergs Gertrud, which pointed out the original plays use of dialogue: how the story often is driven by trivial conversations and failures to communicate. This inspired Dreyer to make a film where speech is more important than images. Adapting the play into a screenplay, Dreyer chose to abridge the third act and added an epilogue.  The epilogue was inspired by the life of Maria von Platen, Söderbergs original inspiration for the Gertrud character.   
 Danmarks Radio for a television production. Exterior scenes were filmed in the Vallø Castle park.    Filming took three months, and editing three days.  The film was mostly made up of long takes of shots of two or more actors talking to each other and continued Dreyers devotion to the principles of kammerspiel. Over the years, Dreyers filming style had become more and more subdued and compared to the fast cutting in The Passion of Joan of Arc or the tracking shots in Vampyr, this film contained slowed down camera shots with restricted angles and an increased length of single takes. Wakeman. p. 272. 

==Reception==
The film premiered at Le Studio Médicis in Paris on 18 December 1964. The cinema equipment failed several times during the screening, the subtitles were of low quality and the reels were shown in the wrong order, prompting extremely negative reactions from the audience.    It was released in Denmark on 1 January 1965 through Film-Centralen-Palladium.  It was later screened at the Cannes Film festival, where it was booed. Wakeman. p. 272  It was later screened to a packed house at the 1965 Venice Film Festival, but more than half of the audience walked out during the film. Those who remained gave the film a standing ovation, causing Dreyer to become visibly moved. 

===Critical response===
From the outset the film divided both critics and audiences. Immediately following the Paris premiere at a Dreyer retrospective where it was booed the film was frequently referred to as a "disaster" in the press; after the Danish premiere the reception became more nuanced but still divided, and the film caused a big debate in Danish media.  

A critic wrote in  , in its social haranguing for female independence, and August Strindberg|Strindberg, in its difficulty in male and female understanding, lends itself admirably to Dreyers dry but penetrating style. Nina Pens Rode has the right luminous quality for the romantic, uncompromising Gertrud, while the men are acceptable if sometimes overindulgent in their roles."  In Esquire Magazine, Dwight Macdonald wrote that "Gertrud is a further reach, beyond mannerism into cinematic poverty and straightforward tedium. He just sets up his camera and photographs people talking to each other." An article in Cinéma65 wrote that "Dreyer has gone from serenity to senility...Not a film, but a two-hour study of sofas and pianos." In defense of Gertrud, Dreyer stated that "What I seek in my films...is a penetration to my actors profound thoughts by means of their most subtle expressions...This is what interests me above all, not the technique of cinema. Gertrud is a film that I made with my heart." 

Jean-Luc Godard rated the film number one in his list of the best films of 1964. As well, Cahiers du cinéma voted it the second-best of 1964, beaten only by Band of Outsiders.  Andrew Sarris rated it the second-best of 1966, only beaten by Blowup.  Tom Milne called it "the kind of majestic, necromantic masterpiece that few artists achieve even once in a lifetime." Penelope Houston called it "an enigmatically modern film with the deceptive air of a staidly old fashioned one... This is a kind of distillation, at once contemplative and compulsive." Jean Sémolué said that "Of all Dreyers works, it is the most inward and thus the culmination, if not the crown, of his aesthetic." 

In the 2012 edition of Sight & Sounds poll of film critics, conducted every ten years to gauge critical opinion about the greatest films of all-time, Gertrud tied for 43rd place. 

===Accolades=== FIPRESCI prize Best Foreign Language Film at the 38th Academy Awards, but was not accepted as a nominee. 

==See also==
 
* 1964 in film
* Cinema of Denmark
* List of submissions to the 38th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
;Notes
 
;Bibliography
*  

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 