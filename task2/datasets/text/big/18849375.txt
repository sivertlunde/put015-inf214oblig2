Thaikkupin Tharam
{{Infobox film
| name = Thaikkupin Tharam   தாய்க்குப்பின் தாரம்
| image = Thaikkupin Tharam Poster.jpg
| caption = Film poster
| director = M. A. Thirumugam
| producer = Sandow M. M. A. Chinnappa Thevar
| writer = Kannadasan S. Ayyapillai 
| starring = M. G. Ramachandran Bhanumathi Ramakrishna|P. Bhanumathi P. Kannamba T. S. Balaiah Kaka Radhakrsihnan
| music = K. V. Mahadevan
| cinematography = R. R. Chandran
| editing = M. A. Thirumugam M. A. Mariappan M. G. Balu Rao
| studio = Devar Films
| distributor = Devar Films
| released = 21 September 1956
| runtime = 157 mins
| country = India Tamil
| budget = 
}}

Thaaikkuppin Thaaram ( ) is a Tamil language film starring M. G. Ramachandran in the lead role. The film was released on 4 September 1956. It is most notable for Ramachandran (or MGR) using an older film as a flashback in the film. The story  was a simple rural tale of love and valour.

==Production==
The movie was directed by M. A. Thirumugam, the younger brother of Sandow M. M. A. Chinnappa Thevar. Thirumugam had already been as an editor while working for Jupiter Pictures, and Thaaikkuppin Thaaram was the first movie he directed. Devar took Thirumugam to famous directors like K. Ramnoth and L. V. Prasad and sought their blessings and guidance. The shooting commenced on 5 July 1955 in the sets put up at the Vauhini Studios, with Nagi Reddy cranking the camera for the first shot.

As the story needed an imposing bull, Devar searched far and wide for a fine specimen. His men came back with reports of a landowner in Periakulam having a majestic bull in his farm. Devar approached the landowner, and managed to buy the bull after much persuasion, paying Rs. 1000 for it.

==Plot==
Muthaiyan is the brave son of Ratnam Pillai and Meenakshi. They are landed gentry and are highly respected in the village for their noble qualities. Meenakshi’s brother Doraiswami, on the other hand, is despised by all for his arrogance, cruelty and dishonourable ways. The two families are not in speaking terms ever since Doraiswami tried to usurp Ratnam Pillai’s traditional rights at the temple festival. Doraiswami’s daughter Sivakami, however, is a good-natured girl who is in love with Muthaiyan. Muthaiyan too reciprocates her love and they are determined to surmount all hurdles and get married. When Doraiswami’s men capture Muthaiyan and keep him a prisoner on the pretext that he had hurled stones at Doraiswami’s prized bull when he had caught it grazing on his crops, Sivakami comes to his rescue.

Meanwhile accosting Doraiswami demanding his son’s release, Ratnam Pillai declares bravely that he would overpower the touted bull. But the bull gores him to death. In his dying breath, he elicits a promise from Meenakshi that she would ensure that their son sets right this slur on their honour. Muthaiyan’s mother makes him promise that he would not even think of Sivakami anymore. Sivakami’s father too has isolated her in house arrest and has started looking out for a suitable husband for her.

How Muthaiyan wins the hand of Sivakami after overpowering the mighty bull Senkodan and reforming his wily uncle forms rest of the tale.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || Muthaiyan
|-
| Bhanumathi Ramakrishna|P. Bhanumathi || Sivakami
|-
| P. Kannamba || Meenakshi
|-
| T. S. Balaiah || 
|-
| Kaka Radhakrishnan || 
|-
| E. R. Sahadevan || 
|-
| G. Sakunthala || 
|-
| K. Rathinam || 
|-
| Surabhi Balasaraswathi || 
|}

==Crew==
*Producer: Sandow M. M. A. Chinnappa Thevar 
*Production Company: Devar Films
*Director: M. A. Thirumugam
*Music: K. V. Mahadevan
*Lyrics: Thanjai N. Ramaiah Dass, A. Maruthakasi & Kavi Lakshmanadas 
*Story: Kannadasan & S. Ayyapillai 
*Dialogue: Kannadasan & S. Ayyapillai
*Art Director: C. Raghavan
*Editing: M. A. Thirumugam, M. A. Mariappan & M. G. Balurao.
*Choreograph: T. C. Thangaraj
*Cinematograph: R. R. Chandran
*Stunt: R. N. Nambiar
*Dance: Sai Subbulakshmi

==Soundtrack== Playback singers are T. M. Soundararajan, A. M. Rajah, S. C. Krishnan, M. L. Vasanthakumari, Jikki & A. G. Rathnamala.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Manushanai Manushan Saapiduraandaa || T. M. Soundararajan || || 04:18
|- 
| 2 || Asaindhaadum Thendrale Thoodhu Sellaayo || P. Bhanumathi || || 03:13
|- 
| 3 || Vittadhadi Aasai.... Eravuttu Eni Edukkum || S. C. Krishnan & A. G. Rathnamala || || 03:18
|- 
| 4 || Kaadhal Viyaadhi Polladhadhu || Jikki || || 02:41
|- 
| 5 || Aahaa Nam Aasai Niraiverumaa || T. M. Soundararajan & P. Bhanumathi || || 03:02
|- 
| 6 || Naadu Sezhitthida Naalum Uzhaitthida || M. L. Vasanthakumari || || 04:54
|- 
| 7 || Unnaamal Urangaamal Uyirodu Mandraadi || T. M. Soundararajan || || 
|- 
| 8 || Thandhaavaram Thandhaaluvaan Thiruchendhooril Vaazhvone Vandhaaluvaan || S. C. Krishnan, A. G. Rathnamala & chorus || || 04:35
|- 
| 9 || En Kaadhal Inbam Idhuthaana || A. M. Rajah & P. Bhanumathi || || 03:35
|- 
| 10 || Thandhayaipppol.... Annaiyum Pidhaavum Munnari Dheivam || T. M. Soundararajan || || 03:11
|}

==Trivia==
The movie was a runaway hit, and proved an auspicious launch vehicle for Devar Films.

After a brief misunderstanding with MGR immediately after Thaaikkuppin Thaaram, Devar went on produce 15 movies with MGR as the hero.

All the 16 movies that Devar produced with MGR as the hero had music by K. V. Mahadevan.

==External links==
*  
* 

==References==
 

 
 
 
 
 
 