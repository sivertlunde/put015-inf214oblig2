Hawalaat
{{Infobox film
 | name = Hawalaat
 | image = Hawalaatfilm.jpg
 | caption = 
 | director = Surendra Mohan
 | producer = Surendra Mohan
 | writer = 
 | dialogue =  Mandakini Amrish Puri Gulshan Grover Om Shivpuri Dalip Tahil
 | music = Anu Malik
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  17 June 1987 (India)
 | runtime = 140 min.
 | language = Hindi Rs 4 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1987 Hindi Indian feature directed by Mandakini and Amrish Puri.

==Plot==
 

==Cast==

*Shatrughan Sinha ...  Gullu Badshah / Sikander Ali Khan 
*Rishi Kapoor ...  Shyam 
*Mithun Chakraborty ...  Mangal dada 
*Padmini Kolhapure ...  Geeta 
*Anita Raj ...  Salma / Shamim Khan 
*Mandakini ...  Leela 
*Prem Chopra ...  Dr. Prem Pal 
*Amrish Puri ...  Seth Dharam Das 
*Gulshan Grover ...  Inspector Sharma 
*Seema Deo ...  Parvati 
*Mukri ...  Director (guest appearance) 
*Om Prakash ...  Raghu (old man, drug addict prisoner) 
*Om Shivpuri ...  Minister 
*Dalip Tahil ...  Sheikh Ibrahim 
*Rakesh Bedi ...  Hawaldar Handa 

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Hawalaat 

==External links==
*  

 
 
 
 

 