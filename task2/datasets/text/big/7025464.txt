Like It Is (film)
{{Infobox film
| name           = Like It Is
| image          = Like-it-is-by-paul-oremland.jpg 
| caption        = Theatrical release poster
| director       = Paul Oremland
| producer       = Tracey Gardiner Robert Gray Steve Bell   Ian Rose   Roger Daltrey   Dani Behr
| music          = Don McGlashan
| cinematography = Alistair Cameron
| editing        = Jan Langford
| distributor    = First Run Features (US)
| released       = April 17, 1998
| runtime        = 95 minutes
| country        = United Kingdom
| awards         = 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1998 Cinema British gay-themed Steve Bell, Ian Rose, Roger Daltrey and Dani Behr.

==Plot summary==
 London music promoter who wants to one day own his own club. His flatmate is Paula (Behr), a pop singer whose music he helps promote. While accompanying her on a personal appearance at a gay club in Blackpool, Matt meets Craig (Bell), an unemployed youth who makes ends meet as a backroom bareknuckles fighter. They go back to Craigs place to have sex but its Craigs first time and he freaks out. Matt leaves his card and takes off.

Craig wins his next fight and the fight promoter plans to put him against an opponent Craig labels a maniac. Rather than fight, Craig goes to London and finds Matt. Matt invites him to stay, but Paulas not pleased about it. Also not pleased is Kelvin (Daltrey), Matts boss, who wants Matt to help put over his new pop group ZKC and doesnt want him distracted. Matt agrees to put the final touches on ZKCs music video in exchange for the opportunity to run Kelvins club when it re-opens.

Meanwhile, a friend of Matts offers Craig a staff job at his club. Unfortunately, on his first night he gets into a fight with one of the patrons. He calls Matt, who ditches a video editing session to retrieve Craig. Kelvin is livid, but invites Craig to a party at his house anyway.

The invitation is a pretext, however, to break up Matt and Craig. Kelvin offers Craig a job doing "market research" (meaning travelling around Northern England with several others, buying copies of Paulas and ZKCs new single to manipulate the pop charts) to keep him away from Matt for several weeks. He also aims Jamie, a member of ZKC, at Craig, who has a drunken one-night stand with him the night before he leaves for his job. While Craig is away, Matt learns about his fling with Jamie. Paula also acts to keep them apart, refusing to give Matt his messages from Craig.

Craig returns to London the night of a music awards show at which Paula and ZKC are scheduled to perform. Matt confronts Craig but forgives him, then has to calm down Paula, whos terrified of performing live, and Jamie. Kelvin tells Craig that he wont be needed any longer for "market research." Jamie and Matt have had a previous fling as well and, while high on cocaine, they start making out. Craig catches them and storms out. Matt catches up to him and Craig asks him to leave with him right then. Matt hesitates and Craig leaves.

Craig returns to Blackpool and agrees to take the fight. Matt, preparing for the club opening, decides to go to Blackpool after him. Kelvin fires him from the club and the record company. Matt meets Craigs brother Tony, who figures out the nature of their relationship. Tony, who just wants Craig to be happy, helps Matt find him, finally tracking him down at the fight. Craig loses badly, but hes bet all the money he made in London against himself and makes a big payday. Matt and Craig decide to give it another go.

==DVD release==
Like It Is was released on Region 1 DVD on August 7, 1999. It was re-released May 19, 2004, along with The Wolves of Kromer and Boyfriends (film)|Boyfriends in a box set entitled The Best of Gay Britain.

==External links==
*   at Internet Movie Database

 
 
 
 
 
 
 
 
 
 
 
 
 