Changing the Game (film)
{{Infobox film 
| name = Changing the Game 
| image = Changing_the_Game_Film_DVD_box.jpeg
| caption =
| director = Rel Dowdell
| producer =  Thomas L. Webster Alain Silver Karen L. Isaac
| writer = Rel Dowdell Aaron R. Astillero 
| starring = Sean Riggs Tony Todd Raw Leiba Sticky Fingaz Brandon Ruckdashel Irma P. Hall  
| music = 
| cinematography = Bob Demers
| editing = Nicholas Schwartz 
| distributor = Barnholtz Entertainment / Lionsgate
| released =  
| runtime = 103 Minutes
| country = United States
| language = English
| budget = 
}}
Changing the Game is a 2012 dramatic film starring Sean Riggs, Tony Todd, Raw Leiba, Sticky Fingaz, Brandon Ruckdashel and Irma P. Hall and directed by Rel Dowdell.   The film opened theatrically by AMC Theatres in New York, Philadelphia, Chicago, DC, Atlanta and was cited by the noted website FilmFresh.com as one of the top three African-American films of 2012.  The film recently premiered on May 15, 2014 on cable channel BET as the "Movie of the Week." 

==Plot==
Darrell, played by actor "Sean Riggs"; is a supremely intelligent African-American man whom rises from the tough streets of Philadelphia to the world of high finance on Wall Street. He soon learns the white-collar world is filled with as much crime as the drug-filled hood he left behind.

==Cast==
* Sean Riggs as "Darrell".  Curtis the Diabolical / FBI Agent Grandma Barnes, Darrells grandmother.
* Sticky Fingaz as Sticky Fingaz|Craig. Marty Levine.
* Dennis L.A. White as Dennis L.A. White|Dre.
* Suzzanne Douglas as Suzzanne Douglas|Mrs. Davis, Darrells teacher.
* Raw Leiba as Raw Leiba|Balu.
* Elizabeth Marie Camacho as Julissa, Sexy Flight Attendant.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 