The Kid from Cleveland
{{Infobox Film
| name           = The Kid from Cleveland
| image          = The Kid From Cleveland (1949) poster.jpg
| image_size     =
| caption        = 
| director       = Herbert Kline
| producer       = Walter Colmes John Bright
| starring       = George Brent Lynn Bari Russ Tamblyn Tommy Cook Ann Doran Louis Jean Heydt K. Elmo Lowe John Beradino
| music          = Nathan Scott
| cinematography = Jack Marta
| editing        = Jason H. Bernie
| distributor    = Republic Pictures
| released       = September 5, 1949
| runtime        = 89 mins.
| country        = United States
| language       = English
}}
 sports drama film starring George Brent, Lynn Bari and Russ Tamblyn. Directed by Herbert Kline, the film was released by Republic Pictures.

==Plot== Baseball Hall-of-Famer Boston Braves players also appear in the film in archive baseball footage segments from the 1948 World Series.

==Cast==
*George Brent as Mike Jackson
*Lynn Bari as Katherine Jackson
*Russ Tamblyn as Johnny Barrows
*Tommy Cook as Dan "The Kid" Hudson
*Ann Doran as Emily Barrows Novak
*Louis Jean Heydt as Carl Novak
*K. Elmo Lowe as Dave Joyce/Jake Dawson
*John Beradino as Mac, the Fence

===Cleveland Indians in the cast===
as themselves:
*Bill Veeck (Owner & President)
*Lou Boudreau
*Tris Speaker
*Hank Greenberg
*Bob Feller
*Gene Bearden
*Satchel Paige
*Bob Lemon
*Steve Gromek
*Joe Gordon
*Mickey Vernon
*Ken Keltner
*Ray Boone Dale Mitchell
*Larry Doby
*Bob Kennedy
*Jim Hegan

===Uncredited Cleveland Indians in the cast===
(as themselves)
*Bobby Ávila
*Al Benton
*Allie Clark Mike Garcia
*Mel Harder
*Bill McKechnie
*Frank Papish
*Hal Peck
*Mike Tresh
*Thurman Tucker
*Lefty Weisman
*Early Wynn
*Sam Zoldak

===Baseball umpires (uncredited)===
*Bill Grieve Bill Summers

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 