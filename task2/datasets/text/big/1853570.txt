Mesa of Lost Women
{{Infobox film
| name           = Mesa of Lost Women
| image          = Mesalostwomen.jpg
| caption        = film poster
| director       = Ron Ormond Herbert Tevos
| producer       = Melvin Gordon William Perkins Joy N. Houck Francis White
| writer         = Herbert Tevos Orville H. Hampton
| narrator       = Lyle Talbot Richard Travis Robert Knapp
| music          = Hoyt S. Curtin
| cinematography = Karl Struss Gilbert Warrenton
| editing        = Ray H. Lockert Hugh Winn
| distributor    = Howco Productions
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
}}

Mesa of Lost Women is a 1953 American low-budget black-and-white science fiction film directed by Ron Ormond and Herbert Tevos from a screenplay by the latter and Orville H. Hampton.

==Plot==
 Hexapods are likely to survive longer that the humans.  The narrator then claims that when men or women venture off "the well beaten path of civilization" and deal with the unknown, the price of their survival is the loss of their sanity. 
 Robert Knapp) Muerto desert". Richard Travis), and Pepe. 

The film flash-backs to events occurring a year earlier in Zarpa Mesa. Famous scientist Leland Masterson (Harmon Stevens) arrives, having accepted an invitation Dr. Aranya (Jackie Coogan). Aranya (name derived from the Spanish araña for spider) has reportedly penned "brilliant" scientific treatises, and Masterson looks forward to meeting him in person. Masterson is genuinely intrigued by Aranyas theories, but his host informs Masterson that his work is not theoretical. He has already completed successful experiments, creating both human-sized tarantula spiders and human women with the abilities and instincts of spiders.  His creation Tarantella has regenerative abilities, sufficient to regrow severed limbs. He seriously expects her to have a lifespan of several centuries.  His experiments have had less success in male humans, who simply turn to disfigured Dwarfism|dwarfs. 

Masterson is horrified and denounces Aranya and his creations, proclaiming that they should be destroyed. In response, Aranya has him injected with a drug, turning him into a doddering simpleton. The front page of a newspaper called Southwest Journal explains that Masterson was eventually found wandering in the desert. He was declared insane and placed in an History of psychiatric institutions|asylum.  Some time later, Masterson escapes the "Muerto State Asylum". He is next seen two days later,  in an unnamed American town of the Mexico–United States border. Also present there are Tarantella, businessman Jan van Croft (Nico Lek), and his fiancée Doreen. They were heading to Mexico for their wedding day, but their private airplane had engine problems and stranded them there.  Jans servant Wu (Samuel Wu) is seen exchanging glances with Tarantella. It serves as the first sign that he is working with her. 

Masterson is tracked to the bar by his nurse at the asylum, George (George Barrows). The entire bar and its patrons observe Tarantella perform an energetic dance. Masterson apparently recognizes her, pulls a handgun, and shoots her. He then takes Jan, Doreen, and George hostage. He heads for Jans private airplane and he forces pilot Grant to prepare for takeoff, despite the pilots protests that only one engine is fully functional. The airplane departs with Doreen, George, Grant, Jan, Masterson, and Wu aboard it.  Meanwhile, Tarantella regenerates following her apparent death, and leaves the bar. 

In mid-flight, Grant discovers that someone sabotaged the gyrocompass. Resulting in them flying towards the wrong direction for most of the flight. Wus facial expression allows the audience to learn who was the saboteur. The airplane Emergency landing|crash-lands atop Zarpa Mesa, where the creations of Aranya were expecting them. For a while being, the creations simply observe them from afar.  The film follows the activities of the stranded group for quite a while. There is sexual tension between Grant and Doreen, culminating in a passionate kiss. Meanwhile, the group dwindles with the deaths of first George, secondly Wu, and lastly Jan. Wu is confirmed to have served as an agent of Aranya, but one who outlived his usefulness. 

The last three members of the group are then captured. Grant soon recognizes that their captors name is identical to the Spanish term for "spider", "araña". Aranya cures Masterson from drug-induced imbecility, hoping to recruit him. This backfires as Masterson uses his intellect in a suicide attack. He allows Doreen and Grant to escape, then sets up an explosion which kills himself and everyone else.  The flashback ends and we return to the hospital. He fails to convince anyone but Pepe of the truth in his story. Yet the finale reveals that at least one of Aranyas spider-women has survived. 

==Criticism==
Mesa of Lost Women takes the theme of a rogue scientist creating humans from animals, but adds several twists. Confusingly, the screenplay is structured as a double flashback. There are enough loose threads and holes in the plot that the film frustrates some viewers.
 Jail Bait.

==Reshooting==
The film was originally made by Pergor Productions under the title Tarantula and was viewed and granted a Motion Picture Production Code seal in October 1951. When the producers had difficulty in securing a distributor, Howco bought the film in the spring of 1952 and assigned Ron Ormond to direct additional footage for the film. 

Tandra Quinn recalled Ormond took over several months after Tevos completed filming and shot additional sequences  including new ones with Jackie Coogan and her character being shot which was not in the original version.

Katherine Victor remembered that she was hired by Ormond as the original film was not able to be picked up for distribution with her desert sequences added to the film. 

==Notes==

The film incorrectly applies the term insects to spiders. 

That Jans private plane got stranded in a border town is initially treated as a chance event, and so is his initial meeting with Masterson. Yet Wu seems to have pre-arranged several events, which depend on these apparent coincidences. This apparent contradiction is never fully explained. 

One of the dwarfs of the film was Angelo Rossitto, whose film career had started in the 1920s. He was a veteran of Poverty Row horror films. Johnson (1996), p. 261 

Director Herbert Tevos reportedly claimed to have had a film career in his native Germany, and to have directed films starring Marlene Dietrich and Erich von Stroheim. He even claimed credit for supposedly directing the film The Blue Angel (1930). Actually Mesa was his only known film credit. Weaver (2009), p. 212-231 
 Jail Bait (1954).  Craig (2009), p. 69-82  The narrator Lyle Talbot also appeared in various films by Ed Wood, such as Plan 9 from Outer Space (1959). One of the various spider-women of Aranya is played by Dolores Fuller, who also appeared in Woods films.  The film also features the film debut of Katherine Victor, as the spider-woman who first drove Masterson to the desert. She would become better known as a regular in Jerry Warren films. Weaver (2000), p. 385-386 

While Tarantella is one of the key characters of the film, this was a silent part for actress Tandra Quinn. She also had a silent part in The Neanderthal Man (1953), playing a deaf-mute. Decades later, Quinn recalled that she never received "a decent speaking part" in a film.  She reportedly chose her stage name by modifying one suggested by Tevos. He had suggested the stage name Tandra Nova. She agreed to the first name "Tandra", but found the last name unsuitable and reminiscent of Lou Nova. She instead chose the last name "Quinn" in honor of dancer Joan Quinn. 
 World Without Red Rock Canyon State Park. Johnson (1996), p. 357 

Hijacked flashbacks — There is a complicated layered flashback structure. The story starts as if being told by Grant, but shifts to Pepe, the oil companys jeep driver, and returns to Grant. Pepes flashback is not a personal account but a collective account of what his people have heard of Dr. Aranya. All that is back story for the cantina scene where the Americans pick up the story.

Role of Tevos — Herbert Tevos is credited for the screenplay. He is not credited for any other work. Tevos is said to have started filming a project for Howco Productions tentatively entitled "Tarantula", doing the directing. It has been said that the project was halted because Tevos was too difficult to work with, though there is scant evidence. Howco later had director Ron Ormond pick up the project, adding footage to finish the project. Its been said that the Dr. Aranya footage is what Ormond added, though this is difficult to confirm. Aranya is so pivotal to the plot, that he must have been in the original screenplay and not invented later. Tevos had an artistic vision, but perhaps was too inexperienced to get that vision onto film. Ormond didnt help much, but the project may have been too far along, or too little budgeted to fix.

Missing Heat — The promotional posters imply salacious elements the movie does not contain. This may be, in part, original plot elements that were edited out or not completed. What may be a surviving trace of this is the cantina scene. The cantina folk seem to calmly accept Tarantellas bizarre dance. Perhaps in the original conception she danced to lure men to Zarpa for Aranya experiments.

Good vs. Evil — Tarantella is Aranyas sensual creation. The other spider women are stoic. She represents the dark, animal, female side. Masterson, in his drug-induced derangement, proclaims Tarantella to be evil. He quotes from the Old Testament (2 Kings 9:33) about the death of evil queen Jezebel. Masterson also pronounces Doreen to be "good." Doreen, with shorter blond hair and modest suit dress, represents the virtuous woman. Aranya himself represents the dark side of science, while Masterson represents the moral and heroic side.

==See also==
* List of films in the public domain

==Sources==
*  
*  
*  
*  
*  
*  

==References==
 

==External links==
 
*  
*  
*   complete film on YouTube

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 