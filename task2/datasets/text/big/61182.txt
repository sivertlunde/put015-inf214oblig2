The Broadway Melody
 
{{Infobox film
| name            = The Broadway Melody
| image           = File:Broadway Melody poster.jpg
| image_size      = 225px
| caption         = theatrical release poster
| director        = Harry Beaumont
| producer        = Irving Thalberg Lawrence Weingarten
| story           = Edmund Goulding
| writer          = Sarah Y. Mason (continuity) Norman Houston (dialogue) James Gleason (dialogue) Earl Baldwin (titles) Charles King Anita Page Bessie Love Jed Prouty
| music           = Nacio Herb Brown George M. Cohan Willard Robison
| cinematography  = John Arnold
| editing         = Sam S. Zimbalist William LeVanway
| distributor     = Metro-Goldwyn-Mayer
| released        =  
| runtime         = 100 minutes
| language        = English
| country         = United States
| budget          = 
| gross           = $3 million 
}}
 Best Picture. lost and only a black and white copy survives in the complete film. The film was the first musical released by Metro-Goldwyn-Mayer and was Hollywoods first all-talking musical.
 You Were Meant For Me". The George M. Cohan classic "Give My Regards To Broadway" is used under the opening establishing shots of New York City, its film debut. Bessie Love was nominated for an Academy Award for Best Actress for her performance.

==Plot== Charles King plays the song-and-dance man whose affection for one sister (Harriet alias Hank) is supplanted by his growing love for the younger, more beautiful sister (Queenie). Queenie tries to protect her sister and derail the love triangle by dating a wealthy but unscrupulous "stage door Johnny."

The movie opens with Eddie Kearns debuting "The Broadway Melody." He tells some chorus girls that he brought the Mahoney Sisters to New York to perform it with him in Francis Zanfields latest revue. Hank and Queenie Mahoney are awaiting Eddies arrival at their apartment. Hank, the older sister, prides herself on her business sense and talent while Queenie is lauded for her beauty. Hank is confident they will make it big while Queenie is less eager to put everything on the line to be stars. Hank declines their Uncle Jeds offer to join a 30-week traveling show, but consents think it over.

Eddie, who is engaged to Hank, arrives and sees Queenie for the first time since she was a girl and is instantly taken with her. He tells them to come to rehearsal for Zanfields revue to present their act. Zanfield isnt interested in it but says he might have a use for Queenie, who begs him to give Hank a part as well. She also convinces him to pretend Hanks business skills won him over. Eddie witnesses this exchange and becomes even more enamored of Queenie for her devotion to her sister. During dress rehearsal for the revue Zanfield says the pacing is too slow for "The Broadway Melody" and cuts Hank and Queenie from the number. Meanwhile, another girl is injured after falling off a prop and Queenie is selected to replace her. Nearly everyone is captivated by Queenie, particularly notorious playboy Jacques "Jock" Warriner. While Jock begins to woo Queenie, Hank is upset that Queenie is building her success on her looks rather than her talent.

Over the following weeks, Queenie spends a lot of time with Jock, of which Hank and Eddie fervently disapprove. They forbid her to see him, which results in Queenie pushing them away and deterioration of the relationship between the sisters. Queenie is only with Jock to fight growing feelings for Eddie, but Hank thinks shes setting herself up to be hurt. Eventually, Eddie and Queenie confess their love for each other but Queenie, unwilling to break her sisters heart, runs off to Jock once again.

Hank, after witnessing Queenies fierce outburst toward Eddie and his devastated reaction to it, finally realizes they are in love. She berates Eddie for letting Queenie run away and tells him to go after her. She claims to never have loved him and that shed only been using him to advance her career. After he leaves she breaks down and alternates between sobs and hysterical laughter. She composes herself enough to call Uncle Jed to accept the job with the 30-week show.

Theres a raucous party at the apartment Jock had recently purchased for Queenie but he insists they spend time alone. When she resists his advances he says its the least she could do after all hes done for her. He begins to get physical but Eddie bursts in and attempts to fight Jock, who knocks him through the door with one punch. Queenie runs to Eddie and leaves Jock and the party behind.

Sometime later, Hank and Uncle Jed await the arrival of Queenie and Eddie from their honeymoon. The relationship between the sisters is on the mend but there is obvious discomfort between Hank and Eddie. Queenie announces shes through with show business and will settle down in their new house on Long Island. She insists that Hank lives with them when her job is over. After Hank leaves with her new partner and Uncle Jed, Queenie laments the fact that her sister hasnt found the happiness she deserves. The final scene is of a distraught Hank on her way to the train station.

==Cast==
  
*Anita Page as Queenie Mahoney
*Bessie Love as Hank Mahoney Charles King as Eddie Kearns
*Jed Prouty as Uncle Jed Kenneth Thomson as Jock Edward Dillon as Stage manager
 
*Mary Doran as Blonde
*Eddie Kane as Zanfield
*J. Emmett Beck as Babe Hatrick
*Marshall Ruth as Stew
*Drew Demarest as Turpe
 

==Production== silent version On with Gold Diggers The Life of the Party (1930), and others. No known color prints of the sequence survive, only black-and-white.

==Musical numbers==
Music by Nacio Herb Brown, lyrics by Arthur Freed, except as noted.  
*"Broadway Melody"
*"Love Boat" You Were Meant For Me"
*"Wedding of the Painted Doll"
*"Boy Friend"
*"Truthful Deacon Brown", music and lyrics by Willard Robison
*"Lovely Lady"
   

==Reception and legacy==
The Broadway Melody was a substantial success and made a profit of $1.6 million for MGM. It was the top grossing picture of 1929, and won the Academy Award for Best Picture.  

Contemporary reviews from critics were generally positive. Variety (magazine)|Variety wrote that it "has everything a silent picture should have outside of its dialog. A basic story with some sense to it, action, excellent direction, laughs, a tear, a couple of great performances and plenty of sex."  "Has everything," agreed Film Daily. "Sure-fire moneymaker that will drag em in everywhere." 

"This picture is great. It will revolutionize the talkies," wrote Edwin Schallert for Motion Picture News. "The direction is an amazing indication of what can be done in the new medium." 

Mordaunt Hall of The New York Times wrote a mixed review, calling it "rather cleverly directed" though "somewhat obvious," with sentiment "served out too generously in most of the sequences." Hall called Kings performance "vigorous", but of Page he wrote, "Her acting, especially her voice, does not enhance her personality. Notwithstanding, it must be admitted that there are girls who talk as she is made to for the screen. Miss Page, however, fails to give one an impression of spontaneity, for she recites rather than speaks her lines." 
 John Mosher of The New Yorker wrote, "The stage background allows opportunity for one or two musical interpolations, and no one is more glad than we that the talkies charmingly succeed in a very pleasant ballet. Because of that we shall try to forget the dialogue of the play, and that James Gleason ever wanted to take any credit for it." 

Historically, The Broadway Melody is often considered the first complete example of the Hollywood musical. While the film was seen as innovation for its time and helped to usher in the concept and structure of musical films, the film has become regarded by contemporary critics as cliché-ridden and overly melodramatic.  It currently holds a rating of 35% at Rotten Tomatoes based on 17 reviews.  Assessing the film in 2009, James Berardinelli wrote, "The Broadway Melody has not stood the test of time in ways that many of its more artistic contemporaries have. Some of its deficiencies can be attributed to ways in which the genre has been re-shaped and improved over the years, but some are the result of the studios validated belief that viewers would be willing to ignore bad acting and pedestrian directing in order to experience singing, dancing, and talking on the silver screen." 

==Sequels==
Three more movies were later made by MGM with similar titles, Broadway Melody of 1936, Broadway Melody of 1938, and Broadway Melody of 1940, were released by MGM. Although not direct sequels in the traditional sense, they all had the same basic premise of a group of people putting on a show (the films also had recurring cast members playing different roles, most notably dancer Eleanor Powell who appeared in all three).

The original movie was also remade in 1940 as Two Girls on Broadway. Another Broadway Melody film was planned for 1942 (starring Gene Kelly and Eleanor Powell) but production was cancelled at the last minute. Broadway Rhythm, a 1944 musical by MGM, was originally to have been titled Broadway Melody of 1944.

==Awards and honors==
===Academy Awards===
Wins Best Picture

Nominations Best Actress – Bessie Love (lost to Mary Pickford for Coquette (film)|Coquette) Best Director – Harry Beaumont (lost to Frank Lloyd for The Divine Lady)

No nominations were announced prior to the 1930 ceremonies. Love and Beaumont are presumed to have been under consideration, and are listed as such by the Academy of Motion Picture Arts and Sciences.

==DVD release==
In 2005, The Broadway Melody was released on Region 1 special edition DVD by Warner Bros. It is also included in the 18-disc release Best Picture Oscar Collection, also released by Warner Bros. 

==See also==
*List of early color feature films

==References==
Notes
 

==External links==
*  
*  
*  
*  
*   at Virtual History

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 