Indrajaalam
{{Infobox film
| name           = Indrajaalam
| image          = Indrajaalam.gif
| image_size     = 
| alt            = 
| caption        = 
| director       = Thampi Kannanthanam
| producer       = Thampi Kannanthanam
| writer         = Dennis Joseph
| narrator       =  Geetha  Anupam Kher
| music          = S.P. Venkatesh
                   O N V Kurup (lyrics) 
| cinematography = Santosh Sivan
| editing        =  G. Murali  
| studio         = 
| distributor    = 
| released       = 1990
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Indrajaalam is a 1990 Malayalam film by Thambi Kannanthanam starring Mohanlal and Geetha (actress)|Geetha. 

The movie revolves round the much repeated theme of uprising of an underworld thug. Mohanlal plays the role of Kannan Nair, the protagonist. He finds his father Balan K Nairs killer at last and has revenge. Rajan P Devs villain character Carlos is perhaps his best performance on silver screen. The film was a huge hit in box office.

==Cast==
*Mohanlal ...  Kannan Nair
*Rajan P. Dev ...  Carlosey Vijayaraghavan ...  Thankappan
*Mohan Jose ...  Michael Geetha ...  Jayanthi
*Jose Prakash ...  Baba
*K.P.A.C. Sunny ...  Adv. Narayana Swamy
*Sathaar ...  Chandrakumar Kunchan ...  Appu Sainuddin ...  Kuttan
*Meenakumari ...  Mariyamma
* Sreeja ...  Vinu
*Anupam Kher ...  Maharashtra Chief Minister
*Balan K. Nair ...  Ayyappan Nair
*Prathapachandran ...  Baburaj
*Ravi Menon ...  Press Photographer

==External links==
*  

 
 
 
 
 


 
 