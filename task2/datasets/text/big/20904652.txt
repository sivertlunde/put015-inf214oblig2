Prem Vivah
{{Infobox film
| name           = Prem Vivah
| image          = PremVivahfilm.jpg
| image_size     = 
| caption        = 
| director       = Basu Chatterjee
| producer       = Ramraj Nahta
| writer         =
| screenplay     =
| story          =
| based on       =
| narrator       =
| starring       = Asha Parekh, Bindiya Goswami, Mithun Chakraborty, Utpal Dutt
| music          = Laxmikant-Pyarelal Anand Bakshi (lyrics)
| cinematography = A. K. Bir
| editing        = V. N. Mayekar
| studio         =
| distributor    =
| released       =  
| runtime        = 150 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1979 Bollywood|Hindi-language Indian feature directed by Basu Chatterjee and produced by Ramraj Nahta, starring Mithun Chakraborty, Bindiya Goswami, Utpal Dutt and Asha Parekh

==Plot==

Asha Parekh plays an unmarried woman in her 30s, past what society considered the marriageable age.  Her younger sister played by Bindiya Goswami has a fiancee Mithun Chakraborty.  She feels guilty that she is about to have a happy married life, while her older sister will be all alone.  She sets out to find a husband for her.  When she sees her sister admire Utpal Dutt on television, she brings him into their lives.  Unfortunately, Utpal Dutt falls for Bindiya Goswami and isnt aware that Asha Parekh has fallen in love with him.  How Bindiya untangles herself from this situation and gets Utpal Dutt and Asha Parekh together forms the rest of the story.

==Cast==

*Asha Parekh
*Bindiya Goswami
*Deven Verma
*Harish Bhimani Keshav Date
*Mithun Chakraborty Neelima
*Pinchoo Kapoor
*Tun Tun
*Utpal Dutt

==Crew==

*Director &ndash; Basu Chatterjee
*Producer &ndash; Ramraj Nahta
*Screenplay &ndash; Basu Chatterjee
*Cinematographer &ndash; A. K. Bir
*Editor &ndash; V. N. Mayekar
*Costumes Designer &ndash; Leena Daru, Mani Rabadi
*Choreographer &ndash; Oscar, Vijay
*Music Director &ndash; Laxmikant-Pyarelal
*Lyricist &ndash; Anand Bakshi Shailendra Singh

==Soundtrack==
{{Tracklisting
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Anand Bakshi
| all_music       = Laxmikant-Pyarelal

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Jinke Aane Se Pahle Sharm Aa Gayi
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Lata Mangeshkar
| length1         = 5:05

| title2          = Kaga Mera Ek Kaam Karna
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Lata Mangeshkar
| length2         = 3:20

| title3          = Milte Rahiye, Milne Se Dil Mil Jaayenge
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Kishore Kumar, Amit Kumar
| length3         = 3:20

| title4          = Prem Hai Jeevan, Prem Hai Yauvan
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Anuradha Paudwal, Shailendra Singh
| length4         = 3:30
}}

==External links==
*  

 
 
 
 
 