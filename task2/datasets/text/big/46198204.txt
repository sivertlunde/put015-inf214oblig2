Sunshine (1973 film)
{{Infobox television film
| bgcolour =  
| name = Sunshine
| image =  
| caption =  
| format = Docudrama 
| runtime = 121 minutes
| director = Joseph Sargent
| producer = George Eckstein
| writer = Carol Sobieski (teleplay)   Jacqueline Helton (see #Movie origins|below)
| starring = Cristina Raines   Cliff DeYoung   Lindsay and Sidney Greenbush
| editor = Buddy Small   Richard M. Sprague
| music = Hal Mooney 
| studio =  
| country =   English
| first_aired = November 9, 1973
| network = CBS
| released =  
}}
 CBS Friday Night Movie on November 9, 1972. At the time of its airing, Sunshine was the most watched made-for-television|made-for-TV movie in history.   

==Movie origins==

The movie was based on a true story. The movie was based on the life of Jacquelyn Helton (September 13, 1951&ndash;November 7, 1971). Helton was a woman diagnosed with osteosarcoma, a form of cancer in the bone marrow of the leg in 1969. Helton, her newlywed husband and their newborn daughter Jill moved to Denver, Colorado to seek medical treatment. Helton agreed to take part in a medical research program to tape and write her thoughts about having a baby and knowing that she is going to die. The thoughts she had written down were saved in journals and tape recordings that inspired the dialogue of the movie. Helton eventually died on November 7, 1971 at the age of 20. 

==Plot==

A young woman named Kate, (Cristina Raines), falls in love with a struggling musician named Sam Hayden, (Cliff DeYoung). The two get married and have a daughter named Jill, (played by various people, see #Cast|below). 

Shortly after Jill is born, Kate, now 20 at this point, gets the shocking news that she has osteosarcoma, a form of cancer that affects the bone marrow of the leg. After her diagnosis, Kate sees Dr. Wilde, (James Hong). Dr. Wilde gives her two options to treat her cancer. She can either get her leg amputated and hope the cancer does not spread or can pursue chemotherapeutic treatments. She decides against either. But she does take part in a medical research study in which she has to keep a recorded journal and in the journal, she was to tell what it was like to have a baby and expecting death at the same time.

Kate keeps a recorded journal but it is stolen one day by a little boy. Suddenly, her stories gain national attention. She gains another tape recorder until her death which comes towards the end of the movie.  

==Cast==

*Cristina Raines as Kate Hayden
*Cliff DeYoung as Sam Hayden
*Meg Foster as Nora
*Brenda Vaccaro as Carol Gillman
*Bill Mumy as Weaver
*Alan Fudge as David
*Corey Fischer as Givitz
*Lindsay and Sidney Greenbush as Jill Hayden
*Sarah Valentini as the infant Jill Hayden
*James Hong as Dr. Wilde
*Bill Stout as Interviewer
*Noble Willingham as Bartender
*Adrian Ricard as Nurse

==References==
  

==External links==
* 
* 

 
 
 
 