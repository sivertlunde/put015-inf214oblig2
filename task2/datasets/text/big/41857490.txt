Our Miss Fred
{{Infobox film
| name           = Our Miss Fred
| image          = "Our_Miss_Fred"_(1972).jpg
| image_size     = 
| caption        = UK poster
| director       = Bob Kellett
| producer       = Josephine Douglas
| writer         = Hugh Leonard Terence Feely
| story          = Ted Willis
| narrator       = 
| starring       = Danny La Rue Alfred Marks Lance Percival
| music          = Peter Greenwell
| cinematography = Dick Bush
| editing        = David Campling
| studio         = Anglo-EMI
| distributor    = Anglo-EMI
| released       = 1972
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| website        = 
| amg_id         = 
}}
Our Miss Fred is a 1972 British comedy film starring Danny La Rue and set during World War II.   The film was also known by its video release titles "Beyond the Call of Duty" (Canada) and "Operation: Fred" (US). In the 1960s, La Rue was one of the highest paid entertainers in Britain, but this represents his only starring role in a feature film.   

==Plot==
Shakespearean actor Fred Wimbush is called up during WWII, and is performing in drag, entertaining the troops in France, when the Nazis advance. Unless he continues his disguise in womens clothes, Fred fears he will be shot as a spy. The double entendres and bullets fly as he attempts his escape in the company of the pupils from an English girls finishing school.  

==Sample gag==
Given his experience as a (Shakespearean) actor, (Fred) ends up...working as an entertainer for the troops. And playing all the female parts. He’s not entirely happy... “Look at me, dressed like a bird,” he grumbles. “They used to come from miles away to see my Titus Andronicus.”  (Alastair Wallis: "Movies About Girls")  

==Cast==
*Fred Wimbush -	 Danny La Rue
*General Brincker -	 Alfred Marks
*Squadron Leader Smallpiece -	 Lance Percival
*Miss Flodden -	 Lally Bowers
*Miss Lockhart -	 Frances de la Tour
*Schmidt -	 Walter Gotell
*Hilary -	 Kristin Hatfield
*Judith -	 Jenny Twigge
*Prunella -	 Vanessa Furse
*Elvira -	 Serretta Wilson
*Emma -	 Sophie Batchelor
*Patron -	 John Barrard
*Patrons Wife -	 Nancy Nevinson
*Doctor -	 Cyril Shaps
*British Colonel -	 Frank Thornton
*French Resistance Fighter -	 André Maranne
*Bertie -	 Barrie Gosney
*Bobby -	 David Ellen
*Vendeuse -	 Toni Palmer
*Jeanette -	 Jennifer Croxton
*R.S.M. -	 Anthony Sagar
*Senior RAF officer -	 Noel Coleman
*M.C. -	 Peter Greenwell

==Critical reception==
*In "The Spinning image", Graeme Clark called the film, "a goodnatured comedy which, while you can see why La Rues prospects in cinema might have been limited, also proved he was no dead loss in front of the camera either."  David McGillivray wrote, "Danny La Rue, Britains most popular female impersonator during the 1970s, seems terribly constricted in his one major film, an old-fashioned wartime comedy written by distinguished playwright Hugh Leonard."  
*Psychotic Cinema wrote, "this is a fun movie with plenty of sexual innuendo jokes and a rousing rendition of the popular song Hitler Has Only Got One Ball."  
*Movies About Girls wrote of La Rue, "he actually comes across remarkably well on screen...It’s all terrifically entertaining... La Rue can’t hide the fact that he’s loving every minute of it. You wouldn’t want him to either, because each and every smirk and grin means you can’t help but enjoy yourself along with him."  

==References==
 

==External links==
*  at IMDB

 
 
 
 
 
 
 
 


 