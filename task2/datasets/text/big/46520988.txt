The Amazing Woman
{{Infobox film
| name           = The Amazing Woman
| image          =
| director       = John G. Adolfi        
| writer         =
| starring       = Ruth Clifford Edward Coxen
| cinematography = Fred G. Hartman
| editing        =
| distributor    = Republic Distributors
| released       = January 1920
| runtime        = 5 reels
| country        = USA
| language       = Silent..English titles
}}
The Amazing Woman is a 1920 silent film drama directed by John G. Adolfi and starring Ed Coxen and Ruth Clifford. It was released by the Republic Distributing Company.   

The film is extant and preserved by the Library of Congress.    


==Cast==
*Edward Coxen - Ralph Strong
*Ruth Clifford - Anitra Frane
*Andrew Robson - John Strong
*Richard Morris - Gaston Duval
*Mrs. Orlamonde - Anitras mother


==References==
 

==External links==
* 
* 

 
 
 
 

 