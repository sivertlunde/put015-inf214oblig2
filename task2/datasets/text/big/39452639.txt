Benazir (1964 film)
{{Infobox film
| name =Benazir
| image = 
| director =S.Khalil
| producer =Bimal Roy Productions
| writer =  Asit Sen
| original_music =Sachin Dev Burman
| music= Sachin Dev Burman
| distributor = 
| released =1964
| language =Hindi
| budget = 
| gross= 
| awards = 
}}

Benazir is a Hindi film released in 1964, Starring Ashok Kumar, Meena Kumari, Shashi Kapoor and Tanuja. Directed by S.Khalil and music by Sachin Dev Burman.

==Storyline==

Benazir, a Muslim family drama, revolves around the films key characters, Nawab, Anwar, Sahida and Benazir, among others. Nawab and Anwar are brothers who live in an upperclass neighborhood. Nawab is married and has a child. A bachelor, Anwar on seeing Sahida fells in lover with her and proposes marriage to her. Although married, Nawab has an affair with Benazir, and keeps her as his mistress. The story takes a twist when Shauket, a family friend, tells Nawab that Anwar frequently sees Benazir. Nawab is furious when one day he sees Benazir in Anwars arms. He is at a loss as to why his younger brother is seeing his mistress when he is already in love with Sahida.   

==Review==

Although Shashi Kapoor as Anwar and Tanuja as Shahid only make brief appearances, they have done justice to their roles as a beautiful young cheerful pair.      

The music of Bimal Roys Benazir scored by the legendary S. D. Burman is praise-worthy and a special mention should be made of the song, "Mil jaa re jaan-e-jaana".  

==Cast==
*Ashok Kumar - Nawab
*Meena Kumari - Benazir
*Shashi Kapoor - Anwar
*Tanuja - Shahida
*Nirupa Roy - Nawabs wife
*Durga Khote		 Asit Sen
*Tarun Bose - Shauket

== Soundtrack ==
{{Infobox album  
| Name        = Benazir
| Type        = Soundtrack
| Artist      = Sachin Dev Burman
| Cover       = 
| Released    = 1964 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = HMV now Sa Re Ga Ma|
| Producer    = Sachin Dev Burman
| Reviews     = 
| Last album  = Tere Ghar Ke Samne  (1963)
| This album  = Benazir  (1964) Kaise Kahoon   (1964) 
|}}

The soundtrack includes the following tracks, composed by Sachin Dev Burman and lyrics penned by Shakeel Badayuni.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#eeeeee" align="center"
! Song !! Singer (s)
|-
| Gam nahin gar zindagi veeran hai
| Asha Bhonsle
|-
| Main shola main barkha
| Lata Mangeshkar, Mohd. Rafi
|-
| Mil ja re jane jana
| Lata Mangeshkar
|-
| Dil mein ek jane tamanna ne
| Mohd. Rafi
|-
| Alvida jaane wafa meri manzil aa
| Lata Mangeshkar
|-
| Baharon ki mehfil suhani rahegi
| Lata Mangeshkar
|-
| Husn ki baharen liye aaye they sanam
| Lata Mangeshkar
|-
| Mubarak hai who dil jisko kisi se
| Lata Mangeshkar, Asha Bhonsle, Usha Mangeshkar
|-
| Ya gafoor rahim ya allah
|}

==References==

 

 
 
 


 