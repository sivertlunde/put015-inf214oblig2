Predestination (film)
{{Infobox film
| name           = Predestination
| image          = Predestination poster.jpg
| alt            =
| caption        = Australian poster
| film name      = Michael Spierig   Peter Spierig
| producer       = Paddy McDonald   Tim McGahan   Michael Spierig
| writer         = Michael Spierig   Peter Spierig
| based on       =  
| starring       = Ethan Hawke   Sarah Snook   Noah Taylor
| music          = Peter Spierig
| cinematography = Ben Nott
| editing        = Matt Villa
| studio         = Blacklab Entertainment   Screen Australia   Wolfhound Pictures
| distributor    = Signature Entertainment
| released       =  
| runtime        = 97 minutes 
| country        = Australia
| language       = English
| budget         =
| gross          = $3.4 million 
}}
 Michael and Peter Spierig. The film is based on the Robert A. Heinlein short story All You Zombies|"—All You Zombies—", and stars Ethan Hawke, Sarah Snook and Noah Taylor.

==Plot==
 

The film follows the story of All You Zombies, the short story it is based on, with few plot divergences.

The movie begins in medias res when an agent is about to accomplish a mission by disarming a bomb, before the bomb explodes and almost kills him, leaving extremely severe burnings, though he is able to finish his work, being then sent to rehabilitation for a new mission.

The following sequence focuses on a dialogue between a barman and a young man who writes newspaper articles, known by the pen name The Unmarried Mother. As part of a bet, he tells the barman his biography, revealing he was born a girl and was impregnated by a mysterious man at eighteen, just before discovering he is a hermaphrodite and needed an immediate sex change after pregnancy in order to survive. He continues telling that things got even worse when his baby was stolen and he was obliged to adapt to his new gender, ultimately finding a way to survive by becoming a writer.

The plot abruptly changes when the barman takes the young man to a basement and operates a time travel device in order to help the man in finding out who is the man who abandoned him as a woman and stole his baby.

== Cast ==
* Ethan Hawke as The Bartender/Temporal Agent
* Sarah Snook as The Unmarried Mother/Jane/John
* Noah Taylor as Mr. Robertson
* Madeleine West as Mrs. Stapleton
* Christopher Kirby as Agent Miles
* Freya Stafford as Alice
* Jim Knobeloch as Dr. Belfort
* Christopher Stollery as The Interviewer
* Tyler Coppin as Dr. Heinlein
* Rob Jenkins as Mr. Jones

==Production==

===Development===
On 14 May 2012, the Spierig brothers—who had already written a screenplay—were announced as the directors of Predestination.  Peter Spierig explained in August 2014 that they remained close to Robert A. Heinleins 1959 short story,    which was over 50 years old by the time that they undertook the adaptation, and did not examine the logic of the narrative overly carefully: "... so we   worked on the logic that if there was a way to pick apart the logic, over that time it would have been done by now. We kind of say, ‘let’s trust the short story and trust that logic’, so we stuck very closely to it."   

Hawke was selected for the lead role, while Wolfhound Pictures and Blacklab Entertainment would produce the film.    Hawke explained in November 2014 that he is a longtime fan of the science fiction genre, but prefers its human elements, rather than special effects:

 
Whether it’s Robert Heinlein, Kurt Vonnegut, Philip K Dick, HG Wells or whoever ... that kind of mind-bendy science-fiction where you can really attack themes in a new way. And when I read Predestination it was like: "What the fuck did I just read?!"  

Arclight Films had bought the international rights to the film,  and on 18 May 2012, Tiberius Film attained the German rights to the film from Arclight.  On 23 May 2012, Sony Pictures Worldwide Acquisitions acquired the American and some international rights to the film. 

On 5 September 2012, Screen Australia announced that it would finance the film as part of a Australian dollar|A$5.5 million (United States dollar|US$5.6 million) investment in three feature films. 

On 28 February 2013, Snook signed on to star in one of the films lead roles,    followed by Taylor, who joined the cast of the film on 13 May 2013. Also in 2013, Pinnacle Films secured the Australian and New Zealand distribution rights to the film.   

===Filming===
On 19 February 2013, pre-production was scheduled to begin on 25 February 2013, while shooting was scheduled to begin on 8 April 2013 in Melbourne, Australia, for a duration of six weeks.  By 13 May 2013, filming was underway. 

Filming predominantly occurred at the Docklands Studios Melbourne facility, located approximately   from the city of Melbourne’s Central Business District (CBD).  Scenes from the film were shot at the Abbotsford Convent, located in the inner-city Melbourne suburb of Abbotsford, Victoria|Abbotsford, and at the RMIT Design Hub. 

In regard to Snook, the brothers explained to the media that they always seek to cast a lesser known actor in their films—Michael Spierig later compared Snooks acting ability to that of fellow Australian actress Cate Blanchett. They also stated that they prefer to film in Australia, with its rebates and incentives, but will film in any geographical location. 

==Release==
On 5 February 2014, some images from the film were released,  and on 21 July 2014, the first Australian trailer for the film was released.  On September 25, another official trailer was released. 
 SXSW Film Festival in Austin, Texas, United States (US).    The film was then selected for the opening night gala of the Melbourne International Film Festival (MIFF), held at the Hamer Hall venue on 31 July 2014 in Melbourne, Australia. The MIFF promotional material described the film as a "distinctive blend of sci-fi, noir and crime fiction with a Bukowskian streak."    The Sydney, Australia, premiere of the film, which also featured a live Q&A session with the directors, occurred on 6 August 2014 at the Palace Verona cinema. 

The film went on general release in the United Kingdom on 13 February 2015.  Following the release of two trailers, and a seven-minute excerpt that was published on 3 December 2014, the film premiered on 9 January 2015 in the United States. 

==Reception==

===Critical response===
Predestination received generally positive reviews from critics. On Rotten Tomatoes, a Review aggregator, the film has a score of 84% based on 97 reviews with an average rating of 6.8 out of 10. The critical consensus states "Fun genre fare with uncommon intelligence, Predestination serves as a better-than-average sci-fi adventure -- and offers a starmaking turn from Sarah Snook."  The film also has a score of 69 out of 100 on Metacritic based on 28 critics, indicating "generally favorable reviews". 

In Variety (magazine)|Variety magazines review of the film, the writer presented an "entrancingly strange time-travel saga" that "succeeds in teasing the brain and touching the heart."  In anticipation of the MIFF opening nights screening, the Sydney Morning Heralds National Film Editor Karl Quinn highlighted Snooks performance, describing it as a "career-making role". In terms of the plot, Quinn states that it is "intriguing" even though it could "unravel at the slightest tug on a thread of loose logic." 

The lead character was variously described as transgender or intersex in different media articles.  Tara Judah and Cerise Howard of Triple Rs film criticism radio show "Platos Cave" raised concerns regarding gender in the film; however, they did not elaborate on their concerns.  Hawke told the UKs Guardian publication prior to the films UK release date that transgender issues are not the focal point of the film, but rather that the narrative is relevant to all people: "There’s something about Predestination that actually does get at identity, for me". 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award  (4th AACTA Awards|4th)   AACTA Award Best Film Paddy McDonald
| 
|- Tim McGahan
| 
|- Spierig brothers
| 
|- AACTA Award Best Direction
| 
|- AACTA Award Best Adapted Screenplay
| 
|- AACTA Award Best Actress Sarah Snook
| 
|- AACTA Award Best Cinematography Ben Nott
| 
|- AACTA Award Best Editing Matt Villa
| 
|- AACTA Award Best Original Music Score Peter Spierig
| 
|- AACTA Award Best Production Design Matthew Putland
| 
|- AACTA Award Best Costume Design Wendy Cork
| 
|- Toronto After Dark Film Festival Special Award for Best Sci-Fi Film
| 
|- Special Award for Best Screenplay Spierig brothers
| 
|- Audience Award for Best Feature Film
| 
|-
|}

==See also==
* List of time travel science fiction
* Predestination paradox
* Predestination paradoxes in film
* Time travel

== References ==
 

== External links ==
  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 