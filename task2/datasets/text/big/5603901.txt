Liberty (1929 film)
{{Infobox film
| name = Liberty
| image = L&H_Liberty_1929.jpg
| caption = Theatrical release poster
| director =  Leo McCarey
| producer = Hal Roach
| writer = Leo McCarey H.M. Walker
| starring = Stan Laurel Oliver Hardy
| music =
| cinematography = George Stevens
| editing = Richard C. Currier
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 20 min.
| language = Silent film English (Original intertitles)
| country = United States
| budget =
}}
Liberty  (aka Criminals at Large) is a 1929 short comedy film starring Laurel and Hardy as escaped convicts who, while trying to change pants, wind up on a skyscraper in construction. 

==Plot==
Stan and Ollie are prison escapees. In their haste to change into street clothes, they wind up wearing each others pants. A cop chases them to a construction site, where they escape by riding an elevator to the top floor of an unfinished building. Atop the girders, 20 stories in the air, they finally switch trousers, contend with a crab that has found its way into Ollies pants, and manage to nearly fall to their death a few dozen times. 

==Cast==
*Stan Laurel - Stan
*Oliver Hardy - Ollie Tom Kennedy - Construction Worker
*Sam Lufkin - Getaway Driver  James Finlayson - Store Keeper Jack Hill - Officer
*Harry Bernard 
*Jean Harlow
*Ed Brandenburg

==References==
{{Reflist|refs=
 {{Citation
 | first1    = William K.
 | last1     = Everson
 | title     = The Complete Films of Laurel & Hardy
 | publisher = Citadel Press
 | page      = 73–75
 | year      = 1967
 | isbn      = 0806501464
 | url       = http://books.google.com/books?id=PK9ZsmT9B5IC&pg=PA73
 | postscript= .
}} 
}}

== External links ==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 

 