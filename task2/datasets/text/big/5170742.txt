Satyakam
 
{{Infobox film
| name           = Satyakam
| image          =
| image_size     =
| caption        =
| director       = Hrishikesh Mukherjee
| producer       = Sher Jeng Singh Punchee
| writer         = Bimal Dutta (Screenplay) Rajinder Singh Bedi (Dialogue)  Narayan Sanyal (Story)
| narrator       =
| starring       = Dharmendra Ashok Kumar Sharmila Tagore Sanjeev Kumar
| music          = Laxmikant Pyarelal
| lyrics         = Kaifi Azmi
| cinematography = Jaywant Pathare
| editing        = Das Dhaimade
| distributor    = 175 minutes
| released       = 1969
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}
 Sanjeev Kumar, and Ashok Kumar. The music for this film is given by Laxmikant Pyarelal.

After the success of  ; dialogue writer:   and cameraman: Jaywant Pathare.

This is considered to be Dharmendras finest acting performance of his career.  In addition its director Hrishikesh Mukherjee names this film as his favourite film. This speaks volumes of the film considering that the renowned director has made several memorable movies like Anand (1971 film)|Anand, Bawarchi, Abhimaan (1973 film)|Abhimaan, Chupke Chupke and Khubsoorat|Khoobsurat.
 1971 Filmfare Best Dialogue Award for Rajinder Singh Bedi. The movie also won National Film Award For Best Feature Film in Hindi. 

==Plot summary==

The story begins in 1946, just a year before India’s independence. Peoples minds are filled with a genuine anticipation of positive change. For some like Satyapriya Acharya (Dharmendra)India’s forthcoming independence spells a paradigm shift towards a sympathetic-rationalism that would take India’s populace from rags to riches.  Satyapriyas conviction is guided by his ascetic grandfather Daddaji Satyasharan Acharya (Ashok Kumar)s world views, whose  pursuit of truth  has led to him living in isolation in a Gurukula studying religious philosophy and observing a variety of rigid rituals.

Armed with an engineering degree, Satyapriya ventures out to realize his dreams about building a new India but  encounters characters who share little of his ideals. During his first assignment he meets Ranjana (Sharmila Tagore) who is about to be sexually exploited by a debauched Prince, the employer of Satyapriya. Despite the obvious awareness that Ranjana loves him, Satyapriya hesitates in rescuing her, letting her become prey of the morally corrupt Prince. The incident shakes the moral foundation of Satyapriya who has betrayed his conscience and feelings. To redress the mounting guilt he marries Ranjana, but their lives are never same again. She bears a child whose paternity is never clearly established.

Later Satyapriya moves from one job to another as he is unable to make dishonest compromises. Satyapriya and Ranjana also have their share of marital conflicts. She tries to lead a normal life and longs to forget her past. Satyapriya is constantly reminded of his failure and appears to make up for it by increasing rigidity about applying his principles in real life. He ruthlessly follows a rationalist obsession to eliminate the difference between a fallible human being and infallible God. This drives him into egocentric dispositions at the expense of everybody around him, including himself.

Struggling professionally, he is struck by an incurable and fatal illness. In the end, hospitalised and unable to even speak, Satyapriya is pursued by an unscrupulous contractor seeking approval for a badly executed civil project, in lieu of which the contractor would give him substantial sum that would take care of Satyapriyas wife Ranjana and their child after his death. Satyapriya has no means to secure his familys future and in the very first compromise of his life, Satyapriya hands over the signed approval papers to his wife. Although Ranjana had suffered many hardships and is not entirely happy with Satyapriyas redder-than-rose approach to life, she does not want to see him falter at the end stage of his life. She tears apart the documents and finds him smiling at her. Although unable to speak, Satyapriya is clearly happy that he was able to convert at least one person to his idealist worldview.

On learning of Satyapriyas condition, his grandfather Daddaji comes visiting. He had earlier turned his back on Satyapriya for marrying a woman without his consent and according to him, of questionable background.  Well versed in religious philosophy, the grandfather offers words of wisdom to Satyapriya. He tells Satyapriya that being aware of ideas like impermanence of worldly life and the larger divine truth, Satyaprakash is morally equipped to confidently face death. After his passing, the grandfather makes some excuses about not feeling responsible for Satyapriyas widow and child, and is about to leave for his Gurukula. At that moment, Satyapriya and Ranjanas child (Sarika) publicly speaks out saying the real reason is that the grandfather is unsure of his, i.e. Satyapriya and Ranjanas childs paternity.

The grandfather is humbled by the fact that he who swore by fidelity to truth regardless of the consequences, could not practice it except in isolation of his Gurukula where he was not being tested. Yet his granddaughter-in-law could share this issue with her child and the child could speak about it in public even though it was uncomfortable and would translate into taunts and humiliation from rest of the world. The grandfather publicly acknowledges his failings that even though he has spent his whole life studying  religious scriptures and philosophical books as well as practising many rituals, he still had much to learn about the nature of truth. He departs for home with Ranjana and her child.

This plot was based on a Bengali novel of the same title, written by a renowned writer Narayan Sanyal.

This film was made in 1969. By this time, disillusionment with post-independence expectations had begun to take root across India. Unemployment, continual poverty and rampant corruption were severely undermining institutions all around. In a way, the film underlines a gradual disappearance of the followers of Universality (philosophy)|absolutism.

==Cast==
* Dharmendra   as  Satyapriya Sath Acharya 
* Ashok Kumar   as  Satyasharan Daddaji Acharya 
* Sharmila Tagore   as  Ranjana
* Sanjeev Kumar   as  Narendra Naren Sharma  David   as  Rustom
* Sarika   as  Kabul S. Archarya (credited as Baby Sarika)
* Tarun Bose   as  Mr. Ladia
* Asrani   as  Peter
* Dina Pathak   as  Harbhajans mother 
* Manmohan   as  Kunver Vikram Singh
* Robi Ghosh   as  Ananto Chatterjee

==Songs==
Music - Laxmikant-Pyarelal, Lyrics - Kaifi Azmi

* Abhi kya sunoge - Lata Mangeshkar
* Do din ki zindagi - Lata Mangeshkar
* Zindagi hai kya bolo - Kishore Kumar, Mukesh (singer)|Mukesh, Mahendra Kapoor

==References==
 

==External links==
*  
*   from "Hrishikesh Mukherjees best films" rediff.com, 28 August 2006. Retrieved 2 October 2009.
*  

 
 

 

 
 
 
 
 