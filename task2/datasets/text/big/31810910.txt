Suspense (1930 film)
{{Infobox film
| name           = Suspense
| image          = 
| image_size     = 
| caption        = 
| director       = Walter Summers
| producer       = Walter Summers
| writer         = Walter Summers   Patrick MacGil
| narrator       = 
| starring       = Mickey Brantford Cyril McLaglen Jack Raine Hay Petrie
| music          = Dallas Bower
| cinematography = Theodor Sparkuhl   Hal Young
| editing        = Walter Stokvis
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 4 July 1930
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Suspense is a 1930 British war film directed by Walter Summers and starring Mickey Brantford, Cyril McLaglen and Jack Raine.  During the First World War a British unit take up a new position in a trench unaware that the Germans are laying a mine underneath it. The battle effects were created under the supervision of Cliff Richardson at Elstree Studios which was owned by British International Pictures. 


==Cast==
* Mickey Brantford – Private Pettigrew
* Cyril McLaglen – Cyril McClusky
* Jack Raine – Captain Wilson
* Hay Petrie – Scruffy
* Fred Groves (actor)| Fred Groves – Private Lomax
* Percy Parsons – Private Brett
* Syd Crossley – Corporal Brown
* Hamilton Keene – Officer

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 