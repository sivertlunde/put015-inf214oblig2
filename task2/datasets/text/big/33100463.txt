The Curse of the Aztec Mummy
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Curse of the Aztec Mummy
| image          = The Curse of the Aztec Mummy poster.jpg
| image_size     = 
| caption        = Mexican release poster
| director       = Rafael Portillo
| producer       = Guillermo Calderón (producer)
| writer         = Guillermo Calderón Alfredo Salazar
| narrator       = 
| starring       = See below
| music          = Antonio Díaz Conde
| cinematography = Enrique Wallace
| editing        = Jorge Bustos José Li-ho
| studio         = Cinematográfica Calderón S.A.
| distributor    = Clasa-Mohme (1957 USA theatrical release)
| released       = 1957
| runtime        = 65 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Curse of the Aztec Mummy (Spanish: La maldición de la momia azteca) is a 1957 Mexican horror film directed by Rafael Portillo. It is the second film in the Aztec Mummy series which began with The Aztec Mummy (Spanish title:La Momia Azteca) which was released earlier that year.

== Plot==
 
The evil Dr. Krupp, once again trying to get possession of the Aztec princess Xochitls jewels, hypnotizes her current reincarnation, Flor, to get her to reveal the jewels location - Xochitls tomb. Confusion reigns as Krupp and his thugs are opposed by Flors lover, Dr. Almada, his assistant, and wrestling superhero, El Angel. Krupp finally meets his match, however, when he comes up against Popoca, the warrior mummy who guards Xochitls tomb. 

== Cast ==
*Ramón Gay as Dr. Eduardo Almada
*Rosa Arenas as Flor / Xochitl
*Crox Alvarado as Pinacate / El Ángel
*Luis Aceves Castañeda as Dr. Krupp Jorge Mondragón as Dr. Sepúlveda
*Arturo Martínez as Tierno
*Emma Roldán as María, the housekeeper
*Julián de Meriche as Comandante de Policía
*Salvador Lozano
*Jaime González Quiñones as Pepe Almada
*Ángel Di Stefani as Popoca, the mummy
*Jesús Murcielago Velázquez as El Murciélago
*Enrique Yáñez as Esbirro del Murciélago Guillermo Hernández as Esbirro del Murciélago
*Alberto Yáñez as Esbirro del Murciélago
*Firpo Segura as Esbirro del Murciélago
*Sergio Yañez as Esbirro del Murciélago
*Estela Inda as Aztec Chanteuse

==Release==
 
==Reception==
Critical reception for the film has been negative.

Horror DVDs.com on their review for the complete series gave the film a negative review calling it "the least satisfying entry in the series". The reviewer also criticized the introduction of the superhero El Ángel as "random". 

Leonard Maltin gave the film 1 1/2 / 4 calling it "Cheap" and "droning".   
==Other films in The Aztec Mummy series==
* La Momia Azteca ( )

* The Robot vs. The Aztec Mummy

* Wrestling Women vs. The Aztec Mummy
==References==
 

== External links ==
* 

* 

* 

*  at DB Cult Film Institute

* 

 
 
 
 
 
 
 
 


 
 