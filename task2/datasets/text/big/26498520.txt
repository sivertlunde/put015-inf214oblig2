Annarakkannanum Thannalayathu
 
{{Infobox film
| name           = Annarakkannanum Thannalayathu
| image          = Annarakannanum Thannalayathu.jpg
| alt            = 
| caption        = 
| director       = Prakash
| producer       = Bharatharatna Kalabhavan Mani
| writer         = 
| starring       = Kalabhavan Mani Jayashree Nakshathra Suraj Venjaramood
| music          = Nikhilprabha
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Annarakkannanum Thannalayathu is a 2010 Malayalam film directed by Prakash. Kalabhavan Mani, Suraj Venjaramood, Jayashree  and Nakshathra plays the lead roles in this film.It is produced by Kalabhavan Mani. It is a tribute to all "kudiuyan"s and   Chalakkukudiuyans.

In this film Kalabhavan Mani is doing a double role and also introduces two new heroines Nakshatra and Jayashree.

== Plot ==
Annarakkannanum Thannalayathu movie tells the story of Changampuzha Pavithran (Kalabhavan Mani) who is a do-gooder who hails from the land of the great poet. Pavithran is bound to be around if a soul finds itself in distress. A bachelor who dreams about meeting his lady love some day, Pavithrans life goes topsy-turvy when an astrologer predicts that he would get married to a widow who would transform his entire life.

== Cast ==
* Kalabhavan Mani as Changampuzha Pavithran
* Jayashree
* Nakshatra
* Suraj Venjaramood
* Bijukuttan
* K. P. A. C. Lalitha
* Jagathy Sreekumar as S. Gunasekharan
* Thilakan Abu Salim
* Indrans Janardhanan
* Geetha Vijayan
* Majeed 
* Ambika Mohan as Srimathi teacher

== External links ==
*  
*   at Oneindia.in

 
 
 
 
 

 