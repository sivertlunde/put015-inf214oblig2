Journey to Freedom
 
{{Infobox film
| name           = Journey to Freedom
| image          =
| caption        =
| director       = Robert C. Dertano
| producer       = Stephen C. Apostolof
| writer         = 
| starring       = 
| music          =
| cinematography = 
| editing        = 
| distributor    = Republic Pictures
| released       = 21 June 1957 
| runtime        = 60 min.
| country        = United States
| language       = English 
| budget         =
}}

Journey to Freedom is a 1957 American film.  The story follows a Bulgarian who escapes from behind the Iron Curtain through Istanbul, Paris and Toronto to seek freedom in Los Angeles, California, but is doggedly pursued by Communist agents.
 The Red Menace (1949) and Big Jim McLain (1952).

The film featured Tor Johnson, the Swedish wrestler best known for appearing in Edward D. Wood Jr.s movies.  It was shot in the legendary Sunset Gower Studios and picked up for distribution by Republic Pictures shortly before they suspended feature film production.

== Cast ==

* Jacques Scott as Stephan Raikin
* Geneviève Aumont as Nanette 
* George Graham as James Wright
* Morgan Lane as Nick Popov
* Eve Brent as Mary Raikin
* Peter Besbas as Pete
* Don Mcart as Louie
* Dan ODowd as Parisian Friend
* Tor Johnson as Giant Turk

== External links ==

*  

 
 
 
 
 


 