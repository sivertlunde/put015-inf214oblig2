Akihabara@Deep
 

{{Infobox animanga/Header
| name            = Akihabara@DEEP
| image           =  
| caption         = Akihabara@DEEP logo
| ja_kanji        = アキハバラ@DEEP
| ja_romaji       = Akihabara atto Dīpu
| genre           = Comedy, Action genre|Action, Drama
}}
{{Infobox animanga/Print
| type            = manga
| author          = Ira Ishida
| publisher       = Coamix
| demographic     = Seinen manga|Seinen
| magazine        = Weekly Comic Bunch
| first           = August 12, 2005
| last            = May 04, 2007
| volumes         = 6
| volume_list     =
}}
{{Infobox animanga/Video
| type            = drama
| director        = Hitoshi Ohne
| producer        =
| writer          =
| music           =
| studio          = TBS
| first           = June 19, 2006
| last            = August 28, 2006
| episodes        = 11 + Special
| episode_list    =
}}
{{Infobox animanga/Video
| type            = live film
| director        = Takashi Minamoto
| producer        =
| writer          =
| music           =
| studio          = Toei Company
| released        = September 2, 2006
| runtime         = 119 min
}}
 
 television drama, and movie adaptation.

The drama and manga versions have considerable differences from the original novel.

The original novel focuses on a group of outcasts and their attempt to make a new search engine, which would have its own Artificial Intelligence that would understand users intentions and help them in their online searches.  This technology gets the attention of Digital Capital, who wants to steal the technology for themselves.

Taking place in the Tokyo district of Akihabara, or Akiba as it is often shortened to, the story in the drama focuses on a group of outcasts of society, mainly otaku, and a company they establish to "troubleshoot" the problems of Akihabara. The manga has a story more similar to the novel, in that the Otaku are in conflict with Digital Capital over the development of new software. The movie, on the other hand, shows a much darker, more violent and slightly twisted perspective of life as an Akiba-otaku. The movie was released in September 2006.

Akihabara@Deep was adapted into a manga a year after the novels publication. It featured art by Makoto Akane, and followed the same story as the original novel. It was serialized in Weekly Comic Bunch, a seinen manga magazine, in August 2005 and spanned a total of six tankōbon volumes. 

== Plot ==

===Novel===

A group of outcasts who meet each other through an online website decide to form an IT company.  They seek to create a search engine that will become more popular than Google.  They call their creation CROOK (クルーク).  This new search engine uses Artificial Intelligence to understand the users intentions and help them narrow their search.  This attracts the attention of Nakagomi Takeshi, the president of Digital Capital (DigiCap), who after a failed attempt to take over Akihabara@Deep, steals the technology for himself.  The group must then find a way to get their technology back from DigiCap.  The novel is narrated from the point of view of CROOKs Artificial Intelligence.

===Drama===

The Drama takes a much more lighthearted approach to the story.  It all begins when a mysterious online personality named Yui decides to assemble these outcasts to try to form a group dedicated to solving the problems that plague Akiba by forming the "troubleshooting" company, Akihabara@DEEP, often shortened to just @DEEP. One example of this troubleshooting would be that the otaku of Akiba have come under a surge of bullying. @DEEPs first task was to eliminate this bullying. After getting rid of this problem, @DEEP began taking up other tasks as well. Some of these include clearing the name of a cosplayer in a case that involved the distribution of underground videos depicting other cosplayers getting undressed in dressing rooms, protecting a news anchor from attacks by otaku who were enraged at a story that she had previously worked on that bashed the youth of Akiba, and helping out a maid cafe that the people of Akiba frequent from an opposing maid cafe that was acting extremely hostile to this maid cafe and even provided various prostitution services to the visitors of the opposing maid cafe.

While @DEEP solves these cases, the group unintentionally attracts the attention of a seedy character named Nakagomi Takeshi. The reason behind this is because of a secret past relationship between Nakagomi and Yui. What adds to this is a fully self-reliant AI program that Nakagomi would greatly benefit from if he got his hands on it. This is because he is the president of a very influential electronics company called Digital Capital, often shortened to DigiCap. He will eventually raid @DEEPs headquarters and steal Yuis AI.

== Characters ==
The main characters of @DEEP are all outcasts of society and usually have something fundamentally wrong with themselves. After getting advice from a website called "Yuis Lifeguard" these six individuals are called together by the websites creator, Yui, to help clean up Akiba.

; :(Drama:  )

:Page is the leader of Akihabara@DEEP. Although articulate, he has a severe stutter that prevents him from speaking at length without the aid of a text-to-speech program on his computer, which Taiko later upgrades to speak more fluently in Pages own voice. Page is the only member of @DEEP without a particular area of expertise, but proves a worthy leader for the group. He always has his friends best interests at heart and works hard to protect them, as seen when he stops the merciless beating of Akira, Box, and Taiko by threatening to blow himself up with bombs (in actuality, fireworks) strapped to his body.

:At the beginning of each mission, he often says, "All right, battle plan!" to the point where he is teased by the others for its constant repetition and his inability to say it properly.

; :(Drama:  )

:Box is @DEEPs resident graphic designer. He wears gloves at all times to prevent direct contact the world, and changes to a fresh pair every two hours. Box is afraid of women to the point that he foams at the mouth if one gets too close. He, however, has no problems at all with what he terms 2D women, for example animated women, female cosplayers, or maids (although not ero maids; Box says they have too much bare skin to be grouped in with his 2D women). Apparently, whenever a girl is around, he hears a noise in his head go, "kyupin!" and he runs away.

:In episode 9 of the drama, Boxs past is revealed along with the reason why he is afraid of women. Also, Boxs nickname is explained.

; :(Drama:  )

:Taiko is a techno-musician who creates ringtones and the like as a hobby. When he is exposed to flashing lights, he seizes, freezing up like a computer. His nickname is derivative of his musical ability. In the movie, he appears to be a mechanic–technician, instead of being a musician.

; :(Drama:  )

:Akira is @DEEPs fighting maid, working at Akane-chin during the day and fighting cat-fight cage matches at night. She provides the initial funding for @DEEPs headquarters and activities with the prize money she wins in a high-profile match. Although already quite popular among Akihabara otaku, Akira begins to attract an exponential amount of attention after Page, Box, and Taiko create her official website.

;  :(Drama:  )

:@DEEPs coding and hacking prodigy, Ism is the one who programmed Yuis website and AI system. Ism is also the first one of Akihabara@DEEP to ever meet Yui in real life. In the novel and film, Ism is male, but in the manga and drama is female. She/he suffers from a condition that creates ice-blue colored eyes.  

:She is also good friends with Tosaka, a programmer working for Nakagomis Digi Capital.

; :(Drama: Yuki Himura)

:Daruma is @DEEPs cosplay expert. In the novel and manga, he is a middle-aged lawyer who provides legal consultation for the team. In the drama, he retains his cosplay expertise, and also serves as @DEEPs accountant, but mostly he provides comic relief. He was excluded from the film entirely.

; :(Drama: Manami Honjo)

:Yui is the mysterious woman who brings the members of Akihabara@DEEP, all regular visitors to her website, together. She once had a relationship with Nakagomi which ended because of their contrary motivations; Nakagomi sought financial gain, while Yui simply wanted to help others. After Yui dies, her AI (designed by Ism) evolves into a sentient program.

; :(Drama:  )

:Nakagomi is the CEO of Digi Capital, and an otaku himself. He and Yui were in a relationship and had planned to start Akihabara@DEEP together but differed in how to run the company, Nakagomi wanted to make money while Yui wanted to help people. After learning of the existence of Akihabara@DEEP, he attempts to help them anonymously, but when they refuse to surrender Yuis AI to him, he seeks to destroy them.

:In the drama, he has a strange addiction to chupa chups and often cosplays as Char Aznable (Mobile Suit Gundam). He refuses to consider himself as an otaku like everyone else in Akihabara and calls himself a "evolved otaku"

; :(Drama: Mickey Curtis)
:He was a Communications Engineering professor at an electronic engineering university. He taught both Yui and Nakagomi, he and Nakagomi were working on a new OS system. When it was almost finished Nakagomi stole the data and sold it. He used the money from this to start Digital Capital. In the drama he warns the @DEEP crew about staying away from Nakagomi.

; :(Drama:  
:Friend of Ism and fellow programmer who works for Digital Capital. He had a company similar to @DEEP but lost it to Nakagomi. At the end of the series he becomes a prominent figure in the programming field.

; :(Drama: Masayuki Izumi)

:Nakagomis manager–assistant who also seems to be Nakagomis gofer, and provides some comic relief. He is a character exclusive to the drama series.

; :(Drama: Youssef Lotfy)
 Indian sidewalk vendor in Akihabara. He offers some fine second-hand items such as computer parts, rare items, and even illegal paraphernalia. In the drama he sells just about everything from oden cans to fireworks to a Densha Otoko novel. In the final episode, he assists Akira and the other @DEEP members in the infiltration of DigiCap Headquarters.

== TV drama episodes ==
;Episode 1: Our City Akihabara. Exterminate the Otaku Hunts!  
;Episode 2: Fighting Maid Akira. Get Revenge in the Cat Fight!  
;Episode 3: Getting Over Yuis Death. @DEEP Gets Started. Catch the Cosplay Peeper!  
;Episode 4: Taiko in Love!? Save the Brainwashed Akiba Children!  
;Episode 5: Appearance of Imposter Yui! Exposing the Suicide Site Targeting Ota!  
;Episode 6: Female Announcer vs Otaku. Prevent a Media Frenzy!  
;Episode 7: Akiba of the Dead. Repel the Otaku Zombie!  
;Episode 8: Great Maid War. Investigate the Ero Maid Cafe!  
;Episode 9: Boxs Past. Conquer that Fear of Women!  
;Episode 10: Daruma Falls!? @DEEP splits up!?  
;Episode 11: @DEEP Goes Forth! Rescue Yui from the Electric Brain Castle!  
;Special: ??

== References ==
 

== External links ==
*   
*   
*   
* 

 
 
 
 
 
 