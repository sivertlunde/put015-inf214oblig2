Bandslam
 
 
{{Infobox film
| name           = Bandslam
| image          = Bandslamposter.jpg
| caption        = Theatrical release poster
| director       = Todd Graff
| producer       = Elaine Goldsmith-Thomas
| screenplay     = Josh A. Cagan Todd Graff
| story          = Josh A. Cagan
| starring       = Aly Michalka Vanessa Hudgens Gaelan Connell Scott Porter Lisa Kudrow Tim Jo
| music          = Adam Lasus Linda Cohen Joseph Magee
| cinematography = Eric Steelberg John Gilbert
| studio         = Walden Media
| distributor    = Summit Entertainment
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $12,225,023
}} musical and romantic comedy film produced by Summit Entertainment and Walden Media. Written by Josh Cagan and Todd Graff, it stars Aly Michalka, Vanessa Hudgens, Gaelan Connell, Lisa Kudrow, Scott Porter,    Ryan Donowho, and Tim Jo. The story revolves around Will and Charlotte, who form an unlikely bond through their shared love of music. Assembling a like-minded crew of misfits, the friends form a rock group and perform in a battle of the bands competition called "Bandslam".

Bandslam was shot in Austin, Texas, with additional scenes filmed in New York City.    The film generated mostly positive reviews but it failed to chart in the top 10 when it was released on August 14, 2009 in the US, where it grossed only US$|$2,250,000 on the weekend.

== Plot   ==
Will Burton (Gaelan Connell) is a music enthusiast and a David Bowie fan with a hint of rock and roll and boy next door. Throughout the movie, Will writes journal-like e-mails to Bowie every day, although Bowie never answers. When Wills mother Karen (Lisa Kudrow) finds a new job, he switches to a new school, which he is eager to do since he was bullied at his previous one. During lunch one day at his new school, he meets a girl who says her name is written Sa5m but pronounced Sam, the 5 is silent (Vanessa Hudgens). She tells him about Bandslam, an annual music competition in which the winning band gets a recording contract. Will and Sa5m quickly become friends but, shortly after, he is sought after by another girl named Charlotte Barnes (Aly Michalka). One afternoon, Charlotte asks Will to join her in an after-school day-care center. When she starts inviting him to hang out with her, the teenager is stunned—as is his single mother Karen. Impressed by his eclectic knowledge of music, Charlotte, who is a gifted singer-songwriter, asks Will to manage her rock/ska band. Her goal is apparently to take revenge on her egocentric musician ex-boyfriend Ben Wheatley (Scott Porter) during Bandslam.

Unsure of what he is getting himself into, Will agrees to help Charlottes band (later called I Cant Go On, Ill Go On) which includes Bug (Charlie Saxton) and Omar (Tim Jo), eventually expanding it with more like-minded outcasts. Against all odds, the groups sound starts to come together and their prospects for success look bright. Will starts losing his "loser-status", but the band gets in the way of completing a project he was going to do with Sa5m. After spending a day with Sa5m, Charlotte teaches Will how to properly kiss a girl, by demonstrating on him; after an awkward start, Will kisses Sa5m at the Overlook. Will stands Sa5m up, breaking the date for a concert with Charlotte.  After that, Sa5m starts ignoring Will at school; he visits her house, hoping to apologize. Her mom shows him a video of a younger Sa5m performing "Everything I Own", leading Sa5m to be outraged and order Will to leave. As an apology, he makes a touching documentary short about her for his Human Studies project (with the song "Young Folks" included in it) and she eventually forgives him.

After Will accidentally ruins Bens attempt to reconcile with Charlotte, Ben decides to do a little research on him, in order to ruin his image. He finds out about Wills father, who was sent to prison years ago when he accidentally killed a child while driving drunk. Ben then starts to call Will "Dewey" (just as other students did at his old school), which stands for "DWI" ("Driving While Intoxicated"). Will detests this nickname because it reminds him of his father, whom he is ashamed of. What is more, Charlottes father dies and she decides to quit the band. As she explains to Will, her father hated how she acted when she was with her ex-boyfriend, so after he got sick, she decided to change her image and be nicer to "people like  ", meaning outcasts, hoping that this good behavior would cause her dad to get well. The band members are hurt by this discovery, as it means she did not genuinely like them, but they decide to go on nonetheless, with Sa5m taking over as lead singer.

On the night of Bandslam, Charlotte comes backstage in order to apologize to the band, and after a moment of hesitation, Will accepts her apology. Right before going on stage, however, they discover that Bens band (the "Glory Dogs") have decided to play the song that I Cant Go On, Ill Go On were originally to perform, forcing them to change their act at the last minute. Will suddenly remembers the video Sa5ms mother showed him earlier and suggests that they perform "Everything I Own", since it is the only song Sa5m knows. To buy some time, Will comes out first onstage, but at first he is embarrassingly silent. The students begin to chant "Dewey! Dewey! Dewey!". He starts to walk off the stage, but then comes back and decides to chant with them instead. After a while, he yells into the microphone, "Do we wanna ROCK?!". The band then shows up and fires up the crowd with an upbeat ska version of "Everything I Own".
 Lodger album). The last scene takes place during Charlottes graduation ceremony where Will and Sa5m are confirmed as dating.

== Cast and characters ==
 
* Aly Michalka as Charlotte Barnes , popular ex-cheerleader and senior at the high school that Will attends. We are told later in the film that her ailing father does not approve of how she only runs with the "Populars" in school.  Charlotte makes a vow to forsake the populars and run with the geekier teens like Will and Sa5m in hopes that this good behavior will cause her dad to get well. She terms this an "experiment".  Her efforts to start a new band ("I Cant Go On, Ill Go On"), include making Will the manager and coach of the eight musicians that make up their newly formed group. Old boyfriend, Ben Wheatley, surfaces several times in the movie attempting to woo Charlotte back.
* Gaelan Connell as Will Burton, the male protagonist. Will is an avid fan of David Bowie and writes regular emails to the latter. Will is also a music enthusiast and well acquainted with the mechanics of well produced, pop music. After relocating to New Jersey and a new high school, he finds himself managing Charlottes new rock band. They hope to compete in the East Coast, "Battle of the Bands" contest called "Bandslam." He lives with his single parent mother.
* Vanessa Hudgens as Sa5m (the 5 is silent). She is Wills first friend at his new school in Lodi, New Jersey. She talks in a monotone and slowly because she says that "emotion is overrated" and she used to stutter when she was young. She develops a relationship with Will, but "breaks up" with him when she is stood up for a date and discovers that Will was at an event with Charlotte. We discover later that she is a gifted singer. After she reconciles with Will, she is appointed the new lead vocalist in their band. She recognizes long before Will that Charlotte is "hard wired" to run with the high rollers. Sa5m suspects that Charlottes "experiment" might have wider scope in that a higher intelligence could be controlling their actions ("Charlotte times a trillion").
* Lisa Kudrow as Karen Burton (Wills single parent mother). She is concerned with Wills involvement with other kids at school because they pick on him due to the fact that his dad was sent to prison. She is a good mom and tries to protect him from the likes of Charlotte.
* Scott Porter as Ben Wheatley, the boastful and show-off lead vocalist in the original band,"Glory Dogs", which is now competing against Charlottes new band, "I Cant Go On, Ill Go On". He is Charlottes ex-boyfriend as well. In the later part of the movie, he reconciles with Charlotte and also becomes friends with the rest of the "I Cant Go On, Ill Go On" band. He serves as the main antagonist.
* The films fictional band, "I Cant Go On, Ill Go On" members include:
** Charlie Saxton as Bug, the bassist.
** Ryan Donowho as Basher Martin, the drummer. Basher has anger management issues, and initially dislikes the concept of Bandslam. He decides to join after Will tells he has an older sister who is often around—in fact his mother.
** Tim Jo as Omar, the guitarist.
** Lisa Chung as Kim Lee, the keyboardist.
** Elvy Yost as Irene Lerman, the cellist.

 
David Bowie officially began talks in early February 2009. Bowie has a vital role in the film as Will Burtons idol, to whom Burton frequently writes e-mails. He makes a short cameo in the end of the movie.  Liam Aiken was originally chosen to play Will, but ultimately the role was given to Gaelan Connell. {{cite news|last=Kit|first=Borys|title=
Kudrow, Aiken to execute "Will" | date = February 1, 2008|url=http://www.reuters.com/article/filmNews/idUSN0164763420080201|accessdate=May 31, 2009 | work=Reuters}}  Actually, when Connell auditioned for "Will", he was recommended to try out for the cellist part since he plays cello. Director Todd Graff watched his cello audition, and decided to give him the starring role as "Will."  Originally, Vanessa Hudgens wanted to play the role of Charlotte, but she was cast for the role of Sa5m on January 12, 2008. Hudgens sang "Rehab (Amy Winehouse song)|Rehab" for her audition. {{cite web|work=MTV News|publisher=Viacom|last=Vena|first=Jocelyn|title=
Vanessa Hudgens Gets Weird In Bandslam | date = April 14, 2009|url=http://www.mtv.com/movies/news/articles/1609147/20090413/story.jhtml|accessdate=June 1, 2009}}  In order to pursue the role, Hudgens watched The Addams Family and tapped into Wednesdays character. {{cite web|publisher=Digital Spy|last=Simpson|first=Oli|title=
Hudgens admits Addams Family influence | date = August 20, 2009|url=http://www.digitalspy.co.uk/movies/a172765/hudgens-admits-addams-family-influence.html?imdb|accessdate=August 23, 2009}} 
 

== Production ==
Bandslam was bought by a studio back in 2004,    but in late January 2007, Walden Media and Summit Entertainment announced that they would co-finance Bandslam, co-written by Todd Graff and Josh A. Cagan and directed by Graff.  Graff was hired to direct in early March 2007 and rewrote the screenplay.  

Before filming, they had two weeks of music rehearsals. The actors and actresses were designated to play their instruments. Donowho and Michalka had extensive experience with the drums and guitar, respectively, so they were used to their instruments, but Lisa Chung, Scott Porter and Vanessa Hudgens were not. Hudgens told MTV News she didnt expect there to be as much music, and "they are like, We are going to have two weeks of music rehearsals, and I was like, What? But it was really cool. Its definitely not the kind of music I do normally."  Principal photography began on February 9, 2008 in Austin, Texas.   Although the film is set in New Jersey, Graff felt strongly that it was important to shoot in a place with a wide array of great live music to choose from. "Im a big believer in local scenes," he says. "I think its the lifeblood of music. Austin is renowned as a music town, and rightly so. They have a ton of really great bands just trying to get their music out there. So, its great we had an opportunity to use several unsigned local Texas bands." 

In an interview with Los Angeles Times, director Todd Graff admitted there were cultural incongruities underlying the film. "I know, its demented. Every once in a while on set I would think to myself, I cant believe we got away with this. I always thought if it only sent one kid to listen to a Velvet Underground record, it would be worth it to me."  All of the vocals were filmed live by each of the singing cast members, as mentioned by director Graff. One of the requirements for the film was that all actors had to be able to provide their own vocals. Because Aly Michalka was on tour with her sister AJ Michalka and Miley Cyrus, she did not arrive in Austin until midway through rehearsals. Conveniently, the tours last stop was in Austin, where rehearsal and filming for the movie took place. All of the instruments and singing in the movie were recorded by the actors, with the exception of the guitar parts of Vanessa Hudgens and Scott Porter, as well as Lisa Chungs piano parts. Although Hudgens and Porter learned how to play their songs, they were dubbed by guitarists Jason Mozersky and JW Wright.

== Marketing ==
Summit Entertainment spent an estimated $10 million to produce and market Bandslam.  The full-length trailer was released in the internet on March 25, 2009.  On June 3, 2009, the official poster was released.  A novelization of the movie written by Aaron Rosenberg was released on July 9, 2009.  Michalka, Hudgens and Connell, hosted a Bandslam Reel Thinking event at the Grammy Museum to promote the importance of music to middle-schoolers.  
 Girl Scouts of Greater Los Angeles. During the induction, a special screening was held for the scouts present. 

In the UK, browser game Stardoll started a campaign which lets users dress Vanessa Hudgens in Bandslam outfits, view the film trailer, create their own band using Stardoll scenery and virtual guitar gifts to give to friends. 

Summit Entertainments marketing strategies have been criticized. The studio apparently received many calls and e-mails from other studios heads of marketing, saying the films campaign may be the worst job they have seen from positioning, title, marketing tie-ins, and targeting audiences. Deadline.com editor Nikki Finke released an e-mail from a Bandslam insider who pointed out that Summits marketing have basically sold it on to their stars Hudgens and Michalka instead of selling the concept. The e-mail also claims that "they Disneyfied this movie with glitter paint". The insider commented that the bad marketing was particularly unfortunate, a result of the good reviews.  Yale Daily News marked that the film had the "Worst Marketing", pointing out that "Summit Entertainment, which has a done an admirable job marketing the Twilight films, completely ruined what should have been a sleeper hit." 

== Reception ==

=== Box office ===
Despite receiving positive reviews, the film was not a box office success. In its opening weekend, Bandslam grossed $2,231,273 million in 2,121 theaters in the United States and Canada, ranking #13 at the box office, was the best debut for a musical film that week. By the end of its run, Bandslam grossed $5,210,988 domestically and $7,014,035 internationally, totaling $12,225,023 worldwide.   

=== Critical ===
Bandslam was well-received critically, before and after it was released. The Broadcast Film Critics Association gave it an 81 rating score and a 3/4 stars rating.  As of August 15, 2009, based on 20 reviews collected, Metacritic gave the film a 66% "metacritic" score, indicating generally favorable reviews. {{cite web|title=Bandslam date = August 12, 2009|url=http://www.metacritic.com/film/titles/bandslam |accessdate=August 12, 2009}}  
Rotten Tomatoes gives a score of 81% "fresh" rating based on 96 reviews collected; 77 "fresh" and 19 "rotten" with the reported consensus "Bandslam is an intelligent teen film that avoids teen film cliches, in an entertaining package of music and coming-of-age drama." {{cite web|title=Bandslam Movie Reviews, Pictures date = August 12, 2009|url=http://www.rottentomatoes.com/m/bandslam/|accessdate=August 14, 2009}}  With the positive critical response from the review Rotten Tomatoes collected, it ranked #9 in the 10 Tomatoemeters of the Summer. {{cite web|title=Rotten Tomatoes Summer 2009 Wrap-up date = September 6, 2009|url=http://rottentomatoes.com/news/1842008/rotten_tomatoes_summer_2009_wrap_up|accessdate=September 9, 2009}}  
Comparatively, Yahoo! Movies gives a grade of "B-" averaged from 7 critic reviews. 

Entertainment Weekly critic, Lisa Schwarzbaum gave the film a "B", and praised Hudgens and Michalkas performances as well.  Variety (magazine)|Variety said that "Bandslam" will make its cracking voice heard amid the summers boy-based blockbuster clique {{cite news|work=Variety|title=Bandslam date = Joe Williams John Hughes playbook of high-school comedies but lack the heart and insight Hughes invested in his pictures.", but still praised the teenage cast saying "The performances in Bandslam are uniformly strong -- good enough to make you wish this bunch of charismatic, talented kids had been given better material." 

  deftly deliver the coming-of-age goods for the three main characters."    of The Los Angeles Times praised the film by saying: "Bandslam is a pretty good movie given that the odds of it having been a pretty bad movie were steep." 

Critic Andy Webster of The New York Times said that Bandslam may not entirely break new teen-movie ground, but it does offer intriguing glimpses of performers ready to bolt from the Disney stable.  Hudgens received an amount of praise from reviewers, emphasizing her transition from being associated with her previous commercial character, Gabriella Montez, multiple times. Reviewers also cited her impressive performance in the film.   Even though Connell and Michalka each received their fair amount of enthusiastic press, David Waddington of North Wales Pioneer claims that Hudgens "outshines the rest of the cast, failing to fit in with the outcast narrative and making the inevitable climactic ending all the more expected."  Hudgens was praised with her performance in the film that The Observer critic, Philip French said that she looks like the young Thandie Newton and "wisecracks like Dorothy Parker." French then adds that "Bandslam is a witty, touching, cleverly plotted film with excellent music." 

Eye Weekly reviewer Will Sloan says, "Bandslam is like a teenybopper version of Adventureland (film)|Adventureland, and if its PG limitations keep it from being quite as insightful or funny as that film, its still a serviceable, surprisingly intelligent bit of tweenertainment."  Reviewers asserted that the films music mainly lifted to the success of Bandslam from reviews, especially the ska version of Everything I Own, that the film is full of unexpected pleasures set to a surprisingly retro soundtrack.    Similarly, Michael Rechtshaffen of The Hollywood Reporter emphasized the effect of the films music-driven comedy set against the backdrop of a high school battle of the bands competition which manages to come up with a fresh backbeat for the familiar alienated teen refrain, boosted by a talented cast and authentic soundtrack.  {{cite web|title=Bandslam - Movie Rating For Kids and Families date = August 13, 2009|url=http://www.commonsensemedia.org/movie-reviews/bandslam/details|accessdate=August 13, 2009}} 

=== Accolades ===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#CCCCCC" align="center" Awards
|- bgcolor="#CCCCCC" align="center" Year
! Award
! Category
! Recipients and nominees Result
|- 2009
|11th Golden Tomato Awards  Best Musical
|Bandslam
| 
|- 2010
|Scribe Awards    Young Adult Novel - Best Adaption
|Bandslam: The Novel
| 
|- Gabriel Awards    Family Film of the Year
|Bandslam
| 
|-
|- bgcolor="#CCCCCC" align="center" Recognitions
|- bgcolor="#CCCCCC" align="center" Year
! Award
! Category
! Recipients and nominees Result
|- 2009
|rowspan="2"|indieWIRE    Best Film
|Bandslam 10th
|- Best Supporting Performance Aly Michalka 5th
|-
|}

=== Distribution ===
  in Westwood Village, Los Angeles on August 6, 2009]]
Bandslam was released on August 12, 2009, in the UK, Ireland, and France; August 13, 2009 in Australia; August 14 in the US, and Canada;  August 20, 2009 in some parts of Asia; August 26, 2009 in The Philippines. Earlier, the films tentative date was April 10, 2009 listed by The Numbers around the time Liam Aiken was involved in the film. {{cite web|title=Movie Bandslam - Box Office Data, News, Cast Information date = PG in the US for some thematic elements and mild language.  In The Philippines, it is rated Movie and Television Review and Classification Board#Classification ratings|GP. 

Bandslam was critically successful but did not do well commercially. Debuting at 2,121 theaters, the film only grossed $890,000 on its first day and failed to chart in the top 10.  The film eventually reached the top 10 and grossed $50,000 more on the same day.    For its opening weekend, Bandslam didnt chart in the US weekend box office top 10, where it only grossed $2,250,000. It only reached the number 12 spot.  As of October 1, 2009, the film has a domestic gross of $5,210,988,  with a foreign gross of $7,014,035 totaling an international gross of $12,225,023. 

== Home media ==
Bandslam was slated to be released in standard DVD and Blu-ray in the UK on December 7, 2009. However, due to unknown circumstances, UK label E1 Entertainment were unable to release the film on Blu-ray at that time.     In the United States, the DVD was released on March 16, 2010.  In the rest of Europe, the DVD was also released between December 2009 and June 2010.

The DVD contains, aside from the movie itself, some extras: 
*Bandslam: Making the Band documentary
*Audio commentary by the Director and the cast
*Deleted scenes Where Are You Now".
*Music video: "I Cant Go On, Ill Go On" featuring Vanessa Hudgens &nbsp;– "Everything I Own".

Additionally:
*International releases included a trailer in the local language.
*Also, the UK DVD includes an   artcard.

{|class=wikitable
!Region
!Date   
|- United Kingdom  December 7, 2009
|- France    December 16, 2009
|- Australia    December 30, 2009
|- Italy    January 13, 2010
|- Spain    January 27, 2010
|- Netherlands    February 10, 2010
|- United States    March 16, 2010
|- Brazil
|April 20, 2010
|- Germany    June 3, 2010
|}

== Soundtrack ==
  Honor Society, the Daze, Nick Drake and David Bowie. 

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 