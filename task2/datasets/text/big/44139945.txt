Kurukkan Rajavayi
{{Infobox film
| name           = Kurukkan Rajavayi
| image          =
| caption        =
| director       = P Chandrakumar
| producer       =
| writer         =
| screenplay     = Mukesh Maniyanpilla Raju Saleema Jagathy Sreekumar
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Roshni Movies
| distributor    = Roshni Movies
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar. The film stars Mukesh (actor)|Mukesh, Maniyanpilla Raju, Saleema and Jagathy Sreekumar in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Mukesh
*Maniyanpilla Raju
*Saleema
*Jagathy Sreekumar
*Kuthiravattam Pappu
*Oduvil Unnikrishnan

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Moham Nee || Ashalatha, Krishnachandran, Oduvil Unnikrishnan || Poovachal Khader || 
|-
| 2 || Thaarunyathin Vaniyil || S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 