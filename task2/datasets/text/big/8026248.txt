Bardaasht
{{Infobox Film
| name           = Bardaasht
| image          = Bardaasht poster.jpg
| image_size     =
| caption        = Movie poster for Bardaasht
| director       = E. Nivas
| producer       = Vijay Galani, Pooja Galani
| writer         = Vikram Bhatt Girish Dhamija
| narrator       =
| starring       = Bobby Deol Lara Dutta Rahul Dev
| music          = Himesh Reshammiya
| cinematography = Rakesh Manikanthan
| editing        = Aarif Sheikh
| studio         =
| distributor    = Film Folks
| released       = 23 April 2004
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 thriller film released in 2004. The director is E. Nivas. It is based on the screenplay written by Vikram Bhatt. The main cast of the movie is Bobby Deol, Lara Dutta and Rahul Dev. Bardaasht is noted for its gritty and brutal portrayal of police force and triumph of human will and justice in the form of Major Aditya Shrivastava portrayed by Bobby Deol.

==Synopsis==

Aditya Shrivastav (Bobby Deol) is a deserted army officer. He has a brother, Anuj (Ritesh Deshmukh). Anuj gets angry at Aditya one day and runs away. ACP Yashwant Thakur (Rahul Dev) helps Aditya find Anuj and later tells him that Anuj was killed in an encounter.

Aditya reads a police report which states that Anuj was running away with drugs and was shot dead on the spot. Aditya cant quite believe this so he investigates it. He later meets Ramona (Tara Sharma), who reveals that ACP Yashwant Thakur and his two other police officers brutally killed Anuj for no apparent reason and lied to cover it up.

Aditya wants to take the matter to court and chooses Payal (Lara Dutta) as his lawyer.  However, after losing the case, Aditya decides to take matters into his own hands by holding the three captive until they confess their crime. He is successful and, having secretly recorded this evidence, shows it to the police commissioner.

==Critical reception==

Released in April 2004 Bardaasht received acclaimed critical reception. Taran Adarsh of Bollywood Hungama praised Bobby Deols performance and wrote "BARDAASHT is a triumph for Bobby Deol, who takes full advantage of the role offered to him and gives his best shot. He displays the gamut of emotions like a seasoned performer and delivers a knock-out performance. In fact, it wont be wrong to state that this is amongst Bobbys best performance to date!"  Critic Subash K Jha praised the film g  wrote "At a time when the film industry is busy showcasing cops as romanticised idealistic super-heroes, "Bardaasht" takes a cynical view of the police force.But the film portrays the reality. Every day law abiding god-fearing citizen gets a taste of the criminalisation of the police force." Jha praises Deols performance and writes "Bobby Deol, who plays the lead role, drops his habitually languorous look and laidback demeanour to deliver a performance of substantial velocity. Deol makes the best of his silently simmering and suffering army man-turned-civilian role. As Aditya Shrivastava, Bobby is what his brother Sunny has repeatedly played in films like "Arjun (1985 film)|Arjun" and "Ghayal (1990 film)|Ghayal". But Bobby gives the seething role his own spin."  Rediff.com praises Bardaasht and writes "Bardaasht is pretty strong on the performance front. Bobby Deol does a good job. Look out for him in the scene where he sees his kid brothers dead body. His body language is frail, broken and numb" citing Deols power-packed performance. 

==Cast==
*Bobby Deol ... Aditya Shrivastav
*Lara Dutta ... Payal (lawyer)
*Rahul Dev ... ACP Yashwant Thakur
*Ritesh Deshmukh ... Anuj Shrivastav
*Tara Sharma ... Ramona (Anujs girlfriend)
*Vishwajeet Pradhan ... Inspector Deepak Sawant
*Shivaji Satham ... Police Commissioner
*Veerendra Saxena ... Mehmood Bhai
*Anjaan Shrivastav ... Dinanth Shrivastav
*Naresh Suri ... Collage Principal
*Ganesh Yadev ... Inspector Yadav

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Silsile Mulaqaton Ke" 
| Udit Narayan, Alka Yagnik
|- 
| 2
| "Janaabe Ali"
| Shaan (singer)|Shaan, Kunal Ganjawala
|- 
| 3
| "Aap Ki Khata Aap Ki Bewafayee"
| Shaan (singer)|Shaan, Alka Yagnik
|- 
| 4
| "Na Na Na Na Re"
| Kunal Ganjawala, Alisha Chinai
|-
| 5
| "Dil Mera Dil Na Maane"
| Alka Yagnik, Udit Narayan
|}

==Direction team==
* Associate Director: Rafiq Lasne
* Assistant Directors: Ramesh, Rajsekhar, Parag Kalita

==References==
 

==External links==
*  
*  

 
 