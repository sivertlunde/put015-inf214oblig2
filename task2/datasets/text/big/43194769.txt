I Adore You
 
{{Infobox film
| name           = I Adore You
| image          = 
| caption        =
| director       = George King 
| producer       = Irving Asher
| writer         = Paul England based on = original story by W. Scott Darling
| starring       = Margot Grahame Harold French Clifford Heatherley O. B. Clarence Peggy Novak 
| music          =Carroll Gibbons 
| cinematography =
| editing        = studio = Warner Bros-Teddington 
| distributor    = Warner Bros
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
I Adore You is a 1933 British musical comedy film set in a movie studio. It was most notable for Errol Flynn appearing as an extra. 

Margot Grahame was the heroine and Clifford Heatherley plays a film magnate. 

The choreography was done by Ralph Reader, a protege of Busby Berkeley. 

It is considered  a lost film.

==Plot==
Norman Young (Harold French) wants to marry Margot Grahame (Margot Grahame) but he contract with a producer prohibits her from marrying during a five year period. Norman spends millions to take over the contact. 

==References==
 

==External links==
* 

 
 
 
 


 
 