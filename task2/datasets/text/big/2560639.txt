Ebba the Movie
{{Infobox film
| name        = Ebba the Movie
| image       = Ebba the Movie.jpg
| caption     = Swedish DVD-cover Lennart Eriksson Gunnar Ljungstedt
| director    = Johan Donner
| producer    = Lisbet Gabrielsson Anders Holt
| distributor = Svenska Filminstitutet
| released    =  
| runtime     = 78 min. Swedish
| country     = Sweden
| music       = Ebba Grön, Dag Vag
}}
 punk band Ebba Grön from 1982, by director and writer Johan Donner.

The film includes a lot of live music and interviews with the band’s members. It starts in Rågsved, a working class suburb of Stockholm, and follows Ebba Grön on tour around Sweden until the band returns to Rågsved.
 Swedish reggaeband Dag Vag also participates in a part of the movie when Ebba is on tour.

There are two versions of the film. When released in 1982, the movie was originally 78 minutes long. Johan Donner later cut down the film to a 52 minutes version which he called the Hardcore version. Both versions are included in the DVD-release.

The title of the film is a reference to the 1977 documentary film  , featuring fellow Swedes ABBA. The reference is intended to be ironic, since Ebba Gröns style is very different from that of ABBA.

== External links ==
*  

 
 
 
 
 
 


 
 