Puncture Wounds
 
{{Infobox film
| name           = Puncture Wounds
| image          =PunctureWounds.Movieposter.png
| alt            =
| caption        =Movie poster
| director       = Giorgio Serafini James Coyne
| producer       = Agustin Gianni Capaldi Phillip B. Goldfine Cung Le  Dolph Lundgren
| writer         = James Coyne
| starring       = Cung Le Dolph Lundgren
| music          = Brian Jackson Harris Justin Raines Michael Wickstrom
| cinematography = Marco Cappetta
| editing        = John Quinn
| studio         = Grindstone Entertainment Group Hollywood Media Bridge Picture Perfect Corporation Voltage Pictures
| distributor    = Lionsgate Home Entertainment
| released       =   }}
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} James Coyne, written by Coyne, and starring Cung Le and Dolph Lundgren.  A veteran (Le) rescues a prostitute, and, in doing so, sets up both of them for reprisals from a local crime lord (Lundgren).  It was released direct to video on March 11, 2014.

== Plot ==
John Nguyen is a veteran suffering from posttraumatic stress disorder.  Despite his status as a war hero, hes had difficulty adjusting to civilian life.  When he hears a woman scream, he investigates and gets into a fight with several Aryan Nation thugs who are hassling Tanya, a prostitute in the service of local crime boss Hollis.  Tanya is horrified when John kills all but one of the thugs; although thankful for his help, she says that John has doomed them both to die at Hollis hands.  Planning to flee the city, Tanya buys a bus ticket, but Hollis convinces her to stay by promising her that she can come off the streets and live with him again.  Hollis directs his lieutenant Vin to murder Johns family.  Although reluctant, Vin complies.  Enraged, John tracks down Bennett, a drug dealer, and strikes back at Hollis meth business, brutally killing several of the men responsible for his familys death.

As Hollis and John war with each other, two cops try to solve the case.  Sgt. Mitchell believes John to be honorable, while his partner, a dirty cop in the employ of Hollis, attempts to derail the investigation.  John recruits his disabled war buddy J. P. to help him in the final confrontation with Hollis.  As Hollis is in the midst of violently raping Tanya, Vin intercedes.  Before Vin and Hollis can come to blows, John assaults Hollis house.  John kills most of Hollis henchmen and leaves Vin to die after a knife fight.  Although John captures Hollis, reinforcements arrive and turn the tide against him.  Both John and J. P. are captured, and Hollis murders J. P.  John challenges Hollis to a one-on-one fight, and, when John appears to be winning, Tanya shoots and kills Hollis remaining henchmen before they can interfere.  After John kills Hollis, Mitchell arrives and tells him to leave the scene of the crime, as he will clean it up.

== Cast ==
* Cung Le as John Nguyen
* Dolph Lundgren as Hollis
* Vinnie Jones as Bennett
* Briana Evigan as Tanya
* Gianni Capaldi as Vin
* James C. Burns as Sgt. Terry Mitchell
* Robert LaSardo as Magico
* Jonathan Kowalsky as J. P.
* Jake Jacobson as OHanrahan
* Scott Sheeley as Carsen

== Production ==
Shooting took 17 days.  Cung Le said that he suffered several minor injuries but considers this his best acting performance so far. 

== Release ==
Puncture Wounds was released direct to video on  March 11, 2014. 

== Reception ==
Nav Qateel of Influx Magazine rated it B and called it a "serviceable low-budget action affair."   Ian Jane of DVD Talk rated it 3/5 stars and called it "a satisfying low budget action movie". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 