Comrades: Almost a Love Story
 
 
 
{{Infobox Film
| name           = Comrades: Almost a Love Story 甜蜜蜜
| image          = Comrades Almost a Love Story.jpg
| caption        = Film poster of Comrades: Almost a Love Story
| director       = Peter Chan
| producer       = Peter Chan
| writer         = Ivy Ho
| starring       = Maggie Cheung Leon Lai
| music          = Chiu Jun-Fun Chiu Tsang-Hei
| cinematography = Jingle Ma
| editing        = Chan Ki-hop Kwong Chi-Leung
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong Mandarin
| budget         = 
| gross          = 
}}
Comrades: Almost a Love Story ( ) is a 1996 Hong Kong film starring Maggie Cheung, Leon Lai, Eric Tsang, and Kristy Yang. It was directed by Peter Chan. The title refers to Tian mi mi, a song by Teresa Teng whose songs are featured in the film. It was filmed on location in Hong Kong and New York City.

==Plot== Chinese mainlanders who migrate to Hong Kong to make a living, but end up falling in love. Leon Lai plays a naive Northerner, Li Xiao-Jun, and Maggie Cheung plays an opportunist/entrepreneur from Guangzhou speaking cantonese, Li Qiao, who takes advantage of mainlanders like herself for financial gains. The loneliness of living in the big city inevitably brings the two into a passionate love affair. But their different ambitions (Li Xiao-Jun wants to bring his fiance to Hong Kong; Li Qiao wants to get rich) mean that they are unable to be together. Eventually, Li Xiao-Jun marries his fiance (Yang) in Hong Kong and Li Qiao winds up in a relationship with a mob boss named Pao (Tsang). Li Qiao also becomes a successful entrepreneur, achieving her Hong Kong dream. Despite their seemingly separate lives, however, they are still in love and they have one final tryst in the room they used to share before they are separated again.

Burdened by guilt and his love for Li Qiao, Xiao-Jun confesses to his wife that he has not been faithful. He then leaves Hong Kong, and becomes a cook in the United States. Pao, chased by the Hong Kong police, escapes with Li Qiao to the U.S. as illegal immigrants. After almost 10 years, Xiao-Jun and Li Qiao meet again as lonely immigrants in the U.S. (after the latter gets her green card). By then, both of them have already been freed from their previous partners - Xiao-Jun left his wife, and Pao is killed in a robbery in the U.S. The film ends with Xiao-Jun and Li Qiao fatefully meeting each other in front of an electronic shop that has a display TV playing a Teresa Teng video, after news of the singers death broke.

==Production==
The Chinese title of the film, Tian Mi Mi, comes from a song of the same name by Teresa Teng, which is famous both in mainland China and among the overseas Chinese community. The movie displays love of the famous singer who died a year before the film was released; the film is considered a love poem in memory of Teresa Teng. Her music is featured prominently throughout the film, and Teresa Teng herself is an important subplot for the movie. Leon Lai sings the title song for the ending credits. In a cameo performance, Christopher Doyle, the internationally-known cinematographer famous for his collaboration with Wong Kar-wai, plays an English teacher.

==Release==
Comrades: Almost a Love Story was released in Hong Kong on 2 November 1996.    The film grossed a total of HK$ 15,557,580 on its initial theatrical run in Hong Kong. 

The film has been screend theatrically for the first time in nearly a decade at the Hong Kong International Film Festival in 2012 with a new 35&nbsp;mm print supervised by director Peter Chan.    The film will be shown at the 70th Venice International Film Festival in 2013. 

==Reception==
The movie was very well received in Hong Kong and Taiwan, winning best picture, director, and actress for the Hong Kong Film Awards, among other wins. Maggie Cheungs performance also won general acclaim. The movie was voted #11 of the Greatest Chinese Films of all time by the Chinese Movie Database (www.dianying.com) and #28 of the 100 Greatest Chinese Films by the Hong Kong Film Awards. It is also listed in the 100 Greatest Chinese Films of the 20th Century by Asia Weekly Magazine.

In 2011, the Taipei Golden Horse Film Festival listed Comrades: Almost a Love Story at number 16 in their list of "100 Greatest Chinese-Language Films".    The majority of the voters originated from Taiwan, and included film scholars, festival programmers, film directors, actors and producers. 

== Awards and nominations ==

=== Awards ===
16th Hong Kong Film Awards
* Best Picture Peter Chan Ho-Sun Maggie Cheung Man-Yuk  Eric Tsang Chi-Wai
* Best Screenplay - Ivy Ho Jingle Ma Chor-Sing
* Best Art Direction - Hai Chung-Man
* Best Costume Design - Ng Lei-Lo
* Best Original Music Score - Chui Jun-Fun

Nominations
* Best Actor - Leon Lai
* Best Newcomer - Kristy Yang

Golden Horse Film Awards
* Best Picture
* Best Actress

== See also ==
* List of Hong Kong films
* Cinema of Hong Kong

==Notes and references==
 

== External links ==
*  
*  

 
 
 
 

 
   
 
 
 
 