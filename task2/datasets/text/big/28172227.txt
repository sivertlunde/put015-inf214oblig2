Are Parents People?
{{Infobox film
| title          = Are Parents People?
| image          =
| caption        = Malcolm St. Clair
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Frances Agnew (adaptation)
| based on       =  
| starring       = Betty Bronson Florence Vidor Adolphe Menjou George Beranger Lawrence Gray Emily Fitzroy William Courtright
| cinematography = Bert Glennon
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}} Malcolm St. Clair and released by Paramount Pictures.   

==Synopsis==
Lita, the daughter (Bronson) of a wealthy couple (Vidor, Menjou) tries to prevent their divorce by "throwing herself" at "movie sheik" Maurice Mansfield (Beranger). Meanwhile, a young doctor (Gray) is interested in Lita.

==Preservation status==
According to the SilentEra website, the film exists in a 16mm print.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 