Vazhakku Enn 18/9
{{Infobox film
| name           = Vazhakku Enn 18/9
| image          = Vazhakku Enn 18-9 Movie Posters.jpg
| caption        =
| director       = Balaji Sakthivel
| producer       = Thirupathi Brothers|N. Subash Chandrabose, Ronnie Screwvala
| writer         = Balaji Shakthivel Sri Mithun Murali Urmila Mahanta Manisha Yadav
| music          = R. Prasanna
| cinematography = Vijay Milton
| editing        = Gopi Krishna
| studio         = Thirupathi Brothers UTV Motion Pictures
| released       =  
| runtime        = 115 minutes
| country        = India
| language       = Tamil
| budget         =
| box office     =
}}
 Tamil crime thriller film written and directed by Balaji Sakthivel, starring newcomers Mithun Murali, Sri (actor)|Sri, Urmila Mahanta and Manisha Yadav in the lead roles.    Pre-production of the film began in 2008,    with the filming commencing in 2010 only,    following several delays. Cinematography was handled by Vijay Milton, while guitarist R. Prasanna had scored the music.
 Black Butterfly Academy Award for Best Foreign Language Film for the year 2012. 

==Plot==
The story is narrated through flashbacks. Velu (Sri (actor)|Sri), a teenager, works in a roadside shop. He meets Jyothi (Urmila Mahanta), who is a maidservant at a few of the nearby apartments. Velu falls in love with Jyothi. Aarthy (Manisha Yadav) lives in one of the apartment buildings. Dinesh (Mithun Murali) is a student who resides in the same building. Dinesh is a spoilt brat. As luck would have it, Aarthy falls for him without knowing his true intentions. Dinesh too is attracted to Aarthy. He seizes the opportunity and shoots video clips of her private moments on his mobile phone and even circulates them among his friends via MMS. When Aarthy finds out, she is aghast and threatens to approach the police. An angry Dinesh tries to murder Aarthy, but Jyothi intervenes accidentally and saves her. She sustains serious injuries in the process. The movie picks up speed as the corrupt Police Inspector Kumaravel (Muthuraman) begins investigations. Kumaravel negotaites with Dineshs mother, who is a school correspondent but refuses 
to later due to her stubborn character and completes the investigation with Dinesh as the culprit of the murder attempt. But Dineshs mother approaches a minister with whom she has an affair with and he intervenes with Kumaravel for a negotiation between them. They agree on for an amount of ten lakh rupees. Kumaravel then talks with Velu to stand in the shoes of Dhinesh, if he wants to cure Jothi. Velu accepts and is sentenced for several years in prison. Kumaravel with the money he got completes the construction of his house and gives nothing to Jothi. Later it is informed to Jothi that Velu did not commit the crime and is falsely accused in order that she must be cured. She realises that Kumaravel has cheated her and Velu, she gets to the court and throws acid on Kumaravel. Jothi is arrested and as enquiries run course the judiciary finds Velu as innocent and releases him. Dinesh is arrested and Jothi is sentenced for years in prison. In the final scene Velu meets Jothi in prison and proposes his love and tells he will be waiting for her and leaves the cell as the door closes with the disfigured face of Jothi is shown on screen.

==Cast==
 Sri as Velu
* Mithun Murali as Dinesh
* Urmila Mahanta as Jothi
* Manisha Yadav as Aarthi
* Muthuraman as Kumaravel
* Chinnasamy as Chinnasamy
* Senthil as Vediappan
* Rani as Rani
* Rethika Srinivas as Jayalakshmi
* Goutham as Goutham
* Vidhya Eeshwar as Gayathri
* Anjalai as Parvathy
* Devi as Rosy

==Production==
Balaji Sakthivel began working on Vazhakku En 18/9 in 2008,  after director   part of the film but was unsure about how to start the film, taking over one year to finalize on the opening sequences, which he worked on with Linguswamy.  He disclosed that the film would focus on relationships and "so-called love" of todays generation, set in a city,  describing it as a "teenage thriller".    He further informed that the climax was inspired from a news report. 

Sakthivel decided to cast new faces in the lead and most of the character roles as well, with early reports claiming that Naveen Rai and Prajna were being introduced in the lead roles.    In May 2010, it was reported that the director had done a screen test and test shoots with the son of senior communist leader and progressive writer C. Mahendran.  Only after the completion of filming, Sakthivel revealed the final cast; Mithun Murali, who has appeared in three Malayalam before, and Sri (actor)|Sri, who has been part of the television serial Kana Kaanum Kaalangal that was aired on Vijay TV, played the male leads, while the lead female characters were played by Urmila Mahanta, a student of the Pune Film Institute, hailing from Assam, and Manisha Yadav, a model from Bangalore. 

Filming was supposed to commence in early March 2010,  but eventually started in August 2010, with shootings held in and around Dharmapuri.  Cinematographer Vijay Milton used a Canon EOS 7D digital camera for the filming.   Noted guitarist R. Prasanna, who Sakthivel was supposed to work with in his previous venture Kalloori, was chosen to compose the films soundtrack and background score,  replacing his usual associate Joshua Sridhar. The composer was chosen only after the filming was completed. 

==Soundtrack==
The soundtrack was composed by noted guitarist R. Prasanna,  making his debut as a feature film composer. The album features two tracks with lyrics penned by Na. Muthukumar. Prasanna collaborated with several international musicians, including Argentinian singer Sofia Tosello, Venezuelan cuatro player Juancho Herrera, American pianist Victor Gould, Argentinian bassist Andres Rotmistrovsky and Hungarian drummer Ferenc Nemeth along with Indian flautist Kamalakar to create a mix of various genres.  The songs were unveiled on 14 March 2012 and subsequently premiered on a FM Radio. 

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| title1          = Vazhakku Enn Theme
| extra1          = Sofia Tosello
| length2         = 1:57
| title2          = Vaanathai
| extra2          = Dhandapani
| length2         = 3:33
| title3          = Oru Kural Karthik
| length3         = 3:41
}}

==Release==
The satellite rights of the film were secured by STAR Vijay. The film was given a "U/A" certificate by the Indian Censor Board.

==Reception==
Vazhakku Enn 18/9 received high critical acclaim upon release. The Times of India gave it 4.5 out of 5, the highest rating ever for a Tamil film since its inception in April 2008, with its reviewer M. Suganth calling the film a "bravura piece of filmmaking that will leave you stunned - and even invigorated - by the time it ends".  Rediffs Pavithra Srinivasan gave the film 3.5 out of 5, citing that Vazhakku En 18/9 was "hard-hitting" and a "must-watch".  Sifys critic labelled the film as "excellent" and commented that it was "refreshing, captivating and engrossing".  Rohit Ramachandran of Nowrunning.com rated it 3.5/5 stating that Vazhakku Enn 18/9 was a "rewarding piece of cinema that establishes Balaji Sakthivel as a film-maker to look out for".  Malathi Rangarajan from The Hindu wrote: "The acumen of the ace story teller comes to the fore yet again in Vazhakku Enn: 18/9. A neat presentation with an all new-cast could have been a tough proposition. But Sakthivel achieves it with élan", concluding that the film was "another product Sakthivel can be proud of".  Indiaglitz.com stated that it had "moments that leave a lump in your throat", further writing that it "needs a man with conviction like Balaji Shakthivel to come up with such a convincing film", while calling it a "splendid show".  

Romal M. Singh from   in her review of the Telugu version Premalo Padithe said: "What is perhaps beautiful about the story is that it is an amalgamation of two levels in society — the high, mighty and rich and the poor". She further continued: "With a heavy heart and a mind ridden with hard truths, take away what you can: a semblance of love in a promise to wait a lifetime for a half-burnt face".

==Accolades==
;60th National Film Awards Best Feature Film in Tamil Best Make-up Artist - Raja

;2nd South Indian International Movie Awards
* Best Director - Balaji Sakthivel
* Nominated—Best Film
* Nominated—Best Cinematographer - Vijay Milton
* Nominated—Best Actress in a Supporting Role - Urmila Mahanta
* Nominated—Best Female Debutant - Urmila Mahanta
 South Asian Film Festival (SAFF)
* Best Film

;7th Vijay Awards Best Film Best Director - Balaji Sakthivel Best Debut Actor - Sri Best Debut Actress - Manisha Yadav Best Debut Actress - Urmila Mahanta Best Villain - Muthuraman Best Cinematographer - Vijay Milton Best Editor - Gopi Krishna Best Story, Screenplay Writer - Balaji Sakthivel

==References==
 

==External links==
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 