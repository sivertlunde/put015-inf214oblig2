List of horror films of 1988
 
 
A list of horror films released in 1988 in film|1988.

{| class="wikitable sortable" 1988
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | After Death
| Claudio Fragasso || Jeff Stryker, Candice Daly, Massimo Vanni ||   ||  
|- American Gothic John Hough || Rod Steiger, Yvonne De Carlo ||    ||
|- Bad Dreams Jennifer Rubin, Bruce Abbott ||   ||  
|-
!   | Beyond Dreams Door
| Jay Woelfel || Rick Kesler, Nick Baldasare, Susan Pinsky ||   ||  
|- Black Roses John Martin, Frank Dietz, Jesse DAngelo ||   ||  
|- The Blob
| Chuck Russell || Kevin Dillon, Shawnee Smith ||   || Film remake 
|-
!   | Blood Relations Graeme Campbell || Jan Rubes, Lydie Denier ||   ||  
|- The Brain David Gale, Cynthia Preston ||    ||  
|- Brain Damage
| Frank Henenlotter || Rick Hearst ||   ||  
|-
!   | Celia (film)|Celia
| Ann Turner || Rebecca Smart ||   ||
|-
!   | Cellar Dweller
| John Carl Buechler || Yvonne De Carlo ||   ||  
|-
!   | Cheerleader Camp
| John Quinn || Betsy Russell, Leif Garrett, Rebecca Ferratti ||    ||  
|-
!   | Childs Play (1988 film)|Childs Play Tom Holland Alex Vincent ||   ||  
|-
!   | Chillers
| Daniel Boyd || Jesse Emery, Kimberly Harbour, Thom Delventhal ||   ||  
|-
!   |  
| Frederico Prosperi || Jill Schoelen, J. Eddie Peck ||     ||  
|-
!   | Curse of the Blue Lights
| John Henry Johnson || Brent Ritter, Bettina Julius, Clayton A. McCaw ||   ||  
|-
!   | Deadly Dreams
| Kristine Peterson || Juliette Cummins, Mitchell Anderson, Xander Berkeley ||   ||
|- Dead Heat
| Mark Goldblatt || Treat Williams, Joe Piscopo, Lindsay Frost, Vincent Price ||   ||  
|-
!   | Dead Mate
| Straw Weisman || Elizabeth Mannino, David Gregory, Larry Bockius ||   ||  
|-
!   | Dial Help
| Ruggero Deodato || Charlotte Lewis, Marcello Modugno, Mattia Sbragia ||   ||
|-
!   | Dinner With a Vampire George Hilton, Patrizia Pellegrino, Isabel Russinova ||   || Television film 
|-
!   | The Discarnates
| Nobuhiko Obayashi || Tsurutarō Kataoka, Yûko Natori, Kenjirō Ishimaru ||   ||
|-
!   | Draculas Widow
| Christopher Coppola || Sylvia Kristel, Lenny von Dohlen ||   ||  
|-
!   | Dream Demon
| Harley Cokeliss || Jemma Redgrave, Kathleen Wilhoite, Timothy Spall ||   ||  
|-
!   | Elvira, Mistress of the Dark
| James Signorelli || Cassandra Peterson, Edie McClurg ||   ||  
|-
!   | Evil Laugh
| Dominick Brascia || Kim McKamy ||   ||
|- Evil Dead Trap
| Toshiharu Ikeda || Miyuki Ono ||   ||  
|-
!   | Faceless (film)|Faceless
| Jesus Franco || Helmut Berger, Brigitte Lahaie, Telly Savalas ||    ||  
|-
!   |  
| John Carl Buechler || Jennifer Banko, Lar Park Lincoln, Kevin Spirtas ||   ||  
|-
!   | Fright Night II
| Tommy Lee Wallace || Roddy McDowall, William Ragsdale ||   ||  
|-
!   | Ghost Town
| Richard Governor || Franc Luz, Catherine Hickland, Jimmie F. Skaggs ||   ||  
|-
!   |  
| Dwight H. Little || Donald Pleasence, Ellie Cornell, Danielle Harris ||   ||  
|- Heart of Midnight Matthew Chapman || Jennifer Jason Leigh, Brenda Vaccaro, Jack Hallett ||    ||  
|-
!   |   Clare Higgins, Richard Waugh ||   ||  
|-
!   | Hide and Go Shriek
| Skip Schoolnik || Brittain Frye, Donna Baltron ||   ||
|-
!   | Hobgoblins (film)|Hobgoblins
| Rick Sloane || Tamara Clatterbuck, Kari French, Duane Whitaker ||   ||
|-
!   | Hollywood Chainsaw Hookers
| Fred Olen Ray || Gunnar Hansen, Linnea Quigley, Jay Richardson ||   ||  
|-
!   |   John Hough Anthony Hamilton ||   ||  
|-
!   | Iced
| Jeff Kwitny || Debra A. Deliso, Doug Stevenson, Ron Kologie ||   ||  
|-
!   | The Jitters
| John Fasano || Sal Viviano, Marilyn Tokuda, James Hong ||    ||  
|-
!   | Killer Klowns from Outer Space
| Stephen Chiodo || Grant Cramer, Suzanne Snyder, John Allen Nelson ||   ||  
|- The Kiss
| Pen Densham || Joanna Pacuła, Meredith Salenger, Mimi Kuzyk ||    ||  
|- The Lair of the White Worm
| Ken Russell || Amanda Donohoe, Hugh Grant, Catherine Oxenberg ||   ||  
|-
!   | Lone Wolf Kevin Hart ||   ||  
|-
!   | Månguden Heinz Hopf, Tomas Laustiola ||   ||
|-
!   | Maniac Cop Tom Atkins, Bruce Campbell ||   ||  
|-
!   | Matinee Richard Martin Jeff Schultz ||   || {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Midnight Matinee.}} 
|-
!   | Memorial Valley Massacre Cameron Mitchell ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Valley of Death (USA) (video title).}}
|-
!   | Midnight Movie Massacre
| Mark Stock, Laurence Jacobs || Ann Robinson, Tamara Sue Hill, Jeanne Beachwood ||   ||  
|-
!   | Monkey Shines
| George A. Romero || Jason Beghe ||   ||  
|-
!   | The Moonlight Sonata
| Olli Soinio || Tiina Björkman ||   ||  
|-
!   | Necromancer (1988 film)|Necromancer
| Dusty Nelson || Elizabeth Kaitan, Russ Tamblyn, John Tyler||   ||  
|- The Nest
| Terence H. Winkless || Robert Lansing, Lisa Langlois ||   ||  
|-
!   |  
| Renny Harlin || Robert Englund, Lisa Wilcox, Rodney Eastman ||   ||  
|- Night of the Demons
| Kevin Tenney || Alvin Alexis, Allison Barron, Amelia Kinkade, Linnea Quigley, Cathy Podewell ||   ||  
|- Not of This Earth
| Jim Wynorski || Traci Lords, Ace Mask, Becky LeBeau ||   ||  
|-
!   | The Ogre: Demons 3
| Lamberto Bava || Paolo Malco, Virginia Bryant, Sabrina Ferilli ||   || Television film 
|-
!   | Out of the Body
| Brian Trenchard-Smith || Mark Hembrow, Tessa Humphries ||   ||  
|-
!   | Phantasm II
| Don Coscarelli || James LeGros, Reggie Bannister ||   ||  
|-
!   | Phantom Brother
| William Szarka || Jon Hammer, Patrick Molloy ||   ||
|-
!   | Pin...
| Sandor Stern || David Hewlett, Cynthia Preston, Terry OQuinn ||   || {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Pin: A Plastic Nightmare (Canada: English title).}} 
|-
!   | Poltergeist III Gary Sherman Nancy Allen, Heather ORourke ||   ||  
|-
!   | Prison (1988 film)|Prison
| Renny Harlin || Viggo Mortensen, Chelsea Field, Lane Smith ||   ||  
|-
!   | Pulse (1988 film)|Pulse
| Paul Golding || Cliff De Young, Roxanne Hart, Joey Lawrence ||   ||  
|-
!   | Pumpkinhead (film)|Pumpkinhead
| Stan Winston || Lance Henriksen, Florence Schauffler, Jeff East ||   ||  
|-
!   | Rabid Grannies
| Emmanuel Kervyn || Caroline Braeckman, Elie Lison, Catherine Aymerie ||     ||
|-
!   | Ratman
| Giuliano Carnimeo || Nelson de la Rosa, Werner Pochath, David Warbeck, Eva Grimaldi ||   ||  
|-
!   | Return of the Living Dead Part II
| Ken Wiederhorn || James Karen, Thom Matthews, Michael Kenworthy ||   ||  
|-
!   | Scarecrows (1988 film)|Scarecrows
| William Wesley || Mike Balog, David Campbell ||   ||  
|-
!   | Spellbinder (1988 film)|Spellbinder
| Janet Greek || Tim Daly, Kelly Preston, Rick Rossovich ||   ||  
|-
!   | Slugs (film)|Slugs
| Juan Piquer Simón || Michael Garfield ||    ||  
|-
!   | Sorority Babes in the Slimeball Bowl-O-Rama Ellen Cabot || Andras Jones, Robin Stille, Linnea Quigley ||   ||  
|- The Serpent and the Rainbow
| Wes Craven || Bill Pullman, Cathy Tyson, Zakes Mokae ||   ||  
|-
!   |  
| Michael A. Simpson || Pamela Springsteen, Brian Patrick Clarke, Renée Estevez ||   ||  
|-
!   | Slime City
| Greg Lamberson || T.J. Merrick, Bunny Levine, Dick Biel ||   ||
|-
!   | Tales from the Gimli Hospital
| Guy Maddin || Kyle McCulloch, Michael Gottli, Angela Heck ||   ||  
|- The Unholy
| Camilo Vila || Ben Cross, Ned Beatty, Claudia Robinson ||   ||  
|-
!   | Uninvited (1988 film)|Uninvited
| Greydon Clark || Alex Cord, George Kennedy, Shari Shattuck ||   ||  
|-
!   | Unmasked Part 25
| Anders Palm || Edward Brayshaw, Gary Brown, Gregory Cox ||   ||  
|- The Unnamable
| Jean-Paul Ouellette || Charles Klausmeyer, Mark Kinsey Stephenson, Alexandra Durrell ||   ||  
|-
!   | Veerana
| Shyam Ramsay, Tulsi Ramsay || Gulshan Grover, Sahila Chaddha, Hemant Birje ||   ||  
|-
!   | Watchers (1988 film)|Watchers Barbara Williams, Michael Ironside ||    ||  
|-
!   | Waxwork (film)|Waxwork David Warner ||   ||  
|-
!   | When Alice Broke the Mirror
| Lucio Fulci || Brett Halsey ||   ||  
|-
!   | Witchcraft (1988 film)|Witchcraft
| Rob Spera || Anat Topol, Gary Sloan, Mary Shelley, Lee Kissman ||   ||  
|-
!   | La Casa 4|Witchery
| Fabrizio Laurenti, Martin Newlin || David Hasselhoff, Linda Blair, Catherine Hickland ||   ||  
|-
!   | Zombi 3
| Lucio Fulci, Bruno Mattei || Marina Loi, Alex Mc Bride, Richard Raymond ||   ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
*  <!--
-->
 

 
 
 

 
 
 
 