Cloven Hoofed
 
{{Infobox film
| name           = Cloven Hoofed
| image          = Cloven Hoofed short film.jpg
| caption        = 
| director       = Dietmar Post
| producer       = Lucia Palacios Dietmar Post Michael ODonnell
| writer         = Dietmar Post
| based on       =  
| starring       = 
| music          = 
| cinematography = Claudia Amber
| editing        = Anton Salaks
| studio         = Play Loud! Productions 
| released       =  
| runtime        = 12 minutes
| country        = United States Germany
| language       = English
| budget         = 
| gross          = 
}} Peeping Tom. The American theatre actor Victor Pagan had its cinematic premiere in Cloven Hoofed. The film helped him to launch his movie career. He consequently appeared in TV series, such as, Sex and the City and The Sopranos. He also played roles in Steve Buscemis Animal Factory and Abel Ferraras R Xmas. Cloven Hoofed was the first collaboration between the director/producer couple Dietmar Post and Lucia Palacios. 

==Cast==
* Victor Pagan - Ray
* Kelly Webb - Girlfriend

==Plot==
"Harsh theatrical monologue by a desperate man. Ray is a crack addict who has not scored for too long. Withdrawal symptoms cloud his paranoid mind. He sharpens his knife to take it out on his girlfriend who fled with his pipe and his crack. Sometimes his cruel and violent sexual fantasies are visualized, but in general the insane verbal ravings of the goaded Ray predominate." Written by Gertjan Zuilhof, Programmer, 27th Rotterdam Film Festival, Netherlands  

==Reception==
Cloven Hoofed was well received by critics and gained recognition among lovers of underground films.

Bryan Wendorf, programmer at the Chicago Underground Film Festival states that "Cloven Hoofed is simple, disturbing and great."
Steve Puchalski of film magazine Shock Cinema thinks that "Cloven Hoofed is about as far from a date film as you are likely to find, this abrasive short has the same intensity as the best of the 80s underground efforts, except instead of their slap dash technique, Post recruits an expert crew for the project. Its much appreciated."

The film premiered at the 27th Rotterdam International Film Festival in February 1998. In their invitation, Rotterdam programmers said the intention was to include the film in "The Cruel Machine" (together with Peeping Tom), a program which is to "do justice to some very recent disturbing, but great films." "Films that revitalize the ongoing and urgent as never before creating debates about the really dark side of the image. By no means we are looking exclusively for the hard core or the extreme but we do not want to avoid it either. We would like to follow sincere, courageous and uncompromising film-makers all the way. Maybe sometimes the films in this programme will be tough to watch, but they will deserve the effort.”

==References==
 

==External links==
*  
*  

 
 
 
 