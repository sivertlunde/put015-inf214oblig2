The Twilight Saga: Breaking Dawn – Part 2
 
{{Infobox film
| name = The Twilight Saga: Breaking Dawn – Part 2
| image = The Twilight Saga Breaking Dawn Part 2 poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Bill Condon
| producer = {{Plainlist|
* Wyck Godfrey
* Stephenie Meyer
* Karen Rosenfelt}}
| screenplay = Melissa Rosenberg
| based on =  
| starring = {{Plainlist|
* Kristen Stewart
* Robert Pattinson
* Taylor Lautner Billy Burke
* Peter Facinelli
* Elizabeth Reaser
* Kellan Lutz
* Nikki Reed
* Jackson Rathbone
* Ashley Greene
* Michael Sheen
* Dakota Fanning}}
| music = Carter Burwell
| cinematography = Guillermo Navarro
| editing = Virginia Katz
| studio = {{Plainlist|
* Sunswept Entertainment
* Temple Hill Entertainment}}
| distributor = Summit Entertainment
| released =  
| runtime = 115 minutes  
| country = United States
| language = English
| budget = $120 million   
| gross = $829.7 million  http://www.boxoffice.com/statistics/movies/the-twilight-saga-breaking-dawn-part-2-2012 
}} Renesmee Cullen.
 41st highest-grossing film,  and the highest-grossing film of the The Twilight Saga (film series)|Twilight series.

==Plot==
  Bella awakens Jacob having imprinted on her child, List of Twilight characters#Renesmee Cullen|Renesmee. It also appears that Bellas father, List of Twilight characters#Charlie Swan|Charlie, has been attempting to contact the Cullens for updates on Bellas illness. They intend to tell him she didnt survive, which requires that they move out of Forks, Washington to protect their identities. Jacob, desperate not to lose Renesmee, tells Charlie that his daughter is in fact alive and well, and explains that Bella has had to change in order to survive. He morphs into a wolf, revealing his tribes shape-shifting power, but does not tell Charlie about vampires.
 Carlisle monitoring Irina sees immortal child. Immortal children were those who were frozen in childhood, and because they could not be trained nor restrained, they destroyed entire villages. They were eventually executed, as were the parents who created them, and the creation of such children outlawed. Irina goes to the Volturi to report what she has seen. Alice sees the Volturi and Irina coming to kill the Cullens and instructs the others to gather as many witnesses as they can to testify that Renesmee is not an immortal. The Cullens begin to summon witnesses, such as the Denali family. One of the Denali, Eleazar, later discovers that Bella has a special ability: a powerful mental shield, which she can extend to protect others from mental attacks.

As some of their potential witnesses are attacked and prevented from supporting the Cullens, Carlisle and Edward realize they may have to fight the Volturi. Their witnesses ultimately agree to stand with them in battle, having realized the Volturi increase the Guard by falsely accusing covens of crimes to gain vampires with gifts. The Volturi arrive prepared for battle, led by List of Twilight characters#The Volturi|Aro, who is eager to obtain the gifted members of the Cullen coven as part of his guard. Aro is allowed to touch Renesmee, and is convinced that she is not an immortal child. Irina is brought forth and takes full responsibility for her mistake, leading to her immediate death. Aro still insists that Renesmee may pose a risk in the future, validating his claim that battle is necessary. Before any violence, Alice shares with Aro her vision of the battle that is to come, during which both sides sustain heavy casualties, including Aro himself. Aro believes her, giving Alice and Jasper an opportunity to reveal their witness (which is a half mortal half vampire just like Renesmee). The witness proves that he is mature, supporting the notion that Renesmee is not a threat. The Volturi leave without a fight.

Back at the Cullen home, Alice glimpses the future, seeing Edward and Bella together with Jacob and a fully matured Renesmee. Edward reads Alices mind and feels relieved that Renesmee has Jacob to protect her. Alone in the meadow, Bella pushes her mental shield away and finally allows Edward a peek into her mind, showing him every precious moment she and Edward shared together and the two share a kiss.

The end credits present the cast members from all five films.

==Cast==
 
 
 
  Isabella "Bella" Cullen (née Swan)
* Robert Pattinson as Edward Cullen
* Taylor Lautner as Jacob Black Renesmee Cullen Carlisle Cullen Esme Cullen Alice Cullen Emmett Cullen Rosalie Hale Jasper Hale Irina
* Aro
* Caius
* Jane
* Marcus
* Billy Burke Charlie Swan Garrett
* Kate Denali Tanya
* Vladimir
* Joe Anderson Alistair
* Alec
* Tia
* Aldo Quintino as Sena Benjamin
* Seth Clearwater Felix
* Eleazar
* Carmen
* Phil
* Paul
* Zafrina
* Demetri
* Nahuel
* Wendell Pierce as List of Twilight characters#J. Jenks|J. Jenks Leah Clearwater Santiago
* Sasha
* Mary
* Kebi
* Austin Naulty as Werewolf Embry Call Sam Uley Jared
* Huilen
* Amun
* Charlotte
* Senna
* Maggie
* Stefan
* Peter
* Lisa Howard Siobhan
* Randall
* Liam
* Henri
* Yvette
* Toshiro
* Dredae Blackman as Annabeth
 
 cameos during the ending credits.   

==Production==
===Development=== Charlie Swan, would return for both parts.

===Pre-production=== Catherine Hardwickes movie". Condon explains, "Like, everything that got set up there gets resolved here. I think youll find that there are stylistic and other nods to that film."
 Clash of the Titans (2010).  However, it was confirmed on February 12, 2012 that Part 2 would not be filmed in 3D. 

===Filming=== Filming started on November 1, 2010 and wrapped, for most of the cast, on April 15, 2011, ending the franchises three years of production since March 2008. Filming was shot on location in Baton Rouge, Louisiana; Vancouver, British Columbia; New Orleans; and at the Raleigh Studios in Baton Rouge.

On the subject of the final day and her final moment as Bella, Stewart stated, "After that scene, my true final scene, I felt like I could shoot up into the night sky and every pore of my body would shoot light. I felt lighter than Ive ever felt in my life."  Pattinson thought the day was "amazing" and commented, "I then asked myself why we didnt do this in those four years. Every difficult moment just vanished."

In April 2012, the crew and cast, including Pattinson and Stewart, returned for reshoots to pick up some additional shots for technical work with some of the cast and stunt actors. However, these re-shoots did not include any new scenes or dialogue. 

===Special effects===
Tippett Studio first began working on the CGI (computer-generated imagery) wolves in February 2009 for The Twilight Saga: New Moon, and the look of the creatures has evolved, becoming more photo real over the course of the saga, with the input of three different directors. "Its a subtle balance of just how anthropomorphic these wolves are," says Eric Leven. "Bill (Condon) wanted to make sure that we had a sense of the human or the shape shifter in there. Finding that balance of how much of a human performance versus an animal performance was important for Bill."

Leven adds, "Bill has always treated the wolves as characters and never as computer generated things, and directs them in the same way hed direct any actor. He would always give us direction like Sam should be angrier. Its the best way to work. His treating these creatures as characters, instead of just computer bits, was really great."

"Because weve been working on this franchise for such a prolonged period of time, weve been able to improve the look from show to show," comments Phil Tippett. "Wolves generally are pretty darn clean and since Bill wanted the wolves rangier, that means a lot more fur matting and clumping, like theyve lived out in the woods. We edged towards something a bit more feral."

"However, there is also a balance between look and technology," adds Tippett. "The body count of the wolves escalates and because were adding a great deal more hair to get the right texture, that fur really ups the rendering time. Weve gone from four wolves to eight to twelve, to sixteen in Part 2. So we have to be very careful about that balance, because it takes hundreds of hours to render each wolf." 

===Music===
  The Forgotten", performed by the American rock band Green Day. "A Thousand Years, Pt. 2" by the American singer Christina Perri is also featured on the soundtrack album.

 , returned to score the final installment of the series. In later announcements, Burwell confirmed that the score for the film was complete. "The movie basically upholds the final installment with a score that has the same jungle-music feeling   brought us," Burwell affirmed. "The music pieces that take place in the catalytic final battle will be very much like the nineteenth song in the previous movies score, Its Renesmee and the twenty-fourth, You Kill Her You Kill Me, which were ,if not the most, one of the boldest pieces in my career. I will tease anything but I recommend for the fans to listen to A Kick in the Head, Exacueret Nostri Dentes in Filia and Aros End if you want to have goosebumps for the rest of your life" 

==Release==
===Box office=== 40th highest-grossing 6th highest-grossing film of 2012, also the highest-grossing film of the The Twilight Saga (film series)|Twilight series. It had a $340.9 million worldwide opening, which was the eighth-largest ever, the largest for the Twilight franchise and the largest for a film released outside the summer period. 

In North America, the film grossed $30.4 million in Thursday night and midnight showings, achieving the third highest midnight gross   and highest midnight gross of the franchise.   Breaking Dawn – Part 2 made an $71.2 million on its opening day, which is the sixth highest opening and single day gross as well as the third highest opening and single day gross of the franchise.  For its opening weekend, the movie earned $141.1 million,  which is the ninth highest-grossing opening weekend of all-time,   the second highest-grossing one of the franchise,  the third largest November opening  and the fourth largest 2012 opening.  It retained first place in its second weekend by dropping 69.1% with a gross of $43.6 million over the three-day weekend and made a total of $64.4 million over the 5-day Thanksgiving holiday weekend.  In its third weekend, Breaking Dawn Part – 2 held onto the No. 1 spot again by dropping 60.1% and grossing $17.4 million.  It became the third highest-grossing film of the franchise behind   and  . 

Outside North America, the film opened on Wednesday, November 14, 2012 in 6 countries earning $13.8 million. By Thursday, it had opened in 37 territories, earning $38.8 million. In all territories it opened with similar or higher earnings than its immediate predecessor.  Through its first Friday, it earned $91.0 million, after expanding to 61 territories.   By the end of its opening weekend (Wednesday-to-Sunday), it scored a series-best $199.5 million opening from 61 territories on 12,812 screens. This is the eighth-largest opening outside North America and the largest 2012 opening.  IMAX showings generated $3 million from 82 locations.  The films largest openings were recorded in the UK, Ireland and Malta ($25.2 million), Russia and the CIS ($22.0 million), and France and the Maghreb region ($17.9 million).    In Spain, it set a 3-day opening-weekend record with $11.9 million.  In total earnings, its three highest-grossing markets after North America are the UK, Ireland and Malta ($57.9 million), Brazil ($54.2 million), and Russia and the CIS ($42.8 million). 

===Critical response===
The film received mixed reviews from critics,  but the reviews were much more favorable than those of  . At   it holds a score of 52 out of 100, based on 31 reviews.  The majority of praise from both fans and critics went towards the ending sequence, Michael Sheens performance as the Volturi leader Aro and Lee Paces performance as vampire Garrett.

Todd McCarthy of The Hollywood Reporter wrote, "The final installment of the immortal Bella/Edward romance will give its breathlessly awaiting international audience just what it wants".  Owen Gleiberman of Entertainment Weekly said, "Breaking Dawn – Part 2 starts off slow but gathers momentum, and thats because, with Bella and Edward united against the Volturi, the picture has a real threat".  Sara Stewart of the New York Post wrote, "Finally, someone took the source material at its terribly written word and stopped treating the whole affair so seriously".  Justin Chang of Variety (magazine)|Variety praised the performance of Stewart by saying, "No longer a mopey, lower-lip-biting emo girl, this Bella is twitchy, feral, formidable and fully energized, a goddess even among her exalted bloodsucker brethren".  Manohla Dargis of The New York Times said, "Despite the slow start Mr. Condon closes the series in fine, smooth style. He gives fans all the lovely flowers, conditioned hair and lightly erotic, dreamy kisses they deserve". 

Roger Ebert of the Chicago Sun-Times gave the film two and a half stars out of four, saying "its audience, which takes these films very seriously indeed, will drink deeply of its blood. The sensational closing sequence cannot be accused of leaving a single loophole, not even some of those we didnt know were there".    However, he concluded by saying, ""Breaking Dawn, Part 2" must be one of the more serious entries in any major movie franchise... it bit the bullet, and I imagine fans will be pleased."  Helen OHara of Empire (magazine)|Empire gave the film a mixed review and said, "Fans will be left on a high; other viewers will be confused but generally entertained by a saga whose romance is matched only by its weirdness". 

==Home media==
The Twilight Saga: Breaking Dawn – Part 2 was released on DVD and Blu-ray Disc|Blu-ray on March 2, 2013.  As of June 1, 2014, Breaking Dawn Part 2 has sold 4,810,249 DVDs along with 1,224,869 Blu-ray Discs for $71,418,469 and $24,472,107 respectively totaling $99,195,325. 

==Awards and nominations==
 

{| class="wikitable" rowspan=6; style="text-align: center; background:#ffffff;"
!Year!!Award!!Category!!Recipient(s)!!Result!!Ref.
|- 2013
|rowspan=5|Empire Cinemas Alternative Movie Awards Best On-Screen Couple Edward Cullen (Robert Pattinson) and Bella Swan (Kristen Stewart)
| 
|rowspan=5|   
|- Best On-Screen Kiss Edward Cullen (Robert Pattinson) and Bella Swan (Kristen Stewart)
| 
|- Best Fight Scenes
|Breaking Dawn – Part 2
| 
|- Best Film Villain Aro (Michael Sheen)
| 
|- Best Male Body Jacob Black (Taylor Lautner)
| 
|- 33rd Golden 2013
|rowspan=11|Golden Raspberry Awards Golden Raspberry Worst Picture
|Breaking Dawn – Part 2
| 
|rowspan=11| {{cite web|url=http://www.razzies.com/history/13winners.asp|title=The 33rd Annual RAZZIE® Awards
|author=|accessdate=1 April 2013|work=Razzies.com}} 
|- Golden Raspberry Worst Actor Robert Pattinson
| 
|- Golden Raspberry Worst Actress Kristen Stewart  (for Breaking Dawn – Pt. 2 and Snow White and the Huntsman) 
| 
|- Golden Raspberry Worst Supporting Actor Taylor Lautner
| 
|- Golden Raspberry Worst Supporting Actress Ashley Greene
| 
|- Golden Raspberry Worst Screen Couple Mackenzie Foy and Taylor Lautner
| 
|- Robert Pattinson and Kristen Stewart
| 
|- Golden Raspberry Worst Prequel, Remake, Rip-off or Sequel
|Breaking Dawn – Part 2
| 
|- Golden Raspberry Worst Director Bill Condon
| 
|- Golden Raspberry Worst Screenplay Melissa Rosenberg and Stephenie Meyer
| 
|- Golden Raspberry Worst Screen Ensemble Entire cast of Breaking Dawn – Part 2
| 
|- 2013 MTV 2013
|MTV Movie Awards 2013 MTV Best Shirtless Performance Taylor Lautner
| 
|   
|- 2013
|Moviefone Fonie Award Most Extreme Role Adjustment Kristen Stewart On the Road) 
| 
|   
|- 2013 Kids 2013
|Nickelodeon Kids Choice Awards 2013 Kids Favorite Movie Actress Kristen Stewart
| 
|   
|- Nickelodeon UK 2013
|Nickelodeon UK Kids Choice Awards Nickelodeon UK Favourite UK Actor Robert Pattinson
| 
|   
|- 39th Peoples 2013
|Peoples Choice Awards 39th Peoples Favorite Movie Fan Following Twihards
| 
|   
|- 2013
|Richard Attenborough Film Award British Performer of the Year Robert Pattinson
| 
| {{cite web|url=http://betheredcarpet.co.uk/2013/02/04/robert-pattinson-wins-british-performer-of-the-year/|title=Robert Pattinson wins British Performer of The Year
|author=|date=4 February 2013|work=BeTheRedCarpet.co.uk}} 
|- 2013
|rowspan=2|Virgin Media Award Hottest Movie Actor Robert Pattinson
| 
|rowspan=2|   
|- Hottest Movie Actress Kristen Stewart
| 
|- 34th Young 2013
|Young Artist Award 34th Young Best Performance in a Feature Film - Supporting Young Actress Mackenzie Foy
| 
|   
|- 2013
|rowspan="7"|Teen Choice Awards
| Actress Romance
| Kristen Stewart
| 
|
|- Teen Choice Scene Stealer Kellan Lutz
| 
|- Teen Choice Award for Choice Movie Actor - Sci-Fi/Fantasy|Sci-Fi/Fantasy Actor Taylor Lautner
| 
|- Teen Choice Award for Choice Movie Actress - Sci-Fi/Fantasy|Sci-Fi/Fantasy Actress Kristen Stewart
| 
|-
| Teen Choice Award for Choice Movie - Sci-Fi/Fantasy|Sci-Fi/Fantasy
|Breaking Dawn - Part 2
| 
|- Actor Romance Robert Pattinson
| 
|- Romance
|Breaking Dawn - Part 2
| 
|}

==See also==
* Vampire film

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 