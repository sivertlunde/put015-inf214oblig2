Manku Thimma
{{Infobox film 
| name           = Manku Thimma
| image          =  
| caption        = 
| director       = H. R. Bhargava
| producer       = Dwarakish
| based on       = Remake of Mastana (Hindi) (1970)
| writer         = Chi. Udaya Shankar
| screenplay     = H. R. Bhargava Manjula
| music          = Rajan-Nagendra
| cinematography = D V Rajaram N K Sathish
| editing        = Yadav Victor
| studio         = Dwarakish Chithra
| distributor    = Dwarakish Chithra
| released       =  
| runtime        = 139 min
| country        = India Kannada
}}
 1980 Cinema Indian Kannada Kannada film, Manjula in lead roles. The film had musical score by Rajan-Nagendra.  

==Cast==
 
*Dwarakish
*Srinath
*Padmapriya Manjula
*Manu
*Hema Choudary
*Baby Lakshmi
*Thoogudeepa Srinivas
*Sudheer
*Arikesari
*Chethan Ramarao
*Shivaprakash
*Sharapanjara Iyengar
*Uma Shivakumar
*B. Jayashree
*Papamma
*Srigowri
*Kokila
*Prabhakar Reddy in guest appearance
*Chanda in guest appearance
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ninna Nodidaaga || S. Janaki|Janaki, S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.30
|- Janaki || Chi. Udaya Shankar || 04.33
|-
| 3 || Nanna Muddu Thaare || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.17
|- Janaki || Chi. Udaya Shankar || 04.15
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 