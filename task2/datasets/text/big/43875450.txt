Kalyana Kurimanam
{{Infobox film
| name = Kalyana Kurimanam
| image =
| image_size =
| caption =
| director = Udaya Kumar
| producer = S Kumar
| writer =
| screenplay = Abbas Vijayaraghavan Vijayaraghavan Nandana Nandana
| music = Ronnie R Raphel
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| country = India Malayalam
}}
 2005 Cinema Indian Malayalam Malayalam film, Vijayaraghavan and Nandana in lead roles. The film had musical score by Ronnie R Raphel.  

==Cast== Abbas  Vijayaraghavan  Nandana

==Soundtrack==
The music was composed by Ronnie R Raphel.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Dil Dil Strawberry || Franco, Rachana John || Bichu Thirumala || 04.29 
|- 
| 2 || Kalyanam Kalyanam || M. G. Sreekumar || Bichu Thirumala || 03.05 
|- 
| 3 || Keralam Oru || Vidhu Prathap, Jyotsna, Nimmi, || Bichu Thirumala || 04.50 
|- 
| 4 || Mazhanilavin || Jayachandran || Bichu Thirumala || 04.31 
|- 
| 5 || Mindattam Venda || Madhu Balakrishnan, Rimi Tomy || Bichu Thirumala || 04.48 
|-  Chithra || Bichu Thirumala || 05.04 
|- 
| 7 || Thalirani Mulle || Sujatha, Jyotsna || Bichu Thirumala || 04.11 
|- 
| 8 || Thotte Thotte || Afsal || Bichu Thirumala || 03.34 
|}

==References==
 

==External links==
*  

 
 
 


 