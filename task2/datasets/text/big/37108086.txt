All Is Lost
 
{{Infobox film
| name           = All Is Lost
| image          = All is Lost poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = J. C. Chandor
| producer       = {{Plainlist|
*Justin Nappi
*Teddy Schwarzman
*Neal Dodson
*Anna Gerb
}}
| writer         = J. C. Chandor
| starring       = Robert Redford
| music          = Alex Ebert
| cinematography = Frank G. DeMarco
| editing        = Pete Beaudreau
| studio         = {{Plainlist| Before the Door Pictures
*Washington Square Films
}}
| distributor    = {{Plainlist| Lionsgate  
* FilmNation Entertainment   
}}
| released       =  
| runtime        = 105 minutes 
| country        = {{Plainlist|
*Canada   
*United States 
}}
| language       = English
| budget         = $8.5 million  
| gross          = $10.2–13.6 million  
}} survival drama Margin Call. out of competition at the 2013 Cannes Film Festival.

==Plot==
Somewhere in the Indian Ocean ("1700 nautical miles from the Sumatra Straits"), a man (Robert Redford) says, "Im sorry. I know that means little at this point, but I am. I tried. I think you would all agree that I tried. To be true, to be strong, to be kind, to love, to be right, but I wasnt." He declares, "All is lost."
 Cal 39 shipping container which has ripped a hole in the Hull (watercraft)|hull. He goes below to get a sea anchor and ties it to the container. The emblazoned container—painted "好運" in Chinese and "Ho Wan" in English—meant "Good Fortune."   After dislodging the container, water is streaming into his boat.  In order to correct this, he has to head in the opposite direction of his original course in order to tilt the boat away from the hole.  He goes to work patching up the hole in his boat and uses the manual- or hand-bilge pump hole to remove the water from the cabin. After the cleanup, exhausted, he finds that the boat’s navigational and communications systems have been damaged, due to saltwater intrusion as a result of the collision.  He tries to repair the radio and connects it to one of the boats batteries but is ultimately unsuccessful.  When he climbs the mast to repair an antenna lead, he sees an oncoming tropical storm.  He immediately descends to make preparations for it.

The storm quickly reaches his position, and he runs before the wind under bare poles for a while, until he feels this storm tactic becomes too tiresome and dangerous. He intends to bring the boat into a Heaving to|hove-to position, but when crawling to the bow to hoist the storm jib, he is thrown overboard and only just regains the deck after a long struggle. The boat capsizes and Turtling (sailing)|turtles, and after a further 180-degree roll, is dismasted, and most of the equipment on board destroyed.  With the boat badly holed and sinking, he decides to abandon ship in an inflatable life raft, salvaging whatever he can to survive.

As he learns how to operate a sextant he recovered from the boat, he discovers he is near one of the major shipping lanes and, a day or two later, finds that he is being pulled towards it by ocean currents. During the journey, he survives another storm. But his supplies dwindle, and he learns too late that his drinking water has been contaminated with sea water. He improvises a solar still from his water container and a plastic bag to get fresh water.

He reaches the shipping lanes and is passed by two container ships. They do not notice him and continue on, despite his use of signaling flares. He eventually drifts out of the shipping lanes and back to open ocean. However, he is out of food and water and cannot hope to survive much longer.  On the eighth day, he writes a letter, puts it in a jar, and throws it in the water as a message in a bottle for anyone to find.

Later that night, he sees a light in the distance, possibly another ship.  He doesnt use his last signaling device, but tears pages from his journal along with charts to create a signal fire. After he loses control of the fire and the fire consumes his raft, he falls into the water, struggling to swim. He stops swimming and lets himself sink. As he sinks, he sees the hull of a boat with a search light approaching his burning raft.

He swims up towards the light and the surface to grasp an outstretched hand, and the scene dissolves into white.

==Production==

===Development=== Margin Call. During his time commuting from Providence, Rhode Island to New York, Chandor developed the idea for All Is Lost.    After meeting Robert Redford at the 2011 Sundance Film Festival where Margin Call premiered, Chandor asked the veteran actor to be in the film. On February 9, 2012, Redfords casting was confirmed for All Is Lost as its only cast member.    In addition to there being only one actor in the film, Redford also stated that the film has no dialogue, however there are a few spoken lines.    Because of these aspects, the shooting script was only 31 pages long. 

===Filming===
Principal photography began in mid-2012 at Baja Studios in Rosarito Beach in Mexico. Baja Studios was originally built for the 1997 film Titanic (1997 film)|Titanic.  Filming took place for two months in the locations water tank.    In addition the crew spent "two or three days" filming in the actual ocean.    Chandor would later remark that completing the film was "essentially a jigsaw puzzle"  and that the crew spent less time on the actual ocean than the film would have viewers believe.  At a press conference after the films screening at Cannes 2013, Redford revealed that his ear was damaged during the production. 

Being filmed on the water and largely within the confines of a sailboat and liferaft, the film was technically difficult.  It joined ranks with other water-plagued films: Alfred Hitchcocks Lifeboat (film)|Lifeboat; Roman Polanskis Knife in the Water; Kevin Costners Waterworld; {{cite news |url=http://articles.latimes.com/1994-09-16/entertainment/ca-39211_1_summer-movie |title=Plenty of Riptides on Waterworld Set: With key crew people quitting and reported turmoil, logistical and organizational problems, the big-budget film, scheduled for release in summer of 95, could end up costing more than any movie ever made.
|date=September 16, 1994 |first1=Claudia |last1=Eller |first2=Robert W. |last2=Welkos |newspaper=Los Angeles Times |accessdate=November 20, 2014}}   James Camerons Titanic (1997 film)|Titanic; Robert Zemeckiss Cast Away  and Steven Spielbergs Jaws (film)|Jaws. 

===Music===
The film score to All Is Lost is composed by   explicate on the unique development of the sound track, music, script and other production considerations.

A soundtrack album featuring ten original compositions and one new song all written, composed, and produced by Ebert was released on October 1, 2013 by Community Music.  On September 12, 2013, the song "Amen" from the soundtrack was made available for streaming. 

==Release== Lionsgate and Universal Pictures purchased distribution of the film in 19 international territories (U.K., France, Italy, Spain, Hong Kong, South Korea, Russia, Portugal and Australia). Other deals were made with HGC in China, Square One in Germany, Sun Distribution in Latin America and Pony Canyon in Japan.  It began a limited release in the United States on October 18, 2013.

==Reception==
Film review aggregator   the film has a score of 87 based on 45 reviews, considered to be "universal acclaim". 
 The Telegraph said, "The films scope is limited, but as far as it goes, All Is Lost is very good indeed: a neat idea, very nimbly executed."   

Peter Bradshaw, writing also for The Guardian, says the "near-mute performance as a mysterious old man of the sea" to be "a bold, gripping thriller."  Being an ambiguous and challenging metaphor, he concludes: "What a strikingly bold and thoughtful film." 
 Jeremiah Johnson. Today, at age 77, without a supporting cast and performing virtually all of his water stunts himself, Redford proves he is still up to the task, shining in what is an extremely physical but also an intellectually demanding role." 

Steve Pulaski of Influx Magazine gave the film an A−, commenting, "All is Lost is a strong film in terms of mood and score. I have no idea what audiences will think of it. Some will hail it and others will loathe it for not getting more to the bottom of things and leaving each scene with some element of ambiguity." 

The film has been criticized in the sailing world for being unrealistic, and in particular for the bad decisions of the main character,   Yacht (magazine) (retrieved 16.11.2014) 
  about sports (retrieved 16.11.2014) 
without details, but in a list of four films with maritime mistakes:     that everything that happened in the film could have happened in reality. His only reservations were the probability of crossing the Indian Ocean single-handed and of not evading the storm with modern technology and due attention. 

==Accolades==
{| class="wikitable" width="90%"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"

! Award
! Category
! Recipients and nominees
! Result
|- AARP The AARP Annual Movies for Grownups Awards 
| Judges Award for Extraordinary Merit
| All is Lost
|  
|- 86th Academy Awards Academy Award Best Sound Editing Steve Boeddeker, Richard Hymns
| 
|- Chicago Film Critics Association Chicago Film Best Actor Robert Redford
| 
|-
|Critics Choice Movie Awards Broadcast Film Best Actor Robert Redford
| 
|- Detroit Film Critics Society  Best Actor Robert Redford
| 
|- 71st Golden Globe Awards       Golden Globe Best Actor – Motion Picture Drama Robert Redford
| 
|- Golden Globe Best Original Score Alex Ebert
| 
|- Gotham Awards  Best Actor Robert Redford
| 
|- Independent Spirit Awards  Independent Spirit Best Feature
|
| 
|- Independent Spirit Best Director
|J. C. Chandor
| 
|-  Independent Spirit Best Male Lead Robert Redford
| 
|- Independent Spirit Best Cinematography Frank G. DeMarco
| 
|- Motion Picture Sound Editors Golden Reel Awards   Best Sound Editing: Sound Effects & Foley in a Feature Film Richard Hymns, Steve Boeddeker
| 
|- New York Film Critics Circle  New York Best Actor Robert Redford
| 
|- Phoenix Film Critics Society  Best Actor in a Leading Role Robert Redford
| 
|- San Francisco Film Critics Circle San Francisco Best Actor Robert Redford
| 
|- Best Editing Pete Beaudreau
| 
|- Satellite Awards  Satellite Award Best Motion Picture
|
| 
|- Satellite Award Best Actor – Motion Picture Robert Redford
| 
|- Satellite Award Best Sound (Editing and Mixing) Brandon Proctor, Richard Hymns, Steve Boeddeker
| 
|- Satellite Award Best Visual Effects Brendon ODell, Collin Davies, Robert Munroe
| 
|- Washington D.C. Area Film Critics Association  Washington D.C. Best Actor Robert Redford
| 
|-
|}

==See also==
* Survival film, about the film genre, with a list of related films

==Notes==
 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 