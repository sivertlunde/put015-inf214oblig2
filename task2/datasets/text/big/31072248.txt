Men Without Souls
 
{{Infobox Film name       = Men Without Souls image      = caption    =  director   = Nick Grinde producer   =  writer     = Harvey Gates Robert Hardy Andrews music =    cinematography  =  editing =  starring   = Barton MacLane John Litel Glenn Ford Rochelle Hudson
|distributor=  Columbia Pictures released   =  May 20, 1940 runtime    =  62 min   country   = EEUU language  English
|}}

----
Men Without Souls (1940) is a Crime movie, starring Barton MacLane - Glenn Ford and directed by Nick Grinde

----

== Plot ==
Johnny Adams (Glenn Ford) goes to prison, under a false name, with the intention of killing the captain White (Cy Kendall), a bastard guard, who had been Johnnys father to death. Rev. Thomas Stoner (John Litel) newcomer, is the chaplain of the jail, the father is the mayors opposition Schafer (Don Beddoe), Stoner finds out the intentions of Johnny and persuades him to follow his plan, but when "Blackie" Drew (Barton MacLane) kills White, Johnny blamed

== Cast ==
{|class="wikitable"
|-
!Actor !! Role
|- Barton MacLane || Blackie Drew  
|- John Litel || Reverend Thomas Storm  
|- Rochelle Hudson || Suzan Leonard    
|- Glenn Ford || Johnny Adams  
|- Don Beddoe || Warden Schafer  
|- Cy Kendall ||  Capt. White   
|-  Eddie Laughton || Lefty    
|- Dick Curtis || Duke     
|- Richard Fiske || Crowley  
|- Walter Soderling || Old Muck   
|- Walter Sande || First Reporter (unconfirmed)   
      
|}

== References ==
*Gunmen and gangsters: profiles of nine actors who portrayed memorable screen, by Michael Schlossheimer
*The American movies reference book: the sound era, by Paul Michael, James Robert Parish, Prentice-Hall, inc

==External links==
* 

 
 
 
 
 
 
 


 