The Cheyenne Social Club
{{Infobox film
| name           = The Cheyenne Social Club
| image          = The Cheyenne Social Club original poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Original cinema poster
| director       = Gene Kelly 
| producer       = James Lee Barrett Gene Kelly
| writer         =  James Lee Barrett
| screenplay     = 
| story          = 
| narrator       = 
| starring       = James Stewart Henry Fonda
| music          = Walter Scharf
| cinematography = 
| editing        = 
| studio         = National General Pictures
| distributor    = National General Pictures (US) Cinerama Releasing (UK)
| released       =   
| runtime        = 103 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
}} Western comedy film|comedy, written by James Lee Barrett, directed and produced by Gene Kelly, and starring James Stewart, Henry Fonda and Shirley Jones. Its the story about an aging cowboy who inherits a brothel and decides to turn it into a respectable boarding house, against the wishes of both the townspeople and the ladies working there.

==Plot==
In 1867, John OHanlan (Stewart) and Harley Sullivan (Fonda) are aging cowboys working on open cattle ranges in Texas. OHanlan gets a letter from an attorney in Cheyenne, Wyoming, that his disreputable and now deceased brother, DJ, left him something called The Cheyenne Social Club in his will (law)|will.

After they make the 1,000 mile (1,600&nbsp;km) trek to Cheyenne, OHanlan and Sullivan learn that The Cheyenne Social Club is a high-class brothel next to the railroad. OHanlans new-found status as a man of property makes him the most popular man in town, until he decides to turn the Club into a respectable boarding house.

The ladies of the Club show no sign of leaving. John gets into a bar-room brawl with several men who are equally angry at the prospect of the Club closing. John then learns from DJs lawyer that DJ had made a deal with the railroad: if the ladies leave the Club, the land the Club is on will revert to the railroad.

John returns to the Club to discover that Jenny, the head girl (Jones), has been assaulted by a man named Corey Bannister. John, with Harley following along, arms himself and goes to the bar where Bannister is. John kills Bannister when Bannister mistakes Harleys cracking pecans for a second gun. "Just like DJ would have done" the barkeeper intones of Johns heroics.

The Sheriff advises John and Harley that Bannisters relatives are sure to head for Cheyenne once they learn of Bannisters death. He says he would like to stay and help John and Harley face down the Bannisters, but has to leave town on business.

Harley heeds the Sheriffs warning and leaves for Texas in spite of Johns pleads to stay. En route, Harley meets several men at a campfire. While engaging in conversation with the men, Harley discovers they are the Bannisters. He gets on his horse and rides on.

The Bannisters show up at the Club and a gunfight ensues. John, with help from Jenny, kills two Bannisters from the window.  A third Bannister enters the house through a back door and is killed by Jenny.  Harley, who has returned, kills the fourth Bannister after climbing the railroad water tower. John yells, "Is that you Harley?"  The head Bannister hears this and remembers Harley as the man who approached them at the campfire. He shoots at Harley, but is gunned down by John. The sixth Bannister runs away.

John and Harley are feted at the bar which had formerly shunned them. The Sheriff congratulates them and then tells them 20 to 30 of the Bannisters cousins, the Markstones, are heading to Cheyenne. He says he would like to stay and help John and Harley face the Markstones, but has to leave town again on business.

This time, John decides to leave and he has DJs lawyer transfer ownership of the Club to Jenny. Months later, while working cattle on the range in Texas, John receives a letter from Jenny. He is touched by it, but tosses it into the fire before him. Harley is upset John has destroyed the letter because he wanted to read it. They then ride off together, arguing.

==Production== How the West Was Won (1962) but had no scenes together despite playing best friends.
 Santa Fe, New Mexico, while the interiors were shot at the Samuel Goldwyn Studios in Hollywood, California|Hollywood.

==Reception== The Out-of-Towners.

==Cast==
*James Stewart as John OHanlan
*Henry Fonda as Harley Sullivan
*Shirley Jones as Jenny
*Sue Ane Langdon as Opal Ann
*Elaine Devry as Pauline
* Jackie Russell as Carrie Virginia
*Jackie Joseph as  Annie Jo
* Sharon DeBord as Sara Jean
*Robert Middleton as Barkeep
*Robert Wilke "as" Corey Bannister

==References==
 

== External links ==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 