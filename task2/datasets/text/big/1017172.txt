Red Nightmare
 
{{Infobox film
| name = Red Nightmare
| image = rednightmare.jpg
| image_size =
| alt =
| caption = Jack Webb introducing Red Nightmare
| director = George Waggner
| producer = William L. Hendricks Jack Webb
| writer = Vincent Fotre Jack Kelly Jeanne Cooper Howard Jackson William Lava
| cinematography = Robert Hoffman
| distributor = Warner Bros.
| country = United States
| editing = Folmer Blangsted
| released =  
| runtime = 29 min./60 min. (1985 video release)
| language = English
}}
Red Nightmare is the best known title of Armed Forces Information Film (AFIF) 120, Freedom and You.  It was an anti-communist propaganda short presented as an educational film about the nature of Communism. The films chief use was to mold public opinion against communism. The film was later released to American television and as an educational film to American schools under the Red Nightmare title.
 Jack Kelly The Wolf Department of Defense, it was shown on American television on Jack Webbs GE True in 1962.

==Plot==
A man takes his American freedoms for granted, until he wakes up one morning to find out that the United States Government has been replaced with a Communist system. The basis for this short film, narrated by Jack Webb, is the alleged Soviet re-creation of US communities for the purpose of training infiltrators, spies, and moles. 

The film begins in what looks like a typical American town. The camera moves back to reveal barbed wire, barricades, and soldiers in Soviet Army uniforms. Narrator Jack Webb informs us that there are several places behind the Iron Curtain used for training Soviet espionage and sabotage forces prior to infiltrating America.
 Jack Kelly), Peter Brown) PTA meeting Army Reserve training does not go over well with Helen. Linda and Bill inform Jerry and Helen they wish to get married but Jerry is angered and says they are too young, but he would have no objection if they waited five years after university.

Jack Webb explains how safe Jerry is in his world, but when Jerry goes to sleep, Webb looks grim and tells the audience Jerry is going to have a Red Nightmare.
 farm collective Russian Army PTA on the glories of communism, which Jerry refuses to do, but his wife says he has no choice. At work, Jerrys foreman (Robert Conrad) tells him that he has not met his quota and must work through the lunch break to meet it.
On Sunday morning, Jerry wakes to find his two youngest children being sent to a State Communist school against his wishes. Jerry insists on the children going to Sunday School, and takes them to their church that has been turned into a museum glorifying the Soviet Union, including many inventions made by Americans which the Soviets claim to have invented. Jerry knocks the exhibits over, and is arrested by troops led by a Commissar (Peter Breck).

Jerry is brought to trial at a Soviet tribunal (Judge, Andrew Duggan; prosecutor, Mike Road), where there is no jury nor a defense attorney. Jerry demands to know what he is charged with, but the rights Americans take for granted are long gone. After condemning testimony from several witnesses, including his own wife, Jerry is convicted and sentenced to death. When he is strapped into the execution chair, Jerry makes a speech about the Soviet people awakening one day to overthrow communism, before he gets a bullet in the head from the Commissar (the killing is offscreen).

Jerry wakes up to his freedoms and apologizes to Bill and Linda. Bill says that Jerry had a point about waiting to get married and he and Linda will do so after he finishes his enlistment in the United States Army.

==Production== The Twilight Department of Defense "Directorate of Armed Forces Information and Education" (commonly referred to as training films) under the direct supervision of Jack L. Warner.
 Soviet takeover of that region following World War II. 
 Jack Kelly, ABC network.

==Remarks== On the Beach (1959), Atomic War Bride (1960), and Panic in Year Zero (1962).
 Department of Defense as they did during World War II. Warner Bros. was no exception. Prior to the war they had made Confessions of a Nazi Spy (1940) that aroused great controversy. During the 1950s the studio made I Was a Communist for the FBI one of the film genre of anti-communist films Hollywood studios made from the late 1940s to the early 1950s.

*Though originally meant for United States Government use, the film was shown on American television in 1962.  The film was also shown as an educational film in American high schools.

==Cast== Jack Kelly	 ... 	Jerry Donavan 
	Jeanne Cooper	... 	Helen Donavan  Peter Brown	... 	Bill Martin 
	Pat Woodell	... 	Linda Donavan (as Patricia Woodell) 
	Andrew Duggan	... 	Judge 
	Peter Breck	... 	Russian Officer 
	Robert Conrad	... 	Pete 
	Mike Road	... 	Prosecutor 
	Jack Webb	... 	On-Camera Narrator

==Video releases==

Rhino Video released this film on video in 1985 under the title The Commies are Coming, the Commies are Coming.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 