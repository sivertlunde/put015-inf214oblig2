Max et les ferrailleurs
{{Infobox film
| name           = Max et les ferrailleurs
| image          = 
| image_size     = 
| caption        = 
| director       = Claude Sautet
| producer       = 
| writer         = Claude Sautet Claude Néron
| narrator       = 
| starring       = Michel Piccoli Romy Schneider Georges Wilson
| music          = Philippe Sarde
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1971
| runtime        = 112 min
| country        = Italy, France French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1971 cinema Italian film directed by Claude Sautet. The film is based on the novel of the same name by Claude Néron.

== Plot ==
Born into a wealthy family of French vintners, Max ( ), a young German-born prostitute who is the companion of Abel. He pretends to be the director of a small bank branch which receives significant amounts of money at regular intervals. He ensures the support of his police commissioner. Max fails however to reveal his role as instigator. Gradually, some feeling arises between Max and Lily. But Max keeps a reserved attitude and merely influences the scrap through her. Finally, guessing the band ready for action, he communicates an ideal date to commit robbery. On the scheduled day, the police await them and they are arrested. Later in the police station, Rosinsky (the top cop in the banks district) reveals to Max that he wants all collaborators brought to justice, including Lily. Distraught, Max tries to save her and ends up threatening Rosinsky. In an argument, Max pulls out his gun and kills him.

== Cast ==
 ]]
* Michel Piccoli : Max
* Romy Schneider : Lily
* Georges Wilson : le commissaire
* Bernard Fresson : Abel Maresco
* François Périer : Rosinsky
* Boby Lapointe : Ptit Lu
* Michel Creton : Robert Saidani
* Henri-Jacques Huet : Dromadaire
* Jacques Canselier : Jean-Jean
* Alain Grellier : Guy Laronget
* Maurice Auzel : Tony
* Philippe Léotard : Losfeld
* Robert Favart : Loiselle
* Dominique Zardi : Baraduch
* Albert Augier : un client de Lily
* Betty Beckers : Maria

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 

 