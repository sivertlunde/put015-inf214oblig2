Dimanche (film)
{{Infobox film
| name = Dimanche
| image = Dimanche (2011 film) poster.jpg
| alt =  
| caption =
| director = Patrick Doyon
| producer = Marc Bertrand Michael Fukushima 
| writer = Patrick Doyon
| starring = Chantal Baril Natalie Hamel Roy Jacques Lavallée François Sasseville Nicolas Scott
| music = Luigi Allemano
| cinematography =
| editing = Jelena Popovic
| studio = National Film Board of Canada
| distributor =
| released = February 2011 January 5, 2012 (online)
| runtime = 10 minutes
| country = Canada
| language =
| budget =
| gross =
}} animated short film by Patrick Doyon. The film debuted at the Berlin International Film Festival in February 2011  and online on January 5, 2012. 

Dimanche is the first professional film by Doyon, a native of Montreal.    Doyon had previously created a three-minute animated short Square Roots in 2006, while enrolled in the NFBs Hothouse program for young animators.   
 scanned into the computer, colourized and began editing. Doyon believes such traditional animation techniques are better for portraying emotion.    

==Awards== Best Animated Best Animated 34th Denver Film Festival. It also received Quebec’s Prix Jutra for best animated short.   

== Plot == elongating coins by placing them on train tracks. Doyon has said that the film is inspired by his youth in Desbiens, Quebec. 

== References ==
 

== External links ==
* Watch   on the NFB website 
*  
*  
* 

 
 
 
 
 
 
 
 


 
 