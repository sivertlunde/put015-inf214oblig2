Pickpocket (film)
 
{{Infobox film
| name     = Pickpocket
| image          = Pickpocketposter.jpg
| caption        = French theatrical poster
| director       = Robert Bresson
| writer         = Robert Bresson
| starring       = Martin LaSalle
| producer       = Agnès Delahaie
| cinematography = Léonce-Henri Burel
| editing            = Raymond Lamy
| distributor       =
| released          =  
| runtime        = 75 minutes
| country          = France
| language          = French
| budget         =
}}

Pickpocket is a 1959 French film directed by Robert Bresson. It stars the young Uruguayan Martin LaSalle, who was a nonprofessional actor at the time, in the title role, with Marika Green as the ingenue (stock character)|ingénue. It was the first film for which Bresson wrote an original screenplay rather than "adapting it from an existing text." 

==Plot==
Michel (Martin LaSalle) goes to a horse race and steals some money from a spectator. He leaves the racetrack confident that he was not caught when he is suddenly arrested. The inspector (Jean Pélégri) releases Michel because the evidence is not strong enough.  Michel soon falls in with a small group of professional pickpockets who teach him their trade and invite him to join them on highly coordinated pickpocketing sprees in crowded public areas.

Visiting his mother, Michel meets Jeanne (Marika Green) who begs him to visit his mother more often. His friend Jacques goes on a date with Jeanne and invites Michel along. But after stealing a watch, Michel leaves Jacques and Jeanne at the carnival. While in a bar, the inspector asks Michel to show him a book by George Barrington about pickpocketing at the station on a convenient morning, and Michel goes down to the police station at the appointed date with it. Once there, the inspector barely glances at the book. Michel returns to his apartment realizing that it was all just a ruse to search his apartment. However, the cops failed to find his stash of money.

Michels mother dies, and he goes to the funeral with Jeanne. Later, the inspector visits Michel in his apartment, and tells him that his mother had had some money stolen, but later dropped the charges, probably knowing the thief was her son. The inspector leaves without arresting Michel, who decides to leave the country.  He travels from Milan to Rome and ends up in England where he "spent two years in London pulling off good jobs", but throws his earnings away on alcohol and women.

Returning to France, Michel returns to Jeanne, who has had a child by Jacques but did not want to marry him and is now left with nothing.  Michel begins to work again to support her, but gives into temptation and goes back to steal at the horse track, where he is caught by a plainclothes policeman. Jeanne regularly visits him in jail.  On one such visit, Michel realizes he is in love with her.

==Cast==
* Martin LaSalle as Michel
* Marika Green as Jeanne
* Jean Pélégri as Chief Inspector
* Dolly Scal as The Mother
* Pierre Leymarie as Jacques
* Kassagi as 1st Accomplice
* Pierre Étaix as 2nd Accomplice
* César Gattegno as An Inspector

==Scholarly and critical reception== syuzhet   or is seemingly equal in importance to it". 

Roger Ebert sees echoes of Fyodor Dostoyevsky|Dostoyevskys Crime and Punishment in this film. "Bressons Michel, like Dostoyevskys hero Raskolnikov, needs money in order to realize his dreams, and sees no reason why some lackluster ordinary person should not be forced to supply it. The reasoning is immoral, but the characters claim special privileges above and beyond common morality. Michel, like the hero of Crime and Punishment, has a good woman in his life, who trusts he will be able to redeem himself.... She comes to Michel with the news that his mother is dying. Michel does not want to see his mother, but gives Jeanne money for her. Why does he avoid her? Bresson never supplies motives. We can only guess." 

As in Diary of a Country Priest, some screen time is devoted to the protagonists writings, and, as in A Man Escaped, the protagonists voice is heard more in the voice over than in dialogue.

==Soundtrack==
The film uses two pieces of orchestral baroque music, which the credits attribute to Jean-Baptiste Lully. However, both fragments are taken from orchestral suite no. 7 by Johann Caspar Ferdinand Fischer.

==Influence== Patty Hearst, and Light Sleeper all include endings similar to that of Pickpocket.  title = Film-makers on film: Paul Schrader | publisher = Telegraph | date=2003-01-25 | url = http://www.telegraph.co.uk/arts/main.jhtml?xml=/arts/2003/01/25/bffmof25.xml | accessdate = 2007-08-18
| location=London}}  In addition, his screenplay for Martin Scorseses Taxi Driver bears many similarities, including confessional narration and a voyeuristic look at society. Schraders admiration for Pickpocket led to his contribution in an extra in The Criterion Collections DVD release in 2005.

Pickpocket has been paraphrased by other films, such as Leos Caraxs Les Amants du Pont-Neuf. 

Pickpocket is loosely based on the novel Crime and Punishment by Fyodor Dostoevsky. 

In a list of  , Susan Sontag placed Pickpocket at number one.

==Awards==
;Nominated
*     

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 