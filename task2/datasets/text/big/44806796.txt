Bring Me the Head of the Machine Gun Woman
{{Infobox film
| name           = Bring Me the Head of the Machine Gun Woman
| image          = File:Bring Me the Head of the Machine Gun Woman poster.jpg
| alt            =
| caption        = 
| film name      = Tráiganme la cabeza de la mujer metralleta
| director       = Ernesto Díaz Espinoza
| producer       = 
| writer         = Ernesto Díaz Espinoza Fernanda Urrejola
| screenplay     = 
| story          = 
| based on       =  
| starring       = Fernanda Urrejola Matías Oviedo Jorge Alis
| narrator       = 
| music          = Rocco
| cinematography = Nicolás Ibieta
| editing        = Ernesto Díaz Espinoza Nicolás Ibieta
| production companies = LatinXploitation Ronnoc Entertainment
| distributor    = Ronnoc Entertainment 
| released       =  
| runtime        = 73 minutes	
| country        = Chile
| language       = Spanish
| budget         = 
| gross          =  
}}
Bring Me the Head of the Machine Gun Woman, also known under its original release title of Tráiganme la cabeza de la mujer metralleta, is a 2012 Chilean action comedy that was directed by Ernesto Díaz Espinoza. The film had its world premiere on 23 September 2012 at the Austin Fantastic Fest and was released in Chile on 23 May 2013. 

==Synopsis== hit on a bounty hunter known only as "Machine Gun Woman" (Fernanda Urrejola). Upon being discovered Santiago does the only thing he can: he offers to kill Machine Gun Woman and bring back her head to prove that she is truly dead. He decides that the best way to approach the situation is to treat the potential murder as he would a violent video game. 

He does manage to find Machine Gun Woman, but Santiago is unaware that Ches underlings have followed him with the intention to kill the both of them. Machine Gun Woman kills both underlings but spares Santiago out of pity. Santiago tries to return home and flee the city with his mother (Francisca Castillo), but is caught by Che, who threatens to murder them both if Santiago does not produce Machine Gun Woman. He manages to successfully find her, but Che again trails them and succeeds in shooting one of them. The two flee together and find that they are both attracted to one another, culminating their encounter by having sex in a jeep. They decide that they will take Che out once and for all, but Santiago forgets to inform Machine Gun Woman of his mothers capture and as a result the operation goes awry. Despite this, Santiago manages to confront Che and with Machine Gun Woman, the two end kill Che. Free from Ches reign of terror, Machine Gun Woman and Santiago share a kiss before she runs off, leaving Santiago alone. Unwilling to accept this, Santiago tries to chase after her in a stolen car but is stopped by the police before he can catch up with her.

==Cast==
*Fernanda Urrejola as La Mujer Metralleta (Machine Gun Woman)
*Matías Oviedo as Santiago Fernández
*Jorge Alis as Che Longana
*Sofía García as Shadeline Soto
*Alex Rivera as Flavio
*Felipe Avello as Jonny Medina
*Pato Pimienta as Pato El Conserje
*Francisca Castillo as Santiagos Mom
*Miguel Angel De Luca as Parguineo
*Daniel Antivilo as El Tronador
*Jaime Omeñaca as Bracoli

==Reception==
Critical reception for Bring Me the Head of the Machine Gun Woman has been mostly positive,    and Film School Rejects and Complex magazine|Complex both considered the film to be one of the best films at the Austin Fantastic Fest.   The Daily Record and The List both gave the movie three stars,  and the Daily Record noted that although the film had some flaws with its production it was ultimately "a fun ride that never outstays its welcome."  Bloody Disgusting gave the film an overly favorable review, specifically praising actress Fernanda Urrejolas performance. 

==References==
 

==External links==
*  

 
 
 