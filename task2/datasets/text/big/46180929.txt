Rings (2015 film)
{{Infobox film
| name           = Rings
| image          = 
| alt            = 
| caption        = 
| director       = F. Javier Gutiérrez
| producers      = Laurie MacDonald   Walter Parkes
| writers        = David Loucka   Jacob Aaron Estes   Akiva Goldsman
| based on       =  
| starring       = Matilda Lutz   Alex Roe   Johnny Galecki
| music          = 
| cinematography = Sharone Meir
| editing        =  production companies Vertigo Entertainment
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $33 million 
| gross          =  
}}
 horror thriller The Ring and The Ring Two. The film is produced by Laurie MacDonald and executive produced by Guillermo del Toro. The film stars Matilda Lutz, Alex Roe and Johnny Galecki. Principal photography began on March 23, 2015 in Atlanta. It is set for a November 13, 2015 release.

== Cast ==
* Matilda Lutz as Julia
* Alex Roe as Holt
* David Dorfman as Aiden Keller
* Johnny Galecki as Gabriel
* Aimee Teegarden

== Production ==
Paramount Pictures announced a third film The Ring 3D with F. Javier Gutiérrez directing.  In August 2014, Paramount was in talks with Akiva Goldsman to write a third draft of the screenplay, previously worked on by David Loucka and Jacob Aaron Estes.  In November, Gutiérrez posted an Instagram photo that showed that the current title of the sequel is Rings.  On January 16, 2015, Matilda Lutz was added to the cast for the lead role.    Alex Roe was cast for the male lead role on March 20, 2015.    Aimee Teegarden joined the cast on March 27, 2015.    Johnny Galecki had signed on to star in the film on April 1, 2015, he would be playing Gabriel, a  professor who mentors and helps both Holt and Julia.   

=== Filming ===
Principal photography on the film began on March 23, 2015 in Atlanta.  

== Release ==
Paramount Pictures set the film for a November 13, 2015 release. 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 