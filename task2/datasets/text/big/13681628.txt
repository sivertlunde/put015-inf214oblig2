Riverburn
{{Infobox film
| name           = Riverburn
| image          =
| caption        =
| director       = Jennifer Calvert
| producer       = Diane Carruthers Wood
| writer         = Thea Boyanowsky
| starring       = Magda Apanowicz
| music          = Julie Blue 
| cinematography =
| editing        = 
| studio         =
| distributor    =
| released       =
| runtime        =
| country        = Canada
| language       = English
| budget         =
| gross          =
}}
Riverburn is a 2004 short film directed by Jennifer Calvert, that won her Best Young Canadian Director of a Short Film at the 2004 Vancouver International Film Festival.

The 20 minute film—inspired by Andrei Tarkovsky and Terrence Malick—is about a city girl (Magda Apanowicz) who while briefly left alone on a camping trip, meets a city boy, sparking a mutual interest.

Jason Whyte included the short in his Top 5 list for the Vancouver festival. 

The short was shot in British Columbia in the summer of 2003. 

==References==
 

 
 
 
 
 
 