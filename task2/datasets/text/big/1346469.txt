El Dorado (1966 film)
 
{{Infobox film
| name           = El Dorado
| image          = El Dorado (John Wayne movie poster).jpg
| caption        = Theatrical release poster
| director       = Howard Hawks
| producer       = Howard Hawks Harry Brown
| based on       =  
| screenplay     = Leigh Brackett
| starring       = {{Plainlist|
* John Wayne
* Robert Mitchum James Caan
* Charlene Holt
* Paul Fix
}}
| music          = Nelson Riddle
| cinematography = Harold Rosson
| editing        = John Woodcock
| studio         = Laurel Productions
| distributor    = Paramount Pictures
| released       =                       
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = $4,653,000 
| gross          = $5,950,000 (US/ Canada)   
}}
 Western film Harry Brown, James Caan as the young gambler, Charlene Holt, Paul Fix, Arthur Hunnicutt, Michele Carey, and Christopher George. 

El Dorado was filmed on location in Tucson, Arizona and Kanab, Utah, and was shot in Technicolor. The paintings in the credits are by Olaf Wieghorst, who plays Swede Larsen in the film. The musical score was composed by Nelson Riddle. Paramount Pictures delayed the release of the film in the United States to avoid competing against another Paramount film, Nevada Smith with Steve McQueen. The film was first released in Japan on December 17, 1966, and was finally released in the United States on June 7, 1967. The film received critical praise and was successful at the box office, generating North American rentals of $5,950,000 on box-office receipts of $12 million. McCarthy, p. 625. 
 Rio Bravo (1959) and before Rio Lobo (1970), both also starring Wayne. The plotlines of all three films are almost similar enough to qualify El Dorado and Rio Lobo as remakes.

Members of the Western Writers of America chose the films theme as one of the Top 100 Western songs of all time.    

==Plot== El Dorado.  The local sheriff, an old friend of Thornton, J.&nbsp;P. Harrah (Robert Mitchum) gives Cole more details that Jason had deliberately left out, including the possibility of having to side against Harrah. Unwilling to fight his friend, Thornton quits, to the relief of saloon owner Maudie (Charlene Holt), who is in love with Thornton (and was for a time a romantic interest of Harrahs).

The McDonalds learn of Thorntons presence in town. Fearing that he might come for them, Kevin McDonald (R.&nbsp;G. Armstrong) puts his youngest son, Luke, on guard. When Thornton passes by on his way back from rejecting Jasons offer, Luke (Johnny Crawford), who has fallen asleep, wakes and fires a wild warning shot whereupon Thornton reflexively shoots him. Luke is still alive when Thornton finds him, but he refuses treatment based upon the belief that a gut-shot man wouldnt have a chance anyway and commits suicide when Thornton turns his back on him.

Thornton subsequently brings the boys body to the McDonald ranch and offers an explanation. The only McDonald daughter, Joey (Michele Carey), impulsively rides off before Thornton can finish his story and ambushes him shortly thereafter. Her shot is not fatal, but the bullet lodges next to Thorntons spine and in time begins to trouble him by occasionally pressing against the spinal cord, causing temporary paralysis of his right side. The local doctor, Dr. Miller (Paul Fix), does not have the skill to remove the bullet and Thornton soon departs El Dorado for a new job.

Several months later, Thornton runs into another gunslinger-for-hire named Nelson McLeod (Christopher George) and a young greenhorn called Mississippi (James Caan), who has come for revenge against one of McLeods men. McLeod has been hired by Jason for the same job Thornton turned down and Thornton hears from McLeod about how Harrah has turned into a drunk after an unhappy love affair. Thornton decides to return to El Dorado, hoping to save Harrah from being gunned down by McLeod and his men. He is followed by Mississippi who also wishes to help, despite his lack of experience and terrible aim with a gun.

Once Thornton and Mississippi arrive in El Dorado, they hear more of the story behind Harrahs change. The two men then join with Deputy Sheriff Bull (Arthur Hunnicutt) in order to get Harrah sober and cleaned up. Mississippi contributes an old folk recipe for a hangover that he learned from his old friend and mentor, Johnny Diamond.  The recipe includes such ingredients as Cayenne pepper|cayenne, mustard, Syrup of ipecac|ipecac, asafoetida, croton oil and gunpowder, and he promises it will make any man unable to drink liquor for a while. The concoction proves to be violently effective, and leaves Harrah sober and furious. Within a day of their arrival, McLeod and his men also come to El Dorado and are hired on by Bart Jason. When one of them shoots one of the McDonalds, Thornton, Harrah, Bull and Mississippi chase the shooter and his friends into an old church and then into Jasons saloon. Harrah arrests Jason and takes him to the jail for his part in the shooting of one of the McDonalds. Later that night, Thornton and Mississippi decide to patrol the town in the hope of keeping the peace and are deputized by Bull. There is another shootout with McLeod and his men, which results in a minor leg injury for Harrah.

The next day, Maudie sends a message to Thornton and his friends, stating that McLeods men are frightening her and her patrons. When Thornton and Mississippi go to help her, they are ambushed and Thornton has an attack that leaves him partially paralyzed and captured by McLeod. Subsequently, McLeod trades the injured Thornton for Bart Jason, a trade to which Harrah agrees, despite knowing that doing so will mean that nothing will stand in the way of McLeod going after the McDonalds.

Sure enough, McLeod and his men shortly thereafter kidnap one of the McDonalds in order to force Kevin McDonald to sign over his water rights to Jason. Thornton and the others are forced to quickly come up with a plan to rescue Kevin McDonalds son and neutralize Jason and McLeod. Despite Thorntons paralysis and Harrahs leg injury, the two of them, along with Bull and Mississippi, return to town on wagons. While Thornton distracts Jason and McLeod outside of the front of the saloon, Mississippi, Harrah and Bull attack from the rear. The kidnapped McDonald is rescued, with a little help from Joey McDonald, Jason, McLeod and his men are killed, and order is restored to El Dorado. Thornton also begins to imply that he may discontinue his wandering ways in order to stay in the town with Maudie.

==Cast==
*John Wayne as Cole Thornton
*Robert Mitchum as Sheriff J.P. Harrah James Caan as Alan Bourdillion Traherne called Mississippi
*Arthur Hunnicutt as Bull Harris
*Charlene Holt as Maudie
*Michele Carey as Josephine "Joey" MacDonald
*Ed Asner as Bart Jason
*Christopher George as Nelson McLeod
*R. G. Armstrong as Kevin MacDonald
*Paul Fix as Dr. Miller
*Robert Donner as Milt (McLeod gang) Jim Davis as Jim Purvis (Bart Jasons foreman)
*Adam Roarke as Matt MacDonald
*Johnny Crawford as Luke MacDonald
*John Mitchum as Elmer (Jasons bartender)
*Chuck Roberson as Jasons gunman
*Don Collier as Deputy Joe Braddock
*Olaf Wieghorst as Swede Larsen

==Production== Rio Bravo over again the sicker I got, because I hate doing things over again. And I kept saying to Howard I did that, and hed say it was okay, we could do it over again." 

Shot at Old Tucson Studios just west of Tucson, Arizona, as well as in and around Kanab, Utah, from October 1965 to January 1966, the film was Waynes 138th picture and was actually filmed before The War Wagon, but its release was delayed so that Paramounts Nevada Smith with Steve McQueen would not have to compete with a John Wayne film at the box office. El Dorado finally reached the theatres in June 1967, a month after The War Wagon had opened. Davis, 1998, pp. 274–275. 

Film footage from El Dorado was later incorporated into the opening montage of Waynes final film, The Shootist, to illustrate the backstory of Waynes character. Director Don Siegel used no scenes from any film shot before 1948 in The Shootist s frontispiece even though Waynes career as a leading man began in 1930 with The Big Trail and had continued throughout the following two decades.

In the film, Mississippi gives Sheriff Harrah a concoction meant to deter him from further drinking. The ingredients include (among others) Cayenne pepper, ipecac (an emetic), Mustard (condiment)|mustard, croton oil (which induces diarrhea), asafoetida (often misspelled acifedida) and gunpowder.

==Poem==
The poem repeated in the film is "Eldorado (poem)|Eldorado", a ballad poem by Edgar Allan Poe. Caan continuously mispronounces the verse "Ride, boldly ride" as "Ride, Bodie, ride," confusing audience members unfamiliar with Poes poem.

==Reception==
===Critical response===
The film maintains a high critical praise, earning a 100 percent perfect rating on review compiler Rotten Tomatoes.  Popular critic Roger Ebert gave the film a near-perfect rating at 3 1/2 out of four stars,  stating "El Dorado is a tightly directed, humorous, altogether successful Western, turned out almost effortlessly, it would seem, by three old pros: John Wayne, Robert Mitchum and director Howard Hawks". Praised film poll, They Shoot Pictures, Dont They? ranked the film 799 out of 1,000 for the list of greatest films of all time. 

===Box office===
The film was successful at the box office, generating North American rentals of $5,950,000 on box-office receipts of $12 million. 

==In popular culture==
The similarity between Rio Bravo and El Dorado gave rise to an amusing exchange in the 1995 film Get Shorty. In this scene, Los Angeles drug dealer Bo Catlett (Delroy Lindo) breaks into the home of B film and horror film actress Karen Flores (Rene Russo) in order to steal a valuable film script. He accidentally touches the TV remote and switches on a cable channel, which is showing Rio Bravo. This awakens Flores and her boyfriend, mafia enforcer Chili Palmer (John Travolta). The pair confront Catlett and, in an attempt to talk his way out of the situation, Catlett confuses details about the respective casts of Rio Bravo and El Dorado, incorrectly stating that Dean Martin had acted in the latter. Palmer, a film buff and would-be film producer, is appalled at this lack of knowledge. He proceeds to give Catlett a tongue-in-cheek lecture setting the facts straight, pointing out that El Dorado was essentially a remake of Rio Bravo, with Robert Mitchum playing the role of Dean Martins drunk, James Caan doing Ricky Nelsons role, and that the only person playing the same role in both films was John Wayne: "he played John Wayne".

In the 2007 film  , when an elderly theater owner cited it as his favorite film.

==See also==
* John Wayne filmography

==References==
Notes
 

Citations
 

Bibliography
 
*  
*  
*  
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 