The Isle of Lost Ships (1923 film)
{{Infobox film
| name           = The Isle of Lost Ships
| image          = The Isle of Lost Ships.jpg
| caption        = 1923 theatrical poster
| director       = Maurice Tourneur
| producer       = Maurice Tourneur Productions Ned Marin
| writer         = Charles Maigne (scenario)
| based on       =  
| starring       =
| cinematography = Arthur L. Todd
| editing        = Frank Lawrence Associated First National
| released       =  
| runtime        = 8 reels (7,425 ft)
| country        = United States Silent (English intertitles)
}}

The Isle of Lost Ships is a 1923 American silent adventure/melodrama film directed and produced by   in 1929 by director Irvin Willat.

Tourneur himself made a different story with similar theme called The Ship of Lost Men (1929) which had a young German actress, Marlene Dietrich, in the cast. 

The 1923 film has long since been thought to be lost film|lost.

==Cast==
*Anna Q. Nilsson - Dorothy Fairfax
*Milton Sills - Frank Howard
*Frank Campeau - Detective Jackson Walter Long - Peter Forbes
*Bert Woodruff - Patrick Joyce
*Aggie Herring - Mother Joyce
*Herschel Mayall - Captain Clark

==Story==
People and ships trapped in seaweed infested section of the southern Atlantic Ocean known as the Sargasso Sea.

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 