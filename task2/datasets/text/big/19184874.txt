Phra Rod Meree
 
{{Infobox film
| name = Phra Rod Meree
| image = Phra rod meree.jpg
| caption = Thai theatrical poster
| imdb_rating =
| director =Sompote Sands
| producer =
| writer =
| starring =Dam Datsakorn Toon Hiransap  Supansa Nuengpirom  Ampha Pusit  Duangcheewan Komolsen
| music =
| cinematography =		
| editing =
| distributor =
| released =1981
| runtime = 120 min
| country = Thailand Thai
| budget =
| preceded_by =
| followed_by =
| mpaa_rating =
| tv_rating =
}} 1981 Cinema Thai fantasy film. Directed by Sompote Sands,  it is one of the most successful Thai feature films ever made in Thailand. 

The story is based on The Twelve Sisters, a Thai folklore myth that originated in one of the Jataka tales.  This same story was also adapted into a Thai fantasy Thai television soap opera|lakhon. 

==Plot==
Twelve girls are abandoned by their parents because they are too poor to take care of them. The twelve daughters are rescued by an ogress (yaksha) in disguise who promises to take care about them as her own daughters.

Phra Rodasan (Phra Rod), the only surviving son of the twelve sisters, goes on a quest to the ogre kingdom in order to heal his mother and his aunts blindness. There he falls in love with the ogress daughter, Meree. 

==See also==
*The Twelve Sisters
*Nang Sib Song (lakorn)
*Ka Kee

==References==
 

==External links==
* 

 
 
 
 
 


 