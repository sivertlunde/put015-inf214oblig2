Jackson (2008 film)
{{Infobox film
| name           = Jackson
| image          = Jackson (2008 film).jpg
| caption        =
| director       = J. F. Lawton
| producer       = Ryan Pilon
| writer         = J. F. Lawton Charles Robinson Steve Guttenberg Debra Jo Rupp
| music          = Frankie Blue
| cinematography = Jack Conroy
| editing        = Roderick Davis
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
}}
 Charles Robinson. 

==Synopsis==
Jackson is about two homeless men in Los Angeles, Donald and Sam. At the start of the day Donald is given a $20 bill (also known as a "United States twenty-dollar bill|Jackson"). The film follows the two and shows their adventures throughout the day with this money, and features songs from various operas.

==Cast==
* Barry Primus as Donald Charles Robinson as Sam
* Steve Guttenberg as Businessman
* Debra Jo Rupp as Nice Lady

==Musical numbers==
# "Una furtiva lagrima from Donizettis Elixer of Love – Richard Brown and Shawnette Sulker
# "Champagne Aria" from Mozarts Don Giovanni – Cedric Trenton Berry
# "Habanera (aria)|Habanera" from Bizets Carmen – Elaa Lee Romani/ Chorus: Ariella Vaccarino, Aleta Braxton, Pilar Diaz, Tahlia McCollum
# Sextet from Donizettis Lucia di Lammermoor – Jennifer Suess, William Gorton, Michael Sokol, Fred Winthrop, Benjamin Von Atrops, Leberta Clark; Music performed by Remy Zero
# Monologue by Ibn Hakia from Tchaikovskys Iolanta – John R. Jackson
# "O mio babbino caro" from Puccinis Gianni Schicchi – Gustavo Hernandez Jr.
# " , Kimarie Torre, Lauren Lee Chorus: Laura Decher, Frances Garcia, Erin Neff, Sara MacBride, Gregory Stapp, Antoine Garth, Gary Murphy, Tom Oberjat, Raphaela Rose Primus
# "Love Cannot Be" (written by J.F. Lawton)
# "El Pueblo" – George Lawton and John Cross
# "Vamos a la Fiesta" – Julie Griffin
# "Abrabo" – Way Depp, Robidebs Okyeame, Paa Dogo and Brekete I Wish I Was in Dixie Land" – John B.J. Smith
# "Downtown Birthday" (written by J.F. Lawton)

==Production details==
Jackson was filmed mainly in Los Angeles with the exception of one scene shot in Kentucky.

==Awards==
*2009 Swansea Bay Film Festival
**Best in Festival
*2009 The Conscious Life Film Festival
**Indigo Award
*2009 Treasure Coast International Film Festival
**Best Feature Film
**Best Editing
* 2008 International Film Festival of South Africa
** Best Feature Film
* 2008 Non Violent Film Festival
** Best Feature Film
* 2008 Lakedance Film Festival
** Audience Award for Best Feature Film
* 2008 Socal Film Festival
** Best Feature Film
** Best Director
** Best Actor: Barry Primus

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 