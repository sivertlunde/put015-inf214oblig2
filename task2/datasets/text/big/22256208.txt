I Don't Want to Talk About It (film)
 
{{Infobox film
| name           = I Dont Want to Talk About It
| image          = De Eso no Se Habla film poster.jpg
| caption        = Theatrical release poster
| director       = María Luisa Bemberg
| producer       = Oscar Kramer
| writer         = María Luisa Bemberg Jorge Goldenberg Julio Llinás Aldo Romero
| starring       = Marcello Mastroianni
| music          =
| cinematography = Félix Monti
| editing        = Juan Carlos Macías
| distributor    =
| released       =  
| runtime        = 106 minutes
| country        = Argentina Italy
| language       = Spanish
| budget         =
}}

I Dont Want to Talk About It ( ,  ) is a 1993 Argentine-Italian drama film directed by María Luisa Bemberg, starring Luisina Brando and Marcello Mastroianni. The script was based on a short story by Julio Llinas.   

==Cast==
* Marcello Mastroianni - Ludovico DAndrea
* Luisina Brando - Leonor
* Alejandra Podesta - Charlotte
* Betiana Blum - Madama
* Roberto Carnaghi - Padre Aurelio
* Alberto Segado - Dr. Blanes
* Mónica Lacoste - Sra Blanes
* Jorge Luz - Alcalde
* Mónica Villa - Sra Zamildio
* Juan Manuel Tenuta - Police Chief
* Tina Serrano - Widow Schmidt
* Verónica Llinás - Myrna
* Susana Cortínez - Sra Peralta
* Martin Kalwill - Mayors Clerk
* Walter Marín - Mojame

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 