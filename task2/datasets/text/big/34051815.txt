Romance of Celluloid
{{Infobox film
| name           = The Romance of Celluloid
| image          =
| caption        =
| director       =
| producer       =
| writer         =
| narrator       = Frank Whitbeck Adrian Jack Robert Montgomery Maureen OSullivan Jessie Ralph Rosalind Russell Herbert Stothart
| music          =
| cinematography =
| editing        =
| distributor    = MGM
| released       =  
| runtime        = 10 minutes
| country        = United States
| language       =
| budget         =
}}
 1937 short short black documentary film, narrated by Frank Whitbeck, which goes behind the scenes to look at the manufacture of film and the making of motion pictures.  The film was the first of the studios Romance of Celluloid series, which also included;   

* Another Romance of Celluloid (1938) 
* From the Ends of the Earth: Another Romance of Celluloid (1939) 
* Electric Power: Another Romance of Celluloid (1939) 
*   (1940)
* A New Romance of Celluloid: Hollywood; Style Center of the World (1940)
* A New Romance of Celluloid: You Cant Fool a Camera (1941) 
* A New Romance of Celluloid: Personalities (1942) 
* A New Romance of Celluloid: We Must Have Music (1942) 
* Twenty Years After: A New Romance of Celluloid (1944) 

==Production==
The film was shot on location at the Metro-Goldwyn-Mayer Studios in Culver City, California and the Kodak plant in Rochester, New York

==Synopsis== Adrian sketching The Firefly Robert Montgomery, Cliff Edwards, Rosalind Russell, Gladys George, Jessie Ralph, Maureen OSullivan and studio trainer Don Loomis. The film concludes with a montage from trailers for coming MGM pictures featuring the studios parade of stars.

==References==
 

 
 
 
 
 
 
 
 
 