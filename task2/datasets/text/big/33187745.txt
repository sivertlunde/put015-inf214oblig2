Silent Souls
{{Infobox film
| name           = Silent Souls
| image          = SilentSouls2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Aleksey Fedorchenko
| producer       = Igor Mishin Maria Nazari
| writer         = Denis Osokin
| starring       = Yuri Tsurilo Igor Sergeyev Yuliya Aug Viktor Sukhorukov
| music          = Andrey Karasyov
| cinematography = Mikhail Krichman
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 75 minutes
| country        = Russia
| language       = Russian
| budget         = 
| gross          = 
}}
Silent Souls (Russian: Овсянки, "The Bunting (bird)|Buntings") is a 2010 Russian film that was nominated for the Golden Lion at the 67th Venice Film Festival. It is based on a 2008 novella by Denis Osokin. The film was awarded the Golden Osella for best cinematography and a FIPRESCI award.  It was considered a frontrunner for the Golden Lion,  but did not win.

== Plot == Gorbatov (the Bunting birds. On their way back to Neya, they get lost and are approached by two prostitutes, with whom they have sex. Later on, while crossing "the great Meryan river" (The Volga), on the Kineshma Bridge, the Buntings fly around the car, causing it to crash into the river. Both men drown.

== Reception ==
Silent Souls received considerable praise from film critics.  The official Rossiyskaya Gazeta compared the film to Andrei Tarkovsky|Tarkovskys best work as a powerful evocation of pre-Christian roots of rural Russia.  Andrei Plakhov praised the film as "a metaphor for the lost (and probably mythical) world that was crushed by the moloch of industrialisation". 

Among American critics,   commented: "Populated by memories and dappled with desire, “Silent Souls” is part folk tale, part lesson in letting go. In its quiet acceptance of the passing of time, this unusual film reminds us that to die is not always the same as to disappear".   Roger Ebert expressed the opinion that the film "in only 75 perfect minutes achieves the profundity of an epic", also mentioning that "not often have I been more deeply touched". 

== References ==
 

== External links ==
* 
* 

 
 
 
 