Vadaka Veedu
{{Infobox film 
| name           = Vadaka Veedu
| image          =
| caption        = Mohan
| producer       = Mubarak Vijayan Dr Pavithran (dialogues)
| screenplay     = Dr Pavithran
| starring       = Sukumari Anupama Sukumaran Ittoop
| music          = M. S. Viswanathan
| cinematography = U Rajagopal
| editing        = G Venkittaraman
| studio         = Sulfikar Movies
| distributor    = Sulfikar Movies
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Mohan and produced by Mubarak. The film stars Sukumari, Anupama, Sukumaran and Ittoop in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Sukumari
*Anupama
*Sukumaran
*Ittoop
*Master Sujith
*P. K. Abraham
*Ravi Menon
*Vidhubala

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aayiram sugandha || P Jayachandran || Bichu Thirumala || 
|-
| 2 || Maarivillinte panthal || Vani Jairam || Bichu Thirumala || 
|-
| 3 || Sugama sangeetham thulumbum || S Janaki || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 