Some Voices (film)
{{Infobox film
| name           = Some Voices
| image          = Some voices film poster.jpg
| caption        = Theatrical release poster
| director       = Simon Cellan Jones
| producer       = Damian Jones Graham Broadbent
| writer         = Joe Penhall
| starring       = Daniel Craig Kelly Macdonald David Morrissey
| music          = Adrian Johnston
| cinematography = David Odd
| editing        = Elen Pierce Lewis
| distributor    = Film Four Distributors
| released       = 25 August 2000 (United Kingdom)
| runtime        = 101 min.
| country        = United Kingdom
| awards         = English
| budget         =
| preceded_by    =
| followed_by    =
}} 2000 drama film directed by Simon Cellan Jones and adapted for the screen by Joe Penhall, from his own stage play (originally a theatre production for the Royal Court, London). It is the first feature film by Cellan Jones, a renowned TV director respected for his work on the British Academy Television Awards|BAFTA-winning Our Friends in the North. The film was almost entirely shot on location in Shepherds Bush, West London, where Cellan Jones lives. The film has a running time of 101 minutes.

==Plot==
The films central character, Ray (Daniel Craig), has schizophrenia. The story begins with Rays discharge from psychiatric hospital. Rays devoted brother Pete (David Morrissey) picks him up and drives Ray to his new abode, the spare room in Petes West London flat. Pete is a chef who works long hours in the café (a traditional "greasy spoon" during the day and a trendy eatery in the evening) that he inherited from his father. He now has to find the time to take care of Ray and monitor the medication that controls the voices in his head.
 abusive boyfriend (Peter McDonald). Laura becomes attracted to Ray because of his spontaneity and his childlike sense of fun. Around this time, Pete also becomes involved in a relationship with Mandy (Julie Graham).

As Rays relationship blossoms, he begins to resent taking his pills, preferring to trust in the soothing properties of love. Over time, this decision has disastrous effects on all three relationships: the relationship between the brothers, Ray and Laura, and Pete and Mandy. Ray may cause disruption, concern and distress to those close to him but that is only a fraction of the distress his condition causes him. In the end, it is the relationship between the brothers that is central to the film. Pete is long-suffering but, despite all his frustration and resentment, his loving commitment keeps his brother from serious harm.

==Cast==
*Daniel Craig – Ray
*Kelly Macdonald – Laura
*David Morrissey – Pete
*Julie Graham – Mandy Peter McDonald – Dave
*Nicholas Palliser – Friend
*Edward Tudor Pole - Lighter seller

==Soundtrack==
*"Speed of the Sound of Loneliness" – Alabama 3
*"Rake It In" – Imogen Heap
*"This Is the Tempo" - Grand Theft Auto
*"54-46 Was My Number" – Toots & the Maytals Squeeze
*"Il ragazzo della Via Gluck" (French Version "La Maison ou jai grandi)" - Françoise Hardy

==Awards and critical acclaim==
Some Voices premiered in Directors Fortnight at Cannes Film Festival|Cannes. It was nominated for the Golden Hitchcock Award at the annual Dinard Festival of British Cinema in France. (Billy Elliot won the award that year.) The first-time director was nominated for the Best Newcomer at the British Academy Awards.

Critics were divided about Some Voices. Channel 4 called it "one of the best British films of 2000", with the directors vision of west Londons "tower blocks, dual-carriageways and crowded streets" mirroring the central characters "gradual disintegration".

Mark Wyman of Film Review also recommended Some Voices as a film "definitely worth seeing" which "showcases some terrific British talent". 

Conversely, Empire (magazine)|Empire called the film, "claustrophobic and cornered" and claimed that it "probably brought the house down on stage, but on film, its simply static".

Total Film also had doubts. "Perhaps its the quirky, jerky This Life camerawork or the dim, grainy film stock, but Some Voices never reaches out and grabs the audience, remaining a watch rather than an experience". However, the director did "draw intelligent, effective turns from his cast – Daniel Craig and David Morrissey are excellent, while Kelly Macdonald delivers the kind of sweetly sexy performance shes rapidly trade-marking". Special mention was also made of one particular "brilliant effect". As Ray, Daniel Craigs central character, "stops taking his tablets, the strange staticky images start to dominate his vision, the odd sounds begin to blot out reality and, gradually, the gulf between the world he experiences and the one everyone else lives in widens disastrously. Its a clever and mammothly effective technique, communicating not just the strangeness of whats happening to Ray, but also the sheer terror of it".
 Time Out, on the other hand, described this "brilliant effect" as "over-egging it somewhat". The "whirling camera effects and freaky sound mix overstates the point that our man really is not well". Nevertheless, the Time Out reviewer is also complimentary about the acting. "Penhalls adaptation of his play remains an actors showcase. Morrissey skillfully registers abiding filial love tested by simmering exasperation; MacDonalds adept at lippy on top, vulnerable underneath; and Craigs vibrant yet haunted expressiveness tells us everything needful about this doomed sweetheart".

Peter Bradshaw in The Guardian and Peter Byrne in the Student BMJ (British Medical Journal) are much more unequivocal in their praise. Bradshaw calls the film "a serious, substantial, and compassionate movie which demands to be seen, not least for its outstanding performances from an excellent cast". Byrne says that Some Voices "works as a film, and a technically accomplished one at that". Both reviewers particularly commend the films avoidance of cliché. Bradshaw stresses that "nothing could be more tiresome and dishonest than shop worn RD Laing-style clichés about schizophrenia being a heightened visionary state which the western world crushes under the jackboot of its dull rationalist enlightenment. Such a proposition would not correspond to the actual experience of schizophrenia sufferers and their carers; in real life, schizophrenia can lead to a lifelong trial of stress and unhappiness, and Some Voices reflects this". He also welcomes the fact that "the schizophrenic is not demonised as a potential criminal or as a care-in-the-community basket-case", and that "Rays essential humanity is transcribed with sympathy and warmth, and so is the patience and perseverance of Pete, who must shoulder most of the burden of schizophrenias terrible mystery". Byrne writes that "the film is refreshing in its avoidance of the standard formulas. Gone are the psycho-killer, pathetic, or crazy funny guy stereotypes… There is no blaming, no mental illness as metaphor, no psychiatry bashing, and - although a romance lies at its core - there is none of the usual message that love is better than tablets. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 