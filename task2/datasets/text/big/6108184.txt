Siddhartha (1972 film)
{{Infobox Film
 | name = Siddhartha
 | image  =  Siddhartha 1972 poster.jpg
 | director = Conrad Rooks
 | writer = Conrad Rooks Paul Mayersberg
 | starring = Shashi Kapoor Simi Garewal Romesh Sharma
 | music = Hemant Kumar
 | cinematography =Sven Nykvist
 | editing = Willy Kemplen
 | distributor = Columbia Pictures (1973)
 | released = 1973
 | runtime = 89 min. English
 }} the novel of the same name by Hermann Hesse, directed by Conrad Rooks.  It was shot on location in Northern India, and features work by noted cinematographer Sven Nykvist.

The locations used for the film were the holy city of Rishikesh and the private estates and palaces of the Maharajah of Bharatpur, India|Bharatpur. 

==Plot==
The film tells the story of the young Siddhartha (played by Shashi Kapoor), born in a rich family, and his search for a meaningful way of life. This search takes him through periods of harsh asceticism, sensual pleasures, material wealth, then self-revulsion and eventually to the oneness and harmony with himself that he had been seeking. Siddhartha learns that the secret of life cannot be passed on from one person to another, but must be achieved through inner experience.

==Controversy==
Simi Garewals nude scene caused controversy in India. The Indian Censor Board, at that time, did not even permit on-screen kissing in Indian films. 

==Music==
All the Indian music was composed and sung by Hemant Kumar, with lyrics to the songs by Gouriprasanna: Mother’s song was by Shanti Hiranand.

Kumars Bengali songs, adapted for Siddhartha, "Pather Klanti Bhule" is from the 1956 movie Maru Tirtha Hinglaj and "O Nodire Ekti Kotha Shudhai" is from the 1959 movie Neel Akasher Neechey.

No soundtrack album for the film was issued.

==Premier and revival==
The film premiered in the West and the U.S. to positive reviews. Then during the 1970s and 1980s, it disappeared from distribution and runs in theaters. In 1996 it came back after much work and restoration, followed by release to TV and Video. Those who saw it applauded the film as a tale of self-discovery and the work to restore it. A region 1 DVD was released in 2002. 

==See also==
*List of historical drama films of Asia

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 