Ab Ke Baras
{{Infobox film |
 name        = Ab Ke Baras|
 image       = Ab Ke Baras Movie Poster.jpg|
 caption     = Movie Poster|
 director    = Raj Kanwar|
 writer      = Robin Bhatt, Sutanu Gupta|
 starring    = Arya Babbar,  Amrita Rao|
 released    = May 10, 2002|
 runtime     = 169 minutes|
 language    = Hindi|
 music       = Anu Malik|
 budget      = |
}}

Ab Ke Baras is a Bollywood film released in the year 2002. It had launched the career of the two newcomers, Arya Babbar and Amrita Rao. 

== Synopsis ==
Anjali (Amrita Rao), an Indian girl, born & brought up in London, starts having a feel that her past is coming back to her. She learns that her love from previous life is calling her and she must come to India. She comes to India in pursuit of her lover. Over to India, she is welcomed by her DCP Uncle (Danny) who wants her to return. She somehow manages to evade him and comes across Karan (Arya Babbar) who is a car thief. They somehow bump into each other a number of times and she tells him about her quest. Karan doesnt believe in rebirth but decides to help her. It so happens that Karan too happens to gain knowledge of his previous birth and learns that he and Anjali were lovers in previous life (Abhay Singh & Nandini). They both were freedom fighters. In freedom struggle against the Britishers, an Indian Police officer who is loyal to the British Raj kills Abhay. Nandini too kills herself in the grief. Next, they learn that the officer is still alive and has become a minister. Karan plans to revenge his death. He plans a conspiracy to kill the minister. Anjalis uncle DCP Baksh also helps them in their mission. And as good triumphs over evil, they too are successful.

== Cast ==
* Arya Babbar ...  Karan/Abhay
* Amrita Rao ...  Anjali Thapar/Nandini
* Ashutosh Rana ...  Tejeshwar Singhal
* Danny Denzongpa ...  CBI Officer Sikander Baksh
* Shakti Kapoor... Cameo appearance
* Ghanshyam ...  Kancha
* Ashish Vidyarthi ...  Rudra Singh

== Music ==
All track composed by Anu Malik.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = no

| title1 = Deewane Aate Jaate
| extra1 = Sonu Nigam, Alka Yagnik, Kunal Ganjawala
| lyrics1 =

| title2 = Main Pyaar Mein Hoon Shaan
| lyrics2 =

| title3 = Saari Umar Main Rahun
| extra3 = Sonu Nigam
| lyrics3 =

| title4 = Aaya Mahi Richa Sharma, Sunidhi Chauhan
| lyrics4 =

| title5 = Mujhe Rab Se Pyaar
| extra5 = Adnan Sami, Anuradha Sriram
| lyrics5 =

| title6 = Hoga Hoga
| extra6 = Sonu Nigam, Preeti & Pinky
| lyrics6 =

| title7 = Pyaar Mohabbat
| extra7 = Udit Narayan, Anuradha Paudwal
| lyrics7 = 
}}

==External links==
* 

 
 
 
 


 