Hurricane Smith (1952 film)
{{Infobox film
| name           = Hurricane Smith
| image          = 
| image size     =
| caption        = 
| director       = Jerry Hopper
| producer       =
| writer         = 
| narrator       =
| starring       = Yvonne de Carlo
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States English
| budget         =
| gross          = $1,175,000 (US rentals) 
}}
Hurricane Smith is a 1952 adventure film directed by Jerry Hopper.

==Plot==
Hurricane Smith steals a boat owned by Capt. Raikes, a slave trader, and heads to Australia with his men. There, he and other men pretending to be researching botany are actually searching for a cache of gold, while Smith also competes for the love of the lovely Luana.

==Cast== John Ireland as Hurricane Smith
* Yvonne de Carlo as Luana Whitmore
* Forrest Tucker as McGuire
* Lyle Bettger as Clobb
* Emile Meyer as Raikes

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 

 