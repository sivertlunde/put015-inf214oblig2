Nurse.Fighter.Boy
{{Infobox film
| name           = Nurse.Fighter.Boy
| image          = nursefighterboy.jpg
| caption        = 
| alt            = 
| director       = Charles Officer
| producer       = 
| writer         = Charles Officer
| starring       = Clark Johnson Karen LeBlanc
| music          = John Welsman
| cinematography = Steve Cosens
| editing        = James Blokland
| studio         = 
| distributor    = Mongrel Media
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} Canadian drama film, originally released in 2008.
 boxer who becomes a father figure for the young boy. The films cast also includes Walter Borden and Ndidi Onukwulu.

The film was directed by Charles Officer and written by Officer and Ingrid Veninger. It was the first feature film released through the Canadian Film Centres training program for emerging film directors since David Weavers Siblings (film)|Siblings in 2005.

==Awards and nominations== Best Motion Best Actor Best Actress Best Director Best Original Screenplay.

==External links==
*  

 
 
 
 
 