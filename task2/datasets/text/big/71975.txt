Hellraiser
 
 
 
{{Infobox film
| name = Hellraiser
| image = Hellraiser poster.png
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Clive Barker
| producer = Christopher Figg
| writer = Clive Barker
| based on = The Hellbound Heart   by Clive Barker Andrew Robinson Oliver Smith
| music = Christopher Young
| cinematography = Robin Vidgeon
| editing = Richard Marden Tony Randel
| studio = Cinemarque Entertainment BV Film Futures Rivdel Films
| distributor = New World Pictures
| released =  
| runtime = 93 minutes  
| country = United Kingdom United States
| language = English
| budget = $1 million   The Numbers. Retrieved 22 September 2012. 
| gross = $14,564,027    
}} series of sequels.

==Plot==
  Frank Cotton puzzle box from a dealer. In a bare room, Frank solves the puzzle box. Immediately, hooked chains emerge from it and tear into his flesh. Later the room is filled with swinging chains spattered with the remnants of Franks body. A black-robed figure picks up the puzzle box and returns it to its original state, restoring the room to normal.

Franks brother, Larry, has decided to move into his childhood home in an attempt to rebuild his strained relationship with his second wife, Julia, who had an affair with Frank shortly after marrying Larry. Larrys teenage daughter, Kirsty Cotton|Kirsty, has chosen not to live with her stepmother and moves into her own home.

The abandoned home shows traces of Franks recent presence, but Larry assumes Frank has fled the authorities. Larry cuts his hand on a nail, and drips blood on the attic floor, which conceals the remnants of Franks body. The blood resurrects Frank. That night, Julia finds Frank in the attic. Still obsessed with him, she agrees to harvest blood for him so that he can be fully restored and they can run away together. Julia begins picking up men in bars and bringing them back to the house, where she murders them. Frank consumes their blood, regenerating his body. Frank explains to Julia that he had exhausted all sensory experiences and sought out the puzzle box on the promise that it would open a portal to a realm of new carnal pleasures. When solved, the box opened up a portal to the realm of the "Cenobite (Hellraiser)|Cenobites", who subjected him to the extremes of sadomasochism.

Kirsty spies Julia bringing men to the house and, believing her to be having an affair, follows her to the attic, where she interrupts Franks feeding. Frank attacks her, but Kirsty throws the puzzle box out the window, creating enough of a distraction for her to escape. Kirsty retrieves the box and runs away, but, shaken by her experiences, collapses shortly thereafter. Awakening in a hospital, Kirsty solves the puzzle box, summoning the Cenobites and a two-headed monster, which Kirsty narrowly escapes from. The Cenobites leader, Pinhead (Hellraiser)|Pinhead, explains that although the Cenobites have been perceived as both angels and demons, they are simply "explorers" of the carnal experience, and they can no longer differentiate between extreme pain and extreme pleasure. Although they attempt to force Kirsty to return to their realm with them, she lets their leader know that one of their "subjects" has escaped, and suggests giving them Frank in exchange for her freedom. They accept.

Kirsty returns home, where Frank, posing as Larry, tells her he had to kill a deranged Frank. Julia shows her a flayed corpse in the attic, locking the door behind her. The Cenobites appear and demand the man who "did this". Kirsty runs out of the room and tries to escape, but is held by Julia and Frank. Frank reveals his true identity to Kirsty, and, when his sexual advances are rejected, he decides to kill her to complete his rejuvenation. He accidentally stabs Julia instead and drinks her blood without remorse.

Frank chases Kirsty to the attic, and when he is about to kill her, the Cenobites appear. Now sure he is their prey, they ensnare him with chains and tear him to pieces. They then attempt to abduct Kirsty. Ripping the puzzle box from Julias dead hands, Kirsty defeats the Cenobites by reversing the motions needed to open the puzzle box, sending them away in a burst of electricity. Her boyfriend shows up and helps her escape the quickly deteriorating house.

As Kirsty looks back at the house, it bursts into flames, and Kirsty throws the box into them. A vagrant who has been stalking Kirsty walks into the fire and retrieves the box before transforming into a winged creature and flying away. The box ends up in the hands of the merchant who sold it to Frank, who offers it to another prospective customer.

==Cast==
* Ashley Laurence as Kirsty Cotton
* Clare Higgins as Julia Cotton
* Sean Chapman as Frank Cotton Andrew Robinson as Larry Cotton Pinhead
* Chattering Cenobite Butterball
* Grace Kirby as Female Cenobite Oliver Smith as "Skinless" Frank / Frank the Monster
* Robert Hines as Steve
* Anthony Allen as Victim No. 1
* Leon Davis as Victim No. 2
* Michael Cassidy as Victim No. 3 Derelict
* Kenneth Nelson as Bill
* Gay Baynes as Evelyn
* Bryan Orcutt as Yukon Cornelius
* Niall Buggy as Dinner guest Dave Atkins as Moving man No. 1
* Oliver Parker as Moving man No. 2
* Pamela Sholto as Complaining customer
* Sharon Bower as Nurse
* Raul Newney as Dr. Joey Baxter

==Censorship==

Clive Barker had to make some cuts on the film after MPAA gave it an X rating. Following scenes were cut for R rating;

* Two and a half shots were excised from the first hammer murder, including a closeup of the hammer lodged in the victims head.
* In the scene where Julia murders another man, the actor playing the victim felt that it made sense for him to do so naked. The nude murder scene was shot but, ultimately, replaced with a semi-clothed version.
* Close-ups of Kirsty sticking her hand into Franks belly, exposing his guts.
* Longer version of the scene where Frank is being torn into pieces by the Cenobites hooks. Final shot where his head explodes and his brain messily splashes out was also cut.

In interview for Samhain magazine in July 1987, Barker mentioned some problems that censors had with more erotic scenes in the movie;

"Well, we did have a slight problem with the eroticism. I shot a much hotter flashback sequence than they would allow us to cut in.... Mine was more explicit and less violent. They wanted to substitute one kind of undertow for another. I had a much more explicit sexual encounter between Frank and Julia, but they said no, lets take out the sodomy and put in the flick knife."

Barker also said on the commentary for the movie that seduction scene between Julia and Frank was, initially, a lot more explicit; "We did a version of this scene which had some spanking in it and the MPAA was not very appreciative of that. Lord knows where the spanking footage is. Somebody has it somewhere…The MPAA told me I was allowed two consecutive buttock thrusts from Frank but three is deemed obscene!"  

==Soundtrack== industrial band Coil (band)|Coil. The music they supplied was rejected,  and Christopher Young provided a more traditional orchestral score for the finished movie. Coils score, which was apparently described by Barker in a complimentary manner as being "bowel churning",  has been released in isolation as The Unreleased Themes for Hellraiser and as part of the compilation Unnatural History II (CD) (1995).

Coils original theme was later covered by the Italian black metal band Aborym on their debut album Kali Yuga Bizarre.

Christopher Young went on to contribute the soundtrack to the first sequel,  , for which he won a Saturn Award for Best Music. Subsequent films in the series used music by different composers.
 Entombed recorded EP Hollowman.

UK black metal band Anaal Nathrakh sampled Frank Cottons final words and used in the track "Tractatus Alogico Misanthropicus". Canadian band Skinny Puppy also sampled "Jesus Wept" in the track "Fascist Jock Itch", as did Belgian Industrial act Suicide Commando for their track "Jesus Wept" on their Mindstrip album. More recently, the EBM group "Project Rotten" sampled Pinheads line "Oh, no tears please, its a waste of good suffering!" in their track "Confessions of a Killer".

Various extreme metal bands have used audio from the film in sampling and as introductions to songs. The most common and famous sample is Pinheads line "Well tear your soul apart".

In 1999,   (directed by Scott Derrickson) which was later thrown and released as an album titled "The Unspeakable" (BSI Records).

;Track listing 
# "Hellraiser" – 1:43
# "Resurrection" – 2:32
# "Hellbound Heart" – 5:05
# "The Lament Configuration" – 3:31
# "Reunion" – 3:11
# "A Quick Death" – 1:16
# "Seduction and Pursuit" – 3:01
# "In Loves Name" – 2:56
# "The Cenobites" – 4:13
# "The Rat Race Slice Quartet" – 3:15
# "Re-Resurrection" – 2:34
# "Uncle Frank" – 2:59
# "Brought on by Night" – 2:18
# "Another Puzzle" – 4:06
:Total length: 42:40

==Reception==
 
Released in the United Kingdom and United States in September 1987, the film grossed $14,564,000 in the United States and Canada.  

However, critic Roger Ebert was less than enthusiastic, commenting "This is a movie without wit, style or reason, and the true horror is that actors were made to portray, and technicians to realize, its bankruptcy of imagination. Maybe Stephen King was thinking of a different Clive Barker." 

As of August 2013, Hellraiser holds a 63% "Fresh" rating on review aggregate website Rotten Tomatoes based on 32 reviews. 
 Time Out conducted a poll with several authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  Hellraiser placed at number 80 on their top 100 list. 

==Home media==
In North America, Hellraiser has been released by   in a Limited Edition tin case which included a 48-page colour booklet and a reproduction theatrical poster for both films. Anchor Bay released the film on Blu-ray Disc|Blu-ray in 2009. This version retains all of the special features found on the 20th anniversary special edition DVD. In 2011, the film was re-released on Blu-ray by Image Entertainment under the "Midnight Madness" series label. This version contains no special features. However, various Blu-ray releases have since emerged with a highly variable selection of special features, although most of these are recycled from previous DVD releases.  

==Remake==
Dimension Films remake of Hellraiser was announced in November 2006.  French director Pascal Laugier was set to direct the film     but was later taken off the project due to creative differences with the producers;    
   Laugier wanted his film to be a very serious take whereas the producers wanted the film to be more commercial and appeal to a teen audience. 

On 20 October 2010, it was officially announced that Patrick Lussier and Todd Farmer were to direct and write, respectively, the reboot of the Hellraiser franchise. The films story would differ from the original film, as Lussier and Farmer did not want to retell the original story out of respect for Clive Barkers work. The film was to instead focus on the world and function of the puzzle box. However in 2011, Farmer confirmed that both he and Lussier were no longer attached to the project.  

On October 24, 2013, Clive Barker posted on his official Facebook page that he would be personally writing the remake of the original "Hellraiser" and that he had already completed a deal with Dimension Films Bob Weinstein. He also stated that he will be pushing for practical effects rather than CGI and the original Cenobite actor Doug Bradley would be returning as Pinhead.  Here is his official post:
"HOT FROM HELL! My friends, I have some news which may be of interest to you. A few weeks ago I had a very productive meeting with Bob Weinstein of Dimension Pictures, in the course of which I pitched a remake of the first HELLRAISER film. The idea of my coming back to the original film and telling the story with a fresh intensity - honoring the structure and the designs from the first incarnation but hopefully creating an even darker and richer film - was attractive to Dimension. Today I have officially been invited to write the script based upon that pitch. What can I tell you about it?
Well, it will not be a film awash with CGI. I remain as passionate about the power of practical make-up effects as I was when I wrote and directed the first HELLRAISER. Of course the best make-up in the world loses force if not inhabited by a first-rate actor. I told the Dimension team that in my opinion there could never be a Pinhead without Doug Bradley, and much to my delight Bob Weinstein agreed. So once the papers are signed, I will open a Lemarchand Configuration, dip my quill in its contents and start writing. I promise that there will be nowhere on the Internet where the news of my progress will be more reliable than here, because the only author of these reports will be Your Infernal Corespondent, me.
My very best wishes to you 
all, my friends. 
Clive."

==Comic books==
The Hellraiser franchise was adapted to comic book form in the late 1980s and early 1990s. Three volumes are available digitally exclusively through Devils Due Digital. 

==See also==
*List of monster movies

==References==
 

==External links==
 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 