Cacería
{{Infobox film
| name = Cacería
| image = Caceriaposter.jpg
| caption =
| director = Ezio Massa
| producer = Marcelo Altmark
| writer = Ezio Massa Jorge Bechara
| narrator =
| starring = Luis Luque Claribel Medina Juan Palomino Matias Sansone Carlos Leyes
| music = Mariano Nuñez West
| cinematography = Mariano Cúneo Ada Frontini
| editing = Luis César DAngiolillo Ezio Massa
| distributor = Maleto Films
| released = April 11, 2002
| runtime = 90&nbsp;minutes
| country = Argentina
| language = Spanish
| budget =
| preceded_by =
| followed_by =
}} Argentine action thriller film directed and written by Ezio Massa and Jorge Bechara. Starring Luis Luque, Claribel Medina, Juan Palomino and Carlos Leyes.

==Plot==
Running away from his criminal history, Daniel (Luis Luque) returns to his hometown of Redemption, a small town near Argentina. He reunites with his old flame Elisa (Claribel Medina) and childhood friend Miguel (Carlos Leyes), hoping to establish a new life for himself. However, his old gangster boss, Lucas (Juan Palomino), plans to hunt him down and force him to confront his past. Also starring Matias Sansone and Carlos Leyes.

==Cast==
* Luis Luque ... Daniel
* Claribel Medina ... Elisa
* Juan Palomino ... Lucas
* Matías Sansone ... Nicolas
* Carlos Leyes ... Miguel
* Fernando Díaz ... Coco
* Pochi Ducasse ... Voz Doña Clara
* Horacio Erman ... Padre Guido
* Bernardo Forteza ... Carlos
* Carlos Galettini ... Beltran
* Miguel Gallardo ... Cajide
* Sebastián García ... Gaby Miguel Gutiérrez ... Cajide
* Carlos Kaspar ... Gordo
* Gustavo Leyes ... Cabo Leyes
* Puky Maida ... Rodo
* Ezio Massa ... Dealer de armas
* Carmela Moreno ... Mecha
* Fabián Rendo ... Poli
* Carlos Roffé ... Micky
* Maria Roman de Caloni ... Doña Clara
* Golo Sid ... Don Antonio

==External links==
*  
*   at cinenacional.com  .

 
 
 
 
 


 
 