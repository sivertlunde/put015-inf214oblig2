Loves Me, Loves Me Not
 
 
{{Infobox film
| name           = Loves Me, Loves Me Not
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Jeff Newitt
| producer       = 
| writer         = Jeff Newitt
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        =  
| studio         = Aardman Animations Channel Four Films
| distributor    = Diaphana Films (1994) (France) (theatrical) Alta Films (Spain) (all media)
| released       = 14 February 1993 (UK) 21 December 1994 (France) 17 August 1995 (Spain) 5 July 1996 (Portugal) 
| runtime        = 8 min
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}

Loves Me, Loves Me Not is a short film created by Aardman Animations. It is known as Me quiere..., no me quiere in Spanish. 

==Production==
 

==Plot==
Imdb explains the plot: "A young man is in love with an image in a picture frame. To find out if the love he feels is returned he picks a flower and starts picking the petals off it." 

==Cast==
 

==Critical reception==
Loves Me, Loves Me Not received a rating of 6.5/10 from 166 users. 

==Awards and nominations==
This is a list of awards and nominations of Loves Me, Loves Me Not.

 
|-
| 1993
| Jeff Newitt 
| Berlin International Film Festival Golden Berlin Bear for Best Short Film
|   
|-
|}

==References==
 

 

 
 
 