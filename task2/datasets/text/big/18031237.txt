Maria, Mirabela
{{Infobox film
| name           = Maria, Mirabela
| image          = Mariamirabela.jpg
| caption        =
| director       = Ion Popescu-Gopo
| studio         = Moldova-Film, Soyuzmultfilm, Casa de Filme 5
| writer         = Ion Popescu-Gopo
| starring       = Gilda Manolescu Medeea Marinescu Ingrid Celia
| music          = Eugen Doga
| cinematography = Alecu Popescu
| editing        = Elena Belyavskaya S. Kusursuz
| distributor    =
| released       =   January 8, 1983
| runtime        = 70 min.
| country        =  /   Romanian Russian Russian
}} Soviet movie studios. The Romanian premiere took place on January 4, 1982 in Bucharest, Soviet — in March of the same year in Moscow. http://www.imdb.com/title/tt0155858/  

== Maria, Mirabela (1981) ==

===Plot===
Kvakis young frog meets old acquaintances — a butterfly Omid and a glowworm Skiperich. Together they watch the girls playing on a clearing, Maria and Mirabella. Kvaki remembers adventures which they once happened to endure together, and begins the story.

The history of joint adventures of friends begins with that the young frog meets the Forests fairy and from false modesty says to it that frogs for anything arent suitable. In punishment for lie ("From a lie water freezes") the fairy freezes a stream together with Kvakis feet.

Two sisters playing a ball — Maria and Mirabela come across the young frog who got stuck in ice. They want to release Kvaki who got to trouble. They are helped by Kvakis colleagues — other young frogs. Cheerful dance they melt ice round Kvaki, and girls take it together with the remained ice slice with themselves. They go to the fairy of the wood that that removed the paternoster and released Kvaki from ice.

On the way to the fairy of the wood of the girl meet other fantastic beings who need the help. The glowworm by the name of Skiperich cant shine because when it lights the spark, his shoes ignite. The butterfly Omid cant fly as is afraid of height.

Girls should be in time to the fairy of the wood till midnight, but time expires. Then on a visit the strict King of hours has Maria and Mirabella resort to cunning and sing to him a lullaby that that fell asleep, and time stopped. But, the fallen asleep king pinched Marias dress therefore Mirabela hurries to the fairy alone. But when it reaches the destination, it appears that when time stopped, the fairy of the wood and her suite fell asleep sound sleep. Girls awake the king and, knowing his power, ask to make so that the fairy didnt disappear.

However the fairy doesnt wish to deal at all with others problems as, her suite — little maids of honor the Winter, Spring, Summer and Fall ached. And besides in a teapot there is no water. To prepare the medical tea helping for cold, girls run behind water which flows from two springs. And here Kvakis young frog helps girls to choose from two springs — a source with the clearest water. As soon as Kvaki understands that too can be useful as right there ice on his feet thaws without any magic intervention. Maria with Mirabella want to warm up water in a teapot, but they have no matches to light gas on a plate. They are helped by Skiperich, setting fire to gas the burning shoe which burns down completely. Having been frightened for a glowworm, the butterfly Omid flies up and brushes away it with wings, forcing down from a shoe a flame. Thus, the fear for the friend helps it to fly up, and Mirabela gives to a glowworm new, toy shoes which shine as the presents.

The final song how it is fine to live when friends are near sounds. During a song it appears that all adventure dreamed girls, the fairy of the wood turns around in their mother, and the king of hours — the father.

The film starred Gilda Manolescu and Medeea Marinescu. Additional crew includes art directors Viktor Dudko and Lev Milchin.

Musical lyrics were written Grigore Vieru and translated into Russian by Valentin Berestov and Eugene Agranovici. Music was composed by Eugen Doga and performed by Romanian Radio and Television Orchestra, conducted by Cornel Popescu. The songs were sung in Romanian by Anda Călugăreanu, Mihai Constantinescu, Alexandrina Halic, Paula Radulescu, Adrian Stefanescu and Vocal Group 5T. Russian songs were performed by Leonid Serebrennikov.

A soundtrack of the Soviet version was authored by Dan Ionescu and Vladimir Kutuzov and character animation was done by Boris Kotov. Editors were V. Istrate, M. Gaspar, and N. Savicev.

==  Maria, Mirabela in Tranzistoria == 1988 continues the adventures of Maria and Mirabela, this time in an imaginary world inside the TV. Along with the characters played by actors in this movie and three animated characters appear: Croaky, Skipirich and Omide, which symbolize the three vital elements of nature: water, fire and air, without which there is no life on earth. They turn into humans and interpreted by live actors.

The film starred Ioana Moraru, Stela Popescu and Andrianu Kuchinska. Additional crew includes co-director Vladimir Pekar, art directors Tatyana Kolyusheva, Constantin Simionescu, and assistant art director Sergey Mavrody. The film is a Romanian-Soviet co-production made by the Romanian Casa de Filme studio in collaboration with the Soviet Soyuzmultfilm animation studio.

== Awards ==
* Best music award, by composer Eugen Doga, by Filmmakers Association of Romania (ACIN), 1981  Union of Cinematographers of Romania (ed.) - "Filmmakers Awards 1970-2000" (publishing and printing Intact, Bucharest, 2001), 48. 
* Honorary achievement award to Alexandru Popescu, Filmmakers Association of Romania (ACIN), 1981 
* Special Mention of the Jury at the 1981 Film Festival in Athens.
* Special prize and diploma in the 1982 All-Union Festival of Childrens Films in Tallinn, Estonia http://www.imdb.com/title/tt0155858/trivia 
* The Grand prize at the Festival of Childrens Films in Piatra Neamt, Romania. 1982  
* Prize for the animation/live action combination at the Festival in Chicago, USA. 1984 Tudor Caranfil - "Universal Dictionary of movies" (Letter Publishing International, Bucharest, 2008)  
* Honorary Mention at the Festival in Quito, Ecuador. 1986 
* Prize at the International Film Festival in Giffoni, Italy. 1989 

==Music==
In 1983 the "Melodiya" firm let out a plate with the audiofairy tale based on this movie on which the announcers text was written down in Russian and all songs — on Romanian.

In the 1990th years on audio cassettes the Twic Lyrec edition published the audiofairy tale according to the animated film of the same name with Alexander Pozharov and Evgeny Kondratyevs texts.

==Production==
The Soviet film studios "Moldova-Film", "Soyuzmultfilm" and "Sovinfilm" and the Romanian film studio of "Casa de Filme 5" took part in creation of the movie. The scenario is written by the director Ion Popescu-Gopo. According to the Romanian critics the plot of the movie was obviously cast by the fairy tale "Fata babei și fata moșneagului" of the Romanian classic Ion Creangă.

Shootings took place in the summer of 1981. To Gildas Manolesku and Medea Marinesku was for six years then they werent able to read yet because of what the most part of their remarks in the movie are improvisations. As Kvaki, Omid and Skipirich were animation characters, in scenes where Maria and Mirabela interact with them, figures from plasticine that girls knew were used, in what party to them to look and speak.

==External links==
* 
* 
* 
* 
*  on animator.ru

== References==

 
*  

 
 
 
 
 
 
 
 