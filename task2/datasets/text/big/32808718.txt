The Kangaroo Kid (film)
 
{{Infobox film name           = The Kangaroo Kid image          =  caption        =  producer       = T.O. McCreadie Harry C. Brown (assoc) Ben Sheil (exec) director       = Lesley Selander writer         = Sherman Lowe based on  = story by Anthony Scott Veitch starring       = Jock Mahoney Veda Ann Borg music          = Wilbur Sampson cinematography = W. Howard Greene editing        = Alex Ezard studio          = Allied Australian Films distributor = British Empire Films (Aust) Eagle Lion (US)  released       = September 1950 (US) March 1951 (Sydney)  runtime        = 72 mins country        = Australia  USA language       = English budget         = £90,000   accessed 28 December 2011 

}} western film directed by Lesley Selander.

==Plot==
In the 1880s, the Remington detective agency sends Tex Kinnane to Australia to track down a notorious gold robber and murderer called John Spengler. In Sydney, Tex makes friends with Baldy Muldoon and travels with him to the small town of Gold Star, where Baldys wife runs the local saloon. Tex adopts a baby kangaroo and earns the name "Kangaroo Kid". He is hired as a stage coach driver and befriends barmaid Stella Grey, who offers to look after his kangaroo.

Tex is challenged to a shooting match by local thugs Phil Romero and Robey, but Tex outshoots them, causing a fistfihgt. Sgt Jim Penrose warns him about his behaviour. Penrose visits his girlfriend, Mary, who says that her father, miner Steve Corbett, has been acting strangely since Tex arrived and wants to leave town.
 
Vincent Moller, an American living in Australia for health reasons, plans to rob the stage coach with Crobett, Romero and Robey and implicate Tex. Corbett is reluctant to join in and Moller plans to kill him.

Tex is driving the stage when it is held up by Romeo and Robey, who kill the guard and knock out Tex, leaving him in the bush. Sgt Jim Penrose is convinced he is guilty. He tracks down Tex and puts him in gaol for robbery and murder. Moller visits Tex and agrees to arrange his escape if he leaves the country quickly. This makes Tex suspicious. He escapes and proves that Moller is John Spengler.

Tex takes Moller back to America but promises to return for Stella. 

==Cast==
*Jock Mahoney as Tex Kinnane
*Veda Ann Borg as Stella Gret 
*Guy Doleman as Sergeant Jim Preston
*Martha Hyer as Mary Corbett
*Douglass Dumbrille as Vincent Moller
*Alec Kellaway as Baldy Muldoon Grant Taylor as Phil Romero
*Alan Gifford as Steve Corbett
* Hayde Seldon as Ma Muldoon
* Frank Ransome as Robey
* Clarrie Woodlands as Black Tracker
* Charles McCallum as Cummings
* Raymond Bailey as Quinn
* Ben Lewin as Fanning
* Sheila McGuire as Girl in Carriage

==Production==
The McCreadie brothers had made two films and for their third decided on a co-production with Hollywood.  It was intended to be the first of a series of co-productions and was budgeted at US$200,000  Producer Howard Brown had extensive experience making movies on location. 

The film was based on a story by Australian writer, Tony Scott Veitch, but rewritten by an American screenwriter.   and Adele Jurgens were announced for the leads. 

Selander arrived in February 1950 and filming began the following month.  Location shooting was done in   (1939). Hyer was a last-minute replacement for Dorothy Malone, who was too ill to travel.  It was an early star role for stunt man Jock Mahoney.

Filming took six weeks and Selander returned to Australia in May. 

==Reception==
The movie was meant to be the first of a series of co-productions involving the McCreadie Brothers Embassy Pictures – two more Kangaroo Kid films were announced, to be shot in December 1950 BACALL CONTRACT AT WARNERS ENDS: HIS CREDO: MERRIMENT
By THOMAS F. BRADY New York Times (1923-Current file) 12 July 1950: 33.   – but this did not eventuate.  Reviews were unenthusiastic.  

==See also==
*Stingaree (1934 film)|Stingaree (1934)
*Rangle River (1936)
*Captain Fury (1939) Raw Deal (1977)
*Quigley Down Under (1990)

==References==
 

==External links==
* 
*  at TCMDB
*  at Oz Movies
 
 
 
 
 
 
 
 
 
 
 
 
 