The Kid from Borneo
{{Infobox film
| name           = The Kid from Borneo
| image          = Kid from bornei.JPEG
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = F. Richard Jones Hal Roach
| writer         = Carl Harbaugh Hal Roach H. M. Walker Hal Yates
| starring       = Our Gang John Lester Johnson
| music          = Leroy Shield Marvin Hatley
| cinematography = Francis Corby
| editing        = William H. Terhune
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 18 41"
| country        = United States
| language       = English
}}

The Kid from Borneo is a comedy short subject; part of the Our Gang (Little Rascals) series. It was produced and directed by Robert F. McGowan for Hal Roach, and was originally released to theaters by Metro-Goldwyn-Mayer on April 15, 1933.    It was the 122nd (34th talking episode) Our Gang short that was released.

==Plot==
Dickie, Dorothy, and Spankys Uncle George are in town. Uncle George manages a show called "Wild Man from Borneo", featuring a foreigner with a mentality of a seven-year-old child. The kids father refuses to let the real Uncle George come over so their mother has the kids visit him at the shows location. Their mother explains to the kids that Uncle George is the black sheep of the family. 

 They come to the show location and another worker tells the kids that Uncle George will be right there. They mistake the Wild Man from Borneo named Bumbo (a man dressed with horns) for the real Uncle George, who is never seen as he was off running an errand. The Wild Man spots Stymies candy, he shouts "Yumm Yumm Eat-Em-Up, Eat-Em-Up!!!", and chases the kids back to their house. Once there, the wild man asks for more candy, saying “Yumm, yumm, Eat-Em-Up!” and chases the kids throughout the house. The wild man runs after them, messes the house up, eats everything in the refrigerator whole (including containers plus eggs & their shells!), drinks wine, gets drunk and further destroys much of the house while continuing to chase the kids inebriated, wielding a kitchen knife, repeating "Eat-Em-Up, Eat-Em-Up". The Little Rascals themselves fend off the Wild Man in their usual mischievous ways. Stymie is almost done in until Dorothy cracks the ruffian over the head with a vase. The Kid from Borneo is countervailed by the Rascals, but at the expense of the destruction of some of the house by the mistaken Uncle George.

Then, the mother arrives and asks Spanky where Uncle George is.  She is directed upstairs and goes to an upstairs bedroom where she finds the Wild Man from Borneo lying in bed with the covers pulled over him, recovering from the cheap wine and the kids attacks.  Thinking that its her brother, the real Uncle George, she is shocked to find the primitive tribesman Bumbo and jumps out of the second-story window to escape the brute. When the father comes home soon after, Dickie says "Uncle George is upstairs."  The dad rolls up his sleeves, vows to punch Uncle George, and heads upstairs.  He looks for George and finds the Wild Man instead, who throws him literally out the 2nd floor window with no effort whatsoever. Then Spanky blasts the Wild Man out of the same window with a Roman candle shot to the rear.  Out on the ground, the Wild Man joins the father and mother, where all three run off into the distance as Spanky giggles with his signature laugh.

==Cast==
===The Gang=== Matthew Beard as Stymie
* Tommy Bond as Tommy
* Dorothy DeBorba as Dorothy
* Bobby Hutchins as Wheezer
* George McFarland as Spanky Dickie Moore as Dick
* Dickie Jackson as Dickie
* Henry Hanna as Our Gang member
* Pete the Pup as Himself

===Additional cast===
* Harry Bernard as Sideshow manager
* Otto Fries as Kids Dad
* Dick Gilbert as Worker
* John Lester Johnson as Bumbo, "The Wild Man from Borneo"
* May Wallace as Kids Mom

==Notes==
The Kid from Borneo was allegedly withdrawn from syndication in 1971 due to perceived racism and mistreatment of handicapped people. 

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 