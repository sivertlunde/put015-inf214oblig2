Deadly Sins (film)
{{Infobox Film |
  name     = Deadly Sins |
    image          =|
  caption  = Poster of Deadly Sins |
  writer         = Malcolm Barbour John Langley |
  starring      = David Keith Alyssa Milano |
  director      = Michael Robison |
  producer    = James Shavick |
  distributor  = WarnerVision Films |
  released   = October 17, 1995 |
  runtime     = 98 minutes |
  language  = English |
  budget      =  |
  }} directed by Michael Robison, and stars David Keith and Alyssa Milano. It had a limited VHS release. In Germany, the film was released on VHS under the title Sins.

==Plot==
Eleven girls have disappeared from an Eau Claire, Wisconsin Catholic school in the last five years. When one of them, an orphaned outsider named Gwendolyn (Telek), is found dead, hanged at the church bells, Seattleite deputy sheriff Jack Gates (Keith) is assigned to the task to the mystery. When he arrives, he is upset to find out that the unexperienced Doc (Mulligan) has already claimed to have examined the body and that the body has already been removed. Mother Superior (Perry) assigns her secretary, and the schools history teacher, nun Cristina Herrera (Milano) to help Jack with the case. Jack is especially interested in a stolen cross that was also reported along with the death. Through one of the students, Beth (Clark) - a popular student who bullies Polly (Collins) - Jack learns that the school keeps the spirit of Mother Bernadette, the former mother superior, alive. Mother Bernadette has her own - locked from the outside - sanctuary and the students are taught that she is always with them.

That night, Beth sneaks out to have sex with delivery boy Eric (Bacic). Meanwhile, student Suzy Carroll (Copping) is being chased and murdered by an unknown person. The following morning, Jack learns from Doc that Gwen did not commit suicide, but was murdered: there was a cross sign carved in her stomach after her death. Doc thinks that Gwen was being punished for being four months pregnant at the time of her death. Jack is next informed that Suzy has disappeared, and that this has happened before at the school. He gets mad at Mother Superior for not having informed him earlier, but she explains that she did not bother, because all the girls were of age, and were probably eloping. Meanwhile in class, Beth accuses her teacher Gray (Hanlon) of having impregnated Gwen; Headmaster Gray responds by revealing that he knows about her affair with Eric.

The following night, Marie (Lenhart) is chased by an unknown person near the graveyard. She is saved by Jack, and claims that a woman has touched her.  She tries to kiss him, but Jack holds his distance, as does he with Rita (Bates), a waitress of a nearby bar whom he befriended. The next day, Cristina shares her doubt in the integrity of some of the staff with Jack: she finds it strange that Father Anthony graduated top of his class at Harvard, and is now teaching at the small Catholic school; Mother Superior supposedly killed her husband in the past; and Emily (Warn-Pegg) - the schools cook - gave birth to a stillborn child. She is unable to find out, however, why Headmaster Gray left his previous school eight years earlier. Jack has recently found out that Headmaster Gray, previously known as Marc Anthony, was arrested in Webster, Illinois for having sex with one of his students; the charges were later dropped.

The girls, meanwhile, fear that they might be the next to disappear. Nevertheless, Beth sneaks out to meet with Eric, but he is stabbed to death by a person dressed in white. Beth is chased as well, but escapes; she later claims to have witnessed the presence of Mother Bernadette. Jack then confronts Gray with his record, and then finds sex tapes of the missing girls and Beth among Grays stuff. He concludes that Gray is the killer, but Gray kills himself before an arrest can be made. Jack celebrates having solved the case with Cristina, who reveals herself not as a nun, but as a private investigator hired by the church. They end up having sex that night.

When Marie is reported missing, Jack and Cristina realize that Gray was not the killer. Cristina concludes that all the girls murdered were good girls and that the murderer must have learned of their innocence through the confessional. Cristina finds a secret tunnel behind the confessional, and is knocked unconscious by the murderer. Jack tries to rescue her, but is stabbed in the stomach by the murderer, who has revealed herself as Emily. Cristina eventually rescues Jack by stabbing Emily to death with a cross. They escape the tunnel and later conclude that Gwen killed herself all along, because she was the only girl who had a sin and thus could not have been a victim of Emily.

==Cast==
*David Keith as Jack Gates
*Alyssa Milano as Cristina Herrera 
*Terry David Mulligan as Doc
*Corrie Clark as Elizabeth Beth
*Heidi Lenhart as Marie
*Pamela Perry as  
*Peter Hanlon as Headmaster Charles Gray
*Joely Collins as Polly
*Jo Bates as Rita
*Oliver Becker as Father Anthony
*April Telek as Gwendolyn Gwen Jones
*Ann Warn Pegg as Emily
*Robert Jones as Billy
*Steve Bacic as Eric
*Jennifer Copping as Suzy Carroll
*Mary McDonald as Mrs. Gray

==Production==
The film was shot between August 20 and September 10, 1994. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 