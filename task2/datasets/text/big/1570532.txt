The Set-Up (1949 film)
For the 2011 film, see Setup (2011 film).
{{Infobox film
| name           = The Set-Up
| image          = SetupPoster.JPG
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Robert Wise
| producer       = Richard Goldstone
| screenplay     = Art Cohn
| based on       =  
| starring       = Robert Ryan Audrey Totter George Tobias
| music          = C. Bakaleinikoff
| cinematography = Milton R. Krasner
| editing        = Roland Gross
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} 1928 poem written by Joseph Moncure March.  The film is about the boxing underworld.

==Plot==
Stoker Thompson (Robert Ryan) is a 35-year-old has-been boxer. Tiny (George Tobias), Stokers manager, is sure he will continue to lose fights, so he takes money for a "dive" from a mobster, but is so sure that Thompson will lose that he doesnt tell the boxer about the set-up.
 Alan Baxter), a feared gangster, is behind the set-up, Thompson refuses to give up the fight and mushes on.

Thompson wins the vocal support of blood-thirsty fans who had at first rooted against him. In the end, he defeats Nelson, but Little Boy has Stokers right hand broken as punishment.  Julie (Audrey Totter), Stokers wife, supports him in the end.

==Cast==
* Robert Ryan as Bill "Stoker" Thompson
* Audrey Totter as Julie Thompson
* George Tobias as Tiny Alan Baxter as Little Boy
* Wallace Ford as Gus
* Percy Helton as Red
* Hal Fieberling as "Tiger" Nelson
* Darryl Hickman as Shanley James Edwards as Luther Hawkins David Clarke as Gunboat Johnson
* Phillip Pine as Tony Souza Arthur Weegee Fellig (New York photographer) has a cameo as the timekeeper

==Background==
In 1947, almost two decades after Marchs poem was published, RKO paid March a little over a thousand dollars for the rights.   Although March had had nearly a decade of Hollywood writing credits during the 1930s (working on what a 2008 essay in The Hudson Review called "one forgotten and now unseeable film after another"), RKO did not ask him to adapt his own poem. 

The screen adaptation including a number of changes from the poem.   The boxers name was changed from Pansy Jones to Stoker Thompson; his race was changed from black to white, he went from being a bigamist to being devotedly married, and Jones beating and subsequent death on a subway track was turned into an alley beating and a shattered hand. In a commentary accompanying the DVD for the film, Robert Wise attributes the change in race to the fact that RKO had no Afro-American star actors under contract. Although the film did have an African American actor (James Edwards) in a minor role as a boxer, Edwards was not a "star" under the then existing studio rules. March later commented on the changes for an Ebony (magazine)|Ebony interview, saying:  Jim Crow South.

The prizefight in the adaptation features an exchange of blows between Stoker and his opponent that is very close to the original, though the opponents name is changed. 

Joan Blondell was first thought of the wifes character before signing Audrey Totter for the role.
 real time narrative structure, three years before the device was used in High Noon.    Prior to The Set-Up, Richard Goldstones production credits had been limited to a half-dozen Our Gang comedy shorts. 

==Reception==

===Critical response===
When the film was released The New York Times reviewed the drama and lauded the pictures screenplay and the realistic depiction of the boxing Social environment|milieu; they wrote, "This RKO production...is a sizzling melodrama. The men who made it have nothing good to say about the sordid phase of the business under examination and their roving, revealing camera paints an even blacker picture of the type of fight fan who revels in sheer brutality. The sweaty, stale-smoke atmosphere of an ill-ventilated smalltime arena and the ringside types who work themselves into a savage frenzy have been put on the screen in harsh, realistic terms. And the great expectations and shattered hopes which are the drama of the dressing room also have been brought to vivid, throbbing life in the shrewd direction of Robert Wise and the understanding, colloquial dialogue written into the script by Art Cohn." 

Jefferson Hunter in a summer 2008 essay for The Hudson Review, said: 
:All through The Set-Up, we see confirmed the oldest of truisms about film, that it tells its stories best in images, in what can be shown—a crowd’s blood lust, the boxers’ awareness of what’s coming to them in the end—as opposed to what is spoken or narrated.

===Awards===
Wins
*   Prize, Robert Wise; 1949.   

Nominated
* British Academy of Film and Television Arts: BAFTA Film Award, Best Film from any Source, United States; 1950.

==Notable quote==
* Red: I tell you, Tiny, you gotta let him in on it.
:Tiny: How many times I gotta say it? Theres no percentage in smartenin up a chump.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 