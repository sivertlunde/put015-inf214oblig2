Son of Saul
 
{{Infobox film
| name           = Son of Saul
| image          = 
| caption        = 
| director       = László Nemes
| producer       = 
| writer         = László Nemes Clara Royer
| starring       = Géza Röhrig
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Son of Saul ( ) is an upcoming Hungarian drama film directed by László Nemes. It has been selected to compete for the Palme dOr at the 2015 Cannes Film Festival.   

==Cast==
* Géza Röhrig as Saul
* Levente Molnár as Abraham
* Urs Rechn as Biedermann
* Sándor Zsótér as Doctor
* Todd Charmont as Braun
* Christian Harting as Busch
* Kamil Dobrowolski as Mietek

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 