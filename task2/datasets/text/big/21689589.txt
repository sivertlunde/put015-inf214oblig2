Us Now
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Us Now
| image          = UsNowAvatar.jpg
| caption        =
| director       = Ivo Gormley
| producer       = Hugh Hartford ( )
| writer         =
| researcher    =  Matan Rochlitz
| music          = Orlando Roberton ( )
| cinematography = Robin Fox
| editing        = Mark Atkins
| released       =  
| runtime        = 60 minutes
| country        = UK
| language       = English
}}

Us Now is a documentary film project "about the power of mass collaboration, the government and the Internet"  The New York Times describes it as a film which "paints a future in which every citizen is connected to the state as easily as to Facebook, choosing policies, questioning politicians, collaborating with neighbours." 

The documentary weaves together the perceptions of leading thinkers on the power of the web, with the overriding suggestion that people gain a sense of satisfaction from active participation rather than symbolic representation in decision-making processes.  ( , editor of Channel 4s FourDocs)

The project claims the founding principles of mass collaborative projects, including transparency (behavior)|transparency, self-selection and  open-participation are nearing mainstream social and political lives.  Us Now describes this transition and confronts politicians George Osborne and Ed Miliband with the possibilities for collaborative government as described by Don Tapscott and Clay Shirky amongst others.

==Synopsis==
The Us Now website describes the project as an examination of the role of the internet, and more specifically   whose Self-organization|self-organising structures may "threaten to change the fabric of government." 

==Release and distribution==
Us Now premiered at the Prince Charles Cinema in December 2008 and was released in spring 09 for free online viewing. 
All of the material generated during the project is available to view and download on a Creative Commons license "and also encourages others to remix the core content with the 20 hours of footage available on the Us Now website (and presumably their own material) to draw other conclusions." 
Following release online the film was screened on Channel 4s More4 on 11 July 2009.  In late 2009 Us Now was the first film to be distributed on VODO,   a legal torrent distribution website, and received 100,000 downloads in its first five days of being available.  The film has been translated and subtitled into 29 languages on the dotSUB  platform and has been screened in many locations across the world. 

==See also==
* Electronic direct democracy
* Open source governance
* Participatory democracy

==References==
 

==External links==
*  
*  . The film itself
*  . International group of projects instantiating a vision similar to the one in this film
*   on Telegraph.co.uk
*  
*  . Estadao newspaper (Brazil)
  
 

 
 
 
 