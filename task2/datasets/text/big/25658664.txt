The Body (1970 film)
{{Infobox film
| name           = The Body
| director       = Roy Battersby
| producer       = Roy Battersby
| narrator       = Frank Finlay Vanessa Redgrave
| music          = Ron Geesin Roger Waters
| cinematography = Tony Imi
| editing        = Alan Cumner-Price
| studio = Kestrel Films distributor = Anglo-EMI (UK) MGM (USA)
| released       = 1970 (UK) 24 February 1971 (US)
| runtime        = 93 minutes
| country        =   English
}}

The Body is a 1970 UK scientific documentary film directed and produced by Roy Battersby. In the film, external and internal cameras are used to showcase the human body. 

The films narrators, Frank Finlay and Vanessa Redgrave, provide commentary that combines the knowledge of human biologists and anatomical experts. The films soundtrack, Music from the Body, was composed by Ron Geesin and Roger Waters, and includes songs that were made using the human body as a medium.  
==Reception==
In August 1971 Nat Cohen, whose company distributed the film, said it had recouped its negative cost in the Far East alone. 

The movie will be released on DVD on 7 October 2013.

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 