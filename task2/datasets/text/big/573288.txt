Babe: Pig in the City
{{Infobox film
| name           = Babe: Pig in the City
| image          = Babe pig in the city.jpg
| caption        = Theatrical release poster George Miller
| writer         = George Miller Judy Morris Mark Lamprell
| based on       = Characters by Dick King-Smith Doug Mitchell Bill Miller
| distributor    = Universal Pictures
| studio         = Kennedy Miller Productions Childrens Television Workshop
| editing        = Jay Friedkin Margaret Sixel
| music          = Nigel Westlake Randy Newman (songs)
| cinematography = Andrew Lesnie
| released       = November 25, 1998
| starring       = Magda Szubanski James Cromwell Mary Stein Elizabeth Daily (voice) Danny Mann  (voice) Steven Wright (voice)
| narrator       = Roscoe Lee Browne
| runtime        = 96 minutes
| country        = Australia   United States
| language       = English
| budget         = $90 million 
| gross          = $69,131,860
}} George Miller, who co-wrote and produced the original film. Most of the actors from the first film reappeared as their respective roles, including James Cromwell, Miriam Margolyes, Hugo Weaving, Danny Mann, and Magda Szubanski. However, most of them have only brief appearances, as the story focuses on the journey of Babe (now voiced by Elizabeth Daily) and the farmers wife Esme (Szubanski) in the fictional city of Metropolis.

Despite being a sequel to a highly acclaimed childrens film which, for the most part, had a charming and light-hearted atmosphere, Pig in the City had an unexpected darker tone and contained more mature subject matter. As a result of its dark tone, Pig in the City was a flop at the box office and received mixed reviews from critics, although Gene Siskel and Roger Ebert highly praised the film, with Ebert calling it better than the first one and Siskel later naming it the Best Movie of 1998.   In the years since its release, the film has come to be seen as an initially underrated work.  

==Plot==
 
Set after the events of the first film, Babe (now voiced by Elizabeth Daily) and his master, farmer Arthur Hoggett (James Cromwell), are given a welcome home parade after Babes success as a "sheepdog". One day, Babe inadvertently causes an accident which results in Arthur ending up in the hospital. Without her husband, Esme has no success tending the farm on her own. A few days later, two men from the bank tell her she and Arthur have not paid their rent on time, and at the end of the month, Esme and Arthur will be evicted from their farm. Among the many letters from their fans, Esme locates one saying that if she enters Babe in a sheepdog herding contest, held at a fair far away, she will win a large amount of money. She decides to enter Babe, and they leave the farm together.

At the airport in the city of Metropolis, a sniffer dog falsely senses that Babe and Esme are carrying illegal substances and drugs. Airport security interrogate them, causing them to miss their connecting flight to the fair. Babe and Esme spend the night in the airport. An airport cleaner tells them about a hotel called the Flealands Hotel that takes animals, so Esme and Babe go there. While Esme runs an errand, Babe goes after a white capuchin monkey named Tug after he steals Esmes suitcase. Fleeing into a hotel room, Babe follows it and meets three clothed chimpanzees: Bob, his pregnant wife Zootie and his little brother Easy, as well as Thelonius, an orangutan butler for the landladys elderly uncle Fugly Floom (Mickey Rooney), a clown who kidnaps Babe to use in his act. Babe initially refuses but accepts when the chimps mention a reward hell receive after doing the act. When Esme returns, Fugly tricks her into thinking that Babe ran into the city. Esme goes to look for Babe, but is arrested after a gang of street thugs try to snatch her purse. Meanwhile, Fugly performs his clown act in a hospital, but Babe accidentally trips him and he throws a flaming torch into the stage curtains which catch fire, activating the sprinklers and forcing everyone out.

The next morning, Fugly goes to the hospital in a food coma escorted by the landlady. Thelonius is in grief about his master and Easy tries to reassure him . Babe witnessed this and sits in his room, hungry and waiting for Esme to return. That night, the chimpanzees try to steal food from a store and use Babe to distract two dogs, who chase Babe around the city and back to the canal in front of the hotel. Babe falls into the canal and swims away. One dog attempts to follow, but its chain becomes lodged against the bridge struts which dangles him in the water with his head submerged until Babe saves him. The dog becomes friends with Babe, and Babe invites him and the other homeless animals into the hotel. They share the food around and then sing "If I Had Words", alerting the duck Ferdinand (who had come after Babe from the farm). After Zootie then gives birth to twins, several people from animal control, break into the hotel and capture all the animals except for Babe, Tug, Ferdinand and a Jack Russell named Flealick. Meanwhile, Esme is released from jail by the judge after explaining her situation. That night, Babe, Tug, Flealick, and Ferdinand sneak into animal control and open their friends cages. Esme returns to the  hotel and reunites with the landlady, who is mourning her uncles death, and tells her that her neighbor Hortense was the one who got all the animals taken away. Esme and the landlady confront Hortense to find out where the animals have been sent, then set off on a two-seater bicycle to find them.

Esme and the landlady track the animals to a charity gala dinner in the hospitals ballroom, where they battle the chefs and waiters with fire extinguishers. In the epilogue, the landlady sells the hotel and gives the money to Esme so she can save the farm. The landlady and all the animals come to stay at the farm, where Arthur has recovered from his injury.

==Cast==
* Magda Szubanski as Esme C. Hoggett
* Mary Stein as Miss. Floom
* James Cromwell as Arthur Hoggett
* Mickey Rooney as Fugly Floom
* Paul Livingston as the Angry Hot Headed Chef
* Julie Godfrey as Hortense

===Voices===
* Elizabeth Daily as Babe.
* Danny Mann as Ferdinand and Tug.
* Roscoe Lee Browne as the Narrator
* Glenne Headly as Zootie
* Steven Wright as Bob
* James Cosmo as Thelonius Doberman who was "BABE" with other people
* Russi Taylor as The Pink Poodle, and a Choir Cat
* Myles Jeffrey as Easy
* Adam Goldberg as Flealick the Jack Russell Terrier
* Eddie Barth as Nigel and Alan
* Bill Capizzi as Snoop
* Miriam Margolyes as Fly
* Hugo Weaving as Rex
* Jim Cummings as a Pelican
* Katie Leigh as a Kitten
* Charles Bartlett as a Cow
* Michael Edward-Stevens as a Horse
* Nathan Kress as Easy, and a Tough Pup
* Al Mancini as a Fish
* Larry Moss as a Fish

==Production== first movie, was approached to reprise her role, but declined because of personal matters  and was replaced by her Rugrats co-star Elizabeth Daily.
 AMC on December 9, 2007, the shot that shows Ferdinand almost getting shot by humans was removed.  
 Oz but is in modern-day form. The city has numerous styles of architecture from around the world. It also has a variety of waterways, noticeable by the hotel at which Babe stays. The downtown area appears to be situated on an island not dissimilar to Manhattan Island. The Downtown Skyline features numerous skyscrapers such as the World Trade Center, the Sears Tower, the Chrysler Building, the Empire State Building,the IDS Center, the MetLife Building, the Sydney Opera House, the Hollywood sign, the Golden Gate Bridge, the Fernsehturm Berlin, Big Ben, Red Square, the Statue of Liberty, the Eiffel Tower, the Christ the Redeemer (statue), and many other landmarks.

The DVD covers feature a similar but different skyline, keeping the World Trade Center, the Golden Gate Bridge, Big Ben, the Sydney Opera House, and Red Square. Several skyscrapers added include 40 Wall Street (Two of them), the Empire State Building, 500 5th Avenue, the Flatiron Building, the World Financial Center, and several Los Angeles skyscrapers including the U.S. Bank Tower (Los Angeles)|U.S. Bank Tower. The river near the hotel is similar to the canals of Venice, Italy, or Amsterdam, Netherlands.

==Reception==
The film received mixed reviews from critics. The movie has a 61% "Fresh" rating from Rotten Tomatoes.  Most of the negative reviews came from people who enjoyed the first Babe, as well as those who were expecting a more family-oriented film, like the first installment. However, the movie has developed a cult following,  and film critic Gene Siskel named it as his choice for the best movie of 1998 and claimed it to be better than its original.   Roger Ebert also praised the movie, giving it a perfect four stars and saying it was "more magical than the original Babe."  The film was also nominated for an Academy Award for Best Original Song in 1998. Tom Waits is apparently a fan of the film, as he expressed in a 2010 feature in Mojo Magazine.  Radio personality/podcaster Jesse Thorn has also praised the film. 

This more recent praise comes despite the film returning a $21 million gross loss, compared to the first movies $224 million gross profit.

==Soundtrack==
The score is again by Nigel Westlake, it also includes sound clips taken from the film. There is also a big band classic "Chattanooga Choo Choo" by Glenn Miller, and "Thats Amore" by Dean Martin. More tracks including "Thatll Do", the Academy Award-nominated theme song, and a song at the end sung by Babes voice actress.

# Main Title / Babe the Brave Little Pig 3:29
# Save the Farm - 1:15
# Airport - 3:59
# Stranded - 3;20
# Apartment Place for Babe - 4;10
# A Pig Gets Wise - 6:38
# Sanctuarys End - 1:45
# Animal Control - 2:39
# Chaos Revisited - 3:16
# Wheres the Animals? - performed by The Mavericks - 2:59
# Wrap-Up - 6:49
# Babes Bathtub Party / End Credits - 7:49

==Home media==
* May 4, 1999 (  and pan and scan formats.)
* May 22, 2001 (DVD - 2-Pack with Babe (film)|Babe)
* September 23, 2003 (DVD - The Complete Adventure Two-Movie Pig Pack, this DVD box set was released in separate widescreen and pan and scan formats, due to the DVD re-release of the first film)	
* November 12, 2004 (DVD - Family Double Feature, this contains Babe) (Note: This DVD shows pan and scan versions of both films and the widescreen version of this film.)

==References==
 

==External links==
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 