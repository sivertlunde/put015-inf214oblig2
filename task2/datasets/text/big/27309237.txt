A.K.47 (film)
{{Infobox film
| name           = A.K.47
| image          = A.K.47 Kannada film DVD cover.jpg
| caption        = Film DVD cover
| director       = Om Prakash Rao
| producer       = Ramu
| story          = S. R. Brothers
| screenplay     = Om Prakash Rao
| starring       = Shivarajkumar Chandini Om Puri 
| music          = Hamsalekha
| lyrics         = Hamsalekha
| cinematography = P. Rajan
| editing        = S. Manohar
| distributor    = Ramu Enterprises
| released       = 17 June 1999
| runtime        = 158 days
| country        = India
| language       = Kannada
| budget         =
}}
 Kannada film Saikumar and Aditya respectively.

==Cast==
* Shivarajkumar
* Chandini
* Om Puri
* Girish Karnad
* Ashish Vidyarthi
* Srividya
* Sadhu Kokila
* Charulatha in special appearance

==Box office==
It successfully completed 175 days in many centers of Karnataka  and became a career highlight for actor Shivarajkumar.This movies credit goes to many people one is Ramu, om prakash rao, of course Shivanna, and last but not least MS Ramesh & HS Rajshekar for their tremendous dialogue.Though it is Kannada movie, but half of the movie containing Hindi dialogues. Om puris Hindi punch dialogues got good appreciation. Ex:- "mera jhootha ka naam bi dawoodh hai" , " maarne keliye naa thu goodse, marnekeliye naa me gaandhi" dialogues impressed non Kannada audience. This is the first full length DTS Kannada movie.

==Soundtrack==
All songs and lyrics are written by Hamsalekha.

{{Infobox album  
| Name        = AK-47
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       =
| Released    =  1999
| Recorded    = Feature film soundtrack
| Length      =
| Label       =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Naanu Kannadada Kanda || K. J. Yesudas ||
|-
| 2 || Kadalo Kadalo || Hariharan (singer)|Hariharan, K. S. Chitra || 
|-
| 3 || Yaari Anjuburuki || Rajesh Krishnan, K. S. Chitra ||
|-
| 4 || Oh My Son || S. P. Balasubramanyam ||  
|- Hariharan ||
|-
|}

==Awards==
* Karnataka State Film Awards
# Best Editing - S. Manohar
# Best Sound Recording - Kodandapani
# Special Award (Novel Techniques)

== External links ==
* http://entertainment.oneindia.in/kannada/movies/a-k-47/cast-crew.html

==References==
 

 
 
 
 
 
 
 
 

 