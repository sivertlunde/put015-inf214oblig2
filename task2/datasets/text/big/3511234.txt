Cat on a Hot Tin Roof (1958 film)
{{Infobox film
| name = Cat on a Hot Tin Roof
| image = Cat_roof.jpg
| image_size =
| alt = poster by Reynold Brown
| director = Richard Brooks
| producer = Lawrence Weingarten
| screenplay = Richard Brooks James Poe
| story = Tennessee Williams  
| starring = Elizabeth Taylor Paul Newman Burl Ives William Daniels
| editing = Ferris Webster
| distributor = Metro-Goldwyn-Mayer Warner Bros. Pictures
| released =  
| runtime = 107 minutes
| country = United States
| language = English
| budget = $2,345,000  . 
| gross = $11,285,000 
}} play of the same name by Tennessee Williams adapted by Richard Brooks and James Poe. One of the top-ten box office hits of 1958, the film stars Elizabeth Taylor, Paul Newman and Burl Ives.

==Plot==
 
Late one night, a drunken Brick Pollitt (Paul Newman) is out trying to recapture his glory days of high school sports by leaping hurdles on a track field, dreaming about his moments as a youthful athlete. Unexpectedly, he falls and breaks his leg, leaving him dependent on a crutch. Brick, along with his wife, Maggie "the Cat" (Elizabeth Taylor), are seen the next day visiting his familys estate in Mississippi, there to celebrate Big Daddys (Burl Ives) 65th birthday.

Depressed, Brick decides to spend his days drinking while resisting the affections of his wife, who taunts him about the inheritance of Big Daddys wealth. Numerous allusions are made to their tempestuous marriage &ndash; there are speculations as to why Maggie does not yet have children while Bricks brother Gooper (Jack Carson) and his wife Mae (Madeleine Sherwood) have a whole clan, many of whom run around the "plantation" (as Big Daddys estate is called) unsupervised and singing obnoxiously.

Big Daddy and Big Mama (Judith Anderson) arrive home from the hospital on their private airplane and are greeted by Gooper and his wife, along with Maggie. Despite the efforts of Mae, Gooper and their kids to draw his attention to them, Big Daddy has eyes only for Maggie. The news is that Big Daddy is not dying from cancer. However, the doctor later meets privately with first Gooper and then Brick where he divulges that it is a deception -- Big Daddy has inoperable cancer, and will likely be dead within a year -- and the truth is kept from him. Maggie wants Brick to take an interest in his father’s wealth as well as health, but Brick stubbornly refuses.

As the party winds down for the night, Big Daddy meets with Brick in his room and reveals that he is fed up with his alcoholic son’s behavior, demanding to know why he is so stubborn.  The revealing moment ensues when Maggie joins them and tells what happened the night Bricks best friend and football teammate Skipper committed suicide. Maggie was jealous of Skipper because he had more of Bricks time. (There is a very subtle hint at this point that Skipper and Brick may have been romantically involved with each other). She decided to ruin their relationship "by any means necessary," intending to seduce Skipper and put the lie to his loyalty to her husband. She ran away without going through with it. Brick had blamed Maggie for Skippers death, but actually blames himself for not helping Skipper when he repeatedly phoned Brick in a hysterical state.

After an argument, Big Daddy learns from Brick that he will die from cancer and that this birthday will be his last. Shaken, he retreats to the basement. Meanwhile, Gooper, whos a lawyer, and his wife argue with Big Mama about the familys cotton business and Big Daddys will. Brick descends into the basement, a labyrinth of antiques and family possessions hidden away. He and Big Daddy confront each other before a large cut-out of Brick in his glory days as an athlete.
 make love, possibly making Maggies "lie" become "truth."

==Cast==
* Elizabeth Taylor as Margaret "Maggie/Maggie the Cat" Pollitt
* Paul Newman as Brick Pollitt
* Burl Ives as Harvey "Big Daddy" Pollitt
* Judith Anderson as Ida "Big Momma" Pollitt
* Jack Carson as Cooper "Gooper" Pollitt
* Madeleine Sherwood as Mae Flynn "Sister Woman" Pollit
* Larry Gates as Dr. Baugh Vaughn Taylor as Deacon Davis

==Production notes== Broadway March 24, 1955, with Ives and Sherwood in the roles they subsequently played in the movie. Ben Gazzara played Brick in the stage production and rejected the film role as did Elvis Presley.  Athlete turned film star Floyd Simmons also tested for the role. 

Lana Turner  and Grace Kelly {{citation
|last1=Parish
|first1=James Robert
|last2=Mank
|first2=Gregory W.
|last3=Stanke
|first3=Don E.
|title=The Hollywood Beauties
|year=1978
|publisher=Arlington House Publishers
|location=New Rochelle, New York
|isbn=0-87000-412-3
|page=326
}}   were both considered for the part of Maggie before the role went to Taylor.
 New York, where he was to be honored the following day by the New York Friars Club.  The plane crashed, and all passengers, including Todd, were killed.  Beset with grief, Taylor remained off the film until April 14, 1958, at which time she returned to the set in a much thinner and weaker condition. 

== Academy Awards ==
 
Tennessee Williams was reportedly unhappy with the screenplay, which removed almost all of the homosexual themes and revised the third act section to include a lengthy scene of reconciliation between Brick and Big Daddy. Paul Newman, the films star, had also stated his disappointment with the adaptation. The Hays Code limited Bricks portrayal of sexual desire for Skipper, and diminished the original plays critique of homophobia and sexism. Williams so disliked the toned-down film adaptation of his play that he told people in the queue, "This movie will set the industry back 50 years. Go home!"  

Despite this, the film was highly acclaimed and though it did not win any Academy Awards, it received several nominations including Best Picture, Best Actor (Newman), Best Actress (Taylor), and Best Director (Brooks).  The film also received nominations for Best Cinematography, Color (William Daniels), and Best Writing, Screenplay Based on Material from Another Medium. Cat may have been too controversial for the Academy voters; the film won no Oscars, and the Best Picture award went to Gigi (1958 film)|Gigi that year. Ives won the Best Supporting Actor Oscar for The Big Country at the same ceremony.

==Box Office==
According to MGM records the film earned $7,660,000 in the US and Canada and $3,625,000 elsewhere resulting in a profit of $2,428,000. 

==See also==
* List of American films of 1958

==References==

 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 