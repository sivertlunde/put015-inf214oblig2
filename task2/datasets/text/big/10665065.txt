Conversation Piece (film)
{{Infobox film
| name           = Gruppo di famiglia in un interno
| image          = Conversationpiecedvd.jpg
| image_size     = 
| caption        = UK DVD cover
| director       = Luchino Visconti
| producer       = Giovanni Bertolucci for Rusconi Film
| writer         = Suso Cecchi dAmico Enrico Medioli Luchino Visconti
| narrator       = 
| starring       = Burt Lancaster Helmut Berger Silvana Mangano Claudia Marsani Stefano Patrizi Elvira Cortese Romolo Valli Claudia Cardinale Dominique Sanda
| music          = 
| cinematography = Pasqualino De Santis
| editing        = Ruggero Mastroianni, Eliane Katz
| distributor    = New Line Cinema in USA
| released       = 10 December 1974 (Italy) 23 June 1977 (U.S.)
| runtime        = 121 Min
| country        = Italy
| language       = Shot in English; in Italy was released dubbed into Italian
| budget         = 
}} director Luchino Visconti.

The film features an international cast including the American actor Burt Lancaster, the Austrian Helmut Berger and the Italians Silvana Mangano and Claudia Cardinale (in a very short role as the professors wife) and the French actress Dominique Sanda in a cameo as the professors mother. It was shot in English language, however, an Italian dubbed version was also produced at the time, in which Lancasters and Bergers lines are dubbed into Italian by other actors.
 nude and political content and because Francisco Francos daughter and son-in-law are mentioned. However, it was re-released there, uncut, in 1983. The word cunt was removed from its UK original release but restored on the British DVD edition.

==Plot==
Retired American professor ( ), her daughter (Claudia Marsani) and her daughters boyfriend (Stefano Patrizi) and is forced to rent to them an apartment on an upper floor of his home. From this point on, his quiet routine is turned into chaos by his tenants machinations, and everybodys life takes an unexpected but inevitable turn.

==Cast==
*Burt Lancaster (The Professor)
*Helmut Berger (Konrad Huebel)
*Silvana Mangano (Marquise Bianca Brumonti)
*Claudia Marsani (Lietta Brumonti)
*Stefano Patrizi (Stefano)
*Elvira Cortese (Erminia)
*Philippe Hersent (Porter)
*Guy Tréjan (Antiquario)
*Jean-Pierre Zola (Blanchard)
*Umberto Raho
*Enzo Fiermonte (Police chief)
*Romolo Valli (Michelli)
*Dominique Sanda (Professors mother  )
*Claudia Cardinale (Professors wife  )

==Cultural references==
* Visconti was inspired in creating the main character of the professor by the Italian literary critic Mario Praz. In an interview Praz recalls how the situation described in the movie (a group of young and loud tenants moving into the old palazzo where he lived, disrupting his peace) happened for real a few months after the movie was released. Arthur Devis is mentioned. Sinfonia Concertante, K. 364, both by Wolfgang Amadeus Mozart are heard. May 1968.
* Lietta (Claudia Marsani) recites a poem that is attributed to W. H. Auden, "Theres no sex life in the grave".
* The film is a critique of the world of the jet set following Federico Fellinis La Dolce Vita. 

==Awards==
*Italian National Syndicate of Film Journalists Nastro dArgento
**Winner: Best Director (Regista del Miglior Film Italiano) – Luchino Visconti
**Winner: Best Cinematography (Migliore Fotografia) – Pasqualino De Santis
**Winner: Best New Actress (Migliore Attrice Esordiente) – Claudia Marsani
**Winner: Best Producer (Migliore Produttore Italiano)
**Winner: Best Production Design (Migliore Scenografia) – Mario Garbuglia
*David di Donatello
**Winner: Best Film
**Winner: Best Foreign Actor (Migliore Attore Straniero) – Burt Lancaster Japan Academy Prize
**Winner: Best Foreign Film
*Seminci, Valladolid Film Festival, Spain
**Winner: Best Film

==External links==
*  
* http://www.dvdbeaver.com/film/DVDReviews21/conversation_piece_dvd_review.htm
* French site about Visconti: http://emmanuel.denis.free.fr/visconti.html
*   by Pauline Kael

 

 
 
 
 
 
 
 
 
 
 
 