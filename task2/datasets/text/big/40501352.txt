Fury of the Congo
{{Infobox film
| name           = Fury of the Congo
| image          = Fury of the Congo poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = William Berke
| producer       = Sam Katzman
| writer         = 
| screenplay     = Carroll Young
| story          = 
| based on       =   
| narrator       = 
| starring       = Johnny Weissmuller
| music          = 
| cinematography = Ira Morgan
| editing        = Richard Fantl
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = February 1951
| runtime        = 69 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Fury of the Congo (1951) is the sixth Jungle Jim film produced by Columbia Pictures. It features Johnny Weissmuller in his sixth performance as the protagonist adventurer Jungle Jim. The film was directed by William Berke and written by Carroll Young.

The film centres on Jungle Jim venturing into tribal land with pilot Ronald Cameron in search of Professor Dunham, who in turn is tracking down a rumoured creature known as the Okongo. Along the journey, Jim faces other obstacles, both man and wild. The film was theatrically released in the United States in February 1951.

==Plot== Congo when he notices a plane diving towards the river. The agile explorer rescues the injured pilot, Ronald Cameron (Joel Friedkin), from the deep waters. Cameron tells Jim that he is trying to find missing biochemistry professor Dunham, under the University of Cairos request. Dunham was last seen venturing into the jungles in search of a beast known as the Okongo. The Okongo, half-antelope and half-zebra, is greatly revered by the tribal natives of Congo and its glands are rumoured to contain a rare type of drug. Jungle Jim and Cameron later discover from a tribal chief, Leta (Sherry Moreland), that Dunham has been kidnapped by hunters who wish to extract the drug from the Okongos glands. Jim, Leta, and Cameron make their way to the hunters hideout. Halting their sinister plans, Leta lets loose the captured Okongo. It proceeds to kill one of the hunters. A fight ensues and during the scuffle, Professor Dunham smashes all the bottles of extracted Okongo drug.  

The trio of Jungle Jim, Leta, and Cameron, flee. They encounter a sandstorm and Jim engages in a battle with a gigantic desert spider, before returning to save Dunhams life.  The professor, having been shot by one of the hunters, is left in Cameron and Letas care. Dunham shockingly recognises Cameron as the leader of the notorious hunters. Too late, they all get captured by Cameron and his henchmen. Jim is commanded to bring the hunters to the main herd of Okongos. Just as they arrive, however, the hunters are attacked by both the natives and the Okongos. Cameron manages to escape but falls from a cliff and dies. Leta and the natives savour their victory, and Jungle Jim and Dunham make their leave. 

==Production==
While still a work-in-progress, the film was referred to as Jungle Menace. Johnny Weissmuller was returned as Jungle Jim for the sixth time.  The film featured Tamba, a chimpanzee actor, as Jungle Jims pet. The film was directed by William Berke with assistance from Wilbur McGaugh. Sam Katzman was in charge of production for Columbia Pictures, while Carroll Young wrote the screenplay. Ira Morgan signed on as cinematographer. The set decorator was Sidney Clifford. Mischa Bakaleinikoff headed the musical direction, and Richard Fantl edited the film.  Filming took place in late-June 1950. Filming locations included Vasquez Rocks and Corriganville. 

==Release and reception==
The film was officially released in North American cinemas in February 1951.  The Hollywood Reporter wrote in its commentary of the film that "Fury of the Congo packs enough excitement and color to please the juvenile and action fans." Variety (magazine)|Variety commented that it was "mediocre filler fare at best", while the Motion Picture Herald described it as "largely juvenile in appeal".  

==See also==
* List of Columbia Pictures films
* List of film series with more than ten entries

==References==
 

==Bibliography==
*  }}
*  }}

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 