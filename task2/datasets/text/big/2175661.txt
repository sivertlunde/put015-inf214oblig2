Little Women (1933 film)
{{Infobox film
| name           = Little Women 
| image          = Little Women 1933 poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = George Cukor
| producer       = Merian C. Cooper
| writer         = Louisa May Alcott (novel)
| based on       = Little Women  (1868 novel)
| screenplay     = Victor Heerman Sarah Y. Mason
| starring       = Katharine Hepburn Joan Bennett Paul Lukas Jean Parker Frances Dee
| music          = Max Steiner
| cinematography = Henry W. Gerrard
| editing        = Jack Kitchin
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 117 mins.
| country        = United States
| language       = English
| budget         = $424,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p55 
| gross          = $2,000,000 
}}
 classic novel of the same name by Louisa May Alcott.
 silent versions Little Women Little Women starring Winona Ryder.

==Plot== vignettes focusing on the struggles and adventures of the four March sisters and their mother, affectionately known as Marmee (Spring Byington), while they await the return of their father, who serves as a colonel and a chaplain in the Union Army. Spirited tomboy Jo (Katharine Hepburn), who caters to the whims of their well-to-do Aunt March (Edna May Oliver), dreams of becoming a famous author, and she writes plays for her sisters to perform for the local children. Amy (Joan Bennett) is pretty but selfish, Meg (Frances Dee) works as a seamstress, and sensitive Beth (Jean Parker) practices on her clavichord, an aging instrument sorely in need of tuning.
 John Lodge). During the next several months John courts Meg, Jos first short story becomes published, and Beth often takes advantage of Mr. Laurences offer for her to practice on his piano.
 German Linguistics|linguist. With his help and encouragement Jo improves her writing, and she resolves her confused feelings about Laurie.

Beth, debilitated, nears death, so Jo returns to Concord. After Beth dies, Jo learns that Amy, who accompanied Aunt March to Europe, has fallen in love with Laurie and accepted his proposal. Professor Bhaer arrives from New York City and proposes to Jo, who accepts, then Amy and Laurie eventually marry.

==Cast==
* Katharine Hepburn (26) as Josephine Jo March (15 - )
* Joan Bennett (23) as Amy March (12 - )
* Paul Lukas as Professor Bhaer
* Edna May Oliver as Aunt March
* Jean Parker (18) as Elizabeth Beth March (14 - )
* Frances Dee (23) as Margaret Meg March (16 - )
* Henry Stephenson as Mr. Laurence
* Douglass Montgomery as Theodore Laurie/Teddy Laurence
* John Davis Lodge as Brooke
* Spring Byington as Marmee March
* Samuel S. Hinds as Mr. March
* Nydia Westman as Mamie
* Harry Beresford as Doctor Bangs

Uncredited cast members include Bonita Granville, Olin Howland, and Lily Lodge.

==Production==
  RKO from MGM to supervise the production as the last film left in his contract with the studio. 

At the request of Katharine Hepburn, costume designer Walter Plunkett created a dress for her character copied from one worn by her maternal grandmother in a tintype Hepburn had. Plunkett also had to redesign several of Joan Bennetts costumes to conceal her advancing pregnancy, a condition Bennett intentionally had not mentioned to George Cukor when he cast her in the film. 

Louise Closser Hale originally was scheduled to portray Aunt March, but after her death on July 26, 1933, Edna May Oliver assumed the role.   
 set decoration, and he modeled the interior of the March home after Louisa May Alcotts Massachusetts house. 
 Warner Bros. Ranch in Pasadena, California|Pasadena.

==Release==
The film opened on November 16, 1933 at Radio City Music Hall, where it broke attendance records and earned over $100,000 during its first week of release.  It was the equal fourth most popular movie at the US box office in 1933  and, after cinema circuits deducted their percentage of exhibition boxoffice ticket sales,  made an eventual profit of $800,000. 
 Depression audiences were ripe for the films evocation of life in a simpler, more innocent and auspicious world. In addition, the film business had come under fire in 1932 and 1933 for presenting an abundance of violent and sexually titillating material. This film was just the type that conservative people felt should be produced. They championed it, sent their children to see it, and made it part of school curricula.   

===Home media=== closed captioned and features an English audio track in Dolby Digital 1.0 and subtitles in English, Spanish, French, Portuguese, Georgian, and Chinese.

==Critical reception==
The film was overwhelmingly praised by critics upon its release. Mordaunt Hall of The New York Times observed, "The easy-going fashion in which George Cukor, the director, has set forth the beguiling incidents in pictorial form is so welcome after the stereotyped tales with stuffed shirts. It matters not that this chronicle is without a hero, or even a villain, for the absence of such worthies, usually extravagantly drawn, causes one to be quite contented to dwell for the moment with human hearts of the old-fashioned days. The film begins in a gentle fashion and slips away smoothly without any forced attempt to help the finish to linger in the minds of the audience." 

Variety (magazine)|Variety called it "a superbly human document, sombre in tone, stately and slow in movement, but always elonquent in its interpretations." 

John Mosher of The New Yorker declared it "an amazing triumph," and "a picture more intense, wrought with more feeling, than any other we are likely to see for a long time to come." 

The New York World-Telegram called the film "a stunningly clever job of recapturing on the screen all the simplicity and charm of its author," and wrote that Hepburn gave "an unforgettably brilliant performance and that once and for all she definitely proves how unlimited and effortless an actress she really is."  > 
 New York American wrote, "It is possible that with the passage of months the memory of Katherine Hepburns portrayal of the sensitive, fiery Jo will be dimmed a bit, or somewhat superseded by later displays of histrionic genius. But at the moment, and for days, weeks, months to come, Miss Hepburns characterization will stand alone on a pedestal of flaming brilliance." 

TV Guide rated the film four stars, calling it "unabashedly sentimental" and "an example of Hollywoods best filmmaking." It added, "The sets, costumes, lighting, and direction by George Cukor all contribute greatly to this magnificent film, but the performances, especially Hepburns, are what make the simple story so moving. There are laughs and tears aplenty in this movie, which presents a slice of American history in a way that children will find palatable. Released during the depths of the Great Depression|Depression, Little Women buoyed Americans spirits. It still does."  

===Accolades=== Academy Award for Best Adapted Screenplay. The film was nominated for the Academy Award for Best Picture but lost to Cavalcade (1933 film)|Cavalcade, and George Cukor lost the Academy Award for Best Director to Frank Lloyd for his direction of that film.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 