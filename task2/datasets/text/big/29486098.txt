Troyee
{{Infobox film
 | name = Troyee
 | image = Troyee.jpg
 | caption = DVD Cover
 | director = Gautam Mukherjee
 | producer =
 | writer =
 | dialogue =
 | starring = Mithun Chakraborty Soumitra Bannerjee Debasree Roy Anil Chatterjee Manik Dutt
 | music =
 | lyrics =
 | associate director =
 | art director =
 | choreographer =
 | released = 18 May 1982
 | runtime = 120 min. Bengali
 Rs 65 lakhs
 | preceded_by =
 | followed_by =
 }}
 1982 Bengali Indian feature film directed by Gautam Mukherjee, starring Mithun Chakraborty, Soumitra Bannerjee, Debasree Roy, Anil Chatterjee and Manik Dutt.

==Plot==
Troyee is the story of a simple man (Mithun), his life, love and relationship.

==Cast==
* Mithun Chakraborty
* Debasree Roy
* Soumitra Bannerjee
* Anil Chatterjee
* Manik Dutt
==Soundtrack==
{{infobox album
| Name          = Troyee
| Type            = Soundtrack
| Artist           = R. D. Burman
}}
{{Track listing
| headline         = Songs
| extra_column           = Playback
| all_lyrics          = Swapan Chakravarty
| all_music           = Troyee

| title1          = Ek Tanete Jemon Temon
| extra1          = Kishore Kumar and Swapan Chakravarhy 
| length1           = 4:48

| title2          = Aaro Kachakachi
| extra2         = Asha Bhosle and Kishore Kumar
| length2          = 3:35

| title3          = Ektu Bosho
| extra3         = Asha Bhosle
| length3         = 4:56

| title4           = Jana Ajana Pathey Cholechi
| extra4          = Kishore Kumar, Asha Bhosle and R. D. Burman
| length4           = 4:29

| title5           = Kotha Hoyechilo
| extra5            = Asha Bhosle
| length5            = 4:50

| title6             = Kobe Ke Kothay
| extra6            = Bhupinder Singh
| length6           = 4:23
}}
 

==Notes==
 
==References==
* http://www.imdb.com/title/tt1321843/

==External links==
*  

 
 
 


 