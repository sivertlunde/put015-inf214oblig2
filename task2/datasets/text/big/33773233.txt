Francisca (film)
{{Infobox film
| name           = Francisca
| image          =  
| caption        = 
| director       = Manoel de Oliveira
| producer       = Paulo Branco
| writer         = Manoel de Oliveira Agustina Bessa-Luís (novel)
| starring       = Teresa Menezes Diogo Dória Mário Barroso 
| music          = 
| cinematography = Elso Roque
| editing        = Monique Rutler    
| distributor    = 
| released       =  
| runtime        = 166 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
}}
 Best Foreign Language Film at the 55th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Teresa Menezes as Francisca Fanny Owen
* Diogo Dória as José Augusto
* Mário Barroso as Camilo

==See also==
* List of submissions to the 55th Academy Awards for Best Foreign Language Film
* List of Portuguese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 