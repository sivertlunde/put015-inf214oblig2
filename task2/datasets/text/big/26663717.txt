Perverted Criminal
{{Infobox film
| name = Perverted Criminal
| image = Perverted Criminal.jpg
| image_size = 
| caption = Theatrical poster for Perverted Criminal (1967)
| director = Kōji Seki 
| producer = Minoru Chiba
| writer = Toshio Godai
| narrator = 
| starring = Setsu Shimizu Shūhei Mutō
| music = 
| cinematography = Jirō Ōyama
| editing = 
| studio         = Shin Nihon Eiga Kenkyūsha
| distributor = Nihon Cinema
| released = December 26, 1967
| runtime = 
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1967 Japanese pink film directed by Kōji Seki. It was the first 3-D film produced in Japan,    and, according to Allmovie, the worlds first 3-D sex film.   

==Synopsis==
A madman is on the loose, raping and murdering women and sometimes engaging in necrophilia. After one woman manages to flee the criminal before he has killed her, she goes to the police for help in bringing him to justice. The criminal targets the woman for murder.   

==Cast==
* Setsu Shimizu (the woman) 
* Shūhei Mutō (the rapist)
* Kōhei Tsuzaki (the policeman)
* Keisuke Akitsu
* Mari Nagisa

==Critical appraisal and background==
On a technical level, Perverted Criminal is notable for being Japans first 3-D film.  Allmovie writes that it was also the first 3-D sex film in the world,  however, in the chapter "Sexploitation Films" in the book Incredibly Strange Films, Jim Morton writes that Jack H. Harris, producer of The Blob (1958), made the first 3-D sexploitation film with Paradisio (1962).  Perverted Criminal was shot in the "part-color" format common with pink films of this period, with only a few scenes shot in color.  In the case of Perverted Criminal, rather than sex scenes, it is the murder scenes, coupled with the 3-D effects, which are given the lurid emphasis of color. 

In their Japanese Cinema Encyclopedia: The Sex Films, Thomas and Yuko Mihara Weisser compare Perverted Criminal to later 3-D sex films such as The Stewardesses (1969), and describe Perverted Criminal as "a considerably more violent and sleazy venture than any of the international 3-D erotica which followed".  Allmovie also judges the film a "gruesome affair" and a "sordid sleazefest". 

The star of Perverted Criminal, Setsu Shimizu, had first come to prominence for her leading role in director Shinya Yamamotos Nihon Cinema pink film, Torture by a Woman (1967). Prior to this breakout performance, she had been cast in supporting roles in various pink films for directors such as Giichi Nishihara and Perverted Criminals Kōji Seki. She later starred in Kinya Ogawas 1968 horror film for Ōkura Studio, Ghost Story Of Barabara Phantom.  Director Sekis 1969 film Abnormal Sex Crimes was a remake of Perverted Criminal. Mari Nagisa, who had a role in Perverted Criminal, starred as the victim in this remake, and Kōhei Tsuzaki was the rapist. 

==Bibliography==

===English===
*  
*  
*  
*  
*  

===Japanese===
*  
*  

==Notes==
 

 
 
 
 
 