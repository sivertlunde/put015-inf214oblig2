Gniazdo
Gniazdo Polans from AD 962 until his death in 992.

This film was directed by Jan Rybkowski and written by Aleksander Ścibor-Rylski.

==Cast==
* Wojciech Pszoniak as "Mieszko I"
* Marek Bargiełowski as "Czcibór, Mieszkos brother"
* Wanda Neumann as "Dubrawa"
* Franciszek Pieczka as "Mrokota"
* Bolesław Płotnicki as "Siemomysł, Mieszkos father"
* Tadeusz Białoszczyński as "Gero"
* Janusz Bylczyński as "Odo I, Margrave of the Saxon Ostmark"
* Czesław Wołłejko as "Otto I, Holy Roman Emperor"

==See also==
* list of historical drama films

==External links==
*  

 
 
 
 
 
 
 
 

 
 