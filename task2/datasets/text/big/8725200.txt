Tulasi (film)
{{Infobox film
| name     = Tulasi
| image          = Tulasi Poster.jpg
| imdb_id        = 
| writer         = Paruchuri Brothers  
| story          = Akula Shiva Boyapati Srinu
| screenplay     = Boyapati Srinu Venkatesh Nayantara Shriya
| director       = Boyapati Srinu 
| producer       = Daggubati Suresh Babu|D.Suresh Babu 
| distributor    = Suresh Productions 
| music          = Devi Sri Prasad 
| country        = India 
| cinematography = B.Balamurugan
| editing        = Marthand K. Venkatesh
| released       =  
| preceded_by    = 
| language       = Telugu        
| runtime        = 2:27:13
| budget         = 
}}
 Telugu film produced by Daggubati Suresh Babu|D.Suresh Babu on Suresh Productions banner, directed by Boyapati Srinu. Starring Daggubati Venkatesh|Venkatesh, Nayantara in lead roles and music is scored by Devi Sri Prasad. It was later dubbed into Malayalam with the same title. The film recorded as Super Hit at box-office. The film was also dubbed into Hindi as The Real Man Hero.

==Plot==
Parvataneni Tulasi Ram aka Tulasi (Daggubati Venkatesh|Venkatesh) is a native of Palnadu region. As his region is well known for factional feuds, his father Dasaradha Ramayya (Vijayakumar (actor)|Vijaykumar) keeps him away in Hyderabad and get him educated and makes him an architect. Once he goes to an abroad trip, where he comes across Vasundhara aka Vasu (Nayantara) and loses his heart. He marries her and they continue to lead a happy life. When Vasu turns pregnant, Tulasi takes her to their native place to share the happy moments with the entire family.

He turns violent when a rival faction pushes his father and teaches all of them a big lesson with a lot of bloodshed. Even as he was silent, the factional feuds continue to haunt him and Vasu couldnt digest the violence and deserts him with their son Harsha (Atulith), when her brother (Sivaji (Telugu actor)|Sivaji) dies in the attack by factionists. Later, Tulasi comes to know that Harsha has a blood clot in his brain and it may cause hemorrhage. With the help of Dr Surekha (Ramya Krishnan), he summons specialists from abroad to get him operated. When everything is ready for the surgery, a gangster (Ashish Vidyarthi) takes away the boy, as both of his sons were killed by Tulasi. In the climax, again Tulasi turns violent and kills everyone to save his sons life. Learning that violence would not only take the lives but also saves lives, Vasu reunites with Tulasi and the film ends on a happy note.

==Cast==
{{columns-list|3| Venkatesh as Parvataneni Tulasi Ram aka Tulasi
* Nayantara as Vasundhara aka Vasu Shriya in a Guest appearance
* Ashish Vidyarthi as An Underworld Gangster, the main villain
* Rahul Dev as Basavaraja Vijaykumar as Dasaradha Ramayya Sivaji as Vasus Brother
* Jaya Prakash Reddy as the Rival of Dasaradha Ramayya
* Tanikella Bharani  Ali 
* Ahuti Prasad 
* Paruchuri Venkateswara Rao
* Benerjee
* Raghu Babu
* Ravi Babu
* Subbaraju as the Elder son of Ashish Vidyarth
* Riyaz Khan as the younger son of Ashish Vidyarthi
* Sameer
* Chitram Seenu 
* Uttej 
* Bandla Ganesh 
* Dr.Siva Prasad
* Narsingh Yadav   Ramya Krishna as Dr. Surekha
* Jhansi as Kokapet Aunty 
* Siva Parvathi 
* Sravani 
* Devisri 
* Saraswatamma  
* Master Athulith 
}}

==Crew==
* Director: Boyapati Srinu
* Story: Akula Shiva
* Producer: D. Suresh Babu
* Music: Devi Sri Prasad
* Fights: Kanal Kannan and Ram Lakshman

==Soundtrack==
{{Infobox album
| Name = Tulasi
| Longtype = to Tulasi
| Type = Soundtrack
| Artist = Devi Sri Prasad
| Cover = Tulasi 2007 ACD.jpg
| Released = 22 September 2007
| Recorded = 2007 Feature film soundtrack
| Length = 26:38 Telugu
| Label = Aditya Music
| Producer = Devi Sri Prasad
| Reviews =
| Last album = Shankar Dada Zindabad (2007)
| This album = Tulasi (2007)
| Next album = Jalsa (2008)
}}

Music composed by Devi Sri Prasad. Music released on ADITYA Music Company. Audio of Tulasi was launched at a function arranged in the song set at Rama Naidu studios in the evening of 22 September 2007. Kovelamudi Raghavendra Rao|K. Raghavendra Rao, D. Ramanaidu and KL Narayana attended this function as guests. Kovelamudi Raghavendra Rao|K. Raghavendra Rao released the audio cassette and gave the first unit to D. Ramanaidu. D. Ramanaidu released the audio CD and gave the first unit to Shriya Saran. 
{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 26:38
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Mia Mia Chandrabose
| extra1          = Naveen,Mamta Mohandas
| length1         = 4:32

| title2          = Vennelintha
| lyrics2         = Chandrabose Sunitha
| length2         = 4:17
 
| title3          = Tula Tula Thulasi
| lyrics3         = Chandrabose
| extra3          = Tippu (singer)|Tippu,Priya Hemesh
| length3         = 4:37

| title4          = Hello Boys
| lyrics4         = Chandrabose
| extra4          = Devi Sri Prasad
| length4         = 3:48

| title5          = Nee Kallathoti
| lyrics5         = Bhaskarabhatla Chitra
| length5         = 3:56

| title6          = Ne Chuk Chuk Bandini
| lyrics6         = Sahithi
| extra6          = Devi Sri Prasad,Malgudi Subha
| length6         = 5:10
}}

==Release==
*The film released with 290 prints worldwide,  in 312 screens in India which includes 20 screens in Andhra Pradesh, 21 screens in Karnataka and 7 screens in Tamil Nadu. 
*The film had a 50 day run in 225 centres and was declared a super hit. 

==Boxoffice==
* It has grossed Rs.12 crores in 12 days. 
* TV rights sold for Rs.2 crores. 
* It is the first telugu film to complete 50 days in 225 theatres across Andhra. 

==References==
 

==External links==
* 

 

 

 
 
 
 