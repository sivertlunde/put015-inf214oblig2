Sleepers West
{{Infobox film
| name           = Sleepers West
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Eugene Forde
| producer       = Sol M. Wurtzel
| writer         = Brett Halliday
| based on       =  
| screenplay     = Lou Breslow Stanley Rauh
| narrator       = 
| starring       = Lloyd Nolan Lynn Bari Mary Beth Hughes
| music          = Cyril J. Mockridge
| cinematography = J. Peverell Marley Fred Allen
| studio         = 
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1941 Drama drama film directed by Eugene Forde, starring Lloyd Nolan and Lynn Bari.

This second entry in 20th Century-Foxs Michael Shayne series was a remake of the 1932 Fox romantic drama Sleepers East from the novel Sleepers East (1933) by Frederick Nebel. The film Michael Shayne - Private Detective (1940) was the first in a series of 12 films. Lloyd Nolan starred as Shayne until the series was dropped by Twentieth Century-Fox and picked up by Producers Releasing Corporation|PRC. In the PRC series, Hugh Beaumont played Shayne.  

==Plot summary==
 
Shadowed by persistent girl reporter Kay Bentley (Lynn Bari), private detective Michael Shayne (Lloyd Nolan) tries his best to secretly escort murder-trial witness Helen Carlson (Mary Beth Hughes) by train from Denver to San Francisco.

Helens testimony will free a man falsely accused of murder, which will also effectively destroy the election chances of a machine politician. 

==Cast==
* Lloyd Nolan as Michael Shayne
* Lynn Bari as Kay Bentley
* Mary Beth Hughes as Helen Carlson
* Louis Jean Heydt as Everett Jason
* Edward Brophy as George Trautwein
* Don Costello as Carl Izzard Ben Carter as Leander Jones - Porter
* Don Douglas as Tom Linscott
* Oscar OShea as Engineer McGowan
* Harry Hayden as Conductor Lyons
* Hamilton MacFadden as Conductor Meyers
* Ferike Boros as Farm Lady

==Film notes==
The 1940 film Michael Shayne – Private Detective was the first in a series of 12 films. Lloyd Nolan starred as Shayne until the series was dropped by Twentieth Century Fox and picked up by PRC. At that point, Hugh Beaumont, who later went on to play Beaver Cleavers father Ward on televisions Leave It to Beaver, took over the role.

* Twentieth Century Fox films with Lloyd Nolan
** Michael Shayne – Private Detective (1940)
** Sleepers West (1941) Dressed to Kill (1941)
** Blue, White and Perfect (1942)
** The Man Who Wouldnt Die (1942)
** Just Off Broadway (1942) Time to Kill (1942)

* PRC films with Hugh Beaumont
** Murder Is My Business (1946)
** Larceny in Her Heart (1946)
** Blonde for a Day (1946)
** Three on a Ticket (1946)
** Too Many Winners (1946)

==References==
 	

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 