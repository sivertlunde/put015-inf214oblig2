Japoteurs
{{Infobox Hollywood cartoon|
| cartoon_name = Japoteurs Superman
| image = Japoteurs1.JPG
| caption = Title card from Japoteurs
| director = Seymour Kneitel Bill Turner Carl Meyer Nicholas Tafuri
| voice_actor = Bud Collyer   Joan Alexander   Julian Noa   Jack Mercer
| musician = Sammy Timberg
| producer = Sam Buchwald
| studio = Famous Studios
| distributor = Paramount Pictures
| release_date = September 18, 1942 (USA)
| color_process = Technicolor
| runtime = 9 min. (one reel)
| preceded_by = Terror on the Midway (1942)
| followed_by = Showdown (1942 film)|Showdown (1942)
| movie_language = English
}}
Japoteurs (1942) is the tenth of seventeen animated Technicolor short films based upon the DC Comics character of Superman, originally created by Jerry Siegel and Joe Shuster. The first Superman cartoon produced by Famous Studios (the successor to Fleischer Studios), Japoteurs covers Supermans adventures stopping Japanese spies from hijacking a bomber plane and bringing it to Tokyo. This cartoon does not bear the Famous Studios name because that company had not yet been fully organized after Max Fleischer was removed by Paramount Pictures from the studio which bore his name. The cartoon was originally released to theaters by Paramount Pictures on September 18, 1942.   

The word "Japoteur" is a portmanteau of the ethnic slur "Jap" and the word "saboteur".

==Plot==
 Japanese flag. bombing plane for the Planet. When everyone is told to get off, Lois stays behind and hides in a locker on board the plane.

As the plane takes off, we see that there are other stowaways aboard. Hidden inside what look like bombs are the Japanese men. The spies tie and gag the pilots and hijack the plane. Meanwhile, Lois emerges from her hiding place and moves over to the cockpit. As shes about to open the door, she notices that the Japanese spies have hijacked the plane. She sneaks into the room and calls for help on the radio, however the spies seize her. In response to her calls for help, fighter planes are sent to stop the hijackers. In response, the hijackers deploy a bomb, which stops the fighters from taking off. Clark Kent goes into an elevator and changes into Superman as it goes up to the roof.

Superman enters the plane to stop the hijackers, but one of them has Lois tied up and is ready to drop her out of the plane through the bomb hatch. Superman jumps out of the plane and comes back in through the bomb hatch to save Lois as shes being dropped. He unties her and starts fighting the hijackers. One of them breaks the planes controls, and the plane starts falling towards the city. Superman takes Lois out of the plane and places her on the ground, then flies back up and catches the plane, bringing it to a safe landing right in the middle of the street.

==Influences==
The cartoon, and the stereotypical Japanese characters in particular, are done in a style typical of American propaganda during World War II.  Many cartoons of the time followed a similar vein, such as the Warner Bros. Cartoons|Schlesinger/Warner Bros. cartoons Scrap Happy Daffy (1943), Daffy - The Commando (1943), and Herr Meets Hare (1945), to name a few.  Such films typically show Japanese and German characters in a negative light as the American hero makes short work of them.
 former Yankee Stadium) by pressing his weight into the nose and carefully lowering it onto the field.

==Voice actors==
*Bud Collyer as Superman/Clark Kent, Japanese Hijacker
*Joan Alexander as Lois Lane
*Jackson Beck as the Narrator
*Jack Mercer as the Press Tour Guide

==See also==
*List of World War II short films

==References==
 

==External links==
*  
*  
*   on YouTube
*  

 

 
 
 
 
 
 
 