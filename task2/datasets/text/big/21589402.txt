Jai Veeru
{{Infobox film
| name           = Jai Veeru
| image          = jaiveeru2.jpg
| alt            =  
| caption        = DVD cover
| director       = Puneet Sira
| producer       = Narendra Bajaj Shyam Bajaj
| screenplay     = Vekeana Dhillon
| starring       = Fardeen Khan Kunal Khemu Dia Mirza Anjana Sukhani
| music          = Bappi Lahiri
| cinematography = K Raj Kumar
| editing        = Kuldeep Mehan
| studio         = 
| distributor    = Siddhi Vinayak Movies
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Hindi
| budget         = INR 35 crores
| gross          = 
}}
 Hindi Action action film Arbaaz Khan and Anjana Sukhani. The film was released on 13 March 2009. It is a remake of the 1996 action, Bulletproof (1996 film)|Bulletproof. The title of the film is named after the famous characters, Jai and Veeru from the film Sholay.

==Plot==
 Arbaaz Khan). He is unaware that his best friend Jai (Fardeen Khan) is an undercover cop, seeking evidence against Tejpal. During a raid on Tejpals warehouse, Veeru accidentally shoots Jai in the head but he miraculously survives and makes a full recovery. Veeru then flees the state, and is subsequently arrested. Jai is assigned the task of returning Veeru to testify against Tejpal. They confront each other, and seem to find their friendship still exists. They both decide to be friends again, when Veeru claims he has Tejpals diary, which includes all his secrets and whereabouts. When they go to catch him, Tejpal shoots Veeru, while Jai attacks Tejpal and kills him. By the time the police get there, Jai has already let Veeru flee, and Jai is arrested for what he has done. Since then, lives on the friendship of "Jai Veeru".

==Cast==
{| class="wikitable" width="40%"
|-
! Actor / Actress || Role
|- Fardeen Khan || Jai
|- Kunal Khemu || Veeru
|- Diya Mirza || Anna
|- Arbaaz Khan Arbaaz Khan || Tejpal
|- Anjana Sukhani || Divya
|- Govind Namdeo || Chief Insp. Ranbir Singh
|}

==Soundtrack==
{{Infobox Album  
| Name        = Jai Veeru
| Type        = Soundtrack
| Artist      = Bappi Lahiri
| Released    =  February 2009
| Cover    = Jai Veeru.jpg Feature film soundtrack
| Length      = 30:43
| Label       = T-Series Reviews =
}}

The soundtrack of the film is composed by Bappa Lahiri while the lyrics are penned by Shravan Kumar.

===Tracklist===
{| border="4" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #FFFFFF; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#B0C4DE" align="center"
! Track !! Song !! Artist(s)
|-
| 1
| Sufi
| Tulsi Kumar,  Saim Bhatt
|-
| 2
| Tennu Le
| Omer Inayat
|-
| 3
| Dhun Lagi
| Mika Singh , Raja Hasan
|-
| 4
| Aisa Lashkara
| Hard Kaur , Rema Lahiri
|-
| 5
| Agre Ka Ghagra
| Mouli Dave, Javed Ali & Raja Hasan
|-
| 6
| Dhun Lagi (Progressive Dance Mix)
| Mika Singh & Raja Hasan
|-
| 7
| Sufi (Rock)
| Tulsi Kumar & Saim Bhatt
|-
| 8
|  Dhun Lagi (Electro Mix)
| Mika Singh & Raja Hasan
|-
| 9
|  Sufi
| Saim Bhatt
|-
| 10
| Tennu Le
| Omer Inayat
|}

==Box office==

It did a business of Rs 5 Crores in all over circuits. It grossed around $2.2 million worldwide. Box Office India declared it as flop.

==External links==
*  
*  

 
 
 
 
 


 