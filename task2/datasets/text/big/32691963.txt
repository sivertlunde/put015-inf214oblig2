Firehouse (1987 film)
{{Infobox film
| name           = Firehouse
| image          = Firehouse 1987.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = J. Christian Ingvordsen
| producer       = C. Steven Duncker J. Christian Ingvordsen Steven Kaman
| writer         = J. Christian Ingvordsen Steven Kaman Rick Marx
| story          = 
| narrator       = 
| starring       = Gianna Ranaudo Gideon Fountain Peter Mackenzie
| music          = Michael Montes
| cinematography = Steven Kaman
| editing        = Steven Kaman
| studio         = Cinema Sciences Corporation
| distributor    =
| released       =
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Firehouse is a 1987 film directed and co-written by J. Christian Ingvordsen. The movie is notable as the film debut of (a then-unknown) Julia Roberts in an uncredited role. 

==Plot== secretly setting fires to some of the old buildings of a dilapidated neighborhood, while at the same time three women become the new recruits at a local fire station. Life is not smooth at the new firehouse, as the women are teased and harassed by their male coworkers. But they learn to stand up for themselves against the torment while also solving the mystery of who is starting the fires.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Gianna Ranaudo (as Gianna Rains)  || Barrett Hopkins
|-
| Martha Peterson  || Shannon Murphy 
|-
| Renee Raiford  || Violet Brown
|-
| Peter Onorati  || Ron J. Sleek 
|-
| Gideon Fountain || John Anderson 
|-
| Peter Mackenzie || Dickson Willoughby 
|- Craig Mitchell || Bummy
|- 
| Julia Roberts (uncredited) || Babs
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 