The Feminine Touch (1956 film)
{{Infobox Film
| name           = The Feminine Touch (1956 Film)
| image          = File:The Feminine Touch (1956) Movie Poster.jpg
| image_size     = thumb
| caption        = 
| director       = Pat Jackson
| producer       = Michael Balcon, Jack Rix
| writer         = Iain MacCormick  Sheila Mackay Russell, novel "A Lamp Is Heavy"
| narrator       =  George Baker,  Belinda Lee Delphi Lawrence
| music          = Clifton Parker
| cinematography = Paul Beeson
| editing        = Peter Bezencenet
| studio         = Rank Organization, Ealing Studios
| distributor    = Rank Organization
| released       = 1956
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} George Baker, Belinda Lee and Delphi Lawrence; also known as "The Gentle Touch" and "A Lamp Is Heavy". 

It is based on the bestselling novel A Lamp Is Heavy by Sheila Mackay Russell. 

It follows a group of student nurses as they enter the National Health Service.

==Cast== George Baker - Dr Jim Alcott
* Belinda Lee - Susan Richards
* Delphi Lawrence - Pat Martin
* Adrienne Corri - Maureen OBrien
* Henryetta Edwards - Ann Bowland
* Barbara Archer - Liz Jenkins
* Diana Wynyard - Matron
* Joan Haythorne - Home Sister
* Beatrice Varley - Sister Snow
* Constance Fraser - Assistant Matron
* Vivienne Drummond - Second-Year Nurse
* Christopher Rhodes - Dr Ted Russell
* Richard Leech - Casualty doctor
* Newton Blick - Porter
* Dandy Nichols - Skivvy Mark Daly - Gardener
* Mandy Miller - Jessie
* Dorothy Alison - The suicide
* Joss Ambler - Mr. Bateman

 
==References==
 

==External links==
* 


 

 
 
 
 
 
 


 