Sunset Strip (film)
{{Infobox film
| name          = Sunset Strip
| image         = Sunset Strip.jpg
| caption       = Theatrical release poster
| alt           = Poster showing six different photos.
| director      = Adam Collis
| producer      = Art Linson John Linson
| screenplay    = Randall Jahnson Russell DeGrazier
| story         = Randall Jahnson
| starring      = 
| music         = Stewart Copeland
| cinematography = Ron Fortunato
| editing       = Bruce Cannon Angus Wall
| studio        = 20th Century Fox
| distributor   = 20th Century Fox
| released      =  
| runtime       = 95 minutes
| country       = United States
| language      = English
}} The Doors and Dudes (film)|Dudes, and he and Russell DeGrazier adapted the story into a screenplay. 
 Tommy Flanagan also feature.  The film began shooting on November 9, 1998, and ended on January 11, 1999. 

==Plot==
Sunset Strip tells the story of a number of music industry artists, all in the span of 24 hours on the Sunset Strip in Hollywood. Michael secretly pines for Tammy. She is busy sleeping with the up-and-coming country rocker Glen Walker and the rock star Duncan. Zach and his band are opening at the Whisky a Go Go for Duncan Reed and the Curb. In these 24 hours, they all cross paths pursue their dreams.   

==Cast== Tommy Flanagan, a rock star who is influenced by Jim Morrison and David Bowie.     Adam Goldberg is Marty Shapiro, a fast-talking record producer from the Valley.  Nick Stahl is Zach, a novice guitarist who believes that he is the Jimi Hendrix successor.   Rory Cochrane is Felix, a troubled songwriter whose dream is die of a drug and alcohol overdose.  Simon Baker is Michael Scott, who photographs Tammy Franklin and the musicians.        
Other cast are:
* Darren E. Burrows as Bobby John Randolph as Mr. Niederhaus
* Stephanie Romanov as Christine
* Mary Lynn Rajskub as Eileen
* Maurice Chasse as Nigel
* Mike Rad as Badger
* Josh Richman as Barry Bernstein

==Music==
Stewart Copeland was approached by director Adam Collis to assemble the score for the film.  Copeland recorded a slew of vintage songs.  The music, some scored by Stewart Copeland, some written and selected by Robbie Robertson, is made in a bygone style that sometimes consciously mimics the multicharacter 1970s dramas. 

==Release and reception==
On August 18, 2000, Sunset Strip opened to the public in limited release in a single theater in Los Angeles and New York City,    and grossed $3,926 during the opening weekend.  After two months, on October 12, 2000, the film was screened at the Austin Film Festival.  Writing in Variety (magazine)|Variety, Robert Koehler said "Interesting structure provides pic with plenty of opportunities for social satire, human comedy and chance encounters, but few setups are ever dramatically fulfilled."  Kevin Thomas in Los Angeles Times said "Moves smoothly amid a near-perfect period evocation, captured in an array of shifting moods."  Writing in Movies.com|Mr. Showbiz, Kevin Maynard praised the film, saying that it "has its funky charms."  Cheryl DeWolfe of the Apollo Movie Guide said "This modestly successful drama follows a young ensemble cast through the ups and downs of the music business in all its stages of stardom." 

Sunset Strip was released on VHS on February 13, 2001,  and was re-released on September 4, 2001.  On June 1, 2004, 20th Century Fox Home Entertainment released a DVD for region 1.  The DVD release includes a wide-screen and a full-screen version of the film. 

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 