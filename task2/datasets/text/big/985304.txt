Deadly Blessing
{{Infobox film
| name           = Deadly Blessing
| image          = Deadlyblessing.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Wes Craven
| producer       = William Gilmore Patricia Herskovic Max A. Keller Micheline H. Keller
| screenplay     = Glenn M. Benest Matthew Barr Wes Craven
| story          = Glenn M. Benest Matthew Barr
| starring       = Maren Jensen Sharon Stone Susan Buckner Jeff East
| music          = James Horner
| cinematography = Robert Jessup
| editing        = Robert Bracken
| studio         = Polygram Pictures Inter Planetary
| distributor    = United Artists
| released       = August 14, 1981
| runtime        = 100 minutes
| country        = United States
| language       = English
| gross          = $8,279,042  (USA) 
}}
 horror film AllMovie comments that the film "finds director Wes Craven in a transitional phase between his hard-hitting early work and his later commercial successes."   

== Synopsis ==

Martha (Maren Jensen) and Jim Schmidt (Douglas Barr) live on an isolated farm named Our Blessing, where most of its population are "Hittites", an austere religious community who according to one of the characters "make the Amish look like swingers". Jim was a Hittite, but left the community when he got married. Jim tells a neighbor, Louisa Stohler (Lois Nettleton), who is the mother of Faith (Lisa Hartman) that his wife, Martha is pregnant and that Louisas services as a mid-wife will soon be needed by them. Louisa and Faith are not part of the Hittite community, either. In fact, they do not like them due in part to the constant harassment of Faith by William, who chases her and calls her, and all "outsides", Incubus. That night, Jim searches in the barn after hearing strange noises from inside, but is murdered when a mysterious figure runs him over with his tractor. This is seen as a mechanical accident.

Friends Lana Marcus (Sharon Stone) and Vicky Anderson (Susan Buckner) visit Martha after Jims funeral. When William Gluntz (Michael Berryman) goes to the house at night to search for his shoe he accidentally left earlier when sneaking around, he is stabbed through the back by an unseen figure.The following day, Williams father and Jims father and the leader of the Hittites, Isaiah Schmidt (Ernest Borgnine) come to the farm looking for William after he does not return home after being sent by his father to retrieve a "lost" shoe. Martha tells the men she has no idea where William is and they start to leave, Isaiah goes back to the door and offers to buy back the farm from Martha but Martha refuses, after Isaiah insults her and calls her the incubus, she asks him if he would like his answer immediately, and "answers" by slamming the door in his face.

Martha is now being accused of being the incubus. Lana enters the barn the next day to look for something in the barn, inside a toolbox for the tractor but all the doors and windows suddenly close, trapping her inside. In a panic, she searches for a way out but encounters a figure dressed in black. When escaping out the now open barn door, Williams corpse swoops down at her, hanging from a rope. The police clean up the mess as the sheriff (Kevin Cooney) advises the three friends to leave town, as someone may be after them. However, Martha decides to stay where she is and buys a gun for protection. Multiple events follow, such as a snake being put into Marthas bathroom while shes taking a bath by an unseen figure who creeps in her house. She manages to get out of the bath tub and kill the snake with a fireplace poker.

John Schmidt (Jeff East), Marthas brother-in-law is unwillingly engaged to Melissa (Colleen Riley) his cousin. However John feels attracted to Vicky. John is eventually sent away from home and the community when he retaliates against his father who begins hitting him. John meets Vicky outside the cinema and she lets John drive her car, giving him a sense of freedom. They stop at the side of a road and begin to make out but they are attacked by an unseen figure who stabs John multiple times and sets fire to the car, which eventually blows up with Vicky still inside.

Lana has a nightmare in which a pair of hands take hold of her head, forcing her to open her mouth as a spider falls in. When she wakes up she finds blood in a milk carton as Martha finds a scarecrow tied in her room with a flower that was buried with Tom. When Martha hurries to Toms grave she finds him dug up. Martha also discovers it was Louisa and Faith who committed the murders as they attack Melissa. Martha is chased back to her home where she engages in a quick battle with Faith. During the struggle, Faiths shirt is ripped open, revealing her to be a man who has been in love with Martha. Lana and Martha have to fight Louisa and Faith. When Martha shoots Faith she is confronted by Louisa with a shotgun. Fortunately, she too is shot by Lana. However, Faith has survived her gunshot and tries to kill Martha once more, but she is killed when Melissa stabs her in the back. Isaiah turns up and tells them that the messenger of incubus is now dead. The day after, Lana leaves Martha to go back to LA. When Martha enters her home a ghost of Jim warns her about the incubus. The film ends immediately after the real incubus bursts through the floor and pulls Martha back into the floor.

== Cast ==

* Maren Jensen as Martha Schmidt
* Sharon Stone as Lana Marcus
* Susan Buckner as Vicky Anderson
* Jeff East as John Schmidt
* Colleen Riley as Melissa
* Douglas Barr as Jim Schmidt
* Lisa Hartman as Faith Stohler
* Lois Nettleton as Louisa Stohler
* Ernest Borgnine as Isaiah Schmidt
* Michael Berryman as William Gluntz
* Kevin Cooney as Sheriff

== Production == on location The Hills Have Eyes playing a similar character.

Universal, the primary distributor for Polygram-produced films at the time, chose not to pick up the finished project until January 22, 2013; it was instead released in theatres by United Artists and was the last United Artists films released in the Transamerica era before being acquired by MGM in the same year.

== Reception ==
 Razzie award for "Worst Supporting Actor" for this movie.
 Time Out wrote, "Deadly Blessing isnt a very good movie, but it holds out distinct promise that Craven will soon be in the front rank of horror filmmakers", calling it "an excellent example of a mundane project elevated into quite a palatable genre movie by its director." 

In his review of the Shout! Factory Blu-ray Scott MacDonald of EuroCultAV.com wrote, "If Deadly Blessing does one thing very well it is to mix the psychological with the supernatural in horror to create something that is quite an interesting addition to Cravens repertoire.  It isnt the missing masterpiece I was hoping from the film, but it is a certainly unique horror experience, and on that alone I can recommend the film."   

==Home Media==
On January 22, 2013, Shout! Factory released a Collectors Edition of the film on Blu-ray and DVD under license from Polygrams successor-in-interest, Universal Pictures. 

== References ==

 

== External links ==

*  

 

 
 
 
 
 
 
 
 
 
 
 