Soldier (1998 Indian film)
 
 
 

{{Infobox film
| name = Soldier
| image = Soldier_screenshot.jpg
| director = Abbas-Mustan
| producer = Kumar Taurani Ramesh Taurani
| story = Shyam Goel
| screenplay = Sachin Bhowmick Shyam Goel
| starring = Raakhee Bobby Deol Preity Zinta
| music = Anu Malik
| cinematography = Thomas Xavier
| editing = Hussain Burmawala
| distributor = Tips Music Films
| released = 20 November 1998
| runtime =
| country = India Hindi
| awards =
| budget =
| gross =   original   adjusted 
}}
 1998 Bollywood Vijay and Nayantara in lead roles in 2009.

==Plot==
 
Captain Vijay Malhotra (Pankaj Dheer) attempts to defend himself when corrupt army officials, Pratap Singh (Suresh Oberoi), Virender Sinha (Dalip Tahil), and Jaswant Dalal (Salim Ghouse), intercept a truck-load of arms and ammunition, with the help of Virenders brother, Baldev (Sharat Saxena) which they are about to sell illicitly to gangsters. Vijay Malhotra stops the trio of smugglers, but subsequently is shot to death by a mysterious man by the name of DK, who is the ringleader of the smugglers.
 ACP Dinesh Kapoor (Ashish Vidyarthi), an old friend of the slain Vijay Malhotra. Dinesh Kapoor sends a copy of the fax to a mysterious man named Vicky (Bobby Deol) and tells him to "prevent" Jaswant from being nabbed by the police. Vicky helps Jaswant escape from the police at the docks, but when Jaswant tries to kill him to erase any witnesses, Vicky shoots him instead. Vicky then escapes to Australia despite the police preventing it.

In Sydney, Australia, Vicky meets Preeti (Preity Zinta), Pratap Singhs daughter, and woos her while getting into trouble with Jojo, Baldev Sinhas son and beats him up. Vicky and Preeti both fall in love. Pratap Singh and Virender Sinha (and his brother) are high-profile criminals in Australia. Once friends and partners in crime, they share a shaky relationship with one another. Vicky meets up with Pratap, who identifies him as Jaswants killer. Vicky promises Pratap Singh that he would make him more powerful than he would ever imagine.

Jojo destroys Vickys apartment for his being beaten up. Vicky storms Baldevs household, kills many of his men, and makes him cough up the money for the damage Jojo did to his apartment. Baldev reports this to his brother who orders Vicky killed. Baldev and Jojo take a sniper to kill Vicky who is receiving his mother at the airport. They discover his mother is none other than Virenders estranged wife, Shanti (Farida Jalal). Convinced that Vicky is none other than Virenders son, the assassination attempt is called off. Vicky is then welcomed into Virenders gang. At a party thrown in Vickys honour, Pratap is stunned to learn Vicky among his rivals ranks.

Virender takes Vicky to a place where he keeps Prataps, Jaswants, and DKs secrets in files. Vicky shoots Virender dead and dumps him into the bay where a shark eats him. He then dumps Prataps signature cigarette lighter at the crime scene. Vicky then goes through the stolen files and discovers who DK actually is. The police retrieve the lighter from Virenders crime scene and hand it over to Vicky who urges his brother, Baldev to rally his men and launch an attack on Pratap. The squad is gunned down at a checkpost by Prataps men, save for Vicky, who was working with Pratap to get rid of Virenders gang. Vicky then reveals to Pratap that he is not Virenders son, and that he had fooled Shanti into believing he was her son, since she had lost her own. He also reveals that he was sent by DK to kill Jaswant, Virender, and Pratap, and that they should leave for India to kill DK to make Pratap the undisputed crime king.

Meanwhile Preeti overhears Vickys conversation with her father and reveals it to Shanti. Shanti calmly acknowledges that she knows Vicky is not her son and that she helped him kill her husband, whose weapons had helped kill her own son, unbeknownst to him. She reveals that Vicky is Vijay Malhotras son, Raju and that it was Vijay Malhotra who was convicted of arms smuggling based on testimonies from Jaswant, Pratap, and Virender. His dead body was stripped of all medals and his wife, Geeta (Raakhee) and son were ostracised by his community. The community even prevented his body from being cremated and leave it in a raging desert storm, where it is lost in the sands. Preeti, upon hearing this story, agrees to assist Raju/Vicky. Raju then calls ACP Dinesh Kapoor and reveals his plan of bringing Pratap to India and his knowledge of DKs true identity. The ACP then sets Jaswant, who is revealed to be alive and have been helping provide Raju the information to finish off Virenders gang, free.

Raju arrives in India with Pratap and takes him to his village where his mother sits desolately in a temple at the village outskirts. He reveals to Pratap who he really is and proceeds to beat him. DK comes to rescue Pratap, and he is revealed to be none other than Jaswant. Soon Jaswant/DKs men arrive and in the raging battle, Raju kills the men and maims Pratap and DK, makes them confess the truth, and then leaves them for the vultures to feed upon.

His name cleared, Vijay Malhotra is given an honourable military burial, and his son and widowed wife watch.

==Cast==
 
* Bobby Deol as Vicky/Raju
* Preity Zinta as Preeti
* Dalip Tahil as Virender
* Suresh Oberoi as Pratap
* Raakhee as Vijays wife
* Farida Jalal as Shanti Sinha
* Johny Lever as Mohan/Sohan
* Sharat Saxena as Virenders brother
* Ashish Vidyarthi as Dinesh Kapoor
* Salim Ghouse as Jaswant
* Jeetu Verma
* Kulbhushan Kharbanda
* Pankaj Dheer as Vijay Malhotra
* Priyanka
* Amrit Patel
* Narendra Bedi
* Brijesh Tiwari
* Trilok Kapoor
* Viveck Vaswani as a College Principal
 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Soldier Soldier"
| Kumar Sanu & Alka Yagnik 
| 06:13
|-
| 2
| "Tera Rang Balle Balle"
| Sonu Nigam & Jaspinder Narula
| 04:50
|-
| 3
| "Mehfil Mein Baar Baar"
| Kumar Sanu & Alka Yagnik
| 05:40
|-
| 4
| "Mere Dil Jigar Se"
| Kumar Sanu & Alka Yagnik
| 05:32
|-
| 5
| "Mere Khwabon Mein Jo Aaye"
| Alka Yagnik
| 04:23
|-
| 6
| "Meri Saanson Mein Samaye"
| Sonu Nigam
| 04:23
|-
| 7
| "Hum To Dil Chahe"
| Kumar Sanu & Hema Sardesai
| 05:18
|-
| 8
| "Theme of Soldier (Instrumental)"
|
| 04:47
|}

The soundtrack was composed by Anu Malik and was a hit amongst the audience. It was a key point in the success of the film. The songs "Tera Rang Bale Bale", "Soldier Soldier", "Mehfil Mein", "Mere Dil Jigar Sei" and " Mere Khwabon Mein Jo Aye" were very popular and the album was the third best selling album of 1998 after Kuch Kuch Hota Hai and Dil Se. Lyrics were penned by Sameer.

==Awards==
* Filmfare Best Female Debut Award - Preity Zinta
* Filmfare Best Action Award - Tinnu Verma and Akbar Bakshi

==References==
 

==External links==
*  

 

 
 
 
 