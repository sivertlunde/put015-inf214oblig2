A Jewish Girl in Shanghai
{{Infobox film
| name           = A Jewish Girl in Shanghai
| image          = A jewish girl in shanghai - poster.png
| caption        = Theatrical release poster
| alt            = 
| director       = Wang Genfa Zhang Zhenhui
| producer       = Wang Tianyun Ren Zhonglun Cai Hongzhuan Shi Bixi Wu Pei
| writer         = Wu Lin
| starring       = Cui Jie Zhao Jing Ma Shaohua
| music          = Shi Jiayang
| cinematography = 
| editing        = 
| studio         = Shanghai Animation Film Studio Shanghai Film Group Corporation
| distributor    = 
| released       =   
| runtime        = 80 minutes
| country        = China
| language       = Mandarin Chinese
| budget         = 
| gross          = 
}} voiced by Cui Jie, Zhao Jing and Ma Shaohua.   
 Second World Japanese army occupying the city, and their allies, the Nazi Germany|Nazis. In the background, the Second Sino-Japanese War takes place, while the children must face the uncertainty that concerns the fate of Rina and Mishallis parents in Europe.

Well received in China and internationally, A Jewish Girl in Shanghai has been heralded as the first animated Chinese film to address the Holocaust, and has been described as "China’s first homegrown Jewish film".    The film has been nominated for awards in China and Israel, and has been considered an important step towards improving Peoples Republic of China – Israel relations|Chinas relations with Israel and intercultural relations between the Chinese and Jewish peoples.

==Historical background==
 
 
 ]]
 
 Japanese Empire, Restricted Sector for Stateless Refugees, one of the poorest and most overcrowded areas of the city. Shanghai had previously had a small population of Baghdadi Jews and Russian Jews, the latter mostly having fled the Russian Empire as a result of Anti-Jewish pogroms in the Russian Empire|anti-Jewish pogroms.

The new Ashkenazi Jews that immigrated to Shanghai began arriving from 1933, firstly German Jews following the Nazi Partys rise to power. Despite the cultural differences they faced, and restrictions imposed by the Japanese under Nazi pressure, the Jews of Shanghai survived the war unharmed, as the Japanese refused to hand them over to the Germans.
 Chinese economic growth in recent years, however, the citys Jewish population has grown to around 1,500 in 2010. This compares to a Jewish population of fewer than 100 around 20 years before.   

==Production==
  that appears on the pocket watch.]] Jewish American Breslau (now Jewish religious practices.  Wu has stated that his main reason for making the film was to let children know about what really happened, and to promote the story and the history so that people all over the world can learn from it.   
 talking monkey, has been noted as one of the more child-orientated features of the film. 
 East China Hebrew version of the graphic novel is under consideration. 

In 2009, Wu began work on a film adaptation to bring the work to a broader audience in China. The film, produced using 35 mm film,    is, unlike the graphic novel, in Mandarin Chinese,  and was developed by the Shanghai Film Group Corporation and the Shanghai Animation Film Studio.  The film score was composed by Shi Jiayang. 

==Release==
  in Kilburn, London|Kilburn, London, the site of the films UK premiere.]]
A Jewish Girl in Shanghai made its world premiere in Shanghai in May 2010. The film began showing across China from 28 May.    After a month following the films general release, it was being shown in 200 Chinese cinemas. When it was first shown in China, it was in front of 700 school pupils, who, according to Wu, "laughed and cried several times". He described the emotional impact of the film as "very rare for an animated film". 
 Robinson Road, as part of the 11th Hong Kong Jewish Film Festival.   

The films Israeli premiere took place in July 2010,  when it was shown as part of the 27th Jerusalem Film Festival on 13 and 14 July.    The film was well received by the Israeli audience. 
 British premiere anachronistic use of the word "badass". 

===Reception===
The critical reception to A Jewish Girl in Shanghai has been generally positive. The film was nominated for the Jewish Experience Award at the Jerusalem Film Festival,    becoming the first Chinese film nominated for that award. At the China International Animation and Digital Arts Festival in Changzhou, Jiangsu, China, the film received the Golden Cartoon Award Best Chinese Film Prize.   

The films animation has been widely praised, being described as "beautifully drawn" with "breathtaking traditional animation"  and as being "artfully created with traditional animated imagery". 

The sensitive treatment of the Holocaust has also been commended: "The fresh perspective of the main characters means that the events of the times are freed from cliched depictions of the Holocaust and the viewer is able to draw their own conclusions."   
 flashback scene alpine setting Nazi planes, despite the date preceding the aerial warfare of the Second World War.  Wu himself has said that the film is "both true and untrue" in that it is a composition of fiction and history; "the main characters were all based on real prototypes…I can’t promise it’s 100% accurate, but I think it fits the background of the time." 

===Intercultural relations===
 
A Jewish Girl in Shanghai has been praised by commentators for being a positive step towards improving Chinese&ndash;Jewish and Chinese&ndash;Israeli relations at a time where there is growing interest in China about Jewish life.  

Wu has stated that only by remembering the past can friendship for the future be built,  declaring that while "the friendship between the Jewish people and the Chinese people is only a spray in the long river of world history",  it is "extremely meaningful because it took place in the hard times of Anti-fascism|anti-Fascism." 

==Sequel==
In an interview with Asian Jewish Life, Wu revealed he was planning to write a sequel to A Jewish Girl in Shanghai, stating his intentions that the second animated work would tell the as-yet unseen story of Rina and Mishallis father. Wu explained the reason for this on the basis that he wrote "the front of the face and now need to write from the back." 

==See also==
 
* Shanghai Ghetto (film)|Shanghai Ghetto (film)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 