Bitter & Twisted (film)
 
{{Infobox film
| name = Bitter & Twisted
| image = 
| director = Christopher Weekes
| producer = Bridget Callow
| writer = Christopher Weekes
| starring = Noni Hazlehurst   Steve Rodgers  Leeanna Walsman   Christopher Weekes   Matthew Newton   Gary Sweet   Rhys Muldoon   Penne Hackforth-Jones   Basia AHern
| music = Brian Cachia
| cinematography = Sam Collins
| editing        = Simon B. Wright
| distributor    = Odins Eye Entertainment
| released       =  
| country        = Australia
| runtime        = 90 minutes
| language      = English
| budget         = $160,000
| gross = A$12,500 (Australia) 
}}
Bitter & Twisted is a 2008 feature film written, directed by and starring Christopher Weekes. It premiered at the Tribeca Film Festival   in 2008 and has since screened at over eleven international and Australian festivals and been critically well received. 

==Plot==

Three years after the death of a young man, Liam Lombard (Jeremy Brennan), the story flashes forward to assess the toll it has taken on his parents, brother, and ex-girlfriend, all set against the backdrop of suburban Sydney.

Jordan Lombard (Steve Rodgers) is a broken man, now hideously obese and unable to function. His once happy marriage is skidding hopelessly out of control. His wife Penelope (Noni Hazlehurst) is trapped in routine, devoid of self-respect. Her pain only deepens with the onset of menopause. This humiliation has driven her straight into the arms of another,
younger, man.

Their surviving son Ben (Christopher Weekes) has developed a peculiar relationship with the local boy Matt (Matthew Newton). Though an unlikely pair, a romance has begun to blossom. As Ben’s sexuality comes further into question, he turns his attentions to the girl next-door Indigo (Leeanna Walsman), his dead brothers former lover. Never quite the same since his death, her destructive relationship with a married man, Greg (Gary Sweet), is fading as is her relationship with her mother Jackie (Penne Hackforth-Jones). As Ben sets out to woo her in his own twisted fashion, including dressing like an old neighbour, Indigo comes to find he might be her one true friend.

Then history repeats. Jordan suffers a heart attack, shaking his family to their core. In the middle of a night, three years on from the death of her son, Penelope fights for her husband at a hospital bedside. Desperate to reclaim his life, Jordan races to quit his oppressive job in spectacular and uncharacteristic fashion on his bosses doorstep. When Jordan finally gets home that night – he crumbles in his wife’s arms - a second chance now awarded.

Meanwhile, Ben makes his way to a lonely bus, planning to skip the city with Indigo. While there, he impulsively reaches over and kisses her, hoping all his questions might finally be answered. But there’s nothing.

Liam is no longer the driving force of their lives. So as Ben races off to Matt, Penelope kisses her loving husband goodnight and Indigo begins her adventure from the back seat of a bus, they all finally see a road promised ahead, one with hope and the lessons learnt of living. 

==Cast==
The film features many distinguished and critically acclaimed Australian actors.  The full cast list is as below:
{| class="wikitable"
|-
! Actor !! Role
|-
| Noni Hazlehurst || Penelope Lombard
|-
| Steve Rodgers || Jordan Lombard
|-
| Leeanna Walsman || Indigo Samvini
|-
| Christopher Weekes || Ben Lombard
|-
| Matthew Newton || Matt
|-
| Gary Sweet || Greg Praline
|-
| Rhys Muldoon || Donald Carn
|-
| Basia AHern || Lisa Lombard 
|-
| Penne Hackforth-Jones || Jackie Samvini
|-
| Sam Haft || Lucas
|-
| Andrea Moor || Pauline Praline
|-
| Jeremy Brennan || Liam Lombard
|}

==Themes==
Bitter & Twisted has been noted as having a dream like quality, part-suburban soap opera and part-fairytale. 

==Production ==
Writer/Director Christopher Weekes completed the first draft of Bitter & Twisted at the age of 20. Having grown up in and around the suburban fringe of Sydney, he drew his inspiration from the characters that dwell there. 

Self-financed on a shoe-string budget, the film was shot in 24 days with a small skeleton crew in and around Sutherland in Sydney, 2005. 

The film has been favorably compared to the early works of Jane Campion  and Mike Leigh. 

==Reception== The Australian claiming that it "should be counted among the most positive achievements of Australian cinema". 

== Release ==
Bitter & Twisted was released theatrically in Australian cinemas on 18 September 2008,  and on Australian DVD and home video in January 2010. 

===Awards & Festivals ===
Bitter & Twisted has featured and been in competition at the following festivals  
* 2008 Tribeca Film Festival
* 2008 Dungog Film Festival
* 2008 Montreal Film Festival
* 2008 Sarajevo Film Festival
* 2008 Torino FIlm Festival
* 2008 Goteborg Film Festival
* 2009 Cleveland Film Festival
* 2009 Cinequest Film Festival
* 2009 Dublin International Film Festival
* 2009 15th London Australian Film Festival
* 2009 Glasgow Film Festival
 2008 AFI IF Award Australian Film Critics Circle Awards, Noni Hazlehurst was named Best Actress for her portrayal of Penelope Lombard. 

==References==
 

==External links==
*  

 
 
 
 
 
 