Three Colors: Blue
{{Infobox film
| name       = Three Colors: Blue
| image      = Bluevidcov.jpg
| alt        = 
| caption    = French release poster
| director   = Krzysztof Kieślowski
| producer   = Marin Karmitz
| writer     = Krzysztof Piesiewicz Krzysztof Kieślowski Agnieszka Holland Edward Żebrowski
| starring   = Juliette Binoche Benoît Régent Emmanuelle Riva Florence Pernel Guillaume De Tonquédec
| music      = Zbigniew Preisner
| cinematography = Sławomir Idziak
| editing    = Jacques Witta
| studio     = Eurimages France 3 Cinéma Canal+
| distributor = MK2 Diffusion   Miramax  
| released   =  
| runtime    = 94 minutes  
| country    = France Poland Switzerland
| language   = French Polish
| budget     = 
| gross      = $1,324,974 (US) 
}}
Three Colors: Blue ( ) is a 1993 French   and  . According to Kieślowski, the subject of the film is liberty, specifically emotional liberty, rather than its social or political meaning.  

Set in Paris, the film is about a woman whose husband and child are killed in a car accident. Suddenly set free from her familial bonds, she attempts to cut herself off from everything and live in isolation from her former ties, but finds that she cannot free herself from human connections. 

==Plot==
Julie (Juliette Binoche), wife of the famous composer Patrice de Courcy, must cope with the death of her husband and daughter in an automobile accident she herself survives. While recovering in the hospital, Julie attempts suicide by overdose, but cannot swallow the pills. After being released from the hospital, Julie closes up the house she lived in with her family and takes an apartment in Paris without telling anyone. She leaves behind all her clothes and possessions, taking only a chandelier of blue beads that presumably belonged to her daughter.

For the remainder of the film, Julie disassociates herself from all past memories and distances herself from former friendships, as can be derived from a conversation she has with her mother who suffers from Alzheimers disease and believes Julie is her own sister Marie-France. She also destroys the score for her late husbands last commissioned, though unfinished, work—a piece celebrating European unity, following the end of the Cold War. It is strongly suggested that she wrote, or at least co-wrote, her husbands last work. Snatches of the music haunt her throughout the film, although this is equally consistent with just being illustrative of the depth of their relationship.

She reluctantly befriends an exotic dancer named Lucille (Charlotte Véry) who is having an affair with one of the neighbors and helps her when she needs moral support. Despite her desire to live anonymously and alone, life in Paris forces Julie to confront elements of her past that she would rather not face, including Olivier (Benoît Régent), a friend of the couple. Olivier is also a composer and former assistant of Patrices at the College or university school of music|conservatory. He is in love with Julie, and suspects that she is in fact the true author of her late husbands music. Olivier appears in a TV interview announcing that he shall try to complete Patrices commission. Julie also discovers that her late husband was having an affair.

While trying both to stop Olivier from completing the score, and finding out who her husbands mistress was, she becomes more engaged in her former life. She tracks down Sandrine (Florence Pernel), Patrices mistress, and finds out that she is carrying his child; Julie arranges for her to have her husbands house and recognition of his paternity for the child. This provokes her to begin a relationship with Olivier, and to resurrect her late husbands last composition, which has been changing according to her notes on Oliviers work. Olivier decides not to incorporate the changes suggested by Julie, stating that this piece is now his music and has ceased to be Patrices. He says that she must either accept his composition with all its roughness or she must allow people to know the truth about her composition. She agrees on the grounds that the truth about her husbands music would not be revealed as her own work. [no such agreement occurs, Julie calls Olivier back and agrees to bring the score that she has finished. There is an implication that she has hidden her own work behind the public face of her husband, at the beginning of the film a journalist asks Julie if she is the author of her husbands work. Julie avoids the question.
 chorus and Saint Pauls 1 Corinthians 13 epistle in Koine Greek|Greek), and images are seen of all the people Julie has affected by her actions. The final image is of Julie, crying—the second time she does so in the film. 

==Cast==
* Juliette Binoche as Julie de Courcy (née Vignon)
* Benoît Régent as Olivier Benôit
* Emmanuelle Riva as Madame Vignon, Julies mother
* Florence Pernel as Sandrine
* Guillaume de Tonquédec as Serge
* Charlotte Very as Lucille
* Lorcan McKeon as Lexhibitionniste
* Yann Trégouët as Antoine
* Hélène Vincent as La journaliste
* Philippe Volter as Lagent immobilier 
* Zbigniew Zamachowski as Karol Karol (Cameo appearance|cameo)
* Julie Delpy as Dominique (Cameo appearance|cameo)

==Symbols==
Music plays an intricate element of the plot in that it illustrates Julies efforts to be isolated from everything but cannot do it, such as music cannot be made with a single note but through harmony with all others and how everyone has (or represents) a different kind of music, such as the union of Julie/Patrice had a special tone, which is quite different and more raw with the union of Julie/Olivier.  

A symbol common to the three films is that of an underlying link or thing that keeps the protagonist linked to his/her past. In the case of Blue, it is the lamp of blue beads and a symbol seen throughout the film in the TV of people falling (doing either   the item that links Karol to his past is a 2 Fr. coin and a plaster   the judge never closes or locks his doors and his fountain pen, which stops working at a crucial point in the story. 

Another recurring image related to the spirit of the film is that of elderly people recycling bottles: In Three Colors: Blue, an old woman in Paris is recycling bottles and Julie does not notice her (in the spirit of freedom), in Three Colors: White, an old man also in Paris is trying to recycle a bottle but cannot reach the container and Karol looks at him with a sinister grin on his face (in the spirit of equality) and in Three Colors: Red an old woman cannot reach the hole of the container and Valentine helps her (in the spirit of fraternity).

==Production==
Blue was an international co-production between the French companies CED Productions, Eurimages, France 3 Cinéma, and MK2 Productions, the Swiss company CAB Productions and the Polish company Studio Filmowe TOR.
 tricolor that inspired Kieślowskis trilogy: several scenes are dominated by red light, and in one scene, children dressed in white bathing suits with red floaters jump into the blue swimming pool. Another scene features a link with the next film in the trilogy: spotting the lawyer Sandrine, her husbands mistress, Julie is seen entering a courtroom where Karol, the Polish main character of White, is being divorced by Dominique, his estranged French wife.

==Reception==
 
Three Colors: Blue has received universally positive reviews, 8.5 out of 10 on Tomatometer and 93% audience likes, according to those collated on the Rotten Tomatoes aggregator site  while Internet Movie Database (IMDB) rates it 8.0 out of 10.   
 Guardian commented on the film as:    Blue remains an intense and moving tribute to the woman at its centre who, in coming back from tragedy, almost refuses, but ultimately accepts the only real love thats on offer. 

==Soundtrack==
 

==Awards== Best Actress (Juliette Binoche), Best Cinematography (Sławomir Idziak)
* César Award,  1993 : Best Actress (Juliette Binoche), Best Sound, Best Film Editing
*   (Juliette Binoche) (Nominated)
* Goya Awards (Spains Academy Awards): Best European Film Guldbagge Awards: Best Foreign Film (nominated)   

==References==
 

==External links==
*  
*  
*  
*  
*  
*  by Colin MacCabe
*  by Nick James

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 