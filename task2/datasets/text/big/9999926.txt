Un bon bock
{{Infobox film
| name           = Un bon bock
| image          = Reynaud-Pantomimes.jpg
| caption        = Promotional poster for Pantomimes Lumineuses
| director       = Émile Reynaud
| producer       = 
| writer         = 
| starring       = 
| music          = Gaston Paulin
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 15 minutes (approx)
| country        = France Silent
| budget         = 
}}
 short Animation|animated film directed by Émile Reynaud. Painted in 1888, it was first screened on October 28th, 1892, using the Théâtre Optique process, which allowed him to project a hand-painted colored film, before the invention of cinematograph.

== Around the film ==
It consists of 700 individually painted images and lasts about 15 minutes.   

It is one of the first animated films ever made and was the first to be screened on Reynauds modified praxinoscope, the Théâtre Optique, translated as "optical theatre". 

Alongside Le Clown et ses chiens - painted in 1890 - and Pauvre Pierrot - painted in 1891 - it was exhibited in October 1892 when Émile Reynaud opened his Cabinet fantastique at the Musée Grévin. The combined performance of all three films was known as Pantomimes Lumineuses, and lasted until February 1894. These were the first animated pictures publicly exhibited by means of picture bands. Reynaud gave the whole presentation himself manipulating the images. 

It is considered a lost film. No copy exists, as Reynaud threw all but two of his picture bands into the Seine. 

== Screenplay ==
A wanderer enters a cabaret in the countryside and asks for a beer to a beautiful waitress. She comes back with the pint, as the wanderer begins to court her. In the mean time the kitchen boy comes, drinks the beer and vanishes. The wanderer, misunderstanding, asks for another beer.

Then a traveler enters and has an argument with the first fellow. During the argument, the kitchen boy appears, sips the second beer and runs away. As the traveler quits, the customer finds his glass empty again. He calls the waitress, expresses his disappointment and leaves. 

The kitchen boy comes in and explains to the waitress what he did with the two beers. They make fun together on the wanderer and leave.

==References==
 

== External links ==
*  
*     on Association des Amis dÉmile Reynaud - Émile Reynauds Friends Association - website

 
 
 
 
 
 
 
 


 
 