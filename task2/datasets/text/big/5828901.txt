Le gendarme en balade
{{Infobox film
| name = The gendarme to stroll
| image = Le gendarme en balade.jpg
| caption        = Theatrical release poster
| director = Jean Girault
| producer = Renè Pigneres et Gérard Beytout
| writer = Richard Balducci Jean Girault Jacques Vilfrid
| starring = Louis de Funès Michel Galabru
| music = Raymond Lefèvre
| cinematography = Pierre Montazel
| editing = Armand Psenny SNC
| released =   
| runtime = 100 minutes
| country = France Italy French
| budget         = 
| gross          = $36,529,567  
}}

The gendarme to stroll ( ) is the fourth instalment of the gendarme series starring   and Le gendarme et les gendarmettes.

== Plot ==
Changes are coming for the Gendarmerie Brigade of Saint Tropez. The gendarmes are forced into retirement to make way for a younger breed. Even so, when they learn that one of them has had an accident and has become amnesiac, they reunite to help him get his memory back. Along the way, they have to stop juvenile delinquents to put a nuclear warhead on a rocket said youths built, while being pursued by their younger colleagues.

==Cast==
* Louis de Funès : Ludovic Cruchot
* Michel Galabru: Jérôme Gerber
* Claude Gensac: Josépha
* Jean Lefebvre: Fougasse
* Christian Marin: Merlot
* Guy Grosso: Tricard
* Michel Modo: Berlicot
* France Rumilly: Sister Clothilde
* Nicole Vervil: Madame Gerber
* Dominique Davray: the Abbess
* Yves Vincent: the Colonel

==References==
 

== External links ==
*  
*  
*  at Cinema-français  

 

 
 
 
 
 
 
 
 
 
 


 
 