I Stole a Million
{{Infobox film
| name           = I Stole a Million
| image          = Istoleamillion.jpg
| image_size     =
| caption        =
| director       = Frank Tuttle
| producer       = Burt Kelly
| writer         = Lester Cole Nathanael West
| starring       = George Raft Claire Trevor Dick Foran Victor Jory Frank Skinner
| cinematography = Milton R. Krasner
| editing        = Edward Curtiss
| distributor    = Universal Pictures
| released       =  
| runtime        = 80 mins
| country        = United States English
}}
I Stole a Million (1939) is a crime drama film starring George Raft as a cab driver and small-time crook who makes a big score and lives to regret it.  The supporting cast includes Claire Trevor, Dick Foran, and Victor Jory. The movie was written by Nathanael West from Lester Coles story, directed by Frank Tuttle, and released by Universal Pictures.

==Plot==
 tramp who fence in San Diego, Sacramento to wait for his share of the take.
  crap game, Amesville and settles down, however, within a year, the police are on his trail. Joe then travels to San Diego to demand his money from Patian, but Patians thugs force Joe to rob a post office. Desperate, and afraid that he will be caught if he returns home, Joe disappears.

Some time later, Joe sees a picture of his newborn baby in the newspaper and meets with Laura, who pleads with Joe to give himself up and serve his time so that he can continue his new life. Hearing footsteps, Joe flees from the police who have followed Laura, and Laura is arrested and jailed as an accomplice.

While Laura is in jail, Joe comes up with a plan to steal enough money to make Laura and his daughter financially secure, and he embarks on a robbing spree which earns him the moniker of "the Million Dollar Bandit." After serving her sentence, Laura manages to meet with Joe. She again pleads with him to give himself up, and as the police surround them, Joe has no other choice but to do so.

==Cast==
  
* George Raft as Joe Lourik
* Claire Trevor as Laura Benson
* Dick Foran as Paul Carver
* Victor Jory as Patton
* Joe Sawyer as Billings
* Robert Elliott as Peterson
* Tom Fadden as Verne

==Development== Call Bulletin in 1938.  On January 1939 Nathanael West was assigned to do a script based on Coles story. West came up with a treatment which prompted Joseph Breen, then Director of the Production Code Administration, to declare that while his office had handled roughly 3,600 texts over the year, "it is our unanimous judgment, here in this office, that this new treatment by Mr. West is, by far, the best piece of craftsmanship in screen adaptation that we have seen - certainly, in a year." 

==Reception==
The film garnered favorable reviews, particularly for its script which Variety (magazine)|Variety called "strongly motivated". A reviewer for the Hollywood Reporter wrote, "it is a story which will exert pulse-quickening effect on audiences of both sexes... plot structure and pithy dialogue are all to the plays advantage." 

However the movie was a box office flop. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 86 

In his 1970 book, Nathanael West: The Art of His Life, biographer Jay Martin wrote of the film: "Rafts ambitions innocently enmesh him with the law. From that minor infraction, he becomes involved in a bank holdup but tries to go straight when he falls in love with Claire Trevor. Finding the law on his trail and needing a stake for a small town hideaway, he knocks over a post office. With the money, he buys a village garage and settles down happily... With a baby in the offing the law picks up his trail again... His warped mind sends him through a series of holdups... to gain enough plunder to provide for his wife and baby. But even that, he finds, is a mirage, and he prefers death from the guns of pursuing officers than face a prison term. 

==References==
 

== External links ==
* 
* 
 

 

 
 
 
 
 
 
 
 
 
 