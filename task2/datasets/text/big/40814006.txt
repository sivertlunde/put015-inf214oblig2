El Americano: The Movie
{{Infobox film
| name = El Americano: The Movie
| image = ElAmericanoTheMovie.jpg
| director = Ricardo Arnaiz Mike Kunkel
| producer = Gerry Cardoso  Phil Roman Alex Flores Michael D. Olmos Edward James Olmos
| screenplay = Ricardo Arnaiz Richard Pursel Phil Roman
| story = Ricardo Arnaiz Dulce Belchez Fernando Lewels Rico Rodriguez Edward James Olmos Argelia Atilano Cheech Marin Kate del Castillo Paul Rodriguez K. C. Porter Erik Estrada Adal Ramones Gabriel Iglesias Don Cheto Hector Suarez Lisa Kudrow Pierre Angelo
| music = Leoncio Lara  Olmos Productions Phil Roman Entertainment
| distributor = FilmSharks International
| runtime = 98 minutes
| released =   }}
| country = Mexico United States
| language = English Spanish
| budget = $4 million 
| gross = 
}}

El Americano: The Movie (simply known as El Americano) is an upcoming   of mostly   and the  .     Diario Rotativo|url=http://www.rotativo.com.mx/entretenimiento/cine/156404-presentan-el-americano-the-movie-coproduccion-mexico-eua/|accessdate=16 October 2013|newspaper=Diario Rotativo|date=26 September 2013}} 

FilmSharks International announced that they have acquired the films international rights.    Distribution in Mexico and the United States will be announced later,  and will be distributed independently.   

It is set to be released in theaters in the United States on 28 August 2015. 

==Plot== Rico Rodriguez), a carefree Mexican pre-teen parrot, whose quest is to venture to Hollywood and enlist the aid of his favorite hero in order to help his father, Gayo (Edward James Olmos) and protect his circus family from the threat of Martin Kingfisher (Cheech Marin) and his henchmen.       

==Cast==
===English cast === Rico Rodriguez  as Cuco
*Cheech Marin  as Martin
*Gabriel Iglesias  as Garcia
*Grecia Villar  as Paquito 
*Kate del Castillo  as Rayito 
*Paul Rodriguez  as Divino 
*Edward James Olmos  as Gayo 
*Argelia Atilano  as Lori 
*Ricardo Sanchez  as Vovo 
*K. C. Porter  as Karl 
*Erik Estrada  as Punch 
*Adal Ramones  as Trueno 
*Don Cheto  as Dovo 
*Héctor Suárez  as Eddie Navarro 
*Lisa Kudrow  as Lucille 
*Pierre Angelo as El Mexicano 

===Spanish cast===
*Aleks Syntek  as Cuco
*Gabriel Villar  as Martin
*Mino DBlanc  as Garcia

   Those cast members will also voice their respective characters in the Spanish-language version.  

==Development==
===Production===
Production began on January 2011 when director and  .  “We’re delighted to be working with Edward James Olmos and his production company here in the US. It’s a story we have been working on for the last three years and we feel we have a really strong team in place to execute this film scheduled in 2013.” 

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:28em; max-width: 40%;" cellspacing="5" Animex   born in Puebla 13 years ago, and is the fourth film and will for many more, with El Americano, we ventured to U.S. and we want to impact   Hollywood film  "
|-
|style="text-align: left;"|— Ricardo Arnaiz 
|}
On 26 September 2013, the film was presented at Ibero Puebla, revealing new screenshots and clips.  

  will serve as the films executive producer.]]

During production, Edward James Olmos had recorded his voice before the animation process.  As a result, many cast members were required to act out their roles before recording their voices. 

According to director Ricardo Arnaiz, it is the most expensive film from Animex Producciones and the first to exceed over $29 million Mexican peso|pesos.   

===Animation===
The visual effects and animation production were done by Boxel Studio in  .  In an interview with Andres Reyes Botello, founder of Boxel Studios, Ricardo Arnaiz initially stated he originally intended to produce the film in traditional animation,  but Edward James Olmos insisted Arnaiz to use CG animation at the best chance of the films success.  "Back then, he was explaining to me his situation with his 2D animation studio and his curiosity about making El Americano into a full 3D CGI production," said Andres Reyes Botello. "From there, we instantly got on to a good relationship and started working on the development for the characters and environments, doing some animation, lighting, and rendering tests for this film."  Arnaiz doesnt have much experience in CG animation and has decided to partner with Boxel Studio, which primarily focuses on animation for video games and promotions.  “They were very creative, they had a lot of knowledge and I said: ‘Would you be interested in doing a movie with me? I don’t know anything about CGI, you don’t know anything about making movies — let’s combine and create this thing together,” says Arnaiz.  For assistance, Arnaiz has tapped animation veteran Raul Garcia, and Mike Kunkel, who is Arnaizs personal friend and longed-hoped to work with him, and brought them to Boxel Studios.  “They taught everyone how to make animation, Hollywood style, and in Mexico, we taught the Americans how to do it with low budgets,” said Arnaiz.  A total of 25 animators have worked on this film.  Arnaiz has stated that the development of the film was challenging.  However, the film was finished on time and on budget.  “The biggest challenge was to get it to the level that everyone was hoping for us,” said Arnaiz. “They thought at first that we were going to go to like a direct-to-video quality at the most, and once we showed them some clips they were really excited about it.” 

Development for the film finished on August 2013.   

===Casting===
On 17 April 2013, actress Lisa Kudrow has joined the voice cast as Lucille in both English and Spanish versions.     She is the only non-Hispanic cast member.    To prepare her role, director Ricardo Arnaiz helped Kudrow with her emphasis in her Spanish for her lines. 

==Release==
The film premiered at the Morelia International Film Festival on 22 October 2013.   The film was originally scheduled for a tentative September 2013 release, then a Summer 2014 release.       It is set to be released around August 2015, in 1,200 theaters across the United States.  
 National Museum of Play theater in Rochester, New York on 15 March 2014.  It was shown in both Spanish and English. 

The film is was presented at the San Diego Comic-Con International on 25 July 2014.    The title of the conference is "Big Ideas for Movies: Crossing the Borders with Mexican Animation". 

It had an advanced screening at the 22nd Annual San Diego Film Festival on 14 March 2015.   

The films first official trailer premiered on 30 April 2015 in both English and Spanish with a confirmed release date of 28 August 2015. 
 Regal Cinemas L.A. Live Stadium 14 theater. [https://www.facebook.com/events/405150819665527/ 
EL AMERICANO THE MOVIE Red Carpet Gala] 

==Score==
Leoncio Lara will compose the films score, and performed by the Orchestra of Puebla.      

==Soundtrack==
The soundtrack features 12 original songs written and composed by various Latin artist, and three score pieces composed by Leoncio Lara Bon. The album features the song "Hasta el Cielo Alcanzar" (films main song) written and performed by six-time Latin GRAMMY nominated Aleks Syntek. The album also contains songs by La Arrolladora Banda Limón, Los Tucanes de Tijuana, 3Ball Mty. featuring Don Cheto, Amanditita, Duelo, Horacio Palencia and many more.  The Soundtrack is produced by Gerry Cardoso. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 