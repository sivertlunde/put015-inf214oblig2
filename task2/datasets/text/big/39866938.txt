Romantic Warriors II: Special Features DVD
{{Infobox film
| name           = Romantic Warriors&nbsp;II: Special Features DVD
| image          = RomanticWarriorsII Special Features DVDcover.jpg
| image_size     = 240
| caption        = DVD cover
| director       = Adele Schmidt José Zegarra Holder 
| producer       = Adele Schmidt José Zegarra Holder 
| writer         = 
| narrator       = 
| starring       = {{Plainlist|
*Chris Cutler Christian Vander
*Giorgio Gomelsky
*Roger Trigaux
*Franco Fabbri
*Ferdinand Richard
*Gérard Hourbette
*Marc Hollander
*Lars Krantz Mike Johnson
*Joris Vanvinckenroye
*Francesco Zago
*Carla Kihlstedt
*Matthias Bossi
*Dave Kerman Bob Drake 
}}
| music          = {{Plainlist|
*Aranis
*Yugen
*Thinking Plague
*Hamster Theatre
*Miriodor Ruins Alone
*Stormy Six
*Rabbit Rabbit 
}}
| cinematography = Adele Schmidt José Zegarra Holder 
| editing        = Adele Schmidt Scott Bastedo Tom Fish
| distributor    = Zeitgeist Media 
| released       =  
| runtime        = 100 min. 
| country        = United States
| language       = English French (English subtitles)
| budget         =
}}

Got RIO? Romantic Warriors&nbsp;II: Special Features DVD is a 2013  . The film was written and directed by Adele Schmidt and José Zegarra Holder, and was released in the United States by Zeitgeist Media.  It was generally well received by critics, with a reviewer at AllMusic saying that it "is a showcase for tremendous musicianship and a must-have for existing   fans". 

The Special Features DVD consists of additional material filmed during the making of, but not used in, the original Romantic Warriors&nbsp;II.  Unlike its predecessor, the DVD is not narrated, and features 60 minutes of live performance videos by eight of the bands that appear in the original film, followed by 40 minutes of additional interview clips of people interviewed in the first Romantic Warriors&nbsp;II. 

==Reception==
{{Album ratings
| rev1 = AllMusic
| rev1Score =   
| rev2 = Babyblaue Seiten
| rev2Score = recommended 
}}
Dave Lynch at AllMusic welcomed the one hours worth of live performance videos on the Special Features DVD, which he said made up for the "short performance snippets" in the original documentary.  He was impressed by the music, particularly Yugens "Becchime", which he called "astounding".  Lynch said that the DVD "is a showcase for tremendous musicianship and a must-have for existing   fans", but added that for context "the uninitiated should probably experience the original Romantic Warriors&nbsp;II first".  

Achim Breiling wrote in a review at Babyblaue Seiten that the performances on the Special Features DVD shows how Rock in Opposition as a style of music has diversified since the movement was formed in the late 1970s. He said that the DVD is a great and very informative ("großartige und sehr aufschlussreiche") addition to the Romantic Warriors series. 

==Video performances==
#"Noise" by Aranis (Joris Vanvinckenroye)&nbsp;– 12:00
#*Performed at Madam Fortuna, Antwerp, Belgium in 2011
#**Joris Vanvinckenroye&nbsp;– contrabass
#**Liesbeth Lambrecht&nbsp;– violin
#**Marjolein Cools&nbsp;– accordion
#**Stijn Denys&nbsp;– guitar
#**Jana Arns&nbsp;– transverse flute
#**Dave Kerman&nbsp;– drums, percussion
#**Pierre Chevalier&nbsp;– piano
#**Live sound recording&nbsp;– Pieter Thys
#**Sound mix&nbsp;– Pieter Thys, Joris Vanvinckenroye
#"Becchime" by Yugen (Francesco Zago)&nbsp;– 12:30
#*Performed at RIO Festival, Carmaux, France in 2011
#**Francesco Zago&nbsp;– guitar
#**Paolo Botta&nbsp;– keyboards
#**Maurizio Fasoli&nbsp;– piano
#**Valerio Cipollone&nbsp;– soprano saxophones, clarinets
#**Michele Salgarello&nbsp;– drums
#**Matteo Lorito&nbsp;– bass guitar
#**Jacopo Costa&nbsp;– marimba, vibraphone
#**Live sound recording&nbsp;– Mike Potter
#**Sound mix&nbsp;– Andrea Rizzardo Mike Johnson)&nbsp;– 4:50
#*Performed at Alexander Dawson Arts School, Colorado, US in 2011 Mike Johnson&nbsp;– guitars
#**Elaine DiFalco&nbsp;– vocals
#**Dave Willey&nbsp;– bass guitar
#**Mark Harris&nbsp;– saxophones, clarinets
#**Robin Chestnut&nbsp;– drums
#**Live sound recording&nbsp;– Jon Stubbs
#**Sound mix&nbsp;– Jon Stubbs, Mike Johnson
#"Bug 2: A History of the United States" by Hamster Theatre (Dave Willey/Jon Stubbs)&nbsp;– 4:00
#*Performed at Alexander Dawson Arts School, Colorado, US in 2011
#**Dave Willey&nbsp;– accordion
#**Jon Stubs&nbsp;– keyboards Mike Johnson&nbsp;– guitar
#**Mark Harris&nbsp;– winds, reeds
#**Raoul Rossiter&nbsp;– drums
#**Brian McDougall&nbsp;– bass guitar
#**Live sound recording&nbsp;– Jon Stubbs
#**Sound mix&nbsp;– Jon Stubbs, Dave Willey Ruins Alone (Tatsuya Yoshida)&nbsp;– 7:10
#*Performed at RIO Festival, Carmaux, France in 2011
#**Tatsuya Yoshida&nbsp;– drums
#**Live sound recording&nbsp;– Mike Potter
#**Sound mix&nbsp;– Mike Potter
#"La Roche/Meeting Point" by Miriodor (Bernard Falaise/Pascal Globensky/Rémi Leclerc/Nicolas Masino)&nbsp;– 9:50
#*Performed at Sonic Circuits Festival, Washington, D.C., US in 2010
#**Pascal Globensky&nbsp;– keyboards
#**Pierre Labbé&nbsp;– saxophone, winds
#**Bernard Falaise&nbsp;– guitar
#**Rémi Leclerc&nbsp;– drums
#**Nicolas Masino&nbsp;– bass guitar, keyboards
#**Live sound recording&nbsp;– Mike Potter
#**Sound mix&nbsp;– Mike Potter
#"Gianfranco Mattei" by Stormy Six (Paolo Fabbri/Franco Fabbri)&nbsp;– 5:30
#*Performed at Sesto San Giovanni, Milan, Italy in 2008
#**Carlo De Martini&nbsp;– violin
#**Tommaso Leddi&nbsp;– mandolin
#**Antonio Zanuso&nbsp;– drums
#**Pino Martini&nbsp;– bass guitar
#**Umberto Fioro&nbsp;– acoustic guitar, vocals
#**Franco Fabbri&nbsp;– electric guitar, vocals
#**Salvatore Garau&nbsp;– drums
#**Giorgio Casani&nbsp;– acoustic guitar, vocals
#**Massimo Villa&nbsp;– bass guitar
#**Sound engineer&nbsp;– Giorgio Albani
#"Tiny Invasion" by Rabbit Rabbit (Carla Kihlstedt)&nbsp;– 4:31
#*Performed at Orion Sound Studios, Maryland, US in 2011
#**Carla Kihlstedt&nbsp;– violin, voice
#**Matthias Bossi&nbsp;– drums, percussion

==References==
{{reflist|refs=

  |publisher=AllMusic|accessdate=2013-07-04}} 

 Romantic Warriors&nbsp;II: Special Features DVD. Washington, D.C.: Zeitgeist Media (2013). 

   

   
}}

==External links==
* 

 
 	
 
 