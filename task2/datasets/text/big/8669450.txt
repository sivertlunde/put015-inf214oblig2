One Six Right
{{Infobox film
| name           = One Six Right: The Romance of Flying
| image          = OneSixRight poster medium.jpg
| caption        = Theatrical release poster
| story by       = Brian J. Terwilliger
| starring       = Sydney Pollack Lorenzo Lamas Tony Bill Paul Moyer Hal Fishman Clay Lacy Bob Hoover Desiree Horton
| director       = Brian J. Terwilliger
| producer       = Brian J. Terwilliger
| music          = Nathan Wang
| distributor    = Terwilliger Productions
| released       =  
| runtime        = 73 min.
| language       = English
| movie_series   =
| budget         =
}}

One Six Right: The Romance of Flying is an independent film about the general aviation industry as seen through a local airport. Within a short period of time, it has achieved a passionate following and presence among pilots and aviation enthusiasts worldwide who see the film as being able to communicate their passion for aviation.    Concurrently, the film has garnered both local and national political attention in the United States as an accurate depiction of general aviation and its important contributions to all aviation industries worldwide. Within the entertainment industry, the film has attracted the sponsorship and support of many large media companies, including Apple Inc.,   Sony Electronics,  Toshiba, Technicolor, Bose Corporation|Bose, and Dolby, as pioneering new standards in high definition (HD) film making and distribution.

== Official synopsis==
One Six Right considers local airports through the life, history and struggle of Southern Californias Van Nuys Airport. Through aerial sequences and stories told by pilots, air traffic controllers, historians and flight enthusiasts, including well-known faces such as Sydney Pollack, Lorenzo Lamas, Paul Moyer, Hal Fishman, Desiree Horton and many others. The film uncovers the history of the airport where Amelia Earhart broke a world speed record over its runways, and where Marilyn Monroe was discovered while working in its hangars; scenes from Casablanca (film)|Casablanca were filmed on the grounds.

A historical perspective of airplanes from the 1920s to the business jets of today frames appreciation for the significance of all general aviation airports as a critical component of the communities they serve. Located in the heart of the San Fernando Valley, VNY is today the worlds busiest general aviation airport and contributes over $1 billion each year to the southern California economy.

One Six Right explores common misconceptions about general aviation airports, which are often criticized for noise pollution and viewed as exclusive playgrounds for the rich. The film creates an awareness of the threat to these community airports through staggering statistics of airports that no longer exist, and the rapid rate at which they are continuing to close: one per week in the U.S. These smaller and often forgotten airports are the foundation of the aviation industry, contributing to global commerce and the breeding ground of the pilots of tomorrow. 

== Independent distribution pioneering ==
The One Six Right DVD made news by selling over 10,000 units independently in its first two weeks of release. By March 2006, Brian J. Terwilliger (Producer/Director) made the cover of The Hollywood Reporter in an article titled Filmmaker Takes Flight to DVD. Brians determination to maintain all distribution rights was a noted departure from the common practice of selling rights to a third party distributor. During the first year of distribution, One Six Rights website became a leading example of successful independent film distribution by developing a viral marketing strategy that included multiple levels of consumer awareness allowing the film to develop significant presence within its niche audiences worldwide.

== Innovation in HD distribution ==

=== First to HD DVD ===
Filmed entirely in high definition, One Six Right took an industry leading position as an independently financed and produced film shot in high definition and distributed on DVD in both SD and HD formats.  

One Six Right is:
* first independent film on HD DVD  
* first documentary film on HD DVD
* first aviation film on HD DVD

The HD DVD was a popular demonstration disc because of its HD special features and has since been replaced by the more popular and widely used Blu-ray Disc format. One Six Right has not been released on Blu-ray.

==== First Exclusive 4K Tour====

In partnership with Sony Electronics, the One Six Right national tour covered twelve U.S. cities between July and December 2006 and showcased Sonys SXRD 4K large venue digital projectors. Completely self-funded by ticket and DVD sales at each venue, the tour received notable coverage within the entertainment industry as pioneering HD theatrical exhibition. The Hollywood Reporter covered the tours success in the November 13, 2006, "Leadership in Hollywood" special edition in an article titled  "Digital do-it-yourself".   {{cite web | url=http://www.digitalcinemasociety.org/TechTips.php?item=Digital+Features+2006 | title=Digitally Acquired Features and TV Dramas for 2006
}} Digital Cinema Society website posting, captured March 17, 2007 
 AirVenture Oshkosh Bose installed a   wide screen and a 5.1 channel digital surround system in the AirVenture Museum. 

====Notable tour finale====

The tour ended on December 2, 2006 with a finale screening at the   Syncro Aviation hangar at Van Nuys Airport. Shown on the largest screen of all; Sony created a custom SXRD 4K high definition 5.1 digital surround sound movie theater in the east half of hangar with 1,300 seats for the audience.
 Pitts biplanes, F-5 Freedom Fighter and T-38 Talon jets, an T-6 Texan, an A-26 Invader, a B-25, a Cirrus VK-30 and a Cessna 172.

Also on display were the actual high-definition cameras used to make the film and the helicopter and its gyroscopically stabilized Gyron camera system that shot the aerial sequences. Two hundred sixty movie lights provided dramatic museum-style lighting from above. 

After the film played at the tour finale, producer/director Brian J. Terwilliger presented EAA president Tom Poberezny with a $15,395 donation to EAAs Young Eagles charity. The money was the result of a $5 per DVD donation from sales on the EAA website (during July 2006) and also during the previous EAA AirVenture Oshkosh airshow where the distributors of One Six Right collective sold 3,079 DVDs. {{cite web | url=http://www.eaa.org/communications/eaanews/pr/060630_onesix_right.htm | title=ACCLAIMED ONE SIX RIGHT AVIATION DVD AVAILABLE THROUGH EAA BEGINNING JULY 1
}} EAA Press Release, July 2006  

A photo slide show of this finale event can be viewed on the films  .

== Political activism ==

On September 22, 2006, One Six Right was exhibited to the United States Congress on Capitol Hill in Washington, D.C. This special screening was organized and sponsored by the Aircraft Owners and Pilots Association (AOPA), the General Aviation Manufacturers Association (GAMA), the National Air Transportation Association (NATA) and the National Business Aviation Association (NBAA).   

In the Spring of 2005, AOPA sent a complimentary copy of One Six Right to every member of Congress who was a private pilot. Additionally, AOPA sent over 1,400 DVDs of One Six Right to all their airport support network volunteers in hopes of inspiring local community discussions on the importance of general aviation.

On November 15, 2005, AOPA President Phil Boyer awarded Brian J. Terwilliger with AOPAs Special Citation for Excellence for promoting General Aviation through the art of filmmaking. 

One Six Right has also been incorporated into the curriculum of many educational programs and is frequently used to educate students on the workings and importance of the General Aviation industry as the foundation to all aviation commerce worldwide. 

==Licensing==
In response to hundreds of grass-roots organized screenings, the distributors of One Six Right have created a public performance licensing process.    {{cite web | url=http://web.nbaa.org/public/cs/amc/2006/news/onesixright.php
 | title=NBAA Theater: One Six Right |accessdate=March 19, 2007 }}   

==Making of==
The Making Of One Six Right was released in 2006 on a companion DVD titled  . This twenty-minute special feature showcased the six-year journey that brought One Six Right from conception to theatrical premiere.

=== Made on a Mac ===

In December 2006, Apple Inc. showcased on their website a profile featuring the   of the making of One Six Right.

* Apple Inc. products used on this film: G5, G4, Xserve RAID, HD Cinema Display, FCP 4.5, FCP 5, Compressor, DSP 2, DSP 4, Cinema Tools, QuickTime, iTunes, iPhoto
* Processed on Macs for this film: DV offline edit, HD online edit, design and render for all graphics, visual effect shots, photo restoration, DVD sleeve, insert, and label design, movie poster design, HD color correction, 30P to 24P frame rate conversions, and final output to D5

====Milestones====
* December 25, 2006, One Six Right had its world television premiere on Discovery HD Theater (now HD Theater), a sister network to The Discovery Channel.
* By its 12th month of distribution, over 45,000 One Six Right products had been sold to consumers worldwide.
* By December 2006, a full line of branded merchandise had been produced and made available. Products included a flex fit hat, a 2007 14-month calendar, a CD soundtrack, a 1/20th scale Piper Cub replica, an HD DVD and One Six Left - The Companion DVD.
* By March 2007, One Six Right products were available internationally through pilot shops and entertainment stores in Australia, North America, Europe, Africa and Asia.

== MPAA rating ==
One Six Right is rated "G" for "General Audiences or All Ages Admitted" by the MPAA Film Rating System.

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 