A Ship Bound for India
{{Infobox film
| name           = A Ship Bound for India
| image          = Skepp till India land.jpg
| director       = Ingmar Bergman
| producer       = Lorens Marmstedt
| screenplay     = Ingmar Bergman
| based on       =  
| starring       = {{Plainlist|
*Holger Löwenadler
*Anna Lindahl
*Birger Malmsten
*Gertrud Fridh }}
| music          = Erland von Koch
| cinematography = Göran Strindberg
| editing        = Tage Holmberg
| distributor    = Nordisk Tonefilm
| released       = 22 September 1947
| runtime        = 98 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Swedish film directed by Ingmar Bergman.  It was originally released as A Ship to India in the United Kingdom and Frustration in the United States.  The screenplay was written by Bergman, based on the play by Martin Söderhjelm. 

The film tells the story of the past of the character Johannes, and his relationships with his cruel father, his mother, and his fathers mistress with whom Johannes falls in love.

The movie contains sequences of despair and anguish. Birger Malmsten, who plays the lead character Johannes and who will be seen in several later Bergman films, is immensely likable and compelling as the hunchback son who finally stands up to his despotic father.

The film is about the relationships within a family, a subject with which Bergman often dealt in later films, and uses other common devices of Bergman such as the hard father figure.

The film was entered into the 1947 Cannes Film Festival.   

==Cast==
*Birger Malmsten– Johannes Blom
*Holger Löwenadler – Kapten Blom
*Anna Lindahl – Alice Blom
*Gertrud Fridh – Sally
*Naemi Briese – Selma
*Hjördis Petterson – Sofi (as Hjördis Pettersson)
*Lasse Krantz – Hans
*Jan Molander – Bertil
*Erik Hell – Pekka
*Åke Fridell – Variety hall owner

*Douglas Håge – Customs officer (scenes deleted)
*Ami Aaröe – Young girl at the beach (uncredited)
*Torgny Anderberg – Man (uncredited)
*Ingmar Bergman  – Man wearing a beret at the funfair (uncredited)
*Rolf Bergström – Bloms companion (uncredited)
*Torsten Bergström – Bloms companion (uncredited)
*John W. Björling – Older man (uncredited)
*Ingrid Borthen – Girl on the street (uncredited)
*Gustaf Hiort af Ornäs – Bloms companion (uncredited)
*Svea Holst – Woman present at the arrest of Blom (uncredited) Kiki – Dwarf (uncredited)
*Uno Larsson – Older man with glasses and hat (uncredited) Peter Lindgren – Foreign sailor (uncredited) Gunnar Nielsen – Young man at the beach (uncredited) Charles White – Black sailor (uncredited)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 

 