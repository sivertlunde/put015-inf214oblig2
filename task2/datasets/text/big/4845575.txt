Genius Party
 
{{Infobox film
| image= 
| image size= 
| caption= 
| director=Atsuko Fukushima Yoji Fukuyama Hideki Futamura Shōji Kawamori Shinji Kimura Mahiro Maeda Kôji Morimoto Kazuto Nakazawa Shinya Ôhira Tatsuyuki Tanaka Shinichirō Watanabe Hiro Yamagata Masaaki Yuasa
| producer= Yukie Saeki
| writer=Yoji Fukuyama Hideki Futamura Shōji Kawamori Mitsuyoshi Takasu Masaaki Yuasa
| narrator= 
| starring= Tomoko Kaneda Rinko Kikuchi Lu Ningjuan Taro Yabe Yûya Yagira Fennesz Nobukazu Takemura Yoko Kanno 
| cinematography = 
| editing= 
| studio=Studio 4°C
| distributor= 
| released= 
| runtime=85 min.
| country= Japan 
| language=Japanese
| budget= 
| gross= 
}}
{{Infobox film
|name=Genius Party Beyond
| image= 
| image size= 
| caption= 
| director=Mahiro Maeda Kôji Morimoto Kazuto Nakazawa Shinya Ôhira Tatsuyuki Tanaka
| producer=
| writer=
| narrator= 
| starring= Arata Furuta Akiko Suzuki Shôko Takada Urara Takano
| music= Akira Ifukube Warsaw Village Band Minami Nozaki Juno Reactor 
| cinematography = 
| editing= 
| studio=Studio 4°C
| distributor= 
| released= 
| runtime=
| country= Japan 
| language=Japanese
| budget= 
| gross= 
}} animated films from Studio 4°C released on July 7, 2007.

A sequel called Genius Party Beyond was released on February 15, 2008, and includes most of the films which were finished but not included in the original Genius Party.

==The films==

{| class="wikitable"
|-
!Title
!Director
|-
|"Genius Party" Atsuko Fukushima
|-
|"Shanghai Dragon" Shoji Kawamori
|-
|"Deathtic 4" Shinji Kimura
|-
|"Doorbell" Yoji Fukuyama
|-
|"Limit Cycle" Hideki Futamura
|-
|"Happy Machine" Masaaki Yuasa
|-
|"Baby Blue"  Shinichiro Watanabe
|}

==Voice cast==
*Tomoko Kaneda
*Rinko Kikuchi
*Lu Ningjuan
*Taro Yabe
*Yûya Yagira
*Yoko Kanno

==Genius Party Beyond==

===The films===
{| class="wikitable"
|-
!Title
!Director
|-
|"Gala"  Mahiro Maeda
|-
|"Moondrive" Kazuto Nakazawa
|-
|"Wanwa the Doggy" Shinya Ohira
|-
|"Toujin Kit" Tatsuyuki Tanaka
|-
|"Dimension Bomb" Koji Morimoto
|}

===Voice cast===
*Arata Furuta
*Akiko Suzuki
*Shôko Takada
*Urara Takano

==References==
 

== External links ==
*  (in Japanese)
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 