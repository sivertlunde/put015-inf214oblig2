Mihail, câine de circ
{{Infobox film
| name           = Mihail, câine de circ
| image          = 
| caption        = 
| director       = Sergiu Nicolaescu
| producer       = 
| writer         = Jack London (Michael, Brother of Jerry|novel) Dumitru Carabăț Rüdiger Bahr 
| starring       = Karl Michael Vogler Ernest Maftei Sergiu Nicolaescu Ilarion Ciobanu Amza Pellea
| music          =  Bert Grund
| cinematography =Nicolae Girardi  
| editing        = Adolf Schlyssleder
| distributor    = Româniafilm
| released       =  
| runtime        = 87 minutes
| country        = Romania  East Germany Romanian
| budget         = 
}}

Mihail, câine de circ ( )  is a 1979 Romanian adventure film directed by Sergiu Nicolaescu.

==Cast==
* Karl Michael Vogler - Dag Daughtry
* Ernest Maftei - Greenleaf
* Sergiu Nicolaescu - Harley Kennan
* Ilarion Ciobanu - Kapitän Duncan
* Amza Pellea - Dr. Emory
* Ion Besoiu - Harry Del Mar
* Vincent Osborne - negrul Kwaque
* Hans W. Hamacher - Kapitän Doane
* Ileana Popovici - Villa Kennan
* Anthony Chinn - Ah Moy
* Cornel Gîrbea - Grimshaw
* Colea Răutu - Nishikanta
* Mircea Albulescu - dr. Edmond Masters
* Dina Mihalcea - doamna Collins
* Valeria Gagialov - Mary, soția dr. Emory (as Valeria Gagealov)
* Ștefan Mihăilescu-Brăila - recrutorul de matrozi
* Ion Anestin
* Constantin Băltărețu - chelner
* Vladimir Găitan - Cop
* Doru Năstase
* Alexandru Dobrescu - pasager
* Mircea Crețu
* Gheorghe Visu - valet
* Dinu Gherasim
* Ion Andrei
* Nicolae Dide
* Păstorel Ionescu - dansator Vasile Popa
* Titus Gurgulescu
* Ovidiu Georgescu

==References==
 

==External links==
*   at IMDb
*    at Cinemagia

 

 
 
 
 
 
 
 
 


 
 