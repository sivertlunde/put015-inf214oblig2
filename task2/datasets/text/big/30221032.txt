All's Well, Ends Well 2011
 
 
{{Infobox film
| name = Alls Well, Ends Well 2011
| image = Alls Well Ends Well 2011 poster.jpg
| caption = 
| film name = {{Film name| traditional = 最強囍事
| simplified = 最强喜事
| pinyin = Zuì Qiáng Xǐ Shì
| jyutping = Zeoi3 Keong4 Hei2 Si6}} Raymond Wong
| director = Chan Hing-ka   Janet Chun
| writer = Edmond Wong Chan Hing-ka
| starring =  
| editing = 
| cinematography = Bruce Yu Pegasus Motion Pictures Icon Pictures Limited
| distributor = Pegasus Motion Pictures Distribution Limited 
| released =   }}
| runtime = 104 minutes
| country = Hong Kong
| language = Cantonese
| budget = 
| gross =
}}
Alls Well, Ends Well 2011 is a 2011 Hong Kong romantic comedy film directed by Chan Hing-ka and Janet Chun. It is the sixth installment in the Alls Well, Ends Well film series, was released on 2 February 2011. 

==Plot==
Make-up artist Sammy (Louis Koo) is hired by Dream (Yan Ni) as the director of a cosmetic company with Claire (Cecilia Cheung) being the only colleague who is willing to assist him. Sammy invites Arnold (Donnie Yen), a fellow make-up artist, to join him. Though Arnold might appear to be a woman’s magnet, his heart still lingers with his first love Mona (Carina Lau), a frustrated writer. A minor incident in the new product commercial ties the friendship among Sammy, Arnold and the yacht billionaire Syd (Chapman To). Syd meets Claire and aims to pursue her. Having grown up in a poor family, Claire seizes this as a golden opportunity to live a prosperous life and asks Sammy for help to fulfill her dream, without realizing that she has fallen in love with Sammy.

==Cast==

===Main cast===
{|class="wikitable" width="60%"
|- style="background:cornflowerblue; color:white" align=center
|style="width:6%"|Cast||style="width:6%"|Role||style="width:15%"|Description
|-
| Donnie Yen || Arnold Cheng 鄭宇強 || Make-up artist on counter Mona Tais first boyfriend
|-
| Louis Koo || Sammy Shum 沈美 || God-class make-up artist Claire Kans boyfriend
|-
| Carina Lau || Mona Tai 戴夢妮 || Frustrated writer Arnold Chengs first girlfriend
|- Raymond Wong || Clerk Chan 陳奇俠 || Oil magnate Dreams boyfriend
|-
| Cecilia Cheung || Claire Kan 簡潔 || Sammy Shums assistant and girlfriend
|-
| Chapman To || Syd Liu 廖小必 (滑溜溜) || Rich Yacht Prince Victoria Lees boyfriend
|-
| Lynn Hung || Victoria Lee || Real estate tycoons daughter Syd Lius girlfriend
|-
| Yan Ni || Dream 占占 || Cosmetic company director Clerk Chans girlfriend
|-
|}

===Other cast===
{|class="wikitable" width="60%"
|- style="background:cornflowerblue; color:white" align=center
|style="width:6%"|Cast||style="width:6%"|Role||style="width:15%"|Description
|-
|  ) || Helen Cheng || Arnold Chengs younger sister
|-
|  ) || || Arnold Chengs aunt
|-
| Nancy Sit || || Arnold Chengs mother
|-
| Elvina Kong || || Arnold Chengs younger sister
|-
| Ronald Cheng || || Delivery boy
|-
| Lee Heung-kam || || Arnold Chengs grandmother
|-
| Stephy Tang || || Arnold Chengs niece
|-
| Ciwi Lam || Social ||
|-
|  ) || Creamy || Sammy Shums secretary
|-
| Angelababy || Better || Receptionist
|-
|  ) || Memo || Dreams secretary
|-
| JJ Jia || Kiki ||
|-
| Jo Kuk || Siu Tim Kuk 小甜谷 ||
|-
| Wilson Yip || Minister 神父 ||
|-
|  ) || Jodi ||
|-
|  ) || Posh lady 	闊太太 ||
|-
| Paw Hee-ching || Cardboard granny 紙皮婆婆 ||
|-
|  ) || Cardboard girl 紙皮妹妹 ||
|-
|  ) || Posh lady 闊太太 ||
|-
|}

==Release==
The film was released on 2 February 2011, which was Chinese New Year Eve. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 


 
 