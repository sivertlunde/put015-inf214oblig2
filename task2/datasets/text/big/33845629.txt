Tokyo File 212
 
 
{{Infobox film
| name = Tokyo File 212
| image_size =
| image = Tokyo File 212 FilmPoster.jpeg
| caption =
| director = Dorrell McGowan Stuart E. McGowan
| producer = Melvin Belli (executive producer) George P. Breakston (producer) Dorrell McGowan (producer) C. Ray Stahl (associate producer)
| writer = George P. Breakston (story) Dorrell McGowan Stuart E. McGowan
| narrator =
| starring = See below
| music = Albert Glasser
| cinematography = Ichirô Hoshijima Herman Schopp
| editing = Martin G. Cohn
| studio =
| distributor =RKO (United States)
| released =   }}
| runtime = 84 minutes
| country = USA Japan
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}

Tokyo File 212 is a 1951 American film directed by Dorrell McGowan and Stuart E. McGowan. It was credited as Hollywoods first feature film to be shot entirely in Japan. 

==Plot==
U.S. intelligence agent Jim Cater is sent to Japan as a journalist to find Taro Matsudo who is helping the Communists there. Matsudo happens to be Carters college friend. In his hotel, Carter meets Steffi Novak who agrees to help him in his mission. He comes to know that Matsudo aspired to be kamikaze pilot but when Japan surrendered during World War II he sided with the Communists. Oyama is revealed to be the leader of the communists. After completing his mission Carter returns to the United States.

== Cast ==
*Florence Marly as Steffi Novak
*Lee Frederick (Robert Peyton)  as Jim Carter
*Katsuhiko Haida as Taro Matsuto
*Reiko Otani as Namiko
*Tatsuo Saitô as Mr. Matsuto
*Tetsu Nakamura as Mr. Oyama
*Heihachirô Ôkawa
*Suisei Matsui as Joe
*Jun Tazaki
*Dekao Yoko
*Hideto Hayabusa
*Gen Shimizu
*Richard W.N. Childs
*Richard Finiels
*Stuart Zimmerley
*James Lyons
*Byron Michie as Mr. Jeffrey
*Ichimaru as Herself (Geisha Singer)

== Production ==
The film was shot on real locations in Japan instead of sets as commonly done by former American films that featured Japan. It was co-produced by Breakston–McGowan Productions and Tonichi Kogyo.  Paris-born George Paul Breakston, who had appeared in It Happened One Night (1934) and The Grapes of Wrath (1940) as a child actor, worked in the Signal Corps during World War II and also visited Tokyo. When the war ended, Breakston shifted his focus towards films and directed Urubu: The Story of Vulture People (1948) and
Jungle Stampede (1950). It was during this time that he drafted Tokyo File 212 and met Hollywood studio executives and producers with the script. Screenwriters Dorrell and Stewart McGowan agreed to back the production and for this venture Breakston–McGowan Productions, Inc. was established.  Lawyer Melvin Belli also invested in the project.  The production company joined hands with Suzuki Ikuzos Tonichi Enterprises Company. The latter agreed to provide half of the budget and Japanese actors and crew members in return for half of the earnings in both the Japan and the United States.  The cast included Robert Peyton, Florence Marly, Saito Tatsuo, Matsui Suisei, Nakamura Tetsu, Haida Katsuhiko and Otani Reiko.  Real military generals and detectives were cast for the roles. 

American actors and crew members reached Japan on July 21, 1950. Principal photography was completed in 36 days and the final version was prepared in 2 months. Japanese priemere was held on January 24, 1951 and it was released in the United States on May 5. 

For the films U.S. premiere, geishas were brought from Japan to perform at Republic Theater. 

== Soundtrack ==

* "Oyedo Boogie" (Music & lyrics by Yasuo Shimizu & Shizuo Yoshikawa)

==Notes==
 

==Bibliography==
*  
*  
*  
*  
*  
*  
*  
*  
*  

== External links ==
 
* 
* 
* 

 
 
 
 
 
 
 
 
 