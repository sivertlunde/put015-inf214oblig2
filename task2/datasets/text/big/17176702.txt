Super Comet: After the Impact
Super documentary produced Stefan Schneider.

The two-hour production hypothesizes the effects on modern-day earth of a large comet impacting in Mexico near the same location of the Cretaceous–Paleogene extinction event, the ancient impact of a comet or meteor that is believed to have triggered the mass extinction of the dinosaurs.

The film alternates between interviews with climatologists and researchers and dramatized scenes following several groups of people as they attempt to survive in the days and months after the disaster: a separated family in France, a pair of scientists in Hawaii, a man who manages to survive for a period of time near the ground zero impact in Mexico, and a tribe in Cameroon.

==DVD release==
Discovery Channel released the film to DVD in North America on Dec.4, 2007. 

==External links==
*  
*   program details. (link broken as at Mar 2012)
*   in The New York Times
*   in The Hindu
*   at Digitally Obsessed

 

 
 
 
 
 


 