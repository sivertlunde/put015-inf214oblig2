Evil Bong
{{Infobox film
| name           = Evil Bong
| image          = bbong teaserposter.jpg
| image_size     = 
| caption        = Teaser poster
| director       = Charles Band
| producer       = 
| writer         = 
| narrator       = 
| starring       = David Weidoff John Patrick Jordan Mitch Eakins
| music          = District 78
| cinematography = 
| editing        = 
| distributor    = Full Moon Features
| released       =  
| runtime        = 80 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Evil Bong is a 2006 horror/comedy film directed by  , and   and the crossover film Gingerdead Man vs. Evil Bong.

== Plot ==
 
Nerdy college student Alistair McDowell (David Weidoff) moves in with law school drop-out Larnell (John Patrick Jordan), typical "surfer-stoner" Bachman (Mitch Eakins) and former baseball player Brett (Brian Lloyd). During Alistairs stay, Larnell sees an ad for a large bong, in which the previous owner claims it was "possessed". After receiving the bong and taking a couple of hits (with the exception of Alister, who doesnt smoke), Brett introduces Alister to his girlfriend Luann (Robin Sydney) and her friend Janet (Kristyn Green), who Alister develops a crush for.
 Ooga Booga Ivan Burroughs. Hes introduced by one of the strippers (Kristen Cladwell) who has skull heads on the cups of her bra. When he comes near the bra, the skull heads start biting him in the neck and he bleeds to death. The next morning, the other roommates find Bachman dead on the couch. Alistair tells to them that its probably from the weed that came with the bong. Larnell also notices that the bong has changed. The trio then hide his body underneath a pile of trash in the basement, after nearly getting caught by Larnells paralyzed-but-wealthy grandfather, Cyril (Jacob Witkin), who came by to tell Larnell that he just got remarried.
 Gingerdead Man, Jack Attack, and Luann is taken away by the bouncer. Brett is then treated by another stripper (Brandi Cunningham), who happens to be Bretts ex-girlfriend Carla Brewster. She eventually uses her lip-cupped bra to bite off Bretts genitals, killing him.

Meanwhile, when Alistair starts figuring out whats going on, Janet falls prey to the bong and passes out. A man named Jimbo Leary (Tommy Chong) randomly enters the room and proclaims that the bong is his. He also explains that the bong (named Eebee) has a voodoo curse on it and that once you take enough hits from it, it brings you to the "Bong World" (the strip club) and kills you. In order to save Janet, Alistair takes a hit from the bong and is instantly sent to the bong world. Meanwhile, Jimbo tries destroying the bong with a hammer, chainsaw, and (resorting to drastic measures) a bomb. Eebee unleashes marijuana smoke from her bong, causing Jimbo to inhale and pass out.

Meanwhile in the Bong World, Eebee forces the strippers to seduce Alistair, but he breaks them off. During his search, he bumps into  !

== Cast ==
*David Weidoff as Alistair McDowell
*John Patrick Jordan as Larnell
*Mitch Eakins as Bachman
*Brian Lloyd as Brett
*Robin Sydney as Luann
*Kristyn Green as Janet
*Tommy Chong as Jimbo Leary
*Michelle Mais as Eebee (voice)
*Jacob Witkin as Cyril Cornwallis
*Phil Fondacaro as Ivan Burroughs
*Tim Thomerson as Jack Deth
*Bill Moseley as Bong World Patron
*Brandi Cunningham as Carla Brewster
*Dana Danes as Bong World Dancer
*Gina-Raye Carter as Bong World Dancer
*Sonny Carl Davis as Delivery Man
*Sylvester "Bear" Terkay as Bouncer
*Dale Dymkoski as Male Dancer
*Mae LaBorde as Rosemary Cornwallis
*John Carl Buechler as Gingerdead Man (puppeteer/voice)

== Soundtrack ==
  associated soundtrack Volume 10 ("Pistol Grip Pump"). It also has music from the films composers, District 78, and two tracks from music group the Kottonmouth Kings.

==External links==
* 

 
 
 

 

 
 
   
   
 
 