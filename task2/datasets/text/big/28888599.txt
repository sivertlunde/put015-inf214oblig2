Maanthrikam
{{Infobox film
| name           = magic
| image          = Maanthrikam.jpg
| image_size     =
| caption        =
| director       = Thampi Kannanthanam
| producer       = Thampi Kannanthanam
| writer         = Babu Pallassery
| narrator       =
| starring       = Mohanlal  Jagadish Priya Raman
| music          = S. P. Venkatesh
| cinematography = Saloo George
| editing        = Sreekar Prasad
| distributor    = Julia Picture Release
| released       = 1995
| runtime        =
| country        = India
| language       = Malayalam
| budget         = 3 crore
| gross          = 355 million
}}
Maanthrikam is a 1995 Malayalam movie directed by Thampi Kannanthanam. The movie features Mohanlal, Jagadish and Priya Raman in the lead roles. This movie marked the debut for the actor Vinayakan. Maanthrikam was the heavy budget movie of the year. And Maanthrikam was declared as Blockbuster of 1995.

== Cast ==
* Mohanlal as Alby / Major Stephen Ronald  
* Jagadish as Jobi Dcosta / Subedar Teddy Lopez
* Priya Raman as Betty Fernadez
* Hemanth Ravan as Gonsalvas
* Rajan P. Dev as Antonio
* Vinayakan as Michael Jackson Dupe
* Raghuvaran as Abdul Rahiman
* Sreenath as Raveendran
* Ravi Menon as Father Thaliyath
* Madhupal as Villy Krishnakumar as Douglas
* Kollam Ajith Santhosh
* Vineetha as Menaka Vaishnavi as Shakeela
* Mithra Joshi

== Crew ==
* Cinematography: Saloo George
* Editing: Sreekar Prasad
* Art: Sabu Cyril
* Makeup: Punaloor Ravi
* Costumes: Mani, Murali
* Choreography: D. K. S. Babu
* Stunts: Super Subbaraayan
* Advertisement: Kitho
* Lab: Prasad Colour Lab
* Stills: Suresh Merlin
* Effects: Murukesh
* PRO: Vazhoor Jose
* Outdoor: Anu Enterprises
* Titles: Balan Palayi
* Associate Director: Lal Jose

== Songs ==
S. P. Venkatesh composed the music for the movie for which the lyrics were written by O. N. V. Kurup. The songs were distributed by Ankit Audios.

* Kelee Vipinam: Biju Narayanan
* Mohikkum Neermizhiyode: K. J. Yesudas, K. S. Chithra
* Dhim Dhim thirudi: M. G. Sreekumar, Alex
* Kelee Vipinam: K. S. Chitra

==Boxoffice==

Maanthrikam was Onam release and it declared as one of the Blockbuster of year.

==External links==
*  

 
 
 

 