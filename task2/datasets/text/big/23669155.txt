Girl Friends (1936 film)
{{Infobox film
| name           = Girl Friends
| image          = 
| image_size     = 
| caption        = 
| director       = Lev Arnshtam
| producer       =  Nikolai Tikhonov Raisa Vasilyeva
| narrator       = 
| starring       = Zoya Fyodorova Yanina Zhejmo Irina Zarubina
| music          = Dmitri Shostakovich
| cinematography = Vladimir Rapoport Arkadi Shafran
| editing        = 
| distributor    = 
| studio         = Lenfilm
| released       = 19 February 1936
| runtime        = 2,611 meters (95 minutes)
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 Soviet film directed by Lev Arnshtam. The film tells story of the friendship between three girls from Petrograd who grow up together and become nurses during the Russian Civil War. The film was released in the USA in 1936 as Three Women.

==Cast==
* Zoya Fyodorova - Zoya
* Yanina Zhejmo - Asya
* Irina Zarubina - Natasha
* Boris Chirkov - Senka
* Boris Babochkin - Andrei
* Nikolay Cherkasov - White Army officer
* Vasili Merkuryev
* Boris Blinov
* Mariya Blyumental-Tamarina
* Pavel Volkov
* Stepan Kayukov
* Stepan Krylov
* Boris Poslavsky
* Serafima Birman
* I. Antipova
* D. Pape
* Varvara Popova
* Vera Popova		
* Pavel Sukhanov

==External links==
* 

 
 

 
 
 
 
 
 
 

 