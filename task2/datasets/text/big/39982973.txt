Meet Me in Montenegro
{{Infobox film
| name           = Meet Me in Montenegro
| image          = 
| alt            = 
| caption        = 
| director       = Alex Holdridge and Linnea Saasen
| producer       =     Ineke Hagedorn    Paul Young   Peter Principato   Alex Holdridge  Linnea Saasen   Seth Caplan  Chris Aagaard
| writer         = Alex Holdridge   Linnea Saasen
| starring       = Rupert Friend   Linnea Saasen   Jennifer Ulrich   Alex Holdridge 
| score         = Stephen Coates The Real Tuesday Weld Robert Murphy
| editing        = 
| studio         =  Good Viking Productions
| distributor    = The Orchard (North America)
| released       =   (Festival Release) 
| runtime        = 90 minutes
| country        = Germany   United States   Norway
| language       = English
| budget         = 
| gross          = 
}}

Meet Me in Montenegro is a globe-trotting romance written and  directed by Alex Holdridge and Linnea Saasen.  It had its premiere at the Toronto International Film Festival in September 2014.   
The film is being distributed by The Orchard and will have its North American Theatrical release in Summer 2015.

== Principal cast ==
* Rupert Friend as Stephen
* Jennifer Ulrich as Federieke
* Alex Holdridge as Anderson
* Linnea Saasen as Lina

With supporting cast including:
* Deborah Ann Woll as Wendy
* Stuart Manashil as Sam
* Ben Braun as Patrick
* Mia Jacob as Katherine 
* Lena Ehlers as Pregnant co-worker

== Production ==
The film has been shot in Berlin, Los Angeles, London and Montenegro.  Shooting in each country ran between November 2011 to June 2014. Post Production was done in Berlin.  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 