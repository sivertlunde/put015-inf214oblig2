Chitrasutram
{{Infobox film
| name           = Chitasutram (The Image Threads)     
| image          = File:Chitrasutram.jpg
| image_size     =
| caption        = Film poster
| director       = Vipin Vijay
| producer       = Altaf Mazid, Zubeen Ahmad, Sushanta Roy
| banner         = Unknown
| story          = M. Nandakumar  
| starring       =
| cinematography = Shahnad Jalal
| screenplay     = Vipin Vijay
| sound          = Subhadeep Sengupta
| editing        = Debkamal Ganguly
| runtime        = 104 Minutes 
| country        = India
| language       = Malayalam
| budget         =
| gross          =|
| banner         = unknown
}}

Chitrasutram (The Image Threads) is a 2010 Malayalam film, written & directed by Vipin Vijay, produced by Altaf Mazid, Zubeen Ahmad, Sushanta Roy under the banner of Unknown Films. The film was also supported by Hubert Bals Film Fund, Rotterdam, Goteborg Film Fund,  Goteborg, Sweden and the Global Film Initiative, US.

== Synopsis ==
A computer teacher, his black-magician grandfather and a cyber-creature—a series of pre-destined rendezvous, both online and offline, over the shreds of mnemonic time and space, at the cleavages of various parlors of subculture—finally the narrative images of the computer screen are drained off from the color and the texture, the images collapse down to a mere pulsating pixel, potentially to start another cycle of the story once again.

== Cast ==
* Sandeep Chatterjee
* Reghoothaman
* Athira
* Arundathi Singha
* Gopalan
* Aadarsha Kumar

==Major Awards Won==
* International Film Festival of Kerala	: Hassan Kutty Award for the Best Debut Director.
* National Film Award 2010	: Best Sound Design.
* Kerala State Award 2010: Special Jury Award for the Director | Best Cinematographer | Best Sound Recordist.
* Padmarajan Puraskaram 2010: Best Director | Best Script Writer.
* John Abraham Award 2010: Special Jury Award for the Director.

==Film Festival Screenings==
* International Film Festival Rotterdam 2011.(Tiger Awards Competition)
* International Film Festival of Kerala.
* Göteborg International Film Festival, 2011		
*  São Paulo International Film Festival
* South Asian International Film Festival in New York, 2010
*  10th Asian Film Festival of Dallas- 
* 11th International Film Festival, Wroclaw. Poland- 2011
* International Film festival, Vladivostok, Russia.
* International Documentary Film festival; Copenhagen

==Reception==
Film Critic Dustin Chang witting on the film said ” Director Vipin Vijay and his cinematographer Shehnard Jalal often distinguish, then blur the boundaries between the past and present, technology and nature, reality and fantasy, tangible and intangible. Devoid of any visible narrative, The Image Thread is unlike any film Ive ever seen. It is more like a visual essay than a film. To enjoy it, you have to give in to its luscious visuals to wash over you. Calming and hypnotic, its literally one of the best films to meditate on. 

==References==
;Notes
 

==External links==
* http://www.filmfestivalrotterdam.com/professionals/films/chitra-sutram/
* http://adimphukan.blogspot.in/2011/01/chitrasutram-image-threads-saturnine.html
* https://www.festivalscope.com/film/the-image-threads
* http://www.nowehoryzonty.pl/film.do?id=5099&ind=idCyklu%3D326%26page%3D0%26typDcykl%26edycjaFest%3D11%26dzien%3D22
* http://kafila.org/2011/02/03/chitrasutram-post-modern-cinema/
* http://twitchfilm.com/reviews/2010/10/saiff-2010-the-image-threads-chitra-sutram-review.php

 
 