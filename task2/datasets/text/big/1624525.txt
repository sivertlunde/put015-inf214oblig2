The Krays (film)
 
 
{{Infobox film
| name           = The Krays
| image          = The Krays UK poster.jpg quad format film poster
| director       = Peter Medak
| producer       = Dominic Anciano Ray Burdis
| writer         = Philip Ridley Martin Kemp Tom Bell
| music          = Michael Kamen Alex Thomson
| editing        = Martin Walsh
| studio         = Fugitive Features
| distributor    = Rank Film Distributors  
| released       =  
| runtime        = 119 minutes
| country        = United Kingdom
| language       = English
}}
 The Krays.    The film was written by Philip Ridley and directed by Peter Medak. 

==Synopsis==
The film charts the lives of the Krays from childhood, paying particular attention to how they were very close to their doting mother (Whitelaw) and also the relationship between the twins, with Ronald (Gary Kemp) the more dominant and violent twin Reginald (Martin Kemp) committing acts of violence primarily at the behest of his brother.

==Main cast==
 
* Billie Whitelaw as Violet Kray Tom Bell as Jack McVitie
* Gary Kemp as Ronnie Kray Martin Kemp as Reggie Kray
* Susan Fleetwood as Rose
* Charlotte Cornwell as May
* Kate Hardie as Frances
* Avis Bunnage as Helen
* Alfred Lynch as Charlie Kray, Snr.
* Gary Love as Steve
* Steven Berkoff as George Cornell
* Jimmy Jewel as Cannonball Lee
* Barbara Ferris as Mrs. Lawson
* Victor Spinetti as Mr. Lawson
* John McEnery as Eddie Pellam
* Norman Rossington as Shopkeeper Michael Balfour as Referee
* Jimmy Flint as Perry
* Sean Blowers as Chris Ripley
* Murray Melvin as Newsagent
* Terence Dackombe as Thug
* Sadie Frost as Sharon Pellam Stephen Lewis as Policeman
* John H. Stracey as Boxer
* Angus MacInnes as Palendri
* Vernon Dobtcheff as Teacher
 

==Awards==
*Nominee Best Supporting Actress - BAFTA (Billie Whitelaw)
*Winner Best Film - Evening Standard British Film Awards (Peter Medak)
*Winner Most Promising Newcomer - Evening Standard British Film Awards (Philip Ridley)
*Winner Best Actress - International Fantasy Film Awards (Fantasporto) (Billie Whitelaw)
*Nominee Best Film - International Fantasy Film Awards (Fantasporto) (Peter Medak)
*Winner George Delerue Prize for Music - Ghent International Film Festival (Michael Kamen)

==Soundtrack==
The film soundtrack came out on the Parkfield music label and featured:
*Chris Rea – "Bitter Sweet"
*The Zombies – "Shes Not There"
*Jimmy Jewel – "Balling the Jack"
*Matt Monro – "Walk Away"

==References==
 

==External links==
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 