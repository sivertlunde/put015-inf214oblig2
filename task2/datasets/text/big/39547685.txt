Wild Horse Mesa (1947 film)
{{Infobox film
| name           = Wild Horse Mesa
| image          = Wild Horse Mesa 1947 Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Wallace Grissell
| producer       = Herman Schlom
| screenplay     = Norman Houston
| based on       =  
| starring       = {{Plainlist|
* Tim Holt
* Nan Leslie
* Richard Martin
}}
| music          = Paul Sawtell
| cinematography = Frank Redman
| editing        = Desmond Marquette RKO Radio Pictures
| distributor    = RKO Radio Pictures 
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Wallace Grissell and starring Tim Holt, Nan Leslie, and Richard Martin. Written by Norman Houston based on the novel of the same name by Zane Grey, the film is about two cowboys who go to work for a rancher and his beautiful daughter. Together they search for wild horses. When they find the horses, a rival rancher offers to purchase them, but during the transaction he murders the good rancher. The rival rancher is soon killed by one of his own men, and he in turn is killed by the wild horse who is the leader of the herd. Produced by RKO Radio Pictures, Wild Horse Mesa was released on November 13, 1947 in the United States. 

It was the eighth and last of a series of Zane Grey novels filmed by RKO. The book had been previously filmed in 1925 and 1933. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p222 

==Cast==
* Tim Holt as Dave Jordan
* Nan Leslie as Sue Melhern
* Richard Martin as Chito Rafferty
* Tom Keene as Hod Slack
* Jason Robards Sr. as Pop Melhern
* Tony Barrett as Jim Horn
* Harry Woods as Jay Olmstead
* William Gould as Marshal Bradford
* Robert Bray as Tex
* Dick Foote as Rusty
* Frank Yaconelli as Clemente

==References==
 

==External list==
*  
*  
*  

 
 
 
 
 


 