Diary of a Mad Black Woman
{{Infobox film
| name           = Diary of a Mad Black Woman
| image          = Diaryofamadblackwoman poster.jpg
| caption        = Theatrical release poster
| director       = Darren Grant
| producer       = Reuben Cannon
| writer         = Tyler Perry
| based on       =  Steve Harris Shemar Moore Tamara Taylor Tiffany Evans Cicely Tyson Tyler Perry
| music          = Camara Kambon
| cinematography = David Claessen
| editing        = Terilyn A. Shropshire Lionsgate Black BET Pictures
| distributor    = Lionsgate
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $5.5 million 
| gross          = $50.7 million 
}} romantic comedy-drama play of the same name. It is Perrys debut feature film, and the first entry in the "Madea" franchise. Directed by Darren Grant, the film was released in the US on February 25, 2005.  It is the only Tyler Perry scripted film not directed by Perry himself.

The sequel, Madeas Family Reunion, was released on February 24, 2006.

==Plot==
Helen McCarter (  truck with Charles kicking her out of the house in favor of Brenda (Lisa Marcos), his young mistress, the mother of his two children. 
 Madea (Tyler Perry), who takes her in and helps her get back on her feet, much to the dismay of Madeas brother Joe (Perry). Joes son and Madeas nephew Brian (Perry) acts as Madea and Helens attorney at trial when Charles and Brenda catch the two breaking into and vandalizing the mansion (Madea rammed her car into the security gate, took a chainsaw to some of the furniture, and helped Helen tear up Brendas clothes).

Since Madea is a repeat offender, Judge Mablean Ephriam places her under house arrest and sets a $5,000 property or cash bond for Helen; meanwhile, Brian gets to the last straw with his drug-addicted wife Deborah (Tamara Taylor) and kicks her out of their home. Helen learns to grow through her pain and is eventually ready to move on. Despite their rocky first encounter, she explores a second chance at love with Orlando (who was only driving that U-Haul as a favor for a friend). 

Their relationship blooms over the course of many months. Meanwhile, Jamison Milton Jackson (Gary Anthony Sturgis) tells Charles to be his defense attorney in his upcoming trial for shooting an undercover cop during a drug deal—and possibly bribe the judge in his favor. This forces the revelation that Charles received his money through drug deals and by buying off judges.

During their divorce-court session, Helen decides to let Charles keep all the money and property provided that he pay Brians attorney fees. She also wants Charles to pay for her mothers stay in the nursing home, since he was the one who forced her to put her mother there in the first place. Charles happily and smugly agrees. Charles tactics turn out to be futile as he loses the shooting case with the jurors finding Jamison guilty. As the bailiff leads the disgruntled Jamison out of the courtroom, Jamison snatches up the bailiffs gun and shoots Charles for failing to get him acquitted. 

Later, Orlando proposes to Helen, promising to take care of her and to love her forever. But before Helen can respond, she sees the shooting on the news and races to the hospital with Brian, where they run into Brenda. The doctor informs them that Charles was shot in the spine and could be paralyzed for life. When he asks if they should resuscitate him, Brenda quickly chooses to let him perish but Helen, who is legally still Charles wife, tells the doctors to do everything they can for him.

Charles recovers, returns home with Helen, and resumes his verbal abuse of her. But Helen has had enough and retaliates for years of abuse in a few days. It is revealed that during Charles hospital stay, Brenda cleaned out his bank account and left him, taking the children. Their maid, Christina, left as well when Brenda left no money to pay her, and all of Charles friends and connections have turned their backs on him. Helen meets with Orlando and they argue when he learns she has moved back in with Charles and is looking after him. Despite Helens offer to remain friends, he angrily storms out of the diner. 

Charles finally realizes his mistakes and understands that Helen was the only one who truly cared about him, and he apologizes sincerely to her and becomes a kinder and changed man. She tends to him through the grueling process of his recovery, and eventually he begins to walk again during an emotional scene in church, in which Deborah, now clean-and-sober, reconciles with Brian and rejoins her family.

Charles hopes he and Helen can start over, but during a family dinner she hands him her wedding ring and signed divorce papers and tells him she will always be his friend. She goes in search of Orlando, asks him to propose to her again, and when he does, she accepts. Orlando picks Helen up and happily carries her out of the factory as the screen fades to black.

==Cast==
* Kimberly Elise as Helen Simmons-McCarter  Steve Harris as Charles McCarter 
* Shemar Moore as Orlando 
* Cicely Tyson as Myrtle Simmons
* Lisa Marcos as Brenda
* Tamara Taylor as Debrah Simmons 
* Tiffany Evans as Tiffany Simmons 
* Tyler Perry as Madea (Mabel Simmons)|Madea, Joe Simmons, and Brian Simmons
* Gary Anthony Sturgis as Jamison Milton Jackson
* Tamela Mann as Cora Simmons Judge Mablean Ephriam as herself

==Music==
The soundtrack was released by Motown Records on April 19, 2005.
{{Track listing
| headline        = Track listing
| extra_column    = Performer(s)
| writing_credits = no
| title1          = Purify Me
| length1         = 4:07
| extra1          = India.Arie
| title2          = Sick & Tired Monica
| length2         = 4:19
| title3          = Different Directions
| extra3          = Angie Stone
| length3         = 3:39
| title4          = Things I Collected
| extra4          = Tamia
| length4         = 5:37
| title5          = I Wanna Swing
| extra5          = Cheryl Pepsii Riley
| length5         = 2:25
| title6          = I Wanna Love Again
| extra6          = Natalie Cole
| length6         = 4:44
| title7          = Fallen in Love
| extra7          = Darlene McCoy
| length7         = 4:28
| title8          = Aint It Funny
| extra8          = Heather Headley
| length8         = 4:03
| title9          = One of Us
| extra9          = Cheryl Pepsii Riley
| length9         = 3:31
| title10         = I Wanna Be Free
| extra10         = Patti Labelle
| length10        = 4:56
| title11         = Father, Can You Hear Me
| extra11         = Cheryl Pepsii Riley, Tamela J. Mann, Terrell Carter, & Anya Washington
| length11        = 4:12
| title12         = Take It to Jesus
| extra12         = Tamela J. Mann
| length12        = 2:57
}}

==Reception==

===Critical reception===
Diary of a Mad Black Woman received mostly negative reviews. 
Rotten Tomatoes gives the film a score of 16% based on 112 reviews. The sites consensus reads "Tyler Perrys successful play cant make the move to the screen; this mix of slapstick, melodrama and spirituality lacks a consistent tone." 
Metacritic gives the film a weighted average score of 36% based on reviews from 30 critics, which the site considers "generally unfavorable".  Roger Ebert gave the film one out of four stars, arguing that the Madea character "is not remotely plausible   not merely wrong for the movie, but fatal to it." However, Ebert also wrote that Elise, Tyson and Harris offered effective performances and that " heres a good movie buried beneath the bad one." 

===Box office===
On its opening weekend, the film arrived at #1 with $21.9 million. The film grossed an estimated $50.6 million in the United States and Canada with an additional $19,000 internationally for an estimated worldwide total of $50.7 million.   

===Controversy===
In early 2008, playwright Donna West filed suit against Perry, contending that he stole material from her 1991 play, Fantasy of a Black Woman. Veronica Lewis, Perrys attorney, said there was no need for her client to appropriate the work of others. 

On December 9, 2008, the case was tried before Judge Leonard Davis in the United States District Court for the Eastern District of Texas. The jury returned an 8–0 verdict in favor of Perry. 

===Difference from the play===
 
* Helen and Charles have been married for 18 years in the movie, but in the play they were married for 20 years.
* In the play, during their divorce, Helen keeps the house and $2,000 a month, but in the movie he throws her out of the house with no money for herself.
* When Charles got paralyzed in the play, Helen didnt want to see him and said that he got what he deserved. In the movie, she and Brian went to see him.
* The characters in the play, Angelo, Willie, and Daddy Charles dont exist. Daddy Charles may exist but was put in a home prior to the movie. Joe and Miltred acted similarly.
* In the movie, Myrtle was put in a home prior to the movie, but in the play she hasnt been put in a home and she visits Helen now and then (due to her being played by Tamela Mann in the play version)
* In the play, Charles and Helen got back together in the end, but in the movie even though she forgave him, she still divorced him and presumably marries Orlando, having accepted his proposal (even though he didnt ask her to marry him in the play).

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 