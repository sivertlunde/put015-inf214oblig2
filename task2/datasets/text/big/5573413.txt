The Civilization of Maxwell Bright
 
  romance film starring Patrick Warburton, Marie Matiko, Eric Roberts, Leland Crooke and Jennifer Tilly that has seen success on the arthouse movie circuit. It has won awards at the WorldFest Houston, New York VisionFest, Florida Film Festival, Boulder International Film Festival, and Beverly Hills Film Festival.  It is about a man who obtains a mail-order bride, with unexpected results. The director is David Beaird.

==Synopsis==
After a series of bad relationships, the last one ending with his lover injuring him with a   from China.

Mai Ling, the Chinese woman, arrives after six weeks.  She is perfect, and even makes his chaotic house into a welcoming home.  But Max reacts by treating her like a prostitute, and humiliates her in front of friends.  At this point Mai Ling reveals that she is really a Buddhist nun who took her sisters place.  Max has an epiphany, and recognises Mai Ling as a deeply spiritual and moral woman.

They fall in love, and their relationship grows amid mutual respect.  Mai Ling attempts to awaken Maxs buddha-nature, and Max introduces Mai Ling to the luxuries of modern American living.
 terminal cancer.  Mai Ling supports him and helps him to prepare for death, and he comes at last to explore his spiritual side and to make his peace with those he has harmed.

When Max dies, Mai Ling resumes her life as a nun.

==Cast==
*Patrick Warburton as Maxwell Bright
*Marie Lan Matiko as Mai Ling
*Eric Roberts
*Leland Crooke
*Jennifer Tilly
*Simon Callow
*Terence Knox as Officer Riggs
*Nora Dunn
*Carol Kane
*Missi Pyle

==External links==
*  

 
 
 
 
 
 
 


 