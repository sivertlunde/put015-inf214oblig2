Mickey (2004 film)
{{Infobox film
| name        = Mickey
| image       = Mickey_movie.jpg Hugh Wilson
| producer    = John Grisham
| writer      = John Grisham Mike Starr Michelle Johnson Hugh Wilson John Grisham
| music       = Guy Moon Craig Sharmat
| distributor = Anchor Bay Entertainment Mickey Productions Slugger Pictures
| released    =  
| runtime     = 90 min
| country     = United States English
| budget      = $6 million
}}
 2004 Cinema American List baseball drama Hugh Wilson, and written by best-selling novelist John Grisham.
 Colonial Heights, Richmond, Virginia|Richmond, and Petersburg, Virginia, and also South Williamsport, Pennsylvania, home of baseballs Little League World Series (LLWS).

Grisham played Little League in his home state of Mississippi. He wrote the first   for Mickey in 1995, inspired by his Little League experience as a Coach (sport)|coach.  Grisham and director Wilson live in the Virginia area where much of the filming took place. 

Mickey was only the second film, after 1994s Little Giants, to receive permission to use the Little League trademarks.

== Plot ==
Tripp Spence (Harry Connick, Jr.) is a widowed Maryland-based lawyer who becomes the focus of an intensive IRS investigation regarding false bankruptcy claims he filed during his wifes fatal illness.  Realizing his case against the inevitable criminal charges is hopeless, he takes his 13-year-old son Derrick (Shawn Salinas), who loves playing Little League baseball and is competing in his final year of eligibility due to age restrictions, and flees from the investigation, moving out west to Las Vegas, Nevada. Through a corporate connection, Tripp acquires new identities for the two of them, with Tripp becoming Glen Simon Ryan and Derrick becoming Michael "Mickey" Jacob Ryan, whose fictional backstory is that they recently moved into town from Fort Lauderdale, Florida.

Derricks new identity makes him a year younger, which enables him to play another year of Little League, an endeavor both of them look forward to since their spontaneous move forced Derrick to miss his All-Star teams participation in the official Little League qualifier tournaments. Tripp researches the local Little League operations and discovers that the most successful leagues top team is coached by Tony (Mike Starr). Tripp contacts Tony and convinces him to give Mickey a private tryout session by reciting the Fort Lauderdale story (Florida is known as a major "hotzone" for youth baseball). Tony is impressed with Mickeys pitching velocity and ultimately drafts Mickey once the real tryouts roll around. Glen also begins dating Patty, who works at Mickeys school.

A full year of physical development ahead of the rest of the players, Mickey displays outstanding abilities on the field, quickly emerging as the leagues top pitcher and hitter, which unanimously earns him a spot on the All-Star team. Glen is initially satisfied, but begins to worry as Mickeys team continuously ascends their way through the district and regional brackets, which attracts considerable media attention as they earn a spot at the Little League World Series in Williamsport, Pennsylvania. Paranoid that the media attention will ultimately give away their location to the IRS, Glen tries to convince Mickey to quit playing, but Mickey refuses to let his new friends down, stating that they knew the risks from the beginning.

After pitching a perfect game against the Gulf States team, Mickey removes the faux glasses that were used as a disguise (despite Glens warnings not to), which allows the IRS agents pursuing Glen and Mickey to positively identify them. Mickeys team ultimately reaches the Little League World Series Championship game as the U.S. representative against a highly publicized team from Cuba, the International finalist. The IRS agent in charge of the investigation approaches Glen, telling him he will be arrested following the tournament. During the game, a politician who had vouched to ban the Cuban team from participating in the tournament passes on information to LLWS officials of ineligible players on both the Cuban and U.S. teams. It is found that the Cuban team, which is supposed to be strictly from Havana, is made up of All Star players from various island teams, and Mickey and Glens true identities are exposed.

Following the game, Glen is arrested and a press conference is called. Tripp/Glen reveals the truth about Derrick/Mickey. The LLWS Commissioner announces that both teams participation in the tournament will be forfeited, and that the semifinal losers will play the next day for the championship. Derrick confesses the truth to his coach and teammates, but his friends forgive him because they never wouldve made it as far as they did without him. Derrick visits Tripp at the local precinct, where they say goodbye. After a year in prison, Tripp is released, and in the final scene he throws a baseball to Derrick that hits the windshield of Pattys car.

== Cast ==
*Harry Connick, Jr. as Glen Ryan (Tripp Spence) Mike Starr as Tony Michelle Johnson as Patty Hugh Wilson as Munson
*John Grisham as Commissioner
*Shawn Salinas as Mickey Ryan and/or(Derrick Spence)
*Mark Joy as Seeger
*Alexander Roos as Pudge
*Danny Bell as Griff
*Jason Kypros as Pete Bracey
*Gill Baker as Peggy
*Stan Kelly as Prater
*Don Yesso as Cardinals Coach
*Richard Fullerton as President Sloan
*Peter Gil as Senator Martinez
*Tom Trigo as Jack Fernandez
*Jon-Michael Moralez as Cuban Coach

==Awards and nominations== WorldFest Film Festival, 2004

==Box office==
According to the International Movie Data Base, the film Mickey had a budget of $6 million but earned less than $300,000.

== External links ==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 