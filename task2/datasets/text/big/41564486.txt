Kung Fury
{{Infobox film
| name           = Kung Fury
| image          = Kung Fury Title Card.jpg
| caption        = Trailer title card
| director       = David Sandberg
| producer       =  
| writer         = David Sandberg
| narrator       = Adrian Ciprian
| starring       = {{Plain list |
* David Sandberg
* Leopold Nilsson
* Andreas Cahling
* Jorma Taccone
* Per-Henrik Arvidius
}}
| music          =  
| cinematography = 
| editing        = 
| released       =  
| runtime        = 30 minutes
| studio         =  
| distributor    = 
| country        = Sweden
| language       = English
| budget         = 
| gross          = 
}} crowdfunded through Kickstarter with pledges reaching US$630,019, exceeding the original target goal of US$200,000, but falling short of the feature film goal of US$1 million.    It has been selected to screen in the Directors Fortnight section at the 2015 Cannes Film Festival.   
==Plot==
Miami-Dade Police Department detective and martial artist Kung Fury timetravels from the 1980s to World War II to kill Adolf Hitler, a.k.a. "Kung Führer", and avenge his friends death at the hands of the Nazi leader. An error in the time machine sends him further back to the Viking Age. With the help of a female Viking and the Norse God Thor, Kung Fury continues his time travels in order to put an end to the Third Reich once and for all.

==Cast==
* David Sandberg as Kung Fury
* Helene Ahlson as Katana
* Leopold Nilsson as Hacker Man
* Jorma Taccone as Adolf Hitler 
* Andreas Cahling as Thor
* Per-Henrik Arvidius as Chief
* Magnus Betnér
* Björn Gustafsson
* Adrian Ciprian as the Narrator

==Production==
David Sandberg is a Swedish filmmaker with years of experience in directing television commercials and music videos. In 2012, he quit the commercial directing business and focused on writing a script for an action comedy film set in the 1980s, inspired by action films of that era. Sandberg initially spent US$5,000 on producing and shooting footage with his friends; most of which became the first trailer. 
 crowdfund the green screen had been filmed using a Canon EOS 5D and a Sony FS700, but additional funding was required for post-production.       

The Kickstarter project ended on January 25, 2014 with US$630,019 pledged by 17,713 backers. 

===Filming===
Working on a very limited budget, Sandberg shot the majority of the film at his office in Umeå, Sweden, using digital effects to replicate the streets of Miami. As he could only afford one police uniform, he filmed the police precinct scene by shooting each extra separately and compositing them in the scene. 

On July 30, 2014, Sandberg announced that he and his crew had begun filming new footage, with the 30 backers who pledged to be in the film as extras. Filming was also done in Stockholm for additional scenes and stunts. 

===Release===
On April 11, 2015, Sandberg announced that they are aiming for a release date between May 22nd and May 29th. 

===Soundtrack===
The soundtrack score is composed by Swedish electronica artists Lost Years and Mitch Murder. 

Singer David Hasselhoffs music video of the theme song "True Survivor" debuted on April 16, 2015.  It begins with an altered scene from the trailer where instead of the police car exploding in mid air, from thug gunfire, Hasselhoff turns the tables and fires back at the thugs from the airborne police car. The video features Hasselhoff singing alongside a white Lamborghini Countach, clips from the film, as well as an end scene of himself and Kung Fury (Sandberg) riding a Tyrannosaurus; Hasselhoff replaces the Viking Katana (Helene Ahlson), who appeared with Kung Fury on the Tyrannosaurus in the original trailer.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 