East Side Kids (film)
{{Infobox Film
| name           = East Side Kids|
| image          = East Side Kids (1940 film) poster.jpg|
| image size     = 190px
| caption        = DVD Cover|
| director       = Robert F. Hill|
| producer       = Sam Katzman| 
| writer         = William Lively| Leon Ames Dennis Moore Joyce Bryant   East Side Kids
| music          = Johnny Lange  Lew Porter
| cinematography = Art Reed |
| editing        = Earl Turner (film editor) |
| distributor    = Monogram Pictures Corporation |
| released       = February 10, 1940 (USA)|
| runtime        = 62 mins.|
| language = English|
| country = United States|
}}
East Side Kids is a 1940 film and the first in the East Side Kids film series.  It is the only one not to star any of the original six Dead End Kids.

==Overview==
The film was released by producer Sam Katzman. This was also his first project at Monogram Pictures, which he joined shortly after the folding of his company Victory Pictures.

==Plot== Leon Ames), Dave OBrien) Dennis Moore), Ted Adams)s, basement. Feeling threatened by Pat, Morris schemes to discredit the policeman by posing as a businessman who wants to hire Pats boys to distribute advertising leaflets. Unknown to Pat, Morris places bogus five dollar bills in the pay envelopes, and when the boys are caught passing fake money, Pat is implicated in the counterfeiting scheme. To prove his innocence, Pat takes to the streets, and Danny, still unaware of Morris involvement in the counterfeiting ring, agrees to deliver a suitcase for him to May. A policeman follows Danny to Mays apartment, where they are greeted by Mileaway, who kills the policeman and takes Danny hostage. As they drive across town, Danny learns that it was Mileaway who killed the treasury agent and framed Knuckles. Pat tracks down Mileaways car, and in the ensuing chase, Mileaway escapes and kills Schmidt. Pat and the kids chase Mileaway to a rooftop, where Dutch (Hal E. Chester), Dannys friend, struggles with Mileaway. When they both fall to the sidewalk, Dutch is killed; but Mileaway lives to confess to the agents murder, and all ends happily as both Knuckles and Pat are exonerated.

==Cast==

===The East Side Kids===
*Hal E. Chester as Fred Dutch Kuhn
*Harris Berger as Danny Dolan
*Frankie Burke as Skinny Jack Edwards as Algernon Mouse Wilkes
*Donald Haines as Peewee
*Eddie Brian as Mike
*Sam Edwards as Pete

===Remaining Cast=== Leon Ames as Pat ODay Dennis Moore as Milton Mileway Harris
*Joyce Bryant as Molly Dolan
*Vince Barnett as Whisper Dave OBrien as Knuckles Dolan Ted Adams as Schmidt
*Maxine Leslie as May Robert Fiske as Cornwall Jim Farley as Police Captain Moran
*Alden Stephen Chase as Detective Joe
*Fred Hoose as Mr. Wilkes
*Eric Burtis as Eric
*Frank Yaconelli as Tony the grocer

==Crew==
*Directed by: Robert F. Hill
*Screenplay by: William Lively
*Produced by: Sam Katzman
*Music composed by: Johnny Lange, Lew Porter
*Film editing by: Earl Turner
*Cinematography by: Arthur Reed
*Production Management: Ed W. Rote
*Assistant Director: Glenn Cook Glen Glenn

==External links==
*  

 

 
 
 
 
 
 
 