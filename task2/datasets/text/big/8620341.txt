An Unseen Enemy
{{Infobox film name           = An Unseen Enemy image          = An Unseen Enemy.jpg caption        = director       = D. W. Griffith producer       = writer         = Edward Acker starring  Harry Carey Elmer Booth Robert Harron music          = Robert Israel (new score) cinematography = Billy Bitzer|G. W. Bitzer editing        = distributor    = General Film Company released       =   runtime        = 17 minutes country        = United States language       = Silent with English intertitles budget         =
}}
  Biograph Company short silent film directed by D. W. Griffith, and was the first film to be made starring the actresses Lillian Gish and Dorothy Gish.  A critic of the time stated that "the Gish sisters gave charming performances in this one-reel film".  The film was shot in Fort Lee, New Jersey where early film studios in Fort Lee, New Jersey#Americas first motion picture industry|Americas first motion picture industry were based at the beginning of the 20th century.   

==Cast==
* Elmer Booth
* Lillian Gish
* Dorothy Gish Harry Carey Sr.
*Robert Harron
*Grace Henderson - The Unseen Enemy
*Charles Hill Mailes -  Walter Miller -
*Henry B. Walthall -
*Adolph Lestina -
*Antonio Moreno - Man on Bridge, flagging car
*Erich von Stroheim - Man in Straw Hat Dancing at Lobby Desk

==Plot==
A physicians death orphans his two adolescent daughters. Their older brother is able to convert some of the doctors small estate to cash. It is late in the day, and with the banks closed he stores the money in his fathers household safe. The slatternly housekeeper, aware of the money, enlists a criminal acquaintance to help crack the safe. They lock the daughters in an adjacent room, and the drunken housekeeper menaces them by brandishing a gun through a hole in the wall. The resourceful girls use the telephone to call their brother who has returned to town. He gets the message and organizes a rescue party. 

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography

==References==
 

==External links==
 
* 
*   on YouTube
* 
*An Unseen Enemy available for free download from  

 

 
 
 
 
 
 
 
 
 
 