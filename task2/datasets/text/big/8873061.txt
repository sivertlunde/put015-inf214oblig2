Feeling from Mountain and Water
{{Infobox film
| name = Feeling from Mountain and Water
| image = FFMWater.jpg
| caption =
| imdb_rating =
| director = Te Wei
| producer =
| writer =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1988
| runtime = 20 mins
| country = China
| language = none
| budget =
| preceded_by =
| followed_by =
| mpaa_rating =
| tv_rating =
}}

Feeling from Mountain and Water (Chinese: 山水情;   short film produced by Shanghai Animation Film Studio under the master animator Te Wei.  It is also referred to as Love of Mountain and River and Feelings of Mountains and Waters.

==Background==
The film does not contain any dialogue, allowing it to be watched by any culture.   The only noises are the sound of the wind or other earthly elements.  The film is considered a masterpiece at the artistic level, since it was essentially a landscape painting in motion.   Artistically, it uses a Shan shui painting style throughout.

==Story==
The story is about an impoverished elderly scholar and a young girl who cares for him briefly in return for guqin lessons.

==Awards==
* Won the Best animated film prize at the Golden Rooster Awards in 1989.

==References==
 

==External links==
*  
*   
*  

 

 
 
 
 
 
 
 
 


 
 