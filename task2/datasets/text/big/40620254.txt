Major Chandrakanth (1966 film)
 
{{Infobox film
| name           = Major Chandrakanth
| image          = 
| image_size     =
| caption        = 
| director       = K. Balachander
| producer       = A. V. Meiyappan
| writer         = K. Balachander
| narrator       =
| starring       =  
| music          = V. Kumar
| cinematography = S. Maruthi Rao
| editing        = R. G. Gopu
| studio         = AVM Productions
| distributor    = AVM Productions
| released       = 1966
| runtime        = 156 mins
| country        = India Tamil
| budget         =
}} Oonche Log in 1965.

==Plot==
Mohan (Nagesh) is a tailor who lives with his younger sister Vimala (Jayalalithaa). Orphaned at a very young age, Mohan goes through a life of hardship and fulfill all his sisters wish. Womanizer Rajinikanth (A. V. M. Rajan) lures Vimala and cheats on her. Unable to face her brother, she commits suicide. Mohan confronts Rajinikanth and in a fit of rage kills him. A wild search is on by the cops and the fugitive hides in Major Chandrakanth (Major Sundarrajan) house. Mohan is amused and fascinated by Chandrakanth virtues and how he manages to live though he is blind. Mohan reveals why he is on the run from law and ironically the deceased happens to be Chandrakanths youngest son. Chandrakanth feels humiliated for nurturing Rajinikanth and is sorry for Mohans plight. In the end Inspector Srikanth (Muthuraman (actor)|Muthuraman), eldest son of Chandrakanth arrests Mohan along with his father for his hospitality to a criminal.

==Cast==
*Major Sundarrajan as Major Chandrakanth
*Nagesh as Mohan
*R. Muthuraman as Srikanth
*A. V. M. Rajan as Rajinikanth
*Jayalalitha as Vimala

==Soundtrack==
The music composed by V. Kumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 04:12
|-
| 2 || Kalyana Sapadu Podava (Pathos) || T. M. Soundararajan || 01:35
|-
| 3 || Naane Pani Nilavu || P. Susheela || 04:06
|-
| 4 || Netrunee Chinnapappa || T. M. Soundararajan, P. Susheela || 04:01
|-
| 5 || Oru Naal Yaaro || P. Susheela || 03:35
|-
| 6 || Thuninthunil || Seerkazhi Govindarajan || Suradha || 01:25
|}

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 


 