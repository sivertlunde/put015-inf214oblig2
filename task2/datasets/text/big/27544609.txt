The End of the Line (1957 film)
{{Infobox film
| name           = The End of the Line
| image          =
| image_size     =
| caption        = Charles Saunders
| producer       = Guido Coen
| writer         = Paul Erickson
| narrator       = Alan Baxter Barbara Shelley Ferdy Mayne Jennifer Jayne
| music          = Edwin Astley
| cinematography = Walter J. Harvey
| editing        = Tom Simpson
| distributor    = Eros Films
| released       = 1957
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} Charles Saunders Alan Baxter, West London.

==Plot==
An American author living in England (Alan Baxter) gets involved with the wife (Barbara Shelley) of a jewel fence. The wife then convinces the author to rob her husband but soon after the robbery the jewel fence winds up dead.

==Cast== Alan Baxter as Mike Selby  
* Barbara Shelley as Liliane Crawford  
* Ferdy Mayne as Edwards  
* Jennifer Jayne as Anne Bruce  
* Arthur Gomez as John Crawford  
* Geoffrey Hibbert as Max Perrin  
* Jack Melford as Inspector Gates  
* Charles Clay as Henry Bruce  
* Stella Bonheur as Mrs. Edwards

==References==
 

==Sources==
*http://www.blockbuster.com/browse/catalog/movieDetails/56840
*http://movies.tvguide.com/end-line/113943
*http://www.fandango.com/theendoftheline_v90467/summary

==External links==
* 

 
 
 
 
 
 
 

 
 