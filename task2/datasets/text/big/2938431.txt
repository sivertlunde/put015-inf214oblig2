The Car
 
{{Infobox film
| name           = The Car
| image          = The_Car_movie_poster.jpg
| caption        = Promotional poster for American release of The Car
| director       = Elliot Silverstein
| producer       = Marvin Birdt Elliot Silverstein Michael Butler Michael Butler & Dennis Shryack and Lane Slate (screenplay) Elizabeth Thompson
| music          = Leonard Rosenman
| cinematography = Gerald Hirschfeld
| editing        = Michael McCroskey
| distributor    = Universal Studios
| released       =  
| runtime        = 96 minutes
| country        = United States English
| budget         =
| gross          =
}}

The Car is a 1977 thriller film directed by Elliot Silverstein and written by Michael Butler, Dennis Shryack and Lane Slate. The film stars James Brolin, Kathleen Lloyd, John Marley, and Ronny Cox, and tells the story of a mysterious car which goes on a murderous rampage, terrorizing the residents of a small town.

The movie was produced and distributed by Universal Studios, and was influenced by numerous "road movies" of the 1970s including Steven Spielbergs 1971 thriller Duel (1971 film)|Duel and Roger Cormans Death Race 2000.

==Plot==
The film is set in the fictional Utah community of Santa Ynez. Two bicyclists are cycling on the canyon, and a mysterious black car is following them down the road. At the bridge, the car rams them at the back, causing them to fly over the bridge, killing them on the spot.

The local sheriffs office is called to the first of a series of hit and run deaths, apparently caused by the same car that appears heavily customized and has no license plate, making identification difficult. Sheriff Everett Peck (John Marley) gets a lead on the car when it is witnessed by Amos Clemens (R. G. Armstrong) after it runs over a hitchhiker. After the car claims the sheriff as its fourth victim, it becomes the job of Chief Deputy (now Sheriff) Wade Parent (James Brolin) to stop the deaths. During the resulting investigation, an eyewitness to the accident states that there was no driver inside the car.

Despite a police cordon being placed around all roads in the area, the car enters the town and attacks the school marching band as it rehearses at the local show ground. It chases the group of teachers and students, among them Wades girlfriend Lauren (Kathleen Lloyd), into a cemetery. Curiously enough, the machine will not enter onto the consecrated ground as Lauren taunts the purported driver that any of the townsfolk have yet to see. Seemingly in anger, the car destroys a brick gate post and leaves. The police chase the automobile along highways throughout the desert before it turns on them, destroying several squad cars and killing five of Wades deputies in the process. Wade confronts the vehicle and is surprised to see that none of his bullets put a dent on the cars windshield or tires. After trying to open the door (when it is revealed that the car has no door handles), Wade is injured, and the car escapes.

The hunt for the car becomes a personal vendetta for Wade when the automobile stalks and eliminates Lauren by driving straight through her house, right when he is speaking to her over the phone. Wades deputy, Luke Johnson (Ronny Cox), puts forward the theory that it acted in revenge for the insults hurled on it by Lauren and notes it cannot enter hallowed ground. Wade concocts a plan to stop the car by burying it beneath a controlled explosion in the canyons that lie outside of town. After discovering it waiting for him in his own garage, he is forced to carry out his plans post haste. He is pursued by the car into a mountainous canyon area where his remaining deputies have set a trap for the machine, and a final confrontation settles the score with a demonic visage appearing in the smoke and fire of the explosion, shocking the deputies.

The final scenes show Wade refusing to believe what the group saw in the flames, despite Deputy Johnsons insistence about what he saw. The film concludes, in some cuts, with the car prowling the streets of downtown Los Angeles, clearly having survived.

==Main cast==
* James Brolin&nbsp;– Captain Wade Parent
* Kathleen Lloyd&nbsp;– Lauren Humphries
* John Marley&nbsp;– Sheriff Everett Peck
* R. G. Armstrong&nbsp;– Amos Clemens
* John Rubinstein&nbsp;– John Morris
* Kim Richards&nbsp;– Lynn Marie Parent
* Kyle Richards&nbsp;– Debbie Parent
* Doris Dowling&nbsp;– Bertha Clemens
* Ronny Cox&nbsp;– Deputy Luke Johnson

==Critical reception== John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made.  A version of the car appears as one of many famous movie monsters in The Simpsons "Treehouse of Horror XXIV" opening credits created by noted horror director Guillermo del Toro.   

==Production== George Barris. There were four cars built for the film in six weeks. Three were used for stunt work – the fourth was for closeups, etc.  The stunt cars were destroyed during production – the fourth is now in a private collection.

The late  .

The films main theme, heard predominantly throughout, is a reworked, orchestral version of the Dies Irae, also used in "The Screaming Skull" (1958).
 Knight Rider episode "Trust Doesnt Rust", shown at the end when "KARR (Knight Rider)|KARR" is destroyed by driving off a cliff, a glimpse of "The Car" is seen going over the cliff instead.   

==In other languages== French – Enfer Mécanique (lit. Mechanical Hell ) German – Der Teufel auf Rädern (lit. The Devil on Wheels) Italian – La Macchina Nera (lit. The Black Car) Japanese – ザ・カー – Za Kā (Transliterated version of  The Car) Portuguese – O Carro: Máquina do Diabo (Translated in English, The Car: Devils Machine) Norwegian - Døden har fire hjul (lit. The Death Has Four Wheels) Swedish - Djävulsbilen (lit. The Devils Car) Russian - Ад на колёсах (lit. The Hell on wheels)

==See also== Black Cadillac, a 2003 film about a mysterious black Cadillac that stalks three young men as they make their way through the virtually deserted mountain roads of Wisconsin.
*Killdozer! (film)|Killdozer!, a 1974 film about a possessed bulldozer. novel of the same name.
*The Hearse, a 1980 horror movie about a possessed hearse.
*Nightmares (1983 film)|Nightmares, a 1983 movie made up of four separate story segments; the third, "The Benediction", features a traveling priest attacked on the highway by a demonic Four-wheel drive|4x4.
*Maximum Overdrive, a 1986 horror movie, based on the short story Trucks (short story)|"Trucks" by Stephen King.
**Trucks (film)|Trucks, a more faithful 1997 made-for-TV film based on the King short story.
*Wheels of Terror, a 1990 made-for-TV film about a driverless car terrorizing a small Arizona community.

*Phantom Racer, a 2009 Syfy movie about a possessed race car.
*Super Hybrid, a 2011 film about a shape shifting monster that transforms into cars.
*"The Honking", Futurama episode where Bender transforms into a similar looking demonic car.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 