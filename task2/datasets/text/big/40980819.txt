In the Sands of Babylon
{{Infobox film
| name           = In the Sands of Babylon
| image          = 
| caption        = 
| director       = Mohamed Al-Daradji Isabelle Stead Atia Al-Daradji Mohamed Al-Daradji
| writer         = Mohamed Al-Daradji
| starring       = 
| music          = Kad Achouri
| cinematography = Mohamed Al-Daradji, Duraid Munajim
| editing        = Mohamed Al-Daradji 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Iraq
| language       = Arabic
| budget         = $1,200,000
}}
In the Sands of Babylon is a 2013 Iraqi, British, Dutch drama film directed by Mohamed Al-Daradji, Varietys Middle Eastern Filmmaker of the year 2010.

The film was developed through The Sundance Institute and was supported by The Iraqi Ministry of Culture, Baghdad City of Culture of the Arab World 2013, Abu Dhabi Film Festival - Sanad Fund, Creative England, San Sebastian Cinema in Motion.

==Plot==
1991 Gulf War: Ibrahim, an Iraqi soldier, has escaped from Kuwait as the Iraqi Army retreats. Facing the perilous journey home, he must cross the southern desert; a chaotic no-mans land between Saddams Regime and American Cross Fire. Whilst unrest spreads across the country, he is captured by the Republican Guard and cast into Saddam’s infamous prisons, suspected of being a traitor. 2013: In search of answers about the past and Ibrahim’s journey, the Director of the film encounters three people: A photographer with a painful secret, a farmer who hides his scars to forget and an ex-prisoner whos humanity was savagely taken from him. By unravelling the courageous and tragic secrets of these people, the Director seeks to reveal the truth behind Ibrahims story. Through the past and the present, fiction and reality, he revisits a fateful climax in the battlefields of Babylon. As Ibrahim’s fate seems written, the Iraqi people are uprising beyond the prison walls, instilling hope in those held captive, that the freedom they long for beckons.

==Cast==
* Samer Qahtan
* Ameer Jabarah

==Awards==
* Abu Dhabi Film Festival, 2013 winner of the Best Film from the Arab World. 
* San Sebastian Cinema in Motion

==References==
 

 
 
 
 
 
 