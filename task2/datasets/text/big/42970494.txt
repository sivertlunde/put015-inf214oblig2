Girl of the Port
 
{{Infobox film
| name           = Girl of the Port
| image          = 
| alt            = 
| caption        = 
| director       = Bert Glennon
| producer       = William LeBaron
| writer         = Beulah Marie Dix Frank Reicher
| based on       =  
| starring       = Sally ONeil Reginald Sharland Mitchell Lewis Duke Kahanamoku
| music          = 
| cinematography = Leo Tover
| editing        =  RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} romantic film, John Russell, and starred Sally ONeil, Reginald Sharland, Mitchell Lewis, and Duke Kahanamoku.   

==Plot==
Jim, a British Lord, suffers from pyrophobia, a fear of fire, which he developed during the war.  Unable to cope with his condition, he flees civilization, coming to rest in the island paradise of Suva, in Fiji. As he is attempting to drink himself into forgetfulness, he meets Josie, who is a showgirl stranded on the island.  Josie had become friends with Kalita, who talked the owner of the bar into giving Josie a job.  It is in the bar where Jim and Josie meet, and the two develop a liking for one another, which causes McEwen, the local heavy to become jealous.

After McEwen challenges Jim to a fight, which Jim backs away from, causing those around him to believe he has a cowardly streak.  Josie, however, continues to believe in him.  McEwen steps up his animosity towards Jim, taunting him into following McEwen to the nearby island of Benga, where McEwen intends to force Jim to participate in the local custom of fire-walking.  Jim, forced to confront his fear, overcomes it, and passes through the fire pit, after which he defeats McEwen in a fight, and ends up with Josie.

==Cast==
* Sally ONeil&nbsp;— Josie
* Reginald Sharland&nbsp;— Jim
* Mitchell Lewis&nbsp;— McEwen
* Duke Kahanamoku&nbsp;— Kalita
* Donald MacKenzie&nbsp;— MacDougal
* Renée Macready&nbsp;— Enid
* Arthur Clayton&nbsp;— Burke
* Gerald Barry&nbsp;— Cruce
* Barrie ODaniels&nbsp;— Blair
* John Webb Dillon&nbsp;— Cole
* William Burt&nbsp;— Toady
* Hugh Crumplin&nbsp;— Wade

==Notes==
John Russells short story, The Fire-walker, appeared in Far Wandering Men, in 1929. 

==References==
 

 
 
 
 
 
 
 
 