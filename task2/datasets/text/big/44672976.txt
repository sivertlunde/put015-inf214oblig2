How to Steal a Dog
{{Infobox film name           = How to Steal a Dog image          =  director       = Kim Sung-ho producer       = Eom Yong-hun   Lee Seong-hwan writer         = Kim Sung-ho   Shin Yeon-shick based on       =   starring       = Lee Re   Kim Hye-ja  music          = Kang Min-guk cinematography = Kim Hyeong-ju editing        = Lee Jin studio         = Samgeori Pictures distributor    = Little Big Pictures Daemyung Culture Factory released       =   runtime        = 109 minutes country        = South Korea language       = Korean budget         =  gross          =
}}
 based on Barbara OConnor.      

==Plot==
Ten-year-old Ji-so lives in a van with her mother Jeong-hyeon and younger brother Ji-seok. Her father disappeared after their pizza business went bankrupt. When she sees a missing dog poster with a five thousand dollar reward, Ji-so naively believes that that amount of money would be enough to buy her family a house. So she hatches a plan with her friend Chae-rang to find a dog with a rich owner, steal it, then return the dog by pretending to have found it and get the reward. Their target is Wolly, the dog of an old rich lady who owns the restaurant where Ji-sos mom works. While undertaking their "perfect" plan, they befriend Dae-po, a homeless man living in the abandoned building where theyve stashed the dog. But someone else is after Wolly: Soo-young, the old ladys nephew, wholl stop at nothing to gain his aunts inheritance.

==Cast==
*Lee Re as Ji-so
*Lee Ji-won as Chae-rang
*Kim Hye-ja as Old lady
*Choi Min-soo as Dae-po
*Kang Hye-jung as Jeong-hyeon
*Lee Chun-hee as Soo-young
*Lee Hong-gi as Seok-gu
*Lee Ki-young as Director Park
*Jo Eun-ji as Chae-rangs mother
*Tablo
*Kim Won-hyo as Traffic policeman
*Kim Jae-hwa as Homeroom teacher
*Hong Eun-taek as Ji-seok
*Kim Do-yeop as Min-seob
*Sam Hammington

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient !! Result
|- 2015
| 20th Chunsa Film Art Awards 
| Best Director (Grand Prix)
| Kim Sung-ho
|  
|-
|}

==References==
 

==External links==
*   
* 
* 
* 

 
 
 
 