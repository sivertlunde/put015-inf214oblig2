I Live in Grosvenor Square
 
 
{{Infobox film
| name           = I Live in Grosvenor Square
| image_size     = 
| image          = I Live in Grosvenor Square FilmPoster.jpeg
| caption        = 
| director       = Herbert Wilcox
| producer       = Herbert Wilcox
| writer         = Maurice Cowan (story) William D. Bayles Arvid david Nicholas Phipps
| narrator       = 
| starring       = Anna Neagle Rex Harrison Dean Jagger Robert Morley Anthony Collins
| cinematography = Mutz Greenbaum Otto Heller
| editing        = 
| distributor    = Pathé Pictures International Twentieth Century-Fox Film Corporation
| released       = 20 July 1945 (UK)  3 March 1946 (USA)
| runtime        = 130 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
 directed and produced by Herbert Wilcox—a forerunner of his "London films" collaboration with his wife, actress Anna Neagle. The film deploys a tragi-comic plot in a context of US-British wartime co-operation, and displays icons of popular music with the purpose of harmonising relationships on both sides of the Atlantic.  An edited version was distributed in the United States, with two additional scenes filmed in Hollywood, under the title A Yank in London.

==Plot== SSgt John Army Air parliamentary by-election.

On a weekend visit to the dukes estate near Exmoor in Devon, Patterson meets the dukes granddaughter, Lady Patricia Fairfax (Anna Neagle), a corporal in the Womens Auxiliary Air Force, who is Davids childhood sweetheart. After a cool beginning based on cultural misunderstandings, they fall in love. David is unaware of what is happening until the final night before the election, when it becomes clear to him during a party on the estate. The next day, the duke learns that his estate has been appropriated by the American army for a base and that David has lost the election.

When Patterson realizes that Pat and David have long expected to marry, he contrives to obtain medical clearance to go back to combat duty. David and Pat have an ugly showdown over Patterson, only to learn that he has gone back to war. David realizes that Pat still loves Patterson and arranges for them to reunite. Returning from a mission with heavy battle damage, Patterson attempts to help his pilot land their B-17 Flying Fortress at an emergency landing strip at Exmoor, but is killed when the bomber stalls as they manoeuvre to avoid crashing in the village. The duke and his family mourn Patterson at a memorial service in the village church, while David takes off with his paratroop unit to parachute into France on D-Day.

==Cast==
* Anna Neagle as Lady Patricia Fairfax
* Rex Harrison as Major David Bruce
* Dean Jagger as Staff Sergeant John Patterson
* Robert Morley as the Duke of Exmoor
* Nancy Price as Mrs. Wilson Dame Irene Vanbrugh as Mrs. Mildred Catchpole
* Jane Darwell as Mrs. Patterson
* Elliott Arluck as Sergeant Benjie Greenburg 
* Walter Hudd as Vicar  
* Edward Rigby as Innkeeper  
* Cecil Ramage as Trewhewy  
* Irene Manning as Herself - U.S.O. Singer  
* Francis Pierlot as Postman  
* Aubrey Mallalieu as Bates  
* Michael Shepley as Lieutenant Lutyens
 John Slater, David Horne, Robert Farnon and Carroll Gibbons.  

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 