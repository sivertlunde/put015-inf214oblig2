Last Train Home (film)
{{Infobox film
| name           = Last Train Home
| image          = Last-train-home-lixin-fan.jpg
| caption        = Theatrical release poster
| producer       =  
| director       = Lixin Fan
| writer         = 
| starring       =  
| music          = Olivier Alary
| cinematography = Lixin Fan
| editing        = Yung Chan
| studio         = EyeSteelFilm
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 85 minutes
| country        = Canada
| language       = Sichuanese Mandarin
| gross          = $288,328(USA)
}}
 Daniel Cross and Mila Aung-Thwin of EyeSteelFilm. It won the Best Documentary Feature at 2009 IDFA and has been distributed by Zeitgeist Films in the US.

==Synopsis== New Years holiday. This exodus is the worlds largest human migration.

Working over several years, director Lixin Fan travelled with one couple who has embarked on these annual treks for almost two decades. Like many of Chinas rural poor, the Zhangs left their native village of Huilong in Sichuan province and their newborn daughter to find work in Guangzhou in a garment factory for 16 years and see her only once a year during the Spring Festival. Their daughter Qin, now a restless and rebellious teenager, resents her parents absence and longs for her own freedom away from school and her rural hometown, much to the dismay of her parents. She eventually leaves school, against the wishes of her parents, to work in the city.

==Developments since the film==
In a March 2010 follow-up interview, director Lixin Fan reveals that the Zhangs are still working in the factory and Qin telephoned, but did not visit, for the New Year. 

In September 2011, Fan said that Qin was now a vocational student in Beijing, and that while Qins mother is back on the farm, her father still works at the factory. 

In January 2012, an update on the family was released:

 
"We always kept contact with the family after we finished filming. The girl, she quit that job at the bar and went to find a new job. Shes floating around. I knew she had a job at a hotel as a bartender and she went back to the factory for a small while and she came to study in a vocational school in Beijing last year for a few months, and then she quit again. Now shes working in a small city in Hubei. Shes 22 now, so a big girl. She doesnt  . She still resents her parents very much. She thinks she never received any love from the parents, so shell deliberately avoid them during Chinese New Year.

The little brother is now 16 years old. He was doing really great in school, he got really good marks. Hes second-year in high school  . He got a few No. 1s in the past few years. His parents were really happy.

Mother, she lost her job. She   quit, but its because of the financial crisis,   brought down the salaries so much. The factory usually doesnt fire you, it just drops the salary to a level that makes you quit by yourself. So she went back to the village to take care of the son. So now its the father working alone in Guangzhou in the factory. So I think its a really sad thing to see: by the end of this documentary, you see this family has been shattered into smaller pieces. Although the daughter did "succeed" in finding her own independence in the city."
 

==Awards==
* 2009: Won Best Feature Documentary at the 22nd annual International Documentary Film Festival Amsterdam (IDFA) 
* 2009: Won Cinémathèque Québécoise Best Quebec film award at the Rencontres internationales du documentaire de Montréal (RIDM) Golden Gate Award in the Investigative Documentary Feature Category at the San Francisco International Film Festival (SFiFF)
* 2010: Official Selection at Sundance Film Festival
* 2010: Official Selection at New Directors/New Films
* 2010: Won Best Documentary Feature at the RiverRun International Film Festival 
* 2010: Won Grand Prix at EBS International Documentary Festival

==Reception==
Last Train Home is certified "fresh" with a 100% rating on Rotten Tomatoes, earning the Golden Tomato award for best limited-release and best foreign film. 

Manohla Dargis of the New York Times picked Last Train Home as one of the most outstanding works from the 2010 Sundance by characterizing it as "a beautifully shot, haunting and haunted large scale portrait."    
 Marx so generously." 

Praising Last Train Home as "a documentary masterpiece," Brian Brooks of IndieWIRE wrote that "filmmaker Lixin Fan may very well be one of modern-day Chinas great non-fiction storytellers."    

Critics of IndieWIRE placed Last Train Home at "top four" in its list of Top Ten Competition Films of the 2010 Sundance Film Festival.   

Last Train Home was one of the top five films nominated for the Directors Guild of America Documentary Prize, announced on January 29, 2011 at the 63rd annual DGA Awards Dinner.     It lost to Charles Fergusons "Inside Job."
 CBC radio program "The Current", on Jan 19th, 2011 talking about Last Train Home.   

==Release==
Last Train Home was released on American screens on September 3, 2010.

==References==
 

==External links==
 
*  
*   at EyeSteelFilm
*  
*  
*  
*  
*   at P.O.V.
*  

 

 
 
 
 
 
 
 
 
 
 
 
 