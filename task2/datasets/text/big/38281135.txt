The Gold Cure
 
 
{{Infobox film
| name           = The Gold Cure
| image          =
| caption        =
| director       = W.P. Kellino 
| producer       = 
| writer         = Sara Jeanette Duncan (story)   Lydia Hayward
| starring       = Queenie Thomas   Gladys Hamer   Jameson Thomas   Eric Bransby Williams
| music          = 
| cinematography = Jack E. Cox
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = October 1925
| runtime        = 5,700 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent comedy film directed by W.P. Kellino and starring Queenie Thomas, Gladys Hamer and Jameson Thomas. It was made by Stoll Pictures at Cricklewood Studios.

==Cast==
* Queenie Thomas as Betty Van Allen
* Gladys Hamer as Bella Box
* Jameson Thomas as Lansing Carter
* Eric Bransby Williams as Lord Dinacre
* Albert E. Raynor as Mr. Van Allen
* Moore Marriott as Janbois
* Judd Green as Mr. Box
* Leal Douglas as Lady Dunacre
* Johnny Butt as Albert Horsey
* Jefferson Gore as Dennis OShamus
* Nell Emerald as Nora Flanegan
* Dave OToole as Mick Mulvaney

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918–1929. George Allen & Unwin, 1971.

 

 
 
 
 
 
 
 
 
 
 


 
 