Dansa als esperits
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Dansa als esperits
| image          = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| starring       = 
| distributor    = 
| released       = 2009
| runtime        = 79 minutes
| country        = Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = 
| editing        = 
| music          = 
}}

Dansa als esperits is a Spanish 2009 documentary film.

== Synopsis ==
To the Evuzok, a tribe in the south of Cameroon, there are two kinds of diseases that are cured different ways: the "natural" ones and the ones from the night world, caused by sorcery. Dance to the Spirits is the story of Mba Owona Pierre, the village chief and ngengan (healer). He deals with the sicknesses that come from the night world where spirits live and attack his people. Pierre has a special gift and a responsibility towards his fellow villagers. The dance to the spirits is his main healing ritual.

== References ==
 

 
 
 
 
 
 


 
 