Birdemic: Shock and Terror
 
{{Infobox film
| name           = Birdemic: Shock and Terror
| image          = Birdemic.jpg
| caption        = First promotional poster
| director       = James Nguyen
| producer       = James Nguyen Tim Ubels
| writer    = James Nguyen
| starring       = Alan Bagh Whitney Moore Janae Caster Colton Osborne Adam Sessa
| cinematography = Daniel Mai
| editing        = Kim Chow
| music          = Andrew Seger
| studio         = Moviehead Pictures
| distributor    = Severin Films
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $10,000
| gross          =
}}
 independent romance romantic horror The Birds, Birdemic tells the story of a romance between the two main characters as their small town is attacked by birds.
 worst films cult following and was picked up for distribution by Severin Films in 2010.

==Plot==
Rod (Alan Bagh) is a young software salesman living a successful life in Silicon Valley. He meets up with old classmate and aspiring fashion model Nathalie (Whitney Moore) and begins dating her. Things go well for the couple, with Rod receiving a large bonus that he uses to start his own business, while Nathalie is chosen as a Victorias Secret model. As they grow closer, the couple remains oblivious to signs of something going wrong around them, such as unexplained wildfires and the carcasses of diseased birds turning up on beaches.
 Marine named Ramsey (Adam Sessa) and his girlfriend Becky (Catherine Batcha). As they leave town, they rescue two young children, Susan (Janae Caster) and Tony (Colton Osborne), whose parents have been killed by the birds.

The group proceeds to drive from one town to the next, fending off more bird attacks along the way and briefly meeting a scientist named Dr. Jones (Rick Camp) studying the phenomenon. Becky is killed by the birds, and Ramsey, in an attempt to exact revenge, tries to save a busload of tourists. As they leave the bus, Ramsey and the tourists are doused in acid excrement by the birds and all die.
 Tree Hugger" named Tom Hill (Stephen Gustavson), who explains to them that the birds have only been targeting gas stations and cars and that the attacks are the result of global warming. After escaping a forest fire, the quartet ultimately settles on a small beach, where Rod fishes for dinner. As they prepare to eat, they are attacked by the birds, which are suddenly—and for no explained reason—chased away by doves. The film ends as Rod, Nathalie and the kids watch the birds fly off into the sunset.

==Cast==
 
*Alan Bagh as Rod
*Whitney Moore as Nathalie
*Adam Sessa as Ramsey
*Catherine Batcha as Becky
*Janae Caster as Susan
*Colton Osborne as Tony
*Rick Camp as Dr. Jones
*Stephen Gustavson as Tree Hugger/Tom Hill
*Patsy van Ettinger as Nathalies Mother
*Mona Lisa Moon as Mai
*Danny Webber as Rick
*Laura Cassidy as News Anchor
*Damien Carter as Nightclub Singer
*James Nguyen as a restaurant customer (cameo)

==Production==
 
Da Nang-born  The Birds (1963)    and Vertigo (film)|Vertigo (1958), which Nguyen considers his favorite Hitchcock film. 
   Nguyen went on to be a software salesman in Silicon Valley.  He first picked up a camera in 1999,  and eventually directed two movies, Julie and Jack and Replica. 
   The former, released in 2003, was a low-budget romance, 
   while the latter was never released.  Nguyen blamed this on the expensive storyboarding process and his casting decisions. 

Nguyen was inspired to write the script for Birdemic: Shock and Terror while spending time relaxing in Half Moon Bay, California,    and much of the filming took place in the area surrounding the community. Birdemic began production in 2006 and took four years to produce,  partly due to time limitations—filming was done mostly on weekends over the course of seven months    —and also due to financial restraints as it was financed through Nguyens day job, plus the time it took for Nguyen to find a distributor.

According to the films website, the movie was partly inspired by Alfred Hitchcocks 1963 film The Birds (the film touts a cameo from Tippi Hedren, which is actually archival footage from an early Nguyen film, Julie and Jack, playing on a television)    and Al Gores An Inconvenient Truth.  The film was produced for a budget of $10,000, but the distributors have spent more on marketing than it cost to produce the film and purchase the rights.   

==Promotion==
 
In January 2009, Nguyen traveled to the Sundance Film Festival in Park City, Utah to promote the film freelance, handing out flyers to passers-by from his van, adorned with stuffed birds and paper signs that read "BIDEMIC.COM" (spelling Birdemic wrong in his haste) and "WHY DID THE EAGLES AND VULTURES ATTACKED  ?", and renting out a local theater to screen the film.  Word of mouth eventually gave Birdemic attention from horror movie websites Dread Central    and Bloody Disgusting,  while the trailer was featured on the July 30 episode of G4 (TV channel)|G4s Attack of the Show. 

==Release== Alamo Drafthouse in Austin, Texas on March 2,    with follow-up screenings in Tempe, Arizona.    and New York City.   

Severin Films acquired the film in early 2010 and launched the Birdemic Experience Tour 2010, which showed the film in numerous cities in the United States and Toronto, Canada from April through July 2010.  Birdemic premiered in the United Kingdom at The Curzon Soho in London on May 28, 2010. 

===Home media===
On April 3, 2009, Moviehead Pictures self-released Birdemic on DVD, selling it exclusively through Movieheads official website and manufactured on demand through Amazon.com.  In early 2010, Birdemic was picked up by Severin Films  with plans to release the film on home media. Birdemic was released on DVD and Blu-ray Disc on February 22, 2011. The DVDs special features include an audio commentary by James Nguyen as well as one by lead actors Alan Bagh and Whitney Moore, two deleted scenes, a feature on the Birdemic Experience Tour, and an episode of the public-access San Francisco TV show Movie Close Up which features Nguyen and was aired as Birdemic was still under production (the host of the show, Bonnie Steiger, later played an extra in the bus rescue scene of Birdemic).

==Reception== CGI eagles sprites in the background will simply rotate 360° in mid-air), spit acid and explode with unrealistic smoke upon impact with the ground with a plane dive sound effect. Morbid.   www.dreamindemon.com. December 18, 2009.  It has also been noted that the birds do not appear until 47 minutes into the film, nearly half way into it.

On a 2009 "Best of" list,   described Birdemic as "one more in the pantheon of beloved trash-terpieces."  Salon (magazine)|Salon commented on the "atrocious CGI" and reported that the film had become "a cult hit among bad-movie fans." 

An online review from the Independent Film Channel stated that the film feels "indebted to Woods Plan 9 from Outer Space with its blend of ultra-low budget filmmaking and cuckoo bananas ecological message."   The Guardian and The New York Times  reported on the films cult status, with The Guardian writing that "Birdemic features acting as wooden as a tree, clunky camera work...and crude special effects that reduce audiences to tears of laughter rather than terror."  Slate (magazine)|Slate wrote that "aspects of Birdemic can seem too bad to be true...The films artlessness comes to function as its own sort of hallucinatory art...we see the narrative space of the film breaking down and rebuilding itself constantly—bloody stitches on its forehead, bolts in its neck. This breakdown can be profoundly discomfiting and surprisingly infectious."    Rotten Tomatoes currently gives the movie a rating of 20%. 

Tim Heidecker and Eric Wareheim, who hosted the Los Angeles premiere, parodied the movie on their television series   released on February 28, 2012 and starring cast member Whitney Moore and guest star "Weird Al" Yankovic (who states that he is a fan of the film) is dedicated to discussing Birdemic. 

"Birdemic" was the  lowest rated film on the Internet Movie Database (IMDb), with a 1.8 rating until it was overtaken by Kirk Camerons Saving Christmas with a rating of 1.5. 
 Kevin Murphy of Mystery Science Theater 3000 fame.  The release of their commentary was quickly followed up by recreations of seven humorous scenes from the film.     On October 25, 2012, Fathom hosted a RiffTrax Live event featuring Birdemic.  A physical dvd/download of the show was released on November 25, 2014.

==Sequel==
A sequel, titled    and also written and directed by James Nguyen, finished filming in March 2012  and was released in April 2013 with a special screening in Los Angeles followed by a tour to select theaters in the United States,  with an online release on April 16, 2013.  The plot centers around a struggling Hollywood filmmaker named Bill (Thomas Favaloro) who casts an aspiring actress named Gloria (Chelsea Turnbo) in his upcoming film before eagles and vultures attack. Alan Bagh and Whitney Moore reprised their roles for the sequel, as did Colton Osborne, Rick Camp, Stephen Gustavson, Patsy van Ettinger, and Damien Carter.

==See also==
*List of films considered the worst
* 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 