The Dagger
 
{{Infobox film
| name           = Нож   The Dagger 
| image          = The Dagger (film).jpg
| image size     = 196px
| border         = 
| alt            = 
| caption        = 
| director       = Miroslav Lekić 
| producer       = Bojan Maljević Bojana Maljević
| writer         = Miroslav Lekić Slobodan Stojanović
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Žarko Laušević Bojana Maljević
| music          = Toma Babović Aleksandar Milić
| cinematography = Veljko Despotović
| editing        = Branislav Milošević
| studio         = 
| distributor    = Monte Royal Pictures
| released       =  
| runtime        = 135 min. FR of Yugoslavia
| language       = Serbian
| budget         = 
}}
The Dagger ( ; which means Knife) is a 1999 Serbian war drama film directed by Miroslav Lekić. The film was written by Miroslav Lekić, Slobodan Stanojević and Igor Bojović. The plot is based on Vuk Draškovićs novel of the same name. 

The main motive of the film is the eventually disclosed nonsense of ethnic division in contemporary Bosnia, poiting out to the same historical origin of both opposed ethnic groups, Bosnian Serbs and Bosniaks, i.e. Christians and Muslims. Set in the 1960s and observed from the point of view of Alija Osmanović, a young Muslim medical student raised by single mother, his entire family slaughtered and his baby brother kidnaped by Serbs in World war II, as the aftermath of Jugovići (Christian) and Osmanovići (Muslim) violent family feud, he learns not only that Osmanovići were once but a branch of Jugovići family who converted to Islam during the Turkish rule, but that, unbeknownst to his mother, he himself was a baby taken from Jugoviči, after the massacre of Jugovići on Christmas Eve in 1942. Both families now extinct, and Alia as the descendant of both, torn between two cultures and two identities, he struggles to maintain his inner peace, desperately searching for his long lost step-brother and fighting the prejudices against the love relationship he has with a Serbian colleague student.

Although based on true events of World War II, but lacking a wider perspective and being centered on the atrocious crimes committed to Serbs during World War II, and the particular families of Jugovići and Osmanovići, the movie stays considered a Serbian propaganda piece by Bosniaks, viewes as attempting to justify the crimes committed during the war in the 90s by presenting a view of history, by which all inhabitants of western Balkans are Serbs. 

In 1999, the film was screened at the 13th Montenegro Film Festival, and gained five featured awards.     The film also earned the “Fipresci Award” for Directing, five acting awards in the Niš Film Festival and the “Crystal Star” at the Brussels Film Festival.  


== Cast ==
*Žarko Laušević : Alija Osmanovic / Ilija Jugovic
*Bojana Maljević : Milica Jankovic
*Aleksandar Berček : Halil Sikter Efendija
*Ljiljana Blagojević : Rabija Osmanovic
*Petar Božović : Sabahudin Aga / Atifaga Tanovic
*Velimir Bata Zivojinovic : Nicifor Jugovic
*Nikola Kojo : Milan Vilenjak
*Svetozar Cvetković : Selim Osmanovic
*Josif Tatić : Kemal Osmanovic
*Dragan Nikolić : Hodza
*Dragan Maksimović : Zulfikar 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 

 