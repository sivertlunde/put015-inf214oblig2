Cyber Seniors
 

{{Infobox film
| name           = Cyber Seniors
| image          = Cyber_Seniors.jpg
| image_size     =
| caption        =
| director       = Saffron Cassaday
| producer       = Brenda Rusnak 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = The Best Part Inc.
| distributor    = Area 23a
| released       =  
| runtime        = 75 minutes 
| country        = Canada
| language       = English
| gross          = 
| preceded_by    =
| followed_by    =
}}

Cyber Seniors is a 2014 documentary film directed by Saffron Cassaday about reluctant seniors who with the help of teenage mentors discover the wonders of the world-wide-web.

==Synopsis==
Cyber Seniors chronicles the journey of a group of senior citizens as they discover the world of the internet through the guidance of teenage mentors.  After 89 year-old Shura decides to create a YouTube cooking video a video competition is organized with the winner coming from the senior who gets the most "views" on YouTube.

The film highlights the "Cyber-Seniors" program, one that creates opportunities for high school students to mentor senior citizens on basic computer and online skills. The program was founded in 2009 by two Toronto high school students, Kascha and Macaulee Cassaday, as a local community service project.    The sisters are younger sisters of the director of the film.   

==Production==
The film is the directorial debut of Saffron Cassaday, an actress best known for her roles on Nightclub Story (2008), White Collar Criminals (2009), and The Swashbuckling Adventures of Pete Winning and the Pirates (2011).   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 

 