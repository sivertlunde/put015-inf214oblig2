Freddy Got Fingered
 
 
{{Infobox film
| name           = Freddy Got Fingered
| image          = Freddy Got Fingered (movie poster).jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Tom Green
| producer       = Larry Brezner Howard Lapides Lauren Lloyd
| writer         = Tom Green Derek Harvie
| starring       = Tom Green Rip Torn Marisa Coughlan Eddie Kaye Thomas Harland Williams Anthony Michael Hall Julie Hagerty Mike Simpson
| cinematography = Mark Irwin
| editing        = Jacqueline Cambas
| studio         = Regency Enterprises
| distributor    = 20th Century Fox
| released       =  
| runtime        = 87 minutes  
| country        = United States
| language       = English
| budget         = $14 million   
| gross          = $14.3 million 
}}
Freddy Got Fingered is a 2001 American comedy film directed, co-written by, and starring Tom Green. The film follows Green as a 28-year-old slacker who wishes to become a professional cartoonist. The films plot resembles Greens struggles as a young man trying to get his TV series picked up, which would later become the popular MTV show The Tom Green Show.
 worst films of all time. It won five Golden Raspberry Awards out of eight nominations, as well as a Dallas-Fort Worth Film Critics Association Award for Worst Picture. The film received a cult following, and was also met with more positive praise over time, most notably from The New York Times, Metacritic, IFC (U.S. TV channel)#IFC.com|IFC.com and The Awl|Splitsider. Despite a terrible box office run, the film became a financial success by selling millions of copies on DVD.

==Plot==
Throughout the film, unemployed 28-year-old cartoonist Gordon "Gord" Brody pursues his lifelong ambition to obtain a contract for an animated television series. He lives in Portland, Oregon with his parents. They give him a car, in which he heads off for Hollywood, and subsequently gets a job in a cheese sandwich factory in Los Angeles. Brody manages to speak to Dave Davidson, the CEO of a major animation studio, and shows him his drawings; despite noting that the drawings are "pretty good", Davidson disparages Brodys idea of an "X-Ray Cat", declaring it "fucking stupid" and remarks that it "doesnt make no sense".

A disheartened Brody quits his job and returns home to his parents, angering his father, Jim, who constantly insults and belittles Brody. One night, he builds a hand-built half-pipe, only to have his friend Darren break his leg due to a skateboarding accident, after being encouraged by Gordy to skate. Darren is sent to the hospital as a result, where Gordy assists a woman in labor to give birth in a very shocking display. He then meets Betty, an attractive wheelchair-bound nurse with an obsessive penchant for fellatio and an ambition to create a rocket-powered wheelchair, who Jim also disparages. After a heated display, Jim smashes Brodys hand-built half-pipe, Brody falsely accuses Jim of sexual molestation of Gords younger brother, Freddy. The 25-year-old Freddy is sent to a home for sexually-molested children, while Brodys mother, Julie, leaves Jim, and ends up dating Shaquille ONeal|Shaq.

After seeing Bettys successful experiment with a rocket-powered wheelchair, Brody returns to Hollywood, with a concept based on his relationship with his father, for a series called "Zebras in America". After Jim bursts in on Brodys proposal and trashes Davidsons office, Davidson is amused enough by Jims antics to greenlight the series and give Brody a million-dollar check. Brody then kidnaps his father and takes him to Pakistan as a response to Jims earlier insult: "If this were Pakistan, you would have been sewing soccer balls when you were four years old!". Brody and Jim soon come to terms, but are soon kidnapped and held hostage. The kidnapping becomes a news item, as Brodys series is already highly popular. After eighteen months, Brody and Jim return to America, with a huge crowd welcoming them home.

==Cast==
* Tom Green as Gordon "Gord" Brody
* Rip Torn as James "Jim" Brody
* Marisa Coughlan as Betty
* Eddie Kaye Thomas as Frederick "Freddy" Brody
* Harland Williams as Darren
* Anthony Michael Hall as Dave Davidson
* Julie Hagerty as Julie Brody
* Drew Barrymore as Davidsons receptionist
* Shaquille ONeal as Himself
* Connor Widdows as Andy Malloy
* Lorena Gale as Psychiatrist/Social worker
* Noel Fisher as Pimply manager
* Stephen Tobolowsky (uncredited) as Uncle Neil

==Release== R rating PG rating. The PG-rated cut of Freddy Got Fingered is a mere three minutes long with a comedic voiceover. Some footage was leaked by the Newgrounds website before release.  Years later, Tom Fulp, owner of Newgrounds, confirmed that the leak was a publicity stunt. 

==Reception==
===Box office===
On a budget of $14 million, Freddy Got Fingered grossed $14,254,993 domestically and $78,259 overseas for a worldwide total of $14,333,252.  The film earned $24,300,000 from DVD sales, and was among the top 50 weekly DVD rentals chart.  Green has stated in a few interviews in 2010 that DVD sales have been growing many years later and that there was a cult following.   

===Critical response===
  worst films Battlefield Earth, a significant number of critics are calling Tom Greens extreme gross-out comedy the worst movie they have ever seen." Metacritic, which assigns a weighted mean rating out of 100 to reviews from film critics, the film has an "Overwhelming dislike" rating score of 13% based on 25 reviews. CinemaScore polls revealed the average grades filmgoers gave Freddy Got Fingered were C and D, and F from older viewers, on an A+ to F scale.

The Toronto Star created a one-time new rating for Freddy Got Fingered, giving it "negative one star out of five stars." CNNs Paul Clinton called it "quite simply the worst movie ever released by a major studio in Hollywood history" and listed the running time as "87 awful minutes." 

 . The day may never come when it is seen as funny."  The magazine Complex (magazine)|Complex also ranked the film at #14 on its "25 Movies That Killed Careers". 
 At the Movies, hosted by Roeper and Ebert, called it "horrible" and expressed the view that Tom Green was a bad comedian, going so far as to say that he "should be flipping burgers somewhere". Along with Ebert, he was also offended by the numerous "gross-out" gags.

Film critic Leonard Maltin shared Ebert and Roepers views of the film: "Instantly notorious word-of-mouth debacle became the poster child for all thats wrong with movie comedy. Gags include the maiming of an innocent child and a newborn spun around in the air by its umbilical cord—compounded by the almost unimaginable ineptitude with which theyre executed." 

===Awards and nominations===
The film received eight  .    It lost to Battlefield Earth.

{| class="wikitable"
|-
! Year
! Award
! Category
! Subject
! Result
|-
|- 22nd Golden 2001
|rowspan=9|Golden Raspberry Award Golden Raspberry Worst Screenplay Derek Harvie Tom Green
| 
|- Golden Raspberry Worst Actor Tom Green
| 
|- Golden Raspberry Worst Director
| 
|- Golden Raspberry Worst Screen Couple Tom Green  Any animal he abuses
| 
|- Golden Raspberry Worst Picture Larry Brezner   Howard Lapides   Lauren Lloyd
| 
|- Golden Raspberry Worst Supporting Actor Rip Torn
| 
|- Golden Raspberry Worst Supporting Actress Drew Barrymore
| 
|- Julie Hagerty
| 
|-
|- 30th Golden 2010
|Worst Picture of the Decade Larry Brezner   Howard Lapides   Lauren Lloyd
| 
|-
|}

==Legacy==
===Resurgence===
Freddy Got Fingered began to see more positive praise over time. One of the few notable critics who gave it a generally positive review was A. O. Scott of The New York Times, who compared the film to conceptual performance art.  Critic Nathan Rabin of The A.V. Club gave the film a rave review in his "My Year Of Flops" column where he partially fulfilled Eberts prediction, comparing it to the work of Jean-Luc Godard and calling the film "less as a conventional comedy than as a borderline Dadaist provocation, a $15 million prank at the studios expense" adding "its utterly rare and wondrous to witness the emergence of a dazzlingly original comic voice. I experienced that glorious sensation watching Fingered...I can honestly say that Ive never seen anything remotely like it" and rated it a "Secret Success"  In a later column, Rabin stated "I was a little worried that Id catch flak for giving mad props to a film as divisive and widely reviled as Freddy Got Fingered. So I was relieved to discover that every single comment agreed with my assessment of it... It also didnt escape my attention that my Freddy post was the most commented-upon post in the history of My Year Of Flops by a huge margin."  Comedian Chris Rock listed Freddy Got Fingered as one of his favorite movies on his website. 

Later, in his review of the film Stealing Harvard, a film co-starring Green, Ebert wrote:
"Seeing Tom Green reminded me, as how could it not, of his movie Freddy Got Fingered, which was so poorly received by the film critics that it received only one lonely, apologetic positive review on the Rotten Tomatoes|Tomatometer. I gave it—lets see—zero stars. Bad movie, especially the scene where Green was whirling the newborn infant around his head by its umbilical cord.    But the thing is, I remember Freddy Got Fingered more than a year later. I refer to it sometimes. It is a milestone. And for all its sins, it was at least an ambitious movie, a go-for-broke attempt to accomplish something. It failed, but it has not left me convinced that Tom Green doesnt have good work in him. Anyone with his nerve and total lack of taste is sooner or later going to make a movie worth seeing." 

The film has subsequently developed a large cult following. In Tom Greens interview on Opie and Anthony|The Opie and Anthony Show, host Opie noted the film had begun to be regarded as one of the funniest movies ever made. Opie and Anthony, Tom Green interview, February 16, 2010  Green noted the film had sold a million units,  and that he wished to make a directors cut due to a lot of footage not making the final cut.  Green notes that he was not trying to make The Jazz Singer and that many fans of the movie shout out scenes from the film regularly at his stand-up performance. 

Unreality Magazine featured the film in its list of "10 Hilarious Movies That Received Terrible Reviews", noting that critics taste in comedies tend not to reflect the general public.  Vadim Rizov for IFC.com wrote an article titled "In defense of Freddy Got Fingered". He calls the film one of the great underrated comedies of the decade and says the film would go on to do better if it was released today, comparing it to the successful Adult Swim series Aqua Teen Hunger Force. 

===Directors Cut===
Green has stated that he would like to do a "directors cut" DVD release of the film in 2011 to celebrate the 10-year anniversary.    

On March 9, 2010, on Loveline, Green officially announced that a directors cut will be released.  In an answer to a question from a fan on his website tomgreen.com in December 2010, Green said that there was no progress yet in regards to the directors cut. In a Reddit "Ask Me Anything" (AMA) Green did on the website Reddit on October 17, 2013, Green responded to a question regarding the release of the directors cut with: The studio didnt give me the footage to make the directors cut. I want to do it. If you contact New Regency or 20th Century Fox and tell them you want a directors cut maybe it will happen!  

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 