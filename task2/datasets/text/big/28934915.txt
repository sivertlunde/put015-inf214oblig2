Max Rules
{{Infobox film
| name           = Max Rules
| image          = MaxRules.jpg
| image_size     =
| alt            =
| caption        = Robert Burke
| producer       = Robert Burke Jerry Mahan
| writer         =
| narrator       =
| starring       = William B. Davis Andrew C. Maier Jennifer Lancheros Spencer Esau
| music          = Matthew Bennett
| cinematography = John Jeffcoat
| editing        =
| studio         = Jumpshot Films
| distributor    = Peace Arch Entertainment North by Northwest Entertainment  }}
| released       = 2004
| runtime        = 80 minutes
| country        = United States English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Robert Burke. 

==Synopsis==
Max (Andrew C. Maier) and his friends Jessica (Jennifer Lancheros) and Scott (Spencer Esau) find thrills in spying on their families, sneaking into each others houses, and organizing elaborate pranks at school.  They have unique access to some of the most sophisticated equipment in the world, thanks to Maxs Uncle, Rick Brinkley (William B. Davis), developer of top-secret equipment for the government.  When Max discovers information about the whereabouts of a stolen FBI microchip, he and his friends use their skills and cutting edge technology to embark on the most dangerous mission of their lives.

==Production==
Filming took place in Seattle and Bellevue, Washington.

==Festivals==
*Tribeca Film Festival
*Seattle International Film Festival
*Salento International Film Festival - Salento, Italy
*HBO New York International Latino Film Festival
*Childrens Film Festival - Cologne, Germany
*Seoul International Youth Film Festival
*Schlingel International Film Festival for Children
*Hannover Childrens Film Festival
*Fort Lauderdale International Film Festival
*Staten Island Film Festival

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 


 