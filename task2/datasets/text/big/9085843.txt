The Lady's from Kentucky
{{Infobox film
| name           = The Ladys from Kentucky
| image          = TheLadys-from-Kentucky-1939.jpg
| image_size     =
| caption        = Film poster
| director       = Alexander Hall
| producer       = Jeff Lazarus
| writer         = Malcolm Stuart Boylan (screenplay) Rowland Brown (story)
| narrator       =
| starring       = George Raft Ellen Drew Zasu Pitts
| music          = John Leipold Leo Shuken
| cinematography = Theodor Sparkuhl
| editing        = Harvey Johnston
| distributor    =
| released       = April 28, 1939
| runtime        = 67 min.
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
}} 1939 film starring George Raft and Ellen Drew.  The movie was written by Malcolm Stuart Boylan from a story by Rowland Brown, and was directed by Alexander Hall.  The screenplay involves a failing bookie (Raft) who becomes half owner of a racehorse, with a Kentucky lady (Drew) owning the other half.  Zasu Pitts plays a supporting role.

==Cast==
*George Raft as Marty Black
*Ellen Drew as Penny Hollis
*Hugh Herbert as Mousey Johnson
*Zasu Pitts as Dulcey Lee
*Louise Beavers as Aunt Tina
*Lew Payton as Sixty
*Forrester Harvey as Nonny Watkins
*Edward Pawley as Spike Cronin
*Gilbert Emery as Pinckney Rodell
*Jimmy Bristow as Brewster
*Stanley Andrews as Doctor George Anderson as Joe Lane

==Production==
The film was the last George Raft made under his contract with Paramount Pictures and had a smaller budget than Raft was used to, most likely because he was using the studio. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 81 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 