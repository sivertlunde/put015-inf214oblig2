The Devil's Advocate (1977 film)
 
{{Infobox film
| name           = The Devils Advocate
| image          =The Devils Advocate (1977 film).jpg
| image_size     =
| caption        = Guy Green
| producer       = Lutz Hengst, Helmut Jedele The Devils Advocate
| narrator       = Jason Miller Daniel Massey Raf Vallone  Patrick Mower Timothy West
| music          = Bert Grund Billy Williams
| editing        =
| distributor    =
| released       = 27 October 1977
| runtime        = 104 minutes
| country        = West Germany
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 1977 Cinema West German Guy Green the novel of the same name by the Australian writer Morris West.
 Jason Miller Daniel Massey. The film is set in Italy but was filmed predominantly in West Germany.

==Plot==
In 1958, the Catholic Church is investigating the case of a mysterious individual, Giacomo Nerone (Leigh Lawson), who is said to have performed miracles in a remote village in southern Italy (Scontrone), before being executed by Italian Communist partisans in 1944.  The process involves a "Devils advocate", who is tasked with discovering any details about the subjects life that would indicate their canonisation would be inappropriate.

Monsignor Blaise Meredith (John Mills) is given this responsibility, shortly after he learns he has terminal cancer.  Meredith discovers that Nerone was actually a British soldier named James Black, who had become detached from the British Army during WW2 and was hiding in this village, where he began a relationship with a local woman.

The film touches on homosexuality in the priesthood, persecution of Jews and other sensitive topics.

==Cast==
* Monsignor Blaise Meredith … John Mills
* Anne, Contessa di Sanctis … Stéphane Audran Jason Miller
* Nina Sanduzzi … Paola Pitagora
* James Black aka Giacomo Nerone … Leigh Lawson
* Father Anselmo … Timothy West
* Il Lupo … Patrick Mower
* Bishop Aurelio … Raf Vallone Daniel Massey
* Cardinal Marotta … Romolo Valli  

==Production==
Morris West wrote the screenplay from his novel of the same name.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 