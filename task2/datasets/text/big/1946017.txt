Talk to Her
 
 
{{Infobox film
| name           = Talk to Her
| image          = Talk to Her English movie poster fairuse.jpg
| image_size     = 215px
| alt            = 
| caption        = US theatrical release poster
| director       = Pedro Almodóvar
| producer       = Agustín Almodóvar Michel Ruben
| writer         = Pedro Almodóvar
| starring       = Javier Cámara Darío Grandinetti Leonor Watling Geraldine Chaplin Rosario Flores
| music          = Alberto Iglesias
| cinematography = Javier Aguirresarobe
| editing        = José Salcedo El Deseo S.A. Warner Sogefilms   Sony Pictures Classics  
| released       =  
| runtime        = 112 minutes
| country        = Spain
| language       = Spanish
| budget         =
| gross          = $51,001,550
}}
Talk to Her ( ) is a 2002 Spanish drama written and directed by Pedro Almodóvar, and starring Javier Cámara, Darío Grandinetti, Leonor Watling, Geraldine Chaplin, and Rosario Flores. The film follows two men who form an unlikely friendship as they care for two women who are both in comas. The films themes include the difficulty of communication between the sexes, loneliness and intimacy, and the persistence of love beyond loss.
 BAFTA for Best Film Golden Globe Best Foreign Best Original Screenplay. In 2005, Time (magazine)|Time magazine film critics Richard Corliss and Richard Schickel included Talk to Her in their list of the All-TIME 100 Greatest Movies.  Paul Schrader placed the film at 46 on his film canon of the 60 greatest films. 

==Plot==
The story unfolds in flashback (narrative)|flashbacks, giving details of two separate relationships that become intertwined with each other.

During a dance recital, Benigno Martín and Marco Zuluaga cross paths, but the two men are no more than strangers. Still, Benigno notices that Marco cries.

Marco is a journalist and travel writer, who happens to see a TV interview of Lydia González, a famous   and is surprised to find Lydia there too, since she had said that she did not want to go. The wedding turns out to be that of Marco’s former fiancée, Angela, who had the same phobia to snakes as Lydia; Marco was very much in love with Angela and had a very hard time getting over her (which was the reason for his constant crying over things he could not share with her). Lydia says that she has something important to say, but she prefers to wait until after the bull-fight that afternoon; but she is gored and becomes comatose. Marco does not leave her side at the hospital and finally befriends Benigno, who recognized him from the dance recital. Marco is told by the doctors that people in a coma never wake up and that, while there are miracle-stories of people who have come back, he should not keep his hopes high.

Benigno is a personal nurse and caregiver for Alicia Roncero, a beautiful dance student, who lies in a coma; but Benigno sees her as alive and he talks his heart out to her patient, and brings her all kinds of dancing and silent black and white film mementos. As it turns out, Benigno had been obsessed with Alicia for a while, before she was in a coma, since his apartment is in front of the dance studio where she practiced every day. At first, his obsession is only from a distance, since Benigno takes care of his possessive mother, who seems to be immobilized. For that reason, he became a nurse and also a beautician. After his mother died, free to move about, he finally picked up the courage to talk to Alicia, after she dropped her wallet on the street. As they walk together to her house, they talk about her discovery of mute black and white films and about dancing. When she walks into her building, Benigno notices that she lives in Dr. Roncero’s house, who is a psychiatrist. Benigno makes an appointment to see the doctor and talks about his unresolved bereavement over his mother. But it all is a ruse to gain access to the apartment, where he steals a hair-clip from Alicia’s room. That night, Alicia is run over by a car and becomes comatose. By mere chance, Benigno is assigned to Alicia, much to the surprise of her father. But since Benigno’s services are the best, he hires him and a colleague permanently to tend for Alicia. Benigno also lies to Dr. Roncero that he is homosexual, so Alicias father wouldnt suspect his love to her.

Benigno keeps telling Marco that he should talk to Lydia, because, despite the fact that they are in a coma, women understand and react to men’s problems. Eventually, Marco learns from el Niño de Valencia that Lydia and el Niño had decided to be together again, and that she intended to tell Marco. 
So Marco finds himself alone again. As he is about to leave, he comes into Alicia’s room, looking for Benigno, but he instead finds himself opening his heart out to her, despite his scepticism over Benigno’s theories. Benigno and Marco leave the hospital and, in the parking lot, Benigno tells Marco of his plans to marry Alicia: Marco is taken aback, telling his friend that Alicia is basically dead and cannot express her will in any manner. But Benigno does not hear any reason. During a routine review at the hospital, the supervisors notice that Alicia has missed several periods; since this is a common occurrence with people in a coma, they do not think twice over it. However, Alicia is pregnant and an investigation ensues where Benigno is the main suspect.
 stillborn baby. Following Benignos lawyers urging, Marco does not tell Benigno about Alicias unexpected recovery. Desperate, Benigno writes a farewell letter to Marco and ingests a large quantity of pills, to try to "escape" into a coma, thus reuniting with Alicia. He dies of an overdose.

Meanwhile, Alicia has begun rehabilitation to recover her ability to walk and dance. The film ends in the same theatre where it began, where Marco and Alicia meet by chance.

==Cast==
* Javier Cámara as Benigno Martín
* Darío Grandinetti as Marco Zuluaga
* Leonor Watling as Alicia
* Rosario Flores as Lydia González
* Mariola Fuentes as Rosa
* Geraldine Chaplin as Katerina Bilova
* Pina Bausch as Café Müller dancer
* Malou Airaudo as Café Müller dancer
* Caetano Veloso as Singer at party as "Cucurrucucú paloma"
* Roberto Álvarez as Doctor
* Elena Anaya as Ángela
* Lola Dueñas as Matilde
* Adolfo Fernández as Niño de Valencia Ana Fernández as Lydias sister
* Chus Lampreave as Caretaker
* Paz Vega as Amparo (in silent film)

==Reception==
Talk to Her received positive reviews, as it currently holds a 92% "fresh" rating on  , which uses an average of critics reviews, the film holds an 86/100, indicating "universal acclaim".  Despite the films success, Talk to Her wasnt submitted as Spains pick for the Academy Award for Best Foreign Film. Mondays in the Sun was selected instead.

The film was a commercial success, grossing $9,285,469 in the United States and $41,716,081 internationally for a worldwide total of $51,001,550. 

==Awards and nominations==
;Wins 2002 Academy Awards: Best Original Screenplay - Pedro Almodóvar
* Argentine Film Critics Association ("Silver Condor"): Best Foreign Film 2003 BAFTA Awards: Best Film Not in the English Language Best Original Screenplay - Pedro Almodóvar
* 2003 Bangkok International Film Festival ("Golden Kinnaree Award"): Best Film, Best Director - Pedro Almodóvar
*  
* Bogey Awards: Bogey Award
* Cinema Brazil Grand Prize: Best Foreign Language Film
* Cinema Writers Circle Awards (Spain): Best Original Score - Alberto Iglesias
* Czech Lions: Best Foreign Language Film
*  
* European Film Awards: Best Film, Best Director (Pedro Almodóvar), Best Screenwriter - Pedro Almodóvar
* French Syndicate of Cinema Critics: Best Foreign Film 2003 Golden Globe Awards: Best Foreign Language Film
* Goya Awards (Spain): Best Original Score - Alberto Iglesias
* Los Angeles Film Critics Association: Best Director - Pedro Almodóvar
* Mexican Cinema Journalists ("Silver Goddess"): Best Foreign Film
* National Board of Review: Best Foreign Language Film
* Russian Guild of Film Critics ("Golden Aries"): Best Foreign Film
* Satellite Awards: Best Motion Picture: Foreign Language, Best Original Screenplay - Pedro Almodóvar
* Sofia International Film Festival: Audience Award – Best Film
* Spanish Actors Union: Performance in a Minor Role: Female - Mariola Fuentes
* TIME Magazine: Best Film
* Uruguayan Film Critics Association: Best Film (tie)
* Vancouver Film Critics Circle: Best Foreign Film

;Nominations 2002 Academy Awards: Best Director - Pedro Almodóvar
* British Independent Film Awards: Best Foreign Film – Foreign Language
*  
* Chicago Film Critics Association: Best Foreign Language Film
*  
*  
* Satellite Awards: Best Director - Pedro Almodóvar

==References==
 
 
==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 