Bio Zombie
 
 
{{Infobox film
| caption =
| director = Wilson Yip
| image	= Bio Zombie FilmPoster.jpeg Joe Ma So Man-Sing Wilson Yip Sam Lee Angela Tong Wayne Lai
| music = Peter Kam
| cinematography = Kwok-Man Keung
| editing = Ka-Fai Cheung
| distributor = Media Blasters (U.S. DVD)
| released = 6 November 1998 
24 April 2001 (U.S. DVD)
| runtime = 94 minutes
| country = Hong Kong
| language = Cantonese
| budget = 
}} Dawn of the Dead and shows many similarities to Peter Jacksons Braindead (1992 film)|Braindead.

== Plot ==
This movie takes place in a mall where two young men and other people work. They do not seem to get along very well due to a lack of customers. One night the two young men give an apparently dying businessman a soft drink, which is in actuality an experimental Iraqi biological weapon that turns him into a flesh-eating zombie. Returning to the mall, the man escapes and begins infecting the population, forcing a small group of misfits to band together in order to survive.

==Cast==
* Jordan Chan as Woody Invincible Sam Lee as Crazy Bee
* Angela Tong as Rolls
* Wayne Lai as Kui
* Emotion Cheung as  Loi
* Yeung Wing Cheung as Chinki Zombie
* Frankie Chan
* Ronny Ching
* Matt Chow
* Ken Lok

Voice cast (English version): Sparky Thornton as Woody Invincible David Umansky as Crazy Bee
* Matt K. Miller as  Kui
* Bob Bobson as VCD Nerd
* Francis Cherry as Man A
* Richard Epcar as Ox
* Tara Jayne as Jelly Steve Kramer as Yung/Zombies
* Wendee Lee as Rolls
* Dave Mallow as Loi
* Dorothy Melendrez as Mrs. Kui / TV announcer / Cindy
* Anthony Mozdy as Boss / Sushi Chef / Movie Actor 1 / Zombies

 
 

==Reception==
 

==References==
 

==External links==
* 
*  
*   at Kung Fu Cult Cinema

 

 
 
 
 
 
 
 


 
 