Murderers' Row (film)
{{Infobox film
| name           = Murderers Row
| image          = MurderersRow.jpg
| image_size     =
| caption        = film poster by Robert McGinnis
| director       = Henry Levin
| producer       = Irving Allen Euan Lloyd
| based on       =   Herbert Baker 
| starring       = Dean Martin Ann-Margret Karl Malden
| music          = Lalo Schifrin
| cinematography = Sam Leavitt
| editing        = Walter A. Thompson
| studio         = Meadway-Claude Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 105 min
| country        = United States
| language       =
| gross          = $6,240,000 (US/ Canada) 
}}
 spy novel Murderers Row (novel)|Murderers Row by Donald Hamilton, which was published in 1962. 
 The Silencers.

==Plot== world domination The Silencers. 
 You Only Live Twice, that also would begin by faking the lead characters demise and having his funeral.
 James Gregory) brainwash him. 

Posing as a Chicago gangster named Jim Peters, an alias of "Lash" Petroni, Helm travels to the French Riviera to follow his only lead, Solariss daughter, Suzie (Ann-Margret).

== Cast ==
* Dean Martin as Matt Helm
* Ann-Margret as Suzie
* Karl Malden as Julian Wall
* Camilla Sparv as Coco Duquette James Gregory as MacDonald
* Beverly Adams as Lovey Kravezit
* Richard Eastham as Dr. Norman Solaris
* Tom Reese as Ironhead
* Duke Howard as Billy Orcutt
* Ted Hartley as Guard
* Marcel Hillaire as Police Capt. Deveraux
* Corinne Cole as Miss January
* Robert Terry as Dr. Rogas
* Dean Paul Martin as Himself
* Desi Arnaz Jr. as Himself
* Billy Hinsche as Himself

==Production==
The film was the second of four produced by Albert R. Broccolis former partner Irving Allen and Martins Meadway-Claude Production company for Columbia Pictures in the mid-1960s starring Martin as secret agent Matt Helm. Euan Lloyd, a former Warwick Films publicity specialist and producer of The Poppy Is Also a Flower, assisted Allen in production chores.
 The Silencers, spoof of James Bond films than Hamiltons original serious spy story. Unlike Hamiltons world weary professional, Martin plays Helm with his own persona, a fun-loving, wise-cracking alcoholic playboy. 

Co-starring is Karl Malden as Dr. Julian Wall, whom New York Times film critic Bosley Crowther describes as a "Kansas type Julius No|Dr. No".  Malden had the idea that his character speaking in a different accent every time he spoke would be amusing. 
 starlet Camilla Sparv plays Maldens assistant Coco Duquette and Soon Tek-Oh makes a brief appearance as a Japanese agent killed in his bath.
 Herbert Baker, who had received a screen credit after he wrote the final version of The Silencers script, was brought in to rewrite Sauls first draft of Murderers Row and received sole credit.  Baker had written several Martin and Lewis screenplays and was a writer for The Dean Martin Show. 

The film was originally intended to be shot totally on location, but Martin, who also co-produced the film, refused to go to Europe.  Second unit teams shot sequences in Villefranche-sur-Mer, Monte Carlo and the Isle of Wight for the hovercraft and helicopter   sequences instead.  The then new Saunders-Roe SR.N6 hovercraft appearing in the films sea sequences (with the land chase through the streets of Monte Carlo and scenes with Martin using a mock up ) was provided by Hoverwork Hovercraft as their first assignment   with a Cushioncraft CC5 appearing as well. 
 titles are again by Wayne Fitzgerald, and James Curtis Havens continued in the series as second unit director. 
 mod 1966 costumes by Moss Mabry. Karl Maldens character uses a Gyrojet spearfiring pistol and a volley gun type pistol.  landau .  

==Reception== Laurel Awards for Best Action Drama and Best Action Performance for Martin. 
 The Ambushers The Wrecking Crew (1969). A fifth film, The Ravagers with Sharon Tate reprising her Wrecking Crew character and Dean Martin doing a dual role, was announced but never produced. Martin refused to make The Ravagers so Columbia reportedly held up Martins share of the profits on Murderers Row. 
 Diamonds Are The Spy Who Loved Me, the hovercraft chase on sea and land reoccurs with a gadget filled gondola in Moonraker (film)|Moonraker with that films evil mastermind Hugo Drax making jokes similar to Julian Wall.

== Soundtrack ==
 
The film score is by  Lalo Schifrin, replacing Elmer Bernstein.  In addition to the driving main theme and spy time score, Schifrin includes some jazz pieces, with one having a cover version by Bud Shank, as well as a song with lyrics by Howard Greenfield ("Im Not the Marrying Kind") for Martin that, due to contractual rights, didnt appear on the soundtrack album.  It did, however, appear on Martins LP, Happiness is Dean Martin.  

Billy Strange slightly changed Schifrins main title to be an "original" composition entitled "Spanish Spy" on his James Bond Double Feature album.

The pop group Dino, Desi & Billy (which featured Martins son, Dean Paul Martin, who calls Helm "Dad" in the film) makes an appearance and sings the Boyce & Hart song, "If Youre Thinkin What Im Thinkin".

== Footnotes ==
 

==External links==
*  
*  
*  
*  
* original teaser trailer http://www.youtube.com/watch?v=QQnQ8xX3-k8

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 