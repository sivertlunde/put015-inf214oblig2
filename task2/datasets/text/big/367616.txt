Pom Poko
{{Infobox film
| name           = Pom Poko
| image          = Pompokoposter.jpg
| alt            = 
| caption        = Japanese release poster
| director       = Isao Takahata Toshio Suzuki
| writer         = Isao Takahata
| starring       =  
| editing        = Takeshi Seyama
| music          = Shang Shang Typhoon
| studio         = Studio Ghibli
| distributor    = Toho
| released       =  
| runtime        = 119 minutes
| country        = Japan
| language       = Japanese
}} animated fantasy film, the eighth written and directed by Isao Takahata and animated by Studio Ghibli.

Consistent with   animals which occasionally wear clothes, and as cartoony figures based on the manga of Shigeru Sugiura (of whom Takahata is a great fan). They tend to assume their realistic form when seen by humans, their cartoony form when they are doing something outlandish or whimsical, and their anthropomorphic form at all other times.

Prominent testicles are an integral part of tanuki folklore, and they are shown and referred to throughout the film, and also used frequently in their shape-shifting. This remains unchanged in the DVD release, though the English dub (but not the subtitles) refers to them as "pouches". Also, in the English dub and subtitles, the animals are never referred to as "raccoon dogs", which is the more accurate English name for the tanuki, instead they are incorrectly referred to as just "raccoons".

==Plot== New Tama, in the Tama Hills on the outskirts of Tokyo. The development is cutting into their forest habitat and dividing their land. The story resumes in early 1990s Japan, during the early years of the Heisei era. With limited living space and food decreasing every year, the tanuki begin fighting among themselves for the diminishing resources, but at the urging of the matriarch Oroku ("Old Fireball"), they decide to unify to stop the development.

Several tanuki lead the resistance, including the aggressive chief Gonta, the old guru Seizaemon, the wise-woman Oroku, and the young and resourceful Shoukichi. Using their illusion skills (which they must re-learn after having forgotten them), they stage a number of diversions including industrial sabotage. These attacks injure and even kill people, frightening construction workers into quitting, but more workers immediately replace them. In desperation, the tanuki send out messengers to seek help from various legendary elders from other regions. 
 theme park takes credit for the parade, claiming it was a publicity stunt.
 senile and Buddhist dancing cult among the tanuki who are unable to transform, eventually sailing away with them in a ship that takes them to their deaths, while the other elder investigates joining the human world as the last of the transforming kitsune (foxes) have already done.

When all else fails, in a last act of defiance, the remaining tanuki stage a grand illusion, temporarily transforming the urbanized land back into its pristine state to remind everyone of what has been lost.  Finally, with their strength exhausted, the tanuki most trained in illusion follow the example of the kitsune: They blend into human society one by one, abandoning those who cannot transform. While the media appeal comes too late to stop the construction, the public responds sympathetically to the tanuki, pushing the developers to set aside some areas as parks. However, the parks are too small to accommodate all the non-transforming tanuki. Some try to survive there, dodging traffic to rummage through human scraps for food, while others disperse farther out to the countryside to compete with the tanuki who are already there.

  addresses the viewer, asking humans to be more considerate of tanuki and other animals less endowed with transformation skills, and not to destroy their living space; as the view pulls out and away, their surroundings are revealed as a golf course within a suburban sprawl.

==Cast==
Here are the Japanese / English voices:
* Narrator – Kokontei Shinchou / Maurice LaMarche
* Shoukichi – Makoto Nonomura / Jonathan Taylor Thomas
* Seizaemon – Norihei Miki / J. K. Simmons
* Fireball Oroku – Nijiko Kiyokawa / Tress MacNeille
* Gonta – Shigeru Izumiya / Clancy Brown Inugami Gyobu – Gannosuke Ashiya / Jess Harnell
* Bunta – Takehiro Murata / Kevin Michael Richardson Kincho Daimyoujin the Sixth – Beichou Katsura / Brian George Yashimano Hage – Bunshi Katsura / Brian George
* Abbot Tsurugame – Kosan Yanagiya
* Tamasaburo – Akira Kamiya / Wally Kurth
* Wonderland President – Takehiro Murata / Kevin Michael Richardson
* Osho – Andre Stojka
* Kiyo – Yuriko Ishida / Jillian Bowen
* Hayashi – Osamu Katou / Brian Posehn
* Ponkichi – Shōzō Hayashiya (9th) / David Oliver Cohen
* Ryutaro – Akira Fukuzawa / John DiMaggio
* Sasuke – Megumi Hayashibara / Marc Donato
* Koharu – Yorie Yamashita / Olivia dAbo
* Otama – Yumi Kuroda / Russi Taylor Mark Moseley
* News Anchor – Mark Moseley

==Reception== Japanese submission for the Academy Award for Best Foreign Language Film for that year.

==See also==
* Ponpoko

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 