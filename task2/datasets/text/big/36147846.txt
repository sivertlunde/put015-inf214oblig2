The Monitor (film)
 
{{Infobox film
| name           = The Monitor
| image          = 
| caption        = 
| director       = Pål Sletaune
| producer       = Turid Øversveen
| writer         = Pål Sletaune
| starring       = Noomi Rapace Kristoffer Joner Vetle Qvenild Werring Fernando Velázquez
| cinematography = John Andreas Andersen
| editing        = Jon Endre Mørk
| studio         = 4½ Film
| distributor    = SF Norge
| released       = October 7, 2011  (Norway) 
| runtime        = 96 minutes
| country        = Norway Germany Sweden
| language       = Norwegian Swedish English kr (United States dollar|US$4.05 million)
| gross          = 
|}} Swedish term for a baby monitor; the film maintained this title upon release in Europe and Australia. The film was released on October 7, 2011 in Norway, and was released direct-to-video in the United States on July 24, 2012.

==Plot==
Anna and her 8-year-old son Anders move into a large flat outside Oslo. Anna is concerned that they will be found by her violent husband. They are being monitored by a couple of child care workers. Anna wants Anders to sleep in her room for fear that he will be abducted. The child care workers force him to sleep in his own room.

Anna looks for a babycall (baby monitor) at an electronics stop and chats Helge (Kristoffer Joner),  a shy gentleman whose mother is dying in a hospital. The basis for this fragile relationship is that while Anna is an over-protective mother, Helge is an over-protected son who is unwilling to acknowledge the negative feelings he has about the way his mother treated him as a child. Compelled to feel charitable towards over-protective mothers, Helge is only too eager to ‘understand’ Anna’s refusal to let go of Anders. Similarly, Anna is only too eager to believe that an over-protected son might grow up to understand why his mother would not let him go.

The first night that Anders sleeps in his own room, Anna hears some voices on the babycall, including a lot of screaming, almost as if somebody was being murdered. Anna is terrified and runs into Anders room, to find that he is sound asleep. The next day, Anna returns to the electronics store and once again speaks to Helge, who said that her monitor could pick up other frequencies if they were close enough, meaning that the screams were emanating from somewhere inside the building. Anna thinks that she should home-tutor Anders, but the Child care workers insist that he needs to go to school.

The first day Anders went to school, Anna waited outside all day for Anders until she could take him home again. Unfortunately, the next day she tried the same thing, and the headmaster asks her to leave. When she returns to the school, Anders tells her that he has a friend that hes going to take home with them. Anna is fairly suspicious of the boy before she even knew his name. The boy hardly speaks at all, let alone to Anna.

The next day after hearing more noise from the babycall, and dropping Anders off at school, she gets on a bus and goes to the Café where Helge told her about his past. She then leaves to go to a lake near her flats, following a woman she believes had something to do with the screaming. While shes there, she sees a man drowning a little boy, who is in fact Anders friend. When they leave, Anna jumps into the water to try and retrieve the boy. She stays underwater for too long and blacks out. 

She wakes up in a hospital, with a nurse telling her she was found unconscious at the bus stop, though her clothes were wet. She then goes to Anders school to get him, but shes told to leave the child for the authorities to decide what to do. Nevertheless she rushes in, grabs Anders and takes him home.

She returns to the electrical store, where she asks Helge for a voice recorder so she can record the voices and sounds she hears on the babycall (monitor). He asks her to try it out, but she doesnt know what to say. Instead, he speaks into the recorder, asking Anna if shed like to go to dinner with him, only if she wanted to. He then plays it back to her, where Anna tells him shed love to but she cant leave Anders on his own. This is when she suggests that Helge come over for dinner at her flat. They go for a drink in the cafe, where she tells Helge that her mind is playing tricks on her, and she remembers things that never happened. Helge then proceeds to take a picture of her, telling her that way she would be able to remember the conversation they had.

When Anna gets home with Anders, he suddenly gets angry, telling Anna that they dont need Helge, ripping the picture of him off the fridge and ripping it to pieces.

One day Anna returns home from Anders school, and the front door is open. She suspects an intruder has entered the flat. She finds scissors and holds them in front of herself while she advances slowly, then she sees that the male social worker is there. He had let himself into Annas flat, telling her that he has keys from all the flats and that the female care worker had quit her job. He sees her with the scissors and says "Are you going to kill me now?". Anna lowers the scissors. The social worker then suggests that someone has to take care of a lonely woman as Anna is, and makes provocatively suggestive comments about returning later while pressing on her. 

Hours later, Helge visits Anna at her flat for their dinner arrangement, where they then have a meal and talk about things. Anna turns on the Babycall but Helge hears only baby crying, so does Anna to her seemingly great surprise. They hear knocking on the door, and Anna, thinking that its the male social worker, asks Helge to answer the door and tell the man that Anna was out. Helge does what she asks, but there is nobody at the door. Helge turns around to see Anders "Friend" whom Helge actually mistakes for Anders himself. The boy then tells Helge things about what happened to him, showing him bruises on his arm and abdomen. Helge asks the boy what happened, and the boy replies with "The same thing that happened to you as a child". Helge asks the boy how does he know what happened to Helge himself when he was a boy, but the boy closes the door of Anders room. Helge storms into the kitchen and demands that Anna tell him what she had told Anders, not knowing that it wasnt really him. Anna replies that she hasnt told him anything, and Helge decides to get Anders and ask him again, in front of Anna. Anna screams at Helge that Anders is her son, and he has no right to go barging in. Helge hurries out of the flat, leaving Anna terrified. A bit later, Anna opens the window and calls Helge back, but he hurries off.

The next day, Anna turns up at the electronics store to apologies, but the shop worker tells her that Helge is at the hospital, because his mother is dying.

Anders tells Anna that hes seen his father, and they hear a knocking on the door. Anna picks up scissors and answers it, seeing that its the male care worker, who tells her that her husband is on his way to take his son away from her. She stabs him in the throat, but later Helge turns up calling from downstairs, whom Anna mistakes for her husband. When Helge sees the careworker, he tells him that hes a caretaker. Helge runs into the apartment, to see Anna sitting on the windowsill with Anders arms wrapped around her. Just as he reaches Anna, she leans forward and falls off the windowsill. When Helge rushes to the bottom, he sees Anna prostrated on the floor, but no Anders. When he asks her where he is, she isnt able to answer. Later Helge is being told that Annas husband killed Anders years ago, and that her husband had killed himself as well. 

Helge goes to the forest and finds a body wrapped in blue dug in the earth. Later Helge sits next to the Annas body and retells the story of a boy and his mother. The ending scene shows happy Anders and Anna strolling in the forest towards the lake, then sitting down by the water.

==Reception==
The film received generally positive reviews; it maintains a 73% "Fresh" score from 22 reviews on review aggregator Rotten Tomatoes.    Several critics singled out Rapaces performance for praise while giving mixed reviews to the film. 

Kim Newman of Empire (film magazine)|Empire called it "genuinely creepy" and summarised it as "another Scandinavian thriller that will nestle uncomfortably in your head",  while Philip French of The Observer judged "The film and Rapace command our attention, though at the end one feels Norway has used up the whole of this years quota of red herrings."  Peter Bradshaw of The Guardian was less positive, writing "Its a great idea for a thriller - but then other plotlines get muddled in, and everything unravels into a cop-out."  Henry Fitzherbert of the Daily Express shared a similar sentiment, opining "Its a great idea but there are too many confusing sup-plots and no satisfactory resolution." 

==References==
 

==External links==
*  
*  

 
 
 
 