Borderland (film)
 Borderland}}
 
{{Infobox film
| name           = Borderland
| image          = Borderland ver2.jpg
| caption        = Promotional film poster
| director       = Zev Berman
| producer       = Deborah Davis
| writer         = Zev Berman
| starring       = Brian Presley Jake Muxworthy Rider Strong Damián Alcázar Sean Astin
| music          = 
| cinematography = 
| editing        = 
| studio         = Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor    = After Dark Films Lionsgate
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Adolfo de Jesús Constanzo, the leader of a religious cult that practiced human sacrifice.   Costanzo and his followers kidnapped and murdered University of Texas Junior (education)|junior, Mark J. Kilroy, in the spring of 1989.

==Plot==
The action begins with Mexico City policemen banging on the door of what seems to be an abandoned house. Ulises (Damian Alcazar) and his partner enter the house and find gruesome remnants of animal sacrifices and human remains.  The two are ambushed by the occupants and Ulises is forced to watch them torture and mutilate his partner until he is decapitated. Ulises is shot in the leg and is allowed to live to warn other law enforcement officials to stay out of their way.

One year later, Ed (Brian Presley), Henry (Jake Muxworthy) and Phil (Rider Strong), three recent Texas college grads, are enjoying a college beach bonfire in Galveston, Texas. They decide to head down to Mexico for the week to hit up the strip clubs and take advantage of a lack of law enforcement.
 hallucinogenic mushrooms before going to a carnival. Phil leaves early to give the prostitutes baby a teddy bear, and as he walks from the carnival alone, Phil reluctantly gets into a car with a couple of men who proceed to abduct him when he tries to leave.
 Palo Myombe and are preparing a human sacrifice (a "gringo", as opposed to the regular Mexican citizens they have been sacrificing) to get the power of Nganga for their drugs to be invisible to the border guards while smuggling them into the US. 

Henry is later hacked to death by several men with machetes on the roof of their hotel, and Ed and Valeria decide to go with Ulises to go kill the men who abducted Phil. By then, it is too late to save Phil, however that doesnt stop Ulises from shooting the leader of the cult to death after being shot himself.

Ed, Valeria, and Ulises travel down the road to a house inhabited by an old man, where Ulises bleeds to death. The cult members followed Ed and Valeria to the house, and the two risk their lives to kill the remaining members, eventually deciding to swim across the Rio Grande, two kilometers north of their location.

The movie ends with a caption explaining that several kilos of cocaine were found in containers along with human hair, over fifty bodies were exhumed from a mass grave at the ranch, Ed and Valeria were questioned after being caught swimming across the river, and that several suspects remain at large.

==Cast==

The principal cast members of Borderland were:  

*Brian Presley as Ed
*Rider Strong as Phil
*Jake Muxworthy as Henry
*Beto Cuevas as Santillan
*Martha Higareda as Valeria
*Sean Astin as Randall
*Mircea Monroe as Nancy
*Damián Alcázar as Ulises
*Marco Bacuzzi as Gustavo Roberto Sosa as Luis
*José María Yazpik as Zoilo
*Humberto Busto as Mario
*Elizabeth Cervantes as Anna
*Francesca Guillén as Lupe
*Alenka Rios as Amelia

==Release==
 
  After Dark Horrorfest 2007.

==Reception==
 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 