Mayerling (1968 film)
 
{{Infobox film
| name           = Mayerling
| image          = MAYERLING POSTER.jpg
| caption        = Theatrical release poster by Tom Jung Terence Young
| producer       = Robert Dorfmann Maurice Jacquin Terence Young (screenplay) Denis Cannan (dialogue) Joseph Kessel (uncredited)
| starring       = Omar Sharif Catherine Deneuve James Mason Ava Gardner
| music          = Francis Lai (original)   Aram Khachaturian (non-original; Adagio from Spartacus (ballet)|Spartacus
| cinematography = Henri Alekan
| editing        = Monique Bonnot Associated British Picture (UK) Winchester-Corona Productions (France)
| distributor    =  Warner-Pathé Distributors|Warner-Pathé (UK) Valoria Films (France)
| released       = 1968 (France UK) 
| runtime        = 140 min
| country        = UK / France English
| budget         = $5,000,000 (estimated)
| gross          = $14,754,720 
}}
 1968 romantic romantic drama tragedy film Terence Young. The film was made by Les Films Corona and Winchester and distributed by MGM.

It was based on the novels Mayerling by Claude Anet and LArchiduc by Michel Arnold and the 1936 film Mayerling (1936 film)|Mayerling, directed by Anatole Litvak, which dealt with the real-life Mayerling Incident. Although not completely historically accurate, the movie was well received, in part because of its lavish sets and costumes.

==Plot==
 Empress Elisabeth (Gardner), over implementing progressive policies for their country. Rudolf soon feels he is a man born at the wrong time in a country that does not realize the need for social reform. The Prince of Wales (Robertson Justice), later to become United Kingdom of Great Britain and Ireland|Britains King Edward VII, provides comic relief.
 Princess Stéphanie (Parisy) by taking a mistress, Baroness Maria Vetsera (Deneuve). Their untimely demise at Mayerling, the imperial familys hunting lodge, is cloaked in mystery, but the films ending suggests the two lovers made a suicide pact when they decided they could not live in a world without love or prospects for peace.

==Cast== Crown Prince Rudolf Baroness Maria Vetsera Emperor Franz Josef Empress Elisabeth Prince Of Wales Countess Larisch Princess Stéphanie
*Ivan Desny  - Count Josef Hoyos
*Fabienne Dali  - Mizzi Kaspar
*Véronique Vendell  - Lisi Stockau Prince Montenuovo
*Irene von Meyendorf  - Countess Stockau
*Mony Dalmes - Baroness Helen Vetsera
*Bernard Lajarrige - Loschek
*Maurice Teynac - Moritz Szeps
*Charles Millot - Count Taafe Jacques Berthier - Archduke Jean Salvator
*Roger Pigaut - Count Karolyi
*Véronique Vendell : Lisl Stockau
*Lyne Chardonnet : Hannah Vetsera Moustache : Bratfish
*Roger Lumont - Inspector Losch
*Jacqueline Lavielle - Marinka
*Alain Saury - Baltazzi
*Jean-Claude Bercq - Michel de Bragance
*Jean-Michel Rouzière 
*Jacques Ciron 
*Liane Daydé 
*Friedrich von Ledebur

==See also==
*Mayerling (1936 film)|Mayerling (1936) feature film directed by Anatole Litvak
*Mayerling (1957 TV film)|Mayerling (1957) TV film also directed by Litvak

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 