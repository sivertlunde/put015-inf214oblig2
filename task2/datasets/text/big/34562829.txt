Finding Me: Truth
{{Infobox film
| name           = Finding Me: Truth
| image          = 
| alt            = 
| caption        = 
| director       = Roger S. Omeus Jr.
| producer       = 
| writer         = Roger S. Omeus Jr.
| starring       = RayMartell Moore JNara Corbin Derrick L. Briggs Eugene Turner Maurice Murrell Ron DeSuze
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = TLA Releasing
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Finding Me: Truth is a 2011 drama film by director Roger S. Omeus Jr. and a sequel to the 2009 Finding Me. 

==Plot==
A year later, Faybien has been keeping in touch with Lonnie by writing him numerous letters, updating him on whats been going on with he and his friends. He never receives a letter in return, assuming that Lonnie has moved on. Faybiens life has changed as he has ended his dead end job at the mall. As for his friends, Amera and Greg, their lives have changed as well. Amera has found love with her boyfriend of four months, Gabe, and awaits the release of her new album. Unfortunately, Greg has lost his job and has trouble finding employment. To ease the employment woes, he has entered a blossoming relationship with Reggie Hunt, a struggling medical student, and a "sex only" arrangement with Tammy Jones, unbeknownst to Amera, Tammys cousin.

==Cast==
* RayMartell Moore as Faybien Allen
* Derrick L. Briggs as Lonnie Wilson
* JNara Corbin as Amera Jones
* Eugene E. Turner as Greg Marsh
* Maurice Murrell as Jaylen Jay Timber
* Ron DeSuze as Wilmar Allen
* Eric Joppy as Reggie Hunt

==Reception==
WBOC-TV gave the film a positive review and commented that it was "fun and funny, a finger-snapping, dulcetly down-low, romantic comedy." 

==References==
 

==External links==
*  

 
 
 
 
 
 


 