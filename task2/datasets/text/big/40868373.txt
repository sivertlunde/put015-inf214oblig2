Fire in the Blood (2013 film)
{{Infobox film
| name =Fire in the Blood
| image =Movie Poster, "Fire in the Blood".jpg
| image_size =
| border =
| alt =
| caption =
| film name =
| director =Dylan Mohan Gray
| producer =Dylan Mohan Gray
| writer =Dylan Mohan Gray
| screenplay =
| story =
| based on =  
| narrator =William Hurt
| starring =
| music =Ashutosh Phatak
| cinematography =Jay Jay Odedra
| editing =Dylan Mohan Gray
| studio = Sparkwater India
| distributor =
| released =  
| runtime =87 min.
| country =India
| language =English
| budget =
| gross =
}}
Fire in the Blood is an internationally-acclaimed 2013 documentary by Dylan Mohan Gray depicting the mass devastation brought about in Africa, Asia and other parts the global South due to intentional obstruction of low-cost antiretroviral drugs used for treatment of HIV/AIDS from reaching people in these countries, spearheaded by Western pharmaceutical companies armed with patent monopolies and the governments (above all those of the United States, European Union and Switzerland) doing their bidding. The documentary also shows how the battle against this blockade, estimated to have resulted in ten- to twelve million unnecessary deaths, was fought and (at least for the time being) won.
 James Love, Peter Rost, Ugandan AIDS physician Peter Mugyenyi, and Nobel Prize-laureates Desmond Tutu and Joseph Stiglitz.  

The film is narrated by Academy-Award winning actor William Hurt, who lent his voice to the film on a pro bono basis because he felt the story and subject matter were so important.

In November 2013, Fire in the Blood set a new all-time record for the longest theatrical run by any non-fiction feature film in Indian history (five weeks).   Re-linked 2014-11-01    Re-linked 2014-11-01 

It is the first Indian non-fiction feature to be theatrically released in either the US or the UK. 

==Production==
The Indian-Irish film director Dylan Mohan Gray first came to know of the issue in 2004, after he read an article in the The Economist about the battle between pharmaceutical companies and the global public health community over access to lower-cost AIDS drugs for Africa.  He decided to make the film three years later. 

The film was shot on four continents from March 2008 to the end of 2010, while editing was completed in 2012.   

==Release==
The film was first released theatrically in Ireland on 21 February 2013, with the UK premiere the following day. It was released theatrically in the US on 7 September 2013 and in India on 11 October the same year, to outstanding reviews. {{cite web accessdate = 2013-11-08}}    Re-linked 2014-11-01 
 VOD via its website in 2014.  It released in the UK and India on DVD in the first quarter of 2014.

Thus far the film has been broadcast on  ), Israel (Yes (Israel)), Norway (NRK), Spain (Televisión Española|TVE), Switzerland (Schweizer Radio und Fernsehen|SRF), Austria (ORF (broadcaster)|ORF), Poland (Telewizja Polska), Ireland (TG4), Brazil (Globosat) and Denmark (Danmarks_Radio|DR).  Transnational broadcasters include Al Jazeera English|AJE, Al Jazeera Arabic and Al Jazeera Balkans.

Fire in the Blood was the first Indian film to be selected for the World Cinema Documentary competition at the Sundance Film Festival  and subsequently participated in numerous leading film festivals in dozens of countries all over the world.   Re-linked 2014-11-01 

==Awards and nominations==
* Nominated for the Grand Jury Prize, World Documentary at the Sundance Film Festival, January 2013   
* Shortlisted for the One World Media Award, February 2013 
* Winner of the Justice Matters Award at the 27th Washington DC International Film Festival, April 2013 
* Winner of the DOXA Feature Documentary Award at the DOXA Documentary Film Festival, May 2013    Grierson British Documentary Awards, July 2013  
* Winner of the Friedrich-Ebert-Stiftung Prize for Political Film at Filmfest Hamburg, October 2013  
* Winner of the Dadasaheb Phalke Chitranagari Award for Best Debut Film for a Director at the Mumbai International Film Festival, February 2014 
* Winner of the Audience Award for Best Documentary at the 16th Fairy Tales International Queer Film Festival in Calgary, June 2014
* Winner of the Best Documentary Award at the International Film Festival of Kashmir (IFFK) in Srinigar, August 2014
* Winner of the Grand Jury Award at the The White Sands International Film Festival, New Mexico, September 2014
* Winner of the Best Feature Documentary Award at the Montréal International Black Film Festival, September 2014
* Winner of the IDPA Gold Award for Excellence in Nonfiction (Direction), 11th Indian Documentary Producers Association (IDPA) Awards, Mumbai, March 2015
* Winner of the IDPA Silver Award for Excellence in Nonfiction (Script), Mumbai, March 2015 
* Winner of IDPA Jury Awards for Excellence in Cinematography (Jay Odedra), Excellence in Sound Design (Kunal Sharma), Mumbai, March 2015
* Winner of IDPA Jury Award for Best Film: Documentary over 30 Minutes, Mumbai, March 2015

==Critical response==
Fire in the Blood received very positive critical notices, both to its North American premiere at the 2013 Sundance Film Festival, as well as to its subsequent theatrical releases in Ireland, United Kingdom|Britain, the USA and India. 

The influential film review aggregator website Rotten Tomatoes has given Fire in the Blood a "92% fresh" rating based on 24 reviews as of January 2014.   This ranks the film within the 5-10% best-reviewed films of 2013. 

Certain critics, such as Gary Goldstein of the Los Angeles Times, while acknowledging the enormous importance of the topic, felt the film should have taken a more emotional approach to its "incendiary subject".  Many others, however, such as the legendary English critic Philip French, who in his review for The Observer described the film as "quietly devastating", praised Grays choice in avoiding a polemical tone and allowing the material to speak for itself. 

Writing in Sight & Sound, Ashley Clark called Fire in the Blood "stirring" and added "Gray deserves credit for his own restraint... Such is the clarity of his ideological stance that any grandstanding would feel redundant." 

David Rooney of the Hollywood Reporter echoed this view, stating that "the admirable balance between impassioned argument and clear-sighted reporting in Dylan Mohan Grays chronicle of the why and how makes Fire in the Blood indispensable viewing", adding that the "very smart", "extremely moving" film is "a shocking account of international trade terrorism sanctioned by Western governments" and "a powerful documentary that demands to be seen by as wide an audience as possible." 

==Other reactions==
Author John le Carré (who became involved with the issue of pharmaceutical company abuses while researching his landmark 2001 novel The Constant Gardener) called Fire in the Blood "a blessing... full of conviction, passion and unanswerable argument". 

Australian-British journalist and documentary film maker, John Pilger wrote "Fire in the Blood  is one of the most powerful, important and humane documentaries I have ever seen. Its the story of ordinary people standing up to unaccountable power. The struggle to save millions from the ravages of untreated HIV is revealed as a struggle against the new lords of the world, transnational corporations, their greed and lies. Genuine hope is rare these days -- youll find it in this film."

Former (2001–06) United Nations (UN) Special Envoy for HIV/AIDS in Africa Stephen Lewis said "I was enraged as I watched, thinking of those years I spent as the Envoy, watching people die.   I rarely watch AIDS documentaries; theyre remarkably repetitive as a rule, largely uninspired and yielding almost nothing new.   is in a wholly different category; a terrific, riveting documentary... dramatic, compelling, but most of all, wonderfully humane.   a remarkably gifted documentary film-maker."

==Special screenings== Washington and New Delhi, and one hosted by the Indian Ministry of External Affairs (MEA) for ambassadors and consuls accredited to India.

==Milestones==
* (November 2013) Fire in the Blood sets a new all-time record for the longest theatrical run by any non-fiction feature film in Indian history  http://www.shorenewstoday.com/snt/news/index.php/regional/events/51565-documentary-on-aids-drugs-to-be-shown-at-pac-.html  
* 1st non-fiction film on a global scale produced out of India (shot in 8 countries/4 continents) http://bollyspice.com/69171/fire-blood-wins-first-ever-prize-political-film-hamburg 
* 1st non-fiction feature from India to be theatrically released in either the US or UK  
* only feature-length Indian film to be selected in the main competition at a Top 5 international film festival between 2010 (Peepli Live) and 2014 (Liars Dice)  
* official selection at over 80 top film festivals in 45 countries (including Sundance, Rio, Valladolid, Mumbai, Vancouver, Washington, Tel Aviv, Thessaloniki, Sheffield, Hamburg, Zurich, Copenhagen, etc.) http://fireintheblood.com/festivals 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 