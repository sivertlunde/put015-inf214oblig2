The Forbidden Girl
{{Infobox film
| name           = The Forbidden Girl
| film name      =  
| image          = The Forbidden Girl film poster 2013 film.jpg
| alt            = 
| caption        = 
| director       = Till Hastreiter
| producer       = 
| writer         = Till Hastreiter Maximilian Vogel Philipp Wolf
| starring       = Peter Gadiot Jytte-Merle Böhrnsen Klaus Tange
| music          = Eckes Malz
| cinematography = Tamás Keményffy
| editing        = Florian Drechsler Thomas Krause
| production companies = EMP 1. Stereofilm European Motion Pictures
| distributor    = Shoreline Entertainment
| released       =  
| runtime        = 99 minutes
| country        = Germany
| language       = English
| budget         = 
| gross          =  
}}
The Forbidden Girl ( ) is a 2013 German horror film that was directed by Till Hastreiter. The film was released in Germany on 7 February 2013 and was released to video in the United States on 26 August 2014.  It stars Peter Gadiot as a young man that finds himself fascinated by a woman who seems to be the mirror image of his former sweetheart, who was carted off by a demon six years ago.

==Synopsis==
Toby McClift (Peter Gadiot) is the son of a strict preacher (Roger Tebb) who continually warns Toby that he must never ever fall in love or have anything to do with love. Despite these strict warnings, Toby falls in love with Katie (Jytte-Merle Böhrnsen) and makes plans to meet up with her in a cemetery one night. Just as they are to meet, Katie is swept away by a demonic beast and is never seen again. Toby tries to convince everyone that her disappearance is due to a beast, but nobody believes him, least of whom is his father, and hes placed in a mental institution for the next six years. Once hes released he takes a job in an old castle serving as a tutor for Laura (also played by Böhrnsen), the niece of the elderly Lady Wallace (Jeanette Hain). Toby is struck by how much Katie and Laura look alike, to the point where hes certain that the two are one and the same. Hes saddened when Laura claims not to know him, but becomes intrigued by how contradictory her nature is, as she is both sensuality and innocence. At the same time Toby is increasingly unnerved by how strange everything is in the house, including the actions of Lady Wallaces butler Mortimer (Klaus Tange) and several increasingly bizarre visions. He also notices that the longer he stays in the house, the younger Lady Wallace seems to appear. Eventually this becomes too much for Toby and he unsuccessfully tries to perform an exorcism on Laura, convinced that she is being possessed by Lady Wallace. Humiliated by his failure, Toby tries to leave but is stopped by Laura, who confesses that she is a witch that requires a pure, innocent being in order to be young forever and that she needs Toby in order to accomplish this.

==Cast==
*Peter Gadiot as Toby McClift
*Jytte-Merle Böhrnsen as Laura/Katie
*Klaus Tange as Mortimer
*Jeanette Hain as Lady Wallace
*Marc Bischoff as Crazy Man
*Roger Tebb as Reverend McClift

==Reception==
Critical reception for The Forbidden Girl has been mixed.    Bloody Disgusting and Dread Central both heavily panned the film,  with Dread Central writing that "If I had to search for a lone beacon of light here, it would be the backdrop of the castle itself, giving off a retro-Italian horror movie feel; however, the lame CGI and dreadful performances bring this film back down to substandard levels of mediocrity. When all was said and done, the only thing ‘”forbidden” about this movie should be the ability to sit through it again."  

The Austin Chronicle was more positive in their review, stating "Sure, there are some shoddy CGI moments, including a lot of inexplicable shots of the sky going red or purple. But then Hastreiters longtime cinematographer Tamás Keményffy suddenly pulls off a surprisingly beautiful image. Its inexplicable that suddenly something so elegant and well-crafted appears in the midst of lumpen cinematic mud, but it happens."  HorrorNews.net gave a favorable review for the work, commenting that it was "definitely the kind of film that you either love it or hate it with no middle ground" and that it would appeal to "a true fan of gothic horrors". 

==References==
 

==External links==
*  
*  

 
 
 
 