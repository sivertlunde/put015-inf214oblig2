Los últimos de Filipinas
{{Infobox Film
| name           = Los últimos de Filipinas
| image          = 
| image_size     = 
| caption        = 
| director       = Antonio Román
| producer       = 
| writer         = Pedro de Juan , Antonio Román 
| narrator       = 
| starring       = Armando Calvo José Nieto (actor)|José Nieto Fernando Rey Guillermo Marín Manolo Morán Conrado San Martín Tony Leblanc Nani Fernández
| music          = Manuel Parada Heinrich Gärtner       
| editing        = Bienvenida Sanz  
| distributor    = 
| released       = 1945 
| runtime        = 99 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}} 1945 Spain|Spanish biographical war film directed by Antonio Román. It is based on a radio script by  Enrique Llovet; Los Héroes de Baler; and novels by  de Enrique Alfonso Barcones El Fuerte de Baler; and Rafael Sánchez Campoy El Fuerte de Baler.

Nani Fernández played famous Yo Te Diré song.

==Historical Facts==

 

"The Last Ones of the Philippines"  is the name given to the Spanish soldiers who fight in the Siege of Baler against the supporters of independence and against the US Army (the latter was in the Spanish-American War.  In Spain, it was called "The Disaster of 98").

The Siege of Baler lasted from the 1st of July, 1898 until the 2nd of June, 1899.  During these 11 months, the Spaniards were isolated in a church that became their fortified position. The Spanish troops were a small garrison of 50 soldiers from the "2º de Cazadores" under the charge of Lieutenant D. Juan Alonso Zayas.  They faced approximately 800 rebel soldiers. The Spanish soldiers fortified the church and resisted the constant attacks of the rebels for 11 months without provisions.

==Cast==
*Armando Calvo	 as  	Teniente Martín Cerezo
*José Nieto (actor)|José Nieto	as	Capitán Enrique de las Morenas
*Guillermo Marín	as 	Doctor Rogelio Vigil
*Manolo Morán	as 	Pedro Vila
*Juan Calvo	as 	Cabo Olivares
*Fernando Rey	as 	Juan Chamizo
*Manuel Kayser	as 	Fray Cándido
*Carlos Muñoz	as 	Santamaría
*José Miguel Rupert	as 	Moisés
*Pablo Álvarez Rubio	as 	Herrero, el desertor
*Nani Fernández	 as 	Tala
*Emilio Ruiz de Córdoba	  as 	El Correo
*César Guzmán	 as 	Jesús García Quijano
*Alfonso de Horna	as 	Marquiado
*Manuel Arbó	as 	Gómez Ortiz

==External links==
*  
* 
*  (in English and Spanish)

 
 
 
 
 
 
 
 
 
 

 
 