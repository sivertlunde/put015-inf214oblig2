Gold Mine in the Sky
{{Infobox film
| name           = Gold Mine in the Sky
| image          = Gold_Mine_in_the_Sky_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Joseph Kane
| producer       = Charles E. Ford (associate)
| screenplay     = {{Plainlist|
*Betty Burbridge
* Jack Natteford
}}
| story          = Betty Burbridge
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette Carol Hughes
}}
| music          = Alberto Colombo
| cinematography = William Nobles
| editor         = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 59 minutes
| country        = United States
| language       = English
}} Western film Carol Hughes. Based on a story by Betty Burbridge, the film is about a singing cowboy and ranch foreman who, as executor of the owners will, must see that the daughter and heiress does not marry without his approval.   

==Plot== will of Craig Reynolds), but Gene refuses, prompting Cummings to attempt to have Gene killed. When this fails, Cummings then demands money so Cody stages a fake kidnaping to raise the funds. However, her plan backfires after Cummings learns about it and turns it into a real kidnaping.

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Carol Hughes as Cody Langham
* J.L. Franks Golden West Cowboys as Musicians and Cowhands   Craig Reynolds as Larry Cummings
* Helen Ainsworth as Jane Crocker  
* LeRoy Mason as Red Kuzak  
* Frankie Marvin as Cowhand Joe
* Robert Homans as Lucky Langham
* Eddie Cherkose as Sykes
* Ben Corbett as Spud Grogan
* Milburn Morante as Mugsy Malone
* Jim Corey as Henchman Chet
* George Guhl as Constable Cy
* The Stafford Sisters as The Levinsky Trio
* Champion as Genes Horse (uncredited)   

==Production==
===Stuntwork===
* Nellie Walker
* Joe Yrigoyen 

===Filming locations===
* Keen Camp, State Highway 74, Mountain Center, San Jacinto Mountains, California, USA
* Lake Hemet, State Highway 74, San Bernardino National Forest, California, USA   

===Soundtrack===
* "Theres A Gold Mine in the Sky" (Charles Kenny, Nick Kenny) by Gene Autry and Cowhands
* "Hummin When Were Comin Round the Bend" (Eddie Cherkose, Alberto Colombo) by Gene Autry, Smiley Burnette, Fred Snowflake Toones, Jack Kirk and Cowhands
* "Theres No Place Like Home (Home, Sweet Home)" (H.R. Bishop, John Howard Payne) by Smiley Burnette (a cappella) and Helen Ainsworth (whistling)
* "Thats How Donkeys Were Born" (Eddie Cherkose, Smiley Burnette) by Smiley Burnette, Frankie Marvin, and J.L. Franks Golden West Cowboys
* "Frühlingslied (Spring Song) Op.62 #6" (Felix Mendelssohn-Bartholdy) by The Stafford Sisters
* "Dude Ranch Cowhands" (Gene Autry, Fred Rose, Johnny Marvin) by Gene Autry, Smiley Burnette, The Stafford Sisters, and J.L. Franks Golden West Cowboys
* "Id Love to Call You My Sweetheart" (Joe Goodwin, Larry Shay, Paul Ash) by Gene Autry
* "Hike Yaa Move Along" (Smiley Burnette) by Gene Autry, Smiley Burnette, and Cowhands
* "Tumbleweed Tenor" (Eddie Cherkose, Smiley Burnette) by Smiley Burnette, Frankie Marvin, and J.L. Franks Golden West Cowboys
* "As Long as I Have My Horse" (Gene Autry, Fred Rose, Johnny Marvin) by Gene Autry   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 