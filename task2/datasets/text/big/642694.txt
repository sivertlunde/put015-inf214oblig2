Beethoven's 2nd
 
 
{{Infobox film
| name           = Beethovens 2nd
| image          = beethovens 2nd.jpg
| caption        = Theatrical release poster
| director       = Rod Daniel
| producer       = Michael C. Gross Joe Medjuck
| writer         = Len Blum
| starring       = Charles Grodin Bonnie Hunt Nicholle Tom Christopher Castile Sarah Rose Karr Debi Mazar Chris Penn
| music          = Randy Edelman Bill Butler
| editing        = William D. Gordean Sheldon Kahn
| studio         = Northern Lights Entertainment Universal Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $118,243,066
}}

Beethovens 2nd is a 1993 family film directed by Rod Daniel, and the first sequel to the 1992 film, Beethoven (film)|Beethoven. It starred Charles Grodin, Bonnie Hunt, and Debi Mazar. It is the second of eight installments in the Beethoven (film series)|Beethoven film series. Initially, no theatrical sequel to Beethoven was planned, but it was produced after the unexpected financial success of it. It is the last one in the franchise to be released theatrically, as well as the last to feature the original cast.

==Plot==
The film begins in the home of the Newton family, where George, Alice, their three children, and Beethoven are all well adjusted to living together. Early on, Beethoven meets Missy, a female St. Bernard whose owners are attempting to settle a divorce. Regina, who is seeking $50,000 in the settlement, has retained full custody of Missy, planning to transfer her to Brillo, her soon-to-be ex-husband, once the divorce is finalized.
 pedigree St. Bernards, Regina might be able to sell them and make a lot of money.

Thinking Regina plans to drown the puppies, Ted and Emily carry them home in a box and hide them from George, who they do not think wants them to deal with. Angered by their disappearance, Regina plans revenge. Ryce, Ted, and Emily, take it upon themselves to feed and care for them, even getting up in the middle of the night and sneaking out of school to do so.

Eventually, George and Alice discover the puppies; George, angry at first, reluctantly agrees to keep them until they are mature. At this point, they are presumably 8–10 weeks old, the children give name them Chubby, Dolly, Tchaikovsky, and Moe, and George re-experiences the ordeals of dealing with growing dogs.

Facing significant financial difficulty, the family is offered a free stay in a lakefront house at the mountains owned by one of Georges business associates. Beethoven and the puppies, somewhat calmed down, go along on the vacation. Ryce attends a party with friends where she is exposed to vices of teen culture such as binge drinking and getting locked in her former boyfriend Taylor Devereauxs bedroom against her will. Beethoven tears the houses patio deck apart, removing her from potential danger.

Regina and her new boyfriend, Floyd, are staying in a location unknown to Brillo, coincidentally near the Newtons vacation residence. They go to a county fair with the dogs and the children persuade George to enter a burger eating contest with Beethoven, which they win. By happenstance, Regina and Floyd were there and had left Missy behind in their car.

Missy escapes from the car with Beethovens help while Regina sneaks behind the children and snatches the puppies from them. Beethoven and Missy run into the mountains, followed by Regina and Floyd. The family follows, eventually catching up with Regina, Floyd, Beethoven, Missy, and the puppies. Floyd threatens to drop the puppies in the river below and George says the situation does not have to get ugly. Floyd pokes George in the chest with a stick, but Beethoven charges into it, ramming it into Floyds crotch. He loses his balance, Regina grabs his hand, and they fall over the cliff into a pool of mud, which breaks, thus they are swept away in the river.

Five months later, Brillo visits the family with Missy who overheard what happened at the mountains, revealing that the judge in the divorce had granted him full custody of her and denied Reginas claim. The puppies, almost grown up by then, run downstairs to see Missy as the film ends.

==Cast==
* Charles Grodin as George Newton
* Bonnie Hunt as Alice Newton
* Nicholle Tom as Ryce Newton
* Christopher Castile as Ted Newton
* Sarah Rose Karr as Emily Newton
* Debi Mazar as Regina
* Chris Penn as Floyd
* Ashley Hamilton as Taylor Devereaux
* Danny Masterson as Seth
* Catherine Reitman as Janie
* Maury Chaykin as Cliff Klamath
* Heather McComb as Michelle
* Felix Heathcombe as Percy Pennypacker
* Scott Waara as Banker
* Jeff Corey as Gus, the Janitor
* Virginia Capers as Linda Anderson
* Jordan Bond as Jordan, the Newspaper Boy
* Pat Jankiewicz as Arthur Lewis
* Damien Rapp as Teen Heckler
* Kevin Dunn as Brillo (uncredited)

Christopher Masterson had a small role in the film. However, his older brother, Danny Masterson, had a leading role in it. They were not playing brothers in it so neither one mentioned to anyone that they were related. When producers went to view it, they noticed the resemblance and reshot all of his scenes with another actor. 

==Production== Glacier National South Pasadena. 

===Song===
The theme song, "The Day I Fall in Love", performed by James Ingram and Dolly Parton, was nominated for an Academy Award, a Golden Globe, and a Grammy Award for Best Song from a Motion Picture.

==Reception==
The film gained a negative response.  It currently holds a score of 27% on Rotten Tomatoes. It grossed more than $118 million at the box office worldwide.

==In other media==
In 1994 a side-scrolling video game titled simply Beethoven, but based on Beethovens 2nd, was developed for the Sega Genesis  and Game Boy.  Though completed, it was cancelled before release.

==References==
 

== External links ==
*  
*  
*  
* Beethoven (film series)

 
 

 
 
 
 
 
 
 