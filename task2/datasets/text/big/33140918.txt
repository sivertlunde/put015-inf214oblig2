Cloudburst (2011 film)
 
{{Infobox film
| name = Cloudburst
| image = Cloudburst (2011 film) posater.png
| image_size = 215px
| alt = 
| caption = Poster
| director = Thom Fitzgerald
| producer = Thom Fitzgerald Doug Pettigrew
| screenplay = Thom Fitzgerald
| based on =  
| starring = Olympia Dukakis Brenda Fricker Ryan Doucette
| music = Jason Michael MacIsaac Warren Robert
| cinematography = Tom Harting
| editing = Angela Baker Emotion Pictures Sidney Kimmel Entertainment
| released =  
| runtime = 93 minutes
| country = United States Canada
| language = English
}}
Cloudburst is a 2011 Canadian-American  , September 13, 2011.  The film is an adaptation of Fitzgeralds 2010 play of the same name. The films cast also includes Kristin Booth, Ryan Doucette, John Dunsworth, and Jeremy Akerman.

==Plot==
Stella and Dotty are a lesbian couple from Maine who embark on a Thelma and Louise-style road trip to Nova Scotia to get married after Dotty is moved into a nursing home by her granddaughter. 

==Stage play==
Cloudburst debuted as a stage play on April 8, 2010 at Plutonium Playhouse in Halifax, Nova Scotia.  The play starred Carroll Godsman, Deborah Allen, Ryan Doucette, Marlane OBrien, Michael McPhee and Amy Reitsma. The successful engagement ran for five weeks and closed on May 8, 2010. The production was nominated for several Merritt Awards, Nova Scotias professional theatre awards, including nominations for Outstanding Production, Outstanding New Play (Fitzgerald) Outstanding Lead Actress (Allen), Outstanding Supporting Actor (Doucette),  and Outstanding Set Design (Fitzgerald).  Fitzgerald won the Merritt Award for Outstanding New Play. 

==Film==
Fitzgerald adapted his own stage play for the screen. The film version was produced by Doug Pettigrew and Fitzgerald, and executive produced by Sidney Kimmel, Vicki McCarty, William Jarblum, Trudy Pettigrew, Dana Warren and Shandi Mitchell.   Fitzgerald had originally planned for the role of Dotty to be played by Joan Orenstein, but as she died while he was writing it, he cast Fricker instead.  In press for the film, Fricker praised the screenplay, "“I was so moved by it. The love story was so beautiful I couldn’t say no to it.”  Three members of the original stage cast reprised their roles: Ryan Doucette, Marlane OBrien, and Michael McPhee.

==Reception==
The film debuted to an enthusiastic standing ovation on September 16, 2011 at the Atlantic Film Festival, where it won an Atlantic Canada Award for Best Screenplay and the Peoples Choice Audience Award for Best Film of the Festival. Its second festival appearance was October 20, 2011 at Cinéfest Sudbury International Film Festival, where it also won the Audience Choice Award for Best Film, and on October 23, 2011 the film was the opening night selection of the Edmonton International Film Festival where it won the Audience Award for Best Canadian Film. Cloudburst was very well received at film festivals from coast to coast in Canada, winning awards at festivals in Halifax, Montreal, Kingston, Edmonton, Victoria, among others.
 Frameline 36.

==Awards and honors==
* Asheville QFest Best Actress Award, Olympia Dukakis
* Asheville QFest Best Supporting Actress Award, Brenda Fricker
* Athens, Greece Outview Film Festival Best Film Award
* Atlanta Film Festival Pink Peach Feature Grand Jury Prize
* Atlanta Out on Film Festival Audience Award for Best Overall Feature    
* Atlanta Out on Film Jury Award for Best Film 
* Atlanta Out on Film Jury Award for Best Actress, Olympia Dukakis 
* Atlantic Film Festival Peoples Choice Audience Award for Best Film of the Festival 
* Atlantic Film Festival Michael Weir Atlantic Canada Award for Best Screenplay, Thom Fitzgerald 
* Barcelona International Gay & Lesbian Film Festival Audience Award for Best Film  
* Birmingham UK Shout Festival Audience Award for Best Picture
* British Film Institute London Lesbian and Gay Film Festival Opening Night Gala 
*Canadian Film Festival Opening Night Gala
* Cinefest Sudbury International Film Festival Audience Choice Award for Best Film of the Festival 
* CNKY Cincinnati Kentucky GLBT Festival Award for Best Feature Film
* Copenhagen MIX Copenhagen Film Festival Audience Award for Best Film
* Edmonton International Film Festival Opening Night Gala 
* Dublin International Film Festival Opening Night Gala 
* Edmonton International Film Festival Audience Award for Best Canadian Indie Film 
* Hannover Queer Film Festival Audience Award for Best Film
* Image+Nation Montreal GLBT Film Festival Best Feature Film Award 
* Indianapolis LGBT Film Festival Audience Award for Best Lesbian Film  
* Kingston Reelout Film Festival Opening Night Gala
* Kingston Reelout Film Festival Audience Award for Best Narrative Feature
* Melbourne Queer Film Festival Opening Night Gala 
*Mix Milan Film Festival, Grand Jury Award for Best Feature
* New Zealand  , Audience Award for Best Feature  
* North Carolina Gay and Lesbian Film Festival Audience Award for Best Feature
* Palm Springs International Film Festival Best of the Fest Selection
* Qfest Philadelphia Audience Award for Best Feature
* QFest St. Louis Audience Award for Best Feature
* Rainbow Reels Waterloo Film Festival Peoples Choice Award for Best Feature Film
* Sacramento Gay & Lesbian Film Festival Audience Award for Best Film
* San Diego FilmOut, Audience Award for Best Feature Film    
* San Diego FilmOut, Jury Award for Best Feature Film  
* San Diego FilmOut, Jury Award for Best Leading Actress  
* San Diego FilmOut, Jury Award for Best Direction  
* San Francisco Frameline Festival Audience Award for Best Film
* Southwest Gay and Lesbian Film Festival Audience Award for Best Feature
* Vancouver International Film Festival Top Ten Most Popular Canadian Film Award 
* Victoria Film Festival Best Canadian Film Award 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 