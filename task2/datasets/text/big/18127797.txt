Star (1982 film)
{{Infobox film
| name           = Star
| image          = Star-1982-1.JPG
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Vinod Pande
| producer       = Biddu
| writer         = Biddu
| screenplay     = Biddu, Tariq Yunus
| story          = 
| based on       =  
| narrator       =  Raj Kiran
| music          = Biddu
| cinematography = Nadeem Khan 
| editing        = 
| studio         = Mehboob Studios
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}} Raj Kiran, Saeed Jaffrey, A.K. Hangal, Dina Pathak and Padmini Kolhapure.

It had a very successful soundtrack released as Star/Boom Boom, produced by Biddu and sung by Nazia Hassan and Zoheb Hassan.

Nazia & Zoheb were offered the chance to act in this movie by Biddu, but they refused to act and chose singing.

==Plot==
Dev Kumar Verma comes from a middle-class family and must find employment to support his dad and mom. Dev, however, has set his mind upon becoming a music sensation like Elvis Presley.

He loses his job because of this, and refuses to work until and unless he gets a job to his liking, much to the dismay of his parents and his brother, Shiv Kumar. Dev does get employment at Charlies Disco, where he meets with Maya and falls in love with her.

When Charlies Discos competitor, Rana, finds out about Dev, he wants to hire Dev, but Dev decides to continue to work with Charlies Disco, as a result Dev and Charlie get a beating by Ranas men, and Dev is unable to sing.

After recuperating, Dev is devastated to find out that Maya and Shiv Kumar are in love with each other. What impact will this have on Dev and his brother on one hand, and what of his career in music?

==Cast==
* Kumar Gaurav...Dev Kumar Verma 
* Rati Agnihotri...Maya 
* Padmini Kolhapure...Devs fan  Raj Kiran...Shiv Kumar Verma 
* Saeed Jaffrey...Rana
* A.K. Hangal...Mr. Verma
* Dina Pathak...Mrs. Verma
* Raja Bundela...Salim (Devs friend)
* Ravindra Kapoor...Charlie
* Rajendra Kumar...Himself (Guest Appearance)
* Bob Christo...Samson
* Yunus Parvez...Ranas employer
* Biddu...Himself (Guest Appearance)

==Soundtrack==

All songs sang by Pakistani singers Nazia Hassan and her brother Zoheb Hassan.

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tere Zindagi Me Yu Toh Kayi Dard Aayege"
| Zoheb Hassan
|-
| 2
| "Boom Boom"
| Nazia Hassan
|-
| 3
| "Ooe Ooe Chori Chori Bate Ho"
| Zoheb Hassan
|-
| 4
| "Aye dil mere chal re dheere dheere dheere"
| Zoheb Hassan
|-
| 5
| "Jaana zindagi se na jaana"
| Nazia Hassan, Zoheb Hassan
|-
| 6
| "Yeh dil tere liye hai yeh jaan tere liye"
| Nazia Hassan
|-
| 7
| "Yado Me Khoya Na Zaga Na Soya, Unse Biched Ke Roya"
| Zoheb Hassan
|-
| 8
| "Star"
| Zoheb Hassan
|-
| 9
| "Khushi"
| Nazia Hassan
|}

==Awards and nominations==
*Filmfare Nomination as Best Female Playback Singer Award - Nazia hassan for the song "Boom boom"

==Facebook, IMDB and other external links==
*  
*  
* http://indiamp3.com/music/index.php?action=album&id=4274

 
 


 