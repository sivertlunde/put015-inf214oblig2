Febbre da cavallo
 
{{Infobox film
| name           = Febbre da cavallo
| image          = Febbre da cavallo.jpg
| caption        = Film poster Steno
| producer       = Roberto Infascelli Steno Enrico Vanzina
| starring       = Gigi Proietti Enrico Montesano Francesco De Rosa  Mario Carotenuto Catherine Spaak Adolfo Celi
| music          = Franco Bixio Fabio Frizzi
| cinematography = Emilio Loffredo
| editing        = Raimondo Crociani
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}
 Steno and starring Gigi Proietti.    It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

Initially conceived as a safe product destined to be forgotten quickly after its passage in movie theaters, numerous television appearances have raised over time to cult-classic status for fans of light Italian comedy.

==Plot==
Bruno Fioretti, known as "Mandrake", is an inveterate gambler who never misses a day at the horse racing track in Rome. He is doubly unlucky: he bets too much on one horse, and his wife is sleeping with his best friend because Mandrake is always at the track. Penniless and cuckolded, Mandrake decides to make one last bet.

==Cast==
* Gigi Proietti as Bruno Fioretti, Mandrake
* Enrico Montesano as Armando Pellicci, Er Pomata
* Catherine Spaak as Gabriella, Mandrakes girlfriend
* Mario Carotenuto as Avv. De Marchis
* Francesco De Rosa as Felice
* Gigi Ballista as Conte Dallara
* Maria Teresa Albani as Fortune-teller
* Nikki Gentile (as Nicky Gentile)
* Adolfo Celi as Judge Ennio Antonelli as Otello Rinaldi, the butcher, Manzotin
* Luciano Bonanni as Fatebenefratelli Hospitals male nurse
* Aristide Caporale (as Aristide Caporali)
* Gianfranco Cardinali
* Giuseppe Castellano as Stelvio Mazza
* Fernando Cerulli

== Production ==
In the opening credits, Gigi Proietti is credited as Luigi Proietti, his full name. The actor has also stated that following Ghost Whisperer to film, feared to really take the game habit. 

In the scene of whisky Vat69 spot the actor who plays the Director (Fernando Cerulli) is voiced by Steno, Director of horse fever, while the driver without a license is voiced by Mario Lombardini. 

===Location===
Much of the film was shot between Piazza Venezia and Piazza Margana. In the first Gabriella manages the Gran Caffè Roma, still existing local but profoundly restructured (there is no longer even the sign); in the second, in a quiet corner, is (still perfectly equal) the gate of the House of ointment, where he is stationed the Ventresca. The Hospital where the Ointment recipe medicines stolen from Luciano Bonanni is the Fatebenefratelli on Tiber island. The pharmacy Magalini still exists, and is not much changed from the time of the film. The train scenes were filmed on the line Rome-Formia-Naples, while those of station set in Naples are actually turn to the Termini station in Rome. The scenes in the various racetracks in the movies are all filmed at Tor di Valle Hippodrome. Finally, the scene where Mandrakes car stops because no gasoline is turned on via Ostiense, among Tor di Valle and Vitinia.

==Sequel==
 
The movie was filmed a sequel in 2002, Febbre da cavallo - La mandrakata directed by Carlo Vanzina (son of Steno, whose real name was Stefano Vanzina), again starring Gigi Proietti.

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 
 