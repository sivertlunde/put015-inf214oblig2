Estudantes
{{Infobox film
| name =  Estudantes
| image = Carmen Miranda em Estudantes - 1935.jpg
| caption = Mesquitinha (left) with Carmen Miranda and Barbosa Júnior in Estudantes.
| director = Wallace Downey	
| producer = Adhemar Gonzaga Wallace Downey	
| writer = Alberto Ribeiro João de Barro	
| starring =
| music = 
| cinematography = Edgar Brasil Antonio Medeiros		
| editing = 	  
| studio = 
| distributor = Waldow Filmes and Cinédia
| released = 8 July 1935
| runtime = 
| country = Brazil Portuguese
| budget =
| gross =
| website =
}}

Estudantes was a Brazilian musical comedy film directed by Wallace Downey and starring Carmen Miranda, Barbosa Júnior, Mesquitinha and Mário Reis. {{cite news|url=http://books.google.com.br/books?id=Qf-SAAAAQBAJ&pg=PT61&dq=Estudantes+1935&hl=pt-BR&sa=X&ei=Uw0HVOaqDYTmsATV3YHYCw&ved=0CDQQ6AEwBA#v=onepage&q=Estudantes%201935&f=false|title=Os reis da voz
|date=|work= Ronaldo Conde Aguiar|page=|accessdate=September 3, 2014}} 

Initially titled Folia de Estudantes, was made entirely in a single week in the studios and labs Cinédia. Premiered at Cine Alhambra, in Rio de Janeiro, on July 8, 1935, staying in theaters for two weeks. It has also been shown in São Paulo, at Cine Odeon (Sala Vermelha), debuting on July 15 of that year. From what we know, there are not preserved copies of this film.

== Production ==
Estudantes (Students) of 1935, is a co-production from Waldow Filmes and Cinédia, directed by Wallace Downey and based on a script by musicians João de Barro and Alberto Ribeiro, was set in a university student residence.  In this film Carmen Miranda played by the crooner Mário Reis, and for the first time she acted out a role as well as singing. Two other students, played by Mesquetinha and Bárbosa Júnior, vie for her affections, giving rise to a wealth of comic incidents. Shot in just one week, "Estudantes" premiered at Rios Cinema Alhambra on 8 July 1935, where it was shown for a further two weeks. Although the films soundtrack featured well-known carnival "marchas" and sambas, it was not designed to tie in with the annual celebrations, but rather was an example of so-called mid-year productions (de meio de ano), the premieres of which were often timed to coincide with the festas juninas or popular Catholic festivals held in June.

Inspired by the success of the film vehicles for carnival music, other mid-year musicals were simply intended to promote the radio and record industries. "Estudantes" featured musical numbers associated with the popular religious celebrations, and song Cadê Mimi? (Wheres Mimi?), performed by Mário Reis as he serenades Carmen Mirandas character. This song subsequently became a huge hit throughout Brazil. In spite of its success at the box-office. {{cite news|url=http://books.google.com.br/books?id=mQsHp5Re3IoC&pg=PA37&dq=Estudantes+1935&hl=pt-BR&sa=X&ei=Uw0HVOaqDYTmsATV3YHYCw&ved=0CEUQ6AEwBw#v=onepage&q=Estudantes%201935&f=false|title=Popular Cinema in Brazil: 1930-2001
|date=|work=Stephanie Dennison|page=|accessdate=September 3, 2014}} 

== Cast ==
 
 
* Carmen Miranda ... Mimi
* Barbosa Júnior ... Flores
* Mesquitinha ... Ramalhete
* Mário Reis ... Mário
* Almirante ... Himself
* Ivo Astolphi ... Himself (as Bando da Lua)
* Simão Boutman ... Himself
* Hervê Cordovil ... Himself
* Aloysio De Oliveira ... Himself (as Bando da Lua)
* Oswaldo de Moraes Eboli ... Himself (as Bando da Lua)
* Jaime Ferreira		
* Silva Filho		
* Adélia Fontes		
* Hélio Jordão ... Himself (as Bando da Lua)
* Benedito Lacerda ... Himself
* César Ladeira	... Locutor
* Nina Marina		
* Silvinha Melo	... Herself
* Aurora Miranda ... Herself
* Jorge Murad		
* Afonso Osório	... Himself (as Bando da Lua)
* Armando Osório ... Himself (as Bando da Lua)
* Stênio Osório	... Himself (as Bando da Lua)
* Carmem Silva ... Herself
* Sílvio Silva	... Teacher
* Haroldo Tapajós ... Himself (as Irmãos Tapajós)
* Paulo Tapajós	... Himself (as Irmãos Tapajós)
* Jeanette Weyting		
* Dulce Weytingh

== Musical numbers ==
* Linda Mimi ... Performed by Mário Reis
* Sonho de Papel ... Performed by Carmen Miranda
* E Bateu-se a Chapa ... Performed by Carmen Miranda
* Onde Está o Seu Carneirinho? ... Performed by Aurora Miranda
* Linda Ninon ... Performed by Aurora Miranda
* Ele ou Eu ... Performed by Silvinha Melo & Irmãos Tapajós
* Lulu ... Performed by Bando da Lua
* Assim Como o Rio ... Performed by Almirante 

== Critical reception ==
Estudantes received mixed reviews. An anonymous P. de L., writing for the Rio newspaper O Globo on 25 June 1935, called it a retrograde step in relation to "Hello, Hello Brazil!|Alô, alô, Brasil!", whereas Alfredo Sade, writing in the Rio-based publication A Batalha in July 1935, argued that Estudantes was a far superior film. He praised the quality of photography and the sound in what he termed this very Brazilian musical comedy. Another anonymous review by a certain A. F. in the newspaper Diário Português of July 1935 argued that Estudantess greatest value was its spontaneity and the promise that it and its young creators represented for the future of the cinema industry in Brazil.

This potential was to be more fully realised in Cinédias next production, Hello, Hello, Carnival!|Alô, Alô, Carnaval!.

==References==
 

==External links==
* 

 
 
 
 
 
 
 