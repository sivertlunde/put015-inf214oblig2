The Prisoner (1923 film)
 
{{Infobox film
| name           = The Prisoner
| image          = 
| caption        =  Jack Conway
| producer       = 
| writer         = Edward T. Lowe Jr. George Barr McCutcheon
| starring       = Herbert Rawlinson Eileen Percy
| music          = 
| cinematography = Ben F. Reynolds
| editing        =  Universal Pictures
| released       =  
| runtime        = 5 reels
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Jack Conway and featuring Boris Karloff.    The film is considered to be lost film|lost.   

==Cast==
* Herbert Rawlinson - Philip Quentin
* Eileen Percy - Dorothy Garrison
* George Cowl - Lord Bob
* June Elvidge - Lady Francis
* Lincoln Stedman - Dickey Savage
* Gertrude Short - Lady Jane
* Bertram Grassby - Prince Ugo Ravorelli
* Mario Carillo - Count Sallonica
* Hayford Hobbs - Duke Laselli
* Lillian Langdon - Mrs. Garrison
* Bert Sprotte - Courant
* Boris Karloff - Prince Kapolski
* Esther Ralston - Marie
* J. P. Lockney - Father Bivot

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 