One Missed Call (2003 film)
{{Infobox film
| name           = One Missed Call
| image          = One-missed-call-poster.jpg
| image_size     = 225px
| alt            =
| caption        = Japanese theatrical release poster
| director       = Takashi Miike
| producer       = Yoichi Arishige Fumio Inoue Kazuo Kuroi Hiroshi Okawa Naoki Sato
| screenplay     = Minako Daira
| based on       =  
| starring       = Kou Shibasaki Shinichi Tsutsumi Kazue Fukiishi Anna Nagata Renji Ishibashi Atsushi Ida Mariko Tsutsui
| music          = Kôji Endô
| cinematography = Hideo Yamamoto
| editing        = Yasushi Shimamura
| studio         = Kadokawa Pictures Toho Company
| released       =  
| runtime        = 112 minutes
| country        = Japan
| language       = Japanese
| budget         = $1.7 million   
| gross          = $16,234,612   
}}
  is a 2003 Japanese horror film directed by Takashi Miike and written by Minako Daira. The film is based on the novel Chakushin Ari by Yasushi Akimoto.  The plot revolves around Yumi Nakamura, a young psychology student whose friend Yoko gets an unusual voice message on her cell phone. The message is dated two days in the future and Yoko can hear herself screaming in it. After Yoko mysteriously dies, her death sets off a chain of events which leads Yumi to discover that this phenomenon has been occurring throughout Japan long before Yoko received an anonymous call from her future self. When Yumi receives a call with the date and time of her future death, she struggles to save herself and learn the identity of the mastermind behind the calls.
 US as One Missed Call.

==Plot==
College student Yoko Okazaki receives a phone call accompanied by an eerie, unusual ringtone, which goes to voicemail. The call is from Yokos own number, dated two days to the future. Yoko and her friend Yumi Nakamura listen to the voicemail, hearing Yokos voice chatting casually, followed by a horrendous scream and then dead silence. Two days later, Yoko calls Yumi that night to discuss shopping plans. Yumi realizes that Yoko is on the exact routine as the voicemail theyd heard before, but can only hear Yoko screaming after she is violently dragged off onto a speeding commuter train, which kills her. Her head then vomits a red candy upon death as her detached hand, still clutching her phone, calls a number. Several days later, Yokos boyfriend, Kenji Kawai, reveals to Yumi that he had also received a voicemail accompanied by the same ringtone as Yokos right after her death. Yumi then watches as Kenji is pulled into an empty elevator shaft to his death. He also spits out a red candy and calls a number, like Yoko.

A colleague of Yumis, Natsumi Konishi, is staying at Yumis apartment when she receives the cursed voicemail, this time accompanied by a video showing Natsumi being haunted by a ghastly figure. Her attempt to discard the phone is futile as she keeps receiving the mails on other phones, and is taken for an exorcism. Desperate, Yumi meets with Hiroshi Yamashita, a detective who had investigated the curse. Yamashita reveals that his sister, Ritsuko, was a social care worker who had received the voicemail and eventually died from a house fire. Natsumis exorcism is a disaster and she is killed when her body horribly contorts. Yumi receives the cursed voicemail shortly after.
 abusing them for the sake of attention. Mimiko succumbed to her asthma attack a year before, while Marie was last seen in a hospital, now destroyed after a fire. Only Nanako is identifiable; she is mute and carries a doll ringing with the tone that is the ringtone of the cursed voicemail. Yumi visits the abandoned hospital, but is haunted by ghosts until she meets Yamashita. During a lockdown, the two find Maries decomposed body clutching a cellphone. The body rises and blasts Yamashita out of the room. It follows Yumi, who reminisces of her abusive mother and hugs Maries body, which becomes inanimate again. 

Yamashita is called to Nanakos orphanage to watch a nanny cam Marie had used to monitor her children. The cam shows that Marie never abused her children; instead, it was Mimiko, who cut Nanakos hand that resulted in Marie taking Nanako to the hospital while Mimiko succumbed to her asthma. Realizing that Mimiko is behind the curse, Yamashita races to Yumis apartment as Yumi is haunted by Mimikos ghost. Yumi stabs Yamashita, revealing she has been possessed by Mimiko. Yamashita dreams that he helps a dying Mimiko to breathe with an inhaler. Upon waking, he is in a hospital with Yumi carrying a knife. Spitting a candy for Yamashita to eat, she waits as he chews it and laughs.

==Cast==
 
{| class="wikitable"
|-
!Character
!  Actor (Original) Dub
|- Yumi Nakamura Kou Shibasaki Kate Higgins
|- Hiroshi Yamashita Shinichi Tsutsumi Liam OBrien
|- Natsumi Konishi Kazue Fukiishi Stephanie Sheh
|- Nanako Mizunuma Shimizu Seinami Karen Strassman
|- Masakazu Hirayama
|???? Doug Stone Doug Stone
|- Yoko Okazaki Anna Nagata Karen Strassman
|- Detective
|Renji Ishibashi Michael McConnohie
|- Kenji Kawai Atsushi Ida Sam Riegel
|- Rina Tsuchiya Kana Ito
|????
|- Marie Mizunuma Mariko Tsutsui Sam Carr
|- Ritsuko Yamashita Takehana Azusa Cristina Valenzuela
|- Mimiko Mizunuma Karen Oshima Karen Strassman
|- Ichiro Fujieda Yutaka Matsuchige Kim Strauss
|- Oka
|Goro Kishitani Joe Cappelletti
|- Additional voices
| - Stephanie Sheh   Steve Staley  (Phone Salesman)    Karen Thompson  (Voicemail) 
|-
|}

===English dubbing staff===
*Dubbing director: Unknown
*Dubbing studio: Studiopolis
*Media: DVD/Blu-ray Disc

==Critical reception==
One Missed Call has received mixed reviews by critics, who generally cite it as being too similar to previous  .   

Entertainment Weekly wrote, "One Missed Call is so unoriginal that the movie could almost be a parody of J-horror tropes", yet "Miike, for a while at least, stages it with a dread-soaked visual flair that allows you to enjoy being manipulated."  LA Weekly called it "a prolonged, maddening, predictable—yet curiously pleasurable—descent into incomprehensibility."  The Philadelphia Inquirer stated that "Miike, whose work usually veers into more surreal, experimental terrain, uses creepy-crawly juxtaposition, grisly violence, and dark humor to create a nightmare scenario for the text-message generation." 

==Sequels==
A sequel,  , the third installment and the end to the Chakushin ari mythos was released in 2006.

==Remake== One Missed Call, starring Shannyn Sossamon.

==References==
 

==External links==
*  
*     at the Japanese Movie Database

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 