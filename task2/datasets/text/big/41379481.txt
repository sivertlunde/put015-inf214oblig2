Crave (film)
{{Infobox film
| name           = Crave
| image          = CraveFilmPosterCDL.jpg
| alt            = 
| caption        = 
| director       = Charles de Lauzirika
| producer       = 
| writer         = Robert A. Lawton, Charles de Lauzirika
| starring       = Josh Lawson, Emma Lung, Ron Perlman
| music          = Justin Caine Burnett
| cinematography = William Eubank
| editing        = David Crowther
| studio         = Iron Helmet, Another Green World Productions
| distributor    = Phase 4 Films
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Crave (also known under the working titles of Shatterbrain and Two Wolves) is a 2012 American drama thriller film directed by Charles de Lauzirika. The film stars Josh Lawson as a man who retreats into a fantasy world that comes with deadly consequences. Crave had its world premiere on July 24, 2012 at the Fantasia International Film Festival and had a wider theatrical and video on demand release on December 6, 2013.  

==Synopsis== freelance crime scene photographer that copes with the world around him by imagining himself as a hero that saves the day. His world is turned upside down when he meets Virginia (Emma Lung), who breathes new life into his daily routine and gives him a new sense of confidence. This gives him the courage to make his daydreams into a reality, which comes with some negative repercussions as Aidens sense of justice might be considered warped.

==Cast==
*Josh Lawson as Aiden
*Emma Lung as Virginia
*Ron Perlman as Pete
*Edward Furlong as Ravi
*Christopher Stapleton as Barry
*William Gines as Chuchi
*Michael Renda as Peanut (as Mike Renda)
*Helena Kash as Redhead
*Jim Hanna as AA Boyfriend
*Tonya Cornelisse as AA Girlfriend
*John W. Hardy as AA Speaker
*Richard Speight Jr. as Master Rupert
*Jordan Trovillion as Young Emily
*Lloyd James as Slave Jim
*Taras Los as Yuri

==Production== Phillip K Dicks I Hope I Shall Arrive Soon.    Dicks daughter, Isa Dick Hackett, encouraged Lauzirika to make a smaller film before beginning the larger project and Lauzirikas former neighbor Robert Lawton pitched him the premise of Crave.  The idea was that the film would be "Travis Bickle meets Walter Mitty" and Lauzirika and Lawton worked on developing the films script and main character for two months, during which point each man wrote two drafts of the script.  Lauzirika based part of Aidens relationship with Virginia upon his own experiences, which he felt made the movie "a little more interesting and a little more human".  He also chose to leave the films ending deliberately ambiguous, as he did have a set interpretation of the ending but liked the idea of multiple interpretations as he feels that "it’s much more interesting to leave the theater with a question so people leave talking about it rather than having the answer handed to you on a silver platter".  Josh Lawson was brought in to portray Aiden, which Lauzirika felt made the character "more human and likable", which he believed made the characters interactions with Lungs character more explainable.    Furlong was not initially considered for the role of Ravi, but was hired upon the recommendation of the movies costume designer Oakley Stevenson. 

Crave was initially written and planned to be shot in New York, but moved the films setting and shooting location to Detroit due to Michigans tax incentives for filmmakers.    Lauzirika had some initial hesitations over this shift, but later felt that the move was appropriate as Detroit was "a good match for Aiden" and was very photogenic.  Filming took place during late 2009, but experienced some setbacks during filming, as a scene that was to be set on the Detroit People Mover was canceled at the last minute due to concerns over the films violent and sexual content. 

==Reception== Complex claimed that "Crave signals the arrival of an attention-demanding filmmaker whos schooled in the down and dirty noir tropes of old but possesses more than enough ingenuity to avoid copycat status." 
 Falling Downs Foster.  Collider opined that the movie "does have a relatable and worthwhile issue worth exploring" but that "Lauzirika is always going big yet going nowhere".  Twitch Film panned the movie overall, criticizing Furlong as the films weakest link while also stating that his character gave the reviewer his favorite moment of the film. 

===Awards===
*New Flesh Award for Best First Feature, Fantasia International Film Festival (2012, won) 
*Next Wave Award for Best Director, Fantastic Fest (2012, won)  
*Best Editing Award, Toronto After Dark Film Festival (2012, won)
*Best Title Sequence (closing credits), Toronto After Dark Film Festival (2012, won) 

==References==
 

==External links==
*  
*  

 
 
 
 