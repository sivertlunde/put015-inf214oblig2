Poojai
{{Infobox film
| name           = Poojai
| image          = Poojai poster.jpg
| alt            =  
| caption        = Theatrical release poster Hari
| Vishal
| writer         = Hari Shashank Vennelakanti (Telugu dialogues) Vishal Shruti Haasan Sathyaraj Raadhika Sarathkumar Mukesh Tiwari
| music          = Yuvan Shankar Raja Priyan
| editing        = V. T. Vijayan T. S. Jai
| studio         = Vishal Film Factory
| distributor    = Vendhar Movies 
| released       =  
| runtime        = 153 minutes
| country        = India
| language       = Tamil
| budget         =  
| gross  =  
}}         Indian Tamil masala film Hari and Priyan and V. T. Vijayan, respectively.
 Deepavali along with its Telugu dubbed version titled Pooja.  

==Plot== aka Vasu (Vishal Reddy|Vishal) is a Coimbatore-based moneylender and also was the heir to the Kovai Group, a cloth manufacturing conglomerate, until he was disowned by his mother and matriarch of his joint family Rajalakshmi (Raadhika Sarathkumar) due to some misunderstanding. He meets a rich girl Divya (Shruthi Haasan) at a mall, who soon falls in love with him for his good nature. One day, Vasu saves the Police Superintendent for Coimbatore District Sivakkozhundhu (Sathyaraj) and his wife (Aishwarya (actress)|Aishwarya) from a group of thugs who work for Anna Thandavam (Mukesh Tiwari), a Pollachi-based businessman and contract killer who kills anyone to acquire their land and money. As Sivakkozhundhu was transferred to Coimbatore district to capture Anna Thandavam, the latter had planned to kill him. Anna Thandavam also plans to illegally grab land sold to a village temple by Vasus late father. These two incidents cause enmity between Vasu and Anna Thandavam.

Vasu soon reconciles with the rest of his family and returns home. When the land is formally sold to the temple, an irate Anna Thandavam assaults Vasus uncle Ramaswamy (Jayaprakash), following which Vasu beats him up in retaliation and breaks his hand. A bitter battle erupts between Vasu and Anna Thandavam, with Vasu, assisted by Sivakkozhundhu, subduing Anna Thandavams henchmen and thwarting all attempts by Anna Thandavam to destroy his family. In the process however, Anna Thandavams wife kills Rajalakshmi during the temple festival. Vasu, seeking revenge for his mothers death, rushes to Patna, where Anna Thandavam is hiding and kills him in a thrilling climax.

==Cast==
  Vishal as Vasudevan Ratnasamy 
* Shruti Haasan as Divya a.k.a D
* Sathyaraj as A. D. S. P. Sivakkozhundhu
* Mukesh Tiwari as Annam Thandavam Soori as Kutti Puli
* Raadhika Sarathkumar as Rajalakshmi Rathnasamy
* Jayaprakash as Ramaswamy
* Thalaivasal Vijay as Kumaraswamy
* Prathap Pothen as Divyas Father
* Barath Raj as Naresh Yadhav
* Awadesh Mishra as Rai Bahadhur Abhinaya as Durga Sithara as Ramaswamys Wife Kausalya as Susheela Kumaraswamy Renuka as Manimekalai  Aishwarya as Sivakkozhundus Wife
* Janaki Sabesh as Divyas Mother
* Madhumila as Selvi
* Sabarna as Chithra
* Nisha as Divyas friend
* Santhana Bharathi as Annathandavams lawyer
* Pondy Ravi as Annathandavams henchman
* Karate Raja as Annathandavams henchman
* O. A. K. Sundar as Durgas Father Pandi as Paandi
* Manobala as Kovai Groups Secretary
* R. Sundarrajan (director)|R. Sundarrajan
* Imman Annachi as Guru
* Vinay Bihari as Kerala Minister
* Rangammal
* Sanjay Singh
* Sounthara Raja
* Andrea Jeremiah in an item number
 

==Production==

===Development=== Hari and Priyan and V. T. Vijayan were confirmed to be the cinematographer and the editor of the film respectively.  Vasuki Bhaskar was selected to do the costume designing and make-up. 

===Casting=== Soori were Kausalya was Vaibhav Reddy Abhinaya was selected to play a supporting role. 

===Filming=== Koyambedu Market in Chennai.  Later, the unit proceeded to Coimbatore for filming major portions of the film.   At that time, it was known that Hari planned to wrap the films shoot in 40 working days.  A fight sequence was shot in a popular mall multiplex there and scenes involving Vishal and Shruti Hassan were also shot in the mall in Coimbatore.  The filming continued at Karaikudi in early July where scenes featuring the principal cast were shot.   The shooting continued in hot weather making things difficult while Vishal suffered injuries for the second time in the films shoot. He suffered a fracture during the shoot of an action sequence after trying to avoid falling on his face while he slipped from an asbestos sheet.   
 AVM facilities and choreographed by Baba Baskar. One more song sequence was also shot in Poland.  On 29 September 2014, Shruti confirmed that the shooting of the film was completed. 

==Music==
{{Infobox album 
| Name = Poojai (Original Motion Picture Soundtrack)
| Longtype =
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Cover  = Poojai audio.jpg
| Border = yes
| Alt =
| Caption = Soundtrack Album Art
| Released = 1 October 2014
| Recorded = 2014 Feature film soundtrack Tamil
| Label = V Music
| Length = 
| Producer = Yuvan Shankar Raja Govindudu Andarivadele (2014)
| This album = Poojai (2014)
| Next album = Vai Raja Vai (2014)
}}

The soundtrack was composed by Yuvan Shankar Raja. The album consists of six tracks and Na. Muthukumar had written the lyrics for all songs. The audio rights were purchased by Vishals newly launched music company, V Music.  The tracklist was released on 29 September 2014,  while the audio launch took place on 1 October 2014 at the Loyola-ICAM College of Engineering and Technology. 

The album received mixed reviews from critics.  Behindwoods.com|Behindwoods gave 2.5 out of 5 stars and concluded that "Yuvan delivers what is needed for a commercial entertainer".  Indiaglitz gave 2.75 out of 5 and wrote that the album was "a commercial cocktail which could have been better", and chose "Devathayai", "Soda Bottle" and "Veraarum" as the albums picks. 

{{track listing
| headline = 
| extra_column = Singer(s)
| all_lyrics = Na. Muthukumar
| total_length = 23:56
| music_credits = no

| title1 = Devathayai
| extra1 = Nivas
| length1 = 4:04
| title2 = Ippadiye
| extra2 = Rahul Nambiar, Mili Nair
| length2 = 4:34
| title3 = Odi Odi
| extra3 = Palakkad Sriram
| length3 = 4:32
| title5 = Uyire Uyire
| extra5 = Yuvan Shankar Raja
| length5 = 2:26
| title4 = Soda Bottle
| extra4 = Yazin Nizar, Andony Dasan, Sathyan (singer)|Sathyan, 
| length4 = 4:34
| title6 = Verarum Kandhirathe
| extra6 = Karthik (singer)|Karthik, Pooja A. V.
| length6 = 3:46
}}

== Release ==
During the films launch, Hari stated that the film would release in October 2014 as a Deepavali release.  The same was stated again in the end of July 2014.   On 11 August 2014, a release stated that the Telugu dubbed version titled Pooja would release simultaneously with the original. 

The Tamil Nadu theatrical distribution rights were acquired by Vendhar Movies.  

Stills featuring Vishal and Shruti Hassan were released on 18 April 2014.   The first look poster of the Telugu dubbed version titled Pooja was released on 11 August 2014. {{cite web|url=http://www.indiaglitz.com/Vishals-pooja-first-look-poster-telugu-news-111817|title=Vishals Pooja First Look Poster Sun TV.   The film is set to release in over 1100 screens worldwide. 

===Critical reception===
IB Times called it a typical Hari commercial entertainer.  The Times of India gave the film 3 stars out of 5 and wrote, "Poojai is commercial masala done well. There is action (punchy), sentiment (effective but not affecting), humour (silly but funny once in a while), romance (ludicrous and strictly functional), and the director manages to keep things ticking —the action is often relentless, and the nondescript songs and the comedy scenes (Soori and Pandi doing a pale imitation of Goundamani and Senthil) are breathers for us".  The Hindu wrote, "Poojai, despite not being Hari’s best, could still be successful in the B and C centres, and affirm his bankability. The writing is generally not well thought out though. It seems almost like groups of imperfect scenes were put together in the hope that they’d somehow come together to form a riveting, entertaining whole. Poojai then is a refutation of Aristotle’s adage that states that the whole is greater than the sum of its parts".  Sify called it "a typical Hari mass entertainer, though this time he has not got the mix in the right proposition".  Bangalore Mirror gave 3 stars out of 5 and wrote, "All the formulaic features that usually form part of a Hari film are all intact here as well, the racy screenplay included. But then, the problem is that the theme is too old-fashioned, with a customary villain trying to dominate a particular community and the hero rising in revolt the moment his family gets affected. Neither there is freshness in treatment nor there is novelty in the narration". 

Indo-Asian News Service gave 2 stars and wrote, "Gopalakrishnan (Hari) has written Poojai with the mindset that anything that comes from his camp will be lapped up by audiences, no matter how good or bad it is. If youve followed his films over the years, its easier to predict Poojai from start to finish with eyes closed. Because of which you realise the director never tries to innovate with his films and usually sticks to a template hes done to death".  Behindwoods stated "the director takes us through a roller coaster in the first half. But it feels like Hari loses the grip post interval a bit and the film plods in the second half, with misplaced song sequences and repetitive action blocks...Poojai is still a watchable entertainer to spend time on, with your family on a festive day", giving it a rating of 2.25 out of 5.  Indiaglitz.com wrote, "Poojai is a mix of a lot of genres, but they seem disproportionate for the times and taste of this day. However, it is an entertaining three hours of festivity, to chill out on. Most importantly, thanks to Hari for a thoroughbred masala movie after a long time in Kollywood".  Cinemalead rated 2.25 out of 5 and wrote,"Customary commercial potboiler!"  

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 