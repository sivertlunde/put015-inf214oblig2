Fire Creek
 
{{Infobox film
| name           = Fire Creek
| image          = Fire Creek Poster.jpg
| caption        = Theatrical poster
| director       = Jed Wells
| producer       = Dennis Packard Charles Cranney Seth Packard Raymond Robinson
| writer         = Nathan Keonaona Chai
| starring       = Seth Packard John Cannon Kim Abunuwara Dayne Rockwood Melinda Lockwood Adam Daveline
| music          = Jay Packard
| cinematography = Jed Wells
| editing        = Russ Lasson
| studio         = Campus Studios Brigham Young UniversityiTunes
| distributor    = Campus Studios Third North Distribution i
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $100,000
| gross          = 
}} Cinemark theaters in Utah, May 8, 2009.

==Plot==
Fire Creek tells the story of a young marines search for an answer to the age-old question "why do bad things happen to good people?" Jason is serving as a marine in Afghanistan when divine intervention saves him from a sniper attack. However, his closest friend, a devoted husband and father, is shot and killed just inches away. Critically wounded but alive, Jason returns home wondering why his life was spared. The rest of the film follows his search for answers. 

==Production== BYU students, as well as Larry Green on guitar and Lloyd Miller on Middle Eastern instruments.

==Release==
Fire Creek was released digitally for select theaters in Utah May 8, 2009. It is now available on iTunes.

==References==
 

==External links==
*  
*  

 
 
 
 
 