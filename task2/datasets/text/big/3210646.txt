Breakfast of Champions (film)
{{Infobox film
| name           = Breakfast of Champions
| image          = Breakfast of Champions (film).jpg
| caption        = Theatrical release poster
| director       = Alan Rudolph David Willis
| screenplay     = Alan Rudolph
| story          = Kurt Vonnegut, Jr.  
| starring       = Bruce Willis Albert Finney Nick Nolte Barbara Hershey Glenne Headly Lukas Haas Omar Epps
| music          = Mark Isham
| cinematography = Elliot Davis
| editing        = Suzy Elmiger
| studio         = Hollywood Pictures Buena Vista Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $12 million
| gross          = $178,278
}} novel of the same name by Kurt Vonnegut, Jr. It was entered into the 49th Berlin International Film Festival.   

==Cast==
* Bruce Willis as Dwayne Hoover
* Albert Finney as Kilgore Trout
* Nick Nolte as Harry LeSabre
* Barbara Hershey as Celia Hoover
* Glenne Headly as Francine Pefko
* Lukas Haas as George "Bunny" Hoover
* Omar Epps as Wayne Hoobler
* Vicki Lewis as Grace LeSabre
* Buck Henry as Fred T. Barry Ken Campbell as Eliot Rosewater / Gilbert
* Jake Johanssen as Bill Bailey
* Will Patton as Moe the truck driver
* Chip Zien as Andy Wojeckowzski
* Owen Wilson as Monte Rapid
* Alison Eastwood as Maria Maritimo
* Shawnee Smith as Bonnie McMahon
* Michael Jai White as Howell Michael Duncan as Eli
* Kurt Vonnegut, Jr. as Commercial director
* Doug Maughan (voice) as TV/radio announcer (uncredited)

==Production==
Lukas Haas makes a cameo as Bunny, Dwaynes son, who, in the novel, plays piano in the lounge at the Holiday Inn. For legal reasons, in the film Bunny instead plays at the AmeriTel Inn.
 Twin Falls, Idaho.

Kurt Vonnegut, Jr. makes a one-line cameo as a TV commercial director.

==Vonneguts reaction==
At the close of the Harper Audiobook edition of Breakfast of Champions, there is brief conversation between Vonnegut and long-time friend and attorney, Donald C. Farber in which the two, among jokes, disparage this loose film adaptation of the book as "painful to watch." 

==Reception==
===Critical response===
Breakfast of Champions received negative reviews, scoring a 25% on Rotten Tomatoes. In his review for The New York Times, Stephen Holden wrote, "In many ways, Breakfast of Champions is an incoherent mess. But it never compromises its zany vision of the country as a demented junkyard wonderland in which we are all strangers groping for a hand to guide us through the looking glass into an unsullied tropical paradise of eternal bliss."    Entertainment Weekly gave the film an "F" rating and Owen Gleiberman wrote, "Rudolph, in an act of insane folly, seems to think that what matters is the story. The result could almost be his version of a Robert Altman disaster — a movie so unhinged it practically dares you not to hate it."    In his review for the San Francisco Chronicle, Peter Stack wrote, "Rudolph botches the material big time. Relying on lame visual gimmicks that fall flat, and insisting on pushing almost every scene as frantic comedy weighted by social commentary, he forces his actors to become hams rather than believable characters."    Sight and Sound magazines Edward Lawrenson wrote, "Willis performance, all madness, no method, soon feels embarrassingly indulgent."    In his review for the Los Angeles Times, Kevin Thomas wrote, "As it is, Breakfast of Champions is too in-your-face, too heavily satirical in its look, and its ideas not as fresh as they should be. For the film to have grabbed us from the start, Rudolph needed to make a sharper differentiation between the everyday world his people live in and the vivid world of their tormented imaginations."    In her review for the Village Voice, Amy Taubin wrote, "Another middle-aged male-crisis opus, it begins on a note of total migraine-inducing hysteria, which continues unabated throughout."    The French filmmaker and critic Luc Moullet, on the other hand, regarded it as one of the greatest films of the 1990s. 

==See also==
* Cross-dressing in film and television
* Kilgore Trout

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 