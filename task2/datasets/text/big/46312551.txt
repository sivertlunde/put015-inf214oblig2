Rajadhi Raja Raja Kulothunga Raja Marthanda Raja Gambeera Kathavaraya Krishna Kamarajan
{{Infobox film
| name           = Rajadhi Raja Raja Kulothunga Raja Marthanda Raja Gambeera Kathavaraya Krishna Kamarajan
| image          = 
| image_size     =
| caption        = 
| director       = Balu Anand
| producer       = J. A. Mohammed Ali
| writer         = Balu Anand
| starring       =  
| music          = Mansoor Ali Khan
| cinematography = B. S. Nandhalal
| editing        = KMP Kumar
| distributor    =
| studio         = Raj Kennedy Films
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1993 Tamil Tamil action Mansoor Ali Uday Prakash, Nagesh, Jaiganesh, Venniradai Moorthy and S. S. Chandran playing supporting roles. The film, produced by J. A. Mohammed Ali, had musical score by Mansoor Ali Khan and was released on 24 June 1993. Its the longest film title ever in Tamil cinema.      

==Plot==
 Mansoor Ali Khan) is a smart petty thief, he can dress up as a conductor or a police officer to rip off his victims. Despite being a thief, he helps the needy like Robin Hood.

Radhika (Nandhini), a wealthy heiress, is brought up by her three guardians : the lawyer (Nagesh), Subramanian (Jaiganesh) and Subramanians wife Shalu (Abhilasha) but Chalu wants to fully benefit of Radhikas heritage and she drugs Radhika with the help of a Swamy. So Radhika later becomes mentally ill as Chalu wanted. One day, Radhika manages to get away. She ends up in a brothel and she is raped by Kulothungan who was drunk. Her guardians finally find her.

Later, Shalu hires Kulothungan to play Radhikas husband and Kulothungan gets married with Radhika. Kulothungan decides to protect Radhika from Shalu. Soon, Shalu joins forces with Kulothungans ennemy Rao (Srihari (actor)|Srihari). They kill Subramanian and blame the innocent Kulothungan. The honest police Guru Subramaniam (Napoleon (actor)|Napoleon) takes charge of this odd affair. What transpires later forms the crux of the story.

==Cast==

  Mansoor Ali Khan as Kulothungan
*Nandhini as Radhika Napoleon as Guru Subramaniam Srihari as Rao Uday Prakash
*Nagesh as lawyer
*Jaiganesh as Subramanian
*Venniradai Moorthy as Pichumani
*S. S. Chandran
*V. K. Ramasamy (actor)|V. K. Ramasamy
*R. Sundarrajan (director)|R. Sundarrajan Vivek as Vivek Pandu
*Idichapuli Selvaraj
*TKS Natarajan
*Vadivukkarasi
*Vichithra
*Silk Smitha
*Disco Shanti
*Abhilasha
*Bindu Kose
*Radha Rani
*Padmavathi
*Jayanthi
*Kumarimuthu
*Kullamani
*Oru Viral Krishna Rao
*Krishnamoorhty as Yoginder
*Gundu Kalyanam
*Thayir Vadai Desigan
*Thavakalai
*Karuppu Subbaiah
*Vellai Subbaiah
*Haja Shareef
*Nellai Siva
 

==Soundtrack==

{{Infobox Album |  
| Name        = Rajadhi Raja Raja Kulothunga Raja Marthanda Raja Gambeera Kathavaraya Krishna Kamarajan
| Type        = soundtrack Mansoor Ali Khan
| Cover       = 
| Released    = 1993
| Recorded    = 1993 Feature film soundtrack
| Length      = 18:38
| Label       = 
| Producer    = Mansoor Ali Khan
| Reviews     = 
}}
 Mansoor Ali Khan. The soundtrack, released in 1993, features 5 tracks with lyrics also written by Mansoor Ali Khan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Chandrabose || 3:03
|- 2 || Rajadhi Raja || Malgudi Subha || 1:45
|- 3 || Aattam Podalam || S. P. Balasubrahmanyam, Chorus || 5:21
|- 4 || Mama Mama || Swarnalatha, Mansoor Ali Khan || 4:26
|- 5 || Marutha Marikozhundu || T. S. Raghavendra, Vani Jairam || 4:03
|}

==Reception==
Malini Mannath of The New Indian Express panned the film and said : "A film stricly for his (Mansoor Ali Khans) fans, that too the most ardent ones". 

==References==
 

==External links==
*  

 
 
 
 