A Certain Sacrifice
{{Infobox film
| name           = A Certain Sacrifice
| image          = ACertainSacrificeVHScover.jpg
| caption        = Swedish VHS front cover
| image_size     =
| director       = Stephen Jon Lewicki
| writer         = Stephen Jon Lewicki Jeremy Pattnosh Robert Manganaro Morris Madonna Jeremy Pattnosh Charles Kurtz
| narrator       = Chuck Varesko
| music          = Jeremy Pattnosh
| editing        = Stephen Jon Lewicki Robert Manganaro Morris
| cinematography = Stephen Jon Lewicki
| studio         = Cine Cine Productions
| distributor    = Vidimax Home Entertainment Virgin Video Worldvision Home Video
| released       = 1985
| runtime        = 60 min.
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
A Certain Sacrifice is a 1985 American drama film co-written and directed by Stephen Jon Lewicki and starring Madonna (entertainer)|Madonna, Jeremy Pattnosh and Charles Kurtz. It was Madonnas first movie, filmed around September 1979, but not released until 1985.

The film is an "oddball" independent film, shot on-and-off over two years in New York City for just $20,000. Madonna finished her scenes in the Fall of 1980.    Almost all of the cast were unpaid.

== Plot == Satanic sacrifice is performed. Dashiell later wipes Raymonds blood all over Bruna.

== Production ==
Despite Madonnas second thoughts about having participated in this movie, Lewicki had nothing but compliments for her. One of his oft-repeated stories was how he "discovered" Madonna and was amazed that she hand-wrote a three-page letter for a part that didnt even pay. She was only paid $100, only because she was short on her apartment rent and Lewicki paid to help out. In Christopher Andersens 1991 biography, Madonna Unauthorized, Lewicki stated: "That woman has more sensuality in her ear than most women have anywhere on their bodies".    

Actor Jeremy Pattnosh wrote and performed several songs in the film, including: "Certain Sacrifice (Raymond Hall Must Die Tonight)" & "Screamin Demon Lover".

== Release ==
In 1985, A Certain Sacrifice was released on video to capitalize on Madonnas fame, and in 1986, there were theatrical midnight screenings.  Madonna tried to buy the rights from director Stephen Jon Lewicki for $5,000. Unsuccessful, she then attempted to ban the film from being seen. Stephen Lewicki invited her to view it; Madonna was reportedly unhappy with the result. According to Lewicki, she had an expression of horror on her face and screamed: "Fuck you".     

== References ==
 

== External links ==
*  

 
 

 