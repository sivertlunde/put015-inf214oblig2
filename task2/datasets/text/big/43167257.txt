Arjun (2011 film)
 
 
{{Infobox film
| name           = Arjun
| image          = Arjun Posters.jpg
| producer       = M. A. Burondkar F. M. Ilyas
| director       = F. M. Ilyas
| screenplay     = F. M. Ilyas
| music          = Lalit Sen
| cinematography = Najeeb Khan
| editing        = Faisal Mahadik Imran Mahadik
| released       =  
| country        = India
| language       = Marathi
}} Marathi अर्जुन )  Indian film released on 16 September 2011 by Cinematics.  It was written, directed and produced by F. M. Ilyas along with A. M. Burondkar.

== Plot ==
 

A youth sets up his own business despite sociocultural and political issues that stand in his way.

== Cast ==

* Sachit Patil As Arjun
* Amruta Khanvilkar As Anushka
* Vidyadhar Joshi As Ratan Shah
* Vinay Apte As Jay Thackeray
* Varsha Usgaonkar As Maya Thackeray 
* Arun Nalawade As Bajirao
* Uday Tikekar As Mahajan
* Snigdha Sabnis As Arjun’s Mother
* Kamalesh Sawant As Police Officer
* Uday Sabnis As Advocate
* Anant Jog As Income Tax Officer 
* Mamata Soni As Dancer

== Soundtrack ==

The songs were composed by Lalit Sen. 

{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Gaar Gaar Ha Pawan Bawra  | extra1 = Shreya Goshal| lyrics1 = Zaheer Kalam | length1 = 5:27
| title2 = Pahilya Priticha Gandh ( Female)  | extra2 = Devaki Pandit | lyrics2 = Ibrahim Afghan | length2 = 4:52
| title3 = He Shwas Tuze  | extra3 = Bela Shende, Kunal Ganjawala| lyrics3 = Ashwini Shende | length3 =5:24
| title4 = Mazya Dolyatil Kajal  | extra4 = Vaishali Samant | lyrics4 = Zaheer Kalam | length4 = 3:37
| title5 = Pahilya Priticha Gandh ( Male)  | extra5 = Udit Narayan | lyrics5 = Ibrahim afghan | length5 = 4:52
| title6 = Roz Kasoti Roz Samna  | extra6 = Kavita Krishnamurthy | lyrics6 = Zaheer Kalam | length6 = 5:22
| title7 = Gaar Gaar Ha Pawan Bawra  | extra7 = Lalit Sen | lyrics7 = Instrumental | length7 = 5:34
| title8 = He Shwas Tuze  | extra8 = Lalit Sen | lyrics8 = Instrumental | length8 =5:22
}}

== Release ==
Arjun is the first film to be screened at the Bombay Stock Exchange. 

== Reception ==

=== Awards ===

* Best Supporting Actor 2011 Maharashtra State Film Awards
* Best Dialogues 2011 Maharashtra State Film Awards
* Best Singer (Female)2011 Maharashtra State Film Awards
* Best Choreographer 2011 Maharashtra State Film Awards
* Best Editing 2011 Maharashtra State Film Awards

==== Maharashtra Times: Mata Sanman ====
* Best Choreographer 

==== Zee Gaurav ====
* Best Singer (Female)

==== Sanskruti Kaladarpan ====
* Best Popular Film
* Best Choreographer
 
==== Sayadri Cine Awards ====
 

== References ==

 

*  

*  
*  
*  
*  
*  

== External links ==
*  

 
 
 