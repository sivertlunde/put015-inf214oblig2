The Seventh Veil
{{Infobox film
| name           = The Seventh Veil
| image          =  Seventh Veil.jpeg
| caption        = The Seventh Veil film poster
| director       = Compton Bennett
| producer       = Sydney Box
| writer         = Sydney Box Muriel Box
| starring       = James Mason
| music          = Benjamin Frankel 
| cinematography = Reginald Wyer
| editing        = Gordon Hales
| distributor    = General Film Distributors (UK) Universal Pictures (US)
| released       = 18 October 1945 (UK) 15 February 1946 (US)  DVD 2012 (UK)
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         = £92,000 Sarah Street, Transatlantic Crossings: British Feature Films in the USA, Continuum, 2002 p 114  gross = £2 million (by Feb 1948) 
| preceded_by    = 
| followed_by    = 
}}

The Seventh Veil is a 1945 British melodrama film made by Ortus Films (a company established by producer Sydney Box) and released through General Film Distributors in the UK and Universal Pictures in the United States.

==Plot==
Francesca Cunningham (Ann Todd) is a silent, suicidal mental patient under the care of Dr. Larsen (Herbert Lom). Via hypnosis, Larsen leads her to describe her life history so he can investigate the events that brought her to attempt suicide. The film largely consists of a series of flashbacks in which Francesca talks about her life, removing successive "veils" to recover memories.
 Hugh McDermott), an American studying in London, becomes romantically interested in Francesca. Although she is initially unresponsive, Francesca and Peter become engaged, but she has not yet reached her majority (then 21) and Nicholas withholds his consent. He insists they leave for Paris in the morning; she completes her education, and begins her career on the continent.

Years pass. Nicholas and Francesca return to Britain when she is invited to perform at the Royal Albert Hall, but she discovers Peter has married someone else. An artist, Maxwell Leyden (Albert Lieven), is invited by Nicholas to paint her portrait; they soon fall in love and agree to live together. Still apparently her guardian, Nicholas becomes angry at the news and strikes her hands with his cane while she plays. She flees from him, but while with Max, is involved in a serious car accident and suffers burns to her hands. Francesca becomes convinced she will never play again.

After therapy — and now cured, according to Dr Larsen — Francesca finds that Nicholas is her real love rather than Peter (now divorced) or Max.

==Production== Grieg and Rachmaninoff 2nd piano concertos.

Eileen Joyce, whose name does not appear in the credits, was the pianist who substituted for Todd on the soundtrack. She also made a short film for Todd to practise to, and even coached Todd personally in her arm movements. It is Joyces hands that are seen in all the close-ups. 

==Reception== Academy Award Best Original Screenplay (for Sydney and Muriel Box) in 1947. The film has the 10th top audience of all films, 17.9 million, placing it above most modern box-office successes.

In 1951 Ann Todd, Herbert Lom and Leo Genn appeared in a stage adaptation in London.

In 2004 the British Film Institute compiled a list of the 100 biggest UK cinematic hits of all time based on audience figures, as opposed to gross takings. The Seventh Veil placed 10th in this list  with an estimated attendance of 17.9 million people. 

==Cast==
* James Mason as Nicholas
* Ann Todd as Francesca
* Herbert Lom as Dr. Larsen Hugh McDermott as Peter Gay
* Albert Lieven as Maxwell Leyden
* Yvonne Owen as Susan Brook David Horne as Dr. Kendall
* Manning Whiley as Dr. Irving
* Grace Allardyce as Nurse
* Ernest Davies as Parker John Slater as James

==References==

===Notes===
 

===Bibliography===
*The Great British Films, pp 88–90, Jerry Vermilye, 1978, Citadel Press, ISBN 0-8065-0661-X

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 