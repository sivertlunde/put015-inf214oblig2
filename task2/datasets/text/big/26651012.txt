Mine Your Own Business
{{Infobox film
| name           = Mine Your Own Business
| image          = Mine_Your_Own_Business_tposter.JPG
| caption        = Theatrical release poster
| director       = Phelim McAleer and Ann McElhinney   
| producer       = Phelim McAleer and Ann McElhinney
| writer         = Phelim McAleer
| starring       = 
| narrator       = Phelim McAleer
| music          = 
| cinematography = Ian Foster
| editing        = Mairead McIvor
| studio         = 
| distributor    = 
| released       =  
| runtime        =  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} industrial progress, and consequently locks the people of the area into lives of poverty.  The film shows that the majority of the people of the village support the mine, and the investment in their hometown.  The film presents foreign environmentalists as alien agents opposed to progress, while residents are depicted as eagerly awaiting the new opportunity. 

==Film content== McAleer and Lucian then travel to other impoverished communities in Madagascar and Chile that are also waiting for large mining projects. 

In the documentary, Lucian meets Mark Fenn from the World Wildlife Fund, who is shown living in luxurious conditions, at one point showing off his $35,000 sailboat to the cameras, all the while advocating the value of living a simplistic, village life. 

===Stated purpose=== correlated with the sort of poverty that afflicts the region.   
 itself was built by riches that it pulled from the Earth; to deny the people of Roşia Montană the same sort of development and prosperity, he concludes, is the height of casuistry.
 high levels of cadmium and lead), and also would be required to maintain a fund with $30 million to be used for further cleanup of the area after the mining company discontinues its operations there.   

==Criticisms==
Criticisms of the film included the claim by environmentalist groups that the film was partly funded by Gabriel Resources, the Canadian mining company behind the proposed project, a fact which the filmmakers readily admit.     The filmmakers vehemently reject, however, the implication that this funding affected the content of the film.  McAleer even goes so far as to say that (contrary to convention) the funders had "absolutely no control" over the film, and never even saw any part of the film before it was complete, allowing him to create, "in many respects," the "most independent documentary   ever made."     
The president of Gabriel Resources, Alan Hill, said at the first screening of the movie, "Before, the environmentalists would lob mortars at us and we would keep our heads down. Now, there is a big push back." 

==See also==
* Not Evil Just Wrong
* FrackNation

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 