Goopy Geer (film)
{{Infobox Hollywood cartoon
| cartoon_name = Goopy Geer
| series = Merrie Melodies (Goopy Geer)
| image = Goopy Geer.jpg
| caption = Goopy Geer playing the piano.
| director = Rudolf Ising
| story_artist =
| animator = Isadore Freleng Rollin Hamilton
| layout_artist = Isadore Freleng (uncredited)
| background_artist = Art Loomer (uncredited)
| narrator =
| voice_actor =
| musician = Frank Marsales
| producer = Hugh Harman Rudolf Ising Leon Schlesinger
| studio = Warner Bros.
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = April 16, 1932
| color_process = Black-and-white
| runtime = 7 minutes
| preceded_by = Crosby, Columbo and Vallee
| followed_by = Its Got Me Again English
}}
 the title character.

==Synopsis==
The customers in a nightclub clamor for Goopy Geer, who then comes out on the stage and entertains them by playing the piano, first with his fingers and his ears, later with his animated gloves. Hes soon accompanied by a girl who tells a joke and sings a song.
 coat racks dance together.

Toward the end, a drunken horse breathes fire and destroys the piano, but Goopy keeps right on playing.

==Notes== Foxy short Lady, Play Your Mandolin! Also, one of the customers, a fat lady hippopotamus|hippo, had also appeared in a Foxy short, Smile, Darn Ya, Smile! Goopy bears some resemblance to Disneys (unnamed at the time) Goofy who first came along 39 days later.

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 

 