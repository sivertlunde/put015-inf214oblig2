Witnesses (film)
{{Infobox film
| name           = Witnesses
| image size     = 
| image	=	Witnesses FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Vinko Brešan
| producer       = Ivan Maloča
| writer         = Vinko Brešan Jurica Pavičić Živko Zalar
| narrator       = 
| starring       = Leon Lučev
| music          = 
| cinematography = Živko Zalar
| editing        = Sandra Botica
| studio         = 
| distributor    = 
| released       = 20 July 2003
| runtime        = 90 minutes
| country        = Croatia
| language       = Croatian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Croatian drama film directed by Vinko Brešan. It was released in 2003.

==Synopsis==
The plot of the movie is centered in the city of Karlovac in 1992, during the Croatian War of Independence. The front lines, where Croatian and Serbian forces fight each other, lie near the city. Meanwhile, in the city of Karlovac, a Serbian civilian Vasić is murdered. The story follows the local police officer Barbir (Dražen Kühn), who tries to solve the murder in spite of ethnic hatred and war revolving nearby.

The films screenplay is based on Jurica Pavičićs 1997 novel The Alabaster Sheep (Ovce od gipsa), which was in turn inspired by a real-life case of murder of the Zec family in Zagreb in 1991. 

==Cast==
* Leon Lučev - Krešo
* Alma Prica - Novinarka
* Mirjana Karanović - Majka
* Dražen Kühn - Barbir
* Krešimir Mikić - Joško
* Marinko Prga - Vojo
* Bojan Navojec - Barić
* Ljubomir Kerekeš - Dr. Matić
* Predrag Predjo Vušović - Ljubo
* Tarik Filipović - Javni tužitelj
* Rene Bitorajac - Albanac
* Ivo Gregurević - Otac
* Vanja Drach - Penzioner
* Helena Buljan - Susjeda

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 


 