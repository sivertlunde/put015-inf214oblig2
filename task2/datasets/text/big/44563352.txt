The Kerry Gow
{{infobox film name = The Kerry Gow director = Sidney Olcott producer = Kalem Company writer = Gene Gauntier based on =   starring = Alice Hollister Jack J. Clark distributor = General Films cinematography = George K. Hollister
| released =  
| runtime = 3000 ft, 3 reels
| country = United States language = Silent film (English intertitles) 
}}

The Kerry Gow is a 1912 American silent film produced by Kalem Company and distributed by General Films. It was directed by Sidney Olcott with Alice Hollister and Jack J. Clark in the leading roles.

==Cast==
* Alice Hollister - Nora Drew
* Jack J. Clark - Dan OHara
* J.P. McGowan - Valentine Hay
* Robert Vignola - Darby ODrive
* Jack Melville - Jack Drew
* Eddie OSullivan - Patrick Drew
* Sidney Olcott - Captain Kiernan
* George Lester - Major Gruff
* Sonny OSullivan - Dinny Doyle
* Helen Lindroth - Alice Doyle

==Production notes==
The film was shot in Beaufort, County Kerry, Ireland, during the summer of 1912.

==References==
* Michel Derrien, Aux origines du cinéma irlandais: Sidney Olcott, le premier oeil, TIR 2013. ISBN 978-2-917681-20-6  

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 
 


 