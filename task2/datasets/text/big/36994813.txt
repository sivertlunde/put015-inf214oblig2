Riders of the Range (1949 film)
{{Infobox film
| name           = Riders of the Range
| image          =
| producer       =
| director       = Lesley Selander
| writer         =
| starring       = Tim Holt
| music          =
| cinematography =
| editing        
| distributor    = RKO Radio Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Riders of the Range is a 1949 Western film.

==Plot==
Two cowboys are hired by a rancher to save her brother from a gambler.

==Reception==
The film reported a loss of $50,000, indicating the declining market for B Westerns with the spread of television. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p240 

==References==
 

==External links==
*  
 
 
 
 
 


 