Down Twisted
{{Infobox film
| name           = Down Twisted
| image          = Down twisted film.jpg
| caption        = Theatrical release poster
| director       = Albert Pyun
| producer       = Yoram Globus Menahem Golan
| writer         = Gene ONeill Tom ONiell Albert Pyun Noreen Tobin
| starring       = Carey Lowell Charles Rocket Trudy Dotchterman Norbert Weisser Linda Kerridge Nicholas Guest Thom Mathews Courteney Cox Galyn Görg
| music          = Eric Allaman Walt Lloyd
| editing        = Dennis M. OConnor
| distributor    = The Cannon Group Norstar Releasing Golan-Globus Productions
| released       = March 16, 1987
| runtime        = 88 mim.
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Down Twisted is a 1987 thriller film, directed by Albert Pyun,  starring Carey Lowell, Charles Rocket, Courteney Cox, Norbert Weisser Linda Kerridge, Trudi Docterman and Nicholas Guest. 

==Plot==
A naïve, good-hearted Los Angeles waitress does not think twice about helping her troubled roommate. Her help lands her in Central America fleeing for her life with a grungy mercenary.

==Release==
The film was given a limited theatrical release in March 1987. The film did not make its home video premiere in the United States until 1990 when Media Home Entertainment released it onto video shelves.

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 


 