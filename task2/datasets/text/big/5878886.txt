Black Like Me (film)
{{Infobox film
| name = Black Like Me
| image = 
| image_size =   
| alt = 
| caption = 
| director = Carl Lerner
| producer = Julius Tannenbaum
| screenplay = Carl Lerner Gerda Lerner Paul Green  
| based on =  
| starring = James Whitmore
| music = Meyer Kupferman
| cinematography = Victor Lukens Henry Mueller II
| editing = Lora Hays
| studio = The Hilltop Company
| distributor = Continental Distributing
| released =  
| runtime = 107 minutes 
| country = United States
| language = English Latin
}}
Black Like Me is a 1964 American drama film co-written (with Gerda Lerner) and directed by Carl Lerner, based on the book Black Like Me. The film stars James Whitmore, Sorrell Booke, and Roscoe Lee Browne. 

The DVD was released December 11, 2012, in North America from Video Services Corp. The DVD also includes a documentary titled Uncommon Vision about John Howard Griffin, the journalist on which the main character is based. 

==Plot== journalist who black man in the deep South, from New Orleans to Atlanta, where he encounters a great deal of racism from both white and black people.

==Cast==
* James Whitmore as John Finley Horton (Black Like Me author John Howard Griffin)
* Sorrell Booke as Dr. Jackson
* Roscoe Lee Browne as Christopher
* Al Freeman, Jr. as Thomas Newcomb
* Will Geer as truck driver
* Robert Gerringer as Ed Saunders
* Clifton James as Eli Carr John Marriott as Hodges
* Thelma Oliver as Georgie Lenka Petersen as Lucy Horton
* P.J. Sidney as Frank Newcomb
* Billie Allen as Vertell
* Alan Bergmann as Charles Maynard

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 