I Love You, I Love You Not
{{Infobox film
| name           = I Love You, I Love You Not
| image          = Loveyounot.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Billy Hopkins
| writer         = Wendy Kesselman
| starring       = Jeanne Moreau Claire Danes Jude Law James Van Der Beek Julia Stiles 
| music          = Gil Goldstein
| cinematography = Maryse Alberti
| editing        = Paul Karasick
| released       =  
| runtime        = 88 mins
| country        = France Germany United States United Kingdom
| language       = English
}}
I Love You, I Love You Not is a 1996 romantic drama film directed by Billy Hopkins and written (also the play) by Wendy Kesselman.

==Plot==
The film is told through the stories of two women: Nana, a grandmother, and Daisy, her granddaughter.  Daisy tells Nana of her strong and blossoming romance with a young man named Ethan and her problems at school because shes Jewish. Nana tells the story of her young life when she was sent to a ghetto and then a concentration camp. The romantic love feelings she has for the boy are indeed strong and genuine, but the romantic love he has for her is questionable. He lets his friends judge her from the outside, not for who she is on the inside, and when she turns out to not be like every other girl he breaks up with her. Daisy is sad so she goes and sees Nana and takes her anger out on her. She then runs away and tries to kill herself but she does not. At the end, she tries to see him again but he looks at her for a long time and walks away with his friends. She stands there; heartbroken, sad and crying, realizing that maybe it was not meant to be and she walks away happy .

==Cast==
*Jeanne Moreau as Nana
*Claire Danes as Daisy/Young Nana
*Jude Law as Ethan
*James Van Der Beek as Tony
*Kris Park as Seth
*Lauren Fox as Alsion
*Emily Burkes-Nossiter as Jessica
*Carrie Szlasa as Jane
*Julia Stiles as Young Nanas Friend
*Robert Sean Leonard as Angel of Death

==External links==
* 
*  

 
 
 
 
 
 
 
 


 