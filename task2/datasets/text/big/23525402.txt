Padre nuestro (1985 film)
 
{{Infobox film
| name           = Padre nuestro
| image          = 
| caption        = 
| director       = Francisco Regueiro
| producer       = Eduardo Ducay Julián Marcos
| writer         = Francisco Regueiro Ángel Fernández Santos
| starring       = Victoria Abril
| music          = 
| cinematography = Juan Amorós
| editing        = Pedro del Rey
| distributor    = 
| released       = May, 1985
| runtime        = 105 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Padre nuestro is a 1985 Spanish drama film directed by Francisco Regueiro. It was screened in the Un Certain Regard section at the 1985 Cannes Film Festival.   

==Cast==
* Victoria Abril - Cardenala
* Rafaela Aparicio
* Luis Barbero
* Lina Canalejas
* Yolanda Cardama
* Diana Peñalver
* Emma Penella - María
* Francisco Rabal - Abel
* Fernando Rey - Cardinal
* Amelia de la Torre Francisco Vidal
* José Vivó

==References==
 

==External links==
* 

 

 
 
 
 
 
 