Gangster High
{{Infobox film
| name           = Gangster High
| image          = Gangster_High_poster.jpg
| caption        =
| film name      =  {{Film name
| hangul         =  
| hanja          =  써클
| rr             = Pongnyeok Sseokeul
| mr             = Pongnyŏk Ssŏkŭl}}
| director       = Park Ki-hyeong
| producer       = Chung Tae-won   Moon Woo-seong   Jo Hyeon-jun
| writer         = Park Ki-hyeong
| starring       = Jung Kyung-ho Jang Hee-jin Cho Jin-woong Kim Hye-sung Kim Ye-ryeong Lee Haeng-seok
| music          = Lee Seung-il
| cinematography = Kim Eung-taek
| editing        = Kim Sun-min
| distributor    = Showbox
| released       =  
| runtime        = 101 minutes
| language       = Korean
| budget         =
}}
Gangster High is a 2006 South Korean film written and directed by Park Ki-hyeong.

==Plot==
In 1991, studious soccer enthusiast Sang-ho creates a soccer club called "Tigers" along with his friends Jae-gu and Chang-bae. Through Jae-gu, he meets an attractive, rebellious girl named Su-hee, who turns out to be Jong-suks girlfriend, leader of a gang called TNT. The two gangs soon become embroiled in intense and violent conflict. When Jae-gu is killed in a car accident related to a gang fight with Jong-suk, the Tigers decide to avenge him. The two gangs meet in a billiards club, and, despite the pleas of Su-hee, a vicious and brutal fight ensues. Three people end up dying, and the rest permanently damaged, including some of Sang-hos friends. Sang-ho, being the person who started the fight and the only one to walk away relatively unscathed, is charged with the murder of the three people and is sent to prison, where he reminisces upon the fact that the Tiger soccer club was initially created for the enjoyment of soccer and friendship.   

==Release and reception==
Gangster High opened in South Korea in October, 2006, and premiered at the European Film Market in Germany on February 8, 2007. In the United States, Derek Elley of Variety (magazine)|Variety described it as bleak and violent, but praised it as "well drawn in all its diversity (and with occasional humor) by a good cast."  James Mudge of Beyond Hollywood gave it a positive review: "Although its premise may sound overly familiar, Gangster High certainly stands as one of the best teen violence films of recent years, with Park proving that he has not lost his touch when it comes to dealing with tales of troubled youth. Hard-hitting and gripping, it offers a different and arguably more convincing take on the usual themes, and contains enough vicious action to entertain as well as provoke." 

The film was distributed in the Netherlands on DVD in 2009 by Splendid Film. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 