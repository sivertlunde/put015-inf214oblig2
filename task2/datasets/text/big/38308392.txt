Mr. Pim Passes By (film)
{{Infobox film
| name           = Mr. Pim Passes By
| image          =
| caption        = Albert Ward 
| producer       =
| writer         = A.A. Milne (play)  Tom Reynolds
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = General Film Distributors
| released       = April 1921
| runtime        = 6,077 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent comedy Albert Ward play of the same title by A.A. Milne.

==Cast==
* Peggy Hyland as Olivia Marsden
* Campbell Gullan as Carraway Pim
* Maudie Dunham as Diana Marsden Tom Reynolds as James Brymer Henry Kendall as Brian Strange
* Hubert Harben as George Marsden
* Annie Esmond as Lady Marsden
* Wyndham Guise as Mr. Fanshawe

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 

 
 
 
 
 
 
 
 


 
 