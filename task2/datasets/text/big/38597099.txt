Oktoberfest (film)
{{Infobox film
| name           = Oktoberfest
| image          = 
| caption        = 
| director       = Dragan Kresoja
| producer       = Stevan Petrovic
| writer         = Branko Dimitrijevic Dragan Kresoja
| starring       = Svetislav Goncic
| music          = 
| cinematography = Predrag Popovic
| editing        = 
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Yugoslavia
| language       = Serbo-Croatian
| budget         = 
}}

Oktoberfest is a 1987 Yugoslav drama film directed by Dragan Kresoja. It was entered into the 15th Moscow International Film Festival.   

==Cast==
* Svetislav Goncic as Luka Banjanin
* Zoran Cvijanovic as Bane
* Zarko Lausevic as Skobi
* Vladislava Milosavljevic as Jasna
* Velimir Bata Zivojinovic as Skoblar
* Zeljka Cvjetan as Svetlana
* Goran Radakovic as Dule
* Tatjana Pujin as Mala Irena
* Djurdjija Cvetic as Skoblarova zena (as Djurdjija Cvijetic)
* Ruzica Sokic as Luletova majka
* Petar Kralj as Luletov otac
* Bogdan Diklic as Vanja
* Vesna Trivalic as Buca
* Branislav Lecic as Lepi
* Srdjan Todorovic as Goran

==References==
 

==External links==
*  

 
 
 
 
 
 