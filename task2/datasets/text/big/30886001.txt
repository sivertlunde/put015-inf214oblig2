Mingus: Charlie Mingus 1968
 
{{Infobox film
| name           = Mingus: Charlie Mingus 1968 
| director       = Thomas Reichman
| producer       = Thomas Reichman
| starring       = Charlie Mingus Carolyn Mingus
| editing        = Richard Clarke Marshall Potakin
| cinematography = Lee Osborne Michael Wadleigh
| studio         = Inlet Films
| distributor    = Film-makers Cooperative Films Inc.
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
}}
Mingus: Charlie Mingus 1968 is a 1968 American documentary film directed by Thomas Reichman (1944-1975) that follows the later life of jazz musician Charles Mingus and his five-year-old daughter in 1966, as they are being evicted from his New York apartment for not paying the rent. Reichman talks with Mingus in a very personal setting as the film documents Mingus outlook on society, women, music, his daughter, politics, and the country as a whole. The camera crew talks with Mingus conversationally and spends time with him and his daughter so that the film takes on an intimate air. 

The film highlights their way of life and how political and racial tensions affected people of the 1960s personally. 
The reason for Reichman’s investigation into Mingus’ eviction was to show the lifestyle of a prominent jazz musician and to show how American society was structured during the Civil Rights era. At a time when the  ), there were African Americans who didnt feel they were treated equally; in many cases they werent.    Throughout this film Mingus makes it clear that he believes that society is run for and by “white America” and goes on to say that if it were not for his standing as a jazz musician he would not be treated anywhere close to equally.

== Content ==
The film looks into the life of Mingus as a musician, a father, and a U.S. citizen simultaneously. To begin, the audience is given a first-hand look at Mingus’ true passion: music. Throughout the documentary there are clips of Mingus performing with a small ensemble of other musicians jamming in the outskirts of Boston at Lennies-On-The-Turnpike in Peabody, MA, a favorite club of jazz performers.

However, though Mingus’ musical talents are part of this film, they are not the main focus. Reichman instead documents how Mingus is being evicted from his New York city apartment. The movie begins with Mingus talking about his possible eviction and how he would not even have gotten a chance to appeal to the city if not for his standing as a musician. In his eyes, a normal Negro wouldnt have gotten a chance at all.

Mingus continues to speak his mind and shows the camera men a rifle that he has in his apartment. This rifle comes up again throughout the movie as he says that anyone can learn to kill and that they won’t be getting him out of his loft easily. If he wanted to, Mingus says, he could get all the winos in the city, bribe them with wine and get them to fight a war on the city. He makes a comparison to Hitlers tactics to get the Nazi’s to fight for him. At points Mingus seems to be rambling about certain topics but it is apparent that he is informed about politics and true to his views.
 
One of the matters he is most passionate about is how much he cares for his daughter. She is five years old and is also accustomed to having to living life out of boxes. The loft they are occupying in the film was intended to be a place for Mingus to build a music school where he could teach his musical talents to others. However, he said that he didnt start it yet because he was worried about being thrown out again, just like he had out of a couple previous places.

When the lease was signed for the apartment, Mingus said, there were supposed to be attorneys that made sure the new place would be secure if he moved out of his old one. Yet Mingus later learned that if he had just stayed in his old place a bit longer the city would have had to pay him to leave since there were plans to build a hospital on the grounds. Mingus blames this on his lack of education but also thinks that if he did not have the standing of a well-known musician he would have been out on the streets already.

Mingus and his daughter are in the end evicted from his apartment along with all of his expensive and sentimental musical equipment. The scene of officers going to his door draws a crowd along with many reporters.

To add insult to injury, Mingus is also questioned by the police about hypodermic needles and a rifle which were found when they were searching through his possessions. This leads to his arrest at the end of the film and with no apartment to go to after his release.

== Personality ==

One thing that Mingus was certainly not was passive. He was known to be a man of lots of charisma and with a tempestuous personality which got him to be known as “The Angry Man of Jazz.”.    During the film it is obvious that he has strong viewpoints on most topics and sticks to them.

There are times when he seems to be rambling on about certain topic or relating it to a completely different topic that doesnt quite make sense. At the time of this film he was suffering from certain psychological problems where his behavior became erratic, he ignored contacts, walked off stage early or got physically violent on stage. One instance around this time was when he punched trombone player Jimmy Knepper in the mouth and may have messed up his embouchure forever.

Although he is eccentric and extreme in some of his viewpoints he takes the time to develop them and they are obviously thought out. This makes the listener more willing to hear  his ideas even when they are as dramatic as Mingus saying that Hitler was simply fighting for land just as the musician is fighting for his apartment.

Yet, despite his sometimes volatile personality, Mingus was a likable person and those in the jazz scene wanted to have him around. When playing with the ensemble at the small Boston club, it is obvious that he has makes a connection with those that he comes in contact with. In between songs he makes playful comments that make the rest of the band laugh.

Mingus definitely had swagger that would make even his biggest skeptic break a smile if he wanted them to. He used lingo of the time like “Ya dig?” when he wanted to know if people understood what he was saying. Also he used musicians lingo such as “shed” (to practice), “jive” (swing) and much more. He was loved by both men and the women due to his passionate personality and amazing musical talent.

== Music ==

Charles Mingus is a renowned jazz musician; a type of music that is famous for its improvisational aspect. In a way this was how Mingus lived his life in that he seemed to take each day at a time.

The clips of footage taken of Mingus playing with other musicians at the jazz club made it apparent how much he felt the music. The musicians were each individually talented but jazz is about the interaction and responses that occurs between them and it was obvious that Mingus and the guys playing with him had that essential component.
 
Jazz musicians of the 1950s such as Dizzy Gillespie, Charlie Parker, John Coltrane, Miles Davis and many more lived and breathed the music. In order to be a true musician’s musician, which Mingus definitely was, there had to be immense dedication. When asked about Mingus, trumpet player Dizzy Gillespie said “Sometimes he plays and his fingers start bleedin.’” He also said that Mingus had a “lack of peers” because not many were up to his caliber and able to make music successfully with him.   

As a talented jazz bassist, which were very rare, Mingus was an amazing artist who played small clubs and ensembles throughout his professional career. Later in life, during the time of this film he wanted to teach others how to create music. At the end of the movie a reporter tells him that he has a lot to contribute to the world through teaching.

As Mingus speaks about his possible upcoming eviction he doesnt appear very worried about where he and his daughter will live – it seems as though he knows they will find somewhere to go. However, when he talks about his musical equipment he gets very sentimental. They are very valuable instruments and have strong sentimental value and he is worried that they will be damaged in the moving process.

== Minguss viewpoint ==
=== On America and society ===
Throughout the film it is clear that Mingus is disillusioned toward the way that the country is run. When asked to recite the pledge of allegiance he says: “I pledge allegiance to the flag–the white flag. I pledge allegiance to the flag of America. When they say “black” or “negro,” it means you’re not an American. I pledge allegiance to your flag. Not that I have to, but just for the hell of it I pledge allegiance. I pledge allegiance to the flag of the United States of America. The white flag, with no stripes, no stars. It is a prestige badge worn by a profitable minority. I pledge allegiance to the United States of America and to see that they will someday live up to the promises to the victims that they call citizens. Not only to the black ghettos but to the white ghettos, to the Chinese ghettos and the Japanese ghettos and to all the ghettos in the world.” He says he has suffered in the society of America.

Mingus had a reason to believe that race made a difference in the quality of life because he grew up in South Central Los Angeles which was still very segregated during his youth. Many African Americans began to inhabit South Central and it became a low-class suburb and affected Mingus’s hometown of Watts. 

This documentary took place a short time after the civil rights movement, touching many raw racial topics of that era. Social changes were taking effect very slowly. Mingus believed that most Negros didnt believe that they had a fair chance to progress in society.

Mingus also spoke about other nations and how most fighting doesnt make sense. When speaking about Hitler and about Germany during WWII he talks of how he knew a jazz pianist that had people close to her die and it didnt make sense because they couldnt have been bad people. He says that when he traveled to perform in Germany their hotel rooms werent clean and Swastika signs were put on the door of the Jewish manager they were traveling with.

At times during the movie it appears that Mingus is going off on tangents about politics and the way the world works. Yet he was a man of many big ideas and thought he could see behind the way that society worked. He had a strong sense of history and perspective that stemmed from his personal struggles and insight. 

=== On women ===
Mingus was known to love women and women also really loved him in more than just sexual ways. He talked to them intellectually which was uncommon for the time period.  Although Mingus’s descriptions of females and the relations between men and women were sexually descriptive they werent at all vulgar.

Sex was like a religion to Mingus in that he saw it as a very natural thing to do because the act made its participants see life and death. Everyone should settle down, kiss and love. He said that a man and a woman should touch for three or four days straight because “Jesus rose for three days didnt he? Well I   lay for three days.” With a laugh he suggested giving a woman three days and three nights of intimacy to see if she really did love him.

He described relations between a man and a woman as a natural fit and demonstrated the example of a light bulb and a socket. The light bulb and the socket couldnt function separately but together they could light things up and function beautifully. Mingus said that he needed to find himself a nymphomaniac and that everyone should take some time to make a little love. He was married four times and had numerous girl friends and mistresses. 

However he states that women should be able to protect themselves and not make bad choices. For example he says that rape in society has been on the rise but even knowing that  women still walk around with short skirts and low cut tops. He implied that he wants women to respect themselves.

=== His daughter ===
Throughout the film it is apparent how much Mingus cares about his five year old daughter and would do anything for her. There are touching scenes where he was playing with his daughter and was teaching her how to play piano. It is obvious that they had the intimate connection that comes along with a single parent and child that are on their own.

Even though his parenting skills may not have been up to par with some standards (he gives her wine and says he has before when they would be out and about), it is obvious he is a caring father. When his daughter says that she wants to move back to their old place on 5th Avenue, he replies that he would go so far as to kill to be able to keep them in a stable place.

On the topic of his daughter and men, he tells her to love only one man and to love him well. This does not match his own actions in that he has loved so many women, but he truly does want his daughter to have a good man that cares for her.

== Film making ==

Director Thomas Reichman compiled the interviews and performances spanning over several days into a 61 minute documentary which gives a real perspective on the life of a famous black jazz musician. That his interviews are not at all formal makes the film even more genuine since the camera crew seemed to literally be just hanging out with him for a few days and nights. Numerous times Mingus offered the director and camera men wine and would openly answer any of their questions. For the most part however, they didn’t seem to have to ask many questions because Mingus could easily go on talking about different topics for quite a while.

The film, being made in 1968, was in black and white which made put even more emphasis on the way that the emotions, feelings and tensions were shown on the screen. Since there was no color involved it made things such as Mingus’s facial expressions while describing “white America” very apparent. Also it was easy to see the love in his demeanor and face when he was on screen with his daughter and involving her in his every day activities.

Although Mingus was a musician, not an actor, he projected considerable charisma on camera and was very at ease. He could play up to the audience which was something that set him apart from nearly all other jazz musicians.  He knew the right cues and angles that make the movie flow smoothly. 
Even though this documentary followed one event in Mingus’s life for, it also offered insight into the rest of his life. The audience could see his love for music through the clips of him performing and the way he talked about his instruments. There was also background on his personal life and viewpoints that showed his ideas, where he had come from and where he wanted to go with his life.

There was a definite plot line which gave the film more depth than simply a series of interviews. The viewer was granted a look into the life of an African American artist which was different from that of a white famous musician of the same time.

== Historical frame ==
The 1960s were a turbulent time concerning race and relations. Five years prior to this documentary Martin Luther King Jr. gave his “I Have A Dream” speech and Malcolm X, The Black Panthers and the Nation of Islam were all vying for leadership of the oppressed American Negro. Obviously, change didnt occur overnight; Charles Mingus was clearly influenced by the rage African Americans felt by societys treatment of their race and he appears confident of the validity behind his feelings. There was definitely overcrowding in the American ghettos, including “black” areas of New York City that had been increasing since 1910.   

Mingus said in the film that his life had always been in boxes, which is why hardly any of his stuff was unpacked or had a place of its own. He wasnt expecting to be able to stay in the loft for long. 
One of the reasons Mingus offers in the film for racial disparity in American society is the enormous gap in education between the races.

He admits several times in the film that he isnt very well educated so it is hard for him to figure out society and the system. This mirrors the case of many African Americans at the time who didnt have the same opportunities as white counterparts due to segregated facilities, “white flight” from urban center and racist authority figures such as.,    among other racial inequalities

== Cultural components ==
At the end of the film Mingus was arrested for having hypodermic needles in his possession. He said he had them because his wife was a nurse and he had prescriptions. Although heroin was a large part of the jazz culture at the time, Mingus never took part in it and thought that people who did lots of drugs werent serious musicians. He said that a person had to work to be creative; all the substances in the world couldnt make a better musician. 

However, Mingus did take prescription drugs for depression which had an effect on him but he never considered himself a part of the drug culture.  He also drank a considerable amount but it didnt seem detrimental to his musical ability.   During the film he often has a glass and a bottle of wine nearby when he is in his apartment.

== References ==
 

==External links==
*  

 

 
 
 
 
 
 