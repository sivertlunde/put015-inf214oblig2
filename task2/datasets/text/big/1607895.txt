Kung Fu Hustle
 
 
 
{{Infobox film name = Kung Fu Hustle image = KungFuHustleHKposter.jpg caption = Mainland China release poster traditional = 功夫 jyutping = Gung1 Fu1}} director = Stephen Chow producer =   writer = {{plainlist|
*Stephen Chow
*Huo Xin
*Chan Man-keung
*Tsang Kan-cheung}} starring = Stephen Chow Danny Chan
*Yuen Wah
*Yuen Qiu Eva Huang}} music = Raymond Wong cinematography = Poon Hang-sang editing = Angie Lam studio = Star Overseas Beijing Film Studio Taihe Film Investment China Film Group Huayi Brothers distributor = Columbia Pictures   Sony Pictures Classics   released =   runtime = 98 minutes    country = Hong Kong China  language = Cantonese  budget = $20 million gross = $100,914,445   
}}
  Hong Kong-Chinese action comedy Danny Chan, Bruce Leung co-starred in prominent roles.
 cartoon style Chinese music, is often cited as its most striking feature.

The film was released on 23 December 2004 in China and on 25 January 2005 in the United States. It received highly positive reviews, with Rotten Tomatoes giving it a 90% fresh rating and Metacritic 78 out of 100. The film was also a commercial success, grossing US$17 million in North America and US$84 million in other countries. Kung Fu Hustle was the highest-grossing film in the history of Hong Kong until it was surpassed by You Are the Apple of My Eye in 2011.
 Golden Horse Awards.

==Plot== Deadly Axe Gang, exercises control over the city, openly killing anyone who gets in his way. One day, two troublemakers, Sing and Bone, come to Pig Sty Alley impersonating members of the Axe Gang to gain respect. Their plan fails, and Sings antics attract the real gang to the scene. In the massive brawl that ensues, more than fifty gangsters are defeated by three tenants who are Coolie, a laborer, Fairy, the clothing retailer, and Donut, the noodle chef. Coolie, Fairy, and Donut turn out to be incognito martial arts masters.

After the fight, Sing and Bone are apprehended by Brother Sum for publicly humiliating the Axe Gang. They are hung by their arms to be killed, but Sing uses his lock picking skills to escape and asks to join the gang. Sum is impressed by Sings lock picking, so he tells them that if they kill a person they will be accepted into the gang. Asked to explain his escape skills, Sing describes his childhood: he bought a Buddhist Palm manual from a beggar and trained himself to be a martial artist, but when he tried to defend a mute girl from bullies, he was beaten up and humiliated.

The next day, the duo return to Pig Sty Alley and attempt to murder the Landlady, but their efforts fail. The cantankerous Landlady chases Sing, who becomes badly injured in the process after being bitten on the lips by multiple snakes and hides in a traffic control pulpit, where his multiple injuries including comically swollen lips spontaneously heal, a surprise even to him. As he recovers, thrashing in pain from his injuries, he unconsciously throws his hands outward and violently dents the pulpit from the inside. These dents appear to be the result of kung-fu strikes.

Meanwhile, Sum hires two mystical assassins to kill the three martial artists at Pig Sty Alley. That night, the Landlady, fearing retaliation from the Axe Gang, evicts the three martial artists. In the process of leaving, Coolie is murdered by the assassins, who use a magical guqin as their weapon, and Fairy and Donut attain fatal injuries during the fight. The assassins are defeated by the Landlady and Landlord, who are in fact martial arts masters in self-imposed retirement. They had sworn not to fight anymore due to their young son being killed years earlier in a fight. Moments after Fairy dies, a very weak Donut speaks to the Landlord and Landlady right before dying before their eyes. The dead three are surrounded by the grateful but saddened Pig Sty Alley denizens. Afterward, sensing an impending showdown with the Axe Gang, the Landlady and Landlord order all the citizens and tenants to evacuate Pig Sty Alley.
 mental asylum to free a legendary fighter known as the Beast. Sing succeeds (although he experiences several moments of great foreboding during the job) and brings the Beast to Sum at his office. The Beast is initially flippant and his sloppy appearance bewilders Sum, who orders his men to torture the fighter. In response, the Beast stops a bullet with two fingers, and the Axe Gang bows in respect.

The Beast then confronts the Landlady and Landlord at the casino and they fight, with the Beast having the advantage. The Landlady and Landlord manage to overpower him with a large temple bell through which the Landlady amplifies her lama (martial art)|lions roar technique. Just before they finish him off, the Beast proclaims his awe at their abilities and claims to surrender. However, the moment the Landlady and Landlord let down their guard, the Beast treacherously stabs them both in the stomach with concealed daggers. The three end up intertwined, both sides immobilizing the other. Brother Sum orders Sing to hit the Landlord with a table leg. When Brother Sums berating becomes too much, Sing angrily bashes Brother Sum in the head to get him to shut up. Sing, having reformed, rushes in to help them by hitting the Beast with the table leg. Unfortunately, the blow only incenses the Beast, who pummels Sing into the floor. Before the Beast can deliver the final blow, the Landlord and Landlady grab Sing and flee. Sum angrily berates the Beast for letting them escape, and the Beast casually murders Sum and takes over his men.

At Pig Sty Alley, Sing undergoes a metamorphosis; the Beasts thrashing has inexplicably awoken his true potential as a kung-fu genius. He quickly recovers from his wounds, and suggests that the Landlord and Landlady rest. He easily defeats the Axe Gang before confronting the Beast. The two engage in a fierce brawl before the Beast uses his Toad Style techniques to send Sing rocketing into the sky. As he falls back to earth, Sing comes to terms with the Buddha and the techniques he learned from the manual as a child, and delivers the Buddhist Palm to defeat the Beast. After a final failed attack on Sing, the Beast accepts his defeat and appears to experience an epiphany. He begins to cry, and kneels before Sing, as if in supplication before his new master.

Some time later, Sing and Bone open a candy store with the vendors lollipop as the store sign. When the mute ice cream vendor walks by, Sing goes out to meet her. As they gaze upon one another, the camera spins around them as they transform into the children seen in the flashbacks. They smile, clasp hands and run into the candy store together. Various characters from the movie, who appear to have formed happy couples, walk through the square in front of the store. The beggar (suspected to be a magical traveller) who gave Sing the Buddhist Palm is then seen attempting to sell the Buddhist Palm manual to a young boy. The boy turns to leave until the beggar offers all of the manuals.

==Cast==

*Stephen Chow as Sing, a loser in life who aspires to join the Axe Gang. Danny Chan as Brother Sum, leader of the Axe Gang.
*Yuen Wah as the Landlord of Pig Sty Alley. He is also a master of Tai chi|taijiquan.
*Yuen Qiu as the Landlady of Pig Sty Alley. She is a master of the Lions Roar technique. Bruce Leung as the Beast, an old but incredibly strong kung fu master. He is rumoured to be the most dangerous person alive, though his skill is disguised by his unkempt appearance.  Twelve Kicks of the Tam School.
*Chiu Chi-ling as Fairy, the Tailor of Pig Sty Alley. He specialises in the art of Hung Ga Iron Fist Kung fu, and he fights with iron rings on his arms. Eight Trigram Gun (staff)|Staff.
*Lam Chi-chung as Bone, Sings sidekick. Eva Huang as Fong, Sings mute love interest and childhood acquaintance.
*Tin Kai-Man as Brother Sums adviser.
*Gar Hong-hay and Fung Hak-on as the Harpists, two killers hired by the Axe Gang. Their instrument is the guqin, or "Chinese harp".
*Lam Suet and Liang Hsiao as high-ranking members of the Axe Gang.
*Yuen Cheung-yan as the Beggar, the man who sold Sing the Buddhist Palm manual.
*Feng Xiaogang as the leader of the Crocodile Gang. He is killed by the Axe Gang at the start of the film.
* Min-hun Fung as 4 eyed clerk

==Production==

===Development===
 
Kung Fu Hustle is a co-production of the Beijing Film Studio and Hong Kongs Star Overseas.    After the success of his 2001 film, Shaolin Soccer, Chow was approached in 2002 by Columbia Pictures Film Production Asia, offering to collaborate with him on a project. Chow accepted the offer, and the project eventually became Kung Fu Hustle.  accessdate|publisher=sensasain.com|accessdate=7 October 2012|archiveurl=http://web.archive.org/web/20051222103730/http://www.sensasian.com/view/catalog.cgi/EN/1030|archivedate=22 December 2005}}  Kung Fu Hustle was produced with a budget of US$20 million. 

Chow was inspired to create the film by the martial arts films he watched as a child and by his childhood ambition to become a martial artist.    A senior Hollywood executive said Chow was "forced to grind through four successive scripts" and "found it very laborious". 

Chows first priority was to design the main location of the film, "Pig Sty Alley". Later in an interview Chow remarked that he had created the location from his childhood, basing the design on the crowded apartment complexes of Hong Kong where he had lived.     The 1973 Shaw Brothers Studio film, The House of 72 Tenants, was another inspiration for Pig Sty Alley.    Designing the Alley began in January 2003 and took four months to complete. Many of the props and furniture in the apartments were antiques from all over China. 

===Casting===
Kung Fu Hustle features several prolific Hong Kong action cinema actors from the 1970s. Yuen Wah, a former student of the China Drama Academy Peking Opera School who appeared in over a hundred Hong Kong films and was a stunt double for Bruce Lee, played the Landlord of Pig Sty Alley.  Wah considered starring in Kung Fu Hustle to be the peak of his career.  In spite of the films success, he worried that nowadays fewer people practice martial arts. {{cite news last = Zhang first = Xiaomin title = 从李小龙替身到影帝 元华：担忧中国功夫后继无人 (From a Bruce Lee impersonator to a movie star: Yuen Wah worries that Chinese martial arts may lack a successor) language = Chinese publisher = Eastern Sports Daily url = http://www.donnieyen.net/htm/kungfustar/185410374.htm accessdate = 2007-05-17 |archiveurl=http://web.archive.org/web/20060901133245/http://www.donnieyen.net/htm/kungfustar/185410374.htm
|archivedate=1 September 2006}} 
 The Man with the Golden Gun at the age of 18.     After a number of other small roles, she retired from films in the 1980s.  Kung Fu Hustle was her first role in nineteen years. Qiu, in order to fulfill Chows vision for the role, gained weight for the role by eating midnight snacks everyday. 
 Bruce Leung, who played the Beast, was Stephen Chows childhood martial arts hero.  Leung Siu Lung was a famous action film director and actor in the 1970s and 1980s, known as the "Third Dragon" after Bruce Lee and Jackie Chan. After becoming unpopular in the Taiwanese film market in the late 1980s following a visit to China, he switched to a career in business. Kung Fu Hustle was his return to the film industry after a fifteen-year hiatus. He regarded Chow as a flexible director with high standards, and was particularly impressed by the first scene involving the Beast, which had to be reshot 28 times. {{cite news last = Li first = Yijun title =《功夫》配角都有功夫 language = Chinese publisher = Zaobao date = 24 December 2004 url = accessdate = 2007-05-17  |archiveurl=http://web.archive.org/web/20041230162842/http://stars.zaobao.com/pages1/stephen241204.html
|archivedate=30 December 2004}} 

In addition to famous martial artists, Kung Fu Hustle features legends of  , who plays Inspector Chan at the beginning of the film, and Feng Xiaogang, who plays the boss of the Crocodile Gang.   
 Eva Huang, in her film debut, was chosen from over 8,000 girls. When asked about his decision in casting her Chow said that he "just had a feeling about her" and said that he enjoyed working with new actors. She chose to have no dialogue in the film so that she could stand out only with her body gestures.  
  

===Filming===
 
Filming took place in Shanghai from June 2003 to November 2003.    Two-thirds of the time was spent shooting the fight sequences.  Those scenes were initially choreographed by Sammo Hung, who quit after two months due to illness, tough outdoor conditions, interest in another project and arguments with the production crew.  Hung was replaced by Yuen Woo-ping, an action choreographer with experience ranging from 1960s Hong Kong action cinema to more recent films like Crouching Tiger, Hidden Dragon and The Matrix. Yuen promptly accepted the offer.  Yuen drew on seemingly outdated wuxia fighting styles like the Deadly Melody and Buddhist Palm.   He remarked that despite the comedic nature of the film, the shooting process was a serious matter due to the tight schedule. 
 wire work.  Centro Digital performed extensive tests on CGI scenes before filming started, and treatment of the preliminary shots began immediately afterwards. The CGI crew edited out wire effects and applied special effects in high resolution. Legendary martial arts mentioned in wuxia novels were depicted and exaggerated through CGI, but actual people were used for the final fight between Chows character and hundreds of axe-wielding gangsters.   After a final calibration of colour, data of the processed scenes was sent to the US for the production of the final version. A group of six people followed the production crew throughout the shooting. 

===Music===
  Raymond Wong and performed by the Hong Kong Chinese Orchestra.    The score imitates traditional Chinese music used in 1940s swordplay films.  One of Wongs works, Nothing Ventured, Nothing Gained, provides a stark contrast between the villainous Axe Gang and the peaceful neighbourhood of Pig Sty Alley, depicted by a Chinese folk song, Fishermans Song of the East China Sea.  Along with Wongs compositions and various traditional Chinese songs, classical compositions are featured in the score, including excerpts from Zigeunerweisen by Pablo de Sarasate and Sabre Dance by Aram Khachaturian.    The song, Zhiyao Weini Huo Yitian (只要為你活一天; Only Want to Live One Day for You), is sung in the background by Eva Huang at the end of the film. Written by Liu Chia-chang (劉家昌) in the 1970s, it tells of a girls memories of a loved one, and her desire to live for him again.  Kung Fu Hustle was nominated for Best Original Film Score at the 24th Hong Kong Film Awards. 

Asian and American versions of the soundtrack were released. The Asian version of the soundtrack was released on 17 December 2004 by Sony Music Entertainment and has 33 tracks.  The American version of the soundtrack was released on 29 March 2005 by Varèse Sarabande and has 19 tracks but has 14 tracks missing from the Asian release. 

The soundtrack for the trailer was mastered at Epiphany Music and Recording, Inc. in Santa Rosa, California.

==References to other works==
  the 1984 The Shining. 
  
 Buddhist Palm Louis Chas wuxia novels. For example, the landlord and landlady refer to themselves as Yang Guo and Xiaolongnü, the names of characters in Chas The Return of the Condor Heroes, when they met the Beast. 

 .|225px|right]] The Blues Brothers, wearing similar hats and sunglasses at all times. Kung Fu Hustle    When they are flattered by the Axe Gang advisor, one of them answers "Strictly speaking were just musicians", similar to a line by Elwood Blues. 
 The Untouchables.  
 the same, the 1943 film. 
 Neo and hundreds of Agent Smiths in The Matrix Reloaded.   The scene in which the Beast prompts an axe member to punch him harder is reminiscent of a similar scene in Raging Bull, with Robert De Niros character prompting Joe Pescis character. 
 Nine Yang Eighteen Dragon Nine Swords of Dugu"). The scene in which the landlady confronts Brother Sum in the back of his car is a homage to Bruce Lee in Way of the Dragon, where he cracks his knuckles and gives a quick upper nod to the mafia boss, telling him to back off. Kung Fu Hustle   

==Releases==
Kung Fu Hustle premiered at the 2004 Toronto International Film Festival.  It was later released across East Asia including China, Hong Kong and Malaysia in December 2004.  The film was first shown in the US at the Sundance Film Festival in January 2005,  and then opened in a general release on 22 April 2005 after being shown in Los Angeles and New York for two weeks. 

The North American DVD release was on 8 August 2005.  A Blu-ray Disc|Blu-ray version of the DVD was released on 12 December 2006 by Sony Pictures. A UMD version of the film was released for the PlayStation Portable.  The United States DVD releases was censored and cut in a number of scenes that featured a lot of blood or human excrement, a later release saw these edits removed.  

In the United Kingdom the standard DVD was released 24 October 2005, the same day a special edition was released with collect item which included playing cards, keyring, sweat band and an inflatable axe.   On 8 April 2007 Sony Pictures Home Entertainment release a Blu-ray version. 
 Portuguese title of the film is Kungfusão, which sounds like Kung Fu and Confusão (confusion).  In the same way as Kungfusão, the Italian and Spanish titles were Kung-fusion and Kung-fusión, puns of "confusion". {{cite web title = Official site of Kung-fusion publisher = Sony Pictures Releasing International url = accessdate = 2007-05-06 |archiveurl=http://web.archive.org/web/20060506211501/http://www.kung-fusion.it/ Hungarian title is A Pofonok Földje, meaning The Land of Punches.  

In Korea a Limited Collectors Edition DVD was released which included a leather wallet, Stephen Chows Palm Figure with his signature, a photo album and Special Kung Fus Booklet with a Certificate of authenticity. 

==Reception==
The film was generally well received by critics, earning the score of 90% fresh on Rotten Tomatoes based on a total of 166 reviews.    Hong Kong director and film critic Gabriel Wong praised the film for its black comedy, special effects and nostalgia, citing the return of many retired kung fu actors from the 1970s.  Film critic Roger Ebert description of the film ("like Jackie Chan and Buster Keaton meet Quentin Tarantino and Bugs Bunny"), was printed on the promotion posters for Kung Fu Hustle in the US.    Other critics described it as a comedic version of Crouching Tiger, Hidden Dragon.  Positive reviews generally gave credit to the elements of mo lei tau comedy present in the film.  A number of reviewers viewed it as a computer-enhanced Looney Tunes punch-up.   In a 2010 GQ interview, actor Bill Murray called Kung Fu Hustle "the supreme achievement of the modern age in terms of comedy". 

Much of the criticism for the film was directed at its lack of character development and a coherent plot. Las Vegas Weekly, for instance, criticised the film for not enough of a central protagonist and character depth.  Criticisms were also directed at the films cartoonish and childish humour.  Richard Roeper gave it a negative review, saying he had "never been a fan of that over the top slapstick stuff". 

===Box office===
Kung Fu Hustle opened in Hong Kong on 23 December 2004, and earned HK$4,990,000 on its opening day. It stayed at the top of the box office for the rest of 2004 and for much of early 2005, eventually grossing HK$61.27 million. Its box office tally made it the highest-grossing film in Hong Kong history,    until it was beaten by You Are the Apple of My Eye in 2011. 

Kung Fu Hustle began a limited two-week theatrical run in New York City and Los Angeles on 8 April 2005 before being widely released across North America on 22 April. In its first week of limited release in seven cinemas, it grossed US$269,225 (US$38,461 per screen).  When it was expanded to a wide release in 2,503 cinemas, the largest number of cinemas ever for a foreign language film, it made a modest US$6,749,572 (US$2,696 per screen), eventually grossing a total of US$17,108,591 in 129 days. In total, Kung Fu Hustle had a worldwide gross of US$101,104,669.  While not a blockbuster, Kung Fu Hustle managed to become the highest-grossing foreign language film in North America in 2005  and went on to gain a cult following on DVD.   

===Awards and nominations===
The film was nominated for sixteen   including an award for Best Director for Stephen Chow.  In the United States Kung Fu Hustle was well received by various film critic associations winning awards for Best Foreign Language Film
from Boston Society of Film Critics Awards|Boston, Chicago Film Critics Association Awards|Chicago, Las Vagas and Phoenix based critics.   it was later nominated for six Satellite Awards  and one MTV Movie Award for best fight scene.  In the United Kingdom at 59th British Academy Film Awards the film was nominated for a BAFTA. 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  Kung Fu Hustle was listed at 50th place on this list. 

{| class="collapsible collapsed" style="width:100%; border:1px solid #cedff2; background:#F5FAFF"
|-

! style="text-align:left;"| List of awards and nominations
|-
|  

{| class="wikitable" style="font-size:90% width=100%;"
|-
! style= width="28%"| Award / Film Festival Category
! style= width="36%"|Recipient(s) Result
|- Imagine Film Amsterdam Fantastic Film Festival  Stephen Chow
|
| 
|- BAFTA Awards    Best Film not in the English Language Stephen Chow  Bo-Chu Chui Jeffrey Lau
| 
|- Boston Society of Film Critics Awards  Best Foreign Language Film China/Hong Kong
|
| 
|- Broadcast Film Critics Association Awards    Best Foreign-Language Film
|
| 
|- Chicago Film Critics Association Awards  Best Foreign Language Film China/Hong Kong.
|
| 
|- Florida Film Critics Circle Awards  Best Foreign Film
|
| 
|- Golden Globe Award (USA)  Best Foreign Language FilmChina.
|
| 
|- Golden Horse Awards    Best Director Stephen Chow
| 
|- Best Film
|
| 
|- Best Make Up & Costume Design Shirley Chan
| 
|- Best Supporting Actress Qiu Yuen
| 
|- Best Visual Effect Frankie Chung  Don Ma Tam  Kai Kwan   Hung Franco
| 
|- Best Action Choreography
|Woo-ping Yuen
| 
|- Best Art Direction Oliver Wong
| 
|- Best Editing Angie Lam
| 
|- Best Sound Effects Steve Burgess  Steven Ticknor  Robert Mackenzie  Paul Pirola
| 
|- Best Supporting Actor Wah Yuen
| 
|- Golden Trailer Awards  Best Foreign
|(Winston Davis & Associates).
| 
|- Hong Kong Film Awards    Best Action Choreography
|Woo-ping Yuen
| 
|- Best Film Editing Angie Lam
| 
|- Best Picture
|
| 
|- Best Sound Effects Steven   Ticknor  Steve Burgess  Robert Mackenzie  Paul Pirola
| 
|- Best Supporting Actor Wah Yuen
| 
|- Best Visual Effects Frankie Chung Ma  Wing-On Tam  Kai-Kwun Hung  Lau-Leung
| 
|- Best Actor Stephen Chow
| 
|- Best Actress Qiu Yuen
| 
|- Best Art Direction Oliver Wong
| 
|- Best Cinematography
|Hang-Sang Poon
| 
|- Best Costume Design and Make Up Shirley Chan
| 
|- Best Director Stephen Chow
| 
|- Best New Artist Shengyi Huang
| 
|- Best Original Film Score
|Ying-Wah Wong
| 
|- Best Screenplay Stephen Chow  Kan-Cheung Tsang  KXin Huo  KMan Keung Chan
| 
|- Best Supporting Actor
|Kwok-Kwan Chan
| 
|- Hong Kong Film Critics Society Awards  Film of Merit
| 
| 
|- Hundred Flowers Awards  Best Supporting Actress Qiu Yuen
| 
|- Best Actor Stephen Chow
| 
|- Best Director Stephen Chow
| 
|- Best Film
|
| 
|- Best Newcomer Shengyi Huang
| 
|- Best Supporting Actor Wah Yuen
| 
|- Las Vegas Film Critics Society Awards  Best Foreign Film
|
| 
|- MTV Movie Awards    Best Fight
|
| 
|- Motion Picture Sound Editors (USA)  Best Sound Editing in Feature Film - Foreign Steve Burgess (supervising sound editor)  
Chris Goodes (sound editor) 
Vic Kaspar (sound editor)  
Jo Mion (sound editor)  
Andrew Neil (sound editor)  
Paul Pirola (sound design)  
Steven Ticknor (sound design)  
Mario Vaccaro (foley artist)  
| 
|- Online Film Critics Society Awards  Best Foreign Language Film
|
| 
|- Phoenix Film Critics Society Awards  Best Foreign Language Film Stephen Chow
| 
|- Satellite Awards    Outstanding Actress in a Supporting Role, Comedy or Musical Qiu Yuen
| 
|- Outstanding Cinematography
|Hang-Sang Poon
| 
|- Outstanding Film Editing Angie Lam
| 
|- Outstanding Motion Picture, Comedy or Musical
|
| 
|- Outstanding Sound (Mixing & Editing) Paul Pirola
| 
|- Outstanding Visual Effects Frankie Chung
| 
|- Shanghai Film Critics Awards  Top 10 Films
| 
| 
|- Southeastern Film Critics Association Awards  Best Foreign Language Film China/Hong Kong.
|
|   (Runner-up)
|}
 
|}

==Sequel==
In 2005, Chow announced that there would be a sequel to Kung Fu Hustle, although he had not settled on a female lead. "There will be a lot of new characters in the movie. Well need a lot of new actors. Its possible that well look for people abroad besides casting locals".     In January 2013 during an interview Chow admitted that plans for making Kung Fu Hustle 2 have been put on hold.  "I was indeed in the midst of making the movie, but it is currently put on hold in view of other incoming projects".  Production of Kung Fu Hustle 2 was delayed while Chow filmed the science fiction adventure film CJ7. As a result, Kung Fu Hustle 2 is slated for a 2014 release.    As of 2015, there has yet to be any confirmed news of the sequel and Stephen is slated to complete another movie, Mermaid, before completing this one.

==Games==

===Online and mobile games=== side scrolling game designed for mobile phones was later released in 2006 by developer Tracebit. 

===MMO=== massively multiplayer fighter game E3 were it received mixed reviews from critics with many comparing it to similar MMO games such as Guild Wars and Phantasy Star Online.   
 PC and PS3 was planned for late 2009  however as of July 2013 the game has not been released and is only available in Asia. 

==See also==
 
*Cinema of Hong Kong
*Cinema of China
* Chandni Chowk to China - A Bollywood film inspired by the same. 

==References==
 

==External links==
 
* 
* 
* 
*  at LoveHKFilm.com
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 