Desiderando Giulia
{{Infobox film
| name           = Desiderando Giulia
| image          = Desiderando Giulia 1986.jpg
| caption        = 
| director       = Andrea Barzini
| producer       = 
| writer         = 
| starring       = Serena Grandi Johan Leysen 
| music          = Antonio Sechi 
| cinematography = Mario Vulpiani
| editing        = 
| distributor    = 
| released       = 1986 
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
 1986 Italian erotic drama film directed by Andrea Barzini. It is loosely based on the 1898 novel Senilità by Italo Svevo.   

==Plot==

A writer named Emilio meets in a theater foyer of a sudden and mysterious girl, Giulia, and the two begin dating. He, who lives with his sister Amalia, bound to him with some morbidity, courting and she rejects him several times, but later she accepts him and finally disappears, leaving him in need. Emilio tries to help but he realizes that he can not help it. Giulia has a disordered life and he tries to change her life, he even beat her, but she can not change and his sister Amalia becomes isolated for the continued absence of his brother, and he is victim of a love who has been betrayed: when Amalia commits suicide, Emilio decides to leave Giulia.

==Cast==

* Serena Grandi: Giulia
* Johan Leysen: Emilio
* Valeria DObici: Amalia
* Sergio Rubini: Stefano
* Massimo Sarchielli
* Giuliana Calandra

==References==
 

==External links==
*  

 
 
 

 
 