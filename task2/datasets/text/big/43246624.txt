Lawful Larceny
 
{{Infobox film
| name           = Lawful Larceny
| image          = LawfulLarcenyFilmPoster.jpg
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Lowell Sherman   
| producer       = William LeBaron Henry Hobart (assoc.) 
| writer         = 
| screenplay     = Jane Murfin   
| story          = 
| based on       =   
| starring       = Bebe Daniels Kenneth Thomson Olive Tell Lowell Sherman
| narrator       = 
| music          = 
| cinematography = Roy Hunt 
| editing        = Marie Halvey  RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =    |ref2= }}
| runtime        = 67 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 melodramatic film, silent film Famous Players-Lasky Corporation

==Plot==
When Marion Corseys husband, Andrew, is conned out of a small fortune by Vivian Hepburn, she dedicates herself to recovering the money.  In order to do so, she hides her identity and insinuates herself into the social circle of Vivian, by becoming her secretary, and studies the tactics employed by the sexy con-artist. While employed by Vivian, Marion meets Guy Tarlow, Vivians love interest.  However, Guy seems to be interested Marion.

Taking advantage of Guys interest, Marion turns the tables on the con-artists and, using Vivians own strategy, she cons Guy out of all the funds which were taken from Andrew. When her reverse larceny is discovered, Vivian enlists the help of Judge Perry, who is romantically interested in Vivian, in an attempt to recover her ill-gotten gains. In the end, however, Marion is able to prove that Vivians gambling club is not run honestly, and that Vivian herself is both a cheater and a thief.  In light of the evidence, the Judge and Guy end their pursuit of Marion, and Vivian slinks away.  Although their marriage is damaged, and may be over, Marion and Andrew decide to stay together as friends, and see how things work out.

==Cast==
*Bebe Daniels as Marion Corsey Kenneth Thomson as Andrew Dorsey
*Lowell Sherman as Guy Tarlow
*Olive Tell as Vivian Hepburn
*Purnell B. Pratt as Judge Perry Lou Payne as Davis
*Bert Roach as French
*Maude Turner Gordon as Mrs. Davis
*Helene Millard as Mrs. French Charles Coleman as Butler

(Cast list as per AFI Database) 

==Notes== Famous Players-Lasky Corporation, it was distributed by Paramount Pictures.   
 Theatre Republic (which is still in existence, now known as the New Victory Theater) in 1922.  It was directed by Bertram Harrison, produced by Albert H. Woods|A.H. Woods, and starred Margaret Lawrence, Alan Dinehart, and Lowell Sherman. 

Olive Tell, cast in the less than sympathetic role of the sexy con-artist, was the wife of associate producer Henry Hobart.   
 public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

==References==
 

 
 
 
 
 
 