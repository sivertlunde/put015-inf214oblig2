China's Century of Humiliation
{{Infobox film
| name           = Chinas Century of Humiliation
| image          =
| caption        = Official poster Mitch Anderson
| producer       = Mitch Anderson
| writer         = Mitch Anderson
| starring       = Kenneth Pomeranz Wang Gungwu Yang Jianli Yang Rui Clayton Dube Richard Baum Yunxiang Yan
| music          = Guy-Roger Duvert
| cinematography = Jason J. Tomaric, Rick Curnutt
| editing        = Mitch Anderson
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
}} Mitch Anderson. Released in 2011, the documentary explores the tumultuous interaction between China and the European powers throughout the 19th century.   

==Subject matter==
The documentary begins by comparing Confucianism and Christianity and how they translated in political thought, resulting in a collectivist society in China and an individualistic one in Europe. Then the film explores why the industrial and scientific revolutions did not happen in China, but the West. The second part of the film, delves in the early encounters between the British Navy and the isolationist Qing Dynasty. The root causes of the two opium wars are explored next, exposing the greed of the western powers and the incompetence of the Chinese rulers. The film time line ends with the First Sino-Japanese War of 1894 providing a comprehensive view of the decay of the last Chinese dynasty.   

== References ==
 
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 

 