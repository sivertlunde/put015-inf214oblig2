Lust for a Vampire
 
 
{{Infobox film
| name           = Lust for a Vampire
| image          = Lustforvampire.jpg
| caption        =
| director       = Jimmy Sangster
| producer       = Michael Style, Harry Fine
| writer         = Tudor Gates  based on characters by Sheridan Le Fanu 
| starring       = Ralph Bates, Barbara Jefford, Suzanna Leigh Harry Robinson
| cinematography = David Muir
| editing        = Spencer Reeve
| distributor    = American International Pictures  (USA, theatrical) , MGM
| released       =  
| runtime        = 95 min / USA:91 min
| country        = United Kingdom & United States English
| budget         =
}} lesbian themes.

Production of Lust For a Vampire began not long after the release of The Vampire Lovers.
 camp and silly. Its most noted scene shows Yutte Stensgaard drenched in blood and partially covered by blood-soaked rags, although the filmed scene is not as explicit as that shown in a promotional still. 
 David Healy and popular radio DJ Mike Raven.

==Synopsis== vampiric family. As students in the school, inhabitants of the nearby village and those who suspect Mircalla is responsible start to die, suspicion turns toward the Karnsteins and their ominous castle.

==Cast==
* Yutte Stensgaard as Mircalla Herritzen/Carmilla Karnstein
* Michael Johnson as Richard Lestrange
* Ralph Bates as Giles Barton
* Barbara Jefford as Countess Herritzen
* Suzanna Leigh as Janet Playfair
* Helen Christie as Miss Simpson
* Pippa Steel as Susan Pelley David Healy as Raymond Pelley
* Harvey Hall as Inspector Heinrich
* Mike Raven as Count Karnstein Michael Brennan as Landlord
* Jack Melford as Bishop
* Christopher Cunningham as Coachman
* Judy Matheson as Amanda
* Christopher Neame as Hans
* Luan Peters as Trudi
* Nick Brimble as 1st Villager
* Kirsten Lindholm as Peasant girl
* Sue Longhurst as Schoolgirl

== Production == lesbian elements in the story. Carmilla, for example, in this film falls in love with a man. Ingrid Pitt was offered the lead but turned it down. Peter Cushing was supposed to have appeared in the film but bowed out to care for his sick wife. Cushing was replaced by Ralph Bates, who described Lust for a Vampire as "one of the worst films ever made".  
  Bates had earlier appeared in Taste the Blood of Dracula with Madeline Smith, who starred in the previous Karnstein film, The Vampire Lovers. The song "Strange Love" was recorded for the film by Tracy (English singer)|Tracy, a teen singer from Wembley, produced as a 45" by Bob Barratt.

== Critical reception ==

The Hammer Story: The Authorised History of Hammer Films 
panned the film, calling it a "cynical and depressing exercise...", noting that "...one can only imagine what  , says that  "there is much to recommend it. I think it was a very good script," Tudor Gates told me (Hallenbeck), "I think, in a way, it was the better of the first two", with Hallenbeck noting that Gothic atmosphere is "ably evoked".

==See also==
*Vampire film

== References ==
 

; Sources

*  }}

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 