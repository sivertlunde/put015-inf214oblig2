The Suspect (1998 film)
 
 
{{Infobox film
| name           = The Suspect
| image          = TheSuspect.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
 | traditional    = 極度重犯
 | simplified     = 极度重犯
 | pinyin         = Jí Dù Zhòng Fàn
 | jyutping       = Gik6 Dou2 Cung5 Faan2}}
| director       = Ringo Lam
| producer       = Wong Sing Lim Nam Yin
| writer         = Ringo Lam Lau Wing Kin
| starring       = Louis Koo Simon Yam Ray Lui Ada Choi Eric Moo Raymond Wong Andrew Worboys
| cinematography = Ross Clarkson Paul Yip
| editing        = Marco Mak
| studio         = Sil-Metropole Organisation Globe Perfect International Colour Business Entertainment
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$4,497,310
}} Hong Kong action film written and directed by Ringo Lam.

==Plot==
Twelve years ago, Don Lee (Louis Koo) was imprisoned due to murder and the fact he did not testify against the mastermind Dante (Simon Yam) and his friend Max (Julian Cheung), who was also involved of committing the crime. Twelve years later, Don is released from prison and decides not to repeat the same mistakes and make a fresh start. However, Don receives a sudden call from Dante and Max, whom force him to assassinate the candidate running for the president of the Philippines. Don rejects their request, but later discovers that the target have been assassinated. Don realizes he have been framed by Dante and begins to exile. Later, with the help of King Tso (Ray Lui), a legion soldier who was hired to take down the murderer, and Annie (Ada Choi), a female reporter, Don reveals the evidence of Dantes crime and cleared himself.

==Cast==
*Louis Koo as Don Lee
*Simon Yam as Dante
*Ray Lui as King Tso
*Ada Choi as Annie Chung
*Eric Moo as Policeman
*Philip Ko as Aquinos henchman
*Julian Cheung as Max
*Johnny Cheung as Gary
*Raven Choi
*Iris Chai
*Fok Wing Fu as Aquinos henchman
*Mike Cassey as TV news reporter
*Tang Cheung
*Leung Ka Hei as Ray / Ah Hei

==Reception==
===Critical===
  gave The Suspect a mixed review describing it as being "more competent than spectacular, which registers the film as an eventual disappointment." 

===Box office===
The film grossed HK$4,497,310 at the Hong Kong box office during its theatrical run from 16 July to 5 August 1998 in Hong Kong.

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 