Seconds (2014 film)
{{Infobox film
| name           = Seconds
| image          = 
| caption        = 
| director       = Aneesh Upasana
| producer       =  
| writer         = 
| starring       = Jayasurya
| music          = Gopi Sunder
| lyrics         = 
| cinematography = 
| editing        = 
| Production Controller  =
| production company = 
| distributor    = 
| country        = India
| released       = 
| language       = Malayalam budget          = gross           =
}}
 thriller film directed by Aneesh Upasana. The film stars Jayasurya, Aparna Nair, Vinay Forrt, Ajay Nataraj, Ambika Mohan, Riyaz Khan, Anusree Nair, Salim Kumar, Shankar Ramakrishnan and Vinayakan in prominent roles. Released on December 5, 2014. 
The film is told in a non linear format is a multi narrative that revolves around a murder that happens in the elevator of a plush city apartment. Four unrelated persons of which one was a ruffian were in the lift of which two were left seriously injured and one, dead. The police officer on investigation, Bimal Vaas then asks for the whereabouts of the persons who were in the lift and we are also given the glimpses of each of their lives on the day of the fatal incident.    

==Cast==
* Jayasurya as Veeramani
* Aparna Nair as Teena
* Vinay Forrt as Feroz
* Ajay Nataraj
* Ambika Mohan
* Riyaz Khan as Abi Thomas
* Salim Kumar as Jeevan
* Vinayakan as Thampi
*Indrans as Shivankutty
*Shankar Ramakrishnan as Mahadev
*Anusree 
*Santhakumari
*Kalabhavan Haneefa
*Meera as Sneha
*Anjali Upasana as Haseena

==Soundtrack==
{{Infobox album
| Name = Seconds
| Artist = Gopi Sunder
| Type = Soundtrack
| caption =
| Cover =
| Released =
| Recorded = 2014
| Genre = Film soundtrack
| Length =
| Language = Malayalam
| Label =
| Producer = Gopi Sunder
| Last album = The Dolphins (2014)
| This album = Cousins (2014 film) (2015)
| Next album =
}}

==References==
 

==External links==
*  

 
 
 