The R.M.
{{Infobox film
| name = The R.M.
| image =The R.M. poster.jpg
| director = Kurt Hale Dave Hunter John E. Will Swenson,  Britani Bateman,  Tracy Ann Evans,  Merrill Dodge,  Michael Birkeland,  Maren Ord   Gary Crowton
| distributor = Halestorm Entertainment
| released = 2003
| runtime = 101 min.
| language = English
| music = Cody Hale
| awards =
| budget = $500,000
| gross  = $1,111,615 
| tagline = Just when you thought it was safe to go back into the world . . .
}}
  LDS Missionary returned missionary. John E. Moyer and directed by Kurt Hale.

==Overview==
The RM is very LDS-centric and generally meant to appeal to an LDS audience.  Many non-LDS viewers may not understand the numerous references and satires of the LDS&mdash;and Utah&mdash;sub-culture.

==Plot==
Jared Phelps returns home from his mission in Wyoming to find a world very different from the one he left two years ago.
 Evanston South Mission.  The opening credits are shown by using instant photographs.  They also show Elder Phelps going from a greenie (new missionary), to district leader, to zone leader and finally to an assistant to the mission president.
 foreign exchange student from Tonga.  Jareds girlfriend, who promised to wait for him to return, is getting married in two weeks.  The jewelry store where he bought the ring wont take it back.  And his former employer who promised to give him a job upon his return has sold the business and started an Internet enterprise and the only job opening is to mine diamonds in South Africa.
 general conference, elders quorum president.  He has to seriously consider whether to lie and keep him and his friend out of jail, or to tell the truth and send himself and/or his friend to jail.

==Selected credits==

===Cast===
*Kirby Heyborne, Jared Phelps
*Britani Bateman, Kelly Powers
*Will Swenson, Kori Swenson
*Gary Crowton, Bishop Andrews
*Fred Derbyshire, Crazy grandpa
*Wally Joyner, Brother Jensen

===Crew=== director
* writing

==See also==
* The Singles Ward &mdash; 2002
* The Best Two Years &mdash; 2003
* The Home Teachers &mdash; 2004

==External links==
* 

 

 
 
 
 