Shiner (2000 film)
{{Infobox film
| name           = Shiner
| image          = Shiner theatrical poster.jpg
| image_size     = 
| caption        = Original theatrical poster
| director       = John Irvin
| producer       = Geoffrey Reeve Jim Reeve Barry Townsley
| writer         = Scott Cherry
| narrator       = 
| starring       = Michael Caine Martin Landau Frances Barber Frank Harper Andy Serkis Matthew Marsden Kenneth Cranham
| music          = Paul Grabowsky
| cinematography = Mike Molloy
| editing        = Ian Crafford
| distributor    = Miramax
| released       = 2000
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} 2000 released film written by Scott Cherry and directed by John Irvin and starred Michael Caine and Martin Landau. It was shot in London.

==Plot==
The story centers on Billy "Shiner" Simpson (Caine) a boxing promoter banned from legitimate fights until he finds a promising start in his son Eddie. The night of the fight sees Eddie killed, and Simpson suspects rival Frank Spedding (Landau). Billy seeks revenge, only to grow mad as his suspicions draw closer to home.

==Cast==
* Michael Caine as Billy "Shiner" Simpson
* Martin Landau as Frank Spedding 
* Frances Barber as Georgie 
* Frank Harper as Jeff "Stoney" Stone 
* Andy Serkis as Mel
* Matthew Marsden as Eddie "Golden Boy" Simpson
* Kenneth Cranham as Gibson "Gibbo"

==Reception==
Jane Crowther of the BBC said, "A Lear-like construction serves well to show the gradual unraveling of a ruthlessly ambitious man trading on familial loyalty and past glory." 

The score by Paul Grabowsky effectively creates a disturbing and sinister mood.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 