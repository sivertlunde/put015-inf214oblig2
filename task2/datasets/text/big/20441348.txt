Kismet (1920 film)
{{Infobox film
| name           = Kismet
| image          =GuidoDeiroKismetCoverSheet.jpg
| image_size     =200px
| caption        =sheet music cover with film scenes
| director       = Louis J. Gasnier
| producer       = 
| writer         = Charles E. Whittaker (scenario)
| based on       =  
| starring       = Otis Skinner
| music          =
| cinematography = Tony Gaudio Glen MacWilliams Joseph du Bray
| editing        =
| studio         = Waldorf Film Corporation
| distributor    = Film Booking Offices of America|Robertson-Cole
| released       =  
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}}

Kismet (1920 in film|1920) is a silent film version of the 1911 play Kismet (play)|Kismet by Edward Knoblock, starring Otis Skinner and Elinor Fair, and directed by Louis J. Gasnier.

Skinners daughter, author Cornelia Otis Skinner, plays a small role. This version was released by Film Booking Office of America|Robertson-Cole Distributing Company, and was released on VHS by Grapevine Video.  In New England the distribution of the film was handled by Joseph P. Kennedy who organized a successful premiere in Boston. 
 1930 talkie. lost but its Vitaphone soundtrack survives.

==Cast==
*Otis Skinner - Hajj the Beggar
*Rosemary Theby - Kut-al-Kulub
*Elinor Fair - Marsinah
*Marguerite Comont - Nargis
*Nicholas Dunaew - Nasir
*Herschell Mayall - Jawan
*Fred Lancaster - Zayd
*Leon Bary - Caliph Abdullah
*Sidney Smith - Jester
*Hamilton Revelle - Wazir Mansur Tom Kennedy - Kutayt
*Sam Kaufman - Amru
*Emmett King - Wazir Abu Bakr
*Fanny Ferrari - Gulnar
*Emily Seville - Kabirah

==References==
 

==Bibliography==
* Cari Beauchamp|Beauchamp, Cari. Joseph P. Kennedys Hollywood Years. Faber and Faber, 2009

==External links==
 
* 
* 

 
 

 
 
 
 
 
 


 