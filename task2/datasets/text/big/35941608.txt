Rose (2011 film)
{{Infobox film
| name = Róża
| image = 
| caption = 
| director = Wojciech Smarzowski
| producer = 
| writer = Michał Szczerbic
| starring = Agata Kulesza Marcin Dorociński
| music = Mikołaj Trzaska
| cinematography = 
| editing =
| distributor = 
| released = 
| runtime  = 94 minutes
| country = Poland
| language = Polish, German, Russian
| budget =  5,302,677 złoty  
| gross = 
}} Masurian  woman and an officer of the Armia Krajowa in postwar Masuria.

== Plot == former German East Prussia, which became part of Poland as a result of the Potsdam Agreement after World War II. He visits Róża, a widow of a German Wehrmacht soldier whose death Tadeusz had witnessed, to hand over her husband’s possessions. Róża invites Tadeusz to stay at her farm to protect her against marauders and the brutal rapes she had previously experienced in the lawless atmosphere of postwar Masuria. From this partnership of purpose, slowly respect and love arises - a "frowned-upon relationship" attracting the "unwelcome attention of the new Polish nationalists as well as the notorious Soviet NKVD".   

While Róża is regarded a German by the new Polish authorities, thus facing her expulsion of Germans from Poland after World War II|expulsion, Tadeusz wants her to declare her Polish nationality as many Masurians did in a "humiliating nationality verification procedure" 

As director Wojciech Smarzowski calls it, the Masurians "fell victim to two instances of renationalisation and were later destroyed".       

== Reception == Variety has called the movie "almost unbearably brutal yet hauntingly romantic" and commended "Genre-savvy helmer Smarzowskis gritty mise-en-scene augments the force of the narrative, putting into visual terms its themes of ill-fated love and a nation doomed by nationalism. What in other hands might have played as costume melodrama focused on the victimized title character here takes the perspective of the loner hero, as Smarzowski gives the pic the hallmarks of a latter-day Western."  

The Krakow Post reviewed the movie as  "from a moral point of view Róża is a Western crammed with violence but filmed without complacency. Smarzowski ironically referred to himself as the “third Cohen brother” in a recent interview, and there may be a kernel of truth in this, at least in his commitment to injecting a dose of realism into movie genres that have never been fully developed in Polish cinema". 

== Cast ==
* Agata Kulesza as Róża Kwiatkowska
* Marcin Dorociński as Tadeusz Mazur
* Edward Linde-Lubaszenko as pastor
* Szymon Bobrowski as Kazik

== Awards ==
*Grand Prix, Warsaw Film Festival 2011 

*Polish Film Awards: Best Film,  Best Directing, Best Actress, Best Supporting Actor, Best Script, Best Sound and the Audience Award. 
*Critics’ Award National Film Festival in Gdynia 2011 
*Special  Jury Prize of the 23rd Polish Film Festival in America for Artistic Excellence and Importance 

== External links ==
* 
*  at  

== References ==
 

 
 
 
 