There You Are!
{{Infobox film
| name           = There You Are!
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Edward Sedgwick
| producer       = 
| writer         = Tay Garnett Ralph Spence (titles)
| screenplay     = F. Hugh Herbert
| story          = 
| based on       =  
| narrator       = 
| starring       = Conrad Nagel Edith Roberts
| music          = 
| cinematography = Benjamin F. Reynolds
| editing        = Arthur Johns
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 mins.
| country        = United States Silent English English intertitles
| budget         = 
| gross          =
}}
 1926 American silent comedy film directed by Edward Sedgwick. Based on the play of the same name by F. Hugh Herbert, the film starred Conrad Nagel and Edith Roberts.  There You Are! is now considered Lost film|lost. 

==Plot==
George (Conrad Nagel) is a clerk who captures a bandit, and in return gets the boss daughter (Edith Roberts).

==Cast==
* Conrad Nagel - George Fenwick 
* Edith Roberts - Joan Randolph 
* George Fawcett - William Randolph 
* Gwen Lee - Anita Grant 
* Eddie Gribbon - Eddie Gibbs 
* Phillips Smalley - J. Bertram Peters 
* Gertrude Bennett - Mrs. Gibbs 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 