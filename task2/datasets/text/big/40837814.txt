I Grew Up in Princeton
{{Infobox film
| name           = I Grew Up In Princeton
| image          =
| caption        = 
| director       = Brad Mays
| producer       = Lorenda Starfelt
| writer         = 
| starring       = Lee Neuwirth, Sydney Neuwirth, Arnold Roth, Phil McPherson, Charles Roth, Rett Campbell, Elizabeth Carpenter, Susan Tenney, David Schankler, Jimmy Tarlau, Zachary Tumin
| music          = Jon Negus
| cinematography = Brad Mays, Rebecca Burr, Sam Freund
| editing        = Brad Mays
| distributor    =
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
I Grew Up in Princeton is an independent documentary film directed by Brad Mays, and produced by Lorenda Starfelt at LightSong Films in North Hollywood, California. {{cite journal
  | last = Persico
  | first = Joyce J.
  | authorlink =
  | coauthors =
  | title = Documentary explores life in Princeton during the late 1960s, early 1970s
  | volume =
  | issue =
  | pages =
  | publisher =
  | location =
  | date = October 6, 2013
  | url =http://www.nj.com/mercer/index.ssf/2013/10/documentary_explores_how_princeton_dealt_with_the_political_and_racial_upheaval_of_the_late_1960s_ea.html
  | issn =
  | doi =
  | id =
  | accessdate = }}  The film had its festival debut at the New Jersey International Film Festival on June 14, 2014   and was followed by another screening at the Philadelphia Independent Film Festival on June 28, 2014. 

==Coming-of-Age story==
The Princeton newspaper Town Topics describes I Grew Up in Princeton as a "deeply personal coming-of-age story that yields perspective on the role of perception in a town that was split racially, economically and sociologically",  is a portrayal of life in the venerable university town during the tumultuous period of the late sixties through the early seventies. Featuring interviews with over 60 current and former Princetonians, as well as archival footage, I Grew Up in Princeton exposes Princeton as a town with, according to Joyce J. Persico of the Trenton Times, "two realities. On the one hand, blacks were accepted in society; on the other, they were accepted as long as they stayed on their side of town." {{cite journal
  | last = Persico
  | first = Joyce J.
  | authorlink =
  | coauthors =
  | title = Documentary explores life in Princeton during the late 1960s, early 1970s
  | volume =
  | issue =
  | pages =
  | publisher =
  | location =
  | date = October 6, 2013
  | url =http://www.nj.com/mercer/index.ssf/2013/10/documentary_explores_how_princeton_dealt_with_the_political_and_racial_upheaval_of_the_late_1960s_ea.html
  | issn =
  | doi =
  | id =
  | accessdate = }}  Racial divisions are explored in considerable depth. Former Superintendent of the Princeton Regional School District, Dr. Philip McPherson, describes the disturbing backlash resulting from his support of the teaching of James Baldwins play Blues for Mr. Charlie in Princeton High School English classes. Ms. Persico describes a scene from the film in which Dr. McPherson discusses returning home from a particularly contentious meeting with a group of Teamsters to find "a racial epithet scrawled across his driveway." {{cite journal
  | last = Persico
  | first = Joyce J.
  | authorlink =
  | coauthors =
  | title = Documentary explores life in Princeton during the late 1960s, early 1970s
  | volume =
  | issue =
  | pages =
  | publisher =
  | location =
  | date = October 6, 2013
  | url =http://www.nj.com/mercer/index.ssf/2013/10/documentary_explores_how_princeton_dealt_with_the_political_and_racial_upheaval_of_the_late_1960s_ea.html
  | issn =
  | doi =
  | id =
  | accessdate = }} 

==On-Campus Vietnam War protest==
The numerous interviewees featured in I Grew Up in Princeton include cartoonist Arnold Roth, famed artist Nelson Shanks, author Zachary Tumin (formerly of Harvard’s John F. Kennedy School of Government), former Superintendent of Princeton Regional Schools Phil McPherson, and former director of the Institute for Defense Analyses (IDA) Lee Neuwirth, who speaks in considerable detail about the 1970 anti war demonstration which occurred on IDA grounds. According to writer Linda Arntzenius in an article for Princeton publication Town Topics, IDA was "thought to be in cahoots with the United States military war machine, plotting bombing routes in Cambodia."  Also featured in the on-camera discussions regarding Princeton student war protest are Jimmy Tarlau and David Schankler, both former members of Students For A Democratic Society (SDS), whose views of the IDA demonstrations clearly differ from Neuwirths. While the film sheds considerable light on IDA, as well as other long-standing controversies regarding the Princeton community, a final historical resolution proves elusive. 

==References==
 

==External links==
*  
*  
* http://www.towntopics.com/wordpress/2013/10/09/phs-grad-filmmaker-back-in-town-for-premier-of-princeton-documentary/
* http://www.trentonian.com/general-news/20130920/new-film-on-princeton-life-in-the-60s-and-70s-to-screen-in-october
* http://www.princetonmagazine.com/in-search-of-lost-time/

 
 
 
 
 