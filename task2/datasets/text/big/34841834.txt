Tony Arzenta
{{Infobox film
| name = Tony Arzenta
| image = Tony Arzenta.jpg
| caption =
| director = Duccio Tessari
| writer =
| starring = Alain Delon
| music = Gianni Ferrio
| cinematography = Silvano Ippoliti
| editing = Mario Morra
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget = gross = 866,746 admissions (France) 
}} 1973 Cinema Italian noir film directed by Duccio Tessari. The film had a good commercial success. 

== Plot ==
The hitman Tony Arzenta, after deciding to get out of the business, witnesses the killing of his wife and daughter. Afterwards, he takes revenge. 

== Cast ==
* Alain Delon: Tony Arzenta
* Richard Conte: Nick Gusto
* Carla Gravina: Sandra
* Marc Porel: Domenico Maggio
* Roger Hanin: Carré
* Nicoletta Machiavelli: Anna Arzenta
* Lino Troisi: Rocco Cutitta
* Silvano Tranquilli: Montani, the Interpol officer 
* Corrado Gaipa: Arzentas Father
* Umberto Orsini: Isnello, Gustos right-hand man
* Giancarlo Sbragia: Luca Dennino
* Erika Blanc: The prostitute
* Ettore Manni: Gesmundo, the sauna owner
* Loredana Nusciak: Gesmundos Lover
* Rosalba Neri: Cutittas Wife 
* Maria Pia Conte: Carrés Secretary
* Anton Diffring: Hans Grunwald
* Alberto Farnese: The Man who meets Carré in the nightclub

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 