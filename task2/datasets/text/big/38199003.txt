Doña Juana (film)
{{Infobox film
| name           = Doña Juana 
| image          =
| caption        =
| director       = Paul Czinner
| producer       = Paul Czinner   Elisabeth Bergner
| writer         = Tirso de Molina (play)   Béla Balázs   Paul Czinner
| starring       = Elisabeth Bergner   Walter Rilla   Hertha von Walther   Elisabeth Neumann-Viertel
| music          = Giuseppe Becce 
| cinematography =  Karl Freund   Robert Baberske   Adolf Schlasy
| editing        = 
| studio         = Elisabeth Bergner Films UFA
| released       = 1927
| runtime        = 
| country        = Germany
| awards         =
| language       = Silent   German intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama film directed by Paul Czinner and starring Elisabeth Bergner, Walter Rilla and Hertha von Walther. It was based on a Spanish play by Tirso de Molina. The adaptation was done by Béla Balázs, who later tried to have his name removed from the credits because he disliked the finished version of the film.

==Cast==
* Elisabeth Bergner as Doña Juana 
* Walter Rilla as Don Ramon 
* Hertha von Walther as Doña Ines 
* Elisabeth Neumann-Viertel as Ines Freundin Clara 
* Fritz Greiner as Ramons Freund Osorio 
* Hubert von Meyerinck as Dichter Don Alfonso 
* Wolfgang von Schwindt as Juanas Diener Francesco 
* Max Schreck as Juanas Vater 
* Lotte Stein as Juanas Erzieherin Duena 
* Max Wogritsch as Don Felipe de Mendoza 
* Rafael Calvo as Don Pedro de Padilla

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 