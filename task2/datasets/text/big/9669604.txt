Once More (film)
{{Infobox film
| name = Once More
| image = Once More VCD Cover.jpg
| director = S. A. Chandrasekhar
| screenplay = S. A. Chandrasekhar
| writer = Shanmugapriyan Vijay Simran Simran B. Saroja Devi
| producer = C. V. Rajendran
| editing = B. S. Vasu Saleem
| cinematography = Selva. R
| studio = Shree Ganesh Vision Deva
| released = 4 July 1997
| runtime = 170
| language = Tamil
| budget = 3.2 Crore
| gross =  17.4 Crore
}}
 Tamil romantic Vijay in Saroja Devi Simran play their love interests. The movie was a hit at box office.

==Plot==
Vijay (Vijay (actor)|Vijay) is a rich young business man, whose only one aim in life is to have fun, is the managing director of a big tea estate in Ooty and his company goes on a big loss because of his mismanagement. Vijay spends most of his time in partying and flirting with girls whenever he feels like. To manage the loss to his company, Vijay invites his father Rajasekar (who is in the United States) to come to India to sign some documents. As fate would have it, Vijays father dies in a plane crash on his way to India. His maternal uncle (Manivannan) comes up with the idea of having another elderly person to act as Vijays father for a while until the documents are signed. So they visit a senior home, where they meet Selvam (Sivaji Ganesan) and they request him to act as Vijays father. He agrees to it and starts to act as Vijays father.
 Saroja Devi) and they have been living separated for the past 30 years. He wants them to get united and he searches and finds the whereabouts of Shantha. Meanwhile, Kavitha (Simran (actress)|Simran) joins Vijays company and she talks to Vijay in his own style. Vijay is knocked out by Kavithas beauty and he falls in love with her. Then, their love gets into trouble because of Kavithas mother. Now, Vijay tries to unite Selvam and Shantha. In return Selvam tries to unite Vijay and Kavitha. Will Selvam and Shantha meet and get married Once More.

==Cast==
*Sivaji Ganesan as Selvam Vijay as Vijay Simran as Kavitha Saroja Devi as Shantha
*Manivannan as Vijays maternal uncle
*Anju Aravind as Anju
*Charle
*S. S. Chandran

==Production==
The director, S. A. Chandrasekhar, convinced Sivaji Ganesan to feature in the film after Sathya Sai Baba told him to continue acting for this role. 

==Critical reception==

Behindwoods gave the film a positive review and stated that the film was a commercial family entertainer. 

==Soundtrack==
{{Infobox album|  
  Name        = Once More
|  Type        = Soundtrack Deva
|  Cover       = Once More VCD Cover.jpg
|  Released    = 1997
|  Recorded    = 1997 Feature film soundtrack
|  Length      = 26.21
|  Label       = Pyramid Deva
|  Reviews     =
|  Last album  = Kaalamellam Kaathiruppen (1997)
|  This album  = Once More (1997)
|  Next album  = Nerrukku Ner (1997)
}} Deva  while lyrics were written by Vairamuthu and  Palani Bharathi.

==Track list==
{{tracklist
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 26:21
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Chinna Chinna Kaadhal 
| extra1          = Malaysia Vasudevan, Deva (music director)|Deva, Anuradha Sriram
| lyrics1          = Vairamuthu
| length1         = 05:46
| title2          = Malargale
| extra2          = Devan
| lyrics2         = 
| length2         = 05:21
| title3          = Ooty Malai Beauty
| extra3          =  Mano, Swarnalatha
| lyrics3         = Palani Bharathi 
| length3         = 05:01
| title4          = Oormila Oormila
| extra4          = Vijay (actor)|Vijay,Shoba Chandrasekhar
| lyrics4         = Vairamuthu
| length4         = 04:46
| title5          = Poove Poove Penpoove
| extra5          = S. N. Surendar, K. S. Chitra
| lyrics5         = Vairamuthu
| length5         = 05:07
}}

==References==
 

==External links==
*  

 
 
 
 
 
 