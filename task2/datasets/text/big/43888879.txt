Minnaminuginum Minnukettu
{{Infobox film
| name           = Minnaminuginum Minnukettu
| image          =
| image_size     =
| caption        =
| director       = Thulasidas
| producer       = Mukesh  R Mehta
| writer         = Thulasidas AK Sajan (dialogues) AK Santhosh (dialogues)
| screenplay     = AK Sajan AK Santhosh
| starring       = Jayaram Shobhana Thilakan Kaviyoor Ponnamma
| music          = S. P. Venkatesh
| cinematography = Utpal V Nayanar
| editing        = G Murali
| studio         = Soorya Cine Arts
| distributor    = Soorya Cine Arts
| released       =  
| country        = India Malayalam
}}
 1995 Cinema Indian Malayalam Malayalam film,  directed by  Thulasidas and produced by Mukesh  R Mehta. The film stars Jayaram, Shobhana, Thilakan and Kaviyoor Ponnamma in lead roles. The film had musical score by S. P. Venkatesh.      

==Cast==
 
*Jayaram
*Shobhana
*Thilakan
*Kaviyoor Ponnamma
*Heera Rajgopal as Pinky S. Menon
*Jagadish Sonia
*Chippy Chippy
*Prem Kumar
*Janardanan
*K. P. A. C. Lalitha
*Oduvil Unnikrishnan Mahesh
 

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Gireesh Puthenchery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kunungi kunungi || K. J. Yesudas, Chorus || Gireesh Puthenchery ||
|-
| 2 || Kunungikkunungi (f) || Sujatha Mohan || Gireesh Puthenchery ||
|-
| 3 || Manjil Pootha || MG Sreekumar, Swarnalatha || Gireesh Puthenchery ||
|-
| 4 || Thankathamburuvo || S Janaki || Gireesh Puthenchery ||
|}

==References==
 

==External links==
*  
*  

 
 
 


 