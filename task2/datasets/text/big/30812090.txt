Ladies' Man (1931 film)
{{Infobox film
| name           = Ladies Man
| image          =Poster - Ladies Man (1931) 02.jpg
| image_size     =200px
| caption        =Lobby card for film
| director       = Lothar Mendes
| producer       =
| writer         = Rupert Hughes (screen story) Herman Mankiewicz (screenwriter)
| narrator       =
| starring       = William Powell Kay Francis Carole Lombard
| music          =
| cinematography = Victor Milner
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English language|English}}
 American drama film directed by Lothar Mendes, starring William Powell and Kay Francis. It was released on May 9, 1931 by Paramount Pictures|Paramount.

==Cast==
*William Powell as Jamie Darricott
*Kay Francis as Norma Page
*Carole Lombard as Rachel Fendley
*Gilbert Emery as Horace Fendley
*Olive Tell as Mrs. Fendley

==External links==
 
* 
*  

 

 
 
 
 
 
 
 
 


 