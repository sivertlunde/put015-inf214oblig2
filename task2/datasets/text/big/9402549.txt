Kidnapping, Caucasian Style
{{Blacklisted-links|1=
*http://www.world-art.ru/cinema/cinema.php?id=4278
*:Triggered by  \bworld-art\.ru\b  on the global blacklist|bot=Cyberbot II|invisible=false}}
{{Infobox film
| name           = Kidnapping, Caucasian Style
| image          = Kidnapping, Caucasian Style.jpg
| image_size     = 190px
| border         = 
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Leonid Gaidai
| producer       = Mosfilm
| writer         = Yakov Kostyukovsky Moris Slobodskoy Leonid Gaidai
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Aleksandr Demyanenko Natalya Varley Yuri Nikulin Yevgeny Morgunov Georgy Vitsin Vladimir Etush Frunzik Mkrtchyan
| music          = Aleksandr Zatsepin
| cinematography = Konstantin Brovin
| editing        = 
| studio         = 
| distributor    = Mosfilm
| released       =  
| runtime        = 77 min
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}}
 Soviet comedy film dealing with a humorous plot revolving around bride kidnapping, an old tradition that used to exist in certain regions of the Northern Caucasus.
 Three Stooges. The film premiered in Moscow on April 1, 1967.

==Background==
As a result of the popularity of the earlier film, Operation Y, Yakov Kostyukovsky and Moris Slobodsky requested Mosfilm to support a new film about the character of Shurik. The screenplay for the new film was initially titled "Shurik in the mountains" and was divided into two parts. The first part, "Prisoner of the Caucasus", was about the student Nina who comes to visit her relatives in the Caucasus and is kidnapped by a local director named Okhokhov. The second part, "Snow Man and Others", was about a scientific expedition seeking the Yeti in a mountainous region, with the Coward, the Fool, and the Pro pretending to be the Yeti by way of hiding from the local militia. In the end, Shurik and Nina were supposed to expose the trio. As the process went forward, it was decided to focus on just the first part of the screenplay.

Yuri Nikulin and Evgeny Morgunov initially refused to be part of the film as they felt the screenplay was too unrealistic. In the end, Gaidai convinced both actors to reprise their roles by agreeing to make some adjustments to the screenplay. It also took a long time to cast the role of Nina, with more than 500 screen tests completed before Natalya Varley was selected for the role. Because she worked as a circus tight rope walker prior to acting, she had an easier time with the stunt work and physicality required in the role. On the other hand, her relative lack of acting experience made the dialogue scenes more challenging for her.

In the case of the character of Saakhov, there were disagreements between Gaidai and the actor playing Saakhov, Vladimir Etush. While Etush thought Saakhov should be played as an accomplished, intelligent man who takes himself seriously, Gaidai wanted more of an over-the-top performance.

During production, the screenplay was also altered by  , once a popular proto-graffiti) and, after a militia officer appears at the scene, the Fool quickly adds " дожественный фильм", which makes for "a feature film". After release, it became the biggest Soviet hit of 1967 and became known as one of the greatest Russian comedies of all time.

==Plot==
A kind, yet naïve, ethnography student named Shurik (Aleksandr Demyanenko|Demyanenko), known from earlier films as a student at the Polytechnic Institute, goes to the Caucasus to learn ancient customs and traditions practiced by the locals, including local "myths, legends, and toasts". At the start of the film, Shurik is making his way along a mountain road in the Caucasus on a donkey. He comes upon a truck driver named Edik whose truck refuses to start. The donkey gets stubborn and neither man is able to get his respective mode of transportation going.
{{external media
| topic     = 
| font-size = 
| align     = right
| clear     = 
| width     = 300px
| video1    =   ("Eсли б я был султан…") Trying to encourage Nina to have some breakfast, the trio, led by Pro (Nikulin) entertain her with a song about the pros and contras of having three wives at once. They get carried away a bit with their own performance, and Nina escapes.}} toasts unless he drank to each of them. He ends up becoming disorderly and carted off by the militsiya.

Meanwhile, Saakhov decides to marry Nina and strikes a deal with Dzhabrail to purchase the bride in return for 20 heads of sheep and an imported Finnish Rosenlew refrigerator. Rather than asking for Ninas agreement, which her uncle realizes would be impossible to get, they decide to kidnap her instead. The trio of the Coward, the Fool, and the Pro are hired to do the job, but find it difficult to get Nina alone because she has started to spend a lot of time with Shurik. At this point, Saakhov has the idea to unwittingly get Shurik in on it by telling him that the kidnapping of the bride is a local custom. Dzhabrail meets with Shurik in a restaurant and tells him this story, lying him that Nina has already agreed to marry Saakhov and that she wants to be kidnapped in order to comply with tradition. Shurik is devastated because he is in love with Nina, but thinking that this is what she wants, he agrees to help.

Nina has gone camping and spends a night in a sleeping bag. Shurik tells her an emotional good-bye and, misunderstanding him, she shrugs and also says good-bye. Shurik then zips her up in her sleeping bag and signals to the Coward, the Fool, and the Pro, who run over to grab the helpless Nina and transport her to Saakhovs dacha. Soon after, Shurik learns that the kidnapping was real and the story about it being a custom was a lie. Shurik immediately runs to the militsiya, but Saakhov (who Shurik does not realize is involved) is waiting for him outside. Saakhov explains to Shurik that if he says anything, the militsiya will arrest him as a co-conspirator and suggests they go straight to the local prosecutor instead. Shurik agrees, but Saakhov tricks him by leading him to a house where there is a party going on and getting him to drink, then calling doctors from the local psychiatric clinic and having Shurik committed.

Meanwhile, at Saakhovs dacha, the trio of kidnappers lock Nina in a room and try to cheer her up by bringing food and singing songs. Nina pretends to be interested, but then when the kidnappers are distracted, she tries to run away. She is stopped by her uncle and forced to return to her room, where she is locked up. Saakhov arrives with a bottle of wine and goes in to speak with Nina, but runs out moments later covered from head to toe in the wine. Deciding to give Nina some time to "think about it", Dzhabrail and Saakhov drive away from the dacha, leaving the trio of kidnappers in charge of Nina.

At the hospital, Shurik finally realizes that Saakhov is the one behind the kidnapping. Shurik is able to escape from the psychiatric ward and happens to run into Edik, the truck driver he had met at the beginning of the film. Together, they drive toward Saakhovs dacha. When they arrive, they have changed into doctors uniforms and convince the Coward, the Fool, and the Pro that they are doing emergency vaccinations against a dangerous plague that is affecting the area. Under this guise, they inject the trio with sedatives. While Edik is doing the injections, Shurik goes up to Ninas room. Still thinking that he was in on the kidnapping, she hits him over the head with a fruit plate, runs out of the room, jumps out of a first floor window, and steals one of the trucks.
  similar to the one pictured.     at the Lomakovka AutoMotoMuseum, Moscow. ]]
A car chase ensues in which the kidnappers chase Nina and Shurik and Edik chase the kidnappers. The kidnappers catch up with Nina, commandeer her vehicle, and tie her up, but at that moment the sedative begins to take effect and they all fall asleep. Shurik catches up with the truck right before it veers off the road and stops it. He begins to untie Nina, but she attacks him, still thinking that he is in on it with the kidnappers. To reveal his feelings for her, Shurik kisses Nina before he finishes untying her.

The action moves to Saakhovs apartment at night. He is alone. Suddenly, Nina, Shurik, and Edik appear holding weapons and dressed in masks, calling themselves the enforcers of the "law of the mountains". Saakhov does not recognize them and, scared to death, jumps out of the window. Edik shoots him with his rifle, which turns out to be loaded with nothing more than salt shot|salt. He hits him in the rump and, when Saakhov is brought up on charges in court the next day, he is unable to sit. The film ends with Shurik walking Nina to a bus and then following after her on his donkey.

==Cast==
*Aleksandr Demyanenko as Shurik
*Natalya Varley as Nina (voice of Nadezhda Rumyantseva)
*Vladimir Etush as comrade Saakhov
*Frunzik Mkrtchyan as Dzhabrail
*Ruslan Akhmetov as Edik
*Yuri Nikulin as Fool
*Georgy Vitsin as Coward
*Yevgeny Morgunov as Pro
*Donara Mcrtchan as Ninas aunt
*Mikhail Gloozskiy as the Hotel Manager
*Nina Grebeshkova as the Nurse
*Petr Repnin as the Doctor
*Noy Acaliani as the Hotel Employee with toast about bird
*Ammanuil Geller as the Shashlik Seller
*Georgy Svetlani as the Oldman by the beer stall
*Leonid Dovlatov as the Judge
*Georgy Ahundov as the Domino player

==Production==
Kidnapping, Caucasian Style followed the success of Gaidais previous film, Operation Y and Shuriks Other Adventures, which featured some of the same characters (Shurik, and the trio of the Fool, the Coward, and the Pro).    According to one of the writers, Yakov Kostyukovsky, Gaidai had decided that the trio had reached the height of their popularity and did not want to feature them again. Kostyukovsky and co-writer Slobodsky convinced Gaidai to film Kidnapping, Caucasian Style. 

==Censorship==
The censors of Goskino had decided to prevent the films release but Leonid Brezhnev, who was sworn in as the Soviet president less than a year before, saw the film and expressed his fondness for Gaidais work. Due to Brezhnevs appreciation, the censors reconsidered their initial decision and the movie was officially released. 

==Reception==
The film topped the Soviet box office in 1967 with 76.54 million viewers. 

==Soundtracks==
;"Pesenka o medvedyah" ("Song about bears")	
The song was recorded by Aida Vedishcheva (Vais), with Varley lip-synching during the performance of the song in the film. Varley accompanies her performance with a brief display of the "Twist (dance)|twist", conveying — in concert with the fast-paced melody — the light-hearted and optimistic theme of the song.

;"Yesli b ya byl sultan" ("If I were a sultan")
The song was performed by Nikulin, with Vitsyn and Morgunov featured in the chorus. It is a comical song about the polygamy practiced in Middle Eastern cultures, with Varley dancing in a style partly reminiscent of the Old Tbilisi dance theme during its performance.

==References==
;Notes
 

;Sources
 

==External links==
* 
* 
*  at official Mosfilm site with English subtitles
*  (Russian language review)

 

 
 
 
 
 
 
 