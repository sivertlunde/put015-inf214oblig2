Elizabeth: The Golden Age
 
 
{{Infobox film
| name           = Elizabeth: The Golden Age
| image          = Elizabeth golden poster.jpg
| image_size     =
| caption        = Promotional film poster
| director       = Shekhar Kapur Jonathan Cavendish William Nicholson Michael Hirst
| starring       = Cate Blanchett   Geoffrey Rush Clive Owen Rhys Ifans Jordi Mollà Abbie Cornish Samantha Morton Craig Armstrong
| cinematography = Remi Adefarasin
| editing        = Jill Bilcock Universal Pictures
| studio         = StudioCanal Working Title Films
| released       =  
| runtime        = 114 minutes
| country        = United Kingdom, Spain, France
| language       = English Spanish  French
| budget         = $50–60 million
| gross          = $74,237,563
}} Universal Pictures Queen Elizabeth William Nicholson Michael Hirst. Craig Armstrong.

It was filmed at Shepperton Studios and various locations around the United Kingdom with an estimated production budget of 50 to 60 million USD.  Guy Hendrix Dyas was the films production designer and co-visual effects supervisor and the costumes were created by Alexandra Byrne.

The film premiered on 9 September 2007 at the Toronto International Film Festival. It opened in wide release in the United States and Canada on 12 October 2007. It premiered in London on 23 October 2007 and was on general release from 2 November 2007 throughout the rest of the UK and Republic of Ireland. It opened in Australia and New Zealand on 15 November 2007.   

The film won an Academy Award for Best Costume Design and Blanchett received an Oscar nomination for Best Actress in a motion picture.

==Historical background== Elizabeth I, to marry him, but she would not agree. Phillip decides to take revenge and launches the Spanish Armada, with the blessing of the Pope, to attack England, Protestantism, and Elizabeth herself. Sir Walter Raleigh, whom the Queen loves, marries a ward of her court after he learns she is carrying his child. Elizabeth I has both of them arrested.

==Plot== Isabella the Queen of England in Elizabeths place. Meanwhile, Elizabeth I of England (Cate Blanchett) is being pressured to marry by her advisor, Francis Walsingham (Geoffrey Rush). She is ageing and, with no child, the throne will pass to her cousin, Mary, Queen of Scots (Samantha Morton). The Queen is presented with portraits of appropriate suitors, but Elizabeth refuses to marry, particularly to the Charles II, Archduke of Austria (Christian Brassington), who has become infatuated with the Queen. English explorer Walter Raleigh (Clive Owen) is presented at Elizabeths court, having returned from the New World, and offers her potatoes, tobacco, two Native Americans, and gold from Spanish ships that he claims were "unable to continue their journey". Elizabeth commands that the Native Americans be treated well, and refuses to accept the gold.
 Bess Throckmorton John Dee Jesuits in London conspire with Philip to assassinate Elizabeth and replace her with Mary, in what Philip calls "The English Enterprise", and which is known to history as the Babington Plot. From her imprisonment, Mary sends secret correspondence to the Jesuits, who recruit Anthony Babington (Eddie Redmayne) to assassinate Elizabeth. Walsingham continues to warn Elizabeth of Spains rising power and of the Catholic plots against her. However she, unlike her predecessor and half sister Mary I of England, refuses to force her people to share her beliefs. Even so, those conspiring against Elizabeth are being hunted and murdered, including Besss cousin, whom Bess had failed to protect.

After learning of her cousins torture and death at Walsinghams hands, Bess turns to Raleigh for comfort. The barely hidden closeness of Bess and Raleigh causes tension between them, testing her desire to keep him in England and increasing his desire to go back to the New World. Walsinghams brother, a Papist, knows of the plot against Elizabeth. It is revealed that Walsingham had known of the plot all along, intercepting letters, and his brother is jailed. He reveals the plot to Elizabeth, who angrily confronts the Spanish diplomats. The Spanish ambassador feigns ignorance and accuses Elizabeth of receiving Spanish gold from pirates and insinuating a sexual relationship with Raleigh. A sword fight nearly ensues between the queens male escorts and the Spanish contingent. She throws the Spaniards out of court. Meanwhile, Philip is cutting the forests of Spain to build the Spanish Armada to invade England. Mary writes letters condoning the plot. Babington storms into a cathedral where Elizabeth is praying and points a gun at her. Elizabeth opens her arms, seemingly fearless. He pulls the trigger, and the gun fires. At first Walsingham is unable to discern why the gun was harmless, though it is later revealed by the traitor in the torture chamber that there was no bullet in the gun.

Elizabeth learns of Marys involvement, and Walsingham insists she be executed to quell any possible revolt. Elizabeth is reluctant, but nevertheless agrees. Mary is tried for high treason. She is beheaded, ascending the block in a blood-red dress, red being the Catholic liturgical color for martyrs. Walsingham sees that this was part of the Jesuits plan all along. Philip had never intended Mary to become queen, but since the Pope and other Catholic leaders regarded Mary as the true Queen of England, Philip uses Marys death to obtain papal approval for war. The "murder" of the last legitimate Catholic in the line of succession gives Philip the pretext he needs to invade England and place his daughter on the throne as a puppet monarch. In England, Raleigh asks to leave for the New World, which Elizabeth forbids, instead knighting him and making him Captain of the Royal Guard. Bess discovers she is pregnant with Raleighs child, and after telling him the news, she pleads with him to leave. He chooses not to, and the couple marry in secret. At the same time, Elizabeth awakes during a dream as the wedding is taking place. She confronts Bess a few weeks later, who confesses that she is indeed pregnant with Raleighs child, and that Raleigh is her husband.
 major storm blows the Armada toward the beaches, endangering its formation and ships. The ships of the Armada drop anchor, and the Armada becomes a sitting duck for English fire ships. Elizabeth, back at her coastal headquarters, walks out to the cliffs and watches the Spanish Armada sink in flames. Elizabeth visits Walsingham on his deathbed, telling her old friend to rest. She then visits Raleigh and Bess and blesses their child. Elizabeth seemingly triumphs personally through her ordeal, again resigned to her role as the Virgin Queen and mother to the English people.

==Cast==
*Cate Blanchett as Elizabeth I of England
*Geoffrey Rush as Francis Walsingham
*Clive Owen as Sir Walter Raleigh Bess Throckmorton
*Samantha Morton as Mary, Queen of Scots
*Jordi Mollà as Philip II of Spain
*Susan Lynch as Annette Fleming
*Rhys Ifans as Robert Reston
*Eddie Redmayne as Anthony Babington
*Tom Hollander as Amias Paulet
*David Threlfall as John Dee
*Adam Godley as William Walsingham
*Laurence Fox as Sir Christopher Hatton William Houston as Guerau de Espés
*Christian Brassington as Charles II, Archduke of Austria
*John Shrapnel as Charles Howard, 1st Earl of Nottingham
*Kelly Hunter as Ursula Walsingham

==Dramatic licence==
 
This representation of a historical period is heavily fictionalised for the purposes of entertainment. Its lead Cate Blanchett has been reported as saying: "Its terrifying that we are growing up with this very illiterate bunch of children, who are somehow being taught that film is fact, when in fact its invention".  Some of the simpler fictions are: Sir Walter Sir Francis Drake and other key leaders are not so credited.  The regional speech used by Clive Owen, is also historically incorrect.  Raleigh was a Devon man, and from various sources spoke broad Devonian until his dying days. Meanwhile, Clive Owen plays him with a middle class London accent, that is RP, which did not even exist in the 16th C.  Presumably artistic license for the US/Canadian market, where it is probable a Devon accent would not be recognized as an English accent. However, a Devon accent would probably have been the significant seafaring speech of the time in England considering the influence of Devon on exploration to the New World.
* Robert Dudley, 1st Earl of Leicester, was lieutenant general at the Armada crisis. In the film, he is not present at the Tilbury camp, his role being given to Raleigh.
* Charles Howard, 1st Earl of Nottingham, says, "Were losing too many ships." In reality, not a single English ship was lost during the battle. Dr John Sir William Cecil, is omitted from the film altogether. the previous film, the persona of Reston was created to replace him. long drop, rather than the actual, and more gruesome method of hanging, drawing and quartering. Erik of Sweden abandoned his proposals to marry Elizabeth after his trip to England was interrupted by the death of his father in 1560, when Elizabeth was twenty-seven. In fact, by 1568, Erik had been deposed from the Swedish throne and died in captivity in 1577. Infanta Isabel of Spain is portrayed as a child. In reality, she was twenty-one by this time.
* The lady-in-waiting, Bess Throckmorton, in fact became pregnant with Walter Raleighs child in the summer of 1591, three years after the defeat of the Armada, not immediately before.
* Mary, Queen of Scots, is depicted as having a Scottish accent when she would surely have spoken with a French accent,  having been raised at the French court from the age of five and not returning to Scotland until she was a young woman.
* The film shows Spanish envoys and other members of court wearing swords during their audiences with Elizabeth. Owing to threats of assassination, only members of the Royal Guard were permitted to carry weapons near Elizabeth while she was in court.
* The execution of Mary, Queen of Scots, is portrayed as having happened very swiftly after her arrest, while she was still a young woman. In fact, Mary was held in custody at various places for 19 years before her execution in 1587, at the age of 44.
* The film depicts the battle between the two forces as consisting of broadsides from the ships of both fleets. In fact, while the English ships were able to fire multiple times during the course of a day, the heavy Spanish guns were so difficult to reload that they were frequently only fired once. Broadsides would accompany later developments in ship design in the first half of the 17th century, the first major actions involving such technology and tactics for the English being the Anglo-Dutch Wars.
• During the film, Elizabeth spoke German to one of her suitors, Charles II, Archduke of Austria and in reality, there is no evidence that Elizabeth was taught German or even spoke German.

• Regarding the appearances of Elizabeth I and Bess Throckmorton,Elizabeth had blue eyes when in reality, Elizabeth had hazel eyes. Whereas Throckmorton, in her portraits show that she had brown hair but in the film, she had blonde hair.

==Critical reception==
Although Cate Blanchetts performance was highly praised, the film received generally mixed to negative reviews from US critics. As of 24 November 2007 on the review aggregator Rotten Tomatoes, 34% of critics gave the film positive reviews, based on 145 reviews.  On Metacritic, the film had an average score of 45 out of 100, based on 32 reviews. 

Peter Bradshaw of   romantic novel". 

Roger Ebert gave the film 2½ stars out of 4, saying there are scenes where the costumes are so sumptuous, the sets so vast, the music so insistent, that we lose sight of the humans behind the dazzle of the production. Ebert did, however, praise many of the actors performances, particularly that of Cate Blanchett as Queen Elizabeth I. He said That Blanchett could appear in the same Toronto Film Festival playing Elizabeth and Bob Dylan, both splendidly, is a wonder of acting.  Blanchett portrayed Bob Dylan in the film Im Not There and was nominated for an Academy Award for her roles in both movies.

Colin Covert of the Minneapolis Star Tribune gave the film 3 stars out of 4, writing ... as a pseudo-historical fable, a romantic triangle and a blood-and-thunder melodrama, the film cant be faulted and This isnt historical fabrication, its mutilation. But for all its lapses, this is probably the liveliest, most vibrant Elizabethan production since Baz Luhrmanns Romeo + Juliet.  while Wesley Morris of The Boston Globe said, "Historians might demand a little more history from Elizabeth: The Golden Age. But soap opera loyalists could hardly ask for more soap." 
 Whig tradition film of that same play had appeared in 1989 and, although grittier in tone, portrayed the English and their king just as heroically as the Olivier version.

===Claims of anti-Catholicism=== Reformation and polarised European jingoistic in his review.

In the US the  , is a crescendo of church-bashing imagery: rosaries floating amid burning flotsam, inverted crucifixes sinking to the bottom of the ocean, the rows of ominous berobed clerics slinking away in defeat. Pound for pound, minute for minute, Elizabeth: The Golden Age could possibly contain more sustained church-bashing than any other film I can think of". Greydanus asked: "How is it possible that this orgy of anti-Catholicism has been all but ignored by most critics?" 
 Star Tribune Lafayette Journal & Courier agreed that anti-Catholicism was one of the films "sore points". 

Monsignor Mark Langham, Administrator of Westminster Cathedral, was criticised by some Roman Catholics for allowing scenes to be shot there; although praising the film as a must see, he suggested that it does appear to perpetuate the myth of “killer priests”.  
 atheists and apocalyptic Christians". Western identity in the face of the Islamic threat, presumed or real? 
 church in Philip had said that they were going to turn the whole world into a very pure form of Catholicism. So its not anti-Catholic. Its anti an interpretation of the word of God that is singular, as against what Elizabeth I of England|Elizabeths was, which was to look upon her faith as concomitant.   The fact is that the Pope ordered her execution; he said that anybody who executes or assassinates Elizabeth would find a beautiful place in the kingdom of heaven. Where else have you heard these words about Salman Khan or Salman Rushdie? Thats why I made this film, so this idea of a rift between Catholicism and Protestants does not arise. My interpretation of Elizabeth is an interpretation of greater tolerance   Philip, which is absolutely true. Its completely true that she had this kind of feminine energy. Its a conflict between Philip, who had no ability to encompass diversity or contradiction, and Elizabeth who had the feminine ability to do that. 

Kapur extended this   against   to say, "Just kill them all", and she was constantly saying no. She was constantly on the side of tolerance. So you interpret history to tell the story that is relevant to us now. 

==Filming locations==
* Baddesley Clinton, Warwickshire, England, UK (Raleighs house exteriors)

* Brean Down, Weston-super-Mare, Somerset, England, UK (Queen Elizabeth addresses her troops)

* Burghley House, Stamford, Lincolnshire, England, UK (John Dees house exteriors/London alley/Paris street scene)

* Burnham-on-Sea, Somerset, England, UK

* Dorney Court, Dorney, Buckinghamshire, England, UK (Raleighs house/Walsinghams house/chapel interiors)

* Doune Castle, Doune, Stirling, Scotland, UK

* Eilean Donan Castle, Kyle of Lochalsh, Highland, Scotland, UK

* Ely Cathedral, Ely, Cambridgeshire, England, UK (Whitehall Palace interior)
 Chartley Hall, also the interior of Walsinghams house)

* Leeds Castle, Kent, England, UK (Chartley Hall/Whitehall exteriors)

* Petworth House, Petworth, West Sussex, England, UK (Windsor Great Park)

* Shepperton Studios, Shepperton, Surrey, England, UK

* St. Bartholomews Hospital, London, England, UK (Fortheringay Castle/Chartley Hall interiors)

* St Johns College, Cambridge, Cambridge, Cambridgeshire, England, UK (Whitehall Palace exteriors/Thames scenes)

* Wells Cathedral, Wells, Somerset, England, UK (Whitehall Palace interiors)

* Westminster Cathedral, Westminster, London, England, UK (Escorial Palace/Lisbon Cathedral interiors)

* Winchester Cathedral, Winchester, Hampshire, England, UK (St. Pauls Cathedral/The Chapel Royal interiors/gallows scene)
 

==Soundtrack==
 

==Home media==
The film was released on Region 1 on DVD and HD DVD 5 February 2008. It was released on Blu-ray in 2009 and bundled with Elizabeth (film)|Elizabeth.

==Awards and nominations== Best Costume Design .    Cate Blanchett was also nominated for an Academy Award for Best Actress for her performance in the film, becoming the first female actor to receive another Academy Award nomination for the reprisal of the same role. Cate Blanchett was nominated for a Golden Globe Award for Best Actress in a Motion Picture Drama for her performance in the film,    and the Critics Choice Award for Best Actress in a leading role, she was also nominated for a SAG Award. The film won two Satellite Awards for Best Production Design for Guy Hendrix Dyas and Best Costume Design for Alexandra Byrne. Guy Hendrix Dyas received a nomination from the Art Directors Guild for Best Production Design in a Period Film, and Alexandra Byrne a nomination from the Costume Designers Guild for Best Costume in a Period Film. The film was also nominated for four BAFTA awards including Actress in a Leading Role, Best Production Design, Best Costume Design and Best Makeup.

At the 11th Pyongyang International Film Festival held on September 2008, one of the awards for special screening were conferred upon the film.

==Box office performance==
Elizabeth: The Golden Age grossed $6.1 million in 2,001 theatres during its opening weekend in the United States and Canada, ranking #6 at the box office.  In the United Kingdom and Republic of Ireland the film entered at No. 4 and earned £1.3 million ($2.7 million) on its opening weekend.    the worldwide total was $74.2 million, including $16.4 million in the US and Canada and $57.8 million elsewhere. 

In 1998, the preceding film, Elizabeth (film)|Elizabeth, opened in 9 theatres and grossed $275,131.    Its widest release in the United States and Canada was in 624 theatres.,  and its largest weekend gross throughout its run in theatres was $3.4 million in 516 theatres,  ranking No. 9 at the box office.  The 1998 film Elizabeth (film)|Elizabeth went on to gross $30 million in the United States and Canada, and a total of $82.1 million worldwide. 

==See also==
*Cultural depictions of Philip II of Spain
*Spanish Armada
*Black Legend

==References==

 

==External links==
 
*  
*  
*  
*  
*  
*  
*   at Working Title Films
*   at ukfilmtourism.com
*  
*   at stv.tv

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 