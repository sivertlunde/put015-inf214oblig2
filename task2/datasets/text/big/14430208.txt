Heaven with a Gun
{{Infobox film
| name           = Heaven with a Gun
| alt            = 
| image          = Heaven with a Gun FilmPoster.jpeg
| caption        = Theatrical release poster
| director       = Lee H. Katzin Frank King Maurice King
| screenplay     = Richard Carr
| narrator       = John Anderson
| music          = Johnny Mandel
| cinematography = Fred J. Koenekamp
| editing        = Dann Cahn
| studio         = King Brothers Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
Heaven with a Gun is a 1969 American western film starring Glenn Ford. 

==Plot==
Preacher Jim Killian (Ford) arrives in a town divided between cattlemen and sheep herders. Killian is not just any preacher. He is a former gunslinger who has set upon a different path.

Leelopa (Barbara Hershey), an American Indian girl who looks up to Killian, becomes a target of opportunity for one of the cowhands, Coke Beck (David Carradine). (The actors became real-life wife and husband three years after the movies release.) Coke happens to be the son of cattle rancher Asa Beck, and when Coke is stabbed to death through the neck, the cattlemen blame Killian.

Madge McCloud (Carolyn Jones), the whiskey-drinking madam of the towns saloon and brothel, acts as Killians conscience. After a gunslinger working for the cattlemen tries to kill Killian and four cowhands burn the church, Killian straps on his gun and prepares to act alone.

Madge tells him that he must make a decision to be either a gunman or a preacher — he must choose between Heaven and Hell, else he risks the trust of the community. She tells him that trying to be both is a worse sort of Hell. But there is also a pending showdown between the cattlemen and the sheepherders over water rights, and somehow Killian must be in the middle of it, whether armed or not.

==Cast==
* Glenn Ford as Jim Killian
* Carolyn Jones as Madge McCloud
* Barbara Hershey as Leloopa John Anderson as Asa Beck
* David Carradine as Coke Beck
* J. D. Cannon as Mace
* Noah Beery, Jr. as Garvey William Bryant as Bart Paterson
* Ed Bakey as Scotty Andrews
* Barbara Babcock as Mrs. Andrews
* Angelique Pettyjohn as Emily the saloon girl

==Reception==

===Critical response===
The New York Times film critic, Howard Thompson, gave the film a positive review, writing, "The typical dour restraint of Glenn Ford, as an exconvict turned pistol-packing parson, is the most steadying ingredient of Heaven With a Gun, a plodding, vest-pocket Western that opened yesterday at neighborhood theaters. As a veteran of many a cattlemen-versus-sheepmen exercise, Mr. Ford plays it cool and, of course, leathery." 

More recently, film critic Dennis Schwartz gave the film a negative review, writing, "...  and writer Richard Carr load the genre pic with cliches and violent sequences. The unpleasant Western features a lynching, torture with shears, a rape, arson, a street brawl and your usual saloon gun fights. The numerous cliches include a world-weary gunfighter wanting to reform and to save the world, your typical western fight between cattlemen and sheepherders, an aging saloon keeper and whorehouse madam with a heart of gold (Carolyn Jones) longing for her unavailable old gunfighter friend and a pretty half-caste Indian (Barbara Hershey) finding it difficult to understand the white world. It preaches an awkward social conscience message that peace can be found without guns. The trouble is the pic is clumsily executed and is leaden, so everything seems absurd and hardly believable." 

==References==
 

==External links==
*  
*  
*  
*   informational site at Films in Review
*  

 

 
 
 
 
 
 
 
 
 
 
 