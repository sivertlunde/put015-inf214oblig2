Stand and Deliver
 
{{Infobox film
| name           = Stand and Deliver
| image          = stand_and_deliver.jpg
| caption        = Theatrical release poster
| director       = Ramón Menéndez
| producer       = Tom Musca
| writer         = Ramón Menéndez Tom Musca
| starring       = Edward James Olmos Lou Diamond Phillips Rosanna DeSoto Andy García
| music          = Craig Safan Tom Richmond
| editing        = Nancy Richardson
| distributor    = Warner Bros.
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $13,994,920
}}
 Best Actor at the 61st Academy Awards.  The film was added to the National Film Registry of the Library of Congress in 2011.

==Plot== James A. Eastern Los Angeles and seeks to change the school culture to help the students excel in academics. He soon realizes the untapped potential of his class and sets a goal of having the students taking AP Calculus by their senior year. The students begin taking summer classes in advanced mathematics with Escalante having to withstand the cynicism of other faculty, who feel the students are not capable enough. As the students struggle with the lower expectations they face in society, Escalante helps them overcome the adversity and pass the AP Calculus exams. To his dismay, the Educational Testing Service questions the success of the students, insisting there is too much overlap in their errors and suggests the students cheated. Escalante defends his students, feeling that the allegations are based more on racial and economic perceptions. He offers to have the students retake the test months later and the students succeed in passing the test again despite only having a day to prepare, dispelling the concerns of cheating.

==Cast==
*Edward James Olmos as Jaime Escalante
*Lou Diamond Phillips as Angel Guzman
*Rosanna DeSoto as Fabiola Escalante
*Andy García as Ramirez
*Ingrid Oliu as Lupe 
*Will Gotay as Pancho

==Historical accuracy==
Ten of the students agreed to sign waivers so that the College Board could show  , their exam papers. Mathews found that nine of the ten had made "identical silly mistakes" on free-response question Number 6. Mathews heard from two of the students that there had been passed around a piece of paper with that flawed solution during the exam.    Twelve students (including the nine with the identical mistakes) retook the exam, and most of them got 4s and 5s on the 5-point exam. In 1987, 27 percent of all Mexican Americans who scored 3 or higher on the calculus AP exam were students at Garfield High. 

Escalante actually first began teaching at Garfield High School in 1974 and taught his first AP Calculus course in 1978 with a group of 14 students. Only five students remained in the course at the end of the year, and, of the five, only two passed the AP Calculus exam. 

After having viewed the film, Escalante praised it, saying it was Ninety per cent truth, ten per cent drama.

==Legacy==
In December 2011, Stand and Deliver was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry.    The Registry said the film was "one of the most popular of a new wave of narrative feature films produced in the 1980s by Latino filmmakers" and that it "celebrates in a direct, approachable, and impactful way, values of self-betterment through hard work and power through knowledge." 

==In popular culture==
A part of the plot and Escalante is parodied (a Latino-American teacher named Julio Estudiante who worked with inner city students to choose math over inner-city gang violence) in the The Simpsons|Simpsons episode "Special Edna".

The episode of South Park entitled "Eek, a Penis!" borrows heavily from the plot of Stand and Deliver, with Cartman assuming a similar role to that played by Edward James Olmos, although where in the film, the students were falsely accused of cheating, in the episode, the students actually did cheat and got away with it.      

In one episode of the seventh season of How I Met Your Mother, entitled "Field Trip", Ted takes his students on a field trip to teach them about how great being an architect is, and Barney reminds him that he cant "Stand and Deliver" his students. At the end of the episode, the movie is mentioned again when Barney says he saw it on television, and they argue about whether the actors name is Jacob James Olmos or Edward James Olmos.

In a March 2013 episode of the sketch comedy series Portlandia (TV series)|Portlandia, an Escalante-like teacher (played by Fred Armisen) is shown teaching middle-class college students, and being in turn "inspired" by them to give up teaching in order to become a social media marketing professional. 

American senator Rand Paul (R-KY) was accused of plagiarizing near-verbatim portions of the plot summary from the Wikipedia article on Stand and Deliver in two speeches on immigration.      

==See also==
* 1988 in film
* AFIs 100 Years...100 Cheers|AFIs 100 Years... 100 Cheers
* List of American films of 1988
 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 