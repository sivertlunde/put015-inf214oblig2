The Queen's Secretary
{{Infobox film
| name           = The Queens Secretary
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = Oskar Messter
| writer         =  Robert Wiene   
| narrator       = 
| starring       = Käthe Dorsch   Ressel Orla   Margarete Kupfer
| music          =  
| editing        = 
| cinematography = 
| studio         = Messter Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
}} German silent silent comedy film directed by Robert Wiene and starring Käthe Dorsch, Ressel Orla and Margarete Kupfer. A young Queen secretly marries the commander of her bodyguard, but things are complicated when his ex-lover arrives with a touring opera company.  The film was widely praised for its direction, acting and cinematography. 

==Cast==
* Käthe Dorsch   
* Ressel Orla   
* Margarete Kupfer   
*  Alexander Antalffy   
* Guido Herzfeld   
* Heinrich Schroth 

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 