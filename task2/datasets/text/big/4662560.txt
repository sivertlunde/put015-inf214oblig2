The Maniac
 
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Maniac
| image_size     = 
| image	=	The Maniac FilmPoster.jpeg
| caption        = 
| director       = Michael Carreras 
| producer       = Jimmy Sangster
| writer         = Jimmy Sangster
| narrator       = 
| starring       = Kerwin Mathews  Nadia Gray  Liliane Brousse  Donald Houston
| music          = Stanley Black
| cinematography = Wilkie Cooper
| editing        = Tom Simpson
| studio         = Hammer Film Productions
| distributor    = Columbia Pictures Corporation
| released       = 20 May 1963
| runtime        = 86 minutes
| country        = UK
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Maniac is a British psychological thriller, which was directed by Michael Carreras and stars Kerwin Mathews, Nadia Gray and Donald Houston.  

==Plot==
The story tells of vacationing American artist Jeff Farrell who becomes romantically involved with an older woman named Eve Beynat, in southern France, while harboring some attraction for her teenage stepdaughter Annette. Eves husband/Annettes father Georges is in an asylum for, four years ago, using a blowtorch to kill a man who had raped Annette. Believing it will help make Eve his for life, Jeff agrees to assist her in springing Georges from the asylum. Of course, Eve has a completely different agenda in mind . . .

==Cast==
* Kerwin Mathews as Jeff Farrell
* Nadia Gray as Eve Beynat
* Donald Houston as Henri
* Liliane Brousse as Annette Beynat
* George Pastell as Inspector Etienne
* Arnold Diamond as Janiello
* Norman Bird as Salon
* Justine Lord as Grace
* Jerold Wells as Giles
* Leon Peers as Blanchard
* André Maranne as Salon

==Production==
It was filmed in black and white in the Camargue district of southern France and the MGM British Studios in Borehamwood, Hertfordshire. 

==Release==
Maniac was released by Hammer Film Productions on 20 May 1963 in the United Kingdom. 

==Critical reception==
Turner Classic Movies wrote, "Maniac has excellent production values but labours under the weight of yet another gimmicky and obvious script by Jimmy Sangster....The acting is fine, especially that of Kerwin Mathews and Liliane Brousse.";   while in The New York Times, Bosley Crowther wrote, "Maniac has one thing and has it in spades—a plot of extraordinary cunning...(It) takes on a twitching suspense that simmers, sizzles and explodes in a neat backflip", though he concluded, "Michael Carreras direction is uneven and the characters are a generally flabby lot...Maniac remains a striking blueprint, with satanic tentacles, for a much better picture."  

==External links==
* 
*  

==References==
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 