Dhoomketu
{{Infobox film
| name           = Dhoomketu
| image          =Dhoomketu (1949).jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Homi Wadia
| producer       = Basant Pictures
| writer         = 
| screenplay     = Homi Wadia
| story          = 
| based on       = 
| narrator       = 
| starring       = Fearless Nadia John Cawas Boman Shroff
| music          =  A. Karim
| cinematography = 
| editing        = 
| studio         = Basant Pictures
| distributor    = M. B. Billimoria & Son
| released       = 1949
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1949 action adventure film directed by Homi Wadia.    Made under the Basant Pictures banner, the music was composed by A. Karim with lyrics by Rajjan and A. Karim.    Raja Sandow an early hero and director of films played a supporting role in this film.    The film starred Fearless Nadia, John Cawas, Sona Chatterji, Dalpat, Boman Shroff, Ram Singh and Raja Sandow.   

The setting is soon after India’s Independence and Inspector Comet has been put in charge of getting rid of black-marketeers.

==Plot==
Seth Dharmadas is a respectable man in society who is shown as a humanitarian helping the needy women of society. However his evil deeds are not known to many. He is one of the biggest black-marketeers in a time when grain is scarce. Inspector Dayal is helped by his daughter Shanti and Inspector Comet in finding the guilty. When Dayal gets near the truth he is killed off by Dharamdas. Mohini Comet’s fiancée and Shanti along with others help in catching the villain after a frantic round of action and chase scenes.

==Cast==
* Fearless Nadia
* John Cawas
* Sona Chatterji
* Dalpat
* Boman Shroff
* Ram Singh
* Raja Sandow
* Habib

==Music==
Songlist.   
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| Dil Mile Kyu Kar Mile Jab Hum Yaha Tum 
|-
| 2
| Kyun Meri Duniya Badal Di Boliye
|-
| 3
| Roye Ja Bulbul Roye Ja 
|-
| 4
| Log Kahte Hai Agar Paisa Nahi To Kuch Bhi 
|-
| 5
| Mai Bina Pankh Ka Panchhi Hu 
|-
| 6
| Tu Hai Mujhse Dur Mai Tujhse Dur 
|-
| 7
| Zindagi Hai Jab Tak Kuch Gam Na Khana 
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 