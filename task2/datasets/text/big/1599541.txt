Bewitched (2005 film)
 
{{Infobox film
| name = Bewitched
| image = Bewitchedmovieposter.jpg
| alt = 
| caption = Theatrical release poster
| director = Nora Ephron
| producer = {{Plainlist|
* Nora Ephron
* Douglas Wick
* Penny Marshall}}
| writer = {{Plainlist|
* Nora Ephron
* Delia Ephron}}
| based on =  
| starring = {{Plainlist|
* Nicole Kidman
* Will Ferrell
* Shirley MacLaine
* Michael Caine
* Jason Schwartzman
* Kristin Chenoweth
* David Alan Grier
* Heather Burns
* Stephen Colbert}}
| music = George Fenton John Lindley
| editing = Tia Nolan
| studio = Red Wagon Entertainment
| distributor = Columbia Pictures
| released =  
| runtime = 102 minutes  
| country = United States
| language = English
| budget = $85 million   
| gross = $131.4 million 
}} romantic comedy the same name (produced by Columbias Screen Gems television studio, now Sony Pictures Television). The film was released in the United States and Canada on June 24, 2005. It was written, produced, and directed by Nora Ephron and starred Nicole Kidman and Will Ferrell.

The film opened theatrically on June 24, 2005 and was a critical and commercial failure.

==Plot==
Jack Wyatt is a narcissistic actor who is approached to play the role of Darrin in a remake of the 1960s sitcom Bewitched, but insists that an unknown play Samantha. Isabel Bigelow is an actual witch who decides she wants to be normal and moves to Los Angeles to start a new life and becomes friends with her neighbor Maria. She goes to a bookstore to learn how to get a job after seeing an advertisement of Ed McMahon on TV. Jack happens to be at the same bookstore after attending some failed Samantha auditions. Jack spots Isabel and persuades her to audition. At the same time, while shes trying to settle into her new life, Isabels intrusive father Nigel keeps appearing to convince her to return home, despite several rejections from Isabel.

After Isabel impresses the shows producers and writers, Jack finally convinces Isabel to join the show. Also joining the show is legendary actress Iris Smythson as Endora. After a successful taping of the pilot, Isabel happens to overhear a conversation Jack is having with his agent Ritchie, who are talking about how they tricked Isabel to appear without having any lines. Furious, Isabel storms off with Maria and new friend Nina close behind. She decides she only has three choices: quit, get mad, or live with it. Instead, Isabels Aunt Clara visits and aids Isabel in casting a spell on Jack in order to make him fall in love with her. At the same time, Nigel is introduced to Iris and becomes infatuated with her.

The hex works and Jack becomes love struck by Isabel, insisting on several script changes to give her some dialogue and jokes, ignoring statements from test groups preferring Isabel over him. Jacks affection for Isabel grows and he asks her out on a date, making Isabel forget about the hex. But when he brings her home, she remembers and reverses it back to when she and Aunt Clara cast it. The next day, rather than the events the hex presented, Jack is outraged by the scores he received and takes his anger out on Isabel, who lashes back at him. Ritchie fires her, and she storms off. Rather than be angry at her, Jack is fascinated with Isabel and chases after her, taking all her comments into thought. So, after another great taping (with Isabel getting dialogue), a romance blossoms between the two. But the next day, Jacks ex-wife Sheila arrives, determined to woo Jack back. Isabel sees this and casts a spell on her making her sign the divorce papers and have her decide to move to Iceland. Jack, thrilled, announces he will be throwing a party at his house celebrating the divorce.

Nigel attends the party with Iris and when Nigel begins flirting with much younger guests. Iris reveals she is also a witch and casts a spell on each girl. When Jack makes a toast stating truth will be revealed with everyone, Isabel decides to tell Jack shes a witch. At first thinking shes simply an amateur magician, Jack officially believes her when she levitates him with her broom. Jack becomes frightened and shoos her away with a stick. Offended, Isabel flies off.

Jack takes this hard, being brought to the studios by the police and becoming disenchanted with the project. Isabel decides to return home as she no longer wishes to stay. Jack, imagining himself on the Conan OBrien Show, is visited by Uncle Arthur. Arthur convinces Jack not to let Isabel leave, because Jack still loves her and wouldnt be able to return for 100 years (which is later proven to be a lie Arthur made up to inspire Jack). Arthur drives him to the studio where he finds Isabel at the set. Jack apologizes to her and tells her he wants to marry her. They do and move into their new neighborhood (which resembles the neighborhood in the series).

==Cast==
 
 
* Nicole Kidman as Isabel Bigelow / Samantha Stephens
* Will Ferrell as Jack Wyatt / Darrin Stephens
* Shirley MacLaine as Iris Smythson / Endora
* Michael Caine as Nigel Bigelow
* Jason Schwartzman as Ritchie
* Kristin Chenoweth as Maria Kelly
* Heather Burns as Nina
* Stephen Colbert as Stu Robison
* David Alan Grier as Jim Fields Jim Turner as Larry
* Katie Finneran as Sheila Wyatt
 
;Characters from the series
* Carole Shelley as Aunt Clara
* Steve Carell as Uncle Arthur
* Amy Sedaris as Gladys Kravitz
* Richard Kind as Abner Kravitz
* Elizabeth Montgomery (uncredited archive footage) as Samantha Stephens
* Dick York (uncredited archive footage) as Darrin Stephens
* Agnes Moorehead (uncredited archive footage) as Endora
* Paul Lynde (uncredited archive footage) as Uncle Arthur
 
;Cameo appearances
* Ed McMahon as himself
* Conan OBrien as himself
* James Lipton as himself
* Nick Lachey as Vietnam soldier Kate Walsh as Waitress Abbey McBride as Auditioner
* Teri Robinson as the witch
 

==Production==
Principal photography took place in late 2004 and early 2005.

==Reception==
Bewitched received negative reviews from critics, and by many of the original shows fanbase. Budgeted at $85 million, the worldwide gross of $131,413,159 was considered disappointing. Rotten Tomatoes reported that 24% of critics gave the film a positive review, based upon 182 reviews.  The total US gross was $63,313,159, with international at $68,100,000.
 Worst Director, Worst Actor Worst Screenplay, Worst Remake or Sequel. The New York Times called the film "an unmitigated disaster".  Australian critics Margaret Pomeranz and David Stratton gave the film three and half stars out of five stars, both Margaret and David said that Kidman captured the character exactly right. 

==Home media==
The DVD was released on October 25, 2005 by Sony Pictures Home Entertainment. It included deleted scenes such as Jack and Isabels wedding and an extended version of Isabel getting mad, several making-of featurettes, a trivia game, and an audio commentary by the director.

==In popular culture==
 
In a season six episode of   and Bewitched. While Luke comments on "the upper ground" and "those flash-light thingies" from the latest Star Wars film, Lorelai says he cannot criticize the movie because he wanted to see it. Luke then accuses of her of doing the same about Bewitched. Lorelai agrees that Nicole Kidman was a good choice but was appalled that Larry Tate was not included in the film.
 Jungle Love" Clippers fan!" The audience in the cinema laughs at this, which infuriates Stewie so much that he flies to Ferrells house in Los Angeles to punch him in the face, exclaiming "Thats not funny!"

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 