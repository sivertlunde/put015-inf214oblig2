Paglu 2
 
{{multiple issues|
 
 
}}
{{Infobox film
| name = Paglu 2
| image = Paglu 2 poster.jpg
| director = Sujit Mondal
| based on =  
| writer = 
| producer = Nispal Singh
| studio = Surinder Films Dev Koel Mallick Tota Roy Chowdhury Rajatava Dutta Rimjhim Mitra
| music = Jeet Ganguly
| cinematography = Kumud Verma
| editor = Rabiranjan Moitra
| released =  
| country = India Bengali
| budget = 
| gross =  screenplay = N.K. Salil
}}
 Dev and Ram and Hansika Motwani.
  

==Plot==
Dev (Dev (actor)|Dev) is a high school drop out from and a worthless village lad. He is hell bent on getting married. But the girl he wants to marry rejects him stating that he doesnt have college education. Dev, who is clever and street-smart decides to go to Kolkata for further education. While travelling in the train, he bashes up a bunch of rowdies teasing college girls. At the college in Kolkata, he meets Riya (Koel Mallick) and falls in love with her. A city gangster, Rudra, (Tota Roy Chowdhury) is in love with her and blackmails her to marry him. He beats anyone moving closely with Riya. Meanwhile, henchmen of the dreaded chieftain Dubai Keshto (Rajatava Dutta), from Dubai, are looking for Dev in the city.

==Cast== Dev as Dev (Paglu)
* Koel Mallick as Riya
* Tota Roy Chowdhury as Rudra
* Rajatava Dutta as Dubai Keshto
* Rimjhim Mitra as Kabita Ghoshal, Dubai Keshtos daughter
* Biswajit Chakraborty as Devs father
* Biswanath Basu as Devs friend
* sayak chakraborty as Devs friend

== Soundtrack ==
{{Infobox album
| Name = Paglu 2
| Type = soundtrack
| Artist = Various
| Cover =
| Released =  
| Recorded = 2012 Feature film soundtrack
| Length =
| Label = V Music
| Producer = Surinder Films
}}
The soundtrack of the film was scored by Jeet Ganguly.
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s) !! Duration (min:sec)
|-
| 1 || "Love You Love You Oh My Paglu" || Mika Singh, Shreya Ghoshal || 4:03
|-
| 2 || "Ekta Premer Gaan Likhechi" || Jeet Ganguly || 4:51
|-
| 3 || "Mon Khali Khali Tui Tui Kore" || Zubeen Garg, Akriti Kakkar || 3:24
|-
| 4 || "Khuda Jaane" || Zubeen Garg, Shyam Bhatt & Shreya Ghoshal || 3:30
|}

== References ==
 

 
 
 
 


 