Tom Brown of Culver
{{Infobox film
| name           = Tom Brown of Culver
| caption        = 
| image	         = 
| director       = William Wyler
| producer       = 
| writer         =  Richard Cromwell Ben Alexander Sidney Toler Russell Hoption Andy Devine
| music          = 
| cinematography = 
| editing        = 
| studio         = Universal
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}

Tom Brown of Culver is a 1932 film. 

==Plot==
A young man attends Culver Military Academy. He is the only son of a deceased soldier who won the Congressional Medal of Honour.

==Production==
Alan Ladd and Tyrone Power have small roles.

==References==
 

==External links==
*  at IMDB
*  at TCMDB
*  at New York Times
*  at Tyrone Power.com
 

 
 


 