War Comes to America
{{Infobox Film
| name           = War Comes to America
| image          = File:Why We Fight.7.War Comes To America.webm
| image_size     = 
| caption        = Film
| director       = Frank Capra Anatole Litvak
| producer       = Frank Capra
| writer         = Julius J. Epstein Philip G. Epstein Emma Lazarus Anthony Veiller
| narrator       = Walter Huston Lloyd Nolan
| starring       = Dean Acheson General Bergeret Adolf A. Berle
| music          = Dimitri Tiomkin
| cinematography = 
| editing        = William Hornbeck
| distributor    = U.S. Army Pictorial Services
| released       =  
| runtime        = 70 min.
| country        = United States
| language       = English
| budget         = 
}}

War Comes to America is the seventh and final film of Frank Capras Why We Fight World War II propaganda film series.

The early part of the film is an idealized version of American history which includes mention of the first settlements, the American Revolutionary War (omitting the American Civil War), and the ethnic diversity of America. It lists 22 immigrant nationalities, 19 of them European, and uses the then-current terms "Negro", "Jap", and "Chinaman". This section of the film concludes with a lengthy paean to American inventiveness, economic abundance, and social ideals.
 Allied cause, and demonstrates this using a series of Gallup polls.

In 1936, public opinion is firmly isolationist, with 95% of Americans answering NO to the question "If another world war develops in Europe, should America take part again?".  Congress responded with an arms embargo and a "Cash and carry rule" when trading with belligerents in raw materials.
 current fight between Japan and China, are your sympathies with either side?" is answered CHINA 43%, JAPAN 2%, UNDECIDED 55%, while in June 1939 the same question gives a 74% vote for China.  Anti-Japanese sentiment thus forced the US government to block trade in oil and scrap iron with Japan.
 attacked by Germany).

Towards the end the film argues in detail (to a backdrop of animated maps and diagrams) that American involvement in the war was essential in terms of self-defense.  The dire consequences for the United States of an Axis victory in Eurasia are spelled out:

:German conquest of Europe and Africa would bring all their raw materials, plus their entire industrial development, under one control.  Of the 2 billion people in the world, the Nazis would rule roughly one quarter, the 500 million people of Europe and Africa, forced into slavery to labor for Germany.  German conquest of Russia would add the vast raw materials and the production facilities of another of the worlds industrial areas, and of the worlds people, another 200 million would be added to the Nazi labor pile.

:Japanese conquest of the Orient would pour into their factory the almost unlimited resources of that area, and of the peoples of the earth, a thousand million would come under their rule, slaves for their industrial machine. Altogether, the German, Italian and Japanese aggressors would undertake a catalystic crisis, one that would enslave most of the worlds population and liquidate about 90% of cultural life on Earth.

:We in North and South America would be left with the raw materials of three-tenths of the earths surface, against the Axis with the resources of seven-tenths.  We would have one industrial region against their three industrial regions.  We would have one-eighth of the worlds population against their seven-eighths.  If we together, along with the other nations of North and South America, could mobilize 30 million fully equipped men, the Axis could mobilize 200 million.

:Thus, an Axis victory in Europe and Asia would leave us alone and virtually surrounded facing enemies ten times stronger than ourselves.

The film ends with the Attack on Pearl Harbor—the film shows how the Japanese negotiators in Washington, led by Saburo Kurusu, were still negotiating with the Americans while the attack was taking place in Hawaii.  This was "the straw that broke the camels back" that caused the United States to enter the war.

==See also==

* Propaganda in the United States

==External links==
*  
*  

 

 
 
 
 
 
 
 