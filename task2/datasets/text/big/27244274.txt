Lovespell
 

{{Infobox film
| name           = Lovespell
| image          = 
| caption        =  Tom Donovan Tom Hayes Douglas Hughes Claire Labine Thomas H. Ryan
| writer         = Claire Labine Niall OBrien
| music          = Paddy Moloney
| cinematography = Richard H. Kline Russell Lloyd
| studio         = Paramount Pictures
| distributor    = John Lucas Ltd
| released       = December 1981
| runtime        = 97mins
| country        = Ireland, UK English
| budget         = 
| gross          =       
| followed_by    = 
}}
 Tom Donovan. Tristan and Isolde.

== Plot synopsis ==

Lovespell is based around a love triangle between King Mark of Cornwall (Richard Burton), Isolt (Kate Mulgrew), and Tristan (Nicholas Clay). Mark discovers Isolts love for Tristan, and banishes Tristan. However, while being away, Tristan is mortally wounded. Isolt persuades Mark to go and take Tristan back to Cornwall. Mark says if he returns casting white sails Tristan is alive and if they are black Tristan is dead. Mark returns with Tristan barely alive with white sails, but casts black sails when Tristan reveals his plans to run away with Isolt as soon as he has recovered. This causes Isolt to kill herself by throwing herself off the White Cliffs of Dover. Mark helps Tristan swim to the shore, and as Tristan and Isolts hands touch they both die, while Mark, knee deep in the water, looks on.

== Cast ==

{| class="wikitable"
|-
! Character Name
! Actor Name
|-
| King Mark of Cornwall
| Richard Burton
|-
| Isolt
| Kate Mulgrew
|-
| Tristan
| Nicholas Clay
|-
| Gondor of Ireland
| Cyril Cusack
|-
| Bronwyn
| Geraldine Fitzgerald
|-
| Andred
| Niall Toibin
|-
| Alix
| Diana Van der Vlis
|-
| Corvenal Niall OBrien
|-
| Yseult of the White Hand
| Kathryn Dowling
|-
| Father Colm
| John Jo Brooks
|-
| Anne
| Trudy Hayes
|-
| Bishop
| John Scanlon
|-
| William the Guard
| Bobby Johnson
|-
| Eoghanin
| John Labine
|}

==External links==
* 
* 

 

 
 
 
 
 