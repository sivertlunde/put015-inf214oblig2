The Principles of Lust
{{Infobox film
| name           = The Principles of Lust
| image          =
| caption        =
| director       = Penny Woolcock
| producer       = Madonna Baptiste
| writer         = Penny Woolcock
| narrator       = 
| starring       = Alec Newman Marc Warren Sienna Guillory Julian Barratt
| music          = 
| cinematography = Graham Smith
| editing        = Brand Thumim
| studio         = Blast! Films FilmFour Studio of the North
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} drama film directed by Penny Woolcock.

==Cast==
* Alec Newman as Paul
* Marc Warren as Billy
* Sienna Guillory as Juliette
* Julian Barratt as Phillip

==Release==
The Principles of Lust premiered January 29, 2003 at the International Film Festival Rotterdam. It was released on DVD July 26, 2004.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 