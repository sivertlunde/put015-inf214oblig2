This Is the End
{{Infobox film
| name           = This Is the End
| image          = This-is-the-End-Film-Poster.jpg
| border         = yes
| alt            = Six worried-looking men stand on a suspended part of a street over a fiery pit. The primary cast members are listed across the top, and the tagline "Nothing ruins a party like the end of the world" is at the bottom.
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Seth Rogen
* Evan Goldberg}}
| producer       = {{Plainlist|
* Seth Rogen
* Evan Goldberg
* James Weaver}}
| screenplay     = {{Plainlist|
* Seth Rogen
* Evan Goldberg}}
| based on       =  
| starring       = {{Plainlist|
 
* James Franco
* Jonah Hill
* Seth Rogen
* Jay Baruchel
* Danny McBride Craig Robinson
* Emma Watson
* Michael Cera}}        
| music          = Henry Jackman
| cinematography = Brandon Trost
| editing        = Zene Baker
| studio         = {{Plainlist|
*Mandate Pictures
*Point Grey Pictures}}
| distributor    = Columbia Pictures
| released       =   
| runtime        = 106 minutes  
| country        = United States
| language       = English
| budget         = $32 million 
| gross          = $126 million 
}} disaster  meta dark directorial debut, Craig Robinson, Emma Watson and Michael Cera. The film centers around a group of real life actors playing versions of themselves which are to varying degrees fictional, in the aftermath of a global apocalypse. The film was released on June 12, 2013, and was a critical and commercial success. As a result, the film was re-released in theaters on September 6, 2013. 

==Plot==
 
Jay Baruchel arrives in Los Angeles to visit with old friend and fellow actor Seth Rogen, who invites him to attend a housewarming party hosted by James Franco. At the raucous party, many celebrities drink, take drugs, and have sex. Jay is uncomfortable being around so many people that he hardly knows and leaves the party to buy some cigarettes, accompanied by Seth.
 Craig Robinson ration system, board up the doors and windows and await help. 

The next morning, Danny McBride,  wakes up first. Unaware of the crisis, he wastes much of the supplies and disbelieves the others accounts until a desperate outsider seeking aid is decapitated in their presence. The men pass the time by taking drugs and filming a homemade sequel to the film Pineapple Express. Tensions rise, however, due to various conflicts, including Jay and Seths growing estrangement, and the others skepticism of Jays belief that the disaster might be the Apocalypse predicted in the Book of Revelation.

Later, Emma Watson, who also survived the party, returns to the house. However, after misunderstanding a conversation between the men, Emma believes that they plan to rape her and leaves, taking all of their drinks with her. Craig is chosen to travel outside to the basement for water, but finds the door locked and returns after an encounter with an unknown being.

Craigs experience with the creature causes him to believe Jays theory of the Apocalypse. The group reaches the basement by digging through the floor and they find the water, but Danny wastes much of it out of spite. The group unanimously votes Danny out of the house. Franco gives him the revolver, which Danny attempts to use to kill the others, only to find out its filled with blanks. Before leaving, Danny ridicules his friends, and reveals that Jay had been to Los Angeles in recent months, a fact which he concealed from Seth. Jonah gently chastises Jay, and Jay punches him in the face. Later that night, Jonah prays for Jay to die and is raped by a demon.
 LBS episode, but he wakes up Demonic possession|possessed. Jonah chases James and Seth while Craig and Jay flee a demonic bull at the neighbors house. The group subdues Jonah and tie him up. Later, the lights go out and Jay gets an idea. During an exorcism attempt, Jay and Seth get into a fight and knock a candle over, engulfing Jonah and the house in flames.

When the remaining four flee outside, James suggests taking his Prius to his home in Malibu, but a winged demon has roosted on his garage and is fixated on them. Craig volunteers to sacrifice himself as a distraction. The plan succeeds and Craig is raptured. As the others escape, they realize that Craig was saved because he sacrificed himself and it means there is still hope for them to be redeemed. Suddenly, the car is Side collision|t-boned by an armored motor home filled with cannibalism|cannibals, led by Danny. James volunteers to sacrifice himself in order to save the others from being eaten and get raptured. However, James starts swearing at Danny as he ascends into Heaven and is left back on Earth as a result. Danny and his cannibals eat James alive and then chase after Seth and Jay. When a colossal-sized Satan approaches them, Jay apologizes to Seth for ruining their friendship and the two embrace while preparing to die together. This results in Jay being raptured, but not Seth. Seth grabs onto Jays hand while Jay is ascending into Heaven as Satan tries to kill them, but a force-field cuts him off. However, because Seth is unworthy, the two slow down and descend. Seth decides to let go in order to spare Jay, but as he falls, another blue beam appears and he also ascends to Heaven.The beam splits Satan down the middle in the process.

In Heaven, Jay and Seth reunite with Craig, who is now an angel and also makes Jay and Seth angels (by giving them Halo (religious iconography)|halos). He tells the two that Heaven is a place where any desire comes true. After Seth wishes for a Segway, Jay wishes for the Backstreet Boys and the band performs "Everybody (Backstreets Back)" while everyone dances along.

==Cast==
Most of the films cast portray fictional, exaggerated versions of themselves: 
 
* James Franco
* Jonah Hill
* Seth Rogen
* Jay Baruchel
* Danny McBride Craig Robinson
* Emma Watson
* Michael Cera
* Mindy Kaling
* David Krumholtz
* Christopher Mintz-Plasse
* Rihanna
* Martin Starr
* Paul Rudd
* Channing Tatum Kevin Hart
* Aziz Ansari
* Jason Segel (uncredited)
* Jason Trost as JTRO (uncredited)
* Brandon Trost as a cannibal (uncredited)
* Brian Huskey as Headless Man
* Backstreet Boys
 

==Production== Sony lot, and Antmen attacked from the center of the earth."    The film is also based on Jay and Seth versus the Apocalypse, a short film created by Stone and Goldberg in 2007.  In an interview with The Guardian, Goldberg commented on influences contributing to the film, saying " f you drilled down to the core of what I do, its just ripping off little bits of Charlie Kaufman. Seth and I always loved The Larry Sanders Show  too. And the popularity of reality television now also feeds into that idea of whether what were watching is actually real. We thought working with our friends in that situation would be awesome because theyre all comedians willing to take stabs at themselves."  The actors play exaggerated versions of themselves, with only James Franco having no objections to doing what the script wanted him to do. 
 The Worlds End, also released in the summer of 2013 and centered around an apocalypse with an ensemble cast. As The Worlds End was the name of a key location in that film, Pegg worried he could not change the name of his film.  Modus FX made 240 visual effects for the film, which include natural disasters, set extensions for the house that serves as a primary location, computer-generated demons, and the Rapture lights that raise people to Heaven. 
 trailer for Pineapple Express 2, which was in fact a teaser trailer for This Is the End.  According to Rogen and Goldberg, however, the homemade Pineapple Express 2 film in This Is the End depicts what they envision for the actual sequel.   

==Music==

===Soundtrack===
{{Infobox album
| Name        = This Is the End: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = June 11, 2013
| Recorded    = 2013
| Genre       = Film soundtrack
| Length      = 48:43
| Label       = RCA Records
| Producer    =
| Reviews     =
}}
This Is the End: Original Motion Picture Soundtrack is the soundtrack of the film. It was released on June 11, 2013   by RCA Records.

{{Track listing
| extra_column    = Performer(s)
| total_length    = 48:43
| title1          = Take Yo Panties Off Craig Robinson
| length1         = 5:06
| title2          = Step Into a World (Raptures Delight)
| extra2          = KRS-One
| length2         = 4:50 Tipsy (Club Mix)
| extra3          = J-Kwon
| length3         = 4:05
| title4          = A Joyful Process
| extra4          = Funkadelic
| length4         = 6:17
| title5          = Love in the Old Days
| extra5          = Daddy
| length5         = 4:18
| title6          = When the Shit Goes Down
| extra6          = Cypress Hill
| length6         = 3:11
| title7          = Watchu Want
| extra7          = Belief & Karniege
| length7         = 2:57
| title8          = Easy Fix
| extra8          = K.Flay
| length8         = 3:36
| title9          = Spirit in the Sky
| extra9          = Norman Greenbaum
| length9         = 3:58
| title10         = Everybody (Backstreets Back)
| extra10         = Backstreet Boys
| length10        = 3:45
| title11         = Please Save My Soul
| extra11         = Church Friends Choir featuring Pamela Landrum
| length11        = 2:07
| title12         = I Will Always Love You
| extra12         = Whitney Houston
| length12        = 4:33
}}

;Songs featured in the film, but not in the soundtrack:
* "Gangnam Style" by Psy
* "Hole in the Earth" by Deftones Disco 2000" Pulp
* "Spiteful Intervention" by Of Montreal
* "Paper Planes" by M.I.A. (artist)|M.I.A. End of the Beginning" and "War Pigs" by Black Sabbath
* "The Next Episode" by Dr. Dre.

===Score===
The score by Henry Jackman, with additional material by Dominic Lewis and Matthew Margeson and conducted by Nick Glennie-Smith, was not officially released on its own, not even as bonus tracks on the CD or digital releases on RCAs album. Despite that, a promotional album for the score does exist, according to Soundtrack.net. 

{{Track listing
| total_length    = 29:30
| title1          = Rapture on Melrose
| length1         = 1:47
| title2          = Hills on Fire/The Sinkhole
| length2         = 3:36
| title3          = Foreboding News Report
| length3         = 0:22
| title4          = Cant Sleep
| length4         = 0:43
| title5          = Head Guy
| length5         = 0:30
| title6          = The Sinkhole Remains
| length6         = 0:35
| title7          = This Shit is Biblical
| length7         = 0:58
| title8          = Boredom Montage
| length8         = 0:36
| title9          = Emma Returns
| length9         = 0:15
| title10         = Drawing Matches Pt 1
| length10        = 0:42
| title11         = Craig Gets the Water
| length11        = 1:37
| title12         = Creepy Basement Memorabilia
| length12        = 0:25
| title13         = The Devil Rapes Jonah
| length13        = 1:17
| title14         = Drawing Matches Pt 2
| length14        = 1:05
| title15         = Jay & Craig Go Outside
| length15        = 1:22
| title16         = Somethings Wrong with Jonah
| length16        = 0:37
| title17         = Jonah is Possessed
| length17        = 0:35
| title18         = Demonic Chase Sequence
| length18        = 4:01
| title19         = Lights Out, Jays Got a Plan
| length19        = 0:22
| title20         = The Exorcism of Jonah Hill
| length20        = 0:37
| title21         = Fire Chase
| length21        = 1:12
| title22         = Craigs Last Stand
| length22        = 2:15
| title23         = Francos Demise
| length23        = 1:31
| title24         = The Rapture of Seth & Jay
| length24        = 2:25
}}

==Reception==

===Critical response===
 
This Is the End received generally positive reviews from critics. Rotten Tomatoes gives the film a rating of 83%, based on 206 reviews, with an average rating of 7.1/10. The sites consensus reads, "Energetic, self-deprecating performances and enough guffaw-inducing humor make up for the flaws in This Is the End s loosely written script."  On Metacritic, the film has an average rating of 67 out of 100, based on 41 critics, indicating "generally favorable reviews". 
 letter grade of "A", saying, "You could sit through a years worth of Hollywood comedies and still not see anything thats genuinely knock-your-socks-off audacious. But This Is the End (opening June 12) truly is. Its the wildest screen comedy in a long time and also the smartest, the most fearlessly inspired and the snort-out-loud funniest." 

Brian D. Johnson of  -Blockbuster (entertainment)|blockbuster." 

At the other end of the scale, Canadas The Globe and Mail compared the film to the interminable wait for a cancelled bus, giving it one and a half stars out of five. He referred to the actors in the film as "the lazy, the privileged and the mirthless". 

===Box office=== Man of Steels $116.6 million. This Is the End grossed $101,470,202 in North America, and $24,571,120 in other countries, for a worldwide total of $126,041,322.    

==Home media==
This Is the End was released on DVD and Blu-ray Disc|Blu-ray on October 1, 2013.  

==Possible sequel==
When asked if a sequel to the film was probable, director and co-writer Evan Goldberg said, "If you ask me, Id say theres a pretty good chance of a sequel. If you ask Seth  , hed say no." On June 26, 2013, Goldberg announced ideas for a sequel in which the apocalypse occurs at the premiere of This Is the End. "Seth’s a cokehead in this version, Michael Cera is a calm dude with a boyfriend, Rihanna and The Backstreet Boys are back," Goldberg said in an interview. “We have a lot of ideas: a heaven and hell, for example, and a garden of Eden version where Danny   is Adam.” 

Despite this, Goldberg has stated it would be difficult to re-create the casting conditions from the first film due to different schedules, believing them to be a stroke of luck, saying, "I honestly dont know if we could get the guys together  ."

On May 29, 2014, Seth Rogen posted a status on Twitter saying, "I dont think well make a sequel to This Is the End, but if we did, it would be called No, THIS Is the End. 

==See also==
* Its a Disaster&nbsp;– a 2012 black comedy in which couples at a brunch realize that the world is ending Craig Robinson

==References==
{{Reflist|30em|refs=
   

   

   

   

   

   

   

   

}}

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 