Dating the Enemy
 
 
{{Infobox Film
| name = Dating The Enemy
| image = datingtheenemy.jpg
| director = Megan Simpson Huberman
| writer = Megan Simpson Huberman John Howard Matt Day
| producer = 
| distributor = 
| released = 1996
| runtime = 97 minutes
| music =
| cinematography =
| editing = 
| country = Australia
| language = English
| budget = 
}} swap bodies and have to live as each other.

==Plot==
One Valentines evening a group of single, dateless friends get together to play Trivial Pursuit. Brett (Guy Pearce), a friend of the host from Melbourne, has just landed a job as presenter of a TV gossip show. He is brash and self-confident. Tash (Claudia Karvan) is a science journalist for a national newspaper, studious, intense and self-conscious. They have nothing in common, so naturally they get it together.

A year later and Bretts career is going well, there is the possibility of a job in New York and popularity has gone to his head. Tash is still trying to write serious scientific articles for a paper more interested in gossip and sex and struggling to prevent her articles being buried on page 12. Their relationship is on the rocks. That night during a Valentines boat trip on Sydney Harbour, an argument ensues and Brett decides he has had enough. Tash tells him: I wish you could be me, so you could see how I feel for once. I wish I could be you, so I could show you what an idiot youve become!.

That night is a full moon and fate decides to lend a hand. They wake to find that each is in the others body. A month of each pretending to be the other ensues and they have to learn what its really like to be in the other persons shoes. Tash has to try to keep Bretts high-profile career on track. Brett is so bored by Tashs job, he ends up trying to make science sexy. Each learns to appreciate the other as only together can they make this enforced predicament work out.

==Production==
The film was co financed by French distributor Pandora Film.
==Reception==
One review described Dating the Enemy as: "A fast and funny look at relationships in the 90s, the smash-hit romantic comedy Dating the Enemy brings a whole new meaning to the battle of the sexes." 

==Soundtrack== OMC song "Right On". The original video for "Right On" features clips from the movie.

==Box Office==
 Dating The Enemy  grossed $2,620,325 at the box office in Australia,  which is equivalent to $3,668,455
in 2009 AS dollars.

==See also==
* Cinema of Australia

==References==
 

==External links==
* 
* 


 
 
 
 
 
 