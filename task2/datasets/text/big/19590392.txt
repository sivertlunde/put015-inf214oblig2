Calling Bulldog Drummond
 
 
{{Infobox film
| name           = Calling Bulldog Drummond
| image          = UK film poster for Calling Bulldog Drummond.jpg
| image_size     =
| caption        = UK quad poster for the film Calling Bulldog Drummond
| director       = Victor Saville
| producer       = Hayes Goetz
| writer         = Gerard Fairlie Howard Emmett Rogers Arthur Wimperis Herman C. McNeile
| narrator       =
| starring       = Walter Pidgeon Robert Beatty Margaret Leighton David Tomlinson
| music          = Rudolph G. Kopp
| cinematography = Freddie Young
| editing        = Frank Clarke
| studio         =  MGM
| released       = 2 July 1951
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = $1,594,000  . 
| gross          = $889,000 
}}

Calling Bulldog Drummond is a 1951 British crime film directed by Victor Saville and featuring Walter Pidgeon, Margaret Leighton, Robert Beatty, David Tomlinson, and Bernard Lee.    It featured the character Bulldog Drummond created by the novelist Herman Cyril McNeile, which had seen a number of screen adaptations. A novel tie-in was also released in 1951.

Drummond is called out of retirement by Scotland Yard to infiltrate a ruthless London crime outfit.

==Plot==
After three robberies are pulled off with military precision, Inspector McIver (Charles Victor) asks Hugh "Bulldog" Drummond (Walter Pidgeon) to give Scotland Yard a hand. As an ex-officer, Drummond knows how the suspected military mastermind would think. He agrees, though he very reluctantly accepts Sergeant Helen Smith (Margaret Leighton) of Special Branch as his partner, believing that women are not cut out for that sort of undercover work.

Drummond arranges to get caught cheating at poker at his London club so he can drop out of sight. Smith causes a minor car accident involving Arthur Gunns (Robert Beatty), suspected of being in the gang. Gunns attraction to Smith and carefully planted evidence showing "Joe Crandall" and "Lily Ross" to be criminals themselves enables the pair to infiltrate the gang.

Drummonds friend Algernon Longworth (David Tomlinson), who has been kept in the dark about the whole matter, becomes convinced that all is not what it seems. He telephones Colonel Webson (Bernard Lee), a member of Drummonds club, to get him to postpone Drummonds disciplinary meeting. By so doing, he inadvertently tips off the secret leader of the gang. Drummond and Smith are taken prisoner.

Gunns girlfriend Molly (Peggy Evans) convinces him to go ahead with the latest planned robbery, enough to set them up for life, despite the police having been put on alert by Drummond. She masquerades as Smith to give phony information to Longworth to pass along to the police regarding the target of the theft. Afterward, Longworth is tied up as well.

The gang steals £500,000 in gold being delivered by airplane. Drummond is able to overpower the guard and his friends before the gang returns. He knocks out Gunns (who has locked up and gassed his unsuspecting confederates nearly to death to avoid sharing the loot). Webson shows up and holds Drummond at gunpoint; he explains he got into the racket because civilian life turned out to be unbearably boring. The police arrive just in time and take him into custody.

== Cast ==
* Walter Pidgeon as Hugh "Bulldog" Drummond
* Margaret Leighton as Sergeant Helen Smith
* Robert Beatty as Arthur Gunns
* David Tomlinson as Algenon "Algy" Longworth
* Peggy Evans as Molly
* Charles Victor as Inspector McIver
* Bernard Lee as Colonel Webson James Hayter as Bill, a friend of Drummonds

==Reception==
According to MGM records the film earned $372,000 in the US and Canada and $517,000 elsewhere, resulting in a loss of $1,052,000. 

==Notes==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 