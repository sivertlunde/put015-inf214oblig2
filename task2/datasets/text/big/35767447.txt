The Relative of His Excellency
{{Infobox film
| name           = The Relative of His Excellency
| image          = 
| image_size     = 
| caption        = 
| director       = Félix Podmaniczky
| producer       = 
| writer         = Zoltán Szitnyai (novel)   Miklós Asztalos 
| narrator       = 
| starring       = László Szilassy Erzsi Simor Artúr Somlay   Zoltán Greguss 
| music          = Szabolcs Fényes 
| editing        = László Katonka
| cinematography = Rudolf Icsey 
| studio         = Magyar Film Iroda
| distributor    = 
| released       = 4 March 1941
| runtime        = 76 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =  Hungarian comedy film directed by Félix Podmaniczky and starring László Szilassy, Erzsi Simor and Artúr Somlay. It was based on a novel by Zoltán Szitnyai.

==Cast==
* László Szilassy  - Szávay Gábor
* Erzsi Simor  - Klára
* Artúr Somlay  - Szávay Ákos, vezérigazgató
* Mária Mezey  - Aliz 
* Kálmán Rózsahegyi  - Kublics
* Gyula Csortos  - Rafael
* Zoltán Greguss  - Tihamér, Aliz bátyja
* Gyula Kőváry  - Józsi bácsi 
* Sándor Pethes  - Ödön

==External links==
* 

 

 
 
 
 
 
 
 