L'Absent
{{Infobox Film
| name = LAbsent
| image = LAbsent original poster.jpg
| image_size =
| caption = Theatrical release poster
| director = Céline Baril
| producer = Céline Baril
| writer = Céline Baril
| narrator =
| starring = Roland Bréard Bobo Vian
| music = Roland Bréard
| cinematography = Céline Baril Michel Lamothe
| editing = Céline Baril Michel Lamothe
| distributor = Cinéma Libre (Quebec)
| released = 1997
| runtime = 78 minutes
| country =   French
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 Canadian experimental film written and directed by Céline Baril.              

The idea for the film was inspired by Barils discovery in a Paris flea market of a dusty old photo album. Interior scenes, and all exteriors of European cities, including Rome, Vienna, Budapest and Prague, as well as glimpses (footage) of old Black-and-white|B&W photos of a family, were shot in 8 mm film|8&nbsp;mm and blown up to 16 mm film|16&nbsp;mm for effect.   

== Synopsis ==
This film tells the story of Paul Kadar, an architect and musician who, en route to Budapest, is overcome by vertigo while contemplating the Danube and throws himself into it. It is an account of the happiness shared by Paul Kadar, his wife Françoise
and Roland, their adopted son.    Years later, Roland, still unclear as to the circumstances that led his father to drown himself in the Danube, heads to Europe to find out for himself. His journey takes him to such places as Budapest, Warsaw, Prague and even as far as Tokyo. 

== Cast ==
*Roland Bréard as Roland Kadar
*Bobo Vian as Hungarian Interviewer

== Reception ==
In August 1997, Brendan Kelly of Variety (magazine)|Variety wrote, "With its snail-like pacing, sub-par acting and underwhelming emotional impact, "The Absent One" is not likely to stir up much interest," and concludes with "Baril moves what little story there is along at an extremely slow speed, further straining viewer patience." 

== See also == 1997 Toronto International Film Festival/Perspective Canada

== References ==
 

== External links ==
* 
* 
* 
* 

 
 
 
 
 