Kodi Parakuthu
{{Infobox film
| name = Kodi Parakuthu
| image =
| caption =
| director = P. Bharathiraja
| writer = R. Selvaraj
| screenplay = P. Bharathiraja
| story = R. Selvaraj Amala Sujatha Sujatha Janagaraj Janagaraj Manivannan Vijayan
| producer =  P. Bharathiraja
| music =  Hamsalekha  
| cinematography = B. Kannan
| editing = P. Mohanraj
| studio = Manoj Creations
| distributor = Manoj Creations
| released = 8 November 1988
| runtime = 144 minutes
| country = India Tamil
| budget =
}}

Kodi Parakuthu is a 1988 Tamil drama film is directed by P. Bharathiraja. The film starred Rajinikanth, who worked with Bharathiraja nearly a decade after their last film 16 Vayathinile (1977). The film got mixed reviews and was a below average grosser at the box office

==Cast==
* Rajinikanth Sujatha
* Amala
* Janagaraj
* Manivannan
* ARS
* Veera Raghavan
* Bhagyalakshmi
* Sudha
* Rani Vijayan  
* Sangili Murugan  

==Soundtrack==
The music was composed by Hamsalekha.    The song "Oh Kadhal Ennai" was reused from Kannada song "O Meghave Nidavanagi" which Hamsalekha had composed for Ranaranga.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics
|- Vairamuthu
|-
| 2 || Annai Madiyil (Men) || S. P. Balasubrahmanyam
|-
| 3 || Oh Kadhal Ennai || S. P. Balasubrahmanyam, K. S. Chithra
|-
| 4 || Selai Kattum || S. P. Balasubrahmanyam, K. S. Chithra
|-
| 5 || Thondaikkulle || Malaysia Vasudevan, K. S. Chithra
|}

==Reception==
Surprisingly, the movie performed badly at the box office.

==References==
 

==External links==
 

 

 
 
 
 
 
 
 


 