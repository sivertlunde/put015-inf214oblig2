Curfew (1989 film)
{{Infobox film
| name           = Curfew
| image          = 
| image_size     = 
| caption        = 
| director       = Gary Winick
| writer         = Kevin Kennedy
| narrator       =  Frank Miller
| music          = Cengiz Yaltkaya 
| cinematography = Makoto Watanabe 
| editing        = Carole Kravetz
| distributor    = New World Pictures 
| released       = 1989
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} American Action horror 1989 film directed by Gary Winick.
 censorship  certification problems" in the United Kingdom. 

In fact, the film was rejected for video by the British Board of Film Classification in 1988 and was finally released fully uncut in 2002.   

== Plot ==

The Movieweb website provides a terse synopsis: "Two escaped brothers track down the people who sentenced them to death row, including a doctor and the judge. But when they get to the district attorney|D.A. and his family they have an especially lengthy revenge plot in mind for them." 
 Rovi also provides a recap in The New York Times: "After late-night carousing on too many weekends and having her parents impose a curfew upon her, a teen-age girl (Kyle Richards) speeds home to keep from winding up in hot water again but finds when she gets home that two escaped convicts (Wendell Wellman, John Putch) have taken her family hostage." 

Keith Bailey of the Unknown Movies website provides a lengthier synopsis:
 Chain Of Command), and got the judge to sentence them to death. But both brothers have escaped from prison, and are dead set on getting even with the people responsible for sending them to death row. After first dealing with other people at the trial (including the judge), they now set their sights on district attorney Walter and his family. While Stephanie is out, the Perkins brothers make their way into the Davenport home and quickly take Walter and Megan captive, and start their long and torture-filled plan of revenge. Of course, when Stephanie eventually has to come home, she too is taken hostage, and soon the question that comes up is if the Perkins will kill Stephanie and her parents before help arrives - if ever, that is.  

==Cast==
 
* Kyle Richards as Stephanie Davenport
* Wendell Wellman as Richard Perkins
* John Putch as Bob Perkins 
* Frank Miller as Walter Davenport 
* Jean Brooks as Megan Davenport Peter Nelson as John
* Niels Mueller as Pete
* Nori Morgan as Monica
* Peggy Rae as Mrs. Mary Cox  Christopher Knight as Sam 
* Robert Romanus as Jack
* Guy Remsen as Dr. Franklin  
* Peggy Pope as Mrs. Alva 
* Douglas Robinson as Judge Collins
* Marla Rix as Mrs. Collins
* Audrey Marxer as Girl in the Red Dress 
* Randal Patrick as Rancher #1
* Samuel Braslau as Rancher #2
 

== Tagline ==

"In by ten. Dead by midnight."

==Critical Reception==
TV Guide gave the movie a tepid review, granting 1 out of 4 stars in its rating:

 CURFEW, a low-budget thriller containing few thrills, centers on Stephanie Davenport (Kyle Richards), a teenage girl whose late-night carousing has forced her parents to impose a 10 oclock curfew on her. At the same time, two vicious brothers (Wendell Wellman and John Putch) have escaped from prison and are seeking revenge on the people who put them there, one of whom happens to be Stephanies father. When they arrive, Stephanie is out--past curfew, of course. Curfew has a few interesting ideas that seem to get lost in all of the cheap gore and shock effects. Among the actors, Wellman is actually pretty good, and Richards has a strong screen presence, but they are stuck in a muddled and cheaply made film, badly directed by Gary Winick... The players all act at a fever pitch and the generally brutal murder sequences are ham-handedly accompanied by noisy music or sound effects. The underlying theme--of Stephanies sexual awakening and her punishment as a result--is glossed over to concentrate solely on the torture and murder of other characters.  

Kevin Bailey of the Unknown Movies website was unimpressed by the acting:

 The general look and feel of the movie has an unconvincing taste to it. ... For the most part, the amateur cast members of Curfew give very amateurish performances... While Kyle Richards gives a mediocre performance as the daughter, it shines next to the actors playing her parents, who more often than not greatly restrain their emotions instead of going all out in a situation that would warrant such great emotion. ... Some praise has to go to Wendell Wellman and John Putch as the Perkins brothers... as the movie goes on, their acting does improve considerably, and by the second half of the movie their characters become genuinely creepy and come across as a real threat." Bailey adds, "the movie greatly improves as time goes on. I have to admit that some parts of the second half of Curfew did manage to land a wallop... Yes, the second half of the movie does sometimes deliver the goods, but what happens before that point is more often than not so dumb, and has been made to be so cheap and unprofessional, that I think many viewers will turn off their televisions before discovering the genuine merit the movie has to offer."  

The reviewer for Azibtorrent.com was even less impressed, giving the film a 3.7/10 rating and saying:

 Pretty stupid and totally insignificant no-budget thriller that looks an awful lot like a typical late 80s slasher... The prosecutor, along with his wife and yummy Demi Moore look-alike daughter are held hostage and their only chance to survive is for   to set the murdering siblings against each other. The script contains one or two creative ideas, but Curfew is overall tedious and without suspense. Actors John Putch and especially Wendell Wellman try incredibly hard to look like genuine bad boys, but they really dont convince and they often evoke unintentional laughs. Theres a lot of killing going on, but were only served a glimpse of the action and all the rest happens off screen.  
 Cape Fear, though unfavorably, before going on to say:

 And thats basically it. Mom, Dad and wannabe sexpot daughter are kept prisoner in their own home by a pair of killers who first threaten to kill them, then promise to kill them, then guarantee that they will definitely kill them but then ultimately, despite being provided with every imaginable opportunity, dont end up killing anyone – oh, apart from a bit part cop and an Art Garfunkel look-a-like who strays into the house to have sex with his girlfriend. For some reason.  

==Soundtrack==

=== Track listing === The Dig
# "Feel Alive" by The Dig
# "Barbararay" by The Dig
# "Foreign Girl" by The Dig
# "Calm or a Storm" by The Dig
# "The Cure" by Ru Ready
# "Wear Me Out" by The Dig
# "The Unknown" by The Dig 

==References==
 

==External links==
* 

 

 
 
 
 
 