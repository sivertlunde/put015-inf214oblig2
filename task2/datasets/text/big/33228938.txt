Weenie Roast
 
{{Infobox Hollywood cartoon|
| cartoon_name = Weenie Roast
| series = Krazy Kat
| image = Weenieroastkrazykat1931.jpg
| caption = Screenshot
| director = Ben Harrison Manny Gould
| story_artist = Ben Harrison
| animator = Manny Gould
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| distributor = Columbia Pictures
| release_date =September 14, 1931
| color_process = Black and white
| runtime = 6 min. English
| preceded_by = Svengarlic
| followed_by = Bars and Stripes
}}

Weenie Roast is a short animated cartoon distributed by Columbia Pictures, and stars Krazy Kat.

==Plot==
Krazy Kat and his spaniel girlfriend are singing and dancing at the beach. As they do their rhythmic leisure, they also roast some sausages for lunch above a bonfire. But because the fire is too close to the shore, waves come by which put out the flames and wash away their food. To startover, Krazy picks up some driftwood in the vicinity and lights them. The cat and the dog resume what they are doing.

Following their trip to the beach, Krazy and the spaniel head to a carnival. The two then decide to start with the roller coaster which the spaniel is quick to take seat. But while Krazy is still outside trying to pay for the ride, the coaster operator suddenly sneezes, therefore blowing the coaster away, much to the girl mutts panic. Krazy scales up the elevated the tracks to rescue his sweetheart. After chasing the coaster for a number of yards, the cat is able to get on board but wonders how he could stop it. The coaster eventually comes off the tracks and onto the fairgrounds where it runs through some tents and a tunnel of mirrors. Upon reaching the tunnels exit, the runaway railway vehicle finally drops into a shallow pond, thus spilling out its passengers. Down on their bottoms, Krazy and the spaniel are daze but also relieved from their nightmarish ride.

==Availability==
*Columbia Cartoon Collection: Volume 2. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 


 