In the Blood (2014 film)
{{Infobox film
| name           = In the Blood
| image          = IntheBlood2014.jpg
| caption        = Theatrical release poster John Stockwell
| screenplay     = James Robert Johnston Bennett Yellin
| starring       = Gina Carano Cam Gigandet Danny Trejo
| music          = Paul Haslinger
| cinematography = P.J. López Doug Walker
| studio         = 20th Century Fox Home Entertainment
| distributor    = Anchor Bay Films Freestyle Releasing
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English US $ 10,000,000 
| gross          = 
}} John Stockwell. The plot revolves around young woman called Ava who searches for her abducted husband.

==Plot==
Gina Carano plays Ava, a trained fighter with a dark past. When Avas husband, Derek (Cam Gigandet), vanishes during their Caribbean honeymoon, Ava faces a brutal underworld conspiracy in an island paradise. Ava decides to discover the truth and take down the men responsible for her husbands disappearance.

==Reception==
On Rotten Tomatoes the film holds a 44% approval rating.  Brian Tallerico from Rogerebert.com gave the film one star out of four, writing that the "film history is filled with xenophobic tales of pretty Americans who disappear in foreign lands and the pretty people tasked with finding them". 

==References==
 

== External links ==
*  
*   at AllMovie
*  

 

 
 
 

 