Ballet Boys
{{Infobox film
| title = BALLET BOYS
| film name = Ballettguttene

| image = 
| director = Kenneth Elvebakk
| producer = Carsten Aanonsen
| production company = Indie Film AS
| writer = Kennet Elvebakk
| composer = Henrik Skram 
| starring = Lukas Bjørneboe Brændsrød Syvert Lorenz Garcia Torgeir Lund
| released =  
| cinema =  
| TV =  
| country = Norway
}}
Ballet Boys (Ballettguttene / Dancers / 芭蕾夢 / Ballettstrakar / バレエ・ボーイズ /  Balettipojat / Балетные Мальчики / Balettpojkarna / Οι μπαλαρίνοι ) is a 2014 Norwegian documentary directed by Kenneth Elvebakk and produced by Indie Film in Oslo. The film is available in both for cinema (75 min) and television (59 min.).

It is not easy being a teenager, especially not seen through the eyes of three ballet boys. This is a film about friendship, ambitions, identity and passion. Ballet Boys takes you through disappointment, victories, forging of friendship, first loves, doubt, faith, growing apart from each other and finding your own way and own ambition. In short, everything about being a teenager, all mixed with the beautiful expression of ballet. Lukas, Syvert and Torgeir are experiencing important first years of their ballet careers. They have to deal with pressure from parents and teachers as well as facing challenges in their first international ballet competitions in France and Sweden. To secure their career, succeeding in the audition for the Norwegian Ballet Academy, is of most importance. Out of the blue, Lukas is invited to the final audition at the Royal Ballet School in London. This is his opportunity of a lifetime. During the course of four years we see the boys become young men, and friends separate.

The music in Ballet Boys is composed by Henrik Skram and performed by F.A.M.E.S project, Macedonian Radio Symphonic Orchestra. The modern music is composed by Goran Obad. Ballet Boys is edited by Christoffer Heie, photographers are Torstein Nodland, Svend Even Hærra and Kenneth Elvebakk, sound mix by Bernt Syversen. The film poster is made by Daniel Barradas and photo by  Jörg Wiesner. Ballet Boys had its   in England in September 2014.   is the films sales agent worldwide.

==Awards and nominations==
* Winner of best youth film (For Tomorrow award) on  
* Childrens Jury 2nd Prize Documentary Feature Film on  
* Winner of best reportage on  
* Nominated for Best Original Music at the Norwegian TV Award "Gullruten" (Henrik Skram)
* Nominated for Best Original Score for a Documentary by the International Film Music Critics Association Award, IFMCA (Henrik Skram)
* Nominated for the award SOS Kinderdörfer weltweit on  

==Film festivals==
* Thessoloniki International Documentary Film Festival, Greece
* Eurodok - European Documentary Film Festival, Norway
* DOK.fest - München International Film Festival, Germany
* Festroia International Film Festival, Portugal
* Krakow Film Festival, Poland
* The Norwegian Short Film Festival
* Edinburgh International Film Festival, UK
* Scandinavian Film Festival, Australia
* Hong Kong International Film Festival
* Kristiansand Internatinal Children’s Film Festival, Norway
* Bergen International Film Festival, Norway
* Helsinki International Film Festival, Finland
* Reykjavik International Film Festival, Iceland
* São Paulo International Film Festival, Brazil 
* Chicago International Children’s Film Festival, U.S. 
* Nordische Filmtage Lübeck, Germany
* Trondheim Dokumentary Festival, Norway
* Oulu International Children’s and Youth Film Festival, Finland
* Cucalorus International Film Festival, U.S.
* IDFA, International Documentary Film Festival in Amsterdam
* Context Dance Festival, Moscow, Russia
* Dance On Camera, New York, U.S.
* Palm Springs International Film Festival, U.S.
* Göteborg International Film Festival, Sweden
* Northern Lights Film Festival, Japan
* EPOS, The International Art Film Festival, Tel Aviv
* Young About International Film Festival, Bologna, Italy
* International Festival of Films on Art, Montréal, Canada
* New York International Children’s Film Festival, U.S.

==External links==
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 


 