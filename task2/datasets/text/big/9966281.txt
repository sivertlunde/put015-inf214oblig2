Storm Rider Clash of the Evils
 
{{multiple issues|
 
 
}}

{{Infobox film name = Storm Rider Clash of the Evils image = Storm Rider Clash of the Evils poster.jpg alt =  caption =  traditional = 風雲決 simplified = 风云决 pinyin = Fēng Yún Jué jyutping = Fung1 Wan4 Kyut3}} director = Dante Lam producer =  writer = Ma Wing-shing starring = Richie Ren Nicholas Tse music =  cinematography =  editing =  studio = Puzzle Animation Studio Limited Shanghai Media Group distributor = Asia Animation Ltd. released =   runtime =  country = China language = Mandarin Cantonese budget =  gross = 
}}
Storm Rider Clash of the Evils is a Chinese animated feature film directed by Dante Lam and produced by Puzzle Animation Studio Limited and Shanghai Media Group. It is based on the manhua series Fung Wan by Ma Wing-shing.

==Plot==
The film is a spinoff of the original story and the two protagonists Wind and Cloud. The residents of Sword-Worshipping Manor, which houses the best sword-smiths in the world, are brutally massacred after they are alleged to be plotting a rebellion against the government. The young master of the manor, Ngou Kuet, is the only survivor. Ngou Kuet vows to finish forging the "Kuet" Sword, a task passed down by generations of his family which has yet to be completed. Ngou Kuet attacks Tin Ha Wui and battles with Wind and Cloud to obtain the blood of the Fire Kirin which can unleash the power of the sword. As the blood of the Fire Kirin runs in Winds veins, he becomes Ngou Kuets primary target.

==Voice cast==
{|class="wikitable"
|-style="background:white; color:black" align=center
|Role||Mandarin voice||Cantonese voice
|-
| Whispering Wind || Richie Ren || Hins Cheung
|-
| Striding Cloud || Nicholas Tse || Raymond Lam
|-
|}

==Production==
The first trailer for the film was released in 2006. It showed a battle between Cloud and Wind in a forest. They eventually show their true powers and escalate the fight to greater heights. They are then shown clashing on top of the Great Wall of China. Characters that are so far confirmed to be in this movie are Cloud, Wind, Duen Long, the Fire Kirin, Hung Ba, Nameless, Second Dream, Frost, Charity and Ngou Kuet.

The film was supposed to be released in the winter of 2006 but its release was delayed to July 2008.

==See also==
 
*The Storm Riders
*The Storm Warriors
*Wind and Cloud
*Wind and Cloud 2

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 