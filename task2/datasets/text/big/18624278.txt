The Dizzy Acrobat
{{Infobox Hollywood cartoon|
| cartoon_name        = The Dizzy Acrobat Woody Woodpecker
| image               = Dizzyacro01.jpg
| caption             =
| director            = Alex Lovy
| story_artist        = Ben Hardaway Milt Schaffer
| animator            = Emery Hawkins
| voice_actor         = Ben Hardaway
| musician            = Darrell Calker
| producer            = Walter Lantz
| studio              = Walter Lantz Productions Universal Pictures
| release_date        = May 21, 1943
| color_process       = Technicolor
| runtime             = 6 40"
| movie_language      = English
| preceded_by         = The Screwball
| followed_by         = Ration Bored
}}
 Universal Pictures.

==Plot== big top but a caretaker kicks him out. He says that if Woody wants to see the show, he will have to water the elephant. Woody attaches the elephant to a water spout and attempts again to enter the tent.
 perch pole, a lions cage and a bicycle.

==Academy Award== Academy Award Best Short Subject, Cartoons. It lost to MGMs The Yankee Doodle Mouse, the first of seven Tom and Jerry cartoons to win this award. It was the fifth film from Walter Lantz to be nominated in this category.
 

==Cultural references== Animal Fair" at the start of the cartoon.
*A sign indicates that the circuss Rubber Man is "Gone for the duration." This is a gag referencing the United States rationing of rubber during World War II.
*While Woody is on the trapeze, a variation of Johann Strauss IIs "Blue Danube Waltz" can be heard in the background score.

==Notes== drafted into the US Navy. The next Woody "cartune" he would direct was 1955s The Tree Medic. He does not receive on-screen credit as director.

==References==
 Cooke, Jon, Komorowski, Thad, Shakarian, Pietro, and Tatay, Jack. " ". The Walter Lantz Cartune Encyclopedia.

 
 
 
 
 
 