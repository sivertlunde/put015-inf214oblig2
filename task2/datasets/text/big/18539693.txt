Brother Martin: Servant of Jesus
{{Infobox film
| name           = Brother Martin: Servant of Jesus
| image          = Brothermartin.jpg
| image_size     =
| caption        = Poster art Spencer Williams
| producer       =  Spencer Williams
| narrator       = Spencer Williams
| music          =
| release        =
| cinematography =
| editing        =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| distributor    = Sack Amusement Enterprises
}} race film Spencer Williams. The film featured an all-black cast and was produced exclusively for exhibition in U.S. cinemas serving African American communities. It was among a number of religious-themed feature films created by Williams during the 1940s, who also wrote and directed The Blood of Jesus (1941) and Go Down, Death! (1944). 

==Preservation status==
No archive or private collection is known to have a print of Brother Martin: Servant of Jesus, and it is now believed to be a lost film. 

==See also==
*List of lost films

==References==
 

== External links ==
*  

 
 
 
 
 
 
 

 