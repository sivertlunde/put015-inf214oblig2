The Hidden Fortress
{{Infobox film
| name = The Hidden Fortress
| image = The Hidden Fortress poster.jpg
| caption = Original Japanese poster
| director = Akira Kurosawa
| producer = Sanezumi Fujimoto Akira Kurosawa
| writer = Shinobu Hashimoto Ryuzo Kikushima Akira Kurosawa Hideo Oguni Misa Uehara Minoru Chiaki Kamatari Fujiwara
| music = Masaru Sato
| cinematography = Kazuo Yamasaki
| editing = Akira Kurosawa Toho Studios Toho Company Ltd.
| released = December 28, 1958 (Japan) January 23, 1962 (USA) 
| runtime = 139 minutes; 90 minutes (1962 USA release) 
| country = Japan
| language = Japanese
| budget =
}} Misa Uehara as Princess Yuki.

==Plot==
The film begins with two bedraggled peasants, Tahei and Matashichi (Minoru Chiaki and Kamatari Fujiwara). Through conversation, they reveal that they had intended to fight alongside the Yamana clan, but turned up too late, were taken for soldiers of the defeated Akizuki clan, and forced to bury the dead. After quarreling and splitting up, the two are both captured again and forced to dig for gold in the Akizuki castle with other prisoners.
  Misa Uehara) and what remains of her familys gold to a secret territory. In order to keep her identity secret, Yuki poses as a mute. 

During the mission, the peasants impede it and sometimes try to seize the gold. They are later joined by a farmer’s daughter (Toshiko Higuchi), whom they acquire from a slave-trader. Eventually, they are captured and held by Rokurōtas rival, who later unexpectedly sides with the Princess and Rokurōta.

The peasants stumble upon the gold, but are later captured, whereupon Rokurōta explains Yukis true identity, and states that all of the gold has been used to restore her familys domain. The peasants are then dispatched, taking a single Ryō (Japanese coin)|ryō. In the final scene, Tahei gives this to Matashichi to protect; but Matashichi allows Tahei to keep it.

== Cast ==
*Toshiro Mifune - General Rokurota Makabe
*Minoru Chiaki - Tahei
*Kamatari Fujiwara - Matashichi
*Susumu Fujita - General Hyoe Tadokoro
*Takashi Shimura - The Old General, Izumi Nagakura Misa Uehara - Princess Yuki
*Eiko Miyoshi - Old Lady-in-Waiting
*Toshiko Higuchi - Farmers Daughter bought from slave trader
*Yū Fujiki - Barrier guard
*Yoshio Tsuchiya - Samurai on horse
*Kokuten Kōdō - Old man in front of sign

==Production==
This was Kurosawas first feature filmed in a widescreen format, Tohoscope, which he continued to use for the next decade. Hidden Fortress was originally presented with Perspecta directional sound, which was re-created for the Criterion DVD release.

In box-office terms, The Hidden Fortress was Kurosawa’s most successful film, until the 1961 release of  Yojimbo (film)|Yojimbo. 

==Critical reception==

Writing for The Criterion Collection in 1987, David Ehrenstein called it  "one of the greatest action-adventure films ever made" and  a "fast-paced, witty and visually stunning" samurai film ."   According to Ehrenstein:  Eisenstein or Gunga Din, The Thief The Tiger The Hindu Tomb. 

Writing for The Criterion Collection in 2001, Armond White said "The Hidden Fortress holds a place in cinema history comparable to John Fords Stagecoach (1939 film)|Stagecoach: It lays out the plot and characters of an on-the-road epic of self-discovery and heroic action. In a now-familiar fashion, Rokurōta and Princess Yuki fight their way to allied territory, accompanied by a scheming, greedy comic duo who get surprised by their own good fortune. Kurosawa always balances valor and greed, seriousness and humor, while depicting the misfortunes of war."   

Upon the films UK re-release in 2002, Jamie Russell, reviewing the film for the BBC, said it "effortlessly intertwines action, drama, and comedy", calling it "both cracking entertainment and a wonderful piece of cinema." 

==Awards==
    

==Influence==
 ,    particularly in the technique of telling the story from the perspective of the films lowliest characters,  .

==Remake==
 
A loose remake entitled Kakushi Toride no San-Akunin: The Last Princess was directed by Shinji Higuchi and released on May 10, 2008.

==References==
{{reflist|30em|refs=
   
   
   
   
}}

==External links==
 
*  
*  
*   at Rotten Tomatoes
*     at the Japanese Movie Database

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 