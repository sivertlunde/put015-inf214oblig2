C.I.D. (1990 film)
{{Infobox film
| name           =  C.I.D.
| image          = C.I.D.1990film.jpg
| caption        =
| director       = Ajay Goel
| producer       = 
| writer         = Suraj Sanim
| starring       = Vinod Khanna Amrita Singh Suresh Oberoi Juhi Chawla Aftab Shivdasani Kiran Kumar
| music          = Kalyanji Anandji
| cinematography = Chaman K. Bajoo
| editing        = Wamanrao
| production_company =
| distributor    =
| released       = 25 May 1990
| runtime        =
| country        = India
| awards         = Hindi
| budget         =
| gross          =
| website        =
}}
C.I.D. is a 1990 Bollywood film directed by Ajay Goel. The film stars Amrita Singh, Vinod Khanna, Suresh Oberoi, Juhi Chawla, Aftab Shivdasani, Shafi Inamdar, Alok Nath and Kiran Kumar. Film inspired from Hollywood flick Witness (1985 film).

==Plot==
C.I.D. Inspector Veer (Vinod Khanna) wages war against organized crime. His enemy is Roshan Lala (Kiran Kumar) the overlord of the drug and gold smuggling racket in India. So dangerous a man is Roshan Lala that no witness has ever dared to come forward to give evidence against him in court. Frustrated by the lack of evidence and witness needed to convict Roshan Lala, Inspector Veer manages to have a young undercover officer Raksha (Juhi Chawla) infiltrate Roshan Lalas organization. However she is soon discovered to be a Police officer and ruthlessly shot down by Roshan in a deserted street. Fatefully, this murder is witnessed by Mr. & Mrs. Saxena, a respectable middle-aged couple, whose only daughter Meghna is engaged to be married to Inspector Veer. Is Roshan Lala successful in his design? Does Inspector Veer convict Roshan Lala or does he lose his beloved forever?

==Cast==
*Vinod Khanna ...  Police Inspector Veer Sehgal
*Amrita Singh ...  Meghna Saxena
*Suresh Oberoi ...  Major Brijmohan Verma
*Juhi Chawla ...  Police Inspector Raksha Mehra
*Aftab Shivdasani... Sunny 
*Kiran Kumar ...  Roshan Lala
*Shafi Inamdar ...  Police Inspector Faizan
*Alok Nath ...  Police Commissioner

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Teri Na Na Na"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Pyas Dil Ki Bujha Do"
| Alisha Chinai
|-
| 3
| "Sapna Kahun Apna Kahun"
| Kishore Kumar
|-
| 4
| "Jeena Padega Tumhen"
| Amit Kumar, Alka Yagnik
|-
| 5
| "Jadu Mere Husna Ka"
| Jolly Mukherjee, Sapna Mukherjee
|}
== External links ==
*  

 
 
 
 
 