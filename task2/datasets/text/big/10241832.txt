The Rage (2007 film)

 
{{Infobox film
| name           = The Rage
| image          = TheRagePoster.jpg
| caption        = The Rage theatrical poster
| director       = Robert Kurtzman
| producer       = John Bisson Matt Jerrams Anne Kurtzman Gary Jones
| writer         = John Bisson Robert Kurtzman
| starring       = Andrew Divoff Erin Brown
| music          = Edward Douglas (Midnight Syndicate)
| cinematography = Robert Kurtzman
| editing        = Andrew Sagar
| distributor    = Screen Media Films
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}} rage virus in his laboratory in the woods.

The film stars Andrew Divoff and Erin Brown and was directed by Robert Kurtzman. It was first shown at the Fantasia Festival in Canada on July 13, 2007 and released on DVD by the independent company Screen Media Films on February 26, 2008.

The entire film is filmed in and around the town of Crestline, Ohio in the United States.

The music videos for Mushroomheads "12 Hundred" and "Damage Done" were filmed on the set, and are featured in the films DVD.

==Plot==
A mad scientist named Dr. Viktor Vasilienko (Andrew Divoff) is disillusioned with capitalist society and creates a virus that is designed to make people rage with anger. In his hidden laboratory in the woods, he begins testing the virus on innocents. His experiments dont go as planned and his infected victims escape into the wilderness. There, the infection spreads as vultures eat the remains of the test subjects and become out of control with the compulsion to eat human flesh.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-Debra Brownley || Sadie Jones
| Andrew Divoff || Dr. Viktor Vasilienko
|-
| Erin Brown || Kat
|-
| Reggie Bannister || Uncle Ben
|-
| Ryan Hooks || Josh
|-
| Rachel Scheer || Olivia
|-
| Sean Serino || Pris
|-
| Anthony Clark || Jay
|-
| Keith Herrick || Misfit/Dave
|-
| Alan Tuskes || Gor
|-
| Christopher Allen Nelson || Chris
|}

==Prequel==
 

A series of prequel comic books entitled Robert Kurtzmans Beneath The Valley of The Rage was released by The Scream Factory in April, 2008 in comics|2008.

==External links==
* 
*  
* 

 
 
 
 
 
 
 