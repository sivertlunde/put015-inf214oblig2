The Gladiator (1938 film)
{{Infobox film name           = The Gladiator image          = The Gladiator movie poster.jpg image_size     = caption        = director       = Edward Sedgwick producer  Edward Gross (assistant producer) writer         = Philip Wylie (novel The Gladiator) Arthur Sheekman Charlie Melson starring  Dickie Moore Robert Kent music          = Victor Young (uncredited) cinematography = George Schneiderman editing        = Robert O. Crandall (as Robert Crandall) distributor    = Columbia Pictures released       =   runtime        = 72 minutes language       = English country        = United States budget         = gross          =
}}
 comedy and fantasy film Dickie Moore and June Travis. The movie is an adaptation of Philip Gordon Wylies 1930 novel Gladiator (novel)|Gladiator, which is often credited with having influenced the creation of Superman. 

==Plot==
A man returns to college and is talked into joining the football team. He is a real joke on the team, until he is given a drug that gives him super strength. 

==Cast==
;Main cast
*Joe E. Brown - Hugo Kipp Dickie Moore - Bobby
*Man Mountain Dean - Himself
*June Travis - Iris Bennett
*Lucien Littlefield - Professor Danner
*Ethel Wales - Mrs. Danner Robert Kent - Tom Dixon Donald Douglas - Coach Robbins
;Uncredited appearances Richard Alexander - Tough Guy William Gould - Professor
*Harrison Greene - Trophy Giver/Jokester Sam Hayes - Announcer
*Eddie Kane - Speed Burns
*Marjorie Kane - Miss Taylor, Student
*Milton Kibbee - Assistant Coach
*Wright Kramer - Dr. DeRay
*Edward LeSaint - Committee Member Frank Mills - Man in Movie Audience
*Jack Mulhall - Spectator at Wrestling Match
*Lee Phelps - Coach Stetson
*Harry Semels - Hamburger Man John Shelton - Student Charles Sullivan - Football Fan Charles C. Wilson - Theatre Manager
*Robert Winkler

==Production==
The movie reached theatres two months after the publication of the first appearance of Superman in a comic book.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 