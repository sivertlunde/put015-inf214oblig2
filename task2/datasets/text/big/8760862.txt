Little Soldier Zhang Ga
{{Infobox film
| name = Little Soldier Zhang Ga
| image = ZhangGa.jpg
| caption =
| director =
| producer = Sun Lijun
| writer =
| starring =
| music =
| editing =
| distributor =
| released = September 2005
| runtime =
| rating =
| country = China
| awards = Mandarin
| budget =
| gross =
| followed by =
}}
 Chinese animated feature film from mainland China.  It is also referred to as "Zhang Ga, The Soldier Boy".  It is based on a true story.

==Background== RMB investment (about US $1.5 million), modified for younger audiences.   The production crew also consist of about 600 people.  This version aims to target audiences of all age groups.

The film was made in a collaboration between "Ai Yi Mei Xun Animation Production Company", an unnamed US-funded Chinese company in association with BTV, and the Youth Film Production Unit at Beijing Film Academy.  The students and faculties from the academy contributed to keep production costs down.  It is the first film created entirely with private investments.   In the cinemas, it was shown due to the 60th anniversary of Chinas War of Resistance Against Japanese in 2005. 

==Story==
The story is based on the backdrop of the Chinese Civil War and the Second Sino-Japanese War with character Zhang Ga in the middle of the chaos along with the Eighth Route Army.   The real story is based on the actual person Yan Xiufeng, whose childhood name was Gazi.  He was born in Baiyangdian in the Hebei Province.

==See also==
*List of animated feature-length films

==References==
 

==External links==
* 

 
 
 