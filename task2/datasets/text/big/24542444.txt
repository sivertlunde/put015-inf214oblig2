Nice Bombs
{{Infobox film
| name           = Nice Bombs
| image          = Nice Bombs.jpg
| caption        = 
| director       = Usama Alshaibi
| producer       = Kristie Alshaibi Ben Berkowitz Ben Redgrave
| writer         = 
| starring       = Usama Alshaibi Kristie Alshaibi
| music          = 
| cinematography = 
| editing        = Usama Alshaibi Amy Cargill Michael Palmerio
| distributor    = 7th Art Releasing
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
}} 2003 invasion of Iraq. The film is co-produced by Alshaibis wife Kristie Alshaibi and co-executive produced by Studs Terkel.

== Plot==
In January 2004, shortly after officially becoming an American citizen, the Iraqi-born filmmaker Usama Alshaibi travels to Baghdad to visit the family he hasnt seen in over two decades. He makes the trip with his wife, Kristie, in tow.

Although Saddam Hussein had been captured by American forces in December, Baghdad is in a state of extreme turmoil with massive explosions a daily, frightening occurrence. Yet, Usama and Kristie are surprised by his familys nonchalance at the chaos. When a bomb blows up and rocks the entire house, Usamas cousin Tareef refers to the explosive device as a "nice bomb," hence the films title.

== Release == Sundance Channel in March 2008. The DVD distributor Cinema Obscura released Nice Bombs on DVD on October 27, 2009.

== External links ==
*  
*  
*  
*  
* 
* 

 
 
 
 
 
 
 

 