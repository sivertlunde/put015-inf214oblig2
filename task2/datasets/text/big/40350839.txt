The Sword of Valor
 
{{Infobox film
| name           = The Sword of Valor
| image          =
| caption        =
| director       = Duke Worne
| producer       = Phil Goldstone
| writer         = Jefferson Moffitt
| based on       = original story by Julio Sabello
| starring       = Snowy Baker
| cinematography = Roland Price
| editing        =
| studio         = Goldstone Pictures
| released       =  
| runtime        =
| country        = United States
| language       = silent
| budget         =
}}
The Sword of Valor is a 1924 American film starring Snowy Baker as an American sailor who falls in love with the daughter of a Spanish nobleman. 

==Plot==
American sailor Captain Crooks (Baker) falls in love with Ynez Montego (Revier), daughter of Don Guzman de Ruis y Montejo (Lederer), who wants Ynez to marry the wealthy Eurasian, Ismid Matrouli (Cecil). 

Her father takes her to the Riviera where she is kidnapped by a deranged gypsy mountaineer and Crooks sets out to rescue her. He has to fight a leading swordsman.  

==Cast==
*Snowy Baker as Captain Grant Lee Cooks
*Otto Lederer as Don Guzma de Ruis y Montejo
*Edwin Cecil as Ismid Matrouli
*Dorothy Revier as Ynez

==Preservation status==
According to the SilentEra website, a print exists. 

==References==
 

==External links==
* 
* 
*  at TCMDB
*  at National Film and Sound Archive

 
 
 

 