Pusher 3
{{Infobox film
| name           = Pusher 3
| image          = Pusher_3.jpg
| caption        = Theatrical poster
| director       = Nicolas Winding Refn
| producer       = Johnny Andersen Henrik Danstrup Mikkel Berg Kim Magnusson Kenneth D. Plummer Rupert Preston
| writer         = Nicolas Winding Refn
| starring       = Zlatko Burić Marinela Dekić Slavko Labović Ilyas Agac Peter Peter Peter Kyed
| cinematography = Morten Søborg
| editing        = Miriam Nørgaard Anne Østerud
| studio         = Det Danske Filminstitut Nordisk Film TV2 Danmark Pusher III Ltd. NWR Film Productions
| distributor    = Nordisk Film   Magnolia Pictures  
| released       =  
| runtime        = 108 minutes
| country        = Denmark Danish Serbian Serbian
| budget         =
| gross          =
}} Danish crime film written and directed by Nicolas Winding Refn. It is the final film of the Pusher trilogy and released in August 2005.

==Plot== ecstasy pills. Albanian supplier Luan. The Albanians agree to send a new shipment of heroin and allow Milo to try to sell the ecstasy as well. 

After a quick talk with his demanding and spoiled daughter, Milena, Milo goes back to the kitchen at his club to cook for the party. After Milo forces his henchman to try his cooking, he meets his associate Little Muhammed, who has come to drop off his daily haul. The pugnacious Muhammed warns Milo to respect younger hoods like himself, calling himself "King of Copenhagen", but Milo mockingly calls him the "King Kong of Copenhagen." However, as Milo knows nothing about ecstasy, he needs Muhammed to set up a buyer for the pills. When all of Milos henchmen get food poisoning from his cooking, Milo has no choice but to trust Muhammed to make the sale alone and return within an hour.
 corrupt cop who promises to find him.
 Polish pimp arrive to sell a young girl into prostitution. Milo tries to distance himself from the transaction in disgust, but Rexho treats him as a subordinate, demanding food and drink and that he serves them. Rexho and the pimp attempt to sell the girl to Jeanette, a local brothel operator, but she refuses to take her, sensing she is likely under the age of 18. Milo gives the girl a piece of his daughters birthday cake after she reveals that it is her birthday as well. After Rexho leaves, the girl attempts to flee, but Milo helps run her down. The furious pimp begins savagely beating the girl, which ultimately sends Milo over the edge. In a rage he beats the pimp to death with a hammer, then waits for Rexho to return and kills him as well.  The corrupt cop then delivers Muhammed to Milo in the trunk of his car, warning him not to hurt him. 

Out of options Milo seeks help from his old friend and ex-henchman Radovan, who left the underworld to start a successful pizza restaurant. Radovan agrees to help Milo one last time. Radovan helps Milo torture Muhammed, who reveals that the ecstasy pills Milo had received were fake. Milo and Radovan stash Muhammed in a freezer after he threatens Milo, and then begin butchering the two corpses for disposal. At dawn, Milo returns to his tranquil home and talks with Milena. She wonders why he disappeared during the party, then goes to bed. Milo walks into the backyard and silently smokes a cigarette while looking into his empty swimming pool.

==Cast==
*Zlatko Burić as Milo: A middle-aged Serbian drug lord.
*Marinela Dekić as Milena: Milos strong-willed daughter.
*Ilyas Agac as Muhammed: An ambitious young drug-dealer.
*Vasilije Bojičić as Branko: Milos henchman.
*Kujtim Loki as Luan: Milos Albanian drug supplier.
*Ramadan Hyseni as Rexho: A weaselly Albanian gangster and Luans interpreter.
*Levino Jensen as Mike: Milenas boyfriend. Mike was one of the bodybuilders Frank robbed in the first film.
*Linse Kessler as Jeanette: A brothel operator. Jeanette was the ex-wife of The Duke from the second film, and previously a prostitute in the brothel when it was run by Kurt the Cunt.
*Slavko Labović as Radovan: Milos old friend and former henchman.
*Kurt Nielsen as Kurt the Cunt: A particularly loathsome drug dealer and pimp.
*Hakan Turan as Ali: Little Mohammeds partner.

==Critical response==

The film was critical acclaimed and holds a score of 92 % positive reviews on Rotten Tomatoes.  

==Trilogy==
 
This film is the final film in Pusher trilogy. The previous two movies take place in the same fictional Copenhagen underworld. The first film, Pusher (1996 film)|Pusher followed Frank (Kim Bodnia), a mid-level drug dealer who becomes indebted to Milo in a similar way that Milo later becomes indebted to the Albanians. Radovan appears in this film as Milos most trusted enforcer and admits to a desire to leave the underworld and open a restaurant, his dream having come true in Pusher 3. Milenas boyfriend Mike also appears as a drug-dealing bodybuilder who is robbed by Frank. Branko also appears as one of Milos other henchmen.

The second film, Pusher II, follows Franks estranged partner Tonny (Mads Mikkelsen) as he struggles with his relationship with his father and the prospect of becoming a father himself. Milo, Muhammed, and Jeanette appear in this film, and Kurt the Cunt has a larger role as Tonnys untrustworthy friend and the initial operator of the brothel which Jeanette will be in charge of in Pusher 3.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 