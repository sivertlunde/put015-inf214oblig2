Dragon Ball Z: The History of Trunks
{{Multiple issues|
 
 
}}

{{Infobox animanga/Header
| name            = Dragon Ball Z: The History of Trunks
| image           =  
| caption         = Region 1 VHS Cover
| ja_kanji        = ドラゴンボールZ 絶望への反抗!!残された超戦士・悟飯とトランクス
| ja_romaji       = Doragon Bōru Zetto Zetsubō e no Hankō!! Nokosareta Chō-Senshi•Gohan to Torankusu
| genre           = 
}}
{{Infobox animanga/Video
| type            = tv film
| director        = Yoshihiro Ueda
| producer        = 
| writer          = Akira Toriyama  (Original Story) 
| music           = 
| studio          = Toei Animation
| licensor        =   Madman Entertainment   Funimation
| network         = 
| network_en      =   Cartoon Network Nicktoons CBS 
| released        = February 24, 1993
| runtime         = 48 minutes
}}
  Goku dies Trunks tries to defeat the androids ravaging Earth.

==Plot== Gohan are Android #17 Android #18. With the death of Piccolo, Kami dies as well and the Dragon Balls are rendered permanently useless, making it impossible for the Z Fighters to be revived.

Thirteen years later, Gohan, now a Super Saiyan, repeatedly tries to challenge the androids, but they are too strong. He begins training Trunks (the son of Vegeta and Bulma), who is eager to defend the Earth. Gohan attempts to provoke Trunks enough to trigger his transformation into a Super Saiyan. Several times, Trunks comes close, but lacks enough motivation to maintain the form.

The androids attack an amusement park. Gohan transforms and battles them but is being overwhelmed. Trunks comes to his aid and fights Android #18 but is easily defeated. Gohan saves him and they hide in some debris. Unable to find their targets, the androids bomb the entire area and leave. Trunks and Gohan were able to survive but at the cost of Gohans left arm. They go home where he recovers and resume Trunks training.

Just as the training is finished, a huge explosion hits the city. Gohan pretends to agree to allow Trunks to join him in the battle, then knocks him unconscious and goes alone. Gohan, having received a  significant zenkai power up to Super Saiyan and puts up a very good fight against the androids, but his lost left arm proves to be a major disadvantage and he is eventually killed. Trunks awakens after sensing Gohans energy signal vanish, and hurries to the city to find Gohans body laying face down in a puddle of water. Trunks is enraged at the death of his best friend and undergoes his transformation into a Super Saiyan.

Three years later, Trunks and Bulma are working on a time machine when a warning sounds, indicating that androids are nearby. Trunks confronts them but is badly beaten and left for dead. He awakens in his house with his mother at his side, and finally decides they must use the time machine to deliver the medicine needed to cure Gokus heart disease years ago by giving it to Goku himself, hoping that this will prevent Gokus death and prevent their future from happening.

Trunks departs for the past in the time machine as the credits roll, showing scenes of Trunks battle with Mecha-Frieza, his encounter with Goku and the Z Fighters, and the awakening of Androids 17 and 18.

==Cast==
 
*Joji Yanami / Dale D. Kelly (Original), Kyle Hebert (Remastered) as Narrator Trunks
*Hiromi Tsuru / Tiffany Vollmer as Bulma Gohan
*Shigeru Android #17 Android #18
*Kōhei Miyauchi / Mike McFarland as Master Roshi Oolong
*Naoko Naoko Watanabe / Cynthia Cranz  as List of Dragon Ball characters#Secondary characters|Chi-Chi Naoko Watanabe Puar
*Daisuke Gōri / Mark Britten (Original), Kyle Hebert (Remastered)  as List of Dragon Ball characters#Secondary characters|Ox-King
*Naomi Nagasawa as Woman

==Music==

===Funimation Soundtrack===
The following songs were present in the English version of Dragon Ball Z: The History of Trunks, as well as its accompanying soundtrack CD, with exception to most of Dream Theaters music, "Home" being the only track showcased in the soundtrack from them and "Prelude" by Slaughter.  The soundtrack also contained remixes of other songs.

# Bootsy Collins with Buckethead - Shackler
# Neck Down - Garden of Grace
# Triprocket - Immigrant Song
# Dream Theater - Regression
# Dream Theater - Overture 1928
# Dream Theater - Fatal Tragedy
# Dream Theater - Through Her Eyes
# Dream Theater - Home
# Dream Theater - Dance of Eternity
# Dream Theater - Beyond This Life Slaughter -  Prelude

==Releases==
In the US, Dragon Ball Z: The History of Trunks was first released to VHS on October 24, 2000 in two formats, "Uncut" and "Edited". The uncut English version was released on DVD that same year, as well as the original Japanese version. On February 19, 2008 it was released as part of a remastered double feature DVD with   with some minor re-dubbings to the English vocal track. The same double feature was released on Blu-ray Disc|Blu-ray on July 15, 2008. The film was released to DVD again on September 15, 2009 in a remastered single-disc edition. Progressive metal band Dream Theaters album Scenes From A Memory is featured on the American version of the movie as the main soundtrack.

<!-- Is this really necessary ? the language links themselves should provide this info and the table is corrupted also: 
==In other Languages==
*Danish:  
*Swedish:  
*Spanish:  
*French:  
*Italian:  
*Portuguese:   -->

==Reception== Trunks are dub as the storyline sounded "completely alien" to the Japanese subtitles. For his final grade he noted "  Dub doesnt contain the original music...   isnt true to the original" but was pleased overall.   
 Funimation leaving the original Japanese soundtrack and the English voice dubs in, describing them as "enveloping". For fans of Dragon Ball he recommended watching the episode as it "worked a lot better than the average theatrical film since they follow DBZ continuity and expand the story while filling in details". Like Shepard the Sinnott review advised those who are not familiar to the Dragon Ball franchise to avoid the episode but that fans will enjoy it. In conclusion he felt the film was enjoyable at best.   

==References==
 

==External links==
 
 
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 