Lust for Life (film)
 
{{Infobox film
| name           = Lust for Life
| image          = Lust for Life DVD cover.PNG
| caption        = Lust for Life DVD cover
| director       = Vincente Minnelli George Cukor (uncredited - supervised one retake)
| producer       = John Houseman
| writer         = Irving Stone (novel) Norman Corwin
| starring       = Kirk Douglas Anthony Quinn James Donald
| music          = Miklós Rózsa
| cinematography = Russell Harlan
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       = September 17, 1956
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $3,227,000  . 
| gross = $2,695,000  
}} Dutch painter novel by Irving Stone and adapted by Norman Corwin.
 Pamela Brown, an Oscar for his performance as Van Goghs fast friend and rival Paul Gauguin. http://www.allmovie.com/work/lust-for-life-30494  

==Plot==
 , Self-Portrait, Summer 1887, Paris Van Gogh Museum, Amsterdam (F77v)]]
Vincent van Goghs obsessive devotion to his art engulfs, consumes, and finally destroys him. The apostate religious leaders do not like his zeal for God and they frown on his social activism and care for the poor in a coal mining town. He returns home to his fathers house where he is rejected by a woman he obsessively loves, takes up with a prostitute who leaves because he is too poor, and discovers painting, which he pursues while agonizing that his vision exceeds his ability to execute.  His brother, Theo van Gogh, provides financial and moral support, while Vincent lives off and on with the critical Paul Gauguin. Vincent begins experiencing hallucinations and seizures and voluntarily commits himself to a mental institution. He signs himself out, and with Theos help, returns to a rural area to paint, where he ultimately shoots himself in despair of never being able to put what he sees on canvas.  

==Cast==
  
* Kirk Douglas – Vincent van Gogh
* Anthony Quinn – Paul Gauguin Theo van Gogh Pamela Brown – Christine
* Everett Sloane – Dr. Gachet
* Henry Daniell – Theodorus van Gogh
* Madge Kennedy – Anna Cornelia van Gogh Noel Purcell – Anton Mauve
  Roulin
* Jill Bennett – Willemien
* Lionel Jeffries – Dr. Peyron
* Laurence Naismith – Dr. Bosman
* Eric Pohlmann – Colbert
* Jeanette Sterke – Kay
* Toni Gerry – Johanna
 

==Production==
Two hundred enlarged colour photos were used representing Vincent’s completed canvases; these were in addition to copies that were executed by an American art teacher, Robert Parker. In preparation for the film, Kirk Douglas practiced painting crows so that he could reasonably imitate van Gogh at work. 
 Academy Awards==
;Wins       Actor in a Supporting Role: Anthony Quinn

;Nominations Best Actor: Kirk Douglas
*  ,  , F. Keogh Gleason
*  

==Reception==
New York Times critic Bosley Crowther praised the films conception, acting and color scheme, noting the design team "consciously made the flow of color and the interplay of compositions and hues the most forceful devices for conveying a motion picture comprehension of van Gogh."  Variety said, "This is a slow-moving picture whose only action is in the dialog itself." 

==Historical accuracy==

While the film depicts van Gogh as committing suicide, some researchers today believe the Dutch painter was murdered.   

==Box office==
According to MGM records, the film earned $1,595,000 in the US and Canada and $1.1 elsewhere resulting in a loss of $2,072,000. 

==Companion short film==
MGM produced a   and showing the European locations used for the filming, to promote Lust for Life. In the film, a 75-year-old woman from Auvers-sur-Oise (not Jeanne Calment, who lived in Arles several hundred km to the south), who claims to have known Van Gogh when she was a young girl, meets star Kirk Douglas, and comments on how much he looks like the painter. This short promotional film is shown on Turner Classic Movies occasionally.
At the start and ending of the film, the creators list and thank a number of galleries, collectors, and historians who allowed the works of Van Gogh to be photographed for the film.

==See also==
* Death of Vincent van Gogh

==References==
 

==External links==
*  
*  
*  
*  , a short companion film


 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 