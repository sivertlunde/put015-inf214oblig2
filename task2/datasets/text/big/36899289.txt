Periya Manushan
{{Infobox film
| name           = Periya Manushan
| image          = 
| image_size     =
| caption        = 
| director       = Guru Dhanapal
| producer       = G. Seethalaxmi
| writer         = Guru Dhanapal
| starring       =   Deva
| cinematography = D. Shankar
| editing        = P. Sai Suresh
| distributor    =
| studio         = Guru Film International
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1997 Tamil Tamil comedy Deva and was released on 30 October 1997.    as one among Deepavali releases.

==Plot==

Ramakrishnan (Sathyaraj) has two wives. Sivagami (Ambika (actress)|Ambika), his first wife, is sterile while Parvathi (Kovai Sarala), his second wife, has a daughter, Indhu (Ravali). Indhu falls in love with, Ramakrishnans nephew, Subramani (Sathyaraj).

==Cast==

*Sathyaraj as Ramakrishnan / Subramani
*Ravali as Indhu
*Manivannan as Johnson Ambika as Sivagami
*Kovai Sarala as Parvathi
*Vadivelu as Chellappa
*Nizhalgal Ravi as Indhus cousin
*Vinu Chakravarthy as Indhus uncle
*Venniradai Moorthy
*Arivoli
*Alphonsa
*Sharmili
*Halwa Vasu
*Thalapathi Dinesh as Marimuthu
*Mayilsamy
*S. P. Balasubrahmanyam as a doctor

==Soundtrack==

{{Infobox Album |  
| Name        = Periya Manushan
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1997
| Recorded    = 1997 Feature film soundtrack |
| Length      = 25:36
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1997, features 5 tracks with lyrics written by Vaali (poet)|Vaali, Kalidasan and Pazhani Bharathi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Arabnha Ethampazhamey || Mano (singer)|Mano, K. S. Chithra || 5:25
|- 2 || Vaalapaari Chinna || Mano, Anuradha Sriram || 5:00
|- 3 || Hollywood Mudhal || Hariharan (singer)|Hariharan, K. S. Chithra || 4:53
|- 4 || Oh Mama || Mano, K. S. Chithra || 4:55
|- 5 || Sona Sona || Deva (music director)|Deva, K. J. Yesudas || 5:23
|}

==Reception==
The film did not well at the box office. 

==References==
 

 
 
 
 