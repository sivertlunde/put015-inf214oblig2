Mighty Lak a Goat
{{Infobox film
| name           = Mighty Lak a Goat
| image          = 
| image_size     = 
| caption        = 
| director       = Herbert Glazer
| producer       = Metro-Goldwyn-Mayer
| writer         = Hal Law Robert A. McGowan
| narrator       = 
| starring       = 
| music          = 
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 9 39"
| country        = United States
| language       = English
| budget         = 
}} short comedy film directed by Herbert Glazer.  It was the 209th Our Gang short (210th episode, 121st talking short, 122nd talking episode, and 41st MGM produced episode) that was released.

==Plot==
The gang tries to clean off their clothes after being splattered with mud accidentally by a passing motorist. A "miracle" cleaning solution devised by Billy "Froggy" Laughlin works beautifully, but with one major drawback: The stuff stinks to high heaven! Froggy tells the gang that they would get used to the smell. They do get used to the bad odor to the point of being oblivious to it. The kids manage to empty out a bus trying to board it.

They walk to school and get thrown out of the classroom due to their smell. Then, being free from school, the gang goes to see a movie called Dont Open That Door at the theater. The movie-house cashier notices their smell, but they head into the auditorium. Then even the actors on the screen cannot stand the smell and stop performing. They finally get removed from the theater and remove their clothes behind a tree.   

==Cast==
===The Gang=== Robert Blake as Mickey
* Billy Laughlin as Froggy
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* John Dilson as Banker Stone
* George B. French as Patron
* Ava Gardner as Girl at the theatre box office
* Robert Emmett OConnor as Detective King
* Anne ONeal as Schoolteacher
* William Tannen as Bus driver Joe Yule Sr. as Patron
* Lee Phelps as Police officer
* Charlie Sullivan as Bus passenger

==Notes== Robert Blake, Joe Yule, Sr. has a cameo, as a movie patron.
*The film they see is called Dont Open That Door. The scene in the movie shown on this film starred Robert Emmet OConnor, Banker Stone and John Dilson.

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 