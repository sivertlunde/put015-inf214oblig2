Mil Mascaras vs. the Aztec Mummy
{{Infobox film
| name           = Mil Mascaras vs. The Aztec Mummy (aka Mil Mascaras: Resurrection)
| image          = MMvsAM.jpg
| caption        = Official movie poster
| director       = Andrew Quint (Jeff Burr), Chip Gubera Chuck Williams
| writer         = Jeffrey Uhlmann Richard Lynch, Marco Lanzagorta, P. J. Soles
| music          = Vaughn Johnson
| cinematography = Tom Callaway
| editing        = Thom Calderon
| distributor    = Osmium Entertainment 
| released       =  
| runtime        = 90 minutes
| country        = United States 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Mil Mascaras vs. the Aztec Mummy (also known as Mil Mascaras: Resurrection) is a 2007 American lucha libre film starring the legendary Mexican wrestler and cult film star Mil Máscaras. It has the distinction of being the first lucha film starring any of the "Big 3" (Santo, Blue Demon, Mil Máscaras) to be produced in English.   

==Plot==
An Aztec mummy is resurrected in a ceremony in which the blood of a human sacrifice is dripped onto the mummified remains.  The mummy possesses a scepter with a jewel that can be used to control people’s minds for purposes of world conquest. Mil Máscaras learns of the mummy’s plans and is determined to thwart him.

When the scepter fails to do so, the mummy attempts to control Mils mind by exploiting the hallucinogenic effects of Aztec magic mushrooms and the allure of identical twin seductresses. Mil tracks the mummy to an abandoned monastery but is captured. After escaping with the help of the Professor, Mil is joined by a team of luchadores in a battle against the mummys warriors. Mil eventually defeats the mummy and entombs him forever.

==Cameos== Richard Lynch plays the President of the United States.   

==Critical Response==
 
The film screened at festivals around the world garnering awards and award nominations along with very positive critical reviews.    In particular, a review from the Cine Fantastico festival in Estepona, Spain, suggested that the film may be the best ever made in the traditionally Mexican lucha genre despite being produced in the United States.    Reviews from major film critics were similarly positive with PopMatters Bill Gibron calling it "amazing movie entertainment... rivals any super hero film made in the mainstream"    and M. J. Simpson giving it an A (the highest lettergrade).   

Similarly positive reviews accompanied the films theatrical release in 2009 with Fangoria Magazine giving it 3.5 skulls (out of 4).    In addition to film festival awards and nominations the film also received a Rondo Award nomination for Best Independent Film (Runner-Up) after its DVD release    and was featured in cover articles in Screem Magazine  and Filmfax. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 