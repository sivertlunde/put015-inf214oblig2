A Fistful of Death
 
{{Infobox film
| name           = A Fistful of Death
| image          =
| caption        =
| director       = Demofilo Fidani
| producer       = Demofilo Fidani
| writer         = Demofilo Fidani Alfredo Medori Mila Vitelli Valenza
| starring       = Klaus Kinski
| music          =Lallo Gori
| cinematography = Joe DAmato
| editing        = Piera Bruni
| distributor    =
| released       =  
| runtime        = 83 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
 Western film directed by Demofilo Fidani and starring Klaus Kinski.   

==Cast==
* Jack Betts as Butch Cassidy (as Hunt Powers)
* Klaus Kinski as Reverend Cotton
* Gordon Mitchell as Ironhead (Italian Version:Testa di ferro)
* Jeff Cameron as Macho Callaghan
* Giancarlo Prete as Sundance Kid (as Philip Garner)
* Benito Pacifico as Buck OSullivan (as Dennis Colt)
* Luciano Conti (as Lucky McMurray)
* Grazia Giuvi as Saloon-Girl
* Enzo Pulcrano as Member of gang (as Paul Crain)
* Pino Polidori (as Giuseppe Polidori)
* Pietro Fumelli
* Manlio Salvatori
* Alessandro Perrella
* Amerigo Leoni (as Custer Gail)
* Giglio Gigli
* Renzo Arbore (as Lorenzo Arbore)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 