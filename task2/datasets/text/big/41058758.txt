Entrance (film)
{{Infobox film
| image          = 
| alt            = 
| caption        = 
| director       = {{plainlist|
* Patrick Horvath
* Dallas Hallam
}}
| producer       = {{plainlist|
* Suziey Block
* Karen Gorham
* Dallas Hallam
* Patrick Horvath
* Michelle Margolis
}}
| writer         = {{plainlist|
* Karen Gorham
* Dallas Hallam
* Patrick Horvath
* Michelle Margolis
}}
| starring       = Suziey Block
| cinematography = Dallas Hallam
| editing        = {{plainlist|
* Dallas Hallam
* Patrick Horvath
}}
| studio         = Toward a New Cinema
| distributor    = IFC Midnight
| released       =   }}
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Entrance is a 2011 American independent film that mixes elements of mumblecore, psychological thrillers, and horror films.  It was directed by Dallas Hallam and Patrick Horvath and was written by Hallam, Horvath, Karen Gorham, and Michelle Margolis.  Suziey Block stars as a barista who lives a repetitive and anxious life in Los Angeles.  When her beloved dog disappears, she decides to give up and move back home, but first she invites all her friends to a going-away party.

== Plot ==
Suziey, a young barista, lives in Los Angeles with her roommate Karen.  Suzieys life has become repetitive, empty, and full of anxiety.  She feels threatened by passers-by when she walks down the street, and a car following her movements causes her to panic.  The sudden disappearance of her beloved dog pushes her to the breaking point, and she recruits Karen to help her search for him.  Unable to find him, she posts notices around the neighborhood, but they are to no avail.  Depressed and longing for a more fulfilling life, like that of her roommate, she agrees to hook up with a friend at a bar.  During the night, an unrevealed person takes a picture of her as she sleeps, and her friend makes an empty apology as he sneaks out the door in the morning.  When Karen returns from a fun vacation with her boyfriend, Suziey tells Karen that shes been unhappy for a long time and wishes to move back home.  Karen convinces Suziey to first hold a dinner party, and they invite all their friends, including Josh and Liesel, Suzieys friends from back home; Morgan, Karens boyfriend; and Jonathan, Karens co-worker.

At the party, Suziey becomes uneasy due to the awkward attention of Jonathan, though Suziey dismisses Jonathans concerns when he asks her if shes uncomfortable with his presence.  After talking and drinking champagne throughout the night, the drunk revelers are interrupted by a power outage.  The power briefly comes back on, only to flicker off again.  Suziey goes to investigate and is quickly incapacitated by a masked man.  When she regains consciousness, the masked man tells Suziey that he has been stalking her for a while, understands her pain, and says that they have something special, even though they do not love each other.  He then states that he is going to kill all of her friends so that they can be alone together.  In a nearly unbroken 20-minute shot, Suziey, who has been bound, stumbles through the house and discovers the grisly fate of each of her friends.  After unsuccessfully trying several avenues of escape, she crawls through a pet door and ends up on her porch, a dead end.  Cornered, she surrenders to her stalker, who cradles her head and comforts her as he forces her to share the view of the city with him.

== Cast ==
* Suziey Block as Suziey
* Joshua Grote as Josh
* Karen Gorham as Karen
* Liesel Kopp as Liesel
* Jonathan Margolis as Jonathan
* Bennet Jones as Bennet
* Florence Hartigan

== Themes ==
In an interview with Bloody Disgusting, co-directors Dallas Hallam and Patrick Horvath stated that horror is about amplification of common issues, which makes themes and metaphors easy to film. The protagonist, Suziey, experiences an amplification of all her problems in the climax.     The Dardenne brothers were a direct influence on Hallam and Horvath, and they attempted to blend that influence with mumblecore while introducing consequences through a horror element.     The events of the first hour are designed to be ambiguous; whether Suziey is paranoid or not is not answered by the film, and only her perceptions and reactions are shown.  Psychological thrillers from Dario Argento and Roman Polanski influenced the display of Suzieys mental state.  Isolation and vulnerability in the big city, especially from a female perspective, were major themes.  The filmmakers say that they did not initially realize that the themes were focused on female issues and perspectives.   

== Production ==
Hallam and Horvath had previously collaborated on Die-ner (Get It?).  The house in which they shot belongs to Karen Gorham; Dallas Hallam and his wife live in the basement.  For the long shot at the end, they rehearsed for a week and performed three takes.  Suziey Block was actually bound for her performance, as she found it distracting to accidentally become free of her bonds.  The film was shot in twelve days, split into two six-day weeks.   The films initial budget of $6000 necessitated certain restrictions, such as Suziey Blocks character working at the same job as the actress, a barista.   The idea to make a slasher film came from a failed project that Hallam had worked on; after successfully pitching the idea of a slasher to Horvath, the filmmakers decided to combine this idea with their love of European art films. 

== Release ==
Entrance premiered at the Los Angeles Film Festival on June 24, 2011.   It was picked up for distribution by IFC Midnight and received both a limited theatrical release and video on demand premiere on May 18, 2012. 

== Reception == Time Out Chicago rated the film 2/5 stars and criticized the film as another mumblecore production fixated on the existential angst of young, white, middle class protagonists. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 