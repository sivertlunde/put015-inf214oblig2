Morocco (film)
{{Infobox film
| name            = Morocco
| image           = Morocco1930.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Josef von Sternberg
| producer       = Hector Turnbull
| screenplay     = Jules Furthman
| based on       =  
| starring       = {{Plainlist|
* Gary Cooper
* Marlene Dietrich
* Adolphe Menjou
}}
| music          = Karl Hagos
| cinematography = Lee Garmes
| editing        = Sam Winston (uncredited)
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English, French, Spanish, Italian, Arabic
| budget         = 
| gross          = 
}}
Morocco  is a 1930 American romance drama film directed by Josef von Sternberg and starring Gary Cooper, Marlene Dietrich, and Adolphe Menjou.    Based on the novel Amy Jolly by Benno Vigny and adapted by Jules Furthman, the film is about a cabaret singer and a Legionnaire who fall in love during the Rif War, but their relationship is complicated by his womanizing and the appearance of a rich man who is also in love with her. The film is most famous for the scene in which Dietrich performs a song dressed in a mans tailcoat and kisses another woman (to the embarrassment of the latter), both of which were rather scandalous for the period. 
 Best Actress Best Art Best Cinematography Best Director (Josef von Sternberg).  In 1992, Morocco was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
The late 1920s. Wealthy La Bessière (Adolphe Menjou) tries to strike up an acquaintance with  disillusioned nightclub singer Amy Jolly (Marlene Dietrich) on a ship bound for the French protectorate in Morocco. Though polite, she later tears up and tosses away his calling card. They meet again at the nightclub where she is the headliner.

Also in the audience is appreciative ladies man, Légionnaire Private Tom Brown (Gary Cooper). During her first performance, after kissing a woman in the audience, Amy slips Tom her key. He takes her up on her offer that night, and they become acquainted. He discovers that she has become embittered with life and men, but, as they talk, she finds herself coming to like him. Unwilling to risk heartbreak once again, she asks him to leave before anything serious happens.

On the street, Tom encounters Adjudant Caesars wife (Eve Southern). It is clear that she has a past clandestine relationship with him, which she desires intensely to maintain. Her husband, Toms commanding officer, watches undetected from the shadows, as Tom rejects her. Meanwhile, Amy changes her mind and seeks Tom out. Madame Caesar then hires two street ruffians to attack the couple. Tom manages to seriously wound both, while he and Amy escape unscathed.

The next day, Tom is brought before Adjutant Caesar on the charge of injuring two allegedly harmless natives. Amy clears him, but Caesar makes him aware that he knows about Toms involvement with his wife. Tom is ordered to leave for Amalfi Pass with a detachment commanded by Caesar. He suspects that Caesar intends to rid himself of his romantic rival.

That night, Tom overhears La Bessière, who has been courting Amy all along, offer to marry her. Though he has fallen in love with her himself, Tom decides that she would be better off with a rich man than with a poor Legionnaire. When Tom hides the depth of his feelings for her before his departure, Amy eventually accepts La Bessières proposal.

On the march to Amalfi Pass, the detachment runs into a machine gun nest. Caesar orders Tom to deal with it, then decides to accompany him. Caesar is killed by the enemy.

Later, at their engagement party, La Bessière and Amy learn that whats left of Toms detachment has returned. Frantic, Amy rushes outside, but learns that Tom was wounded and left behind to recuperate in a hospital. She informs La Bessière that she must go to Tom that very night; wanting only her happiness, he drives her there.

She finds that he has not been injured at all. Because he feigned being wounded to avoid returning, he has been assigned to a new unit, which is about to march away into the desert. Amy is torn, but when she sees a handful of native women stubbornly following the Legionnaires they love, she joins them.

==Cast==
  and Marlene Dietrich]]
* Gary Cooper as Légionnaire Tom Brown
* Marlene Dietrich as Amy Jolly
* Adolphe Menjou as Kennington La Bessière Ullrich Haupt as Adjudant Caesar
* Francis McDonald as Caporal Tatoche
* Eve Southern as Madame Caesar
* Paul Porcasi as Lo Tinto, nightclub owner
* Emil Chautard as French general (uncredited)
* Juliette Compton as Anna Dolores (uncredited)
* Albert Conti as Colonel Quinnovieres (uncredited)
* Thomas A. Curran (uncredited)
* Theresa Harris as Camp Follower (uncredited)
* Harry Schultz as German Sergeant (uncredited) 
* Michael Visaroff as Colonel Barratire (uncredited) 

==Production==
According to Robert Osborne of Turner Classic Movies, Cooper and von Sternberg did not get along. Von Sternberg filmed so as to make Cooper look up at Dietrich, emphasizing her at his expense. Cooper complained to his studio bosses and got it stopped.

When Dietrich came to the US, von Sternberg welcomed her with gifts including a green Rolls-Royce Phantom II which featured in some scenes of Morocco.  

The final scene is recreated in the 1946s Mexican film Enamorada (1946 film)|Enamorada, directed by Emilio Fernández.

==Awards and nominations==
{| class="wikitable"  style="margin:auto; width:100%;"
|-
! style="width:20em;" | Award !! Category !! Nominee !! Outcome
|-
 1930  ||Morocco|| 
|-
 4th Academy Academy Award Best Director||Josef von Sternberg    Winner was Norman Taurog – Skippy (1931 film)|Skippy || 
|- Academy Award Best Actress||Marlene Dietrich    Winner was Marie Dressler – Min and Bill || 
|- Academy Award Best Art Hans Dreier    Winner was Max Rée – Cimarron (1931 film)|Cimarron || 
|- Academy Award Best Cinematography||Lee Garmes    Winner was Floyd Crosby – Tabu (1931 film)|Tabu || 
|- 1932 Kinema Kinema Junpo Best Foreign Josef von Sternberg|| 
|- National Film Narrative feature||Morocco|| 
|}

==References==
 

==External links==
 
*  
*  
*  
*   at Virtual History
*   on Lux Radio Theater: June 1, 1936. Radio adaptation of Morocco starring Clark Gable and Marlene Dietrich.

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 