Garm Hava
 
 
{{Infobox film
| name           = Garm Hava
| image          = Garm Hava.jpg
| image_size     =
| caption        =
| director       = M. S. Sathyu
| producer       = Abu Siwani Ishan Arya M. S. Sathyu
| writer         = Kaifi Azmi  Shama Zaidi 
| story          = Ismat Chughtai
| starring       = Balraj Sahni Farooq Shaikh Dinanath Zutshi Badar Begum Geeta Siddharth Shaukat Kaifi A. K. Hangal
| music          = Bahadur Khan Kaifi Azmi  (lyrics) 
| cinematography = Ishan Arya
| editing        = S. Chakravarty
| distributor    =
| released       =  
| runtime        = 146 minutes
| country        = India
| language       = Hindi/Urdu
| budget         = 
}}

Garm Hava ( ; translation: Hot Winds or Scorching Winds)  is a 1973 Hindi film|Hindi-Urdu drama film directed by M. S. Sathyu, with Balraj Sahni as the lead. It was written by Kaifi Azmi and Shama Zaidi, based on an unpublished short story by noted Urdu writer Ismat Chughtai. The film score was given by noted classical musician Ustad Bahadur Khan, with lyrics by Kaifi Azmi, it also featured a qawwali composed and performed by Aziz Ahmed Khan Warsi and his Warsi Brothers troupe.

Set in Agra, Uttar Pradesh, the film deals with the plight of a North Indian Muslim businessman and his family, in the period post partition of India in 1947. In the grim months, after the assassination of Mahatma Gandhi in 1948, films protagonist and patriarch of the family Salim Mirza, deals with the dilemma of whether to move to Pakistan as many of his relatives or stay back. The film details the slow disintegration of his family, and is one of the most poignant films made on Indias partition.   It remains one of the few serious films dealing with the post-Partition plight of Muslims in India.  

It is often credited with pioneering a new wave of  , 3 October 2005. 

==Plot== United Provinces of northern India (now the state of Uttar Pradesh). The story begins in the immediate aftermath of Indias independence and the partition of India in 1947. The family is headed by two brothers; Salim (Balraj Sahni), who heads the family business, and his elder brother Halim, who is mainly engaged in politics and is a major leader in the provincial branch of the All India Muslim League, which led the demand for the creation of a separate Muslim state of Pakistan. Salim has two sons, the elder Baqar, who helps him in the business, and Sikander (Farooq Shaikh), who is a young student. Halims son Kazim is engaged to Salims daughter, Amina. Although he had publicly promised to stay in India for the sake of its Muslims, Halim later decides to quietly emigrate to Pakistan with his wife and son, believing that there was no future for Muslims in India. Salim resists the notion of moving, believing that peace and harmony would return soon, besides which, he has to care for their ageing mother, who refuses to leave the house of her forefathers. This puts Kazim and Aminas marriage plans on hold, although Kazim promises to return soon to marry her. Halims stealthy migration affects Salims standing in the community. In the aftermath of partition, the sudden migration of many Muslims from Agra left banks and other lenders deeply reluctant to lend money to Muslim businessmen like Salim Mirza, who had previously been held in high esteem, over fears that they would leave the country without repaying the loan. Unable to raise capital to finance production, Salim Mirzas business suffers. Salim Mirzas brother-in-law, formerly a League supporter, now joins the ruling Indian National Congress in an attempt to get ahead in independent India while his son Shamshad unsuccessfully woos Amina, who is still devoted to Kazim and hopeful of his return.

Halims migration to Pakistan makes the family home an "evacuee property" as the house is in Halims name and Halim did not transfer it to Salim Mirza. The Indian government mandates the take over of the house, forcing Salim Mirzas family to move out of their ancestral home, which is very hard on Mirzas aged mother. Salims wife blames him for not raising this issue with his brother Halim before he left for Pakistan. Mirza resists his wifes hints that they also move to Pakistan and his elder sons calls for modernising the family business. Mirza finds it difficult to rent a house, facing discrimination owing to his religion and fears that a Muslim family would skip out on rent if they decided to leave for Pakistan. He finally succeeds in finding a smaller house to rent, but his business is failing and despite his sons exhorting, refuses to change with the times, believing that Allah would protect them. Salim Mirzas passiveness and disconnection from the outside world leaves his wife and son frustrated. The Mirza family house is bought by a close business associate, Ajmani, (A.K. Hangal) who respects Mirza and tries to help him. Despite growing troubles, the family is briefly buoyed by Sikanders graduation from college.

Amina and her family have almost given up on her marrying Kazim after Halim breaks his promise to return soon from Pakistan. Kazim returns on his own, and reveals that his father had become opposed to his marrying Amina, preferring that he marry the daughter of a Pakistani politician.  Having received a scholarship from the Government of Pakistan to study in Canada, Kazim desires to marry Amina before he leaves, but before the marriage can take place, he is arrested by police and repatriated to Pakistan for travelling without a passport and not registering at the police station, as is required of all citizens of Pakistan. Amina is heart-broken, and finally accepts Shamshads courtship. Sikander undergoes a long string of unsuccessful job interviews, where the interviewers repeatedly suggest that he would have better luck in Pakistan. Sikander and his group of friends become disillusioned and start an agitation against unemployment and discrimination, but Salim prohibits Sikander from taking part. Despite his political connections, Salim Mirzas brother-in-law ends up in debt over shady business practices and decides to flee to Pakistan. Amina again faces the prospect of losing her lover, but Shamshad promises to return and not leave her like Kazim. Salim Mirzas reluctance to modernise and cultivate ties with the newly formed shoemakers union results in his business not receiving patronage and consequently failing. Disillusioned, his son Baqar decides to migrate to Pakistan with his son and wife. Salims aged mother suffers a stroke, and through his friend, Salim is able to bring his mother to her beloved house for a final visit, where she dies. While Salim is travelling in a horse-drawn carriage, the carriage driver, a Muslim, gets into an accident and a squabble with other locals. The situation deteriorates into a riot, and Salim is hit by a stone and suffers injuries. With his business and elder son gone, Salim begins to work as a humble shoemaker to make a living. Shamshads mother returns from Pakistan for a visit, leading Amina and her mother to think that Shamshad would also come soon and their marriage would take place. However, Shamshads mother merely takes advantage of Salim Mirzas connections to release some of her husbands money, and reveals that Shamshads marriage has been arranged with the daughter of a well-connected Pakistani family. Shattered with this second betrayal, Amina commits suicide, which devastates the whole family.

Amidst these problems, Salim Mirza is investigated by the police on charges of espionage over his sending plans of their former property to his brother in Karachi, Pakistan. Although acquitted by the court, Mirza is shunned in public and faces a humiliating whisper campaign. Mirzas long aversion to leaving India finally breaks down and he decides in anger to leave for Pakistan. Sikander opposes the idea, arguing that they should not run away from India, but fight against the odds for the betterment of the whole nation, but Salim decides to leave anyway. However, as the family is travelling towards the railway station, they encounter a large crowd of protestors marching against unemployment and discrimination, which Sikander had planned to join. Sikanders friends call out to him, and Salim encourages him to join the protestors. Instructing the carriage driver to take his wife back to their house, and the film ends as Salim Mirza himself joins the protest, ending his isolation from the new reality.
==Cast==
* Balraj Sahni – Salim Mirza
* A. K. Hangal – Ajmani Sahab
* Gita Siddharth  – Amina Mirza
* Farooq Shaikh – Sikander Mirza
* Dinanath Zutshi – Halim
* Badar Begum – Salims mother
* Shaukat Azmi (Kaifi) –  Jamila, Salim Mirzas wife 
* Vikas Anand
* Abu Siwani – Baqar Mirza
* Jalal Agha – Shamshad
* Yunus Parvez
* Jamal Hashmi – Kazim
* Rajendra Raghuvanshi –  Salim Mirzas Driver

==Production==

The film was based on an unpublished short story by writer-screenwriter Ismat Chughtai and later adapted by Kaifi Azmi and Shama Zaidi.    Chugtai narrated the story to Sathyu and his wife Zaidi, deriving from the struggles of her own relatives during the Partition before some of them migrated for Pakistan. While developing the screenplay, poet-lyricist Azmi added his own experiences of Agra and the local leather industry. Later, he also wrote in the dialogues.   
 controversial theme, National Film Development Corporation (NFDC), stepped in later with a funding of Rs 250,000.  Sathyu borrowed the rest 750,000 of the budget from friends.   The film was co-produced and shot by Ishan Arya, who after making ad film made his feature film debut, using an Arriflex camera, loaned by Homi Sethna, Sathyus friend. As Sathyu couldnt afford recording equipment, the film was shot silent, and the location sounds and voices were dubbed in post-production. Shama Zaidi also doubled up as the costume and production designer. 

Sathyu had long been associated with leftist Indian Peoples Theatre Association (Indian Peoples Theatre Association|IPTA), thus most roles in the film were played by stage actors from IPTA troupes in Delhi, Mumbai and Agra. The role of family patriarch, Salim Mirza was played by Balraj Sahni, also known to Sathyu through IPTA, and for whom this was to be his last important film role, and according to many his finest performance.  The role his wife was play by Shaukat Azmi, wife of films writer Kaifi Azmi, and also associated with IPTA. Farooq Shaikh, a law student in Mumbai, till then had done small roles in IPTA plays, made his film debut with the role of Salim Mirza.  The role of Balraj Sahnis mother was first offered to noted singer Begum Akhtar which she refused,  later Badar Begum played the role. The locale of Mirza mansion was an old haveli of R S Lal Mathur in Peepal Mandi who helped the whole unit throughout the shooting. Mathur helped Sathyu find Badar Begum in a city brothel. Badar Begum was then in her 70s and almost blind due to cataract. However, when she was sixteen years old, she ran away to Bombay to work in Hindi films, but soon ran out of money and only managed to get work as an extra in a Wadia Movietone film. She used the money to return to Agra, eventually ended up in the red-light area of the city and ran a brothel in the area. Her voice was later dubbed in by actress Dina Pathak.     Films lead, Balraj Sahni however died the day after he finished dubbing for the film.  The soundtrack included a qawwali "Maula Salim Chishti" by Aziz Ahmed Khan Warsi, of Warsi Brothers. 

==Themes and allusions==
The title alludes to the scorching winds of communalism, political bigotry and intolerance, that blew away humanity and conscience from across North-India in the years after the partition of India in 1947, and especially after the assassination of Mahatma Gandhi to the which the film opens. In its prologue, poet Kaifi Azmi narrates a couplet summing up the theme, "Geeta ki koi sunta na Koran ki sunta, hairan sa eemaan vahan bhi tha yahan bhi" (Nobody listens to Gita or Quran, shocked conscience was here as well as there.)  Just like his ageing mother is reluctant to leave the ancestral haveli where she came a young bride, her son Salim Mirza, the protagonist is also holding on to his faith in new India. Despite the fact, his shoe manufacturing business is suffering in the new communally charged environment, and the family had to sell off their haveli to move into a rented house. Yet, he struggles to keep his faith in secularism and idealism alive, along with his optimistic son. 

==Release and reception==

Prior to its release the film was held by  .  

The film first opened at two theatres Sagar and Sangeeth in Bangalore. Positive response at these theatres paved way for a subsequent nationwide release. The Indian premiere was held at Regal Cinema in Colaba, Mumbai in April 1974. However, prior to this Bal Thackeray, head of Shiva Sena had threatened to burn down the cinema, if the premier was allowed, calling it pro-Muslim and anti-India film. On the day of the premiere, Thackeray was persuaded to attend a special screening of the film in the afternoon, and allowed the film to be screened. Subsequently, the film had a limited pan-India release.    {{cite web | title = Garam Hava to hit screens after four decades – The Hindu | url = http://www.thehindu.com/news/cities/bangalore/garam-hava-to-hit-screens-after-four-decades/article6594640.ece  National Film Awards, it was awarded the Nargis Dutt Award for Best Feature Film on National Integration.

Today it is noted for its sensitive handling of the  s Chhalia (1960), Yash Chopras Dharmputra (1961), Govind Nihalanis Tamas (film)|Tamas (1986), Pamela Rooks Train to Pakistan (1998), Manoj Punjs Shaheed-e-Mohabbat Boota Singh (1999) and Chandra Prakash Dwivedis Pinjar (film)|Pinjar (2003).

==Restoration and re-release==
In 2009, a privately funded restoration work of the film started at Cameo Studios in Pune.  Subsequently, the restoration budget climbed to over Rs 10&nbsp;million, and restoration work was done by Filmlab, Mumbai and the sound quality enhancement by Deluxe Laboratories in Los Angeles, US.  The restoration process, which included restoration of original soundtrack took over three years to complete and the print was re-released on 14 November 2014 across 70 screens in eight metro cities in India.    

==Awards==

===Academy Awards=== Indian submission for the Academy Award for Best Foreign Language Film 

===Cannes Film Festival===
* 1974:   – Nominated for "In Competition" section.   

===National Film Awards===
* 1974: Nargis Dutt Award for Best Feature Film on National Integration   

===Filmfare Awards===
* 1975: Filmfare Best Dialogue Award- Kaifi Azmi
* 1975: Filmfare Best Screenplay Award- Kaifi Azmi, Shama Zaidi 
* 1975: Filmfare Best Story Award- Ismet Chugtai

==Bibliography==
* Three Hindi Film Scripts, by Kafi Azmi and Shama Zaidi, 1974.
*   Our Films, Their Films, by Satyajit Ray, Orient Longman, 2005. ISBN 81-250-1565-5.Page 100-102.
*   Limiting Secularism: The Ethics of Coexistence in Indian Literature and Film, by Priya Kumar, University of Minnesota Press, 2008. ISBN 0-8166-5072-1. Page 186-187.

==See also==
* List of submissions to the 47th Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
 
*   at Upperstall
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 