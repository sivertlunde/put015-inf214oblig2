Senorita (film)
{{infobox film
| name           = Señorita
| image          =
| imagesize      =
| caption        =
| director       = Clarence Badger
| producer       = Adolph Zukor Jesse Lasky B. P. Schulberg (associate producer)
| writer         = Lloyd Corrigan (scenario)  Robert Hopkins (intertitles)
| screenplay     = John McDermott
| story          = John McDermott
| starring       = Bebe Daniels
| music          = William Marshall
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 mins.
| country        = United States
| language       = Silent English intertitles
}}
 1927 American silent Comedy action comedy film directed by Clarence Badger, and starring Bebe Daniels.  Two prints of the film still exist; one is held in a private collection and another is reportedly in Belgium containing French intertitles.  

==Cast==
* Bebe Daniels - Señorita Francesca Hernandez James Hall - Roger Oliveros
* William Powell - Ramon or Manuel Oliveros
* Josef Swickard - Don Francisco Hernandez Tom Kennedy - Oliveros Gaucho (uncredited)
* Jerry Mandy - Juean - Hernandez Gaucho (uncredited)
* Raoul Paoli - Jose - Hernandez Foreman (uncredited)
* Pedro Regas - Hernandez Gaucho (uncredited)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 