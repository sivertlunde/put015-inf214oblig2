Big Animal
{{Infobox film
| name           = Big Animal
| image          = Big animal (movie poster).jpg
| caption        = 
| director       = Jerzy Stuhr
| producer       = Sławomir Rogowski
| writer         = Screenplay:  
| starring       = Anna Dymna Jerzy Stuhr Dominika Bednarczyk Andrzej Franczyk Stanisław Banaś Krzysztof Gluchowski
| music          = Abel Korzeniowski
| cinematography = Paweł Edelman
| editing        = Elżbieta Kurkowska Oscilloscope Milestone Films
| released       =   (Poland) 2004 (US)
| runtime        = 73 minutes
| country        = Poland
| language       = Polish
| budget         = 
| gross          = 
}}

Big Animal ( ) is a 2000 Polish film directed by Jerzy Stuhr from a screenplay by Krzysztof Kieślowski, based on a short story by Kazimierz Orłoś.     

==Plot==
Mr. Zygmunt Sawicki is a bank employee, who finds a camel in his yard one day. He decides to take charge of it and he and his wife Marysia take care of it. However, problems arise for both his fellow town-dwellers and the local authorities.

==Cast==
* Jerzy Stuhr as Zygmunt Sawicki
* Anna Dymna as Marysia Sawicka
* Andrzej Franczyk as Bank Manager
* Dominika Bednarczyk as Bank Clerk 1
* Błażej Wójcik as Bank Clerk 2
* Stanisław Banaś as Fire Chief
* Krzysztof Gluchowski as Mayor
* Feliks Szajnert as Drunkard

==Production==
The film was shot in the following locations: Warsaw, Tymbark, Myślenice, Rabka.

==Awards==
The film won the Special Jury Prize at the 2000 Karlovy Vary International Film Festival.     

==Home media==
After distributing 35mm prints to theaters, Milestone Films released the film on DVD in 2003. 

==Festivals== Los Angeles International Film Festival (International Competition) October 19–26, 2000. 
* Karlovy Vary International Film Festival (in competition) July 5–15, 2000. 
* London Film Festival (Cinema Europa) November 1–16, 2000. 
* San Francisco International Film Festival April 19 - May 3, 2001. 
* Sydney Film Festival June 8–22, 2001. 

==References==
 

==External links==
*  
*  
*  at Milestone Films

 

 
 
 
 
 