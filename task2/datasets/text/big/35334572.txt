Dark Girls
{{Infobox film
| name           = Dark Girls
| image          =
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = D. Channsin Berry & Bill Duke 
| producer       = D. Channsin Berry & Bill Duke 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

  colorism based on skin tone among African Americans, a subject still considered taboo by many black Americans. The film contains interviews with notable African Americans including Viola Davis. It also reports on a new version of the 1940s black doll experiment by Kenneth and Mamie Clark, which proved that black children had internalized racism by having children select a white or a black doll (they typically chose white) based on questions asked. In the updated version, black children favored light-skinned dolls over dark-skinned dolls. 

==Scenes==
Dark Girls explores the many struggles, including self-esteem issues, which women of darker skin face allowing women of all ages recount "the damage done to their self-esteem and their constant feeling of being devalued and disregarded.”(Haque)   

Duke and Berry even take it a step farther and interview African American men who claim they could not date a woman of dark skin. One young man interviewed saying “They   look funny beside me.” 

==Other aspects of documentary==
The documentary takes a look into the trend of black women all over the world investing in the multibillion dollar business of skin bleaching creams. Duke and Berry also examine how black women are trying to look more Caucasian while white women are trying to look more ethnic. “White women are risking skin cancer and tanning booths twice a week, Botoxing their lips, getting butt lifts to look more ethnic and crinkling up their hair.” 

==Reactions==

The film was shown to a sell-out crowd at the Pan African Film Festival in Los Angeles, April 2012. Interviewed on NPR, Duke recounted an reaction he received at another showing, which indicated that colorism is not easily discussed and was asked by someone, Why are you airing our dirty laundry? His answer was, "Because its stinkin up the house!"   

== References ==
 

==See also==
*A Girl Like Me (film)

==External links==
* 

 
 
 
 
 