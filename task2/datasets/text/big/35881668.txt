The White Roses of Ravensberg (1929 film)
{{Infobox film
| name           = The White Roses of Ravensberg 
| image          = 
| image_size     = 
| caption        = 
| director       = Rudolf Meinert
| producer       = 
| writer         = Eufemia von Adlersfeld-Ballestrem (novel)   Emanuel Alfieri   Johannes Brandt
| narrator       = 
| starring       = Diana Karenne   Viola Garden  Jack Trevor   Walter Janssen
| music          = Walter Winnig
| editing        = 
| cinematography =  Nicolas Farkas
| studio         = Omnia-Film
| distributor    = Deutsch-Russische Film-Allianz
| released       = 1929
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent drama The White made into a film in 1919.

==Cast==
* Diana Karenne - Maria von Ravensberg 
* Viola Garden - Sigrid von Erlenstein 
* Jack Trevor - Dr. Marcel Hochwald 
* Walter Janssen - Graf von Erlenstein 
* Dolly Davis - Iris von Ravensberg 
* Luigi Serventi - von Kurla 
* Willi Forst - Boris 
* Emil Heyse - Jacob 
* John Mylong - Andreas, der Gärtner

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2007.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 