Roommates (1995 film)
{{Infobox film
| name           = Roommates
| image          = Roommates.jpg
| caption        = Promotional poster
| director       = Peter Yates
| producer       = Ted Field Robert W. Cort Scott Kroopf
| screenplay     = Max Apple Stephen Metcalfe
| story          = Max Apple
| starring       = Peter Falk D. B. Sweeney Julianne Moore Jan Rubeš Frankie Faison
| music          = Elmer Bernstein Mike Southon
| editing        = Seth Flaum John Tintori
| studio         = Hollywood Pictures Interscope Communications PolyGram Filmed Entertainment Nomura Babcock & Brown Buena Vista Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $22 million
| gross          = $12,096,881
}} 1995 United American comedy-drama film, starring Peter Falk, D.B. Sweeney and Julianne Moore, directed by Peter Yates. The original music score was composed by Elmer Bernstein. The film was marketed with the tagline "Some people talk. Some people listen. When youre 107 and going strong, you do whatever you want."

==Filming details== Admissions House, Old Main.  

The aging of Rocky (the Peter Falk character) through creative make-up earned the picture an Academy Award nomination.

==Plot== American baker who insists, despite relatives protests, upon adopting his young grandson Michael when the boys parents pass away. Twenty years later, Michael is a medical student in Columbus whos forced to take his still-spry grandfather when the old man is evicted from his apartment building. Although the crusty, outspoken Rocky gets along with his Chinese college roommates, he is less enthused about his grandsons girlfriend, Beth. Eventually, Michael and Beth marry and head to Pittsburgh where Michael begins his medical residency, while Rocky continues working as a baker.  An illness forces Rocky to move back to Pittsburgh with his grandson and his wife, and Rocky warms up to Beth.  Seven years pass, and Rocky lives with Michael and Beth and their two children, as Michael has built himself a prominent medical career.  However, when Beth is killed in an automobile accident, the old man once again comes to support his grandson in his time of need.  At the end of the film, Rocky dies at the age of 107, knowing that his grandson is well, and that he has provided all the care that he could for him.

==Main cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Peter Falk || Rocky Holzeck
|-
| D. B. Sweeney || Michael Holzcek
|-
| Julianne Moore || Beth Holzeck
|-
| Ellen Burstyn || Judith
|-
| Jan Rubes || Bolek Krupa
|-
| Ernie Sabella || Stash
|- John Cunningham || Burt Shook
|-
| Frankie Faison || Professor Martin
|-
| Raymond K. Wong || Deng
|-
| William H. Macy || Doctor (uncredited cameo)
|-

|}

==Honors==
The film was nominated for the Best Make-up Academy Award (Greg Cannom, Robert Laden, Colleen Callaghan), and was nominated for the Best Young Actress Award, Young Artist Awards (Courtney Chase).

==External links==
* 
* 
* 

==References==
 
 

 
 
 
 
 
 
 
 
 
 
 