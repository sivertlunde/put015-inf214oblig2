Martin Lawrence Live: Runteldat
{{Infobox film
| name = Martin Lawrence Live: Runteldat
| image = Martin lawrence live runteldat.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = David Raynr
| producer = David Gale Beth Hubbard Michael Hubbard Loretha C. Jones
| writer = Martin Lawrence
| starring = Martin Lawrence
| cinematography = Daryn Okada
| editing = Nicholas Eliopoulos Runteldat Entertainment
| distributor = Paramount Pictures
| released =  
| runtime = 113 minutes
| country = United States
| language = English
| budget = $3 million   
| gross = $19,184,820 
}} Whatever It Takes. Lawrence also has producing and writing credits for the film. It is Lawrences second stand up comedy film after You So Crazy was released in 1994.

Shot on location at the DAR Constitution Hall in Washington D.C., the film was released in August 2002 and went on to gross nearly $20 million at the box office, almost seven times its production cost of $3 million.

After a number of personal crises, Lawrence returns to the stage telling about his stinging social commentary and very personal reflections about his life.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 