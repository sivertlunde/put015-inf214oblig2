Kunjiramayanam
{{Infobox film
| name           = Kunjiramayanam
| image          =Kunjiramayanam_Teaser_Poster.jpg
| alt            = 
| caption        =Teaser Poster
| director       = Basil Joseph
| producer       = Suvin K Varkey
| screenplay     = Deepu Pradeep & Basil Joseph
| story          = Deepu Pradeep
| starring       = Vineeth Sreenivasan Dhyan Sreenivasan Aju Varghese  
| music          = Justin Prabhakaran
| cinematography = Vishnu Sharma
| distributor    = E4E Entertainment
| editing        = Appu Bhattathiri
| studio         = 
| released       = 
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}

Kunjiramayanam is an upcoming 2015 Malayalam language Indian comedy film directed by Basil Joseph starring Vineeth Sreenivasan, Dhyan Sreenivasan and Aju Varghese in the lead roles.  The movie will be the debut of director Basil Joseph who is popular for his short films Oru Thundupadam and Priyamvadha Katharayano. An interesting aspect of the movie is the actor siblings Vineeth Sreenivasan and Dhyan Sreenivasan are sharing screen for the first time in their career
. 

==Plot==
The movie narrates the story of Kunjiraman (Vineeth) who lives in a remote, old world village in Kerala, surrounded by greenery, myths and legends.

==Cast==
* Vineeth Sreenivasan as Kunjiraman
* Dhyan Sreenivasan
* Aju Varghese
* Neeraj Madhav Kalpana
*

==Music==
The music for the movie is by Justin Prabhakaran and lyrics are being penned by Manu Manjith. 

== Production ==
;Filming
The film shooting has started on April 23,2015 at Kollengode,Palakkad and is currently in progress.

==References==
 

 
 
 
 

 