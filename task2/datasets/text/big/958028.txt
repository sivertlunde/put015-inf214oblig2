To Hell and Back (film)
{{Infobox film
| name           = To Hell and Back
| image          = To-Hell-and-Back-Poster.jpg
| caption        = Film poster by Reynold Brown
| writer         = {{plainlist|
* Gil Doud
* Audie Murphy 
}}
| starring       = {{plainlist|
* Audie Murphy
* Marshall Thompson
* Charles Drake
}}
| director       = Jesse Hibbs
| producer       = Aaron Rosenberg
| narrator       = General Walter Bedell Smith
| cinematography = Maury Gertsman
| music          = {{plainlist|
* Irving Gertz
* William Lava
* Henry Mancini
* Lou Maury
}}
| editing        = Edward Curtiss
| studio         = Universal-International
| distributor    = Universal Studios#Universal-International|Universal-International
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| gross          = $6 million (US / Canada rentals)  
}}
 ghostwritten by his friend, David "Spec" McClure, who served in the Armys Signal Corps during World War II. 

==Plot== sharecropper family in Texas. His father deserts them around 1939–40, leaving his mother (Mary Field) barely able to feed her nine children. As the eldest son, Murphy works from an early age to help support his siblings, and when his mother dies in 1941 he becomes head of the family. His brothers and sisters are sent to an elder sister, Corrine, to whom Murphy sends his GI allotment pay.
 3rd Infantry Division in North Africa, as a replacement. Because of his youthful looks, he endures jokes about "infants" being sent into combat.
 Jack Kelly).

The action for which Murphy was awarded the Medal of Honor is depicted near the end of the film. In January 1945, near Holtzwihr, France, Murphys company is forced to retreat in the face of a fierce German attack. However, Murphy remains behind, at the edge of a forest, to direct artillery fire on the advancing enemy infantry and armor. As the Germans close on his position, Murphy jumps onto an abandoned M4 Sherman tank (he actually performed this action atop an M10 tank destroyer) and uses its .50-caliber machine gun to hold the enemy at bay, even though the vehicle is on fire and may explode at any moment. Although wounded and dangerously exposed to enemy fire, Murphy single-handedly turns back the German attack, thereby saving his company. While the film depicts this action as having taken place in balmy weather and good visibility in rolling terrain, it actually took place in the bitter winter of 1945, at the edge of a forest on the flat Alsatian Plain in conditions of poor visibility. After a period of hospitalization, he is returned to duty. The film concludes with Murphys Medal of Honor ceremony shortly after the war ends.

==Cast==
 
 
* Audie Murphy as Himself
* Marshall Thompson as Private/Corporal Johnson
* Charles Drake as Private Brandon Jack Kelly as Private/Staff Sergeant Kerrigan
* Gregg Palmer as Lieutenant Manning
* Paul Picerni as Private/Corporal Valentino
* David Janssen as Lieutenant Lee
* Richard Castle as Private Kovak
* Bruce Cowling as Captain Marks
* Paul Langton as Colonel Howe
 
* Art Aragon as Sanchez
* Felix Noriego as Swope
* Denver Pyle as Thompson
* Brett Halsey as Saunders
* Susan Kohner as Maria
* Anabel Shaw as Helen
* Mary Field as Mrs. Murphy
* Gordon Gebert as Audie as a boy
* Julian Upton as Steiner
 

==Background==
When Universal-International picked up the film rights to Audie Murphys book, he initially declined to play himself, recommending instead Tony Curtis, with whom he had previously worked in three Western (genre)|Westerns, Sierra (film)|Sierra, Kansas Raiders and The Cimarron Kid. However, producer Aaron Rosenberg and director Jesse Hibbs convinced Audie to star in the picture, despite the fact the 31-year-old Murphy would be portraying himself as he was at ages 18–20. 
 Fort Lewis and Yakima Training Center, near Yakima, Washington with actual soldiers.  Murphy received 60 percent of the $25,000 the studio paid for the rights, as well as $100,000 and 10 percent of the net profits for starring and acting as a technical advisor. 

Originally, several generals who served in World War II were considered to perform the voiceover opening for the movie, among them Maxwell D. Taylor and Omar Bradley, until General Walter Bedell Smith was finally chosen. 
 Majestic Theatre in San Antonio, Texas on August 17, 1955.  The date of the premiere was also the tenth anniversary of Murphys army discharge at Fort Sam Houston in San Antonio. 

==Response==
The film was a huge commercial and critical success, and advanced Murphys film career. He had a percentage of the profits and it was estimated the actor earned $1 million from the film. Don Graham, No Name on the Bullet: The Biography of Audie Murphy, Penguin, 1989 p 250  The movie also popularized a term for U.S. Army foot soldiers, " ", written by Lieutenant Ken Hart and Corporal Bert Gold.  
  
 The Young Warriors.

Murphy tried to make a sequel called The Way Back dealing with his post-war life but could never get a script that could attract finance. 

==References==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 