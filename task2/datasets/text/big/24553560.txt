The Invader (1997 film)
{{Infobox film
| name           = The Invader
| image          =
| image_size     =
| caption        =
| director       = Mark Rosman
| writer         =
| narrator       =
| starring       = Sean Young Ben Cross Daniel Baldwin Nick Mancuso
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
The Invader is a 1997 film directed by Mark Rosman.

==Plot summary==
A day has come when not one, but two UFOs make their way into Earths atmosphere.  The larger ship is clearly chasing the smaller one, and the alien has the look of a soldier about him.  The smaller ship is able to cause the larger one to crash, then lands itself.  The pilot (Renn) sets out for town looking for an Earth woman named Annie Nilssen.  Annie is a local school teacher who has recently broken up with her boyfriend, Sheriff Jack Logan.  While shes on a girls night out at the tavern with a friend, she encounters Renn.  Refusing his offer to take her home, they nonetheless share a smoking hot kiss.  Feeling ill during school the next day she goes to her doctor only to be told she is 2  1 / 2  months pregnant - an impossibility not only because she broke up with Logan four months ago, but because uterine scarring had made her infertile.  While reading up on pregnancy and baby books at home that night she is found by Renn.  He explains that he is the father of her baby and insists she come with him to escape the following alien killer.  Renn ends up having to kidnap Annie.  When she wakes the next day he gives her an egg-shaped supplement and tries to explain to her about how his world had died and why his people needed Earth women - to repopulate themselves.  The babys pregnancy is meant to be done in three days and he has to take her back to his ship.  Annie doesnt believe him and tries to escape until she meets Willard, the second alien.  He had accompanied Jack Logan and his deputies on a rescue expedition and tries to kill Annie on the trail.  His attempt is foiled by one of the deputies and Renn is able to reclaim Annie and keep moving her to his ship.  When his ship is found to be destroyed by a lightning storm they must continue on to the nearest mountaintop to try and connect with his mother-ship.

==External links==
* 

 

 
 
 
 
 


 