Bachna Ae Haseeno
 
 
 
{{Infobox film
| name              = Bachna Ae Haseeno
| image             = Bachna Ae Haseeno.jpg
| caption           = Theatrical release poster
| director          = Siddharth Anand
| producer          = Aditya Chopra
| story             = Jaideep Sahni
| screenplay        = Devika Bhagat
| starring          = Ranbir Kapoor Bipasha Basu Deepika Padukone Minissha Lamba Kunal Kapoor
| music             = Songs:  
| narrator          = Ashutosh Dhakate
| cinematography    = Sunil Patel
| editing           = Ritesh Soni
| distributor       = Yash Raj Films
| released          =  
| runtime           = 153 minutes
| country           = India
| language          = Hindi
| budget            =  
| gross             =   image_size = }}
Bachna Ae Haseeno (English: Watch Out, Ladies) is an Indian romantic comedy released 15 August 2008. The rom-com stars Ranbir Kapoor, Bipasha Basu, Deepika Padukone  and Minissha Lamba. Its director was Siddharth Anand, whose previous projects include Salaam Namaste (2005) and Ta Ra Rum Pum (2007). It was the eight highest grossing movie of 2008.

==Plot==
The film narrates the journey of a young man named Raj Sharma ( ), a small-town girl from Punjab; Radhika (Bipasha Basu), an aspiring model in Mumbai; and Gayatri (Deepika Padukone), a non-resident Indian student in Australia. It is the story of a ladies man who runs from one conquest to another only to crash into true love.
 Eurorail during a trip to Switzerland. Mahi is a sweet, dreamy girl who believes in true love and is sure she will someday meet her "Raj" (the lead character from the film Dilwale Dulhania Le Jayenge, released in 1995). When Mahi misses her train, Raj, a passenger on the same train, gets off to help her out. He offers to help her reach the airport through a different route in time for her return flight. On the way, Mahi develops a crush on Raj and they share a kiss after Raj reads her a poem he wrote about her. At the airport, Mahi meets her friends and tells them about Raj. When she opens the paper on which Raj had written the poem, she finds it blank. She overhears Raj boasting to his friends about the "stuff" the pair did, and is shocked and heartbroken. Raj realises she has overheard and shamefacedly leaves the airport.

In 2002, Raj has moved to Mumbai and found a job with Microsoft as a game designer. He is in a live-in relationship with Radhika, an aspiring model. Raj receives an offer to move to Sydney for a Halo 3 game launch. He expects to be able to leave Radhika and move on, assuming she is a "modern girl" capable of moving past a break-up. This vision is shattered when Radhika unexpectedly declares that she intends to sacrifice her career as a model to marry Raj and join him in Australia. Raj, unable to express his commitment-phobia, flees. He boards a flight to Sydney on the day he is to marry Radhika. Radhika learns this while waiting for him at the marriage registrars office, and is left crestfallen.

In 2007, Raj now enjoys a successful career in Sydney. One day, he runs into Gayatri, a feisty and independent woman who drives a taxi at night and studies at business school during the day. Raj befriends her and learns that she, like him, does not believe in the sanctity of marriage. As they date he realises that his feelings for her challenge his deep-set misgivings towards commitment. Raj musters the courage to propose, but Gayatri turns him down, saying that she is happy with her life as is. Rejection cuts Raj deep. As he stands alone watching his true love walk away, he recalls when he broke the hearts of Mahi and Radhika, realising how they must have felt. He decides to seek them out and ask for forgiveness.

On his return to India in 2008, he first seeks out Mahi, who is now married and has two sons. He runs into her husband Joginder (Kunal Kapoor), who expresses his anger, as he had heard about Rajs exploits. He explains that while Mahi on the surface appears the perfect housewife and perfect mother, she no longer believes in love. Raj apologises to Mahi and convinces her that her husband is the real "Raj" she has been waiting for, and Mahi forgives him.

Raj then embarks on a search for Radhika. He discovers that she has changed her name to Shreya and has become a highly successful supermodel. After much effort, he manages to meet her and apologises. Shreya refuses to accept his apologies and tells him that if he really wants her forgiveness, he will have to work hard for it. To do this, Raj becomes her personal assistant and she makes him do all sorts of tasks, from cleaning to serving at parties. She tries to humiliate him on every occasion, but Raj remains firm in his resolve and continues to serve without complaint. She finally gives up and tells him how hurt she felt, alone on the day that was supposed to be their wedding. Exasperated, she tells Raj to give up his penance and go home. However, while Raj is waiting for his flight to Sydney, she arrives and tells him that she has realised that the root of her frustration was bottled-up hate against Raj that she no longer bears. She forgives him. Content, Raj returns to Australia, having completed what he had set out to do.

On his return, Raj finds a pile of letters from Gayatri, written over the six months Raj was gone. While he has been away, Gayatri has had second thoughts and regretted turning him down. The lovers reunite and live happily ever after.

==Cast==
* Ranbir Kapoor as Raj Sharma
* Bipasha Basu as Radhika / Shreya Rathore
* Deepika Padukone as Gayatri
* Minnisha Lamba as Mahi
* Kunal Kapoor as Joginder Singh Ahluwalia
* Hiten Paintal as Sachin
* Puneet Issar as Mahis dad
* Avantika Hundal as Mahis Friend (Mona)
* Sham Mashalkar as Rajs Friend
* Natasha Bhardwaj as Mahis Friend
* Sumeet Arora as Rajs Friend
* Pratik Dixit

==Production==
 ]]
In April 2007, sources indicated that director Siddharth Anand had signed for the main lead in his next film. However, after indicating that the news was "mere speculation" and "absolutely untrue,"  he later confirmed that Kapoor was starring in his film.   
 :St Marks Basilica]]
The director was in talks with actresses Preity Zinta, Bipasha Basu, Deepika Padukone and Amrita Rao for the roles in question but indicated that "it wouldnt be right to reveal their names till we sign them on the dotted line."  In June 2007, Katrina Kaif and Deepika Padukone were finalised to play two of leading ladies,  but the formers role was later removed due to the films story becoming too long.  Actress Amrita Rao was considered to play one of the leading ladies but opted out. 

The heroines share very limited time on screen together and therefore most of their parts were filmed separately. Deepika Padukones portions were filmed in Sydney, Australia while the song "Khuda Jaane" was shot at locations in Italy, including Venice, Alberobello and Santa Cesarea Terme (Apulia), Naples and Capri.  Minissha Lambas portions, inspired by Yash Chopras Dilwale Dulhania Le Jayenge, were filmed in the similar locales of Amritsar and Switzerland. Bipasha Basus portions were filmed in Mumbai, India and Capri, Italy.  Ranbir Kapoors look in the film was styled by Aki Narula, which changed with time. In title track Ranbir had at least 10 wardrobe changes in one shot.  Though he found styling for three lead actresses characters exciting and challenging, as all play different types who enter into Ranbirs life at different times. So he ensured that the styling was unique without any overlapping. 

Continuing the tradition of showcasing the first look of their upcoming films with their latest release, Yash Raj Films showcased the first trailer of the film on 25 April 2008 with the release of Vijay Krishna Acharyas Tashan (film)|Tashan and a second trailer was shown on 27 June 2008 with the release of Kunal Kohlis Thoda Pyaar Thoda Magic.  Yash Raj Films usually does not spend much money on promotion. However, it made an exception with Bachna Ae Haseeno. Instead of the usual promotion 2–3 weeks before a movie release, the production house planned a new strategy with promotion over two months.   

Since two of the films characters work for Microsoft Game Studios, plenty of video games released by them are referenced. In addition, nearly all of the movies referenced are releases from Yash Raj Films, with the notable exception of Hum Kisise Kum Naheen (1977). 

==Reception==

===Critical Reception===
The film opened to mixed response from critics. Indiafm.com gave it 2.5 stars out of 5, with Taran Adarsh commenting, "On the whole,   has an interesting first half, but a weak second hour spoils the show."  AOL India gave it 3 out of 5, with Noyon Jyoti Parasara saying, "Overall, Siddharth Anand seems to have hit the bulls eye this time after a not-so-great Ta Ra Rum Pum. At least the first half of the film looked like it would pull out Yash Raj out of the rot!" 

===Box office===
The box office collections were good. It was declared as a hit.

==Soundtrack==
{{Infobox album
| Name        = Bachna Ae Haseeno
| Type        = soundtrack
| Artist      = Vishal-Shekhar
| Cover       = bahcover.jpg
| Released    =  
| Genre       = Film soundtrack
| Length      = 30:59 YRF Music
| Producer    = Aditya Chopra & Yash Chopra
| Last album  = De Taali (2008)
| This album  = Bachna Ae Haseeno (2008)
| Next album  = Dostana (2008 film)|Dostana (2008)
}}

There are a total of seven tracks in the movie including a remix version. The album entered the Top 3 of the music charts within a couple of days of its arrival on the stands.  The lyrics of all the songs are written by Anvita Dutt Guptan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Singer(s)!! Duration !! Picturized On
|-
| 1
| "Khuda Jaane"
| Krishnakumar Kunnath|KK, Shilpa Rao
| 5:35
| Ranbir Kapoor, Deepika Padukone
|-
| 2
| "Lucky Boy"
| Sunidhi Chauhan, Hard Kaur, Raja Hasan
| 4:15
| Bipasha Basu
|-
| 3
| "Aahista Aahista"
| Lucky Ali, Shreya Ghoshal
| 5:52
| Ranbir Kapoor, Minissha Lamba
|-
| 4
| "Jogi Mahi"
| Sukhwinder Singh, Shekhar Ravjiani, Himani Kapoor
| 4:54
| Ranbir Kapoor, Minissha Lamba, Kunal Kapoor
|-
| 5
| "Small Town Girl"
| Shankar Mahadevan
| 3:49
| Ranbir Kapoor, Bipasha Basu
|-
| 6
| "Khuda Jaane – Revisited"
| KK, Shilpa Rao
| 4:43
| Ranbir Kapoor, Deepika Padukone
|-
| 7 Bachna Ae Haseeno"
| Kishore Kumar, Sumit Kumar, Vishal Dadlani
| 3:31
| Ranbir Kapoor, Deepika Padukone, Bipasha Basu, Minissha Lamba
|}

==Awards and Nominations==

===Won===
*2009 Stars Sabsey Favourite, Sabsey Tez Sitara - Deepika Padukone  KK

===Nominated===
*2009 Screen Award for Best Actor - Ranbir Kapoor
*2009 Stardust Award for Star of the Year – Male - Ranbir Kapoor
*2008 Filmfare Award for Best Supporting Actress - Bipasha Basu
*2008 Stardust Best Supporting Actress Award - Bipasha Basu
*2009 Star Screen Award Best Supporting Actress - Bipasha Basu IIFA Best Supporting Actress - Bipasha Basu KK
*2009 Filmfare Award for Best Female Playback Singer for "Khuda Jaane" - Shilpa Rao
*2009 IIFA Award for Best Lyricist Anvita Dutt Guptan - "Khuda Jaane"
*2009 IIFA Award for Best Male Playback for "Khuda Jaane" - KK
*2009 IIFA Award for Best Female Playback for "Khuda Jaane" - Shilpa Rao

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 