Frontier(s)
 
{{Infobox film
| name           = Frontier(s)
| image          = Frontiersposter.jpg
| alt            = 
| caption        = Original theatrical poster
| director       = Xavier Gens
| producer       = Laurent Tolleron
| writer         = Xavier Gens
| starring       = {{Plainlist|
* Aurélien Wiik
* Samuel Le Bihan
* Estelle Lefébure}}
| music          = Jean-Pierre Taieb
| cinematography = Laurent Barès
| editing        = Carlo Rizzo
| studio         = {{Plainlist|
* BR Films
* Cartel Productions
* Chemin Vert
* Pacific Films}}
| distributor    = EuropaCorp
| released       =  
| runtime        = 108 minutes  
| country        = {{Plainlist|
* France
* Switzerland}}
| language       = {{Plainlist|
* French
* German}}
| budget         = $3 million
}}
Frontier(s) ( ) is a 2007 French horror film written and directed by Xavier Gens and stars Estelle Lefébure, Samuel Le Bihan and Aurélien Wiik. 

==Plot==
An extremist right-wing candidate is elected to the French presidency, sparking riots in Paris. Hoping to escape Paris but needing cash, Alex ( ) and Klaudia (Amélie Daure) claim their rooms are free and seduce the two men.

At the hospital, the emergency room staff report Samis injury to the police. Sami insists Yasmine run before the police catch her. His dying wish is that Yasmine not have an abortion. Alex and Yasmine flee, phoning their friends for directions to the inn. Tom and Farid give them directions but soon after are brutally attacked by Gilberte, Klaudia, and Goetz (Samuel Le Bihan). When Tom and Farid try to escape, Goetz runs their car off a cliff. The injured men wander into a mine shaft, where Tom is quickly recaptured. Farid must fend for himself in the mine with the familys rejected children. Unaware of the danger, Alex and Yasmine arrive at the inn and are captured by the family.

Alex and Yasmine are chained in a muddy-floored pig pen. Alex breaks Yasmines chains and allows her to escape. When the captors discover Yasmines escape, the family patriarch, von Geisler (Jean-Pierre Jorris), cuts Alexs Achilles tendons. Meanwhile, in the mine, Farid finds the storage area for the victims. The family realizes something is amiss in the mine, and Hans (Joël Lefrançois) chases Farid into a boiler where Farid is cooked alive. Yasmine flees from the inn but is quickly picked back up by Goetz. Back in the pig pen, von Geisler personally grants Alexs last wish, which is to be put down quickly. Initially, von Geisler wishes for Karl (Patrick Ligardes) to "wed" Yasmine to carry on the family lineage, but when von Geisler learns she is already pregnant, he entrusts her to the meek Eva (Maud Forget), who tells Yasmine that she came to the family in a very similar manner and that she is obedient because the family promised her that her parents would return for her some day. Eva also tells Yasmine of the rejected children she and Hans care for in the mine.

Eventually, Eva leads Yasmine down to dinner, where the family awaits her. Von Geisler is revealed to be a former (and still practicing) Nazi. Von Geisler offers up a toast to the new blood and Yasmine quickly grabs a large knife and takes von Geisler hostage. Hans grabs a shotgun and shoots and kills von Geisler in the confusion; Karl shoots Hans dead in turn. Yasmine escapes and is chased by Karl and Goetz into the mine. Yasmine eventually makes her way into one of the body storage rooms where she fights with Goetz. After a bloody struggle, she repeatedly hits him with an axe before impaling him on a rotating table saw. Karl catches Yasmine as she tries to return to the surface, but Eva comes to the rescue, blowing off Karls head. Yasmine searches for car keys, but is ambushed by Gilberte and Klaudia. During the shootout, Yasmine hits a gas tank, blowing up the room. Gilberte survives the explosion and attempts to kill Yasmine only to have her throat torn out by her. With everyone else dead, Yasmine tries to persuade Eva to leave with her, but Eva stays to take care of the children in the mine. On the road, Yasmine runs into a police blockade where she surrenders to the authorities.

==Cast==
* Aurélien Wiik as Alex
* Samuel Le Bihan as Goetz
* Estelle Lefébure as Gilberte
* Karina Testa as Yasmine
* David Saracino as Tom
* Chems Dahmani as Farid
* Adel Bencherif as Sami
* Maud Forget as Eva
* Amélie Daure as Klaudia
* Rosine Favey as La mere
* Joël Lefrançois as Hans
* Patrick Ligardes as Karl
* Jean-Pierre Jorris as Von Geisler

==Release== MPAA giving the film an NC-17 rating, it instead hit a limited number of 10 US theaters unrated for one weekend where it grossed $9,913 before being released on DVD the following week. 

==Critical reception==
 
 
The film received mixed reviews from critics. As of 2010, the review aggregator Rotten Tomatoes reported that 56% of critics gave the film positive reviews, based on 20 reviews.  Metacritic reported the film had an average score of 44 out of 100, based on 5 reviews. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 