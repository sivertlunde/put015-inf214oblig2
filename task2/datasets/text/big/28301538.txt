Love, Bitter
{{Infobox film
| name           = Love, Bitter 
| image          = LoveBitterFilmPoster.jpg
| alt            = LoveBitterFilmPoster.jpg
| caption        = Theatrical poster
| director       = A. Taner Elhan
| producer       = Timur Savcı
| writer         = Onur Ünlü
| starring       = Halit Ergenc Cansu Dere Ezgi Asaroğlu Songül Öden Ozan Osmanpaşaoğlu
| music          = Fairuz Derin Bulut
| cinematography = Vedat Özdemir
| editing        = 
| studio         = Eflatun Film TIMS Productions
| distributor    = Tiglon Film
| released       =  
| runtime        = 92 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = US$778,966
}}
Love, Bitter ( ) is a 2009 Turkish drama film co-produced and directed by A. Taner Elhan, about a literature professor caught in a dangerous love quadrangle. The film, which went on nationwide general release across Turkey on  , was screened in competition at the 29th International Istanbul Film Festival. 

==Production==
The film was shot in 30 days beginning on   at the Film Sokağı Studios and on location in Eskişehir and Istanbul, Turkey.

==Plot==
Orhan, who works as a literature professor at a university in Eskisehir (a city in Central Anatolia), unexpectedly ends his relationship with Ayşe whom he was planning to marry, and comes to Istanbul where he meets Oya. After living possibly the happiest days of his life with Oya, who is a photographer, Orhan finds himself in the midst of a challenging love triangle as a result of the horrible things they encounter the first days of their marriage. Meanwhile, his new student Seda, pulls Orhan towards a point of no return. Tough days are ahead for Orhan whose life is turned upside down, torn between three women.

==Release==

===General release===
The film opened on general release in 74 screens across Turkey on   at number six in the Turkish box office chart with an opening weekend gross of US$220,273.   

===Festival screenings===
* 29th International Istanbul Film Festival (April 3–18, 2010) 

==Reception==

===Box Office===
The film was in the Turkish box office charts for eight weeks and has made a total gross of US$778,966. 

===Reviews===
Emrah Güler, writing in Hürriyet Daily News, describes the film as, "similar to last week’s release of novelist-cum-filmmaker Tuna Kiremitçis Adını Sen Koy (You Name It)", and states that, "this film portrays another love triangle of the upper-middle class and is set in Eskişehir." He concludes that, "the trailer promises lame a dialog  ", and he recommends the film to, "those who get a kick out of cardboard characters and far-from-subtle cliché romances as found in a recent avalanche of Turkish TV series." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 