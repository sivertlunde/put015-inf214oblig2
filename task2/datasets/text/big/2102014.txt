The Golden Child
{{Infobox film 
| name           = The Golden Child
| image          = Golden child movie.jpg
| alt            =
| caption        = Theatrical release poster Michael Ritchie
| producer       = Edward S. Feldman Robert D. Wachs
| writer         = Dennis Feldman
| starring       = Eddie Murphy John Barry
| cinematography = Donald E. Thorin
| editing        = Richard A. Harris
| studio         = Eddie Murphy Productions Industrial Light & Magic
| distributor    = Paramount Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         = $25 million
| gross          = $79.8 million (domestic)
}}
 fantasy comedy Michael Ritchie and starring Eddie Murphy as Chandler Jarrell, who is informed that he is "The Chosen One" and is destined to save "The Golden Child", the savior of all humankind.
 gross of $79,817,937 at the United States (US) box office. 

==Plot==

In a temple in an unknown location in northeastern Tibet, a young boy with mystical abilities &mdash; the Golden Child &mdash; receives badges of station and demonstrates his power to the monks of the temple by reviving a dead bird, which becomes a constant companion. However, a band of villains led by a mysterious man, Sardo Numspa (Charles Dance), breaks into the hidden temple, slaughters the monks and abducts the boy.
 astral form familiar begin following him.
 motorcycle gang, the Yellow Dragons, which Cheryll had joined, and Chinese restaurant owner Tommy Tong, a henchman of Numspa, to whom Cheryll had been sold for her blood, used to make the Child vulnerable to earthly harm. Tong, however, is killed by Numspa as a potential traitor. Still not taking the case too seriously, Chandler is drawn by Numspa—whom Chandler begins to continuously call "Numsy"—into a controlled dream, where he receives a burn mark on his arm. Numspa also presents his demands: the Ajanti Dagger (a mystic weapon which is capable of killing the Golden Child) in exchange for the boy. Chandler finally agrees to help, and he and Kee Nang spend the night together.

Chandler and Kee travel to Tibet, where Chandler is apparently swindled by an old amulet seller, who is revealed as the High Priest of the temple where the dagger is kept hidden and, subsequently, Kees father (Chandler calls him "Monty Hall" or "Monty"). In order to obtain the blade, Chandler has to pass a test: an obstacle course in a bottomless cavern whilst carrying a glass of water without spilling a drop. With luck and wits, Chandler recovers the blade and even manages to bring it past customs into the United States.

Numspa and his henchmen attack Chandler and Kee. The Ajanti Dagger is lost to the villains, and Kee takes a crossbow bolt meant for Chandler, and dies in his arms confessing her love for him. Doctor Hong and Kala offers him hope, for as long as the sun shines upon Kee, the Child might be able to save her. Driven now by a personal motive, Chandler — with the help of the Childs familiar — locates Numspas hideout, and retrieves the dagger with the help of Til, one of Numspas men converted to good by the Child, and frees the boy. But when Chandler attempts to confront Numspa, the latter reveals his true face as a demon from hell. Chandler and the Child escape the hideout, only to be tracked down by the demon in a warehouse. Chandler loses the dagger when the warehouse collapses, but Sardo is buried under a chunk of falling masonry. Chandler and the Child escape and head to Doctor Hongs shop where Kee is being kept.

As the two approach Kees body, a badly injured but berserk Numspa attacks Chandler but the amulet the Old Man sold Chandler blasts the dagger from Numspas hand. The Child uses his magic to place the dagger back into Chandlers hands, and Chandler pierces Numspa through the heart with it, destroying him. The Child then uses the last ray of sunlight and his powers to bring Kee back from the dead. As the movie ends, the three take a walk discussing the Childs return to Tibet and (as Chandler jokingly suggests) the boys prospective fame as a stage magician.

==Cast==
* Eddie Murphy as Chandler Jarrell
* J.L Reate as The Golden Child
* Charles Dance as Sardo Numspa
* Charlotte Lewis as Kee Nang Victor Wong as "Monty Hall" The Old Man
* Randall "Tex" Cobb as Til 
* James Hong as Doctor Hong
* Shakti Chen as Kala
* Tau Logo as Yu  Tiger Chung Lee as Khan 
* Pons Maar as Fu 
* Peter Kwong as Tommy Tong 
* Wally Taylor as Detective Boggs 
* Eric Douglas as Yellow Dragon  Charles Levin as TV Host  
* Frank Welker (voice) as The Thing

==Production details==
   It attracted Hollywoods attention and after a bidding war Paramount Pictures purchased the script for $300,000. 

Although the character of The Golden Child is portrayed and treated as a boy throughout the movie, the part was played by actress Jasmine Reate (credited as "J.L. Reate").   
 Victor Wong, James Hong, and Peter Kwong appear in both films).  Starring Kurt Russell, Carpenters project was reported to have been rushed through post-production to ensure a premiere date that preceded The Golden Child. 
 producers selected drama into a comedy. 

Gene LeBell makes a small cameo appearance as a drunk member of the "Yellow Dragons" biker gang.

== Music ==

=== The music score(s) ===
{{Infobox album  
 | Name        = The Golden Child: Music from the Motion Picture
 | Type        = film John Barry, Michel Colombier, and others
 | Released    = July 12, 2011
 | Recorded    = 1986
 | Genre       = Symphonic score, synth-pop/orchestral score, R & B, soft rock, classic rock
 | Length      = 2:45:38
 | Label       = La-La Land Records/Capitol Records
 | Producer    = Lukas Kendall
}} final cut soundtrack release issued by Capitol Records.

In 2011,   Barry-composed song, sung by emerging (at the time) composer, Randy Edelman (Kindergarten Cop). The songs that had previously been released on Capitols first soundtrack in 1986 were also featured in the multi-disc set.

=== Soundtrack ===
The following pieces of music appear in the film alongside Colombiers score:
 Nancy Wilson and Sue Ennis)
* Ratt - "Body Talk" (Composed by Stephen Pearcy, Warren DeMartini and Juan Croucier) conducted by John Barry)
* "The Chosen One" (Composed by Michel Colombier and performed by Robbie Buchanan)
* "Puttin on the Ritz" (Composed by Irving Berlin)
* "Another Days Life" (Composed by David Wheatley )

==Reception==
Released in December 1986, The Golden Child was a box office success.  It earned USD$79,817,937  in the U.S. alone, making it the eighth biggest film of the year. However, the film was considered a disappointment by the studio  when compared to Murphys previous film, Beverly Hills Cop, which garnered USD$234,760,478  at the US box office.

The film received negative reviews from critics, as it holds a 26% rating on Rotten Tomatoes.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 