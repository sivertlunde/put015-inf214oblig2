La Journée de la jupe
{{Infobox film
| name           = La Journée de la jupe
| image          = Lajournee.jpg
| caption        = Film poster
| director       = Jean-Paul Lilienfeld
| producer       = Ariel Askénazi Bénédicte Lesage 
| writer         = Jean-Paul Lilienfeld
| starring       = Isabelle Adjani Denis Podalydès Jackie Berroyer
| music          = Kohann 
| cinematography = Pascal Rabaud 
| editing        = Aurique Delannoy 
| distributor    = Rézo Films
| released       =  
| runtime        = 88 minutes
| country        = France
| language       = French 
| budget         = $1.5 million
| gross          = $1,997,721 
}}
La Journée de la jupe is a 2008 French television film, directed by Jean-Paul Lilienfeld and starring Isabelle Adjani as a high school teacher. 

A key point of the plot of the movie happened in real life: a request was sent to the French Minister of Education to propose a Skirt Day. See  . 

==Plot== unmotivated students, departure of her husband. Her wearing of a skirt is considered a sensitive topic given the schools large Muslim population, some of whom have been raised to understand that such clothing is immodest.
 theatre play loses control and takes her class hostage, opportunistically creating a proper — although biased — teaching environment.

While school officials, a negotiator, the SWAT team and high-ranking politicians outside try to figure out what is going on and how to react, Sonia forces the students to see things her way and ultimately shows them the contradictions in their own lives.

==Cast==
 
;Main Characters

* Isabelle Adjani as Sonia Bergerac, French literature teacher
* Denis Podalydès as Chief Labouret, SWAT negotiator
* Yann Collette as Officer Bechet, Labourets ranking officer, who wants to put a show of force
* Jackie Berroyer as school principal

;Students in the class

* Yann Ebonge as Mouss
* Kévin Azaïs as Sébastien
* Karim Zakraoui as Farid
* Khalid Berkouz as Mehmet
* Sonia Amori as Nawel
* Sarah Douali as Farida
* Salim Boughidene as Jérôme
* Mélèze Bouzid as Khadija
* Hassan Mezhoud as Akim
* Fily Doumbia as Adiy

;Secondary characters

* Nathalie Besançon as the Minister of the Interior
* Marc Citti as Frédéric Bergerac, Sonias husband
* Olivier Brocheriou as Julien
* Anne Girouard as Cécile
* Stéphan Guérin-Tillié as François

==Reception== Berlin International film festival 2009,   and was first broadcast on European culture TV channel Arte on March 20, 2009 before being released in cinemas on March 25, 2009. Lilienfeld said that the lack of funding prevented a typical theatre release and prompted a prior broadcast on TV. The inaugural release covered 50 cinemas, but this number grew because of growing public interest. 

The movie was controversial because of its theme, viewpoint and the hurdles that led to an atypical cinema and TV release. It is debated whether the movie is "politically incorrect", especially in light of the success of similarly themed movie Entre les murs  a few months earlier. It was nominated for Best Film at the 35th César Awards and Isabelle Adjani won a record fifth award for Best Actress. It was her first role in six years and considered to be a comeback for her.

German arts and culture TV channel ZDF Kultur produced a German language theatre play for television broadcast version entitled Verrücktes Blut.

==In popular culture== legitimate for female teachers to dress with a skirt while teaching, instead of complying with ultra-conservative policies implicitly enforced by aggressive students.

==References==
 

==External links==
*    
*  

 
 
 
 
 
 
 