The Adventure of Iron Pussy
{{Infobox film
| name           = The Adventure of Iron Pussy
| image          = Ironpussy2.jpg
| caption        = Thai theatrical poster
| director       = Apichatpong Weerasethakul Michael Shaowanasai
| producer       = 
| eproducer      = 
| aproducer      = 
| writer         = Apichatpong Weerasethakul Michael Shaowanasai 
| starring       = Michael Shaowanasai Krissada Terrence
| music          = 
| cinematography = Surachet Thongmee
| editing        = 
| distributor    = Kick the Machine
| released       = November 1, 2003 (Japan)
| runtime        = 90 minutes
| country        = Thailand
| awards         = 
| language       = Thai. English subtitles
| budget         = 
| gross          = 
}}
The Adventure of Iron Pussy ( ) is a 2003 Thai musical film|musical-action film|action-comedy film written and directed by Apichatpong Weerasethakul and Michael Shaowanasai and starring Shaowanasai. The protagonist is a transvestite Thai secret agent whose alter ego is a gay male convenience clerk. A homage and parody of the 1970s Thai action films, musicals and melodramas, particularly those that starred Mitr Chaibancha and Petchara Chaowarat, the movie premiered at the 2003 Tokyo International Film Festival and has also played at the Berlin Film Festival, the International Film Festival Rotterdam and other festivals. It is a cult film and has screened at several gay and lesbian film festivals as well.

==Plot==
The scene opens on a rural Thai coffee shop, run by an elderly man and his attractive teenage daughter. A gang of local toughs come in and start roughing up the customers and get abusive with the daughter. Suddenly, an elaborately coifed and tastefully dressed woman shows up and rescues the young woman and her father from further harm. This is Iron Pussy.

She disappears into the bushes and re-emerges as a slight, shaven-headed man, who then gets on the back of a motorcycle taxi and heads into Bangkok. Along the way, the motorcycle driver, Pew, relates his memories of the day he and Iron Pussy met – Pew, crazed from a drug overdose, had taken a young woman hostage. Iron Pussy came on the scene and rescued not the young woman, but Pew, and the two have been a couple ever since.

Iron Pussy arrives at her job. She is a clerk in a 7-Eleven in Bangkok. Unfailingly courteous and professional, she greets a man who she believes is just another customer, in the shop to pay his phone bill. When Iron Pussy scans the bill, a message comes up on the computer screen: "Hello Iron Pussy". It is a secret message from the prime minister. It seems theres a job for Iron Pussy.
 Buddhist temple, Prime Minister Thaksin Shinawatra and his cabinet (all lookalike actors), who take time to sing a song, extolling the virtues of Iron Pussy.

Iron Pussy is tasked with uncovering the nefarious activities of Mr. Henry, a foreigner who frequently visits Thailand and leaves with full bank accounts. Mr. Henry will be attending a lavish party at the luxurious mansion of Madame Pompadoy, so Iron Pussy must infiltrate the housekeeping staff as a domestic worker|maid.

Her charms win over Madame Pompadoys debonair son, Tang, who is nonetheless in another relationship, being engaged to marry Rungraree, globetrotting socialite. Iron Pussy feels herself falling for Tangs charms as well, but she has a nagging feeling that something is not right about that relationship.

Donning an all-black Spandex outfit and mask one night, Iron Pussy uncovers Mr. Henrys plot – hes making a mind-control drug. And Tang is involved in the scheme, which breaks Iron Pussys heart.

Later, at the big party being thrown by Madame Pompadoy, Iron Pussy steps up when the main entertainment doesnt show up, and sings a song that really impresses everyone.

The next day, the family is going into the jungle for its annual deer hunt. This is when everything comes out – that Iron Pussy is actually a secret agent, but its also revealed that Madame Pompadoy is Iron Pussys mother, who gave Iron Pussy up for adoption long ago, which means, of course, that Tang is Iron Pussys brother.

==Cast==
* Michael Shaowanasai as Iron Pussy 
* Krissada Terrence as Tang
* Siriyakorn Pukkavesh as Rungranee
* Theerawat Thongjitti as Pew
* Darunee Khrittabhunyalai as Madam Pompadoy
* Krung Sriwilai as Mr. Henry
* Jenjira Pongpis as head maid Somjintana 

==Origin and production==
The Iron Pussy character was developed by Michael Shaowanasai, a Bangkok-based performance artist, who made some short films involving the heroine. She is modeled after Thai cinematic heroine of the 1970s, Petchara Chaowarat, whose trademark appearance always included elaborate hair styles and makeup to accentuate her large, expressive eyes. Petchara made a string of action films, melodramas and musicals, mostly with Mitr Chaibancha, and Iron Pussy, particularly when she is in "stealth" mode, recalls Mitrs famous superhero character, Red Eagle (Insee Daeng).

The feature film was made while Shaowanasais friend, Apichatpong Weerasethakul, was experiencing a delay in funding for his third feature film, Tropical Malady.  To keep his Kick the Machine production company working, Apichatpong and Michael collaborated on the script and quickly made Iron Pussy on an extremely low budget using digital video, which has a grainy look that matches the 1970s 16 mm Thai films that have survived the years to still be shown today.

Enhancing the retro look and feel of the film, the soundtrack is entirely dubbing (filmmaking)|dubbed, which was the standard practice with 16-mm production in Thailand. Many of the voice actors who worked on Iron Pussy are veterans from the earlier era and are well-known to Thais. The dubbing allows Iron Pussys transformation from her male alter ego to her female superhero personality to be even more complete, because a male actor dubbed the mans voice and female actresses dubbed Iron Pussys voice and sang her songs.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 