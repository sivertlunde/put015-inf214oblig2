Pisaj
{{Infobox film
| name = Pisaj (Evil)
| image = Pisaj.jpg
| caption = Movie poster for Pisaj
| director = Chookiat Sakveerakul 
| producer = Prachya Pinkaew Somsak Techaratanaprasert Sukanya Vongsthapat
| writer =
| starring = Ammara Assawanon Alexander Rendel Theeranai Suwanhom Pumwaree Yodkamol
| music =
| cinematography =
| editing =
| distributor = Sahamongkol Film International
| released =  
| runtime = 110 minutes
| country = Thailand
| language = Thai
| budget =
}} 2004 Cinema Thai horror film directed by Chookiat Sakveerakul.

==Plot==

After her parents are killed in a drive-by shooting, a young woman named Oui has no place else to go. She shows up at a printing house run by her Aunt Bua and is given the task of caring for her aunts grandson, a young boy named Arm, a kid who sees ghosts.

Oui suffers from hallucinations, brought on by the trauma of seeing her parents killed, and is taking medications. And Aunt Bua is involved in some sort of mysticism, and keeps a strange shrine in the house.
 drug war by prime minister Thaksin Shinawatra as a subtext, many threads in this strange ghost story are somehow tied together.

==Cast==
*Ammara Assawanon as Aunt Bua 
*Alexander Rendell as Arm
*Pumwaree Yodkamol as Oui 
*Theeranai Suwanhom as Mai

==See also==
*Pishacha

==External links==
* 

 
 
 
 
 
 


 
 