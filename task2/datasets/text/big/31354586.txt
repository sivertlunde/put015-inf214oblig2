The Vanishing American
{{infobox film
| name           = The Vanishing American
| imagesize      =
| image          = Poster - Vanishing American, The (1925) 01 Crisco restoration.jpg
| caption        = Theatrical poster
| director       = George B. Seitz
| producer       = Adolph Zukor Jesse Lasky
| based on       =  
| writer         = Lucien Hubbard (adaptation) Ethel Doherty (scenario) Richard Dix Lois Wilson
| music          = Manny Baer Hugo Riesenfeld Harry Perry
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States Silent (English intertitles)
}}
 Richard Dix Lois Wilson, recently paired in several screen dramas by Paramount. The film is based on the 1925 novel, The Vanishing American, by Zane Grey.
 Harper & Brothers planned the books publication to coincide with the films release but Christian missionaries feared public criticism.  Harper editors thus altered the story before publication. 

==History of Adaptation==

Grey’s serialized novel, published in Ladies’ Home journal in 1922-1923, was the first piece of literature produced that offered a harsh portrayal of American government agencies towards Native Americans    Grey depicted the white settlers as missionaries who preyed upon the subordinate race, forcefully converting them into Christianity and altering their way of life. This depiction sparked a lot of backlash in the form of angry letters from readers once the novels were published  According to Zane Grey’s biographer, Thomas Pauly, “The magazine was deluged with angry letters from religious groups, and the Bureau of Indian Affairs vehemently denounced his depiction of their efforts.”  

In response to critics of the novel, Lasky persuaded Grey to dilute the American’s portrayal in the film. Grey agreed, and instead of American’s as a whole demonstrating contempt towards Native Americans, the script placed most of the blame on the corrupt individual character of Booker. 

According to an interview with Lasky in September 1925, the idea for adapting Grey’s novel into a feature-film originated in 1922 when he and Lucien Hubbard, the editorial supervisor for Zane Grey Productions, received an invitation from Grey to visit Navaho Mountain and Rainbow Bridge in northern Arizona. The reservation’s stark and boundless desert scenery captivated Lasky, and after spending nearly two months there, suggested they used the vast ranges as the background for a motion picture. 

==Plot==

The film is played out on the stage of Monument Valley, long ago, after tribes of Indians defeated the ancient cliff dwellers; then came the Europeans to conquer the Indians.   Now, in the early 20th Century, a tribe of Navajo live on a reservation overseen by an Indian-hating agent, Booker. He and his men steal the best Indian horses for their own profit. Nophaie, a tribal leader, complains to Bookers higher-ups, but he is unable to gain fair treatment from the whites. When World War I breaks out, Army captain Earl Ramsdale comes west in search of the horses that Booker was supposed to have bought from the Indians for a fair price. Marian Warner, the teacher at the Indian School, has befriended Nophaie, teaching him to read; she convinces him that the Great War is a fight for a more just world, and that, when that world comes, the Indian will be better treated. Nophaie not only brings horses for the Army, he and many other Indians enlist, and distinguish themselves in battle. But when they come back after the war is over, they find life for Indians even worse than when they left. The Indians go on the warpath, and Nophaie rides to warn the whites. Nophaie and Booker die in the fighting, and Nophaies only comfort is to die in the arms of Marion.   

==Production==

The Vanishing American was produced by Famous Player-Lasky and distributed through Paramount Pictures, and is considered one of the most ambitious productions of the Twenties. The film began production in June 1925 and finished that September. The majority of the film was shot in the Navaho Nation reservation, including locations in Monument Valley, Rainbow Bridge and Tsegi Canyon. 500 cast and crew members were brought in from Hollywood, in addition to over 1,000 Native American extras, according to newspaper publicity at the time. The film is timed at 110 minutes long. 

==Reviews==

Audiences and critics responded highly to the film, and it was given critical acclaim as the first film of its type to fault white agents for the Indians’ plight    A review by The Motion Picture News in 1925 stated, “It is an epic of the Indian, his beginnings, his rise to power and glory, his fall and the tragic qualities of his existence today.”   The Motion Picture News continued, saying, “The Vanishing American is destined to general popularity and ranks with the best of its type. It depicts beautiful scenes which picture the coming of the Redman as we know him.” Film historian Kevin Brownlow has praised the film noting that "the problem of the Indian and his betrayal by the government was more clearly etched in this picture than in any other silent film." 

Mordaunt Hall of The New York Times had some reservations about the performances, but he admired the films "matchless photography" and the "great artistry" of the cliff dwelling and World War I battle sequences.    He also praised Hugo Riesenfelds score, which the composer himself conducted for the films premiere at the Criterion Theatre.

==Cast==
   Richard Dix as Nophaie Lois Wilson as Marion Warner Noah Beery as Booker
*Malcolm McGregor as Earl Ramsdale
*Nocki as Indian Boy
*Shannon Day as Gekin Yashi
*Charles Crockett as Amos Halliday
*Bert Woodruff as Bart Wilson
  Bernard Siegel as Do Etin
*Guy Oliver as Kit Carson
*Joe Ryan as Jay Lord Charles Stevens as Shoie Bruce Gordon as Rhur
*Richard Howard as Glendon
*John Webb Dillon as Naylor
*Gary Cooper, unbilled
 

==Status==
This film survives at several restoration archives such as the Library of Congress and is available on home video and DVD. 

==See also==
*The House That Shadows Built (1931) promotional film released by Paramount with excerpt of this film

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 