Laptop (2012 film)
 
{{Infobox film
| name = Laptop
| image = laptop_bengali_movie_promotional_poster.jpg
| caption = Film poster
| alt = Seven different faces on a sepia-colored background
| director = Kaushik Ganguly
| screenplay =  Kaushik Ganguly Rajesh Sharma Kaushik Ganguly Gaurav Chakrabarty Pratyay Basu
| producer = Gautam Kundu
| distributor = Brand Value Communications Limited
| cinematography = Sirsha Ray
| released =  
| runtime =  120 Minutes
| language = Bengali
| music = Mayookh Bhaumik
| country = India
}}
Laptop is a Bengali drama film written and directed by Kaushik Ganguly. This film tells some stories related to some different people connected by one single laptop. according to the director, the laptop is the antagonist of the film, but not just a common thread between the main characters. 

==Cast==
* Rahul Bose
* Churni Ganguly
* Saswata Chatterjee
* Ananya Chatterjee
* Kaushik Ganguly Rajesh Sharma
* Pijush Ganguly
* Arindam Sil
* Gaurav Chakraborty
* Ridhima Ghosh
* Arun Guhathakurta Aparajita Adhya
* Anjana Basu Jojo
* Neha Kapoor
* Indranil Banerjee
* Pratyay Basu

==Music==
* Mayookh Bhaumik - Music Composer
* Anirban Sengupta and Dipankar Chaki - Sound

==References==
 

==External links==
* 

 

 
 
 


 