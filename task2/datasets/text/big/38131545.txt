Northwest Outpost
{{Infobox film
| name           = Northwest Outpost
| image          = Poster - Northwest Outpost 01.jpg
| image_size     = 190px
| caption        = Theatrical poster
| director       = Allan Dwan
| producer       = Allan Dwan Richard Sale   Angela Stuart   Laird Doyle
| starring       = Nelson Eddy Ilona Massey Joseph Schildkraut   Elsa Lanchester
| music          = Robert Ambruster   Rudolf Friml
| cinematography = Reggie Lanning
| editing        = Harry Keller
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       = June 25, 1947
| runtime        = 91 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
Northwest Outpost (aka, End of the Rainbow) is a 1947 American musical film directed by Allan Dwan and starring Nelson Eddy, Ilona Massey and Joseph Schildkraut. The film was Eddys last, and is an operetta film like his previous starring roles. He was persuaded to make it by Republic Pictures because Rudolf Friml was writing the film score|score. It was well received by critics and had a strong box office performance. 

==Synopsis== Russian imperial post at Fort Ross in California in the early Nineteenth Century. A visiting American army offices becomes romantically involved with an aristocratic woman whose criminal husband is being held as a prisoner at the Fort.

==Main cast==
* Nelson Eddy as Captain Jim Laurence 
* Ilona Massey as Natalia Alanova 
* Joseph Schildkraut as Count Igor Savin 
* Elsa Lanchester as Princess Tanya Tatiana 
* Hugo Haas as Prince Nickolai Balinin 
* Lenore Ulric as Baroness Kruposny 
* Peter Whitney as Volkoff Overseer 
* Tamara Shayne as Olga Natalias Maid 
* Ernő Verebes as Kyril Balinins Aide  George Sorel as Baron Kruposny 
* Rick Vallin as Dovkin

==References==
 

==Bibliography==
* Everett, William A. Rudolf Friml. University of Illinois Press, 2008.
* Lulay, Gail. Nelson Eddy: Americas Favorite Baritone. iUniverse, 2000.

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 