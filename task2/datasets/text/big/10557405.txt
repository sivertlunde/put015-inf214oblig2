We Must Become the Change We Want to See
{{Infobox album  
| Name        = We Must Become the Change We Want to See
| Type        = video
| Artist      = The Sound of Animals Fighting
| Cover       = 
| Cover size  = 
| Released    = May 1, 2007
| Recorded    = 2007
| Genre       = Experimental rock
| Length      =  Equal Vision
| Director    = 
| Producer    = Rich Balling
| Reviews     = 
| Chronology  = 
| Last album  = 
| This album  = We Must Become the Change We Want to See (2007)
| Next album  = 
| Misc        = 
}}

We Must Become the Change We Want to See is the first live DVD from The Sound of Animals Fighting, released on May 1, 2007. The DVD includes footage from one of the only four shows the band has played  . The show was filmed live at the House of Blues in Anaheim, California on August 27, 2006. The name "We Must Become the Change We Want to See" is taken from a Mahatma Gandhi quote.
 Metropolis is projected behind the band.

==Track listing==
#"The Heretic (a cappella)"
#"Act I: Chasing Suns"
#"Act II: All is Ash or the Light Shining Through It"
#"Act III: Modulate Back to the Tonic"
#"Unaria"
#"Skullflower"
#"My Horse Must Lose"
#"Horses in the Sky"
#"Stockhausen, es ist Ihr Gehirn, das ich Suche"
#"This Heat"
#"Act IV: You Dont Need A Witness"

==The cast==
{| class="wikitable"
! Position
! Name
! Animal
|-
| Vocals
| Rich Balling (Formerly of Rx Bandits)
| Nightingale
|-  bgcolor="#F0F8FF" Bass
| Joseph Troy (Rx Bandits)
| Octopus
|-  Drums
| Chris Tsagakis (Rx Bandits)
| Lynx
|-  bgcolor="#F0F8FF"
| Lead Guitar, Vocals
| Matt Embree (Rx Bandits)
| Walrus
|- Keyboard
| Steve Choi (Rx Bandits)
| Koala
|-  bgcolor="#F0F8FF"
| Vocals Anthony Green (Circa Survive)
| Skunk
|}

==Featuring==
{| class="wikitable"
! Position
! Name
! Animal
|-  bgcolor="#F0F8FF"
| Vocals Craig Owens (Chiodos) (D.R.U.G.S.) Ram
|-
| Vocals, Guitar
| Keith Goodwin (Good Old War)
| Penguin
|-  bgcolor="#F0F8FF" Percussion
| Chris Sheets (Rx Bandits) Dog
|-  bgcolor="#F0F8FF" Sampling
| Ryan Baker
| Hyena
|-  bgcolor="#F0F8FF"
| Vocals, Guitar
| Matthew Kelly (The Autumns)
| Wolf
|-  bgcolor="#F0F8FF" Keyboard
| Bradley Bell (Chiodos)
|
|}

*Bradley Bell from Chiodos is the only musician that is not given an animal name (this may be because he never appeared on any of the studio recordings), he also is wearing a mask and is completely cloaked in black as he performs

 

 
 
 