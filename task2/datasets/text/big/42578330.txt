I've Lost My Husband!
{{Infobox film
| name = Ive Lost My Husband!
| image =
| image_size =
| caption =
| director = Enrico Guazzoni
| producer = 
| writer =  Gian Gaspare Napolitano
| narrator =
| starring = Paola Borboni   Nino Besozzi   Enrico Viarisio   Romolo Costa 
| music = Amedeo Escobar 
| cinematography = Massimo Terzano
| editing = Gino Talamo 
| studio =   Astra Film  ENIC
| released = 1937
| runtime = 75 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Ive Lost My Husband! (Italian:Ho perduto mio marito) is a 1937 Italian romantic comedy film directed by Enrico Guazzoni and starring Paola Borboni, Nino Besozzi and Enrico Viarisio.  A young is in love with her friend and wants him to marry her. In order for them to spend time together she tricks him into believing that she has recently got married and lost her husband at the railway station. She persuades him to assist her in an extensive search for the missing husband, by the end of which he has fallen in love with her.

==Cast==
* Paola Borboni as Valentina 
* Nino Besozzi as Conte Giuliano Arenzi  
* Enrico Viarisio as Mattia  
* Romolo Costa as Ing. Zanni  
* Nicola Maldacea as Giuseppe  
* Vittorina Benvenuti as Signorina Adele 
* Vanna Vanni as Cecilia - sua figlia

== References ==
 

== Bibliography ==
* Clarke, David B. & Doel, Marcus A. Moving Pictures/Stopping Places: Hotels and Motels on Film. Lexington Books, 2009.
 
== External links ==
*  

 

 
 
 
 
 
 


 

 