The Venetian Affair (film)
{{Infobox film
| name           = The Venetian Affair
| image          = Venetianaffairposter.jpg Frank McCarthy
| director       = Jerry Thorpe
| producer       = E. Jack Neuman Jerry Thorpe
| writer         = E. Jack Neuman
| starring       = Robert Vaughn Elke Sommer Boris Karloff Luciana Paluzzi
| music          = Lalo Schifrin
| cinematography = Milton R. Krasner
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
}}
The Venetian Affair  is a 1967 spy film directed by Jerry Thorpe. It stars Robert Vaughn and Elke Sommer and is based on a novel by Helen MacInnes. It was shot on location in Venice, Italy. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 474 

==Plot==
A former CIA agent, Bill Fenner, now a downbeat, loner journalist, is sent to Venice to investigate the shock suicide bombing by an American diplomat at a peace conference.

CIA chief Frank Rosenfeld specifically requests Fenner come out of retirement because one of the suspects in the case is Fenners ex-wife, Sandra Fane, who is believed to be a Communist sympathizer. A secret report by Dr. Vaugiroud could be the key, but Fenners and Fanes lives are greatly endangered, particularly at the hands of a mysterious man named Wahl, while trying to unravel the plot.

==Cast==
* Robert Vaughn as Bill Fenner
* Elke Sommer as Sandra Fane
* Felicia Farr as Claire
* Boris Karloff as Vaugiroud
* Ed Asner as Rosenfeld
* Karl Boehm (Karlheinz Böhm) as Robert Wahl

"The Venetian Affair" was released in America on region 1 DVD by Warner Archives on August 23, 2011. It is widescreen.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 

 