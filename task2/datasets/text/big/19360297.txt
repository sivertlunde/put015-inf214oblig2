Bear Shooters
{{Infobox film
| name           = Bear Shooters
| image          =Bear shooters TITLE.JPEG
| image size     =
| caption        = 
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         =
| narrator       =
| starring       = Bobby Hutchins Allen Hoskins Jackie Cooper Mary Ann Jackson Norman Chaney Pete the Pup
| music          = Ray Henderson
| cinematography = Len Powers
| editing        = Richard C. Currier MGM
| released       =  
| runtime        = 20 16"
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by Robert F. McGowan.    It was the 98th (tenth talking) Our Gang short that was released.

==Plot==
Spud wants to go camping and shoot lots of bears with Jackie, Chubby, and Farina. But his mother has forced him to look after his little brother Wheezer, who has the croup.
In caring for his brother he must periodically apply ointment to his chest. He tells his friends he must stay home and grease Wheezer. He tries to get his sister Mary Ann to do this but she insists on going camping. Spud decides to merely go camping anyway and take both Wheezer and Mary Ann along. The gang all go together in an old dilapidated wagon guided by Dinah The Mule. The trip initially goes smoothly, but after some time two bootleggers who have themselves concealed in the area spot the gang and decide to try to get the gang to leave. One of the bootleggers dresses up like an ape in order to scare the gang away. While he scares the gang, the gang manages to trap the ape. Eventually the gang ends up leaving the forest due to a skunk spraying the area, not the bootlegger in the ape suit.

==Notes==
Bear Shooters is one of four sound Our Gang shorts that fell into the public domain after the copyright lapsed in the 1960s (the other three being Schools Out (1930 film)|Schools Out, Our Gang Follies of 1938 and Waldos Last Stand). As such, these films frequently appear on inexpensive video and/or DVD compilations.

 
==Cast==
===The Gang===
* Norman Chaney as Chubby
* Jackie Cooper as Jackie
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer
* Mary Ann Jackson as Mary Ann
* Leon Janney as Donald Spud
* Pete the Pup as Himself
* Dinah the Mule as Herself

===Additional cast===
* Fay Holderness as Spuds mother Charlie Hall as Charlie (Bootlegger #2)
* Bob Kortman as Bob (Bootlegger #1)
* Charles Gemora as Charlie (in gorilla suit)

==References==
 

==External links==
 
*  

 
 
 
 
 
 
 
 

 