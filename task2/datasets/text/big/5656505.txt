The Cameraman
{{Infobox film
| name           = The Cameraman
| image          = The_cameraman_poster.jpg
| caption        = Theatrical poster
| director       = Edward Sedgwick Buster Keaton
| producer       = Buster Keaton Lawrence Weingarten
| writer         = Story:   Harold Goodwin
| music          =
| cinematography = Reggie Lanning Elgin Lessley
| editing        = Hugh Wynn
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}} silent comedy Harold Goodwin, and others.   

The Cameraman was Keatons first film with Metro-Goldwyn-Mayer. It is considered by fans and critics to be Keaton still in top form, and it was added to the National Film Registry in 2005 as being deemed "culturally, historically, or aesthetically significant."

Within a little over a year, however, MGM would take away Keatons creative control over his pictures, thereby causing drastic and long-lasting harm to his career. Keaton was later to call the move to MGM "the worst mistake of my career." 

The Cameraman was at one point considered a lost film. However, a complete print was discovered in Paris in 1968. Another print, of much higher quality, although missing some footage, was discovered in 1991. The two prints were combined into a best-available quality version which is regularly screened around the world.

==Plot== Harold Goodwin), an MGM cameraman who has designs on Sally himself, mocks his ambition.
 double exposes over exposes much of the footage, and the rest is simply no good.  Despite this setback, Sally agrees to go out with Buster, after her Sunday date cancels. They go to the city plunge (pool), where Buster gets involved in numerous mishaps. Later, Harold offers Sally a ride home; Buster has to sit in the rumble seat, where he gets drenched in the rain.

The next day, Sally gives him a hot tip she has just received that something big is going to happen in Chinatown. In his rush to get there, he accidentally runs into an organ grinder, who falls and apparently kills his monkey.  A nearby cop makes Buster pay for the monkey and take its body with him. The monkey turns out only to be dazed and joins Buster on his venture.
 Tong War", narrowly escaping death on several occasions. At the end, he is rescued from Tong members by the timely arrival of the police, led by a cop (Harry Gribbon) who had been the unintentional victim of several of Busters antics over the last few days. The cop tries to have him committed to the mental hospital, but Buster makes his escape with his camera intact.

Returning to MGM, Buster and the newsreel companys boss are dismayed to find that he apparently forgot to load film into his camera. When Sally finds herself in trouble for giving Buster the tip, Buster offers to make amends by leaving MGM alone once and for all.

Buster returns to his old job, but does not give up on filming, setting up to record a boat race. He then discovers that he has Tong footage after all; the mischievous monkey had switched the reels. Sally and Harold are speeding along in one of the boats. When Harold makes too sharp a turn, the two are thrown into the river. Harold saves himself, but Sally is trapped by the circling boat. Buster jumps in and rescues her.  When Buster rushes to a drug store to get medical supplies to revive her, Harold returns and takes credit for the rescue. The two go off, leaving the broken-hearted Buster behind.

Buster decides to send his Tong footage to MGM free of charge. The boss decides to screen it for Harold and Sally for laughs, but is thrilled by what he sees, calling it the best camerawork he has seen in years. They also see footage of Busters rescue of Sally (taken by the monkey). The boss sends Sally to get Buster. She tells him he is in for a great reception. Buster assumes a ticker-tape parade is in his honor, whereas it is really for Charles Lindbergh.

==Cast==
* Buster Keaton as Buster
* Marceline Day as Sally Richards Harold Goodwin as Harold Stagg
* Sidney Bracey as Edward J. Blake, the boss
* Harry Gribbon as Hennessey, the cop
* The Monkey as The Monkey
;Uncredited cast
* Richard Alexander as The Big Sea Lion,
* Edward Brophy as Man in Bath-House, who insists on sharing Busters tiny changing room
* Ray Cooke as Office Worker
* Vernon Dent as Man in Tight Bathing Suit
* William Irving as Photographer
* Harry Keaton as Man in Swimming Pool
* Louise Keaton as Woman in Swimming Pool
* Charles Lindbergh as Himself (archive footage)
* Bert Moorhouse as Randall

==Critical reception==
The film was well received by film critics.

Critic Mordaunt Hall, writing for the New York Times, liked the film and the work of Buster Keaton.  He said, "Mr. Keatons latest effort is The Cameraman, which is filled with guffaws and grins, the sort of thing with many original and adroitly worked-out gags. But whether they belong to the story is immaterial...There are other sections that are wild and watery, but nonetheless humorous." 

==Honors==
In 2005, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 