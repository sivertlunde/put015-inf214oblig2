Anbu Thollai
{{Infobox film
| name           = Anbu Thollai
| image          = 
| caption        =
| director       = Hayaath
| producer       = 
| story          = 
| screenplay     = 
| starring       = Pandiarajan Ravali Chinni Jayanth
| music          = Soundaryan
| cinematography =
| editing        =
| studio         = 
| distributor    = 
| released       =  
| country        = India
| budget         =
| runtime        = 
| language       = Tamil
}} Tamil action film written and directed by Hayath. The film, which starred Pandiarajan, Ravali and Chinni Jayanth in leading roles, released on 14 February 2003.   

==Cast==
*Pandiarajan as Singamuthu
*Ravali as Chinnathayi
*Chinni Jayanth
*R. Sundarrajan (director)|R. Sundarrajan as Devarajan
*Ganthimathi
*Vinu Chakravarthy
*Shakeela as Padmini

==Soundtrack==
*Mundaasukatti - Srinivas, Srivardhini
*Kalyanam Unakenna - Manikka Vinayagam, Nirmala
*Adicha Adikkanum - Swarnalatha

==Release==
The film opened to poor reviews,  with a critic giving the film "0 stars out of 4", claiming it as "one of the worst Tamil films of the year".   

==References==
 

 
 
 
 


 