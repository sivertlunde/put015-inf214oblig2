Band of Angels
 

{{Infobox film
| name           = Band of Angels
| image          = Band of Angels 1957.jpg
| image size     =
| caption        = Theatrical poster
| director       = Raoul Walsh
| producer       = Ben Roberts
| narrator       =
| starring       = Clark Gable Yvonne DeCarlo Sidney Poitier Efrem Zimbalist, Jr.
| music          = Max Steiner	
| cinematography = Lucien Ballard
| editing        = Folmar Blangsted
| distributor    = Warner Bros.
| released       = 1957
| runtime        = 125 minutes United States
| language       = English
| budget         =
| gross = $2.5 million (US) 
| preceded by    =
| followed by    =
}}
 romantic drama film set in the American South before and during the American Civil War, based on the novel of the same name by Robert Penn Warren. It starred Clark Gable, Yvonne De Carlo, and Sidney Poitier. The movie was directed by Raoul Walsh.

==Plot== pass for white, she is far too valuable to risk losing.

Amantha is put up for auction. When she is callously inspected by a coarse potential buyer, she is rescued from further humiliation by Hamish Bond (Clark Gable), who outbids the cad, paying an exorbitant price for her. Expecting the worst, Amantha is surprised to be treated as a lady, not a slave, by her new owner. At his city mansion, she meets his key slaves, his housekeeper (and former lover) Michele (Carolle Drake) and his conflicted right-hand-man Rau-Ru (Sidney Poitier). Rau-Ru is grateful for the kindness, education and trust Hamish has bestowed on him, but hates him anyway.

As time goes on, Amantha and Hamish fall in love. He arranges to send her to freedom in the North, but she decides to remain with him. To complicate matters further, Hamish harbors a terrible secret from his past that troubles his conscience.

Then, the Civil War breaks out, and New Orleans eventually falls to the Union. Hamish becomes a wanted man when he joins the other plantation owners in burning their crops in defiance. He and Amantha are helped to escape by Rau-Ru, who had fled and joined the Union Army.

==Cast==
*Clark Gable as Hamish Bond
*Yvonne De Carlo as Amantha Starr
*Sidney Poitier as Rau-Ru
*Efrem Zimbalist, Jr. as Lieutenant Ethan Sears, who falls in love with Amantha, believing her to be white
*Rex Reason	as Captain Seth Parton, an old friend from her past
*Patric Knowles as Charles de Marigny, a neighboring plantation owner
*Torin Thatcher as Captain Canavan, Bonds seafaring friend from his past
*Andrea King as Miss Idell
*Ray Teal as Mr. Calloway
*Russell Evans as Jimmee, Bonds steward
*Carolle Drake as Michele
*Raymond Bailey as Mr. Stuart, a plantation owner
*Tommie Moore as Dollie, Bonds house servant
*William Schallert as Union Lieutenant (uncredited)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 