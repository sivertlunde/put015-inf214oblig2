Z'har
 

{{Infobox film
| name           = Zhar 
| image          = 
| caption        = 
| director       = Fatma Zohra Zamoun
| producer       = Les Films du Cygne
| writer         = 
| starring       = Fadila Belkebla, Kader Kada, Eddy Lemar, Fatma Zohra Zamoun, Omar Zamoun, Saliha Ziani, Allel Ziani, Guermia Douaifia, Khlifi El Eulmi, Morad Gourissi
| distributor    = 
| released       = 2009
| runtime        = 78
| country        = Algeria France
| language       = 
| budget         = 
| gross          = 
| screenplay     = Fatma Zohra Zamoun
| cinematography = Benjamin Chartier
| editing        = Julien Chiaretto
| music          = Olivier Manganelli, Takfarinas
}}

Zhar  is a 2009 film.

== Synopsis ==
1997: Alia is a Parisian photographer, travelling from Tunis to Constantine (Algiers) to see her sick father. Cherif is a writer and has just read, according to the newspapers, that he’s dead. Their driver is a cab driver used to doing the Tunis-Constantine route. 2007: Fatma Zohra asks her brother to go with her on a location scout. The film is dear to her heart because it portrays the violence that swept Algiers during the nineties. The crew starts out on a two thousand kilometre journey that leads to a hypothetical fiction or the dream of one during which the main characters get to know one another. But the project can’t find financing. How does one carry out a fiction when all is against you?

== Awards ==
* International Film Festival of Kerala (India)
* Pune International Film Festival (India)
* Famafest (Portugal)
* Festival Cinema Africano
* AsI
* America Latina de Milano (Italia)
* Festival Indie Lisboa (Portugal)

== References ==
 

 
 
 


 