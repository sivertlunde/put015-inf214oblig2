Click Clack Jack: A Rail Legend
{{Infobox film
| name           = Click Clack Jack: A Rail Legend
| image          = Click Clack Jack.jpg
| image_size     = 
| alt            = 
| caption        = Film poster
| director       = Ryan Bodie
| producer       = 
| writer         = Chris Easterly	
| narrator       = 
| starring       = Keith Peyton Thomas James Burns Robert Pierce
| music          = John Doryk
| cinematography = 
| editing        = 
| studio         = Studio 26 Productions Sunday Rising Films
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Christian family film directed by Ryan Bodie. The film has received several film festival awards, including Best Short Film at the New York International Independent Film and Video Festival. It was produced in California on an estimated budget of $1,000,000.

==Plot==
Click Clack Jack is a family film about an 1870s rail engineer, "Click Clack" Jack, who uses principles from the Bible to try and save Potters Gap from the evil Baron Snodgrass. Snodgrass is seeking to destroy the town to further his own ambition and look for gold beneath it.

==Cast==
*Keith Peyton Thomas as Click Clack Jack
*James Burns as Hobo
*Robert Pierce as Baron Snodgrass
*Mike Muscat as Museum Guide

==Production== West Coast and filming in California. The idea for the film was originally conceived in 2000. The filmmakers have said they found technical crew members whose resumes include work with shows such as Heroes (TV series)|Heroes, Scrubs (TV series)|Scrubs, Without a Trace and the Christian-based football film Facing the Giants.  Studio 26 Productions was involved in producing the film. 
 Christian entertainment. There is a real need for both. My dream is to use profit from Studio 26 to produce high-end productions for ministries around the world and to produce clean entertainment for all ages." 

==Awards==
*2008 Christian WYSIWYG Film Festival
Best Feature Film (Won) 
Best Production Value (Won)
*2008 New York International Independent Film and Video Festival
Best Short Film (Won)
*2009 Texandance International Film Festival
Best Family Film (Won) 

==References==
 

==External links==
*  
*  
*   at Facebook

 
 
 