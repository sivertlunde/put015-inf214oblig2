In Love with Alma Cogan
{{Infobox film
| name           = In Love with Alma Cogan 
| image          = In Love with Alma Cogan.jpg
| caption        = Theatrical release poster
| director       = Tony Britten
| producer       =  Anwen Rees-Myers Katya Mordaunt
| screenplay     = Tony Britten
| based on       = 
| starring       = Roger Lloyd-Pack   Niamh Cusack  Gwyneth Strong 
| music          = Tony Britten
| cinematography = Ole Brett Birkeland
| editing        = Jeremy Brettingham
| studio         = Capriol Films
| distributor    = 
| released       =   
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         = £200,000
| gross          = 
}}

In Love with Alma Cogan is a 2011 British romantic comedy film written and directed by Tony Britten. It was filmed in and around the Pavilion Theatre, Cromer Pier, Norfolk, England. 

==Plot==
The film revolves around, Norman, a world-weary manager of a pier theatre in a seaside resort. Norman has worked in the theatre for all of his life, but will not accept that the local council, which own the theatre are planning to install more commercial management in an attempt to boost audience numbers. As the story unfolds he realises it may be time to move on and put behind him the ghost of 50s & 60s singer Alma Cogan, who performed at the theatre many years ago. Sandra, his devoted long-suffering assistant and Norman decide to leave the theatre to fulfill her dream of being a professional singer and unexpectedly enjoying a late blossoming romance.

==Cast==
*Roger Lloyd-Pack               - Norman
*Niamh Cusack                   - Sandra
*Gwyneth Strong                 - Laura
*Neil McCaul                    - Eddie
*Christian Brassington          - George
*Keith Barron                   - Cedric Simon Green- Julian Gary Martin- Larry
*Ann Firbank                    - Mrs. Craske
*Terry Molloy                   - Barry Bates
*Catrine Kirkman                - Alma Collins
*Daniel Bardwell                - Young Norman
*Tim Bell (actor)                       - Mr. Whipit
*Kris Dillon Jr                 - Adonis
*John Hurt                      - Master of Ceremonies

==Awards==
In March, 2012 at the Canada International Film Festival the film won an Award of Excellence. 

==References==
 

==External links==
* 

 
 
 
 