The Midnight Hour
{{Infobox television film
| name           = The Midnight Hour
| image          = The Midnight Hour DVD.jpg
| caption        = The Midnight Hour DVD cover
| genre          = 
| director       = Jack Bender
| producer       =  Ervin Zavada
| writer         = Bill Bleich Shari Belafonte-Harper Jonna Lee Dick Van Patten Kurtwood Smith
| music          = Brad Fiedel
| cinematography = Rexford L. Metz
| editing        = David A. Simmons ABC
| Capital Cities
| released       =  
| runtime        = 94 minutes
| country        = United States
| awards         =
| language       = English
| network        = ABC
| budget         =
}}
The Midnight Hour (also known as In the Midnight Hour) is a  , LeVar Burton, Peter DeLuise, and Dedee Pfeiffer. The plot focuses on a small New England town that becomes overrun with zombies, witches, vampires, and all the other demons of hell after a group of teenagers unlock a centuries-old curse on 
Halloween. The film  marks Macaulay Culkins first screen role as an uncredited trick-or-treater.

The film was released on VHS and DVD by Anchor Bay Entertainment in 2000. In addition to an original musical number, "Get Dead", the films soundtrack features songs by Creedence Clearwater Revival, Sam the Sham and the Pharaohs, Three Dog Night, and The Smiths. {{cite AV media
 | people =
 | year =2000
 | title =The Midnight Hour
 | trans_title =
 | medium =film
 | url =
 | accessdate =
 | archiveurl =
 | archivedate =
 | format =DVD
 | time =
 | location =
 | publisher =Anchor Bay Entertainment
 | id =
 | isbn =
 | oclc =
 | quote =
 | ref =
}} 

== Plot ==
It is Halloween in the small town of Pitchford Cove located somewhere in New England, and five high school friends, Phil (Lee Montgomery), Mary (Dedee Pfeiffer), Mitch (Peter DeLuise), Vinnie (LeVar Burton), and Melissa (Shari Belafonte), plan on making it a night they will never forget. They steal outfits from the towns historic museum and come upon other old artifacts, including an old trunk encasing a paper scroll which contains an ancient curse. When Melissa, latent sorceress, recites the curse at the local cemetery, things take a turn for the worse.
 Jonna Lee), dressed in a vintage 1950s cheerleader outfit, who warns him that the whole town is in danger.

Meanwhile, Lucinda and the various undead crash the costume party. At first, nobody pays much attention to them since everyone is in costume. However, Lucinda begins turning the party guests into vampires, starting with her great-great-great-great-granddaughter Melissa.

When Sandy discovers that Phil and his friends recited the ancient spell in the cemetery, they realize that the whole town is being overrun by the living dead and decide to team up to break the curse. The only way to do so is to find the Grenville Spirit Ring inside the grave of a witch-hunter Nathaniel Grenville - who, coincidentally, was Phils great-great-great-great-grandfather and slave owner of Lucinda Cavender her arch-nemesis - and use it to undo the curse. It is up to Phil and "good ghost" Sandy to restore the town to normal by midnight before it is too late and the curse becomes permanent.

== Cast ==
 
 
*Lee Montgomery as Phil Grenville
*Shari Belafonte as Melissa Cavender
*Peter DeLuise as Mitch Crandall
*LeVar Burton as Vinnie Davis
*Dedee Pfeiffer as Mary Masterson Jonna Lee as Sandy Matthews
*Jonelle Allen as Lucinda Cavender
*Cindy Morgan as Vicky Jensen
*Kurtwood Smith as Captain Warren Jensen
*Dick Van Patten as Martin Grenville
*Sheila Larken as Janet Grenville
*Wolfman Jack as the radio DJ Kevin McCarthy as Judge Crandall
 
*Mark Blankfield as The Ghoul
*Hank Garrett as Sgt. Thompson
*Dennis Redfield as Lester Mitchell
*Mickey Morton as Vernon Nestor
*Vachik Mangassarian as Opera singer
*Joe Gieb as The Elf
*Bill DeLand as Man in Police Station
*Laura Owens as Mother
*Adam Myman as Son
*Jennifer Shockey as Tammy
*Macaulay Culkin as a Halloween kid (uncredited) 
 

==Home media== Anchor Bay Entertainment released it on Region 1 DVD on September 19, 2000.     The film was previously released on VHS by Anchor Bay Entertainment on July 20, 1999. Both releases of the film are out of print and are extremely rare among collectors.  

==Reception==
Gary Militzer of DVD Verdict called it "a mediocre made-for-TV horror/comedy" that was not worthy of being released on DVD.   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle said, "Even zombie movie completists will have a hard time stomaching this lame made-for-TV drivel". 

==Soundtrack==
The film features the following songs, as adapted from the film credits:
 
 
*"In the Midnight Hour" by Wilson Pickett Bad Moon Rising" by Creedence Clearwater Revival
*"Lil Red Riding Hood" by Sam the Sham and the Pharaohs Baby Im Yours" by Barbara Lewis
*"Mama Told Me Not To Come" by Three Dog Night
*"Devil or Angel" by Bobby Vee
 
*"How Soon Is Now?" by The Smiths Sea of Love" by Del Shannon Shari Belafonte-Harper
*"Clap for the Wolfman" by The Guess Who
*"Sea of Love" by Phil Phillips
 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 