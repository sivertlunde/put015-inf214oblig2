Zärtliche Haie
{{Infobox Film
| name           = Zärtliche Haie
| image          = 
| image size     = 
| caption        = 
| director       = Michel Deville
| producer       = Aldo von Pinelli
| writer         = Nina Companéez Gabrielle Upton
| narrator       = 
| starring       = Anna Karina
| music          = 
| cinematography = Heinz Hölscher
| editing        = 
| distributor    = 
| released       = 10 March 1967
| runtime        = 89 minutes
| country        = Germany France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}

Zärtliche Haie ( ) is a 1967 German-French thriller film directed by Michel Deville and starring Anna Karina.   

==Cast==
* Anna Karina - Elena / Costa
* Mario Adorf - Spion SB 3
* Gérard Barray - Gregory
* Fritz Tillmann - Admiral
* Scilla Gabel - Zeezee
* Klaus Dahlen - Alexander
* Rainer von Artenfels - Philander
* Franco Giacobini - Fähnrich

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 