Under Capricorn
 
 
{{Infobox film
| name           = Under Capricorn
| image          = Under Capricorn poster.jpg
| caption        = Theatrical poster
| director       = Alfred Hitchcock Sidney Bernstein (uncredited)
| writer         = Novel:   Screenplay: James Bridie Michael Wilding Ingrid Bergman Joseph Cotten Margaret Leighton Cecil Parker
| music          = Louis Levy (musical direction) Richard Addinsell (score)
| cinematography = Jack Cardiff
| editing        = Bert Bates
| studio         = Transatlantic Pictures
| distributor    = Warner Bros.
| released       = 8 September 1949
| runtime        = 117 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 historical thriller Under Capricorn Helen Simpson, with a screenplay by James Bridie. It was adapted to the screen by Hume Cronyn. This was Hitchcocks second film in Technicolor, and like the preceding color film Rope_(film)|Rope (1948), it also featured 10-minute takes.
 mystery involving a love triangle. Although the film is not exactly a murder mystery, it does feature a previous killing, a "wrong man" scenario, a sinister housekeeper, class conflict, and very high levels of emotional tension, both on the surface and underneath.
 Capricorn is an astrological sign dominated by the goat, which is a symbol of sexual desire.

==Plot== Michael Wilding). transported convict, possibly a murderer. Sam says that because he has bought the legal limit of land, he wants Charles to buy up land and then sell it to him for a tidy profit so that Sam can accumulate even more frontier territory. Though the Governor instructs him not to go, Charles is invited to dinner at Sams house and discovers that he already knows Sams wife, Lady Henrietta (Ingrid Bergman). She is now a hopeless alcoholic who is socially shunned, but she used to be a good friend of Charles sister when they were children in Ireland.

Sam invites Charles to stay at his house, hoping it will cheer up his wife, who is on the verge of madness. The housekeeper, Milly (Margaret Leighton), has taken over running the household and secretly feeds Henrietta alcohol, hoping to destroy her and win Sams affections.

Gradually, Charles restores Henriettas self-confidence. They become so close that they even share a passionate kiss. Henrietta explains that she and Sam are bound together profoundly: when she was young, Sam was the handsome stable boy. Overcome with desire, they ran away and married at Gretna Green. Henriettas brother, furious that aristocratic Henrietta had paired up with a lowly servant, confronted them. Her brother shot at Sam and missed; she shot her brother fatally. Sam made a false confession to save her, and was sent to the penal colony in Australia. She followed him and waited six years in abject poverty for his release.

Sam becomes furious at Charles after listening to Millys exaggerated stories and tells him to leave. Taking Sams mare in the dark, Charles has a fall and the horse breaks a leg. Sam has to shoot her dead and, in a struggle over the gun, wounds Charles. At the hospital, 
Henrietta confesses to the Governor that Sam was wrongly convicted of the original crime; she was the one who killed her brother. By law she should be deported back to Ireland to stand trial.

Milly, still plying Henrietta with drink, is inducing hallucinations by using a real shrunken head. Milly then tries to kill Henrietta with an overdose of medicine; she is caught in the act and leaves in disgrace.

The Governor, Sir Richard, has Sam arrested and charged with the attempted murder of Charles. Sir Richard ignores Henriettas claim that Sam is innocent of both crimes. However, Charles decides to bend the truth; he says, on his word as a gentleman, that there was no confrontation, and no struggle over the gun. It was all an accident.

Finally we see Sam and Henrietta together smiling. They bid Charles a fond and grateful farewell; he is going back to Ireland.

== Cast ==
  Michael Wilding as Hon. Charles Adare, nephew of the governor
* Ingrid Bergman as Lady Henrietta Flusky, old friend of Charles sister
* Joseph Cotten as Samson "Sam" Flusky, successful businessman and Henriettas husband
* Margaret Leighton as Milly, Fluskys housekeeper
* Cecil Parker as the new Governor of New South Wales, Sir Richard
* Denis ODea as Mr. Corrigan, Attorney General
* Jack Watling as Winter, Fluskys paroled butler
* Harcourt Williams as the Coachman
* John Ruddock as Mr. Cedric Potter, bank manager
* Bill Shine as Mr. Banks
* Victor Lucas as the Rev. Smiley Ronald Adam as Mr. Riggs
* Francis de Wolff as Major Wilkins
* G.H. Mulcaster as Dr. Macallister Martin Benson as the man carrying a shrunken head (uncredited)
Kitchen Staff at Mr. Fluskys House
* Olive Sloane as Sal
* Maureen Delaney as Flo
* Julia Lang as Susan
* Betty McDermott as Martha
 

==Production== Sidney Bernstein Michael Wilding, Ingrid Bergman, Joseph Cotten, and Margaret Leighton. {{cite web url        =http://www.imdb.com/title/tt0042004/companycredits title      =Under Capricorn (1949) – Company Credits website    =Internet Movie Database (IMDb) accessdate =2013-08-10}} 

The film was Hitchcocks second film in Technicolor and uses ten-minute takes similar to those in Hitchcocks previous film Rope (film)|Rope (1948). It is thought that the audience had imagined Under Capricorn was going to be a thriller, which it was not — the plot was a domestic love triangle with a few thriller elements thrown in — and this led to its box office failure. However, the public reception of the film may have been damaged by the revelation in 1949 of the married Bergmans adulterous relationship with — and subsequent pregnancy by — the married Italian film director Roberto Rossellini. {{cite web url        =http://www.ingridbergman.com/about/bio3.htm title      =About Ingrid – Biography, page 3 website    =The Official Ingrid Bergman Web Site accessdate =2013-08-10}} 

=== The long take ===
In Style and Meaning: Studies in the Detailed Analysis of Film, Ed Gallafent says: 

 
The use of the long take in Under Capricorn relates to three elements of films meaning.

# Ideas of accessible and inaccessible space as expressed in the gothic house.
# The form in which character inhabit their past
# The divergence or convergence of eyelines – the gaze that cannot, or must meet another’s.

All of these three elements can be linked to concepts of Guilt and Shame. In 1 and 2, the question is how something is felt to be present. In 3, it is difference between representation or sharing, of the past as flashback, and of the past as spoken narrative, where part of what is being articulated is precisely the inaccessibility of the past, its experience being locked inside the speaker. As for 3, the avoided gaze is determining physical sign of shame.
 

Gallafent, professor of film at University of Warwick, also explains these aspects of Under Capricorn:

 
The inscription on the Fluskys mansion — Minyago Yugilla — means "Why weepest thou?"

St.  .
 
 Kamilaroi (Gamilaraay), Australian aboriginal language. See also this similar translation  of the phrase "Minyilgo yugila".

==Background== Alfred Hitchcock cameo: A signature occurrence in three-quarters of Hitchcocks films, he can be seen in the town square during a parade, wearing a blue coat and brown hat at the beginning of the film. He is also one of three men on the steps of the Government House 10 minutes later. Bankers Trust Company, which had financed the film, repossessed the film, which then was unavailable until the first US network television screening in 1968. In the Truffaut interview, Hitchcock also mentioned the New York Times reviewer, who wrote that the viewer had to wait almost 100 minutes for the first suspenseful moment. 
* In Peter Bogdanovichs interview with Alfred Hitchcock, Bogdanovich mentions that French critics writing for Cahiers du cinéma in the 1950s considered Under Capricorn one of Hitchcocks finest films. {{cite web url        =http://www.imdb.com/title/tt0042004/trivia title      =Under Capricorn (1949) – Trivia website    =Internet Movie Database (IMDb) accessdate =2013-08-10}} 
* Playwright James Bridie, who wrote the screenplay for Under Capricorn, is famous for his Biblical plays, such as his Jonah and the Whale. Other Christian references in Under Capricorn include a moment near the end of the film, where Milly says "The Lord works in mysterious ways," and Samson Flusky says "What is it they say in the Bible? Great Gulf Fixed."
* Cecil Parkers character Sir Richard may be a representation of General Sir Richard Bourke, who was Governor of New South Wales from 1831 to 1837.

==See also==
 
*1949 in film
*British films of 1949
*List of drama films
*List of films set in Australia
 

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*   Eyegate Gallery

 

 
 
 
 
 
 
 
 
 
 