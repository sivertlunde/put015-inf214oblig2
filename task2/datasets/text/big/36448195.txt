Vimochanam
{{Infobox film|
| name = Vimochanam
| image =
| caption  = Advertisement for Vimochanam
| director = T. Marconi
| writer =  
| starring = Hemalatha  Kanthamani  Baby Jaya  Indira  Bhagirathi  T. P. Sundari  Selva  Suguna
| producer = 
| music = Ramani
| distributor =
| cinematography = 
| editing = 
| released =  1939 
| runtime =  
| country = India
| language = Tamil
| budget =
}}
 print or stills of this film are known to exist presently, and most film historians are "hardly aware" of this film.   

==Plot==
The male lead, Arumugham, sell his wifes jewelry to buy alcohol until probation in the Salem district offers much-needed relief. He soon goes to jail for trying to brew liquor illicitly. On his release, he finds the liquor shop has become a tea-stall and his wife destitute, leading to his reform.

==Cast==
* Hemalatha
* Kanthamani
* Baby Jaya
* Indira
* Bhagirathi
* T. P. Sundari
* Selva
* Suguna

==Production==
The film was inspired by Rajaji who published a pro-prohibition magazine titled Vimochanam, from his Ashram at Tiruchengode located near Salem. Rajaji obtained most of the material for the publication from the United States where prohibition was then in force. 
 Italian cinematographer T. Marconi, who was then living in Madras, directed the film. 

==Soundtrack==
The films music was composed by Ramani, the founder of the Ramani School of Music, Mylapore. The background score was composed by Sarma Brothers, who also worked for All India Radio. The film had many songs performed by Lalitha. Popular songs include Kallai ozhithida sattamondru Chennai Congress aatchiyil seithanarey…., Mahaan Rajaji-yai ellorum vaazhthuvomey… and Naattuomey jayakodi…  

==Reception==
Film historian Randor Guy praised the film for its "meaningful lyrics, presentation and all roles being played by girl children." 

==References==
 

==External links==
*  

 
 
 