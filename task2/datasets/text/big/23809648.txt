Sealed Cargo
 
{{Infobox film
| name           = Sealed Cargo
| image          = Poster of the movie Sealed Cargo.jpg
| image_size     =
| caption        =
| director       = Alfred L. Werker
| producer       = Warren Duff Samuel Bischoff
| writer         = Edmund Gilligan (novel) Dale Van Every Oliver H. P. Garrett Roy Huggins
| narrator       =
| starring       = Dana Andrews Carla Balenda Claude Rains
| music          =
| cinematography =
| editing        =
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 87-90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} 1951 war Nazis and their U-boats. It was based on the novel The Gaunt Woman by Edmund Gilligan.

==Plot== Battle of Grand Banks fishing grounds. Once at sea, another Dane, Holger, reports that the radio has been sabotaged. As Bannon knows all of the crew well except for Konrad and fellow Dane Holger, he suspects one of them or even Margaret of being a German agent.
 Danish for The Gaunt Woman), a Danish square rigged sailing ship. She appears to have been damaged in a storm and then shelled. Aboard, they discover only the dazed Captain Skalder and a dead body. He claims that his crew abandoned ship in a storm, and that he was subsequently attacked by a U-boat. The Daniel Webster tows the stricken Kvinde to Trabo.

Konrad is suspicious: he notes that the German gunfire hit above the waterline (rather than below it, where a gunner intending to sink a ship would aim), and that while the tarpaulin covering the ships boat is riddled with bullet holes, the boat itself is undamaged.  Bannon and Konrad separately sneak below decks to search the hold. When they meet, Konrad has a pistol, but he gives it to Bannon to prove where his loyalties lie. They accidentally discover a second, hidden hold containing rack upon rack of torpedoes — the ship is a submarine tender|tender, covertly resupplying the U-boat "Wolfpack (naval tactic)|wolfpacks". They watch undetected as Holger enters the hold and uses a radio to signal the Germans. However, before the pair can alert the military, Skalders crew arrives in boats, so they pretend they know nothing. Skalder plans to resupply the U-boats at Trabo.

A Canadian flying boat lands in the harbor, and an officer inspects Skalders papers. Finding nothing wrong, he informs Skalder that a corvette will arrive the next day to inspect his cargo. Bannon offers to leave one of his two Danish crewman as a witness, allowing him to rid himself of the spy Holger without arousing suspicion.
 dories and return. Bannon sets up a night ambush; when the Germans come to take the villagers prisoner, the invaders are wiped out. Bannon and his men then set fire to the Kvinde under cover of darkness. In the resulting confusion, they board, overpower or kill the remainder of the crew, and free Margaret, who had been taken as a hostage. 

Skalder claims to have set the ship to blow up in twenty minutes. Bannon does not believe him, but takes the ship out to sea, intending to destroy her safely away from the village. He and his men rig some of the torpedoes to explode. The Kvinde is approached by two U-boats seeking supplies. Skalder manages to get a gun and wounds his guard, Konrad, before he is killed. As a third U-boat surfaces, Bannon helps Konrad into a boat and flees under German gunfire. When the ship explodes, the resulting wave swamps the submarines, sinking them.

==Cast==
*Dana Andrews as Pat Bannon
*Carla Balenda as Margaret McLean
*Claude Rains as Captain Skalder
*Philip Dorn as Konrad
*Onslow Stevens as Commander James McLean, Margarets father
*Skip Homeier as Steve
*Eric Feldary as Holger
*J. M. Kerrigan as Skipper Ben
*Arthur Shields as Kevin Dolan
*Morgan Farley as Caleb

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 