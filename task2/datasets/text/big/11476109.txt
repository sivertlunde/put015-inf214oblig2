Cabin by the Lake
{{Infobox film
| name           = Cabin by the Lake
| image          =
| caption        =
| director       = Po-Chih Leong
| producer       = Neal H. Moritz
| writer         = David Stephens
| starring       = Judd Nelson Hedy Burress
| distributor    = USA Network
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
}} horror TV movie released in 2000. It tells the story of Stanley, played by Judd Nelson, a script writer who begins killing girls for research for a movie he is writing, where the villain does the same thing.  A 2001 sequel entitled Return to Cabin by the Lake was filmed and follows Stanleys attempts to direct a film based on his previous murders by posing as the films director.

==Plot==
The film opens with Stanley (Judd Nelson) on his boat in the middle of Summit Lake. After speaking with his agent, Regan (Susan Gibney) over the phone, Stanley accesses a hidden room in his house revealing a young woman who is chained to the floor by her ankle. He greets the woman and calls her Kimberly (Daniella Evangelista) as he places a plate of food in front of her. When he leaves the room, Kimberly walks over to the mirror and begins to cry, unaware that Stanley is studying her on the other side. Afterwards, Stanley brings her a fresh change of clothes and instructs her to dress, before gagging her with an orange and binding her hands behind her back. He forcibly escorts her onto his boat and drives out to the middle of Summit Lake, where he ties a cement block around her ankles. Curious about her predicament, he asks Kimberly how she feels, but the terrified woman does not respond. Stanley then pushes her into the lake, but promptly pulls her back up by the hair to study her expression before letting her sink to the bottom. Later, Stanley returns home to resume working on his script using the details he had gleaned from his latest victim. The next morning, he dives to the bottom of Summit Lake to tend his "garden", which consists of several women kidnapped and drowned in a similar fashion.

Seeking yet another victim, Stanley discovers Mallory (Hedy Buress), a young woman working at a movie theater. Intrigued by her self-confessed fear of water, Stanley follows Mallory after she leaves work and swerves in front of her, slamming the brakes on his van and forcing their vehicles to collide. After checking on Mallory, Stanley turns his attention to his dogs, which are heard barking from the van. Mallory follows him, but before she can react, Stanley throws the back doors wide and pushes her into the van, which is completely empty save for the recording of barking dogs, and an ominous message spelled out in yellow tape that reads, "Im the person your mother warned you about." 

Stanley locks his new victim away in his secret room, but Mallory is defiant and shows him no fear. Eventually, he lies to Mallory and tells her that hell let her go free, but that she would have to face her fear and go for a ride on his boat. He then blindfolds her and drives around for a while to disorient her before they board the boat. Stanley attempts to drown her in the lake, but she is quickly rescued by local police officer Boone and a couple who own a movie special-effects shop nearby. After taking time to recuperate, Mallory then gives the idea to make a plaster-mold of herself, with a camera as an eye, to catch the killer when he returns to his garden. When Stanley returns to the bodies, he discovers the ploy and quickly escapes, his identity hidden by his scuba mask.  

Mallory, assuming herself safe, stays at a hotel room for the night, but is soon discovered and recaptured by Stanley who immediately prepares to drown her again. However, complications arise when Stanleys agent Regan and the director to the film he is writing arrive at the cabin unexpectedly. He kills the director with a cleaver, then imprisons Regan with Mallory. He then takes them both to the lake to drown them, but Boone and the other police dive in and quickly release Mallory. Although Regans body is recovered, Stanley is presumed dead, having snagged his leg on a rope in his own garden. Mallory tries to recover from the experience, though she continues to be haunted by visions of her former captor.

The last scene has a person pitching a movie to a new agent, in which a killer buries his victims alive. The agent asks them how the person feels as theyre buried alive. The camera reveals the person to be Stanley with a new look, as he says "Im still doing the research."

==Cast==
*Judd Nelson as Stanley
*Daniella Evangelista as Kimberley Parsons
*Hedy Burress as Mallory
*Michael Weatherly as Boone
*Susan Gibney as Regan
*Bernie Coulson as Duncan
*Colleen Wheeler as Lauren Bob Dawson as Sheriff
*G. Patrick Currie as Logan
*John B. Destry as Melvin (as John Destry)
*Guy Jellis as Deputy
*Rebecca Reichert as Babette
*Marnie Alton as Brooke
*Jennifer Carmichael as Tiffany
*Barbara Pollard as Hotel Manager

==Reception==
Critical reception for Cabin by the Lake and its sequel Return to Cabin by the Lake was largely negative.  Of the first film, Film Threat wrote that while the first act was entertaining, the remainder of the film "turns out to be just another movie that hasn’t found anywhere new to go."  The Palm Beach Post and Sun Sentinel both panned Cabin by the Lake, with the Palm Beach Post writing that the film "takes the horror genre to new TV lows".   
 Variety gave a positive review for Return to Cabin by the Lake, saying it was "Completely in tune with just how flaky showbiz types can be, this serial killer story is comfortable in the confines of feather-light, jokey fright". 

==Sequel==

A sequel was made and it was titled "Return to Cabin by the Lake". Judd Nelson reprised his role of Stanley Cauldwell.

==See also==
* Necrophilia
* Drowning

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 