Up the Creek (1984 film)
{{ Infobox film |
| name = Up the Creek
| image = Up the Creek (1984 film).jpg
| caption = Up the Creek theatrical poster Robert Butler
| producer = Michael L. Meltzer executive Samuel Z. Arkoff Louis S. Arkoff
| writer = Jim Kouf based on = story by Jim Kouf Jeff Sherman Douglas Grossman
| starring = {{Plainlist|
* Tim Matheson
* Dan Monahan
* Stephen Furst
* Jeff East
* Sandy Helberg
* Blaine Novak James B. Sikking
* Jennifer Runyon
* John Hillerman
}}
| cinematography = James Glennon Bill Butler
| music = William Goldstein Cheap Trick
| distributor = Orion Pictures Samuel Z. Arkoff & Louis S. Arkoff Production
| budget = $7 million
| gross = $11,708,269
| released = April 6, 1984
| runtime = 96 min. English
| country = United States
}} 1984 comedy Robert Butler. Although the film itself was not as popular as other "college romp" films, the four lead parts all came to the film with experience in popular comedies, most notably Animal House and Porkys.

==Plot summary== Lepetomane University (known derisively by some as "Lobotomy U"), are volunteered to compete in a collegiate raft race. They are "recruited" by Dean Burch who uses records of McGraws checkered past as a means of blackmail to get them to compete. "Youre not AT the bottom of the list. You ARE the bottom of the list!", says Burch. He even offers them degrees in the major of their choice as additional incentive. Theyre up against Ivy University, prep schoolers who, with the help of an Ivy alumnus named Dr. Roland Tozer, plan to cheat their way to the Winners Circle.

Their adversaries also include the Washington Military Institute, disqualified for their attempts to sabotage the other schools rafts. Captain Braverman, the leader of the Military men, has it in for McGraw because he personally curtailed the attempts to sabotage the other rafts. Also entered is a team of beautiful mixed-sex education|co-eds, one of whom ends up falling for Bob. The dangerous rapids as well as Ivys cheating end up disabling many of the other teams rafts. It is all down river adventure for the Lepetomane gang.

==Cast==
* Tim Matheson ... Bob McGraw
* Dan Monahan ... Max
* Sandy Helberg ... Irwin
* Stephen Furst ... Gonzer
* Jeff East ... Rex Crandall
* James Sikking ... Tozer (as James B. Sikking)
* Blaine Novak ... Captain Braverman
* Mark Andrews ... Rocky
* Jesse D. Goins ... Brown
* Julia Montgomery ... Lisa (as Julie Montgomery)
* Jennifer Runyon ... Heather Merriweather
* Romy Windsor ... Corky 
* John Hillerman ... Dean Burch
* Grant Wilson ... Reggie
* Jeana Tomasino ... Molly
* Will Bledsoe ... Roger van Dyke
* Robert Costanzo ... Campus Guard Charlie
* Ken Gibbel ... Campus Guard Leslie
* Hap Lawrence ... Gas Station Attendant
* Frank Welker as the voice of Chuck the Dog

==Production notes==
This film was filmed near Bend, Oregon.

Writer Jim Kouf later said Robert Butler "was not a great comedy director, he missed a lot of jokes.” 

==Soundtrack==
{{Infobox album  
| Name = Up the Creek
| Type = soundtrack
| Artist = Various artists
| Cover =
| Released = 1984
| Recorded =
| Genre = Rock music|Rock, Hard rock
| Length = 41:28
| Label = Pasha
| Producer =
| Last album =
| This album =
| Next album =
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =    
| noprose = yes
}}
 Up the Creek" – Cheap Trick         Heart       
# "30 Days in the Hole" – Kick Axe         Ian Hunter       
# "Chasin the Sky" – The Beach Boys        Shooting Star      
# "One Track Heart (Passion in the Dark)" – Danny Spanos        
# "Take It" – Shooting Star       
# "Two Hearts on the Loose Tonight" – Randy Bishop        
# "Get Ready Boy (Instrumental)" – Shooting Star

One song that was in the film, but not on the soundtrack is "First Girl President" by Namrac.

==Reception== Police Academy but was "rambunctious and raunchy enough to divert undemanding audiences." MOVIE REVIEW: THIS CREEK GOES WITH THE FLOW
Thomas, Kevin. Los Angeles Times (1923-Current File)   05 Apr 1984: k1.   The Washington Post called it "a moist smut movie" in which the best performance was given by the dog. Get Stuck Up This Creek and Youll Need a Shovel
The Washington Post (1974-Current file)   06 Apr 1984: WK21.   The New York Times called it "a ridiculous ordeal, all right, but certainly not in the way the filmmakers intended." Screen: Up the Creek, College Humor
By LAWRENCE VAN GELDER. New York Times (1923-Current file)   07 Apr 1984: 13.   Gene Siskel of the Chicago Tribune however said the film was "a good time", where Matheson, Furst and Helberg "play their roles with the same whimsical naturalness that made Bill Murray a star. They dont push themselves upon us, and that allows us to identify with them in a relaxed way. The result is a very tight script with breathing room. Thats most unusual for a teen comedy, and thats why Up the Creek is one of the best." Tempo: Teenage comedy flows fast in Up the Creek
Siskel, Gene. Chicago Tribune (1963-Current file)   11 Apr 1984: e4.  

==References==
 
==External links==
*   at the New York Times
*  by Roger Ebert
*  
*  
*  

 

 
 
 
 
 
 
 
 
 