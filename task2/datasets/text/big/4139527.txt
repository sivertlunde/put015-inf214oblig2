World Without Sun
{{Infobox film
| name           = World Without Sun
| image          = Le_Monde_Sans_Soleil.jpg
| image size     =
| caption        = Film poster
| director       = Jacques-Yves Cousteau
| producer       =
| writer = Jacques-Yves Cousteau and James Dugan
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =            
| runtime        =
| country        = France / Italy
| language       = French
| budget         =
| preceded by    =
| followed by    =
}}
World Without Sun ( ) is a 1964 French documentary film directed by Jacques-Yves Cousteau. The film was Cousteaus second to win an Academy Award for Best Documentary Feature, following The Silent World in 1956.

==Plot==
World Without Sun, a documentary produced and directed by Jacques Cousteau in 1964 chronicles Continental Shelf Station Two, or "Conshelf Two", the first ambitious attempt to create an environment in which men could live and work on the sea floor.  In it, a half-dozen oceanauts lived 10 meters down in the Red Sea off Sudan in a star-fish shaped house for 30 days.  The undersea living experiment also had two other structures, one a submarine hangar that housed a small, two man submarine referred to as the "diving saucer" for its resemblance to a science fiction flying saucer, and a smaller "deep cabin" where two oceanauts lived at a depth of 30 meters for a week.  The undersea colony was supported with air, water, food, power, all essentials of life, from a large support team above.  Men on the bottom performed a number of experiments intended to determine the practicality of working on the sea floor and were subjected to continual medical examinations.  The documentary, 93 minutes long, received wide international theatrical distribution, and was awarded an Academy Award for Best Documentary,      as well as numerous other honors. It was Cousteaus second film to win Best Documentary, the first being "The Silent World" released in 1956.

Funded in part by the French petrochemical industry, the Conshelf Two experiment was originally intended to demonstrate the practicality of exploitation of the sea using underwater habitats as base stations. In the end Cousteau repudiated such an approach, turning his efforts instead toward Conservation movement|conservation. The lyrical and dramatic underwater sequences also likely contributed to the beginning of an era of ocean conservation as well as incidentally promoting sport diving. Memorable sequences involve men cavorting with fishes, an underwater chess game, and the diving saucer reaching depths of 300 meters, encountering new and unique forms of life.

Reviews of the film were overwhelming positive, although the film did come under some criticism around accusations of "faking" footage, most notably by New York Times reviewer Bosley Crowther who questioned the authenticity of two of the films more dramatic scenes. Crowther stated in his 1964 review, "Oceanographers consulted here yesterday said it was highly unlikely that a deep-sea cavern, containing a "bubble," or pocket of air, at its top, could exist. If it did, the atmosphere in that bubble would surely be noxious, they said. It would be methane or marsh gas. And the pressure in it would be intolerable for man."     The confusion came from Crowthers assertion that the footage was filmed at great depth, an issue not clearly addressed in the film.  His other complaint was a long tracking shot moving out from the window of one of the underwater structures, which Crowther claimed could only have been produced in an aquarium.  Cousteau later demonstrated how he and his son Philippe produced the shot with a combination of ropes and small underwater motorized vehicles.  Cousteau took great offense, and continued to describe and defend the difficult and innovative techniques used to create the film.

==See also==
* The Silent World
* Voyage to the Edge of the World

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 