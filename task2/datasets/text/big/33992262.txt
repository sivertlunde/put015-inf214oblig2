Lil' Ainjil
{{Infobox Hollywood cartoon|
| cartoon_name = Lil Ainjil
| series = Krazy Kat
| image = 
| caption = 
| director = Manny Gould Ben Harrison
| story_artist =
| animator = Isidore Klein
| voice_actor = William Costello (uncredited)
| musician = Joe de Nat
| producer = Charles Mintz
| distributor = Columbia Pictures
| release_date = March 19, 1936
| color_process = Black and white
| runtime = 5:45 English
| preceded_by = The Bird Stuffer
| followed_by = Highway Snobbery
}}

Lil Ainjil is a short animated film distributed by Columbia Pictures, and features Krazy Kat.

==Overview==
Unlike the Krazy Kat films of the Winkler and Columbia period, animator Isidore Klein attempted to create Lil Ainjil in the milieu of George Herrimans comic strips. However, the film was not well received by critics, prompting the series to revert to their current setting. 

==Plot==
A fat cop and an aristocratic lady are walking down the road, talking about ones own livelihood. On the way, they find Krazy sticking his head inside a small theater box. On the other side of the box, a rat is tossing rocks at Krazys head but the cat appears to be enjoying it. The suspicious cop goes around and eventually notices the reality before arresting the rat.

The cop imprisons the rat and walks away in celebration. Despite the rats malicious treatment, Krazy feels sorry and decides to break the rodent out of the slammer. The naïve cat offers the rat a pie which conceals carpentry tools. The rat uses the tools to demolish the prison to the ground. But in doing so, another criminal is released.

That other criminal runs into the open and begins harassing the aristocratic lady. The cop struggles to intervene. For some reason, the rat decides to help out the cop by taking a machine gun and firing it at the criminal. The criminal is taken down, and the aristocratic lady is safe.

Though he escaped prison, the rat, nonetheless, receives a handshake from the cop for the assistance. As they go their separate ways, the rat sees Krazy joyously dancing around. He finds a square rock and throws it at Krazy, knocking the cat unconscious. The cop, who isnt too faraway, saw the deed, and chases the rat into the horizon.

==Notes==
*The name of the short would become the catchphrase Krazy says in the 1960s TV series after every time the feline gets hit by a 4x4 lumber thrown by a pink mongoose.
*The short is available in the Columbia Cartoon Collection: Volume 8. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
* Leonard Maltin|Maltin, Leonard (1987). Of Mice and Magic: A History of American Animated Cartoons. Penguin Books. ISBN 0-452-25993-2.
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 