La matassa
{{Infobox film
 | name = La matassa
 | image =La matassa.jpg
 | caption =
 | director = Giambattista Avellino and Ficarra e Picone
 | writer = Ficarra e Picone, Fabrizio Testini
 | starring =  Ficarra e Picone, Pino Caruso
 | music = Paolo Buonvino	
 | cinematography = Roberto Forza
 | editing =   
 | producer =   Attilio De Razza
 | released =  
 | runtime = 95 min
 | country = Italy
 | language  = Italian  
 }} 2009 Cinema Italian comedy film written and directed by Giambattista Avellino and by Ficarra e Picone.  The film was a box office success, ranking first for two weeks at the Italian box office and grossing $10,007,765. 

== Plot == Sicilian cousins other than for their character. One is parsimonious and timid, the other globetrotters and messy. The two cousins, separated in youth by hatred of the fathers, they get together at the funeral of the uncle. Gaetano is the heir of the family hotel, but he can only have it if finds himself ill. In fact, the lawyer who has the will wants the hotel for himself, with the help of a priest. Paolo builds a ruse to get the property, but the whole thing turns out. However, Paolo and Gaetano are able to get the tree for them, but now intrudes the Mafia, who orders two cousins to pay protection money building.

== Cast ==
*Ficarra e Picone|Ficarra: Gaetano
*Ficarra e Picone|Picone: Paolo
*Anna Safroncik: Olga
*Claudio Gioè: Antonio
*Domenico Centamore: Ignazio 
*Pino Caruso: Don Gino
*Tuccio Musumeci: Totò
*Mario Pupella: Don Mimí
*Mariella Lo Giudice: Paolos Mother
*Rosa Pianeta: Gaetanos Mother
*Giovanni Martorana: Pietro
*Gino Astorina: Police Commissioner

==References==
 

==External links==
* 

 
 
 
 
 


 
 