The Ice Storm (film)
 
{{Infobox film
| name = The Ice Storm
| image = Ice storm poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Ang Lee
| producer = Ang Lee James Schamus Ted Hope
| based on =  
| screenplay = James Schamus
| starring = {{Plain list |
* Kevin Kline
* Joan Allen
* Sigourney Weaver
}}
| music = Mychael Danna
| cinematography = Frederick Elmes
| editing = Tim Squyres
| studio = Good Machine Canal+|Canal+ Droits Audiovisuels
| distributor = Fox Searchlight Pictures
| released =  
| runtime = 113 minutes  
| country = United States
| language = English
| budget = $18 million
| gross = $8,038,061
}} novel of the same name by Rick Moody.

The film features an ensemble cast of Kevin Kline, Joan Allen, Tobey Maguire, Christina Ricci, Elijah Wood, and Sigourney Weaver. Set during Thanksgiving 1973, The Ice Storm is about two dysfunctional New Canaan, Connecticut families who are trying to deal with tumultuous political and social changes of the early 1970s, and their escapism through alcohol, adultery, and sexual experimentation.
 Bomb at the box office, even though it garnered positive reviews. A new special two-disc DVD set was also released as a part of the Criterion Collection on March 18, 2008. 

==Plot==
Ben, Elena, and their children, 16-year-old Paul and 14-year-old Wendy. And their neighbors, the Carvers, include Jim, Janey, and their children: Mikey and Sandy.

Ben, dissatisfied in his marriage and with the futility of his career, is having an affair with Janey. Elena is bored with her life and is looking to expand her thinking but is unsure of how to do so. Wendy enjoys sexual games with her school peers. 

Wendy decides to make her way to the Carvers to see Mikey, but he has decided to go out into the ice storm, so she and Sandy climb into bed together and remove their clothes. They drink from a bottle of vodka and Wendy tries to seduce him; however, they both fall asleep.

As the key party progresses, Ben becomes drunk. When Janey chooses the keys of a handsome young man, Ben attempts to protest but trips and knocks his head on the coffee table, leading Jim to realize that his wife and Ben are having an affair. Ben, in his embarrassment, retreats to the bathroom where he remains for the rest of the evening. The remaining key party participants are paired off and leave together with only Jim and Elena remaining. She retrieves Jims keys from the bowl and returns them to him. After debating the issue, Jim and Elena leave together, engaging in a quick, clumsy sexual encounter in the front seat of Jims car. Jim, regretting the line he and Elena have just crossed, agrees to drive her home.

Meanwhile, Mikey, out walking in the storm, is enchanted by the beauty of the trees and fields covered in ice. He slides down an icy hill then sits on a guardrail to rest. A moment later a power line, broken by a fallen tree, connects with the guardrail and he is electrocuted.

Jim and Elena return to the Carvers house as dawn is breaking. Elena walks in on her daughter in bed with Sandy and orders her to get dressed.

Janey had also returned home earlier and curled up on her bed in the fetal position without bothering to take off her party clothes. Although it is not revealed what transpired between Janey and her key partner, she is visibly exhausted and sad.

Ben has sobered up by this time and begins driving home. He discovers Mikeys body on the side of the road and carries it back to the Carvers house. The two families are drawn together by Mikeys death and Wendy hugs the shocked and numbed Sandy in an attempt to comfort him. Jim is devastated while Janey remains asleep and ignorant to the recent events. Ben, Elena and Wendy then drive to the train station to pick up Paul who is returning from Libbets apartment, his train delayed by the ice and the power failure caused by the downed wire. Once all four are together in the car, Ben breaks down, sobbing uncontrollably at the wheel as Elena starts comforting him.

==Cast==
* Kevin Kline as Ben Hood
* Joan Allen as Elena Hood
* Sigourney Weaver as Janey Carver
* Jamey Sheridan as Jim Carver
* Henry Czerny as George Clair
* Tobey Maguire as Paul Hood
* Christina Ricci as Wendy Hood
* Elijah Wood as Mikey Carver
* Adam Hann-Byrd as Sandy Carver
* David Krumholtz as Francis Davenport
* Katie Holmes as Libbets Casey
* Michael Cumpsty as Reverend Philip Edwards Jonathan Freeman as Ted Franklin
* Allison Janney as Dot Halford
* Barbara Garrick as Weather reporter
* John Benjamin Hickey as Mark Boland

==Production==
The Ice Storm was first brought to the attention of producer James Schamus by his wife, literary scout Nancy Krikorian, who knew Rick Moody from Columbia Universitys MFA program. "Its an astonishingly cinematic book", says Schamus. "But, because of its truly literary qualities, people may have missed its extraordinary cinematic possibilities."  .  Philosopher Slavoj Žižek claims Schamus was also inspired by one of Žižek’s books at the time of writing the screenplay: "When James Schamus was writing the scenario, he told me that he was reading a book of mine and that my theoretical book was inspiration." 

Schamus brought the book to Ang Lee, who was the first and only contender for the book, and with whom Schamus and partner Ted Hope had already made four films, including The Wedding Banquet in 1993. Despite the obvious appeal of Moodys comedy of familial errors, Lee says what attracted him to the book was its climax: the scene where Ben Hood makes a shocking discovery in the ice, followed by the emotional reunion of the Hood family on the morning after the storm. "The book moved me at those two points", says Lee. "I knew there was a movie there." 

To prepare for the film, Lee let the cast members study stacks of magazine cutouts from the early 1970s. Moody was reportedly very pleased with the final version – and reportedly "sobbed" during the end credits.  He also expressed his happiness that the success of the film brought more attention to his novel, leading to more book sales.

==Reception==
  
Film critics Gene Siskel and Roger Ebert both gave the film Two Thumbs Up, with Gene Siskel calling it the best film of the year, and Roger Ebert calling it Ang Lees best film yet. Rotten Tomatoes gives the film a rating of 84% based on reviews from 61 critics. 
 Best Screenplay.   

 
The films release was limited, and it grossed just US$7.8 million against a production budget of US$18 million. 

For her performance, Sigourney Weaver received the BAFTA Award for Best Supporting Actress and was also nominated for a Best Supporting Actress Golden Globe.

==Soundtrack==
 
Most of the professional music featured in the film was independently produced 1970s-type music, as budget values were tight. Lee and Schamus wanted to have an "actual score" — not a "nostalgic film with radio music of an earlier time".  The soundtrack was first released in the United States on October 21, 1997.

==Home media== Criterion acquired the rights to release a 2-disc DVD edition on March 8, 2008. Criterion released this version in a Blu-ray release on July 23, 2013. 

==References==
 
*  

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 