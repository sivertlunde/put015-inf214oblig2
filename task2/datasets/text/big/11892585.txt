Vengeance of the Zombies
{{Infobox Film
 | name           = Vengeance of the Zombies
 | image          = Vengeance of the Zombies.jpg
 | image_size     = 
 | caption        = 
 | director       = León Klimovsky
 | producer       = Jose Antonio Perez Giner
 | writer         = Paul Naschy
 | starring       = Paul Naschy
 | music          = Juan Carlos Calderón
 | cinematography = Francisco Sánchez
 | editing        = Antonio Ramírez de Loaysa
 | studio         = {{plainlist|
* Profilmes
* Promofilms
}}
 | distributor    = 
 | released       =  
 | runtime        = 90 minutes
 | country        = Spain
 | language       = Spanish
 | budget         = 
 | gross          = 
 }}

Vengeance of the Zombies ( ) is a 1972 horror film directed by León Klimovsky and starring Paul Naschy.

== Plot == zombie horror film, a mysterious man rampages throughout England killing unsuspecting women. Each time a victim is murdered, they are brought back to life by an East Indian named Kantaka to join his army of zombies.

== Cast ==
* Paul Naschy as Krisna, Kantaka, and Satán
* Rommy as Elvire Irving
* Mirta Miller as Kala
* Vic Winner as Dr. Lawrence Radcliffe
* Maria Kosti as Elsie
* Luis Ciges as MacMurdo

== Release ==
The film was released theatrically in its native Spain in 1972.   All Seasons Entertainment released the film was released on VHS in the United States in the 1980s.   Deimos Entertainment, a subdivision of BCI Eclipse, released a special edition DVD in 2007. 

== Reception ==
Steve Barton of Dread Central rated it 4/5 stars and wrote, "Sexy, erotic, scary, and disturbing, Vengeance of the Zombies has it all!"   Bloody Disgusting rated it 4/5 stars and wrote, "Vengeance of the Zombies is an exemplary reason as to why we should celebrate forgotten films. The film manages to play off of its unoriginality in the most entertaining way possible, catering to fans of deliciously cheesy cinema."   Adam Tyner of DVD Talk wrote, "Naschy likened the movie to a drug-induced nightmare in his autobiography, and its every bit as odd and incoherent as that suggests."   Rafael Gamboa of DVD Verdict called it enjoyably bad.   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle called it "a plodding and unengaging zombie mystery". 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 