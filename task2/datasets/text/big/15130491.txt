The Man Without a Country (1917 film)
:For the 1937 Warner Brothers version of the same film, see The Man Without a Country (1937 film).
{{Infobox film
| name           = The Man Without a Country
| image          = The Man Without a Country-ad-1917.jpg
| imagesize      =
| caption        = Advertisement for the film
| director       = Ernest C. Warde
| producer       =
| writer         = Edward Everett Hale (original story)
| starring       = Florence La Badie, Holmes Herbert, and J. H. Gilmour
| cinematography = 
| editing        = Thanhouser Film Corporation
| released       =  
| runtime        =
| country        = United States Silent (English intertitles)
}}
  1917 Cinema American silent film adaptation of Edward Everett Hales short story The Man Without a Country. It was directed by Ernest C. Warde, and starred Florence La Badie, Holmes Herbert, and J. H. Gilmour, and released by Thanhouser Film Corporation. 

The film follows closely to the storyline of the book, and was a success in film theaters. The original story, with its strong patriotic theme, was written during the American Civil War in order to increase public support for the Union cause. The film had a like function with regard to the First World War, which the United States had joined a few months prior to the films release.

The film was released on September 9, 1917, and was the last film role of Florence La Badie, who would die in October 1917 from injuries sustained in an automobile crash in August 1917, just days before the films premiere. 

==References==
 

==External links==
 
*  
* 

 
 
 
 
 
 
 
 
 


 