Les Indiens sont encore loin
{{Infobox film
| name           = Les Indiens sont encore loin
| image          = LesIndienssontencoreloin.jpg
| caption        = 
| director       = Patricia Moraz
| producer       = Robert Boner
| writer         = Patricia Moraz
| starring       = Isabelle Huppert
| music          = 
| cinematography = Renato Berta
| editing        = Thierry Derocles
| distributor    = 
| released       = 5 October 1977 (France)
| runtime        = 95 minutes
| country        = Switzerland
| language       = French
| budget         = 
}}

Les Indiens sont encore loin is a 1977 Swiss drama film directed by Patricia Moraz.   

==Cast==
* Isabelle Huppert - Jenny
* Christine Pascal - Lise
* Mathieu Carrière - Matthias
* Chil Boiscuille - Guillaume
* Nicole Garcia - Anna
* Anton Diffring - Le professeur dallemand
* Bernard Arczynski - Charles Dé (as Bernard Arczinsky)
* Jacques Adout - Le concierge du lycée (as Jacques Addou)
* Connie Grimsdale - La grand-mère
* Marina Bucher - Marianne
* Emmanuelle Ramu - Pascale
* Guillaume Rossier - Le garçon brun
* Catherine Cuenod - La prof de gym
* Claudia Togni - La serveuse
* René Herter - Le garçon du buffet

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 
* 
*  on 20 May 1977 

 
 
 
 
 
 
 