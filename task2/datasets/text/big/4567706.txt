Roti Kapda Aur Makaan
{{Infobox film
| name           = Roti Kapada Aur Makaan
| image          = Rotikapda.jpg
| image_size     =
| caption        = 
| director       = Manoj Kumar
| producer       = Manoj Kumar
| writer         = Manoj Kumar
| narrator       = 
| starring       = Manoj Kumar Shashi Kapoor Zeenat Aman Moushumi Chatterjee Aruna Irani Amitabh Bachchan
| music          = Laxmikant-Pyarelal
| cinematography = Nariman A. Irani
| editing        = Manoj Kumar
| studio         = Chandivali Studio Filmistan Studios Mohan Studios R.K. Studios Rajkamal Studios
| distributor    = V.I.P. Films Digital Entertainment Eros Entertainment Shiva Films Spark Worldwide
| released       = 18 October 1974
| runtime        = 161 mins
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1974 Indian Bollywood Hindi-language film. It was written, produced, directed by and stars Manoj Kumar, alongside Shashi Kapoor, Zeenat Aman and Moushumi Chatterjee in the main lead roles and had Amitabh Bachchan, Prem Nath and Madan Puri in supporting roles. Laxmikant Pyarelal were the music directors. This film was released on October 18, 1974 along with films like Roti and Benaam (1974 film)|Benaam. 

The title of the movie is based on the Urdu phrase referring to the bare necessities of life, which was popularized in the late 1960s by Zulfikar Ali Bhutto ahead of the Pakistani general election, 1970.    The film was the highest grossing Indian film of 1974 and one of the biggest blockbusters of Manoj Kumar. It also gave a boost to the career of Amitabh Bachchan who was then strengthening his position in the film industry. 

The Telugu movie "Jeevana Poratam", starring Vijayashanthi, is a remake of this movie.

==Synopsis==
After the retirement of his dad (Krishan Dhawan), the responsibility is on Bharat (Manoj Kumar) to look after his Delhi-based family. He has two younger college-going brothers, Vijay (Amitabh Bachchan) and Deepak (Dheeraj Kumar) and a sister of marriagable age, Champa (Meena T.). Although Bharat himself is a college graduate, the only work he can find is as a low-paid singer, much to the frustration of his girlfriend, Sheetal (Zeenat Aman). Meanwhile, Vijay has turned to crime as a last resort to provide for the family, but after an argument with Bharat, he leaves home to join the army.

Sheetal starts working as a secretary for rich businessman Mohan Babu (Shashi Kapoor) and Mohan becomes attracted to her. She too becomes attracted to Mohan but is more interested in his wealth and luxury. She loves Bharat but cannot contemplate a life in poverty. Bharat finally finds a job as a builder but starts to realise that Sheetal is slowly drifting away from him. Soon he loses his job after the government takes over the building site and his financial problems increase further. When Mohan proposes marriage to Sheetal, she accepts and soon both are engaged, leaving Bharat heartbroken. After losing his love, Bharat also loses his father, which devastates him. Frustrated, Bharat burns his diploma on his fathers funeral pyre.

Meanwhile, Champa has found a suitor, but Bharat has no money to pay for the wedding and it cannot go ahead. Still depressed at the state of his life, Bharat soon finds salvation by helping a poor girl, Tulsi (Moushumi Chatterjee), who also lives in poverty but gets by. He also makes friends with Sardar Harnam Singh (Prem Nath) who comes to his rescue when he attempts to save Tulsi from a gang of hoodlums. He then receives an offer from a corrupt businessman named Nekiram (Madan Puri) who persuades Bharat to do his illegal activities so he and his family will finally come out of poverty and be wealthy. The plot centers on whether Bharat agree to this, or whether his moral nature stops him from turning to crime to provide for his family.

==Cast==
*Manoj Kumar .... Bharat
*Shashi Kapoor .... Mohan Babu
*Zeenat Aman .... Sheetal
*Moushmi Chatterjee ... Tulsi
*Amitabh Bachchan .... Vijay
*Dheeraj Kumar .... Deepak
*Krishan Dhawan .... Bharats father
*Kamini Kaushal .... Bharats mother
*Prem Nath .... Harnam Singh (Tulsis friend)
*Madan Puri .... Nekiram
*Aruna Irani .... Poonam (Nekirams mistress)
*Meena T. .... Champa (Bharats sister)

==Soundtrack==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| Hay Hay Yeh Majboori
| Lata Mangeshkar 
| Performed on-screen by Zeenat Aman.
|-
|Main Na Bhoolunga Mukesh (singer)|Mukesh and Lata Mangeshkar. There were three versions of this song in the film. This song was performed on-screen by Shashi Kapoor, Manoj Kumar, and Zeenat Aman. The third version was sung solely by Lata Mangeshkar and can be found on the vinyl LP.
|-
|Aur Nahin Bas Aur Nahin Mahendra Kapoor Performed on-screen by Manoj Kumar. Mehendra Kapoor won the award for best singer for the song.  
|-
|Mehngai Mar Gayi Lata Mangeshkar, Mukesh (singer)|Mukesh, Narendra Chanchal and Jani Babu Qawwal
|
|-
|Panditji Mere Marne Ke Lata Mangeshkar Performed on-screen by Aruna Irani.
|}

==Awards and nominations==
*1975 Filmfare Best Director Award for Manoj Kumar
*1975 Filmfare Best Male Playback Award for Mahendra Kapoor for the song Aur Nahin Bas Aur Nahin
*1975 Filmfare Best Lyricist Award for Santosh Anand for the song Main Na Bhoolunga
*1975 Filmfare Nomination for Best Film
*1975 Filmfare Nomination for Best Actor for Manoj Kumar
*1975 Filmfare Nomination for Best Actor in a Supporting Role for Prem Nath
*1975 Filmfare Nomination for Best Actress in a Supporting Role for Moushumi Chatterjee
*1975 Filmfare Nomination for Best Story for Manoj Kumar
*1975 Filmfare Nomination for Best Music for Laxmikant Pyarelal
*1975 Filmfare Nomination for Best Lyricist for Santosh Anand for the song "Aur Nahin Bus Aur Nahin" Mukesh for the song "Main Na Bhoolonga"

==References==
 

== External links ==
* 

 
 
 
 
 
 
 