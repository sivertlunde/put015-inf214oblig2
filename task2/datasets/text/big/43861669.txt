Manithanin Marupakkam
{{Infobox film
| name           = Manithanin Marupakkam
| image          =
| image_size     =
| caption        =
| director       = K. Rangaraj
| producer       = G. Thyagarajan G. Saravanan
| writer         = A. L. Narayanan (dialogues)
| story          = Dennis Joseph Radha Jayashree Jai Jagadish
| music          = Ilayaraja
| cinematography = Dineshbabu
| editing        = K. R. Krishnan
| studio         =  Sathyajyothi Films
| distributor    =  Sathyajyothi Films
| released       = 24 Jul 1986
| country        = India Tamil
}}
 1986 Cinema Indian Tamil Tamil film, directed by K. Rangaraj and produced by G. Thyagarajan and G. Saravanan. The film stars Sivakumar, Radha (actress)|Radha, Jayashree and Jai Jagadish in lead roles. The film had musical score by Ilayaraja.   The film was a remake of Malayalam film Nirakkoottu. 

==Cast==
*Sivakumar Radha
*Jayashree
*Jai Jagadish Rajya Lakshmi
*V. K. Ramasamy (actor)|V. K. Ramasamy
*Venniradai Moorthy
*Delhi Ganesh
*A. R. S.
*Sethu Vinayagam

==Soundtrack==
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF"
! No. !! Song !! Singers !! Lyrics !! Length (m:ss) 
|-  Chitra || Vairamuthu || 04.21 
|-  Janaki || Pulamaipithan || 04.24 
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 


 