The Hayseeds
 
{{Infobox film
| name           = The Hayseeds
| image          = The_Hayseeds.jpg
| image_size     = 
| caption        = Contemporary advertisement for film
| director       = Beaumont Smith Raymond Longford (associate) "Raymond Longford", Cinema Papers, January 1974 p51 
| producer       = Beaumont Smith
| writer         = Beaumont Smith
| based on      = 
| narrator       = 
| starring       = Cecil Kellaway
| music          = Alf Lawrence Fred Chapple
| cinematography = Tasman Higgins
| editing        = Frank Coffey
| music = Frank Chapple Alf Lawrence
| studio         = J.C. Williamson Picture Productions
| distributor   = British Empire Films 
| released       = 8 December 1933 
| runtime        = 98 mins
| country        = Australia
| language       = English
| budget         = ₤4,500 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 164.     
| gross = £20,000  or £16,000 
}} On Our Selection (1932). It was the first starring role in a movie for stage actor Cecil Kellaway.

It was also known as The Hayseeds Come to Town.

==Plot==
Wealthy Mary Townleigh  gets lost in the bush and hurts her ankle, but is rescued and stays with the Hayseed family. She starts a romance with their neighbour, Englishman John Manners. When Joe Hayseed and his girlfriend Pansy Regan decide to get married, the Hayseeds and John visit Sydney to stay with the Townleighs. John is accused of being a fugitive of justice but is eventually proved innocent and he and Mary get married. 

==Cast==
* Cecil Kellaway as Dad Hayseed
* Kenneth Brampton as Mr. Townleigh
* Arthur Clarke as John Manners
* Shirley Dale as Mary Townleigh
* Bryan Kellaway as Billy
* John Moore as Henry Westcott
* Tal Ordell as Joe Hayseed
* Vincent Pantin as Lord Mornington
* Molly Raynor as Pansy Ragen
* Phyllis Steadman as Polly
* Stan Tolhurst as Sam
* Katie Towers as Mum Hayseed
*the J.C. Williamson Chorus (singing hikers)
*the Richard White Girls
*Jimmy Coates and his Orchestra

==Production==
The movie was shot at Cinesound Productions|Cinesounds studios in Rushcutters Bay in August and September 1933, with location work near Pymble. Many of the cast, including Kellaway, John Moore and Shirley Dale, were appearing in the play Music in the Air during filming. 

The movie was a semi musical with a number of songs and dance sequences. The latter were produced by Richard White, who ran a dance academy in Sydney. 

Cecil Kellaways son, Brian, made his film debut alongside his father. 

===Director===
There is some debate as to the true director of the film. While some sources say that Beaumont Smith both scripted and directed the picture, Smith himself announced that Raymond Longford would direct the picture,  and newspapers of the era also gave the credit to Longford.

==Reception==
Reviews for the film were mixed   but it proved popular with audiences on release. 

By the end of 1934 it was estimated to have earned £16,000 in Australia and an overall profit of £5,900. "Counting the Cash in Australian Films", Everyones 12 December 1934 p 19-20  

The film was still screening in cinemas in 1950. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at Australian Screen Online
*  at National Film and Sound Archive
*  at Oz Movies

 
 

 
 
 
 
 
 
 