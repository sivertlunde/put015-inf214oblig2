Bullet to the Head
 
{{Infobox film
| name = Bullet to the Head
| image = Bullet to the Head Poster.jpg
| alt = 
| caption = Theatrical release poster Walter Hill
| producer = Alexandra Milchan Alfred Gough Miles Millar Kevin King-Templeton
| screenplay = Alessandro Camon
| based on =  
| starring = Sylvester Stallone Sung Kang Sarah Shahi Adewale Akinnuoye-Agbaje Christian Slater Jason Momoa
| music = Steve Mazzaro
| cinematography = Lloyd Ahern II
| editing = Tim Alverson
| studio = Dark Castle Entertainment IM Global After Dark Films
| distributor = Warner Bros. Pictures
| released =  
| runtime = 92 minutes  
| country = United States
| language = English
| budget = $55 million   
| gross = $13,561,515
}} Walter Hill. Matz and Colin Wilson. The film stars Sylvester Stallone, Sung Kang, Sarah Shahi, Adewale Akinnuoye-Agbaje, Christian Slater, and Jason Momoa. Alexandra Milchan, Alfred Gough, Miles Millar, and Kevin King-Templeton produced the film.

The film premiered at the International Rome Film Festival on November 14, 2012.  The film received a wide United States release on February 1, 2013.

==Plot== WDCPD policeman, Hank Greely, although Bobo leaves a prostitute, Lola, alive. Later, at a bar, Blanchard is murdered by Keegan, who also attempts to kill Bobo, but fails.

Washington, D.C., Detective Taylor Kwon arrives in New Orleans to investigate his former partners death and meets Lieutenant Lebreton, who informs him Lola confirmed Greely was assassinated. Kwon goes to the morgue, and, after seeing Blanchards body and finding out who he is, he deduces that Blanchard and Bobo killed Greely. Meanwhile, Keegan meets with his employer, Robert Morel, and Morels lawyer Marcus Baptiste. Baptiste reveals that Greely tried to blackmail Morel, and provided local mobster Baby Jack with a file detailing Morels illegal operations. Keegan later kills Baby Jack and his men and retrieves the file.

Kwon meets Bobo in a bar and informs him that he knows Bobo and Blanchard killed Greely. Bobo leaves, and when Kwon tries to follow him, he is attacked by corrupt cops who were ordered by Morel to prevent Kwon from further investigating about Greely. Bobo rescues Kwon and takes him to a tattoo parlor, where Bobos estranged daughter, Lisa, treats Kwons wounds. They later go to a massage parlor where Bobo interrogates Ronnie Earl, the middleman who hired Bobo and Blanchard on Morels behalf. Ronnie Earl tries to kill Bobo, but Bobo manages to kill him, although his gun jams. Bobo later confronts Kwon, who admits to having tampered with Bobos gun, nearly causing his death. Bobo and Kwon agree to work together.

Bobo and Kwon kidnap Baptiste and take him to Bobos house, where he is forced to give them a flash drive detailing Morels plans to acquire housing projects and demolish them to build office buildings and reveals Keegan is an ex-mercenary hired to be Morels enforcer. Afterwards, Bobo shoots him in the head. Keegan and his men trace Baptistes cellphone to Bobos house, but Bobo and Kwon are able to escape and detonate a bomb, killing Keegans men. Keegan then becomes obsessed with killing Bobo.

Kwon meets with Lieutenant Lebreton to ask for his help, but Lebreton tries to kill him, as he is also on Morels payroll, but Bobo kills him and saves Kwon. Meanwhile, Keegan learns about Lisa and kidnaps her. Morel then calls Bobo and offers to trade Lisa for the flash drive. Bobo agrees, and meets with Morel in an abandoned warehouse, where he delivers the flash drive to him and rescues Lisa, while Kwon infiltrates the building to arrest Morel. Keegan becomes furious when Bobo is allowed to leave and kills Morel and his men before going after Bobo.

Keegan confronts Bobo and they have an axe fight, which ends with Bobo stabbing Keegan in the throat with Blanchards knife and Kwon shooting Keegan in the head. Kwon retrieves the flash drive and Bobo shoots him in the shoulder to make it appear as if Kwon failed to capture him. Lisa decides to stay with Kwon, with whom she initiates a romantic relationship, and Bobo leaves. He later meets Kwon at a bar, where Kwon tells him he did not mention Bobos existence to the police this time, but if Bobo continues in the business, Kwon will take him down. Bobo welcomes him to try and drives off into the night.

==Cast==
* Sylvester Stallone as James "Bobo" Bonomo
* Sung Kang as Detective Taylor Kwon 
* Sarah Shahi as Lisa Bonomo
* Adewale Akinnuoye-Agbaje as Robert Nkomo Morel
* Christian Slater as Marcus Baptiste
* Jason Momoa as Keegan
* Jon Seda as Louis Blanchard
* Holt McCallany as Hank Greely
* Brian Van Holt as Ronnie Earl
* Weronika Rosati as Lola
* Dane Rhodes as Lieutenant Lebreton 
* Marcus Lyle Brown as Detective Towne
* Douglas M. Griffin as Baby Jack

==Production==
===Development===
The film is based on  . An executive attached to the film has said, "  is exactly the type of fast-paced, universally themed project that suits our business model. Sylvester Stallone is an international icon and were really excited to be in business with him." (2011-02-06).   The Hollywood Reporter. Retrieved 2011-03-30.  
 Wayne Kramer was attached to direct, but left the project when his vision of the film was darker than Stallone wanted.  Sylvester Stallone then called Walter Hill who had just had a movie fall apart six weeks before that he had been trying to do for a year.    Hill later recalled:
 When Sly and I first talked about doing it, I told him I thought if we did it as an homage to ’70s or ’80s action films – and if he got a haircut and if we played it not at some nuclear level and left a little room for humor – everything would probably work out. I mean, this is one of those plots...  You know, in terms of the real world, they’re fairly preposterous. But that’s OK. That’s part of the given. As long as you don’t break the rules and contradict yourself within that sensibility, people go for the ride.... ly and I have known each other for probably 35 years. I have always been a great admirer of Sly’s. Most directors love movie stars because they’re such fabulous tools to tell stories with. Sly is an actor but he’s a star and he’s been a star for a very long time. When he sent me this, there was a feeling on both our parts, that if this was ever going to happen – us working together – we better sit down and do it. Time is moving on.   31 Jan 2013] accessed 26 April 2015  
Thomas Jane was originally cast for the part that would eventually go to Sung Kang. The role was recast at the insistence of producer Joel Silver, stating a need for a "more ethnic actor" to appeal to a wider audience.  
Hill stated:
 The real truth is these movies are all foreign driven. They need domestic releases. If the economics are right, people feel like they can be commercial in a reasonable way domestically. But they’re really foreign driven. This movie would not exist without expectation of the foreign audience being vastly greater than the domestic.  
Hill said he wanted to have fun with the genre:
 We’re not breaking new ground. We’re trying to be entertaining within a format that’s familiar. There’s a kind of ice skating that goes on where you must let the audience know that you’re not taking yourself too seriously. But at the same time, the jokes are funny but the bullets are real. The jeopardy has to be real. When it gets outlandish, there needs to be no drift into parody – self-parody, maybe inevitable for old directors.  
Hill said the film would be called a "buddy movie" but that he made "anti-buddy movies:
 They don’t like each other. They’re not going to like each other. The most they’re going to achieve by the end is a kind of grudging respect. I’m just comfortable with that. It seems to be an inherently more dramatic situation than if they’re friendly and they get along and respect each other. Also, frankly, it gives you better avenue to work in humor. These things have to be leavened with humor. It actually reinforces the action.  
===Shooting===
Bullet to the Head was shot in New Orleans and started filming on June 27, 2011.  Hill:
 One of the things I like about New Orleans is it feels like you’re in a western with the architecture.  All the balconies, the old buildings, it feels like you’re in the 1880s.  Some of it spills into the movie.  I don’t know how much of it creeps into the edges and helps you or how much of it is just by design.  Usually you’re trying to tell a narrative through your characters and have all this stuff bleed in around the edges.    
Hill said he told Stallone "to play things more casually. I wanted him to play his natural personality as much as possible. He’s a very engaging guy. I told him, “I’m not interested in you inventing a character as much as imagining yourself as character.” He went right with that." 

On August 23, 2011, it was announced that the film would be released on April 13, 2012. On February 23, 2012, the release date was moved back to an unknown date. It was released on February 1, 2013.

==Reception==

===Critical response === Escape Plan Grudge Match, where he lost to Jaden Smith for After Earth.

===Box office===
Bullet to the Head was Sylvester Stallones worst opening weekend gross in 32 years, and his second-lowest opening weekend gross of all time.   Bullet to the Head made $4,458,201 for its opening weekend. As of March 24, 2013, the film has grossed $9,489,829 in the United States and $12,457,380 worldwide for a total of $21,947,209, failing to bring back its $55 million budget. 

==Music ==
  
{{Infobox album
| Name = Bullet to the Head Original Motion Picture
| Type = soundtrack
| Artist = 
| Cover = 
| Released = 2013
| Genre = Soundtrack
| Length = 
| Producer = Hans Zimmer
| Label = Varèse Sarabande (USA) 
}}
The soundtrack album was released digitally on January 29, 2013 and at the stores on February 19. The album features the films score contains 15 tracks composed by Steve Mazzaro and produced by Hans Zimmer. 

{{Track listing
| all_music = Steve Mazzaro
| headline = Bullet to the Head Original Motion Picture
| total_length = 42:25
| title1 = Heres the Story
| length1 = 5:04
| title2 = Staying in the Game
| length2 = 2:42
| title3 = Just Another Soldier
| length3 = 2:58
| title4 = On the Road
| length4 = 1:47
| title5 = Dont Touch My Gun
| length5 = 1:59
| title6 = The Fox and the Hound
| length6 = 1:25
| title7 = This is My City
| length7 = 3:16
| title8 = Ambushed
| length8 = 1:28
| title9 = The Only Life He Had
| length9 = 1:58
| title10 = Guns Dont Kill People
| length10 = 0:59
| title11 = Change of Plans
| length11 = 2:11
| title12 = End of the Line
| length12 = 6:41
| title13 = Vikings
| length13 = 1:16
| title14 = Its All Over
| length14 = 3:07
| title15 = Bullet to the Head
| length15 = 5:24
}}

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 