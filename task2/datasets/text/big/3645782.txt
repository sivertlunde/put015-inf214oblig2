The Pornographers
{{Infobox film
| name           = The Pornographers
| image          = Erogotoshitachi yori Jinruigaku nyumon.jpg
| caption        = VHS video cover of The Pornographers (1966)
| director       = Shohei Imamura
| producer       = Jiro Romado
| writer         = Shohei Imamura Kōji Numata
| starring       = Shōichi Ozawa Sumiko Sakamoto Masaomi Kondō Keiko Sagowa
| music          = Toshirō Mayuzumi Toshirō Kusunoki
| cinematography = Shinsaku Himeda
| editing        = Matsuo Tanji
| studio         = Imamura Productions
| distributor    = Nikkatsu
| released       = March 12, 1966 (Japan) August 1966 (U.S.)
| runtime        = 128 min.
| country        = Japan
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| based on       =  Novel by Akiyuki Nosaka
}}
 1966 Japanese film directed by Shohei Imamura and based on a novel of the same name by Akiyuki Nosaka. Its original Japanese title is Erogotoshitachi yori Jinruigaku nyūmon (エロ事師たちより　人類学入門), which means An introduction to anthropology through the pornographers. It tells the story of porn film-maker Mr. Subuyan Ogata, whose business is under threat from thieves, the government, and his own family.

The film is a dark comic satire, depicting the underbelly of the Japanese post-war economic miracle, in this case pornographers and small-time gangsters in Osaka. It has been called Imamuras best-known film outside of Japan. 

==Cast==
* Shōichi Ozawa as Subuyan Ogata
* Sumiko Sakamoto as Haru Matsuda
* Keiko Sagawa as Keiko Matsuda, Harus Daughter
* Haruo Tanaka as Banteki
* Ganjirō Nakamura as Elderly Executive From Hakucho Company
* Masaomi Kondō as Kōichi Matsuda, Harus Son Akira Nishimura as Detective Sanada
* Ichirō Sugai as Shinun Ogata, Subuyans Father
* Shinichi Nakano as Kabō
* Chōchō Miyako as The Virgin House Madame
* Kazuo Kitamura as The Doctor

==References==
 

==External links==
* 
*  
*Hoberman, J.   Criterion Collection essay, 4 August 2003
*  
 

 
 
 
 
 
 
 
 
 
 


 
 