The Farmer's Daughter (1947 film)
{{Infobox film
| name = The Farmers Daughter
| image = The Farmers Daughter (1947 film).jpg
| caption = theatrical release poster
| director = H. C. Potter	
| producer = Dore Schary			
| writer = Hella Wuolijoki (play) Allen Rivkin Laura Kerr
| starring =Loretta Young Joseph Cotten Ethel Barrymore Charles Bickford
| music = Leigh Harline
| cinematography =  Milton R. Krasner
| editing = Harry Marker
| distributor = RKO Radio Pictures Inc.  
| released =   |ref2= }}
| runtime = 97 minutes
| language = English
| budget = 
}} Congressman and his politically powerful mother.  It stars Loretta Young, Joseph Cotten, Ethel Barrymore, and Charles Bickford, and was adapted by Allen Rivkin and Laura Kerr from the play Juurakon Hulda by Hella Wuolijoki, using the pen name Juhani Tervapää.  It was directed by H.C. Potter.
 Best Actor Mourning Becomes Electra.
 television series William Windom.

==Plot==
 Rhys Williams), who had completed a job for Katies father, offers her a ride, but robs her of her money.  Katie, refusing to ask her family for help, goes to work as a maid in the home of political power broker Agatha Morley (Ethel Barrymore) and her son, U.S. Representative Glenn Morley (Joseph Cotten).  Soon, she impresses Agatha and her loyal majordomo Joseph Clancey (Charles Bickford) with her refreshing, down-to-earth common sense and Glenn with her other charms.
 Art Baker). Knowing of the man, Katie strongly disapproves of their choice. She attends a public meeting arranged to introduce Finley and begins asking pointed, embarrassing questions. The leaders of the opposition party are impressed and offer her their backing in the coming election. When she accepts, Katie reluctantly has to quit her job, much to Glenns annoyance.

When Katies campaign picks up support (with the help of some coaching from Glenn), Finley resorts to smearing her reputation. He pays Petree to claim Katie spent the night with him when he gave her a ride. As a result, Katie becomes so distraught, she runs home. Glenn learns of the chicanery, follows her and proposes to her. 
 KKK sympathizer and that he bribed Petree, but also that he has him safely hidden away at his isolated lodge. With the help of Katies three big, strong brothers (James Arness, Lex Barker, Keith Andes), Glenn takes Petree away from the goons Finley assigned to watch him, and makes him confess over the radio. Agatha withdraws her partys support from Finley and endorses Katie, ensuring her election.  In the final scene, Glenn carries Katie across the threshold of the United States House of Representatives.

==Cast==
  
*Loretta Young as Katie Holstrom
*Joseph Cotten as Glenn Morley 
*Ethel Barrymore as Agatha Morley 
*Charles Bickford as Joseph Clancy 
*Rose Hobart as Virginia Thatcher  Rhys Williams as Adolph Petree  Harry Davenport as Dr. Matthew Sulven 
*Tom Powers as Hy Nordick 
*William Harrigan as Ward Hughes 
*Lex Barker as Olaf Holstrom  Harry Shannon as Mr. Holstrom 
*Keith Andes as Sven Holstrom 
 
*Thurston Hall as Wilbur Johnson  Art Baker as Anders J. Finley 
*Don Beddoe as Einar, campaign reporter 
*James Arness as Peter Holstrom  
*Anna Q. Nilsson  as Mrs. Holstrom 
*John Gallaudet as Van 
*William B. Davidson as Eckers 
*Cy Kendall as Sweeney 
*Frank Ferguson as Martinaan 
*William Bakewell as Windor  Charles Lane as Jackson, campaign reporter
 

==Production==
Due to rumours of Joseph Cotten and Ingrid Bergman having an affair, Bergmans role in the film was taken by Loretta Young. 

==References==
;Notes
 

==External links==
* 
*  
*  
*  on Lux Radio Theater: January 5, 1948

 

 
 
 
 
 
 
 
 
 
 