Ujala
{{Infobox film
| name           = Ujala
| image          = Ujala 1959.jpg
| caption        = DVD cover
| director       = Naresh Saigal
| producer       = F.C. Mehra
| writer         = Qamar Jalalabadi (screenplay)  Naresh Saigal (story)  Manohar Singh Sehrai (dialogues)
| narrator       =
| starring       = Shammi Kapoor  Mala Sinha Raaj Kumar Leela Chitnis
| music          = Shankar Jaikishan
| cinematography = Shivram Malaya
| editing        = Pran Mehra
| distributor    =
| released       =  
| runtime        = 109 mins
| country        = India
| language       = Hindi
| budget         =
| gross          =
| website        =
}}
Ujala (Ujala means Light in Hindi) is a Bollywood movie released in 1959, starring Shammi Kapoor, Mala Sinha, Raaj Kumar, Leela Chitnis and Tun Tun. Naresh Saigal was the story writer as well as the director of the movie. Ujala  features one of the popular Bollywood songs "Jhoomta Masoom, Mast Mahina" played by Shammi Kapoor and Mala Sinha, sung by Manna Dey and Lata Mangeshkar. The film came with a message; "Honesty always triumphs". 

==Plot==
Ramu  (Shammi Kapoor), his family consisting of his mother (Leela Chitnis) and three siblings, and his sweetheart Chhabili (Mala Sinha) are poor, but they dream of a better life  and keep trying to achieve it. In the process his childhood friend and villain in the movie Kalu (Raaj Kumar) creates obstacles for his ambitions.

Ramu is dragged into an evil world, where Kalu makes him realize that it is easy money to rob someone; honesty apparently has no value. Ramu gets pulled into this world when his sister meets with a car accident. He needs money for her treatment and she dies due to the lack of it. Ramu now joins Kalu and his gang. Kalu kills one of his gang members who betrays him. Kalu convinces Ramu that he may get blamed for the murder so Ramu goes into hiding. He works for a man making knives from where Kalu steals a silver knife, which belongs to a police officer. This implicates Ramu further where hes again on the run. After a final fight with Kalu, Ramu is able to prove his innocence.

==Cast==
* Shammi Kapoor as Ramu
* Mala Sinha as Chabeli
* Raaj Kumar as Kalu
* Leela Chitnis as Ramus mother Dhumal as  Bholu Kumkum as Kammo
* Ramesh Sinha as Police Inspector
* Shivraj as Acharya

==Music== Shailendra and Hasrat Jaipuri.
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Jhoomta Masoom, Mast Mahina| extra1 = Manna Dey, Lata Mangeshkar | lyrics1 = | length1 =
| title2 = Tera Jalwa Jisne Dekha | extra2 = Lata Mangeshkar | lyrics2 = | length2 =
| title3 = Duniya Walaon Se Door | extra3 = Mukesh (singer)|Mukesh, Lata Mangeshkar | lyrics3 = | length3 =
| title4 = Ab Kahan Jaye Hum| extra4 = Manna Dey | lyrics4 = | length4 =
| title5 = O Mora Nadan Balma | extra5 = Lata Mangeshkar | lyrics5 = | length5 =
| title6 = Suraj Jara Aa Pass Aa | extra6 = Manna Dey | lyrics6 = | length6 =
}}

==References==
 

==External links==
*  

 
 
 