The Naked Island
{{Infobox film
| name           = 裸の島 The Naked Island
| alt            =  
| image	         = The Naked Island FilmPoster.jpeg
| caption        = 
| director       = Kaneto Shindo
| producer       = Kaneto Shindo Matsuura Eisaku
| writer         = Kaneto Shindo
| starring       = Nobuko Otowa Taiji Tonoyama Shinji Tanaka Masanori Horimoto
| music          = Hikaru Hayashi
| cinematography = Kiyomi Kuroda
| editing        = Toshio Enoki
| studio         = Kindai Eiga Kyokai
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese (no dialogue)
| budget         = 
| gross          = 
}}
 
The Naked Island ( ) is a 1960 Japanese art film directed by Kaneto Shindō. The film was made in black-and-white, and is notable for having no spoken dialogue.

==Plot==
The film depicts a small family, a husband and wife and two sons, struggling to get by on a tiny island in the Seto Inland Sea, over the course of a year. They are the islands only occupants, and survive by farming. They must repeatedly carry the water for their plants and themselves in a row boat from a neighboring island.

When the boys catch a large fish, the family travels to Onomichi by ferry, where they sell it to a fishmonger, then eat at a modern restaurant.

While the parents are away from the island, the older son falls ill. The desperate father runs to find a doctor to come to treat his son, but when they arrive, the boy is already dead. After the boys funeral, which is attended by his classmates from his school on the neighboring island, the family resumes their hard life, with very limited opportunity for grief.

==Production==
Director and scriptwriter Kaneto Shindo decided to make this film because he wanted to make a film without any dialogue.    The independent production company Kindai Eiga Kyokai was close to bankruptcy at the time this film was made, and Shindo sank his last funds into making the film. The films financial success saved the company. 

The lead actor Taiji Tonoyama was suffering from severe liver disease due to alcohol dependence, but recovered his health because there was no alcohol available near the filming location.   These events were later dramatized in Shindos biopic about Tonoyama, By Player.

In his last published book before he died, Shindo noted that the films premise of carrying water to the island is false, because the crop shown being watered in the film, sweet potatoes, do not actually need extensive watering. 

Shindo deliberately made the actors carry heavily-loaded buckets of water so that the yokes they were using would be seen to bend, symbolizing the harshness of their lives.

The island location in the film is an uninhabited island called Sukune off the coast of a larger island called Sagishima, part of Mihara, Hiroshima. 

==Reception== Chistoye nebo) at the 2nd Moscow International Film Festival, at which Luchino Visconti (whose La Terra Trema is an inspiration) was a jury member.   

In 1963, the film was nominated for "Best Film from any Source" at the 16th British Academy Film Awards.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 