Frankenstein's Great Aunt Tillie
{{Infobox film
| name           = Frankensteins Great Aunt Tillie
| image          = "Frankensteins_Great_Aunt_Tillie"_(1984).jpg
| image_size     = 
| alt            = 
| border         = 
| caption        = 
| director       = Myron J. Gold
| producer       = Myron J. Gold
| writer         = Myron J. Gold
| starring       = Donald Pleasence Yvonne Furneaux June Wilkinson Aldo Ray
| music          = Ronald Stein
| cinematography =  Miguel Garzón
| editing        = 
| studio         = Tillie Productions S.A. Filmier
| distributor    = 
| released       = 1984
| runtime        = 100 minutes 
| country        = United States/ Mexico/ UK
| language       = English
| budget         = 
}}
Frankensteins Great Aunt Tillie is a 1984 comedy film about Frankenstein that is based in Transylvania. 

June Wilkinson, who had a part in the film, was interviewed in the book Screen Sirens Scream! about her role.  The films music was written about in the book Musique fantastique. 

==Plot==
Castle Frankenstein has been empty for years and the local council is planning to repossess it, when the Frankenstein family return, seeking to find hidden treasure, and to re-animate the Frankenstein monster. 

==Cast==	
*Victor Frankenstein / Old Baron Frankenstein - Donald Pleasence	
*Matilda Tillie Frankenstein - Yvonne Furneaux	
*Randy - June Wilkinson	
*The Monster -  Miguel Angel Fuentes		
*Bürgermeister - Aldo Ray	
*Clara - Zsa Zsa Gabor	
*Grocer Schnitt - Rod Colbin	
*Police Superintendent Schwerbaum - Chandler Garrison	
*Banker Schlockmocker - Phil Leeds	
*Lawyer Schnabel - Garnett Smith	
*Judge Edelweiss - Ken Smith	
*Secretary Gudrun Geduldig - Karen Haber	
*Feldwebel Erstarren - Edgar Vivar	
*Big Black Bill (as Borolas) - Joaquín García Vargas

==Critical reception==
The Bloody Pit of Horror wrote, "I literally cringe at the thought of ever stumbling across something as bad as this in the future. To make it worse, the damn thing goes on forever."  

==External links==
* 

==References==
 

 
 
 
 
 
 

 