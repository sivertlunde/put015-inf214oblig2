The Hero of the Dardanelles
 
{{Infobox film name           = The Hero of the Dardanelles image          = Hero_of_the_Dardenelles.jpg caption        = Still from film director  Alfred Rolfe  writer         = Phillip Gell Loris Brown starring       = Guy Hastings Loma Rossmore studio   = Australasian Films distributor    =   released       = 17 July 1915 runtime        = 59 minutes (4,000 feet) country        = Australia language       = English  budget         = 
}} Alfred Rolfe, made as a patriotic war recruiting film.

==Plot==
Will Brown (Guy Hastings) enlists in the Australian Army after the outbreak of World War I. He goes through training at Liverpool, New South Wales|Liverpool, near Sydney, and encourages other men to join up, putting up a recruiting poster. A pacifist tears the poster down but Will sees him off. Before leaving Australia, he proposes to Lily Branton (Loma Rossmore).

Arriving in Egypt, Will is stationed at Mena camp near the Great Pyramid at Giza, and trains with the other troops. They then move to Gallipoli, where Will takes part in the first wave ashore on 25 April 1915. He fights a Turkish sniper hand to hand and drowns him. He returns home wounded and marries Lily. The film ends with a call for Australian men to enlist.

==Cast==
*Guy Hastings as William Brown
*Loma Rossmore as Lily Brunton
*C. Throoby as Mr. Brown
*Ruth Wainwright as Mrs. Brown
*Fred Francis as Gordon Brown

==Production==
The movie was the first feature from Australasian Films since 1912, although they had made newsreels and short films during that time. It was a sequel to Will they Never Come? (1915), using many of the same cast and crew.  It too was made with the co-operation of the Department of Defence. 

The film was written by the same team who had done Will They Never Come?. Co-writer Phil Gell allegedly wanted to enlist but was asked not to until he had written this film. 

Some commentators believe Australasian Films were partly motivated to make the movie to ensure government protection of the film industry during the war. 

The landing at Gallipoli was restaged with one thousand real troops from Liverpool Camp at Tamarama Beach in Sydney. 
 On Our Selection. 

The original film ran for approximately 4,000 feet (59 minutes at 18 frames per second), but only 21 minutes survives.

==Reception== Prime Minister and Premier of Victoria. A copy was placed in the archives of Federal Parliament.  It was used as a recruiting tool, taken around the country by members of the armed forces for special screenings. 

Footage from the film was later used in the movie The Spirit of Gallipoli (1928).

==References==
 
* 

==External links==
* 
*The Hero of the Dardanelles at the  
*  at the National Film and Sound Archive
*  at AustLit
 

 
 
 
 
 
 
 
 