The Chaplin Revue
{{Infobox film
| name           = The Chaplin Revue
| image          = ChaplinRevue.jpg
| caption        = 
| director       = Charlie Chaplin
| starring       = Charlie Chaplin
| cinematography =  Paul Davies  Derek Parsons
| stduio         = Charles Chaplin Productions Roy Export Company
| distributor    = United Artists Warner Bros. (DVD) Playhouse Home Video (1985) (USA) (VHS) Key Video (1989) (USA) (VHS) Park Circus (2010) (UK) (DVD) Roy Export Company (all media) Soul media (2011) (Denmark) (all media)
| released       =  
| runtime        = 119 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          = 
}} The Pilgrim.  All three star Chaplins trademark character, The Tramp.  For the 1959 release, Chaplin added a soundtrack to help appeal to modern audiences.  Chaplin also added extra footage including clips from   to express the context.  He provides a personal introduction to each of the clips.
 Pay Day, David Robinson, and behind-the-scenes footage are also included.

== Background ==

During the silent era, Charlie Chaplin was one of the biggest stars in Hollywood. Unfortunately for Chaplin though, he fell victim to McCarthyism and in the 1950s, he lived in exile in Switzerland with his wife Oona ONeill.  He latest three films, Monsieur Verdoux, Limelight (1952 film)|Limelight, and A King in New York, were not universally acclaimed and his star power was fading.  The idea of reviving his role as The Tramp for modern audiences was entertained.      Modern Times, as a vehicle for expressing his ideas.  Critic and friend James Agee wrote a script placing Chaplins trademark character, the Tramp, in apocalyptic New York. 

Chaplin decided the best way to bring the tramp into the new era was by re-releasing three silent films he made with First National as a feature length film. Released in 1959, The Chaplin Revue consisted of A Dogs Life, Shoulder Arms and The Pilgrim&nbsp;– each of which was introduced by Chaplin and juxtaposed with behind the scenes footage and clips from World War I.

==A Dogs Life==
 

Charlies tramp lives alone on the street.  He persistently tries to get a job but there are none left at the employment agency when he gets there.  Down and out, Charlie returns to the streets to find Scraps, a stray dog being attacked.  He rescues her and the two form a bond. They fill the gap in each others lives and they are no longer lonely&nbsp;– or hungry.  Later, they attend a music hall where Edna Purviance is singing while being annoyed by the clubs patrons. Charlie and Scraps are kicked out and Edna is fired.  Scraps finds money in a lost wallet, which is then stolen by a pack of thieves.  Charlie manages to thwart their theft and he, Scraps, and Edna enjoy their newfound happiness, together.

==Shoulder Arms==
 

Originally released in the USA in October 1918, the relatively short black and white silent film ran for 46 minutes and finds Charlie playing the new recruit in the war effort against the Germans. 
Charlie has no friends and seems to make enemies with his allies at the drop of the hat. 
Through sheer dumb luck, Charlie stumbles into the enemy trenches and captures 13&nbsp;German soldiers. After this "heroic" act, Charlie is given the duty of infiltrating enemy lines further under the guise of a tree trunk. His shining moment comes when he is hunted down by the Kaiser but with some quick thinking, reverses the ambush and captures the Kaiser for the allies. His fellow soldiers cheer him as a great wartime hero. He then awakes from his dream.

==The Pilgrim==
 

An escaped convict (Chaplin) dons the vesture of a clergyman and is mistakenly appointed as the new pastor of the small town of Devils Gulch.  After acquainting himself with a local mother and daughter, and subsequently moving in with them, one of his former buddies from prison arrives and steals from the two women.  Charlie tries to get their money back but his former life is discovered by the sheriff who takes him to the border of Mexico; facing life as a convict if he returns.

== Cast ==

{| class="wikitable"
|- bgcolor="#d1e4fd"
! Actor !! A Dogs Life !! Shoulder Arms !! The Pilgrim
|-
|rowspan="1"| Charlie Chaplin || The Tramp || A Recruit || An escaped convict
|-
|rowspan="1"| Edna Purviance || Bar Singer || French Girl || Miss Brown
|-
|rowspan="1"| Syd Chaplin || Lunchwagon Owner || The Kaiser || Boys Father
|-
|rowspan="1"| Henry Bergman || Unemployed man || Field Marshal || Sheriff on train
|-
|rowspan="1"| Charles Reisner || Agency Clerk || - || Crook
|-
|rowspan="1"| Albert Austin || Clerk ||Chaffeur || -
|- Tom Wilson || Policeman || Sergeant || -
|-
|rowspan="1"| Loyal Underwood ||  -  || Short German Officer || Elder
|- Jack Wilson || - || Crown Prince || -
|- John Rand || - || German Soldier || -
|-
|rowspan="1"| J. Park Jones || - || U.S. Soldier || -
|- Tom Murray || - ||- || Sheriff Bryan
|-
|rowspan="1"| Dean Riesner || - || - || Little Boy
|-
|rowspan="1"| Mai Wells ||  -  || - || Little Boys Mother
|-
|rowspan="1"| Mack Swain || - || - || Deacon
|-
|rowspan="1"| Kitty Bradbury || - || - || Mrs. Brown (Ednas Mother)
|-
|rowspan="1"| M.J. McCarthy || Unemployed man || - || -
|- Mel Brown || Unemployed man || - || -
|-
|rowspan="1"| Charles Force || Unemployed man || - || -
|-
|rowspan="1"| Bert Appling || Unemployed man || - || -
|- Thomas Riley || Unemployed man || - || -
|-
|rowspan="1"| Slim Cole || Unemployed man || - || -
|-
|rowspan="1"| Ted Edwards  || Unemployed man || - || -
|-
|rowspan="1"| Louis Fitzroy  || Unemployed man || - || -
|-
|}

==Crew==

Credited for the 1959 release:   

*Charles Chaplin - Director
*Charles Chaplin - Producer
*Charles Chaplin - Writer
*Charles Chaplin - Original Music Paul Davies - Editor
*Derek Parsons - Editor Bob Jones - Sound Recordist
*Wally Milner - Sound Recordist
*J.J.Y. Scarlett - Sound Recordist
*Eric Stockl - Sound Recordist Eric James - Music Arranger Eric Rogers - Conductor
*Eric Spear - Music Arranger
*Jerome Epstein - Assistant to Mr. Chaplin

==Release dates==
*UK: September 1, 1959
*Finland: December 25, 1959
*Denmark: July 28, 1960
*Germany: October 9, 1997: 

==Reception==

The Chaplin Revue was critically acclaimed when released in 1959.  According to Top Ten Reviews, which gives ratings based on average critic scores, it is ranked:

* 88th&nbsp;– 1959
* 835th&nbsp;– 1950s
* 3,095th&nbsp;– comedy

Concerning the DVD release, reviewer Robert Horton says: "This box set is more than film history; its a living treasure."

However, some reviewers have been critical of the re-release due to its format. To allow for a soundtrack, the original footage was stretched and certain frames were duplicated. Walter Kerr in The Silent Clowns declares that the "cadence of all three films, and of Chaplins work in them, is utterly destroyed. Let no newcomer to the form begin acquaintance with Chaplin on such terms; only the originals will do." 

== References ==
 

  
{{succession box |
  | before = A King in New York 1957
  | after = A Countess from Hong Kong 1967 Films by Charlie Chaplin The Chaplin Revue 1959
  | years = |}} 
 

 

 
 
 
 
 
 
 
 
 
 