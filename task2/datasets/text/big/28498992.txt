Hard Guy
{{Infobox film
| name           = Hard Guy
| image_size     =
| image	=	Hard Guy FilmPoster.jpeg
| caption        =
| director       = Elmer Clifton Arthur Alexander (producer) George R. Batcheller Jr. (executive producer) Alfred Stern (producer) Oliver Drake (writer)
| narrator       =
| starring       =
| music          = Eduardo Durant
| cinematography = Edward Linden
| editing        = Charles Henkel Jr.
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
}}

Hard Guy is a 1941 American film directed by Elmer Clifton.

The film is also known as Professional Bride in the United Kingdom.

==Plot==
 
Vic Monroe is the proprietor of the Tropical Inn nightclub. He also runs a special racket involving the women working for him at the club. The female employees catch eligible bacjelors, marry them and then gets the marriage annulled immediately, making quite a profit from the settlement.

Monroes now plans to marry off one of his dancers, Doris Starr, who is still oblivious about the annulment racket going on behind her back. Monroe has his sights on "the catch of the season", Anthony Tremaine, Jr., and Monroes task is to get the two to meet so that they fall into each others arms.

Monroe is the mastermind of the annulment racket and his latest scheme is to marry dancer Doris Starr, who is ignorant of the shakedown racket, to "catch of the season" Anthony Tremaine, Jr.

Monroe manages to pull it off and the two love birds are hastily married without their respective families knowledge. Monroe then sees to it that the story ends up in the papers, to alert the wealthy former bachelor of his recent romantic endeavours. Soon Tonys father surfaces, surprising the couple on their honeymoon, loudly threatening to disinherit his son if he doesnt end the marriage immediately.

Because of the loud Tremaine, Sr., Julie Cavanaugh, Doris sister,  and the rest of her family and the neighbors get involved in the matter. A big brawl ensues between the Tramaines and the rest. The police arrives on the scene and arrests everyone involved in the fighting, including Steve Randall, the governors son.

The next morning the papers are filled with news of the fighting, which they call the "wedding banquet riot". Monroe is overjoyed by the news, and uses his lawyer friend, Ben Sherwood, to pretend being Doris father and negotiate a big settlement to keep a lid on the matter. Doris and Tony quickly drift apart because of the trouble surrounding their marriage.

An investigation of the Tropical Inn starts, led by State Detective Tex Cassidy, working undercover, pretending to be a friend of the governor. He visits the club and brings the governors son Steve. Soon they are accompanied by Julie and another woman who calls herself Goldie Duvall.Doris is worried for her sisters sake, and tells her to stay as far away from Monroe as she can. Monroe has led Doris to believe that he will marry her and she believes this, even though she knows he is as crooked as they come.

When Doris sees Monroe kissing another woman in the club, she immediately quits her job in anger. She also says she will give Tony back the money from the settlement and leaves. Monroe follows her to her apartment and kills her. He is soon suspected of the murder since he was the last person seen alone with Doris.

Julie starts looking into the circumstances surrounding her sisters death, and finds out that "her father" threatened to take legal action against Tony if they didnt settle. Since Julies father has been dead for years she knows something is fishy. She suspects that Monroe is the one murdering her sister.

After Monroe finds out that Doris had written down the serial numbers of all the notes from the Tremaine settlement, he breaks into her home to find and retrieve them. He doesnt find them.

Julie discovers that Monroe in fact runs an annulment racket and pretends to be interested in participating to be able to investigate him from up close and find more evidence. Monroe happily plans to marry her to the governors son Steve. After the marriage, however, Julie refuses to go through with the annulment.

Steve and Monroe get into a fight, and after Steve knocks Monroe in the head, the club owner pulls a gun on Steve. At gunpoint, Monroe forces the couple to sign an annulment agreement.

Later in the evening, Steve returns to the club with a gun, and there is a shootout. It ends when the governor himself arrives to the scene, bringing all the serial numbers Doris wrote down, matching them to the money in a briefcase Monroe is carrying in his hand.

When Monroe is caught and arrested, Steve and Julie continue their true romance and Goldie reveals she hasfallen in live with Tex, the detective. 

==Cast==
*Jack La Rue as Vic Monroe Mary Healy as Julie Cavanaugh
*Kane Richmond as Steve Randall
*Iris Adrian as Goldie Duvall
*Gayle Mellott as Doris Starr
*Jack Mulhall as Tex Cassidy
*Howard Banks as Anthony Tremaine Jr.
*Ben Taggart as Ben Sherwood
*C. Montague Shaw as Anthony Tremaine, Sr.
*Inna Gest as Mona Day Arthur Gardner as Dick Clayton

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 