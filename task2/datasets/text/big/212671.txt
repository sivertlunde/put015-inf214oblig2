So I Married an Axe Murderer
{{Infobox film
| name           = So I Married an Axe Murderer
| image          = Axemurderermovieposter.jpg
| caption        = Theatrical poster
| director       = Thomas Schlamme
| producer       = Robert N. Fried Cary Woods
| writer         = Robbie Fox Uncredited: Mike Myers Neil Mullarkey
| starring       = Mike Myers Nancy Travis Anthony LaPaglia Amanda Plummer Brenda Fricker
| music          = Bruce Broughton
| cinematography = Julio Macat
| editing        = Colleen Halsey Richard Halsey
| distributor    = TriStar Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$20 million
| gross          = $11,585,483
}}
So I Married an Axe Murderer is a 1993 American romantic comedy thriller film starring Mike Myers and Nancy Travis. Myers plays Charlie McKenzie, a man afraid of commitment until he meets Harriet (Travis), who works at a butcher shop and may be a serial killer. In addition to playing the main character, Myers also plays Charlies father, Stuart.

This was Myers first film after achieving success in the Waynes World (film)|Waynes World franchise and was not well received by most mainstream critics or at the box office, grossing a total of USD $11 million in North America, well below its $20 million budget.

== Plot ==
Charlie McKenzie (Mike Myers|Myers) is a beat poet living in San Francisco, after having broken up with yet another woman based on paranoid perception. His policeman friend Tony tries to point out the pattern; that Charlie simply is afraid of commitment and tries to think of, or invent, any reason to break up with someone. Things pick up again when Charlie meets a butcher named Harriet, and becomes infatuated instantly.

After a while of driving near her workplace, he goes in and volunteers to help her out that day, and eventually a date ensues that evening, with him learning various factoids about her. He learns she used to live in Atlantic City, used to be involved with a trainer for Russian martial arts, and screams at someone named Ralph in her sleep. The next day she is gone when he awakes, but her sister Rose, with whom she lives, is there and tries to get to know him a bit. She then kindly warns him to be careful.

Meanwhile, Tony begins losing his enthusiasm for his job on the police force, frustrated that there is no action or comparison to how they portray it on TV. He tries to get his chief to become more short tempered, as per the stereotype in movies, by taking on bigger jobs. Charlie comes to Tony begging him to act as a voice of reason for the times ahead, not wanting to get hung up or paranoid about anything he may find odd about her. Charlie and Harriet begin falling deeper in love; he finds that she gets along great with his friends, they have perfectly matched passions, so he takes her to meet his parents.
 Russian martial arts student.

Now strongly suspicious of her, Charlie begs Tony to investigate. After finding that the husbands in question that have disappeared have been reported missing along with their wives, Charlie tries to let the issue go. Unfortunately his panic gets worse, suspecting her all the while but trying to cover it up, and eventually he breaks up with her. After a few days of gloom, Tony informs him that one of the husbands killers confessed. Charlie, realizing that he needs to make changes, rushes back to Harriet, informing her that he will see a therapist. He wins her over by playing one of his poems from her rooftop, and as they reconcile, every doubt he had gets disproved. Her friend Ralph was actually a woman, for example.

At his parents 30th year anniversary, Charlie proposes to Harriet, and soon a wedding is underway. Later, they venture off to a secluded hotel up in the mountains for their honeymoon. Meanwhile, back at the police station, the chief - now playing the short-tempered stereotype - informs Tony that the woman who had taken credit for the murder of the plumber was actually a compulsive confessor. Tony faxes a photo of Harriet to all of the known associates of the three missing husbands, and they all recognize her as their friends wife.

Tony tries desperately to contact Charlie, but finds that the phone lines are down due to a storm where they are honeymooning, so he charters a plane. Upon landing he calls Charlie and warns him that Harriet is "Mrs. X", but before he can tell him anything else, their phone line gets knocked down, and the power goes out. Charlie desperately tries to escape, but Harriet finds him and begins acting unsettled and confrontational as he tries to leave her without letting on that he knows her traits.

The endearing staff literally forces them into their hotel room to spend their first night together, and Charlie finds himself alone in the room with Harriet and an axe for the firewood. Harriet locks the door and begins telling him that shes been married before. He forces her into the closet and locks it, then looks down at their desk and sees a "Dear Jane" letter for Harriet, saying he has run away.

Harriets sister Rose then suddenly leaps from the shadows and swipes at Charlie with the axe, stating that the husband "is never supposed to be in the room when the note is found", revealing herself to be the true killer. Her motive was the belief that each husband had taken her sister from her. Charlie has no option but to run to the roof and try to evade Rose long enough for the police to show up.

Eventually Tony leads the police into the hotel but arrests Harriet, still believing her to be the murderer. Rose swings the axe at Charlie and is thrown off the building, with only Charlie holding her up from falling to her death, Tony notices and comes to her aid. The authorities take her away, and Charlie and Harriet resume their lives afterward as a happy couple.

==Cast==
*Mike Myers as Charlie and Stuart MacKenzie
*Nancy Travis as Harriet Michaels
*Anthony LaPaglia as Tony Giardino
*Amanda Plummer as Rose Michaels
*Alan Arkin as Police Captain
*Michael Richards as Insensitive Man
*Brenda Fricker as May MacKenzie Matt Doherty as William "Head" MacKenzie
*Charles Grodin as Commandeered Driver
*Phil Hartman as John "Vickie" Johnson, Alcatraz Guide
*Debi Mazar as Susan, Tonys Girlfriend
*Steven Wright as Pilot Jessie Nelson as Ralph

== Production ==
The genesis of the film originated in 1987 when producer Robert N. Fried met with writer Robbie Fox to discuss story ideas. They ended up talking about the problems they had with women and agreed that "most women appeared to be out to destroy us!" the producer recalled. {{cite news
 | last =
 | first =
 | coauthors =
 | title = So I Married an Axe Murderer Production Notes
 | work =
 | pages =
 | language =
 | publisher = TriStar Pictures
 | year = 1993
 | url =
 | accessdate = }}  Fried and co-producer Cary Woods formed their own production company in 1992 and So I Married An Axe Murderer was their first film  and they were given a $20 million budget. {{cite news
 | last = Stanley
 | first = John
 | coauthors =
 | title = Choppy Waters for Axe Murderer
 | work = San Francisco Chronicle
 | pages =
 | language =
 | publisher =
 | date = August 1, 1993
 | url =
 | accessdate = }}  Fox wrote the screenplay in 1987. {{cite news
 | last = Portman
 | first = Jamie
 | coauthors =
 | title = Errors of Outrageous Fortune
 | work = Toronto Star
 | pages =
 | language =
 | publisher =
 | date = July 30, 1993
 | url =
 | accessdate = }}  In the original version, Charlie was Jewish and, according to Fried, it was "initially conceived as being more about paranoia than commitment".  Myers wanted changes to the script that would allow him to do some serious acting and Saturday Night Live-style comedy.  He extensively rewrote the script with writer Neil Mullarkey, an old friend from Britain.  According to Myers, they changed the story and many of the comedic moments. Fox was asked to consider a new set of credits that gave him a "story by" and co-screenplay credit. He rejected the proposal and in arbitration, the Writers Guild of America decided that Fox would receive sole screenwriting credit.  Fried and Myers were upset that Mullarkey, who put a lot of work into the script, did not get any credit. 

===Casting===
To play Charlie MacKenzie, Fried and co-producer Cary Woods asked Mike Myers because of the success he had with the first Waynes World film.  He agreed because he liked the script and many of his friends also had a fear of commitment and "were all suffering from cold feet and what is cold feet but a low-grade terror? This story just expands on that terror". {{cite news
 | last = Kolnow
 | first = Barry
 | coauthors =
 | title = Has Success Spoiled Mike Myers? Comic Says No
 | work = Montreal Gazette
 | pages =
 | language =
 | publisher =
 | date = August 3, 1993
 | url =
 | accessdate = }}  Before he was approached, Woody Allen considered playing Charlie. Chevy Chase, Albert Brooks, and Martin Short also considered the role but did not like the character.  Sharon Stone was initially set to play Harriet Michaels, the supposed axe murderer of the films title. Like Myers who has two roles in the film, Stone wanted to play Harriet and the role of Rose, Harriets sister, the latter part portrayed in the film by Amanda Plummer. Studio executives at Sony did not like the idea of Stone playing both roles, and upon realizing this she refused to accept the part. Nancy Travis was then cast in the role instead.  Travis was drawn to her characters "qualities of danger and compassion mixed with humor   make her an intriguing character".  Anthony LaPaglia said that his character "has grand illusions of being Serpico, you know hes ready to fight crime like his hero did. But hes just not competent".  When the actors did the first cast read-through of the script, Charlies father, Stuart, had not yet been cast. Myers read that characters lines and the filmmakers so enjoyed his interpretation that they realized he could play that role as well.  The film also features cameos by Charles Grodin, Phil Hartman, Michael Richards, Mike Hagarty, Debi Mazar, Steven Wright, and Alan Arkin, appearing uncredited as Tonys sensitive boss. They all agreed to be in the film for the opportunity of working with Myers. 

===Principal photography===
To fit both Charlie and his father in the same scene together, the filmmakers used a split screen (filmmaking)|split-screen process. To look the role of Stuart, Myers spent over three-and-a-half hours having special prosthetic make-up applied.  While filming scenes in the butcher shop, Nancy Travis was distracted by Myers antics and accidentally took off the tip of her middle finger on her left hand while chopping vegetables with a kitchen knife.  A local doctor sewed her finger back together. Travis said that she and Myers did a lot of improvising together and he taught her a lot about comedy and showed her "how to be relaxed and spontaneous on the set". 

There were stories in the press that Myers over-inflated ego forced extensive re-shoots on the film and that he tried to deny Robbie Fox credit for writing the film.  On the set, director Thomas Schlamme said that he had his differences with Myers over how the film should be shaped. He said that Myers was "taking a stretch beyond his usual self and was playing outside himself. Personality clashes were bound to happen. We struggled".  However, the director denied that Myers was a control freak and praised the "total commitment to his work. (But) yes, it was difficult".  In addition, several major newspapers and magazines claimed that the film went over budget, with in-fighting among the principal actors and lengthy release delays, and that it was unfunny.  Despite these early reports, So I Married an Axe Murderer scored high at test screenings. {{cite news
 | last = Fook
 | first = John Evan
 | coauthors =
 | title = Axe Murderer Fete to Benefit Frisco Film Commish Variety
 | pages =
 | language =
 | publisher =
 | date = July 15, 1993
 | url =
 | accessdate = }} 

===Locations===
Set in San Francisco, California, the film features many famous sights and neighborhoods of the Golden State metropolis, including the Golden Gate Bridge, the Palace of Fine Arts and Alcatraz. One scene on Alcatraz was filmed in "A" block, an area that is still in its original state from 1912 and is not open to the public.  The space for actors, crew and equipment is narrow, a challenge also faced by the makers of The Rock, which has Nicolas Cages and Sean Connery escaping from their cells.  The restaurant where Myers and Travis double date with LaPaglia and Debi Mazar is the Fog City Diner. The butcher shop used for "Meats of the World" was Prudente Meats on Grant Ave in the North Beach Section of San Francisco. The final scenes are set at Dunsmuir House in the East Oakland foothills. Additional special effects and matte paintings created the illusion that the location was secluded among mountains.  The exterior shot for the café, Cafe Roads, where Myers recites his beat poetry, is the bar Vesuvio, at Columbus Avenue and Jack Kerouac Alley. The filmmaker picked San Francisco as the films setting because it was an ideal place for a poet like Charlie to live. Myers has said that he was attracted to its "coffeehouse culture, with its clothes and music and its whole sensibility ... people arent going to bars as much. They tend to go out and have coffee".  Several sets were built in warehouses near Candlestick Park and these soundstages were used for many weeks. 

== Soundtrack ==
{{Infobox album  
| Name        = So I Married an Axe Murderer (Original Motion Picture Soundtrack)
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = Axemurderersoundtrack.jpg
| Released    = July 27, 1993
| Recorded    =
| Genre       = Soundtrack
| Length      = 39:14 Columbia
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =   
| noprose = yes
}}

=== Track listing ===
# Boo Radleys - "There She Goes" 2:18
# Toad the Wet Sprocket - "Brother" 4:04
# Soul Asylum - "The Break" 2:46
# Chris Whitley - "Starve To Death" 3:14 Big Audio Dynamite II - "Rush (BAD song)|Rush" 3:55
# Mike Myers - "This Poem Sucks" 2:04
# Neds Atomic Dustbin - "Saturday Night" 3:08
# The Darling Buds - "Long Day In The Universe" 4:08
# The Spin Doctors - "Two Princes" 4:15 Suede - "My Insatiable One" 2:57
# Sun-60 - "Maybe Baby" 3:43
# The Las - "There She Goes" 2:42

=== Other music in the film === Saturday Night"
* Ron Gonnelia - "A Touch of Gaelic"
* Rod Stewart - "Da Ya Think Im Sexy" (sung by Mike Myers) Only You"

===Score===
The films original score was composed and conducted by Bruce Broughton. Due to the movie having a heavy emphasis on popular music, several of Broughtons cues were replaced with songs (such as his main title music being supplanted by The Boo Radleys version of "There She Goes," and "Butcher Shop Montage" having Big Audio Dynamites "Rush" substituted). However, Broughton was appreciative of the support music supervisor Danny Bramson and director Thomas Schlamme gave him, stating "I was given the opportunity to make my case, and I didnt get slighted... But the way it ended up was the way it ended up. It was done with a lot of creative latitude, and creative permission and confidence. I cant complain about the way it came out." 

Intrada Records released an album of his music on November 25, 2013, featuring the complete score plus alternates and original versions of cues.

# Main Title 	2:55
# Boy Meets Girl 	1:09
# Weekly World News 	0:38
# Butcher Shop Montage 	1:58
# Russian Stroll 	2:23
# Goin For It 	2:19
# Forever Wet 1 	1:00
# Forever Wet 2 	0:59
# Creeping Doubt (Revised) 	1:05
# Ralph 	1:05
# Name Your Poison 	1:12
# Ear Needles (Revised) 	0:45
# Globe Bridge 	1:08
# Reflections 	0:42
# You Blew It 	0:27
# The Bath (Revised) 	0:54
# Shes Guilty (Revised) 	1:04
# Wedding Bands 	0:32
# Inn Source #2 	2:14
# Stalking 	0:42
# Shes Mrs. Axe (Revised) 	0:57
# The Finale 	5:27
# The Finale Part 2 	3:13
# End Theme 	1:35
# Go See The Folks 	0:17
# Dinner Drive (Original) 	0:27
# Goin For It (Revised) 	2:19
# Creeping Doubt (Original) 	1:05
# Ear Needles (Original) 	0:14
# Globe Bridge (Alternate) 	0:29
# Second Thoughts 	0:13
# Second Thoughts (Revised) 	0:12
# The Bath 	0:52
# Shes Guilty 	1:05
# Shes Mrs. Axe 	0:57
# The Finale Part 2 (Alternate) 	2:26
# Inn Source #1 (Haydn Quartet) 	1:46
# Inn Source #2 (Alternate) 	1:32
# Only You (And You Alone)	1:31

Total Time: 52:56

== Critical reception ==
So I Married an Axe Murderer was first shown at a screening to benefit the local San Francisco film office on July 27, 1993 at the Palace of Fine Arts Theater and had its official world premiere at the Galaxy Theater in Hollywood on July 28 with Myers, Travis, and LaPaglia in attendance.  The film was not well received at the box office. It was released on July 30 in 1,349 theaters and grossed a total of USD $3.4 million on its opening weekend. It made a total of $11.5 million in North America, well below its $20 million budget. {{cite news
 | last =
 | first =
 | coauthors =
 | title = So I Married An Axe Murderer
 | work = Box Office Mojo
 | pages =
 | language =
 | publisher =
 | date =
 | url = http://www.boxofficemojo.com/movies/?id=soimarriedanaxemurderer.htm
 | accessdate = 2007-09-26 }} 

The film had very mixed reviews and has a 52% rating on Rotten Tomatoes based on 31 reviews. Roger Ebert, in his review for the Chicago Sun-Times, criticized the film for being "a mediocre movie with a good one trapped inside, wildly signaling to be set free". {{cite news
 | last = Ebert
 | first = Roger
 | coauthors =
 | title = So I Married An Axe Murderer
 | work = Chicago Sun-Times
 | pages =
 | language =
 | publisher =
 | date = July 30, 1993
 | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19930730/REVIEWS/307300302/1023 Rolling Stone Trouble with Harry. Axe is a blunt instrument". {{cite news
 | last = Travers
 | first = Peter
 | coauthors =
 | title = So I Married An Axe Murderer
 | work = Rolling Stone
 | pages =
 | language =
 | publisher =
 | date = August 19, 1993
 | url = http://www.rollingstone.com/reviews/movie/5948929/review/5948930/so_i_married_an_axe_murderer
 | accessdate = 2007-09-26 }}  Entertainment Weekly gave the film a "C-" and said, "In some perverse way, So I Married an Axe Murderer seems to be asking us to laugh at how not-funny it is". {{cite news
 | last = Gleiberman
 | first = Owen
 | coauthors =
 | title = So I Married An Axe Murderer
 | work = Entertainment Weekly
 | pages =
 | language =
 | publisher =
 | date = August 13, 1993
 | url = http://www.ew.com/ew/article/0,,307643,00.html
 | accessdate = 2008-05-05 }}  In his review for the Washington Post, Hal Hinson had a mixed reaction to Myers performance: "Everything he does is charmingly lightweight and disposable and reasonably impossible to resist. And in the end, because the character is so easily within reach for him, you may come away feeling a little cheated, as if you hadnt quite seen a movie at all". {{cite news
 | last = Hinson
 | first = Hal
 | coauthors =
 | title = So I Married An Axe Murderer
 | work = Washington Post
 | pages =
 | language =
 | publisher =
 | date = July 30, 1993
 | url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/soimarriedanaxemurdererpg13hinson_a0a83c.htm
 | accessdate = 2007-09-26 }}  However, Janet Maslins review in the New York Times said that it came as "a welcome surprise that So I Married an Axe Murderer, which might have been nothing more than a by-the-numbers star vehicle, surrounds Mr. Myers with amusing cameos and gives him a chance to do more than just coast". {{cite news
 | last = Maslin
 | first = Janet
 | coauthors =
 | title = Yes, Boy Meets Girl, But This Time the Girl Wields Sharp Objects
 | work = New York Times
 | pages =
 | language =
 | publisher =
 | date = July 30, 1993
 | url = http://movies.nytimes.com/movie/review?_r=1&res=9F0CE7D61E39F933A05754C0A965958260&oref=login
 | accessdate = 2007-09-26 }}   In his review for the San Francisco Chronicle, Edward Guthmann called the film a "trifle, at best - but its so full of good spirits, and so rich with talented actors having a marvelous time, that its flaws tend to wash away". {{cite news
 | last = Guthmann
 | first = Edward
 | coauthors =
 | title = Its Waynes Adult World for Mike Myers in Axe
 | work = San Francisco Chronicle
 | pages =
 | language =
 | publisher =
 | date = July 30, 1993
 | url =
 | accessdate = 2007-09-26 }} 

==Home media==
A DVD of the film was first released in June 1999. The artwork for the DVD featured an image different than the original movie poster, which had Harriet holding an axe behind her back. A "Deluxe Edition" DVD and Blu-ray edition was released in June 2008. 

== References ==
 

== External links ==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 