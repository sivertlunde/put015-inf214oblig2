Cirque du Freak: The Vampire's Assistant
 
{{Infobox film
| name = Cirque du Freak:  The Vampires Assistant
| image = Vampires assistant.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster Paul Weitz
| producer = Lauren Shuler Donner Paul Weitz Ewan Leslie Andrew Miano
| screenplay = Paul Weitz Brian Helgeland
| based on =   Ray Stevenson Patrick Fugit Willem Dafoe Michael Cerveris Jane Krakowski Orlando Jones
| music = Stephen Trask
| cinematography = J. Michael Muro Leslie Jones The Donners Depth of Field Universal Pictures
| released =  
| runtime = 109 minutes  
| country = United States
| language = English
| budget = $40 million   
| gross = $39,232,113 
}}
Cirque du Freak: The Vampires Assistant is a 2009 American film adaptation of the Vampire Blood trilogy of the book series The Saga of Darren Shan by author Darren Shan.

==Plot==
Darren Shan and his best friend Steve Leonard visit a traveling freak show. Steve believes a performer, Larten Crepsley, is a vampire. Steve talks to Crepsley after the show and asks to become a vampire. Crepsley initially refuses but is talked into testing Steves blood, that he spits out telling Steve he has bad blood. Steve furiously swears revenge and leaves. Darren overhears everything because he has been hiding in a cupboard after stealing Crepsleys spider, Madam Octa.

When Crepsley realizes that Madam Octa has been stolen, Darren flees from the theater with Crepsley hot on his trail. Outside there is a limo waiting for him, which he is told to get into. Inside he meets Desmond Tiny and mad Vampaneze Murlough. They drop Darren off at his house where he sneaks back into his room.

At school Darren is looking at Madam Octa. Steve sees him and grabs the cage to look too. A school bell rings startling Steve and making him drop the cage and she escapes. After a chase by Steve and Darren Madam Octa slips into Steves shirt, bites him, and makes good her escape. Madam Octas poisonous bite leaves Steve in the hospital. Darren asks Crepsley for the antidote. In exchange for an antidote Crepsley requires Darren become a half-vampire and Crepsleys vampire assistant. Darren agrees, but after Crepsley gives Steve the antidote Darren reneges on his promise to become his assistant. Later, thinking of feeding off his sister, Darren agrees to leave his family and join Cirque Du Freak as Crepsleys assistant. Darren fakes his death and a funeral is held for him.

When Crepsley digs up Darrens grave they are attacked by Murlough who wants to make Darren a Vampaneze. After winning the fight Darren and Crepsley return to Cirque du Freak where Darren meets the snake boy, Evra Von, and Rebecca, the monkey girl. Meanwhile Steve, still attempting suicide, is stopped by Mr. Tiny who offers him a chance to become a Vampaneze, like Murlough. Mr. Tiny tells Steve that he is neither a vampire nor a Vampaneze, but someone with an interest in both. Tiny learns Darren has not fed on humans yet and sends Vampaneze after him, knowing that he will be weak. Darren escapes, so they kidnap Rebecca and have Evra tell Darren to "come home".

Mr. Tiny makes contact with Steve and entices him to join his Vampaneze by playing on his resentments and insecurities. Steve is turned into half-Vampaneze and then he and Murlough capture Darrens family. Darren arrives home to find his family missing and a flier for Cirque du Freak. Darren arrives at the theater, where Murlough and Steve are waiting for him. They fight, but Darren is weak from not feeding. 

Crepsley arrives to fight Murlough. Rebecca frees herself, while Mr. Tiny watches from the balcony. Rebecca offers Darren some of her blood. He eventually agrees and fights with Steve using his new powers. Crepsley stabs Murlough and with his dying words Murlough declares that the truce between the two clans is broken. Steve and Darren continue to fight, but Tiny stops them and turns Murlough into a Little Person. Tiny and Steve then leave. 

Darrens family are hypnotized to have forgotten what happened to them. Left by themselves, Rebecca and Darren then share a long heartfelt kiss before being interrupted by Mr. Crepsley. Crepsley gives Darren his own coffin, and Darren accepts his new life as a member of the Cirque du Freak.

==Cast==
  Larten Crepsley Darren Shan  Steve "Leopard" Leonard  Madame Truska Jessica Carlson as Rebecca (Monkey Girl) Ray Stevenson Murlough      Evra Von  Gavner Purl  Desmond Tiny (Mr. Destiny) Corma Limbs Gertha Teeth Rhamus Twobellies Hans Hands The Wolfman
* Wayne Douglas Morgan as Merman
* Blake Nelson Boyd as Mr. Afraid of the Ground Man Madame Truskas men
* Michael Dean Baker as Mr. Piercing
* Natasha Angelety as Cirque freak
* Nokomis Callender as Cirque freak
* Rose Lamarche as Cirque freak
* Stefanie Oxmann McGaha as Cirque freak
* J. J. Standing III as Cirque "Heavy"
* Ken Watanabe as Mr. Tall
 

==Production==
Cirque du Freak: The Vampires Assistant was shot between February 19-June 1, 2008 in New Orleans, and the villages of Folsom, Louisiana and Baton Rouge, Louisiana. Some of the characters required a great deal of prosthetics and makeup led by Steve Koch, Brian Sipe, and Mark Garbarino. Prosthetics did not quite add the height needed on certain shots for the character of Mr. Tall, played by Academy Award-nominated Ken Watanabe.  Although Watanabe is six feet tall, a body double was cast for certain shots. Trevon Flores, a local basketball player who stands 610" tall and weighs 210 pounds, was used for certain shots to show the abnormal height of Mr. Tall. Additionally, Watanabe utilized dialogue coaches Kathleen S. Dunn and Francie Brown in pre-production and production to further enhance his performance as the circus barker. The filmmakers took advantage of computer-generated imagery to portray other fantasy elements.
 John Marshall High School in Los Angeles was used to film a few parts of the movie. They also used Lusher Charter School in New Orleans to shoot a couple of scenes from the film.

The film began principal photography on February 8, 2008 in New Orleans and ended on June 3, 2008.  The film was distributed by Universal Studios. A portion of The Vampires Assistant was filmed on a set constructed within New Orleans City Park, approximately 1,000 feet off of the side of the road, along Harrison Avenue. In one of the manga additions of the saga, the director says that the character of Gavner Purl was a hint of the sequel he wanted to make.

===Music=== score to The Vampires Assistant was composed by Stephen Trask, marking his third feature film with director Paul Weitz. He recorded his score with an 86-piece ensemble of the Hollywood Studio Symphony at the Newman Scoring Stage at 20th Century Fox.    The movie also features "Something Is Not Right with Me" by Cold War Kids and Chelsea Dagger by The Fratellis and Red Right Hand by Nick Cave. The trailer features the songs Asleep From Day by The Chemical Brothers, Bliss by Syntax (band)|Syntax, and Superhero by Immediate Music.

==Release==
The film was originally set for release on January 15, 2010, but was moved ahead to October 23, 2009. 

===Critical reception===
Reception for the film has been mixed. On IMDB it has a rating of 6.0 out of 10, based on more than 32,000 votes. Rotten Tomatoes gives the film a rating of 38% from 134 critics, but also records an audience reaction of 50% liking the film.  According to Metacritic, the film holds a score of 43 out of 100, indicating "mixed or average reviews", based on 25 reviews. 

===Box office===
The film opened in 2,754 theaters in the United States and made over $14 million, reaching #7 in the charts. In other countries, it made more than $25 million, giving it a worldwide box office total of more than $39 million. On DVD, sales in the United States made more than $5.5 million.  Worldwide its chart placings included getting to #1 in Ukraine, #2 in Hungary, #2 in Russia, #2 in the United Arab Emirates, #3 in Mexico, #3 in Portugal, #4 in Egypt, #4 in Venezuela, #5 in Belgium, #5 in Peru, #5 in Singapore, #5 in the United Kingdom, #6 in Lebanon, #6 in the Philippines, #7 in Bulgaria, #7 in Japan, #8 in Chile, #8 in Colombia, #8 in the Netherlands, #9 in Austria, #9 in Romania, #11 in Germany, #11 in Malaysia, #11 in New Zealand. 

==Home media==
The film debuted on DVD and Blu-ray Disc in Canada, the United Kingdom and United States at the end of February 2010.   In Canada, at the end of its first week on sale and rent, it was #1 on the Rogers DVD bestselling chart and #2 on the Blockbuster Canada bestselling chart, and #6 on the rental charts of both. In the United States it was #2 on the Rentrak bestseller chart, and #6 on the Blockbuster, Home Media and IMDb rental charts. In the United Kingdom it reached #5 on the MyMovies bestsellers chart, and #6 on the Yahoo chart. 

==See also==
*Vampire film

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 