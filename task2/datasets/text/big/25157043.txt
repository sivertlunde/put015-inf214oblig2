Poomagal Oorvalam
{{Infobox film
| name = Poomagal Oorvalam
| image = "Poomagal_Oorvalam,_1999".jpg
| director = Rasu Madhuravan
| writer =
| art director = R. Jana Rambha Livingston Livingston
| producer = R. B. Choudary
| cinematography = M. Prasad
| music = Siva
| lyrics = Vairamuthu
| studio         = Super Good Films
| distributor =
| released = 30 April 1999
| runtime =  Tamil
| budget =
}}
 Tamil Kollywood|film Rambha and Livingston in the lead roles. The films score and soundtrack are composed by Siva. 

==Plot==
Saravanan (Prashanth), orphaned by his dead mother - a mental patient - is adopted by a childless couple (Manivannan and Radhika). Theirs is an inter-caste marriage. At college, he runs into Kavitha (Rambha), the granddaughter of caste-obsessed Sengodan (Rajan.P.Dev). The triangle is completed by Aavudayappan alias Armstrong (Livingston), a US-return who is smitten by Kavitha.

With a little unintentional photo-swapping by the marriage broker, the parents of Saravanan and Aavudayappan (the fathers have the same name) both think they have an alliance for their son with Sengodans family and show up at Kavithas house at the same time. Romance flowers between Saravanan and Kavitha, who assume they are going to wed, while Aavudayappan continues to dream of Kavitha.

A series of contrivances allows this comedy of errors to carry on till the engagement where announcement of the grooms name causes all sorts of confusions. Sengodan is now against the Saravanan-Kavitha union since Saravanans parents are of different castes. Things come to a dramatic conclusion when Sengodan realizes that Prasanth is indeed his own grandson, with a brief flashback relating the Nizhalgal Ravi - Anju marriage split due to her illness. Film ends with Prashanth and Rambha marry each other.

==Cast==
*Prashanth as Saravanan Rambha as Kavitha Livingston as Aavudayappan (Armstrong)
*Rajan P. Dev as Sengodan
*Raadhika Vivek as Sakthi
*Manivannan
*Nizhalgal Ravi
*Radha Ravi
*Vaiyapuri
*Ponvannan
*Sathyapriya Anju

==Production==
It was reported that the actor Prashanth was badly hurt on his face, while he took part in a stunt scene.  A sharp iron rod hit him on his face and he was hurt on his left cheek and immediately had three stitches on this wound from a local hospital. Further reports claimed that he was set to travel to London to partake in cosmetic surgery to avoid scarring.  The issue was later reported to be exaggerated, with Prashanth citing that he a minor injury and that the media blew the incident out of proportion. 

==Release==
The critic from The Hindu wrote: "Fun-laden situations and humour course along at a brisk pace in Supergood Films’ “Poomagal Oorvalam”. Debutant director Mathuravan, who has written the story, dialogue and screenplay, structures his narration in an enjoyable way. For Prasanth, the hero, it is another proof of his calibre, be it pouring out emotions, without overdoing it, carrying with subtlety the lighter moments or dancing comfortably".  The reviewer from Indolink wrote: "Watch it for the comedy, if not for anything else". 

==Soundtrack==
{{Infobox Album |  
| Name        = Poomagal Oorvalam
| Type        = soundtrack Siva
| Cover       = 
| Released    = 1999
| Recorded    = 1999 Feature film soundtrack |
| Length      = 
| Label       = Star Music Siva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Siva (music director)|Siva. The soundtrack, released in 1999, features 6 tracks.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Malare Oru Varthai || Hariharan (singer)|Hariharan, K.S.Chithra | Chithra || 05:01
|- 2 || Vaada Nannbane || Unni Krishnan || 
|- 3 || Naan Thai Yenndru || K.S.Chithra | Chithra || 04:02
|- 4 || Unni Krishnan || 04:08
|- 5 || Chinna Vennilave || Hariharan, Harini || 04:56
|- 6 || Kannai Parikkira || Unni Krishnan, K.S.Chithra | Chithra, Arun Mozhi || 04:58
|}

==References==
 

 

 
 
 
 