Point and Shoot (film)
Point and Shoot is a documentary film written and directed by two-time Academy Award nominee Marshall Curry.  It was produced by Marshall Curry, Elizabeth Martin and Matthew VanDyke.

==Synopsis==
Point and Shoot tells the story of Matthew VanDyke, a sheltered 26-year-old who left his Baltimore home and set off on a self-described “crash course in manhood.” While on a 35,000-mile motorcycle trip through Northern Africa and the Middle East, he struck up an unlikely friendship with a Libyan hippie. When revolution broke out in Libya, VanDyke decided to join his friend in the fight against dictator Muammar Gaddafi.  With a gun in one hand and a camera in the other, VanDyke joined and documented the war until he was captured by Gaddafi forces and held for six months in solitary confinement. 

==Release==
 The Orchard   and was released in theaters on October 31, 2014. 

== Awards ==
* 2014: Best Documentary Feature,  Tribeca Film Festival.   
* 2014: Special Jury Prize, Independent Film Festival of Boston  
* 2014: Special Jury Prize: Extraordinary Courage in Filmmaking - Matthew VanDyke, Little Rock Film Festival 
* 2014: Nomination: Best Documentary - Gotham Independent Film Awards 
* 2014: Nomination: Best Documentary - International Documentary Association 
* 2014: Nomination: Outstanding Achievement in Editing - Cinema Eye Honors 

==Reception==
The film has a 72% positive rating on Rotten Tomatoes.  In the Washington Post, Ann Hornaday described the film as an "absorbing, ingeniously crafted documentary" that gives the viewer a "street-level glimpse of the realities of war."  Stephen Holden wrote in the New York Times that the film suggests "the addictive rush of battlefield adventure is hard-wired into the male psyche."  Peter Bradshaw criticized the film in The Guardian, stating that the movie focusses on VanDyke himself and fails to explore the wider geopolitical context of the Libyan conflict.  Other critics described VanDyke as egocentric and questioned whether his experiences in the Middle East were driven by desire for fame and adrenaline, rather than geopolitical conviction.       

== References ==
 

== External Links ==
*  
*  
*  
* 
* 
* 

 
 
 
 
 
 
 
 
 