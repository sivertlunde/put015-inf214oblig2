Goopy Bagha Phire Elo
{{Infobox film
| name     = Goopy Bagha Phire Elo
| image          =Gupibaghaphireelo.jpg
| caption     = DVD cover art
| director       = Sandip Ray
| writer         = Satyajit Ray  Ajit Bandyopadhyay   Kamu Mukherjee
| producer       = Government of West Bengal
| distributor    = 
| cinematography = 
| editing        = Dulal Dutta
|  released   =  
| runtime        = 119 minutes
| country = India Bengali
| music = Satyajit Ray
}}
 Goopy Gyne Bagha Byne series. It was directed by Sandip Ray and written by his father Satyajit Ray. This film was released eleven years after its Hirak Rajar Deshe|predecessor. 

==Plot==
Goopy Gyne and Bagha Byne have ruled the kingdom of Shundi and are bored of royal luxuries. They want to get back to the days of adventure they had enjoyed all their lives, but age comes in the way. They leave the kingdom in search of new experiences. Finally, they reach Anandapur and win the kings heart with their musical abilities and powers. In the court room they meet Brahmananda Acharya, who invites Goopy and Bagha to come to Anandagarh fort. When they go to his place he offers them a job to steal three valuable stones, making use of their miraculous powers gifted by Bhuter Raja (King of Ghosts). In return he promises to make them 20 years younger. They steal two rare stones with a hope to become young again.

However, in their dream the King of Ghost appears and advises them to keep off injustice. They apologise to him and return the stones to the respective owners. Brahmananda Acharya had gained immense powers, as shown when he was not rendered motionless when they sung in front of the court. But, due to his greed of gaining rare and valuable stones, he was denied immortality. It was foretold that a 12 year boy, named Bikram, with divine powers, would defeat him. To prevent his death, Brahmananda Acharya had all the boys in Anandapur, who were 12 years old and were named Bikram, kidnapped by his soldiers. He hypnotized them, making them his servants. In the end, Goopy and Bagha find out that one boy named Kanu was previously named Bikram. He was to receive divine powers at the age of 12. He, along with Goopy and Bagha, goes towards Anandagarh fort. There, as Bikram entered the fort and came close to Brahmananda Acharya, the Acharya sank beneath the ground, signifying that he had been destroyed. His valuable stones also vanished. 

==Cast==

* Tapen Chatterjee as Gupi Gayen
* Rabi Ghosh as Bagha Bayen Ajit Bandyopadhyay
* Kamu Mukherjee
* Haradhan Bandopadhyay
* Pramod Ganguly
* Bhishma Guhathakurta
* Purab Chatterjee

==Awards==
 
===BFJA Awards  (1993)===
Best Indian Films

Best Art Direction - Ashok Bose

Best Choreography - Barun Raha

Best Editing - Dulal Dutta

Best Lyrics - Satyajit Ray

Best Music - Satyajit Ray

Best Playback Singer (Male) - Anup Ghoshal

Most Outstanding Work Of The Year - Tapen Chatterjee

==Sequel==

Sandip Ray want to make another sequel to this series. He had received many request to make the fourth Goopy - Bagha movie. Ray said to The Times of India about the plot of fourth film: "Making a Goopy Bagha movie without Tapen and Rabi is unthinkable. The only way I can do a fourth is by taking the story forward and introducing Goopy and Baghas sons," he said. The idea to weave a story around the next generation came from a line from the introductory song Mora dujonai rajar jamai in Hirak Rajar Deshe — "aar ache polapan, ek khan ek khan... (we have one child each)". 

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 