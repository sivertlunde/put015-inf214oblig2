Kamen Rider × Kamen Rider OOO & W Featuring Skull: Movie War Core
{{Infobox film
| italic title   = force
| name           = Kamen Rider × Kamen Rider OOO & W Featuring Skull: Movie War Core
| film name = {{Film name| kanji = 仮面ライダー×仮面ライダー オーズ&ダブル feat.スカル MOVIE大戦CORE
| romaji = Kamen Raidā × Kamen Raidā Ōzu ando Daburu fīcharingu Sukaru: Mūbī Taisen Koa}}
| image          = OOO & Double.jpg
| alt            = 
| caption        = Promotional poster for Kamen Rider × Kamen Rider OOO & W Featuring Skull: Movie War Core
| director       = Ryuta Tasaki
| producer       =  
| writer         =  
| narrator       =  
| starring       =  
| music          =  
| cinematography = 
| editing        =  Toei
| Toei Co. Ltd
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2010 film in the  , film was released in Japan on December 18, 2010.  The catchphrase for the movie is  .  Actor Taro Yamamoto guest stars in the film as the former partner of Sokichi Narumi, Koji Kikkawa reprising his role as the character.  Both Kikkawa and Maki Ohguro are tied to a theme song (and music video) for the film, recording the song "HEART∞BREAKER" under the band name  .    This movie is didnt fit the continuity while W take place after the final episode until Eiji used Tajadol Combo again. The character of Kamen Rider Birth made its debut in the films. 

Movie War Core opened at number 1 in Japanese box offices in its first weekend, unseating Harry Potter and the Deathly Hallows – Part 1 from its two week position. 

==Story== Akiko Narumi preparing for her wedding before considering canceling it when the Futo Kamen Riders are distracted by the   who has somehow acquired the Memory Dopant (Kamen Rider)#Gaia Memories|Memory. The Pteranodon Yummy uses these powers to reveal to Akiko how her father and Shotaros mentor, Sokichi Narumi, became Kamen Rider Skull with Shrouds aid.    along with his fight against the   and Eren Komori who transforms into the  . Both Dopants are an homage to the first two inhumanoids (  and  ) from the original Kamen Rider television series. 
 Kougami Foundation Eiji Hinos care. However, Nobunaga attempts to resume his conquest of Japan while targeting the descendants of those responsible for his down fall. But when forced into becoming a mindless Greeed, Eiji must stop Nobunaga before it is too late. 

In the final portion of the film entitled  , is convergence of the two films that brings the casts and characters of W and OOO to fight the evil energy-based giant Kamen Rider Core. 

===Skull: Message for Double===
Confronting a mysterious enemy, the male Pteranodon Yummy, Kamen Rider Double receives aid from Ryu Terui, who is forced to fight as himself due to his bride-to-be Akiko Narumi confiscating his Accel Memory and Accel Driver. No soon did the Pteranodon Yummy fly off then Akiko arrives and is infuriated that the Kamen Riders were too occupied to go attend her wedding to the point of breaking into tears and threatening to call her wedding to Ryu off. However, Akiko is more upset that her father is not able to attend the wedding and demands to know why he even became a Kamen Rider in the first place. This attracts the Pteranodon Yummy as uses the Memory Memory on her to get her the answer to her question.

Akiko finds herself in the past, witnessing her father Sokichi Narumi during a case regarding an opera singer named Melissa and a stalker called the "Spider-man." Heading to city hall for Melissas performance, with a young Shotaro Hidari among the audience, Sokichi meets Melissas seedy manager Kozo Yaguchi before Melissas stalker, the Spider Dopant, appears and attacks the police. Sokichi fights the criminal before a Gaia Memory dealer named Eren Komori appears and the two take their leave. Though his partner Seiichiro "Matsu" Matsui suggests to leave it to the cops, Sokichi returns to the detective agency to meet his benefactor Shroud, who reveals that the Museum has begun their master plan and tries to give him the Skull Memory which he turns down. He later meets with Matsu at the library after which Matsu shows him that Yaguchis previous female talents are among the missing persons.

Finding help in an informant named Stone, Sokichi breaks into the Yaguchi Talent Agency where he finds the missing girls being used as test subjects in Gaia Memory research. Forced to watch Stone get killed by Eren Komori when she becomes the Bat Dopant, Sokichi is unable to escape from her and the Spider Dopant. But Shroud arrives and tosses him the Lost Driver and the Skull Memory, allowing him to become the incomplete Kamen Rider Skull Crystal as he proceeds to defeat several Masquerade Dopants before getting himself into a pinch. Receiving a call from his young daughter Akiko, Sokichi promises her that he will attend her wedding when she grows up. This gives him the drive to fight as he pursues the Dopants. He only finds Yaguchi, who was only supplying the Museum with test subjects before he is killed by a Spider Bomb concealed in his wife. The next day, Sokichi confronts Matsu, knowing that he is the Spider Dopant after recognizing his handwriting at the scene of the crime. Found out, Matsu tries to fend himself from Sokichi before Melissa reveals that Matsu took the Gaia Memory to protect her from Yaguchi before it warped his mind. Furthermore, Matsu has placed a bomb in Melissa so she cannot touch, or be touched by, the one she loves, as the bomb will move towards the one she loves and kills him. The fact she does not love Matsu drives Matsu further into insanity as he assumes his Dopant form and blasts Sokichi and Futo with massive amounts of Spider Bombs. A series of explosions went off soon after in various places in Futo.

Having no choice, Sokichi attempts to chase his ex-partner down. However, the Bat Dopant gets in his way as Sokichi becomes the complete Kamen Rider Skull. He chases after the Spider Dopant while using the SkullGarry to deal with the Bat Dopant controlling an Oil Tanker before taking her down and pinning her to the tanker with the Skull Punisher. Though she begs Kamen Rider Skull for mercy, he leaves her to die in the vehicles resulting explosion. Catching up to his ex-partner, Kamen Rider Skull counts his own crimes and the Dopant must now follow. Using a Rider Kick, the Spider Memory breaks with Matsu dying in the Memorys side effects. Leaving a heart-broken Melissa without a word, Sokichi takes his leave.

In the present day, Akiko is horrified at the tragedy she saw as the Pteranodon Yummy now has the ideal memory of an intense battle needed to achieve his goal.

===OOO: Nobunagas Desire===
At the Kougami Foundation, Kousei Kougami talks of the most avaricious man in Japan, Oda Nobunaga, whose final resting place was found. With Doctor Maki overseeing the process, Kougami commissions the creation of a Cell Medal homunculus based on Nobunaga himself. After a part-time acting gig at a wedding, Eiji Hino gives away the money he and Hina Izumi earned, much to her dismay. While going on a paper route, Eiji confronts a Greeed-like monster in samurai armor attacking a man he deems a "murderer". Eiji becomes Kamen Rider OOO to fight the monster before he runs off and Eiji finds a barely conscious young man in the chase. Taking him to Cous Cossier, the man introduces himself as Nobunaga and reveals that he has no memory of who he is. Nobunaga soon visits a library and started learning at an astonishing pace. Taking him under his wing, Eiji is amazed at Nobunagas skill as he fixes a software bug that would take hours in a matter of seconds, subsequently getting a job in the Marshall Software company. Later that night, Ankh confronts the monster as he kills another "killer". The next day, Nobunaga meets Yoshino Akechi, a ballerina, as he gets full control of the company. Eventually, he goes after the Kougami Foundation as Kougami gives his project a present: the Birth Driver. After firing Eiji, the male Pteranodon Yummy arrives for more memories for his Memory Memory, with Nobunaga becoming Kamen Rider Birth to fight him to gain popularity, one of his steps of taking everything under the heaven for himself. However, as he and Hina watch Yoshino perform, Eiji is shocked to find Nobunaga to be the monster he confronted when he sprains the girls foot in attempt to kill her.

Heartbroken, Eiji becomes Kamen Rider OOO to fight him while Yoshino is confronted by a Greeed known as the Kyouryu Greeed who uses her shoe to create a female Pteranodon Yummy that meddles in the fight before being destroyed by Kamen Rider OOO Gatakiriba Combo. However, Nobunaga begins to suffer a total Cell Medal breakdown to the point of slowly losing his memories. He demands how Eiji can enjoy life without desire, only to find the answer he receives foolish, yet sensible. Nobunaga finds Yoshino at the hospital, using the last of his power to heal her leg. However, Doctor Maki arrives and adds three black Core Medals (Sasori, Ebi, and Kani) into Nobunaga which causes him to assume his complete Greeed-like form as he pleads Eiji to kill him. Having no choice, Kamen Rider OOO battles the Greeed and assumes Sagohzo Combo to end his friends misery. Nobunaga thanks Eiji for opening his eyes to what truly matters in life as he asks Eiji to give Yoshino his regards before he breaks down into Cell Medals. The three black Core Medals fly off with Eiji in pursuit.

===Movie War Core===
Having obtained the most intense Kamen Rider battles in the Memory Memory, the male Pteranodon Yummy is caught off guard when the black Core Medals appear and combine with the Gaia Memory into the giant Kamen Rider Core, much to Akikos horror as Eiji saves her. As Eiji assures Akiko that her father had his reasons for being a Kamen Rider, Kamen Rider Core begins to destroy the area as Kamen Riders OOO and W team up to fight the enemy. However, Kamen Rider Core proves too powerful and the two go underground to destroy the Gaia Memorys source of power to even the odds. Kamen Rider Double CycloneJokerXtreme and Kamen Rider OOO Latorartar Combo manage to outrun Kamen Rider Core and discover a giant crystal near the Earths core - the source of Kamen Rider Cores power. Back on the surface, Ryu persuades Akiko to allow him to become Kamen Rider Accel to protect her from the Pteranodon Yummy before Shintaro Goto arrives as Kamen Rider Birth and backs up Accel in destroying the Yummy. Back underground, after Ankh shows up, which disturbs Shotaro, the Kamen Riders manage to destroy the crystal which only angers Kamen Rider Core. The two Kamen Riders call him a mockery of what a Kamen Rider truly is. Ankh gives Kamen Rider OOO the Medals required to transform into Tajadol Combo, and the resulting wind from the transformation allows Kamen Rider W to transform into CycloneJokerGoldXtreme. The two Kamen Riders execute a combination Prominence Drop and Golden Xtreme Double Rider Kick that destroys Kamen Rider Core and shatters his core components. With a renewed sense of faith in the Kamen Riders, Akiko and the others hurry to her wedding while Eiji finds himself in Rio de Janeiro where Chiyoko Shiraishi just happens to be on vacation. At the wedding, Akiko discovers that Melissa is attending, and learns that Sokichi asked her to take his place at the wedding as he knew he can never see his own daughter again, while pulling his sleeve up revealing that the Spider Bomb didnt die with the Dopant. With that knowledge in mind, and her father with her in spirit, Akikos marriage to Ryu goes without any hitch. Some time after, Eiji and Chiyoko return to Japan to show Hina the items they got from their short time in Brazil.

==Kamen Rider Core== pyrokinetic powers such as firing a beam from his mouth strong enough to set a city on fire. Kamen Rider Core can also turn his lower half of his body into a motorcycle known as  .

== Cast ==
;W cast
*  :  
*  :  
*  ,  :   Uchusen, Vol. 130 
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :   
*  :  
*  :    
*  :   
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
;OOO cast
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :   
*  :   
*  :   
*  :  
*  :  
*  :  
*  :   
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
;Movie War Cast
*  :   

== Theme song ==
*  "HEART∞BREAKER" 
** Lyrics: Maki Ohguro
** Composition: Koji Kikkawa
** Arrangement: Hiroaki Sugawara
** Artist:  
*:Daikichi is a musical unit composed of Koji Kikkawa and Maki Ohguro using the first kanji of their surnames. This name is literally translated as "Great Luck". The unit was originally named  .  The single is scheduled for release on December 15, 2010.

The films soundtrack was released on December 15, 2010.  The movies music includes "Got to Keep it Real", "Nobodys Perfect", and a track titled "Finally", sung by series actress Hikaru Yamamoto as her character Melissa. "Finally" was written & arranged by Shoko Fujibayashi, and composed by Shuhei Naruse, used during the Kamen Rider Skull: Message for Double portion of the film where the character of Melissa performs at a concert. Other versions of "Finally" can be heard during Kamen Rider OOO: Nobunagas Desire.

==Notes==
{{reflist|group=Note|refs=
 Hamadas role is credited as   in the films credits. The name   is mentioned during the course of the film. 
}}

==References==
 

== External links ==
*    
*    

 
 
 

 
 
 
 
 