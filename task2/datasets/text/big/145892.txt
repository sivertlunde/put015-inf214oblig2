28 Days Later
 
{{Infobox film
| name           = 28 Days Later
| image          = 28 days later.jpg
| image_size     = 250px
| caption        = UK release poster
| director       = Danny Boyle Andrew Macdonald
| writer         = Alex Garland
| starring       = {{plainlist|
* Cillian Murphy
* Naomie Harris
* Christopher Eccleston
* Megan Burns
* Brendan Gleeson
}} John Murphy
| cinematography = Anthony Dod Mantle
| editing        = Chris Gill
| studio         = {{plainlist|
* DNA Films British Film Council
}}
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 113 minutes   
| country        = United Kingdom
| language       = English
| budget         = $8 million 
| gross          = $82.7 million 
}}
28 Days Later is a 2002 British post-apocalyptic horror film directed by Danny Boyle. The screenplay was written by Alex Garland. The film stars Cillian Murphy, Naomie Harris, Brendan Gleeson, Megan Burns, and Christopher Eccleston. The plot depicts the breakdown of society following the accidental release of a highly contagious virus and focuses upon the struggle of four survivors to cope with the destruction of the life they once knew.
 28 Days Later.

== Plot ==
In Cambridge, three activists break into a medical research laboratory with the intent of freeing captive chimpanzees. They are interrupted by a scientist who desperately warns that the chimps are infected with "Rage," a highly contagious rage-inducing virus that is spread through blood and saliva.  Ignoring the scientist, the activists release a chimpanzee, who attacks one of them and immediately infects her, leading her to infect everyone else present.

In  ), a bicycle courier, awakens from a coma in St Thomas Hospital and finds the hospital — and the city — deserted, with signs of catastrophe everywhere. Jim wanders into a church, where he is spotted and pursued by infected. He is then rescued by Selena (Naomie Harris) and Mark (Noah Huntley). At their hideout in the London Underground, the two explain how the virus spread uncontrollably among the populace, resulting in societal collapse. They also claim that the virus was being reported in Paris and New York City as well, suggesting that the infection has spread worldwide. 

The next morning, Selena and Mark accompany Jim to his parents house in Deptford, where he discovers that they have committed suicide. That night, the three are attacked by infected; Mark is bitten in the struggle, prompting Selena to quickly kill him; she explains that the virus overwhelms its victims in no more than twenty seconds. They leave and eventually spot some blinking Christmas lights from Balfron Tower, discovering two more survivors — cab driver Frank (Brendan Gleeson) and his teenage daughter Hannah (Megan Burns) — who allow them to take shelter. Frank informs them the next day that their supplies — particularly water — are dwindling, and plays them a pre-recorded radio broadcast from a military blockade near Manchester: it claims that the soldiers have "the answer to infection" and promise to protect any survivors that reach them. 

The group board Franks cab in search of the signal source, and throughout the trip they bond with one another in various situations. When the four reach the deserted blockade, Frank is infected when a drop of blood from a dead body falls into his eye. As he succumbs he is killed by arriving soldiers, who take the rest to a fortified mansion under the command of Major Henry West (Christopher Eccleston). During the groups stay, West reveals that his "answer to infection" entails waiting for the infected to starve to death, and luring female survivors into sexual slavery so that Wests platoon can repopulate the country. The group attempt to flee but Jim is captured by the soldiers, along with the dissenting Sergeant Farrell (Stuart McQuarrie). While imprisoned, Farrell speculates that the virus has not spread beyond Great Britain and that the country is being quarantined.

The next day, the girls are being prepared for gang-rape as two soldiers lead Jim and Farrell to execution. When his escorts quarrel after killing Farrell, Jim escapes and spots an aircraft flying overhead, proving Farrells theory to be correct. Jim lures West and another soldier to the blockade, where Jim kills the latter and leaves West stranded for arriving infected. He then runs back to the mansion and releases Mailer, an infected soldier West kept for observation, who proceeds to attack the rest of the platoon. Amidst the mayhem, Jim reunites with Selena and Hannah and run to Franks cab, where West is waiting and shoots Jim in the stomach. Hannah reverses the cab for Mailer, who grabs West through the rear window and kills him, and the trio finally flee the mansion. 

Another twenty-eight days later, Jim is shown to be recovering at a remote cottage. Downstairs, he finds Selena sewing large swaths of fabric when Hannah appears. The three rush outside and unfurl a huge cloth banner, adding the final letter to the word "HELLO" laid out on the meadow. A lone jet flies over the landscape, and some infected are shown dying of starvation. The film ends with the jet flying over the three survivors, and the pilot calling in a rescue helicopter.

== Alternative endings ==
The DVD extras include three alternative endings, all of which conclude with Jim dying except for the radical ending. Two were filmed, while the third, a more radical departure, was presented only in storyboards. On 25 July 2003, cinemas started showing the alternative ending after the films credits.   

=== Jim dies at the hospital ===
In this ending, after Jim is shot, Selena and Hannah still rush him to the deserted hospital, but the scene is extended. Selena, with Hannahs assistance, attempts to perform life-saving procedures but cannot revive Jim. Selena is heartbroken, and Hannah, distraught, looks to her for guidance. Selena tells Hannah that they will go on; they pick up their guns and walk away from Jims lifeless body. Selena and Hannah, still dressed in ballgowns and fully armed, walk through the operating room doors, which gradually stop swinging.

On the DVD commentary, Boyle and Garland explain that this was the original ending of the films first cut, which was tested with preview audiences. It was rejected for seeming too bleak; the final exit from the hospital was intended to imply Selena and Hannahs survival, whereas test audiences felt that the women were marching off to certain death. Boyle and Garland express a preference for this alternative ending, calling it the "true ending." They comment that this ending brought Jim full circle, as he starts and finishes the story in bed in a deserted hospital. This ending was added in the theatrical release of the film beginning on 25 July 2003, placed after the credits and prefaced with the words "what if..." 

==== "Hospital Dream" ====
The "Hospital Dream" ending is an extended version of the theatrical alternative ending where Jim dies at the hospital. It is revealed by the director during the optional commentary that this was the full version of the original ending. Jim dreams while unconscious and remembers the final moments on his bicycle before the crash. The footage cuts back and forth between the scene with Selena and Hannah trying to save his life and the dream sequence. As he gets hit by a car in his flashback, he simultaneously dies on the operating table.

==== Rescue coda without Jim ====
This ending, for which only a rough edit was completed, is an alternative version of the potential rescue sequence shown at the very end of the released film. Here, the scenes are identical, except that this ending was intended to be placed after the first alternative ending where Jim dies, so he is absent. When Selena is sewing one of the banner letters in the cottage, she is seen facetiously talking to a chicken instead of Jim. Only Selena and Hannah are seen waving to the jet flying overhead in the final shots.

=== "Radical Alternative Ending" ===
The "Radical Alternative Ending", rather than a bare ending, is a radically different development of the movie from the midpoint; it was not filmed and is presented on the DVD as a series of illustrated storyboards with voiceovers by Boyle and Garland. When Frank is infected at the military blockade near Manchester, the soldiers do not enter the story. Instead, Jim, Selena and Hannah are somehow able to restrain Frank, hoping they will find a cure for the virus nearby as suggested in the radio broadcast. They soon discover that the blockade had protected a large medical research complex, the same one featured in the first scene of the film where the virus was developed. Inside, the party is relieved to find a scientist barricaded inside a room with food and water. He will not open the door because he fears they will take his food, although he does admit that the "answer to infection is here". Unfortunately, he refuses to talk further because he does not want to make an emotional attachment to people who will soon be dead. After hours of failed attempts to break through the door or coax the man out, Jim eventually brings Hannah to the door and explains Franks situation. 

The scientist reluctantly tells them that Frank can only be cured with a complete blood transfusion and supplies them with the equipment. After learning that he is the only match with Franks blood type, Jim sacrifices himself so that Frank can survive with his daughter. Just as his journey began, Jim is left alone in the abandoned medical facility and Selena, Hannah and Frank move into the room with the scientist, as a horde of the infected breach the complex. The computer monitors show death and destruction come to life around a thrashing, infected Jim, who is strapped to the same table as the chimp had been in the opening scene. Garland and Boyle explain that they conceived this ending to see what the film would be like if they did not expand the focus beyond the four survivors. They decided against it because the idea of a total blood replacement as a cure was not credible. Boyle said in the DVD commentary that it "didnt make much sense", since the film had already established that one drop of blood can infect a person. "What would we do? Drain him of blood and scrub his veins with bleach?"

== Cast ==
 
* Cillian Murphy as Jim
* Naomie Harris as Selena
* Brendan Gleeson as Frank
* Christopher Eccleston as Major Henry West
* Megan Burns as Hannah
* Noah Huntley as Mark
* Stuart McQuarrie as Sergeant Farrell
* Ricci Harnett as Corporal Mitchell
* Leo Bill as Private Jones
* Luke Mably as Private Clifton
* Junior Laniyan as Private Bell
* Ray Panthaki as Private Bedford
* Sanjay Rambaruth as Private Davis
* Marvin Campbell as Private Mailer David Schneider as Scientist

On the DVD commentary, Boyle explains that, with the aim of preserving the suspension of disbelief, relatively unknown actors were cast in the film. Cillian Murphy had starred primarily in small independent films, while Naomie Harris had acted on British television as a child, and Megan Burns had only one previous film credit. However, Christopher Eccleston and Brendan Gleeson were well-known character actors.

== Production == Canon XL1 digital video camera.  DV cameras are much smaller and more manoeuvrable than traditional film cameras, which would have been impractical on such brief shoots. The scenes of the M1 motorway devoid of traffic were also filmed within very limited time periods. A mobile police roadblock slowed traffic sufficiently, to leave a long section of carriageway empty while the scene was filmed. The section depicted in the film was filmed at Milton Keynes, nowhere near Manchester.  For the London scene where Jim walks by the overturned double-decker bus, the film crew placed the bus on its side and removed it when the shot was finished, all within 20 minutes.  Much of the filming took place prior to the 11 September attacks and in the audio commentary, Boyle notes the parallel between the "missing persons" flyers seen at the beginning of the film and similar flyers posted in New York City in the wake of the attacks. Boyle adds that his crew probably would not have been granted permission to close off Whitehall for filming after the terrorist attacks in New York. A clapperboard seen in one of the DVD extra features shows filming was still taking place on 8 October 2001.
 Trafalgar Park Ennerdale in Lake District National Park.
 post apocalypse Dead trilogy. John Wyndhams The Day of the Triffids as Garlands original inspiration for the story.   

==Reception==
28 Days Later was a considerable success at the box office and became highly profitable on a budget of about £5 million. In the UK, it took in £6.1 million, while in the US it became a surprise hit, taking over $45 million despite a limited release at fewer than 1,500 screens across the country. The film garnered around $82.7 million worldwide.

Critical views of the film were very positive. Based on 215 reviews collected by the film review aggregator Rotten Tomatoes, 87% of critics gave 28 Days Later a positive review.  On Metacritic, the film received a rating of 73 (out of 100) based on 39 reviews. 
 zombie movie of all time. {{cite web |title=Stylus Magazines Top 10 Zombie Films of All Time |url=
http://www.stylusmagazine.com/articles/movie_review/stylus-magazines-top-10-zombie-films-of-all-time.htm |work=StylusMagazine.com |accessdate=18 July 2012}}  The film also ranked at number 456 in Empire (film magazine)|Empire s 2008 list of the 500 greatest movies of all time.  Bloody Disgusting ranked the film seventh in their list of the Top 20 Horror Films of the Decade, with the article saying "Zombie movie? Political allegory? Humanist drama? 28 Days Later is all of those things and more – a genuine work of art by a director at the top of his game. Whats so amazing about the film is the way it so expertly balances scenes of white-knuckled, hell-for-leather horror with moments of intimate beauty."   

=== Accolades ===
* Best Horror Film (U.S. Academy of Science Fiction, Fantasy & Horror Films&nbsp;— Saturn Award) 
* Best British Film (Empire Award) 
* Danny Boyle (Grand Prize of European Fantasy Film in Silver) 
* Best Director — Danny Boyle (International Fantasy Film Award)  Narcisse Award) 
* Best Breakthrough Performance — Naomie Harris (Black Reel) 
* Best Cinematographer — Anthony Dod Mantle (European Film Award) 

== Music ==
 
{{listen
|filename=In The House - In A Heartbeat.ogg
|title=John Murphy – "In The House – In A Heartbeat"
|description=Music from the 2002 film 28 Days Later.}}
 John Murphy Blue States.

A heavily edited version of the song "East Hastings" by the post-rock band Godspeed You! Black Emperor appears in the film, but the track is excluded from the soundtrack, because Boyle could only obtain the rights to use it in the film.   

28 Days Later: The Soundtrack Album was released on 17 June 2003. A modified version of the soundtrack "In The House – In A Heartbeat" was used as the character Big Daddys theme in the 2010 film Kick-Ass (film)|Kick-Ass. The same song was played in the latest advertisement campaign of Louis Vuitton,  LInvitation au Voyage. 

==Legacy==
=== Sequels === Andrew Macdonald. The plot revolves around the arrival of American troops about seven months after the incidents in the original film, attempting to revitalise a nearly desolate Britain. The cast for this sequel includes Robert Carlyle, Rose Byrne, Jeremy Renner, Imogen Poots, Harold Perrineau, Catherine McCormack, Mackintosh Muggleton, and Idris Elba.
 28 Months Later. 

=== Comic books ===
 , written by Steve Niles.
 28 Days Later, a comic sequel also linking Days and Weeks and produced by Fox Atomic (until its demise) and Boom! Studios, began production in 2009. The series focuses on Selena and answers questions about her in the film and her sequel whereabouts. 

== References ==
 

== External links ==
 
 
*  
*  
*  
*   at Facebook
*  
*  
*  
*  
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 