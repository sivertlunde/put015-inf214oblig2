Hitler's Children (2011 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Hitlers Children
| image          =  
| caption        = Hitlers children documentary 2011.
| director       =  Chanoch Zeevi 
| producer = Chanoch Zeevi
| studio       = Maya Productions Saxonia Entertainment
| writer         = Chanoch Zeevi
| starring       = Bettina Goering  Katrin Himmler Monika Goeth Rainer Hoess Eldad Beck Niklas Frank
| music          = Ophir Leibovitch 
| cinematography = Yoram Millo
| editing        = Arik Leibovitz
| distributor    = Film Movement (USA) (theatrical)
| released       =  
| runtime        = 59 minutes
| country        = Israel Germany
| language       = English
| budget         = 
}}
Hitlers Children is an Israeli-German 2012 documentary film directed by Chanoch Zeevi that portrays the struggle of family members of  Adolf Hitler|Hitler’s inner circle with the burden of a family legacy, and surname, identified with the horrors of the Holocaust. The descendants of the most senior Nazis describe the conflicted feelings of guilt and responsibility they carry with them in their daily lives.

==Synopsis==
Hitler’s Children is a film about the descendants of some of the most powerful figures in the Nazi regime, such as Heinrich Himmler, Hans Frank, Hermann Göring, and Rudolf Höss, who have inherited a legacy that permanently associates them with one of the greatest crimes in history. For more than 60 years they lived in the shadows trying to rebuild their lives without the constant reminders of what their fathers and grandfathers once did.
 descendants at all. The film also explores how they relate to the other side, the victims of their fathers crimes, for whom the terror of the Holocaust is still much alive.

==Release== Channel 2 Holocaust Memorial Day, and was first shown internationally in November 2011 at the International Documentary Film Festival Amsterdam.  Subsequently the documentary has been shown at several documentary film festivals and has also been shown by a number of TV broadcasters around the world, such as BBC2,  and the Swedish Kunskapskanalen. 

The film reached the finals in the category of Best TV Programmes Broadcast in Europe in the Prix Europa 2012 competition. 

==Distribution==
The broadcasting rights have been sold to many countries, including Australia, Belgium, Canada, Denmark, Germany, Holland, Poland, Russia, Spain, Sweden, Switzerland, and the UK. The film is available on Netflix in the United States.  

==Critical response==
The documentary received critical acclaim by several UK newspapers after being broadcast on BBC2 on 23 May 2012.  

The film won the 2012 Boston Jewish Film Festival Audience Award for Best Documentary film.

==Production notes==
The production of the documentary was supported by "The New Foundation for Cinema and Television" in Israel, “Reshet” - Channel 2 ; Israel, WDR, MDR, and SWR from Germany, TSR from Switzerand and DR from Denmark.

Chanoch Zeevi, the director, said in an interview that a personal meeting with Hitlers former secretary, Traudl Junge, in her flat in Munich in 1999, was the trigger that led to the production of the film. “When I sat in front of her I suddenly understood that the need to try and understand the evil that led to the horrors of the Holocaust was an integral part of the narrative. I understood that the dialogue with the other side can teach us many new things on the fertile ground on which the hatred grew and in parallel convey a message of hope for the future”. He further said: “My intention is that the film will successfully touch many varied audiences and by doing that it may restart the discussion on the story of the holocaust of European Jewry in an intelligent manner and from a new point of view, and along the way, provide an indisputable answer to Holocaust deniers." 

==See also==
*Inheritance (2006 film)|Inheritance (2006 film)

==References==

 

==External links==
*  
*  

 
 
 
 
 