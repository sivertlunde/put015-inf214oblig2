Altered (film)
 
{{Infobox Film
| name           = Altered
| image          = altered_dvd_cover.jpg
| caption        = DVD Cover Eduardo Sánchez Gregg Hale Eduardo Sánchez  (story)  Adam Kaufman Michael C. Williams Catherine Mangan James Gammon
| music          = Tony Cora Exiquio Talavera
| cinematography = Steve Yedlin
| editing        = Michael Cronin
| distributor    = Rogue Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 Eduardo Sánchez, co-director of the box office success, The Blair Witch Project, and written by Jamie Nash.

The plot is an inversion of the standard alien abduction formula, as four men abduct a lone alien, planning to wreak revenge on the invading species. In its early stages, the film was entitled Probed, and was intended as a comic homage to work of Sam Raimi and Troma Entertainment. 

==Plot synopsis==
Altered is the story of four men who seek revenge on aliens that abducted them and murdered their friend many years ago. As is explained via dialogue throughout the film, fifteen years before the events shown in the film, a group of five fifteen-year-old friends living in a remote American town were captured and experimented on by aliens while on a hunting trip. Only four of the friends returned alive. The main character (Wyatt) has since distanced himself from his childhood friends and is shown to have decided to live with the past, albeit in apparent constant paranoia. Two of the remaining three characters however have been obsessed by revenge and have persuaded the remaining, somewhat leadable, character that this is the correct course of action to take. The story opens with the tracking and subsequent capture of a lone alien - the consequences of which Wyatt and the three friends soon become deeply involved in.

==Cast==
* Paul McCarthy-Boyington - Cody
* Brad William Henke - Duke Michael C. Williams - Otis Adam Kaufman - Wyatt
* Catherine Mangan - Hope
* James Gammon - Sheriff Tom Herderson
* Joe Unger - Mr. Towne

==Reception==
Critical reception for Altered was mixed; Cinema Crazed gave the film a positive review, praising the "good old fashioned special effects mixed with wicked gore",  while Reel Film was more ambivalent, suggesting that the film was "a little too talky for its own good,   practically a horror masterpiece when compared to some of its straight-to-video brethren."  Roger Moore at the Orlando Sentinel was underwhelmed, reflecting that "too many of the arguments are whispered, too many settings are missing the horrific atmosphere the movie requires,"  and Scott Weinberg at DVD Talk described the film as "kind of a mixed bag: half chintzy and raw, half interesting and semi-creepy." 

==Associated media==
Kaufman starred in Taken (TV miniseries)|Taken, a 2002 television miniseries that also features alien abductions and alien implants that are used as tracking devices. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 