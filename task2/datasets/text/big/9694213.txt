Bernardine (film)
{{Infobox film
| name           = Bernardine
| image	     = Bernardine FilmPoster.jpeg
| caption        =
| director       = Henry Levin
| producer       = Samuel G. Engel Mary Chase Theodore Reeves Terry Moore Dick Sargent
| music          = Lionel Newman Johnny Mercer
| cinematography = Paul Vogel
| editing        = David Bretherton
| distributor    = 20th Century Fox
| released       =   
| runtime        = 95 min.
| country        = United States
| language       = English
| budget         = $1.23 million 
| gross          = $3.75 million (US rentals)  
}}
 Terry Moore, 1952 play title song, with words and music by Johnny Mercer, became a hit record for Boone.

==Plot==
A singing teen pairs his military-officer brother with a dream-girl telephone operator new in town.

==Cast==
* Pat Boone as Arthur Beau Beaumont  Terry Moore as Jean Cantrick 
* Janet Gaynor as Mrs. Ruth Wilson 
* Dean Jagger as J. Fullerton Weldy 
* Dick Sargent as Sanford Wilson (as Richard Sargent) 
* James Drury as Lt. Langley Beaumont  Ronnie Burns as Griner 
* Walter Abel as Mr. Beaumont 
* Natalie Schafer as Mrs. Madge Beaumont 
* Isabel Jewell as Mrs. McDuff
* Edith Angold as Hilda  
* Val Benedict as Friedelhauser  
* Emestine Wade as Cleo   Russ Conway as Mr. Mason  
* Thomas Pittman as Olson  
* Jack Costanzo as Himself - Orchestra leader

==References==
 
*  
*Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History (The Scarecrow Filmmakers Series). Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-8108-4244-1.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 


 