Code Unknown
{{Infobox film
| name       = Code Unknown: Incomplete Tale of Several Journeys
| image      = Code unknown poster.jpeg
| director   = Michael Haneke
| producer   = Marin Karmitz
| writer     = Michael Haneke
| starring   = Juliette Binoche Thierry Neuvic Alexandre Hamidi
| cinematography = Jürgen Jürges
| editing    = Karin Martusch Nadine Muse Andreas Prochaska
| music      = Giba Gonçalves
| distributor=MK2 Editions Artificial Eye Leisure Time Features
| released   =  
| runtime    = 117 minutes
| country    = France Germany Romania French Maninka Romanian German German English Arabic French Sign Language
}}
Code Unknown: Incomplete Tales of Several Journeys (Code inconnu: Récit incomplet de divers voyages) is a 2000 film directed by Michael Haneke. Most of the story occurs in Paris, France, where the fates of several characters intersect and connect. Cinematically, the film is composed of unedited long takes filmed in real time, cut only when the perspective within a scene changes from one characters to another in mid-action.

==Plot==
The film features several different storylines, all of which intersect periodically throughout the film. The films opening scene features a brief encounter with four of the main characters: Anne Laurent (Juliette Binoche) is an actress working in Paris, and she walks briefly with her boyfriends younger brother Jean. After they part, Jean throws a piece of garbage at Maria, a homeless woman sitting on the side of the road. Amadou, the child of Malian immigrants, witnesses this and confronts Jean. The two fight, and eventually Amadou and Maria are both taken to a police station for questioning. Amadou is released presumably shortly after, though we learn that he was held, beaten and shamed, but Maria is deported to her native Romania and she reconnects with her family there.

==Cast==
* Juliette Binoche - Anne Laurent
* Thierry Neuvic - Georges
* Josef Bierbichler - The Farmer (as Sepp Bierbichler)
* Alexandre Hamidi - Jean
* Maimouna Hélène Diarra - Aminate
* Ona Lu Yenke - Amadou
* Djibril Kouyaté - The Father
* Luminița Gheorghiu - Maria
* Crenguta Hariton - Irina (as Crenguta Hariton Stoica)

==Awards and nominations==
Code Unknown received the Golden Palm award nomination at the 2000 Cannes Film Festival.    Cinematographer Jürges was nominated for the "Golden Frog" at the Camerimage awards.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 