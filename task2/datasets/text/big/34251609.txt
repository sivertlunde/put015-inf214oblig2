Chinarayudu
{{Infobox film
| name           = Chinarayudu
| image          = Dvd_poster_of_Chinarayudu.jpg
| image_size     =
| caption        =
| director       = B. Gopal
| producer       = P. R. Prasad
| writer         = R. Selvaraj  (narrative|story)  Paruchuri Brothers  (dialogues) 
| screenplay     = Venkatesh Vijayashanti
| music          = Ilayaraja
| cinematography = V.S.R. Swamy
| editing        = Kotagiri Venkateswara Rao
| studio         = Sri Datta Sai Films
| distributor    =
| released       =  
| runtime        = 141 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood film Venkatesh and Vijayashanti played the lead roles and music composed by Ilayaraja.   The film was a remake of Tamil film Chinna Gounder.  The film was flop at the box office.  

==Plot==
Chinnarayudu (Daggubati Venkatesh|Venkatesh) the arbitrator and most respected person in a village and sees through that all the people lives happily. Whenever injustice comes to the fore, he appears to resolve it in an authentic manner, he lives with his mother Durgamma (Nirmalamma). Chinnarayudus brother-in-law Pasupathi (Mohan Raj) is always opponent and anti to him. Gowri (Vijayashanti) a girl in the village lives by rearing the goats have funny fights with Chinnarayudu & Durgamma and they both silently love each other. Gowri has debt of amount to Pasupathi for studying her sister Ganga (Haritha), she not able to repay the money so issue goes to Chinnarayudus judement, then situation arises Gowri has to collect money from villagers for repaying her debt and ultimately landing up in getting married to the Chinnarayudu.

After some time, in an auction Chinnarayudu loss to Pasupathi, to bear a grudge Pasupathi mixes poison in a fish water lake and keeps the blame on Chinnarayudu. Prakash Rao (Vinod) a person who brought up and studied by Chinnarayudu, but he feels that he insulted him many time, for that ire he prepares to give evidence against Chinnarayudu. Gowri comes to know that, she requests him a lot at last she doesnt have any other alternative to stop him expect to axe him. Gowri has send to remand and the case under judgement.

Meanwhile, Ganga becomes pregnant, Chinnarayudu takes blame on him for that Durgamma sends him out of the house. In jail Gowri tries make suicide Chinnarayudu meets her in hospital and reveals the truth "the person who made Ganga pregnant is none other than Prakash Rao, no one will believe this, thats why he took the blame" then Gowri realizes. At the same time Chinnarayudu comes to know that Prakash Rao does not died while Gowri axed him, after that Pasupathi killed him. Chinnarayudu proves this truth, makes Gowri to release, Ganga gives birth to baby, she also tells truth to everyone and dies. Chinnarayudu catches Pasupathi and gets him to panchayat, he also realizes his mistake and surrenders himself to Police.

==Cast==
  Venkatesh as Chinnarayudu
* Vijayashanti as Gowri
* Kota Srinivasa Rao as Chakali Appanna
* Babu Mohan as Appannas assistant
* P. L. Narayana as Lawyer
* Mohan Raj as Pasupathi
* Vinod as Prakash Rao
* Narra Venkateswara Rao as Doctor
* Jeet Mohan Mitra as Lawyer Vijayalalitha as Kameswari
* Rajitha as Chinnarayudus niece
* Haritha as Ganga
* Nirmalamma as Durgamma
 

==Soundtrack==
{{Infobox album
| Name       = Chinarayudu
| Tagline    =
| Type       = soundtrack
| Artist     = Ilayaraja
| Cover      =
| Released   = 1992
| Recorded   =
| Genre      = Soundtrack
| Length     = 30:27
| Label      = Lahari Music 
| Producer   = Ilaiyaraaja
| Reviews    =
| Last album =Chanti (1992 film)|Chanti   (1992)
| This album =Chinarayudu   (1992)
| Next album =Aswamedham (1992 film)|Aswamedham   (1992)
}}

Music composed by Ilayaraja. Lyrics are written by Bhuvanachandra  Music released on Lahari Music Company.
{{Track listing
| extra_column = Singer(s)
| total_length = 30:27
| title1       = Bullipitta SP Balu, S. Janaki
| length1      = 4:52
| title2       = Swathi Mutyamala  Chitra
| length2      = 5:02
| title3       = Chitti Chitti  
| extra3       = SP Balu
| length3      = 5:06
| title4       = Kanti Chupu 
| extra4       = SP Balu
| length4      = 4:22
| title5       = Cheppalanundi Sundari
| extra5       = SP Balu
| length5      = 0:47
| title6       = Nindu Aakasamantha 
| extra6       = Chitra
| length6      = 1:14
| title7       = Bullipitta-II
| extra7       = SP Balu, Chitra
| length7      = 4:21
| title8       = Nindu Aakasamantha-II
| extra8       = SP Balu
| length8      = 4:35
}}

== References ==
 

==External links==
*  

 
 
 
 
 
 


 