Blaze (film)
{{Infobox film
| name           = Blaze
| image          = Blaze_imp.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Ron Shelton
| producer       = Gil Friesen Dale Pollock
| screenplay     = Ron Shelton
| based on       = Blaze Starr: My Life as Told to Huey Perry by Blaze Starr Huey Perry
| starring       = Paul Newman Lolita Davidovich
| music          = Bennie Wallace
| cinematography = Haskell Wexler Robert Leighton Michael King Silver Screen Partners IV A&M Records|A&M Films Buena Vista Pictures
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $18 million
| gross          = $19,131,246
}} appearing in a cameo.

==Plot==
The movie tells the highly fictionalized story of the latter years of Earl Long, a flamboyant Governor of Louisiana, brother of assassinated governor and U.S. Senator Huey P. Long and uncle of longtime U.S. Senator Russell Long. According to the novel and film, Earl Long allegedly fell in love with a young stripper named Blaze Starr.

==Cast==
* Paul Newman as Governor Earl Long
* Lolita Davidovich as Blaze Starr
* Jerry Hardin as Thibodeaux
* Gailard Sartain as LaGrange
* Jeffrey DeMunn as Eldon Tuck
* Richard Jenkins as Picayune Brandon Smith as Arvin Deeter
* Robert Wuhl as Red Snyder James Harper Willie Rainach Alexandria Daily Town Talk reporter No. 1

==Reception==
The movie received mixed reviews from critics.  

===Box office===
Blaze debuted at No. 9. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 