House at the End of the Street
{{Infobox film
| name           = House at the End of the Street
| image          = House at The End of the Street.jpg
| caption        = Theatrical release poster
| director       = Mark Tonderai
| producer       = Aaron Ryder Peter Block Ryan Kavanaugh 
| writer         = David Loucka Jonathan Mostow
| starring       = Jennifer Lawrence Max Thieriot Gil Bellows Elisabeth Shue Eva Link Nolan Gerard Funk Allie MacDonald
| music          = Theo Green
| cinematography = Miroslaw Baszak
| editing        = Steve Mirkovich Karen Porter
| studio         = FilmNation Entertainment
| distributor    = Relativity Media Alliance Films (Canada)
| released       =  
| runtime        = 101 minutes 102 minutes (Unrated cut)
| country        = Canada United States
| language       = English
| budget         = $6.9 million   
| gross          = $44,103,982  
}} psychological Horror horror thriller thriller film directed by Mark Tonderai that stars Jennifer Lawrence, Max Thieriot, Gil Bellows, and Elisabeth Shue.

The films plot revolves around a teenage girl named Elissa who, along with her newly divorced mother Sarah, moves to a new neighborhood only to discover that the house at the end of the street was the site of a gruesome double murder committed by a girl named Carrie Anne who disappeared without a trace. Elissa then starts a relationship with Carrie Annes brother Ryan, who now lives in the same house.

Despite a negative response from critics, it was a commercial success, ranking No. 1 at the box office in its opening weekend.

==Plot==
A young psychotic girl murders her parents with a hammer in the middle of a stormy night.

Four years later, a newly divorced woman, medical doctor Sarah Cassidy (Elisabeth Shue), and her 17-year-old daughter Elissa (Jennifer Lawrence) move to a small upscale town. Their house is near the house where the massacred family lived. As told by the neighbors, four years prior a young girl named Carrie Anne Jacobson killed her parents, then fled into the forest and was never seen again, leaving her brother Ryan (Max Thieriot) as the sole survivor. Ryan now lives alone and is hated by his neighbors; Bill Weaver (Gil Bellows), a local police officer, appears to be Ryans only supporter.
 swing when Jordan Hayes).

Later, some unruly high school boys pick a fight with Ryan and he flees, Elissa drives to his house and subdues a fire the boys started. She finds tampons in the kitchen garbage and suspiciously explores the house until she finds the secret room and is attacked by Carrie Anne, who is revealed to actually be Peggy. Ryan restrains "Carrie Anne" while frantically screaming at Elissa to leave. Elissa finds blue contact lenses and Peggys wallet in the kitchen. It is revealed that Ryan has kidnapped the waitress and attempted to make her look like Carrie Anne. When Elissa tries to leave, Ryan knocks her out.

Elissa wakes to find herself tied up. Ryan reveals that Carrie Anne actually died during the swing accident. He says his parents punished him for it and implies that he was the one that killed them. He explains that he wants Elissa, but needs Carrie Anne and cannot have both. Officer Weaver goes to Ryans house to look for Elissa but Ryan stabs him to death. Elissa frees herself and tries to escape but Ryan subdues her and traps her in his car trunk with Peggys dead body. Sarah arrives and is also stabbed by Ryan. Elissa struggles out and ultimately shoots Ryan with Weavers gun.

Elissa and Sarah move out; Ryan is placed in a psychiatric ward. A flashback shows young Ryan about to blow out birthday candles. His mother calls him "Carrie Anne" and when Ryan protests that his name is Ryan, not Carrie Anne, she slaps him violently; it is revealed that his parents forced him to dress and act like Carrie Anne after she died and most likely abused Ryan when he refused to go along with their fantasy, thus setting Ryan on his troubled path.

==Cast==
* Jennifer Lawrence as Elissa Cassidy
* Max Thieriot as Ryan Jacobson
* Bobby Osborne as Young Ryan
* Elisabeth Shue as Sarah Cassidy
* Gil Bellows as Officer Bill Weaver
* Eva Link as Carrie Anne
* Nolan Gerard Funk as Tyler Reynolds
* Allie MacDonald as Jillian Jordan Hayes as Peggy Jones
* Krista Bridges as Mary Jacobson
* John Healy as John Jacobson
* Grace Tucker-Duguay as Carrie Anne Jacobson

==Production== Richard Kelly scripting but the film was put through development hell for 9 years until production was revived in 2010 with Mark Tonderai directing and Jonathan Mostow scripting instead.

Principal photography and filming mostly took place in Metcalfe, Ontario, Canada on August 2, 2010 until September 3, 2010. 

==Release==
The film was originally scheduled to be released in April 2012  but was moved to a September 2012 release. The film had its theatrical premiere in the USA on September 21, 2012 and was released in Canada on the same date. The film was not released theatrically in Sweden or Spain and was released direct-to-video on January 30, 2013 in Sweden and on August 28, 2013 in Spain.

==Novelization==
A tie-in novelization of the movie was released on August 12, 2012 to accompany the movie by Little, Brown and Company. 

==Reception==

===Box office===
The film debuted at No. 1 at the US box office on its opening Friday and Saturday nights.  In what was one of the tightest races in years for first place at the box office weekend, the film finished the weekend at No. 2 with $12.3 million, just less than a million behind End of Watch which included takings from Thursday night through Monday morning, where that movie finished at No. 1, with $13.1 million.  The film went on to gross over $44 million worldwide, from a budget of $6.9 million. 

===Critical reception===
The film received a CinemaScore of B, indicating it was received well by its target audience. The film was generally received negatively by critics and it holds a 10% positive rating on Rotten Tomatoes based on 58 reviews from critics with the critical consensus stating: "Poorly conceived, clumsily executed and almost completely bereft of scares, House at the End of the Street strands its talented star in a film as bland as its title."   Critics have still praised Jennifer Lawrence for her performance saying that she "does her best with a dull and derivative script in this by-the-numbers suburban shocker." Some of the other positive reviews have also praised the film for its plot twists and for being somewhat unpredictable. 

==Home Media==
House at the End of the Street was released on DVD and Blu-ray Disc on January 8, 2013.  

===Unrated Cut===
The unrated cut was also released on January 8, 2013. The extended edition increased the length of certain scenes in the final cut by a few seconds and the amount of violence, blood, and gore was increased by a small amount. The extended cut also included an additional twist in which Bill Weaver was actually a family friend of the Jacobsons and was aware of Carrie Annes fate and he also knew about Ryans abuse but did nothing to help him. On the day of Carrie Annes accident he supplied John and Mary Jacobson with drugs and actually could have prevented Carrie Annes death if he had not sold them the drugs as it prevented John and Mary from heeding the cries of Ryan, he was then disowned as a friend by John Jacobson.

==Accolades==
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipients and nominees !! Outcome
|-
| 2012 || Directors Guild of Canada  || Best Sound Editing – Feature Film || Mark Gingras, John D. Smith, Katrijn Halliday, Tom Bjelic, James Robb, Dale Lennon ||  
|- 2013 || The Hunger Games)  || Jennifer Lawrence ||  
|- 2013 || Best Scared-As-S**t Performance || Jennifer Lawrence ||   
|-
| 2013 || ASCAP Awards  || Film Award || Theo Green ||  
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 