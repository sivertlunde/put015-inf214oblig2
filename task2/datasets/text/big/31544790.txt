Kings and Desperate Men
{{Infobox Film
| name           = Kings and Desperate Men
| image          = Kings and Desperate Men.jpeg
| image_size     = 
| caption        = Theatrical release poster
| director       = Alexis Kanner
| producer       = Alexis Kanner
| writer         = Edmund Ward   Alexis Kanner
| narrator       = 
| starring       = Patrick McGoohan  Alexis Kanner  Margaret Trudeau  Andrea Marcovicci  Robin Spry Budd Knapp  Jean-Pierre Brown
| music          = Pierre F. Brault   Michel Robidoux
| cinematography = Alexis Kanner   Paul Van der Linden
| editing        = Alexis Kanner
| distributor    =  Magnum Entertainment
| released       = 1981 (Canada)   1983 (United States)   1984 (United Kingdom)
| runtime        = 118 min
| country        = Canada
| language       = English
| budget         = 1.2 million
| gross          = 
}} 1981 Canada|Canadian hostage drama film directed, co-written and produced by Alexis Kanner. The film stars Patrick McGoohan as radio talk show host John Kingsley, Margaret Trudeau as his wife Elizabeth, and Kanner with Andrea Marcovicci as terrorists. The story is set within one day during Christmas Eve. The movie was made on a budget of 1.2 million and was filmed in Montreal. 

Film director Kanner later took legal action against the film producers of Die Hard in the late 1980s, alleging the producers stole the idea for Die Hard from his film Kings and Desperate Men.  Kanner lost his case.

==Plot==
On Christmas Eve, a radio talk show host, his wealthy wife, their mentally challenged son and a federal judge are taken hostage by a group of terrorists. The group demand a new trial on the air for a convicted comrade of theirs whom the group believes was wrongly convicted of manslaughter. The radio listeners are asked by the terrorists to act as the jury and to telephone in their verdicts to the radio station.

==Cast==
*Patrick McGoohan as John Kingsley
*Alexis Kanner as Lucas Miller
*Margaret Trudeau as Elizabeth Kingsley
*Budd Knapp  as Judge Stephen McManus
*Andrea Marcovicci as Barbara
*Frank Moore as Pete Herrera
*Robin Spry as Harry Gibson
*Jean-Pierre Brown as Christopher Kingsley
*Kate Nash as Mrs. McPhearson
*Neil Vipond as Henry Sutton
*Dave Patrick as Grant Gillespie
*Kevin Fenlon as Laz
*August Schellenberg as Stanley Aldini
*Frederic Smith as Bolton
*Peter MacNeill as George
*Marcel Beaulieu as a member of the Special Squad
*Andre Koudsey as a member of the Special Squad
*Bob Lepage as a member of the Special Squad
*Normand Roy as a member of the Special Squad
*Andrew Theodoses as a member of the Special Squad

==Release==
Kings and Desperate Men was shot on location in Montreal, Canada during December 1977. Film director Kanner apparently spent two years editing the film. The movie was eventually released in Canada on August 22, 1981 at the Montreal World Film Festival .  It was released in the USA on November 13, 1983  at the Chicago International Film Festival. It also premiered in the UK at the London Film Festival in 1984. Kings and Desperate Men was rated PG-13 in the United States. The movie was later issued on VHS in 1989.

==References==
 

==External links==
*  at the Internet Movie Database
*  at Rotten Tomatoes

 
 
 
 
 
 
 
 
 
 