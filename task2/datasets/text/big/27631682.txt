Nippulanti Manishi
{{Infobox film
| name           = Nippulanti Manishi
| image          = Nippulanti Manishi.jpg
| image_size     =
| caption        =
| director       = S. D. Lal
| producer       = Y. V. Rao
| writer         = Gollapudi Maruti Rao
| narrator       =
| starring       = N.T. Rama Rao Latha Sethupathi Kaikala Satyanarayana
| music          = Chellapilla Satyam
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1974
| runtime        =
| country        = India Telugu
| budget         =
}}

Nippulanti Manishi (  directed by S. D. Lal and produced by Y. V. Rao. It is remake of 1973 Hindi film Zanjeer (1973 film)|Zanjeer starring Amitabh Bachan. 

Snehame Naa Jeevitam Snehamera Sasvatham song about the real Friendship is one of the memorable songs voiced by S. P. Balasubrahmanyam. It is tuned similar to the Hindi song "Yari Hai Imaan Mera" by Chellapilla Satyam|Satyam.

==Soundtrack==
* Kattiki Sana Churakattiki Sana (Lyrics:  )
* Orabbi Orabbi (Lyrics: C. Narayana Reddy; Singer: S. P. Balasubrahmanyam and S. Janaki)
* Snehame Naa Jeevitam Snehamera Sasvatham (Lyrics: C. Narayana Reddy; Singer: S. P. Balasubrahmanyam; Cast: NTR and Satyanarayana)
* Welcome Swagatam Chesta Ninne Paravasam (Lyrics: C. Narayana Reddy; Singer: L. R. Eswari)
* Yedo Anukunnanu (Lyrics: C. Narayana Reddy; Singer: P. Susheela)

==Box office==
The film was a blockbuster. 

==References==
 

==External links==
* 
* 

 
 
 
 

 