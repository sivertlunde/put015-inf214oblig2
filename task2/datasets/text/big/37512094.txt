Six Degrees of Separation from Lilia Cuntapay
{{Infobox film
| name = Six Degrees of Separation from Lilia Cuntapay
| image = Six Degrees of Separation from Lilia Cuntapay poster.jpg
| image size =
| caption = Original poster
| director = Antoinette Jadaone 
| producer = Joenathann Alandy Ronald Arguelles Daphne Chiu
| writer = Antoinette Jadaone
| narrator =
| starring = Lilia Cuntapay
| music = Ayn Marie Dimaya
| cinematography = Ma. Solita Garcia
| editing = Glenn Ituriaga Leo Valencia
| studio = Cinema One, PostManila
| distributor = Cinema One
| released =  
| runtime = 140 minutes
| country = Philippines English Filipino Filipino
| budget =
| gross =
| preceded by =
| followed by =
}}
Six Degrees of Separation from Lilia Cuntapay is a 2011 Philippine mockumentary independent feature film starring Lilia Cuntapay which is written and directed by Antoinette Jadaone.  
 2011 Cinema One Originals Digital Film Festival, including a Best Actress award for Cuntapay.   

==Plot==
The story follows a veteran bit player through the days leading up to a fictionalized awards night where she is nominated as a Best Supporting Actress for the very first time in her 30-year career. 

==Cast==
*Lilia Cuntapay as Herself
*Geraldine Villamil
*Raymond Rinoza

==Reception==

===Critical response===
The film received mostly positive reviews from critics and Internet websites.

Karen of Audrey Magazine.com gave the film positive response saying “Six Degrees will have you laughing and feeling good throughout. Whether her portrayal in this mockumentary is just part of the show or not, Cuntapay comes off as very adorable, quirky, and imaginative.” 

Philbert Ortiz Dy of the Click in the City.com wrote, "It is a film that I will be recommending to people for years to come. Beyond the fact that it’s funny and smart and well made, I feel that it is representative of everything that I dream of for the new Filipino cinema." 

Jowana of Jowanabueser.com enjoyed the film and wrote, "films like this are an indication Philippine Cinema is far from dead. No. It is not dead because LILIA CUNTAPAY is rockin’ it like a mad ghost."  

Francis Joseph Cruz of ABS-CBN News positively reacted both to the film and Cuntapay and also written, "Jadaone’s film, rooted in that fantasy that someone who has persisted for so long like Lilia Cuntapay may actually cross-over to be pitted against established and talented actresses in a glittery awards ceremonies, is a heartfelt tribute to each and every person who dared to dream dreams as big (and probably far-fetched) as the ones dreamt by Lilia Cuntapay." 

Both the film and Cuntapay as a celebrity were big hits with the festival audience in Italy and South Korea. 

===Awards and nominations===
{| class="wikitable"
|-
! Year
! Association
! Category
! Nominee
! Result
! Ref(s)
|- 2011
| Cinema One|Cinema One Originals Digital Film Festival
| Best Actress
| Lilia Cuntapay
|  
|      
|-
| Best Supporting Actress
| Geraldine Villamil
|  
|    
|-
| Best Screenplay
|
|  
|    
|-
| Best Editing
|
|  
|    
|-
| Audience Choice Award
|
|  
|    
|-
| Special Jury Prize
|
|  
|    
|- 2012
| Gawad Urian 35th Gawad URIAN Awards
| Best Picture
|
|  
|      
|-
| Best Director
| Antoinette Jadaone
|  
|    
|-
| Best Actress
| Lilia Cuntapay
|  
|    
|-
| Best Screenplay
| Antoinette Jadaone
|  
|    
|-
| Best Production Design
| Rodrigo Ricio
|  
|    
|-
| Best Film Editing
| Leo Valencia Glenn Ituriaga
|  
|    
|-
| Udine Far East Film Festival
| Official Selection
|
|  
|    
|-
| Neuchâtel International Fantastic Film Festival
| Official Selection
|
|  
|    
|-
| Busan International Film Festival
| Official Selection A Window on Asian Cinema
|
|  
|    
|-
| Cinemalaya Philippine Independent Film Festival
| Exhibition
|
|  
|    
|}

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 