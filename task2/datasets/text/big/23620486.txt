Under the Mountain (film)
 
 
{{Infobox film
| name           = Under the Mountain
| image          = UTM_hi_2.jpg
| caption        = Theatrical release poster Jonathan King Jonathan King Susan Parker
| executive      = Chris Bailey Chris Hampson Trevor Haysom Jonathan King
| starring       = Sam Neill Oliver Driver Sophie McBride Tom Cameron
| cinematography = Richard Bluck
| music          = Victoria Kelly
| studio         = An Index Films and Liberty Films Production Chris Plummer
| distributor    = Redhead Films
| released       =  
| country        = New Zealand
| language       = English
| budget         = $5 Million
| gross          = $ 2,251,215
}} 1979 novel by New Zealand author Maurice Gee.

==Plot==
Teenage twins Rachel and Theo travel to Auckland to stay with relatives following the sudden death of their mother. Where there was once a psychic bond between them, now there is a rift as Theo, particularly, refuses to confront his grief. Rachel reaches out to him, but is rebuffed.

Staying with their Aunt Kay and Uncle Cliff on Lake Pupuke, the twins are fascinated by the volcanic lake and the smell that seems to come from creepy old Wilberforce house around the shore. They visit Mt Eden, where Theo sees Mr Jones, a strange old man from whose hands fire seems to glow. When it seems the twins are being watched – and that the Wilberforces can smell them – Theo resolves to investigate the Wilberforce house. Inside, he and Rachel find what can only be an alien environment.

They overhear Mr Wilberforce talking about something stirring beneath the ground. He says he will kill the twins if they find "the fire-raiser". Rachel is alarmed and reaches out to Theo but, terrified of getting close to anyone since his mother’s death, pushes her away and sets out alone to find the fireraiser – the man he saw on the mountain top...

==Characters==
Based on the original novel by Maurice Gee:

* Rachel and Theo Matheson: Two seemingly ordinary fraternal twins who lived in a rural New Zealand town.  One day when they were three years old, they had wandered away from their home.  While a search party tried to find them, they were met by the enigmatic Mr. Jones who telekinetically kept them warm through the cold evening until they were found.  Over the years to follow, they had found that they shared a telepathic link and could read each others thoughts.  Upon meeting Mr. Jones again, eight years later, he reveals the reason for his interest in the twins and their important role in his plan for saving the world from the Wilberforces.
* Mr. Jones: The last surviving member of a benevolent alien race.  His true form is essentially a living flame: warm, brilliant, and without a definite shape.  On Earth, however, he takes the form of a kindly old man.  As he explains to Rachel and Theo, his race was known as "The People Who Understand" and had developed technology based on their powerful telepathic abilities.  Mr. Jones has the ability to teleport, communicate telepathically and project powerful beams of light from his physical self.  He had journeyed to Earth with his twin with the intention of finding and stopping the Wilberforces.  His twin didnt survive the journey to Earth.  Mr. Jones has since searched for twins whose minds are telepathically linked in order to complete the work which he and his older counterpart had started. symbiotic organisms consisting of a slug-like brain (roughly twice the mass of an adult) and a large worm like body.  According to the account given to the twins by Mr. Jones, the Wilberforces originated millions of years ago on a world consisting of seas of mud which nourished the worm organisms.  Threatened with extinction, the Wilberforces resolved themselves to conquering other worlds, using their technology to reform other planets surfaces into mud in order to thrive and breed.  When they had finished using a planets resources, no life remained on that world.  By the time Mr. Jones world had been visited by them several thousand years ago, the Wilberforces had overtaken one complete spiral arm of the galaxy.  Mr. Jones race succeeded in permanently incapacitating every Wilberforce on every planet they could be found on, including Earth.  However, the Wilberforces on Earth had learned to slowly counteract the telepathic weapons of The People Who Understand before being rendered dormant.  By the time Rachel and Theo are discovered by Mr. Jones, the Wilberforces have nearly completely awakened.
Kirsty Wilkinson, the former child actress, who originally portrayed Rachel in the Under the Mountain (TV miniseries)| 1981 television adaptation of "Under the Mountain", made a cameo appearance as Mr. Jones Neighbour.  Bill Johnson, who played "Mr. Wilberforce" in the original TV series has a brief cameo appearance as an unnamed gardener who mutters "bloody foreigners" after one of the alien Wilberforces says "lovely day" to him.

==Cast==
*Sam Neill - Mr Jones
*Oliver Driver - Mr Wilberforce
*Tom Cameron - Theo Matheson
*Sophie McBride - Rachel Matheson
*Jon Cummings - Wilberforce Drone # 3
*Matthew Chamberlain - Uncle Cliff
*Matt Gillanders - Wilberforce Drone # 6 Wayne Gordon - Wilberforce Drone # 4 Bruce Hopkins - Richard Matheson
*Toi Iti - Country Policeman
*Bill Johnson - Mr Carpenter (cameo)
*Miriama Kamo - News Reader
*Reuben King - Johan / Lenart Double
*Nathaniel Lees - Detective Gray
*Chelsea McEwan Millar - Clementine
*Nathan Meister - Johan / Lenart
*Colin Moy - Constable Green
*Gareth Reeves - Wilberforce Drone # 2
*Micheala Rooney - Aunty Kay
*Madeleine Sami - Constable Wood
*Allan Smith - Wilberforce Drone # 5
*Matt Sunderland - Wilberforce Drone # 1
*Leon Wadham - Ricky
*David Weatherley - Desk Sergeant
*Kirsty Wilkinson - Mr Jones Neighbour (cameo)

==Release==
Lionsgate Home Entertainment set the release for 10 August 2010 in the United States. 

==See also== Under the Mountain, a 1981 Television New Zealand series also based on Gees novel.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 