Chief Crazy Horse (film)
 
{{Infobox film
| name           = Chief Crazy Horse
| image_size     =
| image	=	Chief Crazy Horse FilmPoster.jpeg
| caption        =
| director       = George Sherman
| producer       = William Alland
| writer         = Gerald Drayson Adams (story and screenplay)
| starring       = Victor Mature
| cinematography = Harold Lipstein Al Clark	
| distributor    = Universal-International
| released       = April 27, 1955
| runtime        = 86 minutes
| country        = United States
| language       = English
| gross = $1.75 million (US)  
}} western film Lakota Sioux Chief Crazy Horse that, unusually for the time, portrays the Native American Indians in a more sympathetic light.

==Plot synopsis==
When young Crazy Horse (Victor Mature) wins his bride, rival Little Big Man (Ray Danton) goes to villainous traders with evidence of gold in the sacred Lakota burial ground. A new gold rush starts and old treaties are torn up. Crazy Horse becomes chief of his people, leading them to war at the Battle of the Little Big Horn.

==Cast==
*Victor Mature - Chief Crazy Horse
*Suzan Ball - Black Shawl (Little Fawn) John Lund - Maj. Twist
*Ray Danton - Little Big Man
*Keith Larsen - Flying Hawk Paul Guilfoyle - Worm
*David Janssen - Lt. Colin Cartwright
*Robert Warwick - Spotted Tail
*James Millican - General George Crook
*Morris Ankrum - Red Cloud/Conquering Bear
*Donald Randolph - Aaron Cartwright
*Robert F. Simon - Jeff Mantz
*James Westerfield - Caleb Mantz Stuart Randall - Old Man Afraid
*Pat Hogan - Dull Knife
*Dennis Weaver - Major Carlisle
*John Peters - Sgt. Guthrie Henry Wills - He Dog

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 