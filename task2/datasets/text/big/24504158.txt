Manasaare
{{Infobox film
| name           = Manasaare
| image          = Manasaare.jpg
| alt            =  
| caption        = 
| director       = Yograj Bhat
| producer       = Rockline Venkatesh
| writer         = Yograj Bhat
| starring       = Diganth Andrita Ray
| music          = Mano Murthy
| cinematography = Satya Hegde
| editing        = Harsha
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}} romantic psychological musical hit of the year. Directed by the veteran director Yogaraj Bhat of Mungaru Male fame, the movie follows the life of Manohar Diganth. Produced by Rockline Venkatesh, the film stars Diganth and Aindrita Ray in the lead roles. The film soundtrack and score was composed by Mano Murthy.  with lyrics by Yogaraj Bhat and veteran lyricist Jayant Kaikini.

==Plot==

Manohara aka Manu (played by Diganth) is an orphaned, unemployed young man, who is often looked down on upon by his family. However, his uncle (played by Achyuth Kumar) is the only exception. 

His friend Satish (played by Satish Neenasam) is a neighbour and cable operator, whose love for one of their neighhour - Bhamini, is unrequited. Bhamini gets married to a software engineer, and at the wedding reception, tells Manu that she loves him, but couldnt live with him (given his status). She also asks him to do something with his life, and not throw it away by spending his time criticizing the people around him. Meanwhile, Satish takes note of the "gulf" between Bhaminis new husband and himself, and decides to prove himself that he is indeed more worthy than she thinks of him. To that end, Manohar and Satish get together and invent a "electricity producing machine" which harnesses its power from vehicles that drive on roads (the fact that this isnt free energy, and that it would cost motorists who drive over it some extra fuel is not mentioned in the movie). His demonstration goes off well, but a police car chasing a criminal on a motorbike suddenly drives onto the machine, which causes the car to flip over, and in fact nearly kills the constables inside. 

Manu and Satish are arrested and reprimanded for their foolishness, but they are soon released. Manus aunt decides to get him married into a wealthy family, to get rid of him and an attempt to temper his behaviour. While they are visiting the intendeds family, a baby next to Manu begins urinating. Manu takes a nearby tea-cup to catch the urine and prevent getting wet. Everyone sees this and his prior behavior as confirmation that he is mentally unstable. Manohar and Satish, both drunk, stop at an isolated hill top and tell one another their opinions about life; they get into a row. Satish drives off leaving behind Manohar. 

Manohar walks back home and sees a van coming from a distance. The van is transporting about a dozen patients bound for an asylum. The inmates cause a commotion in the van, the guards loose order of the inmates paperwork and photographs but the inmates are subdued by the guards. The van stops so that the guards can relieve themselves, and the patient Mahendra escapes. Manu was near the van when the guards went after the escapee. The guards notice him and take him as the escapee. Manohar is forcibly taken to the Kaamanabillu asylum. 
 Pawan Kumar (of Lucia (2013 film) fame), who is a brilliant engineer, but considered a lunatic when one of his inventions, - a baby ass washing machine - injures a baby. He also bonds with Raju Thalikote, who is a self declared veteran of the asylum. Manu soon discloses his story to Shankrappa, who advises him to talk to Dollar, who in turn has always had an uncanny knack to escape from the asylum.

Soon, Manu manages to slip out, but during his escape, which takes him through the female wing of the asylum, he catches a glimpse of Devika (played by Aindrita Ray) and is smitten by her. Although he escapes, he changes his mind and returns to the asylum to pursue a relationship with her. As they are not allowed to mingle by the asylum staff, the inmates (under Shankrappas leadership) hatch and execute a plan to allow Manu to meet Devika. Shankrappa is well aware of the doctors tendency to perform the exact opposite to what he request them to do - since they perceive him to be insane - and he uses this to get them all sent for electro-shock therapy (since the EST room is within the womens wing). Dollar has manufactured a non-conducting gel which they then switch with the EST gel, so that theyre not really affected by the shock. They fake syncope, and when the staff leave for a prayer assembly, they sneak out to meet Devika. It turns out that Devika is sleeping (having been given an illegal pill, by a doctor who, unbeknownst to everyone engages in (implied) sexual abuse of female inmates, using the pill). Nonetheless, Manu - with Shankrappas assistance, carries Devika to an ambulance, and drives it out of the asylum.

Devika is a dental student who is in the asylum as she has cultivated intense misandry, after a lecturer misbehaved with her. This manifests itself as aggressive behaviour from time to time, where she stabs any man she deems dangerous. When she awakens the morning after being spirited away by Manu, she attacks him. However, Manu manages to sudbue her and professes his love to her. Devika and Manu decide to return to the asylum. On the way, at Devikas insistence, they take a temporary detour to her childhood home, where Devika has a flashback - of her uncaring father (who drove her mother to suicide). They then proceed to the asylum, but stop for the night. Manu spouts a monologue of his love for her, once she sleeps - and this is recorded by a CCTV camera that was activated accidentally by Devika, when they had hit a bump in the road earlier. The next morning, she discovers this, but while waking up Manu, unintentionally scares and nearly kills him when he falls down a railway overbridge. Manu is angry, and is about to leave her to her own devices, when she laughs at his rant, and he is smitten again. As they continue on foot (the van having run out of fuel), even as Manu pleads with her to go back (and not to the hospital). She doesnt respond right up until they go to the hospital, and convinces Manu to re-enter it.

As their relationship reaches serious levels, Devika asks Manu point blank whether he is capable of cheating her, to which the latter replies in the negative. However, once the escaped inmate (Mahendra) is caught, and brought to the asylum by the police (Mahendra is a murderer), the staff realise their mix-up. However, in front of everyone, they accuse Manu of being complicit in the charade, so that he could enjoy a free ride at the hospital. Devika construes this as Manus betrayal, and of the disingenuousness of his love, and spurns him. A shattered Manu is thrown out of the asylum, bidding a tearful farewell to his fellow inmates with whom he had developed a bond. As he leaves, he encounters Dollar (who had managed to finally escape) returning to the asylum, claiming that there was nothing in the world worth living for outside the asylum.

When he arrives at his uncles place however, he is once again exposed to the ridicule of people. 

The movie ends with Devika encountering him near his house. It is shown that she is cured of her dementia when she remembered Manu, who helped her to let go of her painful past, and move on; when she fought back the abusive doctor, and having watched the tape again, Devika is convinced that his love for her was real. The doctors have also concluded that Manu was responsible for "curing" Devika, and she says that is why she has returned to him; but Manu says that he himself is deemed to be uncured, and insane - being laughed at by society. Devika asks him, rather nonchalantly, as to how could that be his problem, and Manu comes around - with both of them pitying society, and walking away. The final scene closes with a voice-over, of a line that Shankrappa had delivered to Manu when the latter first arrived at the asylum - "Those in this world that come to an asylum, are those who can be cured. The ones who cannot be cured at all, remain outside. Theres a chance that youll be cured of madness, which is something that others outside dont have. Wheover comes here is only temporarily mad, but the ones on the outside are permanently so."

The movie has shades of "One flew over the Cuckoos Nest", the conundrum illustrated in "Catch-22" and also shows the institutionalisation depicted in "The Shawshank Redemption". The main thrust of the movie is to show the hypocrisy of a society which classifies behaviours, and, by extension, people into normal and abnormal.

==Cast==
* Diganth as Mahendra / Manohara
* Aindrita Ray as Devika
* Raju Thalikote
* Sharan
* Pawan Kumar

==Critical reception==
Critics gave favourable reviews to the movie. 

==Highpoints==
The screenplay of the movie is written by Pawan Kumar, who also wrote the screenplay for Lagori. Currently he has also written the story and the screenplay of Bhats new movie Pancharangi. Pawan also played the character Dollar in the movie Manasaare.

The costumes worn by the lead pair was prominently noticed, especially in the song "Ello Maleyaagideyandu". The experimental designs in the song "ondu kanasu" was especially praised. One of the high points of the movie was also the costumes worn by the staff and inmates of the rehabilitation centre, which gave a counterpoint to the dull and gloomy environment of the asylum. The costumes and styling was done by Bangalore based designer Shachina, who had earlier worked in Suris Junglee. She is generally considered to be the main contributing factor towards Aindritas glamour quotient.

==Box office==
The movie is the average of the year 2009. 

==Awards== Filmfare Awards
*Filmfare Award for Best Lyricist - Kannada - Jayant Kaikini - "Yello Maleyaagide"

;South Scope Awards 
*Best Film
*Best Director
*Best Actress
*Best Lyricist

==Soundtrack==
The music of the film was composed by Mano Murthy with lyrics penned by Jayant Kaikini, Yogaraj Bhat and Sloka.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|- 1 || Ondu Kanasu || Kunal Ganjawala, Ananya Bhagat, Earl Edgar (Rap)||  3.07
|- 2 || Ello Maleyaagideyandu || Sonu Nigam ||  4.09
|- 3 || Kanna Haniyondige || Krishnakumar Kunnath|KK, Shreya Ghoshal || 5.09
|- 4 || Onde Ninna || Sonu Nigam || 3.43
|- 5 || Naa Naguva Modalene || Shreya Ghoshal || 4.31
|- 6 || Naanu Manasaare || Vikas Vasishtha, Lakshmi Nagaraj || 2.40
|- 7 || Sahanabhavatu ||  Vijay Prakash || 2.54
|}

==References==
 

==External links==
*  
*http://movies.rediff.com/report/2009/sep/25/south-kannada-movie-review-manasaare.htm
*http://kannadaaudio.com/Songs/Moviewise/home/Manasaare.php

 

 
 
 
 