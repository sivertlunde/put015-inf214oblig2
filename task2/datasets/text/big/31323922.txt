For Eyes Only
{{Infobox film
| name           =For Eyes Only 
| image          = 
| image size     =
| caption        =
| director       =János Veiczi
| producer       =Siegfried Kabitzke
| writer         = Harry Thürk
| narrator       =
| starring       =Alfred Müller 
| music          = Günter Hauk
| cinematography =Karl Plintzner 
| editing        =Christel Ehrlich
| studio = DEFA
| distributor    = PROGRESS-Film Verleih
| released       =2 August 1963 (premiere held on 19 July)
| runtime        =103 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German espionage film. It was released in List of East German films|1963.

==Plot==
1961, West Germany. Hansen is a man who escaped from the German Democratic Republic and now works for an American company called Concordia. In fact, Hansen is an agent of the Stasi, and Concordia is the headquarters of Military Intelligence Division in Germany, from which countless saboteurs and spies have been sent over the border to wreak havoc in East Germany. The United States plans to invade the country, after staging an internal uprising as a provocation to intervene. The plans are held by Collins, Hansens superior. The latter intends to steal them, but has to evade the suspicions of security officer Rocker, who knows there is a leak. He is assisted by his chauffeur, František, who discovered his covert identity but intends to help him, in order to return to his homeland Czechoslovakia. The two manage to steal the safe with the invasion plans and cross the border to the East, pursued by the police and army. The plan is uncovered and the invasion has to be canceled.    

==Cast==
* Alfred Müller: Hansen
* Helmut Schreiber: Major Ted Collins
* Ivan Palec: František
* Hans Lucke: Colonel Rock
* Werner Lierck: Schuck
* Martin Flörchinger: Stasi colonel
* Peter Marx: Hartmann
* Eva-Maria Hagen: Peggy
* Rolf Herricht: Max
* Gerd E. Schäfer: Charly
* Christine Laszar: Hella
* Ingrid Ohlenschläger: Liz
* Renate Geißler: Gisela
* Marion van de Kamp: Adelheid
* Eberhard Esche: Stasi agent
* Norbert Flohr: Manfred
* Peter Friedrich: bearded man
* Georg Gudzent: MID general
* Hans Köcke: man with glass eye

==Production==
The idea to make For Eyes Only was conceived by dramatist Hans Lucke, who also played in the film. Although the its main theme - the planned invasion of the GDR by American-led NATO forces - was a common motif in East German propaganda since the beginning of the Cold War, author Bernd Stöver viewed For Eyes Only as the final link in a chain of publications, starting at 1958, intended by the government to demonstrate the necessity of building the Berlin Wall and to justify it after it was erected. Dramatist Heinz Hafke commented that "the Imperialist forces view the Cold War as a never-ending struggle against the Socialist states. The Cold War can become hot instantly, if the forces of peace will let down their guard. In this film, although we used some artistic freedom, we based the plot on actual people and documents... The setting of the story sometime before 13 August 1961 is justified and is supported by fact." Stöver claimed that the picture was to give the impression that the planned invasion was prevented by building the wall. This was reinforced by the All persons fictitious disclaimer|disclaimer, that announced: "Resemblance to real persons is intended".  
 17 June 1953, was based on a network uncovered in 1959, when three American agents related to it were captured in Karl-Marx-Stadt.  Thomas Lindenberger (editor). Massenmedien im Kalten Krieg: Akteure, Bilder, Resonanzen. Böhlau Verlag (2006). ISBN 978-3-412-23105-7. Pages 62-75. 

The producers received full cooperation from the Stasi, and several officers from the service were used as consultants. Director János Veiczi saw actor Alfred Müller in theater, playing  , 25 April 2007. 

==Reception==
The film was viewed by 800,000 people in the first month after its release,  and also exported to other Eastern Block countries, selling 9,000,000 tickets overall.  On 5 October 1964, Veiczi and writer Harry Thürk both received the National Prize of East Germany, 3rd Class for For Eyes Only  Erich Mielke had personally awarded the National Peoples Army Medal of Merit to Thürk, editor Christel Ehrlich, the dramatists and the leading actors.  Composer Günter Hauk was awarded the Heinrich Greif Prize on 17 May 1966, in recognition of his work on the pictures soundtrack.  

The film was received in a highly positive manner in the East German press: on 23 July 1963, the critic of the Schweriner Volkszeitung wrote: "it is thrilling, because its plot is true: a real plan to invade our country existed, but had to be canceled after 13 August 1961." In 10 August 1963, a commentator in Das Freie Wort noted "there is nothing fictional in there... Bonn constantly declares its intention to free East Germany."  

Peter Ulrich Weiß, who researched Cold War media in East Germany, pointed out that the film presented a highly politicized portrayal of the characters: Stasi agent Hansen was depicted as humane and devoted to his country, while being contrasted with the West Germans: he was only suspected, as one of the MID personnel tells in the film, because "he is the only one from the East... and the only one to have served during World War II merely as an infantry corporal. All the rest of our German employees have all either been in the SD or the Gestapo, or at the least in the SS." The Americans in the film - whose authenticity was enhanced by having the actors speak in American English, with a German dubbing over it - were all negative characters: one is active in trading stolen art, others are serial fornicators and another makes antisemitic remarks. One more Cold War token figure was the Czechoslovak driver of Hansen, who longs to return to his Socialist homeland. When he realizes his employer is a spy, he volunteers to help him in exchange for being allowed to flee with him to East Germany.  Daniela Berghahn considered For Eyes Only as the most famous and successful spy thriller ever made in the GDR.  

==References==
 

==External links==
*  
*  on the Goethe Institutes website.

 
 
 
 
 
 
 