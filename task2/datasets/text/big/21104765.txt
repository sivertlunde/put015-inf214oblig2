Koodevide
{{Infobox film
| name           = Koodevide?
| image          = Koodevide.jpg
| image_size     =
| caption        = Poster designed by Gayathri Ashokan
| director       = P. Padmarajan   
| producer       = Prem Prakash
| writer         = P. Padmarajan Novel: Vasanthi
| narrator       = Rahman
| Johnson
| cinematography = Shaji N. Karun
| editing        = Madhu Kainakari
| distributor    = Prakash Movietone
| released       =   
| runtime        = 150 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Malayalam drama Rahman who Tamil novel by Vasanthi. 

==Plot==

Alice (Suhasini) is a teacher at a boarding school in Ooty. A prodigal and unruly son of a Member of Parliament Xavier Puthooran, Ravi Puthooran (Rashin Rahman|Rahman) joins the school in Alices class. Alice manages to mentor him into a good student.  Alices boyfriend Captain Thomas (Mammootty) feels intensely jealous of the attention Alice shows upon Ravi Puthooran. He kills the boy apparently by an accident but later surrenders to the police, leaving Alice frustrated in all aspects of her life. 

==Awards and recognitions==

Koodevide? won several State awards and also was selected to Indian Panorama.    Rahman won Kerala State Film Award for Best Second actor and Sukumari for Best Second actress (1983). The film also won the award for Best Film with popular appeal and aesthetic value.  Rahman after this debut film went ahead to become the dream boy of Malayalam film in the 1980s.       

==Cast==
*Suhasini as Alice
*Mammootty as Captain Thomas Rahman as  Ravi Puthooran
*Jose Prakash as Xavier Puthooran
*Manian Pillai Raju as Shankar
*Prem Prakash as Captain George
*Sukumari as Susan
*Devi as Daisy
*Kottayam Santha
*Anjali Naidu as Rajamma

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 