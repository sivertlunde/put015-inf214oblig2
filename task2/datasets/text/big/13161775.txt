I Think I Do
{{Infobox Film
| name           = I Think I Do
| image_size     = 
| image	=	I Think I Do FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Brian Sloan
| producer       = Lane Janger
| writer         = Brian Sloan
| starring       = Alexis Arquette Christian Maelen Lauren Vélez
| cinematography = Milton Kam
| editing        = François Kraudren
| studio         = Danger Filmworks House of Pain Productions
| distributor    = Strand Releasing
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $345,478
}}
I Think I Do is a 1997 American List of LGBT-related films|gay-themed romantic comedy film written and directed by Brian Sloan and starring Alexis Arquette.

==Plot==
The film follows the relationship between Bob (Alexis Arquette) and Brendan (Christian Maelen), roommates at George Washington University in Washington, DC, five years after Bob made his romantic feelings toward Brendan known. When the two reconnect at the wedding of college friends, Bob is in a serious relationship with a soap opera star Sterling Scott (Tuc Watkins) while Brendan is single and re-examining his own identity.

==Cast==
* Alexis Arquette as Bob
* Christian Maelen as Brendan
* Lauren Vélez as Carol
* Tuc Watkins as Sterling Scott
* Jamie Harrold as Matt Guillermo Díaz as Eric
* Maddie Corman as Beth
* Marianne Hagan as Sarah
* Elizabeth Rodriguez as Celia
* Patricia Mauceri as Ms. Rivera
* Marni Nixon as Aunt Alice
* Arden Myrin as Wendy
* Lane Janger (Cameo appearance|cameo) as Bartender

==Reception==
I Think I Do grossed $345,478 while in theaters, only having been released in 10 theaters.  The film currently holds no score on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 