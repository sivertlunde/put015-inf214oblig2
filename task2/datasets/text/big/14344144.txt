Divided We Fall (film)
{{Infobox film
| name = Divided We Fall
| image = Divided_We_Fall.JPG
| director = Jan Hřebejk
| producer = Pavel Borovan Ondřej Trojan
| writer = Jan Hřebejk Petr Jarchovský
| starring = Bolek Polívka   Anna Šišková   Csongor Kassai
| music = Ales Brezina
| cinematography = Jan Malír
| editing = Vladimír Barák
| distributor = 
| released = Czech Republic: 16 March 2000 United States: 8 June 2001 United Kingdom: 31 May 2002
| runtime = 123 min.
| country = Czech Republic
| budget =
| gross = $1,830,938 (worldwide)  Czech German German
}}
 2000 Cinema Czech film directed by Jan Hřebejk. It was nominated for the Academy Award for Best Foreign Language Film.

==Plot==
The film opens in 1939   from David and getting more and more tender toward him. Horsts visits become more frequent, and one afternoon, he attempts to rape Marie. 

Josef gets his fertility tested to confirm a long-time rumor: He cant have children. Humiliated by his earlier antics, Horst takes revenge on Marie by forcing them to provide lodging for his supervisor, a committed Nazi bureaucrat, who had suffered a stroke after Nazis kill his son for deserting the army. Marie refuses to accept him on the grounds that she is pregnant. Unfortunately, the community is well-aware that Josef and Marie are infertile, and Josef proposes that David get Marie pregnant in order to stage a "miracle" and avoid further investigation. After much resistance from Marie, she and David have sex. Marie becomes pregnant, and Horst eventually apologizes for his previous behavior.  As the Germans lose ground in World War II, Horsts behavior begins to change. Based on his previous suspicions that someone else is living with Josef and Marie, he redirects German authorities when the latter search the street house by house. Finally, the Germans are defeated and the Czech people take brutal revenge on them.  As the Germans are being driven out, Marie goes into labor. Josef frantically searches for a doctor, but the streets of the city are in chaos and the Nazi-affiliated doctor has already been captured.

He finally finds the new ruling troika which includes his old neighbor Franta as the representative of the Czech Resistance. Unfortunately, Franta remembers him as a collaborator and orders his arrest. Josef protests his innocence and invites them to meet David as proof of his ambivalence towards the Jews, in exchange for a chance to find the Nazi doctor to deliver Maries baby. In the jails, Josef finds that the doctor has committed suicide but also finds Horst crouched in a corner. Remembering Horsts previous actions that saved Davids life, Josef tells the Czech soldiers that Horst is his doctor. The partisans escort them to Josefs house, driving through the ruins of the city. Horst is able to quickly assume the role of Maries doctor due to his experience delivering his own sons, much to Maries initial horror. The revolutionaries still want to see David in order to prove Josefs allegiance, but in the chaos and gunfire, David had hid himself elsewhere. The captain of the revolutionaries, a member of the external Czech forces, does not believe Josefs story and is about to shoot him, but David shows up at the last minute after Josefs despairing plea: "Let us be human!" After they all return to the household, Marie and Davids son is born. The revolutionaries interrogate David about Horsts background, but David, realizing that Horst had known about the situation for over two years and had not reported it, supports Josefs claim that Horst is a doctor and thus saves Horsts life as well.  

Days later, Josef walks the baby through the devastated streets of his city. In the ruins, he imagines Davids deceased family and his supervisors youngest son sitting around a small table, waving at him. Josef picks up Davids son and waves his hand back. An aria from J.S. Bachs St Matthew Passion (Erbarme dich, mein Gott, God, please have mercy on our frailty!) is the denouement of the film.

== Cast ==
*Bolek Polívka - Josef Cízek
*Anna Šišková - Marie Cizková
*Csongor Kassai - David Wiener
*Jaroslav Dušek - Horst Prohaska
*Martin Huba - Dr. Albrecht Kepke
*Jiří Pecha - Frantisek Simácek
*Simona Stašová - Libuse Simácková
*Vladimir Marek - SS Officer Richard Tesařík - Captain	
*Karel Heřmánek - Captain
==Themes==
Throughout the film, none of the principal characters is shown to be purely good or purely bad.  Josef has had to collaborate with the Nazis to provide additional cover while hiding a Jew in his home; Horst has been a pro-German collaborator, but has also kept his old friends secret, Marie has had to compromise herself for the sake of their safety, and even the anti-Nazi partisan Franta, now in a position of authority, had at one time tried to turn David over to the Germans. But by films end the war is over and it is time to work toward reconciliation and rebuilding. Thus, while ostensibly about the Nazi occupation and its aftermath, "Divided We Fall" carried a similar message of reconciliation for the contemporary Czech Republic in the aftermath of Soviet occupation and domination.

The imagery of the film is not even thinly disguised: Josef and Marie have a child that is savior (both for them and Horst), but Josef is not the father. It is a child of David.  The birth is even attended by three "wise men": a Czech, a German, and a Russian.

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 