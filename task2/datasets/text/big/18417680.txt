An Outcast Among Outcasts
 
{{Infobox film
| name           = An Outcast Among Outcasts
| image          =
| image size     =
| caption        =
| director       = D. W. Griffith Wilfred Lucas
| producer       =
| writer         = George Hennessy
| narrator       =
| starring       = Blanche Sweet Frank Opperman
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short silent silent drama debut film as a director. The film starred Blanche Sweet.   

==Cast==
* Frank Opperman - The Blanket Tramp
* Blanche Sweet - The Young Woman
* W. Chrystie Miller - The Postmaster Charles West - The Station Master
* Kate Toncray - A Homemaker
* Dorothy Bernard
* William A. Carroll - A Tramp
* J. Jiquel Lanoe - A Factory Manager David Miles
* W. C. Robinson - A Tramp

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
   
 

 