It Will Stay Between Us
{{Infobox film
| name           = It Will Stay Between Us
| image          = Zostane to medzi nami film.jpg
| alt            = 
| caption        = Theatrical release poster
| film name      = {{Infobox name module
| original       = Zostane to medzi nami}}
| director       = Miroslav Šindelka
| producer       = Miroslav Šindelka
| writer         = {{plain list|
*Miroslav Šindelka
*Slavena Pavlásková 
*Pavol Rankov
}}
| starring       = {{plain list|
*Tomáš Hanák 
*Danica Jurčová 
*Michal Dlouhý
}}
| music          = {{plain list|
*Michal Kaščák 
*Slavomír Solovic
}}
| cinematography = Ján Ďuriš
| editing        = Ľuba Ďurkovičová
| studio         = Film Factory
| distributor    = Bontonfilm  }}
| released       =  
| runtime        = 100 minutes
| country        = {{plain list|
*Slovakia
*Czech Republic
}}
| language       = {{flat list| Slovak
*Czech Czech
}}
| budget         = 
| gross          = 
}} Czech feature film|movie, directed by Miroslav Šindelka. The film starred Tomáš Hanák, Danica Jurčová and Michal Dlouhý.  

==Cast==
*Tomáš Hanák as Tomáš
*Danica Jurčová as Danica
*Michal Dlouhý as Michal
*Anna Šišková as Anna
*Božidara Turzonovová as Danicas mother
*Jozef Lenci as Danicas father
*Zdena Studenková as interpreter
*Ľubomír Paulovič as interpreters boyfriend
*Miroslav Noga as Šimon
*Szidi Tobias as Lea
*Michal Gučík as opera singer
*Zuzana Fialová as opera singers girlfriend
*Matej Landl as Maťo
*Zuzana Belohorcová as call girl
*Radim Uzel as psychiatrist
*František Kovár as priest
*Rastislav Piško as real estate agent
*Jaroslav Bekr as Jarko
*Marek Vašut as detective
*Marián Varga as organist
*Radoslav Brzobohatý as car accidents victim
*Hana Štěpánková as waitress

===Additional credits===
* Dušan Kojnok - architect sound
* Oľga Detaryová - supervising producer
* Darina Šuranová - costume designer
* Brani Gröhling - makeup artist mix
* Pavel Štverák - sound consultant
* Jaroslav Bekr - choreographer

==Awards ==
{| class="wikitable"
! Year
! Nomination
! Award 
! Category
! Result
|-
| rowspan=2| 2004
| Michal Kaščák 
| rowspan=2| Art Film Fest - SFZ, ÚSTT & LF Reward
| rowspan=2| Film Score
| rowspan=2  
|-
| Slavomír Solovic
|}

==See also==
*List of Slovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 