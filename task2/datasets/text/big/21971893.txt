The Divine Woman
{{Infobox film
| name           = The Divine Woman
| image          = The Divine Woman.jpg
| image_size     =
| caption        =
| director       = Victor Sjöström
| producer       = Richard A. Rowland John Colton (titles) Dorothy Farnum (writer)
| based on       =  
| narrator       =
| starring       = Greta Garbo Lars Hanson
| music          =
| cinematography = Oliver T. Marsh
| editing        = Conrad A. Nervig
| distributor    = MGM
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         = $266,817.14 
{{cite book
| author1 = Alexander Walker
| author2 = Metro-Goldwyn-Mayer
| title = Garbo: a portrait
| url = http://books.google.com/?id=nmZZAAAAMAAJ
| accessdate = 27 July 2010
| date = October 1980
| publisher = Macmillan
| isbn = 978-0-02-622950-0
| page = 184
}} 
| gross          =
}}
The Divine Woman (1928) is an American silent film directed by Victor Sjöström and starring Greta Garbo. Produced and distributed by Metro-Goldwyn-Mayer. Only a single nine-minute reel  and an additional 45 second excerpt    are currently known to exist of this otherwise lost film.

==Origin==
The film is adapted from the 1925 Broadway play Starlight by Gladys Unger, which starred Doris Keane. The plot is loosely based on stories of the early life of the French actress Sarah Bernhardt.

==Plot==
Marianne (Greta Garbo) is a poor French country girl who goes to Paris in the 1860s to seek her fortune as an actress. As she rises to success in the theatre, she must choose between the romantic attentions of two men. The first is Lucien (Lars Hanson), a poor but passionate young soldier who deserts the army to be with Marianne and goes to jail after stealing a dress to give her. Her other suitor is Henry Legrand (Lowell Sherman), a wealthy middle-aged Paris producer who offers her fame and fortune.

==Cast==
*Greta Garbo as Marianne
*Lars Hanson as Lucien
*Lowell Sherman as Henry Legrand
*Polly Moran as Mme. Pigonier
*Dorothy Cumming as Mmme. Zizi Rouck, Mariannes Mother
*Johnny Mack Brown as Jean Lery
*Cesare Gravina as Gigi
*Paulette Duval as Paulette
*Jean De Briac as Stage Director

==Existing reel==
Only one reel from the film is known to exist. It runs nine minutes and was discovered in 1993 at the Gosfilmofond, a film archive in Moscow. This film fragment has been released on DVD with a collection of Garbo films and has been broadcast on Turner Classic Movies. Its Russian intertitles have been translated into English. In this section of the film, Marianne is seen living in Paris in modest rooms. After a playful interchange with Mme. Pigionier, Marianne is joined by Lucien. The two lovers share a few poignant minutes together as the time approaches for him to return to the army.

==See also==
*List of incomplete or partially lost films

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 