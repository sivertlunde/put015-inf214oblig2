Everyone's Going to Die
 
{{Infobox film
| name           = Everyones Going to Die
| image          = 
| caption        =  Jones
| Jones
| writer         = Jones
| starring       = Nora Tschirner Rob Knighton
| music          = Charlie Simpson
| cinematography = Dan Stafford-Clark
| editing        = Jones
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
}}

Everyones Going to Die is a 2013 British film written and directed by directing collective Jones (filmmakers)|Jones.  The film had its World Premiere at South By Southwest in 2013, followed by its UK Premiere at the Edinburgh International Film Festival. 

==Cast==
* Nora Tschirner as Melanie
* Rob Knighton as Ray
* Kellie Shirley as Ali
* Madeline Duggan as Laura
* Stirling Gallacher as Jackie

==References==
 

==External links==
*  
*  
;Reviews
* http://www.dailyrecord.co.uk/entertainment/movies/movie-dvd-reviews/eiff-2013-review-everyones-going-2004398
* http://www.nerdly.co.uk/2013/07/19/eiff-2013-everyones-going-to-die-review/
* http://www.seensome.com/eiff/12-edinburgh-international-film-festival/302-eiff-2013-everyone-s-going-to-die-review
* http://cinehouseuk.blogspot.com/2013/07/everyones-going-to-die-eiff-review.html?spref=tw

 
 


 