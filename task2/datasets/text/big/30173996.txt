Dog Pound (film)
 
 
{{Infobox film
| name           = Dog Pound
| image          = Dog Pound.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = French theatrical poster
| director       = Kim Chapiron
| producer       = Kim Chapiron Georges Bermann
| writer         = Kim Chapiron Jeremie Delon
| starring       = Adam Butcher Shane Kippel Mateo Morales
| cinematography = Andre Chemetoff
| editing        = Benjamin Weill
| studio         = Canal+ Telefilm Canada|Téléfilm Canada Alliance Films Optimum Releasing   Roadshow Entertainment   Paradiso Entertainment   Alamode Film   Mars Distribution   Scanbox Entertainment   Frenetic Films  

| released       =  
| runtime        = 91 minutes  
| country        = Canada
| language       = English Spanish
}}
Dog Pound is a 2010 Canadian direct-to-video drama film|drama-thriller film directed by Kim Chapiron.  Chapiron co-wrote the screenplay with Jeremie Delon. It was released in Canada on 7 September 2010, but not theatrically, and went straight-to-DVD, but it was only theatrically in the United States, United Kingdom (also co-production of the movie), France (also co-production of the movie), Mexico, Switzerland (French speaking region only) and Spain, This is Kim Chapirons only Canadian film as France is served as one of the co-productions for the film and her only film to be in direct-to-video while her film in six countries (including U.S., U.K. and France) theatrically.

The direct-to-video film was an Canadian-majority production with co-producers in France and United Kingdom.

The film is a remake of influential British borstal film, Scum (film)|Scum. 

==Plot==

Butch, Davis and Angel are teenage criminals who have been sentenced to the Enola Vale Correctional Facility in Montana. Goodyear, a guard who looks after the young inmates, is tough, but fair-minded, and urges the youngsters to play by the rules and quietly serve their time so they can earn a second chance on the outside. The new inmates have to deal with young men who have been in lockup for a while, many of whom have become accustomed to the violence and intimidation of life behind bars. Banks is a hard case who thrives on bullying those smaller and more vulnerable than himself. Once he targets Butch, the new inmate starts down a path of retribution from which there is no return.

The focus revolves around Butch, who is imprisoned for attacking and blinding a correctional officer (later revealed to be in self-defense, as the correctional officer is accused of assaulting several other detainees). The film starts with an initial bedding in period where the main characters are briefed by Goodyear about how they should conduct themselves during their incarceration, including the possibility of earning points towards an early release should they behave. Butch initially attempts to conform with the rules. However, Davis finds himself the target of some of the longer term youths who have earned the special privileges reserved for good behavior, but have abused in order to intimidate the other boys.

Butch is eventually attacked by Banks, Loony, and Eckersley, whilst the bullying against Davis increases to the point that he is overdosed with an unknown substance given to him by Banks and Loony. Butch is sent to solitary confinement due to his refusal to reveal his attackers. Once he is out, Butch immediately takes revenge against the youths. After striking Loony across the nose with a ping pong paddle and demanding Eckersley not to snitch, Butch uses Davis as a lookout cover and brutally beats Banks with a cup. The event helps establishes Butchs rank within the detention center and offers temporary protection to Davis and some of the other inmates. Butch is then told that the original charges against him will probably be dropped due to the emergence of other witnesses in relation to his attack against the correctional officer. However, during a routine painting job, Goodyear and Angel get into an altercation. Angel is thrown into the wall and hits his head on a pipe, resulting in his death. Butch is placed in solitary confinement whilst an initial investigation takes place.

While Butch is in solitary, Davis is beaten up and raped by Loony. He tries to contact his mom during the night, but the officer at the time denies his request. The next morning, Davis dies because he cuts his wrists. Not because of internal bleeding because he was raped. As a result of Angel and Davis death, the dormitory unit goes on a hunger strike during breakfast. Frank starts an aggressive chant in the cafeteria. After a stare-down across the cafeteria with Loony, Butch loses control and instigates a riot in the cafeteria, during which he beats Loony. The detention officers are overwhelmed and use riot gear, tear gas, and plastic bullets in an attempt to end the riot. Butch escapes outside the building during the confusion and is set to flee, only to be taken down violently by the prison officers who break his leg and drag him back inside as he screams in agony.

==Cast==
* Adam Butcher as Butch
* Shane Kippel as Davis
* Mateo Morales as Angel Ortiz
* Slim Twig as Max
* Taylor Poulin as Banks
* Dewshane Williams as Frank
* Lawrence Bayne as Officer Goodyear
* Trent McMullen as Officer Sands
* Jeff McEnery as Loony
* Bryan Murphy as Eckersley
* William Christopher Ellis as Meakin
* Alexander Conti as Sal

==Release== premiered at the Tribeca Film Festival on 24 April 2010 and proceeded to a wide theatrical release in France on 23 June.

The film is currently available on video streaming system Netflix. Despite the systems alleged Motion Picture Association of America film rating system#MPAA film ratings|PG-13 rating, it is, in actuality, not rated.

==Critical reception==
The film was described as "intense" and not suited to the tastes of all viewers by reviewer Perri Nemiroff. 

The film received positive reviews; on review aggregator website Rotten Tomatoes, it holds a 65% "fresh" rating with a 6.2 average- based on 26 reviews.  Metacritic shows a 57/100 rating, signifying "mixed or average reviews". 

Dog Pound is French director Chapirons first English-language feature, and the film earned him an award as Best New Narrative Filmmaker at the 2010 Tribeca Film Festival.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 