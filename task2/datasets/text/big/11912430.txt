The Ship That Died of Shame
{{Infobox film
| name           = The Ship That Died of Shame
| image          = Tstdospos.jpg
| image_size     =
| caption        = Original film poster
| director       = Basil Dearden
| producer       = Basil Dearden Michael Relph
| writer         = Basil Dearden Nicholas Monsarrat (novel) Michael Relph John Whiting
| narrator       = George Baker Bill Owen
| music          = William Alwyn
| cinematography = Gordon Dines
| editing        = Peter Bezencenet
| studio         = Ealing Studios
| distributor    =
| released       = April 19, 1955
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} 1955 Ealing George Baker, Bill Owen.
 The Cruel Sea), which originally appeared in Lilliput (magazine)|Lilliput magazine in 1952. It was later published in a collection of short stories, The Ship That Died of Shame and other stories, 1959. 

==Plot==
The 1087 is a British Royal Navy motor gun boat that faithfully sees its crew through the worst that World War II can throw at them. After the end of the war, George Hoskins (Richard Attenborough) convinces former skipper Bill Randall (George Baker) and Birdie (Bill Owen) to buy their beloved boat and use it for some harmless, minor smuggling of black market items like wine. But they find themselves transporting ever more sinister cargoes; counterfeit currency and weapons. Though their craft had been utterly reliable and never let them down in wartime, it begins to break down frequently, as if ashamed of its current use. The crew revolt when they are used in the escape of a child murderer and (probable) paedophile.

==Cast==
*Richard Attenborough as George Hoskins George Baker as Bill Randall Bill Owen as Birdie
*Virginia McKenna as Helen Randall
*Roland Culver as Major Fordyce
*Bernard Lee as Customs Officer Brewster
*Ralph Truman as Sir Richard John Chandos as Raines Harold Goodwin as Customs officer
*John Longden as The Detective
*Alfie Bass as Sailor on board the 1087 (uncredited) John Boxer as Customs Man (uncredited)
*Stratford Johns as Garage Worker (uncredited)
*David Langton as Man in Coastal Forces Club Bar (uncredited)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 