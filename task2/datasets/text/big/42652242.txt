Sangliyana
{{Infobox film|
| name = Sangliyana
| image = 
| caption =
| director = P. Nanjundappa
| writer =  P. Nanjundappa
| based on =  Tara   Ambareesh
| producer = J Rameshlal   S V Ganesh   M R Kashinath   P Nanjundappa
| music = Hamsalekha
| cinematography = Mallikarjun
| editing = K. Balu
| studio = Pushpagiri Films
| released =  
| runtime = 134 minutes
| language = Kannada
| country = India
| budget =
}}
 Kannada action Tara in lead roles with actor Ambareesh making a special appearance.  The film is produced under "Pushpagiri Films" banner and the music is scored and composed by Hamsalekha.

The film was a blockbuster upon release and a sequel to this was made from the same cast and crew and released as S. P. Sangliyana Part 2 in 1990.

== Cast ==
* Shankar Nag 
* Bhavya 
* Ambareesh 
* Devaraj Tara
* Srinath
* Vajramuni
* Master Manjunath
* Disco Shanti
* Doddanna
* Lohitashwa
* Sudheer
* Gayatri Prabhakar

==Soundtrack==
{{Infobox album
| Name        = Sangliyana
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = Sangliyana album cover.jpg
| Border      = yes
| Alt         = 
| Caption     = Soundtrack cover
| Released    = 22 December 1987
| Recorded    =  Feature film soundtrack
| Length      = 19:07
| Label       = Lahiri Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The music was composed by Hamsalekha with lyrics for the soundtracks written by V. Manohar and Doddarangegowda. The album consists of four soundtracks. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 19:07
| title1 = Bandalo Bandalo Kanchana
| extra1 = S. P. Balasubrahmanyam, Manjula Gururaj
| lyrics1 = Hamsalekha
| length1 = 4:32
| title2 = Raja Nanna Raja
| extra2 = Shankar Nag, Manjula Gururaj
| lyrics2 = V. Manohar
| length2 = 4:40
| title3 = Doorada Oorininda
| extra3 = S. P. Balasubrahmanyam, Manjula Gururaj
| lyrics3 = Hamsalekha
| length3 = 4:41
| title4 = Preethiyinda
| extra4 = S. P. Balasubrahmanyam, Manjula Gururaj, B. R. Chaya
| lyrics4 = Doddarangegowda
| length4 = 5:14
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 

 