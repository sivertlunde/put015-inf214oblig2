Something Short of Paradise
{{Infobox film 
| name           = Something Short of Paradise
| image          = Something Short of Paradise.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = David Helpern
| producer       = Lester Burman James C. Gutman  
| writer         = Fred Barron  
| starring       = Susan Sarandon David Steinberg
| music          = Mark Snow
| cinematography = Walter Lassally
| editing        = Frank Bracht
| distributor    = American International Pictures  
| released       = September 14, 1979 (Canada) October 5, 1979 (US)
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget = 
| gross = 
}}

Something Short of Paradise is a 1979 romantic comedy film directed by David Helpern from a screenplay by Fred Barron. It stars Susan Sarandon and David Steinberg.

==Plot==
Madeleine Ross (Susan Sarandon) is a journalist, and her boyfriend, Harris Sloane (David Steinberg), is the owner of an art theatre.  Their relationship is on-again/off-again, and when Madeleine meets the French star Jean-Fidel Mileau (Jean-Pierre Aumont) it provokes changes in the way she and Harris feel about each other.

==Cast==
*Susan Sarandon as	Madeline Ross
*David Steinberg as Harris Sloane
*Jean-Pierre Aumont as Jean-Fidel Mileau
*Marilyn Sokol as Ruthie Miller
*Joe Grifasi as Barney Collins
*Robert Hitt as Edgar Kent

==Production and release==
Something Short of Paradise was shot in New York City,  and was advertised with the tagline "Love isnt blind... just a little nearsighted!" 

==References==
;Notes
 

 

 

==External links==
* 
* 
* 

 
 


 