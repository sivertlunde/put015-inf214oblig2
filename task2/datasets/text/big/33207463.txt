The Crusader (1932 film)
{{Infobox film
| name           = The Crusader
| image          =
| caption        =
| director       = Frank R. Strayer
| producer       = Phil Goldstone
| writer         = Wilson Collison
| starring       = Evelyn Brent
| music          =
| cinematography = 
| editing        = Otis Garrett
| distributor    =
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Crusader is a 1932 American drama film directed by Frank R. Strayer and starring Evelyn Brent.   

==Plot==
A pushy newspaper reporter Eddie Crane (Ned Sparks) schemes to get rid of crusading District Attorney Phillip Brandon (H. B. Warner). Complicating matters is the sordid past of Brandons wife Tess (Evelyn Brent) as well as his sister Marcias affair with a gangster.

== Cast ==
* Evelyn Brent as Tess Brandon
* H. B. Warner as Phillip Brandon
* Lew Cody as Jimmie Dale
* Ned Sparks as Eddie Crane Walter Byron as Joe Carson
* Marceline Day as Marcia Brandon
* John St. Polis as Robert Henley
* Arthur Hoyt as Oscar Shane
* Ara Haswell as Madge
* Joseph W. Girard as Corrigan
* Syd Saylor as Harry Smaltz
* Lloyd Ingraham as Alton

==References==
 

== External links ==
* 

 
 
 
 
 
 
 
 