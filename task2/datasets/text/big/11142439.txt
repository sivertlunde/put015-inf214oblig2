Kannum Karalum
 
{{Infobox film
| name           = Kannum Karalum
| image          = Kannum Karalum.jpg
| image_size     =
| caption        =
| director       = K. S. Sethumadhavan
| producer       = A. K. Balasubramaniam
| writer         = K T Muhammed
| narrator       = Baby Vinodini
| music          = M. B. Sreenivasan
| cinematography =
| editing        =
| distributor    =
| released       = 28 September 1962
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Kannum Karalum is a Malayalam language film starring Sathyaneshan Nadar|Sathyan,  Ambika Sukumaran, Sukumari, Kamal Haasan, and Vinodini Sasimohan|Vinodini. 
 Vinodini was a child artist when she did this film and she did a dual role in this movie. The climax scenes of this film was shot on the top of the Sreenarayana Guru memorial at Varkala, Sivagiri, Kerala|Sivagiri. The movie has been produced by A. K. Balasubramaniam under the banner of Saravana Pictures

==Cast== Sathyan
* Kamal Haasan
* Ambika Sukumaran
* Muthukulam Raghavan Pillai
* Sukumari
* Baby Vinodini (Vinodini Sasimohan)
* S. P. Pillai

==Soundtrack== 
The music was composed by MB Sreenivasan and lyrics was written by Vayalar Ramavarma. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aare Kaanaan Alayunnu || K. J. Yesudas, Renuka || Vayalar Ramavarma || 
|- 
| 2 || Chenthaamarappoonthen || Mehboob || Vayalar Ramavarma || 
|- 
| 3 || Kadaleevanathin || P. Leela || Vayalar Ramavarma || 
|- 
| 4 || Kalimannu Menanju (Happy) || P. Leela || Vayalar Ramavarma || 
|- 
| 5 || Kalimannu Menanju (Sad) || P. Leela || Vayalar Ramavarma || 
|- 
| 6 || Thaatheyyam Kaattile || Latha Raju || Vayalar Ramavarma || 
|- 
| 7 || Thirumizhiyaale || K. J. Yesudas, P. Leela || Vayalar Ramavarma || 
|- 
| 8 || Valarnnu Valarnnu || S Janaki || Vayalar Ramavarma || 
|}

== References ==
 
*http://www.malayalachalachithram.com/movie.php?i=95

 
 
 


 