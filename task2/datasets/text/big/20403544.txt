Oh, My God (2008 film)
 Oh My God}}
 
 
{{Infobox Film
| name           = Oh, My God !!
| image =Oh,_My_God_(2008_film).jpg
| caption        = 
| director       = Sourabh Shrivastava Amit Kapoor
| producer       = Zee Limelight
| writer         = Vipul Binjola Asit Kaul Manisha Ojha Rishi Virmani
| starring       = Vinay Pathak Saurabh Shukla Divya Dutta Gaurav Gera Harsh Chhaya
| music          = Bapi-Tutul
| genre          = Comedy
| cinematography = Amol Rathod
| editing        = Nipun Gupta
| released       =  
| country        = India
| language       = Hindi
}}
 released on 5 December 2008. The film is about an employee who puts more time trying to get people to invest in a money-making scheme.


==Plot==
Vinay Pathak stars as Rajendra, a salesman who wants to make it big in life but through ethical ways. Suman (Divya Dutta) is Rajendras wife, who stands by her husband. As Rajendra continues to fail in his various money-making schemes, Suman pleads with God to help her husband.

God (played in human form by Saurabh Shukla) tries to help Rajendra become successful but is constantly frustrated as Rajendra fails to recognize the opportunities.

==Cast==
*Vinay Pathak.... Rajendra Dubey
*Divya Dutta.... Suman Dubey
*Saurabh Shukla.... Gurdeep Singh Bhanot / Havaldar Choudhary / Doctor / Bhagwan
*Harsh Chhaya.... Shekhar
*Gaurav Gera.... Piddu
*Pubali Sanyal.... Meenu
*Shruti Sharma.... Radicool
*Mushtaq khan (cameo).... Inspector Rana

== External links ==
*  

 
 
 

 