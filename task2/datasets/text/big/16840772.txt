My Monster Mom
{{Infobox film
| name           = My Monster Mom
| image          = MyMonsterMom.jpg
| image_size     =
| caption        = Theatrical movie poster
| director       = Jose Javier Reyes
| producer       = Lily Monteverde Jose Javier Reyes Anna Teresa Gozon-Abrogar Roselle Monteverde-Teo Charyl Chan
| writer         = Jose Javier Reyes
| narrator       =
| starring       = Ruffa Gutierrez Annabelle Rama
| music          = Jesse Lucas
| cinematography = Rodolfo Aves Jr.
| editing        = Ria De Guzman
| distributor    = Regal Films GMA Films
| released       =  
| runtime        =
| country        = Philippines
| language       = Filipino English Visayan
| budget         = Php 10 Million
| gross          = Php 96,562,123
}}

My Monster Mom is a 2008 comedy-drama film produced and distributed by Regal Films and GMA Films. It stars Annabelle Rama as Esme, an abrasive, loudmouth mother, and Ruffa Gutierrez  as Abbey, her United States|American-spoken daughter from the United States. Together they go through a series of antics and mishaps that eventually shapes their relationship for the better.

==Plot== Eddie Gutierrez) her getting pregnant, which made her stop schooling. Left with no choice, she lives with him under one roof, only to find out that he is cheating on her. After a violent confrontation, she flees and resorts to doing odd jobs to earn money. Several months pass and she soon gives birth to Abbey. But unfortunately, she gives her daughter to her relatives, who were at that time leaving the country and migrating to the United States, in hopes that Abbey will find a better life there. 27 years later, Esme has now become an extraordinarily wealthy jewellery trader. She is turned ecstatic upon hearing that her daughter Abby is coming back to the Philippines as a liberal New Yorker. Esme, now having 2 sons named Boboy and Pipo, both of which were born from two different fathers, welcomes her daughter to her real family. This then leads to violent, often humorous confrontations with other people around Esme and her family, ranging from one of her sons "secret" girlfriend, to her rival neighbor. A couple of months later, after her constant battles with other people finally tears her family apart, but is then reunited when she develops a heart attack -forcing her sons and daughter to forgive their mother for all the trouble she has caused. After several weeks in the hospital, Esme, now fully recovered, though still brash, decides to meet her long lost husband once more. The film ends 3 months later, Abbey is in the United States once again, only to be surprised by her mother, who subsequently follows her and plans to live with her in New York.

==Cast==
*Ruffa Gutierrez as Abbey Gail Fajardo
*Annabelle Rama as Adult Esmeralda Fajardo
*Rhian Ramos as Teenage Esmeralda Fajardo
*JC de Vera as Boboy
*Michelle Madrigal as Maureen
*Martin Escudero as Pipo
*Iwa Moto as Vivian
*Eugene Domingo as Marilou
*Richard Gutierrez as Teenage Waldo Eddie Gutierrez as Adult Waldo
*Khryss Adalia as Homer
*Chariz Solomon as Precy
*Bubbles Paraiso as Karen
*Vangie Labalan as Lumeng

==References==
 

==External links==
*  

 
 
 
 
 
 