Who's That Knocking at My Door
 
{{Infobox film
| name = Whos That Knocking at My Door
| image = Whos That Knocking at My Door film poster.jpg
| caption= Theatrical release poster
| director = Martin Scorsese Haig Manoogian
| writer = Martin Scorsese
| starring = Harvey Keitel Zina Bethune  Michael Wadley Richard Coll
| editing = Thelma Schoonmaker
| studio = Trimod Films
| distributor = Joseph Brenner Associates
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = $75,000 USD (estimated)
}} director and Harvey Keitels debut as an actor. Exploring themes of Catholic guilt similar to those in his later film Mean Streets, the story follows Italian-American J.R. as he struggles to accept the secret hidden by his independent and free-spirited girlfriend. 
 1968 Chicago Chicago Film Festival.

==Plot== Catholic Italian-American virgin and he wants to wait.

One day, his girl tells him that she was once raped by a former boyfriend.  This crushes J.R., and he rejects her and attempts to return to his old life of drinking with his friends. However, after a particularly wild party with friends, he realizes he still loves her and returns to her apartment one early morning.  He awkwardly tells her that he forgives her and says that he will "marry her anyway."  Upon hearing this, the girl tells him marriage would never work if her past weighs on him so much.  J.R. becomes enraged and calls her a whore, but quickly recants and says he is confused by the whole situation.  She tells him to go home, and he returns to the Catholic church, but finds no solace.

==Cast==
  
 
* Zina Bethune - Girl
 
* Harvey Keitel - J.R. 
 
* Ann Collette  - Girl in dream
* Lennard Kuras - Joey 
* Michael Scala - Sally Gaga
* Harry Northup - Harry 
* Tuai Yu-Lan  - Girl in dream
* Saskia Holleman  - Girl in dream
  
 
* Bill Minkin - Iggy at Party 
* Philip Carlson - Boy in Copake 
* Wendy Russell - Gagas Girl 
* Robert Uricola - Boy with Gun
* Susan Wood - Girl at Party 
* Marisa Joffrey - Girl at Party 
* Catherine Scorsese - Mother 
* Victor Magnotta - Boy in Fight
* Paul DeBonde - Boy in Fight
 
Martin Scorsese appears in an uncredited role as a gangster

==Production==
Whos That Knocking at My Door was filmed over the course of many years, undergoing many changes, new directions and different names along the way. The film began in 1965 as a student short film about J.R. and his do-nothing friends called Bring on the Dancing Girls. In 1967, the romance plot with Zina Bethune was introduced and spliced together with the earlier film, and the title was changed to I Call First. This version of the film received its world premiere at the Chicago International Film Festival in November 1967. Finally, in 1968, exploitation distributor Joseph Brenner offered to buy the picture and distribute it on the condition that a sex scene be added to give the film sex exploitation angles for marketing purposes. Scorsese shot and edited a technically beautiful but largely gratuitous montage of J.R. fantasizing about  bedding a series of prostitutes (shot in Amsterdam, the Netherlands with a visibly older Keitel) and the film finally became Whos That Knocking at My Door (named for the song which closes the film).  The film was then re-issued under the title "J.R." in 1970,  however all subsequent releases have been published under the 1968 title.
 Eclair NPR camera in order to introduce greater mobility, then blow up the footage to 35&nbsp;mm.

==Reception==
American critic Roger Ebert gave the film an extremely positive review after its world premiere at the Chicago International Film Festival in November 1967 (when it still went by the name "I Call First"). He called the film "a work that is absolutely genuine, artistically satisfying and technically comparable to the best films being made anywhere. I have no reservations in describing it as a great moment in American movies."    

When the film finally received its theatrical release two years later, Ebert admitted that he had been perhaps a little over eager with his first review, admitting that "Scorsese was occasionally too obvious, and the film has serious structural flaws." However, he was still highly positive towards the film, and suggested that "It is possible that with more experience and maturity Scorsese will direct more polished, finished films." 

==Notable appearances and cameos==

Martin Scorseses mother, Catherine Scorsese|Catherine, appears briefly as J.R.s mother. Mrs. Scorsese would continue to appear in many of her sons films until her death in 1997. Scorsese himself appears uncredited as one of the gangsters. To this day, he still makes cameo appearances in many of his films.

The role of Sally Gaga is played by Michael Scala, the father of rapper Pizon.

==Notes==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 