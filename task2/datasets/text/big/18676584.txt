Il padrone del vapore
 
 
{{Infobox film
| name           = Il padrone del vapore
| image          = 
| caption        = 
| director       = Mario Mattoli
| producer       = Dino De Laurentiis Carlo Ponti
| writer         = 
| starring       = Riccardo Billi
| music          = Armando Fragna
| cinematography = Tonino Delli Colli
| editing        = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Il padrone del vapore is a 1951 Italian comedy film directed by Mario Mattoli and starring Riccardo Billi.

==Plot==
A rich American arrives in a little village in the mountains because he wants to advertise a drink he produces. In the village there are also two men from Rome who are at logger-heads with the locals. The coming of the American complicates matters.

==Cast==
* Riccardo Billi
* Carlo Campanini
* Walter Chiari
* Aldo Giuffrè
* Carlo Giuffrè
* Zoe Incrocci
* Sophia Loren - Ballerinetta (as Sofia Lazzaro)
* Raffaele Pisu
* Giusi Raspani Dandolo
* Mario Riva
* Delia Scala
* Gisella Sofio
* Gianrico Tedeschi
* Bice Valori

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 