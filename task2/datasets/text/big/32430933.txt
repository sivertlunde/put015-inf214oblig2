Eeshwar (1989 film)
{{Infobox film
| name = Eeshwar
| image = eshwarfilm.jpg
| caption = Vinyl Record Cover
| writer = 
| starring = Anil Kapoor Vijayshanti Saeed Jaffrey
| director = K. Vishwanath Rajesh Malik
| producer = 
| music = Laxmikant-Pyarelal
| lyrics = 
| released = 24 February 1989
| language = Hindi
}}

Eeshwar is an Indian film directed by K. Vishwanath and Rajesh Malik and released in 1989. The movie stars Anil Kapoor, Vijayshanti, Saeed Jaffrey and Sadashiv Amrapurkar. The film is a remake of the Telugu film Swathi Muthyam which starred Kamal Haasan and Raadhika.

==Cast==
* Anil Kapoor as Ishwar a.k.a. Ishwarchand Vishnuchand Brahmanand Verma
* Vijayshanti as Lalita
* Shammi as Ishwars grandmother
* Saeed Jaffrey as Masterji (Guest Appearance)
* Asha Sachdev as the village washerwoman
* Sadashiv Amrapurkar as Ishwars scheming relative
* Vinod Mehra as Lalitas brother
* Bharti Achrekar as Lalitas sister-in-law
* Sukanya Kulkarni
* Vikram Gokhale as Lalitas father-in-law (father of her first husband)
* Gulshan Grover as Neighbour of Eeshwar in negative role
* Rasik Dave as son of eeshwar

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kaushalya Main Teri"
| Nitin Mukesh, Kavita Krishnamurthy
|-
| 2
| "Aage Sukh To Peeche Dukh Hai"
| Nitin Mukesh, Kavita Krishnamurthy
|-
| 3
| "Ramji Ne Dhanush Toda"
| Suresh Wadkar, Nitin Mukesh, Kavita Krishnamurthy
|-
| 4
| "Dharmam Sharnam Gachhami"
| Nitin Mukesh, Alka Yagnik
|-
| 5
| "Baj Utha Saanson Mein"
| Nitin Mukesh, Kavita Krishnamurthy
|}

==Awards==
*K. Viswanath won Filmfare award for  Best story.

==External links==
*  

 
 
 
 
 
 

 