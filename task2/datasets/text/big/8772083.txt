Tiger Bay (film)
 
 
 
 
{{Infobox film
| name           = Tiger Bay
| image          = Tiger Bay-1959.jpg
| image_size     =
| caption        =
| director       = J. Lee Thompson John Hawkesworth
| writer         = John Hawkesworth Shelley Smith
| starring       = John Mills Horst Buchholz Hayley Mills
| music          = Laurie Johnson Eric Cross
| editing        = Sidney Hayers Independent Artists (Julian Wintle, Leslie Parkyn)
| distributor    = Rank Organisation
| released       = March 1959
| runtime        = 103 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
 John Hawkesworth and Shelley Smith (pseudonym of Nancy Hermione Bodington). It stars John Mills as a police superintendent investigating a murder; his real life daughter Hayley Mills, in her first major film role, as a girl who witnesses the murder; and Horst Buchholz as a young sailor who commits the murder in a moment of passion.
 Newport (12 black street A Taste of Honey.

== Plot summary ==
In Cardiff, a young Polish sailor named Bronislav Korchinsky (Horst Buchholz) returns from his latest voyage to visit his girlfriend Anya (Yvonne Mitchell). After he finds a woman named Christine (Shari) living in her apartment, the landlord tells him that he evicted Anya and gives him her new address, which is also the home of a young girl named Gillie Evans (Hayley Mills), an orphaned tomboy who lives with her Aunt. Gillies angelic face hides the fact that she is a habitual liar. She dearly wants a cap gun so she can play "Cowboys and Indians" with the boys in her neighbourhood. Korchinsky arrives shortly after she gets into a fight; she begins to like him as she leads him to her apartment building. 

Korchinsky finds Anya in her new flat, but she wants nothing to do with him. Dissatisfied with waiting while he is at sea, she has been seeing another man, a married sportscaster named Barclay (Anthony Dawson). When Korchinsky, furious with jealousy, assaults her, she defends herself with a gun, but he takes the gun from her and shoots her dead. Gillie witnesses the incident through the letter box in the apartment door. When the landlord investigates the noise, Gillie hides in a cupboard, and when Korchinsky hides the gun near her, she takes it and runs into her apartment. Barclay then arrives to visit Anya, but, finding her dead, quickly flees. A neighbour discovers the body shortly afterward and summons the police.

Wanting to keep the gun, Gillie lies to police superintendent Graham (John Mills) about what she saw. Korchinsky follows her to a wedding at her church, where she shows the gun to a boy who sings with her in the choir and gives him a bullet; Korchinsky subsequently chases her into the attic. After he takes the gun from her, they make friends and he agrees to take her to sea with him when he flees the country. He learns that a Venezuelan merchant ship, the Poloma, will leave port the next day, so Gillie leads him to a hiding place in the countryside, where he entertains her by re-enacting his overseas adventures. When the Poloma is due to sail (the scene of the vessel departing is actually filmed at Avonmouth), he persuades Gillie to let him go alone, retrieves his identification papers from Christine, and signs on with the ship.
 station at Barry Docks and takes her on a Pilot Boat to the Poloma as the ship approaches the boundary of territorial waters, three miles from shore.

At this point, Gillie is obviously trying to obstruct Grahams progress. When he confronts her and Korchinsky aboard the Poloma, they deny knowing each other. Nevertheless, Graham attempts to arrest Korchinsky, but the ships captain prevents him, saying that his navigation officer has plotted Polomas position as just outside the three-mile limit, and therefore beyond the jurisdiction of the British police.

Finally, Gillie falls overboard while trying to stow away on the ship in the hope of remaining with Korchinksy. Being the only person to see her fall, Korchinsky ignores the risk of arrest and dives into the water to save her and they are both rescued by the police boat. Korchinsky admits his guilt after Gillie hugs him, and Graham commends him for his bravery in saving her.

== Cast ==
* John Mills as Police Superintendent Graham
* Horst Buchholz as Bronislav Korchinsky, a professional Polish seaman
* Hayley Mills as Gillie Evans, a wayward young girl
* Yvonne Mitchell as Anya Haluba, Korchinksys long-time girlfriend
* Megs Jenkins as Mrs Phillips, Gillies aunt.
* Anthony Dawson as Barclay, Anyas new boyfriend
* George Selway as Detective Sergeant Harvey
* Shari as Christine
* George Pastell as Poloma Captain
* Paul Stassino as Poloma First Officer
* Marne Maitland as Dr. Das Meredith Edwards as PC Williams
* Marianne Stone as Mrs Williams
* Rachel Thomas as Mrs Parry
* Brian Hammond as Dai Parry
* Kenneth Griffith as Choirmaster
* Eynon Evans as Mr. Morgan
* Christopher Rhodes as Inspector Bridges
* Edward Cast as Detective Constable Thomas

==Reception==
The film was popular at the box office. J. LEE THOMPSON DISCUSSES CAREER: GUNS OF NAVARONE DIRECTOR TOOK DEVIOUS PATH TO FILMS
By MURRAY SCHUMACH Special to The New York Times.. New York Times (1923–Current file)   25 July 1961: 18. 
== Awards ==
* 1960 Won BAFTA Film Award   –  Most Promising Newcomer to Film, Hayley Mills
* 1960 Nominated BAFTA Film Award  –   Best British Film, J. Lee Thompson
* 1960 Nominated BAFTA Film Award – Best British Screenplay, John Hawkesworth and Shelley Smith
* 1960 Nominated BAFTA Film Award – Best Film from any Source, J. Lee Thompson
* 1959 Won Silver Bear, 9th Berlin International Film Festival Special Prize, Hayley Mills   
* 1959 Nominated Golden Berlin Bear, J. Lee Thompson

== References ==
 

==Notes==
* Williams, Melanie (2005).  "Im Not A Lady: Tiger Bay (1959) and transitional girlhood in British cinema on the cusp of the 1960s".  Screen: Vol. 46, No. 3, Autumn 2005

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 