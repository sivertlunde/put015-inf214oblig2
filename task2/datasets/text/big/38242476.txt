The Barrier (1937 film)
{{Infobox film
| name           = The Barrier
| image          = 
| caption        = 
| director       = Lesley Selander
| producer       = 
| writer         = Rex Beach (novel) Bernard Schubert (screenplay)
| starring       = 
| music          =
| cinematography = 
| editing        =
| distributor    = Paramount Pictures
| released       = November 26, 1937 (USA)
| country        = United States
| language       = English
}}

The Barrier is a 1937 film directed by Lesley Selander. 

==Cast==
*Leo Carrillo as Poleon Doret
*Jean Parker as Necia Gale James Ellison as Lieutenant Burrell
*Otto Kruger as Stark
*J.M. Kerrigan as Sergeant Thomas
*Robert Barrat as John Gale
*Andy Clyde as No-Creek Lee
*Sally Martin as Molly Gale
*Sara Haden as Mrs. John Alluna Gale
*Addison Richards as Henchman Runnion
*Alan Davis as Sergeant Tobin
*Fernando Alvarado as Johnny Gale

==External links==
* 
* 

 
 
 

 