Hidden Agenda (2001 film)
{{Infobox Film
| name           = Hidden Agenda
| image          =Hidden Agenda01.jpg
| caption        =
| director       =Marc S. Grenier
| producer       =  
| writer         = Les Waldon
| starring       = Dolph Lundgren
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       =2001
| runtime        = 95 minutes 
| country        = Canada English
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}} Canadian action film directed by Marc S. Grenier and starring Dolph Lundgren.

==Cast==
*Dolph Lundgren	... 	Jason Price
*Maxim Roy	... 	Renee Brooks
*Brigitte Paquette	... 	Connie Glenn
*Ted Whittall	... 	Sonny Mathis
*Serge Houde	... 	Paul Elkert
*Alan Fawcett	... 	Sam Turgenson
*Francis X. McCarthy	... 	Deputy Director Powell (as Francis McCarthy)
*Harry Standjofski	... 	Kevin Christian Paul	... 	Charlie Radisson
*Andreas Apergis	... 	Boris Yoesky Jeff Hall	... 	Vincent Moretti
*Cas Anvar	... 	Agent McCoomb
*Lynne Adams	... 	Prosecutor
*Alan Legros	... 	Jerry
*Jay Levalley	... 	Paolo Bucci

==External links==
*  

 
 
 
 
 

 
 