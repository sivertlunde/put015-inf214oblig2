Submarine Attack
{{Infobox film
| name = Submarine Attack, aka La Grande Speranza, the Great Hope, Torpedo Zone
| image = SubAttackMovie.jpg
| producer = Duilio Coletti, Excelsa Film
| director = Duilio Coletti
| writer = Oreste Biancoli, Marc-Antonio Bragadin, Duilio Coletti, Ennio De Concini
| starring = Lois Maxwell, Renato Baldini, Folco Lulli
| music = Nino Rota
| cinematography = Leonida Barboni
| editing = Giuliana Attenni
| distributor = I.F.E. Releasing Corporation (English version)
| released = 1954
| runtime = 86 minutes (English version)
| country = Italy
| language = Italian, dubbed into English
}} Berlin International Film Festival.    Also noteworthy is the subsequent career of the only woman in the film, Lois Maxwell, who went on to play Miss Moneypenny in 14 James Bond films.

== Plot summary == Italian submarine captain conducts successful attacks on enemy merchant shipping in the eastern Atlantic Ocean during World War II, and then rescues the survivors of his victims, including a member of the Canadian Womens Army Corps (and a dog). The captains compulsion to save his victims culminates in his taking aboard 24 additional Danish merchant seamen; with no space down below, they are accommodated under the walkway outside the hull, at risk of drowning if the submarine is forced to submerge. He then sails the survivors hundreds of miles across the open ocean on the surface to put them ashore in the Azores.

==Cast==
* Lois Maxwell - Lt. Lily Donald
* Renato Baldini - The Submarine Commandanti
* Carlo Bellini - Officer
* Aldo Bufi Landi - Lieutenant Earl Cameron - Johnny Brown, POW
* David Carbonari
* Ludovico Ceriana
* Carlo Delle Piane - Ciccio
* Edward Fleming - Jean Cartier
* José Jaspe - Spanish POW
* Paolo Panelli
* Rudy Solinas
* Henri Vidon - Robert Steiner
* Folco Lulli - Nostromo, First Mate
* Guido Bizzarri

==References==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 