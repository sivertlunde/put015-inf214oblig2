Siva Rama Raju
{{Infobox film
| name           = Siva Rama Raju
| image          = 
| image size     =
| caption        =
| director       = V. Samudra
| producer       = R. B. Choudary
| writer         = Paruchuri Brothers  
| story          = Erode Soundar
| screenplay     = V. Samudra  Sivaji Poonam Laya Kanchi Monica
| music          = S. A. Rajkumar
| studio         = Super Good Films
| cinematography = Shyam K. Naidu
| editing        = Nandamuri Hari 
| distributor    =
| released       =  
| runtime        = 2:44:10
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}
 2002 Telugu Sivaji and Monica in the lead roles and music composed by S. A. Rajkumar.  The film was a remake of Tamil film Samudhiram.

==Plot==
Sivaramaraju (Jagapati Babu), Rama Raju (Venkat), Raju (Sivaji) and Swathy (Monica) are siblings. They belong to a royal dynasty and at present they are orphans. Sivaramaraju takes the onus of raising his siblings. In such a situation, Sivaramaraju happens to marry off his sister to a guy whose family is on the mission of taking revenge on him. The rest of the film deals with how the sister and brothers travail hard to unite.

==Cast==
{{columns-list|3|
* Jagapati Babu as Shiva Rama Raju
* Nandamuri Harikrishna as Ananda Bhupati Raju
* Venkat as Rama Raju  Sivaji as Raju Monica as Swathi
* Poonam as Shiva Rama Rajus wife  Laya as Rama Rajus wife 
* Kanchi Kaul as Rajus Lover
* Pyramid Natarajan
* Anand Raj
* Bramhanandam
* Dharmavarapu Subramanyam as Lecturer
* Dasari Arun Kumar as Special Appearance
* Sijju as Swathis husbend
* Raghunath Reddy Rami Reddy
* Vizag Prasad
* L.B. Sriram as Srisailam
* Lakshmipati
* Tirupathi Prakash
* Gadiraju Subba Rao
* Chandra Mouli
* Madhu
* Junior Relangi
* Satyapriya
* Sana
* Varsha
* Madhavi Sri
* Suman Sri
}}

==Soundtrack==
{{Infobox album
| Name        = Siva Rama Raju
| Tagline     = 
| Type        = Soundtrack
| Artist      = S. A. Rajkumar
| Cover       = 
| Released    = 2002
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:31
| Label       = Aditya Music
| Producer    = S. A. Rajkumar
| Reviews     =
| Last album  = Nee Premakai   (2002)
| This album  = Siva Rama Raju   (2002)
| Next album  = Vasantham   (2003)
}}

Music composed by S. A. Rajkumar. Lyrics written by Chirravuri Vijay Kumar.  Music released on Aditya Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:31
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Andala Chinni Devatha 
| extra1  = Shankar Mahadevan
| length1 = 6:42

| title2  = Ding Ding Sujatha
| length2 = 5:16

| title3  = Amma Bhavani  SP Balu
| length3 = 6:06

| title4  = Pidugulu Padiponi 
| extra4  = S. P. Balu
| length4 = 2:33

| title5  = Nirupedala Devudivaiah
| extra5  = S. P. Balu
| length5 = 4:22

| title6  = Swagatham 
| extra6  = Udit Narayan,Sujatha
| length6 = 4:12
}}

==References==
 

==External links==
*  

  

 
 
 
 


 