Stolen Women: Captured Hearts
{{Infobox television film
| bgcolour =
| name = Stolen Women: Captured Hearts
| image = DVD cover of the movie Stolen Women- Captured Hearts.jpg
| caption = DVD cover
| format = Drama, Western
| director = Jerry London
| producer = Leigh Murray Vanessa Greene
| starring = Janine Turner Patrick Bergin Jean Louisa Kelly Michael Greyeyes Rodney A Grant
| writer = Richard Fielder
| country = United States
| language = English
| network = CBS
| released = 1997
}} Lakota Indians.  It also stars Patrick Bergin, Jean Louisa Kelly, Michael Greyeyes, and Rodney A. Grant.  The story is loosely based on the real Anna Morgan who was taken by Cheyenne Indians for approximately one year before being returned to her husband.   

==Plot== Lakota Indians led by Tokalah (Michael Greyeyes) attack a wagon train headed to Fort Hays, Kansas, where they kill everyone in the wagons. However, strangely, when Tokalah comes to the remaining wagon that is driven by Anna Brewster (Janine Turner), he lets her and her pregnant friend go.

Anna makes it to Fort Hays and is met by her brother Stewart (Ted Shackelford), a pastor, who has arranged for her to marry Daniel Morgan (Patrick Bergin). Not long after her arranged marriage, Anna is being visited by Sarah (Jean Louisa Kelly) when Tokalah and other Lakota men break into her home and take both women. Daniel and Stewart try to go after them, but are unsuccessful in following their trail, as is Captain Robert Farnsworth (Dennis Weaver) who later joins the hunt.

At the Lakota camp, Sarah resists blending in while Anna takes more readily to their culture as the months pass. She and Tokalah, who begins to learn English from young Cetan (William Lightning) who is half white from his deceased mother, grow closer and Anna realizes she is falling in love with him. Not wanting to commit adultery, she decides to escape with Sarah and they steal horses one night. However, Tokalah and his men catch up with them and Tokalah sends the men back with Sarah. He and Anna argue, with Anna saying that she never asked to be taken from her husband, but Tokalah tells her that she did ask claiming that he heard her. Giving into her feelings for him, they spend a passionate night together on the plains.

Back at Fort Hays, General Custer (William Shockley) arrives to take over the search using his favorite scout, a Native American named Bloody Knife. He, Farnsworth, Daniel and Stewart make contact with Tokalah and Chief Luta (Saginaw Grant), who tell him that they can have Sarah but not Anna. Daniel realizes that Tokalah has "lain with" Anna and Custer puts Luta under arrest, saying he will be hanged if both women are not returned. The next day, the Lakota return where a battle ensures, which ends poorly and with a wounded Tokalah returning to camp. Soon after this, Sarah leaves and rides off to Custers encampment. Anna tells Tokalah she must leave as well in order to spare Luta, where he tells that they belong together as he had seen her before in a vision, however, she still leaves to Custers encampment and Luta is released.

Anna returns to her husband and their home, where Sarah soon visits and tells Anna that she must leave her husband as she now knows where she belongs. After giving Daniel one last night together, Anna rides off to the Lakota camp only to find it destroyed. She then sees Tokalah still on the land, mourning. He assumes she is another vision, but she touches his face and confesses her love for him and they embrace.

==Cast==
{| class="wikitable" style="width:50%"
! Name || Played by
|-
| Anna Brewster || Janine Turner
|-
| Daniel Morgan || Patrick Bergin
|-
| Sarah White || Jean Louisa Kelly
|-
| Tokalah || Michael Greyeyes
|-
| Waxanhdo || Rodney A Grant
|- William Shockley
|-
| Chief Luta || Saginaw Grant
|-
| Cetan || William Lightning
|-
| Stewart Brewster|| Ted Shackelford
|-
| Captain Robert Farnsworth|| Dennis Weaver
|-
| Kimimila || Selina Jayne
|-
| Manipi || Kateri Walker
|-
|}

==The Real Anna Brewster Morgan==
According to historical reports, the real Anna Brewster was born December 10, 1844, and eventually went to live with her brother Daniel, who arranged for her to marry a man named James Morgan in 1868. One month later, a band of Sioux Indians raided their homestead and shot James and took Anna captive.  The Sioux, traveling back to their village, met a band of Cheyenne Indians who had already captured a woman named Sarah White.

Anna was traded to the Cheyenne and went to live with them. She eventually married an Indian Chief whose name is not recorded but was found by General George Custer approximately a year after her capture, by which time she was pregnant with the Chiefs child. Returned to James Morgan, she gave birth to a half-Indian son, Ira, but the boy fell ill and died around age 2, just ten days after the birth of the Morgans daughter, Mary.

Anna bore James two more sons, Claud and Glen, but their marriage was not a happy one; Anna is reportedly quoted as saying, "I often wished they had never found me."   Eventually Anna left James and went to live with her brother Daniel.  James divorced her, and Anna lived under a lifelong stigma because of what happened to her, causing her to be admitted to a mental hospital later in life where she died in 1902.  She was buried next to her son Ira. 

==External links==
*  

==References==
 

 
 
 