Konopielka
{{Infobox film
| name     = Konopielka
| image    =
| director = Witold Leszczyński
| producer = 
| writer   = Edward Redliński (novel)
| starring = Krzysztof Majchrzak   Anna Seniuk
| cinematography =
| music = Wojciech Karolak
| country = Poland
| language = Polish
| runtime = 92 min
| released =  
}}
Konopielka is a 1982 Polish drama film directed by Witold Leszczyński.

== Cast ==
* Krzysztof Majchrzak − Kaziuk Bartoszewicz
* Anna Seniuk − Handzia
* Joanna Sienkiewicz − Nauczycielka Jola
* Tomasz Jarosiński − Ziutek
* Jerzy Block − Józef
* Franciszek Pieczka − Dziad / Bóg
* Jan Paweł Kruk − Dunaj
* Tadeusz Wojtych − Domin
* Sylwester Maciejewski − Żołnierz w motorówce

== External links ==
* 

 
 


 