A Place at the Table
 
{{Infobox film
| name           = A Place at the Table
| image          = A Place at the Table Poster.jpeg
| caption        = Films Official Poster
| director       = Kristi Jacobson Lori Silverbush
| producer       = Tom Colicchio Julie Goldman Ryan Harrington Jeffrey Skoll Diane Weyermann Christina Weiss Lurie Jeffrey Lurie
| writer         = 
| starring       = Jeff Bridges Raj Patel Tom Colicchio
| music          = The Civil Wars T Bone Burnett
| cinematography = 
| editing        = Madeleine Gavin Jean Tsien, A.C.E. Andrea B. Scott
| distributor    = Magnolia Pictures
| studio         = Participant Media
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  $231,378 
}}
A Place at the Table is a 2012 documentary film directed by Kristi Jacobson and Lori Silverbush, with appearances by Jeff Bridges, Raj Patel, and chef Tom Colicchio. The film, concerning hunger in the United States, was released theatrically in the United States on March 1, 2013.    

==Production background==
Participant Media is the entertainment company behind the film, and it is distributed by Magnolia Pictures. The Participant/Magnolia team is also responsible for the film Food, Inc. (2008).  The film was originally titled Finding North but the name was later changed to A Place at the Table.

==Synopsis==
As of 2012, about 50 million Americans were food insecure. This was approximately 1 in 6 of the overall population, with the proportion of children facing food insecurity even higher at about 1 in 4. One in every two children receive federal food assistance. {{cite book
|editor= William A Dando
|title=Food and Famine in the 21st Century chapter =  Food Assistance Landscapes in the United States by Andrew Walters 
|year=2012
|isbn= 1598847309 publisher = ABC-CLIO}}  {{Cite web
|url= http://www.guardian.co.uk/society/patrick-butler-cuts-blog/2013/may/08/food-poverty-american-way-foodbanks-charity
|title= Food poverty: The American Way
|publisher= The Guardian
 |author=  Patrick Butler date = 2013-05-08
|accessdate=2013-06-09}}
  The film sees directors Kristi Jacobson and Lori Silverbush examine the issue of hunger in America, largely through the stories of three people suffering from food insecurity: 
*Barbie, a single Philadelphia mother who grew up in poverty and is trying to provide a better life for her two children; 
*Rosie, a Colorado fifth-grader who often has to depend on friends and neighbors to feed her and has trouble concentrating in school; and 
*Tremonica, a Mississippi second-grader whose asthma and health problems are exacerbated by the largely-empty calories her hard-working mother can afford.
Other Americans struggling with hunger are also featured, including a cop whose monthly paychecks only leaves him enough money to buy food for two weeks, forcing him to use a food bank. 
A Place at the Table shows how hunger poses serious economic, social, and cultural implications for the United States, and that the problem can be solved once and for all, if the American public decides – as they have in the past – that making healthy food available and affordable is in everyones best interest.

==Book release==
There is also a companion book titled A Place at the Table: The Crisis of 49 Million Hungry Americans and How to Solve It, edited by Peter Pringle and published by Public Affairs. The book features contributions from Jeff Bridges, Ken Cook, Marion Nestle, Bill Shore, Joel Berg, Robert Egger, Janet Poppendieck, David Beckmann, Mariana Chilton, Tom Colicchio, Jennifer Harris, Andy Fisher, Kelly Meyer and directors Kristi Jacobson and Lori Silverbush. 

==Awards==
The film was nominated for Grand Jury Prize at the 2012 Sundance Film Festival.  
 IDA Documentary Awards, the film received the Pare Lorentz Award, which recognizes films for model filmmaking while focusing on the use of the natural environment, and justice for all and the illumination of pressing social problems. 

==Critical reception==

The film has received positive reviews. As of June 2013, it has an 89% approval ratings from 56 professional reviews aggregrated by Rotten Tomatoes.  The following are a few examples of critical response to the film:

*"A Place at the Table forcefully makes the case that hunger has serious economic, social and cultural implications for the nation." - Julie Makinen, Los Angeles Times 

*"The film explains with devastating simplicity why so many go hungry in a country with more than enough food to go round." - London Evening Standard 

*"Beautifully shot and edited. The craft is of a very high level." - Toronto HotDocs Film Festival "Must See List" 

*"A Place at the Table is an engaging and enraging movie that will enlist supporters for its cause." - Variety (magazine)|Variety 

==See also==
 
* 
*Bananas!*
*Chew on This, an adaptation of Fast Food Nation for younger readers  
*Deconstructing Dinner
*Eating Animals
*Fast Food Nation
*Food Matters
*Food, Inc.
*Forks Over Knives
*Fresh (2009 film)|Fresh King Corn
*   Our Daily Bread
*Super Size Me
*Taste the Waste
*The Future of Food
*The Jungle
*The Omnivores Dilemma
*We Feed the World
 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 