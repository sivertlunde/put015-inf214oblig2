Dariya Dil
{{Infobox film
| name           = Dariya Dil
| image          = Dariya Dil.jpg
| image_size     = 
| caption        = Promotional Poster
| director       = K. Ravi Shankar
| producer       = Vimal Kumar, Manisha Vimal 
| writer         = Gyandev Agnihotri, Amrit Aryan 
| narrator       =  Raj Kiran. 
| music          = Rajesh Roshan
| cinematography = K.V. Ramanna
| editing        = Prakash Dave
| distributor    = Shivam Chitrya
| released       = 8 January 1988
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Raj Kiran. The film was released on 8 January 1988 in India.

The film has gained renewed interest as one of its song and dance sequences, in which Govindas character Ravi appears dressed as Superman and Kimi Katkars character Radha appears dressed as Spiderwoman, has become a widely seen video on YouTube  with almost 10 million views as of March 2013. DARIYA DIL was a box office hit and it remains for its family drama story and superb acting of Kader Khan.

==Cast== Govinda as  Ravi 
*Kimi Katkar as  Radha 
*Roshni as  Dolly Gogi  Raj Kiran as  Vijay 
*Shoma Anand as  Sapna 
*G. Asrani as  Suleiman (as Asrani) 
*Gulshan Grover as  Gulu Gogi 
*Seema Deo as  Laxmi 
*Shashi Puri as  Ajay 
*Shakti Kapoor as  D.O. Gogi 
*Kader Khan as  Dhaniram (as Kadar Khan) 
*Renu Joshi   
*Ashok Saxena   
*Farita Boyce  
*Jugnu  (as Jugune) 
*Vinod Tripathi   
*Jayshree T. as  Mrs. Maniram 
*Yunus Parvez   
*Raja Duggal as  Suresh 
*Mushtaq Merchant   
*Vikas Anand as  Radhas Brother 
*Bharat Bhushan as  Beggar

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Lyric
|-
| 1
| "Barse Re Sawan"
| Mohammed Aziz, Sadhana Sargam and Chorus
| Indeewar
|-
| 2
| "Patthar Kya Marte Ho"
| Mohammed Aziz, Sapna Mukherjee and Chorus
| Indeewar
|-
| 3
| "Dariya Dil" (Title)
| Shabbir Kumar and Chorus
| Indeewar
|-
| 4
| "Tu Mera Superman"
| Mohammed Aziz, Sadhana Sargam and Chorus
| Indeewar
|-
| 5
| "Woh Kehte Hain Hum Se"
| Nitin Mukesh
| Vitthalbhai Patel
|-
| 6
| "Dariya Dil" (Sad)
| Shabbir Kumar
| Indeewar
|}

==Footnotes==
 

==External links==
* 

 
 
 
 
 


 