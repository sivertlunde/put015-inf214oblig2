Incident at Restigouche
Incident at Restigouche is a 1984 documentary film by Alanis Obomsawin, chronicling a series of two raids on the Listuguj Migmaq First Nation (Restigouche) by the Sûreté du Québec in 1981, as part of the efforts of the Quebec government to impose new restrictions on Native salmon fishermen.   

==Production history== Campbellton newspaper. To add insult to injury, when Obomsawin asked the NFB for permission to shoot more interviews, including with then-Minister of Fisheries Lucien Lessard, who had ordered the raids, the Abenaki filmmaker was informed by NFB management that she should not interview white people for her film, only natives. 

Obomsawin disregarded this order and interviewed Lessard anyway, in an exchange that would serve as a key part of the film. When confronted by NFB management about her disobedience, she told them that no one would tell her who to interview and that while white people had interviewed Natives for years, the opposite had never been true. NFB management relented and Obomsawin was free to complete her film with relatively little interference—an important step forward for her in her increasing autonomy within the NFB. 

==Interview with Lessard==
In Obomsawins pivotal exchange with Lessard, a member of the sovereigntist Parti Québécois, she confronts Lessard with the charge that a movement such as his that favours national self-determination for the Québécois (word)|Québécois was at the same time suppressing those rights for First Nations.     In particular, she calls Lessard to task for a comment he made to the Listuguj chief, when he said "You cannot ask for sovereignty because to have sovereignty one must have ones own culture, language and land." By the end of the film, Lessard offers a personal apology for any harm his actions may have caused the people of Restigouche.   

Obomsawin later said that she respected Lessard for his decision to speak directly and honestly with her, even if she strongly disagreed with his actions:

  }}

==Music==
In addition to documentary material and interviews, the film also includes a musical segment in which found film on the life cycle of salmon is accompanied by a song by Willie Dunn.   

==Legacy==
Mikmaq director Jeff Barnaby, who witnessed the 1981 raids first-hand, has stated that Incident at Restigouche was an inspirational film for him:

 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 