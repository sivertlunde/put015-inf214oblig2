Gangavataran
{{Infobox film
| name           = Gangavataran
| image          =
| alt            =  
| caption        =
| director       = Dadasaheb Phalke
| producer       =
| writer         = Dadasaheb Phalke
| based on       =  
| starring       = {{Plainlist|
* Chitnis
* Suresh Pardesi
}}
| music          = Vishwanathbuwa Jadhav
| cinematography = Vasudev Karnataki
| editing        =
| studio         = Kolhapur Movietone
| distributor    =
| released       =  
| runtime        =
| country        = British Raj
| language       = {{Flatlist| Marathi
* Hindi
}}
| budget         =
| gross          =
}} 1937 Indian film by Dadasaheb Phalke, who is known as the "father of Indian cinema". It was the first sound film    and the last film to be directed by Phalke.    When Phalke directed this film, he was 67 years old. He directed Gangavataran on behalf of Kolhapur Movietone. 

==Plot==
Gangavataran is based on the tales of the Puranas, a genre of important Hindu, Jain and Buddhist religious texts. The film used special effects to show mythological miracles and fantasy scenes, credited to Babaraya Phalke, son of Dadasaheb. Gangavataran depicted the Hindu deity Shiva, and the divine sage from the Vaisnava tradition, Narada, played by Chitnis and Suresh Pardesi respectively.
 river Gangess descent to Earth. For absolution of his ancestors sins Bhagiratha vows to bring the heavenly river (Ganga) to the Earth. Pleased with his tapasya, Lord Brahma agrees on sending Ganga. But He asks Bhagiratha to pursue Lord Shiva, as only Shiva could break Gangas fall. Proud with her heavenly presence, Ganga agreed to descend only because of Brahmas orders. Her pride made her believe that her fall would only destroy the Earth. But then Shiva trapped her tight in His hair and only let her go when she asked for forgiveness. Her gentle flow after release then absolved Bhagirathas ancestors.

==Production== Royal Opera House in Mumbai. Gangavataran was the first Indian sound film to be screened at the Royal Opera House.   

==Analysis==
Gangavataran was a mythological film.    When Phalke directed this film, other filmmakers such as V. Shantaram were directing films with social reform as a thematic element.    Gangavataran was a flop in the box office.  Encyclopaedia of Hindi cinema describes Gangavataran as the "last desperate attempt" by Phalke to direct a film at a time when big-budget Hindi films were flourishing which were characterized by "rural romances with easy-to-follow dialogues, catchy songs, folk dances, dramas of misunderstanding" and the first pioneers of the Hindi film industry were quickly fading away from public memory.   

==References==
 

==External links==
*  

 
 
 
 
 