Janssens tegen Peeters
{{Infobox film
| name           = Janssens tegen Peeters
| image          = 
| image size     =
| caption        =
| director       = Jan Vanderheyden
| producer       = Jan Vanderheyden 
| writer         = Hendrik Caspeele   Edith Kiel   Jan Vanderheyden
| narrator       =
| starring       = Charles Janssens   Louisa Lausanne   Jef Bruyninckx   Toontje Janssens
| music          = Gerard Horens   J. Antoon Zwijsen
| cinematography = Ernst Mühlrad
| editing        = Edith Kiel
| studio         = Jan Vanderheyden-Film
| released       = 1939
| runtime        =
| country        = Belgium Flemish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Belgian comedy film directed by Jan Vanderheyden and starring Charles Janssens, Louisa Lausanne and Jef Bruyninckx. A sequel Janssens en Peeters dikke vrienden was released the following year.

==Cast==
* Charles Janssens - Tist Peeters  
* Louisa Lausanne - Melanie Peeters
* Jef Bruyninckx - Tony Peeters  
* Toontje Janssens - Grootvader Peeters  
* Pol Polus - Stan Janssens 
* Nini De Boël - Juliette Janssens  
* Martha Dua - Simonne Janssens  
* Louisa Colpeyn - Wieske Peeters  
* Gaston Smet - Fred Janssens  
* Nand Buyl - Polleke
* Serre Van Eeckhoudt - Oom Frans  
* Helena Haak - Tante Net  
* Fred Engelen - Zangleraar  
* Mitje Peenen - Marie meid van Janssens  
* Lily Vernon - Juffrouw Anna

==External links==
* 

 

 
 
 
 
 