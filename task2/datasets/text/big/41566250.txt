Wedding Rings (film)
{{Infobox film
| name =  Wedding Rings 
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Robert North Ray Harris
| narrator = Lois Wilson   Olive Borden   Hallam Cooley
| music = 
| cinematography = Ernest Haller 
| editing = 
| studio = First National Pictures
| distributor = Warner Brothers
| released = December 29, 1929 
| runtime = 74 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Lois Wilson and Olive Borden. It is considered a lost film.

==Plot==
Eve Quinn, a shallow but attractive debutante, makes a practice of leading men on, then cooly casting them aside for new conquests. She openly boasts that she would find pleasure in taking a man from her sister, Cornelia, who is an art student. When Cornelia falls in love with wealthy clubman Lewis Dike, Eve succeeds in vamping and capturing him; broken-hearted when they marry, Cornelia deliberately introduces Eve to Wilfred Meadows, a playboy with whom she begins a flirtation. Dike soon tires of the modernistic furnishings of their home and the jazz-mad parasites who frequent his drawing room, and he is refreshed by visits to Cornelia. When Dike accidentally learns of Eves liaison with Wilfred, he realizes his error and is reunited with Cornelia.

==Cast==
*  H.B. Warner as Lewis Dike  Lois Wilson as Cornelia Quinn 
* Olive Borden as Eve Quinn 
* Hallam Cooley as Wilfred Meadows  James Ford as Tim Hazleton 
* Kathlyn Williams as Agatha 
* Aileen Manning as Ester Quinn

==References==
 

==Bibliography==
* Marshall, Wendy L. William Beaudine: From Silents to Television. Scarecrow Press, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 