Close-Up (1990 film)
{{Infobox film
| name = Close-Up
| image =Close Up DVD cover.jpg
| caption=DVD cover
| film name = کلوزآپ ، نمای نزدیک
| director = Abbas Kiarostami
| producer = Ali Reza Zarrin
| writer = Abbas Kiarostami
| starring = Hossain Sabzian Mohsen Makhmalbaf
| cinematography = Ali Reza Zarrindast
| editing = Abbas Kiarostami Kanoon
| distributor = Celluloid Dreams
| released = 1990
| runtime = 98 min.
| country = Iran Persian
| budget = 
| gross = 
}}
 1990 Iranian docufiction written, directed and edited by Abbas Kiarostami. The film tells the story of the real-life trial of a man who impersonated film-maker Mohsen Makhmalbaf, conning a family into believing they would star in his new film. It features the people involved, acting as themselves. A film about human identity, it helped to increase recognition of Kiarostami in the Western world|West. In the 2012 Sight & Sound poll, it was voted by critics onto "The Top 50 Greatest Films of All Time" list. 

==Plot==
 
Hossain Sabzian is a film lover and huge fan of popular Iranian director Mohsen Makhmalbaf. Sabzian is riding the bus one day reading a copy of the novel The Cyclist when he meets Mrs. Ahankhah, a fan of the film. Sabzian tells her that he is Makhmalbaf, the author of the book and film. Shes a bit surprised that a famous director is riding public transportation, but Sabzian explains that this is how he finds his subjects for film and that art must spring from life. Posing as Makhmalbaf, Sabzian visits the Ahankhah family several times over the next couple of weeks. He flatters them by saying he wants to use their house for his next film and their sons as his actors. He even obtains a substantial amount of money from them, ostensibly to prepare for the film. Mr. Ahankhah has his suspicions though, especially when a magazine photo shows a younger darker-haired Makhmalbaf. He invites an ambitious journalist friend (Hossain Farazmand) over, who confirms that Sabzian is indeed an impostor. The police come to arrest Sabzian, while Farazmand takes several pictures for his upcoming article: "Bogus Makhmalbaf Arrested." Kiarostami intersperses these scenes throughout the film, which does not progress chronologically. They are re-enactments.

==Cast==
* Hossain Sabzian as Himself
* Mohsen Makhmalbaf as Himself
* Abbas Kiarostami as Himself
* Abolfazl Ahankhah as Himself
* Mehrdad Ahankhah as Himself
* Monoochehr Ahankhah as Himself
* Mahrokh Ahankhah as Herself
* Haj Ali Reza Ahmadi as Himself, the Judge
* Nayer Mohseni Zonoozi as Herself
* Ahmad Reza Moayed Mohseni as Himself, a family friend
* Hossain Farazmand as Himself, a reporter
* Hooshang Shamaei as Himself, a Taxi Driver
* Mohammad Ali Barrati as Himself, a Soldier
* Davood Goodarzi as Himself, a Sergeant
* Hassan Komaili as Himself, a Court Recorder
* Davood Mohabbat as Himself, a Court Recorder

==Production==
Close-Up is based on real events that occurred in Northern Tehran in the late 1980s. Kiarostami first heard about Sabzian in 1989 after reading about the incident in an article in the Iranian magazine Sorush by journalist Hassan Farazmand. Kiarostami immediately suspended work on the film project that he was in pre-production of and began making a documentary on Sabzian. Kiarostami was allowed to film Sabzians trial and also got Sabzian, the Ahankhahs and Farazmand to agree to participate in the film and to re-enact incidents from the past. Kiarostami also arranged for Mohsen Makhmalbaf to meet Sabzian and help facilitate forgiveness between Sabzian and the Ahankhahs. 

==Critical Reception== The Top 50 Greatest Films of All Time. New York Times film critic Stephen Holden called the film brilliant and that its "radically drab cinema-verite style that helps blur any difference between what is real and what is reconstructed." Holden, Stephen, "Close Up (1990)
FILM REVIEW; The Pathos Of Deceit By a Victim Of Longing." The New York Times, December 31, 1999, Accessed on November 17, 2012.  Los Angeles Times critic Dennis Lim called the film eloquent and direct and that it provided "a window into the psyche of a complicated man and into the social and cultural reality of Iran." 

==Influence== FIPRESCI Prize – Special Mention.  

Nanni Morettis 1996 Italian short film Opening Day of Close-Up follows a theater owner as he prepares to show Kiarostamis film at his independent cinema.

Marcus Söderlunds 2007 music video for Swedish duo The Tough Alliances "A New Chance" pays homage to Kiarostamis film with an almost shot-for-shot reproduction of a scene following two characters on a motorcycle.  

==Awards== Montreal International Festival of New Cinema and Video: Quebec Film Critics Award
*1992: International Istanbul Film Festival: FIPRESCI Prize

==See also==
*Docufiction
*List of docufiction films
*Metafilm

==References==
 

==Bibliography==
*Godfrey Cheshire, Confessions of a Sin-ephile: Close Up, Cinema Scope, no. 2 (Winter 2000), pp.&nbsp;3–8 Law and Literature, vol. 23, no. 2 (Summer 2011), pp.&nbsp;173–194

==External links==
* 
*  
* 
*  at the   list
* 
* 
*  to The Guardian, Thursday 28 April 2005

 

 
 
 
 
 
 
 
 
 