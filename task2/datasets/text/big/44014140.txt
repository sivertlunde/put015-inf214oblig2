Kudumbam Namukku Sreekovil
{{Infobox film
| name = Kudumbam Namukku Sreekovil
| image =
| caption = Hariharan
| producer = TE Vasudevan
| writer = S. L. Puram Sadanandan
| screenplay =
| starring = Prem Nazir Adoor Bhasi Jose Prakash Sankaradi
| music = V. Dakshinamoorthy
| cinematography =
| editing =
| studio = Jaya Maruthi
| distributor = Jaya Maruthi
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Hariharan and produced by TE Vasudevan. The film stars Prem Nazir, Adoor Bhasi, Jose Prakash and Sankaradi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
  
*Prem Nazir 
*Adoor Bhasi 
*Jose Prakash 
*Sankaradi 
*Sukumaran 
*Unnimary 
*Janardanan 
*K. R. Vijaya  Meena  Master Raghu as Raghu
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Daivam Bhoomiyil || K. J. Yesudas, Jayachandran || Mankombu Gopalakrishnan || 
|- 
| 2 || Ettumaanoorambalathin || Jayachandran, Ambili || Mankombu Gopalakrishnan || 
|- 
| 3 || Innolam Kaanatha || K. J. Yesudas, Kalyani Menon || Mankombu Gopalakrishnan || 
|- 
| 4 || Omkaarapporulil || K. J. Yesudas || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 


 