Proprietors: Kammath & Kammath
 
 
{{Infobox film
| name           = Proprietors: Kammath & Kammath 
| image          = Kammath and Kammath - Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Thomson K. Thomas
| producer       = Anto Joseph
| writer         = Udayakrishna-Siby K. Thomas Dileep Narain Narain Karthika Nair Rima Kallingal
| music          = M. Jayachandran
| cinematography = Anil Nair
| editing        = Mahesh Narayanan
| studio         = Anto Joseph Film Company
| distributor    = Ann Mega Media Release
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Malayalam
| budget       =  
| boxoffice       =  
}}
 Malayalam action comedy film, Dileep in Baburaj in supporting roles. Tamil film actor Dhanush has a cameo role in the film as himself.

== Plot ==

Kammath and Kammath is a duo in the hotel business. It includes Raja Raja Kammath (Mammootty) and Deva Raja Kammath (Dileep (actor)|Dileep). Sulaiman sahib (Rizabawa) is their rival. He runs a non-vegetarian hotel that is opposite a now defunct Brahmin vegetarian hotel. He has a target of acquiring this hotels land.

The Kammath brothers plan to inaugurate their new hotel in the building and land of the defunct hotel. On opening day, municipal secretary Mahlakshmi and ward counselor Sebastian Kuzhivelil are brought by Sulaiman Sahib to stop it from opening. They fail because the hotels name is now Kammath & Kammath and it has obtained a new license and ownership.

Sulaiman Sahib, with the help of a thief (Pathrose) arrests the elder Kammath, Raja Raja Kammath, who meets his brother Deva Raja Kammath who has already been arrested for selling liquor in their new hotel.

The Kammaths threaten Pathrose in the police station and the truth is revealed that he has thrown stones at Mahalakshmis house that neighbours the Kammaths house at the request of Sulaiman Sahib and Sebastian Kuzhivelil. The hotel security cameras reveal that people sent by Sulaiman Sahib brought the liquor to the hotel to implicate the Younger Kammath.

Later, both Mahalakshmi and Sulaiman Sahib are convinced by the elder Kammath that they are not problem makers and both become friends with them.

The Kammaths open their new hotel in Coimbatore with superstar Dhanush as inagurator. One night, Sunnichan, attacks Mahalakshmi, asking about his sister, while she was coming back to Palakkad from Coimbatore. The elder Kammath saves her from Sunnichan by sending his driver+fighter Gopi to deal with him.

On this journey Kammath and Mahalakshmi go to a restaurant. There Kammath meets his old wife with her new husband.

While continuing the journey a flashback reveals that she was his wife, but she had a different nature than the Kammath family had expected. One day when Kammaths father suffers from chest pain, his mother asks her to drive him to hospital. She refuses as she had to deal with Mehnthi. The father dies in hospital. So they divorce. Hearing this story Mahalakshmi empathizes with him.

The next day Kammath is invited by Mahalakshmi to talk about a problem. She introduces her sister Surekha to Kammath and says Surekha is scared that somebody is following her. The next day Kammath fights with an helmeted person who comes near her. When the helmet is removed Kammath sees his brother Deva. He becomes sad as he realises that he has beaten up his own brother. When they all leave another person with a helmet who was following her arrives.

Raja Raja Kammath proceeds with the idea of his brother marrying Suekha. Mahalakshmi tells them that Surekha is actually her deceased brothers wife and a Christian instead of a Hindu.

Surekhas history is revealed by Mahalakshmi. Her brother Suresh had married her years ago despite opposition from her family. On the first night of marriage Suresh is killed by Vikram her familys henchman. He is Surekhas real stalker. The story reveals that Sunnichan is her brother.

Even after knowing this story Deva Raja Kammath wishes to marry her. Later at her home, the Kammaths realise to their shock that Surekha cannot speak. Deva still wants to marry her.

On the night before the wedding, Deva is accosted by Vikram and Surekhas brothers. He escapes to a godown where the elder Kammath awaits. A fight ensues.

The next day the villains are handed over to the police at Surekhas home and Deva marries Surekha. To everyones surprise Mahalakshmi is married by the groom found by her deceased brother Suresh.

== Cast ==
{{columns-list|3|
* Mammootty as Raja Raja Kammath Dileep as Deva Raja Kammath Baburaj as Driver Gopi Narain as Suresh IRS
* Karthika Nair as Surekha
* Rima Kallingal as Mahalakshmi
* Riza Bava as Sulaiman Sahib
* Suraj Venjaramoodu as Sebastien Kuzhiveli
* Sukumari Santhosh
* Spadikam George
* Kalabhavan Shajon as Pathrose Abu Salim Janardhanan
* Shiju as Sunnichan
* Ambika Mohan as Maheshwari
* Bindu Ramakrishnan
* Thesni Khan Rajalakshmi
* Joju George
* Rajeev Parameshwar
* Dhanush in a cameo appearance as himself
* Vishnupriya (actress)
* Deepika Mohan
}}

== Production ==
 Dileep to replace him.  Kunchacko Boban had been approached for the role of Income tax officer, but he opted out, again due to schedule conflicts.  Dhanush was chosen to play a star who comes to Kerala to inaugurate the hotel. 

Scenes from the film were shot in Kochi.   

==Release==
The film was shown in additional theatres where Vishwaroopam had been banned from being released. 

== Critical reception ==

The movie is an above average hit at Kerala box office.

Paresh C Palicha of Rediff.com rated the film 2/5 describing it as an "atrocity on the viewer." 

IndiaGlitz.com gave 6.25/10 for the movie and stated "These Kammath brothers are strictly for those who relish masala entertainers" but it gave a positive review of the songs.   

Sify.com gave the overall summary of the film as "tedious" and their review says that "only Baburaj manages to make the viewers laugh" and that "There is no credible storyline that is worth mentioning and the script lacks any imagination or depth."   

The Times of India gave a rating of 3/5 for the movie, stating "Mammootty and Dileep, gifted actors they are, put together their best to salvage what can be called as a sloppy narrative strewn with drab characters."   

Dalton L of Deccan Chronicle says "Rather than preparing an action-romcom out of leftovers, the director ought to have demanded an original recipe and organic vegetables. It’s very sad to see the great Mammootty unendingly swimming in stale soup." 

==Soundtrack==

{| class="wikitable"
|-
! Song !! Length !! Singer(s) !! Picturization
|-
| Dosa Nalloru Dosa ||  || Shankar Mahadevan, M. Jayachandran, Nikhil Raj ||
|-
| Ninte Pinnale ||  || Anwar Saadath ||
|-
| Coimbatore Naatile ||  || Haricharan, Madhu Balakrishnan, Vijay Yesudas ||
|-
| Kattadi Kattadi ||  || Rajesh Krishnan, Sangeetha Sreekanth ||
|}

==References==
 

==External links==
*  

 
 
 
 
 
 