Wormhole Chasers
 
{{Infobox Film
| name           = Wormhole Chasers
| image          = 
| image_size     = 
| caption        = 
| director       = Gregory Zymet 
| producer       = Shannon Kendall Heidi Roller George Vincent
| writer         = Gregory Zymet 
| narrator       = 
| starring       = Nancy Guerriero A.K. Murtadha Travis McElroy Kristen Lorenz Del Gist
| music          = Gregory Zymet   
| editing        = Sean Olson    
| distributor    = 
| released       = 2007
| runtime        = 4 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Wormhole Chasers is a 2007 four-minute short film about a woman who opens her door and finds several people there because a wormhole is about to open in the corner of her apartment.

==Reception== Gilliamesque universe thats slightly to one side of our own."  A Film Guru review says, "Wormhole Chasers is a fun ride, leaving the viewer wanting to hear more about these people and what strange trips they have made. If any short film deserved more time, this is it. While the short form certainly sets up the story for a clever ending, it only whets the appetite."  A Micro Film Maker review says, "The films pace moved along quickly, but not so fast that the audience couldnt keep up with what was going on. The editing was very well done; it was brisk, but easy to follow, and not too choppy or jerky. The ending is a bit ambiguous, but in a sense, its supposed to be, and fits the overall feel of the project." 

==References==
 

 
 

 