Sinners and Saints (2010 film)
 
{{Infobox film
| name           = Sinners and Saints
| image          = Sinners and Saints.jpg
| caption        = Film poster
| director       = William Kaufman
| executive producer       = Robert Ford
| producer       = Mark Clark Ron Balicki Colby Mitchell
| writer         = William Kaufman Jay Moses Kevin Phillips Costas Mandylor Sean Patrick Flanery Bas Rutten Method Man Kim Coates Tom Berenger
| music          =  Johnny Strong
| cinematography = Mark Rutledge
| editing        = Russell White
| studio         = Noire Blanc Films
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 104 minutes 
| country        = United States
| language       = English
}} Kevin Phillips, Costas Mandylor, Sean Patrick Flanery, Bas Rutten, Method Man, Kim Coates and Tom Berenger. The film is set in the dark underbelly of New Orleans. It was released in North America on DVD and Blu-ray Disc|Blu-ray on January 10, 2012.   

==Plot==
Detective Sean Riley (Johnny Strong) is emotionally destroyed after the death of his son and the abandonment of his wife. After the death of his partner, Det. Dave Besson (Kim Coates), Rileys superior, Captain Trahan (Tom Berenger) assigns Riley to investigate a series of murders involving victims burned several times over. One of the victims is the younger brother of infamous gangster Weddo (Method Man), which sparks a gang war in the Big Easy.
 Kevin Phillips), a stable family man, to continue his investigation. They stumble into a group of mercenaries in the act of burning another victim, and a shootout ensues. 

It is revealed that a man named Raymond Crowe (Costas Mandylor) hired the mercenaries. As the investigation continues, Riley discovers that Colin is in possession of incriminating evidence against Spartan: a video of the same mercenaries from the shootout executing innocents while on assignment. Riley and Ganz find Colin at a decrepit part of the city, but too late. The mercenaries, now revealed to be Spartans, attack. A badly wounded Colin takes his own life, blowing up the house he is in so that Riley and Ganz can escape.

Trahan tells Riley that the city wants to blame Riley for the disastrous results of the investigation. Riley decides to take matters into his own hands, stockpiling weapons in his home. Trahan comes over to try to talk him out of it, but is ambushed and killed by Dekker (Bas Rutten), a leader of the mercenaries. Dekker tries to kill Riley, but Riley manages to kill him. He discovers that Crowe has kidnapped Ganz.

Riley breaks into Crowes stronghold and kills the remaining mercenaries. Crowe manages to escape, leading to a street chase. Crowe reveals his intentions and manages to shoot Riley, but Riley shoots him down on the street. Riley is about to kill Crowe, and Ganz shouts at him to do it, but Riley decides not kill him; instead, Weddo and his gang drive up and take Crowe with them, presumably to kill him themselves. Later, Riley visits Ganzs childs birthday party. Afterward, he visits the cemetery where his son is buried.

==Cast==
*Johnny Strong as Detective Sean Riley Kevin Phillips as Detective Will Ganz
*Costas Mandylor as Raymond Crowe
*Sean Patrick Flanery as Colin
*Bas Rutten as Dekker
*Method Man as Weddo
*Kim Coates as Det. Dave Besson
*Tom Berenger as Captain Trahan
*Jürgen Prochnow as Mr. Rhykin
*Jolene Blalock as Stacy
*Louis Mandylor as Cole
*Brooklyn Sudano as Beth Ganz

== References ==
 

== External links ==
*   "Winner, Best Picture: Sinners and Saints" ***2012 Action On Film Festival***
*   "Winner, Breakout Best Action Film of the Year: Sinners and Saints", ***2012 Action On Film Festival***
*   "Winner, Breakout Performer of the Year: Johnny Strong", ***2012 Action On Film Festival***
*   "Four Stars!", Filmthreat.com
*   Aint It Cool News,Harry Knowles
*  
*  

 
 
 
 
 
 