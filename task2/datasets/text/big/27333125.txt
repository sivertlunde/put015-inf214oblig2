The Horror at 37,000 Feet
{{Infobox Film
| name           = The Horror at 37,000 Feet
| image          = The Horror at 37,000 Feet (1973).jpg
| image_size     = 
| caption        = 
| director       = David Lowell Rich
| producer       = Anthony Wilson
| writer         = Ronald Austin  James D. Buchanan
| narrator       = 
| starring       = Chuck Connors William Shatner Buddy Ebsen Roy Thinnes Jane Merrow Russell Johnson Tammy Grimes
| music          = Morton Stevens
| cinematography = Earl Rath
| editing        = Bud S. Isaacs
| studio         = Columbia Broadcasting System (CBS)
| distributor    = CBS
| released       =   (original broadcast date)
| runtime        = 73 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American Horror horror television movie made for CBS Television by David Lowell Rich.  The film first aired in 1973. In the movie, demonic forces terrorize the passengers on a Boeing 747 en route from London to New York. 
 Twilight Zone episode "Nightmare at 20,000 Feet," plays the lead role. Shatner described his characters demise in the movie as one of his "unique ways" of dying: "I get sucked out of an airplane while carrying a lit torch into the airliners baggage compartment to try to confront a druid ghost."  According to Shatner, many of his fans consider the movie the worst Shatner movie ever. 

==Plot==
On a Boeing 747 flight from London to Los Angeles piloted by Captain Ernie Slade (Chuck Connors), wealthy architect (Roy Thinnes) and his wife (Jane Merrow) have placed a precious artifact in the baggage hold of the airliner. The artifact is an ancient altar that holds a deadly secret. Aboard for the ill-fated trip is ex-priest Paul Kovalik (William Shatner) and millionaire Glenn Farlee (Buddy Ebsen). Soon after takeoff, crew and passengers alike face the supernatural horror that is unleashed from the baggage compartment – demons that are seeking revenge from being uprooted from their ancient home.

==Cast==
 
* Chuck Connors as Captain Ernie Slade
* Buddy Ebsen as Glenn Farlee
* William Shatner as Paul Kovalik
* Roy Thinnes as Alan ONeill
* Jane Merrow as Sheila ONeill
* Tammy Grimes as Mrs. Pinder
* Lynn Loring as Manya
* Paul Winfield as Dr. Enkalla
* France Nuyen as Annalik
* Will Hutchins as Steve Holcomb
* Darleen Carr as Margot
* Russell Johnson as Jim Hawley
* H.M. Wynant as Frank Driscoll
* Mia Bendixsen as  Jodi
 

==Production==
The Horror at 37,000 Feet was entirely shot on sound stages at the CBS Studio Center, Culver City, California. 

==Reception==
In a later review, critic Richard Scheib commented: "The Horror at 37,000 Feet is a silly film, although to its credit it and most of the principals do maintain a degree of intent gravity and at least treat the exercise seriously." 

==References==
Notes
 

Bibliography
 
* Roberts, Jerry. Encyclopedia of Television Film Directors. Lanham, Maryland: Scarecrow Press, 2009. ISBN 978-0-81086-138-1.
* Shatner, William and David Fisher. Up Till Now: The Autobiography. New York: MacMillan, 2009. ISBN 978-0-312-56163-5.
* Young, R.G. The Encyclopedia of Fantastic Film: Ali Baba to Zombies. Winona, Minnesota: Hal Leonard Corporation, 2000. ISBN 978-1-55783-269-6.
 
==External links==
* 

 

 
 
 
 
 
 
 
 