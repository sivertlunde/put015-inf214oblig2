Neither by Day Nor by Night
 
{{Infobox film
| name           = Neither by Day Nor by Night
| image          = Gisa W Slonim and Steven H Stern 1972.jpg
| caption        = Gisa W. Slonim and Steven H. Stern receive International Writers Guild award for Neither By Day or By Night, 1972
| director       = Steven Hilliard Stern
| asst director  = Eli Cohen
| producer       = Mordechai Slonim
| writer         = Abraham Raz, play Gisa W. Slonim, script Steven Hilliard Stern
| narrator       =  Eli Cohen
| music          = Vladimir Cosma
| cinematography = Amnon Salomon
| editing        = Alain Jakubowicz
| distributor    = Steve Broidy
| released       =  
| runtime        = 85 minutes
| country        = Israel United States
| language       = English
| budget         = 
}}

Neither by Day Nor by Night ( ) is a 1972 Israeli-American drama film directed by Steven Hilliard Stern. It was entered into the 22nd Berlin International Film Festival.    The film received two awards: 1) Best script, awarded by International Writers Union, and, 2) C.I.D.A.L.C prize, for "understanding between peoples".

==Cast==
* Haim Anitar - Akiva
* Misha Asherov - Doctor
* Miriam Bernstein-Cohen - Sokolova Eli Cohen - Reuven
* Dalia Friedland - Nurse
* Zvika Gold
* Zalman King - Adam
* Gita Luka
* Edward G. Robinson - Father
* David Smadar - kibbutz driver
* Mona Zilberstein - Adams girlfriend

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 