Coming Up Roses
 
{{Infobox Film
| name           = Coming Up Roses
| image_size     = 
| image	=	Coming Up Roses FilmPoster.jpeg
| caption        = 
| director       = Stephen Bayly
| producer       = Linda James
| writer         = Ruth Carter
| narrator       = 
| starring       = Ifan Huw Dafydd
| music          =  Dick Pope
| editing        = Scott Thomas
| distributor    = 
| released       = May, 1986
| runtime        = 93 minutes
| country        = United Kingdom
| language       = Welsh
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1986 Welsh language comedy drama film. It was directed by Stephen Bayly and starred Ifan Huw Dafydd, Gillian Elisa and Mari Emlyn. The film was screened in the Un Certain Regard section at the 1986 Cannes Film Festival.   

The film centres on the closure of the last small town cinema in South Wales and the communitys efforts to pull together to save the cinema.

==Cast==
* Ifan Huw Dafydd - Dave
* Gillian Elisa - Sian
* Mari Emlyn - June
* Iola Gregory - Mona
* Rowan Griffiths - Pete
* Dafydd Hywel - Trevor
* Olive Michael - Gwen Bill Paterson - Mr. Valentine
* W. J. Phillips - Eli Davies
* Clyde Pollitt - Councillor Mike Lewis - Trevors Son

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 