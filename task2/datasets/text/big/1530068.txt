Flying Wild
 

{{Infobox film|
 name = Flying Wild|
 image = FlyingWildVHS.JPG|
 caption = VHS cover | William West| Al Martin|
starring = Leo Gorcey David Gorcey Bobby Jordan Ernie Morrison|Sunshine Sammy Morrison Donald Haines|
 producer = Sam Katzman|
 distributor = Monogram Pictures Corporation|
 released = March 10, 1941|
 runtime = 64 minutes|
 language = English|
}}

Flying Wild is a 1941 film and the fifth installment of the East Side Kids series.

==Plot== Sunshine Sammy Dave OBrien). 

One day, when Toms plane crashes onto the plant airstrip, Reynolds suspects that the crash may have been the work of saboteurs. Later, on the airfield tarmac, Muggs jokingly appoints himself as the new operator of Dr. Richard Nagel (George Pembroke)s ambulance plane and gives his pals a tour of the aircraft. Their playful games are soon brought to a halt by Nagel, the secret leader of a spy ring, who catches the boys on his plane and angrily orders them off. 

Mr. Reynolds, certain that spies are working at the plant, asks Danny to act as a decoy so that the spies can be identified, and has him deliver to a downtown office a fake set of plans for a new bomb site. As Reynolds predicted, Nagels men ambush Danny on his way to the office, but the plan goes awry when the detectives sent to trail Danny lose him. Danny eventually turns up unharmed some time later. When Muggs reports to Reynolds his suspicions that Nagel is behind the espionage ring, Reynolds dismisses the accusation as a product of the boys imagination. 

Not convinced by Reynolds that Nagel is innocent, Muggs and Danny begin their own investigation into Nagel, starting with a visit to the doctor on the pretext of a fake ailment. The visit turns up nothing, however, and when Danny and Muggs return to the hangar, a suspicious "accident" that was apparently meant to harm them leaves Peewee injured. While Peewee recovers at the hospital, Tom nearly loses his life when he is unable to make contact with the control tower for a landing. The controller is later found bound and gagged in the tower, prompting the kids to resume their investigation in earnest. 

Helen provides the gang with further clues when she confirms that the ambulance plane was being flown on many unusual trips to Mexico, supposedly to deliver patients. When Helen tells the East Side Kids that a man named Forbes is the next "patient" to be transported, they rush to his house, where they find secret plans hidden in his head bandage. Disguising Danny as the transportee, the kids send Danny and Muggs on the flight to learn who is behind the espionage ring. Danny and Muggs soon find themselves in trouble, however, when Nagel, having found Forbes locked in his closet, tries to warn the pilot of the boys ruse. 

Meanwhile, Tom learns of the dangerous mission and goes after the ambulance plane in his own plane. Tom arrives in Mexico in time to save Danny and Muggs, and all the spies are arrested. Back at the plant, Reynolds rewards Muggs for his heroism by giving him a job, but his stint there is short-lived as he is soon distracted by a pretty woman and crashes a plane.

==Cast and characters==
===The East Side Kids===
*Leo Gorcey as Muggs McGinnis
*Bobby Jordan as Danny Graham Sunshine Sammy Morrison as Scruno
*David Gorcey as Peewee
*Donald Haines as Skinny
*Eugene Francis as Algy Reynolds
*Bobby Stone as Louie

===Additional Cast===
*Joan Barclay as Helen Munson Dave OBrien as Tom Lawson- 
*George Pembroke as Dr. Richard Nagel III
*Herbert Rawlinson as Mr. Reynolds Dennis Moore as George
*Forrest Taylor as Forbes
*Robert F. Hill as Woodward
*Mary Bovard as Mazie (uncredited)
*George Eldredge as Man (uncredited)
*Alden "Stephen" Chase as Jack, Henchman (uncredited)
*Al Ferguson, Jack Kenny, Carey Loftin, Bud Osborne, Eddie Parker, Dick Scott as Henchmen

==Production==
* Robert F. Hill who plays the role of Woodward, was director of the first East Side Kids film.
* The scene in which the car flips onto its side was not scripted; Leo Gorcey was driving the car, and had made the turn too fast. The expressions on the gangs faces were one hundred percent real. 
* Eugene Francis last East Side Kids film. He was drafted for World War II service shortly after completing this film.
* The working title of the film was "Air Devils"

==Re-release==
The film was re-released twice, once in 1949 and again in 1952.

==DVD release date==
As this film is in the public domain, there have been several releases from a variety of companies over the years.

==References==
 
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 