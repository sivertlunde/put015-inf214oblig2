Libertas (film)
{{Infobox film
| name           = Libertas
| image          = Libertas-film-poster.jpg
| image_size     = 200
| caption        = Theatrical release poster
| director       = Veljko Bulajić
| producer       = Aleksandar Črček Mirko Kovač Veljko Bulajić
| narrator       =
| starring       = Sven Medvešek Sandra Ceccarelli Goran Grgić Radko Polič
| music          = Đelo Jusić
| cinematography = Slobodan Trninić Vanja Černjul Živko Zalar
| editing        =
| studio         = Libertas Tuna Film Jadran Film Croatian Radiotelevision DDC Film RAI   
| distributor    =
| released       =  
| runtime        = 130 min
| country        = Croatia Venetian
| HRK 42&nbsp;million (United States dollar|US$ 3.6&nbsp;million) 
| gross          =
| preceded_by    =
| followed_by    =
}} 2006 Cinema Italian Co-production directed by Veljko Bulajić. It is a biographical film about the 16th-century playwright Marin Držić and his conflict with authorities of the Republic of Ragusa.

The film marked the return of the veteran director Veljko Bulajić after a 17-year break. Following a long and troubled production, it was released to high expectations, but did not meet a favorable reception from either the critics or the box office.

==Plot==
  Ragusa (modern-day Dubrovnik) managing to maintain its independence through diplomatic agreements.
 comedy play, which is an allegory about the hypocrisy and injustice of high society. The verses spoken on stage are met with disapproval by the noblemen present, and lead to the Rector getting up and leaving during the performance.
 grey eminence, patron Lord Zamagna (Radko Polič), a nobleman and former vice-admiral of the mighty Ragusan trade fleet. Luka charges him with conspiracy and gets him arrested. Zamagnas daughter Deša (Sandra Ceccarelli), a noblewoman and wife of the Spanish ship-owner De Cabrera, unsuccessfully tries to free her father, who eventually dies in a Ragusan dungeon.

Enraged by her fathers demise, Deša joins the anti-government conspirators led by Lord Bučinić (Ljubomir Kerekeš) who plot against the Senate (the Ragusan parliament), and, hoping to gain political support abroad, leaves for the Duchy of Florence, itself a powerful city-state in Tuscany in present-day Italy. Despite repression, Držićs company continues to stage plays and provoke local authorities.

The Senate gets increasingly intolerant to any form of criticism and the company soon find themselves in a difficult situation - as the censors had decided to sanction Držićs thinly veiled criticisms by increasing taxes on his stage productions, the company amasses a huge debt which leads to seizures of their property. Because of this, his close friend, actor Lukarević (Žarko Potočnjak), decides to leave Ragusa and emigrates to Florence.
 libertarian beliefs, and unable to continue his work, Držić also decides to join the conspiracy and leaves for Florence. After reaching Tuscany, Držić mingles with other Ragusan exiles, including Lukarević and Deša Zamagna. Inspired by the progressive society of 16th-century Tuscany, Držić pens a draft of a new Ragusan statute, which he titles Libertas (Latin for "liberty"), which enshrines the freedoms of speech and creative expression.
 Cosimo I de Medici (Andrea Buscemi), the Duke of Florence, but to no avail as he ignores their pleas. Meanwhile, the Ragusan authorities hire mercenaries to track down and assassinate them all. Ignored by Cosimo I, the plot is effectively terminated as spies locate and execute Bučinić and Lukarević. Držić and Deša then decide to escape to Venice, hoping to find refuge in the city in which several of his works had been published, and that the Doge of Venice might be more understanding to their plight.

During their perilous journey, the friendship between them develops into a romance. Arriving in Venice, they try to hide but are nevertheless found by assassins. A manhunt through Venetian streets and bridges ensues, in which Držić deliberately draws the pursuers to himself to lure them away from Deša, before escaping by jumping into a canal. At dawn the following day, Držić is washed ashore. Exhausted and frozen, he is found by the city guards and taken to the poorhouse. As he floats between life and death in delirium he sees his ideals becoming reality. In the final scene, undertakers put his casket on a gondola, which floats away across the lagoon.

==Cast==
*Sven Medvešek as Marin Držić, a Croatian Renaissance playwright. Originally, the primary candidate for the lead role was Medvešeks older brother Rene Medvešek|Rene, while Sven Medvešek was cast as Vlaho Držić, Marin Držićs brother, a role that eventually went to Livio Badurina.  Ragusan noblewoman of the House of Džamanjić), daughter of Držićs patron, and later his lover during his exile in Florence
*Žarko Potočnjak as Lukarević, a theatre actor and friend of Držić
*Goran Grgić as Luka, the Ragusan state censor and the main antagonist
*Radko Polič as Lord Zamagna (or "gospar Zamanja"), Ragusan nobleman, father of Deša and patron of Držić, who dies in prison after being charged with high treason by the state censor Luka Cosimo I de Medici, the Duke of Florence who Držić asks for help in overthrowing the Ragusan government
*Vlatko Dulić as Mavro Vetranović, a Ragusan Benedictine friar and writer who supports Držić and his work publicly
*Miše Martinović as the Rector of Ragusa

==Production== Croatian Ministry Mirko Kovač HRK 12 million (United States dollar|US$ 1.4 million), and the start of the shooting was planned for late 2000.   
  in Dubrovnik was one of the shooting locations.]] cast and crew were not being paid. 

The financial difficulties ultimately stopped the filming, and the entire project was in serious jeopardy until the Government of Croatia intervened with a HRK 3.6 million financial guarantee that revived the production activities.    The filming was finally completed on April 22, 2005, more than two years after the start, and was followed by post-production in Zagreb and Rome. 

===Filming locations===
Many of the films scenes were shot Location shooting|on-location. Locations in Dubrovnik include the Revelin Fortress, Stradun (street)|Stradun, the Sorkočević Villa, as well as the historical streets of the Old Town. 

Some of the scenes taking place in Florence were also shot on-location, while some were actually filmed in the medieval Istrian village of Draguć.    Scenes were also filmed in Venice and in Tuscany.    

==Themes==
The films title Libertas (  as a condemnation of modern despotism, Variety (magazine)|Varietys reviewer Robert Koehler felt it was referring to Eastern Europes communist regimes,  while Croatian critics saw it rather as an allegory of post-communist Croatia.   According to Croatian columnist and film critic Jurica Pavičić, a "small republic obsessed with independence, but totalitarian on the inside" is easily recognized as the 1990s Croatia,    a parallel which was explicitly confirmed as intentional by Bulajić. 

==Reception==
Prior to the films premiere at the 2006 Pula Film Festival, the expectations were very high.  Libertas was well received by the festival audience, with an average score of 4.41/5, ranking 3rd out of 8 films,  but the critics rated it second-to-last at 2.55.  The film won two minor awards at the festival, Golden Arena for Best Costume Design and Golden Arena for Best Make-up.  

The reviews were generally unfavorable.  Variety (magazine)|Variety described Libertas as an "old-fashioned European costume drama" which is "grandly produced and blandly staged".    A Nacional (weekly)|Nacional review found faults with rigid directing and lack of character depth.     The critics disliked the poor Dubbing (filmmaking)|dubbing,    the "unconvincing" and "forced" romance subplot,     and mixing of the 16th-century Ragusan dialect with modern Croatian.     Ultimately, Libertas fared poorly at the box office.   

Libertas was Croatias submission to the 79th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.      

==See also==
 
*Cinema of Croatia
*List of submissions to the 79th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 