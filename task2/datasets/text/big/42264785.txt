Jaga Mechida Huduga
{{Infobox film|
| name = Jaga Mechhida Huduga
| image = 
| caption =
| director = H.R. Bhargava
| story = Sai Suthe (novel)
| screenplay = Chi. Udaya Shankar
| writer = Chi. Udaya Shankar Lakshmi
| producer = S. A. Srinivas
| music = Rajan-Nagendra
| cinematography = D. V. Rajaram
| editing = Anand   Mahendra
| studio = Sri Chowdeshwari Art Combines
| released =  
| runtime = 142 minutes
| language = Kannada
| country = India
| budgeBold textt =
}} 1993 Indian Kannada romance Lakshmi and Tiger Prabhakar  in the lead roles.  The film was widely appreciated for its songs and story upon release. The songs tuned by Rajan-Nagendra duo were huge hits.

== Cast ==
* Shiva Rajkumar as Shivu
* Srinath
* Ragini  Lakshmi
* Tiger Prabhakar 
* Chi Guru Dutt
* K. S. Ashwath
* M. P. Shankar
* Vijay Raghavendra
* Ramesh Bhat
* Pandari Bai
* M. S. Karanth
* Lalithasree

== Soundtrack ==
The music of the film was composed by Rajan-Nagendra.

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Aakashave Nanna Thande
| extra1 = S. P. Balasubrahmanyam 
| lyrics1 = Chi. Udaya Shankar
| length1 = 
| title2 = Manadalli Neene 
| extra2 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics2 = Chi. Udaya Shankar
| length2 = 
| title3 = Ittanthe Iruvenu Shivane
| extra3 = S. P. Balasubrahmanyam
| lyrics3 =Chi. Udaya Shankar
| length3 = 
| title4 = Attheya Magane
| extra4 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics4 = Chi. Udaya Shankar
| length4 = 
| title5 = Ello Udurida Hoovu Rajkumar
| lyrics5 = Chi. Udaya Shankar
| length5 = 
}}

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 


 