Anita: Swedish Nymphet
{{Infobox film
| name           = Anita: Swedish Nymphet
| image          = Anita Swedish Nymphet DVD cover.jpg
| caption        = DVD cover
| director       = Torgny Wickman
| producer       = Inge Ivarson Ove Wallius
| writer         = Torgny Wickman
| narrator       = Christina Lindberg Stellan Skarsgård
| music          = Lennart Fors Torgny Wickman
| cinematography = Hans Dittmer Gustaf Mandal
| editing        = Lasse Lundberg
| distributor    = Swedish Film Production
| released       =        
| runtime        = 95 minutes
| country        = Sweden France
| language       = Swedish
| budget         = 
| preceded_by    = 
| followed_by    = 
}} erotic drama film directed by Torgny Wickman, starring Christina Lindberg and Stellan Skarsgård. 

==Plot==
This erotic tale centers on the alluring Anita (Christina Lindberg), whose search for love leads to an 
empty life of nymphomania.  Anitas self-destructive path takes a new turn when she meets college 
student Erik (Stellan Skarsgård), who tries to help her overcome her addiction.  Erik plays the role
of counselor as Anita slowly reveals her troubled past, but will his prescription of ultimate ecstasy 
really cure her?

The film was made in Stockholm, Katrineholm and the church in Vadsbro with its two towers.

==Cast==
* Christina Lindberg as Anita
* Stellan Skarsgård as Erik
* Danièle Vlaminck as Mother
* Michel David as Father
* Per Mattsson as Artist
* Ewert Granholm as Glasier
* Arne Ragneborn as Man at library
* Jörgen Barwe as Lundbaeck
* Ericka Wickman as Anitas twin sister (daughter of director Torgny Wickman)
* Berit Agedahl as Lesbian social worker
* Jan-Olof Rydqvist as School teacher
* Thore Segelström as School teacher
* Lasse Lundberg as Man at railway station

==Alternative titles==
*The Swedish title is Anita – ur en tonårsflickas dagbok (Anita – From the Diary of a Teenage Girl). 
*The French title of the film is Les Impures.

==Distribution==
Because of its explicit nature, the film was banned in Norway and New Zealand.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 