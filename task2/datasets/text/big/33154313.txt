Say It With Flowers
{{Infobox film
| name           = Say It With Flowers
| image          = 
| alt            = 
| caption        =  John Baxter
| producer       = Julius Hagen
| writer         = H. Fowler Mear   Wallace Orton
| starring       = Mary Clare   Ben Field   George Carney
| music          = Colin Wark
| cinematography = Sydney Blythe
| editing        = Michael C. Chorlton
| studio         = Twickenham Studios 
| distributor    = Radio Pictures
| released       = 1934
| runtime        = 71 min
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British musical John Baxter and starring Mary Clare, Ben Field and George Carney.  In London a group of shopkeepers hold a benefit concert in a local pub to raise money for a woman to visit the seaside for her health. It is notable for the performances of several real music hall stars Florrie Forde, Charles Coborn and Marie Kendall.

==Synopsis== seaside to restore her health. However, she is too ill to work to pay for her visit. Her fellow stallholders rally round, and secretly organise a concert at a nearby pub to help raise the money. They approach all the great musical hall performers (many from the golden era of musical hall) who have used her stall over the years. The concert is successfully staged and Kate is able to head to the seaside. 

==Reception==
Picturegoer Weekly reviewed the film favourably observing "there is more entertainment in this unambitous film... than in many alleged super-productions".  In his book The Age of the Dream Palace Jeffrey Richards highlighted the films genuine sympathy with the lives of the ordinary people it is portraying. 

==Cast==
* Mary Clare - Kate Bishop 
* Ben Field - Joe Bishop 
* George Carney - Bill Woods  Mark Daly - Scotty MacDonald 
* Edgar Driver - Titch 
* Freddie Watts  -Steve 
* Edwin Ellis - Ted 
* Wilson Coleman - Doctor 
* Roddy Hughes - Sam, the Newspaper Seller 
* Florrie Forde - Herself 
* Charles Coborn - Himself 
* Marie Kendall - Herself

==References==
 

==External links==
* 

==Bibliography==
* Richards, Jeffrey. The Age of the Dream Palace: Cinema and Society in Britain 1930-1939. Routledge & Kegan Paul. 1984.
* Shafer, Stephen C. British popular films, 1929-1939: The Cinema of Reassurance. Routledge, 1997.

 

 
 
 
 
 
 
 
 


 
 