The Dirk Diggler Story
{{Infobox film name          = The Dirk Diggler Story image         =  image_size    =  alt           =  caption       =  director      = Paul Thomas Anderson producer      = Shane Conrad writer        = Paul Thomas Anderson narrator = Ernie Anderson starring      =Michael Stein Robert Ridgely Eddie Delcore Rusty Schwimmer music         = 
|cinematography= Paul Thomas Anderson editing       = Paul Thomas Anderson studio        =  distributor   =  released      =   runtime       = 32 minutes country       = United States language      = English budget        =  gross         = 
}} porn star. John Holmes. The film was later expanded into Andersons successful 1997 breakout film Boogie Nights.

==Plot summary== church every drops out of school at age 16 and leaves home. Jack Horner (Robert Ridgely) discovers Diggler at a falafel stand. Diggler meets his friend, Reed Rothchild (Eddie Delcore), through Horner in 1979, while working on a film.

Horner slowly introduces Diggler to the business until Diggler became noticeable within the industry. Diggler becomes a prominent model and begins appearing in pornographic films. Diggler has critical and box office hits which leads him to stardom. The hits and publicity lead to fame and money, which lead Diggler to the world of drugs. With the amount of money Diggler is making he is able to support both his and Rothchilds Substance abuse|addictions. The drugs eventually cause a breakup between Diggler and Horner, since Diggler is having issues with his performance on set.
 TV show, which is a failure both critically and commercially. Having failed and with no work, Diggler returns to the porn industry, taking roles in low-budget homosexual films to help support his habit. On July 17, 1981, during a film shoot, Diggler dies of a drug overdose.

The film ends with a quote from Diggler: "All I ever wanted was a cool 78 Chevrolet Corvette|Vette and a house in the country."

==Cast==
*Michael Stein - Steven Samuel Adams (Dirk Diggler)
*Robert Ridgely - Jack Horner
*Eddie Delcore - Reed Rothchild
*Rusty Schwimmer - Candy Kane
*Ernie Anderson - Narrator

==Production== body builder, narrated the film and Robert Ridgely, a friend of Andersons father, played the role of Jack Horner. 
 John Holmes edited the film using two Videocassette recorder|VCRs.    According to Anderson, the film drew admiring laughs when it was shown at a University of Southern California film festival. 

==Boogie Nights==
The Dirk Diggler Story was expanded into Andersons 1997 breakout film Boogie Nights             with a number of scenes appearing almost verbatim in both films.  Two actors had roles in both films; in Boogie Nights, Robert Ridgely played The Colonel, a pornography financier, and Michael Stein had a cameo appearance as a stereo store customer.  The main differences between The Dirk Diggler Story and Boogie Nights are the mockumentary versus narratives styles in the former and latter films, respectively;  Digglers stint in gay porn in the first film versus his prostitution in the second;  and Digglers dying from an overdose in the first film versus his happy return to his former roles and lifestyle in the second. 
 
==See also==
*Boogie Nights

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 