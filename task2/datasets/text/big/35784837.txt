Sign of the Pagan
{{Infobox film
| name           = Sign of the Pagan
| image	         =
| caption        =
| director       = Douglas Sirk
| producer       = Albert J Cohen
| writer         = Oscar Brodney Barre Lyndon
| based on       = story by Oscar Brodney
| narrator       = Jeff Chandler Jack Palance
| music          =
| cinematography = Russell Metty
| editing        = Milton Carruth
| studio         = Universal International
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          = $2.5 million (US rentals) 
}} Jeff Chandler about Attila the Hun (Jack Palance) and his invasion of Rome. 

==Plot== Eastern Empire about the Huns. However Emperor Theodosius is intent on doing deals with the barbarians.

==Cast== Jeff Chandler as Marcian
*Jack Palance as Attila
*Ludmilla Tcherina as Princess Pulcheria
*Rita Gam as Kubra
*Jeff Morrow as General Paulinus
*George Dolenz as Emperor Theodosius II
*Eduard Franz as Astrologer
*Allison Hayes as Ildico
*Alexander Scourby as Chrysaphius
*Howard Petrie as Gundahar
*Michael Ansara as Edecon
*Leo Gordon as Bleda
*Moroni Olsen as Pope Leo I
*Fred Nurney as Chamberlain
*Sara Shane as Myra
*Pat Hogan as Sangiban
*Robo Bechi as Chilothe Charles Hovarth as Olt Glenn Thompson as Seyte Chuck Robertson as Mirrai
*Walter Coy as Emperor Valentinian III

==References==
 

==External links==
*  at IMDB
*  

 

 
 
 
 
 
 
 

 