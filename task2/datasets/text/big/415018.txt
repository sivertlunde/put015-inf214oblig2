Bugsy and Mugsy
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
|cartoon_name=Bugsy and Mugsy
|series=Looney Tunes/Bugs Bunny
|image=BugsyandMugsy Lobby Card.png
|caption=Lobby card
|director=Friz Freleng
|story_artist=Warren Foster Arthur Davis
|layout_artist=Hawley Pratt
|background_artist=Boris Gorelick
|voice_actor=Mel Blanc
|musician=Carl Stalling Milt Franklyn 
|producer=Edward Selzer
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|release_date=August 31, 1957 (USA)
|color_process=Technicolor
|runtime=7 min (one reel)
|movie_language=English
}} American animated short film written by Warren Foster and directed by Friz Freleng in the Looney Tunes series produced by Warner Bros. Cartoons, Inc. 

The films plot features Bugs Bunny with Rocky and Mugsy. Bugs discovers that two robbers are hiding out on the floor above him, and plays them off against each other.

==Plot==
Bugs has relocated his home due to heavy winter rains; he now lives under the floor of a condemned building. All of a sudden, he hears police sirens, which are followed by a car stopping, and then clambering footsteps. Rocky and Mugsy, two gangsters, burst into the room. They have just committed a jewelry robbery, "all 14-carat". Bugs hears the last word as "carrot", and emerges to see whats happening. He realizes whats going on, and vows to take care of the two while they rest for the night.

First, Bugs takes a candlestick telephone and slips one end near Rockys ear and whispers from the other end in his hole that Mugsy is not so very trustworthy and is coming up with ideas, until Rocky gets out of the chair and confronts Mugsy. Mugsy has no idea whats up.
 Detroit Butcher for nothing". And Rocky once again confronts Mugsy, seizes the weapon and slices one of the couchs arms cleanly. Mugsy still doesnt know whats up.

Next, Bugs is in the attic unscrewing the screws holding the ceiling light over Rockys head. Mugsy sees the screws coming loose. Knowing that Rocky will blame him if the light falls on him, he grabs his own screwdriver and a ladder and tries to screw the light back in. But Bugs beats him to it and the lights falls right on Rocky. Rocky kicks Mugsy several times in the air.

Next, Bugs switches Rockys cigarette with a dynamite stick. He walks over to Mugsy and imitates Rockys voice asking for a light. Mugsy gladly does and Rocky is blown up. Rocky snaps, ties Mugsy up and shuts him in a corridor.

Next, Bugs saws a circle around Rockys chair, only letting him see the tool near the end. Bugs then slips it into Mugsys hands and hides, while Rocky shoots wildly and confronts Mugsy with some hitting while screaming, "I dont know how yas done it, but I know yas done it!!!".

Finally, Bugs pops out from under the floor, unties Mugsy and puts him up his feet with a pair of roller skates and a powerful magnet and drags it down with him. Mugsy skates all around Rocky. Then Bugs and Rocky cause Mugsy to crash from wall to wall. Soon the police arrive and arrest the crooks. Rocky thinks it was Mugsy that gave them away to the police (and then begins to mercilessly beat up Mugsy in the police car) but it was actually Bugs, who put up a neon sign flashing the words "ROCKYS HIDEAWAY".

==References==
 

==External links==
*   at the Internet Movie Database

 
{{succession box
|before=Whats Opera, Doc? Bugs Bunny Cartoons
|years=1957
|after=Show Biz Bugs}}
 

 
 
 
 