Pokémon Ranger and the Temple of the Sea
{{Infobox film
| name           = Pokémon Ranger and the Temple of the Sea
| image          = Pokeranger art.jpg
| caption        = DVD Cover
| film name      = {{film name
| kanji          = 劇場版ポケットモンスターアドバンスジェネレーション ポケモンレンジャーと蒼海の王子 マナフィ
| romaji         = Gekijōban Poketto Monsutā Adobansu Jenerēshon Pokemon Renjā to Umi no Ōji Manafi
| translation    = Pocket Monsters Advanced Generation the Movie: Pokémon Ranger and the Prince of the Sea: Manaphy
}}
| director       = Kunihiko Yuyama
| producer       = Yukako Matsusako Takemoto Mori Junya Okamoto Choji Yoshikawa
| writer         = Hideki Sonoda Bill Rogers Emily Williams Rhonda Krempa
| narrator       = Rodger Parsons
| music          = Shinji Miyazaki
| cinematography = Takaya Mizutani
| editing        = Toshio Henmi
| narrated    = Roger Parsons
| studio         = OLM, Inc.
| distributor    = Toho (Japan)   VIZ Media (U.S.A)
| released       =    
| runtime        = 105 minutes
| country        = Japan
| language       = Japanese  
| budget         = 
| gross          = $26,851,324 
}}

Pokémon Ranger and the Temple of the Sea, originally released in Japan as  , is a 2006 Japanese anime film directed by Kunihiko Yuyama as the fourth and final Pocket Monsters Advanced Generation film, and the ninth Pokémon film overall. As indicated by the title, elements from the Nintendo DS game Pokémon Ranger play a large part. The movie is released on July 16, 2006 in Japan. In North America, Cartoon Network aired the film on March 23, 2007.

The film was the first film released in North America to include a  .    This was the first film to be released since The Pokémon Company|Pokémon USAs acquisition of US distribution from 4Kids Entertainment. Its the only Pokémon film to have its English adaptation produced entirely by TAJ Productions, and is the first to feature the new English voice cast. It was the first full-length Pokémon (anime)|Pokémon film to ever be released in its original aspect ratio on Region 1 DVD. With a feature running time of 1 hour and 45 minutes, it is currently the longest film in the Pokémon franchise made-to-date. The story continues with Ash, May, Max and Brock venturing through the Battle Frontier. This is the last film to feature May and Max as main characters in the series. The movies theme song is titled "Mamoru Beki Mono" by Sowelu.

Yuyama visited the ruins of Rome, the city of Naples and the island of Capri, all in Italy, to get ideas of the setting for the film.   

==Plot==
The film opens with Jack “Jackie” Walker, a Pokémon ranger undercover, infiltrating Phantom the Pirate’s ship and retrieving a special Pokémon egg to prevent Phantom’s selfish desires. Jackie manages to escape with the help of a Mantine, a water/flying type Pokémon. Sometime later Ash Ketchum along with his Pikachu, Brock, May, and her younger brother Max stumble upon the Marina Group, a traveling family circus consisted of water and psychic-type Pokémon. They meet Elizabeth, the star and daughter of Kyle and Meredith, performers and Kyle’s father Chip, the conductor. They kindly take in Ash and the others who are suffering from dehydration in the wasteland. After a performance at a nearby town, May discovers the same egg shown earlier in the film by the Marina Group’s pet Buizel, a water-type Pokémon. A clown apparently traveling with the group takes away the egg and hands it to Elizabeth. That night, May has a dream with her encountering a temple under the sea and a mysterious Pokémon. That morning she explains it to everyone and it turns out that Marina Group have all had that dream and tell the others about a group of people called the People of the Water, stating that they are in relation with them. Meanwhile, Team Rocket; Jessie, James and Meowth raid the Marina Group’s trailer for the egg and attempt to hand it over to Phantom. They fail when the Marina Group’s clown reveals himself as Jack Walker who captures a Fearow to retrieve it.

Jackie explains that the egg belongs to a legendary water-type Pokémon called Manaphy and that his mission is to deliver it to Samiya, the sea temple. Shortly, Phantom and his goons arrive to steal the egg. After a brief struggle, the egg hatches in May’s arms revealing Manaphy. After escaping Phantom, the group arrive at a series of ruins where Kyle uses a bracelet referred as the People of the Water’s mark to open a secret water route leading to a chamber that displays a legend about Samiya. Ash and the others learn that Samiya can blend in with the water and only appears during a rare lunar eclipse. Jackie mentions that they must protect Manaphy from Phantom or else Phantom will find and steal Samiya’s treasure, the sea crown. Leaving through a grotto, Jackie informs Ash and the others that they can’t participate in his mission. On the mean time, Phantom encounters Team Rocket who hires them as a reward for leading him to Manaphy. Phantom decides to follow the group toward the temple. Jackie, Manaphy and the Marina Group depart on Chip’s boat, the Blue Lagoon to Samiya however Manaphy starts to cry noticing May isn’t with it. It uses it ability, Heart Swap to link Jackie to Ash, switching bodies. Ash and the others are welcome aboard on the mission, however Jackie feel concerned over May and Manaphy’s developing relationship. Manaphy, choosing its path individually, leads the Blue Lagoon to Samiya’s location. During their journey, Jackie reveals to Ash that he wanted to become Pokémon ranger after a group of migrating Pokémon saved his life in the mountains as a child. Jackie shortly believes that May’s motherly affection toward Manaphy was effecting his mission and so on May starts to avoid overlay contacting with Manaphy, much to her dismay. That night, Elizabeth hands May her bracelet insisting that she should wear it.

The next day, May’s bandana is pushed into the sea by wind and Manaphy sets out retrieve it. In a submarine, Elizabeth, Ash, Pikachu, Brock, May and Max set out to find Manaphy, which they do however they get caught in a riptide. They arrive at Samiya during the lunar eclipse, unaware that Phantom has been following them. Phantom encounters the group in Samiya and opens the door to the sea crown. He removes a jewel from the sea crown, triggering the temple to flood and sink. Jackie arrives and faces off with Phantom, outsmarting him returning most of the jewels but one. Before evacuating, Manaphy attempts save the temple with Ash and May behind it. Elizabeth, Brock and Max are forced to leave the temple. Ash and May reach the sea crown and piece the jewels back on the crown but realize there is one left. The temple begins to flood halfway, Jackie and Phantom escape Samiya, and Team Rocket that stowed away in Phantom’s submarine are carried off by a current into the sea. Ash finds the last jewel and a large capsule that was a part of Phantom’s submarine. He puts May, Manaphy and Pikachu in the capsule and attempts to the return the jewel, nearly drowning in the process. He manages to save Samiya just after it floods completely and the temple rises to the surface. Phantom shows up and snatches Manaphy. Although they believed Ash had died, Ash appears inside a beam of golden light spawned from the crown and takes back Manaphy. Phantom’s ship intervenes and several ocean Pokémon including Kyogre, a water-type legendary Pokémon retaliate under Manaphy’s orders. Phantom is defeated revealing his abnormal strength to be powered by a mecha suit, Jackie’s mission is complete. At dusk, everyone watches as Samiya returns to the depths and May and Manaphy share one last emotional hug before going their separate ways.

During the credits, Phantom and his second-in-command Galen is detained and Ash, Pikachu, Brock, May and Max depart from the Marina Group. Jackie is seen on patrol capturing the legendary electric/flying-type Zapdos. Team Rocket have been swallowed by a Wailord and are shot out of its blowhole and it appears Samiya is now at peace with Manaphy present. Ash and the others continue their journey as the film closes.

==Cast==
{|class="wikitable"
|-
!Character
!Japanese voice
!English voice
|- Rica Matsumoto|| Sarah Natochenny
|- Kaori (voice Kaori Suzuki|| Michele Knotz
|- Fushigi Yamada|Kyoko Yamada|| Kayzie Rogers
|- Yuji Ueda|| Bill Rogers
|-
| 
|- Megumi Hayashibara|| Michele Knotz
|-
|Kojirou/Team Rocket (anime)#James|James||Shin-ichiro Miki|| rowspan="2" | Jimmy Zoppi
|- Inuko Inuyama
|- Jack Walker||Kōichi Yamadera|| Rich McNanna
|- The Phantom||Hiroshi Fujioka|| Eric Schussler
|- Kaori Manabe||Elisabeth Emily Williams
|- Becky (television personality)|Becky|| Rhonda Krempa
|- Yuri Shiratori|| Michele Knotz
|- Unshou Ishizuka|| Rodger Parsons
|} 

==Box office performance==
 
The general screening of Pokémon Ranger and the Prince of the Sea: Manaphy ran for 6 weeks, from July 15 to August 25, 2006.
#July 15–16, 2nd overall, 2nd domestic, 1st anime
#July 22–23, 3rd overall, 2nd domestic, 1st anime
#July 29–30, 4th overall, 3rd domestic, 2nd anime
#August 5–6, 5th overall, 4th domestic, 2nd anime
#August 12–13, 4th overall, 3rd domestic, 2nd anime
#August 19–20, 6th overall, 4th domestic, 2nd anime

==Home media==
The original Japanese version of the movie was released on DVD on December 22, 2006. The English Dub was released first in North America on April 3, 2007. It was later released in Australia nearly a year later, being released on February 6, 2008. The American set included the Pikachu short Pikachus Island Adventure. 

The film has yet to be released on DVD in the United Kingdom.

==References==
 

==External links==
 
* 
*  
* 
*  

 

 
 
 
 
 
 
 
 
 