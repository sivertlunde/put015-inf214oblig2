La Bidonata
{{Infobox Film 
| name = La Bidonata
| image = La Bidonata.jpg
| caption =  
| director = Luciano Ercoli
| producer = Niccolo De Nora
| writer = Ottavio Alessi  Franca Valeri  Venatino Venantini
| narrator =  Susan Scott
| music = Stelvio Cipriani
| cinematography = Sergio DOffizi
| editing = 	 
| distributor = 
| released = 
| runtime = 111 mins
| country = Italy Italian
| budget = 
| preceded_by = 
| followed_by =
}}
 1977 Italian Italian comedy/crime film. This was the final film directed by giallo specialist Luciano Ercoli. 

==Cast==
* Walter Chiari: Renato
* Maurizio Arena: Maurizio
* Ettore Manni: Ettore
* Marisa Merlini: Marìa Susan Scott: Ornella 
* Venantino Venantini: The Frenchman 
* Vittorio Caprioli: Benjamin Bronchi
* Franca Valeri: Giovanna Bronchi 

==Production==
Before the films premier executive producer Niccolo De Nora was kidnapped. Soon after "La Bidonata" was shelved. Debate continues on whether the film was shelved due to the kidnapping of the producer or for the fact the film played poorly to test audiences. Either way the film was pulled and all elements were presumed lost. No promotional materials were printed and Luciano Eroclis final film would become lost for nearly thirty years. A print however was discovered in a box that was supposed to contain Japanese cartoons. The print was shipped off to an LVR lab where after examination they announced that it was in remarkably good condition. This one surviving print was used for the NoShame DVD release.
 

==Releases==
The film released on Region 0 NTSC DVD by NoShame films in 2006. Because of the obscurity of the film, NoShame included it as a bonus with the film Colt 38 Special Squad. The DVD is currently out-of-print. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 