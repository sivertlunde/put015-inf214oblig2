Where the West Begins (1938 film)
{{Infobox film
| name           = Where the West Begins
| image          = 
| image_size     = 
| caption        = 
| director       = J.P. McGowan
| producer       = Maurice Conn (supervising producer) Stanley Roberts (original story and screenplay) and Gennaro Rea (screenplay)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Jack Greenhalgh
| editing        = Carl Pierson Richard G. Wray
| studio         = 
| distributor    = 
| released       = 1938
| runtime        = 54 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Where the West Begins is a 1938 American film directed by J.P. McGowan.

== Plot summary ==
 

== Cast ==
*Addison Randall as Jack Manning
*Fuzzy Knight as Buzz, Jacks Sidekick
*Luana Walters as Lynne Reed
*Arthur Housman as Beano, Jacks Cellmate
*Budd Buster as Sheriff Judson
*Kit Guard as Henchman Smiley Richard Alexander as Barnes
*Ralph Peters as Hawkins
*Joe Garcia as Henchman Miller
*Six-Bar-B Cowboys as Saloon musicians

== Soundtrack ==
*Fuzzy Knight with the Ray Whitley Band - "Thats My Idea of Fun" (Written by Connie Lee)
*Addison Randall (as Jack Randall) - "Born to the Range" (Written by Johnny Lange and Fred Stryker)
*Addison Randall (as Jack Randall), with the Ray Whitley Band - "Sleep Little Cowboy" (Written by Connie Lee)
*Addison Randall (as Jack Randall), with the Ray Whitley Band - "Down the Trail to Dreams" (Written by Johnny Lange and Fred Stryker)
*Sung by Addison Randall (as Jack Randall), with the Ray Whitley Band - "Im in Prairie Heaven" (Written by Connie Lee)

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 


 