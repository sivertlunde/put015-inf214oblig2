The Full Treatment
 
 
{{Infobox film
  | name           = The Full Treatment
  | image          = "The_Full_Treatment".jpg
  | image_size     = 
  | caption        = Italian theatrical poster
  | director       = Val Guest
  | producer       = Val Guest
  | writer         = Val Guest Ronald Scott Thorn
  | based on       = novel The Full Treatment by Ronald Scott Thorn Claude Dauphin Ronald Lewis.
  | music          = Stanley Black
  | cinematography = Gilbert Taylor
  | editing        = Bill Lenny
  | studio         = Hammer Film Productions Falcon Hilary
  | distributor = Columbia Pictures
  | released       = October 1960 (UK) 21 June 1961 (New York) (U.S.)
  | runtime        = 120 minutes
  | country        = United Kingdom
  | language       = English
  }} Claude Dauphin, Ronald Lewis.  It was based on the 1959 novel The Full Treatment by Ronald Scott Thorn.

==Plot==
A racing driver, involved in a bad accident, goes to the Cote DAzur to recuperate. While there he becomes increasingly prone to violence, and draws a Harley Street doctor into his problems.

==Cast== Claude Dauphin ...  David Prade 
* Diane Cilento ...  Denise Colby  Ronald Lewis ...  Alan Colby 
* Françoise Rosay ...  Madame Prade 
* Bernard Braden ...  Harry Stonehouse 
* Katya Douglas ...  Connie 
* Barbara Chilcott ...  Baroness de la Vailion 
* Anne Tirard ...  Nicole 
* Edwin Styles ...  Doctor Roberts  George Merritt ...  Mr. Manfield

==Critical reception==
*The New York Times wrote, "the British have concocted a snug, tautly-strung little thriller called "Stop Me Before I Kill!"...Mr. Guests package is a small one, but trim and adroitly tied. The contents are worth waiting for." 
*The Radio Times wrote, "at times the writer/director, Val Guest, seems to think hes making a Hitchcock picture but he needs more than glamorous locations for that. He also needs Cary Grant and Ingrid Bergman."   Riviera location."  

==References==
 

==External links==
* 
 
 

 
 
 
 
 
 
 
 