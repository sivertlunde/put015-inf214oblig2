Lady in the Lake
 
{{Infobox film
| name            = Lady in the Lake
| image           = Ladyinthelake.JPG
| alt             = 
| caption         = French theatrical release poster Robert Montgomery
| producer        = George Haight
| screenplay      = Steve Fisher
| based on       =   Robert Montgomery Audrey Totter Lloyd Nolan David Snell
| cinematography  = Paul Vogel
| editing         = Gene Ruggiero
| distributor     = Metro-Goldwyn-Mayer
| released        =  
| runtime         = 105 minutes
| country         = United States
| language        = English
| budget          = $1,026,000  .  gross = $2,657,000 
|}} Robert Montgomery, Leon Ames and Jayne Meadows. The murder mystery was an adaptation of the 1944 Raymond Chandler novel The Lady in the Lake.
 viewpoint of the central character, Marlowe. The audience sees only what he does. Metro-Goldwyn-Mayer promoted the film with the claim that it was the first of its kind and the most revolutionary style of film since the introduction of the sound film|talkies. The movie was also unusual for having virtually no instrumental soundtrack, the music in the film being instead provided by a wordless vocal chorus.

==Plot== Robert Montgomery) submits a murder story to Kingsby Publications. He is invited to the publishers’ offices to discuss his work but soon realizes it is merely a ploy. On Christmas Eve, publishing executive Adrienne Fromsett (Audrey Totter) hires him to locate the wife of her boss, Derace Kingsby (Leon Ames).
 Dick Simmons). But, according to Fromsett, Lavery says he hasnt seen Chrystal for two months; shes missing, and the telegraph appears to be a fake.

It quickly becomes obvious to Marlowe that Fromsett wants her boss for herself (for his money - as she later admits).

When Marlowe goes to see Lavery, the Southerner claims to be unaware of any trip to Mexico. And he has a slip of the tongue and says Mrs. Kingsby WAS a beautiful woman, then revises it to an "is". Then Lavery sucker punches the detective. Marlowe wakes up in jail, where he is questioned by Captain Kane (Tom Tully) and a belligerent Lieutenant DeGarmot (Lloyd Nolan). When Marlowe refuses to divulge anything about his case, Kane warns him not to cause trouble in his district and releases him.

Marlowe learns that the body of a woman has been recovered from a lake owned by Kingsby and Kingsbys caretaker there charged with the murder of his wife Muriel. Fromsett suspects that Chrystal is the real killer, as she and Muriel hated each other. Little Fawn Lake was also where Chrystal was last seen. Marlowe investigates and reports to Fromsett that Muriel was an alias for a woman named Mildred Havelend and that she was hiding from a tough cop – whose description fits DeGarmot.

Marlowe goes to call on Lavery again. Inside the unlocked house, he instead encounters Laverys landlady, Mrs. Falbrook, holding a gun she claims to have just found. Upstairs, he finds Lavery dead in the shower, shot several times. He also finds a handkerchief with the monogram "A F".

Before calling the police, he interrupts a Christmas party at the publishing house to confront Fromsett. In private, she denies killing Lavery. Kingsby comes in and, after learning that Fromsett hired Marlowe to find Chrystal, tells her theirs will be strictly a business relationship from now on. A furious Fromsett fires the private eye. Marlowe immediately gets another job; Kingsby hires him to find his wife.

Marlowe then informs the police of Laverys death. At the scene, he suggests that Muriel was hiding from DeGarmot. The two men scuffle, before Kane separates them and sends Marlowe on his way.

Marlowe obtains more information on Muriel from a newspaper contact. It turns out that Muriel had been a suspect in the suspicious death of her previous employers wife - a woman named Florence. The investigating detective, DeGarmot, ruled that death a suicide; but the victims parents strongly disagreed.

When Marlowe goes to question them, he finds they have been intimidated into keeping silent. Afterwards, he is run off the road by DeGarmot. Regaining consciousness after the crash, he manages to get to a telephone and call Fromsett for help. She takes him back to her apartment, where she tells him that they have much in common and that she has fallen in love with him. They spend Christmas Day together while he recovers from his injuries.

Kingsby shows up and informs Marlowe he has received a telegram from his wife, asking for money. Marlowe agrees to drop it off, as Kingsby is being followed by police detectives. Placing his life in Fromsetts hands, Marlowe instructs her to have the police follow him after ten minutes, following a trail of rice he will leave behind.

The woman Marlowe meets (and who had asked for money from Kingsby) turns out to be Mildred Havelend, alias Mrs. Falbrook, alias Muriel and is the one who killed Chrystal (the "lady in the lake"), Florence (her former employers wife), and Lavery.

DeGarmot was in love with Havelend and helped her cover up the first murder, but she fled from him and married Kingsbys caretaker, Mr. Chess.

DeGarmot tracks down Marlowe and Havelend (having overheard Fromsett speaking to Captain Kane and following Marlowes trail of rice grains). DeGarmot plans to kill Marlowe and Havelend with Havelends gun and stage it to look like they shot each other. DeGarmot murders Mildred, but Captain Kane gets there just in time to gun down his own crooked cop. Marlowe and Fromsett leave for New York to start a life together.

==Cast== Robert Montgomery as Phillip Marlowe
* Audrey Totter as Adrienne Fromsett
* Lloyd Nolan as Lt. DeGarmot
* Tom Tully as Police Captain Fergus K. Kane Leon Ames as Derace "Derry" Kingsby
* Jayne Meadows as Mildred Havelend Richard Simmons as Chris Lavery
* Morris Ankrum as Eugene Grayson
* Lila Leeds as Receptionist Robert Williams as Artist
* Kathleen Lockhart as Mrs. Grayson
* "Ellay Mort" as Chrystal Kingsby. This is an inside joke, as the character is never seen in the film; the name is a homonym of the French "elle est morte", which means "she is dead".

==Production==
MGM bought the rights to Chandlers novel for a reported $35,000. SCREEN NEWS.: Oberon and Corvin Will Star at Universal
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   21 Feb 1945: 12. 

==Box Office==
According to MGM records the film earned $1,812,000 in the US and Canada and $845,000 elsewhere resulting in a profit of $598,000. 

==Critical reaction==
The New York Times film critic wrote, "In making the camera an active participant, rather than an off-side reporter, Mr. Montgomery has, however, failed to exploit the full possibilities suggested by this unusual technique. For after a few minutes of seeing a hand reaching toward a door knob, or lighting a cigarette or lifting a glass, or a door moving toward you as though it might come right out of the screen the novelty begins to wear thin." 

==Radio adaptation==
Lux Radio Theater broadcast a 60 minute radio adaptation of the movie on February 9, 1948 with Montgomery and Totter reprising their roles.

BBC Radio 4, as part of its Classic Chandler series, broadcast on February 12, 2011, a 90 minute production of Lady in the Lake, starring Toby Stephens.

==References==
 

==External links==
*  
*  
*  
*  

===Streaming audio===
*   on Lux Radio Theater: February 9, 1948

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 