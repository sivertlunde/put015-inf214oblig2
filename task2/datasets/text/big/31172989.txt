Contraband Spain
{{Infobox film
| name           = Contraband Spain
| image          = "Contraband_Spain"_(1956).jpg
| caption        = UK theatrical poster
| director       = Lawrence Huntington Julio Salvador 
| producer       =  Ernest Gartside
| writer         = Lawrence Huntington
| based on       = a story by Lawrence Huntington
| starring       =  Richard Greene  Anouk Aimée  Michael Denison 
| cinematography = Harry Waxman
| music          = Edwin Astley	(uncredited)
| editor         = Tom Simpson
| studio         = Balcázar Producciones Cinematográficas Diadem Films
| distribution   = Associated British-Pathé (UK)
| released       = 15 March 1955	(London)  (UK)
| runtime        = 78 minutes
| country        = United Kingdom English
| gross = 
}}
Contraband Spain is a 1956 British-Spanish crime film directed by Lawrence Huntington and starring Richard Greene, Anouk Aimée and Michael Denison.    Its Spanish title is Contrabando.  

==Locations== La Rambla Plaza Real with its arcaded pavements. Girona province. 

==Synopsis==
A secret agent works against smugglers on the border between Spain and France.

==Cast==
 
* Richard Greene as Lee Scott 
* Anouk Aimée as Elena Vargas 
* Michael Denison as Ricky Metcalfe 
* José Nieto (actor)|José Nieto as Pierre Robert Ayres as Mr. Dean
* Richard Warner as Inspector LeGrand
* John Warwick as Bryant
* Philip Saville as Martin Scott 
* Alfonso Estela as Henchman 
* Conrado San Martín as Henchman 
* Antonio Almorós as Lucien Remue
* G.H. Mulcaster as Colonel Ingleby
{{Div col end
}}

==Critical reception==
TV Guide wrote, "a lot of action against beautiful continental backgrounds makes up for weaknesses in plot structure" ;   while Allmovie called it a "fast-paced espionager."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 