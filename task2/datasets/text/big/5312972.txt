Swingers (2002 film)
{{Infobox film |
  name     = Swingers |
  image          = Poster_Swingers_%282002%29.jpg|
  director       = Stephan Brenninkmeijer |
  producer       =Stephan Brenninkmeijer, Roel Reiné|
  writer         =Stephan Brenninkmeijer|
  starring       =Ellen van der Koogh Joep Sertons Nienke Brinkhuis Danny de Kok|
  distributor    = A-Film Distributions, Epix Media|
  released   =   |
  runtime        = 93 minutes |
  country        = Netherlands |
  language = Dutch |
    music          = Soundpalette|
  awards         = |
  budget         = $25,000|
}}
Swingers is a Dutch feature film released in 2002 and tells the story of a thirty-something couple Diana and Julian and their experiment in Swinging (sexual practice)|Swinging.

==Synopsis==
A psychological portrait of relationships and sexual desire. Diana and her husband Julian decide to try swinging with another couple for the first time. They invite Alex and Timo over to Dianas parents ultra-modern house (the parents are absent!) for a weekend-long experiment. Alex and Timo are confident swingers, especially gorgeous Alex, whose ravishing sexiness intimidates quiet Diana, about whom it is hinted that the house holds unhappy childhood memories. And Timo seems strangely uninterested. But once night falls and their sexual exploration begins, we discover that all is not as it seems. Diana may have more quiet strength in her than she knows.

==Release==
*The biggest theatre chain Pathé refused to show it on grounds of alleged pornography. The film therefore didnt do too well at the Dutch box office but was embraced by the DVD/Video circuit. 

==References==
 

==External links==
* 
* 

 
 
 
 
 


 
 