Carlton-Browne of the F.O.
{{Infobox film
| name           = Carlton-Browne of the F.O.
| image          = Carlton-Browne of the FO - UK poster.jpg
| image_size     = 
| caption        = UK cinema poster
| director       = Roy Boulting Jeffrey Dell
| producer       = John Boulting
| writer         = Roy Boulting (screenplay and story) Jeffrey Dell (screenplay and story) 
| narrator       = 
| starring       = Terry-Thomas Peter Sellers Luciana Paluzzi  
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 10 March 1959 (London)
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Foreign Office (F.O.) diplomat (played by Terry-Thomas) who is sent to re-establish good relations with the mineral-rich island of Gaillardia, a former British colony that has been forgotten for 50 years and is attracting the attention of both the USA and the USSR.

==Plot==
A title sequence prologue details Britains accidental acquisition of the island Gaillardia during the seventeenth century, the feud between two scions of its royal house and Britains granting the island self-rule in 1916. However, when independence was granted, the Foreign Office (F.O.) failed to recall its ambassador, who is still there forty years later. He writes a letter to the F.O. informing them of Russian moves to annex the islands mineral wealth.

After some research to find out where exactly Gaillardia is, the F.O. put the matter in the hands of Carlton-Browne, head of the Department of Miscellaneous Territories - inept, he only got the role due to the distinguished career of his father. He suggests sending out two British geologists under the cover of a British Council Morris dancing troupe putting on a show for the king of Gaillardia. At the show the king is assassinated and his young University of Oxford|Oxford-educated son Loris flies out to succeed to the throne - on the flight, travelling incognito as Mr Jones, he talks to a beautiful young woman. Carlton-Browne is sent out to see to British interests under the new king, accompanied by his military attaché Colonel Bellingham of the 2nd Dragoon Guards (Queens Bays)|Bays.

Loris and his prime minister Amphibulos stall the British, hoping to start a bidding war between them and the Russians - Amphibulos hopes to get rich, but Loris hopes to modernise his country and benefit its people (Gaillardias backward standard of living, limited funds and negligible military strength have all been made blindingly obvious at a State Parade held in Loris honor). The two are then visited by Loris uncle Grand Duke Alexis and the veiled Princess Ilyena, whom Alexis and his rebels are backing as the true claimant to the throne. 
 partition the island (to save costs, this is accomplished by little more than painting a white line across the island with a cricket pitch marking trolley). 

Soon afterwards the British mineralogists arrive back at the F.O. to announce they have discovered rich cobalt deposits - on what is now Alexis half of the island...

Loris comes to Britain for talks but the F.O. refuses to meet him, instead negotiating with Alexis so Britain can seize the mineral wealth. Loris discovers this and also overhears Amphibulos giving Alexis his support and planning to overthrow Loris in favour of Ilyena. 
 parachutists to put down the revolution. 

After the parachutists mistakenly attack their own HQ, Bellingham and Carlton-Browne are captured and taken to see the leaders of the revolution - Loris and Ilyena, now engaged to be married. Loris pretends that Bellingham and Carlton-Browne are not in Gaillardia to intervene in the revolution but to give his congratulations on the engagement, which Carlton-Browne goes along with. Gaillardia is reunited, the Russians, British and Americans leave and Carlton-Browne is granted orders of chivalry by both Gaillardia and Britain for his services to world peace.

The credits role on a scene of a team of workmen painting out the white line.

==Cast==
*Terry-Thomas as Cadogan De Vere Carlton-Browne
*Peter Sellers as Prime Minister Amphibulos
*Luciana Paluzzi as Princess Ilyena
*Ian Bannen as the young King Loris
*Thorley Walters as Colonel Bellingham
*Raymond Huntley as Tufton-Slade, foreign secretary
*Miles Malleson as Davidson, Resident Advisor 
*John Le Mesurier as the Grand Duke
*Marie Lohr as Lady Carlton-Browne
*Kynaston Reeves as Sir Arthur Carlton-Browne  Ronald Adam as Sir John Farthing
*John Van Eyssen as Hewitt 
*Nicholas Parsons as Rodgers 
*Irene Handl as Mrs. Carter 
*Harry Locke as Gaillardian commentator 
*Basil Dignam as Security Officer 
*Sam Kydd as Signaller 
*Robert Bruce as Major Miller 
*John Glyn-Jones as Newsreel Interviewer 
*Marianne Stone as Woman In Cinema  Kathryn Keeton as Dancer 
*Margaret Lacey as Onlooker

==See also==
*Gaillardia, a relative of the sunflower

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 