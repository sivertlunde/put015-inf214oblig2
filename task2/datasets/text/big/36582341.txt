A Fine and Private Place (film)
 
{{Infobox film
| name           = A Fine and Private Place
| image          = 
| image_size     = 
| caption        =  Paul Watson
| producer       = W.A. Whittaker Paul Watson
| based on = stories by A.E. Coppard
| narrator       = 
| starring       = Edward Woodward Nanette Newman 
| music          = 
| cinematography = 
| editing        = 
| studio         = EMI Films
| distributor    = 
| released       = 1971 (intended)
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
A Fine and Private Place was a proposed feature film from Paul Watson that was abandoned during filming, ostensibly due to poor weather. 

==Production== Paul Watson was a documentary filmmaker who had written the script. Bryan Forbes, head of EMI Films, green lit the film and production began in Cornwall. 
 John Hough but he eventually decided not to proceed and the film was abandoned. 

Watson subsequently went on to a highly successful career as a documentary filmmaker. 

==References==
 

==External links== BFI

 
 
 