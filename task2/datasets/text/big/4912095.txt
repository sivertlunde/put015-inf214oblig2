Paperboys (film)
 
{{Infobox film
| name           = Paperboys
| image          = 
| alt            =
| caption        =
| director       = Mike Mills
| producer       = Ned Brown, Julia Leach, Andy Spade
| music          = Elliott Smith
| cinematography = Joaquín Baca-Asay
| editing        = Haines Hall
| released       = 2001
| runtime        = 41 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 director Mike Mike Mills. The 41-minute film, produced by Andy Spade, profiles six paperboys from Stillwater, Minnesota.  They invite Mills into their homes, show him their personal effects, and answer questions about their lives and the future of paperboys.  Parents weigh in on the benefits of their childrens job and share thoughts on how the city has changed over the years.  Millss camera follows the boys on their routes, and the elegiac footage is accented with contemporary music.

==External links==
*  
*  at the Palm Pictures website

 

 
 
 
 
 
 
 
 

 