P.S. I Love You (film)
{{Infobox film
| name          = P.S. I Love You
| image         = PS I Love You (film).jpg
| caption       = Theatrical release poster
| alt           = 
| director      = Richard LaGravenese
| producer      = {{Plain list |
* Wendy Finerman
* Broderick Johnson
* Andrew Kosove
* Molly Smith
}} Steven Rogers
| based on      =  
| starring      = {{Plain list |
* Hilary Swank
* Gerard Butler
* Lisa Kudrow
* Gina Gershon
* Jeffrey Dean Morgan
* Kathy Bates
* Harry Connick, Jr.
* James Marsters 
}} John Powell
| cinematography= Terry Stacey
| editing       = David Moritz
| studio        = {{Plain list |
* Alcon Entertainment
* Grosvenor Park Productions
* 2S Films
}}
| distributor   = Warner Bros. Pictures (USA) Momentum Pictures (UK)
| released      =  
| runtime       = 125 minutes 
| country       = United States
| language      = English French Spanish
| budget        = $30 million
| gross         = $156,835,339
}} Steven Rogers novel of the same name by Cecelia Ahern. It stars Hilary Swank, Gerard Butler, Lisa Kudrow, Gina Gershon, James Marsters, Harry Connick, Jr. and Jeffrey Dean Morgan. It was distributed by Warner Bros. Pictures and Momentum Pictures.

==Plot==
Holly and Gerry are a married couple who live on the Lower East Side of Manhattan. They are deeply in love, but they fight occasionally. By winter that year, Gerry suddenly dies of a brain tumor and Holly realizes how much he means to her as well as how insignificant their arguments were.

Deeply distraught, Holly withdraws from her family and friends out of grief until they descend upon her on her 30th birthday. They are determined to prod the young widow to face the future and explore what her life choices should be. As they rally around Holly and help organize her apartment, a cake is delivered, and with it is a message from Gerry. It proves to be the first of several meaningful messages — all ending with "P.S. I Love You" — which he had arranged to have delivered to her after his death. As the seasons pass, each new message fills her with encouragement and sends her on a new adventure. Hollys mother believes that Gerrys letters are keeping Holly tied to the past. But they are, in fact, pushing her into the future. With Gerrys words as her guide, Holly slowly embarks on a journey of rediscovery.

Gerry arranged for Holly and her friends Denise and Sharon to travel to his homeland of Ireland. They arrive at their destination, a house in the beautiful Irish countryside where they find letters from Gerry for Sharon & Denise, one asking Denise to take Holly to his favorite pub. While there, they meet William, a singer who strongly reminds Holly of her deceased husband. He asks her to stay to see him after his last song which he dedicates to her. Upon hearing it, she is overcome with emotion and walks out because it was the song Gerry sang to her shortly after they first met. During the vacation, while on a fishing trip they lose the boats oars leaving the three women stranded in the middle of a lake. During their wait for help, Sharon announces that she is pregnant and Denise reveals she is getting married. This news causes Holly to relapse emotionally and again withdraw into herself. They are eventually rescued by William, whom Sharon and Denise invite to stay the night because of the pouring rain. Unable to deny their feelings for each other, they kiss, and William and Holly become intimate. They begin a conversation about her deceased husband and Holly asks William to drive her to visit her in-laws. Upon Holly revealing their names, William realizes she is the widow of his childhood best friend. Revealing this to Holly causes her to panic, but William calms her down and starts to tell stories about his and Gerrys childhood. The next day, Holly visits Gerrys parents and while there, she also receives a letter from Gerry reminding her of their first meeting.

Arriving home, Holly again withdraws from family and friends. As she continues to become more and more lost, she is inspired by Gerry after finding one his suspender clips next to one of her shoes and realizes she has a flair for designing womens shoes and enrolls in a class that teaches how to actually make the shoes she has designed. A new found self-confidence allows her to emerge from her solitude and embrace her friends happiness. While on a walk with her mother, she learns that her mother was the one whom Gerry asked to deliver his letters after his death and receives the last letter. As the film ends with Holly taking her mother on a trip to Ireland, we see that Holly has opened herself up to the journey beginning with the next chapter of her life, and wherever it takes her she has the hope of falling in love again.

==Cast==
* Hilary Swank - Holly Reilly-Kennedy
* Gerard Butler – Gerry Kennedy
* Lisa Kudrow – Denise Hennessey
* Gina Gershon – Sharon McCarthy
* James Marsters – John McCarthy
* Harry Connick, Jr. – Daniel Connelly, a would-be suitor whom Holly rejects
* Jeffrey Dean Morgan – William Gallagher
* Nellie McKay – Ciara Reilly, Hollys sister
* Kathy Bates – Patricia Reilly, Holly and Ciaras mother
* Anne Kent - Rose Kennedy, Gerrys mother
* Brian McGrath -  Martin Kennedy, Gerrys father

==Production==
In A Conversation with Cecilia Ahern, a bonus feature on the DVD release of the film, the author of the original novel discusses the Americanization of her story — which was set in Ireland — for the screen and her satisfaction with the plot changes which screenwriter/director Richard LaGravenese had to make in order to fit the book into the screen.

The film was shot on locations in New York City and County County Wicklow|Wicklow, Republic of Ireland|Ireland. 

==Soundtrack==
{{Infobox album
| Name = P.S. I Love You
| Type = Soundtrack
| Artist = Various Artists
| Cover =
| Released = December 3, 2007 {{cite web
| url = http://www.amazon.com/P-S-Love-You-Various-Artists/dp/B000XS3WWW
| title = P.S. I Love You
| work = 
| accessdate = 2013-11-02
}}
 
| Certification =
| Genre = Pop music|Pop, soundtrack
| Length = 56:44
| Label = Atlantic
| Producer =
}}

The soundtrack for the film was released on December 3, 2007.

 
# "Love You Till the End" – The Pogues
# "Same Mistake" – James Blunt
# "More Time" – Needtobreathe
# "Carousel" – Laura Izibor Hope
# Last Train Home" – Ryan Star
# "Rewind (Paolo Nutini song)|Rewind" – Paolo Nutini
# "My Sweet Song" – Toby Lightman
# "No Other Love" – Chuck Prophet
# "Everything We Had" – The Academy Is...
# "In the Beginning" – The Stills
# "If I Ever Leave This World Alive" – Flogging Molly
# "P.S. I Love You (1934 song)#Recorded versions|P.S. I Love You" – Nellie McKay John Powell Kerry Brown
 Mustang Sally" Camera Obscuras "Lloyd, Im Ready to Be Heartbroken" also plays in the opening credits.  None of songs are included on the official soundtrack.

==Reception==

===Critical response===
 
The film received negative reviews from critics, with most of the criticism being focused on Hilary Swanks performance. Review aggregation website Rotten Tomatoes gives a score of 23% based on 99 reviews.  
At Metacritic the film received a weighted average score of 39%, based on 24 reviews. 

Manohla Dargis of The New York Times said the film "looks squeaky clean and utterly straight and very much removed from the shadow worlds in which Ms.&nbsp;Swank has done her best work. Yet as directed by Richard LaGravenese ... it has a curious morbid quality ...   wont win any awards; it isnt the sort of work that flatters a critics taste. Its preposterous in big and small matters ... and there are several cringe-worthy set pieces, some involving Mr.&nbsp;Butler and a guitar. The film is not a beautiful object or a memorable cultural one, and yet it charms, however awkwardly. Ms.&nbsp;Swanks ardent sincerity and naked emotionalism dovetail nicely with Mr.&nbsp;LaGraveneses melodramatic excesses." 

David Wiegand of the San Francisco Chronicle said, "This is a movie that will leave you stunned and stupefied from beginning to end, if you dont head for the exits first. The only good things in it are Lisa Kudrow and Swanks wardrobe. The plot is unbelievable, although a competent script could have fixed that. The direction is flabby and uninspired, the casting is wrongheaded, and the performances run the gamut from uninteresting to insufferable ... the film wants terribly to be Ghost (1990 film)|Ghost without a potters wheel, but it just succeeds at being terrible." 
 Lifetime territory may define the guilty pleasure of the genre ... As an exercise in Chick flick|chick-flickery, P.S. I Love You wants to possess the soulfulness of harsh reality and the lilt of romantic fantasy at the same time. In this case, at least, it simply cant be done." 

Stephen Whitty of The Oregonian said, "On a week when many people just want a good reason to put down their packages and smile for a couple of hours, P.S. I Love You arrives – signed, sealed and delivered just on time." 

Irish reviewers were particularly critical of Butlers Irish accent. {{cite news 
| date = December 15, 2007
| author =  Róisín Ingle
| title = Author of her own destiny
| url = http://www.irishtimes.com/newspaper/newsfeatures/2007/1215/1197543941816.html
| archiveurl = http://web.archive.org/web/20101029232107/http://www.irishtimes.com/newspaper/newsfeatures/2007/1215/1197543941816.html
| archivedate = 2010-10-29
| work = The Irish Times
}} 
 {{cite news 
| date = December 21, 2007
| author = Michael Dwyer
| title = PS, I love You
| url = http://www.irishtimes.com/newspaper/theticket/2007/1221/1197997106588.html
| archiveurl = http://web.archive.org/web/20101030025818/http://www.irishtimes.com/newspaper/theticket/2007/1221/1197997106588.html 
| archivedate = 2010-10-30
| work = The Irish Times
}}  Butler later jokingly apologized for his effort at an Irish accent. 

===Box office===
The film opened on 2,454 screens in North America and earned $6,481,221 and ranked #6 on its opening weekend. It eventually grossed $53,695,808 at the North American box office and $91,370,273 in the rest of the world for a total worldwide box office of $156,835,339.   

===Accolades===
   Irish Film and Television Award for Best International Actress.

==Cultural influence==
Dialogue between Connicks and Swanks characters inspired Reba McEntires 2011 single "Somebodys Chelsea".   

==See also==
* The Letter (1997 film) (South Korea)
* The Letter (2004 film) (Thai Remake)
* 2007 in film
* Cinema of the United States
* List of American films of 2007

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 