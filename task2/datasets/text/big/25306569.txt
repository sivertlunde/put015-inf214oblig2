The Bed-Sitting Room (film)
 
 
{{Infobox film
| name           = The Bed-Sitting Room
| image          = The Bed-Sitting Room (film).jpg
| image size     =
| caption        =
| director       = Richard Lester
| producer       = Oscar Lewenstein Charles Wood, based on the play by Spike Milligan & John Antrobus
| narrator       =
| starring       = Ralph Richardson Rita Tushingham Michael Hordern Peter Cook Dudley Moore
| music          = Ken Thorne David Watkin
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
 same name. satirical black comedy.

==Plot== Circle Line.

Other survivors include Captain Bules Martin, who holds a "Defeat of England" medal, as he was unable to save Buckingham Palace from disintegration during the war. Lord Fortnum (Richardson) is fearful that he will mutate into the "bed sitting room" of the title. Mate is a fireguard, except that there is nothing left to burn. Shelter Man is a Regional Seat of Government who survived the war in a fallout shelter and spends his days looking at old films (without a projector) and reminiscing about the time he shot his wife and his mother as they pleaded with him to let them in his shelter. Similarly, the "National Health Service" is the name of a male nurse, although overwhelmed by the extent of the war. Finally, there are two policemen, who hover overhead in the shell of a Morris Minor Panda car that has been made into a makeshift balloon, and shout "keep moving" at any survivors they see to offset the danger of them becoming a target in the unlikely event of another outbreak of hostilities.

Lord Fortnum travels to 29 Cul de Sac Place and actually does become a bed sitting room. Penelopes mother is provided with a death certificate, after which she turns into a wardrobe. Penelope is forced to marry Martin because of his "bright future", despite her love for Alan. Her father is initially selected to become Prime Minister due to "his inseam measurements," but unfortunately, he mutates into a parrot and is eaten due to the starvation conditions that prevail.

Penelope finally gives birth, but her monstrous mutant progeny dies. It emerges that Martin is impotent, so he yields marriage consummation to Alan. Penelope has a second child, which is normal, and there is an indication of hope for the future of the country amidst the devastation when it transpires that a team of surgeons have developed a cure for the mutations involving full-body transplant. Finally, a military band pays homage to Mrs. Ethel Shroake of 393A High Street, Leytonstone, the late Queens former charwoman, and closest in succession to the throne.

==Cast==
*Ralph Richardson as Lord Fortnum of Alamein
*Rita Tushingham as Penelope
*Michael Hordern as Bules Martin
*Arthur Lowe as Father 
*Mona Washbourne as Mother
*Peter Cook as Police Inspector
*Dudley Moore as Police Sergeant
*Spike Milligan as Mate
*Harry Secombe as Shelter Man
*Marty Feldman as Nurse Arthur
*Jimmy Edwards as Nigel
*Roy Kinnear as Plastic Mac Man Ronald Fraser as The Army
*Richard Warwick as Alan
*Frank Thornton as The BBC
*Dandy Nichols as Mrs Ethel Shroake Jack Shepherd as Underwater Vicar
*Henry Woolf as Electricity Man

==Production==
After completing Petulia, Richard Lester had intended his next film to be Up Against It, which was written by playwright Joe Orton, but Orton was murdered shortly before production. Lester offered this to United Artists as a replacement. It was filmed between May and July 1968, mainly in and around Chobham Common. When the executives at United Artists saw the film, they hated it, and it was shelved for a year,  only getting its first release at the Berlin Film Festival in 1969. It wasnt released in the UK until March 1970.  The Bed-Sitting Room would be the last film released by United Artists foreign film arm Lopert Pictures Corporation, which folded in 1970.

==Set design==
The absurdity of the film extends even to the settings. One scene is shot beside a pile upon which a British pottery firm had been throwing rejected plates since World War II {{cite encyclopedia
 | last = Brosnan
 | first = John
 | authorlink = John Brosnan
 | editor = Nicholls, Peter
 | encyclopedia = The Encyclopedia of Science Fiction
 | title = Bed-Sitting Room, The
 | accessdate = 2008-07-12
 | edition = 1st
 | date = 1979
 | publisher = Granada
 | location = 
 | isbn =0-586-05380-8
 | pages = 63–64}}  (the joke being that an actor is looking for a dish that isnt broken). Another set of the film is a mock triumphal arch made of appliance doors, beneath which a Mrs. Ethel Shroake ("of 393A High Street, Leytonstone"), the closest in line for the throne, is mounted on a horse. Even the opening credits have a touch of the absurd, listing the cast not by appearance or alphabetically, but by height.

==DVD and Blu-ray Disc releases== Flipside line. 

==See also==
*The Bed-Sitting Room (play)|The Bed-Sitting Room (play)
* List of apocalyptic films

==References==
 

==External links==
* 
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 