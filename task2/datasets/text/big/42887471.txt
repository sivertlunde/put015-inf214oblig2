Your Money or Your Life (1932 film)
{{Infobox film
| name = Your Money or Your Life 
| image =
| image_size =
| caption =
| director = Carlo Ludovico Bragaglia
| producer = 
| writer = Alessandro De Stefani   Gino Mazzucchi   Carlo Ludovico Bragaglia
| narrator =
| starring = Sergio Tofano   Rosetta Tofano   Luigi Almirante   Cesare Zoppetti 
| music = Vittorio Rieti
| cinematography = Carlo Montuori
| editing = Fernando Tropea 
 | studio = Società Italiana Cines 
| distributor = Cinès-Pittaluga
| released = 31 December 1932
| runtime = 71 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Your Money or Your Life (Italian:O la borsa o la vita) is a 1932 Italian comedy film directed by Carlo Ludovico Bragaglia and starring Sergio Tofano, Rosetta Tofano and Luigi Almirante.  It was made at the Cines Studios in Rome.

==Cast==
* Sergio Tofano as Daniele 
* Rosetta Tofano as Renata 
* Luigi Almirante as Giovanni Bensi 
* Cesare Zoppetti as Tommaso 
* Lamberto Picasso as Anarchic 
* Mara Dussia as Lady with dog 
*Mario Siletti
*Mario Ferrari Giovanni Lombardi
*Eugenio Duse Giovanni Ferrari Mario De Bernardi

== References ==
 

== Bibliography ==
* Brunetta, Gian Piero. The History of Italian Cinema: A Guide to Italian Film from Its Origins to the Twenty-first Century. Princeton University Press, 2009.
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.

== External links ==
* 

 

 
 
 
 
 
 


 