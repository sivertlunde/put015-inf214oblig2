Invisible Children
 
 
{{Infobox Film
| name           = Invisible Children: Rough Cut
| image          = Invisible_Children_DVD.jpg
| caption        = Theatrical release poster
| director       = Jason Russell Bobby Bailey Laren Poole
| producer       = 
| writer         = 
| starring       = 
| music          =  
| cinematography =
| editing        = 
| distributor    = Invisible Children, Inc.
| released       = 2006
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget    rtyrt     = 
}}
Invisible Children: Rough Cut is a 2006 American documentary film which depicts the human rights abuses by the Lords Resistance Army in Uganda.   

==Synopsis== conflict in northern Uganda, Africas second longest-running conflict after the Eritrean War of Independence. The documentary depicts the abduction of children who are used as child soldiers by Joseph Kony and his Lords Resistance Army (LRA). This film centers around a group of Ugandan children who walk miles every night to places of refuge in order to avoid abduction by the LRA.

==Exhibition== Joan B. Kroc Institute for Peace and Justice at the University of San Diego. 

Since then, Invisible Children, Inc. has hosted over 9000 screenings at colleges, high schools, churches, concerts and other venues. As of June 2009, it is estimated that more than 5 million people have seen Invisible Children: The Rough Cut. 

==Social activism==
The story of children depicted in the film was the basis for a grassroots movement mobilizing thousands of American teens into action to raise money to rebuild war-torn schools in northern Uganda and provide scholarships to African youth. 

In 2005, a 501(c)#501(c)(3)|501(c)(3) non-profit, Invisible Children, Inc., was created, giving individuals a way to respond to the situation in Uganda.  An employee of the organization, Nate Henn, was killed in the July 2010 Kampala attacks.   

==DVD==
The film is roughly 55 minutes long, and the DVD includes a shorter 35-minute version for different screening options. The DVD also includes special features  deleted scenes, extras, filmmaker commentary, update on the war, and trailers from Invisible Children, Inc. 

==References==
 

==External links==
 
*  
*  
*  
*   images from film
*   the Nate Henn memorial website
*   Kony 2012 Homepage

 

 
 
 
 
 
 
 