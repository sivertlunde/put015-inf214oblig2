Breakfast in Bed (film)
{{Infobox film
| name           = Frühstück im Doppelbett
| image          = 
| alt            =  
| caption        = 
| director       = Axel von Ambesser
| producer       = Artur Brauner Hans Otto Schröder
| writer         = Ladislas Fodor
| starring       = O.W. Fischer Liselotte Pulver Lex Barker Ann Smyrner
| music          = Friedrich Schröder
| cinematography = Richard Angst
| editing        = Walter Wischniewsky
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}}

Breakfast in Bed ( ) is a 1963 German comedy film directed by Axel von Ambesser and starring O.W. Fischer, Liselotte Pulver and Ann Smyrner.  The wife of a newspaper editor grows sick of his frequent absences.

==Cast==
* O.W. Fischer ...  Henry Clausen
* Liselotte Pulver ...  Liane Clausen
* Lex Barker ...  Victor H. Armstrong
* Ann Smyrner ...  Claudia Westorp
* Ruth Stephan ...  Cilly
* Edith Hancke ...  Mrs. Müller
* Loni Heuser ...  Melanie

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 