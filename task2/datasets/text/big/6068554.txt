The Wings (film)
{{Infobox film
| name           = The Wings
| image          =Asther, Nils in Vingarne 1916.jpg
| image_size     =180px
| caption        =Nils Asther in the film
| director       = Mauritz Stiller
| writer         = Axel Esbensen Mauritz Stiller
| starring       = Egil Eide Lars Hanson Lili Bech Julius Hälsig
| cinematography = Julius Jaenzon
| distributor    = Svenska Biografteatern
| released       = 1916
| runtime        = 69 minutes
| country        = Sweden
| language       = Swedish
}}
The Wings ( ) is a 1916 Swedish silent film directed by Mauritz Stiller, starring Egil Eide, Lars Hanson, Lili Bech, and Julius Hälsig, and was based on Herman Bangs 1902 novel Mikaël (novel)|Mikaël, which was the same source Carl Theodor Dreyer used for his 1924 film Michael (1924 film)|Michael.
 framing story and telling the plot primarily through the use of flashback (literary technique)|flashbacks.

==Plot== mythological Icarus.

==Preservation status==
The film is largely lost film|lost, with only half an hour surviving of the original 70-minute film. A restoration was made using still photos and title cards to bridge the missing sections in 1987.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 