305 (film)
{{Infobox Film
| name           = 305
| image          = 305film.jpg
| caption        = Theatrical poster
| director       = Daniel Holechek David M. Holechek
| producer       = David M. Holechek
| writer         = David M. Holechek Brandon Tyra
| narrator       = 
| starring       = Tim Larson
| music          = Hanna Lim
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
305 is a 2008 American mockumentary film about a group of five Spartans charged with protecting a goat path. It premiered March 7, 2008 at the Fargo Film Festival, and was released direct-to-video on DVD July 8, 2008 by Allumination Filmworks and Peace Arch Entertainment. Prior to its release, the film screened at film festivals across the United States and United Kingdom including the 2008 Newport Beach Film Festival and 2008 Palm Beach International Film Festival. The film was selected by filmbio IndiFilms as a participant in its "Spotlight" program in conjunction with the Palm Beach International Film Festival and was featured on the filmbio IndiFilms website. The film received the "Zeilig Award For Innovation" at the 2008 MockFest in Hollywood, CA.

The entire film was shot against a digital backdrop (greenscreen) in studios around Orange County, California. The film began life as a digital short that became a huge hit on YouTube in June 2007, racking up over 4 million views in a few months. Directors Daniel and David Holechek were contracted by Vanguard Cinema in the Fall of 2007 to expand the short film into a feature length film. Production began in September 2007 and lasted until November of the same year.

==Characters==
*Claudius, the leader of the 5 Spartans who becomes a prisoner of war.
*Darryl, the Dwight Schrute reminiscent Assistant captain.
*Testicleese, the only non-inept member of the five.
*Shazaam, a Spartan whose house is in Persia, but due to zoning laws was able to go to a Spartan school.
*Demetrius, a recently blinded Spartan due to a battle/work related incident.
*Aurillia, an old friend of Testicleese who convinces him to save Claudius.

==External links==
* 

 
 
 
 
 
 


 