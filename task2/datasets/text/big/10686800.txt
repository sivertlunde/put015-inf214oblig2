Late Autumn (1960 film)
{{Infobox film
| name = Late Autumn
| image = Late_Autumn_Criterion.jpg
| caption = Criterion Collection DVD cover
| director = Yasujiro Ozu
| producer = Shizuo Yamanouchi
| writer = Kôgo Noda, Yasujiro Ozu
| starring = Setsuko Hara
| music = Kojun Saitô
| cinematography = Yuuharu Atsuta
| editing = Yoshiyasu Hamamura
| studio = Shochiku
| distributor = Shochiku Kinema Kenkyû-jo New Yorker Films (USA)
| released = 13 November 1960
| runtime = 128 minutes
| country = Japan
| language = Japanese
| budget =
| gross = Ton Satomi 
}}

  is a 1960 drama film directed by Yasujiro Ozu. It stars Setsuko Hara and Yoko Tsukasa as a mother and daughter.  It is based on a story by Ton Satomi.
 Best Foreign Language Film at the 33rd Academy Awards, but was not accepted as a nominee. 

==Plot==
Three middle-aged friends and former college mates – Mamiya (Shin Saburi), Taguchi (Nobuo Nakamura) and Hirayama (Ryuji Kita) – meet up for the 7th memorial service of a late college friend, Miwa.  Miwas widow Akiko (Setsuko Hara) and 24-year-old daughter Ayako (Yoko Tsukasa) are also present.  The three friends remark amongst themselves how good Akiko looks despite being in her forties.

The party chats and thinks that it is time for Ayako to get married.  Taguchi tells them he has a prospective suitor for Ayako, but it later turns out the man already has a fiancée.  Mamiya instead offers his employee, Goto (Keiji Sada), as another match, but Ayako confides privately in Akiko that she has no wish of getting married.  Ayako, who lives alone with Akiko, is close to her mother, who teaches dressmaking.

Ayako meets Goto one day at Mamiyas office.  During a hiking trip, a colleague offers to introduce him to Ayako again.  Ayako and Goto begin dating, but Ayako is unwilling to get married because that will mean Akiko will live all alone.  Ayako puts forward to Mamiya her theory that "romance and marriage could be separate".  The three friends think that all this is an excuse and begin to speculate that Ayako will marry if Akiko remarries.  The other two offer Hirayama, a widower, as Akikos prospective remarriage partner.  Hirayama warns them not to go ahead with their plan, but after discussing with his son to remarry, changes his mind.

Hirayama now approaches Taguchi and Mamiya for help.  Before they can break the subject to Akiko, however, Mamiya tactlessly lets Ayako know about their plan.  Thinking that her mother has known about this, an unhappy Ayako goes home to question her and then leaves for her colleague and friend Yurikos (Mariko Okada) place in a huff.  Yuriko, however, approves of Akikos remarriage. She tells Ayako not to be selfish, which gains Ayakos displeasure.

Displeased, Yuriko confronts the three friends, and finds out the truth from them.  Mamiya apologizes for their mishap; however, seeing their cause, Yuriko decides to help Hirayama.  When Akiko and Ayako go for their last trip together, Akiko tells her daughter she has decided not to marry.  She urges Ayako not to worry about her.  With her assurance, Ayako marries Goto, leaving her mother to live alone.

== Cast ==
* Setsuko Hara as Akiko Miwa
* Yoko Tsukasa as Ayako Miwa
* Mariko Okada as Yuriko Sasaki
* Keiji Sada as Shotaru Goto
* Miyuki Kuwano as Michiko
* Shinichiro Mikami as Koichi
* Shin Saburi as Soichi Mamiya
* Chishū Ryū as Shukichi Miwa
* Nobuo Nakamura as Shuzo Taguchi
* Kuniko Miyake as Nobuko
* Sadako Sawamura as Fumiko
* Ryuji Kita as Seiichiro Hirayama
* Fumio Watanabe as Tsuneo Sugiyama
* Ayako Senno as Shigko Takamatsu
* Yuriko Tashiro as Yoko

==Release==

===Home media===
In 2011, the BFI released a Region 2 Dual Format Edition (Blu-ray + DVD).  Included with this release is a standard definition presentation of A Mother Should be Loved.

==See also==
* List of submissions to the 33rd Academy Awards for Best Foreign Language Film
* List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*  
* 

 

 
 
 
 
 
 
 
 