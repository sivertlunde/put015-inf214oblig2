Guni-Guni
{{Infobox film
| name = Guni-Guni
| image = Guni-Guni_theatrical_poster.jpeg
| caption = Guni-Guni promotional poster
| director = Tara Illenberger
| writer = Tara Illenberger
| starring = {{plainlist|
* Lovi Poe
* Benjamin Alves Empress
}}
| editing = Fiona Borres
| cinematography = A. B. Garcia
| studio = 
| distributor = Regal Films
| released =  
| country = Philippines
| language = Tagalog English
| budget = 
| gross = 
}}
 Filipino horror horror film under Regal Entertainment. It stars Lovi Poe and Benjamin Alves. The film is directed by Tara Illenberger. 

==Plot==
Thirty years ago, an unborn child was buried in the garden of what is now a boardinghouse in Cubao. It lies beneath the ground, unbeknownst to the tenants who live there. One of the tenants is Mylene (Lovi Poe) who appears like the perfect girl, nice, pretty and at the top of her Medicine class. But nobody knows about her past, not even the man who loves her most, Paolo (Benjamin Alvez). Paolo doesnt know anything about Mylenes family, her long, lost sibling or her estranged, nervous wreck of a mother. Nor does he know about the very long scar that runs across Mylenes body, nor of how incomplete she always feels. And he finds that the more he tries to win back her love, the more she retreats to her secret world that nobody could enter.

One day Mylene asked to perform an abortion for fee, she feels conflicted about doing what is right and, at the same time, being in dire need of tuition fee. Her decision ultimately leads to a dark outcome and awakens a force that has laid quiet for years in the boardinghouse grounds. Thereafter, she becomes tormented by nightmares of a dark twin, whose presence gets stronger as days pass, as strange things start to happen in the house. When one by one, the boarders die of unexplainable causes, Joanna (Empress Shuck) the resident psychic and Mylenes best friend struggles to understand the impending danger that she senses and decides to get to the bottom of the mystery; and as it unravels, they find themselves confronted by an angry soul that seeks justice. 

==Cast==
*Lovi Poe as Mylene, a medical student who has a dark secret despite of having an orderly and kind look
*Benjamin Alves as Paolo, Mylenes boyfriend who cheated on her
*Empress Schuck as Mylenes best friend Joanna, who has a third eye and can see dead people. 
*Isay Alvarez as Mylenes mom
*Gina Alajar as Mrs. Arevalo, an alcoholic woman waiting for her dead son who committed suicide.
*Jaime Fabregas as Tatay Nanding, the gardener and caretaker of the boarding house
*James Blanco as Angelo, Tatay Nandings estranged son
*Ria Garcia as Alicia, a pregnant boarder
*Neil Ryan Sese as Eddie, a salesman and a widower still coping with his wife’s mysterious death. He works as a medical representative, always on the road, taking work-related trips. 
*Gerald Pesigan as Jay-Jay, autistic son of Eddie.
*Julia Clarete as Vangie, Geralds yaya
*Guji Lorenzana as Javier, the son of Mrs. Arevalo who committed suicide.

==Production==
The film was first announced by   but she turned down the role due to her busy schedule, making another Regal Film and a drama series under GMA Network.  Roselle Monteverde, President of Regal Entertainment confirmed on May 23 that the role was already given to actress, Lovi Poe.  The new GMA 7s contract artist Benjamin Alves signed for the lead man role making the film his first lead role in a movie outfit.   The film began shooting in May 28, 2012.

==Marketing and promotions==
The promotional photo of Guni-Guni was first released on July 20 with Lovi Poe in a sepia shot carrying the title of the film. On July 25, the theatrical poster of the film was released online featuring Lovi Poe in a dual personality.  The photos were taken in Pioneer Studio in Mandaluyong City.  The official trailer was released in the official YouTube channel of Regal Films on July 29, 2012. The film had their premiere night on August 20, 2012 at the SM Megamall.

==External links==
*  

==References==
 

 
 
 
 
 


 
 