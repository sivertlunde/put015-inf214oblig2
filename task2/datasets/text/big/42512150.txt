Regression (film)
{{Infobox film
| name           = Regression
| image          = Regression poster.jpg
| alt            = 
| caption        = Teaser poster
| director       = Alejandro Amenábar
| producer       = Alejandro Amenábar   Fernando Bovaira   Christina Piovesan
| writer         = Alejandro Amenábar
| starring       = Ethan Hawke   Emma Watson   David Dencik
| music          = 
| cinematography = Daniel Aranyó
| editing        = 
| studio         = Mod Producciones   First Generation Films   Himenóptero
| distributor    = The Weinstein Company
| released       =  
| runtime        = 
| country        = United States   Spain
| language       = English
| budget         = 
| gross          = 
}}
Regression is an upcoming American-Spanish thriller film directed and written by Alejandro Amenábar. The film stars Ethan Hawke, Emma Watson and David Dencik.

== Cast ==
* Ethan Hawke  as Detective Bruce Kenner 
* Emma Watson  as Angela Gray 
* David Dencik  as John Gray
* Devon Bostick  as Roy Gray
* Aaron Ashmore as George Nesbitt
* David Thewlis as Professor Kenneth Raines
* Dale Dickey as Rose Gray
* Adam Butcher as Brody

== Production ==
On October 31, 2013 it was announced that Ethan Hawke would star as a lead actor in the upcoming thriller film Regression and Alejandro Amenábar would direct on his own script, while FilmNation Entertainment would be holding the international rights.    On November 6, 2013 The Weinstein Company acquired the US distribution rights to the thriller film.    On November 22, 2013 Weinstein announced to release the film on August 28, 2015 in the US.    On February 5, 2014 Emma Watson also joined the film to co-star with Hawke.    On March 25 David Dencik joined the cast of the film, he would play a man who is arrested in a Minnesota town for sexually abusing his daughter.    On May 23, Devon Bostick was added to the cast of the film.   

=== Filming ===
The principal photography of the film began on April 15, 2014 in Mississauga, Ontario, Canada.  Emma Watson stated, "First day shooting Regression today – a very cool birthday present xx."   The first day of shooting was her 24th birthday which she spent on filming a church scene at University of Toronto Mississauga.  On May 5, filming was taking place in Tottenham, Ontario|Tottenham, and it will be filming through June 12.  On June 3, it was reported that Watson was back in Toronto to continue the rest of filming after spending some time to graduate from UK.  On January 28, 2015, crew was filming a few additional scenes or reshoots in Mississauga. 

== Marketing ==
On June 10, 2014, TWC-Dimension revealed a first look photo from the film.  On February 12, 2015, the first trailer was released.  

== Release ==
The film is scheduled for an August 28, 2015 release. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 