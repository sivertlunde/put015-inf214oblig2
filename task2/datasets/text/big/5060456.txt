Dahmer (film)
{{Infobox Film
| name           = Dahmer
| image          = Dahmer.jpg
| image_size     = 215px
| alt            = 
| caption        = Official logo David Jacobson
| producer       = Larry Ratner
| writer         = David Jacobson
| starring       = Jeremy Renner Bruce Davison Artel Kayàru Matt Newton
| music          = Christina Agamanolis Mariana Bernoski Willow Williamson
| cinematography = Chris Manley
| editing        = Bipasha Shom
| studio         = Blockbuster Films DEJ Productions Two Left Shoes Films
| distributor    = Peninsula Films
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $250,000   
| gross          = $144,008 
}} biopic horror horror film American serial killer Jeffrey Dahmer. Jeremy Renner stars in the title role. There are two timelines in the film: The "present" of the film runs in ordinary chronological order covering the period of one-to-two days; the flashbacks go in reverse order, so that Dahmer is seen as successively younger until the film arrives at his first murder and its aftermath.

==Plot== sadistic experiments on his victims before he murders them. He killed one man this way in Bath Township, Summit County, Ohio|Bath, Ohio, and sixteen men in the metropolitan area of Milwaukee, Wisconsin. At the same time, he rationalizes his crimes with the divorce of his parents and his emotionally isolated childhood; nevertheless, he cant stop inviting more and more young men from bars and clubs to his home, where he kills them. Jeffrey starts to be friends with a young man named Rodney and invites him to his house, but Rodney ends up getting attacked by Jeffrey and leaves.

==Cast==
:The real-life counterpart to the fictional victim is in parentheses
* Jeremy Renner as Jeffrey Dahmer
* Bruce Davison as Lionel Dahmer Artel Kayàru as Rodney (Tracy Edwards)
* Matt Newton as Lance Bell (Stephen Hicks)
* Dion Basco as Khamtay (Konerak Sinthasomphone)
* Kate Williamson as Grandma
* Christian Payano as Letitia
* Tomya Bowden as Shawna
* Sean Blakemore as Corliss

==Production== Artel Kayàru as Rodney, and Dahmers first victim was a man named Stephen Hicks, portrayed in the film by Matt Newton as Lance Bell. Production took place in Los Angeles and one scene in Milwaukee. The mask special effects were created by Christien Tinsley and Kelley Mitchell, who were involved two years later in the makeup of Mel Gibsons The Passion of the Christ.

==Reception==
Dahmer has received mostly mixed to positive reviews. It currently holds a 68% "Fresh" rating on Rotten Tomatoes. 

==Legacy==
In the DVD commentary track for The Hurt Locker, Kathryn Bigelow said that she cast Renner in the lead role because of his performance in Dahmer.

==Awards==
* Independent Spirit Award - Jeremy Renner (Best Male Actor in a Leading Role) (nominated)
* Independent Spirit Award - Artel Kayàru (Best Newcomer) (nominated) David Jacobson (nominated)

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 