I Saw the Sun
{{Infobox film
| name           = I Saw the Sun
| image          = Gunesi gordum.jpg
| image size     = 
| caption        = Theatrical poster
| director       = Mahsun Kırmızıgül
| producer       = Murat Tokat
| writer         = Mahsun Kırmızıgül
| narrator       = 
| starring       = Mahsun Kırmızıgül Demet Evgar Altan Erkekli
| music          = 
| cinematography = Soykut Turan
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Turkey
| language       = Turkish
| budget         = US$6 million
| gross          = US$12,812,281
| preceded by    = 
| followed by    = 
}}
 Turkish drama official submission for the Academy Award for Best Foreign Language Film at the 82nd Academy Awards, but it was not nominated.   

==Production== The White Angel ( ), whose 21-member family had to abandon their village around 15 years previously. I was overwhelmed by what he told me. As I am a native of that region myself, I thought I would recount the story in the way it deserves. I hope everyone gets the right message from the film, Kırmızıgül said. Those who watch this film will see that nothing can be solved through hatred. My aim is to stop the bloodshed by boosting Turkish-Kurdish fellowship. This film will delve more strongly into the concepts of brotherhood and unity. We have to love each other without prejudice because nothing can be solved with hatred.   
 Turkish cinema. He is a professional and his communication with the actors was excellent. I believe he filmed the story he wanted to tell in the best way he could.   

==Plot==
 
The film tells of a Kurdish family who head to İstanbul from their village in southeastern Turkey. Part of the family tries to emigrate to Norway.

==Release==

===Premiere===
The film premiered at a special gala screening at the AFM İstinye Park Cinema in Istanbul on   at which, according to writer-director Mahsun Kırmızıgül, the audience reflected the emotion in the film.   

A second special gala screening was held at Panora Cinema in Ankara on   attended by a number of high-profile guests, including Turkish State Minister and Deputy Prime Minister Cemil Çiçek, who said the film aptly portrays the pain and anguish Turkey has suffered in the last 25 years and includes some very accurate messages, and Turkish Culture and Tourism Minister Ertuğrul Günay, who said the very bold and realistic film showed Turkey’s wounds in all its reality and proposed big steps to be taken to heal those wounds.   

At the film’s premiere in Istanbul Kırmızıgül said, I hope the message of our film reaches the public as quickly as possible, at the following screening in Ankara he said, Message transmitted. 

===General release===
The film opened on nationwide general release in 355 screens across Turkey on   at number one in the Turkish box-office with a first weekend gross of US$2,539,362. The film was re-released in 31 screens across Turkey on   with little success.         

===Festival screenings===
* 22nd Tokyo International Film Festival (October 17–25, 2009)
* 33rd Cairo International Film Festival (November 10–20, 2009)
* 40th International Film Festival of India (November 28-December 3, 2009)

==Reception==

===Box office===
The film was at number one in the Turkish box office charts for 5 weeks of its 11 week run and was the third highest-grossing Turkish film of 2009 with a total gross of US$12,812,281. 

===Reviews===
  is illustrated as an ambitious melodramatic epic through its many subplots and ensemble cast, according to Todays Zaman reviewer Ermine Yıldırım, which actually works in achieving its goal of spreading a thing or two about humanity and unconditional fraternity, even though theres ample force-feeding through its languorous monologues of micro-politics. Nevertheless, Kırmızıgüls message cannot go unnoticed as he continually underlines the importance of different segments of society living in harmony and the ideal that the state is not a divider but a caregiver. Kırmızıgül might not be a great filmmaker, Yıldırım concludes, but he is on his way to becoming a true philanthropist.   

===Awards===
* 22nd Tokyo International Film Festival (October 25, 2009) - Asian Film Award (Special Mention)
* 3rd Yeşilçam Awards (March 23, 2010) - Best Supporting Actor: Cemal Toktaş  (Won)   

== See also ==
* 2009 in film
* Turkish films of 2009

==References==
 

==External links==
*    
*    
*    
*    
*  
 

 
 
 
 
 
 
 