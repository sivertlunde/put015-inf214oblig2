Lego Batman: The Movie – DC Super Heroes Unite
 
{{Infobox film
| name           = Lego Batman: The Movie&nbsp;– DC Super Heroes Unite
| image          = Lego Batman, The Movie cover.jpeg
| caption        = Cover art
| alt            = 
| director       = Jon Burton
| producer       = Jon Burton
| screenplay     = David A. Goodman
| story          =  
| based on       =  
| starring       = {{Plain list | 
* Clancy Brown
* Troy Baker
* Christopher Corey Smith
* Travis Willingham  Laura Bailey
* Brian Bloom 
* Steven Blum 
* Cam Clarke 
* Townsend Coleman
* Rob Paulsen 
* Charlie Schlatter 
* Katherine Von Till
* Erin Shanagher
}}
| music          = {{Plain list | 
* Rob Westwood 
* Danny Elfman (Batman Theme)
* John Williams (Superman Theme)
}} TT Animation
| distributor    = Warner Premiere  Warner Bros. Pictures
| released       =  
| runtime        = 71 minutes
| country        = United Kingdom United States Denmark
| language       = English
| budget         = 
| gross          = 
}}

Lego Batman: The Movie&nbsp;– DC Super Heroes Unite is a direct-to-video  . Though the film has cutscenes from the video game, the gameplay is replaced by new scenes with the same actors. The plot revolves around Lex Luthor and the Joker teaming up to destroy Gotham City with Batman and Robin teaming up with the Justice League in order to save Gotham. The film was released on Blu-ray and DVD on May 21, 2013.

== Plot == Joker breaks in with the Riddler, Harley Quinn, the Penguin, Two-Face,Poison Ivy, Bane, Catwoman, and steals the award, as well as money and gems. Batman arrives and stops everyone except the Joker, who escapes on his boat, but Robins helicopter picks up Batman. When the rope holding Batman is broken, he calls in the Batwing and defeats the Joker. Meanwhile, Lex is running for president, but his poll figures are terrible. He learns that the Joker knows how to produce more Kryptonite, and can make a gas that makes people love him, which will help Luthor win the election and defeat Superman. He breaks the Joker out of Arkham Asylum and promises him the use of Luthors De-Constructor weapon to defeat Batman by dismantling all his gadgets and vehicles.

And as they leave, Joker uses the De-Constructor to break Harley Quinn, Poison Ivy, the Penguin, Two-Face, Bane, Catwoman, and the Riddler out. While the Joker is "chemical shopping" at Ace Chemicals (much to Luthors annoyance), Batman is arriving at Arkham. As the villains escape on their vehicles, Robin takes down Catwoman while Batman goes after Two Faces car, with the Riddler and Harley in tow; while they leave Bane, Poison Ivy, and the Penguin because Banes vehicle was too slow. Batman beats Two Face by stealing his coin, so he does not know which direction to turn and crashes, then follows Banes mole vehicle underground, where he takes control and crashes them into a wall.

Batman and Robin put them back into prison, then investigate the breakout, which leads them to Ace Chemical Plant, booby-trapped by the Joker after a visit to gather materials for the gas and Kryptonite. Superman rescues the Dynamic Duo, but Batman is rude to him (concerned he might go rogue some day) and he leaves. Batman traces Luthors mobile lab and snatches the manufactured Kryptonite, but the Batmobile is deconstructed. At the Batcave, he determines the new Kryptonite is harmless, but it proves to be a tracking device which leads the Joker and Luthor to the cave—Batman has a Kryptonite Vault which the Joker knew about. Using explosive pies, the villains destroy the cave, escaping with a load of Kryptonite, while the heroes are saved by Superman.

The next day, election day, Superman and Batman visit the LexCorp building, and are attacked by Luthor in a giant Kryptonite-powered Joker robot. The villains describe their plan to the heroes as they defeat them, and then leave. However, Batman and Superman had switched costumes to learn Luthor and the Jokers plan. The two then switch back costumes and chase down the robot, but fail; since the Kryptonite drained Supermans powers and Batman doesnt have a vehicle, the two are forced to take a bus to City Hall. Later, outside City Hall, Luthor makes a speech in front of a huge crowd and then has the Joker use the Joker robot to spray the crowd with Joker Gas. Luthor then finds out that the Joker Gas has caused the crowd to start cheering for Joker to be president and realizes that the Joker tricked him. The Jokers handiwork is visible from space, alerting The Justice League, who arrive to help Batman, Robin, and Superman.
 Flash loads Green Lantern for help. Green Lantern then fires a bright green light into space to celebrate their victory.
 Brainiac spots the green light and realizes that he just found the green energy.

==Cast== Bruce Wayne/Batman, Brainiac
* Clark Kent/Superman Joker
* Clancy Brown as Lex Luthor Laura Bailey Poison Ivy, and Wonder Woman Cyborg
* Bane and the Penguin Green Lantern and Martian Manhunter|Jonn Jonzz/Martian Manhunter James Gordon
* Rob Paulsen as Riddler the Flash Robin
* Katherine Von Till as Catwoman and the voice of the Batcomputer
* Erin Shanagher as News Reporter

==Crew==
* Jon Burton - Director, Producer, Story
* David A.Goodman - Writer, Story
* Jeremy Pardon - Director of Photography
* Cam Clarke - Casting and Voice Director

==Reception==

Jeffrey M. Anderson of Common Sense Media gives the film a score of 9/10, saying the lighthearted superhero action is fun, but with the reservation that it is a big advertisement for the toys. {{cite web 
| date = May 23, 2013 
| author = Jeffrey M. Anderson
| work = Common Sense Media
| title = LEGO BATMAN: THE MOVIE - DC SUPER HEROES UNITE
| url = http://www.commonsensemedia.org/movie-reviews/lego-batman-the-movie-dc-superheroes-unite
}} 

Mike McGranaghan from Film Racket called it "Beautifully animated and utterly charming" and gives the film 3 out of 4 stars. {{cite web 
| date = May 22, 2013 
| author = Mike McGranaghan
| work = Film Racket 
| title = LEGO Batman: The Movie -- DC Superheroes Unite
| url = http://aisleseat.com/legobatman.htm
}} 

==Home media== Teen Titans that focus on Batman and Robin teaming up with the members of the Justice League featured in this fillm ("Triumvirate of Terror!" featuring Superman and Wonder Woman, "Scorn of the Star Sapphire!" featuring Green Lantern and Wonder Woman and "Overdrive" featuring Cyborg).

==References==
 

==External links==
*  
*   at Rotten Tomatoes

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 