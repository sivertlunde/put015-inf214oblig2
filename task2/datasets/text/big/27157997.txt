Three Silent Men
{{Infobox film
| name           = Three Silent Men
| image          =
| image_size     =
| caption        =
| director       = Thomas Bentley 
| producer       = F.W. Baker
| writer         = E.P. Thorne (novel)   Jack Byrd   Dudley Leslie
| narrator       = Sebastian Shaw   Derrick De Marney   Patricia Roc   Arthur Hambling
| music          = 
| cinematography = Geoffrey Faithfull
| editing        = Cecil H. Williamson
| studio         = Butchers Film Service
| distributor    = Butchers Film Service
| released       = 7 December 1940
| runtime        = 72 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        = British crime Sebastian Shaw, Derrick De Marney, Patricia Roc and Arthur Hambling. 

==Cast== Sebastian Shaw as Sir James Quentin
* Derrick De Marney as Captain John Mellish
* Patricia Roc as Pat Quentin
* Arthur Hambling as Ginger Brown
* Meinhart Maur as Karl Zaroff John Turnbull as Inspector Gill
* Peter Gawthorne as General Bullington
* André Morell as Klein Charles Oliver as Johnson
* Jack Vyvian as Sergeant Wells
* Billy Watts as Fernald
* Charles Paton as  Mr. Gibbs
* Basil Cunard as Dr. Fairlie
* Hugh Dempster as Nelson Ian Fleming as Pennington Cameron Hall as Badger Wood
* Scott Harrold as Ted Blacklock
* F.B.J. Sharp as Coroner
* Bill Shine as Bystander at accident
* Cynthia Stock as Matron

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 