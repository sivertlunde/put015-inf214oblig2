Crutch (film)
{{Infobox film
| name           = Crutch
| image          = 
| caption        = 
| director       = Rob Moretti
| producer       = Michael Philip Anthony  Rob Moretti Eric Smith
| writer         = Paul Jacks Rob Moretti
| starring       = 
| music          = Ben Goldberg
| cinematography = Brian Fass
| editing        = Jennifer Erickson Rob Moretti
| distributor    = Ardustry Home Entertainment LLC  HP Releasing
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
}}
Crutch is a 2004 autobiographical coming of age film written and directed by Rob Moretti.  

==Synopsis==
Young David (Eben Gordon) seems to have a normal middle-class life in the suburban world outside New York City. When Davids father (James A. Earley) leaves his alcoholic wife (Juanita Walsh) after 17 years, David is forced to become parent to his siblings and caregiver to his alcoholic mother. Theater coach Kenny (Rob Moretti) becomes enamoured of David. Overwhelmed by his home situation, David is weakened and falls prey to the taboo. Giving in to Kennys advances, David becomes involved with drugs and alcohol.

==Cast==
*Eben Gordon as David Graham 
*Rob Moretti as Kenny Griffith 
*Juanita Walsh as Katie Graham 
*Jennifer Laine Williams as Julia 
*Jennifer Katz as Maryann
*James A. Earley as Jack Graham 
*Robert Bray as Michael Graham 
*Laura OReilly as Lisa Graham 
*Tim Loftus as Zack 
*Sylvia Norman as Linda 
*Frankie Faison as Jerry 

==Critical response==
Anita Gates of the New York Times writes "Crutch doesnt have the texture or power of "Blue Car", Karen Moncrieffs 2002 film with Agnes Bruckner as the neglected, emotionally needy teenager and David Strathairn as the high school poetry teacher who takes advantage ...   does sound a note of real anguish, however."   Don Willmott of Filmcritic.com writes "Crutch comes across as an extremely personal exorcism of Moretti’s suburban gothic adolescence, for better and for worse. Like the scribblings in a teenager’s diary, the film vacillates between insight and exaggeration".  Movies Online opines "CRUTCH is a captivating and brutally honest look into love, loss, lies and our own dark secrets".   However, on the negative side, DVD Verdict opines that "Rob Morettis Crutch is the kind of film I feel bad for not liking. Its awfully sincere, and, darn it, everyone involved tries real hard, but the movie still comes up short". 

==References==
 

==Additional reading==
* 

==External links==
* 
*  
* 
* 

 
 
 
 
 