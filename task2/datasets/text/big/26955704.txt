The Christian (1923 film)
{{Infobox film
| name           = The Christian
| image          = The Christian -1923 poster.jpg
| caption        = 1923 theatrical poster
| director       = Maurice Tourneur
| producer       = Samuel Goldwyn
| writer         = Paul Bern
| based on       =   Richard Dix Mae Busch
| cinematography = Charles Van Enger
| editing        =
| distributor    = Goldwyn Pictures
| released       =   reels (8000 ft)
| country        = United States
| language       = Silent film (English intertitles)
| budget         =
}}
 Richard Dix and Mae Busch. 

==Production background== Broadway stage The Christian (Australian), 1913, 1914 and 1915. Some filming for this production was done in the United Kingdom. 

==Preservation status==
Extant at the George Eastman House. 

==Cast== Richard Dix - John Storm
* Mae Busch - Glory Quayle
* Gareth Hughes - Brother Paul
* Phyllis Haver - Polly Love
* Cyril Chadwick - Lord Robert Ure
* Mahlon Hamilton - Horatio Drake
* Joseph J. Dowling - Father Lampleigh
* Claude Gillingwater - Lord Storm
* John Herdman - Parson Quayle
* Beryl Mercer - Liza
* Robert Bolder - Reverend Golightly
* Alice Hesse - Mary
* Aileen Pringle - Lady Robert Ure
* Harry Northrup - Faro King

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 