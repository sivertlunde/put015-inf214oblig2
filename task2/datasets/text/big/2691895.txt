R-Point
{{Infobox film
| name           = R-Point
| image          = R-Point film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | rr             = Al pointeu
 | mr             = Al p‘oint‘ŭ}}
| director       = Kong Su-chang
| producer       = Choi Kang-hyeok Jang Yun-hyeon
| writer         = Kong Su-chang
| starring       = Kam Woo-sung Son Byung-ho Oh Tae-kyung Park Won-sang Lee Sun-kyun Ahn Nae-sang Kim Byeong-cheol Jeon Kyeong-ho Mun Yeong-dong
| music          = Pa-lan Dal
| cinematography = Seok Hyeong-jing
| editing        = Nam Na-yeong
| distributor    = Cinema Service
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| gross          = $6,744,984 
}} South Korean military in Vietnam. Most of the movie was shot in Cambodia. Bokor Hill Station plays a prominent part of the movie, in this case doubling as a colonial French plantation.

==Synopsis==
On 7 January 1972, the South Korean base in Nha-Trang, Vietnam, receives a radio transmission from a missing platoon that has been presumed dead. The high-command assigns the veteran and decorated Lieutenant Choi Tae-in to lead a squad of eight other soldiers to extract the missing soldiers from the rendezvous point or R-Point. When they arrive at the location, they engage and defeat a Vietnamese woman with a machine gun. They later find a tombstone sayinging that one hundred years previously, local Vietnamese were killed by the Chinese who disposed of them in a lake. A temple built over the site has become a sacred location to the Vietnamese. They find an immense, but empty mansion, where they set up their base. They have a week to find the missing soldiers, but time is passing slowly when one of them dies in a horrific way, and instead of finding answers, they encounter further mysteries......

==Marketing==
Before the film was released, filmmakers conducted viral marketing to promote the film. The official website, www.rpoint.com, carried several fictional articles such as a journal written by an American war correspondent, statements made by various soldiers who witnessed events portrayed innthe film, radio transmissions supposedly received by Korean soldiers, internet news links about missing Korean soldiers in Vietnam, and a fictional timeline of R-Point.

==Cast==
*Kam Woo-sung - Lt. Choi Tae-in
*Son Byung-ho - Sgt. Jin Chang-rok

==Awards and nominations==
;2004 Grand Bell Awards 
* Best Sound
* Nomination - Best New Director - Kong Su-chang

;2004 Blue Dragon Film Awards
* Nomination - Best Supporting Actor - Son Byung-ho
* Nomination - Best New Director - Kong Su-chang

;2004 Korean Film Awards
* Nomination - Best Supporting Actor - Son Byung-ho
* Nomination - Best New Director - Kong Su-chang

==See also==
*Korean horror

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 