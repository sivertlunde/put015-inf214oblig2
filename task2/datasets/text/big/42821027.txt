My Last Day (film)
 
 
 
{{Infobox film name           = My Last Day image          = My Last Day poster.jpg caption        = Anime released poster director       = Barry Cook producer       = Barry Cook Studio 4°C studio         = Studio 4°C The JESUS Film Project distributor    = The JESUS Film Project Campus Crusade for Christ released       =   runtime        = 9 minutes language       = English
}}

My Last Day is a 2011 Christian  . This is a short film of regret, repentance and redemption.

The anime film is currently available in various languages including English language|English, Japanese language|Japanese, Spanish language|Spanish, Bengali language|Bengali, Korean language|Korean, and Russian language|Russian.

==Plot==
The film begins with a prisoner watching from the prison bars as Jesus gets flogged in Pilate’s courtyard. He remembers Jesus teaching and wonders why they’re hurting an innocent man. Horrified, he remembers his own crime: He’s in an alley with a rich gentleman. Holding him up with a knife, he tries to take a box of coins and belongings. The thief is nervous so when he tries to go after the man, he fumbles. The man fights him with the box. Coins go flying. The thief accidentally stabs the man in the struggle. He claws at the coins and runs off. The flashback ends. He then breaks down in tears, aggrieved at seeing someone innocent suffer an even more brutal punishment as a criminal than himself.

Most people in the crowds in the courtyard scream for Jesus to be crucified. The thief, another man (who is also a thief) and Jesus are loaded with the beams for their crosses and march to Golgotha. As they make their way there, the thief looks curiously to the crowds, some of who scream that Jesus is innocent. 

They finally arrive at Golgotha and nails are driven through their wrists, painfully securing them to the patibulum. Each man is then hung on their cross due to a large, deep square-shaped groove carved into the stipes and their feet are nailed to a wooden sedile. They hang in agony. Like the crowd, the other thief demands that Jesus save Himself and them. But our thief rebukes the statement, claiming that they are receiving the same punishment that Jesus is, even though He has done no wrong. The thief then asks that Jesus remember him when He enters into His kingdom. Jesus promises him that today they will be in paradise together. A while later, a dark storm overwhelms the hill and Jesus dies.

Afterwards, the thief has his ankles broken and passes away with a gasp. He then sees Jesus (who greets him by reciting John 11:25) in a beautiful place which is, just as Jesus promised, paradise.
  in the Temple of Jerusalem.]]

==Characters==

* Jesus
* The thief
* The second thief


==Release==

 


==Reception==

 

== References ==
 

#  12 April 2011
# . Anime News Network 15 April 2010
# . GMA Network 6 April 2012
#  BuzzFeed 15 April 2011
#  The Christian Post 15 April 2011
#  April 2011

== External links ==
* 
* 
* 
*  (Official anime website)

 
 
 
 
 


 