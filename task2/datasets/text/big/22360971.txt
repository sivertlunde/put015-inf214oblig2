Metello
 
{{Infobox Film
| name           = Metello
| image          = Metello.jpg
| caption        = Film poster
| director       = Mauro Bolognini
| producer       = Gianni Hecht Lucari Fausto Saraceni
| writer         = Luigi Bazzoni Mauro Bolognini Suso Cecchi dAmico Ugo Pirro Vasco Pratolini
| starring       = Massimo Ranieri
| music          = Ennio Morricone
| cinematography = Ennio Guarnieri
| editing        = Nino Baragli
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Metello is a 1970 Italian drama film directed by Mauro Bolognini. It was entered into the 1970 Cannes Film Festival. It starred Massimo Ranieri as the title character.

==Plot==
During the end of the 19th century, young Metello decides to start a new life after the sudden loss of his parents. He meets a young girl, Ersilia. They get married and have a son. Metello becomes involved in a political activity at work and is arrested. He then lies that he wont be involved in the activity anymore, and as a result, has to flee town. He arrives at his new destination, and meets Viola, a middle-aged school teacher. He then has an affair with her, and as a result, she gives birth to his daughter. Towards the end of the film, Metello realizes that he really belongs back home with Ersilia.

==Cast==
* Massimo Ranieri - Metello
* Ottavia Piccolo - Ersilia Frank Wolff - Betto
* Tina Aumont - Idina
* Lucia Bosé - Viola
* Pino Colizzi - Renzoli
* Mariano Rigillo - Olindo
* Luigi Diberti - Lippi
* Manuela Andrei - Adele Salani
* Corrado Gaipa - Badolati
* Adolfo Geri - Del Bueno
* Claudio Biava - Moretti
* Franco Balducci - Chellini
* Steffen Zacharias - Pallesi
* Sergio Ciulli

==Awards== for best Best Actress at the 1970 Cannes Film Festival.   

==References==
 

==External links==
* 
   
 
 
 
 
 
 
 
 
 
 
 