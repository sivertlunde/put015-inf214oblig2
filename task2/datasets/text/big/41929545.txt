Territoriu de bandolerus
{{Infobox film
| name           = Territoriu de bandolerus
| image          = 
| caption        = 
| director       = Néstor del Barco and José Ignacio Cobos
| producer       = El Duendi
| writer         = José Ignacio Cobos
| narrator       = 
| starring       = People of Serradilla
| music          = Según se mire, Aulaga folk, Chambbábilon, Javier Martín y Pedro Antonio Fernández
| cinematography = 
| editing        = Néstor del Barco
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Spain
| language       = Extremaduran
| budget         = 
}} Extremaduran for Territory of Bandits) is a Extremaduran film directed by Néstor del Barco and José Ignacio Cobos and produced by Pilar Cobos and Eduardo Gómez from the cultural association El Duendi. It is the first film made entirely in Extremaduran language (Serradilla dialect). It tells the story of the bandit El Cabrerín. It is based on actual events of the nineteenth century.

Filming is located in the village of Serradilla, province of Cáceres and its surroundings, including the Monfragüe| Monfragüe National Park. Both cast and crew consisted of Serradilla locals. This movie has the distinction of starting as a no budget film, with members of the Serradilla community contributing resources in a collective effort. 

The film premiered on August 17, 2013 in Serradilla, with over 3000 attendees (the village census records 1729 inhabitants). 

== References ==
 

 
 
 
 


 