Britannia of Billingsgate
{{Infobox film
| name           = Britannia of Billingsgate
| image          = 
| image_size     = 
| caption        = 
| director       = Sinclair Hill
| producer       = Michael Balcon
| writer         = George Moresby-White  Ralph Stock   Sewell Stokes (play)   Christine Jope-Slade (play)
| music          = Bretton Byrd
| narrator       = 
| starring       = Violet Loraine   Gordon Harker   Kay Hammond   John Mills
| distributor    = Gaumont British Distributors
| released       = 1933
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British Musical musical comedy film directed by Sinclair Hill and starring Violet Loraine, Gordon Harker, Kay Hammond and John Mills. A family who work in the fish trade at Billingsgate Market encounter a film crew who are shooting there.  It was based on the play Britannia of Billingsgate by Christine Jope-Slade and Sewell Stokes.

==Cast==
* Violet Loraine - Bessie Bolton
* Gordon Harker - Bert Bolton
* Kay Hammond - Pearl Bolton
* John Mills - Fred Bolton
* Drusilla Wills - Mrs Wrigley
* Walter Sondes - Harold Hogarth
* Glennis Lorimer - Maud Anthony Holles - Guidobaldi
* Joyce Kirby - Joan
* Gibb McLaughlin - Westerbrook
* Grethe Hansen - Gwen
* Wally Patch - Harry
* Ernest Sefton - Publicity man

==References==
 

==External links==
 

 

 
 
 
 
 
 


 
 