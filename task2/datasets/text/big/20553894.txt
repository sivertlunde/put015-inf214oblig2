Wind Across the Everglades
{{Infobox Film name           = Wind Across the Everglades caption        =
| image	=	Wind Across the Everglades FilmPoster.jpeg director       = Nicholas Ray producer       = Stuart Schulberg writer         = Budd Schulberg starring       = Burl Ives Christopher Plummer Gypsy Rose Lee Chana Eden Mackinlay Kantor Emmett Kelly music          = Paul Sawtell Bert Shefter cinematography = Joseph C. Brun editing        = Georges Klotz Joseph Zigman distributor    = Warner Bros. released       = September 11, 1958 runtime        = 93 min. country        =   USA awards         = language  English
|budget         = preceded_by    =
}}

Wind Across the Everglades is a 1958 film directed by Nicholas Ray. Ray was fired from the film before production was finished, and several scenes were completed by screenwriter Budd Schulberg, who also supervised the editing. 

The film features Christopher Plummer in his first lead role (and his second film role overall) and, in a minor role, Peter Falk in his film debut. Former stripper Gypsy Rose Lee and circus clown Emmett Kelly also are among those in an unusual cast.
 on location in Everglades National Park  in Technicolor.

==Plot synopsis==
  plume hunters in the Everglades. 

==Critical reputation==
Due to Rays having been fired from the production before the film was completed, Wind Across the Everglades holds a contentious place in film scholarship. In a short review of the film, critic   and Bitter Victory) are made all the more piquant here by his feeling for folklore and outlaw ethics as well as his cadenced mise en scene." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 