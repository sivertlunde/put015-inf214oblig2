Drowning Ghost
{{Infobox film
| name           = Drowning Ghost
| image          = Drowning Ghost (film).jpg
| image_size     =
| caption        = Swedish DVD cover
| director       = Mikael Håfström
| producer       = Hans Lönnerheden
| writer         = Lars Yngwe Vasa Johansson Mikael Håfström
| narrator       =
| starring       = Rebecka Hemse Jesper Salén Jenny Ulving
| music          = Anders Ehlin
| cinematography = Peter Mokrosinski
| editing        = Darek Hodor SFI
| distributor    = Columbia TriStar Films AB
| released       = 15 October 2004
| runtime        = 100 minutes
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} Swedish slasher film directed by Mikael Håfström and written by Lars Yngwe Vasa Johansson and Håfström. It stars Rebecka Hemse, Jesper Salén and Jenny Ulving.

==Plot==
The movie opens at a boarding school, at the time of the big party before summer break. While most of the students are outside listening to the headmasters speech one student, Rebecka, is seen showering and writing a letter before jumping off the schools tower to her death.

A year later the student Sara, who witnessed the suicide, is attending her final year at the school. Two new students, math genius Felix and diplomat son Leo, start to study at school. Sara and her friend Therese start to tell them of the local legend, Strandvaskaren (the Drowning Ghost). A century ago a farmer slaughtered three male students at the school before drowning himself in the nearby lake. He is believed to return once every year on the day of his death to haunt the school, and there is a tradition among the students to have a party at that day in the barn where the farmer once lived.

Sara finds out that the farmer killed the students to avenge the rape and murder of his daughter, but it was all covered up by the authorities because the rapists were upper class. At the party, Saras former boyfriend Måns and his friend Ynge try to pull a prank on Sara, but the drunken Ynge tries to rape her. Sara pushes him away and runs away while Måns yells at Ynge for going too far. Sara runs out and finds Måns current girlfriend hanging dead in the hallway. Ynge goes to have some milk and is killed by a fork struck through his head. Måns find the hysterical Sara and tries to calm her down and apologize.

A masked man appears and rams an axe into Måns head as Sara runs off. Sara locks in Leo into a storeroom and calls for Therese and Felix to help. Sara finds an 8&nbsp;mm film in a box that had belonged to Rebecka, and discovers that Rebecka had a brother who has a weird similarity to Felix. Felix shows up and tells Sara that he and Rebecka were siblings. They were separated because their father was mentally unstable and put away. When Rebecka died, he swore to avenge her, dressed up as the Drowning Ghost, and started to kill the students who tormented Rebecka. He asks Sara to not say anything but believes she would any way and tries to kill her. Sara knocks him out and runs away with Therese. Therese falls into the well and breaks her leg. Sara climbs down after her. Morning comes and Felix awakens. He finds Sara and Therese, who are too afraid to call for help, in the well and climbs after them. Sara and Therese crawls down the pipe to the lake but Felix catches up with Therese and kills her off screen. Sara manages to reach the lake and escapes in a boat at the beach. She keeps her eyes on the pipe to see when Felix comes out of there, but falls asleep. When she awakens she has no idea how long she has been sleeping, and Felix is nowhere to be found. Felix tries to climb into the boat from underwater, but Sara cuts his hand by starting the boat engine. Felix tries to attack once more but Sara apparently kills him by bashing the anchor into his back. Felix sinks down to the bottom.

Sara is saved by the police and reunited with Leo on the beach; the police try to fish Felix up but instead find the farmer. Sara and Leo leave the boarding school and graduate at a school in Stockholm. During the graduation day eight months after the events, with all parents and friends of the many students present, Sara looks out over the sea of people and suddenly sees a sign with the words written in red paint "My beloved sister Rebecka". The film ends with Sara realizing that even though she cannot see him in the crowd Felix is still alive and amongst the people watching her and her classmates graduate.

==Graphic scenes==
Drowning Ghost contains many graphic death scenes such as: a suicide, the decapitation of a head (with the body of the man lying with death twitches afterward) and the mans head later appearing on a plate in a graphic manner. Stabbings, drownings, strangulation and axe murders are also visible in the film. 

== Cast ==
*Rebecka Hemse as Sara
*Jesper Salén as Felix
*Jenny Ulving as Therese
*Peter Eggers as Leo Daniel Larsson as Måns Weine Rebecca Ferguson as Amanda
*Anders Ekborg as Thomas
*Kjell Bergqvist as Peder Weine
*Anders Ahlbom as Bengt Tommy Andersson as Pappan
*Sasa Bjurling as Rebecka
*Alma Duran as Rebecka as a child
*Patrick Gillenhammar as Felix as a child
*Oskar Thunberg as Doctor 1
*Stig Engström as Professor Ek
*Erik Hultqvist as Markus
*Kerstin Steinbach as Laila
*Magnus Ehrner as Doctor 2
*Bojan Westin as Anna-Lisa
*Frans Wiklund as Yngve
*Filip Benko as Student

==Production== Svenska Filminstitutet in Gärdet, Stockholm, Sweden.

==Release==
The film premiered on 13 May 2004 as part of the Cannes Film Market. Strandvaskaren had a 15+ age limit when shown in cinemas in Sweden on the premiere on 15 October 2004. The film was released Drowning Ghost on 22 August 2004 at Screamfest Horror Film Festival. 

== Criticism == Friday the 13th movies. 

In his review, Nils Nordgren stated "and just where are we one of the beach bagss biggest problems: the script. Its sprawling, full of incredible and senseless incident. The fact that Anders Ehlin music in the films opening is a plagiarism of John Carpenters famous piano loop in Halloween makes it painfully direct." 

==Prizes==
Strandvaskaren won awards for best director and best soundtrack of the year at the Screamfest Los Angeles Horror Film Festival. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 