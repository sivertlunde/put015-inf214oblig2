...denn die Musik und die Liebe in Tirol
 
{{Infobox film
| name           = ...denn die Musik und die Liebe in Tirol
| image          = 
| alt            =  
| caption        = 
| director       = Werner Jacobs
| producer       = Karl Heinz Busse Johannes J. Frank
| writer         = Max Rottmann
| starring       = Vivi Bach Claus Biederstaedt Hannelore Auer Gunther Philipp
| music          = Gert Wilden
| cinematography = Dieter Wedekind
| editing        = Elisabeth Kleinert-Neumann
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}}
 musical comedy film directed by Werner Jacobs and starring Vivi Bach, Claus Biederstaedt and Hannelore Auer.

==Cast==
* Vivi Bach - Susanne Berger 
* Claus Biederstaedt - Fred 
* Hannelore Auer ...  Monika 
* Gunther Philipp ...  Herbert Petunius - Unterhaltungsboß vom Fersehen 
* Trude Herr - Rehlein 
* Hubert von Meyerinck - Oskar Ortshaus 
* Gus Backus - Peter 
* Franz Muxeneder - Krummblick 
* Hugo Lindinger - Narbe 
* Gerd Vespermann - Paul 
* Inge Kuntschnigg - Renate 
* Aliha Krause - Gertie 
* Marie France - Yvonne 
* Elke Sommer   
* Peppino Di Capri

==External links==
* 

 

 
 
 
 
 
 
 


 