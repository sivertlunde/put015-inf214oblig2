Aces Go Places IV
 
 
{{Infobox film
| name           = Aces Go Places IV
| image          =
| caption        =
| director       = Ringo Lam
| producer       = Karl Maka
| writer         = Karl Maka
| narrator       =
| starring       = Sam Hui Karl Maka
| music          = Tony Lo
| cinematography = Sander Lee
| editing        = Tony Chow Wong Ming-Gong
| distributor    = Cinema City Co. Ltd.
| released       =    
| runtime        =
| country        = Hong Kong
| language       = Cantonese
| budget         =
}}
Aces Go Places IV ( ) also known in the United States as Mad Mission 4: You Never Die Twice is a 1986 Hong Kong action-comedy film directed by Ringo Lam, and starring Sam Hui and Karl Maka.

==Cast==
* Sam Hui - King Kong
* Karl Maka - Albert Au
* Sally Yeh - Sally Bright
* Sylvia Chang - Supt. Nancy Ho
* Ronald Lacey - Leader of the villains
* Kwan Tak Hing - Police hockey team coach
* Onno Boelee - Hornsby (credited as Onno Boulee)
* Roy Chiao - The Professor
* Cho Tat-wah - Hua
* Gayle-Anne Jones - Henchwoman
* Peter McCauley - Digger (credited as Peter Macaully)
* Shih Kien - Interpol Boss
* Cyrus Wong
* Pomson Shi
* Sandy Dexter

 
 

==References==
 

==See also==
* Aces Go Places (film series)|Aces Go Places film series

==External links==
* 

 

 
 
 
 
 
 
 

 
 