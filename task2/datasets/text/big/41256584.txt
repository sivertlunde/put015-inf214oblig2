The Dinner (1998 film)
{{Infobox film
 | name = The Dinner
 | image = La cena.jpg
 | caption =
 | director =  Ettore Scola
 | writer =  Ettore Scola Furio Scarpelli  Silvia Scola Giacomo Scarpelli
 | starring =  
 | music =  Armando Trovajoli
 | cinematography =  Franco Di Giacomo	   
 | editing =  	 Raimondo Crociani 
 | producer =   
 }} 1998 Cinema Italian commedia allitaliana film directed by Ettore Scola.   

For their performance the male ensamble cast won the Nastro dArgento for Nastro dArgento for Best Supporting Actor, while Stefania Sandrelli won the Nastro dArgento for Best Supporting Actress.   

== Cast ==
* Fanny Ardant : Flora
* Antonio Catania : Mago Adam
* Francesca dAloja : Alessandra Riccardo Garrone : Diomede
* Vittorio Gassman : Maestro Pezzullo
* Giancarlo Giannini : Professore
* Marie Gillain : Allieva
* Nello Mascia : Menghini
* Adalberto Maria Merli : Bricco
* Stefania Sandrelli : Isabella Lea Gramsdorff: Sabrina 
*Corrado Olmi: Arturo, husband of Flora
*Eros Pagni: Duilio
*Daniela Poggi: the stranger 
*Giorgio Tirabassi: Franco 
*Giorgio Colangeli: Vincenzo Petrosillo

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 