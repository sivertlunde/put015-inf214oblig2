A Jitney Elopement
{{Infobox film | name = A Jitney Elopement
 | image = A Jitney Elopement (poster).jpg
 | caption = Theatrical poster to A Jitney Elopement
 | director = Charlie Chaplin
 | producer = Jess Robbins
 | writer = Charlie Chaplin
 | starring = Charles Chaplin   Lloyd Bacon   Ernest Van Pelt   Edna Purviance   Leo White
 | music = Robert Israel (Kino video release)
 | cinematography = Harry Ensign
 | editing = Charlie Chaplin
 | distributor = Essanay Studios   General Film Company
 | released = April 1, 1915
 | runtime = 33 minutes
 | language = Silent film  English (original intertitles)
 | country = United States
 | budget =
 | preceded_by =
 | followed_by =
 }}
 
 Essanay Films. San Franciscos Golden Gate Park and the large windmills still on the parks west side.
 

==Synopsis==
Purviances father wants her to marry wealthy Count Chloride de Lime. Chaplin, Purviances true love, impersonates the Count at dinner.

==Cast==
* Charles Chaplin - Suitor, the Fake Count
* Edna Purviance - Edna
* Ernest Van Pelt - Ednas father
* Leo White - Count Chloride de Lime, Ednas Suitor
* Lloyd Bacon - Young Butler / Cop (uncredited) 
* Paddy McGuire - Old Butler/Cop
* Bud Jamison - Cop with Baton
* Carl Stockdale - Cop
* Fred Goodwins - Undetermined role

==External links==
* 
* 
*   on YouTube
 

 
 
 
 
 
 
 
 
 
 
 


 