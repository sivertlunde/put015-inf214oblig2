Salaat (film)
 
{{Infobox film
| name           = Salaat
| image          = 
| alt            =  
| caption        = 
| director       = Kaz Rahman
| producer       = Kaz Rahman
| writer         = Kaz Rahman
| starring       = Hédi Hurban Sara Rahman Nasreen Umran Yazici Zainulvara Zaheer
| music          = Hédi Hurban
| cinematography = Kaz Rahman
| editing        = Kaz Rahman
| studio         = Charminar Films
| distributor    = 
| released       =   
| runtime        = 74 minutes  
| country        = Canada India USA
| language       = 
| budget         = 
| gross          = 
}}
Salaat is a 2010 art film by Kaz Rahman. 

==Synopsis== Zohar (mid-day), Isha (evening). The film begins with Isha and ends the next day at Maghrib with each prayer being performed in real time amidst stunning settings. This structure also offers a glimpse at the beauty, stress and contradictions of people interacting throughout the day. 

==Cast==
* Hédi Hurban as Zohar Lady
* Nasreen as Isha Lady
* Umran Yazici as Fajr Lady
* Sara Rahman as Asr Lady
* Zainulvara Zaheer as Maghrib Lady

==Screenings== Hyderabad  and India Habitat Center at New Delhi in India and as an installation at the 4th Video Arte Festival in Camaguey, Cuba .

==Reception==
Faisal M. Naim wrote in The Hindu "Kaz Rehman has very beautifully depicted the bonding of mankind with its creator in his film Salaat." 

==References==
 

==External links==
*  

 
 
 
 


 