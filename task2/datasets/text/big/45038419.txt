Bhanumati Gari Mogudu
{{Infobox film
| name           = Bhanumati Gari Mogudu
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| story          = P. Kalaimani
| screenplay     = A. Kodandarami Reddy
| producer       = D.V.S.Raju
| director       = A. Kodandarami Reddy
| starring       = Nandamuri Balakrishna Vijayashanti Chakravarthy
| cinematography = Nandamuri Mohana Krishna
| editing        = K. Babu Rao
| studio         = D.V.S.Enterprises
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}

Bhanumati Gari Mogudu ( , Comedy film produced by D.V.S.Raju on D.V.S.Enterprises banner, directed by A. Kodandarami Reddy. Starring Nandamuri Balakrishna, Vijayashanti in the lead roles and music composed by K. Chakravarthy|Chakravarthy.         

==Cast==
 
*Nandamuri Balakrishna as Jayakrishna 
*Vijayashanti as Bhanumati Aswini as Gowri Ranganath as Inspector
*Giri Babu as Raghava Sudhakar as Baddikotu Pakir
*Balaji 
*Rajesh as Mukundam Paruchuri Venteswara Rao as Director 
*Nagesh as Lawyer Chinta Singinadham
*Suthi Velu as Lawyer Kodal Rao
*Ramana Reddy as Swamy Mallikarjuna Rao as Sarpanch 
*KK Sarma as Production Manager
*Chidatala Appa Rao as Appa Rao
*Jagga Rao as Subbaiah
*P. J. Sarma as Public Prosecutor
*Bhimeswara Rao as Bank Manager
*Vankayala Satyanarayana 
*Sri Lakshmi as Namini Mucharlla Aruna as Rani 
*Rekha 
*Anitha  
*Pushpalata as Jayakrishnas mother
*Nirmalamma as Bhanumathis aunt
 

==Soundtrack==
{{Infobox album
| Name        = Bhanumati Gari Mogudu
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 21:02
| Label       = Saptaswar Audio Chakravarthy
| Reviews     =
| Last album  = President Gari Abbai   (1987)  
| This album  = Bhanumati Gari Mogudu   (1987)
| Next album  = Inspector Pratap   (1988)
}}

Music composed by K. Chakravarthy|Chakravarthy. Lyrics written by Veturi Sundararama Murthy. Music released on Saptaswar Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Bhanumathi Pelli Neede  SP Balu
|4:09
|- 2
|Kattukunna Mogude SP Balu, S. Janaki
|4:11
|- 3
|Mallepoovu SP Balu,S. Janaki
|4:07
|- 4
|Jogilangu Jogilangu SP Balu, P. Susheela
|4:13
|- 5
|Vayyaram Adigindi  SP Balu,P. Susheela
|4:22
|}

==Others== Hyderabad

==References==
 

 
 
 
 

 