Prick Up Your Ears
 
{{Infobox film
| name           = Prick Up Your Ears
| image          = Prick up your ears.jpg
| caption        = UK release poster
| director       = Stephen Frears
| producer       =
| writer         = Book:  
| starring       = Gary Oldman Alfred Molina Vanessa Redgrave Wallace Shawn Julie Walters
| music          =
| cinematography = Oliver Stapleton
| editing        =
| distributor    = The Samuel Goldwyn Company
| released       = April 17, 1987
| runtime        =
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = $1,654,743 
}}
 Margaret "Peggy" Ramsay.

==Plot==
The film tells the story of Orton and Halliwell in flashback, framed by sequences of Lahr researching the book upon which the film is based with Ortons literary agent, Peggy Ramsay. Orton and Halliwells relationship is traced from its beginnings at the Royal Academy of Dramatic Arts. Orton starts out as the uneducated youth to Halliwells older faux-sophisticate. As the relationship progresses, however, Orton grows increasingly confident in his talent while Halliwells writing stagnates. They fall into a parody of a traditional married couple, with Orton as the "husband" and Halliwell as the long-suffering and increasingly ignored "wife" (a situation exacerbated by Ortons unwillingness, in 1960s England, to acknowledge having a male lover). Orton is commissioned to write a screenplay for the Beatles and Halliwell gets carried away in preparing for a meeting with the "Fab Four", but in the end Orton is taken away for a meeting on his own. Finally, a despondent Halliwell kills Orton and commits suicide.

==Cast==
* Gary Oldman as Joe Orton
* Alfred Molina as Kenneth Halliwell
* Vanessa Redgrave as Peggy Ramsay
* Frances Barber as Leonie Orton
* Janet Dale as Mrs. Sugden
* Julie Walters as Elsie Orton
* Bert Parnaby as The Magistrate
* Margaret Tyzack as Madame Lambert
* Lindsay Duncan as Anthea Lahr
* Wallace Shawn as John Lahr
* Joan Sanderson as Antheas Mother

==Reception==
Prick Up Your Ears received a generally positive critical reaction, with a 92% "fresh" rating at review aggregator  .  , Ebert described him as "the best young British actor around."  Ken Hanke of the Mountain Xpress was less impressed, describing the film as a "good, but never quite great biopic", while still awarding it four stars out of five. 
 BAFTA Award nomination for Best Actor; Redgrave received BAFTA- and Golden Globe Award nominations for Best Actress in a Supporting Role. Alan Bennett earned a BAFTA Award nomination for Best Adapted Screenplay. The film won the award for Best Artistic Contribution at the 1987 Cannes Film Festival.   

==References==
 

==External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 