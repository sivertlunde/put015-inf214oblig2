Port Djema
{{Infobox film
| name           = Port Djema
| image          = 
| caption        = 
| director       = Eric Heumann
| producer       = Marc Soustras
| writer         = Eric Heumann Jacques Lebas
| starring       = Jean-Yves Dubois
| music          = Sanjay Mishra (Port Djema (Soundtrack)|soundtrack)
| cinematography = Giorgos Arvanitis
| editing        = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
}}

Port Djema is a 1997 French drama film directed by Eric Heumann. It was entered into the 47th Berlin International Film Festival where Heumann won the Silver Bear for Best Director.   

==Cast==
* Jean-Yves Dubois as Pierre Feldman
* Nathalie Boutefeu as Alice
* Christophe Odent as Jérôme Delbos
* Edouard Montoute as Ousman
* Claire Wauthion as Soeur Marie-Françoise
* Frédéric Pierrot as Antoine Barasse
* Frédéric Andréi
* Franco Sardella

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 