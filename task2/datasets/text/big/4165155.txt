The Work and the Glory: American Zion (film)
{{Infobox film
| name = American Zion
| image = WorkAndGlory2_AmericanZion.jpg
| director = Sterling Van Wagenen
| producer = Scott Swofford
| writer = Gerald N. Lund Matt Whitaker Eric Johnson Alexander Carroll Emily Podleski Jonathan Scarfe
| distributor = Vineyard Productions
| released =  
| runtime = 100 min.
| language = English Reed Smoot
| music = Sam Cardon
| budget = $6,500,000
| gross = $2,025,032
}}
 2004 film The Work Joseph Smith.
 Work and the Glory series. 

==Summary==

The date is November 1833, the state of Missouri has turned a blind eye as hundreds of its peaceful inhabitants were hunted down and driven from their homes in the dead of night. Against this impending strife, a young man that has a divine vision leads a people against the aggression of an anti-hero with a vulnerable past.

== Cast ==

* Sam Hennings : Benjamin Steed
* Brenda Strong : Mary Ann Steed Eric Johnson : Joshua Steed
* Alexander Carroll : Nathan Steed
* Brighton Hertford : Melissa Steed
* Kimberly Varadi : Becca Steed
* Colin Ford :  Matthew Steed
* Sera Bastian : Lydia Steed
* Emily Podleski : Jessica Roundy
* Jim Grimshaw : Josiah McBride
*  
*  
*  
* Curtis Andersen : Carl Rogers
* Jeff Ham : Obadiah Cornwell

==References==
Unless otherwise noted, the information in this article is from the DVD release of this film.

==External links==
* 

 
 
 
 
 
 


 
 