Monsignor (film)
{{Infobox film
| name           = Monsignor 
| image          = Monsignorposter.jpg
| image_size     =
| caption        = theatrical release poster
| director       = Frank Perry
| producer       = David Niven Jr. Frank Yablans
| writer         = Jack-Alain Léger (novel) Abraham Polonsky and Wendell Mayes (screenplay)
| narrator       = 
| starring = {{plainlist|
* Christopher Reeve
* Geneviève Bujold
* Fernando Rey Jason Miller Joe Cortese
* Adolfo Celi
* Tomas Milian
}}
| music          = John Williams Billy Williams
| editing        = Peter E. Berger
| distributor    = 20th Century Fox
| released       = October 22, 1982 (US)
| runtime        = 121 minutes
| country        = United States English
| budget         = $10 million 
| gross          = $6.5 million (US/ Canada) 
}}
 1982 drama Roman Catholic priests rise through the ranks of the Holy See|Vatican, during and after World War II.  Along the way, he involves the Vatican in the black marketeering operations of a Mafia crime boss|don, and has an affair with a woman in the postulant stage of becoming a nun. He eventually repents and returns to his faith, attempting to make right the things he has done wrong.
 Jason Miller, Joseph Cortese, Adolfo Celi, and Leonardo Cimino.

It was not well received by critics and performed poorly at the box office; Reeve later blamed this on poor editing. Supporting actors Miller and Rey were singled out for their strong performances. The film was nominated for a Golden Raspberry Award for Worst Musical Score,  the only Razzie nomination John Williams ever received in his career to date.

The filming location was entirely in Rome, Italy.

==Plot==
 Vatican having financial difficulties during World War II, a young priest from America is sent for, recommended because of his accounting skill.

Father John Flaherty does indeed have a good head for figures, but also believes in any means to an end. To raise money for the church, he is willing to enter into a black market operation with the Mafia, selling cartons of cigarettes by the tens of thousands for a percentage of the take.

The priests morals are strained further when he develops a romantic interest in Clara, a young nun who is having a crisis of faith. They begin an affair, but Flaherty does not confess to her his true identity. One day during a papal audience, Clara catches sight of Flaherty in his clerical robes. Her love and trust are shattered.

Flahertys methods may be overlooked, but his success at raising funds is appreciated by some of his superiors as he rises to become a monsignor, then a cardinal. When an ill-advised stock investment costs the Vatican millions, however, Flaherty must pay the price for his deeds.

==Cast==
* Christopher Reeve as Father Flaherty
* Geneviève Bujold as Clara
* Leonardo Cimino as the Pope
* Fernando Rey as Cardinal Santoni
* Adolfo Celi as Cardinal Vinci Jason Miller as Don Appolini
* Tomas Milian as Father Francisco 
* Robert Prosky as Bishop Walkman
* Joe Pantoliano as Musso
* Joseph Cortese as Ludovico Lodo Varese
* Milena Vukotic as Sister Verna 
* Joe Spinell as Brides Father
* Ettore Mattia as Pietro
* Gregory Snegoff as Soldier 
* Pamela Prati as Roman Girl

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 