Savyasachi (film)
{{Infobox film|
| name = Savyasachi
| image = 
| caption =
| director = M. S. Rajashekar
| writer =  T. N. Narasimhan
| based on = Vijay Sasanurs novel Prema
| producer = R. Niveditha   N. Prema
| music = Sadhu Kokila
| cinematography = Mallikarjuna
| editing = P. Bhaktavatsalam
| studio = Shashvathi Chitra
| released =  
| runtime = 140 minutes
| language = Kannada
| country = India
| budgeBold text =
}}
 Kannada drama action drama film directed by M. S. Rajashekar and the story by Vijay Sasanur is based on his novel. The film features Shivarajkumar and Prema (actress)|Prema, making her acting debut,    in the lead roles.    The films soundtrack and score is composed by Sadhu Kokila.

== Cast ==
* Shivarajkumar Prema
* Charithra
* Thoogudeepa Srinivas
* Malathi
* Tej Sapru
* Avinash
* Shivaram
* Pruthviraj
* Ravikiran
* H. G. Dattatreya

== Soundtrack ==
The soundtrack of the film was composed by Sadhu Kokila.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Changulabi Thotadalli
| extra1 = S. P. Balasubrahmanyam& K. S. Chithra
| lyrics1 = Sriranga
| length1 = 
| title2 = Raja Rani Naavilokake
| extra2 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Namma Madhuchandra
| extra3 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics3 = Sriranga
| length3 = 
| title4 = Naagara Haave
| extra4 = S. P. Balasubrahmanyam, K. S. Chithra, Rajesh Krishnan & Mangala
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Ee Hrudayada Haadu
| extra5 = Manjula Gururaj
| lyrics5 = M. N. Vyasa Rao
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 