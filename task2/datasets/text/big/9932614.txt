Heartbreak Hotel (film)
{{Infobox film
| name           = Heartbreak Hotel
| image          = Heartbreakhotel1988poster.jpg
| image_size     = 
| caption        = Theatrical release poster Chris Columbus
| producer       = Debra Hill Lynda Obst
| writer         = Chris Columbus
| starring       = David Keith Tuesday Weld Charlie Schlatter
| music          = Georges Delerue
| cinematography = Steve Dobson
| editing        = Raja Gosnell
| studio         = Touchstone Pictures Silver Screen Partners III Buena Vista Pictures
| released       = September 30, 1988
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $13 million
| gross          = $5,509,417 (USA)
}} Chris Columbus and starring David Keith and Tuesday Weld. Set in 1972, the story deals with one of the many "legends" involving Elvis Presley (Keith) about his fictional kidnapping, and his subsequent redemption from decadence.
 Green Pastures the former residence of John Henry Faulk.

==Plot==
The story opens on a single mother and her two children, a teenage son and nine-year-old daughter, who run a boarding house.  The mother is hurt in a car accident after her drunkard boyfriend crashed, so the son kidnaps her favorite singer, Elvis Presley, for her birthday, getting the owner of a local pizzeria who looks eerily like Elvis mother to pose as his mothers ghost as a distraction.  Elvis awakens, after being drugged by the boy, in the boarding house.  Elvis and the boy dont get along at first.  The boy has no respect for the 70s Elvis saying that he sold out to Vegas.  Through the course of the movie, however, the boy and Elvis get to know each other and develop a friendship.  Elvis even plays "Heartbreak Hotel" with the boys band at a talent show.

==Cast==
*David Keith as Elvis Presley
*Tuesday Weld as Marie Wolfe
*Charlie Schlatter as Johnny Wolfe
*Angela Goethals as Pam Wolfe

==Reception==

The movie received mixed reviews.    It currently holds a 38% rating on Rotten Tomatoes based on 13 reviews. 

===Box office===

The movie was not successful. 

==Music==

Most of the songs contained in the movie are actual Elvis Presley recordings despite the film being fictional, with David Keith and Charlie Schlatter performing the title track in the style of the 1968 television special recording.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 