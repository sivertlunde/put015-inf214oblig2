Half Moon Street
 
{{Infobox film
| name           = Half Moon Street
| image          = Half moon street post.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Bob Swaim
| producer       = Geoffrey Reeve Edward Behr Bob Swaim Paul Theroux
| narrator       = 
| starring       = Sigourney Weaver Michael Caine Patrick Kavanagh Faith Kent Ram John Holder
| music          = Richard Harvey
| cinematography = Peter Hannan
| editing        = Richard Marden
| studio         = RKO Pictures
| distributor    = 20th Century Fox
| released       = 13 August 1986
| runtime        = 90 min
| country        = United Kingdom United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 escort service who becomes involved in the political intrigues surrounding one of her clients. The film was directed by Bob Swaim, and stars Sigourney Weaver, Michael Caine, and Patrick Kavanagh.
 Jet Pilot, made in 1957.

The film was based on the 1984 novel Doctor Slaughter by Paul Theroux.

==Plot==

Dr. Lauren Slaughter is an American woman of considerable intellect, an expert on China who now lives in London, working for a "think tank".
 paid escort to lonely men. One such, identifying himself by a fake name, turns out to be Lord Bulbeck, a trusted House of Lords member with a key role in national defense.

The two strike up a relationship that goes beyond their lovemaking, enjoying each others conversation and intelligence. But during a delicate peace negotiation in the Middle East, investigators doing a background check on Lord Bulbeck come upon his relationship with the high-priced call girl, someone they suspect might have an ulterior motive.

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 

 
 