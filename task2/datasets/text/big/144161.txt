Conspiracy Theory (film)
{{Infobox film
| name           = Conspiracy Theory
| image          = Conspiracy theory poster.jpg
| alt            = 
| caption        = Original poster
| director       = Richard Donner
| producer       = Richard Donner Joel Silver
| writer         = Brian Helgeland
| starring       = {{Plainlist | 
* Mel Gibson
* Julia Roberts
* Patrick Stewart
}}
| music          = Carter Burwell
| cinematography = John Schwartzman
| editing        = Kevin Stitt Frank J. Urioste
| studio         = Silver Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 135 minutes
| language       = English
| budget         = $80 million
| gross          = $136,982,834   
}} action thriller film directed by Richard Donner.
 eccentric taxicab|taxi government conspiracies, and the U.S. Justice Department attorney (Julia Roberts) who becomes involved in his life.

The movie was a financial success,  but critical reviews were mixed. 

==Plot==
  Justice Department. Jerry Fletcher (Mel Gibson), a conspiracy-theory obsessed New York City taxi driver, continually expounds his ideas to her. Alice humors him because he once saved her from a mugging, but does not know he has been spying on her at home. Her own obsession is to solve the mystery of her fathers murder.

Seeing suspicious activity everywhere, Jerry identifies some men as CIA workers, follows them into a building, and is captured. He wakes up bound to a wheelchair. A doctor (Patrick Stewart) injects him with LSD, and interrogates him using torture. Jerry experiences terrifying hallucinations and flashbacks, panics, and manages to escape, incapacitating the doctor by biting his nose. Although injured, Jerry makes his way to Alices office, eventually collapsing.
 FBI and CIA psychiatrist, Dr. Jonas, with a bandaged nose. Meanwhile Jerry fakes a heart attack and escapes again, with Alices help. In Jerrys hospital room, she meets an FBI agent named Lowry. While they are examining Jerrys personal items, the CIA arrive and confiscate everything. She declines Lowrys offer to work with her, and later finds Jerry hiding in her car. They go to Jerrys apartment where he tells her about the conspiracy newsletter he produces. Just when Alice has decided Jerry is crazy, a SWAT team breaks in. Jerry sets everything on fire and they leave by his secret trapdoor exit. In the room below, there is a large mural on the wall, which features both Alice on her horse and the triple smokestacks of a factory.

The pair go to Alices apartment and he accidentally reveals hes been watching her through her window. Furious, she kicks him out. Outside, Jerry confronts Lowry and his partner staking out her place, and he warns them, at gunpoint, not to hurt her. He goes to a book store and, as he has compulsively done in the past, buys a copy of Catcher in the Rye, even though hes never read it. The electronic record of the purchase alerts agents to his location. Jerry sees their operatives rappelling down from black helicopters and hides in a theater, escaping by causing a panic.

Alice calls each person on the newsletter mail-list and finds that all have recently died except one. Jerry uses a ruse to get her out of the office, and then immobilizes the operatives watching her. During their escape, he tells her that he loves her, then flees on a subway train when she brushes off his feelings.

She goes to see the last surviving person on the subscription list, and finds it is Jonas. He explains that Jerry was brainwashed to become an assassin, and claims that Jerry killed her father. She agrees to help find Jerry.

Jerry sends Alice a message to meet him. They ditch the agents following them and he drives her to her fathers private horse stables in Connecticut, but Alice secretly calls her office so that Jonas can track her. At the stables, Jerry remembers that he was sent to kill her father (a judge who was about to expose Jonas operation) but found he could not. Instead, Jerry promised to watch over Alice before the judge was killed by another assassin. Jonas men capture Jerry, and a sniper tries to get Alice, but she escapes.

Jonas tortures Jerry again. Meanwhile, Alice finds Lowry and forces him at gunpoint to admit that he is not FBI, but from a "secret agency that watches the other agencies" and has been using the unwitting Jerry to uncover and stop Jonas. Alice goes to the site of the smokestacks from Jerrys mural and sees a mental hospital next door. There she bribes an attendant to show her an unused wing, breaks in, and finds Jerry. As Jonas catches them, Lowry arrives with his men and attacks Jonas men. Jerry attempts to drown Jonas, but is shot. Alice shoots Jonas dead. Alice tells Jerry she loves him before he is taken away in an ambulance.

Some time later, Alice visits Jerrys grave, leaving a pin he gave her upon it, before returning to horse riding. Jerry and Lowry are watching from a car. Jerry agrees not to contact her until all of Jonas other subjects are caught, but she joyfully finds the pin attached to her saddle.

==Cast==
* Mel Gibson as Jerry Fletcher
* Julia Roberts as Alice Sutton
* Patrick Stewart as Dr. Jonas
* Cylk Cozart as Agent Lowry
* Steve Kahan as Mr. Wilson Terry Alexander as Flip
* Pete Koch as Fire Captain
* Dean Winters as Cleet
* Sean Patrick Thomas as Surveillance Operator
* Joan Lunden as TV Announcer
* Rick Hoffman as Night Security
* Richard Donner as Cab Passenger
* Tom Schanley as Lawyer

==Production notes== extras acting as passengers were not told what Gibson was going to say because Donner wanted their reactions to be as spontaneous and realistic as possible.
 Union Square, Greenwich Village, the Queensboro Bridge, Roosevelt Island, and the Westchester Medical Center in Valhalla, New York.
 The Police, and two renditions of "Cant Take My Eyes Off You", one by Frankie Valli and the other by Lauryn Hill.

==Reception==

===Critical response===
  
Review aggregation website Rotten Tomatoes gives the film a score of 52% based on reviews from 42 critics.   

In her review in The New York Times, Janet Maslin said, "The only sneaky scheme at work here is the one that inflates a hollow plot to fill 2¼ hours while banishing skepticism with endless close-ups of big, beautiful movie-star eyes ... Gibson, delivering one of the hearty, dynamic star turns that have made him the Peter Pan of the blockbuster set, makes Jerry much more boyishly likable than he deserves to be. The man who talks to himself and mails long, delusional screeds to strangers is not usually the dreamboat type ... After the story enjoys creating real intrigue  ... it becomes tied up in knots. As with too many high-concept escapades, Conspiracy Theory tacks on a final half-hour of hasty explanations and mock-sincere emotion. The last scene is an outright insult to anyone who took the movie seriously at its start." 

Lisa Schwarzbaum of Entertainment Weekly graded the film B- and commented, "Richard Donner ... switches the movie from a really interesting, jittery, literate, and witty tone poem about justified contemporary paranoia (and the creatively unhinged dark side of New York City) to an overloaded, meandering iteration of a Lethal Weapon project that bears the not-so-secret stamp of audience testing and tinkering." 

In the San Francisco Chronicle, Mick LaSalle stated, "If I were paranoid I might suspect a conspiracy at work in the promoting of this movie—to suck in audiences with a catchy hook and then give them something much more clumsy and pedestrian ... Conspiracy Theory can be enjoyed once one gives up hope of its becoming a thinking persons thriller and accepts it as just another diversion ... When all else fails, there are still the stars to look at—Roberts, who actually manages to do some fine acting, and Gibson, whose likability must be a sturdy thing indeed." 
 indie production where the daffy dialogue and weird characters could weave their coils of paranoia into great offbeat humor. Unfortunately, the parts of the movie that are truly good are buried beneath the deadening layers of thriller cliches and an unconvincing love story ... If the movie had stayed at ground level—had been a real story about real people—it might have been a lot better, and funnier. All of the energy is in the basic material, and none of it is in a romance that is grafted on like an unneeded limb or superfluous organ." 

In Rolling Stone, Peter Travers said, "The strong impact that Gibson makes as damaged goods is diluted by selling Jerry as cute and redeemable. Instead of a scalding brew of mirth and malice, served black, Donner settles up a tepid latte, Decaffeination|decaf. What a shame—Conspiracy Theory could have been a contender." 

Todd McCarthy of Variety (magazine)|Variety called the film "a sporadically amusing but listless thriller that wears its humorous, romantic and political components like mismatched articles of clothing ... This is a film in which all things ... are treated lightly, even glibly ... One can readily sympathize with ... the directors desire to inject the picture with as much humor as possible. But he tries to have it every which way in the end, and the conflicting moods and intentions never mesh comfortably." 
 secret team radical right.   

===Box office ===
   Air Force One as the number 1 film. The film eventually grossed $75,982,834 in the U.S. and $61,000,000 internationally for a total worldwide gross of $136,982,834. This final gross qualified Conspiracy Theory as the 19th highest grossing film in the U.S. in 1997. 

==References==
 

==External links==
*  
*  
*   - at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 