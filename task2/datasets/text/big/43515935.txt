Vacanze di Natale '90
 
{{Infobox film
| name = Vacanze di Natale 90
| image = Vacanze di Natale 90.jpg
| caption = 
| director = Enrico Oldoini
| writer = Franco Ferrini Enrico Oldoini
| starring = Massimo Boldi  Christian De Sica  Diego Abatantuono
| music = Giovanni DellOrso
| cinematography =Sergio Salvati
| editing =  Raimondo Crociani
| producer =  Aurelio De Laurentiis language = Italian
| country = Italy runtime = 101 min released = 1990
}}  1990 Cinema Italian comedy film directed by Enrico Oldoini.         

== Plot==
In residence in the mountains of St Moritz, intertwine the stories of five characters. Nick, a poor waiter, wins a bet on a horse race, but he loses his voice. So he goes on vacation meets a rich lady. Bindo and Toni, old friends, meet them after an argument, and one falls for the wife of the other. Arturo and Beppe, also friends, participating in races with runners, stay in a chalet near the residence of St Moritz. Arturo also falls in love with a woman, but she is the wife of his best friend...
 
== Cast ==

* Massimo Boldi: Bindo
* Christian De Sica: Toni
* Andrea Roncato: Beppe
* Ezio Greggio: Arturo Zampini
* Diego Abatantuono: Nick
* Corinne Cléry: Alessandra
* Moira Orfei: Gloria
* Giannina Facio: Rita
* Maria Grazia Cucinotta: Arabella

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 