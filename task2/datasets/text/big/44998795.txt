The Source (1918 film)
{{Infobox film
| name           = The Source
| image          =
| alt            = 
| caption        = 
| director       = George Melford
| producer       = Jesse L. Lasky
| screenplay     = Monte M. Katterjohn Clarence Budington Kelland
| starring       = Wallace Reid Ann Little Theodore Roberts Raymond Hatton James Cruze Noah Beery, Sr. Nina Byron
| music          = 
| cinematography = Paul P. Perry 	
| editor         =
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 lost  drama silent film directed by George Melford and written by Monte M. Katterjohn and Clarence Budington Kelland. The film stars Wallace Reid, Ann Little, Theodore Roberts, Raymond Hatton, James Cruze, Noah Beery, Sr. and Nina Byron. The film was released on September 8, 1918, by Paramount Pictures.  

==Plot==
 

==Cast==
*Wallace Reid as Van Twiller Yard
*Ann Little as Svea Nord
*Theodore Roberts as Big John Beaumont
*Raymond Hatton as Pop Sprowl
*James Cruze as Langlois
*Noah Beery, Sr. as John Nord
*Nina Byron as Ruth Piggins Charles West as Paul Holmquist
*Gustav von Seyffertitz as Ekstrom 
*Charles Ogle as Sim-Sam Samuels (*Charles Stanton Ogle)

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 