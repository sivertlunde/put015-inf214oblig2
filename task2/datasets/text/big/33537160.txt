At Night All Cats Are Crazy
 
{{Infobox film
| name           =At Night All Cats Are Crazy
| image          = 
| image_size     = 
| caption        = 
| director       =Gérard Zingg
| producer       =
| writer         =Gérard Zingg and Philippe Dumarçay
| narrator       = 
| starring       =Gérard Depardieu
| music          =Jean-Claude Vannier
| cinematography = 
| editing        =Hélène Viard
| distributor    = 
| released       =1977
| runtime        =124 min
| country        = France French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

At Night All Cats Are Crazy ( , lit. At night all cats are grey) is a French film by Gérard Zingg released in 1977 in film|1977.

== Synopsis ==
Charles Watson tells Lily, his 10-year-old niece, a story about a character called Philibert, a bad boy. Lily wants to meet the latter and loses herself in a world where fiction and reality are mixed up. Her uncle believes as far as hes concerned, that he possesses literary gifts and loves living in the greatest of comforts.

== Datasheet ==
* Directed by : Gérard Zingg
* Screenplay : Gérard Zingg and Philippe Dumarçay
* Music : Jean-Claude Vannier
* Editing : Hélène Viard
* Casting : Margot Capelier
* Set designer : Jean-Pierre Kohut-Svelko
* Distribution : Accatone Distribution, France
* Production France 3 Cinéma (France), Prodis (France) et Sam Films
* Producer : Pierre Hanin
* Country : France
* Release date : 23 novembre 1977 (France)
* Genre : Comedy drama
* Duration : 124 min

== Cast ==
* Gérard Depardieu : Philibert Larcher
* Robert Stephens : Charles Watson
* Tsilla Chelton :   Banalesco
* Laura Betti : Jacqueline
* Charlotte Crow : Lily
* Albert Simono : Gaston, aka "Gastounet"
* Virginie Thévenet : Jeannette
* Dominique Laffin : The shop assistant
* Raoul Delfosse : The bald man
* Ann Zacharias : The white cat
* Gabriel Jabbour : Mr Banalesco
* Irina Grjebina : The withered beauty
* Lily Fayol : The joyful widow Jean Lemaître : M. Chatin
* Roger Muni : The barman
* Gérard Hernandez : The punter
* Delvin Sandoz : The child
* Fernand Berset : The presenter
* Jacques Chailleux : The painter
* Julien Verdier : The man in his sixties
* Yves Peneau : The opera employee
* Roger Schwartz : The inspector
* Rudy Lenoir : The taxi driver
* Michel Pilorgé : Nanard
* Jean Luisi : The boy from the wagon-restaurant
* Madeleine Bouchez : The old woman from the train
* Raoul Guylad : The policeman of the opera
* Jean-Pierre Lorrain : The barman
* Raymonde Vattier : The usher
* Lyle Joyce

==External links==
*  
*   sur festival-lumiere.orge

 
 
 
 

 