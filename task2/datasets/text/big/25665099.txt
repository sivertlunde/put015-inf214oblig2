The French Kissers
{{Infobox film 
| name           = The French Kissers
| image          = The French Kissers poster.jpg
| caption        = International theatrical poster
| director       = Riad Sattouf
| producer       = Anne-Dominique Toussaint
| writer         = Riad Sattouf Marc Syrigas
| starring       = Vincent Lacoste Anthony Sonigo Alice Trémolières Noémie Lvovsky
| music          = Flairs
| cinematography = Dominique Colin
| editing        = Virginie Bruant
| studio         = Les Films des Tournelles
| distributor    = Pathé
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = $3,5 million
| gross          = $14,751,272 
}}
The French Kissers is a 2009 French teen film. Its original French title is Les Beaux Gosses, which means "the handsome boys".    It was written and directed by Riad Sattouf, marking his film debut. The film follows Hervé (Vincent Lacoste), an average teenage boy who has little luck with finding a girlfriend until the beautiful Aurore (Alice Trémolières) takes a liking to him.

Sattouf, a graphic novel writer, was invited to write a script based on an idea from producer Anne-Dominique Toussaint, and he completed the screenplay with Marc Syrigas. Sattouf cast non-professional actors as the films teenage characters, but he chose to use experienced actors such as Noémie Lvovsky, Irène Jacob, Emmanuelle Devos and Valeria Golino as the adult characters. Filming took place over eight weeks in Gagny and Rennes.

The film was released in France on 10 June 2009, and a soundtrack composed by Flairs was released on 8 June 2009. The film was well received by critics, who particularly praised the humour, the acting and the cinematography.
 2010 César Award for Best First Feature Film and Lacoste also received a nomination for the César Award for Most Promising Actor. It also won the Prix Jacques Prévert du Scénario for Best Adaptation in 2010.

==Plot==
Hervé (Vincent Lacoste) is a teenage boy in junior high school with ordinary looks and middling grades, living with his single mother in a housing estate in Rennes. He and his best friend Camel (Anthony Sonigo) often fantastize about their female classmates and their mothers, but have less luck with girls in reality.

Hervé unsuccessfully pursues romances with various girls at his school, including with Laura (Julie Scheibling), who accepts his offer of a date as a joke. After Aurore (Alice Trémolières), a beautiful and popular girl at school, asks him on a date, they embark on an awkward relationship. Although Hervé and Camel are frequent masturbators, while both alone and together, Hervé and Aurore are slow to engage in sexual activity beyond kissing. Aurore eventually breaks up with Hervé when his friends try to grope her in a game of Dungeons & Dragons and she discovers that he lied to them about having sex with her.

The film concludes with the characters in high school; Hervé is dating Sabrina, Camel is dating Jenifer, Aurore is dating Wulfran, and Hervés mother is married to Anass father.

==Cast==
* Vincent Lacoste as Hervé
* Anthony Sonigo as Camel
* Alice Trémolières as Aurore
* Julie Scheibling as Laura
* Camille Andreys as Meryl
* Robin Nizan-Duverger as Benjamin
* Baptiste Huet as Loïc
* Simon Barbery as Mohamed
* Irwan Bordji as Anas
* Loreleï Chenet as Mégane
* Sihem Namani as Sadia
* Salomé Durchon as Nolwenn
* Noémie Billy as Océane
* Emma Gregory as Emma
* Thania Perez as Jenifer
* Lise Bordenave as Sabrina
* Louis Bankowsky as Goulven
* Nicolas Bouissy as Koulmen
* Pablo Eskenazi as Pablo
* Victorien Rolland as Wulfran
* Yanis Aït-Ali as Mahmoude
* Maya De Rio Campo as Leslie
* Florence Dottel as Françoise
* Noémie Lvovsky as Hervés mother
* Irène Jacob as Aurores mother
* Yannig Samot as Anass father
* François Hassan Guerrar as Camels father
* Christophe Vandevelde as Hervés father
* Emmanuelle Devos as the principal
* Roch Amédet Banzouzi as the deputy principal

French astronaut Jean-Pierre Haigneré makes a cameo appearance as the professor of Physics.

==Production==
 
Graphic novel writer Riad Sattouf had studied animation and had dreams of filmmaking,  but he thought that it would be too exhausting to write and re-write a script, find producers, find funding and censor his ideas to please others.  Sattouf had rejected several film offers before because they lacked creative freedom,    but he agreed when he was contacted by Anne-Dominique Toussaint, a film producer and a fan of Sattoufs graphic novels who wanted to make a teen film.  Sattouf wrote the first draft of the script and then asked screenwriter Marc Syrigas to help him to re-write it.  Rather than make a film about "the codes of todays teens, the way they talk,   their arsenal of electronic devices", Sattouf wanted to focus on "the intensity of their emotions".  He wanted to depict "the things that happen during teenage-hood that we dont show so often". 

Casting all of the characters in The French Kissers took place over three months.  Sattouf cast the teenage characters through casting agent Stéphane Batut &ndash; whose team he described as "experts in casting teenagers" &ndash; and chose from taped auditions of 500 Parisian high school students.  He did not want to work with professional actors who he feared would be egotistical and may have thought that he was not legitimate as a first-time director.    He wanted to cast unknown actors due to his "phobia of stars", but after considering that he might not make another film, he asked Emmanuelle Devos, Irene Jacob and Valeria Golino to appear in the film and they all accepted.  Sattouf sought actors who were "ugly ducklings with unusual features, and their own way of talking, of walking" and who could express emotions without "acting".  He struggled to find teenage actors who were like their characters and were willing to be filmed in an unflattering way. He said that Vincent Lacoste and Anthony Sonigo were less concerned about their appearances when he showed them a picture of himself as a "very ugly" teenager.  Three days before the beginning of principal photography, Lacoste broke his knee at a music concert, and so his resulting limp was added to the character.  According to Sattouf, Lacoste and Sonigo were completely at ease with filming scenes of masturbation, and all of the actors treated their French kisses "like a hug". 

Filming of The French Kissers lasted for eight weeks.    Some street scenes and interiors of Hervés flat were filmed in Rennes, Brittany &ndash; where the film is set &ndash; but the budget was too small for substantial filming in Brittany, so Sattouf tried to find a college in the Paris suburbs that resembled his own in Rennes.  Most of the films interior scenes were shot in Gagny in Pariss eastern suburbs at the Madame-de-Sévigné de Gagny and Eiffel de Gagny colleges.  Sattouf used close-ups and unusual compositions when filming the teenagers to "feel their animal side". There was little camera movement to show "there is some heaviness in them; the world is turning around them, they are their own prisoners".  He wanted the camera to be held "so close that you could feel their oily skin, every imperfection, and smell their  ". 

==Soundtrack==
{{Infobox album
 | Name        = Les beaux gosses: Bande originale du film
 | Type        = Soundtrack
 | Artist      = various artists
 | Cover       = The French Kissers soundtrack.jpg
 | Released    =  
 | Length      = 37:03
 | Label       = Naïve Records
 | Producer    = 
}}
The films original music was written by French electropop musician Lionel Flairs, who performs under the moniker Flairs. It was his first composition for film. When he was approached by Sattouf to compose the soundtrack, Flairs was reluctant because he preferred total freedom when writing music, but after he agreed he enjoyed collaborating with Sattouf. The soundtrack is less like Flairss usual "happy pop" because Sattouf wanted a sad feel to reflect the sadness in the characters lives. 

{{tracklist
| collapsed       = yes
| headline        = Track listing
| extra_column    = Performer(s)
| title1          = Hervé et Leslie et le rateau
| extra1          = Vincent Lacoste, Maya De Rio Campo
| length1         = 0:08
| title2          = Levretto
| extra2          = Flairs, Riad Sattouf
| length2         = 2:21
| title3          = Hervé et le linge sale
| extra3          = Vincent Lacoste, Noémie Lvovsky
| length3         = 0:11
| title4          = Poursuite!
| extra4          = Flairs, Riad Sattouf
| length4         = 0:48
| title5          = On ssert les coudes
| extra5          = Moktar Nassif Et Les Cousins Hariri
| length5         = 1:59
| title6          = Hervé, Camel et la Redoute
| extra6          = Vincent Lacoste, Anthony Sonigo
| length6         = 0:06
| title7          = Les mystères de la branlette
| extra7          = Flairs, Riad Sattouf
| length7         = 0:40
| title8          = Radio chaussette
| extra8          = Flairs, Riad Sattouf
| length8         = 0:37
| title9          = Maman la coquine
| extra9          = Noémie Lvovsky
| length9         = 0:04
| title10         = Ici ou là
| extra10         = Moktar Nassif Et Les Cousins Hariri
| length10        = 1:27
| title11         = Aurore Vole
| extra11         = Vincent Lacoste, Alice Trémolières
| length11        = 0:32
| title12         = Bus
| extra12         = Flairs, Riad Sattouf
| length12        = 0:55
| title13         = Anas et la bite humaine
| extra13         = Irwan Bordji
| length13        = 0:12
| title14         = Mettre les voiles
| extra14         = Ginny Goldswinger
| length14        = 1:54
| title15         = Hervé, Camel et les femmes matures
| extra15         = Vincent Lacoste, Anthony Sonigo
| length15        = 0:26
| title16         = La danse du ragga
| extra16         = Ric
| length16        = 3:58
| title17         = Hervé, Camel et les coulaisons
| extra17         = Vincent Lacoste, Anthony Sonigo, Frédéric Neidhart
| length17        = 0:34
| title18         = Run Camel!
| extra18         = Flairs, Riad Sattouf
| length18        = 0:31
| title19         = Pub rencontres bretonnes
| extra19         = Flairs, Riad Sattouf
| length19        = 0:13
| title20         = Parc
| extra20         = Flairs, Riad Sattouf
| length20        = 0:59
| title21         = Pub festival de rap breton
| extra21         = Flairs, Riad Sattouf
| length21        = 0:26
| title22         = Aurore et le secret des filles
| extra22         = Vincent Lacoste, Alice Trémolières
| length22        = 0:47
| title23         = Éjaculo
| extra23         = Flairs, Riad Sattouf
| length23        = 1:07
| title24         = Hervé, Mégane, le rateau
| extra24         = Vincent Lacoste, Loreleï Chenet
| length24        = 0:14
| title25         = Téléphone arabe
| extra25         = Flairs, Riad Sattouf
| length25        = 1:14
| title26         = Hervé le lover
| extra26         = Vincent Lacoste, Anthony Sonigo
| length26        = 0:18
| title27         = Baby
| extra27         = Booba
| length27        = 4:32
| title28         = Camel larabe de Satan
| extra28         = Anthony Sonigo, Thania Perez
| length28        = 0:07
| title29         = Bus 2
| extra29         = Flairs, Riad Sattouf
| length29        = 0:26
| title30         = Hervé aime
| extra30         = Vincent Lacoste, Alice Trémolières
| length30        = 0:44
| title31         = Scooter Blanc
| extra31         = Flairs, Riad Sattouf
| length31        = 1:57
| title32         = You Think Youre a Man
| extra32         = The Vaselines
| length32        = 5:36
}}

==Reception== The Gazette American Pie John Hughess At the Movies awarded the film 3.5 stars out of 5, describing it as "a likeable insight into the problems of children who want to grow up very quickly". He called the non-professional teenage cast "terrific", and praised Lvovskys performance as Hervés mother.  Herald Sun critic Leigh Paatsch found the film "blatantly funny" but also "surprisingly moving". He called Sattouf "a knockout talent behind the camera" and praised the "great humour and honesty" of the story. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 