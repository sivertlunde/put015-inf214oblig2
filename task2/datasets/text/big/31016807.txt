Armed Response (film)
 
{{Infobox film
| name           = Armed Response 
| image          =
| image_size     = 
| caption        = 
| director       = Fred Olen Ray
| producer       = Paul Hertzberg
| screenplay     = T.L. Lankford
| story          = Fred Olen Ray T.L. Lankford Paul Hertzberg
| narrator       =  Mako
| music          = Thomas Chase Steve Rucker
| cinematography = 
| editing        = 
| distributor    = Cinetel Films
| released       = October 3, 1986 (USA)
| runtime        = 86 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Armed Response (1986) is an action/crime movie, starring David Carradine and Lee Van Cleef and directed by Fred Olen Ray.

== Plot ==
One of the men of dangerous Tanaka has subtracted a statuette that he had planned to use as a peace offering between local Yakusa and the rival Chinese Tong. It hired two private investigators for the exchange of bailout money to restore the statue, but the business to recover the statue gets worse and Clay Roth is killed. This infuriates the Roth brothers and father, all combat veterans, and go in search of justice

== Cast ==
*David Carradine as Jim Roth   
*Lee Van Cleef as Burt Roth  Mako as Akira Tanaka
*Lois Hamilton as Sara Roth  
*Brent Huff as Tommy Roth  
*Ross Hagen as Cory Thorton  
*Dick Miller as Steve  
*Burr DeBenning as Lt. Sanderson

==External links==
*  

 
 
 
 


 