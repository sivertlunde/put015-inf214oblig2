The Master Key (1914 serial)
 
{{Infobox film
| name           = The Master Key
| image          = Themasterkey 1914 newspaperad.jpg
| caption        = Contemporary newspaper advertisement.
| director       = Robert Z. Leonard
| producer       = 
| writer         = Robert Z. Leonard Harry Carter
| music          = 
| cinematography = 
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

The Master Key is a 1914 American film serial directed by Robert Z. Leonard. It is considered to be lost film|lost, with only episode 5 of 15 surviving in the Library of Congress.   

==Cast==
* Robert Z. Leonard - John Dore
* Ella Hall - Ruth Gallon Harry Carter - Harry Wilkerson
* Jean Hathaway - Jean Darnell
* Alfred Hickman - Charles Everett
* Wilbur Higby - Tom Gallon
* Charles Manley - Tom Kane Jack Holt - Donald Faversham
* Jim Corey - Wah Sing
* Allan Forrest
* Mack V. Wright
* Rupert Julian
* Cleo Madison
* Edward A. Mills
* James Robert Chandler - (as Robert Chandler)
* Marc Robbins

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 