River's End (1930 film)
{{Infobox film
| name           = Rivers End
| image          = Rivers_End_1930_Poster.jpg
| image_size     =
| caption        =
| director       = Michael Curtiz
| producer       =
| writer         = Charles Kenyon
| based on       = Novel The Rivers Edge by James Oliver Curwood
| narrator       =
| starring       = Charles Bickford Evalyn Knapp
| music          =
| cinematography = Robert Kurrle
| editing        = Ralph Holt
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
Rivers End  is a 1930 talkie|all-talking drama film directed by Michael Curtiz and starring Charles Bickford and Evalyn Knapp. Bickford plays two roles, a Royal Canadian Mounted Police (RCMP) sergeant and the man he is after. The film is the second of three adaptations of the bestselling novel The Rivers Edge by James Oliver Curwood, the others being released in 1920 and 1940.

== Plot ==
In remote northern Canada, Sergeant Conniston (Bickford) seeks to capture escaped convicted murderer Keith (also played by Bickford). He is accompanied by OToole (J. Farrell MacDonald), a guide who is constantly drunk. When he finally catches his quarry, he is shocked to find that they look exactly alike.

On their way back to the RCMP post, however, their sled overturns. Keith takes Connistons gun and sled and leaves the policeman and his guide to die in the snow. Keith starts to feel guilty about what he has done. He turns back and takes the men to an emergency cabin. In spite of this, Conniston dies of a frozen lung.

After talking to Keith for a while, OToole becomes convinced of his innocence. He coaches Keith so that he can pass himself off as the sergeant. OToole is not well enough to travel, so Keith goes to the RCMP post alone.

Once he arrives, Keith tells McDowell (David Torrence), the post commanding officer, that it was Keith who died. McDowell then informs him that Keith was innocent; the real murderer confessed. Worried that he will be accused of Connistons murder if his true identity is discovered, Keith plans to escape across the border.

There are complications. Miriam (Evalyn Knapp), McDowells daughter, had been Connistons girl, but she decided to break up with him. Keith is very much attracted to her, and proves to be much more romantic than Conniston. Miriam finds herself falling in love with him.

Mickey (Frank Coghlan, Jr.), OTooles young son, had adopted Conniston as a substitute father. He eventually realizes that Keith is not the sergeant, but Keith manages to persuade him to keep his secret.
 gauntlet of angry Mounties and boards a ship, accompanied by Mickey. At the last 
minute, Miriam boards as well.

==Cast==
*Charles Bickford as Keith and Sergeant Conniston 
*Evalyn Knapp as Miriam 
*J. Farrell MacDonald as OToole 
*Zasu Pitts as Louise 
*Walter McGrail as Martin 
*David Torrence as McDowell 
*Frank Coghlan, Jr. as Mickey 
*Tom Santschi as Shotwell

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 