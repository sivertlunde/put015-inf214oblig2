Chatur Singh Two Star
 
{{Infobox film
| name           = Chatur Singh Two Star
| image          = Chatur Singh Two Star.jpg
| caption        = Theatrical release poster
| director       = Ajay Chandhok
| producer       = Mohammad Aslam
| writer         = 
| screenplay     = Rumi Jaffery
| story          = Rumi Jaffery Sai Kabir
| based on       =  
| starring       = Sanjay Dutt Ameesha Patel Suresh Menon
| music          = Sajid-Wajid
| cinematography = Rajeev Shrivastava
| editing        = Nitin Rokade
| studio         = 
| distributor    = Pen India Pvt. Ltd. Lotus Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =   
| gross          = 
}} The Pink Panther film. 

The film was a critical and commercial failure as it received negative reception from both critics and cinema-goers.

==Plot==
Bumbling cop Chatur Singh (Sanjay Dutt) is sent on a special mission to South Africa to solve a high-profile case involving the murder of a politician and a cache of diamonds. But before he can redeem his botched-up career he must deal with a bunch of loonies which includes a crazy mafia don (Satish Kaushik), a weird taxi driver (Sanjai Mishra), a hysterical boss (Anupam Kher) and a pretty damsel in distress (Ameesha Patel). 

==Cast==
* Sanjay Dutt as Chatur Singh 2 Star / James Armani
* Ameesha Patel as Sonia Varma
* Suresh Menon as Purushutam Singh (Pappu Panther)
* Anupam Kher as Commissioner Rajpal Sinha / Dr Jhatka
* Vishwajeet Pradhan as DGP Kulkarni
* Satish Kaushik as Gullu Gulfam
* Shakti Kapoor as Don Chuma
* Gulshan Grover as Agriculture Minister Y.Y. Singh Sanjay Mishra Lallanpur
* Murli Sharma as Tony
* Rati Agnihotri as Savitri Y. Singh
* Indira Krishnan as Sonias Sister
* Ganesh Yadav as Inspector Yadav

==Production==
The film was announced in late 2007 with Sanjay Dutt cast for the role of Chatur Singh.  During June 2008, filming began in India and the following schedule was filmed in Cape Town, South Africa during July and August in the same year.   In 2009, some of the remaining portions were completed and the film moved to its post-production stage in early 2010. 

The theatrical trailer of the film was released on 21 July 2011 online and also in cinemas with Singham (film)|Singham (2011).

== Reception ==

===Critical===
Most critics were dismissive. Vandana Krishnan of Behindwoods rated it as 0.5 out of 5 and said "Chathur Singh 2 star is not even worth half a star."    of Bollywood Hungama awarded one and a half stars saying "Chatur Singh Two Star tries hard to make you laugh, but fails in its endeavour."  Rediff.com gave it zero stars, too.  Nupur Barua of fullhyd.com called it a sorry excuse of a movie, warning "you may get suicidal at the end of it". 

===Box office===
Chatur Singh Two Star earned 30&nbsp;million in its full theatrical run. The film grossed 130&nbsp;million worldwide. It was declared a huge flop everywhere.

==Soundtrack==
{{Infobox album| 
| Name        = Chatur Singh Two Star
| Type        = soundtrack
| Artist      = Sajid-Wajid
| Cover       = 
| Released    =  
| Recorded    = 
| Producer    =  Feature film soundtrack
| Length      = 
| Label       = Shemaroo Entertainment
}}

The films soundtrack is composed by Sajid-Wajid with lyrics penned by Jalees Sherwani, Shabbir Ahmed, Junaid Wasi, Asif Ali Baig.  The soundtrack was released to YouTubes official channel on 29 July 2011.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| title1 = Chandni Chowk Se
| extra1 = Sajid-Wajid, Hard Kaur
| length1 = 04:18
| title2 = Ishqan Da Ishqan Da
| extra2 = Sonu Nigam, Shweta Pandit
| length2 = 04:20
| title3 = Jungle Ki Heerni Hoon
| extra3 = Sunidhi Chauhan
| length3 = 04:40
| title4 = Murga Anda Dega
| extra4 = Sanjay Dutt, Suzanne DMello
| length4 = 04:35
| title5 = Singh Singh Singh
| extra5 = Sanjay Dutt, Sajid-Wajid|Wajid, Khurram Iqbal, Asif Ali Baig
| length5 = 04:10
| title6 = Jungle Ki Heerni Hoon — Remix
| extra6 = Sunidhi Chauhan
| length6 = 04:30
| title7 = Singh Singh Singh — Remix
| extra7 = Sanjay Dutt, Sajid-Wajid|Wajid, Khurram Iqbal, Asif Ali Baig
| length7 = 04:20
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 