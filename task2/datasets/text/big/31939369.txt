Yamanam
{{Infobox film
| name           = Yamanam
| image          = Yamanam.jpg
| image_size     =  Archana and Bharath Gopi at the shooting location 
| director       = Bharath Gopi
| producer       = Ajayan Varicolil
| story          = 
| screenplay     = George Onakkoor
| Art director   = Rajeev Anchal Archana Ramachandran Santha Devi Syama Nedumudi Venu
| music          = G. Devarajan
| lyricist       =  Dr. Ayyappa Panicker
| cinematography = Suresh P Nair
| editing        = Venugopal
| distributor    = 
| released       = 1992
| runtime        = 99 minutes
| country        = India
| language       = Malayalam
}} Malayalam film, directed by Bharath Gopi, and starring Archana (actress)|Archana, Santha Devi, Syama and Nedumudi Venu. 

==Plot==
A physically disabled girl Ambili (Archana (actress)|Archana) is restricted to a wheelchair because of a childhood attack of polio. Unable to move out of home, she has only to fantasize the exteriors. She turns terribly introvert, fathoming the truth within and becoming positive towards people. She is skilled at making models and toys. She and her widowed mother are gradually sidelined in their own home by her brother Devan and his wife Raji.
She and her mother feel isolated when her brother and wife leaves home. Then one of the house models made by Ambili is sold for a good sum, and she becomes more energetic about her work. 
A doctor proposes to marry Ambili, but she refuses because of the realization that his feeling are only based on sympathy. 

==Awards==
The film won the following awards:
 National Film Awards – 1992  Best Film on Other Social Issues Best Supporting Actress – Santha Devi

; Kerala State Film Awards - 1991  Best Background Music - G.Devarajan

== References ==
 

 

 
 
 
 

 