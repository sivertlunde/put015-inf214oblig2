Moustapha Alassane, cinéaste du possible
 
{{Infobox film
| name           = Moustapha Alassane, cinéaste du possible
| image          = 
| caption        = 
| director       = Maria Silvia Bazzoli, Christian Lelong
| producer       = Cinédoc Films
| writer         = 
| starring       = 
| distributor    = 
| released       = 2009
| runtime        = 93 minutes
| country        = France
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Christian Lelong
| editing        = François Sculier
| music          = 
}}

Moustapha Alassane, cinéaste du possible is a 2009 documentary film.

== Synopsis ==
Moustapha Alassane is a living legend in African cinema. His adventures take us to the era of “pre-cinema”, to the times of magical lantern and Chinese shadows. He is the first director of Nigerien cinema and animation films in Africa. He tells very old stories with current technology, but he also narrates the most current events with the most archaic means. This documentary not only tells the adventure of a human being and an extraordinary professional, but the memories of a generation, the history of a country, Niger, in its golden age of cinema.   

== References ==
 
 

 
 
 
 
 
 
 
 
 
 


 
 