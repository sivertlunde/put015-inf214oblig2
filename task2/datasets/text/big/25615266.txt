The Challenge (1960 film)
{{Infobox film
| name           =The Challenge
| image          = The Challenge 1960.jpg
| image size     =
| caption        = 
| director       = John Gilling
| producer       = John Temple-Smith
| writer         = John Gilling
| narrator       = 
| starring       = Jayne Mansfield and Anthony Quayle
| music          = Bill McGuffie
| cinematography = Gordon Dines
| editing        = Alan Osbiston and John Victor-Smith
| studio         = Alexandra	
| distributor    = J. Arthur Rank Film Distributors (UK)
| released       = 17 May 1960	 (London) (UK)
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded by    = 
| followed by    = 
}}
The Challenge, released as It Takes a Thief in the United States,  is a 1960 British crime film directed by John Gilling and starring Jayne Mansfield and Anthony Quayle. 

==Plot==
Mansfield plays Billy, a ruthless gang leader who dumps gang member Kristy when he is convicted of robbery. Kristy serves his time, then recovers the stolen loot when hes released. Billy and her gang then kidnap Kristys son and demand money as ransom.   

==Cast==
*Jayne Mansfield	... 	Billy
*Anthony Quayle	... 	Jim
*Carl Möhner	... 	Kristy Peter Reynolds	... 	Buddy
*Barbara Mullen	... 	Ma Piper Robert Brown	... 	Bob Crowther
*Dermot Walsh	... 	Detective Sergeant Willis
*Patrick Holt       ... 	Max
*Edward Judd	... 	Detective Sergeant Gittens John Bennett	... 	Spider
*Lorraine Clewes	... 	Mrs. Rick Percy Herbert	... 	Shop Steward John Stratton	... 	Rick
*Liane Marelli	... 	Striptease Artiste
*Bill McGuffie	... 	Nightclub Pianist

==Critical reception==
*TV Guide wrote, "most of the actors, with the exception of Quayle, are pretty stiff, and the story is hardly inspired."  Peter Reynolds, Edward Judd, Dermot Walsh and Patrick Holt. Some of the best moments, though, are provided by Hollywoods Jayne Mansfield as the criminal mastermind, demure in black wig and horn-rimmed glasses as she does her firms books by day, but slinking around in sequins by night with a smile and a song." 

==External links==
* 
* 

==References==
 

 

 
 
 
 
 
 


 
 