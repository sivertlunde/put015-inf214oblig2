American Wedding
{{Infobox film
| name           = American Wedding
| image          = American  Wedding movie.jpg
| image_size     = 215px
| alt            = The infamous pie from the first movie takes the place of a traditional wedding cake, providing a series in-joke. Stiflers position behind Jim on the poster represents the characters ascended prominence in the film.
| caption        = Theatrical release poster
| director       = Jesse Dylan Chris Moore Chris Bender Paul Weitz Chris Weitz
| writer         = Adam Herz
| based on       =  
| starring       = Jason Biggs Alyson Hannigan Eddie Kaye Thomas Thomas Ian Nicholas Seann William Scott Eugene Levy
| music          = Christophe Beck
| cinematography = Lloyd Ahern
| editing        = Stuart Pappé
| studio         = LivePlanet Zide/Perry Productions Universal Pictures
| released       =  
| runtime        = 96 minutes  
| country        = United States Germany
| language       = English
| budget         = $55 million 
| gross          = $231,449,203   
}} American Pie and American Pie 2. It is the third (originally intended final) installment in the American Pie (series)#Theatrical films|American Pie theatrical series. It was written by Adam Herz and directed by Jesse Dylan. Another sequel, American Reunion, was released nine years later. This also stands as the last film in the series to be written by Herz, who conceptualized the franchise.

Though the film mainly focuses on the union of Jim Levenstein and Michelle Flaherty, for the first time in the series, the story centers on Steve Stifler, and his outrageous antics including his attempt to organize a bachelor party, teaching Jim to dance for the wedding, and competing with Finch to win the heart of Michelles lovely sister, Cadence.

==Plot==
While on a date in a fancy restaurant with Michelle Flaherty, Jim Levenstein is ready to ask her to marry him. However, his dad still has to arrive with a ring because he forgot it. He tries to stall the question, causing her to think that he actually wants oral sex. She goes under the table and gives Jim fellatio just as Jims dad arrives with the ring, causing her to bump her head on the table and attract the attention of the entire restaurant. Still, Michelle accepts the proposal.

High school pals Kevin Myers and Paul Finch serve as groomsmen, while the MILF (slang)|MILF Guys - John and Justin - proclaim themselves as ushers. Unfortunately, the gangs obnoxious friend Steve Stifler crashes the couples engagement party. The gang tries to keep the wedding a secret from Stifler, fearing he might ruin everything but after he finds out about it, he instantly comes up with an idea to throw Jim a huge bachelor party, upsetting Jim.

Jim is worried about dancing at the wedding, but salvation comes in the form of Stifler, who has taken dance lessons. Stifler agrees to teach Jim to dance on the condition he be allowed to attend the wedding and plan the bachelor party; Jim demands that Stifler tone down his obnoxious personality for Michelles parents in exchange. Meanwhile, the wedding dress Michelle finally settles on after long hours of searching is made by only one designer working for one store, so the boys set out to find the dressmaker for her. They go to Chicago looking for "Leslie Sommers"; Stifler unwittingly walks into a gay bar, and his raucous behavior gets him into a dispute with several of the bars patrons. It turns out Leslie Sommers is actually a man, who agrees to make the dress after witnessing a dance-off between Stifler and Bear. Stifler earns the respect of Bear, with the latter even offering to provide strippers for Jims bachelor party.
 submissive and dominant roles with them.

The party is abruptly halted by the unexpected return of Jim, Harold and Mary. With assistance from Bear, who poses as a butler named "Mr. Belvedere", Jim nearly succeeds in keeping the activities a secret, until Michelles mother opens a closet door and is shocked to find Kevin inside, stripped to his boxers and tied to a chair (following a kinky game with the strippers). The boys explain that it was an attempt to make Jim seem like a hero that went horribly wrong, and Michelles parents accept this explanation, and tell him that if he puts that much effort into the upcoming marriage, she can give him her blessing.

As the ceremony draws near, a series of mishaps occur, including Jims grandmother being displeased that Michelle is not Jewish, Stifler accidentally feeding the ring to the dogs, and Jim shaving his pubic hair, then disposing of it too close to a vent that causes it to be set loose all over the wedding cake. On the night before the wedding, Stifler inadvertently disrupts the walk-in refrigerators power supply while retrieving a bottle of champagne, essentially turning it into an oven and killing the many flowers put together for the ceremony. Previously, Stifler, unaware of Cadences presence, had revealed his true rude and obnoxious personality. Angered and stunned, Jim asks him to leave, and all the others, including Cadence, support Jims decision.

Feeling guilty for his thoughtless behavior, Stifler convinces the local florist to put together a new batch of flowers, and he enlists the help of his football players and Bear. As a gesture of remorse, he also gives a rose to Cadence, much to the amazement of Jim and Michelle. Moved by his actions, Cadence agrees to have sex with him before the ceremony, but Stiflers presence is delayed by a brief thank-you meeting Jim calls among his groomsmen, citing how he is grateful to have friends like them to back him up when he is in need. Quickly returning to the hotel, Stifler hears someone in the supply closet and steps inside, but due to the closets poor lighting, it is only when he gets inadvertently walked in on that Stifler realizes hes actually having sex with Jims grandmother (who becomes pleasant, especially towards Stifler after the act), who was placed in the closet by John and Justin to stop her constant complaining.

Despite the chaotic events leading up to it, Michelle and Jim eventually get married. At the reception, the newly married couple dances while Stifler dances with Cadence. Meanwhile, Finch is sitting by himself when Stiflers mom arrives. Although agreeing they are over each other, Stiflers mom mentions having a double suite and invites Finch to join her. The film ends with John and Justin spying on Stiflers mom and Finch in her suites couple-size bathtub, having oral sex.

==Cast==
{{columns-list|40em|
* Seann William Scott as Steve Stifler
* Jason Biggs as Jim Levenstein
* Alyson Hannigan as Michelle Flaherty
* Thomas Ian Nicholas as Kevin Myers
* Eddie Kaye Thomas as Paul Finch
* Eugene Levy as Noah Levenstein, Jims Dad
* January Jones as Cadence Flaherty
* Deborah Rush as Mary Flaherty
* Fred Willard as Harold Flaherty
* Molly Cheek as Jims Mom
* Angela Paton as Grandma Levenstein
* Eric Allan Kramer as Bear
* Amanda Swisten as Fraulein Brandi
* Nikki Schieler Ziering as Officer Krystal
* Lawrence Pressman as Coach Marshall
* Reynaldo Gallegos as Leslie Summers
* Loren Lester as Celebrant
* Justin Isfeld as Justin (aka MILF Guy #1)
* John Cho as John (aka MILF Guy #2)
* Jennifer Coolidge as Stiflers Mom Julie Payne as Ms. Zyskowsky
}}

==Soundtrack==
The films soundtrack includes songs by Van Morrison, Blue October, The Working Title, Foo Fighters, Feeder, Avril Lavigne, American Hi-Fi, Sum 41, the All-American Rejects, Joseph Arthur, New Found Glory, and Hot Action Cop. Badly Drawn Boy and The Libertines also have songs in the feature. Note that most songs used were already singles. And, this is the first film to feature the song "Laid (song)|Laid" (Matt Nathanson covering the band James (band)|James) in both the trailers and the opening sequence. Notably, it is also the only film in the series to not play the song "Mrs. Robinson" in a scene where Finch has sex with Stiflers mother. It is also the first of the American Pie films not to feature a blink-182 song.

The song "Into the Mystic", played at the end of the film when Jim and Michelle take to the dance floor at the reception, begins as Van Morrisons recording, but midway through it changes to The Wallflowers cover version due to licensing reasons.

The films soundtrack peaked at number 23 on the Billboard 200|Billboard 200 chart.   

{{Infobox album
| Name        = American Wedding
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       =
| Released    = August 1, 2003
| Recorded    = 
| Genre       = 
| Length      = 1:00:13 Universal
| Producer    = 
| Reviews     = 
| Last album  = American Pie 2 Soundtrack 2001
| This album  = American Wedding Soundtrack 2003
| Next album  = American Reunion Soundtrack 2012
}}
{{tracklist
| extra_column  =  Performed by Times Like These
| note1         =
| writer1       =
| extra1        = Foo Fighters
| length1       = 4:26 The Anthem
| note2         =
| writer2       =
| extra2        = Good Charlotte
| length2       = 2:55
| title3        = Forget Everything
| note3         =
| writer3       =
| extra3        = New Found Glory
| length3       = 2:33
| title4        = The Hell Song
| note4         =
| writer4       =
| extra4        = Sum 41
| length4       = 3:19
| title5        = Swing, Swing
| note5         =
| writer5       =
| extra5        = The All-American Rejects
| length5       = 3:54
| title6        = I Dont Give
| note6         =
| writer6       =
| extra6        = Avril Lavigne
| length6       = 3:37 Laid
| note7         =
| writer7       =
| extra7        = Matt Nathanson
| length7       = 3:03
| title8        = The Art of Losing
| note8         =
| writer8       =
| extra8        = American Hi-Fi
| length8       = 3:22
| title9        = Fever for the Flava
| note9         =
| writer9       =
| extra9        = Hot Action Cop
| length9       = 4:03
| title10        = Give Up the Grudge
| note10         =
| writer10       = Gob
| length10       = 2:58
| title11        = Bouncing Off The Walls
| note11         =
| writer11       =
| extra11        = Sugarcult
| length11       = 2:22
| title12        = Come Back Around
| note12        =
| writer12      =
| extra12       = Feeder
| length12       = 3:12
| title13        = Any Other Girl
| note13        =
| writer13      = NU
| length13       = 3:23
| title14        = Beloved
| note14         =
| writer14       =
| extra14        = The Working Title
| length14       = 4:28 Calling You
| note15         =
| writer15       =
| extra15        = Blue October
| length15       = 3:58
| title16        = Honey and the Moon
| note16         =
| writer16       =
| extra16        = Joseph Arthur
| length16       = 4:44
| title17        = Into the Mystic
| note17         =
| writer17       =
| extra17        = The Wallflowers (Van Morrison cover)
| length17       = 3:39
}}

Songs that appear during Stiflers dance in the gay bar:
* "Beat It" - Michael Jackson (only few seconds)
* "Maniac (song)|Maniac" - Michael Sembello
* "Heaven Is a Place on Earth" - Belinda Carlisle
* "Sweet Dreams (Are Made of This)" - Eurythmics
* "Venus (Shocking Blue song)#Bananarama version|Venus" - Bananarama
* "The Reflex" - Duran Duran

Songs that appear during the bachelor party:
*"Summertime Girls" - "Baha Men"

==Release== Freaky Friday.  Closing about 3.5 months later (November 20, 2003), the film had grossed a domestic total of $104,565,114 and $126,884,089 overseas for a worldwide total of $231,449,203, based on a $55 million budget.   Despite being a huge box office success, it is the lowest-grossing film in the series, making roughly $2 million less than American Reunion would in 2012.

American Wedding grossed $15.85 million on DVD and was the number seven DVD rental in 2004. 

==Reception==
American Wedding received mixed reviews from critics. Rotten Tomatoes, a review aggregator, gave the film a rating of 55%, based on 154 reviews, with an average rating of 5.8/10. The sites critical consensus reads, "Raunchier and even more gross than the first two American Pies, American Wedding ought to please fans of the series."   On Metacritic, the film has a score of 43 out of 100, based on 34 critics, which indicates "mixed or average reviews".   

Robert Koeler of Variety (magazine)|Variety compared it to the works of John Waters and called it a "strong finish" for the franchise.   Roger Ebert rated it 3/4 stars and wrote that the film "is proof of the hypothesis that no genre is beyond redemption."   Elvis Mitchell of The New York Times wrote that the film "struggles so hard to be tasteless that its almost quaint."   Mick LaSalle of the San Francisco Chronicle rated it 2/5 stars and called it strained and desperate to find jokes. 

===Awards===
;Wins
* 2004 - MTV Movie Awards for Best Dance Sequence (Seann William Scott)
* 2004 - Teen Choice Awards for Choice Movie Sleazebag (Seann William Scott) and Choice Movie Your Parents Didnt Want You To See

;Nominations
* 2004 - Teen Choice Awards for Choice Movie (Comedy), Choice Movie Actor (Seann William Scott), Choice Movie Actress (Alyson Hannigan), Choice Movie Blush (Seann William Scott), Choice Movie Hissy Fit (Jason Biggs) and Choice Movie Liplock (Jason Biggs & Alyson Hannigan)

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 