Rebel with a Camera
{{Infobox film
| name           = Rebel with a Camera
| image          = 
| caption        = 
| director       = Denys Desjardins
| producer       = National Film Board of Canada
| writer         = Denys Desjardins
| starring       = 
| music          = 
| cinematography = Alex Margineanu
| editing        = Vincent Guignard
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 50 minutes
| country        = Canada
| language       = French English
| budget         = 
}}
Rebel with a Camera (French: Le direct avant la lettre) is a 2006 documentary film by Quebec director Denys Desjardins produced by the National Film Board of Canada (NFB). The title is a reference to the film Rebel Without a Cause

==Synopsis==
Where does cinéma vérité come from? Who were its predecessors and what did they create? Compelled by the urge to get closer to the people in front of the camera, cinema vérité sprouted in a context of unprecedented creative freedom. Avant-garde techniques and the pioneering filmmakers behind them, notably National Film Board of Canada icon Michel Brault, set the stage for a new movie-making philosophy born in the ‘50s and ‘60s, a philosophy whose influence lives on in virtually every facet of today’s media.
 Claude Fournier, Jean-Claude Labrecque, Jacques Giraldeau, and others, revisit a pivotal turning point in the evolution of modern film.

==Award==

* Awarded by the Quebec Film Critics Association for Best medium length Documentary of 2005.

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 

 