Bheeshmar
{{Infobox film
| name           = Bheeshmar
| image          = Bheeshmar tamil film poster.jpg
| image_size     =
| caption        = DVD cover Ranjith
| producer       = Priya Raman
| writer         = Ranjith
| starring       =  
| music          = S. P. Venkatesh
| cinematography = S. D. Kannan
| editing        = Suresh Urs
| distributor    =
| studio         = Maverick Entertainment
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 2003 Tamil Tamil crime Devayani in lead roles. The film, produced by Ranjiths wife Priya Raman, had musical score by S. P. Venkatesh and was released on 27 September 2003. 

==Plot== Rami Reddy).

==Cast== Ranjith as Inspector Bheeshmar Devayani as Gowri, Bheeshmars wife Rami Reddy as R.K
*Riyaz Khan as Doctor Sakthi Anu Mohan as Constable Munu Munisaamy
*Ilavarasu as Aadhi
*Vasu Vikram as Dhandapani
*Sadiq as Assistant Commissioner Singampuli
*Sabash Y.S.D. Sehar
*Chelladurai
*Kottai Perumal
*Thirupur Ramasamy

==Soundtrack==

{{Infobox Album |  
| Name        = Bheeshmar
| Type        = soundtrack
| Artist      = S. P. Venkatesh
| Cover       = 
| Released    = 2003
| Recorded    = 2003 Feature film soundtrack |
| Length      = 1:13
| Label       = 
| Producer    = S. P. Venkatesh
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer S. P. Venkatesh. The soundtrack, released in 2003, features 1 track with lyrics written by Snehan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Nadagam Pol Vaazhkaiyila || Unni Menon || 2:13
|}

==Reception==
The film was an average grosser at the box office.  Ranjith was praised for his acting. 

==References==
 

 
 
 
 
 
 


 