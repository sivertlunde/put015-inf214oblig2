Between Tears and Smiles
 
{{Infobox film
| name           = Between Tears and Smiles
| film name =  
| image          = 
| caption        = 
| director       = Lo Chen
| producer       = Run Run Shaw
| writer         = Chin Hung Tu Wei
| starring       = Li Li-hua Kwan Shan Ivy Ling Po
| music          = 
| cinematography = 
| editing        = 
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 137 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         = 
}}

Between Tears and Smiles is a 1964 Hong Kong  ) and a street kung-fu/acrobat performer (Ivy Ling Po).
 Best Foreign Language Film at the 37th Academy Awards, but was not accepted as a nominee. 

==Cast==
 
* Diana Chang Chung Wen
* Paul Chang
* Chen Li-li Chen Yanyan
* Chiang Kuang Chao Chin Han
* Chin Ping
* Ching Miao as General
* Fan Mei Sheng
* Fang Yin Feng Yi
* Gao Baoshu
* Han Ying-Chieh		
* Peter Chen Ho		
* Julia Hsia
* Margaret Hsing Hui	
* King Hu
* Kao Chao		
* Kao Yuen		
* Carrie Ku Mei	
* Ku Feng as Taoist priest
* Kwan Shan
* Lan Wei Lieh	
* Lee Ching	 Li Kun	
* Li Hua Li
* Li Ting
* Li Ying
* Li Yunzhong	
* Ivy Ling Po	
* Ouyang Sha-fei	
* Peng Peng	
* Tien Feng	
* Pat Ting Hung	
* Margaret Tu Chuan		
* Wang Hao
* Allison Chang Yen
* Angela Yu Chien
* Yu Wen-hua
 

==See also==
* List of submissions to the 37th Academy Awards for Best Foreign Language Film
* List of Hong Kong submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 