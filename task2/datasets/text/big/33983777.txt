King of the Castle (film)
{{Infobox film
| name           = King of the Castle
| image          = 
| image_size     = 
| caption        =  Henry Edwards
| producer       = 
| writer         = Keble Howard   Alicia Ramsey 
| narrator       = 
| starring       = Marjorie Hume Brian Aherne Dawson Millward   Moore Marriott
| music          = 
| cinematography = 
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = 1926
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British silent silent drama Henry Edwards and starring Marjorie Hume, Brian Aherne and Dawson Millward.  It was based on a play by Keble Howard.

==Cast==
* Marjorie Hume - Lady Oxborrow 
* Brian Aherne - Colin OFarrell 
* Dawson Millward - Chris Furlong 
* Prudence Vanbrugh - Leslie Rain 
* Moore Marriott - Peter Coffin 
* Albert E. Raynor - Matlock 
* E.C. Matthews - Ezekiel Squence

==References==
 

==External links==
* 

 

 

 
 
 
 
 
 
 