Bobby (2002 film)
 
{{Infobox Film
| name           = Bobby
| image          = Bobby MoviePoster.jpg
| caption        = Bobby Movie Poster
| director       = Sobhan
| producer       = K. Krishna Mohan Rao
| writer         = Sobhan
| narrator       = 
| starring       = Mahesh Babu Aarthi Agarwal Prakash Raj Raghuvaran Ravi Babu Meher Ramesh Brahmanandam
| music          = Mani Sharma
| cinematography = Venkat Prasad
| editing        = Marthand K. Venkatesh
| distributor    = RK Associates
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
}} 

Bobby  is a 2002 Telugu film produced by K. Krishna Mohan Rao(brother of  K. Raghavendra Rao and directed by Sobhan. The movie stars Mahesh Babu, Aarthi Agarwal, Raghuvaran, Ravi Babu, Brahmanandam, Meher Ramesh and Prakash Raj. Soundtrack of the movie was scored by Mani Sharma. The film released on 31 October 2002 and was a critical and commercial failure.
Bobby was dubbed in Hindi as Daag - The Burning Fire. Shobu Yarlagadda of Arka Media Works was Executive producer for the movie

== Plot ==
Yadagiri (Prakash Raj) is a handicapped mafia leader in Hyderabad and KR (Raghuvaran) is a rich business man. Bhagyamati(Aarthi Agarwal) is daughter of Yadagiri. Bobby(Mahesh) is son of KR. The story of this film deals with what happens when these two kids fall in love with each other. Bobby meets Bhagyamati in a club and falls in love with her. They later find out that their fathers are arch rivals. Bhagyamatis mother was killed in an accident involving their fathers. Knowing this Bobby and Bhagyamati elope. But due to unavoidable circumstances they come back and are injured in a bomb blast. Then their fathers apologize and make up and in the end Bobby and Bhagyamati are married.

== Cast ==
* Mahesh Babu ... Bobby
* Aarthi Agarwal ... Bhagyamati
* Raghuvaran ... K.R
* Prakash Raj ... Yadagiri
* Ravi Babu ... Ravi Shankar
* Brahmanandam ... AmmiRaju
* Meher Ramesh ... Sunil
* Posani Krishna Murali ... Koti
* Rama Prabha ... Bhagmatis Grandmother
* Lakshmipathi ... Home Minister
* Sunil (actor)|Sunil.....Hotel Manager
* L.B. Sriram

== Reception ==
Jeevi from Idlebrain gave a modest 3.25 out of 5 and praised Mahesh Babu for his acting.  The movie was a failure at the boxoffice.
==Alternate Ending==
On release the film climax was such that both the lead actors are killed. With the film receiving negative reviews and to bolster revenues, the climax was changed such that it was an happy ending as Telugu audience in general dont like anti-climax. In spite of this change the film couldnt recover and was termed as a box office bomb.

== Music ==
{{Infobox album|  
  Name        = Bobby
|  Type        = Album
|  Artist      = Mani Sharma
|  Cover       = 
|  Released    = 2002 (India)
|  Recorded    =  Feature film soundtrack
|  Length      = 
|  Label       =  Supreme Music
|  Producer    = Mani Sharma
|  Reviews     = 
|  Last album  = Chennakeshava Reddy (2002)
|  This album  = Bobby (2006)
|  Next album  = Pellam Vurelithe (2002) 
}}

The film has 6 songs composed by Mani Sharma 

* Ee Jenda - Shankar Mahadevan

* Vaa Vaa - S.P. Balu & Sunitha Upadrashta

* Lokam - Kalpana
 Mano
 Ranjith & Hariharan
 Karthik & Chitra

== References ==
 

==External links==
*  
* http://www.idlebrain.com/movie/archive/mr-bobby.html Idlebrain

 
 
 
 