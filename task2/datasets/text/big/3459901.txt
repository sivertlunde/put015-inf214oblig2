Street Fight (film)
 
{{Infobox film |
  name     = Street Fight |
  image          = Streetfightmovieposter.jpg|
  caption        = Promotional poster for Street Fight |
  writer         = Marshall Curry |
  starring       =  |
  director       =  Marshall Curry |
  producer       =  Marshall Curry |
  distributor    = Marshall Curry Productions |
  released       =   |
  runtime        =  83 minutes |
  country        = United States |
  language       = English |
  budget         =  |
}}
Street Fight is a 2005 documentary film by Marshall Curry, chronicling Cory Bookers 2002 campaign against Sharpe James for Mayor of Newark, New Jersey. Other credits include Rory Kennedy (executive producer), Liz Garbus (executive producer), Mary Manhardt (additional editor), Marisa Karplus (associate producer), and Adam Etline (story consultant). PBS series P.O.V. on July 5, 2005, and CBC Newsworld in Canada on May 7, 2006.
 Best Documentary Feature.   

==Synopsis==

The film details the hard-fought mayoral campaign by a young community activist and City Council member (Booker) against a 16-year incumbent mayor (James) with a powerful political machine. The documentary follows Booker and several of his campaign workers from their early days of door-knocking on Newark streets through the campaigns dramatic conclusion. 

Through the course of the film, Bookers living conditions, race, ethnicity, religion, sexuality, political affiliations, and his position in Newark are questioned. From 1998 to 2006, Booker lived in Brick Towers, one of the citys worst public housing buildings, which some accused to be a tactic for acceptance by his constituents. As the election campaigns escalate, Booker receives endorsements from Spike Lee, Cornel West, and other prominent African American figures. 

The movie brings to light many issues plaguing  minority communities in Newark and reveals how the city government has failed to acknowledge these issues. The film also raises questions of race, and what it means to be "black," as Sharpe James questions Bookers African American heritage and roots to his community.
 demoting city federal judge, in what becomes a true urban political "street fight." In one memorable scene, city police assault the documentary maker on a public sidewalk for filming the mayor, breaking the microphone off his camera in broad daylight in front of other journalists.

==Aftermath== 36th mayor New Jersey State Senate until 8 January 2008. In July 2007, James was indicted on federal charges. He was later convicted of five counts of fraud and sentenced to 27 months in prison.

==See also==
*Brick City (TV series) - documentary series about Mayor Bookers term in office
*Newark mayoral election, 2014

==References==
 

==External links==
* 
*  at PBS series P.O.V. web site
*  entry, Tribeca Film Festival
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 