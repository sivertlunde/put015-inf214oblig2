Megher Pore Megh
{{Infobox film
| name           = Megher Pore Megh
| image          = MPM-Poster.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = Chashi Nazrul Islam
| producer       = Faridur Reza Sagar Ibne Hasan Khan (Impress Telefilm)
| writer         = Rabeya Khatun (Book) Wakil Ahmed Riaz Purnima Purnima Mahfuz Ahmed Shahidul Alam Sachchu
| music          = Emon Saha
| cinematography = Majibul Haque Bhuian
| editing        = Atikur Rahman Mallick
| distributor    = Impress Telefilm
| released       = 2004
| runtime        = 
| country        = Bangladesh Bengali
| website        = 
}}

Megher Pore Megh ( , meaning  ) is a 2004 Bangladeshi Bengali language feature film.    Directed by famous film director of Bangladesh Chashi Nazrul Islam.    Based on the events of the Bangladesh Liberation War.  An adaptation of the novel Megher Pore Megh by Rabeya Khatun.  Produced by Faridur Reza Sagar and Ibne Hasan Khan in the banner of Impress Telefilm. Stars Riaz (Actor)|Riaz, Purnima (Actress)|Purnima, Mahfuz Ahmed and Shahidul Alam Sachchu, Riaz first time acting a duplicate character in this film such like Sezan and Majid. 

==Cast== Riaz as Sezan Mahmud/Majid Purnima as Suraiya
* Mahfuz Ahmed as Nishat
* Shahidul Alam Sachchu as Rajakar
* Sejan as Swadhin Mahmud
* Khaleda Akter Kalpana as
* Jamilur Rahman Shakha as
* Uttam Guho as
* Abul Hossain as
* Washimul Bari Rajib as 
* Ameer Sirajee as
* Tareque Sikder as

==Awards and nomination==
===National Film Awards===
* Best singer - Subur Nandi   

==Music==
The music and the background score for the film is composed by Emon Saha.

===Sound Tracking===
# Bhalobashi Sakale – Subur Nandi

== References ==
 

==External links==
*  
*   at the Rotten Tomatoes

 
 
 
 
 
 
 
 
 
 
 
 