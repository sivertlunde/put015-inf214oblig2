Venmegam
{{Infobox film
| name           = Venmegam
| image          = 
| alt            = 
| caption        = 
| director       = Ram-Laxman
| producer       = Sujatha Sunitha
| writer         = 
| starring       = Vidharth Ishara Nair
| music          = Jaffer Hanni
| cinematography = 
| editing        = 
| studio         = Sujatha Sunitha Combines
| distributor    = 
| released       = 21 February 2014
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}}
Venmegam is a 2014 Tamil film directed by director duo Ram-Laxman and produced by Sujatha Sunitha Combines. The film features Vidharth and Ishara Nair in the lead roles, while Jaffer Hanni composes the films music.  The film was released on 21 February 2014 to mixed reviews. 

==Cast==
*Vidharth as Aravind 
*Ishara Nair as Raji
*Jayashree Nellankara as Pooja Rohini as Jyothi
*Jagan as  Bhaskar
*Manjari Vinodhini as Akka

==Production==

The film was first scheduled in April 2012 when Vidharth announced that he would star in a film titled Vilambaram, based on a real-life story, to be directed by twin directors Ram and Laxman, who had previously worked on different commercials.  He began work on the project alongside another project titled Vanavedikkai directed by Praghadeesh, after another project, Kaatumalli, went into a production break. The film began its first schedule in July 2012 with the title changed to Venmegam.  The film held a press meet in December 2013 and revealed the basic plot of the film, noting it would feature the bond between an artist and a school kid as a central theme. 

==Release==
The film released on 21 February 2014, opening in minimal screens as a result of two other bigger Tamil film releases on the same day, Bramman (film)|Bramman and Aaha Kalyanam. A critic from New Indian Express revealed "a more coherent screenplay and a clearer narration would have made the film a riveting experience" though the director duo should be "commended for tackling an issue rarely touched upon in films". 

== References ==
 

 
 
 
 


 