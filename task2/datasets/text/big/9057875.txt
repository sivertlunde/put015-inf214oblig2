Rishtey
:for TV series, see Rishtey (TV series), for TV channel, see Rishtey (TV channel)

{{Infobox film
| name           = Rishtey
| image          = Rishtey_poster.jpg
| caption        = 
| director       = Indra Kumar
| producer       = Indra Kumar   Ashok Thakeria
| writer         = Rajeev Kaul   Tanveer Khan   Praful Parekh
| starring       = Anil Kapoor  Karisma Kapoor  Shilpa Shetty
| music          = Sanjeev Darshan
| cinematography = Baba Azmi
| editing        = Sanjay Sankla
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}}

Rishtey ( ) is a Bollywood film released on 6 December 2002. The film directed by Indra Kumar stars Anil Kapoor, Karisma Kapoor, Shilpa Shetty and Amrish Puri in the lead roles. The film had a "Below Average" opening and flopped at the Box Office.  

==Synopsis==

Suraj Singh (Anil Kapoor) is in love with the beautiful and wealthy Komal (Karishma  Kapoor). They dream of their perfect family together but her arrogant father Yashpal Chaudhary (Amrish Puri) detests Suraj because he is middle class youth, with no riches whatsoever. Komal cuts all ties with her father and marries Suraj, she is soon pregnant. 

Komal and her estranged father meet at a family wedding and he forgives her for leaving abruptly. Yashpal agrees to meet Suraj but when they reach home they see a seductress (Deepshikha) inside, wearing Komals gown and drinking tea in the kitchen. Suraj walks in totally oblivious to the fact that there is a woman inside her house that he does not know. Komal questions the seductress and she says she shares Komals husband. Komal is furious and tries to commit suicide but Suraj stops her and repeatedly tries to prove his innocence. Komal leaves her husband with her father. She delivers a baby boy but Yashpal wants to make sure that the one thing that can bring her daughter and Suraj together be eliminated immediately. He hires hitmen to kill the baby but Suraj manages to save his son and get away. 

They move to an unknown place where he raises his son with a job in a construction company. Over the years the father and son forge a very good relationship. Suraj later meets Vaijanti (Shilpa Shetty) an eccentric fisherwoman who falls for him. She starts to help him out in keeping his house hoping that he feel love her but without any avail. 

Meanwhile a depressed and lonely Komal spots Suraj with her son. She locates their home and comes to meet her son. But in Surajs house she meets Vaijanti doing all the household chores. She misunderstands Vaijanti to be her ex-husbands new sweetheart and leaves distraught. Then begins a bitter custody battle between her and Suraj in which she successfully manages to gain custody of her son. Her son is upset on being separated from  his doting father and refuses to reciprocate to her affection. In a party thrown by Komals father to celebrate their win in the custody battle, Suraj comes in uninvited. He is humiliated by Yashpal but undetered he breaks into their house. The court judge witnesses this incident and sets forth a condition that Suraj must collect 15 lakh Rupees in three months time and he can gain custody of his son by proving his capability to provide for the sons needs.
Suraj decides to fight in a wrestling match to win the prize money which he can use to win over his son. In the meantime, Vaijanti comes across the seductress who had been the reason of Komal leaving her husband. The seductress regrets her actions and is ready to confess to Komal the true state of matters. Komal realises her misconception of matters and rushes to the wrestling match to apologise to Suraj. At last they reunite in a boxing competition where Suraj wins the reward. Komal meets him there and they embrace each other with their son playing a violin .

== Cast ==

* Anil Kapoor ... Suraj Singh
* Karisma Kapoor ... Komal Singh
* Shilpa Shetty ... Vaijanti
* Jibraan Khan ... Karan
* Amrish Puri ... Yashpal Chaudhary
* Sharat Saxena ... Hussain Bakshi
* Master Kaivalya Chheda ... Hussain Bakshis Son
* Alok Nath ... Judge
* Sadashiv Amrapurkar ... Advocate Katre
* Deepshika ... Seductress
* Saurabh Chopra ... Surajs Son
 

==Soundtrack==
All track composed by duo Sanjeev-Darshan. Lyrics penned by Abbas Katka except for Apun Ko Bas by Sumit.
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Note(s)
|-
| 1
| "Har Tarf Tu Hai"
| Shaan (singer)|Shaan, Mahalakshmi Iyer
| "Tune Lifted from Amr Diabs "Baateref (2000)"
|-
| 2
| "Rishta Tera Rishta Mera"
| Udit Narayan
|-
| 3
| "Apna Bana Na Hai"
| Udit Narayan, Anuradha Paudwal
| Tune Lifted from Ameer Alis "Karan Main Nazara - Choorian (1998 film)|Choorian"
|-
| 4
| "Deewana Deewana (Yeh Dil)- Male Version"
| Udit Narayan
| Totally copied from Amr Diabs "Alby Ektarak (2000)" 
|-
| 5
| "Dilbar Dilbar"
| Asha Bhosle
| Totally copied from Samira Saids "Al Bal (1998)"
|-
| 6
| "Tu Tu Dil Mein"
| Udit Narayan, Anuradha Paudwal
| 
|-
| 7
| "Apun Ko Bas" Sanjeev Rathod
| 
|-
| 8
| "Rishta Tera Rishta Mera (Sad)"
|
|  
|-
| 9
| "Rishta Tera Rishta Mera (Sad)"
| Udit Narayan
|  
|-
| 10
| "Deewana Deewana (Yeh Dil)- Female Version"
| Sunidhi Chauhan
| Totally copied from Amr Diabs "Alby Ektarak (2000)"
|-
| 11
| "Yaara Re Yaara Re"
| Sonu Nigam, Kavita Krishnamurthy
| Tune lifted from Diana Haddads "Amaneh (1997)"
|-
|}

==Trivia==

* This was the second time that Karisma Kapoor and Shilpa Shetty were working together, after the 1999 release Jaanwar (1999 film) |Jaanwar. Their younger sisters, Kareena Kapoor and Shamita Shetty later worked together in the film Bewafaa (2005 film)|Bewafaa (2005).

* Shetty received a Filmfare Best Supporting Actress Award nomination for her performance.

==References==


 

==External links==
*  

 
 
 