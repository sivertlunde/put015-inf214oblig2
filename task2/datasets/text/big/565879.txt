The Incredibles

 
 
{{Infobox film
| name           = The Incredibles
| image          = The Incredibles.jpg
| caption        = Theatrical release poster
| border         = yes
| director       = Brad Bird John Walker
| writer         = Brad Bird Jason Lee Holly Hunter Sarah Vowell Spencer Fox Samuel L. Jackson Elizabeth Peña Brad Bird
| music          = Michael Giacchino
| cinematography = Andrew Jimenez  Patrick Lin  Janet Lucroy
| editing        = Stephen Schaffer
| studio         = Walt Disney Pictures Pixar Animation Studios Buena Vista Pictures
| released       =  
| runtime        = 115 minutes
| language       = English
| country        = United States
| budget         = $92&nbsp;million
| gross          = $631.4 million 
}}
 comedy superhero Pixar Animation Studios. The films title is the name of a family of superheroes who are forced to hide their powers and live a quiet suburban life. Mr. Incredibles desire to help people draws the entire family into a battle with an evil villain and his killer robot.

Bird, who was Pixars first outside director, developed the film as an extension of 1960s comic books and spy films from his boyhood and personal family life. He pitched the film to Pixar after the box office disappointment of his first feature, The Iron Giant (1999), and carried over much of its staff to develop The Incredibles. The animation team was tasked with animating an all-human cast, which required creating new technology to animate detailed human anatomy, clothing and realistic skin and hair. Michael Giacchino composed the films orchestral score.
 Best Dramatic Presentation.  , a sequel is officially in development.

==Plot==
 
 Bob and Helen Parr, formerly List of The Incredibles characters#Mr. Incredible|Mr. Incredible and List of The Incredibles characters#Elastigirl|Elastigirl, and their children List of The Incredibles characters#Violet|Violet, List of The Incredibles characters#Dash|Dash, and List of The Incredibles characters#Jack-Jack|Jack-Jack live as a suburban family. Bob is unsatisfied with suburban life and his white-collar job and longs for the glory days. On some nights, Bob and his old friend Lucius Best, formerly Frozone, perform vigilante work, unbeknownst to Helen. One day, Bob loses his temper when his supervisor refuses to let him stop a mugging, causing Bob to be dismissed. Returning home, Bob finds a message from a mysterious woman named Mirage, who convinces him to become Mr. Incredible again and gives him a mission to destroy a savage robot called the Omnidroid on the remote island of Nomanisan, promising a substantial reward. Arriving on Nomanisan Island, Bob is able to find and defeat the Omnidroid by tricking it into ripping out its own power source.

Bob is rejuvenated by being able to use his powers freely, improving his attitude and relationship with his family, and he begins rigorous training while waiting for more work from Mirage. Discovering a fresh tear in his suit, Bob visits superhero costume designer Edna Mode who decides to make him and his whole family suits, unbeknownst to Helen and their kids. Leaving for Nomanisan once again, Bob discovers that Mirage is working for Buddy Pine, a former fan shunned by Mr. Incredible and now identifying as the super-villain Syndrome. Syndrome intends to perfect the Omnidroid and defeat it in public in Metroville while manipulating its controls to become a hero himself, and then sell his inventions so everyone will become equally "super", making the term meaningless. Bob sneaks into Syndromes base and finds Syndromes computer. From it, Bob discovers Syndrome murdered countless retired superheroes with previous Omnidroid prototypes to improve its design. Meanwhile, Helen visits Edna, finds out what Bob has been up to, and activates a homing beacon to find him, inadvertently causing Bob to be discovered and captured.

Helen borrows a private plane to head for Nomanisan, but finds Violet and Dash have stowed away wearing their own costumes, leaving Jack-Jack in the care of a teenage babysitter. Syndrome picks up Helen’s radio transmissions and shoots down the plane, but Helen along with the kids survive and make it to the island, though Bob thinks they are dead. Helen proceeds to the base to find Bob, discovering Syndromes intentions to send the Omnidroid to Metroville in a rocket. Later, Mirage, distraught by Syndromes true plans, releases Bob and informs him that his family is alive. Helen appears and races off with Bob to find their kids when they are spotted and chased by a number of Syndromes guards through Nomanisans tropical jungle. Dash and Violet use their powers to escape their captors and are joined by their parents, only to be captured by Syndrome, who then heads off to initiate his plan.

With Mirages help, the Parrs escape, and use a security RV and a rocket booster to pursue Syndrome. In Metroville, the Omnidroid proves to be too intelligent, and knocks the remote that controls it out of Syndromes grasp, knocking him unconscious and rampaging through the city. The Parrs and Lucius team up to fight the robot, until Bob uses Syndromes remote control and one of the Omnidroid’s detached pincers to make it tear its power source out, destroying it. Returning home, the Parrs find Syndrome has Jack-Jack and intends on raising him as his own sidekick to seek revenge on the family. As Syndrome tries to escape to his jet, Jack-Jacks own shapeshifting superpowers start to manifest and distract Syndrome. Helen rescues Jack-Jack, and Bob kills Syndrome by throwing his own car at the jet, causing him to be sucked into the jets turbine.

Three months later, the Parrs have readjusted to normal life, but the city is attacked by a villain called the Underminer. The family dons their superhero outfits, preparing to face the new threat.

==Voice cast==
 .]]
  Bob Parr / Mr. Incredible, the patriarch of the Parr family, possessing super-strength and limited invulnerability Helen Parr / Elastigirl, Bobs wife, who possesses the ability to stretch her body like rubber Dashiell Robert "Dash" Parr / The Dash, the Parrs fourth-grader second child, possessing super-speed
*Sarah Vowell as Violet Parr, the Parrs junior-high-aged first child, who possesses the ability to become invisible and generate an impact-resistant force shield Jason Lee Buddy Pine / Syndrome, who has no super powers of his own but uses advanced technology to give himself equivalent abilities
*Eli Fucile and Maeve Andrews as Jack-Jack Parr, The Parrs infant third child, who initially shows no powers but eventually reveals himself to have a wide range of abilities Lucius Best / Frozone, Bobs best friend, who has the ability to form ice from the humidity in the air
*Elizabeth Peña as Mirage (The Incredibles)|Mirage, Syndromes agent who lures Supers to Nomanisan Island
*Brad Bird as Edna Mode, the fashion designer for the Supers
*Bud Luckey as Rick Dicker, the government agent overseeing the relocation program
*Wallace Shawn as Gilbert Huph, Bobs supervisor at his white-collar insurance job
*John Ratzenberger as The Underminer, a new villain who appears at the end of the film
*Dominique Louis as Bomb Voyage, a villain from the past who uses Buddys interference in Mr. Incredibles heroism to escape
*Michael Bird as Tony Rydinger, a popular boy at Violets school who develops a crush on Violet
*Jean Sincere as  Mrs. Hogenson, an elderly woman who seeks help from Mr. Incredible for an insurance claim
*Kimberly Adair Clark as Honey Best, Frozones wife
*Bret Parker as Kari McKeen, Jack-Jacks babysitter
*Lou Romano as Bernie Kropp, Dashs teacher
*Wayne Canney as the principal of Dashs school

==Production==

===Writing===
The Incredibles as a concept dates back to 1993, when Bird sketched the family during a period in which he tried to break into film. 
    Personal issues had percolated into the story as they weighed on him in life.  During this time, Bird had inked a production deal with Warner Bros. Animation and was in the process of directing his first feature, The Iron Giant.  Approaching middle age and having high aspirations for his filmmaking, Bird pondered whether his career goals were attainable only at the price of his family life.  He stated, "Consciously, this was just a funny movie about superheroes. But I think that what was going on in my life definitely filtered into the movie."   After the box office failure of The Iron Giant, Bird gravitated toward his superhero story. Price, p. 220  Price, p. 219 
 traditionally animated CalArts in the 1970s.  Lasseter was sold on the idea and convinced Bird to come to Pixar, where the film would be done in computer animation. The studio announced a multifilm contract with Bird on May 4, 2000,  breaking Pixars mold of having directors who had all risen through the ranks. The Incredibles was written and directed solely by Brad Bird, a departure from previous Pixar productions which typically had two or three directors and as many screenwriters. Price, p. 222  In addition, it would be the companys first film in which all characters are human. Price, p. 217 

 
Bird came to Pixar with the lineup of the storys family members worked out: a mom and dad, both suffering through the dads midlife crisis; a shy teenage girl; a cocky ten-year-old boy; and a baby. Bird had based their powers on family archetypes.   After several failed attempts to cast Edna Mode, Bird took on her voice role himself. It was an extension of the Pixar custom of tapping in-house staff whose voices came across particularly well on scratch dialogue tracks. Price, p. 221  During production, Hayao Miyazaki of Studio Ghibli visited Pixar and saw the films story reels. When Bird asked if the reels made any sense or if they were just "American nonsense," Miyazaki replied, through an interpreter, "I think its a very adventurous thing you are trying to do in an American film." Price, p. 215-216 

===Animation===
  wrote and directed the film.]]
Upon Pixars acceptance of the project, Brad Bird was asked to bring in his own team for the production. He brought up a core group of people he worked with on The Iron Giant. Because of this, many 2-D artists had to make the shift to 3-D, including Bird himself. Bird found working with CG "wonderfully malleable" in a way that traditional animation is not, calling the cameras ability to easily switch angles in a given scene "marvelously adaptable." He found working in computer animation difficult in a different way than working traditionally, finding the software sophisticated and not particularly friendly.  Bird wrote the script without knowing the limitations or concerns that went hand-in-hand with the medium of computer animation. As a result, this was to be the most complex film for Pixar yet.  The films characters were designed by Tony Fucile and   that the crew at Pixar had "never really emphasized."   
 CGI at the time. Humans are widely considered to be the most difficult thing to execute in animation.  Pixar animators filmed themselves walking to better grasp proper human motion.    Creating an all-human cast required creating new technology to animate detailed human anatomy, clothing and realistic skin and hair. Although the technical team had some experience with hair and cloth in Monsters, Inc. (2001), the amount of hair and cloth required for The Incredibles had never been done by Pixar until this point. Moreover, Bird would tolerate no compromises for the sake of technical simplicity. Where the technical team on Monsters, Inc. had persuaded director Pete Docter to accept pigtails on Boo to make her hair easier to animate, the character of Violet had to have long hair that obscured her face; it was integral to her character.  Violets long hair was extremely difficult to achieve and for the longest time during production, it was not possible. In addition, animators had to adapt to having hair underwater and blowing through the wind.  Disney was initially reluctant to make the film because of these issues, feeling a live-action film would be preferable, though Lasseter vetoed this. 

The Incredibles not only dealt with the trouble of animating CG humans, but also many other complications. The story was bigger than any prior story at the studio, was longer in running time, and had four times the number of locations.   Supervising technical director Rick Sayre noted that the hardest thing about the film was that there was "no hardest thing," alluding to the amount of new technical challenges: fire, water, air, smoke, steam, and explosions were all additional to the new difficulty of working with humans.  The films organizational structure could not be mapped out like previous Pixar features, and it became a running joke to the team.  Sayre said the team adopted “Alpha Omega," where one team was concerned with building modeling, shading and layout and another that dealt with final camera, lighting and effects. Another team, dubbed the character team, digitally sculpted, rigged and shaded the characters, and a simulation team was responsible for developing simulation technology for hair and clothing.  There were 781 visual effects shots in the film, and they were quite often visual gags, such as the car window shattering when Bob angrily shuts the car door. In addition, the effects team improved upon the modeling of clouds, being able to model them for the first time with volumetric rendering. 

The skin of the characters gained a new level of realism from a technology to produce what is known as "subsurface scattering." Price, p. 223  The challenges did not stop with modeling humans. Bird decided that in a shot near the films end, baby Jack-Jack would undergo a series of transformations, and in one of the five planned he would turn himself into a kind of goo. Technical directors believed it would take upwards of two months to work out the goo effect, and production was at a point where two months of their time was indescribably precious.  They petitioned to the films producer, John Walker for help. Bird, who had brought Walker over from Warner Bros., took great exception to the idea that Jack-Jack could undergo a mere four transformations and that the film could do without the goo-baby. They argued over the issue in several invective-laced meetings for two months until Bird finally gave in. Price, p. 224  Bird also insisted that the storyboards define the blocking of characters movements, lighting, and camera moves, which had previously been left to other departments rather than storyboarded. 

Bird self-admitted that he "had the knees of   trembling under the weight" of The Incredibles, but called the film a testament to the talent of the animators at Pixar, who were admiring the challenges the film provoked.  He recalled, "Basically, I came into a wonderful studio, frightened a lot of people with how many presents I wanted for Christmas, and then got almost everything I asked for." Paik, Karen. (2007) To Infinity and Beyond!: The Story of Pixar Animation Studios, Chronicle Books LLC, pg. 238–51 

===Music===
  John Barry trailer of On Her Majestys Secret Service. However, Barry did not wish to duplicate the sound of some of his earlier soundtracks;  the assignment was instead given to Giacchino.  Giacchino noted that recording in the 1960s was largely different from modern day recording and Dan Wallin, the recording engineer, said that Bird wanted an old feel, and as such the score was recorded on analogue tapes. Wallin noted that brass instruments, which are at the forefront of the films score, sound better on analog equipment rather than digital. Wallin came from an era in which music was recorded, according to Giacchino, "the right way," which consists of everyone in the same room, "playing against each other and feeding off each others energy." Tim Simonec was the conductor/orchestrator for the scores recording. 
  

The films orchestral score was released on November 2, 2004, three days before the film opened in theaters. It won numerous awards for best score including Los Angeles Film Critics Association Award, BMI Film & TV Award, ASCAP Film and Television Music Award, Annie Award, Las Vegas Film Critics Society Award and Online Film Critics Society Award and was nominated for Grammy Award for Best Score Soundtrack for Visual Media, Satellite Award and Broadcast Film Critics Association Award. 

==Themes== The Avengers. Fantastic Four were forced to make significant script changes and add more special effects because of similarities to The Incredibles.  Bird was not surprised that comparisons arose due to superheroes being "the most well-trod turf on the planet," but noted that he had not been inspired by any comic books specifically, only having heard of Watchmen. He did comment that it was nice to be compared to something as highly regarded as Watchmen. 
 Objectivism philosophy, centrist and feel like both parties can be absurd." 

The film also explored Birds dislike for the tendency of the childrens comics and Saturday morning cartoons of his youth to portray villains as unrealistic, ineffectual, and non-threatening.  In the film, Dash and Violet have to deal with villains who are perfectly willing to use deadly force against children.  On another level, both Dash and Violet display no emotion or regret at the deaths of those who are trying to kill them, such as when Dash outruns pursuers who crash their vehicles while chasing him, or when both of them witness their parents destroy several attacking vehicles with people inside, in such a manner that the deaths of those piloting them is undeniable. Despite disagreeing with some analysis, Bird felt it gratifying for his work to be considered on many different levels, which was his intention: "The fact that it was written about in the op/ed section of The New York Times several times was really gratifying to me. Look, its a mainstream animated movie, and how often are those considered thought provoking?" 

==Release==
The film opened on November 5, 2004, as Pixars first film to be rated PG (for "action violence") with the other PG-rated Pixar films being Up (2009 film)|Up and Brave (2012 film)|Brave.  Its theatrical release was accompanied with a Pixar short film Boundin.  While Pixar celebrated another triumph with The Incredibles, Steve Jobs was embroiled in a public feud with the head of its distribution partner, The Walt Disney Company. Price, p. 226  This would eventually lead to the ousting of Michael Eisner and Disneys acquisition of Pixar the following year.

In March 2014, Disney CEO and chairman Bob Iger announced that the film will be reformatted and re-released in 3D. 

===Home media=== UMD for the Sony PSP.  It was released on Blu-ray Disc|Blu-ray in North America on April 12, 2011.  There was also a VHS release to the film on March 15, 2005, notably the last Disney/Pixar film to be widely issued in VHS format (not counting Pixars later film Cars (film)|Cars; whose VHS release was extremely rare). 

==Reception==

===Box office=== The Polar Express. The film ultimately grossed $261,441,092, as the sixth highest-grossing Pixar film behind Toy Story 3 ($415.0 million), Finding Nemo ($380.8 million), Up ($293.0 million), Monsters, Inc. ($289.9 million), and Monsters University ($268.5 million) and the fifth highest-grossing film of 2004.  Worldwide, the film grossed $631,442,092, is the fifth highest-grossing Pixar film behind Toy Story 3 ($1.063 billion), Finding Nemo ($936.7 million), Monsters University ($743.6 million) and Up ($731.3 million), and ranked fourth for 2004.  It is also the second highest-grossing 2004 animated film behind Shrek 2 ($919.8 million). 

===Critical response===
  magazines top 100 villains. ]]
The film received a 97% approval rating at  , another review aggregator, indicates the film "universal acclaim" with a 90 out of 100 rating. 

Critic Roger Ebert awarded the film 3½ stars out of 4, writing that the film "alternates breakneck action with satire of suburban sitcom life" and is "another example of Pixars mastery of popular animation."  Peter Travers of Rolling Stone gave the film 3½ stars and called the film "one of the years best" and said that it "doesnt ring cartoonish, it rings true."    Also giving the film 3½ stars, People (magazine)|People magazine found that The Incredibles "boasts a strong, entertaining story and a truckload of savvy comic touches." 

Eleanor Ringel Gillespie of The Atlanta Journal-Constitution was bored by the films recurring pastiches of earlier action films, concluding, "the Pixar whizzes do what they do excellently; you just wish they were doing something else."   Similarly, Jessica Winter of The Village Voice criticized the film for playing as a standard summer action film, despite being released in early November. Her review, titled as "Full Metal Racket," noted that "The Incredibles announces the studios arrival in the vast yet overcrowded Hollywood lot of eardrum-bashing, metal-crunching action sludge." 
 The National Review Online named The Incredibles No. 2 on its list of the 25 best conservative movies of the last 25 years saying that it "celebrates marriage, courage, responsibility, and high achievement."  Entertainment Weekly named The Incredibles No. 25 on its list of the 25 greatest action films ever.  Entertainment Weekly also named The Incredibles No. 7 on its list of the 20 best animated movies ever.  IGN ranked the film as the third favorite animated film of all time in a list published in 2010. 

===Accolades===
 
 Best Animated Best Sound Best Original Best Sound Mixing (Randy Thom, Gary Rizzo and Doc Kane).    It was Pixars first feature film to win multiple Oscars, followed in 2010 by Up. Joe Morgenstern of The Wall Street Journal called The Incredibles the years best picture.  Premiere magazine released a cross-section of all the top critics in America and The Incredibles placed at number three, whereas review aggregator website Rotten Tomatoes cross-referenced reviews that suggested it was its years highest-rated film. 
 Best Animated Best Dramatic Best Motion Best Animated Top 10 Animated Films list. 

==Merchandising==
Several companies released promotional products related to the film. Dark Horse Comics released a limited series of comic books based on the film.  Kellogg Company|Kelloggs released an Incredibles-themed cereal, as well as promotional Pop-Tarts and fruit snacks, all proclaiming an "Incrediberry Blast" of flavor.    Pringles included potato chips featuring the superheroes and quotes from the film.  Furthermore, in the weeks before the films opening, there were also promotional tie-ins with SBC Communications (using Dash to promote the "blazing-fast speed" of its SBC Yahoo! DSL service) Tide (brand)|Tide, Downy, Bounce and McDonalds.    Toy maker Hasbro produced a series of action figures and toys based on the film, although the line was not as successful as the film itself. 
 Kinder chocolate eggs contained small plastic toy characters from the film.  In Belgium, car manufacturer Opel sold special The Incredibles editions of their cars.  In the United Kingdom, Telewest promoted blueyonder internet services with branding from the film, including television adverts starring characters from the film.  In all merchandising outside of the film itself, Elastigirl is referred to as Mrs. Incredible.  This is due to a licensing agreement between Disney·Pixar and DC Comics, who has a character named Elasti-Girl (a member of the Doom Patrol).  The DC Comics character is able to grow and shrink at will from microscopic size to thousands of feet tall. 

In July 2008, it was announced that a series of comic books based on the film would be published by   and Marcio Takara, which was published from March to June 2009, and collected into a trade paperback published in July of that year.    An ongoing series written by both Mark Waid and Landry Walker, with art by Marcio Takara and Ramanda Kamarga, began later that same year, running for sixteen issues before being cancelled in October 2010.  Marvel Comics began a reprint of the series in August 2011 – set to possibly finish the storyline – which was abruptly cancelled, despite the production of scripts and art for a finale. 

==Video game==
 
A video game based on the film was released on the  , was released for the PlayStation 2, GameCube, Xbox, Game Boy Advance, and  , was released for Windows and OS X.    It is a collection of 10 games and activities for the playable characters to perform.  Another game, Kinect Rush: A Disney Pixar Adventure, was released on March 20, 2012, for  , which was released in August 2013. The play-set for The Incredibles is featured in the starter pack. 

==Sequel==
In 2004, Disney owned the rights to a follow-up, and announced plans to make sequels for The Incredibles and Finding Nemo without Pixars involvement.  Those plans were subsequently scrapped. 

When Disney acquired Pixar in 2006, the expectation of Disney was that Pixar would create more sequels and bankable franchises. Director Brad Bird stated in 2007 that he was open to the idea of a sequel if he could come up with an idea superior to the original film: "I have pieces that I think are good, but I dont have them all together." 

During an interview in May 2013, Bird reiterated his interest in making a sequel: "I have been thinking about it. People think that I have not been, but I have. Because I love those characters and love that world." He continues, "I am stroking my chin and scratching my head. I have many, many elements that I think would work really well in another Incredibles film, and if I can get ‘em to click all together, I would probably wanna do that." 

At the Disney shareholders meeting in March 2014, Disney CEO and chairman Bob Iger confirmed that Pixar is working on another The Incredibles film, with Bird returning as writer.     Later that month, Samuel L. Jackson told Digital Spy that he would likely reprise his role as Frozone in the sequel.  In April 2015, Bird revealed to NPR that he has begun writing the screenplay for the sequel. 

== See also ==
 

==References==
 

==Further reading==
*   

==External links==
 
 
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
{{Navboxes | title = Awards for The Incredibles | list =
 
 
 
 
 
 
}}

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 