Mizhikal Sakshi
{{Infobox film
| name           = Mizhikal Sakshi
| image          = Mizhikal Sakshi.jpg
| alt            = 
| caption        = Film DVD cover
| director       = Ashok R. Nath
| producer       = V.R. Das
| writer         = Anil Mukhathala Satheesh Menon
| music          = V. Dakshinamoorthy
| cinematography = K. RamachandraBabu
| editing        = Vipin Manoor
| studio         = Cyber Vision
| distributor    = Jofrey Release
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = Malayalam
| budget         = 1 crore rupees
| gross          = 
}}
Mizhikal Sakshi  ( ) is a 2008 Malayalam film.  It is directed by Ashok R. Nath and stars Sukumari and Mohanlal.   

==Plot==
Sukumari plays a key role of an aged lady named Kuniyamma and Mohanlal in a guest role, as Kuniyammas son.Kuniyammas son,Syed Ahmed, an idealistic teacher. But due to misfortune he was accused to be jehad bomber, and was sentenced to death on charges of being an extremist.This incident ruined her life, and her life was never the same again. The news of her son’s hanging shattered Nabisa and she loses her mental balance. And then, she begins a search for her son, not ready to face the reality that her son is dead. Movie depicted the present kerala society in a secular way. 

==Cast==
{| class="wikitable" border="1"
! Actor || Role
|-
| Sukumari
| Kooniyamma / Nabisa
|-
| Mohanlal
| Syed Ahmed
|-
| Nedumudi Venu
| Lebba Sahib
|-
| Vineeth
| Aravind
|-
| Mala Aravindan
| Kadarukutty Musaliar
|-
| Manoj K. Jayan
| Aditya Varma
|-
| Krishna
| Ambili
|-
| Ramu Mangalappalli
| Keezhshaanthi
|- Satheesh Menon
| Marampalli Tirumeni
|-
| Kailas Nath
|  Melshaanthi
|-
| Dinesh Panicker Devaswom Manager
|-
| Krishna Prasad
| Feroz
|-
| Kochu Preman
| Vasudeva Valyathaan
|- Renuka 
| Ambilis mother
|}

==References==
 

==External links==
*  

 
 
 
 
 
 

 