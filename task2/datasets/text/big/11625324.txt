Mannequin (1937 film)
{{Infobox film
| name           = Mannequin
| image          = Rsz 1postermannequinwindow2x.jpg
| image size     = 225px
| caption        = Original Film Poster
| director       = Frank Borzage
| producer       = Joseph L. Mankiewicz
| writer         = Story:  
| narrator       =  Alan Curtis
| music          = 
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $595,000  .  gross = $1,634,000 
| preceded_by    = 
| followed_by    = 
}} 1937 film Alan Curtis. In the film, Crawford plays Jessie, a young working class woman who seeks to improve her life by marrying her boyfriend, only to find out that he is no better than what she left behind. Jessie meets a self-made millionaire with whom she falls in love despite his financial problems.
 American release on January 21, 1938.

==Synopsis==
 strike and save their company, Jessie confronts Eddie. He tries to blackmail her, but she says that she will leave Hennessey and flee before seeing him hurt. Just before she is about to leave him, however, Hennessey comes home and Jessie lies that she never loved him.  Eddie then walks in and announces that Hennessey is now broke and "in the gutter" just like him. He also tells Hennessey about the plan for Jessie to marry and divorce him for money.   Eddie then leaves and Hennessey refuses to listen to Jessies word that she loves him.   Later, however, she convinces him that she will stay by his side no matter what and that the money from the sale of her jewels will give them a new start.

==Cast==

* Joan Crawford ...  Jessie Cassidy 
* Spencer Tracy ...  John L. Hennessey  Alan Curtis ...  Eddie Miller 
* Ralph Morgan ...  Briggs 
* Mary Philips ...  Beryl Lee
* Oscar OShea ...  Pa Cassidy
* Elisabeth Risdon ...  Ma Cassidy 
* Leo Gorcey ...  Clifford Cassidy
* Gwen Lee ...  Flo

==Reception==

Frank Nugent in the New York Times remarked, "Mannequin...restores Miss Joan Crawford to her throne as queen of the working girls....Miss Crawford, let it be said, meets these several dramatic emergencies in her best manner, which, as you know, is tender, strong, heroic, and regal."
===Box Office===
According to MGM records the film earned $1,066,000 in the US and Canada and $568,000 elsewhere resulting in a profit of $475,000. 
==References==
 
==External links==
* 

 

 
 
 
 
 
 
 