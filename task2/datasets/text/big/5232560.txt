Hamlet (1969 film)
{{Infobox film
| name=Hamlet
| image=
| caption =
| writer=William Shakespeare   adapted by Tony Richardson
| starring=Nicol Williamson   Marianne Faithfull Anthony Hopkins Judy Parfitt
| director= Tony Richardson
| music=Patrick Gowers Charles Rees	 
| studio = Woodfall Film Productions
| distributor=Columbia Pictures
| released=September 1969 (UK)
| runtime=117 min.
| country = UK
| language=English
| movie_series=
| awards= Leslie Linder Martin Ransohoff
| budget= $350,000 Alexander Walker, Hollywood, England, Stein and Day, 1974 p452 
}}
 Roundhouse theater Queen Gertrude, Gordon Jackson as Horatio (character)|Horatio, and Michael Pennington as Laertes (Hamlet)|Laertes.

The film, a departure from big-budget Hollywood renditions of classics, was made with a small budget and a very minimalist set, consisting of Renaissance fixtures and costumes in a dark, shadowed space. A brick tunnel is used for the scenes on the battlements. The Ghost of Hamlets father is represented only by a light shining on the observers.
The film places much emphasis on the sexual aspects of the play, to the point of including an incestuous relationship between Laertes and Ophelia.

==DVD==
Hamlet was released to DVD by Sony Pictures Home Entertainment on July 3rd, 2012 via the Choice Collection DVD-on-demand setup from Amazon.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 


 