Let's Get Tough!
{{Infobox film
| name           = Lets Get Tough
| image          = Letsgetpos.jpg
| caption        = Original film poster
| director       = Wallace Fox
| producer       = Sam Katzman
| writer         = Harvey Gates based on his story I Am an American
| starring       = Leo Gorcey Huntz Hall
| music          = Johnny Lange & Lew Porter
| distributor    = Monogram Pictures Corporation
| released       =  
| runtime        = 62 mins
| country        = United States
| language       = English
}}
 Robert Armstrong (as "Pops" the Cop). Released in early 1942, it was directed by Wallace Fox, and features the gang caught up in World War II and fighting the Black Dragon Society, an enemy sabotage ring.

==Plot==
Watching a military parade (stock footage from World War I), the gang decides to enlist in order to "kill a million Japs".  Rejected by the Army, Marines, and Navy for being too young, the punks help the war effort by throwing fruit at a shop they believe is owned by a Japanese American.  Confronted by him wielding a short sword, the gang decides to come back at night but find him dead.  Their father figure Police Lieutenant "Pops" Stevens tell them they should be ashamed of themselves as Keno, the owner of the shop, was Chinese, an ally of America and that Danny should be especially ashamed as his brother is in the service.

The boys buy some flowers and go to the shop to apologize to the widow and notice a Japanese man take a pen from a locker the widow opened for him.  Glimpy steals the pen and find that it contains a message written in Japanese.

They visit a Japanese shop run by Mr. Matsui to have it translated. He tries to steal the message but the gang threatens him, whereupon Mr. Matsui commits hari kari in their presence.  The boys run to the police.
 Tom Brown) has supposedly been dishonourably discharged from the US Navy but is working undercover to infiltrate the Black Dragons. Dannys brothers girlfriend Nora (Florence Rice), (who is in the WAVES) has a Japanese friend she went to high school with whom she seeks help from to translate the message. However he turns out to be Matsuis son, the leader of the spy ring and has her locked up in a cell in the basement of the shop.

The gang breaks into Matsuis shop that is filled with haunted house type secret passageways and trapdoors where they discover the Black Dragon Society dressed in hooded costumes that Glimpy refers to as "Japanese Halloween".  The gang frees Nora and revenges the attack on Pearl Harbor by beating up the saboteurs. Hayes, David and Walker, Brent. The Films of the Bowery Boys. Citadel Press, 1984, pp. 66-67.  The film ends with Nora and Phil getting married but as they walk down the church steps with a sabre arch of East Side Kids holding their captured Japanese swords (that are quickly confiscated by the police at the end of the ceremony!). Phil is told he has orders to report back to his base as soon as possible.  Phil and Nora briefly lament their not going on a honeymoon, but Muggs and the gang pile in the car and gallantly offer to accompany Nora on her honeymoon unaware of what a honeymoon entails... 

==Overview==
Made between Mr Wise Guy and Smart Alecks, the East Side Kids go from being a gang of punks to a group of, as Danny puts it, "Junior G-Men" (an in joke as that was the name of two serials the gang did for Universal Pictures).  The film captures the attitudes many Americans felt towards Japanese but this is tempered with the boys being chastised and shamed for attacking an innocent shopkeeper.  Japanese American internment on the West Coast of America did not begin until Civilian Exclusion Order No. 346 was issued on May 3, 1942 authorized by Executive Order 9066.  The Black Dragon Society was an actual Japanese espionage organization that first appeared in the Russo-Japanese War. On March 27, 1942 FBI agents arrested members of the Black Dragon Society in the San Joaquin Valley of California. 

The Black Dragon society also appeared in Black Dragons a 1942 release starring Bela Lugosi also written by Harvey Gates and filmed by Sam Katzmans production company.

Ernest Morrison who played Scuno in the film said that in a scene where the gang runs off he thought the filming had stopped and began counting his money he had in his pocket.  The gang ad-libbed an attack on him. 

The British Board of Film Censors who usually gave the East Side Kids series an A (adult) rating gave Lets Get Tough! a rating approving it for all audiences, perhaps as a wartime morale booster. 

At just over an hour, Lets Get Tough! is fast paced with the romantic subplot and comedy setpieces blending into the main plot of the sabotage ring. For example, Glimpys incompetent violin playing grating on his teachers nerves lead to the gang pawning the violin to buy flowers for Mr Kenos widow, that leads them to see Matsuis son take the pen that contains the secret message.  The same violin case attracts Glimpys attention to discover that it now contains magnesium powder (that Glimpy steals a sample of) to be used for sabotage that the gang only discover when it explodes and burns when placed next to Glimpys mothers stove.  The film stands as a reminder of the attitudes and cultures of the time, such as a fear of sabotage from well known members of the community.

==Cast and characters==

===The East Side Kids===
*Leo Gorceyas Muggs
*Bobby Jordan as Danny Connors
*Huntz Hall as Glimpy
*David Gorcey as Peewee Sunshine Sammy Morrisson as Scruno
*Bobby Stone as Skinny

===Additional cast===
*Gabriel Dell as Fritz Heinbach  Tom Brown as  Phil Connors
*Florence Rice as  Nora Stevens  Robert Armstrong as Officer Pops Stevens
*Sam Bernard as Heinback Sr. 
*Philip Ahn as Joe Matsui 
*Jerry Bergen as Music Master - 

== References ==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 