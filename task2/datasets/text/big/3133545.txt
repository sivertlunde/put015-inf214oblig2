Desperate Hours
 

{{Infobox film
| name        = Desperate Hours
| image       = Desperate Hours.jpg Joseph Hayes (novel, play & screenplay) Lawrence Konner Mark Rosenthal (screenplay)
| starring    = Mickey Rourke Anthony Hopkins Mimi Rogers Lindsay Crouse Kelly Lynch
| director    = Michael Cimino Christopher Rouse Peter R. Hunt (supervising editor)
| producer    = Michael Cimino Dino De Laurentiis
| music       = David Mansfield
| distributor = Metro-Goldwyn-Mayer
| released    =  
| runtime     = 105 minutes
| language    = English
| budget      =
| gross       = $2,742,912        
}}
 David Morse. Year of the Dragon.

==Plot==

In Utah, Nancy Breyers (Kelly Lynch) is a defense lawyer who is inexplicably in love with client Michael Bosworth (Mickey Rourke), a sociopathic convict. During a break from a courtroom hearing, Nancy sneaks a gun to Bosworth. After Bosworth snaps a guards neck, Bosworth and Nancy slip away. 
 David Morse), then changes cars with one Nancy has left for him in a remote location.

Needing a hideout until Nancy can catch up with them, the three settle on the home of decorated Vietnam veteran Tim Cornell (Anthony Hopkins) and Nora Cornell (Mimi Rogers), who have two kids—15-year-old May (Shawnee Smith) and her 8-year-old brother Zack (Danny Gerard). Their house with the "For Sale" sign is picked by Bosworth at random.

Tim and Nora are separated due to his infidelity with a younger woman. But when he shows up trying to reconcile with Nora, with whom he is still in love, the two of them find themselves the prisoners of the Bosworth brothers and Albert. 

Nancys innocent act does not fool FBI agent Brenda Chandler (Lindsay Crouse), who puts surveillance on her every move. Nancy eventually cuts a deal with Chandler to have charges against her reduced by betraying Bosworth.

A friend of the Cornells visits the house by chance, so Bosworth shoots him. An anxiety-ridden Albert decides to go off on his own, and is killed by FBI agents on a river bank.  

The house is surrounded that night, and Wally is killed in a barrage of FBI bullets. Bosworth holds a gun on Nora and is prepared to use it if Tim interferes. He is unaware that Tim has removed the bullets. Tim drags the criminal outside, where Bosworth ignores the FBIs order to surrender, and is fatally shot.

==Cast==
* Anthony Hopkins as Tim Cornell
* Mickey Rourke as Michael Bosworth
* Mimi Rogers as Nora Cornell David Morse as Albert
* Lindsay Crouse as FBI Agent Chandler
* Kelly Lynch as Nancy Breyers
* Elias Koteas as Wally Bosworth
* Shawnee Smith as May Cornell Matt McGrath as Kyle
* Danny Gerard as Zack Cornell

==Reception==
The film, directed by   as a daughter/victim youll beg to see cold-cocked." The film holds a 40% "rotten" rating on the reviews aggregation site Rotten Tomatoes based on 10 reviews.  Christopher Tookey, reviewing the film for the Sunday Telegraph called Desperate Hours: 
"One of those films which should never have been released, even on parole - a danger to itself."  
 Wild Orchid), but lost to Andrew Dice Clay for The Adventures of Ford Fairlane.

== See also ==

* List of films featuring home invasions

==References==
 

==External links==
* 
*  at  

 
 
 

 
 
 
 
 
 
 
 
 
 