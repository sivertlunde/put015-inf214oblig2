Hot Rod (film)
{{Infobox film
| name           = Hot Rod
| image          = Hot-rod-poster.jpg
| caption        = Theatrical release poster
| writer         =  
| starring       =  
| director       = Akiva Schaffer
| producer       = John Goldwyn Lorne Michaels Will Ferrell Andrew Dunn Malcolm Campbell
| distributor    = Paramount Pictures
| released       =  
| music          = Trevor Rabin
| runtime        = 88 minutes
| language       = English
| budget         = $25.3 million
| gross          = $14,334,401   
}}
Hot Rod is a 2007 American comedy film co-written, directed, and starring members of The Lonely Island (Andy Samberg, Jorma Taccone, and Akiva Schaffer). The film stars Samberg as an amateur stuntman whose abusive step-father, Frank (Ian McShane) continuously mocks and disrespects him. When Frank grows ill, Rod raises money for his heart operation by executing his largest stunt yet. In addition to raising money for the operation, he also does so to win Franks respect. The film also stars Taccone, Sissy Spacek, Will Arnett, Danny McBride, Isla Fisher, and Bill Hader. The film was directed by Schaffer (in his directorial debut) and distributed by Paramount Pictures.
 Yes guitarist, Trevor Rabin, and the film features several songs by the Swedish rock band Europe (band)|Europe.

Hot Rod opened on August 3, 2007 and was a box office failure, grossing only $14 million of its $25 million budget. As the films producers predicted, it received very mixed reviews, criticizing the films script and humor. Despite this, the film has in recent years experienced a cult following due to its unconventional humor.

==Plot==
Throughout his entire life, Rod Kimble (Andy Samberg) has believed his father, now deceased, was a successful and respectable stuntman working for Evel Knievel. He aspires to follow in his fathers footsteps and become a famous stuntman himself. Meanwhile, his stepfather Frank (Ian McShane) fails to respect Rod as a man, often going out of his way to beat him in sparring sessions and mocking his stuntman dream. Rod makes many attempts at landing jumps with his Tomos moped, most of them ending unsuccessfully. After an ill-fated jump attempt at the public pool, he returns home to find out that Frank is in urgent need of a heart transplant. Angry at the thought of his stepfather dying without getting a chance to "smash his face in" and gain his respect, Rod tells his childhood friends Rico (Danny McBride), Dave (Bill Hader) and his half-brother Kevin (Jorma Taccone) about his plan to do a jump over the length of fifteen school buses, and give the proceeds to his stepfathers surgery. He also adds Denise (Isla Fisher), his college-graduate neighbor on whom he has a crush, to his crew.
 acid given to him by his friend Derek (Akiva Schaffer). When he does, Dave gives him advice that inspires Rod to apologize to Kevin. As Kevin accepts his apology, he also reveals that Rods stunt footage has gotten popular through the Internet, and a local AM radio station hosted by Barry Pasternak (Chris Parnell) offers to cover the expenses of the jump.

Rod gets the crew back together and they start setting up for the jump. On the day of the jump, his friends give him a new suit, a rock (to represent extensive pyrotechnic work done by Rico), and a motorbike. He also receives a kiss from Denise, who broke up with Jonathan (Will Arnett), her insensitive and callous boyfriend. As Rod jumps off the ramp, the speed of his new motorbike enables him to jump over the buses, but the bike smashes through a stage and goes flying. Rod lands squarely on the ground, and has an unconscious out-of-body experience. When he awakes, Rod, with the help of Kevin and Denise, triumphantly gets up to the applause of the crowd, and sees that the donations have accumulated over $50,000, the cost of Franks conveniently priced surgery. The film ends taking place six months after the events of earning $50,000 as Rod again spars with Frank, in which Rod gains the upper hand and succeeds in making his stepfather respect him.

==Cast==
*Andy Samberg – Rod Kimbless
*Jorma Taccone – Kevin Powell
*Bill Hader – Dave McLean
*Danny McBride – Rico Brown
*Isla Fisher – Denise Harris
*Sissy Spacek – Marie Powell
*Ian McShane – Frank Powell
*Will Arnett – Jonathan Ault
*Chris Parnell – Barry Pasternak
*Chester Tam – Richardson
*Mark Acheson – Homeless Dude
*Alvin Sanders – Furious Boss
*Akiva Schaffer – Derek
*Britt Irvin – Cathy Fay
*Brittany Tiplady – Maggie Mclean
*Andrew Moxham – Sullivan

==Production==
  was key in giving the Lonely Island creative control of the film.]]
Hot Rod was written by  . 

They were initially reluctant, as the script was designed for Ferrell and the summer filming schedule was less than ideal.  Paramount allowed them to re-write the script, allowing them to incorporate bizarre, offbeat humor and absurdist comedy.    The script had to "match their standards": "Which is another way of saying, just dumb it down," said Schaffer.  This involved deleting comedy designed for Ferrell (which Samberg characterized as "so well-written") and replacing it with their own humor. "We didnt want it to seem like I was doing a Ferrell impression," said Samberg.    The result was a balance between "weird" humor and what Paramount considered accessible. Samberg was very inspired by Wet Hot American Summer, which was, according to him, "designed to fuck around with whats expected from a movie."  Many jokes from the film deemed "too weird" were cut, including a scene in which Rod "jokingly" asks his younger brother Kevin to pull out his genitalia.  Samberg aimed for a performance that was "bad, but you know its bad." 
{{Quote box quote  = Theres a lot said about how this is the nerdy generation, and its Internet-driven. The comedy thats influenced me has always been that. From the Three Stooges through Steve Martin in The Jerk and Ace Ventura and Chris Farley and Billy Madison, they were village idiots. Thats definitely the tradition were trying to follow.  source = Sambergs discussing the films influences in 2007  quoted = 1 width  = 25% align  = left
}} Cloverdale in North Vancouver, Burnaby and Downtown Vancouver. Samberg had a stunt double, but did as many of his own stunts as he was allowed to.   

The MPAA objected to the use of the word "semen" in a scene in which Chris Parnell reveals a profane tattoo on his stomach, which resulted in it being changed to "residue."  Another scene that barely made it into the film involved Samberg and Taccone repeating the phrase "cool beans" until it evolves into a "bizarre pseudo-rap." Schaffer had initially cut the scene, but Samberg and Taccone edited the scene themselves. Schaffer reinserted the scene in the last test screening of the film, where it received high marks from audiences as one their favorite bits from the film.  The original poster for the film the troupe preferred featureds silhouetted Samberg atop a hill beside his motorbike in a martial-arts pose. Paramount changed the poster to a large close-up of Sambergs face instead. 

==Reception==
Prior to the films release, the Lonely Island promoted the film with interstitials during Comedy Central movie marathons. In the promos, the trio attempt to convince viewers that Hot Rod is the story of a sex offender ("He does stunts to raise money to sex-offend") and spoof the quality of films run during daytime marathons ("Stay tuned for Teen Wolf Too!").    The premiere of Hot Rod was held at the John Ford Amphitheatre in Los Angeles. 

Samberg predicted the film would not do well, remarking to Entertainment Weekly, "It will get bad reviews. Comedy is traditionally not reviewed that well." In addition, he added that if future generations viewed Hot Rod with a similar reverence to films such as Billy Madison, he would consider the film to have been a success.   

===Commercial performance===
The film opened at #9 at the U.S. box office in 2007 and grossed $5.3 million in its opening weekend. Overall, it bombed in theaters, leaving after 68 days and having grossed just under $14 million in its entire run. 

===Critical reception===
  at the films premiere. His directorial style was criticized in initial reviews.]]
Hot Rod received mixed reviews from film critics upon its initial release.  On Metacritic, the film achieved an average score of 43 out of 100, based on 27 creviews, signifying  "generally mixed reviews".    On IMDb the film has a rating of 6.7/10 from 48,755 users.  Following the films release, Paper (magazine)|Paper described the films reception: "Depending on whom you talk to, Hot Rod is either a terrible stinker or a really strange and wonderful movie that you cant believe they got away with making."   

The Hollywood Reporter  Frank Scheck criticized the films "formulaic" script and humor, but commending Sambergs "reasonably engaging and sweet comedic screen presence."    "No one seems to have told the Lonely Island boys that the stakes are a little higher in features than they are in music videos and that underlighted shots and sloppy editing are more distracting on the big screen than on television," wrote Marjorie Baumgarten of The Austin Chronicle.    Wesley Morris of The Boston Globe characterized the film as "playfully dumb," commenting, "The filmmakers   have skipped right past the kitsch of tribute and gone straight for jokey delusion   And in that sense, Hot Rod is post-parody, taking nothing seriously, not even being a movie."    Peter Travers of Rolling Stone wrote, "The films low-key Waynes World vibe takes it only so far. I laughed, then I wished it was funnier, then I just wished it would end.    Peter Debruge of Variety (magazine)|Variety called the movie "yet another example of a comedy that refuses to be taken seriously — concept as clothesline for all manner of silliness."    Nick Schager of Slant Magazine noted that the trio "care far less about clever plotting than random ridiculousness," deeming the film "a tired rehash of every SNL alums big-screen debut since Adam Sandlers Billy Madison."   

For their part, the films producers remained optimistic about the film in the press. "The movies Ive always liked, comedy-wise — Billy Madison, The Jerk — always got terrible reviews. When our reviews came in, it was like, Oh, were right on track," said Samberg.  Lorne Michaels predicted it would find a different audience in the future:

 

The film has in recent years attracted a cult following; in 2012, The A.V. Club wrote that it differentiated itself from other Lorne Michaels–produced comedies: "They may be just as poorly received, but their rhythms are unpredictable and exciting, shocked to life by moments of anti-comedy and wacky deconstruction. Hardcore comedy devotees pick up on them like a dog whistle."   

==Home release== Region One DVD and HD DVD  on November 27, 2007, and in Region Two in January 2008. It made $24 million on DVD rentals in the United States—46% over its box office gross. It was released on Blu-ray Disc December 16, 2008, but the disc went out-of-print less than a year later on October 11, 2009.  The DVD remained in print. The Blu-ray was rereleased by Warner Bros. and remains in print as part of a deal they have to distribute Paramount catalog titles.

==Soundtrack==
{{Infobox album  
| Name        = Hot Rod 
| Type        = Soundtrack
| Artist      = Various
| Cover       = Hotrodost.jpg
| Released    = July 31, 2007 
| Recorded    = 
| Genre       = Soundtrack
| Length      = 
| Label       = Sony Legacy 
}}

{{Album ratings
| rev1      = Allmusic
| rev1Score =   
| rev2      = 
| rev2Score = 
}}
 Yes guitarist, Europe are Rock the trailer contains Swedish rock The Final Countdown," and The Hives "See Through Head." It also includes the UK rock band, Test Icicles "Circle. Square. Triangle," as well as American Hi-Fis "The Art of Losing." The band called Gown that plays at Rods final jump is actually Queens of the Stone Age.

;Tracklisting Europe
#"A Gringo Like Me" – Ennio Morricone Moving Pictures
#Two of Hearts (song)|"Two of Hearts" – Stacey Q
#Cherokee (Europe song)|"Cherokee" – Europe Glenn Danzig)
#"Street Luge" – Trevor Rabin
#"Youre the Voice" – John Farnham
#"Head Honcho" – Gown
#Chase (composition)|"Chase" – Giorgio Moroder
#"Cool Beans" – Jorma Taccone & Andy Samberg
#"(I Just) Died in Your Arms" – Cutting Crew
#"Dave on Acid" – Trevor Rabin
#"Rock the Night" – Europe
#"Stunt Suite" – Trevor Rabin
#"Time Has Come" – Europe
#"The Real Bass" – Brooklyn Bounce

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 