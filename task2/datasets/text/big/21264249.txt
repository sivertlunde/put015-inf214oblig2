Mr. and Mrs. Kabal's Theatre
{{Infobox Film
| name = Mr. and Mrs. Kabals Theatre
| image =
| caption =
| director = Walerian Borowczyk
| producer = Jacques Forgeot
| writer = Walerian Borowczyk
| starring =
| distributor =
| released =
| runtime = 80 min. France
| French
| music = Avenir de Monfred
| cinematography = Guy Durban Francis Pronier
| editing = Claude Blondel
| budget =
| box office =
| preceded_by =
| followed_by =
}} 1967 Cinema French animation|animated film directed by Walerian Borowczyk. It is Borowczyks first feature-length film and his last animated film. It consists of a sequence of loosely connected scenes, much like a vaudeville program, in which Mr. and Mrs. Kabal perform absurd, surreal, and sometimes cruel acts. Borowczyk introduced the personnel in his short film Le Concert de Monsieur et Madame Kabal in 1962.

A mixture of cut-out and drawn animation is used, but also clippings of old illustrations and photographs and even a processed live-action appearance by the director himself. Most images are black-and-white, with only the occasional coloured element. The sound design adds a lot to the surreal atmosphere. Mrs. Kabal speaks in an illegible collage of cut-up human sounds, sometimes translated into subtitles. The film won the Interfilm Award at the Mannheim-Heidelberg International Filmfestival in 1967.

"The dark and grotesque story of a married couple: the wife - a mechanical monster made up of iron parts, and her henpecked husband. When Mrs. Kabal eats a butterfly, she gets indigestion and her husband has to travel to his wifes insides to search for the cause of her illness. The film includes colour photographic inserts - Mrs. Kabals dreams."   

==Notes==
 

==External links==
*  
*  

 

 
 
 
 
 


 
 