Shadows of Liberty
 

{{Infobox film
| name           = Shadows of Liberty
| director       = Jean-Philippe Tremblay
| producer     = Jean-Philippe Tremblay Dan Cantagallo
| writer         = Jean-Philippe Tremblay Dan Cantagallo
| narrator       = Kerry Shale
| music          = Tandis Jenhudson
| cinematography = Arthur Jafa
| editing        = Gregers Sall
| studio         = DocFactory
| distributor    = First Hand Films
| released       =  
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}

Shadows of Liberty is a 2012 British documentary film directed by Canadian filmmaker Jean-Philippe Tremblay. The documentary examines the impact of corporate media and concentration of media ownership on journalism and the news. It is based on the book The Media Monopoly by Ben Bagdikian. The film’s title is borrowed from a Thomas Paine quote: “When men yield up the privilege of thinking, the last shadow of liberty quits the horizon.”
 1996 TWA 800 air disaster controversy and Nike sweatshops in Asia. It also alleges how the investigation of these stories have cost the jobs, and in some cases the lives, of investigating journalists (as suggested by the case of Gary Webb who committed suicide in 2004. It features interviews with journalists, activists and academics including Amy Goodman, Danny Glover, Julian Assange, Dan Rather, David Simon, Norman Solomon, Robert Baer, Roberta Baskin, Robert W. McChesney, Daniel Ellsberg, Chris Hedges and Kristina Borjesson.

==Release==
The film received its premiere at the 2012 Hot Docs Canadian International Documentary Festival.  It received its U.S. premiere at the 2013 National Conference for Media Reform in Denver, Colorado. 

==Reception==
It was nominated for the Cinema for Peace Award for Most Valuable Documentary of the Year in 2013,  losing to eventual joint-winners, the Academy Award winning Searching for Sugar Man and Academy Award nominated The Gatekeepers (film).  It was also selected at numerous other festivals including Sheffield Doc/Fest  and IDFA. 

Film critics gave the film mixed reviews. Hollywood Reporter reported that “The timing couldn’t be better for a theatrical documentary about a corporate media monopoly in American journalism.” Rabble.ca gave the film high praise, describing it as "beautifully shot and replete with artful graphics and animation, Shadows of Liberty stands on its own as a beautiful artifact. The musical score by Tandis Jenhudson is hauntingly industrial and a perfect accompaniment to the story of the production methods for todays news product" adding that "Artistry, cinematic or otherwise, and clear-eyed political vision rarely come this close together. Shadows of Liberty as a film, and Jean Philippe Tremblay as auteur are both definitely newsworthy."   The Toronto Star gave the film two-and-a-half stars out of five, reporting that "Shadows of Liberty asks consumers to look critically at what they are being told, and seek out what they are not" but added that "Unfortunately, the damning material Tremblay uses is dated".  Exclaim! described the film as "professionally made but somewhat bloated". 

==References==
 

==External links==
 
*   official website
* 

 
 
 
 