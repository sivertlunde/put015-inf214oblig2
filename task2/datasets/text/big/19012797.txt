The Marsh (film)
{{Infobox Film
| name           = The Marsh
| image          = the-marsh-movie.jpg
| director       = Jordan Barker
| writer         = Michael Stokes
| narrator       = 
| starring       = Gabrielle Anwar Justin Louis Forest Whitaker
| released       = May 18, 2006
| country        = United States English
| budget         = $7 million
}}

The Marsh is a 2006 Film directed by Jordan Barker and written by Michael Stokes. The horror films tagline is:

 You can bury the past, but sometimes the past wont stay buried... 

== Plot ==
A troubled young writer finds herself embroiled in a supernatural mystery... one she must solve or die.

The successful childrens writer Claire Holloway is troubled by scary nightmares and is under psychological treatment. While working out watching television, she sees the landscape of the Rose Marsh Farm in the Westmoreland County, and she notes that the farmhouse is linked to her nightmares. She decides to spend her vacation in the farm, which is located nearby a swamp, and she is haunted by the ghosts of a little girl and a teenage boy inside the house. She is befriended by the local publisher and historian Noah Pitney but after a sequence of disturbing visions, she decides to contact the paranormal consultant Geoffry Hunt. Together, they investigate the mystery and disclose a tragedy that happened in the farm about twenty years ago.

== External links ==
*  
*  

 
 