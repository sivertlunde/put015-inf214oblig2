The Night (film)
 The Night}}
{{Infobox film
| name           = The Night (Al-Lail) الليل
| image          =Al-Lail film poster - 1992.jpg
| image size     =
| caption        =
| director       = Mohamed Malas
| producer       = Omar Amiralay
| writer         = Mohamed Malas Ussama Muhammed
| narrator       =
| starring       = Sabah Jazairi Omar Malas Riadh Chahrour Maher Salibi Fares al-Helou
| music          =
| cinematography = Youssef Ben Youssef
| editing        = Kais Al Zubaidi General Organization for Cinema
| released       = 1992
| runtime        = 116 minutes
| country        = Syria
| language       = Arabic
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
The Night or Al-Lail ( ) (The Night) is a Syrian feature drama film by director Mohamed Malas. The film is set in Quneitra and is about the Arab-Israeli conflict. In 1993 it became the first Syrian feature to be played at the New York Film Festival.

==Awards==
*Carthage Film Festival - Tanit dOr, 1992.

==External links==
*  

 

 
 
 
 
 


 