Trafic (2004 film)
{{Infobox film
| name           = Trafic
| director       = Cătălin Mitulescu
| producer       = Cătălin Mitulescu
| screenplay     = Cătălin Mitulescu Andreea Valean
| starring       = Bogdan Dumitrache Maria Dinulescu
| music          = Mihai Bogos Calin Potcoava
| cinematography = Marius Panduru
| editing        = Ion Ioachim Stroe
| studio         = Strada Film
| released       =  
| runtime        = 15 minutes
| country        = Romania Romanian
}}

Trafic is a 2004 Romanian short film directed by Cătălin Mitulescu. It depicts a brief moment in the mundanities of urban life and family life, and a mans quick decision to escape them, if only just for a moment. It won the Short Film Palme dOr at the 2004 Cannes Film Festival.  It is said  to have been the "instigating" film of the Romanian New Wave. 

==See also==
* Romanian New Wave

==References==
 

==External links==
*  
*   at Vimeo

 
 
 
 

 
 