The Cruise of the Make-Believes
{{Infobox film
| name           = The Cruise of the Make-Believes
| image          =
| caption        =
| director       = George Melford
| producer       = Adolph Zukor Jesse Lasky
| writer         = Edith Kennedy (scenario)
| based on       = Novel The Cruise of the Make-Believes by Tom Gallon Harrison Ford
| music          =
| cinematography = Paul Perry
| editing        =
| distributor    = Paramount Pictures
| released       = September 1, 1918
| runtime        = 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} lost  1918 American silent dramatic feature film starring Lila Lee in her first motion picture. It was directed by George Melford and is based on a 1907 novel by Tom Gallon. Famous Players-Lasky produced and Paramount Pictures released. 

The film was released at the height of the 1918 flu pandemic.

==Cast==
*Lila Lee - Bessie Meggison Harrison Ford - Gilbert Byfield
*Raymond Hackett - Daniel Meggison
*William Brunton - Aubrey Meggison
*J. Parks Jones - Jordan Tant(*as Park Jones)
*Spottiswoode Aitken - Simon Quarle
*Bud Duncan - Uncle Ed
*Eunice Murdock Moore - Aunt Julia (as Eunice Moore)
*Mayme Kelso - Mrs. Ewart Crane (as Maym Kelso)
*Nino Byron - Enid Crane
*William McLaughlin - Saloon Proprietor
*Jane Wolfe - Byfields Landlady (as Jane Wolff)
*John McKinnon - Butler of Dream Valley

==See also==
*The Truth About Spring (1965), a similar themed movie from the 1960s about coming of age

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 


 