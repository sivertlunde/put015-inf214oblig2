Quincy Adams Sawyer
{{Infobox film
| name           = Quincy Adams Sawyer
| image          = Quincy Adams Sawyer.jpg
| image size     =
| caption        = 1922 lobby card
| director       = Clarence G. Badger
| producer       =
| writer         = Bernard McConville
| based on       =   John Bowers Lon Chaney Barbara La Marr
| music          =
| cinematography = Rudolph J. Bergquist
| editing        = 
| studio         = Sawyer-Rubin Productions
| distributor    = Metro Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States Silent English intertitles
| budget         =
}} silent comedy-drama film directed by Clarence G. Badger. Distributed by Metro Pictures, the film is based on the 1900 novel Quincy Adams Sawyer and Masons Corner Folks, written by Charles Felton Pidgin.  The film was re-released in 1927, after LaMarrs death, by Metro-Goldwyn-Mayer, and is now considered a lost film. 

==Plot==
Quincy Adams Sawyer is a young attorney who one day meets a girl in the park. He is immediately smitten with her, but is summoned to the village of Masons Corners by his fathers friend Deacon Pettengill to investigate a villainous lawyer named Obadiah Strout before he can pursue her. Meanwhile, Mrs. Putnam, an old woman, is being swindled by Strout. Putnams daughter Lindy, a natural Femme fatale|vamp, attempts to seduce Sawyer. He shows interest, until he finds out who the girl from the park is: Alice, Pettengills niece who has become blind since their first meeting.

Despite this tragedy, Sawyer falls in love with Alice. Meanwhile, Strout attempts to scare away Sawyer and convinces Abner Stiles, who once committed a murder, that Sawyer is in town to investigate him. Lindy, on the other hand, tries to get rid of Alice and, with the help from Strout, Lindy lures her onto a boat, after which the rope is cut. The little boat is sent adrift and she is off to the water falls.

Lindy is initially glad to be rid of Alice, but soon regrets her decision. She rushes to Quincy and tells him the entire truth. Quincy races to the water falls and rescues her from a fatal fall. Overcome by excitement, she regains her sight. Her father is unaware that his daughter has been saved and becomes convinced that Strout has killed her. He grabs a revolver and attempts to shoot Strout, but he appears to have arrived too late. Stiles, who realized he was being lied to, has killed Strout in a rage.    

==Cast== John Bowers as Quincy Adams Sawyer
*Blanche Sweet as Alice Pettengill Lon Chaney as Obadiah Strout
*Barbara La Marr as Lindy Putnam
*Elmo Lincoln as Abner Stiles
*Louise Fazenda as Mandy Skinner
*Joseph J. Dowling as Nathaniel Sawyer
*Claire McDowell as Mrs. Putnam
*Edward Connelly as Deacon Pettengill
*Victor Potel - as Hiram Maxwell
*June Elvidge as Betsy Ann Ross
*Gale Henry as Samanthey
*Hank Mann as Ben Bates
*Kate Lester as Mrs. Sawyer
*Billy Franey as Bob Wood

==Reception== Moving Picture Lon Chaneys acting could save it.  Variety (magazine)|Variety praised the fearful hokum purveyed in the story and was especially positive about Chaney and Elmo Lincoln.  Barbara La Marr received favorable reviews from the critics as well, and the film was a success at the box office. 

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 
* 
*  (University of Washington, Sayre Collection)

 

 
 
 
 
 
 
 
 
 
 