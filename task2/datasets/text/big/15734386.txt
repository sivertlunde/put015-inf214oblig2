The Night of Counting the Years
 
 
{{Infobox film
| name           = The Night of Counting the Years Al-Mummia
| image          = The Night of Counting the Years.jpg
| caption        = A screenshot from the film.
| director       = Shadi Abdel Salam
| producer       = Roberto Rossellini
| writer         = Shadi Abdel Salam
| starring       = Ahmed Marei Ahmad Hegazi Zouzou Hamdy El-Hakim Nadia Lutfi
| music          = Mario Nascimbene
| cinematography = Abdel Aziz Fahmy
| editing        = Kamal Abou-El-Ella
| distributor    = General Egyptian Cinema Organisation Merchant Ivory Productions
| released       =  
| runtime        = 102 minutes
| country        = Egypt
| language       = Classical Arabic
| budget         =
| gross          =
}}
 Best Foreign Language Film at the 43rd Academy Awards, but was not accepted as a nominee. 

==Plot==  British colonial cache of mummies discovered at tomb DB320 near the village of Kurna, and selling the artefacts on the illicit antiquities black market. After a conflict within the clan, one of its members goes to the police, helping the Antiquities Service find the cache.

==Symbolism==  Egyptian national ancient Egyptian civilisation. However, the conflict between city and countryside suggests questions that are not resolved in the film, making it an ambiguous, unsettling reflection on the price of identity. 

==Visual style== 
Its slow Pace (speed)|pace, unusual camera angles and striking colours give the film a dreamlike quality, reinforced by Mario Nascimbenes eerie Film score|music.  Moreover, the dialogue is entirely in classical Arabic, a very unusual trait for an Egyptian film, which adds to the sense of unreality. 

==See also==
* Grave robbing
* List of submissions to the 43rd Academy Awards for Best Foreign Language Film
* List of Egyptian submissions for the Academy Award for Best Foreign Language Film

==Bibliography==
*{{Cite book
  | last = Colla
  | first = Elliott
  | title = Beyond Colonialism and Nationalism in the Maghrib: History, Culture, and Politics
  | publisher = Palgrave Macmillan
  | date = 2000-10-06 New York
  | pages = 109–146 ("Shadi Abd al-Salams al-Mumiya: Ambivalence and the Egyptian Nation-State")
  | isbn = 978-0-312-22287-1}}

==References==
 

==External links==
* 
*  
*  
*  


 
 

 

 
 
 
 
 
 
 
 
 
 
 