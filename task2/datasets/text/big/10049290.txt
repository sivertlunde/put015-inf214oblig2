Firehouse Dog
 
{{Infobox film
| name           = Firehouse Dog
| image          = Firehouse dog poster.jpg
| image_size     = 220px
| alt            = 
| caption        = Theatrical release poster Todd Holland
| producer       = Michael J. Maschio
| writer         = Claire-Dee Lim Mike Werb Michael Colleary
| starring       = Josh Hutcherson Bruce Greenwood Dash Mihok Steven Culp Bill Nunn
| music          = Jeff Cardoni
| country        = United States
| cinematography = Victory Hammer
| studio         = Regency Enterprises
| editing        = Scott J. Wallace
| distributor    = 20th Century Fox
| released       =  
| runtime        = 111 minutes
| language       = English
| budget         = 
| gross          = $17,299,151
}} Todd Holland, it stars Josh Hutcherson, Bruce Greenwood, Dash Mihok, Steven Culp and Bill Nunn. It was released April 4, 2007, in the U.S.

==Plot==
Dog superstar Rexxx lives the high life-with adoring crowds, a loving owner and an array of best-selling blockbusters under his belt, he should be the happiest dog around. However, when his owner, Trey (Dash Mihok) tries to convince him to perform a skydiving stunt, he stubbornly refuses. Eventually, due to Treys agent Liz Knowles (Bree Turner) convincing him to co-operate, Rexxx performs his stunt; however, the plane malufunctions and Rexxx is sent tumbling from the sky, landing in a truck full of tomatoes. Whilst Trey mourns his apparent death and begins to regret not treating him like a real dog, Rexxx settles into an abandoned warehouse, desperately missing his owner.

Meanwhile, firefighter Captain Connor Faheys son, Shane (Josh Hutcherson) is struggling his uncles recent death. Realizing he forgot to revise for a test, Shane ditches school, but is quickly caught by two other firefighters, Lionel and Terrence. Arriving back at the fire station in disgrace, he is chastised by his father Connor Fahey (Bruce Greenwood), who is having problems of his own; station Dogpatch Engine 55  is about to be closed due to bad publicity. However, before Connor can properly address his sons problems, he is called out to put out a fire in a warehouse-Shane is reluctantly dragged along. Although the fire is quickly put out, Shane notices a terrified Rexxx balancing on top of the burning building; Connor manages to rescue him, and orders Shane to put up Lost Dog flyers. Due to the name on his collar, the station renames Rexxx Dewey, and keeps him at the station until someone comes to claim him.

Whilst city manager Zachary Hayden (Steven Culp) reminds Connor of the stations upcoming shutdown, Shane struggles to cope with Deweys spoilt needs and strange habits. Realizing that the dog is fast and active, Shane enters him in a firefighters competition, where they are pitted against rival fire station Greenpoint. Although Dewey initially beats Greenpoints record score, he is distracted by their dog, who reminds him of his time in stardom. Despite losing the competition, Shane and Dewey begin to bond.
Soon after, Engine 55 are called out to yet another fire, one of the many suspected arsons that have been surrounding the area. Realizing that Greenpoints captain, Jessie Presley, is still trapped inside as the building begins to collapse, Connor rushes into the wreckage, as Shane, fearing for his fathers safety, allows Dewey to run in after him. Dewey manages to alert Connor to Jessies presence, subsequently saving her life. Following this, Engine 55 begins to gain popularity, as they realize that Dewey could become a potential firehouse dog. Due to their sudden increase in popularity, Zachary eagerly notifies them that the station is saved.

However, Shanes excitement is lost when he discovers his father has moved to his uncles former office. Angered that Connor is trying to take his uncles place, he roots through the files, where he discovers an unnerving number of suspected arsons. Upset that Shane felt he was being neglected, Connor makes an effort to reconcile with his son, and is shocked when Shane reveals that he feels like a bad person for being relieved when he discovered it was his uncle who died instead of his father. The next day, at a firefighters gala, Dewey is awarded a medal for his bravery, but the moment is ruined when Dewey spots Trey in the audience, and abandons Shane for his former owner. Although Shane is heartbroken, Connor reluctantly allows an ecstatic Trey to keep the dog. 

A few hours later, however, Dewey escapes Treys hotel room to chase after an Engine 55 truck that was recently called out. The team is only too happy to allow him to climb on board. Meanwhile, Shane returns to the station to discover that the fire Engine 55 was called out to was simply a decoy, so that the suspected arsonist could burn the station to the ground. Panicking, he calls Jessie Presleys daughter, Jasmine JJ Presley to work out what to do, before becoming conscious of footsteps upstairs, which turn out to be the arsonist. Ignoring JJs warnings, he heads upstairs to confront the arsonist. To his horror, he realizes that the arsonist is in fact city manager Zachary Hayden, who wanted to burn buildings in order to build a football stadium for Corbin Sellars (Matt Cooke), killing Shanes uncle in the process. After Engine 55s closure was denied, he had no choice but to burn the station down himself, but didnt realize that Shane was still in the building. As the two become trapped inside the burning building, Zachary quickly gives up his search for an unconscious Shane and leaves.

Meanwhile, Dewey, sensing that Shane is in danger, races back to the station as Connor follows behind, having been alerted to the fire by JJ. Dewey finds Zachary trying to escape, and traps him in a phone booth (which he also escapes from) before finding Shane. Connor arrives on the scene, only to find the station completely inaccessible. Hearing Deweys barking, he eventually manages to break down the entrance and finds a terrified Shane on the other side of a locked door. Shane manages to convince him to pass him his axe through some broken glass so he can try to break the latch himself, despite the risks; Dewey then leads them out. Shane manages to tell Connor about Zacharys arson attacks before he is sent to hospital. Furious, Connor confronts Zachary as he is arrested. 

Corbin Sellars scam is exposed and he is arrested. Following the events of the fire, all of the firefighters of Engine 55 are awarded medals, including Shane and Dewey. Upon seeing how happy Dewey is with them, Trey allows Shane to keep him, adding that now that Dewey has been a true hero, he wont be content with just acting like one. Both Shane and Connor are overjoyed, with Dewey realizing his true potential as a firehouse dog.

==Cast==
*Josh Hutcherson as Shane Fahey
*Bruce Greenwood as Captain Connor Fahey
*Dash Mihok as Trey Falcon
*Steven Culp as City Manager Zachary "Zach" Hayden
*Bill Nunn as Probationary FF Joe Musto
*Bree Turner as Liz Knowles
*Scotch Ellis Loring as FF Lionel "Burr" Bradford
*Mayte Garcia as FF Pep Clemente
*Teddy Sears as FF Terrence Kahn
*Claudette Mink as Captain Jessie Presley
*Hannah Lochner as Jasmine "J.J." Presley
*Matt Cooke as City Chief Corbin Sellars
*Shane Daly as Burr Baldwin
*Arwen, Frodo, Rohan, Stryder as Rexxx/Dewy the Dog

==Production notes==
Rexxx/Dewy is played in the film by four different Irish Terriers named Arwen, Frodo Baggins|Frodo, Rohan (Middle-earth), and Aragorn|Stryder, named after the characters/location from The Lord of the Rings story.

The film was shot in Toronto and Hamilton, Ontario, Canada.   

==Distribution==
The film was filmed in 2005 but was released in 2007. The DVD was released July 31, 2007. The Blu-ray was released 2012.

===Release dates===
*USA and Canada: April 6, 2007
*Philippines: April 25, 2007  (Manila)
*Spain: June 8, 2007
*Philippines: June 20, 2007 (Davao)
*France: June 27, 2007
*UK: July 20, 2007
*Sweden: August 3, 2007
*Italy: August 10, 2007
*Belgium: August 22, 2007
*Malaysia: August 30, 2007
*Mexico: August 31, 2007
*Japan: September 1, 2007
*Colombia: October 12, 2007
*Russia: October 23, 2007 (DVD premiere)
*Iceland: November 7, 2007 (DVD premiere)
*Panama: November 9, 2007
*Poland: December 7, 2007
*Hungary: December 12, 2007 (DVD premiere)
*Argentina: January 16, 2008 (DVD premiere)
*Venezuela: February 22, 2008

==Reception==
Firehouse Dog received mainly negative to mixed reviews from film critics. It garnered 38% positive reviews on the film-critic aggregate site Rotten Tomatoes, and a 43/100 on Metacritic. Justin Chang of Variety (magazine)|Variety called it, "A likable but ungainly mutt of a movie".  Ty Burr in The Boston Globe found "the human scenes in Firehouse Dog are perfectly acceptable on the level of a heartwarming family B-movie" but "that dog—or, rather, that digitally enhanced replicant—is just plain creepy".  While Carrie Rickey of The Philadelphia Inquirer called it "a touching, family-friendly entertainment about a dog and his boy", 

Chris Kaltenbach of The Baltimore Sun felt it was "too busy being inspirational and cuddly to be funny or pointed" and "plays out as though its plot was stuck in molasses".  Frank Lovece of Film Journal International capped his review by suggesting that, "Firehouse Dog should be put to sleep before it can do the same to audiences".  Michael Phillips of the Chicago Tribune (April 11, 2007) says: "Once it figures out it is more drama than comedy, "Firehouse Dog" exceeds your limited expectations....While the movies ad campaign suggests wacky antics all the way, a surprisingly affecting and well-acted father/son relationship develops."

==See also==
* List of American films of 2007
*List of firefighting films

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 