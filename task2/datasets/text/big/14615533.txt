Wham Bam Slam
{{Infobox Film |
  | name           = Wham-Bam-Slam!
  | image          = WhabamsalmLOBBY.jpg
  | caption        = 
  | director       = Jules White  Felix Adler
  | starring       = Moe Howard Larry Fine Shemp Howard Matt McHugh Wanda Perry Alyn Lockwood Doria Revier
  | cinematography = Fred Jackman Jr.
  | editing        = Paul Borofsky
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 15 54"
  | country        = United States
  | language       = English
}}

Wham-Bam-Slam! is the 164th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
Shemp has been ill with a toothache for quite some time. The Stooges friend Claude (Matt McHugh), gives Moe and Larry some specific instructions on how to cure the toothache, which, of course, they misinterpret every which way possible. After finally yanking the troublesome tooth, Claude suggests they take Shemp on a camping trip for a little R&R (military)|R&R. Since the Stooges do not own a car, Claude offers to sell them a car that turns out to be a "Lemon (automobile)|lemon".

The trio run into a series of mishaps trying to get the car to work, including a flat tire that lands Moes foot under the car. After all is said and done, Shemp realizes that he feels better after all.

==Production notes== cossack dance) were filmed on January 18, 1955. 

==Quotes== brain food."
*Moe: "You know, you should fish for a whale!" *SLAP!*
*Larry: "Well, well, ketchup! My favorite fruit!"

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 