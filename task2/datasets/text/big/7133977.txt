South of the Border with Disney
{{Infobox Film
| name           = South of the Border with Disney
| image          =  Norman Ferguson
| producer       = Walt Disney Productions, under the auspices of the Office of the Coordinator of Inter-American Affairs
| starring       = Walt Disney
| distributor    = Walt Disney Productions, Office of the Coordinator of Inter-American Affairs
| released       = November 23, 1942
| runtime        = 32 minutes
| country        = United States English Spanish Spanish Brazilian Portuguese
}} short Documentary Pluto cartoon, Pluto and the Armadillo. Film also includes some pencil test animation. 

In 2000, South of the Border with Disney was released on the Gold Classic Collection DVD of Saludos Amigos as an extra, and it was released again as a bonus feature on the 2008 Saludos Amigos/The Three Caballeros "Classic Caballero Collection".

==See also==
*Saludos Amigos

==External links==
*  

 
 
 
 
 
 

 