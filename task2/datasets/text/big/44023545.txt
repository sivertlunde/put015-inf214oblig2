The Witching Hour (1985 film)
 
{{Infobox film
| name           = The Witching Hour
| image          = 
| caption        = 
| director       = Jaime de Armiñán
| producer       = 
| writer         = Jaime de Armiñán
| starring       = Francisco Rabal
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Francisco Rabal as César
* Concha Velasco as Pilar
* Victoria Abril as Saga
* Sancho Gracia as Rubén Blázquez y Delgado de Aguilera
* Asunción Balaguer as Monja
* Juan Echanove as Telmo

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 