Ultraman Story
 
 
{{Infobox film
| name = Ultraman Story
| image = Ultraman Story.jpg
| image_size = 
| caption = Theatrical release poster
| director = Kōichi Takano 
| producer = Noboru Tsuburaya Kiyotaka Ugawa
| writer = Yasushi Hirano
| narrator = Yoshio Kanauchi
| starring = Tarō Ishida
| music = Toru Fuyuki Shunsuke Kikuchi
| cinematography = 
| editing = 
| distributor = Tsuburaya Productions
| released =  
| runtime = 93 minutes
| country = Japan Japanese
| budget = $7 million
| gross = $8.6 million
| preceded_by = 
| followed_by = 
}}
 1984 Japanese tokusatsu kaiju film directed by Koichi Takano and produced by Tsuburaya Productions. The film features the kaiju superhero Ultraman.

==Plot==
 

==Cast==
* Tarō Ishida as Father of Ultra
* Masako Ikeda as Mother of Ultra
* Hiroya Ishimaru as Ultraman Taro 
* Masako Nozawa as Taro (child)
* Yoshio Kaneuchi as Narrator

==Bibliography==
*  
*  
*   
*  
*  

==Notes==
 

 

 
 
 
 

 