Nerosubianco
{{Infobox film
 | name = Nerosubianco
 | image = Nerosubianco.jpg
 | caption =
 | director = Tinto Brass
 | writer = Tinto Brass Francesco Longo Giancarlo Fusco
 | starring = Anita Sanders Freedom
 | cinematography = Silvano Ippoliti
 | editing =  Tinto Brass
 | producer = Dino De Laurentiis
 | distributor =
 | released = 26 February 1969 	
 | runtime = 85 mins
 | awards =
 | country = Italy
 | language = Italian, English
 | budget =
 }} Italian black comedy (part collage film) directed by Tinto Brass. The film deals with a variety of contemporary themes such as sexual freedom, racial tensions, and political radicalism from the perspective of a young upper-class Italian woman. The film has also been titled rather exploitatively like The Artful Penetration of Barbara and Black on White.   

Nerosubianco shooting began in October 1967 and it was premiered at the 1968 Cannes Film Festival.  The film saw theatrical release in February 1969.

==Plot== Hyde Park for his business transactions and Barbara starts sightseeing, soon to realise that an African American man (Terry Carter) is luring her. She sees it as an opportunity for an adventurous outreach to a new world and as her observations intermingle with her fantasies, she begins to question her own life.

==Cast==
*Anita Sanders: Barbara
*Terry Carter: the man
*Nino Segurini: Paolo
*Umberto Di Grazia: psychic/himself
*Tinto Brass: gynecologist (cameo)
* 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 