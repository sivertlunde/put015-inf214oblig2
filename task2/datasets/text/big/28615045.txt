TPB AFK
{{Infobox film
| name           = TPB AFK: The Pirate Bay Away From Keyboard
| image          = TPBAFKposter.tif
| caption        = Cover art
| director       = Simon Klose
| producer       = Martin Persson
| screenplay     = Simon Klose
| starring       = {{Plainlist|
* Gottfrid Svartholm
* Fredrik Neij
* Peter Sunde }}
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 82 minutes
| country        = Sweden
| language       = Swedish English English (subtitles)
| budget         =
| gross          =
}}
TPB AFK: The Pirate Bay Away From Keyboard is a  , Fredrik Neij and Gottfrid Svartholm.  Filming began in Summer 2008, and concluded on 25 February 2012. 

==Production==
The films website was launched on 28 August 2010, along with a Kickstarter campaign to raise US$25,000 to hire an editor after the Court of Appeal The Pirate Bay trial|trial. The campaign was fully funded within three days and raised $51,424 in total.    In February 2011, the Swedish governments Arts Grants Committee Konstnärsnämnden  granted the project an additional 200,000 SEK (≈ $30,000). 

==Release==
The full film was released under a Creative Commons  license (BY-NC-ND) onto The Pirate Bay and other BitTorrent sites. Also, a four minute shorter version was released at the same time for those who wish to remix or re-edit their own version of the film, featuring an edit with certain copyright restricted content removed, under a different Creative Commons   license.
 digital download. The pre-order price of the DVD is $23 and a digital download is $10, which comes with deleted scenes and bonus material. Pre-orders can be made via the official movie website. TPB AFKs premiered at the 63rd Berlin International Film Festival on 8 February 2013,  opening the festivals documentary section, and was released online for worldwide free download (or purchase) at exactly the same time on YouTube and on The Pirate Bay. 

On 19 February 2013, the film was broadcast on BBC Four in the UK as part of the BBCs Storyville (television series)|Storyville documentary series strand.

==Reception==
Peter Sunde, one of the main characters in the film, wrote on his blog, that he has "mixed feelings about the movie and the release of it". While he likes the technical side, he has serious issues with some scenes and general attitude of the film. This includes too much focus put on the trial, too dark depiction of it and portraying himself beyond self recognition. Despite having such different views on the subject, he regards the director as a friend. 

==Censorship by Hollywood== Hollywood studios Fox and Lionsgate started DMCA takedown notices to Google requesting the removal of links related with the TPB AFK.   In response, Simon Klose contacted Chilling Effects which recommended him to file a DMCA counter-notice where he explained that the purpose is "to share the film as much as possible".  Two months later, the censored links were reinstated only after public complaints made by the film director. 

==References==
 

==External links==
===Main sites===
*   – official site
*   on Kickstarter
*  
*  
*   — as part of BBCs Storyville (television series)|Storyville TV documentary strand

===Social networking sites===
*  
*  
*  

===Documentary online===
*   on The Pirate Bay
*   on YouTube Mega

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 