Kadhal Kottai
{{Infobox film
| name = Kadhal Kottai
| image = Kaadhal Kottai poster.jpg
| caption = Poster
| director = Agathiyan
| writer = Agathiyan Devayani Heera Karan
| producer = Sivashakthi Pandian
| cinematography = Thangar Bachan
| editing        = Lancy-Mohan Deva
| studio = Sivashakthi Movie Makers
| distributor = Sivashakthi Movie Makers
| released =   
| country = India
| language = Tamil
| budget =
| gross =  
}} Devayani play Karan who played important roles. 
 Telugu as Kannada as Yaare Neenu Cheluve (1998). Agathiyan also remade the movie in Hindi titled Sirf Tum (1999. This film also remade in Bengali titled Hothat Brishti (1998) directed by Basu Chatterjee . The film was released as a novel in 2010 under the same name. 

==Plot==
Kamali (Devayani (actress)|Devayani) lives with her sister and brother-in-law in Ooty and is searching for a job. While in Madras, her purse is stolen and she fears she has lost her university certificates. Surya (Ajith Kumar) who works in Jaipur finds the purse and sends it back to her, and a love develops through letters and phone calls. They agree to love each other without meeting, and Surya eventually moves to Madras for work. However Kamalis brother-in-law is pressuring her to marry a suitable man and Suryas boss keeps trying to seduce him. Eventually, Kamali and Surya unite through a run of coincidences.

==Cast==
* Ajith Kumar as Surya Devayani as Kamali
* Heera Rajgopal as Neha Raja as Jeeva Karan as Siva
* Manivannan as Kaliyaperumal
* Thalaivasal Vijay as Paneer Rajeev as Sekhar Pandu as Ramasamy Ramji in a special appearance Rani in the song "Vellarikka"

==Production==
The basic theme of the film was inspired from "Puranaanooru" literature on the relationship between King Kopperum Chozhan and poet Pisirandhaiyar, though they never met but they developed a strong relationship until their deaths. Agathiyan confirmed this in his interviews.  Agathiyan initially wanted to make his directorial debut with this subject since no producer were willing to produce the film, he made few films and earned his breakthrough with Vaanmathi.  Post its success, he chosen Ajith, actor from that film to play the leading role with Sivasakthi Pandian agreeing to produce the film. 

Agathiyan had asked Devayani to change her glamorous on screen image for the film, and Devayani duly agreed with the film becoming the start of several other similar roles for her.  

==Soundtrack==
The music composed by Deva (music director)|Deva. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Agathiyan || 5:05
|- Deva || 4:26
|-
| 3 || "Mottu Mottu Malaradha" || Swarnalatha || 4:54
|-
| 4 || "Nalam Nalamariya Aval" - 1 || S. P. Balasubrahmanyam, Anuradha Sriram || 4:48
|-
| 5 || "Nalam Nalamariya Aval" - 2 || Krishnaraj, Anuradha Sriram || 4:49
|-
| 6 || "Sivappu Lolakku" || S. P. Balasubrahmanyam || Ponniyin Selvan || 5:23
|-
| 7 || "Vellarikka" || Deva, Krishnaraj || Agathiyan || 4:23
|}

==Release==
Tamil magazine Ananda Vikatan appreciated the film by giving 52 marks and mentioned that "The basic plot is a novel idea which has been given a good shape and life by the filmmaker". 

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|- 1998
|Yaare Neenu Cheluve Kannada
|V Sangeetha
|D. Rajendra Babu
|- 1998
|Hothat Brishti Bengali
|Ferdous Ahmed, Priyanka Trivedi Basu Chatterjee
|- 1999
|Sirf Tum Hindi
|Sanjay Kapoor, Priya Gill Agathiyan
|-
|}

==Awards== National Film Awards- 1996
*   Best Screenplay: Agathiyan

;Tamil Nadu State Film Awards- 1996
*   Devayani
* Best Editor: Lachi Mohan Best Costume Designer: Maasanam  

==References==
 

==External links==
*  

==Bibliography==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 