Samson (1961 Polish film)
{{Infobox Film
| name = Samson
| image =samsonwajda.jpg
| image_size =
| caption =
| director = Andrzej Wajda
| producer = Hal B. Wallis
| writer = Kazimierz Brandys Andrzej Wajda
| narrator =
| starring = Serge Merlin Alina Janowska
| music =Tadeusz Baird
| cinematography =
| editing =
| distributor =
| released = September 11, 1961
| runtime = 117 minutes
| country = Poland
| language = Polish
| budget =
| preceded_by =
| followed_by =
}}
 1961 film Polish director Andrzej Wajda that uses art house aesthetics to tell a story about the Holocaust. Wajdas World War II film alludes to the Old Testament story of Samson, who had supernatural physical strength. But unlike the Biblical character, Wajdas Samson has great emotional strength.

==Plot== Jewish ghetto, expressionist cinematography and the weighty issues facing the Jewish people.

The construction of the Jewish ghetto is communicated through a single, stationary shot. A shabbily dressed mass is clustered in front of the camera, and a pair of hands with a hammer and nails secures one board at a time, until the shot of people has been replaced with a shot of a wall. Through minimalism and simplicity, Wadja establishes a separation between the world of the impoverished Jew and the world outside the ghetto. The viewer looking on as the ghetto walls block the view of what happening inside, is made to feel detached from the horror inside.

One question Wajda raises is that of Jewish solidarity and the guilt of being saved while ones brethren are suffering. Samson escapes from the Jewish ghetto but immediately wants to return. Although he could enjoy a comfortable life of cocktails and women, hed rather be in the ghetto, collecting corpses off the streets. Samson argues that his place is with the Jews, that he should suffer alongside them. A fake-blond beauty offers a different take. She confides to Samson that shes Jewish and has been concealing her roots in order to avoid the ghetto. Although she argues passionately, Samsons emotional strength inevitably inspires her to accept her fate as a Jew.

When Samson is bruised and exhausted, lying on the ground, he is encouraged by a close friend who says, “one man can suffer such blows and rise again.” For Wajda, this is the greatness displayed in Jewish history. Samson is a scrawny, haggard young man, who says very little and might almost border on boringly average; but he has the ability to rise again despite any blow, proving his strength of spirit.

==Production==
Of Samson, Wajda wrote, On first reading Kazimierz Brandyss novel Samson, I remembered the drawings by Gustave Doré I had seen as a child, and I started dreaming of making a modern film version of the great Biblical tale. What the novel demanded of a director, however, was simplicity, modesty and, above all, respect for details.
 Jerzy Wójcik and myself, realized the power of narrative shortcuts and the impact of symbolism on the screen. We wanted to continue in that direction. Brandys novel, however, contradicted and desperately resisted our concepts.   }}

==Reception==
Georges Sadoul
"Les Lettres Françaises", Paris, 1964
"In its first part, the film is a masterpiece. Never before has Wajda revealed such virtuosity. He has not succumbed to the temptation of formal exercise. Far from any baroque mannerism, he says what he has to say firmly, even brutally, while using a minimum of effects, in shades nearly classical. This style present throughout the film reveals a great talent on the threshold of maturity."    

Konrad Eberhardt
"Film", Warsaw, September 1961
No attempt has been made to discuss this new offer, so different from Wajdas previous works, in terms of creative, rather than propaganda merits, or the authors intentions and the values which the film contributes to our cinematography. 

==Cast==
*Serge Merlin ...  Jakub Gold 
*Alina Janowska ...  Lucyna 
*Elżbieta Kępińska ...  Kazia, Malinas Niece 
*Jan Ciecierski ...  Józef Malina 
*Tadeusz Bartosik ...  Pankrat 
*Władysław Kowalski (actor)|Władysław Kowalski ...  Fialka 
*Irena Netto ...  Jakubs Mother 
*Beata Tyszkiewicz ...  Stasia 
*Jan Ibel ...  Genio 
*Bogumil Antczak ...  Prisoner 
*Edmund Fetting ...  Guest at Lucynas Party 
*Roland Głowacki ...  Guest at Lucynas Party 
*Andrzej Herder ...  Gestapo Officer 
*Zygmunt Hübner ...  Gestapo Officer 
*Zofia Jamry ...  Woman Blackmailing Malina

==Awards==
*Nominated for a Golden Lion in 1961 at the Venice Film Festival

==Trivia==
Serge Merlin went on to play the cyclops leader, Gabriel Marie, in The City of Lost Children (1995), and in Amelie (2001) he played Dufayel, an elderly artist that Amalie befriends.

==Notes==
 

==References==
*{{cite news 
  | last = Sadoul
  | first =Georges
  | title= Samson
  | publisher = Les Lettres Fancaises
  | year = 1964
  | url =http://www.wajda.pl/en/filmy/film06.html
  | accessdate = July 23 }}
*{{cite news 
  | last =Eberhardt
  | first =Konrad
  | title = Samson
  | publisher =Film
  | date = September 1961
  | url =http://www.wajda.pl/en/filmy/film06.html
  | accessdate = July 23 }}
*{{Cite web
  | last = Wajda
  | first = Andrzej
  | title = Samson
  | year = 2000
  | url =http://www.wajda.pl/en/filmy/film06.html
  | accessdate = July 23
  | postscript =   }}

==See also==
*The Fifth Horseman is Fear

==External links==
* 
*  
*   

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 