Le Poisson noyé
 

{{Infobox film
| name           = Le Poisson noyé 
| image          = 
| caption        = 
| director       = Malik Amara
| producer       = Nomadis Images
| writer         = 
| starring       = Fethi Akkari, Fatma Bensaidane, Sondos Belhassen
| distributor    = 
| released       = 2008
| runtime        = 18
| country        = Tunisia
| language       = 
| budget         = 
| gross          = 
| screenplay     = Malik Amara
| cinematography = Nebil Saidi
| sound          = Karim Gmira
| editing        = Penda Houzanbge
| music          = 
}} 2008 film.

==Synopsis==
This fable tells of an old fishmonger who dies and resuscitates several times. The man is a despicable character and his resurrections gives rise to general curiosity. But superstition soon takes over in the villagers’ hearts, they believe nature should reclaim its rights.

==Awards==
* Beirut International Film Festival 2008
* FESPACO 2009

==References==
 

 
 
 


 