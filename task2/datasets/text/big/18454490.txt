Escape to Witch Mountain (1975 film)
 
{{Infobox film
| name           = Escape to Witch Mountain
| image          = Escape_to_witch_mountain_movie_poster.jpg
| image_size     =
| caption        = Theatrical release poster John Hough Ron Miller Jerome Courtland Robert M. Young (screenplay) Alexander Key (novel)
| starring       = Eddie Albert Ray Milland Donald Pleasence Kim Richards Ike Eisenmann
| music          = Johnny Mandel
| cinematography = Frank V. Phillips, ASC
| editing        = Robert Stafford Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 97 min.
| country        = United States
| language       = English
| gross          = $20,000,000 
}} Walt Disney Buena Vista John Hough.

==Plot== telepathically to Tony, commune empathically with animals, and experience Precognition|premonitions. Tia also possesses minor telekinetic abilities. She carries a "star case" with her at all times, which eventually reveals a strange map.

Tia has fragmented memories of her early childhood, including an accident at sea and a man she later remembers as the childrens Uncle Bené ( ), who they believe drowned during their rescue. 

During a field trip to see a movie, Tia experiences a premonition and warns wealthy attorney Lucas Deranian against a potentially dangerous accident. Deranian informs his employer, millionaire Aristotle Bolt, of the childrens unique abilities. Bolt, obsessed with the paranormal, demands that Deranian retrieve the children at all costs. Deranians detective work leads him to the orphanage, where he poses as Tia and Tonys uncle, though not under the name Bené, and takes them to Bolts mansion.

Though initially suspicious of Bolts motives, Tia and Tony are lured in by the wealthy trappings of Bolts home. Bolt eventually reveals that he has been monitoring the children via a closed-circuit television system, and that he and Deranian are fully aware of their unusually strong powers. The night of this revelation, Tia and Tony make an escape, using their abilities to psionically control a wild mustang, guard dogs, and the security fence, as well as using Winkie, Tias cat, to make the allergic security guard let them pass.
 Winnebago motor home owned by a crotchety widower named Jason ODay (Eddie Albert). Initially negative towards the children, Jason gradually begins to recognize their powers and the truth of their story; Tias vague memories of a disaster at sea intrigue him. He agrees to take the children on the route indicated by Tias star case, which leads them to a mountain known as Witch Mountain, home to unexplainable phenomena. Avoiding Bolt, the law and an incited mob convinced the children are witches, they eventually make their way up Witch Mountain, pursued by Deranian and Ubermann, as well as by Bolt in a helicopter.
 extraterrestrial origin; the double star emblem on the star case stands for a binary star system where their home planet was located.

Having come to Earth because their own planet was dying, survivors of the journey made their way to Witch Mountain and formed a community to await the surviving children, each pair in possession of a star case to help them find their way to their new home. Tony and Tia are the first to reach their destination. The children are reunited with their Uncle Bené (who survived after all, thanks to an "accommodating" shark whom hed telepathically asked for help) and board another spacecraft. When Bolt and the others leave in defeat, Jason witnesses the spaceships return as it flies over him to say a final goodbye.

==Cast==
* Eddie Albert as Jason ODay, an embittered widower who decides to travel across the country in his motor home.
* Ray Milland as Aristotle Bolt, a ruthless and greedy multi-millionaire obsessed with the paranormal and occult who intends to exploit Tony and Tias powers to increase his wealth.
* Donald Pleasence as Lucas Deranian, a well-to-do attorney who works for Mr. Bolt.
* Kim Richards as Tia Malone, a nine-year-old orphan with psychic powers.
* Ike Eisenmann as Tony Malone, older brother to Tia, orphan with telekinetic powers. Walter Barnes as Sheriff Purdey, a sheriff bribed by Bolt to pursue the children.
* Reta Shaw as Mrs. Grindley, owner of the orphanage Tia and Tony are sent to after the death of their foster parents, the Malones.
* Denver Pyle as Uncle Bené, the childrens true uncle.
* Alfred Ryder as Astrologer.
* Lawrence Montaigne as Ubermann, a henchman who assists Deranian in his pursuit of the siblings. Terry Wilson as Biff Jenkins.
* George Chandler as Grocer.
* Dermott Downs as Truck, a child from the orphanage who bullies Tony to the point that Tony reveals his powers.
* Shepherd Sanders as Guru.
* Don Brodie as Gasoline Attendant.
* Paul Sorenson as Sergeant Foss.
* Alfred Rossi as Police officer.
* Tiger Joe Marsh as Lorko, a security guard of Mr. Bolts who is allergic to cats.
* Harry Holcombe as Captain Malone.
* Sam Edwards as Mate.
* Dan Seymour as Psychic.
* Eugene Daniels as Cort.
* Al Dunlap as Deputy.
* Rex Holman, Tony Giorgio as Hunters.
* Kyle Richards as young Tia Malone (in "elder Tias" memories) (uncredited)

== Differences from novel ==
 
Escape to Witch Mountain is based on the novel by Alexander Key. Significant differences from the book include its tone and plot elements. For example, in the book, the children are befriended by Father ODay, a crusty, old Catholic priest, rather than widower Jason ODay.  The childrens ship is shot down, rather than crashed, and the children are olive-skinned, though with light-colored hair, rather than fair-skinned and blond. In the book, Deranian is the main antagonist, and he is working for a shadowy European cabal who are trying to capture the children for their special powers, instead of for Aristotle Bolt.   The novel is set along or near the Atlantic coast of the United States, whereas the film was shot along the Pacific coast in California.

==Remake==
 
The movie was remade in 1995 with a different cast and several details changed or omitted. In the remake, Tia and Tony are twins, renamed Anna and Danny. They are separated as children, but reunite in an orphanage where they discover their powers. In the remake, Bolt is a land magnate who wants to develop Witch Mountain, using the twins power to do so. The children are rescued by a shopkeeper. The 2009 movie Race to Witch Mountain is a continuation of the 1975 movie.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 