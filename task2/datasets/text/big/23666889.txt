Devotion (1950 film)
{{Infobox film
| name           = Devotion
| image          =Devotion (1950 film).jpg
| image_size     =
| caption        =
| director       = Augusto Genina
| producer       = Cines
| writer         =
| narrator       =
| starring       =
| music          = Antonio Veretti
| cinematography =
| editor         =
| distributor    =
| released       = 1950
| runtime        =
| country        = Italy Italian
| budget         =
}}
 1950 Cinema Italian film directed by Augusto Genina.

The film is adapted from the famous Italian novel LEdera (novel)|LEdera written by Grazia Deledda and published in 1908.

==Synopsis==
Annesa (played by Columba Dominguez), is a from a declining aristocratic family and very much devoted to her family, the Decherchi, which is at the edge of financial collapse. The fate of the family depends on the success of their efforts to inherit the estate of a distant uncle who, is hosted by the Decherchis, because of his ill health and is now on the verge of dying. Annesa, exacerbated by the refusal of the uncle to help financially Annesas financially strapped nephew Don Paulu Decherchi (played by Roldano Lupi), that she is secretly in love with, murders the uncle. Despite the suspicions and investigations by the police, the murder is not discovered. But Annesa, overcome by remorse, confesses her deed to a priest.

==Cast==
* Columba Dominguez: Annesa
* Roldano Lupi: Don Paulu Decherchi
* Juan De Landa: Virdis, il sacerdote
* Franca Marzi: Zana
* Emma Baron: Donna Francesca
* Nino Pavese: Salvatore Spanu
* Gualtiero Tumiati: Zio Zua

==External links==
*  

 

 
 
 
  
 
 
 
 
 