Hot Lead and Cold Feet
{{Infobox film
| name = Hot Lead and Cold Feet
| image = Hot Lead and Cold Feet.jpg
| caption =  Robert Butler
| producer = Ron W. Miller Don Nelson Rod Piffath
| starring = Don Knotts Darren McGavin Jim Dale Buddy Baker
| cinematography = Frank V. Phillips
| editing = Ray de Leuw Walt Disney Productions Buena Vista Distribution
| country = United States
| released =  
| runtime = 90 minutes
| language = English
| budget = 
}} Walt Disney Productions and distributed by  Buena Vista Distribution Company, starring Jim Dale, Don Knotts, Karen Valentine, Darren McGavin, and Jack Elam released on July 5, 1978.

==Plot==
Jasper Bloodshy (Dale) runs the rough-and-tumble town of Bloodshy—named after him because he founded it—which lives in fear of Jaspers gunslinging son Wild Billy (also played by Dale). Jasper has just found out he has another son named Eli (again, played by Dale), who lives in Philadelphia.

It turns out that years ago, Jaspers crazy ways were too much for his bride from England, so she left—leaving behind one twin—and returned to England. With the help of his English butler Mansfield, he writes a new will that mentions Eli, then fakes his death by pretending to fall off a cliff in front of Bloodshys corrupt mayor Ragsdale (McGavin) and sheriff The Denver Kid (Knotts), both of whom he has just told about his second son.

We next meet Eli, who turns out to be the opposite of Wild Billy. Eli has been trained to live for the Lord. He works as a Salvation Army missionary in Philadelphia with orphans named Roxanne (Debbie Lytton) and Marcus (Michael Sharrett).

One day, during a fight in which people are throwing vegetables at him and the children, Eli receives a telegram informing him about his fathers death, a father he didnt know existed. He decides to accept the invitation to come to Bloodshy for his inheritance and take Marcus and Roxanne.

Their stagecoach is held up by the Snead brothers, a group of outlaws that Ragsdale has sent to run off Eli. Unfortunately, nobody was told that Jaspers other son was a twin, so they mistake Eli for Wild Billy (the first of many to mistake the two).

The Sneads return to Bloodshy, but did cause the stagecoach to run off, leaving Eli, Marcus, and Roxanne stranded. On their way to Bloodshy (by foot), they meet a woman named Jenny (Valentine) who is also headed for Bloodshy to start a school. They head for the town together.

Mansfield brings the will to Sheriff Denver to deliver to Ragsdale. From there, its learned that a contest is involved in the inheritance. Ragsdale sends Denver to find Billy and tell him that the fortune is his.

The contest is a miles-long obstacle course known as the Bloody Bloodshy Trail that involves operating train engines, crossing a gorge using a rope, climbing a mountain, and driving a wagon.

During the contest both Eli and Billy realize that Ragdale has set them up to kill each other so that he would collect the entire fortune. Both brothers make up and expose Ragdale for what he really is. Soon after Ragdale is imprisoned. Denver Kid becomes the new mayor and makes his sheriff Rattlesnake, who throughout the film was trying to get his job as sheriff.

==Cast==
* Jim Dale as Jasper Bloodshy/Wild Billy Bloodshy/Eli Bloodshy
* Karen Valentine as Jenny, a teacher
* Don Knotts as Sheriff Denver Kid
* Darren McGavin as Ragsdale
* Jack Elam as Rattlesnake

==Production==

The films theatrical accompaniment was a reissue of the 1949 Disney short The Wind in the Willows (from The Adventures of Ichabod and Mr. Toad) re-titled The Madcap Adventures of Mr. Toad.

Eli Bloodshys Iron Donkey, number 11, can be found in the queue for Big Thunder Railroad at Disneyland in California.  A second train is also located on the trail from Big Thunder Ranch to Fantasyland.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 