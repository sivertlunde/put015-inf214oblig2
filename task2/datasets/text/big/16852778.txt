Don't Touch the White Woman!
{{Infobox film
| name = Dont Touch The White Woman!
| image = Dont Touch the White Woman!.jpg
| caption =
| director = Marco Ferreri
| producer = Jean-Pierre Rassam, Jean Yanne, Alain Sarde, François Rochas
| writer = Marco Ferreri, Rafael Azcona
| starring = Marcello Mastroianni, Catherine Deneuve, Darry Cowl, Alain Cuny, Ugo Tognazzi, Paolo Villaggio, Franca Bettoia, Monique Chaumette, Francine Custer
| music = Philippe Sarde
| cinematography = Étienne Becker
| editing = Ruggero Mastroianni
| distributor = Image Entertainment
| released = 23 January 1974
| runtime = 110 minutes
| country = France Italy
| language = French
| budget =
| gross = $1,402,866 
}} Native American scouts, who runs a curio shop selling Native artifacts made in sweatshops by white women. Alain Cuny plays Sitting Bull who must defend his people when their homes (apartment buildings) are destroyed by the Union Cavalry. The movie climaxes with the Battle of the Little Bighorn held in a large construction excavation where Les Halles market used to be. The language used to justify the conflict parodies the Vietnam War and the Algerian War.

==Cast==
* Catherine Deneuve - Marie-Hélène de Boismonfrais
* Marcello Mastroianni - George A. Custer
* Michel Piccoli - Buffalo Bill
* Philippe Noiret - Gen. Terry
* Ugo Tognazzi - Mitch
* Alain Cuny - Sitting Bull
* Serge Reggiani - The Mad Indian
* Darry Cowl - Major Archibald
* Monique Chaumette - Sister Lucie
* Daniele Dublino - Daughter
* Henri Piccoli - Sitting Bulls Father
* Franca Bettoia - Rayon de Lune (as Franca Bettoja)
* Paolo Villaggio - The CIA agent
* Franco Fabrizi - Tom (as Franco Fabrizzi)
* Laurente Vedres - (as Vedres et Boutang)

==See also==
* Mr. Freedom
* Blazing Saddles
* Revisionist Western
* Cultural depictions of George Armstrong Custer

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 
 