Illuminata (film)
{{Infobox Film
| name           = Illuminata
| image          = Illuminata DVD Cover.jpg
| image_size     = 
| caption        = DVD cover
| director       = John Turturro
| producer       = 
| writer         = Brandon Cole   John Turturro
| narrator       = 
| starring       = Katherine Borowitz   Christopher Walken   Susan Sarandon
| music          = Arnold Black   William Bolcom
| cinematography = Harris Savides
| editing        = Michael Berenbaum
| distributor    =  Cannes Film Festival)   France: April 28, 1999   USA: August 6, 1999
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Illuminata is a 1998 romantic comedy film directed by John Turturro and written by Brandon Cole and John Turturro, based on Coles play. The cinematographer was Harris Savides. The puppet sequences were done by Roman Paska. Music for the Tuccio Operatic Dream Sequence was composed by Richard Termini.

==Synopsis==
The plot follows the backstage dramas of a theater company in turn of the century New York while it struggles to produce a new work.

==Cast==
*John Turturro - Tuccio, the playwright
*Katherine Borowitz - Rachel, Tuccios girlfriend, an actress
*Beverly DAngelo - Astergourd, a theater owner
*Donal McCann - Pallenchio, Astergourds husband
*Georgina Cates - Simone, an actress
*Susan Sarandon - Celimene, an aging actress
*Christopher Walken - Umberto Bevalaqua, an influential theater critic
*Ben Gazzara - Old Flavio, an actor in the company
*Rufus Sewell - Dominique, a young actor in the company
*Bill Irwin - Marco, a bit player and the object of Bevalaquas affections
*Aida Turturro - Marta
*Rocco Sisto - Prince
*Matthew Sussman - Piero

==Awards==
* 1998 Cannes Film Festival – Palme dOr Nomination (John Turturro)   

==References==
 

==Notes==
*Martin, Mick. DVD & Video Guide 2007. New York: Ballantine Books, 2006.
*"Backstage With a Cast Of Foibles and Follies" The New York Times, August 6, 1999.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 