Daaliya Pookkal
{{Infobox film 
| name           = Daaliya Pookkal
| image          =
| caption        =
| director       = Prathap Singh
| producer       =
| writer         = PN Chandran
| screenplay     = PN Chandran
| starring       = Pappan Roopesh Shobha Jameela Malik
| music          = Kanjangad Ramachandran
| cinematography = Beypore Mani
| editing        =
| studio         = Akshara Films
| distributor    = Akshara Films
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by Prathap Singh. The film stars Pappan, Roopesh, Shobha and Jameela Malik in lead roles. The film had musical score by Kanjangad Ramachandran.   

==Cast==
*Pappan
*Roopesh
*Shobha
*Jameela Malik
*MG Soman
*Mala Aravindan
*Mallika Sukumaran
*PK Venukkuttan Nair

==Soundtrack==
The music was composed by Kanjangad Ramachandran and lyrics was written by KK Venugopal. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Rajanigandhikal Vidarum || Vani Jairam || KK Venugopal || 
|-
| 2 || Swapna Bhoovil || Jolly Abraham || KK Venugopal || 
|}

==References==
 

==External links==
*  

 
 
 

 