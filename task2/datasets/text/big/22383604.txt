Mira (film)
{{Infobox film
| name           = Mira
| image          = 
| caption        = 
| director       = Fons Rademakers
| producer       = Jan van Raemdonck Gérard Vercruysse
| writer         = Hugo Claus Magda Reypens Stijn Streuvels
| starring       = Willeke van Ammelrooy
| music          = 
| cinematography = Eduard van der Enden
| editing        = Jan Dop
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Netherlands Belgium
| language       = Dutch
| budget         = 
}}
 Best Foreign Language Film at the 44th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Willeke van Ammelrooy as Mira
* Jan Decleir as Lander
* Carlos van Lanckere as deken Broeke
* Luc Ponette as Maurice Rondeau
* Roger Bolders as Sieper
* Mart Gevers as Manse
* Freek de Jonge as Treute
* Charles Janssens as Snoek
* Josephine van Gasteren as Moeder van Maurice
* Fons Rademakers as Notaris
* Romain DeConinck as Landmeter
* Ann Petersen as Hospita
* Ward de Ravet as Rijkswachter
* Jo Gevers
* Bert André (as Marc André)

==See also==
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 