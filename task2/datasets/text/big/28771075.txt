The Adventures of Buratino (1975 film)
{{Infobox film
| name           = The Adventures of Buratino 
| image          = The Adventures of Buratino (1975 film).jpg
| image_size     = 200px
| border         = 
| alt            = 
| caption        = The Adventures of Buratino
| film name      = Приключения Буратино
| director       = Leonid Nechayev
| producer       = Belarusfilm
| writer         =  
| screenplay     = 
| story          = Carlo Collodi The Golden Key, or the Adventures of Buratino
| narrator       = 
| starring       =      
| music          = Alexey Rybnikov
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 132 minutes
| country        = Russia
| language       = Russian language
| budget         = NA
| gross          = NA
}}
The Adventures of Buratino (Russian: Приключения Буратино. Transliteration: Priklyucheniya Buratino) was a Soviet childrens musical film, made in 1975 at Belarusfilm. 
Марта Лаховец.   Брестская газета. №26 (393) 25 июня - 1 июля 2010. 

Directed by Leonid Nechayev, the film was an adaptation of The Golden Key, or the Adventures of Buratino by Alexey Tolstoy in turn an adaptation of The Adventures of Pinocchio by Carlo Collodi. Inna Vetkina wrote the screenplay for The Adventures of Buratino as well as several other films directed by Nechayev. 

The plot of the film follows Buratino (Italian for puppet), a boy made of wood, who meets the children of Karabas Barabas theatre and sets out to free them. In order to do so, he needs to unravel the mystery of a golden key given by the turtle Tortila.   and Pierrot, who act in the childrens theatre are part of commedia dellarte.

Music for the film was composed by Alexey Rybnikov and the lyricists included Bulat Okudzhava, and Yuri Entin. There was an early interest by the director Nechayev to work with Yuli Kim (then writing under the last name Mikhailov) as a songwriter. At that time Yuli Kim was banned from television, so they turned to Okuzhdava. Okudzhava wrote music as well as lyrics, though only the music of Rybnikov was used in the film. The songs that Okudzhava wrote were serious and philosophical, so Nechayev also incorporated lyrics by Yuri Entin and omitted some of Okudzhavas.  

Almost all of the children who acted in the film were from Minsk.  Dima Iosifov played Buratino. The adults in the cast were famous actors from the rest of the Soviet Union. Nikolai Grinko played Papa Carlo. Vladimir Etush played Karabas Barabas. Rina Zelyonaya played the turtle Tortila. Rolan Bykov played the cat Bazilio. 

A number of musical childrens films followed, by the makers of The Adventures of Buratino, including About Little Red Riding Hood in 1977. 

==Notes==
{{Cnote2|A|Лаховец 2010. "  многие из песен были настолько серьезно философски запутаны, что от некоторых тоже пришлось отказаться."
(  many of the songs were so intricate on a serious philosophical level, that some had to be abandoned.)
}}

==References==
 

==External links==
*  
*  
 

 
 
 
 
 