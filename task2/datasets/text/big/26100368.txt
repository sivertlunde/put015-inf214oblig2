Love Eterne (2011 film)
 
{{Infobox film
| name           = Love Eterne
| image          = love_eterne_page.jpg
| alt            =  
| caption        = Love Eterne film poster
| director       = Joseph Villapaz
| producer       = Joseph Villapaz
| writer         = Joseph Villapaz
| starring       = {{Plainlist|
* Melissa Navia
* James Gill
* Bonnie Piesse
* Romy Valentina
* Francesco Plazza
* Terilyn Marshelle-Fleming
* Jorell Stills Haigler
*  
* Julio Perez
* Joseph Villapaz
}}
| music          =
| cinematography = Joseph Villapaz
| editing        = Joseph Villapaz
| studio         =
| distributor    =
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = $10,000 
| gross          =
}}
Love Eterne is a 2011 romantic comedy film written and directed by Joseph Villapaz, and starring Melissa Navia and James Gill. It takes place in New York City. It received two Awards of Merits and an Honorable Mention at film festivals.

== Plot ==
 
Medina (Melissa Navia) gets up after hearing a voice in her dreams. She prepares to go out, has a coffee, and does a quick breath relaxation exercise. Her friend, Sidonia (Bonnie Piesse), arrives and finds Medina, who is looking at herself in the mirror with a sad expression. Sidonia tries to lighten the moment, and reminds Medina that her friends and family are waiting for her. Medina gets herself together and they head off to the funeral service for Medinas fiancé.

Medina is with her friend Tesla (Romy Valentina), who tells her she is love with Medinas brother, Enzo (Francesco Plazza), and is thinking of getting engaged. Medina is a bit surprised, but congratulates her. They try to visit an art exhibit, but the female security guard kicks them out, as she remembers the duos wild reputation. Medine is upset, but Tesla invites her and Enzo for drinks. Tesla and Enzo cheer Medina up with their light conversation.

 
Quinn (James Gill) looks depressed as he meets up with his friend Fera (Terilyn Marshelle-Fleming) at the street. He tells her that Nilda left him and took everything, including his possessions. He shows her the "Dear John letter" composed on bath tissue. Feras husband, Camden (Jorell Stills Haigler), arrives and they explain the situation. Fera and Camden console Quinn but he leaves to clear his mind. They follow Quinn to make sure he does not do something regretful.
 Lincoln Center.

For the promotion exam, Medina must knock Master down once before she is knocked down three times. In the first round, Medina goes down quickly after losing her balance from a kick to her thigh. In the second round, she falls from a palm strike to her chest. As she gasps for air, Tesla and Enzo show concern. Medina quickly plans a better offense, and wins the next round. Master promotes her and gives her some words of wisdom.

Some weeks later, Enzo and Tesla catch up to Medina and tell her they are now engaged. They are worried about Medinas isolation but Medina ensures them she is dealing with things. Enzo reminds Medina about the upcoming meeting at Françoises restaurant. Camden and Fera look after Quinn, while Tesla and Enzo worry about Medina.

Medina visits Sidonia, who works as a psychic. Sidonia tells Medina she will meet someone and be happy, but she must allow herself to fall in love again. Medina meets her ex-boyfriend, Jarrod, who is engaged to Lumina. He shows Medina some pastries which he learned how to make back when they were a couple, and wishes her a happy thirtieth birthday by "toasting" her with cannolis. Meanwhile, Quinn sniffs his Dear John letter one last time, and throws it in a garbage bin.

As she finishes a workout and prepares to leave, Medina senses a presence. She turns around and addresses the voice that sounds like Master, and is reminded of his advice at the exam. She encounters Sidonia, who is sitting on a chair, on the city sidewalk and meditating. Sidonia reminds her of the party later; Medina thanks her and walks off. Quinn and Fera see Sidonia and ask for directions. She provides it and gives Quinn a pack of tissues to clean his camera lens.

At Françoises, Medina talks to the voice and they agree to part ways. Fera interrupts the conversation as she recognizes Medina as her favorite fashion designer. Camden calms Fera down, but Quinn is stunned upon seeing Medina, and Medina is likewise frozen. They break their trance; Medina is about to cry; Quinn offers her a tissue from the pack that Sidonia gave him. As Medina takes one, Quinn apologizes for disturbing her and walks away. Medina smells the tissue and is full of joy; she quickly turns and requests another from Quinn, who eagerly gives her the whole pack. Camden and Fera inform Quinn that their dinner reservations were denied because of a party. Medina realizes its the same party that Enzo mentioned, so it must be for her. She invites Quinn, Camden and Fera to her party, and they excitedly accept. As they begin to walk away, Medina looks to the sky and smiles. With tissue in hand, she turns and joins Quinn and the others.

== Cast ==
 
* Melissa Navia as Medina
* James Gill as Quinn
* Bonnie Piesse as Sidonia - In an interview with Back Stage, Piesse noted that she was looking for a spiritually fulfilling role and liked that the character was a psychic. "Shes always giving words of wisdom to the lead character, and I really resonated with what she was saying." 
* Romy Valentina as Tesla
* Francesco Plazza as Enzo
* Terilyn Marshelle-Fleming as Fera
* Jorell Stills Haigler as Camden
* Andre Correa as Jarrod
* Julio Perez as Master
* Joseph Villapaz as the Waiter

== Soundtrack ==
 
The following tracks are in Love Eterne:
* "There For Me" - Bonnie Piesse, performer
* "Swirling" - Mark Petrie, composer
* "Bason Journe" - Jørn Lavoll, composer
* "Ghost Processional" - Kevin MacLeod, composer
* "Time Passes" - Kevin MacLeod, composer
* "Atlantean Twilight" - Kevin MacLeod, composer

The following tracks are in Love Eterne mourning:  {{cite web
| date = 2014-02-03
| url = http://www.imdb.com/title/tt3420044/soundtrack?ref_=tt_ql_trv_7
| title = Love Eterne Mourning Soundtrack
| work = IMDB
| accessdate = 2014-02-03
}} 
* "Long Note - One" - Kevin MacLeod, composer
* "There is Romance" - Kevin MacLeod, composer
* "Touching Moments Two - Higher" - Kevin MacLeod, composer
* "Time Passes" - Kevin MacLeod, composer

NOTE: There is no soundtrack available.

== Release ==
On January 27, 2011 Villapaz posted a press release on PRWeb for the film.  Bonnie Piesse interviewed with Back Stage writer Dan Lehman. She described her music and film career, and her in role in Love Eterne.    From April–December 2011, Villapaz submitted the film to various film festivals.

== Reception ==
Mark Bell of Film Threat cited numerous technical issues that detracted from the film. While he anticipated the film because on the cheerful Friends-like opening credits that resembled a television pilot, Bell considered the audio horrible for almost all of the movie, and was annoyed by the smudges on the camera lens for some of the scenes. Despite this, he considered Navias performance of value when you can hear her.  Duane L. Martin of Rogue Cinema also noted the audio problems and sound control, as well as the smudges.  However, Richard Propes of The Independent Critic sympathized with the films low $10,000 budget. He likened Navias performance to Marisa Tomei and found the soundtrack music choices good. "There are bad reviews where Id say Dont see this film! and there are bad reviews where I lean more towards Give this filmmaker a chance. Hes learning. Love Eterne strikes me as a teachable moment film for a new filmmaker learning his way around the obstacles and challenges of ultra-indie filmmaking."   

== Awards and nominations ==
The film received some accolades among the film festivals. It received an Award of Merit in the Short Film category from The Accolade {{cite web
| date = 2011-04-27
| url = http://www.accoladecompetition.org/PDF_Winners/WinnersAccoladeDeadline052711.pdf
| format = PDF
| title = Accolade Competition Deadline: April 27, 2011
| work = The Accolade
| accessdate = 2012-01-01
}}  and from the Best Shorts {{cite web
| date = 2011-06-24
| url = http://bestshorts.net/PastWin/June_2011.pdf
| format = PDF
| title = Best Shorts Competition Deadline: June 24, 2011
| work = Best Shorts
| accessdate = 2012-01-01
}}  competitions. It was an Honorable Mention in the Narrative Feature category for the second edition of the 2011 Los Angeles Movie Awards. {{cite web
| date = 2011-09-07
| url = http://thelamovieawards.com/2011_%28II%29_Winners_.html
| title = LAMA 2011 (II) Winners
| work = Los Angeles Movie Awards
| accessdate = 2012-01-01}}  It was officially selected by the New York International Independent Film & Video Festival and NewFilmmakers New York, for screening in November and December, respectively, in New York City.

<!---- table take out as the awards can be summarized in prose paragraph
{| class="wikitable"
|-
! Competition !! Date !! Award !! Category
|- The Accolade || April 27, 2011 || Award of Merit  || Short Film
|-
| Best Shorts || June 24, 2011 || Award of Merit   || Short Film
|-
| Los Angeles Movie Awards (II)  || September 7, 2011 || Honorable Mention || Narrative Feature
|}

{| class="wikitable"
|-
! Official Selection !! Screening Date !! Venue
|-
| New York International Independent Film & Video Festival || November 20, 2011 || Quad Cinema, NYC
|-
| NewFilmmakers New York Annual Christmas Show || December 13, 2011 || Anthology Film Archives, NYC
|}

---->

== Love Eterne Mourning ==
 In 2013, with newer technology available, director Joseph Villapaz re-edited the films scenes from scratch, and fixed the sound quality flaws from the previous release. A new opening scene was completed which contains the voice of Troiano (portrayed by Henry Martinez), the deceased fiance. The text boxes which originally garnered negative reviews was also removed. Villapaz also added new music. {{cite web
| date = 2014-01-31
| url = http://www.imdb.com/title/tt3420044/soundtrack?ref_=tt_ql_trv_7
| title = Love Eterne Mourning Soundtracks
| work = IMDB
| accessdate = 2014-01-31
}}  The short was submitted to film festivals with the title LOVE ETERNE mourning and became an Official Selection at the Wizard World Film Festival in Portland.  {{cite web
| date = 2014-01-25
| url = http://issuu.com/jenna1/docs/programguide2014_portland_final_sin/42
| format = Adobe Flash
| title = Wizard World Film Festival Program Guide 2014
| work = Wizard World
| accessdate = 2014-01-26
}} 
The Bare Bones International Film Festival screened LOVE ETERNE mourning on April 5, 2014. {{cite web
| date = 2014-02-28
| url = http://www.barebonesfilmmusicfestival.org/id18.html
| title = Master Screening Schedule
| work = Bare Bones International Film & Music Festival
| accessdate = 2014-02-28
}}  {{cite web
| date = 2014-03-11
| url = http://barebonesfilmfest00.tripod.com/sitebuildercontent/sitebuilderfiles/2014day3satscheduleataglance.pdf
| title = Saturday Fest Day 3
| work = Bare Bones International Film & Music Festival
| accessdate = 2014-03-14
}}  {{cite web
| date = 2014-03-11
| url = http://www.americantowns.com/ok/muskogee/events/bare-bones-film-festival-saturday-screening-at-roxy-theater-2014-04-05
| title = Bare Bones Film Festival Saturday Screening at Roxy Theater
| work = AmericanTowns.com
| accessdate = 2014-03-17
}}  {{cite web
| date = 2014-04-04
| url = http://www.muskogeephoenix.com/features/x493450611/Bare-Bones-schedule-for-today
| title = Bare Bones schedule for today
| work = MuskogeePhoenix
| accessdate = 2014-04-04
}} 
 {{cite web
| date = 2014-03-31
| url = http://retellity.com/event/facebook-event-380887-LOVE-ETERNE-mourning-Film-Screening-Muskogee-04052014
| title = LOVE ETERNE mourning Film Screening
| work = Retellity
| accessdate = 2014-04-05
}} 
It won the award for Best Family Dramedy Micro Short on April 13, 2014. {{cite web
| date = 2014-03-11
| url = http://barebonesfilmfest00.tripod.com/barebones2014/id60.html
| title = 2014 Award Nominees
| work = Bare Bones International Film & Music Festival
| accessdate = 2014-03-14
}}  {{cite web
| date = 2014-04-14
| url = http://www.muskogeephoenix.com/local/x493461630/Bare-Bones-International-Film-Festival-awards
| title = Bare Bones Film Festival awards
| work = MuskogeePhoenix
| accessdate = 2014-04-16
}} 
 

== References ==
 

== External links ==
* {{cite web
| url = http://www.azunnearts.com/LoveEterne/Blog/
| title = Love Eterne: The Official Film Blog
| work = Mantis Eagle Productions
}}
*  
*  
* {{cite web
| url = http://www.azunnearts.com/LoveEterneMourn/
| title = LOVE ETERNE mourning: The Official Film Blog
| work = Mantis Eagle Productions
}}
*  

 
 
 
 
 