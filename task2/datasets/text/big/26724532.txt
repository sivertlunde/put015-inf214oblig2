Three Secrets
{{Infobox film
| name           = Three Secrets
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wise
| writers        = Martin Rackin Gina Kaus
| narrator       = 
| starring       = Eleanor Parker Patricia Neal Ruth Roman
| music          = David Buttolph
| cinematography = Sidney Hickox
| editing        = 
| distributor    =  Warner Bros.
| released       = October 20, 1950
| runtime        = 98 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Three Secrets is a 1950 film directed by Robert Wise and released by Warner Bros.  It stars Eleanor Parker, Ruth Roman and Patricia Neal.  

==Plot==
A private plane crashes in the California mountains and a 5-year-old boy survives. Little else is known except the child is an orphan.

Susan Chase believes the boy could be hers. Before she was wed to lawyer Bill Chase, she was involved with a Marine during the war, and became suicidal later, putting their child up for adoption. Bill has never been told Susans secret.

Newspaper reporter Phyllis Horn investigates the crash. She, too, has a secret, having given birth after a divorce from husband Bob Duffy, who has since remarried. 

A third woman, Ann Lawrence, turns up at the crash site as well. Ann was once a chorus girl, involved with wealthy Gordon Crossley, who spurned her after she became pregnant. Scorned, Ann bludgeoned him to death, and served five years in prison for manslaughter, giving up the baby. The boy appears to be hers, but she believes Susan is better qualified to give the child a good home.

==Cast==
*Eleanor Parker as Susan Adele Connors Chase
*Patricia Neal as Phyllis Horn
*Ruth Roman as Ann Lawrence
*Frank Lovejoy as Bob Duffy Leif Erickson as Bill Chase

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 