Splice (film)
 
{{Infobox film
| name           = Splice
| image          = Splice-poster.jpg
| caption        = Final theatrical poster
| director       = Vincenzo Natali
| producer       = Steve Hoban
| screenplay     = {{plainlist|
* Vincenzo Natali
* Antoinette Terry Bryant
* Doug Taylor
}}
| story          = {{plainlist|
* Vincenzo Natali
* Antoinette Terry Bryant
}}
| starring       = {{plainlist|
* Adrien Brody
* Sarah Polley
* Delphine Chanéac
}}
| music          = Cyrille Aufort
| cinematography = Tetsuo Nagata
| editing        = Michele Conroy
| studio         = {{plainlist|
* Dark Castle Entertainment Gaumont
* Copperheart Entertainment
* Senator Entertainment
}} Warner Bros. Pictures
| released       =  
| runtime        = 104 minutes
| country        = {{plainlist|
* Canada
* France
* United Kingdom
}}
| language       = English
| budget         = $30 million   
| gross          = $26,857,459 
}} science fiction splicing animal genes.   Guillermo del Toro, Don Murphy, and Joel Silver executive produced.

==Plot== Genetic engineers hybrids for medical use at the company N.E.R.D. (short for Nucleic Exchange Research and Development). Their work previously yielded Fred, a dog-sized vermiform creature intended as a mate for their female specimen, Ginger. After successfully mating them, Clive and Elsa plan to create a human-animal hybrid that could revolutionize science. Their employers Joan Chorot (Simona Maicanescu) of N.E.R.D. and William Barlow (David Hewlett) forbid them from doing this. Instead, they are to find and extract proteins used for commercial drug production from Fred and Ginger. Clive and Elsa pursue their own agenda in secret. They develop a viable female creature (Abigail Chu).

Although they had planned to terminate the hybrid before it reached full term, Elsa persuades Clive to let it live. The hybrid subsequently becomes aggressive and stings Elsa several times. The hybrid sheds body parts in an effort to escape when they try to destroy it, but they subdue it anyway. They discover that she is aging at a vastly accelerated rate. Elsa discovers the creature is undergoing mental development such as that of a young human child. Elsa names the creature "Dren" after the creature spells out NERD having seen the letters on Elsas Shirt. Elsa subsequently refuses to let Clive refer to her as a "specimen".
After moving Elsa to a new location for fear of discovery they find Dren has a dangerously high fever. In an attempt to save her they place her in a large industrial sink full of cold water. Clive places a hand round Drens neck and pushes her under the water, seemingly drowning her, however it is found that Dren is Amphibian|amphibious, though it is unclear if Clive knew about this, having analysed scans of Dren, or if he did intend to kill her. 
 had spontaneously changed to a male, but Elsa and Clive failed to notice because they were focused on Dren. 

Elsa forms a motherly bond with Dren. After Dren attacks Clives brother Gavin (Brandon McGibbon), they move her to an isolated farm. There, Dren develops carnivorous tendencies and retractable wings. She grows into adolescence (Delphine Chanéac) and becomes bored with being locked up in the barn, but Elsa and Clive fear letting her outside where she might be discovered. Clive realizes that the human DNA used to make Dren was Elsas, not from an anonymous donor as Elsa had told him. When Dren assaults Elsa again, Elsa removes Drens stinger. She then uses the living tissue from the stinger to isolate and synthesize the protein for which they had been searching.

Dren seduces Clive, and Elsa discovers them having sex in the barn, and becomes upset. Clive accuses Elsa of never having wanted a child because she was afraid of losing control; instead she chose to raise one as an experiment, where control could be assured. Deciding the only solution is to terminate Dren, they return to the farm and find Dren already dying. 

William Barlow discovers human DNA in Drens protein samples and arrives to investigate. Elsa tells Barlow that Dren is dead. When he doubts her, Elsa tells him that Drens corpse is buried behind the barn. Dren, who has transformed into a winged male, rises from the grave and attacks the group. Dren kills Barlow and Gavin. Dren rapes Elsa as Clive searches for Gavin. Dren is attacked by Clive during the act and kills Clive just before he is killed by Elsa.

Elsa is later informed by Joan that Drens body contained numerous biochemical compounds for which the company has begun filing patents. Joan offers Elsa, now pregnant with Drens baby, a large sum of money to carry the pregnancy to term to provide further experimental value. Elsa accepts.

==Cast==
* Adrien Brody as Clive Nicoli
* Sarah Polley as Elsa Kast
* Delphine Chanéac as Dren
* Brandon McGibbon as Gavin Nicoli 
* Simona Maicanescu as Joan Chorot 
* David Hewlett as William Barlow
* Abigail Chu as Child Dren

==Production==
Splice was written by director Vincenzo Natali and screenwriters Antoinette Terry Bryant and Doug Taylor.    The script was originally meant to follow up Natalis Cube (film)|Cube (1997), but the budget and restricted technology hindered the project. In 2007, the project entered active development as a 75% Canadian and 25% French co-production, receiving a budget of $26 million.    The director described the film: "Splice is very much about our genetic future and the way science is catching up with much of the fiction out there.   is a serious film and an emotional one. And theres sex... Very, very unconventional sex. The centerpiece of the movie is a creature which goes through a dramatic evolutionary process. The goal is to create something shocking but also very subtle and completely believable." 

In October 2007, actors Brody and Polley were cast into the lead roles. Production began the following November in Toronto, Ontario.  It was aided by Telefilm Canadas funding of US$2.5 million.  Filming took place in Toronto and concluded in February 2008. 

In an interview, when asked if there would be any sequels, Natali responded, "I dont think so. It could happen, but it would have required the movie to make a lot of money in the States, but even though the ending of the film appears to be setting up a sequel, that was never my intention. All of my films end with a question, and somewhat ambiguously, and they always imply the beginning of another story, I like to leave the audience with something to ponder." 

==Release== The Losers A Nightmare on Elm Street. 

Splice was released on DVD and Blu-ray on October 5, 2010 in the USA and on November 29, 2010 in the UK. 

==Reception==
===Critical reception===
The film has received generally positive reviews from critics. Review aggregate Rotten Tomatoes reports that 75% of critics have given the film a positive review based on 178 reviews, with an average score of 6.6/10.    The critical consensus is: "It doesnt take its terrific premise quite as far as it should, but Splice is a smart, well-acted treat for horror fans." 
Review aggregate Metacritic awarded the film an average score of 65 out of 100 based on 34 reviews, indicating "generally favorable reviews." 

The Flick Cast said "Splice is funny, frightening, and shocking all at once. Its a disturbing commentary on where science is heading, and it is not easily shaken off once you leave the theatre." 

Richard Roeper panned Splice, calling it one of the worst movies of 2010. He gave the film a D+ calling it "ridiculous" but giving it credit for trying to be different. 

===Box office===
The film opened on June 4, 2010 in wide release to a $7.4 million opening weekend in 2,450 theaters, averaging $3,014 per theater. 

===Accolades=== Telefilm Golden Box Office Award, Canadian dollar|CAD$40,000, for being the highest-grossing Canadian feature film in English in 2010.   

The film was nominated for Best Science Fiction Film at 37th Saturn Awards, but lost to Inception, another film from Warner Bros..

==See also==
*2009 in film
*Parahuman

==References==
 

==External links==
 
* 
* 
* 
* 
* 
* 

 
 

 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 