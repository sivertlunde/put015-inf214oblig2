I Aim at the Stars
{{Infobox film
| name           = I Aim at the Stars
| image          = I Aim at the Stars poster.jpg
| image_size     =
| caption        = US Style B poster
| director       = J. Lee Thompson
| producer       = Charles H. Schneer
| writer         = Jay Dratler (screenplay) George Froeschel (story) H. W. John (story) Udo Wolter (story)
| narrator       = Victoria Shaw Herbert Lom Gia Scala
| music          = Laurie Johnson
| cinematography = Wilkie Cooper
| editing        = Frederick Wilson
| studio         = Morningside Productions Fama-Film
| distributor    = Columbia Pictures
| released       =  
| runtime        = 107 min.
| country        = United States English
| budget         =
}}

I Aim at the Stars is a 1960 biographical film which tells the story of the life of Wernher von Braun. The film covers his life from his early days in Germany, through Peenemünde, up until his work with the United States Army|U.S. Army, NASA, and the American space program.  
 Victoria Shaw, James Daly. 

The movie was written by Jay Dratler based on a story by George Froeschel, H. W. John, and Udo Wolter.  It was directed by J. Lee Thompson. 

The film was premiered in Munich on 19 August 1960; it subsequently opened in New York City and Los Angeles on 19 October and London on 24 November.  In Germany the film was titled Ich greife nach den Sternen ("I Reach for the Stars"). In Italy the film was released as Alla Conquista dell Infinito.

Satirist Mort Sahl and others are often credited with suggesting the subtitle "(But Sometimes I Hit London)",  but in fact the line appears in the film itself, spoken by actor James Daly, who plays the cynical American press officer.
 Dell published a comic book adaptation of the film with art by Jack Sparling as Four Color #1148 (Oct. 1960). 

==References==
{{reflist|refs=
   
   
    
   
   
   
}}

==External links==
* 
*  

 

 
 
 
 
 
 
 


 
 