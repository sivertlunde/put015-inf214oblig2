The Fortune
 Fortune}}
{{Infobox film
| name = The Fortune
| image = Fortune_movie_poster.jpg
| image_size =
| caption = Theatrical release poster
| director = Mike Nichols
| producer = Don Devlin Mike Nichols Adrien Joyce
| starring = Warren Beatty Jack Nicholson Stockard Channing Florence Stanley Richard B. Shull Tom Newman John Fiedler Scatman Crothers
| cinematography= John A. Alonzo
| music = José Padilla Sánchez David Shire
| editing = Stu Linder
| distributor = Columbia Pictures
| released =   
| runtime = 88 minutes
| country = United States  
| language = English
}}
 Adrien Joyce con men who plot to steal the fortune of a wealthy young heiress, played by Stockard Channing in her first film starring role.

==Plot==
Nicky Wilson (Beatty) and Oscar Sullivan (Nicholson) are inept 1920s scam artists who see pay dirt in the guise of Fredericka Quintessa Bigard (Stockard Channing), the millionaire heiress to a sanitary napkin fortune. She loves the already married Nicky, but because the Mann Act prohibits him from taking her across state lines and engaging in immoral relations, he proposes that she marry Oscar and then carry on an affair with the man she wants. Oscar, who is wanted for embezzlement and anxious to get out of town, is happy to comply with the plan, although he intends to claim his spousal privileges after they are wed. 

Once they reach Los Angeles, the men try everything they can to separate Freddie from her inheritance without success, but with sufficient determination to arouse her suspicions. When she announces her plan to donate her money to charity, Nicky and Oscar conclude that murder might be their only recourse if theyre going to get rich quick.

==Production==
When Warren Beatty was unable to stir interest in his and Robert Townes screenplay for Shampoo (film)|Shampoo, about an amoral hairdresser he had been developing since 1967, he bundled it with the more appealing The Fortune and convinced Columbia Pictures head David Begelman to finance both films. The fact that Carole Eastman, writing under the pen name Adrien Joyce, had yet to complete her 240-page script fazed Beatty less than it did director Mike Nichols, who needed a box office hit after Catch-22 (film)|Catch-22 and The Day of the Dolphin, both of which were critical and commercial flops.
 satirical and more Slapstick comedy|slapstick, and she was eventually fired from the production.   

Nichols wanted Bette Midler to portray Freddie, but he changed his mind when, seemingly unaware of his career, Midler insulted him by asking what films he had previously made. He ultimately cast relative newcomer Stockard Channing, whose credits were limited to a few television appearances and a minor role in the Barbra Streisand film Up the Sandbox. 
 One Flew Over the Cuckoos Nest was delayed, Jack Nicholson, who had worked with Nichols on Carnal Knowledge, was available for the role of Oscar Sullivan. During filming, the actor was forced to deal with two events that impacted his personal life. First, a fact checker working on a biographical piece for Time (magazine)|Time discovered that the woman Nicholson believed was his sister was actually his mother, and the woman who raised him was his grandmother. Then his close friend Cass Elliot died in her sleep, and rumors about the cause of her death circulated in the media. These two events, linked with the films eventual failure, made The Fortune a subject that Nicholson never discussed in interviews and biographies. 

The film was shot on location in Albuquerque, New Mexico, and on a segment of street constructed in the corner of the former RKO Forty Acres backlot where the "Stalag 13" sets for TVs Hogans Heroes were located during the Desilu days.  Nichols did not direct another film for seven years. 

==Cast==
*Stockard Channing  as Fredrika Quintessa Bigard
*Jack Nicholson  as Oscar Sullivan
*Warren Beatty  as Nicky Wilson
*Florence Stanley  as Mrs. Gould
*Richard B. Shull  as Detective Sgt. Power
*Tom Newman as John the Barber
*John Fiedler as Police Photographer
*Scatman Crothers as Fisherman Brian Avery as Airline Steward
*Kathryn Grody as Police Secretary
*Ian Wolfe  as Justice of the Peace
*Dub Taylor as Rattlesnake Tom
*Joe Tornatore as Detective
*Jim Antonia as 1st Policeman
*Vic Vallard as 2nd Policeman

==Critical reception== situation and/or wise-crack comedy, all of which Mr. Nichols already can do with – perhaps – too great an ease. The Fortune will probably be compared to The Sting, because of the overlapping of the eras and the con-man theme. Incorrectly, though. The Sting is an adventure. The Fortune is farce of a rare order." 

Time Out London said it "starts promisingly as a sardonic comedy . . . but once in California lethargy settles in. The film becomes almost static, a series of stagy, glossy tableaux: such lack of momentum may be an adequate assessment of the characters limited capacity for development, but it has a disastrous effect on the films pacing. Events degenerate into miscalculated farce and underline Nichols continuing slick superficiality. Adrien Joyces much hacked-about script sounds as though it was once excellent: a pity everyone treats it so off-handedly." 

Channel 4 called it a "flat-footed attempt to revive the 1930s screwball comedy" but liked the leads, commenting, "The trios timing and delivery almost rescue the movie from degenerating into bad farce." 

TV Guide rated it four stars, calling it "an offbeat but often hilarious comedy" and adding, the film "works well through the fine performances of the leads and the superb timing of director Nichols." It concluded, "Full of period and period-sounding music, The Fortune is cold to the core – agreeably disagreeable amusement." 

==Awards and nominations==
Stockard Channing was nominated for the Golden Globe Award for New Star of the Year – Actress but lost to Marilyn Hassett in The Other Side of the Mountain.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 