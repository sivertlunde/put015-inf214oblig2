The Lovemakers (film)
{{Infobox film
| name           = The Lovemakers
| image          = Laviaccia.jpg
| image size     =
| caption        = Poster
| director       = Mauro Bolognini
| producer       = Alfredo Bini
| writer         = Mario Pratesi Vasco Pratolini
| narrator       =
| starring       = Claudia Cardinale Jean-Paul Belmondo
| music          = Piero Piccioni
| cinematography = Leonida Barboni
| editing        = Nino Baragli
| distributor    = Titanus Arco Film  Galatea Film
| released       = 13 June 1961 (France)
| runtime        =
| country        = Italy
| language       = Italian language
| budget         =
| gross          =698,077 admissions (France) 
| preceded by    =
| followed by    =
}} 1961 cinema Italian drama film directed by Mauro Bolognini based on a novel by Mario Pratesi. The film stars Claudia Cardinale and Jean-Paul Belmondo. It was entered into the 1961 Cannes Film Festival.   

==Plot==
The original Italian is La Viaccia (the name of the family farm which motivates the plot). The death of a wealthy patriarch in 1885 sets off an interfamily power struggle. Son Ferdinando buys out his other relatives in order to gain full control over the dead mans property. But Ferdinandos country-bumpkin nephew Amerigo holds out. Amerigos stance is weakened when he heads for the city and meets prostitute Bianca. To support her in the manner in which she is accustomed, Amerigo steals from his uncle. Disgraced in the eyes of his family, Amerigo decides to stay near his beloved Bianca by becoming a bouncer in her brothel.

==Cast==
*Jean-Paul Belmondo as  Amerigo
*Claudia Cardinale  as  Bianca
*Pietro Germi  as  Stefano
*Gabriella Pallotta  as Carmelinda
*Romolo Valli  as  Dante
*Paul Frankeur as Ferdinando
*Gina Sammarco  as  Maîtresse
*Marcella Valeri as  Beppa
*Emma Baron  as  Giovanna
*Franco Balducci  as  Tognaccio
*Claudio Biava  as   Arlecchino
*Nando Angelini as   Young man
*Duilio DAmore  as   Bernardo
*Giuseppe Tosi  as  Casamonti
*Paola Pitagora as   Anna
* Gianna Giachetti as  Boarder at brothel
*Rosita di Vera Cruz  as  Margherita
*Dante Posani  as  Gustavo
*Olimpia Cavalli as  Boarder
*Aurelio Nardi  as  Ball-man
*Maurice Poli
*Renzo Palmer
*Rina Morelli

==References==
 

==External links==
* 
*  
 

 
 
 
 
 
 
 
 

 
 