I Smile Back
{{Infobox film
| name = I Smile Back
| image =
| caption =
| director = Adam Salky
| producer = 
| writer = Paige Dylan Amy Koppelman
| based on =  
| starring = Sarah Silverman Josh Charles
| music = Zack Ryan
| cinematography = Eric Lin
| editing = Tamara Meem
| distributor = Broad Green Pictures
| released = 2015
| runtime = 85 minutes
| country = United States
| language = English
| budget = 
| gross =
}}
I Smile Back is a 2015 American drama film directed by Adam Salky and based on the 2008 novel of the same name by Amy Koppelman, who wrote the screenplay with Paige Dylan. The film stars Sarah Silverman as drug-addicted housewife Laney Brooks. The film premiered at the 2015 Sundance Film Festival in its U.S. Dramatic Competition.

==Reception==
Indiewire reviewer Katie Walsh gave strong praise to Silvermans performance in the film. 

==Cast==
*Sarah Silverman as Laney
*Josh Charles as Bruce
*Thomas Sadoski as Donny
*Mia Barron as Susan
*Skylar Gaertner as Eli
*Shayne Coleman as Janey

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 

 