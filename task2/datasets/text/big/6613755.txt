Bloodfist II
{{Infobox Film
| name           = Bloodfist II
| image          = Bloodfist2.jpg
| image size     = 190px
| caption        = DVD Cover Art
| director       = Andy Blumenthal New Horizons Concord Production Inc. (Roger Corman Catherine Cyran Cirio H. Santiago)
| writer           = Catherine Cyran Maurice Smith Richard Hill Steve Rogers Kris Aguilar
| music          = Nigel Holton
| cinematography = Bruce Dorfman
| editing        = Karen D. Joseph 
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| box office gross      =  $1,292,323
}}

Bloodfist II is a 1990 action/adventure film starring Don "The Dragon" Wilson, Kris Aguilar and Ronald Asinas. It was directed by Andy Blumenthal and written by Catherine Cyran.

==Plot==
The film opens with Jake Raye as he fights Mickey Sheehan in a pro-kickboxing bout. The movie opens as they enter the fifth Round of the Lightweight Championship Match. Jake delivers Mickey a lightning fast kick to the throat in the middle of the sixth round, instantly killing him. Seeing what he had done, he decides to give up kickboxing once and for all.

A year later, a friend and manager Vinny Petrello (Kickboxing and UFC champion Maurice Smith) asks him for a favor to travel to Manila and bail him out of trouble with a guy named Su. Although Jakes evening with a prostitute (Liza David) is interrupted, he agrees to help his friend in need.  Jake Raye travels to Manila, and meets up with local fighters John Jones (James Warring), Sal Taylor (Timothy D. Baker), Manny Rivera (Manny Samson), and Tobo Casenerra (Monsour Del Rosario). He also meets up with Dieter (Robert Marius), the head of the Dojo. Thugs attack Jake, and is helped by a woman named Mariella (Rina Reyes) into an abandoned safehouse. Mariella betrays him, and the thugs enter the safehouse. Dieter drugs Raye, and puts him on the ship with the other fighters. Raye is re-acquained with his friend Bobby Rose (Rick Hill) and meets another fighter named Ernesto (Steve Rodgers). It is revealed that Su (Joe Mari Avellana) is the one who bring the fighters to his island home called Paradise, and it is also revealed that Vinny is helping Su get the fighters there to battle in gladiator matches.

The fighters briefly rebel giving Jake Raye time to escape. Soon Raye has a change of heart and decides to free the other fighters. He makes it back to the house undetected by Su, and is helped once more by Mariella. Mariella and Raye uncover a plot for Su to give anabolic steroids to each of his fighters before the match.

Jake Raye takes out some guards before he is discovered by Dieter and knocked unconscious by Vinny, pretending to be in trouble. Jake is taken to the challenger’s box of the arena, where Su, Vinny, and his guests are awaiting the matching. Both John and Ernest die in the arena while battling their opponents while Manny is killed trying to escape. (Ernest does win his fight, but Su orders Vinny to kill him either due to his unorthodox fighting i.e. low blows and eye gouging however it should be noted the fights were not fair to begin  With and they were fighting for survival or his embarrassment of sus fighter) the help of Mariella, the remaining surviving fighters (Bobby, Sal, Tobo, and Jake) Jake fights and kills Vinny while the others defeat the guards and the elite fighters. Bobby shoots Dieter while escaping, and the film ends after Jake defeats Su with a swift kick off the balcony. The five people begin to walk off Paradise forever.

==Cast==
*Don "The Dragon" Wilson as Jake Raye
*Rina Reyes as Mariella
*Joe Mari Avellana as Su
*Robert Marius as Dieter Maurice Smith as Vinny Petrello
*Timothy D. Baker as Sal Taylor
*James Warring as John Jones 

==Release==
Bloodfist II received a limited release theatrically from Concorde Films in October 1990.  It ended up grossing $1,292,323 at the box office.  

 , and  .  The DVD is currently out-of-print.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 