The Trojan Women (film)
{{Infobox film
| name           = The Trojan Women
| image          = Trojanhepburn.jpg
| caption        = Promotional poster
| director       = Michael Cacoyannis
| producer       = Michael Cacoyannis Anis Nohra Josef Shaftel
| writer         = Euripides Edith Hamilton Mihalis Kakogiannis
| starring       = Katharine Hepburn Vanessa Redgrave Geneviève Bujold Irene Papas Brian Blessed
| music          = Mikis Theodorakis
| cinematography = Alfio Contini
| editing        = Michael Cacoyannis
| studio         = 
| distributor    = 
| released       = September 27, 1971
| runtime        = 105 min
| country        = United States United Kingdom Greece
| language       = English
| budget         = 
| gross          = 
}}

The Trojan Women ( ) is a 1971 film, directed by Michael Cacoyannis and starring Katharine Hepburn and Vanessa Redgrave. The film was made with the minimum of changes to Edith Hamiltons translation of Euripides original The Trojan Women|play, save for the omission of Greek_gods#Immortals|deities, as Cacoyannis said they were "hard to film and make realistic."

==Synopsis==
The Trojan Women was one of a trilogy of plays dealing with the suffering created by the Trojan Wars. Hecuba (Katharine Hepburn), Queen of the Trojans  and mother of Hector, one of Troys most fearsome warriors, looks upon the remains of her kingdom;  Andromache (Vanessa Redgrave), widow of the slain Hector and mother of his son Astyanax, must raise her son in the wars aftermath; Cassandra (Geneviève Bujold),  Hecubas daughter who has been driven insane by the ravages of war, waits to see if King Agamemnon will drive her into concubinage;  Helen of Troy (Irene Papas), waits to see if she will live. But the most awful truth is unknown to them until Talthybius (Brian Blessed), the messenger of the Greek king, comes to the ruined city and tells them that King Agamemnon and his brother Menelaus have decreed that Hectors son Astyanax must die — the last of the male royalty of Troy must be executed to ensure  the extinction of the line.

==Cast==
*Katharine Hepburn as Hecuba, Queen of the Trojans
*Vanessa Redgrave as Andromache, widow of Hector
*Geneviève Bujold as Cassandra, Hecubas daughter
*Irene Papas as Helen of Troy
*Brian Blessed as Talthybius Patrick Magee as Menelaus, King of Sparta

==Production==
When filming began in the Spanish village of Atienza, 80 miles north-east of Madrid, sections of the press were speculating that there might be fireworks between the lead actresses. Hepburn had recently gone on record deploring the  moral squalor and carelessness of the modern generation, and the impulsive and radical Redgrave was thought by some of the press to be a symbol to that sloppy generation. In fact the actresses got on well, talking about painting, politics, and acting —Hepburn expressed enthusiasm for Redgraves 1966 Rosalind in As You Like It— and both actresses began to learn Spanish. Photoplay Film Monthly February 1971 

Cacoyannis first staged The Trojan Women in Italy in 1963, with Rod Steiger, Claire Bloom, and Mildred Dunnock in the leading roles. Later in the same year he took the production to New York and in 1965, to Paris. "For me", he said in a 1971 magazine interview, "the play is particularly pertinent and real. What the play is saying is as important today as it was when it was written. I feel very strongly about war, militarism, killing people ... and I havent found a better writer who makes that point more clearly than Euripides. The play is about the folly of war, the folly of people killing others and forgetting that they are going to die themselves." 
 Covent Garden. Cacoyannis hand-picked Italys Franco Freda and Adalgisa Favella as make-up artist and hair stylist respectively for the film. Both were veterans of the films of Federico Fellini, Michelangelo Antonioni and Luchino Visconti.

Hepburn said of her acting for this part: "My acting has always been a little flamboyant and rococo. But for this part, Ive had to pare right down to the bare essentials." Her acting voice dropped, after special training, by an octave and was almost accentless, the familiar twanging  pitch and  East Coast rhythms almost vanished. 

==Awards==
Kansas City Film Critics Circle
*Kansas City Film Critics Circle Award for Best Actress - Katharine Hepburn (won)

National Board of Review of Motion Pictures
*NBR Award for Best Actress - Irene Papas (won)

==See also==
* List of historical drama films
* Greek mythology in popular culture
* List of films based on military books (pre-1775)

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 