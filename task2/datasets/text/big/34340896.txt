Rip Van Winkle (1903 film)
{{Infobox film
| name           = Rip Van Winkle
| image          = Joseph Jefferson as Ripvanwinkle by Napoleon SArony (1821-1896).jpg
| image_size     =
| caption        = Joseph Jefferson as Rip Van Winkle by Napoleon Sarony
| director       = William K.L. Dickson
| producer       =
| writer         = William K.L. Dickson Joseph Jefferson Dion Boucicault Washington Irving
| narrator       =
| starring       = Joseph Jefferson
| music          =
| cinematography = G.W. Bitzer
| editing        =
| studio         = American Mutoscope and Biograph Co.
| released       =  
| runtime        =
| country        = United States Silent
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
  1903 United American Short short black-and-white silent compilation story of the same name by Washington Irving, featuring Joseph Jefferson as a neer-do-well, who wanders off one day into the Kaatskill mountains where he runs into an odd group of men, drinks some of their mysterious brew and passes out only to wake up to find 20 years have passed. The film is compiled from a series of films produced in 1896, which consisted of;

*Rips Toast (AM&B Cat. #45)
*Rip Meets the Dwarf (AM&B Cat. #46)
*Rip and the Dwarf
*Rip Leaving Sleepy Hollow (AM&B Cat. #52)
*Rips Toast to Hudson and Crew
*Rips Twenty Years Sleep (AM&B Cat. #50)
*Awakening of Rip
*Rip Passing Over Hill

These films were added to the National Film Registry by the Library of Congress in 1995 and featured on the DVD release More Treasures from American Film Archives, 1894-1931.

==Production==
The serial&mdash;filmed at   registered the copyright on February 4, 1897. The actor selected scenes that were largely pantomimed, eliminating the need for explanatory titles.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 