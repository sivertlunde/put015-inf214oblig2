The Book of Life (2014 film)
 
{{Infobox film
| name           = The Book of Life
| image          = Book Of Life Film Logo.jpg
| alt            = 
| caption        = Theatrical release poster Jorge Gutierrez
| producer       = Aaron Berger Brad Booker Guillermo del Toro Carina Schulze 
| writer         = Jorge Gutierrez Doug Langdale
| starring       = Diego Luna Zoe Saldana Channing Tatum Ron Perlman Christina Applegate Ice Cube Kate del Castillo
| music          = Gustavo Santaolalla
| editing        = Ahren Shaw   
| studio         = Reel FX Creative Studios 20th Century Fox Animation
| distributor    = 20th Century Fox
| released       =  
| runtime        = 95 minutes  
| country        = United States
| language       = English Spanish
| budget         = $50 million   
| gross          = $97.4 million 
}} 3D Computer adventure Musical musical comedy Jorge Gutierrez, Best Animated Feature Film.

==Plot== Mexican town of San Angel from the Book of Life, which holds every story in the world.
 La Muerte, ruler of the Land of the Remembered, and Xibalba, ruler of the Land of the Forgotten, set a wager at San Angels Day of the Dead festival after seeing two boys, Manolo and Joaquín, competing over a girl named María. La Muerte bets that Manolo will marry María, while Xibalba bets on Joaquín. If La Muerte wins, Xibalba can no longer interfere in mortal affairs, but if Xibalba wins, he and La Muerte switch realms. However, Xibalba cheats by giving Joaquin his Medal of Everlasting Life, which grants the wearer invincibility. María frees a herd of pigs from being slaughtered, angering her father, who sends her away to a boarding school in Spain. While saying their goodbyes, Manolo gives her a baby pig from earlier and she gives him a guitar which is engraved with "Always play from the heart". 
 bullfighter like the rest of their family, while Joaquín becomes the revered town hero with the Medals aid. On the day of Marías return, a celebration is held culminating in Manolos first bullfight. Manolo defeats the bull but refuses to kill it, disappointing Carlos and the crowd but impressing María. That night, María is pressured by her father to marry Joaquín so that he will stay and protect San Angel from the bandit Chakal, though she is conflicted by her feelings for Manolo. María and Manolo meet that night to profess their love for each other, but they are interrupted when a snake, sent by Xibalba, bites María once and seemingly kills her. Despondent and blamed for her death, Manolo is conned by Xibalba into being sent to the afterlife: the snake bites him twice, killing him.

Manolo arrives in the Land of the Remembered where he reunites with his mother Carmen and his illustrious deceased family members. They travel to La Muertes castle to seek María, but only find Xibalba, who explains the bet to an outraged Manolo and that the snake put María in a coma, but killed him. When María awakens, she learns of Manolos death and solemnly accepts Joaquíns proposal. Manolo, Carmen, and Luis, Manolos grandfather, travel to the Cave of Souls to reach La Muerte. They meet the amiable Candle Maker, who oversees the lives of everybody in the living world. After seeing that Manolos story in the Book of Life is blank (due to Xibalbas meddling) and can be rewritten by Manolos own actions, the Candle Maker takes them  to the Land of the Forgotten. Manolo exposes the cheating to La Muerte, who furiously summons Xibalba. Another deal is negotiated; Manolos life will be returned if he completes a challenge Xibalba sets him, but if he fails, Xibalba will rule both lands and Manolo will be forgotten. Xibalba manifests every bull the Sanchez family ever fought, which combine to become one giant one for Manolo to defeat.

In the living world, Chakal, who previously owned the Medal, leads his army to San Angel to find it. Chakal kills Carlos, who joins the deceased to watch Manolo fight. Manolo again refuses to deliver the finishing blow, instead singing an apology to the grudge-filled spirit asking it to forgive his familys transgressions, which it does by peacefully dissolving away. Impressed, the deities grant Manolo his life back and send him and his familys spirits to the living world to defeat Chakal. Manolo is almost killed again when Chakal blows them both up but is saved by the Medal, which Joaquín gives him at the last second. Joaquín returns it to Xibalba and resolves to be a hero of his own accord, while Manolo and María wed happily as La Muerte and Xibalba reconcile.

The story ends, and as the children leave the museum, Mary Beth and a security guard reveal themselves as La Muerte and Xibalba.

==Cast==
* Diego Luna as Manolo Sánchez,  a torero with a guitar and two swords.
** Emil-Bastien Bouffard as Young Manolo
** Joe Matthews as Young Manolo (Singing)
* Zoe Saldana as María Posada, Manolo and Joaquíns best friend and love interest. She is also General Ramiro Posadas daughter. 
** Genesis Ochoa as Young María
* Channing Tatum as Joaquín Mondragon, a man who is the town hero of San Angel. 
** Elias Garza as Young Joaquín
* Christina Applegate as Mary Beth,  a museum tour guide. 
* Ice Cube as The Candle Maker, a being who oversees the lives of the living.    
* Ron Perlman as Xibalba, the ruler of the Land of the Forgotten and La Muertes husband. 
* Kate del Castillo as La Muerte, the ruler of the Land of the Remembered and Xibalbas wife. 
* Héctor Elizondo as Carlos Sánchez, Manolos father. 
* Danny Trejo as Skeleton Luis Sánchez, Manolos deceased grandfather. 
* Carlos Alazraqui as General Ramiro Posada, Dali, Chuy
* Ana de la Reguera as Carmen Sánchez, Manolos deceased mother. 
* Plácido Domingo as Skeleton Jorge Sánchez, Manolos deceased great-grandfather.  Jorge R. Gutierrez as Skeleton Carmelo Sánchez
* Eugenio Derbez as Chato 
* Gabriel Iglesias as Pepe Rodríguez 
* Anjelah Johnson as Adelita / Nina 
* Dan Navarro as Chakal, the large leader of a group of bandits. 
* Miguel Sandoval as Land of the Remembered Captain
* Grey DeLisle as Grandma Sanchez, Manolos  great grandmother who later dies due to cholesterol problems.
* Ricardo Sánchez as Pablo Rodriguez 
* Cheech Marin as Pancho Rodríguez   
* Eric Bauza as Father Domingo, Cave Guardian
* Aron Warner as Thomas Troy Evans as Old Man Hemingway
* Guillermo del Toro as Land of the Remembered Captains Wife
* Brad Booker as Conductor

==Production==
  at a panel for the film at San Diego Comic-Con International in July 2014]]
The Book of Life was originally optioned by DreamWorks Animation in 2007, but never went beyond development because of "creative differences". 

On February 21, 2012, Reel FX announced Guillermo del Toro would produce the film, originally titled Day of the Dead.  On December 12, 2012, it was announced that the film would be released on October 10, 2014, and that 20th Century Fox would distribute the film.  On October 15, 2013, it was announced that the film would be pushed back seven days to October 17, 2014.  On October 16, 2013, it was announced that Channing Tatum, Zoe Saldana, Diego Luna and Christina Applegate would star as voice actors in the film. 
 Jorge Gutierrez explained that he wanted the film to look exactly like the concept art in "The Art of" books, saying: "I saw every single one that comes out and my biggest heartbreak is that I see all this glorious art, and then the movie doesnt look like that! The mandate of this movie was: Our Art of book is going to look exactly like the movie. And every artist poured their heart and soul into that idea." 

==Release==
The film premiered in Los Angeles on October 12, 2014  and was released in the United States on October 17, 2014. 

===Home media===
The Book of Life was released on DVD, Blu-ray and Blu-ray 3D on January 27, 2015. 

==Music==
{{Infobox album
| Name       = The Book of Life
| Type       = Soundtrack
| Artist     = Gustavo Santaolalla
| Cover      =
| Released   = September 26, 2014
| Recorded   = 2014
| Genre      = Film score
| Length     = 37:34
| Label      = Sony Masterworks
| Producer   =
| Chronology = Gustavo Santaolalla film scores Wild Tales (2014)
| This album = The Book of Life (2014)
| Next album = 
}}
 Paul Williams would be writing songs for the film.  The soundtrack was released on September 26, 2014, on iTunes,  and was released on CD on October 27, 2014, by Sony Masterworks. 

{{tracklist
| extra_column = Performer
| total_length = 37:34
| title1 = Live Life
| extra1 = Jesse & Joy
| length1 = 3:05

| title2 = The Apology Song
| extra2 = La Santa Cecilia
| length2 = 2:32

| title3 = No Matter Where You Are
| extra3 = Us The Duo
| length3 = 2:58

| title4 = I Love You Too Much 
| extra4 = Diego Luna & Gustavo Santaolalla
| length4 = 2:35

| title5 = I Will Wait
| extra5 = Diego Luna, Joe Matthews & Gustavo Santaolalla
| length5 = 1:55

| title6 = Más Kinky
| length6 = 4:20

| title7 = Cielito Lindo
| extra7 = Plácido Domingo
| length7 = 0:25
 Creep
| extra8 = Diego Luna & Gustavo Santaolalla
| length8 = 1:20

| title9 = Cant Help Falling in Love
| extra9 = Diego Luna
| length9 = 0:52

| title10 = The Ecstasy of Gold
| extra10 = Gustavo Santaolalla
| length10 = 2:05

| title11 = Da Ya Think Im Sexy?
| extra11 = Gabriel Iglesias & Gustavo Santaolalla
| length11 = 0:20

| title12 = Just a Friend
| extra12 = Biz Markie & Cheech Marin
| length12 = 2:49

| title13 = El Aparato / Land of the Remembering
| extra13 = Café Tacuba & Gustavo Santaolalla
| length13 = 1:46

| title14 = Visiting Mother
| extra14 = Gustavo Santaolalla
| length14 = 1:43

| title15 = The Apology Song
| extra15 = Diego Luna & Gustavo Santaolalla
| length15 = 2:52

| title16 = No Matter Where You Are
| extra16 = Diego Luna & Zoe Saldana
| length16 = 1:37

| title17 = Te Amo y Más 
| extra17 = Diego Luna & Gustavo Santaolalla
| length17 = 2:36

| title18 = Si Puedes Perdonar
| extra18 = Diego Luna & Gustavo Santaolalla
| length18 = 1:44
}}

==Reception==

===Box office===
;North America Gone Girl ($17.8 million).       The film played 57% female and 54% under 25-years old. It played 59% under 10-years old while 31% of tickets sold were in 3D. 

;Outside North America
In other territories, The Book of Life earned $8.58 million from 3,654 screens in 19 markets. The highest debuts came from Mexico ($3.84 million) and Brazil ($1.98 million).  In Mexico, the film was number two behind the local film Perfect Dictatorship. 

===Critical reception===
The Book of Life received positive reviews. On Rotten Tomatoes, the film has a "Certified Fresh" rating of 82%, based on 101 reviews, with an average rating of 7/10. The sites consensus reads "The Book of Life s gorgeous animation is a treat, but its a pity that its story lacks the same level of craft and detail that its thrilling visuals provide."  On Metacritic, the film has a score of 67 out of 100, based on 27 critics, indicating "generally favorable reviews". 

Geoff Berkshire of Variety (magazine)|Variety gave the film a positive review, saying "Repping a major step forward for Dallas-based Reel FX Animation Studios (after their anemic feature bow on last year’s Free Birds), the beautifully rendered CG animation brings an unusually warm and heartfelt quality to the high-tech medium and emerges as the film’s true calling card."    Frank Scheck of The Hollywood Reporter gave the film a positive review, saying "The Book of Life is a visually stunning effort that makes up for its formulaic storyline with an enchanting atmosphere that sweeps you into its fantastical world, or in this case, three worlds."  Simon Abrams of The Village Voice gave the film a negative review, saying "The Book of Lifes hackneyed stock plot preaches tolerance while lamely reinforcing the status quo."  Marc Snetiker of Entertainment Weekly gave the film an A-, saying "Overflowing with hyperactive charm and a spectacular sea of colors, it showcases some of the most breathtaking animation weve seen this decade."  Claudia Puig of USA Today gave the film two and a half stars out of four, saying "The dizzying, intricate imagery is so beautiful, and the Latin-inspired songs catchy enough that the overall effect is often enchanting."  Sara Stewart of The New York Post gave the film two out of four stars, saying "Just in time for Mexico’s Day of the Dead holiday comes this gloriously colorful animated musical, which almost (but not quite) makes up in visuals what it lacks in snappy dialogue."  Katie Rife of The A.V. Club gave the film a B-, saying "Ultimately, what drags The Book Of Life down is its insistence on trying to update an (original) folkloric story for a contemporary audience. In practice, this means adding some pop-cultural touches that only serve to take the viewer out of the fantastic setting." 

Michael Ordoña of the   gave the film three out of four stars, saying "Funny without being frantic, seamlessly switching from dry humor to slapstick, it shows death as a part of life -- and, judging from a preview audience of very young tykes, does so in a gentle, delightful way."  Manohla Dargis of The New York Times gave the film a negative review, saying "This often beautiful and too-often moribund, if exhaustingly frenetic, feature tends to be less energetic than the dead people waltzing through it."  Charles Solomon of the Los Angeles Times gave the film a negative review, saying "The Book of Life juxtaposes overwrought visual imagery with an undernourished, familiar story - regrettable flaws in one of the few animated films to focus on Latino characters and the rich heritage of Mexican folk culture."  Marjorie Baumgarten of The Austin Chronicle gave the film two and a half stars out of five, saying "Visually arresting but dramatically rote, The Book of Life at least introduces American kids to the Mexican holiday of Día de los Muertos and should score points with families looking for kid-friendly movies that reflect aspects of their Mexican cultural heritage." 

Calvin Wilson of the St. Louis Post-Dispatch gave the film two and a half stars out of five, saying "The Book of Life is a flawed but intriguing new chapter in animation."  James Berardinelli of ReelViews gave the film three out of four stars, saying "The Book of Life moves breezily from one scene to the next, keeping the pace brisk and rarely skipping a beat."  Laura Emerick of the Chicago Sun-Times gave the film three out of four stars, saying "Whether en ingles o en espanol, The Book of Life is a delight. In an animated universe cluttered with kung-fu pandas, ice princesses and video-game heroes, Gutierrez and del Toro have conjured up an original vision."  Tasha Robinson of The Dissolve gave the film three and a half stars out of five, saying "It’s all flawed, and distracted, and conceptually messy, prioritizing color over common sense and energy over consistency. But as an afternoon’s diversion for a handful of misbehaving kids—both within the movie, and within the movie theater—it’s authentically winning."  Michael Ordona of the San Francisco Chronicle gave the film a positive review, saying "There are no great surprises, no shocking reveals (except to the characters themselves). But there’s so much to appreciate along the way that it’s a real page-turner."  Kenji Fujishima of Slant Magazine gave the film two out of four stars, saying "Jorge R. Gutierrez subsumes the films darker themes in a relentlessly busy farrago of predictable kids-movie tropes and annoying attempts at hipness."  Ben Sachs of the Chicago Reader gave the film a negative review, saying "This Pixar knockoff from 20th Century Fox is more imaginative than most, though like far too many of them, its undone by a surfeit of glib one-liners and pop culture references." 

===Accolades===
{| class="wikitable plainrowheaders"
|+  List of Awards and Nominations 
|-
! scope="col" style="width:4%;"| Year
! scope="col" style="width:25%;"| Award
! scope="col" style="width:33%;"| Category
! scope="col" style="width:33%;"| Recipients and nominees
! scope="col" style="width:5%;"| Results
|-
! scope="row" rowspan="5" style="text-align:center;"| 2014 42nd Annual Annie Awards  Annie Award Best Animated Feature
|The Book of Life
| 
|- Animated Effects in an Animated Production Augusto Schillaci, Erich Turner, Bill Konersman, Chris Rasch, Joseph Burnette
| 
|- Character Design in an Animated Feature Production Paul Sullivan, Sandra Equihua, Jorge R. Gutierrez
| 
|- Annie Award Directing in an Animated Feature Production Jorge R. Gutierrez
| 
|- Production Design in an Animated Feature Production Simon Varela & Paul Sullivan
| 
|-
! scope="row" style="text-align:center;" rowspan=2 | 2014
| scope="row" rowspan="1" style="text-align:center;"| 72nd Golden Globe Awards Golden Globe Best Animated Feature Film
|The Book of Life
| 
|-
| scope="row" rowspan="1" style="text-align:center;"| Producers Guild of America
| Best Outstanding Producer of Animated Theatrical Motion Pictures
| Guillermo del Toro and Brad Booker
|  
|-
|}

==Sequels==
Director Jorge Gutierrez revealed in an interview that one of the ideas for the next chapter in the story involves Joaquin and his relationship with his father. "I had always imagined the first movie to be about Manolo, the second to be about Joaquín and the third one to be about Maria...Ive always conceived it as a trilogy." 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 