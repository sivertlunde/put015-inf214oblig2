Old Wives for New
 
{{Infobox film
| name           = Old Wives for New
| image          = Old Wives for New (1918) 1.jpg
| caption        = Gustav von Seyffertitz and Sylvia Ashton in Old Wives for New
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille Jesse L. Lasky
| writer         = Jeanie MacPherson David Graham Phillips
| starring       = Elliott Dexter
| cinematography = Alvin Wyckoff
| editing        = Cecil B. DeMille
| studio         = Famous Players-Lasky / Artcraft
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States  Silent (English intertitles)
| budget         = 
}}
 
Old Wives for New is a 1918 American drama film directed by Cecil B. DeMille. Prints of the film survive at the International Museum of Photography and Film at George Eastman House.   

==Cast==
* Elliott Dexter - Charles Murdock
* Florence Vidor - Juliet Raeburn
* Sylvia Ashton - Sophy Murdock
* Wanda Hawley - Sophy in Prologue
* Theodore Roberts - Tom Berkeley
* Helen Jerome Eddy - Norma Murdock
* Marcia Manon - Viola Hastings
* Julia Faye - Jessie
* J. Parks Jones - Charley Murdock
* Edna Mae Cooper - Bertha
* Gustav von Seyffertitz - Melville Bladen
* Tully Marshall - Simcox
* Lillian Leighton - Maid
* Mayme Kelso - Housekeeper
* Alice Terry - Saleslady (as Alice Taafe) Noah Beery - Doctor (uncredited) William Boyd - Extra (uncredited)
* Edythe Chapman - Mrs. Berkeley (uncredited)
* Raymond Hatton - Beautician (uncredited)
* Lloyd Hughes - Reporter (uncredited) Charles Ogle - Bit Role (uncredited)
* Guy Oliver - Berkeleys Butler (uncredited)
* Larry Steers - Nightclub Patron (uncredited)
* Madame Sul-Te-Wan - Violas Maid (uncredited)

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 