Winter Carnival (film)
{{Infobox film
| name           = Winter Carnival
| image          = 
| caption        =  Charles Kerr (assistant)
| producer       = Walter Wanger
| writer = Lester Cole Budd Schulberg Maurice Rapf Corey Ford F Scott Fitzgerald (uncredited)
| based on         =  Richard Carlson
| music          = 
| cinematography = 
| editing        =
| studio = 
| distributor    =  
| released       = 1939
| runtime        = 
| country        = US
| language       = English
| budget = $412,640 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p439 
| gross = $474,286 
}}
Winter Carnival is a 1939 film.

The script was worked on by Budd Schulberg and F. Scott Fitzgerald. This experience led to Schulbergs novel The Disenchanted. 

==Reception==
It recorded a loss of $33,696. 

==References==
 

==External links==
*  at IMDB
*  at TCMDB
 

 
 
 
 
 


 