100 Ways to Murder Your Wife
 
 
{{Infobox film
| name           = 100 Ways to Murder Your Wife
| image          = 100WaystoMurderYourWife.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 殺妻二人組
| simplified     = 杀妻二人组
| pinyin         = Shā Qī Èr Rén Zǔ
| jyutping       = Saat3 Cai1 Ji6 Jan4 Zou2 }}
| director       = Kenny Bee
| producer       = Wong Jing
| writer         = 
| screenplay     = Alex Law
| story          = 
| based on       = 
| narrator       = 
| starring       = Kenny Bee Anita Mui Chow Yun-fat Joey Wong
| music          = Kenny Bee Alan Tsui So Chan Hung
| cinematography = Jimmy Leung
| editing        = Ma Chung Yin Chiu Cheuk Man Siu Fung Golden Harvest Go Go Film Production
| distributor    = Golden Harvest
| released       =  
| runtime        = 92 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$14,106,643
}}
 1986 Cinema Hong Kong comedy film directed by Kenny Bee and starring Bee, Anita Mui, Chow Yun-fat and Joey Wong.                             

==Plot==
Two well-respected football players meet one night in a Hong Kong bar. Roberto is a highly rated striker for a popular team while Football Fa is a star goalkeeper. Over a few drinks, the pair share compliments and, as the evening draws on, their problems. As things turn out, both of these cowardly men have the same major problem: their wives. Robertos wife Anita constantly nags him and designs outrageous outfits that she insists he wears. Meanwhile Football Fa is insanely jealous about his beautiful wife Wong Siu Yin and her popularity with his team-mates. Falling into a drunken stupor, both men foolishly agree to get rid of each others wives. Football Fa seems to have succeeded when he goes to Robertos house and wakes up thinking he has done the deed. In actual fact, Robertos wife has left thinking that her husband is with another woman. These two misunderstandings remain hidden though and Football Fa, recovering from the distressing thought of being a murderer, insists that Roberto returns the favour. What follows is an elaborate series of ideas to achieve this dubious goal and get away with it free from blame.

==Cast==
*Kenny Bee as Roberto
*Anita Mui as Anita
*Chow Yun-fat as Football Fa
*Joey Wong as Wong Siu Yin
*Wong Jing as Mr. Wong
*Eric Yeung as George
*Bennett Pang as Bomb seller Anthony Chan as Bridegroom Chan Chi To (cameo)
*Siu Woon Ching as Bride Tsui Ching Ching
*Danny Yip as Reverend at wedding ceremony
*Lai Mei Po
*Wu Ma as Bathroom attendant (cameo)
*Angela Chan as Anitas neighbor
*Angela Leung as Anitas neighbor
*Sandy Lamb as Anitas neighbor
*Shing Fui-On as Fas football teammate
*Alex Ng as Fas football teammate
*Lee Yiu King as Fas football teammate
*Shing Fuk On as Fas football teammate
*Leung Mei Kei
*Alan Tsui
*Lee Man Piu as Fas birthday party guest
*Raymond Lee as BBQ Pork seller
*Sze Kai Keung as Love Gay at gay club
*Alan Tam (cameo)
*Ho Kam Kong as Match commentator
*Yat Poon Chai as traffic cop (cameo)
*Cho Yuen Tat as Journey of Love at gay bar
*Poon Man Kit as BBQ Pork seller
*Cheung Yuen Wah as Reporter
*Annabelle Lui as Reporter
*Kobe Wong as Parking valet
*Wan Chi Keung as Party guest

==Box office==
The film grossed HK$14,106,643 at the Hong Kong box office during its theatrical run from 5 to 19 June 1986 in Hong Kong.

==References==
 

==See also==
*Anita Mui filmography
*Chow Yun-fat filmography
*Wong Jing filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 