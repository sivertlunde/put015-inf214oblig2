The Dancers
{{infobox_film
| title          = The Dancers
| image          =
| imagesize      =
| caption        =
| director       = Chandler Sprague
| producer       = Fox Film Corporation
| writer         =  (play) Edwin Burke(screenplay)
| starring       = Lois Moran Phillips Holmes Mae Clarke Mrs. Patrick Campbell
| music          = Hugo Friedhofer Jean Talbot
| cinematography = Arthur L. Todd
| editing        = Alex Troffey
| distributor    = Fox Film Corporation
| released       = November 8, 1930
| runtime        = 70 minutes
| country        = USA
| language       = English
}}
The Dancers is a 1930 American talking film melodrama produced and distributed by Fox Film Corporation and directed by Chandler Sprague.  It is based on a 1923 Broadway play by Viola Tree and Gerald du Maurier.  This film has the feature sound film debut of Mrs. Patrick Campbell.


==Cast==
*Lois Moran - Diana
*Phillips Holmes - Tony Walter Byron - Berwin
*Mae Clarke - Maxine
*Tyrell Davis - Archie
*Mrs. Patrick Campbell - Aunt Emily

==References==
 

==External links==
* 
* 


 
 
 
 