Just a Gigolo (1931 film)
{{Infobox film
| name           = Just a Gigolo
| image          = Haines1931.jpg
| image_size     = 190px
| alt            = 
| caption        = A screenshot from the film Jack Conway
| producer       = Irving Thalberg
| writer         = Fanny Hatton   Frederic Hatton
| narrator       = 
| starring       = William Haines   Irene Purcell   C. Aubrey Smith   Charlotte Granville
| music          = 
| cinematography = Oliver T. Marsh
| editing        = Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    =
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}} Jack Conway, produced by Irving Thalberg and starred William Haines, Irene Purcell, C. Aubrey Smith, and Ray Milland. It was adapted from the 1930 play of the same name, which also starred Irene Purcell in the role of Roxana Roxy Hartley.
 Just a Gigolo".

==Cast==
*William Haines as Lord Robert Bobby Brummel
*Irene Purcell as Roxana Roxy Hartley
*C. Aubrey Smith as Lord George Hampton
*Charlotte Granville as Lady Jane Hartley
*Lilian Bond as Lady Agatha Carrol
*Albert Conti as A French Husband
*Maria Alba as A French Wife
*Ray Milland as Freddie
*Lenore Bushman as Gwenny
*Gerald Fielding as Tony
*Yola dAvril as Pauline, Roxanas Maid

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 