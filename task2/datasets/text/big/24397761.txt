Mahamantri Timmarusu (film)
{{Infobox film
| name           = Mahamantri Timmarusu
| image          =
| image_size     =
| caption        =
| director       = Kamalakara Kameshwara Rao
| producer       = Atluri Pundarikakshaiah N. Ramabrahmam
| writer         = Pingali Nagendrarao
| narrator       = Gummadi S. Varalakshmi Devika Rajasree Mudigonda Lingamurthy Relangi Venkata Ramaiah Dhulipala Mukkamala Prabhakar Reddy
| music          = Pendyala Nageswara Rao
| cinematography =
| editing        =
| studio         = Vauhini Studios
| distributor    =
| released       = 26 July 1962
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Mahamantri Timmarusu  is a 1962 Telugu historical film directed by Kamalakara Kameshwara Rao. Versatile character actor Gummadi Venkateswara Rao played the key role, while N.T. Rama Rao the role of Krishnadevaraya.

==Plot==
The story is about the critical role of Timmarusu (Gummadi) played during the reign of Sri Krishnadevaraya (NTR).  The story begins with the pattabhishekam of Sri Krishnadevaraya. Before the function, he participates in the dance and music function of Chinna Devi (Vijayalaxmi). They get married and becomes his first queen. After coronation, Timmarusu arranges the marriage with Tirumala Devi (S. Varalaxmi) daughter of Srirangapatnam kingdom. Timmarusu slaps him on the cheek to remind him about the kickbacks while seating  on the crown. He plans to attack Gajapathis of Kalinga kingdom lonely. Knowing about this Timmarusu reaches Kalinga and protects him. Pratraparudra Gajapathi (Mukkamala) wanted to kill him with the help of his son Veerabhadra Gajapathi (Prabhakar Reddy). However his daughter Annapurna Devi (Devika) openly opposes it and loves him. In a political dialogue, Prataparudra agrees to marry his daughter to Krishnadevaraya. After the marriage, he plans to kills him. With the help of Timmarulu he comes out of danger. They reach the Hampi with Rani Annapurna Devi along with Hamvira (Lingamurthy). Annapurna gives birth to Tirumala Raya. Hamvira creates differences between Krishnadevaraya and Timmarusu and kills Tirumala Raya. He convinces Rayalu that Timmarusu is the culprit. The court on the instructions from Rayalu orders to make him blind and imprison him. Before Rayalu Knows the fact, the punishment is implemented. However Timmarusu pardons him and their relationship continued.

==Cast==
{| class="wikitable"
|-
! Character
! Actor/Actress
|-
| Mahamantri Timmarusu
| Gummadi Venkateswara Rao
|-
| Krishnadeva Rayalu
| N. T. Rama Rao
|-
| Tirumala Devi
| S. Varalakshmi
|-
| Annapurna Devi
| Devika
|-
| Chinna Devi
| L. Vijaya Lakshmi
|-
| Prataparudra Gajapati
| Mukkamala Krishna Murthy
|-
| Veerabhadra Gajapati
| M. Prabhakar Reddy
|-
| Govinda Rayalu
| Sobhan Babu
|-
| Allasani Peddana
| Dhulipala Seetharama Sastry
|-
|
| Relangi Venkataramaiah
|-
| Hamvira
| Mudigonda Lingamurthy
|-
| Ramalinga Nayakudu Mikkilineni
|-
| Yerukalasani
| Rajasree
|-
| Krishnaveni
| Radhakumari
|}

==Crew==
* Director: Kamalakara Kameswara Rao
* Producer: Atluri Pundarikakshaiah and N. Ramabrahmam
* Production Company: Gautami Productions
* Story, Dialogues and Lyrics: Pingali Nagendra Rao
* Music Director: Pendyala Nageswara Rao
* Choreographer: Vempati Satyam
* Director of Photography: Annayya
* Playback Singers: Ghantasala Venkateswara Rao, S. Varalakshmi, P. Susheela, P. Leela

==Songs==
* "Andhra Deva Venkateswara Virupaksha Swamy Charitra Erugani Mahapatakam Madesniki Pattinada" (Cast: Rajashri)
* "Jaya Anare Jaya Anare Telugu Velugulanu Naludesalaku"
* "Jayavani Charana Kamala Sannidhi Mana Sadhana" (Cast: NTR and Vijayalaxmi)
* "Leela Krishna Nee Leelalu Ne Leelaganaina Teliyanuga" (Singer: S. Varalakshmi; Cast: S. Varalakshmi and NTR)
* "Mohanaragamaha Moortimantamaye" (Singers: Ghantasala and P. Susheela; Cast: Devika and NTR)
* "Tadhastu Swamula Kolavandi Asthi Nathula Teliyandi" (Cast: NTR, Relangi and Rajashri)
* "Tirumala Tirupati Venkateswara" (Singers: S. Varalakshmi and P. Susheela)

==Awards== National Film Awards
*     

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 