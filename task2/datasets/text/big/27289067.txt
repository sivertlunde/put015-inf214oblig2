The Private Secretary (film)
{{Infobox film
| name           = The Private Secretary
| image          = "The_Private_Secretary"_(1935_film).jpg
| image_size     = 
| caption        = Original British trade ad Henry Edwards
| producer       = Julius Hagen
| writer         = George Broadhurst   Arthur Macrae   H. Fowler Mear Charles Hawtrey (The Private Secretary) and the book by Von Moser (Der Bibliotheker)
| narrator       =  Barry MacKay  Judy Gunn  Oscar Asche
| music          =  W. L. Trytel  (uncredited)
| cinematography = Sydney Blythe William Luff
| editing        = 
| studio         = Julius Hagen Productions
| distributor    = Twickenham Film Distributors Ltd  (UK)
| released       = September 1935  (UK)
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Henry Edwards Barry MacKay, Judy Gunn and Oscar Asche.  It is an adaptation of the play The Private Secretary by Charles Henry Hawtrey.

==Plot==
Wealthy Englishman Robert Cattermole returns from a trip to India to discover that instead of achieving success, his beloved nephew Robert has got into such debt that he now disguises himself as a mild-mannered minister to evade his creditors.

==Cast==
* Edward Everett Horton - Reverend Robert Spalding Barry MacKay - Douglas Cattermole
* Judy Gunn - Edith Marsland
* Oscar Asche - Robert Cattermole
* Sydney Fairbrother - Miss Ashford
* Michael Shepley - Henry Marsland
* Alastair Sim - Nebulae
* Aubrey Dexter - Gibson
* O. B. Clarence - Thomas Marsland
* Davina Craig - Annie

==Critical reeption==
TV Guide felt the comedy of the Victorian farce "didnt translate well into later times. Horton and Sim (in a secondary role) serve as the films saving graces with some nice comic moments"   and Sky Movies agreed, calling the film "a mostly dismal British farce stickily directed by former acting superstar Henry Edwards, but held back from disaster by the pawkily amusing performances of Edward Everett Horton, dithering delightfully in the leading role, and Alastair Sim, offering a lugubrious contribution as Mr Nebulae."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 