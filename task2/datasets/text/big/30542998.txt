Ente Veedu Appuvinteyum
 
{{Infobox film
| name = Ente Veedu Appuvinteyum
| image = 
| caption = 
| director =Sibi Malayil
| producer = Prem Prakash
| writer = Bobby-Sanjay
| starring =Jayaram   Jyothirmayi    Kalidas Jayaram
| music = Ouseppachan
| cinematography = 
| editing = 
| studio = 
| distributor = Murali films & PJ Entertainments 
| released = 2003
| runtime = 140 mins
| country = India
| language = Malayalam
| budget =
| gross =
}}

 Ente Veedu Appuvinteyum is 2003 family drama film in Malayalam language directed by Sibi Malayil and starring Jayaram, Jyothirmayi and Kalidas Jayaram. The film was remade in Tamil as Kannadi Pookal.

==Plot==
The movie revolves around Viswanathan (Jayaram), Meera (Jyothirmayi) and little Vasudev (Kalidas Jayaram) who are form a very happy family. Meera is not Vasus real mother. She had married Viswanathan, who was a widower and the father of a little boy. And she had never tried to keep Vasu in the dark about these truths, and she had always loved Vasu as her own son. For Vasu, Meera was his mother and his best friend too. They live happily.

In the meantime, Meera gives birth to a son, and it is Vasu who is the happiest. He names his little brother Appu and cares much for him. But then, gradually, Vasu begins to feel that Appu is getting more attention from his parents and that he himself is being neglected. This begins to create problems leading Vasu to unintentionally kill the baby, which in-turn gets him imprisoned in the juvenile home. After completing the terms in juvenile home, he comes back to the home and gets a surprise from Meera in the form of another baby. He kisses the baby, with Viswanathan and Meera happy about their bonding.

==Cast==

* Jayaram as Viswanathan
* Jyothirmayi	as Meera
* Kalidas Jayaram as Vasudev
* Sanusha as Teena
* KPAC Lalitha Siddique
* Kalashala Babu
* Nedumudi Venu
* TS Raju
* Gayathri
* Nisha Sarang

==External links==
* 
*  at Oneindia.in

==References==
 

 
 
 
 


 