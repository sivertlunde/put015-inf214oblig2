Khrustalyov, My Car!
{{Infobox film
| name           = Khrustalyov, My Car!
| image          = 
| image_size     = 
| caption        =  Aleksei German
| producer       = Aleksandr Golutva Armen Medvedev Guy Séligmann Aleksei German Joseph Brodsky Svetlana Karmalita
| narrator       = 
| starring       = Yuriy Tsurilo
| music          = Andrei Petrov
| cinematography = Vladimir Ilyin
| editing        = Irina Gorokhovskaya
| distributor    = Polygram Filmed Entertainment
| released       = 20 May 1998
| runtime        = 150 minutes
| country        = Russia
| language       = Russian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1998 cinema Russian drama Aleksei German based on a story by Joseph Brodsky, screenplay by Svetlana Karmalita. It was produced by Canal+, CNC, Goskino, Lenfilm and VGTRK.

==Plot summary==
Surgeon General Klensky (Yuri Tsurilo), is captured and sent to Gulag convicted for the Doctors Plot to kill Joseph Stalin.

==Cast==
* Yuri Tsurilo as Gen. Klensky
* Nina Ruslanova as Wife
* Mikhail Dementyev as Son
* Aleksandr Bashirov as Idiot
* Jüri Järvet Jr. as Finnish reporter
* Sergei Dyachkov

==Awards== 1999 Russian Guild of Film Critics Awards as the Best Film; Aleksei German won Best Director for Khrustalyov, mashinu!

It was nominated for the Palme dOr at the 1998 Cannes Film Festival.   

==References==
 

==External links==
* 
*  at Epinions

 
 
 
 
 
 

 
 