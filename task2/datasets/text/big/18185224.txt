Fireball 500
{{Infobox film
| name           = Fireball 500
| caption        =
| image	         = Fireball 500 FilmPoster.jpeg
| director       = William Asher
| producer       =James H. Nicholson Samuel Z. Arkoff
| writer         = William Asher  Leo Townsend Burt Topper Fabian Chill Wills
| cinematography =
| editing        = studio = American International Pictures
| distributor    = American International Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2 million (est. US/ Canada rentals) 
}} directed by William Asher, it tells the story of Dave Owens (Avalon), a stock car racer forced to run moonshine.

==Plot==
Stock car racer "Fireball" Dave Owens from California goes to race in Spartanburg, South Carolina, where he intends on competing against local champ Sonny Leander Fox. Dave beats Leander in a race, impressing the latters girlfriend, Jane, and the wealthy Martha Brian.

Martha persuades Dave to drive in a cross country night race, not telling him he is in actuality smugging moonshine. Agents from the IRS threaten to send him to prison unless Dave helps them bust the local moonshine ring. 

After a driver, Joey, is killed while doing a run, Dave and Leander agree to team up to investigate the accident. They discover it was caused by placing a huge mirror across the road. It turns out that Marthas partner, Charlie Bigg, was responsible for the moonshine and the murder... and tried to kill Dave because he was jealous of Marthas attachment to him.

Dave wins the big race but Leander is injured. Jane helps him recover and Dave drives off into the sunset with Martha.

==Cast==
*Frankie Avalon as Dave "Fireball" Owens
*Annette Funicello as Jane Harris Fabian as Sonny Leander Fox
*Harvey Lembeck as Charlie Bigg
*Chill Wills as Big Jaw Harris
*Julie Parrish as Martha Brian Sandy Reed as Race Announcer Douglas Henderson as Hastings
*Baynes Barron as Bronson
*Ed Garner as Herman Mike Nader as Joey
*Vin Scully as The Narrator (prologue) Dodgers Vin Sully Calls Fireball 500
Los Angeles Times (1923-Current File)   26 Aug 1966: c19.  

==Production notes==
The movie was part of a conscious attempt on AIP to move away from beach party movies, which were losing popularity, and go towards youth rebellion films such as Fireball 500 and The Wild Angels. AIP executive Deke Heyward said that:
 The next big thing for teenage films is protest. Teenagers empathize with protest because they are in revolt against their parents... These films represent a protest against society. These will be moral tales, there will be good guys and bad guys. But we will show the reasons for young people going against the dictates of the establishment.  
Stock car racing had already been the subject of Red Line 7000 but this movie would be specifically told from the teenagers point of view.

Fabian signed a multi picture deal with AIP in late 1965 and this was the first movie he made for them. It was shot in early 1966.  George Barris, Batmobile for the Batman (TV series)|Batman television show which premiered in January 1966.

Footage from Fireball 500, specifically shots of the 4B car (Jim Douglas car) toppling over on its roof, show up later in the demolition derby scenes at the beginning of The Love Bug. When making the film AIP would hire a race car driver and install cameras in the front and rear of his car to obtain shots.  The film is notable for its depiction of the inherently dangerous Figure 8 racing.
 Thunder Alley.

===Music===
The films soundtrack is by Les Baxter, and features six songs written by Guy Hemric and Jerry Styner.  Frankie Avalon sings:
*"Fireball 500,"  the song made popular by Frank Sinatra), 
*"Turn Around," 
*"A Chance Like That", and 
*"Country Carnival."
Annette Funicello sings "Step Right Up."

==Reception==
===Critical===
The Los Angeles Times said the film "leaves American Internationals beach formula pretty much intact despite William Ashers attempt to inject some sophistication into his story" but thought it was "always easy to watch" with "a brisk tempo, a stylish verve that leaps over large holes in the story." FIREBAL IN BRISK TEMPO
Thomas, Kevin. Los Angeles Times (1923-Current File)   09 Sep 1966: C11.   The New York Times called it "a real turkey... one old bird that should have been cremated, not cooked." Screen: Dean Martin in Texas Across the River: Weak Western Spoof Has Local Premiere 2 Other Movies Open at Theaters Here
By BOSLEY CROWTHER. New York Times (1923-Current file)   24 Nov 1966: 65.  
===Sequel===
In July 1966 it was announced Burt Topper would produce a follow up, Malibu 500 with a budget of $1.4 million. This became Thunder Alley.  American International Expanding Operations
Los Angeles Times (1923-Current File)   04 May 1966: e13.  

==References==
 

==Further reading==
* Leonard Maltins Movie Guide 2006, p.&nbsp;428. New York: Penguin/New American Library, 2006.

==External links==
*  
*  at Brians Drive In Theatre
*  at TCMDB

 
 
 

 
 
 
 
 
 
 
 