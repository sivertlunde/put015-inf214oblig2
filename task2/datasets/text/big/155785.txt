A Goofy Movie
{{Infobox film
| name        = A Goofy Movie
| image       = A Goofy Movie.png
| caption     = Theatrical release poster
| director    = Kevin Lima
| producer    = Dan Rounds
| screenplay  = Jymn Magon Chris Matheson Brian Pimental
| story       = Jymn Magon
| starring    = Bill Farmer Jason Marsden Don Davis
| editing     = Gregory Perler Disney MovieToons Buena Vista Pictures Distribution, Inc.
| released    =  
| runtime     = 78 minutes
| country     = United States
| language    = English
| budget      = 
| gross       = $35.3 million 
}} animated musical musical comedy Disney MovieToons, and released in theaters on April 7, 1995 by Walt Disney Pictures. The film features characters from The Disney Afternoon television series Goof Troop; the film itself acts as a sequel to the TV show.
 Max as Goofy believes that hes losing Max. A direct-to-video sequel called An Extremely Goofy Movie was released in 2000. The film was dedicated to Pat Buttram, who died during production.

==Plot==
 
Goofy is the single father of a teenage boy named Max Goof in the town of Spoonerville, though the two have a tense relationship. On the last day of school before summer vacation, Max and his best friends Pete Junior|P.J. and Robert "Bobby" Zimmeruski hijack the auditorium stage in the middle of Principal Mazurs speech, creating a small concert where Max performs, while costumed as the pop singer Powerline. The performance succeeds in making Max a school celebrity and impressing his love interest, Roxanne; but he, P.J. and Bobby are sent to Mazurs office. Roxanne speaks with Max and agrees to go with him to a party where Powerlines concert will be aired live, but Mazur forewarns Goofy that Maxs actions may result in facing  capital punishment.

Goofy desperately decides to take Max on a fishing trip to Lake Destiny, Idaho, following a map route he and his father took years ago, and the two go into his station wagon. However, he is oblivious to what Max is planning to do with Roxanne. Max stops by Roxannes house to call off their date, but when Roxanne says she will just have to go with someone else, Max instead fabricates a story about his father knowing Powerline; he tells her he will be on stage at the concert.
 Pete and P.J. join them. Following Petes advice to keep Max under control, Goofy takes his son fishing and performs the Perfect Cast fishing technique, luring Bigfoot to their camp. Pete and P.J. flee, leaving Goofy and Max to spend the night with Bigfoot. At night, while Goofy is still asleep, Max alters the map route to Los Angeles, where the concert is to be held.

The next morning, Goofy decides to make Max the navigator of the trip. The two go to several locations that satisfy both of them. They stop by a motel where they meet Pete and P.J. again. When Pete overhears a conversation between Max and P.J., he tells Goofy that Max has tricked him in traveling to Los Angeles. The next day, Goofy and Max come to a junction: One leading to Idaho, the other to California. Max chooses the route to California, making Goofy stop the car and storm off in anger. With the brake loose, the car drives off on its own; As they chase after it and end up plummeting in a river, they have a heated argument. Goofy reveals that no matter how old Max gets, he will always be his son and the two reconcile with each other. Realizing Max had promised to Roxanne to go to the concert, Goofy decides to take him to Los Angeles. The two nearly plummet down a waterfall to their deaths, but Max fortunately saves Goofy, using the Perfect Cast technique.
 	
Goofy and Max get to Los Angeles and they end up onstage and dance with Powerline, watched by Pete, P.J. and Roxanne on separate televisions. Goofy and Max later return to Roxannes house in their damaged car. Max tells the truth to Roxanne, though she accepts it and admits she always had feelings for him, ever since he first said, "Ahyuck!"; thus, a relationship starts between them. Goofys car suddenly explodes due to its damaged sustained at the waterfall after it fell, but he safely falls through the porch roof of Roxannes house, and he is introduced to her by Max.

==Voice cast== Goofy Goof
* Jason Marsden as Max Goof (singing voice by Aaron Lohr)
* Rob Paulsen as Pete Junior|P.J. Pete Peter Pete
* Kellie Martin as Roxanne
* Pauly Shore (uncredited) as Robert "Bobby" Zimmeruski
* Wallace Shawn as Principal Arthur Mazur
* Frank Welker as Bigfoot Jenna von Oy as Stacy
* Julie Brown as Lisa
* Kevin Lima as Lester
* Tevin Campbell as Powerline
* Florence Stanley as Waitress
* Jo Anne Worley as Miss Maples
* Joey Lawrence as Chad
* Wayne Allwine as Mickey Mouse
* Pat Buttram as Possum Park Emcee
* Herschel Sparber as Security guard Pat Carroll as Restaurant waiter
* Corey Burton as Wendell Brittney Alyse Smith as Photo Studio Girl

==Production==
A Goofy Movie was the directorial debut for Disney crewmember Kevin Lima, who went on to direct the Disney films Tarzan (1999 film)|Tarzan and Enchanted (film)|Enchanted.   In 1995, Lima said that "Instead of just keeping Goofy one-dimensional as hes been in the past, we wanted to give an emotional side that would add to the emotional arc of the story. We wanted the audience to see his feelings instead of just his antics."   

The main characters of this film, specifically Goofy, Max Goof, Pete (Disney)|Pete, and Pete Junior|PJ, are based on their incarnations in the Goof Troop television show, albeit slightly older: Max and PJ are high-school aged rather than middle-schoolers. However, other characters that had been established in Goof Troop do not appear in this film, such as Petes wife Peg, his daughter Pistol, and pets Waffles and Chainsaw. Goofy and Pete retain their classic looks from the 1940s cartoons as opposed to the looks that they had in the 1950s cartoons and Goof Troop.
 Walt Disney Walt Disney Animation France S.A. and Walt Disney Animation Australia (later DisneyToon Studios) . Pre-production was done at the main WDFA studio in Burbank, California, starting as early as mid-1993. The animation work was done at Walt Disney Animation France S.A. (formerly Brizzi Films) in Paris, France supervised by Paul and Gaëtan Brizzi, with additional scenes animated at Disneys studio in Sydney, Australia (later DisneyToon Studios) under the direction of Steve Moore, and clean-up work done at the main Burbank studio.   
Additional clean-up/animation was done by Phoenix Animation Studios in Canada, and digital ink and paint by the Pixibox studio in France.   

==Music== Don Davis.  Bobby Brown was the original choice for Powerline and had some songs recorded but was cut due to drug problems. Some of the songs Bobby did for the movie were revamped and ended up on his Forever (Bobby Brown album)|Forever album. The songs "I 2 I" and "Stand Out" were performed by Contemporary R&B|R&B singer Tevin Campbell. The soundtrack album for A Goofy Movie was released by Walt Disney Records on March 18, 1995.  Mitchell Musso covered the song "Stand Out" for the DisneyMania 7 album, which was released on March 9, 2010. Amazon.com. " ". Retrieved 20 February 2010. 

{{Tracklist
| collapsed = no
| headline = A Goofy Movie Original Soundtrack
| title1 = I 2 I
| note1 = Tevin Campbell
| length1 = 4:02
| title2 = After Today
| note2 = Aaron Lohr and Chorus
| length2 = 2:21
| title3 = Stand Out
| note3 = Tevin Campbell
| length3 = 3:00
| title4 = On the Open Road
| note4 = Bill Farmer, Aaron Lohr, and Chorus
| length4 = 3:01
| title5 = Lesters Possum Park
| note5 = Kevin Quinn and Chorus
| length5 = 1:25
| title6 = Nobody Else But You
| note6 = Bill Farmer and Aaron Lohr
| length6 = 2:35
| title7 = Opening Fanfare / Maxs Dream / Transformation
| length7 = 1:25
| title8 = Deep Sludge
| length8 = 2:35
| title9 = Bigfoot
| length9 = 1:50
| title10 = Hi Dad Soup
| length10 = 2:04
| title11 = Runaway Car
| length11 = 2:14
| title12 = Junction
| length12 = 1:32
| title13 = The Waterfall! / The Truth
| length13 = 2:17
}}

Additional songs featured in the film include:
 High Hopes" by Rick Logan
* "Stayin Alive" by The Bee Gees
* "Ride of the Valkyries" by Richard Wagner

==Release==
A Goofy Movie was originally scheduled for a November 1994 theatrical release, {{cite news
|title=Calendar Of Feature Releases
|work=The Film Journal
|date=1994-01-01
}}  but production setbacks resulted in a pushback to 1995, while The Lion King was reissued to fill in for the films absence. {{cite news
|first=Aimee
|last=Miller
|title=The ‘Lion’ Sleeps This Fall
|work=The Washington Post
|date=1995-08-13
}}  The films premiere took place on April 5, 1995 at the AMC Pleasure Island in Lake Buena Vista, and was attended by director Kevin Lima and voice stars Bill Farmer and Jenna von Oy. On the 7th, it was released nationwide. {{cite news
|first=Tom
|last=Spitz
|title=‘Goofy Movie’ World Premiere
|work=The Orlando Sentinel
|date=1995-04-06
}} 

The film was first released on VHS home video on September 6, 1995, and included a music video for the Parachute Express song Doctor Looneys Remedy on their video, Come Sing with Us. In the UK, it was released in theaters succeeding the Mickey Mouse short Runaway Brain on October 18, 1996 and on VHS in 1997. It was reissued on June 20, 2000, along with a DVD version as part of the short-lived Walt Disney Gold Classic Collection. To date, this film and Dougs 1st Movie are the only two Disney animated films produced in widescreen that have pan and scan-only Region 1 DVD releases (not counting separate widescreen and pan and scan DVD releases of the two Disney/Pixar films The Incredibles and Cars (film)|Cars). However, the films PAL and NTSC (Japan) counterpart does have a non-anamorphic widescreen DVD, and the film is available in a letterbox presentation on Laserdisc.
 Toon Disney Disney Channel HD on June 10, 2008, it was in the standard-definition television|standard-definition format instead of the high-definition television|high-definition format.

It was revealed inside a Mickey Mouse Clubhouse DVD that this film along with its sequel will be re-issued as a two-movie pack; however the DVD has been put into hiatus.

A Goofy Movie is available for rent on Amazon.com Video On Demand in widescreen, and is now available in all English-speaking countries (including Ireland, UK and Australia) on iTunes.

==Reception==
A Goofy Movie received mixed reviews from critics. It holds a score  of 53% Rotten Tomatoes based on reviews from 13 critics. 

Variety (magazine)|Variety s Todd McCarthy criticized the films score, calling the six featured songs "unmemorable". He also felt that the personality of Goofys character, while agreeable enough in support, proved a bit over the top for a headliner, and that "by any reasonable reckoning, hes distinctly overbearing and selfish, and responds with a bland dismissal to any opinion offered up by his son." McCarthy praised the films technical aspects, calling them "crisp and clean".   
Louis Black of The Austin Chronicle summed up his review by saying the film was "bland, a barely television-length cartoon stretched out to fill a feature, and not much fun." 

Siskel and Ebert both approved of the movie, praising the color scheme and the "sweet" father-son plot and gave it a "Two Thumbs Up". 

===Box office===
A Goofy Movie was considered a relative success for Disney, opening in 2,159 theaters at #2 on its opening weekend with $6,129,557 - held from the #1 spot only because of the Will Smith blockbuster Bad Boys (1995 film)|Bad Boys that opened the same weekend, with $15,523,358 in box office returns.    It ultimately ended its run at the US box office grossing $35,348,597 - coming in as the 51st highest-grossing domestic film in 1995.   

===Accolades=== Best Animated Feature" in the production categories and "Best Production Design", "Best Storyboarding", "Best Music", and "Best Animation" in the individual categories at the 23rd Annie Awards. 

==Sequel== House of Mouse (specifically the episode "Maxs Embarrassing Date"), where she was voiced by Grey DeLisle instead of Kellie Martin.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 