Elavamkodu Desam
{{Infobox film
| name           = Elavamkodu Desam
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = K. G. George
| producer       = Media Club
| writer = K. G. George  (story & screenplay)  Sreevaraham Balakrishnan  (dialogues)  Rajeev Kushboo Kushboo Thilakan
| music          = Songs & Score:  
| cinematography = Ramachandra Babu
| editing        = G. Murali
| studio         = Media Club Private Limited
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
 Rajeev in the lead roles, and Kushboo Sundar|Kushboo, Thilakan and Babu Nampoothiri in major supporting roles.

==Plot==

Jathavedan is invited to Elavamkodu Desam, a princely state in pre-independent India, to treat the kings wife. He comes to know that Unikkoman, the wicked king of Elavamkodu Desam, rose to power in coup and killing the noble king Udayavarman. How Jathavedan raises an army to bring the crown back to its true owner form the crux of the story.

==Cast==

* Mammootty as Jathadevan Rajeev as Unikkoman Kushboo as Ammalu, Unikkomans wife
* Thilakan as Mooss
* Babu Namboothiri as Raru Asan
* Jagathy Sreekumar as Kurungodan
* Shruti as Nandini, the princess
* Vakkom Jayalal as Baladevan, the prince
* Spadikam George as Idicheman, Unikkomans army head
* Manka Mahesh as Mangalabhai thampuratti
* Captain Raju as Udayavarma
* Narendra Prasad as Adityan, Unikkomans uncle and Ammalus first husband
* Bharath Gopi as Agnisharman, Jathadevans uncle
* V. K. Sreeraman as Nethran
* Subair Abu Salim
* Suresh Gopi as the King

==External links==

*  

 
 
 

 