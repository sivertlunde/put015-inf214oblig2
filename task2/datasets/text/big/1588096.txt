Dream On!
{{Infobox film
| name        = Dream On!
| image       =
| director    = Ed Harker
| writer      = Ed Harker
| starring    = Ed Harris Erin Nico Steven Hartley Paul Reubens Philip Baker Hall Karen Kondazian
| producer    = Erin Nico
| distributor =
| released    =  
| runtime     = 98 min
| language    = English
| budget      =
}}
 Festival International Reubens have since become well known actors.

==Plot summary== the same name, Dream On! is a tale of struggling LA actors seeking out an audience. This talented but impoverished troupe stages a "guerilla theatre" production, wherein each actor takes on a variety of characterizations. Given that the actors include an ex-hooker and a pair of mismatched homosexuals, perhaps the troupe is using their production as a means of escaping the torments of their own lives. Perhaps nothing—thats just what theyre doing.

==Awards and nominations==
*Honorable Mention (Roger Ebert) at the US Film and Video Festival (Sundance Film Festival), 1983
*Silver Award at the Festival of the Americas (Houston), 1983

==External links==
* 
* 

 