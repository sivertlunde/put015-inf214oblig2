Sauda (film)
{{Infobox Film |
 name        = Sauda|
 image        = |
 director     =Ramesh Modi | Neelam Vikas Bhalla Saeed Jaffery Dalip Tahil Kiran Kumar|
 music       = Aadesh Shrivastava|
 released    = 3 February 1995|
 runtime     = 120 min|
 language    = Hindi |
 }}
 1995 Hindi Indian feature directed by Neelam and Sumeet Saigal in lead roles, with Saeed Jaffery, Dalip Tahil and Kiran Kumar in supporting roles. The film has been inspired from Hollywood blockbuster Indecent Proposal.

==Plot==

A young boy named Deepak (Vikas Bhalla) is a student in a college, where he falls in love with a beautiful girl named Jyoti (Neelam Kothari|Neelam). Jyoti ignores Deepak until he impresses her with a song, where she is impressed and the two fall in love. The two get married and Deepak starts searching for a job of an architect. He gets the job and becomes an architect.

His friend, Madan works in a bank. Pradeep Singh (Kiran Kumar) is a rich man, who has an account in Madans bank. He has to go abroad, so he gives Rs. 25 Lacs to Madan and tells him that he can give him the money back after five years. Deepak offers Madan to use the money and improve his life, but Madan refuses, so Deepak takes the money himself and buys himself and Jyoti, a big bungalow.

The two go to casino, where Deepak wins casino and earns lots of money. They decide to play a bigger game, where Deepak and Jyoti lose more money. A rich businessman named Prakash (Sumeet Saigal) offers Jyoti to play casino for him and she wins the casino. Prakash and Deepak make friends and he invites them to his birthday, where he sings a song for them.
 sleeps with him for one night. Deepak sells the bungalow to a dealer. On his way back to home, he sits in a taxi, where the taxi driver and some of his friends fight Deepak and snatch his money. He goes back home at night and he tells Jyoti about the incident.

At morning, when Deepak wakes up, Jyoti is not at home. When Jyoti returns to home, Deepak asks her where she had gone. She tells him that she has slept with Prakash, which causes Deepak and Jyoti to quarrel with each other and suddenly Jyoti leaves Deepak. Jyoti begins working in an estate agency, where she finds Prakash and the two become friends. Deepak earns a lot of money in his job and returns the 25 Lac Rs. to his friend Madan.

One day, Prakash invites Jyoti to a house, where he tries to rape her until Deepak rescues her from Prakash and almost kills him until Jyoti stops Deepak from killing him. Jyoti and Deepak are together again and Prakash tells his uncle (Saeed Jaffery) that he had to do it because he wanted Deepak and Jyoti to become a couple again.

==Cast==

* Vikas Bhalla as Deepak Neelam as Jyoti
* Sumeet Saigal as Prakash
* Saeed Jaffery as Prakashs uncle
* Dalip Tahil as Jyotis father
* Kiran Kumar as Pradeep Singh

==Songs==

*Baagh Mein Tu Chal      -- Asha Bhosle, Udit Narayan     
*Deewana To Keh Diya      -- Kumar Sanu     
*Dil Mein Mohabbat      -- Udit Narayan, Vijeta Pandit      
*Koi Dil Na      -- Asha Bhosle, Sonu Nigam      
*Pyar Ka Pehla Khat      -- Udit Narayan, Vijeta Pandit      
*Yeh Baat Hai      -- Udit Narayan

The song,  Deewana to keh diya  sung by Kumar Sanu was a chartbuster.

==External links==
* 

 
 
 