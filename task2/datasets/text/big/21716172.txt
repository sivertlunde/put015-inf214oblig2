Shimkent hôtel
{{Infobox Film 
| name = Shimkent hôtel
| image = Shimkent hôtel poster.jpg 
| image_size = 
| caption = Theatrical poster
| director = Charles de Meaux 
| producer =Gil Donaldson   Xavier Douroux  Franck Gautherot   Helen Olive  Carol Polakoff 
| writer = Charles de Meaux 
| narrator = 
| starring = 
| music = Charles de Meaux  Vladimir Karoev  Pierre Mikaïloff 
| cinematography = 
| editing =Guillaume Le Du 
| studio =Anna Sanders Films  
Donaldson Polakoff Productions 
| distributor =Bodega Films 
| released =23 April 2003 
| runtime =90 min 
| country =France / UK  Tajik 
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}} 
Shimkent hôtel is a 2003 French fictional film directed and written by Charles de Meaux who co-wrote music score with Vladimir Karoev and Pierre Mikaïloff. It tells the story of a young man who failed a business venture in the Afghan mountains and is suffering from shock in Kazahkstan.

==Cast==

*Romain Duris as  Romain 
*Caroline Ducey as  Caroline 
*Melvil Poupaud as  Alex 
*Yann Collette as  Le consul 
*Thibault de Montalembert as Le Docteur de Montalembert 

==External links==
* 
* 
* 
*  at uniFrance
*  at Cineuropa

 
 
 
 

 