An Awful Moment
 
{{Infobox film
| name           = An Awful Moment
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith
| starring       = George Gebhardt
| cinematography = Arthur Marvin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 12 minutes (one Reel#Motion picture terminology|reel)
| country        = United States
| language       = Silent
| budget         = 
}}
  silent short short drama film directed by D. W. Griffith. A print of the film exists in the film archive of the Library of Congress.   

==Plot==
Judge Mowbray sentences a man, at which a gypsy woman protests. The Judge later goes home and sees his wife and daughter. However the gypsy woman breaks into his house. She knocks out Mrs. Mowbray, gags her and ties her to a chair. She sets up a gun to shoot her dead when the door is opened. However the daughter wakes up and is able to tell her father, who saves his wife, and the gypsy woman is arrested.

==Cast==
* George Gebhardt as Matteo Rettazzi / Policeman
* Marion Leonard as Fiammetta, Matteos Wife
* Harry Solter as Judge Mowbray
* Florence Lawrence as Mrs. Mowbray
* Gladys Egan as The Mowbrays Daughter
* Linda Arvidson as The Maid Florence Barker
* Dorothy Bernard
* Kate Bruce Charles Gorman
* Gertrude Robinson as Woman in Court
* Mack Sennett as Policeman / Man in Court Dorothy West

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 