Night Warning (1946 film)
{{Infobox film
| name = Night Warning
| image =
| image_size =
| caption =
| director = Léon Mathot
| producer = Lucien Masson
| writer =  René Wheeler
| narrator =
| starring = Hélène Perdrière   Roger Pigaut   Pierre Dudan Jean Lenoir
| cinematography = Charles Bauer   
| editing =     Aleksandr Uralsky
| studio = La Société des Films Sirius 
| distributor = La Société des Films Sirius 
| released = 25 September 1946 
| runtime = 120 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} war drama film directed by Léon Mathot and starring Hélène Perdrière, Roger Pigaut and Pierre Dudan.  The films sets were designed by the art director Roland Quignon. A waitress helps an escaped French resistance fighter to evade capture and make it to Britain.

==Partial cast==
* Hélène Perdrière as Hélène  
* Roger Pigaut as Pierre  
* Pierre Dudan as Klaus  
* Philippe Hersent as Stefan Hess  
* Régine Montlaur as Lily  
* Marcelle Monthil  Henry Murray 
* Pierre Collet 
* Howard Vernon as Laviateur anglais  
* Abel Tarride as Laubergiste  
* Alexandre Mihalesco 
* Georges Jamin 
* Marc Cassot 
* Charles Lemontier as Le cheminot 
* Jane Marken as Madame Morizot  
* Marcel Delaître as Morizot  
* Simone Cerdan as La fille

== References ==
 

== Bibliography ==
* Rège, Philippe. Encyclopedia of French Film Directors, Volume 1. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 

 

 