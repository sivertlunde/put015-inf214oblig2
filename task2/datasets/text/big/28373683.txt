Barbora Hlavsová
{{Infobox film
| name           = Barbora Hlavsová
| image          = 
| image size     = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Jaroslav Havlíček Karel Steklý
| starring       = Terezie Brzková
| music          = 
| cinematography = 
| editing        = Jan Kohout
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
 Czechoslovak drama film directed by Martin Frič.   

==Cast==
* Terezie Brzková as Barbora Hlavsová
* František Smolík as Vojtech Hlavsa
* Jiřina Štěpničková as Klára Hlavsová
* Jindřich Plachta as Zanta, pensioner
* Jaroslav Průcha as Prouza, city major
* Vladimír Řepa as Kvech, barber
* Stella Májová as Vlasta Kvechová
* František Filipovský as Bartyzal, jeweller
* Karel Dostál as Lukás Hlavsa, carver
* Eliška Kuchařová as Eliska
* Rudolf Hrušínský as Rysavý, miller
* Vilém Pfeiffer as Kaliba Eman Fiala as Weaver
* Václav Trégl as Oldrich, weaver

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 