Vida Parayaanmathram
{{Infobox film
| name           = Vida Parayaanmathram
| image          =
| caption        =
| director       = PK Joseph
| producer       =
| writer         = Kamala Govind
| screenplay     = Devan Sandhya Sandhya Jagathy Sreekumar Kaviyoor Ponnamma
| music          = M. K. Arjunan
| cinematography =
| editing        = K Sankunni
| studio         = TBC Presents
| distributor    = TBC Presents
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by PK Joseph. The film stars Devan (actor)|Devan, Sandhya (actress)|Sandhya, Jagathy Sreekumar and Kaviyoor Ponnamma in lead roles. The film had musical score by M. K. Arjunan.   

==Cast== Devan
*Sandhya Sandhya
*Jagathy Sreekumar
*Kaviyoor Ponnamma Murali
*Prathapachandran
*KPAC Sunny

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Thaaraka Deepaankurangalkkidayil || P Jayachandran || Poovachal Khader || 
|-
| 2 || Vidaparayaan Maathram || P Jayachandran || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 