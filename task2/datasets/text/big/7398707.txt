Breaking News (2004 film)
 
 
{{Infobox film
| name           = Breaking News
| image          = Breakingnews.jpg
| caption        = 
| film name =  
 | simplified     =  
 | jyutping       = Daai si gin
 | pinyin         = Dà Shì Jiàn}}
| director       = Johnnie To
| producer       = Johnnie To Cao Biao  Yip Tin-shing Milkyway Creative Team Eddie Cheung Simon Yam Maggie Shiu
| music          = Ben Cheung Chung Chi-wing Cheng Siu-cheng
| editing        = David M. Richardson Media Asia China Film Group Milkyway Image Media Asia Distribution
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong 
| language       = Cantonese Mandarin English
| budget         = 
| gross          = $1,028,420 
}} Hong Kong Eddie Cheung, Simon Yam and Maggie Shiu. The film premiered out of competition at the 2004 Cannes Film Festival.   

==Plot== Hong Kong Police finds itself in a public relations crisis after a disastrous shootout and the scene of a police officer surrendering in apparent fear to the mobsters was captured and telecast by the local media. Inspector Cheung and his crew are assigned to the task of catching these mobsters, led by the intelligent and resourceful Yuen. 

In the meantime, Superintendent Rebecca Fong leads an effort on the part of the Hong Kong Police to mislead the media and salvage the reputation of the police team. She sees the chance in a raid on the mobsters hiding out in an apartment, while Inspector Cheung leads his own team to search for the mobsters, with many gunfights breaking out in between. 
 PTU officers SDU operators were called in to flush out Yuen and his fellow mobsters. 

Inspector Cheung successfully tracks down Yuen, though he is defeated only after a high-octane chase and gunfight sequence, with Fong held hostage.

==Cast==
* Richie Jen - Yuen
* Kelly Chen - Superintendent Rebecca Fong
* Haifeng Ding - Ah Lung
* Nick Cheung - Inspector Cheung Eddie Cheung - Eric Yeung
* Benz Hui - Hoi
* Lam Suet - Yip
* You Yong - Chun
* Li Haitao - Chung
* Simon Yam - Asst. Commissioner Wong
* Maggie Shiu - Grace Chow
* Wong Chi-Wai
* Wong Wah-Wo
* Alan Chui

 
 

==Awards== Golden Horse Awards 
* 2004 Cannes Film Festival: official selection

==Remakes== American film director Joel Schumacher is in talks to direct the remake.
 Swedish director Anders Banke. The Russian/Swedish version of Breaking News was theatrically released in Russia and ex-USSR on 7 May 2009. 

==See also==
* List of Hong Kong films
* Johnnie To filmography

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 