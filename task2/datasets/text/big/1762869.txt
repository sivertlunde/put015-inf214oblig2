The Squaw Man (1914 film)
:For other uses, see: The Squaw Man (disambiguation).
{{Infobox film name            = The Squaw Man image           = TheSquawMan1914.jpg image_size      = 250px caption         = A scene from The Squaw Man director        = Oscar Apfel Cecil B. DeMille producer        = Cecil B. DeMille   Jesse L. Lasky writer          = Beulah Marie Dix (scenario) story           = Beulah Marie Dix based on        =   starring        = Dustin Farnum music           = cinematography  = Alfred Gandolfi editing         = Mamie Wagner distributor  Famous Players-Lasky Corporation released        =   runtime         = 74 minutes country         = United States language  English intertitles budget          = gross           = $244,700 preceded_by     = followed_by     =
}}
 
  1914 silent silent western western drama film starring Dustin Farnum  and co-directed by Cecil B. DeMille.

==Production background== produced by adapted by of the same name, written by Edwin Milton Royle.
 shorts had In Old San Pedro, California and the western saloon set was built beside railroad tracks in the San Fernando Valley. Footage of cattle on the open range were shot at Keen Camp near Idyllwild-Pine Cove, California|Idyllwild, California, while snow scenes were shot at Mount Palomar. 
 silent remake talkie The version in 1931 in film|1931.

==Plot==
James Wynnegate (Dustin Farnum) and his cousin, Henry (Monroe Salisbury), upper class Englishmen, have been made trustees for an orphans’ fund. Henry loses money in a bet at a derby and embezzles money from “the fund” to pay off his debts. When war office officials are informed of the money missing from “the fund”, they pursue James, but he successfully escapes to Wyoming. In Wyoming, James rescues Nat-U-Rich (Lillian St. Cyr), daughter to the chief of the Utes tribe, from local outlaw Cash Hawkins (William Elmer). Cash plans on exacting his revenge on James but has his plans thwarted by Nat-U-Ritch who fatally shoots him. Later, James gets into an accident in the mountains and needs to be rescued. Nat-U-Ritch tracks him down and carries him back to safety. As she nurses him back to health, they become lovers and have a child. During an exploration in the Alps, Henry falls off a cliff. Before he succumbs to his injuries, Henry signs a letter of confession proclaiming James’ innocence in the embezzlement. Before Henrys widow Lady Diana (Winifred Kingston) and others arrive at James and Nat-U-Ritch’s residence in Wyoming to tell James about the news, the Sheriff recovers the murder weapon that was used against Cash Hawkins inside of the couples’ residence.  Nat-U-Rich, facing the possibilities of losing both her son and her freedom, decides to take her own life. 
 

==Controversies==
A non-Native American actor by the name of Joseph Singleton played the role of Tabywana, Nat-U-Ritchs father. Lillian St. Cyr a Native American was cast to play the role of a Native American from the Utes tribe. She is famously noted for her role as Princess Redwing. Lillian St. Cyr along with her husband James Young Deer have been regarded by many as the first Native American power couple in Hollywood.  DeMille selected Lillian St. Cyr to play her role because he wanted an Nat-U-Ritch to be played by an authentic Native American. 
At this point of time in the silent film era, films that were based on the experiences of Native Americans were very popular. The central theme of this film was miscegenation. In the state of California, anti-miscegenation laws existed until 1948. Although African Americans couldn’t legally marry whites in the state of California during the filming process, marriages between Native Americans and whites were recognized. Though there were more than enough to Native American actors to play the role of Native Americans at the moment, whites were mostly cast as Indian characters. Whenever Native Americans actors played Indian roles, they performed in redface.  

The costuming choices that Native American filmmakers made were intentionally traditionally inaccurate. Native Americans influenced the cultural perception of Native Americans by satirizing the stereotypical depictions of Native Americans. Young Deer and his wife Lillian St. Cyr transformed the way Native American characters were represented. The characters they created and portrayed were sympathetic in much more complex ways than any other silent-era filmmaker. 
However, Joanna Hearne says otherwise. She claimed that Native American themed silent films did not alter in anyway the dominant perception of Native Americans. Hearne also goes on to note that there were a great number of films that displayed the Native American experience from many different perspectives. However, she does acknowledge the involvement of Native American writers, filmmakers, actors during this time period. 

==Cast==
*Dustin Farnum as Capt. James Wynnegate aka Jim Carston
*Monroe Salisbury as Sir Henry, Earl of Kerhill (uncredited) Red Wing (real name Lillian St. Cyr) as Nat-u-ritch (uncredited)
*Winifred Kingston as Lady Diana, Countess of Kerhill (uncredited)
*Baby Carmen De Rue as Hal (uncredited)
*Joseph Singleton as Tab-y-wana (uncredited)
*William Elmer as Cash Hawkins (uncredited)
*Mrs. A.W. Filson as The Dowager Lady Elizabeth Kerhill (uncredited)
*Haidee Fuller as Lady Mabel Wynnegate (uncredited)
*Foster Knox as Sir John (uncredited)
*Dick La Reno as Big Bill (uncredited) Richard LEstrange as Grouchy (uncredited)
*Fred Montague as Mr. Petrie (uncredited)
*Cecil B. DeMille as Faro Dealer (uncredited)
*Cecilia de Mille as Child (uncredited)
*Hal Roach as Townsman (uncredited)
*Art Acord as Townsman (uncredited)
*Raymond Hatton as Bit part (uncredited)

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
 
* 
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 