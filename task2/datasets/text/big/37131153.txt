Tengkorak Hidoep
{{Infobox film
| name      = Tengkorak Hidoep
| image     = Poster tengkorak hidoep.jpg
| image size   = 
| border     = 
| alt      = 
| caption    = Poster
| director    =Tan Tjoei Hock
| producer    =The Teng Chun
| writer     = Tan Tjoei Hock
| starring    = {{plainlist|
*Tan Tjeng Bok
*Moh Mochtar
*Misnahati
*Bissu
}}
| music     = 
| cinematography = Tan Tjoei Hock
| editing    = 
| studio     = Action Film
| distributor  = 
| released    =  
| runtime    = 
| country    = Dutch East Indies Malay
| budget     = 
| gross     = 
}}
Tengkorak Hidoep (literally The Living Skull) is a 1941 film from the Dutch East Indies (now Indonesia) directed by Tan Tjoei Hock. It has been called Indonesias first domestic horror film.

==Plot==
Raden Darmadji and several of his friends go to the island of Mustika in search of Darmadjis brother, who was lost in a shipwreck ten years earlier. There, they discover that the god Maha Daru, who was locked away on the island 2,000 years before after losing a battle to the goddess Gumba. Darmadji, while exploring a cave, notices a huge rain storm. The earth rips asunder and Maha Daru escapes his prison.

As Darmadji tries to escape the cage, he is accosted by savage men and supernatural beings. His daughter, Rumiati, is also caught up in this apocalypse but seemingly rescued by Maha Daru. However, Maha Darus ill-will towards Rumiati&nbsp;– whom he considers a reincarnation of Gumba&nbsp;– is soon made clear. A man living in the jungle rescues her, and they fall in love. 

==Production==
The black-and-white film was written, shot, and directed by Tan Tjoei Hock, with Java Industrial Films The Teng Chun as producer;  it was Tans last film as director.  It starred Tan Tjeng Bok, Moh Mochtar, Misnahati, and Bissu. 
 film adaptations films centred on Edgar Rice Burroughss Tarzan. 

==Release and reception==
Tengkorak Hidoep was released in 1941 and reportedly a commercial success.  Tan credited this success on the films special effects, including a scene in which lightning smashes Maha Darus grave and he comes out, a living skull, surrounded by flames.  A 35 mm copy of the film is reportedly stored at Sinematek Indonesia in Jakarta. 
 Indonesian film industry. 

==References==
Footnotes
 

Bibliography
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
* {{cite news
  | last=Irwansyah
  | first=Ade
  | title = Tahukah Anda: Apa Film Horor Indonesia Pertama?
  |trans_title=Did You Know: The First Indonesian Horror Film?
  | language = Indonesian
  | url = http://www.tabloidbintang.com/extra/wikibintang/17249-tahukah-anda-apa-film-horor-indonesia-pertama.html
  | work = Tabloid Bintang
  | location = Jakarta
  | accessdate = 26 September 2012
  | archiveurl = http://www.webcitation.org/6Ay3vIard
  | archivedate = 26 September 2012
  |date=30 October 2011
  | ref =  
  }}
*{{cite book
 |title=Profil Dunia Film Indonesia
 |trans_title=Profile of Indonesian Cinema
 |language=Indonesian
 |last=Said
 |first=Salim
 |publisher=Grafiti Pers
 |location=Jakarta
 |year=1982
 |oclc=9507803
 |ref=harv
}}
* {{cite web
  | title = Tengkorak Hidoep
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-t015-41-948428_tengkorak-hidoep
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 27 July 2012
  | archiveurl = http://www.webcitation.org/69ShUUQNo
  | archivedate = 27 July 2012
  | ref =  
  }}
 
 

 
 

 
 
 
 