Duel in the Jungle
 
 
{{Infobox film
| name           = Duel in the Jungle
| image          = Duel in the Jungle 1954 poster.jpg
| image_size     = 300px
| caption        = 1954 US Theatrical Poster
| director       = David Farrar
| writer         = Sam Marx
| narrator       = 
| starring       = Dana Andrews Jeanne Crain David Farrar
| music          = 
| cinematography = 
| editing        =  ABPC
| distributor    = 
| released       =  
| runtime        = 98 min 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £205,010 (UK) 
}} George Marshall David Farrar. 

==Synopsis==
Its plot involves an American insurance investigator who is sent to Southern Rhodesia to investigate the suspicious death of a major diamond dealer. 

==Cast==
* Dana Andrews - Scott Walters 
* Jeanne Crain - Marian Taylor  David Farrar - Perry Henderson / Arthur Henderson 
* Patrick Barr - Superintendent Roberts 
* George Coulouris - Captain Malburn 
* Charles Goldner - Martell 
* Wilfrid Hyde-White - Pitt 
* Mary Merrall - Mrs Henderson
* Heather Thatcher ...  Lady on the Niagara 
* Michael Mataka ...  Vincent  Paul Carpenter ...  Clerk 
* Delphi Lawrence ...  Pan American Girl 
* Mary Mackenzie ...  Junior Secretary 
* Bee Duffell ...  Irish Landlady 
* Alec Finter ...  Waiter 
* Patrick Parnell ...  Wireless Operator 
* John Salew ...  Clerk - Hendersons Office 
* Walter Gotell ...  Jim 
* Bill Fraser ...  Smith - Hotel Clerk 
* Lionel Ngakane ...  Servant (as Lionel MacKane) 
* Robert Sansom ...  Steward

==Production notes==
* Production Dates: 24 Aug-early Dec 1953 
* Although the copyright states that the screenplay was based on an original story by S. K. Kennedy, a July 1953 Variety article reports that screenwriters Samuel Marx and Tommy Morrison used a German novel originally published in 1942 as its source.
* Portions of the film were shot in South Africa at Port Elizabeth, Bechuanaland, Victoria Falls and Johannesburg. An October 1953 Daily Variety news item stated that scenes were shot at Krueger National Park. 
* During production, assistant director Anthony Kelly died when he was thrown from his overturned canoe into a whirlpool on the Zambesi River and then into the jaws of crocodiles
* The Hollywood Reporter review noted that after audiences at a 29 July 1954 Los Angeles preview jeered at the films ending, Warner Bros. re-edited the final scenes. The Variety review lists the running time of the British release as 105 minutes; reviews of the American version list the running time as 98 min

==References==
 

 

 
 
 
 
 
 
 
 
 
 


 
 