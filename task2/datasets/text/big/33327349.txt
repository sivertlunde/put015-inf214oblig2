Victim (1999 film)
 
 
{{Infobox film
| name           = Victim
| image          = Victim-poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Hong Kong poster for Victim
| director       = Ringo Lam
| producer       =  Li Kuo-hsing Joe Ma Ringo Lam
| starring       = Tony Leung Ka-fai Lau Ching-wan Amy Kwok Wayne Lai Collin Chou Raymond Wong
| cinematography = Ross Clarkson
| editing        = Andy Chan
| studio         = Brilliant Idea Group Ltd.
| distributor    = Mei Ah Film
| released       =  
| runtime        = 104 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$ 3,915,929
}}

Victim ( ) is a 1999 thriller film directed and co-written by Ringo Lam. The film stars Tony Leung Ka-fai, Lau Ching-wan and Amy Kwok and is about a computer programmer named Ma who is found in a haunted hotel by a cop. The programmer begins to terrify his girlfriend Amy Fu, which leads the cops to think that Ma is covering up some larger crime.

On its release in Hong Kong theaters, Victims ending was changed for 50% of the film prints due to an argument between Ringo Lam and producer Joe Ma. It was nominated for several year-end awards in Asia and was included as a Film of Merit by the 6th Hong Kong Film Critics Society Awards.

==Plot==
 
Ma (Lau Ching-wan) is kidnapped in a parking structure. His girlfriend Amy Fu (Amy Kwok) informs the police that though he had been jobless for a while and had a lot of debt, he was not a bad man. Police detective Pit (Tony Leung Ka-fai) later discovers Ma beaten, bloodied, and dangling upside down from the ceiling of an old abandoned hotel. The hotel in question is said to be haunted from murder-suicide of the original owner and his wife. On returning home, Ma begins starts terrifying Amy by behaving like the famous ghost of the hotel. The police begin to suspect that Mas possession might be a ruse to hide something other crimes that are happening.

==Release== Joe Ma.   50% of the prints released contained an extra shot in the final scene that clarified the question of whether or not the character of Ma was possessed by a ghost. The other 50% reflected the original script, which left this plot element unanswered.  The film grossed HK$ 3,915,929. 

==Reception==
Variety (magazine)|Variety gave a mixed review, stating that the film was at its best with drew "some remarkable playing from its cast" while the ghost story elements were "the weakest".    A negative review came from the San Francisco Chronicle, who referred to the film "as ridiculous as it is tepid. Only late in the second half of this almost-two-hour - way too long! - cat-and-mouse game does the film catch fire."   

===Awards and nominations===
{| class="wikitable" style="font-size:95%;" ;
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Ceremony
! Category
! Name
! Outcome
|-
|rowspan=7|1999 Golden Horse Film Festival and Awards 
|- Best Feature Film 
|
| 
|- Best Director  Ringo Lam
| 
|- Best Leading Actor Lau Ching Wan
| 
|- Best Cinematography Ross Clarkson
| 
|- Best Film Editing Chan Chi Wai
| 
|- Best Sound Effects Martin Chappel
| 
|-
|rowspan=5|19th Hong Kong Film Awards {{cite web|url=http://www.goldenhorse.org.tw/ui/index.php?class=ghac&func=archive&search_regist_year=1999&nwlist_type=award|title=
第十九屆香港電影金像獎得獎名|work=Hong Kong Film Awards|accessdate=6 October 2011}} 
|- Best Director  Ringo Lam
| 
|- Best Actor Lau Ching Wan
| 
|- Best Cinematography Ross Clarkson
| 
|- Best Sound Design Cheuk Bo Yi
| 
|-
|rowspan=2|6th Hong Kong Film Critics Society Awards {{cite web|url=http://www.filmcritics.org.hk/%E5%AD%B8%E6%9C%83%E5%A4%A7%E7%8D%8E/%E5%BE%97%E7%8D%8E%E5%90%8D%E5%96%AE/%E7%AC%AC%E5%85%AD%E5%B1%86%E9%A6%99%E6%B8%AF%E9%9B%BB%E5%BD%B1%E8%A9%95%E8%AB%96%E5%AD%B8%E6%9C%83%E5%A4%A7%E7%8D%8E|title=
第六屆香港電影評論學會大獎|work=Hong Kong Film Critics Society Awards|accessdate=6 October 2011|date=17 February 2000}} 
|- Film of Merit
|
| 
|-
|}

==Notes==
 

==See also==
 
* Hong Kong films of 1999
* List of thriller films of the 1990s

==External links==
*  
*  

 

 
 
 
 
 
 