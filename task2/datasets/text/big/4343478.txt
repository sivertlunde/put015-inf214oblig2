House of the Long Shadows
{{Infobox film
| name           = House of the Long Shadows
| image          = LongShadows.jpg
| caption        = "The screens masters of terror together for the first time." Pete Walker
| producer       = Jenny Craven Yoram Globus Menahem Golan Michael Armstrong Based on the novel Seven Keys to Baldpate by Earl Derr Biggers
| starring       = Vincent Price Christopher Lee Peter Cushing John Carradine Sheila Keith Julie Peasgood Richard Todd Desi Arnaz, Jr. Richard Harvey
| cinematography = Norman G. Langley
| editing        = Robert C. Dearberg
| distributor    = Cannon Film Distributors
| released       = 17 June 1983
| runtime        = 100 minutes
| country        = United Kingdom English
| budget         = $7,500,000 (estimated)
}} 1983 horror-parody Pete Walker. Michael Armstrong Richard Harvey. 

==Plot summary==
 Welsh manor house|manor. Upon his arrival, however, Magee discovers that the manor is not as empty as he was told. Still there are Lord Grisbane and his daughter, Victoria, who have been maintaining the mansion on their own. As the stormy night progresses, more people come to the mansion, including Lord Grisbanes sons Lionel and Sebastian, Magees publishers secretary, Mary Norton, and Corrigan, a potential buyer of the property.

After much coaxing, the Grisbanes reveal that they are here to release their brother, Roderick, who was imprisoned in his room for 40 years because he seduced a village girl when he was 14 and killed her when he found out she was pregnant. When they go to release him, they find the room empty and conclude that he broke out recently by breaking the bars in front of the window. Moments later, Lord Grisbane has a fatal heart attack. As Magee talks about getting the police, screams are heard and they find Victoria strangled to death. When Corrigan, Magee, and Mary decide to leave, they discover all of their cars have slashed tires. Soon, Diane and Andrew, a young couple who Magee met at the train station, arrive seeking shelter from the storm. They are soon killed when Diane washes her face with water that has been replaced by acid and Andrew drinks poisoned punch. The remaining five decide to find Roderick and kill him before he kills them.

Magee, Sebastian, and Mary search a series of tunnels behind a bookcase. During their search, they get separated and Sebastian is hanged to death from the ceiling. Mary makes it back to Corrigan and Lionel while Magee remains lost in the tunnels. Corrigan soon reveals that he, in fact, is Roderick and that he escaped his prison decades ago, but returns every now and then to make them think he was still trapped. He then proceeds to kill Lionel with a battle axe and chase Mary throughout the manor. Magee soon finds then them and after the ensuing fight, knocks Roderick down the stairs; during the fall, Roderick accidentally stabs himself with the axe. As Roderick is dying, his victims suddenly walk into the room, very much alive; it is revealed that all it was a joke put on by Magees publisher.

It is revealed that it was all Magees story, as he finishes his novel and returns to give it to his publisher. When his publisher gives him his $20,000 he proceeds to rip it up, as he has learned that some things are more important than money.

==Main cast==
*Vincent Price as Lionel Grisbane
*Christopher Lee as Corrigan
*Peter Cushing as Sebastian Grisbane
*Desi Arnaz, Jr. as Kenneth Magee
*John Carradine as Lord Elijah Grisbane
*Sheila Keith as Victoria Grisbane
*Julie Peasgood as Mary Norton
*Richard Todd as Sam Allyson
*Louise English as Diane Caulder 
*Richard Hunter as Andrew Caulder 
*Norman Rossington as Station Master

==Cast notes==
*Of historical importance is the fact that this film is the only co-starring effort of the four masters of terror: Vincent Price, Peter Cushing, Christopher Lee and John Carradine.
*This is also the last film in which Cushing and Lee appeared together.
*The role of Victoria Grisbane was intended for Elsa Lanchester but she declined due to ill health.
*It features the fifth and final appearance by Sheila Keith in a Pete Walker directed film.
==Production==
The film was shot at Rotherfield Park - a manor house in rural Hampshire, England. FOUR FILM GHOULS GATHER IN HOUSE OF LONG SHADOWS
Hall, William. Los Angeles Times (1923-Current File)   26 Dec 1982: m5.  

==References==
 

==External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 