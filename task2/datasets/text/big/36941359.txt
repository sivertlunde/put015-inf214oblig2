Redemption Road
{{Infobox film name = Redemption Road image = Redemption Road.jpg caption = director = Mario Van Peebles producer = Charlie Poe writer = Morgan Simpson George Richards starring = Michael Clarke Duncan Luke Perry
|released=   runtime = 95 minutes country = United States language = English distributor = Freestyle Releasing  budget = $2.3 million    gross = $29,384 
}}

Redemption Road (also Black, White and Blues) is a 2010 American film directed by Mario Van Peebles and starring Michael Clarke Duncan and Luke Perry.

== Plot ==
In Tennessee, Bailey, a debt-straddled blues guitarist, is escorted across the state by a man named Augy so that he can collect his recently deceased grandfathers estate.

== Cast ==
* Michael Clarke Duncan as Augy
* Morgan Simpson as Bailey
* Luke Perry as Boyd
* Tom Skerritt as Santa
* Taryn Manning as Jackie
* Kiele Sanchez as Hannah
* Melvin Van Peebles as Elmo

== Release ==
After it played at film festivals such as the Nashville Film Festival and Hollywood Film Festival in 2010, it received a limited theatrical release in the United States on August 26, 2011. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 57% of seven surveyed critics gave the film a positive review; the average rating was 4.5/10.   Metacritic rated it 44/100 based on five reviews.   Joe Leydon of Variety (magazine)|Variety called it an "emotionally satisfying tale about a young man in need of a mentor and an older fellow in search of forgiveness."   Duane Byrge of The Hollywood Reporter called it a "battling-buddy road movie   carries viewers on an entertaining ride into blues country."   Kimberley Jones of the Austin Chronicle rated it 1/5 stars and called it a mawkish melodrama sunk by Simpsons poor acting. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 