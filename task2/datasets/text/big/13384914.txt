Brutality (film)
 
{{Infobox film
| name           = Brutality
| image          =
| image size     =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = D. W. Griffith
| narrator       = Walter Miller
| music          =
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 33 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

Brutality is a 1912 American drama film directed by  D. W. Griffith. 

==Cast== Walter Miller - The Young Man
* Mae Marsh - The Young Woman
* Joseph Graybill - The Victim of Anger
* Lionel Barrymore - At Wedding
* Elmer Booth - In Play
* Clara T. Bracy - At Wedding/At Theatre
* William J. Butler - At Theatre Harry Carey - At Theatre John T. Dillon - At Wedding/Outside Bar
* Frank Evans - Outside Bar
* Dorothy Gish
* Lillian Gish - At Theatre
* Robert Harron
* Madge Kirby - At Theatre
* Walter P. Lewis - At Wedding
* Charles Hill Mailes - At Theatre
* Alfred Paget - Outside Bar
* Jack Pickford - At Theatre
* Gus Pixley - At Theatre
* W. C. Robinson - At Theatre
* Henry B. Walthall - In Play
* J. Waltham - At Theatre

==See also==
* List of American films of 1912
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography
* Lionel Barrymore

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 