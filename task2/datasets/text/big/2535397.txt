Left Behind: World at War
{{Infobox film
| name           = Left Behind: World at War
| image          = Leftbehind4.jpg
| director       = Craig R. Baxley
| producer       = Ron Booth Paul Lalonde Peter Lalonde Nicholas Tarbarrok André van Heerden
| writer         = Paul Lalonde Peter Lalonde André van Heerden Brad Johnson
| music          = Gary Chang David Connell
| editing        = Sonny Baskin
| studio         = 
| distributor    = Cloud Ten Pictures Sony Pictures Home Entertainment
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
}}
 thriller film Left Behind new adaption of the first book was made and released on October 3, 2014. 

== Plot ==
 
Set 18 months after the events of the second movie, an attack is taking place in Washington DC, explosions, and helicopters and machine guns firing. A man runs through a burning White House to the Oval Office. United States President Gerald Fitzhugh (Louis Gossett Jr.) videotapes his confession in the oval office of The White House. Afterwards, he looks out the window as a shadowy figure arrives in the background.
 Buck Williams, Bruce Barnes, Trib Force members escape. The guards ask Chris who he works for to which he replies "The Son of Man," they ask him again and Chris answer is "God Almighty," at which point the guard executes him.

At Washington, D.C., somewhere at a rural lake, the president and his vice-president, John Mallory, are taking some time away from the White House. On the way back, Mallory informs Fitzhugh of Nicolaes plans and how he has found evidence that Nicolae is planning an attack on American soil. Before he shares the information, a car explodes behind them. Troops storm in attempting to kill the President. A soldier fires a rocket-propelled grenade and Mallory dies from the ambush attack, but a militia group comes to the aid of the President.
 Amanda White, the newest member who once knew Rayfords first wife before the vanishings. After the ceremony is over, Buck heads over to D.C. and Rayford flies to New Babylon. Nicolae meets with Fitzhugh, who expresses his deepest concern over the news of Mallorys death, and teams up with Carolyn Miller (Jessica Steen), who poses as Nicolaes top aide at GC headquarters. Together, they find Nicolaes secret plan of stealing Bibles and lacing them with anthrax before distributing them. The GC block their escape, with Fitzhugh killing one of the guards in the process.

Fitzhugh is recruited by Millers militia group to help take out Nicolae. Along with the Prime Minister of Great Britain and the President of Egypt, they plan an attack on GC forces in their respective countries. Fitzhugh also argues that they must take out Carpathia, which he states he will do personally. Fitzhugh enters the GC building and asks to see the president (it is also revealed that one of the GC guards is really an insider, but is shot by another). Fitzhugh enters Nicolaes office, but Nicolae was already aware of Fitzhughs assassination attempt and foils it. Fitzhugh tries to shoot Nicolae with three rounds, but he isnt affected as they go through him and hit a guard instead. Nicolae turns on all of the TVs in his office, revealing that World War III has begun, with the GC military attacking British, Egyptian, and American forces. Outside in the background of Nicolaes office, air raid sirens are blaring, explosions rise into the sky, and machine guns and tank blasts are heard. Using supernatural techniques, Nicolae throws Fitzhugh out of a 20-story window, who lands on top of a car. Nicolae goes over to the window to see, in disgust, Fitzhugh getting up and walking away. Nicolae then looks to the sky and says, "Thats not humanly possible, is it?" Everyone is shocked when Fitzhugh returns to the militia base and informs them that the plan failed: Carolyn takes this the hardest.

The underground Trib Force HQ is hit as World War III approaches. Bruce and Chloe are infected with the virulent bacteria, but in the end, it is Chloe who survives when red wine, used in the communion they just took part of, is revealed to be the antidote. Buck meets Fitzhugh in a destroyed White House, where he helps the president become a Christian. Fitzhugh then confronts Nicolae, where he activates a personal transmitter (and dials Carolyns cell phone, where she hears the entire conversation), hoping to obliterate the entire GC headquarters, and himself, with a missile locked onto the transmitters location. The missile hits the GC building and Fitzhugh is killed in the explosion, as the building collapses and dust flies from the bottom.

Buck Williams then gets a call in the elevator from Chloe as she tells him about the wine and Bruces death. Buck promises to come home from his trip soon, as the elevator stops and the door opens to reveal an armed Carolyn. She lowers her weapon and Buck states "we need to talk," implying that they had met before. Before the credits, whats left of the Global Community Building is burning down, police sirens are wailing in the background, and explosions are still going on. Nicolae Carpathia walks out from the flames looking very mad, completely unharmed.

== Production ==
Filming took place in Toronto, Ontario during 2005. Clarence Gilyard Jr. was originally signed to reprise his role of Bruce Barnes, but couldnt return due to a scheduling conflict. As a devout Catholic he has also admitted his priest was happy he didnt reprise the role, as the films portray a premillennial rapturist theology that is in opposition to the Catholic Churchs amillennial teachings.

== Differences from the novel ==
The final two chapters of the book Tribulation Force and the first four or five chapters of Nicolae Carpathia|Nicolae have events that match up to what is shown in World at War. Recognizable events were the marriages of Buck with Chloe and Rayford with Amanda, the death of Bruce Barnes, and the President of the United States|U.S. President heading an attack with Britain and Egypt, against the Global Community.

Major parts of the movie, however, were not in any of the books: the poisoning of Bibles by the forces of Nicolae, and an attempt by Fitzhugh to assassinate Nicolae. Bucks meeting with the president in the books makes it into the movie, but in a totally different form. Also, in the movie Nicolae Carpathia, the main antagonist, is portrayed almost as a supernatural being, displaying supernatural powers against his enemies; this is most clearly shown at the end, where Carpathia walks out completely undamaged from the wreckage of a building that was literally blown into pieces.

== Cast ==
* Louis Gossett Jr.: President Gerald Fitzhugh
*  
*  
* Jessica Steen: Carolyn Miller
*  
*  
* Chelsea Noble: Hattie Durham
* Arnold Pinnock: Bruce Barnes
* Charles Martin Smith: Vice President John Mallory David Eisner: Chief of Staff Allan Campbell
* Richard Fitzpatrick: Major Kent

== Reception ==
The film received very little critical attention, but it proved popular with its target demographic. Rotten Tomatoes reports only two reviews from critics, which is not enough data for a score. Both critical reviews were negative, but 73% percent of the ratings by Rotten Tomatoes users were positive. One review by the website Christianity Today gave the film two out of four stars. Todd Hertz from Christianity Today said, "The mere existence of cheese in a Left Behind film comes as no surprise to some movie fans. However, excluding a few goofy moments, Left Behind: World at War plays out as an average TV drama. Many times, I caught myself thinking of it as 24—with less action and more prayer time. Theres impressive acting, some intrigue, mostly decent special effects, and good themes for Christian discussion". 

A game based on this film was released by Inspired Media Entertainment (formerly Left Behind games). 

== MPAA ==
Left Behind: World at War was given a PG-13 rating for violence by the Motion Picture Association of America|MPAA. It is considerably darker and more intense than the first two films. It is also the most violent of the three.

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*   

 
 

 
 
 
 
 
 
 
 
 
 