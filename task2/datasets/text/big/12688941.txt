Bye Bye Africa
{{Infobox Film | name = Bye Bye Africa
 | image          = ByeByeAfrica.jpg
 | image_size     =
 | caption        = Screenshot
 | director = Mahamat Saleh Haroun
 | writer = Mahamat Saleh Haroun
 | starring = Mahamat Saleh Haroun Garba Issa Aïcha Yelena Abakar Mahamat-Saleh
 | sound = Ousmane Bougoudi
 | music = Al-hadj Ahmat dit Pecos, Issa Bongo, Ringo Efoua-Ela
 | cinematography = Stephane Legoux, Mahamat Saleh Haroun
 | editing = Sarah Taouss Matton
 | distributor = California Newsreel (USA) Les Histoires Weba (France)
 | Country = France / Chad
 | released = 20 May 2002 (Cannes Film Festival premiere)
 | runtime = 86 minutes French
 | budget = ~USD100,000   by David Walsh, World Socialist Website, 28 September 2000 
 }} 1999 award winning Chadian film. It was the first by Chadian director Mahamat Saleh Haroun, who also starred. The docu-drama centers on a fictionalized version of Haroun.

== Plot ==
A Chadian film director who lives and works in   victim. Haroun learns about the destruction of the African cinema from directors in neighboring countries, but also finds Issa Serge Coelo shooting his first film, Daressalam. Things go badly and, convinced that it is impossible to make films in Africa, Haroun departs Chad in despair, leaving his film camera to a young boy who had been assisting him.

==Awards==
The film won the following awards: 
* 1999 Amiens International Film Festival: Special Mention in the category Best Feature Film
* 2000   (1999))
* 1999 Venice Film Festival:CinemAvvenire Award in the category Best First Film, Luigi De Laurentiis Award - Special Mention

== Notes and references ==
 

== External links ==
*  
*  

 
 
 
 


 