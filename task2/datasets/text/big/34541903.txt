Ice Age: A Mammoth Christmas
{{Infobox film
| name           = Ice Age: A Mammoth Christmas
| image          = Ice Age - A Mammoth Christmas poster.jpg
| caption        = DVD cover
| director       = Karen Disher
| producer       = Lori Forte	
| writer         =   
| starring       = Ray Romano   John Leguizamo   Denis Leary   Chris Wedge   Queen Latifah   Seann William Scott    Josh Peck   Ciara Bravo   Billy Gardell   T. J. Miller   Judah Friedlander
| music          = John Paesano
| cinematography = 
| editing        = 
| studio         = Blue Sky Studios
| distributor    = 20th Century Fox
| released       =  
| runtime        = 26 minutes
| country        = United States English
}} Fox and it was released two days later to DVD and Blu-ray.

Despite being produced by Blue Sky Studios, the films animation was actually done by some animators at Blue Sky and mainly by the Santa Monica/Texas-based special effects and animation company, Reel FX Creative Studios.

==Plot==
Christmas has come and Scrat eyes a beaver assembling a cache of foods, among them an acorn: Scrat sneaks in and takes the acorn, along with others he finds in the area set up as decorations: he ties a large stockpile of them on a piece of bark and tries to leave with them, but they break out and leave him with nothing. Meanwhile, Manny brings the Christmas Rock out of storage, to Ellies delight: the rock is intended as a surprise for Peaches, who comes sliding down a snow slope in a snowball fight with Crash and Eddie. Manny reveals that the Christmas Rock is the same one from his childhood, a family heirloom, and important as it lets Santa Claus find them to deliver presents.

Diego and Sid step in to see the Christmas Rock, with Sid deriding it. Manny will not let Sid near the Rock and Sid, to find his own means of decorations, decides to find a different decoration and chooses a tree. Crash and Eddie help him to decorate the tree with insects, small animals, and fish bones: to top the tree, Sid puts a star-shaped piece of ice on the top, which is accidentally flung off and hits the Christmas Rock, shattering it. Manny, furious at Sid, declares that Sid is on the "Naughty List". Manny dismisses the idea of Santa to Ellie, which Peaches overhears, and the young mammoth is shocked that Manny does not believe in Santa. Sid, in tears that he destroyed the rock, slides downhill as his tears freeze solid.

Scrat finds an acorn half frozen in a pond and steps onto the slippery surface to pull it out. After pulling it out successfully, Scrat begins to ice skate with the acorn, going through a log, where he misplaces the acorn and instead mistakenly holds onto a spider, which attacks him. Meanwhile, Sid, feet still frozen, sulks about being on the Naughty List until Peaches calls on him to snap out of it: she intends to head to the North Pole with Sid, Crash, and Eddie so as to convince Santa Claus to take Sid off the Naughty List, along with Crash and Eddie, who, despite their misdeeds, still want Christmas.

The four set off to the North Pole by following the Northern Lights, and move on until they reach a whiteout, which separates them for a moment until they find one another and move on, led by Sid, who mistakenly leads them off a cliff. As they fall, they are rescued by a flying reindeer, who takes them to the other side of the cliff and introduces himself as Santa Clauss reindeer|Prancer. Sid thanks the reindeer and moves on before he nearly falls down the cliff again and is caught by Peaches, who decides that the reindeer will go with them. Back at the village, however, Manny attempts to patch up the Christmas Rock with mud and sticks, to poor effect, when Ellie comes along, calling out that she cannot find Peaches, Sid, Crash, or Eddie. Diego states that the four of them headed out to the North Pole to find Santa; with that, Manny and Ellie move on to find Peaches and the others, led by Diego, who tracks them by, reluctantly, picking up Sids scent.

==Voice cast==
  Manny
* Sid
* Diego
* Crash
* Eddie
* Ellie
* Peaches
* Scrat
* Billy Gardell as Santa Claus Prancer
* Judah Friedlander as Head Sid (Ice Age)#Mini Sloths 2|Mini-Sloth

==Release== Annie Award for Best Animated Special Production. 

===Home media===
Ice Age: A Mammoth Christmas was released on DVD and Blu-ray on November 26, 2011. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 