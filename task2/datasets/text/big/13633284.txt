Cold Water (film)
{{Infobox film
| name           = Cold Water (Leau froide)
| image          = LEAU_FROIDE.jpg
| image_size     = 
| caption        = 
| director       = Olivier Assayas
| producer       = Georges Benayoun
| writer         = Olivier Assayas
| narrator       = 
| starring       = Virginie Ledoyen Cyprien Fouquet Jackie Berroyer
| music          = 
| cinematography = Denis Lenoir
| editing        = Luc Barnier
| distributor    = 
| released       = 6 July 1994
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $185,917 
}} 1994 cinema French film written and directed by Olivier Assayas. It is about two troubled teenagers in France during the early 1970s. The film was screened in the Un Certain Regard section at the 1994 Cannes Film Festival.   

In October, 2007, it was screened at the Pacific Film Archive in Berkeley, California, where Assayas spent a week in residence introducing and discussing his films with the audiences there. He was accompanied by his friend Jean-Michel Frodon, the current director of the journal Cahiers du cinéma, of which Assayas was once the director. Assayas said the film is autobiographical, reflecting upon his own teenage years. He went out of his way to cast actors who were not professionals or who, as in the case of male lead Cyprien Fouquet (playing Gilles), had never acted before. However, female lead Virginie Ledoyen (playing Christine) had some film experience. The film was the result of a commission from French TV, which wanted a 52-minute film but allowed Assayas to make a longer version, after some negotiations.

==Plot==
Gilles and Christine are attractive, sexually active teenagers from unstable homes.  When they do some shop-lifting together she is arrested, but he gets away. She escapes from a mental institution and meets him at an abandoned house in the country, where a large group of rebellious teenagers are having a wild, all-night party. American rock music from the period is played prominently and has a very strong effect, especially "Me and Bobby McGee," sung by Janis Joplin. Drugs are used, mainly pot and hash. As the party is winding down, Gilles and Christine escape even deeper into the countryside, searching for a commune where artists are said to live without electricity or running water. This is Christines idea, but Gilles reluctantly goes along. A jolting conclusion shows us that, as Assayas puts it, "Gilles real life has now begun."

==Cast==
* Virginie Ledoyen - Christine
* Cyprien Fouquet - Gilles
* László Szabó (actor)|László Szabó - Gilless father
* Jean-Pierre Darroussin - Inspector
* Jackie Berroyer - Christines father
* Dominique Faysse - Christines mother
* Smaïl Mekki - Mourad
* Jean-Christophe Bouvet - Professor
* Ilona Györi - Marie
* Renée Amzallag - The nurse
* Jérôme Simonin - Vendeur de dynamite
* Laetitia Lemerle - Corrine
* Alexandra Yonnet - Copine blouson
* Caroline Doron - Copine blessée
* Laetitia Giraud - Christiane

==References==
 

==External links==
* 

 

 
 
 
 
 