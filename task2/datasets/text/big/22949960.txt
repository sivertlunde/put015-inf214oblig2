Doshaburi (film)
{{Infobox film
| name           = Doshaburi
| film name      = 土砂降り
| image          =
| caption        = Japanese movie poster
| director       = Noboru Nakamura
| producer       = Shochiku
| writer         =
| starring       = Keiji Sada
| music          = Toru Takemitsu
| cinematography = 
| editing        = 
| distributor    = Shochiku
| released       = June 11, 1957
| runtime        = 105 minutes
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}} Japanese film drama directed by Noboru Nakamura. It was announced that the film will screen as When It Rains, It Pours at the Tokyo Filmex in 2013. 

== Cast ==
* Keiji Sada
* Mariko Okada
* Miyuki Kuwano
* Sadako Sawamura
* So Yamamura

== References ==
 

== External links ==
*   at Shochiku

 

 
 
 
 
 
 
 

 
 