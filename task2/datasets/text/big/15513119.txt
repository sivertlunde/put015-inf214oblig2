Who Loves the Sun
 
 
{{Infobox film
| name           = Who Loves the Sun
| image          = Who Loves the Sun poster.jpg
| caption        = 
| director       = Matt Bissonnette
| producer       = Corey Marr Brendon Sawatzky
| writer         = Matt Bissonnette Adam Scott R.H. Thomson Wendy Crewson
| music          = Mac McCaughan
| cinematography = Arthur E. Cooper
| editing        = Michele Conroy
| distributor    = Christal Films
Axiom Films  
| released       =  
| runtime        = 94 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Who Loves the Sun is a 2006 Canadian film directed and written by Matt Bissonnette.

== Plot == Adam Scott) were best friends. Daniel was Will’s best man at his wedding to Maggie Claire (Molly Parker). Then one day Will disappeared without a word. Five years later, he re-surfaces.
When Will and Daniel meet again, they go together to the docks to pick up Maggie, who slaps Will the minute she sees him. 
It appears that five years ago, Will came into the room where Daniel and Maggie were having an affair, which Maggie claimed later that it was a one time quickie. She was upset for the sudden disappearance of Will, five years ago, and demands an explanation. Will ends up apologizing for that. Daniel kisses and tries to renew his affair with Maggie, to whom he kept writing throughout the past five years with no response from her. Maggie refuses his move and tells him that their affair was a great mistake.

The film was shot in the Canadian Shield.

== Awards ==
 
* 2006 AFI Fest - Grand Jury Prize - Nominated - Matt Bissonnette
* 2007 WorldFest Houston - Bronze Award for Best Film - Won
* 2007 Beverly Hills Film Festival for Best Female Performance - Molly Parker - Won 
* 2007 Genie Award for Best Performance by an Actress in a Leading Role - Nominated - Molly Parker
* 2008 Mississauga Independent Film Festival - Best Feature Film - Won
* 2011 Film North – Huntsville International Film Festival - Film North Best Feature Award - Won

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 