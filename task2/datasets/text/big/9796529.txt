El Haragán de la familia
 

{{Infobox film
| name           =El Haragán de la familia
| image          =
| image_size     =
| caption        =
| director       = Luis Cesar Amadori
| writer         = Luis Cesar Amadori with Ivo Pelay
| starring       = Pepe Arias
| music          =
| editor         =   Nicolas Proserpio
| distributor    =
| released       = 1940
| runtime        = 87 minutes
| country        = Argentina Spanish
| budget         =
}}

El Haragán de la familia  (The lazy one in the family) is a black and white Argentine film that premiere on February 21, 1940.

==Cast==
* Pepe Arias
* Amelia Bence
* Ernesto Raquén
* Gloria Bayardo
* Mirtha Rey
* Alfredo Fornaserio
* Justo José Caraballo
* Angelina Pagano
* Juan José Piñeiro
* Elvira Quiroga
* José A. Paonessa
* Miguel Coiro

Directed by:
* Luis César Amadori
Screenplay by:
* Luis César Amadori based on the storyline by Ivo Pelay
Photography:
* Hugo Chiesa
Music
* Mario Maurano
Editing
* Nicolas Proserpio
Set Design
*  Raúl Soldi 

==External links==
*  

===References===
 

 
 
 
 
 


 