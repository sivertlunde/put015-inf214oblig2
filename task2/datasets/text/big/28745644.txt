Scene of the Crime (1949 film)
{{Infobox film
| name           = Scene of the Crime
| image_size     =
| alt            =
| image	         = Scene of the Crime FilmPoster.jpeg
| caption        = Theatrical release poster Roy Rowland
| producer       = Harry Rapf
| screenplay     = Charles Schnee
| based on       =  
| narrator       =
| starring       = Van Johnson Arlene Dahl Gloria DeHaven
| music          = André Previn
| cinematography = Paul Vogel
| editing        = Robert Kern
| studio         = Metro-Goldwyn-Mayer
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $761,000  . 
| gross          = $1,391,000 
}} Roy Rowland, Leon Ames. 

==Plot== LAPD homicide detective, investigates when a former partner is found murdered and carrying $1,000 in cash.

Out to dispel a theory that the dead cop was secretly in cahoots with crooks, Conovans trail leads to a stripper, Lili (Gloria DeHaven), whose ex-boyfriend Turk (Richard Benedict) has apparently pulled off a robbery with a man named Lafe (William Haade).  Conovan tracks down Lafe and places him under arrest, but just outside the police station, gunshots ring out, killing Lafe and wounding the detective. Conovan is convinced by his wife Gloria (Arlene Dahl) that police work is too dangerous. He agrees and tenders his resignation.

Lili calls headquarters with a tip for Conovan on where Turk can be found. Detective Fred Piper (John McIntire) intercepts the message, investigates it himself and is gunned down.  Conovan concludes that Lili has been double-crossing him, secretly helping Turk all along. Over the objections of his wife, he gets his old job back with the police force.  Turk attempts to pull off an armed robbery, but Conovan gives chase. His vehicle crashes into Turks, causing it to catch fire. Turk confesses to the murders before he dies.

==Cast==
 
 
* Van Johnson as Mike Conovan
* Arlene Dahl as Gloria Conovan
* Gloria DeHaven as Lili
* Tom Drake as Detective C.C. Gordon Leon Ames as Captain A.C. Forster
* John McIntire as Detective Fred Piper Donald Woods as Bob Herkimer Norman Lloyd as Sleeper
* Jerome Cowan as Arthur Webson
 
* Tom Powers as Umpire Menafoe
* Richard Benedict as Turk Kingby Anthony Caruso as Tony Rutzo
* Robert Gist as P.J. Pontiac
* Romo Vincent as Hippo
* Tom Helmore as Norrie Lorfield
* Caleb Peterson as Loomis
* William Haade as Lafe Douque
 

==Background==
According to film critic Dennis Schwartz, the film is one of "few film noirs attempted by MGM. It came when Dore Schary was the studio head and insisted on producing more realistic films. This is a transitional film from the 1930s gangster film and a forerunner of the modern day TV cop show. It preaches the credo that Crime Does Not Pay." 

==Reception==
According to MGM records the film earned $968,000 in the US and Canada and $423,000 overseas resulting in a profit of $151,000. 

===Critical response=== Dragnet style, following ordinary police procedures in solving the case. The film had a violent conclusion, which underscores the dangers of being an urban cop. It portrayed the hard-working policemen in a sympathetic light and showed how they are often misunderstood by the public and betrayed at times by reporters who are eager to grab the headlines and run with them even though they dont have all the facts. Mike comes out as a good cop, but is disillusioned by his low pay and all the pressures from home, the job and its politics, and from an unappreciative public." 

===Awards===
Nominations Edgar Allan Poe Awards: Edgar - Best Motion Picture; 1950.

==References==
 

==External links==
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 
 
 
 