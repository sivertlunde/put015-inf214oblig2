The Quiet Storm (film)
 
{{Infobox film
| name           = The Quiet Storm
| image          = 
| caption        = 
| director       = Guðný Halldórsdóttir
| producer       = Halldór Þorgeirsson
| writer         = Guðný Halldórsdóttir
| starring       = Tinna Hrafnsdóttir
| music          = 
| cinematography = Svein Krøvel
| editing        = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 
}}

The Quiet Storm ( ) is a 2007 Icelandic drama film written and directed by Guðný Halldórsdóttir. It was entered into the 30th Moscow International Film Festival.   

==Cast==
* Tinna Hrafnsdóttir
* Hilmir Snær Guðnason
* Hera Hilmar (as Hera Hilmarsdóttir)
* Atli Rafn Sigurðsson
* Jörundur Ragnarsson
* Gunnur Martinsdóttir Schlüter as Eyja
* Baltasar Breki Baltasarsson as Diddi
* Arnmundur Ernst Björnsson as Otti
* Þorsteinn Bachmann as Keli
* Thórey Sigthórsdóttir as Dísa eldri

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 