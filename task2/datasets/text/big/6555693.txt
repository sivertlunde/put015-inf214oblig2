Carry On Emmannuelle
 
 
{{Infobox film
| name = Carry On Emmannuelle
| image = Carry On Emmanuelle.jpg
| caption = "Carry On Emmannuelle" Theatrical poster
| director = Gerald Thomas
| producer = Peter Rogers
| writer = Lance Peters Jack Douglas Peter Butterworth Beryl Reid Suzanne Danielle Eric Rogers
| cinematography = Alan Hume
| editing = Peter Boita Hemdale
| released = November 1978
| runtime = 88 minutes
| language = English
| country = United Kingdom
| budget = £320,000
}}
 the series Jack Douglas AA by British Board of Film Censors. This restricted audiences to those aged fourteen and over.

==Plot== Jack Douglas). He removes her coat only to find she has left her dress on the plane!  The chauffeur, Leyland (Kenneth Connor), housekeeper, Mrs Dangle (Joan Sims) and aged boot-boy, Richmond (Peter Butterworth) sense saucy times ahead ... and they are right!  Emile is dedicated to his bodybuilding, leaving a sexually frustrated Emmannuelle to find pleasure with everyone from the Lord Chief Justice (Llewellyn Rees) to chat show host, Harold Hump (Henry McGee). Theodore is spurned by Emmannuelle, who has genuinely forgotten their airborne encounter and despite reassurances from his mother (Beryl Reid), Theodore exacts revenge by revealing Emmannuelles antics to the Press. However, after a visit to her doctor (Albert Moses), she discovers she is pregnant and decides to settle down to a faithful marriage with Emile ... and dozens of children.

==Cast==
*Kenneth Williams as Emile Prevert
*Suzanne Danielle as Emmannuelle Prevert
*Kenneth Connor as Leyland Jack Douglas as Lyons
*Joan Sims as Mrs Dangle
*Peter Butterworth as Richmond
*Larry Dann as Theodore Valentine
*Beryl Reid as Mrs Valentine
*Tricia Newby as Nurse in surgery
*Albert Moses as Doctor
*Henry McGee as Harold Hump Howard Nelson as Harry Hernia
*Claire Davenport as Blonde in pub
*Tim Brinton as BBC newscaster
*Corbett Woodall as ITN newscaster
*Robert Dorning as Prime Minister
*Bruce Boa as US Ambassador
*Eric Barker as Ancient General
*Victor Maddern as Man in launderette
*Norman Mitchell as Drunken husband
*Jack Lynn as Admiral
*Michael Nightingale as Police Commissioner
*Llewellyn Rees as Lord Chief Justice
*Steve Plytas as Arabian official
*Joan Benham as Cynical lady
*Marianne Maskell as Nurse in hospital
*Louise Burton as Girl at zoo
*Dino Shafeek as Immigration officer
*David Hart as Customs officer
*Gertan Klauber as German soldier
*Malcolm Johns as Sentry
*John Carlin as French parson
*Guy Ward as Dandy
*James Fagan as Concorde steward
*John Hallet as Substitute football player
*Deborah Brayshaw as French buxom blonde
*Suzanna East as Colette
*Bruce Wylie as Football referee
*Philip Clifton as Injured footballer
*Stanley McGeagh as Fleet Street journalist
*Bill Hutchinson as 1st reporter
*Neville Ware as 2nd reporter
*Jane Norman as 3rd reporter
*Nick White as Sent-off footballer

==Crew==
*Screenplay – Lance Peters Eric Rogers
*Song – Kenny Lynch
*Performers – Masterplan
*Director of Photography – Alan Hume
*Editor – Peter Boita
*Art Director – Jack Sampan
*Production Manager – Roy Goddard
*Camera Operator – Godfrey Godar
*Make-up – Robin Grantham
*Production Executive for Cleves – Donald Langdon
*Assistant Director – Gregory Dark
*Sound Recordists – Danny Daniel & Otto Snel
*Continuity – Marjorie Lavelly
*Wardrobe – Margaret Lewin
*Stills Cameraman – Ken Bray
*Hairdresser – Betty Sherriff
*Costume Designer – Courtenay Elliott
*Set Dresser – John Hoesli
*Assistant Editor – Jack Gardner
*Dubbing Editor – Peter Best
*Titles & Opticals – GSE Ltd
*Processor – Technicolor Ltd
*Producer – Peter Rogers
*Director – Gerald Thomas

==Filming and locations==

*Filming dates – 10 April-15 May 1978

Interiors:
* Pinewood Studios, Buckinghamshire

Exteriors:
* Wembley, London
* Trafalgar Square, London
* Oxford Street, London
* London Zoo, London

==Critical reception==
Philip French took a very negative review of Carry On Emmannuelle: "This relentless sequence of badly-written, badly-timed dirty jokes is surely one of the most morally and aesthetically offensive pictures to emerge from a British studio."   Christopher Tookey considered the film to be "Embarrassingly feeble". 

==Bibliography==
* 
* 
* 
* 
*Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (fourth edition) (2011) (Titan Books)
* 
* 
* 
* 
* 

==References==
 
* Robert Ross The Carry On Companion, Batsford Books, 1996

* Simon Sheridan Keeping the British End Up: Four Decades of Saucy Cinema, Titan Books, 2011 (fourth edition)

== External links ==
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 