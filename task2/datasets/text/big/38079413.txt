Bolibar
{{Infobox film
| name           = Bolibar
| image          =
| caption        =
| director       = Walter Summers
| producer       = H. Bruce Woolfe
| writer         = Leo Perutz (novel)   Walter Summers Michael Hogan   Hubert Carter   Carl Harbord
| music          =
| cinematography = Jack Parker
| editing        = Adrian Brunel   Ivor Montagu
| studio         = British Instructional Films 
| distributor    = New Era Films
| released       = 26 July 1928
| runtime        = 8,000 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Michael Hogan and Carl Harbord. It was based on the 1920 novel The Marquis of Bolibar by Leo Perutz. It was made by British Instructional Films at Cricklewood Studios.

==Cast==
* Elissa Landi as Françoise-Marie / La Monita  Michael Hogan as Lt. Donop 
* Hubert Carter as Col. Bellay 
* Carl Harbord as Lt. Gunther 
* Jerrold Robertshaw as The Marquise of Bolibar 
* Cecil Barry as Dapt. Egolstein 
* Evelyn Roberts as Captain Brockendorf 
* Gerald Pring as Captain OCallaghan 
* Charles Emerald as Colonel 
* Hector Abbas as Artist 

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 