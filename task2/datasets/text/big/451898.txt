The Hot Spot
{{Infobox film
| name           = The Hot Spot
| image          = The Hot Spot.jpg
| image_size     =
| caption        = theatrical poster
| director       = Dennis Hopper Paul Lewis
| executive producers =Bill Gavin Derek Power Steve Ujlaki  Charles Williams   (Novel & Screenplay)  Nona Tyson   (Screenplay) 
| narrator       =
| starring       = Don Johnson Virginia Madsen Jennifer Connelly
| music          = Jack Nitzsche
| cinematography = Ueli Steiger
| editing        = Wende Phifer Mate
| distributor    = Orion Pictures
| released       =  
| runtime        = 130 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,293,976
}} Charles Williams. Taj Mahal, Roy Rogers and drummer Earl Palmer.

==Plot==
Drifter Harry Madox takes a job as a used car salesman in a small Texas town. In the summer heat, he develops an interest in two women: Gloria Harper, who works at the car dealership, and Dolly Hershaw, who is married to the dealerships owner. Harry carries on with both of them while looking for an opportunity to rob the local bank. 

When he robs the bank, he sets a fire to cover his tracks, but has to go back into the burning building to save a man who is trapped inside. The towns sheriff suspects Harry, but Dolly gives him an alibi and tells him she will sell him out unless he kills her husband. When he refuses, Dolly threatens to expose him. She ultimately kills her husband herself by overstimulating his weak heart during sex. 

Meanwhile, Gloria is being blackmailed by con artist Frank Sutton, who has nude photographs of her with another woman. Harry, who has fallen in love with Gloria, confronts Sutton to get the pictures, and kills him in the ensuing struggle. Harry invents a story to draw suspicion away from him, and receives a hefty reward from the police. He plans to marry Gloria and take her to the Caribbean Islands.

However, Dolly ruins Harrys plans by showing him a letter implicating him in the robbery and Suttons death, and telling Gloria about the affair. A heartbroken Gloria leaves Harry. Enraged, Harry tries to strangle Dolly, but cannot bring himself to kill her. Harry resigns himself to life with Dolly, and leaves town with her.

==Cast==

* Don Johnson as Harry Madox
* Virginia Madsen as Dolly Harshaw
* Jennifer Connelly as Gloria Harper
* Jerry Hardin as George Harshaw William Sadler as Frank Sutton
* Charles Martin Smith as Lon Gulick
* Barry Corbin as the Sheriff
* Leon Rippy as Deputy Tate
* Jack Nance as Julian Ward
* Virgil Frye as Deputy Buck

==Production== Last Tango in Texas. Real hot, steamy stuff".   

A bedroom scene originally called for Madsen to appear naked, but she decided to put on a negligee because she felt that, "Not only was the nudity weak storywise, but it didnt let the audience undress her".    Hopper later admitted that Madsen was right. 

The director gave his impressions of working with Johnson:
 He wasnt that bad. He has a lot of people with him. He came on to this film with two bodyguards, a cook, a trainer, ah lets see, a helicopter pilot he comes to and from the set in a helicopter, very glamorous lets see, two drivers, a secretary, and, oh yes, his own hair person, his own make-up person, his own wardrobe person. So when he walks to the set he has five people with him.    
Johnson found Hoppers approach to filmmaking "a little disappointing, I gotta tell you".     He later recalled:
 Mike Figgis had written a script called The Hot Spot, and it was a heist movie. Three days before we started shooting, Dennis Hopper came to all of us, he called a meeting on a Sunday, and he said, “Okay, we’re not making that script. We’re making this one.” And he passed a script around the table that had been written for Robert Mitchum in the ’60s… or maybe it was the ’50s… and it was based on a book called Hell Hath No Fury. And that was the movie that we ended up making. This was three days before we started shooting! So he was kind of looking around the table at everybody and saying, “Well, you know, if Don Johnson bails, we don’t have a movie.”   And I read the script, and I said, “Wow!” I mean, the Figgis script was really slick and cool, and it was a heist movie, but this was real noir, the guy was an amoral drifter, and it was all about how women were going to take him down.  
Hopper shot the film on location in Texas during what he described as the "hottest, steamiest weather you could imagine". The primary locations were in Taylor, Texas, especially its iconic downtown area,  locations around and in Austin as well as in Luling, Texas|Luling.  The swimming scenes were filmed at the famous Hamilton Pool Preserve west of Austin.   

==Reception==
 
The Hot Spot had its world premiere at the 1990 Toronto Film Festival.    Director Dennis Hopper felt that stars Don Johnson and Virginia Madsen were not as enthusiastic in promoting the film as he would have liked. Hopper said of Johnson that "He says hes not going to do anything for this picture until he reads the reviews."    Johnson claims that he was unable to do promotion because he was shooting the film Harley Davidson and the Marlboro Man with Mickey Rourke.  Of Madsen, Hopper claimed that she "was very embarrassed" by the amount of her nudity in film|on-screen nudity.    The film was released on October 12, 1990, in 23 theaters, grossing USD $112,188 in its opening weekend. The film grossed only $1.2 million in the North America, far less than the cost of its production.   
 stars and B movies RKO and Republic will recognize The Hot Spot as a superior work in an old tradition."    In her review for The New York Times, Janet Maslin wrote, "Mr. Hoppers direction is tough and stylish, in effective contrast with the sunny look of Ueli Steigers cinematography."    USA Today gave the film two-and-a-half stars out of four and wrote, "In other words, Hoppers direction isnt any great shakes, and the wrap-up is somewhat confusing, but this film does make you want to go nude swimming|skinny-dipping with someone elses mate."    In his review for the Washington Post, Desson Howe wrote, "Hot Spot will never go down as timeless, neoclassic film noir|noir. But, with its Hopperlike moments, over-the-top performances and infectious music, it carries you along for a spell."    Entertainment Weekly gave the film a "B" rating and Owen Gleiberman wrote, "Hopper still hasnt learned how to pace a movie, but working from Charles Williams 1952 novel Hell Hath No Fury he comes up with a reasonably diverting hothouse yarn."   

==Home media== Killing Me Softly. 

==Soundtrack== Taj Mahal Roy Rogers. Allmusic describes the soundtrack album as "marvelous music ... something listeners should be thankful for, particularly fans of either Miles Davis or John Lee Hooker". 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 