Inkaar (2013 film)
 
 

{{Infobox film
| name           = Inkaar
| image          = Inkaarmovieposter.jpg
| caption        = Theatrical release poster
| director       = Sudhir Mishra
| producer       = Viacom18 Motion Pictures
| writer         = Sudhir Mishra Manoj Tyagi
| starring       = Arjun Rampal Chitrangada Singh
| music          = Shantanu Moitra
| cinematography = Sachin Krishn
| editing        = 
| distributor    = Viacom 18 Motion Pictures Tipping Point Films
| released       =  
| runtime        = 133 mins
| country        = India
| language       = Hindi
| budget         =  
| gross          =    
}}
 drama Romance romance film directed by Sudhir Mishra and produced by Viacom18 Motion Pictures, starring Arjun Rampal and Chitrangada Singh in the lead roles. Shantanu Moitra has composed the music. Deepti Naval, Vipin Sharma, Gaurav Dwivedi are featured in supporting roles. The film was produced and distributed under the Viacom 18 Motion Pictures and the Tipping Point Films banners. The film deals with sexual harassment in a corporate set-up following the story of Rahul Verma, the CEO of an advertising agency, who has to follow the lawsuit of sexual harassment filed by Maya Luthra, his protégé. The two are battling for the top job of the company. A committee is set up by the agency to hear both sides of the story and work through lies and accusations to find out the truth.   

The development of the film commenced in 2011. The movie was completed in early 2012 and was titled "Inkaar". It was scheduled to release in May but conflicts started rising over the title of the movie which delayed its release. After a lot of options, "Kaam" was chosen as the title but ultimately "Inkaar" was finalised to be the title. 

After the title conflict was resolved, the release date of the movie was delayed to 2013. The movie was released on 18 January 2013. The film received positive to mixed responses but the performances of the lead pair and the direction of Sudhir Mishra earned positive reviews from critics. The movie and grossed   in the first week.   

==Cast==
*Arjun Rampal as Rahul Verma
*Chitrangada Singh as Maya Luthra
*Deepti Naval as Mrs. Kamdhar
*Kanwaljeet Singh as Rahuls father
*Rehana Sultan as Mayas mother Pradeep Rawat* as Gupta
*Mithun Rodwittiya as Jamshed
*Gaurav Dwivedi as Atul
*Asheesh Kapur as Praful
*Akanksha Nehra as Payal
*Kaizaad Kotwal as KK
*Sandeep Sanchdev as Tarun
*Sujata Sehgal as Kavita
*Shivani Tanksale as Nimmi
*Saurabh Shukla as Rockstar in Kuch Bhi Ho Sakta Hai
*Shilpa Sapatnekar as Rani

==Plot==
Inkaar is the love story of Maya Luthra (Chitrangada Singh) and Rahul Verma (Arjun Rampal) set against the backdrop of sexual harassment. When misunderstandings are created between the two lovers Maya and her mentor Rahul, she files a case of sexual harassment against him. A committee is set up by the advertising agency they work in to investigate their case and find out the truth. Rahul denies that he ever had any such intentions to sexually harass Maya, but she insists that he makes her feel insecure and unsafe in her own office. The committee is unable to reach a clear conclusion as they do not know who to believe. The climax of the film brings an unexpected twist to the story, and in a final confrontation the two protagonists love emerges victorious.

==Production==

===Casting=== niche movies. However, in an interview with Bollywood Hungama, the lead actress Singh said that it was a commercial film.  The film was created on a budget of  . 

===Filming=== Grey Worldwide.   

==Controversy==
 

===Title Conflict===
The initial title of the movie was chosen to be Inkaar but after the completion of filming, a title conflict rose as Mishra and the production team decided to opt a title out of six titles for the film based on its story. New five titles were Dream Factory, Power Play, Agency, Yeh Kahan Aa Gaye Hum and The Boss. Mishra stated in an interview with Bollywood Hungama, "Our film is about gender politics and power play in a corporate house. Given this brief, well decide which title suits the film the best and well go by it."  After some conflict, the title was chosen to be Kaam – The Unofficial Story.  The release date was delayed to 25 August but Kaam was also removed as a title as the release date came closer.  After four title hunts, the original title Inkaar was finalised as the title of the movie. The release date of the movie was pushed to 18 January, thus making it one of the early films of 2013. 

===Ad Agencies=== Grey Worldwide agencies in which Inkaar was extensively shot, warned the production team that they did not want any branding or reference to their respective agencies in the film. 

==Release==

===Critical reception===

Inkaar earned positive to mixed response from critics. Rachit Gupta of Filmfare gave 4 out of 5 stars. He appreciated the performances of the lead pair and quoted "What works is the chemistry between Arjun Rampal and Chitrangda Singh. Not just their individual performances but their love-hate relationship drives the drama of Inkaar. Both Arjun and Chitrangda handle their characters to great effect. But good performances can give you a watchable film, but not a believable story. And that is the big letdown in Inkaar. The films logic and the climax just dont make sense. Not from characters who spent the last two hours (or seven years in case of the movies narrative) convincing you that theyd do anything to come out on top. A film that promises to hit you hard in the solar plexus, doesnt even tickle you.".    

India Today wrote "The dynamics of office politics have never been more dynamic. Inkaar is one helluva jolt in January."    
Rummana Ahmed of Yahoo! Movies wrote "What works for Inkaar is the solid performances by the lead characters and the supporting cast. Arjun might not have been very strong in the emotional bits but he delivers one of his best performances till date. Chitrangda looks and acts sharp, its a pleasure to watch Vipin Sharma, Deepti Naval and Kanwaljeet on the big screen after quite a while. Shantanu Moitra music is as usual peppy and hummable. This one is definitely a must watch."    

Karan Anshuman of Mumbai Mirror gave 4 out of 5 stars, quoting "Inkaar is not about office politics as you might imagine, even though many moments shape an accurate portrayal. It is not about sexual harassment in the workplace as it is being marketed though that is the searing crucible in which complex, often unnatural dollops of human emotion are left to sputter and interact, never coalescing. Everything else is an elaborate backdrop. And finally when the truth unravels – when motives come to light – I had a great urge to watch the film again. And with movies, this urge supersedes all flaws."    

Ananya Bhattacharya of Zee News wrote "Inkaar is another of Sudhir Mishras brilliances captured on celluloid. Dont say a no to this one." {{cite web|url=http://zeenews.india.com/entertainment/bollywood/inkaar-movie-review-a-no-is-a-no-for-this-one-it-s-a-yes_126327.htm|title=‘Inkaar’ movie review: Arjun-Chitrangadas stellar performances make for a good watch
|date=18 January 2013|accessdate=1 February 2013|author=Ananya Bhattacharya|publisher=Zee News}}   Mayank Shekhar of Daily Bhaskar.com wrote "In parts thoughtful, engaging and funny, this is a film then about ambition. As for sexual harassment, I would go with the womens rights activist (Deepti Naval) judging this case: "Flirtation agar woman ko pasand na ho, toh harassment hoti hai." If I were you, Id definitely go for this movie too!"    

Taran Adarsh wrote "INKAAR is for spectators of serious cinema. Caters to a niche audience!"    

Sukanya Verma of Rediff.com has given 2 out of 5 stars, quoting "Inkaar trivialises something serious like sexual harassment into a terrible joke."    

Rajeev Masand of CNN-IBN gave 2 out of 5 stars as she wrote "This is a half watchable film despite all that melodrama flying around. Sadly, if it had kept its head, this thriller could have gone places."    

Shubhra Gupta of The Indian Express wrote "Inkaar could have been truly radical. But it becomes a film that prefers to cop out, rather than deliver on the promise it held out so bravely in its initial passages."    

Sonia Chopra of Sify wrote "The film has taken an important subject and turned it into slush. In the end the film is a mockery of sexual harassment, of love, of the ad fraternity and of professionalism. Too bad, really."    

===Box office===
Inkaar was released on 18 January 2013. The movie took a very dull start with 15% occupancy rate in multiplexes and major metros. It collected only   in the opening day.  The movie did not show any significant growth in business and collected only   in the opening weekend.  The film managed to earn   within the first week and collected a total of approximately   in the first week.   

==Soundtrack==
{{Infobox album 
| Name = Inkaar
| Type = Soundtrack
| Artist = Shantanu Moitra
| Cover = 
| Released =
| Recorded = Producer = Shantanu Moitra Feature film soundtrack
| Length =
| Label = T-Series
}}

Music is composed by Shantanu Moitra and Shamir Tandon.
The lyrics was penned by Swanand Kirkire.
{| class="wikitable"
|- style="background:#ff9; text-align:center;"
! Track: !! Song !! Singer(s) !! Length
|- 1 || "Darmiyan" || Swanand Kirkire || 5:01
|- 2 || "Maula Tu Malik Hai" || Shantanu Moitra & Swanand Kirkire || 4:57
|- 3 || "Zindagi Ka Karobar" || K. Mohan (Agnee) || 2:48
|- 4 || "Kuch Bhi Ho Sakta Hai" || Suraj Jagan || 3:58
|- 5 || "Inkaar (Aloop)" || Monali Thakur || 3:07
|- 6 || "Inkaar - Theme (Hindi Version)" || Papon, Shahid Mallya || 4:20
|- 7 || "Inkaar - Theme (English Version)" || Monica Dogra, Shahid Mallya || 4:21
|-
|}

==References==
 

==External links==
*  

 
 
 