Mattie the Goose-boy (film)
{{Infobox film
| name = Mattie the Goose-boy
| image =
| image_size =
| caption =
| director = Attila Dargay
| producer =
| writer = Screenplay: Attila Dargay József Nepp József Romhányi Epic poem: Mihály Fazekas
| narrator =
| starring =
| music = Tamás Daróczi Bárdos
| cinematography = Irén Henrik
| editing = Magda Hap
| studio = PannóniaFilm
| distributor =
| released = April 7, 1977
| runtime = 75 minutes
| country = Hungary
| language = Hungarian
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 Hungarian animated eponymous poem, written in 1804 by Mihály Fazekas. 

==Cast==
* András Kern as Mattie the goose-boy
* Péter Geszti as young Mattie the goose-boy
* László Csákányi as Döbrögi
* Gábor Agárdi as Ispán
* Antal Farkas as Fõhajdú
* László Csurka as Hajdú I
* Gellért Raksányi as Hajdú II
* Sándor Suka as Puskatöltögetõ
* Hilda Gobbi as Biri néni
* László Inke as Gyógykovács
* Ferenc Zenthe as Öreg csikós
* János Garics as Csikós legény
* Károly Mécs as Narrátor

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 