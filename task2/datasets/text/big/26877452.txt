Riding High (1937 film)
{{Infobox film
| name           = Riding High
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        =  David MacDonald
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Claude Dampier John Garrick Kathleen Gibson Helen Haye
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British comedy David MacDonald and starring Claude Dampier, John Garrick, Kathleen Gibson and Helen Haye. It is based on the story of the inventor Thomas McCall who came up with a radically new design for a bicycle in Victorian Britain. 

==Cast==
* Claude Dampier - Septimus Earwicker
* John Garrick - Tom Blake
* Kathleen Gibson - Grace Meadows
* Helen Haye - Miss Broadbent
* John Warwick - George Davenport
* Billy Mersing - Popping
* Mae Bacon - Mrs Winterbottom
* Peter Gawthorne - Sir Joseph Wilmot

==References==
 

 

 

 
 
 
 
 
 
 
 
 


 
 