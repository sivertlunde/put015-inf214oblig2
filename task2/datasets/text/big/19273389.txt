Made in Paris
{{Infobox film
| name           = Made in Paris
| image_size     = 
| image          = Made in Paris FilmPoster.jpeg
| caption        = 
| director       = Boris Sagal
| producer       = Joe Pasternak
| writer         = Stanley Roberts
| starring       = Louis Jourdan Ann-Margret Richard Crenna Chad Everett
| music          = George E. Stoll	 
| cinematography = Milton R. Krasner
| editing        = William McMillin	 
| distributor    = Metro-Goldwyn-Mayer
| released       = February 9, 1966 (United States)
| runtime        = 103 minutes
| country        = United States
| language       = English
| gross = $1,300,000 (est. US/ Canada rentals) 
}}
 American romantic comedy film starring Louis Jourdan, Ann-Margret, Richard Crenna, Edie Adams, and Chad Everett. The film was written by Stanley Roberts and directed by Boris Sagal.
This was the last screen credit for veteran MGM musical director Georgie Stoll before retirement.

==Plot== City of Light(Paris, France). Maggie Scott (Ann-Margret) works as an assistant buyer for Irene Chase (Edie Adams). Irene is a fashion buyer for Barclay Ames, an upscale clothing store in New York owned by Roger Barclay (John McGiver).

Ted Barclay (Chad Everett), the son of Roger Barclay, takes a special interest in Maggie. After taking her on a date, he finds that her morals are different from the multitude of his previous women. This bachelor doesn’t seem to mind a good chase.

Irene sends Maggie to Paris as her representative for the annual fashion shows of the major European fashion designers such as Marc Fontaine, Dior, and Balenciaga. The most important show is Marc Fontaine (Louis Jourdan) because Barclay Ames is the only store in New York that handles Fontaine gowns, and Maggie must keep that rapport between the two companies on her trip. Worried for Maggie’s safety, Ted calls his Paris-based columnist friend, Herb Stone (Richard Crenna), to look after her in Paris. 

Maggie’s arrival in Paris is paired with a warning from Herb Stone that she may lose all of her inhibitions, which she quickly refuses could happen.  Marc Fontaine, the handsome French designer, had a relationship with Irene. It doesn’t take long for the Parisian scenery to play with Maggie’s emotions, leading her into the arms of Mr. Fontaine. 
Herb Stone completes the love triangle by pursing Maggie as well. His version of a good time doesn’t involve the exciting dance club Maggie dances in for Mr. Fontaine. He would rather settle down in the bedroom.

Ted Barclay decides to fly out to Paris to win Maggie’s heart once and for all.
==Production==
MGM announced the film was part of their line up in February 1964. MGM Readies Record 34 Films for Release: 31 Others Scheduled for Production Including Adaptation of Dr. Zhivago
Los Angeles Times (1923-Current File)   26 Feb 1964: A8.   Doris Day was meant to star but she did not like the script. Success Story Heroes Top Coup
Dorothy Kilgallen:. The Washington Post, Times Herald (1959-1973)   25 June 1964: C10.   So Ann Margaret, who had just made Once Upon a Crime and The Cincinnati Kid for MGM was signed instead. Miss Latham Avers Shes Already Pro: Career Antedates Marnie; Oppenheimer Off to Saigon
Scheuer, Philip K. Los Angeles Times (1923-Current File)   12 Jan 1965: C7.  

Bob Crane, who had just shot the pilot for Hogans Heroes, was offered the male lead, as a newspaperman. FILMLAND EVENTS: Bing Plans to Sing a Different Tune
Los Angeles Times (1923-Current File)   20 Feb 1965: 17.   This part ended up being played by Richard Crenna.

Richard Chamerblain was offered the role of the department store buyer. Looking at Hollywood: Ailing Patricia Neals Friends Tell Hope
Hopper, Hedda. Chicago Tribune (1963-Current file)   12 Mar 1965: b13.   This was played by MGM contractee Chad Everett.

Louis Jourdan signed to play the male lead. There was a report he pulled out when he discovered his character did not get the girl in the end. Looking at Hollywood: Sophia Worlds Favorite, Says Zanuck
Hopper, Hedda. Chicago Tribune (1963-Current file)   14 Apr 1965: a1.  

Filming took place on the MGM backlot.
==Fashion==
The costumes worn by Edie Adams, Ann-Margret and the fashion models were created by costumer designer Helen Rose. Angie Put Her Foot in It
Scott, John L. Los Angeles Times (1923-Current File)   16 May 1965: N10.  Stylish Look at Made in Paris
Los Angeles Times (1923-Current File)   25 May 1965: c9.   Designer Steals Own Ideas
Hammond, Fay. Los Angeles Times (1923-Current File)   10 Sep 1965: c7.  

Edie Adams wears a form fitting black velvet beaded gown that flares out at the knee with a satin skirt covered in coque feathers. Her matching cape is made of black crepe chiffon featuring beading and three rows of coque feathers (13 min. 13 sec. into the film)  .

Ann-Margret’s arrival in Paris costume is a blue beige coat completely lined with fox fur and worn over a sheath  . 

The Fontaine fashion show starts at 42 min into the movie featuring Helen Rose designs.
‘Golden Avalanche’
Three piece ski suit of golden yellow stretch jersey slim pants, fingertip jacket is lined with silver grey Persian lamb, and a hooded sweater of Persian lamb  .  


‘Swirling Amethysts’ (45 min. 35 sec. into the film)
Three hundred yards of pleated silk chiffon, the high rise neckline and low back bodice is of amethysts, rubies, gold, and diamonds .


Ann-Margret’s ‘After-Five Costume’ (50 min. 30 sec. into the film)
Carl velvet coat embroidered and banded with sables  .

==Music==
*‘Lottie’ – Count Basie Orchestra
*‘Skol Sister’ – Count Basie Orchestra
Jazz music plays in the background for most of the film. Maggie Scott (Ann-Margret), performs a dance to a band in a Paris night club 55minutes and 47seconds into the film  .
==Reception==
MGM were so impressed with Crenns performance they signed him to a three picture deal. Looking at Hollywood: Elizabeth Ashley Will Fight Film Studio
Hopper, Hedda. Chicago Tribune (1963-Current file)   08 June 1965: b1.  

The Los Angeles Times said the film "was just not in the game class as Gigi" although Ann Margret "gave her all". Made in Paris an Evening of Almosts
Scheuer, Philip K. Los Angeles Times (1923-Current File)   11 Mar 1966: c11.  
==Quotes==
Ted- “Ms. Scott, are you bucking for sainthood?”

Maggie – “No, no I’m just an average American girl. I have the foolish idea that I’d like to settle down in the suburbs with a man I love and have children, and maybe even have a station wagon, and two of those large dogs with hair in front of their eyes. Im sure you think thats square Mr. Barclay.”

Herb Stone – “Any American girl today has two completely different sets of morals. Back in the States, a girl, like Maggie, watches every step but she has her mind on just one thing, a wedding ring. Well, here in Paris, she has no chance of getting married so she lets her hair down. She does all the things shes always wanted to do. Plus, a few things that uh, she never thought of.”

Mark Fontaine – “Do you know what you really want Maggie? You want a thrilling evening of almost. Yes, almost romance, almost love, almost sex. Maggie, I told you Paris would give you whatever you were looking for, youve got it. And youve put me in the position of a guide. Very well, very well, I hope I’ve given you proper service, Miss Scott. Now that weve shown you our best imitation of romance, what would you like to see next? The Eiffel Tower? The Arch of Triumph? Oh, I know the wax museum. Sex, lust, passion, but not real-not real, Miss Scott. Just the way you like it. Fake, all in one.”
(Sagal, B. (Director). (1966). Made in Paris  . United States. )

==References==
 

==External links==
* 
* 
* 
* 
*Sagal, B. (Director). (1966). Made in Paris  . United States. 

 

 
 
 
 
 
 
 
 


 