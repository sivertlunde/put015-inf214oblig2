AKA: Girl Skater
 
 
 
{{Infobox Film
| name = AKA: Girl Skater
| image =   
| caption = 
| alt =
| director = Mike Hill Whyte House Productions
| starring = Amy Caron, Vanessa Torres, Monica Shaw, Jaime Reyes, Dave Carnie, Jessica Landon
| music = Kristen Missen
| cinematography = Jean-Jacques Briquet, Colleen Hughson, Lisa Whitaker
| editing = Adam Wright
| distributor = Whyte House Productions
| released = 2003
| runtime = 26 minutes English
| country = Australia
}} documentary about four female professional skateboarders on a skateboarding tour in Australia.

The documentary follows Dave Carnie (from Big Brother Skateboard Magazine) as he leads pro skaters Amy Caron, Vanessa Torres, Monica Shaw, and Jaime Reyes on a tour of Australia, visiting a variety of skate parks and tourist attractions.  .   

== Overview ==
The film features the skaters doing demos at local skate parks, signing autographs for local people. There are also scenes of the skaters visiting a local zoo.

One part of the film takes place at the Gallaz Skate Jam in Melbourne, Australia, where the street course is won by Amy Caron. At the time, this contest was the largest prize in womens skateboarding, with Caron winning a new car.   

While the film features some skateboarding, it was viewed more as a documentary of the culture of female skateboarding|skateboarders, as opposed to a technical skate video. This is the first major skate film to focus solely on the progression and culture of women in skateboarding, by highlighting a select number of top female skaters.

Mike Hill, the director of Girl Skater once quoted that; "Given the current standard of womens skateboarding, a film that finally gives a voice to this subculture, and the phenomenal athletes that represent it, is well overdue." 

== Cast ==
* Amy Caron
* Vanessa Torres
* Monica Shaw
* Jaime Reyes
* Dave Carnie
* Jessica Landon

== Home media ==
  
The DVD features special bonus features including Gallaz Skate Jam Highlights, trick tips with Lauren Mollica, a behind the scenes feature and a bails feature. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 