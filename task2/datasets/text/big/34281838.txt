Uu Kodathara? Ulikki Padathara?
 
{{Infobox film
| name           = Uu Kodathara? Ulikki Padathara?
| image          = Uu Kodathara Ulikki Padathara.jpg
| caption        = Film poster
| writer         = Lakshmi Bhupal  
| story          = Sekhar Raja
| screenplay     = Sekhar Raja
| director       = Sekhar Raja
| producer       = Lakshmi Manchu  Mohan Babu  
| starring       = Nandamuri Balakrishna Manoj Manchu Deeksha Seth Lakshmi Manchu
| music          = Bobo Shashi  Chinna   
| cinematography = B. Rajasekar
| editing        = M. R. Varmaa
| studio         = Manchu Entertainment
| distributor    = Manchu Entertainment    Cool Flicks Cinemas   
| released       =  
| runtime        = 134 minutes
| country        = India
| language       = Telugu
| budget         =  10,86,24,000 
| gross          =
}}
 fantasy film produced by Lakshmi Manchu on Manchu Entertainment banner, written and directed by Sekhar Raja. Starring Nandamuri Balakrishna Manoj Manchu, Deeksha Seth, Lakshmi Manchu in lead roles.  The film soundtrack is composed by Bobo Shashi and the background score of the film is given by Chinna. Cinematography is handled by B. Rajasekar. The soundtrack of the film was launched on May 30, 2012. The film was released worldwide on July 27, 2012. It was also dubbed into hindi as Simha 2. The film recorded as Above Average at the box-office  The Movie was a Success at the box office and held its success meet on July 30, 2012. 

==Plot==
The story of the film revolves around a palace known as ‘Gandharva Mahal’ which belongs to Rayudu (Prabhu (actor)|Prabhu), the present head of a royal family that has lost its former splendor. Rayudu lives with his wife (Meena Kumari) and two daughters Visalakshi (Madhumitha) and Jagadha (Deeksha Seth) in a small house beside the Mahal. Rayudu rents out the palace to make ends meet. He is shown as a soft and generous man. Some of the tenants take this generosity for granted and harass Rayudu, without paying the rent.

Into this scenario comes Manoj (Manoj Manchu), as someone seeking a place to rent. He slowly gets rid of the problematic elements in the palace and restores control to Rayudu. Manoj falls in love with Jagadha. A happy Rayudu decides to get Visalakshi married and decides to give away the Gandharva Mahal as dowry. One night, Manoj comes to Rayudu with a bloodied nose and claims that he saw a ghost which tried to kill him and warned him about selling the Mahal. Rayudu reveals that the ghost might be his dead father, Rudramaneni Narsimha Rayudu (Nandamuri Balakrishna) and explains the history of Gandharva Mahal to Manoj.

Gandhrva Mahal has been with the Rudramaneni family for centuries and was passed on to Narasimha Rayudu (Nandamuri Balakrishna), a Zamindar who is well respected in the village. He lives with his wife (Panchi Bora) and sister Jagadamba (Simer Motiani). He marries his sister Jagadamba to Phanindra Bhoopati (Sonu Sood). Few days after the marriage, Bhoopati insults Narsimha Rayudu and asks him to give their share in the family property. Deeply hurt by the incident, Narsimha Rayudu gives half of everything he owns including the Gandharva Mahal to his sister and walks out of the house. He also gives hundred acres of land to Seshayya (Bhanu Chander), his trusted employee and friend. It is later revealed that Bhoopati married Jagadamba only for the money and wants to marry his sweetheart Amrutha Valli (Lakshmi Manchu). Amrutha doesnt want to marry Bhoopathi since he already has a wife. Consequently, Bhoopathi kills Jagadamba and makes it look like a suicide.

On hearing the news, Narasimha Rayudu is devastated. Only a week after Jagadambas death, Bhoopathi tries to marry Amrutha in the Gandhrva Mahal. Narasimha Rayudu, angered by Bhoopathis actions, confronts him. Bhoopathi gets into a fight with Narasimha Rayudu and stabs him with a sword. He also reveals to Amrutha and the dying Narasimha Rayudu that he was the one who murdered Jagadamba. In anger, Narasimha Rayudu kills Bhoopathi and dies in the Mahal. Amrutha, saddened by the events, blames herself for the whole incident and eventually turns into a beggar.
 Sai Kumar) wants to take the palace and convert it into a hotel. Later that night, Bujji sees Narasimha Rayudus ghost and gets scared. Bujji seeks the help of a Mantrik (Ajay (actor)|Ajay) who reveals that the house is indeed haunted by the souls of Bhoopati and Narasimha Rayudu. He captures the violent soul of Bhoopathi and traps it in a bottle. It is also revealed that Manoj is Seshayyas grandson and was sent by his mother Suguna (Suhasini Maniratnam), Seshayyas daughter to help Rayudu and his family. Manoj reveals to Jagadha that he was never attacked by a ghost and tells her it was part of his plan to avoid giving the Mahal as dowry. He also reveals that it was not a ghost but him that scared Bujji. Bujji overhears the conversation and is angered. Believing that the Mantrik too was a fraud, in anger he breaks the bottle in which the Mantrik captured Bhoopatis soul. Now freed, Bhoopathis soul enters Rishis body and tries to kill the family. Narasimha Rayudus soul enters Manojs body and tries to stop Bhoopathi. Mantrik brings Amrutha Valli to stop Bhoopathi. Amrutha Valli lies to Bhoopathi that she got married and kills herself to stop him. In the end, Visalakshi marries Rishi and Manoj gets married to Jagadha. Manoj also sees the ghost of Narsimha Rayudu sitting on a chair and smoking a cigar, indicating that he would forever protect the Mahal.

==Cast==
  Balakrishna as Rudramaneni Narasimha Rayudu
* Manoj Manchu as Manoj
* Deeksha Seth as Jagadhamba
* Lakshmi Manchu as Amrutha Valli
* Sonu Sood as Phanindra Bhoopati Prabhu as Rayudu
* Richard Rishi as Rishi Kumar
* Bhanu Chander as Seshayya Sai Kumar as Bujji
* Dharmavarapu Subramanyam as Utsava Murthy Pruthvi Raj as Siddha Ajay as Mantrik
* Raghu Babu as Nerala Satti Babu
* Raja Ravindra as Doctor
* Prabhas Sreenu as Cable Keshav
* Praveen as Lavudu
* Nizhalgal Ravi as Manojs father
* Gollapudi Maruti Rao as Venkat Rao
* Chalapathi Rao as Raja Rao
* Panchi Bora as Visalakshi (Narasimha Rayudus wife)
* Simer Motiani as Jagadamba Suhasini as Suguna Aishwarya as Rajyalakshmi
* Abhinayashree as Golla Savitri
* Madhumitha as Vishalakshi
* Meena Kumari as Jagadhas mother
* Prabha as Rishi Kumars mother
* Ragini as Utsava Murthys wife
 

==Soundtrack==
{{Infobox album
| Name      = Uu Kodathara? Ulikki Padathara?
| Artist     = Bobo Shashi
| Type       = Film
| Cover      =
| Released   =  
| Recorded   = 2012 Feature film soundtrack
| Length     = 25:15 Telugu
| Label      = Aditya Music
| Producer   = Bobo Shashi
| Reviews    =
| Last album = Bindaas (2010 film)|Bindaas  (2010)
| This album =  Thakita Thakita   (2010)
| Next album = Uu Kodathara? Ulikki Padathara?  (2012)
}}

The soundtrack of the film was composed by Bobo Shashi, which happens to be his second Manoj Manchu project after Bindaas (2010 film)|Bindaas. One of the song in the album was composed by popular Indian composer Vidyasagar (music director)|Vidyasagar. Initially the audio launch event was planned to be held on May 18, 2012 in Guntur. But it was postponed due to unknown reasons. The soundtrack album was released on May 30, 2012 under Aditya Music label at Shilpakala Vedika in Hyderabad, India|Hyderabad.  All songs in the album were penned by R. Ramu  and the background score of the film was given by chinna.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:15
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Anuragame Haaratulaye Vidyasagar
| extra1  = Karthik (singer)|Karthik,Anwesha Datta Gupta
| length1 = 4:42

| title2  = Prathi Kshanam Narakame
| extra2  = Ramee,Tupakeys,G-Arulaz
| length2 = 4:52

| title3  = Adhi Ani Idhi Ani
| extra3  = Haricharan,Prashanthini
| length3 = 4:13

| title4  = Are you ready
| extra4  = Instrumental
| length4 = 2:02

| title5  = Abbabba Abbabba
| extra5  = Ramee,Nrithya,Janani,Reeta,Ramya NSK
| length5 = 4:38

| title6  = Hai Re Hai
| extra6  = Ranjith (singer)|Ranjith,M. L. R. Karthikeyan|M. L. R. Karthik,Senthil,Sam,Sormuki,Ramya,Deepa
| length6 = 4:48
}}

==Production==
Manoj Manchu was quoted many times stating that UKUP was his dream project. The movie will be shot simultaneously in Telugu and Tamil. Director Shekar Raja worked as an assistant to director Krishna Vamsi. Although planned three years ago, the movie entered pre-production in early 2011. Lakshmi Manchu, along with Manoj, established Manchu Entertainments banner to produce this movie.
 Vara Prasad Simbu on May 21, 2012.  Uu Kodathara? Ulikki Padatara? was released on July 27, 2012 in more than 60 theatres in Hyderabad alone. 

==Release==

===Critical reception===
The film opened to mixed to negative reviews but balakakrishnas performance was praised by critics. Jeevi from Idlebrain gave 3.25 of 5.0 rating and said "UKUP is a special film for two reasons. One is that it introduces a big star like Bala Krishna in a vital role. If big stars accept story oriented films and act as per demand of the role instead of looking for equal screen space, it’s good for the industry. The second reason is that the producer has passionately invested on the project though it’s a story oriented film with a bit of experimentation. Watch it for the performances of Bala Krishna Nandamuri, Manoj Manchu and Lakshmi Manchu. And for the spectacular Gandharva Mahal set".  Mahesh Koneru from 123Telugu gave 3.0 of 5.0 rating and said "Balakrishna’s majestic performance is the biggest attraction for UKUP. Though the film has been made with rich production values and has a superb last 25 minutes, a below par first half and unconventionally shot songs work against the film."  Karthik Pasupulate from The Times of India gave 2.5 stars and said "Vu Kodathara Vulikki Padathara is one of those films that tries hard push the envelope, but succeeds only in parts. It does offer some moments though, better execution could have raised the movie experience by a fair few notches. " 

==References==
 

==External links==
* 

 
 