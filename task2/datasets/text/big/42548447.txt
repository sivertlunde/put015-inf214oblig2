Love Child (2014 film)
{{Infobox film
| name           = Love Child
| image          = Love Child poster.jpg
| alt            = 
| caption        = Promotional poster
| director       = Valerie Veatch
| producer       = Valerie Veatch David Foox Danny Kim Dong Hyun Danny Kim Minji Kim Daniel Levin
| writer         = Valerie Veatch
| starring       = 
| music          = 
| cinematography = Daniel Levin
| editing        = Valerie Veatch Christopher Donlon
| studio         = HBO
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = South Korea United States
| language       = English
}}
Love Child is a 2014 South Korean-American documentary film written, directed and produced by Valerie Veatch.   The film premiered in-competition in the World Cinema Documentary Competition at 2014 Sundance Film Festival on January 17, 2014.  

The film was theatrically released by HBO on June 18, 2014. 

==Synopsis==
The film narrates the story of South Korean couple, who were immersed in an online game, while their baby named Sarang died of malnutrition.

==Reception==
Love Child received mixed to positive reviews upon its premiere at the 2014 Sundance Film Festival. Christie Ko  in his review for ScreenCrave gave the film 6/10 and said that "This documentary is not an exhaustive look at the addiction to gaming, but does a good job of explaining this incident of the couple and their baby in South Korea. It is thorough in its treatment of their story. But it scratched the surface of broader implications that left me with many questions."  While, Daniel Fienberg of HitFix said in his review that Love Child never attempts to cheapen the tragedy of Sarangs death, but like "Web Junkie," it wants to lay out a pattern of behavior that might seem a little extreme, but also wont feel that outlandish to anybody who counts the Internet among the spaces in which they feel most social." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 

 
 