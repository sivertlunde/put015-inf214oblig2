Zombie and the Ghost Train
{{Infobox film
| name = Zombie and the Ghost Train
| image = 
| caption = 
| director = Mika Kaurismäki
| producer = Mika Kaurismäki
| screenplay = Mika Kaurismäki
| story = {{plainlist|
* Sakke Järvenpää
* Mika Kaurismäki
* Pauli Pentti
}}
| starring = {{plainlist|
* Silu Seppälä
* Marjo Leinonen
* Matti Pellonpää
}}
| music = Mauri Sumén
| cinematography = Olli Varja
| editing = Mika Kaurismäki
| distributor = Finnkino
| released =  
| runtime = 88 minutes
| country = Finland
| language = Finnish English Turkish
}}
Zombie and the Ghost Train ( ) is a 1991 Finnish film directed by Mika Kaurismäki.  It focuses on Antti (aka Zombie), a loner who loves performing music but leads a miserable life otherwise.

==Cast==
* Silu Seppälä as Zombie
* Marjo Leinonenas Marjo
* Matti Pellonpää as Harri
* Vieno Saaristo as Äiti
* Juhani Niemelä as Isä

== Reception == Kevin Thomas wrote, "It is as fine as anything either of the talented, charismatic Kaurismaki brothers has ever done. It is a rueful, often darkly funny portrait". 

== References ==
 

== See also ==
* Docufiction
* List of docufiction films

==External links==
* 
*  

 
 
 
 


 