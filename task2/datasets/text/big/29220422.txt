The Man in the Back Seat
 
 
{{Infobox film
| name           = The Man in the Back Seat
| image          = "The_Man_in_the_Back_Seat"_(1961).jpg
| caption        = 
| director       = Vernon Sewell
| producer       = Leslie Parkyn Julian Wintle
| writer         = Malcolm Hulke Eric Paice Edgar Wallace (story)
| starring       = Derren Nesbitt Keith Faulkner
| cinematography = Reginald Wyer
| editing        = John Trumper
| music          = Stanley Black Independent Artists Studios
| distributor    = Anglo-Amalgamated
| released       = June 1961
| runtime        = 57 minutes
| country        = United Kingdom
| language       = English
}}
 1961 British B-movie crime film, directed by Vernon Sewell and starring Derren Nesbitt and Keith Faulkner.  The film was based on an Edgar Wallace story, and was tightly shot, with only four name-credited actors (one of whom remains silent through most of the film) and much of the action taking place in a cramped flat and the claustrophobic confines of a car at night. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 98-101 

==Plot==
Cold and vicious Tony (Nesbitt) and his more pleasant natured but easily influenced partner-in-crime Frank (Faulkner) hatch a plan to rob bookmaker Joe Carter (Harry Locke) of his takings as he leaves the local dog track.  They attack him brutally, then realize to their disgust that the case containing the cash is chained to Joes wrist.  They bundle him unconscious into the back seat of his car and they drive around trying to figure out a way to release the case.  They come up with various possible solutions, but nothing works and they end up at Franks flat, to the horror of Franks wife Jean (Carol White), who does not want their criminal activities to be brought to her doorstep.

They manage to free the case after administering another severe beating to Joe, and decide to get rid of him by dousing him in alcohol and dumping him near the local hospital, where they assume a passer-by will find him and think he has suffered a drunken fall.  As they are about to leave the scene, they realize that they have left behind incriminating fingerprints, so have no option but to retrieve the unconscious Joe again.  Not knowing what else to do, they go back to Jeans and contact a former male nurse, who after looking at Joe says he will soon be dead.  As a last resort, Tony and Frank decide to drive through the night to Birmingham and leave the body there, where there will be nothing to connect the crime to them.

They again load Joe into the back seat and set off on the journey in the rainswept night, with Frank becoming increasingly paranoid that every vehicle behind them on the road is on their trail.  They are barely out of London when Frank looks in the driving mirror, and to his terror sees the ghostly wide-open eyes of the no longer dead Joe staring reproachfully at him from the back seat.  In total panic, Frank drives the car off the road and down an embankment.  The crash kills Tony instantly but Frank, seriously injured but alive, manages to crawl clear of the wreckage.  The police and ambulance services arrive and confirm Tonys death.  The remorse-stricken Frank pleads with them to see if they can help the other passenger, but the car explodes before anything more can be done.

==Cast==
* Derren Nesbitt as Tony
* Keith Faulkner as Frank
* Carol White as Jean
* Harry Locke as Joe Carter

==Critical reception==
in the Radio Times, David Parkinson wrote, "the phrase "quota quickie" was synonymous with cheaply made, under-plotted films notable only for the ineptitude of the acting. Its a rare treat, therefore, to stumble across a British B with an intriguing idea thats been ingeniously executed. Director Vernon Sewell, who was responsible for some of the very worst quickies, outdoes himself with this haunting story."  

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 