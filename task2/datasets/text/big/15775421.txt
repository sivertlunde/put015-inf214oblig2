Denver and Rio Grande (film)
{{Infobox film
 | name = Denver and Rio Grande
 | image = Denver and Rio Grande VideoCover.jpeg
 | caption =
 | director = Byron Haskin
 | producer = Nat Holt Harry Templeton Frank Gruber
 | starring = Edmond OBrien Sterling Hayden
 | music = Paul Sawtell
 | cinematography = Ray Rennahan
 | editing = Stanley E. Johnson
 | distributor = Paramount Pictures
 | released =  
 | runtime = 89 minutes
 | country = United States
 | language = English
 | budget =
| gross = $1.18 million (US rentals) 
}}

Denver and Rio Grande is a western film, directed by Byron Haskin and released by Paramount Pictures in 1952.

The film is a dramatization of the building of the Denver and Rio Grande railway, which was chartered in 1870. It was filmed on location on the actual railway near Durango, Colorado. The main character, Jim Vesser (Edmond OBrien), and his workers are trying to build part of the railroad line through the Rocky Mountains, but their rivals try to stop them.

The films storyline is recognized to be a fictional account with the basic premise centered on the factual "Royal Gorge War" 
between the D&RG and AT&SF during 1878 to 1880. 
The film includes a head on collision of two Denver and Rio Grande locomotives that were slated for retirement and scrapping.

==Cast==
*Edmond OBrien as Jim Vesser
*Sterling Hayden as McCabe
*Dean Jagger as General William J. Palmer
*Kasey Rogers as Linda Nelson alias Linda Prescott (as Laura Elliott)
*Lyle Bettger as Johnny Buff
*J. Carrol Naish as Gil Harkness
*Zasu Pitts as Jane Dwyer
*Tom Powers as Sloan
*Robert Barrat as Charlie Haskins
*Paul Fix as Engineer Moynihan
*Don Haggerty as Bob Nelson James Burke as Sheriff Ed Johnson
*Jack Daly as Tolliver, Generals Secretary (uncredited)
*Lester Dorr as the Dealer (uncredited)
*Jimmie Dundee as a Train Engineer (uncredited)
*George Magrill as a Railroad Worker (uncredited)
*Forrest Taylor as Mac (uncredited)

==Home media==

The film was originally released on VHS on November 11, 1998. DVD and Blu-ray issues were released on May 29, 2012.

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 