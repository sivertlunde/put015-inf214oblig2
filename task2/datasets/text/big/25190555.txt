List of horror films of 2000
 
 
A list of horror films released in 2000 in film|2000.

{| class="wikitable sortable" 2000
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable" | Cast
! scope="col" | Country
! scope="col" class="unsortable" | Notes
|-
!   |   Jeff Peterson ||   ||  
|-
!   | Bless the Child
| Chuck Russell || Kim Basinger,  Christina Ricci,  Holliston Coleman ||   ||  
|-
!   |  
| Hiroyuki Kitakubo || ||   ||  
|-
!   |  
| Joe Berlinger || Erica Leerhsen, Tristine Skyler, Stephen Barker Turner ||   ||  
|-
!   | The Massacre of the Burkittsville 7: The Blair Witch Legacy
| Ben Rock || ||   ||  
|- The Calling
| Richard Caesar || Laura Harris, John Standing ||   ||  
|-
!   | Cherry Falls
| Geoffrey Wright || Brittany Murphy, Jay Mohr, Michael Biehn ||   ||  
|-
!   |  
| Lloyd Kaufman || Ron Jeremy, Jasi Cotton Lanier, Debbie Rochon ||   ||  
|- The Convent
| Mike Mendez || Adrienne Barbeau ||   ||  
|-
!   | Crocodile (2000 film)|Crocodile
| Tobe Hooper || Mark McLachlan, Caitlin Martin, Summer Knight ||   ||  
|-
!   | Cut (2000 film)|Cut
| Kimble Rendall || Molly Ringwald, Tiriel Mora, Kylie Minogue ||   ||  
|-
!   | Deadfall
| Vince Di Meglio || Kristin Leigh, Melissa Di Meglio, Paul Di Meglio ||   ||  
|-
!   | Deep in the Woods
| Lionel Delplanque || Clotilde Courau, Clément Sibony, Alexia Stresi ||   ||  
|-
!   | The Demon Within
| Ian Merrick || Jeff Fahey, Katie Wright, Emmanuelle Vaugier ||   ||  
|-
!   | Dial D for Demons
| Billy Tang Hin-Shing || Jordan Chan, Joey Meng, Lee Ann, Terence Yin, Alice Chan, Winnie Leung ||   ||   
|-
!   | Dracula 2000
| Patrick Lussier || Gerard Butler, Christopher Plummer, Jonny Lee Miller ||   ||  
|-
!   | Drainiac!
| Brett Piper || Georgia Hatzis, Ethan Krasnoo, Samara Doucette ||   ||  
|-
!   | Final Destination James Wong || Devon Sawa, Ali Larter, Seann William Scott ||   ||  
|- Ginger Snaps John Fawcett || Emily Perkins, Katharine Isabelle, Kris Lemche ||   ||  
|-
!   |  
| Scott Derrickson || Craig Sheffer, Nicholas Turturro, Doug Bradley ||   ||  
|-
!   | Hollow Man
| Paul Verhoeven || Kevin Bacon, Elisabeth Shue, Josh Brolin ||   || Science fiction ~ science experiment based 
|-
!   | The Irrefutable Truth About Demons
| Glenn Standring || Karl Urban, Katie Wolfe ||   ||  
|-
!   | Ju-on 1
| Takashi Shimizu || Yuurei Yanagi, Ryôta Koyama, Kazushi Andô ||   ||  
|-
!   | Ju-on 2
| Takashi Shimizu || Makoto Ashikawa, Yûko Daike, Kaori Fujii ||   ||  
|-
!   | Killjoy (2000 film)|Killjoy
| Craig Ross Jr. || Lee Marks, William L. Johnson, Jamal Grimes ||   ||  
|-
!   |  
| Rob Spera || Warwick Davis, Ice-T ||   ||  
|- Lost Souls
| Janusz Kamiński || Winona Ryder, Ben Chaplin, Sarah Wynter ||   || Supernatural horror 
|- They Nest John Savage ||   ||  
|- Pitch Black
| David Twohy || Vin Diesel, Radha Mitchell, Cole Hauser ||   || Science fiction ~ alien lifeform  
|-
!   | Prison of the Dead Julin Breen || Alicia Arden, Patrick Flood, Debra Mayer ||   ||  
|-
!   | Psycho Beach Party
| Robert Lee King || Lauren Ambrose, Thomas Gibson, Kimberley Davies ||   || Comedy horror 
|-
!   | Python (film)|Python
| Richard Clabaugh || Robert Englund, Jenny McCarthy, Casper Van Dien ||   ||  
|-
!   | School Day of the Dead
| Tetsuo Shinohara || Thane Camus, Masaya Katô, Yôzaburô Itô ||   ||  
|-
!   | Scream 3
| Wes Craven || David Arquette, Neve Campbell, Courteney Cox Arquette ||   ||  
|-
!   | Shadow of the Vampire
| E. Elias Merhige || John Malkovich, Willem Dafoe, Cary Elwes ||    ||  
|-
!   | Shriek If You Know What I Did Last Friday the Thirteenth
| John Blanchard || Tiffani Amber Thiessen, Coolio, Julie Benz ||   || Horror comedy 
|-
!   | Supernova (2000 film)|Supernova Thomas Lee || James Spader, Angela Bassett, Robert Forster ||   || Science fiction horror 
|-
!   | Subconscious Cruelty
| Karim Hussain || Brea Asher, Eric Pettigrew ||   ||  
|-
!   |  
| Tomijiro Mitsuishi || Yoshiko Yura, Kumija Kim, Kenichi Endô ||   ||  
|-
!   |  
| John Ottman || Jennifer Morrison, Matthew Davis, Hart Bochner ||   ||  
|-
!   | Uzumaki (film)|Uzumaki
| Higuchinsky || Ren Osugi, Hinako Saeki ||   ||  
|-
!   |  
| Yoshiaki Kawajiri || Hideyuki Tanaka, Ichirô Nagai, Megumi Hayashibara ||   ||  
|-
!   | Versus (film)|Versus
| Ryuhei Kitamura || Tak Sakaguchi, Hideo Sakaki, Chieko Misaka ||   ||  
|-
!   | Voodoo Academy
| David DeCoteau || Chad Burris, Ben Indra, Debra Mayer ||   ||  
|-
!   | What Lies Beneath
| Robert Zemeckis || Harrison Ford, Michelle Pfeifer, Katherine Towne ||   ||
|-
!   | Witchcraft XI: Sisters in Blood
| Ron Ford || Mikul Robins, Anita Page, Mark Shady ||   ||  
|}

==References==
 

 
 
 

 
 
 
 