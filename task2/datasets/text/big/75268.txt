The Ox-Bow Incident
 
{{Infobox film
| name           = The Ox-Bow Incident
| image          = The Ox-Bow Incident poster.jpg
| caption        = original movie poster
| director       = William A. Wellman
| producer       = Lamar Trotti
| based on       =  
| writer         = Lamar Trotti
| starring       = Henry Fonda Dana Andrews Mary Beth Hughes
| music          = Cyril J. Mockridge
| cinematography = Arthur C. Miller
| editing        = Allen McNeil
| distributor    = 20th Century Fox
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = $565,000 
| gross          = $750,000   
}}
 western film directed by William A. Wellman and starring Henry Fonda, Dana Andrews and Mary Beth Hughes, and featuring Anthony Quinn, William Eythe, Harry Morgan and Jane Darwell. It was nominated for the Academy Award for Best Picture. 
{{cite web|url=http://awardsdatabase.oscars.org/ampas_awards/DisplayMain.jsp?curTime=1211030051708
|title=1943 (16th annual) Academy Award winner for Outstanding Motion Picture|accessdate=2008-05-16}} 
 novel of the same name, written by Walter Van Tilburg Clark. 

==Plot==
In Bridgers Wells, Nevada in 1885,  Art Croft (Harry Morgan) and Gil Carter (Henry Fonda) ride into town and enter Darbys Saloon. The atmosphere is subdued due to recent incidents of Cattle raiding|cattle-rustling. Art and Gil are suspected to be rustlers because they are rarely seen in town.
 posse to Harry Davenport), Frank Conroy) and his son Gerald (William Eythe). The major informs the posse that three men and cattle bearing Kinkaids brand have just entered Bridgers Pass.

The posse encounters a stagecoach. When they try to stop it, the stagecoach guard assumes that it is a stickup, and shoots, wounding Art. In the coach are Rose Mapen (Mary Beth Hughes), Gils ex-girlfriend, and her new husband, Swanson (George Meeker).
 Francis Ford, brother of film director John Ford). Martin claims that he purchased the cattle from Kinkaid but received no bill of sale. No one believes Martin, and the posse decides to hang the three men at sunrise.

Martin writes a letter to his wife and asks Davies, the only member of the posse that he trusts, to deliver it. Davies reads the letter, and, hoping to save Martins life, shows it to the others. Davies believes that Martin is innocent and does not deserve to die.

The Mexican, a gambler named Francisco Morez, tries to escape and is shot and wounded. The posse discovers that Juan has Kinkaids gun.

Major Tetley wants the men to be lynched immediately. A vote is taken on whether the men should be hanged or taken back to face trial.  Only seven, among them Davies, Gerald Tetley, Gil and Art, vote to take the men back to town alive; the rest support immediate hanging. Gil tries to stop it, but is overpowered.

After the lynching, the posse heads back towards Bridgers Wells and meets Sheriff Risley, who tells them that Lawrence Kinkaid is not dead and that the men who shot him have been arrested. Risley strips the deputy of his badge.

The men of the posse gather in Darbys Saloon and drink in silence. Major Tetley returns to his house and shoots himself. In the saloon, Gil reads Martins letter while members of the posse listen. In the final scene Gil and Art head out of town to deliver the letter and $500 raised by those in the posse to Martins wife.

==Cast==
  
*Henry Fonda as Gil Carter 
*Dana Andrews as Donald Martin 
*Mary Beth Hughes as Rose Mapen / Rose Swanson 
*Anthony Quinn as Juan Martínez / Francisco Morez 
*William Eythe as Gerald Tetley 
*Harry Morgan as Art Croft
*Jane Darwell as Jenny Grier 
*Matt Briggs as Judge Daniel Tyler  Harry Davenport as Arthur Davies  Frank Conroy as Maj. Tetley 
*Marc Lawrence as Jeff Farnley 
  Paul Hurst as Monty Smith 
*Victor Kilian as Darby 
*Chris-Pin Martin as Poncho 
*Willard Robertson as Sheriff
*Leigh Whipper as Sparks (uncredited) Margaret Hamilton as the house keeper (uncredited)
*Billy Benedict as the young man
*Dick Rich as Deputy Butch Mapes
*George Meeker as Mr. Swanson Francis Ford as Alva Hardwicke
 

==Production==
Filming took place from late June to early August 1942. Additional sequences and retakes were made from mid-August to late August 1942. 

==Reception==
La Furia Umana s Toshi Fujiwara said the film is "one of the most important westerns in the history of American cinema". 

==Awards and honors== Best Picture at the 16th Academy Awards, losing to Casablanca_(film)|Casablanca.

==See also==
* Trial movies

== References ==
Notes
 

==External links==
*  
*  
*  
*  
*  
*  on Screen Guild Theater: September 18, 1944

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 