Thai Thief
{{Infobox film
| name           = Thai Thief
| image          = ThaiThief.jpg
| caption        = The Thai movie poster.
| director       = Pisut Praesangeam
| producer       = 
| writer         = Suphachai Sittiaumponpan
| narrator       = 
| starring       = Todsaporn Rottakij Sahatchai Chumrum
| music          = 
| cinematography = 
| editing        =  RS Film
| released       = April 12, 2006
| runtime        = 105 minutes Thailand
| Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2006 Cinema Thai action action comedy RS Film and released on April 12, 2006. The film is set during World War II in Thailand.

==Plot== Japanese troops occupied Southeast Asia with the intention of making new colonies. Thailand was one country that allowed the Japanese troops to transport their weapons and gold via train.  For Kom, a well-known Thai thief, this was the perfect opportunity to commit a crime. Meanwhile, Toe, the leader of an anti-Japan movement has a plan to stop the train, but the situation becomes more difficult when Kom and Toe are forced to help a secret agent from being captured by the troops.

==Cast==
*Sahatchai "Stop" Chumrum as Laem 18 Uan 
*Sara Leigh as Seena 
*Amthida Ngoencharoen as Patty 
*Suthep Po-ngam as Toe 
*Todsaporn Rottakij as Kom 
*Somlek Sakdikul as General Sant 
*Than Thanakorn as Yai Thaareu

==External links==
*  

 
 
 
 
 
 
 


 
 