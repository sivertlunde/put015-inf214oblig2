Jung (2000 film)
 

 
{{Infobox film
| name           = Jung
| image          = Jung film poster.jpg
| caption        = Promotional Poster of the film
| director       = Sanjay Gupta (director)
| producer       = Satish Tandon
| screenplay     = Abhinav Kashyap Anurag Kashyap
| starring       = Sanjay Dutt Jackie Shroff Shilpa Shetty
| music          = Anu Malik
| cinematography = Sameer Arya
| distributor    = Digital Entertainment
| released       = 12 May 2000
| runtime        = 147 minutes
| country        = India
| language       = Hindi
}} Desperate Measures.

==Plot==
Police Inspector Veer Chauhan is an honest and diligent man, who lives in India with his wife, Naina, and a son, Sahil. While Veer likes to go by the book, his partner, Inspector Khan, is exactly the opposite, reckless, and trigger-happy. When Veer is told that his son requires a bone-marrow transplant, Veer does his best, but is unable to come up with any donor, and is told that Sahil may not live long. Veer finds out that the only bone-marrow transplant compatible with that of Sahil is that of jailed criminal, Balli. The only problem is Balli is not likely to even consider donating any organ of his body, as Veer was the one who had arrested him. A short while later, Balli appears to have a change of heart, agrees to be a donor, and is transported to the hospital, but escapes. Now with time running out, Veer must locate Balli - that is if he is not shot dead first by Khan.

==Controversy==
Producer Satish Tandon & Sanjay Gupta had a tiff during the making of this film. Gupta was disappointed by Tandon, who added unwanted scenes without Guptas permission. This resulted Gupta to withdraw himself from credits & he wasnt credited in film. But in the end credits, he was credited. Sanjay Dutt, one of best friends of Sanjay Gupta refused to dub the film and his voice was dubbed by another person.  

==Cast==
*Sanjay Dutt - Bali
*Jackie Shroff - Inspector Veer Chauhan
*Aditya Pancholi - Inspector Khan 
*Raveena Tandon - Naina V. Chauhan 
*Shilpa Shetty - Tara (Ballis girlfriend)
*Saurabh Shukla - Musa
*Neeraj Vora - Lachmandas Dholakia, Lacchu
*Jash Trivedi - Sahil V. Chauhan (as Master Jash)/ Veer Chauhans Son
*Sachin Khedekar - Doctor 
*Navin Nischol - Police Commissioner

==References==
 

== External links ==
*  

 
 
 
 
 
 
 

 
 