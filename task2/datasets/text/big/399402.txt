Cinderella (1950 film)
{{Infobox film
| name           = Cinderella
| image          = Cinderella-disney-poster.jpg
| alt            = 
| caption        = Original theatrical release poster
| director       = {{Plainlist|
* Clyde Geronimi
* Hamilton Luske
* Wilfred Jackson}}
| producer       = Walt Disney
| writer         = {{Plainlist| Ken Anderson
* Perce Pearce
* Homer Brightman
* Winston Hibler
* Bill Peet
* Erdman Penner
* Harry Reeves
* Joe Rinaldi
* Ted Sears}}
| based on       =  
| narrator       = Betty Lou Gerson
| starring       = {{Plainlist|
* Ilene Woods
* Eleanor Audley
* Verna Felton
* Rhoda Williams James MacDonald
* Luis van Rooten Don Barclay
* Mike Douglas
* Lucille Bliss}}
| music          = {{Plainlist|
* Oliver Wallace Paul J. Smith}}
| editing        = Donald Halliday Walt Disney Productions RKO Radio Pictures, Inc.
| released       =   |ref2= }}
| runtime        = 75 minutes  
| country        = United States
| language       = English
| budget         = $2.9 million    $28.6 million  
| gross          = $85 million  $839.4 million  
}} musical fantasy RKO Radio Pictures.
 Walt Disney Animated Classics series, and was released on February 15, 1950. Directing credits go to Clyde Geronimi, Hamilton Luske and Wilfred Jackson. Songs were written by Mack David, Jerry Livingston, and Al Hoffman. Songs in the film include "A Dream Is a Wish Your Heart Makes", "Bibbidi-Bobbidi-Boo", "So This Is Love", "Sing Sweet Nightingale", "The Work Song", and "Cinderella".

At the time,   and  .
 Henry V, Thor (film)|Thor) and starring Lily James as Cinderella and Cate Blanchett as Lady Tremaine was released on March 13, 2015.   

==Plot==
 
  and List of Disneys Cinderella characters#Anastasia Tremaine|Anastasia. After her father dies unexpectedly, Cinderella is abused and mistreated by her stepfamily, who take over the estate and ultimately reduce her to being a scullery maid in her own home. Despite this, Cinderella grows into a kind and gentle young woman, befriending the animals in the barn and the mice and birds who live around the chateau.

One day, while Cinderella is preparing breakfast, Lady Tremaines wicked cat Lucifer chases List of Disneys Cinderella characters#Jaq and Gus|Gus, one of the mice, into the kitchen. Cinderella delivers breakfast to her stepfamily, unaware that Gus is hiding under Anastasias teacup. The angry Anastasia tells her mother of the apparent joke, and Tremaine punishes Cinderella with extra chores.
 ball in the Prince without arousing suspicion. Cinderella asks her stepmother if she can attend, as the invitation says "every eligible maiden" is to attend. Lady Tremaine agrees, provided that Cinderella finishes her chores and finds a nice dress to wear. Cinderellas animal friends, led by List of Disneys Cinderella characters#Jaq and Gus|Jaq, Gus and the other mice, fix up a gown that belonged to Cinderellas mother using beads and a sash thrown out by Drizella and Anastasia, respectively. When Cinderella comes down wearing her new dress, Lady Tremaine compliments the gown, pointing out the beads and the sash. Angered by the apparent theft of their discarded items, the stepsisters destroy the gown.
 Fairy Godmother Bruno into a footman. Cinderellas godmother warns her that the spell will break at the stroke of midnight. At the ball, the Prince rejects every girl until he sees Cinderella. The two fall strongly in love and dance alone throughout the castle grounds until the clock starts to chime midnight. Cinderella flees to her coach and away from the castle, dropping one of her glass slippers by accident. After her gown turns back into rags, the mice point out that the other slipper is still on her foot.

Back at the castle, the Duke tells the King of the Princes meeting with the unknown girl. The King, after hearing that the girl disappeared, and thinking that the Duke was "in league with the Prince all along", goes into a rage and tries to behead him. Fortunately, the Duke is able to calm him down with news of the girls glass slipper and states that the Prince will only marry the girl who fits that slipper.

The next morning, the King proclaims that the Grand Duke will visit every house in the kingdom to find the girl whose foot fits the glass slipper. When news reaches Cinderellas household, her stepmother and stepsisters prepare for the Dukes arrival. Overhearing this, Cinderella dreamingly hums the song played at the ball. Realizing that Cinderella was the girl who danced with the Prince, Lady Tremaine locks her in the attic.

When the Duke arrives, Jaq and Gus steal the key to Cinderellas room, but Lucifer ambushes them before they can free her. With the help of the other animals and Bruno, they chase him out the window and Cinderella is freed. As the Duke prepares to leave after the stepsisters have tried to shove their enormous feet into the slipper, Cinderella appears and requests to try it on. Knowing the slipper will fit, Lady Tremaine trips the footman, causing him to drop the slipper, which shatters on the floor. Cinderella then produces the other glass slipper, much to her stepmothers horror. A delighted Duke slides the slipper onto her foot, and it fits perfectly.
 happily ever after.

==Cast== Cinderella
* Eleanor Audley as Lady Tremaine Grand Duke and the King Jimmy MacDonald Bruno
* William Phipps Prince Charming Prince Charmings singing voice Anastasia Tremaine Drizella Tremaine Fairy Godmother Don Barclay as Footman Lucifer
* Betty Lou Gerson as Narrator

==Directing animators== Marc Davis, Eric Larson (Cinderella (Disney character)|Cinderella)
* Milt Kahl (Prince Charming, Fairy Godmother, Grand Duke, the King) Frank Thomas (Lady Tremaine)
* Ollie Johnston (Anastasia Tremaine, Drizella Tremaine)
* Ward Kimball, John Lounsbery (Jaq, Gus, Lucifer, Bruno)
* Wolfgang Reitherman
* Les Clark Norm Ferguson Fred Moore

==Production==
Made on the cusp between the classic "golden age" Disney animations of the 1930s and 1940s and the less critically acclaimed productions of the 1950s, Cinderella is representative of both eras.

Cinderella was the first full-bodied feature produced by the studio since Bambi (1942 film)|Bambi in 1942; World War II and low box office returns had forced Walt Disney to produce a series of inexpensive package films such as Make Mine Music and Fun and Fancy Free for the 1940s. Live action reference was used extensively to keep animation costs down. According to Laryn Dowel, one of the directing animators of the film, roughly 90% of the film was done in live action model before animation, using basic sets as references for actors and animators alike.
 Sleeping Beauty William Phipps acted the part.

In earlier drafts of the screenplay, the Prince originally played a larger role and had more character development than what he ultimately received in the final version of the film. In one abandoned opening, the Prince was shown hunting a deer, but at the end of the sequence, it was to be revealed that the Prince and the deer were actually friends playing a game. In an abandoned alternate ending, after the Duke discovered Cinderellas identity, she was shown being brought to the castle to be reintroduced to the Prince, who is surprised to learn that Cinderella was actually a modest servant girl instead of the princess he thought she was, but the Princes feelings for her were too strong to be bothered by this and he embraced her; the Fairy Godmother was to reappear and restore Cinderellas ball gown for the closing shot. Walt Disney himself reportedly cut the alternate ending because he felt it was overlong and did not give the audience its "pay off", but the scene would later be incorporated in the video game,  .
 cloned into an army to divide up the work while pondering what the ball itself will be like. The sequence was cut, but the title was applied to the song the mice sing when they work on Cinderellas dress. Additionally, there was a scene that took place after the ball in which Cinderella was seen returning to her home and eavesdropped on her stepfamily, who were ranting about the mystery girl at the ball, and Cinderella was shown to be amused by this because they were talking about her without realizing it. Walt Disney reportedly cut the scene because he thought it made Cinderella look "spiteful" and felt the audience would lose sympathy for her.

For the first time, Walt turned to Tin Pan Alley song writers to write the songs. The music of Tin Pan Alley would later become a recurring theme in Disney animation.  Cinderella was the first Disney film to have its songs published and copyrighted by the newly created Walt Disney Music Company. Before movie soundtracks became marketable, movie songs had little residual value to the film studio that owned them and were often sold off to established music companies for sheet music publication.

The song "Bibbidi-Bobbidi-Boo" became a hit single on four occasions, including a cover version recorded by Perry Como and the Fontane Sisters. Woods beat exactly 309 girls for the part of Cinderella, after some demo recordings of her singing a few of the films songs were presented to Walt Disney. However, she had no idea she was auditioning for the part until Disney contacted her; she initially made the recordings for a few friends who sent them to Disney without her knowledge. Reportedly, Disney thought Woods had the right "fairy tale" tone to her voice.

Interestingly, almost 30 years before he made "Cinderella" into a feature-length animated film, Walt Disney already made a short film of it as the last of the Laugh-O-Gram Studio|Laugh-O-Gram series, as a Roaring 20s version. This short is included as an extra on the Cinderella Platinum Edition DVD.

During production, Walt Disney pioneered the use of double tracked vocals for the song "Sing Sweet Nightingale", before it had been used by artists in studio recordings such as the Beatles.  When Ilene Woods had completed the days recording of "Sing Sweet Nightingale", Walt listened and asked her if she could sing harmony with herself. She was apprehensive about the idea as it was unheard of; though she ended up singing the double recording, including second and third part harmonies. Ilene Woods reveals the innovation in an interview. 

===Music===
* "Cinderella" – The Jud Conlon Chorus, Marni Nixon
* "A Dream Is a Wish Your Heart Makes" – Cinderella
* "Oh, Sing Sweet Nightingale" – Drizella, Cinderella
* "The Work Song" – The Mice
* "Bibbidi-Bobbidi-Boo" – The Fairy Godmother
* "So This Is Love" – Cinderella, Prince Charming
* "So This Is Love (reprise)" – Cinderella
* "A Dream Is a Wish Your Heart Makes (reprise)" – The Jud Conlon Chorus
 the first the second.

===Soundtrack===
{{Infobox album   
| Name        = Cinderella
| Type        = soundtrack
| Artist      = Various artists
| Cover       =
| Released    = February 4, 1997
| Recorded    = 
| Genre       = 
| Length      =  Walt Disney
| Producer    =
| Chronology  = 
| Last album  = 
| This album  = 
| Next album  = 
}}
The soundtrack for Cinderella was first released by   was released on the same day. 

 
{{Track listing
| collapsed       = 
| headline        = 
| total_length    = 
| all_writing     = Mack David, Jerry Livingston, Al Hoffman
| lyrics_credits  = 
| music_credits   = 
| extra_column    = Performer(s)
| title1          = Cinderella (Main Title)
| writer1         = 
| extra1          = The Jud Conlon Chorus; Marni Nixon
| length1         = 2:52
| title2          = A Dream Is a Wish Your Heart Makes
| extra2          = Ilene Woods
| length2         = 4:34
| title3          = A Visitor/Caught in a Trap/Lucifer/Feed the Chickens/Breakfast is Served/Time on Our Hands
| extra3          = Oliver Wallace; Paul J. Smith
| length3         = 2:11
| title4          = The Kings Plan
| extra4          = Paul J. Smith; Oliver Wallace
| length4         = 1:22
| title5          = The Music Lesson/Oh, Sing Sweet Nightingale/Bad Boy Lucifer/A Message from His Majesty
| extra5          = Rhoda Williams; Ilene Woods; Paul J. Smith; Oliver Wallace
| length5         = 2:07
| title6          = Little Dressmakers/The Work Song/Scavenger Hunt/A Dream Is a Wish Your Heart Makes/The Dress/My Beads/Escape to the Garden
| extra6          = James MacDonald; Oliver Wallace; Paul J. Smith
| length6         = 9:24
| title7          = Where Did I Put That Thing?/Bibbidi-Bobbidi-Boo
| extra7          = Verna Felton; Paul J. Smith; Oliver Wallace
| length7         = 4:48
| title8          = Reception at the Palace/So This Is Love
| extra8          = Ilene Woods; Paul J. Smith; Mike Douglas; Oliver Wallace
| length8         = 5:45
| title9          = The Stroke of Midnight/Thank You Fairy Godmother
| extra9          = Oliver Wallace; Paul J. Smith
| length9         = 2:05
| title10         = Locked in the Tower/Gus and Jaq to the Rescue/Slipper Fittings/Cinderellas Slipper/Finale
| extra10         = Oliver Wallace; Paul J. Smith
| length10        = 7:42
| title11         = Im In The Middle Of A Muddle
| note11          = Demo Recording
| length11        =
}} 
{{Track listing
| collapsed       = yes
| headline        = 2005 Special Edition
| total_length    = 56:58
| all_writing     = Mack David, Jerry Livingston, Al Hoffman, except track 12 written and composed by Larry Morey, Charles Wolcott and track 13 written and composed by Jim Brickman, Jack Kugell, Jamie Jones
| lyrics_credits  = 
| music_credits   = 
| extra_column    = Performer(s)
| title1          = Cinderella (Main Title)
| writer1         = 
| extra1          = The Jud Conlon Chorus; Marni Nixon
| length1         = 2:52
| title2          = A Dream Is a Wish Your Heart Makes
| extra2          = Ilene Woods
| length2         = 4:34
| title3          = A Visitor/Caught in a Trap/Lucifer/Feed the Chickens/Breakfast is Served/Time on Our Hands
| extra3          = Oliver Wallace; Paul J. Smith
| length3         = 2:11
| title4          = The Kings Plan
| extra4          = Paul J. Smith; Oliver Wallace
| length4         = 1:22
| title5          = The Music Lesson/Oh, Sing Sweet Nightingale/Bad Boy Lucifer/A Message from His Majesty
| extra5          = Rhoda Williams; Ilene Woods; Paul J. Smith; Oliver Wallace
| length5         = 2:07
| title6          = Little Dressmakers/The Work Song/Scavenger Hunt/A Dream Is a Wish Your Heart Makes/The Dress/My Beads/Escape to the Garden
| extra6          = James MacDonald; Oliver Wallace; Paul J. Smith
| length6         = 9:24
| title7          = Where Did I Put That Thing/Bibbidi-Bobbidi-Boo
| extra7          = Verna Felton; Paul J. Smith; Oliver Wallace
| length7         = 4:48
| title8          = Reception at the Palace/So This Is Love
| extra8          = Ilene Woods; Paul J. Smith; Mike Douglas; Oliver Wallace
| length8         = 5:45
| title9          = The Stroke of Midnight/Thank You Fairy Godmother
| extra9          = Oliver Wallace; Paul J. Smith
| length9         = 2:05
| title10         = Locked in the Tower/Gus and Jaq to the Rescue/Slipper Fittings/Cinderellas Slipper/Finale
| extra10         = Oliver Wallace; Paul J. Smith
| length10        = 7:42
| title11         = Im in the Middle of a Muddle (Demo Recording)
| length11        = 1:55
| title12         = Dancing on a Cloud (Demo Recording)
| extra12         = Ilene Woods; Mike Douglas
| length12        = 3:49
| title13         = Beautiful
| extra13         = Jim Brickman; Wayne Brady
| length13        = 3:43
| title14         = A Dream Is a Wish Your Heart Makes
| extra14         = Kimberly Locke
| length14        = 4:41
}}
{{Track listing
| collapsed       = yes
| headline        = 2012 Collectors Edition
| total_length    =
| all_writing     = Mack David, Jerry Livingston, Al Hoffman
| lyrics_credits  = 
| music_credits   = 
| extra_column    = Performer(s)
| title1          = Main Title/Cinderella
| extra1          = The Jud Conlon Chorus; Marni Nixon
| length1         = 2:52
| title2          = A Dream Is a Wish Your Heart Makes
| extra2          = Ilene Woods
| length2         = 4:34
| title3          = A Visitor/Caught in a Trap/Lucifer/Feed the Chickens/Breakfast is Served/Time on Our Hands
| extra3          = Oliver Wallace; Paul J. Smith
| length3         = 2:11
| title4          = The Kings Plan
| extra4          = Paul J. Smith; Oliver Wallace
| length4         = 1:22
| title5          = The Music Lesson/Oh, Sing Sweet Nightingale/Bad Boy Lucifer/A Message from His Majesty
| extra5          = Rhoda Williams; Ilene Woods; Paul J. Smith; Oliver Wallace
| length5         = 2:07
| title6          = Little Dressmakers/The Work Song/Scavenger Hunt/A Dream Is a Wish Your Heart Makes/The Dress/My Beads/Escape to the Garden
| extra6          = James MacDonald; Oliver Wallace; Paul J. Smith
| length6         = 9:24
| title7          = Where Did I Put That Thing?/Bibbidi-Bobbidi-Boo
| extra7          = Verna Felton; Paul J. Smith; Oliver Wallace
| length7         = 4:48
| title8          = Reception at the Palace/So This Is Love
| extra8          = Ilene Woods; Paul J. Smith; Mike Douglas; Oliver Wallace
| length8         = 5:45
| title9          = The Stroke of Midnight/Thank You Fairy Godmother
| extra9          = Oliver Wallace; Paul J. Smith
| length9         = 2:05
| title10         = Locked in the Tower/Gus and Jaq to the Rescue/Slipper Fittings/Cinderellas Slipper/Finale
| extra10         = Oliver Wallace; Paul J. Smith
| length10        = 7:42
| title11         = Im In The Middle of a Muddle 
| note11          = Lost Chords) (Demo
| length11        =
| title12         = Im In The Middle of a Muddle 
| note12          = Lost Chords) (New Recording
| length12        =
| title13         = I Lost My Heart At the Ball 
| note13          = Lost Chords) (Demo
| length13        =
| title14         = I Lost My Heart At the Ball
| note 14         = Lost Chords) (New Recording
| length14        =
| title15         = The Mouse Song
| note15          = Lost Chords) (Demo
| length15        =
| title16         = The Mouse Song
| note16          = Lost Chords) (New Recording
| length16        =
| title17         = Sing a Little, Dream A Little
| note17          = Lost Chords) (Demo
| length17        = 
| title18         = Sing a Little, Dream A Little
| note18          = Lost Chords) (New Recording
| length18        = 
| title19         = Dancing On A Cloud
| note19          = Lost Chords) (Demo
| length19        = 
| title20         = Dancing On A Cloud
| note20          = Lost Chords) (New Recording
| length20        = 
| title21         = The Dress My Mother Wore
| note21          = Lost Chords) (Demo
| length21        = 
| title22         = The Dress My Mother Wore
| note22          = Lost Chords) (New Recording
| length22        = 
| title23         = The Face That I See In the Night
| note23          = Lost Chords) (Demo
| length23        = 
| title24         = The Face That I See In the Night
| note24          = Lost Chords) (New Recording
| length24        = 
}}
{{Track listing
| collapsed       = yes
| headline        = 2012 Limited Edition Music Box Set
| total_length    =
| all_writing     = Mack David, Jerry Livingston, Al Hoffman
| lyrics_credits  = 
| music_credits   = 
| extra_column    = Performer(s)
| title1          = Main Title/Cinderella
| extra1          = The Jud Conlon Chorus; Marni Nixon
| length1         = 2:52
| title2          = A Dream Is a Wish Your Heart Makes
| extra2          = Ilene Woods
| length2         = 4:34
| title3          = A Visitor/Caught in a Trap/Lucifer/Feed the Chickens/Breakfast is Served/Time on Our Hands
| extra3          = Oliver Wallace; Paul J. Smith
| length3         = 2:11
| title4          = The Kings Plan
| extra4          = Paul J. Smith; Oliver Wallace
| length4         = 1:22
| title5          = The Music Lesson/Oh, Sing Sweet Nightingale/Bad Boy Lucifer/A Message from His Majesty
| extra5          = Rhoda Williams; Ilene Woods; Paul J. Smith; Oliver Wallace
| length5         = 2:07
| title6          = Little Dressmakers/The Work Song/Scavenger Hunt/A Dream Is a Wish Your Heart Makes/The Dress/My Beads/Escape to the Garden
| extra6          = James MacDonald; Oliver Wallace; Paul J. Smith
| length6         = 9:24
| title7          = Where Did I Put That Thing/Bibbidi-Bobbidi-Boo
| extra7          = Verna Felton; Paul J. Smith; Oliver Wallace
| length7         = 4:48
| title8          = Reception at the Palace/So This Is Love
| extra8          = Ilene Woods; Paul J. Smith; Mike Douglas; Oliver Wallace
| length8         = 5:45
| title9          = The Stroke of Midnight/Thank You Fairy Godmother
| extra9          = Oliver Wallace; Paul J. Smith
| length9         = 2:05
| title10         = Locked in the Tower/Gus and Jaq to the Rescue/Slipper Fittings/Cinderellas Slipper/Finale
| extra10         = Oliver Wallace; Paul J. Smith
| length10        = 7:42
}}

==Release==
The film was originally released in theaters on February 15, 1950 in Boston, Massachusetts,  followed by theatrical re-releases in 1957, 1965, 1973, 1981, and 1987.   Cinderella also played a limited engagement in select Cinemark Theatres from February 16–18, 2013. 

===Home media=== moratorium on Diamond Edition in October 2, 2012 in a 3-disc Blu-ray/DVD/Digital Copy Combo, a 2-disc Blu-ray/DVD combo and in a 6-disc "Jewelry Box Set" that includes the first film alongside its two sequels. A 1-disc DVD edition was released on November 20, 2012. 

===Reception===
Cinderella currently has a score of 97% on Rotten Tomatoes. The overview of the film is, "The rich colors, sweet songs, adorable mice and endearing (if suffering) heroine make Cinderella a nostalgically lovely charmer".

The profits from the films release, with the additional profits from record sales, music publishing, publications and other merchandise gave Disney the cash flow to finance a slate of productions (animated and live action), establish his own distribution company, enter television production and begin building Disneyland during the decade.
 Snow White and the Seven Dwarfs. The production of this film was regarded as a major gamble on his part. At a cost of nearly $3 million, Disney insiders claimed that if Cinderella failed at the box office, then the Disney studio would have closed (given that the studio was already heavily in debt).  The film was a huge box office success and allowed Disney to carry on producing films throughout the 1950s.  It was the 5th most popular movie at the British box office in 1951. 

===Awards=== Best Sound Best Song Golden Bear (Music Film) award and the Big Bronze Plate award.   
 10 Top 10"— the best ten films in ten "classic" American film genres—after polling over 1,500 people from the creative community. Cinderella was acknowledged as the 9th greatest film in the animation genre.  

American Film Institute recognition:
* AFIs 100 Years...100 Movies – Nominated
* AFIs 100 Years...100 Passions – Nominated
* AFIs 100 Years...100 Heroes and Villains:
** Lady Tremaine (Stepmother) – Nominated Villain
* AFIs 100 Years...100 Songs:
** Bibbidi-Bobbidi-Boo – Nominated
** A Dream Is A Wish Your Heart Makes – Nominated
* AFIs Greatest Movie Musicals – Nominated
* AFIs 100 Years...100 Movies (10th Anniversary Edition) – Nominated
* AFIs 10 Top 10 – #9 Animated film

==Sequels and other media==
* A   was released on February 26, 2002.
* A second direct-to-video sequel   was released on February 6, 2007.
* Cinderella and the Fairy Godmother have appeared as guests in Disneys House of Mouse.
* Cinderella and the Fairy Godmother appear in the video game  . All the main characters except Gus and the King appear. stage musical version of the film known as Disneys Cinderella KIDS is frequently performed by schools and childrens theaters.  2015 live-action re-imagining of the film directed by Kenneth Branagh was released on March 13, 2015; starring Lily James, Richard Madden, Cate Blanchett, Helena Bonham Carter, Holliday Grainger, Sophie McShera, Stellan Skarsgård, Derek Jacobi, Hayley Atwell, and Ben Chaplin.

==See also==
 
*List of animated feature films
*List of Disney animated films based on fairy tales
*List of Disney theatrical animated features

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 