Hum Tum Shabana
{{Infobox film
| name           = Hum Tum Shabana
| image          = hum tum shabana.jpg
| caption        = Theatrical release poster
| director       = Sagar Ballary
| producer       = Sunil Chainani Sameer chand Srivastava Subhash Dawar
| writer         = 
| screenplay     = Farhajaan Sheikh
| story          = Farhajaan Sheikh
| based on       =  
| starring       = Tusshar Kapoor Shreyas Talpade Minissha Lamba
| music          = Sachin-Jigar
| cinematography = Anirudh Garbyal
| editing        = Suresh Pai
| studio         = 
| distributor    = Horseshoe Pictures Pvt. Ltd Alliance Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Hum Tum Shabana is a 2011 Indian comedy and romance film. It stars Tusshar Kapoor, Shreyas Talpade, and Minissha Lamba in lead roles. 

==Plot==
Hum Tum Shabana is a comedy movie revolving around three characters, played by Tusshar Kapoor, Shreyas Talpade and Minissha Lamba. The two men compete to woo the woman, but then it turns into a desperate battle to lose her. Despite gaining negative reviews the film did good business and was declared an above average grosser.  

==Cast==
* Tusshar Kapoor as Rishi Malhotra
* Shreyas Talpade as Karthik Iyer
* Minissha Lamba as Shabana Raza Sanjay Mishra as Munna Military (Manoj Tyage)
* Mantra as Nandu
* Satish Kaushik as Chacha Panju
* Pia Trivedi as Rhea Dixit Rahul Singh as Ravi Aggarwal
* Rajesh Khattar as Vikram Malhotra
* Pooja Batra as Puja (Cameo Appearance)
* Surveen Chawla as Shabana (Cameo Appearance)
* Madhur Bhandarkar as himself (Cameo Appearance)

==Soundtrack==

The music of the film is composed by Sachin-Jigar.

{{Infobox album
| Name       = Hum Tum Shabana
| Type       = soundtrack
| Artist     = Sachin-Jigar
| Cover      =
| Released   =  
| Recorded   =
| Genres     = Bollywood Music
| Label      =
| Producer   =
| Last album = F.A.L.T.U. (2011)
| This album = Hum Tum Shabana (2011)
| Next album = Shor in the City (2011)
}}

1. Musik Bandh Na Karo - Abhishek Nailwal, Palash Sen,  Anushka Manchanda

2. Hey Na Na Shabana - Raghav Mathur	

3. Thank U Mr DJ - Mika Singh, Suzanna DMello
	
4. Piya Kesariyo -  Anushka Manchanda, Jigar Saraiya
	
5. Kaari Kaari -  
	
6. Hey Na Na Shabana (Party Map Remix) Remix - Dj Akhil Talreja
	
7. Musik Bandh Na Karo (Remix)  Remix - Kiran Kamath

== Reception ==

=== Critical response ===
Hum Tum Shabana received negative reviews all throughout. Preeti Arora of rediff.com said, "... an unashamed attempt to ape the Golmaal series, Hum Tum Shabana is a rudderless ship which simultaneously endeavors to straddle many genres. And fails miserably," and gave it half a star out of 5 stars. http://www.rediff.com/movies/review/review-hum-tum-shabana-is-best-avoided/20110930.htm
  The Indian Express rated it half a star as well.  Popular film critic and trade analyst Taran Adarsh, who gave it 1.5 out of 5 stars, said that it has a funny first half but a disappointing second hour.  
Nupur Barua of fullhyd.com lamented the fact that Sagar Ballary, the director, "seems to prefer the Golmaal and Dhamaal styles, rather than that of his first film  ." fullhyd.com rated the movie 4/10.  Times Of Indias Nikhat Kazmi wasnt positive about the film either, and rated it 2/5. 

==References==
 

==External links==
*  
*  

 
 
 
 