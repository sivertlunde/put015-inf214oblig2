Ansel Adams: A Documentary Film
{{Infobox film
  | name     = Ansel Adams: A Documentary Film
  | image    = Ansel Adams and camera.jpg
  | caption  = Ansel Adams and Camera.
  | director = Ric Burns
  | producer = Marilyn Ness Ric Burns
  | writer   = Ric Burns
  | starring =
  | cinematography = Buddy Squires Michael Chin Jon Else
  | music    = Brian Keane
  | editing  =	Li-Shin Yu Sierra Club Productions
  | released = 2002
  | runtime  = 100 minutes English
  | budget   =
}}
 Josh Hamilton, Barbara Feldon and Eli Wallach.

==Production==
 Carl Pope, Alex Ross, John Sexton, Jonathan Spaulding, Andrea Gray Stillman, William A. Turnage and John Szarkowski.

Director Ric Burns shot portions of the film in the same landscapes that were the settings for Adams most iconic images. Said Burns, "to get to the heart of what so inspired Ansel Adams, we literally followed in his footsteps. We lugged our cameras up sheer rock faces and hiked the winding trails that led Ansel to his photographic revelations. And they led us to Ansel."  

==Critical reception==
Variety’s Laura Fries wrote: “Burns creates a visually mesmerizing retrospective of Adams’s career...the film examines the inspirations and intentions of the artist who transcended the medium to become an American folk hero.” 

==Awards==

The Alfred I. duPont-Columbia Silver Baton Award 2001-2002 
 Emmy Award for Outstanding Cultural and Artistic Programming 2002 

==References==
 

==External links==
* http://www.pbs.org/wgbh/amex/ansel

 
 
 
 
 
 
 

 