Don't Play with Tigers
{{Infobox film
 | name = Dont Play with Tigers
 | image = Dont Play with Tigers.jpg
 | caption =
 | director = Sergio Martino 
 | writer = Sergio Martino   Alberto Silvestri   Castellano & Pipolo
 | starring =  
 | music = Detto Mariano
 | cinematography = Giancarlo Ferrando
 | editing =  
 | language = Italian 
 }}
Dont Play with Tigers (  Italian comedy film directed by Sergio Martino.      

== Plot ==
Three segments. Cesare Domenichini is a poor man who, to bring the family to the sea, builds an abusive hut on a nudist beach. The businessman Mario Zamboni, on vacation in Livorno with his wife and his daughter Aurora, gives in to the advances of Frau Kruppe, a rich German woman with a passion for card games. Alberto Del Pra, owner of a shipyard on the brink of bankruptcy, is able to get the assignment to build a super yacht for a wealthy Arab emir, on condition that his wife Francesca would stay a night with him.

== Cast ==

* Pippo Franco: Cesare Domenichini
* Lino Banfi: Mario Zamboni
* Renato Pozzetto: Alberto Del Prà
* Edwige Fenech: Francesca Del Prà
* Janet Agren: Frau Kruppe aka Evelina Krugher
* Adriana Russo: Maria Domenichini 
* Néstor Garay: Kuzz Viller aka Federico Partibòn George Hilton: Sheik Omar Abdul Youssef El Rāchid 
* Daniele Formica: Akim
* Annabella Schiavone: Adalgisa Cavallari 
* Pippo Santonastaso: Praetor   Riccardo Garrone: Admiral Ulderisi

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 