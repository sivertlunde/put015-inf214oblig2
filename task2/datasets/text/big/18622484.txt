The Daffy Duckaroo
 
{{Infobox Hollywood cartoon|
| cartoon_name = The Daffy Duckaroo
| series = Looney Tunes/Daffy Duck
| image =
| caption =
| director = Norman McCabe   
| story_artist = Melvin Millar
| animator = Cal Dalton
| voice_actor = Mel Blanc
| musician = Carl W. Stalling
| producer = Leon Schlesinger
| distributor = Warner Bros. Pictures
| release_date = October 24, 1942
| color_process = black, & white
| runtime = 7 min
| movie_language = English
}} crooning cowboy film star. The film is set in the American West.

==Plot== Indian encampment. He is about to run away when he is wooed by an Indian girl. He serenades her and follows her into her teepee.

The Indian girl says she would love to be Daffys girlfriend, but her boyfriend Little Beaver will never allow it. When Little Beaver arrives, Daffy hides in a dresser and emerges disguised as an Indian girl himself. Little Beaver attempts to kiss him until he discovers the disguise.
 Painted Desert Petrified Forest until he calls for aid with smoke signals. The Indians surround Daffys trailer and remove the tires. One Indian promptly returns them saying the tires do not fit his vehicle.

==Cultural references==
* Daffy Ducks character is based on the archetypal cowboy crooner (i.e., Gene Autry), a common trend in American film at the time.
* Hooray for Hollywood is heard in the background score as images of newspapers announce Daffys retirement from show business.
* While he rides a donkey, Daffy sings My Little Buckaroo, a song written by M.K. Jerome and Jack Scholl and recorded by many artists, including Bing Crosby. Would You Like to Take a Walk? (Sumpn Goodll Come From That), a song by Mort Dixon and Billy Rose with music by Harry Warren.
* When Little Beaver first meets Daffy Duck (in disguise), the popular tune Playmates is heard in the background score.
* California, Here I Come, a song recorded by Al Jolson and common in Warner Bros. cartoons, is heard in the background score when the camera shows a sign reading: Los Angeles City Limits. Ludwig van Symphony No. 5 is heard in the background score.
* Little Beaver frequently places a "-um" at the end of his words, i.e. follow-um car!. This is a common racial stereotype of Native Americans.

==References==
 

==External links==
*  
* Free download of the PD original version (B & W)   at Internet Archive

 
{{succession box |
before= The Impatient Patient | Daffy Duck Cartoons |
years= 1942 |
after= To Duck or Not To Duck|}}
 

 
 
 
 
 


 