The Bedroom Window (1924 film)
{{infobox film
| title          = The Bedroom Window
| image          =
| imagesize      =
| caption        =
| director       = William C. deMille
| producer       = Adolph Zukor Jesse Lasky
| writer         = Clara Beranger(story & scenario)
| starring       = May McAvoy Malcolm McGregor
| cinematography = L. Guy Wilky
| editing        =
| distributor    = Paramount Pictures
| released       = June 15, 1924
| runtime        = 70 minutes; 7 reels
| country        = USA
| language       = Silent film..(English intertitles)
}}
The Bedroom Window is a 1924 mystery or who-dunnit silent film directed by William C. deMille and starring May McAvoy. It was produced by Famous Players-Lasky and distributed through Paramount Pictures.  

The film still exists and preserved at the Library of Congress.  

==Cast==
*May McAvoy - Ruth Martin
*Malcolm McGregor - Frank Armstrong
*Ricardo Cortez - Robert Delano
*Robert Edeson - Frederick Hall
*George Fawcett - Silas Tucker
*Ethel Wales - Matilda Jones Charles Ogle - Butler
*Medea Radzina - Sonya Malisoff
*Guy Oliver - Detective

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 