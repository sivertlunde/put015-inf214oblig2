Department (film)
 
 
{{Infobox film
| name = Department
| image = Department movie poster.jpg
| caption = Theatrical release poster
| director = Ram Gopal Varma
| written by = Nilesh Girkar
| producer = Siddhant Oberoi Amit Sharma
| starring = Amitabh Bachchan Sanjay Dutt Rana Daggubati Vijay Raaz Abhimanyu Singh Deepak Tijori Lakshmi Manchu Anjana Sukhani Madhu Shalini
| music = Bappi Lahiri Dharam-Sandeep Vikram Magi
| cinematography =
| editing  = Abhijit Kokate  Vinay Chauhan
| distributor = Viacom 18 Motion Pictures Oberoi Line Productions
| released =  
| country = India Hindi
| budget =  
| gross =    (complete run Days Total) 
}}

Department is a 2012 Indian action film directed by Ram Gopal Varma.   The film stars Sanjay Dutt, Rana Daggubati and Amitabh Bachchan in lead roles.  Several scenes were shot using Canon EOS 5D cameras.   The film released on 18 May 2012, and received mixed response from critics.  

==Plot==
When Inspector Mahadev Bhosale (Sanjay Dutt) is asked by his seniors to form a hit squad to take on the Mumbai underworld, he recruits Shiv Narayan (Rana Daggubati), an honest and brave police officer who had been suspended for an encounter killing. Together, Mahadev and Shiv along with others in the "Department" take on underworld don Sawatya (Vijay Raaz) and his gang. Sawatya, in spite of many exhortations by his number two, D.K. (Abhimanyu Singh), and D.K.’s feisty girlfriend, Naseer (Madhu Shalini), does not hit back at the police.

There is also the gangster-turned-minister, Sarjerao Gaikwad (Amitabh Bachchan), who takes Shiv under his wing after Shiv saves his life at a public rally. Mahadev warns Shiv that Sarjerao is just using him. After a certain turn of events, it is exposed that Mahadev is actually working for Mohammad Gauri, who is actually an underworld don, and is, at the ganglord’s behest, finishing off Sawatya’s gang. While Shiv declines to be a part of Mahadev’s corrupt world, and Mahadev agrees to let him be, things start falling apart between the mentor and the apprentice when Shiv starts taking on a sub-gang formed by D.K., under Mahadev’s protection.

What happens next? Do Mahadev and Shiv come to loggerheads? What about the Sawatya and Mohammad Gauri gangs? What role does Sarjerao play in the final outcome of all this? The rest of the drama answers these questions.

==Cast==
* Amitabh Bachchan as Sarjerao Gaikwad, a gangster-turned politician
* Sanjay Dutt as Inspector Mahadev Bhosle, the DSP of an encounter squad
* Rana Daggubati as Inspector Shivnarayan, an encounter specialist
* Vijay Raaz as Sawatya, Mumbai Don
* Abhimanyu Singh as D.K., Right-hand man of Sawatya
* Deepak Tijori as Inspector Danaji, an honest member of the department
* Lakshmi Manchu as Satya Bhosle (Mahadevs Wife)
* Anjana Sukhani as Bharati (Shivnarayans Wife)
* Madhu Shalini as Naseer, D. K.s gun moll
* Neeraj Vora as Lalchand and Valchand

==Written by==
* Nilesh Girkar (Story, Screenplay, & Dialogue)

==Soundtrack==
{{Infobox album| 
| Name = Department
| Type = soundtrack
| Artist = Bappi Lahiri Dharam-Sandeep Vikram Magi
| Cover =  
| Released =
| Recorded =
| Producer = Feature film soundtrack
| Length =
| Label = T-Series
}}
The soundtrack is composed by Bappi Lahiri along with Dharam-Sandeep and Vikram Magi. The music received negative response worldwide.

===Track List===
{| class="wikitable"
|- style="background:#ccc; text-align:center;"
! Track # !! Song !! Singer(s)!! Duration
|-
| 1
|"Dan Dan Cheeni Shoot Mix"
| Paroma P.Dasgupta, Ravi, Sandeep Patil
| 4:05
|-
| 2
| "Kammo"
| Mika Singh, Sudesh Bhosle
| 5:18
|-
| 3
| "Theme Of Department – Ek Do Teen Chaar"
| Sandeep Patil
| 4:11
|-
| 4
| "Bad Boys"
| Ritu Pathak, Earl
| 4:05
|-
| 5
| "Mumbai Police"
| Sanjay Dutt, Farhad Bhiwandiwala
| 4:35
|}

==References==
 
* http://ibnlive.in.com/news/varmas-department-clash-of-the-titans/185104-8-66.html
==External links==
 
*  


 

 
 
 
 
 