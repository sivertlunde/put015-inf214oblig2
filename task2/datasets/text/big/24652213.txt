The Lost Continent (1968 film)
 
 
:This article is about a 1968 film. For other uses, see The Lost Continent (disambiguation).

{{Infobox film
| name           = The Lost Continent
| image          = Lost_Continent_1968.jpg
| image_size     =
| caption        = Film poster
| director       = Michael Carreras
| producer       = Michael Carreras
| writer         = Michael Nash aka Michael Carreras
| based on       =  
| narrator       =
| starring       = Eric Porter Hildegard Knef Suzanna Leigh Tony Beckley
| music          = Soundtrack  Gerard Schürmann   Songs The Peddlers
| cinematography = Paul Beeson
| editing        = James Needs Hammer Films, Seven Arts Productions
| distributor    = Warner-Pathé Distributors|Warner-Pathé (UK) 20th Century Fox (US)
| released       =   
| runtime        = 91 mins
| country        = United Kingdom
| language       = English
| budget         = Over £500,000 
| gross          =
}}
 Seven Arts Dennis Wheatleys  novel Uncharted Seas (1938).

The film sees the crew and passengers of the dilapidated tramp steamer Corita heading from Freetown to Caracas. While the passengers all have their own reasons for getting out of Africa, the captain of the ship is also eager to leave, as he is smuggling a dangerous explosive cargo. Whilst en route to South America the ship is holed and eventually whats left of the crew and passengers find themselves marooned in a mist-enshrouded Sargasso Sea surrounded by killer seaweed, murderous crustaceans and previously marooned descendants of Spanish Conquistadores and pirates.

== Plot == pan shot burial rites over a coffin. The coffin is subsequently ditched overboard and the captain asks What happened to us? How did we all get here...? The film then cuts back in time to previous events.
 bridge of the tramp steamer Corita, Captain Lansen (Eric Porter) orders his crew to avoid the repeated requests of a customs launch from the port of Freetown to stop for inspection. The captain orders the ship full steam ahead and to avoid the usual shipping lanes on its way to Caracas.
 Nigel Stock) and his daughter Unity (Suzanna Leigh) for his indiscretions with patients, an alcoholic conman Harry Tyler (Tony Beckley), and Ms Eva Peters (Hildegard Knef) who has stolen bearer bonds to pay for the ransom on her son in Caracas, but who has a lawyer, Ricaldi (Ben Carruthers), after her to retrieve them.

In the hold (ship)|ships hold the crew comes across a large cache of unknown yellow containers. The captain tells the crew to not ask what is inside the containers but that they must be kept dry and handled securely. The captain is also informed of a storm ahead but ploughs on regardless knowing that the passengers do not want to return to the African port.

Captain Lansen informs his First Officer Hemmings (Neil McCullum) that they are transporting a dangerous explosive – Phosphor B (Phosphore Blanc, i.e. white phosphorus) – and he demonstrates in a sink what happens if it comes into contact with water - it explodes violently.
 generator breaks and all power is lost to the pumps.

The crew convince the First Officer that it’s too dangerous on-board and that they need to abandon ship. They overpower Captain Lansen and club the ships Indian chef (Shivendra Sinha) unconscious and take to a lifeboat (shipboard)|lifeboat. The passengers and engine room crew decide to remain on board as the call to abandon ship hasn’t been made by the captain. Whilst attempting to lower the lifeboat, Lansen opens fire on the mutineers and the lifeboat crashes into the sea. The boat survives and the crew row away.

Lansen informs the passengers about the cargo and they help him move it from the flooding storage room. However, the Chief Engineer, Nick (James Cossins) tells Lansen that he cannot fix the generator and the captain decides to abandon ship and gets the remaining crew and passengers into a lifeboat.

The lifeboat survives the storm and the captain tries to maintain morale but arguing breaks out about the supplies and too many people in the lifeboat. The alcoholic Tyler manages to drink a flagon of rum and he and Dr Webster end up in the sea. Dr Webster is devoured by a shark and a fight in the lifeboat sees another crew member shot with a flare gun.
 carnivorous seaweed.

The lifeboat then stumbles into a ship. It transpires to be the Corita with the bartender (Jimmy Hanley) still aboard. They all get aboard the ship but find the propellers are fouled with the seaweed and they are left drifting with the currents. During the night the lawyer is attacked and dragged overboard by a huge octopus.

The next day, a girl called Sarah (Dana Gillespie) appears walking on the weed using large shoes and lighter than air balloons attached to her shoulders. She tells the captain they will be attacked soon and shortly thereafter the ship is attacked by a number of Spanish soldiers/pirates. The crew and passengers fight them off and the remains of the attackers return to a Spanish galleon. On board the galleon we find a child leader – El Supremo (Darryl Read) – the descendent of the Spanish Conquistadores, and members of the Spanish Inquisition who ordered the attack on the Corita to get stores. The failure of the attack sees a member of the attackers thrown into a pit in the ship that contains a sea monster that devours him.

Sarah tells the captain about how her ancestors were trapped in the Sargasso Sea many years before and that they live on an island. They have been at war with the Spanish descendants for many years. Sarah then takes an opportunity to try and return to her island. A few of the crew go after her but get lost in the mist. Sarah finds them but they decide to stay on an island for the night and find the ship in the morning. While on the rock the bartender is attacked and killed by a giant hermit crab which itself is then attacked by a giant scorpion but is killed after it is shot in its eye. Sarah and the remaining members of the Corita crew are then captured by the Spanish and taken to the galleon.

Lansen then appears on the galleon to rescue his crew and tell the Spanish that they can join him rather than be under the control of the religious zealots. Even the child leader El Supremo wants to join Lansen but is killed by the head of the Inquisition. The crew battles some the galleons crew and uses the Phosphor B explosives to set alight the galleon and the seaweed.

The captain, crew and members of the Spanish group who decided to join Lansen then retreat to the Corita. We then return the start of the film with the burial of the child leader. The ship is seen moving through the mist – leaving the viewer to decide whether they are still trapped or sailing away from the Lost Continent.

==Principal cast==
 
* Eric Porter as Captain Lansen
* Hildegard Knef as Eva Peters
* Suzanna Leigh as Unity Webster
* Tony Beckley as Harry Tyler Nigel Stock as Dr Webster Neil McCallum as First Officer Hemmings
* Ben Carruthers as Ricaldi
* Jimmy Hanley as Patrick, bartender
* James Cossins as Nick, chief engineer
* Dana Gillespie as Sarah
* Darryl Read as El Supremo
* Victor Maddern as The mate
* Reg Lye as The Helmsman
* Norman Eshley as Jonathan
* Michael Ripper as Sea Lawyer
 

== Crew ==
*Directed by Michael Carreras
*Produced by Michael Carreras
*Music by Gerard Schürmann and title song by The Peddlers
*Special effects by Robert A Mattey

==Production==
A 175,000 gallon tank was constructed at Elstree Studios to shoot the sea scenes. The credits list Michael Nash — a pseudonym for Michael Carreras — as the screenwriter.
 Leslie Norman, but he was soon replaced by Carreras. Hammers musical director Philip Martell rejected the original film score by Benjamin Frankel and commissioned a new one from Gerald Schumann. 
 X when first released.

==Soundtrack==
The film titles has the song Lost Continent performed by The Peddlers played over them.

==References==
 

==External links== Google videos]
*  
*  

 

 
 
 
 
 
 
 
 
 