Mountain of Destiny
{{Infobox film
| name           = Mountain of Destiny
| image          = Berg des Schicksals 1924 Poster.jpg
| border         = 
| alt            = 
| caption        = German film poster
| director       = Arnold Fanck 
| producer       = Arnold Fanck 
| writer         = Arnold Fanck 
| starring       = {{Plainlist|
* Hannes Schneider
* Frida Richard
* Erna Morena
* Luis Trenker
}}
| music          = Florian C. Reithner  
| cinematography = {{Plainlist|
* Arnold Fanck
* Sepp Allgeier
* Eugen Hamm
* Herbert Oettel
* Hans Schneeberger
}}
| editing        = Arnold Fanck
| studio         = {{Plainlist|
* Berg- und Sportfilm
* Alpenfilm AG, Villars-sur-Glâne
}}
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Germany
| language       = {{Plainlist|
* Silent film
* German intertitles
}}
| budget         = 
| gross          = 
}}
 silent drama The Holy Mountain.

==Plot==
After a mountaineer is killed attempting to climb a difficult mountain, his son dreams of conquering the peak that has defeated his father. But his mother makes him promise never to attempt it. Events eventually force her to release him from the promise, and he ascends the mountain successfully.

==Cast==
* Hannes Schneider as Bergsteiger 
* Frida Richard as Mutter des Bergsteigers 
* Erna Morena as Frau des Bergsteigers 
* Luis Trenker as Sohn des Bergsteigers 
* Gustav Oberg as Freund des Bergsteigers 
* Hertha von Walther as Hella, Tochter des Bergsteigers 
* Werner Schaarschmidt as Unbekannter Kletterer 
* H. von Hoeslin as Unbekannter Kletterer

==References==
;Notes
 
;Bibliography
 
*  
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 