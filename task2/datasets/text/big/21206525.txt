Weather Girl
 
{{Infobox film
| name           = Weather Girl
| image          = Weather girl post.jpg
| caption        = Promotional flm poster
| director       = Blayne Weaver
| producer    = Tricia OKelley
| writer         = Blayne Weaver
| released       =   
| starring       = Tricia OKelley Mark Harmon Jon Cryer Patrick J. Adams Enrico Colantoni Marin Hinkle
| music          = Andrew Hollander
| cinematography = Brandon Trost
| editing = Abe Levy
| studio = Secret Identity Productions Steakhaus Productions
| distributor = Secret Identity Productions
| country        = United States
| gross          = $22,779 (USA) 
| language       = English
}}
Weather Girl is a 2009 comedy film written and directed by Blayne Weaver. The film stars Tricia OKelley, Mark Harmon, Jon Cryer, and Enrico Colantoni.

==Plot==
A Seattle morning show weather girl, after learning her boyfriend has cheated on her, freaks out on-air and quits. Forced to move in with her little brother and to cope with being 35, single and unemployed, she begins an unlikely romance with a younger man—her brothers best friend, Byron.

==Cast==
{| class="wikitable"
|-
! Cast !! Role
|-
| Tricia OKelley || Sylvia
|-
| Mark Harmon || Dale
|-
| Jon Cryer || Charles
|-
| Patrick J. Adams || Byron
|-
| Enrico Colantoni || George
|-
| Ryan Devlin || Walt
|-
| Amie Donegan || Mary
|-
| Timothy Dvorak || Jack
|-
| Kaitlin Olson || Sherry
|-
| Lucas Fleischer || Arthur
|-
| Marin Hinkle || Jane
|-
| Bubba Lewis || Irving
|-
| Jane Lynch || J.D.
|}

==Filming locations==
*Los Angeles, California, USA

== References ==
 
* 
* 
* 

==External links==
*  
*  
*  

 
 
 
 
 
 


 