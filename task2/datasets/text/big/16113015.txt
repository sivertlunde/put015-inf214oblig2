Stella Dallas (1937 film)
 
{{Infobox film
| name           = Stella Dallas
| image          = Stella-dallas-37.jpg
| caption        = Original theatrical poster
| director       = King Vidor
| producer       = Samuel Goldwyn
| writer         = Harry Wagstaff Gribble Gertrude Purcell Sarah Y. Mason   Victor Heerman   Joe Bigelow (uncredited)  Olive Higgins Prouty   John Boles Anne Shirley Alfred Newman
| cinematography = Rudolph Maté
| editing        = Sherman Todd
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
}}
 novel of John Boles, Anne Shirley. Best Actress Best Actress in a Supporting Role.   

==Plot== John Boles) and catches him at an emotionally vulnerable time. Stephens father killed himself after losing his fortune. Penniless, Stephen disappeared from high society, intending to marry his fiancée Helen (Barbara ONeil) once he was financially able to support her. However, just as he reaches his goal, he reads in the newspaper the announcement of her wedding. So he marries Stella.
 Anne Shirley as a young woman) is born. To Stellas great surprise, she discovers she has a strong maternal instinct. Even when she is out dancing and partying, she cannot help but think about her child. As Laurel grows up, Stellas ambition and scheming to rise socially is redirected to her daughter.
   Alan Hale). Finally, when Stephen receives a promotion that requires him to move to New York, Stella tells him he can go without her or Laurel; they separate, but remain married. Laurel stays with her mother, but visits her father periodically.

Years later, Stephen runs into Helen, now a wealthy widow with three sons. They renew their acquaintance. Laurel is invited to stay at Helens mansion; she gets along very well with Helen and her sons. Stephen asks Stella for a divorce, but she turns him down.

Stella takes Laurel to a fancy resort, where Laurel and Richard Grosvenor III (Tim Holt) fall in love. However, when Stella makes her first appearance after recovering from an illness, she becomes the target of derision for her vulgarity, though she herself is unaware of it. Embarrassed for her mother, Laurel insists they leave at once without telling her why. On the train back, Stella overhears the truth.

Stella goes to talk with Helen. After learning that Helen and Stephen would marry if they could, she agrees to a divorce and asks that Laurel go live with them. Helen realizes the reason for the request and agrees.

When Laurel learns of the arrangement, she refuses to put up with it and returns home. However, Stella has been notified by a telegram and is ready for her. Stella pretends that she wants Laurel off her hands so she can marry Ed Munn and travel to South America. Laurel runs crying back to her father.

Later, Laurel and Richard get married. Stella watches them exchange their wedding vows from the city street through a window (whose curtains have been opened at Helens order), her presence unnoticed in the darkness and among the other curious bystanders. Then, she slips away in the rain.

==Cast==
* Barbara Stanwyck as Stella (Martin) Dallas John Boles as Stephen Dallas Anne Shirley as Laurel Dallas
* Barbara ONeil as Helen (Morrison) Dallas Alan Hale as Ed Munn
* Marjorie Main as Mrs. Martin, Stellas mother
* Tim Holt as Richard Grosvenor III
* George Wolcott as Charlie Martin
* Hattie McDaniel as Maid

==References==
 

==Further reading==
* Williams, Linda. "Something Else besides a Mother: Stella Dallas and the Maternal Melodrama," Cinema Journal Vol. 24, No. 1 (Autumn, 1984), pp.&nbsp;2–27  
* Stevenson, Diane. "Three Versions of Stella Dallas" for Jeffrey Crouse (editor), Film International, Issue 54, Volume, 9. Number 6 (2011), pp.&nbsp;30–40.

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 