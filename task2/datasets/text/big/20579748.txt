Naya Raasta
{{Infobox film
| name           = Naya Raasta
| image          = Naya Raasta.jpg
| image_size     = 
| caption        = 
| director       = Khalid Akhtar
| producer       = 
| writer         =
| narrator       = 
| starring       =Jeetendra Asha Parekh   Farida Jalal
| music          = Datta Naik|N. Dutta
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood drama film directed by Khalid Akhtar. The film stars Jeetendra, Asha Parekh and Farida Jalal.

==Cast==
*Jeetendra ...  Chander 
*Asha Parekh ...  Shallo 
*Balraj Sahni ...  Bansi 
*Farida Jalal ...  Radha Pratap Singh 
*Sujit Kumar ...  Ramu  Veena ...  Rukmini 
*Lalita Pawar ...  Tulsi 
*Kishan Mehta ...  Suraj Pratap Singh

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ponchh Kar Ashq Apni Ankhon Se"
| Mohammed Rafi
|-
| 2
| "Ishwar Allah Tere Naam"
| Mohammed Rafi
|-
| 3
| "Jaan Gayee Main To Jaan Gayee"
| Asha Bhosle
|-
| 4
| "Chunar Mori Kori"
| Mohammed Rafi, Asha Bhosle
|-
| 5
| "Maine Pee Sharab"
| Mohammed Rafi
|-
| 6
| "More Saiyan Padoon Paiyan"
| Asha Bhosle
|-
| 7
| "Zulfon Ke Mahkte Saye Hai"
| Asha Bhosle
|}
==External links==
*  

 
 

 
 
 
 