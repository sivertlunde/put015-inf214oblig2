When the Night
 
 
{{Infobox film
| name           = When the Night  (Original  ) 
| image          = When the Night.jpg
| caption        = Film poster
| director       = Cristina Comencini
| producer       = {{Plainlist|
* Marco Chimenz
* Giovanni Stabilini
* Riccardo Tozzi
* Exec. producers:
* Fabio Massimo Cacciatori
* Franco Bevione
* Matteo De Laurentiis
* Gina Gardini
}} 
| screenplay     = {{Plainlist|
* Cristina Comencini
* Doriana Leondeff
}}
| based on       =  
| starring       = {{Plainlist|
* Filippo Timi
* Claudia Pandolfi
* Michela Cescon
}}
| music          = Andrea Farri
| cinematography = Italo Petriccione
| editing        = Francesca Calvelli
| studio         = {{Plainlist|
* Cattleya
* Rai Cinema
}}
| distributor    = {{Plainlist|
* 01 Distribution (Italy)
* Celluloid Dreams (worldwide)
}}
| released       =  
| runtime        = 116 minutes
| country        = Italy
| language       = Italian
| budget         = $10,000,000
}}

When the Night (Original  ) is a 2011 Italian drama film directed by Cristina Comencini, and based upon Comencinis novel of the same title.        

==Plot==
Marina (Claudia Pandolfi) is a young woman attempting to be a good mother to her two-year-old son. She decides to spend the summer in the mountains, in the hope that the change will improve her childs sleeping habits.  She rents an apartment from Manfred (Filippo Timi), a man who is distrustful of women.  The two generally avoid each other until an unfortunate event of the boy having a fall forces them together. Fifteen years later, Marina returns to the mountain to find Manfred to rekindle the desire she had felt, but upon which she had not acted.

==Cast==
* Filippo Timi as Manfred
* Claudia Pandolfi as Marina
* Michela Cescon as Bianca
* Thomas Trabacchi as Albert
* Franco Trevisi as Gustav
* Denis Fasolo as Stefan
* Manuela Mandracchia as Luna

==Production==
The film project was announced at the Cannes Film Festival in May 2010,  casting was performed in June 2010,  and pre-production began in July 2010.   Filming began on 23 August 2010, at locations in Macugnaga, a mountain village in the very northern part of the Piedmont region in Italy.     Certain scenes shot on the slopes of Monte Rosa, were filmed at high altitude and during -30 degree snowstorms.   The project completed filming and entered post production in May 2011. 

===Release=== Rio de Janeiro International Film Festival,   the Haifa International Film Festival,   the Marrakech International Film Festival,  and the London Film Festival,  before its theatrical release 28 October 2011 in Italy. 

Due to its content, there had been a restriction placed on the film on 26 October, banning it from being seen by children under 14 . After an appeal by Cattleya, the restriction was lifted on the 27th.  
 Bari International Festival Cinematográfico Intenacional del Uruguay,    and is slated to screen in May 2012, at the Seattle International Film Festival. 

==Recognition==

===Awards and nominations===
The received a Golden Lion nomination at the 2011 Venice International Film Festival, and a Golden Anchor Award nomination at the 2011 Haifa International Film Festival.     At the Bari International Film Festival in March 2012, Claudia Pandolfi won the Anna Magnani Award for Best Actress for her role as Marina. 

===Critical response===
Monsters & Critics reported that the drama film produced unintended laughter from its audience at the Venice Film Festival when some of the verbal interaction between the characters of Marina and Manfred were misinterpreted by viewers.   In defending her film after its screening, writer/director Comencini stated, "Alcuni momenti della storia sono emozionanti, e non sempre ai festival lemozione è accettata. Ci vuole coraggio, ad avere emozione" (Emotions are not always accepted at festivals. In this film there are two or three very emotional moments and courage is needed to feel emotions). She offered that while it might be acceptable to insult a film when it was over, doing so during a screening is a form of violence that negatively affects the perceptions of other viewers.     Internet Group|Último Segundo offered that the it was surprising that a 55-year-old filmmaker could write such a childish screenplay.    Variety (magazine)|Variety panned the film for its lack of proper character development by writing of the two leads "The script doesnt bother to give her   a career or background, other family or friends. Shes not in her 20s, but it seems shes never known anyone to have a baby before. If Marina is a cipher, Manfred is given too much of a backstory, allowing Comencini to indulge in the most facile pop psychology profiling."   They also make note of Comencini treating the situations "as wisdom from the skies"  when the commonplace events surrounding a child sometimes injuring himself were, for most viewers, "hardly a revelation".   The also noted problems in its "laughable dialogue" being too overly ernest.     Mymovies.it wrote that the novel was betrayed by the film in that Comencini faled to create a proper screenplay from her book, itself judged to be "pieno di una tensione che non si placa mai, come in certi amori che fanno male" (full of tension that never subsides, as in certain loves that hurt).   
 Passionate Friends Falling In Love but is distinct from both." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 