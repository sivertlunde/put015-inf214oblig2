Jawai Maaza Bhala
 

{{Infobox film
| name           = Jawai Maaza Bhala
| image          =  
| released       =  
| country        = India
| language       = Marathi
}}

Jawai Maaza Bhala ( : My Good Son-in-law) is a Marathi film that was released on March 16, 2008.  The film was written by Ratnakar Matkari, directed by Adwait Dadarkar and presented by KalaShri Boston. The film stars Vikram Gokhale, Amita Khopkar, Jayant Savarkar, Sujata Joshi and Rajashri Tope. 

The film is based on a what a father goes through just before his daughter is about to get married.

==Synopsis==
A simple man raises his children with utmost sincerity. Most of his life he works as a senior accountant and performs his homely duties as required.

On his 50th birthday, his daughter announces that she wants to get married to the son of a rich business man. On hearing this news, her father rejects their union, solely because he doesnt believe some other man could take better care of his daughter. Thus begins the efforts of family to convince father for the wedding.

This film is a drama with light humor.

==Cast==
* Vikram Gokhale
* Amita Khopkar
* Jayant Savarkar
* Sujata Joshi
* Rajashri Tope

==Crew==
*Writer - Ratnakar Matkari
*Director - Adwait Dadarkar
*Producer - KalaShri Boston

==References==
 

==External links==
*  
*  

 
 
 
 


 