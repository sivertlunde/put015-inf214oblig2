Ladies Only (1939 film)
{{Infobox film
| name           = Ladies Only   
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sagar Movietone
| writer         = 
| narrator       =  Surendra Bibbo Bibbo Prabha
| music          = Anupam Ghatak
| cinematography = Faredoon Irani
| editing        = 
| distributor    =
| studio         = Sagar Movietone
| released       = 1939
| runtime        = 158 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1939 Hindi Bibbo and Prabha.  This was the last comedy film made by Sarvottam Badami before he left Sagar Movietone. He joined his mentor Ambalal Patel, at Sudama Pictures to start making "socially relevant" films.    

The story involved three girls from different region (States) meeting at a railway station, and their relationship with each other thereafter.   

==Plot==
Three girls from different regions of India, Sabita Devi (Gujarat), Bibbo (Punjab, India|Punjab) and Prabha (Bengal) meet at a railway station. With no place to stay they decide to find accommodation together. A cook (Bhudo Advani) joins them speaking the language from each state. A young crook (Surendra) enters their life and trouble starts brewing between the girls when they all fall in love with him.

==Cast==
* Sabita Devi
* Surendra
* Bibbo
* Prabha
* Bhudo Advani
* Harish
* Pande
* Sunalini
* Kaushalya

==Music==
Music was composed by Anupam Ghatak with lyrics by Zia Sarhadi and Pandit Indra Chandra. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| Aaye Piharva Aaye Aaye
|-
| 2
| Ashram ashram Hum Banaye
|-
| 3
| Banayi Batiyan Mose Kahe Ko Jhoothi
|-
| 4
| Chamke Poonam Ko Poonam Ka Chand
|-
| 5
| Jaldi Pee Ko Paana Mushkil
|-
| 6
| Meri Sejon Ko Mehman Hai Nadan
|-
| 7
| Paniya Bharan Panghat Pe Gayi Main
|-
| 8
| Sagar Ke Marg Ghar Baithe Ganga Aayi
|-
| 9
| Soye Bin Dekhoon Sapna Yeh Haal Raat Din Apna
|-
| 10
| Tere Tirchhe Nishane Yahan Seedhe Lage
|}


==References==
 

==External links==
* 

 

 
 
 
 