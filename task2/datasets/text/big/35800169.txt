The Bonnie Parker Story
{{Infobox film
| name           = The Bonnie Parker Story
| image          = BonnieParkerStory-poster.jpg
| image_size     = 
| caption        = Film poster by Reynold Brown
| alt            = The top third of the poster contains large lettering, reading "The Bonnie Parker Story" and "Cigar Smoking Hellcat of the Roaring Thirties". Immediately below is an image of the head and arms of a woman firing a submachine gun. The woman appears to be in her 20s; she is blonde. The woman is smoking a cigar. She is firing the gun through a jagged hole in a glass window. Below the image is more text: "Starring - Dorothy Provine - Jack Hogan - Richard Bakalayan". In a smaller font near the bottom is the text: "Written and produced by Stan Shpetner - Directed by William Witney - A James H. Nicholson and Samuel Z. Arkoff Production".
| director       = William Witney
| writer         = Stanley Shpetner
| narrator       = 
| starring       = Dorothy Provine Jack Hogan Richard Bakalyan
| music          = Ronald Stein
| cinematography = Jack A. Marta
| editing        = Frank P. Keller
| studio         = American International Pictures Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 31 
| distributor    = 
| released       = April 28, 1958
| runtime        = 79 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Bonnie Parker, Machine Gun Kelly.

==Plot==
Diner waitress Bonnie Parker is just as tired of her job in 1932 Texas as she is of customers like Guy Darrow, who try too hard to make her acquaintance. When she goes too far, fending off Guy with hot oil, Bonnie is fired.
 Texas Rangers being assigned to the case.

Guys incarcerated cousin Chuck is paroled in late 1933, so Bonnie and the Darrows travel north to Missouri and Iowa for more robberies. Bonnie thinks its time to stop thinking small and aim for banks instead of gas stations and such. She also decides the gang should bust Duke out of the pen.

Their daring breakout succeeds, but Chuck is shot. Now that her husbands in charge, the trio begins making some big scores and become Americas most wanted criminals. But when a big scheme by Bonnie to rob an armored truck backfires, the guards locking themselves inside a vehicle thats bulletproof, things continue to go wrong when Guy accidentally kills Duke.

On the lam, Bonnie decides its time to hide out in Louisiana, but its only a matter of time before Steel and the Rangers find them. Bonnie and Guy go down in a hail of bullets.

==Cast==
*Dorothy Provine as Bonnie Parker
*Jack Hogan as Guy Darrow
*Richard Bakalyan as Duke Jefferson
*Joe Turkel as Chuck Darrow
*William Stevens as Paul Baxter Douglas Kennedy as Tom Steel

==Critical response== Bonnie and Clyde, Pauline Kael dismissed The Bonnie Parker Story as "a cheap - in every sense - 1958 exploitation film."    Recent critics have been more enthusiastic. Bob Mastrangelo wrote, "The Bonnie Parker Story is an obscure oddity that exists in the shadow of the far better known Bonnie and Clyde, but this little film is also able to stand on its own legs."  Quentin Tarantino considers the films director, William Witney, to be a "lost master"; hes quoted as saying of this film that, "I was blown away. It was like, whoa, who made this? I have to see everything he ever did."  Elaine Lemmon wrote in Senses of Cinema that, "Other than the predictable final shoot-out, The Bonnie Parker Story bears no other resemblances to the later film, especially in terms of visual style, where it remains strictly in the B-movie tradition of American International Pictures, its production company. However, it is told and shot with verve, and is pleasingly lurid, with an appropriately vivacious characterisation by Dorothy Provine." 

==See also== Bonnie and Clyde (1967 film directed by Arthur Penn).

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 