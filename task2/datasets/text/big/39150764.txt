Venus in Fur (film)
 
{{Infobox film
| name           = Venus in Fur
| image          = Venus in Fur poster.jpg
| caption        = Theatrical release poster
| director       = Roman Polanski
| producer       =   Alain Sarde
| writer         = David Ives Roman Polanski
| based on       =  
| starring       = Emmanuelle Seigner Mathieu Amalric
| music          = Alexandre Desplat
| cinematography = Paweł Edelman
| editing        = Hervé de Luze Margot Meynier
| distributor    = Lionsgate (International) 
| released       =  
| runtime        = 96 minutes
| country        = France Poland
| language       = French
| budget         = 
| gross          = $5.9 million (Worldwide)
}} play of the same name by American playwright David Ives, which itself was inspired by Leopold von Sacher-Masochs novel Venus in Furs. 
 Best Director.   

==Plot==
Thomas is a writer-director of a new play, an adaptation of the 1870 novel Venus in Furs by Austrian author Leopold von Sacher-Masoch. Alone in a Parisian theatre after a day of auditioning actresses for the lead character, Wanda von Dunayev, Thomas laments on the phone of the poor performances to come through. As he is preparing to leave the theatre, an actress named Vanda arrives disheveled. In a whirlwind of energy and unrestrained aggression, Vanda convinces the director to let her read for the part. To Thomass amazement, Vanda shows great understanding of the character and knows every line by heart. As the audition progresses, the intensity is redoubled and the attraction of Thomas turns into obsession.

==Cast==
* Emmanuelle Seigner as Vanda Jourdain
* Mathieu Amalric as Thomas Novacheck

==Production==
In September 2012, it was announced that Roman Polanski would direct an adaptation of David Ives 2010 play entitled Venus in Fur. At that time, Emmanuelle Seigner and Louis Garrel were attached in the lead roles.  While filming was scheduled to begin in November 2012, production was delayed until January 2013 and Garrel was replaced by Mathieu Amalric. 

==Release==
Sundance Selects/IFC Films acquired the U.S. rights to the film following its premiere at Cannes. 

==Critical reception==
A. O. Scott of The New York Times said, "Working from a French translation of the play (which was widely acclaimed when it ran on and off Broadway a few years ago), Mr. Polanski has marked the text with his own fingerprints. One of the two characters — the splendidly volatile Vanda, an actress — is played by Emmanuelle Seigner, his wife. Her foil — a writer and theater director named Thomas — is played by Mathieu Amalric in a performance that is very close to a Polanski impersonation."   

J.C. Maçek III of PopMatters wrote, "Venus in Fur is fascinating to the point that (subtitles or not), you simply cannot look away from the screen. There is always something to see, hear and feel."   

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 