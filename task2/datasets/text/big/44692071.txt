Azhage Unnai Aarathikkiren
{{Infobox film
| name           = Azhage Unnai Aarathikkiren
| image          = AzhageUnnaiAarathikkiren.png
| image_size     =
| caption        = LP Vinyl Records Cover
| director       = C. V. Sridhar
| producer       = B. Bharani Reddy
| writer         = C. V. Sridhar (dialogues)
| screenplay     = C. V. Sridhar
| story          = C. V. Sridhar Vijayakumar Latha Latha Jaiganesh Nagesh
| music          = Ilayaraja
| cinematography = R. K. Thiwari
| editing        = Balu Shyam
| studio         = Sreebarani Chithra Enterprises
| distributor    = Sreebarani Chithra Enterprises
| released       =  
| runtime        = 135 minutes
| country        = India Tamil
}}
 1979 Cinema Indian Tamil Tamil film, directed by C. V. Sridhar and produced by B. Bharani Reddy. The film stars Vijayakumar (actor)|Vijayakumar, Latha (actress)|Latha, Jaiganesh and Nagesh in lead roles. The film had musical score by Ilayaraja.  

==Cast==
  Vijayakumar
*Latha Latha
*Jaiganesh
*Nagesh
*V. S. Raghavan
*V. Gopalakrishnan
*Gopal
*Jayasri
*Sudha
*Prakash (debut)
*Subashini (debut)
 

==Soundtrack==
The music was composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali || 02.07
|- Vaali || 04.23
|- Vaali || 04.26
|- Vaali || 03.57
|- Vaali || 04.25
|- Vaali || 05.02
|- Vaali || 04.21
|}

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 


 