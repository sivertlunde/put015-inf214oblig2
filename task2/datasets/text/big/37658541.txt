Ghazi Shaheed
{{Infobox film
| name           =Ghazi Shaheed 
| image          =PNS Ghazi 134 DN-SC-92-03633.JPEG 
| image_size     =250px 
| border         = 
| alt            =  PNS Ghazi Tench class submarine.
| director       =Kâzım Pasha     
| producer       =Inter-Services Public Relations (ISPR) 
| writer         =Asad Mohammad Khan   
| screenplay     = 
| story          =Based on PNS Ghazi (submarine of Pak Bahria)
| based on       =   Zia Mohideen  
| starring       =Shabbir Jan Mishi Khan Ayesha Khan Humayun Saeed
| music          =PTV Music  
| cinematography =Tanweer Malik   
| editing        =Ghulam Qadir Khan   
| studio         =PTV Studios 
| distributor    =Techno Time Production  
| released       =  
| runtime        =120 minutes 
| country        =Pakistan
| language       =Urdu 
| budget         = 
| gross          = 
}}
 thriller drama disasters that PNS Ghazi Zafar Khan ISPR and the Navy and was filmed mostly in Arabian sea. 

==Synopsis==

 

==Cast== Commander Zafar Zafar Khan, Commanding Officer of the Ghazi 
* Mishi Khan as Lala Rukh Zafar, wife of Cdr Zafar Khan
* Mobina Dossel as Sitara Shamshad, mother of Cdr Zafar Khan
* Ayesha Khan as Ayesha Zafar, daughter Cdr Zafar Khan 
* Adnan Jilani as Lieutenant-commander|Lieutenant-Commander Pervez Hameed
* Sadia Jilani as Begum Kalsoom Pervez, wife of Lt. Cdr Pervez Hameed
* Nassarullah as Lieutenant-commander|Lieutenant-Commander Shamshad Ahmad Lieutenant Muhammad Bashir Rajput Lieutenant Nazir Ahmed Awan
* Rizwan Wasti as Commodore KM Hussain  Torpedo Officer (TPO)
* Fehmid Ahmad Khan as Admiral Sardarilal Mathradas Nanda, Indian Navy
* Mohammad Ayub as Rear Admiral R. Krishna, main antagonist
* Imtiaz Taj as Commodore G M Hiranandan, secondary antagonist
* Mohammad Imdad Khan as Captain Inder Singh, secondary antagonist

==Production development==

 

==Reception==

 

==Cultural legacy==

 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 