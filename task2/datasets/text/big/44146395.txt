Justice Raja
{{Infobox film
| name           = Justice Raja
| image          =
| caption        =
| director       = R Krishnamoorthy
| producer       =
| writer         = Pappanamkodu Lakshmanan
| screenplay     = Pappanamkodu Lakshmanan Menaka Balan K Nair K. R. Vijaya
| music          = Gangai Amaran
| cinematography = Prasad
| editing        = Chakrapani
| studio         = Sujatha Creations
| distributor    = Sujatha Creations
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by R Krishnamoorthy. The film stars Prem Nazir, Menaka (actress)|Menaka, Balan K Nair and K. R. Vijaya in lead roles. The film had musical score by Gangai Amaran.   

==Cast==
*Prem Nazir Menaka
*Balan K Nair
*K. R. Vijaya Sujatha

==Soundtrack==
The music was composed by Gangai Amaran and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Janmam thorum || K. J. Yesudas, S Janaki || Poovachal Khader || 
|-
| 2 || Kanni malare || K. J. Yesudas, P Susheela, SP Shailaja || Poovachal Khader || 
|-
| 3 || Mungaakkadal muthum || K. J. Yesudas, S Janaki || Poovachal Khader || 
|-
| 4 || Police namukku || P Jayachandran, Kalyani Menon || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 