Pelli Kanuka
{{Infobox film
| name           = Pelli Kanuka
| image          =
| caption        =
| writer         = Posani Krishna Murali  
| story          = N. V. S. Unit
| screenplay     = Kodi Ramakrishna
| producer       = Nannapaneni Anna Rao
| director       = Kodi Ramakrishna
| starring       = Jagapati Babu   Lakshmi  Bhanumathi Ramakrishna
| music          = M.M. Keeravani
| cinematography = Kodi Lakshmna Rao
| editing        = Tata Suresh
| studio         = N. V. S. Creations
| released       =  
| runtime        = 142 minutes
| country        = India
| language       = Telugu
| budget         =
}}   Pardes starring Shah Rukh Khan and Mahima Chaudhry. 


==Cast==
* Jagapati Babu as Sagar/Airport
* Lakshmi as Ganga
* Bhanumathi Ramakrishna as Savitri
* Viren Chaudhury as Shrinivasa Prasad/Chintu
* Betha Sudhakar as Bholak Singh Mallikarjuna Rao as Sita Rama Raju
* Pokula Narasimha Rao
* John as Ramakrishna
* Chalasani Krishna Rao
* Rajita as Indira
* Radha Prasanthi as Prasads mother
* Indu Anand

==Soundtrack==
{{Infobox album
| Name        = Pelli Kanuka
| Tagline     = 
| Type        = film
| Artist      = M.M. Keeravani
| Cover       = 
| Released    = 1998
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:10
| Label       = T Series
| Producer    = M.M. Keeravani
| Reviews     =
| Last album  = Paradeshi   (1998) 
| This album  = Pelli Kanuka  (1998)
| Next album  = Shri Sita Ramula Kalyanam Chootamu Raarandi   (1999)
}}
 Sirivennela Sitarama Sastry. All songs are chartbusters. The music was released on T Series Music Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:10
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =


| title1  = Suvvi Suvvi Suvvala SP Balu,Malgudi Subha 
| length1 = 4:26

| title2  = Rammane Kanti Reppala Chitra
| length2 = 3:35

| title3  = Bangaru Bommaki
| extra3  = SP Balu,Bhanumathi Ramakrishna  
| length3 = 4:50

| title4  = Ee Gaali Ee Nela
| extra4  = M.M. Keeravani,Chitra
| length4 = 4:35

| title5  = Jeevinchu Prema
| extra5  = M.M.Keeravani,V.Srinivas,Chitra
| length5 = 4:00

| title6  = Manninchamma    
| extra6  = SP Balu
| length6 = 3:44
}}

==Awards==
Nandi Award for Best Female Comedian - Rajita

==External links==
 

 

 
 
 
 
 
 
 
 
 
 
 

 