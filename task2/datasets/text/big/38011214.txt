Una storia semplice (film)
{{Infobox film
 | name = Una storia semplice
 | image =  Una storia semplice (film).jpg
 | caption =
 | director = Emidio Greco
 | writer = 
 | starring =  Gian Maria Volonté
 | music =  Luis Enriquez Bacalov
 | cinematography = Tonino Delli Colli
 | editing =  
 | producer =
 | distributor =
 | released =
 | runtime =
 | awards =
 | country =
 | language =  
 | budget =
 }} 1991 Cinema Italian drama the novel with the same name written by Leonardo Sciascia.    It premiered at the 1991 Venice International Film Festival, in which it entered the main competition.   The film was awarded with a Nastro dArgento for best screenplay, two Globi doro for best film and best screenplay and a Grolla doro for best actor to the ensamble cast. 

== Cast ==
*Gian Maria Volonté: Il professor Franzò
*Massimo Dapporto: Il questore   
*Ennio Fantastichini: Il commissario
*Ricky Tognazzi: Il brigadiere Lepri   
*Massimo Ghini: Il rappresentante di medicinali con la Volvo   
*Paolo Graziosi: Il colonnello dei Carabinieri   
*Omero Antonutti: Padre Cricco   
*Gianmarco Tognazzi: Il figlio del Roccella   
*Gianluca Favilla: Il procuratore   
*Tony Sperandeo: Il primo agente  
*Giovanni Alamia: Il secondo agente 
*Macha Méril: La vedova del Roccella 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 