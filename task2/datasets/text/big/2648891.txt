Jane Eyre (1970 film)
{{Infobox film
| name           = Jane Eyre
| image          = Janeeyre.1.jpg
| caption        = Original Movie Poster
| director       = Delbert Mann
| producer       = Omnibus Productions
| based on       =  
| screenplay     = Jack Pulman
| starring       = George C. Scott Susannah York John Williams
| cinematography = Paul Beeson
| editing        = Peter Boita
| distributor    = British Lion Film Corporation
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| Gross          = 
}}
Jane Eyre is a 1970 television film directed by Delbert Mann starring George C. Scott and Susannah York. It is based on the 1847 novel Jane Eyre by Charlotte Brontë. The film had its theatrical debut in the United Kingdom in 1970 and was released on television in the United States in 1971. A popular Mandarin Chinese dubbed version of the film was released in China both as a video film and as an audio-only cassette tape.

== Plot ==
 
Jane Eyre is an orphan, sent to the cruel school institution of Lowood. On leaving, she takes a position as governess to a girl named Adele at Thornfield Hall. Fully aware of her low rank and plain countenance, she makes the best of her situation. But Thornfield holds many secrets and despite mysterious occurrences that Jane cannot comprehend, she and Edward Rochester, owner of Thornfield and Adeles guardian, fall in love. Suddenly, when Jane is about to win the happiness she deserves, a dark secret comes to light which needs all her courage, love and maturity.

== Cast ==
*George C. Scott as Edward Rochester Jane Eyre
*Sara Gibson as Jane Eyre as a Child
*Ian Bannen as St. John Rivers
*Rachel Kempson as Mrs. Fairfax
*Nyree Dawn Porter as Blanche Ingram
*Jack Hawkins as Mr. Brocklehurst
*Jean Marsh as Mrs. Rochester (Bertha Mason)
*Kenneth Griffith as Mason
*Angharad Rees as Louise

== Release ==
In the 1980s,  the movie was dubbed into Mandarin and widely released in China.    The dubbed version became dominant form by which the classic was known to the Chinese, with the dubbed monologues of the film becoming more widely-recited than the original English.  The dubbed version was also release on audio cassette tape, and the cassette version was more popular than the dubbed film. 

== Awards == John Williams).

== Soundtrack == John Williams composed the score, recording it at Anvil Studios, Denham, outside London. 

{{Tracklist
| collapsed = yes
| headline = Jane Eyre: Limited Edition 
| title1 = Love Theme from Jane Eyre
| length1 = 3:15
| title2 = Overture (Main Title)
| length2 = 3:55
| title3 = Lowood
| length3 = 2:25
| title4 = To Thornfield
| length4 = 1:51
| title5 = Festivity at Thornfield
| length5 = 2:08
| title6 = Grace Poole and Masons Arrival
| length6 = 3:00
| title7 = Meeting
| length7 = 3:07
| title8 = Thwarted Wedding
| length8 = 2:37
| title9 = Across the Moors
| length9 = 2:37
| title10 = Restoration
| length10= 3:56
| title11 = Reunion (End Title)
| length11= 4:22
}}

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 