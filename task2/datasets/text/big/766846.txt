Come Back to the Five and Dime, Jimmy Dean, Jimmy Dean (film)
 
{{Infobox film
| name           = Come Back to the Five and Dime, Jimmy Dean, Jimmy Dean
| image          = Come_Back_to_the_5_&_Dime_JD,_JD-poster.jpg
| caption        = Original theatrical poster
| alt            = Three women are seen in different poses against an orange background. The one on the right smiles as she rests her hand on a jukebox; to her left, the lower one stoops before a table, next to a mask; and the one standing up is holding a glass, with a blunt look on her face. The films title is displayed in a cursive script inside a tilted pink square.
| screenplay     = Ed Graczyk
| based on       =  
| starring       = Sandy Dennis Cher Karen Black Sudie Bond Kathy Bates
| director       = Robert Altman
| producer       = Scott Bushnell
| cinematography = Pierre Mignot
| editing        = Jason Rosenfield United States   }}
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = US$850,000    
| gross          = US$2,276,900    Viacom Enterprises Sandcastle 5 Cinecom International Films (1982, original) Olive Films (under license from Paramount Pictures) (2014, Blu-ray DVD)
| italic title   = no
}} of the Broadway and screen versions were directed by Robert Altman, and starred Sandy Dennis, Cher, Mark Patton, Karen Black, Sudie Bond and Kathy Bates.   

As with the original play, the film version takes place inside a small F. W. Woolworth Company|Woolworths Variety store#North America|five-and-dime store in a small Texas town, where an all-female fan club for actor James Dean reunites in 1975.    Through a series of flashback (narrative)|flashbacks, the six members also reveal secrets dating back to 1955.

Jimmy Dean was the first of several feature adaptations of plays by Altman in the 1980s, after the directors departure from Hollywood.  It was screened at various film festivals in North America and Europe, and won the top prize at the 1982 Chicago International Film Festival.  This was the first release for New York-based independent outlet Cinecom, which Altman chose over a major studio "to guarantee a long play" in art house venues.

==Plot==
On September 30, 1975, an all-female fan club called the Disciples of James Dean meets inside a Woolworths five-and-dime store in McCarthy, Texas,   to honor the twentieth anniversary of the actors death.  The store is 62&nbsp;miles away from Marfa, Texas|Marfa, where Dean filmed Giant (1956 film)|Giant in 1955.  Inside, store owner Juanita prepares for another day on the job while swatting flies and listening to Gospel music on the radio, and also calls for Jimmy Dean by name.  Meanwhile, one of the Disciples, Sissy, comes in late after helping out at the truck stop; Juanita remarks that more members could arrive at any moment.  Another one, Mona, is taking a late bus.

After Sissy worries about the weather ("118 degrees in the shade"), a flashback to a stormy night in 1955 occurs.  Coming inside for shelter, Sissy asks about three friends of hers: Mona, Sydney and Joseph Qualley (or "Joe" for short).    Joe is busy stocking up some new issues of Photoplay magazine; Mona arrives late due to the weather.  To Juanitas chagrin, Sissy, Mona and Joe go up at the counter and begin singing the doo-wop tune "Sincerely (song)|Sincerely", which Juanita opposes, preferring to listen only to Gospel music.

Back in 1975, the sun is still shining; another two Disciples, Stella Mae and Edna Louise, make their way to the five-and-dime, bringing a red jacket that the club used to wear.  Mona joins them and explains that the bus she was riding on broke down and had to be repaired.  Looking at a group picture (with James Dean), she recalls the last time the Disciples, all dressed in jackets, came together.  As reunion preparations continue, so does the flashback.  (At this point, Sissys friends joyfully break the news that Elizabeth Taylor, Rock Hudson and James Dean will be visiting the McCarthy vicinity to film Giant; auditions will be carried out across that area.  This only prompts Monas desires to play alongside Dean, her idol, in that film.)  When Mona reveals that Dean chose her to raise his son, Juanita thinks that she is "warped and demented".  Mona loses her temper when Sissy badmouths her; the latter goes outside to "cool off".
 sex change operation   almost thirteen years ago, and (as Joe) was the only man in this group.  In a flashback, Joe was scorned by town-dwellers after attending a high school dance in drag,  and got brutally beaten in a graveyard.  Hearing that story, Stella wonders whether Joanne is a hermaphrodite—"half-man, half-woman". Joanne explains she had a sex-change operation 13 years before and is a woman, showing physical proof to several of the friends.

Later in the day, Juanita tells the Disciples to brace themselves for stormy weather, after thinking she hears thunder.  Instead, the loud noise comes from a sports car—Monas son Jimmy Dean has stolen Joannes car.  As Joanne phones the local Highway Patrol to have Jimmy Dean returned, another flashback takes root: they hear the radio announcer reveal that a car accident has killed actor James Dean.  They decide to hold a vigil.

Among the other secrets and revelations, Mona announces that she almost died from asthma. She claims she was an extra in Giant.    As the reunion winds down, it appears that Jo/Joanne was the father of Monas never-seen 20-year-old son, Jimmy Dean, who has stolen Joannes Porsche.  The Disciples make a pact to hold another one in the next twenty years. Mona refuses. She, Sissy and Joanne appear in front of the mirrors and sing "Sincerely" again.  The film ends with shots of the decaying, abandoned five-and-dime store, while the song fades and the wind blows.

==Cast==
* Sandy Dennis as Mona    
* Cher as Sissy
* Karen Black as Joanne
* Sudie Bond as Juanita
* Marta Heflin as Edna Louise
* Kathy Bates as Stella Mae
* Mark Patton as Joe Qualley
* Caroline Aaron as Martha Ruth Miller as Clarissa
* Gena Ramsel as Sue Ellen
* Ann Risley as Phyllis Marie
* Dianne Turley Travis as Alice Ann

==Production== Come Back Beyond Therapy, among others.   
 Viacom Enterprises, through game show company Mark Goodson Productions,   almost as much money as the play had cost him.   It was his intention to shoot Jimmy Dean for the cinema; "the initial press report that it was made for cable is not true," he said.  
 Super 16 equipment   during the nineteen-day shoot;    this was later converted to 35&nbsp;mm film|35&nbsp;mm stock for the first answer print.    For the films flashbacks, he built a double set with two-way mirrors that were controlled by computerized lighting techniques—which became problematic for both him and the films critics.  

==Release== Montreal and Toronto film Venice and Deauville American Film Festival|Deauville.    The film received its U.S. premiere on September 30, 1982 (the 28th anniversary of the late actors death) at the Chicago International Film Festival, where it received a ten-minute standing ovation.  After this screening, Altman discussed various aspects of the production during a question-and-answer session.  
 20th Century-Fox over his 1979 production Health (film)|HealtH.    Instead, he let Cinecom, an independent distributor in New York City, open it in arthouse theaters "to guarantee a long play";   it became the first release for that company.    The film opened on a limited basis in just two theaters on November 12, 1982, grossing US$22,298 and placing 18th at the North American box office that weekend.   By its fourth week, it made US$177,500 after going to four venues;  during its entire run, it grossed US$840,958. 
 Showtime cable Embassy Home Virgin Vision followed in June 1989.  It will be released on Blu-ray on November 18, 2014, by Olive Films, under license from Paramount Pictures. 

===Restoration / re-release===
A restored version of the film was released in 2011. The film was restored by the UCLA Film & Television Archive (in cooperation with Sandcastle 5 Productions) as “the first fruit of a new, larger project ... to preserve Mr. Altmans artistic legacy.”  The preservation was funded by the Hollywood Foreign Press Association and The Film Foundation.   

The new print was made “from the original Super-16mm color negative, a 35mm CRI, a 35mm print, and the original ½ inch analog discreet mono D-M-E track.” 

The restoration premiered at the UCLA Festival of Preservation on March 3, 2011   and was screened at other North American cities in 2011 including New York City,  Chicago,   and Vancouver. 

==Critical reception==
On its original release, The Boston Globe s Michael Blowen hailed Jimmy Dean as "  best film since Nashville (film)|Nashville".  He added, "  is having fun again. He seems more comfortable in a desolate Woolworths than he did on the frozen tundra of Quintet (film)|Quintet. In contrast with A Wedding, in which Altman cynically patronized his characters, he seems to love these three women. And why not?"   The Associated Press Bob Thomas said, "The film is a heartening example of how good writing  , gifted direction and solid acting can produce something worthwhile on a tiny (under $1 million) budget." 

The New York Times  Vincent Canby gave Jimmy Dean a mixed review. "There are some interesting things about  ," he observed, "but they have less to do with anything on the screen than with the manner in which the film was produced and with Mr. Altmans unflagging if misguided faith in the project."  He complained that "The actresses are not treated kindly, either by the material or by the camera," and noted that Sandy Dennis character, Mona, received most of the close-ups. "The only person in the film who comes off well," he said, "is Miss   Bond."  New York Magazine s David Denby wrote: "Altman uses cinema to celebrate theater, and his technique is so fluidly self-assured that he almost makes you forget the rubbishy situations and lines created by playwright Ed Graczyk. Almost, but not quite." 
 Movie Guide. "Strong performances, and Altmans lively approach to filming   Broadway play," he said, "cant completely hide the fact that this is second-rate material."     The staff of Halliwells Film Guide wrote, "  descends from cynicism through gloom to hysteria and is never very revealing."   

At the Chicago premiere of Jimmy Dean, Altman spoke of its festival acclaim to his audience: "I never have had a film of mine received as well as this film—I dont understand it, but I like it!"    In a January 1983 interview with the Boston Globe, he stated that "The critical reaction doesnt surprise me. Nothing surprises me any more. I take that back. One thing surprised me when I showed Jimmy Dean at film festivals—no one walked out."  
 Golden Globe Best Supporting Actress in a Motion Picture. 

==Themes and criticism==
Come Back to the Five and Dime, Jimmy Dean, Jimmy Dean has been noted to address the subject of feminism.         In Robert Altman: Hollywood Survivor, Daniel OBrien wrote that "  is in part an attempt to explore the way women are forced to suppress their emotions and personalities in order to be accepted by the male-dominated society around them".  

On the films sexuality issues, Robin Wood said: "What is especially interesting about Come Back to the Five and Dime is the connection it makes between the oppression of women and patriarchys dread of sexual deviation and gender ambiguity. Joe (the only male character to appear in the film, in flashback) is clearly (and sympathetically) presented as feminine (as opposed to the stereotypically effeminate), woman-identified, and gay; as Don Short has perceptively shown, the film implies that he has become a transsexual   because his society had no place for a gay male." 

In his 1985 book on Altman, Gerard Plecki wrote: "The reference to the James Dean myth is a clue to Altmans pervasive film message. Altman knows that James Dean had the kind of screen presence and magic that caused people to give in to cinema."  He added that "In each film   strives to reinforce and respond to that essential need—to give in to cinema."  Plecki observed that "It is fascinating that, after The James Dean Story, Altman would select another project touching upon the life of  ."  
 character development of the supporting roles,   while Plecki said that, compared to those in previous Altman films, none of the characters "are immediately likable".    "The reasons for the limited appeal of the characters are quite complex," Plecki wrote. "Most of the womens problems are physical or sexual ones."  

In Jimmy Dean, Altman frequently uses mirrors as a device for seamlessly connecting scenes between the present and the past.  Reflections in mirrors are part of many of the films frame compositions.      As noted Daniel OBrien in Robert Altman: Hollywood Survivor, they "  a window into 1955, enabling the characters to gaze into the past".  

==See also==
*List of American films of 1982

==Notes==
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 