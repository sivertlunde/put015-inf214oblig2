Judge Hardy and Son
{{Infobox film
| name           = Judge Hardy and Son
| image_size     =
| image	         = 
| caption        =
| director       = George B. Seitz
| producer       = Lou L. Ostrow Carey Wilson 
| based on = 
| starring       = Lewis Stone Mickey Rooney Cecilia Parker Fay Holden David Snell
| cinematography = Lester White
| editing        = Ben Lewis
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Judge Hardy and Son is the 8th film in the Andy Hardy series. 

==Plot summary==
 
Judge Hardy {Lewis Stone) has two problems. He must rescue an elderly couple from eviction, and he must cope with his wifes (Fay Holden) life threatening illness. 

==Cast==
 
* Lewis Stone as Judge James K. Hardy
* Mickey Rooney as Andy Hardy
* Cecilia Parker as Marian Hardy
* Fay Holden as Mrs. Emily Hardy
* Ann Rutherford as Polly Benedict
* Sara Haden as Aunt Mildred Millie Forrest
* June Preisser  as Euphrasia Phrasie Daisy Clark
* Maria Ouspenskaya as Mrs. Judith Volduzzi
* Henry Hull as Dr. Jones
* Martha ODriscoll as Leonora V. Elvie Horton
* Leona Maricle as Mrs. Maria Horton
* Margaret Early as Clarabelle V. Lee
* George Breakston as Beezy Anderson
* Egon Brecher as Mr. Anton Volduzzi
* Edna Holland as Nurse Trowbridge
* Marie Blake as Augusta McBride
 

==References==
 

==External links==
*   at IMDB
*   at Andy Hardy Films
*  
*  
*  

 
 

 
 
 
 
 
 
 
 

 