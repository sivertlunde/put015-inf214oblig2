Manhunt (2008 film)
{{Infobox film name           = Manhunt image          = Rovdyr.jpg caption        = International film poster director       = Patrik Syversen producer       =  writer         = Nini Bull Robsahm Patrik Syversen starring       = Kristina Leganger Aaserud Janne Beate Bønes Henriette Bruusgaard Jørn Bjørn Fuller Gee music          = Simon Boswell cinematography =  editing        = studio         = Fender Film distributor    = Euforia Film (Norway) released       = 2008 runtime        = 75 min. country        = Norway language       = Norwegian budget         = gross          =
}} Norwegian horror film directed by Patrik Syversen.

== Synopsis ==
The story is set in 1974. Four friends, Camilla, Roger, Mia and Jørgen go for a vacation in a forest. They stop at an inn and meet people and a girl who joins their group. Their newfound friend takes them on a journey to the deep end of the forest, where they become systematically hunted and killed for sport by a party of locals. The friends try to escape while avoiding a series of traps the trackers left on the place.

== Reception == dice throw of 3 out of 6 in both Verdens Gang and Dagbladet,   and 4 given in Nettavisen and Dagsavisen.   ABC Nyheter had a different grading system, giving it 5 out of 10. 

== Soundtrack == The Last House on the Left, which also starred Hess.  The end theme En Spennende Dag For Josefine is a Norwegian folkloric pop song sung by Inger Lise Rypdal. 

==References==
 

 
 
 
 
 
 
 

 
 