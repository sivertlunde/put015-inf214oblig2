The Tunnel (2009 film)
 

{{Infobox film
| name           = The Tunnel 
| image          = 
| caption        = 
| director       = Jenna Bass
| producer       = Fox Fire Films
| writer         = 
| starring       = Sibulele Mlumbi Finch Moyo Patricia Matongo Vuyisile Pandle Pakamisa Zwedala
| distributor    = 
| released       = 2009
| runtime        = 25
| country        = South Africa
| language       = 
| budget         = 
| gross          = 
| screenplay     = Jenna Bass
| cinematography = Jacques Koudstaal
| editing        = Jacques de Villiers
| music          = John Turest-Swartz
}} 2009 film.

==Synopsis==
Set in Matabeleland during the 80s, The Tunnel follows young Elizabeth, who loves inventing tall tales. When she arrives at a guerilla camp desperate for help, Elizabeth must tell her greatest story of all, a story about her village, strangers, ghosts, the day her father dug a tunnel to the city and her journey to find him. As the story unfolds, Elizabeth embarks on a quest for truth, weaving together fact and illusion. But reality is not far behind and, to save her village, Elizabeth will have to confront it.

==References==
 

 
 
 


 