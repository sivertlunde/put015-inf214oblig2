Khawnglung Run
 

{{Infobox film
| name           = Khawnglung Run
| image          = Khawnglung Run movie poster.jpg
| alt            = Khawnglung Run 2012 movie poster
| caption        = Poster of the film
| director       = Mapuia Chawngthu
| writer         = C. Lalengkima
| starring       = Alex Lalchhuankima   Zoremsangi Hnamte
| music          = Kima Chhangte   Francis   Sena   Pro Scores
| costumes       = Numami L.M.Botique
| editing        = Mapuia Chawngthu
| studio         = Leitlang Pictures
| released       =  
| runtime        = 122 minutes
| country        = India Mizo    (with English subtitles) 
| budget         =  11,00,000
}}
 Mizo history. 

==Cast==
*Alex Lalchhuankima as Chala
*Zoremsangi Hnamte as Thangi
*A.Zothanpuia
*David C.Lalrinawma
*Lalthakimi
*F. Zomuankima
*Lalnunmawia
*Ms-i
*R.Saptawna
*K.Rodingliana
*Joseph Lalnuntluanga
*Rohluzuala
*Lalropuii Pachuau
*CR.Laldingliani
*P.C. Lianmawia
*C.Laldinthara

==Production==
Filming began in 20 May 2010 and completed after three months. Major filming took place at Khawnglung Village set created on the top of Darkhuang Hill, Thenzawl, Tui Rihiau, Mat River, Nghasih Valley, Kawmzawl, Zohnuai High School compound and Pukthiang. Although filming was completed early, the processing took almost two years.  , ‘Khawnglung Run’ almost ready for public release 

==Soundtrack==
A music video on the original sound track Khawnglung Run was released on May 27, 2012. The song was performed by C.Lalruatkima, music by Kima Chhangte and composed by LT Muana Khiangte.   

==References==
 

== External links ==
*  

 
 
 

 