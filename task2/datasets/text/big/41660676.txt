Black Coal, Thin Ice
{{Infobox film
| name           = Black Coal, Thin Ice
| image          = Black Coal, Thin Ice Poster.jpg
| border         = 
| alt            = 
| caption        = 
| director       = Diao Yinan
| producer       = 
| writer         = 
| screenplay     = Diao Yinan
| story          = 
| based on       =  
| narrator       = 
| starring       = Liao Fan Gwei Lun-Mei Wang Xuebing
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 106 mins
| country        = China
| language       = Mandarin
| budget         = 
| gross          = US$16,460,000 
}}

Black Coal, Thin Ice ( ) is a 2014 Chinese thriller film written and directed by Diao Yinan.  The film won the Golden Bear award at the 64th Berlin International Film Festival. 

==Plot==
The story is set in northern China in 1999 and 2004.   A detective works to solve a series of murders where the victims dismembered parts are scattered across the province via coal shipments.  The common thread is a female clerk in a dry cleaners.

==Cast==
* Liao Fan as Zhang Zili
* Gwei Lun-Mei as Wu Zhizhen
* Wang Xuebing as Liang Zhijun
* Wang Jingchun as Rong Rong
* Yu Ailei as captain Wang
* Ni Jingyang as Su Lijuan

==Production==
The project started from Diao Yinans idea to film a detective story.   Diao spent eight years in total to write the screenplay;  the final version that was filmed was the third draft. The film was then laid out into a detective film noir. 

Liao Fan gained 44 pounds of weight to play the films protagonist of an alcoholic and suspended police officer. 

===Name===
The films English title Black Coal, Thin Ice is different from its Chinese title Bai Ri Yan Huo, which translates literally as Daylight Fireworks. Diao Yinan came across this phrase from a friend of his.  Diao further clarified the meaning of "daylight fireworks" as a state of sentiment or a state of condition. For him, the Chinese and English names together helped to construct the difference between reality and fantasy. In an interview he explained, "Coal and ice both belong to the realm of reality, but fireworks in daylight is something fantastic; they are the two sides of the same coin." The English name refers to the two visual clues in the film: coal as "where the body parts were found" and ice as "where the murder was committed".  He further explained, "when the two are combined, the reality of this murder is constructed ... while daytime fireworks is a fantasy, it is what we use to coat ourselves from the cruel side of this real world."

==Reception==
The film was shown in competition  at the 64th Berlin International Film Festival, {{cite web |url= http://www.berlinale.de/en/presse/pressemitteilungen/alle/Alle-Detail_21396.html|title= Competition
Jan 15, 2014: Berlinale 2014: Competition Complete |date= 15 January 2014|accessdate= 16 January 2014|work= berlinale.de}}  and went on to win the Golden Bear prize for Diao.      The films leading actor Liao Fan also won the Silver Bear for Best Actor.   The film received critical praises at the Berlin Film Festival, but audience reaction was more divided.      

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 