Purification (film)
 
 

{{Infobox film
| name           = Purification
| image          = Purification film poster.jpg
| alt            = 
| caption        = Purification film poster
| director       = Joseph Ciminera
| producer       = Joseph Ciminera
| writer         = Joseph Ciminera
| starring       = 
| music          = 
| cinematography =  
| studio         = Worldwide Distributors
| distributor    = Vanguard Cinema
| released       =  
| runtime        = 83 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}

Purification is an independent supernatural  thriller film written, directed, and produced by Joseph Ciminera. The film had its world premiere on 13 January, 2012 and it was given a limited theatrical release on 6 April 2012.  Purification stars Ciminera as Bret, a ruthless New York real-estate developer who begins to experience terrifying visions of souls trapped in limbo for the sins they committed in life.

==Plot==
Film introduces the viewers to Bret Fitzpatrick (Joe Ciminera) a New York real-estate developer. It is revealed that Bret values his business and money over the well-being of his family and friends. Bret has had a difficult childhood and all the success he has had in his life makes him want more.
 
Soon Bret has to travel back to his home town to evict Mrs. Keller (Natalie Swan) his tenant. Mrs. Keller has been late paying up rent. Bret arrives at Mrs. Kellers rented house and gives her 12 days time to vacate the house. Mrs. Keller agrees to vacate the premises but requests for longer time but Bret doesnt agree. As he leaves the premises, he is distracted by a call and walks into the path of a car and nearly get hit by the car.

After this Bret starts seeing dead people. He sees his dead sister and goes to a priest to consult him. Rest of the movie is about Bret coming to terms with his eminent death.

==Cast==
 
* Joseph Ciminera as Bret Fitzpatrick
* Natalie Swan as Mrs. Keller
* Daniella Ventura as Chloe Fitzpatrick
* Francisco Huergo as Scott Fitzpatrick
* Bob Socci as Stanley Jeffers
* James Terriaca as Dr. Michaels
* Melissa Bale as Marissa
* Tiffany Browne-Tavarez as Rape Victim
* Luca Kahn as Strangler
* Dillon Wilson as Mental Patient #1
* Theo van Golen as Brets Dead Son
* Michael Edwards Jr. as Kid With Ball
* Anthony Rosas Jr. as Parking Attendant
* Rachel Wellner as Real Estate Agent
* Julia Gregorio as Lead Paint Girl
* John Basedow as Man With Gun
* Caitlin A. Brockell as Knife Girl
* Michelle Palmieri as Adult Baby
* Nicolas Palmieri as Crawling Demon
* Alaa Salem as Bomber
* Anthony Aroya as Carnival MC
 

==Production==
Filming took place in Long Island, New York    from August 2011 and post-production wrapped in December 2011.    Actors were not given their script and dialogue until the first day of the shoot,  at which time they were assigned their roles.   

==Reception==
Aint It Cool News gave a favorable review for Purification, writing "Decently performed and directed, yet I saw the twist from a mile away, PURIFICATION ain’t all bad."    Bryan Cain-Jackson of Technorati wrote in praise on the film, "once in a blue moon, something truly unique comes along that will add a much welcomed shakeup to the current zeitgeist", calling Purification "a profoundly thought provoking film with the best examples of a Kubrickian modern horror piece and a Hitchcockian psychological thriller".   

==References==
 

==External links==
* 
* 
* 
* 

 
 
 