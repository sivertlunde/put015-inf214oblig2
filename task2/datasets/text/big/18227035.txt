Dhoondte Reh Jaaoge
 
{{Infobox film
| name           = Dhoondte Reh Jaoge
| image          = Dhoondte Reh Jaoge Poster.JPG
| caption        = The movie poster
| director       = Umesh Shukla
| producer       = Ronnie Screwvala
| writer         =
| starring       = Kunal Khemu Soha Ali Khan Sonu Sood Paresh Rawal
| music          = Sajid-Wajid
| cinematography = 
| editing        = 
| distributor    = UTV Motion Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| gross          =  
}} UTV Motion Pictures, starring Sonu Sood, Soha Ali Khan, Kunal Khemu, and Paresh Rawal, released 6 March 2009 in India,  and 20 March 2009, in the United States. The film is directed by Umesh Shukla and produced by Ronnie Screwvala.  The music was composed by duo Sajid-Wajid. {{cite web|title=Salman Khan returns with Sajid-Wajid The Producers. The film was declared a hit by boxoffice India.

Another movie with the same name was made in 1998 that was directed by Kumar Bhatia.  

==Plot==

Set in Mumbai, the film starts with Raj (Paresh Rawal), a good-for-nothing movie director who has released certain films, but none of them ever do well at the box office. He gets threatened by his landlords that if he does not pay them on time, he will be homeless. While chartered accountant Anand (Kunal Khemu) tries to cheer up his girlfriend Neha (Soha Ali Khan) on her birthday. Neha is another useless wannabe actress who Anand takes to a 5-star restaurant for her birthday. Raj meets Ratan (Asrani) at the restaurant. Ratan is the manager of famous Bollywood actor Aryan Kapoor (Sonu Sood), and Raj has come to see Ratan to sign a movie deal with Aryan.

Raj and Ratan buy as much food as they can, and put the bill on Anand, who can not pay for it all that is why he is sent to jail. When he comes out, Anand is eager for revenge, and sees Raj shoplifting. Raj goes over to Aryan and Ratan, and right when he gets the movie contract papers out, Inspector Avtar Gill shows up and arrests Raj for shoplifting, which was recorded by Anand. Raj escapes from jail only to get his movie deal signed, and when Raj is attacked, he is saved by Anand. Anand and Raj come up with a movie plan together, to put money on the movie, ONLY if it is a flop, but if it is a hit, then thats their loss. They collect a lot of money and pretend to be rich and wealthy men to show off in front of Aryan, who signs the papers, and gets in the movie. Anand and Raj have to try and make the movie as bad as they can to get their share on money, but they dont know that each of them has a different plan.

They borrow money from Assassins, who make a deal, if the movie flops then the Assassins will be pleased, but if it is a hit they will get murdered. They use Neha for the actress role, because Anand knows that with Neha in the film if would definitely be a flop. While the movie is filming, they tell Asharraff ( , and Lagaan (in that order). At the premier, the film is declared as a "hit" and the assassins get ready to kill them both. When Raj is about to escape with the money, he finds that Anand has already looted all the money. Both on the run, meet each other while looking for a hiding place, and both decide there is no better place to hide than Jail itself. The two run in jail for making a fraud film and looting money. Once they come out of jail, the two embrace friendship, and Neha finally marries to Anand, the two get married and Raj and Anand get thinking about creating a real movie.

==Cast==

{| class="wikitable"
|- 
! Actor/Actress 
! Role 
|- Paresh Rawal|| Raj Chopra
|-  Kunal Khemu|| Anand Pawar
|- Soha Ali Khan|| Neha
|- Sonu Sood|| Aryan Kapoor
|- Dilip Joshi|| Mama Nautanki
|- Deepal Shaw|| Lulu - Secretary 
|- Hrishita Bhatt|| Riya
|- Johnny Lever|| Parvez Asharraff
|-
|Asrani|| Ratanji
|-  Razak Khan|| Usman Khujli
|}

==Soundtrack==

1. Dhoondte Reh Jaaoge - Sonu Nigam

2. Pal Yeh Aane Wala Pal - Neeraj Shridhar

3. Salma-O-Salma - Palash Sen

4. I Am Falling In Love - Wajid

5. Nako-Re-Nako - Wajid, Javed Ali

6. Apne Ko Paisa Chahiye - Wajid, Sowmya Raoh

7. Instrumental - Sajid

==References==
 

== External links ==
*  

 
 
 