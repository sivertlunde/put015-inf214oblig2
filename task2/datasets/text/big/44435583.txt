Sadarame
{{Infobox film
| name           = Sadarame
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Raja Chandrashekar
| producer       = Gubbi Veeranna
| writer         = 
| screenplay     = Devudu Narasimha Shastry
| story          = 
| based on       =  
| narrator       = 
| starring       = Gubbi Veeranna   K. Ashwathamma
| music          = Venkataramaiah
| cinematography = 
| editing        = 
| studio         = Shakuntala Films
| distributor    = 
| released       =  
| runtime        = 142 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 1935 Indian mythological drama Tamil version released as Naveena Sadarame produced by K. Subramanyam and starred S. D. Subbulakshmi in the title role. 
 Marathi play "Mitra"  by Shirish Athwale,  Veeranna had adapted it in his theater play and finally brought it onto the big screen in 1935. He produced the film under the banner Shakuntala Films.

The film cast consisted of Veeranna himself in the role of a thief named "Pucca Kalla" and K. Ashwathamma, a singing actress, made her first onscreen appearance in the film. The music was composed by Venkataramaiah.

==Plot==
The film revolves around Sadarame, a brave woman who dares all the challenges she faces in life and a prince who admires her for her bravery and falls in love with her. She dons a male outfit to escape from a thief with a roving eyes ,called Pucca Kalla. Thinking her to be a male, a princess falls in love with her and a confusion over the characters follows. The films ends up with the prince marrying both Sadarame and the princess.

==Cast==
* Gubbi Veeranna as Pucca Kalla
* K. Ashwathamma as Sadarame
* CID Shakuntala
* Swarnamma
* B. Jayamma
* Murarachaar

==See Also==
* List of Kannada films of the 1930s

==References==
 

==External Sources==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 