The Conquest of Canaan
{{Infobox film
| name           = The Conquest of Canaan
| image          = Thomas Meighan-Doris Kenyon in The Conquest of Canaan.jpg
| image_size     =
| caption        = Still with Thomas Meighan and Doris Kenyon
| director       = Roy William Neill
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Frank Tuttle (scenario)
| based on       =  
| starring       = Thomas Meighan Doris Kenyon
| music          = Harry Perry
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States Silent (English intertitles)
}} silent drama film produced by Famous Players-Lasky and distributed by Paramount Pictures. It starred Thomas Meighan and Doris Kenyon and was directed by Roy William Neill. A previous version of the story was filmed in Asheville, North Carolina, in 1916.

==Cast==
*Thomas Meighan - Joe Louden
*Doris Kenyon - Ariel Taber
*Diana Allen - Mamie Pike
*Ann Egleston - Mrs. Louden
*Alice Fleming - Claudine
*Charles Abbe - Eskew Arp
*Malcolm Bradley - Jonas Taber
*Paul Everton - Happy Farley
*Macey Harlam - Nashville Cory
*Henry Hallam - Colonel Flintcroft
*Louis Hendricks - Judge Pike
*Charles Hartley - Peter Bradbury
*Jed Prouty - Norbert Flintcroft
*Cyril Ring - Gene Louden
*J. D. Walsh - Squire Buckelew
*Riley Hatch - Mike Sheenan

==Preservation status== lost for over seventy years until 2010, when a digital copy was returned to the United States from Russia. 

==References==
 

==External links==
 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 