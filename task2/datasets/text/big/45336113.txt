Samurai Cop
 
{{Infobox film
| name           = Samurai Cop
| image          = 
| caption        = 
| director       = Amir Shervan
| producer       = 
| writer         = Amir Shervan
| based on       = 
| starring       = Robert ZDar, Matt Hannon, Mark Frazer
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 
| rating         =
| country        = 
| language       = 
| budget         = 
}}
Samurai Cop is a 1991 action thriller directed and written by Amir Shervan and starring Robert ZDar, Matt Hannon, and Jannis Farley.

The film was never released theatrically and has since attained a cult appeal. A sequel is to be released in 2015.

==Plot==

A police officer trained in martial arts is asked to aid a police department fight a Yakuza gang operating in their jurisdiction.

==Cast==

* Robert ZDar as Yamashita 
* Matt Hannon as Joe Marshall
* Mark Frazer as Frank Washington

==Production==

Filming took place over several months from June 1990. When actor Hannon had considered shooting to be finished, he had his hair cut short, only to be told that further shooting was to be done. Director Shervan obtained a wig for the actor, which can be seen in several close up shots throughout the movie.   

==Release and sequel==

The movie was never given a theatrical release. The movie was given a more recent distribution by Cinema Epoch. Greg Hatanaka, founder of Cinema Epoch, is also responsible for producing and directing a sequel, Samurai Cop 2. 

==References==
 

==External links==
*  

 

 
 


 