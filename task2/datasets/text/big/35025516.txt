Borrowing Matchsticks
{{Infobox film name = Borrowing Matchsticks image = caption = director =Leonid Gaidai Risto Orko writer = Original play:   Tapio Vilpponen Vladlen Bakhnov Risto Orko starring = Yevgeny Leonov Vyacheslav Nevinny released =   runtime = 94 min (Soviet version) 100 min (Finnish version) music = Aleksandr Zatsepin country = Soviet Union Finland language = Russian / Finnish
}}
 Finnish comedy film produced by Mosfilm and Suomifilm in 1980. Film based on the novel by Algot Untola.

== Cast ==
* Yevgeny Leonov (Antti Ihalainen)
* Vyacheslav Nevinny (Jussi Vatanen)
* Georgy Vitsin (Tahvo Kennonen)
* Rita Polster (Anna-Lisa Ihalaynen)
* Ritva Valkama (Miina)
* Sergey Filippov (Hyuvyarinen)
* Nina Grebechkova (wife of Hyuvyarinen)
* Vera Ivleva (Anna-Qays)
* Galina Polskikh (Kaysa Karchutar)
* Mikhail Pugovkin (chief of Police)
* Kauko Helovirta (lieutenant Torvelaynen)
* Olavi Ahonen (Ville Huttunen)
* Leonid Kuravlyov (peasant)
* Viktor Uralsky (peasant)

== Plot ==
Antti Ihalaynen (Yevgeny Leonov) lives happily on the farm with his wife Anna-Lisa (Rita Polster). Once, when the house ran out of matches and the Ihalaynens had nothing to make a fire for cooking coffee so loved by the Finns, Anna-Lisa sent her husband to a neighbor Hyuvyarinen (Sergei Filippov) for matches.

On the way Ihalaynen meets an old friend Jussi Vatanen (Vyacheslav Nevinny). Widower Jussi asks Ihalaynen woo him Hyuvyarinens daughter Anna-Qays (Vera Ivleva). When they came to visit Hyuvyarinen, Antti praised his friend for a long time ("the meat in the house Vatanen is never depleted, even in summer") and finally uttered the sacred words: "And why not Anna-Qays to marry Jussi Vatanen?". The Hyuvyarinens were enthusiastic about this idea. Antti had forgotten what was the original purpose of his visit to the Hyuvyarinens.

Meanwhile, Taylor Tahvo Kennonen (Georgy Vitsin) from a nearby town in a drunken conversation with a customer tells that long ago he liked Anna-Lisa, but Antti Ihalaynen ahead of him. Tahvo Kennonen plans to see his old sweetheart.

Ihalaynen between said Jussi successful courtship, and the friends decide to go into the city – to buy gifts for the brides family, including grain spirit (vodka) for the old man Hyuvyarinen. In order to avoid buying a new bottle of this product, Jussi found an old one in the attic.

The friends Antti and Jussi themselves for 10 years as a "tied" to drink, because 10 years before, being drunk, they fought with a miller, he broke four ribs, and then for every broken millers rib the friends had to give him a cow. Since then, they refuse to drink.

Sadly, the bottle wasnt empty. Although Jussi suggested to "pour this stuff," two friends drank it and then went to town drunk. Got to meet them tailor Tahvo Kenanen they intimidated and nearly beat him. Known gossiper Ville Huttunen (Olavi Ahonen), who also caught them on the road, they jokingly said that they were going to America. Ville Huttunen was quick to spread the news throughout the district. The rumor came to the ears, and Anna-Lisa, who has already begun to worry about why the husband is not so long back from the neighbors. And inspired by this news Tahvo Kennonen in a hurry to make a marriage proposal, "the rest of the widow" Anne-Lisa. At this time, Antti and Jussi, arriving in the city, it is not in a hurry to go home. When Ihalaynen finally returned, and saw his wifes new husband, that it came out sideways...

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 