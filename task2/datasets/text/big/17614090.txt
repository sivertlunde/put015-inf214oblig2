Thin (film)
 
The 2006 cinéma vérité documentary film, Thin, directed by Lauren Greenfield and distributed by HBO, is an exploration of The Renfrew Center in Coconut Creek, Florida; a 40-bed residential facility for the treatment of women with eating disorders. The film mostly revolves around four women with anorexia nervosa and/or bulimia and their struggles for recovery. It premiered on HBO on November 14, 2006. 

 
THIN is the centerpiece of a multi-faceted campaign designed to explore issues surrounding body image and eating disorders, including a companion book, traveling exhibition of Greenfields work and a website. 
 

Having already shot photographs at Renfrew for her book Girl Culture, Greenfield returned to the facility to direct THIN, her directorial debut, which she produced in collaboration with producer R.J. Cutler. Living at the center for six months, Greenfield and director of photography Amanda Micheli received unrestricted access, filming not just the therapy sessions, mealtimes and daily weigh-ins that construct the highly structured routine of inpatients daily lives, but also exploring their turbulent interpersonal relationships with each other, with family and with staff. Access to staff meetings allows us insight into the efforts of the Renfrew medical team and the complex tasks facing them.

 
The making of the documentary THIN was a continuation of my decade-long exploration of body image and the way the female body has become a primary expression of identity for girls and women in our time.  I am intrigued by the way the female body has become a tablet on which our culture’s conflicting messages about femininity are written and rewritten. - Lauren Greenfield 
 

==Main Participants==

===Shelly=== PEG feeding tube  surgically implanted in her stomach. She admits herself into Renfrew after ten hospitalizations. On her arrival she weighs 84.3 pounds, having been anorexic for six years and tube fed for five of those years. She has an identical twin, Kelly, who does not have an eating disorder. Shelly describes a reliance on various mood stabilizers and tranquilizers and has volatile mood swings throughout the film, in particular getting very angry and aggressive when stolen food mistakenly attributed to her and pulse deficits lead to her being accused of purging.

At the end of the film we learn through on screen text that Shelly lost 17 pounds after discharge and underwent electric shock therapy to treat her depression.

Shelly is now recovering from her eating disorder. 

===Polly===
Pollack "Polly" Ann Williams has been at Renfrew for nine weeks. She admitted herself after a suicide attempt over two slices of pizza, explaining that while they werent the whole motivation, they were "kind of the straw that broke the camels back".  Interviewed, she tells us that "dieting has always been a huge part of my life" and that she was "counting calories and counting fat by the time I was 11".

She celebrates her 30th birthday in the Center and seems to be making progress with her anorexia but has trouble with some of the rules and is expelled after Shelly tells the staff about Polly getting the NEDA symbol tattooed on her hip while on a day pass from the Center; this compounded with Pollys disruptive history, having admitting during a Community Group therapy session to smoking in her bathroom and Shelly telling staff that Polly gave her the mood stabilisers discovered in her room during a routine check for contraband.

Upon being given her 24 hour notice to leave, Polly expresses despair and purges on camera. At the end of the film she is described through on screen text as still having trouble with purging and weight loss.

Polly moved to Chattanooga after leaving Renfrew, went back to school to study photography and was managing a studio at the time of the films release. She died at her residence on February 8, 2008.  Her death is believed to have been a suicide. 

===Brittany===
Brittany Robinson is a 15 year old student who was admitted to Renfrew with liver damage, a low heart rate and hair loss after dropping from 185 to  97 pounds in less than a year. She describes herself as a compulsive overeater from the age of 8, leading into compulsive dieting and anorexia from the age of 12, citing "a bad body image" and a craving for acceptance amongst her peers as her motivation to lose weight. According to Brittany, her mother also has an eating disorder, and in an interview Brittany describes how they would have "the greatest time" with EDNOS|"chew and spit"; chewing "bags and bags of candy" and spitting it out without swallowing. Her mothers experience of anorexia is touched upon in greater detail in an interview in the companion book, also titled THIN.
Throughout the film Brittany is resistant to recovery, explaining that she would like to lose "another forty pounds" and that she "just want  to be thin". She tells her nutritionist she has purged twelve times since entering Renfrew and walks out of group therapy in tears when her dedication to recovery is challenged by Polly.

Brittanys insurance benefits run out, forcing her to leave treatment early. She relapses into anorexia before leaving, prompting concern amongst patients and staff, and is released against medical advice.

At the end of the film we learn through on screen text that she began to restrict after discharge and lost weight rapidly. Insurance would not pay for further treatment.

A couple years after filming ended she mentioned going to rehab for a heroin addiction on her Myspace. 

===Alisa===
Alisa Williams, a 30 year old divorced mother of two, traces her eating issues back to an incident at the pediatrician when she was 7 and put on a diet which then let to anorexia. She describes massive binge/purge sessions which resulted in hospitalizations due to the resulting dehydration and her severe abuse of diuretics, hydrochlorothiazide, laxatives, enemas and ipecac. Apart from her binges, which she describes as occurring "every few weeks or so, over and over again, for three or four days", she was restricting to under 200 calories a day prior to entering Renfrew. She had been hospitalized five times in the three months leading to her admittance to the Center.  Having struggled with her eating disorder for 16 years, Alisa took disability leave from her job as a pharmaceutical rep in order to enter treatment.

Alisa seems to respond well to Renfrew and towards the end of her stay expresses a desire to "taste recovery".

After discharge we see Alisa having dinner with her children and Shelly at a restaurant. Later we see Alisa purging in her bathroom at home. We learn through on screen text that she would go on to lose 20 pounds and attempt suicide, and that she returned to Renfrew for treatment and maintained a healthy weight after leaving.

== Awards ==

THIN was selected for competition at the 2006 Sundance Film Festival and won the John Grierson Award for best feature-length documentary at the London Film Festival 2006. It has also screened at Hot Docs and Full Frame, as well as film festivals in Chicago, Vancouver, Austin, St. Louis, Ojai, Galway and Sweden. It received the 2006 Documentary Grand Jury Prize at the Boston Independent Film Festival, Newport International Film Festival and Jackson Hole Film Festival. THIN has also been nominated for an International Documentary Association Award. 

== External links ==
*  
*  
*  
*  
*   - Greenfield picks five films that have influenced her photographic style
*  

== References ==

THIN by Lauren Greenfield, ISBN 0-8118-5633-X

 

 
 
 
 
 
 
 