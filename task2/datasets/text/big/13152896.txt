Long Fliv the King
 
{{Infobox film
| name           = Long Fliv the King
| image          =
| caption        =
| director       = Leo McCarey
| producer       = Hal Roach
| writer         = Charles Alphin H. M. Walker
| starring       = Charley Chase
| music          =
| cinematography = Floyd Jackman
| editing        = Richard C. Currier
| distributor    =
| released       =  
| runtime        = 25 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
 1920 Harold Lloyd film His Royal Slyness, about a young man who accidentally becomes the King of a tiny country.

==Cast==
* Charley Chase - Charles Chase
* Martha Sleeper - Princess Helga of Thermosa
* Max Davidson - Warfield
* Oliver Hardy - The Prime Ministers Assistant
* Fred Malatesta - Hamir of Uvocado, the Prime Minister
* John Aasen - Giant Swordsman (uncredited)
* Sammy Brooks - (uncredited)
* Helen Gilmore - Helgas Lady-in-Waiting (uncredited)
* Lon Poff - (uncredited)

==See also==
* List of American films of 1926
* Filmography of Oliver Hardy

==External links==
* 

 
 
 
 
 
 
 
 
 


 