Heroine Shikkaku
{{Infobox animanga/Header
| name            = Heroine Shikkaku
| image           = 
| caption         = 
| ja_kanji        = ヒロイン失格
| ja_romaji       = 
| genre           = Romance manga|Romance, Slice of life 
}}
{{Infobox animanga/Print
| type            = manga
| title           = 
| author          = Momoko Kōda
| illustrator     = 
| publisher       = Shueisha
| publisher_en    = 
| demographic     = Shōjo manga|Shōjo
| imprint         = 
| magazine        = Bessatsu Margaret
| magazine_en     = 
| published       = 
| first           = March 13, 2010
| last            = March 13, 2013
| volumes         = 9
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Tsutomu Hanabusa
| producer        = 
| writer          = Erika Yoshida
| music           = Katsu Yokoyama
| studio          = 
| released        = September 19, 2015
| runtime         = 
}}
 
  is a manga series written and illustrated by Momoko Kōda. It is published in French by Delcourt (publisher)|Delcourt.   A live action movie adaptation has been announced and planned to be released in 2015 with Mirei Kiritani as Hatori Matsuzaki.

==Cast==
*Mirei Kiritani as  
*Kento Yamazaki as  
*Kentarō Sakaguchi as  
*Ayano Fukuda as   
*Miwako Wagatsuma as   
*Maryjun Takahashi as  
*Akira Nakao as   
*Shingo Yanagisawa as   
*Seiji Rokkaku as    Mari Hamada as Ritas mother
*Riki Takeuchi as A man at a school cafeteria

==Reception==

===Sales===
Volume 7 has sold 40,276 copies (as of July 1, 2012),  volume 8 has sold 35,408 copies (as of October 28, 2012),  and volume 9 has sold 45,803 copies (as of March 2, 2013). 

==References==
 


==External links==
*  at Betsuma Magazine   
*    
*  at Twitter   

 

 
 
 
 
 
 
 
 


 