M (1931 film)
{{Infobox film
| name           = M
| image          = M poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Fritz Lang
| producer       = Seymour Nebenzal
| writer         = Fritz Lang Thea von Harbou Paul Falkenberg Adolf Jansen Karl Vash
| based on       = A newspaper article by Egon Jacobson
| starring       = Peter Lorre Otto Wernicke Gustaf Gründgens
| music          = Edvard Grieg
| cinematography = Fritz Arno Wagner
| editing        = Paul Falkenberg
| studio         = Nero-Film A.G.
| distributor    = Vereinigte Star-Film GmbH   Paramount Pictures (US) 20th Century Fox (UK)
| released       =  
| runtime        = 111 minutes  
| country        = Weimar Republic
| language       = German
}} thriller film directed by Fritz Lang and starring Peter Lorre. It was written by Lang and his wife Thea von Harbou and was the directors first sound film. 

Now considered a classic, Lang himself believed it was his finest work.  

==Plot== elimination game in the courtyard of an apartment building in Berlin  using a chant about a child murderer. A woman sets the table for dinner, waiting for her daughter to come home from school. A wanted poster warns of a serial killer preying on children, as anxious parents wait outside a school. 

Little Elsie Beckmann leaves school, bouncing a ball on her way home. She is approached by Hans Beckert, who is whistling "In the Hall of the Mountain King" by Edvard Grieg. He offers to buy her a balloon from a blind street-vendor. He walks and talks with her. Elsies place at the dinner table remains empty, her ball is shown rolling away across a patch of grass, and her balloon is lost in the telephone lines overhead.
 underworld business criminal bosses. They decide to organize their own manhunt, using beggars to watch and guard the children.

The police discover two clues corresponding to the killers letter in Beckerts rented rooms. They wait there to arrest him. 

Beckert sees a young girl in the reflection of a shop window. Following her, he is thwarted when the girl meets her mother. When he encounters another young girl, he succeeds in befriending her, but the blind beggar recognizes his whistling. The blind man tells one of his friends, who tails the killer with assistance from other beggars he alerts along the way. Afraid of losing him, one young man chalks a large M (for Mörder, meaning "murderer" in German) on his hand, pretends to trip and bumps into Beckert, marking the back of his clothing.
 silent alarm, the crooks narrowly escape with their prisoner before the police arrive. One, however, is captured and eventually tricked into revealing the purpose of the break-in (nothing was stolen) and where Beckert would be taken.
 homicide under German law). Beckert pleads to be handed over to the police, asking, "Who knows what its like to be me?" Just as the enraged mob is about to kill him, the police arrive to arrest both Beckert and the criminals.

As the real trial passes, five judges prepare to pass judgment on Beckert. Before the sentence is announced, the shot cuts to three of the victims mothers crying. Elsies mother says no sentence would bring back the dead children, and "One has to keep closer watch over the children". The screen goes black as she adds, "All of you". 

==Cast==
<!--These are all sources used in the Hans Beckert article. They were used for primary information, but they may have some actual use in this article.

 

   

   

 

   -->
 Mad Love Crime and The Man English along the way.   

* Otto Wernicke as Inspector Karl Lohmann. Wernicke made his breakthrough with M after playing many small roles in silent films for over a decade. After his part in M, he was in great demand due to the success of the film, including returning to the role of Karl Lohmann in The Testament of Doctor Mabuse, and he played supporting roles for the rest of his career.   

* Gustaf Gründgens as Der Schränker ("The Safecracker"). Gründgens received acclaim for his role in the film and established a successful career for himself under Nazi rule, ultimately becoming director of the Staatliches Schauspielhaus (National Dramatic Theatre).   

;Others
* Ellen Widmann as Frau Beckmann
* Inge Landgut as Elsie Beckmann
* Theodor Loos as Inspector Groeber
* Friedrich Gnaß as Franz, the burglar
* Fritz Odemar as Cheater Paul Kemp as Pickpocket with seven watches
* Theo Lingen as Bauernfänger
* Rudolf Blümner as Beckerts defender
* Georg John as Blind balloon-seller
* Franz Stein as Minister
* Ernst Stahl-Nachbaur as Police chief
* Gerhard Bienert as Criminal secretary
* Karl Platen as Damowitz, a night-watchman
* Rosa Valetti as Innkeeper
* Hertha von Walther as Prostitute
* Hanna Maron (uncredited) as Girl in circle at the beginning Klaus Pohl as Witness / one-eyed man (uncredited) 

==Production==
Lang placed an advert in a newspaper in 1930 stating that his next film would be Mörder unter uns (Murderer Among Us) and that it was about a child murderer. He immediately began receiving threatening letters in the mail, and was also denied a studio space to shoot the film at Staaken studio. When Lang confronted the head of Stakken studio to find out why he was being denied access, the studio head informed Lang that he was a member of the Nazi party and that the party suspected that the film was meant to depict the Nazis.  This assumption was based entirely on the films original title and the Nazi party relented when told the films plot. 

M was eventually shot in six weeks at a Staaken Zeppelinhalle studio, just outside Berlin. Lang made the film for  s Squaring the Circle at night. Jensen. pp. 93.  

Lang did not show any acts of violence or deaths of children on screen, and later said that by only suggesting violence he forced "each individual member of the audience to create the gruesome details of the murder according to his personal imagination." Wakeman. pp. 615. 

 
M has been said, by various critics and reviewers,    to be based on serial killer Peter Kürten—the "Vampire of Düsseldorf"—whose crimes took place in the 1920s.    Lang denied that he drew from this case in an interview in 1963 with film historian Gero Gandert; "At the time I decided to use the subject matter of M there were many serial killers terrorizing Germany—Fritz Haarmann|Haarmann, Carl Großmann|Grossmann, Peter Kürten|Kürten, Karl Denke|Denke,  ". 

===Leitmotif===
M was Langs first sound film Jensen. pp. 95.  and Lang experimented with the new technology. It has a dense and complex soundtrack, as opposed to the more theatrical "talkies" being released at the same time. The soundtrack includes a narrator, sounds occurring off-camera, sounds motivating action, and suspenseful moments of silence before sudden noise. Lang was also able to make fewer cuts in the films editing, since sound effects could now be used to inform the narrative. 
 Peer Gynt Suite No. 1. Later in the film, the mere sound of the song lets the audience know that he is nearby, off-screen. This association of a musical theme with a particular character or situation is now a film staple.    Peter Lorre could not whistle, however – it is actually Langs wife and co-writer Thea von Harbou who is heard in the film.   

==Release and reception== premiered in Criterion Collection using prints from the same period from the Cinemateque Suisse and the Netherlands Film Museum.  A complete print of the English version and selected scenes from the French version were included in the 2010 Criterion Collection releases of the film. 

M was later released in the U.S. in 1933 by Foremco Pictures. After playing in German with English subtitles for two weeks, it was pulled from theaters and replaced by an English version. The re-dubbing was directed by Eric Hakim and Lorre was one of the few cast members to reprise his role in the film.  As with many other early talkies from the years 1930–1931, M was partially reshot with actors (including Lorre) performing dialogue in other languages for foreign markets after the German original was completed, apparently without Langs involvement. An English-language version was filmed and released in 1932 from an edited script with Lorre speaking his own words, his first English part. An edited French version was also released but despite the fact that Lorre spoke French his speaking parts were dubbed.

A Variety (magazine)|Variety review said that the film was "a little too long. Without spoiling the effect—even bettering it—cutting could be done. There are a few repetitions and a few slow scenes."  Graham Greene compared the film to "looking through the eye-piece of a microscope, through which the tangled mind is exposed, laid flat on the slide: love and lust; nobility and perversity, hatred of itself and despair jumping at you from the jelly". 

In 2013 a DCP version was released by Kino Lorber and played theatrically in North America    in the original aspect ratio of 1.19:1.  Critic Kenneth Turan of the Los Angeles Times called this the "most-complete-ever version" at 111 minutes.  The film was restored by TLEFilms Film Restoration & Preservation Services (Berlin) in association with Archives françaises du film - CNC (Paris) and PostFactory GmbH (Berlin). 

==Legacy==
Lang considered M to be his favorite of his own films because of the social criticism in the film. In 1937, he told a reporter that he made the film "to warn mothers about neglecting children". 
 the same name was released in 1951, shifting the action from Berlin to Los Angeles. Nero Films head Seymour Nebenzal and his son Harold produced the film for Columbia Pictures. Lang had once told a reporter "People ask me why I do not remake M in English. I have no reason to do that. I said all I had to say about that subject in the picture. Now I have other things to say."  The remake was directed by Joseph Losey and starred David Wayne in Lorres role. Losey stated that he had seen M in the early 1930s and watched it again shortly before shooting the remake, but that he "never referred to it. I only consciously repeated one shot. There may have been unconscious repetitions in terms of the atmosphere, of certain sequences."  Lang later said that when the remake was released he "had the best reviews of my life". 

The original 1931 M was ranked at number thirty-three in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. 

==See also==
* Trial movies
* List of films featuring surveillance

==References==
 

==External links==
 
*  
*  
*   from TLEFilms.com
*  
*  
*  
*  
*   by Stanley Kauffmann
*   Photographs and literature
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 