Kannukkul Nilavu
{{Infobox film
| name           = Kannukkul Nilavu
| image          =  Kannukkul Nilavu dvd.jpg
| caption        = Official DVD Box Cover
| starring       =   Fazil
| writer         = 
| narrator       =
| music          = Ilaiyaraaja
| cinematography = Ananda Kuttan
| editing        = T. R. Sekar
| distributor    = 
| studio         = Sinthamani Cine Arts
| producer       = Mohan Natarajan
| released       =  
| runtime        = 170 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 2000 Tamil Tamil drama Vijay and Kaveri and Ponvannan play other supporting roles.  The film opened in January 2000 to positive reviews, and hit at boxoffice.

==Plot==
Gautham Prabhakar (Vijay) meets Dr.Rajasekhar (Raghuvaran), a psychologist, his daughter Hema (Shalini) and her friends, Surya (Charle) & Chandru (Dhamu). He has memories of a girl named Gayathri (Kaveri), whom he met briefly and wishes to find again. After examining his subconscious mind, Dr.Rajasekhar finds out that Gautham is suffering from split personality and has forgotten a lot of things, including the details of Gayathris disappearance. Dr.Rajasekhar finds out that Gautham is looking for Shanmugam and his two friends, who Gautham feels have killed Gayathri. Hema falls in love with Gautham. Because of the affection and care shown by his mother, Gautham slowly recovers all the missing details about why and how he got to where he is. After attempts at revenge, Gautham starts a new life with Hema gaining the acceptance his mother and her father.

==Cast== Vijay as Gautham Prabhakar Shalini as Hema Kaveri as Gayathri
*Raghuvaran as Dr.Rajasekhar
*Srividya as Gauthams mother
*Charle as Surya
*Dhamu as Chandra
*Madhan Bob as Belgium
*Thalaivasal Vijay as Shanmugam
*Ponvannan as Soundar
*Shajin as Prakash

==Production==
Mohan Natarajan brought together the team of the successful 1997 film, Kadhalukku Mariyadhai, in director Fazil and leading pair Vijay and Shalini with the venture. Anandakuttan was signed on to handle the camera and Ilaiyaraja to compose the music, while Shekar took charge of the editing, while the art direction was handled by Mani Suchitra. The director shot the film across 75 days across the picturesque sites of Alapuzha, Ernakulam and the hilly area of Vahamam. 

==Release==
The film opened to positive reviews with The Hindu claiming that it was a "tremendous comeback vehicle for Vijay, wherein he portrays an entire gamut of emotions". The reviewer added that the film was "subtle in scenic presentation and characterisation, suspense without melodrama and spontaneity in reactions are plusses", while mentioning that Shalini was "flawless" in her role.  Rediff.com described it as a "film worth seeing", labelling that "the highlight of the film is actor Vijays power-packed performance". The critic described that the "actor has shown laudable skill both in the way he has handled this complicated role and in expressing various shades of Gauthams tormented mind. Especially from the moment he becomes aggressive, with the violent streak predominating and eyes blazing with manic fury." 

Cinematographer Ananda Kuttan won the Tamil Nadu State Film Award for Best Cinematographer for his work in the film. 
==Soundtrack==
The films score and soundtrack is composed by Ilaiyaraaja.   

{{Infobox album
| Name = Kannukkul Nilavu
| Longtype = to Kannukkul Nilavu
| Type = Soundtrack Ilaiyaraaja
| Label = Five Star Audio
| Cover =
| Released = 2000 Feature film soundtrack Tamil
| Ilaiyaraaja
| Last album = Guru Ramana Geetam (2000)
| This album = Kannukkul Nilavu (2000)
| Next album = Kutty (2001 film)|Kutty (2001)
}}

{{tracklist
| extra_column = Singer(s)
| title1 = Iravu Pagalai Theda &nbsp;
| extra1 = K. J. Yesudas
| length1 = 5:16
| title2 = Nilavu Paatu Hariharan
| length2 = 6:04
| title3 = Oru Naal Oru Kanavu (Male version)
| extra3 = K. J. Yesudas
| length3 = 4:49
| title4 = Oru Naal Oru Kanavu (duet)
| extra4 = K. J. Yesudas, Anuradha Sriram
| length4 = 4:49
| title5 = Adida Melathai
| extra5 = S.P.Balasubramaniam, S. N. Surendar, Arun Mozhi
| length5 = 5:11
| title6 = Roja Poonthotam
| extra6 = P. Unni Krishnan, Anuradha Sriram
| length6 =  4:54
| title7 = Enthen Kuilenge
| extra7 = P. Unni Krishnan, Anuradha Sriram
| length7 =  4:55
| title8 = Sinnan Chiru (Female version)
| extra8 = K.S.Chitra
| length8 =  5:03
}}

==References==
 

==External links==
*  

 

 
 
 
 
 