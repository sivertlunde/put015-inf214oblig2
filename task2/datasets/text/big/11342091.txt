Word Wars
{{Infobox Film
| name           = Word Wars
| image          = Word Wars poster.jpg
| caption        = 
| producer     = Eric Chaikin
| director       = Eric Chaikin  Julian Petrillo Matt Graham Marlon Hill Joel Sherman
| music          = Thor Madsen
| cinematography = Laela Kilbourn
| editing        = Conor ONeill
| distributor    = 7th Art Releasing
| released       = 
| runtime        = 80 min. English 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 2004 documentary film directed by Eric Chaikin and Julian Petrillo about competitive Scrabble playing. 
Its full title is: Word Wars - Tiles and Tribulations on the Scrabble Circuit. The film was an official selection at the 2004 Sundance film festival,  had a 25-city theatrical run, was included as part of the Discovery Times Channels "Screening Room" series, and was nominated for numerous awards including a 2004 Documentary Emmy for "Best Artistic or Cultural Programming" and an International Documentary Association (IDA) Award. The film is distributed by 7th Art Releasing. 
 Matt Graham, Word Freak, as does Chaikin. Fatsis and Chaikin are both tournament Scrabble players themselves. 

== Soundtrack ==
* E Wolf & the Moneylenders - "Paralyzed" (Written by Chalkin & Haber)
* Your Mom - "The One" (Written by Your Mom)
* The Minutemen - "Do You Want New Wave (Or Do You Want the Truth)" (Written by Mike Watt)
* The Minutemen - "The Glory of Man" (Written by Mike Watt)
* Andrew Chalkin & Austin Willacy - "Across the Universe" (Written by John Lennon and Paul McCartney)
* Joel Sherman - "Across the Universe" (Written by John Lennon and Paul McCartney)

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 


 