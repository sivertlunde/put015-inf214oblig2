Profesor Wilczur
{{Infobox film
| name           = Profesor Wilczur
| image          =
| image_size     = 
| caption        = 
| director       =Michał Waszyński
| producer       = 
| writer         =Tadeusz Dolega-Mostowicz (novel), Anatol Stern (writer)
| narrator       = 
| starring       = 
| music          =    
| cinematography =  
| editing        = 
| distributor    = 
| released       =18 October 1938
| runtime        =88 minutes
| country        = Poland Polish
| budget         = 
}}
 1938 Poland|Polish romantic drama film directed by Michał Waszyński. It is based on the novel by Tadeusz Dolega-Mostowicz.

==Cast==
*Kazimierz Junosza-Stepowski ...  Prof. Rafal Wilczur 
*Jacek Woszczerowicz ...  Jemiol 
*Elzbieta Barszczewska ...  Marysia Wilczurówna / Czynska 
*Witold Zacharewicz ...  Leszek Czynski 
*Dobieslaw Damiecki ...  Juliusz Dembicz 
*Józef Wegrzyn ...  Dr. Stefan Dobraniecki 
*Pelagia Relewicz-Ziembinska ...  Lila Dobraniecka 
*Mieczyslawa Cwiklinska ...  Florentyna Szkopkowa 
*Marysia R. ...  Elza Czynska (as Three-Year-Old Marysia R.) 
*Henryk Modrzewski ...  Dembiczs Manager 
*Wlodzimierz Lozinski ...  Wasyl Prokop 
*Wanda Jakubinska ...  Sick Boys Mother 
*Tekla Trapszo ...  Dr. Zygmunts Mother 
*Mieczyslaw Winkler ...  Wilczurs Servant Ludwik 
*Paweł Owerłło ...  Minister Dolant

== External links ==
*  

 

 
 
 
 
 
 
 
 

 
 