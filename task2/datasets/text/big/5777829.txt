Aurora: Operation Intercept
{{Infobox film
| name           = Aurora: Operation Intercept
| image          = Aurora Operation Intercept.jpg
| image size     =
| caption        =
| director       = Paul Levine
| producer       =
| writer         = Paul Levine
| narrator       =
| starring       = Bruce Payne Lance Henriksen Corbin Bernsen
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
}}
 John Stockwell.
 Aurora aircraft.  In the film, it is being stolen by a Russian terrorist who tries to destroy the White House. The propulsion system is a "combined cycle" conventional/scramjet engine.

== Summary ==
Francesca Zaborszin (Natalya Andreychenko) believes that the U.S. government murdered her father (Curt Lowens), a Soviet scientist and defector, and made it look like suicide.

Francesca sets up her own base in the deserts of Kazakhstan, wielding powerful electromagnetic pulses channelled though orbiting navigation satellites to attack and bring down civilian aircraft. She also captures the revolutionary high-altitude fighter-bomber Aurora One.
 John Stockwell), who soar into action in Aurora Two. Forced down by Zaborszin, the pair are caught and tortured by her private army of sinister Slavs, but Pruitt escapes.

Eventually, things lead both Francesca and Pruitt to take to the skies in separate Auroras. She penetrates D.C. airspace to make a bombing run at the White House, but Pruitts afterburners cause Francescas jet to barrel roll and explode, killing Francesca.

==Significance==

Commentators have noted that since the making of the movie, reality has imitated art, in that the September 11 attacks involved the crashing of planes into strategic buildings.  

==Cast==

*Bruce Payne as Gordon Pruett
*Natalya Andreychenko as Francesca Zaborszin
*Lance Henriksen as William Stenghel
*Corbin Bernsen as Flight Engineer Murphy
*Curt Lowens as Dr. Zaborszin John Stockwell as Andy Aldrich
*Michael Champion as Johann Wells
*Dennis Christopher as Victor Varenkov
*Corinne Bohrer as Sharon Pruett

==References==
 

==External links==
*  
*  

 
 
 

 