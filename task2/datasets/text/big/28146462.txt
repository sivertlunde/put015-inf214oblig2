Oral Mathram
{{Infobox film
| name           = Oral Mathram
| image          = 
| image_size     = 
| caption        = 
| director       = Sathyan Anthikkad
| producer       = 
| writer         = S. N. Swamy
| narrator       =  Shruti
| Johnson
| cinematography = Vipin Mohan	
| editing        = 
| distributor    = 
| released       =  
| runtime        = 149 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} 
 Shruti and Lalu Alex.

==Plot==
Shekhara Menon (Thilakan) is retired income tax officer from Mumbai and has  now settled in a remote village in Kerala. He owns some land and a house where he and his three daughters live together and leads a clam, silent and happy life. Menon owns a smaller house adjacent to his own which is given for rent, and his source of income other that agricultural income. Mr. Menons Pension from service has been blocked due to some unclear issues.  Hareendran (Mammootty) is a struggling small time contractor and comes in as a new tenant to the rented house. Hareendran is constantly put under unwanted troubles by his aide Balachandran (Sudheesh), who convinces Hareendran to help people in trouble. His experience with Balachandran has made Hareendran a selfish man and he presently is least concerned about other peoples problems around him.

But the things take a change after Menon goes absconding leaving three daughters alone in the house. Hareendran the selfish tenant quickly decides to vacate the house to avoid unwanted troubles. Even though selfish and unkind outside, the good hearted Hareendran cannot stand the harsh difficulties forced to be faced by the girls in front of his house and rest of the story is about how only Hareendran (Oral Mathram -> the only person) steps up to help the girls.

==Cast==
* Thilakan as Shekhara Menon
* Mammootty as Hareendran Sreenivasan as Sachidanandan
* Sudheesh as Balachandran
* Lalu Alex as Inspector Mathews Shruti as Devika Menon
* Praveena as Malavika Menon
* Kavya Madhavan as Gopika Menon
* Oduvil Unnikrishnan as Pankunny Menon
* Mamukkoya as Kunjhalikutty
* Sankaradi as Nambiar, the tea shop owner
*  Mahesh (actor)| Mahesh

==Soundtrack==
{{Infobox album
| Name        = Oral Mathram
| Type        = Soundtrack
| Artist      = Johnson (composer) | Johnson 
| Language = Malayalam
| Genre       = Film
|}}

The film features songs composed by Johnson (composer) | Johnson  and written by Kaithapram.

{| class="wikitable"
|-
! Song Title !! Singer
|-
| Aardramaay Chandrakalabham ||  KJ Yesudas
|-
| Aardramaay Chandrakalabham ||  KS Chithra
|-
| Chaithranilaavinte ||  KJ Yesudas
|-
| Kaarvarnane Kando Sakhi ||  KJ Yesudas
|-
| Kaarvarnane Kando Sakhi ||  KS Chithra
|-
| Mangalappaala ||  KS Chithra
|}

== External links ==
* http://www.imdb.com/title/tt0246831/
* http://www.chakpak.com/movie/oral-mathram/cast/7523

 
 
 


 