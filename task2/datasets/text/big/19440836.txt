Men of War (film)
 
{{Infobox Film
| name           = Men of War
| image          = Men-of-War-poster.jpg
| caption        = Film poster
| director       = Perry Lang
| producer       = Arthur Goldblatt Andrew Pfeffer
| writer         = John Sayles Ethan Reiff Cyrus Voris
| starring       = Dolph Lundgren Charlotte Lewis
| music          = Gerald Gouriet Paul Rabjohns
| cinematography = Rohn Schmidt
| editing        = Jeffrey Reiner
| studio         = MDP Worldwide Pomerance Corporation Grandview Avenue Pictures
| distributor    = Miramax Films MDP Worldwide
| released       =  
| runtime        = 103 minutes
| country        = United States Spain Thailand 
| language       = English
| budget         = $6 million 
| gross          =
}}
Men of War is a 1994 action film directed by Perry Lang, written by John Sayles, and revised by Ethan Reiff and Cyrus Voris. It stars Dolph Lundgren as Nick Gunar, a former Special Ops soldier who leads a group of mercenaries to a treasure island in the South China Sea.

== Plot ==
Nick Gunar, an ex-soldier, is down and out in Chicago in a typically cold winter. When two men offer him a job on a tropical island, he reluctantly agrees and gathers together a group of soldier friends also down on their luck. They arrive in the Far East and come up against the evil Keefer, who was with them in Angola. He now owns the local police force and shows Nick and the others whos boss. Afterwards, Nick and his crew go to the island, which they find is occupied by peaceful natives. Nick respects them, but there is friction caused by a few mercenaries who want to kill natives to reveal where the treasure is. Nick stops them and finds out the treasure they are after is guano (which does have value in sufficient quantities). The two men intend to strip the island to get it, leaving the natives as casualties. The group divides up, and Nick hands half of them their marching orders (and pay) as he refuses to kill the natives to do the job.
 Carl Gustav), which causes mayhem. After a number of attacks by both sides, many are dead and  there is a final showdown between Nick and Keefer; Keefer gains the upper hand in the fist fight and attempts to strangle Nick to death while drowning him. Nick manages to grab a nearby bone and stab Keefer in the neck, then proceeds to drown him. As the natives bury their dead, the few surviving mercenaries leave, but Nick stays behind.

== Cast ==
* Dolph Lundgren as Nick Gunar
* Charlotte Lewis as Loki
* B. D. Wong as Po
* Anthony Denison as Jimmy G
* Tim Guinee as Ocker Don Harvey as Nolan
* Tom Lister, Jr. as Blades Tom Wright as Jamaal Catherine Bell as Grace Lashield
* Trevor Goddard as Keefer
* Kevin Tighe as Colonel Merrick
* Thomas Gibson as Warren
* Perry Lang as Lyle
* Aldo Sambrell as Goldmouth
* Juan Pedro Tudela as Waldo

== Production ==
Sayles original idea for the plot involved a mercenary who rethinks his life when he comes upon a tribe of pacifists.  Rewrites from Reiff and Voris added more testosterone-fueled action.   Filming took place in Thailand in September 1993. 

== Release == Region 1.   On February 23, 2009, Anchor Bay Entertainment released it on DVD in the United Kingdom in Region 2.   

== Reception ==
Michael Sauter of Entertainment Weekly rated it C− and called it a "straight-to-video clunker".   Susan King of the Los Angeles Times called it "a better-than-average straight-to-video action thriller."   Leigh Riding of DVDactive rated it 7/10 stars and called it "a sturdy action drama" that is "surprisingly intriguing and entertaining". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 