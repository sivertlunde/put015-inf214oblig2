Woman of Straw
{{Infobox film
  | name = Woman of Straw
  | image = Woman of Straw poster.jpg
  | image_size = 175px
  | caption = original movie poster
  | director = Basil Dearden 
  | producer = Michael Relph
  | writer = Robert Muller Stanley Mann
  | starring = Gina Lollobrigida Sean Connery Ralph Richardson
  | music = Norman Percival
  | cinematography =Otto Heller
  | editing =John D. Guthridge
  | distributor = United Artists
  | released = 1964
  | runtime = 122 min.
  | country = United Kingdom
  | language = English
  | budget = 
  }}
Woman of Straw is a 1964 British crime thriller starring Gina Lollobrigida and Sean Connery. It was directed by Basil Dearden and written by Robert Muller and Stanley Mann, adapted from the 1964 novel by Catherine Arley.

== Plot ==

Connerys character Anthony Richmond schemes to get the fortune of his tyrannical, wheelchair-using tycoon uncle Charles Richmond (Richardson) by persuading Maria, a nurse he employs (Lollobrigida), to marry him. After his uncles demise Anthony becomes a murder suspect. Lollobrigidas character is the Woman of Straw of the title.

== Cast ==
* Gina Lollobrigida as Maria Marcello
* Sean Connery as Anthony Tony Richmond
* Sir Ralph Richardson as Charles Richmond
* Alexander Knox as Detective Inspector Lomer
* Johnny Sekka as Thomas
* Laurence Hardy as Baynes, the butler Peter Madden as Yacht Captain
* Danny Daniels as Fenton
* Noel Howlett as Assistant Solicitor

==Production==
The film was shot at Pinewood Studios, Audley End House in Saffron Walden, Essex and in Majorca in the Balearic Islands between August and October 1963.       The Majorca footage, including much footage in a boat off the coast, was shot on location in September 1963. Gina Lollobrigida was reportedly "demanding and temperamental" during the filming, frequently clashing with Connery and director Dearden. 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 
 