Nice Girl?
{{Infobox film
| name           = Nice Girl?
| image          = Nice Girl? FilmPoster.jpeg
| border         = 
| caption        = Theatrical release poster
| director       = William A. Seiter
| producer       = Joe Pasternak
| screenplay     = {{Plainlist|
* Richard Connell
* Gladys Lehman
}}
| based on       =  
| starring       = {{Plainlist|
* Deanna Durbin
* Franchot Tone
* Walter Brennan
* Robert Stack
* Robert Benchley
}}
| music          = Charles Previn (director)
| cinematography = 
| editing        = 
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Nice Girl? is a 1941 American musical film directed by William A. Seiter and starring Deanna Durbin, Franchot Tone, Walter Brennan, Robert Stack,       and Robert Benchley.  Based on the play Nice Girl? by Phyllis Duganne, the film is about a young girl finds herself attracted to one of her fathers business partners who comes to town to give her father a scholarship for his dietary studies. 

==Plot==
The musical starts with the busybody postman who reads everybodys mail, Hector (Walter Brennan), delivering mail to the Dana household and particularly to Cora (Helen Broderick), the maid who he is in love with. Professor Oliver Dana (Robert Benchley) is the head of the household. The oldest sister is Sylvia (Anne Gwynne), an actress, and the youngest is Nancy (Ann Gillis), who is a bit of a flirt and has all the boys fighting over her. The middle sister Jane (Deanna Durbin), the "nice girl", makes her entry singing the song "Perhaps" to the rabbits she takes care of. Her father is trying to write a book about diet and is testing it on rabbits.  Don Webb (Robert Stack) is her boyfriend, an avid car buff.  

One day, Richard Calvert (Franchot Tone) visits the Danas to study what Professor Dana is practicing with the rabbits. The three daughters are instantly enamored with him and do all they can to impress him. The family gets together with Jane playing the piano and singing "Beneath the Lights of Home", accompanied by Oliver, Nancy, and Sylvia. 

There is a Fourth of July celebration in which Oliver Dana gives a speech. After a bit of dancing, Jane sings "Old Folks at Home". Don lends Jane the car to take Richard to the train station, but she decides to stop being a "nice girl" and drives him to New York. When they get there she changes into some clothes that belong to Richard’s sister and then plays a song on the piano, "Love at Last" while she sings and Richard comes down and sees her at the piano. Richard quickly sees that she is trying to not be a "nice girl", but both of them realize that she is just playing a game and not really being who she is.  So she leaves his house and drives back home, still wearing Richard’s sister’s pajamas. She gets into town but the car runs out of gas and she manages to wake up the entire neighborhood. They all see that she is no longer a "nice girl" and rumors start to fly instantly. Jane locks herself in her room until her father gives her a telegram from Richard.
	
Jane decides to face the music and go to the benefit for the International Red Cross and Red Crescent Movement, but doesnt realize that everyone thinks that she and Richard are engaged. She sings "Beneath the Lights of Home" at the benefit. Everyone congratulates her afterwards about her engagement, but she thinks they are just fooling her. She goes to Don for comfort, and he agrees that it’s all nonsense and of course Richard wouldn’t go for her. Jane gets angry at that and decides to show Don. She tells him that she is engaged to Richard and Don storms off. Right at that time Oliver and Richard come to the benefit. They congratulate Prof. Oliver (for the engagement of his daughter to Richard Calvert), but he is confused and thinks that they are congratulating him for getting his fellowship. Jane intercepts them and tells Richard that everyone thinks they are engaged.  They fake a big quarrel so that they can break their engagement.
	
Meanwhile, Don has enlisted in the army, and Jane goes to see him to explain what happened. When she goes, Don tells her that he loves her. They kiss (off-screen) and then she goes to a bandstand and sings the song "Thank You America" for the crowd at the Army base. 

==Alternative Endings for Foreign Release==
 
For the theatrical release in Great Britain, the movie instead concludes with Deanna Durbin singing "Therell Always be an England", since WWII was raging in  Europe - in fact, the Battle of Britain had concluded just days before Nice Girl? began shooting. Deanna Durbin had a huge fan base in England who knew that Durbins parents were English immigrants to the Americas. The set behind Durbin shows both the Flag of the United States and the Union Jack at the back of the army base bandstand.

For release in Latin America, Durbin was filmed singing a rendition of "Thank You America" in Spanish.

==Cast==
* Deanna Durbin as Jane Dana
* Franchot Tone as Richard Calvert
* Walter Brennan as Hector Titus
* Robert Stack as Don Webb
* Robert Benchley as Prof. Oliver Dana
* Helen Broderick as Cora Foster
* Ann Gillis as Nancy Dana
* Anne Gwynne as Sylvia Dana
* Elisabeth Risdon as Martha Peasley
* Nana Bryant as Mary Peasley
* Georgie Billings as Pinky Greene Tommy Kelly as Ken Atkins
* Marcia Mae Jones as Girl    

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 