Doosra Aadmi
{{Infobox film
| name           = Doosra Aadmi
| image          = Doosra Aadmi,1977 Hindi film poster.jpg
| image_size     = 190px
| caption        = 
| director       = Ramesh Talwar
| producer       = Yash Chopra
| writer         = Raju Saigal (story) Sagar Sarhadi (screenplay)
| narrator       = 
| starring       = Rishi Kapoor Neetu Singh Raakhee
| music          = Rajesh Roshan Majrooh Sultanpuri (Lyrics)
| cinematography = Romesh Bhalla	
| editing        = B. Mangeshkar	 	
| distributor    = 
| released       =  
| runtime        = 136 Minutes  
| country        = India
| language       = Hindi
| budget         = 
}}

Doosra Aadmi (The Second Man) is a 1977 Hindi film, produced by Yash Chopra and directed by Ramesh Talwar. The film stars Rishi Kapoor, Neetu Singh, Raakhee, Shashi Kapoor, Deven Verma and Parikshat Sahni. The films music is by Rajesh Roshan.

==Plot summary==
Nisha (Raakhee), an accomplished architect, becomes a recluse after the tragic death of her boyfriend, Shashi Saigal (Shashi Kapoor). Years later, Karan Saxena (Rishi Kapoor) offers her employment with his advertising agency and she accepts. She realises that Karan, recently married to Timsy (Neetu Singh), reminds her of Shashi, and she will do anything in her power to be with him.

==Trivia==
The movie that Karan and Nisha watch at the cinema, when Timsy is spying on them, is 40 Carats (film)|"40 Carats". 

Shashi Kapoor, who played Shashi Sehgal in this movie, is in real life the youngest paternal uncle of Rishi Kapoor, who played Karan Saxena (Kannu). That was used as the plot point for their similar looks.

==Cast==
* Raakhee - Nisha
* Rishi Kapoor -  Karan Saxena aka Kannu
* Neetu Singh - Timsi
* Deven Verma -  Timsis Uncle
* Parikshat Sahni - Bhisham
* Satyendra Kapoor - Ram Prasad Saxena (as Satyendra Kappu)
* Gita Siddharth - Mrs. Saxena
* Shashi Kapoor - Shashi Sehgal Javed Khan
* Jagdish Raj - Police Inspector
* Kiran Vairale

==Crew==
* Art Direction: Desh Mukherji
* Costume Design: Jennifer Kapoor, Rajee Singh

==Soundtrack==
The soundtrack includes the following tracks, composed by Rajesh Roshan, and with lyrics by Majrooh Sultanpuri. 
{{Infobox album  
| Name        = Doosra Aadmi
| Type        = Soundtrack
| Artist      = Rajesh Roshan
| Cover       = Doosra Aadmi, 1977 Hindi film, soundtrack album cover.jpg
| Released    = 1977 (India)
| Recorded    =  
| Genre       = Film soundtrack
| Length      = 
| Label       = EMI
| Producer    = Rajesh Roshan
| Reviews     = 
| Last album  = Yehi Hai Zindagi   (1977)
| This album  = Doosra Aadmi  (1977)
| Next album  = Jay-Vijay   (1977)
|}}
{| class="wikitable" width="70%"
 |- style="background:#d9e4f1; text-align:center;"
 !Song
 !Singers
 |-rowspan="1" style="text-align:center; background:#d9e4f1; textcolor:#000;"
| Chal Kahin Door Nikal Jayein
| Kishore Kumar, Lata Mangeshkar, Mohd. Rafi
|-rowspan="1" style="text-align:center; background:#d9e4f1; textcolor:#000;"
|  Nazron Se Kah Do
| Kishore Kumar, Lata Mangeshkar
|-rowspan="1" style="text-align:center; background:#d9e4f1; textcolor:#000;"
| Aao Manayen Jashn-E-Mohabbat
| Kishore Kumar, Lata Mangeshkar
|-rowspan="1" style="text-align:center; background:#d9e4f1; textcolor:#000;"
|  Ankhon Mein, Kajal Hai Kishore Kumar, Lata Mangeshkar
|-rowspan="1" style="text-align:center; background:#d9e4f1; textcolor:#000;"
|  Jaan Meri Rooth Gayi Kishore Kumar, Lata Mangeshkar
|-rowspan="1" style="text-align:center; background:#d9e4f1; textcolor:#000;"
|  Angna Ayenge Sanvariya
| Deven Verma, Pamela Chopra, Chorus
|}

==Awards and nominations==
* 1978: Filmfare Awards: Nominations
**  
**   Best Story: Raju Saigal 

==References==
 

==External links==
*  
*   at Yash Raj Films

 

 
 
 
 