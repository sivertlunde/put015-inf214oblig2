G.I. Wanna Home
{{Infobox Film |
  | name           = G.I. Wanna Home 
  | image          = GIWannaHomeTITLE.jpg
  | caption  =  Columbia Pictures tagged Moe and Larrys names incorrectly on this one-sheet for G.I. Wanna Home. |
  | director       = Jules White Felix Adler
  | starring       = Moe Howard Larry Fine Curly Howard Judy Malcolm Ethelreda Leopold Doris Houck Symona Boniface Al Thompson
  | cinematography = George F. Kelley 
  | editing        = Edwin H. Bryant
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 15 44"
  | country        = United States
  | language       = English
}}

G.I. Wanna Home is the 94th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
At the end World War II, the Stooges are discharged from the service and return home. They are prepared to marry their fiancées (Judy Malcolm, Ethelreda Leopold, and Doris Houck), but are dispossessed. The boys search around for a room to rent, and hit blind alley after blind alley until finally settling for an open-lot-turned living quarters. All goes well with the unusual setup until a farmer on a tractor plows down the boys domicile.

Afterwards, the Stooges build a pathetically small apartment from "their own little hands", with the living room, dining room, and kitchen cramped into the space of a den.

==Production notes==
G.I. Wanna Home is often inadvertently referred to as G.I. Wanna Go Home. 

-During an gag in which Moe is pelted by several eggs Larry dropped from a tree, a camera man can be heard chuckling.

==Curlys illness== ad lib for the camera as in previous instances. His scene where he cleans potatoes is sluggish and lethargic. Films like Playing the Ponies, An Ache in Every Stake, Sock-a-Bye Baby, and I Can Hardly Wait are finer examples of Curly preparing food and creating comedy genius with little effort. 
 

==References==
 

==External links==
*  
*  
*  at  

 

 
 
 
 
 
 
 
 