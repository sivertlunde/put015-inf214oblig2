Eyes of a Thief
 
{{Infobox film
| name           = Eyes of a Thief
| image          = Eyes of a Thief.jpg
| caption        = Film poster
| director       = Najwa Najjar
| producer       = 
| writer         = Najwa Najjar
| starring       = Khaled Abol Naga
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Palestine
| language       = Arabic
| budget         = 
}}
 an incident Best Foreign Language Film at the 87th Academy Awards, but was not nominated.    The film is set in the West Bank. Its world premiere took place at the Ramallah Cultural Palace in Ramallah, Palestine. 

==Cast==
* Khaled Abol Naga as Tarek Senno
* Souad Massi as Lila
* Nisreen Faour as Duniya
* Maisa Abd Elhadi as Houda (as Maisa Abdel Hadi)
* Areen Omari as Salwa

==Reception== 
The film was received with criticism on both Israeli and Palestinian sides. While Israelis criticized the glorification of a murderer, Palestinians criticized the portrayal of the wife, seeing her as being not entirely faithful to her husband, who is being held in an Israeli jail. 

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Palestinian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 


 
 