Heaven's Prisoners
{{Infobox film
| name           = Heavens Prisoners
| image          = Heavens prisonersmp.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Phil Joanou
| producer       = Leslie Greif Andre Morgan Albert S. Ruddy
| screenplay     = Harley Peyton Scott Frank
| based on       =  
| starring       = Alec Baldwin Kelly Lynch Mary Stuart Masterson Teri Hatcher Eric Roberts Vondie Curtis-Hall Joe Viterelli Badja Djola Hawthorne James
| music          = George Fenton
| cinematography = Harris Savides
| editing        = William Steinkamp
| distributor    = New Line Cinema (US) Rank Film Distributors (UK)
| released       =  
| country        = United States
| budget         = $25,000,000 http://www.imdb.com/title/tt0116508/business 
| gross          = $5,009,305   
| language       = English
| runtime        = 132 min.
}} crime Thriller thriller film directed by Phil Joanou and starring Alec Baldwin, Kelly Lynch, Mary Stuart Masterson, Teri Hatcher and Eric Roberts. It is based on a Dave Robicheaux homonymous novel by James Lee Burke. Harley Peyton and Scott Frank wrote the screenplay.

The film was followed by In the Electric Mist (2009), starring Tommy Lee Jones as Dave Robicheaux. In the sequel, Robicheaux still lives in Louisiana and has come out of retirement as an Iberia Parish sheriffs detective.

==Plot==
A former police detective in New Orleans and a recovering alcoholic, Dave Robicheaux is living a quiet life in the swamplands of Louisiana with his wife Annie. The couples tranquility is shattered one day when a drug smugglers plane crashes in a lake, right before their eyes.
 Salvadoran girl, DEA officer drug kingpin in the area and Robicheauxs childhood friend from New Iberia, Dave becomes involved in solving the case and consequently finds himself and his family in danger.

Robicheaux is assaulted by two thugs as a warning. With help from his former girl-friend Robin, an exotic dancer who still has feelings for him, he continues to investigate. His longtime acquaintance Bubba denies any involvement, but Dave warns him and Bubbas sultry wife Claudette that he is going to find out who is behind all this and do something about it. He tracks down one of the men who attacked him, Eddie Keats, and splits his head open with a pool cue in Keats own bar.

Killers come to the Robicheaux home late one night. Robicheaux is unable to prevent his wife Annie from being killed. He falls off the wagon and neglects the young girl they adopted. Robin comes to stay with them.

Clearing his head, Robicheaux seeks vengeance against the three killers. He first goes after a large man called Toot, chasing him onto a streetcar and causing his death. Bubba and Claudette reassure a local mob boss named Giancano that they will not let this vendetta get out of hand, and Bubba gets into a fistfight with Robicheaux, falsely suspecting him of an affair with Claudette.

Eddie Keats is found dead before Robicheaux can get to him. Going after the last and most dangerous of the killers, Victor Romero, he knows that someone else must be giving them orders.

He finds Romero and kills him. Then, going to Bubbas home, Robicheaux discovers that it is Claudette who planned the hit. Bubba shoots her, and Robicheaux calls in the crime. When he returns home, Robin has left forever, and all Robicheaux has left in his life is his daughter Alafair.

==Cast==
* Alec Baldwin as Dave Robicheaux
* Kelly Lynch as Annie Robicheaux
* Mary Stuart Masterson as Robin Gaddis
* Teri Hatcher as Claudette Rocque
* Eric Roberts as Bubba Rocque
* Vondie Curtis-Hall as Minos P. Dautrieve
* Hawthorne James as Victor Romero
* Badja Djola as Batist
* Joe Viterelli as Didi Giancano
* Paul Guilfoyle as Det. Magelli
* Don Stark as Eddie Keats
* Carl A. McGee as Toot
* Tuck Milligan as Jerry Falgout

==Reception==
The film opened in fifth place grossing $2,308,797 its opening weekend playing in a total of 907 theaters at its widest point. However the film was a box office failure, grossing only $5,009,305, far below its $25,000,000 budget. The film also received generally negative reviews with a 16% "rotten" rating on Rotten Tomatoes based on 19 reviews.    Teri Hatchers performance earned her a Golden Raspberry Award nomination for Worst Supporting Actress.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 