About a Girl (film)
 {{Infobox film
| name           = About a Girl
| image          = 
| caption        = 
| director       = Brian Percival
| producer       = Janey de Nordwall Rebecca Wright
| writer         = Julie Rutterford
| screenplay     = 
| story          = 
| based on       =  
| starring       = Ashley Thewlis Sue Jaynes Laren Creek
| music          = 
| cinematography = Geoff Boyle
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 9 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 TCM Classic Shorts Award prize at the London Film Festival, and the Jury Prize at the Raindance Film Festival.  The script was written by Julie Rutterford and the film was produced by Janey de Nordwall. The leading role was played by Ashley Thewlis.

==Plot==
About a Girl opens with a striking shot of a silhouette &mdash; against a skyline of clouds above a field &mdash;  of a girl singing the Britney Spears song " " by Britney Spears and doing the routine. 

As she goes on walking alongside a canal, the girls stories become more and more underlain by an uncomfortable feeling that the gravity of her experiences does not match her flippant retelling of her everyday life: her descriptions of her pop idols and her favourite ice cream are mixed with hints about family troubles, poverty and domestic violence. 

The underside to her light-hearted storytelling is revealed in a shocking scene when, stating that she has become "good at hiding things", she throws the plastic bag she has been carrying into the canal. A camera shot from under the water reveals a baby surrounded by blood.

==External links==
*  
*   at the BBC Film Network

 
 
 
 

 