Pizza II: Villa
 
 

{{Infobox film
| name           = Pizza II: Villa
| image          = Pizza II the Villa.jpg
| caption        = Theatrical release poster
| director       = Deepan Chakravarthy
| producer       = C. V. Kumar
| writer         = Deepan Chakravarthy
| starring       = Ashok Selvan Sanchita Shetty
| music          = Santhosh Narayanan
| cinematography = Deepak Kumar Padhy
| editing        = Leo John Paul
| studio         = Thirukumaran Entertainment   Studio Green
| distributor    = ABI & ABI PICTURES
| released       =  
| runtime        = 102 minutes
| country        = India
| language       = Tamil Telugu
| budget         =  
| gross          =  (both versions)
}}

Pizza II: Villa is an Indian   (2012). {{cite web|title=Pizza 2-Villa is a gripping suspense thriller
 |publisher=Rediff.com |date=15 November 2013|accessdate=15 November 2013|url=http://www.rediff.com/movies/review/review-pizza-2-villa-is-a-gripping-suspense-thriller-south/20131115.htm}}  The filming commenced on 17 April and was completed in May 2013.    The film released in November 2013 to positive reviews from the critics and audience, and was later dubbed into Telugu under the same name. 

==Plot==
A debutant English novel writer Jebin M. Jose discovers that his dead father is bankrupt with an unlisted property in Puducherry. To pay off the debts, he makes a visit to the villa for valuation. But after seeing framed paintings by his father, he calls his girlfriend Aarthi, an amateur artist, to accompany him. She immediately likes the villa and urges him to not to sell. Meanwhile, Jebins career gets a new break when an established publisher comes forward to buy rights for his first novel Maybe, Maybe Not !. He also gets advance   in a two-book contract. So, Jebin continues his stay in the majestic villa as good omen and begins work on his second novel The Director.

Aarthi returns to Chennai to seek permission from her father for their marriage. One night, ecstatic Jebin plays the Victorian piano. The strings get jammed abruptly. He opens the lid and finds a secret key underneath. Jebin also gets curious on the remarkable geometry of a painting. It seemed more like a map than a work of art. Then he realises that it was similar to the floor plan of the villa given by his advocate. So he probes each room and stumbles upon a secret door behind a wardrobe. He uses the secret key to open the lock and sees plenty of frameless paintings hidden in a trunk. Many scenes appear to have come straight out of his life like his mothers death by car accident. Suddenly he realises that his father had precognition skills. So he concludes that though some of the paintings did not make sense but may predict the future events. Soon, Jebin wins a literary award (as predicted by his fathers paintings) and critical acclaim for his debut novel. But he gets panicky and tries to sell off the villa. The estate agent is injured in a freak accident by the cast-iron gate and the buyers back out of deals at the last minute because they thought the agent getting hurt was a bad sign. Jebin is frustrated and begins furiously working on his second novel. When Aarthi arrives back from Chennai, Jebin tells her about his discovery. They decide to burn all the paintings in the secret room. But almost instinctively, the secret room defends itself by toppling furniture and fittings to block his entry.
 negative energy apparitions begin to appear. In the chaos, one of Jebins friends lose his legs. Jebin also finds the identities and whereabouts of prior owners. But all report similar grim events in their lives like infanticide, Lunatic|lunacy, fratricide, disease, insolvency, death, et al. Frustrated Jebin decides to set fire to the villa and end the cycle once and for all.

But there is a multilayered twist in the tale. It is revealed that Jebins girl friend is not what she claimed to be, hoping to secure the cash Jebin would gain from the sale of the house rather than to continue their relationship, and she eventually dumps him and marries another. It is then implied that the biggest mistake Jebin does is thinking that he is the one in every painting, as the person in the marriage and murder paintings is not himself, but another man dubbed as the Director (S. J. Surya), who also turns out to be Aarthis husband. Thus, it can be understood that Jebin did not realise that he too inherited his fathers gift, with his second novel depicting that a "director" character would soon appear in the prophecy.

==Cast==
* Ashok Selvan as Jebin M. Jose
* Sanchita Shetty as Aarthi
* Nassar as Marshall P. Jose
* S. J. Surya as the director (Guest appearance)
* Kaali Venkat 
* Jaikumar
* Vegan Rajesh as Pratap

==Production==
After Pizza became successful, young software professional and short filmmaker Deepan Chakravarthy came up with the idea of making a sequel. He wrote the script in three weeks and submitted it to the producer, who thought of building it up as a franchise.    Thirukumaran Entertainment who produced Pizza produce the sequel too. In March 2013, Studio Green secured the film and co-produced it.  Abinesh Elangovan of Abi & Abi Pictures bought the theatrical rights of the movie. 

Vaibhav Reddy and Sanchita Shetty were cast as the lead pair at first.  Later it was said that K.E. Gnanavel Raja of Studio Green, chose to remove Vaibhav and Sanchita from the cast to replace them with bigger names.  But Sanchita was retained and Vaibhav only was replaced by Ashok Selvan,  who were both part of the hit film Soodhu Kavvum.  Deepan said that most of the actors of Pizza are part of Villa as well, but were playing different characters.    Ashok Selvan plays a 30-year-old writer in the film,  and Sanchitha plays lover of the hero in the film.    S. J. Suryah was also added to the cast, but his role was not disclosed to the public. 

A song sequence, a romantic number at that, called Aayiram Enngal was filmed at   sound. 
This film had completed their shooting and production work and now getting ready for release.  A one minute teaser on the film was released on 15 June 2013. 

==Soundtrack==
{{Infobox album
| Name        = Pizza II: Villa
| Longtype    = to Pizza II: Villa
| Type        = Soundtrack
| Cover       = Villa Audio Cover.jpg
| Caption     = Front Cover
| Artist      = Santhosh Narayanan
| Producer    = Santhosh Narayanan
| Genre       = Film soundtrack
| Label       = Think Music
| Released    = 2 September 2013 Tamil
| Last album  = Soodhu Kavvum    (2013)
| This album  = Pizza II: Villa   (2013)
| Next album  = Jigarthanda   (2014)
}}

The soundtrack album was composed by Santhosh Narayanan. The lyrics were penned by Ganeshkumar. B and Arun Raja. The album was released on 2 September 2013 in the presence of actor Prasanna (actor)|Prasanna.   Think Music purchased the audio rights for the album.

; Tracklist
{{track listing
| extra_column    = Singer(s)
| lyrics_credits  = yes
| total_length    = 16:99

| title1     = Boomiyil
| extra1     = Pradeep Kumar
| lyrics1    = Arun Raja
| length1    = 03:58

| title2     = Disco Woman
| extra2     = Dhee
| lyrics2    = Ganeshkumar
| length2    = 03:27

| title3     = Kannum Gnyanam
| extra3     = Kalyani Nair
| lyrics3    = Ganeshkumar
| length3    = 03:25

| title4     = Pa Pa
| extra4     = Gaana Bala, Heavy metal singing by Vineet Mani
| lyrics4    = Ganeshkumar
| length4    = 04:36

| title5     = The Villa (Theme Music)
| extra5     = Studio Orchestra of Sydney
| lyrics5    = Instrumental
| length5    = 01:02

| title6     = Varaipadam
| extra6     = Studio Orchestra of Sydney
| lyrics6    = Instrumental
| length6    = 01:51
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 