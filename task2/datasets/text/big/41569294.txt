Face Value (film)
{{infobox film
| title          = Face Value
| image          =
| imagesize      =
| caption        =
| director       = Robert Z. Leonard
| producer       = Bluebird Photoplays
| writer         = Mae Murray(story)  Robert Z. Leonard  Fred Myton(scenario)
| starring       = Mae Murray
| music          =
| cinematography = Alfred G. Gosden
| editing        =
| distributor    = Universal Film Manufacturing Company
| released       = January 19, 1918 reels
| country        = USA
| language       = Silent film (English intertitles)

}}
Face Value is a 1918 silent film drama starring Mae Murray and directed by Robert Z. Leonard. It was released by Universal Film and produced by their second tier production unit Bluebird.  

This film survives in a copy at the George Eastman House. 

==Cast==
*Mae Murray - Joan Darby
*Clarissa Selwynne - Mrs. Van Twiller
*Florence Carpenter - Margaret Van Twiller
*Wheeler Oakman - Bertram Van Twiller
*Casson Ferguson - Louis Maguire
*Mrs. Griffith - Mrs. Kelly

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 