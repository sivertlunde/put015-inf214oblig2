Anuraga Devatha
{{Infobox film
| name           = Anuraga Devatha
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| story          = Ram Kelkar
| screenplay     = Tatineni Rama Rao
| producer       = Nandamuri Harikrishna
| director       = Tatineni Rama Rao
| starring       = N. T. Rama Rao Sridevi Jayasudha Chakravarthy
| cinematography = Nandamuri Mohana Krishna
| editing        = Ravi
| studio         = Ramakrishna Cine Studios   
| released       =  
| runtime        = 147 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu romantic Hindi movie Aasha (1980 film)|Aasha.       

==Plot==
Ramu (N. T. Rama Rao) is a truck driver who gives a lift to a famous singer Rupa Devi (Sridevi) when her vehicle breaks down. They become friends.  He is already in love with Tulasi (Jayasudha), whom he marries.  Rupa wishes him well calling him "Nestham" (friend), even though she has fallen in love with him.  Ramu has an accident, and everyone believes that he is dead.  His grieving mother tells a pregnant Tulasi to go away. Upset with everything in her life, she jumps from a bridge into the water. She is saved by a young guy Prakash (Nandamuri Balakrishna) and his colony members finds that she lost her sight. Thulasi gives birth to a baby girl and names her as Ramatulasi, combining her and her husbands names. Prakash gives shelter to them and threats Thulasi as his on sister. Ramu turns out to be alive and he comes to know Thulasi had committed suicide. He becomes depressed. Rupa re-enters his life and helps him overcome his depression. They get engaged. They become acquainted with Ramatulasi, now a little girl, selling little Gods statutes on the street. They are completely enchanted by her, even though they dont know that she is really Ramus daughter. Rupa meets Thulasi and tells her theyll pay for her eye operation, so she can regain her sight. Rupa also invites Thulasi to her wedding with Ramu.  After she regains her sight from the operation, she goes to Rupas wedding and is shocked to see Ramu. She quickly leaves, not wanting to disrupt Rupa and Ramus lives.  Prakash tells Ramu that Thulasi is still alive and that Ramatulasi is actually his daughter.  Rupa cancels the wedding and  tells him to go and reunite with his wife and daughter, which he does. Rupa goes back on stage permanently.

==Cast==
 
*N. T. Rama Rao as Ramu
*Sridevi as Rupa Devi
*Jayasudha as Tulasi
*Nandamuri Balakrishna as Prakash Allu Ramalingaiyah  Gummadi 
*Nutan Prasad as Gopal
*Mikkilineni 
*Mukkamala  Chitti Babu
*Chidatala Appa Rao Annapurna 
*Kavitha 
*Rushyendramani 
*Dubbing Janaki 
*Baby Anuradha as Ramatulasi
 

==Soundtrack==
{{Infobox album
| Name        = Anuraga Devatha
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1981
| Recorded    = 26:47
| Genre       = Soundtrack
| Length      = 
| Label       = AVM Audio Chakravarthy
| Reviews     =
| Last album  = Kondaveeti Simham   (1981)  
| This album  = Anuraga Devatha   (1982)
| Next album  = Justice Chowdhary   (1982)
}}

Te music was composed by K. Chakravarthy with lyrics by Veturi Sundararama Murthy. The soundtrack was released by AVM Audio Company.
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Andhala Hrudayama  SP Balu
|3:52
|- 2
|Choosuko Padhilanga
|P. Susheela
|4:20
|- 3
|Nee Aata Naa Pata  SP Balu
|4:58
|- 4
|Aadave Gopika  SP Balu,P. Susheela
|6:05
|- 5
|Mugggurammala SP Balu,P. Susheela
|3:18
|- 6
|Mugggurammala Ganna
|P. Susheela
|4:14
|}
   

==Others==
* VCDs and DVDs were released by Universal Videos, SHALIMAR Video Company, Hyderabad.

==References==
 

 
 
 


 