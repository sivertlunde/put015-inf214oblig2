The Son's Room
{{ Infobox film
| name           = The Sons Room
| image          = La_stanza_del_figlio.jpg
| caption        = Original film poster
| director       = Nanni Moretti
| producer       = Angelo Barbagallo Nanni Moretti
| writer         = Nanni Moretti
| starring       = Nanni Moretti Laura Morante Jasmine Trinca Giuseppe Sanfelice Silvio Orlando Sofia Vigliar Claudio Santamaria Stefano Accorsi Simona Lisi
| music          = Nicola Piovani
| distributor    = Sacher Film
| released       = 9 March 2001
| runtime        = 99 minutes
| country        = Italy
| language       = Italian
}} Italian film directed, written, produced by and starring Nanni Moretti. It depicts the psychological effects on a family and their life after the death of their son. It was filmed in and around the city of Ancona.

==Plot==
The film tells a very raw and realistic story of the death of a teenager in a middle-class Italian family. Giovanni has a troubled relationship with his seventeen-year-old son Andrea and would like to make up with him by jogging. Andrea, annoyed by the presence of his father, accepts the invitation, but on the very first day of jogging, Giovanni has a firm commitment. The second day Giovanni has to cancel the jogging with Andrea due to an urgent call from one of his patients. Andrea goes with friends to go scuba diving instead. Due to a minor accident on the rocks, Andrea died, without being able to be reconciled with his father. Giovanni, with his wife Paola, is no longer able to console himself for the loss of his very young son and he can only give him a final farewell in the funeral home along with all the other young friends of Andrea.

==Cast==
* Nanni Moretti as Giovanni
* Laura Morante as Paola
* Jasmine Trinca as Irene
* Giuseppe Sanfelice as Andrea
* Sofia Vigliar as Arianna
* Renato Scarpa as Headmaster
* Roberto Nobile as Priest
* Paolo De Vita as Lucianos Father
* Roberto De Francesco as Record Store Clerk
* Claudio Santamaria as Dive Shop Clerk
* Antonio Petrocelli as Enrico
* Lorenzo Alessandri as Filippos Father
* Alessandro Infusini as Matteo
* Silvia Bonucci as Carla
* Marcello Bernacchini as Luciano

==Accolades==
The Sons Room was the winner of the Palme dOr at the 2001 Cannes Film Festival.    The film also appears in Empire (magazine)|Empires 2008 list of the 500 greatest movies of all time at number 480. 

Peter Bradshaw of The Guardian, included the film in its list of ten Best films of the noughties (2000-2009).   

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 


 
 