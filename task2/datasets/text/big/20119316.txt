Eoudong
{{Infobox film
| name           = Eoudong
| image          = Eoudong.jpg
| caption        = Theatrical poster for Eoudong (1985)
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Eo Udong
 | mr             = Ŏudong }}
| director       = Lee Jang-ho 
| producer       = Lee Tae-won 
| writer         = Bang Ki-hwan 
| starring       = Lee Bo-hee   Ahn Sung-ki
| music          = Lee Jong-gu 
| cinematography = Park Seung-bae 
| editing        = Hyeon Dong-chun 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Synopsis== King Seonngjong, when strict Confucianism forced women to follow the male dominant society. 

==Reception==
Eoudong was a successful film both at the box office and with the critics. It eventually sold over 500,000 tickets, and critics praised the film for its cinematography and depiction of the historical time period. 

==Cast==
* Lee Bo-hee - Uhwudong
* Ahn Sung-ki - Galmae
* Kim Myung-gon - Chungha
* Park Won-sook - Hyangji
* Shin Chaong-shik - Yun Phil-sang
* Kim Ki-ju - PArk Yun-chang
* Moon Tai-sun - Jeong Chang-son
* Kim Seong-chan
* Kim Ha-rim
* Yun Sun-hong - Seongjong of Joseon

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 