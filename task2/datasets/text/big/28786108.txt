Lady Windermere's Fan (1935 film)
{{Infobox film
| name = Lady Windermeres Fächer
| image =
| caption =
| director = Heinz Hilpert
| producer =
| writer = Oscar Wilde (play), Herbert B. Fredersdorf
| starring = Lil Dagover, Walter Rilla, Hanna Waag
| music = Walter Gronostay
| cinematography =
| editing =
| studio =
| distributor =
| released = 25 October 1935 (Germany)
| runtime = 95 min
| country = Germany
| awards =
| language = German
| budget =
| preceded_by =
| followed_by =
}}
 German comedy film directed by Heinz Hilpert and starring Lil Dagover, Walter Rilla, Hanna Waag and Aribert Wäscher. It is based on the play Lady Windermeres Fan by Oscar Wilde.

==Cast==
* Lil Dagover ... Mrs. Erlynne
* Walter Rilla ... Lord Windermere
* Hanna Waag ... Lady Windermere
* Fritz Odemar ... Lord Augustus
* Karl Günther ... Lord Darlington
* Karl Ludwig Schreiber ... Lord Arthur
* Ernst Karchow ... Cecil Graham
* Heinz Salfner ... Duke of Barwick
* Ilse Fürstenberg ... Duchess of Barwick
* Olga Limburg ... Lady Hutfield
* Paul Dahlke ... Bankier Brown
* Aribert Wäscher ... Direktor
* Paul Bildt ... Dramaturg Gray
* Grethe Weiser ... Diseuse
* Eduard Bornträger ... Inspizient

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 