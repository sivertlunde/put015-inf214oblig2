Crude Impact
{{Infobox film
| name           = Crude Impact
| image          = crudeimpact1.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Promotional poster
| director       = James Jandak Wood
| producer       = 
| writer         = James Jandak Wood
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Sharon Anderson
| editing        = Sharon Anderson Pamela Spitzer James Jandak Wood 	
| studio         = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Crude Impact is a 2006 film written and directed by James Jandak Wood. It is a documentary about the effect of fossil fuels on issues such as global warming, the environmental crisis, society and the questionable practices of oil companies.

Crude Impact was an official selection at over thirty film festivals around the world. The film had a limited theatrical release in the United States and Canada. It has been broadcast on television in several countries. Crude Impact has been translated into French, Spanish, Czech, Turkish and Finnish.

==Awards and nominations== Artivist Film Festival in Los Angeles.
*Grand Jury Prize at the 2nd Annual Cinestrat Film Festival in Finestrat, Spain.
*Social Justice Award at the 22nd Annual Santa Barbara International Film Festival.
*One of The Best of Fest films at DOCNZ New Zealand Documentary Film Festival.
*The Hands Around the World Award and the Best Cultural Issues Film award at the 4th Annual Montana CINE International Film Festival.
*The Host City Award and the Student Judges Awards at EKOFILM 2007 in the Czech Republic.
*Nominated for three awards at the Sacramento International Film Festival - Best Documentary, Best Editing, and Best Environmental Film.
*Nominated for Best World Popular Scientific Film at the 42nd International Popular Scientific and Documentary Film Festival in Olomouc, Czech Republic.
*

==Featured speakers==
*Tundi Agardy
*Guy Caruso
*Cindy Cohn
*Julian Darley
*Kenneth S. Deffeyes
*Steve Donziger
*Emeka Duruigbo
*Michael Economides
*Christopher Flavin
*Amy Goodman
*Thom Hartmann
*Richard Heinberg
*Terry Lynn Karl
*Santiago Kawarim
*Michael Klare
*Evon Peter
*Kavita Ramdas
*William Rees
*Matthew Simmons
*Bill Twist
*Lynne Twist

== External links ==
*  - official website
* 

 
 
 
 
 
 
 


 