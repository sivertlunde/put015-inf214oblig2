Vedham Pudhithu
{{Infobox film
| name           = Vedham Pudhithu
| image          = Vedam Pudhithu.jpg
| director       = Bharathiraja
| producer       = S. Rangarajan
| writer         = K. Kannan
| story          = K. Kannan
| screenplay     = Bharathiraja Amala Raja Raja Charuhasan Janagaraj
| music          = Devendran
| cinematography = B. Kannan
| editing        = P. Mohan Raj
| studio         = Janani Art Creations
| distributor    = Janani Art Creations
| released       = 27 December 1987
| country        = India Tamil
}}
 Amala is Tamil movie, directed by Bharathiraja. Charuhasan, Saritha, Raja and Nizhalgal Ravi played supporting roles in the movie. The story and dialogue was written by Kannan.

This is one of the few films that highlights some of the issues caused by the Caste system.  The films narrative was seamless and starred Sathyaraj as Balu Thevar. It contains some of Bharathirajas trademark directorial touches as well as a lot of path breaking scenes, along with Kannans powerful dialogues.

This was considered the last film MGR watched, before his death.

== Plot ==
Balu Thevar (Sathyaraj) and Saritha live in a village and belong to a land-owning warrior caste (Thevar), held lower in the Vedic caste system hierarchy than Brahmins.  Balu Thevar though, is an atheist and speaks openly against the caste system, but is nevertheless tolerated by the villagers because he is generous in helping others in need.  Their son, Raja, has just returned from the city having completed his education.  He meets Amala, who is the daughter of a Brahmin Sastri (Priest), and they fall in love.  Both of their parents discover their love and Amalas father tries to marry her off to another man in a neighboring village.  On the way, Amala fakes her suicide to escape the marriage, and hides in a house (Nizhalgal Ravis) that she happens to pass by.  In the meantime, thinking that Amala is really dead, Amalas father confronts Raja and accuses him of causing her death.  During the discussion, they slip and fall into the waterfall and both men die.

At this point, Amalas younger brother (named Sankara - a play on Adi Sankaracarya, the founder of the Monistic system of Hindu Vedic Philosophy called Advaita Vedanta), who is devoutly studying the Vedas and passing through the student phase of his Brahmin life, is left an orphan.  Being considered inauspicious, since his mother, father, and sister are all dead, no one from the Brahmin community wants to take care of him.  He thus wanders the streets begging for food.  Balu Thevar is bothered by this, and having lost his own son, he takes him home to raise him as his own son.  They give up eating meat, so as not to offend the boy.  However, since the boy has been eating in a lower caste home, he is rejected by his community from learning the Vedas.  Sarita (the wife) is enraged, and promises to educate the boy instead in an English medium school. Balu Thevar makes fun of the boy telling him that it is not important to learn Vedas and worry about caste.  At this point, the boy points out Balu Thevars hypocrisy, at his preference for using his caste name (Thevar), while at the same time professing against the caste system.  Sathyaraj sees the merit in this argument, and immediately after this abandons all his weapons (symbols of his warrior Thevar caste) by immersing them in a river, and stops referring to himself by his caste name, going only by "Balu".

Amala, not knowing of Raja and her own fathers death, tells Nizhalgal Ravi about her love, after which he promises to reunite them.  Theres a beautiful and sad song here where she imagines her happy future.  Nizhalgal Ravi comes to the village and finds out what has happened and informs Amala.  Amala then sadly returns to her village, and informs Saritha (the wife) to take care of her younger brother for the rest of his life and prepares to leave.

Meanwhile, Janakaraj, a Brahmin who had wanted to marry Amala, but was rebuked publicly by her, sees Amala return.  He riles up the villagers with news of Amalas return and states that its extremely inauspicious for the village, since her last death rites have already been performed.  He also states that it is not proper for Brahmins to live in a non-Brahmin house.  He then sets some hay on fire and tells the villages its the gods disapproval of these two crimes.  He assembles a parade of villagers towards Balu (Sathyaraj)s house with weapons, and they demand that Balu kick out Amala from his home.  Balu refuses, a fight breaks out, and in the ensuing scuffle, he is stabbed and dies.  His final request is for the villagers to live in unity, and not let caste divide them. In the poignant and controversial closing scene, Sankara the young boy, who has now lost two fathers, is seen alone in the twilight hour of the holy Sandhya ritual, removing his Brahminical sacred thread (invested only upon the upper castes) and immersing it in a nearby stream, disgusted with, and in open defiance of the hypocrisies of strict Brahminical casteist beliefs, while performing the last funeral rites of Balu Thevar as though he were his own son.

==Cast==
* Sathyaraj as Balu Thevar
* Master Dasarathi 
* Saritha Amala as Vaidehi Raja as Sankarapandi Janagaraj as Krishna Iyer
* Charuhasan as Neelakantasastrigal
* Nizhalgal Ravi
* Ilavarasu

==Music==
The film has music composed by Devendran while written by Vairamuthu. The songs, including Kannukkul Nooru Nilava, were a hit. It is a common misconception that the music was composed by veteran South Indian composer Ilaiyaraaja. 
{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Kannukkul Nooru
| extra1       = S.P.Balasubrahmanyam, K. S. Chithra
| lyrics1      = Vairamuthu
| length1      = 5.14
| title2       = Mandhiram Sonnen
| extra2       = Mano (singer) |Mano, S. Janaki
| lyrics2      = Vairamuthu
| length2      = 4.53
| title3       = Putham Puthu Olai
| extra3       = K. S. Chithra
| lyrics3      = Vairamuthu
| length3      = 4.55
| title4       = Mattu Vandi Salai
| extra4       = Malaysia Vasudevan
| lyrics4      = Vairamuthu
| length4      = 4.06
| title5       = Sandhikka Thudittaen
| extra5       = S.P.Balasubrahmanyam, S. Janaki
| lyrics5      = Vairamuthu
| length5      = 4.58
}}

==Reception== 
The movie was a super hit at the box office and went on complete 150 days on the screens making it second consecutive Victory for Sathyaraj-Bharathiraja duo.

==Awards== National Film Awards - 35th National Film Awards Best Film on Other Social Issues for P. Bharathiraja and S. Rangarajan Best Editing for P. Mohan Raj

;Filmfare Awards South - 35th Filmfare Awards South Best Tamil Film for S. Rangarajan Best Tamil Actor for Sathyaraj

==References==
 

== External links ==
* 

 
 
 

 
 
 
 
 