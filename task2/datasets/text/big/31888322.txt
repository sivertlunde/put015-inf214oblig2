A Sea Cave Near Lisbon
{{Infobox film
| name           = A Sea Cave Near Lisbon
| image          = ASeaCaveNearLisbon.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = Henry Short
| producer       = Robert W. Paul
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Henry Short
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 13 secs
| country        = United Kingdom Silent
| budget         =
}}
 1896 UK|British short  silent actuality film, directed by Henry Short, featuring a view looking out to sea through the Boca do Inferno (Hells Mouth) cave near Lisbon, with waves breaking in. The film was popular with audiences and received positive reviews.

==Synopsis==
A Sea Cave Near Lisbon consists of a single shot, looking out through the entrance of the Boca do Inferno (Hells Mouth) cave near Lisbon. Waves enter the cave, breaking on the rocks at the caves mouth. The film lasts 13 seconds.

==Production==
In 1896, film pioneer R. W. Paul sent his associate Henry Short on a film-making trip to the Iberian Peninsula, with a new lightweight portable camera he had developed.    Paul, who had earlier in the year developed a projection system known as the "Theatrograph", was at the time in commercial competition with the Lumière brothers, who themselves had demonstrated a projection system in London on the same day, 20 February. The Alhambra Theatre in Leicester Square, London, were impressed by Pauls system and offered him a contract to supply equipment and staff. Paul was thus keen to acquire footage to make a positive impact on audiences at the Alhambra. Soto Vázquez, "Lo que de real tiene el mar", p. 206. 

During the five-week trip in August and September, Short created 18 actuality films, mostly in the cities of Cádiz, Lisbon, Madrid and Seville. Most of these were either urban views, indcluding the Puerto del Sol in Madrid and Triana, Seville, or cultural scenes, such as an Andalusian dance and Fado performers. Soto Vázquez, "Lo que de real tiene el mar", p. 209. 

Films documenting waves had become popular with audiences, as exemplified by the April 1896 film Rough Sea at Dover, and many others were produced in the years up to 1912.    A Sea Cave Near Lisbon was, however, the first cinematic depiction of a cave.     Short travelled to the Boca do Inferno for filming on 13 September.  It was filmed using a camera mounted on a boat inside the cave. 

==Release and reception==
The film was shown for the first time at the Alhambra Theatre on 22 October 1896,  as the thirteenth part of a fourteen-part programme of Shorts films, entitled "A Tour in Spain and Portugal".     Paul included it in his film catalogue for wider exhibition, where it was described as "a very striking and artistic photograph of a large cave near the Atlantic coast, into which waves dash with great violence". 

The film was immediately popular with audiences and received very positive reviews. A reviewer in The Era described it as "one of the most beautiful realisations of the sea that we have ever witnessed...the grandeur of the scenes are remarkable".  The Daily Telegraph described it as "a picture of real beauty".    In the Morning Post, a reviewer described it as "one of the most remarkable effects produced by any of the graphies yet put forward".  The film was the most popular of the 14 films, and one of the most successful films in early British cinema.        Its popularity continued in subsequent years, and it still appeared in Pauls sales catalogue in 1903, with the statement: "This film has never been equalled as a portrayal of fine wave effects". 

==Legacy==
Film historian Michael Brooke has described the film as "a very impressive achievement", and "one of the first instances in early cinema of a creative approach towards framing a shot".    

He has also pointed to the importance of A Sea Cave Near Lisbon and the other films shown in A Tour of Spain and Portugal in terms of its contribution to the history of the British documentary movement, of which he describes R. W. Paul as "was one of its most important precursors." Where previously, factual films generally consisted of stand-alone actualities intended for individual show, the programme put together by Paul from Henry Shorts sequences was his first attempt to gather short actualities into a longer collective work. Paul sent Short on a similar trip to Egypt in 1897. From this time Paul continued to experiment with multi-shot actualities, leading to longer works such as Army Life (1900) and Whaling Afloat and Ashore (1908). 

Unlike most of the other films from Shorts trip, A Sea Cave Near Lisbon has survived in its entirety, and has been made available on the British Film Institute DVD collection RW Paul: The Collected Films 1895-1908, with music by silent film accompanist Stephen Horne.

==References==
;Notes
 

;References
 

;Bibliography
* 

==External links==
* 

 
 
 
 