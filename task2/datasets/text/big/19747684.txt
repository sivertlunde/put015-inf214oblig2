Time Out for Lessons
{{Infobox film
| name           = Time Out for Lessons
| image          = Timeoutlesson TITLE.JPEG
| image_size     =
| caption        = Edward Cahn Bud Murray
| producer       = Jack Chertok for MGM
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Robert Planck
| editing        = Ralph E. Goldstien MGM
| released       =  
| runtime        = 10 49"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 185th Our Gang short (186th episode, 97th talking short, 98th talking episode, and 17th MGM produced episode) that was released.

==Plot==
Reprimanded by his father for his poor grades, Alfalfa is told that, unless he improves his academic standing, hell never get to college. Alfalfa responds, "Dont D stand for Dandy?" He then informs his father that he intends to sail through college on a football scholarship. Alfalfa dreams that he is a student at "Hale University" (a spoof of Yale University) and that he is a big football star with poor grades. During his dream about future gridiron triumphs, Alfalfa is brought down to earth when he envisions himself being disqualified from the inevitable "big game" due to his lousy grades. Awakening from this nightmare, our hero vows to put football on the back burner in favor of cracking the books.   

==Notes==
*This is one of many MGM Our Gang episodes about teaching a lesson to children. The series continues to head downhill with one morality lesson after another. Soon, World War II propaganda will be a subject of many Our Gang episodes.
*This is the last appearance of Sidney Kibrick. He was initially a background character while his older brother Leonard was the gang bully. After his brother leaves, Sidney is teamed up with a new bully, Butch, played by Tommy Bond. Sidney becomes known as "Woim". His last episode as Butchs sidekick had been Captain Spankys Showboat. The Wizard of Oz.

==Cast==
===The Gang===
* Carl Switzer as Alfalfa Mickey Gubitosi as Mickey
* Darla Hood as Darla
* George McFarland as Spanky
* Billie Thomas as Buckwheat
* Shirley Coates as Muggsy
* Darwood Kaye as Waldo
* Leonard Landy as Leonard

===Additional cast===
* Gloria Brown as Spankys dance partners
* Hugh Chapman as Kid encouraging Leonard
* Paul Hilton as Roommate
* Dickie Humphries as Kid encouraging Buckwheat
* Valerie Lee as Mickeys dance partner
* Si Wills as Alfalfas father

===College extras===
Joe "Corky" Geil, James Gubitosi, Jovanni Gubitosi, Janice Hood, Jackie Horner, Payne Johnson, Larry Kert, Sidney Kibrick, Rae-Nell Laskey, Gerald Mackey, Tommy McFarland, Glenn Mickens, Priscilla Montgomery, Betty Ann Muir, Jo-Jo La Savio, Harold Switzer

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 