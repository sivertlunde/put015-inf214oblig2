The Colony (2013 film)
 
{{Infobox film
| name           = The Colony
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Jeff Renfroe Pierre Even Marie-Claude Poulin
| writer         = Jeff Renfroe Svet Rouskov Patrick Tarr Pascal Trottier
| starring       = Laurence Fishburne Bill Paxton Kevin Zegers
| music          = Jeff Danna
| cinematography = 
| editing        = 
| studio         = RLJ Entertainment
| distributor    = Entertainment One Image Entertainment
| released       =  
| runtime        = 93 minutes 
| country        = Canada
| language       = English
| budget         = $16 million ( )    
| gross          = 
}} 

The Colony is a 2013 Canadian science fiction horror film directed by Jeff Renfroe. It had a limited release on 26 April 2013 in Canada, and was released on 20 September 2013 in the United States.

==Plot==
By 2045, humans have built weather machines to control the warming climate due to climate change and global warming. The machines break down when one day it begins to snow and doesnt stop. Whatever humans remain live in underground bunkers to escape the extreme cold. Their challenges are controlling disease and producing sufficient food. Two soldiers, Briggs (Laurence Fishburne) and Mason (Bill Paxton) are the leaders of one such bunker, Colony 7. Briggs, Sam (Kevin Zegers) and Graydon (Atticus Dean Mitchell) travel to nearby Colony 5 after receiving a distress signal.

Upon arrival, they find Colony 5 covered in blood. They eventually reach a locked door which Sam picks open. Inside they find Leyland, who shows them a message they received from another group of people who fixed a weather machine and have caused the snow to thaw. The group offers aid to anyone and asks that they bring seeds so they can be planted in the newly thawed permafrost soil. Leyland shows them where the signal came from but informs them that a Colony 5 expedition failed to return. Moreover, the expeditions tracks led a marauding group of cannibals back to Colony 5 and the killing ensued. Briggs, Sam, and Graydon try to force Leyland to return with them but he pushes them out and locks himself back into his room.

The three then begin to explore Colony 5 and approach a room with a fire burning.  Here they see a human chopping up members of Colony 5, while others feast on human remains. As the three try to escape, Graydon is killed by the cannibals. Briggs and Sam are able to make it up the ladder out of the colony and destroy the shaft with a stick of dynamite. Taking shelter in an abandoned helicopter, they wake up in the morning to find that the cannibals have managed to escape the entombed Colony 5 and have tracked their footprints in the snow. Briggs suggests that they lose the group over a decrepit bridge using dynamite. Due to the cold weather with high winds though, the dynamite fuse goes out. Briggs is able to make it back to light the fuse but not before he is confronted by the cannibals. He lights the fuse and sacrifices himself while blowing up the bridge to enable Sam to return to Colony 7.

Upon Sams return, he finds Mason has assumed control of the Colony and has plans to enforce harsh changes. Sam explains that the cannibals are en route and shares the message from Colony 5 that there is a thawed-out area they should migrate to. Mason disregards Sams comments as shock from his voyage and handcuffs him to a bed. Sam is able to get free and checks images from a functioning satellite to locate the thawed-out zone. He is confronted by Mason just as the cannibals have reached their Colony. Many of the Colony 7 inhabitants are killed and Mason is left behind while the others escape. After killing the cannibals leader, Sam and a few others survive but the Colony is destroyed. The survivors gather seeds and begin their journey to the thawed-out location.

==Cast==
* Laurence Fishburne as Briggs
* Kevin Zegers as Sam
* Bill Paxton as Mason
* Charlotte Sullivan as Kai
* John Tench as Viktor
* Atticus Dean Mitchell as Graydon
* Dru Viergever as Feral Leader
* Romano Orzari as Reynolds
* Michael Mando as Cooper
* Earl Pastko as the Scientist
* Julian Richings as Leyland

==Production==
The film was shot at CFB North Bay using former NORAD facilities      and at the Hearn Generating Station|R. L. Hearn Generating Station in Toronto.

==Reception==
Rotten Tomatoes gives the film a score of 16% based on reviews from 31 critics, and a rating average of 3.9 out of 10. The consensus states "A formulaic sci-fi thriller, The Colony features cliched dialogue, cheesy special effects, and underdeveloped characters."  Critics considered the film to consist of old science fiction ideas. Peter Howell of the Toronto Star noted it could "show investors how well Canadians can mimic Hollywood blockbusters, decent CGI and all, using a fraction of the budget.".      Jay Stone of Postmedia deemed the film had "too many clichés ... and half-developed characters to make us care enough."  The Globe and Mails Liam Lacey also panned the recycled nature of the plot, although noting the scenes appeared to be "authentically chilly throughout". 

==Home media==
The Colony was released on DVD and Blu-ray Disc|Blu-ray on 27 August 2013 in Canada and on 15 October 2013 in the United States.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 