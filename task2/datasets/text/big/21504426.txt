What Have You Done to Solange?
 
{{Infobox film
| name           = What Have You Done to Solange?
| image          =What Have You Done to Solange?.jpg
| caption        =
| director       = Massimo Dallamano
| producer       = Leo Pescarolo
| writer         = Bruno Di Geronimo, Massimo Dallamano
| starring       = Karin Baal Joachim Fuchsberger Cristina Galbó Camille Keaton Fabio Testi
| music          = Ennio Morricone
| cinematography = Joe DAmato
| editing        = Antonio Siciliano
| distributor    =
| released       = 1972
| runtime        = 105 min.
| country        = Italy West Germany
| language       = Italian English dub
| budget         =
| preceded_by    =
| followed_by    =
}}
 The Clue of the New Pin.  It was shot on location in London, England over the course of six weeks in the autumn of 1971. The films Italian title is Cosa avete fatto a Solange? In recent years, the film has gained a reputation as one of the best giallo films of the early 1970s among fans of the genre.

==Plot==
 
Italian teacher Enrico Rosseni is having an affair with one of his lovely young students. While on an afternoon outing the two lovers learn that a fellow student is brutally murdered mere yards from them. Now other female students are being targeted by a killer with a sinister vendetta and Rosseni is looking like a suspect.

==Cast==

*Fabio Testi	 ... 	Enrico Henry Rosseni
*Cristina Galbó	... 	Elizabeth Seccles (as Christine Galbo) 
*Karin Baal	... 	Herta Rosseni
*Joachim Fuchsberger	... 	Inspector Barth
*Günther Stoll	... 	Professor Bascombe
*Claudia Butenuth	... 	Brenda Pilchard
*Camille Keaton	... 	Solange Beauregard
*Maria Monti	... 	Mrs. Erickson
*Giancarlo Badessi	... 	Mr. Erickson
*Pilar Castel	... 	Janet Bryant
*Giovanna Di Bernardo	... 	Helen Edmonds
*Vittorio Fanfoni	... 	Enricos friend
*Antonio Casale	... 	Mr. Newton (as Antony Vernon)
*Emilia Wolkowicz	... 	Ruth Holden (as Emilia Wolkowich)
*Daniele Micheletti	... 	Mr. Bryant
*Rainer Penkert	... 	Mr. Leach, the headmaster 
*Carla Mancini	... 	Susan, girl in Enricos class
*Antonio Anelli     ...     Father Herbert
*Joe DAmato        ...     CID officer w / The Daily Telegraph (uncredited)
* Uncredited actress    ...     Hilda Erickson

==Production==
 

==Alternate Versions==
* The "uncut" DVD has some scenes in the still and artwork gallery that are not shown in that 2002 video release. These include: more nude shots of Elizabeths body (Cristina Galbó); a scene of a topless Solange (Camille Keaton) being visited by the unidentified killer which is very crucial to the plot; the shower scenes are cropped so that the schoolgirls are only shown topless. This does not necessarily mean that those scenes were actually included in the original theatrical release print, so the DVD could be "uncut".

* The UK cinema certificate was rejected by the BBFC. It was eventually released on the Redemption video label in 1996 after 2 minutes 15 secs of cuts to edit the bath murder, and heavily reduce shots of nudity and knives between victims legs & knees.

==Alternate Titles==
*Das Geheimnis der grünen Stecknadel (Germany)
*Solange (UK)
*Terror in the Woods (USA)
*The School That Couldnt Scream (USA)
*The Secret of the Green Pins (USA)
*What Have They Done to Solange? (USA)
*What Have You Done to Solange? (UK)
*Who Killed Solange?
*Whos Next? (UK)
*¿Qué habéis hecho con Solange? (Spain)
*Que Fizeram a Solange? (Portugal)
*O Que Fizeram a Solange? (Brazil)
*¿Qué hicieron con Solange? (Argentina)
*¿Qué le han hecho a Solange? (Peru, Venezuela, Colombia)

==Reception==
 Allmovie called the film a "first-rate thriller," a "creepy mystery, and noted "Massimo Dallamanos direction is assured."  

==External links==
*  

==References==
 


  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 