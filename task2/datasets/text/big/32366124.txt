Motoring (film)
{{Infobox film
| name           = Motoring
| image          =
| caption        =
| director       = George Dewhurst
| producer       = 
| writer         =  Harry Tate (sketch)   Eliot Stannard Henry Latimer   Roy Travers
| music          =
| cinematography = 
| editing        = 
| studio         = Inter-Cine
| distributor    = Inter-Cine
| released       = 1927 
| runtime        = 5,506 feet  
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent comedy Henry Latimer and Roy Travers.  A passing motorist helps a woman to elope with her lover. It was based on one of Tates own music hall sketches. 

==Cast==
* Harry Tate - Harry Henry Latimer - Basil Love
* Roy Travers - Sir Stone Flint
* Ronald Tate - The Boy

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 