Father Goose (film)
{{Infobox film
| name           = Father Goose
| image          = Father Goose film poster.jpg
| director       = Ralph Nelson Robert Arthur
| based on       =  
| writer         = Peter Stone Frank Tarloff
| starring       = Cary Grant Leslie Caron
| music          = Cy Coleman
| cinematography = Charles Lang
| editing        = Ted J. Kent
| studio         = Granox Productions
| distributor    = Universal Pictures
| released       = December 10, 1964
| runtime        = 118 minutes
| country        = United States
| language       = English
| gross          = $12,500,000   The Numbers. Retrieved April 30, 2013. 
}}

Father Goose is a 1964 romantic comedy film set in World War II, starring Cary Grant, Leslie Caron and Trevor Howard. HOLLYWOOD FATHER GOOSE SAGA: CARY GRANT FINDS ROLE OF DERELICT IS FUN AND CHALLENGING Academy Award for its screenplay. The film introduced the song "Pass Me By" by Cy Coleman and Carolyn Leigh, later recorded by Peggy Lee, Frank Sinatra and others.

==Plot== Japanese invasion, Commander Frank beachcomber Walter Japanese planes. Houghton rewards Ecklands sightings with directions to one of the whisky bottles hidden around the island, once coast-watchers on other islands confirm it. To ensure Eckland remains on duty Houghtons naval vessel "accidentally" hits his boat, leaving a large hole in its hull.

Houghton finds a replacement watcher, but Eckland has to retrieve him from nearby Bundy Island. He unexpectedly finds Frenchwoman Catherine Freneau (Leslie Caron) and seven young schoolgirls under her care stranded there. She informs him that the man he came for was killed in an air raid, and Eckland reluctantly takes them back to Matalava with him.  
 Goody Two Shoes" and "a rude, foul-mouthed, drunken, filthy beast". He adjusts to her and her girls, however, and cares for her through what they mistakenly believe is a deadly snakebite. Freneau learns that Eckland had been a history professor before he fled civilization to the South Pacific.
 American submarine to pick them up, but an enemy patrol boat shows up at the same time. Eckland takes his boat out to lure the Japanese vessel out beyond a reef so the submarine can torpedo it. His boat is sunk, but Eckland survives and the submarine safely evacuates everyone.

==Cast==
(Codenames for each cast member are shown in parentheses)
* Cary Grant as Walter Christopher Eckland (Mother Goose/Filthy Beast)
* Leslie Caron as Catherine Louise Marie Ernestine Freneau (Goody Two Shoes)
* Trevor Howard as Commander Frank Houghton (Big Bad Wolf)
* Jack Good as Lieutenant Stebbings (Bo Peep)

The children:
* Sharyl Locke as Jenny
* Pip Sparke as Anne
* Verina Greenlaw as Christine
* Stephanie Berrington as Elizabeth Anderson
* Jennifer Berrington as Harriet "Harry" MacGregor
* Laurelle Felsette as Angelique
* Nicole Felsette as Dominique

==Production==
Father Goose was filmed on location in Jamaica. The Japanese patrol vessel at the end of the film was portrayed by a former U.S. Coast Guard WPB 83-foot wooden patrol boat.

==Reception== theatrical rentals, 7th highest grossing film of 1964.

Time Out Film Guide panned the film, complaining, "Its a shame that Grant ... should have logged this sentimental claptrap as his penultimate film" and "Grant frequently looks as if he really didnt want to be there, wading lost in a sludge of turgid drama and pallid comedy."  Film4 agreed, stating "the story all too slowly descends into sentimental sludge." 

In its contemporary review, Variety (magazine)|Variety found more to like: "Cary Grant comes up with an about-face change of character....   plays an unshaven bum addicted to tippling and tattered attire, a long way from the suave figure he usually projects but affording him opportunity for nutty characterization. Leslie Caron and Trevor Howard are valuable assists to plottage...." 

==Awards and nominations== Oscar for Best Writing, Story and Screenplay, which was written directly for the screen by S. H. Barnett, Peter Stone, and Frank Tarloff, and was also nominated for Best Film Editing (Ted J. Kent) and Best Sound (Waldon O. Watson).    It received a nomination for the 1965 Golden Globe Best Motion Picture - Musical/Comedy award.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 