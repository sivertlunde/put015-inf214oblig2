The Phantom of the North
 
{{Infobox film
| name           = The Phantom of the North
| image          =
| caption        =
| director       = Harry S. Webb
| producer       =
| writer         = F. E. Douglas George C. Hull Carl Krusada
| starring       = Edith Roberts Donald Keith
| music          =
| cinematography = Arthur Reeves William Thornley
| editing        = Frederick Bain
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}

The Phantom of the North is a 1929 American drama film directed by Harry S. Webb for the independent Biltmore Productions and featuring Boris Karloff.    The film is considered to be lost film|lost.   

==Cast==
* Edith Roberts - Doris Rayburn
* Donald Keith - Bob Donald
* Kathleen Key - Colette
* Boris Karloff - Jules Gregg Joe Bonomo - Pierre Blanc
* Josef Swickard - Colonel Rayburn
* Muro the Dog - Himself
* Arab the Horse - Himself

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 