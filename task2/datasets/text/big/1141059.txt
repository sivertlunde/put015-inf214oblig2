The Clearing
 
  
{{Infobox film
| name = The Clearing
| image = Clearing movie poster.jpg
| director = Pieter Jan Brugge
| producer = Pieter Jan Brugge Jonah Smith
| writer = Justin Haythe
| starring = Robert Redford Helen Mirren Willem Dafoe
| studio = Thousand Words
| distributor = Fox Searchlight Pictures
| music =
| cinematography = Denis Lenoir
| editing =
| released =  
| runtime = 95 minutes
| language =
| budget =
}}

The Clearing is a 2004 American drama film and the directorial debut of Pieter Jan Brugge, who has worked as a film producer. The film is loosely based on the real-life kidnapping of Gerrit Jan Heijn that took place in the Netherlands in 1987. The screenplay was written by Justin Haythe.

==Plot==
Wayne Hayes (Robert Redford), and his wife Eileen (Helen Mirren) are living the American dream in a wealthy Pittsburgh suburb, having raised two children (Alessandro Nivola, Melissa Sagemiller) and built up a successful business from scratch. He is looking forward to a peaceful retirement with Eileen. However, everything changes when Wayne is kidnapped in broad daylight by a former employee, Arnold Mack (Willem Dafoe). While Wayne tries negotiating with the kidnapper, Eileen works with the FBI to try to secure her husbands release. During the investigation, Eileen learns that Wayne has continued an extramarital affair that he promised to end months previously.

Eileen is eventually instructed to deliver the ransom to the kidnapper, but Arnold takes the money without returning her husband. As it turns out, Wayne had been murdered by Arnold the day of the kidnapping. Although Eileens ordeal takes place over the course of a week, the film is edited to show Waynes kidnapping as if it was happening at the same time.

Arnold is eventually caught when he begins to spend the ransom money in the neighborhood where he lives. At a local grocery store, he uses a $100 bill to make a purchase. The store manager calls authorities and verifies the serial number on the $100 bill is on a watch list the FBI distributed to local businesses. In the end, Eileen receives a loving note written by Wayne before his death.

==Cast==
*Robert Redford – Wayne Hayes
*Helen Mirren – Eileen Hayes
*Willem Dafoe – Arnold Mack
*Alessandro Nivola – Tim Hayes
*Matt Craven – Agent Ray Fuller
*Melissa Sagemiller – Jill Hayes
*Wendy Crewson – Louise Miller
*Larry Pine – Tom Finch
*Diana Scarwid – Eva Finch

==Locations==
The film was shot in and around Asheville, North Carolina and Downtown Pittsburgh, PA. 

== Reception == Rolling Stone "The pleasures of this endeavor, directed with a keen eye for detail by Pieter Jan Brugge, come from what the actors bring to the material." 

Ty Burr in the Boston Globe felt that the film had a "lack of emotion" and "could have been more than it is".   M. Torreiro, in the Spanish newspaper El País, described the film a "tense thriller, cramped and made of downtime and sensations on the limit, a strange film." 

== References ==
 

==External links==
* 
* 

 
 
 
 
 
 
 