Love Lifting
{{Infobox film
| name           = Love Lifting
| image          = LoveLifting.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 高舉‧愛
| simplified     = 高举‧爱
| pinyin         = Gāo Jǔ‧Ài
| jyutping       = Gou1 Geoi2‧Ngoi3 }}
| director       = Herman Yau
| producer       = Alvin Lam Zhang Zhao Ng King-hung Ko Hiu-kong Tung Pui-man
| writer         = 
| screenplay     = Herman Yau Yeung Yee-shan Wang Yawen
| story          = 
| based on       = 
| starring       = Chapman To Elanne Kong
| narrator       = 
| music          = Brother Hung
| cinematography = Joe Chan
| editing        = Azrael Chung
| studio         = Universe Entertainment Hong Kong Film Development Fund Le Vision Pictures Shanxi Film Studio Guangzhou Yingming Culture Media Grant Talent Limited Local Production
| distributor    = Universe Films Distribution   Gala Film Distribution  
| released       =  
| runtime        = 92 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = US$165,589
}}

Love Lifting is a 2012 Hong Kong romantic drama film written and directed by Herman Yau and starring Chapman To and Elanne Kong.

==Plot== triad leader. While feeling frustrated and lonely, Yung meets his neighbor Lee Lai (Elanne Kong), a former weightlifter who was forced to retire due to diabetes. Later, the two of them marry and have son. However, Lai cannot let go of her beloved weightlifting career and Yung helps and encourages his wife to repelt back into weight training and fulfil her uncompleted dream. In this way, Yung serves the role as a house husband to support his wife to return to the weightlifting arena.

==Cast==
*Chapman To as Shek Yung
*Elanne Kong as Lee Lai
*Tien Niu as Pretty Hung
*Jeremy Xu as Kin
*Feng Haoxu as Shek Lui
*Zhang Songwen as Coach Chan Chiu
*Huang Jianxin as Coach Qiu
*Jun Kung as Brother Ding
*Bob Lam as Furniture mover
*Terence Siufay as Furniture mover
*Vincent Chui as Doctor
*Joe Cheung as Yungs father
*Lee Fung as Yungs mother
*Tam Kon-chung as Condos security guard

==Theme song==
*You Give Me Strength (你給我力量)
**Composer: Alan Cheung
**Lyricist: Lee Man
**Singer: Elanne Kong

==Reception==
===Critical===
James Mrash of Twitch Film gave the film a negative review and writes "While Love Lifting has plenty of potential, as a sweet natured romance, domestic drama, or competitive sports movie, the script never feels committed to developing its story in any particular direction. A number of subplots are introduced - Yungs financial struggles and money-hungry ex-wife (whom we never see), a rivalry between Li Lis former and current coaches, Li Lis diabetes itself - only to be forgotten about or easily resolved without any drama or difficulty. Instead, the story simply progresses from A to B to C without any sense of urgency, peril, anticipation or excitement. {{cite web|url=http://twitchfilm.com/2012/03/review-love-lifting.html|title=
Review: LOVE LIFTING lacks both weight and strength}}    gave the film a relatively positive review and writes "Seemingly outrageous premise is played straight to positive, low-key effect. Director Herman Yau never oversells Love Lifting, making it enjoyable and probably a little too light. A likable if inessential film. Elanne Kong is surprisingly effective in the lead role." 

===Box office===
The film grossed US$165,589 at the Hong Kong box office. 

==Awards and nominations==
*32nd Hong Kong Film Awards Best Actress (Elanne Kong)

*19th Hong Kong Film Critics Society Award
**Won: Film of Merit

==References==
 

==Exxternal links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 