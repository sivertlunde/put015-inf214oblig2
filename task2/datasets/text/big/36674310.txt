The Mystery of Marie Roget (film)
{{Infobox film name            = The Mystery of Marie Roget image           =  caption        = Theatrical Poster director        = Phil Rosen producer        = writer          = based on novel by Edgar Allan Poe starring        = Maria Montez Patric Knowles Maria Ouspenskaya music           = cinematography  = editing         = studio          = Universal Pictures distributor     =  released        =   runtime         = 60 minutes country         = United States language        = English budget          =
}}
The Mystery of Marie Roget (1942) is a gothic mystery film starring Patric Knowles. The story was adapted from the short story The Mystery of Marie Rogêt written by Edgar Allan Poe in 1842. The film, directed by Phil Rosen and produced by Universal Pictures, is set in 1889.

Maria Montez appears in a supporting role. When the film was reissued in 1943, after she obtained stardom as The Phantom of Paris, she was given prominent billing. 

It is a sequel to Murders in the Rue Morgue (1932 film). The sequel is set 44 years later. Pierre Dupin is now Paul Dupin. Their are several references to the first film, and some scenes are set in the Rue Morgue.

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 

 
 