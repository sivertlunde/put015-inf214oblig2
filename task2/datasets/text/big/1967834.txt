Stop! Or My Mom Will Shoot
{{Infobox film
| name           = Stop! Or My Mom Will Shoot
| image          = Momshootposter.jpg
| caption        = Theatrical release poster
| director       = Roger Spottiswoode
| producer       = Ivan Reitman Joe Medjuck Michael C. Gross William Osborne William Davies
| starring       = Sylvester Stallone Estelle Getty JoBeth Williams Roger Rees
| music          = Alan Silvestri
| cinematography = Frank Tidy
| editing        = Mark Conte Lois Freeman-Fox
| studio         = Northern Lights Entertainment Universal Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $45 million
| gross          = $70,611,210
}} buddy cop comedy film directed by Roger Spottiswoode. The film stars Sylvester Stallone and Estelle Getty. The film was released in the United States on February 21, 1992.

==Plot==
 
A tough cops (Sylvester Stallone) seemingly frail mother (Estelle Getty) comes to stay with him and progressively interferes in his life. She buys him an illegal MAC-10 machine pistol and starts poking around in his police cases.

==Cast==
*Sylvester Stallone as Sgt. Joseph Andrew Joe Bomowski
*Estelle Getty as Mrs. Tutti Bomowski
*JoBeth Williams as Lt. Gwen Harper John Wesley as Sgt. Tony
*Ving Rhames as Mr. Stereo
*Richard Schiff as Gun Clerk
*Al Fann as Sgt. Lou
*Roger Rees as J. Parnell
*Martin Ferrero as Paulie
*Gailard Sartain as Munroe
*Dennis Burkley as Mitchell

==Reception==
The film received extremely negative reviews and retains a 4% rating on  " but stated that "the concept is actually better for Stallone than the premises of his earlier awful romps, Rhinestone and Oscar."  Clifford Terry wrote in the Chicago Tribune that the film "plays like an extended sitcom-perhaps four episodes of Shes the Sheriff" and also that "About two-thirds into Stop! Or My Mom Will Shoot, Sylvester Stallone actually delivers the title line. Thats how numbingly awful this is. Give it half a star for being in focus."  The film found more success on VHS and DVD. 

Both Gene Siskel and Roger Ebert aggressively disliked the film and both gave it a thumbs down in their onscreen review of the film, with Ebert claiming it to have been "one of the worst movies hed ever seen"; in his newspaper review, Ebert labeled it as "one of those movies so dimwitted, so utterly lacking in even the smallest morsel of redeeming value, that you stare at the screen in stunned disbelief. It is moronic beyond comprehension, an exercise in desperation during which even Sylvester Stallone, a repository of self-confidence, seems to be disheartened."  Siskel stated that "if the script of this picture were submitted to The Golden Girls television show staff it would be summarily dismissed as too flimsy for a half-hour sitcom. There is not one laugh nor surprising moment to be found, starting with the scene where Stallone and Getty happen upon a jumper atop a building and Getty manages to bring the man down safely using a bullhorn." 

It was also the winner of three Golden Raspberry Awards for Stallone as Worst Actor, Getty as Worst Supporting Actress and the film earning Worst Screenplay. 

===Sylvester Stallones reaction===
Sylvester Stallone claimed it was the worst film he ever did.  In an interview with Aint It Cool News, Stallone referred to it as "maybe one of the worst films in the entire solar system, including alien productions we’ve never seen", that "a flatworm could write a better script", and "in some countries – China, I believe – running   once a week on government television has lowered the birth rate to zero. If they ran it twice a week, I believe in twenty years China would be extinct." 

===Box office===
 
Despite the poor reviews, the film was somewhat successful at the box office. The film brought in only $28.4 million domestically but did a little better overseas with over $42.2 million internationally to a total of $70.6 million worldwide.

==In popular culture==
 
The film was mentioned when Stallone hosted an episode of Saturday Night Live in 1997; in one particular skit Stallone comes across someone in a terrible car accident (Norm Macdonald) who does not like any of his work and ridicules his films. As he lies dying, he mutters something quietly that only Stallone can hear, and when a passerby (Will Ferrell) asks what he said, Stallone is reluctant to say it until he is grilled some more, in which he virulently yells "He said Stop! Or My Mom Will Shoot...SUCKED!"

The title of The Simpsons episode "Stop or My Dog Will Shoot" is a reference to the film.

== References ==
 
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 