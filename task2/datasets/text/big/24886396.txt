Nora's Will
{{Infobox film
| name           = Noras Will
| image          = Cinco días sin Nora.jpg
| caption        = Film poster
| director       = Mariana Chenillo
| producer       = 
| writer         = Mariana Chenillo
| starring       = Fernando Luján
| music          = 
| cinematography = 
| editor         = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
Noras Will ( , also released as Five Days Without Nora) is a 2008 Mexican drama film written and directed by Mariana Chenillo. It was entered into the 31st Moscow International Film Festival.   

==Plot==

Nora commits suicide in a timely way consistent with her plan to bring her ex-husband, Jose (Luján), and the rest of their family together for a Passover together. 

A photograph from the past, hidden under the bed, leads Jose to reexamine their relationship. 

==Cast==
*Enrique Arreola as Moisés
*Ari Brickman as Rubén
*Juan Carlos Colombo as Dr. Nurko
*Marina de Tavira as Young Nora
*Max Kerlow as Rabbi Jacowitz
*Verónica Langer as Aunt Leah
*Martin LaSalle as Rabbi Kolatch
*Fernando Luján as José
*Silvia Mariscal as Nora
*Fermín Martínez as Doorman
*Juan Pablo Medina as Young José
*Arantza Moreno as Paola
*Vanya Moreno as Laura
*Angelina Peláez as Fabiana
*Cecilia Suárez as Bárbara
*Daniela Tarazona as Sales woman

==Reception==

Review aggregator Rotten Tomatoes gives the film a score of 88% based on reviews from 33 critics, with a "Certified Fresh" rating.  

==Awards==

Biarritz Festival Latin America, 
*Best Film 
Expresión en Corto International Film Festival, 
*Best First Film 
Havana Film Festival, 
*Grand Coral - Third Prize 
Huelva Latin American Film Festival, 
*Best Actor (Fernando Luján)  
Los Angeles Latino International Film Festival, 
*Jury Award for Best Director and Best First film 
Mar del Plata International Film Festival, 
*Best Film 
Miami International Film Festival, 
*Audience Award 
Morelia International Film Festival, 
*Audience Award 
Moscow International Film Festival, 
*Silver St. George (Best Director)   
Skip City International D-Cinema Festival, 
*Best Screenplay 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 