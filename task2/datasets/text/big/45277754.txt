Children of Jazz
{{Infobox film
| name           = Children of Jazz
| image          = 
| alt            = 
| caption        = 
| director       = Jerome Storm
| producer       = Jesse L. Lasky
| screenplay     = Harold Brighouse Beulah Marie Dix
| starring       = Theodore Kosloff Ricardo Cortez Robert Cain Eileen Percy Irene Dalton Alec B. Francis
| music          = 
| cinematography = Devereaux Jennings
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Jerome Storm and written by Harold Brighouse and Beulah Marie Dix. The film stars Theodore Kosloff, Ricardo Cortez, Robert Cain, Eileen Percy, Irene Dalton and Alec B. Francis. The film was released on July 8, 1923, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Theodore Kosloff as Richard Forestall
*Ricardo Cortez as Ted Carter
*Robert Cain as Clyde Dunbar
*Eileen Percy as Babs Weston
*Irene Dalton as Lina Dunbar
*Alec B. Francis as John Weston
*Frank Currier as Adam Forestall
*Snitz Edwards as Blivens
*Lillian Drew as Deborah

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 