Thirakkatha
{{Infobox Film
| name           = Thirakkatha
| image          = Thirakadha-srividya-priyamani.jpg
| image_size     = 
| caption        = Official poster feat. Srividya to whom the film is dedicated and Priyamani as Malavika. Ranjith 
| producer       = Ranjith Maha Subair
| writer         = Ranjith 
| screenplay     = Ranjith
| story          = Ranjith
| narrator       =  Prithviraj Samvrutha Ranjith Mallika Sukumaran Cochin Haneefa
| music          = Sharreth
| cinematography = M. J. Radhakrishnan
| editing        = Vijay Shankar 
| distributor    = Varnachithra Big Screen
| released       =  
| runtime        = 120 minutes 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} 2008 Malayalam Malayalam romantic drama film co-produced, written, and directed by Ranjith (director)|Ranjith. 

The film is a tribute to the yesteryear actresses who had been graceful, popular figures in cinema during their younger age with patronage and later got completely disregarded by the industry, media and the masses alike. Ranjith has drawn inspiration from the life of many actresses but especially from the romantic link-up of actress Srividya with actor Kamal Haasan. He has adopted a special narrative style for the film. The story shifts between the present and flash back through the perspective of different characters. The story is divided into three monologues which meet in the climax. The film has garnered positive reviews from the critics and audiences alike.
 National Film Best Feature Best Actress.

==Plot summary==

 
 Hollywood film. For Akbar cinema is not a job, it is his passion. Akbar decides to choose a very different kind of story for his second film and he sets on a journey in search of it.

Akbar decides to base his second film on the life of yesteryear actress Malavika (Priyamani), who was once a very popular actress and whose present whereabouts are unknown. Akbar and his friends go tracking Malavika’s biography. Her husband was Ajaya Chandran (Anoop Menon). Currently Ajayachandran is the reigning super star of the industry. Akbar starts his search from film director Aby Kuruvila’s (Ranjith) house. Kuruvila’s son gives his father’s old letters and diaries to Akbar from which he starts learning about the whirlwind romance of Malavika and Ajayachandran which led to marriage. How differences between them led to a breakup and how this affected their careers along with Akbars efforts to find Malavika form the major plot of the film. The film ends in a touching yet marvelous climax with a poetic narration.

==Cast==
{| class="wikitable" style="width:50%;"
|-
! Actor !! Role
|- Priyamani || Malavika
|- Ajayachandran
|- Prithviraj Sukumaran|Prithviraj || Akbar Ahmed
|- Samvrutha Sunil || Devayani
|- Ranjith (director)|Ranjith || Director Aby Kuruvila
|-
| Praveena & Jyotsna || Special Appearance
|-
| Nishanth Sagar|| Kevin
|-
| Siddharth Siva || 
|-
| Mallika Sukumaran || Sarojam
|-
| Cochin Haneefa || Krishnadas
|-
| N. L. Balakrishnan || Baletan
|-
|T. P. Madhavan||
|- Augustine ||
|-
|Nandhu||
|-
| Joju George
|}

==Reception==
 Ranjith bounce back to form with his best effort so far."  Almost all reviewers were unanimous about the brilliant performances of the lead stars, especially of Priyamani, Anoop Menon and Prithviraj Sukumaran|Prithviraj.

The film fetched Anoop Menon the State Government award for Best Performance in a Supporting Role and Filmfare Award for Best Actress for Priyamani.

==Music==
The score and soundtrack of the movie were composed by Sharreth. The soundtrack featured five songs and has received positive reviews.

#Oduviloru - K. S. Chithra
#Oduviloru - Sharreth
#Palappoovithalil - Swetha Mohan, Nishad
#Onnodonnu - Haridas, Shankar Mahadevan, Renjini Haridas
#Arikil - Madhu Balakrishnan
#Arikil - Teenu Telence
#Manjuneeril - Kalpana

Rendition of Oduviloru by K. S. Chithra got huge appreciation and she got a Filmfare Award for her extremely soulful rendition. The song Onnodonnu had a vocal interlude bit rendered by Renjini Haridas, a popular Television anchor from Kerala.

==References==
 

https://www.academia.edu/9204740/When_Ghosts_Come_Calling_Re-projecting_the_Disappeared_Muses_of_Malayalam_Cinema==External links==
*  

 

 
 
 
 
 