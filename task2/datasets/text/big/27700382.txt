Midnight Ballad for Ghost Theater
{{Infobox film
| name           = Midnight Ballad for Ghost Theater
| image          = Midnight_Ballad_for_Ghost_Theater_Poster.jpg
| alt            = 
| caption        = 
| film name = {{Film name
 | hangul         = 삼거리 극장
 | hanja          =  거리  
 | rr             = Samgeori Geukjang
 | mr             = Samgŏri Kŭkjang}}
| director       = Jeon Kye-soo
| producer       = Hwang Yoon-kyeong Lee Seung-jae
| writer         = Jeon Kye-soo
| starring       = Kim Kkot-bi Chun Ho-jin Park Joon-myeon Jo Hee-bong
| music          = Kim Dong-ki
| cinematography = Kim Yeong-min
| editing        = Kim Hyeong-joo
| studio         = LJ Films Prime Entertainment
| distributor    = CJ Entertainment Cinema Service
| released       =  
| runtime        = 120 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} musical Fantasy fantasy Comedy comedy horror film written and directed by Jeon Kye-soo.

== Plot ==
The plot follows a young girl, Seong So-dan as she searches for her grandmother, who disappears at the beginning of the film after announcing that she wanted to go and see "her film". So-dan goes to the movie theatre to look for her and stops the suicidal manager from hanging himself, he gives her a job working in the box office so she can wait and see if her grandmother will show up. One night, while she is working late at the theater, four ghosts appear to her; Wanda, a bulimic ex-kisaeng who obsessively counts her own hair; Elisa, a Joseon Dynasty princess; Ijawa Hiroshi, a Japanese soldier stationed in Korea and Mosquito. Each of the ghosts is identical to a member of the theater staff working during the day.

So-dan overcomes her fear of the ghosts and joins them as they perform in the theatre at night. She eventually discovers that they were part of an acting troupe, which also included her grandmother and the manager. The four of them were killed at the premier of their debut film "Minosoo: The Bull-headed Man" and have decided to haunt the theatre until the film is shown again.

== Cast ==
* Kim Kkot-bi as Seong So-dan
* Chun Ho-jin as Woo Ki-nam
* Park Joon-myeon as Elisa
* Jo Hee-bong as Hiroshi
* Park Yeong-soo as Mosquito
* Han Ae-ri as Wanda

== Reception ==
Although the film performed only moderately at the box office and had only a limited DVD release, it is gaining reputation as a cult film and has been compared to the work of Tim Burton and to The Rocky Horror Picture Show thanks to its surreal edge.  
== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 