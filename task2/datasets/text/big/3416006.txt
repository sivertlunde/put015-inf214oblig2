The Legend of Boggy Creek
{{Infobox film
| name           = The Legend of Boggy Creek
| image          = BoggyCreek.jpg
| caption        = Promotional Movie Poster
| director       = Charles B. Pierce
| producer       = Charles B. Pierce
| writer         = Earl E. Smith
| starring       = William Stumpp Chuck Pierce, Jr. Vern Stierman Willie E. Smith
| music          = Jaime Mendoza-Nava
| cinematography = Charles B. Pierce
| editing        = Tom Boutross
| distributor    = Howco International Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $160,000    
| gross          = $20 million 
}} horror docudrama trucking company, used an old 35mm movie camera and hired locals (mainly high school students) to help make the 90-minute film. The film has generated approximately $20 million in box office revenue and is available on DVD.

==Plot==
The film, which claims to be a true story, sets out to detail the existence of the "Fouke Monster," a Bigfoot-like creature that has reportedly been seen by residents of a small Arkansas community since the 1950s. It is described as being completely covered in reddish-brown hair, leaving three-toed tracks in bean fields, and having a foul odor.
 hogs as well as other animals. In one scene, a kitten is shown as having been "scared to death" by the creature. The narrator informs the audience that while people have shot at the creature in the past, it has always managed to escape. In another scene, hunters attempt to pursue the creature with dogs, but the dogs refuse to give chase. A police constable states that while driving home one night, the creature suddenly ran across the road in front of him.

In a later sequence, culled from the actual newspaper accounts inspiring the film, the creature is shown menacing a family in a remote country house. After being fired upon, the creature attacks, sending one family member to the hospital.

==Cast==
 
 
*Vern Stierman as narrator
*Chuck Pierce, Jr. as Young Jim
*William Stumpp as Adult Jim
*Willie E. Smith as Willie
*Buddy Crabtree as James Crabtree
*Jeff Crabtree as Fred Crabtree
*Judy Baltom as Mary Beth Searcy
*Mary B. Johnson as Mary Beths sister
*George Dobson as George
*Dave Ball as Dave
 
*Jim Nicklus as Jim
*Flo Pierce as Bessie Smith
*Glenn Carruth as Bobby Ford
*Bunny Dees as Elizabeth Ford
*John Wallis as Mr. Ford
*Sarah Coble as Mrs. Carter
*Dave OBrien as Mr. Turner
*Billy Crawford as Corky Bill
*Dennis Lamb as Mr. Kennedy
*Loraine Lamb as Mrs. Kennedy
 
*Lloyd Bowen as himself
*B. R. Barrington as himself
*J. E. "Smokey" Crabtree as himself
*Travis Crabtree as himself
*John P. Hixon as himself
*John W. Oates as himself
*Herb Jones as himself
*Anthony Newsom as himself
*Cecil Newsom as himself
*Denise Newsom as herself
 

==Production notes== Walking Tall; Jackson County Jail) when most, if not all, of what was portrayed on screen was outright fiction.

Pierce originally planned to call the film Tracking the Fouke Monster.   

==Sequels==

===Return to Boggy Creek (1977)=== Tom Moore. The film carries over none of the originals docudrama elements. It stars Dawn Wells of Gilligans Island fame, and Dana Plato of Diffrent Strokes. Wells portrays the mother of three children who become lost in the swamp until the creature comes to their rescue. 

===  (1985)===
A third film, originally titled The Barbaric Beast of Boggy Creek, Part II, involved Pierce and was written as a sequel to the original film, thus the reason for styling the title as "II" instead of "III." It follows the adventures of a University of Arkansas professor (Pierce) and his students, one of which is Pierces son, on their trip to Fouke, Arkansas, to find and study the creature. A few scenes in the beginning of the movie were shot at the university, including an Arkansas Razorbacks football game.    The movie was featured in an episode of Mystery Science Theater 3000.  The "Big Creature" in the film was portrayed by James Faubus Griffith. 

===Boggy Creek: The Legend Is True (2010)===
This films story is unrelated to the others in the franchise, shifting from Arkansas to Texas. It deals with a bigfoot-like creature attacking a group of teenagers that are vacationing in the fictional area of Boggy Creek, Texas. The film was written and directed by Brian T. Jaynes. It was originally produced in 2010 and released straight to DVD on September 13, 2011. 

===The Legacy of Boggy Creek (2011) === indie film was originally released in 2009 under the title The Skunkape Story,   but was later re-edited and released to home video in 2011 as The Legacy of Boggy Creek. The docudrama chronicles the events that began after the original attacks in Fouke. It was written and directed by Dustin Ferguson. 

==Releases==

===Theatrical===
The Legend of Boggy Creek was released theatrically to major financial success given its budget of only $160,000, earning around $20 million at the box office. It was the   were released to theaters later, in 1977 and 1985, respectively. Neither of the sequels were as successful as the original film. The final two films have been released straight-to-video.

===Home Video===
Both The Legend of Boggy Creek and Boggy Creek II: And The Legend Continues have been released on VHS several times.  Between 2002 and 2011, Hens Tooth Video, Education 2000 Inc., Sterling Entertainment, Unicorn Video, RHR Home Video, and Cheezy Flicks Entertainment all released The Legend of Boggy Creek on Region 1 DVD.  Several of these versions are now out of print. The version RHR Home Video offers is the only true widescreen print available.

In 2005, Elite Entertainment released Boggy Creek II: And The Legend Continues on Region 1 DVD. Additionally, in 2004, the Mystery Science Theater 3000 episode that lampooned the film was released on DVD by Rhino Entertainment.  Only the Rhino Entertainment version is still in print. Return to Boggy Creek has only been issued on VHS by CBS Home Entertainment with no plans for a DVD release as of 2011.   

On September 13, 2011, Boggy Creek: The Legend Is True was released on DVD and Blu-ray by Hannover House. It features a Widescreen transfer and a handful of special features.    Also in 2011, The Legacy of Boggy Creek was released on DVD by RHR Home Video. 

== Cinematic influence ==
Its docudrama format was purposefully echoed in 1999s The Blair Witch Project.   In 2008, Duane Graves and Justin Meeks accurately recreated the drive-in feel of the movie in their blatant Boggy homage titled The Wild Man of the Navidad, released by IFC Films.   

==See also==
* Bigfoot
* Fouke Monster

==References==
 

== External links ==
*  
*   ( )

 

 
 
 
 
 
 
 
 
 