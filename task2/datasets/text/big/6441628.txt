The White Masai
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The White Masai
| image          = The White Masai poster.jpg
| caption        = The German theatrical poster.
| director       = Hermine Huntgeburth
| producer       = Günter Rohrbach
| writer         = Johannes W. Betz Hermine Huntgeburth (adaptation) Günter Rohrbach (adaptation) Ruth Toma (narrators script) Corinne Hofmann (novel)
| narrator       =
| starring       = Nina Hoss Jacky Ido
| music          = Niki Reiser
| cinematography = Martin Langer
| editing        = Eva Schnare
| distributor    = Constantin Film
| released       =  
| runtime        = 131 minutes
| country        = Germany German Swahili Swahili Maasai Maa
| budget         =
| gross          = 
}} 2005 film Maasai Lemalian (Jacky Ido). The film is based upon an autobiographical novel by the German born writer Corinne Hofmann. 

==Plot== Maasai warrior Lemalian (Ido), who is visiting dressed in the clothing of his area. At the airport on the way home she decides to stay. It turns out that Lemalian has gone to his home village in the Samburu District. Carola travels to the area, and stays at the house of another European woman. Lemalian hears about her stay and comes to meet her.  Eventually they start living together.

She travels to Switzerland to sell her shop there, promising Lemalian to come back to him. She does, and they marry and have a daughter. Carola buys a car and starts a shop. They lose money on the shop because Lemalian gives too much credit to friends and neighbors, and because they have to pay bribes to the mini-chief. Lemalian argues that this is no problem because she has more money in Switzerland.

The mini-chief demands that Carola hires his teenage nephew as a shop assistant. She has to accept this although she does not need him and he does not work hard. After some time, when he is just drinking beer and not working, she fires him. Later he returns and attacks her. A local judge rules that she has to pay two goats for firing him, but the boys family has to pay her five goats to compensate for the attack.

Carola is frustrated by the female circumcision being practised in the village. She wants to stop it, but it is a long tradition that is not easily changed.

When Carola helps a pregnant woman in labour with a breech birth, Lemalian refuses to assist because the woman is supposedly bewitched.

Lemalian does not want Carola to be friendly with other men, even if she just serves a customer in a friendly way. He is very jealous and suspicious of Carola having a boyfriend. He even wants to kill a man he suspects.

Carola wants to return to Switzerland stating she needs a two week holiday taking their daughter with her. After some hesitation, Lemalian signs a form giving Carola consent to take the girl out of Kenya, although he suspects that she will not return to Kenya.

== Thematic content ==
The themes of the film have been controversial.  Ultimately, the film is about the clash of cultures and worldviews. Two individuals who believe that their worldview is superior and therefore right (thus Carola condemns female circumcision because it does not fit her cultural outlook while Lemalian cannot understand how she could talk to men without being unfaithful to him), and it is their inability to understand the other that brings about their misery, separation, and divorce.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 