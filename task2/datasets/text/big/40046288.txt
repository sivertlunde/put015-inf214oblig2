Ain't Nobody Got Time for That
{{Infobox film
| name           = Aint nobody got time for that
| image          = Aint nobody got time for that screengrab.jpg
| caption        = Screengrab from the viral video
| narrator       = Joleen Chaney
| starring       = Kimberly "Sweet Brown" Wilkins
| cinematography = 
| distributor    = YouTube
| released       =  
| runtime        = 42 seconds
| country        = United States English
}}
 The View.   
 cameo role A Madea Christmas saying a part of her line from her television interview during an interview at the end of the movie. 

==Critical analysis==
Charles E. Williams, writing for the Huffington Post, said that the humor evoked by Sweet Browns interview should stay within the confines of the African-American community, linking it to the  "code-switching" phenomenon of which W. E. B. Du Bois spoke. 

==Lawsuit== Apple for selling a song called "I Got Bronchitis" on iTunes for profit, using catchphrases uttered by her in the video, such as "Aint nobody got time for that?", "Ran for my life," and "Oh, Lord Jesus, its a fire!". 

==Jimmy Kimmel parody==
 
In March 2014, variety show Jimmy Kimmel Live did a parody of the video in the style of a movie trailer. The plot is Sweet Brown going through important moments in history and being responsible for things such as Barack Obama getting into politics and Steve Jobs making Apple. It starred Queen Latifah as Sweet Brown, Barkhad Abdi as Barack Obama and Adam Driver as Steve Jobs. In the end, it features Matt Damon doing an interview when the real Sweet Brown comes in and interrupts him with her well-known line. As of September 2014, it has over 700,000 views on YouTube.

== References ==
 

== External links ==
* 

 

 
 


 