Meek's Cutoff
 
{{Infobox film
| name           = Meeks Cutoff
| image          = Meeks cutoff poster.jpg
| caption        = Theatrical release poster
| director       = Kelly Reichardt
| producer       = Elizabeth Cuthrell Neil Kopp Anish Savjani David Urrutia
| writer         = Jonathan Raymond
| narrator       =  Michelle Williams Paul Dano Bruce Greenwood Shirley Henderson Neal Huff Zoe Kazan Tommy Nelson Will Patton
| music          = Jeff Grace
| cinematography = Chris Blauvelt
| editing        = Kelly Reichardt
| studio         = Evenstar Films Film Science Harmony Productions Primitive Nerd
| distributor    = Oscilloscope Laboratories
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
}}
Meeks Cutoff is a 2010 western film directed by Kelly Reichardt. The film was shown in competition at the 67th Venice International Film Festival.    The story is loosely based on a historical incident on the Oregon Trail in 1845, in which frontier guide Stephen Meek led a wagon train on an ill-fated journey through the Oregon desert along the route later known as the Meek Cutoff.

==Synopsis==

In 1845, a small band of settlers traveling across the Oregon High Desert suspect their guide, Stephen Meek, may not actually know where he is going. What was supposed to be a two-week journey stretches into five. With no relief in sight, tensions rise as water becomes increasingly scarce and supplies run low. The wives look on as the husbands discuss what to do, unable to participate in the decision making. The dynamics of power begins to shift when they capture a lone Indian and hold him captive so he may lead them to water. Questions plague the settlers as they press on with dwindling resources: Should they trust Meek? Will the Indian lead them to water or a trap? Are there others following them? How much longer can they survive?

==Cast==
 , Bruce Greenwood, Shirley Henderson, Paul Dano, Zoe Kazan, and Will Patton. Not pictured are Neal Huff, Tommy Nelson, and Rod Rondeaux.]] Michelle Williams as Emily Tetherow
*Bruce Greenwood as Stephen Meek
*Shirley Henderson as Glory White
*Neal Huff as William White
*Paul Dano as Thomas Gately
*Zoe Kazan as Millie Gately
*Tommy Nelson as Jimmy White
*Will Patton as Solomon Tetherow Cayuse

==Critical reception==

Review aggregator Rotten Tomatoes gives the film a score of 85% based on reviews from 108 critics and reports a rating average of 7.5 out of 10, a generally positive reception. It reported the consensus, "Moving at a contemplative speed unseen in most westerns, Meeks Cutoff is an effective, intense journey of terror and survival in the untamed frontier."  At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 85 based on 36 reviews. 

==References==
 

==External links==
*  
*  
*  
*   at Metacritic
*  

 

 
 
 
 
 
 
 
 
 