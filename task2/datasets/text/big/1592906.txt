Way Out West (1937 film)
 
{{Infobox film
| name           = Way Out West
| image          = Way_Out_West_Poster.gif
| caption        = Theatrical release poster
| director       = James W. Horne
| producer       = Stan Laurel Hal Roach Felix Adler James Parrott James Finlayson Rosina Lawrence
| music          = Marvin Hatley
| cinematography = Art Lloyd Walter Lundin
| editing        = Bert Jordan
| studio         = Hal Roach Studios
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 64 21"
| language       = English
| country        = United States
| budget         =
}}

Way Out West is a Laurel and Hardy comedy film released in 1937. It was directed by James W. Horne, produced by Stan Laurel and distributed by Metro-Goldwyn-Mayer.  This was the second picture for which Stan Laurel was credited as producer (the first was 1936s Our Relations); Laurel served in that capacity uncredited for the duos entire career.

==Plot== saloon owner James Finlayson), and his equally-cruel saloon-singer wife, Lola Marcel (Sharon Lynn).

Traveling by stage coach, they attempt to flirt with the woman (Vivien Oakland) who is riding with them. She rebuffs the pair, and upon arriving in Brushwood Gulch, she complains to her husband, the towns sheriff (Stanley Fields). The angry sheriff orders the pair to leave on the next coach out of town, or else theyll be "riding out of here in a hearse". Stan and Ollie promise to do so once they have completed their mission.

After dancing to "At the Ball, Thats All" by The Avalon Boys, Stan and Ollie arrive at Mickey Finns saloon. When Mickey Finn learns why theyre here, he has Lola play Mary in order to hijack the deed from them. Stan and Ollie have never seen Mary before, and are duped by their charade. However, before leaving town, they encounter the real Mary Roberts and immediately try to get the deed back. The evil Finns will not surrender the deed, however, and a major struggle ensues as Stan and Ollie attempt to reclaim the deed. Stan manages to grab it, but Lola traps him in the bedroom and wrests the deed from him by tickling him into hysterics. After further chasing, Mickey and Lola manage to seal the deed into their safe. Ollie calls for the police, but the police turn out to be the angry sheriff, who chases Stan and Ollie out of town.

Outside the town, Stan and Ollie plan to sneak back into Brushwood Gulch at night to reclaim the deed. They arrive at the saloon and, after a series of mishaps (including Laurel stretching Hardys neck an incredible 3 feet while freeing him from a trapdoor in which his head was stuck), manage to make it inside. They are met by Mary, who helps them open the safe, grab the deed and escape before Mickey Finn can intervene, managing to get him entangled in the grill in the front door. Outside the town again, and accompanied by Mary, the happy trio sing "Were Going to See My Home in Dixie" as they head off into the sunset with Ollie yet again falling into the hole in the pond, as he did twice before in the film.

Unlike most of Laurel and Hardys films and shorts, the story has a happy ending as opposed to the usual "unfortunate ending". (Running gag of Ollie falling into a sink hole in the stream notwithstanding.)

==Cast==
* Stan Laurel as Stan
* Oliver Hardy as Ollie
* Sharon Lynn as Lola Marcel (credited as Sharon Lynne) James Finlayson as Mickey Finn
* Rosina Lawrence as Mary Roberts Stanley Fields as Sheriff
* Vivien Oakland as Sheriffs wife
* The Avalon Boys as themselves
* Dinah as herself

Uncredited:
* Harry Bernard as Man Eating at Bar
* Flora Finch as Maw Mary Gordon as Cook Jack Hill as Finns Employee
* Sam Lufkin as Stagecoach Baggage Handler
* Fred Toones as Janitor
* May Wallace as Cook

==Soundtrack== score was Academy Award Trail Of The Lonesome Pine" sung by Laurel and Hardy except for a few lines by Chill Wills and Rosina Lawrence, lip-synched for comedic effect by Laurel, and secondly "At The Ball, Thats All" sung by The Avalon Boys and accompanied by Laurel and Hardy performing an extended dance routine, one which they rehearsed endlessly. In recent years, the latter scene has been widely mashup (music)|mashed-up with various modern tunes, such as "Party Train" by The Gap Band. 

"Trail Of The Lonesome Pine" was released as a single in Britain in 1975 backed by "Honolulu Baby" from Sons of the Desert, reaching number 2 in the British charts.

==Colorization== colorized in 1985.

==References==
;Notes
 
;Bibliography
 
*  
*  
*  
*  
*  
*  
*  
*  
 

==External links==
*  
*  
*  
*  
*   at Trailers from Hell
*  

 
 

 
 
 
 
 
 
 
 
 
 
 