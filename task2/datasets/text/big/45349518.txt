Malli Malli Idi Rani Roju
{{Infobox film
| name           = Malli Malli Idi Rani Roju
| image          = File:Malli_Malli_Idi_Rani_Roju_Movie_Poster.jpg
| caption        = 
| director       = Kranthi Madhav
| producer       = K. A. Vallabha
| story          = Kranthi Madhav
| screenplay     = Kranthi Madhav
| starring       = Sharwanand Nithya Menen
| music          = Gopi Sunder
| cinematography = V. S. Gnana Sekhar
| editing        = Kotagiri Venkateswara Rao
| studio         = Creative Commercials 
| released       =  
| runtime        = 144 min
| country        = India
| language       = Telugu
}}

Malli Malli Idi Rani Roju is a 2015 Telugu romance film written and directed by Kranthi Madhav starring Sharwanand and Nithya Menen.  The film released on 6 February 2015.

==Cast==
* Sharvanand as RajaRam
* Nithya Menon as Nazira
* Pavani Reddy as Naziras friend Jyothi
* Pavithra Lokesh as RajaRams mother Parvathi
* Tejaswi Madivada as Naziras daughter Mehak
* Punarnavi_Bhupalam as RajaRams daughter Parvathi 
* Nassar as Naziras father
* Sana
* Naveen
* Surya
* Chinna 
* Guneeth


==Soundtrack==
The soundtrack was composed by Malayalam composer Gopi Sunder, making his debut in Telugu cinema. The album features five tracks.

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| music_credits = no
| title1 = Yenno Yenno
| extra1 = Karthik (singer)|Karthik, Chinmayi
| lyrics1 = Sahiti
|-
| title2 = Marhaba
| extra2 = K. S. Chitra
| lyrics2 = Sahiti
|-
| title3 = Choti Zindagi
| extra3 = Hiranmayi
| lyrics3 = Ramajogayya Sastry
|- 
| title4 = Gathama Gathama 
| extra4 = Priya Hemesh
| lyrics4 = Ramajoggaya Sastry
|-
| title5 = Varinche Prema 
| extra5 = Haricharan
| lyrics5 = Sahiti
}}

==Critical reception==
The Times of India gave the film 3 stars out of 5 and wrote, "That a filmmaker can evoke at least a bit of awe while dealing with romance, amidst scores of sex-crazed rom-coms, is no mean feat. Between its simplistic approach to romance and lack of a strong conflict, theres a layer of magic in Malli Malli Idhi Rani Roju. Thats more than enough to make you crave for more, especially for Nithya and Sharwanands crackling chemistry".  The Hindu wrote, "If you love old-world romances, Malli Malli… is up your alley. On the flipside, the film’s snail pace gets grating at times and the contrived ending...takes some sheen off the otherwise beautiful film". 

123telugu.com gave 3.25 out of 5 and wrote, "Malli Malli Idi Rani Roju is a feel good love story which stays within your heart. However, the slow paced narration and lack of entertainment will not go well with certain sections of the audience".  idlebrain.com gave 3 out of 5 and wrote, "Malli Malli Idi Rani Roju is a good attempt, but not engaging enough". iqlikmovies.com user ratings garnered 3 out 5. 

==References==
 

==External links==
*  

 
 
 
 
 