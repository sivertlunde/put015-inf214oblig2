Filhaal...
{{Infobox Film
| name           = Filhaal...
| image          = Filhaal.jpg
| caption        = DVD cover
| writer         = Meghna Gulzar Tabu Sanjay Suri, Palash Sen
| director       = Meghna Gulzar
| producer       = Jhamu Sughand
| cinematography = Manmohan Singh
| editing        = Chandan Arora distributor     = Dharma Productions
| released       = 1 February 2002
| runtime        = English
| music          = Anu Malik
| awards         =
| budget         =
}}

Filhaal... ( ) is a Bollywood film released in 2002 in film|2002. It stars Sushmita Sen, Tabu (actress)|Tabu, Sanjay Suri and Palash Sen in the lead roles. Meghna Gulzar, daughter of lyricist Gulzar and actress Raakhee, made her directorial debut with the film, the film was average but was critically well received especially the performance of MS sen, and won Zee Cine Awards for best supporting actress.

== Synopsis ==
Rewa Singh (Tabu (actress)|Tabu) and Sia Sheth (Sushmita Sen) are best friends. Having grown up together, the pair are inseparable and are always there for each other. They are very different when it comes to marriage and commitment. Rewa is passionate and dreams of having her own family whilst Sia is always running away from settling down and is determined to put her career first.

Sias boyfriend Sahil (Palash Sen) has proposed to her three times and she always turns him down, yet he is determined to marry her. Rewa meets the love of her life Dhruv Malhotra (Sanjay Suri) and marries him. They are overjoyed when they find out Rewa is pregnant. The whole family is thrilled but their joy is short-lived when Rewa suffers a miscarriage and, due to complications, is unable to have children. Rewa is devastated but tries to carry on with the support of Dhruv and Sia. However, feeling incomplete and empty, she slowly sinks into depression.

Sia cannot stand to see her friend like this and asks to be a surrogate mother to their child. Dhruv is opposed to the idea, but Rewas insistence sways his decision and he agrees. Soon Sia is pregnant and she is overjoyed for herself but more specifically for Rewa. Unfortunately, Sahil disagrees with what Sia did and abruptly breaks off their relationship.

Rewa and Dhruv do everything to make sure that Sia takes care of herself and the baby stays healthy. However cracks between Rewa and Sia begin to surface. Rewa is jealous that Dhruv is spending more time with Sia and is frustrated as she is not able to experience the joy of carrying a child. Rewa begins to lash out at Sia and at times insults her. Things come to a head when Sia decorates the babys room. Rewa, unable to cope, shouts at Sia and declares that she does not want the baby. Sia leaves in tears and goes into an early labour. Rewa, realising her mistake, rushes to her friends side. Sia gives birth to a girl and hands the child to Rewa. Rewa and Dhruv leave with their child and Sia meets Sahil who proposes to her again and this time she accepts.

== Cast == Tabu ... Rewa Singh
* Sushmita Sen ... Sia Sheth
* Sanjay Suri ... Dhruv Malhotra
* Palash Sen ... Sahil

==Soundtrack==
The songs of the movie were composed by Anu Malik to lyrics penned by Gulzar.

* Filhaal   Artist: Asha Bhosle 
* Le Chalen Doliyon   Artists: Roopkumar Rathod, Chitra 
* Sola Singaar   Artists: Jaspinder Narula, Palash Sen 
* Kyun Baar Baar   Artist: Chitra 
* Naya Naya   Artists: Kay Kay, Chitra 
* Dil Ke Sannate   Artist: Palash Sen 
* Waqt Ka Saaya   Artists: Sonu Nigam, Jaspinder Narula 
* Filhaal   instrumental

The background score was composed by Anjan Biswas.

==External links==
*  

 
 
 
 