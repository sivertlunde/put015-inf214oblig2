The Court Martial of Major Keller
{{Infobox film
| name           = The Court Martial of Major Keller
| image          = 
| caption        = 
| director       = Ernest Morris
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         = Brian Clemens
| starring       = Laurence Payne Susan Stephen Ralph Michael Richard Caldicot
| music          = Bill LeSage James Wilson
| editing        = Spencer Reeve
| studio        = Danziger Productions
| distributor    = Associated British Picture Corporation|Warner-Pathé Distributors
| released       = 1961
| runtime        = 69 mins
| country        = United Kingdom
| language       = English
}}
The Court Martial of Major Keller is a 1961 British film directed by Ernest Morris and written by Brian Clemens. It stars Laurence Payne, Susan Stephen and Austin Trevor.  The film recounts the court martial for murder of Major Keller, a British army officer during the Second World War.

==Cast==
* Laurence Payne as Major Keller  
* Susan Stephen as Laura Winch  
* Ralph Michael as Colonel Winch  
* Richard Caldicot as Harrison  
* Basil Dignam as Morrell  
* Austin Trevor as Power  
* Simon Lack as Wilson  
* Jack McNaughton as Miller  
* Hugh Cross as Captain Cuby   Peter Sinclair as Sergeant  
* Humphrey Lestocq as Lieutenant Cameron

==References==
 

==External links==
*  

 
 
 
 
 

 