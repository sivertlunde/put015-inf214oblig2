Pradeshika Varthakal
{{Infobox film
| name = Pradeshika Varthakal
| image =Pradeshika varthakal.jpg
| caption = Kamal
| producer = S Sivaprasad Ranjith
| Ranjith
| Parvathy Jagathy Innocent
| Johnson
| cinematography = Saloo George
| editing = G Murali
| studio = Sree Sai Productions
| distributor = Sree Sai Productions
| released =  
| country = India Malayalam
}}
 1989 Cinema Indian Malayalam Malayalam film, Kamal and Innocent in lead roles. The film had musical score by Johnson (composer)|Johnson. 

==Cast==
  
*Jayaram  Parvathy 
*Jagathy Sreekumar  Innocent 
*KPAC Lalitha  Siddique 
*Kuthiravattam Pappu 
*Mamukkoya 
*Oduvil Unnikrishnan 
*Philomina 
 

==Soundtrack==
The music was composed by Johnson (composer)|Johnson.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Pandu Pandu || M. G. Sreekumar, Dinesh || Shibu Chakravarthy || 
|- 
| 2 || Thulasitharayil || M. G. Sreekumar, Sunanda || Shibu Chakravarthy || 
|- 
| 3 || Title Song || Shibu Chakravarthy || Shibu Chakravarthy || 
|-  Susheela || Vayalar || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 