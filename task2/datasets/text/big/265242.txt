The Brown Bunny
 
{{Infobox film
| name = The Brown Bunny
| image = Brown bunny post.jpg
| alt = 
| caption = Theatrical release poster
| director = Vincent Gallo
| producer = Vincent Gallo
| writer = Vincent Gallo
| starring = {{Plain list | 
* Vincent Gallo
* Chloë Sevigny
* Cheryl Tiegs
}}
| music = {{Plain list | 
* Jackson C. Frank
* Jeff Alexander
* Gordon Lightfoot
* Ted Curson
* Accardo Quartet
* John Frusciante
}}
| cinematography = Vincent Gallo
| editing = Vincent Gallo Wild Bunch Vincent Gallo Productions
| distributor = Wellspring Media
| released =  
| runtime = 118 minutes    93 minutes  
| country = {{Plain list | 
* United States
* Japan
* France
}}
| language = English
| budget = $10 million
| gross = $366,301
}} independent art world premiere thumbs up".   
 cameo performance model Cheryl handheld 16 16 mm cameras in various locations throughout the United States, including New Hampshire, Massachusetts, Ohio, Missouri, Utah, Nevada, and California.

==Plot==
Motorcycle racer Bud Clay undertakes a cross-country drive, following a race in New Hampshire, in order to participate in a race in California. All the while he is haunted by memories of his former lover, Daisy. On his journey he meets three women, but Bud seems to be a lost soul, and hes unable to form an emotional connection with any of them. He first meets Violet at a gas station in New Hampshire and convinces her to join him on his trip to California. They stop at her home in order to get her clothes, but he drives off as soon as she enters the house.

Buds next stop is at Daisys parents home, the location of Daisys brown bunny. Daisys mother does not remember Bud, who grew up in the house next door, nor does she remember having visited Bud and Daisy in California. Next, Bud stops at a pet shelter, where he asks about the life expectancy of rabbits (he is told about five or six years). At a highway rest stop, he joins a distressed woman, Lilly, comforts and kisses her, before starting to cry and eventually leaving her. Bud appears more distressed as the road trip continues, crying as he drives. He stops at the Bonneville Speedway to race his motorcycle. In Las Vegas, he drives around prostitutes on street corners, before deciding to ask one of them, Rose, to join him for a lunch. She eats McDonalds food in his truck until he stops, pays her, and leaves her back on the street.

After having his motorcycle checked in a Los Angeles garage, Bud stops at Daisys home, which appears abandoned. He leaves a note on the door frame, after sitting in his truck in the driveway remembering about kissing Daisy in this place and checks in at a hotel. There, Daisy eventually appears. She seems nervous, going to the bathroom twice to smoke crack cocaine, while Bud waits for her, sitting on his bed. As she proposes to go out to buy something to drink, Bud tells her that, because of what happened the last time they saw each other, he doesnt drink anymore.
 pot with them. Bud becomes upset because Daisy was pregnant and it transpires that the baby died as a result of what happened at this party.

Through flashback scenes, the viewer understands that Daisy was raped at the party, a scene witnessed by Bud, who did not intervene. Daisy asks Bud why he didnt help her, and his feelings of guilt on this are considerable. But Bud explains to her that he didnt know what to do, and so he decided to leave the party. After he came back a bit later, he saw an ambulance in front of the house and Daisy explains to Bud that shes dead, having passed out prior to the rape and then choked to death on her own vomit. Bud awakens the next morning, alone; his encounter with Daisy turns out to be a figment of his imagination. The movie ends as Bud is driving his truck in California.

==Cast==
* Vincent Gallo as Bud Clay
* Chloë Sevigny as Daisy
* Cheryl Tiegs as Lilly
* Elizabeth Blake as Rose
* Anna Vareschi as Violet
* Mary Morasky as Mrs. Lemon
* Rick Doucette (uncredited) as Featured racer

==Production and release== 16 mm 35 mm, which gives the photography a typical "old-school grain".    Vincent Gallo is credited as director of the photography as well as one of the three camera operators along with Toshiaki Ozawa and John Clemens.

The version of the film shown in the U.S. has been cut by about 25 minutes compared to the version shown at Cannes, removing a large part of the initial scene at the race track (about four minutes shorter), about six minutes of music and black screen at the end of the film, and about seven minutes of driving before the scene in the Bonneville Speedway. 

Neither Anna Vareschi nor Elizabeth Blake, both in the film, were professional actresses. Kirsten Dunst and Winona Ryder were both attached to the project but left. In an interview from The Guardian  Sevigny said of the sex scene: "It wasnt that bad for me, I have been intimate with Vincent before."

For the films promotion, a trailer was released featuring a split screen in the style of Andy Warhols Chelsea Girls, depicting on one side of the screen a single point-of-view-shot of a driver on a country road, and the other side various scenes from the end of the film featuring Chloë Sevigny. Both sides of the screen had no audio tracks attached, although the song "Milk and Honey" by folk singer Jackson C. Frank played over the trailers duration.

==Controversy==

===Cannes reception and reviews===
The film was entered into the 2003 Cannes Film Festival.   
 hex on Eberts colon (anatomy)|colon, cursing the critic with cancer. In response, Ebert quipped that watching a video of his colonoscopy had been more entertaining than watching The Brown Bunny.  Gallo subsequently stated that the hex had actually been placed on Eberts prostate and that he had intended the comment to be a joke which was mistakenly taken seriously by a journalist. He also conceded to finding Eberts colonoscopy comment to be an amusing comeback. 
 Ebert & Roeper, Ebert gave the new version of the film a "thumbs up" rating. In a column published about the same time, Ebert reported that he and Gallo had made peace. According to Ebert:

 

Nevertheless The Brown Bunny still received mixed reviews from other critics and has a rating of 43% on Rotten Tomatoes based on 90 reviews with an average score of 5.1 out of 10. The sites consensus states "More dull than hypnotic, The Brown Bunny is a pretentious and self-indulgent bore."  Metacritic gives the film a score of 51 out of 100 on based on reviews from 30 critics. 

French cinemas magazine, Les Cahiers du Cinéma, voted The Brown Bunny one of the ten best films of 2004.  The film won the FIPRESCI Prize at the Vienna International Film Festival for its "bold exploration of yearning and grief and for its radical departure from dominant tendencies in current American filmmaking".  The film, aside from the feud with Roger Ebert, gained some positive reaction from American critics as well. Neva Chonin of the San Francisco Chronicle called it "a somber poem of a film sure to frustrate those who prefer resolution to ambiguity... like an inscrutably bad dream,   lingers on". 

The Daily Telegraph listed The Brown Bunny as one of the 100 "defining" films of the decade, calling it the decades "most reviled" film, but saying it was "destined to become a future lost classic". 

===Sevignys response=== limited theatrical release in the United States, star Sevigny took to defending the film and its controversial final scene, stating:

 

Right before the films Cannes premiere, the William Morris Agency had dropped her as a client. A source there reportedly said, "The scene was one step above pornography, and not a very big one. William Morris now feels that her career is tainted and may never recover, especially after rumors began circulating about the even more graphic outtakes that didnt make it into the actual film." 

Sevigny continues to work as a professional actress using another talent agency, and her career has been substantially enlarged and elevated after the release of this film; the year after the release of The Brown Bunny, Sevigny received a leading role on the HBO series Big Love, for which she would later win a Golden Globe award; she has also continued to appear in a number of major studio films as well as other well received independent and "art" films.

Despite the negative backlash toward Sevignys involvement in the film, some critics praised her decision. New York Times reviewer Manohla Dargis said:
 Girls Gone Wild, its genuinely startling to see a name actress throw caution and perhaps her career to the wind. But give the woman credit. Actresses have been asked and even bullied into performing similar acts for filmmakers since the movies began, usually behind closed doors. Ms. Sevigny isnt hiding behind anyones desk. She says her lines with feeling and puts her iconoclasm right out there where everyone can see it; she may be nuts, but shes also unforgettable.  

Seven years later, in an interview for Playboy Magazine|Playboy s January 2011 issue, Sevigny talked about the infamous oral sex scene in the film:

 

===Billboard promotion=== billboard erected The Players (Les infidèles), triggered a similar controversy.

==Soundtrack==
{{Infobox album
| Name        = The Brown Bunny
| Type        = Soundtrack
| Artist      = John Frusciante & Various Artists
| Cover       = The Brown Bunny.jpg 
| Released    = May 4, 2004
| Recorded    =
| Genre       =
| Length      =
| Label       = Tulip Records|Tulip, Twelve Suns
| Producer    =
| Reviews     =
| Chronology  = John Frusciante
| Last album  = To Record Only Water for Ten Days  (2001)
| This album  = The Brown Bunny  (2004)
| Next album  = Shadows Collide with People  (2004)
}}
The motion picture soundtrack to The Brown Bunny was released exclusively in Japan. The first five tracks come from artists Gordon Lightfoot, Jackson C. Frank, Matisse/Accardo Quartet, Jeff Alexander and Ted Curson. The last five tracks are performed by John Frusciante.

  Come Wander with Me" (Jeff Alexander)
# "Tears for Dolphy" (Ted Curson)
# "Milk and Honey" (Jackson C. Frank)
# "Beautiful" (Gordon Lightfoot)
# "Smooth" (Matisse/Accardo Quartet)
# "Forever Away" (John Frusciante)
# "Dying Song" (John Frusciante)
# "Leave All the Days Behind" (John Frusciante)
# "Prostitution Song" (John Frusciante)
# "Falling" (John Frusciante)

;Re-release
Australian indie label Twelve Suns re-issued The Brown Bunny Soundtrack on deluxe gatefold vinyl on April 26, 2014. This reissue was fully authorized by Vincent Gallo and remastered from his master recordings. The re-issue was limited to 1000 copies. The first 5 tracks are from the film The Brown Bunny. The last 5 tracks were written before the movie and used as inspiration during filming. 

==See also==
* 2003 Cannes Film Festival
* Art house films
* Independent films
* List of American films of 2004

==References==
 

==External links==
*  
*  
*  
*  
*  
*  , David Edelstein, Slate.com, 10 September 2004.
*  , Charles Taylor, Salon.com, 17 September 2004.

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 