The Burning Secret
{{Infobox film
| name = The Burning Secret 
| image =
| image_size =
| caption =
| director = Robert Siodmak
| producer =  Alfred Sternau
| writer =  Stefan Zweig (novella)   Frederick Kohner   Alfred Polgar   Robert Siodmak 
| narrator =
| starring = Alfred Abel   Hilde Wagener   Hans Joachim Schaufuß   Lucie Höflich   Willi Forst
| music = Allan Gray  
| cinematography = Richard Angst   Robert Baberske   Max Brenner     
| studio = Tonal-Film 
| distributor = Deutsche Universal-Film 
| released = 20 March 1933
| runtime = 92 minutes
| country = Austria   Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Propaganda Minister. remade in 1988.

==Cast==
* Alfred Abel as Der Mann  
* Hilde Wagener as Die Frau 
* Hans Joachim Schaufuß as Edgar  
* Lucie Höflich as Mutter der Frau  
* Willi Forst as Herr von Haller, Rennfahrer  
* Ernst Dumcke as Baron Tosse  
* Alfred Beierle as Müller, Hoteldetektiv   Hans Richter as Fritz, Page 
* Rina Marsa as Fräulein de la Roche 
* Heinz Berghaus as Hotelportier 
* Lotte Stein as Frau Klappholz  
* F.W. Schröder-Schrom
* Babette Jenssen
* Edwin Jürgensen
* Kurt Pulvermacher 
* Alexander Fernoff

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.
* Reimer, Robert C. & Reimer, Carol J. The A to Z of German Cinema. Scarecrow Press, 2010.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 