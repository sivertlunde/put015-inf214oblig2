Chocolate (2001 film)
{{Infobox film
| name           = Chocolate
| image          = Chocolate DVD Cover.jpg
| caption        = DVD Cover
| director       = A. Venkatesh (director)|A. Venkatesh
| writer         = Pattukkottai Prabakar   (Dialogue) 
| screenplay     = A. Venkatesh
| story          = R. Madhesh Livingston
| producer       = R. Madhesh
| cinematography = S.D. Vijay Milton Deva
| studio         = R. Madhesh | Movie Magic
| distributor    = Movie Magic
| editing        = B. Lenin V. T. Vijayan
| released       = September 7, 2001
| runtime        =
| country        = India Tamil
}} Tamil film Prashanth and Jaya Re in the lead roles, the film also has Livingston (actor)|Livingston, Suhasini Mani Ratnam|Suhasini, Mumtaj and Nagendra Prasad in supporting roles, while the films score and soundtrack are composed by Deva (music director)|Deva. The film opened to a positive response at the box office in September 2001. 

== Plot ==
Aravind (Prashanth (actor)|Prashanth) meets Anjali (Jaya Re) & falls in love at first sight without knowing her background. When this two meets Anjali tells Aravind that she would like to go for a trial-romance for a week, which might lead to a long-term romance, for which Aravind agrees.

Meanwhile, Anjali is the daughter of Jayachandran (Livingston (actor)|Livingston) – (the Commissioner of Police) & Saradha (Suhasini). Aravind maintains a cordial acquaintance with both of them, not knowing that Anjali is their daughter. Anjali acknowledges them as her parents. Jayachandran convinces Anjali to dispense with this trial romance, & make a deeper commitment as he finds Aravind a gentleman. Aravind convinces Anjalis mother Saradha to support him as her suitor and so Saradha makes Aravind marry Anjali .

== Cast == Prashanth as Aravind
* Jaya Re as Anjali
* Mumtaj as Nirmala (Nimmi) & Bhama
* Suhasini Mani Ratnam as Saradha Livingston as Jayachandran
* Dhamu Charlie as Sasi
* Nagendra Prasad as Guru

==Production==
The film began production in January 2001 and scenes were shot at Mayajaal for three days later that month.  At an official launch event held soon afterwards, producer R. Madhesh donated seventy five kilogrammes of chocolate to poor children, equivalent to the weight of the films actor Prashanth. A fight scene was shot at Koyambedu Market Road near Chennai with 16 cameras, while a song was shot on a cruise ship between Cambodia and Vietnam. 

==Release==
The film opened to mixed reviews in September 2001, after having an initial premiere at Bay Area, San Francisco. A critic from Sify.com noted "The film tantalises in the beginning only to loose its fizz halfway through", adding "our heart goes straight out to Prashanth who despite the odds, comes up with a decent performance."  Malathi Rangarajan of The Hindu reviewed the film and added that "this youthful bonanza gets stretched, testing ones patience towards the end, none but the screenplay is to blame."  Rediff.com meanwhile gave the film a negative review noting "Matters are built towards a loud, dramatic, implausible climax. This ones a mess and what you keep wondering is why an accomplished actress like Suhasini Mani Rathnam had to make this the vehicle for one of her rare celluloid appearances."  A critic from Cinesouth.com noted "The film is bubbling with youthful feelings. It contains some admirable scenes also. Thus, movie manages to make its mark with a lot difficulties." 

The film was later dubbed and released in Telugu in March 2002 with the same name and won positive reviews from critics.  The success of the film led Prashanth and A. Venkatesh re-unite and announced a project called "Petrol" in 2005   but Prashanths marital problems eventually led to delays in the directors schedule and the film remains uncompleted. 

==Music==
The film features song composed by Deva. Producer R. Madhesh opted against having a traditional audio cassette release function and chose to distribute the cassettes to music shops enclosed with Cadbury chocolate. 
{{Infobox album|  
  Name        = Chocolate
|  Type        = Soundtrack Deva
|  Cover       =
|  Released    = July 2001
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = The Best Audio
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Chocolate (2001)
|  Next album  = 
}}
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)
|- 1 || Mathangi
|- 2 || "Chocolate" || 
|- 3 || "Dhuryodhana Dhuryodhana" || Shankar Mahadevan, Mahalakshmi Iyer 
|- 4 || "Hosima Hosima" || Devan Ekambaram, Anupama
|- 5 || "Kappaleh Kappaleh" || Hariharan (singer)|Hariharan, Mahalakshmi Iyer 
|- 6 || "Kokaragiri" || Sabesh-Murali, Mathangi
|- 7 || "Malai Malai" || Anuradha Sriram, A. R. Reihana 
|- 8 || "En Nizhalai " || Srikanth, Timmy, Mathangi
|- 9 || "Oru Five Star Paarvai" || Niruban 
|}

== References ==
 

 

 
 
 
 
 