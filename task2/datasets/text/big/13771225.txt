Change of Mind
{{Infobox film
| name           = Change of Mind
| image          = changeofmind.jpg
| caption        = Film Poster Robert Stevens	 Dick Wesson Henry S. White Dick Wesson
| starring       = Raymond St. Jacques Susan Oliver Janet MacLachlan Leslie Nielsen
| music          = Duke Ellington and Orchestra
| cinematography = Arthur J. Ornitz
| editing        = Donald Ginsberg
| distributor    = Cinerama Releasing Corporation
| released       = 1969
| runtime        = 98 Min.
| country        = United States
| language       = English
}}
Change of Mind (1969) is a science fiction/drama film starring Raymond St. Jacques, Susan Oliver, Janet MacLachlan, and Leslie Nielsen.
 
==Plot==
A married couple struggles to adjust when the husbands brain is transplanted into the skull of a black man.

David Rowe (St. Jacques) is a white district attorney who must now live his life as a black man. His wife Margaret (Oliver) tries to deal with the transformation of her husbands appearance as David feels the stings of racial prejudice for the first time.

Sheriff Webb (Nielsen) is a local lawman who resents the district attorney, but after the sheriff kills his own black mistress, he must rely on David for his legal defense. 

Margaret has trouble being intimate with the man she knows is still her husband. Rowe investigates the murder of the young black woman while his superiors, friends and family treat him differently.

==Cast==
*Raymond St. Jacques : David Rowe
*Susan Oliver : Margaret Rowe
*Janet MacLachlan : Elizabeth
*Leslie Nielsen : Sheriff Webb
*Donnelly Rhodes : Roger Morrow David Bailey : Tommy Benson
*Andre Womble : Scupper
*Clarice Taylor : Rose Landis
*Jack Creley : Bill Chambers
*Cosette Lee : Angela Rowe
*Larry Reynolds : Judge Forrest
*Hope Clarke : Nancy
*Rudy Challenger : Howard Culver
*Henry Ramer : Chief Enfield 
*Joseph Shaw : Gov. LaTourette

==Translations==
Spain : Cambio de mente    
Greece : Gefsi apo klemmeni idoni  
Italy : Impostor Involuntario   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 