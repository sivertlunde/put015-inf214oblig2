Naked Fame
{{Infobox Film
|  name     = Naked Fame
|  image = Naked_Fame.jpg
|  caption = 
|  starring       = Colton Ford Blake Harper Chi Chi La Rue Pepper Mashay Bruce Vilanch Christopher Long
|  distributor    = Regent Releasing TLA Releasing
|  released   = February 18, 2005 (limited)
|  runtime        = 85 minutes
|  language = English
|    gross = $213,621 (USA)
}}

Naked Fame a documentary that follows Colton Fords transition from the world of gay porn to that of a mainstream singer.     It opened on February 18, 2005 in the United States to mixed critical reviews.

==Synopsis==
At the age of 40, porn star Colton Ford decides to leave the industry and pursue his true calling — a singing career.  He feels he has the voice, but his "overexposed" past could be the hook that gets him noticed or the hook that yanks him off stage.  Naked Fame follows Colton and his former life-partner, porn star Blake Harper, as they try to ease their way back into the mainstream.

The film examines the inner-workings of music industry and documents the emotional struggles involved in the pursuit of ones dreams.  It contains footage and interviews from Chi Chi LaRue, Bruce Vilanch, Pepper Mashay and Lonnie Gordon, and Fords parents.

==Reaction==
Naked Fame received mixed reviews from leading movie critics, getting a 50% fresh score on the Tomatometer.  LA Times film critic Kevin Thomas called it "an engaging and forthright documentary"  and New York Post critic Russell Smith found it to be "a sunny movie full of likable characters"  In contrast, Peter LOfficial of Village Voice said "there are many dreadful elements in this chronicle"  and David Noh of Film Journal International thought the director was "scrambling" with "flimsy material." 

Naked Fame grossed $213,621 in the United States. 

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 


 
 