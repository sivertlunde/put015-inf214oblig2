Les Hommes nouveaux
{{Infobox film
| name           = Les Hommes nouveaux
| image          = 
| caption        = 
| director       = Marcel LHerbier
| producer       = Les Films Albert Lauzin
| writer         = Marcel LHerbier Claude Farrère
| starring       = Harry Baur Henri Rollan
| music          = Marius-François Gaillard
| cinematography = 
| editing        = 
| distributor    = 
| released       = 4 March 1936 (France)
| runtime        = 110 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Drama drama film from 1936, directed by Marcel LHerbier, written by Marcel LHerbier, starring Harry Baur.  Film was based on a novel of Claude Farrère. 

== Cast ==
* Harry Baur: Bourron
* Natalie Paley: Christiane (as Nathalie Paley)
* Gabriel Signoret: Maréchal Lyautey de Tolly
* Max Michel: Henri de Chassagnes
* Claude Sainval: Jean de Sainte-Foy
* Sylvio De Pedrelli: Medhani, the gangster boss
* Jean Marais: the clerk
* André Numès Fils: Roussignol
* René Bergeron: Mingasse
* Marie-Jacqueline Chantal: the nurse
* Gustave Gallet: Clémenceau
* Ben Gassin: Zerfatti
* André Carnège: the commander
* Hugues de Bagratide: an officier
* Paul Amiot: DAmade

== References ==
 

== External links ==
*  
*   at the Films de France

 

 
 
 
 
 
 
 
 


 