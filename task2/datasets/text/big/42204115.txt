False as Water
 
{{Infobox film
| name           = False as Water
| image          = Falsk som vatten.jpg
| caption        = Swedish poster
| director       = Hans Alfredson
| producer       = Hans Alfredson Waldemar Bergendahl
| writer         = Hans Alfredson
| starring       = Malin Ek
| music          = Stefan Nilsson
| cinematography = Jörgen Persson (cinematographer)|Jörgen Persson
| editing        = Jan Persson
| studio         = Svensk Filmindustri Svenska Ord
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 102 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Best Director Best Actress at the 21st Guldbagge Awards.   

==Cast==
* Malin Ek as Clara
* Sverre Anker Ousdal as John
* Marie Göranzon as Anna (as Mari Göranzon)
* Stellan Skarsgård as Stig
* Örjan Ramberg as Carl
* Lotta Ramel as Fia
* Philip Zandén as Jens (as Philip Zanden)
* Catharina Alinder as Tina
* Martin Lindström as Lill-John
* Magnus Uggla as Schüll, shop owner Folke Lind as The Old Man
* Ing-Marie Carlsson as Göteborgskan
* Gunilla Olsson as New Tenant (as Gunilla Ohlsson)
* Jan Wirén as Doctor (as Jan Wiren)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 