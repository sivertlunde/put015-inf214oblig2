Vaiki Vanna Vasantham
{{Infobox film
| name = Vaiki Vanna Vasantham
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer =
| writer = Balachandra Menon
| screenplay = Balachandra Menon Madhu Srividya Ambika Raghuraj Shyam
| cinematography = Vipin Das
| editing = G Venkittaraman
| studio = Uma Arts
| distributor = Uma Arts
| released =  
| country = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Ambika and Raghuraj in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
   Madhu 
*Srividya  Ambika 
*Raghuraj 
*Sukumari 
*Sankaradi 
*Rajan Sankaradi
*Dhanya
*Kailasnath
*Oduvil Unnikrishnan 
*T. P. Madhavan 
 

==Soundtrack==
The music was composed by Shyam (composer)|Shyam. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ee vada kando sakhakkale || Jayachandran || Sreekumaran Thampi || 
|- 
| 2 || Kaalindi Vilichaal Vilikelkum kanna || Vani Jairam || Sreekumaran Thampi || 
|- 
| 3 || Ore Paathayil || P. Susheela|Susheela, Jayachandran || Sreekumaran Thampi || 
|- 
| 4 || Oru Poovirinju || Vani Jairam || Sreekumaran Thampi || 
|- 
| 5 || Vaasanayude theril || K. J. Yesudas, Vani Jairam, Chorus || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 