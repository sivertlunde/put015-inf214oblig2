Investigation of a Flame
 
Investigation of a Flame is a 2001 documentary by  , May 3, 2001. 
 Daniel and John Hogan, Thomas Lewis, Marjorie and Tom Melville.   The film also includes commentary by historian Howard Zinn.

== Reviews ==
Francis X. Clines of The New York Times described the film as "a documentary about the protest events that made Catonsville, Maryland, an unpretentious suburb on the cusp of Baltimore, a flash point for citizens resistance at the height of the war. . . .  Sachs found assorted characters still firm to fiery on the topic. She came to admire the consistency of the mutual antagonists in an argument that still rages (today)."   Michael OSullivan of The Washington Post wrote that Sachs "uses a mosaic technique and seemingly random shots of plants and houses to create a moody, subjective portrait of an era as much as a group of people."   Molly Marsh of Sojourners magazine called the film "wonderfully intimate; Sachs brings the camera within inches of her subjects faces, capturing their thoughtful reminisces and personal regrets.   Fred Camper of the Chicago Reader called it a "poetic essay" with "no omniscient narrator talking down to the viewer . . . while "images like a newspaper going in and out of focus remind us that shifting contexts alter our understanding of complex events." 
 talking heads file footage by interspersing impressionistic shots. (The film) provides a potent reminder that some Americans are willing to pay a heavy price to promote peace." 

== Awards ==
* San Francisco International Film Festival
* New Jersey Film Festival
* Ann Arbor Film Festival
* First Prize Documentary Athens Film Festival
* Vermont Film Fest. Social Issue Doc. Award

== References ==
 

==External links==
*  
*  
* 
*  
*  , All Things Considered, May 17, 2008 (transcript, includes excerpt from film)
*  , Maryland Film Festival, May 3, 2001
* Ray Ellis,   at Blogcritics, October 22, 2006

 
 
 
 
 
 