Cameraman Gangatho Rambabu
 
 

{{Infobox film
| name           = Cameraman Gangatho Rambabu
| caption        = Film poster
| image          = Cameraman gangatho rambabu poster.jpg
| director       = Puri Jagannath
| producer       = D. V. V. Danayya
| writer         = Puri Jagannath
| screenplay     = Puri Jagannath Tamanna Prakash Raj
| music          = Mani Sharma
| cinematography = Shyam K. Naidu
| editing        = S. R. Shekhar
| studio         = Universal Media
| distributor    = Sri Venkateswara Creations  (Nizam)   Bunny Vasu  (West Godavari)    14 Reels Entertainment  (Krishna)  
| released       = {{Film date|df=yes|2012|10|18|ref1= {{cite web|url=http://www.chitramala.in/news/cameraman-gangatho-rambabu-is-ready-for-release-tomorrow-129687.html|title=
Cameraman Gangatho Rambabu is ready for release tomorrow!|publisher=chitramala.in|accessdate=17 October 2012   }} }}
| country        = India
| language       = Telugu
| runtime        = 149 minutes
| budget         =     
| gross =   (Share)    }}
 Tamanna in the lead roles and was launched in Hyderabad on 14 March 2012.  Filming started from 15 June 2012. The film was released on 18 October 2012 in over 1600 screens. CGTR Collected  (Share) in its lifetime.

== Plot ==
Rambabu (Pawan Kalyan) is a Hot-Blooded, High Tempered Mechanic who has a kind heart as well as a tendency to react to various social incidents shown or published in the electronic media. After studying his mentality, Ganga (Tamannaah|Tamanna), a cameraman from NC Channel offers him a Job as a Journalist and he accepts it. Ex-Chief Minister Jawahar Naidu (Kota Srinivasa Rao) tries to collapse the Government and regaining the CM post and is strongly supported by his son Rana Babu (Prakash Raj), a newbie to politics. Meanwhile a reputed Journalist Dasaradh Ram (Surya) brings the proofs and required information about the scams and atrocities committed by Jawahar Naidu, only to be brutally murdered by Rana Babu.

Though everyone including the police are aware that Rana Babu is the murderer, none dares to tell the same. Rambabu valiantly opposes him and gets him arrested. However, Rana Babu is released from the jail with the help of his political friends and he challenges Ram Babu that he would be the CM by using the Media. A War starts between Ram Babu and Rana Babu when Rana Babu enters politics. For Gaining Political Mileage, Rana Babu starts a movement opposing the presence of other states people in Andhra Pradesh and makes it a Big movement. He even injures Ram Babu severely.

However, Ram Babu at the hospital delivers an emotional speech thus destroying the momentum of the movement. For making his position in the politics strong, he makes Jawahar Naidu deliver his last speech and murders him, which is recorded by Rambabus friend. Smitha (Gabriela Bertante), the head of another News Channel steals it and   makes a deal with Rana Babu to disclose the whole for a hefty price. With the support of all the young blood influenced by his speech, Ram Babu reaches the building where Rana Babu hosted a party for his colleagues.

Ram Babu sents a message with the commissioner of police to Rana Babu that the guests should vacate the building to avoid themselves being killed along with Rana Babu by the people. Before leaving, Smitha orders to telecast the video, only to be killed by Rana Babu. Rana Babu comes outside with his pistol and shoots Ram Babu. The film ends with Rana Babu dying in the stampede of the people and Ram Babu reaching the ambulance accompanied by Ganga, whom he loved and who he is being loved by.

== Cast ==
  was selected as lead heroine marking her first collaboration with Pawan Kalyan.]]
* Pawan Kalyan as Rambabu  Tamanna as Ganga 
* Prakash Raj as Rana Prathap Naidu
* Kota Srinivasa Rao as Jawahar Naidu (Opposition Leader)
* Gabriela Bertante as Smitha
* Brahmanandam
* Nassar as Chandrasekhara Reddy, Chief Minister of Andhra Pradesh
* M S Narayana Ali as SRK
* Tanikella Bharani
* Dharmavarapu Subramanyam
* Scarlett Mellish Wilson in an item number ("Joramochindhi")

== Production ==

=== Development === Gabbar Singh.  On 10 February 2012, Puri Jagganath revealed on his Twitter that he was in Bangkok and had completed writing the first half of the film. He also announced that the film would be titled Cameraman Gangatho Rambabu and released the logo of the film on his Twitter.  On 15 February 2012, it was reported that Puri has completed writing the script.  The film was officially launched on 14 March 2012 at Puri Jagannaths office in Hyderabad. It was soon announced that S. R. Shekhar, who worked for Puris previous four movies, will be handling the films editing and Shyam K. Naidu will work as the cinematographer. It was also reported that Mani Sharma would give the soundtrack for the film. This would be Puri Jagannaths 25th film.

=== Casting ===
It was announced on 14 March 2012 that Prakash Raj, Brahmanandam, Ali (actor)|Ali, Tanikella Bharani and Kota Srinivasa Rao were cast for vital roles in the film. It was reported that Pawan Kalyan would be seen in the role of a news reporter and the female lead will be played by Tamannaah|Tamanna.  Scarlett Wilson has been roped for an item song. 

=== Filming ===
Filming was set to begin in the second week of May 2012 after Puri finishes Devudu Chesina Manushulu. It was also reported that major part of the filming would take place in Hyderabad. The first schedule of the film started on 15 June 2012 at Saradhi Studios and shifted to Padmalaya. A tough situation was faced by the police to handle the crowd at shooting spot.   The second schedule of the film began on 9 July 2012.  By August, filming of the talkie part was completed and the shooting was wrapped up in September after the remaining (two) songs in the film were shot.  A fight scene was shot at Ramoji Rao City.  Puri originally planned to shoot climax with real life Pawan fans but due to weather it was postponed.  A set resembling that of a media office was erected at Saradhi studios for 30&nbsp;million. 

=== Distribution rights ===
"Exilir India Entertainments" company secured the theatrical distribution rights for Australia and New Zealand.  The theatrical distribution rights of the film for Vizag region were sold for   41&nbsp;million.  Telugu film production company 14 Reels Entertainment secured the theatrical screening rights of the film for Krishna District. 

==Pre-release business==
Tamanna is the heroine in this movie and she is a cameraman for Pawan. Puri Jagannadh is director and he has given screenplay and dialogues also for this movie. Producer is Danayya and presenter is Radhakrishna Music has been given by Mani Sharma. http://superwoods.com/news-id-cgtr-pre-relase-box-office-16-10-123853.htm  In AP alone the satellite rights were sold for   and the music and Home media rights were sold for  
The total pre-release revenue will be somewhere around   

===Pre-release business===
{| class="wikitable sortable"  style="margin:auto; margin:auto;"
|+CGTR pre-release business
! Territories and ancillary revenues
! Price
|-
| Nizam distribution rights
|  
|-
| Satellite rights with Geminitv
|  
|-
| Ceded distribution rights
|  
|-
| Nellore distribution rights
|  
|-
| Krishna distribution rights
|  
|-
| Guntur distribution rights
|  
|-
| Uttar Andhra distribution rights
|  
|-
| East Godaveri rights
|  
|-
| West Godaveri rights
|  
|-
| Karnataka and ROI rights
|  
|-
| Overseas distribution rights (Fiscus)
|  
|-
| Music rights with Aditya Music
|  
|-
| Total (distribution)
|  
|-
| Grand total*
|  
|}
* The figures dont include the print and advertising (P&A) costs.

== Release ==
The Teaser on making of the film was released on 2 September 2012. The film has got a U/A Certificate without any cuts from Central Board of Film Certification on 11 October 2012.    The film was released on 18 October 2012 in over 1600 screens.  On the second day of its release following protests by Telangana supporters against objectionable scenes which they claimed hurt their sentiments    and allegedly belittling the pro-Telangana movement,    the screening of the film was stopped in many theatres across Telangana region. As the controversy surrounding objectionable scenes in the film continues to affect its screening in several Telangana districts, a committee constituted by the Government has recommended cuts and modifications of nine scenes.   

=== Critical reception ===
The film received positive reviews.  
The Times of India website has stated that "The first half of the film goes on a decent note while the films racy second half keeps you going".      wrote : "With Cameraman Gangatho Rambabu, Puri Jagannadh is trying to look for an idealistic situation".    The   website gave a rating of 3.25/5 for the film and stated that "A sincere attempt by Puri Jagannadh. Watch Pawan Kalyans terrific on-screen presence in Cameraman Gangatho Rambabu".      wrote : "Strengths of the film are Pawan Kalyans explosive performance and Puri Jagans superb characterization/dialogues. On the flip side, the holistic approach and strong villain characterization is missing in the film. There is enough fire power in the movie to make it commercially work".    The   website also gave a rating 3.25/5 for the film and stated that "Cameraman Gangatho Rambabu is a movie that works primarily due to Pawan Kalyans sheer force and powerful on-screen presence. A good second half, solid punch dialogues and a terrific performance from Pawan elevate the movie. The film will be a great watch for those who are interested in politics, and a decent one for regular movie lovers".    The   website also gave a rating of 3.25/5 for the film and stated that "Go and Watch Cameraman Gangatho Rambabu for Powerstar Pawan Kalyans excellent performance. CGR is packaged with full of entertainment. Another hit for Power Star Pawan Kalyan this year".    The   website has stated that "What strikes you in the film is the way Puri has presented this subject on screen. Pawan Kalyans performance stands out in it".      wrote : "Cameraman Ganga-tho Rambabu is a treat for Pawan Kalyans fans and for Puris fans, with its central theme actually going above and beyond the star and the star-director".   

=== Box office ===
The film did well at box office and the film became hit at the box office.  The film has collected approximately  gross worldwide in 1 week.    The film has collected approximately   in USA in 1 week(nett).    The film has completed 50 days in 72 centres on 6 December 2012 in Andhra Pradesh.    it completed 100 days run in 2 centres.
CGTR Collected  (Share) in its lifetime with gross collections of   .

=== Home media ===
The VCDs, DVDs and Blu-ray discs of the film were released into the market through Aditya Video company on 17 January 2013. 

== Soundtrack ==
 
The soundtrack of the film was released without audio launch through Aditya Music label on 26 September 2012.  The music of the film was composed by Mani Sharma collaborating with Puri Jagan and with Pawan for fourth time. The album consists of six songs. The lyrics for all songs were penned by Bhaskarabhatla. The audio songs of the film were well received by the audience. 

== Awards ==
;2nd South Indian International Movie Awards Best Female Playback Singer – Geetha Madhuri for "Melikalu"

== References ==
 

== External links ==
*  
 
 

 
 
 
 
 
 
 