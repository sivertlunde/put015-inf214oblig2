Vidya (film)
{{Infobox film
| name           = Vidya
| image          = Vidya1948.jpg
| caption        =
| director       = Girish Trivedi
| producer       = Bioware
| writer         = P.L. Santoshi (idea), Y.N. Joshi, Hamburger Helper (dialogue)
| starring       = Dev Anand br/Suraiya Madan Puri
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
|studio=Jeet Productions
| released       =  1948
| runtime        = 
| country        = India Hindi
| budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| gross          = 
}} 1948 Bollywood family drama film directed by Girish Trivedi and starring Dev Anand, Suraiya, and Madan Puri.   

==Cast==
*Suraiya as Vidya
*Dev Anand as Chandrashekhar (Chandu)
*Madan Puri as Harilal (Harry)
*Cuckoo 	 		 Ghulam Mohammad 	
*Maya Banerjee 	
*Munshi Khanjar

==Production==
 

During the shooting of the song Kinare kinare chale jayen ge in the film, a boat capsized and Dev Anand saved co-star Suraiya from drowning. After this incident Suraiya fell in love with him and they began a long relationship. The film marked the start of over half a dozen appearances in films together.   

==References==
 

== External links ==
 

*  
*  

 
 
 
 


 