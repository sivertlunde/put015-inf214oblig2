Old Dogs (film)
 
{{Infobox film
| name           = Old Dogs
| image          = Old dogs poster.jpg 
| alt            =  
| caption        = Theatrical release poster
| director       = Walt Becker
| producer       = Andrew Panay Robert Levy Peter Abrams  David Diamond David Weissman
| starring       =  John Travolta Robin Williams Kelly Preston Seth Green Ella Bleu Travolta Lori Loughlin Matt Dillon
| music          = John Debney
| cinematography = Jeffrey L. Kimball
| editing        = Tom Lewis Ryan Folsey 
| studio         = Walt Disney Pictures Tapestry Films Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 88 minutes 
| country        = United States
| language       = English
| budget         = $35 million
| gross          = $96.8 million   
}}
 ensemble comedy film directed by Wild Hogs  Walt Becker and starring John Travolta and Robin Williams with an ensemble supporting cast played by Kelly Preston, Matt Dillon, Justin Long, Seth Green, Rita Wilson, Dax Shepard, and Bernie Mac. It was released in theaters on November 25, 2009 and was released on DVD March 9, 2010.

The movie is dedicated to both Bernie Mac (who died in August 2008 and had his final acting role in the film) and Jett Travolta (John Travoltas son who died in January 2009).

Old Dogs received poor reception from film critics.  ."   Reviews in the Milwaukee Journal Sentinel and AV Club said the movie was not recommended for adults or children.  

Despite the negative criticism, it was a box office success, beating its budget.  

At the 30th Golden Raspberry Awards ceremony, Old Dogs was nominated in four categories: Worst Picture, Worst Actor for John Travolta, Worst Supporting Actress for Kelly Preston and Worst Director for Walt Becker, but "lost" in all categories.

Canadian rocker Bryan Adams wrote the theme song for the film, "Youve Been a Friend to Me".

==Plot==
Dan Rayburn (Robin Williams) and Charlie Reed (John Travolta) are best friends and co-owners of a successful sports marketing firm. Seven years prior, Dan, recently divorced, married Vicki (Kelly Preston) after being whisked away by Charlie for a tropical vacation. The wedding, however, is short lived. Seven years later, Vicki resurfaces to tell Dan that their short marriage resulted in something he never suspected: twins Zach (Conner Rayburn) and Emily (Ella Bleu Travolta).

Vicki, facing jail time for her work as an environmental activist, asks Dan to take care of the kids while she does her time. Thinking this might be his chance to get back with Vicki, Dan agrees, but only if Charlie will help him since neither have any experience taking care of kids. At the same time, the two must finalize a huge marketing deal with a Japanese company; something theyve always dreamed of, but will take all of their talents to clinch.

Because Dans condo does not allow children, he has to board with Charlie. Whilst this is happening, Charlie and Dan are close to securing the biggest account in the history of their careers with a Japanese corporation. Charlie and Dans attempts to take care of the kids are well-intentioned, but very misguided. On a trip with the kids to an overnight camp, a hard-nosed camp instructor (Matt Dillon) becomes convinced that Dan and Charlie are homosexual partners. The trip ends with a bang after Dan accidentally sets a beloved statue of the camps founder on fire.

The kids then proceed to spill and replace Charlie and Dans prescriptions, mixing them up in the process. Dan then must play a game of golf with the Japanese executives while experiencing extreme side effects and Charlie tries to woo Amanda (Lori Loughlin) with a face frozen by the pills.
 flamboyant childrens entertainer, who is famous around the world. Jimmy comes by and straps Dan and Charlie in motion control puppet suits so Charlie can help Dan make all the right moves with his daughter while having a tea party. The suits malfunction, but Dan speaks from the heart, winning over Emily but his speech makes Jimmy emotional. Everything is great with Vicki as she returns home upon having served time in jail. However, the guys have sealed their Japanese deal, sending junior associate Craig (Seth Green) to Tokyo. When Craig goes missing after arriving there, Charlie and Dan must fly to Tokyo themselves to work. Dan must leave the kids and Vicki despite his (and their) desire to be a family.

Once in Tokyo, Dan realizes that what he really wants is to be a good father. He leaves the meeting without sealing the deal, rushing with Charlie to Vermont for the kids birthday party. They arent able to get into the Burlington Zoo in time and are forced to break in with the help of Craig. However, they mistakenly wind up in the gorilla enclosure. Though Dan and Charlie escape, Craig is captured by the gorilla (which takes a strong liking to him).

Dan then pays a birthday party performer hired by Vicki to use his jet pack and suit, flies into the ceremony and wins his kids back over. When the jet pack stops working in mid-air, he is taken to an ambulance on a stretcher. One year later, Dan and Vicki are together, Charlie has married Amanda, and Craig has become like a new "uncle" to the kids.

==Cast==

===Primary===
* John Travolta as Charlie "Chuck" Reed
* Robin Williams as Daniel "Dan" Rayburn
* Kelly Preston as Vicki Greer
* Seth Green as Craig White
* Lori Loughlin as Amanda
* Ella Bleu Travolta as Emily Greer
* Conner Rayburn as Zachary "Zach" Greer

===Minor===
* Sab Shimono as Yoshiro Nishamura (as Saburo Shimono)
* Dax Shepard as Child Proofer Gary (uncredited)
* Luis Guzmán as Child Proofer Nick (uncredited)
* Bernie Mac as Jimmy Lunchbox
* Matt Dillon as Troop Leader Barry
* Rita Wilson as Jenna
* Justin Long as Troop Leader Adam (uncredited)
* Ann-Margret as Martha
* Laura Allen as Kelly
* Amy Sedaris as Condo Woman
* Kevin W. Yamada as Riku
* Bradley Steven Perry as Soccer Kid
* Dylan Sprayberry as Soccer Kid
* Paulo Costanzo as Zoo Maintenance (uncredited)
* DeRay Davis as Zoo Security Guard (uncredited)
* Paul Thornton as Restaurant Patron (uncredited)
* Residente as Tattoo Artist

===Muppet performers===
The following have performed the puppets in Jimmy Lunchboxs show and are credited as "Muppet":

* Bruce Connelly
* Josh Cohen
* Joe Kovacs  John Kennedy Edward Noel MacNeal Matt Vogel

Four of the puppets identified in Jimmy Lunchboxs show are Bozark the Elephant from Animal Jam, Beak the Bird and YesNo from the proposed series Muppetmobile, and Scales the Dragon from the pilot to Little Mermaids Island.

==Reception==

===Critical response===
Old Dogs was panned by critics. On Rotten Tomatoes, the film holds a rating of 5%, based on 108 reviews, with an average rating of 2.3/10. The sites consensus reads, "Its cast tries hard, but Old Dogs is a predictable, nearly witless attempt at physical comedy and moral uplift that misses the mark on both counts."  {{cite web 
| title = Old Dogs (2009) 
| url = http://www.rottentomatoes.com/m/10009596-old_dogs/ 
| work = Rotten Tomatoes 
| publisher = Flixster 
| accessdate = 2010-01-17 
}} 
  The film was ranked number three on their list of the ten most moldy films of 2009. {{cite web 
| title = ROTTEN TOMATOES: 11th Annual Golden Tomato Awards: Moldy 
| url = http://www.rottentomatoes.com/guides/rtawards/moldy/ 
| work = Rotten Tomatoes 
| publisher = Flixster 
| accessdate = 2010-01-17 
}}
 
At Metacritic, Old Dogs received an aggregated rating of 19 out of 100, based on 22 reviews, indicating "overwhelming dislike."  

Film critic Roger Ebert gave Old Dogs a rating of one star out of a possible four.      Ebert opened his review commenting, "Old Dogs is stupefying dimwitted. What were John Travolta and Robin Williams thinking of? Apparently their agents werent perceptive enough to smell the screenplay in its advanced state of decomposition". 

The Salt Lake Tribune gave Old Dogs a rating of zero stars out of a possible four, and criticized the film for "hammy acting and sledgehammer editing".    Film critic Roger Moore of The Orlando Sentinel gave Old Dogs a rating of one and a half stars out of a possible four.    "Trashing Old Dogs is a bit like kicking a puppy. But here goes. The new comedy from some of the folks who brought us Wild Hogs is badly written and broadly acted, shamelessly manipulative and not above stopping by the toilet for a laugh or two," wrote Moore. 

Bill Goodykoontz of  , Goodykoontz gave the film a rating of one and a half stars out of a possible five. 

Writing for the   wrote, "Too bad this shrilly tuned comedy doesnt demand more than clock-punching effort from everyone involved."  The Telegraph savaged the film, saying, "Old Dogs is so singularly dreadful it halts time, folds space and plays havoc with the very notion of the self."  He added to the review, "Being a film critic is a wonderful job, but there are weeks when the bad film delirium strikes and we’d all be better off in straitjackets. A colleague opined to me the other day that this might be the deadliest run of releases in his 20-year history on the job, and I can completely see that." He also said, "Youd have to hate your family to take them to this!" He gave the film zero stars.
 cameos in the film, "A child of 5 can see that these brief appearances serve to pad a gauze-thin script."  The review concluded, "Old Dogs may not be good. But the sight of pesky penguins pecking Travolta and Green in the embrace of an unlikely partner makes it just good enough."  Pete Hammond of Boxoffice (magazine)|Boxoffice gave the film 3/5 stars, and concluded, "Old Dogs may not reach the box office heights of Wild Hogs but its fun family friendly attitude should guarantee a healthy holiday haul." 

===Box office=== The Blind Side, 2012 (film)|2012, and Ninja Assassin.     The film came in fourth in its second day with $4.1 million, for a two-day pickup of $7.2 million.  The film remained in fourth place for its third day, with a box office take of $6.8 million.    Overall, the film grossed $96,753,696 worldwide on a budget of $35,000,000.  

The movie was also a moderate success on DVD, gaining more than $20,000,000 (20 million dollars) domestically during its first two months of release. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 