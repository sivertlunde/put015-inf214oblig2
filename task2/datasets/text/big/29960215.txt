Santa Santita
 
{{Infobox film
| name           = Santa Santita
| image          = Santa Santita movie cover.jpg
| caption        = Theatrical poster
| director       = Laurice Guillen
| writer         = Jerry Gracio
| starring       =  
| studio         = Unitel Pictures International
| released       =  
| runtime        = 113 minutes
| country        = Philippines
| language       = Filipino
| gross          = $6,470  (Domestic only)  
}} Filipino film starring Angelica Panganiban and Jericho Rosales, directed by Laurice Guillen. The film follows Malen, the daughter of an intercessor, her love affair with a hustler and gigolo, and her confrontation of the evil within her life. Based on a script by Jerry Gracio, the film was picked up by Guillen in 2000 but did not start production because an appropriate actress for Malen could not be found. After picking Panganiban, Guillen began work, and the eventual film was released on November 17, 2004, in the Philippines and March 11, 2005, internationally. Receiving good reviews from Variety and the Philippine Daily Inquirer, Santa Santita was one of only two films to be rated as Grade A by the Cinema Evaluation Board of the Philippines, and represented the Philippines at the Asiaticafilmediale festival in Italy and the Bangkok International Film Festival.

==Plot==
Malen (Angelica Panganiban) is the daughter of Chayong (Hilda Koronel), a widowed intercessor at Quiapo Church in Manila.    Selling religious charms as a way of meeting men, Malen falls for Mike (Jericho Rosales), a gigolo and hustler.    Mike hustles both for his own survival and that of his son, but also because "he knows essentially hes just strong enough and unscrupulous enough" to do so.    After her mother takes umbrage at Malens assumed promiscuity, she moves out of Chayongs house and stays with Mike. When her mother then dies of a heart attack, Malen feels guilt and becomes an intercessor despite having previously sworn the profession off. 

Initially met with mistrust by her colleagues, one of whom exclaims that "She is defiling prayer", Malen heals a child with a hole in its heart through her prayers,  despite not even praying seriously.  This meets with additional hostility, both from the other intercessors and the clergy,    and when Malen dreams of having stigmata, she is forced to confront the problems with her own life, something which comes to a climax when she is asked to bring Mikes dead son back to life.    A side plot involves Father Tony (Johnny Delgado), an alcoholic priest who lives with Mike. Initially his drinking partner, Mike begins to taunt the priest about his failures as the film goes on.  At the end of the film, Mike is sent to prison for killing a man after a traffic accident, while Tonys interactions with Malen convince him that he is addicted to alcohol, prompting him to return to his parish and continue serving as a priest.   

==Cast==
{| class="wikitable"
|-
!Actor|| Role|| Notes
|-
| Angelica Panganiban|| Malen || 
|-
| Jericho Rosales|| Mike ||  
|-
| Hilda Koronel|| Chayong ||  
|-
| Johnny Delgado|| Father Tony ||  
|}

==Production==
The script was written by Jerry Gracio, influenced by the poem The Hound of Heaven, and won first prize at the scriptwriting competition hosted by the Film Development Council of the Philippines in 2000.    This attracted the attention of Laurice Guillen, who bought the script (the first time in her career that she had done so) and signed on to direct the eventual film. The script was brought to Star Cinema, who became interested in producing it, but production stalled due to difficulties finding an appropriate actress to play Malen. Guillen stated that "Since she was a Magdalene, she had to be sexy but she also had to be much more than that....I was shown many sexy actresses, some of them name stars, but I couldnt find my Santa-Santita. I felt that the role should go to someone relatively new, who had no fixed image, so that there would be no expectations". An additional problem was over the religious content of the film, which met with controversy.    Guillen eventually found Angelica Panganiban to play Malen, and after five years without production, work on the film began for Unitel Pictures International,  whose CEO stated that "the majority of Filipinos, the so-called masses, are not the targets of this movie. Those who frequent the cineplexes and look for fresh material in the movies are".  The title, Santa Santita, translates as "saint who is not really a saint". 
 Iguig and dream sequences in Currimao, Ilocos Norte. Guillen noted the stress of recording in Quiapo, due to the large number of extras, the already crowded nature of the area and the small size of the time periods during which they were allowed to film in the church itself. 

==Release==
The film was previewed to good reviews,  and after an initial release in the Philippines on November 17, 2004,    opened in cinemas in Manhattan, San Diego and Honolulu on March 11, 2005, as Magdalena, The Unholy Saint.  It was given good reviews by both Filipino and American magazines and newspapers; Variety (magazine)|Variety magazines reviewer noted that "Performances are solid, and former child actress Panganiban is impressive as a young woman who finds herself with a vocation she never asked for.... Guillens helming is slick and confident, and the HD-sourced photography and other technical credits are of good quality",  and the Philippine Daily Inquirer reported that "Gorgeously crafted, Santa Santitas strength lies in its illuminating take on mans neverending search for meaning, and in the thoughtful, truthful characterization of its leads". 

Other reviewers were more cautionary; the New York Times wrote that "the aim of the filmmaker seems unclear, with Magdalena at first celebrated for her humanness and then exalted for her sudden saintliness" and describing the story as "wearyingly eventful".  The Manila Bulletin noted that while the film is about belief (or lack thereof) in miracles, "the effort to explore this issue is not brought into fulfilling fruition. We keep on hoping something else more significant would happen later in the film to invoke the viewers own feelings about faith and an all-seeing, all-merciful God, but this never comes". Although Rosales and Panganiban were praised for their acting, the reviewer felt that there was "just no combustible chemistry between the two of them". 

The Cinema Evaluation Board of the Philippines gave the film a Grade A rating, making it one of only two films to qualify into that category at that point,  and the only film to qualify in 2004.  This rating gave Santa Santita a 100&nbsp;percent rebate on amusement taxes, and is only awarded to films that the Board feels can "revitalize the moribund industry".  The movie represented the Philippines at the Asiaticafilmediale festival in Italy,  and was also shown at the Sine! Sine! Film Fest in San Francisco.  It was a finalist in the Catholic Mass Media Awards,  and represented the Philippines at the Bangkok International Film Festival. 

==References==
 

==External links==
* 

 
 