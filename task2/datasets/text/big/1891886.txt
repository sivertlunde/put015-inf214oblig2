V for Vendetta (film)
  
 
 
{{Infobox film
| name           = V for Vendetta
| image          = vforvendettamov.jpg
| image_size     =
| caption        = Theatrical release poster
| alt            =
| director       = James McTeigue
| producer       = {{plainlist |
* Joel Silver Larry Wachowski Andy Wachowski Grant Hill
}}
| screenplay     = Larry Wachowski Andy Wachowski
| based on       =  
| starring       =  {{plainlist|
* Natalie Portman
* Hugo Weaving
* Stephen Rea
* John Hurt
}}
| music          = Dario Marianelli
| cinematography = Adrian Biddle Martin Walsh
| studio         = {{plainlist|
* Virtual Studios
* Silver Pictures Anarchos Productions
}}
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 132 minutes 
| country        = United States Germany  
| language       = English
| budget         = $54 million 
| gross          = $132.5 million 
}}
 political thriller Wachowski Brothers, Vertigo V graphic novel David Lloyd. fascist regime dystopian United Kingdom and exterminated its opponents in concentration camps. Natalie Portman plays Evey Hammond|Evey, a working class girl caught up in Vs mission, and Stephen Rea portrays the detective leading a desperate quest to stop V.
 From Hell The League of Extraordinary Gentlemen (2003), declined to view the film and asked not to be credited.
 libertarians and anarchists have used it to promote their beliefs. David Lloyd stated: "The Guy Fawkes mask has now become a common brand and a convenient placard to use in protest against tyranny – and Im happy with people using it, it seems quite unique, an icon of popular culture being used this way." 

==Plot== fascist police Political opponents, Guy Fawkes-masked Houses of Parliament, which he promises to destroy. During the broadcast, the police attempt to capture V. Evey helps V escape but is knocked unconscious.

V takes Evey to his home, where she is told she must remain until November 5 of the following year. After learning that V is killing government officials, she offers to help kill a perverted priest by acting as a child prostitute. She escapes to the home of her boss, comedian and talk show host Gordon Deitrich. In return for Evey trusting him with her safety, Gordon reveals prohibited materials including subversive paintings, an antique Quran, and homoerotic photographs by Robert Mapplethorpe. Gordon explains that he acts the part of a womanizer to conceal his homosexuality; if he were honest he would be executed. Finch learns that V is the result of experimentation on humans; his targets tortured him while he was detained. After Gordon performs a satire of the government on his show, his home is raided and Evey is captured. She is imprisoned and tortured for information about V, with her only solace being a note written by actress Valerie Page, arrested for being a lesbian.

Evey is told she will be executed unless she reveals Vs location, but when she says she would rather die she is released. Her imprisonment and torture were staged by V to free her from her fears. The note was real, passed from Valerie to V when he was imprisoned. He also informs her that Deitrich had been executed when the Quran was found in his home. While Evey initially hates V for what he did to her, she realises she has become a stronger person. She leaves him, promising to return before November 5.
 bioweapons program Adam Sutler, resulted in the creation of the "St. Marys Virus" and its release in a false flag terrorist attack. The deaths of over 100,000 people and the resulting fear enabled the Norsefire Party to win the next election and turn the country into a totalitarian state under Sutlers rule as High Chancellor. Finch later discovers that Rookwood has been dead for years; the man he met was V in disguise, and Finch initially disbelieves his story.

As November 5 nears, Vs distribution of thousands of Guy Fawkes masks causes chaos in the UK and the population questions Party rule. On the eve of November 5, Evey visits V, who shows her an explosive-laden train in the abandoned London Underground, set to destroy Parliament. He leaves it to Evey to decide whether to use it. V meets Party Leader Creedy, with whom he made a deal to surrender in exchange for Sutlers execution. After Creedy executes Sutler, V reneges on his deal and kills Creedy and his men. Mortally wounded, he returns to Evey to thank her and tell her he loves her before dying.

As Evey places Vs body aboard the train she is found by Finch. Disillusioned by the Partys regime, Finch allows Evey to send the train. Thousands of unarmed Londoners wearing Guy Fawkes masks march towards Parliament. Without orders, the military allows the crowd to pass. Parliament is destroyed as Finch asks Evey for Vs identity, to which she replies, "He was all of us."

==Cast==
  in 2012. He portrayed V in the film, keeping his face hidden by his mask or obscured throughout.]]
* Hugo Weaving as V (comics)|V:
:A charismatic and skilled freedom fighter who was the unwilling subject of experimentation by Norsefire. James Purefoy was originally cast as V, but left six weeks into filming, citing difficulties wearing the mask for the entire film.    He was replaced by Weaving, who had previously worked with Joel Silver and the Wachowski Brothers on The Matrix (franchise)|The Matrix series. 
* Natalie Portman as Evey Hammond: dialectologist Barbara The Weather Underground and read the autobiography of Menachem Begin.  Portman received top billing for the film. Portmans role in the film has parallels to her role as Mathilda Lando in the film Léon (film)|Léon.  According to Portman: "the relationship between V and Evey has a complication   the relationship in that film." Portman also had her head shaved on screen during a scene where her character is tortured.   
* Stephen Rea as Eric Finch:
:Finch is the lead inspector in the V investigation, who, during his investigation, uncovers an unspeakable government crime. When asked whether the politics attracted him to the film, Rea replied "Well, I dont think it would be very interesting if it was just comic-book stuff. The politics of it are what gives it its dimension and momentum, and of course I was interested in the politics. Why wouldnt I be?"    High Chancellor Adam Sutler: film adaptation of Nineteen Eighty-Four.      

Stephen Fry portrays Gordon Deitrich, a closeted homosexual and talk/comedy show host. When asked in an interview what he liked about the role, Fry replied "Being beaten up! I hadnt been beaten up in a movie before and I was very excited by the idea of being clubbed to death."    Also included in the cast are Tim Pigott-Smith as Peter Creedy, Norsefires Party leader and the head of Britains secret police (the "Finger");  Rupert Graves as Dominic Stone, Inspector Finchs lieutenant; Roger Allam as Lewis Prothero, a propagandist for Norsefire; John Standing as Anthony James Lilliman, a corrupt bishop at Westminster Abbey; and Sinéad Cusack as Dr. Delia Surridge, the former head physician at the Larkhill Detention Centre, now a coroner. Natasha Wightman portrays Valerie Page, a lesbian imprisoned for her sexuality. Imogen Poots portrays Valerie as a child.

==Themes==

===Sources===
 Percy and Keyes are Alexandre Dumas The Count of Monte Cristo, by drawing direct comparisons between V and Edmond Dantès. (In both stories, the hero escapes an unjust and traumatic imprisonment and spends decades preparing to take vengeance on his oppressors under a new persona.)          The film is also explicit in portraying V as the embodiment of an idea rather than an individual through Vs dialogue and by depicting him without a past, identity or face. According to the official website, "Vs use of the Guy Fawkes mask and persona functions as both practical and symbolic elements of the story. He wears the mask to hide his physical scars, and in obscuring his identity&nbsp;– he becomes the idea itself." 

As noted by several critics and commentators, the films story and style mirror elements from Gaston Lerouxs The Phantom of the Opera.       V and the Phantom both wear masks to hide their disfigurements, control others through the leverage of their imaginations, have tragic pasts, and are motivated by revenge. V and Eveys relationship also parallels many of the romantic elements of The Phantom of the Opera, where the masked Phantom takes Christine Daaé to his subterranean lair to re-educate her.     
 Big Brother. War is Peace. Freedom is Slavery. Ignorance is Strength" in Orwells book.    There is also the states use of mass surveillance, such as closed-circuit television, on its citizens. Valerie was sent to a detention facility for being a lesbian and then had medical experiments performed on her, reminiscent of the persecution of homosexuals in Nazi Germany and the Holocaust.  The name "Adam Sutler" and his hysterical style of speech are inspired by Adolf Hitler although Jews appear to have been replaced by Muslims as a target for persecution. Norsefire has replaced St Georges Cross with a national symbol similar to the modern Cross of Lorraine (both crossbars near the top). This was a symbol used by Free French Forces during World War II, as it was a traditional symbol of French patriotism that could be used as an answer to the Swastika#Nazi Germany|Nazis swastika.
 

===Modern fears of totalitarianism===
 
 corporate corruption avian flu biometric identification and ECHELON|signal-intelligence gathering and analysis by the regime.
 George W. rendition are Coalition of Friedrich Nietzsches concept of Will to Power.   
 News Corp." 

==Production and release==

===Development=== The Battle of Algiers as his principal influence in preparing to film V for Vendetta.   
 American neo-conservatism American liberalism". David Lloyd supports the film adaptation, commenting that the script is very good but that Moore would only ever be truly happy with a complete book-to-screen adaptation. 

===Filming===
V for Vendetta was filmed in   were filmed at the disused Aldwych tube station. Filming began in early March 2005 and principal photography officially wrapped in early June 2005.  V for Vendetta is the final film shot by cinematographer Adrian Biddle, who died of a heart attack on December 7, 2005. Guard, Howard
 , www.guardian.co.uk, January 18, 2006. Retrieved June 10, 2012 
 Big Ben Ministry of MP David David Davis due to the films content. However, the filmmakers denied Euan Blairs involvement in the deal,    stating that access was acquired through nine months of negotiations with fourteen different government departments and agencies. 

===Post-production=== Mildred Pierce The Lady of Shalott by John William Waterhouse and statues by Giacometti.
One of the major challenges in the film was how to bring V to life from under an expressionless mask. Thus, considerable effort was made to bring together lighting, acting, and Weavings voice to create the proper mood for the situation. In order to prevent the mask from muffling Weavings voice, a microphone was placed in his hairline to aid post-production, when his entire dialogue was re-recorded.   

===Promotion===
The cast and filmmakers attended several press conferences that allowed them to address issues surrounding the film, including its authenticity, Alan Moores reaction to it and its intended political message. The film was intended to be a departure from some of Moores original themes. In the words of Hugo Weaving: "Alan Moore was writing about something which happened some time ago. It was a response to living in Thatcherite Britain... This is a response to the world in which we live today. So I think that the film and the graphic novel are two separate entities." Regarding the films controversial political content, the filmmakers have said that the film is intended more to raise questions and add to a dialogue already present in society, rather than provide answers or tell viewers what to think. 

===Release=== July 7 July 21 bombing.  The filmmakers have denied this, saying that the delays were due to the need for more time to finish the visual effects production.    V for Vendetta had its first major premiere on February 13 at the Berlin Film Festival.  It opened for general release on March 17, 2006 in 3,365 cinemas in the United States, the United Kingdom and six other countries.   

==Reception==

===Commercial===
By December 2006, V for Vendetta had grossed $132,511,035, of which $70,511,035 was from the United States. The film led the U.S. box office on its opening day, taking in an estimated $8,742,504, and remained the number one film for the remainder of the weekend, taking in an estimated $25,642,340. Its closest rival, Failure to Launch, took in $15,604,892.  The film debuted at number one in the Philippines, Singapore, South Korea, Sweden and Taiwan. V for Vendetta also opened in 56 IMAX cinemas in North America, grossing $1.36 million during the opening three days.   

===Critical===
 
The critical reception of the film was generally positive.   states the film "works as a political thriller, adventure and social commentary and it deserves to be seen by audiences who would otherwise avoid any/all of the three". He added that the film will become "a cult favourite whose reputation will only be enhanced with age."    Andy Jacobs for the BBC gave the film two stars out of five, remarking that it is "a bit of a mess... it rarely thrills or engages as a story." 
 Best Actress award.  Rotten Tomatoes gives the film a 73% "Certified Fresh" approval rating.    The film was nominated for the Hugo Award for Best Dramatic Presentation, Long Form in 2007.  V was included on Fandomania  list of The 100 Greatest Fictional Characters.  Empire (film magazine)|Empire magazine (in 2008) named the film the 418th greatest movie of all time. 

===Political===
V for Vendetta deals with issues of homosexuality, criticism of religion, totalitarianism, Islamophobia and terrorism. Its controversial story line and themes have been the target of both criticism and praise from sociopolitical groups.

On April 17, 2006 the New York Metro Alliance of Anarchists organised a protest against DC Comics and Time Warner, accusing it of watering down the storys original message in favour of violence and special effects.       David Graeber, an anarchist scholar and former professor at Yale University, was not upset by the film. "I thought the message of anarchy got out in spite of Hollywood." However, Graeber went on to state: "Anarchy is about creating communities and democratic decision making. Thats what is absent from Hollywoods interpretation." 

Film critic Richard Roeper dismissed right-wing Christian criticism of the film on the television show Ebert and Roeper, saying that Vs terrorist label is applied in the film "by someone whos essentially Hitler, a dictator."   

LGBT commentators have praised the film for its positive depiction of homosexuals. Sarah Warn of AfterEllen.com called the film "one of the most pro-gay ever". Warn went on to praise the central role of the character Valerie "not just because it is beautifully acted and well-written, but because it is so utterly unexpected  ."   
 David Walsh from the World Socialist Web Site criticises Vs actions as "antidemocratic," calling the film an example of "the bankruptcy of Propaganda of the deed|anarcho-terrorist ideology;" Walsh writes that because the people have not played any part in the revolution, they will be unable to produce a "new, liberated society."   

Clay Duke, the perpetrator of the 2010 Panama City school board shootings, was reportedly obsessed with the film V for Vendetta. Prior to the shootings and his eventual suicide, Duke spray-painted a red V inside a red circle, a supposed allusion to his fascination with the graphic novel. 
 Chinese film censorship might be loosened.   

==Differences between the film and graphic novel== Vertigo imprint and in the United Kingdom under Titan Books. 
 liberalism and racial purity had been excised, whereas actually, fascists are quite big on racial purity." Furthermore, in the original story, Moore attempted to maintain moral ambiguity, and not to portray the fascists as caricatures, but as realistic, rounded characters. The time limitations of a film meant that the story had to omit or streamline some of the characters, details, and plotlines from the original story.  Chiefly, the original graphic novel has the fascists elected legally and kept in power through the general apathy of the public whereas the film introduces the "St. Marys virus", a biological weapon engineered and released by the Norsefire Party as a means of clandestinely gaining control over their own country.
 LSD in order to enter into a criminals state of mind. Gordon, a very minor character in both adaptations, is also drastically changed. In the novel, Gordon is a small-time criminal who takes Evey into his home after V abandons her on the street. The two share a brief romance before Gordon is killed by a Scottish gang. In the film, however, Gordon is a well-mannered work colleague of Eveys, and is later revealed to be gay. He is arrested by fingermen for broadcasting a political parody on his TV program but is later executed when a Quran is found in his possession. 

==Marketing==

===Home media=== Easter egg high definition format, which features a unique in-film experience created exclusively for the disc. Warner Bros. later released the video on Blu-Ray, on May 20, 2008.  The film also saw release on Sonys PSP UMD format. 

===Soundtrack===
  blacklisted tracks Wurlitzer jukebox that V "reclaimed" from the Ministry of Objectionable Materials. The climax of Tchaikovskys 1812 Overture appears at the end of the track "Knives and Bullets (and Cannons too)". The Overtures Finale (music)|finale  is played at key parts at the beginning and end of the film.

Three songs were played during the ending credits which were not included on the V for Vendetta soundtrack.  The first was " " by Spiritualized.
 The Girl Quiet Nights of Quiet Stars". These songs were played during the "breakfast scenes" with V and Deitrich and were one of the ways used to tie the two characters together. Symphony No. 5 (Beethoven)|Beethovens Symphony No.5 also plays an important role in the film, with the first four notes of the first movement signifying the letter "V" in Morse code.       Gordon Deitrichs Benny Hill-styled comedy sketch of Chancellor Sutler includes the "Yakety Sax" theme. Inspector Finchs alarm clock begins the morning of November 4 with the song "Long Black Train" by Richard Hawley, which contains the foreshadowing lyrics "Ride the long black train... take me home black train."

===Books===
The original graphic novel by Moore and Lloyd was re-released as a hardback collection in October 2005 to tie into the films original release date of November 5, 2005.  The film renewed interest in Alan Moores original story, and sales of the original graphic novel rose dramatically in the United States.   
 Steve Moore and based on the Wachowski Brothers script, was published by Pocket Star on January 31, 2006. Spencer Lamm, who has worked with the Wachowskis, created a "Making-of|behind-the-scenes" book. Titled V for Vendetta: From Script to Film, it was published by Universe on August 22, 2006. 

===Other===
As well as promotional items created to publicise the film (which included a shoulder bag and bust of "V"s Guy Fawkes mask), replicas of the mask and action figures were released.  Figures released by NECA include a 12-inch (30&nbsp;cm) action figure which speaks phrases from the film, a 12-inch resin statue and a seven-inch (17&nbsp;cm) figure. 

Semi-official V costumes have been created for Halloween. These range from the full costume of cape, hat, mask and dagger-belt, to various individual aspects — gloves, hat, mask, hair and daggers.  All are available separately or in combinations.

==See also==

*List of films featuring surveillance

==References==
 

==Further reading==
*  
*  

== External links ==
 
 
*   at Warner Brothers
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 