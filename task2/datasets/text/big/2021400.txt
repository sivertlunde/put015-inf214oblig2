The 40-Year-Old Virgin
{{Infobox film
| name = The 40-Year-Old Virgin
| image = 40-Year-OldVirginMoviePoster.jpg
| caption = Theatrical release poster
| alt = 
| director = Judd Apatow
| producer = {{Plain list | 
* Judd Apatow 
* Clayton Townsend 
* Shauna Robertson
}}
| writer = {{Plain list | 
* Judd Apatow 
* Steve Carell
}}
| starring = {{Plain list | 
* Steve Carell
* Catherine Keener
* Paul Rudd
* Romany Malco
* Seth Rogen
* Elizabeth Banks
* Leslie Mann
* Jane Lynch 
}}
| music = Lyle Workman Jack Green
| editing = Brent White Apatow
| Universal Pictures
| released =   
| runtime = 116 minutes 133 minutes (unrated version)
| country = United States
| language = English
| budget = $26 million 
| gross = $177.4 million 
}}
The 40-Year-Old Virgin is a 2005 American  .

==Plot==
Andy Stitzer (Steve Carell) is a 40-year-old virgin who is involuntarily Celibacy|celibate. He lives alone, collects action figures, plays video games, and his social life seems to consist of watching Survivor (TV series)|Survivor with his elderly neighbors. He works in the stockroom at an electronics store called SmartTech. When a friend drops out of a poker game, Andys co-workers David (Paul Rudd), Cal (Seth Rogen), and Jay (Romany Malco) reluctantly invite Andy to join them. At the game (which he wins, due to playing online poker constantly), when conversation turns to past sexual exploits, Andy desperately makes up a story, but when he compares the feel of a womans breast to a "bag of sand", he is forced to admit his virginity.

Feeling sorry for him (but also generally mocking him), the group resolves to help Andy lose his virginity. Throughout the next several days, the groups efforts prove to be unsuccessful, partly because all three men give Andy different and sometimes contradictory advice. They take him to have his chest waxed. Cal advises Andy to simply ask questions when talking to women, which makes Andy seem mysterious. His advice proves to be the most helpful, when Beth (Elizabeth Banks), a bookstore clerk, takes a liking to Andy. Andy starts to open up, and begins to form true friendships with his co-workers. David continues to obsess over his ex-girlfriend, Amy (Mindy Kaling). After meeting her unexpectedly during a speed-dating event attended by the group, he has an emotional breakdown while making a sale and is subsequently sent home by store manager Paula (Jane Lynch), who promotes Andy to fill in for him.

Jay, seeing Andys continued reluctance to approach female customers, attempts to force the issue by hiring Andy a prostitution|prostitute. When Andy discovers that Jay has inadvertently hired a transvestism|transvestite, he is prompted to confront his friends, and tells them that he is taking matters into his own hands. Andy lands a date with Trish Piedmont (Catherine Keener), a woman he met on the sales floor who owns a store across the street. After Andy and Trishs first date, in which they are interrupted by Trishs teenage daughter Marla (Kat Dennings) as they are about to have sex, Andy decides to tell Trish he is a virgin. Before he can tell her, Trish suggests that they postpone having sex, to which Andy enthusiastically agrees; they decide they wont have sex until their twentieth date. Meanwhile, Paula is impressed by Andys salesmanship and promotes him to floor manager.
 Kevin Hart), Jay reveals that his girlfriend Jill broke up with him after learning he had been cheating on her. Andy comforts Jay, who says that sex can ruin a relationship. Jill later decides to take Jay back (she is pregnant, and her misgivings about Jay as a father figure were what had spurred the breakup). Andy and Trishs relationship grows, and Trish suggests that Andy sell his collectible action figures in order to raise enough money to open his own store. Later, Andy takes Marla to a sexual health clinic, where Marla reveals herself to be a virgin. The counselor (Nancy Carell) remains sympathetic, while the other patients in the clinic laugh at Marla. Andy defends Marla by admitting that he is a virgin as well, but only gains ridicule himself. On the way back to Trishs house, Andy tells Marla that he only fabricated his virginity to protect her, but Marla deduces that Andy was actually telling the truth, and promises to keep this secret away from Trish, feeling that Andy should later inform her about it himself.

When they finally reach the twentieth date, Andy is still reluctant and resists Trish, upsetting her. An argument ensues, in which Andy accuses Trish of pushing him into changing his life against his will, and Andy leaves for the nightclub where Jay is celebrating his girlfriends pregnancy. He quickly gets drunk, and after running into Beth, leaves for her apartment with her. Meanwhile, David finally relinquishes his celibacy and hooks up with Bernadette, Marla convinces Trish to go and make up with Andy. By this time Andy has sobered up and, after witnessing Beths methods of foreplay, he starts to have second thoughts. As Andy is leaving her bathroom, he finds his friends waiting outside, having followed to warn him about Beth and encourage him to go back to Trish. They leave together (except for Cal), and Andy returns to his apartment, where he finds Trish waiting for him.

He attempts to apologize, but Trish, having found myriad suspicious belongings in his apartment, now thinks that Andy may be some sort of sexual deviant. Andy tries to convince her otherwise and declares his love for her, but she leaves in alarm and disgust. Andy chases after her on his bike, but at the moment of intercepting her, he collides with her car and flies headlong into the side of a truck. Trish rushes to his side in concern, and he finally confesses to her that he is a virgin. She is surprised to learn that this is the reason behind his strange behavior, as she does not consider it to be important, and they kiss. Later, Andy and Trish are married in a lavish ceremony with everyone in attendance, with a sidelong mention of Andys action figures having sold for approximately half a million dollars. Afterwards, they consummate the marriage over a period of two hours and one minute transitioning into a musical scene where the characters sing and dance to "Aquarius/Let the Sunshine In".

==Cast==
* Steve Carell as Andy Stitzer
* Catherine Keener as Trish Piedmont
* Paul Rudd as David
* Romany Malco as Jay
* Seth Rogen as Cal
* Jane Lynch as Paula
* Elizabeth Banks as Beth
* Leslie Mann as Nicky
* Kat Dennings as Marla Piedmont
* Gerry Bednob as Mooj
* Jordan Masterson as Mark
* Shelley Malil as Haziz
* Jonah Hill as eBay Customer
* Marisa Guterman as Girl with Braces
* Marika Dominczyk as Bernadette
* Mindy Kaling as Amy
* Mo Collins as Gina
* Stormy Daniels as Porn Star Loudon Wainwright as Priest
* Cedric Yarbrough as Health Clinic Dad #1
* David Koechner as Health Clinic Dad #2
* Jeff Kahn as Health Clinic Dad #3 Kevin Hart as Smart Tech Customer
* Rose Abdoo as Mother at Restaurant
* Jazzmun as Prostitute Call Girl
* Nancy Carell as Health Clinic Counselor
* Wyatt Smith as Boy at Wedding  
* Ann Christine as Kim  
* Jenna Fischer as Woman #1  
* Phyllis Smith as Andys Mother  

== Production ==
The production used over a million feet of film, a milestone reached on the last day of filming and recognized with free    Using the conversion of 90 feet of film per minute, this means that the shooting ratio for the film is  96:1 for the theatrical (84:1 for the unrated version).

==Reception==

===Critical response===
The 40-Year-Old Virgin was met with positive reviews. Rotten Tomatoes gives it a rating of 85%, based on 181 reviews, with an average rating of 7.2/10. The sites consensus reads, "Steve Carells first star turn scores big with a tender treatment of its titular underdog, using raunchy but realistically funny comedy to connect with adult audiences."  At Metacritic, the film scored a 73 out of 100, based on 35 critics, indicating "generally favorable reviews".  Rotten Tomatoes declared it the "Best Reviewed Comedy of 2005". 

  format  The pair gave minor criticisms, with Ebert describing "the way she (Catherine Keener as Trish) empathizes with Andy" as "almost too sweet to be funny"  and   of The New York Times called the film a "charmingly bent comedy", noting that Carell conveys a "sheer likability" and a "range as an actor" that was "crucial to making this film work as well as it does." 
 conservative columnist Cal Thomas for not being a "tribute to self-control or purity". 

   ten best movies of the year, the only comedy film to be so recognized (though the comedy-drama The Squid and the Whale was also chosen). The film was also ranked No. 30 on Bravo (US TV channel)|Bravos 100 Funniest Movies.

===Box office===

The film was a summer hit, and opened at No. 1 at the box office, grossing $21,422,815 during its opening weekend, and stayed at No. 1 the following weekend. The film grossed a total of $109,449,237 at the domestic market, and $67,929,408 overseas, for a total of $177,378,645. The film was 25th in global gross, and 19th in the United States that year.   

===Home media===
On home video the film was released with an additional 17 minutes under the banner "unrated". 

For the 100th Anniversary of Universal the theatrical edition was released on Blu-ray.

===Disclaimer===
The American Humane Association withheld its "no animals were harmed..." disclaimer due to the accidental deaths of several tropical fish used in the film. 

==See also==
 
* Naughty @ 40, a 2011 Bollywood remake
* The 41-Year-Old Virgin Who Knocked Up Sarah Marshall and Felt Superbad About It, a 2010 direct-to-DVD parody of this and several other Judd Apatow films

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 