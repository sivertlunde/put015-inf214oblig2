The Fall of the House of Usher (1950 film)
{{Infobox film
| name = The Fall of the House of Usher
| image =
| image_size =
| caption =
| director = Ivan Barnett
| producer = Ivan Barnett 
| writer = Edgar Allan Poe (story)   Dorothy Catt  Kenneth Thompson  
| narrator =
| starring = Gwen Watford   Kay Tendeter    Irving Steen    Vernon Charles 
| music = W.L. Trytel 
| cinematography = Ivan Barnett 
| editing = 
| studio = GIB Films
| distributor = Vigilant Films
| released = June 1950 
| runtime = 70 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} of the same title by Edgar Allan Poe.

==Synopsis==
The film uses a framing device set in a Gentlemens club where one of the members reads to his friends from a copy of Poes book. A century before a young man visits a bleak-looking mansion in the English countryside where his friend Lord Roderick Usher lives with his sister Madeline, both of whom are mysteriously ill. He discovers that they are suffering from a curse brought on them by their father which will cause them both to die shortly, leading to the downfall of the ancient family of Usher.

==Production and release== produced the director and cinematographer. Worked commenced in 1947, but it wasnt fully released until 1950. It was issued an History of British film certificates#1932–1951|H Certificate, a rarity at the time, by the British Board of Film Censors. Despite its limited budget the film proved surprisingly successful on its release as a second feature and even topped the bill in some cinemas.  It was reissued in 1955 and again in 1961.  It may have been an influence on the subsequent development of Hammer Horror. 

==Cast==
* Gwen Watford as Lady Usher  
* Kay Tendeter as Lord Roderick Usher 
* Irving Steen as Jonathan 
* Vernon Charles as Dr. Cordwall 
* Connie Goodwin as Louise
* Gavin Lee as The Butler  
* Keith Lorraine as George 
* Lucy Pavey as The Hag 
* Tony Powell-Bristow as Richard 
* Robert Wolard as Greville

==References==
 

==Bibliography==
* Chibnall, Steve & McFarlane, Steve. The British B Film. Palgrave MacMillan, 2009.
* Harper, Sue. Picturing the Past: The Rise and Fall of the British Costume Film. British Film Institute, 1994.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 