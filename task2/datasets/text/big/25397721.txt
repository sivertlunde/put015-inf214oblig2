5150 Elm's Way
{{Infobox film
| name           = 5150 Elms Way
| image          = 5150, Rue des Ormes.jpg
| caption        = Official Teaser Poster
| director       = Éric Tessier Pierre Even Josée Vallée
| writer         = Patrick Senécal
| starring       = René-Daniel Dubois Marc-André Grondin
| music          = Christian Clermont
| cinematography = François Dutil
| editing        = Alain Baril
| studio         = Melrose Studios Cirrus Communications
| distributor    = Alliance Vivafilm
| released       =  
| country        = Canada
| language       = French
}}
5150 Elms Way ( ) is a Canadian psychological-drama thriller film directed by 
Éric Tessier and starring  , written by author Patrick Senécal. 

==Plot==
Elms Way is a calm street in a small town. When Yannick falls from his bike, he knocks on the door of the Beaulieu residence, to call a cab home. Entering the house, Yannick hears a man screaming upstairs. When he finally encounters the source of the screams  he realizes that Beaulieu has wounded the man and was holding him hostage. Beaulieu then locks down Yannick in fear of him calling the police. Over time he learns Beaulieu is a righteous psychopath and fanatical chess player who kills drug-dealers, pedophiles and other bad people for a better world. 

Weeks pass and Yannick remains a prisoner, though is otherwise not mistreated by Baeulieu. He tries to escape, but is recaptured by Baeulieus daughter Michelle, who breaks his leg. As he has done nothing wrong, Beaulieu doesnt want to kill Yannick and eventually agrees to let him go if he wins a game of chess against him, Beaulieu having never lost a game in his life so far. They play chess constantly, but Yannick never wins, though he rattles Baeulieu by once managing a draw. After Beaulieus wife and daughter finally stand up to Beaulieu, they free Yannick. But Yannick has gone mad sitting locked in the room playing chess games against Beaulieu and doesnt leave, believing that the only option to stop Beaulieu is to win against him.

In the final showdown the two play a chess game in the cellar, where Beaulieu has conserved all of his victims and placed them as pieces on a giant chessboard. During the game, Beaulieus little stepdaughter enters the cellar and witnesses her dead mother placed as a piece on the chessboard. She is then shot by Beaulieu and the scene ends. The police arrive, free Yannick and arrest Beaulieu. Four months later Yannick is still madly obsessed with the interrupted chess game, so thoroughly consumed by the thought of the final position that he alienates himself from his girlfriend.  

==Cast==
* Marc-André Grondin as Yannick Bérubé
* Normand DAmour as Jacques Beaulieu
* Sonia Vachon as Maude
* Mylène St-Sauveur as Michelle
* Élodie Larivière as Anne
* Catherine Bérubé as Josée
* Normand Chouinard as Jérome Bérubé
* Louise Bombardier as Francine Bérubé
* Pierre-Luc Lafontaine as Simon
* René-Daniel Dubois as M. Ruel

==Production==
The film was filmed at Melrose Studios in Saint-Hubert, Québec, Canada. 

===Awards===
Éric Tessier won for his script the Audience Award at the Gérardmer Film Festival  and Joan Patricia Parris was nominated for the Jutra Award for the best make-up. 

==Release==
The film premiered on 9 October 2009 in Canada.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 