Romeo and Juliet (1968 film)
{{Infobox film
| name = Romeo and Juliet
| image = Romeo-and-juliet-DVDcover.jpg
| image_size =
| caption = 
| director = Franco Zeffirelli
| producer = John Brabourne Anthony Havelock-Allan
| screenplay = Franco Brusati Masolino DAmico Franco Zeffirelli
| based on = Romeo and Juliet by William Shakespeare
| narrator = Laurence Olivier
| starring =  
| music = Nino Rota
| cinematography = Pasqualino De Santis
| editing = Reginald Mills
| distributor = Paramount Pictures
| released =  
| runtime = 138 minutes
| country = United Kingdom Italy
| language = English
| budget =$850,000 Alexander Walker, Hollywood, England, Stein and Day, 1974 p399 
| gross = $38.9 million 
}}
 play of the same name (1591–95) by William Shakespeare.
 Best Cinematography Best Costume Best Director Best Picture. Sir Laurence Olivier spoke the films prologue and epilogue and reportedly dubbed the voice of the Italian actor playing Lord Montague, but was not credited in the film.

Being the most financially successful film adaptation of a Shakespeare play at the time of its release, it was popular among teenagers partly because it was the first film to use actors who were close to the age of the characters from the original play. Several critics also welcomed the film enthusiastically.  

==Plot summary== Montague and the Capulet clans breaks out in a street brawl, broken up by the Prince of the city. The same night, two teenaged children of the two families — Romeo (Montague) and Juliet (Capulet) —  meet at a Capulet feast and become deeply infatuated. Later, Romeo stumbles into the secluded garden under Juliets bedroom balcony and the two exchange impassioned pledges. They are soon secretly married by Romeos confessor and father figure, Friar Laurence, with the assistance of Juliets nursemaid. Unfortunately, another street duel breaks out between Juliets first cousin Tybalt and Romeos best friend Mercutio when Tybalt insults Romeo. Since Tybalt is Juliets cousin and Romeo has just been married to Juliet, he sees Tybalt as family and refuses to fight him, leading Mercutio to be a loyal friend and fight for him. This leads to Mercutios death. Romeo retaliates by fighting Tybalt and killing him, and is punished by the Prince with banishment instead of the death penalty.

Unaware of Juliets secret marriage, her father has arranged for her to marry wealthy Count Paris. In order to escape this arranged marriage and remain faithful to Romeo, Juliet consumes a potion prepared by Friar Laurence intended to make her appear dead for forty-two hours. Friar Laurence plans to inform Romeo of the hoax so that Romeo can meet Juliet after her burial and escape with her when she recovers from her swoon, so he sends another Friar, John, to give Romeo a letter describing the plan. However, when Romeos servant, Balthasar, sees Juliet being buried, under the impression that she is dead, he goes to tell Romeo and reaches him before Friar John. In despair, Romeo goes to Juliets tomb and kills himself by drinking poison. Awakening shortly after he expires, Juliet discovers a dead Romeo and proceeds to stab herself with his dagger, piercing her abdomen. Later, the two families attend their joint funeral and agree to end the feud.

==Production==
Set in a 14th-century Renaissance Italy in varying locations: 
*The balcony scene: At the Palazzo Borghese, built by Cardinal Scipione Borghese in the 16th century, in Artena, 20 miles south of Rome. Romanesque church named St. Pietro in Tuscania, 50 miles northwest of Rome.
*The  .
*The palace of the Capulets scenes: At Palazzo Piccolomini, built between 1459-62 by Pope Pius II, in the city of Pienza, in Siena province.
*The street scenes: Also in Pienza.
*The fight scenes: In Gubbio, a town in the region of Umbria.

===Casting===
According to Franco Zeffirellis autobiography, Paul McCartney was originally asked to play the part of Romeo. 

Main Characters
*Leonard Whiting as Romeo
*Olivia Hussey as Juliet
*John McEnery as Mercutio
*Milo OShea as Friar Lawrence
*Pat Heywood as The Nurse
*Robert Stephens as The Prince
*Michael York as Tybalt
*Bruce Robinson as Benvolio
*Paul Hardwick as Lord Capulet
*Natasha Parry as Lady Capulet
*Antonio Pierfederici as Lord Montague
*Esmeralda Ruspoli as Lady Montague
*Keith Skinner as Balthasar
*Roberto Bisacco as Paris
*Laurence Olivier as Chorus (uncredited)

==Reception==
The film earned $14.5 million in domestic rentals at the North American box office during 1969.  It was re-released in 1973 and earned $1.7 million in rentals. 

Film critic Roger Ebert has written: "I believe Franco Zeffirellis Romeo and Juliet is the most exciting film of Shakespeare ever made". 

According to ratings aggregate site Rotten Tomatoes, the film is positively reviewed by 97% of critics. 

==Soundtrack==
 
Two releases of the score of the film, composed by Nino Rota, have been released.  

==="Love Theme from Romeo and Juliet"===
The films love theme was widely disseminated, notably in "Our Tune", a segment of BBC disc jockey Simon Batess radio show. In addition, various versions of the theme have been recorded and released, including a highly successful one by Henry Mancini, whose instrumental rendition was a Number One success in the United States during June 1969. 

There are two different sets of English lyrics to the song.
*The films version is called "What Is a Youth?", featuring lyrics by Eugene Walter, and sung by Glen Weston. This version has been released on the complete score/soundtrack release.
*An alternate version, called "A Time for Us", featuring lyrics by Larry Kusik and Eddie Snyder. This version has been recorded by Johnny Mathis and Andy Williams, among others.  Josh Groban performed "Un Giorno Per Noi", an Italian version of "A Time for Us".
*A third version is called "Ai Giochi Addio", featuring lyrics by Elsa Morante, and has been performed by opera singers such as Luciano Pavarotti and Natasha Marsh.

==In popular culture== William Shakespeares Romeo + Juliet. Said Yorke, "I saw the Zeffirelli version when I was 13, and I cried my eyes out, because I couldnt understand why the morning after they shagged, they didnt just run away. The song is written for two people who should run away before all the bad stuff starts. A personal song."
 season three of The Wonder Years.

Celine Dion referenced this film, in particular the "hand dance" scene, in the video for her 1992 single "Nothing Broken but My Heart".

Japanese manga artist Rumiko Takahashi referenced the Zeffirelli film in two of her manga and anime works. In one episode of Urusei Yatsura, devious troublemaker Ryoko Mendou invites the series male protagonist, Ataru Moroboshi, to have a "Romeo and Juliet" rendezvous with her, and wears a dress based on Husseys from the film. Later, Takahashis Ranma 1/2 featured a storyline in which the lead characters, Ranma Saotome and Akane Tendo, are cast as Romeo and Juliet in a production of the play at their high school. Takahashi designed Ranma and Akanes costumes for the play with Whiting and Husseys outfits in the Zeffirelli film in mind. 

==References==
 

==External links==
* , featuring magazine articles and film reviews (archived).
*" " — 1968 review in Time (magazine)|Time (magazine)
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 