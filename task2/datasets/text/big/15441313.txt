Long John Silver (film)
 
{{Infobox film
|name=Long John Silver
|image= Ljspos.jpg image size=
|caption= Original film poster
|director=Byron Haskin
|producer=Joe Kaufmann Mark Evans
|writer=Martin Rackin based on= 
|starring=Robert Newton Connie Gilchrist Rod Taylor
|music=David Buttolph
|cinematography=Carl E. Guthrie
|editing=Manuel del Campo
|studio=Treasure Island Pictures Pty. Ltd. distributor = Distributors Corporation of America
|released=16 December 1954 (Australia)  17 December 1954 (UK)  21 December 1954 (USA)  
|runtime=106 minutes
|country=United States Australia

|budget=US$ 1,000,000     
|gross=754,745 admissions (France) 
}}
 Treasure Island, starring Robert Newton as Silver and Rod Taylor as Israel Hands.
 The Adventures Treasure Island in 1950, with Newton as Silver.
 Return to Treasure Island, starring Tab Hunter and Dawn Addams.

==Plot==
The movie is set some time after the events of Treasure Island. Long John Silver receives grave news from Dod Perch of a massacre by Mendoza, who had also kidnapped Governor Strongs daughter Elizabeth for ransom along with Jim Hawkins. Long John also learns of a second treasure cache on Treasure Island; the only clue to its location is a pirate medallion. Long John visits Governor Strong and his wife and proposes to deliver the ransom before they pursue Mendoza.

During the pickup of the ransom, Long John goes with Billy Bowlegs to Mendozas ship and explains that Billy shot his two partners to hoard the ransom money for himself. Long John, invited on the ship, suggests to Mendoza that he leave Elizabeth on shore and lure the governors warships away in order to sack the kings warehouses. As Mendoza carries out the plan, Long John finds that Jim possesses the pirate medallion indicating the second treasures location. Mendoza begins to double cross Long John, but Long John his men to ambush and capture Mendoza along with the warehouse fortune, while Jim and Elizabeth make their escape.

Back at the governors house, Jim is offered the chance to go back to England, but Long John has plans to take Jim with him on the second voyage to Treasure Island. Long John seizes an opportunity to charter Captain MacDougalls ship for the voyage. Long John sets off, avoiding becoming engaged to Purity Pinker, and barely escaping the alert local sentries.

Long John plots a mutiny on Captain MacDougalls ship. MacDougall discovers Long Johns plan and decides to maroon Long John and his men on an island that is the secret hideout of Mendoza. Jim sets fire to Mendozas warehouse so that Long John and his crew can capture Mendozas ship. As Long John sails for Treasure Island, Mendoza awaits his next ship.

Once on Treasure Island, Long John and his men take shelter in the stockade from Israel Hands, who had survived Jims shot some time ago, but is blind. Israel keeps Long John and his men trapped, killing them a few at a time. Soon, Mendozas men arrive, and Israel offers to side with Long John in return for a passage to Cornwall and vengeance against Jim. After they flee, Mendoza burns down the stockade.

Long John follows the trail of the map to the caves where the treasure is buried. Israel tries to kill Jim, but Jim leads him to the coast, where Israel plunges to his death. As Jim heads back to the caves, he is taken by Mendoza, who is going to use him as bait to get Long John, but Long John surrenders to Mendoza, giving his men the opportunity to make a gunpowder attack, cutting down Mendozas forces and leaving the rest marooned. Long John returns as an honorable citizen, but he and Jim ride off.

==Cast==
*Robert Newton as Long John Silver Jim Hawkins
*Connie Gilchrist as Purity Pinker
*Lloyd Berrell as Capt. Mendoza, El Toro Grant Taylor as Patch
*Rod Taylor as Israel Hands (as Rodney Taylor)
*Harvey Adams as Sir Henry Strong
*Muriel Steinbeck as Lady Strong Henry Gilbert as Billy Bowlegs John Brunskill as Old Stringley
*Eric Reiman as Lanky pirate
*Harry Hambleton as Big Eric Syd Chambers as Ned Shill
*George Simpson-Lyttle as Capt. Asa MacDougall (as George Simpson-Little)
*Duane Cahill as Mendoza pirate Al Thomas as Mendoza pirate

==Production== Treasure Island (1950), starring Robert Newton as Long John Silver, had been very successful at the box office. Because the novel was in the public domain, producer Joseph Kaufman decided to make a sequel in which Newton reprised his role.

The film was produced by Treasure Island Pictures Pty. Ltd. The companys dominant shareholder and financier was Joseph Kaufman. The minor shareholders were director Byron Haskin, writer Martin Rackin and star Robert Newton. 

The producer choose Australia to film, rather than Egypt, as a number of other films had been successfully made in Australia to reduce production costs, which was a common practice in the 1950s for US and British films, as the Australian crews spoke English.  Part of the funding from the film came from notorious Wall Street financier Louis Wolfson. Byron Haskin alleged that producer Joseph Kaufman ran out of money during production, making shooting extremely difficult.  Haskin arrived in February 1954. 
 Grant Taylor, Muriel Steinbeck, and Guy Doleman. Doleman was selected to play Israel Hands but refused to grow a beard and wear contact lenses which were required for the part. He dropped out and Rod Taylor stepped in instead.  The only actors imported were Robert Newton and Connie Gilchrist. The role of Jim Hawkins was given to Grant Taylors son Kit. 
 Waterfall (substituting for Treasure Island).   

Production began on 3 May 1954  and shooting lasted for 63 working days.  Filming was complicated by the fact that it was the first movie in Australia shot in CinemaScope.   This was also the first movie to be shot in DeLuxe Color outside the United States.  Del Campo became the second Mexican, after Joe MacDonald, to work on a CinemaScope picture.

While making the film, court proceedings were initiated against Newton in England to fulfill his debts, which resulted in his being declared bankrupt. 

==Reception==
The film was not a large success at the box office and critical reaction was poor.   A colour television series, The Adventures of Long John Silver, resulted nonetheless; it ran for one series of 26 episodes.

Kylie Tennant wrote a novelisation of the script. 

Kaufman took out an option on Pagewood Studios for two more years and announced plans to make other films in Australia including Come Away, Pearler, from the novel by Colin Simpson.   This did not happen.

==References==
 

==Further reading==
* 
*  at Peter Rodgers Organization website

==External links==
* 
* 
* 
* 
*  at Classic Australian Television
*  at the Rod Taylor Site
*  at Oz Movies
*  Registration Number PA0001292202

 
 

 
 
 
 
 
 
 