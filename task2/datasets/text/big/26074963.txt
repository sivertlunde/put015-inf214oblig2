Georgootty C/O Georgootty
{{Infobox film
| name           = Georgootty C/O Georgootty
| image          = O Georgekutty.jpg
| image size     = 
| alt            = 
| caption        = CD Cover Haridas
| producer       =  Ranjith
| narrator       =  Sunitha Thilakan
| music          = Mohan Sithara
| cinematography = 
| editing        = 
| studio         = Chandragiri Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
 romantic comedy drama film Sunitha and Haridas and written by Ranjith (director)|Ranjith,  the film was produced under the banner of Chandragiri Productions. It was Haridas directorial debut. Haridas got the State Award for Best New face Director for this film. The music was composed by Mohan Sithara with lyrics by Gireesh Puthenchery.

==Synopsis==
George Kutty (Jayaram) is a senior engineering student. His family, once rich, is now struggling financially after going through a drawn-out legal issue. He has three younger sisters.

Ittichen (Thilakan) is a rich but miserly, cruel and uncultured landlord. His daughter is Alice (Sunitha (actress)|Sunitha) who lives in the world of novels that come in weekly magazines. To save Georges family from hardship, Chandy (Jagathy Sreekumar), the marriage broker, brings Alice together with George Kutty. He lies to Ittichen about the wealth and status of Georges family. Ittichen comes to know about the truth very late and he vents all his anger on George. Despite being a brilliant student with excellent job prospect,  George is forced to stay at his in-laws house doing menial jobs. He accepts his fate since he was also part of the initial fraud. When it becomes too much for him to bear, he changes his approach and decides to take on his father-in-law. 

With the help of his old friend (Siddique (actor)|Siddique), who is the Police Inspector of the area and friends, he teaches Ittichen a lesson. He also saves Ittichens life from a local bully. Finally Ittichen accepts George not only as his son-in-law but as his own son. Once denied the right to go outside, George and Alice set off with Ittichens blessings.

==Cast==
*Jayaram as George Kutty Sunitha as Alice  
*Thilakan as Itichen 
*K. P. A. C. Lalitha as Aleyamma, Alices mother
*Jagathy Sreekumar as Chandi  Siddique as Inspector 
*Kuthiravattam Pappu as Paulose 
*Rizabawa as Prakash, George Kuttys friend
*Jagadish as Cheerankandathu Anto
*Babu Namboothiri as Ouseppachan, George Kuttys father
*Sukumari as George Kuttys mother
*Unnimary as Maria, Alices aunt
*Oduvil Unnikrishnan as Kunjeriya, Marias husband  Vijayaraghavan as Goonda

==References==
 

==External links==
*  
*   at the Malayalam Movie Database
*   at Oneindia.in

 
 
 
 
 
 