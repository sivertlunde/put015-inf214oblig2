Wide Awake (1998 film)
{{Infobox film
| name = Wide Awake
| image = Wideawakeposter.jpg
| caption = Theatrical release poster
| director = M. Night Shyamalan
| producer = Cathy Konrad Cary Woods
| writer = M. Night Shyamalan Joseph Cross Rosie ODonnell
| music = Edmund Choi
| cinematography = Adam Holender
| editing = Andrew Mondshein
| studio = Woods Entertainment
| distributor = Miramax Films
| released =  
| runtime = 88 minutes   
| country = United States English
| budget = $6 million 
| gross = $282,175 
}} Joseph Cross and Rosie ODonnell. Wide Awake also features Julia Stiles in one of her earliest roles as the main characters teenage sister, Neena.

Although it was made in 1995, the film was not released until 1998. The script was written in 1991. It was nominated for "Best Family Feature&nbsp;— Drama" and "Best Performance in a Feature Film&nbsp;— Leading Young Actor" at the 1999 Young Artist Awards. Shyamalan has described Wide Awake as a comedy that he hoped would also make people cry.

==Plot== Joseph Cross) passing of his beloved grandfather (Robert Loggia). 
 Catholic boys school (in Merion Station, Pennsylvania, Philadelphia suburb, where Shyamalan attended and where much of the movie is filmed). The adults in his world have not been able to convince him that his grandfather is in good hands, so he sets out on a personal mission to find God. In their varying ways he is guided on his metaphorical journey by his best friend Dave (Timothy Reifsnyder) and a baseball-loving nun (Rosie ODonnell) who teaches at his school.

Josh shows doubts about his religion as he questions if God truly exists, particularly when Dave is diagnosed with epilepsy and the moments as Josh experiences his first crush. As his academic year comes to an end, he finds his answer in an unexpected way. 

==Cast==
*Denis Leary as Mr. Beal
*Dana Delany as Mrs. Beal Joseph Cross as Joshua A. Beal
*Rosie ODonnell as Sister Terry

==Reception==
Wide Awake received mixed reviews from critics. Rotten Tomatoes gave the film a score of 40% based on reviews from 30 critics and reports a rating average of 4.8 out of 10. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 