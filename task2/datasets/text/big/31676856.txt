Sing Your Song
{{Infobox film
| name           = Sing Your Song
| alt            =  
| image	=	Sing Your Song FilmPoster.jpeg
| caption        = 
| director       = Susanne Rostock
| producer       = 
| writer         = Susanne Rostock
| starring       = Harry Belafonte
| music          = Hahn Rowe
| cinematography = 
| editing        = Jason L. Pollard Susanne Rostock
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $45,765   
}}
 Civil Rights movement.

This inspirational biographical film begins with Belafontes birth into poverty in Harlem in 1927, and childhood years in Jamaica, sent there by his immigrant mother. Director Susanne Rostock takes the viewer through his discovery of theater and training as an actor as a young man, and on to his career and success as a singer.

The film shows not only Belafontes remarkable success as a singer and actor, but also his true passion for social change. The film outlines some highlights of his entertainment career, but is more focused on how he helped change the world in other ways: marching with the Rev. Martin Luther King Jr. in the civil-rights era; working against apartheid in South Africa; fighting hunger through his instrumental work with USA for Africa; and, most recently, working to combat gang violence through programs with inner-city youth.*  

In an interview about the film, Belafonte discussed his activism from Civil Rights to poverty in Africa. {{cite news| url=http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2012/01/25/NSPO1MRFIV.DTL 
|title=Sing Your Song: Harry Belafonte on activism |first=G. Allen |last=Johnson|newspaper=San Francisco Chronicle|date=January 26, 2012}} 

The film won an award in section "American Docs" on the 2. American Film Festival.

At the 2011 Vancouver International Film Festival, Sing Your Song won the Most Popular Nonfiction Film Award,  which is based on ballots cast by audiences at the festival. 

Belafonte presented the film as the Closing Night selection of Maryland Film Festival 2011.

The film advanced to the final 15 contenders for the Academy Award for Best Documentary Feature  but was not nominated.

On January 10, 2012, REACT to FILM screened Sing Your Song at the Museum of Modern Art in Manhattan, N.Y. and moderated a Q&A with Harry Belafonte. 

==References==
 

==External links==
*  
*  
*   at Vimeo
*   (The Globe and Mail)
*   (The Vancouver Sun)

 
 
 
 
 
 
 