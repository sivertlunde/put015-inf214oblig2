Rocket's Red Glare
{{Infobox television film
| name         = Rockets Red Glare
| image        =   
| image_size   = 
| caption      = 
| genre        = 
| runtime      = 
| creator      = 
| director     = Chris Bremble
| producer     = Mike Elliott
| writer       = Chris Bremble John Palmer (story)
| starring     = Robert Wagner Marilu Henner Ryan Merriman
| editing      = 
| music        = Lior Rosner
| cinematography = Jacques Haitkin
| budget       = 
| country      = United States
| language     = English
| network      = ABC Family
| released     = August 27, 2000
}}

Rockets Red Glare is an American television movie that originally aired on ABC Family on August 27, 2000. The film stars Robert Wagner, Marilu Henner, and Ryan Merriman. Rockets Red Glare was originally titled Mercury Project. 

==Plot==
Teenager Todd Baker (Merriman) and his former astronaut grandfather Gus (Wagner) team up to rebuild a Mercury Redstone Rocket for Todds school project.

==Cast==
* Robert Wagner as Gus Baker
* Ryan Merriman as Todd Baker
* Marilu Henner as Meg Baker Fred Coffin as Mitch Greer
* Danielle Fishel as Sarah Miller
* John Finn as Wyatt Claybourne
* Sarah Lancaster as Carol
* Clayton Landey as Colonel Vincenze
* Cory Pendergast as Jason Jones
* Joel Polis as Colonel Mitchell
* Taryn Reif as Jen Karsten
* Bill Timoney as Pete Baker
* Brandon Tyler as Vic Henry
* Matt Winston as Mr. Lake
* Alan Bean as Himself
* Gordon Cooper as Himself

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 