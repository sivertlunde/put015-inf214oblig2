Jatt James Bond
{{Infobox film
| name           = Jatt James Bond caption = 
| director       = Rohit Jugraj Chauhan
| producer       = Fortune House Productions
| producer        = Gurdeep Dhillon
 Executive Producer: Ratan Aulakh
| writer         = Jas Grewal
| starring       = Gippy Grewal Zarine Khan Gurpreet Ghuggi Yashpal Sharma
| music          = Jatinder Shah
| cinematography = Parixit Warrier
| editing        = Sandeep Fransis
| budget         =   5 crores
| gross          =   40 crores
| studio         = 
| distributor    = Speed Records (record label)
| released       =   
| country        =  India
| language       = Punjabi
}}
Jatt James Bond is a 2014 Indian Punjabi film  directed by Rohit Jugraj, and starring Gippy Grewal and Zarine Khan as leads, along with Gurpreet Ghuggi, Yashpal Sharma. The music of the film is by Jatinder Shah. 

The film marks the Punjabi cinema debut of Bollywood actress Zarine Khan in her first punjabi movie, and released on April 25, 2014.   

==Plot==

Shinda was mistreated by his relatives, so he finds other ways to have his love Laali. Shinda and his two other friends come up with a plan to solve all of their problems.  

==Cast==
* Gippy Grewal as Shinda 
* Zarine Khan as Laalli 
* Gurpreet Ghuggi as Binder
* Yashpal Sharma as Bant Mistri
* Mukesh Rishi as MLA 
* Vindu Dara Singh as Bank Manager 
* Avatar Gill as Sarpanch
* Sardar Sohi as Jarnail   Shahbaz Khan as INS Harnek Singh
* Karamjit Anmol as Sucha Singh

==Production==
In August 2013, the principal cast including Zarine Khan was announced.  The film is being produced by  Gurdeep Dhillon under Fortune House Productions Inc. Subsequently two songs performed by Rahat Fateh Ali Khan with lyrics by S M Sadiq were recorded. Music Director Mukhtar Sahota has also done two songs for the film featuring Arif Lohar & Rahat Fateh Ali Khan.  The film went into principal photography in late October 2013. {{cite web  accessdate = 2014-01-20 }} 

==Release==
The first poster of the film was released in January 2014, ahead of its release on April 25, 2014. {{cite web  accessdate = 2014-03-25 }} 
The movie opened well across Punjab and collect more than 5.5 Crore in first 12 days of the release. Jatt James Bond got best opening among all the Punjabi release.
Jatt James Decleared Blockbuster By Box office India. 

==Box Office==

 Worldwide Box Office 
 
 *                                                  - Rs 4.51 Cr (3 weeks)
 *                                                 - Rs 4.69 Cr(4 weeks)
 *                                                      -
Rs 2.37 Cr(4 weeks)
 *                                                  - Rs 2.68 Cr(4 weeks)
 *                                                   - Rs 2.15  Cr(2 weeks)
 * Total Worldwide                                             - Rs  40.43 Crores

==JJ Bond Game==

The first experiment in Punjabi Cinema Jatt James Bond Official Game to download on android devices {{cite web  url = accessdate = 2014-04-19 }} 

==Soundtrack==
{{Infobox album | 
 
|  Name       = Jatt James Bond (Music Album)
|  Film       = Jatt James Bond
|  Type       =Soundtrack
|  Artist     = Gippy Grewal, Badshah, Rahat Fateh Ali Khan, Sunidhi Chauhan & Arif Lohar
|  Cover      = 
|  Released   = March 2014
|  Genre      = Film soundtrack  Speed Records
|  Producer   = Gurdeep Dhillon
|  
|  
|}}

{{Track listing
| collapsed       =  
| headline        =  Track Listing
| extra_column    =  Artist
| 
| all_writing     = 
| all_lyrics      = Kumar,Happy Raikoti,SM Sadiq,Ravi Raj 
| music       = Jatinder Singh-Shah,Mukhtar Sahota Surinder Rattan
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = yes
| title1          = Jatt Deyaan Tauran Ne
| note1           = 
| writer1         = 
| lyrics1         = Kumar
| music1          = Jatinder Singh-Shah
| extra1          = Gippy Grewal
| length1         = 02:54
| title2          = Chandi Di Dabbi
| note2           = 
| writer2         = 
| lyrics2         = Happy Raikoti
| music2          = Jatinder Singh-Shah
| extra2          = Gippy Grewal & Sunidhi Chauhan  
| length2         = 03:58
| title3          = Kale Kale Rahan Raat Nu 
| note3           = 
| writer3         = 
| lyrics3         = SM Sadiq 
| music3          = Mukhtar Sahota
| extra3          = Rahat Fateh Ali Khan
| length3         = 05:48
| title4          = Jis Tan Nu Lagdi Aye
| note4           = 
| writer4         = 
| lyrics4         = SM Sadiq
| music4          = Mukhtar Sahota
| extra4          = Arif Lohar
| length4         = 05:24
| title5          = Tu Meri Baby Doll 
| note5           = 
| writer5         = 
| lyrics5         = Ravi Raj
| music5          = Surinder Rattan
| extra5          = Gippy Grewal ft Badshah
| length5         = 03:16
| title6          = Tera Mera Saath Ho
| note6           = 
| writer6         = 
| lyrics6         = SM Sadiq
| music6          = Mukhtar Sahota
| extra6          = Rahat Fateh Ali Khan
| length6         = 04:15
| title7          = Ek Jugni,Do Jugni
| note7           = 
| writer7         = 
| lyrics7         = SM Sadiq 
| music7          = Mukhtar Sahota 
| extra7          = Arif Lohar
| length7         = 02:17
| title8          = Rog Pyaar De Dilan Nu
| note8           = 
| writer8         = 
| lyrics8         = SM Sadiq
| music8          = Mukhtar Sahota
| extra8          = Rahat Fateh Ali Khan
| length8         = 05:40
| 
| 
|
}}

==External links==
* 
* 
* 


==Awards==

PTC Film Awards 2015 

* Won - Best Editing - Sandeep Francis 
* Won - Best Story - Jas Grewal
* Won - Best Popular Song of The Year - Gippy Grewal / Sunidhi Chauhan for Chandi Di Dabbi
* Won - Best Supporting Actor - Yashpal Sharma
* Won - Best Debut Female - Zarine Khan
* Won - Best Debut Director - Rohit Jugraj Chauhan
* Won - Best Director - Rohit Jugraj Chauhan
* Won - Best Actor - Gippy Grewal

* Nominated - Best Background Score - Raju Singh
* Nominated - Best Cinematography - Parixit Warrior
* Nominated - Best Screenplay - Jas Grewal / Jitender Lal
* Nominated - Best Dialogues - Jas Grewal / Jitender Lal
* Nominated - Best Lyrics - S M Sadiq for Kalle Kalle 
* Nominated - Best Music Director - Surinder Ratan / Rahat Fateh Ali Khan / Mukhtar Sahota / Jatinder Shah
* Nominated - Best Performance In A Negative Role - Mukesh Rishi
* Nominated - Best Supporting Actor - Gurpreet Ghuggi
* Nominated - Best Actress - Zarine Khan
* Nominated - Best Film - Gurdeep Dhillon Films & Fortune House Production

== References ==
 

 

 
 
 
 
 