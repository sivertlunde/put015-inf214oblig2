Bye Bye Blues (film)
 
{{Infobox film
 | name = Bye Bye Blues
 | image_size = 
 | caption = 
 | director = Anne Wheeler
 | producer = Arvi Liimatainen Anne Wheeler
 | writer = Anne Wheeler
 | narrator = 
 | starring = Rebecca Jenkins Michael Ontkean
 | music = George Blondheim
 | cinematography = Vic Sarin Christopher Tate
 | distributor = Artificial Eye (UK) Circle Films (USA)
 | released = 1989
 | runtime = 117 min.
 | country = Canada
 | language = English
 | budget = 
 | preceded_by = 
 | followed_by = 
 | image = Bye Bye Blues VideoCover.jpeg
}} 1989 Canadian film. It was written and directed by Anne Wheeler and produced by Alberta Motion Picture Development Corporation with the assistance of Allarcom Limited.               

==Plot==
  Canadian military. The cast also includes Leslie Yeo, Kate Reid, Wayne Robson, Robyn Stevan, and Stuart Margolin.

==Awards==
The film was nominated for twelve  ).

==Soundtrack==
# Main Title
# Jazz Spring 	
# Theme For Teddy 	
# Marry Me Daisy 	
# When I Sing 	
# India 	
# Sweet Georgia Brown 	
# Maxs Theme (I Love You Daisy) 	
# Am I Blue 	
# Bath Blues 	
# Unfinished Blues 	
# Whos Sorry Now 	
# Home Movie/Its A Plane 	
# You Made Me Love You 	
# Blues For Anne 	
# Bye Bye Blues

;Credits
* Rebecca Jenkins - vocals John McCullough, and George Blondheim
* Music preparation by Laurie Bardsley
* Original film music composed and conducted by George Blondheim
* Mixed by Gary Dere, Paul Shubat, and Hayward Parrott
* Musicians: George Blondheim - piano, Mike Lent - bass, Bob McLaren - drums, Gene Bertoncini - guitar, Bob Stroup - trombone, P.J. Perry - clarinet and saxophone, Gary Guthman - trumpet, Vinod Bhardwaj - bansuri, Damyanti Bhardwaj - tanpura, Hari Sahay - tabla, George Ursan - drums ("When I Sing" and "Sweet Georgia Brown"), Melvin Wilson - guitar ("When I Sing"), Gary Koliger - guitar ("Sweet Georgia Brown"), Wayne Robson - background vocal ("When I Sing"), Wayne Robson, George Blondheim, Luke Reilly, and Stuart Margolin - vocal shouts ("Sweet Georgia Brown")
* Members of the Edmonton Symphony Orchestra: James Keene, Broderick Olson, Tom Johnson, Mary Johnson, Richard Caldwell, Hugh Davies, Stephen Bryant, Evan Verchomin, Andrew Bacon, Susan Ekholm, Derek Gomez, Tanya Prochzaka, Nora Bumanis, Colin Ryan, David Hoyt, Donald Plumb, Brian Jones, Susan Flook, Neria Mayer, Mikkio Kohjitani, John Taylor, Donald Hyder

==Trivia==
Springwater School in Starland County, Alberta, Canada was used as a set.

==Copyright status== Copyright Act, which allows the Copyright Board to issue a licence in respect of orphan works where "the Board is satisfied that the applicant has made reasonable efforts to locate the owner of the copyright and that the owner cannot be located".  Pursuant to the licence, the film is available online in Canada through the iTunes Store,  and two theatrical screenings were held in October 2014 at the Vancouver International Film Festival. 

==References==
 

==External links==
*  , at The Film Reference Library (a division of the Toronto International Film Festival Group)
*  
*  

 

 
 
 
 
 
 
 
 
 
 