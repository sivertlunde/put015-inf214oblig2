Otōto (2010 film)
{{Infobox film
| name           = Otōto
| image          = Ototo.jpg
| image size     = 
| caption        = 
| director       = Yoji Yamada
| producer       =  
| writer         =  
| narrator       = 
| starring       =  
| music          = Isao Tomita
| cinematography = Masashi Chikamori
| editing        = Iwao Ishi
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = $19,969,473 
| preceded by    = 
| followed by    = 
}}
Otōto ( ) is a 2010 film by Yoji Yamada. The first screening of this film outside of Japan was at the closing ceremony of the 60th Berlin Film Festival in 2010. The screening was a special tribute to then 81-year-old Kon Ichikawa,    whose films have been invited to the festival several times before.

==Plot==
The story unfolds as the young Koharu (Yū Aoi), the daughter of a pharmacist in a modest neighborhood of Tokyo, is about to get married to the son of a prestigious family, and even before the event everybody is anxious that Tetsuro (Tsurube Shofukutei), the younger brother of Koharus mother, Ginko (Sayuri Yoshinaga), might join the wedding ceremony, as he is considered to be the black sheep of the family and even Ginko and Koharu consider him to be an embarrassment, even though he has lived with the family for quite some time after the death of Koharus father.

As the invitations to the wedding had been returned to the sender, everybody is relieved that Tetsuro will not show up, as he has embarrassed the family in the past with his childish behavior and his drunkenness.

But during the party after the ceremony he turns up anyway in a loaned kimono and even after Ginko warns him, he starts drinking heavily and causes a ruckus, which is unsettling the grooms family. During the event he reveals his special relationship to Koharu: Her father asked him to name her, something he is still particularly proud of as every other endeavor in his life failed miserably.

After the event the family is forced to apologize formally to the grooms family and the oldest brother of Ginko and Tetsuro severs all ties with his younger brother. After staying with Ginko for a short time, she lends him money to go back to Osaka where he came from in the first place.

Her uncles behavior casts a shadow over Koharus marriage and soon she has got to move in with her mother again. Even Ginkos patience with her brother comes to an end when she is forced to reimburse a lover of his who had entrusted him with her savings which he has gambled away. Now even Ginko severs her ties with Tetsuro and does not want to hear from him anymore: When he visits her next time, Ginko and Koharu throw him out. 
Some time passes while Koharu struggles with her ultimate divorce and tries to get her life back together and starts a new relationship with a shy carpenter from the neighborhood.

Finally Ginko- who has secretly filled a missing person report- gets the news that Tetsuro has been hospitalized in Osaka. Despite his misbehavior, she is deeply worried and visits him in Osaka, learning that he is terminally ill with lung cancer and living in a hospice. Despite his illness, Tetsuro has neither lost his sense of humor, nor his childish character and Ginko is moved to see him being merry among the other patients and the staff of the hospice who have learned to enjoy his character.

Tetsuro predicts his own day of death, telling Ginko that the Buddha told him in a dream.

When the day draws closer, Ginko gets the news that Tetsuros health is deteriorating. She hurries to Osaka and finds him being as cheerful as ever, even tricking her into giving him a drink through his feeding tube. When his death gets closer, Koharu arrives as well and finally forgives her uncle on his deathbed.
 senile mother-in-law suggests inviting Tetsuro and Koharu and Ginko agree, moved to tears by their memories.

==Cast==
*Sayuri Yoshinaga - Ginko
*  - Tetsuro
*Yū Aoi - Koharu
*Ryō Kase - Akira

==Production==
This movie is dedicated to the memory of director Kon Ichikawa who had made a film with the same name, Her Brother|Otōto in 1960, which won a Special Distinction Award at the Cannes Film Festival in 1961.   

==Film festivals==
{| class="wikitable" style="font-size: 95%;"
|-
! Film Festival
! Date of ceremony
! Category
! Participants/Recipients
! Result
|- Japan Academy Prize  || rowspan="10" | 18 February 2011  || Best Film || Otōto || 
|- Best Director || Yoji Yamada || 
|-
| Best Male Actor || Tsurube Shofukutei ||  
|-
| Best Female Actor || Sayuri Yoshinaga ||  
|-
| Best Female Accompanying Actor || Yū Aoi ||  
|-
| Best Music || Isao Tomita ||  
|-
| Best Cinematography || Masashi Chikamori ||  
|-
| Best Lighting || Takakazu Watanabe ||  
|-
| Best Sound Effects || Kazumi Kishida ||  
|-
| Best Editing || Iwao Ishi ||  
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 