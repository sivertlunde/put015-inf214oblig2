Monisha En Monalisa
{{Infobox film
| name = Monisha En Monalisha
| image =
| director = T. Rajendar
| writer = T. Rajendar Mumtaj Deepshika Vadivelu T. Rajendar Silambarasan
| producer = T. Rajendar
| music =  T. Rajendar
| cinematography = T. Rajendar
| editing = P. R. Shanmugam
| studio = Chimbu Cine Arts
| released = 12 April 1999
| runtime = Tamil
| country = India
| budget =
| website =
}}
Monisha En Monalisa is a Tamil film released in 1999 directed and produced by T. Rajendar. His son, Silambarasan starred in a cameo role in the film, whilst debutants, Ramanakanth and Mumtaj played the lead roles. The film received primarily poor reviews upon release, with one reviewer labelling it the "low point of Tamil cinema." 

==Cast==
*Ramakanth Mumtaj as Monisha
*T. Rajendar as Kaadhaldasan
* Meenal pendse as Sharmila
*Deepshika
*Vadivelu
*Silambarasan

==Production==
The song Tholaipesi Enna was shot at a grand budget of 10 million rupees but the film had troubles when a flood that hit Chennai washed away a set erected on the banks of the Cooum.
During the production of the film, the film was renamed from Monisha to Monisha En Monalisa due to astrological reasons. 

==References==
 

==External links==
*  

 

 
 
 
 


 