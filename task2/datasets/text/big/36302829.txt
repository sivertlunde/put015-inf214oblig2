A Girl in Summer
{{Infobox film
| name           = A Girl In Summer
| image          =
| caption        = Original 1986 theatrical release poster
| director       = Vítor Gonçalves
| producer       = José Bogalheiro
| screenplay     = Vítor Gonçalves
| based on       =
| starring       = Isabel Galhardo Diogo Dória João Perry Virgílio Castelo José Manuel Mendes Alexandra Guimarães Joaquim Leitão
| music          = Andrew Poppy
| cinematography = Daniel Del-Negro
| editing        = Ana Luísa Guimarães
| studio         = Trópico Filmes
| distributor    = 
| released       = 1986
| runtime        = 85 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
}}
A Girl In Summer ( ) is a Portuguese film from 1986, directed by Vítor Gonçalves. It was considered by the Harvard Film Archive as One of the great Portuguese films of the 1980s,  when screened as part of the School of Reis program.

== Release and reception ==
The film had its world premiere as part of the official selection of the Berlin Film Festival in 1986, and was part of the official selection of the International Film Festival Rotterdam. School of Reis film program, at the Harvard Film Archive, also screend at the Anthology Film Archives and the UCLA Film and Television Archive.

== Cast ==
* Isabel Galhardo as Isabel
* Diogo Dória as Diogo
* Joaquim Leitão as Quim
* José Manuel Mendes as José
* João Perry as The Hunter
* Virgílio Castelo as João
* Alexandra Guimarães as Joana
* Rui Reininho as a dancer

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 