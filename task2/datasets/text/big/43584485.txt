Vetta
{{Infobox film
| name           = Vetta
| image          = Vettamalfilm.png
| caption        = Promotional Poster
| director       = Mohan Roop
| producer       = Sudheesh Kumar for Gallery Films 
| writer         = Kaval Surendran Sreenivasan Anjali Anuradha Balan Nithya Santhakumari Santhakumari T. Raveendran 
| music          = M. G. Radhakrishnan
| cinematography = Vipin Das
| editing        = K.Sankunni
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
 1984 Cinema Indian feature directed by Raveendran playing other important roles.   

== Cast ==
 
*Mammootty
*Mohanlal 
*Adoor Bhasi 
*Beena 
*Prameela  Sreenivasan 
*Anjali Naidu  Anuradha 
*Balan K Nair  Nithya  Santhakumari 
*T. G. Ravi 
*Vazhoor Rajan  Raveendran
 

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by Ajeer Ilankaaman and Chirayinkeezhu Ramakrishnan Nair.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Madaalasa || Seema Behan || Ajeer Ilankaaman ||
|-
| 2 || Thudi thudi || Seema Behan || Chirayinkeezhu Ramakrishnan Nair ||
|-
| 3 || Ullala thenala || K. J. Yesudas || Chirayinkeezhu Ramakrishnan Nair ||
|-
| 4 || Vilaasa Lathike  || K. J. Yesudas || Chirayinkeezhu Ramakrishnan Nair ||
|}

==References==
 

==External links==
*  

 
 
 
 
 
 