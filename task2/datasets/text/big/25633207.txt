Stark Love
{{infobox film
| name           = Stark Love
| image          =
| caption        = Karl Brown Paul Wing (asst. director) Karl Brown William LeBaron Adolph Zukor Jesse Lasky Karl Brown Walter Woods
| starring       = Helen Mundy Forrest James
| music          =
| cinematography = James Murray Richard Pittack (asst. camera)
| editing        =
| distributor    = Paramount Pictures
| released        = February 28, 1927
| runtime        = 70 minutes,  7 reels; 6,203 ft.
| country        = United States Silent (English intertitles)
}}
 Karl Brown and released by Paramount Famous Lasky Corporation, now known as   Paramount Pictures. The film is a maverick production in both design and concept, is a beautifully photographed mix of lyrical anthropology and action melodrama from director Karl Brown. “Man is absolute ruler. Woman is working slave.” Such are the rigid attitudes framing this tale of a country boy’s beliefs about chivalry that lead him to try to escape a brutal father with the girl he loves. Stark Love, was cast almost exclusively with amateur actors and filmed entirely in the Great Smoky Mountains, near Robbinsville, North Carolina.

In 2009, it was named to the National Film Registry by the Library of Congress for being “culturally, historically or aesthetically” significant and will be preserved for all time. 

An extensive account of the movies making, and its aftermath, can be found in the book Hillbillyland: What the Movies Did to the Mountains and What the Mountains Did to the Movies by J. W. Williamson.

Forrest James is the father of Fob James,  48th governor of Alabama.

Plot Synopsis

"Amidst the primitive mountain culture of the Carolina hills lives young Rob Warwick. He, unlike his fellowmen, has learned to read and entertains ambitions of another life. He learns of another world, where woman is looked up to by man, who builds a home for her and protects and supports her, as opposed to the position of drudge that she maintains in his society. Fired with ambition to attend school, he tells young Barbara, whose parents are his nearest neighbors, of his plans. When the itinerant minister arrives to perform the yearly marriage and burial services, Rob goes with him to the settlement, sells his horse, pays the tuition for schooling, but enrolls Barbara in his place. He returns to find that his mother has died and that his father, left with a brood to care for, has selected Barbara to be his wife. Rob pleads with his father but is beaten; the girl is aroused to threaten Warwick with an ax, and she escapes with the boy, floating down the swollen stream to the settlement and freedom". 

==Cast==
*Helen Mundy &ndash; Barbara Allen
*Forrest James &ndash; Rob Warwick
*Reb Grogan &ndash; Quill Allen
*Silas Miracle &ndash; Jason Warwick

== History of the film ==
Stark Love was released by Paramount Pictures on February 28, 1927.  When the film was released it was praised for its documentary realism.  Even with such praise, the film was not a commercial success.  Paramount most likely burned the picture along with 1,014 other feature silent films, for the silver they contained.  But in 1968 a  single  original copy of the film was found in the Czechoslovakian film archives.  The film was discovered by British film historian Kevin Brownlow.   Brownlow found the picture while filming in Prague.  After the picture was discovered copies were made for the Museum of Modern Art and the United States Library of Congress.  The film was selected for screening at the 1969 New York Film Festival.  After the films short-lived revival, the film went back into obscurity.  Besides scholars and those lucky enough to see a screening, the film is still widely unknown.    

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 