Beach Blanket Bingo
{{Infobox film
| name           = Beach Blanket Bingo
| image          = Beach blanket bingo333.jpg
| image size     = 225px
| caption        = theatrical poster
| director       = William Asher
| producer       = Samuel Z. Arkoff James H. Nicholson
| writer         = Leo Townsend Sher Townsend William Asher
| narrator       =
| starring       = Frankie Avalon Annette Funicello
| music          = Les Baxter
| cinematography = Floyd Crosby, ASC
| editing        = Eve Newman Fred R. Feitshans Jr. American International
| released       = April 14, 1965
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
}}

Beach Blanket Bingo is an American International Pictures beach party film, released in 1965 and was directed by William Asher. It is the fifth film in the beach party film series. The film starred Frankie Avalon and Annette Funicello and also featured cameos by Paul Lynde, Don Rickles and Buster Keaton. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 20  Linda Evanss singing voice was dubbed by Jackie Ward.

==Plot== skydiving stunt, John Ashley) The Perils of Pauline-like twist, with the evil South Dakota Slim (Timothy Carey) kidnapping Sugar and tying her to a buzz-saw.

==Cast==
  
* Frankie Avalon as Frankie
* Annette Funicello as Dee Dee
* Harvey Lembeck as Eric Von Zipper   John Ashley as Steve
* Deborah Walley as Bonnie
* Jody McCrea as Bonehead
* Marta Kristen as Lorelei
* Linda Evans as Sugar Kane
* Don Rickles as Big Drop
* Paul Lynde as Bullets
* Donna Loren as Donna
 
* Timothy Carey as South Dakota Slim
* Buster Keaton as Buster
* Bobbi Shaw as Bobbi Earl Wilson as Himself
* Michael Nader as Butch
* Donna Michelle as Animal
* Patti Chandler as Patti
* Andy Romano as J.D.
* Alberta Nelson as Puss
* Myrna Ross as Boots
* Chris Cranston as a Beach Girl
 

;Cast notes
*Beach Blanket Bingo was Frankie Avalons last "starring role" in the Beach Party films. He appears for only a few minutes in the next film, How to Stuff a Wild Bikini, and not at all in the last film The Ghost in the Invisible Bikini.
*Jody McCrea, who played "Deadhead" in Beach Party, Muscle Beach Party and Bikini Beach, is now called "Bonehead" in this film, as AIP had decided the term "Deadhead" was a so-called bankable noun and had decided to cast Avalon as the title character of their upcoming Sergeant Deadhead. McParland  John Ashley, who played "Ken" in Beach Party, and "Johnny" in both Muscle Beach Party and Bikini Beach, returns in this film as "Steve," playing opposite his real-life wife Deborah Walley.
*Rat Pack leader Harvey Lembeck (Eric Von Zipper) is given more screen time than ever before in this third film to feature his character. He even gets to sing his own song titled "Follow Your Leader" (which he reprises as "I Am My Ideal" for the follow-up How to Stuff a Wild Bikini).
*The part of Sugar Kane, played by Linda Evans, was originally intended for Nancy Sinatra. This change was due in part to the fact that the plot involved a kidnapping, which was somewhat reminiscent of her brother Frank Sinatra, Jr.s kidnapping a few months before shooting began and it made her uncomfortable causing her to drop out. McParland  Playmate of the Year for 1964.
* Bobbi Shaw, again is playing her "ya, ya" Swedish bombshell part. 
* Though this was Rickles fourth film in the series, its the only one in which he was allowed to "be himself" in one scene. He did a little of his night-club act and insulted the cast members, notably asking why Avalon and Funicello were in the picture: "Youre 40 years old!"

==Production==
Elsa Lanchester was originally announced for a small role off the back of her performance in Pajama Party George Succeeds Despite Miscasting
Dorothy Kilgallen:. The Washington Post, Times Herald (1959-1973)   07 Dec 1964: B11.   but does not appear in the final film.

;Deleted sequences and songs
* After the sequence wherein Frankie sings “These Are The Good Times”: McParland 
:Dee Dee leaves the beach club and sings “I’ll Never Change Him” by herself at the beach house.
 (This sequence can still be seen in 16mm prints and television broadcasts of Beach Blanket Bingo,  but the Region 1 MGM DVD omits it. See Music section below) 
* After Frankie completes his skydiving jump: Beach Blanket Bingo, pp. 20–22 

:Bonehead asks Frankie if Lorelei and himself can double-date with Frankie and Dee Dee;

:Bonehead then goes to a dress shop to get Lorelei’s clothes – where an older saleslady flirts with him as he tries to illustrate Lorelei’s dress size;

:A strolling Frankie and Dee Dee see Bonehead with his arms around the older saleslady and figure she must be his date;

* After Bonehead brings Lorelei her clothes and shoes: Beach Blanket Bingo, pp. 20–22 

:Frankie and Dee Dee arrive to pick them up, and the four of them sing “A Surfer’s Life For Me” as they drive to the beach club in Frankie’s hot rod coupe. Then, as seen in the release print, the two couples arrive together at the beach club as the Hondells are performing “The Cycle Set.”

;Movie tie-in
Dell Comics published a 12 cent comic book version of Beach Blanket Bingo in conjunction with the movies release. Beach Blanket Bingo 

==Music==
The score for this fifth film, like the four preceding it, was composed by Les Baxter.

 ; “Follow Your Leader” sung by Harvey Lembeck with the “Rat Pack;” and the two songs “New Love” and “Fly Boy” – both of which were sung by studio call vocalist Jackie Ward off-screen – and lip-synched by Linda Evans onscreen.
 Roger Christian wrote three songs, “Cycle Set” and the instrumental “Freeway” - both performed by the Hondells; and  “Ill Never Change Him” performed by Annette Funicello. (“Ill Never Change Him” was included in initial prints, but later excised for wide release when the decision was made to feature the song as "Well Never Change Them" in Ski Party.)

==Legacy==
* The title of this film inspired the title for Steve Silvers 1974 play, Beach Blanket Babylon, which has become Americas longest-running musical revue.
 The Outsiders, set in the mid 1960s, Beach Blanket Bingo is shown playing at a drive-in.

* An excerpt from the title song and a partial scene from Beach Blanket Bingo, dubbed into Vietnamese, feature in the 1987 film, Good Morning, Vietnam

* Beach Blanket Bingo is also a band from the UK

==References==
;Notes
 

;Bibliography
* {{cite book
  |last=McParland
  |first=Stephen J. location = Riverside, California, USA 
  |publisher=PTB Productions
  |year=1994
  |isbn=0-9601880-2-9}}

* {{Cite journal
  | title = Beach Blanket Bingo
  | journal = Dell Movie Classic
  | publisher = Dell Publishing Inc.
  | date = July–September 1965}}

==External links==
*  
*  
*  
*   at Brians Drive-in Theatre

 
 

 
 
 
 
 
 
 
 
 
 
 