Green Lantern: First Flight
 
{{Infobox film
| name           = Green Lantern: First Flight
| image          = GL First Flight.jpg
| alt            =  
| caption        = Two-Disc Special Edition DVD cover art
| director       = Lauren Montgomery
| producer       = {{Plainlist|
* Sam Register
* Bruce Timm
* Alan Burnett
* Bobbie Page }}
| writer         = {{Plainlist|
* Alan Burnett
* Michael Allen }}
| starring       = {{Plainlist|
* Christopher Meloni
* Victor Garber
* Tricia Helfer
* Michael Madsen }}
| music          = Robert Kral
| cinematography = 
| editing        = 
| studio         = {{Plainlist|
* Warner Bros. Animation
* Warner Premiere
* DC Comics }}
| distributor    = Warner Home Video
| released       =   }}
| runtime        = 75 minutes   
| country        =  
| language       = English
| budget         = 
| gross          = $8,122,562  
}}
Green Lantern: First Flight is a 2009  .

==Plot==
Before any other sentient beings existed in the universe, a race of beings calling themselves the Guardians of the Universe harnessed the power of the green element, the greatest power in the universe, to create the Green Lantern battery. However, the battery was vulnerable to the color yellow, the one part of the light spectrum that could resist green. The Guardians hid the most concentrated source of yellow energy, the yellow element, to prevent others from using it against them.

After the death of Abin Sur, several Green Lanterns arrive to take Ferris Aircrafts test pilot Hal Jordan (Christopher Meloni) to the Green Lantern Corps on Oa. He is placed under the supervision of respected senior officer Sinestro (Victor Garber), who is investigating Abins murder.  While undercover on the ship of Kanjar Ro (Kurtwood Smith) searching for the whereabouts of the stolen yellow element, Abin had come under attack. Fleeing to Earth, he had his ring find his successor and died of his injury shortly after. Unbeknownst to the other Green Lanterns, Sinestro had provided Kanjar with the location of the element in order to have it fashioned into a weapon of comparable power to the Green Lantern battery.

Jordan quickly comes to understand that Sinestros beliefs are not in line with those of the Guardians: Sinestro believes that the Guardians have reduced the Corps to merely picking up the messes criminals create as opposed to proactively dealing with the problem. During a mission to capture Kanjar Ro, Jordan is knocked unconscious by Kanjars energy staff. Sinestro comes in and kills Kanjar, pinning the blame on Jordan. As punishment, the Guardians strip Jordan of his ring.

While Jordan waits to be taken home, Sinestro uses his ring to temporarily animate Kanjars corpse, allowing him to learn the location of Qward where the yellow element weapon is being fashioned. Jordan convinces fellow Lanterns Boodikka (Tricia Helfer) and Kilowog (Michael Madsen) that Sinestro is not what he seems. When they catch Sinestro red-handed, Boodikka reveals her true allegiance and incapacitates Kilowog, allowing Sinestro to escape. Jordan tricks her into destroying Kanjars unstable energy staff, the explosion launching her into the tools hanging from the ceiling and killing her.

On Qward, the Weaponers bestow Sinestro with the yellow ring and battery, the latter of which resembles Ranx the Sentient City. Using its power, he lays waste to Oa, the yellow light easily overpowering the Green Lantern rings. The yellow battery destroys the green battery, rendering all the Green Lantern Corps power rings inert and causing death by asphyxiation of countless Green Lanterns who were in space at the time of their rings failure. Jordan, having recovered his ring moments too late, finds the battery and pounds on the inert green element. He places his ring on the small crack that appears, absorbing the whole of its power. Imbued with the full might of the green energy, he destroys the yellow battery by crushing it between two moons.

Having exhausted most of the green power to destroy the yellow battery, Jordan is left to fight against Sinestro under his own power. After an intense hand-to-hand battle without constructs, Jordan uses the last of his power to knock Sinestro to the surface of Oa where Kilowog crushes the yellow ring (as well as Sinestros hand) with his foot. Having regained partial power to his ring earlier, Kilowog takes to the air and saves Jordan from a fatal fall to the planets surface.
 Green Lantern oath to Jordan. Jordan then leaves for Earth to check in with his other boss, Carol Ferris (Olivia dAbo), remarking on the long "Commuting|commute".

==Cast== Hal Jordan / Green Lantern
* Victor Garber as Sinestro
* Tricia Helfer as Boodikka
* Michael Madsen as Kilowog Tomar Re
* Kurtwood Smith as Kanjar Ro
* Larry Drake as Ganthet Appa Ali Apsa
* Malachi Throne as Ranakar
* Olivia dAbo as Carol Ferris Richard Green as Cuch
* Juliet Landau as Labella
* David Lander as Chp
* Richard McGonagle as Abin Sur
* Rob Paulsen as Weaponers Arisia
* Jim Wise as Lieutenant
* Bruce Timm as Bug Boy

==Production==
According to director Montgomery, Jordans origin story was previously covered in the   film: "...we really didnt want to spend a whole lot of time telling that same story over again. So in Green Lantern: First Flight, the origin story is over and done before the opening credits." 

==Soundtrack==
  
{{Infobox album
| Name = Green Lantern: First Flight (Soundtrack from the DC Universe Animated Original Movie)
| Type = Film score Digital download)
| Artist = Robert J. Kral
| Cover = GreenLanternFirstFlight.jpg
| Released = August 25, 2009
| Length = 60:44
| Label = WaterTower Music (Digital download)
La-La Land Records (CD)
| Reviews = 
}}

{{Track listing
| collapsed       = no
| headline        = Green Lantern: First Flight (Soundtrack from the DC Universe Animated Original Movie)
| extra_column    = 
| total_length    = 60:44

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          = Main Title 
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = 
| length1         = 2:06

| title2          = The Ring Chooses Hal 
| note2           =  
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = 
| length2         = 4:42

| title3          = Hal Meets / The Flight of The Lanterns 
| note3           =
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = 
| length3         = 3:46

| title4          = Labellas Club 
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = 
| length4         = 3:28

| title5          = Going After Cuch 
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = 
| length5         = 3:04

| title6          = The Way I Heard It 
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = 
| length6         = 2:19

| title7          = Bugs in the Baggage 
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = 
| length7         = 4:14

| title8          = Teleport Pursuit 
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = 
| length8         = 2:28

| title9          = Brutal Attack / Fate of Kanjar Ro 
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = 
| length9         = 3:50

| title10         = Relinquishing the Ring 
| note10          =  
| writer10        = 
| lyrics10        = 
| music10         = 
| extra10         = 
| length10        = 1:16

| title11         = Back From / Boodikka Turns 
| note11          =   
| writer11        = 
| lyrics11        = 
| music11         = 
| extra11         = 
| length11        = 5:49

| title12         = Weaponers / Sinestro Transforms 
| note12          = 
| writer12        = 
| lyrics12        = 
| music12         = 
| extra12         = 
| length12        = 4:28

| title13         = The New Power Arrives 
| note13          = 
| writer13        = 
| lyrics13        = 
| music13         = 
| extra13         = 
| length13        = 2:35

| title14         = The Corps Fights Sinestro  
| note14          = 
| writer14        = 
| lyrics14        = 
| music14         = 
| extra14         = 
| length14        = 2:48

| title15         = The Corps Fall 
| note15          = 
| writer15        = 
| lyrics15        = 
| music15         = 
| extra15         = 
| length15        = 1:34

| title16         = Revival of the Green Lantern" 
| note16          = 
| writer16        = 
| lyrics16        = 
| music16         = 
| extra16         = 
| length16        = 2:25

| title17         = Asteroid Battle 
| note17          = 
| writer17        = 
| lyrics17        = 
| music17         = 
| extra17         = 
| length17        = 2:47

| title18         = Ring Against Ring 
| note18          = 
| writer18        = 
| lyrics18        = 
| music18         = 
| extra18         = 
| length18        = 3:00

| title19         = The Green Lantern Pledge  
| note19          = 
| writer19        = 
| lyrics19        = 
| music19         = 
| extra19         = 
| length19        = 1:03

| title20        = End Credits 
| note20         = 
| writer20       = 
| lyrics20       = 
| music20        = 
| extra20        = 
| length20       = 3:00
}}

==Reception==
Like previous DC Universe Animated Original Movies, Green Lantern: First Flight received mostly positive reviews, currently holding a 7.1/10 at   praised the animation and the scale, but complained that the film glosses over a lot of Hal Jordans backstory from the comics and lacks character development. "The filmmakers seem less interested in his transition from an ordinary man into a intergalactic superhero, and in their eagerness to get him up into space and fighting aliens right away." Overall they gave the film a 7 out of 10.  Comic Book Resources gave a positive review for Green Lantern: First Flight, citing that "Green Lantern: First Flight is a welcome portrayal of the material. It shows the appropriate scale and scope of the concept. It illustrates the characters in their best light and, most crucially, makes you wish Green Lantern was its own ongoing animated series."  AMCs Filmcritic.com also gave a positive film review, giving the animated feature 4.0 out of 5 stars. 

==Home media== Justice League Duck Dodgers episode "The Green Loontern". 

==Related media== John Stewart.

==References==
 

==External links==
*  
*  
*  
*   DVD Trailer
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 