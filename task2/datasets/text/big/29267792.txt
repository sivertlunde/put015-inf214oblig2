The Dark Stairway
 
 
 
{{Infobox film
| name           = The Dark Stairway
| image          =
| caption        =
| director       = Arthur B. Woods
| producer       = Irving Asher Brock Williams Mignon G. Eberhart
| starring       = Hugh Williams Chili Bouchier
| music          =
| cinematography = Robert LaPresle
| editing        = Warner Brothers-First First National Productions
| released       =  
| runtime        = 73 minutes
| country        = United Kingdom
| language       = English
}}
The Dark Stairway is a 1938 British crime film, directed by Arthur B. Woods and starring Hugh Williams, Chili Bouchier and Garry Marsh.  

The film was a quota quickie production, based on the 1931 novel From This Dark Stairway by Mignon G. Eberhart.  In the film, professional jealousy and rivalry erupts in a hospital over the discovery of a revolutionary new formula for anaesthetic, leading to murder.  The Dark Stairway is now classed as a lost film. 

==Cast==
* Hugh Williams as Dr. Thurlow
* Chili Bouchier as Betty Trimmer
* Garry Marsh as Dr. Mortimer
* Reginald Purdell as Askew
* Lesley Brook as Mary Cresswell
* Aubrey Pollock as Dr. Cresswell
* Glen Alyn as Isabel Simmonds
* John Carol as Merridew
* Robert Rendel as Dr. Fletcher

==References==
 

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 

 