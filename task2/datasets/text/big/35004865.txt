Dix mille ans de cinéma
{{Infobox film
| name           = Dix mille ans de cinéma
| image          = 
| caption        = 
| director       = Balufu Bakupa-Kanyinda
| producer       = Scolopendra Productions
| writer         = 
| starring       = 
| distributor    = 
| released       = 1991
| runtime        = 13 minutes
| country        = Democratic Republic of the Congo France
| language       = 
| budget         = 
| gross          = 
| screenplay     = Balufu Bakupa-Kanyinda
| cinematography = Stephan Oriach
| editing        = Stephan Oriach
| music          = 
}}

Dix mille ans de cinéma  is a 1991 short documentary film.

== Synopsis ==
This documentary offers the reflections of filmmakers shot at FESPACO 1991. Djibril Diop Mambéty, David Achkar, Moussa Sene Absa, Mambaye Coulibaly, Idrissa Ouedraogo, Mansour Sora Wade... express their faith in the eternity of African cinema.

== References ==
 

 
 
 
 
 
 
 
 
 


 
 