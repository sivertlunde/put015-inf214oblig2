La Tour Montparnasse Infernale
{{Infobox film
| name           = La Tour Montparnasse Infernale
| image          =
| caption        =
| director       = Charles Nemes
| producer      = 
| writer         = 
| starring       = Éric Judor Ramzy Bedia Marina Foïs
| music          = Jean-Claude Vannier
| cinematography = Étienne Fauduet
| editing        = Dominique Galliéni	
| distributor    = UGC-Fox Distribution
| studio         = 
| released       =  
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = $9,570,000
| gross          = $19,563,849 
}}
La Tour Montparnasse Infernale is a 2001 French comedy film directed by Charles Nemes and written by Kader Aoun, Ramzy Bedia, Eric Judor and Xavier Matthieu. When it came out in cinemas in Canada, it was translated into Dont Die Too Hard in reference to Die Hard, starring Bruce Willis.

==Plot==
Eric and Ramzy are working as window washers at the Montparnasse skyscraper in Paris. Thinking that he has a date set up with beautiful executive Marie-Joelle (who in reality hates his guts), Ramzy stays at work late while Eric hangs around with him. As a result, the pair witness a gang of terrorists seize the tower and take its late-night occupants (including Marie- Joelle) hostage. Knowing that only they can save the day, Eric and Ramzy swing into action.

==Cast==
*Eric Judor as Eric
*Ramzy Bedia as Ramzy
*Marina Foïs as Stéphanie Lanceval
*Serge Riaboukine as De Fursac
*Michel Puterflam as Lanceval
*Bô Gaultier de Kermoal as Ming
*Pierre Semmler as Hans
*Edgar Givry as Greg
*Georges Trillat as Peter
*Bruce Johnson as Chris
*Laurence Pollet-Villard as Sylvie
*Pierre-François Martin-Laval as Tran
*Olivier Balazuc as Fils 4 Lanceval
*Jean-Claude Dauphin as Le commissaire
*Étienne Fauduet as Séparatiste corse
*Benoît Giros as Jean-Louis
*François Goizé as Pilote hélicoptère
*Vincent Haquin as Chef GIGN
*Thibault Lacroix as Fils 1 Lanceval
*Grégory Lemoigne as Fils 3 Lanceval
*Frederic Moreau as Séparatiste basque
*Gyuri Nemes as Séparatiste Breton
*Marc Raffat as Pilote hélicoptère
*Lionel Roux as Homme du RAID
*Philippe Schwartz as Fils 5 Lanceval
*Volodia Serre as Fils 2 Lanceval
*Joey Starr as Youston
*Omar Sy as Taxi driver
*Fred Testot as Policier fumeur

== References ==
 

==External links==
*  

 
 
 
 
 


 
 