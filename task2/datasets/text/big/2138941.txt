Dragon Ball Z: Cooler's Revenge
{{Infobox film
| name           = Dragon Ball Z: Coolers Revenge
| image          = DBZ THE MOVIE NO. 5.jpg
| caption        = Japanese box art
| director       = Mitsuo Hashimoto
| producer       = Chiaki Imada Rikizô Kayano
| writer         = Original story: Akira Toriyama Screenplay: Takao Koyama
| starring       = See  
| music          = Shunsuke Kikuchi
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        =  47 minutes gross           = ¥2.20 billion ($16.2 million) Japanese and English
}}
Dragon Ball Z: Coolers Revenge, also known by its Japanese title  , or by   animated feature movie, originally released in Japan on July 20, 1991 at the Toei Anime Fair.

 , on Blu-ray and DVD on May 27, 2008. The film was released to DVD again on November 1, 2011 in a remastered box set containing the first five Dragon Ball Z movies. 

==Plot== Cooler is Saiyan home world, Planet Vegeta. Coolers ship soon picks up a Saiyan space pod on the radar. The pod, carrying a baby Saiyan named Kakarot, is shown to be heading towards Earth, but Coolers henchmen believe it is an escape. Cooler lets the ship go, accounting that its Friezas naïveté, and responsibility. More than twenty years later, Kakarot, eventually renamed Goku, transforms into a Super Saiyan and defeats Frieza on Planet Namek. Despite showing no compassion towards his brother, Cooler takes his Armored Squadron - Doore, Neiz, and Salza to Earth with the intention of killing Goku in order to reclaim his familys honor. His forces ambush Goku and his friends on a camping trip, and Goku is wounded when he takes a blast from Cooler that was intended for Son Gohan|Gohan.

After Krillin sets up a shelter in a cave with Goku, Gohan flies off to obtain Senzu beans from Korin. After obtaining the beans, he is ambushed by Coolers henchmen, but is saved by Piccolo (Dragon Ball)|Piccolo. Piccolo dispatches Doore and Neiz, and takes on Salza, easily gaining the upper hand. During the battle, Cooler arrives and catches Piccolo off-guard with a Death Beam, which impales him through the chest but does not kill him. Cooler then sends Salza after Gohan as he continues to blast Piccolo. Gohan gets to the cave with the Senzu beans, but they are destroyed by Salza. However, he heals Goku with a spare bean that Yajirobe gave him earlier. After Krillin and Gohan are beaten down by Salza, Goku appears fully healed. Cooler shows up, holding onto an unconscious Piccolo. He then blasts Piccolos body with an unexpected attack as Goku watches in horror. Goku blasts Salza into the distance, and begins to fight Cooler.

After Goku proves to be a worthy opponent, Cooler tells of his bleak history with Frieza. He then reveals his final transformation, one that Frieza never showed or achieved, and this one is far more powerful than Friezas final form. Now at full power, Cooler pummels Goku around effortlessly, despite Goku using the Kaioken attack numerous times. Cooler then proceeds to destroy the landscape. However, after seeing a bird die from wounds inflicted by damage caused during the struggle, Goku thinks back on his friends and family, and how they all will be killed if Cooler wins. He then picks up the dead bird, letting out a yell so powerful it cracks solid stone. Goku then transforms into his Super Saiyan form, and gives enough of his energy to revive the bird. Cooler then finds himself, like Frieza was, grossly outclassed. After attempting to punch and blast the Super Saiyan with no effect, Cooler summons a trump card when he powers up an enormous ball of energy. Cooler launches the attack with hopes to destroy Goku and the Earth along with him. After a stint of struggling, Goku eventually manages to send it back with the Kamehameha wave, forcing Cooler off the Earth, towards the sun. As he hits the suns surface, Cooler finally realizes that Goku was none other than the baby Saiyan within the escape pod that he ignored back during Friezas attack on Planet Vegeta. Lamenting his own naïveté, Coolers body is then seemingly disintegrated by the sun.

Goku, drained after his battle, is found by Krillin and Gohan. They are looking for Piccolo when Salza reappears and prepares to kill them all. However, before he can attack, he is killed by Piccolos "Special Beam Cannon" from a distance. Gohan looks around for Piccolo, and the movie ends with Piccolo, having recovered, gazing skyward.

==New characters==

===Cooler===
  is the main villain in the fifth and sixth Dragon Ball Z films and a minor villain in the OVA special Plan to Destroy the Saiyans. He is the brother of Frieza who travels to Earth in Coolers Revenge to seek revenge on Goku for what happened to his father, King Cold. While he admitted that he never liked his brother and thanked Goku for killing him, he felt that he needed to punish the one who had disgraced his family (Namely his father), by killing Frieza. Despite transforming into his final form, he is defeated by Super Saiyan Goku. He returns in Return of Cooler, his remains having combined with the Big Gete Star, a sentient planet-sized machine. This gives him the ability to create an indefinite amount of Meta-Coolers, which have the ability to constantly repair and improve upon themselves. He tries to consume the planet New Namek, but he is eventually destroyed by the efforts of Goku and Vegeta. He is later resurrected as a ghost by Dr. Raichi in Plan to Destroy the Saiyans along with Frieza, Slug and Turles but was defeated along with them when Goku and the fighters realized they were ghosts and shouted out that they werent real. He is voiced by Ryūsei Nakao in the Japanese films and Andrew Chandler in the English versions.  In the English dubbed version, Cooler has a voice similar to that of Hugo Weaving. He also made a cameo in Dragon Ball GT and on the cover of Fusion Reborn.

===Coolers Armored Squadron===
Like the mercenary  .

==Cast==
{| class="wikitable"
|-
! Character Name
! Voice Actor (Japanese)
! Voice Actor (English)
|-
| Goku|| Masako Nozawa || Sean Schemmel
|- Gohan || Masako Nozawa || Stephanie Nadolny
|- Piccolo || Toshio Furukawa || Christopher Sabat
|-
| Krillin || Mayumi Tanaka || Sonny Strait
|- Oolong || Naoki Tatsuta || Bradford Jackson
|- Naoko Watanabe || Cynthia Cranz
|-
| Master Roshi || Kōhei Miyauchi || Mike McFarland
|-
| Korin || Ichirō Nagai || Mark Britten
|-
| Yajirobe || Mayumi Tanaka || Mike McFarland
|- Cooler || Andrew Chandler
|-
| Salza || Shō Hayami || Michael Marco
|- Masato Hirano || Bill Townsley
|-
| Doore ||  Masaharu Satō || Mike McFarland
|-
| Frieza || Ryūsei Nakao || Linda Young
|- Bardock || Masako Nozawa|| Sonny Strait
|-
| Icarus || Naoki Tatsuta || Christopher Sabat
|-
| Narrator || Jōji Yanami || Kyle Hebert
|}

==Music==
Original music
* Opening Song
*# "Cha-La Head-Cha-La"
*#* Lyrics:  , Vocals: Hironobu Kageyama

* Ending Song
*#  
*#* Lyrics: Masaru Satō, Music: Chiho Kiyoka, Arrangement: Kenji Yamamoto, Vocals: Hironobu Kageyama & Ammy

===Funimation soundtrack===
The following songs were present in the Funimation dub of Dragon Ball Z: Coolers Revenge. But not the remastered Blu-Ray version   The remaining pieces of background music were composed by Mark Menza.

# Drowning Pool - Reminded
# Dust for Life - Poison
# American Pearl - Seven Years Breaking Point - Under
# Finger Eleven - Stay and Drown Breaking Point - Falling Down
# Drowning Pool - Mute Disturbed - The Game
# Drowning Pool - Told You So
# Deftones - Change (In the House of Flies)
# American Pearl - Revelation Breaking Point - Phoenix

==References==
 

==External links==
*   of Toei Animation
*  
*  

 

 
 
 
 
 
 