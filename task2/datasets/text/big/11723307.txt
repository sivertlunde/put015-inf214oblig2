My Brother the Pig
{{Infobox Film
| name = My Brother the Pig
| image = My Brother the Pig.jpg
| caption = Theatrical release poster Erik Fleming
| producer = Don Reardon
| writer = Matthew Flynn
| starring = Nick Fuoco Scarlett Johansson Judge Reinhold Romy Windsor Eva Mendes Alex D. Linz
| music = Michael Giacchino
| cinematography = Michael Stone
| editing = Scott Conrad
| studio = Unapix Productions
| distributor = Brimstone Entertainment Ardustry Home Entertainment
| released = September 10, 1999
| runtime = 92 minutes
| country = United States
| language = English
}}
 1999 American Erik Fleming and starring Scarlett Johansson, Judge Reinhold, Alex D. Linz, and Eva Mendes.

==Plot==
A boy named George is magically transformed into a pig. In a dangerous and crazy adventure, the boy, his sister, Kathy, his best friend Freud, and their college housekeeper, Matilda leave for Mexico to try to undo the witchcraft before their parents return from their Paris trip. While gathering ingredients in order for Matildas grandmother, Berta, to undo the spell, Kathy grows impatient and after insulting both Matilda and Berta, storms off into town, where she befriends two Mexican girls who speak English and have satellite TV. Meanwhile, Freud accidentally loses George to a butcher. With Kathy and her friends, they try to rescue George, in the process of angering the butcher. They are able to get George to Coyote Mountain, under a full moon phase, where a potion has been prepared to return George to normal. Unfortunately, the butcher had followed them, just as the ritual had begun. Matilda and Berta dose the butcher with the potion, turning him into a vulture, while George is restored to normal. They soon return home and act like nothings happened, except for the fact that George still has a pigs tail.

==Cast==
* Nick Fuoco as George Caldwell
* Scarlett Johansson as Kathy Caldwell
* Judge Reinhold as Richard Caldwell
* Romy Windsor as Dee Dee Caldwell
* Eva Mendes as Matilda
* Alex D. Linz as Freud

== Trivia ==
The UK DVD of the film states it stars " ". Johansson was originally cast in the Keri Russell role in the Mission Impossible film but dropped out due to schedule conflicts, this obviously caused the confusion for the UK DVD producers.

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 