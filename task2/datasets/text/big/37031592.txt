The Last Outlaw (1927 film)
{{Infobox film
| name           = The Last Outlaw
| image          = The Last Outlaw 1927 Poster.jpg
| border         = 
| caption        = Theatrical release poster
| director       = Arthur Rosson
| producer       = {{Plainlist|
* Jesse L. Lasky
* E. Lloyd Sheldon
* Adolph Zukor
}}
| writer         = {{Plainlist|
* John Stone
* J. Walter Rubin
}}
| story          = Richard Allen Gates
| starring       = {{Plainlist|
* Gary Cooper
* Jack Luden
* Betty Jewel
}}
| music          = 
| cinematography = James Murray
| editing        =  Paramount Famous Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = {{Plainlist|
* 63 minutes
* 6 reels, 6,032 ft
}}
| country        = United States
| language       = English intertitles
| budget         = 
| gross          = 
}} Western silent film directed by Arthur Rosson and starring Gary Cooper, Jack Luden, and Betty Jewel.    Written by John Stone and J. Walter Rubin, based on a story by Richard Allen Gates, the film is about a frontiersman who falls in love with a pretty woman whose brother is accused of murder. He tries to prove the young man innocent of the charges, but when he is appointed sheriff, he is obliged to track down and arrest the boy.  A 16mm reduction positive print exists of this film.   

==Plot==
On his way to Steer City, Buddy Hale (Gary Cooper) rescues Janet Lane (Betty Jewel) from a runaway horse. Unknown to Buddy, the womans brother Ward (Jack Luden) just shot the sheriff. Heading a ring of indignant ranchers whose cattle are being systematically rustled, Ward suspects that the sheriff and Justice Bert Wagner are leading the gang of thieves. Justice Wagner makes Buddy sheriff and sends him to arrest his predecessors murderer.

Soon after, Ward is deliberately shot by one of Wagners accomplices, and Buddy returns the dying man to his sister. Thinking that Buddy shot him, she resents the new lawman. Later, Janet leads the other ranchers and steals back their cattle. At Chicks insistence, Janet discusses the situation with Buddy, who is convinced of Wagners guilt. In an ensuing showdown, Butch and Wagner are killed in a cattle stampede, while Janet is saved by Buddy, who is then made mayor of Steer City. 

==Cast==
* Gary Cooper as Sheriff Buddy Hale
* Jack Luden as Ward Lane
* Betty Jewel as Janet Lane
* Herbert Prior as Bert Wagner
* Jim Corey as Butch
* Billy Butts as Chick
* Hank Bell as Henchman (uncredited)
* Lane Chandler as Rancher (uncredited)
* Flash as The Horse (uncredited) 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 