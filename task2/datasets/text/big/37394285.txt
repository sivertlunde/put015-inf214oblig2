Save the Date
{{Infobox film
| name           = Save the Date
| image          = 
| caption        = 
| director       = Michael Mohan
| writer         = Jeffrey Brown, Egan Reich, Michael Mohan 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Save the Date is a 2012 film directed by Michael Mohan. It stars Lizzy Caplan and Alison Brie. It won the Achievement Award at the 2012 Newport Beach Film Festival and was nominated for the Grand Jury Prize at the 2012 Sundance Film Festival. 

==Plot Summary== Mark Webber), who has a crush on her and has been hanging around the bookstore she manages. He decides to approach her but withdraws when he overhears shes living with the lead singer of the band (Kevin). Kevin, ignoring warnings from Andrew, proposes to Sarah from the stage. Overwhelmed, she moves out and gets her own apartment. She talks to Jonathan at the bookstore and a new relationship blossoms. Beth in the meantime has become obsessive about her upcoming wedding plans, causing friction with Andrew and Sarah. Sarah feels isolated when she discovers shes pregnant and pulls away from Jonathan without explanation. She eventually tells Beth, who seems to take it as an attempt to ruin her wedding plans. At this point Sarah has a first solo show of her work. Andrew, Kevin, and Jonathan all attend, but Beth doesnt. The opening at the gallery acts as a catalyst, and all things come to a head.

==Cast==
* Lizzy Caplan as Sarah
* Alison Brie as Beth
* Martin Starr as Andrew
* Geoffrey Arend as Kevin Mark Webber as Jonathan

==Reception==
 
Review aggregation website Rotten Tomatoes gives the film a score of 46% based on reviews from 28 critics. 

 

 

==References==
 

==External links==
*  
* Save the Date at  

 
 
 


 