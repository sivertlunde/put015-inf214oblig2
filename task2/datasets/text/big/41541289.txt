Poriyaalan
{{Infobox film
| name           = Poriyaalan
| image          = Poriyaalan.jpg
| alt            =  
| caption        = Film poster
| director       = Thanukumar
| producer       = Vetrimaaran A. K. Vetri Velavan M. Devarajulu
| writer         = Manimaran Rakshita Achuta Kumar
| music          = M. S. Jones
| cinematography = Velraj
| editing        = G. B. Venkatesh
| studio         = Grass Root Film Company Ace Mass Medias
| distributor    = Vendhar Movies
| released       =  
| sound effects editor        = Rajshekar
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}} Rakshita in the leading roles. Velraj is the cinematographer for the venture,  while newcomer M. S. Jones composed the music.  The film released on 5 September.

==Cast==
 
* Harish Kalyan as Saravanan Rakshita as Shanthi
* Aadukalam Naren
* Achyuth Kumar as Sundar
* Ajay Raj as Prabhu
* Mohan Raman as Shastri
* Delhi Ganesh
* Udhayabhanu Maheswaran
* Ajay Rathnam
* Mayilsamy
* Crane Manohar
* Velraj as Velraj (Cameo appearance)
* Gaana Bala in a cameo appearance
 

==Production==
The film was first announced in January 2013, with reports claiming that a Vetrimaarans assistant Dharmaraj would make the film for Ace Mass Media with Harish Kalyan in the lead role.  The film progressed slowly until November 2013, when it was announced that director Vetrimaaran would produce the film which had been written by Manimaran of Udhayam fame. The films director was revealed to be Thanukumar, with over ninety percent of the film having been completed.  Hasika|Rakshita, who has previously appeared in Telugu films and in Kayal (film)|Kayal (2014), played the lead role. 

== Release ==
The distribution rights of the film were acquired by Vendhar Movies.  The film was released on 5 September 2014.

==Critical reception==
Baradwaj Rangan wrote, "Poriyaalan is a sort of sibling to Vetrimaaran’s first film, Polladhavan...What sets these films apart from the typical action-thrillers is their texture — you can taste the grit. The storytelling, after (a) flabby beginning, is superbly economic...the film makes good on its promise of a tight little thriller, with mostly adequate performances".  The Times of India gave the film 3 stars out of 5 and wrote, "If the film succeeds, it is mainly because of its screenplay by Mani Maaran. Barring one hard-to-believe turn, it manages to hit the right buttons to keep us on the edge of the seat. It is racy, thrilling and entertaining".  Behindwoods.com wrote, "Poriyaalan has a fairly gripping story, with a commendable, but crisp performance from Mohan Raman". 

== References ==
 

==External links==
*  

 

 
 
 
 
 
 


 
 