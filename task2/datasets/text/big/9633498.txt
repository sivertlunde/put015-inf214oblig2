La Aventura explosiva
{{Infobox film
| name     =       La Aventura explosiva
| image          = Aventuraexposiva1.jpg
| caption        = Screenshot
| director       = Ricardo Bauleo Orestes Trucco
| producer       = Orestes Trucco
| writer         = Salvador Valverde Calvo
| starring       = Víctor Bó
| music          =
| cinematography = Juan Carlos Desanzo
| editing        =
| distributor    =
| released       = February 24, 1977
| runtime        = 85 minutes
| country        = Argentina Spanish
| budget         =
| followed_by    =
}} 1977 Argentina|Argentine action comedy film directed by Ricardo Bauleo and Orestes Trucco and written by Salvador Valverde Calvo.  The film starred Víctor Bó. The cinematography was performed by Juan Carlos Desanzo.

 

== Cast ==
* Ricardo Bauleo
* Víctor Bó
* Julio De Grazia
* Thelma Stefani
* Aldo Barbero
* Hugo Caprera
* Adriana Costantini
* Juan Carlos de Seta
* Emilio Disi
* Enrique Kossi
* Arturo Maly
* Pablo Palitos
* Rodolfo Ranni
* Jorge Villalba

 

== External links ==
*  

 
 
 
 
 

 