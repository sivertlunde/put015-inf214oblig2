Black Bread
{{Infobox film
| name = Pa negre
| image = Pa negre.jpg
| caption =Theatrical release poster
| director = Agustí Villaronga Isona Passola
| screenplay = Agustí Villaronga
| based on =   Sergi López
| cinematography = Antonio Riestra
| editing =  TVC Televisión TVE
| distributor = 
| released =  
| runtime = 108 minutes
| country = Spain
| language = Catalan
| budget = $6 million
| gross = 
}}

Black Bread ( ,  ) is a 2010 Catalan-language Spanish drama film written and directed by Agustí Villaronga. The screenplay is based on the homonymous novel by Emili Teixidor, with elements of two other works by him, Retrat dun assassí docells and Sic transit Gloria Swanson. 
 Best Foreign Language Film at the 84th Academy Awards       being the first Catalan-language film to do it.

== Plot == war years Catalan countryside, Dionís, a bird dealer, is killed with his son by a man in a hooded cape. Andreu, an 11-year-old boy, discovers the bodies. The falangist mayor of the town blames Farriol, Andreu’s father. Farriol, who was Dionís’s business partner dealing with birds, is an easy target for incrimination due to his suspicious background as a supporter of the Second Spanish Republic. Years before, the mayor and Farriol were rivals vying for the love of Florència, Andreu’s mother, cementing the major resentment against both of them. Fearing for his life, Farriol decides to flee and cross the border into France. Florència has to work in a factory in Vic so Andreu is sent to live with his paternal relatives in a house full of women and children. Àvia, Andreu’s old grandmother and Ció, a widower aunt who has a son a few years older than Andreu, work looking after the country home of the richest family of the region, the Manuben. There is also a younger aunt, Enriqueta, who is being pressured into marrying an older neighbor she does not love. The grim household is completed by Andreu’s orphan cousin Núria, a maimed but beautiful girl around his age, who lost a hand while playing with a grenade.
 civil guard. The precocious and lively Núria is engaging in sexual games with her alcoholic school teacher. Andreu befriends an older boy who he first spots bathing naked in a river in the forest. The boy is a tuberculosis patient in a nearby monastery, who imagines he has angels wings. Andreu helps him by setting aside some food for him. Farriol, as Andreu accidentally discovers, has not really left for France but is hiding in the farmhouse attic.

Núria and Andreu become frequent companions roaming the mysterious forest together. She has a crush on him and tries a sex game, but he rejects her sexual advances. When the mayor orders a search of the farmhouse, Farriol is found and sent to prison. Farriol furtively tells Andreu to convince Florència to ask for help from the influential Mrs. Manubens. Florència pays her a visit. Because the rich lady is childless and has a weakness for children Florència takes Andreu with her. Mrs. Manubens reluctantly writes a letter to the mayor interceding on behalf of Farriol. However, the mayor tries to take sexual advantage of Florència instead. Andreu slowly discovers that his mother has a secret of her own. In her youth she was a close friend of Pitorliua, the ghost of the legend, who was actually a delicate young man. Visiting his burial place, Andreu and Núria encounter Pauletta, Dionís’s half-demented widow. Pauletta tells them that Pitorliua was the homosexual lover of the only brother of Mrs. Manubens and because of this he was castrated. She implies that Farriol had something to do with it. Exploring the cave where Pitorliua was castrated Andreu and Núria discover the names of the culprits on the wall: Dionís and Farriol. Andreu confronts his mother and Florència confesses that Dionís and Farriol were paid by Mrs. Manubens to scare Pitorliua off, but things went too far.

Farriol is condemned to death. Before he is executed Florència and Andreu visit him in jail to say goodbye. He tells his son not to forget his beliefs and ideals. After Farriol’s funeral, Pauletta spitefully reveals to Florència and Andreu that Farriol was the killer of her husband and son following orders from Mrs. Manubens. Dionís tried to blackmail her and Mrs. Manubens first got rid of Dionís and then of Farriol. The rich lady bought Farriol’s silence in exchange for providing an excellent education for Andreu. Andreu begins to reject his family full of lies and deceptions. Instead of running away with Núria, as they originally planned, he ultimately accepts to be educated at the expense of Mrs. Manubens. Florència comes to see him at the boarding school, but Andreu has not forgiven his parents. When a classmate asks him who was the woman who came to see him he answers that it was just a woman from his village.

== Cast ==
*Francesc Colomer as Andreu
*Marina Comas as Núria
*Nora Navas as Florència
*Roger Casamajor as Farriol
*Lluïsa Castell as Ció
*Mercè Arànega as Sra. Manubens
*Laia Marull as Pauletta
*Marina Gatell as Enriqueta
*Elisa Crehuet as Àvia
*Eduard Fernández as the teacher Sergi López as the mayor

== Prizes == Goya Awards (Spain)
* Won: Best Actress– Leading Role (Nora Navas)
* Won: Best Actress – Supporting Role (Laia Marull)
* Won: Best Art Direction (Ana Alvargonzález)
* Won: Best Breakthrough Actor (Francesc Colomer)
* Won: Best Breakthrough Actress (Marina Comas)
* Won: Best Cinematography (Antonio Riestra)
* Won: Best Director (Agustí Villaronga)
* Won: Best Picture
* Won: Best Screenplay – Adapted (Agustí Villaronga)
* Nominated: Best Actor – Supporting Role (Sergi López)
* Nominated: Best Costume Design (Mercè Paloma)
* Nominated: Best Make Up
* Nominated: Best Production Supervisor (Aleix Castellón)
* Nominated: Best Sound

San Sebastián International Film Festival (Spain)
* Won: Best Actress (Nora Navas)

3rd Gaudí Awards|Gaudí Awards (Catalonia, Spain)
* Won: Best Film in Catalan Language
* Won: Best Actress– Leading Role (Nora Navas)
* Won: Best Actress – Supporting Role (Marina Comas)
* Won: Best Actor – Supporting Role (Roger Casamajor)
* Won: Best Director (Agustí Villaronga)
* Won: Best Screenplay (Agustí Villaronga)
* Won: Best Picture (Antonio Riestra)
* Won: Best Art Direction (Ana Alvargonzález)
* Won: Best Production Supervisor (Aleix Castellón)
* Won: Best Sound (Dani Fonrodona, Fernando Novillo and Ricard Casals)
* Won: Best Make Up (Satur Merino i Alma Casal)
* Won: Best Sound (José Manuel Pagán)
* Won: Best Costume Design (Mercè Paloma)
* Won: Best Film Editing (Raúl Román)

== See also ==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 