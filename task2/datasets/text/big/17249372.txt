Wild West Comedy Show: 30 Days & 30 Nights – Hollywood to the Heartland
{{Infobox film
| name           = Wild West Comedy Show: 30 Days & 30 Nights - Hollywood to the Heartland
| image          = Wild West Comedy Show- 30 Days & 30 Nights – Hollywood to the Heartland.jpg
| caption        =
| director       = Ari Sandel
| producer       = Peter Billingsley John Isbell John M. Pisani Sandra J. Smith Victoria Vaughn Vince Vaughn
| writer         =
| starring       = Ahmed Ahmed Sean Fitzgerald Peter Billingsley John Caparulo Bret Ernst Justin Long Sebastian Maniscalco Keir ODonnell Vince Vaughn
| music          = John OBrien
| cinematography =
| editing        = Jim Kelly Dan Lebental Picturehouse Entertainment
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $603,894   
}} stand up comedians. It premiered September 8, 2006 at the Toronto International Film Festival. It opened in wide release in the United States on February 8, 2008.

==Synopsis== Hollywood at improv sketches Midwestern states. The film highlights their performances on-stage and contains interviews with the various comedians.

==Cast==
*Ahmed Ahmed
*Peter Billingsley
*John Caparulo
*Bret Ernst
*Jon Favreau
*Justin Long
*Sebastian Maniscalco
*Keir ODonnell
*Vince Vaughn

==Production==
Vince Vaughn was a producer for the film. Vaughns older sister Victoria was also a producer. Chris Henkel, Paul Ruffolo, Dave Rutherford, and Jani Zandovskis served as camera operators. The show director was Chad Horning. The stage manager was Jason Ruffolo. 

==Critical reception==
The film received mixed reviews from critics. The review aggregator Rotten Tomatoes reported that 58% of critics gave the film positive reviews, based on 85 reviews, with the consensus that the film "has some entertaining moments, but is a mostly hit-and-miss documentary."  Metacritic reported the film had an average score of 51 out of 100, based on 24 reviews. 

Peter Hartlaub of the San Francisco Chronicle wrote "Its a funny comedy, and sometimes an even better drama" and called it a "nice companion piece" to the 2002 film Comedian (film)|Comedian. Hartlaub said "The comics all have their good and bad moments, but John Caparulo is arguably the most hilarious both on- and offstage" and "the movie is best when Vaughn plays off his own pop culture stature." Hartlaub also wrote that "director Ari Sandel paces the film well." 
 Gulf Coast. Holden wrote "In the most revealing scene Mr. Vaughn and his crew visit an Alabama trailer camp to give free tickets to the residents, many of them New Orleans evacuees who lost everything." 

Kyle Smith of the New York Post gave the film one star out of four, writing "A 2½-year-old collection of mediocre stand-up routines and dull backstage chatter, Vince Vaughns Wild West Comedy Show demonstrates why comedy clubs require you to have a couple of drinks." Smith said "Only about 40 percent of the movie is even comedy; the rest consists of lots of shots of maps" and said the film was "worse than open-mike night." 

==Box office performance==
The film opened in wide release in the United States on February 8, 2008 and grossed $464,170 in 962 theaters that weekend, averaging $483 per theater.  The film grossed a total of $603,894 after three weeks in theaters.  It has often been considered a box office bomb.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 