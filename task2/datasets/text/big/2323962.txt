Teenage Mutant Ninja Turtles (1990 film)
 
{{Infobox film
| name           = Teenage Mutant Ninja Turtles
| image          = TMNTMoviePoster.jpg
| caption        = North American release poster
| director       = Steve Barron
| screenplay     = Todd W. Langen Bobby Herbeck
| story          = Bobby Herbeck
| based on       =  
| producer       = Simon Fields David Chan Kim Dawson
| starring       = Judith Hoag Elias Koteas Brian Tochi Robbie Rist Corey Feldman Josh Pais Raymond Serra
| music          = John Du Prez
| cinematography = John Fenner
| editing        = Sally Menke Golden Harvest Mirage Enterprises Northshore Investments
| released       =    
| runtime        = 93 minutes
| distributor    = New Line Cinema
| country        = United States  English
| budget         = $13.5&nbsp;million 
| gross          = $201.9 million
| succeeded by   =  
}} Splinter and Casey Jones, The Shredder and his Foot Clan. The film stars Judith Hoag and Elias Koteas in supporting roles, along with the voices of Brian Tochi, Robbie Rist, Josh Pais and Corey Feldman.
 Donatello and Raphael &mdash; 1987 TV series that was airing at the time, such as April being a news reporter, and the turtles having different-colored masks, as opposed to the uniform red masks of the comic. 

The film became the second-highest-grossing independent film of all time, and became the ninth-highest-grossing film worldwide of 1990. It was the most successful film in   in 1991, &   in 2007. 

==Plot==
  weapon as storm sewers, turtles return Raphael admits Casey Jones who tries to exact justice on a pair of muggers by beating them with sporting implements. After a brief stand off between the two, Casey takes off into the city and Raphael loses his temper. Returning home, Splinter confronts Raph about his temper.
 subway by Donatello and The Shredder, who acts as a surrogate father to the outcast teenagers. Danny gives The Shredder his information on the Turtles.

After a heated argument between Leo and Raph, Raph goes up to the roof of Aprils building and is attacked by dozens of Foot Clan members. The fight moves into the building, where April and the others are attacked after Raph is thrown down a skylight window. The numbers cause the floor to give way, and they continue their battle in the basement.  After Foot reinforcements arrive, Casey appears, having seen Raph earlier on the roof and he helps even the odds. In the melee, with Raph in a coma, the building catches fire, forcing the Turtles to escape. Retreating to an abandoned farm Aprils family owns, April learns that Charles has fired her from the television station.  Raph awakens from his coma and the four of them train. After receiving visions from Splinter, Leonardo and the rest decide it is time to return to the city. Meanwhile, Danny encounters Splinter, whose kindness casts doubts on his decision to betray the Turtles. He goes to hide in the Turtles den, and is there when they return. In the night, Danny slips away, followed by Casey who discovers the thieves hideout. Danny meets with Splinter again, who tells him the story of his master who was killed by a man named Oroku Saki; who is The Shredders alter-ego. He inadvertently warns Shredder that the turtles have returned and they raid the Turtles home. They are ambushed however as the group is lying in wait for them.

Casey and Danny free Splinter, and after Casey defeats Shredders second in command Tatsu, they convince the teens to follow them and teach them the error of their ways. They return to the city as the Turtles face off against Shredder, who outmatches them. Splinter then faces Shredder, revealing his identity as the rat who belonged to Hamato Yoshi. Shredder charges Splinter in his rage and falls over the ledge of the roof. Splinter tries to spare him, but Shredder attacks, forcing the rat to let go, and he falls into a garbage truck. Casey turns on the trash compactor and crushes Shredder. April is reinstated by Charles, with better perks, to cover the story.  Danny reunites with his father who is overjoyed to see him, but Danny insists on being called "Dan" as a sign that hes grown up. Casey and April meet, and she tells him to kiss her, which he obliges, much to the cheering from the Turtles as "Turtle Power" plays over the credits.

==Cast==

===Live-action actors===
* Judith Hoag as April ONeil, a reporter for Channel 3 News Casey Jones, a street punk who becomes an ally of the Turtles
* Josh Pais as Raphael (in-suit performer)
* Michelan Sisti as Michaelangelo (in-suit performer)
* Leif Tilden as Donatello (in-suit performer)
* David Forman as Leonardo (in-suit performer)
* Michael Turney as Danny Pennington, Charless teenage son and a member of The Foot
* Jay Patterson as Charles Pennington, Aprils boss
* Raymond Serra as Chief Sterns, the Police Chief of New York City The Shredder, the main antagonist of the film
* Toshishiro Obata as Tatsu, Shredders second-in-command
* Sam Rockwell as Head Thug
* Skeet Ulrich as Thug (uncredited)
* Ernie Reyes, Jr. as Donatello in-suit martial arts stunt double

===Voice cast===
* Josh Pais as Raphael (Teenage Mutant Ninja Turtles)|Raphael, the rebellious and angry Turtle
* Robbie Rist as Michelangelo (Teenage Mutant Ninja Turtles)|Michelangelo, the fun loving, party Turtle
* Brian Tochi as Leonardo (Teenage Mutant Ninja Turtles)|Leonardo, the leader of the Turtles and the closest to Splinter
* Corey Feldman as Donatello (Teenage Mutant Ninja Turtles)|Donatello, the brains of the TMNT
* Kevin Clash as Splinter (Teenage Mutant Ninja Turtles)|Splinter, the Turtles master
* David McCharen as The Shredder
* Michael McConnohie as Tatsu

===Puppeteers===
* David Greenaway as Raphael (facial assistant)
* Mak Wilson as Michelangelo (facial assistant)
* David Rudman as Donatello (facial assistant)
* Martin P. Robinson as Leonardo (facial assistant)
* Kevin Clash as Splinter (puppeteer)
* Rickey Boyd as Splinter (facial assistant)
* Robert Tygner as Splinter (assistant puppeteer)


NOTE: All four actors who played the in-suit Turtles also appeared in cameos as minor characters, with David Forman (Leonardo) as a gang member, Michelan Sisti (Michaelangelo) as a pizza delivery man, Leif Tilden (Donatello) as a messenger of The Foot and Josh Pais (Raphael) as a passenger in a taxi. Pais was the only actor to portray a Turtle on screen and provide his voice.

==Production==
 
Filming took place from July to September 1989.    The films budget was $13.5&nbsp;million.  Much of the production took place in North Carolina (with a couple of location shoots in New York City during the summer of 1989 to capture famous landmark areas such as Times Square, Empire State Building, and the Hudson River), at the North Carolina Film Studios, where New York rooftop sets were created. Production designer Roy Forge Smith and his art director, Gary Wissner, went to New York City four months prior to filming and took still photographs of rooftops and other various locations. While in NYC, Smith and Wissner were allowed to explore an abandoned Brooklyn subway line, as they could not gain access to a city sewer, but the structure of the subway had the same principle as a sewer. They also went to a water tunnel which had large pipes running through it.   

After design sketches were created, the construction team used the studios backlot to create some of the sets. There were problems with the manholes that led to the Turtles home, in that an eight-foot square room had to be constructed beneath them, but found water at about five-feet, and thus had to pour concrete into the underground rooms to keep the water out. In order to make the sewer authentic, a tide-mark was given, and it was covered with brick, plaster and stucco paint to give the walls a realistic look. The Turtles themselves were created by Jim Hensons Creature Shop in London. Jim Henson said that the creatures were the most advanced that he had ever worked with. The creatures were first made out of fiberglass, and then remolded out of clay.  They were produced as molds to cast the whole body in foam rubber latex. The work at the Shop was completed within 18 weeks. 
 Paramount (whose Masters of the Universe was just a couple years prior. The film finally found distribution roughly halfway through the initial production via the then small and independent production company New Line Cinema which at that point had been known more for distributing low budget B movies and arthouse fare.

==Marketing==
Live Entertainment Inc. announced that the film would go to VHS via its   at The Deseret News. July 22, 1990. Retrieved on September 6, 2011. 

==Alternate versions==
The UK version was severely censored due to its censorship guidelines considering Eastern fighting weapons like the nunchaku. Alternate shots of Michaelangelo were used in order to conceal his nunchaku weapon, or omitted altogether - for instance, the show-off duel between Michaelangelo and a member of the Foot clan. Also, the death scene of Shredder was heavily cut because of this and the Turtle Power song was edited to change the word ninja to hero as per the UK TV series. The uncensored version was released on DVD in 2004 in the UK due to relaxations of the censorship laws. 
The German theatrical Dubbing (filmmaking)|voice-dubbed version is identical with the UK version, i.e. it omits the usage of the nunchaku. Furthermore, the German dubbing audio track contains several "cartoon-like" sounds in order to soften the violence of the fight scenes. Although the German dub of the film was released with uncensored picture on DVD in Germany, the German dub audio version with the "funny noises" was still kept, because they were permanently merged into the German voice-dubbing audio.

==Reception==
The film was a commercial success and was praised by its fanbase, but received mixed reviews from critics.    Based on a sample of 49 reviews, the film holds a 41% rating score on   gave it 2½ stars out of 4, saying, "this movie is nowhere near as bad as it might have been, and probably is the best possible Teenage Mutant Ninja Turtle movie. It supplies, in other words, more or less what Turtle fans will expect." The film was also criticized for its level of violence, though Ebert opined that it was mostly stylized and not graphic.   

===Box office===
The film opened at the box office in North America on March 30, 1990, entering at #1 over the weekend and taking in more than $25&nbsp;million.   The film turned out to be a huge success at the box office, eventually making over $135&nbsp;million in North America, and over $66&nbsp;million outside North America for a worldwide total of over $200&nbsp;million, making it the ninth highest grossing film of 1990 worldwide.    The film was also nominated for awards by The Academy of Science Fiction, Fantasy and Horror Films. 

==Home release==
In 1990, the film was released to VHS  and reached No.4 in home video market.  The film was released to DVD in Region 1 on September 3, 2002; it includes only minor special features such as a trailer and interactive menus. The film was also released in the MiniDVD format. 

On August 11, 2009, the film was included in a special 25th anniversary box set (25th anniversary of the original comic book, not the movie), released to both DVD and  , Teenage Mutant Ninja Turtles III, and 2007s animated release, TMNT (film)|TMNT. No additional features, other than theatical trailers, were included.

In Germany, however, a "Special Edition" was released on March 12, 2010 with additional features, including an audio commentary by director Steve Barron, an alternate ending, and alternate takes from the original German release where Michelangelos nunchaku had been edited out. 

Warner Home Video released the film along with Secret of the Ooze and Teenage Mutant Ninja Turtles III as part of a "Triple Feature" on Blu-ray in June 2012, minus the fourth film TMNT. Warner Home Video released the film separately on Blu-ray on December 18, 2012.

==Soundtrack==
 

==Legacy== a fourth CGI animated Teenage Mutant Ninja Turtles, was released in 2014. 

==References==
 

==External links==
 
 
*   on the Official Ninja Turtles website.
*  
*  
*  
*  
*   at the Official Ninja Turtles website.

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 