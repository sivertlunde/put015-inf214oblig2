Spring (2014 film)
 
{{Infobox film
| name           = Spring
| image          = Spring (2014 film) POSTER.jpg
| alt            = 
| caption        = 
| director       = {{Plainlist| Justin Benson
* Aaron Moorhead}}
| producer       = {{Plainlist|
* Justin Benson
* Aaron Moorhead
* David Clarke Lawson, Jr.}}
| writer         = Justin Benson
| starring       = {{Plainlist|
* Lou Taylor Pucci
* Nadia Hilker}}
| music          = {{Plainlist|
* Jimmy LaValle
* Sigur Rós}}
| cinematography = Aaron Moorhead
| editing        = {{Plainlist|
* Justin Benson
* Aaron Moorhead
* Michael Felker}}
| studio         = XYZ Films
| distributor    = {{Plainlist| Drafthouse Films
* FilmBuff}}
| released       =  
| runtime        = 109 minutes  
| country        = United States
| language       = {{Plainlist|
* English
* Italian}}
| budget         = 
| gross          = $46,226 
}} supernatural Romance romantic Science science fiction Justin Benson and Aaron Moorhead. 

==Cast==
* Lou Taylor Pucci as Evan
* Nadia Hilker as Louise
* Vanessa Bednar as Gail
* Shane Brady as Brad
* Francesco Carnelutti as Angelo
* Holly Hawkins as Nicole Chris Palko as Bancroft Dawson

==Release== limited theatrical Drafthouse Films  and an video on demand release a day later, through FilmBuff. 

===Critical reception===
The film received positive reviews from critics. On  , the film has a 68 out of 100 rating out of 11 critics, indicating "generally favorable reviews". 

Jon Dickinson of SCREAM: The Horror Magazine gave Spring a 5 star rating, stating that it "transcends all genres to deliver a story that feels entirely unique…a monster you won’t want to miss."  

===Accolades===
;Austin Fantastic Fest:
* Next Wave Award for Lou Taylor Pucci as Best Actor 

;Palm Springs International Film Festival
* Directors to Watch for Justin Benson & Aaron Moorhead 

==Soundtrack==
Eastern Glow Recordings released the soundtrack on 25 March 2015,  which was composed by Jimmy LaValle.  The Soundtrack featured songs from Sigur Rós. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 

 