South Solitary
 
{{Infobox film
| name           = South Solitary
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Shirley Barrett
| producer       = Miranda Culley
| writer         = Shirley Barrett
| screenplay     = 
| story          = 
| based on       = 
| starring       = Miranda Otto Marton Csokas
| music          = Mary Finsterer Anna Howard
| editing        = 
| studio         = 
| distributor    = Icon Film Distribution
| released       =  
| runtime        = 121 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = $5441 (New Zealand) A$279,341 (Australia) 
}}

South Solitary is a 2010 Australian romance film set on South Solitary Island and directed by Shirley Barrett.

==Plot==
 
Meredith Appleton (Miranda Otto) arrives on South Solitary island with her elderly uncle George Wadsworth (Barry Otto).  Her uncle has been sent as the new replacement Head Lighthouse Keeper as the previous one committed suicide.  Her uncle is shown to be a gruff man very critical of the slightest disorder.  Meredith meets the girl Nettie (Annie Martin), a local girl whom she asks to care for her lamb.  She then meets the mother, Alma Stanley (Essie Davis) and her two sons, Tom (Reef Ireland) and Robbie (Benson Adams).  The father, Harry Stanley (Rohan Nichol) takes the George for a tour of inspection.  George is disapproving of the state of the loose animals, the absence of lighthouse keeper Jack Fleet (Marton Csokas), and a previous history of lighthouse outages.  The first night, as Meredith is preparing tea for her uncle, she hears baby birds chirping in the stovepipe.  She douses the fire and is not able to prepare hot tea for her uncle.  During Jack Fleets watch, he has a panic attack brought on by his war experiences.  Nettie introduces Meredith to the emergency message system using homing pigeons.  Nettie tries to send a message that all is well on South Solitary but the birds return to their coop thus rendering the system useless.  While having afternoon tea with Alma, we learn that Meredith is unable to give birth.  She lost her fiancee in the war, then became pregnant during an affair with a married man.  Complications cause her womb to be removed.  Harry visits her during her bath, and seduces her.  Alma becomes aware of her husbands infidelity, and informs George, then forces her family to leave the island on the next passing ship.  While pushing up a store of supplies from the beach, George is forced to overly exert himself and becomes very ill and dies.  Jack must keep solitary watch as there is no one else.  They bury George and provide a eulogy for him.  A whole gale forms while Meredith is at the lighthouse forcing her to stay there for several days.  Meredith learns how to turn the lighthouse and the Flag semaphore system.  She conveys her sense of loneliness to Jack and her desire for a sense of permanence.  Jack has a panic attack, seeing hallucinations of a ship in distress, and runs into the wilderness.  While going through the island, she finds her lost lamb, and Jack helps her recover it.  Jack sees a ship in the distance and signals for it to pick up Meredith.  She begs him to let her stay but he feels insecure being close to someone.  As she leaves, they kiss, and he promises to come visit her on the mainland.  Meredith is seen smiling as she floats away from South Solitary.

==Cast==
*Miranda Otto ... Meredith Appleton
*Marton Csokas ... Jake Fleet
*Rohan Nichol ... Harry Stanley
*Essie Davis ... Alma Stanley
*Barry Otto ... George Wadsworth

==Reviews==
Rotten Tomatoes gave the film a 71% with a rating of 5.6 out of 10. Out of 17 reviews counted, 12 gave it a positive review and 6 gave it a negative review. 

==External links==
 

==References==
 

 
 
 
 
 
 