Agent 3S3: Passport to Hell
{{Infobox film
| name           =Agente 3S3: Passaporto per linferno
| image          =Agent 3S3- Passport to Hell.jpg
| image size     =
| caption        =
| director       = Sergio Sollima
| producer       = Cesáreo González
| writer         = Jesús María de Arozamena, Alfonso Balcázar
| narrator       =
| starring       =
| music          =Piero Umiliani
| cinematography = Carlo Carlini
| editing        =Teresa Alcocer, Bruno Mattei
| distributor    =
| released       = 1965
| runtime        =
| country        = Italy Italian
| budget         =
}}
 1965 Italy|Italian eurospy film directed by Sergio Sollima, here credited as Simon Sterling. This is the first chapter in the Sollimas spy film trilogy, and inaugurated the film series of the Agent 3S3 played by George Ardisson.    It is also the first Sollimas full-length film, after the episode he filmed in Lamore difficile three years before. 

Location filming includes Spain, Rome, Beirut and Vienna.  This was followed by the sequel Agent 3S3, Massacre in the Sun (1966) also directed by Sollima.

==Cast==
*George Ardisson ...  Walter Ross, Agent 3S3
*Barbara Simon ...  Irmgard von Wittstein
*José Marco ...  Ahmed
*Georges Rivière ...  Professor Steve Dickson
*Franco Andrei ...  Bellamy (as Frank Andrews)
*Liliane Fernani ...  Karina (as Senta Heller)
*Seyna Seyn ...  Jackye Vein
*Charles Kalinski ...  Salkoff (as Karl Wirth)
*Francisco Sanz ...  Nobell (as Paco Sanz / Paul Fabian)
*Henri Cogan ...  Sanz (as Heinrich Rauch)
*Fernando Sancho ...  Colonel Dolukin (as Ferdinand Bergmann)
*Béatrice Altariba ...  Elisa von Sloot
*Sal Borgese ...  Man in Vienna Bar
*Jeff Cameron

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 
 