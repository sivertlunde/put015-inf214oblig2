A Gruesome Twosome
 
 
 
 }}
{{Infobox Hollywood cartoon|
| cartoon_name = A Gruesome Twosome Tweety
| image =
| caption = Title Card for A Grusome Twosome Robert Clampett
| story_artist =Warren Foster
| animator = Robert McKimson  Rod Scribner   Manny Gould   Basil Davidovich A. C. Gamer (effects animation)
| voice_actor = Mel Blanc
| musician = Carl Stalling
| producer = Edward Selzer
| distributor = Warner Bros. Pictures
| release_date = June 9, 1945
| color_process = Technicolor
| runtime = 7 mins
| movie_language = English
}}

A Gruesome Twosome is a Warner Bros. Merrie Melodies cartoon directed by Bob Clampett and released on June 9, 1945. it stars Tweety, and two cats. This is the last Tweety film directed by Clampett, following 1942s A Tale of Two Kitties and 1944s Birdy and the Beast,  and the last one before he is permanently paired with Sylvester the Cat, and the last one that Tweety have no feathers.

==Plot==
The cartoon opens up with two cats (a couple) talking/singing in meows (set to the tune of the 1909 pop-tune, Me-ow (song)|Me-ow). Then we see a red cat (a caricature of Jimmy Durante) who hits a yellow cat talking to the female cat with a frying pan. Then the red cat tries to get the girl to kiss him. As he tries to kiss her, out of nowhere an insane dog (who admits that he "  actually belong in this picture") pops up and kisses the female cat, prompting the Durante cat to exclaim that "Everybody wants to get into the act. Umbriago! ITS DISGUSTING!" As the two cats fight, the female tells them whoever can bring her a bird can be her "fella". Then the red cat uses numerous tricks to stop the yellow cat. Then we see the cats climbing up the pole. Tweety states his usual catchphrase "I tawt I taw a putty tat" followed by "I tawt I taw ANOTHER putty tat". Then both cats realize that the other one is up with them and they fight and Tweety hits them with a mallet causing them to fall off the pole; then Tweety states "Aw, the poor puddy tats! They fall down and go BOOM!" (on the word BOOM, Tweety yells at the top of his lungs) and smiles. Then the red cat wakes up, and so does the yellow but the yellow gets hit by a frying pan again. Then the cats fight again and the red cat realizes that they must use strategy (which the Durante cat pronounces "stragedy"). Then the red cat comes up with a plan. We then see them in an unrealistic horse costume and the red cat states that he is the head. Then Tweety pops out of the tail and grabs a bee. As he slaps it he puts it in the horse costume and hits the costume and the cats get stung and crash into a tree. Then the red cat comes up with another plan. But then Tweety lures a dog to attack the cats by grabbing its bone and putting it into the cats costume and the cats get attacked; then Tweety says "You know I get wid of more putty tats that way!" then does the Durante "hot cha cha cha" as the film irises out.

==DVD release==
A Gruesome Twosome is presented (uncut and restored) on the  .

==References==
 

== External links ==
*  

 
 
 
 