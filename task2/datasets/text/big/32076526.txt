Jaag Utha Insan (1984 film)
 
{{Infobox film
 | name = Jaag Utha Insan
 | image = JaagUtha.jpg
 | caption = DVD Cover
 | director = K Vishwanath
 | producer = Rakesh Roshan
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Sridevi Rakesh Roshan Sujit Kumar Deven Verma
 | music = Rajesh Roshan
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = July 06, 1984
 | runtime = 135 min.
 | language = Hindi Rs 2 Crores
 | preceded_by = 
 | followed_by = 
 }} 1984 Hindi Indian feature directed by Telugu also directed by K Vishwanath.

It is a love story between a Harijan boy and a Brahmin Girl, played by Mithun Chakraborty and Sridevi respectively in the lead role, supported by Rakesh Roshan, Sujit Kumar and Deven Verma.

==Plot==
In the film, Sandhya - a Brahmin dancer - i.e. Sridevi falls in love with Hari (Mithun Chakraorty), a Dalit. She visits her maternal grandfather - who is a staunch Hindu pundit - accompanied by her father and Hari, to perform at the village temple, where her grandfather is the head-priest. There, the grandfather decides to get Sandhya married to his adopted grandson Nandu (Rakesh Roshan), who is also a priest at the temple, but with a broader mind. Hari, citing the apparent caste difference, urges Sandhya to marry Nandu.

Hence Sandhya marries Nandu. However on their wedding night, when Nandu enters the bedroom, he sees a Devi in Sandhya, and not his wife. This continues for a few nights, and people start talking about Nandu spending his nights outside the house. Before long, Sandhya confronts Nandu at the temple and asks him to try and accept her. Here Nandu tells her that he sees a Devi in her, and not his wife, because she belongs to someone else and not him. They are bound into this marriage by the Pundits chants, mantras, and the seven pheras, but Sandhya has taken the eighth phera, which is the phera of the heart and mind, with someone else, and thus she belongs to that person alone, and must go back to him, whoever that may be.

Nandu explains this to the rest of the family, and the grandfather accepts Sandhyas love for Hari, in spite of Hari being a Harijan. This comes as a result of Nandu explaining the truth, that every person is a human being first, and a Shudra at birth. It is only because of his/her deeds that a person becomes a Brahmin. The common misinterpretation of the Hindu caste system is that, a person acquires his caste at birth, which is not so, as explained above.

Hari soon gets this news, and quickly arrives at the village. Even though the head priest of the village, Nandus grandfather accepts Sandhya and Haris relationship, his son and the rest of the village opposes it, and a fight breaks out. In the end both, Hari and Sandhya lose their lives.

Deven Verma, who plays the head priests blood-related grandson, provides for the counter view throughout the film, and is the only one besides Nandu, who understands the real meaning of the Vedas, and supports him towards the end.

==Cast==
*Mithun Chakraborty
*Sridevi
*Rakesh Roshan
*Sujit Kumar
*Deven Verma

==References==
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Jaag+Utha+Insan
*http://www.imdb.com/title/tt0087492/

==External links==
*  

 
 
 
 
 