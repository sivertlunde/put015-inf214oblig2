The Compleat Beatles
 
{{Infobox film
| name     =The Compleat Beatles 
| image          = Compleat Beatles.JPG
| caption        = videocassette cover
| writer         = David Silver 
| narrator       = Malcolm McDowell
| starring       = John Lennon Paul McCartney George Harrison Ringo Starr George Martin  Marianne Faithfull Bruce Johnston Billy J. Kramer Gerry Marsden Billy Preston Tony Sheridan
| director       =Patrick Montgomery 
| producer       =Patrick Montgomery, Stephanie Bennett
| music          =The Beatles 
| distributor    =MGM
| released   =May 28, 1982 
| runtime        =119 min. 
| language =English 
| movie_series_label=
| movie_series   =
| awards         =
| budget         =
}}
The Compleat Beatles, released in 1982, is a two-hour documentary, chronicling the career of  "
is a tongue-in-cheek reference to the intentional misspelling of "Beetles" and a reference to the famous book on fishing, The Compleat Angler.
 DJ Bob Wooler, music writer Bill Harry, and musicians Gerry Marsden, Billy J. Kramer, Marianne Faithfull, Billy Preston and Tony Sheridan. The film also includes archival footage of interviews with members of the Beatles and their manager Brian Epstein. Authors Nicholas Schaffner and Wilfred Mellers are among the commentators who offer their views on the bands career. The Compleat Beatles also features early concert footage, behind-the-scenes background on the making of their albums, and candid footage of their often obsessed, hysterical fans.

Directed by Patrick Montgomery,  the film was produced by Delilah Films/Electronic Arts Pictures and released theatrically by Metro-Goldwyn-Mayer and United Artists in 1984.

==Video Releases==

The Compleat Beatles was initially released as a PBS documentary in the United States, and then on VHS and Laserdisc that same year on the MGM/UA Home Video label.  The 1982 Laserdisc was released in both Analogue and Stereo versions, as well as being released in Japan and England (in PAL format) in 1983. 

The film did very well, and in 1984 Delilah Films and Metro-Goldwyn-Mayer arranged for it to be released theatrically in the U.S. by a small distributor named Teleculture. This contributed to its continuing to be a best seller on VHS. Some years later, when Paul McCartney was preparing the Beatles Anthology, he bought the negative and all the rights to the film from Delilah to get it off of the market and clear the way for his production.  That, according to the films director Patrick Montgomery, is why it is not available on DVD or any newer formats and "probably never will be." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 