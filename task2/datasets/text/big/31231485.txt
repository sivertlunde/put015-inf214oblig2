Raton Pass (film)
{{Infobox film
| name           = Ratón Pass
| image          = 
| image_size     = 
| caption        = 
| producer       = Saul Elkins
| director       = Edwin L. Marin
Oren W. Haglund (assistant director)
| writer         = 
| narrator       = 
| starring       = Dennis Morgan
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = April 7, 1951
| runtime        = 84 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = $1.3 million (US rentals) 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Ratón Pass is a 1951 film directed by Edwin L. Marin. It stars Dennis Morgan and Patricia Neal. 

==Plot==
A feud  in 1880 New Mexico territory pits wealthy rancher Pierre Challon and son Marc versus homesteaders on the other side of Raton Pass|Ratón Pass.

Two strangers arrive by stagecoach, a ruthless man named Van Cleave and an attractive woman, Ann, who promptly seduces and marries Marc Challon, blissfully unaware that shes strictly in it out of greed, not love.

While he and his father are away on business, Ann offers to work out a land irrigation deal with Prentice, a banker. But when her husband returns, he finds Ann and Prentice are romantically involved and planning to swindle him.

Marc sells the ranch to Ann and his father leaves the territory. Lena Casamajor, a homesteaders niece, has always loved him. She sets off to bring Pierre back after Anns new foreman, Van Cleave, cold-bloodedly shoots Marc in the back and also her uncle as well.

Fed up with her ways, Prentice leaves her, but Van Cleave kills him and the sheriff. The Challons are the only ones who can stop him, and they do, but not before a shot by Van Cleave accidentally kills Ann.

==Cast==
*Dennis Morgan as Marc Challon
*Patricia Neal as Ann Challon
*Steve Cochran as Cy Van Cleave
*Scott Forbes as Prentice
*Dorothy Hart as Lena Casamajor

==References==
 

==External links==
* 

 

 
 
 