Touring Talkies (film)
 

{{Infobox film
| name           = Touring Talkies
| narrator       = Vinay Apte
| director       = Gajendra Ahire
| producer       = Trupti Bhoir
| story          = Gajendra Ahire
| screenplay     = Gajendra Ahire
| starring       = Subodh Bhave, Trupti Bhoir,  Milind Shinde,  Kishore Kadam, Vaibhav Mangle,  Chinmay Sant,  Neha Pense 
| music          = Ilaiyaraaja
| country        = India
| cinematography = Mahesh Limaye
| released       =  
| language       = Marathi 
| budget         = 
}}

Touring Talkies is a 2013 Marathi language drama film on the topic of Indian Cinema. The movie is based on the concept of roaming cinema which was one of the oldest traditions in the bygone era of Indian cinema where the movies were showcased in tents for the local folks also knows as Touring talkies. For the past few decades these films have made a common recurring occurrences in most of the Carnival Fun fair taking place across villages where the Marathi Cinema is adored in a completely distinctive fashion

==Cast==
* Trupti Bhoir as Chaandi
* Subodh Bhave 
* Milind Shinde as antagonist
* Kishore Kadam
* Vaibhav Mangle
* Chinmay Sant
* Neha Pense

==Music==
The songs of the film are composed by Ilaiyaraaja.

==Reception==
The film was nominated for the Oscar. 

==References==
 

==External links==
*  

 
 
 
 
 

 