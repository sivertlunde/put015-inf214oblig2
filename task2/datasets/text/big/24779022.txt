Like a Dragon: Prologue
{{Infobox film
| name           = Like a Dragon: Prologue 龍が如く 〜序章〜
| image          = Ryu ga gooku prologue dvd.jpg
| caption        = original DVD box
| director       = Takeshi Miyasaka, Takashi Miike  (executive director) 
| producer       = Artport
| writer         = Masaru Nakamura  (credited as NAKA雅MURA) 
| starring       = Masakatsu Funaki Mikio Ohosawa Ayaka Maeda Hirotaro Honda 
| music          = Kentaro Nijima 
| cinematography = 
| editing        = 
| distributor    = Sega
| released       = Japan: March 24, 2006  (DVD)  Europe/UK: August 15, 2006  (VOD) 
| runtime        = 43 min. Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 Japanese as crime drama Original Video directed by Takeshi Miyasaka with Takashi Miike as executive director. It is based upon Toshihiro Nagoshis 2005 video game Yakuza (video game)|Yakuza released on PlayStation 2.

This drama stars  .

==Plot==
 s Kabukicho (歌舞伎町) district which itself served as a basis for the background design of the games Kamurocho area.]]
 

During the 1970s three children, Kazuma Kiryu, Akira Nishikiyama (a.k.a. Nishiki) and his younger sister, Yuko Nishikiyama, are raised in Shintaro Kazama (a.k.a. Fuma)s Sunflower Orphanage. In summer 1980, Yumi Sawamura, a young girl who had her parents incidentally shot during a gangs shootout joins them. Following a yakuza tradition, the honorable Kazama secretly raises orphans he has directly or indirectly killed the parents. In return, these children look at him as their father and he eventually introduces the teenagers in the Dojima Family, a Tojo Clan affiliate.

Years later the promising Kazuma Kiryu quickly rises the yakuza hierarchy and earns the nickname "the Dragon of the Dojima Family" for the Dragon irezumi tattoo on his back (hence the original title  ). His childhood friend Nishikiyama his torn between loyalty for his kyodai (yakuza "brother") and jealousy against the one who has always been Kazamas protégé. Another subject of rivalry between the two friends is their secret love for Yumi who looks at them as her older brothers. 1990, in order to remain close to both of them, she left the orphanage and moved to Tokyos red-light district Kamurocho, where they found her a job as hostess at Reina (Yakuza series)|Reinas Serena bar.

October 1, 1995, Kazuma Kiryu announces his friends he is ready to create his own yakuza Family, only lacks the Chairman of the Dojima Family Sohei Dojimas go ahead. Later that night the latter kidnaps Yumi from Serena, Nishikiyama tries to interfere but Dojimas men hold him. When Nishikiyama eventually reaches Dojimas office, he finds his boss raping Yumi and shoots him dead. Kazuma who was at a meeting with Kazama had been called by Reina and comes shortly after only to find Dojima on the ground, Nishikiyama and Yumi in shock. Then Kazuma takes the responsibility in order to protect Yuko who needs her brother Nishikiyama as she is about to get a last chance operation. Kazuma orders the pair to leave before the police arrives.

==Cast==
 
*Masakatsu Funaki as Kazuma Kiryu (桐生一馬)
*Mikio Ohosawa as Akira Nishikiyama (錦山彰)
*Ayaka Maeda as Yumi Sawamura (澤村由美)
*Harumi Sone as Sohei Dojima (堂島宗兵)
*Hirotaro Honda as Shintaro Kazama (風間新太郎)
*Unknown actor as Sunflower principal

==Video==
As a direct-to-video product it was released on DVD by Sega on March 24, 2006 in Japan.  Extra material included game Full motion video|FMVs.
 VOD with four chapters called "episodes" available for download.

==References==
 

==External links==

 

 
 
 
 
 
 
 
 
 