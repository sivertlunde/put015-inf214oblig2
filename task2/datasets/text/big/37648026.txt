A Cross the Universe (film)
 

{{Infobox film
| name           = A Cross the Universe
| image          = A Cross the Universe.jpg
| caption        =
| director       = Romain Gavras, So-Me, Justice
| producer       = Gaspard Augé, Xavier de Rosnay, Romain Gavras, So Me Justice
| music          = Justice
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 65 minutes
| country        = France
| language       = French, English
| budget         =
}}
 French electronic Justice directed Justice themselves.

==Background==
The film follows Justices March 2008 North American tour. This documentary is directed by Romain Gavras, So Me and the band themselves. The documentary is meant to cover less of the bands live shows, and more of their personal experience touring. 

The live portion of this release was recorded at a concert in San Francisco, California at the Concourse Exhibition Center, on March 27, 2008. 
 A Cross the Universe.

The films style inspired award-winning 2012 documentary film Lets Make Lemonade directed by Justin Friesen. 

==DVD song credits==
{| class="wikitable plainrowheaders" border="1"
|- Title
!scope="col" Notes 
|-
!scope="row"|"DVNO"
|
|-
!scope="row"|"D.A.N.C.E.|D.A.N.C.E"
|
|-
!scope="row"|"NY Excuse" Is a Justice remix of the track of the same name by Soulwax
|- Phantom Part 2" Contains samples Goblin
|-
!scope="row"|"Waters of Nazareth (Prelude)" Contains samples The Fallen" Franz Ferdinand
|-
!scope="row"|"Genesis"
|
|-
!scope="row"|"We Are Your Friends (Reprise)" Contains samples Ministry
|-
!scope="row"|"Final" Contains samples Master of Puppets" by Metallica
|-
!scope="row"|"Pedrophilia" Song by Busy P
|-
!scope="row"|"Pocket Piano" Song by DJ Mehdi
|-
!scope="row"|"Psychotic Reaction" Song by Count Five
|-
!scope="row"|"Love Me, Please Love Me" Song by Michel Polnareff
|}

==Reception==
Reception of the film has been positive.

Justine JC of Scene Crave said: "The cinematography is artistic and raw, yet deft, making the mess of a story line (well, lack there of…) strangely engaging. The film is literally an arbitrary compilation of the people and events involved in the band’s tour."  

==References==
 

==External links==
 

 
 
 
 
 