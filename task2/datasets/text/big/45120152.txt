Meltdown (2004 film)
{{Infobox film
| name           = Meltdown
| image          = 
| alt            = American Meltdown
| caption        = 
| film name      =  
| director       = Jeremiah Chechik
| producer       =   Paul Barber
| screenplay     = 
| story          = 
| based on       =  
| starring       = Bruce Greenwood, Leslie Hope, Arnold Vosloo
| narrator       =  
| music          = 
| cinematography = 
| editing        = 
| production companies = Craig Anderson Productions, Beth Grossbard Productions, ApolloScreen Filmproduktion GmbH & Co., Filmproduktion KG
| distributor    = FX Network
| released       =  
| runtime        = 
| country        = 
| language       = English, Arabic
| budget         = 
| gross          =  
}}
 FOX on 6 June 2004. A action-drama-thriller film is about terrorist take-over of the fictional San Juan Nuclear Plant in southern California.

==Cast==
* Bruce Greenwood - Agent Tom Shea
* Leslie Hope - Zoe Cox
* Arnold Vosloo - Khalid / Sands
* James Remar - Colonel Boggs
* Susan Merson - Attorney General Zutrow
* Will Lyman - Homeland Chief Utley
* Bill Mondy - FBI Agent Tucci
* Brent Stait - FBI Commander Hall
* Adrian Holmes - Agent Charlie Jansen
* Manoj Sood - Syed Kahn
* Tony Alcantar - Reactor Tech Chuck Pasquin
* Diego Diablo Del Mar - Marwan / Raul (as Diego Del Mar)
* Alessandro Juliani - Salem / Emmett
* Darren Shahlavi - Waleed / Frank
* David Neale - Shafig / Jesse

==External links==
* 

 
 
 
 
 
 
 
 
 

 
 