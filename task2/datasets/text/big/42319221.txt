Heaven over the Marshes
{{Infobox film
| name =  Heaven over the Marshes
| image =Cielo_sulla_palude_locandina.jpg
| image_size =
| caption =Inés Orsini (actress) with Alessandro Serenelli (Maria Goretti murderer) in 1950
| director = Augusto Genina 
| producer =  Carlo José Bassoli   Renato Bassoli  
| writer =   Suso Cecchi DAmico    Elvira Psorulla    Fausto Tozzi   Augusto Genina  
 | narrator =
| starring = Rubi DAlma   Michele Malaspina   Domenico Viglione Borghese   Inés Orsini
| music = Antonio Veretti   
| cinematography = G.R. Aldo  
| editing = Otello Colangeli   Edmond Lozzi     
| studio = Film Bassoli   
| distributor = 
| released = 24 November 1949
| runtime = 111 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama 100 Italian films to be saved.   The films sets were designed by Virgilio Marchi.

==Cast==
* Rubi DAlma as La contessa Teneroni 
* Michele Malaspina as Il conte 
* Domenico Viglione Borghese as Il dottore 
* Inés Orsini as Maria Goretti 
* Assunta Radico as Assunta Goretti - La madre di Maria 
* Giovanni Martella as Luigi Goretti - il padre di Maria 
* Mauro Matteucci as Alessandro Serenelli 
* Francesco Tomalillo as Giovanni Serenelli - il padre di Serenelli 
* María Luisa Landín as Lucia 
* Ida Paoloni as Teresa 
* Federico Meloni as Angelo 
* Jole Savoretti as Anna 
* Giovanni Sestili as Mariano 
* Vincenzo Solfiotti as Antonio

== References ==
 

== Bibliography ==
* Moliterno, Gino. The A to Z of Italian Cinema. Scarecrow Press, 2009. 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 