What Every Woman Knows (1934 film)
{{Infobox film
| name           = What Every Woman Knows
| image          = What Every Woman Knows 1934.JPG
| image_size     =
| caption        = Helen Hayes in film trailer
| director       = Gregory La Cava
| producer       = Gregory La Cava Albert Lewin John Meehan James Kevin McGuinness
| story          =
| based on       =  
| narrator       =
| starring       = Helen Hayes Brian Aherne Madge Evans
| music          =
| cinematography =
| editing        =
| studio         = Metro-Goldwyn-Mayer
| distributor    =
| released       =  
| runtime        = 89-92 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} American romantic What Every Paramount back 1921 and Lois Wilson. An even earlier British silent version was filmed in What Every Woman Knows (1917 film)|1917.

==Plot== Dudley Digges) are greatly concerned about Alicks daughter Maggie (Helen Hayes), who has been jilted by a minister. She is less than  heartbroken, but they fear for her marital prospects at the age of 27. When they catch poor but ambitious 21-year-old John Shand (Brian Ahern) breaking into their house late at night to use their library, they seize the opportunity. Impressed by his initiative, they offer him £300 to finance his studies provided that he give Maggie the option of marrying him after five years. After some thought, he agrees. The Wylies insist he sign a formal contract.
 Parliament and wins. Two of the "quality", la Contessa la Brierre (Lucile Watson) and her niece Lady Sybil Tenterden (Madge Evans), show up to congratulate him. Lady Sybil had lately shown an unexpected interest in politics after seeing the handsome victor.

On the train to London to take his seat, John is given the opportunity by Maggie to back out of their agreement, but he avers that a bargain is a bargain and marries her. Lady Sybil helps forward Johns political career, and the two fall in love. Maggie too works diligently and craftily on her husbands behalf, planting ideas that he takes for his own. John has become a financial expert, and is for the country dropping the gold standard, which is against the policy of his Labour Party. When the influential politician Charles Venables (Henry Stephenson) comes to sound him out, Maggie sees him first and takes it upon herself to state that John would resign from the party rather than betray his principles. Venables is delighted.  
 
Matters finally come to a head on the Shands second wedding anniversary, with John telling Maggie and her family that he and Lady Sybil are in love. He insists on writing a letter of resignation from Parliament. Maggie persuades him to postpone their public separation for a month so he can finish writing his book. She arranges for him to stay at the estate of her good friend, la Contessa; secretly, she also asks la Contessa to invite Lady Sybil as well. Things turn out the way she had hoped: John and Lady Sybils ardor does not survive having to spend an entire month together.

Meanwhile, Maggie goes to see Venables. She presents him with Johns letter, telling him that her husband has resigned over the gold standard. Venables is so impressed he offers John an important post in the coalition government he is forming. When John finds out what Maggie has done, he is somewhat offended. However, she assures him that "what every woman knows" is that behind every successful man is a woman who secretly strives to help him. In the end, he sees things her way, and is cajoled into laughing for the first time in his life.

==Cast==
* Helen Hayes as Maggie Wylie
* Brian Aherne as John Shand
* Madge Evans as Lady Sybil Tenterden
* Lucile Watson as La Contessa la Brierre Dudley Digges as James Wylie
* Donald Crisp as David Wylie
* David Torrence as Alick Wylie
* Henry Stephenson as Charles Venables

==References==
 

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 