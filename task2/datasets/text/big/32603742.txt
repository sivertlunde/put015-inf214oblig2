Now Is Good
 
 
 
{{Infobox film
| name           = Now Is Good
| image          = Now Is Good poster.png
| alt            =  
| caption        = Theatrical release poster
| director       = Ol Parker
| producer       =  
| screenplay     = Ol Parker
| based on       =  
| starring       = {{Plain list |
* Dakota Fanning 
* Jeremy Irvine
* Paddy Considine
* Olivia Williams
* Kaya Scodelario
}}
| music          = Dustin OHalloran
| cinematography = Erik Wilson
| editing        = Peter Lambert
| studio         = Blueprint Pictures
| distributor    = BBC Films Warner Bros.
| released       =  
| runtime        = 103 minutes 
| country        = United Kingdom
| language       = English
| budget         = $500,000
| gross          = $2,699,265 
}} teen drama film directed by Ol Parker. Based on the 2007 novel Before I Die by Jenny Downham, it was adapted by Parker who had recently written the screenplay for The Best Exotic Marigold Hotel.    The film, which stars Dakota Fanning, Jeremy Irvine and Paddy Considine, centres on Tessa, a girl who is dying of leukaemia and tries to enjoy her remaining life as much as she possibly can.   

The first trailer for the film was released on 5 March 2012. 

==Plot== bucket list until an unplanned pregnancy test comes up positive.

Tessas parents are separated  and have very different views on her desire to experience the dangerous side of life before she dies. Her mother (Olivia Williams) is loving and joking about the situation and seems supportive whereas her father (Paddy Considine) is timid and just wants to spend time with his daughter. Tessas dad is resistant to Tessas behavior from the start but realizes he has little influence and can only enjoy the time they have left. Her fathers main mechanism for coping is denial. She mentions that he spends hours on the computer looking up possible treatments for her even after the doctors have told her that the cancer has consumed her body.

Tessas brother, Cal (Edgar Canham), is a brutally honest individual who expresses varying feelings throughout the novel, ranging from lack of care to jealousy to sadness. In the beginning of the film, Cal says to his sister "Im gonna miss you" during a joking situation.

One of Tessas last wishes is to find love, which she thinks she has achieved with her neighbour Adam (Jeremy Irvine). Adam is shy, and his main priority is taking care of his sickly mother since his fathers death.

The film follows Tessas last few months of life and explores her relationships with her loved ones and personal feelings about being trapped in a failing body. At the end of the film, Tessa dies.

==Cast==
* Dakota Fanning as Tessa Scott
* Jeremy Irvine as Adam
* Paddy Considine as Tessas father
* Olivia Williams as Tessas mother
* Edgar Canham as Cal, Tessas brother
* Kaya Scodelario as Zoey, Tessas friend
* Rose Leslie as Fiona Joe Cole as Scott
* Sarah Hadland as Caroline
* Patrick Baladi as Richard
* Franz Drameh as Tommy Susan Brown as Shirley
* Rakie Ayola as Phillipa
* Julia Ford as Sally
* Tom Kane as Paul 
* Isabella Laughland as Beth 
* Morgan Watkins as the nurse
* (Harrison Brown) as Baby

==Production==
 

===Development and writing===
Blueprint Pictures brought director and writer Ol Parker and cinematographer Erik Wilson together to tell Tessas story on the big screen. "I was brought into the project somewhat late in the game, but I connected with Ol right away, and loved the script", notes Wilson. "The decision to shoot on film had already been made, which I was very happy to hear. The movie is a performance driven piece with a very strong script, and I have always felt that the etiquette of film lends itself to drama".

No digital tests had been considered for the feature, and Wilson moved right into testing stocks. "I wanted to keep the look as clean, crisp and sharp as possible. The story was so strong; it didnt need help romanticizing it", says Wilson. The filmmakers choose KODAK VISION3 50D Color Negative Film 5203 and KODAK VISION3 250D Color Negative Film 5207 for daylight scenes, and KODAK VISION3 500T Color Negative Film 5219 for night shoots and one scene shot in Tessas bedroom. As the set for that particular sequence was located in a warehouse, the lighting set-up dictated the use of the 5219.

===Filming===
Now Is Good utilized two ARRI cameras, the ARRICAM Studio and the ARRICAM Lite. ARRIs Master Prime lenses were used throughout production. "I wanted a clear and bright image. There was no need to hide anything or build a look. The nature of the story against that crisp, unfiltered look stood on its own", Wilson continues.

The movie had a seven-week shooting schedule, and all but two days of production took place in and around London. Now Is Good is actually set in the seaside city of Brighton, but only two days of location shooting took place in Brighton and along Englands southern coast. A nighttime shoot in which Tessa and Adam go for an evening swim proved particularly tricky to capture. "We had to figure out how to light them in that scene," Wilson comments. "The sea at night, far from any city, offered no potential sources of light beyond the moon. We put some lights on a boat, and created a river outlet. Our gaffer, Andy Lowe, added some clever ideas, but in the end we realized we just needed to get over ourselves and get some light on them." The temperature that night was also a challenge, even for Wilson, a Norwegian native who originally moved to the UK to attend film school. "We also had to take care here that the lighting was romantic. We couldnt have it look freezing, which it was!"

Burnham Beeches, 540 acres of woodland west of London, was the site of a key forest sequence in which Tessa scales a tree and Adam follows her. Wilson says, "Shooting in a forest is always strange. Its a big green reflector with all of these crazy highlights, but its dark and hard to get a handle on. We split the scene over a few days and different locations, and we used three trees; a very large one, a small one and a fake tree. Technically it was quite interesting and rewarding, and the film handled the contrast so well."

"The whole experience on Now Is Good was a very pleasant one", says Wilson. "It was a well-planned shoot and there werent many surprises. Whenever Dakota was on set, it was a good day, and Dakota was on set virtually every day. My only warning is to bring a box of tissues with you to the movie... you will need them." 

==Reception==
Now Is Good received mixed reviews from critics when it was released in September 2012. The Guardian awarded it two out of five stars and said that it "never really rises above being a collection of clichés, despite some decent performances".     Empire Magazine awarded it three out of five stars and called it an "uneven but ultimately effective weepie with terrific turns from Considine and Williams, who outshine the younger cast.".   The Birmingham Mail said "If youre a fan of Nicholas Sparks films – or fancy a good old weep in the cinema – then you could do worse than see this movie.".    Total Film awarded it three out of five stars and said "Youll be in bits, but your critical faculties might weep too.".    Norwich Evening News said "The film avoids all the sentimentality and gives you moments that seem uncomfortably realistic but it doesnt lose sight of the fact, while that it is a weepie, it is still an entertainment."   

Review aggregation website Rotten Tomatoes gave the film a score of 58% based on reviews from 26 critics.  

==References==
 

==External links==
*  
*  
*   at Box Office Mojo

 
 
 
 
 
 
 
 
 
 