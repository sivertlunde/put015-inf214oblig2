The Witch Who Came from the Sea
 
 
{{Infobox film
| name           = The Witch Who Came from the Sea
| director       = Matt Cimber
| image	=	The Witch Who Came From the Sea FilmPoster.jpeg
| producer       = Jefferson Richard Robert Thom
| starring       =  Millie Perkins Lonny Chapman Vanessa Brown Peggy Feury Rick Jason George Buck Flower Roberta Collins
| music          = Herschel Burke Gilbert
| sound mix      = Mono
| cinematography = Dean Cundey
| aspect ratio   = 2.35 : 1
| distributor    = Cinefear/Moonstone Entertainment
| distributor = Cinema Epoch (DVD Reissue)
| released       = February 1976
| runtime        = 88 minutes (UK uncut version) 83 minutes
| country        = United States
| language       = English
}}

The Witch Who Came From the Sea is a 1976 American horror film directed by Matt Cimber and shot by cinematographer Dean Cundey. The films tagline was "Molly really knows how to cut men down to size!"

==Plot==
The film concerns a dysfunctional and disturbed woman called Molly (played by  Millie Perkins) who, after suffering repeated sexual abuse as a child at the hands of her seafaring father, embarks on a spree of gruesome sexual encounters with men who she meets during her job as a waitress in a seaside bar.

==Cast==
*Millie Perkins - Molly
*Lonny Chapman - Long John
*Vanessa Brown - Cathy
*Peggy Feury - Doris
*Jean Pierre Camps - Tadd
*Mark Livingston - Tripoli
*Rick Jason - Billy Batt
*Stafford Morgan - McPeak
*Richard Kennedy - Detective Beardsley
*George Buck Flower - Detective Stone
*Roberta Collins - Clarissa
*Stan Ross - Jack Dracula
*John F. Goff - Mollys Father

==Censorship== BBFC for certification and declared them prosecutable for obscenity. This list of "video nasties" included The Witch Who Came From the Sea, but it was in the sub-group of 33 titles that were unsuccessfully prosecuted and was soon dropped from the DPP list. In the United Kingdom, the film was eventually released completely uncut in 2006 with a complete running time of 87m 43 secs. 

==Reception==
 
The Witch Who Came From the Sea has been recommended by film critic Mark Kermode as one of the best video nasties of the era.  One critic viewed the film as not being a horror film but actually representing a scathing indictment of child sexual abuse as well as a study of a troubled womans descent into madness; "a study of a woman whose sanity teeters on the edge".  Another completely dismissed the film as representing nothing more than "an absurd story with no redeeming qualities. Highly recommended for lovers of bad cinema". 

The 2004 DVD release of the film (whose 16:9 transfer was overseen and approved by Dean Cundey ) sparked renewed interest,  with one reviewer remarking that The Witch Who Came From the Sea is "an unsung psychological gem" among 1970s exploitation films. 

==See also==
*Cult film
*Video nasty
*Rape and revenge films

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 