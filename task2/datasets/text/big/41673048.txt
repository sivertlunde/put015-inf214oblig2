Finding Fela
{{Infobox film
| name           = Finding Fela
| image          = Finding Fela poster.jpg
| alt            = 
| caption        = Film poster
| director       = Alex Gibney
| producer       = Alex Gibney Jack Gulick
| writer         = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| studio         = Jigsaw Productions
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Finding Fela is a 2014 documentary film directed by Alex Gibney, portraying and analysing the life of pioneering musician Fela Kuti.  The film had its world premiere at the 2014 Sundance Film Festival on January 17, 2014.   

==Synopsis==
A look at the life and music of Nigerian singer Fela Kuti.

==Reception==
Finding Fela received mixed reviews from critics. Geoffrey Berkshire of Variety (magazine)|Variety, said in his review that "This watchable but rather rote chronicle fails to find a compelling perspective on Kutis significant life and legacy."  David Rooney of The Hollywood Reporter praised the film by saying that "An absorbing if not quite definitive patchwork of the life and legacy of an unorthodox artist."  

==See also==
*List of films at the 2014 Sundance Film Festival

==References==
 

==External links==
* 
*  
*  
 
 
 
 
 
 

 

 