Gadibidi Aliya
{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = Gadibidi Aliya
| image          = 
| image_size     = 
| caption        =
| director       = Om Sai Prakash
| producer       = K. Raghava Rao
| writer         = 
| screenplay     = 
| narrator       =  Mohini    Jayamala Koti
| lyrics         = R. N. Jayagopal
| cinematography = Janilal
| editing        = K. Narasaiah
| distributor    = 
| released       = 1995
| runtime        = 149 minutes
| country        = India
| language       = Kannada
| budget         = 
}}
 Kannada comedy Mohini in the lead roles whilst Jayamala and Srinath play other pivotal roles. 
 Koti for the lyrics of R. N. Jayagopal.  The film is a remake of Telugu film Allari Alludu (1993), starring Akkineni Nagarjuna, Nagma and Meena Durairaj|Meena.

==Cast==
* Shivrajkumar as Shivu and Raj
* Malashri  Mohini 
* Jayamala 
* Srinath 
* Advani Lakshmi Devi
* Tennis Krishna
* Doddanna
* Ramesh Bhat
* Bank Janardhan
* Sathyabhama

==Soundtrack==
{| class=wikitable sortable
|-
! # !! Title !! Singer(s) || Lyrics
|-
|  1 || "Jama Jama Jamaisi" || S. P. Balasubrahmanyam, K. S. Chithra || R. N. Jayagopal
|-
|  2 || "Ladyige Jentle Manu" || S. P. Balasubrahmanyam, K. S. Chithra || R. N. Jayagopal
|-
|  3 || "Havana Havana" || S. P. Balasubrahmanyam || R. N. Jayagopal
|-
|  4 || "Umma Beku Sai" || Rajesh Krishnan, Manjula Gururaj, Sangeetha Katti || R. N. Jayagopal
|-
|  5 || "Rama Rasave"  ||  S. P. Balasubrahmanyam, Manjula Gururaj || R. N. Jayagopal
|-
|  6 || "Kannadada Kuvara" || Rajkumar (actor)|Rajkumar, Rajesh Krishnan || R. N. Jayagopal
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 

 