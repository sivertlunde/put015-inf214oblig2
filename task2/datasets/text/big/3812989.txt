Tick Tock Tuckered
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
|cartoon_name=Tick Tock Tuckered
|series=Looney Tunes (Daffy Duck/Porky Pig)
|image=
|caption=
|director=Robert Clampett
|story_artist=Warren Foster
|animator=Tom McKimson	
|background_artist=
|layout_artist=
|voice_actor=Mel Blanc
|musician=Carl Stalling, Milt Franklyn
|producer=Leon Schlesinger
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|studio=Warner Bros. Cartoons
|release_date=April 8, 1944
|color_process=Technicolor
|runtime=
|movie_language=English
}}

Tick Tock Tuckered is a 1944  .

==Plot==
The plot of the two cartoons are very similar.

When Porky and   Aircraft Co. and sneaking in. When it came to clocking in, Daffy ends up turning the clock backwards two hours earlier and clocks in only for the alarm to go off. Their boss (a caricature of Clampetts immediate boss, production manager Ray Katz)  catches them and states that if they werent going to make it, he wouldve sent their work to them. The boss warns them that  . Then he orders them to get to work.

Later that night at 08:00, Porky Pig sets the alarm clock as Daffy complains about having to go to bed early. Porky reminds Daffy that if they are late again, theyll get canned. Porky climbs into bed and they both fall asleep until a bunch of cats & dogs next door wake them up. Later that night, the moon comes out and its light wakes up Porky. One of Porkys attempts to close the blind ends up wrecking his bed. This also disturbs Daffy who ends up shooting the moon, which then falls as a result ("Unbelievable, isnt it?") As the night progresses, a thunderstorm occurs while Porky is sleeping in Daffys bed. Porky closes the window only for a leak in the roof to disturb him and Daffy. Daffy opens an umbrella in the house with Porky telling him that its bad luck. Daffy ignores Porkys statement until lightning destroys the umbrella. When Daffy quotes that he should try sleeping under Niagara Falls, a lot of water comes through the roof and down on them.

The next morning, Porky and Daffy are shown sleeping in the drawers when the alarm clock goes off at 06:00. They get themselves ready and drive off to work. When Porky and Daffy arrive at the Fly By Night Aircraft Co., they see a sign on the door that says "Closed Sunday." Porky states that they dont have to work today and they drive home. When they climb back into the drawers to sleep, the alarm clock goes off again at 08:15. It gets shot by Porky, falls over and dies.

==References==
 

==External links==
* 

 
 
 
 
 
 