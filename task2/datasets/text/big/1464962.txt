The Osterman Weekend (film)
{{Infobox film
| name           = The Osterman Weekend
| image          = The_Osterman_Weekend_movie.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Sam Peckinpah
| producer       = Peter S. Davis William N. Panzer
| screenplay     = Alan Sharp
| based on       = The Osterman Weekend by Robert Ludlum Ian Masters  (adaptation) 
| starring = {{Plainlist|
* Rutger Hauer
* John Hurt
* Craig T. Nelson
* Dennis Hopper
* Burt Lancaster
}}
| music          = Lalo Schifrin
| cinematography = John Coquillon
| editing        = Edward M. Abroms David Rawlins
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| gross          = $6,500,000
}}
 1983 suspense thriller film novel of the same name by Robert Ludlum. The film stars Rutger Hauer, John Hurt, Burt Lancaster, Dennis Hopper, Meg Foster and Craig T. Nelson. It was Peckinpahs final film before his death in 1984.

==Plot==
The film begins with CIA director Maxwell Danforth (Burt Lancaster|Lancaster) watching a filmed recording of agent Laurence Fassett (John Hurt|Hurt) and his wife making love. When Fassett goes into the bathroom to take a shower, two assassins enter the bedroom and kill his wife (Merete Van Kamp) by holding her down and injecting poison into her nostril with a syringe. It is hinted that the woman was an innocent bystander sacrificed by Danforth as collateral damage. Fassett, unaware of his employers involvement, goes almost insane with grief and rage and begins to hunt down the assassins, eventually uncovering a Soviet spy network known as Omega.

Fassett is called into the directors office and says that he wants to turn some of Omegas agents to the side of the West, and he has the perfect opportunity in John Tanner ( ), a plastic surgeon, and stock trader Joseph Cardone (Chris Sarandon|Sarandon). Tanner agrees, but only on the condition that someone from the CIA appear as a guest on his show. Danforth agrees to this condition.

Tanner has a troubled marriage with his wife Ali (Meg Foster|Foster) that is not helped when Tanner asks her to avoid the upcoming "Osterman weekend" and to take their son. He does not want to involve them in the events but cannot tell her why he wants her to stay away. While Tanner is driving his wife and son to the airport, their car is ambushed, and Ali and the child are kidnapped.  With Fassetts intervention they are rescued unhurt and the kidnapper is shot dead.  In the meantime, Tanners home has been wired with closed circuit video so Fassett can gather more evidence.

The three arrive for the weekend, each having recently encountered difficulties engineered by the CIA in order to unsettle them and make them receptive to turning. Tensions flare up and on the second night Fassett puts a video-feed on Tanners living room television, showing video evidence of the spy ring to the traitors. Having reached breaking point, Ali and Virginia, Tremaynes wife (Helen Shaver|Shaver), end up in a brief scuffle and everyone goes to their rooms. Tanners son discovers the severed head of the family dog in the refrigerator, but this turns out to be a fake head. Tanner has had enough and orders everyone out of his house and then confronts Fassett and presents an ultimatum: arrest the suspects. Fassett orders a guard to kill the broadcaster.

Meanwhile, Cardone and Tremayne have taken their wives and escaped in the Tanners RV. Tanner confronts Osterman and tries to attack him but Osterman easily overpowers the television reporter and demands to know what is going on. Tanner tells him that he knows that Osterman and his friends are Soviet agents, which Osterman dismisses as being ridiculous. He states that they have been illegally stashing money away in Swiss bank accounts to avoid taxation, saying that "It aint legal, but it sure as hell isnt traitorous."

Then Fassett reappears on the television by closed circuit, admitting the truth: his friends are nothing more than tax evaders. Nevertheless, Fassett kills the Tremaynes and Cardones by detonating an explosive device on the RV by remote control and then orders his soldiers into the house to kill Osterman and Tanner. Using the televisions in the house and the video cameras, Fassett taunts Tanner during the attack on the house, having learned that Danforth authorized his wifes murder. Fassett tells Tanner he will give his family back to him if he will expose Danforth on air.

After an interval, we see Danforth preparing for his remote interview with Tanner. Danforth is at his office and will speak into a camera and microphone crewed by the TV station. Tanner introduces Fassett on the air and Danforth explodes into a rage when he discovers he has been tricked. Fassett, who is also being filmed remotely, exposes Danforth as a murderer. Fassetts remote location is a secret, but it is clear someone is coming for him. It is revealed that Tanner himself has pre-recorded his questions for both men and has used the video feed to locate Fassett, whom he shoots and kills. He then rescues his wife, son, and his dog.

==Cast==
{{columns-list|2|
*Rutger Hauer as John Tanner
*John Hurt as Lawrence Fassett
*Dennis Hopper as Richard Tremayne
*Craig T. Nelson as Bernie Osterman
*Meg Foster as Ali Tanner
*Chris Sarandon as Joseph Cardone
*Helen Shaver as Virginia Tremayne
*Cassie Yates as Betty Cardone
*Sandy McPeak as Walter Stennings
*Burt Lancaster as Maxwell Danforth
}}

==Production==

William Castle initially purchased the film rights and asked author Ludlum to write the script. Ludlum was reluctant. Despite his extensive film and theatre experience, he said "I didnt leave that crowd of ocelots to go back into it."  
 Robert Ludlums 1972 novel The Osterman Weekend, but was giving up on turning it into a feature film since he had not been able to develop a satisfactory screenplay. Davis and Panzer immediately offered to purchase the rights, as they felt this could be the project that elevated them out of the B-movie features that they had been financing up to that point. Jones and a partner agreed, and Davis and Panzer began pre-production.
 Ian Masters. Davis claims that Masters heavily followed conspiracy theories and closely paid attention to the CIA|CIAs activities throughout the world. After Masters developed the scripts groundwork, Alan Sharp was hired to work on characters and dialogue.
 Straw Dogs Don Siegels Jinxed! in 1981. The competence and professionalism he displayed made it at least possible for him to be considered as director of The Osterman Weekend. 

Peckinpahs reputation was such that many studios did not want to work with him because of his antagonistic relationship with producers. Additionally, the directors health was in poor shape. Davis and Panzer were undaunted, as they felt that having Peckinpahs name attached to their film would lend it an air of respectability. However, due to the directors damaged reputation, the producers were forced to seek financing from independent sources.

According to the commentators on the films special edition DVD, Peckinpah hated Ludlums novel and he did not like the screenplay either.  Peckinpah requested and was given permission to work on the script himself, but after submitting his first few pages the producers forbade him from any more rewrites.

In Marshall Fines book Bloody Sam, screenwriter Sharp said that he himself did not like the screenplay he had written, and that he found it incredible that Davis and Panzer used his draft as the shooting script.  Fine also wrote that Ludlum had stated to his friend Jason Robards that he would provide a free rewrite; if this is true the producers never accepted his offer.  But in spite of his distaste for the project, Peckinpah immediately accepted the job as he was desperate to re-establish himself within the film community.
 Robert Taylors former residence in the Mandeville Canyon section of Los Angeles, the "Robert Taylor Ranch."

Peckinpah managed to keep up with the 54-day shooting schedule and within a budget of just under $7 million, but his relationship with the producers soon soured and became combative.  On the other hand, the cast greatly respected him and stated that Peckinpah put everything he could into directing the picture in spite of his physical exhaustion and health problems.

By the time shooting wrapped in January 1983, Peckinpah and the producers were hardly speaking. Peckinpah delivered the film on time and on budget, submitting his directors cut to the producers. 

This version was screened once on May 25, 1983.  Test audiences reacted unfavorably and many walked out of the theater during the first few minutes. Peckinpah opened with a distorted image of Fassett and his wife making love, and the way he had edited the scene made it difficult for the audience to discern what was going on.

Panzer and Davis were hoping that Peckinpah would go back and re-edit the film himself, as they did not desire to antagonize him any further, but the director refused to make changes. Peckinpah had also filmed several satirical scenes, subtly ridiculing the product.  As a result, the producers felt they had no choice and effectively fired Peckinpah and re-edited the film themselves. 

The producers changed the opening sequence and deleted other scenes they deemed unnecessary. Peckinpah proclaimed that producers had once again sabotaged his film, a complaint he made after filming Major Dundee (1965) and Pat Garrett & Billy the Kid (1973). He was less vocal this time, mindful that studios and producers were keeping an eye on his behavior.

Anchor Bay Entertainment has included the directors cut of the film on its DVD release, but it is sourced from the only known copy in existence: a low-quality, full-screen videotape.

The film was not a blockbuster (entertainment)|blockbuster, though it grossed $6 million domestically and did extremely well in Europe and on the new home-video market.   Theatrical distribution was handled by 20th Century Fox, and Thorn EMI picked up video rights; a laserdisc edition was published by Image Entertainment.  It is currently available on DVD from Anchor Bay Entertainment.

==Reception==
Critics reacted unfavorably towards the film, with one of the common complaints being that the story was full of plot holes.  Roger Ebert wrote, "I sometimes enjoy movies that make no sense whatsoever, if thats their intention.  But a thriller is supposed to hold together in some sort of logical way, isnt it?"  The Chicago Readers Dave Kehr has stated, "The structure is a mess...which ultimately makes it too difficult to tell whether its oddly compelling qualities are the result of a coherent artistic strategy or the cynical carelessness of a director sidelined." Vincent Canby of the New York Times wrote that it was "incomprehensible" and "full of gratuitous sex and violence", but "has a kind of hallucinatory craziness to it".  It currently holds a 43% approval rating on Rotten Tomatoes. 

==Alpha to Omega: Exposing The Osterman Weekend==
Alpha to Omega: Exposing The Osterman Weekend is a 2004 documentary about the making of The Osterman Weekend. It was included as a special feature on Anchor Bay Entertainments 2004 DVD release of the film. Featuring interviews with many members of the cast and crew, it examines not only the process of bringing Ludlums novel to the screen, but also provides a portrait of Peckinpahs approach to the filmmaking process, and of his frame of mind and physical health following years of substance abuse.  It was directed by Jonathan Gaines, who co-wrote with Michael Thau; Thau also served as the editor.

===Interviewees===
{{columns-list|2|
*William N. Panzer
*Peter S. Davis
*Rutger Hauer
*John Hurt
*Chris Sarandon
*Craig T. Nelson
*Cassie Yates
*Nick Redman
*Meg Foster
*Martin Baum
*Helen Shaver
*Edward Abroms
*Lalo Schifrin
}}

==Remake==

In February 2012, it was reported that talks were under way to film a new adaptation of Ludlums book. 

==See also==

* List of films featuring surveillance

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 