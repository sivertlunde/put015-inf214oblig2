Buffalo Soldiers (2001 film)
{{Infobox film
| name           = Buffalo Soldiers
| image          = Buffalo Soldiers film poster.jpg
| caption        = Buffalo Soldiers film poster
| director       = Gregor Jordan
| producer       = Rainer Grupe Ariane Moody Robert OConnor Eric Weiss Nora Maccoby Gregor Jordan
| starring       = Joaquin Phoenix Ed Harris Anna Paquin Scott Glenn Haluk Bilginer
| cinematography = Oliver Stapleton Lee Smith FilmFour Good Machine Grosvenor Park Productions
| distributor    = Miramax Films
| released       = July 25, 2003
| runtime        = 98 minutes 
| language       = English
| budget         = $15 million 
| gross          = $2,300,684 
}}
 Robert OConnor,  which follows the rogue activities of a group of US soldiers based in West Germany during 1989 when the fall of the Berlin Wall is imminent. It stars Joaquin Phoenix, Ed Harris, Anna Paquin, Haluk Bilginer, Scott Glenn, and Elizabeth McGovern and is directed by Gregor Jordan. 
 US military, the films wider theatrical run was delayed by approximately two years because of the September 11 attacks until it was released on July 25, 2003.   

==Plot== Specialist Ray Elwood (Joaquin Phoenix) is a U.S. Army soldier stationed in Stuttgart, Germany, in 1989. Bored with the lack of a war, he instead devotes his attention to black market deals, as well as cooking heroin for a gang of drug-dealing Military Police led by the psychotic Sergeant Saad. As a supply specialist, Elwood poses as a model soldier and friendly confidant to his incompetent commanding officer, Colonel Berman (Ed Harris), who gives Elwood the opportunity to turn Army protocol to his advantage. Berman has no idea what supplies his subordinate is requisitioning or that Elwood is sleeping with his wife (Elizabeth McGovern).
 First Sergeant, Robert E. Lee (Scott Glenn). This new Top is both menacing and savvy, quickly assessing that soldiers like Elwood are engaged in Graft (politics)|graft. Meanwhile a tank crew, under the influence of Elwoods heroin, unintentionally kill two soldiers in charge of a weapons convoy by crashing through a gas station. Elwood happens upon the cache and steals it, hiding the trucks in a missile base.

Lee confronts Elwood about his abuses, and when Elwood tries to bribe him, Lee revokes all of Elwoods privileges, destroys much of his property, and makes him share his room with a new, naive and deeply honest soldier, Pvt. Knoll (Gabriel Mann), who doesnt fit well with Elwoods lifestyle.

To get back at Lee, Elwood pursues the first sergeants daughter, Robyn (Anna Paquin), taking her out for the night and then deliberately having sex with her in his car outside her house, which Lee sees from his window. Though thinking he has won, the next morning Elwood finds his squad are doing weapons training, with Elwoods car used as target practice. Lee then leaves a hand grenade trap in a locker some soldiers have been using to hide heroin. The grenade explodes and kills Stoney, one of Elwoods friends.

Elwood sells his hidden cache of weapons to a Turkish dealer nicknamed "The Turk" (Haluk Bilginer) who will only exchange them for a massive amount of opium. Elwood reluctantly accepts and plans to cook it into heroin. However, a fight between Saad and Knoll occurs, and Elwood is forced to save Knoll and recruit the pair as assistants into his plan to cook the opium. In order to have time to trade the weapons from the missile base and collect the drugs, Elwood sells out Colonel Berman, who is on an army exercise against another regiment. His units failure as part of the test ultimately leads to the colonels dismissal. Berman, unaware that he has been betrayed, reflects ironically on this turn of events with Elwood.
 2nd Lieutenant Inspector Generals Office disguised as a junior soldier in order to investigate Elwood. Knoll puts Robyn in a car while Lee beats up Elwood. However, Robyn tells Knoll that her dad is going to kill Elwood, something Knoll  as an honest officer   cannot allow. Meanwhile commandos reporting to Knoll attempt to arrest Elwoods partners, but Saad, intoxicated by opium fumes, provokes a bloody gunfight that kills many of the fighters. Lee attempts to kill Elwood by throwing him out of a window but Knoll holds him at gunpoint. The building explodes from a gas explosion in the basement, caused by burning morphine and the firefight. Elwood wraps his handcuffs around Lees neck and draws him out the window as Knoll is engulfed in flames. Elwood survives by landing on Lee.

The film concludes with heavy irony. The US Army decorates Elwood with medals (Lee receives a posthumous Silver Star) and transfers him to Hawaii, where his new CO is just as naive as Col. Berman. Elwood states that Robyn remains his sweetheart and that she will be visiting soon. The film ends with Elwood requesting excessive supplies, implying that he plans to sell them on the black market again.

==Cast==
* Joaquin Phoenix as SPC Ray Elwood
* Ed Harris as COL Berman
* Scott Glenn as 1SG Robert E. Lee
* Anna Paquin as Robyn Lee
* Haluk Bilginer as The Turk
* Elizabeth McGovern as Mrs. Berman
* Michael Peña as Garcia
* Leon Robinson as Stoney
* Gabriel Mann as Pfc./2nd Lt. Brian Knoll
* Dean Stockwell as GEN Lancaster
* Brian Delate as COL Marshall
* Glenn Fitzgerald as Hicks
* Idris Elba as Kimborough

==Critical reception==
Buffalo Soldiers has a rating of 73% on Rotten tomatoes based on 114 reviews with an average score of 6.5 out of 10. The consensus states "Overall, this caustic comedy hits more of its targets than it misses."  The film also has a score of 56 out of 100 on Metacritic based on 35 reviews. 

==See also==
* Sgt. Bilko (film)|Sgt. Bilko

==References==
 

==External links==
*  
* Gregor Jordan interview http://www.moviecitynews.com/Interviews/jordan.html
* Eric Axel Weiss interview http://www.moviemaker.com/screenwriting/article/soldiering_on_2696/

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 