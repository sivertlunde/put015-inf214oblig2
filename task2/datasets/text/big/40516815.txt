Bare Knees
{{infobox film
| title          = Bare Knees
| image          =
| imagesize      =
| captive        =
| director       = Erle C. Kenton
| producer       = Sam Sax Harold Shumate
| writer         = Adele Buffington (story) Harold Shumate (adaptation) Casey Robinson (intertitles)
| starring       = Virginia Lee Corbin
| music          =
| cinematography = James Diamond
| editing        =
| distributor    = Gotham Pictures / Lumas Film Corporation
| released       = February 1, 1928
| runtime        = 61 minutes
| country        = United States
| language       = Silent (English intertitles)

}}
Bare Knees is a 1928 American silent film comedy-drama directed by Erle C. Kenton and starring Virginia Lee Corbin.  

An extant film, it is available on DVD from the Amazon and Grapevine companies.  

==Cast==
*Virginia Lee Corbin - Billie Durey Donald Keith - Larry Cook
*Jane Winton - Jane Longworth
*Johnnie Walker - Paul Gladden
*Forrest Stanley - John Longworth
*Maude Fulton - Bessie

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 


 