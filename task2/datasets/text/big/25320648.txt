Sex Kittens Go to College
{{Infobox Film
| name           = Sex Kittens Go to College
| image          = Sexkittenscollege.jpg
| image_size     = 
| caption        = 
| director       = Albert Zugsmith
| producer       = Albert Zugsmith
| writer         = Robert Hill Albert Zugsmith (Story)
| narrator       = 
| starring       = Mamie Van Doren Tuesday Weld Mijanou Bardot Mickey Shaughnessy Louis Nye
| music          = Dean Elliott
| cinematography = Ellis W. Carter
| editing        = William Austin Allied Artists Pictures
| released       = August 24, 1960
| runtime        = 94 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Sex Kittens Go to College (also known as Beauty and the Robot) is a 1960 American comedy film starring Mamie Van Doren, Tuesday Weld and Mijanou Bardot. It was also released in adult movie houses including a dream scene of the robot, Sam Thinko, and some striptease dancers.

==Plot==
Collins Colleges administrators are expecting new professor Dr. Mathilda West, who holds 13 degrees and speaks 18 languages. What they arent expecting is the buxom blonde beauty who gets off the train.

Dr. West has an effect on everybody, from public relations director George Barton, who is the boyfriend of jealous dean Myrtle Carter, to football star Woo Woo Grabowski, who gets very nervous around beautiful women, including student Jody, who loves him.

A campus computer, affectionately known as Thinko, has a knack for knowing the future, including winning lottery numbers and race horses. A hoodlum, Legs Rafertino, comes looking for Thinko, thinking hes a bookie, while foreign exchange student Suzanne tries to interview Legs for her thesis.

Barton exposes the fact that Dr. West was once the Tallahassee Tassle Tosser, a stripper. The schools primary benefactor, Admiral Wildcat MacPherson, is concerned. Dr. West defends her former occupation and even gives a tassle demonstration that hypnotizes several of the men.

Woo Woo wins enough money on Thinkos gambling advice to marry Jody and buy a car. Myrtle dyes her hair blonde and woos Wildcat MacPherson. And not wanting to stay where shes not wanted, Dr. West prepares to leave town, only to have Barton steal a fire engine and race to catch up with her.

==Cast==
* Mamie Van Doren as Dr. Mathilda West
* Tuesday Weld as Jody
* Mijanou Bardot as Suzanne
* John Carradine as Professor Watts
* Jackie Coogan as Wildcat MacPherson
* Babe London as Miss Cadwallader
* Pamela Mason as Dr. Myrtle Carter
* Martin Milner as George Barton
* Louis Nye as Dr. Zorch
* Mickey Shaughnessy as Boomie
* Conway Twitty as himself Vampira as Etta Toodie
* Norman Grabowski as Woo Woo Grabowski
* Charles Chaplin, Jr. as Fire chief (Uncredited)
* Harold Lloyd Jr. as Policeman (Uncredited)
 Westinghouse in 1937. 

==References==
 

==External links==
*  
*  
*  at Trailers from Hell
 
 
 
 
 
 


 