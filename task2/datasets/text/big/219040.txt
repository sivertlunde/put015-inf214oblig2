Bedtime for Bonzo
{{Infobox film
| name           = Bedtime for Bonzo
| image          = Bedtime for Bonzo 1951.jpg
| image size     = 190px
| caption        = Original 1951 film poster
| director       = Frederick de Cordova
| producer       = Michael Kraike
| writer         = screenplay by Lou Breslow & Val Burton story by  Ted Berkman &  Raphael Blau Jesse White Brad Johnson  "Bonzo"   Lucille Barkley Frank Skinner
| cinematography = Carl E. Guthrie
| editing        = Ted Kent
| distributor    = Universal-International
| released       = April 5, 1951
| runtime        = 83 min
| country        = United States
| awards         =
| language       = English
| budget         =
| gross = $1,225,000 (US rentals) 
}}
Bedtime for Bonzo is a 1951 comedy film directed by Frederick de Cordova, starring future U.S. President Ronald Reagan and Diana Lynn.  It revolves around the attempts of the central character, Psychology Professor Peter Boyd (Ronald Reagan), to teach human morals to a chimpanzee, hoping to solve the "nature versus nurture" question. He hires Jane Linden, a woman (Diana Lynn) to pose as the chimps mother while he plays father to it, and uses 1950s-era child rearing techniques. 

This movie is one of the most remembered of Reagans acting career and renewed his popularity as a movie star for a while. Reagan, however, never even saw the film until 1984. 
 Bonzo Goes The Damneds 2000 AD, which featured President Ronald Reagan being kidnapped out of his own era and taken into the far flung future setting of the comic. Other notable references include the 1966 Stan Freberg comedy album Freberg Underground, and the 1986 video of the British band Genesis (band)|Genesiss song "Land Of Confusion". In the 1980s satirical British TV show Spitting Image, Reagan was shown as having appointed a dead taxidermied Bonzo as vice president.
A sequel was released entitled Bonzo Goes to College (1952), but featured neither lead performer from the original.

==In popular culture==
* The movie is referenced in the MMORPG video game DC Universe Online. Following the two player duo "Gorilla Grodds Lab" The Flash quips at Gorilla Grodd "Its bedtime for Bonzo". 2004 Presidential Elections had a line mention that Reagan "acted with a chimp when he was a movie star."

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 

 