Labyrinth of Lies
 
 
{{Infobox film
| name           = Labyrinth of Lies
| image          = 
| caption        = 
| director       = Giulio Ricciarelli
| producer       = 
| writer         = Giulio Ricciarelli Elisabeth Bartel
| starring       = Alexander Fehling
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Labyrinth of Lies ( ) is a 2014 German drama film directed by Giulio Ricciarelli. It was screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.   

==Cast==
* Alexander Fehling as Johann Radmann
* Johannes Krisch as Simon Kirsch
* Friederike Becht as Marlene
* Hansi Jochmann as Sekretärin
* Johann von Bülow as Otto Haller
* Gert Voss as Fritz Bauer
* Robert Hunger-Bühler as Walter Friedberg
* André Szymanski as Thomas Gnielka

==Plot==
Johann Radmann is a young and idealistic public prosecutor who takes an interest in the case of Charles Schulz, a former SS officer who is now teaching at a school in Berlin.  Radmann is determined to bring Schulz to justice, but he finds his efforts frustrated because many former Nazis serve in the government, and they look out for each other.

His boss the prosecutor-general Fritz Bauer puts him in charge of investigating former workers at the Auschwitz externmination camp. The American occupation forces give him access to their files and he discovers there were 8,000 of them. He goes after Josef Mengele, who is living in Argentina but flies back to Germany at will to visit his family. After officialdom block his attempt to issue an arrest warrant, his boss warns him off and tells him to stick to the small fry. The department then invites Mossad agents to visit, and shares its information with them. As a results, Adolf Eichmann is kidnapped and tried in Israel. Having pulled of this coup, Israel declines to pursue Mengele.

Meanwhile Radmann allows himself to be seduced by Marlene, a seamstress who, benefiting from his connections, is able to starts a business as a dress designer. He is brought to a  crisis when he discovers that his own father was in the Nazi party. After he tells her that her father too was in the Nazi party she breaks of with him, but by the end of the film there is a chance she will have him back. He resigns his official post and goes to work for an industrialist, but when he finds this means working with a colleague who had defended a former Nazi he was investigating, he walks out. After going to Auschwitz to say kaddish, the jewish mourning prayer, for a friends two daughters who were killed there, he goes back to work for the state prosecutor. The film ends with the opening of the trial of several hundred former Auschwitz workers.

==Accolades==
At 2014s Les Arcs European Cinema Festival, the film received a Special Mention from the Jury, and won the Prix du Public (audience award).    

==References==
 

==External links==
*  

 
 
 
 
 
 