Midsummer Madness (film)
{{Infobox film
| name           = Midsummer Madness
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        =  Alexander Hahn
| producer       = 
| writer         = Joe W. Wrist
| screenplay     = 
| story          = Alexander Hahn
| based on       = 
| narrator       = 
| starring       = Orlando Wells Gundars Āboliņš Maria de Medeiros Dominique Pinon Chulpan Khamatova
| music          = Klaus Hundsbichler
| cinematography = Jerzy Palacz
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 mins.
| country        = Latvia Lithuania Austria Latvian English English Russian Russian French French
| budget         = $3,500,000 (estimated)
| gross          =
}}

Midsummer Madness ( ) is a 2007 film telling 6 different stories all taking place in Latvia during the Latvian national festivities of Jāņi. One of the stories shows a French woman (played by Maria de Medeiros) who brings the ashes of her late husband to Latvia, believing it to be the same country as Lithuania as she wants to spread her husbands ashes near the Hill of Crosses, which is in Lithuania. She gets to the hill of crosses and this shows the only part of the film outside of Latvia.

==Cast==
* Orlando Wells – Curt (later Kurts)
* Gundars Āboliņš – Oskars
* Maria de Medeiros – Livia
* Dominique Pinon - Toni 
* Chulpan Khamatova – Aida
* Tobias Moretti – Peteris
* Victor McGuire – Mike
* Detlev Buck – Axel
* Roland Düringer – Karl
* Birgit Minichmayr – Maja
* Daniil Spivakovskiy – Foma
* Aurelija Anuzhite – Natasha
* Dainis Porgants – Purvinsh
* Yevgeni Sitokhin – Leonid
* Imbi Strenga – Marite
* James-William Watts – Lewis
* Benito Sambo – Yuki

==External links==
*  

 
 

 