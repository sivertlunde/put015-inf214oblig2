Sky Fighters
 
{{Infobox film
| name = Les Chevaliers du ciel
| image = Les Chevaliers du ciel poster.jpg
| caption = French theatrical poster
| director = Gérard Pirès
| writer = Gilles Malençon
| based on =  
| starring = Benoît Magimel Clovis Cornillac Géraldine Pailhas Alice Taglioni
| producer = Eric Altmeyer Nicolas Altmeyer Christopher Granier-Deferre |
| distributor = Pathé
| released =  
| runtime = 102 minutes
| country = France
| language = French
| budget = €19,610,000
}} comics series TV series from 1967 to 1969, making the characters of Tanguy and Laverdure a part of popular Francophone culture.

== Plot == Mirage 2000-10 is stolen. French Air Force pilots Captain Antoine "Walkn" Marchelli (Benoît Magimel) and Captain Sébastien "Fahrenheit" Vallois (Clovis Cornillac) are instructed to escort it back. When the rogue pilot attempts to shoot at Vallois, Marchelli is forced to destroy the stolen Mirage.

The French "Special Missions" department, seeking to blackmail Marchelli into working for them, tampers with the video from Marchellis gun camera. A subsequent court martial finds Marchelli guilty of destroying the stolen Mirage without provocation and discharges him from the air force, prompting Vallois to resign as well.
 Cannonball run over hostile territory to the Horn of Africa to help sell the Mirage to an Asian customer. Marchelli and Vallois agree but are forced to land in hostile territory before completing the flight. They are captured by terrorists intent on stealing their planes. Marchelli and Vallois manage to escape in one of the jets, while the terrorists dismantle and make off with another.

The terrorists plan use the stolen Mirage to shoot down a tanker aircraft over Paris during the Bastille Day celebrations. Marchelli and Vallois intercept the rogue Mirage and shoot it down over an unpopulated area.

== Filming ==

Les Chevaliers du Ciel was filmed in co-operation with the French Air Force.  Initially the standard safety rules applied, but eventually the minimum allowed altitude was reduced to 3 m (10 ft) and the minimum distance between aircraft was reduced to 1 m (3 ft). 
 HD camera was considered for this purpose, but it did not fit in the fuel pod. Tracking shots were done from a hired Lear Jet. 

Additionally, jet aircraft are not allowed to fly over Paris.  As a result of this, all the Paris filming had to be done on the actual Bastille Day (14 July) for which the filming crew got special permission. 

== Soundtrack Listing ==

#Chris Corner – "Attack 61" (02:51)
#Chris Corner – "We Rise" (03:10)
#Thirteen Senses – "Into The Fire" (03:36) Sue Denim – "Gonna Wanna" (02:52)
#Chris Corner feat. D. Manix – "Sugar Jukebox" (02:10)
#Chris Corner feat. Sue Denim – "Girl Talk" (02:08)
#Ghinzu – "Cockpit Inferno" (03:50)
#Chris Corner feat. Sue Denim – "Youre The Conversation (Im The Game)" (03:55) Placebo – "The Crawl" (02:58)
#Chris Corner – "The Clash" (03:47)
#Chris Corner – "14th Of July" (01:47)
#I Monster – "Heaven Is Inside You" (03:55)
#Chris Corner – "Youre The Conversation (Im The Game)" Original Version (03:02)

== See also ==
* Dassault Mirage 2000

== External links ==
*   (French)
*   (English)
*  
*  

 
 
 
 
 
 
 