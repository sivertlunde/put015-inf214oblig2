The River Woman
{{infobox film
| name           = The River Woman
| image          =
| imagesize      =
| caption        =
| director       = Joseph Henabery
| producer       = Gotham Productions
| writer         = Harold Shumate(story,dialogue) Adele Buffington(adaptation,scenario)
| starring       = Lionel Barrymore
| music          =
| cinematography = Ray June
| editing        = Donn Hayes
| distributor    = Lumas Film; States Rights
| released       = December 1, 1928
| runtime        = 7 reels; 6,565 feet
| country        = United States
| language       = Silent film(English intertitles)
}}
The River Woman is a 1928 drama film directed by Joseph Henabery that is part-silent/part-sound. Made by an independent company, Gotham, the film starred Lionel Barrymore and Jacqueline Logan. It is preserved at the Library of Congress.   

==Cast==
*Lionel Barrymore - Bill Lefty
*Jacqueline Logan - The Duchess
*Charles Delaney - Jim Henderson
*Sheldon Lewis - Mulatto Mike
*Harry Todd - The Scrub
*Mary Doran - Sally

==See also==
*Lionel Barrymore filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 