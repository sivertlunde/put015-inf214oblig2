Les Tortillards
{{Infobox film
| name           = Les Tortillards
| image          = 
| caption        = 
| director       = Jean Bastia
| producer       = Horizons Cinématographiques (France)
| writer         = Jean Bastia Pascal Bastia
| starring       = Jean Richard Louis de Funès Louiguy
| cinematography = 
| editing        = 
| distributor    = 
| released       = 30 December 1960 (France)
| runtime        = 82 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1960, directed by Jean Bastia, written by Pierre Gaspard-Huit, starring Jean Richard and Louis de Funès. The film was known under the titles: "Io... mio figlio e la fidanzata" (Italy), "Der Umstandskrämer" (East Germany). 

== Cast ==
* Jean Richard : César Beauminet, manager of the theatrical troops
* Roger Pierre : Gérard Durand, the son
* Louis de Funès : Emile Durand, créateur de la bombe insecticide "Cicéron"
* Danièle Lebrun : Suzy Beauminet, daughter of César
* Madeleine Barbulée : Adélaïde Benoit, the aunt of Gérard
* Jeanne Helly : Marguerite Durand, wife of Emile
* Annick Tanguy : Fanny Raymond, the companion of César
* Robert Rollis : Ernest, a man of the troupe
* Billy Bourbon : Albert Albert, the acrobat of the troops
* Max Desrau : "Pépé", the painter of the troops
* Christian Marin : Léon, Emiles colleague
* Nono Zammit : Paulo, the props man

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 