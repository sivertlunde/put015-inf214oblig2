Chinna Thayee
{{Infobox film
| name           = Chinna Thayee
| image          = 
| image_size     =
| caption        = 
| director       = S. Ganesaraj
| producer       = M. Vedha
| writer         = S. Ganesaraj
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Viswam Nataraj
| editing        = Srinivas Krishna
| distributor    =
| studio         = M.S.R Films
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1992 Tamil Tamil drama Vignesh and Padmashri in lead roles. The film, produced by M. Vedha, had musical score by Ilaiyaraaja and was released on 1 February 1992. 

==Plot==

Ponrasu (Vignesh (actor)|Vignesh) returns to his village after studying in the city. Ponrasus father Veeramuthu Naicker (Vinu Chakravarthy) is the religious figure of the village called "Samiyaadi" (disguised as the male deity Sudalai Madan) who delivers divine judgement and kills those who faced him during the ceremony. Ponrasu loves the village belle Chinna Thayee (Padmashri) since his childhood and Chinna Thayee develops a soft corner for Ponrasu.

Chinna Thayees mother Raasamma (Sabitha Anand) is against their love, because in the past Raasamma was in love with a singer and he dumped her after consummating their relationship, she is afraid that the same thing happens to her daughter. Raasamma is the mistress of Samundi (Napoleon (actor)|Napoleon), a brute, who is married to another person and has a child.

Raasamma brainwashed Chinna Thayee and convinced her to forget him. Chinna Thayee first avoids Ponrasu, but then they get closer and Chinna Thayee gets pregnant. In a routine ceremony, Veeramuthu Naicker as "Samiyaadi" faces Raasamma who reveals everything on their childrens relationship and she implores him to kill her. Shocked, Veeramuthu Naicker ignores her. This incident is considered a bad omen by the villagers. Then, Ponrasus parents brainwash him and the heartbroken Ponrasu returns to the city.

One day, Samundi tries to rape Chinna Thayee and during the confrontation he kills his mistress Raasamma. At the village court (Gram panchayat), Veeramuthu Naicker solely orders Samundi to make offerings at the temple and he succeed to cover up his sons love affair. Chinna Thayee complains it to the police and the police inspector Sankarapandian (Radha Ravi) attempts to arrest Samundi. Sankarapandian faces the villages opposition, so he arrests Veeramuthu Naicker and he releases him later. Ponrasu returns to his village and he cannot forget Chinna Thayee whereas his father forces him to marry with another girl. The previous ceremonys incident overwhelmed the entire village, so Ponrasu becomes the new "Samiyaadi". During the ceremony, Ponrasu faces Chinna Thayee with his newborn baby. What transpires later forms the crux of the story.

==Cast==
 Vignesh as Ponrasu
*Padmashri as Chinna Thayee
*Radha Ravi as Inspector Sankarapandian
*Vinu Chakravarthy as Veeramuthu Naicker Napoleon as Samundi
*Goundamani Senthil
*Sabitha Anand as Raasamma
*Vaani
*Surya
*Vasuki
*G. Jayanthi
*Vichithra as Ponnamma
*Junior Balaiah as Vathiyar
*A. K. Veerasamy
*Krishnamoorthy

==Soundtrack==

{{Infobox album |  
| Name        = Chinna Thayee
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1992
| Recorded    = 1992 Feature film soundtrack
| Length      = 31:39
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1992, features 8 tracks with lyrics written by Vaali (poet)|Vaali.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Arumbarumba Saram|| P. Susheela || 05:17
|- 2 || Aarumuga Mangalathil || Chorus || 1:15
|- 3 || Kottaiya Vittu || S. P. Balasubrahmanyam || 1:17
|- 4 || Kottaiya Vittu || S. Janaki || 5:02
|- 5 || Kottaiya Vittu || S. P. Balasubrahmanyam || 5:02
|- 6 || Naan Erikarai || K. J. Yesudas, Swarnalatha || 5:08
|- 7 || Naan Erikarai || Ilaiyaraaja || 4:58
|- 8 || Naan Ipothum || S. P. Balasubrahmanyam, S. Janaki || 4:55
|}

==Reception==
N. Krishnaswamy of The New Indian Express gave the film a positive review citing "director Ganesharaj comes up with winning scenes that have elemental power, making his artistes with one another, and the audience well". He also praised the cinematographer Viswam Nataraj and the music director Ilaiyaraaja for their work. The film completed 100 day-run at the box-office.  

==References==
 

==External links==
*  

 
 
 
 
 
 