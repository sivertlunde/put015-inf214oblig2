Abbott and Costello Meet Dr. Jekyll and Mr. Hyde
 
{{Infobox film
| name           = Abbott and Costello Meet Dr. Jekyll and Mr. Hyde
| image          = a&cjekyll.jpg
| caption        = Theatrical release poster
| director       = Charles Lamont
| producer       = Howard Christie John Grant Lee Loeb
| starring       = Bud Abbott Lou Costello Boris Karloff
| music          = Joseph Gershenson George Robinson
| based on       =  
| editing        = Russell Schoengarth
| distributor    = Universal-International
| released       = August 10, 1953
| runtime        = 76 min.
| language       = English
| budget         = $724,805 
| country        = United States
| gross = $1.2 million (US & Canada rentals) 
}}
 directed by Charles Lamont and stars the comedy team of Abbott and Costello, and co-stars Boris Karloff. {{Citation
 | title = Horror Spoofs of Abbott and Costello: A Critical Assessment of the Comedy Teams Monster Films
 | year = 2004
 | author = Miller, Jeffrey S.
 | isbn = 978-0-7864-1922-7
}} 
 American detectives visiting Edwardian London who become involved with the hunt for Dr. Jekyll, who is responsible for a series of murders. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 365 

== Plot == Craig Stevens), finds one of the murder victims while coming home from a bar at night and calls the police. The next day, two American policemen, Slim (Bud Abbott) and Tubby (Lou Costello), who are working for the London Police Force, respond to a mob fight at a Womens Suffrage Rally in Hyde Park. Reporter Adams, young suffragette Vicky Edwards (Helen Westcott), Slim, and Tubby, all get caught up in the fray and wind up in jail. Later, Vickys guardian, Dr. Henry Jekyll (Boris Karloff), bails Vicky and Adams out. Tubby and Slim are thereafter kicked off the police force. Unknown to anyone, however, Dr. Jekyll has developed an injectable serum which transforms him into Mr. Hyde (the "monster" responsible for the recent murders). When Jekyll notices Vickys and Bruces mutual attraction, he has more thoughts of murder, injects himself, and transforms once again into Hyde (with the intent of murdering Adams).
 Reginald Denny), Adams, and Slim to the scene, the monster has already reverted to Dr. Jekyll and Tubby is once again scolded by the Police Inspector. The "good" doctor, however, asks Slim and Tubby to escort him to his home. Once at Jekylls home, Tubby goes off exploring and winds up drinking a potion which transforms him into a large mouse. Afterward, Slim and Tubby try to bring news of Jekylls activities to the Inspector, but the Inspector refuses to believe them.

Later, when Vicky announces to Jekyll her intent to marry Adams, Jekyll (who is secretly in love with Vicky) does not share her enthusiasm and transforms into Hyde right in front of her. Bent, this time, on murdering Vicky, Hyde attempts to attack her. However in the nick of time, Bruce, Slim, and Tubby save her and Hyde escapes. During the struggle, though, Jekylls serum needle is dropped into a couch cushion, which Tubby accidentally falls onto, transforming him also into a Hyde-like monster. Another mad-cap chase ensues, this time with Bruce chasing Jekylls monster and Slim pursuing Tubbys monster (both believing they are after Jekyll). Bruces chase ends up back at Jekylls home, where Hyde falls from an upstairs window to his death, revealing to everyone his true identity when he reverts to normal form. Slim then brings Tubby (still in monster form) to the Inspector. Tubby then bites the Inspector (and four officers) and reverts to himself, much to the chagrin of Slim.  However, before Slim and Tubby can be once again derided by the Inspector, the Inspector and his men have each transformed into monsters themselves (probably from Tubbys bite) and chase Slim and Tubby out of the office.

== Cast ==
* Bud Abbott as Slim
* Lou Costello as Tubby
* Boris Karloff as Dr. Henry Jekyll/Mr. Hyde Craig Stevens as Bruce Adams
* Helen Westcott as Vicky Edwards Reginald Denny as Inspector
* John Dierkes as Batley

==Production==
Abbott and Costello Meet Dr. Jekyll and Mr. Hyde was filmed between January 26th and February 20th, 1953 and received an "X" rating in Britain because of the scenes with Mr. Hyde.  Furthermore, Boris Karloff (contrary to the credits) only actually played Dr. Jekyll and did not play Hyde. Once the transformation sequences were over, Hyde was, instead, played by stuntman Eddie Parker, who remained uncredited.

In the movie, Bud Abbotts character consistently pronounces Dr. Jekylls name as "JAKE ull." Most movies about the character pronounce the name as "JEEK ull," which is the British style and the way Stevenson pronounced it, or as "JECK ull," which is the standard American style (as in Jekyll Island). Abbotts pronunciation of the name appears to be unique.

==Reviews==
The film received a 6.4 film rating on IMDb based 2,343 user Reviews. Many reviewers complained that, in this version, there was no struggle in the transformation between Dr. Jekyll and Mr. Hyde, giving the impression that Dr. Jekyll himself was evil and enjoyed the acts of Mr. Hyde. Other reviews complained of the lack of a strong script; calling the production cheap. Rotten Tomatoes has given the film a 6/10 score and the movie stands at an overall rating of 84. It also received an audience rating of 3.4 out of 5 (based on 3,553 user ratings). One critic, Steve Crum of the Kansas City Kansan, gave the film 3/5 saying "Bud and Lou meet another monster for infrequent laughs."  

==DVD releases==
This film has been released twice on DVD, on The Best of Abbott and Costello Volume Four, on October 4, 2005, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.

==References==
 

===Bibliography===
* Wingrove, David. Science Fiction Film Source Book (Longman Group Limited, 1985)

==External links==
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 