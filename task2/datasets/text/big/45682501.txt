The Headmaster (film)
{{Infobox film
| name           = The Headmaster 
| image          =
| caption        =
| director       = Kenelm Foss
| producer       = H.W. Thompson
| writer         = Edward Knoblock  (play)   Wilfred Coleby (play)   Kenelm Foss 
| starring       = Cyril Maude   Margot Drake  Miles Malleson 
| cinematography = 
| editing        = 
| studio         = Astra Films 
| distributor    = Astra Films
| released       = January 1921
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent comedy The Headmaster by Edward Knoblock and Wilfred Coleby. A clergyman working as the headmaster of a school tries to persuade his daughter to marry the idiotic son of an influential figure in the hope of being promoted to bishop.

==Cast==
* Cyril Maude as Rev. Cuthbert Sanctuary  
* Margot Drake as Portia Sancturay  
* Miles Malleson as Palliser Grantley  
* Marie Illington as Cornelia Grantley  
* Lionelle Howard as Jack Strahan 
* Simeon Stuart as Dean of Carchester  
* Ann Trevor as Antigone Sanctuary  
* Louie Freear as Bella  
* Will Corrie as Sgt. Munton  
* Alan Selby as Richards   Gordon Craig as Stuart Minor

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
  
 

 