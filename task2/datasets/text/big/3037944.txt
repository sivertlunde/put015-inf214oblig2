The Guardian (2006 film)
{{Infobox film
| name           = The Guardian
| image          = The Guardian (2006 film) promotional poster.jpg
| caption        = Theatrical release poster Andrew Davis
| producer       = Armyan Bernstein Lowell D. Blank Zanne Devine Beau Flynn
| writer         = Ron L. Brinkerhoff
| starring       = Kevin Costner  Ashton Kutcher
| music          = Trevor Rabin
| cinematography = Stephen St. John
| editing        = Thomas J. Nordberg Dennis Virkler Beacon Communications Buena Vista Pictures
| released       =  
| runtime        = 139 minutes
| country        = United States
| language       = English
| budget         = $80 million
| gross          = $94,973,540
}} action drama drama film Andrew Davis. The setting for the film is the United States Coast Guard and their Aviation Survival Technician (AST) program.

==Plot==
Ben Randall (Kevin Costner) is the top rescue swimmer at the United States Coast Guards Aviation Survival Technician (AST) program. Jake Fischer (Ashton Kutcher) is a hot-shot candidate for AST, who was ranked as a top competitive swimmer in high school with scholarships to every Ivy league college, but he opted to enlist in the Coast Guard. The films title is introduced by a mythic tale: people lost at sea often claim they feel a presence lifting them to the surface, breathing life into their bodies while they are waiting for help to arrive. They call this presence "The Guardian".

Ben is confronted by his wife, who requests a separation due to his frequent time at work. He receives a page for an immediate rescue. Out at sea, he loses his rescue team in an HH-60J Jayhawk helicopter crash and, while waiting in a survival raft, his best friend, Chief Petty Officer Carl Billings (Omari Hardwick), dies. Shaken, Ben is forced to either retire or teach at a Coast Guard training school; he reluctantly chooses the latter. Here, Jake arrives as a hopeful AST candidate at "A" school, where Ben is considered a legend with a countless number of saves.

During the training, Jake meets a local schoolteacher, Emily Thomas (Melissa Sagemiller), and they begin a "casual" relationship, as they both know their time together is limited. Once the initial weeks of training are over and most of the students have dropped out, detailed instruction begins at the academy. After sleeping at Emily’s house, Jake arrives late to class, and Ben confronts him. Although Jake is not expelled, he and the entire team are punished for his tardiness. Ben tries to force Jake to quit, but later sees Jakes persistence and dedication. 

Meeting Emily in a bar, Jake tells her about his beating all of his instructor Ben Randalls records. However, Maggie the barkeep (Bonnie Bramlett), an old friend of Bens, tells Jake of the unbreakable record Ben set at a rescue at a ship fire, where Ben worked tirelessly to save all the victims. With one man left and a broken winch, Randall held the man by his fingertips for the entire flight, through landing, resulting in extensive injuries to Bens hand and shoulder.
 Navy bar, they get involved in a fight and land in jail, thusly standing up Jakes girlfriend. Jake arrives back at base beaten and bandaged, and takes the blame entirely. 

Ben confronts Jake about why he left his prospects as a competitive swimmer to join the AST program, and tells Jake what he learned about Jakes past: on a late night out, Jake, the designated driver, got into an accidental automobile crash, resulting in the deaths of his high school relay team. After a moment of sorrow, Ben and Jake share common ground; they both know how it feels to be the only survivor. 

Instruction nears completion, and Jake takes to the role of leader during exercises. At graduation only a handful of the original candidates remain. Emily comes to see Jake graduate, but the two part ways because Jake is leaving town.
 CG Air Station Kodiak, Alaska, Randalls previous post. On a mission together, they are sent to rescue two kayakers trapped in a cave. Ben experiences flashbacks and appears to be incapacitated during the rescue. However Jake is able to guide him and together successfully rescue the kayakers. Ben retires and tells Jake of his only record he kept track of – the 22 people he lost during his career. As Jake is sent out on another mission to rescue the crew of a sinking fishing trawler, Ben visits and apologizes to his wife and gives her the signed divorce papers. Meanwhile during the rescue, Jake becomes trapped in the ship after attempting to rescue the Captain and his helicopter is forced to return to base. Back at the station, Ben hears of the situation and opts to suit up to rescue Jake.

Once on scene, Ben finds and releases Jake from the hull of the ship. As they hook up to the winch and proceed upwards towards the helicopter, their combined weight causes the cable to begin separating. Knowing that the cable would not last, Ben unclips himself from the cable so that Jake can survive. As Ben falls Jake catches him by the hand and tells him he will not let him go. Ben sees that Jake will do anything to keep him alive and he says " I know...." but Ben does not want him to die, so he unstraps his glove, plummeting from a fatal height into the ocean. Ben does not resurface and his body is never found.

Jake is on a rescue mission sometime later, when one of the survivors tells of a man in the sea who refuses to let go. Jake connects this to the legend, as well as Ben. He goes back to Emily and they rekindle their relationship.

==Cast==
Many of the supporting actors, including ASTC instructors, helicopter pilots, and support personnel, are actual U.S. Coast Guard rescue swimmers, pilots, and ground personnel. Several characters, including Kutchers, identify themselves as Airman. An Airman is the enlisted rating of a Coast Guardsman who is undesignated and/or currently undergoing training in an aviation related field. Similar ratings within the Coast Guard are those of Seaman and Firefighter|Fireman.
 Athens Olympic Games. 
 
* Kevin Costner as Senior Chief Aviation Survival Technician Ben Randall
* Ashton Kutcher as Airman/Petty Officer 3rd Class Jake Fischer
* Neal McDonough as Chief Aviation Survival Technician Jack Skinner
* Melissa Sagemiller as Emily Thomas
* Clancy Brown as Captain William Hadley
* Brian Geraghty as Aviation Survival Technician Third Class Billy Hodge
* Sela Ward as Helen Randall
* Omari Hardwick as Chief Petty Officer Carl Billings
* Michael Rady as Zingaro
* Peter Gail as Airman Danny Doran
* Shelby Fenner as Airman Cate Lindsey
* Damon Lipari as Damon Bennett
* Bonnie Bramlett as Maggie McGlone John Heard as Captain Frank Larson
* Dulé Hill as Airman Ken Weatherly
* Brian Patrick Wade as Mitch Lyons
* Joe Arquette as Co-Pilot Antunez
* Andrew Schanno as Pilot Henry Mitchell
* Tilky Jones as Tilky Flint
* Jeff Loftus as USCG Commander, Executive Officer 
* Daniel Molthen as Richard Wakefield
* Bryce Cass as Manny
* Chicago Catz as the bar band
 

==Production==
 
  used for filming]]
 
* The production company hired local contractors to build a massive indoor wave pool for production. in the southern United States in 2005, production moved to Shreveport, Louisiana. Some of the base scenes were filmed at Barksdale Air Force Base in Bossier City, Louisiana and at Camp Minden in Minden, Louisiana.
* The film was revised after Hurricane Katrina, with the addition of several comments on the storm and the rescues. The end credits are replete with "glory" shots of U.S. Coast Guard helicopters conducting rescues in the greater New Orleans area. The DVD contains a special feature on U.S. Coast Guard rescue operations, especially in the aftermath of Katrina. CG Air Station Elizabeth City, North Carolina. 60,000 pounds of ice were needed on the set.
* The training pool used in the movie was LSU-Shreveports natatorium.

==Historical relevance==
The mishap where Randall loses his crew is loosely based on an actual U.S. Coast Guard aviation mishap in Alaska. The aircraft was an Sikorsky S-61R|HH-3F Pelican (USCG variant of the Jolly Green Giant) instead of the HH-60 Jayhawk|HH-60J Jayhawk (USCG variant of the Blackhawk/Seahawk) pictured in the movie. 

==Release==
===Box office===
The film earned $18 million on its opening weekend, and almost $95 million worldwide by January 4, 2007. 

===Critical reception===
The film received average reviews:   rates it a 53/100 based on 29 reviews.    at yet adds: The Guardian "regurgitates formulaic elements in a way that pays off repeatedly and potently." 

==Home media==
 

===Alternate ending===
In an alternate ending found on the DVD, Randall survives. As he unhooks and tries to fall, Jake again grabs him and vows not to let go. Instead of unstrapping his glove, Randall lets the cable pull them up and it breaks just as they get into the helicopter. This ending was added because some of the writers were worried that the original ending was too strong for viewers. Nonetheless, it was scrapped when Disney chairman, Dick Cook, applauded the original ending.

===Soundtrack===
{{Album ratings
| rev1      = iTunes
| rev1Score =    
}}

The soundtrack was released on September 12, 2006.  The soundtrack uses a variety of music genres, including R&B, Country music, Rock and Soul blues.

;Track listing
{{Track listing
| total_length = 50:29
| title1 = Never Let Go
| note1 = performed by Bryan Adams
| length1 = 5:05
| title2 = Something to Talk About
| note2 = performed by Shedaisy
| length2 = 3:54
| title3 = Saturday Night
| note3 = performed by Ozomatli
| length3 = 4:01
| title4 = Love & Happiness
| note4 = performed by Bonnie Bramlett
| length4 = 4:32
| title5 = The Mockingbird
| note5 = performed by Lisa Lavie
| length5 = 3:07
| title6 = Hold Tight
| note6 = performed by Tad Robinson
| length6 = 4:03
| title7 = Tri-Me
| note7 = performed by Abby Ahmad
| length7 = 4:33
| title8 = Hold On, Im Coming
| note8 = performed by Bonnie Bramlett
| length8 = 2:57
| title9 = Shake Up the World
| note9 = performed by Stevie "Funkworm" Butler
| length9 = 4:09
| title10 = Friday Night
| note10 = performed by Cheryl Wilson
| length10 = 3:00
| title11 = Run Me in the Dirt (Throwdown)
| note11 = performed by Butch Flythe & Joseph "Butch" Flythe
| length11 = 3:29
| title12 = The Guardian Suite
| note12 = performed by Trevor Rabin
| length12 = 7:39
}}

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 