Man of the House (2005 crime comedy film)
{{Infobox film
| name        = Man of the House
| image       = Man of the housemposter.jpg
| caption     = Theatrical release poster
| director    = Stephen Herek
| producer    = Steven Reuther Todd Garner Allyn Stewart Robert Ramsey Matthew Stone John J. McLaughlin
| story       = John J. McLaughlin Scott Lobdell
| starring    = Tommy Lee Jones David Newman
| cinematography = Peter Menzies Jr.
| editing     = Chris Lebenzon Joel Negron
| studio      = Revolution Studios Bel Air Entertainment
| distributor = Columbia Pictures
| released    =  
| runtime     = 100 minutes
| language    = English
| country     = United States
| awards      =
| budget      = $40 million 
| gross       = $21,577,624
}} Texas Ranger University of Texas Governor Rick Perry has a cameo appearance in the film as himself. The house used in the film was The Star of Texas Inn.

== Plot == Texas Rangers, Roland Sharp (Tommy Lee Jones) and Maggie Swanson (Liz Vassey), are going to a church in order to question Percy Stevens (Cedric the Entertainer) about the whereabouts of his former prison roommate, Morgan Ball, who they want to testify against organized crime boss John Cortland. Percy is indignant, telling Sharp and Swanson that he is a "man of God" and hasnt spoken with Ball in years. However, Percys cellphone rings, displaying Balls name. Sharp and Swanson track down Ball to the warehouse, where Ball gives Sharp a key in an attempt to buy him off. Instead, Sharp takes the key and forces Ball outside, where FBI agent Eddie Zane (Brian Van Holt) is waiting. As they talk, a sniper begins shooting, wounding Swanson and giving Ball a chance to escape. It is revealed the sniper is after Ball, and a group of cheerleaders from the University of Texas at Austin witness his murder. Agent Zane is found shot in the arm next to Balls body and claims he didnt see the sniper.

Anne (Christina Milian), Teresa (Paula Garcés), Evie (Monica Keena), Heather (Vanessa Ferlito) and Barb (Kelli Garner) are taken to the police station, where they all have conflicting descriptions of the shooter. Sharp is given the task of protecting the girls at all times, because their fathers are worried about them. The information is relayed to him by the Governor of Texas (Rick Perry). It is revealed that Sharp is divorced and has a daughter, Emma (Shannon Marie Woodward) who is in high school and doesnt feel as though her father had ever been around. It is also revealed that John Cortlin has been exonerated from all charges pressed on him due to a lack of evidence. It is also revealed that FBI Agent Zane is working with Cortlin, having killed Ball and shot himself in the arm. Cortlin scolds Zane for letting some "loose ends" escape, and Zane begins searching for Sharp and the cheerleaders; he also kills the sniper he hired. With Swanson in the hospital recovering from her near-fatal wound, Sharp and two additional rangers must now pick the girls up from school and secure their sorority house. Sharp moves in with the girls and the two young men with him move into the fraternity house across the street, where they end up busting a drug deal.
 Longhorns football game he tackles an opposing teams mascot when the mascot approaches the girls with a gun, later revealed to be a water gun. Other humorous mishaps occur, but Sharps relationship with the girls begins to strengthen; indeed, Barb begins to develop a crush on him. However, Sharp finds himself attracted to Barbs English teacher Molly (Anne Archer) who calls him into her office to complain about Barbs plagiarism. Later he invites her over for dinner, which the girls coach him through using an earpiece and tiny video screen. After they fall asleep, he turns it off and woos Molly himself. He admits to the girls about his last failed marriage and the way he feels about his estranged daughter. This interests Evie, who has a 4.0 GPA and wants to write a paper on Emma. She uses the houses "emergency phone" to call her, revealing Sharps location to Zane, who had contacted Emma.

Sharp takes the girls to a "spirit rally", where he is forced to give a speech about cheerleading. He becomes more and more impassioned, proving to the girls he finally "gets it". The night is ruined, however, when Sharp realizes in the nick of time that somebody put a bomb under their van, and Teresa (who was convinced that nobody was after them) is almost killed when her seatbelt gets stuck. Sharp saves her, and she admits that maybe someone is trying to kill them. Evie tells Sharp she contacted Emma, and when Sharp calls his daughter he learns that Zane has her. He tells Sharp to take the key Ball gave him in the beginning of the movie to open a lockbox, both of them unaware the cheerleaders are listening in. The next day Sharp gets the money out of the lockbox and drives to where Zane instructs him. Zane gives Sharp instructions over a cell phone, telling him he will shoot Emma if Sharp disobeys. After Sharp handcuffs himself to the steering wheel, Zane thanks him and tells him hes a "good parent." Zane takes off with the money, Sharps keys and phone, and Emma. Zane and Emma get on a bus, but as it pulls away Sharp sees Barb in the back. As he wonders what is going on, Heather gets in the car with him and picks his handcuffs. They take off after the bus in a stolen Volkswagen Beetle. On the bus, Teresa pretends to go into labor and Evie demands the bus be stopped. She attempts to steal the bag with the money, but Zane pulls a gun and Evie runs away from the bus. Everyone else ran away, with Zane telling to leave. He forced the driver out, too, becoming one himself. Emma is rescued by the cheerleaders, but Zane attempts to drive to Mexico with the bag to hide. Sharp shoots a wheel, causing the bus to flip over 90 degrees. Zane gets out the bus, his face injured after he hits the buss ceiling. The USA/Mexico border closes and the border officers draw guns towards him, forcing him to surrender after seeing his gun. Sharp shoots the gun out of Zanes hand, and handcuffs him by the handcuffs he used handcuff himself when Zane instructed him. He and Emma are reunited. At the end of the film, Cortlin is arrested and taken back to court, Sharp and Molly are married, and Emma and the cheerleaders are a part of the service.

== Cast ==
* Tommy Lee Jones as Roland Sharp
* Cedric the Entertainer as Percy Stevens
* Vanessa Ferlito as Heather
* Monica Keena as Evie
* Kelli Garner as Barb
* Christina Milian as Anne
* Paula Garcés as Teresa
* Anne Archer as Professor Molly McCarthy
* Brian Van Holt as Eddie Zane
* Shea Whigham as Ranger Holt
* Terrence Parks as Ranger Riggs
* R. Lee Ermey as Captain Nichols
* Paget Brewster as Binky
* Shannon Marie Woodward as Emma Sharp
* Liz Vassey as Maggie Swanson

== Reception ==

=== Critical reaction ===
Man of the House was met with largely negative reviews upon its release. Review aggregation website Rotten Tomatoes gives the film a score of 8% based on 61 reviews. On Metacritic, the film has a weighted average score of 35%, based on 20 reviews.

=== Box Office ===
In its opening weekend, the film grossed $8,917,251 in 2,422 theaters in the United States and Canada, ranking #5 at the box office and averaging $3,681 per theater. The film closed on April 7, 2005 with a North American domestic gross of $19,699,706 and an international gross of $1,877,918 for a worldwide gross of $21,577,624.

== Soundtrack == Man of the House" - Chuck Wicks We Are Family" - Pointer Sisters
* "Rising Sun" - Rusted Root Lil Jon & the East Side Boyz
* "U Cant Touch This" - Tree Adams All I Wanna Do" - Sheryl Crow
* "Should I Stay Or Should I Go" - The Clash
* "Funny How Time Slips Away" - Willie Nelson

==References==
 

== External links ==
*  
*  
*  
*  
*   at ManoftheHouse.com

 

 
 
 
 
 
 
 
 
 
 