Tombstone (film)
{{Infobox film
| name           = Tombstone
| image          = Tombstoneposter.jpeg
| caption        = Theatrical release poster
| director       = George P. Cosmatos
| producer       = James Jacks Sean Daniel Bob Misiorowski
| writer         = Kevin Jarre
| narrator       = Robert Mitchum
| starring       = Kurt Russell Val Kilmer 
| music          = Bruce Broughton
| cinematography = William A. Fraker
| editing        = Frank J. Urioste Roberto Silvi Harvey Rosenstock
| studio         = Hollywood Pictures Cinergi Pictures Buena Vista Pictures
| released       =  
| runtime        = 130 minutes   134 minutes   
| language       = English
| budget         = $25 million 
| gross          = $56,505,065   
}} western film directed by George P. Cosmatos, written by Kevin Jarre (who was also the original director, but was replaced early in production    ) and starring Kurt Russell and Val Kilmer, with Sam Elliott, Bill Paxton, Powers Boothe, Michael Biehn, and Dana Delany in supporting roles, as well as a narration by Robert Mitchum.

The film is based on events in Tombstone, Arizona, including the Gunfight at the O.K. Corral and the Earp Vendetta Ride, during the 1880s. It depicts a number of western outlaws and lawmen, such as Wyatt Earp, William Brocius, Johnny Ringo, and Doc Holliday.
 Western genre it ranks number 14 in the list of highest grossing films since 1979.  Critical reception was generally positive, but the film failed to garner award nominations for production merits or acting from any mainstream motion picture organizations.

==Plot== Virgil (Sam Morgan (Bill Josephine Marcus (Dana Delany) and Mr. Fabian (Billy Zane) are also newly arrived in Tombstone with a traveling theater troupe. Meanwhile, Wyatts wife, Mattie Blaylock (Dana Wheeler-Nicholson), is becoming dependent on a potent narcotic. Wyatt and his brothers begin to profit from a stake in a gambling emporium and saloon when they have their first encounter with a band of outlaws called the Cowboys, led by William Brocius|"Curly Bill" Brocious (Powers Boothe). The Cowboys are identifiable by the red sashes worn around their waist. 
 Stephen Lang) Turkey Creek Jack Johnson (Buck Taylor), join forces to administer justice.

Wyatt and his posse are ambushed in a riverside forest by the Cowboys. Hopelessly surrounded, Wyatt seeks out Curly Bill and kills him in a fast draw gunfight. Curly Bills second-in-command, Johnny Ringo (Michael Biehn), becomes the new head of the Cowboys. When Docs health worsens, the group are accommodated by Henry Hooker (Charlton Heston) at his ranch. Ringo sends a messenger (dragging McMasters corpse) to Hookers property telling Wyatt that he wants a showdown to end the hostilities; Wyatt agrees. Wyatt sets off for the showdown, not knowing that Doc had already arrived at the scene. Doc confronts a surprised Ringo and kills him in a duel. Wyatt runs when he hears the gunshot only to encounter Doc. They then press on to complete their task of eliminating the Cowboys, although Clanton escapes their vengeance. Doc is sent to a sanatorium in Colorado where he later dies of his illness. At Docs urging, Wyatt pursues Josephine to begin a new life. The film ends with a narration of an account of their long marriage, ending with Wyatts death in Los Angeles in 1929.

==Cast==
 
 
* Kurt Russell as Wyatt Earp
* Val Kilmer as Doc Holliday
* Sam Elliott as Virgil Earp
* Bill Paxton as Morgan Earp
* Powers Boothe as William Brocius|"Curly Bill" Brocius
* Michael Biehn as Johnny Ringo
* Charlton Heston as Henry Hooker
* Jason Priestley as Billy Breakenridge
* Jon Tenney as Sheriff Johnny Behan
* Stephen Lang as Ike Clanton
* Thomas Haden Church as Billy Clanton Josephine Marcus
* Paula Malcomson as Allie Earp Lisa Collins as Louisa Earp
* John Philbin as Tom McLaury
* Robert Mitchum as Narrator (voice) 
 
* Dana Wheeler-Nicholson as Mattie Blaylock
* Joanna Pacuła as Big Nose Kate
* Michael Rooker as Sherman McMaster Fred White
* Billy Bob Thornton as Johnny "Madcap" Tyler
* Tomas Arana as Frank Stilwell
* Paul Ben-Victor as Florentino Cruz
* Robert John Burke as Frank McLaury
* Billy Zane as Mr. Fabian John Corbett as Barnes Turkey Creek Jack Johnson
* Terry OQuinn as Mayor John Clum
* Frank Stallone as Ed Bailey
* Peter Sherayko as Texas Jack Vermillion
* Christopher Mitchum as Ranch Hand
* Don Collier as High Roller 
* Pedro Armendáriz Jr. as Priest
 

==Production==
===Filming===
The film was shot primarily on location in Arizona. 
 his own movie with Lawrence Kasdan.    Russell made an agreement with executive producer Andrew G. Vajna to finance Tombstone with a budget of $25 million. 
 Walt Disney The Last Temptation of Christ.  As Costner was making a competing Wyatt Earp film, he used his then-considerable clout to convince most of the major studios to refuse to distribute Tombstone—Disney was the only studio willing to do so.  Jarre and Russell then went with their next choice, Val Kilmer.

Filming was plagued with several problems. Russell and Kilmer both have said that the screenplay was too long (Russell estimated by 30 pages).  Kilmer told True West Magazine, "virtually every main character, every cowboy, for example, had a subplot and a story told, and none of them are left in the film."  He said that over 100 people, cast and crew, either quit or were fired over the course of the production.  Russell even went so far as to cut his own scenes in order to let other actors have more screen time. 

Early in the production, screenwriter Jarre was fired as director due to his refusal to cut his screenplay and going over schedule.  Disney panicked because the film was two weeks behind and contacted   (1985). After Cosmatos death in 2005, Russell claimed in the True West Magazine interview that Cosmatos had in fact ghost-directed the movie on Russells behalf. Russell claimed he gave Cosmatos a shot list every night for the next day, and developed a "secret sign language" on set to exert influence.  

Robert Mitchum was originally set to play Newman Haynes Clanton, but suffered a horse riding accident which left him unable to work. Mitchum ultimately narrated the film, and the part was written out of the script. Much of Old Man Clantons dialogue was spoken by other characters, particularly Curly Bill, who was effectively made the gang leader in lieu of Clanton. Glenn Ford was also cast as Marshall White, and Harry Carey, Jr. was to play a wagonmaster, but Ford dropped out of the project and Carey was cast as White.

===Soundtrack===
The original motion picture soundtrack for Tombstone was originally released by Intrada Records on December 25, 1993.  On March 16, 2006, an expanded two-disc version of the film score was also released by Intrada Records.  The score was composed and produced by Bruce Broughton, and performed by the Sinfonia of London. David Snell conducted most of the score (although Broughton normally conducts his own scores, union problems mandated another conductor here), while Patricia Carlin edited the films music.   
 The Searchers (1956) with variations on the Indian Traders theme used midway through the Ford movie. The album begins with the Cinergi logo, composed by Jerry Goldsmith and conducted by Broughton.

{{Infobox album
| Name = Tombstone: Complete Original Motion Picture Soundtrack
| Type = Film
| Artist = Bruce Broughton
| Cover = TombstoneSoundtrack.jpg
| Released = March 16, 2006
| Length = 1:25:29 Intrada
}}

{{Track listing
| headline        = Disc: 1
| total_length    = 
| title1          = Logo
| length1         = 0:21
| title2          = Prologue; Main Title; And Hell Followed
| length2         = 3:50
| title3          = A Family
| length3         = 2:03
| title4          = Arrival in Tombstone
| length4         = 2:14
| title5          = The Town Marshall; A Quarter Interest
| length5         = 0:48
| title6          = Josephine
| length6         = 1:30
| title7          = Gotta Go to Work
| length7         = 1:10
| title8          = Ludus Inebriatus
| length8         = 1:15
| title9          = Fortuitous Encounter; Wyatt and Josephine
| length9         = 5:16
| title10         = Thinking Out Loud
| length10        = 0:28
| title11         = Opium Den; Law Dogs; You Got a Fight Comin
| length11        = 7:08
| title12         = Virgil Thinks
| length12        = 0:53
| title13         = The Antichrist; Gathering for a Fight; Walking to the Corral; OK Corral Gunfight
| length13        = 7:36
| title14         = Aftermath
| length14        = 1:30
| title15         = The Dead Dont Dance; Dehan Warns Josephine; Upping the Ante; Morgans Murder
| length15        = 5:15
| title16         = Defections
| length16        = 0:58
| title17         = Morgans Death
| length17        = 2:12
| title18         = Hells Comin; Wyatts Revenge
| length18        = 3:53
| title19         = No More Curly Bill
| length19        = 0:36
| title20         = The Former Fabian
| length20        = 1:34
| title21         = Brief Encounters; Ringos Challenge; Doc and Wyatt
| length21        = 5:38
| title22         = Youre No Daisy; Finishing It
| length22        = 3:55
| title23         = Doc Dies
| length23        = 2:46
| title24         = Looking at Heaven; End Credits
| length24        = 8:45
}}

{{Track listing
| collapsed       = no
| headline        = Disc: 2
| total_length    = 
| title1          = Arrival in Tombstone
| note1           = w/alternate intro
| length1         = 2:14
| title2          = Josephine
| note2           = short version
| length2         = 1:00
| title3          = Fortuitous Encounter
| note3           = w/alternate mid-section
| length3         = 2:26
| title4          = Morgans Death
| note4           = short version
| length4         = 1:47
| title5          = Tombstone
| note5           = main theme only
| length5         = 2:23
| title6          = Pit Orchestra Warm-Up
| length6         = 0:39
| title7          = Thespian Overture
| note7           = long
| length7         = 0:45
| title8          = Tympani
| length8         = 0:08
| title9          = Waltz
| length9         = 0:14
| title10         = Piano/Cello Duet
| length10        = 0:36
}}

==Marketing==
===Novel===
A paperback novel published by Berkley Publishers titled Tombstone, was released on January 1, 1994. The book dramatizes the real-life events of the Gunfight at the O.K. Corral and Earp Vendetta, as depicted in the film. It expands on western genre ideas written by Kevin Jarres screenplay, which took place during the 1880s. 

==Reception==
===Box office=== Costner and Wyatt Earp, on December 24, 1993 in wide release throughout the United States. During its opening weekend, the film opened in third place, grossing $6,454,752 in business showing at 1,504 locations.     The films revenue increased by 35% in its second week of release, earning $8,720,255. For that particular weekend, the film jumped to third place, screening in 1,955 theaters. The film went on to earn $56,505,065 in total ticket sales in the North American market.  It ranks 20th out of all films released in 1993. 

===Critical response===
 . IGN Entertainment. Retrieved 2011-03-15.  Following its cinematic release in 1993, Tombstone was named "One of the 5 greatest Westerns ever made" by True West Magazine. The film was also called "One of the years 10 best!" by KCOP-TV in Los Angeles, California.  

Siskel & Ebert originally thought they would have to miss reviewing the film as they could not get a screening but, as Ebert explained, "... a strange thing started to happen. People started telling me they really liked Val Kilmers performance in Tombstone, and I heard this every where I went. When you hear this once or twice, its interesting, when you hear it a couple of dozen times, its a trend. And when you read that Bill Clinton loved the performance, you figured you better catch up with the movie." Ultimately, Ebert recommended the movie while Siskel did not.
 Wild Bill, Dark Blue, he stated, "Every time I see Russell or Val Kilmer in a role, Im reminded of their Tombstone, which got lost in the year-end holiday shuffle and never got the recognition it deserved."

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#FFFFE0; color:black; width:25em; max-width: 20%;" cellspacing="5"
|style="text-align: left;"|"Grafted onto this traditional framework, the films meditative aspects are generally too self-conscious to fit comfortably. Especially when the movie tries to imagine a more enlightened role for women in the Old West, the screenplay begins to strain."
|-
|style="text-align: left;"|—Stephen Holden, writing in The New York Times Holden, Stephen (December 24, 1993).  . The New York Times. Retrieved 2011-03-17. 
|}
In a mixed review, Chris Hicks writing in the  . Retrieved 2011-03-17. 
 Time Out commented that "Kilmer makes a surprisingly effective and effete Holliday". He negatively acknowledged that there was "a misguided romantic subplot and the ending rather sprawls" but ultimately exclaimed the film was "rootin, tootin entertainment with lots of authentic facial hair." 
 
{|class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#FFFFE0; color:black; width:40em; max-width: 30%;" cellspacing="5"
|style="text-align: left;"|"From the audiences viewpoint, its difficult to assign responsibility for the most serious of this films shortcomings, but one thing is clear: somewhere along the way, the creative process misfired. Large segments of Tombstone belong buried at Boot Hill."
|-
|style="text-align: left;"|—James Berardinelli, writing for ReelViews 
|}
Richard Harrington of The Washington Post highlighted on the films shortcomings by declaring, "too much of Tombstone rings hollow. In retrospect, not much happens and little that does seems warranted. There are so many unrealized relationships you almost hope for redemption in a longer video version. This one is unsatisfying and unfulfilling."  Alternately though, columnist Bob Bloom of the Journal & Courier openly remarked that the film "May not be historically accurate, but offers a lot of punch for the buck." He concluded by saying it was "A tough, guilty-pleasure Western." 

===Home media=== Region 1 Code widescreen edition of the film was released on DVD in the United States on December 2, 1997. Special features for the DVD include French and Spanish subtitles, Dolby Digital Surround Sound, original theatrical trailers, and chapter search options.  A directors cut of Tombstone was also officially released on DVD on January 15, 2002. The DVD version includes a two-disc set and features "The Making of Tombstone" featurette in three parts; "An Ensemble Cast"; "Making an Authentic Western"; and "The Gunfight at the O.K. Corral". Other features include an audio commentary by director George P. Cosmatos, an interactive Tombstone timeline, the directors original storyboards for the O.K. Corral sequence, the Tombstone "Epitaph" – an actual newspaper account, the DVD-ROM feature "Faro at the Oriental: Game of Chance", and a collectible Tombstone map.  

The widescreen high-definition Blu-ray Disc edition of the theatrical cut was released on April 27, 2010, featuring the making of Tombstone, directors original storyboards, trailers and TV spots.  A supplemental viewing option for the film in the media format of video-on-demand is available as well. 

 

==References==
;Footnotes
 

;Further reading
 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
 

==External links==
 
*  
*  
*  
*  
*  
*   at the Movie Review Query Engine
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 