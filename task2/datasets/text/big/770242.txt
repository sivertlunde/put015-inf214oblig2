Flash Gordon Conquers the Universe
{{Infobox film
| name           = Flash Gordon Conquers the Universe
| image          = Flash_Gordon_3.jpg Ray Taylor
| producer       = Henry MacRae
| writer         = George H. Plympton Basil Dickey Barry Shipman Alex Raymond (comic strip) Carol Hughes Charles B. Middleton Frank Shannon Roland Drew
| cinematography = Jerome Ash William Sickner Universal Pictures
| released       =  
| runtime        = Brazil:220 min / UK:195 min (12 episodes)
| country        = United States
| language       = English
}}
 Ray Taylor, Universal Pictures. Carol Hughes as Dale Arden,  Charles B. Middleton as Ming the Merciless, and Roland Drew as Prince Barin. In 1966 the serial was edited into two feature films for television showing Purple Death from Outer Space and Perils from the Planet Mongo.
 Universal Pictures titles. In the mid-1970s, all three serials were shown by PBS stations across the US, bringing Flash Gordon to a new generation, a full two years before Star Wars and Close Encounters of the Third Kind re-ignited interest in the science fiction genre. The re-edited television version, with the title card reading Flash Gordon - Space Soldiers Conquer the Universe, was used for some VHS and DVD releases of the serial.

==Plot summary== plague is Mongo to polarite in the Kingdom of Frigia. Flash and Zarkov distribute the antidote the same way the original Death Dust was spread. Ming sends an army of robot bombs after the three and he succeeds in capturing Zarkov for a short time before Flash frees him.

The trio continue to battle Ming and his allies. Mings Captain Torch is the "head villain" of this serial. He is in charge of stopping the Earthlings.
 Solarite into it. Prince Barin takes his rightful place as ruler of Mongo. Mings last words are "I am the universe!". Zarkov announces that Flash Gordon has conquered the universe.

==Cast==
  with Carmen DAntonio and Charles Middleton]]
* Buster Crabbe as Flash Gordon Carol Hughes as Dale Arden
* Frank Shannon as Hans Zarkov|Dr. Alexis Zarkov
* Charles B. Middleton as Ming the Merciless. Ming is portrayed as a military dictator in this serial, rather than as a Fu Manchu or Devil-like character as in the two previous Flash Gordon serials. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 44
 | chapter = 2. "We Come from Earth, Dont You Understand?" 
 }} 
* Roland Drew as Prince Barin
* Shirley Deane as Princess Aura
* Donald Curtis as Captain Ronal Lee Powell as Roka
* Ron Rowan as Officer Torch
* Victor Zimmerman as Officer Thong
* Anne Gwynne as Lady Sonja John Hamilton as Professor Gordon
* Edgar Edwards as Captain Turan
* William Royale as Captain Sudan
* Sigurd Nilssen as Count Korro
* Luli Deste as Queen Fria Michael Mark as Professor Karm
* Byron Foulger as Professor Druk
* Ray Mala as Prince of the Rock People

==Production==
Plot points taken from the preceding serial, Flash Gordons Trip to Mars.
 Buck Rogers serial. One money-saving gimmick used by Universal Studios was to take some exciting mountain climbing search and rescue scenes from the  German film White Hell of Pitz Palu (1930)  and its music as well.
 ingenue whose role does not develop until the middle of the serial.

Jean Rogers who had played Dale Arden in the two previous Flash Gordon serials was contracted to 20th Century Fox and neither she nor her studio wanted to repeat the Dale Arden role so it was given to a recent Universal contract starlet Carol Hughes. 

==Critical reception==
According to Harmon and Glut, Flash Gordon Conquers the Universe "was the most picturesque of the trilogy but surrendered much compelling charm for its cinematic sophistication." 

==Chapter titles==
# The Purple Death
# Freezing Torture
# Walking Bombs
# The Destroying Ray
# The Palace of Horror
# Flaming Death
# The Land of the Dead
# The Fiery Abyss
# The Pool of Peril
# The Death Mist
# Stark Treachery
# Doom of the Dictator
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 226
 | chapter = Filmography
 }} 

== Soundtrack ==
Les préludes by Franz Liszt

==See also==
*Purple Death from Outer Space

==References==
 
 

==External links==
* 
* 
* 
* 
* 
* 

===Download or view online===
* 
* 
  
*  
*  
*  
*  
*  
*  
 
*  
*  
*  
*  
*  
*  
 

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 