Sparsh (film)
{{Infobox film 
  | name          = Sparsh image          = Sparsh, 1980 Hindi film.jpg   writer         = Sai Paranjpye producer       = Basu Bhattacharya director       = Sai Paranjpye starring       = Naseeruddin Shah Shabana Azmi Sudha Chopra Om Puri editing        =Om Prakash Makkar	 	   cinematography = Virendra Saini	 music          = Kanu Roy released       =   runtime        = 145 minutes   language       = Hindi/Urdu
}}

Sparsh ( : Touch) is a 1980 Indian Hindi feature film directed by Sai Paranjpye. It stars Naseeruddin Shah and Shabana Azmi playing the characters of a visually impaired principal and a sighted teacher in a school for the blind, where they fall in love though soon their complexes tag along and they struggle to get past them to reconnect with the "touch" of love. The film remains most memorable for the subtle acting of its leads, plus the handling of the issue of relationships with the visually handicapped, revealing the emotional and perception divide between the world, the "blind" and the "sighted", epitomized by the characters.   The film won the National Film Award for Best Feature Film in Hindi 
 Filmfare Award Best Dialogue Best Actress, Best Actor lost out to Anupam Kher in Saaransh.

== Synopsis ==
 
This film is about the blindness|blind, in particular about the lives and feelings of blind children and the principal of their school. Sparsh refers to the sensation and feeling of touch upon which blind people rely in the absence of sight.

The story opens with Anirudh Parmar (Naseeruddin Shah) as the principal of Navjivan Andhvidyalay, a school for the blind that educates about 200 blind children. Anirudh has a dark and lonely existence for the most part. One day, while on his way to the doctor, he hears a lovely song and ends up mesmerized at the singers door instead of the doctor. 

The voice belongs to Kavita Prasad (Shabana Azmi), a young woman recently widowed after three years of marriage. Kavita, too, prefers a secluded existence. Her childhood friend Manju (Sudha Chopra) is about the only friend she has.

 
Manju throws a small party where Kavita and Anirudh meet again. He recognizes her from her voice. During the conversation, he mentions that the school is looking for volunteers to read to, sing to, teach handicrafts and spend time with the children. Kavita is reluctant, but she is urged by Manju and her husband Suresh to strongly consider it. Kavita decides to volunteer.

As Kavita spends more time at the school, she begins forming a friendship with Anirudh. The friendship grows stronger over time and they become engaged. But their personalities and feelings are different. Anirudh is of strong character: He firmly believes that the blind need help but not pity or charity. (When once, in his office, Kavita attempts to assist him with coffee, he gets enraged at the thought of his guest offering to overcome his obvious difficulty with hospitality.) Kavita, recently bereaved, looks to the school (and Anirudh) as a way towards an ideal, one of sacrificial service. Anirudh gets wind of this and assumes Kavita is simply seeking to fill the void in her life with this form of service. He assumes she accepted the proposal not out of love but as a sacrifice towards a way out of her dark life. During this time, Anirudhs fellow blind friend Dubey (Om Puri) laments that his recently deceased wife was not happy in their marriage.

Anirudh is shaken, confused and disturbed by all this. He breaks off the engagement (but does not mention the reason to Kavita). She accepts his decision.

Kavita, now a salaried employee of the school, continues to help the children. The initial coldness between her and Anirudh gives way to friction and eventually, over a series of events at the school, brings up the feelings they were not able to discuss before. The situation spirals downward and one of them must leave the school.

The movie ends with Anirudh and Kavita being touched by the depth of their feelings for one another and finally seeing a way out.

==Cast==
* Shabana Azmi - Kavita
* Naseeruddin Shah - Anirudh Parmar
* Sudha Chopra - Manju
* Mohan Gokhale - Jagdish
* Pran Talwar		
* Arun Joglekar		
* Om Puri - Dubey

==Awards== National Film Awards
* 1980: National Film Award for Best Feature Film in Hindi
* 1980:  
* 1980:  

;Filmfare Awards
* 1985:  
* 1985:  
* 1985:  

==Music==
Sparshs music is by veteran music director Kanu Roy and lyrics are by Indu Jain. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer (s)
|-
| Concert (Sarod)
| Amjad Ali Khan 
|-
|Geeton Ki Duniya Men Sargam  
| Sulakshana Pandit
|-
|Khali Pyala Dhundhla Darpan
| Sulakshana Pandit
|-
|Pyala Chhalka Ujla Darpan Sulakshana Pandit
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 