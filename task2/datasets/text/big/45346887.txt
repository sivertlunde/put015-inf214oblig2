Gone to Coney Island and Booming Business
 
 
 
{{Infobox film
| name           = Gone to Coney Island Booming Business
| image          = Gone to Coney Island.jpg
| caption        = A film still from Gone to Coney Island
| director       = 
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
| italic title   = no
}}
 silent short short comedy productions by the Thanhouser Company. Both were released together on a single film reel on July 5, 1910. Gone to Coney Island is a comedy that features Coney Island, which the mere subject would make for a successful film. Booming Business may have been the very type of slapstick comedy that Edwin Thanhouser specifically said the Thanhouser Company would not produce. The productions of both films have no credits for the cast or crew, but possible candidates for these roles exist. Reviews of the films favored Gone to Coney Island, but some reviewers specifically refused to explain the plot because Coney Island subjects were deemed self-explanatory. Booming Business received one detailed review in the The New York Dramatic Mirror which was negative. The films are presumed lost film|lost.

== Plots ==
Though the films are presumed  . Kate tries to tell her mistress of her intended departure, but Mrs. Greene is busy and refuses to listen. Mrs. Greene goes out, during her absence, Kate takes French leave, leaving a note saying she is going to Coney Island. Never having visited the Island, Mrs. Greene considers it a jungle place. When she gets Kates note, she thinks that her duty to immediately start in pursuit of the misguided girl. Not knowing how to get to the island, she appeals to her old friend, Professor Griggs. He also is ignorant of the ways of Coney, but in turn appeals to his friend Casey, a ward politician, who of course knows the Island, and consents to act as their escort. The three set out for Coney and start on their search for Kate. The hunt is almost forgotten in the joys of looping the loop, shooting the chutes and various other diversions of the Island. When they finally find Kate, she is at her post, selling tickets at Luna. She laughs at Mrs. Greenes fears for her safety and announces the fact that she is happily married, and presents the lucky man. In a cool Coney corner, Mrs. Greene confesses that she found Coney wasnt as bad as it was painted, and she is going to revisit it often."   

The second production, Booming Business, was also featured in the same issue of the publication with a short summary. It states: "Jack is an industrious young businessman, who has invested in a small stock of stationery, and set up shop. He unfortunately has no customers, and his various schemes to bring trade prove extremely unlucky but mighty laughable. What they are we had better let the little picture tell."   

== Production ==
The writer of the two scenarios is unknown, but it was most likely Lloyd Lonergan. Lonergan was an experienced newspaperman employed by The New York Evening World while writing scripts for the Thanhouser productions. He was the most important script writer for Thanhouser, averaging 200 scripts a year from 1910 to 1915.  The film director is unknown, but it may have been Barry ONeil. Bowers does not attribute a cameraman for this production, but two possible candidates exist. Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.    Members cast may have included the leading players of the Thanhouser productions, Anna Rosemond, Frank H. Crane and Violet Heming.   

The setting of Gone to Coney Island is Coney Island, a popular amusement park and picture subject.  One review in The Moving Picture World went so far as to give a review that was devoid of meaning by stating, "Not much further explanation is needed. Those who have been there and those who have heard of it know what it means. Perhaps nothing further is required."  Bowers notes that the popularity of Coney Island was itself able to make a successful film regardless of the scenario involved.  Booming Business is notable because it appears to have contained slapstick comedy if The New York Dramatic Mirror review was accurate.  Edwin Thanhouser specifically stated that the company would not produce slapstick comedy in an article in The Moving Picture World. 

== Release and reception ==
The two comedy productions were released on a single reel, approximately 1000 feet in total, on July 5, 1910.   A Thanhouser Filmography Analysis, provided by Thanhouser Company Film Preservation, lists each film as comprising half a reel without further detail.    The films were shown together in theaters, with known advertisements in Indiana,   Kansas,   Pennsylvania,  and North Carolina.   

Reviews for the Gone to Coney Island were positive with two positive, but altogether undetailed reviews in The Moving Picture World. A detailed review in the The Moving Picture News acknowledged that the film will have a lasting impact "as it is one of a class of pictures which can almost be shown as a repeater without hurting the reputation of the theatre."  The reviewer praised the photography and found the acting adequate, but the decision to include the marriage scene was deemed peculiar.  For Booming Business the The Moving Picture World gave it two sentences which said the film was funny beyond description, but the The New York Dramatic Mirror provides a more detailed and negative review of the film by stating, "This comic is not up to the usual Thanhouser standard. In fact, it is rather silly all through, the only laughs being brought about by blows, falls, and smashing furniture. The principal character comes down to the camera and indicates the things he intends doing all through the picture, robbing it of every element of reality. He has a small store and adopts various schemes to boom business, such as giving health treatment with a magnetic battery, teaching boxing, and so on. Everything turns out badly and ends in general confusion."  Bowers notes that The New York Dramatic Mirror was not an unbiased reviewer, for the publication was slanted to the favor the Licensed companies instead of the Independents.  Another window into the film is provided by an advertisement by the Amuzu in The Twin-City Daily Sentinel which states the grocery-man attempts to spur business in a variety of comical ways. 

== References ==
 

 
 
 
 
 
 
 
 