Number 111 (1938 film)
{{Infobox film
| name           = Number 111
| image          = 
| image_size     = 
| caption        = 
| director       = Steve Sekely
| producer       = Richárd Horovitz
| writer         = Jenő Heltai (novel)   István Mihály 
| narrator       = 
| starring       = Jenő Törzs Pál Jávor (actor)|Pál Jávor Mária Lázár   Marica Gervai
| music          = Dénes von Buday
| editing        = József Szilas
| cinematography = István Eiben
| studio         = Mûvész Film
| distributor    = 
| released       = 1 April 1938
| runtime        = 80 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian thriller Number 111, directed by Alexander Korda, which was itself an adaptation of a novel by Jenő Heltai.

==Cast==
* Jenő Törzs - Joe Selfridge 
* Pál Jávor (actor)|Pál Jávor - Baron Sandor Vajk 
* Mária Lázár - Vera Komarowska / Olga Komarowska 
* Marica Gervai - Mabel Arnett 
* Gyula Csortos - Sam Arnett 
* Gábor Rajnay - Baranyai 
* Andor Lendvay - Himself 
* Zoltán Makláry - Selfridges Aide 
* Ferenc Hoykó - Hotel Alkalmazott 

==External links==
* 

 

 
 
 
 
 
 