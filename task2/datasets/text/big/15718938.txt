Aegan
 
 
{{Infobox film
| name           = Aegan
| image          = Aegan Poster.jpg
| caption        = Theatrical release poster
| director       = Raju Sundaram
| producer       = K. Karunamoorthy C. Arunpandian
| based on       =  
| writer         = S. Ramakrishnan Raju Sundaram  (Dialogue) 
| screenplay     = Raju Sundaram  (Adepted Screenplay)  Farah Khan Abbas Tyrewala Rajesh Saathi  (Original Screenplay) 
| story          = Farah Khan  (Original Story)  Raju Sundaram  (Additional Story)  Suman
| music          = Yuvan Shankar Raja
| cinematography = Arjun Jena
| editing        = V. T. Vijayan
| studio         = Ayngaran International
| distributor    = Ayngaran International
| released       = 25 October 2008 
| runtime        = 120 minutes
| budget         =  160&nbsp;million
| box office         =  800&nbsp;million     
| country        = India Tamil

}} Tamil action-comedy film directed and co-written by Raju Sundaram and produced by Ayngaran International based on the 2004 Shahrukh Khan starrer Main Hoon Na. It stars Ajith Kumar, Nayantara, Suman (actor)|Suman, Jayaram, Navdeep and Piaa Bajpai.

The film opened to Indian audiences on 25 October 2008, coinciding with the Diwali season. The film, upon release received mixed reviews and was an average grosser at the box office.

== Plot ==
Aegans story is based around Shiva (Ajith Kumar), a CBI officer, and his efforts to foil the terrorist John Chinnappa (Suman (actor)|Suman). Major Shiva is simultaneously attempting to mend relations with his fathers estranged first wife and his half-brother Narain (Navdeep). Complications ensue.
 police approver and is on the verge of revealing Chinnappas ploys to the police. However, he is on the run to avoid charges. The case falls to Lieutenant Karthikeyan (Nassar), Major Shivas father, who had ordered his decommission, who assigns his son to go and help solve the mission.

Karthik tells Shiva to attend college in Tamil Nadu – St. Johns College, Ooty – to protect Ram Prasads daughter, Pooja (Piaa Bajpai), from meeting with her father. Shiva pleads that he has never seen a civilian college before and doesnt know much about the current generation. The General points out that, by a strange coincidence, Pooja attends school in the same town where Ram Prasad is hiding. Shiva can protect Pooja and look for Ram Prasad at the same time. Shiva goes undercover as a student returning to college after many years away taking care of the family business. He is much older than the other students, who at first make fun of him because of his lack of new-generation styles. He also has to deal with the many goofy teachers as well the idiotic forgetful principal of the college, Albert (Jayaram).

He finds Narain, who is later revealed to be Shivas brother. Pooja is Narains best friend and he is secretly jealous that other girls can get his attention but she cant. Shiva saves Pooja from John Chinnapas men, who kidnap her while Narain and others think its a prank. He later becomes a sensation among the students and teachers. He befriends Narain and Pooja and falls in love with the chemistry teacher, Mallika (Nayantara), after being attracted by her beauty and in a bid to convince other students that he is one of them. During his regular jogging sessions Shiva spots Ram Prasad and chases him till he escapes with the help of an helmet-clad bike rider who is later revealed to be Narain. Pooja comes to know that Narain has been helping her father and feels cheated by him.

Narain tries to apologise to Pooja many times, but she refuses to talk to him. Irked by this, Narain drinks and explains his actions when Johns men come again to kidnap Pooja. They beat up Narain, but Shiva comes to their rescue. Narain and Pooja patch up after the fight sequences. Shiva takes Narain to his home where he learns that Narain is his brother and meets his mother. The flashback sequences describe Shivas childhood (he is an orphan) and explain why his mother and father live separately.

The next day when Pooja introduces Ram Prasad to Shiva, Shiva arrests him – much to the disappointment of Pooja and Narain. Then everyone in the college including Mallika learns that Shiva is a policeman.

Shiva then drives Ram Prasad straight to John Chinnapas hiding place, avoiding the traps that he had set. A fight ensues between Shiva and John Chinnappas men in which John is finally killed.
In the final scenes Mallika is shown with Shivas parents and brother while he has gone to Afghanistan as part of another mission.

== Cast ==

* Ajith Kumar as Shiva.  
* Nayantara as Mallika. 
* Nassar as Karthikeyan.  
* Jayaram as Albert Aandiyapatham.   Suman as John Chinnappa. 
* Navdeep as Narain. 
* Piaa Bajpai as Pooja.   Devan as Ram Prasad. 
* Cochin Hanifa as Hanifa.

== Production ==

=== Development ===
In September 2007, early indications suggested that Ayngaran International were set to make a film starring Ajith Kumar directed by Venkat Prabhu,    however the chance went to first-time director Raju Sundaram, in a project titled Akbar.    Despite denying the title, Ajith confirmed the project and stating that he had put on weight and grew a beard for his participation in the project. Following the success of Billa (2007 film)|Billa, Ajith Kumar waited till the birth of his first child, in January 2008, before commencing his shoot for the film.

The 40-member unit consisting of Ajith, director Raju Sundaram, cinematographer Arjun Jena, action choreographer Stun Shiva and a few stunt men from Chennai flew to Hong Kong on 14 January 2008 and began their first schedule for a 10-day stint.   

The portions in Hong Kong were shot showing Ajiths introduction stunt scene set against the skyscrapers, Hong Kong airport, and the sea with the help of Chinese action choreographers. The crew shot schedules in Theni and Ooty and other locations in South India during the filming. The film finished its talkie portion in early September 2008, and the songs were canned in India as well as parts of Switzerland.          The film, initially described as a "musical action comedy,". The film previously referred to as Akbar, Ramakrishnan and Anthony Gonsalves  was christened as Aegan, a name of Lord Shiva.

=== Casting === America where she was then shooting for her Hollywood film, The Other End of the Line. However other indications claimed that Saran was ousted from the project due to her involvement in Indiralohathil Na Azhagappan, in which she appeared in an item number, which Ajith Kumar was unhappy with due to previous feuds with the lead, Vadivelu of that film.    Reports indicated that Parvati Melton had replaced Saran but later denied it signalling that she was not approached. 

Suhani Kalita was announced as the second heroine of the project but was later removed for unknown reasons.    Several Bollywood heroines including Deepika Padukone,    Bipasha Basu,    Katrina Kaif, Ayesha Takia,    Ileana DCruz,    Tanushree Dutta     and Sneha Ullal  were linked to the role, as well as reports that Shriya Saran would reprise the role after making up with Ajith.  The role eventually was given to Katrina Kaif, who was set to make her debut in Tamil films with Aegan,    however she later opted out due to callsheet clashes.    It was announced that Nayantara would play the role of the heroine in the film, following her role opposite Ajith in Billa (2007 film)|Billa.    In February 2008, it was confirmed that Navdeep would play a role in the film as well as Suman (actor)|Suman, who will appear in a villainous role. Malayalam actor, Jayaram will also play a role in the film, which will be co-produced by actor, Arun Pandian. 

Nassar and Suhasini were selected to play the roles of the parents of Ajith Kumar|Ajith,(the childhood role played by master yash lodha)    whilst Raju Sundarams elder brother, Prabhu Deva was given a chance to make a cameo appearance in the film as a dancer but politely refused.    Supporting actors Livingston (actor)|Livingston, Sathyan and Sriman also play roles in Aegan, as does model Piaa Bajpai, who appears as the ladylove to Navdeep.       Yuvan Shankar Raja was signed on as the music composer whilst Arjun Jena was the cinematographer for the project  and Milan, who did the art direction for Billa (2007 film)|Billa, renews his association with Ajith in the film.   

== Reception == climax as "a colossal embarrassment", but adding that "no film is perfect".  

==Character map of remakes==
{| class="wikitable"
|-
!  Aegan (2008 film) Tamil language|Tamil!! Main Hoon Naa (2004 film)Hindi 
|-
| Ajith Kumar || Shah Rukh Khan
|-
| Nayantara || Sushmita Sen
|- Suman || Sunil Shetty
|-
|}

== Soundtrack ==
{{Infobox album
| Name        = Aegan
|  Type        = soundtrack
|  Artist      = Yuvan Shankar Raja
|  Cover       = Aegan (soundtrack to 2008 film - cover art).JPG
|  Released    = 9 October 2008
|  Recorded    = 2008 Feature film soundtrack
|  Length      = 29:08
|  Label       = Ayngaran Music An Ak Audio
|  Producer    = Yuvan Shankar Raja
|  Reviews     =
|  Last album  = Saroja (film)#Soundtrack|Saroja (2008)
|  This album  = Aegan (2008)
|  Next album  = Silambattam (film)|Silambattam (2008)
}}

The soundtrack of Aegan was composed by Yuvan Shankar Raja as was the film score. It was noticeably, the third time Yuvan Shankar was scoring music for an Ajith Kumar film, after Dheena (2001) and Billa (2007 film)|Billa (2007), the songs of which had been very popular and went on to become chartbusters. 

The soundtrack released on 9 October 2008    at a private ceremony at the HelloFM Studios by composer Yuvan Shankar Raja as the lead cast and the director were away shooting the video for the song in Switzerland.  The album features 6 tracks overall, including one of the songs ("Hey Salaa") repeated at the end.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration (min:sec) !! Notes
|-
| 1 || "Hey Salaa" || Blaaze, Naresh Iyer, Mohammed Aslam || 5:09 ||
|- Ranjith & Naveen || 4:36 ||
|-
| 3 || "Hey Baby" || Shankar Mahadevan || 4:39 ||
|- Kay Kay, Bela Shende || 4:31 ||
|-
| 5 || "Kichu Kichu" || Vasundhara Das, Yuvan Shankar Raja || 5:11 ||
|-
| 6 || "Hey Salaa 2" || Blaaze, Naresh Iyer, Mohammed Aslam || 5:02 || Provided as background music.
|}

==Release==
The satellite rights of the film were sold to Kalaignar TV|Kalaignar. The film was give a "U" certificate by the Indian Censor Board.

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 