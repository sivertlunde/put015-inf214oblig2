The Metro (film)
{{Infobox film
| name           = The Metro
| image          = TheMetro_film.jpg
| image_size = 300px
| director       = Bipin Prabhakar Dileep
| writer         = Vyasan Edavanakkadu Suresh Krishna Bhavana
| music          = Shaan Rahman
                 Rajeev Alunkal(lyrics)
| cinematography = Sreeram
| editing        = Mahesh Narayanan
| distributor    = 
| studio         = Graand Production
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =
}} Suresh Krishna, Dileep under his Grand Productions. 

==Plot== Suresh Krishna). The investigation is affected due to his political influences.

The next story is about Anupama (Bhavana Menon|Bhavana), an I. T. Employee. She is being followed by a group of gangsters. The story takes new turns and the mystery gets solved.

The third story is about a group of five friends, Harikrishnan (Nivin Pauly), Usman (Bhagath Manuel), Sujath (Suraj Venjaramoodu), Sooraj (Arun) and Govind (Biyon Gemini). Harikrishnan is a youth working in Gulf. He and his friends are travelling to Palai. He has a package from Gulf which he is supposed to give to another person. The story takes new turns during their journey.

==Cast==
* Sharath Kumar as CI Jacob Alexander
* Nivin Pauly as Harikrishnan
* Bhagath Manuel as Usman
* Suraj Venjaramoodu as Sujathan
* Biyon  as Govind
* Arun as Sooraj Suresh Krishna as Parathikkadan Shaji Bhavana as Anupama
* G. K. Pillai (actor)|G. K. Pillai as Kumbalam Varkey (MP)
* Nishanth Sagar as Freddy
* Kalashala Babu as Viswan/Viswettan
* Jagathy Sreekumar as Achayan
* Sadiq
* Kalabhavan Shajon as A worker of Kattappuram Automobiles
* Rajeev Parameshwar as a politician who was killed by Shaji
* Mahima
* Baby Ester as Sujathans daughter
* Deepika Mohan as Harikrishnans mother

==Production== One Way Ticket and Khaki.  The films cast includes several new faces along with Sharath Kumar, who does his first lead role in a Malayalam film.   Nivin Pauly and Bhagath Manuel, who debuted through Malarvaadi Arts Club once again combines in this film. Sreeram handles the camera while Mahesh Narayanan is the editor. Major parts were filmed from Kochi. 

==References==
 

 
 
 
 
 
 
 