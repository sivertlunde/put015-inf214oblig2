Love and Troubles
{{Infobox film
| name           = Love and Troubles
| image          = Love and Troubles.jpg
| caption        = Film poster
| director       = Angelo Dorigo
| producer       = Angelo Dorigo
| writer         = Lianella Carell Nino Lillo Amedeo Marrosu Roberto Natale Giorgio Stegani
| starring       = Marcello Mastroianni
| music          = Carlo Innocenzi
| cinematography = Giuseppe Aquari
| editing        = Mariano Arditi
| distributor    =
| released       =  
| runtime        = 87 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Love and Troubles ( ) is a 1958 Italian comedy film directed by Angelo Dorigo.   

==Cast==
* Marcello Mastroianni - Franco
* Richard Basehart - Paolo Martelli
* Valentina Cortese - Marisa
* Maurizio Arena - Roberto
* Eloisa Cianni - Teresa
* Irène Galter - Lisa
* Umberto Spadaro - Antonio
* Checco Durante - Virgilio Santucci
* Nino Musco - Padre di Teresa
* Silvio Bagolini - Collega di Franco
* Andrea Aureli - Ivo
* Luigi Tosi - Capocantiere
* Liana Ferri - Madre di Teresa e Ivo
* Emma Baron - Signora Renata (as Emma Baron Cerlesi)
* Mario Passante - Un viaggiatore

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 