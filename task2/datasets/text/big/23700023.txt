Moonlight Masquerade
{{Infobox film
| name           = Moonlight Masquerade
| image          =
| image_size     =
| caption        =
| director       = John H. Auer
| producer       =
| writer         = John H. Auer (story)  Lawrence Kimble (screenplay)
| narrator       =
| starring       = Dennis OKeefe  Jane Frazee
| music          = Harry Revel (Music)  Mort Greene (Lyrics)
| cinematography =
| editor         =
| distributor    =
| studio         = Republic Pictures
| released       = 1942
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
}}
 1942 United American film starring Dennis OKeefe and Jane Frazee. It is also known as Moonstruck and Tahiti Honey.

==Plot Summary==

Two business partners, John Bennett, Sr. and Robert Forrester, are starting to get nervous when the birthday of Victoria, Forresters daughter, approaches. A long time ago the two men made an agrrement that they would sign over one third of their company to their oldest children when they turned twenty-one, with the condition they married each other within thirty days. The family who wouldnt agree to the marriage would lose its entire company share to the other family.

Because of this Bennett tries to make his son John marry Victoria, a girl he has never met. John says he can save them by asking Vicki to marry him, because he is certain she will refuse him, since she is known as a snob who aims for more upscale targets.

But Vicki and Forrester are scheming to make John an offer he wants to refuse. Vicki hires an exotic dancer, Mikki Marquette, to play the role of her and lure John into a trap. Since the wedding is to take place in Havana according to the contract, Vicki, Mikki and John all board a train to Miami, where a ship will take them to Cuba. When John meets Mikki, he buys her role as Vicki, but he also pretends to be someone else, and actually falks in love with the real Vicki, who travels as "Vickis" companion. John talks aboy all the bad things he has heard about Vicki with the real Vicki, and she starts to dislike him.

In Miami, John meets a playboy fortune seeker who calls himself Lord Percy Ticklederry, and sends him in Mikkis way. Percy likes Mikki, but John still offers to pay him to court her. Jihn also reveals his true identity to the real Vicki, and she is quite disappointed, since she is falling in love with him too. She decides to play along to see what happens and put John to he test.

In Havana, Percy decides to confess his real identity to Mikki, and tells her he is no English lord, just an ordinary guy from Brooklyn. To win this game of charades, Vicki tells Mikki to keep up appearances until John eventually proposes to her. John truly believes Mikki will turn him down since she is in love with Percy. And he wants to propose to Vicki, whom he loves.

To Johns surprise and dismay, Mikki accepts his proposal, and John has to forfeit by giving up hs interest in the firm, all because he wants to marry Vicki instead. Unknown to him and Vicki, their fathers have reconciled and changed the marriage-clause. John hearsabout this, and also learns about the girls true identities. He is also informed that Vicki in reality is already engaged to a Count Eric Nordvig, and guves her up for good.

Percy and Mikki marry each other and spend their honeymoon in Miami. Vicki is upset that Jihn gave her up so easily and joins Eric, who is also in Miami. But Vicki soon learns that Eric wanted her only for her money, so she breaks up their engagement. John hears bout this and they reunite on the dance floor of a nightclub. 

==Cast==
*Dennis OKeefe as John Bennett Jr.
*Jane Frazee as Vicki Forrester
*Betty Kean as Mikki Marquette
*Eddie Foy Jr. as Lord Percy Ticklederry   Paul Harvey as John Bennett Sr.
*Ernő Verebes as Count Eric Nordvig
*The Chocolatiers as Black tap dancing group

==External links==
*  

==References==
 

 

 
 
 
 
 
 
 