The Pleasure Garden (1961 film)
 
{{Infobox film
| name           = The Pleasure Garden
| image          = The Pleasure Garden (1961 film).jpg
| caption        = 
| director       = Alf Kjellin
| producer       = 
| writer         = Ingmar Bergman Erland Josephson
| starring       = Sickan Carlsson Gunnar Björnstrand Bibi Andersson
| music          = 
| cinematography = Gunnar Fischer
| editing        = Ulla Ryghe
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

The Pleasure Garden ( ), is a 1961 Swedish film directed by Alf Kjellin and written by Ingmar Bergman.

==Cast==
* Sickan Carlsson – Fanny, waitress
* Gunnar Björnstrand – David Samuel Franzén 
* Bibi Andersson – Anna, Fannys daughter
* Per Myrberg – Emil 
* Kristina Adolphson – Astrid Skog, book store proprietor 
* Stig Järrel – Ludvig Lundberg 
* Hjördis Petterson – Ellen, Davids sister 
* Gösta Cederlund – Liljedahl 
* Torsten Winge – Wibom 

==External links==
* 

 

 
 
 
 
 
 
 