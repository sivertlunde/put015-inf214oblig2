The Hatchet Man
{{Infobox film
| name           = The Hatchet Man
| image          = Poster of the movie The Hatchet Man.jpg
| image_size     = 200
| caption        = Original film poster
| director       = William A. Wellman
| producer       =
| writer         = J. Grubb Alexander Achmed Abdullah (play) David Belasco (play)
| starring       = Edward G. Robinson Loretta Young
| music          = Bernhard Kaun
| cinematography = Sidney Hickox
| editing        = Owen Marks
| distributor    = Warner Bros.
| released       =  
| runtime        = 74 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Tong gang wars. Made during the few years before strict enforcement of the Production Code, The Hatchet Man has elements that would not be allowed later, such as adultery, narcotics, and a somewhat graphic use of a flying hatchet.

==Plot summary==
Wong Low Get (Edward G. Robinson) is the most highly respected hatchet man of his Tong (organization)|Tong. Having sworn total allegiance, he cannot turn down an order, even one to kill his best friend Sun Yat Ming (J. Carrol Naish). His friend forgives him in advance of his execution, begging only that Wong raise his daughter Toya San (Loretta Young) as his own. Wong does as he has sworn, but as she grows up, he falls in love with her. She marries him out of a sense of obligation, but a handsome younger gangster, Harry En Hai (Leslie Fenton), gets her to leave Wong, disgracing him and leading to a shocking turn of events.

==Cast (in credits order)==
*Edward G. Robinson as Wong Low Get
*Loretta Young as Sun Toya San Dudley Digges as Nog Hong Fah
*Leslie Fenton as Harry En Hai
*Edmund Breese as Yu Chang
*Tully Marshall as Long Sen Yat
*J. Carrol Naish as Sun Yat Ming Charles Middleton as Lip Hop Fat
*E. Alyn Warren as Soo Lat, The Cobbler
*Edward Peil, Sr. as Bing Foo

As was typical of the time, almost no Asian actors appear in the cast of a film set completely among Chinese characters. Makeup artists had noticed that audiences were more likely to reject Western actors in Asian disguise if the faces of actual Asians were in near proximity. Rather than cast the film with all Asian actors, which would have then meant no star names to attract American audiences, studios simply eliminated most of the Asian actors from the cast. That said, this film contains a number of Asian actors in minor roles, including James B. Leung, Toshia Mori, Willie Fung, Otto Yamaoka and Anna Chang. 

==See also==
*List of American films of 1932
*Portrayal of East Asians in Hollywood

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 