The Fall Guy (1930 film)
 
{{Infobox film
| name           = The Fall Guy
| image          = TheFallGuyTitleScreen.jpg
| alt            = 
| caption        = Title Screen from film
| film name      = 
| director       = Leslie Pearce   
| producer       = William LeBaron William Sistrom (assoc.) 
| writer         = 
| screenplay     = Tim Whelan 
| story          = 
| based on       =      Pat OMalley
| narrator       = 
| music          = 
| cinematography = Leo Tover  
| editing        = Archie Marshek  RKO Radio Pictures 
| distributor    = 
| released       =   }}
| runtime        = 66 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 crime drama Pat OMalley, and its supporting cast included Mae Clarke, who would become famous the following year when James Cagney pushed a grapefruit into her face in the film, The Public Enemy.   

==Plot==
When Johnny Quinlan loses his job in a drug store, he is afraid to tell his wife, Bertha, and therefore keeps up the pretense of leaving each morning for a non-existent job, as he begins the search for a new job. As the days pass and he is unable to find employment, their household, which includes his sister, Lottie, and Berthas brother, Dan Walsh, goes through what little savings they have.

As he gets more desperate, he agrees to do small jobs for "Nifty" Herman, a small time gangster.  Nifty had loaned Johnny $15, as part of a plan to entice him to work for him.  After Johnny gets insulted by a common laborer job offer from a neighbor, Nifty lies to him and says that he has a friend who will get him a managerial position at a liquor store.  All Johnny has to do is hold onto a case of high-priced alcohol for a few days.  Dubious, Johnny reluctantly agrees and takes the suitcase back to his apartment.  However, when Bertha finds out who he got the suitcase from, she demands that he return it, threatening to leave him if he doesnt.

Taking the case back to Nifty, he finds the office locked, and so returns home.  When he arrives, his sisters suitor, Charles Newton, is visiting.  Newton is a government agent.  Even though Johnny tries to hide the case, his efforts are futile, and Newton spies it and becomes suspicious, seeing a resemblance to a case he and his men have been attempting to track down.  Opening it, he discovers it contains a cache of drugs.  When he interrogates Johnny, he gets the whole story, and is about to arrest Johnny, when Nifty arrives to retrieve the suitcase.  Johnny tricks Nifty into confessing, and then subdues him, when he is resisting the efforts of Newton and his deputies to arrest him.  The film ends with Johnny being rewarded for the way he handled himself by becoming Newtons assistant.

==Cast==
* Jack Mulhall as Johnny Quinlan
* Mae Clarke as Bertha Quinlan
* Ned Sparks as Dan Walsh
* Wynne Gibson as Lottie Quinlan Pat OMalley as Charles Newton
* Thomas E. Jackson as Frederick "Nifty" Herman Tom Kennedy as Detective Burke
* Alan Roscoe as Detective Keefe

(cast list as per AFI database) 

==Reception==
The New York Times film critic, Mordaunt Hall, gave the film a mostly favorable review, saying that the film was "a little crude in spots, it at least succeeds in holding the attention, the action being fairly good combination of comedy and drama."   

==Notes==
The Broadway play upon which this film is based, The Fall Guy, a Comedy in Three Acts, was performed at the Eltinge 42nd Street Theatre in 1925, starring Ernest Truex. 

==References==
 

 
 
 
 
 
 
 
 
 
 