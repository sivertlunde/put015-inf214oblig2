Rosies of the North
{{Infobox film
| name           =Rosies of the North
| image          =Rosies of the North poster.jpg
| image size     =150px
| caption        =Theatrical poster
| director       = Kelly Saxberg
| producer       = Joe MacDonald
| writer         = Bob Lower
| narrator       =Martine Friesen  
| starring       =
| music          =Greg Lowe
| cinematography =Ian Elkin
| editing        =Kelly Saxberg
| studio         = National Film Board of Canada 
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 46 minutes, 40 seconds
| country        = Canada
| language       = English
| budget         =
| gross          =
}}
 Second World War.  It also is the story of female engineer Elsie MacGill, who became known as the "Queen of the Hurricanes". Bourgeois-Doyle 2008, p. 157.  The title of the film is an allusion to the wartime iconic image of Rosie the Riveter. 

==Plot==
 
 
 
In 1939, Canada joined the worldwide war effort with factories turning out war machines. At the Canadian Car and Foundry (nicknamed "Can-Car") in Fort William, Ontario, a large workforce was recruited to build the Hawker Hurricane fighter aircraft, including a preponderance of women. Many of them were young, and came from as far away as the Canadian Prairies|Prairies.

Of the 7,000 workers at Can-Car, 3,000 were women on the shop floor; initially, they were not treated with respect. Men typically were the "lead-hands", more experienced workers who would train, at least at first, the new women trainees. It was soon evident that women reacted much more favourably to other women as their mentors and instructors.

For a while, the animosity between the men and women was palpable. One "old hand" who showed his dislike by refusing to let women sign out before him, ensured the first seats on the buses leaving the factory were then occupied by the men. They steadfastly refused to give up their seats, leaving the women "hanging on by the straps". The obstinate fellow came on board after women welded his lunchbox to a piece of steel.

Eventually, the factory assigned female "matrons" to look after the women, acting as nurses, nannies and "cops". The factory called them "intelligent, likeable, friendly women," while the women knew that the matrons were there to "keep them in line," and not "tempt the men". Women still made less than the men whom they had often trained, and married women were summarily dismissed, but some of them kept their marriages secret and worked anyway.

The one woman "boss" at the factory was Elsie MacGill, who was first in charge of the production of fighter aircraft, and heralded in the press as the "Queen of the Hurricanes". Wakewich 2006, p. 397.   By the time the production line shut down in 1943, Can-Car had produced over 1,400 Hurricanes.  Just as the factory was re-tooling for production of Curtiss SB2C Helldiver torpedo and dive bomber aircraft for the United States Navy, MacGill and the works manager, E. J. (Bill) Soulsby, were dismissed. It was later revealed they were having an affair (and subsequently married). 

The women on the shop floor continued on until the end of the war, when most of them relinquished their jobs as the men came home. At the refurbished Can-Car plant, now building rolling stock and buses, 60 years later, the women came back to the factory to remember when they "had the greatest experience of their lives". For the reunion, a highlight was the arrival of a restored Curtiss Helldiver which did a flypast with one of the original women factory workers, Margaret Cook (née Nixon) on board. Saxberg, Kelly, Director.    National Film Board of Canada, 1999 via IMDb. Retrieved: October 3, 2014.  

==Production==
Director Kelly Saxberg was able to access archival footage from the NFB wartime archives that showed not only the production effort of aviation companies, but also newsreels such as To The Ladies (1946) that dealt with the issues of women and men in the workforce. With the return of men from the war, women came under intense pressure to return to running households.  Rosies of the North also had the benefit of gathering first-hand interviews at a reunion of Can-Car workers that coincided with the "Thunder in the Air Air Show" held on August 29, 1998, at Thunder Bay, Ontario.

==Reception==
Rosies of the North quickly found favour with viewers and has been broadcast on at least eight networks in North America.  Librarian Joan Payzant, in her review for CM magazine, observed, "Highly recommended viewing, illustrating an important side of life in Canada during the Second World War." She also indicated that seniors "... will appreciate the nostalgia in the film." 

In a detailed analysis of Rosies of the North, Professor David Frank, noted: "The film treatment cuts back and forth between present and past as the women review the evidence of their experience and share personal observations. As such it seems very much an exercise in a visual form of oral history." He agreed that the film left it up to the viewer to decide whether the gains made in the industrial workplace were fleeting. One of the most poignant still photographs shown in the film is a postwar image, as the camera closes up on a sign that the formerly employed female workers hold up. It says: "We want work". 

==See also==
* The Life and Times of Rosie the Riveter, a 1980 American documentary film

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Bourgeois-Doyle, Richard I.   Ottawa: NRC Research Press, 2008. ISBN 978-0-660-19813-2.
* Wakewich, Patricia. " Queen of the Hurricanes: Elsie Gregory MacGill, aeronautical engineer and womens advocate." in Cook, Sharon Anne, Lorna R. McLean and  Kate ORourke, eds. Framing Our Past: Canadian Womens History in the Twentieth Century. Montreal: McGill-Queens University Press, 2006, First edition 2001. ISBN 978-0-7735-3159-8.
 

==External links==
*  
*  
*  .

 
 
 
 
 
 
 
 
 
 
 
 
 