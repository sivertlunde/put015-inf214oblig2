Is It Fall Yet?
{{Infobox film
| image = Daria - Is It Fall Yet?.jpg
| image size =
| name = Is It Fall Yet?
| director = Karen Disher Guy Moore
| writer = Glenn Eichler Peggy Nicoll Marc Thompson Alvaro J. Gonzalez Carson Daly Bif Naked Dave Grohl
| distributor = MTV and The N
| runtime = 75 minutes
| released =  
| country = United States
| language = English
}}
Is It Fall Yet? is the first of two movie-length installments in MTVs animated series Daria. {{cite news|title= SPOTLIGHT; Daria: Smart, Alienated and ... Dating?
|work= New York Times|date=27 August 2000|url= http://www.nytimes.com/2000/08/27/tv/spotlight-daria-smart-alienated-and-dating.html?scp=2&sq=Glenn%20Eichler&st=cse|accessdate=11 August 2010 | first=Anita | last=Gates}}  

The two telemovies, Is It Fall Yet? and Is It College Yet?, chronicled, respectively, the summer break between the school years of seasons four and five; the second film was the shows finale.

==Plot== Helen forces Timothy ONeills summer camp for pre-pubescent children. While at the camp, Daria meets a nihilistic young boy named Link, who feels disillusioned with his existence and voices his bad attitude every chance he gets. Recognizing herself in Link, Daria attempts to reach out to him. However, he rejects her overtures, which results in Daria feeling worse. Paralleling these emotions is her relationship with Tom, which she effectively ends for the discomfort it brings.

Daria comes to visit Jane, and, due to some meddling from Janes brother Trent Lane|Trent, the two reconcile. After returning home, Daria tells Jane that she was always impressed by Janes strong sense of identity, which resolves Janes identity crisis over her sexual orientation. Daria receives a letter from Link that invites her to email him, thus assuaging her fears that she is incapable of reaching out to or connecting with another human being. Jane affirms that she is no longer upset by Daria and Tom dating and encourages Daria to get back together with him.

==Cast==
*Tracy Grandstaff as Daria Morgendorffer Jane Lane
*Russell Hankin as Tom Sloane

==References==
 

==External links==
*  

 

 
 