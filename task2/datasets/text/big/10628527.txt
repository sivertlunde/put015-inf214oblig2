Once in a Lifetime (2000 film)
{{Infobox Film 
 | name = Livet är en schlager
 | image = Livet är en schlager.jpg
 | caption = 
 | director = Susanne Bier
 | writer = Jonas Gardell
 | starring = Helena Bergström, Jonas Karlsson, Björn Kjellman 
 | producer = Thomas Heinesen 
 | distributor = 
 | music = 
 | budget =
 | released = 10 November 2000 
 | runtime = 106 minutes
 | country = Sweden Swedish
 | }}
 TV2 Denmark. 

==Plot== Lena PH and Carola Häggkvist|Carola. Her partner Bosse is unemployed, and she is left to feed the family. Her brother, Candy, is a transvestite and an AIDS-sufferer who designs clothes. David, Monas employer, a cerebral palsy-sufferer, is a proficient songwriter however, and composes a song which Mona steals and sends a demo of (with her own lyrics) into Melodifestivalen, the Swedish heats for the Eurovision contest. Much to Monas delight, the song qualifies for the finals. Monas new-found fame takes her to unexpected places, including a TV interview, and an invitation to lunch at Berns Salonger|Berns. Throughout, the film, though, Mona is torn as to whether she should reveal that she was not in fact the only writer of her song, and thus risk losing the publics support.

==Music== I Believe in Miracles" performed by Lena Philipsson and "Kärleksikonen" performed by Regina Lund. 

The film also features the song "Aldrig ska jag sluta älska dig", originally performed by Helena Bergström, which was later released by Gardell,  and "Handen på hjärtat" performed by Sofia Källgren. "Handen på hjärtat" was released on the soundtrack but was performed by Björn Kjellman feat. Salome.

==Cast and characters==
*Helena Bergström as Mona
*Jonas Karlsson as David
*Björn Kjellman as Candy Darling
*Thomas Hanzon as Bosse
*Sissela Kyle as Woman at the employment office
*Katarina Ewerlöf as Moa, project manager
*Regina Lund as Sabina
*Douglas Johansson as Hairdresser
*Jessica Zandén as Woman at the café
*Frida Hallgren as TV engineer

==References==
 

==External links==
   
* 

 

 

 
 
 
 
 
 