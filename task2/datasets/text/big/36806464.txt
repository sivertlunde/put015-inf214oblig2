Rahgeer
{{Infobox film
| name           =Rahgeer
| image          = 
| image_size     = 
| caption        = 
| director       = A. Rashid  
| producer       = Rasik Productions
| writer         =
| narrator       =  Yakub Zohra Sehgal 
| music          = Khan Mastana
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1943
| runtime        = 120 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Rahgeer  is a Bollywood film. It was released in Bollywood films of 1943|1943.   It was directed by A. Rashid for Rasik Productions.    The film starred Trilok Kapoor, Shantarin, Masood, Yakub (actor)|Yakub, Zohra Sehgal and Shahzadi. The music director was Khan Mastana and the lyricist was Shewan Rizvi.    

==Cast==
*Trilok Kapoor
*Shantarin, Yakub
*Zohra Sehgal  
*Shahzadi 
*Anwaribai 
*Habib

==Soundtrack==
The music for the film was composed by Khan Mastana and lyrics written by Shewan Rizwi. The songs were sung by Khan Mastana, Zohrabai Ambalewali, Amirbai Karnataki, Faizbai and Ayaz.   

===Songs===
*"Rahgeer Sambhal Kar Chalna" sung by Khan Mastana
*"Kahan Se Aaya Kahan Hai Jaana" 
*"Jaa Bhanwre Jaa"
*"Aaja Saaki Aaja" by Khan Mastana
*"Ae Zindagi Hansa De" by Zohrabai, Ayaz
*"Dekho Badal Na Jaana"
*"Ameeri Lee Toh Aisi" by Faizbai, Khan Mastana
*"Apni Toh Zamane Se" by Amirbai Karnataki
*"Thodi Si Khushi Humne Paayi Thi"
  
==References==
 

==External links==
*  

 

 
 
 