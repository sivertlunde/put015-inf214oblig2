Abhi (2003 film)
 
 
{{Infobox film
| name           = Abhi
| image          = Abhiramya.jpg
| caption        =
| director       = Dinesh Babu
| writer         = Dinesh Babu
| producer       = Parvathamma Rajkumar
| starring       = Puneeth Rajkumar Ramya
| music          = Gurukiran
| cinematography = Dinesh Babu
| editing        = S. Manohar
| studio         = Poornima Enterprises
| distributor    = 
| released       = 25 April 2003
| runtime        = 140 minutes Kannada
| country        = India
| website        =
}}
 romance Comedy comedy drama Rajkumar sang a soundtrack for the film composed by Gurukiran.  The film was remade in Telugu as Abhimanyu (2003 film)|Abhimanyu.

==Plot==
Abhi (Puneeth Rajkumar) is a cool guy who is good at cultural activities. Bhanus (Ramya) family approaches Abhis Family in search of a rental house. In the beginning,there is hatred between Abhi and Bhanu,but then they start to know each other. As Bhanu is a Muslim and Abhi  is a Hindu, Bhanus father does not agree for this and he eventually comes in search of Bhanus lover to thrash him. Abhi does not agree with her fathers behavior. In college they start to fight with each other. Bhanu comes to stop them and tells that hes her father. Bhanus Father vacates the house and they all leave for Hubli. Abhi with his friends goes in search of Bhanu in Hubli. Somehow Abhi manages to talk with bhanu as their family does not know that he was the lover. Abhi and Bhanu hug in front of their family, then they will come to know that those two were lovers. The old lady in bhanus house advises Abhi to leave the house immediately as the men from that house will be coming from mosque after the prayer. But Abhi asks why should he be scared of them, the old lady tells him that they will try to kill you. Abhi asks why and the old lady says that ask your mother. Abhi goes to Bangalore and ask his mother about that matter what that old lady told. Abhis mother tells him a flash back story about Abhis Real Parents and their Assassination. She will also reveal that she was  his real mother. Bhanu also will hear the same story from her grandma. After some days bhanu  decides to give up her love for her familys sake and she leaves Hubli and goes to Bangalore to tell this to Abhi.  Abhi also agrees with her. But Bhanus father thinks that she ran away to marry Abhi and he tries to kill Abhi. Then there will be fight between Abhi and Bhanus father. The old lady advices Bhanus father to stop his cruelty. He  realizes his mistake and allows Bhanu to marry  Abhi.

==Cast==
* Puneeth Rajkumar as Abhi
* Ramya as Bhanu
* Sowcar Janaki Sumithra
* Raju Ananthaswamy
* Umashree
* Sathyajith

==Production==
* The film is produced by Parvathamma Rajkumar for Poornima Enterprises banner.

==SoundTrack==

{{Infobox album  
| Name        = Abhi
| Type        = Soundtrack
| Artist      = Gurukiran
| Cover       =
| Released    =  June 2007
| Recorded    = Feature film soundtrack
| Length      =
| Label       =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Lyrics
|-
| 1 || Mama Mama Maja Madu || Puneeth Rajkumar || Hamsalekha
|-
| 2 || Sum Sumne || Udit Narayan, Sowmya Raoh || Bangiranga
|-
| 3 || Ee Nanna Kannane || Udit Narayan, Mahalakshmi Iyer || K. Kalyan
|- Rajkumar ||  K. Kalyan
|-
| 5 || Bittaku Guru || Shankar Mahadevan, Chetan Sosca || V. Nagendra Prasad
|-
|}

==Box office==
* The film successfully ran for 150 days. It gross a box office of 16 crores.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 