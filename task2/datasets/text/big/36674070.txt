Siren of Atlantis
{{Infobox film name            = Siren of Atlantis image           = director        = Gregg G. Tallas producer        = Seymour Nebenzal writer          = Rowland Leigh Robert Lax Thomas Job based on Pierre Benoit starring        = Maria Montez Jean-Pierre Aumont Dennis OKeefe music           = Michael Michelette cinematography  = Karl Struss editing         = Gregg Tallas studio = Atlantis Productions distributor     = United Artists released        =   runtime         = 76 minutes language        = English budget          = $1,800,000 (est.) Maria Montez Tells Court About Astrology
Los Angeles Times (1923-Current File)   22 Apr 1950: A1.   THE OSCAR DERBY: Annual Scramble for Academy Awards Is On -- Role for Mitchum -- Other Items
By THOMAS F. BRADYHOLLYWOOD.. New York Times (1923-Current file)   02 Jan 1949: X5.   gross           = $335,000 
}}
Siren of Altantis is a 1949 American adventure film starring Maria Montez and her husband Jean Pierre Aumont. It was the first movie she made after leaving Universal Pictures.   

It was also known as Atlantis the Lost Continent.

==Plot==
Andre St Avit of the French Foreign Legion is discovered unconscious in the African desert. He claims he stumbled upon the lost kingdom of Atlantis, ruled by the beautiful Queen Antinea, who drove him to commit murder.

==Cast==
*Maria Montez as Queen Antinea
*Jean Pierre Aumont as Andre St. Avit
*Dennis OKeefe as Jean Morhange
*Morris Carnovsky as Le Mesge
*Henry Daniell as Blades
==Production== Pierre Benoit which had been previously filmed in 1921 and 1932. The latter version had been directed by G.W. Pabst and produced by Seymour Nebenzal in Berlin with German and French dialogue. SIFTING THE HOLLYWOOD NEWS: Peace Comes to United Artists as Selznick Bows Out--Third Remake of LAtlantide Coming Up--Soviets Ignore Metro
By THOMAS F. BRADY. New York Times (1923-Current file)   02 Mar 1947: 69.  

In September 1946 it was announced Nebenzal bought the rights to film the novel and had signed Maria Montez to star. The movie would be distributed through United Artists. Of Local Origin
New York Times (1923-Current file)   13 Sep 1946: 5.   

Jay Dratler was originally signed to write the script. AYRES TO APPEAR IN WARNER MOVIE: Signed for Role With Sheridan and Scott in Unfaithful, a Drama of Veterans Of Local Origin MUSIC NOTES Goldina Players to Do Shows
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   13 Nov 1946: 41.   A number of other writers also worked on it, including an uncredited   but said he was "fairly sure I didnt do any shooting" on the film. 

It proved difficult to come up with a screen play that satisfied the censors. In the novel, the queen had an insatiable appetite for male lovers and turns them into statues when she has finished with them. The Joseph Breen office wrote to Nebenzal complaining about the depiction of "hasheesh and illicit sex". Adjustments to the script were made. 

Montezs husband Jean Pierre Aumont was borrowed from MGM to appear opposite his wife. DE HAVILLAND QUITS ROLE IN IVY FILM: Actress Decides Against Part in Objection to Scenario-- Replacement Is Sought
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   29 Nov 1946: 43.   Dennis OKeefe was then signed to support them. 

Filming was to start in December 1946 but was postponed because Montez needed to have an operation and was required for another film for Universal. Hedda Hooper: LOOKING AT HOLLYWOOD
Los Angeles Times (1923-Current File)   19 Nov 1946: A3.  

The movie started filming on 17 February 1947 at a cost of $1.3 million under the direction of Arthur Ripley.  Lionel Banks, who had worked on Lost Horizon, did the sets. DRAMA AND FILM: Ford, Holden and Drew Latest Stellar Trio
Schallert, Edwin. Los Angeles Times (1923-Current File)   17 Feb 1947: A2.   It was shot at Samuel Goldwyn Studios. HOLLYWOOD SURVEY: Sharp Drop in Production Noted -- Still Another Dumas Exploit -- Other Items
By THOMAS F. BBADY. New York Times (1923-Current file)   11 May 1947: X5.  

Montez said during filming that she hoped to give a good performance along with the "sex and stuff people expect of me... Not that I have anything against glamor. But I would like a role I could get my teeth into. After all, I have two years typing to overcome, of going from one to another until I was groggy. And it is the hardest thing to do that sort of vamp - like Theda Bara - and not be laughable." Weird Sets in Atlantis Reflect Beauty of Montez
Scheuer, Philip K. Los Angeles Times (1923-Current File)   09 Mar 1947: B1.  

After filming wrapped both Aumont and Montez signed three year contracts with the producer to make one movie a year. Article 9 -- No Title: Laraine Day Signs With Coslow to Play Role in Film -- Wins Point on Dodgers
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   24 June 1947: 26.  BOGEAUS ACQUIRES BROMFIELD NOVEL: United Artists Producer Buys Early Autumn for Screen --Ida Lupino Will Star
By THOMAS F. BRADYSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   09 Apr 1947: 31.   "It is a picture of which I am very proud," said Montez. Montez the Magnificent
LOOKING AT HOLLYWOOD WITH HEDDA HOPPER. Chicago Daily Tribune (1923-1963)   28 Mar 1948: B18.  
===Reshoots===
Test screenings in Las Vegas went poorly and the producer became convinced that audiences did not understand the Pierre Benoit story because it was "too philosophical".  Douglas Sirk saw the movie and thought that "for various reasons not to do with Ripley, but mainly with the cast, it did not come off."  Sirk was asked to salvage the movie "but I didnt want to have anything to do with it anymore." 

Nebenzal managed to raise n estimated further $250,000 in funds for reshoots done over two weeks in July with John Brahm directing. PATRICIA NEAL GETS LEAD AT WARNERS: To Portray Dominique in Film of Ayn Rands Fountainhead, as Adapted by the Author
By THOMAS F. BRADYSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   21 June 1948: 18.    Morris Carnovskys role was reduced since he was not available and his character was replaced with a new one played by Henry Daniell. Maria Montez and Aumont returned and "violence and movement" was introduced, according to the producer. Neither Ripley nor Brahm were willing to take credit for the final version so the editor Gregg G. Tallas, who put together the two versions, was credited as director. 

==Reception==
The film had trouble securing distribution in the US, requiring re-editing. However it has now come to be appreciated as a camp classic. 

It was also known as Atlantis. Reissue Bill Scheduled
Los Angeles Times (1923-Current File)   03 Dec 1948: B6.  

The movie performed poorly at the box office and was described as "a calamity from a financial standpoint." Maria Montez in Rush to End Salary Hearing
Los Angeles Times (1923-Current File)   18 Apr 1950: 17.   The producer later revealed at a trial (see below) that the film needed to gross $3.5 million to break even and as at 1950 had only grossed $335,000.  However it did perform respectably in France with admissions of 2,188,732. 
===Critical Reception===
The Los Angeles Times said the film "does have its moments of action and violence but too much of it is given over to the philosophical introspection (or thinking aloud) of the characters." La Montez Queens It in Atlantis
Scheuer, Philip K. Los Angeles Times (1923-Current File)   11 Feb 1949: 19.  
==Law Suit==
In October 1948 Montez sued Nebenzal for $38,000. She claimed under her contract on 2 October 1946 she was to be paid $100,000, half during filming and the rest within nine months. Although the film finished 12 June 1947, Montez said she had only received $62,000. Maria Montez Files New Suit
Los Angeles Times (1923-Current File)   14 Oct 1948: 5.   The matter went to trial in 1950 and Montez had to fly back to the US from Europe to give evidence. Sues for $38,000
Chicago Daily Tribune (1923-1963)   18 Apr 1950: 16.   Maria Montez Tells of Purchases for Film
Los Angeles Times (1923-Current File)   19 Apr 1950: A1.   Judgement was returned in Montezs favor and it was ruled she was entitled to the whole amount. Maria Montez Victor in $38,000 Salary Suit
Los Angeles Times (1923-Current File)   03 Jan 1951: A12.  
== References ==
 

== External links ==
*  
*   BFI

 
 
 
 
 
 
 
 
 
 

 