Charlotte and Her Boyfriend
{{Infobox film name            = Charlotte and Her Boyfriend image           =  caption         =  director        = Jean-Luc Godard producer        = Pierre Braunberger writer          = Jean-Luc Godard starring        = Jean-Paul Belmondo, Gérard Blain, Anne Collette, Jean-Luc Godard (narrator) music           = Pierre Monsigny cinematography  = Michel Latouche editing         = Cécile Decugis, Jean-Luc Godard distributor     =  released        = 1961 runtime         = 13 min. language  French
|budget          = 
|}}
Charlotte and Her Boyfriend ( ) is a 13-minute 1960 film by Franco-Swiss director Jean-Luc Godard. It is shot entirely in or from Godards hotel room, in which Belmondos Jules gives Collettes Charlotte a seemingly endless and self-indulgent tirade on her faults and his tribulations. Belmondos voice is in fact dubbed by Godard.

It is a homage to Jean Cocteaus successful one-act play Le Bel Indifférent, where the roles are opposite.

It can be seen on the Criterion and Optimum DVDs of Breathless (1960 film)|À Bout de Souffle. 

==Cast==
* Jean-Paul Belmondo - Jean, the Old Boyfriend
* Gérard Blain - The New Boyfriend
* Anne Collette - Charlotte
* Jean-Luc Godard - Jean (voice)

== External links ==
*  

 

 
 
 
 
 
 
 
 


 