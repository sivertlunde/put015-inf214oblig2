The Broken Melody (1929 film)
{{Infobox film
| name           = The Broken Melody
| image          =
| caption        =
| director       = Fred Paul
| producer       = 
| writer         = Herbert Keith (play)   James Leader (play)   Thomas Coutts Elder   Fred Paul
| starring       = Georges Galli    Andrée Sacré   Enid Stamp-Taylor   Cecil Humphreys
| music          =
| cinematography =  Bernard Knowles
| editing        = 
| studio         = Welsh-Pearson
| distributor    = Paramount Pictures
| released       = October 1929
| runtime        = 6,414 feet 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}}
The Broken Melody is a 1929 British romance film directed by Fred Paul and starring Georges Galli, Andrée Sacré and Enid Stamp-Taylor.  It was originally made as a silent film, with sound added later. The film was shot at Cricklewood Studios and distributed by Paramount Pictures. It was based on a play by Herbert Keith and James Leader. An exiled Prince living in Paris, begins a daliance with an opera singer before returning to his wife. 

==Cast==
* Georges Galli as Prince Paul 
* Andrée Sacré as Bianca 
* Enid Stamp-Taylor as Gloria 
* Cecil Humphreys as Gen. Delange 
* Mary Brough as Marthe 
* Albert Brouett as Jacques 

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 