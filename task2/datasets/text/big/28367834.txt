Deewana (1952 film)
{{Infobox film
| name = Deewana
| image =
| caption = 
| director = Abdul Rashid Kardar
| producer = Abdul Rashid Kardar
| writer = 
| starring = Suraiya Suresh Sumitra Devi Shyam Kumar
| music = Naushad
| cinematography = 
| editing = 
| distributor =
| released = 1952
| country  = India Hindi
}}
Deewana is a 1952 Bollywood movie directed and produced by Abdul Rashid Kardar. It stars Suraiya, Suresh, Sumitra Devi and Shyam Kumar in the lead roles. The music for the film was composed by renowned musician Naushad with lyrics penned by Shakeel Badayuni. The film went on to become a huge success and celebrated Silver Jubilee.

==Cast==
*Suraiya as Laali Sumitra Devi as Mangala (as Sumitra)
*Suresh as Kumar
*Shyam Kumar	
*Ramesh
*Madan Puri as Phulwa
*S.N. Banerjee
*Amirbai Karnataki		 Mumtaz Begum 
*Nilambai
*Amir Banu
*Roop Kumar		
*Rattan Kumar
*Amir Ali

==Music==
{{Infobox album   
| Name = Deewana
| Type = soundtrack 
| Artist = Naushad 
| Cover = 
| Released = 1952
| Recorded =  Feature film soundtrack 
| Length = 
| Label =  
| Producer =
| Music Director = Naushad 
| Reviews =  Baiju Bawra (1952)
| This album = Deewana (1952)
| Next album = Amar (1954 film)|Amar (1954)
}}
The music for the film was composed by renowned musician Naushad with lyrics penned by Shakeel Badayuni.
#Mora Nazuk Badan - Suraiya
#Dil Mein Aa Gaya Koi - Suraiya
#Jeene Diya Na Chain Se - Suraiya
#Lagi Hai Manmandir  - Suraiya
#Mere Chand Mere Lal - Suraiya, Lata Mangeshkar
#Tasveer Banata Hoon Teri - Mohammed Rafi
#Teer Khate Jayenge - Lata Mangeshkar  
#Yeh Duniya Kaisi Hai - Hridayanath Mangeshkar
#Humein Jo Koi Dekhle - Shamshad Begum

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 