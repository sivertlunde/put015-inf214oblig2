Sling Blade
 
{{Infobox film
| name = Sling Blade
| image = Slingbladeposter.jpg
| caption = Theatrical release poster
| director = Billy Bob Thornton
| producer = Larry Meistrich David L. Bushell Brandon Rosser
| writer = Billy Bob Thornton
| based on = Some Folks Call It a Sling Blade by Billy Bob Thornton
| starring = Billy Bob Thornton Dwight Yoakam J. T. Walsh John Ritter Lucas Black Robert Duvall
| music = Daniel Lanois
| cinematography = Barry Markowitz
| editing = Hughes Winborne
| studio = Shooting Gallery
| distributor = Miramax Films
| released =  
| runtime = 135 minutes
| rating = R
| country = United States
| language = English
| budget = $1,000,000 
| gross = $24,444,121 
}} James Hampton, and Robert Duvall.
 Academy Award Best Writing, Best Actor in a Leading Role. The music for the soundtrack was provided by French Canadian artist/producer Daniel Lanois.

==Summary== intellectually disabled raping his mother. When he discovered that his mother was a willing participant in the affair, he killed her also.

Having developed a knack for small engine repair during his childhood and his institutionalization, Karl lands a job at a small-engine repair shop in the small town where he was born and raised. Around this time, he befriends 12-year-old Frank Wheatley (Lucas Black). Karl shares with Frank some of the details of his past, including the killings. Frank reveals that his father was killed - hit by a train - leaving him and his mother on their own - he later admits that he lied, and that his father committed suicide.
 gay friend, dollar store abusive alcoholic boyfriend, Doyle Hargraves (Dwight Yoakam). Eventually, Karl bonds with both Linda and Vaughan. In an early scene, Vaughan tells Karl that a gay man and a mentally challenged man face similar obstacles of intolerance and ridicule in small-town America.

Karl quickly becomes a father figure to Frank, who misses his father and despises Doyle. For Karl, Frank becomes much like a younger brother. Karl eventually reveals that he is haunted by the task given to him by his parents when he was six or eight years old to dispose of his preterm birth|premature, unwanted, newborn brother. In a subsequent scene, he visits his father (Robert Duvall), who has become a mentally unbalanced hermit living in the dilapidated home where Karl grew up. Karls parents performed an abortion, causing the baby to "come out too soon," and Karl was given a bloody towel wrapped around the baby, which survived the abortion. Karl was instructed to "get rid of it," but when Karl detected movement inside the towel, he inspected it, discovering "a little ol boy" that "wasnt no bigger than a squirrel." While recounting this story to Frank, Frank asks why Karl just didnt keep the baby, but Karl replies he had no way to care for a baby. He placed the baby, still in the bloody towel, inside a shoe box and buried the baby alive, saying he felt it was better to just "return him to the good Lord right off the bat," because of the abuse and neglect he himself had received at the hands of his own parents. Karl tells his father that killing the baby was wrong, and that he had wanted to kill his father for making him do it, but eventually decided that he wasnt worth the effort.

Meanwhile, Doyle becomes increasingly abusive towards Karl and Frank, leading to an eventual drunken outburst and physical confrontation with Linda and Frank. Linda then kicks Doyle out of the house (despite his threats to kill her if she ever left him). The next day, Linda and Doyle reconcile. Knowing that he has the upper hand again, Doyle confronts Karl and Frank and announces his plan to move into the house permanently; he plans "big changes", including Karls removal from the house. Karl begins to realize that he is the only one who can bring about a positive change and thus spare Frank and his mother a grim fate. Karl makes Frank promise to spend the night at Vaughans house, and asks Vaughan to pick up Linda from work and have her stay over also.

Later that evening, Karl returns to Lindas house, but seems undecided about whether he should enter. When confronted, a drunk Doyle asks what Karl is doing with the lawnmower blade he had sharpened and fashioned into a weapon which he was carrying. Karl replies, "I aim to kill you with it." After Karl asks Doyle how to reach the police by phone, Doyle says Karl should dial 911 and request "an ambulance, or a hearst". Then Karl kills Doyle with two chopping blows of the lawnmower blade to the head. Karl then calls the police to turn himself in, and requests a "hearst" be sent for Doyle. He then eats mustard and biscuits while waiting for the police.

Returned to the state hospital, he seems a different person than he was during his previous incarceration. He silences a sexual predator (played by J. T. Walsh) who had previously forced him to listen to tales of his horrible deeds.

==Cast==
* Billy Bob Thornton as Karl Childers
* Dwight Yoakam as Doyle Hargraves
* J. T. Walsh as Charles Bushman
* John Ritter as Vaughan Cunningham
* Lucas Black as Frank Wheatley
* Natalie Canerday as Linda Wheatley James Hampton as Jerry Woolridge
* Robert Duvall as Karls father
* Christine Renee Ward as Melinda, Karls girlfriend who worked at the Dollar Store
* Jim Jarmusch as Gene, the Frostee Cream employee  
* Vic Chesnutt as Terence
* Rick Dial as Bill Cox, the boss of the small engine shop
* Brent Briscoe as Scooter, Bills employee
* Tim Holder as Albert, Vaughans boyfriend
* Bruce Hampton as Morris, the songwriting band manager Ian Moore as Randy Horsefeathers

==Reception==
===Critical response===
The film garnered both critical acclaim and box office success. It grossed $24,444,121 on a $1 million budget. The film received a 96% "Certified Fresh" rating by Rotten Tomatoes with an average rating of 8.3 out of 10, with 49 critics giving generally favorable reviews and only two negative reviews; the sites consensus states "You will see whats coming, but the masterful performances, especially Thorntons, will leave you riveted." 

The Washington Post called it a "masterpiece of Southern storytelling."  Kevin Thomas wrote in the Los Angeles Times that the film is "a mesmerizing parable of good and evil and a splendid example of Southern storytelling at its most poetic and imaginative." New York Times critic Janet Maslin praised the performances but said that "it drifts gradually toward climactic events that seem convenient and contrived."

==Awards and nominations==
* Academy Awards Best Adapted Screenplay (Thornton) Best Actor (Thornton) Chicago Film Critics Awards Best Actor (Thornton)
* Edgar Awards Best Motion Picture Screenplay (Thornton)
* Independent Spirit Awards
** Won for Best First Feature Kansas City Film Critics Awards Best Actor (Thornton) National Board of Review Awards Special Achievement in Filmmaking (Thornton)
* Satellite Awards Best Actor – Motion Picture Drama (Thornton) Best Original Screenplay (Thornton) Screen Actors Guild Awards Outstanding Performance by a Cast
** Nominated for Outstanding Performance by a Male Actor in a Leading Role (Thornton) Writers Guild of America Awards
** Won for Best Adapted Screenplay (Thornton)
* Young Artist Award Best Leading Young Actor in a Feature Film (Black)
* YoungStar Award
** Won for Best Young Actor in a Drama Film (Black)

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   – Actual locations of Movie Landmarks
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 