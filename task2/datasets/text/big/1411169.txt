Andrei Rublev (film)
 
{{Infobox film
| name           = Andrei Rublev
| image          = Andrei Rublev Poster.jpg
| image_size     =
| caption        =
| director       = Andrei Tarkovsky
| producer       = Tamara Ogorodnikova  
| writer         = Andrei Konchalovsky Andrei Tarkovsky
| narrator       =
| starring       = Anatoly Solonitsyn Ivan Lapikov Nikolai Grinko Nikolai Sergeyev Nikolai Burlyayev Irma Raush
| music          = Vyacheslav Ovchinnikov
| cinematography = Vadim Yusov
| distributor    =
| studio         = Mosfilm
| released       =  
| runtime        = 205 min. (original version)
186 min. (standard version)
| country        = Soviet Union
| language       = Russian rubles
| preceded_by    =
| followed_by    =
}}
 icon painter. art historian, was a scientific consultant of the film.
 realistic portrait of medieval Russia. Tarkovsky sought to create a film that shows the artist as "a world-historic figure" and "Christianity as an axiom of Russia’s historical identity" {{cite web
  | last = Hoberman
  | first = Jim
  | authorlink =
  | title = Andrei Rublev 
  | work =
  | publisher = The Criterion Collection
  | url = http://www.criterion.com/asp/release.asp?id=34&eid=50&section=essay
  | format =
  | doi = Russian history that ultimately resulted in the Tsardom of Russia. The films themes include artistic freedom, religion, political ambiguity, autodidacticism, and the making of art under a repressive regime. Because of this, it was not released domestically in the officially atheist and authoritarian Soviet Union for years after it was completed, except for a single 1966 screening in Moscow. {{cite book
  | last = Turovskaya
  | first = Maya
  | authorlink =
  | title = Tarkovsky: Cinema as Poetry 
  | publisher = Faber and Faber
  | year = 1989
  | location = London
  | pages =
  | url =
  | doi =
  | id =  
  | isbn = 0-571-14709-7}}  A version of the film was shown at the 1969 Cannes Film Festival, where it won the FIPRESCI prize.    In 1971, a censored version of the film was released in the Soviet Union. The film was further cut for commercial reasons upon its U.S. release through Columbia Pictures in 1973. As a result, several versions of the film exist.

In recent years, Andrei Rublev has come to be regarded as one of the greatest films of all time, and ranked in both the Sight & Sound critics and directors polls. 

==Plot summary==
:Note: The following synopsis refers to the original, 205&nbsp;minute version of the film.
 15th century Russia, a turbulent period characterized by fighting between rival princes and the Tatar invasions.

The films prologue shows the preparations for a hot air balloon ride. The balloon is tethered to the spire of a church next to a river, with a man named Yefim (Nikolay Glazkov) attempting to make the flight by use of a harness roped beneath the balloon. At the very moment of his attempt an ignorant mob arrive from the river and attempt to thwart the flight, putting a firebrand into the face of one of the men on the ground assisting Yefim. In spite of this the balloon is successfully released and Yefim is overwhelmed and delighted by the view from above and the sensation of flying, but he can not prevent a crash landing shortly after. He is the first of several creative characters, representing the daring escapist, whose hopes are easily crushed. After the crash, a horse is seen rolling on its back by a pond, a symbol of life &mdash; one of many horses in the movie.

  (c. 1360–1430)]]

I. The Jester (Summer 1400)
 Andrei (Anatoly Daniil (Nikolai Andronikov Monastery, where they have lived for many years, heading to Moscow. During a heavy rain shower they seek shelter in a barn, where a group of villagers is being entertained by a jester (Rolan Bykov). The jester, or skomorokh, is a bitterly sarcastic enemy of the state and the Church, who earns a living with his scathing and obscene social commentary and by making fun of the Boyars. He ridicules the monks as they come in, and after some time Kirill leaves unnoticed. Shortly, a group of soldiers arrive to arrest the skomorokh, who they take outside, knock unconscious and take away, also smashing his musical instrument. As the rain has stopped the three monks thank the villagers for allowing them to shelter and continue on their way. As they walk on the heavy rain starts again.

II. Theophanes the Greek (Summer–Winter–Spring–Summer 1405–1406)

Kirill arrives at the workshop of   in Moscow. Kirill refuses at first, but then accepts the offer on the condition that Theophanes will personally come to the Andronikov Monastery and invite Kirill to work with him in front of all the fraternity and Andrei Rublev, who according to Theophanes comments has some fame as an icon painter in the outside world.

A short while later at the Andronikov Monastery, a messenger arrives from Moscow to ask Andrei for his assistance in decorating the Annunciation Cathedral with Theophanes the Greek. Both Daniil and Kirill are agitated by the recognition that Andrei receives. Daniil refuses to accompany Andrei and reproaches him for accepting Theophanes’ offer without considering his fellows, but soon repents of his temper and tearfully wishes Andrei well when the younger monk comes to say goodbye to his friend. Kirill is jealous of Andrei and in a fit of anger, decides to leaves the monastery for the secular world, throwing accusations of greed in the face of his fellow monks, who also dismiss him. Kirill stumbles out of the monastery into the snowy countryside and is pursued by his dog, but Kirill savagely beats it with his walking stick and leaves it for dead. Andrei leaves for Moscow with his young apprentice Foma (Mikhail Kononov). Foma is another creative character, representing the light-hearted and practical-minded commercial artist. Still he seems to be contemplative enough to get along with Andrei.

III. The Passion According to Andrei (1406)

While walking in the woods, Andrei and Foma have a conversation about Foma’s faults, especially lying. Foma confesses to taking honey from the bee garden, after Andrei notices his cassock is tacky, and smears mud on his face to soothe a bee sting. While Foma has talent as an artist, he is less concerned with the deeper meaning of his work and more concerned with practical aspects of the job, like perfecting his azure (color)|azure, a colour which in painting was often considered unstable to mix. They encounter Theophanes in the forest, and the old master sends Foma away. As he leaves, the apprentice finds a dead swan and pokes at it with a stick. We cut to banks of a stream where Andrei and Theophanes are arguing about religion, while Foma cleans his masters paint brushes. Theophanes argues that the ignorance of the Russian people is due to stupidity, while Andrei says that he doesn’t understand how he can be a painter and maintain such views. This section contains a reenactment of Christs Crucifixion on a snow-covered hillside which plays out as Andrei recounts the story and expresses his belief that the men who crucified Jesus were obeying Gods will and loved him.

IV. The Feast (1408)

Camping for the night on a riverbank, Andrei and Foma are collecting firewood for their group when Andrei hears the distant sounds of celebration further upstream in the woods. Going to investigate he encounters a large group of naked Paganism|pagans, who are conducting a torch lit ritual for Midsummer. Andrei is intrigued and excited by the behaviour of the pagans but is caught spying on a couple making love, is tied to the crossbeam of a hut in a mockery of Jesus crucifixion and is threatened with drowning in the morning. A woman named Marfa (Nelly Snegina), dressed only in a fur coat approaches Andrei. After explaining that her people are persecuted for their beliefs she drops her coat, kisses Andrei and then unties him. Andrei runs away, and is lost in the dense woods, scratching his face. The next morning Andrei returns to his group, including Daniil, and as they leave on their boats a group of soldiers appear on the riverbank chasing after several of the pagans including Marfa. Her partner is captured but she escapes by swimming into the river past Andrei’s boat. He and his fellow monks look away in shame.

V. The Last Judgment (Summer 1408)

Andrei and Daniil are working on the decoration of a church in Vladimir. Although they have been there for several months the walls are still white and bare as Andrei is doubting himself. A messenger arrives with word from the furious Bishop to say they have until the Autumn to finish the job. On a nearby road in the middle of a field of flowers Andrei confides to Daniil that the task disgusts him and that he is unable to paint a subject such as the Last Judgement as he doesn’t want to terrify people into submission. He comes to the conclusion that he has lost the ease of mind that an artist needs for his work. Foma, impatient and ambitious, resigns and leaves Andreis group to take up the offer of painting a smaller, less prestigious, church. Stone carvers and decorators of Andreis party have also been working on the Grand Princes mansion. The Prince wants the work to done again more in line with his tastes but the workers already have another job, at the mansion of the Grand Princes brother, and refuse. On a path through the woods soldiers accost the artisans on the orders of the Grand Prince and gouge their eyes out, so that they cannot replicate their work. Back at the church Andrei is dismayed by the news of their fate and angrily throws paint and smears it on one of the walls. Sergei (Vladimir Titov) one of the young apprentices who escaped the attack unharmed reads a random section of the bible aloud, at Daniils request, concerning women. Durochka (Irma Raush) (whose name identifies her as a holy fool or Yurodivy), wanders in out of the rain and is upset by the sight of the paint on the wall. Her feeble-mindedness and innocence leads Andrei to the idea to paint a Banquet|feast.

VI. The Raid (Autumn 1408)

While the Grand Prince is away in Lithuania his power hungry younger brother forms an allegiance with a group of Tatars and raid Vladimir. We see flashbacks of the Grand Prince and his brother attending a religious service in the church, and see the rivalry and animosity between them. The invasion of the combined armed forces on horseback and the resulting carnage is shown in great detail. The city is burned, the citizens are murdered and women raped and killed. One scene shows a horse falling from a flight of stairs and being stabbed by a spear. Another shows a cow being set on fire. Foma narrowly escapes being killed in the city and escapes into the nearby countryside. As he is crossing a river a Tatar sentry shoots him in the back with an arrow, as he dies he falls into the river and is swept away. The Tatars force their way into the barricaded church, now fully decorated with Andreis paintings, where the majority of the citizens have taken refuge. The Tatars show no mercy and massacre the people inside and burn all the painted wooden altarpieces. Andrei saves Durochka from being raped by killing the invader with an axe. The Bishops messenger is cruelly tortured to make him reveal the location of the citys gold, which he refuses to do. After being repeatedly burned, he has liquid metal from a melted crucifix poured into his mouth and is dragged away tied to a horse. In the aftermath only Andrei and Durochka are left alive in the church. Andrei imagines a conversation with the dead Theophanes the Greek, lamenting the loss of his work and the evil of mankind, while Durochka distractedly plaits the hair of a dead woman. Andrei decides to give up painting and takes a vow of silence to atone for killing another man.

  from the so-called Zvenigorod Chin (ca. 1410; today at the Tretyakov Gallery, Moscow)]]

VII. The Silence (Winter 1412)

Andrei is once again at the Andronikov Monastery as famine and war grips the country. He no longer paints and never speaks, and keeps Durochka with him as a fellow companion in silence. Several refugees discuss the problems in their respective home towns, and one man talks in a broken voice of his escape from Vladimir. He is recognised by a younger monk as the long absent Kirill. He has suffered during his time away from the monastery and begs the father superior to allow him to return. His wish is granted but he is instructed to copy out the holy scriptures fifteen times in penance. A group of Tatars stop by at the monastery while travelling through, much to the concern of Andrei and Kirill who experienced their brutality first hand. Durochka is too simple minded to remember what the Tatars did and is fascinated by one of the soldiers shining breastplate. The group taunt and play with her, but the soldier takes a liking to her, putting his horned helmet on her head and dressing her as a bride, finally deciding to take her away with him as his eighth, and only Russian, wife. Andrei attempts to stop her from leaving, but she is determined and rides away with the Tatars. Kirill talks to Andrei for the first time since they both left the monastery, and he assures him that Dorochka wont be in any danger, as harming a holy fool is considered bad luck, and she will be let go. Andrei continues with his menial work of carrying large hot stones from a fire with tongs to heat water for the monastery, but drops the stone in the snow.  

VIII. The Bell (Spring–Summer–Winter–Spring 1423–1424)

Andrei’s life turns around as he witnesses the casting of a bell for the Grand Prince. The bellmaker and all his family have died of a plague that has ravaged the area, and only his son Boriska (Nikolai Burlyayev) has survived. He tells the Princes men that he is the only one who possesses his fathers secret, delivered on his death bed, of casting a copper bell and persuades them to take him with them as he is the only person left alive who can make it successfully. Boriska is put in charge of the project and frequently contradicts and challenges the instincts of his co-workers when choosing the location of the pit, the selection of the proper clay, the building of the mold, the firing of the furnaces and finally the hoisting of the bell. The process of making the bell grows into a huge, expensive endeavour with many hundreds of workers and Boriska makes several risky decisions, guided only by his instincts. As the furnaces are opened and the molten metal pours into the mould, he privately asks God for help. Andrei silently watches Boriska during the casting, and the younger man notices him too.

During the bell-making, the skomorokh from the first sequence makes a reappearance amongst the crowds who have come to watch the bell being raised up and he threatens to kill Andrei, who he mistakes for the man who denounced him years earlier which led to his arrest, torture and prison sentence. Kirill intervenes on behalf of the silent Andrei and later privately confesses that his sinful envy of Andrei’s talent dissipated once he heard Andrei had abandoned painting and that it was he, Kirill, who had denounced the skomorokh. Kirill then criticises Andrei for allowing his God-given talent for painting to go waste and pleads with him to resume his artistry, to no response.

 , Moscow)]]

As the bell-making nears completion Boriska’s confidence slowly transforms into a stunned, detached disbelief that he’s succeeded at the task. The work crew takes over as Boriska makes several attempts to fade into the background of the activity. Once the bell has been hoisted into its tower the Grand Prince and his entourage arrive for the inaugural ceremony as the bell is blessed by the priests. As the bell is prepared to be rung the royal entourage is overhead discussing their doubts that it will. It is revealed that Boriska and the work crew know if the bell fails to ring the Grand Prince will have them all beheaded. (It is also overheard that the Grand Prince had his brother, who raided Vladimir in The Raid sequence, beheaded.) There is a quiet, agonizing tension as the foreman slowly coaxes the bells clapper back and forth, nudging it closer to the lip of the bell with each swing. A pan across the assembly reveals white-robed Durochka, leading a horse (preceded by a boy, presumably her son) as she walks through the crowd. At the critical moment the bell rings perfectly and she smiles. After the ceremony, Andrei finds Boriska collapsed on the ground, sobbing as he admits his father never told him the secret of casting a bell. Andrei comforts him, breaking his vow of silence and telling the boy that they should carry on their work together: “You’ll cast bells. I’ll paint icons.” Andrei sees Durochka, the boy, and the horse walk off across a muddy field in the distance.

The epilogue is the only part of the film in colour and shows time-aged, but still vibrant, details of several of Andrei Rublev’s actual icons. The icons are shown in the following order: Enthroned Christ, Twelve Apostles, The Annunciation, Twelve Apostles, Jesus entering Jerusalem, Birth of Christ, Enthroned Christ, Transfiguration of Jesus, Resurrection of Lazarus, The Annunciation, Resurrection of Lazarus, Birth of Christ, Trinity, Archangel Michael, Paul the Apostle, The Redeemer. The final scene crossfades from the icons and shows four horses standing by a river in the rain.

==Cast==

* Anatoly Solonitsyn - Andrei Rublev
* Nikolai Grinko - Daniel Chorny
*   - Theophanes the Greek
* Nikolai Burlyayev - Boriska
* Ivan Lapikov - Kirill
* Yuri Nikulin - Patrikei Yuriy Nazarov - Prince Yury of Zvenigorod/Grand Prince Vasily I of Moscow
* Rolan Bykov - the Skomorokh
* Irma Raush - Durochka (the holy fool girl)
* Mikhail Kononov - Foma
* Nikolay Glazkov - Yefim  Khan of the Nogai Horde
* Irina Miroshnichenko - Mary Magdalene

==Production== treatment was medieval history and art. In April 1964 the script was approved and Tarkovsky began working on the film.  At the same time the script was published in the influential film magazine Iskusstvo Kino, and was widely discussed among historians, film critics and ordinary readers. The discussion on Andrei Rublev centered on the sociopolitical and historical, and not the artistic aspects of the film.

According to Tarkovsky, the original idea for a film about the life of Andrei Rublev was due to the film actor Vasily Livanov. Livanov proposed to write a screenplay together with Tarkovsky and Konchalovsky while they were strolling through a forest on the outskirts of Moscow. He also mentioned that he would love to play Andrei Rublev. {{cite journal
  | last = Ciwilko
  | first = Artur
  | authorlink =
  | title = Interview Andrzej Tarkowski — o filmie "Rublow"
  | journal = Ekran
  | volume = 12
  | issue =
  | pages = 11 
  | publisher = 
  | location =
  | year = 1965
  | url = http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/On_Rublov.html
  | doi =
  | id =
  | accessdate = 2007-12-09 | archiveurl= http://web.archive.org/web/20071211124453/http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/On_Rublov.html| archivedate= 11 December 2007  | deadurl= no}}  Tarkovsky did not intend the film to be a historical or a biographical film about Andrei Rublev. Instead, he was motivated by the idea of showing the connection between a creative characters personality and the times through which he lives. He wanted to show an artists maturing and the development of his talent. He chose Andrei Rublev for his importance in the history of Russian culture. {{cite journal
  | last = Bachman
  | first = Gideon
  | authorlink =
  | title = Begegnung mit Andrej Tarkowskij
  | journal = Filmkritik
  | volume = 12
  | issue =
  | pages = 548–552 
  | publisher = 
  | location =
  | year = 1962
  | url = http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/On_Rublov.html
  | doi =
  | id =
  | accessdate = 2007-12-09 | archiveurl= http://web.archive.org/web/20071211124453/http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/On_Rublov.html| archivedate= 11 December 2007  | deadurl= no}} 

Tarkovsky cast Anatoli Solonitsyn for the role of Andrei Rublev. At this time Solonitsyn was an unknown actor at a theater in Yekaterinburg|Sverdlovsk. According to Tarkovsky everybody had a different image of the historical figure of Andrei Rublev, thus casting an unknown actor who would not remind viewers of other roles was his favoured approach. Solonitsyn, who had read the film script in the film magazine Iskusstvo Kino, was very enthusiastic about the role, traveled to Moscow at his own expense to meet Tarkovsky and even declared that no one could play this role better than him. {{cite journal
  | last = Ciment
  | first = Michel
  | last2 = Schnitzer
  | first2 = Luda & Jean
  | authorlink =
  | title = Interview Lartiste dans lancienne Russe et dans lURSS nouvelle (Entretien avec Andrei Tarkovsky)
  | journal = Positif
  | volume = 109
  | issue =
  | pages = 1&ndash;13
  | publisher = 
  | location =
  | date = October 1969
  | url = http://www.acs.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/On_Rublov.html
  | doi =
  | id =
  | accessdate = 2007-12-08 }}  Tarkovsky felt the same, saying that "with Solonitsyn I simply got lucky". For the role of Andrei Rublev he required "a face with great expressive power in which one could see a demoniacal single-mindedness". To Tarkovsky, Solonitsyn provided the right physical appearance and the talent of showing complex psychological processes. {{cite journal
  | last = Veress 
  | first = Jozsef
  | authorlink =
  | title = Hüsség a vállalt eszméhez
  | journal = Filmvilág
  | volume = 10
  | issue =
  | pages = 12&ndash;14
  | publisher = 
  | location =
  | year = 1969
  | url = http://www.acs.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/On_Rublov.html
  | doi =
  | id = The Mirror, and Stalker (1979 film)|Stalker, and in the title role of Tarkovskys 1976 stage production of Hamlet in Moscows Lenkom Theatre. Before his death from cancer in 1982, Solonitsyn was also intended to play protagonist Andrei Gortchakov in Tarkovskys 1983 Italian-Russian co-production Nostalghia,  and to star in a project titled The Witch which Tarkovsky would significantly alter into his final production, The Sacrifice. 

 

Tarkovsky chose to shoot the main film in black and white and the epilogue, showing some of Andrei Rublevs icons, in Color motion picture film|color. In an interview he motivated his choice with the claim that in everyday life one does not consciously notice colors. {{cite journal
  | last = Chugunova 
  | first = Maria
  | authorlink = 
  | title = On Cinema - Interview with Tarkovsky
  | journal = To the Screen
  | volume = 
  | issue = 
  | pages = 
  | publisher = 
  | location = 
  | date = December 1966
  | url = http://www.acs.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/On_Color.html
  | doi = 
  | id =  }}  Consequently Rublevs life is in black and white, whereas his art is in color. The film was thus able express the co-dependence of an artists art and his personal life. The color sequence of Rublevs icons begins with showing only selected details, climaxing in Rublevs most famous icon, The Trinity. One reason for including this color final was, according to Tarkovsky, to give the viewer some rest and to allow him to detach himself from Rublevs life and to reflect. The film finally ends with the image of horses at river in the rain. To Tarkovsky horses symbolized life, and including horses in the final scene (and in many other scenes in the film) meant that life was the source of all of Rublevs art. 

Filming did not begin until April 1965, one year after approval of the script. {{cite book
  | last = Johnson
  | first = Vida T.
  | last2 = Petrie
  | first2 = Graham
  | authorlink =
  | title = The Films of Andrei Tarkovsky: A Visual Fugue
  | publisher = Indiana University Press
  | year = 1994
  | location = Bloomington
  | pages =
  | url =
  | doi =
  | id =   War and Nerl River and the historical places of Vladimir/Suzdal, Pskov, Izborsk and Pechory. {{cite journal
  | last = Lipkov 
  | first = Aleksandr
  | authorlink =
  | title = Strasti po Andreiu (Interview with Andrei Tarkovsky on February 1, 1967, transl. by Robert Bird)
  | journal = Literaturnoe obozrenie 
  | volume = 
  | issue =
  | pages = 74&ndash;80
  | publisher = 1988
  | location =
  | url = http://www.acs.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/PassionacctoAndrei.html
  | doi =
  | id =
  | accessdate = 2007-12-07 }} 

Several scenes within the film depict violence, torture and cruelty toward animals, leading to controversy and censorship attempts upon completion of the film. Most of these scenes took place during the raid of Vladimir, showing for example the blinding and the torture of a monk. Most of the scenes involving cruelty toward animals were simulated. For example, during the Tatar raid of Vladimir a cow is set on fire. In reality the cow had an asbestos-covered coat and was not physically harmed; however, one scene depicts the real death of a horse. The horse falls from a flight of stairs and is then stabbed by a spear. To produce this image, Tarkovsky injured the horse by shooting it in the neck and then pushed it from the stairs, causing the animal to falter and fall down the flight of stairs.  From there, the camera pans off the horse onto some soldiers to the left and then pans back right onto the horse, and we see the horse struggling to get its footing having fallen over on its back before being stabbed by the spear.  The animal was then shot in the head afterward off camera. This was done to avoid the possibility of harming what was considered a lesser expendable, highly prized stunt horse. The horse was brought in from a slaughterhouse, killed on set, and then returned to the abattoir for commercial consumption. In a 1967 interview for Literaturnoe obozrenie, interviewer Aleksandr Lipkov suggested to Tarkovsky that  "the cruelty in the film is shown precisely to shock and stun the viewers. And this may even repel them." In an attempt to downplay the cruelty Tarkovsky responded: "No, I dont agree. This does not hinder viewer perception. Moreover we did all this quite sensitively. I can name films that show much more cruel things, compared to which ours looks quite modest." 
 The Mirror, made in 1975.  It thus forms the first part in a series of three films by Tarkovsky referencing Andrei Rublev.

==Distribution==

The first cut of the film was completed in July 1966 and was named The Passion According to Andrei and ran 205 minutes. Goskino demanded cuts to the film, citing its length, negativity, violence, and nudity.  {{cite book
  | last = Le Fanu
  | first = Mark
  | authorlink =
  | title = The Cinema of Andrei Tarkovsky
  | publisher = BFI
  | year = 1987
  | location = London
  | pages =
  | url =
  | doi =
  | id =  
  | isbn = 0-85170-193-0}}   After Tarkovsky completed this first version, it would be five years before the film was widely released in the Soviet Union.

The ministrys demands for cuts first resulted in a 190-minute version. Despite Tarkovskys objections expressed in a letter to Alexey Romanov, the chairman of Goskino, the ministry demanded further cuts, and Tarkovsky trimmed the length to 186&nbsp;minutes. {{cite journal
  | last = Vinokurova 
  | first = Tatyana
  | authorlink =
  | title = Khozdenye po mukam Andreya Rublyova
  | journal = Iskusstvo Kino
  | volume = 10
  | issue =
  | pages = 63&ndash;76
  | publisher =
  | location = Moscow
  | year = 1989
  | url =
  | doi = Central Committee of the Communist Party.
 FIPRESCI prize. Soviet officials tried to prevent the official release of the film in France and other countries, but were not successful as the French distributor had legally acquired the rights in 1969. 

In the Soviet Union, influential admirers of Tarkovskys work—including the film director Grigori Kozintsev, the composer Dmitri Shostakovich and Yevgeny Surkov, the editor of Iskusstvo Kino —began pressuring for the release of Andrei Rublev.  Tarkovsky and his second wife, Larisa Tarkovskaya wrote letters to other influential personalities in support of the films release, and Larisa Tarkovskaya even went with the film to Alexei Kosygin, then the Premier of the Soviet Union.

Despite Tarkovskys refusal to make further cuts, Andrei Rublev finally was released on December 24, 1971 in the  186-minute 1966 version. The film was released in 277 prints and sold 2.98&nbsp;million tickets.  When the film was released, Tarkovsky complained in his diary that in the entire city not a single poster for the film could be seen but that all theaters were sold out. {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=translated by Kitty Hunter-Blair
  | title =  
  | publisher = Seagull Books
  | year = 1991
  | location = Calcutta
  | pages = 
  | url =
  | doi =
  | id =  
  | isbn = 81-7046-083-2}} 

Despite the cuts having originated with Goskinos demands, Tarkovsky ultimately endorsed the 186-minute cut the film over the original 205-minute version:

 Nobody has ever cut anything from Andrei Rublev. Nobody except me. I made some cuts myself. In the first version the film was 3 hours 20 minutes long. In the second — 3 hours 15 minutes. I shortened the final version to 3 hours 6 minutes. I am convinced the latest version is the best, the most successful. And I only cut certain overly long scenes. The viewer doesnt even notice their absence. The cuts have in no way changed neither the subject matter nor what was for us important in the film. In other words, we removed overly long scenes which had no significance. 

 We shortened certain scenes of brutality in order to induce psychological shock in viewers, as opposed to a mere unpleasant impression which would only destroy our intent. All my friends and colleagues who during long discussions were advising me to make those cuts turned out right in the end. It took me some time to understand it. At first I got the impression they were attempting to pressure my creative individuality. Later I understood that this final version of the film more than fulfils my requirements for it. And I do not regret at all that the film has been shortened to its present length.  

In 1973, the film was shown on Soviet television in a 101-minute version that Tarkovsky did not authorize. Notable scenes that were cut from this version were the raid of the Tartars and the scene showing naked pagans. The epilogue showing details of Andrei Rublevs icons was in black and white as the Soviet Union had not yet fully transitioned to color TV. In 1987, when Andrei Rublev was once again shown on Soviet TV, the epilogue was once again in black and white, despite the Soviet Union having completely transitioned to color TV. Another difference from the original version of the film was the inclusion of a short explanatory note at the beginning of the film, detailing the life of Andrei Rublev and the historical background.  When the film was released in the U.S. and other countries in 1973, the distributor Columbia Pictures cut it by an additional 20 minutes, making the film an incoherent mess in the eyes of many critics and leading to unfavorable reviews. 
 Criterion Collection released the original, 205-minute version of Andrei Rublev on laserdisc, which Criterion re-issued on DVD in 1999. (Criterion advertises this version as the "directors cut," despite Tarkovskys stated preference for the 186-minute version.) According to Tarkovskys sister, Marina Tarkovskaya, one of the editors of the film, Lyudmila Feiginova, secretly kept a print of the 205-minute cut under her bed. {{cite web
  | last = Blasco
  | first = Gonzalo
  | authorlink =
  | title = An Interview with Marina Tarkovskaia and Alexander Gordon
  | work =
  | publisher = www.andreitarkovski.org
  | date = November 10, 2003
  | url = http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/Marina_and_Alexandr.html
  | doi =
  | accessdate = 2007-12-10 | archiveurl= http://web.archive.org/web/20071211124406/http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/Marina_and_Alexandr.html| archivedate= 11 December 2007  | deadurl= no}}
   Criterions producer from the project stated that the video transfer was sourced from a film print that filmmaker Martin Scorsese had acquired while visiting Russia. 

==Awards==
 Grand Prix. Nevertheless, it won the prize of the international film critics, FIPRESCI. In 1971 Andrei Rublev won the Critics Award of the French Syndicate of Cinema Critics, and in 1973 the Jussi Award for best foreign film.

===Regard===
In 2010 the film was honoured when it came equal second in a U.K. newspaper series of the "Greatest Films of All Time" as voted by critics from  The Guardian and The Observer. 

The film was ranked No. 87 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web 
| title = The 100 Best Films Of World Cinema:  87. Andrei Rublev
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=87
| work = Empire
| year = 2010 
| accessdate=24 June 2013 
}} 
 

Also in 2010, the Toronto International Film Festival released its "Essential 100" list of films in which Andrei Rublev also placed No. 87.   

The film is rated at No. 25 on the Rate Your Music websites top 100 films chart. 

==Influence==
The film inspired Polish composer, Kasia Glowicka to construct a 2009 audio-visual performance called "Quasi Rublev," inspired by the film, with Goska Isphording playing harpsichord and Roos Theuws performing live visuals. 

==See also==
*Middle Ages in film

==References==

===Footnotes===
:  In the Soviet Union the role of a producer was different from that in Western countries and more similar to the role of a line producer or a unit production manager. 

===Notes===
 

===Bibliography===
* 
* 
*  
* 

==External links==
* 
* 
*  at official Mosfilm site with English subtitles
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 