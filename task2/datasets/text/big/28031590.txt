Sivaranjani (film)
 
 
}}
{{Infobox film
| name           = Sivaranjani
| image          = Sivaranjani.jpg
| image_size     =
| caption        =
| director       = Dasari Narayana Rao
| producer       = Dasari Padma
| writer         = Dasari Narayana Rao (story, dialogues, screenplay)
| narrator       =
| starring       = Jayasudha Hari Prasad Mohan Babu Allu Ramalingaiah Nirmalamma Subhashini Murali Mohan
| music          = Pasupuleti Ramesh Naidu
| cinematography = K. S. Mani
| editing        = K. Balu
| studio         =
| distributor    =
| released       = 1978
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Sivaranjani ( ) is 1978 Telugu drama film written and directed by Dasari Narayana Rao. The film is about a village girl Sivaranjani who goes on to become a famous film actress, played by Jaya Sudha. Later it was remade in Tamil as Natchathiram with Sripriya in the leading role in 1980.

It is a musical film with lyrics penned by C. Narayana Reddy. The music score is provided by P. Ramesh Naidu. The Abhinava Thaaravo Naa Abhimana Thaaravo and Joru Meedunnave Thummeda, Nee Joru Evari Kosame Thummeda ? are memorable songs.

==Plot==
The film is about a village girl Sivaranjani (Jayasudha), who goes on to become a famous film actress. She goes on to become a big film star. A small town boy, Hari Prasad forms her fan club and starts admiring her. Sivaranjani starts finding her true love in this boy. She gives up her career as a film actress, escapes the trauma of limelight and settling down with this boy.

==Cast==
* Jayasudha	... 	Sivaranjani
* Hari Prasad	... 	Sivaranjanis Fan
* Mohan Babu		
* Allu Ramalingaiah		
* Nirmalamma		
* Subhashini		
* Gokineni Rama Rao	... 	Sivaranjanis Brother
* K. V. Chalam		
* Murali Mohan

==Soundtrack==
* Abhinava Thaaravo Naa Abhimana Thaaravo (Lyrics:  )
* Chandamama Vacchaadamma Tongi Tongi Ninu Choochaa
* Joru Meedunnave Thummeda, Nee Joru Evari Kosame Thummeda ?
* Maapalle Vadalaku Krishnamurthi Nuvvu Konte Panulakochava
* Mee Ammavaadu Naa Kosam Eeniuntadu
* Navami Naati Vennela Nenu Dasami Naati Jaabili Neevu (Singers: S. P. Balasubrahmanyam and P. Susheela)
* Paalakollu Santalona Paapayammo Paapayamma (Singers: S. P. Balasubrahmanyam and P. Susheela)

==Features==
* Actor Hari Prasad, who is remembered for his role in this film. He died at the age of 50 years in 2008 while participating in a discussion on entry of Chiranjeevi into politics in a TV news channel in Chennai.  He has acted in over 100 films and also produced some movies in Telugu and Kannada languages.
* The lead Violin tune of Joru Meedunnave Thummeda, Nee Joru Evari Kosame Tummeda? song is played by Y. N. Sharma, father of Music Director Mani Sharma. 

==References==
 

==External links==
*  

 
 
 
 
 