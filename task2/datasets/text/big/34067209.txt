Trumbo (2007 film)
{{Infobox film
| name           = Trumbo
| image          = Trumbo (film).jpg
| caption        =
| director       = Peter Askin
| producer       = {{plainlist|
* Will Battersby
* Alan Klingenstein
* Tory Tunnell
}}
| writer         = Christopher Trumbo
| starring       = Dalton Trumbo
| music          = Robert Miller
| cinematography = {{plainlist|
* Jonathan Furmanski Fred Murphy
* Chris Norr
* Frank Prinzi
}}
| editing        = Kurt Engfehr
| studio         = {{plainlist|
* Filbert Steps Productions
* Reno Productions
* Safehouse Pictures
}}
| distributor    = {{plainlist|
* IDP Distribution
* Red Envelope Entertainment
* Samuel Goldwyn Films
}}
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Communist influences in the Hollywood film industry.   

The film debuted at the Toronto International Film Festival and includes film clips and interviews, readings from Trumbos letters by performers such as Michael Douglas, Joan Allen, Donald Sutherland, Liam Neeson, and Paul Giamatti, and a reenactment by David Strathairn of a speech given by Dalton Trumbo in 1970.    The readings include parts of what the New York Times calls "Dalton Trumbos remarkably stage-ready personal letters"  that cover the period from the late 1940s to the early 1960s. Interspersed with these are archival clips from the HUAC hearings led by Senator Joseph McCarthy, footage from home movies, and "exceptionally well-selected interview clips with Trumbo".   

==Reception==
  rated the film 71/100 based on 18 reviews. 

==References==
 

==External links==
*  
*  

 

 
  
 
 
 
 
 
 


 
 