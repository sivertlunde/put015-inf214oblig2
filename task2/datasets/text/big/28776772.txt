The Voyage (film)
 
 
{{Infobox film
| name           = The Voyage
| image          = The Voyage (film).jpg
| caption        = Film poster
| director       = Vittorio De Sica
| producer       = Carlo Ponti
| writer         = Diego Fabbri Massimo Franciosa Luisa Montagnana Luigi Pirandello
| starring       = Sophia Loren Richard Burton
| music          = 
| cinematography = Ennio Guarnieri
| editing        = Franco Arcalli
| studio           =C.A.P.A.C Compagnia Cinematografica Champion
| distributor    = Les Productions Artistes Associés (France) United Artists (US) EuroVideo (Germany, DVD) MGM (USA, DVD)
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Voyage ( , and also released as The Journey) is a 1974 Italian drama film directed by Vittorio De Sica. It was De Sicas final film.   

==Plot==
Adriana De Mauro (Sophia Loren) loves Cesar Braggi (Richard Burton), but Cesar, honoring his fathers dying wish, allows his brother, Antonio (Ian Bannen), to marry Adriana. As fate wills, Antonio dies in an automobile accident. Adrianas mourning for Antonio ends when Cesar steps in to rekindle her lust of life. Soon, Adriana begins having dizzy spells. Cesar helps her to a specialist, and the diagnosis is not good. She has an incurable disease. For the rest of their time together, Cesar woos Adriana and eventually proposes to her on a gondola. Yet, Signora De Mauro (Barbara Pilavin), Adrianas mother, is not pleased with the relationship and argues bitterly with Cesar and stands in the way.

==Cast==
* Sophia Loren as Adriana de Mauro
* Richard Burton as Cesare Braggi
* Ian Bannen as Antonio Braggi
* Barbara Pilavin as Adrianas Mother
* Renato Pinciroli as Dr. Mascione
* Daniele Vargas as Don Liborio, Lawyer
* Sergio Bruni as Armando Gill
* Ettore Geri as Rinaldo
* Olga Romanelli as Clementina
* Isabelle Marchall as Florist
* Riccardo Mangano as Dr. Carlini
* Annabella Incontrera as Simona

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 