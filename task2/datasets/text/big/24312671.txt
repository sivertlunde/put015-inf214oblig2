Midnight Whispers (film)
 
 
{{Infobox film
| name           = Midnight Whispers
| image          = 
| image_size     = 
| caption        = 
| director       = David Lai Michael Mak
| producer       = 
| writer         = 
| narrator       = 
| starring       = Moon Lee Josephine Koo
| music          = 
| cinematography = 
| editing        =  Golden Harvest
| released       = 1986 
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Hong Kong melodramatic film starring Josephine Koo and Moon Lee. The film reportedly sat on the shelf for over a year before finally being released in 1988.

==Alternate versions==
This film has at least two version in every of its release:

*In the original theatrical release and first video release available in Hong Kong during the late 1980s-early 1990s, there were scenes involving real life radio personality Pamela Peck who plays herself as the host of a popular late-night radio call-in program.

*In the DVD edition, any scenes with real life radio personality Pamela Peck present in the theatrical version were absences and replaced with an alternate cut.

==External links==
* 
*  
*  
*   at Hong Kong Cinemagic

 
 
 


 