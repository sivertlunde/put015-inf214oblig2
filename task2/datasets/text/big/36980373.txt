Why Girls Go Back Home
{{infobox film
| name           = Why Girls Go Back Home
| image          = 
| imagesize      =
| caption        =
| director       = James Flood
| producer       = Warner Brothers
| writer         = Catherine Brody (screen story) Walter Morosco (adaptation) Sonya Hovey (scenario)
| starring       = Patsy Ruth Miller Clive Brook
| music          =
| cinematography = Charles Van Enger
| editing        =
| distributor    = Warner Brothers
| released       =   reels (5,262 feet)
| country        = United States
| language       = Silent (English intertitles)

}} lost   1926 silent film comedy produced and distributed by Warner Bros. James Flood directed and Patsy Ruth Miller and Clive Brook starred. Myrna Loy has a feature role. The film is a sequel to Warner Bros.s 1921 Why Girls Leave Home, which was a box office hit.   

==Plot==
Marie Downey (Patsy Ruth Miller), a trusting country girl falls in love with a touring stage-actor, Clifford Dudley (Clive Brook) as his touring troupe takes up residence in the hotel run by Maries father. Both lovestruck and stagestruck, Marie follows Clifford to old Broadway, where she ends up getting a job as a chorus girl. She tries desperately to get in touch with Clifford, but he acts as if he does not even know shes alive as he becomes a matinée idol on Broadway. Thanks to a lucky break, Marie becomes the star of the show in which she is appearing, whereupon Clifford finally acknowledges her existence. This time, however, she gives Clifford the cold shoulder then turns her back on New York and heads home (hence the title). Clifford follows her on the train, setting the stage for a tender reconciliation.

==Cast==
*Patsy Ruth Miller - Marie Downey
*Clive Brook - Clifford Dudley
*Jane Winton - Model
*Myrna Loy - Sally Short George OHara - John Ross
*Joseph J. Dowling - Joe Downey
*Virginia Ainsworth - Crook in Badger Game
*Brooks Benedict - Crook in Badger Game
*Herbert Prior - Crook in Badger Game

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 