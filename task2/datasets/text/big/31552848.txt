Amrit (film)
{{Infobox film
| name           = Amrit
| image          = Amritfilmkk.jpg
| caption        = 
| director       = Mohan Kumar R.Verman Shetty (Art Director)
| producer       = Mohan Kumar
| story          = Mohan Kumar
| starring       = Rajesh Khanna Smita Patil Aruna Irani Shafi Inamdar Satish Shah Zarina Wahab Mukri Sujit Kumar Daljit Kaur
| music          = Laxmikant Pyarelal Anand Bakshi (Lyrics)
| cinematography = V. K. Murthy
| distributor    = 
| released       =   
| runtime        = 190 minutes
| country        = India Hindi
| budget         = 
| gross          =  9.3 crores
}}
Amrit is a 1986 Indian Hindi film starring Rajesh Khanna in the title role playing the character of Amritlal Sharma. The film was directed by Mohan Kumar immediately after  directing  Avtaar with Khanna in the title role and lyrics written by Anand Bakshi. This movie gave Rajesh Khanna his fourth BJFA Best Actor award in 1987.  The film was critically acclaimed and received five stars in Bollywood Guide Collections.

==Synopsis==
Amrit (Rajesh Khanna) stays with his only son and meanwhile Kamla (Smita Patil) a widow stays with her only son at Kamal Niwas. But Kamla is treated as servant in her own house and the only person who loves her in the house is her granddaughter Sunita. Amrit has the habit of taking his grandson to the nearby garden when his grandson is free from studies. Amrit learns about Kamla’s problems due to him meeting her coincidentally quite regularly in the garden. Amrit and Kamla meet up to develop a  good relationship. One fine day Kamla gets late to pick up her granddaughter Sunita  from the school, as the auto rickshaw she was travelling to reach school, meets with an accident. Amrit, meanwhile goes to pick up his grandson Rahul, comes to know of this and brings Sunita to her house. Amrit sees  Kamla being humiliated by her daughter-in-law when he goes to their house to nurse Kamla’s injury. Amrit decides to donate some money to Brahmins on the occasion of his son’s birthday but he does not have money and so asks his son to give him few bucks. amrit and kamla watch tv with sunita and rahul upon this amrit sons get angry and humiliate them, his son throws him out of the house permanently. In the torrential rains, the hurt Amrit meets with the accident. Kamla, then with help of like minded people, admits Amrit in a hospital. The rest of the story is about how Amrit and Kamla  decide to separate from their respective selfish children and spend the rest part of life together and how society perceives their togetherness.

==Reception==
The film had collections worth gross of 9.3 crores at the box office in 1986 in India. It received five stars in the Bollywood guide Collections.

==Cast==
*Rajesh Khanna  as  Amrit
*Smita Patil  as  Kamla Srivastav
*Aruna Irani  as  Hasinabanu Ali
*Shafi Inamdar  as  Advocate Sharafat Ali
*Satish Shah  as  Ramcharan Yadav
*Sujit Kumar  as  Doctor
*Mukri  as  Kamru
*Zarina Wahab  as  Savitri Sharma
*Rishabh Shukla  as  Virendra Srivastav

==Music==
{{Track listing
| extra_column = Singer(s)
| all_lyrics   = Anand Bakshi
| all_music    = Laxmikant–Pyarelal
| title1       = Duniya Mein Kitna Gham Hai | extra1 = Mohammad Aziz, Anuradha Paudwal
| title2       = Jeevan Sathi Saath Mein Rahna | extra2 = Manhar Udhas, Anuradha Paudwal Jaspal Singh, Mahendra Kapoor
| title4       = Zindagi Kya Hai Ik Lateefa Hai | extra4 = Kishore Kumar, Chorus
}}

==References==
 

== External links ==
*  

 
 
 
 
 