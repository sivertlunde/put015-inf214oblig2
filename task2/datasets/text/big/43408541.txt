The Marine 4: Moving Target
{{Infobox film
| name           = The Marine 4: Moving Target
| image          = The Marine 4 Artwork.jpg
| size           = 
| alt            =
| caption        = Film poster
| director       = William Kaufman
| producer       = Michael J. Luisi
| writer         = Alan B. McElroy
| based on       =  
| starring       = {{plainlist| Mike "The Miz" Mizanin
* Summer Rae
* Melissa Roxburgh
* Josh Blacker
* Matthew MacCaull
}}
| music          = Jeff Tymoschuk
| cinematography = Mark Rutledge
| editing        = Paul Martin Smith
| studio         = WWE Studios  (Marine 4 Films, Inc.) 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $1.95 million   
| gross          = 
}} Mike "The The Marine film series, and the second to include Mizanin. The film was released digitally on April 20, 2015 and released direct-to-video|direct-to-DVD and Blu-ray on April 21, 2015. 

==Synopsis==
Jake Carter is assigned to protect a "high-value package", a beautiful whistleblower|whistle-blower who wishes to expose a corrupt military defense contractor. However, the military hires a heavily armed team of mercenaries to put an end to her life, along with anyone else who gets in their way, and its up to Carter to stop them at any cost.

==Cast== Mike "The Miz" Mizanin as Jake Carter
* Summer Rae as Rachel Dawes
* Melissa Roxburgh as Olivia Tanis
* Josh Blacker as Andrew Vogel
* Matthew MacCaull as Ethan Smith
* Paul McGillion as Det. Redman
* Roark Critchlow as Nate Miller

==Production==
In February 2014, The Miz was announced to reprise his role of Jake Carter in the third sequel to The Marine and would be co-starring with Summer Rae, who became the first ever WWE Diva to appear in a WWE Studios film.   Filming started in April 2014  in Squamish, British Columbia and with principal photography ending in May 2014.  

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 