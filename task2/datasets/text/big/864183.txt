Indecent Proposal
 
{{Infobox film
| name           = Indecent Proposal
| image          = Indecent_proposal.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Adrian Lyne
| producer       = Sherry Lansing
| screenplay     = Amy Holden Jones
| based on       =  
| starring       = Robert Redford Demi Moore Woody Harrelson Oliver Platt Seymour Cassel John Barry
| cinematography = Howard Atherton
| editing        = Joe Hutshing
| distributor    = Paramount Pictures
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $38 million
| gross          = $266,614,059
}}

Indecent Proposal is a 1993 drama film based on the novel of the same name by Jack Engelhard. It was directed by Adrian Lyne and stars Robert Redford, Demi Moore, and Woody Harrelson.  

==Plot== Las Vegas, hoping they can win enough money to finance Davids fantasy real estate project. They place their money on red in roulette and lose.

After gambling away all of their savings, they encounter billionaire John Gage (Robert Redford). Gage is attracted to Diana and offers David one million dollars to spend a night with her. After a difficult night, David and Diana decide to accept the offer, and a contract is signed the next day. Gage flies Diana to a private yacht where he offers her a chance to void the deal and return to her husband if he loses a toss of his lucky coin. Gage calls it correctly and she spends the night with him.

Although he had hoped to forget the whole incident, David grows increasingly insecure about his relationship with Diana, consumed with a fear that she remains involved with Gage; this insecurity is heightened by the fact Diana discovers that Gage has bought their home/property while it was going into foreclosure. As tension between them builds, David and Diana separate.

Gage renews his advances on Diana. Although she initially resists, Diana eventually consents to spending time with him and a relationship develops. David, meanwhile, hits rock bottom and then slowly pulls his life back together. When Diana files for divorce, David signs the divorce papers and gives the million dollars away. This is significant because the movie opens with a voiceover from Diana saying "if you love something, set it free....".

Diana tells Gage "we need to speak". Gage, perhaps sensing whats coming, recognizes that, even if Diana stayed with him, their relationship would never achieve the intensity she had with David. Realizing that she longs to return to her husband, Gage makes up a story that she was only the latest in a long line of "million-dollar girls". Diana understands that Gage is doing this to make it easy for her to leave. Gage gives her his lucky coin, which is revealed to be double sided. She returns to the pier where David proposed, finding him there waiting. They join hands.

==Cast==
* Robert Redford as John Gage 
* Demi Moore as Diana Murphy
* Woody Harrelson as David Murphy
* Seymour Cassel as Mr. Shackleford
* Oliver Platt as Jeremy
* Billy Bob Thornton as Day Tripper
* Rip Taylor as Mr. Langford
* Billy Connolly as Auction M.C.
* Tommy Bush as Davids Father
* Sheena Easton Cameo as Herself
* Herbie Hancock Cameo as Himself

==Reception==

===Box office===
The film was a box office success, earning $106,614,059 in the U.S. and $160,000,000 internationally for a worldwide total of over $266,000,000.  

===Critical response===
The film received mixed reviews from critics at the time of its release.  Gene Siskel gave the film thumbs down. Roger Ebert, however, gave it thumbs up on Siskel & Ebert,  and also wrote a positive print review.  The film caused controversy amongst feminists.  Today, it maintains a 36% "rotten" rating at Rotten Tomatoes based on 36 reviews.

Indecent Proposal was nominated for seven Razzie Awards in 1994 including Worst Actor (Robert Redford), Worst Actress (Demi Moore), Worst Director and Worst Original Song (In All the Right Places). It would ultimately win three trophies for Worst Picture, Worst Supporting Actor (Woody Harrelson) and Worst Screenplay. John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

==Differences between novel and film==
Engelhards novel contained cultural friction that the screenwriter left out of the movie: the main character, named Joshua, is Jewish, and his billionaire foil is an Arab. In a review of the novel, The New York Times summarized its themes as "the sanctity of marriage versus the love of money, the Jew versus significant non-Jews such as shiksas and Sheikh|sheiks, skill versus luck, materialism versus spirituality, Israel versus the Arab countries, the past versus the future, and the religious world versus the secular one." 

==Soundtrack== Sade was also prominently featured in film, though it was not included on its soundtrack album.

{{Track listing
| extra_column    = Producer(s)
| writing_credits = yes
| headline = Indecent Proposal: Music from the Original Motion Picture Soundtrack

| title1          = Im Not in Love
| note1           = The Pretenders
| writer1         = Graham Gouldman, Eric Stewart
| extra1          = Trevor Horn
| length1         = 3:50

| title2          = What Do You Want the Girl to Do
| note2           = Vince Gill featuring Little Feat
| writer2         = Allen Toussaint Tony Brown
| length2         = 5:07

| title3          = If Im Not in Love With You
| note3           = Dawn Thomas
| writer3         = Thomas
| extra3          = Scott Sheriff
| length3         = 3:38

| title4          = Out of the Window Seal
| writer4         = Seal
| extra4          = Horn
| length4         = 5:35

| title5          = Will You Love Me Tomorrow
| note5           = Bryan Ferry
| writer5         = Gerry Goffin, Carole King
| extra5          = Robin Trower
| length5         = 4:15

| title6          = The Nearness of You
| note6           = Sheena Easton
| writer6         = Hoagy Carmichael, Ned Washington
| extra6          = Patrice Rushen
| length6         = 3:16

| title7          = In All the Right Places
| note7           = Lisa Stansfield John Barry , Stansfield, Ian Devaney, Andy Morris
| extra7          = Devaney
| length7         = 5:42

| title8          = Instrumental Suite from Indecent Proposal
| note8           =
| writer8         = Barry
| extra8          = Barry
| length8         = 25:20

| title9          = A Love So Beautiful
| note9           = Roy Orbison
| writer9         = Jeff Lynne, Orbison
| extra9          = Lynne
| length9         = 3:31
}}

{| class="wikitable sortable"
|-
!Chart (1993)
!Peak position
|- Dutch Albums Chart  71
|- Billboard 200|US Billboard 200  137
|-
|}

==Cultural references==
The 2002 episode "Half-Decent Proposal" of The Simpsons parodies and follows the story of the film loosely. 

The episode "Conference" of UK comedy series Peep Show references the central concept of the film, where one of the Marks bosses offers Jeremy £530 to spend the night with Jeremys past girlfriend Big Suze.

==See also==
* Lecherous millionaire

==References==
 

==External links==
*  
*  
*  
*  

   
{{Succession box
| title=Golden Raspberry Award for Worst Picture
| years=14th Golden Raspberry Awards
| before=Shining Through
| after=Color of Night
}}
 

 
 
 

 
 
 
 
 
 
 
 
 