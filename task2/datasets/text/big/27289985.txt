State Secret (1950 film)
{{Infobox film
| name           = State Secret
| image          = "State_Secret"_(1950).jpg
| caption        = 
| director       = Sidney Gilliat 
| producer       = Sidney Gilliat Frank Launder
| writer         = Sidney Gilliat
| based on       = novel by Roy Huggins
| starring       = Douglas Fairbanks Jr.  Glynis Johns Jack Hawkins
| cinematography = Robert Krasker
| music          = William Alwyn
| studio = London Films
| distribution   = British Lion Films  (UK)
| released       = 11 September 1950  (UK)
| runtime        = 104 minutes
| country        = United Kingdom English
| gross = £187,022 (UK) 
}} British drama film directed by Sidney Gilliat and starring Douglas Fairbanks Jr., Jack Hawkins, Glynis Johns, Olga Lowe    and Herbert Lom. It was released in the United States under the title The Great Manhunt. 

== Plot ==
John Marlowe (Douglas Fairbanks Jr.) is an American surgeon. He is visiting England when he is decoyed to the mythical European country of Vosnia. He finds that he is to operate on the Vosnian dictator. The dictator dies but is replaced by a look-alike. As one of the few who know, Marlowe is hunted by the countrys secret police.

He flees and seeks the help of Lisa Robinson (Glynis Johns). They are pursued across the country and are on the point of escaping when Lisa is shot. Marlowe could escape but stays. 

The head of the secret police, Colonel Galcon (Jack Hawkins) arranges a shooting accident for Marlowe but, as Marlowe walks outside to his fate, the false dictator is heard making a live speech on the radio. Shots are heard and Marlowe goes back inside the building. Galcon confirms by telephone that the stand-in has been assassinated.

Marlowe and Galcon discuss the situation and Galcon realises that it may be over for him. As the people have see the dictator die, it is no longer necessary to have a cover-up. Marlowe and Robinson are released and fly to freedom.

==Cast==
* Douglas Fairbanks Jr. as Doctor John Marlowe
* Jack Hawkins as Colonel Galcon
* Glynis Johns as Lisa Robinson
* Walter Rilla as General Nivo
* Karel Stepanek as Doctor Revo
* Herbert Lom as Karl Theodor
* Hans Olaf Moser as Sigrist
* Guido Lorraine as Lieutenant Prachi Robert Ayres as Arthur J. Buckman
* Howard Douglas as Clubman  
* Martin Boddey as Clubman  
* Russell Waters as Clubman  
* Arthur Howard as Clubman
* Carl Jaffe as Janovic Prada
* Gerard Heinz as Tomasi Bendel

==Critical reception==
In The New York Times, Bosley Crowther wrote, "this picture is just about as lively as they come, and under Mr. Gilliats direction, it moves like an auto gaining speed...Beautifully photographed in Italian cities and in the Italian Dolomites, the whole adventure has the eminent advantage of a sparkling Continental atmosphere. And it also has the advantage of good performance by all concerned—by Mr. Fairbanks as the kidnapped surgeon, looking a little like Eugene ONeill; by Miss Johns, very saucy and explosive, as the music-hall girl; by Jack Hawkins as the Vosnian premier, with an Oxford accent and a Nazi attitude; by Herbert Lom as the Balkan shyster and any number of others in small roles."  {{cite web|url=http://www.nytimes.com/movie/review?res=9405E2D9133EEF3BBC4D53DFB667838B649EDE|title=Movie Review -
  The Great Manhunt - THE SCREEN IN REVIEW; State Secret, Thrilling Chase Film With Douglas Fairbanks Jr., Bows at Victoria - NYTimes.com|publisher=}} 

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 

 
 