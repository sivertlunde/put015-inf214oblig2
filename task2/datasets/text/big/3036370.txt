Shadow Man (2006 film)
{{Infobox film
| name           = Shadow Man
| image          = ShadowMan.2006.StevenSeagal.png
| image_size     = 
| caption        = Official movie poster
| director       = Michael Keusch
| producer       = Steven Seagal Pierre Spengler Andrew Stevens
| writer         = Steven Collins Joe Halpin Steven Seagal
| starring       = Steven Seagal Eva Pope Imelda Staunton Garrick Hagon
| music          = Barry Taylor
| cinematography = Geoffrey Hall
| editing        = Andy Horvitch
| studio         = Clubdeal Steamroller Productions
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 95 minutes
| country        = United States United Kingdom Romania
| language       = English
}} thriller film directed by Michael Keusch, and also written and produced by Steven Seagal, who also starred in the film. The film co-stars Eva Pope, Imelda Staunton and Garrick Hagon. The film was released on direct-to-video|direct-to-DVD in the United States on June 6, 2006.

==Plot==
 
Widowed former CIA agent Jack Foster (The Shadow Man) is an enigmatic Fortune 500 business owner, and is the father of an 8-year-old daughter named Amanda. Its the anniversary of the death of Jacks wife, and Jack is taking Amanda to Romania, which is the birthplace of Amandas mother, who died 5 years ago. But upon arrival at the airport in Bucharest, Romania, a car bomb blows up his CIA agent father-in-law Georges limo, and Amanda is kidnapped. In the chaos, Jack reunites with Harry, a former CIA buddy who has mysteriously reappeared after not seeing Jack for about 7 or 8 years. They take off to track Amanda down, but they lose her—and Jack is suspicious of Harry.

At a safe house, Harry leaves Jack locked in a secured room, and sends four men after Jack. Jack kills them, and then he escapes. Outside, Jack forces Harry to get inside of an SUV. Harry explains that it has to do with a biological weapon called MK Ultra. Its a virus that causes the infected person to come down with things like influenza, various forms of cancer, you name it. The infected died in 6 months to a year, and the virus is untraceable. George had the formula—he lifted it from the Black Ops section. He was planning on selling it to the highest bidder—the FSB, a new branch of the KGB. And now the FSB thinks George slipped the formula to Jack. Harry tells Jack that the FSB doesnt intend to let Amanda go until they get the formula.

Jack puts a gun to Harrys head, and gives Harry two hours to find Amanda. They go to the U.S. Embassy, where he talks to Ambassador Cochran, who wants him to find Amanda. Outside, two Romanian cops named Seaka and Urich put Jack in a car, and take him to the station. They introduce Jack to a woman named Anya, and Jack recognizes Anya as the woman who kidnapped Amanda. Jack is forced to beat up Seaka and Urich while Anya escapes. Later, Jack goes to a building where he is to meet with Harry, who has two men named Schmitt and Chambers with him. Chambers kills Harry, and Jack kills Chambers and exchanges gunfire with Schmitt, who escapes. At the place where Amanda is being held, Anya and Amanda talk, with Anya explaining that she took Amanda in order to protect her. Anya explains that her husband and daughter died three years ago while she was an intelligence agent in England. Her husband and her daughter were killed by people from her own agency.

Since Anya is a cab driver, Jack has a cab driver take him to Club Lido. Seaka and Urich get wind of it, and decide to send a couple of patrol cars to Club Lido. At the club, Jack sees Anya, and he confronts her in the womens restroom as Seaka, Urich, and the patrol cars arrive. Anya pulls a gun and demands a passport and passage to America in exchange for Amanda. Jack takes Anya to a different room, ties her to a chair, and demands answers. Schmitt arrives at the club and runs the cops off. A man named Jensen enters the room just as Jack hides, and demands to know where Amanda is, and then he wants to know where Jack is. Jensen and one of his men start looking for Jack, while Jensens other two men stay with Anya. Jack and Anya beat up the two men, and head to Anyas car.

Seaka and Urich go to Anyas apartment, and tear the place apart, finding no one there. After Seaka and Urich leave, Jack and Anya arrive, and see a patrol car outside the building. They go around back. Once inside, Anya grabs some documents, and they ditch the two cops. What Jack and Anya dont know is that Schmitt is working for a corrupt CIA agent named Waters, who wants to sell the MK Ultra to the highest bidder. Seaka and Urich want to get their hands on the MK Ultra for the same reason. And it was Schmitt who oversaw the bombing of Georges limo. Jack and Anya lie low for a night at a large house. On the next day, Schmitt and one of his men track down Cyrell, a wheelchair-using man who is watching over Amanda for Anya. Not long after, Seaka and Urich get there, and a few minutes later, Jack and Anya get there.

Jack and Anya go inside and find Cyrell dead. Amanda is not there. But Jack does find Amandas backpack. Seaka and Urich come in, and Seaka grabs Anya and puts a gun to her head. Seaka tells Jack to give him the MK Ultra, or Anya dies. Jack shoots Seaka and Urich, and Jack and Anya escape. Seaka dies, and Urich calls Jensen and asks for a meeting on the roof of a parking garage. Back at the large house, Jack calls his CIA friend Rogers, who is a hacker. Jack asks Rogers to hack into the MK Ultra project and stop it for good. On the roof of the parking garage, Urich meets with Jensen, the man that Urich and Waters each want to sell the MK Ultra to. Jensen and his man start beating Urich up.

Anya calls Jensens cell phone number. As part of a plan to set Jensen up, she tells him that Jack is meeting with Waters and Schmitt in the central library. After they hang up, Jensens man kills Urich. Just as Jack and Anya are leaving the large house, a man in a helicopter opens fire on them. The man is working for Waters. Jack and Anya run back inside and exit out the side door to Anyas cab. As they leave, the helicopter gives chase, and Jack and Anya hide the car in some woods. Jack gets out and fires several shots, causing the helicopter to explode. At the embassy, Jack waits for Ambassador Cochran in her office. Jack tells her to tell Waters to meet with him at the Central Library. At the library, Jack sees Schmitt and Waters.

Jack sits down and tells Waters that Waters wont get the MK Ultra formula until he (Jack) sees Amanda. Waters has one of his men show her to him on a balcony above. Water tells Jack that he has 60 seconds to give up the formula, or Amanda will be thrown off the balcony. All of a sudden, Jack fatally shoots the man who is holding Amanda. Schmitt tries to shoot Anya. Jack grabs Waters hand, and forces Waters to fatally shoot Schmitt. Jack then fires five shots, killing Waters. As Anya tries to get Amanda to safety, Jensens two henchmen open fire on Jack. Jack kills one of them, and then confronts Jensen and his other henchman. Jack grabs Jensens gun hand and causes Jensen to fire two shots, killing the henchman. Then Jack puts Jensens eyes out, leaving him to die. As Jack leaves, George steps in front of him with a gun.

George, who is the man behind everything, wasnt in the limo when it exploded. Jack kills George by using a martial arts move, a special hit to the chest, that sends George backwards into a wall, and there is blood on the wall as George slumps down. Rogers hacks into the MK Ultra program and puts an end to the project, and Jack is reunited with Amanda. Later, Ambassador Cochran gives Jack a passport to give to Anya so she can go to America. After Jack and Amanda get back to the USA, Jack gets Amanda a horse for being so brave in Romania.

==Cast==
* Steven Seagal as Jack Foster
* Eva Pope as Anya
* Imelda Staunton as Ambassador Cochran
* Vincent Riotta as Harry
* Michael Elwyn as George
* Skye Bennett as Amanda Foster
* Garrick Hagon as Waters
* Alex Ferns as Schmitt
* Michael Fitzpatrick as Chambers
* Elias Ferkin as Velos
* Levani Uchaneishvili as Jensen
* Zoltan Butuc  as Seaka
* Emanuel Parvu as Urick
* Vincent Leigh as Roger
* Werner Daehn as Cyrell

==Critical reviews==
David Johnson of DVD Verdict called the film "a generic action film with an uninvolving plot, stilted action sequences,   some shabby green screen work, predictable twists," adding: "Seagal doesnt distinguish himself from any other character hes ever played and theres not enough interesting stuff happening on the periphery to distract us."  J. Andrew Hosack of   author Vern ranks it among Seagals worst films, explaining that, "SHADOW MAN, I’m sorry to say, is the most boring movie Seagal has made so far," and criticizing the extensive use of body doubles and incoherent plot.   

==Production==
 Into the Sun, and Today You Die). It was re-titled Shadows of the Past, and the plot assumed its current form. These changes were met with disappointment from some fans, who were excited to see Seagal star in a film that was a departure from the action movies he had done in recent years.

The film is set in Bucharest, Romania, and was filmed there in 49 days from July 13 to August 31, 2005.
 Academy Award-nominated performance in Vera Drake, appears in the film as Ambassador Cochran. She explained to the BBCs Mark Lawson that she filmed for two days, but that Seagal did not film any reverses with her, resulting in her filming all her scenes with Seagals stand-in and a Romanian film student, who would read Seagals dialogue off-camera. She claims she did the film for money, and as a chance to portray a character with an American Accent. {{cite AV media
 | people = Mark Lawson
 | title = Mark Lawson Talks to... Imelda Staunton.
 | medium = television
 | publisher = BBC four
 | location = England
 | date = March 19, 2010
 | url =http://www.bbc.co.uk/programmes/b00p50nr }} 

==Home media== Region 1 in the United States on June 6, 2006, and also Region 2 in the United Kingdom on 30 October 2006, it was distributed by Sony Pictures Home Entertainment.

==See also==
*Shadow Man (1988 film), a Dutch-British film of the same name made in 1988, starring Tom Hulce.

==References==
 

==External links==
* 

 
 
   
   
 
 
 
 