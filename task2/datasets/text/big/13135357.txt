The Cassandra Cat
{{Infobox film
| name           = The Cassandra Cat
| image          = Az prijde kocour dvd LRG.jpg
| image_size     = 
| caption        = 
| director       = Vojtěch Jasný
| producer       = Jaroslav Jílovec
| writer         = Jiří Brdečka Vojtěch Jasný Jan Werich
| narrator       = 
| starring       = Jan Werich Vlastimil Brodský Emília Vášáryová Jiří Sovák Vladimír Menšík
| music          = Svatopluk Havelka
| cinematography = Jaroslav Kučera
| editing        = Jan Chaloupek
| distributor    = Ústřední půjčovna filmů
| released       =   (1963 Cannes Film Festival|Cannes)    (Czechoslovakia)
| runtime        = 91 mins or 101 mins
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1963 Czechoslovakian film directed by Vojtěch Jasný. 

It tells the story of a mysterious traveling circus that arrives in a village accompanied by a sunglass-wearing cat named Mokol. When the cats glasses are removed, people in the village appear bathed in different colours that reflect their true feelings.
 Cannes Film Special Jury Prize, Cannes.   

==Cast==
* Jan Werich - Magician / Oliva
* Emília Vášáryová - Diana (as Emilie Vasáryová)
* Vlastimil Brodský - Teacher Robert
* Jirí Sovák - School director
* Vladimír Mensík - School janitor
* Jirina Bohdalová - Julie
* Karel Effa - Janek
* Vlasta Chramostová - Marjánka
* Alena Kreuzmannová - Gossip
* Stella Zázvorková - Ruzena
* Jaroslav Mares - Restaurant owner
* Jana Werichová - Chairmans wife
* Ladislav Fialka - Stealer
* Karel Vrtiska - Miller
* Václav Babka - Policeman

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 
 