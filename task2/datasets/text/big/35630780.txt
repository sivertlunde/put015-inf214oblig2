Turn Me On, Dammit!
{{Infobox film
|name=Turn Me On, Dammit!
|image=Turn_Me_On,_Dammit_Poster.jpg
|alt=
|caption=
|director=Jannicke Systad Jacobsen
|producer=Brede Hovland
|writer=Jannicke Systad Jacobsen  based on= 
|starring=
|music=Ginge Anvik
|cinematography=Marianne Bakke
|editing=Zaklina Stojcevska
|studio=Motlys
|distributor=
|released= 
|runtime=76 minutes
|country=Norway
|language=Norwegian
|budget=
|gross=$126,085 
}} Norwegian Coming-of-age story|coming-of-age comedy film premiered in 2011. It is based on Olaug Nilssen’s novel of the same name.  Set in Skoddeheimen, a fictional small town in western Norway,  the film is about Alma (Helene Bergsholm), a 15 year old girl and her sexual awakening.

==Plot== 
Alma is addicted to telephone sex hotlines. She secretly feels for Arthur, an equally shy teenager. One day he approaches her in a very bizarre manner. She tells her best friends about this and her story makes her the talk of the town. When Arthur denies the episode, shes subsequently considered a slandering crank and becomes an outcast.

==Cast==
*Helene Bergsholm as Alma 
*Malin Bjørhovde as Sara
*Beate Støfring as Ingrid
*Matias Myren as Artur
*Lars Nordtveit Listau as Kjartan
*Henriette Steenstrup as Almas mother
*Jon Bleiklie Devik as Sebjørn
*Julia Bache-Wiig as Maria
*Finn Tokvam as Math teacher
*Per Kjerstad as Stig (voice)

==Reception==
===Critical response===
Turn Me On, Dammit! received generally positive reviews. Based on 39 reviews collected by Rotten Tomatoes, the film holds an 92% "Fresh" rating and an average score of 7.4 out of 10. 

===Accolades===
*Screenplay award at the 2011 Tribeca Film Festival 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 