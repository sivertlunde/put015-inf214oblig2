The Land Has Eyes
{{Infobox film
| name           = The Land Has Eyes
| image          = 
| image_size     = 
| caption        = 
| director       = Vilsoni Hereniko
| producer       = Jeannette Paulson Hereniko Corey Tong Vilsoni Hereniko
| writer         = Vilsoni Hereniko
| narrator       = 
| starring       = Sapeta Taito Rena Owen
| music          = Clive Cockburn Audy Kimura Paul Atkins
| editing        = Jonathan Woodford-Robinson
| distributor    = PBS other international
| released       = 2004
| runtime        = 87 minutes
| country        = Fiji Rotuman English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Fiji Islander film written and directed by Vilsoni Hereniko. It is the first ever (and so far only) feature film from Fiji. 

==Plot==
The main character, Viki (Sapeta Taito), is a young Rotuman woman shamed as the daughter of a man wrongly accused of being a thief. She finds inspiration in a mysterious "Warrior Woman" (Rena Owen) from her peoples legends.    

==Cast==
*Sapeta Taito as Viki 
*Rena Owen as Warrior Woman 
*Voi Fesaitu as Hapati 
*Elisapeti Kafonika Inia as Mata 
*John Fatiaki as Noa 
*Ritie Titofaga as Maurea 
*James Davenport as Judge Clarke 
*Maniue Vilsoni as Koroa 
*Sarote Fonmanu as Rako / Teacher 
*Emily Erasito as Hanisi - Vikis sister

==Production==
The film was shot on  . The film also stars John Fatiaki as a corrupt court official.

==Reception==
The Land Has Eyes was presented at the Sundance Film Festival in 2004, as well as a number of other international film festivals.  It was Fijis official submission to the 2006 Academy Awards. 

The Star Bulletin (Hawaii) praised the film, "made on a shoestring budget".  The Honolulu Advertiser noted that it "received an enthusiastic reception from the audience" at the Sundance Festival.  A reviewer for the Advertiser explained that "All but two cast members were recruited from Rotuma. Most had never seen a movie before, much less acted in one. When the film was finished, Hereniko brought it back to Rotuma, borrowed a sheet from the hospital to use as a screen, and showed it eight times around the island." The reviewer recommended the film, and concluded:
:"Go see this movie if you have ever cringed over an outsiders misinterpretation of island culture. Go see this movie if you have ever cried over the systemic injustice visited upon poor, honest families. Go see this movie if your heart needs to hear that justice can prevail and that a force greater than all of us is watching and keeping score. Go see this movie if you want to be reminded that hard work leads to success. Go see this movie." 

A reviewer from the Seattle Weekly wrote:
:"The film is weak in conventional narrative and production values.   Yet that very authenticity is what gives Land its power. Hereniko captures the everyday texture of Polynesian life: the clan connections that make nobody want to rock the boat  , the tension between pagan and Christian beliefs, the power of ancient stories and rituals.   Land reminds us of the power of film to take us to another world." 

The Land Has Eyes received the "Best Overall Entry" award at the 2005 Wairoa Maori Film Festival, and the "Best Dramatic Feature" award at the 2004 Toronto ImagineNative Film & Media Arts Festival. 

==References==
 

==External links==
*  
*  
*  
*  
*  , Josephine Bridges, The Asian Reporter, April 12, 2005 (review)

 
 
 
 
 
 