Assignment in Brittany
{{Infobox film
| name           = Assignment in Brittany
| image_size     = 225px
| image	         = Assignment in Brittany FilmPoster.jpeg
| caption        = Theatrical poster Jack Conway
| writer         = Anthony Veiller William H. Wright Howard Emmett Rogers
| based on       = Helen MacInnes novel Cross Channel
| starring       = Jean-Pierre Aumont Susan Peters
| music          = Lennie Hayton
| cinematography = Charles Rosher Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Assignment in Brittany is a 1943   

==Plot== occupied France due to his striking resemblance to Bertrand Corlay, a suspected Nazi collaborator being held prisoner by the British.

At the Corlay family farm, Madame Henriette Corlay (Margaret Wycherly), Bertrands mother, eventually realizes Matard is not her son, but does not give him away, as she disapproves of Bertrands cooperation with the Germans. Matard has better luck fooling both Anne Pinot (Susan Peters) and Elise (Signe Hasso), Bertrands fiancee and mistress respectively.

After French Resistance contact Louis Basdevant is unmasked as a turncoat, Matard forces him at gunpoint to get him into a heavily guarded town. When the masquerade is uncovered, Matard shoots Basdevant and a German officer and escapes by jumping in the harbor. There, he witnesses a U-boat entering a hidden pen.
 William Edmunds) to transmit on his wireless, but the man is caught. Matard is also taken. Though a number of civilians refuse to betray him (and are shot), Plehecs young nephew Etienne (Darryl Hickman) identifies him. When Matard refuses to tell Captain Hans Holz (George Coulouris) the location of the wireless, he is tortured for days. Schoolteacher and Resistance member Jean Kerenor (Richard Whorf) and other villagers manage to free him.

Hidden in a secret room beneath the church, he is nursed back to health by Anne. She, knowing his true identity, admits she loves him, much to his delight. Etienne shows up, telling Matard that his uncle had instructed him to do whatever he had to in order to protect the wireless. The boy takes Matard to it; Matard sends the location to his superiors.

A commando raid is organized, led by Colonel Trane (Reginald Owen). On the way to rendezvous with the British, Matard and Kerenor stumble upon Elise and Holz in each others arms. Furious that he had defended Elise from the charge of collaboration all this time, Kerenor shoots the captain and strangles the woman. After they depart, Madame Corlay arrives, attracted by the sound of gunfire. When she hears German soldiers approach, she grabs the pistol and allows herself to be captured to shield the others.

Matard joins some of the soldiers in the assault on the control room. When they are trapped inside, they are rescued by Kerenor, who attacks the Germans from behind, though he himself is finally killed. The pens are blown up, and the commandos re-embark on their boats. Aboard, Matard finds Anne and Etienne.

==Cast==
  
*Jean-Pierre Aumont as Capt. Pierre Matard / Bertrand Corlay  
*Susan Peters as Anne Pinot
*Margaret Wycherly as Mme. Henriette Corlay
*Signe Hasso as Elise
*Richard Whorf as Jean Kerenor
*George Coulouris as Capt. Hans Holz John Emery as Capt. Deichgraber
*Darryl Hickman as Etienne
 
*Sarah Padden as Albertine
*Adia Kuznetzoff as Louis Basdevant
*Reginald Owen as Col. Trane
*Miles Mander as Col. Herman Fournier
*Alan Napier as Sam Wells
*Odette Myrtil as Louis sister
*Juanita Quigley as Jeannine William Edmunds as Plehec
 

Cast notes:
*Both French actor Jean-Pierre Aumont and Swedish actress Signe Hasso made their American film debuts in this film. 

==Production==
The working title for Assignment in Brittany was Fire in the Night. 

==Reception==
The New York Times review said of the film at the time of its release that it was "an ordinary box-office adventure whose pretensions fall flat." 

==References==
Notes
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 