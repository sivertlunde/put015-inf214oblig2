I Soferina
{{Infobox film
| name           = I soferina Η Σωφερίνα
| image          = 
| image_size     = 
| caption        = 
| director       = Alekos Sakellarios Kostas Lychnaras (crew) Takis Vougiouklakis (crew)
| producer       = 
| writer         = Giorgos Roussos
| starring       = Alekos Alexandrakis Giorgos Pantzas Giorgos Konstantinou Dionysis Papagiannopoulos Vasilis Avlonitis Aliki Vougiouklaki Maro Kontou Kaiti Lambropoulou Kostas Papachristos 
| music          = Gerasimos Lavranos
| cinematography = 
| editing        = 
| distributor    = Damaskinos-Mihailidis, Aigaion Film G. Roussos & Ass. 1964
| runtime        = 98mins
| country        = Greece Greek
}}
 1964 Greece|Greek theatrical comedy film directed by Giorgos Roussos, The Last Joke (Το Τελευταίο Ψέμα = To Telefteo Psema).  The movie was released into cinemas the same as To doloma was released which it had the same protagonistic twin, but with a different company.

The movie acted together for the first and only time in two great star, Aliki Vougiouklaki and Maro Kontou.

== Plot ==
  
Mary is a young woman happily married to Nikos. However, her life is turned upside down when her best friend, Lilly, borrows Marys car to pursue an extramarital fling, only to get involved in a car accident. Mary is drawn to the court, where she has to decide whether to refrain from exposing Lillys infidelities, or sacrifice Lillys confidentiality and save her own marriage in the process...

==General information==
 
*Total number of tickets; 515,625
*Montage: Aristidis Karydis-Fuchs
*Songwriter: Alekos Sakellarios
*Songs sung by: Trio Athene
*Bouzouki solo: Giorgos Zambetas

==Cast==
  
*Alekos Alexandrakis as Nikos Diamantidis
*Giorgos Pantzas as Takis Maragkakis Giorgos Konstantinou as Mihalis Haridimos
*Dionysis Papagiannopoulos as a judge chief
*Vasilis Avlonitis as Nikolaos Spanovangelodimitris-Gylos
*Aliki Vougiouklaki as Mary Diamantidou
*Maro Kontou as Lilly Haridimou
*Kaiti Lambropoulou as Aikaterini Toufexi
*Alkis Giannakas as Pakis P
*Tasos Giannopoulos as Evangelos Fanouridos (police traffic officer)
*Thodoros Katsadramis as Spyros
*Kostas Papahristos as a warden
*Giorgos Velentzas as a photographer
*Costas Hajihristos as an organist
*Joly Garbi as Evterpi Karavangeli
*Kleo Skouloudi as Kalliopi

== References ==
 
 

== External links ==
 
*  
*  
 

 
 
 
 
 


 