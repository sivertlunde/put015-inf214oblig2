Madame Bovary (1937 film)
{{Infobox film
| name = Madame Bovary 
| image =
| image_size =
| caption =
| director = Gerhard Lamprecht
| producer = Gerhard Lamprecht  Hans Neumann
| narrator =
| starring = Pola Negri   Aribert Wäscher   Ferdinand Marian   Werner Scharf
| music = Giuseppe Becce 
| cinematography = Karl Hasselmann
| editing =Gerhard Lamprecht
| studio = Euphono-Film
| distributor = Terra Film
| released = 23 April 1937  
| runtime = 95 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama film directed by Gerhard Lamprecht and starring Pola Negri, Aribert Wäscher and Ferdinand Marian. It is an adaptation of Gustave Flauberts 1857 novel Madame Bovary. 

==Cast==
* Pola Negri as Emma Bovary 
* Aribert Wäscher as Charles Bovary  
* Ferdinand Marian as Rodolphe Boulanger  
* Werner Scharf as Léon Dupuis 
* Alexander Engel as Homais, Apoteker  
* Paul Bildt as Lheureux  
* Olga Limburg as Madame Lefrançois  
* Karl Hellmer as Binet  
* Katharina Brauren as Madame Homais  
* Werner Stock as Justin, Apoteker Helper  
* Carla Rust as Félicité  
* Rudolf Klein-Rogge as Prof. Canivet  
* Eduard von Winterstein as Huret 
* Barbara von Annenkoff as Marquise de Andervillier  
* Georg H. Schnell as Marquis de Andervillier 
* Franz Stein as Herzog de Laverrière  
* Bertold Reissig as Hyppolite  
* Johannes Bergfeldt as Amandé  
* Gerhard Dammann as Robinet, Bauer  
* Angelo Ferrari as Gast bei Marquis Andervillier  
* Robert Forsch as Gast beim Empfang des Marquis  
* Albert Karchow as Bürger von Yonville  
* Alfred Karen as Gast bei Marquis Andervillier  
* Hildegard Kiesewetter as Verkäuferin  
* Adelberg Ludwigshausen as Jean, Diener  
* Edgar Nollet as Gerichtsbeamter 
* Hellmuth Passarge as Monsieur Renard  
* Ingeborg Peter as Mädchen   Klaus Pohl as Gerichtsbeamter  
* Ulla Ronge as Sylvée  
* Lili Schoenborn-Anspach as Fischhändlerin 
* Käte Strebel as Gemüsefrau

== References ==
 

== Bibliography ==
* Donaldson-Evans, Mary. Madame Bovary at the Movies: Adaptation, Ideology, Context. Rodopi, 2009. 
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 