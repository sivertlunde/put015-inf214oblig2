King of the Newsboys
{{Infobox film
| name           = King of the Newsboys
| image          = 
| image_size     = 
| caption        = 
| director       = Bernard Vorhaus
| producer       = Bernard Vorhaus (producer)
| writer         = Louis Weitzenkorn (screenplay) & Peggy Thompson (screenplay) Samuel Ornitz (story) & Horace McCoy (story)
| narrator       = 
| starring       = See below
| music          = Alberto Colombo
| cinematography = Jack A. Marta
| editing        = Ernest J. Nims
| studio         = 
| distributor    = Republic Pictures
| released       = 1938
| runtime        = 65 minutes 69 minutes (DVD version)
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

King of the Newsboys is a 1938 American film directed by Bernard Vorhaus.

== Plot summary ==
 

== Cast ==
*Lew Ayres as Jerry Flynn
*Helen Mack as Mary Ellen Stephens
*Alison Skipworth as Nora
*Victor Varconi as Wire Arno
*Sheila Bromley as Connie Madison
*Alice White as Dolly
*Horace McMahon as Lockjaw
*William Billy Benedict as Squimpy
*Ray Cooke as Pussy
*Jack Pennick as Lefty
*Mary Kornman as Peggy
*Gloria Rich as Maizie
*Oscar OShea as Mr. Stephens

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 