Beowulf (1999 film)
{{Infobox film
| name = Beowulf
| image = Beowulf French poster.jpg
| image_size =
| caption = Beowulf French poster
| director = Graham Baker
| producer =
| writer =
| narrator =
| starring = Christopher Lambert Rhona Mitra Oliver Cotton
| music = Jonathan Sloate Ben Watkins
| cinematography = Christopher Faloona
| editing = Roy Watts
| distributor = Dimension Films Miramax Films
| released =  
| runtime = 95 min.
| country = United States
| language = English
| budget = $20 million (estimated)
| gross =
}}
 Mortal Kombat, which also starred Lambert. 
 Hrothgar has an affair with Grendels mother, and they have a child together, Grendel; Wealhþeow|Hrothgars wife commits suicide).

== Plot ==
The setting is a post-industrial castle that defends the border of an unnamed kingdom. It is terrorized by a demon named Grendel, who kills the castles defenders, one by one. After fighting his way past several soldiers trying to keep anyone from entering or leaving, the warrior Beowulf offers his help to the castles king, Hrothgar, who welcomes his help.

Hrothgar has a daughter named Kyra, who is loved by Roland, the castles strongest soldier, but she does not return his affections. It is revealed that Hrothgars wife and Kyras mother, committed suicide when she found out Hrothgar had an affair. The woman he had an affair with was actually an ancient being who had originally lived on the castles lands. The affair resulted in an offspring, Grendel.

Beowulf and Grendel fight, wounding each other. Later, after recovering, they fight again and this time Beowulf rips Grendels arm off with a retracting cestus. That night the castle celebrates as they believe Grendel is dead. Kyra declares her love for Beowulf and he returns her affection. Kyra tells him that she killed her previous husband after he abused her. Beowulf tells her that his mother is human and his father is Baal, "God of darkness, Lord of lies". This explains his tremendous fighting prowess.

While Kyra is with Beowulf, everyone else in the castle is killed by Grendels mother. Beowulf attacks and kills Grendel and his mother by stabbing them. He then burns her body, while the flames also consume  the castle. Beowulf barely escapes with Kyra. The castle is destroyed, with Beowulf and Kyra the only survivors.

== Cast ==
* Christopher Lambert as Beowulf
* Rhona Mitra as Kyra
* Oliver Cotton as Hrothgar
* Götz Otto as Roland
* Vincent Hammond as Grendel Charles Robinson as Weaponsmaster
* Brent Jefferson Lowe as Will
* Roger Sloman as Karl
* Layla Roberts as Grendels mother
* Patricia Velásquez as Pendra

==Production==
The production was filmed in Romania.

==Reception==
Critical reaction to the film has been highly negative. The general criticisms for the film were the weak script, below-average acting, corny dialogue, derivations from the source material, and over-reliance on camp (style)|camp, although it was hailed for its production design. Danél Griffin of Film as Art said the film "understands that liberties must be taken with the poems characters to create a more cinematic experience, and there are moments that, even in its liberties, it reveals a deep appreciation for the poem, and a profound understanding of its ideas. There are other moments, however, that seem so absurd and outlandish that we wonder if the writers, Mark Leahy and David Chappe, have even read the poem." Griffin added that "Lambert is certainly effective", but concluded that "clever ideas aside, the film is unfortunately mediocre at best. The set design and some of the revised storyline are both stupendous, but the overall experience makes for poor cinema." 

Beyond Hollywoods review said that "genre films dont get any sillier than this", but called the film "above average". The review praised the films "energetic action" and said that it "excels in set design", but added that "the techno (music) is pretty annoying."  Calling the film "a cheesy post-apocalyptic update of the ancient tale", Carlo Cavagna of About Film praised the films action scenes but felt that Lambert and Mitra had no chemistry. 

Nathan Shumate of Cold Fusion Video Reviews also praised the films action scenes, but felt it used all its good ideas in the first half, "leaving most of the rest of the movie to die of attrition." Shumate added, "Thats not to say that there are no effective scenes to be had,   certainly can’t carry the full 90-minute running time". Perhaps its truly impossible to come up with a definitive film version of this epic. But I wouldn’t want to make a judgement on that simply due to this attempts mediocrity. 

== Soundtrack ==

The films soundtrack mainly featured electronic and industrial songs from various artists and original score material by Juno Reactors Ben Watkins.

Despite the many songs used in the film, a soundtrack CD was never issued. 

* Jonathan Sloate - Beowulf
* Front 242 - Religion (Bass under siege mix by The Prodigy)  Pig - No One Gets out of Her Alive, Jump the Gun (Instrumental)  
* Gravity Kills - Guilty (Juno Reactor remix) 
* Juno Reactor - God is God
* Fear Factory - Cyberdyne
* Laughing US - Universe
* KMFDM - Witness
* Lunatic Calm - The Sound
* Junkie XL - Def Beat
* Urban Voodoo - Ego Box
* 2wo - Stutter Kiss
* Spirit Feel - Unfolding Towards the Light
* Mindfeel - Cranium Heads Out
* Frontside - Dammerung
* Praga Khan - Luv u Still Anthrax - Giving the Horns
* Monster Magnet - Lord 13

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 