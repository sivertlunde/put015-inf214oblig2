Maan Karate
{{Infobox film
| name           = Maan Karate
| image          = Maan karate.jpg
| caption        =
| director       = Thirukumaran
| producer       = P. Madhan   AR Murugadoss
| story          = AR Murugadoss
| Genre          = Fantasy
| writer         = R. Senthi Kumar  (Dialogue) 
| screenplay     = Thirukumaran
| starring       = Sivakarthikeyan Hansika Motwani
| music          = Anirudh Ravichander
| cinematography = M. Sukumar
| editing        = Sreekar Prasad
| studio         = AR Murugadoss Productions
| distributor    = Escape Artists Motion Pictures
| released       =  
| runtime        = 159 minutes
| country        = India
| language       = Tamil
| gross     =   

}} 2014 Kollywood|Indian Tamil fantasy film directed by Thirukumaran, a former associate of AR Murugadoss.    It stars Sivakarthikeyan, Hansika Motwani, Soori (actor)|Soori, Vamsi Krishna and Sathish.    Murugadoss wrote the story for the film, which was produced by Escape Artists Motion Pictures and has music by Anirudh Ravichander.The film released on 4 April 2014 to mixed reviews from critics, but was a massive success at the box office, establishing Sivakarthikeyan as a leading hero in Tamil cinema and with a new urban look.

==Plot== IT employees, Satyam Computers) would close down the next day. When this does happen, they accept the credibility of the newspaper and decide to make money through it. While Nikita is reading the paper, she sees an article where one Peter who stays in Royapuram wins a boxing tournament on the day of Ayudha Puja and dedicates the prize money of  2,00,00,000 to the five of them. The IT gang is astounded on reading this news and decide to find this Peter and sponsor him for the boxing tournament, with the hope of getting  2,00,00,000, despite eventually finding out from the boxing association that the cash prize for the winner of the tournament is only  1,00,000.

The IT gang successfully manages to track down Peter (Sivakarthikeyan), but immediately find out that he is not a boxer, but an unambitious youth who just spends his time drinking and roaming around. However, he is in love with a young woman Yazhini Sethuraman (Hansika Motwani), who is a sports buff. To impress Yazhini, he accepts their offer to participate in the tournament. However, Peter does not show any inclination to train for the tournament, instead he uses the IT gang to fulfil his own desires, including winning Yazhinis heart, much to their chagrin. Later, the IT gang find out that there is another boxer named Peter (Vamsi Krishna) who also resides in Royapuram like the Peter they are sponsoring, but is a champion boxer who is even nicknamed as "Peter the Killer" as he had killed several opponents in the ring. Thinking that they are sponsoring the wrong Peter, the IT gang approach "Killer" Peter and offer to sponsor him, but he refuses. They reluctantly decide to continue sponsoring the Peter they are currently sponsoring.
 disqualified or knocked out. The media give a name for his technique: "Maan Karate", which in English means, "running for life". Peter soon becomes known as "Maan Karate" Peter (to distinguish from "Killer" Peter) and becomes an Internet sensation overnight, winning a lot of fans. Due to Peters rising popularity, the prize money is raised from  1,00,000 to  2,00,00,000, which excites the IT gang, who now feel that their dreams will come true with the Peter they are sponsoring after all.

Eventually, Peter enters the final of the tournament, where he is to face "Killer" Peter. Peter becomes upset at this development, fearing that he might be killed by "Killer" Peter due to his lack of boxing skill. He tries to avoid fighting in the final through various means, none of which are successful. In a last-ditch attempt, he tries to convince "Killer" Peter to match fixing|"throw" the match so that he could win and impress Yazhini, but is verbally abused and physically assaulted by him in response. Hurt at the treatment which "Killer" Peter meted out to him, Peter begins to train hard for the final with the sole purpose of defeating "Killer" Peter. Meanwhile, unknown to him, the IT gang who is sponsoring him switch over to "Killer" Peters side after being offered  75,00,000 by "Killer" Peters father-in-law, despite Nikitas reservations.

The final takes place on the day of Ayudha Puja. "Killer" Peter immediately gains the upper hand, pummelling Peter with heavy blows. However, Peter is not daunted and continues to put up a brave fight, but is severely weakened and is soon on the verge of being knocked out. A teary-eyed Yazhini, who has found out from Nikita that Peter is not a boxer, tries to convince him to call off the fight, but he refuses. Peter manages to fight back, eventually knocking out "Killer" Peter, causing a major upset. Peter wins the boxing tournament and dedicates the prize money of  2,00,00,000 to the astounded IT gang, oblivious to their treachery, but they deny the offer. He also publicly conveys his love to a sobbing Yazhini, who is relieved that he has survived.

The movie ends with a display of a portion of the newspaper the siddhar had materialised torn by Nikita and left at the Chandragiri forest several months ago. The portion shows a photo of Peter lifting the boxing tournament trophy.

==Cast==
* Sivakarthikeyan as Maan Karate Peter
* Hansika Motwani as Yazhini Sethuraman
* Vamsi Krishna as Killer Peter
* Sayaji Shinde as Sethuraman
* Sathish as Sandy
* Preethi Shankar as Vaishnavi
* Ashwathy Ravikumar as Nikitha Ravi Prakash as Fernando (Killer Peters father-in-law) Soori as Tyger Tyson
* Swaminathan  as Doctor
* Vikramadithyan as Siddhar
* Arunraja Kamaraj as Neruppu Kumar
* Rajesh Gopalan as Joe
* Vinu Karthik as Gokul
* Yogi Babu as Vavvaal
* Tejaswini as Nancy (Killer Peters wife)
* Shaaji as Peters Coach
* Rajkumar as Lift Guy
* AR Murugadoss (Special appearance in the song "Open the Tasmac")
* Anirudh Ravichander (Special appearance in the song "Open the Tasmac")

==Production==
A pooja for the launch of the film took place on 10 July followed by a press meet. The film was shot in Chennai, Bangalore and Athirappilly during the first schedule and a couple of songs were shot at Malta. Hansika started shooting for her portions first while Siva Karthikeyan joined the team from the first week of August.   

The film was named after a dialogue spoken by Dhamu in Gemini (2002 Tamil film)|Gemini (2002),  where he explains that Maan Karate means nothing more than how a deer runs away when in danger. 

The climax was shot in a very grand set location in Chennai. The outdoor set for the climax was set up at a cost of   

==Soundtrack==
{{Infobox album|  
| Name        = Maan Karate
| Longtype    =
| Type        = soundtrack
| Artist      = Anirudh Ravichander
| Release     = March 16, 2014
| Cover       =
| Caption     = Feature film soundtrack
| Length      = Tamil
| Label       = Sony Music
| Producer    = Anirudh Ravichander
| Last album  = Velaiyilla Pattathari (2014)
| This album  = Maan Karate (2014)
| Next album  = Kaththi (2014)
}}
 Ethir Neechal (2013). The audio was launched at Sathyam Cinemas (Chennai) on 16 March 2014. Behindwoods rated the album 3.25/5 stating "Hit machine Anirudh delivers one more with his signature sound". 

{{track listing
| extra_column = Singer(s)
| music_credits = no
| lyrics_credits = yes
| total_length = 24:52
| title1     = Maanja
| extra1     = Anirudh Ravichander 
| lyrics1    = Madhan Karky
| length1     = 4:44

| title2     = Darling Dambakku
| extra2     = Benny Dayal, Sunidhi Chauhan
| lyrics2    = Yugabharathi
| length2    = 4:10

| title3     = Un Vizhigalil
| extra3     = Anirudh Ravichander, Shruti Haasan
| lyrics3    = R D Raja
| length3    = 4:04

| title4     = Royapuram Peter
| extra4     = Sivakarthikeyan, Paravai Muniamma
| lyrics4    = R D Raja
| length4    = 3:37

| title5     = Open the Tasmac
| extra5     = Deva (music director)|Deva, Anirudh Ravichander
| lyrics5    = Gaana Bala
| length5    = 4:06

| title6     = Darling Dambakku Reprise Version
| extra6     = Nivas, Kalpana
| lyrics6    = Yugabharathi
| length6    = 4:11}}

==Release==
The satellite rights of the film were sold to STAR Vijay. A teaser was released on Valentines Day. The film was given a "U" certificate by the Indian Censor Board. The film released on 4 April 2014. 
The film released in 650 screens worldwide which is the biggest release in Sivakarthikeyans career.

==Critical reception==
Maan Karate received garned mixed reviews from critics but positive talk from public due to comedy.  Baradwaj Rangan of The Hindu, wrote, "Sivakarthikeyan, the likeable boy next door, has transformed into Sivakarthikeyan, the big star — and Maan Karate is less a film than a ticker-tape celebration of this reality...This isn’t about Maan Karate or even about boxing. It’s about the cult of the star. And as is the case with these movies, some two hours go by during which nothing seems to be at stake. And then we get the last half-hour, soaked in melodramatic sentiment, where everything seems to be at stake". The Times of India gave the film 2.5/5, wrote, "Maan Karate is nothing but a showcase for Sivakarthikeyan...  dispenses with any form of logic, and wants us to take it as it is, no questions asked. The story, by director AR Murugadoss, is a mix of fantasy and romance, but Thirukumarans script is underdeveloped".  Hindustan Times gave it 2/5 and wrote, "Maan Karate has an interesting plot, but the way it is scripted and narrated is illogical...the movie drags you along in its sometimes strong, sometimes weak currents, interspersed with the silliest of songs and the dumbest of dances".  Indo-Asian News Service|IANS gave it 2/5 and wrote, "While the makers present the film as an out-and-out commercial entertainer and thats what it is, you still find Maan Karate meaningless because debutant Thirukumaran only tried to do justice to the heros image by compromising on the plot. He also takes the audience for granted and gives them a film under the assumption that they will embrace it because it has been written by Murugadoss".  Rediff gave the film 2/5 and wrote, "Maan Karate is a letdown by uninspiring direction".  The New Indian Express wrote, "With a watchable first half and a disappointing second half, Maan Karate is an average entertainer".  Behindwoods gave 2.75/5 and concluded that the film is "Siva Karthikeyans entertainer to beat the summer heat".  Deccan Chronicle gave 2.5/5 and summarised that, "For Sivakarthikeyan and Hansika fans, this movie will prove to be decent entertainer, if not a blockbuster.".  Sify stated that the film is "On the whole, a perfect summer outing with your family.".  Siddarth Srinivas of Cinemalead gave 2.5/5 and concluded, "This boxer packs a punch which is half as powerful.".  Indiaglitz wrote, "Barring the boxing sequences, a good watch!" and rated the movie 2.75/5.  Bharath Vijaykumar of Moviecrow gave 2.25/5 and concluded, "Maan Karate is not as smart as the title suggests.".  OneIndia gave 2.5/5 and concluded, "Maan Karate is for Sivakarthikeyan and Hansika Motwanis fans.". 

===Box office===
The film opened to great response by collecting   in its first day which is the biggest opening for Sivakarthikeyan till Kaaki Sattai release.The film saw a huge drop in its second day by collecting   and on third day it collected  .Over its first weekend,the film made total around   worldwide.  On chennai, the film collected   which is the biggest opening in 2014 Chennai box office by beating Jilla and Veeram.  In Chennai alone, it collected over   which is also a biggest hit in Sivakarthikeyans career. The film collected   after its successful run at the box office.   The film became 6th Hit for Sivakarthikeyan.The film declared Super-Hit at the box office.

==References==
 

==External links==
*  

 

 
 
 
 
 
 