War Is Hell (film)
{{Infobox film
| name           = War is Hell
| image          = War Is Hell (movie poster).jpg
| image_size     =
| caption        =
| director       = Burt Topper
| producer       = Burt Topper
| writer         = Burt Topper
| narrator       = Audie Murphy
| starring       = Baynes Barron Michael Bell
| music          = Ronald Stein
| cinematography = Jacques Marquette
| editing        = Ace Herman Allied Artists Pictures Corporation
| released       =  
| runtime        = 81 min.
| country        = US
| language       = English
| budget         =
}}
War is Hell is a 1963 American war film written, produced and directed by Burt Topper. The film stars Baynes Barron and Michael Bell and is narrated by Audie Murphy. 

== Plot summary ==

Set during the Korean War, the film depicts the atrocities of battle. Sgt. Garth (Barron), an egomaniacal, blood thirsty leader, neglects to tell his soldiers that there has been a cease fire.  The Sergeant sends his unit into an enemy bunker, where they are fiercely attacked by the enemy.  The few who survive secure the bunker, and Garth attempts to take credit for their actions.  Further chaos ensues, resulting in the deaths of many in the platoon, as well as the mortal wound to the Sergeant himself.

== Cast ==
* Baynes Barron as Sgt. Garth
* Michael Bell as Seldon
* Bobby Byles as Gresler
* Wally Campo as Laney
* Kei Thin Chung as Korean Lieutenant (as Kei Chung)
* J.J. Dahner as Koller
* Judy Dan as Yung Chi Thomas
* Robert Howard as Connors
* Audie Murphy as Narrator
* Russ Prescott as Bender
* Tony Rich as Miller
* Tony Russel as Sgt. Keefer
* Paul Sheriff as Thurston
* Burt Topper as Lt. Hallen

==Release== From Russia with Love, with War Is Hell as the second feature of a double bill in the United States on May 27, 1964. 

==In popular culture==
War Is Hell was the second of two features (along with Cry of Battle) playing at the Texas Theatre in Dallas, Texas on November 22, 1963. After his alleged shootings of President John F. Kennedy, Governor John Connally, and Dallas patrolman J.D. Tippit that day, Lee Harvey Oswald snuck into this theater without paying. After box office cashier Julie Postal received a tip on Oswald from nearby shoe store employee John Brewer, she called Dallas Police and Oswald was arrested before the movie ended.  Oswald was fatally shot two days later while being transferred to another jail by club owner Jack Ruby. 

==Notes==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 