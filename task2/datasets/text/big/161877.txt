This Sporting Life
 The Sporting Life}}
 
{{Infobox film
| name           = This Sporting Life
| image          =   Sporting-life-poster.jpg
| image_size     = 
| writer         = David Storey Rachel Roberts Alan Badel William Hartnell
| director       = Lindsay Anderson
| producer       = Karel Reisz
| cinematography = Denys Coop
| music          = Roberto Gerhard Peter Taylor
| distributor    = Rank Organisation, Janus Films
| released       = 7 February 1963 (London, West End)
| runtime        = 134 minutes
| country        = United Kingdom
| language       = English
| budget         = £230,000 
}}
This Sporting Life is a 1963 British feature film based on a novel of the same name by David Storey which had won the 1960 Macmillan Fiction Award. It recounts the story of a rugby league footballer, Frank Machin, in Wakefield, a mining area of Yorkshire, whose romantic life is not as successful as his sporting life. Storey, a former professional rugby league footballer, also wrote the adapted screenplay.
 Rachel Roberts, Best Actor Oscar for Best Actor another BAFTA Saturday Night Oscar nomination The Servant.
The film opened at the Odeon Leicester Square in Londons West End on 7 February 1963. 

==Plot==
Set in Wakefield, the film concerns a bitter young Yorkshire coal miner, Frank Machin (Harris). Following a nightclub altercation in which he takes on the captain of the local rugby league club and smacks a couple of the others, he is recruited by the teams manager, who sees profit in his aggressive streak.
 loose forward (number 13) and impresses all with his aggressive forward play. He often punches or elbows the opposition players throughout the game.
 brain haemorrhage shortly after their split; she soon dies without regaining consciousness. In the end he is seen as "just a great ape on a football field", vulnerable to the ravages of time and injury.

==Cast==
* Richard Harris as Frank Machin Rachel Roberts as Margaret Hammond
* Alan Badel as Gerald Weaver
* William Hartnell as Dad Johnson
* Colin Blakely as Maurice Braithwaite
* Vanda Godsell as Mrs. Anne Weaver
* Anne Cunningham as Judith Jack Watson as Len Miller
* Arthur Lowe as Charles Slomer
* Harry Markham as Wade
* George Sewell as Jeff
* Leonard Rossiter as Phillips, Sports writer
* Peter Duguid as Doctor
* Wallas Eaton as Waiter
* Anthony Woodruff as Tom, Head waiter Tom Clegg as Gower
* Ken Traill as the Trainer
* Frank Windsor as the Dentist
* Ian Thompson as the Batley Town captain

==Production== Saturday Night and Sunday Morning (1960), passed it to his friend, Lindsay Anderson. Anderson accepted and Reisz produced.

Notable among the supporting cast is William Hartnell, who would soon gain notice as the First Doctor on Doctor Who. It was his role in This Sporting Life which brought Hartnell to the attention of the first Doctor Who producer Verity Lambert. It also featured the future Dads Army star, Arthur Lowe, who also appeared in four later films directed by Anderson.

===Filming locations=== Wakefield Trinitys Belle Vue and at Halifax RLFC|Halifaxs then stadium Thrum Hall. The scene where Frank (Richard Harris) leaps from a bus to buy a newspaper, then leaps back on the bus was filmed at the top of Westgate, Wakefield. The location is still instantly recognisable today and has changed very little apart from the addition of small bars and clubs. The houses used for the outdoor scenes in This Sporting Life were actually filmed in Servia Terrace in Leeds.  The canteen van was parked in Servia Grove. 

===Editing===
Anthony Sloman wrote:  
A specific description of the editing has been given in the 2001 book by Don Fairservice:  

==Lindsay Anderson on directing Richard Harris==
Anderson, who often developed unrequited feelings for unobtainable heterosexual men,  wrote in his diary on 23 April 1962, after the first month or so of production: "the most striking feature of it all, I suppose, has been the splendour and misery of my work and relationship with Richard." He felt that Harris was acting better than ever before in his career, but feared his feelings for Harris, whose combination of physicality, affection and cruelty fascinated him, meant that he lacked detachment he needed as a director. "I ought to be calm and detached with him. Instead I am impulsive, affectionate, infinitely susceptible." 

==Critical reception== John Davis, by now the Chairman of the Rank Organisation Board to announce that he was pulling out of British New Wave, Kitchen sink realism|"kitchen sink" drama, nor would his company make such a "squalid" film again.  In the United States, the film was well received. Variety (magazine)|Variety praised its "gutsy vitality", and praised the production of Reisz and the directorial feature début of Anderson, who "brings the keen, observant eye of a documentary man to many vivid episodes without sacrificing the story line." 

John Russell Taylor in 1980 thought it a mistake to link This Sporting Life with the kitchen sink films released in the preceding few years, because its "emotionalism" made it "unique", apart from Andersons other work:
 "every scene in the film is charged with the passion of what is not said and done, as well as what is. ... Though real enough and believable enough, this kind of amour fou is remote indeed from what the staid middle class cinema would generally consider as realism."  

On 22 January 2008, the film was released as a Region 1 DVD by the Criterion Collection.

==See also==
* BFI Top 100 British films

==References==
 

==External links==
*  
* 

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 