The Lady Lies (film)
 
{{Infobox film
| name           = The Lady Lies
| image          = Ladyliesposter.jpg
| caption        =
| director       = Hobert Henley
| producer       = Walter Wanger
| writer         = Mort Blumenstock (titles) Garrett Fort (adaptation) Tom Brown
| music          =
| cinematography = William O. Steiner
| editing        = Helene Turner
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 75 min
| country        = United States
| awards         =
| language       = English
| budget         =
}} 1929 film directed by Hobert Henley, and starring Walter Huston, Claudette Colbert and  Charles Ruggles.

==Plot==
Children of a widower who is having an affair with a salesgirl try to break it up but are won over by the girl.

==Cast==
*Walter Huston as Robert Rossiter 
*Claudette Colbert as Joyce Roamer
*Charles Ruggles as  Charlie Tayler Tom Brown as Bob Rossiter 
*Betty Garde as Hilda Pearson 
*Jean Dixon  as Ann Gardner 
*Duncan Penwarden as Henry Tuttle 
*Virginia True Boardman as  Amelia Tuttle

==Filming locations==
Paramount Studios - 5555 Melrose Ave., Hollywood, Los Angeles, California, USA

==External links==
*   
*  
*  

 

 
 
 
 
 
 
 
 
 


 