El Santo de la Espada
El Argentine historical historical epic film directed by Leopoldo Torre Nilsson and starring Alfredo Alcón. It narrates the life of José de San Martín.  It was written by Beatriz Guido y Luis Pico Estrada, based on the eponymous novel by Ricardo Rojas. The script was supervised by the Sanmartinian National Institute. It had a great success, and it is the most successful movie about "The Emancipator" San Martín, although it received some negative reviews due to historical inaccuracies. 

== Synopsis ==
The story relates the life and career of José de San Martín during the South American wars of independence. It starts with his return to Mendoza province, with the rest of the narration being  told as a Flashback (narrative)|flashback.

== Cast ==
  
* Alfredo Alcón as José de San Martín
* Evangelina Salazar as Remedios de Escalada
* Lautaro Murúa as OHiggins
* Ana María Picchio
* Héctor Alterio as Simón Bolivar
* Héctor Pellegrini
* Alfredo Iglesias
* Mario Casado
* Walter Soubrié
* Fernando Lewiz
* Onofre Lovero
* Miguel Bermúdez
* Juan Carlos Lamas
* Diego Varzi
* Aldo Barbero
* Eduardo Pavlovsky
 
* Leonor Benedetto
* Eduardo Humberto Nóbili
* Rodolfo Brindisi
* Luis Manuel de la Cuesta
* Carlos Lucini
* Miguel Herrera
* Marcelo Miró
* Hugo Arana
* Ramón Cas
* Carlos Davis
* Rubén Green
* José Slavin
* Leonor Manso
* Alejo Patricio López Lecube
* Mercedes Sosa
 

== History ==
The role of San Martín was played by Alfredo Alcón. Evangelina Salazar represented Remedios de Escalada, San Martins wife. Other important actors included were Lautaro Murúa, Héctor Alterio and Alfredo Iglesias; Leonor Benedetto, Rubén Green and Hugo Arana were novice actors by that time, and eventually developed notable careers afterwards.
 Regiment of Mounted Grenadiers.

His relation with   and Peru, San Martín resigns to his powers in Peru after his meeting with Simon Bolivar, in the Guayaquil conference; he returns to Mendoza, upon receiving a letter informing him of the grave illness of Remedios. When he returned to Mendoza, he found Remedios already dead.

It was premiered in Mendoza on March 25, 1970, and quickly became a Blockbuster (entertainment)|blockbuster. A few months later, a group of revisionists created a documental movie about San Martín, in an effort to make clear some events the film narrated.

== See also ==
* José de San Martín

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 