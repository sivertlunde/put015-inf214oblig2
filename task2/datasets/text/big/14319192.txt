Kicking the Germ Out of Germany
 
{{Infobox film
| name           = Kicking the Germ Out of Germany
| image          = 
| caption        = 
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = 
| editing        = 
| studio         = Rolin Film Company
| distributor    = Pathe Exchange
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd. The film is now considered a lost film.   

==Cast==
* Harold Lloyd as The Boy
* Snub Pollard 
* Bebe Daniels 
* Lige Conley (as Lige Cromley)
* Mildred Forbes
* William Gillespie
* Helen Gilmore
* Max Hamburger
* Estelle Harrison
* Lew Harvey
* Wallace Howe
* Bud Jamison
* Dee Lampton
* Oscar Larson
* Maynard Laswell (as M.A. Laswell)
* Grace Madden
* Belle Mitchell
* James Parrott
* Hazel Powell Charles Stevenson
* Myrtle Watson
* Noah Young

==See also==
* Harold Lloyd filmography
* List of lost films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 