The Unknown Tomorrow
{{Infobox film
| name           = The Unknown Tomorrow
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Korda
| producer       = Alexander Korda 
| writer         = Sydney Garrick (play)   Alexander Korda   Ernest Vajda
| narrator       = 
| starring       = Werner Krauss   Maria Corda   Olga Limburg  Carl Ebert
| music          = 
| editing        = Karl Hartl
| cinematography = Sophus Wangöe
| studio         = Korda Film
| distributor    = 
| released       = 1923
| runtime        = 90 minutes
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =  German silent silent drama film directed by Alexander Korda and starring Werner Krauss, María Corda and Olga Limburg.

==Production and reception== Samson and Delilah. The film was a financial success, and Korda used his share of the profits to buy a stake in the film distribution company FIHAG. 

Werner Krausss performance has been noted for its expressionist acting, even though much of the rest of the film is non-expressionist. 

==Cast==
* Werner Krauss - Marc Muradock 
* María Corda - Stella Manners 
* Olga Limburg - Zoé, Maid 
* Carl Ebert - Gordon Manners 
* Louis Ralph - Alphonse, Muradocks accomplice 
* Friedrich Kühne - Raorama Singh 
* Antonie Jaeckel - the Aunt 
* Paul Lukas - Minor role

==Plot==
A wife is wrongly believed of adultery by her husband who leaves her. She then struggles to prove her innocence and win him back while foiling the machinations of an admirer of hers who wishes to keep her apart from her husband.

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 