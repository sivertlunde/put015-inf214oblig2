Private Buckaroo
{{Infobox film
| name           = Private Buckaroo
| image          = Private-Buckaroo-1942.jpg
| caption        = Film poster
| director       = Edward F. Cline
| producer       = Ken Goldsmith
| writer         = Edward James (writer) Edmond Kelso (writer) Paul Girard Smith (story)
| narrator       =
| starring       =
| music          =
| cinematography = Elwood Bredell
| editing        = Milton Carruth
| distributor    = Universal Studios
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Private Buckaroo (1942) is an American musical film directed by Edward F. Cline and starring The Andrews Sisters, Dick Foran, Harry James, Shemp Howard, Joe E. Lewis, and Jennifer Holt. The film tells the story of army recruits following basic training, with the Andrews Sisters attending USO dances.

== Plot summary ==
Entertainer Lon Prentice initially is keen to enlist in the US Army but is prevented from this due to his having one flat foot.  After having the flat fixed, he is accepted for enlistment. Soon after basic training begins, Private Prentice informs his commanding officer that he finds most military training useless, unnecessary and beneath him.  His commander orders all the men that Private Prentice is exempt from doing things he doesnt want to do, which turns the entire camp against him.

== Cast ==
*The Andrews Sisters (Maxene Andrews, Patty Andrews and Laverne Andrews)
*Dick Foran	as Lon Prentice
*Joe E. Lewis as Lancelot Pringle McBiff
*Ernest Truex as Col. Elias Weatherford
*Jennifer Holt as Joyce Mason
*Shemp Howard as First Sgt. Muggsy Shavel
*Harry James as Himself (Harry James and His Music Makers) Richard Davies as Lt. Howard Mason
*Mary Wickes as Bonnie-Belle Schlopkiss
*Donald OConnor as Donny
*Peggy Ryan as Peggy
*Huntz Hall as Cpl. Anemic
*Susan Levine as Tagalong
*Jivin Jacks and Jills (dance troupe)
 Edmund Glover, Harold Miller, Sidney Miller, Edmund Mortimer, Joey Ray, Addison Richards, Jeffrey Sayre, Edwin Stanley and Billy Wayne appears uncredited.

== Soundtrack == Private Buckaroo" (written by Charles Newman and Allie Wrubel) Irving Taylor and Vic Mizzy)
*Dick Foran - "Im in the Army Now"
*The Andrews Sisters - "Six Jerks in a Jeep" (written by Sid Robin)
*The Andrews Sisters - "Dont Sit Under the Apple Tree (With Anyone Else but Me)|Dont Sit Under the Apple Tree" (written by Sam H. Stept and Charles Tobias)
*The Andrews Sisters - "James Session" danced by Donald OConnor, Peggy Ryan and The Jivin Jacks and Jills
*The Andrews Sisters - "Steppin Out Tonight" based on the song "Thats The Moon, My Son" (written by Art Kassel and Sammy Gallop.
*Dick Foran and Helen Forrest - "Nobody Knows the Trouble Ive Seen"
*Harry James and His Music Makers - "Concerto for Trumpet"
*The Andrews Sisters - "Johnny Get Your Gun Again" (written by Don Raye and Gene de Paul)
*Dick Foran and The Andrews Sisters - "Weve Got a Job To Do" (written by Vickie Knight)
*Joe E. Lewis - "I Love the South" You Made Me Love You" (written by Joseph McCarthy and James V. Monaco)

==See also==
* This Is the Army Hollywood Canteen
* Follow the Boys
* Stage Door Canteen Thank Your Lucky Stars
* Star Spangled Rhythm
* Thousands Cheer The Yanks Are Coming

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 