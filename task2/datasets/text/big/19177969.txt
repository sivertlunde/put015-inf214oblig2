Dead Man's Folly (film)
{{Infobox film
| name           = Dead Mans Folly
| image_size     =
| image	         = Dead Mans Folly FilmPoster.jpeg
| caption        = The DVD Cover
| director       = Clive Donner
| writer         = Agatha Christie Rod Browning
| narrator       =
| starring       = Peter Ustinov Jean Stapleton Constance Cummings Tim Pigott-Smith Jonathan Cecil
| distributor    = CBS Television
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
}}
Dead Mans Folly is a 1986 British-American television film featuring Agatha Christies Belgian detective Hercule Poirot. It is based on Christies novel Dead Mans Folly. The film was directed by Clive Donner and starred Peter Ustinov as Poirot.

The cast included Jean Stapleton, Jonathan Cecil, Constance Cummings and Nicollette Sheridan. It was shot largely on location at West Wycombe Park in Buckinghamshire, England.

==Plot introduction==
Hercule Poirot and his associate, Captain Hastings, are called in by his eccentric mystery author friend, Ariadne Oliver, to a manor house in Devon. Oliver is organizing a "Murder Hunt" game for a local fair to be held at Nass House, but she is troubled by something she cannot quite put her foot on.

Things take a turn for the worse when during the "Murder Hunt" the girl playing the "dead" body is murdered for real. Soon afterwards, the lady of the manor mysteriously disappears and an old mans body is pulled from the river. Poirot must discover who and what are behind these seemingly unconnected events.

==Cast==
*Peter Ustinov - Hercule Poirot
*Jean Stapleton - Ariadne Oliver
*Constance Cummings - Amy Folliat 
*Tim Pigott-Smith - Sir George Stubbs 
*Jonathan Cecil - Captain Arthur Hastings
*Kenneth Cranham - Detective Inspector Bland 
*Susan Wooldridge - Amanda Brewis
*Christopher Guard - Alec Legge
*Jeff Yagher - Eddie South
*Nicollette Sheridan - Hattie Stubbs | Italian Student (as Nicolette Sheridan) 
*Ralph Arliss - Michael Weyman
*Caroline Langrishe - Sally Legge Alan Parnaby - The Boatman

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 
 