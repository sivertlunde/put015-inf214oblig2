3 al rescate
{{Infobox film
| name           = 3 al rescate
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Jorge   Luis Morillo
| producer       = Jorge Morillo   José Miguel Bonetti
| writer         = Christian López   Edwin Gautreau   Lucy Bedeglez
| screenplay     = 
| story          = 
| based on       =  
| starring       = Cuquín Victoria Kenny Grullón Roger Zayas 
| narrator       = 
| music          = Alexander Nadal Piantini
| cinematography = 
| editing        = 
| studio         = Raycast Animation Studio   Antena Latina Films
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Dominican Republic
| language       = Spanish
| budget         = 
| gross          = 
}}

3 al rescate (English: 3 to the rescue) is the first animated film made in the Dominican Republic. Based on an animated short called 3 for the banquet of Raycast Animation Studio. Directed by Jorge and Luis Morillo, written by Christian López, Edwin Gautreau and Lucy Bedeglez, produced by Animation Studio Raycast and Antena Latina Films. It premiered on January 6, 2011 in Dominican Republic cinemas.

== Synopsis ==
It tells the story of a goat (Enrique), a chicken (Frank) and a pig (Mauricio) who, after suspecting that they will become the Christmas Eve dinner, escape from the farm where they were to embark on an unusual adventure.

On their way to the unknown they form a close friendship with an unusual iguana (Bilpo), who rescues them from various situations in which involved are due to their inexperience in a new world of freedom. Suddenly, a ruthless poacher (the claw) who is engaged in the illegal practice of selling endangered animals, kidnaps Bilpo to sell it to a collector of exotic animals.

Our friends, driven by a sense of loyalty and gratitude to her partner follow his trail to the big city (Santo Domingo) where with a group of captured animals carry out the most daring and dangerous rescue operation before made.

== Cast ==
* Cuquín Victoria as Vinicio the Cat
* Kenny Grullón as Frank the chicken
* Roger Zayas as Enrique the goat
* Antonio Melenciano as the Hunter claw Méndez
* Frank Perozo as Larry and Juanchy Panky Saviñón as Harry and René
* Irving Alberti as Don Nicanor and Alfredo Memo Cortines as Bilpo the iguana
* Luís José Germán Mauricio the pig and Camilo alligator
* Giovanna Bonelly as Miss Jiménez
* Alejandro Alfonso as Sammy
* Carolina Rivas as Manuela
* Dominique Bonelly as Lucy
* Tony Rojas as Mr. Ruffini

==External links==
*  

 
 

 