Bheekaran
{{Infobox film
| name           = Bheekaran
| image          =
| caption        =
| director       = Prem
| producer       = Prem
| writer         =
| screenplay     = Rajkumar Bheeman Madhuri Ramya Krishnan
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Madhuri and Ramya Krishnan in lead roles. The film had musical score by G. Devarajan.   

==Cast== Rajkumar
*Bheeman Raghu Madhuri
*Ramya Krishnan
*Ravi Menon
*Sabitha Anand
*TG Ravi

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery and Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Karimbinte Villum || K. J. Yesudas || Yusufali Kechery || 
|-
| 2 || Swargam Swargam || K. J. Yesudas, P. Madhuri || Yusufali Kechery || 
|-
| 3 || Youvanam Arulum || Vani Jairam || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 