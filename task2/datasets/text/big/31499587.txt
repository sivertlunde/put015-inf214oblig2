The Great Meadow
{{infobox film
| name           = The Great Meadow
| image          = The Great Meadow 1931 film.jpg
| imagesize      =
| caption        = 1931 theatrical poster
| director       = Charles Brabin
| producer       = Louis B. Mayer Irving Thalberg Edith Ellis Charles Brabin
| starring       = Johnny Mack Brown Eleanor Boardman
| music          = William Axt
| cinematography = William H. Daniels Clyde De Vinna
| editing        = George Hively
| distributor    = Metro-Goldwyn-Mayer
| released       = January 24, 1931
| runtime        =
| country        = United States
| language       = English
}} Drums Along the Mohawk by Walter D. Edmonds and made into a 1939 film titled Drums Along the Mohawk by John Ford. 

==Cast==
*Johnny Mack Brown – Berk Jarvis
*Eleanor Boardman – Diony Hall
*Lucille La Verne – Elvira Jarvis
*Anita Louise – Betty Hall Gavin Gordon – Evan Muir Guinn "Big Boy" Williams – Rubin Hall Russell Simpson – Thomas Hall
*Sarah Padden – Mistress Hall
*Helen Jerome Eddy – Sally Tolliver

unbilled
*William Bakewell – Jack Jarvis
*James Bradbury Jr – Bit
*Heinie Conklin – Bit Dale Fuller – Bit 
*Lloyd Ingraham – Elly Harmon
*Gardner James – Bit
*Paul Kruger – Bit
*Lillian Leighton – Bit
*James A. Marcus – Bit
*Frank McGlynn, Sr. – Bit
*John Miljan – Daniel Boone
*Helen Millard – Bit 
*Virginia Sale – Bit
*Andy Shuford – Bit
*Jerry Stewart – Bit
*Greg Whitespear – Bit

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 


 