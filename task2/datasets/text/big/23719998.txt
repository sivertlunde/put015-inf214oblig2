A Successful Man
{{Infobox film
| name           = A Successful Man
| image          =
| caption        =
| director       = Humberto Solás Humberto Hernández
| writer         = Humberto Solás Juan Iglesias
| starring       = César Évora
| music          =
| cinematography = Livio Delgado
| editing        = Nelson Rodríguez
| distributor    =
| released       = 1985
| runtime        = 116 minutes
| country        = Cuba
| language       = Spanish
| budget         =
}}

A Successful Man ( ) is a 1985 Cuban drama film directed by Humberto Solás. It was screened in the Un Certain Regard section at the 1987 Cannes Film Festival    and it was entered into the 15th Moscow International Film Festival.    It won Best Production Design and Grand Coral - First Prize in the 1986 Havana Film Festival.

==Cast==
* César Évora - Javier Argüelles
* Raquel Revuelta - Raquel
* Daisy Granados - Rita
* Jorge Trinchet - Darío Argüelles
* Rubens de Falco - Iriarte
* Mabel Roch - Ileana Ponce Carlos Cruz - Puig
* Miguel Navarro - Lucilo
* Omar Valdés - Facundo Lara
* Ángel Espasande - Father Rubén
* Ángel Toraño - Ponce
* Isabel Moreno - Berta
* Nieves Riovalles - Daríos wife
* Jorge Alí - Baseball player
* Denise Patarra - Puigs girlfriend
* Max Álvarez - Priest
* Niola Montes - Mrs. Ponce Orlando Contreras - Daríos friend

==References==
 

==External links==
* 

 
 
 
 
 
 
 