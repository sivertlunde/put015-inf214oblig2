City That Never Sleeps
:This article is about a 1953 film. For other uses, see The City That Never Sleeps (disambiguation).
{{Infobox film
| name           = City That Never Sleeps
| image          = Citysleeps.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = John H. Auer
| producer       = John H. Auer
| screenplay     = Steve Fisher William Talman
| music          = R. Dale Butts
| cinematography = John L. Russell Fred Allen
| distributor    = Republic Pictures Corporation
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
|}}
City That Never Sleeps is a  1953 film noir produced and directed by John H. Auer with cinematography by John L. Russell. 

==Plot== Edward Arnold), a corrupt, powerful attorney, wants him for a job, Johnny is tempted.  He needs money in order to make a quick escape from Chicago and start life anew with "Angel Face".
 William Talman), now a criminal, across the border to Indiana. Not all is what it seems and the more Kelly learns the more hes determined to do right.

==Cast==
* Gig Young as Johnny Kelly
* Mala Powers as Sally Angel Face Connors William Talman as Hayes Stewart Edward Arnold as Penrod Biddel
* Chill Wills as Sgt. Joe, the Voice of Chicago
* Marie Windsor as Lydia Biddel
* Paula Raymond as Kathy Kelly
* Otto Hulett as Sgt. John Pop Kelly Sr.
* Wally Cassell as Gregg Warren
* Ron Hagerthy as Stubby Kelly
* James Andelin as Lt. Parker
* Tom Poston as Detective
* Bunny Kacher as Agnes DuBois

==Reception==

===Critical response===
Film critic Craig Butler wrote, "City That Never Sleeps is an uneven crime drama, one that contains some enough good elements that its frustrating the film as a whole is not better. The chief culprit is, as so often, the screenplay, which starts out promisingly. Gig Youngs character seems to be one that is fairly complex, a cop who is dissatisfied with his lot in life and could fall prey to temptation. Unfortunately, the character is not developed sufficiently beyond that, which is also the case with the Wally Cassell "mechanical man" character; he, too, shows promise that goes unfulfilled, although the sheer strangeness of his job does fascinate. 

The staff at Variety (magazine)|Variety magazine gave the film a mixed review, and wrote, "Production and direction loses itself occasionally in stretching for mood and nuances, whereas a straightline cops-and-robbers action flavor would have been more appropriate. Same flaw is found in the Steve Fisher screen original...John L. Russells photography makes okay use of Chicago streets and buildings for the low-key, night-life effect required to back the melodrama. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 