Baharon Ki Manzil (1968 film)
{{Infobox film
| name           = Baharon Ki Manzil
| image          = 
| image_size     = 
| caption        = 
| director       =Yakub Hassan Rizvi
| producer       =
| writer         = 
| narrator       =  Rehman Anwar Hashmi music          = Laxmikant Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1968
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 Rehman and Farida Jalal. The films music is by Laxmikant Pyarelal.

==Plot==
Darjeeling-based Roy family consists of Subodh, his wife, Nanda, and their daughter, Nalini. On the occasion of Nalinis engagement ceremony with Mohan, a number of celebrations take place, including fireworks, which result in Nandas accident, she passes out, is treated for minor injuries, and is allowed to recuperate. The next morning she wakes up claiming that she is Radha; that Nalini is not her daughter; and that Subodh is her brother-in-law. She also states that she is to get married to Ram Kumar of Bombay. Fearing that his wife is losing her sanity, Subodh first summons their family doctor, Dr. Verma, and then a Psychiatrist, Dr. Rajesh Khanna. Rajesh conducts some tests on Nanda and concludes that her brain activity is normal. Then Nanda sees a dead womans body in her closet, which subsequently disappears when the others arrive; she also tries to kill herself - in vain. On her insistence, Subodh and she travel to Bombay to unravel this mystery - to no avail, and they return home. Then Nanda recalls a dressmaker named Glory DSilva, and Rajesh undertakes the journey to Byculla, Bombay, and brings Glory back - who positively identifies Nanda and states that Radha, Nandas sister, and her parents were all killed in a fire 16 years ago. The question remains, if Radha is dead, then why is Nanda claiming that she is her, and why is she unable to recollect the last 16 years of her life as Subodhs wife, and Nalinis mother?

==Cast==
* Meena Kumari	 ...	Nanda / Radha
* Dharmendra	 ...	Dr. Rajesh Khanna
* Rehman	 ...	Subodh Roy
* Farida Jalal	 ...	Nalini S. S. Roy
* Tun Tun	 ...	Glory DSilva
* Malika	 ...	Ballet Dancer / Singer
* Polson	 ...	Roys butler
* Shefali	 ...	Rosy
* Ramayan Tiwari	 ...	Roys goon

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Yeh Daman Ab Na (Revival)"
| Lata Mangeshkar
|-
| 2
| "Nigahen Kyon Bhatakti Hai"
| Lata Mangeshkar
|-
| 3
| "Aaja Re Piya"
| Lata Mangeshkar
|-
| 4
| "Yeh Daman Ab Na Chhutega Kabhi"
| Lata Mangeshkar
|-
| 5
| "Janam Din Aaya"
| Lata Mangeshkar
|}

== External links ==
*  

 
 
 
 

 