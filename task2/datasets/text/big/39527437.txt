Director's Special (film)
{{Infobox film
| name           = Directors Special
| image          = Kannada film Directors Special poster.jpg
| caption        = Film poster
| director       = Guruprasad
| producer       = Govindu
| writer         = Guruprasad
| screenplay     = Guruprasad
| story          = Guruprasad Dhananjaya Rangayana Raghu
| music          = Anoop Seelin
| cinematography = Mahendra Simha
| editing        = Kemparaju
| studio         = Ajay Pictures Guruprasad Inc
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Kannada Satire|satire Dhananjay along with Rangayana Raghu in the lead roles. Actress Pooja Gandhi makes a special appearance in an item song. 

==Cast==
* Rangayana Raghu as Panche Shastry / Ramachandra   Dhananjay as Dhananjaya
* Vatsala Mohan as Seethamma
* Sumitra Devi as Gayathri 
* Ram as Hanuma
* M. S. Nagaraj Karanth
* Sathyanarayana Shastry
* S. Keshavamurthy
* Krishnappa in a cameo appearance
* Pooja Gandhi in a cameo appearance
* Anoop Seelin in a cameo appearance
* Guruprasad in a cameo appearance

==Production==
The film was announced by director Guruprasad in 2009.  However, the official announcement came in  August 2010, when he launched the film with the title Directors Special at Abbaiah Naidu Studios, Bangalore, in the presence of actors Raghavendra Rajkumar and V. Ravichandran. 

===Casting and filming===
The films lead role was first offered to Komal Kumar who rejected the offer after hearing the story line which makes the spoof of Kannada film industry.  After re-working with the script, the director approached Dhananjay (actor)|Dhananjay, a software techie, to play the lead role. Subsequently he signed actor Rangayana Raghu to play a prominent role.

Filming began in 2011 and was completed the same year, excluding a song sequence which was filmed in 2012.  The song, an item number featured actress Pooja Gandhi in a cameo role in the song. Choreographed by Imran Sardhariya, the sequence was filmed at a studio in Bangalore. 

==Soundtrack==
{{Infobox album
| Name        = Directors Special
| Type        = Soundtrack
| Artist      = Anoop Seelin
| Cover       = Kannada film Directors Special album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 31 May 2013
| Recorded    =  Feature film soundtrack
| Length      = 11:18
| Label       = Jhankar Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Anoop seelin composed music for the soundtracks for the lyrics written by B. R. Lakshman Rao and Sharadasutha. The album consisting of three tracks includes an instrumental number "Mouna Raaga". 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 11:18
| lyrics_credits = yes
| title1 = Devare Agadha
| lyrics1 = B. R. Lakshman Rao
| extra1 = Rajesh Krishnan
| length1 = 4:17
| title2 = Kannalle Yeshtotthu
| lyrics2 = Sharadasutha
| extra2 = Sunitha
| length2 = 3:53
| title3 = Mouna Raaga
| lyrics3 = 
| extra3 = Instrumental
| length3 = 3:08
}}

==Release and reception==
After several delays in the production and post-production, director announced the release to be in April 2013. But owing to the other big production releases and theater availability issues, the film was pushed for a May release. The film made its theatrical release on May 31, 2013 in 50 theatres across Karnataka. 

===Critical reception=== Dhananjay received Dhananjay and the supporting cast, and the music, camera and editing departments.  Prakash Upadhyaya of Oneindia.in|Oneindia gave the film a rating of three and a half stars out of five and praised the role of the technical and the acting departments in the film, giving special praise to Rangayana Raghus performance.  Sify.com gave the film a "good" rating and credits Guruprasads style of filmmaking with a "futuristic topic as the theme" and adds that the film "  ... beyond the traditional realm to explore human mind". He concluded, "Performance wise, it is Rangayana Raghu, who excels on the screen. The new comer Dhananjay shows promise."  The Hindu writes "A dialogue with greed" on the theme of the film. Of the acting performances, the reviewer writes, "Though it is Rangayana Raghu who steals the show with his restrained performance, the new comer Dhanajaya proves his mettle."   

==References==
 

==External links==
*  
*   at  

 
 
 
 