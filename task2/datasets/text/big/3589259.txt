Second Name
{{Infobox film
| name           = Second Name
| image          = Second_name_poster.jpg
| caption        = Film Poster
| director       = Paco Plaza
| producer       = Julio Fernández
| writer         = Ramsey Campbell (story) Fernando Marías (screenplay) Paco Plaza (screenplay)
| starring       = Erica Prior Denis Rafter Craig Stevenson John OToole
| music          = Mikel Salas
| cinematography = Pablo Rosso
| editing        = José Ramón Lorenzo Picado
| distributor    = Filmax International
| released       =  
| runtime        = 93 minutes
| country        = Spain
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 2002 Cinema Spanish horror film. The film is based on 2001 horror novel Pact of the Fathers by English horror writer Ramsey Campbell.

==Plot==
After the suicide of her father, Daniella (Erica Prior) investigates her familys past.

The plot involves a sect called the Abrahamites, who (according to the movie) sacrifice their first borns following an alternative interpretation of Gods Will in the Binding of Isaac|near-sacrifice of Isaac.
 Ufizzi version of the biblical scene is shown in the movie to illustrate this alternative belief.

The movie version of the Abrahamites traditionally give their first borns the fathers middle name, hence the title of the movie.

==Cast==
*Erica Prior....Daniella
*Denis Rafter....Simon Hastings
*Craig Stevenson....Toby Harris
*John OToole....Father Elias

==Awards and nominations==
Cinénygma - Luxembourg International Film Festival
*2003: Nominated, "Grand Prize of European Fantasy Film in Gold"

Fantasporto
*2003: Nominated, "Best Film"

Sitges Film Festival
*2002: Won, "Grand Prize of European Fantasy Film in Silver"
*2002: Nominated, "Best Film"

Turia Awards
*2003: Won, "Best First Work" - Paco Plaza

==External links==
*  
*  

 
 
 
 
 


 
 