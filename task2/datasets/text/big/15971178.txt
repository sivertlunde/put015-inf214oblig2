The Lucky Star (film)
{{Infobox Film
| name           = The Lucky Star
| image          = 
| image_size     = 
| caption        =  Max Fischer
| producer       = André Fleury (producer)|André Fleury (executive producer) Pieter Kroonenburg (associate producer) Claude Léger (producer)
| writer         = Max Fischer Jack Rosenthal
| narrator       = 
| starring       = Brett Marx Rod Steiger Louise Fletcher Art Phillips
| cinematography = Frank Tidy Yves Langlois
| distributor    = 
| released       = August 22, 1980 (Montreal Film Festival)   May 28, 1982  
| runtime        = 
| country        =   English
| budget         = Canadian dollar|$ 3,700,000 (estimated)
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Lucky Star is a 1980 Canadian drama film.

== Plot ==
 American western westerns - where good triumphs over evil and the outcast becomes a hero.

== Recognition ==
* 1981
** Genie Award for Best Achievement in Sound Editing - Jean-Guy Montpetit - Won Art Phillips - Won Max Fischer - Won
** Genie Award for Best Achievement in Overall Sound - Michel Descombes, Patrick Rousseau - Nominated
** Genie Award for Best Motion Picture - André Fleury, Claude Léger - Nominated
** Genie Award for Best Performance by a Foreign Actor - Brett Marx - Nominated
** Genie Award for Best Performance by a Foreign Actor - Rod Steiger - Nominated

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 