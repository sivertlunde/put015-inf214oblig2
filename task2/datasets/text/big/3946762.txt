Apaharan
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Apaharan
| image          = Apaharan.jpg
| caption        = Theatrical release poster
| director       = Prakash Jha 
| producer       = Prakash Jha
| story          = Prakash Jha
| screenplay     = Manoj Tyagi Shridhar Raghavan
| starring       = Ajay Devgn Bipasha Basu Nana Patekar
| music          = Wayne Sharpe
| editing        = Santosh Mandal
| cinematography = Arvind K.
| distributor    = Entertainment One Holi Cow Pictures
| released       = 2 December 2005
| runtime        = 173 minutes
| language       = Hindi
| country        = India
| budget         =
| gross          =   
}}
 Hindi crime drama film directed by Prakash Jha and stars Ajay Devgan and Nana Patekar in the lead roles.  It is the story of a complex relationship and clashing ideologies between a father and son set in the backdrop of the kidnapping industry in the eastern state of Bihar, India. Apaharan was declared Above Average at the Indian box-office.

==Plot==
Ajay Shastri (Ajay Devgan) is an unemployed, honest graduate who dreams of joining the police force. His father, Raghuvansh Shastri (Mohan Agashe) is a highly principled and moralistic man. An ex-schoolteacher and Gandhi follower, now a social activist, Prof. Shastri expects his son to follow in his steps and believe in his ideals and values. When his fathers ideals start clashing with Ajays dreams of becoming a police officer, a rift between father and son emerges.

Ajay bribes higher officials to get his name on the police force list and, with his friend (Ayub Khan), he gets Rs.500,000 loan as well. When things go awry, Ajay and his friend decide to kidnap a government official to repay the amount. Despite all this, Ajay loves Megha (Bipasha Basu), a smart and educated young woman, who is also in love with him, but their worlds are far apart.

The kidnap goes wrong at the last minute. It turns out the official is under protection of Gaya Singh, one of Tabrez Alams (Nana Patekar) lieutenants. Tabrez Alam is a powerful and influential muslim leader. Tabrez runs a parallel government: an empire that lives off money and fear and one which Tabrez has under his thumb with the help of his right-hand man, Gaya Singh (Yashpal Sharma). Murders, extortion, bribery and, above all, kidnappings are just some of his illegal activities.

Ajay and his friends are brought to jail under police custody. It is in jail that they are tortured by Gaya Singh and his men, who runs the kidnapping business shuffling between jail and hospital. Ajay pleads with DSP Shukla to save him and promises to do anything for him and to be of a great use in future. DSP saves him as he is pissed off with Gaya Singh for not getting his share of trading and creating pressure for his transfer.

Ajay then kidnaps Sooraj Mal, one of the leading businessman and rising figure in local politics. This infuriates Tabrez as he had previously ordered Gaya Singh to kidnap Sooraj Mal to make his power felt by other businessmen but failed because of his high security. Gaya Singh goes frantic upon hearing the kidnapping and goes to kill the person upon information provided by DSP Shukla. He is surprised to find Ajay as the kidnapper who kills him after humiliating him avenging his own humiliation in jail by the hands of Daya Singh. He surrenders himself to Tabrez, and requests him to take him into his gang. Tabrez sees potential in Ajay and soon Ajay starts running Tabrezs entire empire. He takes Ajay in like a son and places him higher than his own brother Usmaan (Mukul Nag). Swimming in power, Ajay becomes the states most powerful gangster and, under Tabrezs authority, the head of Bihars most successful kidnapping trade, which Ajay consolidates by killing smaller player and removing all competitors.

The states home ministers wife is caught on camera taking money and the scandal becomes the hottest news. The home minister offers Ajay to join him and leave Tabrez Alam; Ajay refuses. He then provides the taped conversation between him and Ajay to Tabrez to make a rift between them. Meanwhile, news correspondent Akash Ranjan calls a press conference to clarify the scandal involving the home minister inviting a discussion. Tabrez sends Ajay to kill Akash so that he would bring a no-confidence motion against the government, bringing its fall. He would come to power by taking advantage of the political instability. Ajay is contacted by SP Anwar Khan who makes him aware of Tabrezs real motive. Ajay reaches the press conference venue only to find that the real person behind the ongoing debate of bribery scandal is his father. He leaves without killing Akash Ranjan and is confronted by DSP Shukla sent by Tabrez to kill him.

Ajay surrenders himself to SP Khan and gives his statement revealing everything after killing DSP Shukla. This report is presented to the home minister by the commissioner of police citing Tabrez Alams arrest warrant. The home minister makes a deal with Tabrez Alam, citing the evidence against him and both join hands to form new government in the state with the help of their respective MLA support. SP Khan is sorry for Ajay as all his efforts are ruined by the political upheaval. Ajay goes home one last time with the help from SP Khan where he watches his father reminiscing about him and make amends with him after knowing how much he loved him. He goes back to prison where Tabrez comes to meet him after becoming the new home minister of the state. He gloats in front of Ajay at the jail, but Ajay has a gun. He shoots Tabrez after they converse for a while. In return, Tabrezs men shoot Ajay and the film ends.

==Cast==

* Ajay Devgn as Ajay Shastri
* Bipasha Basu as Megha Basu
* Nana Patekar as Tabrez Alam Yashpal Sharma as Gaya Singh
* Mohan Agashe as Professor Raghuvansh Shastri Ayub Khan as Kashinat
* Anoop Soni as Akash Ranjan (Press Reporter)
* Mukesh Tiwari as SP Anwar Khan
* Daya Shankar Pandey as Daya Shankar
* Pankaj Tripathi as Gaya Singhs Crony
* Ehsaan Khan as DSP Veerendra Shukla
* Murli Sharma as Muralidhar
* Brij Gopal as Jagannath Mandal
* Akhilendra Mishra as Brijnath Mishra (Chief Minister)
* Radhakrishna Datta as Seth Soorajmal
* Mukul Nag as Usman
* Chetan Pandit as Dinkar Pandey (Home Minister)
* Padam Singh as Dharam Singh
* Prithvi Zutshi as Dwarka
* Khalid Siddiqui as Kamal Kishore
* Mrinalini Sharma
* Raj Khan as Seth Chandmal
* Prathmesh Mehta
* Khan Jahangir Khan as Salim Khan

==Box office==
Apaharan grossed     at Indian box-office till 10 weeks of theatrical run 

==Awards== 2006 National National Film Awards (India) Best Screenplay - Prakash Jha, Manoj Tyagi and Shridhar Raghavan
Filmfare Awards Best Dialogue - Neeraj Pandey Best Villain - Nana Patekar

==External links==
*  

==References==
 

 

 
 
 
 
 
 
 