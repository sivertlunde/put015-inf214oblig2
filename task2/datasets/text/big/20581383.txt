Gaon Hamara Shaher Tumhara
{{Infobox film
| name           = Gaon Hamara Shaher Tumhara
| image          = Gaon Hamara Shaher Tumhara.jpg
| image_size     = 
| caption        = 
| director       = Naresh Kumar
| producer       = 
| writer         =
| narrator       = 
| starring       = Rajendra Kumar and Rekha
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1972
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1972 Bollywood romance film directed by Naresh Kumar. The film stars Rajendra Kumar and Rekha .

==Plot==
Chandershekhar Pandey and his wife, Lajwanti, come to visit Brij Bhushan and his widowed mother in their village. They assure Brijs mother that in exchange for their land, they will get Brij a small shop in Bombay, and in this way they get the land transferred into their name. Ten years later, Brijs mother dies and Brij moves to Bhuleshwar, Bombay, where Chandershekhar lives with his wife, two unmarried sons, Ram and Shyam, and an unmarried daughter, Bindu. Brij is treated as a mere servant, not paid, but abused and beaten everyday, needless to say Chandershekhar is not going to fulfill his promise of getting him a shop. His only companion on whose shoulder he can cry is the Pandeys maidservant, Parvati alias Paro. Then things turn better for Brij when he finds himself the lucky owner of a Rs.2,50,000 lottery ticket. This suddenly changes everyones attitude toward him, he is pampered, fed, driven around town, and even given new clothes. But when the Pandeys find out that the ticket he is holding is not the winning number, Brij is once again beaten, and asked to leave. But then, a government official arrives and tells the family and Birju that he has instead won the first prize of lottery worth Rs. 5,00,000. Once again Brij is flattered by family members, but he realizes their intentions and runs away with Paro. Later there is a fight between the family and Birju in which he finally manages to scare away Pandeys family. The story end showing Birju now happily farming in the village with his wife Paro.

==Cast==
*Rajendra Kumar ...  Brij "Birju" Bhushan
*Rekha ...  Parvati (Paro)
*Sujit Kumar ...  Shyam C. Pandey
*Lalita Pawar ...  Mrs. Lajwanti C. Pandey Kanhaiyalal ...  Advocate Chandershekhar Pandey Sunder ...  Sunder (Paanwala)
*Bhagwan ...  Film Director
*Aarti ...  Bindu C. Pandey
*Kamaldeep ...  Ram C. Pandey
*Satyadeep
*Shamsher

==External links==
*  

 
 
 
 
 
 


 
 