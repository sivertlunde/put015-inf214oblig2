Voodoo Moon
 
{{Infobox television film
| name           = Voodoo Moon
| image          = voodoomoon-cover.jpg
| caption        = Voodoo Moon box cover
| director       = Kevin VanHook
| producer       = Karen Bailey
| writer         = Kevin VanHook
| starring       = Eric Mabius, Charisma Carpenter, Rik Young, Jeffrey Combs
| music          = Ludek Drizhal
| cinematography = Matt Steinauer
| editing        = Kevin VanHook
| distributor    = IDT Entertainment
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
}}
Voodoo Moon is a 2006 horror film written and directed by comic book artist and writer Kevin VanHook. It aired as a Sunday night movie on the Sci-Fi channel on June 4, 2006. 

==Plot synopsis==
A demonic being destroys an entire town, save a young boy and his sister. Twenty years later, the sister is an artist with psychic abilities and her brother has grown obsessed with tracking down the demon who took out his town. Together, they fight to destroy the evil being that could kill them both.

==Cast==
*Eric Mabius ...  Cole 
*Charisma Carpenter ...  Heather 
*Rik Young ...  Daniel 
*Jeffrey Combs ...  Frank Taggert 
*Jayne Heitmeyer ...  Lola 
*Dee Wallace ...  Mary-Ann 
*John Amos ...  Dutch 
*Reynaldo Gallegos ...  Ray (as Rey Gallegos) 
*Kimberly Hawthorne ...  Diana (as Kim Hawthorne) 
*David Jean Thomas ...  Jean-Pierre 
*Kelley Hazen ...  Helen Taggert  Geoffrey Lewis ...  Old Man #1 
*Vernon Duckett ...  Old Man #2 
*Frank Collison ...  Mac 
*Alison Lees-Taylor ...  Sally 
*Jason Rodriguez ...  Bobby 
*Cathrine Grace ...  Art Gallery Patron 
*Merritt Bailey ...  Lolas Husband 
*James DiStefano ...  Cab Driver 
*Cameron VanHook ...  Skinned Old Man 
*Georgia Anderson ...  Crucified brunette girl 
*Anissa Briggs ...  Girl at Cemetery 
*Mary Ann Farkas ...  Old Lady with Fork 
*Cathy Fitzpatrick ...  Art Gallery Patron 
*Michael Lloyd Gilliland ...  Biker Leader (as Michael Gilliland) 
*Vester Grayson ...  Rocking Chair Demon 
*Lauren M. Higgs ...  Young Woman at Motel 
*Keegan Holst ...  Boy with scissors 
*Paul Latham ...  Zombie 
*Alexis Longo ...  Scared Girl 
*Justin Ordman ...  Tom 
*Brian OSullivan ...  Thief 
*Blair Redford ...  Evil Young Man 
*Kevin VanHook ...  Cemetery Gardener 
*Scott Whyte ...  Billy 
*David Keith Anderson ...  Demon (uncredited)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 


 