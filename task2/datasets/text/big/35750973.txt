Tiger Rose (1923 film)
{{infobox film
| name           = Tiger Rose
| image          =
| imagesize      =
| caption        = Sidney Franklin
| producer       = Warner Brothers David Belasco
| based on       =  
| writer         = Edmund Goulding (scenario) Millard Webb (scenario)
| starring       = Lenore Ulric
| music          =
| cinematography = Charles Rosher
| editing        =
| distributor    = Warner Brothers
| released       = December 9, 1923
| runtime        = 90 minutes; 8 reels
| country        = United States Silent (English intertitles)
}} silent adventure talkie by George Fitzmaurice. The SilentEra database lists this film as surviving.    

==Cast==
*Lenore Ulric - Rose Bociion; TigerRose
*Forrest Stanley - Michael Devlin
*Joseph J. Dowling - Father Thibault
*George Beranger - Pierre (*as Andre de Beranger)
*Sam De Grasse - Dr. Cusick
*Theodore von Eltz - Bruce Norton
*Claude Gillingwater - Hector McCollins

unbilled
*Frances Starr - unknown role

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 