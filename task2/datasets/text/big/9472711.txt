Educating Rita (film)
 
 
{{Infobox film
| name           = Educating Rita
| image          = Educating rita uk.jpg
| caption        = Theatrical release poster
| director       = Lewis Gilbert
| producer       = Lewis Gilbert
| screenplay     = Willy Russell
| based on       =  
| starring = {{Plainlist|
* Michael Caine
* Julie Walters
}}
| sound         = Michel Cheyko
| cinematography = Frank Watts
| editing        = Garth Craven
| studio         = Acorn Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English
| budget         = ₤4 million 
| gross          = $14.6 million (USA)  
}} on Russells stage play. The film stars Michael Caine, Julie Walters, and Maureen Lipman. It won multiple major awards for best actor and best actress and was nominated for three Academy Award|Oscars.

==Plot==
Liverpudlian working-class young woman&nbsp;&ndash; hairdresser&nbsp;&ndash; Rita (Julie Walters) wants to better herself by studying literature. Her assigned Open University professor, Frank Bryant (Michael Caine), however, has long ago openly taken to the bottle, and soon develops misgivings about Ritas ability to adapt to student culture. Bryant is a jaded university lecturer, who describes his occupational ability as "appalling but good enough for his appalling students". His passion for literature is reignited by Rita, whose technical ability for the subject is limited by her lack of education but whose enthusiasm Frank finds refreshing.

==Theme==
The film focuses on Ritas unhappiness with her life in her blue-collar, working-class environment including her husband, who wants her to have children and stop with this university stuff, as well as her struggles to fit into a new educated middle-class existence in academia, while seeking a "better song to sing".  Ritas original preconceptions that the educated classes have better lives and are happier people are brought into question throughout the film through Franks failing social life and alcoholism and her flatmate Trishs attempted suicide. Rita, her search, and her searchs meaning for her, all evolve as she adapts to academia and grows as a person.

==Cast==
* Michael Caine as Prof. Frank Bryant
* Julie Walters as Susan "Rita" White
* Michael Williams (actor) as Brian (Professor and husband of Elaine)
* Dearbhla Molloy as Elaine (wife of Brian)
* Jeananne Crowley as Julia (Girl-Friend of Frank Bryant)
* Malcolm Douglas (actor) as Denny (Husband of Susan/Rita)
* Godfrey Quigley as Ritas Father
* Patricia Jeffares as Ritas Mother
* Maeve Germaine as Sandra (Sister of Rita)
* Maureen Lipman as Trish (Roommate of Rita)
* Gerry Sullivan as Security Officer
* Pat Daly as Bursar
* Kim Fortune as Collins
* Philip Hurdwood as Tiger
* Hilary Reynolds as Lesley Jack Walsh as Price
* Christopher Casson as Professor
* Gabrielle Reidy as Barbara
* Des Nealon as Invigilator
* Marie Conmee as Customer in Hairdressers
* Oliver Maguire as Tutor
* Derry Power as Photographer
* Alan Stanford as Bistro Manager

==Production==
Julie Walters, in her feature film debut, reprised her role from the stage production.

The film was shot in Dublin. Trinity College, Dublin, is used as the setting for the university, and University College Dublin, in Belfield, Dublin, is used for Ritas summer school. The rooms used by Bryant as his office and tutorial room were those of the College Historical Society and the University Philosophical Society, respectively; and while the building was considerably refurnished, the production chose to leave portraits of Douglas Hyde and Isaac Butt and committee photographs in the former, and a bust of John Pentland Mahaffy in the latter. No. 8 Hogan Avenue in Dublin 2 near Grand Canal Dock was used for Ritas house in the film, and one in Burlington Road, Ballsbridge for Bryants. The scene where Rita runs into her ex Denny and his new wife was filmed in the South Lotts area of Ringsend. The scene in France was filmed in National University of Ireland, Maynooth|Maynooth, County Kildare, and Pearse Station and Dublin Airport were also used. The scene in the pub was shot in The Stags Head pub on Dame Court in Dublin. However, the pub which Rita enters is the Dame Tavern which is opposite The Stags Head. Filming also took place in Stonybatter with Aughrim St Church being used for the wedding scene. Stanhope St school was used as a production base. 

==Reception==

===Critics=== Variety lauds Walters interpretation of Rita as " itty, down-to-earth, kind and loaded with common sense."  "Rita," the review continues, "is the antithesis of the humorless, stuffy and stagnated academic world she so longs to infiltrate. Julie Walters injects her with just the right mix of comedy and pathos." 

Ian Nathan reviewing the film in British Empire magazine calls the film a "gem," and gives it four out of five stars. He describes Walterss "splendidly rich interpretation" of Rita and characterises her "reactions to the traditions of English lit    carry  the caustic brilliance of true intelligence, a shattering of blithe pretension..."  Of Walters and Caine, Nathan opines, " hey make a beautifully odd couple, in a love story at one remove."  This reviewer depicts the directors effort as "effective, and finally optimistic," and observes about the film that the playwrights "angry message that people are trapped by their environment not their abilities, is salved by the sweetness of   final parting."  Nathans "verdict" of the film is one of " harming, glittering characterisations that, though they dont run deep, nevertheless refresh." 

American critic Janet Maslin called the film "an awkward blend of intellectual pretension and cute obvious humour ... the perfect play about literature for anyone who wouldnt dream of actually reading books"; she noted that "the essentially two-character play has been opened up to the point that it includes a variety of settings and subordinate figures, but it never approaches anything lifelike." 
 formula relationship"; screen adaptation "added mistresses, colleagues, husbands, in-laws, students and a faculty committee,   all unnecessary" and said the playwright/screenwriter "start  with an idealistic, challenging idea, and then cynically tr  to broaden its appeal."   

===Awards and major nominations===
 Best Film, Best Newcomer, Best Supporting Best Adapted Screenplay.

Caine and Walters also took Best Actor and Best Actress, respectively, at the Golden Globes. The much awarded film was further nominated for but did not win Best Actor (Caine) and Best Actress (Walters) as well as Best Writing for derivative screenplay (Russell) at the 56th Academy Awards. 

===Retrospective assessments===
In 1999, the film was among the BFI Top 100 British films.

In 2007, while promoting the remake of Sleuth (2007 film)|Sleuth, Caine called Educating Rita "the last good picture   made before   mentally retired." 

==Professors mistake==

To illustrate the rhyming principle of assonance, the repetition of vowel sounds, Dr. Bryant gives as an example the words swans and stones from W B Yeatss The Wild Swans at Coole. This is instead an example of literary consonance|consonance, the repetition of consonant sounds. However, in a scene just before Rita meets Dr. Bryant, some students mockingly observe, "He doesnt even know what assonance means!", confirming that it is Dr. Bryants mistake, not the films. In an ensuing discussion between Dr. Bryant and Rita, he asks her whether she understands assonance.  Rita replies, "Yeah, it means youve got the rhyme wrong."

==Proposed remake==
In November 2002, the then-82-year-old director Lewis Gilbert went public with plans to remake his film "with a black cast that could include Halle Berry and Denzel Washington", with principal photography to commence in 2003. The project, however, never got off the ground. 

==References==
 

==External links==
*   at BFI Screenonline
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 