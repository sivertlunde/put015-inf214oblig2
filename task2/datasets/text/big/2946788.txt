Blonde Ambition
 
{{Infobox film
| image = Jessicablonde.jpg caption = DVD cover
| name = Blonde Ambition Scott Marshall
| producer = Justin Berfield Jason Felts David McHugh
| starring = Jessica Simpson Luke Wilson Drew Fuller Paul Vogt Willie Nelson Rachael Leigh Cook
| distributor = Screen Gems  Sony Pictures Entertainment
| released = December 21, 2007
| runtime =  93 minutes English
| budget = $40,000,000
| gross = $1,530,479 
}}

Blonde Ambition is an American film released in December 2007 and inspired by the theme of the Academy Award-winning movie Working Girl, starring singer/actress Jessica Simpson playing the part of a small town girl who moves to New York City and rises up into a career as a business woman. The film also stars Luke Wilson, Paul Vogt and actor/comedian Andy Dick.

Before the movie started filming, the media reported that Blonde Ambition was a remake of the 1980s film Working Girl. After Simpson learned about this rumor, she talked to Empire Online and stated, "I dont know where that came from", "its a movie called Blonde Ambition co-starring Luke Wilson. Its definitely the theme of Working Girl - this small town girl that moves to New York City to rise up into this great career as a business woman pretty much. But its definitely not a remake." Simpson also said that this film is more of a knockabout comedy than the Melanie Griffith starrer, which was a drama genre movie.

The official trailer for the movie leaked to the Internet on early May 2007,  a full version of a pre-release DVD was leaked on December 16, 2007.

==Box office==
It was released into eight theatres in Texas, the home state for stars Simpson and Wilson, on December 22, 2007 before a DVD release date on January 22, 2008.  Blonde Ambition averaged $48 per screen on December 22, 2007 for a total box office of $384 . This meant, based on an $8 ticket price, that only 6 people paid to see the movie at each of those eight theatres and only 48 people in total went to see the movie. 

Opening weekend, the film finished 54th at the North American box office with a three-day gross of $1,332 and a per-theatre-average of $165.  {{cite web url        = http://www.boxofficemojo.com/movies/?id=blondeambition.htm title      = Blonde ambition gross publisher  = BOX OFFICE MOJO author     = Lacey Rose
   |date=        2008-02-10 accessdate =
}}   In Ukraine, however, the film opened at #1 in its first weekend (February 16–17, 2008), earning $253,008.  In Russia, Blonde Ambition opened up at #7 at box office and has grossed $399,854 to date, and in the Philippines it opened at #5 and has grossed $16,538 to date. 

Blonde Ambition, despite being a box office disappointment, impressively grossed $2.7 million in the first five days of DVD release ranking #23 on the DVD sales chart. This is possibly due to Jessica Simpsons fanbase supporting her.  Since the movies DVD release date it has grossed $11.56 million in the United States. 

==Plot==
When Katie Gregerstitch visits her boyfriend Billy in New York, she finds him in bed with another woman. Thus, Billy breaks up with Katie, who then leaves to stay with her close friend Haley. She convinces Katie to start an independent life of her own, instead of sitting around doing nothing. One day, when Haley has to go to an audition, Katie takes over her job as a messenger and eventually has to deliver a shipment to a large building construction company.

The deputy director of that company, Debra, preys on the job of Richard and has come up with a plan to frame Richard for sabotage, in order to take over the company. To help herself with that, she makes sure that Richard’s secretary gets dismissed and that the naive Katie takes her place. Katie meanwhile meets Ben, who works as a postman in the building, and befriends him.

After losing a contract, by Debra’s doing, Katie gets fired, but by using her charms, she manages to get a second chance from Richard. When she manages to get the interest of a group of Norwegian investors, Richard shares an important confidential project with her. Debra manages to get the confidential information out of Katie and thereby make sure that the Board of Directors fires Richard.

Meanwhile, Katie has discovered that Ben is actually Richard’s son. Together with him, she devises a ruse to outsmart Debra. When Debra proposes the project to the investors, she sends out Haley and some friends, who pose as the investors. At the same time, she presents her own proposal to the real investors, who satisfyingly take it. When Debra discovers the deception, she goes ballistic and thus gets fired.

==Cast==
  
* Jessica Simpson as Katie Gregerstitch
* Luke Wilson as Ben Connelly
* Drew Fuller as Billy
* Rachael Leigh Cook as Haley 
* Paul Vogt as Floyd
* Andy Dick as Freddy
* Bill Jenkins as Robert Perry
* Karen McClain as Betty
  Larry Miller as Richard Connelly
* Penelope Ann Miller as Debra
* Piper Mackenzie Harris as Amber Perry
* Sarah Ann Schultz as Samantha
* Dan Braverman as cab driver
* Willie Nelson as Pap Paw
* Casey Keene as office assistant
* Ryan Dunn as Griswold
 

== Soundtrack ==
* Jessica Simpson - "A Public Affair" (For Trailer Purposes Only)
* KT Tunstall - "Suddenly I See" (For Trailer Purposes Only)
* Lily May - "I Got Love"
* Monique Ximenez - "You Can Fly"
* Austin - "Lets Make Love"
* Hipjoint feat. Stef Lang - "Girl From Nowhere"
* MegaJive - "The Way I Rock"
* Stretch Nickel - "Beautiful Day"
* Price - "Something In Your Eyes"
* Hugh James Hardman - "Live A Little"
* Adam Ant - "Wonderful"

== DVD release ==
The DVD was officially released in the US on January 22, 2008, and has also been released on iTunes. In the United Kingdom and Ireland the film was released direct to DVD in June 2010.

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 