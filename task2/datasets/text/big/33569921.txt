Immer die Radfahrer
{{Infobox film
| name           = Immer die Radfahrer
| image          = 
| image_size     = 
| caption        = 
| director       = Hans Deppe
| producer       = Alfred Stöger (producer) Kurt Ulrich (producer)
| writer         = Hans-Joachim Kulenkampff (story) Wolf Neumeister (screenplay)
| narrator       = 
| starring       = See below Hans Lang
| cinematography = Elio Carniel
| editing        = Renate Jelinek
| studio         = 
| distributor    = 
| released       = 1958
| runtime        = 97 minutes
| country        = Austria West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Immer die Radfahrer is a 1958 Austrian / West German film directed by Hans Deppe.

== Plot summary ==
As students Fritz (Heinz Erhardt), Ulrich (Hans-Joachim Kulenkampff) and Johannes Wolf Albach-Retty went biking in Carinthia (state)|Carinthia. Twenty-five years later they have made their ways. Fritz is a producer of alcoholic beverage, Ulrich is a stage actor and Johannes a teacher. When they look back at this biking tour they come across their mutual wish to feel once again as carefree as they did then. Consequently the old friends decide altogether to take a time-out from all their duties and to enjoy themselves by repeating the endeavour. But things become complicated when Katinka, the future daughter-in-law of Fritz confronts him because shes mad about his son (Robby, played by Peter Kraus) and decides to hide from him (Robby) by joining the three friends. The wives of Fritz and Johannes and Ulrichs girl-friend hear rumours about their partners getting involved with women along the way. So they pick up their trace and follow them secretly. Indeed the men run into their old flings, unaware of being watched...

== Cast ==
*Heinz Erhardt as Fritz Eilers
*Hans-Joachim Kulenkampff as Ulrich Salandt
*Wolf Albach-Retty as professor Johannes Büttner
*Waltraut Haas as Tilla Büttner
*Mady Rahl as Malchen Eilers
*Katharina Mayberg as "pussycat" Beryl
*Corny Collins as Katinka
*Christiane Hörbiger as Angelika Zander
*Inge Meysel as Sylvia Koschinsky
*Vera Balser-Eberle
*Edith Elmay as Uschi
*Eva Fichte
*Traute Duscher as Lotte
*Antonia Mittrowsky as Marianne Hopfleder
*Erna Schickl as Grete Köck
*Günther Bauer as tenor Bert Erichsen
*Walter Janssen as theatre manager Popp
*Peter Kraus as Robby Eilers

== Soundtrack ==
*Polydor - "Mit Siebzehn" (Music:  )

== External links ==
* 
* 

 
 
 
 
 
 

 