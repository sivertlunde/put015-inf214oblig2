The Youth of Maxim
{{Infobox film
| name           = The Youth of Maxim
| image          = The Youth of Maxim (poster).jpg
| image_size     = 200px
| caption        = Film poster
| director       = Grigori Kozintsev Leonid Trauberg
| producer       = 
| writer         = Grigori Kozintsev Leonid Trauberg
| narrator       = 
| starring       = Boris Chirkov
| music          = 
| cinematography = Andrei Moskvin	 	
| editing        = 
| distributor    = 
| studio         = Lenfilm
| released       =  
| runtime        = (98 minutes)
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

The Youth of Maxim ( ) is a 1935 Soviet film directed by Grigori Kozintsev and Leonid Trauberg, the first part of trilogy about the life of a young factory worker named Maxim.

==Plot==
In 1910, a revolutionary underground group spreads leaflets featuring anti-tsarist slogans. Maksim, a young, happy-go-lucky worker and his comrades help the teacher Natasha, who is engaged in illegal activities in the factory, hide from the police. 

Maksims friend Andrei and another worker lose their lives. Their funeral turns into a huge demonstration which is suppressed by the police. Numerous people are arrested, among them Maksim, who subsequently becomes a Social Democratic activist.  

==Cast==
* Boris Chirkov - Maksim
* Valentina Kibardina - Natasha
* Mikhail Tarkhanov - Polivanov
* Stepan Kayukov - Dmitri "Dyema" Savchenko
* Aleksandr Kulakov - Andrei
* Boris Blinov
* S. Leontyev	
* M. Shelkovsky
* Vladimir Sladkopevtsev
* Leonid Lyubashevky	
* Pavel Volkov - The workman with the accordion (uncredited)

==References==
* 

==External links==
* 

 
 

 
 
 
 
 
 
 
 

 