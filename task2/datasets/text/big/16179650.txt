Oru Naal Oru Kanavu
{{Infobox film
| name = Oru Naal Oru Kanavu
| image = 
| caption = Fazil
| story = T. A. Lal Fazil
| screenplay = Fazil Srikanth Sonia Agarwal Nizhalgal Ravi
| producer = K. Muralidharan V. Swaminathan G. Venugopal
| music = Ilaiyaraaja
| cinematography = Ananda Kuttan
| editor = T. R. Sekhar K. R. Gowri Shankar
| studio = Lakshmi Movie Makers
| released =  
| runtime =
| language = Tamil
| country = India
| budget =
}}

Oru Naal Oru Kanavu ( : One day, one dream) is a 2005 Indian Tamil romance film directed by Fazil (director)|Fazil.  Srikanth (actor)|Srikanth, Sonia Agarwal and Nizhalgal Ravi play the lead roles and it has music by Ilaiyaraaja and camera by Anandakuttan.   Oru Naal Oru Kanavu his 27th film, released on August 26, 2005. 

==Plot==
The story revolves around Maya (Sonia Agarwal), a college student, who always wants to triumphs in all her endeavors. She comes across a middle class youth, Cheenu (Srikanth (actor)|Srikanth), a happy-go-lucky-youngster, who strives to work hard to get his sisters married well.

A few encounters (Maya and Cheenu) results in wordy duels between them leading to Maya challenging Cheenu that she will make him fall in love with her. Her further course of action to achieve her challenge leaves Cheenu frustrated and he decides to take revenge on her by pretending to fall in love with her.

Maya who starts to spend more time with Cheenu comes to know of his kind heart and his good nature and eventually develops an honest love towards him. She even helps him in setting his own business house. Slowly Cheenu too comes to know of Mayas nobleness and generosity land falls in love with her.

Trouble comes in the form of her brothers who threaten Cheenu of dire consequences. Cheenu resolves to earn more and become rich and then hold the hands of Maya. But Maya has plans otherwise. Does the couple end up getting married forms the rest of the story.

==Cast== Srikanth as Srinivsan (Cheenu)
* Sonia Agarwal as Mayadevi (Maya)
* Nizhalgal Ravi
* Remya Nambeesan as Vanaja
* Saranya Mohan as Meera
* Ilavarasu as Rajamani
* Shyam Ganesh
* Madhupal
* Suvarna Mathew Sathyan
* Sukumar
* Bombay Gnanam
* Sudha as Srinivasans mother
* Sridevi as Vasanthi

==Music==
The soundtrack was done by Ilayaraja. The song "Kaatril varum geethame" was penned by Vali and was considered an instant hit by music lovers.

Enna Paada Venum - Sonu Nigam

Ilamaikkoor Vegam - Sonu Nigam

Kaatril Varum - Bhavatharini, Hariharan (singer)|Hariharan, Ilaiyaraja, Sadhana Sargam, Shreya Ghosal

Khajiraho - Hariharan, Shreya Ghoshal

Konjum Thira - Shreya Ghosal, Sonu Nigam
 Tippu

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 