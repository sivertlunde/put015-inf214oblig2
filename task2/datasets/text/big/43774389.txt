Treacle Jr.
 
{{Infobox film
| name           = Treacle Jr.
| image          =
| caption        =
| director       = Jamie Thraves
| writer         = Jamie Thraves Tom Fisher Riann Steele
| music          =
| cinematography =
| editing        =
| producer       = Soda Pictures
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Treacle Jr. is a British 2011 film written and directed by Jamie Thraves.  It was filmed on location in Southwark, London, England, United Kingdom. 

==Plot== Tom Fisher) sitting eating breakfast with his family. He drives away from his house to Birmingham New Street, where he boards a train; he gets off in London, finds a spot outside a shop and lays down. When he wakes up, he goes to the park, where he opens his wallet, destroys his bank cards and throws them (along with his wallet) in the bin and puts the money in his back pocket.

That night, he is sitting drinking cans of beer on a bench in the park, where he is mistaken for a gay man by a gang of youths; he tries to get away, but ends up running into a tree, knocking himself out.

He awakes the next morning and goes to A&E, while sitting in the waiting room, he hears a man (Aidan Gillen) talking loudly and disturbing the receptionist, the man then takes a seat next to Tom and starts to talk to him, asking him his name, he is then called through to see a doctor. Tom is next seen walking away from the hospital, but Aidan catches him up and proceeds to follow him.

Next, we see Tom and Aidan walking through a graveyard, Aidan tells Tom that he needs to go to the toilet, so he goes to urinate behind a tree, Tom, seeing his chance to escape, runs away from Aidan, while running, he is stopped in his tracks when he spots a couple having sex against one of the gravestones, he carries on running but accidentally runs into Aidan again. They carry on walking, when Tom suddenly faints, Aidan decides to take him back to his flat.

At Aidan’s flat, Aidan gives Tom a beer, while they are talking, Aidans girlfriend Linda (Riann Steele) comes home, upon her arrival, Tom is shocked to discover (unbeknownst to her) that she is the woman he saw having sex in the graveyard with another man, despite the fact that she is supposed to be with Aidan, she is not pleased that Aidan didn’t make enough money, so she punches him in the face, then she tells him to throw Tom (who she calls Lurch) out of the flat.

Tom leaves and goes to buy a cup of tea from a street vendor, he reaches for some money to pay, but his pocket is empty, so he goes back to the flat, Linda opens the door, he asks her if she has seen his money, but she says No, so he politely asks if he can come in and have a look around, Linda refuses and tells him to go away, as Tom walks away, Linda looks through the curtain on the door and smirks.

Tom goes back on the streets, he is seen begging various people for money, one man is kind enough to give him some change, so he uses it to buy a cup of tea from a café, while he is sat down, the door burst open and in comes Aidan with a cat basket, he asks the woman on the counter if he could talk to the manager about rats, hoping to release the cat to catch them. The manager lets him through to the back, but Tom spots the man throwing Aidan out into the back alley. Tom runs out of the shop and hears Aidan being beaten up, so he goes to investigate, he finds Aidan and tries to see if he can tell if Aidan has a concussion, while they are talking, Aidan suddenly jumps up, realizing that the cat (Treacle), who belongs to one of Aidan’s elderly neighbors is missing, they go in search of the cat, when they eventually find it they find that it’s dead, they then go to a pet shop and buy a kitten, (which Aidan names Treacle Jr), and attempt to give it to the woman, but she yells at them, telling them to go away, calling Aidan a F***** retard.

Aidan takes Tom to a music shop where he shows him a drum kit that he wants to save up for, walking away from the shop; Tom starts to get annoyed by Aidan and eventually tells him to leave him alone and Tom takes off.

Tom is seen on the steps of the town hall, feeling depressed, he opens his wallet and takes out a picture of him with his family, when he does, a bank card falls out from behind the picture, Tom picks it up and rushes away to a cash machine.

Tom goes back to Aidans flat, where he apologizes, Aidan accepts and Tom asks him if he can stay for a while and offers him some money for rent, Tom, Linda and Aidan then have a housewarming party, during the party, Linda discovers that Aidan has received the money from Tom, which Aidan says he can add to his savings to buy the drum kit, annoyed that she didn’t know about Aidan’s money, she corners Tom in the Kitchen, berates him, then sexually assaults him, annoyed and embarrassed, Tom abruptly leaves the kitchen and goes to bed.

The next day Aidan is walking home, when he gets jumped from behind by the man Linda was having sex with in the graveyard; he steals the money Tom gave Aidan. Tom finds out what happened to Aidan and realizes from the description Aidan gives him, that it is Lindas friend. Linda returns home with bags full of designer clothes, Tom then tells Aidan that Linda stole his money, Aidan shouts at Linda and she hits him and kicks him to the floor, while she is attacking him, Tom shouts "Leave him alone!", Leave him alone!", Linda tells him to shut up or shell get one of her friends to cut his f*****g head off. Tom grabs a knife from the side and threatens Linda, he tells her that hell kill her if she doesnt leave him alone, in shock, Linda has an asthma attack, Aidan tries to get her inhaler, but he drops it and kicks it under the fridge, Linda looks like she is going to choke to death, as they scramble to get it back.

Final day, Aidan goes back to the music shop to see the drum kit he wants, but when he gets there, he finds out that it has been sold, upset Aidan goes home, he goes into his bedroom, where he finds the drum kit with a note from Tom, telling him that he bought it for him as a Thank you. The film ends with Aidan playing the drums (while the camera), acting as Tom enters a train platform and boards the train home.

==Cast== Tom Fisher as Tom
*Aidan Gillen as Aidan
*Riann Steele as Linda
*Spencer Cowan   as Honeytrap Thug

==Reception==
It currently holds an 83% positive review on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  

 
 
 