Bulldog Drummond's Revenge
{{Infobox film
| name           = Bulldog Drummonds Revenge
| image_size     =
| image	         = Bulldog Drummonds Revenge FilmPoster.jpeg
| caption        =
| director       = Louis King Stuart Walker (producer)
| writer         = Edward T. Lowe Jr. (writer) Herman C. McNeile (novel The Return of Bulldog Drummond)
| narrator       = John Howard
| music          =
| cinematography = Harry Fischbeck
| editing        = Arthur P. Schmidt
| distributor    = Paramount Pictures
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
 Stuart Walker, John Howard Bulldog Drummond) Lost Horizon.  Billing (filmmaking)|Top-billed John Barrymore portrays his friend Colonel Nielsen.

==Plot== Captain Hugh British officer who, while on a drive with his friend Algy Longworth and valet Tenny, is the first to discover a mysterious suitcase that is parachuted from an aircraft above, minutes before the plane crashes. The case is found to contain a highly explosive chemical, the plans for which have been stolen, and despite the urging of his fiancee Phyllis Claverling, Drummond is dragged into the mystery surrounding the whole affair, traveling by both train and ship to recover the formula.

==Cast==
*John Barrymore as Col. J.A. Nielson
*John Howard as Bulldog Drummond|Capt. Hugh Chesterton Bulldog Drummond Louise Campbell as Phyllis Clavering Reginald Denny as Algy Longworth
*E.E. Clive as "Tenny" Tennison
*Frank Puglia as Draven Nogais
*Nydia Westman as Gwen Longworth
*Robert Gleckler as Hardcastle
*Lucien Littlefield as Mr. Smith John Sutton as Jennings, Nielsons Secretary
*Miki Morita as Sumio Kanda
*Benny Bartlett as Cabin Boy Matthew Boulton as Sir John Haxton

==Soundtrack==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 