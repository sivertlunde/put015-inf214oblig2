Streets of Laredo (film)
{{Infobox Film
| name           = Streets of Laredo
| image          = Poster of the movie Streets of Laredo.jpg
| image_size     = 
| caption        = 
| director       = Leslie Fenton
| producer       = Robert Fellows
| writer         = Louis Stevens (story) Elizabeth Hill (story) Charles Marquis Warren King Vidor (uncredited)
| narrator       = 
| starring       = William Holden Macdonald Carey Mona Freeman William Bendix
| music          = Victor Young
| cinematography = Ray Rennahan
| editing        = Archie Marshek
| distributor    = Paramount Pictures
| released       = May 27, 1949
| runtime        = 93 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1949 western Texas Rangers, while the third continues on a life of crime.
 The Texas Rangers (1936), which starred Fred MacMurray in Holdens role, Jack Oakie in William Bendixs, Lloyd Nolan in MacDonald Careys role, and Jean Parker as the girl they rescue.

==Plot==

A trio of outlaws, Jim Dawkins (Holden), Loren Renning (Carey), and Reuben "Wahoo" Jones (Bendix), rob a stage. But when a young lady, Rannie Carter (Freeman), is menaced by rich and ruthless Charley Calico (Alfonso Bedoya) after her father is killed, the robbers come to her rescue. They run him off, then pay old Pop Lint (Clem Bevans) to watch over her at his ranch.

Loren ends up separated from his partners but continues his life of crime. Jim and Wahoo inadvertently aid some Texas Rangers and are sworn in as Rangers themselves. Loren sees an opportunity, steals a herd of cattle the Rangers are guarding, then lets Jim and Wahoo enhance their reputation by being the ones who bring the cattle back.

Lorens friends turn a blind eye to his activities for a while. Calico is a worse villain, burning Pops barn and causing the old man to have a fatal heart attack. Calico assaults a Ranger as well, and is ultimately killed by Jim.

But it doesnt end there. Loren now wants Calicos empire for himself. He also wants Rannie, who has grown to be a beautiful woman. Jim, who loves her, calls off the agreement to look the other way at Lorens misdeeds. But he does remove a bullet when a wounded Loren hides out at Rannies after a holdup.
 Laredo for a showdown. The former partners face each other for the last time, then Loren is killed.

==Cast==
*William Holden as Jim Dawkins
*Macdonald Carey as Loren Renning
*Mona Freeman as Rannie Carter
*William Bendix as Reuben "Wahoo" Jones
*Stanley Ridges as Major Bailey
*Alfonso Bedoya as Charley Calico
*Ray Teal as Cantrell
*Clem Bevans as Pop Lint James Bell as Ike
*Dick Foote as Texas Ranger Pipes
*Joe Dominguez as Texas Ranger Francisco
*Grandon Rhodes as Phil Jessup
*Perry Ivins as Mayor Towson
*Hank Bell as a Texas Ranger

==Inspiration== The Streets of Laredo".

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 