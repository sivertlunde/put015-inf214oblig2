Karunamayudu
{{Infobox film
| name           = Karunamayudu 
| image          = Karunamayudu.jpg
| caption        =
| director       = A. Bhimsingh|
| producer       = Vijayachander
| writer         = Christopher Coelho, Modukuri Johnson (dalogues)
| starring       = Vijayachander Kongara Jaggaiah Sreedhar Surapaneni
| music          = Joseph Fernandez, B. Gopalam
| cinematography = K. S. Prasad
| editing        =Paul Doraisingh, B. Lenin
| distributor    = Radha Chitra
| released       =  December 21, 1978
| runtime        = 160 minutes
| country        = India Telugu
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         = | Gross          = }}


Karunamayudu (Telugu: కరుణామయుడు) is a 1978 Telugu film directed by A. Bhimsingh and starred Vijayachander as Lord Christ. The film won the bronze Nandi Award for Best Feature Film, was dubbed in Hindi as Daya Sagar, Tamil as Karunamoorthy and English as Ocean of Mercy. The most notable aspect about this movie is the two song sequences picturised to narrate the Birth of Jesus and enumerate his entry into Jerusalem with dancers welcoming as splendid spectacular which has never been shown in any of previous Jesus related movies 

==Film as Basis for Evangelization in India==
Directed and produced in India, with all-Indian actors, Karunamayudu was seen by many missionaries as one of the most culturally relevant tools for Christian evangelization in India.

In India, many of the villagers encounter a story that they are unfamiliar with: the Life of Christ.  The gospel is rendered in their cultural context when the film is shown.

Karunamayudu is a professionally produced film that was initially released in cinema houses throughout India.  Native actors performing in the same language and lifestyle as the people watching the film watch a depiction of the life of Jesus Christ. Karunamayudu has been the pioneer in culturally-relevant film evangelism for over thirty years.

==Credits==
===Cast===
* Vijayachander - as Ishu (Jesus Christ).
* Kongara Jaggaiah Surekha as Mother Mary Chandra Mohan
* Mukkamala Krishna Murthy
* Rajasulochana
* Sumalatha

===Crew===
* Director: A. Bhimsingh
* Co-director: Fr. Christopher Coelho, OFM
* Playback Singers: S. P. Balasubrahmanyam, Vani Jayaram, V. Ramakrishna

==Other language titles==
*Hindi - Daya Sagar. Tamil - Karunamoorthy. English - Ocean of Mercy.

==Songs==
* Devudu Ledani Anakunda (Singer: S.P. Balasubrahmanyam)
* Kadile Muvvala Sandadilo (Singer: Vani Jayaram)
* Kadilindi Karuna Radham (Singer: S.P. Balasubrahmanyam)
* Paripoorna Kreestu
* Puvvulakanna Punnami Vennelakanna (Singer: V. Ramakrishna)

==References==
 

==External links==
* 
*  
*  
*  

 

 
 
 
 