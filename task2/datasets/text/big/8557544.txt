The Terror of Tiny Town
{{Infobox film
| name           = The Terror of Tiny Town
| image_size     =
| image	=	The Terror of Tiny Town FilmPoster.jpeg
| caption        =
| director       = Sam Newfield
| producer       = Jed Buell (producer)  Abe Meyer (associate producer)  Bert Sternbach (associate producer)
| writer         = Fred Myton (writer)  Clarence Marks (additional dialogue)
| narrator       =
| starring       = See below
| music          =
| cinematography = Mack Stengler
| editing        = Martin G. Cohn  Richard G. Wray
| distributor    =
| released       = December 1, 1938
| runtime        = 62 minutes
| country        = United States English
| budget         = $100,000 (estimated)
| gross          =
}}
  musical Western Western with dwarf cast.
 Shetland ponies while roping Calf|calves.

==Plot summary==
The plot is about a cowboy helping out a beautiful ranch owner menaced by local thugs.

==Cast==
*Billy Curtis as The Hero (Buck Lawson)
*Yvonne Moray as The Girl (Nancy Preston)
*Little Billy Rhodes as The Villain (Bat Haines)
*Billy Platt as The Rich Uncle (Jim Tex Preston)
*John T. Bambury as The Ranch Owner (Pop Lawson)
*Joseph Herbst as The Sheriff
*Charlie Becker as The Cook (Otto)
*Nita Krebs as The Vampire (Nita, the dance hall girl)
*George Ministeri as The Blacksmith (Armstrong)
*Karl Karchy Kosiczky as The Barber (Sammy)
*Fern Formica as Diamond Dolly
*William H. ODocharty as The Old Soak
*Jerry Maren as Townsperson
*Clarence Swenson as Preacher
 The Wizard of Oz, released in 1939.

==Reception==
Not only did the film receive negative reviews, but it also was a commercial bomb, grossing an untold amount of money out of a budget of $100,000, which adjusted for inflation would be approximately $1.6 million in 2013. Additionally, it also was included in The Golden Turkey Awards, and was also featured on a bottom-10 list in The Book of Lists. Years later, The Terror of Tiny Town was included as one of the choices in the book The Fifty Worst Films of All Time.

In 1986, the movie was featured in an episode of the Canned Film Festival. 

The film was referenced in the season 8 episode of M*A*S*H*, Morale Victory, incorrectly called "Terror in the Tiny Town".

==See also==
*Even Dwarfs Started Small – German film featuring an all-dwarf cast
*List of films in the public domain
*List of Columbia Pictures films

==References==
{{reflist|refs=
   
}}

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 