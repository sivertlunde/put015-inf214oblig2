Azhagan
{{Infobox film
| name = Azhagan
| image =
| caption = Official DVD Box Cover
| director = K. Balachander   
| producer = Kovai Chezhiyan
| writer = K. Balachander
| narrator = Geetha   Madhoo Babloo Prithviraj Maragadha Mani
| cinematography = Raghunatha Reddy
| editing = Ganesh Kumar
| studio = Kovai Chezhiyan |K. C. Film Combines
| distributor = Kavithalayaa Productions
| released =   
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 1991 Indian Tamil language film directed by K. Balachander, produced by Kovai Chezhiyan, starring Mammootty, Bhanupriya, Geetha (actress)|Geetha, Babloo Prithviraj and Madhoo.  It marked the debut of Madhoo.

==Plot==
Azhagappan (Mammootty) a successful hotelier and father of four small children has lost his wife (whose face is not shown in the photo) in an accident. College student Swapna (Madhoo) play pranks on him and also falls in love with him but Azhagappan refuses her love considering the age difference between him. Though illiterate Azhagappan studies in a tutorial centre where his teacher Kanmani (Geetha (actress)|Geetha) is smitten by him but Azhagappan is not interested in her. Classical dancer Priya Ranjan (Bhanupriya) and Azhagappan though fall in love. After egos and differences crop up between them, Driver Santhanam blurts out the truth that the four children are the orphans adopted by him. In the end, Swapna understands the situation and would love to call him as "Daddy". With the help of Kanmani and Swapna, four children devises a plan and plays a mediator between Azhagan and Priya by making them speak on the phone. In the end, both reveal their love through the phone and get united in the end.

==Cast==
* Mammootty as Azhagappan
* Bhanupriya as Priya Ranjan
* Madhoo as Swapna Geetha as Kanmani Prithviraj as Azhagappans servant
* Kavithalaya Krishnan
* K. S. Jayalakshmi as Swapnas teacher
* Sujitha as Baby (Azhagappans daughter)
* Charle as Television Anchor
* Peeli Sivam as Inspector
* Showkar Janaki as Doctor
* Yuvarani Robert as Azhagappans son
* Suresh Chakravarthy as Chakku
* Sonia as Priyas brother
* Ramya Krishnan in cameo appearance

==Soundtrack== Maragadha Mani . One particular song was filmed throughout the night. The lyrics were written by Pulamaipithan.

# Sangeetha Swarangal by S. P. Balasubramaniam, Sandhya
# Sathi Malli Poocharame by SP. Balasubramaniam
# Mazhaiyum Neeye by SP. Balasubramaniam Chitra
# Thathithom by Chitra
# Nenjamadi Nenjam by SP. Balasubramaniam,  Chitra Seerkazhi Sivachidambaram,  Malaysia Vasudevan,  Chitra
# Avan Thaan Azhagan by Minmini

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 