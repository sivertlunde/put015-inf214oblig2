Last of the Living
 
 
 
{{Infobox film
| name           = Last of the Living
| image          = Lastoftheliving.jpg
| caption        = 
| director       = Logan McMillan
| producer       =
| writer         = Logan McMillan Morgan Williams Robert Faith Mark Hadlow Emily Paddon-Brown Ashleigh Southam
| music          = Ben Edwards, Kurt Preston
| cinematography = Kirk Pflaum
| editing        = 
| distributor    = Quantum Releasing
| released       =  
| runtime        = 88 minutes
| country        = New Zealand
| language       = English
| budget         =
}}
Last of the Living is a 2009 New Zealand comedy horror|comedy-horror film written and directed by Logan McMillan.

==Plot==

A contagious virus has spread throughout the land turning everybody who is bitten into zombies. Morgan, Ash, and Johnny believe they are the only humans left, and spend their time lounging in mansions playing video games. When they stumble upon scientist Stef who claims to have a cure, the three of them decide to help save mankind whilst trying to each win her affections.

==Cast==

* Morgan Williams as Morgan
* Robert Faith as Johnny
* Ashleigh Southam as Ash
* Emily Paddon-Brown as Stef
* Mark Hadlow as Dad

==External links==
*  
*  

 
 
 
 
 
 


 
 