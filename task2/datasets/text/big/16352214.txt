Duplicity (film)
{{Infobox film
| name           = Duplicity
| image          = Duplicity_film.jpg
| caption        = Theatrical release poster
| director       = Tony Gilroy
| producer       = Jennifer Fox Laura Bickford
| writer         = Tony Gilroy
| starring       = Clive Owen Julia Roberts Tom Wilkinson Paul Giamatti
| cinematography = Robert Elswit
| music          = James Newton Howard John Gilroy
| studio         = Relativity Media Universal Pictures
| released       =  
| runtime        = 125 minutes  
| country        = United States
| language       = English
| budget         = $60 million
| gross          = $78,146,652 
}} corporate spies with a romantic history who collaborate to carry out a complicated con.  The film was released on March 20, 2009. 

==Plot==
The film opens five years before the present day, showing the Fourth of July celebration at the American consulate in Dubai, where Ray Koval (Clive Owen), an MI6 agent, appears to seduce Claire Stenwick (Julia Roberts), who, unbeknownst to him, is a CIA agent. Claire drugs Ray and steals classified documents from him.
 tarmac between Howard Tully (Tom Wilkinson), the CEO of Burkett & Randle, and Dick Garsik (Paul Giamatti), the CEO of Equikrom, illustrating the longstanding rivalry between the two executives.

In the present day, Ray is now a corporate spy in New York City who recently went to work for Equikrom. At a meet, he spots Claire and thinks the mission is blown. Ray follows her and confronts her about the incident in Dubai. Claire puts on an innocent act, pretending she has never met Ray, until they both realize they were supposed to meet. Claire has been working undercover for Equikrom at Burkett & Randle for the past 14 months, and Ray is to be her new handler.

At Burkett & Randle a major development is underfoot, and Tully makes a speech that paints them as innovators defending themselves from duplicity and theft. At Equikrom, Garsik obtains a copy of the speech through Claire and plots to steal whatever Burkett & Randle has developed.

The scene cuts to two years earlier in Rome, where we see Ray and Claire again meeting for the "first" time since Dubai, replaying the same dialog as in NYC. The audience learns that Ray and Claire did not meet at Equikrom by chance—they plan to cheat both companies and sell a corporate secret to the highest bidder. However, neither still completely trusts the other, nor knows who is playing whom.
 Thomas McCarthy) from Burkett & Randle foil their plans by planting evidence of them cheating the casino.

In return, Tully at Burkett & Randle thanks Claire for successfully defending the companys new product, revealing it to be a cure for baldness. When Bauer is later caught attempting to steal the formula and Claire is left guarding him and the formula by herself, she uses one of the rigged photocopiers at Burkett & Randle to transfer it to Equikrom.

Back at Equikrom, Claire accuses Ray of stealing the formula for himself. He is searched and exposed when it is found. Claire waits at the Zürich Airport in Switzerland and when Ray arrives, it is revealed that Claire and Ray were only putting on an act at Equikrom. Each pretends at first they dont have the formula and then Claire confesses she loves Ray and says they are each the only one who can ever understand the other. That she knows who he is, and loves him anyway. Claire proposes they each reveal what they have on the count of three but Ray says he loves Claire back by saying first that he had another copy of the formula. Claire admits that she had it too, recorded on her phone, creating real trust between them for the first time.

Ray and Claire attempt to sell the formula to a Swiss company for $35 million. Meanwhile, Garsik tells his shareholders that they are in the final stages of testing for a product that cures baldness. The Swiss, however, say the formula is a fraud. The scene cuts to ten days earlier, where Claire and Ray were practicing the act they would present when it was revealed that Ray would be Claires handler. Unbeknownst to them, there was a hidden microphone in their room monitoring them. From the beginning, Claire and Ray were being played by Tully and his people. Pam Frailes (Kathleen Chalfant) from Equikrom was really working for Burkett & Randle all along, Partiz was used as the bait, Bauer staged stealing the formula, and the formula never existed at all. It was all a trick to get Garsik to announce he had a revolutionary product, which was in fact just a regular skin lotion, and which would ruin Garsik and Equikrom. The film ends with Ray and Claire, still a little suspicious that perhaps theyre still being played by the other, and that each switched the real formula, when they receive a bottle of champagne compliments of Tully, and thats when they finally realise theyve been played by Tully all along. Theyre obviously disappointed, but ultimately also quite impressed theyve been had, and by the strength of Tullys plan. They end sipping champagne, holding hands and saying "Well, at least we have each other", in a rather sarcastic way.

==Cast==
* Clive Owen as Ray Koval
* Julia Roberts as Claire Stenwick
* Tom Wilkinson as Howard Tully
* Paul Giamatti as Richard "Dick" Garsik
* Denis OHare as Duke Monahan
* Kathleen Chalfant as Pam Fraile Thomas McCarthy as Jeff Bauer
* Wayne Duvall as Ned Guston
* Carrie Preston as Barbara Bofferd
* Christopher Denham as Ronny Partiz
* Oleg Shtefanko as Boris Fetyov (as Oleg Stefan)

==Production==
Production on Duplicity began in New York City on March 9, 2008, and wrapped shooting on May 27 of that year. Filming locations included Paradise Island in the Bahamas for the casino shots, New York City including the West Village (Manhattan), Trafalgar Square in London and outside the Pantheon in Rome.

==Release== US and the United Kingdom|UK.  It had its world premiere on March 11, 2009 at Londons Leicester Square. 

==Reception==
The film received mixed to positive reviews from film critics. Based on 178 reviews, it garnered a 64% approval rating on Rotten Tomatoes, the consensus being "Duplicity is well-crafted, smart, and often funny, but its mostly more cerebral than visceral and features far too many plot twists." 

Film critic Roger Ebert gave the film three out of four stars and wrote, "Duplicity is entertaining, but the complexities of its plot keep it from being really involving: When nothing is as it seems, why care?", but admitted that "the fun is in watching Roberts and Owen fencing with dialogue, keeping straight faces, trying to read each others minds".   

In his review for  , wrote, "Comedy seems to have liberated Gilroy, who directs Duplicity with the high gloss and fleet-footed hustle of a golden-age Hollywood craftsman. Theres nary a dull stretch in its two-hour breadth".   
 Thin Man-style undercurrent of sexual sparring to sustain our interest in two scheming corporate operatives despite the fact that nothing much else is going on".   

In his review for The New York Times, A. O. Scott praised Julia Roberts performance: "Ms. Roberts has almost entirely left behind the coltish, Americas-sweetheart mannerisms, except when she uses them strategically, to disarm or confuse. Curvier than she used to be and with a touch of weariness around her eyes and impatience in her voice, she is, at 41, unmistakably in her prime".   

Sukhdev Sandhu, in his review for The Daily Telegraph, wrote, "Duplicity is really all about Roberts and Owen. Theyre con artists, but they dont fool us. Their pairing here feels duplicitous. Gilroy, it seems, is better at thrilling audiences than he is at seducing them".    However, not all reviews were positive; Peter Travers of Rolling Stone gave the film two and a half stars out of four and said "Gilroy and his stars make it elegant fun to be fooled, but they sure as hell make you work for it." 

==Awards and nominations==
{| class="wikitable" class="wikitable" 02px #aaa solid
|-  style="background:#ccc; text-align:center;"
! colspan="4" style="background: LightSteelBlue;" | Awards
|-  style="background:#ccc; text-align:center;"
! Award
! Category
! Recipient(s)
! Outcome
|- style="border-top:2px solid gray;" Golden Globe Awards
| Best Actress, Musical/Comedy
| Julia Roberts
| Nominated
|}

==Home media==
The film was released on DVD and Blu-ray Disc on August 25, 2009.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 