Ewiger Wald
{{Infobox film
| name           = Ewiger Wald
| image          =
| image_size     =
| caption        =
| director       = Hanns Springer Rolf von Sonjevski-Jamrowski
| producer       = Albert Graf von Pestalozza (producer)
| writer         = Albert Graf von Pestalozza (writer) Carl Maria Holzapfel (poems )
| narrator       =
| starring       = See below
| music          = Wolfgang Zeller
| cinematography = Sepp Allgeier Werner Bohne Otto Ewald Wolf Hart Guido Seeber A.O. Weitzenberg Bernhard Wentzel
| editing        = Arnfried Heyne
| distributor    =
| released       = 1936
| runtime        = 75 minutes 88 minutes (Germany)
| country        = Nazi Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Ewiger Wald is a 1936 German film directed by Hanns Springer and Rolf von Sonjevski-Jamrowski.

The film is also known as Enchanted Forest (International English title)

== Plot summary ==
 
Commissioned by Alfred Rosenbergs cultural organization Militant League for German Culture in 1934 under the working title Deutscher Wald–Deutsches Schicksal (German Forest–German Destiny), the feature-length movie premiered in Munich in 1936. Intended as a cinematic proof for the shared destiny of the German woods and the German people beyond the vicissitudes of history, it portrayed a perfect symbiosis of an eternal forest and a likewise eternal people firmly rooted in it between Neolithic and National Socialist times. 
 assumption of power and thus the film culminated in a National Socialist May Day celebration filmed at the Berlin Lustgarten. 

=== Differences from poems ===
 

== Cast ==
*Günther Hadank (voice)
*Heinz Herkommer (voice)
*Paul Klinger (voice)
*Lothar Körner (voice)
*Aribert Mog
*Kurt Wieschala (voice)

== Soundtrack ==
 

== External links ==
* 
* 

== References ==
 

* Meder,Thomas. “Die Deutschen als Wald-Volk. Der Kulturfilm EWIGER WALD (1936).” in: Il bosco nella cultura europea tra realtá e immaginario, ed. Guili Liebman Parrinello, 105-129. Rom: Bulzoni, 2002.
* Wilke, Sabine. “Verrottet, verkommen, von fremder Rasse durchsetzt. The Colonial Trope as Subtext of the Nazi-Kulturfilm EWIGER WALD (1936).” German Studies Review 24 (2001): 353-376.
* Zechner, Johannes. “Wald, Volksgemeinschaft und Geschichte: Die Parallelisierung natürlicher und sozialer Ordnungen im NSKG-Kulturfilm EWIGER WALD (1936).” in: Kulturfilm im „Dritten Reich“, ed. Ramón Reichert, 109-118. Wien: Synema, 2006.
*  .

 
 
 
 
 
 
 
 
 