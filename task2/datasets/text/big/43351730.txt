Veendum
{{Infobox film
| name           = Veendum
| image          =
| caption        =
| director       = Joshiy
| producer       =
| writer         = Jose Kurian Dennis Joseph (dialogues)
| screenplay     = Dennis Joseph Baiju Geethu Mohandas
| music          = Ouseppachan
| cinematography = Anandakkuttan
| editing        = K Sankunni
| studio         = Vijaya Film Circuit
| distributor    = Vijaya Film Circuit
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Baiju and Geethu Mohandas in lead roles. The film had musical score by Ouseppachan.    

==Cast==
 
*Mammootty as Vijaya Chandran
*Ratheesh Baiju as Bike Passenger
*Geethu Mohandas as Anu
*Kollam Ajith Kunchan as Kuttappan
*Lalithasree
*Lalu Alex as Sukumaran
*MG Soman as Alex
*Mala Aravindan
*Noohu
*P. K. Abraham
*Rajasekharan
*Santhakumari as Sukumari
*Jayashree  as Lalitha
*Vettoor Purushan
 

==Soundtrack==
The music was composed by Ouseppachan and lyrics was written by Shibu Chakravarthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Doore maamalayil || K. J. Yesudas || Shibu Chakravarthy || 
|-
| 2 || Thenoorum Malar Pootha Poovaadiyil || K. J. Yesudas, S Janaki || Shibu Chakravarthy || 
|}

==References==
 

==External links==
*  

 
 
 

 