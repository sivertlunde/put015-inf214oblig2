The Felistas Fable
{{Infobox film
| name           = The Felistas Fable
| image          = 
| alt            = 
| caption        = 
| director       = Dilman Dila producer       = {{Plainlist|
*Dilman Dila
*Nathan Magoola
*Reiza S Degito}}
| screenplay     = Dilman Dila
| based on       = 
| starring       =  
| music          =   cinematography  = Cyril Ducottet editing         = Jonathan Ojok 
| production companies=  
| distributor    = 
| released       = 
| country        = Uganda
| language       = English
| runtime        = 90 minutes.  ama-awards.com. Retrieved December 13, 2014. 
| budget         = 
| gross          = 
| website          = http://www.thefelistasfable.com/
}}

The Felistas Fable is a 2013 film written, dricted and produced by Dilman Dila.  blogs.indiewire.com. Retrieved December 13, 2014.  The film stars Veronica Namanda, Mike Wawuyo, Kaddzu Isaac and Mathew Nabwiso.  thefelistasfable.com. Retrieved December 13, 2014. 

==Plot==
Felistas is cursed. She stinks. No one can stand to stay near her. She lives in seclusion in an abandoned house. One day, a witchdoctor finds a solution to her problems. A cry-baby man can inherit the smell from her. Felistas is hesitant to grab the opportunity, because she does not want another person to go through the same pain she has endured. But she longs to reunite with her husband and child. So she kidnaps a man, Dan, who is a virgin desperate to get married. Dan recently got a job that makes him very rich. This attracts the attention of Kate, a gold-digger who he has wooed for a long time, and that of a corrupt cop, Jomba, who frames him for murder in an extortion scheme. As Felistas races against time to deliver Dan to the witch and win back her husband’s love, it turns into a high-energy chase with a voluptuous Kate and a trigger-happy Jomba hot on her tail.  imdb.com. Retrieved December 13, 2014.    thefelistasfable.com. Retrieved December 13, 2014. 

==Cast==
*Veronica Namanda as Felistas
* Kuddzu Isaac   as Dan
* Tibba Murungi     as Kate
*Gerald Rutaro as   Jomba   
*Joanitta Bewulira-Wandera    as Mama Dan
*Raymond Kayemba   as Kiza
*Mike Wawuyo as Kuku
*Esther Bwanika as Miria     
*Gamba Lee   as John      
*Nandaula Zam Zam as Jean      
*Wilberforce Mutetete as Police   
*Oyugi Jackson Otim    as Driver     
*Mathew Nabwiso as Fred          
*Opio Henry Opolot as Ogwang    
*Mate Jackson as Priest    
*Juliet Akanyijuka as Hr        
*Kyarisiima Allen as Junior    
* Nalubowa Aidah as Vendor       
*Wagaba Dauda as Idler     
*Mataze Charles   as Spy       
* Kazibwe Edwin as Son
      
==Critical reception==
The Felistas Fable   indiewire.com. Retrieved 10 June 2014.  won two nominations at the   for Best Make-up Artist.   viasat1.com. Retrieved 13 June 2014.  It won four awards at the Uganda Film Festival 2014,  http://kamukamapolly.wordpress.com/2014/09/01/felistas-reigns-supreme-at-film-awards/  for Best Screenplay, Best Actor, Best Feature Film, and Film of the Year (Best Director).

==Awards==
*Winner of Film of the Year (Best Director) at the Uganda Film Festival 2014. 
*Winner of Best Feature Film at the Uganda Film Festival 2014 
*Winner of Best Screenplay at the Uganda Film Festival 2014 
*Winner best actor (Isaac Kadzu)at the Uganda Film Festival 2014  
*Winner overall film of the year at the Uganda Film Festival 2014.  observer.ug. Retrieved December 13 2014  
*Nominated for Best First Feature by a Director at the Africa Movie Academy Awards 2014 African Magic Views Choice Awards, 2014

== References ==
 

== External links ==
* 
* 
* 
* 
* 

 
 
 