51 (film)
 
{{Infobox film
| name           = 51
| image          = 51 Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Jason Connery
| producer       = Courtney Solomon
| writer         = Kenny Yakkel
| starring       = Bruce Boxleitner Rachel Miner Vanessa Branch Jason London and John Shea
| music          = 
| cinematography = 
| editing        = Andrew Bentler
| studio         = 
| distributor    = After Dark Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $1&nbsp;million 
| gross          = 
}}
51 is an American horror film, which was directed by Jason Connery and starring Bruce Boxleitner and John Shea. {{cite news
| title = SyFy and After Dark Films Unite for Saturday Original Movie Franchise
| url = http://tv.broadwayworld.com/article/Syfy_and_After_Dark_Films_Unite_for_Saturday_Original_Movie_Franchise_20100401
| date = April 1, 2010
| accessdate = 2010-09-16
| work = Broadway World
}}
 

==Plot==
Political and public pressure coerces the government into allowing two well-known reporters and their assistants limited access to the ultra-secretive Area 51. The group consists of 20-year news veteran Sam Whitaker; his camera-woman Mindy; Claire Fallon, an ambitious writer, journalist, and head of an acclaimed news blog called The Fact Zone; and her cameraman Kevin. The four tour the base and things go well for a while, but when one of the bases "occupants" attempts to liberate both himself and those of his fellow species, Area 51 changes from being a secured government facility to a place of horror.

==Cast==
*Bruce Boxleitner as Col. Martin
*Jason London as Aaron Schumacher
*Rachel Miner as Sgt. Hannah
*Vanessa Branch as Claire
*Andrew Sensenig as Dr. Keane
*John Shea as Sam Whitaker
*Jillian Batherson as Gomez
*Beau Brasso as Airmen
*Lena Clark as Mindy
*Damon Lipari as Kevin
*VyVy Nguyen as Elisia

==Production==
The creature feature  was directed by Jason Connery  and written by Kenny Yakkel.  51 began filming in April in Louisiana. 

==Release==
The film is to be released in theaters as part of "After Dark Originals" this fall.  It will also air as an original film on SyFy in 2011. {{cite news
| date = April 25, 2010
| accessdate = 2010-09-16
| work = Airlock Alpha
| author = Melissa Girmonte
| url = http://www.airlockalpha.com/node/7364
| title = Bruce Boxleitner Slated to Star in SyFys 51
}}
 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 