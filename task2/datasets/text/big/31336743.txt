The Music Never Stopped
 
{{Infobox film
| name           = The Music Never Stopped
| image          = The Music Never Stopped.jpg
| alt            = 
| caption        = 
| director       = Jim Kohlberg
| producer       = Neal Moritz
| writer         = Gwyn Lurie Gary Marks
| based on       =  
| starring       = J.K. Simmons Julia Ormond Mía Maestro Lou Taylor Pucci
| music          = Paul Cantelon
| cinematography = Stephen Kazmierski
| editing        = Keith Reamer
| studio         = Essential Pictures Mr. Tamborine Man
| distributor    = Roadside Attractions
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $150,515   
}}
The Music Never Stopped is a 2011 American drama film directed by Jim Kohlberg, who makes his directorial debut from a script by Gwyn Lurie and Gary Marks.
 the 2011 Sundance Film Festival, and was given a limited release in the US on March 18, 2011.

==Plot== The Last Hippie, the film tells the father-son relationship between Henry Sawyer (J.K. Simmons) and his son, Gabriel (Lou Taylor Pucci), who suffers from a brain tumor that prevents him from forming new memories. Henry, with his son unable to shed light on their strained relationship, must connect with him through music. 

==Cast==
* J.K. Simmons as Henry Sawyer
* Julia Ormond as Dianne Daley
* Mía Maestro as Celia
* Lou Taylor Pucci as Gabriel Sawyer
* Tammy Blanchard as Tamara
* Cara Seymour as Helen Sawyer
* Scott Adsit as Doctor Biscow
* Max Antisell as Young Gabriel Sawyer

==Release==
===Critical response===
The film currently holds a 64% "fresh" rating on Rotten Tomatoes, based on 47 reviews.  Ty Burr of the Boston Globe remarked the film was "one to remember", also calling it "sentimental, yet so honest and eccentric that it rises above schmaltz."  Nathan Rabin of the AV Club compared the films story to The Kings Speech, giving praise to J.K. Simmons and Lou Taylor Pucci and calling the film a "powerful, even shattering look at music’s power to unite where it once divided." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 