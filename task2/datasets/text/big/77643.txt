The Talk of the Town (1942 film)
{{Infobox film
| name           = The Talk of the Town
| image          = The Talk of the Town dvd cover.jpg
| image_size     = 225px
| caption        = theatrical poster
| alt            = Threes a Crowd
| director       = George Stevens
| producer       = George Stevens Fred Guiol
| story          = Sidney Harmon
| screenplay     = Dale Van Every Irwin Shaw Sidney Buchman
| starring       = Cary Grant Jean Arthur Ronald Colman
| music          = Friedrich Hollaender
| cinematography = Ted Tetzlaff  Otto Meyer
| studio         = Columbia Pictures
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $1 million Dick, p 160 
}}
 drama film directed by George Stevens, starring Cary Grant, Jean Arthur and Ronald Colman, with a supporting cast featuring Edgar Buchanan and Glenda Farrell. The screenplay was adapted by Dale Van Every, Irwin Shaw and Sidney Buchman from a story by Sidney Harmon. The picture was released by Columbia Pictures. This was the second time that Grant and Arthur were paired in a film, earlier was in Only Angels Have Wings (1939).

==Plot==
Mill worker and political activist Leopold Dilg (Cary Grant) is accused of burning down a mill and causing the death of a foreman in the fire. In the middle of his trial, Dilg escapes from jail and seeks shelter in a house owned by former schoolmate Nora Shelley (Jean Arthur), now a schoolteacher on whom he has had a crush for years. Shelley has the house rented for the summer to distinguished law professor Michael Lightcap (Ronald Colman), who plans to write a book. Both Lightcap and Dilg arrive within minutes of each other.

When Dilg is spotted by Lightcap, Shelley passes him off as her gardener. Lightcap and Dilg enjoy having spirited discussions about the law, Lightcap arguing from an academic viewpoint, while Dilg subscribes to a more practical approach. They become good friends as a result, but meanwhile they become romantic rivals, as Lightcap also falls in love with Nora.

As a result of prodding by Shelley and Dilgs lawyer, Lightcap becomes suspicious and starts, in spite of his initial reluctance, to investigate the case against Dilg further. He romances the girlfriend of the supposed murder victim and discovers that the former foreman is still alive and hiding in Boston. He is persuaded to return to town and admit his guilt and that of the mill owner.

Lightcap also convinces Dilg of the importance of following the law and Dilg gives himself up. In due course, he is set free.
 Supreme Court. He asks Shelley to marry him. Dilg tells Shelley that Lightcaps a fine man, but she decides in favor of Dilg.

==Cast==
  
* Cary Grant as Leopold Dilg - Joseph
* Jean Arthur as Miss Nora Shelley
* Ronald Colman as Professor Michael Lightcap
* Edgar Buchanan as Sam Yates
* Glenda Farrell as Regina Bush
* Charles Dingle as Andrew Holmes
 
* Emma Dunn as Mrs. Shelley Rex Ingram as Tilney
* Leonid Kinskey as Jan Pulaski
* Tom Tyler as Clyde Bracken
* Don Beddoe as Police Chief
 

==Production==
The Talk of the Town began with the working title "Mr. Twilight", but Cary Grant insisted it be changed, suspecting that if the movie appeared to be about a single male character, then Colman, who had the better role, would steal the show.  The title The Talk of the Town was registered to Universal Studios, and Columbia had to give them the rights to use Sin Town in return. The film is now considered a classic. 

Other titles once mentioned as possible for the film included "Threes a Crowd", "The Gentlemen Misbehave", "Justice Winks an Eye", "In Love with You", "Youre Wonderful", "A Local Affair", "The Womans Touch", "Morning for Angels", "Scandal in Lochester", "The Lochester Affair", and even "Nothing Ever Happens". 

Principal photography, originally scheduled to begin January 17, 1942, was delayed when the news of the accidental death of Carole Lombard became known. 

The role of Colmans valet, played by  s to determine the ending.   

==Reception==

===Critical assessments===
According to Bosley Crowther, "the essential purpose of this tale is to amuse with some devious dilemmas, and that it does right well"; he called the script "smart and lively."  According to Variety, the films "transition from serious or melodramatic to the slap-happy and humorous sometimes is a bit awkward, but in the main it is solid escapist comedy." 
 Academy Awards===
;Nominations    Outstanding Motion Picture: Columbia
*  
*  , Sidney Buchman
*  ,  
*  
*  
*  , Morris Stoloff

==References==
Notes
 

Bibliography
* 

==External links==
 
*  
*  
*  
*  
*   on Lux Radio Theater: May 17, 1943

 

 

 
 
 
 
 
 
 
 
 

 