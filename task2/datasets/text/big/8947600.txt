RevoLOUtion: The Transformation of Lou Benedetti
{{Infobox film
| name           = RevoLOUtion: The Transformation of Lou Benedetti 
| image          = 
| caption        = 
| director       = Bret Carr
| producer       = Bret Carr Alice Shashy Mary Helen Shashy Michael Kastenbaum
| writer         = Bret Carr Quinn Redeker Mary Shashy Jon Jacobs Kumar Pallana
| music          = Bill Conti Ashly Irwin Dan Newman    
| cinematography =   
| editing        = Bret Carr
| distributor    = 
| released       = 2006 experimental release in NY/LA "Pay After The Movie"
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Deer Hunter writer Quinn Redeker, with a theme by Rocky composer Bill Conti. 
 Jon Jacobs and Kumar Pallana.

==Plot== boxer who can only speak normally when starting trouble  protecting the Brooklyn neighborhood in which he lives. Lou transforms from violent, extreme stutterer into a great, powerful communicator.

==Production & release==
Carr has shot additional scenes for the film every to two years since 1996, and having been rejected 5 years in a row from major film festivals, he eventually won or was nominated for Jury Prize and/or special distinctions at 14 of the top festivals such as Best Picture at Malibu Film Festival, Best Picture at San Francisco World Film Festival, Jury Prize Nomination at Slamdance Film Festival and others.

Quoting from Awareness Magazine, "the viewer so identifies with Carrs Oscar Caliber performance, that they are taken on a near death psychological journey with "Lou", and come out having had their own awakening." 
 The Secret made RevoLOUtion their first dramatic film to be streamed online.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 