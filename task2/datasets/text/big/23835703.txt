Gnomes and Trolls: The Secret Chamber
{{Infobox film
| name           = Gnomes and Trolls: The Secret Chamber
| image          =
| caption        =
| director       = Robert Rhodin
| producer       = Carl Johan Merner Robert Rhodin
| writer         = Salvatore Cardoni (screenplay) Robert Rhodin (story)
| starring       = 
| music          = Anders Bagge Oscar Merner
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Sweden United States
| language       = English
| budget         = 
| gross          = 
}}

Gnomes and Trolls: The Secret Chamber is an 2008 Swedish animated childrens fantasy film directed by Robert Rhodin for the Stockholm based studio White Shark. The movie was the first CG animated movie produced by a Swedish company.

== Plot ==
Gnomes and Trolls, an action-adventure comedic fairy tale, marks White Shark Films first foray into computer animation. Junior, a teenage gnome, wants nothing more than to invent gizmos and gadgets in his tree-house laboratory. But Juniors old school father, Jalle, the head gnome of the forest, would prefer his son follow in his footsteps and one day be in charge of his own forest. In spite of their differences, on the eve of the first winter storm Junior helps Jalle distribute food rations to the soon-to-be-hibernating animals. Then disaster strikes.

Perpetually bickering troll brothers Face and Slim execute their evil father Fassas carefully plotted plan to steal the food from the gnome secret food chamber. After Jalle is accidentally injured, Junior and his best friend Sneaky, a paranoid neurotic crow, embark on a heroic journey to the depths of troll cave to retrieve the stolen food. Junior and Sneakys adventure takes them across vast landscapes and into encounters with bizarre animals.

Along the way, Junior learns that it is possible to merge his passion for inventing with his newfound impetus to defend the forest. As Sneaky taps into an innate bravery he never knew he had, Slim battles an identity crisis, and Juniors mother Svea shatters the preconceived notions of what it means to be a gnome wife.

== Cast ==
*Greg Berg as Sneaky
*Joe Cappelletti as Face
*Elizabeth Daily as Junior
*Kate Higgins as Alley
*Kym Lane as Fawn
*Lloyd Sherr as Jalle
*André Sogliuzzo as Fassa
*James Arnold Taylor as Slim

== Production == Joel Cohen.  The total budget for the film was $5m (USD).  The production was fast with a production time of 12 months from writing the script to delivering the 35mm print . 

== Film Festivals ==

===2008===
*Halifax, Nova Scotia, Canada (Tue 21 April 2009) 
*Kristiansand, Norway (28 April 2009) 
*5th Dubai Film Festival 2008 

===2009===
*Zin Fest in Czech Republic 
*Buff filmfestival 
*Cairo International film festival 
*11th Seol International Youth Film Festival 2009  
* Jerusalem Cinematheque Childrens film festival 
* Nice film festival in Liverpool Nov/Dec 2009 
*10th China International Childrens Film festival 
*4th International Bursa Silk Road Film Festival 2009 

===2010===
*6th Childrens India Jan 2010 
*Bermuda Film Festival March 2010 

== Reception ==
The movie has mixed receptions over the 85 countries. The movie reached the top ten in box office admissions in three markets (Turkey, Sweden and Middle East)  while it was only released to DVD in most other markets. In 2013, Gnomes & Trolls was introduced to the Chinese market and became an instant hit with 1.6 million views on the popular VOD channel youku scoring 8.2 of 10 points.  
Turkey had 75.000 visitors giving it 8.6 out of 10 points.  The movie has also been released in Kuwait (topping number 6 on top-ten box office), Dubai, Argentina, Brazil, the Czech Republic, Finland, Greece, US, Canada and Hungary. Netflix has released the movie in late 2014 and the verdict from 70.000 viewers is 3.1 out of 5 . 
IMDB users (close to a 1000, Jan 2015) has given the movie 5.9 of 10 score.  

The reception in Swedish press was mixed. There are more information about the Swedish reviews on the Swedish Wikipedia.

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 