Palmetto (film)
{{Infobox film
| name           = Palmetto
| image          = Palmetto film poster.jpg
| border         = yes
| caption        = Theatrical release poster Volker Schlondorff
| producer       = Al Corley Eugene Musso Bart Rosenblatt Matthias Wendlandt
| writer         = James Hadley Chase (writer) E. Max Frye (screenplay)
| narrator       =
| starring       = Woody Harrelson Elisabeth Shue Gina Gershon Rolf Hoppe Michael Rapaport Chloe Sevigny
| music          = Klaus Doldinger
| cinematography =
| editing        = Peter Przygodda
| studio         = Castle Rock Entertainment
| distributor    = Columbia Pictures (theatrical) Warner Bros. (current)
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $5,878,911
}}

Palmetto is a 1998 neo-noir film directed by Volker Schlöndorff (as Volker Schlondorff) with a screenplay by E. Max Frye. It is based on the novel Just Another Sucker by James Hadley Chase. The film stars Woody Harrelson, Elisabeth Shue and Gina Gershon. 

==Plot==
Harry Barber is serving time in prison after being framed in a corruption scandal.

Before his arrest, he was a reporter for a Florida newspaper that uncovered widespread corruption in the local government.  After rejecting a bribe that would have ensured his silence, Harry finds the funds deposited into his bank account and he is promptly arrested.  Now, two years later, he is released when an ex-cops testimony vindicates him.

Though he is bitter against the town officials, Harry wanders back to Palmetto with his girlfriend Nina, who has been patiently waiting for him.  Unable to find a job, he spends his days lounging in a local bar. In walks Rhea Malroux, the very attractive femme, wife of the richest man in town, who offers him a job: help her and her daughter Odette scam the old man out of $500,000 with a bogus kidnapping scheme, in which Harry would receive a ten percent cut.

Tempted by both Rheas seductive charms and the prospect of some quick cash, Harry goes along with the plan.  But when Odette winds up dead, and all indications point to Harry as the murderer, he finds himself in way over his head.

==Cast==
*Woody Harrelson as Harry Barber
*Elisabeth Shue as Mrs. Donnelly/Rhea Malroux
*Gina Gershon as Nina
*Rolf Hoppe as Felix Malroux
*Michael Rapaport as Donnely Chloe Sevigny as Odette Tom Wright as John Renick
*Marc Macaulay as Miles Meadows
*Joe Hickey as Lawyer Ralph Wilcox as Judge
*Peter Paul DeLeo as Bartender
*Richard Booker as Billy Holden

==References==
 

==External links==
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 


 