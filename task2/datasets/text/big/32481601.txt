Journey from Zanskar
Journey From Zanskar is a 2010 documentary film written, produced, and directed by Frederick Marx, for Warrior Films. It tells the emotional story of 17 small children who leave home and family, possibly forever, in order to save their dying Tibetan culture. Parting from one of the most remote and desolate places on Earth – Zanskar, in northwest India – the expedition must travel on foot over 17,000 foot Himalayan passes. The two monks serving as guides walked this same path 30 years ago when they were children. The 17 children with them may not return home for 10–15 years or more. Narrated by Richard Gere, featuring the Dalai Lama, the film tells the story of their incredible journey.

==Plot==
Zanskar is the last remaining original Tibetan Buddhist society with a continuous untainted lineage dating back thousands of years. In nearby Tibet and Ladakh, in Sikkim, Bhutan, and Nepal, traditional Tibetan Buddhist culture is either dead already or dying. The horror of Chinese government design in Tibet is being matched by the destruction of global economics elsewhere. Zanskar, ringed by high Himalayan mountains in northwest India, one of the most remote places on the planet, has been safe until now. But that’s changing.

In 3–5 years a road connecting Padum, the heart of Zanskar, with Leh, the heart of neighboring Ladakh, will be finished. The route which previously took up to two days by car will take only 4–5 hours. As economic growth descends on Zanskar it will bring with it an end to this unbroken Buddhist social tradition. Will the native language, culture, and religious practice be able to survive?

The Dalai Lama has instructed two monks from Zanskar’s Stongde Monastery to do everything in their power to insure that it does. The monks are building a school to educate the children from surrounding villages in their own language, culture, history, and religion. Presently, the government school teaches none of those subjects, and is closed most of the year. The nearby private school also doesn’t teach those subjects and is additionally unaffordable for the area’s poor families. At Stongde, along with indigenous traditions, the children will be educated in the best Western curricula.

The monks are racing against the clock. While they complete the school they are also placing local children in other schools and monasteries in the city of Manali and beyond. This requires walking over a 17,500 foot pass. On foot. On horseback. Led by yaks... Whatever it takes.

==Awards==
* European Spiritual Film Festival, Best Documentary, March 2011 
* Arizona International Film Festival, Special Jury Award for Bridging Cultures, April 2011 

==Festivals==
* World Premiere: Boulder International Film Festival Feb. 2010 
* Big Sky International Film Festival, Missoula, Montana Feb. 2010 
* Doc Edge Film Festival, Auckland and Wellington, New Zealand, March 2010 
* Cleveland International Film Festival, March 2010 
* New Jersey International Film Festival, May 2010 
* Ischia International Film Festival, Sicily, Italy, July 2010 
* Sun Valley Spiritual Film Festival Sept. 2010 
* Woodstock Film Festival, October 2010 
* Mill Valley International Film Festival, October 2010 
* International College Peace Film Festival, Seoul, Korea, October, 2010 
* 3rd I Film Festival, San Francisco, CA, Nov. 2010 
* Artivist Film Festival, LA, CA, Dec. 2010
* Sonoma International Film Festival, April, 2011 
* Florida International Film Festival, April 2011 
* Sedona International Film Festival, March 2011 

==Distribution==
Distributed in France by Jupiter films. Frederick Marx is currently self-distributing Journey From Zanskar in the United States through his non-profit company Warrior Films.

== References ==
 
 

== External links ==
*   at  
*   at  

 

 
 
 
 