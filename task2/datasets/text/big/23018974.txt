Office Lady Love Juice
{{Infobox film
| name = Office Lady Love Juice
| image = Office Lady Love Juice - DVD.jpg
| image_size = 
| caption = Office Lady Love Juice (1999) in DVD release as Working Sister: Cant Stand it After 5
| director = Yūji Tajiri 
| producer = 
| writer = Kōsuke Takeda
| narrator = 
| starring = Azumi Kubota
| music = 
| cinematography = Masahide Iioka
| editing = 
| distributor = Kokuei / Shintōhō
| released = February 26, 1999
| runtime = 58 min.
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1999 Japanese Pink film directed by Yūji Tajiri. It was chosen as Best Film of the year at the Pink Grand Prix ceremony.

==Cast==
* Azumi Kubota
* Mikio Satō
* Yumika Hayashi
* Yūji Sawayama
* Yuka Komatsu

==Synopsis==
After her boyfriend leaves her, a 28-year-old woman has a romance with a 20-year-old man.   

==Critical reception==
Jasper Sharp writes that Office Lady Love Juice is a good entry film for newcomers to the pink film genre. It is, he judges, a "subtly-realised and well-drawn character study" which marked a change in style for Kokuei. 

Longtime fans of the genre also showed their approval by awarding it Best Film at the 1999   for the top spot. The older Zeze had been a vocal critic of the younger directors films which avoided the experimentation and politically charged messages which characterize the work of the shitenno. Calling the shichifukujins films "weak" and "light", Zeze engaged in a heated spat with Tajiri on the stage of the awards ceremony. Jasper Sharp writes that the scuffle was mainly a staged controversy for the benefit of the audience, as no real personal animosity exists between the two groups of directors. The younger group all gained their first filmmaking experiences by working on films of the older directors. 

The mainstream film community also awarded the film at the 9th "Japanese Film Professionals Award" ceremony, where Love Juice was named the 7th best release of the year.  In 2002, three years after its initial release, the film had the distinction of winning the last P-1 Grand-Prix. This festival was designed to highlight the current crop of pink films by showing 16 of the best releases on double-bills over a one-week period. At the end of the showings, the audience voted for the best film. Because of the expense involved in mounting the festival, it was only held three times. The 2002 ceremony was included as part of the Udine Far East Film Festival. 

==Availability==
The film was released on DVD in Japan on October 11, 2002. As with many pink films, it was retitled when released on video. The DVD-release title is  .  It has also seen Japanese releases as  ,  , and  .  In the UK, the film was released with English subtitles as No Love Juice: Rustling In Bed. 

==Bibliography==
*  
*  
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 
 