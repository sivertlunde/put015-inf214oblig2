Persiane chiuse
{{Infobox film
| name           = Persiane chiuse
| image          = 
| image_size     = 
| caption        = 
| director       = Luigi Comencini
| producer       = 
| writer         = Massimo Mida Gianni Puccini Franco Solinas Sergio Sollima
| narrator       = 
| starring       = Massimo Girotti Eleonora Rossi Drago Giulietta Masina
| music          = Carlo Rustichelli
| cinematography =
| editing        = 
| distributor    =
| released       = 1950
| runtime        = 90 min.
| country        = Italy Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Persiane chiuse (Behind Closed Shutters) is a 1950 Italian film directed by Luigi Comencini.

==Plot==
Sandra (Rossi Drago) searches her missing sister. For this, she enters the morally degraded seaside of Genoa.

==Cast==
* Massimo Girotti as Roberto
* Eleonora Rossi Drago as Sandra
* Giulietta Masina as  Pippo
* Liliana Gerace as  Lucia
* Renato Baldini as Primavera
* Ottavio Senoret as Signorino

== External links ==
* 
*  
 

 
 
 
 
 
 

 