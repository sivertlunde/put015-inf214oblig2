The Crawling Hand
{{Infobox Film
| name = The Crawling Hand
| image = Crawlinghand.JPG
| image_size =
| caption = One-sheet for The Crawling Hand
| director = Herbert L. Strock
| producer = Joseph F. Robertson  Robert M. Young
| narrator =
| starring = Peter Breck Kent Taylor Rod Lauren Alan Hale Jr. Allison Hayes Sirry Steffen Arline Judge
| music =
| cinematography = Willard Van der Veer
| editing = Herbert L. Strock
| production assistant = John Orland
| distributor = 1963
| runtime = 89 min
| country = United States English
| budget = $100,000 (estimated) 
| gross =
| preceded_by =
| followed_by =
}}
 The Canned Film Festival. 

==Plot==

The hand of an exploded astronaut takes on a life of its own.   Near a spacecraft crash site, a naive young med student discovers a disembodied hand and takes it home as a grisly souvenir.  He is not aware that the hand is possessed by a strange, murderous alien who gradually begins to take over the hapless med student.  One by one, townsfolk are found mysteriously strangled to death.  In the end, a heroic and hungry cat saves the rest of the town.

== Cast ==
*Peter Breck as Steve Curan
*Kent Taylor as Dr. Max Weitzberg
*Rod Lauren as Paul Lawrence
*Alan Hale Jr. as Sheriff Townsend (as Alan Hale)
*Allison Hayes as Donna
*Sirry Steffen as Marta Farnstrom
*Arline Judge as Mrs. Hotchkiss
*Richard Arlen as Lee Barrenger
*Tris Coffin as Security Chief Meidel (as Tristam Coffin)
*Ross Elliott as Deputy Earl Harrison Stan Jones as Funeral Director (as G. Stanley Jones)
*Jock Putnam as Ambulance Attendant
*Andy Andrews as Ambulance Attendant
*Syd Saylor as Soda Shop Owner
*Ed Wermer as Prof. Farnstrom
*Les Hoyle as Mel Lockhart

==Home video== Rhino Home Video in October 1999 and on DVD in June 2002, including the uncut film as a bonus feature.

==In other media==
Rick Moodys novel The Four Fingers of Death, released in July 2010 by Little, Brown and Company, is a metafictional novelization of a 2025 remake of The Crawling Hand (which means that Moodys fictional novelization is set in a future very different from that of the 1963 film). 

==References==
{{reflist|refs=
   
   
   
   
}}

== External links ==
*  

=== Mystery Science Theater 3000 ===
*  
*  

 
 
 
 
 
 
 