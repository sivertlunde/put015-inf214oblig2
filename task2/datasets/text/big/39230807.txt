Minha Mãe é uma Peça
{{Infobox film
| name           = Minha Mãe é uma Peça
| image          = Minha Mãe é uma Peça Poster.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = André Pellenz
| producer       = Iafa Britz
| writer         = Fil Braz Paulo Gustavo
| starring       =  Paulo Gustavo Ingrid Guimarães Herson Capri  Suely Franco  Monica Martelli  Samantha Schmütz   Alexandra Richter
| music          = Plínio Profeta
| cinematography = Nonato Estrela
| editing        = Marcelo Moraes
| studio         = Migdal Filmes
| distributor    = Downtown Filmes Paris Filmes 
| released       =  
| runtime        = 
| country        = Brazil
| language       = Brazilian Portuguese
| budget         = 
| gross          = $21,909,567 
}}

Minha Mãe é uma Peça - O Filme is a 2013 Brazilian comedy film directed by André Pellenz, starring Paulo Gustavo, and written by him in partnership with Fil Braz. The film is based on the play of the same name created and starred by Paulo Gustavo. The cast of the film version is also composed by Ingrid Guimarães, Herson Capri, Suely Franco, Monica Martelli, Samantha Schmütz and Alexandra Richter.  
  

The film was released in Brazil in June 21, 2013, reaching a mark of two million spectators in its third week in theaters. It was the most watched Brazilian film in 2013 with more than 4,600,145 spectators. 

==Cast==
 
 
* Paulo Gustavo as Dona Hermínia
* Ingrid Guimarães as Soraia
* Herson Capri as Carlos Alberto
* Mariana Xavier as Marcelina
* Mônica Martelli as Mônica
* Alexandra Richter as Iesa
* Rodrigo Pandolfo as Juliano 
* Suely Franco as Aunt Zélia
* Samantha Schmütz as Valdéia
Juan Pablo Segundo
 

==References==
 

 
 
 
 
 
 


 