Highland Fling (film)
{{Infobox film
| name =  Highland Fling 
| image =
| image_size =
| caption =
| director = Manning Haynes John Findlay
| writer =  Alan DEgville   Ralph Stock
| narrator =
| starring = Charlie Naughton   Jimmy Gold   Frederick Bradshaw   Evelyn Foster
| music =  
| cinematography = Stanley Grant
| editing = 
| studio = Fox Film Company
| distributor = Fox Film Company
| released = June 1936
| runtime = 68 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Highland Fling is a 1936 British comedy film directed by Manning Haynes and starring Charlie Naughton, Jimmy Gold and Frederick Bradshaw. It was made as a quota quickie by the British subsidiary of 20th Century Fox at Wembley Studios.  Two incompetent detective search for a missing document at the Highland Games.

==Cast==
* Charlie Naughton as Smith
* Jimmy Gold as Smythe 
* Frederick Bradshaw as Tony  
* Evelyn Foster as Jean 
* Gibson Gowland as Delphos  
* Naomi Plaskitt as Katherine  
* Peter Popp as Clockmender 
* Bill Shine as Lizards

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 