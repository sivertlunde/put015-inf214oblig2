La comunidad (film)
{{Infobox film
| name           = La Comunidad
| image          = La comunidad film.jpg
| caption        = Spanish film poster
| director       = Álex de la Iglesia
| producer       = Andrés Vicente Gómez
| writer         = Jorge Guerricaechevarría Álex de la Iglesia
| starring       = Carmen Maura Emilio Gutiérrez Caba Terele Pávez Jesús Bonilla Kitti Mánver Marta Fernández Muro Sancho Gracia Enrique Villén Eduardo Gómez Manuel Tejada Paca Gabaldón Ion Gabola
| music          = Roque Baños 
| cinematography = Kiko de la Rica 
| distributor    =
| released       =  
| runtime        = 107 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}
La comunidad (UK title: Common Wealth) is a 2000 Spanish black comedy film directed by Álex de la Iglesia.

==Plot==
Carmen Maura plays a real estate agent who discovers a fortune in the apartment of a dead man. Unfortunately, a group of neighbors have been waiting for the man to die so that they can seize the money for themselves.

==Locations==
The address of the building is stated as Carrera de San Jerónimo, 14,   in Madrid but the building actually existing there is not the one shown in the film. Banco de Bilbao building in Madrid.]]

==Awards==
* 3 Goya Awards: Best Actress (Carmen Maura), Best Supporting Actor (Emilio Gutiérrez Caba), Best F/X
* San Sebastian Film Festival: Best Actress (Carmen Maura)

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 