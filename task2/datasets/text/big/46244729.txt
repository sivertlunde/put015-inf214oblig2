While the Patient Slept
{{Infobox film
| name           = While the Patient Slept
| image          = 
| image_size     = 
| caption        = 
| director       = Ray Enright
| producer       = Harry Joe Brown (uncredited)
| writer         = Robert N. Lee Eugene Solow Brown Holmes (add. dialogue)
| story          = 
| based on       =  
| narrator       = 
| starring       = Aline MacMahon Guy Kibbee
| music          = Bernhard Kaun (uncredited)
| cinematography = Arthur Edeson
| editing        = Owen Marks
| studio         = 
| distributor    = First National Pictures
| released       =  
| runtime        = 65-67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
While the Patient Slept is a 1935 comedy murder mystery film starring   and The Patient in Room 18, both released in 1938, though MacMahon was replaced by Ann Sheridan.

==Cast==
* Aline MacMahon as Nurse Sarah Keate
* Guy Kibbee as Detective Lt. Lance OLeary
* Lyle Talbot as Ross Lonergan
* Patricia Ellis as March Federie
* Allen Jenkins as Police Sgt. Jim Jackson
* Robert Barrat as Adolphe Federie
* Hobart Cavanaugh as Eustace Federie
* Dorothy Tree as Mittie Federie
* Henry ONeill as Elihu Dimuck
* Russell Hicks as Dr. Jay
* Helen Flint as Isobel Federie
* Brandon Hurst as Grondal
* Eddie Shubert as Detective Muldoon Walter Walker as Richard Federie

==Reception==
The New York Times reviewer was unimpressed: "Mr. Kibbie and Miss MacMahon finally break the case ... but the solution is not altogether satisfactory. Neither, for that matter, is the picture. Come right down to it, its quite unsatisfactory." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 

 