Vidheyan
{{Infobox film name           = Vidheyan director       = Adoor Gopalakrishnan screenplay     = Adoor Gopalakrishnan based on Zacharia
|starring       = Mammootty Tanvi Azmi M. R. Gopakumar Sabitha Anand Aliyar Azeez producer       = K. Ravindran Nair distributor    = cinematography = Mankada Ravi Varma editing        = M. Mani released       = 1993 runtime        = 112 minutes country        = India language       = Malayalam music          = Vijaya Bhaskar
}}

Vidheyan ( ,  ) is a 1993 Indian Malayalam film directed and written by Adoor Gopalakrishnan. It is a cinematic adaptation of the novella Bhaskara Pattelarum Ente Jeevithavum by Malayalam writer Paul Zacharia|Zacharia. The film explores the master-slave dialectic in a South Karnataka setting. Mammootty plays the lead role in the film, who received several accolades for his work.

==Plot==
Thommy, a Christian migrant labourer from Kerala is an obedient slave of his aggressive, tyrannical landlord Bhaskara Pattelar. Thommy obeys all the orders of his master, whether it is to make his own wife sexually available to his master or in killing Pattelars kindly wife, Saroja. When Pattelar escapes to a jungle, due to his own deeds, Thommy escorts him like a pet. But when Pattelar is killed Thommy exults in freedom.
==Reviews==
 
Apart from the tyranny of a master and the obedience of a slave, the film opens windows for other discussions also. Thommy, basically a weak person both in the physical and psychological sense, if not always but at times is portrayed as the opposite force following his master (for reasons of making a living and in fear of the consequences that might arise from not obeying the master). Patterlar is portrayed as a very rude, cruel, sexually obsessive landlord. These contradictory demeanor of the two have been successfully incorporated into the film. For most of the time Thommy acts as a moral light or conscience of Pattelar; Thommy tries to reason Pattelar when he kicks a person for no obvious reasons who came there to search his brother. Another time he tells that it will not be good to catch the fish of the pond near to the temple who are seen as the citizens of the local deity. Thommy also shows his lack of interest in killing Saroja Akka, wife of Pattelar who treated him (Thommy) like a mother and who saw him as a dutiful husband who loves his wife Omana very deeply. To her, Thommy acted almost like a surrogate husband. It can be said that it is Thommys inner wish that the fish of the pond should not be killed that made all thottas that were thrown to the pond left unsuccessful. And it is also his wish not to see Saroja Akka as dead that Pattelar missed his aim while trying to kill her. We see occasions in which Thommy had to be part of Pattelars evil plans always ended in vain. This might be because Thommy always had a very exotic moral conscience that operated in a very different level, in a way that could actually reverse Pattelars evil plans. When we come to Pattelar, we see him saying "Ill live the way I feel like." Though reasons for why Pattelar acts in this unacceptable manner is not dealt very deeply in the movie, we can deduce that Pattlar always held an inner judge within his consciousness. This can be brought into light on his worries after killing his wife which forced him to think if she might have guessed who the killer was, i.e, Pattelar. This is what forces Pattelar to abandon the idea to go to Police to surrender. It is at this point Pattelar feels for the first time that he did something really wrong and decides to hide instead. In all other times it can be said that Pattelar did not feel guilty of what he was doing. Because in all those moments his consciousness could not see him as a culprit but as someone who was successful in fulfilling all he wanted and someone who made full opportunities of a life which gave him full rein. Though Thommy obeyed his master every time he was given an order, he always knew that his master was wrong and immoral. This is what supported him in finding nothing wrong in helping to kill his master (Though it was a failure). But we cannot overlook Thommys concern for his master. He loved his master for sure. If he did not, he would not be able to cry saying ...is this really thou who has fallen!..". Finally when Thommy leaves his dead master to freedom, he was proving himself that he always stood in the righteous path.

==Cast==
*Mammootty as Bhaskara Patelar
*Tanvi Azmi as Bhaskara Patelars Wife
*M. R. Gopakumar as Thommy
*Sabitha Anand as Thommys Wife Aliyar
* Babu Namboothiri Azeez
*Prof. K V Thampi

==Writing==
The film is a cinematic adaptation of the novel Bhaskara Pattelarum Ente Jeevithavum by Malayalam writer Paul Zacharia. Zacharias novella was inspired by a real-life character named Shekhara Shetty Patela. Zacharia happened to hear the stories of Patelar when he was residing near Mangalore in Karnataka.  After the release of the film, Adoor had a tiff with Zacharia over the film. Zachariah said Adoor tainted his story with Hindutva. 

==Awards== National Film Awards Best Actor - Mammootty Best Feature Film in Malayalam - Adoor Gopalakrishnan

; Kerala State Film Awards   Best Film - K. Raveendran Nair (producer), Adoor Gopalakrishnan (director) Best Director - Adoor Gopalakrishnan Best Actor - Mammootty Best Story - Paul Zacharia Best Screenplay - Adoor Gopalakrishnan

; Other awards
* NETPAC Award at the Rotterdam International Film Festival
* Interfilm Award - Honourable Mention at the International Filmfestival Mannheim-Heidelberg|Mannheim-Heidelberg International Film Festival
* Feature FIPRESCI and Special Jury Prize, Singapore

==References==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 
 