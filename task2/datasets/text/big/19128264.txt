Ankuram
{{Infobox film
| name           = Ankuram
| image          =
| image size     =
| caption        =
| director       = C. Umamaheswara Rao
| producer       = K. V. Suresh Kumar
| writer         = C. Umamaheswara Rao
| narrator       = Revathy  Balaiah
| Sirivennela (lyrics)
| cinematography =
| editing        =
| distributor    =Film India Art Creations
| released       = 1993
| runtime        = 131 min
| country        = India
| language       = Telugu
| budget         =
| preceded by    =
| followed by    =
}} 1993 Telugu Telugu drama film directed by C. Umamaheswara Rao, with Revathi in the lead role. The film is about an inspiring journey of a middle-class woman to return an abandoned child to his father. Ankuram runs, in the background, issues like social stigmas, feudalism, naxalism, bureaucracy and human rights.    The film was premiered at the 1993 International Film Festival of India in the mainstream section. 

The plot unfolds like a Chinese puzzle; its a journey where we discover gradually the reason for the lead being harassed by the police, the blocks she faces uncovering a fathers identity, and the darker side of an authoritarian police force and its brutality towards tribals.  The film has received the National Film Award for Best Feature Film in Telugu for that year. 

==Plot==
The movie starts with the marriage of a young couple. The bride Sindhura (Revathy) finds a child on a train. She wants to support the child, against the wishes of her husbands family, until she locates the childs parents.

She starts enquiring about the passenger who left his daughter on the train. Satyam (Om Puri), father of the child, has been on the chase by feudals and police. The police are unable to find him and arrest his pregnant wife. Tribals protested and planned to attack the police. They were stopped by Dr. Mitra (Charuhasan), a pro-tribal doctor. A sadistic officer forced the mother to do situps, resulting in loss of her life. Angered crowd killed the police officer, which caused more violence between officials and tribals.

During the course of the search, Sindhura is implicated in a false case and loses her married life. She faces the threats by rowdies to her own parents and sisters. She persists with the help of Rao (Sarat Babu) (a civil liberty activist and lawyer), goes to the village, brings the atrocities towards the oppressed people to light, and returns the child.

Ankuram ends with the message that citizens who can speak have the responsibility of speaking about the rights of the fellow citizens who can not speak.

==Awards== National Film Awards
*National Film Award for Best Feature Film in Telugu (1992) - (director) - C. Umamaheswara Rao (1992)

;Nandi Awards
*Nandi Award for Best Director - C. Umamaheswara Rao.

;Filmfare Awards South
*Filmfare award for best Telugu actress.

==Cast==
* Revathy - Sindhura
* Sarath Babu - Lawyer Rao
* Om Puri - Satyam Balaiah  - Sindhuras father
* Kota Sankararao - Police officer
* Charuhasan - Mitra
* Hari Prasad - Sindhuras husband
* P. L. Narayana

==Crew==
* Story, screenplay and direction: Umamaheswara Rao. C
* Production: K. V. Suresh Kumar from Film India Art Creations
* Music: Hamsalekha
* Camera: Madhu Ambat Sirivennela
* Chitra

==Soundtrack== Sirivennela and voice by playback singers S. P. Balasubramanyam and K. S. Chithra|Chitra. The theme of the song is that the first person walking towards a new goal is always alone at first and then others follow.
The film has one comic song "Athaarintiki Railekkindhi rubber bomma" which was shot in the train where Revathy was traveling.

==References==
 

 
 
 
 
 
 

 