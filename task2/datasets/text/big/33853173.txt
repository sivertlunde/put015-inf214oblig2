Upendra (film)
{{Infobox film
| name           = Upendra (film)
| image          = Upendra film poster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Upendra
| producer       = Shilpa Srinivas
| writer         = Upendra
| screenplay     = Upendra
| story          = Upendra Prema   Damini
| music          = Gurukiran
| cinematography = A.V. Krishna Kumar
| editing        = T. Shashi kumar
| studio         = 
| distributor    = 
| released       =   
| runtime        = 138 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Kannada film Prema and Damini in the lead roles.  The film explores three human emotions through the relationship between the main character and the three heroines. The songs of the film were written by Upendra and music was composed by Gurukiran.
 Filmfare Award for Best Film (Kannada) and Filmfare Award for Best Director (Kannada).  A Japanese who saw the film in Bangalore was impressed by it as he found it similar to the story of Buddha and got it released in Japan. It was screened at the Yubari International Fantastic Film Festival in Japan in 2001. 

==Cast==

* Upendra
* Raveena Tandon Prema
* Damini

==Reception==
===Critical reception===
The film was heavily panned by the critics at the time of its release. Upendra said in an interview, "the theme is philosophical, but when I said it in a different way, some people were unable to digest it." 

===Box Office===
Upendra was a commercial success and was well received by the audience in Karnataka as well as in Andhra Pradesh.  The film had a 200 days run in Karnataka and its Telugu version had a 100 days run in Andhra Pradesh. 

==Awards==

* Filmfare Award for Best Director (Kannada) (1999)    Filmfare Award for Best Film (Kannada) (1999) 

==Soundtrack==
The music was composed by Gurukiran and lyrics penned by Upendra.

{| class="wikitable sortable"
|-
! Title !! Singers !! Duration
|-
| "Uppigintha Ruchi Bere Illa" || Upendra ||  3:53
|-
| "MTV Subbalakshmige" || Udit Narayan  || 4:30
|-
| "2000 AD Lady" || Gurukiran ||  5:36
|-
| "Enilla Enilla" || Pratima Rao ||  4:32
|- Mano  || 4:19
|}

==Sequel==
In January 2012, Upendra announced that he is working on the script for a sequel to the film, titled Upendra 2 (stylized as Uppi2). The film will be directed and produced by Upendra under his home banner, Upendra Productions. The first posters of Upendra 2 were released on the internet on 16 September 2012.  The posters featured a huge rectangle, with no clear name inside it, but with a heap of mathematical formulae – be it arithmetic, algebra, geometry, analytical geometry, calculus and so on – and had a byline – "directed by Upendra". 

Uppi 2 was launched by Upendra on 18 September 2013 at Kanteerava Studios in Bangalore, amongst thousands of fans gathered to witness the launch. Lathi Charge from the police to control the crowd of fans gathered at the venue was reported.  The Songs Recording for Uppi2 started on 30 March 2014 at Gurukiran Studio in Bangalore.    US rap artist Arod (also known as ARodomus) will render a rap song for the film. The song, sung in a mix of Spanish, English, and a line of Kannada, was recorded in  , New York.  The movie stars Kristina Akheeva, Parul Yadav and Upendra in the lead roles. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 