I Love You Phillip Morris
{{Infobox film
| name           = I Love You Phillip Morris
| image          = I Love You Phillip Morris.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Glenn Ficarra John Requa
| producer       = Andrew Lazar Far Shariat
| screenplay     = Glenn Ficarra John Requa
| based on       =  
| starring       = Jim Carrey Ewan McGregor Rodrigo Santoro Antoni Corone Leslie Mann
| music          = Nick Urata
| cinematography = Xavier Pérez Grobet
| editing        = Thomas J. Nordberg
| studio         = EuropaCorp LD Entertainment
| distributor    = Roadside Attractions
| released       =  
| runtime        = 93 min.
| country        = United States France
| language       = English
| budget         = $13 million
| gross          = $20,712,236   
}}
I Love You Phillip Morris is a 2009   of Glenn Ficarra and John Requa. It grossed a little over $20 million worldwide after its limited theatrical release. 

==Plot==
Steven Jay Russell (Jim Carrey) is on his deathbed, recalling the events of his life that led him to this point. He spent his early adult years in Virginia Beach as a police officer. He plays the organ at church, has unenthusiastic sex with his wife, Debbie (Leslie Mann), and spends his off hours searching for his biological mother, who gave him up as a child. Steven locates his biological mother, but she rejects him.

After a car crash, Steven leaves his family and previous life behind (though he keeps in touch with his wife and young daughter), and explores the world as his true self – a gay man. He moves to Miami, finds a boyfriend (Rodrigo Santoro), and lives a luxurious lifestyle. To keep himself and his boyfriend in the style to which they have become accustomed, Steven becomes a con man. Steven is pursued by the police, and after narrowly surviving falling off a parking garage, has a reunion with his lover, who at the time is dying of AIDS, and is sent to prison, where he falls in love with inmate Phillip Morris (Ewan McGregor).

Steven cannot bear to be separated from Phillip. After being released, he helps get Phillip freed from prison by posing as a lawyer, then attains wealth by fraudulently acquiring a position as chief financial officer of a large medical management company called USAMM.
 embezzling from the company, and goes back to prison. Phillip is also sent to prison as an accomplice, and angrily tells Steven he never wants to see him again. Months later, Phillip learns from another inmate that Steven is dying of AIDS. Heartbroken, Phillip calls Steven while he is in the infirmary and confesses that while he is still upset with Steven for lying to him, he still loves him. Phillip is later told that Steven has died.

Sometime later, Phillip is taken to meet with his lawyer and finds Steven waiting for him. Steven describes how he faked having AIDS and dying (having based parts of it off his boyfriend who died of AIDS while he was in prison) in order to see Phillip again, and promises to never lie to him again. He runs one last con to break Phillip out of prison, only to be caught when he runs into an old coworker.

The end of the movie explains that the real-life Phillip Morris was released from prison in 2006, but Steven was given a life sentence and is in 23-hour lockup, only having one free hour a day to shower and exercise, which the film implies to be because an official involved in the sentencing had a nephew who was conned by Morris, in-universe and in the story it was based on.

The last scene shows Steven laughing joyfully while running across the prison yard, guards in pursuit, in another attempt to be with Phillip.

==Cast==
* Jim Carrey as Steven Jay Russell
* Ewan McGregor as Phillip Morris
* Leslie Mann as Debbie
* Rodrigo Santoro as Jimmy
* Antoni Corone as Lindholm
* Brennan Brown as Larry Birkheim Michael Showers as Gary
* Marc Macaulay as Houston Cop
* Annie Golden as Eudora Mixon Michael Mandel as Cleavon
* Phillip Morris (in an uncredited cameo) as a lawyer during Stevens sentencing

==Production==
After original difficulty finding a U.S. distributor, likely due to its explicit gay sexual content, the film was re-edited.   In May 2009, it was announced by Variety (magazine)|Variety that Consolidated Pictures Group has acquired the rights for distribution. 
 Mother Jones. July/August 2011 Issue. p.  . Retrieved on March 23, 2013.  The real Phillip Morris even appears in a cameo as Steven’s lawyer in one scene.    

==Release==
The film was released in Europe, Taiwan, and Japan between February and April 2010. Although a limited run in the United States was initially scheduled for April 30, 2010, it was later reported that the films release had been indefinitely postponed by its distributors, Consolidated Pictures Group  but on April 12, 2010, Variety (magazine)|Variety announced the distributor had had a change of heart and that I Love You Phillip Morris would be shown in limited theaters starting July 30 before expanding nationwide on August 6. 

On June 3, 2010, the film was delayed yet again due to legal battles. The film was finally released on December 3, 2010, after Roadside Attractions and Liddell Entertainment acquired the rights to distribute in the United States. 

===Box office===
I Love You Phillip Morris has grossed $20,722,843 as of August 31, 2011. 

===Critical reception===
I Love You Phillip Morris received positive reviews. Review aggregate Rotten Tomatoes reports that 71% of critics have given the film a positive review based on 141 reviews, with an average score of 6.7/10. The critical consensus is: "This fact-based romantic comedy has its flaws, but theyre mostly overcome by its consistently sweet, funny tone and one of the best performances of Jim Carreys career." 
 Variety wrote “Less of a comedy than a hilarious tragedy, I Love You Phillip Morris stars Jim Carrey in his most complicated comedic role since “The Cable Guy.”   Empire wrote  “One of the funniest films of the year, this is a wonderful mix of old-school Carrey outrageousness with a genuinely touching – and very modern – love story.”  

Damon Wise of The Times gave the film four stars out of five, stating, "I Love You Phillip Morris is an extraordinary film that serves as a reminder of just how good Carrey can be when hes not tied into a generic Hollywood crowd-pleaser. His comic timing remains as exquisite as ever." {{cite news
| url=http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/film/film_reviews/article5548068.ece
| title=I Love You Phillip Morris at the Sundance Film Festival, Utah
| publisher=The Times
| author=Wise, Damon
| date=2009-01-20
| accessdate=2010-03-19
| location=London
}}  Xan Brooks of The Guardian also gave the film a positive review, describing the movie as "fast, funny and rather daring. A whisk of caffeine with a center thats sweet." {{cite news
| url=http://www.guardian.co.uk/film/video/2010/mar/19/i-love-you-phillip-morris
| title=I Love You Phillip Morris: Fast, funny and rather daring
| publisher=The Guardian
| author=Brooks, Xan
| date=2010-03-19
| accessdate=2010-03-19
| location=London
}} 

==Home media==
The film was released on DVD and Blu-ray Disc on April 5, 2011. 

==Soundtrack==
{{Infobox album  
| Name        = I Love You Phillip Morris: Original Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = 
| Genre       = Soundtrack
| Length      = 40:25
| Label       =
| Producer    =
| Reviews     =
}}
I Love You Phillip Morris: Original Soundtrack was composed by Nick Urata.

{{Track listing
| extra_column    = Artist
| title1          = I Cried Like a Silly Boy
| extra1          = DeVotchKa
| length1         = 3:27
| title2          = Dance Hall Days
| extra2          = Jack Hues
| length2         = 4:00
| title3          = Key West
| extra3          = Nick Urata
| length3         = 0:53
| title4          = Jesus Has a Plan
| extra4          = Nick Urata
| length4         = 2:14 To Love Somebody
| extra5          = Nina Simone
| length5         = 2:41
| title6          = Written in the Stars
| extra6          = Nick Urata
| length6         = 3:59
| title7          = Nobody Knows the Trouble Ive Seen
| extra7          = Orlandus Wilson
| note7           = Golden Gate Quartet
| length7         = 3:16
| title8          = Promise to Jimmy
| extra8          = Nick Urata
| length8         = 2:31
| title9          = The Escape Artist
| extra9          = Nick Urata
| length9         = 4:36
| title10         = The Last Time
| extra10         = Nick Urata
| length10        = 3:00
| title11         = Steal Away
| extra11         = Robbie Dupree
| length11        = 3:31
| title12         = Faking Death
| extra12         = Nick Urata
| length12        = 2:44
| title13         = The Marriage of Figaro: Sullaria
| extra13         = German Opera Orchestra of Berlin
| length13        = 3:33
}}

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   for  
*   at  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 