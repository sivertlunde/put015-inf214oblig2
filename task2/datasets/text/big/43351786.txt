Omphalos (film)
{{Infobox film
| name           = Omphalos
| image          = 
| alt            = 
| caption        = 
| director       = Gabriel Judet-Weinshel Red Giant Media, Edoardo Ballerini, Nitsa Benchetrit, Christian H. Cooper, Gill Holland, Gabriel Judet-Weinshel, George Nicholas, Isen Robbins, Aimee Schoof, Kenzan Tsutakawa-Chinn, Ty Walker
| writer         = Gabriel Judet-Weinshel
| starring       = Al Sapienza, Ashley Nicole Anderson, Austin Pendleton, Edoardo Ballerini, Emmanuelle Chriqui, Giuliana Carullo, Greg Bennick
Lynn Cohen, Sarah Sokolovic
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Red Giant Media It is written and directed by Gabriel Judet-Weinshel. 

==Plot==
A detective investigates a murder, only to find that the victim is... himself. Soon, he discovers multiple versions of himself, not all of them friendly.

Darius Lefaux is a gumshoe detective. His career is in shambles, his romantic life comically void, his only real human connection a cantankerous old woman who lives next door.

One day a murder comes in, and Darius is summoned to view the body. But there’s something very strange about this one. The body is ... him. The body is identical to Darius.

Memories begin to haunt Darius, fragments of events that dont make sense. As the plot thickens, more duplicates of Darius emerge. One version is trying to kill him. In a race against the clock, Darius sets out to find this other self before it finds him.

Meanwhile, John Luka—an old ally of Darius and an eccentric, out-of-work juggler—learns that his friend may be in trouble. We learn that Luka was involved in a time travel experiment run amok, left scarred and destitute by the ordeal. Re-invigorated by the chance to help his friend, he sets out in search of Fyodor Wax, the father of the experiment, hoping Fyodor will lead him to his "brother Darius" before it’s too late.

As Darius chips away at the case of his multiple selves, he is reunited with Alise, a beautiful woman from his past. A long-buried, yet troubled, romance is reignited. More memories surface—from a life they once had together, a life interrupted by an accident on a desert road ten years ago. When Luka finds Darius, the two men learn more about their shrouded past and the scientific experiment that links their existence, and its suddenly clear what has to be done.

Darius and Luka journey to a secret site called “Omphalos,” where they hope to put a stop to the disastrous experiment that may be at the root of their troubles. But the road to "Omphalos" isnt quite as it seems. The journey outward becomes a journey inward. 

==Cast==
* Al Sapienza
* Ashley Nicole Anderson
* Austin Pendleton
* Edoardo Ballerini
* Emmanuelle Chriqui
* Giuliana Carullo
* Greg Bennick
* Lynn Cohen
* Sarah Sokolovic

==References==
 

 