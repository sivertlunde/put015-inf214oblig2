Mother (1937 film)
{{Infobox film
| name = Mother 
| image = 
| image_size =
| caption =
| director = Johann von Vásáry
| producer = János Smolka 
| writer =  Rezsö Török  (play)   Sári Fedák    Johann von Vásáry
| narrator =
| starring = Sári Fedák   Jenő Pataky   Lia Szepes   Jenő Törzs
| music = Dénes Buday   Jenő Sándor
| cinematography = István Eiben
| editing =  Ferenc Vincze 
| studio = Budapest Film 
| distributor = 
| released =19 November 1937 
| runtime = 84 minutes
| country = Hungary Hungarian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Mother (Hungarian:Mámi) is a 1937 Hungarian comedy film directed by  Johann von Vásáry and starring Sári Fedák, Jenő Pataky and Lia Szepes. The film was based on a play by Rezsö Török, with art direction by Márton Vincze. The arrival from Texas of an eccentric relative and her son, disrupt the rhythm of a wealthy Hungarian fmily. 

==Cast==
*    Sári Fedák as Mámi,Özvegy Kovács Gáspárné  
* Jenö Pataky as Kovács Gazsi  
* Lia Szepes as Horváth Ilonka  
* Jenő Törzs as Torday Henrik,bankvezér  
* István Bársony as Inas  
* Piroska Vaszary as Torday nõrokona 
* Andor Heltai as Cigányprímás  
* Béla Mihályffi as Mr.Benedek  
* Sándor Peti as Kárpáthi úr 
* Annie Réthy as Benedek felesége 
* Mary Vágó as Benedek lánya  

== External links ==
* 

 
 
 
 
 
 
 

 