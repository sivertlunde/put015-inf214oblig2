Blue Planet (film)
{{Infobox film
| name           = Blue Planet
| caption        =
| image	=	Blue Planet FilmPoster.jpeg
| director       = Ben Burtt
| producer       =
| writer         = Toni Myers
| narrator       = Toni Myers
| starring       =
| music          = Micky Erbe Maribeth Solomon
| cinematography = David Douglas James Neihouse
| editing        =
| distributor    =
| released       = 1990
| runtime        = 40 min. US
| English
| budget         =
| preceded by    =
| followed by    =
}}
Blue Planet is an IMAX film directed by Ben Burtt, and produced by the IMAX Space Technology corporation for the Smithsonian Institutions National Air and Space Museum, as well as Lockheed Corporation. Filmed with the cooperation of the National Aeronautics and Space Administration (NASA), it was written, edited, and narrated by Toni Myers.      
 orbit during space shuttle missions, the film is about the planet Earth. The changes and constants are highlighted, and the film attempts to show how fragile and unique Earth is. Origins of the planet, how it has changed, what mans role in change is, and other issues are discussed. The film features footage that was filmed from space, underwater, computer animations based on satellite data, and a variety of views from the surface to illustrate the topics. 

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 


 
 