The Pirate (1973 film)
 
 
{{Infobox film
| name           = The Pirate 
| image          = ThePirate.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 大海盜
| simplified     = 大海盗
| pinyin         = Dà Hǎi Dào
| jyutping       = Daai6 Hoi2 Dou6 }}
| director       = Chang Cheh Pao Huseh Li Wu Ma
| producer       = Runme Shaw
| writer         = 
| screenplay     = Ni Kuang
| story          = 
| based on       = 
| starring       = Ti Lung David Chiang Tin Ching Dean Shek Yuen Man Tzu Fan Mei Sheng Yue Feng
| narrator       = 
| music          = Frankie Chan
| cinematography = Kuang Han Lu Yuen Teng Bong
| editing        = Kwok Ting Hung 
| studio         = Shaw Brothers Studio
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 96 minutes
| country        = Hong Kong Mandarin
| budget         = 
| gross          = 
}}
 Hong Kong action martial arts film directed by Chang Cheh, Pao Huseh Li and Wu Ma. It is based on the life of 19th century pirate Cheung Po Tsai, who is portrayed in the film by Ti Lung.   

==Plot==
Pirate Cheung Po Tsai sales to a shoreline southern China disguised as a rich trader. There, the villagers live in poverty due to struggle against exploitation and corruption by the government. There he attracts the attention of merchant Xiang You Lin (Tin Ching) and his sister who are keen on killing him for bounty. Cheung then agrees to expedite funds to them in the form of cash and goods stolen from foreign invading colonists.

While still on land, Cheung discovers that one of his former crew member Hua Er Dao (Fan Mei Sheng), who is an escaped convict, has captured his ship, goods and his crew. As Hua determines to seek vengeance on him, Cheung flees and seeks refuge in a casino.
 Qing Imperial imperial court sends young general Wu Yee (David Chiang) to investigate where pirates are hiding and ultimately finds Cheung. As the two encounter each other, each of them display their skills in duel while also appreciating each other. Wu eventually grows respect for Cheung for his righteous nature and begins to question his own conscience.

==Cast==
*Ti Lung as Cheung Po Tsai
*David Chiang as General Wu Yee
*Tin Ching as Xiang You Lin
*Dean Shek as Master Bai
*Yuen Man Tzu as Hai Tang
*Fan Mei Shang as Hua Er Dao
*Yue Fung as 2nd Miss
*Lau Gong as Zeng Guo Xiong
*Bruce Tong as Ma Ping
*Wu Chi Chin as corrupt policeman
*Yeung Chak Lam as corrupt policeman
*Lo Dik as Leader at the ship docks
*Wang Kuang Yu as secret agent
*Cheng Keng Yeh as secret agent
*Ko Hung as Xiao Bao
*Wong Ching Ho as Deseperate fisherman
*Liu Wai as Brothel Boss
*Shum Lo as Casino clerk
*Chui Fat as Pirate
*Lee Yung Kit as Pirate who gambles in casino
*Leung Seung Wan as Master Shing
*Tung Choi Bo as Casino Guard
*Tino Wong as thug
*Lau Chun Fai as thug
*Yuen Shun Yi as Pirate
*Lee Chiu as Pirate

==Critical reception==
James Mudge of Beyond Hollywood gave the film a positive review and writes "Well made and featuring charismatic turns from two of the studio’s biggest stars, it should appeal not only to Shaw Brothers aficionados, but to all fans of the pirate film."  Matthew Le-feuvre pf City on Fire rated the film 8 stars out of 10 and writes "the defining novelty of both Ti Lung and David Chiang spearheading their eighteenth collaboration for an indelible saga of obligation, revenge and misguided loyalties, is itself a landmark achievement even by Hollywood conventions." 

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 