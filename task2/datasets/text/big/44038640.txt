Mucheettukalikkaarante Makal
{{Infobox film
| name = Mucheettukalikkaarante Makal
| image =
| caption =
| director = Thoppil Bhasi
| producer = SK Nair
| writer = Vaikkom Muhammad Basheer Thoppil Bhasi (dialogues)
| screenplay = Thoppil Bhasi
| starring = KPAC Lalitha Adoor Bhasi Manavalan Joseph Alummoodan
| music = G. Devarajan
| cinematography = U Rajagopal
| editing = G Venkittaraman
| studio = Chithrasala
| distributor = Chithrasala
| released =   
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed by Thoppil Bhasi and produced by SK Nair. The film stars KPAC Lalitha, Adoor Bhasi, Manavalan Joseph and Alummoodan in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  
*KPAC Lalitha 
*Adoor Bhasi 
*Manavalan Joseph 
*Alummoodan 
*Bahadoor 
*Bobby Kottarakkara 
*Chandraji
*Krishnamma Kunchan 
*Kuthiravattam Pappu 
*Paravoor Bharathan 
*Rani Chandra 
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kudukudupaandippennu || K. J. Yesudas || Vayalar || 
|- 
| 2 || Mucheettukalikkana Mizhi || P. Madhuri || Vayalar || 
|- 
| 3 || Muthumethiyadiyitta || P. Madhuri || Vayalar || 
|- 
| 4 || Sangathiyarinjo || Ayiroor Sadasivan, Manoharan || Vayalar || 
|}

==References==
 

==External links==
*  

 
 
 


 