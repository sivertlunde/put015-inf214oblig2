The Ambassador (2011 film)
{{Infobox film
| name           = The Ambassador
| image          = The Ambassador (2011) poster.jpg
| caption        = Theatrical release poster
| director       = Mads Brügger
| starring       = Mads Brügger 
| producer       = Peter Aalbæk Jensen Peter Garde 
| cinematography = Johan Stahl Winthereik
| distributor    = Drafthouse Films
| studio         = Zentropa
| released       =  
| runtime        = 93 minutes
| country        = Denmark
| language       = Danish English French
| gross          = $28,102 (US)
}}
The Ambassador is a 2011 Danish documentary film created and directed by Danish filmmaker and journalist Mads Brügger. The film was nominated for the Grand Jury Prize at the 2012 Sundance Film Festival. 

==Premise==
Mads Brügger goes undercover as the Liberian diplomat and businessman Mr. Cortzen with the object of building a match factory in the Central African Republic; in reality he is uncovering corruption linked to diplomatic title brokerage and blood diamonds.

==Controversy== Dutch businessman International Documentary Film Festival in Amsterdam.  According to Danish media, Willem Tijssen is pursuing legal actions in Denmark, for having been filmed with a hidden camera. 

==Reception==
The film was well received by critics and currently holds a 76% fresh rating on Rotten Tomatoes with Kyle Smith of the New York Post calling it, "a stunning, funny, and vital piece of guerilla cinema." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 