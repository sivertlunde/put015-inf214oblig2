Caustic Zombies
 
{{Infobox film name           = Caustic Zombies  image          = Caustic Zombies.jpg caption        =  alt            = director       = Johnny Daggers    producer       =  writer         = Johnny Daggers  starring       =  cinematography = James Bowley  music          = Nim Vind Serpenteens DieMonsterDie Johnny B. Morbid The Dead Vampires 13PaganHoliday13 Horrid Ordeal Rozz Williams Kult Ikon Veniculture Toxic Zombies Sick City Daggers  editing        = James Bowley Johnny Daggers John Stefanik  released       =    runtime        =  country        = United States language       = English budget         =  gross          = 
}} Latrobe and Ligonier, Pennsylvania.  The film premiered on July 22, 2011 at the Hollywood Theater in Dormont, Pennsylvania. 

==Plot==
Residents of a small town must survive a zombie attack stemming from the Three Mile Island accident. 

==Cast and crew==
The cast and crew listed at the films official website are: 
* Aleesha Asper - survivor
* Greg Wainwright - survivor
* Jake Hursh - survivor
* Melanie Stone - caged zombie
* Chad Hammitt - zombie hunter
* Matt Eames -  hacker

* Writer/Director - Johnny Daggers
* Cinematographer - James Bowley
* Editor - James Bowley, Johnny Daggers, John Stefanik
* Title Credits Design - Brian Cottington
* Key grip - Barry Stephens

==References==
 
*  

==External links==
*  
*  

 
 
 
 
 

 