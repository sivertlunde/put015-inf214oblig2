Under the Bridges
{{Infobox film
| name = Under the Bridges
| image =
| image_size =
| caption =
| director = Helmut Käutner
| producer = Walter Ulbrich
| writer =  Leo de Laforgue   Helmut Käutner   Walter Ulbrich
| narrator =
| starring = Hannelore Schroth   Carl Raddatz   Gustav Knuth   Margarete Haagen
| music = Bernhard Eichhorn
| cinematography = Igor Oberberg
| editing =  Wolfgang Wehrum UFA   Terra Film
| distributor = Gloria Film
| released = 1 September 1946
| runtime = 99 minutes
| country = Germany German
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Under the Bridges (German:Unter den Brücken) is a 1946 German drama film directed by Helmut Käutner and starring Hannelore Schroth, Carl Raddatz and Gustav Knuth. The film was shot in Berlin during the summer of 1944, but was not released until after the defeat of Nazi Germany. It premiered in Locarno in September 1946, and wasnt released in Germany until 1950. 

The film uses poetic realism to portray the everyday lives and romances of two Havel boatmen. 

== Cast ==
* Hannelore Schroth as Anna Altmann
* Carl Raddatz as Hendrik Feldkamp
* Gustav Knuth as Willy
* Margarete Haagen as Landlady
* Ursula Grabley as Vera, a waitress
* Hildegard Knef as Girl in Havelberg Walter Gross as Man on the bridge
* Helene Westphal
* Hildegard König
* Erich Dunskus as Holl, ship captain Klaus Pohl as Museums employee
* Helmuth Helsig as Muhlke - Café owner

== References ==
 

== Bibliography ==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918–1945. University of California Press, 1999.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 