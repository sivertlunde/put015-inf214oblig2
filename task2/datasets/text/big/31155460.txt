Kuppivala
{{Infobox film
| name           = Kuppivala
| image          =
| caption        =
| director       = SS Rajan
| producer       = N Krishnan
| writer         = Moithu Padiyathu
| screenplay     = Moithu Padiyathu Ambika Bahadoor
| music          = MS Baburaj
| cinematography =
| editing        = G Venkittaraman
| studio         = Lotus Pictures
| distributor    = Lotus Pictures
| released       =  
| country        = India Malayalam
}}
 1965 Cinema Indian Malayalam Malayalam film, Ambika and Bahadoor in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Prem Nazir as Majeed
*Sukumari as Pachumma Ambika as Khadeeja
*Bahadoor as Porker/Chellathodu
*Kottayam Chellappan as Beeran Sahib
*Nilambur Ayisha as Pathiri Amina
*Johnson as Muhammedali
*Muthukulam Raghavan Pilla as Kittumman
*Haji Abdul Rahman as Maulavi
*S Malathi as Vijayamma
*Kutty Padmini as Tharabi

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ithu Bappa Njan Umma || Renuka || P. Bhaskaran ||
|-
| 2 || Kaanaan Pattaatha || A. M. Rajah || P. Bhaskaran ||
|-
| 3 || Kaattupaaya Thakarnnallo || K. J. Yesudas || P. Bhaskaran ||
|-
| 4 || Kanmani Neeyen Karam || P Susheela, A. M. Rajah || P. Bhaskaran ||
|-
| 5 || Kuru Kuru Mecham Pennundo || LR Eeswari, Chorus || P. Bhaskaran ||
|-
| 6 || Kurunthottikkaaya || AP Komala || P. Bhaskaran ||
|-
| 7 || Madhurappoovana || LR Eeswari, Chorus || P. Bhaskaran ||
|-
| 8 || Peraattin Karayil || MS Baburaj || P. Bhaskaran ||
|-
| 9 || Pottichirikkalle || P. Leela || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 


 