An American Hippie in Israel
{{Infobox film
| name           = An American Hippie in Israel
| image          = American_Hippie_in_Israel_Grindhouse_Releasing.jpg
| image_size     = 210px
| border         = yes
| alt            = 
| caption        =
| film name      = Ha-Trempist
| director       = Amos Sefer 
| producer       = Amos Sefer
| writer         = Amos Sefer
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = Yaackov Kallach
| editing        = Amos Sefer
| studio         =  
| distributor    = Grindhouse Releasing Box Office Spectaculars
| released       = 1972
| runtime        = 95 minutes
| country        = Israel
| language       = English
| budget         = 
| gross          = 
}} Hebrew הטרמפיסט, science fiction-action rediscovered decades later by the cult film enthusiasts at Grindhouse Releasing who have digitally restored the film and presented it in Blu-ray and DVD.

 

==Plot==
Young American hippie and war veteran, Mike (Asher Tzarfati), travels to Israel shortly after his involvement in Vietnam War|Vietnam. Hitchhiking and "bumming around" Hippie Mike meets up with three Israelis along his journey — stage actress Elizabeth (Lily Avidan), another female hippie friend (Tzila Karney), and her Hebrew-speaking boyfriend Komo (Shmuel Wolf). They join a larger group of hippies and decide to form an isolated community on a deserted island, where they can live in peace "without clothes, without government, and without borders."   

The hippies are not without their foils, and find them in two gun-toting, top-hat-clad, mime-like enforcers (who regularly appear out of nowhere) and who are out to make Mikes life miserable. Reviewer Brian Orndorf notes that, “Sefer doesnt explain who these guys are or what theyre ultimately after, hinting that the duo might be the personification of "The Man" out to silence the hippie uproar.”   
 
Led by Hippie Mike, the quartet of hippies manages to survive a gun attack by the mad mimes and the group flees to the uninhabited island. On the road trip to the island, the hippies quickly forget their worries, tossing their clothes in the wind and enjoying the ride in Elizabeth’s chic convertible car. As they drive down the Israeli coast, they stop along the way to make love, to drive in circles for no reason, and to pick up the supplies theyll need on their island (including the purchase a bag of groceries and a live goat).

They finally arrive at the island and park their car on shore, using a small dingy boat to row their way out there. Once on the Island, the elated hippies do a free form dance, stripping off their clothes and when dusk hits, they settle in for some delicious canned food by the fireside. That night, the hippies pronounce their love for each other and proclaim how, “full of shit” the world is. They wake up the following morning to find that both their boat and the goat have mysteriously vanished overnight.

Shark-infested waters then make it impossible for any of them to swim to the mainland where the car sits on shore. As the women get hungry and tempers flare, the men forage for food, winding up with nothing but a handful of barnacles that Mike manages to scrub off of some rocks. Much like the story that clearly was an inspiration, The Lord of the Flies,  when push comes to shove, the idyllic situation quickly turns from Eden to hell and the dark psyche of the Hippies’ true nature (the nature of all men) is revealed in the end. Ultimately man can escape from society but he cannot escape from his own nature, which director Sefer is saying, is crazy, selfish and violent.

==Cast==
* Asher Tzarfati as Mike
* Lily Avidan as Elizabeth
* Shmuel Wolf as Komo
* Tzila Karney as Françoise

==Development and production==
An American Hippie in Israel was written and directed by Amos Sefer, with principal photography having commenced around 1971.      It was director Sefer’s second film.

==Release==
An American Hippie in Israel had its original limited theatrical release in 1972, but initially failed to find a distributor willing to take on the film.    "Why Grindhouse Releasings Bob Murawski wants you to see An American Hippie in Israel", Nashville Scene, by Coco Hames, August 15, 2013  Several decades later, it  was rediscovered by film editor/distributor   that Murawski had edited, which was then picked up and seen by cult fans all over the world, including in Israel, the country of origin. Suddenly the film was receiving renewed attention and quickly became a hot ticket on the Tel Aviv midnight movie circuit, where it became so popular that it is now screened monthly. In Tel Aviv, led on by uber fan Yaniv Edelstein,  the film’s enthusiasts have acted out scenes from the film and talked back to the screen, interacting with the film much in the same way as fans do with the American midnight staple The Rocky Horror Picture Show.      Hippie was released for the first time on Blu-ray and DVD on September 10, 2013, by Grindhouse Releasing.       The film had also previously screened theatrically in the United States and at the Grindhouse Film Festival on August 27, 2013. 

Turner Classic Movies showed it on Sunday, December 28, 2014 at 2:00 AM EST (and made it available on TCM On-Demand), as part of their "TCM Underground" series, clocking in at about ninety minutes, apparently uncut. No intro by TCMs hosts.

===Reception===
The allegorical extravaganza known as An American Hippie in Israel has been called a “cinematic oddity” with a “creepy sincerity.”   DVD Talk “An American Hippie in Israel” Blu-ray review, by Jesse Skeen, September 14, 2013]      Gil Shefler of The Forward described the film as "perfectly awful," offering that it " robably   the worst Israeli movie ever made, and a serious candidate for the worst movie of all time, which, surprisingly, is enjoying an unexpected revival 38 years after it was made."    Reviewer Brian Orndorf of Blu-ray.com remarked, “for all the nonsense and pull-your-hair-out padding thats included in the feature, Sefer has a weird vision for "Hippie" that almost works if one squints hard enough, attempting to make an anti-war picture thats soaked in oddity and nudity. Its an admirable effort, with periods of floppy B-movie shenanigans that are surprisingly entertaining.    Orndorf gave Grindhouse Releasing’s Blu-ray release a rating of 4 stars out of 5, and 4-1/2 stars alone for the generous bonus materials on the release, commenting that “As a punchline, An American Hippie in Israel is easily digestible and consistently amusing—a perfect addition to any cult movie collection.”   

==See also==
 
* List of films related to the hippie subculture
* List of films considered the worst

==References==
 

==External links==
* 
* 
* 
*  at Trailers from Hell

 
 
 
 
 