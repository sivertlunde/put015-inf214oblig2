The City Slicker
 
{{Infobox film
| name           = The City Slicker
| image          = 
| caption        = 
| director       = Gilbert Pratt
| producer       = Hal Roach
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 12 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd. Prints of the film survive in the film archive of the Library of Congress.   

==Cast==
* Harold Lloyd - Harold
* Snub Pollard - Snub (as Harry Pollard)
* Bebe Daniels - The Girl
* Helen Gilmore - Girls Mother
* William Blaisdell - Bebes rejected suitor
* Gus Alexander
* Sammy Brooks
* Lige Conley - (as Lige Cromley)
* Billy Fay
* William Gillespie
* Wallace Howe
* Dee Lampton - Driver
* Gus Leonard - Old man playing checkers Charles Stevenson - (as Charles E. Stevenson)

==See also==
* List of American films of 1918
* Harold Lloyd filmography

==References==
 

==External links==
* 
*   on YouTube

 
 
 
 
 
 
 
 
 


 