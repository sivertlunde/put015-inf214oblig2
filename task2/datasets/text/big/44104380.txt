Sunes familie
{{Infobox film
| name           = Sunes familie
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Hans Kristensen
| producer       = 
| writer         = 
| screenplay     = Hans Kristensen Thorvald Lervad John Stefan Olsen
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    =  1997
| runtime        = 
| country        = Denmark Danish
| budget         = 
| gross          = 
}}
Sunes familie is a is a Danish comedy film|comedy, childrens film|childrens and family film opening on 10 October 1997 in film|1997.
 book rather than the Swedish film. Names have been translittered, renaming the familys last name from Andersson to Andersen, Anna is Anne, Isabelle is Isabel, Håkan is Håkon and Sophie is Sofie. Places are relocalized, allowing the Anderssons family to live in Denmark instead of Sweden.

==Actors==
*Claus Bue
*Vibeke Hastrup
*Per Damgaard Hansen
*Sofie Lassen-Kahlke
*Niels Anders Thorn
*Inge Sofie Skovbo
*Stephanie Leon
*Erni Arneson
*Joachim Knop
*Jarl Friis-Mikkelsen
*Henrik Lykkegaard
*Anders Nyborg
*Mari-Anne Jespersen
*Peter Jorde
*Susan Olsen
*Folmer Rubæk
*Bodil Jørgensen
*Jarl Forsman
*Jan Hertz
*Hans Henrik Voetmann

==Opening dates==
Opening dates 

{| class="wikitable" Country
!align="center"|Cinema opening date
|- Denmark
|align="center"|10 1997
|}

==References==
 

 
 
 
 
 
 
 
 