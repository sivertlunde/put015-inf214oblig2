Enter Arsène Lupin
{{Infobox film
| name           = Enter Arsene Lupin
| alt            =  
| image	=	Enter Arsène Lupin FilmPoster.jpeg
| caption        = 
| director       = Ford Beebe
| producer       = {{Plainlist|
*Ford Beebe
*Joseph Gershenson}}
| writer         = {{Plainlist|
*Maurice Leblanc
*Bertram Millhauser}}
| starring       = {{Plainlist|
*Charles Korvin
*Ella Raines
*J. Carrol Naish
*George Dolenz}}
| music          = Milton Rosen
| cinematography = Hal Mohr
| editing        = Saul A. Goodkind
| studio         = 
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Enter Arsène Lupin is a 1944 American crime film directed by Ford Beebe and starring Charles Korvin, Ella Raines and J. Carrol Naish. It features the French gentlemen thief Arsène Lupin, a creation of the writer Maurice Leblanc. Lupin keeps watch on a young woman whose jewels make a tempting target for a gang of thieves. It was made by Universal Pictures.

==Cast==
* Charles Korvin as Arsene Lupin 
* Ella Raines as Stacie Kanares 
* J. Carrol Naish as Ganimard 
* George Dolenz as Dubose 
* Gale Sondergaard as Bessie Seagrave 
* Miles Mander as Charles Seagrave 
* Leyland Hodgson as Constable Ryder 
* Tom Pilkington as Pollett 
* Lillian Bronson as Wheeler 
* Holmes Herbert as Jobson 
* Charles La Torre as Inspector Cogswell 
* Gerald Hamer as Doc Marling 
* Ted Cooper as Cartwright 
* Art Foster as Superintendent 
* Clyde Kenney as Beckwith

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 