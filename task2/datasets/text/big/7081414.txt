Starting Out in the Evening
{{Infobox Film
| name           = Starting Out in the Evening
| image          = SOITE_poster.jpg
| caption        = Original poster
| director       = Andrew Wagner
| producer       = Jake Abraham Nancy Israel Fred Parnes Andrew Wagner Gary Winick Brian Morton (novel)
| starring       = Frank Langella Lauren Ambrose Lili Taylor Adrian Lester
| music          = Adam Gorgoni
| cinematography = Harlan Bosmajian
| editing        = Gena Bleier
| distributor    = Roadside Attractions
| released       = November 23, 2007
| runtime        = 111 minutes
| country        = United States
| language       = English
| gross          = $898,786
}}
 2007 Cinema American drama Brian Morton.

==Plot==
Now aging and ailing, one-time celebrated author Leonard Schiller has been forgotten by his readers, literary colleagues, and critics during the decade he has struggled to complete what he knows will be his final novel. When brash, ambitious Brown University graduate student Heather Wolfe approaches him with a request for access to his thoughts and recollections for the Dissertation|Masters thesis she hopes will reintroduce the public to his work, he initially refuses to cooperate. But the young woman is relentless, and he finally agrees to weekly meetings in which he slowly begins to open up to her as he reluctantly recalls his past.

Slightly suspicious of Heathers motives is Leonards daughter Ariel, a former professional dancer who supports herself by teaching yoga and Pilates. Rapidly approaching forty, Ariel has stopped using birth control with her boyfriend Victor without telling him about her determination to have a baby. When he learns about her plan, she ends their relationship, coincidentally at the same time Ariels former lover Casey Davis returns to New York City after a five-year absence. He and Ariel had reached an impasse in their relationship because of his refusal to have a child, and as they begin to see each other again, he is quick to let her know his position hasnt changed.

The film focuses on these four individuals and their evolution as they are thrust out of their comfort zones and into arenas that force them to examine their lives and decide how much they are willing to compromise and sacrifice their own desires in order to accommodate the demands of others.

==Production==
Starting Out in the Evening was shot on location on the Upper West Side of Manhattan in eighteen days.   

It premiered at the Sundance Film Festival and was shown at the Toronto International Film Festival, the Edmonton International Film Festival, the Mill Valley Film Festival, the Austin Film Festival, the Hamptons International Film Festival, the Virginia Film Festival, and the Denver Film Festival before opening on seven screens in the US on November 23, 2007. It grossed $76,214 on its opening weekend and eventually earned $898,786 in the U.S. 

==Cast==
*Frank Langella as Leonard Schiller
*Lauren Ambrose as Heather Wolfe
*Lili Taylor as Ariel Schiller
*Adrian Lester as Casey Davis
*Michael Cumpsty as Victor
*Jessica Hecht as Sandra Bennett
*Dennis Parlato as Author
*Jeff McCarthy as Charles

==Critical reception==
Rotten Tomatoes reported 86% of critics gave the film positive reviews, based on 92 reviews,  while Metacritic reported the film had an average score of 78 out of 100, based on 33 reviews. 

A.O. Scott of the New York Times observed, "Even though it is less populous than Mr. Morton’s novel  . . . the adaptation . . . rarely feels unduly claustrophobic or rarefied. Allusions and incidents that evoked the milieu of Leonard’s younger days, and the texture of his mind, have been pruned away. But in their place is the marvelous fact of Mr. Langella, who carries every nuance of Leonard’s experience — including his prodigious, obsessive reading — in his posture and his pores . . . And what is so remarkable about   is that he seems to hold Leonard’s intellectual cosmos inside him, to make it implicit in the man’s every gesture and pause." 

Roger Ebert of the Chicago Sun-Times rated the film four out of four stars and commented, "The movie is carefully modulated to draw us deeper and deeper into the situation, and uses no contrived plot devices to superimpose plot jolts on what is, after all, a story involving four civilized people who are only trying, each in a different way, to find happiness." 

Mick LaSalle of the San Francisco Chronicle said the original novels appeal was in "the simple but elegant prose and in the way Morton lets us see into the thought processes of his characters. Neither of those elements translates into a visual medium, and so anyone adapting this novel was sure to face a challenge. Unfortunately, director and co-writer Andrew Wagner compounds the difficulty by wallowing in the storys bleakness and settling for sentimental gestures from his actors. Frank Langella is the single big exception on the acting score . . . Starting Out in the Evening has the feeling of a film in which the actors, left to direct themselves, played into their own self-indulgent instincts, and the only one who resisted was the old pro who knew better." 

Peter Travers of Rolling Stone called the film "remarkable" and said "Langella delivers a master class in acting   deeply felt portrait of a lion in winter." 

Meghan Keane of the New York Sun stated, "The glowing accolades that the filmmakers attempt to bestow on the novelist Brian Morton ultimately result in an undercooked product . . .  he film is unable to re-create the delicate balance of emotions that Mr. Morton created in his book of the same name. The deference to   work is clear throughout the picture, but the interactions between the characters often fail to realize the soft sentiment that his novel achieved." 

Scott Foundas of Variety (magazine)|Variety called the film "a wise, carefully observed chamber drama," a "small yet deeply resonant pic," and "a sophomore film of unusual maturity and confidence - the work of a filmmaker with a sure grasp of his characters." 

Critics for the Washington Post, the New York Times, and the Baltimore Sun cited the film as one of the ten best of the year.

==Awards and nominations== Chicago Film Critics Association Award for Best Actor, the Independent Spirit Award for Best Male Lead, and the Satellite Award for Best Actor - Motion Picture Drama.

Andrew Wagner and Fred Parnes were nominated for the Independent Spirit Award for Best Screenplay.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 