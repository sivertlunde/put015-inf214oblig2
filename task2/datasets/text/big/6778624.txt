The Trail Beyond
{{Infobox film
| name          = The Trail Beyond |
| image         = TheTrailBeyondposter.jpg
| caption       = Film poster
| director      = Robert N. Bradbury
| writer        = Lindsley Parsons (screenplay)
| music         = Lee Zahler
| based on      =  
| starring      = {{plainlist|
*John Wayne
*Noah Beery, Sr.
*Noah Beery, Jr.
}}
| producer      = Paul Malvern
| cinematography= Archie Stout
| distributor   = Monogram Pictures
| released      =  
| runtime       = 55 minutes
| country       = United States
| language      = English
}}
 Western film The Wolf The Wolf The Wolf Hunters (1949).

This film presents an extremely rare opportunity to see Wallace Beerys brother and nephew appear together in a movie.  Noah Beery, Jr., who played "Rocky" in The Rockford Files forty years later, has an extremely large role as John Waynes characters best friend and appears alongside Wayne in almost every scene, while the senior Beery enjoys only a few minutes of screen time despite his higher billing. Wayne was 27 years old when The Trail Beyond was shot, while Beery, Jr. was 21.

Stunning location backgrounds filmed around Mammoth Lakes, California set this film firmly apart from most of the other Poverty Row westerns shot during the decade in which Wayne found himself trapped between his screen masterpieces The Big Trail (1930) and Stagecoach (1939 film)|Stagecoach (1939).

==Cast==
* John Wayne as Rod Drew
* Verna Hillie as Felice Newsome
* Noah Beery, Sr. as George Newsome 
* Noah Beery, Jr. as Wabi
* Robert Frazer as Jules LaRocque
* Iris Lancaster as Marie LaFleur
* James A. Marcus as Felices uncle 
* Eddie Parker as Ryan, the Mountie
* Earl Dwire as Henchman Benoit

==See also==
* John Wayne filmography
* List of American films of 1934

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 