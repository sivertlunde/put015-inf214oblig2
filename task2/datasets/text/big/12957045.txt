Everyday People (film)
{{Infobox Film
| name           = Everyday People
| image_size     = 
| image	=	Everyday People FilmPoster.jpeg
| caption        =  Jim McKay
| producer       = 
| writer         = Jim McKay
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = HBO
| released       = January 18, 2004
| runtime        = 
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Jim McKay. The storyline revolves around the lives of the employees working at a restaurant in Brooklyn, New York City, which is to be closed down due to economic shortfall.

==Plot summary==
The plot takes place on a day in Brooklyn. The owner of a neighborhood diner has decided to shut down his restaurant but, on the last day of its business, he decides to give it a second thought after realizing that peoples lives depend on it. In the meantime, this sudden shutdown notice puts a heavy impact on the employees, as many of them become uncertain about their future. The movie does not have an explicit ending as to whether the diner was shut down or not.

==Cast==
*Jordan Gelber as Ira
*Steve Axelrod as Sol
*Earl Baker Jr. as Benjamin
*Bridget Barkan as Joleen
*Kalimi Baxter as Ruby
*Ron Ben Israel as Walter
*Stephanie Berry as Angry Black Waiter
*Miles Bridgett as Joleens Son
*David Brummel as Iras Father
*Ron Butler as Ron Harding
*Kadijah Carlisle as Benita
*Julia Carothers Hughes as Miss Meyers
*Reg E. Cathey as Akbar
*muMs da Schemer as Ali

==Reception==
The film was well received for its genuine representation of the struggles of low-income people.  The film won "Black Reel" award in "Best supporting Actress" category. It received 75% (Fresh) rating at Rotten Tomatoes.

==External links==
* 
* 

 
 
 
 
 
 


 