So This Is Love (film)
 
{{Infobox film
| name           = So This Is Love
| image          = 
| image_size     = 
| caption        =  Gordon Douglas
Oren W. Haglund (assistant director)
| writer         = 
| narrator       = 
| starring       = Kathryn Grayson
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Warner Brothers
| released       = July 15, 1953
| runtime        = 101 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = $1.75 million (US) 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Gordon Douglas, based on the life of singer Grace Moore. The film stars Kathryn Grayson as Moore, and Merv Griffin. 

==Cast==
*Kathryn Grayson as Grace Moore
*Merv Griffin as Buddy Nash
*Joan Weldon as Ruth Obre
*Walter Abel as Colonel James Moore
*Rosemary DeCamp as Aunt Laura Stokley
*Douglas Dick as Bryan Curtis

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 
 