American Samurai (film)
 
{{Infobox film
| name           = American Samurai
| image          =
| caption        =
| director       = Sam Firstenberg
| producer       =
| writer         = John Corcoran David Bradley Mark Dacascos
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1992
| runtime        = 94 minutes
| country        = United States English
| Budget         =
| preceded by    =
| followed by    =
}}
 David Bradley and Mark Dacascos and produced by Cannon Films. The movie was filmed in Turkey and released in the U.S. in 1992.  This movie represents the first major role for actor Mark Alan Dacascos.

==Plot==
After a plane crash in the Japanese mountains, its only survivor—a baby named Andrew—is adopted by a samurai master. Andrew, along with the samurais son, Kenjiro, are trained in the warrior’s way. Andrew excels in his training and soon surpasses his stepbrother’s skills. Kenjiros jealousy pushes him to join the Yakuza, where he takes the Mafia oath and forsakes the moral values of the samurai’s code. He leaves his father’s home, swearing to one day take revenge on his brother.
Ten years later, Andrew works in L.A. as a journalist. He and a female photographer track down an opium-smuggling operation in Turkey. Soon, the drug dealers kidnapped the girl, forcing Andrew to enter a deadly martial-arts tournament ruled by illegal gamblers whose greatest champion is the lethal Kenjiro.

==Cast==
* David Bradley as Andrew Drew Collins
* Mark Dacascos as Kenjiro Sanga
* Valarie Trapp as Janet Ward
* Rex Ryon as Ed Harrison
* Melissa Hellman as Samantha
* John Fujioka as Tatsuya Sanga
* Douvi Cohen as Stephane
* Mark Warren as Turk in Disco / Lars
* Koby Azarly as Turk in Disco
* Shalom Avitan as Turk in Disco
* Baruch Berkin as Hotel Clerk
* Arie Moscuna as Announcer
* Michael Morim as Police Chief
* Misha Gal as Body Guard
* John Slater as Body Guard

== DVD and versions ==
The DVD is available in Region 1. However, it is based on the edited R-rated cut. This version has subtitles added to the days of the tournament (i.e. "Day 2", "Day 3", etc.) Additionally, many scenes of violence or injury are zoomed in on, poorly cropped, or deleted altogether to avoid explicit details. The unrated cut has different dialogue in some scenes, no subtitles, and all of the violence is onscreen and considerably more graphic. This version is not available in the U.S. but can be found in other regions. There is an uncut Belgian bootleg DVD and the British version has approximately one second of footage cut. 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 