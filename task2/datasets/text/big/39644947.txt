Sorga Palsoe
{{Infobox film
| name           = Sorga Palsoe
| image          = 
| image_size     = 
| border         = 
| alt            = A black-and-white advertisement
| caption        = Newspaper advertisement, Surabaya
| director       = Tan Tjoei Hock
| producer       =
| screenplay     = Fred Young
| narrator       = 
| starring       ={{plain list|
*Lo Tjin Nio
*Tong Hui
*Lim Poen Tjiaw
*Rohana
}}
| music          = Mas Sardi
| cinematography = 
| editing        = 
| studio         = Java Industrial Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}}
Sorga Palsoe ( ; Indonesian for False Heaven) is a 1941 film from the Dutch East Indies (present-day Indonesia) which was directed by Tan Tjoei Hock for Java Industrial Film. The tragedy, starring Lo Tjin Nio, Tong Hui, Lim Poen Tjiaw, and Rohana, was a commercial failure. It is likely lost film|lost.

==Plot==
Hian Nio is displeased as her mother, Roti, treats her poorly and favours her siblings. One day, Hian Nios boyfriend  Kian Bie is fired from his work. Kian Bies former employer, Bian Hong, uses the opportunity to take Hian Nio as his wife. She is, however, unhappy, and after a year abandons Bian Hong and their newborn daughter. Eventually Hian Nio returns to claim their daughter, but she dies soon afterwards. 

==Production== native oriented Fred Young;  it was Youngs first foray into the industry. 

The film starred Lo Tjin Nio, Tong Hui, Lim Poen Tjiaw, and Rohana.  Filming for the black-and-white film began on 27 December 1940 and was completed by January of the following year.  Music direction was handled by Mas Sardi. 

==Release and reception==
Sorga Palsoe was screened in Surabaya, East Java, by late February 1941  and in Medan, North Sumatra, by April.  The film, targeted at uneducated, lower-class viewers,  was a commercial failure.  An anonymous review in the daily Soerabaijasch Handelsblad found the film to have a tragic ending and emphasised the role of ethnic Chinese in production. 

The film is likely lost film|lost. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and film historian Misbach Yusa Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
 

==Works cited==
 
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite web
 |title=Fred Young
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/name/nmp4b9bad3b7af11_Fred-Young#.UGF4tKA24gc
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |archivedate=25 September 2012
 |accessdate=25 September 2012
 |archiveurl=http://www.webcitation.org/6AwFOkSNM
 |ref= 
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G.
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Mas Sardi
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b97cbf8435d6_Mas-Sardi
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 11 June 2013
  | archiveurl = http://www.webcitation.org/6HIX86QtM
  | archivedate = 11 June 2013
  | ref =  
  }}
* {{cite web
  | title = Sorga Palsoe
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s011-40-986379_sorga-palsoe
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 25 July 2012
  | archiveurl = http://www.webcitation.org/69QHZmwic
  | archivedate = 25 July 2012
  | ref =  
  }}
*{{cite news
 |title=Sorga Palsoe (De Valsche Hemel)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011122601%3Ampeg21%3Ap006%3Aa0112
 |work=Soerabaijasch Handelsblad
 |location=Surabaya
 |page=6
 |date=24 February 1941
 |publisher=Kolff & Co.
 |accessdate=11 June 2013
 |ref= 
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011176887%3Ampeg21%3Ap008%3Aa0072
 |work=De Sumatra Post
 |location=Medan
 |page=8
 |date=21 April 1941
 |accessdate=11 June 2013
 |ref= 
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011122598%3Ampeg21%3Ap004%3Aa0070
 |work=Soerabaijasch Handelsblad
 |location=Surabaya
 |page=4
 |date=22 February 1941
 |publisher=Kolff & Co.
 |accessdate=11 June 2013
 |ref= 
}}
 

==External links==
* 
 
 
 
 
 
 

 
 