Not Suitable for Children
 
 
{{Infobox film
| name           = Not Suitable for Children
| image          = notsuitableforchildrenposter.jpg
| image_size     = 
| alt            =
| caption        = Theatrical film poster
| director       = Peter Templeman
| producer       = Jodi Matterson
| writer         = Michael Lucas
| starring       = Ryan Kwanten Sarah Snook Ryan Corr
| music          = Jono Ma Matteo Zingales
| cinematography = Lachlan Milne
| editing        = Matthew Walker
| studio         = Wild Eddie Exit Films Frame, Set & Match
| distributor    = Icon Film Distribution
| released       =  
| runtime        = 97 minutes 
| country        = Australia
| language       = English
| budget         = $4.5 million 
| gross          = $468,959 
}}
Not Suitable for Children is an 2012 Australian romantic comedy film directed by Peter Templeman and written by Michael Lucas. It was released on 12 July 2012. It stars Ryan Kwanten, Sarah Snook, and Ryan Corr.

==Plot==
 

The movie opens up with a scene from a party. The main character, Jonah (Ryan Kwanten), is introduced as a young party-boy without a care in the world. The power goes out for a short period of time because Jonah has not paid his electric bills, but gets it back thanks to his neighbour. Afterwards, Jonah meets up with Becky (Kathryn Beck) and they retreat up to Jonahs room away from the party, only for Becky to find a lump in Jonahs testicle. Jonah goes to see a doctor, who reveals that Jonah has testicular cancer. He tells Jonah that they can remove it in time, but Jonahs upset that he wont be able to father children. The doctor suggests that Jonah uses a sperm bank in the event that he would like to have kids after the operation.

Now at a sperm bank, Jonah is left in a room to privately collect the semen. After he is finished, he returns home to his best friends and fellow party hosts, Stevie (Sarah Snook) and Gus (Ryan Corr). When Stevie arrives home from work, Gus blurts out that Jonah has cancer, much to Jonahs dismay. Gus asks if would still like to have the party scheduled for the coming Friday, and Jonah allows them to keep the date, not wanting to explain his condition to party-goers.

Jonah receives a call from the sperm bank and goes in for a meeting. The nurse explains that Jonah falls into a small percentage of men whose sperm cannot be frozen because of biological complications. Jonah asks what other options there are, now very worried that he will never have children. The nurse replies with, "Well, do you have a girlfriend?"

Jonah then begins the complicated journey of finding a woman to carry his child. He first approaches his ex-girlfriend Ava (Bojana Novakovic) who is disgusted with Jonah for even contacting her. He then tries Becky (or "Stalker Becky" as Gus calls her) who rejects him by claiming she never thought of her and Jonah as a couple. Jonah never tells the girls he asks that he has cancer. Jonah goes back to the doctor to ask for more time and moves his operation three weeks.

Meanwhile, Gus, Stevie, and Jonah realize that hosting parties could be their job. After the power went out in the earlier party and party-goers pitched in to help pay, Gus realizes that the three of them could make a weekly salary if they held a party once a week. Jonahs first "paycheck" is worth over $700.

Jonah then begins making lists of all the women he has ever dated or known and asks them if they would ever consider after his child. He is rejected by all the women he asks. Stevie suggests adoption and Jonah looks into it, but soon figures out that in order to be a candidate for adoption you must have a clean health record, and Jonah has "cancer" written all over his. After having no luck, Stevie suggests that Jonah try an "arrangement" with a woman. Stevie says she knows of a lesbian couple at her work who were looking into sperm donation and sets up a meeting for Jonah. The meeting does not go in Jonahs favour and the couple declines Jonahs offer. Stevie suggests another woman at work who wants a child, but is not married and sets up a meeting with her too. Although Jonah goes home with her, he ends up not sleeping with her. He claims that he was too drunk to have sex and couldnt focus.

Stevie starts to like the idea of an arrangement, especially after Jonah claims that he would give his house to the woman who carried his child. Stevie accidentally tips off Jonah that she would be interested in a deal and sends Jonah out of her room to think.

At the weekly party Stevie stays in her room, claiming that she has a migraine. Jonah calls her and tells her that he would give her anything if she would carry the child. The morning after the party, Jonah receives a text message from Stevie asking him to come to her room. It is revealed that the whole night Stevie stayed up and wrote a contract concerning her and Jonahs deal. Jonah reads through the contract and signs it and Stevie tells him that they have a four-day window that Stevie should be ovulating in.

They book a room at a cheap motel for the four days, with the final being two days before Jonahs operation. Stevie and Jonah go to a pharmacy and buy a syringe because Stevie does not want to have sex with Jonah. Back at the motel, Jonah accidentally breaks the syringe. Stevie says that it should be okay, they just missed the first day of a four day window. Jonah is frustrated and Stevie gives in and says she is okay with natural. They have sex, and for the rest of the four days they meet at the hotel, have sex, and then stagger their arrival at home so Gus doesnt notice.

On the fifth day while Stevie is at work, she gets her period. She calls Jonah and asks him to meet her outside her work. They get into a fight, with Stevie claiming she was happy her period came because she was having second thoughts anyway. Jonah leaves, realizing he is out of time.

Stevie is absent at the party that night, but Gus has made a call to Ava. Ava arrives at the party, first mad at Jonah for not telling her he had cancer and then says she would carry his child if he wanted. They go upstairs to his room and have sex, but Jonah pulls out at the last moment because he realizes he is in love with Stevie. Jonah leaves to go find her, hoping she is home. Jonah asks Ava to stay in his room. Jonah finds Stevie, but Ava comes out of Jonahs room and Stevie storms off, feeling betrayed. Meanwhile, the police arrive to stop the party.

Jonah catches up to Stevie and tells her that he loves her, but Stevie leaves.

The next morning, Jonahs sister comes to pick him and Gus up for the operation. Jonah is sad at first, but Stevie does show up for moral support. Stevie tells Jonah (in front of Gus, who never suspected anything going on between them) that she thought Jonah would make a great father and he should look into sperm donation or adoption with her. They kiss and Jonah is wheeled away into the operating room. The final line of the movie is, "What the f***." Said by a very confused Gus.

==Cast==
* Ryan Kwanten as Jonah
* Sarah Snook as Stevie
* Ryan Corr  as Gus
* Bojana Novakovic as Ava
* Alice Parkinson as Alison
* Daniel Henshall as Dave
* Clare Bowen as Gypsy
* Kathryn Beck as Becky
* Susan Prior as Marcy
* Tasneem Roc as Miranda

==Reception==
Not Suitable for Children earned AUD$468,959 at the Australian box office.    The film received  positive reviews from critics, earning an 88% approval rating on Rotten Tomatoes.

==Awards and nominations==
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (2nd AACTA Awards|2nd)  AACTA Award Best Actress in a Leading Role Sarah Snook
| 
|- AACTA Award Best Original Music Score Jono Ma
| 
|- Matteo Zingales
| 
|- AACTA Award Best Actor in a Supporting Role Ryan Corr
| 
|- AACTA Award Best Original Screenplay Michael Lucas
| 
|- AWGIE Awards|AWGIE Award Best Writing in a Feature Film - Original
| 
|- Film Critics FCCA Awards Best Film
|  
|- Best Director Peter Templeman
| 
|- Best Actress Sarah Snook
|  
|-
| Best Actor in a Supporting Role
| Ryan Corr
|  
|- Best Music Score
| Jono Ma
|  
|-
| Matteo Zingales 
|  
|-
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 