The King of Kings (1963 film)
{{Infobox film
| name           = The King of Kings
| image          = 
| image size     = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Jirí Mucha Jirí Weiss Martin Frič
| starring       = Jirí Sovák
| music          = 
| cinematography = Václav Hunka
| editing        = Jan Kohout
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
 Czechoslovak comedy film directed by Martin Frič.   

==Cast==
* Jirí Sovák as Lojza Králu - mounter
* Milos Kopecký as Izmail el Sarif ben Serfi
* Vlastimil Brodský as Eda Brabec
* Jirina Bohdalová as Coiffeuse Lenka
* Walter Taub as Assassin Poupard
* Jirí Nemecek as Company director
* Ilja Prachar as Novák - Officer of Ministry of Foreign Affairs
* Zdenek Rehor as Pantucek - Officer of Ministry of Foreign Affairs
* Eduard Kohout as Deputy minister
* Jan Skopecek as Chamberlain
* Ota Motycka as Janitor in factory
* Lubomír Kostelka as Teacher of language
* Zdenek Braunschläger as Constructor
* Josef Bartunek as Guard
* Jitka Frantová as Prostitute

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 