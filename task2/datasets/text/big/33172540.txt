The Cossacks (1928 film)
{{infobox film
| name           = The Cossacks
| image          = 1928_The_Cossacks_poster.jpg
| imagesize      =
| caption        =
| director       = George W. Hill Clarence Brown
| screenplay     = Frances Marion 
| based on       =   John Colton (intertitles) John Gilbert Renée Adorée
| music          = William Axt Paul Lamkoff
| cinematography = Percy Hilburn
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 74 min 
| country        = United States
| language       = Silent English intertitles
}}
 silent drama George Hill John Gilbert The Cossacks by Leo Tolstoy.  

==Cast== John Gilbert - Lukashka
*Renée Adorée - Maryana
*Ernest Torrence - Ivan
*Nils Asther - Prince Olenin Stieshneff Paul Hurst - Sitchi Dale Fuller - Ulitka (Maryanas mother)
*Josephine Borio - Stepka
*Yorke Sherwood - Uncle Eroshka
*Joseph Mari - Turkish Spy

==Production==
The Cossacks was beset with problems due to MGM executives requesting various script changes during filming. Frances Marion, who wrote the screenplay, became frustrated by the numerous requests and later said she "lost track of what the story was really about and the material seemed frayed on all edges." The films stars John Gilbert and Renée Adorée complained about the numerous rewrites and felt their roles were "not worthy".   

Before filming completed, director George W. Hill requested that he be removed as director as he did not like the films subject matter and had tired of Gilbert and Adorées complaints. Clarence Brown was later hired to reshoot several scenes and ultimately completed the film. 

==References==
 

==External links==
* 
*  
* 


 

 
 
 
 
 
 
 
 
 
 
 
 
 


 