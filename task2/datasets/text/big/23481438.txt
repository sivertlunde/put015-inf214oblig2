Dream with the Fishes
{{Infobox Film
| name           = Dream with the Fishes
| caption        = An oddball odyssey about voyeurism, LSD and nude bowling!
| image          = Dream with the Fishes FilmPoster.jpeg
| director       = Finn Taylor
| writer         = Finn Taylor
| starring       = David Arquette
| distributor    = Sony Pictures Classics
| released       = 1997
| runtime        = 97 min English
}}
Dream with the Fishes is a 1997 film directed by Finn Taylor. The film is Taylors directorial debut.

==Plot==
The film follows Terry, a suicidal voyeur who doesnt seem to be able to kill himself. While preparing for jumping off a bridge, he meets Nick who ends up saving his life. Terry discovers that Nick is terminally ill and doesnt have much time left. Scared by the lack of time, Nick offers Terry a deal he cant refuse: Terry will become the beneficiary of Nicks life insurance or, since money doesnt matter to Terry, Nick promises to kill him before he dies. All Nick asks is Terrys help to realize a few fantasies before dying.

Taylor has claimed that the film is loosely autobiographical. Taylor himself once spent six years traveling around the country with a friend.  In one interview, Taylor claimed: "When I was 19, I contemplated suicide and attempted to hold up a drug store." 

==Reception==
The film debuted at the Sundance Film Festival, and would go on to earn $460,000 in limited release.  The film also received a relatively positive reception from critics. Roger Ebert said that the film "shows some of the signs of unchained ambition."   The Los Angeles Times said that "of all the towering blockbusters this summer, Dream With The Fishes has more heart than the lot of them."

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 


 
 