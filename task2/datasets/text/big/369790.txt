The Master of Disguise
 
 
{{Infobox film
| name           = The Master of Disguise
| image          = Masterdisguise.jpg
| caption        = Theatrical release poster
| director       = Perry Andelin Blake
| producer       = Barry Bernardi Sid Ganis Todd Garner Adam Sandler Alex Siskin
| writer         = Dana Carvey Harris Goldberg
| narrator       = Harold Gould
| starring       = Dana Carvey Jennifer Esposito Harold Gould James Brolin Brent Spiner
| music          = Marc Ellis
| cinematography = Peter Lyons Collister
| editing        = Peck Prior Sandy Solowitz
| studio         = Revolution Studios Happy Madison Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = $16 million   
| gross          = $43.4 million   
}} Happy Madison one of the worst films of all time.

==Plot==
In Palermo, Italy in 1979, Fabbrizio Disguisey, the latest in a long line of Italian secret agents known as "Masters of Disguise", breaks up a smuggling ring run by the evil Devlin Bowman but barely avoids getting caught (while disguised as Bo Derek). Bowman is arrested and Fabbrizio decides it best to keep his familys identity a secret from his infant son, Pistachio.

Twenty-three years later, Fabbrizio runs an Italian restaurant in America with his unnamed wife and Pistachio working as, respectively, the cook and a waiter. Thanks to his yet-untold family heritage, Pistachio has a tendency to mock people at random, which are annoying and badly timed impersonations. Hes also looking for a wife that has a large rear like his mother, but his nerdy demeanor makes him unlikeable to women, as well as a target for bullies, like fellow waiter Rex.
 Michael Johnson, Governor Jesse Ventura, and famous singer Jessica Simpson while also threatening to harm his wife, who is brainwashed to not notice anything. Shortly after Fabbrizios disappearance, Pistachio is visited by his unnamed grandfather, who reveals Pistachios heritage and begins training him in the ways of becoming a Master of Disguise.

Eventually, Pistachio gets the basics of the Disguisey way down and his grandfather gets him an assistant, the gorgeous (but small-bottomed) Jennifer Baker, while he has bonded with her son Barney who is trying to learn how to ride a skateboard and becomes attached to Pistachios dog The Cuteness, and who is a little confused about what the job entails. The two search the area where Fabbrizio was kidnapped and find one of Bowmans cigars, which is emblazoned with the symbol of the "Turtle Club". To get into the club, Pistachio disguises himself as a man who dresses and acts like a turtle and ends up embarrassing himself and Jennifer. They manage to learn Bowmans name and scheme, as well as that hell be at an antiques fair the next day.
 Quint and a large cow pie. He uses other disguises, like Bavarian tax agent Constable Mueller and British secret agent Terry Suave, to get Jennifer to safety.

That night, Pistachio and Jennifer look through the clues at a local restaurant, and they deduce that Bowman has forced Fabbrizio to go back to his disguising ways to steal the treasures for Bowman. While there, they find Jennifers boyfriend Trent cheating on her by being out on a date with Pistachios former love interest Sophia, who was also involved with Rex. Pistachio then ends up slapping him into submission (after learning moves from his slapping dummy), then leaving with the love-struck Jennifer.

Pistachio takes Jennifer home, when Jennifer kisses Pistachio, leaving him love-struck. Bowmans men kidnap Jennifer as soon as Pistachio leaves and Jennifer is in her home. Barney finds Pistachio talking with his grandfather via pre-recorded hologram and a plan is formed. Pistachio, after hiding in a cherry pie and disguised as a "Cherry Pie Man," breaks into Bowmans house to rescue Jennifer and his parents and stops the auction. But even after defeating Bowmans ninja army, Bowman has one final trick up his sleeve: he has attached a mask of his own face to Fabbrizios head, making him appear to be Bowman. While the real Bowman escapes, Pistachio is forced to fight his father, who is brainwashed to believe he is Bowman. Grandfather soon arrives to witness Pistachio obtain his first victory.

In the end, Pistachio helps his father snap out his trance by showing off the old habit of putting his underwear on his head, they free "Mama", return the artifacts, Pistachio marries Jennifer, becomes Barneys stepfather and becomes an official Master of Disguise. However, there is one final thing to deal with — Bowman still has the Constitution. Luckily, the Disguiseys locate him in Costa Rica. Disguised as George W. Bush, Pistachio, along with Fabbrizio and his grandfather (who disguised themselves as Bowmans henchmen) defeat Bowman once and for all and retrieve the Constitution. 

Over the end credits, we see numerous out-takes, deleted scenes, musical numbers and the characters partying along to the soundtrack in between credits. We also see Jennifer dressed in a bridal gown, proving that she was married, or engaged, to Pistachio.

After the end credits finally end, while Pistachio is cleaning up his restaurant, he discovers that a dwarfed man who resembles Mario was in the slapping dummy the whole time. Then the dwarfed man chases him. The two have a conversation and say goodbye. Then so does The Cuteness who reveals himself to have been Pistachios grandfather all along.

==Cast==
* Dana Carvey as Pistachio Disguisey Cole Mitchell Sprouse and Dylan Thomas Sprouse as young Pistachio Disguisey
** Dane Morris as teenage Pistachio Disguisey
* Jennifer Esposito as Jennifer Baker
* Harold Gould as Grandfather Disguisey
* James Brolin as Fabbrizio Disguisey
* Edie McClurg as "Mama" Disguisey
* Brent Spiner as Devlin Bowman
* Michael Bailey Smith as Bald Henchman
* Vincent Riverside as Henchman
* Austin Wolff as Barney Baker
* Kenan Thompson as Kenan
* Jay Johnston as Rex Maria Canals as Sophia
* Mark Devine as Trent
* Gabriel Pimentel as Slapping Robot
* Buddy Bolton as Abraham Lincoln
* Robert Machray and Rachel Lederman as Texas couple
* Al Goto and Simon Rhee as Ninja Warrior Steven Ho as Ninjas
* Bryan Jeffrey Price as Jock
* Eugene C. Palmer as Chef Constitution Security Guards
* Andrew Shaifer as Liberty Bell Security Guard
* Virginia Hawkins as Interview Woman
* Leland Crooke as Appraiser
* Erick Avari as Cigar Maker
* Vincent Castellanos as Art Dealer
* Phil Jones, Michael DeLuise, and Larry Cedar as Businessmen
* Tony Wilde as Doug
* Andy Morrow, Naya Rivera, Julius Ritter, and Brighton Hertford (deleted scene) as Captain America kids
* Kyle Sullivan (deleted scene, uncredited) as Kyle
* Shane Lyons (deleted scene, uncredited) as Shane
* Lisa Foiles (deleted scene, uncredited) as Lisa

;Celebrities as themselves
* Bo Derek Michael Johnson
* Jessica Simpson
* Jesse Ventura (uncredited)
* Paula Abdul – Choreographer (uncredited)

==List of Pistachios disguises==
* Inflatable suit
* Schoolgirl
* Prince Lallijama from the Ringidingi Heights
* Turtle Guy
* Gammy Num-Nums (Ruth Gordon)
* Mr. Peru (Tony Montana from Scarface (1983 film)|Scarface) Quint from Jaws (film)|Jaws
* A patch of grass with a fake cow pie as a helmet
* Constable Mueller of the Bavarian Tax Authority
* Terry Suave (David Niven)
* Cherry Pie Man
* Henchman
* George W. Bush
* Mayor Maynot (end credits/deleted scene)
* Bob Ross (end credits/deleted scene)
* Vampire (end credits/deleted scene)
* Charlie McCarthy with an Edgar Bergen mannequin (end credits/deleted scene)
* Groucho Marx (end credits/deleted scene)
* Captain America (deleted scene)
* Gluteus Maximus from Gladiator (2000 film)|Gladiator (end credits) Forrest Gump (end credits)
* Caveman (end credits)

==Soundtrack==
{{Infobox album  
| Name       = The Master of Disguise: Music from the Motion Picture
| Type       = soundtrack
| Artist     = Various Artists
| Cover      = 
| Alt        = 
| Released   = July 23, 2002
| Recorded   = 
| Genre      = 
| Length     = 38:20 
| Label      = Sony Music Entertainment
| Producer   = 
| Last album = 
| This album = 
| Next album = 
}}

{{Album ratings
| rev1      = Allmusic
| rev1Score =   
}} 
===Track listing===
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Recording artist
| total_length    = 38:20

| all_writing     = 
| all_lyrics      = 
| all_music       = 

| writing_credits = yes
| lyrics_credits  = 
| music_credits   = 

| title1          = M.A.S.T.E.R., Pt. 2
| note1           = featuring Lil Fizz from B2K Jerome Jones / Kelton Kessee / Tony Oliver
| lyrics1         = 
| music1          =  Play
| length1         = 2:56

| title2          = Fun
| note2           = 
| writer2         = Rose Falcon / Billy Falcon
| lyrics2         = 
| music2          = 
| extra2          = Rose Falcon
| length2         = 2:55

| title3          = Happy Face
| note3           = 
| writer3         = Beyoncé Knowles / Michael Cooper / Rob Fusari / Calvin Gaines / Falonte Moore
| lyrics3         = 
| music3          = 
| extra3          = Destinys Child
| length3         = 4:18

| title4          = Eenie Meenie Minie Mo
| note4           = 
| writer4         = Brian Adams / Teron Beal / Eddie Berkeley / Keir Gist
| lyrics4         = 
| music4          = 
| extra4          = Strong
| length4         = 3:22
 Walking on Sunshine
| note5           = 
| writer5         = Kimberley Rew
| lyrics5         = 
| music5          = 
| extra5          = Val-C
| length5         = 2:49

| title6          = Master of Disguise
| note6           =  Colleen Fitzpatrick / Jimmy Harry
| lyrics6         = 
| music6          =  Vitamin C
| length6         = 3:00

| title7          = Double Dutch Bus
| note7           = 
| writer7         = Frankie Smith
| lyrics7         = 
| music7          = 
| extra7          = Devin Vasquez
| length7         = 3:21
 Conga
| note8           = 
| writer8         = Enrique García
| lyrics8         = 
| music8          =  Miami Sound Machine
| length8         = 4:14

| title9          = This Could Be Love
| note9           = 
| writer9         = Troy Johnson / Solange Knowles
| lyrics9         = 
| music9          = 
| extra9          = Solange Knowles
| length9         = 4:02

| title10         = Cherry Pie
| note10          = 
| writer10        = Marques Houston / Jerome Jones
| lyrics10        = 
| music10         = 
| extra10         = Jhené Aiko
| length10        = 3:07

| title11         = M.A.S.T.E.R., Pt. 1
| note11          = featuring Play
| writer11        = Bryan Bonwell
| lyrics11        = 
| music11         = 
| extra11         = Hardhedz / Hardhedzz
| length11        = 4:16
}}

==Reception==
The film was universally panned by critics, many of whom considered the plot  , compounding the fact that there were several disguises that would clearly not be recognized by children (Tony Montana from Scarface (1983 film)|Scarface, for example).

As of March 2007, review-aggregation website  , the film scored 12 out of 100, indicating "Overwhelming Dislike".  Comedian and former Mystery Science Theater 3000 host Michael J. Nelson named the film the third-worst comedy ever made. 

==Awards and honors== Worst Supporting Madonna of Die Another Day.  

==Home media==
The film was released on VHS and DVD on January 28, 2003. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 