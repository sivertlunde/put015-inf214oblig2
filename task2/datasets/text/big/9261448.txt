The Tiger Makes Out
{{Infobox film
| name           = The Tiger Makes Out
| image          = Thetigermakesoutmp.jpg
| image_size     = 230px
| caption        = Promotional movie poster for the film
| director       = Arthur Hiller
| producer       = George Justin Eli Wallach (uncredited)  Anne Jackson (uncredited) 
| based on       =  
| screenplay     = Murray Schisgal
| narrator       =
| starring       = Eli Wallach Anne Jackson
| music          = Shorty Rogers
| cinematography = Arthur J. Ornitz
| editing        = Robert C. Jones
| studio         = Elan Productions
| distributor    = Columbia Pictures (1967, original) Sony Pictures (2014, DVD)
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
}}

The Tiger Makes Out is a 1967 comedy film about a kidnapper and his unintended victim, starring Eli Wallach and Anne Jackson and directed by Arthur Hiller. This is Dustin Hoffmans film debut.

==Plot==
Loser Ben Harris (Eli Wallach), an alienated mailman, decides to get a girl the only way he can — by kidnapping her. Putting his plan into operation one rainy night, he spots an attractive young woman. He races ahead of her and prepares an ambush. However, his would-be target finds shelter from the downpour and he ends up pulling a bag down over Gloria Fiske (Anne Jackson) instead. When he carries her back to his basement apartment and removes the bag, he is dumbfounded to find he has captured a middle-aged housewife. With no alternative, he makes do with the person he has caught, but she proves to be not quite what he envisaged.

==Cast==
* Eli Wallach as Ben Harris
* Anne Jackson as Gloria Fiske
* Bob Dishy as Jerry John Harkins as Leo Ruth White as Mrs. Kelly
* Roland Wood as Mr. Kelly
* Rae Allen as Beverly
* Sudie Bond as Miss Lane David Burns as Mr. Ratner
* Jack Fletcher as Pawnbroker
* Bibi Osterwald as Mrs. Ratner
* Charles Nelson Reilly as Registrar
* Dustin Hoffman as Hap

==DVD==
The Tiger Makes Out was released to DVD by Sony Pictures Home Entertainment on August 5, 2014 via its Choice Collection DVD-on-demand service as a Region 1 DVD.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 

 