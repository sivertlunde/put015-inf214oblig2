Son of the Sunshine
{{Infobox film
| name           = Son of the Sunshine
| image          = Son of the Sunshine.jpg
| alt            = 
| caption        =  Ryan Ward
| producer       = Paul Fler
| writer         =  
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         =  
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} Ryan Ward and written by Ward and Matthew Heiti, the film stars Ward as Sonny Johnns, a young man with Tourette syndrome who undergoes an experimental surgical procedure to cure the condition, only to discover that he also loses his supernatural ability to heal others.

The film premiered at Slamdance Film Festival and other film festivals in 2009, and had a general release in 2011.

==Critical response==
The film received 3.5 out of 4 stars from both the Toronto Star  and the National Post. 

==Awards and nominations== Best Original Screenplay at the 32nd Genie Awards in 2012.  Ward won the Best Actor Rising Star Award at the 2009 Edmonton International Film Festival,  and the film won the Audience Award for Best Feature at the 2009 Malibu Film Festival. 

==References==
 

== External links ==
* 

 
 
 
 
 


 