The Other Side of the Mountain Part 2
{{Infobox film
| name           = The Other Side of the Mountain Part 2
| image          = The-other-side-of-mountain-part-2-movie-poster-1978.jpeg
| image_size     = 
| caption        = 
| director       = Larry Peerce
| producer       = Edward S. Feldman
| writer         = Douglas Day Stewart
| narrator       = 
| starring       = Marilyn Hassett Timothy Bottoms
| music          = Lee Holdridge
| cinematography = Ric Waite
| editing        = Walter Hennemann Eve Newman
| art directing  = William L. Campbell
| studio         = Universal Pictures Filmways
| distributor    = Universal Pictures
| released       = February 10, 1978
| runtime        = 100 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $11,565,678   Box Office Mojo. Retrieved April 30, 2014. 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Other Side of the Mountain Part 2 is a 1978 film directed by Larry Peerce. It stars Marilyn Hassett and Timothy Bottoms. It is a sequel to The Other Side of the Mountain.  It follows a recovered Jill Kinmont who goes to Southern California and meets a man named John, who she falls for. But as they learn later, there is more to deal with than just a new romance.

==Cast==
*Marilyn Hassett as Jill Kinmont
*Timothy Bottoms as John Boothe
*Nan Martin as June Kinmont
*Belinda Montgomery as Audra Jo Nicholson
*Gretchen Corbett as Linda Mae Myers

==Production==
The movie was filmed on location in Bishop, California and Victoria, British Columbia.

==Reception==
Vincent Canby of the New York Times was not impressed: "The first film, directed by Larry Peerce, who also directed the spinoff, was a slickly prefabricated bathtub of movie sentimentality, at the heart of which was a gallant young woman facing real handicaps. Part II is really a 30-minute postscript to that story, blown up to feature-length by artificial plot problems and so many flashbacks to the first movie that the second becomes a sort of reverse trailer, a coming attraction for something thats already gone. ...Mr. Peerces direction suggests a certain amount of desperation. You can almost hear him asking, What can I do to make things interesting?" 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 