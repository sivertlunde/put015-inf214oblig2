A Song Is Born
 
{{Infobox film
| name           = A Song Is Born
| image          = A Song is Born poster.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Howard Hawks
| producer       = Samuel Goldwyn Thomas Monroe (story) Harry Tugend (uncredited)
| narrator       =
| starring       = Danny Kaye Virginia Mayo Steve Cochran Benny Goodman Louis Armstrong Tommy Dorsey Lionel Hampton Charlie Barnet Mel Powell
| music          = Hugo Friedhofer Emil Newman
| cinematography = Gregg Toland
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures
| released       =   |ref2= }}
| runtime        = 113 minutes
| country        = United States English
| budget         =
| gross          =
}}
 1948 Technicolor musical film remake of the 1941 movie Ball of Fire with Gary Cooper, starring Danny Kaye and Virginia Mayo. It was directed by Howard Hawks from an original story by Billy Wilder, produced by Samuel Goldwyn and released by RKO Radio Pictures.

Filmed in Technicolor, it featured a stellar supporting cast of musical legends, including Tommy Dorsey, Benny Goodman, Louis Armstrong, Lionel Hampton, and Benny Carter. Other notable musicians playing themselves in the cast include Charlie Barnet, Mel Powell, Harry Babasin, Louis Bellson, Al Hendrickson, The Golden Gate Quartet, Russo and the Samba Kings, The Page Cavanaugh Trio, and Buck and Bubbles. Other actors include Steve Cochran and Hugh Herbert.

==Plot==
Mild-mannered Professor Hobart Frisbee (Danny Kaye) and his fellow academics, among them Professor Magenbruch (Benny Goodman), are writing a musical encyclopedia. In the process, they discover that there is some new popular music that is called jazz, Swing music|swing, boogie woogie or bebop|rebop, introduced to them by two window washers Buck and Bubbles. The professors become entangled in the problems of nightclub singer Honey Swanson (Virginia Mayo). She needs a place to hide out from the police, who want to question her about her gangster boyfriend Tony Crow (Steve Cochran). She invites herself into their sheltered household, over Frisbees objections. While there, she introduces them to the latest in jazz, with which they are unfamiliar, giving the film an excuse to feature many of the best musicians of the era. The songs they play include "A Song Is Born", "Daddy-O", "Im Getting Sentimental Over You", "Flying Home", and "Redskin Rumba".

Eventually, Tony comes by to collect Honey, but by that time, she and Hobart have fallen in love. And the finale, of course, is not decided by guns but by music, its resonance and reverberation.

==Cast==
 
* Danny Kaye as Professor Hobart Frisbee
* Virginia Mayo as Honey Swanson
* Benny Goodman as Professor Magenbruch
* Tommy Dorsey as Himself
* Louis Armstrong as Himself
* Lionel Hampton as Himself
* Charlie Barnet as Himself
* Mel Powell as Himself
* Harry Babasin as Himself
* Ford Washington Lee as Buck (as Buck and Bubbles) John William Sublett as Bubbles (as Buck and Bubbles)
* Page Cavanaugh Trio as Themselves
* The Golden Gate Quartet as Themselves
* Samba Kings as Themselves (as Russo and the Samba Kings)
* Hugh Herbert as Professor Twingle
* Steve Cochran as Tony Crow
* Felix Bressart as Professor Gerkikoff
 

==Production==
Kayes personal writer/composer, Sylvia Fine, who also happened to be Kayes wife, refused to take part in any more of his projects because Kaye had recently left her for actress Eve Arden. Kaye didnt want anyone else writing songs for him, so he simply did not perform any songs in the film.    Nobodys Fool: The Lives of Danny Kaye. Martin Gottfried. Simon and Schuster, 1994, ISBN 0-7432-4476-1 

Hawks had almost no interest in the film, and only came to work on it because of the $250,000 paycheck. When speaking of the film, he said "Danny Kaye had separated from his wife, and he was a basket case, stopping work to see a psychiatrist   day. He was about as funny as a crutch. I never thought anything in that picture was funny. It was an altogether horrible experience."  

==Release== Red River, was second.  However, A Song Is Born never broke even, only earning about $2.2 million, while Red River went on to gross $4.1 million.   It was shown on American Movie Classics, hosted by Nick Clooney, and has been released on home video in both VHS and DVD formats. 

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 