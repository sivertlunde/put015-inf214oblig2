Eat Drink Man Woman
 
{{Infobox film
| name           = Eat Drink Man Woman
| image          = Eat Drink Man Woman.jpg
| caption        = DVD cover
| film name = {{Film name| traditional    = 飲食男女
 | simplified     = 饮食男女
 | pinyin         = yǐn shí nán nǚ}}
| director       = Ang Lee
| producer       = Hsu Li Kong Hsu Kong
| writer         = Ang Lee James Schamus Hui-Ling Wang
| starring       = Sihung Lung  Wang Yu-wen|Yu-wen Wang Jacklyn Wu|Chien-lien Wu Yang Kuei-mei|Kuei-mei Yang
| music          = Mader
| cinematography = Lin Jong
| editing        = Ang Lee Tim Squyres
| distributor    = The Samuel Goldwyn Company
| released       =  
| runtime        = 123 minutes
| country        = Taiwan
| language       = Mandarin
| gross          = $7,294,403
}} Taiwanese film directed by Ang Lee and starring Sihung Lung, Wang Yu-wen|Yu-wen Wang, Jacklyn Wu|Chien-lien Wu, and Yang Kuei-mei|Kuei-mei Yang.    The film was released on August 3, 1994, the first of Lees films to be both a critical and box office success.    In 1994, the film received the Asia Pacific Film Festival Award for Best Film, and in 1995 it received an Academy Award Nomination for Best Foreign Language Film.   

The title is a quote from the  ), Chinese:  . 
 Pushing Hands. These three films show the tensions between the generations of a Confucian family, between East and West, and between tradition and modernity. They form what has been called Lees "Father Knows Best" trilogy. 

==Plot==
The setting is 1990s contemporary Taipei, Taiwan. Mr. Chu (C: 老朱, P: Lǎo Zhū "Old Chu"; Sihung Lung), a widower who is master Chinese chef, has three unmarried daughters, each of whom challenges any narrow definition of traditional Chinese culture:

* Chu Jia-Jen (C: 朱家珍, P: Zhū Jiāzhēn), the oldest one (Yang Kuei-mei|Kuei-Mei Yang), is a school teacher nursing a broken heart who converted to Christianity.
* Chu Jia-Chien (C: 朱家倩, P: Zhū Jiāqiàn), the middle one (Jacklyn Wu|Chien-lien Wu), is a fiercely independent airline executive who carries her fathers culinary legacy, but never got to pursue that passion.
* Chu Jia-Ning (T: 朱家寧, S: 朱家宁, Zhū Jiāníng), the youngest one (Wang Yu-wen|Yu-Wen Wang), is a college student who meets her friends on-again off-again ex-boyfriend and starts a relationship with him.

Each Sunday Mr. Chu makes a glorious banquet for his daughters, but the dinner table is also the family forum, or perhaps “torture chamber,” to which each daughter brings “announcements” as they negotiate the transition from traditional “father knows best” style to a new tradition which encompasses old values in new forms.

Other characters include:

* Uncle Wen (T: 老溫, S: 老温, P: Lǎo Wēn "Old Wen"), chef friend of Mr. Chu
* Liang Jin-Rong (T: 梁錦榮, S: 梁锦荣, P: Liáng Jǐnróng), a young single-mother
* Shan-Shan (C: 珊珊 Shānshān), Jin-Rong’s daughter
* Mrs. Liang (C: 梁母, P: Liáng-mǔ "Liang Mother"), Jin-Rong’s mother, who comes to live with her
* Li Kai (T: 李 凱, S: 李 凯, P: Lǐ Kǎi), an up-and-coming airline executive
* Raymond (C: 雷蒙), Jia-Chien’s ex-lover, with privileges
* Zhou Mingdao (C: 明道, P: Míngdào), volleyball coach with a motor bike
* Guo Lun (T: 國 倫, S: 国 伦, P: Guó Lún), ex-boyfriend of Jia-Nings friend

As the film progresses, each daughter encounters new men. When these new relationships blossom, their roles are broken and the living situation within the family changes. The father eventually brings the greatest surprise to the audience at the end of the story.

==Development==
 . October 19, 1994. Retrieved on November 20, 2013. 

==Cast==
* Sihung Lung as Chu
* Yang Kuei-mei|Kuei-Mei Yang as Jia-Jen
** Jia-Chien believes that Li Kai broke the heart of her older sister Jia-Jen, and Dariotis and Fung wrote that the event seems to have caused Jia-Jen to turn away from the world. Dariotis and Fung, p.  .  Later in film it is revealed that Jia-Jen fabricated the story, in order to "create a barrier against intimacy—even with her family" according to Dariotis and Fung.  Ultimately she marries a new boyfriend after being abstinent for nine years. Her family members seem puzzled when they realize he is not a Christian but Jia-Jen says "He will be." 
** Wei Ming Dariotis and Eileen Fung, authors of "Breaking the Soy Sauce Jar: Diaspora and Displacement in the Films of Ang Lee", wrote that Jia-Jens story is that of a "spinster turned sensual woman".  They wrote that her Christianity was there "perhaps to match her role as a mother-figure". She suspects Jia-Chien of disapproving of her moral system.  Dariotis and Fung wrote that after Jia-Chien states that she needs not a mother but sister, Jia-Jen "is able to become who she really is with all the complexity that entails"  rather than being someone she believed her family needed, with "who she really is" being "a modern, conservative, Christian, sexually aggressive Taiwanese woman".  Desson Howe of the Washington Post wrote that of the actresses, Yang was the "most memorable". 
** Dariotis and Fung argue that Jia-Jens story, along with Jia-Nings, is "not only flat but also dangerously uncomplicated." 
* Jacklyn Wu|Chien-lien Wu as Jia-Chien
**Jia-Chien is sexually liberated. She suspects Jia-Jen of disapproving of her moral system. Dariotis and Fung wrote that the films main focus is on the relationship between Jia-Chien and her father. 
**Chien-lien Wu, who plays Jia-Chien, also portrays Mr. Chus dead wife. Lizzie Francke wrote that Jia-Chien taking the role of the cook "makes manifest the various needs that bind a family by setting a mother back at the heart of it". Dariotis and Fung, p.  .  Dariotis and Fung wrote that therefore the phrase from Francke has multiple meanings since Jia-Chien takes her fathers role of being a chef and therefore "is trying to be the son her father never had" and she takes the role of the mother. 
* Wang Yu-wen|Yu-Wen Wang as Jia-Ning
** Jia-Ning becomes involved with an on-and-off boyfriend of her friend and gets into a love triangle. She unexpectedly becomes pregnant and goes off to live with her boyfriend. Dariotis and Fung wrote that the Chu family expresses "little ceremony or question" before she leaves to be with Guo Lun. 
** Dariotis and Fung wrote that Jia-Nings story is of "naïveté and immature love" and that the love triangle involving her, Guo Lun, and her friend "is in many ways a parody of comic book romance."  Dariotis and Fung argue that Jia-Nings story, along with Jia-Jens, is "not only flat but also dangerously uncomplicated."  They further state that " he lack of inquiry is endemic of this storyline" and that its "superficial treatment" is "quite disturbing." 
* Sylvia Chang as Jin-Rong
* Winston Chao as Li Kai
** Jia-Chien believes that Li Kai broke the heart of her older sister Jia-Jen, and Dariotis and Fung wrote that the event seems to have caused Ji-Jen to turn away from the world.  Later in film it is revealed that Jia-Jen fabricated the story, in order to "create a barrier against intimacy—even with her family" according to Dariotis and Fung. 
* Chao-jung Chen as Guo Lun
** Guo Lun reads Fyodor Dostoyevskys works. In the beginning of the movie his girlfriend, Jia-Nings friend, keeps standing him up and he complains about the situation.  He lives alone in a house because for most of the year his parents are out of Taiwan. Dariotis and Fung wrote that Guo Luns family is "dysfunctional".  Desson Howe of the Washington Post describes him as "mopey".  Dariotis and Fung wrote that Guo Lun has "invisible financial resources that no one in the film questions." 
* Lester Chit-Man Chan as Raymond
* Yu Chen as Rachel
* Kuei Ya-lei as Madame Liang
* Chi-Der Hong as Class Leader
* Gin-Ming Hsu as Coach Chai
* Huel-Yi Lin as Sister Chang
* Shih-Jay Lin as Chiefs Son
* Chin-Cheng Lu as Ming-Dao
* Cho-Gin Nei as Airline Secretary
* Yu-Chien Tang as Shan-Shan
* Chung Ting as Priest
* Hari as Construction Worker
* Cheng-Fen Tso as Fast Food Manager
* Man-Sheng Tu as Restaurant Manager
* Zul as Mendaki
* Chuen Wang as Chief
* Reuben Foong as Drama Mamma
* Jui Wang as Old Wen
* Hwa Wu as Old Man   

==Reception==

===Critical response===
In her review in The New York Times, Janet Maslin praised Ang Lee as "a warmly engaging storyteller." She wrote, "Wonderfully seductive, and nicely knowing about all of its characters appetites, Eat Drink Man Woman makes for an uncomplicatedly pleasant experience."   

In his review in the Washington Post, Hal Hinson called the film a "beautiful balance of elements ... mellow, harmonious and poignantly funny." Hinson concluded:
  }}

On the aggregate reviewer web site Rotten Tomatoes, the film received a 94% positive rating from top film critics based on 31 reviews, and a 91% positive audience rating based on 13,132 reviews. 

===Influence===
Tortilla Soup, a 2001 American comedy-drama film directed by Maria Ripoll, is based on Eat Drink Man Woman.

===Awards===
* 1994 Asia Pacific Film Festival Award for Best Film (Ang Lee) Won
* 1994 Asia Pacific Film Festival Award for Best Editing (Tim Squyres) Won
* 1995 Academy Award Nomination for Best Foreign Language Film
* 1994 Golden Horse Film Festival Award Nomination for Best Supporting Actress (Ya-lei Kuei)
* 1994 National Board of Review of Motion Pictures Award for Best Foreign Language Film Won
* 1994 National Board of Review of Motion Pictures Award for Top Foreign Films Won
* 1995 BAFTA Awards Nomination for Best Film not in the English Language 
* 1995 Golden Globe Awards Nomination for Best Foreign Language Film
* 1995 Independent Spirit Awards Nomination for Best Cinematography (Lin Jong)
* 1995 Independent Spirit Awards Nomination for Best Director (Ang Lee)
* 1995 Independent Spirit Awards Nomination for Best Feature (Ted Hope, Li-Kong Hsu, James Schamus)
* 1995 Independent Spirit Awards Nomination for Best Female Lead (Chien-lien Wu)
* 1995 Independent Spirit Awards Nomination for Best Male Lead (Sihung Lung)
* 1995 Independent Spirit Awards Nomination for Best Screenplay (Hui-Ling Wang, James Schamus, Ang Lee)
* 1995 Kansas City Film Critics Circle Award for Best Foreign Film Won 

==References==
;Notes
 

;Citations
 

;Bibliography
 
*  
* Dariotis, Wei Ming and Eileen Fung. "Breaking the Soy Sauce Jar: Diaspora and Displacement in the Films of Ang Lee." in: Lu, Sheldon Hsiao-peng (Xiaopeng) (editor). Transnational Chinese Cinemas: Identity, Nationhood, Gender. University of Hawaii Press, January 1, 1997. ISBN 0824818458, 9780824818456.
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 