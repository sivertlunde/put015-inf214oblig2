Luna Sea 3D in Los Angeles
{{Infobox album  
| Name        = Luna Sea 3D in Los Angeles
| Type        = live
| Artist      = Luna Sea
| Cover       = Luna Sea 3D album.jpg
| Released    = June 1, 2011
| Recorded    = December 4, 2010 Hollywood Palladium
| Genre       = Progressive rock, alternative rock
| Length      = 
| Label       = Avex Group|HPQ/Avex
| Producer    = Luna Sea
}}{{Infobox film
| name           = Luna Sea 3D in Los Angeles
| image          = 
| caption        = 
| director       = Sojiro Otsubo
| producer       = Sony
| writer         = 
| screenplay     = 
| story          = 
| based on       =   Ryuichi Sugizo J Shinya Shinya
| music          = Luna Sea
| cinematography = Peter Anderson, ASC 
| editing        = 
| studio         = 
| distributor    = Sony PLC
| released       =   
{{cite web
| title      = Press release: LUNA SEA will broadcast the premiere of "LUNA SEA 3D in LOS ANGELES" on Ustream
| work       = purpleskymagazine.com
| date       = 2011-05-27
| url        = http://purpleskymagazine.com/2011/05/press-release-luna-sea-will-broadcast-the-premiere-of-luna-sea-3d-in-los-angeles-on-ustream/
| accessdate = 2012-01-21}}
 
| runtime        = 104 minutes
| country        = Japan
| language       = 
| budget         = 
| gross          = 
}}{{Infobox album  
| Name        = Luna Sea 3D in Los Angeles
| Type        = video
| Artist      = Luna Sea
| Cover       = 
| Cover size  = 
| Released    = February 22, 2012
| Recorded    = December 4, 2010 Hollywood Palladium
| Genre       = Progressive rock, alternative rock
| Length      =  Avex
| Director    = Sojiro Otsubo
| Producer    = 
| Chronology  = 
| Last album  = Ichiya Kagiri no Fukkatsu Live Luna Sea Chinmoku no 7 Nen wo Koete (2011)
| This album  = Luna Sea 3D in Los Angele (2012)
| Next album  = Luna Sea For Japan A Promise to the Brave (2012)
| Misc        = 
}}
 3D concert 20th Anniversary World Tour Reboot -to the New Moon-.
 3D Blu-ray on February 22, 2012.    The DVD version reached number 67 on Oricons DVD chart   oricon.co.jp Retrieved 2012-03-16 

==Live album track listing==
{{tracklist
| headline        = Disc 1
| all_writing     = Luna Sea
| title1          = Gekkou -Opening SE-
| note1           = 月光-Opening SE
| length1         = 1:59
| title2          = Loveless
| length2         = 5:53
| title3          = Déjàvu
| length3         = 5:18
| title4          = Slave
| length4         = 3:40
| title5          = End of Sorrow
| length5         = 4:28
| title6          = True Blue
| length6         = 3:53
| title7          = Gravity
| length7         = 5:48
| note7           = bonus track not included in the film 
| title8          = Ra-Se-N
| length8         = 7:16
| title9          = Providence
| length9         = 6:17
| title10         = Genesis of Mind ~Yume no Kanata e~
| note10          = GENESIS OF MIND〜夢の彼方へ〜
| length10        = 9:07
}}
{{tracklist
| headline        = Disc 2
| title1          = Storm
| length1         = 5:02
| title2          = Desire
| length2         = 4:26
| title3          = Time is Dead
| length3         = 5:30
| title4          = Rosier
| length4         = 5:59
| title5          = Tonight
| length5         = 5:09
| title6          = I for You
| length6         = 6:25
| title7          = Wish
| length7         = 6:57
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 