Kengere
 

{{Infobox film
| name           = Kengere
| image          = 
| alt            =  
| caption        = 
| director       = Tukei Peter
| producer       = Rural Area
| screenplay     = Tukei Peter
| starring       = 
| music          = Sunna Peter
| cinematography = Tukei Peter
| editing        = Sunna Peter
| studio         = 
| distributor    = 
| released       =   
| runtime        = 23 minutes
| country        = Uganda
| language       = 
| budget         = 
| gross          = 
}}

Kengere is a 2010 Ugandan documentary film.

== Synopsis ==
In 1989, Ugandan soldiers accused 69 people of being rebels and locked them in train wagons, then set fire to them. Kengere tells the story of a cyclist who returns to his home village in search of a tape that contains evidence of the crime. Halfway between a documentary and an animation film, this short feature invites us to join a captivating journey with sounds that evoke daily life and some dire moments of Ugandan history.

== References ==
 

 
 
 
 
 
 


 
 