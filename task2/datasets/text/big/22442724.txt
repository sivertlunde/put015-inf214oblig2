The Visitors (1972 film)
{{Infobox film
| name           = The Visitors
| image	         = The Visitors FilmPoster.jpeg
| caption        = Film poster
| director       = Elia Kazan
| producer       = Chris Kazan Nicholas T. Proferes
| writer         = Chris Kazan
| starring       = Patrick McVey
| music          = 
| cinematography = Nicholas T. Proferes
| editing        = Nicholas T. Proferes
| distributor    = United Artists
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $160,000  Cannes Entries Rated by Talking Geiger Counter Champlin, Charles. Los Angeles Times (1923-Current File)   13 May 1972: k1.
 
}}
 Daniel Langs Casualties of War story as a jumping-off point for this film.   

==Plot== Vietnam in the same platoon but later ended up on opposite sides of a court-martial. Bill has never told his girlfriend what happened in Vietnam nor at the court-martial. The story slowly unfolds. Under orders in Vietnam not to take any prisoners, and faced with potentially hostile civilians who might attack them if left behind, Mike kills a civilian. Bill testifies against him and Mike is sent to the brig (military prison) for two years. He is angry. There is sexual tension between Mike and Martha. The tension builds and culminates in a fight and a rape.

==Cast==
* Patrick McVey as Harry Wayne
* Patricia Joyce as Martha Wayne
* James Woods as Bill Schmidt
* Steve Railsback as Mike Nickerson
* Chico Martínez as Tony Rodrigues

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 