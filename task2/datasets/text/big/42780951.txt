Yodha (2009 film)
{{Infobox film name           = Yodha image          = image_size     = caption        = director       = Om Prakash Rao producer       = Rockline Venkatesh writer         = Senthil Kumar narrator       = starring  Darshan Nikita Thukral Ashish Vidyarthi music          = Hamsalekha cinematography = K. M. Vishnuvardhan editing        = Lakshman Reddy studio         = Rockline Productions released       =   runtime        = 152 minutes country        = India language       = Kannada budget         =
}} Kannada action Srikanth and Sneha (actress)|Sneha.

The film was released on 19 June 2009 across Karnataka cinema halls and got mixed response from critics and audience.  However the box-office results showed the film to be a success.  This was the first film in the combination of Rockline Venkatesh - Darshan.

==Cast== Darshan as Capt.Ram 
* Nikita Thukral as Asha
* Ashish Vidyarthi as Patil
* Rahul Dev
* Srinivasa Murthy
* Avinash
* Lokanath
* Sadhu Kokila
* Padma Vasanthi
* Rekha Kumar

==Soundtrack==
The music of the film was composed and lyrics written by Hamsalekha. 

{{Infobox album  
| Name        = Yodha
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Adithya Music
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Lip To Lip
| lyrics1  	= Hamsalekha
| extra1        = Jassie Gift, Chaitra H. G.
| length1       = 
| title2        = Namm India
| lyrics2 	 = Hamsalekha
| extra2        = K. S. Chithra
| length2       = 
| title3        = Surya Nodayya
| lyrics3       = Hamsalekha
| extra3  	= Suresh Peters, Anuradha Sriram
| length3       = 
| title4        = Mangalyam Tantunanena
| extra4        = Rajesh Krishnan, Nanditha
| lyrics4 	 =Hamsalekha
| length4       = 
| title5        = Namm India
| extra5        = Vijay Yesudas
| lyrics5       = Hamsalekha
| length5       = 
}}

==References==
 

==External source==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 

 