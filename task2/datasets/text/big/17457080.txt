Beheading the Chinese Prisoner
Beheading the Chinese Prisoner, also known as Beheading a Chinese Prisoner, was a 1900 silent film produced by Siegmund Lubin. The 42-second-long film, which was inspired by news reports of the Boxer Rebellion, was produced on the roof of the Lubin Studios building in Philadelphia. {{cite book
|last=Eckhardt
|first=Joseph P.
|title=The King of the Movies: Film Pioneer Siegmund Lubin
|year=1997 Fairleigh Dickinson University Press
|isbn=0-8386-3728-0
|pages=34
}}  {{cite book
|last=Fullerton
|first=John
|title=Celebrating 1895: The Centenary of Cinema
|year=1998
|publisher=Indiana University Press
|isbn=1-86462-015-3
|pages=280
}} 

It is considered an early example of "yellowface", and is featured in Arthur Dongs 2007 documentary film Hollywood Chinese.  

==Description of the film==

 

==Status== print of Beheading is kept in the George Eastman House International Museum of Photography and Film. {{cite web
|url=http://web.archive.org/web/20090315194057/http://www.eastmanhouse.org/inc/collections/b.php
|title=Film Stills Title List
|publisher=George Eastman House International Museum of Photography and Film (via archive.org
|accessdate=2011-08-18
}} 

==References==
 

 
 
 
 
 

 