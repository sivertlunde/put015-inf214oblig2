Riki-Oh: The Story of Ricky
 
 
{{Infobox film
| name           = Riki-Oh
| image          = Storyofricky.jpg
| caption        = Original Theatrical Poster Lam Nai-choi
| producer       = Chan Dung Chow
| writer         = Lam Nai-choi Tetsuya Saruwatari
| narrator       = Fan Siu-wong Fan Mei Sheng Ho Ka-kui Yukari Oshima Frankie Chen Golden Harvest
| released       =  
| runtime        = 91 min.
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = $2,147,778 HKD
}} Lam Nai-choi, Japanese manga Fan Siu-wong, Fan Mei-sheng (Siu-wongs real life father), Ho Ka-kui and Yukari Oshima.

Fan Siu-wong plays Ricky Ho Lik Wong (Lik Wong is the characters given name, but the subtitles use the   during Craig Kilborns time as the host.

It was alleged that a sequel titled Dint King, Inside King (aka, Story of Ricky 2 or Super Powerful Man) was released in Hong Kong in 2005, however, the plot does not follow the events that supposedly occur after Ricky breaks out of prison, and is set in the distant future as opposed to 2001 for the first film. The film was never released in the United States or in Europe, but is available on DVD (without English subtitles) through Panorama Entertainment. Oddly, and possibly due to rights issues, the film is built as a stand alone project despite casting Terry Fan Sui Wong in the title role, sporting the camouflage poncho seen in flashbacks and in the manga. Even the characters have different names (Rickys name is He Shen in this film).

== Plot ==
The plot closely follows the events depicted in the original Japanese comic and its anime adaptation, with some minor modifications in certain instances.
 Fan Siu-wong), a martial artist and former music student, is sentenced to 10 years in prison for manslaughter after killing a crime lord who was indirectly responsible for the death of his girlfriend Anne (Gloria Yip). Flashback scenes reveal that a group of thugs had captured her after she had witnessed their heroin deal. Anne was so scared in captivity that she ran upstairs and then leapt off the roof to her death.
 hangs himself.

The next day, Zorro attacks Ricky, but is quickly and horribly killed along with Samuel. Shortly after, a member of the fearsome Gang of Four named Oscar, the leader of the North Cell, suggests that Ricky should see the sadistic one-eyed Assistant Warden Dan. After Ricky confronts Dan, he suggests Oscar to kill Ricky. Outside the prison yard, Oscar and Ricky engage in a fight, which ends with Ricky defeating and killing Oscar.

Later on, Ricky soon discovers that the Gang of Four is growing illegal opium for profit. Rogan, leader of the West Cell, discovers that Ricky had set the poppy garden on fire and they both fight. As the fight goes on, Brandon, leader of the South Cell, throws his needles and ties Riki-Oh up with them, leaving him defenseless. Meanwhile, the guards report to Dan that the Warden is returning from his vacation in Hawaii, prompting Dan to raise the Zero Alarm. While the fight continues, Tarzan, leader of the East Cell, interferes, claiming that he wants to fight Ricky. As the Zero Alarm goes off, Ricky and the Gang of Four part ways for now.

The next day, the Warden and his spoiled, overweight son return from their vacation. Dan informs the Warden about the incidents during his absence, including the poppy garden which infuriates the Warden. Dan escorts the Warden to Rickys cell. As the Warden questions him, Tarzan bursts through the wall and fights Ricky once again, which ends with Ricky brutally defeating Tarzan. The Warden then activates a ceiling trap to crush Ricky. As Ricky struggles to stay alive, Tarzan regains consciousness and saves him from being crushed by the ceiling, at the cost of Tarzans life.

Later, the Warden orders the inmates to bury Ricky alive, which they reluctantly obey. The Warden claims if Ricky survives by staying underground for a week, he will consider freeing him from prison. As the week quickly passes by, Ricky survives, but the Warden does not free him. Later that night, Ricky is chained in his cell. A fellow inmate, named Freddy, brings him food so Ricky can regain his power. However, another inmate sees this and informs Dan about it and in response, Dan mortally wounds Freddy. Dan, dragging Freddys body with his hook, then opens Rickys cell to show the body to him. However, Dan and the other inmate find that Ricky has broken free from the chains, and Ricky kills the inmate and knocks out Dans remaining eye. As the guards rush to the scene, the inmates rebel and violently ambush Dan.

In the kitchen, Ricky, the prisoners, and Dan burst through the wall and the Warden shoots Dan with a gas-pressured bullet, causing him to inflate and violently explode. Rogan and Brandon then confront Ricky one last time, which ends with Ricky gravely injuring Rogan. Brandon, realizing Ricky is far too powerful for him, flees from the scene, but not before the Warden shoots and kills Brandon. The Warden, revealing that he too, is a martial artist, transforms into a grotesque creature and battles Ricky. The fight quickly ends with Ricky crippling and throwing the mutated Warden into a meat grinder. In the finale, the prisoners rebel once again and start to attack the guards. Ricky then breaks the prison wall, allowing all the prisoners and himself to go free.

==Cast== Fan Siu-wong as Ricky Ho
* Fan Mei-sheng as Assistant Warden Dan
* Ho Ka-kui as Warden 
* Yukari Oshima as Huang Chung/Rogan
* Tamba Tetsuro as Master Zhang
* Gloria Yip as Keiko / Ricky Hos Girlfriend
* Kwok Chun-fung as Lin Hung 
* Frankie Chin as Hai
* Lau Shung-fung as Prisoner
* Wong Kwok-leung as Prison Commanders Son	 	 
* Bill Lung Biu as prisoner
* Kong Long as Prison Guard
* Ling Chi-hung as Prisoner
* Cheung Yiu-sing as Prisoner
* Choi Kwok-ping as Prisoner
* Cheung Yuk-san as Prison Guard
* Chiu Chi-shing as Prisoner

===Notable scenes===
The film is notorious for its excessive use of graphic violence and gore, primarily due to the fact that the lead character is practically invincible, has superhuman strength and can virtually withstand all pain, which is partially explained by Ricky being a practitioner of a mystical kung-fu style known as Qigong. This leads to attempts by other characters to subdue him which end up being extremely gory and over-the-top.

Aside from the aforementioned head crushing and meat grinding scenes, most notable  is a fight scene between Ricky and a knife-wielding prisoner named Oscar. During the fight, Oscar throws powdered glass in Rickys eyes and then slashes Rickys right arm. Ricky seemingly finished, smashes a water pipe and cleans his eyes, then uses his teeth and left hand to tie the veins and tendons in his arm back together. Oscar then charges at Ricky, but Ricky dodges and smacks him in the back of the head, popping one of his eyes out, leaving it to be eaten by crows. Seeing himself at a disadvantage, Oscar attempts suicide by seppuku. However, when Ricky approaches Oscar to try and stop the suicide, Oscar grabs his own intestines and wraps them around Rickys neck in an attempt to strangle him, prompting the assistant warden to exclaim in the English dub: "Youve got a lot of guts, Oscar!". Ricky then punches Oscar in the face, with an X-ray image showing the front of his skull being shattered. The deceased Oscar crumples to the ground without so much as a visibly broken nose.

In another fight scene, Ricky punches and graphically breaks the Gang of Four member, Tarzans arm, then punches several of Tarzans fingers off. Finally, Ricky goes for the Coup de grâce and lands an uppercut with such force that Tarzans jaw is torn off.

Another scene includes the wardens graphic death which depicts Ricky throwing him into an industrial meat grinder. Ricky pushes the struggling warden through the grinder, until his whole body is shredded and only his head remains. In that scene, so much fake blood was used that Fan Siu-wong could not wash the blood off his skin for three days.

The films low budget shows in the scene where Rickys girlfriend Keiko jumps to her death. For this scene, an obvious mannequin wearing her clothes is thrown off the top of the building, landing with a dull thud and a slight bounce. A dummy is also used in some scenes where there is a close up.

== Box office and reception ==
  Category III rating (the Hong Kong equivalent of an NC-17) greatly inhibited its ability to make money at the box office. It was one of the first Hong Kong movies that used Category 3 film rating system for non erotic media. It grossed $2,147,778 HKD in Hong Kong.

The film, however, has received surprisingly positive reviews overseas. Michael Atkinson of The Village Voice called the film "a rather astonishing, starkly stylized blood flood set inside a privatized prison."  Kurt Ramschissel of Film Threat gave the film 5 stars, saying that "the violence comes fast and furious and is just as outrageous and over-the-top as Sam Raimi or Peter Jackson ever were."  Rotten Tomatoes currently has the film rated at 89% fresh on their Tomatometer. 

==Home video and online streaming releases== City Hunter.

The film was released on  s Instant Streaming service in mid-2012. The movie aired on Turner Classic Movies on 2 November 2012 and 14 April 2013 as part of TCM Underground.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 