Aelita
 
 
{{Infobox film
 | name = Aelita
 | image = Aelita.jpg
 | caption = A 1927 poster
 | director = Yakov Protazanov
 | producer =
 | writer = Fedor Ozep Vera Orlova
 | music =
 | cinematography = Emil Schünemann   Yuri Zhelyabuzhsky
 | editing =
 | released =  
 | runtime = 113 min.
 | country = Soviet Union Russian intertitles
 | budget =
 }} Soviet filmmaker Alexei Tolstoys novel of the same name. Mikhail Zharov and Igor Ilyinsky were cast in leading roles.

Though the main focus of the story is the daily lives of a small group of people during the post-war Soviet Union, the enduring importance of the film comes from its early science fiction elements. It primarily tells of a young man, Los ( , literally Elk), traveling to Mars in a rocket ship, where he leads a popular uprising against the ruling group of Elders, with the support of Queen Aelita who has fallen in love with him after watching him through a telescope.

==Influences==
  space travel, constructivist Martian sets and costumes designed by Aleksandra Ekster. Their influence can be seen in a number of later films, including the Flash Gordon (serial)|Flash Gordon serials and probably Fritz Langs Metropolis (1927 movie)|Metropolis and Woman in the Moon.
 Flight to Mars.

While very popular at first, the film later fell out of favor with the Soviet government and was thus very difficult to see until after the Cold War.
 

==DVD release==
The 2004 DVD from Ruscico runs 104 min. and has a musical score based on the music of Alexander Scriabin|Scriabin, Igor Stravinsky|Stravinsky, and Alexander Glazunov|Glazunov.

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 