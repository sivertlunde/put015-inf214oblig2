Domestic Disturbance
 
{{Infobox film
| name           = Domestic Disturbance
| image          = Ddmovie.jpg
| caption        = Theatrical release poster
| director       = Harold Becker
| producer       = Harold Becker Donald De Line Jonathan D. Krane
| writer         = Lewis Colick William S. Comanor Gary Drucker
| starring       = John Travolta Vince Vaughn Teri Polo Matt OLeary Steve Buscemi
| music          = Mark Mancina
| cinematography = Michael Seresin
| editing        = Peter Honess
| distributor    = Paramount Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $75 million  . Box Office Mojo. Retrieved 2010-11-23. 
| gross          = $54,249,294 
}}
Domestic Disturbance is a 2001 American psychological thriller film directed by Harold Becker and stars John Travolta and Vince Vaughn. It co-stars Teri Polo, Matt OLeary and Steve Buscemi.

==Plot==
Susan Morrison (Teri Polo), recently divorced from her husband Frank (John Travolta) who is a struggling ship builder, is getting married to a younger and wealthier Rick Barnes (Vince Vaughn). Danny (Matt OLeary), Susan and Franks twelve-year-old son, is clearly unhappy that his mother is remarrying. Susan asks Frank to allow Rick to go sailing with him and Danny, to help Danny bond with and accept Rick as a stepfather. 

After the wedding and a brief improvement in Dannys and Ricks relationship, Danny dislikes Rick once again. During a game of catch between the two, Rick clearly becomes agitated with Dannys ambivalent playing style and starts criticizing him harshly. The revelation that Susan and Rick are having a baby worsens the situation. After finding out about the baby, Danny stows away in Ricks Chevy Suburban, planning to drop off it en route and visit his father. But while Danny is inside, he sees Rick murdering mysterious stranger Ray Coleman (Steve Buscemi), who earlier attended the wedding unannounced, claiming to be an ex-business associate of Rick. 

Danny reports the murder to his father and to the local police. Rick, however, has managed to dispose of most of the evidence, and is widely considered a pillar of the local community as he invested his money in the area, whereas Danny has a history of lying and misdemeanors. Frank believes his son though, stemming from Ricks notable unease around Coleman at the ceremony. 

Frank does some investigating of his own and unearths Ricks criminal past, which now stands to put his son and ex-wife in risk. Frank learns that Ricks real name is Jack Parnell and that he is a criminal who was acquitted while his partners, which included Coleman, were convicted. Jack tries to kill Frank by setting his boathouse on fire, but Frank manages to escape. 
 electrocuting him. miscarries her child.

==Cast==
*John Travolta as Frank Morrison
*Vince Vaughn as Jack Parnell
*Teri Polo as Susan Barnes
*Matt OLeary as Danny Morrison
*Steve Buscemi as Ray Coleman
*Ruben Santiago-Hudson as Sgt. Edgar Stevens Chris Ellis as Detective Warren

==Production==
In April 2001, while shooting the film in Wilmington, North Carolina, actor Steve Buscemi was slashed and badly scarred on his face while intervening in a bar fight between his friend Vince Vaughn, screenwriter Scott Rosenberg and a local man, Timothy Fogerty, who allegedly instigated the brawl.  

==Release==
Paramount Pictures held the world premiere of Domestic Disturbance at the studio on October 30, 2001. The films stars were in attendance as well as many other guest celebrities.   The film was then officially released on November 2, 2001 in 2,910 theaters throughout the United States. It did not prove to be a financial success, grossing only $45,246,095 domestically. By the end of its run, the film was only able to gross $54 million worldwide from its $75 million budget. 

===Critical reaction===
The film was received poorly by critics, and gains a 23% rating on Rotten Tomatoes based on 98 reviews.

Roger Ebert awarded it a meager one-and-a-half stars (out of a possible four),  reciting an anecdote about how the Chicago film critics had been shown the wrong last developing tank|reel. He saw the correct one the following Monday, and scathingly said of it in his review: "The earlier reel was lacking the final music. Music is the last thing wrong with that reel."

===Accolades===
Matt OLeary was nominated for a Young Artist Award, for Best Performance in a Feature Film - Supporting Young Actor. However, star John Travolta was nominated for a Razzie for Worst Actor (also for Swordfish (film)|Swordfish). Vaughn and Travolta have also worked in Be Cool together.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 