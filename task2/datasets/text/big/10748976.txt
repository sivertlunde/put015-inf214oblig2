Bellissima (film)
{{Infobox film
| name           = Bellissima
| image          = Bellissima poster.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = Luchino Visconti
| producer       =
| writer         = Cesare Zavattini Suso Cecchi dAmico Francesco Rosi Luchino Visconti
| narrator       =
| starring       = Anna Magnani Walter Chiari Tina Apicella Gastone Renzelli Tecla Scarano Arturo Bragaglia Alessandro Blasetti
| music          = Franco Mannino, inspired by Donizettis Lelisir damore
| cinematography = Piero Portalupi
| editing        = Mario Serandrei
| distributor    =
| released       = 27 December 1951
| runtime        = 115 minutes {release version}
| country        = Italy
| language       = Italian
| budget         =
}}
 Italian film director Luchino Visconti. The film, which is a satire of the film industry, was shot at the Cinecittà studios. Alessandro Blasetti, a contemporary film director, appears as himself.

==Plot==
Bellissima centers on a working-class mother in Rome, Maddalena (Anna Magnani), who drags her young daughter (Tina Apicella) to Cinecittà for the "Prettiest Girl in Rome" contest. Maddalena is a stage mother who loves movies and whose efforts to promote her daughter grow increasingly frenzied.

==Cast==
*Anna Magnani - Maddalena Cecconi
*Walter Chiari - Alberto Annovazzi
*Tina Apicella - Maria Cecconi
*Gastone Renzelli - Spartaco Cecconi
*Tecla Scarano - Tilde Spernanzoni
*Lola Braccini - the photographers wife
*Anton Giulio Bragaglia - the photographer
*Nora Ricci - the laundry girl
*Vittorina Benvenuti
*Linda Sini - Mimmetta
*Teresa Battaggi - a mother
*Gisella Monaldi - a concierge
*Amalia Pellegrini

==Awards==
*Italian National Syndicate of Film Journalists Nastro dArgento
**Winner: Best Actress (Migliore Attrice)- Anna Magnani

== External links ==
*  
* Pauline Kaels  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 