Shooters (2002 film)
{{Infobox film
| name = Shooters
| image = Shooters.JPG
| caption = DVD cover
| director = Colin Teague Glenn Durfort
| producer = Margery Bone Will Bone Gary Young
| narrator = Louis Dempsey
| starring = Adrian Dunbar Andrew Howard Louis Dempsey Gerard Butler Melanie Lynskey
| music = Kemal Ultanur
| cinematography = Tom Erisman
| editing = Kevin Whelan
| distributor = Media Cooperation One (MC-One)
| released =      
| runtime = 95 minutes
| country = United Kingdom United States Netherlands
| language = English
}} Gary Young. Howard and Dempsey also play the films two leading roles.

==Plot synopsis==
After six years behind bars, Gilly (Dempsey) wants to settle down and live a quiet life.  But it is not to be.  He is released from prison only to find that his crime partner J (Andrew Howard|Howard) has invested all their money in a massive array of submachine guns, forcing Gilly back into a life of crime.  Their last deal goes down in seven days, but the way things are going, Gillys not sure he can hold out that long.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| Adrian Dunbar
| Max Bell 
|-
| Andrew Howard
| J "Justin" 
|-
| Louis Dempsey
|  Gilly 
|-
| Gerard Butler
| Jackie Junior 
|- Jason Hughes
| Charlie Franklin 
|-
| Matthew Rhys
| Eddie
|-
| Ioan Gruffudd
| Freddy Guns
|-
| Jamie Sweeney
| Skip
|-
| Melanie Lynskey
| Marie 
|-
| Emma Fielding
| Detective Inspector Sarah Pryce
|- David Kennedy
| Sergeant Webb
|- Joseph Swash
| Boy
|-
| Ranjit Krishnamma
| Pac
|-
| Nitin Ganatra
| Ajay
|-
| Walter Roberts
| Jason
|-
| Ted Nygh
| Mickey
|-
| Mike Martin
| Vic
|-
| Glenn Durfort
| Glenn
|-
| Treva Etienne
| Benny
|-
|}

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 