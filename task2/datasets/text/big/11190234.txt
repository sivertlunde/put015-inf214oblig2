Mumbai Meri Jaan
{{Infobox Film
| name           = Mumba Meri Jaan
| image          = MumbaiMeriJaan.jpg
| image_size     =
| caption        = poster
| director       = Nishikanth Kamath
| producer       = Ronnie Screwvala
| writer         = Yogesh Vinayak Joshi  Upendra Sidhaye
| narrator       = Irfan Khan
| music          =
| cinematography = Sanjay Jadhav
| editing        = Amit Pawar
| distributor    = UTV Motion Pictures
| released       = 22 August 2008
| runtime        = 119 minutes
| country        = India Hindi Urdu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Mumbai Meri Jaan ( : Mumbai My Life) is a 2008 Hindi film directed by Nishikanth Kamath and produced by Ronnie Screwvala. It stars R. Madhavan, Soha Ali Khan, Irrfan Khan, Paresh Rawal and Kay Kay Menon. It deals with the aftermath of the 11 July 2006 Mumbai train bombings, where 209 people lost their lives and over 700 were injured.  It won multiple Filmfare Awards.

==Plot==
Mumbai Meri Jaan tells the story of five people whose lives are affected by the 2006 Mumbai train bombings. 

Rupali Joshi (Soha Ali Khan) is a successful reporter who is getting married in two months. 
Nikhil Agrawal (R. Madhavan) is an environmentally conscious executive who rides the train to work every day and is expecting his first child. Suresh (Kay Kay Menon) is a struggling computer tech who spends his time loafing at a local cafe and criticizing Muslims. Meanwhile, Sunil Kadam (Vijay Maurya) struggles with the corruption and inefficiency of the Mumbai police force and his boss, Tukaram Patil (Paresh Rawal), who is nearing retirement. 
 Irfan Khan) who begins calling in fake bomb scares at malls to relieve his feelings. After an elderly man suffers a heart attack while the police are evacuating one mall, Thomas feels guilty and decides to stop.

Rupali, who rushed to the scene of the bombings to cover the story, is devastated when she discovers that her fiancé died in the blasts. Her grief is augmented when the news channel she works for tries to exploit her story for ratings. Meanwhile, Suresh pursues a Muslim that he suspects of being a terrorist. However, after Patil stops him and lectures him on communal harmony, Suresh befriends the man. 

After Nikhils wife goes into labour, he is forced to take the train to get to the hospital. Mumbai stops for two minutes while the city observes a moment of silence for those killed in the bombings. Patil finally retires from the police force and Kadam forgives him for his corrupt actions. Nikhil overcomes his fear of trains and Thomas gives a rose to the elderly man whose heart attack he caused.

==Cast== Madhavan as Nikhil Agarwal Irfan Khan as Thomas
* Soha Ali Khan as Rupali Joshi
* Paresh Rawal as Tukaram Patil
* Kay Kay Menon as Suresh
* Aanand Goradiya
* Vijay Maurya as Sunil Kadam
* Vibhavari Deshpande as Archana Kadam

==Awards==
*Filmfare Critics Award for Best Movie - Winner 
*Filmfare Best Screenplay Award - Winner 
*Filmfare Best Editing Award - Winner
*National Film Award for Best Special Effects for Govardhan (Tata Elxsi) - Winner
*Best Feature Film at the New Generation Cinema Lyon Film Festival - Winner 
*Best Screenplay award at the 2009 Asia Pacific Screen Awards  - Nomination

==Music==
The end titles are accompanied by the song "Aye Dil Hain Mushkil" (also known as "Bombay Meri Jaan") from the 1956 film C.I.D. (1956 film)|C.I.D., performed by Mohammed Rafi and Geeta Dutt.

==See also==
* 11 July 2006 Mumbai train bombings &mdash; the attacks on which the movie is based.
* Terrorism in Mumbai
* Aamir (film)
* A Wednesday
* Black Friday (2004 film)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 