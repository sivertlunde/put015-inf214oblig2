Sampoorna Ramayanam (1971 film)
{{Infobox film
| name           = Sampoorna Ramayanam
| image          =
| image_size     =
| caption        = Bapu
| producer       =  Nidamarthi padmakshi
| writer         = Valmiki Mullapudi Venkata Ramana
| narrator       = Gummadi  Mikkilineni Jamuna Jamuna Chhaya Devi  Hemalatha Pandari Bai
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| studio         = Durga Cinetone
| distributor    =
| released       = 1971
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Sampoorna Ramayanam is a 1971 Telugu mythological film directed by Bapu (artist)|Bapu. It is based on the Valmiki Ramayanam. It is a commercial hit film with artistic picturisation and good songs particularly "Ramaya Tandri".

==Plot==
The story is the complete Ramayana from the birth of Lord Rama to his Pattabhisheka after completing his exile.

==Cast==
* Shobhan Babu as Rama
* Chandrakala as Sita Gummadi as Dasaratha
* Chittor V. Nagaiah as Vasishtha
* Kaikala Satyanarayana
* S.V.Ranga Rao as Ravana Mikkilineni
* Jamuna as Kaika
* Chhaya Devi as Mandhara
* Hemalatha
* Pandari Bai

==Soundtrack==
* "Adigo Ramayya"
* "Choosindhi Ninnu Choosindhi" (Singer: P. Susheela)
* "Evadu Ninnu Minchu Vaadu"
* "Oorike Kolanu Neeru Uliki Uliki Padutundhi" (Lyrics: Devulapalli Krishnasastri; Singer: P. Susheela)
* "Rama Laali Meghasyama Laali" (Singer: P. Susheela)
* "Ramaya Tandri O Ramaya Tandri.. Maa Nomulanni Pandinayi Ramaya Tandri" (Lyrics:  )
* "Vana Jallu Kurisindhi Lera Lera Ollu Jhallu Jhallandi Rara Rara" (Singer: Jikki Krishnaveni)
* "Vedalenu Kodandapani" (three parts) (Singers: P. B. Srinivas and P. Susheela)

== DVD ==
The DVD version of Sampoorna Ramayanam was released By KAD Entertainment. It is the first Indian Movie DVD to be released with Multiple Indian soundtracks, the DVD has soundtracks in Hindi, Telugu, Tamil, Kannada and subtitles in various languages.

==Box-office==
The film ran for more than 100 days in 10 theaters in Andhra Pradesh. 

==References==
 

==External links==
*  

 
 
 
 
 
 


 