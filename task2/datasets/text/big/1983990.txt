Knight Rider 2010
 
{{Infobox film
| name           = Knight Rider 2010
| image          = Knight Rider 2010.jpg
| alt            =  
| caption        = 
| director       = Sam Pillsbury
| producer       = Alex Beaton
| writer         = John Leekley
| starring       = Richard Joseph Paul
| music          = Tim Truman
| cinematography = James Bartle
| editing        = Skip Schoolnik
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Action Pack Knight Rider.

== Differences between Knight Rider and Knight Rider 2010 ==
* The car is a custom "Ford Mustang" built on an MN12 Ford Thunderbird chassis; its grunge style very different from the sleek Pontiac cars that were the two incarnations of KITT, whose AI would have been removed and replaced with K.D. (Hannah Tyree)
* There is nothing in common with Knight Riders basic concept, except the title, a talking car, and the "one man can make a difference" concept.
* Notably, early drafts of the script were far closer to the series even including KITT (who would have been a female); however, the makers believed at that time there would be no high-tech cars.

== Plot ==
In a Mad Max style future, Jake McQueen is the ultimate smuggler, smuggling in Mexicans for money to survive, only for his smuggling to come to a halt when he is busted by his brother while getting his truck repaired.

However, what he doesnt know is that he is under observation by Jared, the crippled head of Chrysalis Corporation, who sends one of his most valued employees, Hannah Tyree, to bring him in to work for them as part of their video games division.

Jake initially is skeptical about the idea of working with Hannah, and is scared away when she admits that she accidentally downloaded herself onto PRISM, a crystalline solid-state memory unit for her computer, once, due to an unexpected side-effect.

Jake is then hunted down after Jared has his data, and eventually finds his way back home, only to find his father near death. Acquiring a junked Mustang, and a special engine his father had kept in trust, he goes to find a way to stop Chrysalis.

While pursuing a lead, he ends up shot, and is witness to Hannahs apparent death, only to find she was trapped in her PRISM. Going into battle against Jared, with Hannah as his cars new AI, he eventually destroys him when he discovers the one side effect of Jareds life support: that it is slowly killing the person it protects.

Now, Jake and Hannah travel the world of the future, fighting for justice in a lawless desert that is forgotten by the world.

== See also ==
* Knight Rider (franchise)|Knight Rider
* Knight Rider 2000

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 