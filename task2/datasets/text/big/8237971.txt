1. April 2000
{{Infobox film
| name           = 1. April 2000
| image          = 
| alt            =  
| caption        = 
| director       = Wolfgang Liebeneiner
| producer       = Karl Ehrlich
| writer         = Rudolf Brunngraber Ernst Marboe
| starring       = Hilde Krahl Josef Meinrad
| music          = Josef Fiedler Alois Melichar Robert Stolz
| cinematography = Sepp Ketterer Karl Löb Fritz Arno Wagner
| editing        = Henny Brünsch-Tauschinsky
| studio         = 
| distributor    = Herzog-Filmverleih Lewis Productions Inc.
| released       =  
| runtime        = 105 minutes
| country        = Austria
| language       = German
| budget         = 
| gross          = 
}}
 Allied Occupation Allied powers, as established following the defeat of Nazi Germany in World War II (as it was when the film was made). The film was entered into the 1953 Cannes Film Festival.   

==Plot summary== Allies about the independence of Austria, the Austrian prime minister prompts his fellow countrymen to shred their four-language identity cards, which have been issued by the Allies, thus sending a clear signal to the world. Thereupon, Austria is charged for breaking the "world peace" at the fictitious "world court". The implicated message is clear: in the same manner as Austria was, in Austrias eyes, falsely indicted for breaking the world peace (1914 and 1939), they are now being indicted again in 2000.
 Viennese wine, the Viennese waltz, the mountains, the classic bands, etc. Despite the presented evidence, Austria will be found guilty. Just before the conviction is made, the Moscow Declaration of 1943 is discovered. The declaration clearly states that Austria is to be freed, which happens at the end of the film. Back in the current time of 1952, and in reality, it is bemoaned that those actions and the independence of Austria will not take place until the year 2000.

==Cast==
{| class="wikitable"
! Actor
! Role
|- 
| Hilde Krahl || President of the Global Union
|-
| Josef Meinrad || Prime Minister of Austria
|-
| Waltraut Haas || Mitzi
|-
| Judith Holzmeister || Ina Equiquiza
|-
| Elisabeth Stemberger || Sekretärin
|-
| Ulrich Bettac || Moderator Robinson
|-
| Karl Ehmann || Cabinet Chief
|-
| Peter Gerhard || Hieronymus Gallup
|-
| Curd Jürgens || Capitano Herakles
|-
| Robert Michal || Wei Yao Chee
|-
| Heinz Moog || Hajji Halef Omar
|-
| Guido Wieland || Alessandro Bibalini
|-
| Paul Hörbiger || Augustin
|- Hans Moser || Composer
|-
| Pepi Glöckner-Kramer ||
|-
| Martha Marbo ||
|-
| Eva Payrer ||
|-
| Erika Pirschl ||
|-
| Erna Schickl ||
|-
| Marianne Schönauer ||
|-
| Alma Seidler || Reporterin
|-
| Anneliese Stöckl-Eberhard ||
|-
| Hansi Stork ||
|-
| Ingeborg Wieser || Alessandro Vitalini
|-
| Karl Bachmann ||
|-
| Theodor Danegger || Russ. Hochkommissar 
|-
| Karl Eidlitz ||
|- Hans Frank ||
|-
| Erik Frey || Prinz Eugen
|-
| Harry Fuß || Franzl
|-
| Hugo Gottschlich ||
|-
| Fred Hennings || Deutscher Kaiser
|-
| Franz Herterich || Amerik. Hochkommissar
|-
| Hans Holt ||
|-
| Fritz Imhoff ||
|-
| Fred Liewehr ||
|-
| Heribert Meisel ||
|-
| Alfred Neugebauer || Finanzminister
|-
| Toni Nießner ||
|- Hans Richter || Reporter
|-
| Leopold Rudolf ||
|-
| Stefan Skodler ||
|-
| Ernst Stankovski ||
|-
| Otto Treßler || Engl. Hochkommissar
|-
| Hans Ziegler || Franz. Hochkommissar
|-
| Kurt Bülau ||
|-
| Rita Gallos ||
|-
| Edith Prager ||
|-
| Helmut Qualtinger ||
|-
| Gerhard Riedmann || Reitender Bote (uncredited)
|-
| Die Wiener Sängerknaben || Singers
|}

== References ==
 
* Wingrove, David. Science Fiction Film Source Book (Longman Group Limited, 1985)

==External links==
*  

 

 
 
 
 
 
 
 