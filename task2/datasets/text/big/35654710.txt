Plan B (2009 film)
{{Infobox film
| name           = Plan B
| image          = Plan-b-by-marco-berger.jpg
| caption        = Theatrical release poster
| director       = Marco Berger
| producer       = Martín Cuinat
| writer         = Marco Berger
| starring       = Manuel Vignau  Lucas Ferraro  Mercedes Quinteros
| music          = Pedro Irusta 
| cinematography = Tomas Perez Silva
| editing        = Marco Berger
| studio         = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}} Argentine director Marco Berger.

==Plot==
Bruno (Manuel Vignau) is dumped by his girlfriend Laura (Mercedes Quinteros); so Brunos mind plans a cold, sweet vengeance on Lauras new boyfriend Pablo. Bruno wants to manipulate Pablo to dump Laura by sleeping with her. When that plan doesnt seem to be working, Bruno conceives of "Plan B": seducing Pablo and stealing him away from Laura himself. 

Bruno discusses his ideas and his progress with his friend Victor (Damián Canduci), a sort of partner in crime and a sounding board for Bruno. As according to his plan, Bruno begins a clandestine "relationship" with Pablo. 
 sexualities in a new light. By the end of the film, they slowly fall for each other.

==Cast==
*Manuel Vignau as Bruno
*Lucas Ferraro as Pablo
*Mercedes Quinteros as Laura
*Damián Canduci as Victor
*Ana Lucia Antony as Ana
*Carolina Stegmayer as Verónica
*Antonia De Michelis as Madre Victor
*Ariel Nuñez Di Croce as Javier

==Screenings==
The film was an official selection at the Buenos Aires International Festival of Independent Cinema (BAFICI), at The Rome International Film Festival, BFI London Film Festival, and films festivals in Havana, Palm Springs, Bilbao, Toulouse, Amsterdam and Melbourne. 

==See also==
*Ausente (film)|Ausente (film)
*Hawaii (2013 film)|Hawaii (2013 film)

==References==
 

==External links==
* 

 
 
 
 
 