No. 1 Snehatheeram Banglore North
{{Infobox film
| name           = No. 1 Snehatheeram Bangalore North
| image          = No.1.Snehatheeram-Banglore North.jpg
| director       = Sathyan Anthikkad Fazil
| Fazil
| Innocent Thilakan  Oduvil Unnikrishnan Sukumari
| music          = Jerry Amaldev
| cinematography = Vipin Mohan
| editing        = K. Rajagopal
| distributor    = Swargachithra Release
| released       =  
| country        = India
| language       = Malayalam
}}
No. 1 Snehatheeram Bangalore North is a 1995 Malayalam film written by Fazil (director)|Fazil, directed by Sathyan Anthikkad, and produced by Fazil (director)|Fazil, starring Mammootty and Priya Raman supported by Innocent(actor)|Innocent, Thilakan, Oduvil Unnikrishnan and Sukumari.

==Plot==
Sudhi and Anu are siblings, studying in a boarding school in Ooty. Their parents are separated and they never saw their mother, whom they always long to see. When they come on vacation to their father’s (Vijay Bhaskar - Mammootty) home, they make an attempt to bring back their mother by going on a fast for an indefinite time. Even though this attempt fails, Vijay is forced to give a brief description of the physical appearance of their mother. They also know that their mother lives somewhere in Bangalore. 

When they hear the next day about their father’s business trip to Bangalore, they forcefully accompany him. During their stay in Bangalore, they successfully trick the caretakers and go on a search for their mother. When their father comes and finds that the children are missing, he gets a phone call from the children saying that they found their mother and they are at her house (House No. 1, Love shore, Bangalore North). He goes there to find the children with a rich lady named Hema (Priya Raman), who was mistaken by the kids as their mother due to the similarities with the description of their mother given earlier. Hema, on the other hand pretended to be their mother so as to make an attempt to bring them to their original mother. Considering the children, they agree to continue with the drama till the end of the vacation, on the condition that Vijay solve the problems with his wife and give back the children their mother. She also asks him not to enquire more about her, which he agrees. 

Things get worse when on the journey back to school on the last day of the vacation; they learn that the vacation is extended for one more weak owing to the extreme weather in Ooty. Hema disagrees to continue with the drama and leaves the family, leading the children to believe that their parents are on a fight again. The children follow Hema pleading not to fight. Shattered at the children’s request she continued acting as their mother. Things get twisted when Anu falls ill on the same night and Vijay watches the love and care that Hema have for his children, leading him to propose to marry him. Hema on the belief that she was separating the children from their real mother; gets irritated at the proposal and rejects him. The conversation leads Vijay to reveal his story. He reveals that he is not the father of the children and that their parents are both dead. The children belonged to his only sister Sindhu (Chippy (actress)). He pretended to be their father and cooked up a story of a separated mother so as not to reveal the truth about the death of their parents. Hema becomes stunned and reveals her story. She ails from a poor family. She have a mother, an elder sister and a brother, all three of them suffering from various diseases. She gave up the happiness in her life for them. She comes to this house of a friend when she feels depressed and needs a change. 

She starts to feel the same for Vijay but still does not accept the proposal. That night she thinks a lot and decides to accept the proposal. The next morning she leaves to her home to ask for permission to marry, which she receives. When she arrives, she finds that Vijay revealed the truth to the children and to his surprise; the children were not upset at the news. The children still consider Vijay as their father. Vijay leaves with the children leaving the heartbroken Hema alone in the house. The next morning, when the children leave for the school, they find Hema waiting for them on the way. She ask for permission to finish what she started and accompany them to the school. The children expecting this question from her, said that she can accompany only if she stays with them for the rest of their life. They accept her as their mother and their father’s wife. The film ends with them driving together to school.

==Cast==
*Mammootty - Vijayabhaskar
*Priya Raman - Hema
*Sukumari - School principal
*Thilakan - Vijayabhaskarss father Chippy -Sindhu, Sudhi/Anus mother and Vijayas sister Innocent - Kuriakkos
*Kaviyoor Ponnamma - Vijayabhaskars mother Kalpana - Metlda, School Teacher
*Oduvil Unnikrishnan - Shivaraman
*Sankaradi - Danielkutty
*Kunjandi Santhakumari

==Soundtrack==
{{Infobox album
| Name        = No. 1 Snehatheeram Bangalore North
| Type        = Soundtrack
| Artist      = Jerry Amaldev
| Language = Malayalam
| Genre       = Film
|}}

The film features songs composed by Jerry Amaldev and written by Gireesh Puthenchery.

{| class="wikitable"
|-
! No. !! Song Title !! Singer
|-
| 1 || Appom Chuttu || Sujatha Mohan, chorus
|-
| 2 || Kokkurasumen || KJ Yesudas, KS Chithra, Chandrasekhar
|-
| 3 || Mele mele ||  KJ Yesudas
|-
| 4 || Mele Mele (Bit) || S Janaki
|-
| 5 || Mele Mele (F) || S Janaki
|-
| 6 || Minnum Minna Minni ||  KS Chithra
|-
| 7 || Ponnambili ||  K J Yesudas
|-
| 8 || Thilangum Thinkale ||  K J Yesudas
|}

==External links==
 

 

 
 
 
 
 