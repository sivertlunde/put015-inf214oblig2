Edison and Leo
{{Infobox film
| image=
| caption=
| director=Neil Burns
| producer= Dean English Karen Powell David Valleau
| screenplay=Daegan Fryklind George Toles
| story=George Toles Gregory Smith Carly Pope
| music= Michael Richard Plowman
| cinematography = Brian Johnson
| editing= Joseph Fitzpatrick
| studio=Perfect Circle Productions
| distributor= TVA Films
| released= 
| runtime=79 minutes
| country= Canada
| language=English French
| budget=
| gross=
}}
Edison and Leo is a 2008 stop-motion animated feature film produced by Perfect Circle Production and Infinity Entertainment. It is Canadas first stop-motion animated feature.

==Plot==
The film is about the relationship between crazy scientist George Edison and his son Leo. After being electrocuted as a child, Leo is no longer able to touch people without electrocuting them.

==Voice cast==

*Powers Boothe ...  George T. Edison
*Jay Brazeau ...  Captain Samuel Edison
*Ben Cotton ...  Faraday Edison
*Aiden Drummond ...  Young Faraday
*Brian Drummond ...  Braken-Carl
*Colin Murdock ... Gokul
*Quinn Lord ...  Young Leo
*Venus Terzo ...  Lotte
*Scott McNeil ... Batchelor
*Ashley Michaels ...  Helka
*Carly Pope ...  Zella
*Jacqueline Samuda ...  Toni Gregory Smith ...  Leonardo "Leo" Edison

==Production==

It is one of the first stop-motion feature films to not target the family audience, and is being distributed in Canada by TVA Film. Technical director Jean-Luc Dinsdale used Automatic Duck to arrange the film. 

===Premier===

The films world premier was on September 4, 2008, as the Opening Film of the Canada First section at the Toronto International Film Festival. Toronto International Film Festival Programmer Jesse Wente said about it: "The animation is so gorgeous it almost made me vomit - in a good way - when you see the scene you’ll know what I mean"
. 

==See also==
*List of stop-motion films

==References==
 

==External links==
*  

 
 
 
 
 

 