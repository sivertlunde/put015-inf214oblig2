Dragon Age: Dawn of the Seeker
 
{{Infobox film
| name           = Dragon Age: Dawn of the Seeker
| image          = Dragon Age Dawn of the Seeker cover.jpg
| alt            = 
| caption        = 
| director       = Fumihiko Sori
| producer       = Justin Cook Makoto Hirano Carly Hunter Tomohiko Iwase Lindsey Newman
| writer         = Jeffrey Scott
| starring       = Colleen Clinkenbeard J. Michael Tatum
| music          = Tetsuya Takahashi
| cinematography = 
| editing        =  FUNimation Entertainment Oxybot T.O Entertainment 
| distributor    = T.O Entertainment
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese English
| budget         = 
| gross          = 
}}

  is a 2012 Japanese CG anime film directed by Fumihiko Sori and based on the video game series Dragon Age.  

==Summary==
Dragon Age: Dawn of the Seeker tells the story of a Chantry Seeker Knight named Cassandra Pentaghast|Cassandra. She, through the last acts of her mentor, learns of a plot by blood mages to bring down the entire Chantry. The blood mages have discovered a girl that has the ability to control dragons and will use her to usher in a new era of magic domination. Its up to Cassandra, one of the last dragon hunters, to stop the blood mages and save the Chantry.

==Cast==
{| class="wikitable"
|-
!Character
!Japanese Voice Actor 
!English Voice Actor
|-
!Cassandra Pentaghast Chiaki Kuriyama Colleen Clinkenbeard
|-
!Regalyan DMarcall
|Shōsuke Tanihara
|J. Michael Tatum
|-
!Frenic Hiroshi Iwasaki Chuck Huber
|-
!Grand Cleric Kaya Matsutani Brina Palencia
|-
!Byron Tetsuo Komura John Swasey
|-
!High Seeker Takaya Hashi Robert Bruce R Bruce Elliott
|-
!Knight Commander Gackt
|Christopher Sabat
|-
!Divine Gara Takashima Pam Dougherty
|-
!Lazarro Hiroomi Sugino Mike McFarland
|-
!First Enchanter Kazuaki Itō Kenny Green
|-
!Alte Hajime Iijima Joel McDonald
|-
!Haydi Go Shinomiya Ian Sinclair Ian Sinclair
|-
!Anthony Pentaghast Eiji Miyashita John Burgmeier
|-
!Avexis Tomoko Nakamura Monica Rial
|-
!Revered Mother Junko Kitanishi Luci Christian
|}

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 


 