Whore Angels
{{Infobox film
| name = Whore Hospital
| image = Whore Angels.jpg
| image_size = 160px
| caption = Theatrical poster for Whore Hospital (2000)
| director = Mototsugu Watanabe   
| producer = 
| writer = Takao Nakano
| narrator = 
| starring = Shiori Kuroda Nao Nishifuji Shōko Kudō
| music = 
| cinematography = Masahide Iioka
| editing = Shōji Sakai
| studio = Shintōhō
| distributor = Shintōhō
| released = December 29, 2000
| runtime = 62 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2000 Japanese pink film fantasy directed by Mototsugu Watanabe. It won the Silver Prize at the Pink Grand Prix ceremony.

==Synopsis==
Komasa is a female drifter works at a pink salon—a shop specializing in fellatio—called "Hot Lips". On the way to work one day she rescues a female stranger from an attack by an evil-looking man. Monroe, the stranger, kisses Komasa, whose wounds received during the fight are then healed. Monroe takes up employment at the pink salon, where it is learned that her supernatural powers cure every man on whom she performs fellatio of all ailments. Monroes miraculous healing powers lead to the shops increased popularity. Eventually it is discovered that Monroe is actually an angel and that the man Komasa rescued her from is the devil, bent on subjugating the living. Komasa vows to fight to save Monroe and life on earth.    

==Cast==
* Shiori Kuroda ( )  
* Nao Nishifuji ( )
* Shōko Kudō ( )
* Mariko Naga ( )
* Rira Mizuno ( )
* Shin Yamazaki ( )
* Jimmy Tsuchida ( )
* Shūetsu Tōkaiki ( )

==Release history and awards==
Whore Angels, or Pink Salon Hospital 3: No-Pants Exam Room in its original Japanese theatrical release, is the third entry in Shintōhōs popular Pink Salon Hospital or Whore Hospital series.  Director Sachi Hamano inaugurated the series in 1997 with  . The series continued with director Sachio Kitazawas   (1998).   Whore Angels, the English title for the third release in the series, was followed in 2001 with Sachi Hamanos return to direct  . 

 
Mototsugu Watanabe is a pink film director with a cult following known for his eclectic, wild scenarios with stylistic influences from the pop-culture world of the  , Whore Angels won the Silver Prize for Best Film of the year.  

After its initial theatrical run, Shintōhō has re-released the film theatrically twice: on September 10, 2004 as No Pants Angel: Underwear Heaven and on January 9, 2009 as No Pants Night Life: Suck!.   It was released in VHS home video format in Japan on August 24, 2001 as part of Interfilms "R" Series under the title New Nurse Buddhist Cross Festival: Five Secret Consultation Techniques.     The U.S.-based company Pink Eiga has plans to release the film on DVD for English-speaking audiences under the title Whore Angels. 

==Bibliography==

===English===
*  
*  

===Japanese===
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 

 

 
 
 
 
 