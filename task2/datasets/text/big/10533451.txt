The Butcher Boy (1997 film)
 
{{Infobox film
| name            = The Butcher Boy
| image           = Butcher boy poster.jpg
| alt             = 
| caption         = Theatrical release poster
| director        = Neil Jordan Redmond Morris Stephen Woolley Patrick McCabe Neil Jordan
| based on        =  
| starring        = Stephen Rea Fiona Shaw introducing Eamonn Owens
| music           = Elliot Goldenthal
| cinematography  = Adrian Biddle
| editing         = Tony Lawson
| studio          = The Geffen Film Company
| distributor     = Warner Bros.
| released        =   
| runtime         = 110 minutes
| country         = Ireland United States
| language        = English
| budget          = 
| gross           = $1,963,654 
}} tragicomic drama adapted to Patrick McCabe 1992 novel of the same name.   

Set in the early 1960s, The Butcher Boy is about Francie Brady (Eamonn Owens), a 12-year-old boy who retreats into a violent fantasy world to escape the reality of his dysfunctional family; as his circumstances worsen, his sanity deteriorates and he begins acting out, with increasing brutality. The film won the Silver Bear for Best Director at the 48th Berlin International Film Festival in 1998 and a Special Mention for Owens "astonishing lead". It also won the European Film Award for Best Cinematographer for Adrian Biddle. The Butcher Boy is Neil Jordans tenth feature film and Geffen Pictures final production.

==Plot== nuclear annihilation." molested by Virgin Mary (Sinéad OConnor). He returns home to find Joe has outgrown him and befriended Phillip Nugent. Before long, his father has drunk himself to death. Faced with being left completely alone in the world, Francie loses his grip on reality and lashes out with uncontrollable brutality, which shocks his provincial hometown.   

==Cast==
* Eamonn Owens as Francie Brady
* Stephen Rea as Benny Brady (Da)
* Fiona Shaw as Mrs. Nugent
* Andrew Fullerton as Phillip Nugent
* Aisling OSullivan as Annie Brady (Ma)
* Alan Boyle as Joe Purcell
* Seán McGinley as Sergeant
* Ian Hart as Uncle Alo
* Brendan Gleeson as Father Bubbles
* Milo OShea as Father Sullivan Virgin Mary / Colleen
* Gina Moxley as Mary John Kavanagh as Doctor Boyd

==Production==
This was the final film produced by Geffen Pictures, which distributed its films through Warner Bros. Geffen Pictures would be sold to Universal Studios years later.
 adaptation is mostly faithful to the novel, but there are some differences, the principal change being the ending. In the book, Francie is not seen to leave prison, and attempts to forge a friendship with an inmate similar to the one he had with Joe. In the film, a much older Francie is released from prison at the end to be brought to a halfway house. He picks a snowdrop, echoing the opening of the film.
 Casting the child to play Francie was difficult. With no previous filming experiences, Eamonn Owens and Alan Boyle (who played Francies best friend, Joe) were found at the local school in Killeshandra in County Cavan where casting assistant Maureen Hughes went to visit her uncle. Owens younger brother Ciaran was also cast. Jordan cast OConnor  because "she looks like the Virgin Mary." 

==Adaptation== Patrick McCabes The Butcher Boy was deemed unattainable in a film.   During the screenwriting process, author McCabe wrote two drafts that digressed from the original novel, like "planets within planets within planets" according to Neil Jordan, consequently, Jordan wrote the third draft that was more faithful to the novel. 
 abuse and Beckettian ambiguity." However, he argues that Jordan "brings a tenderness and sweetness" to the otherwise unforgiving subject matter. 

==Reception==
The reception of the film has been generally good. Review aggregator Rotten Tomatoes show a 79% rating and an average rating of 7.5/10.  Andrew OHehir at Salon Entertainment says "Neil Jordans sweetly tragicomic movie" has "elaborate fantasy sequences   feel like irrelevant amusements." He also praises the film as "a compelling exploration of the permeable border between normal childhood and full-on insanity." 
Jeffrey M. Anderson at Combustible Celluloid calls the film "a roller-coaster ride for your brain. Its the most alive and deeply-felt movie Ive seen in 1998."    Emanuel Levy at Variety (magazine)|Variety says it is "Neil Jordans most accomplished and brilliant film to date."   

Owens performance was hailed unanimously; as such, he was awarded a Special Mention at the Berlin Film Festival in 1998.

The cumulative box office according to Variety is $1,963,654. 

==Awards==
The Butcher Boy won the following awards:     

{| class="wikitable" border="1" align="center"
|-
! Award !! Category !! Name !! Outcome
|-
| rowspan="1"| European Film Awards || European Film Award for Best Cinematography || Adrian Biddle || Won
|- Berlin International Film Festival    || Silver Bear for Best Director || Neil Jordan || Won
|-
| Special Mention || Eamonn Owens || Won (for his astonishing lead)
|- Best Music || Elliot Goldenthal || Won
|-
| rowspan="1"| National Board of Review Awards 1998 || Top 10 Films || - || 6th place
|}

==Soundtrack==
 
Elliot Goldenthal composed the soundtrack for the film, which was released on CD in 1998. Goldenthal for this score mixes many different music genres and styles, yet this is one of his most melodic scores. The title song is performed by Sinéad OConnor.

==DVD release==
A widescreen, closed-captioned version of the film was released on DVD on 13 February 2007 by Warner Home Video. The disc contains deleted scenes and an audio commentary by Neil Jordan. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 