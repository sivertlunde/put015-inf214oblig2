Anari
 
{{Infobox film
| name = Anari
| image = Anari59.jpg
| image_size = 
| caption = Poster
| director = Hrishikesh Mukherjee
| producer = L. B. Lachman
| writer = Inder Raj Anand (dialogue, screenplay, story)
| narrator = 
| starring = Raj Kapoor Nutan Motilal
| music = Shankar Jaikishan
| lyrics = Hasrat Jaipuri 
| cinematography = Jaywant Pathare
| editing = Hrishikesh Mukherjee
| distributor = 
| released = 1959
| runtime = 
| country = India
| language = Hindi
| budget = 
| preceded_by = 
| followed_by = 
}}
 1959 Bollywood film directed by Hrishikesh Mukherjee. The film stars Raj Kapoor, Nutan, Motilal and Lalita Pawar. The music was by Shankar Jaikishan and the lyrics by Hasrat Jaipuri as well as Shailendra (lyricist)|Shailendra. Among the few movies that Lalita Pawar played a positive role and Motilal a role with shades of grey.

==Plot==
Raj Kumar is an honest, handsome and intelligent young man. He works as a sole trader and being a painter trade, he is unable to earn a living and unable to pay rent to his kind-hearted but talkative landlady Mrs. Dsa. One day Raj finds a wallet containing money and returns it to the owner Mr Ramnath. Ramnath admires Raj and pleased with his honesty, employs him to work in his office as a clerk. Raj meets with Ramnaths maidservant Asha and soon both fall in love. This all ends when Raj finds out that Asha is really Aarti, the niece of his employer. Unfortunately, his landlady dies consuming medicine manufactured by his employer Mr Ramnath and she passes away suddenly leaving Raj Kumar alone. The police conducts a post-mortem and as a result find out that Mrs. Dsa was poisoned. Raj became the prime suspect and is subsequently taken for questioning and is arrested and held in a cell. In the trial that he faces, however, Ramnath reveals the truth and thus Raj is saved.

==Cast==
*Raj Kapoor ...  Raj Kumar
*Nutan ...  Aarti Sohanlal
*Lalita Pawar ...  Mrs. L. DSa
*Shubha Khote ...  Asha
*Motilal ...  Seth. Ramnath Sohanlal
*Mukri ...  Kamdaar
*Nana Palsikar ...  Evil Priest
*Ruby Mayer ...  Girls College Facilitator

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!!Lyricist!!Duration
|-
| 1
| "Dil Ki Nazar Se"
| Mukesh (singer)|Mukesh, Lata Mangeshkar Shailendra
| 04:38
|-
| 2
| "Nineteen Fifty Six " 
| Manna Dey, Lata Mangeshkar
| Shailendra
| 04:59
|-
| 3
| "Woh Chand Khila Woh Tare"
| Mukesh, Lata Mangeshkar
| Shailendra
| 04:13
|-
| 4
| "Sab Kuchh Seekha Ham Ne"
| Mukesh
| Shailendra
| 03:40
|-
| 5
| "Ban Ke Panchhi Gaaye Pyar Ka Tarana"
| Lata Mangeshkar
| Hasrat Jaipuri
| 03:35
|-
| 6
| "Kisi Ki Muskurahaton Pe"
| Mukesh
| Shailendra
| 04:31
|-
| 7
| "Tera Jana"
| Lata Mangeshkar
| Shailendra
| 03:41
|}

==Trivia==
Motilal plays Nutans uncle in the film. In real life, he lived with Nutans mother, Shobhana Samarth.

==Awards==
* 1959 Filmfare Best Actor Award for Raj Kapoor
* 1959 Filmfare Best Music Director Award for Shankar Jaikishan
* 1959 Filmfare Best Supporting Actress Award for Lalita Pawar Shailendra for "Sab Kuchh Seekha Ham Ne" Mukesh for "Sab Kuchh Seekha Ham Ne"
*     

==References==
 

==External links==
*  
*  
* 

 
 

 
 
 
 
 