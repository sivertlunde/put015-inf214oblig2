Champion (2003 film)
 
{{Infobox film
| name =Champion
| image =Championfilm.jpg
| image_size     =
| caption        = The poster for the film Champion
| director = Rabi Kinagi
| writer =Rabi Kinagi Jeet Srabanti Chatterjee
| producer =Shree Venkatesh Films
| distributor = Ashadeep Entertainment
| cinematography =V. Probhakar
| editing =
| released = 15 April 2003
| country        = India
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| runtime = 166 mins  
{{Cite web
|url=http://rental.bigflix.com/bigflix/Movie/Bengali/2003/Champion
|title=Champion - BIGFlix.com
|publisher=rental.bigflix.com
|accessdate=2008-11-14
|last=
|first=
}}
  Bengali
| music = S P Venkatesh
| website        =
}}
 Bengali  film was released in 2003. Directed by  Rabi Kinagi,  the movie featured Jeet (actor)|Jeet, Srabanti Chatterjee, Sandhita Chatterjee . This movie is Jeets  third movie and Srabanti Chatterjees debut movie. 
   This film is inspired from Aamir Khans Jo Jeeta Wohi Sikandar.

==Plot==
The main theme of the film depicts how an irresponsible boy can be very responsible at a particular stage of life. In this film we have got a glittering appearance of a new hero at Tollygunge
named Jeet. Raja is a college student who is forever committing mischief. He and his peers Bhola, Dhonu & Kanu always stay on the top of the list of failures. Raja has an elder brother, Rohit who is the exact opposite of the infamous Raja. Rohit works at their fathers cafe, and is bright in studies. Their father is fed up with the restless Raja. Even after repeated whackings from his father the stubborn mule refuses to change. Rajas only support is his childhood friend Kavita who is secretly in love with him. But the flirtatious Raja finds solace in the spoilt brat Riya, the daughter of a millionaire. Raja saves Riya from the villainous millionaire student Ronny. But Riya dumps Raja & patches up with the affluent Rony. In the meanwhile the devastated Raja starts rigorously practising for the upcoming kick boxing championship final against Rony. All hell breaks loose when Rohit is beaten by Rony & his mates. Rohit is hospitalised in a critical condition. Raja approaches Rohits coach and begs him to train Raja. Thus a vigorous practice schedule begins. Raja with support from Kavita gets himself ready for the match. In the match he emerges victorious and dedicates the trophy to Rohit and then celebrates his love with Kavita.

==Cast== Jeet as Raja
* Srabanti Chatterjee as Kavita
* Dipankar Dey as Rajas Father
* Nimu Bhowmik Rajesh Sharma as Sports Coach
* Pushpita Mukherjee
* Siddhartho Banerjee as Rohit
* Sagnik Chatterjee as Ronny
* Sandhita Chatterjee as Riya
* Shyamal Dutta as Kavitas Father

==Crew==
* Producer(s): Shree Venkatesh Films
* Director: Rabi Kinagi
* Story:
* Production Design:
* Dialogue:
* Lyrics: Gautam Susmit
* Editing : Atish Chandra Sarkar
* Cinematography:
* Fight: Shantanu Pal
* Singer: Babul Supriyo, Shreya Ghoshal, Jojo (Bengali singer)|Jojo, Reema Mukherjee

==Reception==
Champion was a high critically and commercially success. Mainly Jeet (actor)|Jeets comic timing and acting was praised.

==Awards==
 

==References==
 

==External links==

 

==See also==
 
 

 
 
 
 
 