Sword of Venus
{{Infobox film
| name           = Sword of Venus
| image          = Sword of Venus poster.jpeg
| alt            = 
| caption        = Theatrical release poster
| director       = Harold Daniels 
| producer       = Jack Pollexfen Aubrey Wisberg
| writer         = Jack Pollexfen Aubrey Wisberg
| starring       = Robert Clarke Catherine McLeod Dan OHerlihy William Schallert Marjorie Stapp
| music          = Charles Koff 
| cinematography = John L. Russell
| editing        =
| studio         = American Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Sword of Venus is a 1953 American adventure film directed by Harold Daniels and written by Jack Pollexfen and Aubrey Wisberg. The film stars Robert Clarke, Catherine McLeod, Dan OHerlihy, William Schallert and Marjorie Stapp. The film was released on February 20, 1953, by RKO Pictures.   

==Plot==
 

== Cast ==
*Robert Clarke as Robert Dantes
*Catherine McLeod as Claire
*Dan OHerlihy as Danglars
*William Schallert as Valmont 
*Marjorie Stapp as Duchess De Villefort
*Merritt Stone as Fernand
*Renee De Marco as Suzette
*Eric Colmar as Goriot Stuart Randall as Hugo 

== References ==
 

== External links ==
*  

 
 
 
 
 