Everything Goes (film)
 
 
{{Infobox film 
 | name = Everything Goes
 | image = Everything Goes - Poster.jpg
 | alt        =
 | caption = Theatrical release poster
 | director = Andrew Kotatko
 | writer = Raymond Carver Andrew Kotatko
 | based on = Why Dont You Dance? by Raymond Carver
 | starring = Hugo Weaving Abbie Cornish Sullivan Stapleton Nikki Bennett
 | music = Ben Frost
 | producer = Colin Englert 
 | studio = Soft Paw Films
 | distributor = Soft Paw Films
 | budget = $180,000
 | released =  
 | runtime = 18 minutes
 | country = Australia
 | language = English 
  | }}
Everything Goes (stylized as everything goes) is a 2004 short film directed by Andrew Kotatko. It is based on the short story Why Dont You Dance? from Raymond Carvers collection What We Talk About When We Talk About Love. The film stars Hugo Weaving, Abbie Cornish and Sullivan Stapleton. Everything Goes won the award for Best Short Film at the 2004 Inside Film Awards and was the only Australian film selected for the prestigious Clermont-Ferrand International Short Film Festival in 2005.

==Synopsis==

The film depicts the unlikely relationship that forms between a young couple (Cornish and Stapleton) looking to begin their future together and a lonely middle-aged man (Weaving) trying to rid himself of the past.

==Cast==
*Hugo Weaving as Ray
*Abbie Cornish as Brianie
*Sullivan Stapleton as Jack
*Nikki Bennett as Rays Wife

==Reviews==

The film received positive reviews from critics. In an article on Australian cinema for Cinematical.com, respected American film critic Jeffrey M. Anderson praised Everything Goes as "everything a great short film can be" and noted that "Kotatko makes spectacular use of light and space, using Carvers words when necessary and conjuring up wordless images just as potent".  , Cinematical.com|Cinematical 20 November 2008. Retrieved 21 November 2008. 

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 