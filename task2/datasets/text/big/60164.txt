It's a Wonderful Life
 
{{Infobox film
| name           = Its a Wonderful Life
| image          = Its A Wonderful Life Movie Poster.jpg
| caption        = Theatrical release poster
| director       = Frank Capra
| producer       = Frank Capra
| screenplay     = {{Plain list|
* Frances Goodrich
* Albert Hackett
* Frank Capra
}}
| based on       =  
| starring       = {{Plain list|
* James Stewart
* Donna Reed
* Lionel Barrymore Thomas Mitchell
* Henry Travers
* Beulah Bondi
* Ward Bond
* Frank Faylen
* Gloria Grahame
}}
| music          = Dimitri Tiomkin Joseph Walker
| editing        = William Hornbeck
| studio         = Liberty Films
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 130 minutes
| country        = United States
| language       = English
| budget         = $3.18 million 
| gross          = $3.3 million Willian 2006, p. 4. 
}} fantasy comedy-drama The Greatest Gift", which Philip Van Doren Stern wrote in 1939 and published privately in 1945.    The film is now considered one of the most popular films in American cinema and due to numerous television showings in the 1980s has become traditional viewing during the Christmas season.
 George Bailey, Bedford Falls would be had he never been born.

Despite initially performing poorly at the box office because of high production costs and stiff competition at the time of its release, the film has come to be regarded as a classic and is a staple of Christmas television in many households.   Theatrically, the films break-even point was $6.3&nbsp;million, approximately twice the production cost, a figure it never came close to achieving in its initial release. An appraisal in 2006 reported: "Although it was not the complete box office failure that today everyone believes&nbsp;... it was initially a major disappointment and confirmed, at least to the studios, that Capra was no longer capable of turning out the populist features that made his films the must-see, money-making events they once were." 
 one of Best Picture 100 best 1998 greatest its list of the most inspirational American films of all time.  Capra revealed that this was his personal favorite among the films he directed and that he screened it for his family every Christmas season. 
 

==Plot==
  (as Mary Bailey) and James Stewart (playing George Bailey)]] Bedford Falls, New York,  on Christmas Eve, George Bailey is deeply troubled and suicidal. Prayers for his well-being from friends and family reach Heaven. Clarence Odbody, Angel 2nd Class, is assigned to save George. In order to prepare Clarence for his trip to Earth, his superior Joseph shows flashbacks of Georges life.

In 1919, at age 12, George saves the life of his younger brother Harry, who has fallen through the ice on a frozen pond, costing George the hearing in his left ear.
 Building and crush on him. Georges Uncle Billy tells George that his father has had a stroke, which proves to be fatal. Following his fathers death, George gives up his summer travel plans to stay in Bedford Falls and sort out the firms affairs, and a few months later, Mister Potter|Mr. Henry F. Potter, a rapacious slumlord and a member of the Building and Loan Association board, tries to persuade the board of directors to dissolve the Building and Loan. George talks them into rejecting Potters proposal, but they agree on condition that George run the Building and Loan. Giving his college money to Harry, George delays his plans with the understanding that Harry will take over the business upon graduation.

When Harry graduates from college, he brings home a wife, whose father has offered Harry a job. Although Harry vows to decline the offer out of respect for his brother, George cannot deny Harry such a fine opportunity and decides to keep running the Building and Loan. George calls on Mary, who has recently returned home from college. After several arguments, they reveal their love for each other, and marry, restoring an abandoned house to splendor to live in as their home.

George never manages to leave Bedford Falls, but does start Bailey Park, an affordable housing project. With his own interests compromised, Potter tries to hire him away, offering him a $20,000 salary, }} today. |group=N}} along with the promise of business trips to Europe, something that George always wanted. George turns Potter down after realizing that he intends to close down the Building and Loan and take full control of Bedford Falls. He and Mary have four children: Pete, Janie, Tommy, and Zuzu.
 bankruptcy for the Building and Loan and criminal charges for George. Uncle Billy cannot remember what happened to the money, and with a bank examiner present, he and George frantically search the town, turning up nothing.
 )]]

A desperate George appeals to Potter for a loan. Potter sarcastically turns George down, and then swears out a warrant for his arrest for bank fraud. George, now completely depressed, gets drunk at the bar and he silently prays for help. After leaving the bar and crashing his car into a tree, George staggers to a bridge, intending to commit suicide. Before he can leap, Clarence jumps in and pretends to be drowning. George rescues him and Clarence reveals himself to be Georges guardian angel.

George does not believe him and bitterly wishes he had never been born. Inspired by this comment, Clarence shows George what the town would have been like without him. In this alternate scenario, Bedford Falls is instead named Pottersville. Bailey Park was never built, and remains an old cemetery. Harry is dead as a result of George not being there to save him from drowning, and the servicemen Harry would have saved in the war are also dead. George and Marys home is the same derelict, empty shell it was on the night of their wedding. Georges mother is a bitter widow, who throws George out and reveals Uncle Billy has been in an insane asylum for many years since he lost his business. Mary is a shy, single spinster. Clarence explains how George single-handedly prevented this fate. He, and he alone, kept Potter in check, preventing the town from descending into squalor and vice.

George runs back to the bridge and begs to be allowed to live again. His prayer is answered, and he runs home joyously, but the authorities are there to arrest him. Mary, Uncle Billy, and a flood of townspeople arrive with more than enough donations to save George and the Building and Loan. Harry arrives to support his brother, and toasts George as "the richest man in town". In the pile of donated funds, George finds a copy of The Adventures of Tom Sawyer inscribed, "Dear George: Remember no man is a failure who has friends. P.S. Thanks for the wings! Love, Clarence." A bell on the Christmas tree rings, and his daughter, Zuzu, remembers aloud that it means an angel has just earned his wings. George then realizes that he has had a wonderful life.

==Cast==
{{columns-list|2| George Bailey Mary Hatch Bailey
* Henry Travers as Clarence Odbody
* Lionel Barrymore as Mr. Potter|Mr. Henry F. Potter Thomas Mitchell as Uncle Billy Bailey
* Beulah Bondi as Ma Bailey
* Frank Faylen as Ernie Bishop, the cab driver
* Ward Bond as Bert, the cop
* Gloria Grahame as Violet Bick
* H. B. Warner as Mr. Gower
* Todd Karns as Harry Bailey
* Samuel S. Hinds as father Peter "Pop" Bailey
* Lillian Randolph as Annie, maid
* Frank Albertson as Sam Wainwright
* Virginia Patton as Ruth Dakin Bailey, Harrys wife
* Mary Treen as Cousin Tilly, employee Charles Williams as Cousin Eustace, employee Sarah Edwards as Marys mother
* Harold Landon as Marty Hatch William Edmunds as Mr. Giuseppe Martini
* Argentina Brunetti as Mrs. Martini Bobby Anderson as Little George Bailey
* Ronnie Ralph as Little Sam Wainwright Jean Gale as Little Mary Hatch
* Jeanine Ann Roose as Little Violet Bick
* George Nokes as Little Harry Bailey
* Danny Mummert as Little Marty Hatch
* Sheldon Leonard as Nick, the bartender
* Frank Hagney as Potters mute aide Charles Lane as Potters rent collector
* Jimmy Hawkins as Tommy Bailey
* Karolyn Grimes as Zuzu Bailey
* Larry Simms as Pete Bailey
* Carol Coomes (aka Carol Coombs) as Janie Bailey
* Charles Halton (uncredited) as Mr. Carter, bank examiner
* J. Farrell MacDonald (uncredited) as Hank Biddle (the man whose grandfather planted the tree)
* Harry Holman (uncredited) as Mr. Partridge, College teacher Carl "Alfalfa" Switzer (uncredited) as Freddie (Marys annoying high school suitor)
* Max Wagner (uncredited) as Cashier/Bouncer at Nicks Bar
* Dick Elliott (uncredited) as fat man on porch
* Tom Fadden (uncredited) as Bridge Caretaker
* Stanley Andrews (uncredited) as Mr. Welch, teachers husband
* Al Bridge (uncredited) as Sheriff with arrest warrant
* Ellen Corby (uncredited) as Miss Davis
* Marian Carr (uncredited) as Jane Wainwright, Sams wifet 
* Adriana Caselotti (uncredited) as the singer in Martinis Bar  
* Joseph Granby (uncredited voice) as Angel Joseph
* Moroni Olsen (uncredited voice) as the Senior Angel
}}
 ), and their youngest daughter Zuzu (Karolyn Grimes)]]
The contention that James Stewart is often referred to as Capras only choice to play George Bailey is disputed by film historian Stephen Cox, who indicates that "Henry Fonda was in the running." Greene, Liz.   Blockbuster Inc. Retrieved: August 2, 2011.  Cox 2003, p. 6. 

Although it was stated that Jean Arthur, Ann Dvorak, and Ginger Rogers were all considered for the role of Mary before Donna Reed won the part, this list is also disputed by Cox as he indicates that Jean Arthur was first offered the part but had to turn it down for a prior commitment on Broadway before Capra turned to Olivia de Havilland, Martha Scott, and Dvorak. Rogers was offered the female lead, but turned it down because she considered it "too bland". In Chapter 26 of her autobiography Ginger: My Story, she questioned her decision by asking her readers: "Foolish, you say?" 
 Edward Arnold, Thomas Mitchell. You Cant Take It with You.
 Lost Horizon, You Cant The King Gower Street for many years. Also on Gower Street was a drugstore that was a favorite for the studios employees. 

Charles Williams, who was cast as Eustace Bailey, and Mary Treen, who was cast as Matilda "Tilly" Bailey, were both B-list actors, as they both had appeared in 90 films each before filming Its a Wonderful Life. 

Jimmy the raven (Uncle Billys pet) appeared in You Cant Take It with You and each subsequent Capra film.  Cox 2003, p. 24. 

==Production==

===Background===
 ]]
The original story " " was written by  s Hollywood agent, and in April 1944, RKO Pictures bought the rights to the story for $10,000, hoping to turn the story into a vehicle for Grant.  RKO created three unsatisfactory scripts before shelving the planned movie, and Grant went on to make another Christmas movie staple, The Bishops Wife.  
 Michael Wilson, and Dorothy Parker brought in to "polish" the script,  turned the story and what was worth using from the three scripts into a screenplay that Capra would rename Its a Wonderful Life.  The script underwent many revisions throughout pre-production and during filming.  Final screenplay credit went to Goodrich, Hackett and Capra, with "additional scenes" by Jo Swerling.

  opened in Seneca Falls, named for George Baileys guardian angel. On December 10, 2010, the "Its a Wonderful Life" Museum opened in Seneca Falls, with Karolyn Grimes, who played Zuzu in the movie, cutting the ribbon. 

Both James Stewart (from Indiana, Pennsylvania) and Donna Reed (from Denison, Iowa) came from small towns. Stewarts father ran a small hardware store where James worked for years. Reed demonstrated her rural roots by winning an impromptu bet with Lionel Barrymore when he challenged her to milk a cow on set. 

===Filming===
 .]]
Its a Wonderful Life was shot at RKO Radio Pictures Studio in Culver City, California, and the 89 acre RKO movie ranch in Encino, California|Encino,  where "Bedford Falls" consisted of Art Director Max Rees Academy Awards|Oscar-winning sets originally designed for the 1931 epic film Cimarron (1931 movie)|Cimarron that covered 4 acres (1.6 ha), assembled from three separate parts, with a main street stretching 300&nbsp;yards (three city blocks), with 75 stores and buildings, and a residential neighborhood.  For Its a Wonderful Life Capra built a working bank set, added a tree-lined center parkway, and planted 20 full grown oak trees to existing sets. 

Pigeons, cats, and dogs were allowed to roam the mammoth set in order to give the "town" a lived-in feel.  Due to the requirement to film in an "alternate universe" setting as well as during different seasons, the set was extremely adaptable. RKO created "chemical snow" for the film in order to avoid the need for dubbed dialogue when actors walked across the earlier type of movie snow, made up of crushed cornflakes.  Filming started on April 15, 1946 and ended on July 27, 1946, exactly on deadline for the 90-day principal photography schedule. 

RKOs movie ranch in Encino, Los Angeles|Encino, a filming location of "Bedford Falls", was razed in 1954.  There are only two surviving locations from the film. The first is the swimming pool that was unveiled during the famous dance scene where George courts Mary. It is located in the gymnasium at Beverly Hills High School and is still in operation as of 2014. The second is the "Martini home", in La Cañada Flintridge, California. 

During filming, in the scene where Uncle Billy gets drunk at Harry and Ruths welcome home/newlyweds party, George points him in the right direction home. As the camera focuses on George, smiling at his uncle staggering away, a crash is heard in the distance and Uncle Billy yells, "Im all right! Im all right!" Equipment on the set had actually been accidentally knocked over; Capra left in Thomas Mitchells impromptu ad lib (although the "crashing" noise was augmented with added sound effects).

Dimitri Tiomkin had written "Death Telegram" and "Gowers Deliverance" for the drugstore scenes, but in the editing room Capra elected to go with no music for those scenes. Those changes, along with others, led to a falling out between Tiomkin and Capra. Tiomkin had worked on several of Capras previous films, and was saddened that Capra decided to have the music pared or toned down, moved, or cut entirely. He felt as though his work was being seen as a mere suggestion. In his autobiography Please Dont Hate Me, he said of the incident, "an all around scissors job". Willian 2006, p. 15. 
 ) with Mary and Violet in Mr. Gowers drugstore]] Camel cigarettes, Lucky Strike Chesterfield cigarettes, Bayer Aspirin ("for colds and influenza"), and The Saturday Evening Post. 

In an earlier draft of the script, the scene where George saves his brother Harry as a child was different.  The scene had the boys playing ice hockey on the river (which is on Potters property) as Potter watches with disdain.  George shoots the puck, but it goes astray and breaks the "No Trespassing" sign and lands in Potters yard. Potter becomes irate, and the gardener releases the attack dogs, which causes the boys to flee.  Harry falls through the ice, and George saves him with the same results.   In the alternate scenario, Clarence erred by saying that Harry drowned at the age of nine, whereas Harrys grave was marked 1911–1919, indicating Harrys age at death as only seven or eight, depending on his birth and death dates.

Another scene that was in an earlier version of the script had young George visiting his father at his work. After George tells off Mr. Potter and closes the door, he considers asking Uncle Billy about his drugstore dilemma. Billy is talking on the phone to the bank examiner, and lights his cigar and throws his match in the wastebasket. This scene explains that Tilly (short for Matilda) and Eustace are both his cousins (not Billys kids, though), and Tilly is on the phone with her friend Martha and says, "Potters here, the bank examiners coming. Its a day of judgment." As George is about to interrupt Tilly on the phone, Billy cries for help and Tilly runs in and puts the fire out with a pot of coffee. George decides he is probably better off dealing with the situation by himself. 

Capra had filmed a number of sequences that were subsequently cut, the only remnants remaining being rare stills that have been unearthed. Cox 2003, p. 15.   A number of alternative endings were considered, with Capras first script having Bailey falling to his knees reciting "The Lords Prayer" (the script also called for an opening scene with the townspeople in prayer).  Feeling that an overtly religious tone did not have the emotional impact of the family and friends rushing to rescue George Bailey, the closing scenes were rewritten.     

==Reception==
 
Its a Wonderful Life premiered at the Globe Theatre in New York on December 20, 1946, to mixed reviews.  While Capra considered the contemporary critical reviews to be either universally negative or at best dismissive,  Time (magazine)|Time said, "Its a Wonderful Life is a pretty wonderful movie. It has only one formidable rival (Goldwyns The Best Years of Our Lives) as Hollywoods best picture of the year. Director Capras inventiveness, humor and affection for human beings keep it glowing with life and excitement." 

Bosley Crowther, writing for The New York Times, complimented some of the actors, including Stewart and Reed, but concluded that "the weakness of this picture, from this reviewers point of view, is the sentimentality of it—its illusory concept of life. Mr. Capras nice people are charming, his small town is a quite beguiling place and his pattern for solving problems is most optimistic and facile. But somehow they all resemble theatrical attitudes rather than average realities." 
 1946 Academy Awards. This move was seen as worse for the film, as 1947 did not have quite the stiff competition as 1946. If it had entered the 1947 Awards, its biggest competition would have been Miracle on 34th Street. The number one grossing movie of 1947, The Best Years of Our Lives, made $11.5 million. 

The film recorded a loss of $525,000 at the box office for RKO. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p215 
 FBI issued a memo stating "With regard to the picture Its a Wonderful Life,   stated in substance that the film represented rather obvious attempts to discredit bankers by casting Lionel Barrymore as a scrooge-type so that he would be the most hated man in the picture. This, according to these sources, is a common trick used by Communists.    addition,   stated that, in his opinion, this picture deliberately maligned the upper class, attempting to show the people who had money were mean and despicable characters."  

The films elevation to the status of a beloved classic came decades after its initial release, when it became a television staple during Christmas season in the late 1970s.  This came as a welcome surprise to Frank Capra and others involved with its production.  "Its the damnedest thing Ive ever seen," Capra told The Wall Street Journal in 1984.  "The film has a life of its own now, and I can look at it like I had nothing to do with it.  Im like a parent whose kid grows up to be President of the United States|president.  Im proud ... but its the kid who did the work.  I didnt even think of it as a Christmas story when I first ran across it.  I just liked the idea." Cox 2003, p. 11.   In a 1946 interview, Capra described the films theme as "the individuals belief in himself" and that he made it "to combat a modern trend toward atheism".   In an interview with Michael Parkinson in 1973, James Stewart declared that out of all the movies he had made, Its a Wonderful Life was his favorite. 

 ) was placed in AFIs 100 Years...100 Heroes & Villains as No. 6 of villains, while George Bailey was voted No. 9 of heroes.]]

Somewhat more iconoclastic views of the film and its content are occasionally expressed.  Wendell Jamieson, in a 2008 article for The New York Times which was generally positive in its analysis of the film, interpreted it as "a terrifying, asphyxiating story about growing up and relinquishing your dreams, of seeing your father driven to the grave before his time, of living among bitter, small-minded people. It is a story of being trapped, of compromising, of watching others move ahead and away, of becoming so filled with rage that you verbally abuse your children, their teacher and your oppressively perfect wife." 
 Richard Cohen described Its a Wonderful Life as "the most terrifying Hollywood film ever made". In the "Pottersville" sequence, he wrote, George is not "seeing the world that would exist had he never been born", but rather "the world as it does exist, in his time and also in our own."  Nine years earlier, another Salon writer, Gary Kamiya, had expressed the opposing view that "Pottersville rocks!", adding, "The gauzy, Currier and Ives|Currier-and-Ives veil Capra drapes over Bedford Falls has prevented viewers from grasping what a tiresome and, frankly, toxic environment it is... We all live in Pottersville now." 

 
In 1990, Its a Wonderful Life was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in their National Film Registry.

In 2002, Britains Channel 4 ranked Its a Wonderful Life as the seventh greatest film ever made in its poll "The 100 Greatest Films" and in 2006, the film reached No. 37 in the same channels "100 Greatest Family Films".
 10 Top 10, the best ten films in ten "classic" American film genres, after polling over 1,500 people from the creative community. Its a Wonderful Life was acknowledged as the third-best film in the fantasy genre.  
 IMDb consumer reviews and a 93% "Fresh" rating on Rotten Tomatoes.

===Antecedent=== epiphany and a renewed view of his life. 

==Awards and honors==
Prior to the Los Angeles release of Its a Wonderful Life, Liberty Films mounted an extensive promotional campaign that included a daily advertisement highlighting one of the films players, along with comments from reviewers. Jimmy Starr wrote, "If I were an Oscar, Id elope with Its a Wonderful Life lock, stock and barrel on the night of the Academy Awards". The New York Daily Times offered an editorial in which it declared the film and James Stewarts performance to be worthy of Academy Award consideration. 

Its a Wonderful Life received five Academy Award nominations:  . oscars.org. Retrieved: August 17, 2011. 

{| class="wikitable" border="1"
|- Award !! Nominee / Winner
|- Best Picture ||   || Frank Capra    Winner was Samuel Goldwyn - The Best Years of Our Lives 
|- Best Director ||   || Frank Capra    Winner was William Wyler - The Best Years of Our Lives 
|- Best Actor ||   || James Stewart    Winner was Fredric March - The Best Years of Our Lives 
|- Best Film Editing ||   || William Hornbeck    Winner was Daniel Mandell - The Best Years of Our Lives 
|- Best Sound Recording ||   || John Aalberg    Winner was John P. Livadary - The Jolson Story 
|- Technical Achievement RKO Radio Studio Special Effects Dept.   For the development of a new method of simulating falling snow on motion picture sets. 
|}

Its a Wonderful Life won just the one Academy Award, in the Technical Achievement category for developing a new method of creating artificial snow.  Before Its a Wonderful Life, fake movie snow was mostly made from cornflakes painted white. And it was so loud when stepped on that any snow filled scenes with dialogue had to be re-dubbed afterwards. RKO studios head of special effects, Russell Sherman, developed a new compound, utilizing water, soap flakes, foamite and sugar. 

The Best Years of Our Lives, a drama about servicemen attempting to return to their pre-World War II lives, won most of the awards that year, including four of the five for which Its a Wonderful Life was nominated. (The award for "Best Sound Recording" was won by The Jolson Story.) The Best Years of Our Lives was also an outstanding commercial success, ultimately becoming the highest grossing film of the decade, in contrast to the more modest initial box office returns of Its a Wonderful Life. 

Its a Wonderful Life received a Golden Globe Award:

{| class="wikitable" border="1"
|- Award !! Winner
|- Best Director  ||   || Frank Capra
|} Cinema Writers Circle in Spain, for Mejor Película Extranjera (Best Foreign Film). Jimmy Hawkins won a "Former Child Star Lifetime Achievement Award" from the Young Artist Awards in 1994; the award recognized his role as Tommy Bailey as igniting his career, which lasted until the mid-1960s. 

American Film Institute lists

* AFIs 100 Years...100 Movies&nbsp;– 11
* AFIs 100 Years...100 Passions&nbsp;– 8
* AFIs 100 Years...100 Heroes and Villains:
** Mr. Potter&nbsp;– No. 6 Villain
** George Bailey&nbsp;– No. 9 Hero
* AFIs 100 Years...100 Songs:
** "Buffalo Gal (Wont You Come Out Tonight)" - Nominated 
* AFIs 100 Years...100 Movie Quotes:
** "What is it you want, Mary? What do you want? You want the moon? Just say the word, and Ill throw a lasso around it and pull it down." - Nominated   
** "To my big brother George, the richest man in town!" - Nominated 
** "Look, Daddy. Teacher says, Every time a bell rings an angel gets his wings." - Nominated 
* AFIs 100 Years...100 Cheers&nbsp;– No. 1
* AFIs 100 Years...100 Movies (10th Anniversary Edition)&nbsp;– No. 20
* AFIs 10 Top 10&nbsp;– No. 3 Fantasy Film

==Release==

===Ownership and copyright issues===

====Ancillary rights====
  original nitrate film elements, the music score, and the film rights to the story on which the film is based, "The Greatest Gift".  National Telefilm Associates (NTA) took over the rights to the film soon thereafter.

A clerical error at NTA prevented the copyright from being renewed properly in 1974.   Despite the lapsed copyright, television stations that aired the film (after 1993) were still required to pay royalties. Although the films images had entered the public domain, the films story was still protected by virtue of it being a derivative work of the published story "The Greatest Gift", whose copyright was properly renewed by Philip Van Doren Stern in 1971. Cox 2003, p. 115.    The film became a perennial holiday favorite in the 1980s, possibly due to its repeated showings each holiday season on hundreds of local television stations. It was mentioned during the deliberations on the Copyright Term Extension Act of 1998.  

In 1993,   is licensed to show the film on U.S. network television, and traditionally shows it twice during the holidays, with one showing on Christmas Eve. Paramount (via parent company Viacoms 1998 acquisition of Republics then-parent, Spelling Entertainment) once again has distribution rights for the first time since 1955.  
 Riding High in 1950). 

====Colorization====
Director Capra met with Wilson Markle about having Colorization, Inc.,   (1941) and Lady for a Day (1933)".  However, the film was believed to be in the public domain at the time, and as a result Markle and Holmes responded by returning Capras initial investment, eliminating his financial participation, and refusing outright to allow the director to exercise artistic control over the colorization of his films, leading Capra to join in the campaign against the process. 
 Video Treasures. A third colorized version was produced by Legend Films and released on DVD in 2007 with the approval of Capras estate. 

===Home media===

====VHS and LaserDisc====
Throughout the 1980s and early 1990s, when the film was still under public domain status, Its a Wonderful Life was released on VHS by a variety of home video companies. Among the companies that released the film on home video before Republic Pictures stepped in were Meda Video (which would later become Media Home Entertainment), Kartes Video Communications (under its Video Film Classics label), GoodTimes Home Video and Video Treasures (now Anchor Bay Entertainment). The film was also issued on LaserDisc by The Criterion Collection.

After Republic reclaimed the rights to the film, all unofficial VHS copies of the film in print were destroyed.  Artisan Entertainment (under license from Republic) took over home video rights in the mid-1990s. Artisan was later sold to Lions Gate Entertainment, which continued to hold US home video rights until late 2005 when they reverted to Paramount, who also owns video rights throughout Region 4 (Latin America and Australia) and in France. Video rights in the rest of the world are held by different companies; for example, the UK rights are with Universal Studios.

====Technological first: CD-ROM====
In 1993, due in part to the confusion of the ownership and copyright issues, Kinesoft Development, with the support of Republic Pictures, released Its a Wonderful Life as one of the first commercial feature-length films on CD-ROM for the Windows PC (Windows 3.1). Predating commercial DVDs by several years, it included such features as the ability to follow along with the complete shooting script as the film was playing.   
 486SX processor and only 8&nbsp;MB of RAM. 

====DVD and Blu-ray Disc====
  White Christmas as part of the "Classic Christmas Collection" two-disc set on that same day. Paramount released a two-disc "special edition" DVD of the film that contained both the original theatrical black-and-white version, and a new, third colorized version, produced by Legend Films using the latest colorization technology on November 13, 2007. Paramount released a DVD version with a "Collectors Edition Ornament", and a Blu-ray Disc edition on November 3, 2009.

==Adaptations in other media== radio in 1947, first on Lux Radio Theater (March 10) and then on The Screen Guild Theater (December 29), then again on the Screen Guild Theater broadcast of March 15, 1951. James Stewart and Donna Reed reprised their roles for all three radio productions. Stewart also starred in the May 8, 1949 radio adaptation presented on the Screen Directors Playhouse.
 A Wonderful Life, was written by Sheldon Harnick and Joe Raposo. This version was first performed at the University of Michigan in 1986, but a planned professional production was stalled by legal wrangling with the estate of Philip Van Doren Stern. It was eventually performed in Washington, D.C. by Arena Stage in 1991, and had revivals in the 21st century, including a staged concert version in New York City in 2005 and several productions by regional theatres.
 Majestic Theatre, Dallas, Texas in 1998. It was an annual Christmas show at the theatre for five years. It has since been performed at venues all around the United States.  }} 

The film was also adapted into a play in two acts by James W. Rodgers. It was first performed on December 15, 1993 at Paul Laurence Dunbar High School. The play opens with George Bailey contemplating suicide and then goes back through major moments in his life. Many of the scenes from the movie are only alluded to or mentioned in the play rather than actually dramatized. For example, in the opening scene Clarence just mentions George having saved his brother Harry after the latter had fallen through the ice. 

Its a Wonderful Life: A Live Radio Play, a stage adaptation presented as a 1940s radio show, was adapted by Joe Landry and has been produced around the United States since 1997. The script is published by Playscripts, Inc.

In 1997, PBS aired Merry Christmas, George Bailey, taped from a live performance of the 1947 Lux Radio Theatre script at the Pasadena Playhouse. The presentation, which benefited the Elizabeth Glaser Pediatric AIDS Foundation, featured an all-star cast including Bill Pullman as George, Nathan Lane as Clarence, Martin Landau as Mr. Potter, Penelope Ann Miller as Mary, and Sally Field as Mother Bailey. 

Philip Grecians 2006 radio play based on the film Its a Wonderful Life is a faithful adaptation, now in its third incarnation, that has been performed numerous times by local theatres in Canada. 
 John McDaniel told Saint Louis Magazine, "Im in the throes of writing a musical version&nbsp;... right now, working with Kathie Lee Gifford, whos doing the lyrics. I find were mostly writing to character: Is it George, or the old guy who runs the bank? What do they want, what are they trying to do, what is the mood of that&nbsp;— is it staccato, are they agitated, is it a ballad?" 
 John Pierson. The novel imagines the future lives of various characters if George had not survived his jump into the river. 

==Remakes== film was remade as the 1977 television movie It Happened One Christmas. Lionel Chetwynd based the screenplay on the original Van Doren Stern short story and the 1946 screenplay. This remake employed gender-reversal, with Marlo Thomas as the protagonist Mary Bailey, Wayne Rogers as George Hatch, and Cloris Leachman as the angel Clara Oddbody.  Leachman received her second Emmy nomination for this role. In a significant departure from his earlier roles, Orson Welles was cast as Mr. Potter.  Following initial positive reviews, the made-for-television film was rebroadcast twice in 1978 and 1979, but has not been shown since on national re-broadcasts, nor issued in home media.  Webb, Chad.  . 411mania.com, 2010. Retrieved: August 2, 2011. 

==Unauthorized sequel==
A sequel aimed for 2015 release is in development, to be called Its a Wonderful Life: The Rest of the Story. It will be written by Bob Farnsworth and Martha Bolton and follow the angel of George Baileys daughter Zuzu (played once again by Karolyn Grimes), as she teaches Baileys evil grandson how different the world would have been if he had never been born. Producers were originally looking for directors in hopes to shoot the film with a $25–$35 million budget in Louisiana early in 2014.  The film has been announced as being produced by Star Partners and Hummingbird Productions, neither of which are affiliated with Paramount, owners of the original film (Farnsworth claimed that Its a Wonderful Life was in the public domain). Days later, a Paramount spokesperson claimed that they were not granted permission to make the film, and it cannot be made without the necessary paperwork. "To date, these individuals have not obtained any of the necessary rights, and we would take all appropriate steps to protect those rights," the spokesperson said. 

==Spin-off==
In 1991, another made-for-television film called Clarence (1990 film)|Clarence starred Robert Carradine in a new tale of the helpful angel.  

==Popular culture==
 
 
Its a Wonderful Life has been popularized in modern cultural references in many of the mainstream media. Due to the proliferation of these references, a few examples will suffice to illustrate the films impact.

Its story has been parodied in episodes of American television series such as Married... with Children, Mork & Mindy and Dallas (1978 TV series)|Dallas, as well as the television film Its a Very Merry Muppet Christmas Movie. Also in the episode "Caved In" of the Nickelodeon show, Henry Danger. The town of Kingston Falls in the 1984 film Gremlins is laid out to look like Bedford Falls, and clips from Its A Wonderful Life also appear within the film. 

The   (1996), which featured a clip from the film, pokes fun at the persistent reports of a connection, having them look at each other in disbelief as George calls Bert and Ernie by name. 

Stephen Jay Goulds book Wonderful Life: The Burgess Shale and the Nature of History takes its main title from the film. The book proposes that the evolution of life, rewound and replayed multiple times, would yield a different world each time, just as life without George Bailey is Pottersville, not Bedford Falls.
 

==See also==
 
* Mr. Destiny, a 1990 film
* The Family Man, a 2000 film Alternate universe in film
* Dastar Corp. v. Twentieth Century Fox Film Corp. (a legal case partially relating to another example of an out of copyright adaption of a work still under copyright)
* Zu Zu Ginger Snaps

==References==
 

===Citations===
 

===Bibliography===
 
* Barker, Martin and Thomas Austin. "Films, Audiences and Analyses". From Antz To Titanic: Reinventing Film Analysis. London: Pluto Press, 2000, pp.&nbsp;15–29. ISBN 0-7453-1584-4.
* Cahill, Marie. Its a Wonderful Life. East Bridgewater, Massachusetts: World Publications Group, 2006. ISBN 978-1-57215-459-9.
* Capra, Frank. Frank Capra, The Name Above the Title: An Autobiography. New York: The Macmillan Company, 1971. ISBN 0-306-80771-8.
* Cox, Stephen. Its a Wonderful Life: A Memory Book. Nashville, Tennessee: Cumberland House, 2003. ISBN 1-58182-337-1.
* Eliot, Mark. Jimmy Stewart: A Biography. New York: Random House, 2006. ISBN 1-4000-5221-1.
* Finler, Joel W. The Hollywood Story: Everything You Always Wanted to Know About the American Movie Business But Didnt Know Where to Look. London: Pyramid Books, 1988. ISBN 1-85510-009-6.
* Goodrich, Francis, Albert Hackett and Frank Capra. Its a Wonderful Life: The Complete Script in its Original Form. New York: St. Martins Press, 1986. ISBN 0-312-43911-3.
* Jones, Ken D., Arthur F. McClure and Alfred E. Twomey. The Films of James Stewart. New York: Castle Books, 1970.
* McBride, Joseph. Frank Capra: The Catastrophe of Success. New York: Touchstone Books, 1992. ISBN 0-671-79788-3.
* Michael, Paul, ed. The Great Movie Book: A Comprehensive Illustrated Reference Guide to the Best-loved Films of the Sound Era. Englewood Cliffs, New Jersey: Prentice-Hall Inc., 1980. ISBN 0-13-363663-1.
* Rodgers, James W. Its a Wonderful Life: A Play in Two Acts. Woodstock, Illinois: Dramatic Publishing, 1994. ISBN 0-87129-432-X.
* Walters, James. "Reclaiming the Real: Its a Wonderful Life (Frank Capra, 1946)". Alternative Worlds in Hollywood Cinema. Bristol UK: Intellect Ltd, 2008, pp.&nbsp;115–134. ISBN 978-1-84150-202-1.
* Wiley, Mason and Damien Bona. Inside Oscar: The Unofficial History of the Academy Awards. New York: Ballantine Books, 1987. ISBN 0-345-34453-7.
* Willian, Michael. The Essential Its a Wonderful Life: A Scene-by-Scene Guide to the Classic Film, 2nd ed. Chicago: Chicago Review Press, 2006. ISBN 978-1-55652-636-7.
 

==Further reading==
* Stewart, Jimmy. " ". 1977. MyMerryChristmas.com, 2012. Web. 09 Jan. 2012.
* Cox, Stephen. " ". Los Angeles Times 23 Dec. 2006: E-1. Web. 09 Jan. 2012.
* Sullivan, Daniel J. " ", Humanitas (2005) 18.1-2: 115-140. Web. 09 Jan. 2012.
* Kamiya, Gary. " " Salon.com|Salon 22 Dec. 2001. Web. 09 Jan. 2012.
*  

==External links==
 
 
*  
*  
*   at Eeweems.com
*   at AmericanMusicPreservation.com
*  , at sendaframe.com

Streaming audio
*   on Lux Radio Theater: March 10, 1947
*   on Screen Directors Playhouse: May 8, 1949

Video
*   on YouTube: December 18, 2014

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 