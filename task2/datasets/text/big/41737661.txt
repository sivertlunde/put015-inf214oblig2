Daawat-e-Ishq
{{Infobox film
| name = Daawat-e-Ishq
| image = Daawat-e-ishq poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Habib Faisal
| producer = Aditya Chopra
| writer = Habib Faisal Jyoti Kapoor
| starring = Aditya Roy Kapur Parineeti Chopra Anupam Kher
| music = Songs: Sajid-Wajid Background Score: Hitesh Modak
| runtime = 
| Release Date =  
| cinematography = Himman Dhamija
| editing = Meghna Sen
| studio = Yash Raj Films
| distributor = Yash Raj Films
| released =      
| country = India
| language = Hindi
| budget =  (approx.)
| gross =   (Worldwide Collection)
}}
 romantic comedy film directed by Habib Faisal and produced by Aditya Chopra under the banner of Yash Raj Films. It features actors  Parineeti Chopra and Aditya Roy Kapur in the lead roles. Music of the film has been composed by Sajid-Wajid.   

The film was released on September 19, 2014, to mostly mixed reviews. The Hindu termed the film as a Potent recipe, and Times of India gave the film a 2.0 rating.    The film collected   at the box office in its first week run and was declared a flop at the boxoffice.   

==Plot== 498A (dowry act) and to recover lacs of money from him, in order to fulfill her dream of going to America.

She along with her father goes to Lucknow with fake identity and comes up against "Big Boss Haidari Kebab," Tariq also known as "Taru" Haider (Aditya Roy Kapur). They choose Tariq as their target and when Tariqs parents asks for dowery from Gullus father, she secretly records all the conversation. During three days before the wedding Taru and Gullu get to know each other and Gullu starts falling for Taru. To her surprise Taru returns her 40 lakhs (approx. 3lakh diram) cash which his father asked for dowry.

Gullu still sticks with her plan of drugging Taru on the wedding night and runs away with all the cash and also recovring 40 lakhs more from Tarus family via a police officer, black mailing to file fir under section 498A. Taru decides to take revenge as he finds out the real identity of Gullu. Meanwhile, Gullu and her father start preparing to leave for America. But Gullu kept feeling the guilt of bluffing an honest person and hence decides to return all the money. When they reach railway station to board a train to Lucknow, Gullu is confronted by Taru. Gullu returns him all the money and confesses her love for him. They both reunite and plan a real wedding without any dowry. Meanwhile, Amjad also realises his mistake and revolts to his parents for demanding dowry.

==Cast==
* Parineeti Chopra as Gulrez "Gullu" Qadir / Sania Habibullah
* Aditya Roy Kapur as Tariq "Taru" Haidar 
* Anupam Kher    as Abdul Qadir / Seheryar Habibullah
* Sumit Gaddi as Neeraj
* Karan Wahi as Amjad

==Production==
Daawat-e-Ishq is the second collaboration of Habib Faisal and Parineeti Chopra after Ishaqzaade .    Television actor Karan Wahi will be making his silver-screen debut with this film.    Anupam Kher has been said to also star in the film. 

== Music==
{{Infobox album
| Name = Daawat-e-Ishq
| Type = soundtrack
| Artist = Sajid-Wajid
| Lyrics = Kausar Munir
| Cover =
| Alt = 
| Released =  
| Recorded =  Feature film soundtrack
| Length =  
| Label = YRF Music
| Producer = 
| Last album = Heropanti (2014)
| This album = Daawat-e-Ishq (2014)
| Next album = Tevar (2014)
}}

The music of Daawat-e-Ishq was composed by the duo Sajid-Wajid,  while its lyrics were penned by Kausar Munir.  The title song of Daawat-E-Ishq was released on 17 July 2014,  and the soundtrack of the film was released on 18 July 2014. 

{{track listing
| headline = Daawat-e-Ishq  Soundtrack
| extra_column = Singer(s)
| lyrics_credits = no
| total_length = 27:53
| music_credits = no
| title1 = Daawat-e-Ishq
| extra1 = Javed Ali, Sunidhi Chauhan
| music1 = Sajid-Wajid
| lyrics1 = Kausar Munir
| length1 = 5:20
| title2 = Mannat
| extra2 = Keerthi Sagathia, Sonu Nigam, Shreya Ghoshal
| music2 = Sajid-Wajid
| lyrics2 = Kausar Munir
| length2 = 9:59
| title3 = Rangreli
| extra3 = Sajid-Wajid|Wajid, Shreya Ghoshal
| lyrics3 = Kausar Munir
| music3 = Sajid-Wajid
| length3 = 3:54
| title4 = Shayarana
| extra4 = Shalmali Kholgade
| lyrics4 = Kausar Munir
| music4 = Sajid-Wajid
| length4 = 4:16
| title5 = Jaadu Tone Waaliyan
| extra5 = Shabab Sabri
| lyrics5 = Kausar Munir
| music5 = Sajid-Wajid
| length5 = 4:12
| title6 = Mannat (Reprise)
| extra6 = Sonu Nigam, Shreya Ghoshal
| lyrics6 = Kausar Munir
| music6 = Sajid-Wajid
| length6 = 2:55
| title7 = Daawat-e-Ishq (Instrumental)
| extra7 = -
| music7 = Sajid-Wajid
| lyrics7 = 
| length7 = 1:50
}}

== Choreography==
The Choreographers for the movie songs are: 
* Rekha and Chinni Prakash for the song "Daawat-e-Ishq" and "Rangreli".  
* Bosco-Caesar for the song "Shayrana".
* Adil Shaikh for the songs "Mannat" and "Jaadu Tone Waaliyan".

==Box office == Mary Kom but they also had competition with Khoobsurat (2014 film)|Khoobsurat. The movie managed to collect   on its opening day and collected approximately   on its opening weekend. The movie ended with   and was unable to make the budget of   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 