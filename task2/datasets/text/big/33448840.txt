The Blue Yonder
{{Infobox film
| name           = The Blue Yonder
| image          =
| director       = Mark Rosman
| producer       =  
| writer         =  
| starring = Peter Coyote Huckleberry Fox Art Carney Dennis Lipscomb Joe Flood
| music          = Steven M. Stern
| cinematography = Hiro Narita
| editing        =  
| studio         = Walt Disney Home Video
| distributor    = Walt Disney Television
| released       =  
| runtime        = 89 minutes
| country        = United States English
| budget         =
| gross          =
}} back in time from 1985 to 1927 by a time machine built by his neighbor, Henry Coogan (Carney). There Jonathan meets his grandfather, Max (Coyote), and must find a way to prevent Maxs fatal attempt at a solo trans-Atlantic flight.

==Cast==
*Peter Coyote...Max Knickerbocker
*Huckleberry Fox...Jonathan Knicks
*Art Carney...Henry Coogan
*Dennis Lipscomb...Finch
*Joe Flood...Leary
*Mittie Smith...Helen Knickerbocker
*Frank Simons...Young Coogan
*Stu Klitsner...Mr. Knicks
*Morgan Upton...Police Captain
*Bennett Cale...Dooley
*Cyril Clayton...Drunk Charles Adams...News Vendor
*Gretchen Grant...Mrs. Knicks

==External links==
 

 

 
 
 
 
 
 


 