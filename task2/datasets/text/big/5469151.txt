A Date with Your Family
{{Infobox Film
| name = A Date with Your Family
| image = A Date with Your Family.jpg
| image_size =
| caption =A screenshot from A Date with Your Family
| director = Edward G. Simmel
| producer = Edward G. Simmel
| writer = Arthur V. Jones
| narrator = Hugh Beaumont
| starring = Ralph Hodges
| music =
| cinematography = Harry F. Burrell
| editing = Miriam Bucher
| distributor = Encyclopaedia Britannica Films
| released = 1950
| runtime = 10 min.
| country = USA English
}}
 social engineering short film presented by Simmel-Meservey, directed by Edward G. Simmel, and written by Arthur V. Jones to primarily show youth how to act and behave with parents during dinner to have a pleasant time. The subject family consists of a father, mother and their children - a daughter, older son and a younger son. The narrator tells what happens with the family; what should happen during the meal, what types of manners and socializing should be exhibited to not sour the time with your family and what should not happen. There are many stereotypical views of each person to coincide with the preferred image of a nuclear family in the post-war era of the 1950s.

==Cast==

* Hugh Beaumont: Narrator
* Ralph Hodges: Brother (uncredited)

==Overview==
The film starts with the father coming home from work in the afternoon and is met with his wife. The older son is completing his homework when Junior comes home dirty from baseball and is helped to clean up by the older brother. Then the older brother meets Father to have a pleasant chat. The narrator points out that it is not the time for the son to bring up any bad news such as poor grades. The mother and daughter wear their best dresses to the table to please the men of the family and the daughter helps the mother to set up the table. When the food is served, no one is to begin eating until the mother and father do so. Table talk is to remain pleasant and inoffensive and everyone should be themselves, no one is to be emotional. It is shown that the conversation should be light and not egocentric. The daughter begins to criticize her clothes to other women which causes negativity. The older son begins to talk and motion graphically about a fight, in turn disgusting the father, and then argues with his sister. When the meal is over, the narrator then points out the dishes being put away by the brother and sister while the mother and father talk. The short then ends with the narrator saying that a pleasant time can be shared with the family every time if all of these steps are taken and also the actions that should be avoided.

==Mystery Science Theater 3000== Invasion U.S.A.. Mike Nelson commented, "Hey, I like my family as a friend!".
 Mystery Science Theater 3000 - XXVI DVD collection with The Sword and the Dragon.

==External links==
*  

 
 
 
 
 
 
 