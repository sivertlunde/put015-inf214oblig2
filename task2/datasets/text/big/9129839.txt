Mentor (film)
{{Infobox Film  
| name           = Mentor
| image          = Mentor_poster_comp1.jpg
| caption        = Theatrical poster
| producer       = Jeff Eline Gill Holland Lillian LaSalle William Whitehurst
| director       = David Langlitz
| writer         = William Whitehurst
| starring       = Rutger Hauer Matthew Davis Dagmara Dominczyk Susan Misner
| cinematography = Miguel Ioan Littin
| editing        = Simeon Hunter
| art director   = Alexandra Brandenburg
| music          = Ceiri Torjussen
| distributor    = BMore Pictures
| released       = April 28, 2006
| country        = United States
| language       = English
| runtime        = 90 minutes
| budget         = $2,000,000
|}}
Mentor is a 2006 drama film directed by David Langlitz and written by William Whitehurst, exploring the relationship between a Pulitzer Prize–winning author and his protégée.

The film stars Rutger Hauer, Matthew Davis, Dagmara Dominczyk, and Susan Misner.

==Plot==
Matthew Davis stars as Carter, introduced as a thirtysomething professor at a mediocre college. Through flashbacks, we learn about Carters time as a promising writer enrolled in an exclusive grad school class taught by Sanford Pollard (Rutger Hauer), a hard-drinking, hard-driving, brilliant but abrasive writer whose career has stalled since winning a Pulitzer Prize decades ago. Julia (Dagmara Dominczyk) is Sanfords graduate assistant as well as his lover. Sanford and Julia "adopt" the young, eager Carter and expose him to the world of wealth, television, and drugs. Carter travels with them to Sanfords beach house, where he and Julia eventually become lovers.

In the present, Carter has become a shadow of his former promise, a lethargic teacher, an alcoholism|alcoholic, excessive smoker, lacking the ability to sustain relationships. Like Sanford, he has worked his way through a series of graduate assistant girlfriends, and we watch as he apparently allows his relationship with Susan (Lynn Chen) to wither away. Carter is on a reckless path until he receives notice that Sanford has died of cancer.

The film jumps back and forth from the present to ten years earlier, as Carter wrestles with the unresolved emotions he felt with Sanford and Julia. After attending Sanfords funeral, Carter returns to the beach house, where Sanfords will is being read. There he again meets Julia, older but still appealing, and they discover that Sanford has left the two of them his entire estate. The film ends ambiguously, with Carter and Julia driving off into the night in the Porsche that had been Sanfords, but which is now theirs in "joint custody".

==Film notes==
Mentor was first presented at the Tribeca Film Festival in the spring of 2006. It also was accepted at the Maryland Film Festival on May 13, 2006. 

As of January 2007, the film has not been generally released.

===Editing techniques===
The movie makes liberal use of flashback techniques, introducing the characters in 1997 at the end of their friendship, moving to the present day, then alternating between to sequential timelines, one in 1997 the other a decade later.

===Filming locations===
The film was shot entirely in Maryland. Johns Hopkins University doubled for both colleges, St. Michaels, Maryland was the location for all of the vacation scenes, and various locations in Baltimore served as the background to the story.

==Cast==
* Rutger Hauer as Sanford Pollard
* Matthew Davis as Carter
* Dagmara Dominczyk as Julia
* Susan Misner as Marilyn Conner
* Matt Servitto as Howard
* Peter Scolari as Jonathan Parks
* Lynn Chen as Terry Valentine
* Isabel Glasser as Margaret Burger
* Lawrence Pressman as  Kendal
* Ronald Guttman as Interviewer
* Carrie Yaeger as Nurse

==Footnotes==
 

==External links==
*  
*  
*  

 
 
 
 
 
 