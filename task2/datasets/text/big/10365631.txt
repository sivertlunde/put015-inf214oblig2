The Matrimonial Bed
{{Infobox film name           = The Matrimonial Bed image          = Matrimonialbed19302.jpg caption        = Poster showing Florence Eldridge and Lilyan Tashman producer       = director       = Michael Curtiz writer         = Harvey F. Thew Seymour Hicks (based on the play by André Mouézy-Éon and Yves Mirande) starring  Frank Fay  Lilyan Tashman Florence Eldridge Beryl Mercer Arthur Edmund Carewe Vivien Oakland James Gleason music          = Louis Silvers cinematography = Devereaux Jennings editing        = Jack Killifer distributor    = Warner Bros. released       =   runtime        = 69 minutes language  English
|country        = United States
}}

The Matrimonial Bed is a 1930 American Pre-Code comedy film produced and released by Warner Bros.. It was based on the French play by André Mouézy-Éon and Yves Mirande. The English version of the play, by Seymour Hicks, opened in New York on October 12, 1927 and had 13 performances.

==Synopsis==
Frank Fay is a man who was in a train wreck five years earlier and was taken for dead by his wife, who is played by Florence Eldridge. Fay and Eldridge have both remarried. Fay, who remembers nothing that occurred before the train wreck, is the father of two sets of twins by his new wife, who is played by Lilyan Tashman. Eldridge has recently had a child with her new husband, played by James Gleason. Fay is a very popular hairdresser and some of Eldridges friends urge her to try him. When Fay shows up at the house, he shocks the servants and his ex-wife. A doctor, played by Arthur Edmund Carewe, manages to restore Fays memory through the use of hypnosis but in the process makes him forget what has happened in the last five years. When Fay awakes from hypnosis, he thinks he has only been unconscious for a short while. He assumes he is still Eldridge´s husband. The doctor warns everyone not to tell him the truth because the shock could kill him. Just at this crucial moment, Eldridges current husband arrives home and is shocked to find Fay in his bed. Later on, Eldridges current wife arrives only to find her husband in bed with Gleason. Eventually, Fay finds out what has happened and asks the doctor to pretend to take back his memory so that his former wife, whom he deeply loves, can continue to live with her new husband.

==Pre-Code Sequences==
* Frank Fay is discovered in bed with James Gleason and Lilyan Tashman assumes they are having an affair and shockingly exclaims: "What kind of a house is this?"
* When Dr. Beaudine (Arthur Edmund Carewe) attempts to examine Frank Fay, Frank assumes he is gay and refuses to take off his shirt. When Dr. Beaudine closes the light in order to hypnotize him Frank Fay exclaims that he was right in his suspicions about him.
* The movie has numerous gay jokes as the hairdresser/husband played by Frank Fay camps up the hairdresser persona to differentiate himself from the personality of the husband. There are lines like - "I may be a hairdresser but that doesnt mean I hold mens hands" And when he asks what manner of person was he as the hairdresser, he is told, "You were gay, a bit dandified." 

==Songs==
* "Fleur DAmour"

==Miscellany== Dickie Moore makes an uncredited cameo appearance in this film.

==Preservation==
The film survives intact and has been by United Artists Associated.

== External links ==
*  

 

 
 
 
 
 
 
 
 