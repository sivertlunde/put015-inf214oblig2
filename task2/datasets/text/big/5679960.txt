The Spoilers (1930 film)
 
{{Infobox film
| name           = The Spoilers
| image          = Spoilers1930.jpg
| caption        = Theatrical release poster
| director       = Edwin Carewe
| producer       = Edwin Carewe
| writer         = {{Plainlist|
* Bartlett Cormack
* Agnes Brand Leahy
}}
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Gary Cooper
* Kay Johnson
* Betty Compson
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film 1898 Gold Rush, the film is about a gold prospector and a corrupt Alaska politician who fight for control over a gold mine. The film features a spectacular saloon fistfight between Cooper and William "Stage" Boyd.   
 of the 1923 (with 1942 (with 1955  Jeff Chandler as Glennister, and Rory Calhoun as McNamara). The 1930 and 1942 versions were the only instance of Gary Cooper and John Wayne playing exactly the same role in the same story in two different films.

==Plot==
While traveling to Nome, Alaska, Roy Glenister ( ), and a politician named Alec McNamara (William "Stage" Boyd ). They have been engaged in a racket claiming titles to various mines, ejecting the miners, and then making McNamara owner of the disputed properties. 

The three corrupt officials lay claim to The Midas. McNamara also steals money from Glenister, Dextry, and Slapjack, preventing them from enlisting legal help from the United States. When Dextry and Glenister plan a vigilante action, McNamara calls in a detail of soldiers to protect "his property". As Glenister and McNamara prepare for a gunfight, they are dissuaded by Helen, who suggests that the courts handle the dispute. Later, after jealous saloon owner Cherry Malotte (Betty Compson) lies to Glennister telling him that Helen and McNamara are conspiring to cheat him again, Glennister and McNamara settle their differences with a spectacular fistfight, with McNamara getting the worst. Afterwards, Glenister wins the hand of Helen.

==Cast==
*Gary Cooper as Roy Glenister
*Kay Johnson as Helen Chester
*Betty Compson as Cherry Malotte
*William "Stage" Boyd as Alec McNamara
*Harry Green as Herman
*Slim Summerville as Slapjack Simms James Kirkwood as Joe Dextry
*Lloyd Ingraham as Judge Stillman
*Oscar Apfel as Struve George Irving as William Wheaton
*Knute Erickson as Captain Stevens
*Merrill McCormick as Miner
*Charles K. French as Man in Bar
*Jack Holmes as Voorhees
*John Beck as Hanson 

==Production==
The Spoilers was filmed on location in Oregon.   

==Reception==
In his review for The New York Times, Mordaunt Hall gave the film a negative review for its poor narrative, unconvincing plot, and "absurdly melodramatic dialogue".    Believing that the film would have benefitted from more details of the working for gold and fewer scenes in gambling halls and other places,  Hall continued:
 
Finally, Hall criticized the films "general lack of intelligence" and the narrative, which "runs from one scene to another with too much threatening talk and an ineffectual misunderstanding between Glenister and Helen Chester, who are in love with each other."  

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 