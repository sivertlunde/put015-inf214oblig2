Li'l Abner (1940 film)
{{Infobox film
| name = Lil Abner
| image = Billie Seward-Jeff York in Lil Abner.jpg
| caption = Still with Billie Stewart and Jeff York
| director = Albert S. Rogell| John E. Burch (assistant) Charles Kerr Johnnie Morris Buster Keaton
| producer = Lou L. Ostrow Otto Ludwig
| music = Lucien Moraweck
| distributor = RKO Radio Pictures
| released =  
| runtime = 78 minutes
| language = English
}}
 of the same name created by Al Capp. The three most recognizable names associated with the film are Buster Keaton as Lonesome Polecat, Jeff York as Lil Abner, and Milton Berle, who co-wrote the title song.
 1956 Broadway musical of the same name.

==Synopsis==
Lil Abner becomes convinced that he is going to die within twenty-four hours, so agrees to marry two different girls: Daisy Mae (who has chased him for years) and Wendy Wilecat (who rescued him from an angry mob). It is all settled at the Sadie Hawkins Day race.

==Cast== Jeff York (as Granville Owen)
* Daisy Mae Scraggs played by Martha ODriscoll
* Pansy "Mammy" Yokum played by Mona Ray Johnnie Morris
* Lonesome Polecat played by Buster Keaton
* Cousin Delightful played by Billie Seward
* Wendy Wilecat played by Kay Sutton
* Granny Scraggs played by Maude Eburne

==See also==
* Al Capp
* Dogpatch
* Dogpatch USA
* Lil Abner
* Sadie Hawkins Day Salomey

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 


 