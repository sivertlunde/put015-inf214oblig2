South of St. Louis
{{Infobox film
| name = South of St. Louis
| image = 
| caption = 
| director = Ray Enright
| producer = Milton Sperling Douglas Kennedy
| story =  James R. Webb
| music = Max Steiner
| cinematography = Karl Freund
| editing = Clarence Kolster
| distributor = Warner Bros.
| released =   
| runtime = 88 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
 Western film Civil War army deserters. 

==Plot== Douglas Kennedy), are run out of town by the guerilla raider and Union Army leader Luke Cottrell, who burns down their ranch. Though Kips fiancee, Deb (Dorothy Malone) begs them to stay in the small Texas town of Edenton, the three ranch owners vow vengeance on the Cottrell and decide to head south to find him. When they get to Brownsville, Texas, Lee decides to join the Confederate Army, while Kip and Charlie attempt to rebuild Three Bell ranch. Before they do, however, they take on offer from an attractive local lounge singer, Rouge de Lisle (Alexis Smith), to transport a box of furniture for fifty dollars. It turns out, however, that the box is instead filled with an illegal shipment of firearms and Kip is subsequently arrested. Before he is punished, however, he is freed and is picked up by Rouge, who offers him a job gun-running for the Confederacy. He accepts the offer, hoping to get enough money smuggling to rebuild the ranch. The trio then hires a group of gunmen, one of which is Slim Hansen, who used to work for Cottrell, and heads to Matamoros, Mexico to pick up a shipment of guns for the Confederacy.

As they attempt to cross the border, they run into Cottrell and his gang. In the ensuing gunfight, Kip and Charlie are saved by a company of Confederate soldiers led by Lee, one of which is Lee. The three return to Edenton, where Kips fiancee again attempts to convince him to stay. Kip is determined, however, to get enough money to restart his farm and instead continues to smuggle guns. When Brownsville is finally captured by rebel soldiers, the three must decide what the next course of action is. Lee continues to fight with the Confederate army, Kip wants to restore Three Bell ranch, and Charlie, more interested in the money, opts to continue smuggling guns. When Deborah refuses to leave her duties as a nurse to join him at the ranch, Kip decides to go with Charlie and return to smuggling. This pleases Rouge, who has fallen in love with Kip.

As they near the border, they get word that Cottrell has threatened to kill Charlie and Kip if they return to Matamoros. At Slims suggestion, they decide to steal the shipment of guns before it even reaches Mexico. The men dress up as Union soldiers in order to steal the guns, but then run into Confederate soldiers who confuse them for the enemy and open fire. Lee, who suspects that Kip and Charlie are behind the attack, breaks off all connections with his two former friends.

When Cottrell kills one of Kips men, Kip resolves that he must kill him. Before he can, however, Slim warns Cottrell, who ambushes Kip. Kip avoids the ambush, but Cottrell is killed by Slim before he is able to tell Kip about Slims treachery. When Kip returns to Edenton, he finds out that his fiancee has fallen for Lee. Realizing that he has lost both of his closest friends and his fiancee, he leaves with Rouge to Matamoros.

After the war is over, Lee joins the Texas Rangers. While serving in Brownsville, he is threatened by his former friend, Charlie. Hoping to resolve the situation, Deb goes to Matamoros to ask Kip for help. With the help of Rouge, Deb convinces Kip to go to Brownsville. Kip gets there just in time to stop the fight, but Charlie is shot by the treacherous Slim. With Charlie dying in his arms, Kip promises Charlie that he will rebuild the Three Bell ranch.

==Cast==
* Joel McCrea as Kip Davis
* Zachary Scott as Charlie Burns Douglas Kennedy as Lee Price Art Smith as Bronco
* Victor Jory as Luke Cottrell
* Alexis Smith as Rouge de Lisle
* Nacho Galindo as Manuel
* Dorothy Malone as Deborah Miller
* Alan Hale, Sr. as Jake Everts Bob Steele as Slim Hansen
* Monte Blue as Captain Jeffery

==References==
 

==External links==
*   at the Internet Movie Database
*   at Rotten Tomatoes

 

 
 
 
 
 