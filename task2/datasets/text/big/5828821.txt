Gendarme in New York
{{Infobox film
| name = The Gendarme in New York
| image =
| director = Jean Girault
| producer = SNC Champion Film
| writer = Richard Balducci Jean Girault Jacques Vilfrid
| starring = Louis de Funès Geneviève Grad Michel Galabru Jean Lefebvre Christian Marin
| music = Raymond Lefèvre Paul Mauriat
| cinematography = 
| editing = 
| distributor = SNC
| released = October 29, 1965 (USA)  (  ago) 
| runtime = 100 minutes
| country =United States France Italy
| language = French
}}
Gendarme in New York ( ) is the sequel to the French comedy film Le gendarme de Saint-Tropez. It stars Louis de Funès as the gendarme. With Michel Galabru, Christian Marin, Grosso and Modo, Alan Scott.

==Plot==
 SS France and travels to America as a stowaway. During the journey Cruchot sees her hiding among the lifeboats but his captain convinces him that he is imagining things.
 Italian gendarme who already tried to win her over while on the ship. The Italian takes her to his relatives soon after Cruchot who has seen her performance on TV chases her out of the YWCA hotel and gets arrested.
 West Side Sicilians who believe that Nicole was kidnapped. Cruchot and Nicole manage to escape by hiding in Chinatown and dressing as local Chinese couple.

Meanwhile the Italian gendarme sollicits the help of the NYPD and other gendarmes present at the congress to help him find his lost love. Cruchot transports Nicole to the airport by taxi in a luggage chest. He fails to arrive because of a small traffic incident and is forced to release Nicole from her confinement. Through a series of incidents Cruchot and Nicole manage to evade the NYPD and Cruchots captain at a construction site and return to their abandoned taxi, which takes them to the airport.

At the airport Cruchot agrees to meet Nicole in the bar. When he arrives there she meets him dressed as an Air France flight attendant. She places him before a choice: either take the plane she will be flying on, thus risking discovery by the captain or make sure that they are late for their flight. Cruchot sabotages the gendarmes luggage making them late for the flight, while the captain sees a girl resembling Nicole fumbling with the airplane door. Cruchot manages to convince him that he is imagining things.

The movie cuts to Saint Tropez where the gendarmes are welcomed by the townsfolk, their wives and Nicole. The movie ends with the captain discovering that Nicole is wearing a dress that Cruchot bought for her in America. He confronts Cruchot with his insubordination.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 