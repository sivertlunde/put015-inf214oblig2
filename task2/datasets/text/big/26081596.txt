Run Wild, Run Free
{{Infobox film
| name           = Run Wild, Run Free
| image          = Run Wild, Run Free.jpg
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Richard C. Sarafian
| producer       = Andrew Donally John Danischewsky Monja Danischewsky
| based on       =  
| writer         = 
| narrator       =
| starring       = John Mills Mark Lester David Whitaker
| cinematography = Wilkie Cooper
| editing        = Geoffrey Foot
| studio         = Meadway
| distributor    = Columbia Pictures
| released       = 23 July 1969 (USA)
| runtime        = 100 minutes
| country        = United Kingdom English
| budget         =
| gross          =
}}
 1969 film directed by Richard C. Sarafian. The film was written by David Rook, based on his novel The White Colt. The film was shot on location in Dartmoor, Devon, England. 

==Plot==
"Philip (Mark Lester) is a troubled autistic 10-year-old boy who has been fleeing the confines of his familys home since babyhood. Like some wild animal, Philip refuses to be penned up. Even more frustrating for his devoted mother and more irritable father, Philip has refused to speak since the age of three. Run Wild, Run Free (1969), directed by Richard C. Sarafian, is occasionally graced with experimental, art film touches, as when, at one point the internal thoughts of Philips mother (Sylvia Syms) describing her fatigue and inability to love her son can be heard as voice-over as they drive to a therapist appointment.

Out roaming the moors, Philip encounters the kindly, nature-loving retired Colonel (John Mills) who is deeply sympathetic to the boys plight. Like the nature that surrounds him -- the film was shot on location in Dartmoor, Devon, England -- the boy is a creature of mute-impulse who must be patiently tamed and drawn out just like the animals he encounters.

The Colonel introduces Philip to the wonders of the moors: the newly hatched birds whose nests are tucked in tree boughs and the copious bugs crawling under the peat. A world opens up to Philip that expands triple fold when he makes the acquaintance of a wild blue-eyed white colt grazing on the moor. The child forms a deep bond to the animal, a creature that seems to understand him like no other. The experience is transformative, until the horse runs away and Philip becomes distraught. He is distracted by a pet kestrel, Lady, given to him by a farm girl neighbour, Diana (Fiona Fullerton). Together Philip, the Colonel and Diana train the bird, reveling in its progress and ability to fly to them. When the bird is horribly injured through Philips carelessness, all of the progress the Colonel has made seems for naught.

But the bird recovers, the white horse returns and the Colonel teaches Philip to ride. When Diana and Philip are out riding one night and become lost on the foggy moor, it seems possible that this could be their last adventure amidst the wild forces of nature." 

==Cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| John Mills || The Moorman (Colonel)
|-
| Mark Lester|| Phillip Ransome
|- Gordon Jackson || Mr. Ransome
|-
| Fiona Fullerton || Diana
|-
| Sylvia Syms || Mrs. Ransome
|-
| Bernard Miles || Reg
|-
| Paul Griffiths|| Young Phillip Ransome
|}

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 