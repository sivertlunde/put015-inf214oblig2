The First Traveling Saleslady
{{Infobox film
| name           = The First Traveling Saleslady
| image          = Poster of the movie "The First Traveling Saleslady".jpg
| caption        = Film poster
| director       = Arthur Lubin
| producer       = Arthur Lubin
| writer         = Devery Freeman Stephen Longstreet
| narrator       =
| starring       = Ginger Rogers Carol Channing Barry Nelson
| music          = Irving Gertz William E. Snyder Otto Ludwig RKO Radio Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       =
| budget         =
}}

The First Traveling Saleslady was a 1956 American film, starring Ginger Rogers and Carol Channing.   Commercially unsuccessful, it was among the films that helped to close RKO Pictures.

Future western icons Clint Eastwood and James Arness have supporting roles in the film.

==Plot==
Corset company owner and independent-thinking suffragette Rose Gillray has her wagon struck by a new-fangled horseless carriage in 1897 New York. This so-called automobile is the proud new possession of Charlie Masters, who tells her its the transportation means of the future.

At work, Rose is helping singer Molly Wade into a boldly designed new corset when she gets the idea of that being Mollys costume on stage. The show is raided by police.

Rose owes money to Jim Carter, whose steel business manufactures the metal used for a corsets stays. Jim takes a shine to Rose and offers her a chance to sell his surplus of barbed wire, which is going out of fashion out west because its gaining a reputation as life-threatening to livestock.

Ending up in Kansas City, accompanied by Molly and followed by Charlie, a cattlemens association convention seems a good place to try to sell her goods. But cattle rancher Joel Kingdon gives her the runaround, attracted to her personally but warning her against peddling wire. She tries his home state of Texas next, but once again, Joel interferes, putting the women out of business temporarily and knocking Charlie cold.

Joel and Jim both end up in love with Rose and proposing marriage, but she rejects both. Charlie, though, comes along offering a ride to California, where hes got another new notion that he wants to explore: machines that fly.

==Cast==
* Ginger Rogers as Rose Gillray
* Carol Channing as Molly Wade
* Barry Nelson as Charles Masters
* David Brian as James Carter
* James Arness as Joel Kingdom
* Clint Eastwood as Lt. Rice

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 