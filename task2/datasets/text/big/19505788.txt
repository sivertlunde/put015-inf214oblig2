The Richest Girl in the World (1958 film)
{{Infobox film
| name           = The Richest Girl in the World
| image          = Verdens rigeste pige.jpg
| caption        = 
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       =  Walter Ellis  Børge Müller
| starring       = Poul Reichhardt
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw
| distributor    = 
| released       = 31 July 1958
| runtime        = 110 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

The Richest Girl in the World ( ) is a 1958 Danish comedy film directed by Lau Lauritzen, Jr. and Alice OFredericks. It marked the film debut of the already popular singing duo Nina & Frederik, with Nina as the title character, and Frederik as the handsome but poor calypso singer she falls in love with.

==Cast==
* Poul Reichhardt - John
* Nina Van Pallandt - Lisa Hoffman
* Frederik van Pallandt - Jacques
* Birgitte Bruun - Judy
* Gunnar Lauring - Onkel Toby
* Jessie Rindom - Clara
* Jeanne Darville - Elisabeth
* Asbjørn Andersen - Hoffmann
* Else-Marie (actor)|Else-Marie - Frk. Bartholin
* Paul Hagen - Journalist Lund
* Johannes Marott - Pressefotograf Smith
* Christa Rasmusen - Johns sekretær
* Christa Rasmussen - Johns sekretær
* Judy Gringer - Omstillingsdame
* Alfred Wilken - Severinsen
* Grethe Mogensen

==External links==
* 

 
 

 
 
 
 
 
 
 

 
 