The Lady of the Harem
{{Infobox film
| name           = The Lady of the Harem
| image          = 
| alt            = 
| caption        = 
| director       = Raoul Walsh
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = James Elroy Flecker James T. ODonohoe
| starring       = Ernest Torrence William Collier, Jr. Greta Nissen Louise Fazenda George Beranger Sôjin Kamiyama Frank Leigh
| music          = 
| cinematography = Victor Milner
| editing        =  
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 adventure silent film directed by Raoul Walsh and written by James Elroy Flecker and James T. ODonohoe. The film stars Ernest Torrence, William Collier, Jr., Greta Nissen, Louise Fazenda, George Beranger, Sôjin Kamiyama and Frank Leigh. The film was released on November 1, 1926, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Ernest Torrence as Hassan
*William Collier, Jr. as Rafi
*Greta Nissen as Pervaneh
*Louise Fazenda as Yasmin
*George Beranger as	Selim 
*Sôjin Kamiyama as Sultan 
*Frank Leigh as Jafar
*Noble Johnson as Tax Collector
*Daniel Makarenko as Chief of Police
*Christian J. Frank	as Captain of the Military
*Snitz Edwards as Abdu
*Chester Conklin as Ali
*Brandon Hurst as Beggar
*Leo White as Beggar

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 