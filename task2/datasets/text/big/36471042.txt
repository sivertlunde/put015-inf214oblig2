Let's Make a Movie
{{Infobox film
| name           = Lets Make a Movie
| image          = Official_Film_Poster.jpg
| border         = yes
| caption        = 
| director       = Elana Mugdan
| producer       = Usher Morgan
| starring       = Hallie York Johnathan Fernandez Brian Cheng  Eric James Eastman
| cinematography = Dave Dodds
| editing        = Ricky Monroe
| distributor    = Digital Magic Entertainment
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Lets Make a Movie, originally titled Directors Cut is a 2010 American independent comedy-drama film and the directorial debut of Elana Mugdan. The film was financed by Mugdan in 2010 and was never released to the general public. The film was purchased by Digital Magic Entertainment and was re-released under its new title on December 14, 2012. Lets Make a Movie won Best Comedy at the NYC Downtown Feature Film Festival in 2011,  the Indie Spirit Award in the Boston International Film Festival  and the Indie Spirit Award at the New Hope Film Festival in 2011. 

==Plot==
Lets Make a Movie is the story of Cassie Thompson (played by York), a college dropout and ex-film student who is tired of being disrespected and downtrodden. In a subconscious effort to turn her life around, she decides to make a movie. The only problem is that she has no money, and her cast and crew are neurotic and inexperienced.  

==Production==
Elana Mugdan shot the film under the title, Directors Cut, in Little Neck as well as in several Long Island and Manhattan locales in February 2010. The film was said to be autobiographical and draws upon the films that Mugdan made as a child with a home video camera.  Mugdan wrote the screenplay for the film in August 2009 and shot it with a $30,000 budget in winter 2010, wrapping up principal photography by May.  The film premiered in December 2010 at the Hollywood Reel Independent Film Festival and was later accepted to other festivals across the nation, winning a total of 4 awards.  The official trailer was released on September 15, 2012  

==Release==
Producer Usher Morgan announced the acquisition of the film in June 2012. Morgan re-cut, renamed and re-released the film. The film had a limited release in New York City on December 14, 2012. It was produced and distributed by Digital Magic Entertainment under its new title, Lets Make a Movie.

==Media Attention and Critical reception==

The film received many early reviews and media attention by bloggers, independent filmmakers and websites promoting low budget, independent filmmaking, in addition to local newspapers and magazines.    Movie critic Mark Bell of Film Threat wrote, "Lets Make a Movie captures the innocence of filmmaking at that stage when youve just got to film something, do anything, or else you feel like youre going to be consumed by a life spent doing something you never wanted to do." Despite the positive review, Film Threat gave the film only 2 out of 5 stars.   Felix Vasquez Jr. of Cinema Crazed Magazine praised the film, and wrote "and in the end Lets Make a Movie is a charming and entertaining indie film about making indie films", the magazine gave the film 3 out of 3 stars.  Catherine Tosko, the editor for   Magazine wrote "In my opinion, this film should be given out at film school. First time film students should be made to watch it as part of their syllabus", the magazine gave the film 3.5 out of 5 stars. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 