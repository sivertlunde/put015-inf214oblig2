The Debt (2010 film)
{{Infobox film
| name           = The Debt 
| image          = The Debt Poster.jpg
| caption        = Theatrical release poster
| alt            = John Madden
| producer       = Matthew Vaughn Kris Thykier
| screenplay     = Matthew Vaughn Jane Goldman Peter Straughan
| based on       = The Debt (2007 film)|Ha-Hov by Assaf Bernstein Ido Rosenblum
| starring       = Helen Mirren Sam Worthington Jessica Chastain Jesper Christensen Marton Csokas Ciarán Hinds Tom Wilkinson
| music          = Thomas Newman Ben Davis
| editing        = Alexander Berner
| studio         = Marv Films Miramax Films
| distributor    =    
| released       =  
| runtime        = 113 minutes 
| country        = United Kingdom United States
| language       = English, German, Russian
| budget         = $20 million   
| gross          = $45.6 million 
}}

The Debt is a 2010   from a screenplay by Matthew Vaughn, Jane Goldman and Peter Straughan. It stars Helen Mirren, Sam Worthington, Jessica Chastain, Ciarán Hinds, Tom Wilkinson, Marton Csokas and Jesper Christensen.

Although ready for release already in July 2010,   Linked 2014-06-11  and scheduled for a December 2010 release in the United States,   Retrieved February 19, 2011  the film only toured various film festivals during the autumn of 2010 and spring of 2011. It didnt see a general release until it was released in France on 15 June 2011, followed by Kazakhstan and Russia in July 2011, and United States, Canada and India on 31 August 2011.   Linked 2014-06-11 

==Plot== Nazi war war criminal medical experiments on Jews during World War II—and bring him to Israel to face justice.

Rachel and David present themselves as an ethnic German married couple from Argentina and Rachel plants herself as a patient at Vogels obstetrics and gynaecology clinic. 

Both Stefan and David develop an attraction to Rachel. Rachel shows a preference for David, yet sleeps with Stefan shortly after David rebukes her advances. This tryst leads to Rachel becoming pregnant. Stefan also reveals to Rachel that David lost his entire family in The Holocaust. 
 Wollankstrasse Station, East German guards to their presence. In the ensuing shootout, David sacrifices his chance to escape in order to collect the compromised Rachel. The agents are left with no choice but to bring Vogel to their apartment and plan a new extraction.

The agents take turns monitoring and feeding Vogel, who attempts to psychologically humiliate and intimidate them. During his shift, David becomes violently enraged after Vogel explains his beliefs that Jews have many weaknesses, such as selfishness, making them easily subdued. David smashes a glass plate over Vogels head and repeatedly beats him, only to be stopped and restrained by Stefan. While Rachel is in charge of monitoring, Vogel manages to cut through his bonds using a shard of the broken plate and ambushes Rachel with the shard, leaving her with a permanent scar on her face. He then escapes into the night as the agents are left to assess their failure. 

Panicking and hoping to save face for both himself and for Israel, Stefan convinces Rachel and David to go along with the fiction that Vogel was killed. They agree to lie and use the cover story that Rachel shot and killed Vogel as he attempted to flee.

In the following years, the agents become venerated as national heroes for their roles in the mission. During a party in 1970, at the home of Rachel and Stefan (now married), Rachel confesses to David her distaste with her current life; Stefan puts his career and social status ahead of her while also punishing her for not loving him and having feelings for David. David admits his intention to leave Mossad and the country, imploring Rachel to come with him. Rachel cannot bring herself to abandon her daughter (the result of Rachel and Stefans time together in hiding in East Germany) and she and David part ways.
	
In 1997, Rachel (Helen Mirren) is honoured by her daughter Sarah (Romi Aboulafia) during a release party in Tel Aviv for Sarahs book based on the account Rachel, Stefan and David gave of the events in 1965. Concurrently, David (Ciarán Hinds) is escorted from his apartment by an Israeli government agent for a debriefing. David recognises Stefan (Tom Wilkinson) waiting in another vehicle and commits suicide by stepping in front of an oncoming truck.

At a dinner after their daughters book release party, Stefan takes Rachel aside to set a meeting to discuss new information he has obtained. Later, at Davids flat, Stefan provides evidence that Vogel now resides at an insane asylum in Ukraine, and is soon scheduled to be interviewed by a local journalist. 

David had been investigating the man at Stefans request and, according to Stefan, killed himself out of fear that the lie would be exposed. Rachel refutes Stefans explanation, recalling an encounter with David a day before his suicide, in which he revealed his shame about the lie and disclosed that he had spent years unsuccessfully searching the world for Vogel. He was further disheartened by Rachel’s admission that she would continue propagating the lie to protect those closest to her, particularly her daughter.

Nevertheless, Rachel finally feels compelled to travel to Kiev, where she investigates the journalists lead and identifies the asylum. She reaches the room just minutes before the journalist and discovers the man claiming to be Vogel is an impostor, a senile old man who apparently fancies the notoriety. Describing the encounter to Stefan over the phone, Rachel declares she will not continue to lie about the 1965 mission. She leaves a note for the journalist and prepares to leave, but suddenly spots Vogel (in his 80s by now) among the other patients and follows him to an isolated area of the hospital. 

After a confrontation in which Vogel stabs her twice with scissors, Rachel kills Vogel by plunging a poisoned syringe into his back. As she limps from the asylum, Rachels note is discovered and read by the journalist. It describes the truth of the mission, ready to be relayed to the world.

==Main cast==
* Helen Mirren as Rachel Singer in 1997
* Jessica Chastain as Rachel Singer in 1965 and 1970
* Ciarán Hinds as David Peretz in 1997
* Sam Worthington as David Peretz in 1965 and 1970
* Tom Wilkinson as Stefan Gold in 1997
* Marton Csokas as Stefan Gold in 1965 and 1970
* Jesper Christensen as Dieter Vogel
* Romi Aboulafia as Sarah Gold, daughter of Stefan and Rachel

==Production==
Israeli papers reported that Mirren was "immersing herself" in studies of the Hebrew language, Jewish history and Holocaust writings, including the life of Simon Wiesenthal, while spending time in Israel in 2009 to shoot scenes in the film.  "My character is carrying the memory, anger and passion of the Holocaust," she said. 

==Release==
The film premiered at the Deauville American Film Festival in France on 4 September 2010,  followed by 2010 Toronto International Film Festival on 14 September 2010,   and various other festivals during the autumn of 2010 and spring of 2011. 
 Miramax had Disney and the new owner Filmyard Holdings|Filmyard. 

The film saw its first general release in France on 15 June 2011, followed by Kazakhstan and Russia in July 2011, and United States, Canada and India on 31 August 2011. 

==Critical reception== Films in Review said of the film, "The twists are shocking and mesmerizing. A high wire, intelligent espionage thriller. It is one of the best movies of 2011."  

Roger Ebert of the Chicago Sun-Times gave the film   stars out of four. He said, "Maybe the problem is a structure that cuts around in time. Three characters, six actors, and although the woman is always presumably Rachel, I was sometimes asking myself which of the two men I was seeing when younger. In a thriller, you must be sure. I suspect this film would have been more effective if it had remained entirely in the past, especially given all we know." 

==Differences from the original film== of the same name (known as Ha-Hov or HaChov in Hebrew). Differences between the Israeli film and the remake include:
* The main female character, Rachel Berner, was renamed from Rachel Singer; her fellow agents, Ehud and Zvi, were renamed David and Stefan; and the Nazi war criminal Max Rainer was renamed Dieter Vogel.
* The love triangle between Rachel and the other two agents is given much greater focus in the remake.
* In the remake, Rachel married and later divorced Stefan, while in the original Rachel had no special relationship with either man after the operation occurred.
* In the original film, the book about the groups exploits -- whose release party is seen at the beginning of the film -- was written by Rachel herself, while in the remake it was authored by the daughter Rachel conceived with Stefan while on the mission in East Berlin.
* The remake has an extended scene, not in the original, where an attempt to transport the abducted Vogel out of East Berlin is botched.
* In the original film, Rachel travels with Ehud to Ukraine, but must complete the mission alone after he suffers from cowardice; in the remake, Rachel goes to Ukraine by herself because David (Ehud) has killed himself.

==See also==
*The Debt (2007 film)|The Debt (2007 film)

== References ==
 

==External links==
 
*  
*  
*   at The Numbers
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 