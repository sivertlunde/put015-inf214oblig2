Mr. Fraud
{{Infobox film
| name           = Mr. Fraud
| image          = Mr. Fraud.jpg
| caption        = Theatrical release poster
| director       = B. Unnikrishnan
| producer       = A.V._Anoop|A. V. Anoop
| writer         = B. Unnikrishnan
| starring       = Mohanlal Dev Gill Vijay Babu Miya George Manjari Phadnis Pallavi Purohit
| music          = Gopi Sunder
| cinematography = Satheesh Kurup
| editing        = Manoj
| studio         = Medimix (soap)|A.V.A Productions Maxlab Entertainments PJ Entertainments  (Overseas) 
| released       =   (India)    (Overseas) 
| runtime        = 138 minutes
| country        = India
| language       = Malayalam
| budget         =   
}}

Mr. Fraud is a 2014 Malayalam action thriller-heist film written and directed by B. Unnikrishnan and starring Mohanlal, Dev Gill, Vijay Babu, Miya George, Manjari Phadnis and Pallavi Purohit.  The film is produced by A.V._Anoop|A. V. Anoop under the banner A.V.A Productions. The film is about a conman whose task is to plunder the treasure from a royal household.

The films soundtrack and background score were composed by Gopi Sunder, with lyrics penned by Hari Narayanan and Chittoor Gopi. Cinematography is handled by Satheesh Kurup, editing is by Manoj and stunt choreography is by Stunt Silva.  The film released on 17 May.  Upon release the film received mixed reviews from critics. 

==Plot==
  Suresh Krishna) Sai Kumar). They appoint a private consultant named Sivaram from Jaipur to assess the value of the treasure but before he reaches there Bhai Ji kidnaps him and he goes to Rajashekara Varmas house disguised as Sivaram sporting a salt and pepper look. Rajashekara Varma is not in good terms with his daughters and instead lives separately with his adopted daughter Saraswathi(Mia George). Rajashekara Varma takes Bhai Ji to the royal palace where he meets the other family members. Later the cellar is opened and Bhai Ji begins assessing the value of the treasure. He realizes that this is the most risky mission he has ever attempted. Meanwhile Rajashekhara Varma is attacked by some goons and before they could kill him Bhai Ji makes a dramatic entry into the scene,fights with the goons and saves him. Rajashekhara Varma thanks Bhai Ji for saving his life but Sajan and Saraswathy have a suspicion. Soon Nikki and Javed arrives along with the real Sivaram and meets Bhai Ji. He gives them video clippings of the treasures he shot and tells Nikki to make a replica of it. Nikki agrees and leaves. Meanwhile Rajashekar has become friends with Bhai Ji and tells about Sarawathy and shows him the school run by her which is still under construction. Bhai Ji and his assistants decide to find out a way to take the treasure from the cellar. He guesses that there should be a secret path behind the cellar that would lead to another exit after studying the architecture of several other palaces. In the night the three of them go out to find the door. Bhai Ji enters the cellar and using a metal detector finds the passage and the exit point. Before leaving out of the cellar he talks to the idol and tells that now there is a secret between them which is also known to someone else. Sajan accidentally tells the other family members regarding Saraswathys past which only he and Rajashekara Varma knew. Saraswathy was raped by unknown people when she was only 12 years and Rajashekhara Varma had adopted her after that incident.He even instructed Sajan who was trying to find the culprits to close the case.Rajashekhara Varmas daughters and their husbands threaten him to withdraw the case he had filed or else they would file another case to re-investigate the issue. An infuriated Rajashekhara Varma starts to hit Sajan and Bhai Ji intervenes. Sajan narrates the incident to Bhai Ji. Nearly 15 years back he was chasing a thief who had robbed a bank and on the way he encountered an unconscious Saraswathy. He takes her to the hospital and informs Rajashekhara Varma who was the local MLA. Though they try to find the culprits they dont succeed. So Rajashekhara Varma tells Sajan not to charge any case as it would affect her future and Rajashekhara Varma adopts her. Sajan also tells Bhai Ji that though he had a suspicion on him he is now convinced that Bhai Ji is innocent and that he has found out the people who were behind the attack on Rajashekhara Varma and that he was planning to meet them at night when a party will be conducted at the palace. Bhai Ji tells that it is dangerous for Sajan to go alone and he would accompany him but Sajan refuses Bhai Jis help. Saraswathy comes to the palace along with Rajashekhara Varma and tells the family members that they can do whatever they want but her father wouldnt withdraw the case. Shekara Varma comes in defense of Saraswathi and promises her that no one will harm her. Bhai Ji is disturbed and when asked by Abbas and Priya he tells them that he was the thief that Sajan was chasing and on the way he saw three people chasing Saraswathy. He extended his hand to help her while he was driving but she missed it. He became confused whether to stop and save the girl or run away as the police was right behind him. He decided to movie away from there to avoid getting caught. He tells them that from that day the incident used to haunt him and he was shocked to see the girl again. That night after the party Nikki and gang arrive with the duplicate treasure and they begin the heist. Bhai Ji enters the cellar through the secret tunnel and Nikki places a GPS inside the boxes to make sure that Bhai Ji doesnt cheat them. After taking the treasure out Nikki informs Bhai Ji that the final transaction will be in Jaipur and Abbas can come with them so that Bhai Ji can trust them. The next day Bhai Ji submits his evaluation report and before he leaves Police arrive there and informs them that Sajan has been murdered and arrest Rajashekhara Varma as he is the main suspect. Bhai Ji leaves for Jaipur and arrives at the place in a stylish look.There Javed and Nikki tells him that they are not going to pay him anything and he should return the 250cr back if he wants Abbas back. Bhai Ji tells them that a few more people have to come there and tells them that he has brought the 3rd party(unknown entity) that teamed up with Nikki and hired him for the heist. The unknown entity happens to be Shekhara Varma and his son Sudhakara Varma who was captured by Bhai Jis man Bittoo(Stunt Silva) when they came to Jaipur for the final transaction. Bhai Ji tells Shekhara Varma that he knows well about him and he is not a simple person like others think but a cruel and wicked person.He tells him that it was the goddess which told him the truth. When Bhai Ji was searching for the door to a secret tunnel inside the cellar he finds an old bullet stuck on the walls and he realized how the prism broke. During the fight when Vasudeva Varma was going to strike Shekara Varma with a sword he secretly had a gun with which he shot at the prism. Mahendra Varma found out about his sons evilness and lied to others about the goddess wrath and locked the cellar as a punishment for Shekhara Varma. Though he was happy with getting one-fourth of the treasure he was upset when his brother Rajashekhara Varma filed a case and this prompted him to join with Nikki and plan a heist. Even he was the one who planned to kill Rajashekhara Varma so that if he dies Vasudeva Varma and Sri Krishna Varma would become the suspects and they will begin to distrust each other while Shekhara Varma can move forward with the case and extend the time for a heist. Bhai Ji knows that it was Sudhakara Varma who killed Sajan and Abbas had secretly shot the murder in his phone upon Bhai Jis instruction and the visuals have been sent to the Police.A fight starts and after some time Abbas has Nikki at his gunpoint after Bhai Ji defeats him. Javed begs for mercy and Bhai Ji tells him to take the van having the treasure to a place along with Sudhakara Varma and Sivaram. After they leave he tells Nikki ans Shekhara Varma that Abbas had planted a bomb in the Van and presses the button triggering a blast which kills the three of them and he shoots Nikki and kills him. Bhai leaves a gun back telling Shekara Varma that it was destiny that brought him there for the job and he had never stolen the treasure.Instead he exchanged the treasure between the boxes along with the boxes and that he got 250cr for doing nothing. He also leaves telling that Shekhara Varma can choose his punishment. Shekara Varma commits suicide by shooting himself. 2 years later the construction of Saraswathys school is completed and Bhai Ji visits Saraswathy and Rajashekhara Varma. They thank him for all the help he did like funding for the school, helping Rajashekhara Varma to win the case and also saving him from Sajan murder case. Bhai Ji confesses to Sarawathy that he was the man who had offered his hand to save her but couldnt do it and he tells them that this is his only confession. When Saraswathy asks him his real name he tells her that she can call him Mr. Fraud.

==Cast==
 
*Mohanlal as Bhai Ji / John Cliff / Sivaram / Mr. Fraud
*Dev Gill as Nikhil Adharva / Nikki
*Vijay Babu as Abbas
*Mia George as Saraswathi
*Manjari Phadnis as Priya
*Pallavi Purohit as Damini Varma Siddique as Rajashekhara Varma Sai Kumar as Intelligence DySP Sajan Vijayakumar as Shekara Varma
*Rahul Madhav as Sudhakara Varma
*P. Balachandran as Vasudeva Varma Devan as Sri Krishna Varma Suresh Krishna as Thrivikrama Varma
*Rajeev Parameshwar as Prathap Varma
*Ashvin Matthew as Javed
*Balaji as Shivaram
*Mukundan
*Manoj
*V. K. Sreeraman as Ravindra Varma
*Kalasala Babu as Mahendra Varma
*Sathaar
*Balachandran Chullikkadu as Advocate Idiculla
*Biju Pappan
*Amritha Anilkumar As Young Saraswathi
*Gopi Sunder As Himself (cameo appearance)
*Balabhaskar As Himself (cameo appearance)
*Stunt Silva As Bittoo (cameo appearance)
 

==Production==

Though delayed for a while due to Mohanlals Ayurvedic treatment, the film commenced its production on 19 February 2014.   The film was shot mainly from Ottappalam and Kochi in two schedules.  The climax scenes were shot from Mumbai and Jaipur. The shooting of the film was wrapped on the day of Vishu festival (April 15) in Jaipur. 

==Release==
The film was slated to be released on May 8, 2014. However, the Film Exhibitors Federation imposed a ban on the film in protest against B. Unnikrishnans alleged instructions to the Film Employees Federation of Kerala (FEFKA) and Association of Malayalam Movie Artists (AMMA) representatives not to participate in the inauguration function of the federation’s new office building.  The issue reached a flash point with the FEFKA saying that no other Malayalam film will be released in cinemas refusing to screen Mr. Fraud.   Finally, Exhibitors Federation allowed to release Mr. Fraud, but imposed a blanket ban on B. Unnikrishnan. Liberty Basheer, the president of Exhibitors Federation said in a statement: "The federation had announced the ban on the film without any prior intimation and we dont want the distributor or the producer to incur any losses because of this. So the Mohanlal-starrer will hit the theatres on the said date. But we wont exhibit any film that Unnikrishnan plans to write, direct or produce in the future in Kerala." 

There were allegations that the controversy over the ban on the film had been a marketing strategy which director B. Unnikrishnan called "baseless". 

The film released on 17 May.  An Android app was developed to publicise details of the film. 

==Reception==

===Critical response===
The film received mixed reviews from critics. Sangeetha Seshagiri  (18 May 2014).  . International Business Times. Retrieved 19 May 2014. 

Aswin J. Kumar from   rated the film 2 in a scale of 5 and concluded that "only the hardcore Mohanlal fans will like Mr Fraud which is otherwise an uninteresting film." 

Chandrakanth Viswanath of The New Indian Express described the film as "a fine attempt in a novel genre" and concluded his review saying: "Carved in the popular format, with enough action-packed sequences and convincing performances, coupled with technical finesse, Mr Fraud has the potential to win hearts of die-hard Mohanlal fans and thrill junkies alike." 

==Soundtrack==
{{Infobox album
| Name = Mr. Fraud
| Artist = Gopi Sunder
| Type = Soundtrack
| caption =
| Cover =
| Released =  
| Recorded = 2013
| Genre = Film soundtrack
| Length =  
| Language = Malayalam
| Label = Muzik 24x7
| Producer = Gopi Sunder How Old Are You (2014)
| This album = Mr. Fraud (2014)
| Next album = Bangalore Days (2014)
}} viral in music sharing websites. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 20:14
| lyrics_credits = yes
| title1 = Poonthinkale
| lyrics1 = Chittor Gopi
| extra1 = Shankar Mahadevan, Shakthisree Gopalan
| length1 = 5:02
| title2 = Khuda Woh Khuda
| lyrics2 = Harinarayanan
| extra2 = Shankar Mahadevan
| length2 = 6:45
| title3 = Sadaa Paalaya
| lyrics3 = G. N. Balasubramaniam Sithara
| length3 = 5:12
| title4 = Mr.Fraud Theme 
| lyrics4 = Gopi Sunder
| extra4 = Gopi Sunder, San Jaimt
| length4 = 3:15
}}

==References==
 

==External links==
*  
* 

 
 
 
 
 
 