Stormy Waters (film)
{{Infobox Film
| name           = Remorques
| image          = 1941 Remorques.jpg
| caption        = Theatrical poster
| director       = Jean Grémillon
| producer       = 
| writer         = Jacques Prévert (scenario & dialogue)  André Cayatte (adaptation)
| starring       = Jean Gabin  Madeleine Renaud  Michèle Morgan
| music          = Alexis Roland-Manuel
| cinematography = Armand Thirard
| editing        = Yvonne Martin
| distributor    = Metro-Goldwyn-Mayer (US)
| released       = 27 November 1941 15 June 1946 (US)
| runtime        = 81 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Remorques (English title: Stormy Waters) is a 1941 French drama film directed by Jean Grémillon. The screenplay was written by Jacques Prévert (scenario & dialogue) and André Cayatte (adaptation), based on the novel by Roger Vercel. The film stars Jean Gabin, Madeleine Renaud and Michèle Morgan.  

==Plot==
A tugboat captain indulges a love affair with a married woman, despite having a seriously ill wife in a small ocean front town.

==Cast==
* Jean Gabin as Captain André Laurent 
* Madeleine Renaud as Yvonne Laurent 
* Michèle Morgan as Catherine 
* Charles Blavette as Gabriel Tanguy 
* Jean Marchat as Marc, captain of the Mirva 
* Fernand Ledoux as Kerlo, the boatswain

==References==
 

==External links==
*   at Films de France
*  
*  
*   at FilmCan 

 

 
 
 
 
 
 
 
 
 
 

 