Brothers of the West
 

{{Infobox film
| name           = Brothers of the West
| image_size     =
| image	=	Brothers of the West FilmPoster.jpeg
| caption        =
| director       = Sam Katzman
| producer       = Sam Katzman (producer)
| writer         = Basil Dickey
| narrator       =
| starring       = See below
| music          =
| cinematography = William Hyer
| editing        = Holbrook N. Todd
| studio         =
| distributor    =
| released       = 1937
| runtime        = 58 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Brothers of the West is a 1937 American film directed by Sam Katzman.

== Plot summary ==
 

== Cast ==
*Tom Tyler as Tom Wade
*Lois Wilde as Celia Chandler
*Dorothy Short as Annie Wade, Eds Wife
*Lafe McKee as Sheriff Bains
*Bob Terry as Ed Wade, Toms Brother Dave OBrien as Bart, Tracy Henchman Roger Williams as Jeff Tracy, Attorney
*Jim Corey as Larry, Tracy Henchman
*James C. Morton as Cattle Mans Protective Assoc. Chief
*Jack "Tiny" Lipson as Jake the Hobo

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 

 