Oyster Village
{{Infobox film
| name           = Oyster Village
| image          = Oyster Village.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Seokhwachon
 | mr             = Sŏk‘wach‘on
 | context        = }}
| director       = Jeong Jin-woo 
| producer       = Jeong Jin-woo
| writer         = Moon Yang-hoon Na Yeon-sook Na Bong-han
| based on       =  
| starring       = Yoon Jeong-hee
| music          = Han Sang-ki
| cinematography = Park Seung-bae
| editing        = Kim Hee-soo
| distributor    = Woo Jin Films Co., Ltd.
| released       =  
| runtime        = 96 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 1972 South Korean drama film directed by Jeong Jin-woo. It was awarded Best Film at the Blue Dragon Film Awards ceremony     and was nominated for the Golden Bear at the 22nd Berlin International Film Festival.   

==Synopsis==
In an island village that makes its living from oyster fishing, the villagers believe that a dead persons soul cannot go to heaven until another person has died. After Byol-Ryes father drowns, her mother kills herself. Byol-Rye then marries a sickly man on the condition that his father helps Byol-Ryes mother go to heaven. Based on a novel.   

==Cast==
* Yoon Jeong-hee 
* Kim Hee-ra
* Yoon Il-bong
* Yoon In-Ja
* Kim Sin-jae

==Bibliography==

===English===
*   
*  
*  

===Korean===
*  
*  

==Notes==
 

 
 
 
 
 

 
 
 
 
 
 
 
 


 