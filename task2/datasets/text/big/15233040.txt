Norman Normal
{{Infobox Hollywood cartoon|
| cartoon_name = Norman Normal
| series = 
| image = Norman Normal screenshot.png
| caption = The boss tries various disguises to try to persuade Norman to take Fanshawe to a bar and get him drunk to sign a contract.
| director = Alex Lovy
| story_artist = N. Paul Stookey Dave Dixon Ed Solomon
| voice_actor = N. Paul Stookey Dave Dixon
| musician = William Lava N. Paul Stookey (songs)
| producer = William L. Hendricks Paul Stookey|N. Paul Stookey John Freeman
| background_artist = Bob Abrams Ralph Penn
| distributor = Warner Bros.-Seven Arts
| release_date = February 3, 1968
| color_process = Technicolor
| runtime = 6 minutes 3 seconds
| movie_language = English
}}
 animated cartoon Warner Bros.-Seven Arts Cartoons. It was produced as a collaboration between musician Paul Stookey (of Peter, Paul and Mary fame) and the studios animation department. Rather than being released as part of the Looney Tunes or Merrie Melodies series, it was released as a one-time "Cartoon Special." 

The short has been released on disc 4 of the  , making it the first (and thus far, the only) short from the "Seven Arts" era of the studio to be released on DVD or any form of home video officially in the United States.
 pop artist Milton Glaser, and then refined by animator Volus Jones to create a character that would be easier to animate. Further cartoons starring Norman were envisaged by Stookey, although the studios closure the following year prevented these plans coming to fruition.

==Storyline==
The cartoon is introduced by a band, playing the cartoons theme song (written and performed by Stookey). Eventually the main character, a ball-bearing salesman named Norman appears and closes a door on them. He then introduces himself as the hero of the piece, and walks down a long corridor filled with doors, explaining that each of them has a different one of his problems behind them.

Norman then enters a door, which takes him to the office of his boss. Their company has been having difficulty getting a man named Fanshawe to buy a large consignment of ball-bearings, but the boss has discovered that Fanshawe is an alcoholism|alcoholic. To take advantage of this, he orders Norman to take Fanshawe to a bar, buy him as much alcohol as he wants, and then get him to sign the contract while hes drunk. Norman refuses to do this however, and tells his boss that "it just isnt right," but the boss re-assures him that "everybodys doing it." Norman continues to argue with his boss, and during the argument, the two suddenly revert to children, and the subject of the argument changes to the boss demanding that Norman bully a fellow child in order to get into the bosss gang. The two then revert to adults, and the boss tries reverse psychology, wondering out loud if he misjudged Norman and whether or not he is really suitable for the job. Norman seemingly caves in and agrees to the bosss demands, but on exiting the office (and walking back into the corridor), he vows not to do what is being asked of him, and to simply ask Fanshawe to sign the contract if he thinks the ball-bearings are good enough. 

He then enters another door, and enters a room containing his father. Norman asks his father serious questions about what is right and wrong, but his father merely floats around the room, giving Norman vague psychobabble and stories from his childhood. He then tells Norman that the key to success in life is not to make waves, and to fit in, after which he vanishes.

Walking through another door, Norman is taken to a party, and greeted by a man who wears a lampshade on his head and walks around the room repeating the word "approval." Another, drunken salesman then greets Norman, congratulates him for closing the deal with Fanshawe (it is not revealed how Norman has done this, but it is implied he did it through honest means), and then begins telling a joke which involves a travelling salesman mistaking an Eskimo woman for a walrus. The audience does not hear most of the joke however, as Norman talks over it and tells the drunken man that he shouldnt be telling jokes that involve another race or minority group, and is designed to make them look inferior (somewhat ironically, considering the attitude earlier WB shorts were often seen to demonstrate towards races such as the African-Americans or Japanese people|Japanese).

Once the man has finished telling his joke, Norman walks over to the bar and Hal the bartender, who is also drunk, asks him if he wants some more to drink. Norman tells Hal that hes had enough to drink (even though Norman has not drunk anything alcoholic) and asks for a ginger ale. Hal then taunts him, accusing Norman of hating himself when he is drunk, which causes Norman to walk away without reply.

Back in the corridor, Norman apologizes for the display that just took place, and re-opens the door containing the band seen at the start of the film. It is then revealed that both the band and this version of Norman are inside the head of another, larger version of Norman, visible through a door inside his head. This version then ends the film by closing the door on his head.
==References==
 
==External links==
*  

 