Rajkumar (1996 film)
{{Infobox film 
| name           = Rajkumar
| image          = Rajkumar (1996).jpg
| director       = Pankaj Parashar
| writer         = 
| narrator       = 
| starring       = Naseeruddin Shah Anil Kapoor Madhuri Dixit Danny Denzongpa Reena Roy Farida Jalal Sanjai Mishra
| released       = 22 March 1996
| country        = India
| language       = Hindi
}}

Rajkumar is an Indian film starring Naseeruddin Shah, Anil Kapoor,Sanjai Mishra, and Madhuri Dixit, which was released in 1996.  Box-Office-India declared it a Disaster at the box office.  

==Plot==
Rani Maas(Reena Roy) husband is killed by the neighboring kingdoms evil Prime Minister Man Singh (Naseeruddin Shah). The Prime Minister absolves himself from this killing and blames it on the king, the father of Rajkumari Vishaka (Madhuri Dixit). Rani Maa swears to avenge the death against the Rajkumari. Man Singh also has a twin brother, Surjan Singh (also Naseeruddin Shah) who is not evil at all albeit a little naive. Rani Maa is shocked and aghast when she finds out that her only son, Rajkumar (Anil Kapoor) is in love with Rajkumari. She sets out to oppose this marriage, while Rajkumar will leave no stone unturned to marry Rajkumari. The stage is set for mother and son to decide whether it is in their best interest to include someone in the family, who has killed a husband and a father respectively.

==Songs==
* Payal Meri    -  Alka Yagnik, Udit Narayan  
* Yeh Khubsoorat Badan    -  Alka Yagnik
* Aankhon Ke Aage Peeche    -  Kavita Krishnamurthy
* Aaja Aaja Tu Aanewale    -  Iqbal Afzai, Sabri, Sukhwinder Singh, Jayshree Shivram
* Bechain Hoon Main    -  Alka Yagnik, Udit Narayan
* O Mere Rajkumar    -  Alka Yagnik
* Tu Bijli Hai    -  Alka Yagnik, Udit Narayan

== Cast ==
* Anil Kapoor....Rajkumar 
* Madhuri Dixit....Rajkumari Vishaka
* Danny Denzongpa....Ali
* Saeed Jaffrey....Bhanuwala
* Reena Roy....Rani Maa
* Farida Jalal....Panna (Dai maa)
* Naseeruddin Shah....Man Singh/Surjan Singh
* Sanjay Mishra
==Notes==
 

==External links==
* 

 
 
 
 

 