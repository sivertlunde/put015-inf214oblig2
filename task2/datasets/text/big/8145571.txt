In the Shadow of the Stars
{{Infobox film
| name           = In the Shadow of the Stars
| image          = In the Shadow of the Stars.jpg
| caption        = 
| director       = Allie Light Irving Saraf
| producer       = Allie Light Irving Saraf
| writer         = 
| starring       = 
| music          = 
| cinematography = Michael Chin
| distributor    = Docurama - New Video
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
In the Shadow of the Stars is a 1991 documentary film about the San Francisco Opera. It depicts the lives of the various members of the choir|chorus, rather than the big name stars.

The film was co-directed, co-produced and edited by the husband and wife team of Irving Saraf and Allie Light.   

The film won the 1991 Academy Award for Best Documentary Feature for Light and Saraf.    

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 