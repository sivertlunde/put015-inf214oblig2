Muqaddar Ka Badshaah
{{Infobox film
| name           =  Muqaddar Ka Badshah
| image          = MuqaddarKaBadshaah.jpg
| caption        =
| director       = Rama Rao Tatineni
| producer       = 
| writer         = Iqbal Durrani
| starring       = Vinod Khanna Shabana Azmi Vijayshanti Anupam Kher Kader Khan
| music          = Viju Shah
| cinematography = 
| editing        = 
| production_company =
| distributor    =
| released       = 1990
| runtime        =
| country        = India
| awards         =
| language       = Hindi
| budget         =
| gross          =
| website        =
}}
Muqaddar Ka Badshah is a 1990 Bollywood film directed by Rama Rao Tatineni. The film stars Vinod Khanna, Shabana Azmi, Vijayshanti, Anupam Kher, Kader Khan and Amrish Puri. Dialogues of this movie became very popular and therefore audio cassette of Dialogues were released it was written by Iqbal Durrani.

==Cast==
*Vinod Khanna ...  Naresh
*Shabana Azmi ...  Advocate Sharda Singh
*Vijayshanti ...  Bharti Rathod
*Anupam Kher ...  Vijay Singh
*Kader Khan ...  Inspector Gulshan
*Amrish Puri ...  Vikral Singh
*Navin Nischol ...  DSP Rathod
*Suparna Anand ... Geetha

==Music==
Music is composed By Viju Shah, and lyrics are penned by Sameer.
* 01 - Aiko Haina - Amit Kumar & Sapna Mukherjee            - 5.00
* 02 - Jaaneman - Amit Kumar & Sadhna Sargam                - 5.10
* 03 - Muqaddar Ka Badshah - Amit Kumar                         - 4.34
* 04 - Shararat Karoonga - Amit Kumar & Sapna Mukherjee     - 5.46
* 05 - Ho Jaayega Loaccha - Amit Kumar & Boney                  - 7.05
* 06 - Aiko Haina - Amit Kumar                                  - 1.50

== External links ==
*  

 
 
 


 