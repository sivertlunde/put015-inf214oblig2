Beratha Jeeva
{{Infobox film 
| name           = Beratha Jeeva
| image          =  
| caption        = 
| director       = Ku Ra Seetharam Shastry
| producer       = Ku Ra Seetharam Shastry S Shivaram
| based on        = Remake of Paalum Pazhamum (Tamil) (1961)
| screenplay     = Ku Ra Seetharam Shastry Jayanthi K. S. Ashwath
| music          = Vijaya Bhaskar
| cinematography = K Janakiram
| editing        = Bal G Yadav
| studio         = Viridha Bharathi
| distributor    = Viridha Bharathi
| released       =  
| runtime        = 120 min
| country        = India Kannada
}}
 1965 Cinema Indian Kannada Kannada film, Jayanthi and K. S. Ashwath in lead roles. The film had musical score by Vijaya Bhaskar.  

==Cast==
 
*Kalyan Kumar
*B. Sarojadevi Jayanthi
*K. S. Ashwath
*Shivaram Balakrishna
*Narasimharaju Narasimharaju
*Ramachandra Shastry
*Raghavendra Rao
*Shivaraj
*B. Jayashree
*Lakshmidevi
*B. Jaya (actress)|B. Jaya
 

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Ku Ra See || 03.29
|}

==References==
 

==External links==
*  

 
 
 


 