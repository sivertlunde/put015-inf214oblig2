Tiara Tahiti
{{Infobox film
| name           = Tiara Tahiti
| image          = 
| image_size     = 
| caption        = 
| director       = Ted Kotcheff
| producer       = 
| writer         = Geoffrey Cotterell Ivan Foxwell
| narrator       = 
| starring       = James Mason John Mills Rosenda Monteros
| music          = 
| cinematography = 
| editing        = Antony Gibbs
| distributor    =  1962
| runtime        = 100 min.
| country        = U.K. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Tiara Tahiti is a 1962 comedy-drama film starring James Mason and John Mills and the directorial debut of Ted Kotcheff; it is based on the novel by Geoffrey Cotterell, who also adapted it for the screen with Ivan Foxwell. It was filmed in London and Tahiti. Rosenda Monteros, a Mexican actress, plays a Tahitian beauty. Roy Kinnear had a minor role.

==Plot==
Clifford Southey (Mills) is a clerk at a brokerage firm who is promoted to lieutenant colonel during the war.  His subordinate officer, Captain Brett Aimsley (Mason), was a partner at Southeys firm.  Popular and charismatic, Capt. Aimsley is everything Col. Southey is not, but aspires to be.  Unfortunately money is Aimsleys weakness. His profligacy sees him removed from Southeys command.

Some time after the war, Aimsleys comfortable exile in Tahiti is rudely interrupted by the arrival of his old adversary, now director of a hotel chain looking to expand into the burgeoning South Seas market.

==Cast==
* James Mason as Aimsley
* John Mills as Southey
* Herbert Lom as Chong Sing
* Rosenda Monteros as Belle Annie Claude Dauphin as Henry
* Roy Kinnear as Enderby

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 