Zombie Strippers
{{Infobox Film
| name           = Zombie Strippers
| image          = Zombie strippers.jpg
| caption        = Theatrical release poster
| director       = Jay Lee
| producer       = Larry Schapiro   Andrew Golov   Angela J. Lee
| writer         = Jay Lee
| starring       = Robert Englund   Jenna Jameson   Penny Drake   Roxy Saint   Jessica Custodio   Tito Ortiz
| music          = Billy White Acre
| cinematography = Jay Lee
| editing        =
| studio         = Stage 6 Films
| distributor    = Triumph Films
| released       =   
| runtime        =
| country        = United States
| language       = English
| budget         =
}}
Zombie Strippers is a 2008 American zombie comedy film written and directed by Jay Lee, starring Robert Englund, Jenna Jameson, and Tito Ortiz and distributed by Sony Pictures Home Entertainment. The film is loosely based on Eugène Ionescos classic play Rhinoceros (play)|Rhinoceros.

== Plot ==
This movie opens with a news montage explaining that it is set in a dystopic near-future in which George W. Bush has been elected to a fourth term. The United States Congress has been disbanded; public nudity is banned; the United States is embroiled in wars with France, Iraq, Afghanistan, Iran, Pakistan, Syria, Venezuela, Canada, and Alaska. With more wars than there are soldiers to fight them, a secret laboratory run by Dr. Chushfeld (Brad Milne) in fictional Sartre, Nebraska, has developed a virus to re-animate dead Marines and send them back into battle. However, this virus has broken containment and infected test subjects and scientists, and they are at risk of escaping the lab. A team of Marines codenamed the "Z" Squad is sent in to destroy the zombies. One of the Marines named Byrdflough (Zak Kilberg) is bitten but escapes. He ends up in an alley outside an underground strip club named "Rhino". The Marine dies and awakens as a zombie who goes into the strip club.

"Rhino" is run by Ian Essko (Robert Englund). A new stripper named Jessy (Jennifer Holland) has arrived at the club to save up enough money for her grandmothers operation. She is introduced to the clubs star dancer, Kat (Jenna Jameson). Kat begins her dance on the stage, but is attacked by Byrdflough. Essko is concerned about losing his best dancer, so he lets her go back on stage as a zombie. To everyones surprise, Kat is a better and more popular dancer as a zombie than she was as a human.

The other strippers now find themselves faced with the prospect of losing their customers, as the customers prefer zombie strippers to human strippers. One by one, the human strippers become zombies, some by choice in order to compete or (in the case of Gothic rock stripper, Lillith) for fun. During private dances, the zombie strippers bite and kill their customers. Essko tries to keep the zombies hidden in a cage in the clubs cellar, but eventually, the zombies escape and overrun the club. Kat and the underrated stripper Jeanni (Shamron Moore) fight for supremacy.  The remaining humans in the club struggle to survive until the "Z" Squad burst in to destroy the zombies. But they discover that the zombies were allowed to escape by the Bush Administration, in the hopes that the ensuing zombie plague would distract Americans from their gross mishandling of the war effort and the economy.

== Cast ==
 
*Robert Englund as Ian Essko
*Jenna Jameson as Kat
*Roxy Saint as Lillith
*Penny Drake as Sox
*Joey Medina as Paco
*Whitney Anderson as Gaia
*Jennifer Holland as Jessy
*Shamron Moore as Jeannie
*Jeannette Sousa as Berengé
*Carmit Levité as Madame Blavatski
*John D. Hawkes as Davis
*Brad Milne as Dr. Chushfeld
*Zak Kilberg as Byrdflough
*Jen Alex Gonzalez as Lt. Ryker
*Tito Ortiz as Door Bouncer
*Margad as fighter

== Release ==
{|class=wikitable
!Region
!Date
!Format
|- United Kingdom 13 October 2008 DVD
|- USA
|27 October 2008 DVD
|-
|}

==Charts==
{|class="wikitable" Chart (2008) Peak position
|- UK Top 40 DVD Chart 40
|- USA Top 50 DVD Chart 11
|-
|}

== Critical Acclaim ==
Zombie Strippers has received mixed reviews. As of August 20, 2008, the review aggregator Rotten Tomatoes reported that 41% of critics gave the movie positive reviews, based on 51 reviews.  Metacritic reported that this movie had an average score of 45 out of 100, based on 14 reviews. 
 camp style and its attempt as a satire. Richard Roeper of Ebert & Roeper stated, "It looks terrible. It doesnt work as camp. It doesnt work as low budget crap", Dennis Harvey of Variety Magazine also called it a "one-joke pic". In contrast, Michael Rechtshaffen of the Hollywood Reporter thought that there was something "perversely affecting" about this movie, despite its "lame political satire". 

== See also ==
*Zombies! Zombies! Zombies! (also known as Strippers vs Zombies)
*Big Tits Zombie – a 2010 Japanese fantasy-horror film, adapted from the manga Kyonyū Dragon
* 2008 in film

== References ==
 

== External links ==
* 
*  at Sony Pictures Home Entertainment
* 
* 
* 
* 
*  at MySpace
*  at Yahoo! Movies
*  at Apple.com

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 