12 Angry Men (1957 film)
 
{{Infobox film
| name           = 12 Angry Men
| image          = 12 angry men.jpg
| caption        = Original film poster
| director       = Sidney Lumet
| producer       = Henry Fonda Reginald Rose
| writer         = Reginald Rose Joseph Sweeney George Voskovec  Robert Webber
| music          = Kenyon Hopkins
| cinematography = Boris Kaufman
| editing        = Carl Lerner
| distributor    = United Artists
| released       =  
| runtime        = 96&nbsp;minutes
| country        = United States
| language       = English
| budget         = $340,000  Anita Ekberg Chosen for Mimi Role
Louella Parsons:. The Washington Post and Times Herald (1954-1959)   08 Apr 1957: A18.  
| gross = $1,000,000   
}}
 
12 Angry Men is a 1957 American   followed by the judges final instructions to the jury before retiring, a brief final scene on the courthouse steps, and two short scenes in an adjoining washroom, the entire movie takes place in the jury room. The total time spent outside the jury room is three minutes out of the full 96&nbsp;minutes of the movie.

12 Angry Men explores many techniques of Consensus decision-making|consensus-building, and the difficulties encountered in the process, among a group of men whose range of personalities adds intensity and conflict. No names are used in the film: the jury members are identified by number until two of them exchange names at the very end, the defendant is referred to as "the boy", and the witnesses as "the old man" and "the lady across the street".
 under the same title was released by Metro-Goldwyn-Mayer|MGM.

In 2007, 12 Angry Men was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

==Story==
The story begins in a New York City courthouse, where an eighteen-year-old Hispanic boy from a slum is on trial for allegedly stabbing his father to death. Final closing arguments having been presented, a visibly tired judge instructs the jury to decide whether the boy is guilty of murder. The judge further informs them that a guilty verdict will be accompanied by a mandatory death sentence.   

The jury retires to a private room, where the jurors spend a short while getting acquainted before they begin deliberating. It is immediately apparent that the jurors have already decided that the boy is guilty, and that they plan to return their verdict without taking time for discussion—with the sole exception of Juror 8 (Henry Fonda), who is the only "not guilty" vote in a preliminary tally. He explains that there is too much at stake for him to go along with the verdict without at least talking about it first. His vote annoys the other jurors, especially Juror 7 (Jack Warden), who has tickets to a baseball game that evening; and Juror 10 (Ed Begley), who believes that people from slum backgrounds are liars and are wild and dangerous. 

The rest of the films focus is the jurys difficulty in reaching a unanimous verdict. While several of the jurors harbor personal prejudices, Juror 8 maintains that the evidence presented in the case is circumstantial, and that the boy deserves a fair deliberation. He calls into question the accuracy and reliability of the only two witnesses to the murder, the "rarity" of the murder weapon (a common switchblade, of which he has an identical copy), and the overall questionable circumstances. He further argues that he cannot in good conscience vote "guilty" when he feels there is reasonable doubt of the boys guilt.
 hanging the Joseph Sweeney) reveals that he himself changed his vote, feeling that Juror 8s points deserve further discussion.
 George Voskovec) questions whether the defendant would have reasonably fled the scene before cleaning the knife of fingerprints, then come back three hours later to retrieve the knife (which had been left in his fathers chest); then changes his vote.

Juror 8 then mentions the mans second claim: upon hearing the fathers body hit the floor, he had gone to the door of his apartment and seen the defendant running out of the building from his front door in 15 seconds. Jurors 5, 6 and 8 question whether this is true, as the witness in question had had a  , saying he wants the defendant to die only because "he personally wants it, not because of the facts". Juror 3 shouts "Ill kill him!" and starts lunging at Juror 8, but is restrained by Jurors 5 and 7. Juror 8 calmly retorts, "You dont really mean youll kill me, do you?", proving his previous point. 

Jurors 2 (John Fiedler) and 6 (Edward Binns) also decide to vote "not guilty", tying the vote at 6–6. Soon after, a rainstorm hits the city, threatening to cancel the baseball game for which Juror 7 has tickets.

Juror 4 (E. G. Marshall) states that he does not believe the boys alibi, which was being at the movies with a few friends at the time of the murder, because the boy could not remember what movie he had seen three hours later. Juror 8 explains that being under emotional stress can make you forget certain things, and tests how well Juror 4 can remember the events of previous days. Juror 4 remembers, with some difficulty, the events of the previous five days, and Juror 8 points out that he had not been under emotional stress at that time, thus there was no reason to think the boy could remember the movie that he had seen. 

Juror 2 calls into question the prosecutions claim that the accused, nearly a foot shorter than the victim, was able to inflict the downward stab wound found on the body. Jurors 3 and 8 conduct an experiment to see if its possible for a shorter person to stab downward into a taller person. The experiment proves the possibility, but Juror 5 then explains that he had grown up amidst knife fights in his neighborhood, and shows, through demonstrating the correct use of a switchblade, that no one so much shorter than his opponent would have held a switchblade in such a way as to stab downward, as the grip would have been too awkward and the act of changing hands too time-consuming. Rather, someone that much shorter than his opponent would stab underhanded at an upwards angle. This revelation augments the certainty of several of the jurors in their belief that the defendant is not guilty.

Increasingly impatient, Juror 7 changes his vote just so that the deliberation may end, which earns him the ire of Jurors 3 and 11, both on opposite sides of the discussion. Juror 11, an immigrant who has repeatedly displayed strong patriotic pride, presses Juror 7 hard about using his vote frivolously, and eventually Juror 7 claims that he now truly believes the defendant is not guilty. 

The next jurors to change their votes are Jurors 12 (Robert Webber) and 1 (Martin Balsam), making the vote 9–3 and leaving only three dissenters: Jurors 3, 4 and 10. Outraged at how the proceedings have gone, Juror 10 goes into a rage on why people from the slums cannot be trusted, of how they are little better than animals who gleefully kill each other off for fun. His speech offends Juror 5, who turns his back to him, and one by one the rest of the jurors start turning away from him. Confused and disturbed by this reaction to his diatribe, Juror 10 continues in a steadily fading voice and manner, slowing to a stop with "Listen to me. Listen..." Juror 4, the only man still facing him, tersely responds, "I have. Now sit down and dont open your mouth again." As Juror 10 moves to sit in a corner by himself, Juror 8 speaks quietly about the evils of prejudice, and the other jurors slowly resume their seats.

When those remaining in favor of a guilty vote are pressed as to why they still maintain that there is no reasonable doubt, Juror 4 states his belief that despite all the other evidence that has been called into question, the fact remains that the woman who saw the murder from her bedroom window across the street (through the passing train) still stands as solid evidence. After he points this out, Juror 12 changes his vote back to "guilty", making the vote 8–4.

Then Juror 9, after seeing Juror 4 rub his nose (which is being irritated by his glasses), realizes that, like Juror 4, the woman who allegedly saw the murder had impressions in the sides of her nose which she rubbed, indicating that she wore glasses, but did not wear them to court out of vanity. Juror 8 cannily asks Juror 4 if he wears his eyeglasses to sleep, and Juror 4 admits that he does not wear them – nobody does.  Juror 8 explains that there was thus no logical reason to expect that the witness happened to be wearing her glasses while trying to sleep, and he points out that the attack happened so swiftly that she would not have had time to put them on. After he points this out, Jurors 12, 10 and 4 all change their vote to "not guilty".

At this point, the only remaining juror with a guilty vote is Juror 3. Juror 3 gives a long and increasingly tortured string of arguments, ending with, "Rotten kids, you work your life out—!" This builds on a more emotionally ambivalent earlier revelation that his relationship with his own son is deeply strained, and his anger over this fact is the main reason that he wants the defendant to be guilty. Juror 3 finally loses his temper and tears up a photo of himself and his son, then suddenly breaks down crying and changes his vote to "not guilty", making the vote unanimous.

As the jurors leave the room, Juror 8 helps the distraught Juror 3 with his coat in a show of compassion. The film ends when the friendly Jurors 8 (Davis) and 9 (McCardle) exchange names, and all of the jurors descend the courthouse steps to return to their individual lives. 

==Cast of characters==
The 12 jurors in the order in which they are referred. They are seated in this order in the movie.

#The jury foreman, somewhat preoccupied with his duties and never gives any reason for changing his vote; proves to be helpful to others. An assistant high school American football coach. He is the ninth to vote "not guilty". Martin Balsam
#A meek and unpretentious bank worker who is at first dominated by others, but as the climax builds up, so does his courage.   He is the fifth to vote "not guilty". John Fiedler
#A businessman and distraught father, opinionated, disrespectful, and stubborn with a temper.  He is the last to vote "not guilty". Lee J. Cobb
#A rational, unflappable, self-assured and analytical stock broker who is concerned only with the facts, and avoids any small talk.  He is the 11th to vote "not guilty". E. G. Marshall 
#A man who grew up in a violent slum, a Baltimore Orioles fan. An ambulance crewman. He is the third to vote "not guilty". Jack Klugman 
#A house painter, tough but principled and respectful.  He is the sixth to vote "not guilty". Edward Binns
#A salesman, sports fan, superficial and indifferent to the deliberations.  He is the seventh to vote "not guilty". Jack Warden
#An architect and the first to vote "not guilty". Henry Fonda  Joseph Sweeney 
#A garage owner; a pushy and loudmouthed bigot.  He is the 10th to vote "not guilty". Ed Begley
#A European watchmaker and naturalized American citizen.  Very polite and makes wordy contributions.  He is the 4th to vote "not guilty". George Voskovec
#A wisecracking, indecisive advertising executive. He is the 8th to vote "not guilty".  Robert Webber

Uncredited
* Rudy Bond as Judge 
* James Kelly as Guard  Billy Nelson as Court Clerk
* John Savoca as the Accused

==Production== Studio One in September 1954. A complete kinescope of that performance, which had been missing for years and was feared lost, was discovered in 2003. It was staged at Chelsea Studios in New York City. 
 Studio One, was recruited by Henry Fonda and Rose to direct. 12 Angry Men was Lumets first feature film, and for Fonda and Rose, who co-produced the film, it was their first and only role as film producers. Fonda later stated that he would never again produce a film.

The filming was completed after a short but rigorous rehearsal schedule in less than three weeks on a tight budget of $340,000, or approximately $2.9 million (in 2014) when adjusted for inflation.

At the beginning of the film, the cameras are positioned above eye level and mounted with wide-angle lenses to give the appearance of greater depth between subjects, but as the film progresses the focal length of the lenses is gradually increased. By the end of the film, nearly everyone is shown in closeup using telephoto lenses from a lower angle, which decreases or "shortens" depth of field. Lumet, who began his career as a director of photography, stated that his intention in using these techniques with cinematographer Boris Kaufman was to create a nearly palpable claustrophobia. 

==Reception==

===Critical response===
On its first release, 12 Angry Men received critical acclaim. A. H. Weiler of   Filmsite.org|FilmSite. Retrieved April 14, 2012.   The advent of color and widescreen productions resulted in a disappointing box office performance.  It was not until its first airing on television that the movie finally found its audience. 

===Legacy=== in a AFI also most inspiring most heart-pounding hundred years. 100 movies list in 1998.  As of January 2015, the film holds a List of films with a 100% rating on Rotten Tomatoes|100% approval rating on the review aggregate website Rotten Tomatoes.  In 2011, the film was the second most screened film in secondary schools in the United Kingdom.  In the 54 Best Legal Films of all-time,  12 Angry Men received 11 of a possible 15 votes. 

American Film Institute lists:
*AFIs 100 Years...100 Movies – No. 87
*AFIs 100 Years...100 Thrills – No. 88
* AFIs 100 Years...100 Heroes & Villains: Juror No. 8 – No. 28 Hero
*AFIs 100 Years...100 Cheers – No. 42
*AFIs 100 Years...100 Movies (10th Anniversary Edition) – No. 87
*AFIs 10 Top 10 – No. 2 Courtroom Drama

===Awards=== Best Director, Best Picture, Best Writing of Adapted Screenplay. It lost to the movie The Bridge on the River Kwai in all three categories. At the 7th Berlin International Film Festival, the film won the Golden Bear Award. 

==Cultural influences==
Speaking at a screening of the film during the 2010 Fordham University Law School Film festival, Supreme Court Justice Sonia Sotomayor stated that seeing 12 Angry Men while she was in college influenced her decision to pursue a career in law. She was particularly inspired by immigrant Juror 11s monologue on his reverence for the American justice system. She also told the audience of law students that, as a lower-court judge, she would sometimes instruct juries to not follow the films example, because most of the jurors conclusions are based on speculation, not fact. {{Citation
| last = Semple | first = Kirk
| title = The Movie That Made a Supreme Court Justice
| newspaper = The New York Times
| date = October 18, 2010
| url = http://www.nytimes.com/2010/10/18/nyregion/18sonia.html?ref=nyregion mistrial  (assuming, of course, that applicable law permitted the content of jury deliberations to be revealed).

The movie has also had an impact beyond the United States. A 1991 homage by  , posits a Japan with a jury system and features a group of "normal" Japanese people grappling with their responsibility in the face of Japanese cultural norms.

Russian director Nikita Mikhalkov also made a 2007 adaptation, 12 (2007 film)|12.

BBC Radio comedy "Hancocks Half Hour", starring Tony Hancock and Sydney James, written by Ray Galton and Alan Simpson, broadcast a half-hour parody on 16 October 1959.

==See also==
* 12 Angry Men (1997 film)|12 Angry Men (1997 film)
* Twelve Angry Men

==References==
{{Reflist|30em|refs=
   
}}

==Further reading==
* Making Movies, by Sidney Lumet. (c) 1995, ISBN 978-0-679-75660-6
*  In depth analysis compared with research on actual jury behaviour.
* The New York Times, April 15, 1957, "12 Angry Men", review by A. H. Weiler
* Readings on Twelve Angry Men, by Russ Munyan, Greenhaven Press, 2000, ISBN 978-0-7377-0313-9
* Chandler, David. “The Transmission model of communication” Communication as Perspective Theory. Sage publications. Ohio University, 2005.
* Lanham, Richard. “Introduction: The Domain of Style analyzing prose”. (New York, NY: Continuum, 2003)

==External links==
 
*  
*  
*  
*  by Thane Rosenbaum

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 