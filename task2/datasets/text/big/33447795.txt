The Suicide Forecast
{{Infobox film
| name = The Suicide Forecast
| image = TheSuicideForecast2010Poster.jpg
| border = yes
| caption = Film poster
| film name = {{Film name
 |hangul=   
 |hanja= 한  들
 |rr=Susanghan Gogaekdeul
 |mr=Susanghan Kogaekdŭl}}
| director = Jo Jin-mo
| producer = Park Mae-hee
| writer = Yoo Sung-hyub Im Joo-hwan Younha
| music = Kim Hyung-seok
| cinematography = Choi Sang-mook
| editing = Shin Min-kyung
| distributor =
| released =  
| runtime = 124 minutes
| country = South Korea
| language = Korean
| budget =
| gross =   
}}
The Suicide Forecast ( ; lit. "Suspicious Customers") is a 2011 South Korean comedy-drama film. It is Jo Jin-mos directorial debut.

==Plot==
Baseball player-turned-insurance salesman Byung-woo (Ryoo Seung-bum) is seen as the cocky ace of his company until one of his clients commits suicide. The police suspect him of aiding and abetting the suicide, which almost jeopardizes Byung-woos career. Anxious to ensure it wont happen again, he gets in touch with his previous clients. Particularly those who seem the type to commit suicide.
 Im Joo-hwan), and a widowed mother (Jung Sun-kyung).  

==Cast==
*Ryoo Seung-bum ... Bae Byung-woo   
*Seo Ji-hye ... Lee Hye-in
*Sung Dong-il ... Manager Park Jin-seok
*Park Chul-min ... Oh Sang-yeol
*Jung Sun-kyung ... Choi Bok-soon Im Joo-hwan ... Kim Young-tak
*Younha ... Ahn So-yeon 
*Kim Chae-bin ... Jin-hee
*Lee Ji-eun ... Seon-hee
*Lee Joon-ha ... Mi-hee
*Oh Eun-chan ... Ok-dong
*Sungha Jung ... Ahn Hyeok
*Hong So-hee ... Kim Young-mi
*Kim Byeong-chun ... Homeless guy Park
*Choi Il-hwa ... Hwang Woo-cheol

==References==
 

==External links==
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 


 
 