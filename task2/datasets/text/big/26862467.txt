Lucky Man (film)
{{Infobox film
| name           = LuckyMan
| image          = Lucky_Man_(film).jpg
| caption        = 
| director       = Prathap K. Pothan
| producer       = K. Prashath
| writer         = Rajagopalan (dialogues)
| screenplay     = Prathap K. Pothan
| story          = S. V. Krishna Reddy Karthik Sanghavi Senthil Manjula Vijayakumar Radha Ravi Thyagu
| music          = Adhityan
| cinematography = K. Aravind
| editing        = B. Lenin V. T. Vijayan
| studio         = Prashath Art Films
| distributor    = Prashath Art Films
| released       = 14 April 1995
| runtime        = 
| country        = India
| language       = Tamil
}}
 Tamil film Karthik in Telugu film, Ali and Indraja (actress)|Indraja, and directed by S. V. Krishna Reddy. The film did well at the Box Office.

==Plot==
A book that belongs to the Hindu god of death, Yamadharman, accidentally drops to earth. This book can be used to kill or save any human’s life by writing his/her name in one of the pages of the book. An antihero character called Gopi(Karthik) gets a hold of the book and abuses its powers for his selfish needs.  Yamadharman god of death. Senthil is Chitra Gupta an assistant of Yamadharman. Chitra Gupta misses a book which contains all the details of human beings. The book falls into a city in Tamil Nadu. Brahmadeva orders Yamadharma to locate the book within one month. Chitra Gupta fears that if the book is seen by the human beings chaos will happen. But Yamadharma assures him that only the details of the person who is viewing the book will be shown by the book. Gopi and his friend Thyagu happens to see the book. They become rich as they come to know the result of lottery before hand by using the book. Gopi becomes rich by using the book. Meanwhile Yamadharma and Chitra Gupta comes to Tamil Nadu to search the book. But they dont really understand the current civilization leading to fun. Gopis opponent Radha Ravi wanted to know the secret of success of Karthik and he plans to steal the book from Karthik.  One day Karthik learns about the date of death of his mother Manjula through that book. Now Karthik wanted to save his mother and win his lover Sanghavi. Whether he succeeds or not, whether Yamadharma finds the book is the story.

==Cast== Karthik as Gopikrishna
*Sanghavi
*Manjula Vijayakumar as Karthiks mother Yamadharmajan
*Senthil as Chitra Gupta
*Radha Ravi as "Cheating" Sivaraman
*Vinu Chakravarthy as Inspector Ranjith Kumar
*Jayamani as Police Constable
*Prathap K. Pothan Thyagu as Karthicks friend

==Music==
The music composed by Adhityan and lyrics written by Piraisoodan.

{{tracklist	
| headline     = Tracklist
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Ammama 1000
| extra1       = K. S. Chithra|K.S Chithra
| lyrics1      =
| length1      = 
| title2       = Ammama
| extra2       =
| lyrics2      = 
| length2      = 
| title3       = Cokka cola
| extra3       =
| lyrics3      = 
| length3      = 
| title4       = Kooam rootu
| extra4       =
| lyrics4      = 
| length4      = 
| title5       = Pallana party
| extra5       =
| lyrics5      = 
| length5      = 
| title6       = Yar Sontham
| extra6       =
| lyrics6      = 
| length6      =
| title7       = Lucky luckyman
| extra7       =
| lyrics7      =
| length7      =
| title8       =
| extra8       =
| lyrics8      =
| length8      = 
}}

==References==
 

== External links ==
*  

 
 
 
 
 


 