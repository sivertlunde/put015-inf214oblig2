The Vanishing (1993 film)
{{Infobox film
| name           = The Vanishing
| image          = Thevanishing1993poster.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = George Sluizer
| producer       = Larry Brezner Pieter Jan Brugge
| screenplay     = Todd Graff
| based on       =  
| starring       = Jeff Bridges Kiefer Sutherland Nancy Travis Sandra Bullock
| music          = Jerry Goldsmith
| cinematography = Peter Suschitzky
| editing        = Bruce Green
| distributor    = 20th Century Fox
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $23 million
| gross          = $14,543,394 
}} thriller starring The Vanishing, and also directed by George Sluizer.

==Plot==
Jeff Harriman (Kiefer Sutherland) goes on vacation with his girlfriend Diane Shaver (Sandra Bullock), who vanishes without a trace at a gas station. Three years later, Jeff is still obsessed with finding out what happened. One day, Barney Cousins (Jeff Bridges) arrives at Jeffs door and admits that he was responsible for her disappearance. Cousins promises to show Jeff what happened to Diane, but only if he agrees to go through exactly the same thing she did.

In a short series of flash-backs, the build-up to the crime is shown. Jeff is taken to the gas station where his lover went missing, and is told that if he drinks a cup of coffee which has been drugged, he will discover her fate by experiencing it. He does, and wakes up to find he has been buried alive.

Jeffs new girlfriend, Rita (Nancy Travis), has traced him and his abductor to the area, and discovers just in time what has happened. She gets Cousins to drink drugged coffee by talking about his daughter, but does not realize the drug takes 15 minutes to take effect. She goes in search of Jeff, but is thwarted at the last minute by Cousins. Fortunately, Jeff has revived and is able to climb out of the grave and kill his tormentor with the shovel he had used to bury Jeff and Diane. The remake ends with Jeff and Rita back together, selling the story as a novel to a publishing company.

==Cast==
* Jeff Bridges as Barney Cousins
* Kiefer Sutherland as Jeff Harriman
* Nancy Travis as Rita Baker
* Sandra Bullock as Diane Shaver
* Park Overall as Lynn
* Maggie Linderman as Denise Cousins
* Lisa Eichhorn as Helene Cousins
* George Hearn as Arthur Bernard Lynn Hamilton as Miss Carmichael

==Reception== Time Out lobotomized Hollywood remake."  Mark Kermode would later summarise that "the original was about the banality of evil, but the remake became about the evil of banality. It was a mess."   Salon.com named the film as the worst remake of all time.  The movie currently holds a 47% rating on Rotten Tomatoes based on 34 reviews.

==See also==
*The Vanishing (1988 film)|The Vanishing (1988 film)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 