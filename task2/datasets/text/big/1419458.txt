Liberty!
{{Infobox film
| name           = Liberty! The American Revolution
| image          = 
| image_size     = 
| caption        = 
| director       = Ellen Hovde Muffie Meyer
| producer       = Ellen Hovde Muffie Meyer
| writer         = Ronald Blumer
| narrator       = 
| starring       = 
| music          = Richard Einhorn
| cinematography = James Brown 
Robert Elfstrom 
Boyd Estus 
Tom Hurwitz 
Joel Shapiro 
Joe Vitagliano
| editing        = Eric Davies 
Donna Marino 
Sharon Sachs
| distributor    = Public Broadcasting Service
| released       =    
| runtime        = 360 minutes
| country        =  English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
  documentary miniseries Revolutionary War, and the instigating factors, that brought about the United States independence from the Kingdom of Great Britain. It was first broadcast in 1997.
 Stephen Lang read the words of George Washington, but is not seen on camera.

==Episodes==
:1. "The Reluctant Revolutionaries" (1763-1774)
:2. "Blows Must Decide" (1774-1776)
:3. "The Times That Try Mens Souls" (1776-1777)
:4. "Oh Fatal Ambition" (1777-1778)
:5. "The World Turned Upside Down" (1778-1783)
:6. "Are We to Be a Nation?" (1783-1788)
 a companion album in 1997.
 Benjamin Franklin.

==External links==
* 
* 

 
 
 
 


 