Indie Game: The Movie
 
 
 
{{Infobox film
| name           = Indie Game: The Movie
| image          = Indie Game The Movie poster.png
| alt            = Against a pale blue background, a SNES game controller hangs from a wire, above a pit of bloodied spikes. 
| image_size     = 
| border         = 
| caption        = 
| director       = James Swirsky Lisanne Pajot
| producer       = James Swirsky Lisanne Pajot
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plain list | 
* Edmund McMillen
* Tommy Refenes
* Phil Fish
* Jonathan Blow
}} Jim Guthrie
| cinematography = James Swirsky Lisanne Pajot
| editing        = James Swirsky Lisanne Pajot
| studio         = BlinkWorks Flutter Media
| distributor    = BlinkWorks Media
| released       =    
| runtime        = 94 minutes (Canada) 103 minutes (International)
| country        = 
| language       = English
| budget         = 
| gross          =  
}}

Indie Game: The Movie is a 2012 documentary film by Canadian filmmakers James Swirsky and Lisanne Pajot. The film documents the struggles of independent game developers Edmund McMillen and Tommy Refenes during the development of Super Meat Boy, Phil Fish during the development of Fez (video game)|Fez, and also Jonathan Blow, who reflects on the success of Braid (video game)|Braid. 

After two successful Kickstarter funds,   interviews were conducted with prominent indie developers within the community. After recording over 300 hours of footage, Swirsky and Pajot decided to cut the movie down to follow the four developers selected.   Their reasoning behind this was to show game development in the "past, present and future" tenses through each individuals story. 

==Synopsis==
The film shows the high level of personal expression that typically goes into independent games, through the story of three games: Braid (video game)|Braid was released in 2008, Super Meat Boy was preparing for its 2011 release, while Fez (video game)|Fez was struggling with development hell for several years.

Braid developer Jonathan Blow recounts his thought process for the game: how he wished to put his "deepest flaws and vulnerabilities" into it and how his initial design experience quickly turned from experimentation to discovery. He also talks about the aftermath of the game: When Braid comes out, it receives widespread critical acclaim, but Blow is disillusioned, when a large portion of players dont "get" the underlying message and themes of the game. He makes attempts to influence the audiences impression of the game through forum posts and blog comments, but this eventually turns him into something of a comic figure, which he feels uncomfortable with. The game remains a commercial and critical success.

Super Meat Boy developers Team Meat (Edmund McMillen and Tommy Refenes) set out to do a platform game that harkens back to their own childhood video game experiences. McMillen talks about his lifelong goal of communicating to others through his work. He goes on to talk about his 2008 game Aether (video game)|Aether that chronicles his childhood feelings of loneliness, nervousness, and fear of abandonment.   He also sheds light on the level design techniques he uses, on how he teaches players to play without extensive tutorials.

About a year into development, Microsoft offers Team Meat a chance to take part in an  , and takes its toll on McMillens marriage, and on the health of Refenes, who bears the brunt of the work. Refenes also laments how he sacrificed his social life to get the game done, but expresses gratitude for his family being supportive of his goal. The team successfully delivers the game, but come release day, the game is nowhere to be found on Xbox Live, which greatly upsets Refenes, who predicts low sales as a result. The game does eventually appear on the marketplace, and doubles Braid s sales, selling 20,000 units in the first 24 hours. McMillen is surprised by both the sales and touched by the fan reaction, and although Refenes, exhausted and cynical, is less enthusiastic, his joy shows through when he sees videos of people enjoying the game. The game eventually goes on to sell a million copies, providing a level of financial security to both developers.

Fez developer Polytron ( , which thrust Fish into the limelight as an "indie developer celebrity", but little was heard of the game since. The development is troubled casting doubt on the future of the project. Fish himself admits to his perfectionism protracting the development, as well as losing perspective over time about how good the game really is. Similar to Refenes, Fish also notes that he does not see himself doing anything else other than indie games, saying that Fez has become his identity over time.

Polytron prepares to present Fez at   expresses his excitement about the product as well. Towards the closing of the show, Fishs new partner Ken Schachter announces that he and the former business partner had a meeting and have come to an agreement, which relieves Fish, who in the end is satisfied with the results of the show, and vows to continue working on the game and releasing it in 2012.

The film bookends itself with Jonathan Blows opening monologue about how indie gaming differs by offering flaws and vulnerabilities, making the games more personal. During the closing credits, videos of other upcoming indie games are interspersed.

==Reception==
  
Indie Game: The Movie received a high level of interest from the gaming community almost from its inception.        echoed these statements, stating that "there are victories, defeats, tears and smiles. Indie Game: The Movie is a must-see doc for anybody that fancies themselves a gamer or for anyone who gets sucked into a good underdog story." 

The film won the World Cinema Documentary Editing Award at the 2012 Sundance Film Festival. 

 Indie Game: The Movie was also named Best Documentary by the Utah Film Critics Association and nominated for a Canadian Screen Award in the category of Best Feature Documentary. 

==Release==
The producers chose a non-traditional distribution method for the film, coupling the usual film festival circuit with a focused theater tour and aggressive online distribution. Online distribution was initially via iTunes and the gaming platform   in the United Kingdom on 30 November 2013, and was preceded by a documentary "How Video Games Changed The World". 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 