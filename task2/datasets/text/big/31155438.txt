Kathirunna Nikah
{{Infobox film 
| name           = Kathirunna Nikah
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = M Raju Mathan
| writer         = KG Sethunath
| screenplay     =
| starring       = Prem Nazir Sheela Adoor Bhasi Thikkurissi Sukumaran Nair
| music          = G. Devarajan
| cinematography = Venkata Varanasi Janardanan
| editing        = N Pokkalath
| studio         = Thankam Movies
| distributor    = Thankam Movies
| released       =  
| country        = India Malayalam
}}
 1965 Cinema Indian Malayalam Malayalam film,  directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by M Raju Mathan. The film stars Prem Nazir, Sheela, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir
*Sheela
*Adoor Bhasi
*Thikkurissi Sukumaran Nair Ambika
*Bahadoor
*Haji Abdul Rahman Meena
*S. P. Pillai

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Agaadhaneelimayil || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Kandaalazhakulla || LR Eeswari, Chorus || Vayalar Ramavarma || 
|-
| 3 || Kaniyallayo || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Maadappiraave || A. M. Rajah || Vayalar Ramavarma || 
|-
| 5 || Nenmeni Vaakappoonkaavil || P Susheela || Vayalar Ramavarma || 
|-
| 6 || Pachakkarimbu Kondu || KP Udayabhanu || Vayalar Ramavarma || 
|-
| 7 || Swapnathilenne || P Susheela || Vayalar Ramavarma || 
|-
| 8 || Veettil orutharum || P Susheela, A. M. Rajah || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 