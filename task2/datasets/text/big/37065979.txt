Hollow (2011 horror film)
{{Infobox film
| name           = Hollow
| image          = Promotional poster.jpg
| caption        = Promotional poster
| director       = Michael Axelgaard
| writer         = Matthew Holt
| starring       = Emily Plumtree Sam Stockman Matt Stokoe Jessica Ellerby Simon Roberts
| studio         = Hollow Pictures
| distributor    = Metrodome Distribution
| released       =    
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
}} found footage horror film, directed by Michael Axelgaard, starring Emily Plumtree, Sam Stockman, Jessica Ellerby, and Matt Stokoe. The film is being distributed by Tribeca Film via nationwide On Demand outlets. Hollow premiered at Fantasia Festival. The film also screened at the Raindance Film Festival and was nominated for British Independent Film Award. 

==Plot==
The film tells the story of an "old monastery in a small, remote village in Suffolk, England that has been haunted by a local legend for centuries. Left in ruin and shrouded by the mystery of a dark spirit that wills young couples to suicide, the place has been avoided for years, marked only by a twisted, ancient tree with an ominous hollow said to be the home of great evil. When four friends on holiday explore the local folklore, they realize that belief in a myth can quickly materialize into reality, bringing horror to life for the town." 

==Cast==
* Emily Plumtree as Emma
* Sam Stockman as James
* Jessica Ellerby as Lynne
* Matt Stokoe as Scott

==References==
 

==External links==
*  
*   at Tribeca

 
 
 
 
 
 
 


 