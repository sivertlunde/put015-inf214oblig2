Successive Slidings of Pleasure
{{Infobox film
| name           = Successive Slidings of Pleasure
| image          = Successive Slidings of Pleasure.jpg
| caption        = Film poster
| director       = Alain Robbe-Grillet
| producer       = André Cohen Marcel Sébaoun
| writer         = Alain Robbe-Grillet
| starring       = Anicée Alvina
| music          = 
| cinematography = Yves Lafaye
| editing        = 
| distributor    = 
| released       = 7 March 1974
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
}}

Successive Slidings of Pleasure ( ) is a 1974 French fantasy film directed by Alain Robbe-Grillet.   

==Plot==
The film delves into the surreal and demented psyche of a young woman following the murder of her partner Nora. She is incarcerated in a convent prison where her sexual and sadistic desires interrupt her sense of reality.

==Cast==
* Anicée Alvina - The Prisoner
* Olga Georges-Picot - Nora
* Michael Lonsdale - The Judge
* Jean Martin - The Priest
* Marianne Eggerickx - Claudia
* Claude Marcault - Soeur Julia
* Maxence Mailfort - Client / Customer
* Nathalie Zeiger - Sister Maria Bob Wade - Fossoyeur / Gravedigger
* Jean-Louis Trintignant - The police Lieutenant
* Isabelle Huppert - Bit
* Hubert Niogret - Le photographe
* Alain Robbe-Grillet - Un passant
* Catherine Robbe-Grillet - Une soeur

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 