Paradise Camp
 
{{Infobox Film
| name           = Paradise Camp
| image          = Arbeitmachtfrei_01.jpg
| image_size     = 
| caption        = Gate at Theresienstadt Little Fortress
| director       = Frank Heimans
| producer       = Frank Heimans Paul Rea 
| narrator       = Peter Caroll
| starring       = 
| music          = Adam Hoptman
| cinematography = Geoff Simpson
| editing        = Frank Heimans
| distributor    = Cinetel Productions Pty Ltd 15 Fifth Avenue Cremorne NSW Australia 2090
| released       =  
| runtime        = 56 min.
| country        = Australia
| language       = English
| budget         = $136,000
}}
 documentary about Paul Rea Frank Heimans, respectively. Czechoslovakian Jews were first told that Theresienstadt was a community established for their safety. They quickly recognized it as a ghetto and concentration camp.

In 1944, the Nazis cleaned up the camp, painting buildings and planting flowers, and deporting inmates to reduce overcrowding, in order to fool international Red Cross officials on a visit into believing the Jews were being well cared for. That year, the Germans also filmed a propaganda documentary at Thereienstadt to promote how they were caring for Jews.

The 1986 film includes excerpts from the propaganda film, in contrast with interviews of survivors, other material about the facts of the camp, and examples of art made by prisoners, including thousands of childrens drawings hidden and preserved by their teacher.

==Summary== Czech Jews entering Theresienstadt at its opening. “They wanted to make their beautiful spa stay very nice, and, when they arrived in big barracks, they couldn’t understand what happened.”
 
Paradise Camp reveals how Nazi deception fooled Jews into entering Theresienstadt willingly. Elderly Jews were told the camp would be their safe haven, World War I veterans thought that their service to Germany was being rewarded, and prominent Jews thought they were being given special treatment for their German nationalism, with fine accommodations and protection from the war. But they all were soon held in densely overcrowded barracks, eating meager portions of bread, and fearing for their lives. Paradise Camp features interviews with survivors, who experienced the hunger, filth and terror that the Nazi officials intermittently masked, and displays old photographs and archival film footage, to reconstruct the truth about Theresienstadt.
 Austrian Empress Jewish ghetto, and transported tens of thousands of Jews there, forcing them to do the work to house and feed the large population. More than 150,000 Jews passed through Theresienstadt; most were killed in death camps. In late 1942, the Germans began to deport Jews from Theresienstadt to extermination camps throughout Eastern Europe, including Auschwitz, Majdanek and Treblinka. A smaller fortress on the other side of the river was used for political prisoners and, later, some Allied prisoners of war.

In 1944, preparing for a visit from the Red Cross (the Danish government had expressed concern about their nationals and the Germans were trying to maintain some Danish cooperation for forced labor), the Nazis had Jewish workers improve the complex, painting buildings and cleaning the streets, planting flower pots. The Germans deported thousands of inmates to concentration camps reduce overcrowding. They brought in ample props. That year they directed a Jewish prisoner filmmaker to produce a propaganda film portraying life at Theresienstadt concentration camp as comfortable and enjoyable. In the film, older women knit, a small boy waters a garden from an oversized water barrel, and everyone wears a lazy smile and a healthy layer of fat.
 

The 1986 documentary has interviews with survivors, who recount what life was really like at Theresienstadt.  One woman remembers eating crushed red brick and pretending it was paprika, while crushed bark served as an alternative spice. Another woman recalls that before the Red Cross inspection, the Nazis built children’s rooms painted in bright colors and lined with small beds. But the Nazis never told the Red Cross inspectors that the tiny beds were used by 17-year-olds, because all the younger children had already been deported and murdered in extermination camps.

Together with academic classes, prisoners arranged for drawing classes for children. Their teacher hid 4,000 drawings, which were found a decade after the war. Adult artists were also among the prisoners, and their work expressed the horror of daily life. The documentary shows their work: sketches of prisoners with sunken eyes, ragged clothes, and bony fingers document starvation and need.

==Filmmakers==
Paul Rea is an Australian journalist who in 1985 produced Where Death Wears a Smile, a documentary about Allied  . They paid the survivors $10,000 each in compensation. 

Frank Heimans is a director and producer.

==References==
 
* {{cite web
  | title = Paradise Camp
  | publisher = Media Rights
  | url= http://www.mediarights.org/film/paradise_camp
  | format =
  | doi =
  | accessdate =7 August  }}

*{{cite web
  | title = Paradise Camp
  | publisher= Because Films database
  | date =
  | url= http://ftvdb.bfi.org.uk/sift/title/330637
  | accessdate =7 August }}

==External links==
*  
* , reviewed by The Jewish Channel
*  , I Survived
*  
*  

 
 
 
 
 