Little Orvie
{{Infobox film
| name           = Little Orvie
| image          = 
| alt            = 
| caption        = 
| director       = Ray McCarey
| producer       = William Sistrom Frank Fenton Robert Chapin
| based on       =  
| starring       = Johnny Sheffield Ernest Truex Dorothy Tree Ann E. Todd Emma Dunn
| music          = Paul Sawtell
| cinematography = J. Roy Hunt
| editing        = Theron Warth 
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Frank Fenton and Robert Chapin. The film stars Johnny Sheffield, Ernest Truex, Dorothy Tree, Ann E. Todd and Emma Dunn. The film was released on March 1, 1940, by RKO Pictures.   

==Plot==
 

== Cast ==
*Johnny Sheffield as Orvie Orvie Stone
*Ernest Truex as Frank Stone
*Dorothy Tree as Clara Stone
*Ann E. Todd as Patsy Balliser 
*Emma Dunn as Mrs. Welty
*Daisy Lee Mothershed as Corbina
*Fay Helm as Mrs. Balliser
*Virginia Brissac as Mrs. Green
*Paul E. Burns as Angelo
*Dell Henderson as Mr. Brown
*Fern Emmett as Mrs. Jackson
*Edgar Dearing as Policeman
*Ray Turner as Jefferson 

== References ==
 

== External links ==
*  

 
 
 
 
 
 