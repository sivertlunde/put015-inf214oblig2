Lake of the Dead
{{Infobox film name           = Lake of the Dead  De dødes tjern  image          = Lake of the Dead (1958 film) coverart.jpg image_size     = caption        = Video Cover of "Lake of the Dead" director       = Kåre Bergstrøm producer       =  writer         = André Bjerke Kåre Bergstrøm starring       = Henny Moan Henki Kolstad André Bjerke	 music          = Gunnar Sønstevold cinematography = Ragnar Sørensen editing        = Olav Engebretsen narrator       = Kåre Bergman distributor    = Norsk Film released       = December 17, 1958 runtime        = 76 minutes country        = Norway location       = Jotunheimen, Norway language  Norwegian
|budget         =  gross          = preceded_by    =  followed_by    = 
}} 1958 Cinema Norwegian horror directed by Kåre Bergstrøm. The film stars Henki Kolstad, Henny Moan and Georg Richter.

==Plot==
The film takes place 20–23 August 1958. Crime Author Bernhard Borge and his wife Sonja, psychoanalyst Kai Bugge, magazine editor Gabriel Mørk, lawyer Harald Gran and his fiancée Liljan Werner are six Oslo people who will visit Bjørn Werner (Liljan brother) in his cabin deep in the Østerdal forests. But, when the guests arrive, Werner is missing and his dog is found dead at a pond nearby.
Its not long before they begin to ponder the old legend that is associated with the place: a man is said to have killed his sister and her lover and then drowned himself in the lake. It is said that everyone who stays in the house - the murders cabin - would be possessed by a strange attraction: They would be forced to drown themselves in the pond.
The company decides to solve the mystery, but soon, it appears that they are exposed to the mysterious, fascinating powers that are tied to the lake.

==Cast==
* André Bjerke as Gabriel Mørk
* Bjørg Engh as Sonja, Bernhards wife
* Henki Kolstad	as Bernhard Borge, crime writer
* Per Lillo-Stenberg as Bjørn Werner, Liljans brother
* Erling Lindahl as Kai Bugge, psychologist
* Henny Moan as Liljan Werner
* Øyvind Øyen as Bråten, policeman
* Georg Richter as Harald Gran
* Leif Sommerstad as Tore Gruvik, the ghost
* Inger Teien as Eva, Bjørns girlfriend

==Production==
The movie is based on the 1942 novel by the same name, written by André Bjerke, the movie is shot all Black-and-white.

==Reception==
 
Lake of the dead was nominated as the fourth best film of all time in Norway in a test developed by Dagbladet in 1998, were 101 movie critics gave Norwegian movies points.  Lake of the dead is in hard competition with Villmark for the title of best Norwegian Horror movie of all time. Both movies take place by a lake. A DVD was released October 14, 2009, with an English subtitle feature. Before that, the movie was hardly known outside of Norway.

==References==
 

==External links==
*  

 
 
 
 
 