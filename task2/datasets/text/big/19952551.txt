Souls Protest
{{Infobox film
| name           = Souls Protest
| film name      = {{Film name
 | hangul         =     
 | hanja          = 살아있는  들
 | rr             = Sara-inneun ryeonghondul
 | mr             = Sara-innŭn ryŏnghondŭl
 | context        = north}}
| director       = Kim Chun-song
| starring       = Chul Kim Kim Ryon-hwa
| released       =  
| runtime        = 100 minutes
| country        = North Korea
| language       = Korean
}} North Korean film directed by Kim Chun-song. The film is an epic dramatisation of the Ukishima Maru incident in which hundreds of Koreans were killed when the ship was sunk by a mysterious explosion, and supports the Korean view that the explosion was deliberately set off by the ships Japanese crew. It has been dubbed as "Koreas Titanic (1997 film)|Titanic". 
 Japanese colonial rule. The film was shown intact, however, for its Seoul premiere on 24 August 2001, the 56th anniversary of the incident. One survivor of the incident, Lee Chul-woo, said of the film: "I didnt like the propaganda stuff about Kim Il Sung... But the scene about the explosion was so real, and it is laudable for North Korea to make a movie about this incident." 

Souls Protest was later screened at the 2003 Jeonju International Film Festival. 

==References==
 
 
*  . Peoples Korea, 25 July 2001. Retrieved on 27 October 2008.
* Rajpal, Minita.  . CNN.com, 7 September 2001. Retrieved on 27 October 2008.
* Cho, Grace M..  . The Affective Turn, pp.&nbsp;151–169. Duke University Press, 2007. ISBN 978-0-8223-3925-0.
 

==External links==
*  

 

 
 
 
 

 