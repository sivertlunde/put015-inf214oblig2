Home, Sweet Home (1914 film)
 
 
{{Infobox film
| name           = Home, Sweet Home
| image          =Home, Sweet Home (1914 film).jpg
| image_size     =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = H. E. Aitken D. W. Griffith
| starring       = Earle Foxe Henry Walthall Dorothy Gish Lillian Gish
| music          =
| cinematography = G. W. Bitzer
| editing        = James Smith Rose Smith
| distributor    = Mutual Film
| released       =  
| runtime        = 55 minutes
| country        = United States Silent English intertitles
| budget         =
}}
  silent biography|biographical drama directed by D.W. Griffith.     The film starred Earle Foxe, Henry Walthall and Dorothy Gish in the lead roles.

== Plot ==
John Howard Payne leaves home and begins a career in the theater.  Despite encouragement from his mother and girlfriend, Payne begins to lead a dissolute life that leads to ruin and Clinical depression|depression.  In deep despair, he thinks of better days, and writes a song, Home! Sweet Home! that later provides inspiration to several others in their own times of need.

== Cast ==
* Henry B. Walthall ....  John Howard Payne
* Josephine Crowell ....  Paynes Mother
* Lillian Gish ....  Paynes Sweetheart
* Dorothy Gish ....  Sister of Paynes Sweetheart
* Fay Tincher ....  The Worldly Woman
* Mae Marsh ....  Apple Pie Mary
* Spottiswoode Aitken ....  Marys Father
* Robert Harron ....  The Easterner, Robert Winthrop
* Miriam Cooper ....  The Fiancee
* Mary Alden ....  The Mother
* Donald Crisp ....  The Mothers Son
* Earle Foxe James Kirkwood ....  The Mothers Son
* Jack Pickford ....  The Mothers Son Fred Burns ....  The sheriff
* Courtenay Foote ....  The Husband
* Blanche Sweet .... The Wife

==See also==
*Lillian Gish filmography
*Blanche Sweet filmography

== References ==
 

== External links ==
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 