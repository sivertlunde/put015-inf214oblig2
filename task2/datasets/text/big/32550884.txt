By Berwin Banks
{{Infobox film
| name           = By Berwin Banks
| image          =
| alt            =
| caption        =
| director       = Sidney Morgan
| producer       =
| writer         = Hugh Croise Allen Raine
| starring       = Langhorn Burton Eileen Magrath J. Denton-Thompson Charles W. Somerset
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = United Kingdom
| language       =
| budget         =
| gross          =
}} British silent silent romance film directed by Sidney Morgan and starring Langhorn Burton, Eileen Magrath and J. Denton-Thompson. 

==Cast==
* Langhorn Burton - Cardo Wynne 
* Eileen Magrath - Valmai Powell 
* J. Denton-Thompson - Owen Davies 
* Charles W. Somerset - Essec Powell 
* Arthur Lennard - Reverend Menrig Wynne 
* Judd Green - Joe Powell 
* Charles Levey - Reverend Gwynne Ellis 

==References==
 

==External links==
*  

 

 
 
 
 
 

 