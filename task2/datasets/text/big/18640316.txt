Ashurajō no Hitomi
{{multiple issues|
 
 
}}
{{Infobox Film
| name           = Ashurajō no Hitomi 阿修羅城の瞳
| image          = 
| caption        = Movie cover 
| director       = Yōjirō Takita
| producer       = Shochiku
| writer         = 
| starring       = Somegorō Ichikawa Rie Miyazawa Kanako Higuchi Atsuro Watabe Takashi Naito Erika Sawajiri
| music          = Yoko Kanno
| cinematography = 
| editing        = 
| country        = Japan
| language       = Japanese
| distributor    = Shochiku Co., Ltd.
| released       = 2005
| runtime        = 119 minutes
}} play set in the 16th century. It has been released on DVD-Video as Ashura in United Kingdom by Yume Pictures in 2006 and in the United States by AnimEigo in 2007.

== Story ==
A beautiful Oni (folklore)|oni named Bizan (Kanako Higuchi) appears on Earth. She wants to bring demon queen Ashura back to life, so the demons can take over the world beginning in Edo (modern day Tokyo). Opposing her is the Oni Makado, an army of demon slayers that was created by the Japanese government. Izumo (Somegorō Ichikawa) once fought with the Makado, but resigned to become a kabuki actor after he thought he accidentally killed a young girl. After 5 years, Izumo encounters Tsubaki (Rie Miyazawa), who is a member of an all-female acrobatic troupe who rob the citizens of Edo during the night. Falling deeply in love, Izumo notices a red, ugly scar on Tsubakis shoulder, which is the mark of Ashura. Meanwhile, Jaku, one of Izumos ex-comrades (Atsuro Watabe), falls for Bizan, leading the Oni Makado over to the dark side. 

Jaku desires the power of Ashura and captures Tsubaki, but Izumo rescues her. The passion between Izumo and Tsubaki eventually causes the spirit of Ashura to awaken within the latter. After introspection, both Izumo and Tsubaki recall the event they were unable to remember from 5 years ago. This explains why their supposedly chance meetings always invoked a feeling of familiarity.

With great disdain for the transgressions in her human life, Tsubaki leaves for the demon world to fulfill her destiny as their queen. In forlorn pursuit, Izumo arrives at Ashuras inverted sky castle of the demon world and slays Jaku, and then Bizan. Eventually Izumo finds Ashura and fights to the death.

==Cast==
{{columns-list|3|
*Somegorō Ichikawa as Izumo
*Rie Miyazawa as Tsubaki
*Kanako Higuchi as Bizan
*Atsuro Watabe as Jaku Abe
*Takashi Naito as Nobuyuki Kuninari
*Erika Sawajiri as Yachi
*Fumiyo Kohinata as Nanboku Tsuruya IV
*Yukijiro Hotaru as Magotaro
}}

==Music== Sting and features piano playing by Herbie Hancock.

== DVD extras ==
The two disc set contains special features on the second disc. Two   with behind-the-scenes footage and interviews with the cast and crew. The special features documentary is 15 minutes long. Also included are a photo gallery, which is viewed without a remote, trailers for other releases from the same distributor and production notes for Ashura.

== References ==
 

 

 
 
 
 
 
 
 
 
 