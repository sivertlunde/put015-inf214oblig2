Starting Over Again (film)
{{Infobox film
| name           = Starting Over Again
| image          = Starting Over Again movie poster.jpg
| border         = yes
| caption        = Theatrical movie poster Olivia M. Lamasan
| producer       = 
| writer         = Carmi Raymundo
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
{{plainlist|
* Toni Gonzaga
* Piolo Pascual
* Iza Calzado
}}
| music          = 
| cinematography = 
| editing        = 
| distributor    = Star Cinema
| released       = 
{{plainlist|
* February 12, 2014
*  (Philippines) 
* February 21, 2014
*  (North America) 
* March 6, 2014
*  (Australia & the Middle East) 
}}
| runtime        = 
| country        = Philippines Filipino English English
| budget         = 
| gross          = 
{{plainlist|
* Domestic:
* PHP 410,188,028 
* Foreign:
* PHP 78,380,000   
* Australia:
* PHP 4,038,777 
* Total Box Office:
* PHP 492,606,805
}}
}} Filipino romantic Olivia M. Lamasan, starring Piolo Pascual and Toni Gonzaga. It was produced and distributed by Star Cinema as part of its 20th anniversary.    It officially premiered in the Philippines on February 12, 2014.   
 pesos on its opening day.  On March 15, 2014, a month after its theatrical release, the film earned a total domestic gross of 400 million pesos becoming the first non-Metro Manila Film Festival film to reach the said amount.    As of April 1, 2014, the movie registered Php 491.67 million both in domestic and international box offices.

==Cast==
===Main Cast===
* Toni Gonzaga as Genina "Ginny" Gonzales
* Piolo Pascual as Marco Antonio Villanueva III
* Iza Calzado as Patricia "Patty" de Guia

===Supporting Cast===
* Joross Gamboa as Ali   
* Edgar Allan Guzman as Baba
* Cai Cortez as Beb   
* Beauty Gonzalez as Wella 
* Bryan Santos as Migo Samanego 
* Lito Pimentel as Ginnys Father
* Yayo Aguila as Ginnys Mother

===Cameos===
 
* Vhong Navarro
* Luis Manzano
* Sam Milby
* Paul Soriano

==Development==

===Production=== In My In the The Mistress (2012).    According to Lamasan, the film was about a lover wishing for a second chance.    Despite being the head of ABS-CBN|ABS-CBNs creative department, Lamasan was told to do a film that would fit Pascual to make their reunion worth it since they havent done any projects for the last decade.  The film also served as a reunion not only with Pascual and Lamasan, but for the lead actor and actress as well.  Their first collaboration was 13 years ago in a soda commercial.   In an article by Crispina Martinez-Belen of the Manila Bulletin, it was stated that Malou Santos personally chose Gonzaga to portray the lead role. 

===Promotions===
The official trailer was released on YouTube on January 18, 2014 and became viral. 

==Release== Fox Theatre in Redwood City, California, United States on February 15, 2014.   It was later released commercially starting February 21 to 27 in the United States, February 21 to March 6 in Canada, March 6 in Australia and the Middle East, specifically in Dubai, Qatar and Bahrain. 

==Critical reception== average rating of 4 out 5. 

===Reviews=== One More Chance in terms of story and character. Tantiangco awarded the film 3.5 out of 5 stars and said that it is a "funny" and well-written romantic-comedy flick. However, she criticizes Gonzagas character as "shallow," but noted Pascuals character as "mature" and "outstanding." 

Rapplers Zig Marasigan praised how the pacing of the movie went, making the audience realize that a perfect love story can also have flaws. He also called the story "realistic, satisfying, and brave". However, he criticizes the films tone and said that it is questionable inserting comedy in a dramatic and painful scene.  Florence Hibionada of The Daily Guardian said that the scenes are "memorable" and worth it. She also noted the films unpredictability and sense of direction.  Rianne Hill Soriano of Yahoo! Voices expressed dismay at most scenes that she described as "annoying." She criticizes the pre-climax scenes as "lame" and the hospital scene for the characters closure as "disappointing."  Philbert Ortiz-Dy of ClickTheCity.com awarded the film 2 stars out of 5, stating that the supposedly mature film turned to a completely novice-made one. He also stressed that it "failed to take its own advice," wherein the film ended up the old way. 

==Box office== pesos on Box Office Mojo International, the film was consistently number one in the box office since February 19 to 23.  On March 15, 2014, after a month since it was released, the film had already earned 400 million pesos. 

As of April 1, 2014, the film has already earned US$ 1,741,838 or PHP 78.38 million pesos in the United States after being screened in 52 cinemas and theaters, according to Los Angeles-based Hollywood news site, moviecitynews.com.    With a total of PHP 491.67M in both domestic and international box office, it easily surpassed Girl, Boy, Bakla, Tomboy as the highest grossing Filipino film to date.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 