The Girl on the Train (2009 film)
 
 
{{Infobox film
| name           = The Girl on the Train
| image          = La fille du RER.jpg
| caption        = Theatrical release poster
| director       = André Téchiné
| writer         = André Téchiné Odile Barski  Jean-Marie Besset
| starring       = Emilie Dequenne  Catherine Deneuve  Michel Blanc
| music          = Philippe Sarde
| cinematography = Julien Hirsch
| editing        = Martine Giordano
| distributor    = Strand Releasing
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = €5,834,380   
| gross          = $1,538,762 
}}
The Girl on the Train ( ) is a 2009 French drama film directed by André Téchiné, starring Emilie Dequenne, Catherine Deneuve and Michel Blanc. The plot centers on an aimless girl who lies about being the victim of a hate crime.   

==Plot==
Jeanne Fabre, an attractive late-teen-aged  carefree loner, spends her time rollerblading through Paris and job-hunting, a nuisance she endures to indulge her widowed mother, Louise, who runs a day-care center out of their house. As they are watching a news program on television about anti-semitic attacks, Louise recognizes a person on the TV: Samuel Bleistein, a prestigious Jewish lawyer, who was many years ago in love with her. Louise arranges a job interview for her daughter at Bleistein’s law firm.

On his part, Samuel Bleistein receives the visit of his son Alex, who has comes to Paris to bar mitzvah. Alexs encounter with his ex-wife Judith, who is Samuel’s assistant, is tense since they do no get along.

Jeanne’s job interview is a disaster. Evidently she is ill qualified and badly prepared. Unfazed by the interview, Jeanne resumes rollerblading and unexpectedly meets Franck a young wrestler, who takes an instant interest on her. A relationship ensues and the couple eventually move-in together when Franck finds a job as the caretaker in an electrical shop in the owner’s absence. The place turns out to contain hidden drugs and Franck is badly wounded in a fight with a drug dealer, who flees. The police let Jeanne go but arrest Franck, who rejects her when she visits him at the hospital. He knew she was lying the whole time during the relationship about having a job.

Heartbroken and angered, she goes back home to live with her mother. During the night, she wakes up from her bed and grabs a black marker. After drawing three swastikas on her body, she then takes a knife and proceeds to minimally cut herself in a few places, and then uses scissors to cut off part of her hair. She then sneaks quietly outside into the night. She soon alleges to the police to have been brutally attacked by hoodlums on the RER train because they thought she was Jewish. The incident becomes a huge national cause célèbre.  Louise, however, believes her daughter has lied about the incident.

Meanwhile Alex, indisposed towards his ex-wife, decides not to go to Nathan’s bar mitzvah. Judith begs him to reconsider. Behind their mutual apparent animosity, they still love each other. At his hotel room, they make love and reconcile.

Louise asks Samuel for help about Jeannes problem. Samuel suggests that they hear this through, and wants her to come and stay at his house for the night. Louise and Jeanne board a train and travel to meet Samuel and his family at his country house by a lake. As Samuel drives the pair to his home, Nathan, also in the car, whispers to Jeanne, accusing her of lying about the whole affair. When all gather for dinner, Jeanne sticks to the same story she told the police: six youths approached her and, assuming she was Jewish (which she is not), proceeded to assault her. After some extensive questioning, she decides to call it a night, but instead walks away and crosses the lake in a row-boat.

Nathan helps Jeanne when it starts to rain hard and invites her into his little shack, a safe haven to get away from his parents once in a while. He notices she is all wet, and has her take off her clothes. Jeanne strips down and sits next to the fireplace with Nathan. She shows him her scars, as he still does not believe her story, but confesses in the end she made it up. Nathan convinces her to confess she lied. The next morning Jeanne confesses the truth to Samuel. Samuel has her write and sign an open apology to all who were affected by the story. Jeanne and Louise return to Paris by train.

Jeanne goes to the police and is put in jail for 48 hours for her serious false statements. She eventually receives a suspended sentence and is required to attend psychiatric counseling. When Franck is interviewed by Samuel about Jeanne, Franck says he was in love with her, despite her lying, and would do it all over again given the chance.

Samuel attends Nathan’s bar mitzvah, when he also sees television footage of reporters interviewing Louise about the scandal. When they ask her about how her daughter knew the name of Bleistein, Louise lies and replies she does not know. Jeanne returns to live with her mother. She searches the internet for secretarial jobs. She receives a postcard from Nathan, who is in love with her. Jeanne is last seen rollerblading on a long path through trees.

As the ending credits begin to roll, they say the story is inspired by true events.

==Cast==
* Emilie Dequenne as Jeanne
* Catherine Deneuve as Louise
* Michel Blanc as Samuel Bleistein
* Mathieu Demy as Alex
* Ronit Elkabetz as Judith
* Nicolas Duvauchelle as Franck
*Jérémie Quaegebeur as Nathan

==Production==
The Girl on the Train has its genesis on a real life case that made headlines in France. Marie Leonie Leblanc, a woman in her twenties, walked into a police station in Paris on 9 July 2004 claiming she had been the victim of an antisemitic attack on a suburban RER train. According to her account, six men of North African descent ripped her clothes, cut some of her hair and daubed a swastika on her stomach, knocking over the pram containing her baby. Sight & Sound, June 2010. Interview  with Téchiné about The Girl on the Train, p.8  
Fellow passengers did nothing to help. The case provoked national outrage for its virulent antisemitism; politicians and the media seized on the incident.  
President Jacques Chirac, condemned the "shameful act", while Israels  prime minister Ariel Sharon advised French Jews to emigrate to Israel to avoid " the wildest antisemitism".  
Four days later Leblanc, who was not Jewish herself, admitted she had made the whole affair up. The revelation that  the incident was a total invention created consternation and further outrage, particularly criticized was the media sensational exploration of the affair. 

The case inspired Jean-Marie Bessets  2006 play RER which in turn was the base for Téchinés film script. Téchiné was interested in what he called the "human truth" behind the case.  
"I wanted to explore the genealogy of a lie, how it came to being. Thats why I divided the film into two parts. The first is the circumstances, so you see the context under which the young woman was able to construct her lie. You see the difference elements that she takes from the context around her and puts into. Bleinstein, whose name she has taken. Its the name on the business card found in her bag, which she claims is the reason for being attacked. That was how I constructed the story." 
 Rosetta (1999). "I didnt want Jeanne to be depressive or a melancholic character," Téchiné explained. "I wanted her to be physical and athletic, which is why we came up with the idea of her rollerblading. Its significant that she falls in love with a top class athlete, which is based on the fact that the girls real life lover was an athlete. And alongside her athleticism, Emilie has a day dreaming quality. In real life she is about 30, but in the film she looks much younger and more childlike." 

==Reception==
The film garnered a favorable critical reaction, holding a fresh rating of 79% on Rotten Tomatoes.    Critics single out for praise the performances of Emilie Dequenne. {{cite news
 | last = Scheib
 | first = Ronnie 
 | title =The Girl on the Train Variety
 | date = March 10, 2009
 | url = http://variety.com/2009/film/reviews/the-girl-on-the-train-1200474224/ | accessdate = 2011-04-24 }}  Metacritic gave the film an average score of 68/100, indicating "generally favorable reviews".

James Berardinelli from ReelViews called the film "a compelling piece of cinema". {{cite news
 | last =Berardinelli
 | first =James
 | title =Girl on the Train, The
 | work =Reelviews
 | date = January 12, 2011
 | url = http://www.reelviews.net/php_review_template.php?identifier=2072
 | accessdate = 2011-04-24 }} 
In his review for   from the San Francisco Chronicle commented that "What its really about – and this sounds so boring, and so nothing, when in fact its really rather wonderful – is people. Just regular people, a mother and daughter, whose lives are observed with economy and precision, and with an eye for the telling detail and the tense, revealing moment." {{cite news
 | last = LaSalle
 | first =Mike
 | title =Girl on the Train, The
 | work =San Francisco Chronicle
 | date = April 22, 2010
 | url = http://www.sfgate.com/movies/article/Review-Girl-on-Train-notable-for-interactions-3195449.php | accessdate = 2011-04-24 }}  In Variety (magazine)|Variety Ronnie Scheib said that in  The Girl on the Train    Téchiné fashions a brilliantly complex, intimate multi-strander, held together but somewhat skewed by the central performance of Emilie Dequenne." 

Steven Rea from the Philadelphia Inquirer commented that "Presented with an economy and emotional cool that add to, rather than subtract from, its dramatic impact, The Girl on the Train reverberates with a quiet, seductive power." {{cite news
 | last = Rea
 | first =Steven 
 | title =Complex story of a hate crime
 | work =Philadelphia Inquirer
 | date = April 23, 2010
 | url = http://articles.philly.com/2010-04-23/entertainment/24956869_1_anti-semitic-attack-crime-nicolas-duvauchelle| accessdate = 2011-04-24 }}  Rene Rodriguez in his review for The Miami Herald concluded: "Like Techines best films, the movie appears to be a story about nothing - until it suddenly becomes a meditation on the vagaries of the human heart." {{cite news
 | last = Rodriguez
 | first =Rene 
 | title =The Girl on the Train
 | work =The Miami Herald
 | date = March 23, 2010
 | url = http://miamiherald.typepad.com/reeling/2010/03/review-the-girl-on-the-train.html| accessdate = 2011-04-24 }} 
In the New York Times Manohla Dargis called the film "A seductive drama." {{cite news
 | last = Dargis
 | first = Manohla
 | title =The Girl on the Train
 | work =The New York Times
 | date = January 21, 2010
 | url = http://www.nytimes.com/2010/01/22/movies/22girl.html?_r=0| accessdate = 2011-04-24 }} 

==Notes==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 