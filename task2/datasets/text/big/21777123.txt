Mass Appeal (film)
{{Infobox film
| name           = Mass Appeal
| image          = MassAppealPoster.jpg
| image_size     =
| caption        = Original poster
| director       = Glenn Jordan
| producer       = David Foster Lawrence Turman
| writer         = Bill C. Davis
| narrator       =
| starring       = Jack Lemmon Željko Ivanek
| music          = Bill Conti
| cinematography = Donald Peterman John Wright
| studio         =
| distributor    = Universal Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,945,658 
}}
 dramedy film play of the same title.

==Plot== seminarian Mark Dolson, who questions Farleys position on the ordination of women. The older priest charmingly sidesteps the young man but is annoyed that he was placed in an uncomfortable position. This is a man who relies on charm, harmless white lies, and inane jokes when interacting with his parishioners, and he always has been careful not to get involved in controversial issues.

Dolson defends two seminarians who were expelled after being suspected of engaging in a homosexual relationship. After he is ordained a deacon, frustrated Monsignor Thomas Burke assigns him to Farleys parish in the hope the older man will inspire him to toe the line and become more complacent. Although in some ways conservative—he criticizes his sister Liz for her affair with a married man—the young man primarily is a liberal firebrand who is anxious to make changes in the church, whereas Farley prefers  study with a bottle of alcohol and not make waves.

The pastor tries to become a mentor to his new charge, but Dolson ignores the priests efforts to teach him the necessity of tact. He enrages the congregation with his first, highly critical sermon.

Questions as to why Dolson defended the gay seminarians arise. He confides having spent two years engaging in sexual relations with both men and women, saying he now is committed to celibacy. Farley urges him to keep quiet about his past, but the deacon admits his secret to the monsignor and is expelled.

Farley promises to convince his followers that the church needs liberal thinkers who dont always do things by the book. As soon as he senses he is losing support, however, the priest backs down. Dolson angrily confronts him with a feeling of betrayal, forcing Farley to rethink his position and do the right thing, even if it means the loss of his parish.

==Cast==
*Jack Lemmon ..... Father Tim Farley
*Željko Ivanek ..... Deacon Mark Dolson
*Charles Durning ..... Monsignor Thomas Burke
*Louise Latham ..... Margaret
*Talia Balsam ..... Liz Dolson

==Critical reception== Educating Rita, although she found it to be "less strident . . . and more prone to dry humor." She added, "The momentum of Mr. Daviss drama and the stars intensity are enough to sustain interest, even when Glenn Jordans television-style direction seems excessively bland. The casting of the two key roles works in the long run, but it initially seems a shade off. Father Farley, as written, is rather too self-satisfied and facile for the priesthood, qualities better emphasized in Milo OSheas stage performance than in Mr. Lemmons on film, since the characters glibness comes too close to the actors usual screen persona. And Mr. Ivanek, beginning on a note of intelligence and severity, later has moments of surprisingly callowness, even petulance. But the stars work together very effectively, making the storys progress believable as each of their characters evolves into a better man. Mass Appeal doesnt have to tug too hard at the audiences heartstrings to arrive at its simple and satisfying resolution." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 