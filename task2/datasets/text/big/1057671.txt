Everyman's Feast
{{Infobox film name        = Everymans Feast image       = caption     = Everymans Feast film poster director    =   writer      = Fritz Lehner starring    = Klaus Maria Brandauer Juliette Gréco Sylvie Testud producer    = Veit Heiduschka distributor = budget      = released    =   runtime     = 173 minutes country     = Austria language    = German, French
}}
Everymans Feast, also known as Jedermanns Fest, is a 2002 Austrian drama film written and directed by  . It was entered into the 24th Moscow International Film Festival.   

==Plot==
Jan Jedermann (played by Klaus Maria Brandauer) is an ingenious fashion designer, but also an unscrupulous showman. He dies in a car accident and ruminates during his last hours about how one more night in his life could have gone by. He just cannot accept that Death comes to get him on the evening of his greatest triumph, a fashion show set on the roof of the Vienna Opera. He makes a pact with Death, allowing him to remain among the living at least until the morning.

==Cast==
* Klaus Maria Brandauer as Jan Jedermann
* Juliette Gréco as Yvonne Becker
* Redbad Klynstra as Daniel
* Sylvie Testud as Sophie
* Alexa Sommer as Isabelle
* Veronika Lucanska as Cocaine
* Susan Lynch as Maria
* Piotr Wawrzynczak as Jurek

==References==
 

==External links==
* 

 
 
 
 
 
 
 