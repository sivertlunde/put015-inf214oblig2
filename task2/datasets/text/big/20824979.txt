Alucarda
{{Infobox film
| name           = Alucardam
| image          = Alucardaposter.jpg
| image size     =
| caption        = original poster for Alucarda
| director       = Juan López Moctezuma
| writer         = Alexis Arroyo  Juan López Moctezuma
| starring       = Tina Romero Claudio Brook Susana Kamini David Silva
| distributor    = Yuma Films, Films 75
| released       =   
| runtime        = 85 min.
| country        = Mexico English
| budget         =
| gross          =
}}
 possessed by Satan. Though it is a Mexican Spanish language film, it was originally filmed in English, as evidenced by the fact that the lip movements match the dubbed English dialogue. It was released in theaters across Mexico on January 26, 1978. 

==Plot outline==
The film takes place in a Mexican convent that houses nuns and is also an orphanage. Alucarda, a fifteen-year-old orphaned girl, has lived at the convent her entire life. Justine, another orphan girl of similar age, arrives at the convent. She and Alucarda become very close friends and form a relationship that borders on being Human sexuality|sexual.
 gypsies and Satanic entity and wreak havoc on the convent and its inhabitants.

==Cast==
*Tina Romero as Alucarda/Lucy Westenraa
*Susana Kamini as Justine
*David Silva as Father Lázaro
*Claudio Brook as Dr. Oszek/Hunchbacked Gypsy
*Lily Garza as Daniela Oszek
*Tina French as Sister Angélica
*Birgitta Segerskog as Mother Superior
*Adriana Roel as Sister Germana
*Martin LaSalle as Brother Felipe

==Themes==
 
Alucarda is notorious for its extreme subject matter and themes, which includes that of Satanism, murder, demonic possession, exorcism, orgy|orgies, and lesbianism, among other things, within a religious setting. All of these things made the film controversial, especially for the time it was made. Because of its extreme violence, scenes of sacrilege, and perversely defiled religious imagery, it has gained notoriety among fans of the horror genre. Although the film continues to be situated in this genre, it is undergirded by anti-government and anticlerical sentiments that are manifest in the exaggerated idolatry, representations of clerics as tyrants and persecutors, and overt iconoclasm. 
 The Devils The Exorcist because Alucarda was released within five years of both of them and shares similar plot themes.

==Reception==
After the films release in January 1978 in Mexico, it never received much attention from critics nor audiences, but over the years became something of an underground film and a B-movie cult classic. While it is not widely known by horror fans, many fans who have seen it consider it an unrecognized gem.

Columnist Michael Weldon of the Psychotronic Video Guide said the film was "The strongest, most imaginative, and visual witch movie since Ken Russells "The Devils"."  Well-known Mexican filmmaker Guillermo del Toro has also expressed his appreciation for the film and other works from director Juan López Moctezuma. 

== See also ==
*Satánico pandemonium: a Mexican horror movie also dealing with similar themes from the 1970s

==References==
 

==External links==
* 
*  at  
* 
*  at the Dread Central

 
 
 
 
 
 
 