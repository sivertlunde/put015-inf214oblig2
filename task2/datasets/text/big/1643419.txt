The Interpreter
 
 
{{Infobox film
| name           = The Interpreter
| image          = Interpreterthe.jpg
| caption        = Film poster
| director       = Sydney Pollack
| producer       = Tim Bevan Eric Fellner Kevin Misher Brian Ward (story) Charles Randolph Scott Frank Steven Zaillian David Rayfiel (uncredited)
| starring       = Nicole Kidman Sean Penn Catherine Keener
| music          = James Newton Howard
| cinematography = Darius Khondji
| editing        = William Steinkamp
| studio         = Working Title Films Mirage Enterprises
| distributor    = Universal Pictures
| released       =            
| runtime        = 128 minutes
| country        = United Kingdom United States Ku
| budget         = $80 million
| gross          = $162,944,923
}}
The Interpreter is a 2005 political thriller film starring Nicole Kidman, Sean Penn, and Catherine Keener. It was the final film to be directed by Sydney Pollack before his death in 2008.

==Plot== indicting Edmond Earl Cameron), General Assembly, in an attempt to avoid the indictment.
 Ku (an guerrilla group, that her parents and sister were killed by land mines laid by Zuwanies men, and that she has dated one of Zuwanies political opponents. Although Keller is suspicious of Silvias story, the two grow close and Keller ends up protecting her from attacks on her person. Silvia later finds that her brother Simon and her lover Xola were killed (as shown in the opening scene).

The purported assassin is discovered (and shot to death) while Zuwanie is in the middle of his address to the General Assembly, and security personnel rush Zuwanie to a safe room for his protection. Silvia, anticipating this, has been hiding in the safe room, and confronts Zuwanie and intends to kill him herself. Keller determines that the assassination plot is a false flag operation created by Zuwanie to gain credibility that his rivals are terrorists and to deter potential supporters of his removal. Keller rushes to the safe room and arrives just in time to prevent Silvia from murdering Zuwanie. Zuwanie is indicted, and Silvia is expelled from the U.S., returning home to Matobo soon afterwards.

==Production==
The Interpreter was shot almost entirely in New York City. The opening sequence was shot in Mozambique with a support crew made up largely of South African nationals.

===Filming in U.N. buildings=== Security Council chambers. The producers approached the U.N. about filming there before, but their request was turned down. The production would have relocated to Toronto with a constructed set; however, this would have substantially increased costs, and so Sydney Pollack approached then-Secretary-General of the United Nations|Secretary-General Kofi Annan directly, and personally negotiated permission to film inside the United Nations. Annan commented on The Interpreter that "the intention was really to do something dignified, something that is honest and reflects the work that this Organization does. And it is with that spirit that the producers and the directors approached their work, and I hope you will all agree they have done that."

Ambassadors at the U.N. had hoped to appear in the film, but actors were asked to play the roles of diplomats. Spains U.N. Ambassador Inocencio Arias jokingly complained that his "opportunity to have a nomination for the Oscar next year went away because of some stupid regulation." 

===Matobo and Ku===  Swahili and Shona language|Shona, with some unique elements.

The films tagline, "The truth needs no translation.", in Ku is Angota ho ne njumata.

==Cast==
* Nicole Kidman as Silvia Broome
* Sean Penn as Tobin Keller
* Catherine Keener as Dot Woods
* Jesper Christensen as Nils Lud
* Yvan Attal as Philippe Earl Cameron as Edmond Zuwanie George Harris as Kuman-Kuman Michael Wright as Marcus Tsai Chin as Luan
* Clyde Kusatsu as Police Chief Lee Wu
* Eric Keenleyside as Rory Robb
* Hugo Speer as Simon Broome
* Maz Jobrani as Mo
* Yusuf Gatewood as Doug
* Sydney Pollack as Jay Pettigrew
* Curtiss Cook as Ajene Xola
* Byron Utley as Jean Gamba
* Satish Joshi as U.N. Secretary General

==The Interpreter and Zimbabwe==
There are strong parallels between President Robert Mugabe and the character Dr. Zuwanie in the film, as well as between Matobo and Zimbabwe – which banned the film after it had been shown in the country. The parallels include:
* Both Robert Mugabe and Zuwanie were once regarded as legitimate "freedom fighters".
* In real life, President Mugabe had ruled Zimbabwe for 25 years when the film was released. The films Zuwanie had been in power for 23 years.
* Australia and New Zealand are pushing for President Mugabe to be indicted by the United Nations Security Council|U.N. Security Council for trial before the International Criminal Court on charges of crimes against humanity; Zuwanie is indicted by the Security Council for trial before the International Criminal Court on charges of crimes against humanity.
* Both President Mugabe and Zuwanie were teachers before being involved with politics.
* President Mugabe tends to wave his fist; Zuwanie his gun.
* Zuwanie is portrayed as collaborating with a Dutch mercenary to arrange an assassination attempt on him, since – as described in the film – "a nearly-assassinated president gains credibility and sticks around to enjoy it." In reality, opposition leader Morgan Tsvangirai was caught on tape trying to arrange an assassination attempt on Mugabe. He was tried for treason but later exonerated based on the ambiguity of the word "eliminate" President Mugabe. Tsvangirai wanted to hire Ari Ben-Menashe, an ex-Israeli secret service agent, for the job, but the latter taped the conversation and made it public.
* President Mugabe has a preoccupation with the British and frequently accused Tony Blair of trying to unseat him. Zuwanie thinks the French are doing the same.
* Matobos flag bears a strong resemblance to flag of Zimbabwe|Zimbabwes flag.
* Zuwanie is the dictator of the fictional country of Matobo, an apparent reference to Zimbabwes Matobo National Park. Movement for Democratic Change.

Zimbabwes government has itself spotted the parallels between Mugabe and Zuwanie. 
* In September 2005, the Herald,  a state owned newspaper in Harare, Zimbabwe, attacked the film, calling it an anti-Zimbabwean work supported by the CIA. The film has been approved for release and distribution inside the country by the countrys official censorship board. Acting Minister of Information and Publicity Chen Chimutengwende said, "The CIA-backed film showed that Zimbabwes enemies did not rest. They have resources and are determined to wage their war on the economic, social and cultural fronts. The names of the main character in the film are Shona," Mr Chimutengwende said, referring to Zimbabwes main ethnic group to which President Mugabe belongs. "The film talks about an African president going to the United Nations and our president is going to the UN next week so the connection is so obvious," he said. "But we will defeat them and we will defeat neo-colonialism. We have defeated a powerful enemy before which was colonialism," he said. Tafataona Mahoso, chairman of the Zimbabwe government Media and Information Commission, said it was "cheap American and Rhodesian propaganda . . . typical of the tactics used during the Cold War."
* Zimdaily.com reported on 23 September 2005 that the Presidents Office had issued an interdict banning screening of The Interpreter. The interdict, seen by Zimdaily, stated that the film is "mischievous" and a "subtle denigration of our head of State by the Bush administration and the CIA." It states that screening the film risks contravening Section 13(1)(A) as read with subsection (6) of the Censorship and Entertainment Control Act, and that it is in contravention of the Public Order and Security Act, which outlaws communicating statements deemed to undermine the head of State.
* Zimbabwes government also linked the film to efforts by Australia and New Zealand to have Mugabe indicted by the U.N. Security Council for trial before the International Criminal Court on charges of crimes against humanity.

==Reception==
The Interpreter earned mixed reviews from critics, as it now holds a 57% rating on Rotten Tomatoes based on 191 reviews.

===Box office===
The picture was #1 In its opening weekend. According to Box Office Mojo, The Interpreter had a domestic gross of $72,708,161 and an international tally of $90,236,762, bringing the pictures worldwide gross to $162,944,923 versus an $80 million budget, so the film was considered a box office success.

==Awards==
In 2005, the Los Angeles Film Critics Association awarded Catherine Keener as Best Supporting Actress, for her parts in several films including The Interpreter.

==See also==
* Ku (language)
* United Nations
* United Nations General Assembly United Nations Interpreters
* Matobo National Park

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
* 
*  , retrieved on May 31, 2007
*  , retrieved on May 31, 2007
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 