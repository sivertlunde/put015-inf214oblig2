Sila Nerangalil Sila Manithargal
{{Infobox film
| name           = Sila Nerangalil Sila Manithargal
| image          =
| caption        = Movie Poster
| director       = A. Bhimsingh
| producer       = 
| based on       =  
| story          = 
| screenplay     = A. Bhimsingh Lakshmi
| music          = M. S. Viswanathan
| cinematography = D. S. Pandian
| editing        = 
| studio         = A. B. S. Productions
| distributor    = 
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}} Best Actress National Award winning performance, won by Lakshmi for her portrayal of the lead character Ganga.  

==Plot==
Ganga, a college student from an orthodox Tamil Brahmin family, has a sexual encounter with a stranger who offers her lift on a rainy day. Ganga is uncertain about her participation in the event. She, overcome by guilt and self-loathing, construes the event as rape. Her disillusioned face forces out a confession to her mother about what happened earlier. Overhearing this, Gangas brother, whos the bread winner of the family, disowns and evicts her from the house (in Sri Rangam). She then moves in with her uncle—mothers elder brother in Madras—who assures full support in continuing her education. After successful graduation she gets employed in a private firm and grows to take the top managerial position.  Ganga until then lives a single life resisting the pressure to lie about the incident (which, presumably, would ruin her life with another man). Her assumed status as a spoilt woman also implicitly encourages her lecherous uncle to make sexual advances.

It is during this time she chances up on the stranger, Prabhu. She musters up the courage to introduce herself as who she really is and get Prabhu to discuss about that fateful evening. She then realizes that she probably showed as much interest in the sex as did Prabhu. The revelation brings Ganga closer to Prabhu as friends. They find their characteristics agreeable and the friendship matures into love. But unable to transcend the societys norms, Prabhu advices her to get married to someone else. When all attempts to convince Prabhu fails, Ganga is forced to part ways with him. The film ends with a note appreciation for her purity and self-induced monogamous relationship with Prabhu.

==Cast== Lakshmi as Ganga
* Sreekanth as Prabhu Nagesh as R. K. Viswanatha Sarma
* Jayageetha as Manju
* Nilakanthan as Ganesan - Gangas brother
* Y. G. Parthasarathy as Venkatarama Iyer
* Bhuvanadevi as Mrs. Immanuel
* Rajasulochana as Padma
* Sukumari as Ganesans wife
* Sundari Bai as Gangas Mother

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 