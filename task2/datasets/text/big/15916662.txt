Teri Yaad (film)
 
{{Infobox Film
| name           = Teri Yaad  
| image          = Teri Yaad film.jpg
| image_size     = 250px
| caption        = 
| director       = Daud Chand
| producer       = Sardari Lal   D.P. Singha (co-producer)
| writer         = Sardari Lal (story)  Khadim Mohiudin (screenplay)
| narrator       = 
| starring       = Jahangir Sardar Muhammad Ghulam Muhammad Najma Nazar Asha Posley Nasir Khan Ghulam Qadir Ragni Shakir Shola
| music          = 
| cinematography = Raza Mir
| editing        = Muhammad Latif Durrani
| distributor    = 
| released       = August 7, 1948
| runtime        = 
| country        =  
| language       = Urdu
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 independence on 7 August 1948,    on Eid ul-Fitr|Eid. It was the first film released after the birth of Pakistan but not the first to be produced indigenously in the new land. 

==History and setting== independence of Indian film industry was segregated and the only film production centre left in Pakistan was at Lahore. With the industry reeling in its infancy, it was hard to work on film productions that had initiated before the  ] as many of the working filmmakers and actors had left for the new country of India.

With many hardships, the new film industry was able to release its first feature film, Teri Yaad on  , premièring at the Parbhat Theatre in Lahore.  It starred   Dilip Kumar who had traveled off to India. The playback soundtrack was written and composed by Inayat Ali Nath. Produced by Dewan Sardari Lals Dewan Pictures and directed by Daud Chand, the film stayed for a significant time on the celluloid screens in Lahore, Quetta and Dhaka.

==Notes==
 

==External links==

*   on  
*   on Internet Movie Database
*   on Pakistan Film Magazine

 
 
 
 
 

 