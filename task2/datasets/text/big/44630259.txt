Frog (film)
Frog was a television film made in 1987 starring Shelley Duvall, Scott Grimes, and Elliott Gould. 

The central character Arlo Anderson (played by Scott Grimes) is an unpopular youth with an obsession with lizards and amphibians. His life changes for the better when he purchases a frog named Gus. Gus, an Italian prince turned frog several hundred years, earlier retains the ability to speak. He and Arlo seek out a kiss to turn Gus back into a man.

A sequel to the movie titled Frogs! followed in 1991.

== Cast ==
* Scott Grimes: Arlo Anderson 
* Shelley Duvall: Annie Anderson
* Elliott Gould: Bill Anderson Paul Williams: Gus
* Amy Lynne: Suzy

==References==
 

==External links==
*  at the Internet Movie Database

 
 
 

 