Uthamaputhiran (2010 film)
 Uthama Puthiran}}
 
 

 
{{Infobox film|
| name           = Uthama Puthiran
| image          = Uthamaputhiran.JPG
| caption        = Official Poster
| director       = Mithran Jawahar
| producer       =  M. Mohan Apparao T. Ramesh
| writer         = Gopimohan Mithran Jawahar
| based on       =   Vivek K. Bhagyaraj Ashish Vidyarthi Jaya Prakash Reddy
| music          = Vijay Antony
| cinematography = Balasubramaniyem
| editing        = M. Thiyagarajan
| studio         = Balaji Studios
| distributor    = Ayngaran International Balaji Studios
| released       =  
| runtime        = 169 minutes
| language       = Tamil
| country        = India
| gross          = 
}}
 Telugu film Ready (2008 film)|Ready, it stars Dhanush and Genelia DSouza, reprising her role from the original version, in the lead roles;  the supporting cast includes K. Bhagyaraj, Vivek (actor)|Vivek, Ashish Vidyarthi and Jaya Prakash Reddy among others. The film received positive reviews from critics and ended up as a commercial success among 2010 Deepavali releases along with Mynaa.

==Plot==
Siva (Dhanush) is a happy-go-lucky young boy in a large family. The head of this family are 3 older brothers. One of the brothers, is the father of Siva. Siva studies from a hostel away from home. He is always helpful and always comes in when others need him. In one of his obliging acts he helps his cousin Kalpana (Shriya Saran) to elope with her lover against the familys choice. This aggravates the family and they are asked to forget all about him.

On another occasion, he is asked to help a friend in a love marriage. Siva kidnaps Pooja (Genelia DSouza) on a mistaken identity from the marriage hall. When Pooja is kidnapped, her uncles and their henchmen follow them. Pooja while on the run tells Siva that she wasn’t interested in the wedding and her uncles are forcing her marriage with one of their sons only to seize her properties. When Siva went to kidnap her, she was thinking of how to run away from this marriage. While escaping from Poojas uncles, Siva brings Pooja to his house under a false identity. Soon, Siva falls in love with Pooja and determines to marry her only with the consent of all the members of both families. When Sivas family along with Pooja visit a temple, her uncle kidnaps her and locks her away in his house.
To save Pooja, Siva joins Emotional Ekambaram (Vivek (actor)|Vivek) — the auditor of Poojas uncles – as an assistant. He makes Ekambaram believe that he is capable of creating new worlds with characters of their own. Ekambaram then "creates" two American billionaires and with the help of Siva, convinces the two uncles to marry their sons to the daughters of these billionaires. To prove that they are real and not merely the figment of Ekambarams imagination, Siva asks his parents and his uncles and aunts to play the role. They manage to win the Gounders hearts and bring about a change in their behaviour.

Then, with the consent of all the family members Siva marries Pooja.

==Cast==
* Dhanush as Shivaramakrishnan/(Dandapani)
* Genelia DSouza as Pooja Padmanadhan Vivek as Emotional Ekambaram
* K. Bhagyaraj as Raghupathi/(Washington Vetrivel
* Ashish Vidyarthi as Periyamuthu Gounder
* Jaya Prakash Reddy as Chinnamuthu Gounder
* R. Sundarrajan as Sundaram
* Karunas as Janaki
* Vijay Babu as Raghavan/(Shochakko Shakthivel)
* Charuhasan as Swamiji
* Mayilsamy as Santhosh Khan Srinath as David
* Rajendran as Velu Ambika as Sivakaami Rekha as Meenakshi
* Rajalakshmi as Lakshmi Raghupathi
* Nithya as Bharatham Sundaram
* Uma Padmanabhan as Sundari Raghavan
* Surekha Vani Aarthi as Raji/(Gundumani)
* Sonia (actress)| Sonia as Lalitha alias Lallu
* Shriya Saran as Kalpana (Guest appearance)

== Soundtrack ==
{{Infobox album
| Name       = Uthamaputhiran
| Type       = soundtrack
| Artist     = Vijay Antony
| Cover      = 
| Released   = 5 October 2010
| Recorded   = 
| Genres     = 
| Label      = Think Music
| Producer   = Vijay Antony
| Last album = Aval Peyar Thamizharasi (2010) 
| This album = Uthamaputhiran (2010) 
| Next album = Velayudham (2011) 
}}

The soundtrack, composed by Vijay Antony, was released on 5 October 2010 in Chennai. The album consists of 6 tracks overall.

{{tracklist
| headline     = Tracklist 
| extra_column = Singer(s)
| total_length = 35:02
| title1       = Ussumu Laresay 
| extra1       = Vijay Antony, Emcee Jazz, Janani 
| length1      = 4:46
| title2       = Kan Irrandil 
| extra2       = Naresh Iyer
| length2      = 4:29
| title3       = Idicha Pacharisi 
| extra3       = Ranjith, Vinaya, Sangeetha 
| length3      = 4:43
| title4       = En Nenju
| extra4       = Vijay Prakash, Saindhavi
| length4      = 4:47
| title5       = Thooral Thedum 
| extra5       = Ajeesh, Janani
| length5      = 4:19
| title6       = Ulagam Unnaku 
| extra6       = Vijay Prakash
| length6      = 3:36
}}

==Release==
The film netted approximately  58 lakh in three days from Chennai city and  67 lakh from Salem area on its opening weekend. It was hat-trick victory for Dhanush-Mithran combination.  The film was however banned in western districts of Tamil Nadu. 

==References==
 

== External links ==
*  

 
 
 
 
 