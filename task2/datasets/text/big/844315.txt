Belle de Jour (film)
 
{{Infobox film
| name         = Belle de Jour
| image        = Belle de jour poster.jpg
| caption      = Theatrical release poster
| border       = yes
| director     = Luis Buñuel
| producer     = Henri Baum Robert and Raymond Hakim
| writer       = Luis Buñuel Jean-Claude Carrière
| based on     =  
| starring     = Catherine Deneuve Jean Sorel Michel Piccoli Francisco Rabal
| music        =
| cinematography = Sacha Vierny
| editing      = Louisette Hautecoeur Allied Artists  (USA) 
| released     =  
| runtime      = 101 minutes Italy
| language     = French Spanish
| budget       =
| gross        = $20,246,403  
}}
 Belle de jour by Joseph Kessel, the film is about a young woman who is compelled to spend her midweek afternoons as a prostitute while her husband is at work.   

The title of the film is a pun in French. The phrase "belle de nuit" is best translated by the English phrase "lady of the night", i.e. a prostitute. Séverine works as a prostitute during the day, so she is "belle de jour". It may also be a reference to the French name of the day lily (Hemerocallis), meaning "beauty of   day", a flower that blooms only during the day.
 Yves St. Laurent.

==Plot==
Séverine Serizy (Catherine Deneuve), a young and beautiful housewife, is unable to share physical intimacy with her husband, Dr. Pierre Serizy (Jean Sorel), despite their love for each other. Her sexual life is restricted to elaborate fantasies involving Dominance and submission|domination, sadomasochism, and Bondage (sexual)|bondage. Although frustrated by his wifes frigidity toward him, he respects her wishes.

While visiting a ski resort, they meet two friends, Henri Husson (Michel Piccoli) and Renée (Macha Méril). Séverine does not like Hussons manner and the way he looks at her. Back in Paris, Séverine meets up with Renée and learns that a mutual friend, Henriette, now works at a brothel. At her home, Séverine receives roses from Husson and is unsettled by the gesture. At the tennis courts, she meets Husson and they discuss Henriette and houses of pleasure. Husson mentions a high-class brothel to Séverine at 11 Cité Jean de Saumur. He also confesses his desire for her, but Séverine rejects his advances.

Haunted by a childhood memories, including one involving a man who appears to touch her inappropriately, Séverine goes to the high-class brothel, which is run by Madame Anaïs (Geneviève Page). That afternoon Séverine services her first client. Reluctant at first, she responds to the "firm hand" of Madame Anaïs, who names her "Belle de Jour," and has sex with the stranger. After staying away for a week, Séverine returns to the brothel and begins working from two to five oclock each day, returning to her unsuspecting husband in the evenings. One day Husson comes to visit her at home, but Séverine refuses to see him. Still she fantasizes about having sex with him in her husbands presence. Ironically, Séverines physical relationship with her husband is improving and she begins having sex with him.

Séverine becomes involved with a young gangster, Marcel (Pierre Clémenti), who offers her the kind of thrills and excitement of her fantasies. When Marcel becomes increasingly jealous and demanding, Séverine decides to leave the brothel, with Madame Anaïs agreement. Séverine is also concerned about Husson, who has discovered her secret life at the brothel. After one of Marcels gangster associates follows Séverine to her home, Marcel visits her and threatens to reveal her secret to her husband. Séverine pleads with him to leave, which he does, referring to her husband as "the obstacle."

Marcel waits downstairs for Pierre to return home and shoots him three times. He flees, but is shot dead by the police. Séverines husband survives, but is left in a coma. The police are unable to find a motive for the attempted murder. Sometime later Séverine is at home taking care of Pierre, who is now completely paralyzed and in a wheelchair. Husson visits Pierre to tell him the truth about his wifes secret life; she does not try to stop him. Afterwards, in an  ambiguous ending, Séverine sees her husband as healthy again, and they are happy.

==Cast==
 
* Catherine Deneuve as Séverine Serizy, alias Belle de Jour
* Jean Sorel as Pierre Serizy
* Michel Piccoli as Henri Husson
* Geneviève Page as Madame Anaïs
* Pierre Clémenti as Marcel
* Françoise Fabian as Charlotte
* Macha Méril as Renée
* Marguerite Muni as Pallas
* Maria Latour as Mathilde
* Claude Cerval as le chauffeur
* Michel Charrel as Footman
* Iska Khan as Asian client
* Bernard Musson as Majordomo
* Marcel Charvey as Prof. Henri
* François Maistre as The professor
* Francisco Rabal as Hyppolite
* Georges Marchal as Duke
* Francis Blanche as Monsieur Adolphe   
 

==Production==
;Filming locations
* 1 Square Albin-Cachot, Paris 13, Paris, France
* 79 Champs-Élysées, Paris 8, Paris, France
* Chalet de la Grande Cascade, Bois de Boulogne, Paris 16, Paris, France
* Champs Elysées, Paris 8, Paris, France
* Rue de Messine, Paris 8, Paris, France (Serizys home)   

==Awards and nominations==
{| class="wikitable sortable" width="80%"
|-
! width="5%"| Year
! width="50%"| Award
! width="20%"| Recipient
! width="5%"| Result
|-
| 1967
| Venice Film Festival Golden Lion Award
| Luis Buñuel
|  
|-
| 1967
| Venice Film Festival Pasinetti Award for Best Film
| Luis Buñuel
|  
|-
| 1968
| Bodil Award for Best European Film
| Luis Buñuel
|  
|-
| 1968
| French Syndicate of Cinema Critics Award for Best Film
| Luis Buñuel
|  
|-
| 1969
| BAFTA Award Nomination for Best Actress
| Catherine Deneuve
|  
|}

==See also==
* Sadism and masochism in fiction
* Surrealism

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 