Trapped in Silence
 
{{Infobox film
| name = Trapped In Silence
| image =
| caption =
| director = Michael Tuchner
| producer = Dick Atkins
| writer = Torey Hayden Vickie Patik
| starring = Kiefer Sutherland Marsha Mason
| editing = Eric Albertson
| released =  
| runtime = 94 min
}}
Trapped In Silence is a 1986 film starring Kiefer Sutherland and Marsha Mason. The film is about a psychologist (Marsha Mason) works with a traumatized boy (Kiefer Sutherland) with elective mutism - the patient chooses not to talk because of his trauma. Eventually the boy starts to open up to reveal his sordid story.

 

 
 
 