A Funny Man
 
{{Infobox film
| name           = A Funny Man
| image          = AFunnyMan2011Poster.jpg
| caption        = Film poster
| director       = Martin Zandvliet
| producer       = Michael Ricks
| writer         = Martin Zandvliet Anders Frithiof August
| starring       = Nikolaj Lie Kaas
| music          = 
| cinematography = Jesper Tøffner
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

A Funny Man ( ) is a 2011 Danish drama film directed by Martin Zandvliet about the Danish actor and comedian Dirch Passer.    

==Cast==
* Nikolaj Lie Kaas as Dirch Passer
* Lars Ranthe as Kjeld Petersen
* Lars Brygmann as Stig Lommer
* Malou Reymann as Bente Askjær (as Malou Leth Reymann)
* Morten Kirkskov as Ove Sprogøe
* Frederikke Cecilie Berthelsen as Inge (as Frederikke Cecilie Bertelsen)
* Silja Eriksen Jensen as Judy Gringer
* Laura Christensen as Hanne Passer
* Sarah Grünewald as Vicky
* Martin Buch as Preben Kaas
* Laura Bro as Sigrid "Sitter" Horne-Rasmussen

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 