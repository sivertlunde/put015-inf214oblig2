A.W.O.L. (2006 film)
{{Infobox film 
| name           = A.W.O.L.
| image          =
| caption        =  
| director       = Jack Swanstrom 
| producer       = Shane Black (as Harry Lime)   Sukee Chew   Jack Swanstrom    Jessica Wethington
| writer         = Shane Black (as Holly Martins) David Morse   John C. McGinley   Caroline Kristiahn
| music          = Daniel Lessner William Richter
| cinematography = Lynda Cohen
| editing        = Susan K. Hoover
| distributor    = 
| released       = 2006
| country        = United States
| language       = English 
| budget         =  
|}}
 David Morse, John C. McGinley, and Caroline Kristiahn. 

==Synopsis==



From the original promotional material:

“Major Cliff Marquette (David Morse) and his Special Forces Detachment are called on by the CIA to investigate alleged "black magic" in the jungles of Cambodia. After a sudden and devastating firefight, Cliff is caught and subjected to brutal torture at the hands of the enemy.  Pain beyond his ability to endure; then, at the height of his agony, Cliff is mysteriously transported to another world, an alternate reality of fondest desires.  Forged from his own mind, perhaps... or maybe a result of the very magic he had been sent to observe.  Either way, Cliffs terror has not ended, it is only begun, for in his seeming salvation lies a horrifying catch.  A fine-print provision with which fate can still claim him... unless, with the clock counting down, Cliff Marquette can beat the odds one more time.  Manage to cheat fate.  It will take his wits.  And it will require a gun...”


==External links==
* 

 

 
 
 
 

 