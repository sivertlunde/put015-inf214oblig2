Black Noon
{{Infobox film
| name           = Black Noon
| image          = Black Noon.jpg
| image_size     = 185px
| alt            = 
| caption        = 
| director       = Bernard L. Kowalski
| producer       = Andrew J. Fenady
| writer         = Andrew J. Fenady
| narrator       = 
| starring       = Roy Thinnes Yvette Mimieux Ray Milland Gloria Grahame
| music          = George Duning
| cinematography = Keith C. Smith
| editing        = Dann Cahn
| studio         = Andrew J. Fenady Productions  
| distributor    = Columbia Broadcasting System (1971) 
Columbia TriStar Television Sony Pictures Television (2002-)
| released       = November 5, 1971
| runtime        = 74 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 American made-for-TV horror western western feature film that debuted in 1971. It was written and produced by Andrew J. Fenady and directed by Bernard L. Kowalski. 
 {{cite web
 |url=http://www.citwf.com/film38860.htm
 |title=Black Noon (1971)
 |publisher=Complete Index to World Film
 |accessdate=2009-07-23}}  
 {{cite web
 |url=http://ftvdb.bfi.org.uk/sift/title/150510
 |title=Black Noon (1971)
 |publisher=British Film Institute
 |accessdate=2009-07-23}}  
 {{cite book
 |last=Perry
 |first=Jeb H.
 |title=Screen Gems: a history of Columbia Pictures Television from Cohn to Coke, 1948-1983
 |publisher=Scarecrow Press
 |year=1991
 |edition=illustrated
 |isbn=0-8108-2487-6
 |oclc=9780810824874
 |url=http://books.google.com/books?id=Ls1kAAAAMAAJ&q=%22Black+Noon%22,+CBS&dq=%22Black+Noon%22,+CBS&ei=Yx1pSsekH4H4kASb863mAg
 |accessdate=July 23, 2009}}  
 {{cite book
 |last=Terrace
 |first=Vincent
 |title=The complete encyclopedia of television programs, 1947-1979
 |publisher=A. S. Barnes
 |year=1979
 |edition=2
 |volume=2
 |url=http://books.google.com/books?id=NVOI_gdWzdwC&q=%22Black+Noon%22,+CBS+movie&dq=%22Black+Noon%22,+CBS+movie&ei=Ry9ySrX6N5POlQSR4uSqAQ}} 

The film originally aired on November 5, 1971 as part of CBSs The CBS Friday Night Movies, 
 {{cite news
 |url=http://news.google.com/newspapers?id=aL4SAAAAIBAJ&sjid=XPoDAAAAIBAJ&pg=7478,1143494&dq=black-noon+film
 |title=Tonights Best on TV
 |date=November 5, 1971
 |publisher=The Ledger
 |accessdate=2009-07-24}}  and was shown repeatedly in 1982. 
 {{cite news
 |url=http://news.google.com/newspapers?id=RicVAAAAIBAJ&sjid=afoDAAAAIBAJ&pg=7493,5498887&dq=black-noon+film
 |title=Tonights Best on TV
 |date=May 9, 1972
 |publisher=The Ledger
 |accessdate=2009-07-23}}  
 {{cite news
 |url=http://books.google.com/books?id=AOYCAAAAMBAJ&pg=PA111&dq=%22Black+noon%22%2B%22movie%22&lr=&as_brr=3&ei=VCxpSuiCG5HWlASZs5H3Ag&client=firefox-a
 |title=Weekend, May 9-10
 |date=May 11, 1981
 |publisher=New York Magazine
 |accessdate=2009-07-24}}  
 {{cite news
 |url=http://books.google.com/books?id=wOcCAAAAMBAJ&pg=PA108&dq=%22Black+noon%22%2B%22movie%22&lr=&as_brr=3&ei=VCxpSuiCG5HWlASZs5H3Ag&client=firefox-a
 |title=Evening, June 16-18 and 21-22
 |date=June 21, 1982
 |publisher=New York Magazine
 |accessdate=2009-07-24}} 

Jerry Beigel wrote in the Los Angeles Times about the premiere stating that the films release would have been more fitting a week earlier, before Halloween. 

==Plot==
When Reverend John Keyes (Roy Thinnes) and his wife Lorna (Lynn Loring) arrive in a western town, they find that there is mysterious force causing bad luck to plague the settlers.  Once the Reverend is able to get the recalcitrant residents to speak about the ongoing troubles, he finds his spiritual leadership is being challenged by a cult of devil worshippers who practice voodoo, and have to get to the heart of a strange relationship between a mute young girl and a gunslinger who seem possessed by Satanic spirits.

It was noted in The Monster Book, that in Black Noon, Roy Thinnes character battled devil worshippers, but that in a later film, Satans School for Girls (1973 film)|Satans School for Girls, he led his own cult. 
 {{cite book
 |authors=Christopher Golden, Stephen Bissette, Thomas E. Sniegoski
 |title=The Monster Book
 |publisher=Simon and Schuster|year=2000
 |edition=illustrated
 |isbn=0-671-04259-9
 |oclc=9780671042592
 |url=http://books.google.com/books?id=ah7QSsMKDZsC&pg=PT219&dq=%22Black+noon%22%2B%22movie%22&lr=&as_brr=3&ei=VCxpSuiCG5HWlASZs5H3Ag&client=firefox-a
 |accessdate=July 25, 2009}} 

==Partial cast==
*Roy Thinnes as Reverend John Keyes 
*Yvette Mimieux as Deliverance 
*Ray Milland as Caleb Hobbs 
*Gloria Grahame as Bethia 
*Lynn Loring as Lorna Keyes 
*Henry Silva as Moon 
*Hank Worden as Joseph 
*Stan Barrett as Man in Mirror 
*Joshua Bryant as Towhead 
*Leif Garrett as Towhead

==References==
 

==External links==
*  at the Internet Movie Database
*  at All Movie Guide
*  at New York Times

 

 
 
 
 
 
 
 
 
 