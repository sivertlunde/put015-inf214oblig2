Thalattu
{{Infobox film			
| name = Thalattu
| image =
| caption =
| director = T. K. Rajendran
| producer = P. Mohanraj
| story =
| writer = T. K. Rajendran
| narrator = Sukanya Sivaranjani Sivaranjini Goundamani
| music = Ilaiyaraaja
| cinematography = B. R. Vijayalakshmi
| editing = M. N. Raja
| studio = Kiran Films
| distributor = Kiran Films
| released =  	
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}}			 Sukanya and Sivaranjini in Vijayakumar and Kovai Sarala play other supporting roles. 

==Cast==
*Arvind Swamy as Kuzhanthai Sukanya
*Sivaranjani Sivaranjini
*Goundamani Vijayakumar
*Kovai Sarala Mansoor Ali Khan
*Poornam Vishwanathan
*Vinu Chakravarthy
*Kanthimathi

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Pulamaipithan || 05:42
|-
| 2 || Kulandhai Paadura || Malaysia Vasudevan || 05:05
|-
| 3 || Methuva Thanthi || Mano (singer) |Mano, Sunandha || 05:01
|-
| 4 || Tajumahal Thevai || Mano (singer) |Mano, Minmini || 04:09
|-
| 5 || Vannapura || S. N. Surendar || 04:52
|}

==References==
 

 
 
 
 
 


 