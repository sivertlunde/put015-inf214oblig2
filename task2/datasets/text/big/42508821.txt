John Needham's Double
{{infobox book | 
| name          = John Needhams Double: A Story Founded Upon Fact
| title_orig    = 
| translator    = 
| image         = 
| caption = 
| author        = Joseph Hatton
| illustrator   = 
| cover_artist  = 
| country       = 
| language      = English
| series        = 
| genre         = Novel
| publisher     = John & Robert Maxwell (U.K.) / Harper (U.S.)
| pub_date      = 1885
| english_pub_date =
| media_type    = 
| pages         = 208 pp
| isbn          = 
| preceded_by   = 
| followed_by   = 
}} 

John Needhams Double is an 1885 novel and 1891 play by Joseph Hatton, and 1916 silent film.

==Novel==

The novel is subtitled "A Story Founded on Fact" and is based on the story of Irish financier and politician   (October 10, 1885), p. 491 

  (August 22, 1885), p. 85 

==Play==
  as Kate Norbury in the 1891 stage adaptation of John Needhams Double]]
  (April 29, 1922), p. 16     and played for just over a month. (4 March 1891). [http://query.nytimes.com/mem/archive-free/pdf?res=F50E13F9345F10738DDDAD0894DB405B8185F0D3 Theatrical Gossip, The New York Times 

The cast included Willard playing the dual roles of John Needham and Joseph Norbury, Marie Burroughs as Kate Norbury, Burr McIntosh as Col. Calhoun Booker, and Royce Carleton as Mr. Grant. Dale, Alan.  , p. 23 (February 13, 1891), Vol. IX, No. 210 

===Cast===
*Joseph Norbury/John Needham ... Edward Smith Willard
*Richard Woodville ... E.W. Gardiner
*Mr. Horace West ... Charles Harbury
*Mr. Grant ... Royce Carleton
*Mr. Nolan ... Sant Matthews
*Col. Calhoun Booker ... Burr McIntosh
*Percy Tallant ... Bessie Hatton (daughter of Joseph Hatton)
*Thomas ... Harry Cane
*Sanders ... Lysander Thompson
*Kate Norbury ... Marie Burroughs
*Miss Dorothy Norbury ... Cecile Rush
*Mrs. Needham ... Katherine Rogers
*Miss Virginia Fleetwood ... Maxine Elliott
*Hannah ... Cora Edsall (14 February 1891) , New York Dramatic Mirror 

==Silent film==

A silent film version directed by      

===Cast=== Tyrone Power
*Ellen Norbury ... Marie Walcamp Frank Elliott
*Aunt Kate ... Agnes Emerson
*Dobbins ... Walter Belasco
*Cruet ... Frank Lanning
*Thomas Creighton ... Buster Emmons 

==References==
 

==External links==
*  

 
 
 