We Want Our Mummy
{{Infobox Film |
  | name           = We Want Our Mummy|
  | image          = WeWantOur Mummy39.jpg
  | director       = Del Lord |
  | writer         = Searle Kramer Elwood Ullman| Ted Lorch John Tyrrell
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 27"  |
  | country        = United States
  | language       = English
}}

We Want Our Mummy is the 37th short subject starring American slapstick team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
Two museum curators (Bud Jamison, James C. Morton) hire the Stooges (as private detectives) to locate Professor Tuttle of Egyptology, who went missing while attempting to find the mummy of Egyptian King RootinTootin in Cairo. The Stooges check the basement and help a man take a box onto a truck, not aware that Tuttle is bound and gagged inside. They are then told by the curators to find the tomb and bring back the mummy, for which they will be paid $5000. They hail a taxicab in New York City, and inform the bewildered driver they are bound for Egypt.

Once in Egypt the boys, under the duress of a mirage, believe an empty patch of sand is a lake of cool water and dive in, inadvertently diving right into a series of underground tunnels that may lead to the tomb of RootinTootin.  They begin to investigate, but end up separated and Curly runs afoul with a living mummy.  He takes off running, and he and his pals reunite.  

 ) looks on]]
Upon their arrival, the Stooges learn that Tuttle is being held hostage by a group of thieves. While the Stooges wander around in the underground tunnels, the thieves have the professor bound and gagged. Curly finds what the Stooges believe to be the mummy of RootinTootin in a secret room, activated by a trap door. When Curly tries to pick it up he clumsily drops it, crumbling it to dust. 
 Yanks win World Series—can Cubs and..." realizing he has been tricked, he charges Curly, but in the process of chasing the Stooges he and his cronies fall into a well Curly had fallen into earlier and hid using a carpet. The Stooges admit to the professor that Curly had destroyed the mummy, but the Professor says, "That was his wife, Queen Hotsy-Totsy!" He holds up a small mummy case, containing the real mummy of RootinTootin, who was a midget. As the men discuss about all the trouble for a midget, an alligator comes out of the hole in the wall. Curly turns and sees the gator and thinks it is a mummy alligator and tries to take it home to hang it on his wall. When Curly reaches for a piece of rope, the gator bites his buttocks, Curly cries in pain and tells the fellas the gator bit him. Nobody believes him until they see the gator snap its jaws at them.  The men scream and run for their lives to the taxi.

==Production notes==
We Want Our Mummy is the first Stooge film to employ "Three Blind Mice" as the Stooges official theme song (the song also appeared somewhat prematurely in 1938s Flat Foot Stooges, due to some confusion in that films release date). This version of "Three Blind Mice," often known as the "sliding strings" version, would be used regularly up to and including 1942s Whats the Matador?. An alternate version of the sliding strings version would be used for a brief period starting with 1945s If a Body Meets a Body.   

The reference to the 1938 World Series between the Yankees and Cubs is a rare acknowledgement of a real-life sporting event.

==In popular culture== TBS Halloween special The Three Stooges Fright Night both in 1992 and 1995.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 