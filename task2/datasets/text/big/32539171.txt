Keyhole (film)
{{Infobox film
| name           = Keyhole
| image          = Poster for Keyhole.jpg
| caption        = 
| director       = Guy Maddin
| producer       = Jean du Toit Lindsay Hamel Phyllis Laing Guy Maddin Jody Shapiro
| writer         = Guy Maddin George Toles
| starring       = Jason Patric Isabella Rossellini Udo Kier Kevin McDonald Brooke Palsson
| music          = Jason Staczek
| cinematography = Benjamin Kasulke
| editing        = John Gurdebeke
| studio         = Entertainment One Buffalo Gal Pictures
| distributor    = monterey media inc. (US)
| released       =  
| runtime        = 93 min
| country        = Canada
| language       = English
| budget         = 
}}
Keyhole is a 2012 Canadian film directed by Guy Maddin, starring Jason Patric, Isabella Rossellini, Udo Kier and Kevin McDonald.  A surreal combination of gangster film and haunted house film, which draws on Homers Odyssey as well,  Keyhole tells the story of a Ulysses Pick (Patric), who returns to his home and embarks on an odyssey through the house, one room at a time.  Filming began in Winnipeg on July 6, 2010.  Maddin shot Keyhole digitally rather than his usual method of shooting on 16mm or Super-8mm. 

==Plot==
 
The gang of Ulysses Pick ( ), reveals that the house is haunted (and not only by him) because although a houses happiness is able to vacate the premises after its inhabitants have left, its sorrow is doomed to remain inside forever. This house had once belonged to Ulysses and his four children with his wife Hyacinth (Isabella Rossellini).

Ulysses himself arrives, carrying a drowned young woman named Denny. After entering the house, Denny is able to stand under her own strength, and seems alive although injured and blind. She reveals that she can hear the thoughts of Ulysses as he wanders through the house, examining the objects that he had previously left behind. Calypso reveals that Hyacinth remains in Ulyssess former bedroom at the top of the house, where she has chained him (her fathers ghost). Ulysses and Denny join the gang, who are confused about their plan (and dont understand why the police havent shot their way into the house). Ulysses deflects questions about the plan and claims that the police wont enter the house while the storm is still in force. Big Ed, annoyed at being kept in the dark, criticizes Ulysses for adopting Heatly after he killed one of Ulyssess sons, and is thrashed.

Calypso reveals that although he is chained to Hyacinths bed, his chains stretch to unknown lengths, and torments Ed further through supernatural flogging. Ulysses collects the gangs guns and throws them into the ducts to be destroyed in the houses furnace. The gangster Ogilbe (Kevin McDonald) thinks he hears a noise and goes to investigate, exiting with a Scream (film series)|Scream-esque "Ill be right back." He is immediately killed when he tries to have sex with one of the ghosts even though Ulysses just warned him not to bother the ghosts (upon contact with a washerwoman ghost, hes electrocuted—his ghost shortly completes the sex act). The bullets from the guns that had been thrown into the furnace begin to fire throughout the house (the laughing face of Calypso suggests that this is somehow the ghosts doing) and a stray bullet kills Heatly. As he dies, Ulysses reveals that he had at first adopted Heatly to torture him and himself as revenge for the death of his son but grew to love Heatly more than any of his sons. (Ulysses is unaware, as he says this, that the hostage who watches/hears is his only living son, Manners, having forgotten and failed to recognize him.)

Ulysses disposes of the corpses of Heatly and Ogilbe in the bog located in the houses centre (Hyacinths secluded outdoor garden located in a quadrangle within the labyrinthine house). Ulysses gathers his scout knife, the stuffed wolverine Crispy, Denny and his hostage son and sets forth into the house to find Hyacinth, ordering his gang to stay put but leaving the untrustworthy Big Ed in charge. Ulysses orders Denny to stay focused on reading his memories and forget about drowning, since he has forgotten too much to find Hyacinth without her help. She recounts his memory of coming home early to find Hyacinth at home alone, running naked with the familys dogs. Ulysses remembers being distressed at how the episode revealed the extent to which Hyacinth was a stranger to him, even though at the same time this moment was when he loved her the most. Ulysses contemplates the first of the houses many locked doors (which "all have to be opened").

In her bedroom at the top of the house, Hyacinth is startled by Calypso screaming awake from a nightmare of a young girl drowning. Calypso begs Hyacinth for release from his chains, revealing that Hyacinth is now sleeping with Chang, and Hyacinth fondly remembers her son Manners. Calypso tells Hyacinth that Ulysses is in the house coming to either save or kill Hyacinth (or both). Ulysses himself then speaks to Hyacinth through the keyhole of the first locked door, offering her his scout knife and convincing her to pretend he isnt there, allowing him to open the door. Ulysses investigates the room (the blind, drowned Denny helps him "see" the room as it once was) and begins remembering past events. He discovers the ghost of his son Brucie masturbating/playing Yahtzee in a cubbyhole (but doesnt recognize him or his milk-drinking ghost son Ned). Calypsos narration reveals that the hostage is Ulyssess only surviving son, Manners (also unrecognized). Meanwhile, the gangsters have begun to construct an electric chair. Ulysses makes another offering to Hyacinth (of Neds bowl) to have her allow him through a second locked door.

A doctor (Udo Kier) has been summoned to examine Denny. The doctor relates the tragic story of his only sons death, which occurred earlier that day from a fall that broke his neck and was followed by a wasp attack. In the next room (a bathroom), Denny bathes Manners while Ulysses remembers trying to console Hyacinth in her grief. The girl climbs into the bath with Manners, and Ulysses remembers accidentally breaking Neds bowl after his death, upsetting Hyacinth. Denny discovers a secret passage that passes the "cyclops" (a penis sticking out the wall: "That penis is getting dusty," remarks Ulysses). Calypso begs Hyacinth to release him so that he can stop Ulysses, who he has discovered is "bent on forgiveness," a much worse scenario than the revenge he had feared.

As ghosts and gangsters mill about, attempting to restore the house to its former glory, the doctor examines Denny and declares the drowned young woman to be drowned, and therefore dead. Manners frees himself from his gag and reveals his identity to his father, and that he was in love with Denny but she broke up with him and then drowned afterward. Manners tries to revive Denny but fails, and Ulysses leaves the doomed lovers to return to his gang, who strap him into the homemade electric chair (which is powered by pedalling). However, electrocuting Ulysses fails since he is revealed to be already dead (having been, ironically, executed in an electric chair).

Having now fully recovered his memory, Ulysses has Big Ed electrocuted for insubordination and thrown with the chair into the bog. Ulysses takes Manners and the somewhat recovered Denny into the houses next room, where Manners relives a fond memory of Hyacinth. Calypso begs for release from Hyacinth before Chang returns, and she begins to file through his chains, while Manners leads Denny away to show her the desktop family organizer he invented to send messages between family members via pneumatic tubes. Ulysses interrupts Heatly about to have sex with his daughter Lota (a memory, he soon realizes). Lota, dying of cancer, throws herself into the bog to be reunited with Heatly in death. Ulysses tries to bond with Manners by playing catch with him, but Manners ignores his father. Calypso has been freed by Hyacinth, and Manners answers a ringing phone as Ulysses enters the master bedroom to find Hyacinth.

Ulysses kills Chang and Manners hangs up the phone to remember Denny heading out for a midnight swim. Manners heads upstairs to his bedroom (which the gangsters have restored) and remembers Ulysses praising one of his inventions while Hyacinth notes that she has rearranged the house while Ulysses was away. As Hyacinth smiles and looks on, Manners and Ulysses restore the room to the way it used to be. The gangsters and the rest of the ghosts fade away (even the bullet holes fade) and Manners is left alone with his memories, holding a gun, in the empty house, hand turning a doorknob but not fully, unable or unwilling to leave.

==Cast==
* Jason Patric as Ulysses Pick
* Isabella Rossellini as Hyacinth
* Udo Kier as Dr. Lemke
* Louis Negin as Calypso / Camille
* Brooke Palsson as Denny
* Kevin McDonald as Ogilbe
* David Wontner as Manners
* Daniel Enright as Big Ed
* Theodoros Zegeye-Gebrehiwot as Heatly
* Brent Neale as Denton
* Olivia Rameau as Rochelle
* Claude Dorge as Belview
* Jorge Requena as Frosty
* Mike Bell as Milo
* Tattiawna Jones as Lota
* David Evans as Nate
* Darcy Fehr as Ned Pick
* Tyhr Trubiak as Dwayne
* Johnny Chang as Chang
* Reegan McCheyne as Bruce
* Cynthia Wolfe-Nolin as Gun Moll
* Nihad Ademi as Ghost
* Paula Blair as Ghost
* Jeff Funnell as Ghost
* Geneviève LaTouche as Ghost (voice)
* Madelina Chesley as Ghost (voice)
* Garrett Hnatiuk as Gangster

==Release==
In November 2011, Monterey Media brought the United States distribution rights from Entertainment One.     The Film Society of Lincoln Center selected Keyhole as a part of their Film Comment Selects program, which will screen the film at a Special Premier event on March 20, 2012 with director Guy Maddin in attendance.    Keyhole began its theatrical release in New York at the IFC Center on April 6, 2012.    The film then opened at Laemmle Theatres NoHo7 and Playhouse 7 on April 13, 2012.   

===Festivals===
Keyhole was selected to screen at the following film festivals:

*2011 Toronto International Film Festival   
*2011 Whistler Film Festival   
*2012 South by Southwest      
*2012 Independent Film Festival of Boston   
*2012 Wisconsin Film Festival   
*2012 Fantasporto   
*2012 Berlin International Film Festival   

===Home Video===
Keyhole was released to home video on DVD and Blu-ray by Monterey Media in 2012. The home video release also includes Maddins short films Send Me to the Lectric Chair (2009, co-directed by and starring Isabella Rossellini)  and Glorious. 

===Soundtrack===
The soundtrack for Keyhole was composed by Jason Staczek and released by Milan Records. In a note from the soundtracks digital booklet, Maddin states the importance of the films music: "I needed my movie to have the timeless logic of music, but it didn’t on the page and it didn’t on the set.   until   wrote the score for it." 

==Critical reception==
The film received generally positive reviews, with review aggregator Rotten Tomatoes reporting a 70% approval rating based on 37 reviews.  Metacritic, which assigns a weighted average score out of 100 to reviews from film critics, posts a rating score of 63 based on 16 reviews. 

While some critics reacted negatively to the film, including Anna Coatman, who stated that "Keyhole is perhaps a little too inward-looking, a little too claustrophobic,"  others, such as Roger Ebert, were more positive. Ebert gave the film 3/4 stars, writing that "Keyhole plays like a fever dream using the elements of film noir but restlessly rearranging them in an attempt to force sense out of them. You have the elements lined up against the wall, and in some mercurial way, they slip free and attack you from behind."  Steve Dollar of The Wall Street Journal stated that "Keyhole proves that   is as spellbinding in pixels as in celluloid."  A. O. Scott of The New York Times also gave the film a positive review saying "Keyhole may also be a perfect gateway into the bizarre and fertile world of a unique film artist."  Alonso Duralde (in The Wrap) called Keyhole "a deliciously disturbing dreamscape for audiences who want to follow one of today’s most fascinating film artists on another wild ride."  The Village Voice writer Karina Longworth praised the film saying it is "stunning to look at... handmade, logic-defying, and magical."  Jason Buchanan noted that "Few contemporary directors possess such a bold, singularly unique vision as Maddin, and since the mid-80s hes built an impressive body of work distinguished by the juxtaposition of dazzling imagery with darkly comic themes and dialogue." 

==See also==

*List of black-and-white films produced since 1970

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 