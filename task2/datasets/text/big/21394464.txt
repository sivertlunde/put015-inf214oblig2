Munna-A Love Story
{{Infobox Film |
name = Munna-A Love Story|
image = Munnaals.jpeg|
caption = Movie poster for Munna-A Love Story| Romi Naina Das Mihir Das |
director = N. Padhi |
producer = K.M. Rai |
writer = Dr. Raman Ranjan|
music = Abhijit Majumdar |
cinematography = ? |
editing = Susanta Mani | 
distributor = Bishnupriya Arts & Graphics |
released       = 15 August2008  |
runtime        =   ?  |
country = India| Oriya |
}}

Munna-A Love Story ( ),   is a 2008 Indian Oriya film directed by N. Padhi .  
 

== Synopsis ==
"Munna" played by Ollywood superstar Anubhav is a gangster in the film who works under the influence of a Mafia Don played by Mihir Das. Munnas life revolves around his crime world and his mother. A girl named "Nisha" played by Naina Dash works as a Bar Dancer and she loves Munna but has never told anything about her love to Munna. In the past Munna had loved a girl whome for some reasons he could not marry and his love interest played by Romi latter on marries Dushmant who plays the role of a cop.When at one point of time Munna was given a contract of killing the cop played by Dushmant he soon realises that the cop is now the husband of Munnas past Love interest "Pooja".The story forwards with a flashback showing the love story of Munna and what happened due to which he could not marry the girl in the past.; {{cite web|url=http://www.izeans.com/node/131|title=Munna-A Love /Story review
}} 

==Cast==
*Anubhav Mohanty  as Munna
*Dushmant Romi as Pooja
*Naina Das as Nisha
*Mihir Das as Mafia Don
*Anita Das
*Rai Mohan

==Box office reception==
The Movie made on a medium budget got a wonderful opening across all the theaters but due to its weak storyline and average Music,the collections started to drop from its 2nd week. Various other factors like rains, strikes and Orissa Bandh also affected the business of the movie. The film is doing an average business at the Box Office. {{cite web|url=http://www.izeans.com/node/81|title=Box Office Reports / Munna-A Love Story
}} 

== References ==
 

==External links==
*  

 
 
 