Belønningen
{{Infobox film
| name           = Belønningen
| image          = 
| caption        = 
| director       = Bjørn Lien
| producer       = 
| writer         = Bjørn Lien
| starring       = Rolf Søder Joachim Calmeyer
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 84 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
}}

Belønningen ( ) is a 1980 Norwegian drama film written and directed by Bjørn Lien, starring Rolf Søder and Joachim Calmeyer. It was entered into the 12th Moscow International Film Festival where it won the Silver Prize.   

Reidar (Søder) was in the merchant navy during the war, but has since become an alcoholic and a bum. One day outside Vinmonopolet (the government owned alcoholic beverage monopoly), he bumps into successful businessman Sverre Nordvåg (Calmeyer), and drops his bottle of liquor. The two begin to fight, and the police get involved. It turns out the two knew each other before the German occupation of Norway, but fell out when Sverre began to collaborate with the Germans. A young journalist gets involved in the story of the two men during the proceeding trial.

==Cast==
* Rolf Søder as Reidar
* Joachim Calmeyer as Sverre Nordvåg
* Elsa Lystad as Ann
* Tone Danielsen as Unni
* Nils Sletta as Rune
* Svein Sturla Hungnes as Terje
* Lars Andreas Larssen as Terje
* Bente Børsum as Elisabeth Norvåg
* Per Erling Dahl as Fossum
* Morten Søder as Reidar som ung
* Kirsten Hofseth as Ann as young

==References==
 

==External links==
*  
*   at Filmweb.no (Norwegian)

 
 
 
 
 
 
 