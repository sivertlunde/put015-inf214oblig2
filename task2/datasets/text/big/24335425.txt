Woman Times Seven
{{Infobox film
 | name = Woman Times Seven
 | image = Woman Times Seven VideoCover.jpeg
 | caption = 
 | director = Vittorio De Sica
 | producer = Arthur Cohn Joseph E. Levine
 | writer = Cesare Zavattini
 | starring = Shirley MacLaine Peter Sellers Michael Caine Lex Barker Anita Ekberg Adrienne Corri Vittorio Gassman
 | music = Riz Ortolani Christian Matras
 | editing = 
 | distributor = Embassy Pictures
 | released = 1967
 | runtime = 108 minutes
 | country = France Italy United States
 | awards = 
 | language = English French Italian
 | budget = 
}} 1967 Italian/French/American co-production anthology film of seven different episodes, all starring Shirley MacLaine, most of them based on aspects of adultery.

==Episodes==

===Paulette/Funeral Procession===
Leading a walking funeral procession behind the hearse containing the remains of her late husband, a widow is propositioned by her family doctor (Peter Sellers). Vittorio De Sica has a cameo as one of the mourners.

===Maria Teresa/Amateur Night=== strumpets who help her accomplish her goal.

===Linda/Two Against One===
A Scotsman (Clinton Greyn) and an Italian (Vittorio Gassman) are invited to the room of a translator who reads T. S. Eliot in the nude. Linda has a photo of her lover (Marlon Brando) on a table.

===Edith/Super Simone===
Ignored by her bestselling author husband (Lex Barker), who is only interested in his fictional female creation Simone, a neglected wife turns her visions of herself as Simone into reality. Her shocked husband invites a psychiatrist (Robert Morley) to dinner to examine her for mental illness, but the husband, guest, and housekeeper (Jessie Robins) insist that the guest is a lawyer.

===Eve/At the Opera===
A fashion queen is horrified when her archrival Mme Lisari (Adrienne Corri) has been photographed in what her husband (Patrick Wymark) had promised was an exclusive creation for her alone. When asking her archrival not to wear it encourages her to do the opposite, the head of research and development in her husbands fashion house suggests planting a bomb in her archrivals car. Louis Alexandre Raimon has a cameo as himself. 

===Marie/Suicides===
Two lovers, feeling rejected by the world decide on committing suicide in their small room dressed for the wedding they will never have. Fred (Alan Arkin) however is afraid of pills, doesnt want to mess up his tuxedo by jumping out of the window, and doesnt trust Marie to use his fathers pistol on him in case she only wounds him, or kills him and changes her mind.

===Jeanne/Snow===
Two friends meet for lunch on a winter afternoon. They notice a handsome but seedy-looking man (Michael Caine) who appears to be following them. Claudie (Anita Ekberg) suggests the two leave the restaurant and go their separate ways to see which one of them he follows. As Paris is hit by a sudden blizzard, Jeanne realises that the man is following her.

==Production== sketches lying about; they turned these into the movie.  The first choice for the lead role, Natalie Wood, turned the film down. 

The concepts of adultery in the film have a European flavor. In the film, Vittorio Gassman reminds Clinton Greyn that divorce was, at the time of filming, impossible for an Italian.

It was filmed in Paris. The wardrobe was supplied by Pierre Cardin, the jewellery by Van Cleef & Arpels, the furs by Henri Stern and the hairdressing by Louis Alexandre Raimon.

Lord Lucan, later to be suspected of murder, unsuccessfully screentested for a role in the film; after that failure he decided to turn down an audition from Cubby Broccoli for the part of James Bond. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 