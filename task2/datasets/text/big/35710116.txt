Shadows and Illuminations
 

Shadows and Illuminations is a 2010 documentary film that is part of the " " ethnographic film series based on material drawn from 12 years of person-centered research by anthropologist Robert Lemelson. The film series was directed by Robert Lemelson and produced by Robert Lemelson and Alessandra Pasquino.

==Synopsis== Balinese man spirit visitations that villagers interpret within the frame of their culture and religion.

“Shadows and Illuminations” explores how unusual mental events and behavior can be understood or interpreted in multiple ways outside the confines of western psychiatric diagnostics. It looks at Nyoman’s history of trauma and loss during the politically inspired mass killings that swept through Indonesia in 1965-66, his prolonged sickness from pesticide poisoning, and his treatment by traditional healers. The film illustrates how his wife’s love and support have ameliorated his pain and helped him find peace in life with what the west terms “mental illness.”

Total Running Time 34:36 minutes

==Awards==
* 2011 Best Shorts Competition, Award of Merit, Winner
* 2011 Indie Fest, Award of Merit, Winner
* 2011 Cine Golden Eagle Award, Winner
* 2010 International Documentary Association Awards, Best Limited Series, Nominee

==External links==
* 
* 
* 
* 

 
 

 