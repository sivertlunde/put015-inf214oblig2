House of Tolerance
 
{{Infobox film
| name           = House of Tolerance
| image          = Lapollonide-poster.png
| caption        = French poster
| director       = Bertrand Bonello
| producer       = Kristina Larsen Bertrand Bonello
| writer         = Bertrand Bonello
| starring       = Hafsia Herzi Céline Sallette Jasmine Trinca Adèle Haenel Alice Barnole Iliana Zabeth Noémie Lvovsky
| music          = Bertrand Bonello
| cinematography = Josée Deshaies
| editing        = Fabrice Rouaud
| studio         = Les Films du Lendemain My New Picture
| distributor    = Haut et Court
| released       =  
| runtime        = 125 minutes
| country        = France
| language       = French
| budget         = $3.8 million
| gross          = $1.5 miliion 
}}
House of Tolerance ( , also known as House of Pleasures), is 2011 French drama film directed by Bertrand Bonello,  starring Céline Sallette, Hafsia Herzi, Jasmine Trinca, Adèle Haenel, Alice Barnole, Iliana Zabeth and Noémie Lvovsky. The film premiered In Competition at the 2011 Cannes Film Festival.

==Plot==
The story is set in a luxurious Parisian brothel (a maison close, like Le Chabanais) in the dawning of the 20th century and follows the closeted life of a group of prostitutes: their rivalries, their hopes, their fears, their pleasures, and their pains.

==Cast==
* Céline Sallette as Clotilde 
* Hafsia Herzi as Samira 
* Jasmine Trinca as Julie 
* Adèle Haenel as Léa 
* Alice Barnole as Madeleine 
* Iliana Zabeth as Pauline 
* Noémie Lvovsky as Marie-France
* Louis-Do de Lencquesaing as Michaux
* Xavier Beauvois as Jacques
* Jacques Nolot as Maurice

==Production==
 . Left to right: Bertrand Bonello, Iliana Zabeth, Pauline Jacquard, Maïa Sandoz, Judith Lou Lévy, Alice Barnole, Adèle Haenel, Noémie Lvovsky, unidentified.]]
 On War The Man novel with the same name. Bonello says he dreamed about the film two nights in a row while he was writing House of Tolerance, and decided to include a female character with such a scar.   
 Arte France CNC and 416,000 euro from the Île-de-France (region)|Île-de-France region, as well as pre-sales investment from Canal+ and CinéCinéma.     The total budget was 3.8 million euro.    Casting took almost nine months. Bonello wanted a mixed ensemble of both professionals and amateurs who above all worked well together as a group. 

Filming started in Saint-Rémy-lès-Chevreuse on 31 May 2010 and lasted eight weeks.  The film was recorded on one continuous set, which allowed the camera to move between each room without cuts. Bonello chose to focus the camera on the girls and almost never their clients. He explained: "it reinforces the impression that the prostitute is above the client. I told the actresses: Be careful, I want twelve intelligent girls. It was really important for me: theyre not being fooled, they are strong women." 

==Release==
The film had its premiere at the 2011 Cannes Film Festival where it played In Competition on 16 May 2011.    It was the fourth time a film by Bonello was screened at the festival, and the second time in the main competition, after Tiresia from 2003.  Haut et Court distributed the film in France, where it was released on 21 September 2011. 

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 