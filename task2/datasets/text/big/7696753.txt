Lord of the Flies (1963 film)
 
{{Infobox film
| name           = Lord of the Flies
| image          = Lordofthefliesposters.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Peter Brook
| producer       = Lewis M. Allen
| writer         = Peter Brook
| based on       =   James Aubrey Hugh Edwards
| music          = Raymond Leppard
| cinematography = Tom Hollyman
| editing        = Peter Brook Gerald Feil Jean-Claude Lubtchansky British Lion   Continental Distributing  
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = $250,000
| gross          = 
}} novel of the same name. It was directed by Peter Brook and produced by Lewis M. Allen. The film was in production for much of 1961 though the film was not released until 1963. Golding himself supported the film. When Kenneth Tynan was a script editor for Ealing Studios he commissioned a script of Lord of the Flies from Nigel Kneale, but Ealing Studios closed in 1959 before it could be produced.

The film is generally more faithful to the novel than the 1990 adaptation.

==Plot== British schoolboys, living in the midst of a war, are evacuated from England. Their airliner is shot down by briefly glimpsed fighter planes and ditches near a remote island. 

The main character Ralph is seen walking through a tropical forest. He meets an intelligent and chubby boy, who reveals his school nickname was Piggy, but asks that Ralph not repeat that. The two go to the beach where they find a conch shell which Ralph blows to rally the other survivors. As they emerge from the jungle it becomes clear that no adults have escaped the crash. Singing is heard and a small column of school choir boys, wearing dark cloaks and hats, appearing to be walking in pairs, led by a boy named Jack. 

The boys decide to appoint a chief. The vote goes to Ralph, and not Jack. Initially Ralph is able to steer the children (all of whom appear to be aged between about six and fourteen) towards a reasonably civilized and co-operative society. Only boys holding the conch are allowed to speak during meetings or "assemblies". The choir boys make wooden spears, further reinforcing their appearance as warriors within the group. Crucially Jack has a knife, capable of killing an animal.

The boys build shelters and start a fire using Piggys glasses. With no rescue in sight, the increasingly authoritarian and violence-prone Jack starts hunting and eventually finds a pig. Meanwhile, the fire, for which he and his "hunters" are responsible, goes out, keeping them hidden from a passing airplane. Piggy chastises Jack, and Jack strikes him in retaliation, knocking his glasses off, and breaking one lens on the rocks. Ralph is furious with Jack. Soon some of the children begin to talk of a beast that comes from the water. Jack, obsessed with this imagined threat, leaves the group to start a new tribe, one without rules, where the boys play and hunt all day. Soon, more follow until only a few, including Piggy, are left with Ralph.

Events reach a crisis when a boy named Simon finds a sows head impaled on a stick, left by Jack as an offering to the Beast. He becomes hypnotized by the head, which has flies swarming all around it. Simon goes to what he believes to be the nest of the Beast and finds a dead pilot under a hanging parachute. Simon runs to Jacks camp to tell them the truth, only to be killed in the darkness by the frenzied children who mistake him for the Beast. After this, Piggy explains a series of rationalizations and denials that parallel directly the Kübler-Ross model, commonly referred to as the "five stages of grief". The hunters raid the old groups camp and steals Piggys glasses. Ralph goes to talk to the new group using the still-present power of the conch to get their attention. However when Piggy takes the conch, they are not silent (as their rules require) but instead jeer. Roger, the cruel torturer and executioner of the tribe, pushes a boulder off a cliff and kills Piggy. 

Ralph hides in the jungle. Jack and his hunters set fires to smoke him out, and Ralph staggers across the smoke-covered island. Stumbling onto the beach, Ralph falls at the feet of a naval officer who stares in shock at the painted and spear-carrying savages that the children have become, before turning to his accompanying landing party. A small boy tries to tell the officer his name but cannot remember it. The last scene shows Ralph weeping as flames spread across the island.

==Cast== James Aubrey as Ralph
* Tom Chapin as Jack Hugh Edwards as Piggy
* Roger Elwin as Roger
* Tom Gaman as Simon
* David Surtees as Sam
* Simon Surtees as Eric
* Nicholas Hammond as Robert
* Roger Allan as Piers

==Theme==
As with Goldings book, the pessimistic theme of the film is that fear, hate and violence are inherent in the human condition – even when innocent children are placed in seemingly idyllic isolation. The realisation of this is seen as being the cause of Ralphs distress in the closing shots. 

==Production==

===Filming=== wedding night". 
 El Yunque and on the island of Vieques. The boys in the cast had mostly not read the book, and actual scripting was minimal; scenes were filmed by explaining them to the boys, who then acted them out, with some of the dialogue improvised. 

The NYC premiere of the film was attended by some of the original cast members followed by a photo shoot in Times Square (picture subsequently published in a Life Magazine article) and a party at Sardis attended by others such as Zero Mostel.  

===Trivia=== HMS Troubridge. 

==Song==
The song, heard throughout the film, of the boys singing is Kyrie Eleison which, translated from Greek, means "Lord, have mercy". It is an expression used in a prayer of the Christian liturgy.

==Reception==

===Critical response=== average score of 8.3/10. However, it has a mixed to positive score from the audience, with 63%.  

PopMatters journalist J.C. Maçek III wrote "The true surprise in Lord of the Flies is how little these child actors actually feel like child actors. With few exceptions, the acting rarely seems to be forced or flat. This practiced, well-honed craft aids Brook’s vision of a fly on the wall approach that pulls the viewer into each scene." 

===Accolades===
Peter Brook was nominated for the Golden Palm at the 1963 Cannes Film Festival.   

==Home media==
The Criterion Collection released it on DVD and Blu-ray Disc in America and Canada. Janus Films also released the DVD in the UK.

==References==
 

==External links==
*  
*  
*  
*  
*   by Peter Brook

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 