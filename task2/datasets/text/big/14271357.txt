Pop Carn (2003 film)
{{Infobox film
| name           = Pop-carn
| image          = PopCarnfilm.jpg
| alt            = 
| caption        = DVD cover
| film name      =  
| director       = Nassar Kameela Nassar
| writer         = S. Ramakrishnan Nassar (Dialogue)
| screenplay     = Nassar
| story          = Nassar
| based on       =  
| starring       =  
| narrator       = 
| music          = Yuvan Shankar Raja
| cinematography = Dharan
| editing        = S. Sathesh J. N. Harsha
| studio         = Kana Film Makers
| distributor    =  
| released       =  
| runtime        = 127 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Jyothi Naval.   The title is short for the phrase "Pop Carnival",    with a story-line focusing on why marriages between celebrities can suffer due to conflicts between egos.     In JUly 2002 the film was reported as being "in the finishing stages."   

==Plot== Jyothi Naval) attempts to re-unite her father and mother, but fails.

==Cast==
 
* Mohanlal as Vikramaditya
* Simran Bagga as Jamuna Jyothi Naval as Megha
* Kunal Shah as Satish Oorvasi
* Vivek
* Sriman
* Anju
* Pop Shalini Alphonsa
* Sakthi
 

==Production== Malayalam and released in 2007 under the same name.

==Songs==
{{Infobox album|  
| Name = Pop Carn
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover =
| Released = 4 December 2002
| Recorded = 2002 Feature film soundtrack
| Length = 29:01
| Label =
| Producer =  Kameela K. Nassar
| Reviews =
| Last album  = Punnagai Poove (2002)
| This album  = Pop Carn (2002)
| Next album  = Winner (film)|Winner (2003)
}}

The soundtrack was composed by Yuvan Shankar Raja and features seven tracks, the lyrics of which were written by Vaali (poet)|Kavignar Vaali. Although the films lead actor, Mohanlal, was said to sing one of the songs, titled "Amme Inge Vaa", it did not feature either in the soundtrack or in the film itself. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Notes
|- 1 || Mathangi || 4:42 ||
|- 2 || "Kathalaaki Kaninthathu" || S. P. Balasubramanyam, Srilekha Parthasarathy || 4:25 ||
|- 3 || "Antha Semai Thurai" || Hariharan (singer)|Hariharan, Manikka Vinayagam, Sujatha Mohan || 5:25 ||
|- 4 || "Poovellam Paaraddum" || Karthik (singer)|Karthik, Tippu (singer)|Tippu, Pop Shalini || 1:38 ||
|- 5 || "En Isaikku" || Hariharan (singer)|Hariharan, Sriram Parthasarathy || 4:28 ||
|- 6 || "Naan Vachen Lesa" || Srinivas (singer)|Srinivas, Vasundhara Das || 5:19 ||
|- 7 || "Theme Music" || Instrumental || 3:04 ||
|}

==Reception==
Director Nassar predicted that Pop-Carn would be "a bih hit",     and Sify wrote that Pop-carn "is modern family drama about relationships”, expanding that director Nassar and his music director Yuvan Shanker Raja "created a new fusion music that’s elevated and uplifting."    

Now Running gave the film one out of five stars, offering that "There seems to be too many flaws in the script and narration",  and expanded that "The film which is all about music has no noteworthy songs with the music director Yuvan Shankar Raja failing".   They felt however, that the films actors and their performances made the film tolerable. Mohanlals role as Vikramaditya was a "finely tuned performance as the popular-singer turned alcoholic", and Simran in her role as Jamuna was captivating. "Going through the myriad of emotions, Simran is at her best".   

==References==
 

==External links==
*   at CineSouth
*   at the Internet Movie Database

 
 
 
 
 