Crayon Shin-chan: The Legend Called Buri Buri 3 Minutes Charge
 
{{Infobox film
| name           = Crayon Shin-chan: The Legend Called Buri Buri 3 Minutes Charge
| image          =
| caption        =
| director       =
| producer       =
| screenplay     =
| writer         = Yoshito Usui (original manga)
| narrator       =
| starring       = Akiko Yajima Miki Narahashi Keiji Fujiwara Satomi Kōrogi
| music          =
| cinematography =
| studio         = Shin-Ei Animation
| editing        =
| distributor    = Toho
| released       =  
| runtime        =
| country        = Japan
| language       = Japanese
| gross          = $11,351,370
}}
  is a 2005 anime film. It is the 13th film based on the popular comedy manga and anime series Crayon Shin-chan. The film was released to theatres on April 16, 2005 in Japan. 

The film was produced by Shin-Ei Animation, the studio behind the TV anime. It was later released on DVD in Japan on November 25, 2005. 
==Cast==
*Akiko Yajima - Shin-chan
*Keiji Fujiwara - Hiroshi
*Miki Narahashi - Misae
*Satomi Kōrogi - Himawari
*Kunio Murai - Miraiman

==See also==
* Crayon Shin-chan
* Yoshito Usui

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 

 