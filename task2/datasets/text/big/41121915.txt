Seeing Is Believing: Handicams, Human Rights and the News
{{Infobox film
| name           = Seeing Is Believing: Handicams, Human Rights and the News
| image          = 
| alt            = 
| caption        = 
| director       = Peter Wintonick Katerina Cizek
| producer       = Francis Miquet Katarina Cizek Peter Wintonick
| writer         = Peter Wintonick Katerina Cizek
| narrator       = Katerina Cizek
| music          = Eric Lemoyne
| cinematography = 
| editing        = 
| studio         = Necessary Illusions
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Seeing Is Believing: Handicams, Human Rights and the News is a 2002 Canadian documentary film co-directed by Katerina Cizek and Peter Wintonick about the impact of camcorders and digital media on citizen media creation and grassroots democracy.   

The one-hour documentary focuses on Joey Lozano, a videographer helping a tribe in the rural southern Philippines where business interests are taking precedence over human rights.    It also looks at Serb atrocities in Bosnia, skinhead activity in Prague as well as how portable cameras are used by police to film protesters. The film also explores the role of faxes in the Tiananmen Square uprising and the then-emerging use of text messaging in protests.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 