Leif (film)
 
{{Infobox film
| name              = Leif
| image             = Leif movie poster.jpg
| image_size        = 200px
| director          = Claes Eriksson
| producer          = Waldemar Bergendahl
| writer            = Claes Eriksson
| cinematography    = Dan Myhrman
| music             = Claes Eriksson
| country           =  
| language          = Swedish Anders Eriksson Kerstin Granlund Claes Eriksson Knut Agnred Per Fritzell Peter Rangmar Jan Rippe Laila Westersund Kimmo Rajala
| runtime           = 108 minutes
| released          =  
| studio            = AB Kulturtuben AB Svensk Filmindustri
| distributor       = AB Svensk Filmindustri Svenska Filminstitutet
}}Leif is a 1987 Swedish comedy film directed by Claes Eriksson and the first to star the members of Galenskaparna och After Shave.

== Plot == Anders Eriksson), the head of the weapons factory Kanoner & Krut ("Cannons & Gunpowder") in the small community of Rotum plans for the upcoming demonstration of a new weapon called "The Fighting Egg". However, this is interrupted by an article in the local newspaper where the signature Leif claims that the company engaged in the illegal arms trade. The name Rotum spelled backwards is mutor, which is the Swedish word for bribes. When Inspector Mård (Peter Rangmar) and his assistant Nilsson (Jan Rippe), two police officers from Stockholm, comes to the town hell breaks out. At the same time two Iranians arrives to the office to close a deal, when the police knocks at the door. Volt, the deputy directors Max Kroger (Claes Eriksson) and Rambo (Knut Agnred) flees through the window with the Iranians. After stealing some clothes they head to Volts brother Håkan (Per Fritzell), which operates in the entertainment industry.

Later, after a series of incidents like Volts wife Doris (Kerstin Granlund) giving birth to their son, they decide to confront Leif at the demonstration of their latest weapons. After a car chase between the main characters and the Iranians, Volt finds out that he is Leif. The film ends with Volt quitting the weapons industry to start selling Japanese dance bands.

== Cast == Anders Eriksson - Gunnar Volt 
*Kerstin Granlund - Doris Volt / Florence Hasselback
*Claes Eriksson - Max Koger / Mayor Hylén 
*Knut Agnred - Sven "Rambo" Larsson / Tommy Backman 
*Per Fritzell - Niklas Kortnoj / Håkan Volt/ Assarsson
*Peter Rangmar - Inspector Mård / taxi driver / bishop
*Jan Rippe - Lars Erik Ingemar Fred / Assistant Nilsson
*Laila Westersund - Signe Fred 
*Kimmo Rajala and Pierre Jonsson - Iranians

== Trivia ==
The films story proved to compare fairly well with reality. Around the same time as it was filmed media uncovered the Bofors scandal. Regarding this, one can amuse themselves with reading the place name backwards, as it says mutor which is Swedish for bribes.

At the start of the film there is an animation that seems to be inspired by the animated introduction to Monty Pythons Life of Brian. The name "Brian", is replaced with "Leif".

== External links ==
*  
*  

 
 
 
 
 
 