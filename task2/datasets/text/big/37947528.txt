Sindoor (film)
{{Infobox film
| name           = Sindoor
| image          = Sindhoorfilm.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = K. Ravi Shankar
| producer       = A. Krishnamurthy
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Govinda Jaya Prada Neelam Kothari
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =    
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by K. Ravi Shankar and produced by A. Krishnamurthy. It stars Shashi Kapoor, Jeetendra, Govinda (actor)|Govinda, Jaya Prada and Neelam Kothari in pivotal roles.   

==Cast==
* Shashi Kapoor...Professor Vijay Choudhury
* Jeetendra...Prem Kapoor
* Govinda (actor)|Govinda...Ravi
* Jaya Prada...Laxmi Choudhury
* Neelam Kothari...Lalita Kapoor
* Kader Khan...Advocate Dharamdas
* Shakti Kapoor...Shera
* Gulshan Grover...	Sheras Nephew
* Asrani...Chunilal
* Aruna Irani...Ramkatori
* Rishi Kapoor...Mr. Kumar (Guest Appearance)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Patjhad Saawan Basant Bahaar"
| Lata Mangeshkar
|-
| 2
| "Naam Saare Mujhe Bhool Janey Lagey"
| Mohammed Aziz, Lata Mangeshkar
|-
| 3
| "Patjhad Saawan Basant Bahaar (Sad)"
| Lata Mangeshkar
|-
| 4
| "Chalo Chalo Door Kahin"
| Mohammed Aziz, Kavita Krishnamurthy
|-
| 5
| "Jhat-Pat Ghunghat Khol" Hariharan
|-
| 6
| "Patjhad Saawan Basat Bahaar"
| Suresh Wadkar, Lata Mangeshkar
|}

==References==
 

==External links==
* 

 

 
 
 
 