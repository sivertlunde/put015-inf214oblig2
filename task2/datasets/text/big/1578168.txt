The Dangerous Lives of Altar Boys
{{Infobox film
| name           = The Dangerous Lives of Altar Boys
| image          = The Dangerous Lives of Altar Boys movie.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Peter Care
| producer       = Jodie Foster Jay Shapiro Meg LeFauve Todd McFarlane  
| screenplay     = Jeff Stockwell Michael Petroni
| based on       =  
| starring       = Emile Hirsch Kieran Culkin Jena Malone and Jodie Foster
| music          = Marco Beltrami
| cinematography = Lance Acord
| editing        = Chris Peppe
| studio         = Egg Pictures Initial Entertainment Group
| distributor    = THINKFilm
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $12 million   
| gross          = $2,036,599   
}} independent Comedy-drama|comedy-drama film directed by Peter Care. The film stars Emile Hirsch, Kieran Culkin, Jena Malone, Jodie Foster, and Vincent DOnofrio. The film is based on the semiautobiographical Bildungsroman|coming-of-age novel by Chris Fuhrman. 

The film is about a group of Catholic school friends in the Southern United States in the 1970s who engage in a series of pranks and general mischief. The boys also collaborate on a comic book they call The Atomic Trinity. Interspersed within the film are segments of animated footage based on the comic book. 

Fuhrman died of cancer before completing the final draft of the novel.  The film is dedicated to his memory.

==Plot==
Set in the 1970s in the rural South, the film follows the lives of protagonist Francis Doyle (Emile Hirsch), and three of his friends, Tim Sullivan (Kieran Culkin), Wade Scalisi (Jake Richardson), and Joey Anderson (Tyler Long). The four boys all attend a private Catholic school named Agatha of Sicily|St. Agathas, which they detest. The boys experiment with smoking and drinking, obsess over girls, and play pranks on their teachers, such as stealing their schools statue of St. Agatha and keeping it in their clubhouse. The four friends dedicate much of their time to a comic book of their own creation titled The Atomic Trinity in order to escape the monotony and avoid the difficulties in their own lives.

After receiving a love note from Francis, which was actually written by Tim, Margie Flynn ( ); and Sorcerella, based on Margie Flynn.

After a school field trip to the zoo, Tim and Francis have the idea of playing another prank on Sister Assumpta. They decide to drug the cougar at the local zoo and then transport it to Sister Assumptas office to scare her. When they learn how serious Tim and Francis are, the other half of the Atomic Trinity wimp out, which leaves an unlikely group of friends consisting of Margie, Tim, and Francis. Francis soon learns that Margie had been sexually assaulted by her own brother. During gym class, Donny (Arthur Bridges), Margies older brother, bullies Tim during class. Tim, out of pressure and his own impulsive nature, insults Donny for molesting his own sister. He regrets telling Donny, and then tells Francis who becomes angry with him. Donny takes Tim and Franciss comic, The Atomic Trinity and gives it to the nun. The violent, blasphemous and inappropriate drawings in the notebook cause Tim and Francis to be suspended, pending expulsion from the school.

In an act of final retribution, Tim, Francis, Wade, and Joey attempt to steal a cougar to place inside the school to cover up a wrecking of the school they did that night. At the zoo, a makeshift tranquilizer created from several narcotic drugs is used to put the cougar to sleep. The other three boys go down to the gate to retrieve the cougar in a cage, while Tim impulsively climbs over the fence into the cougars den. He checks to see if the cougar is alive, and happily replies that it is. When the other boys reach the gate to retrieve the cougar, another cougar leaps at Tim, mauling him to death. At Tims funeral, Francis quotes the poem "The Tyger" by William Blake, whom Sister Assumpta earlier condemned as a "dangerous thinker". Francis places the book at the stolen statue of St. Agatha in their hideout, and starts a new comic series dedicated entirely to the character based on Tim, Skeleton Boy.

==Cast==
* Emile Hirsch as Francis Doyle
* Kieran Culkin as Tim Sullivan
* Jena Malone as Margie Flynn
* Jodie Foster as Sister Assumpta
* Vincent DOnofrio as Father Casey Jake Richardson as Wade Scalisi
* Tyler Long as Joey Anderson
* Arthur Bridges as Donny Flynn

==Production==
In order to  , and they are visually similar to basic superhero comic strips. Screenwriter Jeff Stockwell helped to create the storyline for the animated sequences.
 American Psycho. 

===Casting===
Casting the characters of Francis Doyle and Tim Sullivan was essential to the films success. Filmmakers say that Emile Hirsch was the first actor that had that innocence and naivete that they were looking for. Similarly, filmmakers believed that Kieran Culkin had a similar personality to Tim. "I think he is going through a lot of similar things  ," states Jodie Foster, producer and star of the film. 

Neither filmmakers nor Foster had planned on her also playing the part of the stern Catholic school teacher and headmistress, Sister Assumpta, as well as being producer. Foster was somehow attracted to the role, and called producing partner Meg LeFauve up with the offer to play this role, who graciously accepted, saying that its not "traditional casting to see a young beautiful woman in that kind of a role." 

===Setting===
Although the novel by Chris Fuhrman was set in 1970s Savannah, Georgia, the filmmakers wanted a more "universal look" and decided not to specify a place.  The filming took place in South Carolina and North Carolina.  There was also a lot of debate about whether the characters should have Southern accents, but to keep with this "universal feeling," the producers decided against any strong accents.

==Reception and awards==
The film has received fairly good praise for an independent film.  s Songs of Innocence and of Experience and Songs of Experience., becoming its one glaringly off-key note." 

Roger Ebert gave the film two and a half stars, calling it "an honorable film with good intentions." Roger Ebert (2009) The Dangerous Lives of Altar Boys  , available: http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20020621/REVIEWS/206210301/1023  .   Ebert goes on to say that the screenplay is trying too hard to impress and falls short of achieving the "emotional payoff" it is searching for. 

In 2002, the film and director Care won the award for Best New Filmmaker from the Boston Society of Film Critics.  In 2003, the film won the Independent Spirit Award for Best First Feature.  

Rotten Tomatoes gives the film a rating of 77%, based on various critical reviews. 

==Soundtrack==
The soundtrack was composed by Marco Beltrami and was released in 2002 by Milan Records. A good part of the songs on the soundtrack were specifically written and performed for this film by Queens of the Stone Age guitarist Joshua Homme. In addition, the Crosby, Stills and Nash song "Do It for the Others" lets the audience know what the time period and setting probably are. 

# "The Atomic Trinity" - Joshua Homme
# "The Atomic Trinity vs. Heavens Devils" - Marco Beltrami
# "The Empty House" - Marco Beltrami
# "The Couch" - Marco Beltrami
# "Hanging (aka Ramble Off)" - Joshua Homme
# "Margies Confession" - Marco Beltrami 
# "The Atomic Trinity vs. Heavens Devils, Round II" - Marco Beltrami
# "St. Agatha" - Marco Beltrami On the Road Again" - Canned Heat
# "Francis and Margie" - Joshua Homme
# "Stoned" - Joshua Homme
# "Dead Dog, Part II" - Marco Beltrami
# "Skeleton Boy is Born" - Marco Beltrami
# "Do It for the Others" - Stephen Stills
# "Story of the Fish" - Marco Beltrami
# "For the Gods / Act Like Cougars" - Marco Beltrami
# "Torn Apart" - Marco Beltrami
# "Someone is Coming" - Marco Beltrami
# "Eulogy" - Marco Beltrami
# "All the Same" - Joshua Homme

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 