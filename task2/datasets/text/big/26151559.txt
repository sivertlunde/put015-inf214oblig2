Voice in the Wind
 
{{Infobox film
| name           = Voice in the Wind
| image          = Voice in the Wind poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Arthur Ripley
| producer       = Rudolph Monter Arthur Ripley
| screenplay     = Friedrich Torberg
| story          = Arthur Ripley
| starring       = Francis Lederer Sigrid Gurie J. Edward Bromberg 
| music          = Michel Michelet Richard Fryer
| editing        = Holbrook N. Todd
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Voice in the Wind is a 1944 American film noir directed by Arthur Ripley and written by Friedrich Torberg, based on a story written by Arthur Ripley. The drama features Francis Lederer, Sigrid Gurie, J. Edward Bromberg, among others. 

==Plot==
 
Jan Foley (Francis Lederer), an amnesiac Czech pianist is a victim of Nazi torture for playing a banned song. Living under a new identity on the island of French-governed Guadalupe Island|Guadalupe, Jan tries to recall his past life while working for crooked refugee-smuggler Angelo (Alexander Granach). 

==Cast==
* Francis Lederer as Jan Volny
* Sigrid Gurie as Marya
* J. Edward Bromberg as Dr. Hoffman
* J. Carrol Naish as Luigi
* Alexander Granach as Angelo
* David Cota as Marco
* Olga Fabian as Anna
* Howard Johnson as Capt. von Neubach
* Hans Schumm as Piesecke
* Luis Alberni as Bartender
* George Sorel as Detective
* Martin Garralaga as Policeman
* Jacqueline Dalya as Portuguese Girl
* Rudolf Myzet as Novak
* Fred Nurney as Vasek

==Reception==

===Critical response===
Bosley Crowther, the film critic for The New York Times, liked the film, writing, "A dark and distressing motion picture, bravely called Voice in the Wind, which deeply laments the violation of all things beautiful in this brutal modern world, is the offering with which Arthur Ripley and Rudolph Monter, a new producing team, are presenting themselves to the public on the screen of the Victoria...Francis Lederer plays the bleakly tragic pianist quite stiffly in his lucid, normal phase and makes a wild and pathetic Looney Louie when he is hopelessly out of his mind. Sigrid Gurie is persistently somber, both in sickness and in health, as his wife, and Alexander Granach and J. Carrol Naish are splendid as two brothers who conflict within the plot. Mr. Ripley has directed the picture, as noted, for mordant effects, and has utilized music elaborately to actuate sad moods. 

===Awards===
Nominations
*  , Michel Michelet; 1945. Best Sound, Recording, Mac Dalgleish (RCA Sound); 1945.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 