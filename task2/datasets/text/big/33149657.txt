Night of Terror
 
{{Infobox film
| name           = Night of Terror
| image          = NightOfTerror1933.jpg
| caption        =
| director       = Benjamin Stoloff
| producer       = Bryan Foy
| writer         = Willard Mack Beatrice Van Lester Neilson William Jacobs
| starring       = Bela Lugosi Sally Blane Wallace Ford Tully Marshall
| music          =
| cinematography = Joseph A. Valentine
| art direction  =
| editing        = Arthur Hilton
| distributor    = Columbia Pictures
| released       = April 24, 1933
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} 1933 horror film directed by Benjamin Stoloff and starring Bela Lugosi, Sally Blane, Wallace Ford, and Tully Marshall.  Despite receiving top billing, Bela Lugosi has a relatively small part.   The film is also known as He Lived to Kill and Terror in the Night. 

==Plot==
Police have been vainly searching the countryside for the knife-wielding Maniac, who has been on a murderous spree killer|spree.  The Maniacs victims are each found with a taunting newspaper clipping attached to their body.  After the wealthy uncle of a young scientist is mysteriously murdered, people wonder if the Maniac is responsible.
 buried after taking a dose of the serum.  Despite his incapacity, the death of his uncle leaves a vast fortune, which is to be divided amongst his family members and servants.  In the event that one or more them dies, the inheritance is split among the remaining survivors.  Subsequently, members of the family begin to die, one-by-one, and suspicion is cast on the servants, including the "mystic" butler (Bela Lugosi).

At the end, we discover that Dr. Hornsby faked his burial and was using it as a cover for committing the murders.  His plan was to kill any other heirs to his uncles fortune so that he may obtain sole possession.  He is eventually discovered and seemingly killed, but in the closing moments of the film, he comes back to life and claims that he will haunt the audience if they reveal the plot twist to anyone.

== Cast ==
* Bela Lugosi as Degar
* Sally Blane as Mary Rinehart
* Wallace Ford as Tom Hartley
* Tully Marshall as Richard Rinehart
* George Meeker as Arthur Hornsby
* Edwin Maxwell as The Maniac
* Gertrude Michael as Sarah Rinehart
* Bryant Washburn as John Rinehart

==Production== International House by day and Night of Terror by night. 

==See also==
* Bela Lugosi filmography

==Notes==
 

==References==
* {{Citation
  | title = The Overlook Film Encyclopedia
  | url = http://books.google.com/books?id=3-ehQgAACAAJ&dq=overlook+film+encyclopedia
  | editor-last = Hardy
  | editor-first = Phil
  | editor-link = Phil Hardy (journalist)
  | volume = 3
  | publisher = Overlook Press
  | year = 1995
  | isbn=0-87951-624-0 }}
* {{Citation
  | title = Golden horrors: an illustrated critical filmography of terror cinema, 1931-1939
  | url = http://books.google.com/books?id=YmBZAAAAMAAJ
  | author-last = Senn
  | author-first = Bryan
  | publisher = McFarland & Company
  | year = 1996
  | isbn=0-7864-0175-3
  | page = 467}}

== External links ==
* 
* 

 
 
 
 
 
 
 