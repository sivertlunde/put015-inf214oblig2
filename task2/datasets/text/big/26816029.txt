Digimon Savers: Ultimate Power! Activate Burst Mode!!
{{Infobox film
| name           = Digimon Savers: Ultimate Power! Activate Burst Mode!!
| image          = 287px-Digimon movie 9.jpg
| film name = {{Film name| kanji          = デジモンセイバーズ: 究極パワー！ バーストモード発動！！
| romaji         = Dejimon Seibāzu: Kyūkyoku Pawā! Bāsuto Mōdo Hatsudō!!}}
| director       = Tatsuya Nagamine
| producer       = 
| writer         = Ryota Yamaguchi
| music          = Takanori Arisawa
| cinematography =
| editing        =
| released       =  
| runtime        = 22 minutes
| budget         =
| gross          =
}} 2006 anime film by Toei Animation based on the Digimon Data Squad anime. It is considered non-canon to the series.

==Plot==
The plot revolves around Agumon, Gaomon and Lalamon, whose partners are put into an eternal sleep, along with the rest of the humans, because of a mysterious thorn that spread throughout the city. After saving Rhythm, a Digimon in the form of a young girl, they learn from her that the thorns are the work of an Ultimate Digimon, named Argomon, and the four set out for his castle to confront him.

==Cast==
*Taiki Matsuno as Agumon
*Kazuya Nakai as Gaomon
*Yukana as Lalamon
*Akiko Yajima as Rhythm
*Hanawa as Argomon
*Souichiro Hoshi as Masaru Daimon
*Kenta Miyake as Ogremon
*Satoshi Tsuruoka, Toshinobu Iida, Shintarou Oohata, Kazunari Kojima as Goblimons

==External links==
*   (Japanese)
*   (Japanese)
*  
*  

 
 

 
 
 
 
 
 
 
 

 