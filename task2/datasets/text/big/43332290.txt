Akash Ar Mati
{{Infobox film
| name           = Akash Ar Mati
| image          = Akash Ar Mati.jpg
| alt            =
| caption        = Promotional poster for Akash ar Mati
| film name      = 
| director       = Fateh Lohani
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = Subal Das
| cinematography = Baby Islam
| editing        = 
| production companies = Film Development Corporation
| distributor    = 
| released       =   
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          =  
}} Bangladeshi black and white film directed by Fateh Lohani and produced by Film Development Corporation (FDC).    It was the first sound feature produced in Bangladesh (then East Pakistan) including post-production,    though used some cast and crew from the Calcutta film industry like Mukh O Mukhosh (The Face and the Mask), the first film made in Bangladesh. 

==Background==
Akash Ar Mati (The sky and the earth) was actor-director Fateh Lohanis second venture. Satrang, a urdu film directed by him was released in 1965.  
He based the film on one of dramatist Bidhayak Bhattacharyas stories.  Akash ar Mati, a musical film, was thematically ambitious films. But it suffered from poor technical knowledge and inexperience of film-makers of Dhaka.   The film flopped commercially. 

Legendary Bangladeshi actress Sumita Devi,    Fazlul Karirt, Pradip, the first Bangladeshi hero Aminul Haque,  Dagu, Ali, Zinat, Rablul, Madhuri, Tejon, and Ranon acted in the film.  Baby Islam was the cinematographer.  Subal Das was the music director.    

==Plot==

 

==Cast==
* Sumita Devi	
* Fazlul Karirt
* Madhuri
* Aminul Haque

==References==
 

==External links==
*  

 
 
 
 
 