Assault on Precinct 13 (2005 film)
{{Infobox film
| name        = Assault on Precinct 13
| image       = Assault on Precinct 13.jpg
| caption     = Theatrical release poster
| director    = Jean-Francois Richet
| producer    = Pascal Caucheteux Jeffrey Silver Stephane Sperry
| writer      = James DeMonaco Assault on Precinct 13 written by John Carpenter
| starring    =  
| distributor = Rogue Pictures
| released    =        
| runtime     = 109 minutes
| country     = United States France
| language    = English
| budget      = $30 million
| gross       = $35,294,470 
| music       = Graeme Revell
}} film of the same name, with an updated plot.

==Plot==
Sergeant Jake Roenick (Ethan Hawke), veteran officer Jasper OShea (Brian Dennehy) and secretary Iris Ferry (Drea de Matteo) are the only people remaining in a soon to be shut down Detroit police precinct on New Years Eve. Roenick is deskbound and abusing alcohol and prescription drugs.  He is haunted by a botched undercover operation eight months prior that resulted in the deaths of two members of his team.  Psychiatrist Alex Sabian (Maria Bello) is treating Roenick at the station.

Meanwhile, crime lord Marion Bishop ( ), petty crook Anna (Aisha Hinds), and counterfeiter Smiley (Ja Rule), but a raging snowstorm forces the two guards to hunker down at Precinct 13 until the storm is over.

Bishop and Police Captain Marcus Duvall (Gabriel Byrne) were formerly partners in crime.  Duvall and his men must kill Bishop before he can testify about their involvement.

Heavily outnumbered and outgunned by the corrupt street cops and SWAT officers, Roenick sets free and arms the prisoners to bolster the defense of the station.  Roenick and Bishop forge an uneasy truce between cops and criminals and their combined efforts repel several more attacks by the corrupt police officers.

Roenick and Bishop decide to take action instead of waiting for another attack.

With so few defenders left alive, the precinct stands vulnerable to attack. OShea suddenly remembers a sewage tunnel running beneath the building and the survivors set fire to the station to cover their escape and flee down the passageway.  When they emerge from the tunnel, the survivors find themselves surrounded by the corrupt policemen. The real traitor is revealed to be OShea and Duvall prepares to execute the rest. Roenick and Bishop work together to survive the final confrontation with Duvall and his men, eventually leading to Roenick killing Duvall and letting Bishop go, giving him a head start.

==Cast==
* Ethan Hawke as Sgt. Jake Roenick
* Laurence Fishburne as Marion Bishop
* Gabriel Byrne as Capt. Marcus Duvall
* Maria Bello as Dr. Alex Sabian
* Drea de Matteo as Iris Ferry
* John Leguizamo as Beck
* Brian Dennehy as Sgt. Jasper OShea
* Ja Rule as Smiley
* Currie Graham as Mike Kahane
* Aisha Hinds as Anna
* Matt Craven as Officer Kevin Capra
* Fulvio Cecere as Ray Portnow
* Peter Bryant as Lt. Holloway
* Kim Coates as Officer Rosen
* Hugh Dillon as Tony
* Tig Fong as Danny Barbero
* Jasmin Geljo as Marko
* Jessica Greco as Coral
* Dorian Harewood as Gil
* Roman Podhora as Cop #3

==Reception==
Assault on Precinct 13 has received mixed or average critic reviews, with a 60% positive rating on Rotten Tomatoes  and a metascore of 54 on Metacritic.  The film was a letdown at the box office. According to Box office Mojo, it made $35 million at worldwide box office on a budget of $30 million.   

==See also==
* List of American films of 2005 Assault on Precinct 13 - the 1976 original The Nest (Nid de guêpes) - French film influenced by original

==References==
 

==External links==
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 