Secret Service (film)
{{Infobox film
| name           = Secret Service
| image          = SecretServiceTitleCard.jpg
| alt            = 
| caption        = Title card for the film James Anderson (assistant)
| producer       = William LeBaron
| screenplay     = Bernard Schubert 
| based on       =  
| starring       = Richard Dix William Post Jr. Shirley Grey Nance ONeil Harold Kinney
| music          = Max Steiner
| cinematography = Edward Cronjager	
| editing        = Jack Kitchin
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Secret Service is a 1931 American drama film directed by J. Walter Ruben and written by Bernard Schubert. The film stars Richard Dix, William Post Jr., Shirley Grey, Nance ONeil and Harold Kinney.   The film was released on November 14, 1931, by RKO Pictures.

==Plot==
Two Union officers, Captain Lewis Dumont and his younger brother, Lieutenant Henry Dumont, receive orders from General Grant to go behind enemy lines and become undercover agents to feed false information to the Confederate Army. Lewis is tasked with becoming part of a Confederate telegraph office in Richmond, Virginia, under the guise of a deceased Confederate officer, Thorne.  Meanwhile, Henry is ordered to allow himself to be captured by the enemy, during which he is supposed to pass incorrect information about Union positions.

Thorne comes upon the tail-end of a skirmish between Union and Confederate soldiers, during which he is wounded.  After the battle, he carries one of the wounded southerners, Howard Varney, back to his familys plantation. This endears Thorne to the family of the wounded rebel, particularly Varneys father, who is a Confederate general. While recuperating from his own wounds, Thorne develops a romantic relationship with Howards sister, Edith.  However, this creates enmity with one of the locals who was interested in Edith prior to Thornes arrival, Arlesford.

When Thorne enlists one of the Varneys slaves to take a message to his brother in the prisoner of war camp, Arlesford, already suspicious of Thorne, sets a trap.  When the trap is sprung, the slave is executed by hanging.  As the two Dumont brothers develop their plan for giving false information, and allowing Union troops to break through the Confederate lines, the younger brother is brought from the prison camp to the Varney plantation. Rather than allow their plan to be uncovered, Henry shoots himself. 

The elder Dumont, torn between duty and his love for Edith, is eventually uncovered as a spy.  However, it is also discovered that he never sent the false information. As he is led away to prison, Edith vows to wait for him.

== Cast ==
*Richard Dix as Captain Lewis Dumont
*William Post Jr. as Lieutenant Henry Dumont
*Shirley Grey as Miss Edith Varney
*Nance ONeil as Mrs. Varney
*Harold Kinney as Lieutenant Howard Varney Gavin Gordon as Mr. Arlesford
*Florence Lake as Miss Caroline Mitford Frederick Burton as General Randolph
*Clarence Muse as Jonas Polk
*Eugene Jackson as Israel Polk

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 