Aakrosh (1980 film)
 
 
{{Infobox film
| name           = Aakrosh
| image          = Aakrosh80.jpg
| caption        =
| director       = Govind Nihalani NFDC / Devi Dutt
| writer         = Vijay Tendulkar Satyadev Dubey
| starring       = Naseeruddin Shah Smita Patil Amrish Puri Om Puri
| music          = Ajit Varman Vasant Dev  (lyrics) 
| cinematography = Govind Nihalani
| editing        = Keshav Naidu
| distributor    = Krsna Movies Enterprise
| released       =  
| runtime        = 144 minutes
| country        = India Hindi
}}
Aakrosh ( ) is a 1980 Hindi arthouse film directed by auteur Govind Nihalani and written by renowned Marathi playwright Vijay Tendulkar.

The film starred Naseeruddin Shah, Om Puri and Amrish Puri in lead roles and went to win 1980 National Film Award for Best Feature Film in Hindi and several Filmfare Awards.

It won the Golden Peacock for the Best Film at the International Film Festival of India. 

This was the debut film of Nihalani, who went on to be known for his dark and frighteningly real depictions of human angst in other landmark alternative movies such as Ardh Satya and Tamas (film)|Tamas. 

It is in the list of 60 films that shaped the Indian film industry spanning six decades. 

==Plot==
Allegedly based on a true incident reported on page 7 of a local newspaper, the film was a scathing satire on the corruption in the judicial system and the victimization of the underprivileged by the able and the powerful. 

Aakrosh forms a part of the series of works, based around explorations in violence, written by noted playwright Vijay Tendulkar, who had earlier written Shyam Benegals Nishant (film)|Nishant (1974) and went to write Govind Nihalanis next surprise breakaway hit, Ardh Satya (1983).

Here the victim is shown so traumatized by excessive oppression and violation of his humanity, that he does not utter a single word almost for the length of the film and only bears a stunned look,  though later he uses the same violence as a tool to express his own sense of violation and rage. 

Basically, the story is of a peasant who is oppressed by landowners and his foremen while trying to eke out a living as a daily laborer. His comely wife, played by Smita Patil, is raped by the foreman who then has him arrested to hide his own crime. His wife commits suicide out of shame. The police bring him to the funeral grounds in manacles and shackles to complete the Last Rites of his dead father by lighting the funeral pyre &mdash; which in the Hindu religion only the son has the right to.
 Andrei Rublev.

==Cast==
* Naseeruddin Shah  as  Bhaskar Kulkarni, Lawyer
* Om Puri  as  Lahanya Bhiku
* Smita Patil  as  Nagi Bhiku
* Amrish Puri  as  Dusane, Public Prosecutor
* Mohan Agashe  as  Bhonsle
* Mahesh Elkunchwar  as  Social worker
* Nana Palsikar  as    Bhikus father

==Aakrosh and Yagnam==
The aggressive act of Lahanya Biku {literal trans.: beggar of the Lahanya caste} mirrors the climax of Telugu short story "Yagnam" by Kalipatnam Ramarao.  Appalanaidu, a character in Yagnam, cuts the throat of his child with an axe after the verdict delivered by village heads comes against him. In view of the future sorrow of his heirs, now that he will not be there to protect them or provide for them, Appalanaidu commits this act. 

In Aakrosh, the story line is very similar, as a similar murderous act is carried out by Biku - as he envisions the traumatic future of his sister.

==Songs==
# Kanha Re - Vandana Khandekar - 7.33, Music :  
# Sanson Mein Dard - Madhuri Purandare - 5.44, Music : Ajit Varman, Lyrics : Suryabhanu Gupt
# Tu Aisa Kaisa Mard - Madhuri Purandare - 3.10, Music : Ajit Varman, Lyrics : Vasant Dev

==Awards==
 
|-
| 1980
| Devi Dutt
| National Film Award for Best Feature Film in Hindi
|  
|- 1981
| Govind Nihalani
| Filmfare Award for Best Director
|  
|-
| Naseeruddin Shah 
| Filmfare Award for Best Actor
|  
|-
| Om Puri
| Filmfare Award for Best Supporting Actor
|  
|-
| Vijay Tendulkar
| Filmfare Award for Best Screenplay
|  
|-
| Vijay Tendulkar
| Filmfare Award for Best Story
|  
|-
| C.S. Bhatti 
| Filmfare Award for Best Art Direction
|  
|}

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 