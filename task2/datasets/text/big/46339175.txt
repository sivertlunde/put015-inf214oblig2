Mandir (film)
 
 
{{Infobox film
| name           = Mandir
| image          = 
| image_size     =
| caption        = 
| director       = Abdul Rashid Kardar
| producer       = Shankar Talkies Corporation
| writer         = 
| narrator       =
| starring       = Rajkumari Dhiraj Bhattacharya Anees Khatoon Akhtar Nawaz
| music          = Professor Ramzan Khan
| cinematography = 
| editing        = 
| studio         = Shankar Talkies Corporation 
| distributor    =
| released       = 1937
| runtime        = 
| country        = British India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| website        =
}} 1937 Hindi/Urdu devotional film directed by A. R. Kardar.      
Produced for the Shankar Talkies Corporation, it had music by Professor Ramzan Khan.    The lyricist was Manjhar Hashiri.   

The cast included Rajkumari, Dhiraj Bhattacharya, Anees Khatoon, Agha, Akhtar Nawaz.   

==Cast==
* Mahmood Shah
* Rajkumari
* Dhiraj Bhattacharya
* Anees Khatoon
* Akhtar Nawaz
* Agha
* A. R. Pahelwan
* R. P. Kapoor

==Soundtrack==
The music director was Professor Ramzan Khan and the lyrics were written by Manjjar Hashiri.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| "Sunheri Duniya Par Kyun Gazab Giraya Ram"
|-
| 2
| "Manao Gao Aaj Badhai Dukh Ke Din Gaye Beet"
|-
| 3
| "O Bhar De Bhr De Bhagwati Hamare Bhandaar Ko"
|-
| 4
| "Tumhe Ho Gyan Tumhin Se Dhyan"
|-
| 5
| "Tripuraari Tum Bhakton Ke Ho Dukh Haari"
|-
| 6
| "Karega Bhala Tera Bhagwan"
|-
| 7
| "Kya Kya Na Kar Raha Hai Duniya Mein Naam Tera"
|-
| 8
| "Khush Ho Ki Bel Booton Ne Gaayi Bahaar Hai"
|-
| 9
| "Jab Jab Hui Dharam Ki Haani"
|-
| 10
| "Jai Jai Durga Mai Kar De Ek Bhalayi"
|-
| 11
| "Jaago Murli Mukutdhari Zulmon Se Roti Hai"
|-
| 12
| "Jhoolao Gao Hari Ko Jhoole Min Jhulao"
|}

==References==
 

==External links==
*  

 

 
 
 
 

 