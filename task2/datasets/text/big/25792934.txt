The Scenesters
{{Infobox film name = The Scenesters image =  caption =  director = Todd Berger producer = Kevin M. Brennan Jeff Grace Brett D. Thompson writer = Todd Berger starring = Blaise Miller Suzanne May Jeff Grace Kevin M. Brennan Todd Berger Sherilyn Fenn music = Dan Houlbrook cinematography = Helena Wei production design = Eve McCarney editing = Kyle Martin distributor = monterey media inc. released =   runtime = 96 minutes country = United States language = English budget =  gross = 
}}
The Scenesters is a 2009 art film|art-house black comedy film written and directed by Todd Berger. The film was made by Los Angeles-based comedy group The Vacationeers and stars Blaise Miller, Suzanne May, Jeff Grace, Kevin M. Brennan, Todd Berger and Sherilyn Fenn. The film was shot in July 2008 in Los Angeles, California, USA, and premiered on October 23, 2009, at the 16th Annual Austin Film Festival.

==Plot==
A group of crime scene videographers go after a serial killer.

== Cast ==
*Blaise Miller as Charlie Newton
*Suzanne May as Jewell Wright
*Jeff Grace as Roger Graham
*Kevin M. Brennan as Investigator Henry Muse
*Todd Berger as Wallace Cotten
*Monika Jolly as Investigator Carlita Travers
*James Jolly as Irving Shaw
*Sherilyn Fenn as A.D.A. Barbara Dietrichson
*John Landis as Judge Paxton B. Johnson
*Robert R. Shafer as George Porter

==Festivals==
The Scenesters has been selected to screen at the following film festivals:
*2009 NewFilmmakers LA (August 31, 2009)
*2009 Edmonton International Film Festival (October 2, 2009)
*2009 New Orleans Film Festival (October 10, 2009)
*2009 Austin Film Festival (October 23, 2009)
*2009 Hollywood Film Festival (October 25, 2009)
*2009 Lone Star International Film Festival (November 11, 2009)
*2010 Slamdance Film Festival (January 22, 2010)
*2010 Oxford Film Festival (February 6, 2010)
*2010 Chicago International Movies and Music Festival (March 5, 2010)
*2010 Lake County Film Festival (March 6, 2010)
*2010 Phoenix Film Festival (April 9, 2010)
*2010 Wisconsin Film Festival (April 16, 2010)

==Awards==
;Slamdance Film Festival
:2010: won Most Interesting Film

;Hollywood Film Festival
:2009: won Hollywood Award for Best Comedy

;Phoenix Film Festival
:2010: won Best Screenplay

;Edmonton International Film Festival
:2009: won Rising Star Award for Best Director &ndash; Todd Berger

;Blue Whiskey Independent Film Festival
:2010: won Best of Fest 
:2010: won Best Screenplay 
:2010: won Best Director 
:2010: won Best Editor 
:2010: won Best Production Designer 
:2010: won Best Actor - Blaise Miller

==Critical reception==
The film received mixed reviews from critics on Rotten Tomatoes. Review aggregate Rotten Tomatoes reports that 50% of critics have given the film a positive review based on 6 reviews, with an average score of 4.9/10, and that 86% of audiences liked it with an average rating of 4.5/5 based on 93 reviews.  Amy Handler from Film Threat called The Scenesters "a film that is provocative, intelligent, hilarious - and that moves so swiftly, that youre left gasping for more.".  Scott Ross from NBC New York said "The Scenesters is a gutsy experiment that rewards the viewers knowledge of Los Angeles, movies and TV with a funny and engaging hyper-meta crime story."   Nick W. from USA Today.com had wonderful things to say about the film saying "The Scenesters is the movie I keep telling my friends about. Its funny, inventive and unlike anything Ive seen in long time"   After the premier of The Scenesters at the Austin Film Festival Sean ONeal from The Onion AV Club praised the film saying it was "A genuinely suspenseful whodunit about a team of wannabe filmmakers exploiting a rash of L.A. murders targeting hipsters...The Scenesters is definitely, if dryly, funny in its satirical take on fame-seeking indie-rock types—boosted by a literally killer soundtrack".  
Todd Gilchrist from Fearnet called the film "A compelling whodunit that manages to pack more of a punch than the latest retro-noir murder mystery, The Scenesters is a satisfying, successful look at predators and prey, hipsters and Hollywood dreamers, and films and filmmakers that doesnt purely chase its own tail - and better yet, doesnt make moviegoers chase it either... destined to become a cult classic."  

==See also==
*9/11 (film)

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 