Death of a Man in the Balkans
Death of a Man in the Balkans ( ) is a 2012 Serbian black comedy film with elements of drama written and directed by Miroslav Momčilović. Following showings on the film festival circuit throughout summer 2012, the film had its Serbian premiere on 16 November 2012, after which it went into general theatrical release.

==Plot==
An introverted loner who composes music for a living (Nikola Kojo) commits suicide in a Belgrade apartment in front of his web camera. 

Drawn by the gunshots, his neighbours, handyman Aca (Emir Hadžihafizbegović) and bean-loving Vesko (Radoslav Milenković), gather in the dead mans apartment waiting for the paramedics, police, and forensic investigator to show up. Their respective wives Nada (Nataša Ninković) and Vera (Anita Mančić) also come in and out. They pass time by eating, drinking, playing chess, and telling stories of the deceased man unaware that the web cam is still running. They extol his music though nobody can quite remember his name.

When they finally arrive, paramedics and police display more concern for their personal mobile phone conversations than for the dead man. Eventually even a glib real-estate agent (Nikola Đuričko) shows up with a reluctant client (Mirjana Karanović) as well as a pizza delivery guy (Miloš Samolov).

It takes the arrival of the forensic investigator (Aleksandar Đurica) to alert the room to the presence of the webcam, which has been filming all along. Now aware of the unblinking eye of the camera, the members of the randomly assembled group suddenly feel the need to justify their previous statements and actions.

==Cast==
*Emir Hadžihafizbegović ... Aca
*Radoslav Milenković ... Vesko
*Nataša Ninković ... Nada, Acas wife
*Ljubomir Bandović ... Policeman #1

==Production==
Made in chamber play format, Smrt čoveka na Balkanu consists of a single 80-minute shot from the web cam perspective. The plot takes place in real time. Director Momčilović said that the scaled-down format of his film is a direct consequence of the global economic crisis: "The latest economic crisis forced me into coming up with a film that doesnt require a lot of initial funds".  He also revealed the initial script idea came from reading an actual local news item about a man suffering a heart attack during a game of table tennis in the local sports center where the other people who happened to be there called the paramedics and then continued playing table tennis while waiting for the ambulance, reasoning they didnt want to waste the valuable time slot at the sports center. 

==Reception==
Jessica Kiang of Indiewire included Death of a Man in the Balkans on her 2013 top ten list, referring to it as "an immensely enjoyable, frequently laugh-out-loud funny story" and "an absurd comedy of manners that observes the very different way people behave when they know they’re being watched as opposed to when they think they’re alone". 

==References==
 

==External links==
* 
* 
* 

 
 
 
 