Mr. and Mrs. Khiladi
 
{{Infobox film
| name           = Mr. and Mrs. Khiladi
| image          = KhiladiMister.jpg
| image_size     =
| caption        =
| director       = David Dhawan
| producer       = Keshu Ramsey
| writer         = Rumi Jaffery
| narrator       =
| starring       = Akshay Kumar Juhi Chawla Kader Khan Satish Kaushik Paresh Rawal
| music          = Anu Malik
| cinematography = K.S. Prakash Rao
| editing        = A. Muthu
| distributor    = D.M.S. Films
| released       = 9 October 1997
| runtime        = 150 minutes
| country        = India Hindi
| budget         =
| gross          = 
}}
 Khiladi series. It was the fifth installment in the series & get title of Superhit.
 Telugu hit Rajendra Prasad, Rambha and Rao Gopal Rao.

== Synopsis ==
When his astrologer uncle (Satish Kaushik) predicts a favorable future for Raja (Akshay Kumar), he decides to do nothing until the prediction comes true. When he meets Shalu (Juhi Chawla) the daughter of a millionaire (Kader Khan) he decides his predicted future has arrived. However, his lazy lifestyle does not meet with his future father-in-laws standards, and he insists that Raja does some hard work for a change.

==Cast==
* Akshay Kumar ... Raja/Mr.Khiladi
* Juhi Chawla ... Shalu/Mrs.Khiladi
* Kader Khan ... Badri Prasad
* Paresh Rawal ... Pratap
* Himani Shivpuri ... Rajas mother
* Satish Kaushik ... Chanda Mama
* Johnny Lever ... bandaged patient on street (as Jhonny Lever)
* Emmanuel Yarborough ... himself/wrestler

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! # !! Song !! Singer(s) !! Length

|-
| 1
| "Akela Hai Mr. Khiladi"
| Udit Narayan, Anuradha Paudwal
| 06:24
|-
| 2
| "Mujhe Hero Ban Jaane De" Poornima
|
|-
| 3
| "Jumme Ke Jumme"
| Abhijeet Bhattacharya|Abhijeet, Poornima
| 07:11
|-
| 4
| "Jab Naukri Milegi"
| Kumar Sanu
|
|-
| 5
| "Samosa Mein Aaloo"
|Abhijeet, Sapna Mukherjee, Poornima
| 05:26
|-
| 6
| "Hum Total Fida Tumpeh"
| Anu Malik, Anuradha Paudwal
| 06:56
|-
| 7
| "Tu Kya Bangala Banaayega"
| Udit Narayan, Alka Yagnik
|
|}

==Location==
The shooting of the song "Akela Hai Mr. Khiladi" took place in Ontario, Canada. Some parts of it were shot at the Niagara Falls, while the other part (75%) was shot near the Square One Shopping Centre in Mississauga. The Central library and other buildings nearby were shown. One of the songs  was shot inside BCE Place in Toronto.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 