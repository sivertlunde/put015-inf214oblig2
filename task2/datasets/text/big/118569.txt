Topkapi (film)
{{Infobox film
  | name =Topkapi
  | image =Topkapi 01(1964).jpeg
  | caption =Original film poster
  | director = Jules Dassin
  | producer = Jules Dassin
  | based on =  
  | writer =  Monja Danischewsky
  | starring = Melina Mercouri Peter Ustinov Maximilian Schell Robert Morley
  | music = Manos Hadjidakis
  | cinematography = Henri Alekan
  | editing = Roger Dwyre
  | studio  = Filmways
  | country = United States
  | distributor = United Artists (1964, original) Metro-Goldwyn-Mayer (2001, DVD) Kino Lorber   (2014, DVD and Blu-Ray DVD)
  | released =  
  | runtime = 119 min
  | language = English
  | budget =
  | gross = $7,000,000   The Numbers. Retrieved May 19, 2013. 
}}
 directed by The Light of Day (1962), adapted as a screenplay by Monja Danischewsky.

The film stars Melina Mercouri (who later became Dassins wife), Maximilian Schell, Peter Ustinov, Robert Morley, Gilles Ségal and Akim Tamiroff. The music score was by Manos Hadjidakis, the cinematography by Henri Alekan and the costume design by Theoni V. Aldredge.

==Plot==
Elizabeth Lipp (Melina Mercouri) visits Istanbul, where she sees a traveling fair featuring replicas of treasures from the Topkapı Palace.  Next she cases the Topkapı, fascinated by the emerald-encrusted dagger of Sultan Mahmud I. Leaving Turkey, she recruits her ex-lover, Swiss master-criminal Walter Harper (Maximilian Schell), to plan the theft. They engage Cedric Page (Robert Morley), master of all things mechanical, and Giulio the Human Fly (Gilles Ségal), a mute acrobat.

Harper and Lipp then hire small-time hustler Arthur Simon Simpson (Peter Ustinov) to drive a car into Turkey to transport hidden explosives and firearms for use in the burglary. Turkish Customs search the car, find the firearms, and conclude that the gang are plotting an assassination. They then recruit Simpson to spy on Harper and Lipp. Page, picking up the car in Istanbul, is told a police ruse that only the "importer" Simpson is permitted to drive it in Turkey. While traveling with the gang, Simpson leaves notes for his police handlers, but most of his intelligence is worthless.

When an accomplice who was to have helped during the robbery is injured, Simpson is engaged as a substitute and confesses that the police are watching them. Harper arranges to give the police the slip. That evening, Harper, Simpson, and Giulio steal the dagger and leave a replica in its place. Unnoticed by the thieves, during the robbery a bird flies through the window they entered  and is trapped inside the vault.

The gang deliver the dagger to Joseph, proprietor of the traveling fair display, who will smuggle it out of the country. The gang members then go to police headquarters to "reveal" their discovery of weapons in the car.  The inspector asks Simpson to vouch for Harper and Lipps whereabouts that day. Simpson backs up their alibi; however, the inspector learns that the Topkapı alarm was triggered by the trapped bird. Simpsons notes to the police provide a connection between the gang and Josephs replicas of the Topkapı treasures.
 Russian Imperial Crown Jewels in the Moscow Kremlin|Kremlin. Then, before the final credits run, four words appear on the screen: There they go again!

== Cast ==
* Melina Mercouri as Elizabeth Lipp
* Peter Ustinov as Arthur Simon Simpson
* Maximilian Schell as Walter Harper
* Robert Morley as Cedric Page
* Jess Hahn as Hans Fisher
* Akim Tamiroff as Gerven the Cook
* Gilles Ségal as Giulio
* Titos Vandis as Harback
* Joe Dassin as Joseph
* Ege Ernart as Major Ali Tufan

==Production==
Amblers novel is different from the movie on several counts, with the story narrated by Simpson (named Arthur Abdel Simpson in the book), so that the reader only gradually comes to work out what Harper and his associates are really up to.  Simpson in the book is blackmailed into driving the car to Istanbul after Harper catches him trying to steal Harpers travelers checks.  The book features frequent flashbacks to Simpsons schooldays in England, which help to explain his character and motives more clearly than in the film.

According to Jules Dassin, he originally planned to cast Peter Sellers as Simpson,  but Sellers refused to work with Maximilian Schell, who he claimed had a reputation for being difficult.  Dassin was not prepared to dispense with Schell, and so cast Ustinov in place of Sellers.

Although he played one of the movies leading roles, Peter Ustinov was nominated in 1964 for—and won—the Academy Award for Best Supporting Actor rather than the Academy Award for Best Actor for his portrayal of Simpson. In an interview given on Ustinovs death in 2004, Maximilian Schell surmised that this may have been due to the misconception that a servile individual like Simpson could only be portrayed via a "supporting actor" role.

Appearing in supporting roles were Gilles Ségal as the human "fly" and the Joe Dassin as Joseph, who runs the traveling fair display that is supposed to smuggle the dagger out of Turkey. The athletic Ségal later inspired other trickwire stunts, including a few used for the Mission Impossible TV show and movie. Joe Dassin was the son of the films director Jules Dassin: he appeared as an actor in a handful of films, but was better known as a singer-songwriter.

The film was shot on location in Istanbul, Turkey, and in Paris at the Boulogne-Billancourt Studios.

==Reception== theatrical rentals. 

==DVD==
Topkapi was released to DVD by MGM Home Video on December 11, 2001 as a Region 1 widescreen DVD and to DVD and Blu-ray DVD on October 7, 2014 by Kino Lorber as a Region 1 widescreen DVD and Blu-ray DVD.

==References==
 
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 