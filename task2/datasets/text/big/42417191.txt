Everything for Gloria
{{Infobox film
| name = Everything for Gloria
| image =
| image_size =
| caption =
| director = Carl Boese
| producer =  Rüdiger von Hirschberg   Friedrich Lorenz (play)   Alfred Möller (play)   Fritz Rau    Johannes Riemann   Carl Boese 
| narrator =
| starring = Leo Slezak   Laura Solari   Johannes Riemann   Lizzi Waldmüller
| music = Anton Profes  
| cinematography = Eduard Hoesch 
| editing = Willy Zeyn     
| studio = Deka Film  
| distributor = 
| released = 25 October 1941 
| runtime = 94 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Everything for Gloria (German:Alles für Gloria) is a 1941 German romance film directed by Carl Boese and starring Leo Slezak, Laura Solari and Johannes Riemann. The film was shot at the  Cinecittà in Rome, and marked the German debut of the Italian actress Solari. 

==Synopsis==
The ambitious female chief executive of a gramophone record company, slowly finds herself falling in love with the companys head of production.

==Partial cast==
*   Leo Slezak as Kammersänger Möbius  
* Laura Solari as Regine Möbius  
* Johannes Riemann as Dr. Herbert Gerlach  
* Lizzi Waldmüller as Anita Rodino  
* Hans Fidesser as Fernando Rodino  
* O.E. Hasse as Dr. Heinz  
* Henry Lorenzen as Max-Egon Schuster-Köhler  
* Erika Helmke as Hidegard Schuster-Köhler   Hermann Pfeiffer as Franz Momber  
* Herbert Weissbach as Eduard Wiesel, Akustiker
* Harald Foehr-Waldeck as Lehrling Erich 
* Bert Schmidt-Maris as Lehrling Viktor

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001. 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 