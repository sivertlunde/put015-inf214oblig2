Muscle Heat
{{Infobox Film
| name           = Muscle Heat
| image          = BloodheatUSdvdcover.jpg 
| caption        = U.S. DVD cover
| director       = Ten Shomiyama
| producers       = Morihiro Kodama Norihiko Tani Kazuya Hamana Toshiaki Nakazawa
| writer         = Story:   Tetsuya Oishi
| starring       = Kane Kosugi Show Aikawa Masaya Kato Naoto Takenaka Ikkei Watanabe
| music          = Kōji Endō
| action directors        = Sam Won Chan Man Chin TBS
| released       = October 26, 2002
| runtime        = 93 min.
| country        = Japan
| language       = Japanese language|Japanese, English
}}
 Japanese action film starring Kane Kosugi and directed by Ten Shimoyama.

== Plot ==
In the year 2009, Joe Jinno, a Navy SEAL who is court-martialed for disobeying orders, is sent to Tokyo to assist Det. Aguri Katsuragi stop the distribution of the illegal drug known as Blood Heat. After an attempt to stop the drug lord Rai Kenjin fails, Katsuragi ends up in Kenjins arena of death, the Muscle Dome, and loses against Kenjins #1 fighter, Lee. Joe wants to seek revenge and needs the help of Katsuragis sister Akane and Ken, a young underground rebel whose father was one a victim of Kenjin and the Muscle Dome, to exact revenge for Aguri and stop the circulation of Blood Heat. 

== Stunts == Rush Hour), and Sam Won (Thunderbolt, Police Story 3|Supercop). A translator was assigned to both men. Kane Kosugi performed all of his own stunts.

==Reception/U.S. release==
Despite not being released in North American theaters, the film received reviews from American critics. High Impact Reviews awarded the film a solid 4 stars out of 4, calling it a "MUST-SEE Japanese action-er!" Netflix.com called it an "Intense pulse pounding action film". Upon the films release in the United States, it was retitled Blood Heat, named after the drug constantly referenced in the movie. Some international DVDs also renamed the film Blood Heat upon release while others kept the films original title.

==Soundtrack==
The film was composed by Kōji Endō. The R&B band Full Of Harmony contributed their song Casino Drive to the films ending credits.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 