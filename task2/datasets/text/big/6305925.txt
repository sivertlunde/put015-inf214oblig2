The Astronaut Farmer
{{Infobox film
| name           = The Astronaut Farmer
| image          = Astronaut farmer.jpg
| caption        = Original film poster
| alt            = 
| director       = Michael Polish
| producer       = Len Amato Mark Polish Michael Polish Paula Weinstein
| writer         = Mark Polish Michael Polish
| starring       = Billy Bob Thornton Virginia Madsen Bruce Dern Max Thieriot
| music          = Stuart Matthewman
| cinematography = M. David Mullen
| editing        = James Haygood
| distributor    = Warner Bros.
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $13 million   
| gross          = $11,130,889 
}}
The Astronaut Farmer is a 2006 American drama film directed by Michael Polish, who co-wrote the screenplay with his brother Mark Polish|Mark. The story focuses on a Texas rancher who constructs a rocket in his barn and, against all odds, launches himself into outer space.

==Plot== FBI and FAA step in to investigate, and the ensuing publicity thrusts Farmer into the spotlight and makes him a media darling.

Farmers launch is delayed by endless red tape created by government officials, who seek to stall him beyond his deadline: foreclose on the farm. Farmer was counting on publicity to help him
financially. He is denied the fuel he wants (liquid hydrogen), officials claiming he is a security risk and that it is too dangerous to allow a private citizen to launch a space vehicle. Facing financial ruin, he panics, climbs aboard, and, using substitute fuel, he somehow launches the rocket. The rocket falls over and horizontally blasts out of the old wooden barn where it was constructed.

Farmer nearly dies with head trauma and other injuries after his capsule is thrown from the rocket. Spectators and their vehicles are nearly crushed. During the months he spends recuperating, public interest in his project wanes, and while he recovers slowly, he is depressed at the failure of the project and of his dream. 

Fortunately, an inheritance unexpectedly left to Audrey by her father Hals passing allows them to bring their debts current. Audrey,  realizing how much Charles dream means to the entire family, encourages Charles to construct another rocket, financing it with the rest of her inheritance. He is able to do so in relative privacy. 
Using a ruse to distract snooping officials, Charles succeeds in launching the rocket, while the FAA claims no such thing has occurred. As the rocket rises out of the barn, the locals and law enforcement authorities in the area are amazed to watch it rise into space. After orbiting Earth nine times and suffering a brief period of a communication blackout, Charles returns safely and is given a heros welcome home, appearing on the Tonight Show with Jay Leno and as seen in still photos shown during the end credits.

==Cast==
*Billy Bob Thornton ..... Charles Farmer
*Virginia Madsen ..... Audrey "Audie" Farmer
*Max Thieriot ..... Shepard Farmer
*Jasper Polish ..... Stanley Farmer
*Logan Polish ..... Sunshine Farmer
*Bruce Willis ..... Colonel Doug Masterson (uncredited)
*Bruce Dern ..... Hal
*Mark Polish ..... FBI Agent Mathis
*Jon Gries ..... FBI Agent Killbourne
*Tim Blake Nelson ..... Kevin Munchak
*Sal Lopez ..... Pepe Garcia
*J. K. Simmons ..... Jacobson
*Kiersten Warren ..... Phyllis
*Rick Overton ..... Arnold "Arnie" Millard
*Richard Edson ..... Chopper Miller
*Elise Eberle ..... Madison Roberts
*Julie White ..... Beth Goode
*Graham Beckel ..... Frank
*Marshall Bell ..... Judge Miller
*Kathleen Arc ..... Mrs. Harder

==Production==
In How to Build a Rocket: The Making of The Astronaut Farmer, a bonus feature on the DVD release of the film, screenwriters Michael and Mark Polish reveal they used their father as a role model for the character of Charles Farmer.

The space suit worn by Farmer is a replica of the Mercury-era Navy Mark IV pressure suit worn by all Mercury Seven astronauts prior to Mercury-Atlas 9. Additionally, the rocket featured in the film is a nearly-scale replica of the Mercury-Atlas that launched Americas first astronauts into Orbital spaceflight|orbit. 
 Rocket Man" Dale Watson, "I Made a Lovers Prayer" by Gillian Welch.

The film premiered at the 2006 Mill Valley Film Festival. Its February 23, 2007 theatrical release in the United States was three days after the 45th anniversary of the countrys first orbital mission, Friendship 7, piloted by John Glenn.

When Thorntons character is being interviewed by Jay Leno during the closing credits, the studio audience members are not extras but the actual audience from that days filming of The Tonight Show with Jay Leno.

==Critical reception==
The movie was a financial flop and a moderate critical success. It maintains a 57% rating on Rotten Tomatoes. Negative reviews concentrate on the ridiculous  plot and the total reliance on "a decidedly American formula of can-do crazy" while positive reviews highlight the same elements as the good things about the movie.
 Twin Falls Idaho and Northfork, have always placed wonderment above storytelling, and the availability of big stars and a reasonable special-effects budget has not entirely blunted their taste for odd, resonant images. The opening shots, of Farmer on horseback in his space suit, hint at a strangeness that the rest of the movie never quite lives up to, but it does have a visual freshness that makes the bromides and clichés palatable." 

Kevin Crust of the Los Angeles Times observed, "Theres something old-fashioned about The Astronaut Farmer thats so conventional it feels unconventional. It follows the paradigm of inspirational movies so perfectly that even the smallest deviation seems rebellious. The movies orthodoxy is precisely what allows us to take such pleasure in its irregularities . . . With this movie, the   brothers have been given a giant coloring book. While both write and produce, Mark directs and Michael acts . . . and for the most part, they attempt to stay within the lines. But its in the few moments when they go outside those lines that the movie momentarily soars." 
 Twilight Zone . . . The brothers are following a path set by David Lynch, the king of weird, who ventured into wholesome territory with The Straight Story and came up with something profound in its simplicity . . . The Polishes set up a classic David and Goliath situation, leaving no question of whom the audience will root for. There are sufficient surprises along the way, so the ending is far from predictable. The Astronaut Farmers goofy quality makes it totally endearing. Its also super entertaining." 

Steve Dollar of the New York Sun said, "Even for a comedy with dramatic drive, The Astronaut Farmer demands that the audience suspend its disbelief on multiple fronts . . . What is believable, however, is the passion of Billy Bob. He genuinely makes all the tearjerker, hug-a-munchkin family stuff resonate. Maybe it takes an actress as sensual and earthy as Ms. Madsen to match Mr. Thornton in emotional honesty, but their grown-up dynamic is what keeps the movie from drifting out of orbit." 

==DVD release==
 .

==See also==
* List of American films of 2006
*SpaceShipOne
*Private spaceflight Brian Walker
*Salvage 1
*Trends (short story)

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 