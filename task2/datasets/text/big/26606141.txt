João and the Knife
{{Infobox film
| name           = João and the knife
| image          = 
| caption        = 
| director       = George Sluizer
| producer       = Roberto Bakker
| writer         = George Sluizer Odylo Costa Filho
| starring       = Joffre Soares
| music          = 
| cinematography = Jan de Bont
| editing        = Jan Dop
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}} Best Foreign Language Film at the 46th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Joffre Soares as João
* Ana Maria Miranda as Maria
* Joao-Augusto Azevedo as Judge Douglas Santos as Zeferino
* João Batista (actor)|João Batista as Deodato
* Áurea Campos as Dona Ana (as Aurea Souza Campos)

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of Brazilian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 