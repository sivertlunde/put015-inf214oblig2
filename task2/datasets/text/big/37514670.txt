Goat Story 2
{{Infobox film
| name           = Goat Story 2
| image          = Kozi pribeh 2.jpg
| caption        = Poster
| director       = Jan Tománek
| producer       = Art And Animation studio
| writer         = Jan Tománek    Jiří Růžička
| starring       = Jiří Lábus   Mahulena Bočanová   Matěj Hádek   Michal Dlouhý   Martin Dejdar   Miroslav Vladyka   Miroslav Táborský   Karel Heřmánek   Milan Šteindler   Dalimil Klapka   Ota Jirák
| music          = David Solař
| cinematography =
| editing        =
| distributor    = 
| released       = October 25, 2012    
| runtime        = 85 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
}} Czech 3D Computer animation|computer-animated feature film. Directed by Jan Tománek and produced by Art And Animation studio. It is a sequel to the 2008 Goat Story. The film was animated by a changed team from the first film, with animators joining the project from countries such as Spain, Bulgaria and India.  The film was released in 2D and 3D. 
The movie was rendered in in-house GPU renderer FurryBall

==Plot==
The Goat leaves old Prague for fairy cheese kingdom. She must save parents of her new friends.

==Cast==
The film stars the voice talents of:
* Jiří Lábus as Goat
* Matěj Hádek as Kuba
* Mahulena Bočanová as Máca
* Michal Dlouhý as Matěj
* Miroslav Táborský as Priest Ignác
* Karel Heřmánek as Devil / Leader
* Dalimil Klapka as Beggar
* Ota Jirák as Taverner
* Filip Jevič as Student

==See also==
*List of animated feature films
*List of computer-animated films

==References==
 

==External links==
*   (in Czech)
*  

 
 
 

 