Soup for One (film)
{{Infobox Film |
 name = Soup for One |
 image =   |
 director = Jonathan Kaufer |
 writer = Jonathan Kaufer |
 starring = Saul Rubinek Marcia Strassman Gerrit Graham Teddy Pendergrass Richard Libertini|
 music = Nile Rodgers Bernard Edwards |
 studio = |
 distributor = Warner Bros. |
 released = April 30, 1982 |
 runtime = 87 min |
 followed_by =|
 language = English |
 budget = |
}}

Soup For One is a 1982 sexually themed romantic comedy that was directed and written by Jonathan Kaufer and produced by Marvin Worth. The Motion Picture Association of America film rating system|R-rated film was released by Warner Bros. Pictures. Its tagline is "When youre looking for love, you find yourself doing some very funny things."

While the film was not a box office success, mostly due to mixed reviews and the over the top sex scenes (including one involving sadomasochism|S/M), it is best remembered for its soundtrack, which was produced by Nile Rodgers and Bernard Edwards of the group Chic (band)|Chic, who performed the title track.

==Overview==
Allan, a cable television producer in New York City, is determined to find the perfect woman, and he would even go so far as having a description of what she would look like done on an artist sketch. But before he can encounter the girl of his dreams he finds himself encountering a series of disastrous dating roadblocks. He finally meets Maria, who seems to be his perfect woman, and tries to make the relationship work.

==The Soundtrack==
 

In addition to "Soup For One" (Pop #80, R&B #14), Chic also performed and produced four other cuts from the soundtrack: "Open Up," "Tavern on the Green," "I Work For A Living," and "Riding." "I Want Your Love" (Pop #7, R&B #5) which is also included, was a 1979 recording. Rodgers and Edwards also wrote and produced three songs for the film, one by Teddy Pendergrass ("Dream Girl"; he performs this in a cameo as a night club singer), another by Sister Sledge ("Lets Go On Vacation") and one for Carly Simon ("Why (Carly Simon song)|Why") (UK #10). Debbie Harry contributed a song to the soundtrack as well, called "Jump Jump."
The album, released on Warner Music Group|WEA/Mirage Records, remains unreleased on compact disc.

Main article: Soup for One (soundtrack)

==Trivia==
* In addition to Pendergrass, another singer, Suzzy Roche of The Roches, also has a cameo.
* The 2000 hit by Modjo, "Lady (Hear Me Tonight)", samples Chics "Soup For One."
* Carly Simons "Why" would end up becoming an underground club classic as well as a major hit in Europe.
* In 1990, A Tribe Called Quest sampled the whole instrumental version of "Why" for a remix of their hit "Bonita Applebum".
* Writer/director Jonathan Kaufer was later briefly married to starlet Pia Zadora with whom he fathered a child.

==Cast==
* Saul Rubinek - Allan 
* Marcia Strassman - Maria 
* Gerrit Graham - Brian 
* Teddy Pendergrass - Night Club Singer 
* Richard Libertini - Angelo 
* Andrea Martin - Concord Seductress 
* Mordecai Lawner - Furniture Salesman 
* Lewis J. Stadlen - Allans Father 
* Joanna Merlin - Allans Mother 
* Christine Baranski - Blonde in Bar 
* Ellen March - Blind Date 
* Maury Chaykin - Dr. Wexler 
* Deborah Offner - Girl in Neon Bedroom 
* Michael Jeter - Mr. Kelp 
* Anna Deavere Smith - Deborah  Laura Dean - Linda 
* Marley Friedman - Zak 
* Andrew Friedman - Zak 
* Jessica James - Miss Farr 
* Kate Lynch - WPCP Receptionist 
* Suzzy Roche - Girl #1 
* Claudia Cron - Girl #2 
* Cheri Jamison - Girl #3  Hilary Shepard - Girl #4 
* Libby Boone - Girl #5 
* Catherine Lee Smith - Girl #6 
* Marisa Smith - Concord Dental Assistant 
* Jaime Tirelli - WPCP Announcer 
* Cristina San Juan - WPCP Weathergirl 
* James Rebhorn - Lawyer 
* Ron Faber - Man with Video Tapes 
* Gloria Cromwell - Mrs. Franken 
* Thomas Quinn - Police Officer 
* Rick Lieberman - Sketch Artist 
* Ellie Covan - Concord Registration Woman 
* Bo Rucker - Man in Washington Sq. Park 
* Jack Chandler - Stoned Cameraman 
* Michael Pearlman - Young Allan 
* Lauren Sautner - Young Rhonda 
* Karen Werner - Receptionist  
* Maggie Wheeler - Nurse 
* Max Gulak - Italian Waiter 
* William Cuellar - Chief Running Brook 
* Mitchell Jason - Judge 
* Sherrie Bender - Topless Girl at Door 
* Kim Chan - Harold the Cook 
* Linda Ray - Sally in Pool 
* Lisa Parker - A Girl in Pool 
* Merwin Goldsmith - Mr. Blum 
* Olivia K. LeAauanae - Fire Knife Dancer 
* Victor Truro - Man at Cellmates 
* Anthony Cesare - Man at River Cafe 
* Marvin Beck - Wedding Guest (uncredited) 
* Jennifer Delora - Girl at Bar (uncredited)

== External links ==
*  

 
 
 
 
 
 