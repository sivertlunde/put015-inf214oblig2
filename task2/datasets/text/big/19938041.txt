Darker than Amber (film)
{{Infobox film
| name           = Darker than Amber
| image          = Darker than Amber-poster.jpg
| image_size     =
| caption        = Original poster
| director       = Robert Clouse
| producer       = Jack Reeves (executive producer) Walter Selzer (producer)
| writer         = John D. MacDonald  Ed Waters
| starring       = Rod Taylor
| music          = John Carl Parker
| cinematography = Frank V. Phillips
| editing        = Fred A. Chulack
| distributor    = National General Pictures
| released       = August 14 1970
| runtime        = 96 min
| country        = U.S.
| language       = English
| budget         = $2,607,328 
| gross          = $1,621,897 Stephen Vagg, Rod Taylor: An Aussie in Hollywood (Bear Manor Media, 2010) p154 
}}

Darker than Amber is a 1970 film adaptation of the John D. MacDonald novel Darker than Amber. It was directed by Robert Clouse from a screenplay by MacDonald and Ed Waters. It starred Rod Taylor as Travis McGee. Darker than Amber and The Empty Copper Sea (adapted as the film Travis McGee (1983) starring Sam Elliott) remain the only McGee novels adapted to the big screen as of 2013. Prior to her death in 2011, with the exception of a documentary appearance in 2007, this film was the last one made by actress Jane Russell.

==Plot summary== Nassau to investigate her would-be murderers, who work in male/female pairs to lure rich, lonely men traveling on cruise ships into their confidence, rob them, and then toss them overboard to drown. McGee and Meyer set out to dismantle the operation.

==Cast==
*Rod Taylor as McGee
*Theodore Bikel as Meyer
*Suzy Kendall as Vangie
*Jane Russell as Alabama Tigress William Smith as Terry

==Production==
Other actors considered for the role of Travis McGee were Jack Lord and Robert Culp. John D. MacDonald pushed for Steve McQueen or Vic Morrow. The movie was shot on location in Florida and Nassau, Bahamas|Nassau. 

==Critical reception==
Though it did not gross well in the box office, the film was praised by such critics as Roger Ebert as "a surprisingly good movie".  The New York Times also gave the film reserved praise, stating that the screenplay lagged in parts despite the good material to work from (in the original novel), and that the real star of the film was its Florida setting. 

It is a cult film, due to its scarcity, and to the fact it is almost never shown on broadcast or on cable TV, and when it is, the fight scene is edited to some extent. If and when a print can be located, there are almost always missing minutes. Though many cuts of this film exist, pristine American prints seem to have disappeared years ago. 

The film played a rare theatrical screening at Anthology Film Archives in New York City, New York, on August 14, 2009.   

==Box Office==
The film recorded admissions of 17,351 in France. 

The film recorded a loss of $2,958,251. Producer Jack Reeves had bought the rights for another McGee novel The_Deep_Blue_Good-by | The Deep Blue Goodbye but it was decided not to proceed with it. 

==Fight scenes and rating==
  William Smith). With the cameras rolling one actor accidentally struck the other (no one has ever revealed who struck the first real blow), the second threw a retaliatory punch, and a staged fight scene became a real fight, with real wounds, real lost teeth, and real blood all over their clothing. 
 William Smith John Saxon, however. 

==Footnotes==
 

==References==
*  
*  

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 