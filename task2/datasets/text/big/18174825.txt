The Stain (film)
 
{{Infobox film
| name           = The Stain
| image          = 
| caption        = 
| director       = Frank Powell
| producer       = Pathe Freres
| writer         = Forrest Halsey (scenario)
| starring       = Edward José Thurlow Bergen
| music          = 
| cinematography = 
| editing        = 
| studio         = Pathé|Pathé Exchange
| distributor    =  Eclectic Film Company
| released       =  
| runtime        = 6 reels
| country        = United States 
| language       = Silent English intertitles
| budget         = 
}}
 silent drama film directed by Frank Powell and starring Edward José and Thurlow Bergen. The film also features Theda Bara (credited under her birth name Theodosia Goodman) in her screen debut. A print of the film was discovered in Australia in the 1990s and is preserved at the George Eastman House.   

==Cast==
* Edward José as Stevens (later The Judge)
* Thurlow Bergen as The young lawyer
* Virginia Pearson as Stevens daughter
* Eleanor Woodruff as Stevens wife
* Sam Ryan as The political boss Theodosia Goodman as Gang moll 
* Creighton Hale as Office clerk

==Production notes==
The Stain was shot at the Fox Studios in Fort Lee, New Jersey and on location in Lake Ronkonkoma, New York.  

==See also==
* List of rediscovered films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 