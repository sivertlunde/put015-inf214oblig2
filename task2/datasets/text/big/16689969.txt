Manifestations of Shiva
{{Infobox Film
| name           = Manifestations of Shiva
| image          = Mani Madhava Chakyar-Sringara-new.jpg
| caption        = Guru Mani Madhava Chakkiar
| director       = Malcolm Leigh
| producer       = Muriel Peters Smithsonian Institution Atlantic Richfield Foundation
| writer         = Malcolm Leigh
| narrator       = 
| starring       = Guru Mani Madhava Chakkiar
| music          = 
| cinematography = Malcolm Leigh
| editing        = Robert T Megginson
| distributor    = Philadelphia Museum of Art, The Asia Society 1980
| runtime        = 90 min
| country        = USA English
| budget         =
| preceded_by    =
| followed_by    =
}}

Manifestations of Shiva is a 1980 documentary film (90-minute) about the Hindu god Shivas worship. It carries the performance aspects of Shiva-worship through dance, performing art, visual art and music. The movies is directed by Malcolm Leigh and main casting is done by the legendary Kutiyattam (2000 year old Sanskrit Indian theatre tradition) artist and authority of Abhinaya (the classical Indian acting style) and Natya Shastra scholar - Nātyāchārya Guru Māni Mādhava Chākyār.

==See also==
*Shiva
*Māni Mādhava Chākyār
*Kutiyattam

==References and further reading==
*  by Prof. L. Erdman, JSTOR Ethnomusicology, Vol. 27, No. 3 (Sep., 1983), pp.&nbsp;577–579
* {{Citation
 | last=Bhargavinilayam
 | first=Das
 | title=Mani Madhaveeyam (biography of Guru Mani Madhava Chakyar)
 | url=http://www.kerala.gov.in/dept_culture/books.htm
 | publisher=Department of Cultural Affairs, Government of Kerala
 | year=1999
 | isbn= 81-86365-78-8
}}

==External links== Joan L. Erdman, JSTOR Ethnomusicology, Vol. 27, No. 3 (Sep., 1983), pp.&nbsp;577–579]
* 
* 
*http://afifest.studiosystem.com/project.aspx?projectid=7135
*  
*  

 
 
 
 
 
 


 