Too Much Johnson
{{Infobox film
| name           = Too Much Johnson
| image          = TooMuchJohnson.jpg Ruth Ford in Too Much Johnson (1938)
| director       = Orson Welles
| producer       = John Houseman Orson Welles
| writer         = Orson Welles
| based on       = Too Much Johnson by William Gillette
| starring       = Joseph Cotten Virginia Nicolson Edgar Barrier Arlene Francis
| music          = Paul Bowles (Music for a Farce)
| cinematography = Paul Dunbar Harry Dunham
| editing        = William Alland Orson Welles Richard Wilson
| studio         = Mercury Theatre
| distributor    =
| aspect ratio   = 1.33 : 1
| runtime        = 40 minutes
| country        = United States
| language       = English
| budget         = $10,000
| gross          =
}}
 Stony Creek Theatre in Connecticut. The resulting plot confusion contributed to the stage productions failure.

The film was believed to be Lost film|lost, but in 2008 a print was discovered in a warehouse in Pordenone, Italy.     The film premiered Wednesday, October 9, 2013, at the Pordenone Silent Film Festival.    In 2014 the film was made available online by the National Film Preservation Foundation.   
 Paramount film in 1919 Lois Wilson and Bryant Washburn. Both of these films are now lost.

This was the second film to be directed by Welles, but his first as a professional. In 1934, while still attending The Todd School for Boys, he co-directed (with friend William Vance) a short avant garde film called The Hearts of Age. 

==Cast==
*Joseph Cotten as Augustus Billings
*Virginia Nicolson as Lenore Faddish
*Edgar Barrier as Leon Dathis
*Arlene Francis as Mrs. Dathis Ruth Ford as Mrs. Billings Howard Smith as Joseph Johnson
*Mary Wickes as Mrs. Battison
*Eustace Wyatt as Faddish Guy Kingsley as MacIntosh
*George Duthie as Purser
*Orson Welles as Keystone Kop
*John Houseman as Duelist

==Production==
The film was not intended to stand by itself, but was designed as the cinematic aspect of Welless Mercury Theatre stage presentation of William Gillettes 1894 comedy about a New York playboy who flees from the violent husband of his mistress and borrows the identity of a plantation owner in Cuba who is expecting the arrival of a mail order bride.    

Welles planned to mix live action and film for this production. The film was designed to run 40 minutes, with 20 minutes devoted to the plays prologue and two 10-minute introductions for the second and third act. Welles planned to create a silent film in the tradition of the Mack Sennett slapstick comedies, in order to enhance the various chases, duels and comic conflicts of the Gillette play. 

"The multi-media concept was a throwback to the early age of cinema when vaudeville shows were punctuated by quick cinematic vignettes," wrote Welles biographer Bret Wood. Bret Wood|Wood, Bret, Orson Welles: A Bio-Bibliography. Westport, Connecticut: Greenwood Press, 1990 ISBN 0-313-26538-0  
 Howard Smith, John Berry, composer Marc Blitzstein and the New York Herald Tribunes drama critic Herbert Drake; Welles gave himself a small role as a Keystone Kop.  Among the uncredited extras for a crowd sequence was a young Judy Holliday. 

Paul Dunbar, a newsreel cameraman for Pathé News, was the films cinematographer. Location photography took place in New York Citys Battery Park and Central Park. Additional shooting took place on a Hudson River day-trip excursion boat and at locations in Yonkers, New York, and Haverstraw, New York.  Interior shots were set up at a studio in the Bronx, New York.  For the Cuban plantation, Welles created a miniature structure next to a papier-mâché volcano, with store-bought tropical plants to suggest the exotic Caribbean flora. 

==Post-production and exhibition problems==
Welles and his crew spent ten days shooting Too Much Johnson, which resulted in approximately 25,000 feet of film. He edited the footage on a  , 1989 ISBN 0-684-18982-8 
 John Berry, who had been hired by the Mercury Theatre as an apprentice and extra for Caesar and worked with Welles on subsequent Mercury productions, including Too Much Johnson. Years later he recalled building the sets on a vacant lot in Yonkers, and organizing a crew that literally held them up against in the wind during filming.  

"Orson had a wonderful time making the film," Berry said, reporting that Welles was so absorbed in the cutting that he took little note when the film caught fire in the projector:

 What I remember, most remarkably, is me running with the projector in my hand, burning, trying to get out of the door into the goddamn hallway, and Houseman racing for the door at the same time — so we had one of those comic who-gets-out-first moments; while Orson, with absolutely no concern whatsoever, was back inside, standing and looking at some piece of film in his hand, smoking his pipe.  , 1997. ISBN 9780312170462   

During post-production, Welles ran into financial problems relating to his cast (who were not originally paid for the film shoot) and the film laboratory, which refused to deliver the processed film until it received payment. Welles also received an attorneys letter from Paramount Pictures informing him that the studio owned the film rights to Too Much Johnson, and that public presentation of his film would require payment to the studio. 
 Broadway trial run, but discovered that the theaters ceiling was too low to allow for film projection. The show opened on August 16, 1938, without the filmed sequences. Audience and critical reaction to the show was poor and Welles opted not to attempt a Broadway version. 

==Loss and rediscovery==
Welles never completed editing Too Much Johnson and put the footage in storage. He rediscovered it three decades later at his home outside of Madrid, Spain. "I cant remember whether I had it all along and dug it out of the bottom of a trunk, or whether someone brought it to me, but there it was", he later recalled. "I screened it, and it was in perfect condition, with not a scratch on it, as though it had only been through a projector once or twice before. It had a fine quality."  Welles, however, never allowed the footage to be seen publicly, stating the film would not make sense outside of the full context of the Gillette play.   In August 1970, Welles, Orson, and Peter Bogdanovich, edited by Jonathan Rosenbaum, This is Orson Welles. New York: HarperCollins Publishers 1992 ISBN 0-06-016616-9  a fire broke out at Welless villa and the only known complete print of Too Much Johnson was destroyed. Heylen, Clinton, Despite the System, Chicago Review Press, 2005 p 10. 

A copy was discovered in Italy in 2008.   and on August 5, 2013, the George Eastman House museum of film and photography in the U.S. announced that it had completed a long process of restoration together with Cinemazero, the National Film Preservation Foundation, and laboratory experts in the U.S. and the Netherlands. Cinema Arts, a Pennsylvania film laboratory, performed most of the preservation work.  "The next step was to begin the actual photo-chemical preservation", said Tony Delgrosso, Head of Preservation at George Eastman House. "For that we turned to a lab called Cinema Arts who are world famous for the quality of their black-and-white photo-chemical preservation and restoration."  

Too Much Johnson premiered October 9, 2013, at the Pordenone Silent Film Festival.     The films North American premiere was held October 16, 2013, at George Eastman Houses Dryden Theatre, and the films New York City premiere took place on November 25, 2013, at the Directors Guild of America Theater. 

On August 21, 2014, the complete 66-minute work print of the film was posted online by the National Film Preservation Foundation.  An edited version running about 34 minutes including intertitles, with new music composed and performed by Michael Mortilla, was also made available.  "If this edit is only a guess, it strives to be an educated one," stated the foundation, "informed by research into the unpublished play scripts left behind by Welles and the Mercury Theatre company." 

==Reception==
Demand to see Too Much Johnson at its world premiere at the Pordenone Silent Film Festival warranted two additional showings.  Reporting the "snaking queues outside the auditorium", The Guardian described the film as "a gleeful experiment in silent cinema pastiche":
 And while it is a work print, not a finished film, Too Much Johnson offers breathlessly enjoyable viewing. Joseph Cotten makes a tremendous movie debut as the plays philandering lead, displaying unimagined guts and agility in a series of tumbles and leaps across Manhattan rooftops, pursued by a prancing, moustache-twirling Edgar Barrier. And Too Much Johnson is itself an affectionate romp through Keystone two-reelers, Harold Lloyds stunt slapstick, European serials, Soviet montage and, notably, Welless favoured steep expressionist-influenced camera angles.  

Variety (magazine)|Variety called the unedited footage "an unanticipated delight":

 There are moments of ultra-broad comedy — Arlene Francis, looking ravishing, hams it up with evident delight at the start — yet the outdoor footage, much of it shot in the Meatpacking District and further south, is a delightful spin on slapstick chase conventions, and a dashing Joseph Cotten, in Harold Lloyd mode, demonstrates a remarkable lack of vertigo along with physical grace. Genuinely funny rather than derivative, the film as seen, complete with multiple takes, feels fresh and spirited up until the last section, when scenes meant to take place in Cuba (but shot near the Hudson, with a few rented palm trees) lose a bit of steam before the amusing final shots.  
 Joseph McBride categorizes Too Much Johnson as Welles’s pre-Hollywood filmmaking experiment,

 a youthful tribute not only to the spirited tradition of exuberant low comedy but also to the past of the medium he was about to enter. Welles was always enamored of the past, though more, he said, of the mythical past we prefer to revere in fantasy than the actual messy past that was, e.g., “Merrie Old England” and “Camelot” rather than the real England of those days. He explores that theme in deeply personal ways in The Magnificent Ambersons and Chimes at Midnight, and Too Much Johnson is also reflective of his obsession with bygone times, cultural mores, and means of expression.  

==See also==
*List of rediscovered films
*Around the World (musical)|Around the World (musical)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 