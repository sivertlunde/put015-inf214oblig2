Play Girl
 
{{Infobox film
| name           = Play Girl
| image	         = Play Girl FilmPoster.jpeg
| caption        = theatrical film poster
| director       = Frank Woodruff
| producer       = Cliff Reid
| screenplay     = Jerome Cady
| starring       = Kay Francis
| music          = Paul Sawtell
| cinematography = Nicholas Musuraca
| editing        = Harry Marker
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 75-77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 James Ellison, Margaret Hamilton and Katherine Alexander.

== Plot == Margaret Hamilton) head to Miami where Grace hopes to find another rich man. When that plan falls through, she stumbles upon Ellen Daley (Mildred Coles), a young lady who is looking for a job as a secretary. Instead, Grace decide to make the girl her protege and teach her how to make money leading older wealthier men on for money. 
 James Ellison) when he fixes their flat tire. All they know is that hes a cowboy, and while Ellen is attracted to him, Grace dismisses him. Grace introduces Ellen to Bill Vincent (Nigel Bruce), a vain man who likes young women, and who coaches Ellen on exactly how to lead a man on enough to get expensive present from him, including a fat settlement to avoid a lawsuit. Despite some initial misgivings, Ellen begins to enjoy her role. 

After they are finished with Vincent in Chicago, the ladies move on to New York City and Van Payson (G.P. Huntley), another older wealthy man who is happy to squire a much younger woman. While out on a date, Ellen runs into Tom and the two of them end up sharing a cab when she gets separated. He makes arrangements to call on her, but Grace, who still thinks that Tom is "just a cowboy" criticizes Ellen for wanting to see him. When she does some research, Grace finds out that Tom is a multi-millionaire and is in favor of the two of them spending time together. She encourages Ellen to marry him because he is so wealthy.

While Ellen is very much in love with Tom, she refuses to consider marrying him because of how she has been earning her gifts. When Tom proposes, she tells him that she needs to think about it for a day. That night she leaves a note for Grace and runs away. Grace decides that since Ellen is gone, shes going to try to run her old game on Tom. 

In a mens club steamroom, Van overhears Bill talking about his experience with Ellen and the two of them realize that they were dealing with the same woman. When the two of them show up at her hotel and accuse her or running a scam, she turns the tables on them accusing Bill and Van of defaming Ellens character and finagles them into paying her outstanding bills and sends them on their way. As they leave they justify their behavior to each other reassuring themselves that they have not been taken for another ride. 

In the meantime, Grace resumes her seduction of Tom and manipulates him into proposing to her. The next morning, Toms mother (Katharine Alexander) pays her a visit only instead of threatening to block the marriage, she just lets Grace know that she knows all about Graces past, but will bless their marriage as long as she promises to always love and care from him. This moves something in Ellen and when Tom comes to visit, she tells him that she knows he still loves Ellen and he should go to Miami and marry her. He rushes out to get on a plane to find her. 

Toms mother lets her know that Toms uncle &ndash; who is also a wealthy cattleman &ndash; is in town alone, and tells her that he is a man who needs to settle down and get married to a woman who can bring som femininity to his batchelors life. When she tells Grace that hes in the lobby of the building, Grace tells Josie her maid to have him sent up while she helps Grace get ready to meet him. as Josie helps her dress, Grace puts on the perfume she uses when shes seducing a man and says to Josie, "For the last time".

==Cast==
  
* Kay Francis as Grace Herbert James Ellison as Thomas Elwood Dice
* Mildred Coles as Ellen Daley, Graces protegée
* Nigel Bruce as William McDonald Vincent, one of Graces former conquests Margaret Hamilton as Josie, Graces maid and confidante
* Katharine Alexander as Mrs. Dice
 
* G. P. Huntley as Van Payson
* Kane Richmond as Don Shawhan
* Stanley Andrews as Joseph Shawhan
* Selmer Jackson as Fred Dice
* Marek Windheim as Dr. Alonso Corivini
 

==Production==
Play Girl began with the working title of "Debutantes, Inc.". Both   

==References==
Notes
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 