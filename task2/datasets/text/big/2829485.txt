The NeverEnding Story (film)
{{Infobox film
| name        = The NeverEnding Story
| image       = Neverendingstoryposter.jpg
| caption     = American release poster
| director    = Wolfgang Petersen
| producer    = Bernd Eichinger Dieter Geissler
| based on    =  
| screenplay  = Wolfgang Petersen Herman Weigel
| narrator    = Alan Oppenheimer
| starring = {{Plainlist|
* Noah Hathaway
* Barret Oliver
* Tami Stronach
* Patricia Hayes
* Sydney Bromley
* Gerald McRaney
* Moses Gunn
}}
| music       = Klaus Doldinger Giorgio Moroder
| cinematography = Jost Vacano
| editing     = Jane Seitz
| studio      = Producers Sales Organization Neue Constantin Warner Bros. Pictures  
| released    =  
| runtime     = 107 minutes
| country     = West Germany
| language    = English
| budget      = DM 60 million (~$27 million)
| gross       = $100,000,000
}} epic fantasy the novel Thomas Hill; USA or the Soviet Union|USSR. The film was later followed by two sequels.   

Ende felt that this adaptations content deviated so far from his book that he requested that production either be halted or the films title be changed; when the producers did neither, he sued them and subsequently lost the case.    The film only adapts the first half of the book, and consequently does not convey the message of the title as it was portrayed in the novel. The second half of the book would subsequently be used as the rough basis for the second film,  . The third film,  , features a completely original plot.

==Plot== Thomas Hill). Atreyu (Noah Hathaway) to discover the cure. Atreyu is therefore given The Neverending Story#AURYN|AURYN. As Atreyu sets out, the Nothing summons Gmork (voiced by Alan Oppenheimer), a werewolf, to kill Atreyu.

 , representing infinity/eternity. The original prop is now owned by Steven Spielberg.  ]]
Atreyus quest directs him to the advisor Morla the Ancient One in the Swamps of Sadness, where his beloved horse Artax is lost to the swamp. Atreyu continues alone, and is surprised when Morla reveals itself as a giant tortoise. Bastian, reading, is also surprised and lets out a scream, which Atreyu and Morla appear to hear. Morla does not have the answers Atreyu seeks, but directs him to the Southern Oracle, ten thousand miles distant. In the walk through the Swamps, Atreyu is rendered unconscious and rescued by the luckdragon  ".

==Cast==
 

* Barret Oliver as Bastian Balthazar Bux. 
* Noah Hathaway as List of The Neverending Story characters#Atreyu|Atreyu.
* Tami Stronach as The Childlike Empress, to whom Bastian gives the new name of "Moonchild."
* Alan Oppenheimer as the voices of Falkor, Gmork, Rock Biter, and the Narrator. Thomas Hill as Mr. Coreander.
* Deep Roy as Teeny Weeny, a messenger riding on a racing snail. narcoleptic bat.
* Moses Gunn as Cairon, a servant of the Empress.
* Sydney Bromley as List of The Neverending Story characters#Engywook and Urgl|Engywook, a gnomish scientist.
* Patricia Hayes as List of The Neverending Story characters#Engywook and Urgl|Urgl, Engywooks wife and a healer.
* Gerald McRaney as Mr. Bux, Bastians widowed, workaholic father.
* Darryl Cooksey, Drum Garrett, and Nicholas Gilbert as Ethan, Todd, and Lucas, who torment Bastian.

==Production==
This film adaptation only covered the first half of the book. The majority of the movie was filmed in Germany, except for Barret Olivers scenes, which were shot in Vancouver, BC, Canada, and the beach where Atreyue falls, which was filmed at Monsul Beach in Almería (Spain). It was Germanys highest budgeted film of the time.

===Music=== theme song Beth Anderson. This song, along with other "techno-pop" treatments to the soundtrack, are not present in the German version of the film, which features Doldingers orchestral score exclusively.
 theme song Norwegian synthpop 2001 maxi-single German techno Scooter on 2007 album Jumping All Over the World. 
 1994 Italy|Italian Club House released the song Nowhere Land (featuring Carl), which combines the melody of the tune Bastians Happy Flight with original lyrics. 

An official soundtrack album was released featuring Doldingers score and Moroders theme tune (Moroder also rescored several scenes for the version released outside Germany). {{cite web
|title=Klaus Doldinger / Original Soundtrack - Never Ending Story 
|url=http://www.cduniverse.com/search/xx/music/pid/1267727/a/Never+Ending+Story+(Die+Unendliche+Geschichte).htm
|accessdate=11 January 2013
}}  The track listing (Doldinger is responsible for everything from track 6 onwards) is as follows:

{{tracklist
| headline = The Never Ending Story (Original Motion Picture Soundtrack)
| collapsed = yes
| title1 = Never Ending Story
| length1 = 3:32
| title2 = Swamps of Sadness
| length2 = 1:57
| title3 = Ivory Tower
| length3 = 3:10
| title4 = Ruined Landscape
| length4 = 3:03
| title5 = Sleepy Dragon
| length5 = 3:59
| title6 = Bastians Happy Flight
| length6 = 3:16
| title7 = Fantasia
| length7 = 0:56
| title8 = Atreyus Quest
| length8 = 2:52
| title9 = Theme of Sadness
| length9 = 2:43
| title10 = Atreyu Meets Falcor
| length10 = 2:31
| title11 = Mirrorgate - Southern Oracle
| length11 = 3:10
| title12 = Gmork
| length12 = 0:29
| title13 = Moonchild
| length13 = 1:24
| title14 = AURYN
| length14 = 2:20
| title15 = Happy Flight
| length15 = 1:21
}}

In Germany an album featuring Klaus Doldingers score was released.

{{tracklist
| headline = Die Unendliche Geschichte — Das Album
| collapsed = yes
| title1 = Flug auf dem Glücksdrachen
| length1 = 3:12
| title2 = Die Unendliche Geschichte (Titelmusik)
| length2 = 2:44
| title3 = Im Haulewald
| length3 = 3:01
| title4 = Der Elfenbeinturm
| length4 = 1:54
| title5 = Atréjus Berufung – Auryn Thema
| length5 = 2:47
| title6 = Phantásien
| length6 = 0:52
| title7 = Artax’ Tod
| length7 = 1:13
| title8 = Die Sümpfe der Traurigkeit
| length8 = 2:39
| title9 = Atréjus Flug
| length9 = 2:27
| title10 = Die uralte Morla
| length10 = 2:27
| title11 = Das südliche Orakel
| length11 = 3:19
| title12 = Die drei magischen Tore
| length12 = 3:25
| title13 = Spukstadt
| length13 = 1:37
| title14 = Flug zum Elfenbeinturm
| length14 = 3:02
| title15 = Mondenkind
| length15 = 1:19
| title16 = Die kindliche Kaiserin
| length16 = 2:16
| title17 = Flug auf dem Glücksdrachen (Schlußtitel) 
| length17 = 1:19
}}

==Reaction==

===Critical response===
The film has a Rotten Tomatoes score of 81% based on reviews from 36 critics.    Metacritic gives the film a score of 46% based on reviews from 10 critics.  

Film critic Roger Ebert gave it three out of four stars and praised its visual effects, saying that "an entirely new world has been created" because of them,  a comment echoed by Variety (magazine)|Variety.  Joshua Tyler of CinemaBlend referred to it as "One of a scant few true Fantasy masterpieces". 

Vincent Canby panned the film as a "graceless, humorless fantasy for children" in a 1984 The New York Times review. Canbys criticism charged that parts of the movie "sounded like The Pre-Teenagers Guide to Existentialism". He further criticized the "tacky" special effects, and that the construction of the dragon looked like "an impractical bathmat." {{cite web|url=http://movies.nytimes.com/movie/review?res=9C02E3D9143AF933A15754C0A962948260|title=The Neverending Story (1984)
|work=New York Times|date=July 20, 1984|last=Canby|first=Vincent|accessdate=11 January 2013}} 

===Box office===
The film performed very well at the box office, grossing $100 million worldwide against a production budget of DM 60 million (approximately $27 million at the time). Almost five million people went to watch it in Germany, a number rarely achieved by German productions, resulting in a gross of about $20 million domestically. It also grossed a similar amount in the United States; only a modest sum in the American market, which director Wolfgang Peterson ascribed to the films European sensibilities. 

===Awards===

This film won two awards in 1984 and three in 1985

* 1984 won the Bambi Award for: National film Golden Screen Award
* 1985 won the Saturn Award for: Best Performance by a Young Actor
* 1985 won the Brazilian Film Award for: Best Production
* 1985 won the Film Award in Gold for: Best Production Design

===Nominations===
This film was nominated for three awards in 1985

* 1985 nominated for the Saturn Award for : Best Fantasy Film, and Best Music
* 1985 nominated for the International Fantasy Film Award for: Best Film
* 1985 nominated for Young Artist Award for : Best Family Motion Picture, Best Young Actor, Best Young Supporting Actress.

==Home media==

===LASERDISC===
The film was released by Warner Bros. on digital LaserDisc with a digital stereo soundtrack in 1985.

===DVD===
The Region 1 DVD was first released in 2001 by Warner Bros, containing only the North American release of the film. The only audio option is a 2.0 stereo mix in either English or Spanish. The theatrical trailer is the lone extra feature presented.

There is also a quite lavish 2003 European version, which is a 2-disc special edition with packaging shaped like the book from the film and containing both the North American and German releases of the film. Various extras, such as a 45-minute documentary, music video, and galleries, are presented on the second disc.  However, there is no English audio for the German version of the film. This edition is currently out of print.
The standard 1-disc edition is also available for the Region 2 market.

A Dutch import has also appeared on the Internet in various places, which only contains the North American release of the film but also includes a remastered DTS surround track, which is not found in either the German or the Region 1 release.

Also, in 2008, Czech and Slovak language DVD versions appeared in Czech Republic and Slovakia.

===Blu-ray===
The first Blu-ray release was a region-free Dutch edition on March 24, 2007.

On March 2, 2010, Warner released a Region A Blu-ray edition of the film. The disc includes a lossless DTS-HD Master Audio 5.1 surround track, which marks the first time a 5.1 surround track has been included in a US home video version of the film. No special features or theatrical trailer are included. 

On October 7, 2014, a 30th Anniversary Edition Blu-ray was released, which duplicates the DTS surround track of its predecessor. Originally described as a "newly" remastered version of the film, Warner released a statement indicating that "the only remastered version is The NeverEnding Story II", while not elaborating further on this current US release.  The 30th Anniversary Edition contains the original theatrical trailer, a commentary track by director Wolfgang Petersen, documentaries and interviews from both 1984 and 2014, and a German-language/English-subtitled feature detailing the digital restoration process of the film.

==Legacy==
The film has since been an inspiration in popular culture. 

=== Music ===
 Atreyu derived their name from the character of Atreyu. Bayside have used quotes from the film as titles of their songs. Examples include "They look like strong hands" and "Theyre not horses, theyre unicorns". Luck Dragon Lady", and live performances of the song are accompanied by a large Falkor puppet.  RPG has resemblances to Limahls Never Ending Story. 
* The American rock band from Austin, TX They Look Like Good Strong Hands also derived their name from the film. They wrote a song inspired by the film called "RIP Artax"

=== Similar story ===

* CLAMPs Magic Knight Rayearth also had a childlike Empress (named "List of Magic Knight Rayearth characters#Emeraude|Émeraude", French for Emerald), who also contacted an outsider to save her fantasy style country. The story was patterned to resemble a RPG; an actual RPG version of it was later on released for the SEGA Saturn.
* Hudson Soft|Hudson-SEGAs Sonic Shuffle for the SEGA Dreamcast pretty much followed the same theme as the NeverEnding Story and Magic Knight Rayearth, with its Maginary, an entire world created by the imagination of its goddess, Illumina. But the game also used contemporary elements, such as from CLAMPs Card Captor Sakura and the upcoming (back then) SEGA of Americas Sonic Adventure 2s theme of Yin-yang|Duality.

== Warner Bros planned adaptation of the novel ==
In 2009, it was reported that Warner Bros, The Kennedy/Marshall Company, and Leonardo DiCaprios Appian Way were in the early stages of creating another adaptation of Michael Endes novel. They intend to "examine the more nuanced details of the book" rather than remake the original film by Wolfgang Petersen. 
 Kathleen Kennedy said that problems securing the rights to the story may mean a second adaptation is "not meant to be." 

==References==
 
 

==External links==
 
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 