Wolf Lake (film)
{{Infobox film
| name           = Wolf Lake
| image          = Wolf Lake (film).png
| image_size     = 
| caption        = 
| director       = Burt Kennedy
| writer         = 
| narrator       = 
| starring       = Rod Steiger
| music          = Ken Thorne
| cinematography = Carlos Montaño Álex Phillips Jr.
| editing        = Warner E. Leighton
| studio         = 
| distributor    = 
| released       = February 8, 1980
| runtime        = 88 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Wolf Lake is a 1978 film directed by Burt Kennedy. It stars Rod Steiger and Susan Tyrrell.  

== Plot ==
The story follows a group of men traveling to a duck hunt in the Northwest. One of them, Charlie, is hoping to travel and forget the grief he has over his sons death in Vietnam. When he learns a young member of his party, David, is a deserter, Charlie goes berserk and hunts David.

==Cast==
*Rod Steiger as Charlie
*David Huffman as David
*Robin Mattson as Linda
*Jerry Hardin as Wilbur
*Richard Herd as George
*Paul Mantee as Sweeney

==References==
 

==External links==
* 

 

 
 