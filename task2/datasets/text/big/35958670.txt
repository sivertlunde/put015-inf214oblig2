The Acquittal
 

{{Infobox film
| name           = The Acquittal	 thumb
| alt            = 
| caption        = Advertisement in Universal Weekly
| film name      = 
| director       = Clarence Brown
| producer       = 
| writer         = Jules Furthman
| screenplay     = Raymond L. Schrock
| story          = 
| based on       =   Barbara Bedford
| narrator       = 
| music          = 
| cinematography = Silvano Balboni
| editing        =  Universal Pictures Universal Pictures
| released       = 1923
| runtime        = 7 reels
| country        = United States Silent
| budget         = 
| gross          = 
}} eponymous play Barbara Bedford. The film was released by Universal Pictures.  A print of the film exists in the Library of Congress archives in the Raymond Rohauer collection. 

==Synopsis==
Who killed Andrew Prentice? Kenneth Winthrop is tried for the murder of his foster-father, Andrew Prentice. His foster-brother, Robert Armstrong is his accuser. Both love Madeline, who has chosen Winthrop. Armstrong alleges that Edith Craig, secretly engaged to the dead man, and Winthrop were in love, but it is not believed. Circumstantial evidence is introduced offering the time as given by a certain clock. Madeline discovers that what they took for a clock in a butchers store window was a circular meat scale, the hand of which stays at twelve, the top of the scale, when not in use. This discovery clears her husband. A postal inspector produces a letter written on the night of the murder by Prentice, which had been stolen in a mail holdup and recovered.  This letter solves the whole mystery and puts the guilt on the shoulders of Prentice, who then confesses and commits suicide. Madeline finally finds happiness with Armstrong, who has always loved her.
::Exhibitors Trade Review (November 3, 1923)

==Cast==
*Claire Windsor - Madeline Ames
*Norman Kerry - Robert Armstrong
*Richard Travers - Kenneth Winthrop Barbara Bedford - Edith Craig
*Charles Wellesley - Andrew Prentice
*Frederick Vroom - Carter Ames
*Mildred Manning - Mrs. Crown
*Ben Deeley - Butler
*Harry Mestayer - District attorney
*Emmett King - Minister
*Dot Farley - Maid
*Hayden Stevenson - Taxi driver

==Reviews and reception==
George T. Pardy wrote in the Exhibitors Trade Review that Brown had "utilized the flashback with excellent effect in developing the evidence in the case, and right here it should be stated that the courtroom stuff, so frequently overdone and burdened with unnecessary wearisome detail where the average film is concerned, is handled with such dexterity and colorful appeal that the trial scene rivets the spectators attention from the beginning to end".  Mary Mac said of the film in the Universal Weekly, that "theres a real mystery for you to set your teeth into. You may figure yourself into delirium and youll probably figure wrong. The picture has been excellently directed, well knit, shorn of irrelevant details, full of suspense, and building steadily and forcefully up to its climaxes". 

A theater manager in Seattle, invited the prosecuting attorneys, county officials, and fifteen law students to a special screening of the film, where he passed out printed ballots for the guests to mark as to who they thought was guilty. Only 4 out of 137 guessed correctly who the murderer was. 

==References==
 

==External links==
* 
* 
* 
*  at the Internet Broadway Database
 

 
 
 
 
 
 
 
 
 