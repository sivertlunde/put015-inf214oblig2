The Hole in the Wall
 
{{Infobox film
| name           = The Hole in the Wall
| image          = The Hole in the Wall dvd cover.jpg
| caption        =
| director       = Robert Florey
| producer       =
| writer         = Pierre Collings Frederick J. Jackson (play)
| starring       = Claudette Colbert Edward G. Robinson David Newell
| music          = Gerard Carbonara W. Franke Harling
| cinematography = George J. Folsey
| editing        = Mort Blumenstock
| distributor    = Paramount Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| awards         = English
| budget         =
}} 
The Hole in the Wall is a 1929 film directed by Robert Florey, and starring Claudette Colbert and Edward G. Robinson. This film marks the first appearance of Edward G. Robinson as a gangster.
<!-- ==Plot==
  -->

==Cast==
*Claudette Colbert as Jean Oliver
*Edward G. Robinson as The Fox
*David Newell as Gordon Grant
*Nellie Savage as Madame Mystera
*Donald Meek as Goofy
*Alan Brooks as Jim
*Louise Closser Hale as Mrs. Ramsay
*Katherine Emmet as Mrs. Carlake
*Marcia Kagno as Marcia
*Barry Macollum as Dogface
*George MacQuarrie as  Inspector
*Helen Crane as Mrs. Lyons

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 