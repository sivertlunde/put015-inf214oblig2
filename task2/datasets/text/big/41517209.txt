Girls (1980 film)
{{Infobox film
| name           = Girls
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Just Jaeckin
| producer       = {{plainlist|
*Léon Zuratas
*Claude Giroux
*Georg M. Reuther   }}
| writer         = {{plainlist|
*Just Jaeckin
*Jean-Luc Voulfow }}
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = {{plainlist| Duncan Mackay
*Eric Stewart }}
| cinematography = Claude Agostini
| editing        = Yves Langlois
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = {{plainlist|
*France
*West Germany
*Canada}}
| language       = French
| budget         = 
| gross          = 
}}

Girls is a 1980 French-West German-Canadian film directed by Just Jaeckin. The film is about four teenage friends who experience the trials and tribulations of maturing into women.

==Cast==
* Anne Parillaud as Catherine Flavin
* Zoé Chauveau as Annie
* Charlotte Walior as Suzanne, Bettys sister
* Isabelle Mejias as Betty
* Christophe Bourseiller as Bernard
 

==Production==
Girls was shot in Montreal in Canada and in Normandy and Paris in France.     The film was a French, West German and Canadian co-production, with funding from the French Society of International Co-productions, FFF - French Movies (Paris), The Caneuram Films (Montreal), TV13 Filmproduktion (Munich). 

==Release==
Girls was released in Paris on May 7, 1980 and June 27, 1980 in West Germany.     The film was shown for 9 weeks in Paris.  In Canada, the film premiered in Montreal on October 2, 1981.  The film was distributed by Les Productions Karim in Montreal and released in both French and an English dub. 

==Notes==
 

==External links==
 

 
 
 
 
 
 
 
 
 
 


 
 