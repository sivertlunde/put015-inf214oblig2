Paris (1929 film)
{{Infobox film
| name           = Paris
| image          = Paris1929PPP.jpg
| image_size     =
| caption        = theatrical poster
| producer       = First National Pictures and Robert North
| director       = Clarence G. Badger
| writer         = Martin Brown E. Ray Goetz Hope Loring(titles)
| starring       = Irene Bordoni Jack Buchanan Louise Closser Hale Jason Robards Sr. Zasu Pitts Edward Ward
| cinematography = Sol Polito
| editing        = Edward Schroeder
| distributor    = Warner Brothers
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| gross          =
| budget         =
|}}
 On with Broadway musical of the same name. The musical was Porters first Broadway hit. No film elements of Paris are known to exist, although the complete soundtrack survives on Vitaphone disks.

Paris was the fourth movie Warner Brothers had made with their Technicolor contract. Paris used a color (Technicolor) process of red and green, at the time it was the third process of Technicolor.  

==Plot==
Irene Bordoni is cast as Vivienne Rolland, a Parisian chorus girl in love with Massachusetts boy Andrew Sabbot (Jason Robards Sr.) Andrews snobbish mother Cora (Louise Closser Hale) tries to break up the romance. Jack Buchanan likewise makes his talking-picture debut as Guy Pennell, the leading man in Viviennes revue.

==Cast==
*Irene Bordoni - Vivienne Rolland
*Jack Buchanan - Guy Pennell
*Louise Closser Hale - Cora Sabbot Jason Robards - Andrew Sabbot
*Zasu Pitts - Harriet
*Margaret Fielding - Brenda Kaley

==Production==
Warner Bros. paid the celebrated French music hall star and Broadway chanteuse Irene Bordoni $10,000 a week to star in this film, playing the role she had originated on Broadway, introducing the enduring Porter standard "Lets Do It, Lets Fall in Love". While this film was being shot, the studio was in the process of completing their all-star revue The Show of Shows (1929), so they had Bordoni film a number for the revue. Their initial intention was to have Bordoni star in two musical features, but due to the poor box-office reception of Paris, they decided not to make any more films with her. 

==Songs==
{| width="65%"
|-
| width="50%" valign="top" |
*"My Lover"
*"Paris"
*"Somebody Mighty Like You"
*"An Furthermore"
*"Wob-a-ly Walk"
*"Dont Look at Me That Way"
*"Crystal Girl"
*"Im a Little Negative"
*"I Wonder What is Really on his Mind"
*"Miss Wonderful"
*"Among My Souvenirs"
*"The Land of Going to Be"
| width="20%" valign="top" |
|}

==Advertisement==
Paris utilized advertisements of a type which were common for its time, featuring the talking in the film and Irene Bordoni starring. One ad for Paris said "See the talking picture of the future".

==See also==
*List of early color feature films
*List of incomplete or partially lost films

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 