Reign of Terror (film)
{{Infobox film
| name           = Reign of Terror
| image          = The Black Book Poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Anthony Mann
| producer       = William Cameron Menzies Walter Wanger  (executive, uncredited)
| screenplay     = Aeneas MacKenzie Philip Yordan
| story          = Aeneas MacKenzie Philip Yordan
| starring       = Robert Cummings Richard Basehart
| music          = Sol Kaplan
| cinematography = John Alton Fred Allen
| Studio         = Walter Wanger Productions
| distributor    = Eagle-Lion films International Film Distributors (1949) (Canada) Super-Film Verleih GmbH (1950) (West Germany) Hygo Television Films (1953) (USA, TV) Carroll Pictures (1954) (USA) (Theatrical Re-Release) Hollywoods Best (1995) (USA, DVD) Alpha Video Distributors (2003 & 2004) (USA) (DVD) Reel Media International (2004) (Worldwide) (VHS) Hollywoods Best (2005) (USA, VHS) Payless Entertainment (2005) (Australia, DVD) Reel Media International (2007) (Worldwide, all media) Punto Zero (2008) (Italy, DVD) Koch Media (2013) (Germany, DVD) Sony Pictures Home Entertainment (2012) (USA, DVD)
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $771,623 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p445. 
| gross          = $692,671 
}}
Reign of Terror (also known as The Black Book) is a 1949 American drama film directed by Anthony Mann and starring Robert Cummings, Richard Basehart and Arlene Dahl. The film is set during the French Revolution. Plotters  seek to bring down Maximilien Robespierre and end his bloodthirsty Reign of Terror. 

==Plot== Richard Hart), the only man who can nominate him before the National Convention. Barras refuses to do so and goes into hiding.

Meanwhile, patriot Charles DAubigny (Robert Cummings) secretly kills and impersonates Duval (Charles Gordon), the bloodstained prosecutor of Strasbourg#From Thirty Years War to First World War|Strasbourg, who had been summoned to Paris by Robespierre for some unknown purpose (which Robespierres enemies want very much to ascertain). Neither Robespierre nor Joseph Fouché|Fouché (Arnold Moss), the chief of his secret police, have met Duval before, so the substitution goes undetected. Robespierre informs DAubigny that his black book, containing the names of those he intends to denounce and have executed, has been stolen. Robespierres numerous foes are kept in check by the uncertainty of whether their names are on the list. If they were to learn for certain that they are, they would band together against him. He gives DAubigny authority over everyone in France, save himself, and 24 hours to retrieve the book.
 Richard Hart) through his sole contact, Madelon (Arlene Dahl), whom DAubigny once loved. However, he was followed, and Barras is arrested by the police, led by Louis Antoine de Saint-Just|Saint-Just. DAubigny finds himself in an uncomfortable position, but manages to allay the suspicions of both sides that he has betrayed them.

He goes to visit Barras in prison, and informs him that three of his best men have been murdered. Strangely, their rooms had not been ransacked to search for the book, leading DAubigny to surmise that it was never stolen in the first place, and that Robespierre is using the alleged theft to distract his foes. Saint-Just, still suspicious, sends for Duvals wife to identify her husband. Through quick thinking, Madelon sends a friend to pretend to be Madame Duval and extricates her former lover.

Before news of his impersonation gets out, DAubigny returns to Robespierres private office to look for the book. There he encounters the opportunistic Fouché, who is seemingly willing to sell out his master. When DAubigny finds the book, however, Fouché tries to stab him. DAubigny strangles him into unconsciousness and escapes.

He and Madelon hide out at the farmhouse of a fellow conspirator, but their location is extracted through torture. A nighttime chase ensues. DAubigny gets away, but Madelon is caught, taken back to Paris, and tortured. She refuses to talk.

As the Convention is about to convene the next day, Fouché shows up and offers to trade Madelon for the book. DAubigny turns him down. The book is passed from hand to hand among the delegates. Thus, when Robespierre arrives to denounce Barras, the crowd turns on him instead. He nearly brings the mob to heel with his golden words, but Fouché has his henchman shoot Robespierre through the jaw, silencing him. Robespierre is taken to meet Madame Guillotine.

DAubigny searches Robespierres office, finds a secret room, and rescues Madelon. Fouché meets an army officer in the crowd. When asked his name, the man replies, "Bonaparte. Napoleon Bonaparte." Fouché promises to remember the name.

==Cast==
* Robert Cummings as Charles DAubigny
* Richard Basehart as Maximilien Robespierre Richard Hart as François Barras
* Arlene Dahl as Madelon
* Arnold Moss as Fouché Norman Lloyd as Jean-Lambert Tallien|Tallien, Barras associate
* Charles McGraw as Sergeant
* Beulah Bondi as Grandma Blanchard, the woman who shelters DAubigny and Madelon at the farmhouse
* Jess Barker as Saint-Just

==Production== Walter Wagner, director Anthony Mann, cinematographer John Alton and production designer William Cameron Menzies used their combined talents to make a low budget "epic" using Broadway stars and shooting on sets only costing $40,000. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 