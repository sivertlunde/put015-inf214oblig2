Fatal Deviation
{{ infobox film
| name           = Fatal Deviation
| image          = Fatal_deviation.jpg
| image size     =
| alt            = 
| caption        = VHS cover
| director       = Shay Casserley Simon Linscheid
| producer       = James Bennett Justin Harmon
| writer         = Simon Linscheid
| narrator       = 
| starring       = James Bennett  Mikey Graham  Colin Hamilton   Justin Harmon  Michael Regan
| music          = Donal Norton
| cinematography = Shay Casserley
| editing        = John Cook Video Productions
| studio         = 
| distributor    = Rising Sun Productions
| released       =  
| runtime        = 76 minutes
| country        = Ireland English
| budget         = Irish pound|IR£8,900.23
| gross          = 
| preceded by    = 
| followed by    = 
}}
Fatal Deviation is a low-budget cult film produced and set in Trim, County Meath, Ireland. Produced in 1998, it enjoys the distinction of being Irelands first full-length martial arts film. The film stars real-life martial arts enthusiast James Bennett. The film was conceived by James Bennett and Peter Crinion. The movie went straight to video without a theatrical release. In 2010, Luke McKinney of Cracked.com labeled the film "the worst film ever made." 

==Plot==
Fatal Deviation tells the story of Jimmy Bennett, a disenfranchised young man trying to rebuild his life. On returning home after a ten year stay in St. Claudes Reform School, he aims to discover who he is,what it is he should do and what happened to his father. Shortly after his return to his hometown, Jimmy gets on the wrong side of a local gang, beating up two of its members who had been harassing local shop worker Nicola, in an attempt to force her to date gang member Mikey (played by Mikey Graham of Boyzone fame). The fight is witnessed by a monk in a local secret kung-fu group, with mysterious links to Jimmys father. This order organises the Bealtaine tournament, an underground no-rules fighting tournament, in which Jimmy is invited to partake.

The gang leader, known only as Loughlan, who happens to be the father of the aforementioned Mikey, decides they should add Jimmy to their group ("Why not? Wouldnt it be ironic to have the son of the man I killed working for us?"). When Jimmy refuses to join them, they turn on him.  Loughlan arranges for henchman Seagull to return from his successful mission in Hong Kong on a direct flight to Trim airfield in order to take part in the festival.  Meanwhile Mikey has Nicola kidnapped and leaves Jimmy a note warning him to "Loose   or else".

Jimmys fortunes begin to change when he is brought under the tutelage of a group of mysterious local monks who had trained his father, a martial arts champion, many years before. Under the guidance of the mysterious head monk, Jimmy undertakes an intensive training programme in preparation for the tournament. Jimmy goes on to reach the final, where he faces Seagull. After remembering that he had witnessed the local drug lord kill his father with a sword in the front of him, back when he was a child, he defeats Seagull with a well-timed use of the mysterious "Fatal Deviation" move as taught to him by the head monk.

Having triumphed over Seagull, Jimmy then takes on the gang and rescues his girlfriend. In doing so, he kills Mikey.  On hearing of his sons death the gang leader seeks bloody vengeance ("You killed my son, now Im going to kill you, just as I killed your father"), but Jimmy ultimately dispatches him too.  The film ends with Jimmy reunited with his girlfriend and looking confidently forward to a happy and peaceful future in Trim.

==Cast==   
* Jimmy Bennett as Jimmy Bennett
* Mikey Graham as Mikey
* Michael Regan as Loughlan
* Nicola OSullivan as Nicola
* Colin Hamilton as Seagull
* Justin Harmon as Man in Bath

==Production==
* The car crash towards the end of the film was not intended, but was included for dramatic effect. The crash is shown in a post credits sequence. 
Fatal Deviation
Production started using a Sony Hi8 camera ( which got destroyed during filming ) and all scenes were reshot when Shay Casserley took over as Director and Camera operator shooting the film on SVHS.
http://www.amazon.ca/Fatal-Deviation/dp/B00009ZK5T
Quote: One of the great things about the film is that one of the car rollovers which happens in the film was not suppose to happen at all but the stunt team got carried away and you will see a real wreck that nearly killed some of the actors. 

==References==
 

* {{cite web
| year = 2007
| author = Kevin Morron
| title =  Bits and Pecans
| url = http://www.mongrel.ie/issue14/reviews.php
| archiveurl = http://web.archive.org/web/20070512160758/http://www.mongrel.ie/issue14/reviews.php
| archivedate = 2007-05-12
| publisher = Mongrel
| accessdate = 2009-10-06
}}

==External links==
*  
*  
*   on RTÉs "Nationwide" 1998

 
 
 