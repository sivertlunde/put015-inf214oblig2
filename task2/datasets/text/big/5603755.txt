Habeas Corpus (1928 film)
{{Infobox film
| name = Habeas Corpus
| image = L&H_Habeas_Corpus_1929.jpg
| caption = Theatrical release poster
| director =  Leo McCarey James Parrott
| producer = Hal Roach
| writer = Leo McCarey H.M. Walker
| starring = Stan Laurel Oliver Hardy Richard Carle Charles A. Bachman Charley Rogers
| music =
| cinematography = Len Powers
| editing = Richard C. Currier
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 20 min.
| language = Silent film English (Original intertitles)
| budget =
| country = United States
}}
Habeas Corpus is a 1928 short comedy silent film starring Laurel and Hardy as grave-robbers hired by a mad scientist. It was shot in July 1928 and released by M-G-M on December 1st.  Although technically a silent film — having intertitles and no synchronized dialogue — it was the inaugural Roach film released with a synchronized music and sounds effects track for theatres wired for sound.  The Victor sound discs were long thought lost until a lone set surfaced in the 1990s and was reunited with the film elements.   

==Cast==
* Stan Laurel - Stan
* Oliver Hardy - Ollie
* Richard Carle - Professor Padilla
* Charley Rogers - Ledoux
* Charles A. Bachman - Detective

== See also ==
* Laurel and Hardy films

==References==
 

== External links ==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 

 