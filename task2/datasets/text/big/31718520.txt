Deadlock (1931 film)
{{Infobox film
| name           = Deadlock
| image          = 
| image size     =
| caption        = 
| director       = George King
| producer       = George King Charles Bennett (story) Billie Bristow (story) H. Fowler Mear
| based on       = 
| narrator       =
| starring       = Stewart Rome Marjorie Hume Warwick Ward
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1931
| runtime        = 85 minutes
| country        = United Kingdom English
| budget         =
| gross          = 
| preceded by    =
| followed by    = George King 75 Most Wanted list of lost films. 

==Plot==
A murder takes place in a film studio during the shooting of a new film.

==Cast==
* Stewart Rome as James Whitelaw 
* Marjorie Hume as Mrs Whitelaw 
* Warwick Ward as Markham Savage 
* Annette Benson as Madeleine dArblay 
* Esmond Knight as John Tring 
* Janice Adair as Joan Whitelaw 
* Alma Taylor as Mrs. Tring  Cameron Carr as Tony Makepeace 
* Hay Plumb as Publicist 
* Pauline Peters as Maid 
* Kiyoshi Takase as Taki 
* Philip Godfrey as Nifty Weekes 
* H. Saxon-Snell as Prosecutor

==References==
 

 

==External links==
* 
* , with extensive notes

 
 
 
 
 
 
 

 