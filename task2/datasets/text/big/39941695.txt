Misa's Fugue
 
 
Misa’s Fugue is a documentary film created and produced by students and faculty of the Fleetwood Area High School in Fleetwood, Pennsylvania.  The documentary tells the story of Holocaust survivor Frank Grunwald   using student-produced artwork, as well as photos and film from both archived sources and the Grunwald family collection.  The film was a collaboration of the school district’s media, technology, history, music, art, and English departments. http://www.readingjewishcommunity.org/local_includes/downloads/43996.pdf 

== Summary ==
 
The film begins by defining “fugue,” as a musical arrangement of several different components woven together.   

Holocaust survivor Frank Grunwald narrates his life story, beginning with his youth amidst the artistic elite of early twentieth-century Prague.  When the Nazis invaded Czechoslovakia in 1929, Grunwald was sent to internment and concentration camps. http://www.readingjewishcommunity.org/local_includes/downloads/44528.pdf  The film depicts his experiences between 1939 and 2010 as he continues to work on an incomplete sculpture that appears to be a hunched over woman in agony.    Grunwald tells how his older brother John, who was born with a congenital defect, was accompanied by his mother to the gas chambers at Auschwitz-Birkenau, and how his relationship with artist Dina Babbit spared him the same fate. 

The film describes Grunwalds liberation from Gunskirchen in 1945,   his reunion his father, and their escape from communist Czheckoslovakia across Europe to London and later The United States.   

The film depicts Grunwalds love of art and music, his a career at General Electric, his professorial position at Pratt Institute, and his life in Indianapolis, Indiana, where he lives with his wife.   He has two sons and five grandchildren. 

The film concludes with a discovery Grunwald makes after his father commits suicide in 1965: a letter that his mother had written to her husband before she and John were taken to the gas chambers.    

The film identifies the clinical definition of “fugue”: a loss of identity, especially following some significant trauma.   

In the final shots of the movie, Grunwalds completed sculpture is revealed, named for the date his mother and brother were murdered in a Nazi gas chamber.

== Development ==

Director Sean D. Gaston, a media educator who previously spent fifteen years in the movie industry, taught a “Holocaust through Film” course with history teacher and Holocaust scholar Jennifer Goss at Fleetwood Area High School.   During a 2010 summer workshop promoting Holocaust education, Gaston met Frank Grunwald, the subject of Misa’s Fugue.   During the next few months, Gaston, Goss, cinematographer Jim Hollenbaugh and photographer Matthew Goss filmed Grunwald’s testimony at his home near Indianapolis, IN.

At the start of the 2010-2011 school year, Gaston and Goss began writing the script for the film with English teacher Zachary Houp,     truncating and reorganizing the story into an accessible documentary narrative. Gaston’s media students edited and transcribed the interview film.  These students were assisted by professional editor, Chad Haberstroh.  Gaston enlisted the art and music students of Fleetwood Area School District to complement the film’s audio and visual tracks with the necessary elements of a full-fledged motion picture.  Orchestra director Sarah Shuey, Band director Charles Ebersole, and Choral director Cathy Williamson oversaw the musical performances of the score which was composed by FAHS graduate Justin Reinert.  The student artwork was guided by Art teacher Diane Chisdak.    Initial advertising media for the film was created by students in the FAHS Technical Education Department under the guidance of teacher Sara (Shelton) Sanocki.

After 22 months of post-production work that included seven departments in the school district, 200 students, nine faculty members, and four industry professionals, Misa’s Fugue premiered at the Miller Center on the campus of Reading Area Community College in Reading, PA, on April 16th, 2012.  

== Release ==

After the official premier, the first run of 1,000 DVDs became available for history scholars throughout the country for the price of shipping and handling.    Subsequently, the film played for two weeks at the Goggleworks art theatre in Reading, PA.  A repeat performance at the Goggleworks occurred several months later by popular demand.

Later screenings occurred on university campuses, such as Kutztown University and Millersville University.   Nearby churches screenede the film during fundraising endeavors.   In the months following the premiere, Misa’s Fugue also screened at several film festivals.  Hollywood film producer Gary Goetzman assisted the film as a late entry in the 2012 Philly Film Festival.     In addition, the film was an official selection of the Phoenix Film Festival, The New Hope Film Festival, and the Greater Reading Film Festival. 

The United States Holocaust Memorial Museum hosted a special internal screening of the film for employees and Museum survivor volunteers. 

The film is available to educators for only the price of shipping and handling via the films official website. 

== Reception ==

The film was well received at the premier.  and has been accepted for screening at several festivals.  To date, the film has been seen in more than seven countries, and more than 3,000 DVDs have been disseminated around the world.   

== Technical Details ==

The color feature film is 97 minutes in length with a 16:9 aspect ratio using 6-track stereo sound The film was edited exclusively on Apple hardware, using the Final Cut Studio editing suite.   With the exception of archival photographs and film footage, all other visuals where filmed using an HD camera on loan from Fleetwood Area School District.  The film is primarily in English with intermittent subtitled scenes in which Czech or German is spoken.  Filming locations include Indianapolis, IN; Fleetwood, PA; Washington, DC; Ephrata, PA; and Smyrna, NY.  The estimated budget for the production was $13,000.  Butter Productions, LLC, is the official production company holding the copyright to the film Misa’s Fugue.

== Awards ==

The Pennsylvania School Press Association (PSPA) President Robert Haneke presented Gaston with the Rachel S. Turner award for Innovative Excellence in Student Journalism.   Misa’s Fugue won five Telly awards in 2013,  . Current in Fishers.   including an award for Art Direction and Historical/Biography and one for Education and Production.  The film also won the Silver Telly in the People’s Telly category.

In 2014, the film has been nominated for two Mid-Atlantic Emmy Awards,   but did not win. 

== References ==
 

== External Links ==
* 
* 
* 

 
 
 
 
 