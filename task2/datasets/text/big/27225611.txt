The Comedy Man
{{Infobox film
| name           = The Comedy Man
| image          = "The_Comedy_Man"_(1964).jpg
| image_size     = 
| caption        = UK campaign book cover
| director       = Alvin Rakoff
| producer       = David Henley Jon Penington
| writer         = Peter Yeldham
| based on       = novel by Douglas Hayes
| narrator       = 
| starring       = Kenneth More
| music          = Bill McGuffie
| cinematography = Ken Hodges
| editing        =  Ernest Hosler
| studio         =  Consant Films Gray-Film
| distributor    = British Lion Film Corporation  (UK)
| released       = 3 September 1964	(London)  (UK)
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Comedy Man is a 1964 British drama film directed by Alvin Rakoff and starring Kenneth More, Cecil Parker, Dennis Price and Billie Whitelaw. It depicts the life of a struggling actor in Swinging London.  
 Lord of the Flies (1963).

==Plot== sold out, Chick leaves London to return to rep.

==Cast==
* Kenneth More - Chick Byrd
* Cecil Parker - Thomas Rutherford
* Dennis Price - Tommy Morris
* Billie Whitelaw - Judy
* Norman Rossington - Theodore Littleton
* Angela Douglas - Fay Trubshaw
* Edmund Purdom - Julian Baxter
* Frank Finlay - Prout
* Alan Dobie - Jack Lavery
* J.G. Devlin - Sloppitt
* Valerie Croft - Yvonne
* Leila Croft - Pauline
* Gerald Campion - Gerry
* Jacqueline Hill - Sandy Lavery Harold Goodwin - Assistant director
* Penny Morrell - Actress
* Naomi Chance - Bit part
* Guy Deghy - Schuyster
* Derek Francis - Merryweather
* Myrtle Reed - Tommys secretary
* Edwin Richfield - Commercial director
* Gordon Rollings - Skippy
* Eileen Way - Landlady
* Freddie Mills - Indian Chief
* Frank Thornton - Producer John Horsley - Co-pilot
* Wally Patch - Bar manager
* Talitha Pol - Actress at Party
* Hamilton Dyce - Burial minister
* Anthony Blackshaw - Bus conductor Richard Pearson - Advertising Man
* Maurice Durant - Barman
* Ronald Lacey - Assistant Director
* Chubby Checker - himself
* Jill Adams - Jan Kennedy
* Robert Raglan - Minor role

==Critical reception==
Radio Times wrote, "written by Peter Yeldham with a nice balance between irony and drama, and directed by Alvin Rakoff with an accurate eye for the dingy environments and brave bonhomie of unemployed actors, this modest British film boasts a superior cast" ;   while Allmovie wrote, "matching Mores terrific starring performance are such British "regulars" as Dennis Price, Billie Whitelaw, Cecil Parker, Norm Rossington, and Frank Finlay" ;  and the Sunday Mirror noted, "Kenneth More in the greatest performance of his career. Brilliantly directed."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 