Los signos del zodiaco
 
{{Infobox film
| name           = Los signos del zodiaco
| image          = 
| caption        = 
| director       = Sergio Véjar
| producer       = Gabriel Torres Garcés
| writer         = Emilio Carballido Sergio Magaña Hidalgo
| starring       = Kitty de Hoyos
| music          = 
| cinematography = Agustín Jiménez
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 100 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

Los signos del zodiaco is a 1963 Mexican drama film directed by Sergio Véjar. It was entered into the 3rd Moscow International Film Festival.   

==Cast==
* Kitty de Hoyos as María
* Angélica María as Sofía
* Pilar Souza as Ana Romana
* Luis Bayardo as Pedro Rojo
* Mario García González as Daniel
* Enrique Aguilar as Augusto Sotomayor
* María Eugenia Ríos as Estela
* Marta Zamora as Polita
* Angeles Marrufo
* Yolanda Guillaumin (as Yolanda Guillomán)
* Socorro Avelar as Justina

==References==
 

==External links==
*  

 
 
 
 
 
 
 