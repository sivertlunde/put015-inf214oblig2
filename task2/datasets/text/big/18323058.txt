Up, Down, Fragile
{{Infobox film name           = Up, Down, Fragile image          = caption        = director       = Jacques Rivette producer       = writer         = Scenario: Laurence Côte Marianne Denicourt Nathalie Richard Pascal Bonitzer Christine Laurent Jacques Rivette  Dialogue: Pascal Bonitzer Christine Laurent starring       = Marianne Denicourt Nathalie Richard Laurence Côte André Marcon Bruno Todeschini music          = François Bréant Peter Lorne Stéphane Vilar cinematography = Christophe Pollock editing        = Nicole Lubtchansky distributor    = Pan Européenne Distribution released       =   runtime        = 169 minutes country        = France language       = French budget         =
}}

Up, Down, Fragile ( ) is a 1995 French film directed by Jacques Rivette.    It was entered into the 19th Moscow International Film Festival.   

==Cast==
* Marianne Denicourt - Louise
* Nathalie Richard - Ninon
* Laurence Côte - Ida
* André Marcon - Roland
* Bruno Todeschini - Lucien
* Wilfred Benaïche - Alfredo
* Marcel Bozonnet - Man in the stairway
* Philippe Dormoy - Le complice de Ninon
* Enzo Enzo - La chanteuse
* Pierre Lacan - Recordshop seller
* Stéphanie Schwartzbrod - Lise
* Christine Vézinet - Estelle
* Anna Karina - Sarah (avec)
* László Szabó (actor)|László Szabó - Le père de Louise (voice) (as Laslo Szabo)
* Alain Rigout - La victime de Ninon

==References==
 

== External links ==
*  

 

 
 
 
 
 
 


 
 