The Salzburg Connection
 
{{Infobox film
| name           = The Salzburg Connection
| image          = The Salzburg Connection.jpg
| caption        = Video artwork
| director       = Lee H. Katzin
| producer       = Ingo Preminger
| writer         = Edward Anhalt Helen MacInnes Oscar Millard
| starring       = Barry Newman Anna Karina
| music          = 
| cinematography = Wolfgang Treu
| editing        = John Woodcock
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $1,955,000 
}}

The Salzburg Connection is a 1972 American thriller film directed by Lee H. Katzin, starring Barry Newman and Anna Karina.    It is based on a novel by Helen MacInnes.

==Plot==
After a chest is brought up from the bottom of a lake, the diver, Richard Bryant, is found dead. Bill Mathison, an American photographer apparently on vacation in Austria, meets the dead mans widow, Anna, as well as an American woman, Elissa Lang, and together they end up fighting for their lives as well as the possession of long-lost, incriminating documents linking certain individuals to the Nazi Party.

==Cast==
* Barry Newman - Bill Mathison
* Anna Karina - Anna Bryant
* Klaus Maria Brandauer - Johann Kronsteiner
* Karen Jensen - Elissa Lang
* Joe Maross - Chuck
* Wolfgang Preiss - Felix Zauner
* Helmut Schmid - Grell
* Udo Kier - Anton
* Mischa Hausserman - Lev Benedescu
* Whit Bissell - Newhart
* Raoul Retzer - Large Man
* Elisabeth Felchner - Trudi Seidl
* Bert Fortell - Rugged Man
* Adolf Beinl - Antons Companion
* Patrick Jordan - Richard Bryant

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 