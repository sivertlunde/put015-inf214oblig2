Bachelor Apartment
{{Infobox film
| name           = Bachelor Apartment
| image          = Bachelor-Apartment-1931.jpg
| caption        = Theatrical film poster
| director       = Lowell Sherman
| producer       = William LeBaron
| screenplay     = J. Walter Ruben
| story          = John Howard Lawson
| starring       = Lowell Sherman Irene Dunne
| music          = Max Steiner   
| cinematography = Leo Tover
| editing        = Marie Halvey
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   }}| runtime        = 72-77 minutes 
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 RKO comedy romance film directed by and starring Lowell Sherman as a womanizing playboy who falls in love with Irene Dunnes honest working girl.  The credits for the film, and all sources from that time show that the film was based on a story by New York playwright John Howard Lawson, the screenplay was adapted by J. Walter Ruben.  However, Lawson would later claim that the final screenplay had not been altered from what he had originally written.  The cast features Mae Murray (a former silent film star, who was attempting to make a come-back in sound film|talkies), Norman Kerry and Ivan Lebedeff. 

==Plot==
Wayne Carter (Lowell Sherman) is a New York playboy, who pays no attention to the marital status of his many dalliances. However, there are some women whos attention he attempts to avoid, one such being the married Agatha Carraway (Mae Murray).

Helene and Lita Andrews (Irene Dunne and Claudia Dell, respectively) are small town girls who have come to the big city in order to find fame and fortune.  Helene is much more sensible than her younger sister, Lita, who is a bit flighty.  Eventually, Lita believes she has a millionaire interested in her, Carter. When she goes to have dinner at his apartment, an alarmed Helene goes to track her down to prevent anything untoward from occurring.  However, upon her arrival, she discovers that Lita has really attracted the attention of Carters butler, Rollins (Charles Coleman), with whom she is having dinner.

Carter is entranced with the sensible, earnest Helene.  Discovering she is in need of employment, he offers her a job in his office as a executive secretary.  She at first refuses, cautious about his intentions, but in need of work, she eventually relents and accepts the position.  Their mutual attraction grows, and Carter is seemingly beginning to give up his libidinous liaisons, until one afternoon when Carter asks her over to his apartment, not on a personal level, but to take some dictation.  Again leery, she agrees and meets him at his apartment, and all is going well until the flirtatious Agatha shows up at the apartment.  When her husband (Purnell Pratt) shows up shortly after, and Agatha hides in the bedroom while the two men have a discussion about marital issues, Helene once again becomes disenchanted with Carter, and resigns her position.

Realizing that he is truly in love with Helene, Carter is relentless in attempting to convince her of his sincerity, and of his deep feelings for her.  Eventually, she comes to believe him, and agrees to meet him at his apartment.  Unfortunately, Agatha is also relentless, and shows up once again.  This time, when her husband shows up slightly later, he is armed and threatens Carter, since he knows his wife is hiding in the bedroom.  To save Carter, Helene, who was with Agatha in the bedroom, exits, and swears that she is the only woman in the apartment.  Mollified, Carraway leaves.  After Agatha also departs, Carter is relieved and thinks everything is all right, but Helene is upset over the entire episode, and leaves deeply upset.

Carter is distraught, thinking he has lost the woman he loves.  Helene rebuffs all of his attempts to win her back.  Nothing works, until Lita runs off to live in sin with a musical producer, Lee Graham (Norman Kerry).  Carter had introduced the two, in an attempt to further endear himself to Helene, since he found out that Lita dreamed of being a stage performer.  Helene is beside herself with worry, since she has no idea on how to find Lita and Graham.  She turns to Carter, who tracks them down, and reunites the two sisters. Helene finally understands that Carter is being sincere, and accepts his proposal of marriage.

==Cast==
  
*Lowell Sherman as Wayne Carter
*Irene Dunne as Helene Andrews
*Mae Murray as Mrs. Agatha Carraway
*Ivan Lebedeff as Henri De Maneau  
*Norman Kerry as Lee Graham
 
*Noel Francis as Janet
*Claudia Dell as Lita Andrews
*Purnell Pratt as Henry Carraway 
*Kitty Kelly as Miss Clark Charles Coleman as Rollins
 

==Production==
Bachelor Apartment was the fifth film directed by Lowell Sherman, who directed his first film, a comedy short called Phipps, in 1929.  The combination of actor and director was highly unusual at the time.  

  for adaptation and dialogue, Lawson claimed that the script was filmed as he wrote it.  Lawson later went on to write Theory and Technique of Playwriting and Screenwriting, to become the head of the Screen Writers Guild, to write films such as 1938s Algiers (film)|Algiers and Sahara (1943 film)|Sahara (1943), and to be blacklisted as one of the Hollywood Ten. 

==Reception==
The film received mostly favorable reviews, although several were lukewarm.  Photoplay called the film a "... sophisticated story interesting from start to finish",  while Motion Picture Magazine said the movie was "sophisticated" and "entertaining", praising both the direction and acting.  Silver Screen, however, stating that only Lowells performance saved the film from "utter triteness".   Mordaunt Hall, of The New York Times, gave the film a somewhat positive review, praising both Lowell and Coleman, as well as several other players, but merely calling Dunnes performance "competent".  Overall, he stated, "For the most part it is a sophisticated comedy with a Parisian flair. It is equipped with elaborate modernistic settings and effectively photographed. Highly improbable though most of the action is, there is no gainsaying that it accomplished its purpose in arousing waves of merriment from an audience at the first showing."   

==Notes==
Bachelor Apartment was Irene Dunnes third film, after the now-lost   
 High Stakes in 1931, as her attempt was unsuccessful. 

==References==
Notes
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 