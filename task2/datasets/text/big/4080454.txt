The Silver Streak
 
{{Infobox Film
|  name=The Silver Streak
|  image=Possilstk.jpg
|  caption=Original film poster Tommy Atkins
|  producer=Glendon Alvine
|  writer=H.W. Hanemann Jack ODonnell Roger Whately Arthur Lake
|  country  =    English
|  editing=Fred Kundtson
|  released=November 30, 1934|
  }}
 1934 film loosely based on the record-setting "dawn-to-dusk" run of the Pioneer Zephyr on May 26, 1934.  The original Zephyr trainset was used for the exterior shots in the film, while interior scenes were filmed on a sound stage in Hollywood, California|Hollywood.  For the film, the "Burlington Route" nameplate on the trains nose was replaced with one that read "Silver Streak".

One of the movies promotional items was a small book called "The Story of the Silver Streak" which was illustrated with black and white stills from the movie.  
 model of the Pioneer Zephyr.  A limited number (350 HO scale and 250 N scale) of these bore the nameplates of the Silver Streak from the motion picture instead of the standard Burlington Route nameplates.

==Plot== railroad passenger travel, engineer Tom Caldwell presents to the president of the CB&D Railroad,  B.J. Dexter, a design for a revolutionary diesel-electric transmission|diesel-electric train that will increase efficiency and lower costs. Dexter opposes change, however, and the railroads conservative board of directors agrees with him, rejecting Toms design. Tom quits in frustration. Sure that Toms theory is sound, Dexters daughter Ruth convinces Ed Tyler, a locomotive manufacturer, to look into Toms design. Tyler is impressed with the concept and initiates immediate construction of a prototype. Soon Tom and his team prepare the Silver Streak for a well-publicized trial run with Dexter and Ruth aboard as passengers.
 freight train. Century of Boulder Dam. 
 infantile paralysis iron lung respirator, Ruth telephones her father to have the machine shipped to the dam by airplane. Dexter is told that the iron lung is too heavy for any transport airplane to carry and cannot be disassembled. Tom and Tyler persuade Dexter to take a gamble on the Silver Streak as Allens only hope. 
 Drinker Respirators Boulder City, however, Bronte is revealed as a fugitive and sabotages the engine trying to stop the train, but instead causes it to speed out of control. Tom knocks the spy unconscious and regains control of the runaway train just before it arrives at the station.

==Cast==
  for The Silver Streak]]
*Sally Blane as Ruth Dexter
*Charles Starrett as Tom Caldwell
*William Farnum as B.J. Dexter
*Hardie Albright as Allen Dexter
*Irving Pichel as Bronte Arthur Lake as Crawford
*Theodore von Eltz as Ed Tyler
*Guinn "Big Boy" Williams as Higgins
*Edgar Kennedy as OBrien
*Doris Dawson as Molly

==Production==
The Pioneer Zephyr was chosen for the film after the California-based Union Pacific Railroad declined to provide its high-speed passenger train, the M-10000. The plot element of the infantile paralysis epidemic took advantage of public fears of the disease rampant in 1934 when more than 1,000 cases were diagnosed in Los Angeles alone. Location filming of the Zephyr was done over a two-day period in September 1934 at the CB&Q yards in Galesburg, Illinois. The train was renamed The Silver Streak for the film, which had been a discarded choice of CB&Q president Ralph Budd, and the local high school adopted it as the nickname for its athletic teams. 

==Reception==
The film made a profit of $107,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p78 

==References==
 
 
 

==External links==
*  
*  
*   

 
 
 
 
 
 
 
 
 
 