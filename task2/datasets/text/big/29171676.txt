No Strings Attached (film)
{{Infobox film
| name           = No Strings Attached
| image          = No Strings Attached Poster.jpg
| alt            = the characters, getting dressed in a bedroom and smiling at each other.
| caption        = Theatrical release poster
| director       = Ivan Reitman
| producer       = {{Plainlist|
* Jeffrey Clifford
* Joe Medjuck
* Ivan Reitman
}}
| screenplay     = Elizabeth Meriwether
| story          = {{Plainlist|
* Elizabeth Meriwether
* Mike Samonek
}}
| starring       = {{Plainlist|
* Natalie Portman
* Ashton Kutcher
* Cary Elwes
* Kevin Kline
}}
| music          = John Debney
| cinematography = Rogier Stoffers
| editing        = Dana E. Glauberman
| studio         = {{Plainlist|
* Cold Spring Pictures
* Spyglass Entertainment
* The Montecito Picture Company
}}
| distributor    = Paramount Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $149,228,077 
}}
No Strings Attached is a 2011 American romantic comedy film directed by Ivan Reitman and starring Natalie Portman and Ashton Kutcher. Written by Elizabeth Meriwether, the film is about two friends who decide to make a pact to have "no strings attached" casual sex without falling in love with each other. The film was released in the United States and Canada on January 21, 2011.

== Plot ==
  
After first meeting at a summer camp as teenagers, Emma (Natalie Portman) and Adam (Ashton Kutcher) run into each other a few times as young adults but never keep in touch. Emma becomes a doctor in Los Angeles, Adam a production assistant for a musical television show. Adams father Scott (Kevin Kline), the well-known star of a former hit television comedy series, has begun a romance with Adams ex-girlfriend, Vanessa (Ophelia Lovibond). Adam finds out, gets drunk and calls the women in his phone seeking a hookup. The next morning, he wakes on a sofa wearing nothing but a small towel. It turns out that he texted Emma and then came to the apartment she shares with some other residents — Patrice (Greta Gerwig), Shira (Mindy Kaling), and Guy (Guy Branum). Once there, he took off all his clothes and then passed out. Emma leads Adam to her bedroom to retrieve his missing pants and they end up having sex.

The two have sex again at Adam’s house and before she leaves Adam agrees to her proposal for a casual relationship (as she puts it, using each other for sex and nothing else). Adam warns Emma about falling in love with him, but she dismisses the idea and sets ground rules to keep what they’re doing from becoming too serious. At first things go well, but Adam becomes jealous when Sam (Ben Lawson) — another resident — seeks her attention. Adam brings Emma a gift (a mix CD) and she rebuffs him, saying they should stop for a while and hook up with other people. But after being apart for two weeks Emma returns to Adam and they continue being sex friends only.

Adams birthday comes along a few months later. He goes out for dinner with Scott and Vanessa, who announce their plan to have a baby together. Emma berates the other couple while defending Adam. He persuades her to go out together on Valentines Day. Things fall apart when she becomes too uncomfortable during the date. An angry Emma advises Adam that he should find someone else who won’t hurt him. Adam tells Emma that he loves her — something shes not at all receptive to hearing — they have a fight, ending their arrangement.

Six weeks later, a script Adam wrote is being filmed. He gets a regular writing job on the show with the help of Lucy (Lake Bell), the shows assistant  director (Jennifer Irwin), who is clearly attracted to Adam. Meanwhile, Emma is depressed over not being with Adam. The situation is compounded and complicated by her younger sister Katies (Olivia Thirlby) wedding the next day and her widowed mother (Talia Balsam) arriving for the event with a male companion (Brian Dierker) of her own. Emma feels she is being strong for her mom by not letting herself get too close to anyone so she wont become upset by seeing Emma get hurt if a relationship ends poorly. Emmas mom tells her to stop.
 Santa Barbara where the wedding is taking place and drives to Adams house. Her plans are ruined – and she has to hide to avoid being seen — when he arrives home with Lucy. Emma assumes Adam has a new girlfriend and tearfully drives away. Vanessa calls Adam before he and Lucy can have sex — Scott has overdosed on a cough syrup-based drink called "Purple drank". Meeting Adam outside the hospital, Vanessa says that she is ending her relationship with Scott and leaves for a party. Adam goes in to visit Scott who surprisingly gives him some tender advice about falling in love.

Shira tells Emma about Adams dad being admitted to the hospital. As Adam leaves the building he calls Emma and tells her that she must be present if she is going to say that she misses him. Emma gets out of her car as the call ends and Adam is stunned to suddenly find her there. She tells Adam that she is sorry she hurt him and confesses that she really loves and cares about him and they reconcile. After eating breakfast together the next morning — something that never happened before — they arrive in Santa Barbara just before Katies wedding is starting. As they enter a room and pause Emma asks Adam what will happen next, and with a smile on his lips he silently intertwines her hand with his — for the first time they are holding hands together as a couple.

The end credits show an epilogue in which; Scott and Lucy are in a relationship and in a restaurant waiting for Adam to arrive to tell him.  Adam’s roommate (Jake Johnson) and Patrice are in a relationship and are shown meeting his two dads. Vanessa had told Adam that old people scare her and she is ridden with anxiety when she is trapped in an elevator full of senior citizens. Adam and Emma are seen kissing in the park. Sam and Shira are in a relationship, but he wants his freedom and isnt pleased when she reveals that she’s already been seeing other men. Katie is having a baby and Emma is the delivering doctor. Guy lures a nervous-looking Sam into a hospital room. At last, Adam and Emma are shown peacefully sleeping together.

==Cast==
 
 
* Natalie Portman as Dr. Emma K. Kurtzman
* Ashton Kutcher as Adam Franklin
* Kevin Kline as Scott Franklin, Adams father
* Cary Elwes as Dr. Metzner, Emmas boss
* Greta Gerwig as Patrice, Emmas friend
* Lake Bell as Lucy
* Olivia Thirlby as Katie Kurtzman, Emmas younger sister
* Ludacris as Wallace, Adams friend
* Jake Johnson as Eli, Adams friend
* Mindy Kaling as Shira, Emmas friend
* Talia Balsam as Sandra Kurtzman, Emma and Katies mother
* Ophelia Lovibond as Vanessa, Scotts girlfriend
 
* Guy Branum as Guy, Emmas friend
* Ben Lawson as Sam, a co-worker of Emma, that likes her
* Jennifer Irwin as Megan, Adams boss
* Adhir Kalyan as Kevin, Katies fiancé
* Brian H. Dierker as Bones, Sandras boyfriend 
* Abby Elliott as Joy
* Vedette Lim as Lisa
* Nasim Pedrad as Writer 
* Mollee Gray as Sari
* Matthew Moy as Chuck
* Dylan Hayes as Young Adam 
* Stefanie Scott as Young Emma 
 

==Production== different film with a similar premise that opened on July 22, 2011. The Paramount Pictures film was first announced in March 2010 as an untitled project. Actors Ashton Kutcher and Natalie Portman were cast in the lead roles, and Paramount anticipated a release date of  , 2011.  Reitman said of casual sex, "I noticed from my own kids that with this generation in particular, young people find it easier to have a sexual relationship than an emotional one. That is how the sexes deal with each other today."    Principal photography began in May 2010.  By November 2010, the film was titled No Strings Attached with a new release date of  , 2011. 
 Black Swan. 

==Release==

===Theatrical run=== Fox Village The Green Hornet, which opened the previous weekend in first place with  .   

 
Ultimately, No Strings Attached beat The Green Hornet with an opening weekend gross of  .  70% of the audience were women.  According to CinemaScore, audiences under the age of 25 gave the film an "A-" grade while audiences over the age of 25 gave it a "B" grade. Future grosses were expected to be dependent on the younger demographic. 

The film has grossed   in the United States and Canada and   in other territories for a worldwide total of  . 

===Home media===
No Strings Attached was released on DVD and Blu-ray Disc on May 10, 2011. 

 

== Reception ==
No Strings Attached received mixed reviews. Review aggregator  , the film received an score of 50 out of 100, based on 36 reviews, indicating "mixed or average reviews". 
 When in Leap Year"; according to Scott, the film is "Love & Other Drugs without the disease", a film whose pleasures "are to be found in the brisk, easy humor of some of Elizabeth Meriwether|Ms. Meriwethers dialogue and in the talented people scattered around Ms. Portman and Mr. Kutcher like fresh herbs strewn on a serving of overcooked fish."     Scott considered "the films great squandered opportunity&mdash;and also the source of some of its best comic moments&mdash;is that Greta Gerwig|Ms. Gerwig and Mindy Kaling in effect share the role of Emma’s zany sidekick.  How can this be? Why are these two entirely original and of-the-moment performers marginal players in this agreeable, lackluster picture and not stars of the year’s greatest girl-bromance?... To imagine Ms. Kaling and Ms. Gerwig in a remake of Thelma and Louise or the Wedding Crashers is to experience an equal measure of frustration and hope. Why can’t we have a few movies like that and not quite so many like this?" 
 The Telegraph Black Swan." 

==Music   ==
 

The soundtrack includes songs such as
"Bossa Nova Baby" (from 1963),
"I Wanna Sex You Up" (from 1991), Bang Bang Bang", Love Lost",
a cover version of "99 Problems". and "Bleeding love"

== See also == Friends with Benefits When Harry Met Sally
*Platonic love

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 