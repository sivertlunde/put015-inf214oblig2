Schoolgirl Diary
{{Infobox film
| name           = Schoolgirl Diary
| image          = 
| caption        = 
| director       = Mario Mattoli
| producer       = Giulio Manenti
| writer         = Aldo De Benedetti Marcello Marchesi Mario Mattoli Laura Pedrosi
| starring       = Alida Valli
| music          = 
| cinematography = Jan Stallich
| editing        = Fernando Tropea
| distributor    = 
| released       = 6 September 1941
| runtime        = 95 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Schoolgirl Diary ( ) is a 1941 Italian drama film directed by Mario Mattoli and starring Alida Valli.    

==Cast==
* Alida Valli - Anna Campolmi
* Irasema Dilián - Maria Rovani (as Irasema Dilian)
* Andrea Checchi - Il professore Marini
* Giuditta Rissone - La direttrice del collegio
* Ada Dondini - La signorina Elgsorina Mattei
* Carlo Campanini - Campanelli, il bidello
* Olga Solbelli - La signorina Bottelli
* Sandro Ruffini - Carlo Palmieri, padre di Maria
* Bianca Della Corte - Luisa
* Dedi Montano - Linsegnate di musica

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 