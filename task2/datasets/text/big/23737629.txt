Magician Mickey
{{Infobox Hollywood cartoon
| cartoon_name = Magician Mickey
| series = Mickey Mouse
| image = Magician Mickey poster.jpg
| caption = Original theatrical poster David Hand
| story_artist =
| animator = Ed Love
| voice_actor = Walt Disney Clarence Nash Pinto Colvig
| musician = Albert Hay Malotte
| producer = Walt Disney
| studio = Walt Disney Productions
| distributor = United Artists
| release_date = February 6, 1937
| color_process = Technicolor
| runtime = 8 min (one reel)
| preceded_by = Don Donald (1937)
| followed_by = Moose Hunters (1937) English
}}
Magician Mickey is a 1937 Walt Disney Mickey Mouse cartoon, originally released to theaters on February 6, 1937. Mickey puts on a magic show, but is constantly interrupted by a heckler, Donald Duck, whom he in turn tries to get his revenge on through his magic tricks.

==Releases==
* 1937 &ndash; theatrical release
* 1957 &ndash; Walt Disney anthology television series|Disneyland, episode #3.15: "All About Magic" (TV)
* c. 1983 &ndash; Good Morning, Mickey!, episode #38 (TV)
* 1984 &ndash; "Cartoon Classics: Mickeys Crazy Careers" (VHS)
* c. 1992 &ndash; Mickeys Mouse Tracks, episode #18 (TV)
* c. 1992 &ndash; Donalds Quack Attack, episode #28 (TV)
* 2001 &ndash; " " (DVD)
* 2006 &ndash; " " (DVD)
* 2011 &ndash; Have a Laugh!, episode #24 (TV)
* N/A &ndash; "Have a Laugh!: Volume 5" (DVD)

==Trivia==
This cartoon sometimes deletes the scene where Goofy gets electrocuted when he sticks his finger in a live light socket.

==External links==
*  
 

 
 
 
 
 
 


 