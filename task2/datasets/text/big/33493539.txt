Wenn Ludwig ins Manöver zieht
{{Infobox film
| name           = Wenn Ludwig ins Manöver zieht
| image          = 
| caption        =  Georg Laforet   Ludwig Thoma  
| based on       =  Hansi Kraus as Ludwig Thoma Heidelinde Weis Rudolf Rhomberg Elisabeth Flickenschildt
| director       = Werner Jacobs  Franz Seitz
| cinematography = Wolf Wirth
| music          = Rolf Wilhelm Jane Sperr
| studio         = 
| distributor    = Constantin Film
| released       = 19 December 1967
| runtime        = 91
| country        = Germany German
| budget         = 
| gross          = 
}}
 West German Hansi Kraus goalkeeper Sepp Maier.

==Plot==
The Prussian Army comes to Bavaria for a military exercise. They show off and try to demonstrate superiority. Yet they dont stand a chance because they are not prepared to tackle Ludwigs nifty pranks.

==Cast== Hansi Kraus as Ludwig Thoma
* Heidelinde Weis: Cora Reiser
* Elisabeth Flickenschildt: Aunt Frieda
* Rudolf Rhomberg: Pfarrer „Kindlein“ Falkenberg / Feldwebel Falkenberg 
* Hubert von Meyerinck: Oberst von Below
* Chantal Goya: Melanie
* Hans Terofal: Bader Gschwind
* Georg Thomalla: Hauptmann Stumpf
* Karl Schönböck: Corpsgeneral
* Hans Quest: Rittmeister von Stülphagel
* Dieter Borsche: Wilhelm II
* Beppo Brem: Rafenauer 
* Friedrich von Thun: Franz Reiser
* Claus Wilcke: Oberleutnant von Busch
* Evelyn Gressmann: princess
* Veronika Fitz: waitress Fanny
* Sepp Rist: Oberst von der Tann
* Zlatko Čajkovski: companys cook Čajkovski
* Sepp Maier: Sepp Maier
* Gerd Müller: Müller

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 