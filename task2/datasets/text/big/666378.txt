Donald's Nephews
 
{{Infobox Hollywood cartoon cartoon name=Donalds Nephews
|series=Donald Duck
|image= image size=
|alt=
|caption= Jack King
|producer=Walt Disney story artist=Carl Barks, Jack Hannah voice actor=Clarence Nash
|animator= Walt Disney Productions RKO Radio Pictures release date=  (USA) color process=Technicolor
|runtime=8 minutes
|country=United States
|language=English followed by=Good Scouts
}}

Donalds Nephews (1938 in film|1938) is a Donald Duck animated cartoon which features Donald being visited by his three nephews, Huey, Dewey, and Louie. This cartoon is Huey, Dewey, and Louies very first appearance in animation.     The short, and the three nephews, was the idea of Al Taliaferro, the artist for the Silly Symphonies comic strip, which featured Donald Duck. The Walt Disney Productions Story Dept. on February 5, 1937, sent Taliaferro a memo recognizing him as the source of the idea for the planned short.  Taliaferro subsequently introduced the nephews in his comic strip, which by this time had been renamed Donald Duck, on Sunday, October 17, 1937, beating the theatrical release of Donalds Nephews by almost six months. 

==Plot==
Donald receives a postcard from his sister, Duck family (Disney)#Della Duck|Dumbella, which says that her three "angelic" boys, Huey, Dewey, and Louie, are coming to visit him. At first Donald is excited to see his nephews, but soon the boys start causing problems, being a constant annoyance to their uncle.

Donald consults a book entitled Modern Child Training, and tries to use the suggestions to gain control over his nephews, but things only get worse. In the end, after the boys leave and with his house left almost destroyed, Donald spots a page which reads "After all, little children are only angels without wings". This so enrages Donald that he rips the book to pieces and enters an explosive rage.

===Nephews Antics===
Throughout the cartoon, Huey, Dewey, and Louie play tricks on their Uncle Donald in order to annoy him. They cause extensive damage throughout his house, and have fun at Donalds expense. Some of their activities include:
* Playing croquet on tricycles in Donalds living room, breaking vases, lamps, and windows bow at Donald
* While Donald plays the piano, Huey fills a bag with water, and Louie bursts it with his slingshot
* Giving Donald a pie full of scorching hot mustard, then, to cool him down, they throw water on him and spray him in the face with a fire extinguisher, then throw his book at him and ride away on their tricycles

==Availability==
* Walt Disneys Funny Factory With Huey Dewey & Louie Vol. 4   

==References==
 

==External links==
*  
*  
 
 
 
 
 
 
 

 