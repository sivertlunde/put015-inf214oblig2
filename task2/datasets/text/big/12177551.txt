When Do We Eat? (2005 film)
 
{{Infobox film |
  name     = When Do We Eat? |
  image          = WhenDoWeEatMoviePoster.jpg|
  writer         = Nina Davidovich Salvador Litvak  | Michael Lerner Ben Feldman Adam Lamberg|
  director       = Salvador Litvak |
  producer       = Horatio C. Kemeny |
  distributor    = When Do We Eat? Inc. |
  released   =   |
  runtime        = 93 minutes |
  language = English language|English, Hebrew |
  gross    =  $431,513  
}}

When Do We Eat? is a 2005 American comedy film.  

==Plot== Ben Feldman) Michael Lerner) ecstasy and LSD.  As the night continues, the family releases secrets that cause fights but bring them closer in the end.

==Characters==
*Ira - The father of the family.  Ira manufactures and distributes Christmas ornaments for a living.
*Peggy - A stay-at-home mother.  Raises money for autism awareness.
*Artur - Iras father and a survivor of the Holocaust.  Artur carries a suitcase wherever he goes in case the Nazis come and he has to run.
*Zeke - A high school student who enjoys recreational drugs.
*Ethan - After failing in business, Ethan becomes more involved in religion, becoming a Hasidic Jew.
*Vanessa - A first cousin to the Stuckmans.  Vanessa is a celebrity publicist.
*Jennifer - Iras daughter from a previous marriage.  Types closed-captioning for a living. Protestant girlfriend who is attending her first Seder.
*Nikki - Nikki is a professional sex surrogate who assists men in getting over their fears of intimacy.
*Lionel - An idiot savant, Lionel has  a fascination of the number 7.
*Rafi - An Israeli and a professional tent builder invited to the seder.

==Cast==  Michael Lerner as Ira
*Lesley Ann Warren as Peggy
*Jack Klugman as Artur
*Meredith Scott Lynn as Jennifer
*Shiri Appleby as Nikki
*Mili Avital as Vanessa Ben Feldman as Zeke
*Adam Lamberg as Lionel
*Max Greenfield as Ethan
*Cynda Williams as Grace
*Mark Ivanir as Rafi
*Victoria Justice as Young Nikki
*Jeremy Glazer as Young Artur

== Awards and nominations == 
*deadCENTER Film Fesival in Oklahoma City: Winner - Best Narrative Feature {{cite news
 |url=http://whendoweeat.com/news.html
 |title=When Do We Eat? - Official Site}} 
*Napa/Sonoma Wine Country Film Festival: Winner - Best Comedy
* Tahoe/Reno International Film Festival: Best Pick and Winner - Best Director
*San Diego Film Festival: Winner - Best Screenplay

==References==
 

==External links==
*  

 
 
 
 
 
 