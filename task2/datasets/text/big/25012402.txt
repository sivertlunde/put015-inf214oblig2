Jack of Diamonds (1967 film)
{{Infobox Film
| name           = Jack of Diamonds
| image_size     = 
| image	         = Jack of Diamonds FilmPoster.jpeg
| caption        = Original film poster by Robert McGinness Don Taylor
| writer         = 
| narrator       =  George Hamilton Maurice Evans   Carroll Baker   Zsa Zsa Gabor   Lilli Palmer   Wolfgang Preiss|
| music          = 
| cinematography = 
| editing        =  studio = Sandy Howard Productions Maran Films
| distributor    = Metro-Goldwyn-Mayer
| released       = November 10, 1967
| runtime        = 108 min.
| country        = United States West Germany
| language       = English
| budget         = $1.3 million 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Don Taylor George Hamilton in the lead role of an international cat burglar and jewel thief.

==Production==
The film was originally meant to be directed by Herschel Daugherty. It filmed in New York for a month and then 12 weeks in Europe in Paris, the Bavarian Alps, Genoa and a Munich studio. Whos Afraid Of Togetherness?: More on Movie Matters
By A.H. WEILER. New York Times (1923-Current file)   02 Oct 1966: 127. 

Producer Sandy Howard described George Hamilton as "a hot commodity now" before filming, due in part to the fact he was dating the Presidents daughter.  Reports put his fee around this time at $100,000 a movie. HAMILTON TELLS OF BUSY CAREER: But Sought-After Actor Is Silent on Role as Suitor
By VINCENT CANBY. New York Times (1923-Current file)   07 Oct 1966: 38. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 