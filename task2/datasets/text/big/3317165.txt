The Joker Is Wild
 
{{Infobox film
| name           = The Joker Is Wild
| image          = jokerwild.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Charles Vidor
| producer       = Samuel J. Briskin
| screenplay     = Oscar Saul
| based on       =  
| starring       = {{Plainlist|
* Frank Sinatra
* Mitzi Gaynor
* Jeanne Crain
* Eddie Albert
}}
| music          = Walter Scharf
| cinematography = Daniel L. Fapp
| editing        = Everett Douglas
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3 million (USA) 
}}
The Joker Is Wild is a 1957 American musical drama film directed by Charles Vidor, starring Frank Sinatra, Mitzi Gaynor, Jeanne Crain, and Eddie Albert, and released by Paramount Pictures. The film is about Joe E. Lewis, the popular singer and comedian who was a major attraction in nightclubs from the 1920s to the early 1950s.

==Plot== Mob during Prohibition era. His eventual decision to work elsewhere results in his face being slashed and his throat cut, preventing him from continuing with his current act as a singer.

Lewis soon develops an acerbic and witty sense of humor and is given a break as a stand-up comedian from singer Sophie Tucker (playing herself). Soon, Lewis makes a career for himself, but heavy drinking and a self-destructive streak leads him to question his way of living and what his life has become.

==Cast==
* Frank Sinatra as Joe E. Lewis
* Mitzi Gaynor as Martha Stewart
* Jeanne Crain as Letty Page
* Eddie Albert as Austin Mack
* Beverly Garland as Cassie Mack
* Jackie Coogan as Swifty Morgan
* Barry Kelley as Captain Hugh McCarthy
* Ted de Corsia as Georgie Parker
* Leonard Graves as Tim Coogan
* Valerie Allen as Flora, Chorine
* Hank Henry as Burlesque Comedian

==Production==
Sinatra read   for the film rights to his story. Variety (magazine)|Variety reported in November 1955 that Paramount Pictures would finance what was, for all intents and purposes, an independent feature film which was headed by Lewis and Sinatra along with director Charles Vidor and author Art Cohn. Each of the four partners were paid a reported $400,000, along with 75% of the films net profits. The New York Times would report that Sinatras share was in the region of $125,000 along with 25% of the films profits.

The filming of the movie was mostly done later in the day, Sinatra preferring to work at that time and the filming schedule being tailored around this. Sinatra also insisted that all the musical scenes in the film and songs therein be recorded live on set to keep the performances more genuine. Frank Sinatra: "When I do a concert and someone coughs, I like that," Sinatra remarked. "I like the scraping of chairs. You get the feeling that its really happening. Ive always thought Lewis was one of only about four or five great artists in this century - one of them was Jolson - and I remember him screaming like the devil when he made a soundtrack." (from All the Way: A Biography of Frank Sinatra)

==Critical reception==
The Joker Is Wild opened to mostly favorable reviews.

Los Angeles Times reviewer Phillip K. Scheuer: "Sinatra "catches the bitter inner restlessness almost too well...When Lewis, highball in hand, is reciting them   his natural clowns grin takes the curse off their cynicism; from Sinatra the gags come out bitter and barbed."

Films and Filming reviewer Gordon Gow: "One consolation in the glossy gloom of this downbeat drama is that Frank Sinatra has sufficient talent and taste to break through the wall of embarrassment that is bound to arise between an audience and the film case-history of an unanonymous alcoholic."

Variety magazine|Variety commented on the "major job Sinatra does... alternately sympathetic and pathetic, funny and sad."
 All the Way" by Jimmy Van Heusen and Sammy Cahn. When the film was re-released some years later, the title was changed for a period to All the Way due to the immense popularity of the films theme song, which peaked at No. 2 on Billboard (magazine)|Billboard.

Sinatra actually became friends with the real Lewis, who commented that "You (Sinatra) had more fun playing my life than I had living it."

==References==
 

==External links==
*  
*  in Variety (magazine)|Variety

 

 
 
 
 
 
 
 
 
 
 
 
 
 