Twin Sisters (2002 film)
{{Infobox film
| name           = Twin Sisters
| image          = Film poster Twin Sisters.jpg
| caption        = Film poster
| director       = Ben Sombogaart
| producer       = 
| writer         = Tessa de Loo Marieke van der Pol
| narrator       = 
| starring       = Thekla Reuten, Nadja Uhl, Ellen Vogel, Gudrun Okras
| music          = Fons Merkies
| cinematography = 
| editing        = 
| distributor    = Miramax Films released        = May 6, 2005 (United States|US)
| runtime        = 
| country        = Netherlands German
| budget         = 
| gross          = $5,145,363  http://www.boxofficemojo.com/movies/?id=twinsisters.htm 
| preceded_by    = 
| followed_by    = 
}} Dutch film, The Twins by Tessa de Loo, with a screenplay by Dutch actress and writer Marieke van der Pol.

== Film plot ==
The film tells the story of twin German sisters Lotte (Thekla Reuten) and Anna (Nadja Uhl) who are separated when they are six. After the death of their parents, they are "divided" between quarreling distant relatives, one being raised in the Netherlands and the other in Germany.  Lotte grows up in a loving middle-class intellectual family in Amsterdam, and Anna is raised in virtual servitude by a poor Catholic peasant family in a backward area .
The two girls seek to keep in contact, but Annas family lacks Lotties address, and Lottes new family fails to mail her letters for fear that the brutal farmers will claim her as well.  The cataclysmic events of World War II ] sweep them even further apart. Lotte falls in love with a young Jewish man— who is eventually caught by the Nazis and sent to his death. Anna  falls in love and marries a young Wehrmacht soldier who joins the Waffen SS and is killed in the last days of the war. Although the girls find each other just before the outbreak of the war, Annas attempt to reunite with Lotte  in its aftermath is thwarted by Lottes bitter discovery that Annas husband had been part of the apparatus that murdered her fiancé. 

Only in their old age, when they meet again  at a spa, does Lotte reconcile herself to their divergent lives and reclaim the tender sibling feeling of her childhood.
 
The two girls/women are each played by three different actors, from the Netherlands and Germany respectively.

== Reception ==
In Israel, some critics objected to the film as "creating a moral equation between the killers and their victims". Still, it was shown successfully for several months in cinemas all over Israel. As the Jewish Chronicle was later to remark,
 "A thought provoking film, raises big questions about responsibility for the Holocaust and what ordinary individuals do when faced with extraordinary evil". 

Miramax Films had also acquired the United States distribution rights to Twin Sisters and the film was given a limited US theatrical release in 2005.
 76th Academy Awards nominee for Academy Award for Best Foreign Language Film of 2003.

== Box office ==
The film received commercial release on May 6, 2005 and grossed  $1,207 in the opening weekend in 1 theater. It went on to gross $1,563 domestically and $5,143,800 in the foreign markets for a worldwide total of $5,145,363. 

== References ==
 

==External links==
*  
* 
* 

 
 
{{succession box
| title=Golden Calf for Best long feature film
| years=2003 Black Book
| after=Simon (2004 film)|Simon
}}
 

 
 
 
 
 