Everyday Is Like Sunday (film)
{{Infobox film
| name           = Everyday Is Like Sunday
| image          = Everyday Is Like Sunday poster.jpg
| image_size     = 220
| caption        = Film poster
| director       = Pavan Moondi
| producer       = Brian Robertson Pavan Moondi
| writer         = Pavan Moondi Michael Sloane
| starring       =  
| original music = Kathryn Calder
| cinematography = Joseph Puglia
| editing        = Simone Smith 	
| released       = August 16, 2013
| runtime        = 90 minutes
| country        = Canada
| language       = English
}}

Everyday Is Like Sunday is a 2013 Canadian feature film directed by Pavan Moondi,  written by Pavan Moondi and Michael Sloane and produced by Brian Robertson and Pavan Moondi.

The film stars stand-up comedian David Dineen-Porter, Coral Osborne, Adam Gurfinkel, Nick Flanagan, Bo Martyn and musicians Nick Thorburn (of Islands (band)|Islands, Mister Heavenly and The Unicorns) and Dan Werb (of Woodhands). 

Everyday Is Like Sunday was released theatrically in  , in association with Daylight on Mars Pictures and Riddle Films. 

The independent film was acquired for Canadian distribution by Mongrel Media beginning January 2014.    

On August 1st, 2014, the film became available to stream instantly on Netflix Canada.    
 
==Reception==
Everyday Is Like Sunday premiered to critical acclaim. Manori Ravindran of The National Post remarked, "Millennial angst in gritty urban centres could warrant its own section in The New York Times. We’re poor, we’re jobless, we’re lonely, we get it. But there’s an honesty and whip smart humour to the micro-budget Everyday Is Like Sunday that separates it from similar fare, and Moondi and Sloane have expertly distilled the disillusioned charm shrouding Toronto youth into snappy dialogue. If only every film about this doomed generation could be so smart." 

==References==
 

==External links==
* 

 
 