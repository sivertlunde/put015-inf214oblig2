Paradise Express
{{Infobox film
| name           = Paradise Express
| image          = Dorothy Appleby-Grant Withers in Paradise Express.jpg
| image_size     =
| caption        = Dorothy Appleby and Grant Withers in the film
| director       = Joseph Kane
| producer       = Nat Levine (producer) Sol C. Siegel (associate producer)
| writer         = Allan Vaughan Elston (story) and Paul Perez (story) Betty Burbridge (screenplay) and Jack Natteford (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Jack A. Marta Edward Mann
| distributor    =
| released       = 22 February 1937
| runtime        = 60 minutes 53 minutes (American edited version)
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Paradise Express is a 1937 American film directed by Joseph Kane.

== Plot summary ==
The "Moon Valley Short line" Railroad is losing money to the "Armstrong Trucking Company". When the railroad goes into receivership, the railroad is forced to lay off several people. The president of the railroad, Jed Carson, has acquired a hatred for the new receiver, Lawrence Larry Doyle. His granddaughter, Kay Carson, also does not like Doyle. After getting himself acquainted with both Jed Carson and Kay Carson, Doyle goes and wins back some business. Kay starts take a liking to Doyle, but her grandfather still hates him. When the new customers freight is damaged, Doyle knows it is the Armstrong Trucking Company. After talking to Doyle it is revealed that the owner of the Armstrong Trucking corp, Mr. Armstrong, had Doyle appointed as the receiver, thinking it would benefit him. However, Doyle has no plans to help the Armstrong Trucking Company. Now that the railroad has won some business back, it must work on its speed, to attract more business. Doyle asks a former railroad employee to run a fast freight to beat the trucking companys schedule. When the train is mysteriously wrecked, the town blames Doyle. However, Jed Carson does research and finds that the wreck was not Doyles fault, and reveals it to the people of the town. Before the wreck occurred, the train beat the trucking companys schedule. When the trucking company challenges the railroad to a race for a contract, the railroad starts to win, but is sabotaged by the trucking company, which has been sabotaging the railroad all along. With no water in the water tank, the engine cannot run, but Doyle thinks up the idea to use the ice in the refrigerator cars. Cutting it close, the railroad wins, and Armstrong and his henchmen are convicted when one of the trucking companys employees writes a confession. The film ends with Kay embracing Doyle, for she has fallen for him.

== Cast ==
*Grant Withers as Lawrence Larry Doyle
*Dorothy Appleby as Kay Carson
*Arthur Hoyt as Phineas K. Trotter
*Maude Eburne as Maggie Casey Harry Davenport as Jed Carson
*Donald Kirke as Armstrong
*Arthur Loft as Glover
*Lew Kelly as Tom Wilson
*Anthony Pawley as Stymie
*Fern Emmett as Landlady John Holland as Gus
*Robert McClung as Harmonica Player Bruce Mitchell as Train Conductor
*Guy Wilkerson as Skinny Smith
*George Cleveland as Farmer Beasley
*Ralph McCullough as Dispatcher
*William L. Thorne as Farmer at meeting

== Soundtrack ==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 