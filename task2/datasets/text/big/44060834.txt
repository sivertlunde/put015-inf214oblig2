Iniyaval Urangatte
{{Infobox film
| name           = Iniyaval Urangatte
| image          =
| caption        =
| director       = KG George
| producer       = Gopi Menon
| writer         =KG George (dialogues)
| screenplay     = KG George
| starring       = Sukumaran
| music          = M. K. Arjunan
| cinematography = B. Kannan
| editing        = G. Venkitaraman
| studio         = Angel Films
| distributor    = Angel Films
| released       =  
| country        = India
| language       = Malayalam Language|Malayalam}}
 1978 Cinema Indian Malayalam Malayalam film,  directed KG George. The film stars Sukumaran in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Sukumaran

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Poovachal Khader.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mayakkathin Chirakukal || Ambili || Poovachal Khader ||
|-
| 2 || Prethabhoomiyil Naavukal || Selma George || Poovachal Khader ||
|-
| 3 || Rakthasindooram Chaarthiya || K. J. Yesudas || Poovachal Khader ||
|}

==References==
 

==External links==
*  

 
 
 


 