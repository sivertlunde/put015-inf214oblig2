Elephant (1989 film)
{{Infobox film
| name        = Elephant
| image       = Elephant_(1989_film).jpg
| starring    =
| director    = Alan Clarke
| producer    = Danny Boyle
| distributor = BBC Northern Ireland
| released    = 1989
| runtime     = 39 minutes
| language    = English
| budget      =
| music       =
| awards      =
}} Bernard MacLavertys the elephant in our living room" — a reference to the collective denial of the underlying social problems of Northern Ireland. Produced by BBC Northern Ireland, it first screened on BBC2 in 1989. The film was first conceived by Boyle, who was working as a producer for BBC Northern Ireland at the time. 
 16mm film with much of it filmed using a steadicam and features a series of tracking shots, a technique the director used regularly. The grainy 16mm film, together with the lack of dialogue, plot, narrative and music give the film a cold, observational documentary feel. Nothing is learnt about any of the gunmen or victims. Each of the murders are carried out calmly and casually; in one scene the gunman is seen to drive away slowly, even stopping to give way for traffic. The victims are shown for several seconds in a static shot of the body.

As with several of Clarkes films, "Elephant" received high praise and attracted controversy. After watching the film, Clarkes contemporary David Leland wrote: "I remember lying in bed, watching it, thinking, Stop, Alan, you cant keep doing this. And the cumulative effect is that you say, Its got to stop. The killing has got to stop. Instinctively, without an intellectual process, it becomes a gut reaction." 
 Gus Van Sants 2003 film Elephant (2003 film)|Elephant, based on the Columbine High School Massacre. Van Sants film borrowed not only Clarkes title, but also closely mirrors his minimalist style.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 