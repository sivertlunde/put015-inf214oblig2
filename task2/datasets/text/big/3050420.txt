Saraswatichandra (film)
 
{{Infobox film
| name           = Saraswatichandra
| image          = Saraswatichandra film 1968.jpg
| image size     = 
| alt            = 
| caption        = Saraswatichandra (1968)
| director       = Govind Saraiya
| producer       = 
| writer         = 
| narrator       = 
| starring       = Nutan Manish
| music          = Kalyanji-Anandji 
| cinematography = Nariman A. Irani
| editing        = 
| studio         = 
| distributor    = 
| released       = 1968 
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Saraswatichandra is a black-and-white Hindi film released in 1968 . It starred Nutan and Manish among others and was directed by Govind Saraiya. 
 Saraswatichandra by Bollywood director Sanjay Leela Bhansali, and currently airs on Star Plus.
 Gujarati novel, National Film Awards in the Best Cinematography and Best Music Director categories.  

==Story==
 
Saraswati Chandra tells the story of a young aristocrat, Saraswatichandra (Manish), who has a fixed marriage to Kumud (Nutan), an educated girl from a rich family. Saraswati decides to cancel the engagement and writes to Kumud to inform her. But soon she replies and soon the two keep on exchanging letters. Soon Saraswati decides to defy the customs and pays a visit to his fiancée. The two soon serenade and a short-lived romance takes place. Soon Saraswati returns home after promising Kumud and her family that he will return. However, on his return a family feud takes place and Saraswati writes to Kumud that he is not able to marry her (the IMDb synopsis stops here). This triggers a series of misunderstandings, which end up in Kumud’s marriage to a rich but illiterate suitor named Prabhat (Ramesh Deo). She is forced to marry because of family pressure. But as soon as she joins her husband at his palace, he quickly disdains her for nautch girls, and hardly hides his double life, asking her not to comment on his “weakness”.

Meanwhile, Saraswati, having forsaken his home, has been roaming the country, certainly not very far away, because he reaches Prabhat’s mansion and is found sleeping by the pool there one morning. His presence is made known to Kumud’s father in-law, who despises his son’s cheap life, and adopts Saraswati as his secretary. Of course the two former lovers meet, but Kumud is adamant about her duties, and staunchly stands up for religious traditions. Saraswati witnesses her anguished life and tries to reach out to her, but she objects: why does he interfere in their lives? She is “bohot khush” (very happy)! Nevertheless things change, because Prabhat’s behaviour is more and more openly flirtatious, and Saraswati’s presence lasts longer and longer! One night Kumud cannot resist entering his room, and if nothing happens, Saraswati decides to leave. On his way he is caught by dacoits and left for dead in the sun. A group of holy men spot him and take him away to their hermitage where he starts leading the life of a recluse.

Things darken for Kumud. She’s chased away from Prabhat’s mansion after one of his mistresses lays her hand on a fragment of her former lover’s letters. This gives Prabhat the pretext he’s been looking for: she must go back to her mother. The blackmail works well, because if she says he’s been unfaithful, he shows the letter to everyone. Nevertheless her dignified attitude has earned her the friendship of women in her in-laws’ household, and they reveal to Prabhat’s parents that he has chased his wife out of lust and selfishness. These chase him away and he vows he will die (he does die, strangely enough, because such a weak character would hardly have the resolve). Kumud takes advantage of a halt on her way back, and tries to drown in the river. But she doesn’t die, and is retrieved by some holy women on the bank. They take her to the same temple where Saraswati is trying to atone for his sins. So they meet again!

Last episode: the two lovers are once more face to face. First Kumud cannot believe she’s bumped into him again, but submits to her fate, and accepts the senior sister’s advice that she has to do something for Saraswati. The latter, on the other hand, has a mission to fulfil: told by the guru that Prabhat is dead, he will have to break the news to Kumud. A (very static) meeting is organised: after having realised that their fate has brushed them together, they admit they are made one for the other, and love starts developing. But Kumud doesn’t know she’s a widow, and still hangs on to the hope that she might change her husband, and that her life will continue at her in-laws once she gets back there.

When Saraswati reluctantly tells her, he faces a new Kumud, who must now embrace the widow’s status. The film ends with Saraswati accepting Kusum

==Cast==
* Nutan  as   Kumud Sundari
* Manish as  Saraswati Chandra / Navin Chander
* Vijaya Choudhury as  Kusum
* Ramesh Deo as  Pramad
* Sulochana Latkar as  Kumuds mother
* B.M. Vyas   as   Kumuds grandfather
* Seema Deo  as   Alak
* Jeevan Kala
* S.B. Nayampalli   as   Pramads father
* Sulochana Chatterjee   as   Pramads mother
* Babu Raje Dulari   as  Saraswati Chandras step-mother
* Shivraj  as   Saraswati Chandras father
* Praveen Paul   as  Kumuds aunt

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      =
| all_music       = Kalyanji Anandji

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Chandan sa badan
| note1           =
| writer1         =
| lyrics1         =
| music1          = Mukesh
| length1         =

| title2          = Chandan sa badan
| note2           =
| writer2         =
| lyrics2         = 
| music2          =
| extra2          = Lata Mangeshkar
| length2         = 

| title3          = Chod de saari duniya
| note3           =
| writer3         =
| lyrics3         = 
| music3          =
| extra3          = Lata Mangeshkar
| length3         = 

| title4          = Hamne apna sab kuch khoya 
| note4           =
| writer4         =
| lyrics4         = 
| music4          =
| extra4          = Mukesh
| length4         = 

| title5          = Phool tumhe bheja hai khat mein
| note5           =
| writer5         =
| lyrics5         = 
| music5          =
| extra5          = Lata Mangeshkar, Mukesh
| length5         = 

| title6          = O main to bhool chali babul ka desh 
| note6           =
| writer6         =
| lyrics6         = 
| music6          =
| extra6          = Lata Mangeshkar
| length6         = 
}}

==Awards and recognition==
* 1969, National Film Awards
** Best Cinematography, Nariman Irani
** Best Music Director, Kalyanji-Anandji
* 1969, Filmfare Best Dialogue Award, Ali Raza

==See also== National Film Awards
* Filmfare Awards
* Filmfare Best Dialogue Award
* Saraswatichandra (disambiguation)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 