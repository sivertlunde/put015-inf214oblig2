Love Thy Neighbour (1973 film)
 
 
{{infobox Film 
 | name = Love Thy Neighbour
 | image = "Love_Thy_Neighbour"_(1973).jpg
 | caption = 
 | director = John Robins
 | producer = Roy Skeggs
 | writer = Harry Driver Vince Powell Kate Williams
 | music = Albert Elms
 | cinematography = Moray Grant
 | editing = James Needs
 | studio  = Hammer Films
 | distributor = Anglo-EMI
 | released = 4 July 1973
 | runtime = 85 minutes
 | country = United Kingdom
 | language = English
 | budget =
 }} 1973 British Kate Williams and Nina Baden-Semper, spun off from the television series Love Thy Neighbour.

==Plot==
Eddie and Joan Booth live next door to Bill and Barbie Reynolds.  Whilst Joan and Barbie are best friends as well as neighbours, Bill and Eddie are complete opposites in all things, including colour.  Unbeknown to their husbands Joan and Barbie enter a Love Thy Neighbour competition to win a cruise, but are unsure how to get around the problem of their antagonistic husbands.  To add to the problems Joans Mother In Law is coming to stay, and Barbie has her Father In Law Coming from Trinidad.  What will Bill and Eddie do when they realise their parents get on?    

==Cast==
* Jack Smethurst as Eddie Booth
* Rudolph Walker as Bill Reynolds
* Nina Baden-Semper	as Barbie Reynolds Kate Williams as Joan Booth
* Bill Fraser as Mr. Granger
* Charles Hyatt as Joe Reynolds
* Patricia Hayes as Annie Booth	
* Melvyn Hayes as Terry
* Keith Marsh as Jacko
* Tommy Godfrey as Arthur
* Azad Ali as Winston
* Arthur English as Carter
* Andrea Lawrence as Norma
* Clifford Mollison	as Registrar
* Lincoln Webb as Charlie
* Norman Chappell as Indian Conductor
* Anna Dawson as Betty
* Bill Pertwee as Postman
* Pamela Cundell as Dolly
* Annie Leake as Lil
* Damaris Hayman as Woman on Bus
* Siobhan Quinlan as Carol
* James Beck as Cyril
* Dan Jackson as Black Groom
* John Bindon as White Groom
* Lesley Goldie as White Bride
* Nosher Powell as Bus Driver
* Michael Sharvell-Martin as Police Constable

==Reception==
*The film was popular at the box office, being ranked the 15th most popular movie of the year in England. Tom Johnson and Deborah Del Vecchio, Hammer Films: An Exhaustive Filmography, McFarland, 1996 p368 
*Britmovie wrote, "this dated, politically incorrect tale of bigotries and one-upmanship is sprinkled with ignorant comments and insults that are frequently more laughable than offensive when viewed today."  

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 


 