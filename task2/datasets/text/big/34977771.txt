Chinna Thambi Periya Thambi
{{Infobox film|
| name = Chinna Thambi Periya Thambi
| banner = 
| caption =
| director = Manivannan
| writer = Manivannan
| story = Shanmuga Priyan Prabhu Nadhiya Sudha Chandran
| producer = Thara Sivakumar K. Padmavathy M. L. Nallamuthu
| music = Gangai Amaran
| cinematography = A. Sabapathy
| editing = Gouthaman
| studio = Chemba Creations
| distributor = Chemba Creations
| released = 27 February 1987  
| runtime =
| country = India Tamil
| budget =
}}

Chinna Thambi Periya Thambi ( ),( ) is a 1987 Tamil Romantic Comedy film directed by Manivannan. The film casts Sathyaraj and Prabhu Ganesan in lead role along with Nadia Moidu|Nadia. The film was a box-office blockbuster and completed a 100 day run at all centres.

==Plot== Prabhu and Sathyaraj are brothers who fall in love with their uncles daughter Nadia Moidu|Nadia. Nadiya is initially arranged to marry a wealthy man but due to dire circumstances the wedding is called off.She then goes back to the village and starts to live with her uncles and grandmother.Eventually she falls in love with Prabhu,meanwhile, Sathyaraj happens to rescue a widow Sudha Chandran from dire straits, and falls for her.The story ends with both the brothers happily married.

==Cast==
* Sathyaraj Periya Thambi
* Prabhu Ganesan Chinna Thambi
* Nadia Moidu Kavitha
* Sudha Chandran
* Gandhimathi as the protagonists mother Vijayan Sudha Chandrans brother
* Nizhalgal Ravi Nadias fiance

==Soundtrack==
Music composed by Gangai Amaran  except Oru Kaathal (composed by Ilaya Raja)  



{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chinna Thambi Periya Thambi || Gangai Amaran || Gangai Amaran || 03:40
|-
| 2 || En Paatta Ketta || S. P. Balasubrahmanyam || Vairamuthu || 04:21
|-
| 3 || Mamam Ponnukku || Malaysia Vasudevan, S. P. Balasubrahmanyam || Gangai Amaran || 04:09
|-
| 4 || Mazhayin Thuliyile || K. S. Chithra || Vairamuthu || 04:21
|-
| 5 || Oru Aala Marathula || K. S. Chithra || Madhampatti Sivakumar || 04:20
|-
| 6 || Oru Kathal || S. P. Balasubrahmanyam, S. Janaki || Vairamuthu || 04:49
|-
| 7 || Ya Ya || S. P. Sailaja || Gangai Amaran || 04:04
|}

==Location==
The movies was shot in and around the Coimbatore city area showcasing places such as the Venkatlakshmi Kalyana Mandapam and the Viking baniyan company (currently in Tirupur).

==Reception==
The film received positive reviews from the critics. The Prabhu-Sathyaraj duo was well appreciated as well as their comedy track in the restaurant where they try to get off without paying the bill.

==References==
 

==External links==
*  

 
 
 
 
 


 