Nenjai Thodu
{{Infobox film|
| name = Nenjai Thodu
| image =
| caption =
| director = Rajkannan
| writer =
| starring = Gemini Lakshmi Rai Nagesh Nassar
| producer = M Sivakumaran, M Thoufeeq
| music = Srikanth Deva
| editor =
| released =  
| runtime =
| language = Tamil
| country = India
| budget =
}}

Nenjai Thodu is a South Indian Tamil film released in 2007.

==Plot==
The movie starts off with the Nasser losing his wife at the birth of his second son. He starts to hate his son and even leaves home with his first child. The child grows up into Siva(Gemini) and he lives with his grandparents who love him so much.

Sivas brothers Krishna(M Thoufeeq) marriage gets fixed up. At this time, Siva meets his father again. His father still hates him and barely talks to him. This really hurts Siva. He tries to not keep it in his mind and just goes along. At this point he meets Aishwarya(Lakshmi Rai), who is his brothers sister-in-law. These two lock horns with each other right in their first meeting.

As time goes by, Siva and Aishwarya develop feeling towards each other. At the same point, his father also starts to like him. Unfortunately, Sivas marriage gets fixed up with another girl, chosen by his father. Now, Siva is scared to tell his father, that he loves Aishwarya, because he doesnt want to break his fathers trust in him. Sivas grandfather Nagesh knows about the love between Siva and Aishwarya and even tries to convince his son to get them both married, but Nasser refuses. Now, Siva is stuck between his father and his love. But fate has some other games to play.

When both Siva and Aishwarya were travelling in a train, they come to know that bomb is fixed in a train. All the passengers had came out, the train gets crashed which makes both Siva think that Aiswarya is dead while Aiswarya assumes that Siva is dead, they both fall unconscious and admitted to the hospital. It is revealed that Sivas mind has damaged (doctor reveals that since his mother died his mind has become emotional) and he is under the wrong impression that Aiswarya is dead. The film ends with Sivas father and Aishwarya taking care of Siva but it is revealed that Sivas mind will become normal only after four or five months.

==Songs==
Puthu Vasam- Selva, Sadhana Sargam

Kichu Kichu- Sangeetha

En Thai Aval- Vijay Yesudas

LKG- Malgudi Subha, Srilekha Parthasarathy

Thondathe- Shankar Mahadevan, Anuradha Sriram

Ulalela- Udit Narayan

 
 
 
 


 