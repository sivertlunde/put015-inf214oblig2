The Kings of Summer
{{Infobox film
| name           = The Kings of Summer
| image          = The Kings of Summer.jpg
| alt            = 
| caption        =
| director       = Jordan Vogt-Roberts
| producer       = Tyler Davidson  John Hodges  Peter Saraf
| writer         = Chris Galletta Nick Robinson  Moisés Arias  Gabriel Basso  Alison Brie  Nick Offerman  Megan Mullally Ryan Miller
| cinematography = Ross Riege
| editing        = Terel Gibson Big Beach Films Low Spark Films
| distributor    = CBS Films (U.S.A) StudioCanal (UK/Germany)
| released       =  
| runtime        = 94 minutes  
| country        = United States
| language       = English
| budget         =
| gross          = $1,315,590 
}} independent coming-of-age story|coming-of-age comedy-drama film that premiered at the 2013 Sundance Film Festival,   and received a limited U.S. release in May 2013.   

==Plot== Nick Robinson), on the verge of adulthood, finds himself increasingly frustrated by the attempts of his single father, Frank (Nick Offerman), to manage his life. Declaring his freedom once and for all, he escapes to a clearing in the woods with his best friend, Patrick (Gabriel Basso), who was also sick of his life at home with his annoying and seemingly lame parents, and a strange kid named Biaggio (Moisés Arias) who just happened to tag along. He announces that they are going to build a house there, free from responsibility and parents. Once their makeshift abode is finished, the three young men find themselves masters of their own destiny, alone in the woods. Joe claims himself and Biaggio to be the hunters, while Patrick goes off to harvest. Several weeks pass and Patrick and Joe are reported missing and appear on multiple news channels.
 Erin Moriarty) Monopoly game by teaming up with Biaggio to trade land and buy hotels in the orange Monopoly in front of Patricks piece, leading him to lose the game on the very next turn. The two get into a scuffle. Joe calls Kelly a "cancer" and a "bitch" who ruined the peace and harmony the three of them were having and eventually makes her walk out of the house in despair. Joe, realizing that Patrick feels bad for Kelly, taunts him to go after her and stomps on his previously broken foot. Patrick leaves the house and goes after Kelly, comforting her with a kiss. Biaggio, who has become good friends with Joe, is told to leave as well, leaving Joe to live by himself.

About a month later, Joe is still living alone in the woods. Short on money, he sets out to hunt his own food, eventually leading him to kill and eat a rabbit. Joe, not disposing of the body properly, attracts a snake into the house. Meanwhile, Kelly goes to a concerned Frank and says she can take him to Joe. When they arrive, they find Joe cornered by the presumably venomous snake that appeared the night before. Biaggio comes barging in and attempts to kill the snake with his machete, but is bitten instead and collapses, becoming violently ill. Frank, Kelly and a mildly feral Joe rush Biaggio to the hospital. Biaggio survives and tells Joe that he saw heaven and if he had to do it all over again, he would do the same thing, but then changes his mind.

Joe and Patricks parents each drive them home. As they view each other from their respective cars, they flip each other off jokingly and part ways. The film ends showing shots of the house Joe, Patrick and Biaggio built.

After the credits, Biaggio is seen still residing in the house in the woods.

==Cast== Nick Robinson – Joe Toy
*Gabriel Basso – Patrick Keenan
*Moisés Arias – Biaggio
*Nick Offerman – Frank Toy, Joes father
*Alison Brie – Heather Toy, Joes sister
*Megan Mullally – Mrs. Keenan
*Marc Evan Jackson – Mr. Keenan
*Eugene Cordero – Colin
*Mary Lynn Rajskub – Captain Davis
*Thomas Middleditch - Rookie Cop Erin Moriarty – Kelly
*Nathan Keyes - Paul Angela Trimbur – FacePaint
*Kumail Nanjiani – Delivery Guy
*Austin Abrams – Aaron
*Craig Cackowski – Mr. Larson
*Lili Reinhart – Vicki
*Cristoffer Carter - Construction Kid
*Hannibal Buress - Bus Driver
*Tony Hale - Bus Passenger

==Production== Chagrin Falls, Lyndhurst and South Pointe Hospital in Warrensville Heights, Ohio|Warrensville. 

==Release==
The film premiered on January 19, 2013, during the 2013 Sundance Film Festival as Toys House.   The title of the film was later changed to The Kings of Summer. It was shown at the Cleveland International Film Festival on April 3, 2013. 

The Kings of Summer began its limited U.S. release on May 31, 2013.   It received a UK release in August 2013. 

==Reception==
The film received positive reviews and currently holds a 75% on Rotten Tomatoes, with the consensus stating: "Thanks to charming performances and endearingly off-kilter spirit, The Kings of Summer proves to be a slight, sweet entry in the crowded coming-of-age genre." 

According to Sheila OMalley of the Chicago Tribune, "despite some beautiful sequences and solid acting, the script by first-timer Chris Galletta pulls its punches, over-explains the emotional meaning of its moments, and tries to lighten the mood in sometimes awkward sit-com-style ways, betraying the movies more honest spirit. The Kings of Summer flirts with profundity, seeming to yearn for it and fear the honest expression of it at the same time. There is much here to admire, but the overall impression is of a film that does not have the courage of its convictions." 
 closing act cant quite maintain the laugh rate, but theres a lot of warm-hearted and commendably daft business along the way. 

The film made it onto Complexs Best Summer movies of 2013 list at number 17 and Not Made in Chelseas   list at number 4.

==References==
{{reflist|colwidth=30em|refs=
   
   
}}

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 