Flamingo Road (film)
{{Infobox film
| name           = Flamingo Road
| image          = Posterflamingox.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Jerry Wald Robert Wilder
| based on       =  
| narrator       =  
| starring       = {{plainlist|
* Joan Crawford
* Zachary Scott
* Sydney Greenstreet
* David Brian
}}
| music          = Max Steiner
| cinematography = Ted D. McCord
| editing        = Folmar Blangsted
| studio         = Michael Curtiz Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $1.5 million 
| gross          = $2.9 million 
}} Robert Wilder was based on a 1946 play written by Wilder and his wife, Sally, which was based on Robert Wilders 1942 novel of the same name. 

The plot follows an ex-carnival dancer who marries a local businessman to seek revenge on a corrupt political boss who had her railroaded into prison.

==Plot==
Lane Bellamy is a carnival dancer stranded in the small town of Boldon City in the Southern United States. She becomes romantically involved with Fielding Carlisle, a deputy sheriff whose career is controlled by Sheriff Titus Semple, a corrupt political boss who runs the town. Semple dislikes Bellamy and mounts a campaign against her. She has difficulty finding work and is arrested on a trumped-up morality charge. Meanwhile, Carlisle is the political machines choice for state senator, and to portray the perfect political family, he marries his long-time girlfriend, Annabelle Weldon.
 roadhouse run by Lute Mae Sanders. There, she meets Dan Reynolds, a businessman who supports the corrupt Semple so long as it is profitable. She charms Reynolds into marrying her and the couple moves to the towns best neighborhood, Flamingo Road.

As a kingmaker in the state, Semple decides to run Carlisle for governor and unseat the incumbent. This is too much even for Reynolds and now he decides to oppose Semple. When Carlisle, who has a weakness for drink, also begins to show his limits in cooperating with Semple, Semple flies into a rage and abandons him, destroying Carlisles career. Then Semple makes himself the candidate. At this, Reynolds grows stronger in his opposition. So Semple arranges to have Reynolds framed.

Later a drunken Carlisle, who knows whats happening but feels the situation is hopeless, visits the mansion on Flamingo Road and commits suicide practically in front of Bellamy. This gives Semple another weapon in his bid to ruin Bellamy and husband, who has now been indicted for graft (politics)|graft. Bellamy confronts Semple with a gun and demands he phone the attorney general and confess everything, but a physical struggle ensues and she shoots him dead. At the end, Bellamy is in prison awaiting a ruling and Reynolds indicates he will stick by her.

==Cast==
* Joan Crawford as Lane Bellamy
* Zachary Scott as Fielding Carlisle
* Sydney Greenstreet as Sheriff Titus Semple
* Gladys George as Lute Mae Sanders
* David Brian as Dan Reynolds
* Virginia Huston as Annabelle Weldon
* Fred Clark as Dr. Waterson
* Gertrude Michael as Millie
* Tito Vuolo as Pete Ladas
* Alice White as Gracie
* Sam McDaniel as Boatright

==Reception==
Howard Barnes wrote in the New York Herald Tribune, "Joan Crawford acquits herself ably in an utterly nonsensical and undefined part...Its no fault of hers she cannot handle the complicated romances and double crosses in which she is involved."   Bosley Crowther of The New York Times called it a "jumbled melodrama" in which Crawford robotically experiences a series of crises.   Variety (magazine)|Variety described it as "a class vehicle for Joan Crawford, loaded with heartbreak, romance and stinging violence." 

==Adaptation== Flamingo Road.

==Home media==
The film was released on VHS by Warner Home Video in 1998, which also issued it on DVD in 2008 as part of "The Joan Crawford Collection: Volume 2".

==References==
 

==External links==
*  
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)

 

 
 
 
 
 
 
 
 
 
 
 
 