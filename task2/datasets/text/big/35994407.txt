Identity Theft (film)
  Lifetime television network in 2004. 

==Plot==
Michelle Brown (Kimberly Williams-Paisley|Paisley) is a young woman who buys her first house and has an excellent credit history.  As she submits the paperwork to the realty company, one of the employees, Connie Volkos (Annabella Sciorra|Sciorra), uses Michelles credit card number, social security number and address to purchase numerous items,        including electronics and other luxuries. Eventually, this gets out of hand to the point that Michelle begins to suspect her identity has been stolen after receiving bills for items she had not paid for.   Volkos flees her apartment and travels through the country, dying her hair brown and even creating a false driver license in Michelles name.  She soon purchased her own apartment after reaching a different state. 

Within a matter of months, Volkos is arrested for attempting to deliver marijuana and flees the jurisdiction after her brother puts up her bail. Brown, Michelle. " ." Privacy Rights 
Clearinghouse | Empowering Consumers. Protecting Privacy. Web. 14 Apr. 
2010.   Coincidentally, Michelle has traveled to the southern United States in search of Volkos. Because Volkos was still using Michelles name, Michelle is arrested and detained at an airport but later released after explaining her predicament.       

Volkos is later arrested and is sentenced to two years in prison for her crimes. Because of the lack of regulations against identity theft at the time, Volkos did not face as severe of charges as an identity thief today would. Yet, this case caused reforms in the United States laws to increase regulations against identity theft. 

==Reception==
The film currently holds a 6.1 out of 10 on the Internet Movie Database.   Written reviews range from poor to acclaim on the internet.  

==References==
 

 
 
 
 
 
 

 