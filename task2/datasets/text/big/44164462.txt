Uyaran Orumikkan
{{Infobox film
| name           = Uyaran Orumikkan
| image          =
| caption        =
| director       = Vayanar Vallabhan
| producer       =
| writer         =
| screenplay     =
| starring       =
| music          = Aravindan Thevara
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by Vayanar Vallabhan.  The film had musical score by Aravindan Thevara.   

==Cast==

 

==Soundtrack==
The music was composed by Aravindan Thevara and lyrics was written by Vayanar Vallabhan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaalidaasante Bhaavana || KP Brahmanandan, Lathika || Vayanar Vallabhan || 
|-
| 2 || Mithilaapuriyile || Lathika || Vayanar Vallabhan || 
|-
| 3 || Thaazhvarayil Oru Thathamma || Vani Jairam || Vayanar Vallabhan || 
|}

==References==
 

==External links==
*  

 
 
 


 