The Sidelong Glances of a Pigeon Kicker
{{Infobox film
| name           = The Sidelong Glances of a Pigeon Kicker
| image          = The Sidelong Glances of a Pigeon Kicker poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John Dexter
| producer       = Richard Lewis 
| screenplay     = Ron Whyte
| based on       =   William Redfield Lois Nettleton Patrick Williams
| cinematography = Urs Furrer 
| editing        = John Oettinger 
| studio         = Saturn Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 William Redfield and Lois Nettleton. The film was released on February 1, 1971, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Jordan Christopher as Jonathan
*Jill OHara as Jennifer
*Robert Walden as Winslow Smith
*Kate Reid as Jonathans Mother William Redfield as Jonathans Father
*Lois Nettleton as Mildred
*Boni Enten as Naomi
*Elaine Stritch as Tough Lady
*Melba Moore as Model at Party
*Riggs OHara as Oliver
*Kristoffer Tabori as Olivers Boy Friend
*Donald Warfield as Young Stutterer
*Jean Shevlin as Mrs. Abelman
*Matt Warner as Mr. Abelman

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 