Evangeline (1919 film)
{{infobox film
| name           = Evangeline
| image          =Evangeline (1919) - ad still.jpg
| imagesize      =200px
| caption        = Miriam Cooper and Alan Roscoe in still from film (Motion Picture Magazine, February 1920)
| director       = Raoul Walsh William Fox
| based on       =  
| writer         = Raoul Walsh (scenario)
| starring       = Miriam Cooper
| music          =
| cinematography = 
| editing        =
| distributor    = Fox Film Corporation reels
| reels (5,200 feet)
| country        = United States
| language       = Silent film (English intertitles)
}} silent film produced and distributed by the Fox Film Corporation and directed by Raoul Walsh. The star of the film was Walshs wife at the time Miriam Cooper in the oft filmed story  based on a poem by Henry Wadsworth Longfellow. Filmed previously in 1908, 1911, and 1914.

Currently the film is considered to be a lost film.   

==Cast==
*Miriam Cooper - Evangeline
*Alan Roscoe - Gabriel
*Spottiswoode Aitken - Benedict Bellefontaine
*James A. Marcus - Basil
*Paul Weigel - Father Felician
*William A. Wellman - A British Lieutenant

==References==
 

==External links==
 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 


 