Tropical Trouble
Tropical British comedy Harry Hughes and starring Douglass Montgomery, Betty Ann Davies and Alfred Drayton.  It was based on the novel Bunga-Bunga by Stephen King-Hall. A series of misunderstanding leads to a colonial governors wife suspecting him of an affair with his assistant.

==Cast==
* Douglass Montgomery - George Masterman
* Betty Ann Davies - Mary Masterman
* Alfred Drayton - Sir Monagu Thumpeter
* Natalie Hall - Louise van der Houten
* Sybil Grove - Lady Thumpeter
* Victor Stanley - Albert
* Gerald Barry - Sir Pomfrey Pogglethwaite
* Morris Harvey - Chief of the Bungs
* Marie Ault - Nonnie
* Vernon Harris - Martindale

==References==
 

==Bibliography==
* Sutton, David R. A chorus of raspberries: British film comedy 1929-1939. University of Exeter Press, 2000.

==External links==
* 

 

 
 
 
 
 
 
 


 