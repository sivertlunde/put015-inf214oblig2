Frank McKlusky, C.I.
{{Infobox film
| name = Frank McKlusky, C.I.
| image = 
| image_size = 
| alt = 
| caption = 
| director = Arlene Sanford
| producer = Robert Simonds
| screenplay = Mark Perez Dave Sheridan Mark Perez
| starring = Dave Sheridan Cameron Richardson Randy Quaid Dolly Parton
| music = Randy Edelman
| cinematography = Tim Suhrstedt Jeffrey Greeley
| editing = Alan Cody Robert Simonds Productions Buena Vista Pictures
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = $18 million
| gross = 
}} Dave Sheridan and Mark Perez, and directed by Arlene Sanford. The film stars Sheridan in the titular role. The film was given a very limited release in the United States. 

==Plot==
Frank McKlusky is an insurance claims investigator who has been zealously safety conscious since witnessing a horrible motorcycle stunt gone awry in his childhood, an accident that left his father comatose. Frank still lives at home under the watchful eye of his mother and wears a helmet everywhere he goes. When his partner is killed, Frank is forced to leave his safe and secure life behind, go undercover, and crack the case, which he discovers is a sinister conspiracy. Unfortunately for the villains, Frank turns out to be a first-class bumbler in the Inspector Clouseau mold. 

==Cast== Dave Sheridan as Frank McKlusky
* Cameron Richardson as Sharon Webber
* Randy Quaid as Madman McKlusky
* Dolly Parton as Edith McKlusky
* Enrico Colantoni as Scott Bayou
* Kevin Farley as Jimmy
* Orson Bean as Mr. Gafty Joanie Laurer as Freeda
* Kevin Pollak as Ronnie Rosengold
* Tracy Morgan as Reggie Rosengold
* Andy Richter as Herb
* Josh Jacobson as Darryl McKlusky
* Adam Carolla and George Lopez as Detectives
* DeRay Davis as Basketball player
* Lou Ferrigno as Knife thrower Hanson as themselves
* Scott Baio (uncredited) as himself
* Gary Coleman (uncredited) as himself
* R. Lee Ermey (uncredited) as Jockey master
* Chad Everett (uncredited) as Doctor

==Reception==
The film received mostly negative reviews.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 