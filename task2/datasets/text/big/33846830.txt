Life's Whirlpool
{{infobox film
| mame           = Lifes Whirlpool
| image          =
| imagesize      =
| caption        =
| director       = Lionel Barrymore
| producer       = B. A. Rolfe William A. Brady (executive producer)
| writer         = Lionel Barrymore (story, scenario)
| starring       = Ethel Barrymore
| music          =
| cinematography = John M. Bauman
| editing        =
| distributor    = Metro Pictures
| released       = November 8, 1917
| runtime        = 5 reels
| country        = United States
| language       = Silent film (English intertitles)
}}
Lifes Whirlpool (1917 in film|1917) is a silent film written and directed by Lionel Barrymore with his sister Ethel Barrymore as the star. This is the brother and sisters only collaboration on a silent film as director and star.

This film shouldnt be confused with McTeague (film)|McTeague which was also known as Lifes a Whirlpool, an early telling of Frank Norriss McTeague, later filmed by Eric von Stroheim as Greed (film)|Greed (1923).

The Lionel Barrymore directed film was produced by B. A. Rolfe and released through Metro Pictures. Barrymore would return for a short time to directing films in the early sound era.  This is now considered a lost film.  

==Cast==
*Ethel Barrymore - Esther Carey
*Paul Everton - B.J. Hendrix Alan Hale - Dr. Henry Grey
*Reginald Carrington - John Martin
*Ricca Allen - Ruth Martin
*Frank Leigh - Dirk Kansket
*Walter Hiers - Fatty Holmes
*Harvey Bogart - Ezra Craddock

==See also==
*Lionel Barrymore filmography
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 