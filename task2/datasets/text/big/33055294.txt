Creature of the Walking Dead
{{Infobox film
| name           = Creature of the Walking Dead
| image          = 
| image_size     = 
| caption        = 
| director       = Jerry Warren
| producer       = Jerry Warren (producer)
| writer         = Jerry Warren
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Jerry Warren
| editing        = Jerry Warren
| studio         = 
| distributor    = 
| released       = 1965
| runtime        = 74 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Creature of the Walking Dead is a 1965 American film directed by Jerry Warren.

== Plot summary ==

A mad scientist discovers the secret of eternal youth by killing and draining the blood of a young woman. He is executed for his crime, but his descendent moves into his home and revives him.

== Cast ==
*Rock Madison as Ed, a cop
*Ann Wells as Girl on telephone
*Willard Gross
*George Todd
*Katherine Victor as Mrs. Roger Vernon
*Fernando Casanova as (archive footage)
*Aurora Alvarado
*Robert Christopher
*Rosa María Gallardo
*Fred Hoffman
*Lloyd Nelson
*Chuck Niles
*Bruno VeSota

== Soundtrack ==
 

== Promotion ==
The tagline was "The Fountain Of Youth Is Filled With Blood!"

== External links ==
* 
* 

 
 
 
 
 
 
 
 

 