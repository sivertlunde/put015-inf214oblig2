La Poison
{{Infobox film
| name           = La Poison
| image          = La Poison poster.jpg
| caption        = Theatrical release poster
| director       = Sacha Guitry
| producer       = Paul Wagner, Alain Poiré
| writer         = Sacha Guitry
| starring       = Michel Simon Louis de Funès
| music          = Louiguy
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
}}
 comedy Drama drama film, written and directed by Sacha Guitry, starring Michel Simon and Louis de Funès.

== Cast ==
*Michel Simon: Paul Louis Victor Braconnier, the gardener
*Germaine Reuver: Blandine Braconnier, Pauls wife
*Jean Debucourt: Maître Aubanel, the famous lawyer
*Louis de Funès: André Chevillard, a citizen of Remonville
*Marcelle Arnold: Germaine Chevillard, Andrés wife
*Georges Bever: Mr Gaillard, the chemist of Remonville
*Nicolas Amato: Victor Boitevin, an inhabitant
*Jacques Varennes: the prosecutor
*Jeanne Fusier-Gir: Mrs Tyberghen, the florist
*Pauline Carton: Mrs Michaud, the grocer
*Albert Duvaleix: Abbot Méthivier
*Léon Walther: the General State Attorney
*Henry Laverne: Judiciary President
*Harry Max: Henri, an inhabitant

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 
 
 