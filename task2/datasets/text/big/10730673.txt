Maddalena, zero in condotta
{{Infobox film
| name           = Maddalena, zero in condotta
| image          = Maddalenazeroincondotta.jpg
| image size     =
| caption        = Film poster
| director       = Vittorio De Sica
| producer       = Franco Magli
| writer         = Ferruccio Biancini
| narrator       =
| starring       = Vittorio De Sica Vera Bergman Carla Del Poggio Irasema Dilián
| music          = Nuccio Fiorda
| cinematography = Mario Albertelli
| editing        = Mario Bonotti
| distributor    = Artisti Associati
| released       = 1940
| runtime        = 79 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
 1940 comedy film directed by Vittorio De Sica.

==Cast==
*Vittorio De Sica ...  Alfredo Hartman
*Vera Bergman ...  Linsegnate Elisa Malgari
*Carla Del Poggio ...  Maddalena Lenci
*Irasema Dilián ...  Eva Barta, la privatista (as Eva Dilian)
*Amelia Chellini ...  La direttrice
*Pina Renzi ...  La professoressa Varzi
*Paola Veneroni ...  Lallieva Varghetti, la spiona
*Dora Bini ...  Lallieva Caricati
*Enza Delbi ...  Un allieva
*Roberto Villa ...  Stefano Armani
*Armando Migliari ...  Malesci, il professore di chimica
*Guglielmo Barnabò ...  Il signore Emilio Lenci
*Giuseppe Varni ...  Amilcare Bondani, il bidello
*Arturo Bragaglia ...  Sila, il professore di ginnastica

==External links==
*  

 

 
 
 
 
 
 
 
 
 