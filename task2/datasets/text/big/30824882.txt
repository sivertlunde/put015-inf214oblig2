Retreat (film)
{{Infobox film
| name           = Retreat
| image          = Retreat film poster.jpg
| alt            =  
| caption        = 
| director       = Carl Tibbetts Sir David Frost
| screenplay     = Janice Hallett Carl Tibbetts
| starring       = Cillian Murphy Jamie Bell Thandie Newton
| music          = Ilan Eshkeri
| cinematography = Chris Seager
| editing        = Jamie Trevill
| studio         = Magnet Films Ripple World Pictures
| distributor    = Sony Pictures Entertainment
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = $6.4 million  
| gross          = 
}}

Retreat is an 2011 British Horror film|horror-thriller film  and the directorial debut of former film editor Carl Tibbets. The film stars Cillian Murphy, Jamie Bell, and Thandie Newton as three people isolated from the rest of the world on a remote island, who are told they are survivors of a fatal airborne disease that is sweeping over the entire world. However, their induced isolation may be the result of a lie, and it may be that they are being held at the whim of a madman. The film has had mainly positive reviews.

== Plot ==
London architect Martin Kennedy (Cillian Murphy) and his journalist wife Kate (Thandie Newton) often visit a small, remote, uninhabited island off the west coast of Scotland called Blackholme Island, for their holiday retreats. The only dwelling on the island, Fairweather Cottage, is seasonally operated by the owner Doug from the mainland and only reachable by ferry. After previously suffering a miscarriage, Kates relationship with Martin has become tense, and in an effort to rekindle their marriage they decide on a return visit to the island. A few nights into their stay, the generator in the cottage explodes, injuring Martins arm and leaving them without electricity. They use the CB radio, their only source of communication to the mainland, to call Doug who agrees to ferry out to help them.
 spread over the entire world in the timespan of a few weeks. The virus is incurable and highly contagious, attacking the respiratory system and causing the victims to choke on blood with a 100% fatality rate. Jack says that the military has lost control, and is now advising civilians to seal themselves up in their homes and not allow anybody access. With only static now coming from the CB radio, Martin decides to play it safe and help Jack, who aggressively takes command, board up the door and the windows.

Over the next few days, Jack becomes increasingly strange and erratic, and his intimidating behaviour begins to disturb the couple even further. They even suspect that the virus might not be real and Jack is insane. When Kate asks him if he is married, he tells her his wife died of R1N16 and he becomes aggressive and threatening. Martin and Kate decide to leave the cottage and take their chances outside, but Jack refuses to let them, forcing them into the bedroom at gunpoint and locking them in. Martin sneaks outside through a skylight, finding the bodies of Doug and his wife at the pier, killed by gunshot wounds. Using Dougs hunting shotgun, Martin returns to the cottage and gets the upper hand on Jack. However, just as Kate begins to tie Jacks hands behind his back, Martin suddenly begins coughing up blood; it appears the Argromoto Flu is very real and he is infected. Kate is forced to shoot her husband dead with the shotgun, to spare him a slow and agonising death.
 carrier of R1N16. Jack infected his wife, killing her, and he fled to Blackholme Island to quarantine himself. Jack reveals the CB radio worked all along, and he just changed the settings so Kate and Martin would not be able to reach anyone. Jack fixes the CB radio and the military broadcast on it claims the soldiers have a vaccine to the virus. He tells her that the military are lying, there is no vaccine, and that the military will not allow them to leave the island alive. Kate does not believe him, and angered by the possibility that Martin could have been saved she shoots Jack dead. As she attempts to leave the island in the boat with Martins body, a military helicopter flies over and she is killed by a sniper who shoots her in the head.

== Cast ==
* Cillian Murphy as Martin Kennedy
* Thandie Newton as Kate Kennedy
* Jamie Bell as Pvt. Jack Coleman
* Jimmy Yuill as Doug, the boat ferryman and owner of Fairweather Cottage

== Release ==
Retreat premiered at the Fantasia Film Festival on July 18, 2011,  and received a limited release in the United Kingdom and the United States respectively on 14 and 21 October 2011.

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 73% of 11 surveyed critics gave the film a positive review; the average rating was 5.7/10.  Bloody Disgusting rated it 4.5/5 stars and praised the intense, chilling atmosphere and plot twists.  Peter Bradshaw of The Guardian rated it 3/5 stars and called it "a neat, tense thriller from a first time director that provides a decent role for Jamie Bell."  John Anderson of Variety (magazine)|Variety wrote, "A potential menage a trois of terror is served up as rather weak tea in "Retreat," which fails to make its alleged suspense, thrills or even its mist-enshrouded landscapes particularly plausible."  Neil Smith of Total Film rated it 3/5 stars and called it "Tense if rather monotonously bleak".  Mark Adams of Screen Daily wrote, "An unrelentingly moody and claustrophobic three-handed thriller, Retreat is an assured and nicely staged debut from Carl Tibbetts that might feel all rather familiar but manages to keep the story suitably unpredictable and nicely paced."  Damon Wise of Empire (film magazine)|Empire wrote that the performances make up for the story.  Scott Weinberg of Fearnet wrote that the film is "certainly more engaging than its basic premise might imply." 

== References ==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 