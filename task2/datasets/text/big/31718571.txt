Masterji
{{Infobox film
| name = Masterji
| image = Masterjifilm.jpg
| image_size =
| caption =
| director       = K. Raghavendra Rao 
| producer       = R.C. Prakash
| writer         =  
| lyrics         = Indeevar
| story          = K. Bhagyaraj
| starring       = Rajesh Khanna Sridevi Anita Raj  Kader Khan  Shakti Kapoor  Aruna Irani  Asrani  Shyama  Om Shivpuri  Jayshree Gadkar
| music          = Bappi Lahiri
| cinematography = K. S. Prakash Rao
| editing        = V.R. Kotagiri
| distributor    =
| released       = 30 Aug 1985
| runtime        =
| language       = Hindi
| country        = India
| gross      =   7.75 Crores
| preceded_by    =
| followed_by    =
| website        =
}}
 Bollywood romantic comedy film film directed by K. Raghavendra Rao, released on 30 August, Bollywood films of 1985|1985. The film stars Rajesh Khanna in the title role as Masterji  and Sridevi as his heroine as the main lead characters of the film. This film was of different genre - Comedy than the other films of the pair. This film was remake of 1984 Tamil film Mundhanai Mudichu, directed by  K. Bhagyaraj, where Bhagyaraj also played the lead male role. After the Tamil version, K. Bhagyaraj decided to remake it in Hindi but handed over directorship to Kovelamudi Raghavendra Rao. K. Bhagyaraj himself wrote the story and  screenplay for Masterji.

==Plot==
Radha (Sridevi) is a notorious prankster creating a variety of mischiefs along with her gang, which often end up in the village court. Masterji (Rajesh Khanna) enters the village with his infant child to take up the vacant teacher post in the local school, but isnt spared from Radhas pranks upon his arrival. He takes up the job with one hand holding the book, and the other rocking the cradle in the classroom much to the amusement of local folk in the village. Radhas playful nature transforms into love when she learns that hes a widower. She tries many ways to win the teachers heart, but fails every time. The teacher believes that a stepmother would not take care of his child, and thus even rejects the offer to marry his dead wifes sister. As a last hope to attain him, Radha blames the teacher for molesting her and even swears on it by crossing over his child in front of the village court. Petrified, and with the whole village surrounded, he is left with very little option than to marry her. But vows never to touch her and remain in celibacy. But Radha doesnt give. She relentlessly tries to seduce him by unconventional methods that prove to be testing times for the teacher. With the chances of winning him slowly decreasing, Radha comes to a conclusion once and for all.

==Cast==
* Rajesh Khanna ....  Raju
* Sridevi ....  Radha
* Anita Raj ....  Shobha
* Kader Khan....  Jamnadas
* Shakti Kapoor ....  Bholashankar
* Aruna Irani ....  Roopa
* Asrani ....  Pandit
* Shyama ....  Shanti
* Om Shivpuri ....  Vaid
* Jayshree Gadkar ....  Masterjis mother-in-law

==Tracks list==
* "Bulbul Mere Bata Kya Hai Meri Khata" : Kishore Kumar, Lata Mangeshkar
* "Jab Tanhai Me Do Badan Pas Ahtein Hain" : Kishore Kumar, Asha Bhosle
* "Aankhen To Kholo Swami" : Kishore Kumar, Asha Bhosle
* "Dum Kham Wala Balam Matwala" : Asha Bhosle
* "Galon Par Yeh Kaisey Nishan, Hont Kisike Meherbaan" : Kishore Kumar, Asha Bhosle

==Box office==
Masrterji was a box office super hit. It received praised from critics and it collect 7.77cr rupees.  

==References==
 

==External links==
*  

 
 
 
 
 
 