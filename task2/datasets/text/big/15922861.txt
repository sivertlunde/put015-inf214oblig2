Common Places
{{Infobox film  
| name           = Lugares comunes
| image          = CommonPlaces.jpg
| director       = Adolfo Aristarain
| producer       = Adolfo Aristarain Joasean Gómez Gerardo Herrero
| writer         = Adolfo Aristarain Lorenzo F. Aristarain Kathy Saavedra
| starring       = Federico Luppi Mercedes Sampietro Arturo Puig
| cinematography = Porfirio Enríquez    
| editing        = Fernando Pardo  
| distributor    = Transmundo Films
| released       = September 12, 2002  (Argentina)  September 20, 2002  (Spain) 
| runtime        = 115 minutes
| country        = Argentina Spain Uruguay Spanish
}}
 Argentine and Spanish drama film co-written, co-produced and directed by Adolfo Aristarain. The film marks the seventh collaboration between Aristarain and actor Federico Luppi. It also stars Mercedes Sampietro and Arturo Puig.

== Synopsis ==
After being forced to retire, literature professor Fernando Robles (Federico Luppi) and his wife Liliana (Mercedes Sampietro) are forced to reevaluate their lives and make major changes in them. These include setting the record straight with their son, exiled in Madrid and starting out a new life. They decide to buy a small lavender farm in Córdoba, Argentina|Córdoba from a widowed man, Zacarías, and with the aid of their lawyer friend Carlos (Arturo Puig), attempt to start out their new business.

Along the way, Fernando, an aspiring writer himself, jots down notes and ideas for a novel in a book, which he frequently narrates in voice-overs. But this attempt at a new life is cut short when Fernando suddenly contracts pneumonia one cold night on the mountains. He is hospitalized and dies over a week, with enough time to bid farewell to his family and tell his wife all he never said out loud.

Liliana rejects the help of the sympathetic Carlos and the offer from her son Pedro to come live with his family in Spain. She opts to carry on her late husbands attempt of a new life, and stays in the farm Fernando helped rebuild.

==Cast==
*Federico Luppi ... Fernando Robles
*Mercedes Sampietro ... Liliana Rovira
*Arturo Puig ... Carlos Sulla
*Carlos Santamaria ... Pedro Robles
*Valetina Bassi ... Natacha
*Claudio Rissi ... Demedio

==Awards and Nomination==
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Argentinean Film Critics Association Awards Best Actor Federico Luppi
| 
|- Best Actress Mercedes Sampietro
| 
|- Best Art Direction Abel Facello
| 
|- Best Director Adolfo Aristarain 
| 
|- Best Film
|
| 
|- Best Screenplay Adolfo Aristarain, Kathy Saavedra
| 
|- Best Supporting Actor Claudio Rissi
| 
|- Butaca Awards Best Film Actress Mercedes Sampietro
| 
|- Cartagena Film Festival Best Film Adolfo Aristarain
| 
|- Cinema Writers Circle Awards Best Actress Mercedes Sampietro
| 
|- Best Screenplay Kathy Saavedra
| 
|- Fotogramas de Plata Best Actress Mercedes Sampietro
| 
|- Fribourg International Film Festival Audience Award Adolfo Aristarain
| 
|- Grand Prix
| 
|- Goya Awards Best Actress Mercedes Sampietro
| 
|- Best Screenplay Kathy Saavedro
| 
|- Gramado Film Festival Best Actress Mercedes Sampietro
| 
|- Best Film
|
| 
|- Havana Film Festival Glauber Rocha Award Adolfo Aristarain
| 
|- OCIC Award
| 
|- Vigia Award
| 
|- San Sebastián International Film Festival Best Screenplay Kathy Saavedra
| 
|- Best Actress Mercedes Sampietro
| 
|- Golden Seashell Adolfo Aristarain
| 
|- Sant Jordi Awards Best Spanish Actress Mercedes Sampietro
| 
|- Spanish Actors Union Best Lead Performance - Female Mercedes Sampietro
| 
|-
|}

== External links ==
*  

 
 
 
 
 
 

 