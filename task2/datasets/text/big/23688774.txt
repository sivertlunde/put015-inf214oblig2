Beck – Den japanska shungamålningen
{{Infobox film
| name           = Beck – Den japanska Shungamålningen
| image          =Beck 21 - Den japanska shungamålningen.jpg
| alt            = 
| caption        = Swedish DVD cover
| director       = Kjell Sundvall
| producer       = Lars Blomgren Tomas Michaelsson
| writer         = Rolf Börjlind Cecilia Börjlind
| starring       = Peter Haber Mikael Persbrandt Måns Nathanaelson Stina Rautelin
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2007
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
}}
Beck – Den japanska Shungamålningen is a 2007 Swedish police film about Martin Beck, directed by Kjell Sundvall.

== Cast ==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Måns Nathanaelson as Oskar Bergman
*Stina Rautelin as Lena Klingström
*Ing-Marie Carlsson as Bodil Lettermark
*Marie Göranzon as Margareta Oberg
*Ingvar Hirdwall as Martin Becks neighbour
*Rebecka Hemse as Inger (Martin Becks daughter)
*Neil Bourguiba as Wilhelm (Ingers son)
*Peter Hüttner as Oljelund
*Dieter Pfaff as Hans Sperling
*Jan Malmsjö as Ernst Levendahl
*Philip Zandén as Malte Beverin
*Katarina Lundgren-Hugg as Marie Lisowska
*Dan Bratt as Jan Forsgren
*Jeff Ranara as Sun
*Hans V. Engström as Jovan Andrecz
*Ingrid Luterkort as Gila Andrecz

== References ==
* 

== External links ==
* 

 

 
 
 
 
 
 
 


 
 