Bottle Rocket
 
{{Infobox film
| name           = Bottle Rocket
| image          = Bottle-Rocket.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Home video release poster
| director       = Wes Anderson
| producer       = Polly Platt Cynthia Hargrave James L. Brooks (exec.)
| writer         = Owen Wilson Wes Anderson James Caan
| music          = Mark Mothersbaugh
| cinematography = Robert Yeoman
| editing        = David Moritz
| studio         = Gracie Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English Spanish
| budget         = $7 million
| gross          = $560,069   
}} crime comedy directorial debut, Owen and Luke Wilson, James Caan and Robert Musgrave.

The film was a commercial failure but launched Andersons career by drawing attention from critics. Director Martin Scorsese later named Bottle Rocket one of his top-ten favorite movies of the 1990s.   

Bottle Rocket is also the name of a short film directed by Anderson and starring both Wilson brothers and Musgrave—shot in 1992 and released in 1994—on which the feature-length film was based.

==Plot==
In Arizona, Dignan "rescues" Anthony from a voluntary psychiatric unit, where he has been staying for self-described exhaustion. Dignan has an elaborate escape planned and has developed a 75-year plan that he shows to Anthony. The plan is to pull off several heists, and then meet up with a Mr. Henry, a landscaper and part-time criminal known to Dignan.
 getaway driver because he is the only person they know with a car. The three of them buy guns and return to Bobs house to plan their next heist, which will be at a local bookstore. The group bickers as Dignan struggles to describe his intricate plan.

The group steals a small sum of money from the bookstore and "go on the lam" at a motel. Anthony meets Inez, a maid, and the two spark a romance despite Inezs lack of English. Bob learns that his marijuana crop has been discovered by police and that his older brother has been arrested. Bob leaves to help his brother. Before leaving, Anthony gives Dignan an envelope for Inez. Dignan delivers the envelope to Inez while she is cleaning a room, not knowing the envelope has most of his and Anthonys money inside. Inez does not open the envelope and hugs Dignan to say goodbye. As Dignan is leaving, Inez asks an English-speaking male friend of hers to chase after Dignan and tell him she loves Anthony. When he delivers the message he says, "Tell Anthony I love him". Dignan fails to realize he is speaking for Inez and does not deliver the message.

Taking an abandoned Alfa Romeo Spider, Dignan and Anthony continue with the 75-year plan, but the car breaks down. Anthony reveals that the envelope Dignan gave to Inez contained the rest of their cash. The two get in a fight and go their separate ways. Narrating a letter to his sister, Anthony says he and Bob have settled into a routine that is keeping them busy. Dignan, who has joined Mr. Henrys gang, tracks Anthony down and they reconcile. Dignan invites Anthony into a job with Mr. Henry and Anthony accepts on the condition that Bob is allowed in. The trio meet the eccentric Mr. Henry and plan to rob a safe at a cold storage facility. Mr. Henry becomes a role model for the trio, standing up to Bobs abusive brother and tutoring Dignan on success. He invites the trio to a party at his house and visits the group at the Mapplethorpes house, which he compliments. Anthony learns of Inezs love for him and contacts her. She has learned some English and the two rekindle their relationship.
 nuthouse and now Im in jail?" as he walks back into the prison.

==Cast==
* Luke Wilson as Anthony Adams
* Owen Wilson as Dignan (billed as Owen C. Wilson)
*Robert Musgrave as Bob Mapplethorpe James Caan as Mr. Abe Henry
* Lumi Cavazos as Inez
* Ned Dowd as Dr. Nichols
* Shea Fowler as Grace
* Haley Miller as Bernice Andrew Wilson as Jon Mapplethorpe / Future Man
* Brian Tenenbaum as H. Clay Murchison
* Stephen Dignan as Rob
* Anna Cifuentes as Carmen
* Kumar Pallana as Kumar
* Leslie Mann (uncredited) as Sorority girl

==Production== Fort Worth, and Hillsboro, Texas.   
The scenes at Bob Mapplethorpes house were filmed at the John Gillin Residence, designed by Frank Lloyd Wright. {{cite web| last = Neilson| first = 
Charlotte | title = Bottle Rocket 1996 - The Gillin Residence | publisher = Casting Architecture | url = http://www.castingarchitecture.com/2012/12/05/bottle-rocket-1996-the-gillin-residence/| accessdate = 2014-07-22}} 

==Reviews==
Bottle Rocket received generally positive reviews from film critics. As of March 2014, it maintains an 84% "fresh" rating with critics and an 80% with audience on Rotten Tomatoes,  while  on Metacritic it has a 65/100 weighted average score with critics, and a 7.7 user score, which both translate to "generally favorable reviews". 

==Home media==
 
On November 25, 2008, Bottle Rocket was released on DVD and Blu-ray Disc|Blu-ray as part of The Criterion Collection. This is Andersons fourth film to be released in the collection after Rushmore (film)|Rushmore, The Royal Tenenbaums and The Life Aquatic with Steve Zissou.  The original short film it was based on is included as a special feature. 

==See also==
 
* Bottle Rocket (soundtrack)

==References==
 

==External links==
 
*  
*  
*  
*  
*  
* Screenplay by Wes Anderson and Owen Wilson  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 