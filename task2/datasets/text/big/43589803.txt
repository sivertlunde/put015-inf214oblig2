Lucky Jim (1909 film)
{{infobox film
| title          = Lucky Jim
| image          =
| imagesize      =
| caption        =
| director       = D. W. Griffith
| producer       = American Mutoscope and Biograph Company
| writer         = Stanner E. V. Taylor
| starring       = Marion Leonard
| cinematography = Arthur Marvin Billy Bitzer
| editing        =
| distributor    = Biograph Company
| released       = April 26, 1909
| runtime        = 5 minutes
| country        = USA
| language       = Silent
}}
Lucky Jim is a 1909 short film directed by D. W. Griffith. It was produced by the Biograph Company and starred Marion Leonard and Mack Sennett.  

==Cast==
*Marion Leonard - Gertrude
*Mack Sennett - Jack
*Barry OMoore - Jim, the First Husband (*under his name Herbert Yost)
*Anita Hendrie - The Mother
*David Miles - The Father
*Harry Solter - Jims Friend

also uncredited
*Linda Arvidson - Wedding Guest
*Billy Bitzer
*John R. Cumpson - Wedding Guest
*Francis R. Grandon - 
*Grace Henderson - 
*Charles Inslee - Wedding Guest
*Arthur V. Johnson - Wedding Guest
*Florence Lawrence - Wedding Guest
*Owen Moore - Wedding Guest

==References==
 

==External links==
* 
* 
*  available for free download at  

 

 
 
 
 
 
 
 
 


 