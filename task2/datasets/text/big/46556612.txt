Acapulco Gold (film)
{{Infobox film
| name           = Acapulco Gold
| image          = 
| caption        = Theatrical release poster
| director       = Burt Brinckerhoff
| producer       = Allan F. Bodoh Mitchell Cannold Bruce Cohn Michael Leone Joel Tator
| screenplay     = Don Enright OBrian Tomalin
| story          = David Lees Stan Berkowitz
| starring       = Marjoe Gortner Robert Lansing Ed Nelson John Harkins Randi Oakes Lawrence P. Casey
| music          = Craig Safan Gregory Prestopino John Caper Jr.	
| cinematography = Robert Steadman
| editing        = John Wright
| studio         = Bruce Cohn Productions Mar Vista Productions
| distributor    = American Cinema Releasing R.C. Riddell and Associates
| released       =  
| rating         = PG
| country        = United States
| runtime        = 88 minutes  approx. 
| language       = English
| budget         = 
| gross          = 
}}
Acapulco Gold is a 1978 drug culture film starring Marjoe Gortner and Randi Oakes. 

==Plot==
While marijuana growers pick their crop, and use a boat to smuggle in order it on a truck today. In the meantime an ordinary insurance salesman named Ralph Hollio (Marjoe Gortner) has arrived at the airport.

==Cast==
{|class="wikitable"
|-
! Actor !! Role
|-
| Marjoe Gortner || Ralph Hollio
|-
| Robert Lansing || Captain Carl Solborg
|-
| Ed Nelson || D.E.A. Hollister
|-
| John Harkins || Morgan Frye
|-
| Randi Oakes || Sally 
|-
| Lawrence P. Casey || Gordon
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 