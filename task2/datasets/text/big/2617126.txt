Get Rich or Die Tryin' (film)
{{Infobox film
| name = Get Rich or Die Tryin
| image = Get rich or die tryin.jpg
| director = Jim Sheridan
| writer = Terence Winter Curtis "50 Cent" Jackson Terrence Howard Joy Bryant  Bill Duke Adewale Akinnuoye-Agbaje Omar Benson Miller Viola Davis
| music = Quincy Jones Gavin Friday Maurice Seezer
| cinematography = Declan Quinn Roger Barton Conrad Buff Paul Rosenberg Jim Sheridan
| studio = MTV Films G-Unit Films Interscope Records|Interscope/Shady/Aftermath Films
| distributor = Paramount Pictures
| released =  
| runtime = 117 minutes
| language = English
| budget = $40,000,000
| gross = $46,442,528
}}
Get Rich or Die Tryin  is a 2005 American  -nominee Jim Sheridan.

== Plot ==
 Curtis "50 Cent" Jackson) is a quiet young boy who adores his loving mother (Serena Reeder), and the two live a relatively comfortable life as his mother is a local drug dealer.  She often has to leave him with his grandparents to be looked after while she takes care of her business. After she is brutally murdered in an apparent drug deal gone wrong, Marcus heads down the wrong road himself. 

Forced to live with his grandparents full-time, they themselves also having children to look after, Marcus finds his life less appealing as his grandfather works long hours to support the large family.  As he grows older, he rejects the idea of legal work and decides to deal drugs, buying new clothing and even a gun.  Eventually he abandons high school to sell drugs for local kingpin Levar (Bill Duke) and his underling, Majestic, (Adewale Akinnuoye-Agbaje) full-time. Majestic, however, has plans of his own to become a major drug lord himself.
 Roman general and Emperor, with Bama as his manager and producer.

Marcus and his drug crew end up robbing a local shop, and Majestic doesnt want to let him go, resulting in a tragedy that might have destroyed his life. Marcus is gunned down outside of his familys home by Majestics associate, Justice (Tory Kittles), who secretly works for Majestic, posing as Marcus friend just to obtain information and report back to Majestic. 

The shooting leads Marcus to rethink his life and put his priorities in order, including that of his young child. Angered that he failed to kill Marcus, Majestic brutally murders Justice with a sword cane. Shortly after, Marcus meets with Levar, who remorsefully reveals that he is his biological father, and regrets not being there for him and his mother.

Marcus begins preparations to go on stage and begin his walk of becoming a top-notch and real entertainer, donning a bulletproof vest for his protection from his enemies. In the moments before the show, he gains the ire of Majestic, who comes to him with a revelation that he was the one who murdered his mother years earlier. A fight ensues and Marcus leaves as the victor. Finally at peace with his inner demons, leaves Majestic in the hands of his cronies. As he walks out towards the crowd he stops as he hears a lone gunshot in the room behind him, implying Majestic is now dead, which is confirmed as the film cuts to Bama shooting up Majestics corpse.

As Marcus steps onto the stage to perform for the waiting crowd, he removes the bulletproof vest he had on.

== Cast ==
 Curtis "50 Cent" Jackson as Marcus Greer
** Marc John Jefferies as Young Marcus Greer
* Terrence Howard as Bama
* Adewale Akinnuoye-Agbaje as Majestic
* Joy Bryant as Charlene
* Omar Benson Miller as Kyrl
* Tory Kittles as Justice
* Ashley Walters as Antoine
* Viola Davis as Grandma
* Sullivan Walker as Grandpa
* Serena Reeder as Katrina
* Bill Duke as Levar
* Mpho Koaho as Junebug
* Russell Hornsby as Odell Joseph Pierre as Uncle Deuce
* Ryan Allen as Uncle Roy
* Ben Walsh as 15 Euro
* Mykelti Williamson as Charlenes step-father (uncredited)

== Soundtrack ==
 
The soundtrack was released on November 8, 2005. In December 2005, the Recording Industry Association of America (RIAA) certified the album RIAA certification|platinum.  The album has so far sold over 3 million copies worldwide.

== Controversy ==
 rap and 2006 film Home of the Brave.

== Reception ==

Reception to Get Rich or Die Tryin by critics was poor; it holds a 16% rating at Rotten Tomatoes based upon 117 reviews.  Radio Times criticized the film, saying that "as a vehicle for hip-hop superstar Curtis 50 Cent Jackson, this   runs out of gas a fair few kilometres short", giving it a "could be worse" rating of 2/5 stars.  CinePassion stated that "  Sheridans surface vividness is applied around a vacuum."  

FilmFocus was harsh, saying that the films "real danger is that it sets a precedent for the director; if the price is right hes on board."  The BBC was not entirely impressed with the film, saying that "while it boasts a first-class director and is loosely based on the singers own life-story, the results leave you feeling a little short-changed." 

Jonathan Ross gave a positive review, calling Get Rich or Die Tryin  "gripping" and suggesting that it had "excellent performances".  Roger Ebert also praised the film, giving the film a 3 out of 4 rating and saying that it was "a film with a rich and convincing texture, a drama with power and anger". 

Get Rich or Die Tryin  grossed $12,020,807 in its opening weekend. Altogether, the film grossed $46,442,528 in total worldwide. 

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 