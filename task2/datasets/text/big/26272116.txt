Julie Walking Home
{{Infobox film
| name           = Julie Walking Home
| image          = JulieWalkingHome2002Cover.jpg
| caption        = German DVD Cover
| director       = Agnieszka Holland
| writer         = 
| narrator       = 
| starring       = Miranda Otto
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       =  
| runtime        = 113 minutes
| country        = Germany
| language       = English
| budget         = 
| gross          = 
}}
Julie Walking Home is a 2002 drama film directed by Agnieszka Holland. It stars Miranda Otto and William Fichtner. It won an award at the 2003 Method Fest. 

==Plot==
Julie finds her husband Henry in bed with another woman when she returns home early from a trip with their twins, Nicholas and Nicole (who have an amazing bond, believe in magic and even have their own language). When she discovers that her son has lung cancer, Julie seeks help from a faith healer in Poland. A romantic relationship develops between Julie and Alexei (the healer). After Nicholas is cured, Alexei pays Julie a visit in Canada and they begin a relationship. Nicholas gets sick again and Alexei is unable to cure him. By choosing love, Alexei has lost his gift. Julie is pregnant (from Alexei) and she and her husband reunite, both resigned about the tragic fate of their son and trying to make the best out of the situation for their daughters sake. In the twins magical world, death is certainly not the end, we find out in the last scene.

==Cast==
*Miranda Otto as Julie Makowsky
*William Fichtner as Henry
*Lothaire Bluteau as Alexei
*Ryan Smith as Nicholas
*Bianca Crudo as Nicole

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 