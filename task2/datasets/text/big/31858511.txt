Daivathinte Vikrithikal
 
 
{{Infobox Film
| name           = Daivathinte Vikrithikal
| image          = 
| caption        = 
| director       = Lenin Rajendran
| producer       = 
| screenplay     = M. Mukundan Lenin Rajendran
| based on       =  
| starring       = Raghuvaran Srividya Rajan P. Dev Malavika Avinash
| music          = Mohan Sitara
| cinematography = Madhu Ambat
| editing        = N. Gopalakrishnan
| studio         = Sowparnika Movie Arts
| distributor    =  
| released       =   
| runtime        = 120 minutes
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}} novel of the same name. The film tells the story of Alphonso, a man who chooses to suffer a slow, torturous life in his little village, Mahé, India|Mahe, in preference to fortunes and pleasures away from it. Raghuvaran, Srividya, Rajan P. Dev and Malavika Avinash play the pivotal roles.

Raghuvaran was considered for the prestigious National Film Award for Best Actor, along with Mithun Chakraborty for Tahader Katha, but Mithun Chakraborty won the award. 

==Awards==
Kerala state film awards 1992
*  Kerala State Film Award for Best Film
*   
*   
*  Kerala State Film Award for Best Costume Designer:Danda Pani

==Plot==
The story begins in 1954, when the French, the colonial rulers were packing off from Mahé, India|Mahé, a coastal town in North Malabar (Northern Kerala)|Malabar, after 230 years, leaving behind remnants of a cultural history. Those, who considered themselves as belonging to Francophone culture, jumped onto the first available vessel to France.

Alphonso ignored the repeated pleas of his wife, Maggi to leave the land, where they no longer "belonged". The new social order became more, suffocating as Alphonsos earnings (as a "magician" of sorts) dwindled. The arrival of their son, Michael, from France revived hopes of a life without poverty, but Michael went back, leaving behind counterfeit gold and plunging the Alphonso family in deeper debts. Daughter Elsies affair with Sasi became a local scandal.

Alphonso decided to leave, but the decision hung in the air. Alphonso looked around in the realization that he cannot tear himself away from Mahé and the river to which he belonged. Mahé was within him even in a society, where he had no reason for the sense of belonging. In a way, the film reveals what is now described as authentic "ethnicity".

==Cast==
* Raghuvaran as Alphonso
* Srividya as Maggi
* Rajan P. Dev
* Malavika Avinash as Elsie
* Thilakan as Landlord 
* Vineeth as Son of landlord
* Sudheesh
* Anil Murali Siddique

==Novel==
  Kendra Sahitya Akademi Award and N. V. Prize. It was translated to English under the title Gods Mischief by Penguin Books in 2002.

==External links==
*  

 

 
 
 