Cry of the Banshee
{{Infobox film
| name     = Cry of the Banshee
| image    = Cry of the Banshee Poster.jpg
| caption  = Theatrical release poster.
| director = Gordon Hessler
| writer   = Tim Kelly Christopher Wicking (screenplay) based on = story by Tim Kelly
| starring = Vincent Price Elizabeth Bergner Hilary Dwyer Essy Persson Patrick Mower Sally Geeson Pamela Fairbrother Hugh Griffith
| producer = Louis M. Heyward Executive Samuel Z. Arkoff James H. Nicholson Gordon Hessler
| editing  = Oswald Hafenrichter
| music    = Les Baxter
| cinematography = John Coquillon
| distributor = American International Pictures 
| released = Jul 29, 1970 (U.S. release)
| runtime  = 91 min. English
|budget = $450,000-$500,000 (est.) Tom Weaver, "Gordon Hessler", Return of the B Science Fiction and Horror Heroes: The Mutant Melding of Two Volumes of Classic Interviews 2000 McFarland, p 148 
| gross = $1,306,000 (US/ Canada rentals) 
}}
 1970 horror film directed by Gordon Hessler, starring Vincent Price as an evil witchhunter.  The film was released by American International Pictures. The film costars Elizabeth Bergner, Hilary Dwyer, and Hugh Griffith. 

The title credit sequence was animated by Terry Gilliam.

== Plot == The Bells.  

Lord Edward Whitman (Vincent Price) is hosting a great feast when suddenly two poor and ragged-looking little children enter the great hall. A burst of wolf-like howling from outside the walls warns that they may be "devil-marked." The lord decides to kill them in spite of the risk. As his shifty eldest son Sean (Stephan Chase) sleeps with his fathers pretty young wife Lady Patricia (Essy Persson), Lord Whitman begins mumbling that he wants to "clean up" the witches in the area.

Assisted by his two older sons, Whitman goes hunting in the hills for witches. His armed posse breaks up what is apparently meant to be a witches Black Sabbath.  He kills several of them, and tells the rest to scatter to the hills and never return.  This makes the leader of the coven, Oona (Elizabeth Bergner), extremely angry. To get revenge on the Whitman clan Oona calls up a magical servant, a "sidhe", to destroy the lords family.  Unfortunately, the demonic beast takes possession of the friendly, decent young servant, Roderick (Patrick Mower), that free-spirited Maureen Whitman (Hilary Dwyer) has been in love with for years. The servant turned demon begins to systematically kill off members of the Whitman family, until only Lord Whitman and his daughter are left.

Meanwhile, Harry (Carl Rigg), Whitmans son from Cambridge, and Father Tom (Marshall Jones) find Oona and kill her. Maureen shoots the demon in the head with a blunderbuss, apparently killing him.
 Andrew McCulloch), the coachs driver, was murdered by Roderick, who is now driving the coach.

The film ends with Whitman screeching his drivers name in terror, as the coach heads for parts unknown.

The titular "cry of the banshee" is the signal that someone will die, but this is a Celtic legend about a type of ghost, and has nothing to do with satanism - no banshee appears in the film.

== Trivia ==
* The film was played at the first Quentin Tarantino Film Festival in 1997 at the Dobie residence hall near the University of Texas.

* Is mentioned in the Rob Zombie song Demonoid Phenomenon, from his 1998 album Hellbilly Deluxe.

* The opening credits were created by Terry Gilliam.

* The film was promoted with a poem that went:

"Who spurs the beast the corpse will ride? 
"Who cries the cry that kills? 
"When Satan questioned, who replied? 
"Whenst blows this wind that chills? 
"Who walks amongst these empty graves 
"And seeks a place to lie? 
"Tis something God neer had planned, 
"A thing that neer had learned to die."

This verse was spuriously attributed to Edgar Allan Poe.

==Production==
Gordon Hessler did not like Tim Kellys original script and hired Chris Wicking to rewrite it. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p279  Hessler says he would have got Wicking to change it further and improving the witch characters - but AIP would not let him. The original music score was composed by Wilfred Josephs but AIP decided not to use it, commissioning a score by Les Baxter instead. Elisabeth Bergner plays a small rôle; it was her first appearance in a film in 29 years. 

==Release==
The US theatrical release featured the GP rated print which replaced the opening animated credits with still ones, completely altered the music score, and was cut to remove all footage of topless nudity and to tone down assorted whippings and assault scenes. This print was also used for the original UK cinema release in 1970.
The film was a commercial success but Hessler was dissatisfied with it and calls it the least interesting of the four movies he made for AIP. 

==Home Video Release==
 
In April 1991, Cry of the Banshee was packaged as a Laserdisc double feature, paired with the first of the Count Yorga movies, Count Yorga, Vampire. Both films were not letterboxed, but employed a full screen, pan-and-scan process.

The 1988 UK Guild video release featured the same heavily edited print as the US and UK cinema ones. All DVD releases however have featured the full uncut version.

==References==
 

== External links ==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 