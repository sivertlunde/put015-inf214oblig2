The Four Horsemen of the Apocalypse (film)
 
{{Infobox film
| name        = The Four Horsemen of the Apocalypse
| image       = Four Horsemen of the Apocalypse Poster.jpg
| image_size  = 225px
| caption     = Metro Pictures poster for the film (1921)
| screenplay  = June Mathis
| based on    =   Rex Ingram
| producer    = Rex Ingram
| starring    = Pomeroy Cannon Josef Swickard Bridgetta Clark Rudolph Valentino Wallace Beery Alice Terry
| music       = Louis F. Gottschalk
| studio      = Rex Ingram Productions
| distributor = Metro Pictures
| released    =  
| runtime     = 134 minutes (edited version) 156 minutes (complete version)
| country     = United States Silent English intertitles
| budget      =
| gross       = $4.5 million  
}}
 silent epic epic war Metro Pictures Rex Ingram. The Four Horsemen of the Apocalypse by Vicente Blasco Ibáñez, it was adapted for the screen by June Mathis. The film stars Pomeroy Cannon, Josef Swickard, Bridgetta Clark, Rudolph Valentino, Wallace Beery, and Alice Terry. 
 The Kid, and going on to become the sixth-best-grossing silent film of all time.  The film turned then-little-known actor Rudolph Valentino into a superstar and associated him with the image of the Latin Lover. The film also inspired a tango craze and such fashion fads as gaucho pants.   The film was masterminded by June Mathis, who, with its success, became one of the most powerful women in Hollywood at the time. Alt Film Guide.  Journal of Humanities. 2007. 

In 1995, The Four Horsemen of the Apocalypse was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". The film is now in the public domain, having been made before 1923. A DVD version was released in 2000 but is now out of print. The film is now available for free download on the Internet Archive. 

==Plot==
 
 Argentine landowner, German son-in-law French one tango sequence occurs. A man and a woman (Beatrice Dominguez) are dancing the tango. Julio strides up and asks to cut in. The woman stares at Julio alluringly. The man brushes him off, and they resume dancing. Julio then challenges the man and strikes him, knocking him into some tables and out of the scene. Julio and the woman then dance a dramatic version of the tango that brings cheers from the people in the establishment. Following the dance, the woman sits on Julios lap. Madariaga then slides to the floor, drunk. The woman laughs at Madariaga. Julio casts her aside in scorn and helps his grandfather home.

Sometime later, Madariaga dies. The extended family breaks up, one half returning to Germany and the other to France.

In Paris, Julio enjoys a somewhat shiftless life as a would-be artist and sensation at the local tea dances. He falls in love with Marguerite Laurier (Alice Terry), the unhappy and much younger wife of Etienne Laurier, a friend of Julios father. The affair is discovered, and Marguerites husband agrees to give her a divorce. It seems as though Julio and Marguerite will be able to marry, but both end up getting caught up in the Great War.

Marguerite becomes a nurse in Lourdes. The bravery of Etienne is reported, and he is blinded in battle. Etienne happens to end up at the hospital where she is working, and  Marguerite attends to him there. Julio travels to Lourdes to see Marguerite and instead sees her taking care of Etienne. Julio, ashamed of his wastrel life, enlists in the French Army.
 German Army Miracle of the Marne".
 trenches on the front. During a mission in no mans land, he recognizes his German cousin. Moments later, they are both killed by a shell. Back in Paris, Marguerite considers abandoning the blinded Etienne, but Julios ghost guides her to continue her care for him. Both families mourn for their fallen sons as the film ends.

 

==Cast==
  
* Pomeroy Cannon as Madariaga
* Josef Swickard as Marcelo Desnoyers
* Bridgetta Clark as Doña Luisa
* Rudolph Valentino as Julio Desnoyers
* Virginia Warwick as Chichí Alan Hale as Karl von Hartrott
* Mabel Van Buren as Elena
* Stuart Holmes as Otto von Hartrott
* John St. Polis as Etienne Laurier
 
* Alice Terry as Marguerite Laurier
* Mark Fenton as Senator Lacour
* Derek Ghent as René Lacour
* Nigel De Brulier as Tchernoff
* Bowditch M. Turner as Argensola
* Edward Connelly as Lodgekeeper
* Wallace Beery as Lieut. Col. von Richthosen
* Harry Northrup as The General
* Arthur Hoyt as Lieut. Schnitz
 

==Production==
In 1919, screenwriter June Mathis became head of the scenario department for Metro Pictures. Maher. 2006. p.200   With this position, she became one of the first female executives in film history.  Holding a major belief in Spiritualism and the Book of Revelation, Mathis was determined to turn Vicente Blasco Ibáñezs novel The Four Horseman of the Apocalypse into a film. The book had been a best seller, but most studios found it impossible to adapt to film. 

Mathiss adaptation so impressed the studio, they asked for her input in director and star. For director, she chose   and Virginia Rappe that would eventually become The Isle of Love. It has been suggested Mathis might have first seen him in that film, as she was a close friend of Eltinge. 

Mathis insisted Valentino would play Julio; however, studio executives were nervous with the young actor. Valentino, whose parents were French and Italian, had a distinctly Latin look that had not been used much in pictures at that time. Leider, Emily W., Dark Lover. pp. 61-85.  However, Mathis got her way, and after seeing the rushes, she and Ingram decided to expand the role of Julio to showcase the talents of Valentino. Valentino had worked as a taxi dancer during his time in New York. To show off his dancing skills, the tango scene was included, though it was not part of the original story.

Alice Terry, a former Follies Girl,  was cast as Julios lover, Marguerite. She would marry Ingram that same year.
 lip readers. Valentino was fluent in French, as his mother was French. Leider, 2004. 

Mathis also injected some early depictions of alternative lifestyles; it featured a scene with German officers coming down the stairs in Drag (clothing)|drag. Of the scene, Mathis would later tell the Los Angeles Times, "I had the German officers coming down the stairs with womens clothing on. To hundreds of people that meant no more than a masquerade party. To those who have lived and read, and who understand life, that scene stood out as one of the most terrific things in the picture." 

==Reception==
The film premiered in New York to great critical acclaim. Many critics hailed it as a new Birth of a Nation. However, the German press was less enthused with the portrayal of Germans in the film.

The film became a commercial success as well, and was one of the first films to make $1,000,000 at the box office. The film is considered to be the sixth-best-selling silent film of all time.     During its initial run, it grossed $4,500,000 domestically.

With its extended scenes of the devastated French countryside and personalized story of loss, The Four Horsemen of the Apocalypse is often considered to be one of the first anti-war films made.
 Pennsylvania board, upon reviewing the affair between Julio and Marguerite, required that Marguerite be described in intertitles as being the Engagement|fiancée of Etienne Laurier rather than his wife. 

The film made Mathis one of the most powerful and respected women in Hollywood, said to be only second to Mary Pickford.   She was one of the highest-paid executives of her time and went on to work with Famous Players-Lasky and Goldwyn Pictures. LA Times, 1923.   She became known for association with Valentino, who became a close friend. She wrote many more films for him, helping to shape his image. 

Julio proved to be a breakthrough role for Valentino, who became a superstar overnight. He became heavily associated with the image of a "Latin lover", though eventually his image as "The Sheik" may have overshadowed this. Metro refused to acknowledge that they had made a star and immediately put him into a B movie|B-picture titled Uncharted Seas.  Leider, Emily W., Dark Lover. pp. 131-150.   Valentino soon left them for Famous Players-Lasky. 

The film also helped launch the name of Ingram. Ingram came to resent the break-out success of Valentino, as he felt it was his own work that made Four Horsemen a success. He went on to make films with Terry and eventually discovered Ramon Novarro, whom he promoted as the new Valentino. 

==Adaptations and remakes== 4 Horsemen of the Apocalypse (1962), with the setting changed to World War II. Vincente Minnelli was the director.

==In popular culture==
*The tango sequence was parodied by Gene Wilder during the opening credits of The Worlds Greatest Lover (1977).

==See also==
*List of highest-grossing films
*National Film Registry

==References==
Notes
 

Bibliography
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 