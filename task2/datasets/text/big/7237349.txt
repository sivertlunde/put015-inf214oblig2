Azhagi
{{Infobox film
| name           = Azhagi
| image          = Azhagi.jpg
| director       = Thangar Bachchan   
| writer         = Thangar Bachchan Vivek
| producer       = Udhaya Geetha
| cinematography = Thangar Bachchan
| editing        = B. Lenin V. T. Vijayan
| music          = Ilaiyaraaja
| studio         = Uthaya Geetha Productions
| distributor    = Uthaya Geetha Productions
| released       = 14 January 2002
| Manager        = Krishnagiri A.Arjunan
| runtime        = 155 minutes
| country        = India
| language       = Tamil
}}
 Tamil romance romantic drama film directed by Thangar Bachchan. The film is critically acclaimed and stars Parthiban, Nandita Das and Devayani.  The films score and soundtrack are composed by Ilaiyaraaja. The movie was remade in Telugu as Letha Manasulu (2004) starring Srikanth, Kalyani and Gopika in lead roles.

==Plot==
Shanmugam (Parthiban), a veterinary doctor, lives in the city with his wife Valarmati(Devayani) and their two children. In school, Shanmugam was in love with his classmate Dhanalakshmi but fate had forced them to go their separate ways, with Dhanalakshmi (Nandita Das) being forced to wed her brother-in-law. One day, Shanmugam spots Dhanalakshmi, who, having lost her husband, now lives a life of poverty on the platforms with her son, Balu. After an unsuccessful attempt to find her a job in a friends house, he hires her as their servant-maid. However, memories of the past starts to create a tension between Shanmugam and Dhanam, despite their attempts to maintain a distance.

One day, Valarmati finds out from Shanmugams old classmates how they both were in love when they were young and she starts fearing that Shanmugam will leave her and her children for Dhanam. Valarmati becomes so distraught that she even humiliates Dhanam at a party organized by one of their friends. When they return home, Valarmati confronts Shanmugam and an argument ensues. Unfortunately, Dhanam overhears their argument and silently goes to bed. The next morning, Dhanam and Balu are nowhere to be found. When Shanmugam searches the house, he finds a letter written by Dhanam saying that she wants Valarmati and Shanmugam to be happy and that she doesnt want to interfere. Soon, Valarmati realises the truth and wants to bring Dhanam back home and ask her for her forgiveness.

Shanmugam searches high and low for them and at last finds Balu in an orphanage. The matron informs them that his mother had left instructions that her son should remain at the orphanage till her return. However, when the matron questions Balu, he replies that he wants to go with Shanmugam and stay in their house and to tell his mother that he is there when she returns. The matron agrees and lets him go. At the beginning of the movie, it is mentioned that Balu has been adopted by Shanmugam but still continues to call him "Sir" and never "Father or "Dad". As Parthiban leaves for home from the orphanage, he mentions that he is still searching for the whereabouts of Dhanalakshmi to this day.

==Cast==
*Parthiban as Shanmugam (Veterinary Doctor)
*Devayani as Valarmati
*Nandita Das as Dhanalakshmi aka Dhanam Vivek as Veterinary Doctor Desigan Pandu as Assistant to veterinary doctor, Minister
*Loose Mohan as Pandus father-in-law
*Pyramid Natarajan

==Soundtrack==

The soundtrack features songs composed by Ilaiyaraaja, with lyrics by Palani Bharathi
*Paattu Solli Paada Solli  - Sadhana Sargam

*Damakku Damakku Dum - Bhavatharini|Bhavatharani, Chorus

*Un Kuthama En Kuthama  - Ilaiyaraaja

*Oru Sundari Vandhalam  - P. Unni Krishnan, Sadhana Sargam, Malgudi Subha
 Bhavatharani

*Kuruvi Kodanja - Pushpavanam Kuppusamy, Swarnalatha

==Awards==
*Sadhana Sargam awarded National Film Award for Best Female Playback Singer in 2002 for the song "Pattu Solli".

==References==
 

==External links==
* 
* 
* 
 
 

 
 
 
 
 
 
 