Sleeping with Strangers
{{Infobox Film
| name = Sleeping with Strangers
| image =
| image_size =
| caption =
| director = William T. Bolson
| producer = Joan Carr-Wiggin
| writer = Joan Carr-Wiggin
| starring = Adrienne Shelly Alastair Duncan Kim Huffman Shawn Thompson Scott McNeil
| editing = Richard Benwick
| released = 1994 English
| budget = $2,500,000
}}
 1994 Romantic romantic comedy.  It was directed by William T. Bolson and written by Joan Carr-Wiggin. The movie stars Adrienne Shelly, Alastair Duncan (credited as Neil Duncan), Kim Huffman, Shawn Thompson, and Scott McNeil.  It was filmed around Victoria, British Columbia|Victoria, British Columbia, Canada with a budget of $2.5 million.   

==Plot==
A popular actress (Shelly) and a rock star (McNeil) come to a small Canadian town with two competing hotels next door to each other.  The rock star is escorted into one of the hotels and the actress checks into the other.  Daniel (Duncan), the owner of one of the two hotels, is trying to stay afloat.  The other hotel owner, Mark (Thompson), is trying to steal away Daniels business and his fiancee (Huffman).  The paparazzi arrives in town and makes everybody wonder, who is sleeping with whom?

==Cast==
* Adrienne Shelly as Jenny Dole
* Alastair Duncan as Daniel (credited as Neil Duncan)
* Kim Huffman as Teri
* Shawn Thompson as Mark
* Scott McNeil as Todd Warren Gary Jones as Loan Officer
* Anthony Ulc as Sam
* Claire Caplan as Elsie
* Betty Linde as Margaret

==References==
 

==External links==
*  
*  

 
 
 
 
 


 