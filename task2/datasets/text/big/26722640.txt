The Empress Wu Tse-tien (1939 film)
{{Infobox film
| name           = The Empress Wu Tse-Tien
| image          = 
| image_size     = 
| caption        = 
| director       = Fang Peilin
| producer       = Zhang Shankun
| writer         = Ke Ling
| narrator       = 
| starring       = Gu Lanjun Yin Xiucen Huang Naishuang Li Ming Liang Xin
| music          = Huang Yijun
| cinematography = Yu Xingsan
| editing        = Xu Ming
| studio         = Xinhua Film Company
| distributor    = 
| released       = 
| runtime        =  China
| language       = Mandarin
| budget         = 
| gross          =
}}
The Empress Wu Tse-Tien ( ) is a 1939 Chinese historical film based on the life of Wu Zetian, the only female emperor in Chinese history. Directed by Fang Peilin, the film starred Gu Lanjun as the titular character.

== Cast ==
* Gu Lanjun
* Yin Xiucen
* Huang Naishuang
* Li Ming
* Liang Xin

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 

 