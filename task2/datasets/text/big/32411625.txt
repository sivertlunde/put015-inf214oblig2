This Angry Age
{{Infobox film
| name           = This Angry Age
| image          = This Angry Age.jpg
| caption        = Promotional poster
| director       = René Clément
| producer       = Dino De Laurentiis
| writer         = Marguerite Duras (novel) Diego Fabbri Irwin Shaw
| starring       = Anthony Perkins Silvana Mangano Richard Conte Jo Van Fleet
| music          = Nino Rota
| cinematography = Otello Martelli	 Leo Cattozzo		
| distributor    = 
| released       =  
| runtime        = 105 min
| country        = Italy USA France
| language       = English
| budget         =
| gross          =
}} The Sea Wall. The film stars Anthony Perkins and Silvana Mangano. The original novel was adapted again in 2008 by Rithy Panh as The Sea Wall, starring Isabelle Huppert.

==Plot==
Twenty-year-old Joseph (Perkins) and  his sixteen-year-old sister  Suzanne (Mangano) live in the merciless conditions of an intemperate foreign land with their widowed mother (Van Fleet). Their mother attempts to exert a hold on her children by involving them in the familys run-down rice plantation. However the siblings seek liberation, and look for this in their romantic lives. Suzanne becomes involved with Michael (Conte) and Joseph finds a love interest in Claude (Valli). 

==Cast==
*Anthony Perkins as Joseph Dufresne
*Silvana Mangano as Suzanne Dufresne
*Richard Conte as Michael
*Jo Van Fleet as Mme. Dufresne
*Alida Valli as Claude
*Nehemiah Persoff as Albert
*Yvonne Sanson as Carmen
*Guido Celano as Bart

==Production==
Clément purchased the film rights to the Duras novel in 1956. The original male lead was supposed to have been James Dean, but he was replaced by Perkins. Mangano, the wife of the films producer, was cast in the female lead.    

Clément shot the film in wide-screen Technirama and Technicolor. He was unable to film in Indochina, the setting of the original novel, as it no longer existed. Nor could he film in the newly independent Vietnam, as nationalist struggles continued there. Thus, Clément reconstructed the storys setting in Thailand. 

==Reception==
The New York Times described Clément as "a specialist in that sort of tragedy that evolves from the inability of deeply pained people to face their own feelings." The reviewer also praised the "great pictorial beauty and admirable psychological truth" of Cléments film. The reviewer also praised how the "crumbling of the dam against the assaults of the river stands as an image of what is going on within the family." 

The film was also a critical success in France, being lauded as "a complete success, a chef-dœuvre", although François Truffaut did not share this enthusiasm, accusing  Clément of directing "his career". He added that "For Clément, the essential thing is that the film he is making costs more than the last one and less than the next." 

Duras was dismayed by the absence of certain colonial themes that were important in her novel, yet absent in the film. She said she felt "betrayed" and "dishonoured" by the film. 

==References==
 

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 