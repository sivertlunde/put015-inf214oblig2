Needhikkuppin Paasam
{{Infobox film
| name           = Needhikkuppin Paasam
| image          = Needhikku pin paasam.jpg
| director       = M. A. Thirumugam
| writer         = Aarur Dass
| story          = Aarur Dass
| starring       = M. G. Ramachandran B. Saroja Devi M. R. Radha S. V. Ranga Rao Pasupuleti Kannamba|P. Kannamba M. N. Nambiar S. A. Ashokan
| producer       = Sandow M. M. A. Chinnappa Thevar 
| music          = K. V. Mahadevan
| cinematography = N. S. Varma
| editing        = M. A. Thirumugam M. G. Balu Rao M. A. Mariappan
| studio         = Devar Films
| distributor    = Devar Films
| released       =   
| runtime        = 150 mins
| country        =   India Tamil
| budget         = 
}}
 1963 Tamil language drama film directed by M. A. Thirumugam. The film features M. G. Ramachandran, B. Saroja Devi and M. R. Radha in lead roles. 
The film, produced by Sandow M. M. A. Chinnappa Thevar, had musical score by K. V. Mahadevan and was released on 15 August 1963.  The film was super hit at box-office and ran more than 100 days. 

==Plot==
Gopal (M.G.R.) is a honest lawyer who holds law & order above family and emotions. He meets a simple village girl named Gauri (Saroja Devi) and falls in love with her. But trouble brews in the form of Muthayya (Nambiar) who sets his wicked eyes on Gauri. In order to marry Guari, Muthayya murders her father (M.R. Radha) and Gopals mother is framed for the murder. Struggling between family and justice, can Gopal save his mother? Will he triumph in his love?

==Cast==
* M. G. Ramachandran
* B. Saroja Devi
* M. R. Radha
* S. V. Ranga Rao
* Pasupuleti Kannamba|P. Kannamba
* M. N. Nambiar
* S. A. Ashokan
* Sandow M. M. A. Chinnappa Thevar
* G. Sakunthala
* S. M. Thirupathisamy
* Radha
* Sasirekha

==Crew==
*Director: M. A. Thirumugam
*Producer: Sandow M. M. A. Chinnappa Thevar
*Production Company: Devar Films
*Music: K. V. Mahadevan
*Lyrics: Kannadasan
*Story: Aarur Dass
*Screenplay: 
*Dialogues: Aarur Dass
*Cinematography: N. S. Varma
*Editing: M. A. Thirumugam, M. G. Balu Rao & M. A. Mariappan
*Art Direction: A. K. Ponnusamy
*Choreography: S. M. Rajkumar
*Stunt: Shyam Sundar
*Audiography: T. S. Rangasamy

==Soundtrack==

The music composed by K. V. Mahadevan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:29
|-
| 2 || Idi Idichu Mazhai || T. M. Soundararajan, P. Susheela || 03:05
|-
| 3 || Kaadu Kodutha Kaniyirukku || P. Susheela || 02:54
|-
| 4 || Maanallavo Kangal || T. M. Soundararajan, P. Susheela || 03:44
|-
| 5 || Sirithalum Pothume || T. M. Soundararajan, P. Susheela || 02:54
|-
| 6 || Vaanga Vaanga || P. Susheela || 03:30
|-
| 7 || Ponaley || T. M. Soundararajan || 03:50
|-
|}

==References==
 

==External links==
 

 
 
 
 
 


 