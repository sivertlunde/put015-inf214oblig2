Sam the Man
{{Infobox Film
| name           = Sam the Man
| image          = Sam the Man 2000.jpg
| image_size     =
| caption        = 
| director       = Gary Winick
| writer         = 
| narrator       = 
| starring       = Fisher Stevens
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = 2000
| runtime        = 87 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Sam the Man (2000) is an American film directed by Gary Winick.

==Cast==
*Fisher Stevens	... 	Sam Manning
*Annabella Sciorra	... 	Cass
*Alex Porter	... 	Saxophone Player
*John Slattery	... 	Maxwell Slade
*Annika Peterson	... 	Emily
*Ron Rifkin	... 	Richard
*Saverio Guerra	... 	Lorenzo Pugano
*George Plimpton	... 	Himself
*Griffin Dunne	... 	Man in Bathroom
*Maria Bello	... 	Anastasia Powell
*Danielle Ferland	... 	Ella
*Joshua Dov	... 	College Boy (as Josh Dov)
*Rob Morrow	... 	Daniel Lenz
*Luis Guzmán	... 	Murray (as Luis Guzman)
*Jean-Luke Figueroa	... 	Buster Pugano

==External links==
* 

 

 
 
 
 
 


 