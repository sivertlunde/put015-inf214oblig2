Nomos 4000
{{Infobox film
| name           = Law No. 4000  Nomos 4000
| image          =
| image size     =
| caption        =
| director       = Giannis Dalianidis
| producer       = 
| writer         = Giannis Dalianidis
| starring       = Vasilis Diamantopoulos Zoi Laskari Vangelis Voulgaridis Thanos Papadopoulos Kostas Voutsas Eleni Zafeiriou
| cinematography = 
| photographer   =
| music          = 
| editing        = Petros Licas
| distributor    = Finos Film 1962
| runtime        = 93 min
| country        = Greece Greek
| imdb           = 124437
| cine.gr_id     = 701221
}}
 1962 Greece|Greek drama film directed and written by Giannis Dalianidis and starring Zoi Laskari, Vangelis Voulgaridis and Vasilis Diamantopoulos.  The film was produced by Finos Films.  The title is derived from the Greek Law 4000/1958 about teddy boyism.

==Plot==

Andreas Ikonomou (Vasilis Diamantopoulos) was a strict father and a High School teacher which he taught Giorgos (Vangelis Voulgaridis) . The latter  fell in love with the daughter (and only child) of Andreas, Maria (Zoi Laskari).  An incident in the school results in teachers being aware of the relationship between his student and his daughter, who has an abortion. In another school incident, a student, called Evangelou,  mocks Mr Andreas during the lesson by drawing him with ears of a donkey, causing the teachers fury and the eventual expulsion of the boy from the school. Then, the same student is lured by some thugs to splash yogurt on his teacher. After his action, the perpetrator is caught by police and has his head shaved and be paraded with a sign reading about his offense. 

The movie made 118,841 tickets.

==Cast==

*Zoi Laskari as Maria Economou
*Kostas Voutsas as Renos
*Vangelos Voulgaridis as Giorgos Anagnostou
*Vasilis Diamantopoulos as Andreas Economou
*Eleni Zafeiriou  as Andreas wife
*Spyros Moussouris as Giorgos father
*Hloi Liaskou as Marias friend
*Katerina Gogou  as Marias friend
*Athina Michailidou as Giorgos mother
*Katerina Helmi  as a prostitute
*Thanassis Papadopoulos as student (Evangelou)

==External links==
* 
* 
*   

 
 
 
 

 