Miss Mary
 
{{Infobox film
| name           = Miss Mary
| image          = File:Miss Mary film poster (1986).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster by Steven Chorney 
| director       = María Luisa Bemberg
| producer       = Lita Stantic
| writer         = 
| screenplay     = María Luisa Bemberg Beda Docampo Feijóo Juan Bautista Stagnaro Jorge Goldenberg
| story          = 
| based on       =  
| narrator       = 
| starring       = Julie Christie Nacha Guevara Eduardo Pavlovsky Gerardo Romano Iris Marga Luisina Brando
| music          = Luis María Serra
| cinematography = Miguel Rodríguez
| editing        = Luis César DAngiolillo
| studio         = 
| distributor    = 
| released       = 1986
| runtime        = 100 min
| country        = Argentina
| language       = English Spanish
| budget         = 
| gross          = 
}} American co-production shot on location in Argentina.
==Plot== English and oversees their childhood and adolescent development, at times both nurturing and disciplining them. As they move into their teenage years, the children mature, exploring sexuality and their place in society. 

Meanwhile, the aristocratic world around them is falling apart. The adults converse about politics, hinting at the impending arrival of  ) shoots a pistol blindly into the parlor where the affair is unfolding, and although the bullets cause no injury, the children are greatly disturbed. Mecha slowly deteriorates mentally and emotionally and grows stoically detached in the presence of her family.   

A few years down the road, Teresa, the eldest daughter in the family, proudly announces the loss of her virginity to her younger sister, Carolina. Johnny, their older brother, has a sexual encounter with a woman more than twice his age. Confused by the experience, he rushes home despite a thunderstorm and confronts Mary in her bedroom. After a brief moment of awkwardness, they embrace and spend the night together, capping off a long, vaguely-defined relationship that has displayed both maternal and romantic tendencies. As Johnny sneaks back half-nakedly to his room in the early morning, his mother happens to see him stumble down the hallway. Immediately aware that Miss Mary has gone too far, she relays the information to her indifferent husband before returning to find Mary already packing up her things. She formally dismisses Mary, just as Mary reports that she is planning to leave the family anyway.     

Later, Teresa is shown preparing for her wedding, which she expresses doubts about following through with. Motivated by her support for her sister and her contempt for the shallow world in which she lives, Carolina announces her intentions to boycott the wedding. Mary is present at the wedding, and she slips Johnny a note as he walks down the aisle in a procession of the brides family. Shortly thereafter, Mary converses with Johnny, ultimately revealing that she plans to return to England now that World War II has ended. She boards a cruise liner and departs.

==Note== the 1957 Hindi remake of the Telugu movie Missamma.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 
 