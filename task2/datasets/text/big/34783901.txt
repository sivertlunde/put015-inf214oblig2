Staying Alive (2012 film)
{{Infobox film
| name           = Staying Alive
| image          = Staying Alive.jpg
| alt            =  
| caption        = 
| director       = Anant Mahadevan
| producer       = Pulakesh Bhowmik
| story          = 
| starring       = Anant Mahadevan Saurabh Shukla Chandan Roy Sanyal
| music          = Sanjoy Chowdhury
| cinematography = Rajkumar K.
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Staying Alive is a 2012 Hindi-language social film directed by Anant Mahadevan, featuring Anant Mahadevan, Saurabh Shukla, Chandan Roy Sanyal in the lead roles. The films music was composed by Sanjay Chowdhury.
The film was released on February 3, 2012.

==Plot==
The story is about two men who suffer a heart attack and share the hospital room. Aditya (Ananth Mahadevan) is a newspaper editor who has already survived two strokes in the past and has somewhat adapted to the crisis. Shaukat Ali (Saurabh Shukla), an underworld kingpin who has often witnessed death closely, gets paranoid with his first heart-attack. The story is about their interactions and exchange of ideas in the ICU room that changes Alis perspective towards life.

==Cast==
* Anant Mahadevan
* Saurabh Shukla
* Chandan Roy Sanyal
* Sanjay Swaraj
* Navni Parihar
* Ranjana Sasa
* Khan Jahangir Khan

==Critical Reception==
The film received mixed reviews from critics.Taran Adarsh from Bollywood Hungama gave the film 2/5 and said "On the whole, STAYING ALIVE is more of an experiment that holds appeal for a tiny segment of cineastes -- those with an appetite for meaningful, festival films.".  Avijit Ghosh from Times of India gave it 3/5 stating that "Staying Alive is a subtle yet engrossing human drama." http://timesofindia.indiatimes.com/entertainment/movie-reviews/hindi/Staying-Alive/movie-review/11742715.cms
 

==References==
 

 
 
 
 


 