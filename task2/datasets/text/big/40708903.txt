Chicago Heights (film)
{{Infobox film
| name           = Chicago Heights
| image          = CHICAGO HEIGHTS poster.jpg
| alt            = 
| caption        = 
| director       = Daniel Nearing
| producer       = Sanghoon Lee
| writer         = Daniel Nearing & Rudy Thauberger
| based on       =  
| narrator       = Benny Stewart
| starring       = Andre Truss, Keisha Dyson, Jay Johnson, Benny Stewart
| music          = Raymond Dunlap
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Rogue Arts Films / Vanguard Cinema
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Chicago Heights (alternate title: The Last Soul on a Summer Night) is a 2009 film written and directed by Daniel Nearing. This experimental, non-linear film is an ambitious adaptation of Sherwood Anderson’s short story cycle, Winesburg, Ohio. Noted film critic Roger Ebert included Chicago Heights in his list of the Top Art Films of 2010. 

== Plot ==

Chicago Heights is set in the mind of an old writer (William Gray) as lays on his bed. Sensing his death is imminent, his mind drifts, and he remembers people, but cant discern if they are part of his dreams, his memories, or his inventions.

The reflections relate to the elderly man’s coming of age (Nathan Walker is played as younger by Andre Truss) and his relationship with his mother (Keisha Dyson). He also sees into the lives of others he might have known, like a Chicago Heights church pastor (Gerrold Johnson), a professor (Michaele Nicole), and a psychologist (Benny Stewart) who worked with his mother.

== Cast ==

*Andre Truss - Nathan Walker
*Keisha Dyson - Elizabeth Walker 
*Benny Steward - Narrator / Doctor Reefy 
*Jay Johnson - Reverend Curtis Hartman 
*William Gray - Sherwood Anderson / Older Nathan 
*Ron Jarmon, Jr. - Wash Williams 
*Simone Wilson - Louise Trunnion 
*Terah Jene Weddington - Helen White 
*Michaele Nicole - Kate Swift 
*James Barbee - Dante Hard 
*Raven Reeves - Calla Hard 
*Jason Coleman - The Stranger 
*Brian Harris - Tom Walker 
*Tovah Hicks - Margot Williams 
*Barbara Hogu - Mrs. Swift 
*Homer Talbert - Elizabeth’s Father 
*Sherri Evans - Mrs. Hartman 
*Mercedes Kane - Actress / Snow Angel 
*Leah Shortell - Patient / Snow Angel 
*Lynn Werth - Margot’s Mother 
*Lisa Klein - Dead Elizabeth
*Linsey Savage - Man in Snowstorm 
*Victor Collazo - Man in Forest Under the Church 
*Finnegan Klein-Nearing - Depression

== Factual basis ==

See article on Winesburg, Ohio (the novel)

== Adaptation ==

Because Winesburg, Ohio is collection of inter-related stories and not a linear novel,
Many critics thought it could never be made into a film.  Nearing and writing partner Rudy Thauberger began adapting the book more than a decade before the film was made.

The pair struggled to find a way to adapt the book to fit the conventional three-act film script structure. When it became clear that a straight adaptation wouldn’t work, Nearing decided to draw a line through the book’s key stories by consolidating some characters and taking some carefully considered liberties with the overall story.  One of the biggest changes was moving the period story from rural Ohio to a present day urban setting. Those modifications allowed Nearing to capture the essence of Sherwood Anderson’s sprawling book in a feature length film. 

== Production ==
 University Park, and Chicago.

== Music ==
Chicago Heights music was composed by Minister Raymond Dunlap. Lyrics to "Nobody Knows" and numerous other songs are written by Daniel Nearing and Sherwood Anderson.

== Recognition and honors ==
Recognition and Awards
*Chicago Heights (aka The Last Soul on a Summer Night) 
Best Art Films of 2010 - Roger Ebert
*In Competition - Flash Forward - Busan International Film Festival 
*Best Film in a Fine Arts Discipline - Berlin Black Film Festival
*Official Selection - Black Harvest Film Festival, Gene Siskel Film Center
*Official Selection - Ohio Independent Film Festival
*Short Film Corner (as "Nobody Knows), Festival de Cannes
*Honorable Mention, Narrative Category - Columbus International Film Festival

== References ==

 

 
 
 