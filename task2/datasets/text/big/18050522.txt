Baghead
{{Infobox Film
| name           = Baghead
| image          = Bagheadposter.jpg
| image_size     = 
| caption        = 
| director       = Mark Duplass Jay Duplass
| producer       = Mark Duplass Jay Duplass John E. Bryant
| writer         = Jay Duplass & Mark Duplass
| narrator       = 
| starring       = Ross Partridge Steve Zissis Greta Gerwig Elise Muller
| music          = 
| cinematography = 
| editing        = 
| distributor    = Sony Pictures Classics
| released       = 25 July 2008
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $140,106
| preceded_by    = 
| followed_by    = 
}}
Baghead is a 2008 comedy horror film directed by Jay Duplass and Mark Duplass.  Its limited release began on 25 July 2008.
The film stars Ross Partridge, Elise Muller, Greta Gerwig, and Steve Zissis. 
==Plot==
Four actors, Matt (Ross Partridge), Catherine (Elise Muller), Michelle (Greta Gerwig), and Chad (Steve Zissis) go to a cabin in the woods to write, direct, and act in a film that will jump-start their careers.  Their idea is a horror film about a man with a bag over his head, but what happens when that man mysteriously shows up? The actors suddenly find themselves in a dangerous situation where their lives are put at risk as they try to escape the mysterious bag-head.

==Reception== Michael Phillips At the Movies. The film currently holds a rating of 78% on review aggregator RottenTomatoes, based on 95 reviews.

==External links==
*  }}
*  
*  
*  
*   at Cinefantastique

 

 
 
 
 
 

 