Red Hook Summer
{{Infobox film
| name           = Red Hook Summer
| image          = Red Hook Summer film poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Spike Lee
| producer       = Spike Lee James McBride Spike Lee
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Clarke Peters Nate Parker Thomas Jefferson Byrd Toni Lysaith Jules Brown
| music          = Bruce Hornsby (score) Judith Hill (songs) Jonathan Batiste (Hammond B3 organ music)
| cinematography = Kerwin DeVonish
| editing        = Hye Mee Na 
| studio         = 40 Acres and a Mule Filmworks
| distributor    = Variance Films
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $338,803   
}}

Red Hook Summer is a 2012 American film co-written and directed by Spike Lee. It is Lees sixth film in his "Chronicles of Brooklyn" series following Shes Gotta Have It, Do the Right Thing, Crooklyn, Clockers (film)|Clockers, and He Got Game. 

==Plot==
Flik Royale is a 13-year old boy from Atlanta who is sent to live with his preacher grandfather, Da Good Bishop Enoch Rouse, in Red Hook, Brooklyn. 

==Cast==
*Clarke Peters as Da Good Bishop Enoch Rouse
*Nate Parker as Box 
*Thomas Jefferson Byrd as Deacon Zee   
*Toni Lysaith as Chazz Morningstar 
*Jules Brown as Flik Royale  
*Jonathan Batiste as Da Organist T.K. Hazelton
*Colman Domingo as adult Blessing Rowe
*Heather Simms as Sister Sharon Morningstar
*James Ransone as Kevin 
*DeAdre Aziza as Colleen Royale 
*Isiah Whitlock, Jr. as Detective Flood
*Tracy Camilla Johns as Miss Darling

Spike Lee reprises his role as Mookie, the main character from his 1989 film Do the Right Thing. 

==Production==
Principal photography lasted three weeks, "on a small budget, guerrilla filmmaking|guerrilla-style, like Shes Gotta Have It|Lees first feature film." 

Red Hook Summer marked the first time that Lee has acted in one of his films since Summer of Sam (1999).

==Release==
A 135-minute version of Red Hook Summer was previewed at the 2012 Sundance Film Festival;   The film was released on August 10, 2012, in select theaters of the New York City area  and was released in Los Angeles and other parts of the United States on August 24, 2012.   The film reached 41 theaters at its peak. 

The film was released on home video on December 21, 2012. 

==Reception==
Peter Debruge of Variety (magazine)|Variety said "Its fiery, passionate stuff, at times inelegantly presented, but impossible to ignore." 

Roger Ebert gave the film   (2½ out of four stars) saying it "plays as if the director is making it up as he goes along. Thats not entirely a bad thing, although some will be thrown off-balance by an abrupt plot development halfway through that appears entirely out of the blue and is so shocking that the movie never really recovers. Here is Lee at his most spontaneous and sincere, but he could have used another screenplay draft."   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 