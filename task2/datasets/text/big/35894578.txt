Rosenmontag (film)
{{Infobox film
| name           = Rosenmontag 
| image          = 
| image_size     = 
| caption        = 
| director       = Rudolf Meinert
| producer       = Rudolf Meinert
| writer         = Otto Erich Hartleben (play)   Emanuel Alfieri
| narrator       = 
| starring       = Gerd Briese   Helga Thomas  Maria Reisenhofer   Charles Willy Kayser
| music          = Bruno Gellert
| editing        = 
| cinematography =  Otto Kanturek
| studio         = International Film-AG
| distributor    = International Film-AG 
| released       = 1924
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent romance play of the same title by Otto Erich Hartleben.

==Cast==
* Gerd Briese   
* Helga Thomas   
* Maria Reisenhofer   
* Charles Willy Kayser   
* Franz Schönfeld   
* F.W. Schröder-Schrom   
* Alfred Braun   
* Rio Nobile   
* Otto Reinwald   
* Ernst Nessler   
* Jutta Jol   
* Adele Reuter-Eichberg   
* Rudolf Maas

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 
 


 
 