Her Best Move
{{Infobox film
| name           = Her Best Move 
| image          = Her Best Move film poster.jpg
| caption        = Theatrical release poster
| director       = Norm Hunter
| producer       = Norm Hunter Win Seipp Gerald T. Olson Craig Weisz
| writer         = Norm Hunter Tony Vidal
| narrator       =  Scott Patterson Lisa Darr Drew Tyler Bell Lalaine Daryl Sabara
| music          = Didier Lean Rachou
| cinematography = Paul Ryan
| editing        = Mitch Stanley
| studio         = Summertime Films
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 soccer prodigy named Sara Davis who has a chance to join the United States womens national soccer team|U.S. National Team. At the same time, she must juggle high school, Romantic love|romance, sports, and parental pressure. 

==Plot== Scott Patterson), sacrifices her interest in dance, photography, and her social life to concentrate on her sport.

With the encouragement of her best friend Tutti (Lalaine), Sara begins a relationship with Josh (Drew Tyler Bell), the solitary photographer on the school newspaper. As she takes control of her life, Sara faces the challenge of discovering what she really wants, so that she can make the best move of her life.

==Cast==
* Leah Pipes as Sara Davis  Scott Patterson as Gil Davis 
* Lisa Darr as Julia
* Drew Tyler Bell as Josh
* Lalaine as Tutti
* Daryl Sabara as Doogie
* Jhoanna Flores as Regina
* Denise Dowse as Lisa
* Fay Masterson as Lori
* Brandi Chastain as Herself

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 