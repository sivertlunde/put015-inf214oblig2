Mamma Roma
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = Mamma Roma
| image          = Mammaroma.jpg
| caption        =
| director       = Pier Paolo Pasolini
| producer       = Alfredo Bini
| writer         = Pier Paolo Pasolini
| starring       = Anna Magnani Ettore Garofolo
| music          =
| cinematography = Tonino Delli Colli
| editing        = Nino Baragli
| distributor    =
| released       =  
| runtime        = 106 minutes
| country        = Italy
| language       = Italian
| budget         =
| preceded_by    =
| followed_by    =
}}

Mamma Roma is a 1962 film directed by Pier Paolo Pasolini.

==Story==
An ex-prostitute, Mamma Roma (Anna Magnani), tries to start a new life selling vegetables with her 16-year-old son Ettore (Ettore Garofolo). When he later finds out that she was a prostitute, he succumbs to the dark side with the petty theft of a radio in a hospital and goes to prison.

==Cast==
* Anna Magnani: Mamma Roma
* Ettore Garofolo: Ettore
* Franco Citti: Carmine
* Silvana Corsini: Bruna
* Luisa Loiano: Biancofiore
* Paolo Volponi: Priest
* Luciano Gonini: Zacaria

==Production==
The lingering shots of Ettore, strapped to a prison bed in his underwear are seductive and haunting, as are shots of Mamma Roma walking at night, joined by different men in conversation, one after another in one continuous shot.

Pier Paolo Pasolini said that he wasnt able to rebirth Anna Magnani as she was in Roma, città aperta because, as an actor, she chose to maintain her independence from his artistic visions. "If I had to shoot the film over, I would have still chosen her", said Pasolini later.

==Pasolini vs. Rossellini==
Mamma Roma was dedicated to the director of Roma, città aperta (1945 in film|1945), Roberto Rossellini. Anna Magnani plays a pregnant woman who is killed in the middle of Rossellinis film. Rossellini represents "good Italians" through the deaths of a priest, Don Pietro, who helps a communist group and a mother who tries to help her communist husband. People who killed these "good Italians" are Nazis. On the other hand, Pasolini comments on how the country changed from 1945 to 1962 in Mamma Roma. First of all, characters in the film are whores, pimps, and thieves. None of them are people who work for people. Secondly, in the beginning of the film, Mamma Roma brings three piglets to the wedding of her old pimp, Carmine. Carmine calls them "Brothers of Italy". Then, Mamma Roma calls one of them a slut. Contrasting with Rossellinis patriotic film, Pasolini brings irony into his film. Ettore dies in an Italian prison even though he doesnt die from extreme physical torture. Pasolini shows that the corruption of society is not because of outside forces like Rome under Nazi occupation. The problems are actually created within the society or the people in it.

There is a shot of the landscape of Rome in the end of Mamma Roma. The dome of a church (Basilica di San Giovanni Bosco) sticks out from the rest of the tops of buildings. The scene shares a similarity with the aforementioned Rossellinis film again. In Rossellinis film, the dome of St. Peters appears as the background when boys are walking on the street dragging their sadness. They seem like they are walking toward St. Peters or "the house of God". On the other hand, in Pasolinis film, when Mamma Roma learns of Ettores death, she goes back to her apartment and tries to jump out of the window.  She sees the dome of a church and stares at the dome. The image of her and the dome switches back and forth. Her stare gets more intense. The film ends.

==Pasolini vs. neo-realist films==
What separates Pasolini from other neo-realist filmmakers is the use of actors and their acting. Most of Pasolinis cast didnt have any acting training. In his book A Certain Realism: Making Use of Pasolinis Film Theory and Practice, Maurizio Viano points out that actors in other filmmakers films are "beginners" and they start establishing acting careers afterwards. But in Pasolinis case, most of the actors "only acted with Pasolini". So, actors who have worked with Pasolini dont develop a career in acting because Pasolinis film is the first and the last film of their acting career. Author of Pier Paolo Pasolini: Cinema as Heresy, Naomi Greene says the use of non-professional actors is the same as what other neo-realist filmmakers do, but the philosophy behind it is different. Neo-realist filmmakers believed that using nonprofessional actors "would add to the realism to their films, Pasolini turned to nonprofessionals because their acting did not seem real".  Nonprofessional acting obviously interrupts "narrative flow".  For example, when Mamma Roma and Ettore dance a tango together, Ettores rigid inexpert dance move causes his mother to fall on the floor. She laughs brightly. Ettore laughs shyly and looks at the camera. But Pasolini doesnt cut or reshoot the scene. He leaves it as it is.

==Pasolinis obsession and techniques in Mamma Roma==
===Christian art=== Lamentation over the Dead Christ. Ettore is lying on the wooden bed. He is stripped down to a shirt and underpants. His body is tied to the bed. The light coming from the ceiling emphasizes Ettores pale skin. The camera shoots his body from Ettores feet, similar to the Mantegna painting.

===Invisibility as "sacred"===
Pasolini often uses the word "sacred" to describe the images he creates or the images that he excludes from the frame. In the film, there are two long shots, and both of them are when Mamma Roma walks at night, working as a prostitute. The background is dark. Only lined street lights are dimly shining. The audience cant see what is going on in the background. Mamma Roma seems isolated from the world. Greene explains what this cinematography technique creates in Pasolinis film. "By fixing and isolating segments of what is visible, Pasolini creates the sense that what we see is merely one part of reality, and that the truly essential — and sacred — remains unseen."  Just as people cant see God, Pasolini is attracted by something that is invisible to the audiences eye, and the invisible images carry something mysterious. In this scene of Mamma Roma, the invisible background makes the scene more mysterious and outstanding than other scenes in the film.

===Direct body language===
Pasolini uses physical language to show an emotional or psychological state of Ettore. Ettore walks abnormally as if he is "sleepwalking".  Viano suggests that "Pasolini has Ettore sleepwalking throughout the film because sleepwalking is the best visual translation of a state of nonparticipation in normal, waking life". Ettores physical state fits with his psychological state. He starts having a fever in the end of the film. He gets excluded from his friends and becomes lonely. His "abnormal body temperature" shows abnormality or difference between him and other people and his strong loneliness and wanting to belong and to have somebody to trust.

==Public reaction==
As soon as the film was released, critics and audiences claimed the film as immoral due to swearing.   The issue was brought to court, but the case was turned down as unfounded.   

At the films premiere in the Quattro Fontane Cinema in Rome on 22 September 1962, Pasolini was attacked by fascists who protested against the film.

==References==
* Greene, Naomi. Pier Paolo Pasolini: Cinema as Heresy. Princeton: Princeton UP, 1990.
* Restivo, Angelo. The Cinema of Economic Miracles:Visuality and Modernization in the Italian Arts Film. Durham and London: Duke UP, 2002. 
* Siciliano, Enzo. Pasolini:A Biography. New York: Random House, 1982.
* Viano, Maurizio. A Certain Realism:Making Use of Pasolinis Film Theory and Practice. Berkeley and Los Angeles: University of California P, 1993.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 