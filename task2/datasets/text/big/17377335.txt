The Story of Seabiscuit
{{Infobox film
| name           = The Story of Seabiscuit
| image_size     = 
| image	         = The Story of Seabiscuit FilmPoster.jpeg
| caption        =  David Butler William Jacobs
| writer         = John Taintor Foote
| starring       = Shirley Temple Barry Fitzgerald Lon McCallister
| music          = David Buttolph
| cinematography = Wilfred M. Cline
| editing        = Irene Morra
| distributor    = Warner Bros.
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
}}
 1949 American David Butler and starring Shirley Temple. The screenplay was written by John Taintor Foote.

Though shot in Technicolor, the film incorporates actual black-and-white footage of Seabiscuit in races, including the 1940 Santa Anita Handicap and the 1938 match race against rival War Admiral, which is still considered by many to be the greatest horse race of all time. 

==Summary==

The film is a fictionalized account of the career of the racehorse Seabiscuit (1933&ndash;1947) with a subplot involving the romance between the niece (Temple) of a horse trainer (Barry Fitzgerald) and a jockey (Lon McCallister). The role of Seabiscuit was played by one of his sons, Sea Sovereign.

==Cast==
* Shirley Temple as Margaret OHara
* Barry Fitzgerald as Sean OHara
* Lon McCallister as Ted Knowles
* Rosemary DeCamp as Mrs. Charles S. Howard
* Donald McBride as George Carson
* Pierre Watkin as Charles S. Howard William Forrest as Thomas Milford

==Reception== AMC critic Christopher Null agreed, writing, "The only actual reason to watch this film ... is the black and white footage of Seabiscuits actual races". 

==See also==
* Shirley Temple filmography

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 