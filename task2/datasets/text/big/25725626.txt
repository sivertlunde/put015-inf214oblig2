Pony Soldier
{{Infobox film
| name           = Pony Soldier
| image          = Ponysolpos.jpg
| image_size     =
| caption        = Original film poster
| director       = Joseph M. Newman
| producer       = Samuel G. Engel
| writer         = Garnett Weston (story Mounted Patrol) John C. Higgins
| narrator       = Michael Rennie Tyrone Power  Robert Horton Penny Edwards Cameron Mitchell
| music          = Alex North Harry Jackson
| technical advisor = Nipo T. Strongheart
| distributor    = 20th Century Fox
| released       =  
| runtime        = 82 minutes
| country        = United States English
| budget         =
| gross          = $1.65 million (US rentals) 
}}
 1952 Technicolor Northern Western Western set in Canada but filmed in Sedona, Arizona.   It is based on a 1951 Saturday Evening Post story "Mounted Patrol" by Garnett Weston.  It was retitled MacDonald of the Canadian Mounties in England and The Last Arrow in France and Spain.

==Plot== Fata Morgana type mirage that they mistake for the power of Queen Victoria.
 Robert Horton Penny Edwards) arrests a murderer, and adopts a Cree son (Anthony Earl Numkena).

==Production== Native American issues, Nipo T. Strongheart, wrote a critical review of the proposed screenplay, even though other departments of the studio had begun work on it. This led to a meeting with studio executives which, though he described it as feeling like he was called to the principals office, led to a major reconstruction of the whole project. {{cite journal
 | last = Strongheart | first = Nipo T.
 | date = Autumn 1954
 | title = History in Hollywood
 | journal = The Wisconsin Magazine of History
 | volume = 38 | issue = 1 | pages = 10–16, 41–46 Navajo to play Cree when large numbers were needed. Strongheart, who also plays a Medicine Man in the film) also toured to promote the movie.  {{cite news
 | title =Film Actor works with Ty Jr, now
 | newspaper =The Deseret News
 | location =Salt Lake City, Utah
 | page =4
 | date =Aug 31, 1952
 | url =http://news.google.com/newspapers?nid=336&dat=19520831&id=JhYkAAAAIBAJ&sjid=oU0DAAAAIBAJ&pg=3355,5806967
| accessdate = August 25, 2014 }}  Strongheart had appeared in the film Braveheart (1925 film)|Braveheart with Tyrone Power Sr.  

Included in the cast were Richard Boone, Frank deKova, and in his film debut, Anthony Earl Numkena.

==References==
 

==External links==
* 
*  
*filming at Sedona http://www.sedonamonthly.com/gonehollywood/soldier.html
*New York Times review http://movies.nytimes.com/movie/review?res=9C0CE5DF1E3AE23BBC4851DFB4678389649EDE

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 