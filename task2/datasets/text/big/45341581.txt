Running Mates (1992 film)
 
{{Infobox film
| name           = Running Mates
| image          =
| caption        =
| writer         =
| starring       = Diane Keaton Ed Harris Ed Begley Jr. Ben Masters
| director       = Michael Lindsay-Hogg
| cinematography =
| editing        =
| music          =
| distributor    = HBO
| released       = October 4, 1992
| runtime        = 92 min.
| country        = United States
| language       = English
| movie_series   =
| awards         =
| budget         =
| gross          =
| producer       =
| followed_by    =
| website        =
}}
 political comedy/drama television film directed by Michael Lindsay-Hogg and starring Ed Harris. The film follows the presidential election campaign of Senator Hugh Hathaway, who faces scandal and controversy when his enemies share secrets of Aggie Snow, the woman he is with.

==Plot==
Running for President, Senator Hugh Hathaway must go up against scandal and controversy after the woman he loves comes under political attack.

==Cast==
*Diane Keaton as Aggie Snow
*Ed Harris as Senator Hugh Hathaway
*Ed Begley Jr. as Chapman Snow
*Ben Masters as Mel Fletcher

==See also==
* 1992 in film
* Cinema of the United States
* List of American films of 1992

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 