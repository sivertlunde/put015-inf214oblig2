High School (1954 film)

{{Infobox film
 | name = High School
 | image =  High School (1953 film).jpg
 | caption =
 | director = Luciano Emmer
 | writer =   Sergio Amidei  Carlo Bernari  Vasco Pratolini Luciano Emmer 
 | starring =  
 | music =  Carlo Innocenzi
 | cinematography = Mario Bava
 | editing =    Eraldo Da Roma
 | producer =  
 }} 1954 Cinema Italian Coming-of-age film|coming-of-age comedy-drama film directed by Luciano Emmer.      

== Cast == 

*Giulia Rubini: Camilla
*Ilaria Occhini: Isabella
*Anna Maria Sandri: Teresa
*Ugo Amaldi: Franco
*Ferdinando Cappabianca: Andrea
*Franco Santori: Bruno
*Paola Borboni: Mother of Bruno
*Bartolomeo Rossetti: Valenti  
*Antonio Battistella: Father of Giulia
*Eriprando Visconti: Carlo
*Marcella Rovena: Mother of Camilla
*Turi Pandolfini: Professor 
*Giuliano Montaldo: Professor
*Carlo Bernari: Professor 
*Christine Carère: Giovanna
*Valeria Moriconi  
*Rita Livesi

==References==
 

==External links==
* 
 
 
 
 
   
 
 
 
 

 
 
 