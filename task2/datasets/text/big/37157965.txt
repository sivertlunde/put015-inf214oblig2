The Pervert's Guide to Ideology
 
{{Infobox film
| name = The Perverts Guide to Ideology
| image = The Perverts Guide to Ideology poster.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster
| director = Sophie Fiennes
| producer = Sophie Fiennes Katie Holly Martin Rosenbaum James Wilson
| writer = Slavoj Žižek
| starring = Slavoj Žižek
| music = Brian Eno
| cinematography = Remko Schnorr
| editing = Ethel Shepherd
| studio = 
| distributor = P Guide Productions  Zeitgeist Films
| released =  
| runtime = 136 minutes  
| country = United Kingdom
| language = English
| budget = 
| gross = 
}} philosopher and psychoanalyst Slavoj Žižek.  It is a sequel to Fienness 2006 documentary The Perverts Guide to Cinema. Though the film follows the frameworks of its predecessor, this time the emphasis is on ideology itself. Through psychoanalysis Žižek explores "the mechanisms that shape what we believe and how we behave".   Among the films that are explored are Full Metal Jacket and Taxi Driver.  The film was released in the United States by Zeitgeist Films in November 2013. 

==Synopsis== The Sound Zabriskie Point, The Searchers, The Dark Knight, John Carpenter’s They Live (“one of the forgotten masterpieces of the Hollywood Left”), Titanic (1997 film)|Titanic, Kinder Surprise eggs, verité news footage, the emptiness of Ludwig van Beethoven|Beethoven’s Symphony No. 9 (Beethoven)|"Ode to Joy", and propaganda epics from Nazi Germany and Soviet Russia all inform Žižek’s psychoanalytic-cinematic argument.

==List of films discussed in this documentary==
* Triumph of the Will (1935) The Eternal Jew (1940)
* Brief Encounter (1945) The Fall of Berlin (1950) The Searchers (1956) West Side Story (1961) The Sound of Music (1965)
* Loves of a Blonde (1965)
* Seconds (film)|Seconds (1966)
* The Firemens Ball (1967)
* Oratorio for Prague (1968)
* If.... (1969)
* MASH (film)|MASH (1970) Zabriskie Point (1970)
* Cabaret (1972 film)|Cabaret (1972) A Clockwork Orange (1971)
* Jaws (film)|Jaws (1975)
* Taxi Driver (1976)
* Brazil (1985 film)|Brazil (1985)
* Full Metal Jacket (1987) The Last Temptation of Christ (1988)
* They Live (1988)
* Titanic (1997 film)|Titanic (1997) I Am Legend (2007) The Dark Knight (2008)

Also included is some news footage of the September 11 attacks, 2011 Norway attacks, and 2011 England riots.

==Awards==
* 2012: Toronto International Film Festival, Official Selection

==Reception==
"Though its ideas are indeed heady and high-flown, they are presented in a way thats consistently engaging and accessible. And the bearded, bulky, Slovenia-born Žižek comes across as a born raconteur and explainer, the kind of professor whose courses are deservedly his departments most popular. You dont have to share his materialist philosophy to find his analyses of culture and movies witty, insightful and usefully thought-provoking."
—Godfrey Cheshire, RogerEbert.com 

"What remains? You will get a lot of answers to questions you never knew you had."
—Anne-Katrin Titze, Eye for Film 

"Intellectual rock star Slavoj Žižek dishes out another action-packed lesson in film history and Marxist dialectics with The Pervert’s Guide to Ideology, a riveting and often hilarious demonstration of the Slovenian philosopher’s uncanny ability to turn movies inside out and accepted notions on their head."
—Jordan Mintzer, The Hollywood Reporter 

"Žižek’s romp through pop culture feels like a strange dream, with a mad professor re-enacting our favourite movie moments through the eyes of a therapist. The Pervert’s Guide to Ideology is invigorating, zany, completely memorable and often hilarious. Žižek goes from praising Coca-Cola to analyzing what the shark attacks really mean in Jaws."
—Matthew Hays, Rover Arts Review 

Rotten Tomatoes gives the film a score of 91% based on reviews from 22 critics. 

==See also==
* The Perverts Guide to Cinema
* Žižek!
* The Reality of the Virtual
* Liebe Dein Symptom wie Dich selbst!
* Examined Life
* Marx Reloaded

==References==
 

==External links==
*  
*   reviewed at Truth-out.org, 28 November 2012.
* 

 
 
 
 
 
 
 
 
 
 
 