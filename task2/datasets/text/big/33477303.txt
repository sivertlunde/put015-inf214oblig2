7 Zwerge – Der Wald ist nicht genug
{{Infobox film
| name           = 7 Zwerge – Der Wald ist nicht genug
| image          = 
| caption        = 
| director       = Sven Unterwaldt
| producer       = Douglas Welbat
| writer         = Bernd Eilert Sven Unterwaldt Otto Waalkes
| starring       =
| music          = Joja Wendt
| cinematography = Peter von Haller
| editing        = Norbert Herzner
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
7 Zwerge – Der Wald ist nicht genug ( ) is a 2006 German comedy film directed by Sven Unterwaldt. It is a sequel to the film 7 Zwerge – Männer allein im Wald. The movie is based upon the fairy tale Rumpelstiltskin and characters from Snow White and the Seven Dwarfs.

==Plot==
The movie starts some years after the events of 7 Zwerge: Men Alone In The Wood. In the intro Hansel and Gretel are lost in the forest. Gretel thinks to see a dwarf in the bushes but it is a disgusting humanlike creature. It scares Hansel and Gretel who run further into the woods. Bubi appears not much later and he asks the creature his name. The creature only reveals to be "The Evil One" and nobody else knows his real name. Bubi sneakily follows the creature and witnesses how it dances around a fire singing a song in which he mentions his real name.

The story moves to the castle where Snow White lives. She became a mother. Her husband, the jester, left a year ago to buy some cigarettes in a nearby shop. Snow White wonders why this takes so long. The Evil One suddenly enters the castle and claims the child. The day before Spliss, one of Snow White guards, rescued The Evil One from a trap. The Evil One wanted to thank Spliss by giving him a wish. Spliss wished to have a beautiful haircut which he got. However, Spliss did not like the color and wanted to have it blond. The Evil One agreed on condition Spliss signs a contract so Snow Whites child will become his property. Snow White asks The Evil One if the contract can be undone. The Evil One agrees nor or less: if someone can reveal his real name within 48 hours, Snow White can keep the child.

Snow White seeks for the dwarves, but is surprised only Bubi still lives in the cabin. Bubi explains this is Snow White her own fault. Some time ago she visited the dwarves telling them her husband is missing. She wanted to find a new man and this could be even a dwarf. There was one main condition: the man must have a successful career. Not much later all dwarves left except Bubi. Snow White is a bit surprised she cant remember her visit. Snow White asks Bubi to reunite the dwarves and to find the name of The Evil One.

The six other dwarves work in a nearby town. Coockie, Cloudy and Sunny have their own inn. Speedy is chief of the fire department. Ralfie works at the brewer. Tschakko extincts vermin. Bubi finds them all and can convince them to help Snow White. According them, there is only one man who can help them: The Wise Grey. The dwarves cant find him. According his diary, he left to "The Fishing Palace" in another world. The dwarves find the magical mirror which once belonged to the former queen. They jump into the mirror and end up in the other world: modern Germany.

In meantime, The Evil One arrived at the candy house of the witch, who is actually the former queen. The Evil One follows a therapy. The queen says she cant remember three things: names, faces and a third other thing she does not know anymore. As the witch always forgets the real name of The Evil One, last one writes it down on a paper:  .

The dwarves find The Wise Grey in some sort of fish and chips-stand. He does know The Evil One but never heard his real name. He does know the dwarves should look for it in the witches candy house. The dwarves return to their own world by using a magic mirror in the railway station. In meantime, The Evil One tried unsuccessfully to stop the dwarves.

The dwarves head to the candy house where the witch tried to make her invisible. The Evil One is also present. The dwarves use a tricky way to obtain the envelope with The Evil Ones real name. They all go to Snow White. However, The Evil One gave the dwarves an envelope with another name so the dwarves think his real name is Mother Hulda. They realize it was not Snow White who visited them to tell she is in search for a new man, but The Evil One. Eventually, Bubi says the real name is Rumpelstiltskin. He knew this the whole time and tried to tell but nobody let him speak. The dwarves also meet their former head dwarf Brummboss who became a king at the end of Männer allein im Wald. The king asks if he can become a dwarf again. This is rejected by the others as there are already seven dwarves.

==References to other media==
*The scene at the railroad station where the dwarves search for a way to return to their own world is a reference to Harry Potter and the Philosophers Stone.
*References to The Lord of the Rings:
**The Wise Grey is a reference to Gandalf
**The child is described as "precious" a reference to the one ring.
**Rumpelstiltskin has a same psychic illness as Golem
*The title is a reference to James Bonds The World Is Not Enough.
*There are references to Little Red Riding Hood, Pinocchio, Hansel and Gretel and Mother Hulda
*the song hummed by dwarfs while they approached witchs house is "Only you" by The Flying Pickets

==Cast==
* Otto Waalkes as Bubi
* Mirco Nontschew as Tschakko
* Gustav Peter Wöhler as Cookie
* Martin Schneider as Speedy
* Boris Aljinovic as Cloudy
* Ralf Schmitz as Sunny
* Norbert Heisterkamp as Ralfie
* Axel Neumann as Rumpelstiltskin
* Cosma Shiva Hagen as Snow White
* Hans Werner Olm as Spliss
* Nina Hagen as witch
* Heinz Hoenig as king Brummboss
* Christian Tramitz as hunter
* Rüdiger Hoffmann as ghost of the mirror
* Helge Schneider as The Wise Grey
* Olli Dittrich as Pinocchio

==Trivia==
*Markus Majowski did not return in his role as Coockie
*Mirja du Mont neither played Little Red Riding Hood. She was replaced by Mavie Hörbiger.

==External links==
*  

 
 
 
 
 
 
 