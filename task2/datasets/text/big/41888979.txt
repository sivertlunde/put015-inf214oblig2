Hotel Noir
{{Infobox film
| name           = Hotel Noir
| image          = Hotel Noir.jpg
| border         = yes
| alt            = 
| caption        = 
| director       = Sebastian Gutierrez
| producer       = Steve Bing Sebastian Gutierrez Zach Schwartz
| writer         = Sebastian Gutierrez
| starring       = {{Plainlist |
* Rufus Sewell
* Malin Akerman
* Carla Gugino
* Danny DeVito
* Rosario Dawson Kevin Connolly
* Robert Forster
* Mandy Moore
}}
| music          = Robin Hannibal Mathieu Schreyer	
| cinematography = Cale Finot
| editing        = Lisa Bromwell John Wesley Whitton
| studio         = Gato Negro Films Shangri-La Entertainment
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Hotel Noir is a 2012 crime film directed and written by Sebastian Gutierrez. The film stars Carla Gugino and Rufus Sewell. The film was released at Video on demand on October 9, 2012 in USA.

==Plot==
Los Angeles, 1958: a detective holes up in a downtown hotel awaiting killers to come get him. During the course of one night he will meet various occupants of the hotel and the truth of how he came to be in his present situation will be revealed.

==Cast==
* Rufus Sewell as Felix, the Detective
* Malin Akerman as Swedish Mary, the Dancer
* Carla Gugino as Hanna Click, the Singer
* Danny DeVito as Eugene Portland, the Salesman
* Rosario Dawson as Sevilla, the Maid Kevin Connolly as Vance, the Gambler
* Robert Forster as Jim Logan, the Partner
* Mandy Moore as Evangeline Lundy
* Cameron Richardson as Maureen Chapman
* Michael B. Jordan as Leon
* Aaron Behr as Paul
* John Colella as Barman Jack
* Andrew Fiscella as Otto
* Kim Mahair as Bettie Page
* Derek Schreck as Mickey
* Michael Raif as Mysterious Gentleman
* Michelle Dawley as Dancer
* Genny Sermonia as Nightclub Dancer
* Patrick Hyde as Nightclub Patron
* Jade Willey as Man at train station

==Release==
* USA : October 9, 2012 
* France : 2015

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 