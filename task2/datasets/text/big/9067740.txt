Porky's Spring Planting
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
|cartoon_name=Porkys Spring Planting
|series=Looney Tunes (Porky Pig)
|image=
|caption=
|director=Frank Tashlin
|story_artist=George Manuell
|animator=Joe DIgalo
|layout_artist=
|background_artist=
|voice_actor=Mel Blanc
|musician=Carl Stalling, Milt Franklyn (uncredited)
|producer=Leon Schlesinger
|studio=Warner Bros. Cartoons
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|release_date=July 1938
|runtime=7:22
}}
Porkys Spring Planting is a seven minute Looney Tunes short film starring Porky Pig, released in July 1938.  Like all Looney Tune cartoons of the time, it was produced by Leon Schlesinger and distributed by Vitaphone.

==Plot==
Porky Pig begins ploughing his land and planting seeds, with some help from his dog Streamline. They plant many kinds of vegetables. But when the crops are ripe, a rooster sells tickets to other chickens who make a self-servicing cafe out of the field. Porky notices and tries to chase them off, but they persist. To protect the last of his crops, Porky makes a deal with the chickens to plant a separate vegetable garden for them.

==References==
 

==External links==
*  

 
 
 
 
 
 

 