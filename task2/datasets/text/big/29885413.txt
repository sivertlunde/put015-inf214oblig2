Pratigyabadh
{{Infobox film
 | name = Pratigyabadh 
 | image = Prathigyabadh.jpg
 | caption = DVD Cover
 | director = Ravi Chopra
 | producer = B. R. Chopra
 | writer =
 | dialogue =
 | starring = Mithun Chakraborty Kumar Gaurav Neelam Kothari Sunil Dutt
 | music = Kalyanji-Anandji
 | lyrics = Hassan Kamal
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 19 July 1991
 | runtime = 125 min.
 | language = Hindi
 | country = India Rs 6 Crores
 }}
 Hindi feature film Film directed by Ravi Chopra. It stars  Mithun Chakraborty, Kumar Gaurav, Beena Banerjee, Neelam Kothari and Sunil Dutt.

==Plot==

Pratigyabadh is the story of a simple man (Mithun Chakraborty), and his brother (Kumar Gaurav), their love and the hurdles they face in life.

==Cast==
* Mithun Chakraborty...Shankar Yadav
* Kumar Gaurav...Shakti Yadav
* Neelam Kothari...Shobhna
* Sunil Dutt...Pascal
* Beena Banerjee...Laxmi Baburam Yadav
* Shafi Inamdar...S. Merchant
* Anupam Kher...Tej Bahadur / Tejaa
* Manmauji...Havaldar
* Sujata Mehta...Phoolrani
* Yunus Parvez...Lala Kedarnath
* Sharat Saxena...Tarzan
* Girja Shankar...Lala Sukhilal
* Ashalata Wabgaonkar...Nun (as Ashalata)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Are Hey Ri Chhori Malan Ki"
| Kumar Sanu
|-
| 2
| "O Jane-e-jana Jane Bahara Sun"
| Sadhana Sargam, Anwar
|-
| 3
| "Rama Ho Rama Tere Ishq Me Mat Mori Mari Gayi"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Dhin Tara Bole Man Ka Ik Tara"
| Mahendra Kapoor, Anuradha Paudwal
|-
| 5
| "Dhin Tara Bole Man Ka Iktara (Female)"
| Anuradha Paudwal
|-
| 6
| "Dhin Tara Bole Man Ka Iktara (Male)"
| Mahendra Kapoor
|-
| 7
| "Kale Rang Di Madhani"
| Sonali Bajpai
|}

==References==
*http://www.imdb.com/title/tt0102702/

==External links==
*  

 
 
 
 

 