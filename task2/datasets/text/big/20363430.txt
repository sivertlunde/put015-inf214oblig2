And Along Come Tourists
{{Infobox Film
| name           = And Along Come Tourists  (Am Ende kommen Touristen) 
| image_size     = 
| image	         = And Along Come Tourists FilmPoster.jpeg
| caption        = English-language poster
| director       = Robert Thalheim
| producer       = Britta Knöller Hans-Christian Schmid
| writer         = Robert Thalheim
| starring       = Alexander Fehling  Ryszard Ronczewski  Barbara Wysocka
| music          = Anton K. Feist Uwe Bossenz 
| cinematography = Yoliswa Gärtig
| editing        = Stefan Kobe
| released       = 16 August 2007
| runtime        = 85 minutes
| country        = Germany German
}} 2007 German Second World War in 1945.  Jürgen Fauth and Marcy Dermansky have written of the film that, "Without ever resorting to preachiness, Thalheim, who was a Zivi at Auschwitz himself, offers incisive insights into the thorny contradictions and treacherous cross-currents of guilt and memory that turn any kind of exploration of the overbearing past into a minefield."   

The principal performers are Alexander Fehling as Sven Lehnert and Ryszard Ronczewski as the survivor Stanislaw Krzemiński. Barbara Wysocka plays Ania Łanuszewskaa, a young Polish woman from Oświęcim with whom Sven develops a romantic relationship. The film premièred August 16, 2007 in Germany; its North American premiere was September 12, 2007 at the Toronto International Film Festival.   

The film received a nomination for "Best Film" at the   for his performance as Sven.

A DVD version of the film was released in Europe in 2008,  but not in North America.

==References==
 

== External links ==
*   Official website for film.
* 
*  

 
 
 
 


 
 