List of lesbian, gay, bisexual or transgender-related films of 1976
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1976. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1976==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|- Car Wash|| 1976 ||   ||  || Comedy ||
|- 
|A Child in the Crowd|| 1976 ||   ||  || Drama||
|-  Coup de Grâce|| 1976 ||   ||    || Drama, war ||
|-
|In the Realm of the Senses|| 1976 ||   ||  || Drama||
|- Je taime... moi non plus|| 1976 ||   ||  || Drama||
|-
|Johan (1976 film)|Johan|| 1976 ||   ||  || Drama ||
|-
| || 1976 ||   ||  || Drama||
|-
|Next Stop, Greenwich Village|| 1976 ||   ||  || Comedy, drama||
|-
|Norman... Is That You?|| 1976 ||   ||  || Comedy||
|-
| || 1976 ||   ||   || Comedy||
|-
|Sebastiane|| 1976 ||  , Derek Jarman ||   || Drama, thriller||
|}

 

 
 
 