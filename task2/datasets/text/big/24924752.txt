The Twin Diamonds
{{Infobox film
| name           = The Twin Diamonds
| image          = The_Twin_Diamonds.jpg
| caption        = Khmer promotional Poster
| director       = Davy Chou
| producer       = Kun Khmer Kone Khmer
| writer         = Davy Chou
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = October 22, 2009
| runtime        = 45 minutes
| country        = Cambodia
| language       = Khmer
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} 2009 Cinema Cambodian suspense film. It is the first movie  directed by Davy Chou and produced by Kon Khmer Koun Khmer along with 60 students from 6 different universities in Phnom Penh.

==Plot==
 

Lyda, a university student, lost a valuable item, in which nobody knows what exactly the item is or where it is at. A cleaner spots a man with a dog. That night, two strange people exchange a dog in a deserted land. The day after, the cleaner recognizes the dog in front of the school and brings it inside the school. They decide to follow the dog. Vutha, the volunteer, follows the dog and discovers a group of young gangsters in an old abandoned building. Hidden, Vutha hears that the gangsters killed Lydas’ father. Trying to escape, Vutha slips and he gangsters fire at him. Seila, another student, was walking in the streets when he heard the gunshots. He finds his friend Vutha dead. He takes Vuthas phone. Seila meets Lyda in the streets and tells her about the incident. They look at a video Vutha took with his phone while listening to the gangsters, but then they see among the group a girl with exactly the same necklace Lyda inherits. Seila, confused, gets scared and escapes from Lyda. But suddenly someone hits him in the face and he knocks out. When Seila wakes up, he sees the girl in the video, with the same necklace, speaking with Lyda. The other girl, named Lina, explains to Lyda that she belongs to the gangsters group, but that when they wanted to kill Lyda’s dad, then the dad saw Lina’s necklace and explained to her that she was his daughter. Lina believed him, but couldn’t succeed in saving her dad from the gangsters. The two sisters, crying, turn to reconciliation. In Lyda’s house, Lyda’s nanny explains to the two girls that she knew their mother: when they were babies she asked her to take care of them, and also gave her the two necklaces. While walking in the streets the two girls help a woman who had fallen on the floor. Staring at their necklaces, the woman seems to be very surprised and ask Lyda and Lina to have tea with her in order to thank them. In the restaurant they realize that the woman is their mother. 20 years ago their dad was the mafia leaderwhich is the reason why their mother left him for he wouldnt quit. Indeed she asked the nanny to take care of her two daughters. But she doesnt know why Lina was separated from her father. That is when the nanny explains that Lina was stolen as a baby. Back at He came to tell them that the gangsters caught Seila during their absence. Furthermore,  he reveals the reason why the gangsters killed Lyda and Lina’s father.  He points out that 20 years ago their father had stolen 2 diamonds from the mafia. The gangsters are now willing to hold their friend, Seila, hostage in exchange for their 2 diamonds that has been stolen from them.  Then their mother indicates that she stole the diamonds from her husband and hid them in the two necklaces she gave to her daughters. Surprised, Sovannara runs to find Seila when they get a knock at the door. That is when they realize that their father was alive all along. The necklaces were the reason why they were separated at first but in the end, the necklaces were the items that helped  build the foundation of their family.

==Release==
The film was released at the Lux Theater as a part of the Golden Awakening event hosted by Davy Chou in tribute to the golden age of Cambodian cinema. Director Ly Bun Yim and actress Dy Saveth were also present at the event. Dy Saveth claimed it was a stunning surprise to find out that films from her generation were still being watched and appreciated, especially by those of the newer generation. This event was marked as the beginning of the revival of the Cambodian film industry.

==References==
*http://twindiamonds.wordpress.com/
*http://ladypenh.com/article21073.html
*http://ladypenh.com/event/20113.html
*http://www.abc.net.au/news/stories/2009/10/23/2722710.htm
*http://www.phnompenhpost.com/index.php/2009102729198/Lifestyle/films-rock-a-new-generation.html
*http://www.earthtimes.org/articles/show/291068,young-cambodians-salute-golden-era-of-1960s-filmmaking--feature.html
*http://www.lepetitjournal.com/content/view/48188/1841/

 
 
 
 