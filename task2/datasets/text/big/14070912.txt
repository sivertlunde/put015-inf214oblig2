A Thousand Clouds of Peace
{{Infobox Film
| name           = A Thousand Clouds of Peace
| image          = ThousandCloudsOfPeace.jpg
| image_size     = 
| caption        = DVD cover
| director       = Julián Hernández (filmmaker)|Julián Hernández
| producer       = Roberto Fiesco
| writer         = Julián Hernández
| narrator       = 
| starring       = Juan Carlos Ortuno
| music          = 
| cinematography = Diego Arizmendi
| editing        = Emiliano Arenales Osorio Jacopo Hernández
| distributor    = Strand Releasing (USA)
| released       = February 11, 2003 (Berlin Film Festival) April 2, 2004 (USA) October 15, 2004 (Mexico)
| runtime        = 83 mins.
| country        = Mexico Spanish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 2003  romantic drama Spanish title is Mil nubes de paz cercan el cielo, amor, jamás acabarás de ser amor and alternative titles for it are A Thousand Clouds of Peace Fence the Sky, Love; Your Being Love Will Never End and A Thousand Peace Clouds Encircle the Sky.

==Plot==
Not long after coming to terms with his homosexuality, 17-year-old Gerardo has broken up with his first serious boyfriend. Trying to ease his pain he has a number of casual sexual encounters in Mexico City.

==Cast==
*Juan Carlos Ortuno as Gerardo
*Juan Torres as Bruno
*Perla de la Rosa as Anna
*Salvador Alvarez as Susana
*Rosa-Maria Gomez as Mary
*Mario Oliver as Umberto
*Clarisa Rendón as Nadia
*Salvador Hernandez as Antonio
*Pablo Molina as Andres
*Martha Gomez as Martha
*Manuel Grapain Zaquelarez as Jorge
*Miguel Loaiza as Adrian
*Llane Fragoso as Mirella
*Pilar Ruiz as Lola

==Reception==
Movie review website Rotten Tomatoes gave A Thousand Clouds of Peace a "rotten" rating of 32% based on eight reviews. {{cite web
  | title =A Thousand Clouds Of Peace (2003)
  | publisher =Rotten Tomatoes
  | url =http://uk.rottentomatoes.com/m/thousand_clouds_of_peace/
  | accessdate =2007-11-04  }}  Metacritic gave it a "generally negative" rating of 35% based on 16 reviews. {{cite web
  | title =Thousand Clouds of Peace, A
  | publisher =Metacritic
  | url =http://www.metacritic.com/film/titles/thousandcloudsofpeace?q=A%20Thousand%20Clouds%20of%20Peace
  | accessdate =2007-11-04  }} 

In 2003 the film won the Teddy Award for Best Feature Film at the Berlin International Film Festival as well as the awards for Best First Work at the Lima Latin American Film Festival. In 2004 it won the Silver Ariel at the Ariel Awards in Mexico. {{cite web
  | title =Awards for Mil nubes de paz cercan el cielo, amor, jamás acabarás de ser amor
  | publisher =Internet Movie Database
  | url =http://imdb.com/title/tt0358590/awards
  | accessdate =2007-11-04  }} 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 