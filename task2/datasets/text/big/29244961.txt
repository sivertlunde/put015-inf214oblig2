Shri Krishnanjaneya Yuddham
 
{{Infobox film
| name           =    Sri Krishnanjaneya Yuddham
| image          = 
| image_size     =
| caption        = War between Lord Krishna and Lord Anjaneya
| director       = C. S. Rao
| producer       = 
| writer         = 
| narrator       =
| starring       = N. T. Rama Rao, Devika, Vanisri
| music          = T. V. Raju
| cinematography = G. K. Ramu
| editing        =
| studio         = Jayanthi Pictures
| distributor    =
| released       = 9 January 1963
| runtime        = 
| country        = India Telugu
| budget         =
}}

Sri Krishnanjaneya Yuddham (  film directed by C. S. Rao. The film was released in the year 1972. The film mainly revolves around Lord Anjaneya waiting for Lord Sri Ramas darsanam in Dwaparayuga.

==Plot==

This Hindu mythological movie is based on Lord Anjaneyas devotion towards Lord Rama who later finds Rama in Lord Krishna. The movie begins with how the pride of Pandavas (Arjuna & Bhima) is destroyed by Hanuma and later Garuda in search of his daily food and finds a Nagakanya as his prey. Nagakanya is guided by Narada to go to Hanuma for help where she becomes safe. Garuda reports this to Lord Krishna and his wives Rukmini & Sathyabhama. Krishna sends out a warning to Hanuma to release Nagakanya for garudas prey. But Hanuma doesnt care for his warning & continues to protect Nagakanya. This leads to a war between them as Hanuma doesnt recognise Rama in Krishna at first. Sathyabhama disguised as Sita tries to mislead Hanuma but fails. Later how the war gets stopped and Nagakanya is saved forms the rest of the story.

==Cast==

{| class="wikitable"
|-
! Actor !! Character
|-
| Nandamuri Taraka Rama Rao || Lord Krishna & Lord Rama
|-
| Devika || Rukmini & Sita
|-
| Vanisri || Sathyabhama
|-
| Tadepalli Lakshmi Kanta Rao || Narada Muni
|-
| S. V. Ranga Rao || Balaram
|-
| Rajanala || Hanuman
|}

==Crew==
* Director: C. S. R. Rao Samudrala
* Cinematography: G. K. Ramu
* Music Director: T. V. Raju	
* Playback singers: Ghantasala Venkateswara Rao, S. P. Balasubramanyam, P. Susheela, S. Janaki, B. Vasantha

==Soundtrack==
* Yennaallu Vechenu Oh Rama (Lyricist: C. Narayana Reddy; Singer: Ghantasala Venkateswara Rao)
* Padhyams (Singers: Ghantasala Venkateswara Rao, S. P. Balasubramanyam, P. Susheela)
* Rama Rama RamaSeetha (Lyricist: C. Narayana Reddy; Singers: Ghantasala Venkateswara Rao)
* Padyams Kausalya(Shlokam) (Singer: Ghantasala Venkateswara Rao)
* Rama Raghurama (Lyricist: C. Narayana Reddy; Singer: Ghantasala Venkateswara Rao)
* Chakkani Gopala (Lyricist: C. Narayana Reddy, Dasarathi, Samudrala; Singers: S. Janaki, B. Vasantha)
* Neevaina Cheppave (Lyricist: C. Narayana Reddy; Singer: Ghantasala Venkateswara Rao, P. Susheela)

== References ==
 

== External links ==
*  
*  
*  

 
 