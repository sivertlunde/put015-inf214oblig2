The Rubber Room
{{Infobox film
| name           = The Rubber Room
| image          = Rubber Room poster.jpg
| alt            = 
| caption        = 
| director       = Jeremy Garrett & Justin Cegnar
| producer       = Jeremy Garrett & Justin Cegnar
| writer         = Jeremy Garrett & Justin Cegnar
| starring       = 
| music          = 
| cinematography = Ani Karougian
| editing        = Jeremy Garrett
| studio         = Five Boroughs Productions Moving Picture Institute 
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Rubber Room is a 2010 documentary film about the reassignment centers run by the New York City Department of Education, which the filmmakers claim exist in various forms in school districts across the United States.  Allegedly intended to serve as temporary holding facilities for teachers accused of various kinds of misconduct who are awaiting an official hearing, these reassignment centers have become known amongst the "exiled" teachers subculture as "rubber rooms", so named after the padded cells of psychiatric hospitals.   

==Film content==
The film documents the daily routines of teachers accused of misconduct or incompetence.  Several days, weeks, months, or even years are spent in these "Kafkaesque"  rubber rooms waiting for some kind of resolution; the filmmakers estimate that the average wait time is three years, but cited cases lasting as long as ten years.  During this time in "exile", the teachers receive their full pay and benefit packages at the cost of up to 65 million dollars per year to the city,    spending each day reading, playing cards, balancing checkbooks, sleeping, or simply staring blankly at the walls.  Interviewed teachers from the rubber room describe the high tensions that develop between teachers angry at their inexplicable situations that curiously lead to a "prison-like" environment in which teachers largely separate themselves into fiercely territorial cliques, sometimes even resulting in physical altercations.
 tenured teacher.  Reasons that teachers are sent to the rubber room range from accusations as extreme as physical and sexual abuse of students to as minor as excessive tardiness  or even simple personality conflicts with administrators, and yet these groups of teachers are shown to be lumped together in the same situation with egregiously delayed hearings, often without evidence of any misconduct, sometimes even going without knowledge of what the alleged misconduct actually is, for up to three years.

Furthermore, the film shows some students boasting about having learned to take advantage of this system after realizing that they have the power to remove teachers almost at will.  This upset in the balance of power, they show, can have drastic effects on the teachers ability to do their jobs and effectively teach while maintaining order in the classrooms.

==Making of the film==
In an interview with MSNBCs Joe Scarborough and Mika Brzezinski, filmmaker and former New York City teacher Jeremy Garrett explained that they intended their film to be an Investigative journalism|exposé on not only the excessive waste that is associated with maintaining the rubber rooms, but also to highlight the "shifting landscape in education", modern students lack of respect for teachers and how it affects their ability to teach, and mismanagement of the educational system in general.    
 covertly filming the events of daily life inside the rubber room.     According to Garrett, the arrest came after the Department of Education learned of his intent to collect footage inside the rubber rooms for the exposé. 

The film was profiled in a 2008 segment of This American Life with Ira Glass called "Human Resources", which related various human resources problems in the country.    
 American public education, Waiting for "Superman", licensed footage shot by Five Boroughs Productions for The Rubber Room to use in Waiting for "Supermans" short segment on the reassignment centers. 

==Release and reception==
On April 12, 2010, the New York Department of Education (DoE) announced that it would be closing down its reassignment centers, facing pressures from the public and the teachers union.    

Due in part to media attention on the subject including the release of Waiting for "Superman" earlier in the year, as well as the recent settlement between the United Federation of Teachers (UFT) and DoE, the film was released to three full audiences on April 16, 2010, with largely positive reactions from the audience and critics in attendance.   

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 