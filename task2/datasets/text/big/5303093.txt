Garlic Is as Good as Ten Mothers
 
Garlic Is As Good As Ten Mothers (1980 in film|1980) is a documentary film about garlic directed by Les Blank. In 2004, the film was selected for preservation in the United States’ National Film Registry by the Library of Congress as being “culturally, historically, or aesthetically significant.”

It was filmed at the Gilroy Garlic Festival in Gilroy, California, as well as in other locations in Northern California. Its official premiere was at the 1980 Berlin Film Festival.

The director recommends that, when the film is shown, a toaster oven containing several heads of garlic be turned on in the rear of the theater, unbeknownst to the audience, with the intended result that approximately halfway through the showing the entire theater will be filled with the smell of garlic. 
 1982 film Burden of Dreams, a documentary chronicling the filming of Fitzcarraldo, director Werner Herzog and other crew members can be seen wearing "Garlic Is As Good As Ten Mothers" T-shirts. 

==Title==
The title is said to be a shortened form of the old saying "Garlic is as good as ten mothers... for keeping the girls away." 

==See also==
*All in This Tea

==External links==
* 
*  from Les Blank site
* 
 

 
 
 
 
 
 
 

 