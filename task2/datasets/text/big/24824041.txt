The Idiot Cycle
{{Infobox film
| name           = The Idiot Cycle
| image          = The Idiot Cycle.jpg
| alt            = 
| caption        = 
| director       = Emmanuelle Schick Garcia
| producer       = Emmanuelle Schick Garcia and Laila Tahhar
| writer         = Emmanuelle Schick Garcia
| narrator       = Emmanuelle Schick Garcia
| starring       = 
| music          = Joseph Guigui, David Dahan
| cinematography = Jean Marc Abela, Florian Geyer
| editing        = Yael Bitton
| studio         = JPS Films
| distributor    = JPS Films 
| released       =  
| runtime        = 96 minutes
| country        = France
| language       = French English
| gross          = 
}}
The Idiot Cycle is a 2009 French-Canadian documentary which alleges that six major chemical companies are responsible for decades of cancer causing chemicals and pollution, and also develop cancer treatments and drugs.  It also argues these companies own the most patents on genetically modified crops that have never been tested for long term health impacts like cancer.

==Production==
The film was shot in Canada (Sarnia, Ottawa, Toronto, ON and Saskatoon, SK), the United States (Memphis, TN, Midland, MI, and Washington DC), The Netherlands (Amsterdam), France (Agen, Paris), Italy (Bologna), Germany (Weilheim, Frankfurt, Bad Neihum, Burladingen, Tübingen), Belgium (Brussels), England (London, Hemel Hempstead), and Northern Ireland (Coleraine).

==Film festivals==

===Awards===
*"Green Report" Award - Romanian Film Festival Document.Art. 

===Appearances===
*RIDM (Montreal)
*IDFA (Amsterdam)
*OXDOX (Oxford)
*International Health Film Festival (Greece)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 