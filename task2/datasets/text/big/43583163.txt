Delitto passionale
{{Infobox film
| name           = Delitto passionale
| image          = Delitto passionale.jpg
| caption        = 
| director       = Flavio Mogherini
| producer       = Remo Angioli
| writer         = 
| starring       = Serena Grandi   Fabio Testi Florinda Bolkan
| music          = Gianni Ferrio
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1994  
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}

Delitto passionale is a 1994 Italian film directed by Flavio Mogherini. Story is based on a real life incident that took place in Eastern Europe.

==Plot==

Peters wife is murdered for having serious relationship with her boyfriend of London. Murder takes place in a hotel of Bulgaria, and the police begin investigating. It gets to know that Peter and his wife had no good relationship and his wife was thinking of divorce. Peter got a lover named Milena. Inspector Ivan Zanova investigates.

Tania, who came to Sofia from Florence, she decides to stay in the Bulgarian capital to collaborate with the servants in the estate of the house and, above all to take care of Anja, the daughter of the Peter Doncevs wife. Anja was paralyzed as a result of a nervous shock, she lives with the thought of recent dramatic death of her mother.

The police investigation remains on going, they have not yet established any results.

Meanwhile, Tania, now settled permanently in Doncev home, she decides to cooperate with the brother in law in the creation of a play. This causes the jealousy of Giulia, actress and colleague and romantically linked to Peter. Despite the feud, the success of the show is remarkable and Tania becomes a great value for the company. Tania became friends with Milena, part of the crew of Peter. One evening, while Milena is swimming in a swimming pool, a strange person enters, after 1 hour, the pool is virtually deserted, the murderer fire on a large amount of gunshots echoing terribly soundproof environment. Milena dies there.

The police states that the gun that killed Milena is the same one that killed the wife of Peter Doncev. The tramp is released and the police all over again. After suspicions, investigations, uncertainties police ends up indicting Peter Doncev that, in an attempt to escape, he loses his life. It is, therefore, attributed to Peter responsibility for two murders, but Ivan, the young police officer is not convinced, argument continues that Peter might be targeting someone else. Ivan rushes home and save Peter and Tania just in time, it is now established that the murderer is Anja, the young daughter of Peter who, suffering from nerve paralysis, she killed people who could pull her away from Peter.

Anja is submitted to a psychiatric hospital. The film ends with Tania after her visit to her niece. In her cell in the hospital, Anja, has fixed her gaze on a small animated puppet and compulsively repeats the phrase "Stay with me Daddy?". Tania after seeing the scene starts to cry. Before the credits of the film would start to appear, you are notified that the girl is still detained at a psychiatric hospital of the East Europe.

==Cast==

*Serena Grandi: Tania
*Fabio Testi: Peter
*Florinda Bolkan: Giulia
*Paul Martignetti: Ivan
*Anna Maria Petrova: Milena
*John Armstead: Chief Inspector
*Daniele Stroppa
*Cesare Barro
*Vassela Dimitrova
*Valcho Kamarashev
*Anya Pencheva
*Susanna Bugatti

==References==
 

==External links==
*  

 
 
 