Left Behind (2014 film)
 
{{Infobox film
| name           = Left Behind
| image          = Left Behind - Teaser Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Vic Armstrong
| producer       = {{Plainlist|
* Michael Walker
* Paul LaLonde}}
| writer         = {{Plainlist|
* Paul LaLonde
* John Patus}}
| based on       =  
| starring       =  {{Plainlist|
* Nicolas Cage
* Chad Michael Murray
* Cassi Thomson
* Nicky Whelan
* Jordin Sparks}}
| music          = Jack Lenz
| cinematography = Jack N. Green
| editing        = Vic Armstrong
| studio         = {{Plainlist|
* Entertainment One
* Stoney Lake Entertainment }}
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 110 minutes  
| country        = United States, Canada
| language       = English
| budget         = $16 million  
| gross          = $19.7 million   
}} apocalyptic Thriller thriller film novel of the same name written by Tim LaHaye and Jerry B. Jenkins, and starring Nicolas Cage, Chad Michael Murray, Cassi Thomson, Nicky Whelan, and Jordin Sparks, the film was released on October 3, 2014, and was universally panned by critics.

==Plot==
University of Central Arkansas student Chloe Steele has flown in from college to surprise her father, pilot Rayford Steele, for his birthday party. Her mother Irene Steele quickly calls to inform her, however, that her father cannot make it. While at the airport waiting for him, Chloe meets up with investigative reporter Cameron "Buck" Williams.

Rayford shows up on his way to a flight and apologizes to Chloe for missing her birthday party, insisting he was called in to pilot a flight to London at the last minute. He also assures Chloe things are fine between himself and his wife, who recently had become an active believing Christian, much to Chloe’s disgust. Chloe suspects things are not fine between her father and mother - she had seen him flirting with flight attendant Hattie Durham and notices he has removed his wedding ring. Her suspicions soon are confirmed when an airport worker hands Chloe two hard-to-get theater tickets in London that Rayford had ordered, indicating his trip to London and possible extramarital fling was planned all along.

Chloe brushes off another one of her mother’s preachings about Christianity and takes her brother to the mall. While there, her brother suddenly vanishes, leaving only his clothes behind. Chloe is shocked and notices this same thing has happened to numerous others at the mall. Mayhem breaks loose as shoppers begin looting the stores. A driver-less car plows through the mall windows, and a small plane without a pilot crashes in the mall parking lot. Chloe sees television reports of children and some adults disappearing, as worldwide panic sets in.

On Rayford’s flight, the same strange event has occurred – several people, including his co-pilot Chris Smith, Kimmy, one of the flight attendants, and all the children on board, have simply disappeared, leaving their clothing and personal effects behind. The remaining passengers panic and demand answers. Rayford does his best to reassure the passengers he will pass on information once he has any. Rayford has difficulty getting radio or cell phone contact with anyone on the ground, until he is finally informed that people have disappeared everywhere and the world is in uproar. Soon a pilot-less jet approaches directly into Rayford’s flight path. He narrowly avoids a midair collision but the jet damages Rayford’s fuel line. He decides his only option is to return to New York and hope his fuel holds out.

On the ground, Chloe hears her father’s Mayday call on her cell phone and assumes his plane has crashed. She later finds her mother’s jewelry left behind in the shower, as she has also disappeared. Chloe makes her way to New Hope Church to discover the family pastor Bruce Barnes who explains God has taken his believers to heaven, and the rest have to face the end of days. The pastor explains he was not taken because he didnt really believe what he had preached. Rayford comes to the same conclusion by examining his copilot and stewardess’ personal effects. He tells Hattie the truth about his wife. She is initially upset as she didnt know he was married, but Rayford convinces her to be brave and to help calm the passengers down until they can safely land.

Chloe climbs to the top of a bridge, intending to jump off it and end her life, when she gets a call from Buck, who is in the cockpit with Rayford. Rayford explains to Chloe all the New York area airports are closed and the streets full, and he is low on fuel and has nowhere to land. Chloe finds an abandoned Ford truck and uses it to clear away the equipment from a bridge under construction in order to create a makeshift runway. She uses her compass app and tells Rayford the coordinates of the landing site. Rayford is able to glide to a rough landing, saving the passengers, who leave the plane only to see the world aflame. Buck mentions that it looks like the end of the world, while Chloe informs him that it is just the beginning. 

==Cast==
 
 
* Nicolas Cage as Rayford Steele 
* Chad Michael Murray as Cameron "Buck" Williams 
* Cassi Thomson as Chloe Steele 
* Nicky Whelan as Hattie Durham
* Jordin Sparks as Shasta Carvell
* Lea Thompson as Irene Steele
 
* Lance E. Nichols as Pastor Bruce Barnes
* William Ragsdale as Chris Smith
* Martin Klebba as Melvin Weir
* Quinton Aaron as Simon
* Judd Lormand as Jim Stephanie Honore as Kimmy
 
* Gary Grubbs as Dennis
* Georgina Rawlings as Venice Baxter
* Lolo Jones as Lori
* Han Soto as Edwin
* Major Dodson as Rayford "Raymie" Steele, Jr.
* Alec Rayme as Hassid
 

==Production== settlement agreement legal dispute over the Left Behind film rights that began in 2000. 

On October 1, 2010, Cloud Ten again reacquired rights to the film.  Cloud Ten announced that they were rebooting the Left Behind film series. It was in the news that they planned on making the remake of the big-budgeted theatrically released film series based on the novel Left Behind. 

On October 31, 2011, it was reported that   together, had written a screenplay for the reboot.    On October 19, 2012, it was reported in The Hollywood Reporter that stunt artist Vic Armstrong was set to direct the reboot film with the budget around $15 million.   

===Casting===
On October 19, 2012, The Hollywood Reporter reported that Nicolas Cage was in talks to join the cast as a lead actor.    He portrays the lead role of Rayford Steele in the film.  In December 2012, it was reported that Chad Michael Murray was in talks to join the cast, to play the role of journalist Cameron "Buck" Williams.     On January 3, 2013, Ashley Tisdale joined to play the role of Chloe Steele in the film.     In March 2013, Tia Mowry was in talks to join the cast of the film; later she joined.    It was announced on August 9 that Tisdale dropped out due to scheduling conflicts. At that time, it was also announced that Jordin Sparks had signed on to join the cast. Sparks’ character is named Shasta.  On August 13, Cassi Thomson joined the lead cast, replacing Tisdale as Chloe Steele.    On August 19, 2013, Olympic bobsledder and hurdler Lolo Jones was announced as a cast member, portraying an airport gate attendant.  On September 9, 2013, Lea Thompson was announced to portray Rayford Steeles wife, Irene Steele|Irene. 

The character Nicolae Carpathia does not appear in the film. 

===Filming===
Principal photography began on August 9, 2013 in Baton Rouge, Louisiana.    

==Release==
The film was theatrically released on October 3, 2014 in the United States via Stoney Lake Entertainment.   

===Reception===
Left Behind received near universal negative reviews. On  , the film has a score of 12 out of 100, based on 25 critics, indicating "overwhelming dislike". 

In a review for Entertainment Weekly, film critic Lindsey Bahr writes, "At best, Left Behind is shoddily made sensationalist propaganda—with atrocious acting—that barely registers as entertainment. At worst, its profoundly moronic. Audiences, Christian or not, deserve better, and its hard to imagine that the ham-fisted revelations in this schlock could serve any higher purpose."   

Richard Roeper gave the film a grade of D−, stating that "the writing is horrible, the direction is clunky, the special effects are not special   the acting is so wooden you could make a basketball court out of it. Everything about this film feels forced and overwrought. With all due respect: Oh. My. God." 

On her ½ star review of the film, Linda Barnard from the Toronto Star writes, "The tantalizing prospect that this could have been a camp set-up of the Snakes on a Plane or Sharknado ilk pops up as Left Behind starts to echo 1970s flight deck-driven disaster films—and the parodies that followed. No such luck. Armstrong appears humourlessly earnest about his task. Score one for Satan."   

Even many Christian film critics were critical of Left Behind. Paul Chambers from MovieChambers.com begins his scathing review with, "There are millions of Christians with average or above-average intelligence. I’d like to think that I’m one of them. So, what possessed the makers of Left Behind to produce such an ignorant piece of garbage that’s easily one of the worst films of 2014, if not all-time?"   

Christian magazine Christianity Today heavily criticized the film, saying, "Left Behind is not a Christian movie, whatever Christian Movie could even possibly mean. In fact, most Christians within the world of the movie—whether the street-preacher lady at the airport or Rayford Steeles wife—are portrayed as insistent, crazy, delusional, or at the very least just really annoying. They want churches to book whole theaters and take their congregations, want it to be a Youth Group event, want magazines like this one to publish Discussion Questions at the end of their reviews—want the system to churn away, all the while netting them cash, without ever having to have cared a shred about actual Christian belief. They want to trick you into caring about the movie. Dont." And they would say that they "tried to give the film zero stars, but our tech system wont allow it." 

However, the film was praised by Tim LaHaye and Jerry Jenkins, the original authors of the Left Behind Series. Neither liked the 2000 version and LaHaye filed suit against it for breach of contract.  After watching an early screening of the film, LaHaye said “It is the best movie I have ever seen on the rapture”, while Jenkins said "I believe it does justice to the novel and will renew interest in the entire series.".  When asked if it was good, Jerry said “It’s better than good.” 

===Box office===
Left Behind played on 1,825 screens on its opening weekend, taking the number six position at the US box office with $6,300,147. The film gained 62 screens the following weekend, but posted a decline of 55% to end with $2,834,919. The third weekend saw the film dip to 923 screens and posted a 67.5% drop to $922,618. Its fourth weekend was the biggest drop yet for the movie at 71%, but rebounded in the later weeks. The final two weekends of the films US theatrical release, however, posted a robust 265% increase before ending its run on December 11, 2014.

It also debuted in Singapore and Russia on the same day as its US run (October 3), with the former grossing $101,585 from 13 screens and the latter grossing $620,636 from 470 screens. The following week, the film opened in three additional countries: Lebanon ($63,585 opening week) on October 8, and Malaysia ($303,833 opening week) and Philippines ($259,303 opening week) on October 9. All three countries had the highest debuts in the top three, with the countries of Lebanon and Philippines had the film ranked second place on their Box Office Chart. The film opened in two more countries the following week. The film ranked eighth place in Taiwan and came in second place in the United Arab Emirates with $336,544 from 47 screens.

Egypt and Brazil soon followed on October 22 and 23, taking in $24,951 and $633,096 respectively. Brazil is the first country to actually post a second-week increase (but only a scant +0.2% increase) and retaining its fifth place spot, making $634,150 (from 232 screens) for a total of $1,591,847. The film dropped 41% in its third week to end the weekend with $373,034 and a total of $2,221,533.

The movie debuted in South Africa on December 4, 2014, ranking 11th place with a total of $14,424.

On December 11, 2014, the film closed and had grossed $14,019,924 domestically and $5,677,122 overseas for a worldwide total of $19,697,047.  Earning in excess of $3,500,000 profit against its budget of $16,000,000.

===Accolades=== Razzies in Worst Picture, Worst Screenplay, Worst Actor for Nicolas Cage, losing all of them to Saving Christmas starring Kirk Cameron. 

==Home media==
Left Behind was released via DVD and Blu-ray Disc on January 6, 2015. 

==Sequels==
According to LaLonde, two sequels are planned, with Thomson, Murray, and Whelan all signed on to reprise their roles. 

On April 7, 2015 Paul Lalonde launched an Indiegogo crowdfunding campaign, asking for $500,000 in contributions.  If the goal is met, Lalonde plans to begin filming this autumn for a summer 2016 theatrical release.

==See also==
 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 