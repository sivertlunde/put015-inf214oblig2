Catch Me If You Can (1989 film)
{{Infobox film
| name           = Catch Me If You Can
| image          =
| caption        = Theatrical release poster
| director       = Stephen Sommers
| producer       = Jonnathan D. Krane Rachel Langsam Alan Lasoff Don Schain
| based on       = 
| screenplay     = Stephen Sommers Geoffrey Lewis M. Emmet Walsh Matt Lattanzi
| music          = Tangerine Dream Ronn Schmidt   
| editing        = Bob Ducsay MCA
| released       = July 28, 1989
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 3,686 
}}
 Geoffrey Lewis directorial debut of writer and director Stephen Sommers, with the soundtrack provided by Tangerine Dream.  

==Synopsis==
The movie follows the antics of high school students and their adventure in saving their school from being closed. Class president Melissa (Loryn Locklin) has started raising money through donations to keep the school open, but when the fundraising begins to slow down, Dylan (Matt Lattanzi) convinces Melissa that he can save the school with his plan. They take $3000 of the money that has already been raised to bet on an illegal car race that Dylan is convinced he would win. Dylan doesnt win the race and in order to get their money back, he is forced to double-down on an impossible race against the clock that only the town legend has ever accomplished. The film ends with a spectacular stunt with car and driver jumping through the goal-posts during a football game between two local high schools, Apollo and Cathedral.   

==Cast==
*Matt Lattanzi - Dylan Malone 
*Loryn Locklin - Melissa Hanson 
*Grant Heslov - Nevil 
*Billy Morrissette - Monkey  Geoffrey Lewis - Mr. Johnson 
*M. Emmet Walsh - Johnny Phatmun

==Production== Cathedral High Apollo High School, which Sommers attended. The movie was funded through independent money and had a budget of $800,000. The film was Sommers directorial debut and it was written by him as well. After production was finished on the movie, the studio that had promised to distribute the movie had already gone out of business, and it was eventually released by MCA Inc. in July 1989. The movie did not fare well at the box office, only grossing $3686 in its domestic run, but the film made 7 million dollars in the overseas market.  
 The Mummy, released in 1999 and the sequel in 2001, The Mummy Returns. 

==Soundtrack==
The films soundtrack was performed and recorded by Tangerine Dream, with all tracks composed by Edgar Froese and Paul Haslinger. 

===Tracklist===
# Dylans Future - 4:42  
# Sad Melissa - 2:07  
# Fast Eddies Car - 1:28  
# Back To The Race - 2:15  
# Melissa Asks Dylan Out - 1:14  
# Dylan Alone At Home - 3:32  
# Melissa Needs Help - 1:01  
# The Kiss - 0:59  
# Racing Montage - 3:40  
# The Clock Is Ticking - 2:13  
# Widow Maker - 1:41  
# Dylans Dream - 1:25  
# Taking The Test - 1:27  
# Back To The Race Again - 2:28  
# One More Chance - 2:25  
# Melissas Challenge - 1:25  
# Widow Maker Race - 2:43  
# Dylans Triumph - 4:17  
# Catch Me If You Can (Main Theme) - 1:45

==See also== Odd Thomas
*  The Jungle Book (1994) Van Helsing

==References==
 

==External links==
* 
* 

 

 
 
 
 
 