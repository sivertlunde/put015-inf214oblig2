Sky Larks
{{Infobox Hollywood cartoon
| cartoon name = Sky Larks
| series = Oswald the Lucky Rabbit
| image = Oswald in Sky Larks.jpg
| caption = Oswald and the big beagle about to go on the stratosphere flight to Mars. Bill Nolan Bill Nolan
| animator = Fred Avery Jack Carr Ray Abrams Joe DIgalo Ernest Smyth Victor McLeod
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release date =  
| color_process = Black and white
| runtime = 7:39
| language = English
| preceded_by = Ye Happy Pilgrims
| followed_by = Spring in the Park
}}
Sky Larks is an animated short produced by Walter Lantz Productions and is part of the Oswald the Lucky Rabbit series.

==Plot==
Oswald and a big beagle are at a cinema watching a documentary. The documentary tells about explorers who travel place to place in their hot air balloons. Oswald was amazed by what the explorers became famous for, and that he decided to try his luck.

One day at a fairground, a large crowd gathered to see Oswald take off in his balloon. The balloon is a large hot water bottle and the carriage is a metal stove (with switches inside). Oswald invited the big beagle, who was in the crowd, to join him.

The big beagle, at first, declined the invitation before he shakes hands with his little friend and releases the balloon. But as the balloon rises and one of its anchors snagged away his pants, the big beagle changed his mind. He would spend the entire ride on the anchor.

Oswalds balloon rapidly ascended into the heavens. They went so fast and so high that they even reached space. Their voyage ended when they landed on the planet Mars. Unfortunately for them, their balloon was wreck, and the two were left wondering if they would ever get back. They began walking on the rocky Martian surface. On the way, they saw a giant sipping soup and gobbling explosives. Because of the giants hostile nature, Oswald and the big beagle try to flee, only to unknowingly ran into the bore of a huge cannon. The cannon shoots them airborne and they landed in the giants soup bowl.

The giant was surprised to see the two visitors in his bowl. Nevertheless, he became obliged to gobble them as he scoops up Oswald and the big beagle with his spoon. Suddenly the giant decides to delay eating his victims. He then puts Oswald and the big beagle in a large salt shaker and then chooses have some music played by walking guns.

While the giant was enjoying his tunes, Oswald and the big beagle rocked the salt shaker back and forth until they turned it upside down. By putting their legs through the shakers holes, they were able run. As they make their move, they collided into a large pitcher, thus shattering the shaker and freeing them.

In no time, the giant notices their escape and tosses large forks on the table to entrap them. The giant then holds Oswald and the big beagle in separate hands, and their doom seem pretty much sealed.

It turns out that what Oswald and his buddy were experiencing was merely a bad dream as they were actually dozing at the cinema. The cinemas janitor came in, waking and telling them the film was over and all the audiences had left. Oswald and the big beagle quickly got up and started leaving hysterically.

==Notes==
*The big beagle is the father of the girl beagle (Oswalds third girlfriend). His appearance and personality are very similar to J. Wellington Wimpy|Wimpy, a character from the Popeye universe.
*The documentary Oswald and the big beagle watched at the cinema shows real-life people, particularly Swiss physicist Auguste Piccard in his balloon mission on how he went higher than anyone at that time. {{cite web
|url=http://lantz.goldenagecartoons.com/1934.html
|title=The Walter Lantz Cartune Encyclopedia: 1934
|accessdate=2011-06-03
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==See also==
* Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 