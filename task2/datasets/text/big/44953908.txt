Alice: Ignorance is Bliss
{{Infobox film
| name = Alice: Ignorance is Bliss
| image = Ignorance_is_Bliss,_Poster.jpg
| caption = Film poster
| director = Thomas McNaught
| starring = Alice Whinnett
| editing = Thomas McNaught
| released =  
| runtime = 7 minutes
| country = United Kingdom
| language = English
}}

Alice: Ignorance is Bliss is a 2014 short documentary by Thomas McNaught. It was featured on BBC Three’s FRESH scheme.   

The documentary was nominated for Best British Short Film    at the 28th Leeds International Film Festival – where it received a special mention    – and was shortlisted for Independent Age’s “Best Factual New Media Content" at the Older People Media Awards.   

==Plot==
In 2004, Alice Whinnett was diagnosed with dementia. Over the following decade her condition naturally worsened, and everything in her world changed, all apart from how her "favourite grandson" behaved with her. In this short documentary, grandson Thomas McNaught gives a brief look at the relationship between his 84-year-old grandmother and himself, showing a lighter side to the disease that breaks so many families apart.

==Cast==
* Alice Whinnett as herself

==References==
 

==External links==
*  

 
 
 
 

 