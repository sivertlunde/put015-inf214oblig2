Kalisundam Raa
 
 

{{Infobox film
| name           = Kalisundam Raa
| image          = Venkatesh Simran Simran
| producer       = Daggubati Suresh Babu|D. Suresh Babu D. Rama Naidu  
| writer         = Paruchuri Brothers 
| story          = Deenaraj Udaya Shankar
| screenplay     = Udaya Shankar Udaya Shankar
| music          = S. A. Rajkumar
| studio         = Suresh Productions
| cinematography = K Ravendra Babu
| editing        = Marthand K. Venkatesh K.Madhav
| released       = 14 January 2000
| language       = Telugu
| country        = India
| budget         =  
| gross revenue  =  
}}
 Telugu drama Simran in the lead roles and music composed by S. A. Rajkumar. The film has garnered the National Film Award for Best Feature Film in Telugu for that year.    The film was remade in Hindi as Kuch Tum Kaho Kuch Hum Kahein with Fardeen Khan and introducing Richa Pallod in 2002.

==Cast==
{{columns-list|3| Venkatesh as Raghu Simran as Manga
* K Viswanath as Raghaviah
* Srihari as Erra Babu
* Brahmanandam as Ramavadhani Ali as Snake Man AVS as Venkatadri
* M. S. Narayana as Lawyer Lingam
* Ahuti Prasad as Shankar Rao Ranganath as Ram Mohan Rao Rallapalli as Govindu
* Prasad Babu as Prasad
* Vinod as Siva
* Achyuth as Bhaskar Rao
* Raja Ravindra as Ram Mohan Raos son-in-law Ananth as Constable Chitti Babu as Subbavadhani
* Gundu Hanumantha Rao as Barber
* Gowtham Raju as Peon
* Chandra Mouli
* Madhu as Rowdy Jenny as Preist Annapurna as Janaki Venniradi Nirmala as Kousalya
* Rama Prabha as Kantham
* Vijaya Kumari as Aliveelu Mangatayaru
* Vinaya Prasad as Anasuya
* Sudha as Raghus aunty
* Sana as Parvathi
* Malika as Raghus aunty
* Kalpana
* Meena Kumari as Rajani
* Harika as Sirisha
* Deepika
* Manoja
* Lata
* Kalpana Rai as Rathalu
* Master Teja as Sandeep
* Master Aajaa Mohar
* Master Kireethi
* Master Pavan
* Master Aseem
* Baby Sindhura 
* Baby Bhagyasri
}}

==Soundtrack==
{{Infobox album
| Name        = Kalisundam Raa
| Tagline     = Living Together
| Type        = film
| Artist      = S. A. Rajkumar
| Cover       =
| Released    = 1999
| Recorded    =
| Genre       = Soundtrack
| Length      = 29:37
| Label       = Supreme Music
| Producer    = S. A. Rajkumar
| Reviews     =
| Last album  = Bobbili Vamsham   (1999) 
| This album  = Kalisundam Raa   (2000)
| Next album  = Maa Annayya   (2000)
}}

Music composed by S. A. Rajkumar. Music released on SUPREME Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:37
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Pacific lo Chandrabose
| extra1  = Udit Narayan, Anuradha Sriram
| length1 = 4:16

| title2  = Nuvve Nuvve
| lyrics2 = Sirivennela Sitaramasastri Sujatha
| length2 = 4:23

| title3  = Prema Prema
| lyrics3 = Sirivennela Sitaramasastri Unni Krishnan
| length3 = 1:59

| title4  = Manasu Manasu
| lyrics4 = Veturi Sundararama Murthy SP Balu, Chitra
| length4 = 4:40

| title5  = Kalisunte Kaladusukam
| lyrics5 = Veturi Sundararama Murthy
| extra5  = Rajesh
| length5 = 4:42

| title6  = Boom Boom  
| lyrics6 = Chandrabose
| extra6  = Shankar Mahadevan
| length6 = 5:05

| title7  = Nachave Palapitta  
| lyrics7 = Chandrabose
| extra7  = SP Balu, Swarnalatha
| length7 = 4:11
}}

==Remakes==
{|class="wikitable"
|-
! Year !! Film !! Language !! Cast
|-
| 2002 || Kuch Tum Kaho Kuch Hum Kahein || Hindi || Fardeen Khan, Richa Pallod
|-
| 2003 || Ondagona Baa || Kannada || V Ravichandran, Shilpa Shetty
|-
|}

==Box office==
* Kalisundham Raa was rated as the biggest hit in Telugu cinema in 70 years until the release of Narasimha Naidu in 2001. 
* It ran for record 100 days in 103 centres, 175 days in 35 centres and 200 days in 20 centres.
* It broke the records made by Samarasimha Reddy in 1999.

==Awards== National Film Awards
* National Film Award for Best Feature Film in Telugu

;Nandi Awards Best Feature Film (Gold) - D. Suresh Babu  Best Actor Venkatesh
* Best Supporting Actor - K. Vishwanath Best Story Writer - Dinraj and Uday Shankar

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 