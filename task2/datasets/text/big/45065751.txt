Joanna (2013 film)
{{Infobox film
| name           = Joanna
| image          = 
| caption        = 
| director       = Aneta Kopacz
| producer       = 
| writer         = Aneta Kopacz, Tomasz Średniawa
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 45 minutes
| country        = 
| language       = 
}} Polish documentary film by Aneta Kopacz about Joanna Salyga, a young woman with cancer with only three months to live, and her efforts to make the most of her time with her husband and young son. Kopacz came to know Salygas story through her blog Chustka. Salygas readers sponsored a crowdfunding campaign to produce this film. Joanna was nominated for the Academy Award for Best Documentary (Short Subject) at the 87th Academy Awards.    

{| class="wikitable"
! colspan="5" style="background: LightSteelBlue;" | Awards
|-
! Award 
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| Academy Award
| February 22, 2015 Best Short Subject Documentary 
| Aneta Kopacz
|  
|-
|}
  
==References==
 

==External links==

*  

 
 
 
 
 
 
 
 
 
 

 