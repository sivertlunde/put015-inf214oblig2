Assia and the Hen with the Golden Eggs
 
{{Infobox film
| name           = Assia and the Hen with the Golden Eggs
| image          =
| caption        =
| director       = Andrei Konchalovsky
| producer       = Sergei Bayev
| writer         = Andrei Konchalovsky Viktor Merezhko
| starring       = Inna Churikovai
| music          =
| cinematography = Yevgeni Guslinsky
| editing        = Yelena Gagarina
| distributor    =
| released       =  
| runtime        = 117 minutes
| country        = Russia
| language       = Russian
| budget         =
}}

Assia and the Hen with the Golden Eggs ( , Transliteration|translit.&nbsp;Kurochka Ryaba) is a 1994 Russian comedy film directed by Andrei Konchalovsky. It was entered into the 1994 Cannes Film Festival.   

==Cast==
* Inna Churikova - Asya
* Viktor Mikhaylov - Vasili Nikitich Aleksandr Surin - Stepan
* Gennadi Yegorychyov - Chirkunov
* Gennadi Nazarov - Seryozha
* Mikhail Kislov
* Mikhail Kononov
* Lyubov Sokolova
* Aleksandr Chislov
* Tatyana Bukamina
* Yevgeni Shkayev
* Larisa Kudryashova

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 