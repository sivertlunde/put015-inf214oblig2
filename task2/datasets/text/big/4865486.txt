Code Rush
 

{{Infobox film name           = Code Rush image          = File:Code Rush cover.png director       = David Winton producer       =  cinematography = editing        = studio         = distributor    = released       = March 30, 2000 (TV) April 25, 2000 (VHS) runtime        = 56 minutes country        =  language  English
|budget         = gross          =
}}

Code Rush is a 2000 documentary following the lives of a group of Netscape engineers in Silicon Valley. It covers Netscapes last year as an independent company, from their announcement of the Mozilla open source project until their acquisition by AOL. It particularly focuses on the last minute rush to make the Mozilla source code ready for release by the deadline of March 31, 1998, and the impact on the engineers lives and families as they attempt to save the company from ruin.

After Andy Baio uploaded the documentary to his personal website for the release of Mozilla Firefox 3 in 2009, director David Winton requested it be taken down, pending his decision about future distribution under a free content license. It has since been released under the Creative Commons Creative Commons licenses|BY-NC-SA 3.0 US license.  

== Featured Netscape employees ==
* Jim Barksdale, CEO
* Scott Collins
* Tara Hernandez
* Stuart Parmenter, then a 16-year-old open-source volunteer
* Jim Roskind
* Michael Toy, co-author of Rogue (video game)|Rogue
* Jamie Zawinski

== References ==
 

== External links ==
*  
*  
* Annotated Code Rush  
* Download/Watch Code Rush  
*  

 
 
 
  
 
 
 
 
 
 
 
 
 

 