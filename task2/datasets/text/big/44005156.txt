Checkpost (film)
{{Infobox film 
| name           = Checkpost
| image          =
| caption        =
| director       = JD Thottan
| producer       = JD Thottan
| writer         = JD Thottan S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan Sathyan Adoor Bhasi Thikkurissi Sukumaran Nair Muthukulam Raghavan Pillai
| music          = PS Divakar
| cinematography = 
| editing        = VP Krishnan
| studio         = T&T Productions
| distributor    = T&T Productions
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, directed and produced by JD Thottan . The film stars Sathyan (actor)|Sathyan, Adoor Bhasi, Thikkurissi Sukumaran Nair and Muthukulam Raghavan Pillai in lead roles. The film had musical score by PS Divakar.   

==Cast==
  Sathyan
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Muthukulam Raghavan Pillai
*Pattom Sadan Ambika
*Bahadoor
*Kamaladevi
*Kottarakkara Sreedharan Nair
*S. P. Pillai Sadhana
*Shailasree
*Ushanandini
 

==Soundtrack==
The music was composed by PS Divakar and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kalluvalayitta Kayyil || Pattom Sadan || P. Bhaskaran || 
|-
| 2 || Poothachedayan Kaadu || K. J. Yesudas, Kalyani Menon || P. Bhaskaran || 
|-
| 3 || September Moon || K. J. Yesudas, Latha Raju || P. Bhaskaran || 
|-
| 4 || Thaalolam Kiliyude || S Janaki || P. Bhaskaran || 
|-
| 5 || Thaamarathoniyil || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 