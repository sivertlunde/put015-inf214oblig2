Mr. Nice
{{Infobox film
| name           = Mr Nice
| image          = Mr. Nice.jpg
| image_size     = 
| caption        =  Bernard Rose
| producer       = Luc Roeg
| based on       =   
| screenplay     = Bernard Rose
| narrator       = 
| starring       =  
| music          = Philip Glass
| cinematography = Bernard Rose
| studio     = Independent Film Productions Kanzaman
| editing        =  
| distributor    = Contender Entertainment (UK) MPI Media Group (US)
| released       =  
| runtime        = 121 minutes
| country        =  
| language       = English
| budget         = 
| gross          = £528,534
}} 2010 British-Spanish Bernard Rose, Mr Nice, cult autobiography by Howard Marks. The film features an ensemble cast starring Rhys Ifans as Howard Marks (with Marks himself giving Ifans instruction), along with David Thewlis, Omid Djalili and Jack Huston, with Crispin Glover and Chloë Sevigny.
 Welsh marijuana smuggler who on the IRA and Terre Haute, pro cannabis campaigner, stand-up comedian, actor (at least in cameo appearance|cameos), lads mag columnist, television show panelist, music producer, motivational speaker, and even prospective Member of Parliament.

Like Marks autobiography on which it is based, the film has polarized critics.

==Plot==
The film begins in 2010, with a 65-year-old Howard Marks going onto a stage in front of a packed theatre to great applause. Marks asks if there are any undercover|plain-clothes policemen sat in the audience, to which the reply is negative. Then, while lighting up a joint (cannabis)|joint, Marks asks "who here is a dope (marijuana) smoker?", to even greater applause. The film then diverts into Marks internal monologue as he recounts his life.
 Welsh valleys national standard of the United Kingdom. This remarkable aptitude earns him a scholarship to Oxford University at nineteen years old, reading philosophy and physics, but Marks destiny changes forever one night when dutifully studying alone in his dorm. A beautiful, rebellious and hedonistic foreign exchange student from Latvia, Ilze Kadegis (Elsa Pataky) - breaks into Marks room, looking for a secret passageway within. Marks follows Kadegis through the secret passageway and into a forgotten storage space used by one of the schools top marijuana dealers, Graham Plinson (Jack Huston). Kadegis seduces Marks, and introduces him to cannabis for the first time. For the next few years, Marks becomes an enthusiastic customer of Plinsons, and continues his love affairs with both Kadegis and cannabis; the group enjoy a series of wilder and wilder nights, with their academic lives suffering as a result.
 harder variety of drugs. The trio of Marks, Kadegis and Plinson promise to each other to turn over a new leaf, and they pass their scholarships through some intense last-minute revision, and a little cheating. They then all move onto teacher training jobs at the University of London in 1967, where Marks hastily marries Kadegis. Fractures begin appearing in the early stages of the marriage, with Marks becoming despondent, apathetic and suspecting Kadegis of having an affair. Whats more, Marks gets into trouble at the London University for "having long hair and flashy suits".

When plans to bring a large cache of hashish into England via Germany go wrong with Plinson getting arrested, Marks steps in to help - figuring he has nothing left to lose anyway. Marks goes to Germany and drives the car with the stash inside across the borders himself, simply driving through customs. The customs officers are on the lookout for Plinsons crew, but do not know Marks, who  sails through without incident. Marks says the thrill of getting away with it was like "religious flash and an asexual orgasm". When selling the hashish back in London to an Arab oil sheik named Saleem Malik (Omid Djalili), Marks makes a fortune, and swiftly becomes addicted to this new but dangerous lifestyle as a big league marijuana trafficker - eventually running a large percentage of the worlds cannabis.

It is a path that will lead Marks face-to-face with terrorists, government agents, and lose him his freedom to one of the toughest prisons in the United States in 1988, through to the present day as a media personality and cult hero.

==Cast==
*Rhys Ifans as Howard Marks
*Chloë Sevigny as Judy Marks
*Jack Huston as Graham Plinson
*Crispin Glover as Ernie Combs James McCann
*Omid Djalili as Saleem Malik
*Elsa Pataky as Ilze
*Andrew Tiernan as Alan Marcuson Jamie Harris as Patrick Lane
*Christian McKay as Hamilton McMillan
*Ken Russell as Russell Miegs

==Production==
After Howard Marks 1997 autobiography Mr Nice became a best-seller (ranked #1 book regarding drugs on  , Spain.  The film features a 1960s pop-inspired soundtrack by Philip Glass, with original songs reflective of the era such as Deep Purple, Fraternity of Man and John Lennon. 

===Differences from the autobiography=== Lord Moynihan, peer embroiled in vice scandal. Marks alleged Lord Moynihan betrayed him to the American Drug Enforcement Administration in a plea bargain, although there is no mention of this in the film.

==Reception==

===Box office===
Mr Nice made its worldwide premiere at the South by Southwest Film Festival in Texas, United States, in March 2010  and was first shown in the UK at the Edinburgh Film Festival in June 2010.  On 8 October 2010, it was released in the UK on 107 screens, taking in a first weekend gross of £528,534.  In June 2011, it was released in the United States.

===Awards===
Mr Nice won the Award for Best Cinematography at the 2010 Kodak Awards. 

===Critical Reception===
Mr Nice polarized critics; the film currently has a rating of 55% on Rotten Tomatoes, with 24 out of 44 reviews being positive, and 20 being negative. 

Dan Jolin of the film magazine Empire magazine|Empire gave Mr Nice three out of five stars, writing "A solid, often entertaining life-of-crimer which benefits from some stylistic touches and a faithful, convincing central performance."  

Kevin Thomas of Los Angeles Times gave the film 3.5 out of 5, calling the Philip Glass soundtrack "pulsating" and writing "Though the film takes a while to cast its spell, writer-director-cinematographer Bernard Roses close observation of Marks and those around him becomes increasingly involving and allows Rose to comment on the widespread failure of the War on Drugs." 

  staple throughout the land – struggles to capture the sheer breadth of Marks life." 

Benjamin Mercer of Village Voice also gave a polarized review, awarding the film 3.5 out of 5, yet also claiming the film gave him the impression it was glorifying cannabis use, or at the very least, being a vehicle for the advocation of legalizing cannabis - "Though told here with appealing drollness, Marks story makes an odd vessel for the filmmakers casually advanced legalization arguments, what with its mischief making on the grandest scale possible." 

==References==
 

==External links==
*  
*   
*  
 

 
 
 
 
 
 
 
 
 