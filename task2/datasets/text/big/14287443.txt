Luke's Busy Day
 
{{Infobox film
| name           = Lukes Busy Day
| image          =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film starring Harold Lloyd.   

==Cast==
* Harold Lloyd - Lonesome Luke
* Bebe Daniels
* Snub Pollard
* Bud Jamison
* Sidney De Gray
* Earl Mohan
* C.G. King
* Max Hamburger
* Norman Napier
* W.L. Adams
* Gus Leonard
* William Brown - (as William N. Brown)
* H. Granger
* J.A. Irvin - (as J. Irvine)
* Frank Lake
* Marie Mosquini
* Virgil Owens - (as Virgil Owen)
* Harry Russell

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 