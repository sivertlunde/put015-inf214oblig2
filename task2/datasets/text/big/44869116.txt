The House of Lies
 
{{Infobox film
| name           = The House of Lies
| image          = The House of Lies 2.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Desmond Taylor
| producer       = Oliver Morosco
| screenplay     = L.V. Jefferson 
| starring       = Edna Goodrich Juan de la Cruz Kathleen Kirkham Lucille Ward Harold Holland Herbert Standing
| music          = 
| cinematography = Homer Scott
| editing        = 
| studio         = Oliver Morosco Photoplay Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by William Desmond Taylor and written by L.V. Jefferson. The film stars Edna Goodrich, Juan de la Cruz, Kathleen Kirkham, Lucille Ward, Harold Holland and Herbert Standing. The film was released on September 14, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==  
*Edna Goodrich as Edna Coleman
*Juan de la Cruz as Marcus Auriel
*Kathleen Kirkham as Dorothy
*Lucille Ward as Mrs. Coleman
*Harold Holland as Winthrop Haynes
*Herbert Standing as Dr. Barnes 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 