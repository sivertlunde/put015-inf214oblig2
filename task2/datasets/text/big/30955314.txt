The Happy Life
{{Infobox film name           = The Happy Life image          = The Happy Life Poster.jpeg
| film name = {{Film name hangul         =   hanja          = 즐거운  rr             = Jeulgeoun insaeng mr             = Chŭlgŏun Insaeng }} director       = Lee Joon-ik producer       = Jo Cheol-Hyeon writer         = Choi Seok-hwan starring  Kim Sang-ho Jang Keun-suk music          = Lee Byung-woo editing        = Kim Sang-beom Kim Jae-beom
| cinematography = Kim Yeong-chul distributor    = CJ Entertainment released       =   runtime        = 112 minutes country        = South Korea language       = Korean budget         =  gross          = $8,664,972 
}} 2007 South Korean film directed by Lee Joon-ik, whose previous film, The King and the Clown was the highest grossing Korean film of 2005. The film sold 1,263,835 tickets nationwide . 

== Synopsis ==
Sang-woo, the leader of college rock band Active Volcano, dies and sets up a reunion for Gi-yeong and the other members of the group. Former bass player Seong-wook lives a hand-to-mouth existence working two jobs. Drummer Hyeok-su is a single father struggling to make a living as a car salesman. The jobless lead guitarist Gi-yeong dreams of taking over Volcano as the new frontman. When he suggests they reform the band while the old friends reminisce at the funeral, they all spurn the idea. But Gi-yeong persists and gets each to relent, setting the stage for a rock and roll reunion. 

== Cast ==
*Jung Jin-young
*Kim Yoon-seok Kim Sang-ho
*Jang Keun-suk

== Musical theatre adaptation == Ryan alternated as Sae-ki.

== References ==
 

 

 
 
 
 
 
 
 
 
 


 
 