Melting Pot (film)
{{Infobox film
| name = Melting Pot
| image =
| caption = Promotional Poster for "Race"
| director = Tom Musca
| writer = Mark Kemble Tom Musca
| cinematography = Arturo Smith
| distributor = A-Pix Entertainment, Ardustry Home Entertainment LLC
| runtime = 104 minutes
| language = English
| country = United States
}}
Race (movie) redirects here.  For the animated movie see Race (animated film)

Melting Pot, also known as Race, is a 1998 feature film directed by Tom Musca (notable writer and producer of Stand and Deliver).

==Cast==
* Paul Rodriguez - Gustavo Alvarez 
* CCH Pounder - Lucinda Davis 
* Cliff Robertson - Jack Durman 
* Una Damon - Chungmi Kong 
* Annette Murphy - Reyna Álvarez 
* Efren Ramirez - Miguel Álvarez 
* Lillian Hurst - Grandma Álvarez
* Peter Krause - Pedro Marine 
* Danielle Nicolet - Deuandranice 
* Judy Herrera - Dolores 
* Winston J. Rocha - Carlos 
* Brian Poth - Walter Cahill Jr. 
* Leticia Robles - Latina Reporter 
* Bruce Sabath - White Cop 
* John Mooney - Walter Cahill Sr. (as John C. Mooney) 
* Parul Bartel - Moderator
* Luis Guizar - Iannicito

==External links==
*  

 
 
 
 
 
 


 