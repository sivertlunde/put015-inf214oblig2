Pareni Maya Jalaima
{{Infobox film 
| name = Pareni Maya Jalaima
| director = Prithvi Rana Magar/ Ramsharan Ghimire
| producer = Prithvi Rana Magar
| starring = Jharana Bajracharya, Dilip Raymajhi, Uttam Pradhan, Prithvi Rana Magsr, Sunil Thapa, Mithila Sharma, Laxmi Giri, and Hritika Gurung
| music = Sachin Sing
| cinematography = Gavin Liew
| editing = Shiva Puri
| released = 2004
| runtime = 132 minutes
| country = Nepal Nepali
}}
 Nepali alternative 2004 movie filmed 80% in Hong Kong with the remaining footage shot in Nepal. It was nominated for best story, best screenplay, and best cinematography at the 2005 National Film Festival, and it won the best screenplay award for writer-director Prithvi Rana Magar.  It is the most expensive film ever made in the history of Nepalese Cinema with a production cost of about 18 million rupees. It was shot on 35-millimeter film—a rarity for Nepalese movies—by Hong Kong cinematographer Gavin Liew. Because of a long and exhausting legal battle between the investors, the films release was delayed for three years. By the time it hit theaters, the charm of the highly anticipated movie had vanished. Although people who watched the movie liked and appreciated it, it was not successful at the box office because of poor marketing and excessive use of the English language, which Nepalese audiences are not accustomed to. The movie only made around 30% of its investment.

==Cast==
* Dilip Rayamajhi as Himal
* Uttam Pradhanas Suraj
* Dikshya Baral as Naina
* Jharana Bajracharya as Kiran
* Mitthila Sharma
* Sunil Thapa
* Prithvi Rana Magar
* Laxmi Giri
* Hritika Gurung

==Plot==
Man Bahadur (Prithvi Rana Magar) is an ex-Gurkha soldier living in Hong Kong with his family. He has a son named Himal (Dilip Raimajhi), a nephew Suraj (Uttam Pradhan)—whom he has looked after since childhood—and his wife (Mithila Sharma). Himal and Suraj are of the same age and are close. However, there is a great difference in their personalitites. Himal’s thinking is Westernized and has friends of poor character. Himal uses drugs occasionally and is dating a girl named Jenny from the Philippines. He spends most of his time in discos, rather than with his family or at work. On the other hand, Suraj has a good work ethic and, unlike Himal, likes Nepalese music, food., and culture. Man Bahadur and his wife are concerned about Himal’s future: Man Bahadur fears their son has escaped their influence, while his wife has not lost hope yet. She thinks if Himal marries a Nepalese girl, everything will be all right. 

One day in a disco, Himal gets drunk and, not knowing what he is doing, kisses a girl against her will. A fight breaks out between Himal and the girl’s boyfriend, and Himal is badly beaten up. Man Bahadurs worst fears are becoming real now. However, his wife is still optimistic and insists on finding a Nepalese girl for him. Man Bahadur resists at first, but eventually gives up to his wifes pressure and agrees to write to his childhood friend Bal Bahadur (Sunil Thapa) for his daughter Kiran’s (Jharana Bajracharya) hand in marriage for Himal. Man proposes that Bal send his daughter to Hong Kong as a tourist; during her stay in Hong Kong, if Himal and Kiran like each other, only then will their parents announce the marriage. Bal is delighted, thinking Himal is the perfect groom for his daughter and hoping she can escape hardship in Nepal. After consulting his wife, Bal sends Kiran to Hong Kong, telling her that it is their gift for her college vacation. 

Kiran arrives in Hong Kong, and meets Himal and Suraj; during her stay with the family she gets to know them and finds herself gradually falling in love with Suraj. Himal, on the other hand, is happy with his girlfriend Jenny, drinking and using drugs. Due to absenteeism at work, he is fired from his job, refuses to get another one, and runs through his savings; this drives Jenny to leave him. Himal incurs a serious head injury in a fight with Jennys new boyfriend and his friends. While in the hospital after surgery, he realizes the error of his ways and vows to turn over a new leaf. Out of gratitude to Man Bahadur and his family, Kiran visits Himal in the hospital and takes care of him. After spending times with Kiran, Himal finds her attractive and slowly starts liking her, but he doesnt know that she is already in love with Suraj.

Man Bahadur and his wife see that Himal and Kiran like each other; thinking Kiran is responsible for Himals rehabilitation. they announce their engagement at a party, which stuns Suraj and Kiran. Kiran leaves the party; Suraj follows her and, after a long talk, convinces Kiran to marry Himal to keep the family together. What happens next? Better leave it here as what happens next is astonishing.

==External links==
*  

 
 
 
 
 
 