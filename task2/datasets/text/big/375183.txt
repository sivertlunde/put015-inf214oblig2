Enemy at the Gates
 
{{Infobox film
| name           = Enemy at the Gates
| image          = Enemy at the gates ver2.jpg 
| image_size     =
| caption        = Film poster
| director       = Jean-Jacques Annaud
| producer       = Jean-Jacques Annaud John D. Schofield
| writer         = Jean-Jacques Annaud Alain Godard
| based on       =  
| starring       = Joseph Fiennes Rachel Weisz Jude Law Bob Hoskins Ed Harris Ron Perlman
| music          = James Horner Robert Fraisse 
| editing        = Noëlle Boisson Humphrey Dixon
| studio         = Mandalay Pictures Repérage Films   Retrieved 2012-06-27 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 131 minutes
| country        = France  Germany United Kingdom Ireland United States English German German Russian Russian
| budget         = $68,000,000   
| gross          = $96,976,270 
}}

Enemy at the Gates is a 2001 war film directed by Jean-Jacques Annaud.

The films title is taken from  , which describes the events surrounding the Battle of Stalingrad in the winter of 1942/43.  While fictional, the film is loosely based on war stories told by Soviet sniper Vasily Zaytsev.

== Plot == commisar Danilov (Joseph Fiennes). Nikita Khrushchev (Bob Hoskins) arrives in Stalingrad to coordinate the citys defences and demands ideas to improve morale. Danilov, now a Senior Lieutenant, suggests that the people need figures to idolize and give them hope, and publishes tales of Vasilys exploits in the armys newspaper that paint him as a national hero and propaganda icon. Vasily is transferred to the sniper division, and he and Danilov become friends. They also both become romantically interested in Tania (Rachel Weisz), a citizen of Stalingrad who has become a Private in the local militia. Danilov has her transferred to an intelligence unit away from the battlefield.

With the Soviet snipers taking an increasing toll on the German forces (by particularly seeking out officers), German Major Erwin König (Ed Harris) is deployed to Stalingrad to take out Vasily and thus crush Soviet morale. A renowned marksman and head of the German Army sniper school at Zossen, he lures Vasily into a trap and takes out two of his fellow snipers, but Vasily manages to escape. When the Red Army command learns of Königs mission, they dispatch his former student Koulikov (Ron Perlman) to help Vasily kill him. König, however, outmaneuvers Kulikov and kills him with a very skillful shot, shaking Vasilys spirits considerably. Khrushchev pressures Danilov to bring the sniper standoff to a conclusion.

Sasha, a young Soviet boy, volunteers to act as a double agent by passing König false information about Vasilys whereabouts, thus giving Vasily a chance to ambush the Major. Vasily sets a trap for König and manages to wound him, but during a second attempt Vasily falls asleep after many hours and his sniper log is taken by a looting German soldier. The German command takes the log as evidence of Vasilys death and plans to send König home, but the Major does not believe that Vasily is dead. The commanding German general takes Königs dog tags to prevent Russian propaganda from profiting if König is killed. König also gives the general a War Merit Cross that was posthumously awarded to Königs son, who was a lieutenant in the 116th infantry division and was killed in the early days of the battle for Stalingrad. This reveals Königs reason for volunteering for the mission. König tells Sasha where he will be next, suspecting that the boy will tell Vasily. Tania and Vasily have meanwhile fallen in love, and the jealous Danilov disparages Vasily in a letter to his superiors.

König spots Tania and Vasily waiting for him at his next ambush spot, confirming his suspicions about Sasha. He reluctantly kills the boy and hangs his body off a pole to bait Vasily. Vasily vows to kill König and sends Tania and Danilov to evacuate Sashas mother (Eva Mattes) from the city, but Tania is wounded by shrapnel en route to the evacuation boats. Thinking her dead, Danilov regrets his jealousy of Vasily and his resulting disenchantment with the communist cause. Finding Vasily waiting to ambush König, Danilov intentionally exposes himself in order to provoke König into shooting him and exposing Königs position. Thinking he has killed Vasily, König goes to inspect the body, but realizes too late that he has fallen into a trap and is in Vasilys sights. He turns to face Vasily, taking off his hat in a final gesture of respect, after which Vasily kills him. Two months later, after Stalingrad has been liberated and the German forces have surrendered, Vasily finds Tania recovering in a field hospital.

== Main cast ==
 
*Joseph Fiennes – Commisar Danilov
*Rachel Weisz – Tania Chernova
*Jude Law – Vasily Zaytsev
*Bob Hoskins – Nikita Khrushchev
*Ed Harris – Major Erwin König
*Ron Perlman – Kulikov
*Eva Mattes – Mother Filippova
*Gabriel Thomson – Sasha Filippov
*Matthias Habich – General Friedrich Paulus
*Sophie Rois – Ludmilla
*Ivan Shvedoff – Volodya
*Mario Bandi – Anton
*Gennadi Vengerov – Starshina
*Mikhail Matveyev – Grandfather
 

==Historical accuracy== 284th Tomsk Rifle Division. He was interviewed by Vasily Grossman during the battle, and the account of that interview, lightly fictionalized in his novel, Life and Fate (Part One, Chapter 55), is substantially the same as that shown in the movie, without putting a name to the German sniper with whom he dueled.

Historian Antony Beevor suggests in his book Stalingrad (book)|Stalingrad that, while Zaytsev was a real person, the story of his duel (dramatised in the film) with König is fictional. Although William Craigs book Enemy at the Gates: The Battle for Stalingrad includes a "snipers duel" between Zaytsev and König, the sequence of events in the film is fictional. Zaytsev claimed in an interview to have engaged in a sniper duel over a number of days. Zaytsev, the only historical source for the story, stated that after killing the German sniper, and on collecting his tags, he found that he had killed the head of the Berlin Sniper School.  No sniper named König has ever been identified in the German records.

In the film, Jude Law uses a 7.62x54r Mosin Model 1891/30 sniper rifle with a PU 3.5 power sniper scope (i.e. the image is magnified 3 and a half times). Vasily Zaytsev used a Model 1891/30 sniper rifle with an earlier and larger sniper telescope  (his rifle is preserved in Stalingrad History Museum in Russia) . Also, the poster for the film reverses the Mosin 91/30 rifle photograph so that the bolt handle appears on the left side of the rifle, instead of the right side where it should be.

The love story between Vasily and Tania has no basis in Zaytsevs memoirs.

== Reception == average score normalized rating in the 0–100 range based on reviews from top mainstream critics, calculated an average score of 53, based on 33 reviews.   
 New York Magazines Peter Ranier was less kind, declaring "Its as if an obsessed film nut had decided to collect every bad war-film convention on one computer and program it to spit out a script."  Peter Travers of Rolling Stone admitted the film had faults, but that "any flaws in execution pale against those moments when the film brings history to vital life." 

The film was poorly received in the former Soviet Union.  Some Red Army Stalingrad veterans were so offended by inaccuracies in the film and how the Red Army was portrayed that on 7 May 2001, shortly after the film premiered in Russia, they expressed their displeasure in the Duma, demanding a ban of the film, but their request was not granted.  

The film was received poorly in Germany. Critics claimed that it simplified history and glorified war.    At the Berlinale film festival, it was booed. Annaud stated afterwards that he would not present another film at Berlinale, calling it a "slaughterhouse" and claiming that his film received much better reception elsewhere.  

==Soundtrack==

The soundtrack to Enemy at the Gates was released on March 13, 2001.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 76:31 

| title1          = The River Crossing to Stalingrad
| length1         = 15:13
| extra1          = James Horner

| title2          = The Hunter Becomes the Hunted
| length2         = 5:53
| extra2          = James Horner

| title3          = Vassilis Fame Spreads
| length3         = 3:40
| extra3          = James Horner

| title4          = Koulikov
| length4         = 5:13
| extra4          = James Horner

| title5          = The Dream
| length5         = 2:35
| extra5          = James Horner

| title6          = Bitter News
| length6         = 2:38
| extra6          = James Horner

| title7          = The Tractor Factory
| length7         = 6:43
| extra7          = James Horner

| title8          = A Snipers War
| length8         = 3:25
| extra8          = James Horner

| title9          = Sachas Risk
| length9         = 5:37
| extra9          = James Horner

| title10         = Betrayal
| length10        = 11:28
| extra10         = James Horner

| title11         = Danilovs Confession
| length11        = 7:13
| extra11         = James Horner

| title12         = Tania (End Credits)
| length12        = 6:53
| extra12         = James Horner

}}

==Notes==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   Reviewed by David R. Stone, History Department, Kansas State University, published by H-War (June, 2002)
*   Reviewed by Valeri Potapov

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 