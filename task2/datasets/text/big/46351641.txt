Paula (1915 film)
{{Infobox film
| name           = Paula
| image          =
| caption        =
| director       = Cecil Birch
| producer       =  Victoria Cross (novel)
| starring       = Hetty Payne   Frank McClellan
| cinematography = 
| editing        = 
| studio         = Holmfirth Films
| distributor    = Initial Films
| released       = November 1915
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
Paula is a 1915 British silent drama film directed by Cecil Birch and starring Hetty Payne and Frank McClellan. It was made at the Holmfirth Studios in Yorkshire.  A widow follows her love to Italy, and dies after donating blood to save his life.

==Cast==
*  Hetty Payne as Paula 
* Frank McClellan as Vincent Hallam  

==References==
 

==Bibliography==
* Warren, Patricia. British Film Studios: An Illustrated History. Batsford, 2001.

==External links==
* 

 
 
 
 
 
 
 

 