Dance of the Damned
{{Infobox film
| name           = Dance of the Damned
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Katt Shea
| producer       = 
| writers        = 
| screenplay     = 
| story          = 
| based on       = 
| starring       = 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1989
| runtime        = 
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
Dance of the Damned is a 1989 American  film directed by Katt Shea and executive produced by Roger Corman.
==Production==
Katt Shea later recalled:
 Corman wanted to use a strip club again and he had a haunted house set that he had left over from another film. So Andy Ruben and I came up with an idea to shoot in those two locations.  Of course we changed the haunted house into this really modern, amazing, great house.   accessed 21 April 2015  
She wrote the script with Andy Rubern. "It was always a process of including the elements Roger wanted into the script and story that Andy and I envisioned. We always had very high aspirations. Roger didnt discourage that, in fact I think he was proud of it, but he wanted to make sure his style of commercial elements were included." 

Shea said Corman insisted the female lead be a stripper. How Poison Ivy Got Its Sting: The studio wanted a teen-age Fatal Attraction. Katt Sheas movie may be more than that. Poison Ivy: Art or Exploitation?
By LAURIE HALPERN BENENSON. New York Times (1923-Current file)   03 May 1992: 70.  

She added about casting:
 It was really hard to find that vampire. In fact, Cyril came in so late I thought he was delivering a pizza to my house. It was really late at night and I thought ‘God, we’re never going to find a vampire’ and he walked in. He was the first one auditioned who didn’t sound like he was doing a stage play; and he didn’t sound like he was doing a period piece or something either. He just talked like a real person and I said "Okay this is the guy!"  

==Proposed Remake==
In 2011 it was announced Shea would remake the movie:
 I’ve made it now into much more of a love story. I love the new script. I think the new script is much better than the old one. It’s much more of a progression of two people over the course of a night who want to kill each other in the beginning. He’s taking her to kill her later on and she’s trying to kill him as well. Then, and over the course of the night they fall in love. They truly fall in love. He tries to save her at the end. He’s trying to prevent himself from killing her. I think it’s much more accessible.  

==References==
 

==External links==
*  at IMDB

 
 
 