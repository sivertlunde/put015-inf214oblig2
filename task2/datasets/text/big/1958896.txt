Empire (2002 film)
{{Multiple issues|section=|
 
 
 
}}
{{Infobox film
| name        = Empire
| image       = Empire ver2.jpg
| caption     = Theatrical release poster
| director    = Franc. Reyes
| writer      = Franc. Reyes
| starring    = John Leguizamo Peter Sarsgaard Denise Richards Sonia Braga Isabella Rossellini Fat Joe Treach
| producer    = Daniel Bigel Michael Mailer
| studio      = Arenas Entertainment   
| distributor = Universal Pictures
| released    =  
| runtime     = 90 minutes
| country     = United States
| language    = English
| budget      = $4 million   
| gross       = $18,591,272
}}
Empire is a 2002 gangster film starring John Leguizamo and Peter Sarsgaard.

==Plot==
 
Victor Rosa (Leguizamo) is a drug dealer in New York who sells a specific brand of heroin called "Empire". His territory is located in the South Bronx, where his other rivals all maintain an uneasy truce maintained because they all purchase their drugs from the same supplier, the drug lord La Colombiana (Isabella Rossellini).  Victor is invited to a chic white collar party by his girlfriend Carmen (Delilah Cotto).  The party is being thrown by her friend Trish (Denise Richards) and her boyfriend Jack (Sarsgaard), an investment banker. With a baby on the way, Vic decides to go straight, and begins to invest money with Jack, receiving significant returns. His friendship with Jack affords Vic a whole new lifestyle, and creates a rift between him and Carmen.

Jack offers Vic an investment opportunity for over 300% return, but theres a catch: the minimum buy-in is $4.5 million, $1.5 million more than Vic has. He approaches La Colombiana with an offer, in which she agrees to lend Vic the money he needs, if he gives her a 500% return and stops a feud between his best friend Jimmy (Vincent Laresca) and a rival dealer.  However, the feud escalates and Jimmy kills the dealer.  Vic receives his money and gives it to Jack, who disappears the next day.  Victor tracks him down and tries to get his money back, but kills Jack and Trish when they resist.  He escapes with Carmen and her family to Puerto Rico and opens a bar on the south side of the island with what little money he has left.

At the end of the movie, Vic is heading to the hospital to see the birth of his child when Rafael, La Colombianas younger brother,shoots him dead.

== Cast ==
 
*John Leguizamo as Victor Rosa a.k.a. "Vic"
*Peter Sarsgaard as Jack Wimmer
*Delilah Cotto as Carmen
*Denise Richards as Trish
*Vincent Laresca as Jimmy
*Isabella Rossellini as La Colombiana
*Sonia Braga as Iris
*Nestor Serrano as Rafael Menendez
*Treach as Chedda
*Fat Joe as Tito Severe
*Rafael Baez as Jay ("the idiot savant")

==Production==
 
Empire was the first release from Arenas Entertainment, a branch of Universal Studios specializing in productions for Hispanic audiences.    

==Release==
 
Universal picked up the film for US$650,000,  and released it on December 6, 2002 in 867 theaters. 

==Reception==
 
Empire received negative reviews, and carries a score of 21% on Rotten Tomatoes based on 99 reviews. 

==See also==
*List of American films of 2002

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 