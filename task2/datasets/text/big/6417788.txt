The Half-Way Girl

{{Infobox film
| name           = The Half-Way Girl
| image          = The Half Way Girl poster.jpg
| caption        = Lobby poster John Francis Dillon
| producer       = Earl Hudson
| writer         = 
| screenplay     = Joseph F. Poland Earl Snell
| story          = E. Lloyd Sheldon
| starring       = Doris Kenyon Lloyd Hughes
| music          =
| cinematography = George Folsey
| editing        = Marion Fairfax
| studio         = First National
| distributor    = First National
| released       = August 16, 1925
| runtime        = 80 minutes
| country        = United States Silent English intertitles
}} silent picture filmed around the Jersey Shore in 1925 in film|1925.

==Plot==
Doris Kenyon plays Poppy La Rue, an actress who winds up stranded in Singapore when her theatrical troupe goes bust. She winds up in the Red-light district where she works as a "hostess" (generally a silent film euphemism for prostitute), where she meets Philip Douglas, a down-at-the-heels Brit (Lloyd Hughes).
 Sam Hardy), a plantation owner, is determined to have Poppy, and when she wants to escape from the Oriental underworld, he offers to help, provided she accompanies him to Penang. They board a ship. Douglas is also on board and when a fire breaks out in the hold, he rescues Poppy from Jardines advances. They manage to get in a lifeboat just before the ship explodes, and are picked up by a passing vessel. Douglas father (Hobart Bosworth) wants the couple to separate, but finally he accepts Poppy as his daughter-in-law.

The spectacular fire aboard an ocean liner was shot in color, and to make it even more exciting, a leopard also breaks free on the ship.  The Corvallis, a 270-foot wooden-hulled freighter that was surplus from World War I, was purchased from the U.S. government by First National Pictures for a fraction of its original cost. First National Pictures bought it for the sole purpose of blowing it up in The Half-Way Girl.

In June 1925, under the supervision of the United States Coast Guard, the Corvallis was towed 45 miles offshore, loaded with dynamite, and blown up while the cameras rolled. After the explosion, the stern remained afloat and had to be sunk by the Coast Guard.

No copies of this film are known to exist today.

==Cast and crew== John Francis Dillon
*Cinematography by: George J. Folsey   (as George Folsey)
*Film Editing by: Marion Fairfax
*Art Direction by: Milton Menasco

===Writing credits===
*Joseph F. Poland   (as Joseph Poland)
*E. Lloyd Sheldon   story
*Earle Snell   (as Earl Snell)

===Cast (in credits order)===
*Doris Kenyon as Poppy La Rue
*Lloyd Hughes as Phil Douglas
*Hobart Bosworth as John Guthrie
*Tully Marshall as The Crab Sam Hardy as Jardine
*Charles Wellesley as Gibson
*Martha ODwyer as Miss Brown (as Martha Madison)
*Sally Crute as Effie

==External links==
*   at Star Pulse
* 

 

 
 
 
 
 
 
 
 
 
 
 


 