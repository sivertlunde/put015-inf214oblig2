The Faces of the Moon
{{Infobox film
| name           = The Faces of the Moon
| image          =
| caption        = 
| director       =Guita Schyfter
| producer       = Octavio Maya Guita Schyfter
| writer         = Hugo Hiriart
| starring       = Carola Reyna Geraldine Chaplin Ana Torrent Carmen Montejo Diana Bracho
| music          = Eduardo Gamboa	 
| cinematography = Jaime Reynoso
| editing        = Carlos Puente	 	 	
| distributor    = 
| released       =  
| runtime        =
| country        =Mexico
| language       = Spanish
| budget         =
| gross          =
}} Mexican drama film directed by Guita Schyfter. The film centres upon a group of female jurors at the 3rd Latin American Womens Film Festival in Mexico City.  The film reunites Geraldine Chaplin and Ana Torrent who previously starred as mother and daughter in the Carlos Saura films; Cría cuervos (1976) and Elisa, vida mía (1977).

==Plot==
The female jury, representing the United States, Spain, Uruguay, Costa Rica, Mexico and Argentina interract over their shared experiences. Shosh (Reyna) is an Argentine director that was a political exile in Mexico. Joan (Chaplin) is an American theorist and lebsian activist. Mariana (Montejo) is a pioneer of filmmaking, Julia (Lev) is a former terrorist from Uruguay who was imprisoned for thirteen years. The group complete their duties under the direction of the organizer, Magdalena (Bracho).   

==Cast==
*Carola Reyna as Shosh Balsher
*Geraldine Chaplin as Joan Turner
*Ana Torrent as Maruja Céspedes 
*Carmen Montejo as Mariana Toscano 
*Diana Bracho as Magdalena Hoyos 
*Haydee de Lev as Julia

==References==
 

==External links==
*  

 
 
 
 


 