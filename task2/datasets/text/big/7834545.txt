Original Sin (1992 film)
{{Infobox film
| name           = Original Sin
| image          =
| caption        =
| director       = Takashi Ishii
| producer       = Yasushi Enomoto
| writer         = Takashi Ishii Bo Nishimura (novel)
| starring       = Shinobu Otake Masatoshi Nagase Hideo Murota
| music          = Goro Yasukawa
| cinematography = Yasushi Sasakibara
| editing        = Yoshio Sugano
| distributor    = Argo Project
| released       =  
| runtime        = 117 min
| country        = Japan Japanese
| budget         =
}}
 1992 film Japanese film director Takashi Ishii. Based on a novel by Bo Nishimura, the film is about an extramarital affair between the wife of an estate agent and a 22 year old man.

The main cast members are Shinobu Otake (Nami Tsuchiya, the wife), Masatoshi Nagase (Makoto Hirano, the young man) and Hideo Murota (Hideki Tsuchiya, the husband).

==Plot==

Nagase is a young man on a train, apparently deciding at random where to go. As he walks out of a train station, he bumps into a woman, Nami. He is immediately fascinated by her, and follows her to the estate agents office where she works.

At the office her boss and husband, Hideki, invites him inside. He rents an apartment and asks for a job. Although Nami tells her husband that she has a certain feeling about Nagase, he is given a job.

Later, Nagase rapes Nami at a house that is on the estate agents books. However, they soon move upstairs to the bedroom and make consensual love. The affair continues, with Nagase telling her it was love at first sight.

At a hotel on a company trip, Hideki catches the couple together in the bath and finds out about their affair. He sacks Nagase. However, Nami later tracks him down and they spend a night together in a hotel where Nagase decides to murder Hideki.

Hideki catches Nami and Nagase together again, but finds himself unable to divorce his wife, instead wanting to make a fresh start to their marriage. To achieve this, he books them a break in an expensive Tokyo hotel.

However, Nagase comes to the hotel intent on carrying out his plan to kill Hideki. After hiding in the couples bedroom when Hideki arrives unexpectedly early and watching the married couple make love, he attacks Hideki in the bathroom. After a protracted fight, Hideki is finally killed, and Nagase hits Nami so that the police will believe that the incident was a robbery gone wrong.

The final scene shows Nagase asking Nami when he can see her again, Nami replying when everything has settled down. The final shot shows her pensively smoking a cigarette.

==External links==
*  
*  

 

 
 
 
 
 
 


 
 