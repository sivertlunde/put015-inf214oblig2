Achan (1952 film)
{{Infobox film
| name           = Achan
| image          =Achan.jpg
| image_size     =
| caption        =
| director       = MRS Mani
| producer       = M Kunchacko
| writer         = Thikkurissi Sukumaran Nair
| screenplay     =  Thikkurissi Sukumaran Nair
| music          = PS Divakar
| released       =  
| cinematography = P Balasubramaniam
| editing        = Mrs. Rajagopal
| studio         = Excel Productions
| lyrics         =
| distributor    =
| starring       = Prem Nazir, B. S. Saroja
| country        = India Malayalam
}}
 Indian Malayalam Malayalam film, directed by MRS Mani and produced by M Kunchacko.  The film stars Prem Nazir, B. S. Saroja in lead roles.  The film had musical score by PS Divakar. This was the only film in which veteran actor Sesbastian Kunjukunju Bhagavatahar did a comic role. It is one of the major box office hits of early Malayalam cinema. It will be remembered as the debut of Boban Kunchacko, the maiden venture of XL Productions, and first film of Thiruvananthapuram V. Lakshmi. 

==Cast==
* Prem Nazir
* Pankajavalli
* S. P. Pillai
* B. S. Saroja
* Adoor Pankajam
* Adoor Pankajam
* CR Rajakumari
* Mathappan
* Boban Kunchacko
* CR Lakshmi
* Sebastian Kunjukunju Bhagavathar
* T Jayasree
* Thikkurissi Sukumaran Nair
* Vanakkutty

==References==
 

==External links==
*  

 
 
 


 