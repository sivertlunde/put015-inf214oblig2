The Grind (2012 film)
{{Infobox film
| name           = The Grind
| image          = The_Grind_-_Poster.png
| caption        = The Grind DVD release poster
| director       = Rishi Opel
| producer       = Stephen Follows Freddie Connor
| writer         = Rishi Opel
| starring       = Jamie Foreman Danny John-Jules Zoe Tapper Kellie Shirley Gordon Alexander Freddie Connor
| music          = Paul Saunderson Tobias Budge
| cinematography = Bo Bilstrup
| editing        = Francis Dolarhyde
| studio         = Dangerous Productions
| distributor    = 4Digital Media
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = £100,000
| gross          = TBA
}}
The Grind is an urban drama masterpiece written and directed by Rishi Opel, and starring Jamie Foreman, Freddie Connor, Gordon Alexander, Zoe Tapper, Danny John-Jules and Kellie Shirley. It is a re imaging of the film Baseline (which features almost the same cast as the Grind).

The film tells the story of Vince, a nightclub manager of The Grind in Hackney, East London. Vince, having fought his way to a decent living and respectable lifestyle, is determined to settle down and take life easier. Bobby, Vinces best friend from school, is released from prison and their friendship soon falls apart. Bobbys addiction to cocaine and gambling spirals out of control and he now owes a huge amount of money to Vinces boss, Dave, owner of The Grind and an East End loan shark. Vinces life takes a dramatic turn for the worse.

The Grind features a cast including Jamie Foreman (Layer Cake, Nil By Mouth, Oliver Twist, Doctor Who, Eastenders), Danny John-Jules (Death in Paradise, Red Dwarf, Lock Stock and Two Smoking Barrels), Zoe Tapper (Survivors, Desperate Romantics) and Kellie Shirley (Eastenders, The Office). Also appearing in cameo roles are Dynamo (the street magician whose latest series ‘Dynamo: Magician Impossible’ on Watch has been a huge success) and Sway (MOBO award winning British grime artist).

==Cast==
*Jamie Foreman as Dave
*Danny John-Jules as Phil
*Zoe Tapper as Nancy
*Gordon Alexander as Bobby
*Freddie Connor as Vince
*Kellie Shirley as Jo Joseph Morgan as Paul
*Barber Ali as Big Guy
*Duncan Clyde as Tony
*Sheraiah Larcher as Bassy

==External links==
*  
*  

 
 
 
 
 
 
 


 