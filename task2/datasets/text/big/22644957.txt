The Shadow Walkers
 
{{Infobox film
| name           = De Schaduwlopers
| image          =
| image_size     =
| caption        =
| director       = Peter Dop
| producer       =
| writer         = {{Flatlist|
* Gianni Celati
* Peter Dop
}}
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Netherlands Dutch
| budget         =
}}
The Shadow Walkers or  De Schaduwlopers  is a 1995 Dutch film directed by Peter Dop. It is Dops debut as a feature film director.   

Leen and Albert are two brothers who follow people around randomly at night in a small town. Both are married to a sister that is related to each other and while they go on with their normal daily lives, the brothers follow people and note slight changes in peoples behavior. Their name for the activity is walking shadow. The brothers end up following a Japanese tour group heading to Paris.

==Cast==
* Pierre Bokma ...  Leen
* Aat Ceelen ...  Albert
* Jeroen Willems ...  Stijn
* Marlies Heuer ...  Judith
* Lineke Rijxman ...  Paula
* Ariane Schluter ...  Serveerster
* Betty Schuurman ...  Margreet
* Jack Wouterse ...  Man met Koffer
* Han Kerkhoffs ...  Marco
* Dirk Zeelenberg ...  Frans
* Dennis Rudge ...  Amerikaanse Militair
* Jan Kruse ...  Schoonhoven
* Marie Kooyman ...  Vrouw in Bontjas
* Ferry Kaljee ...  Echtpaar in Snackbar
* Anke Vant Hof ...  Echtpaar in Snackba

==Production==
Filmed in the directors hometown of Deventer,  the film is based on stories written by Dylan Thomas and Gianni Celati.  Despite being filmed in Dops hometown, not much of the town is shown.  Musician Fons Merkies plays the films jazz score while Fred van Straaten plays the trumpet.    The cameramen were Jan Wich, Lex Wertwijn and Peter Brugman Rotterdam, whose work on the film was praised by Variety.    Van der Hoop Film Productions in collaboration with Studio Nieuwe Gronden and ARTE produced the film. The Dutch Film Fund and the Deventer City Marketing Foundation helped finance the production.    The Shadow Walkers premiered at the International Film Festival Rotterdam. 

==Reception==
The film was a moderate success in the Netherlands.    David Rooney of Variety (magazine)|Variety called the film "lazily compelling" and said that the comedy is drawn out.  The Dutch film magazine Film Krant reviewed the film.  Mark Duursma of Trouw said that nothing in the film is important and that the director has a lack of expression.  According to Film 1, there is no "driving storyline". 

==References==
 

==External links==
*  

 
 
 
 