Sakuran
{{Infobox animanga/Header
| name            = Sakuran
| image           =  
| caption         = Cover of the first Japanese manga volume
| ja_kanji        = さくらん
| ja_romaji       = 
| genre           = Period drama
}}
{{Infobox animanga/Print
| type            = manga
| author          = Moyoco Anno
| publisher       = Kodansha
| publisher_en    =  
| demographic     = Seinen manga|Seinen Evening
| first           = 2001
| last            = 2003
| volumes         = 1
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Mika Ninagawa
| producer        = Fellah Pictures
| writer          = Yuki Tanada
| music           = Shiina Ringo
| studio          = 
| released        = February 24, 2007
| runtime         = 111 minutes
}}
 

 , is a manga series created by Moyoco Anno. The manga is about a girl, Kiyoha (though she goes through different names while growing up the hierarchy) who becomes a tayu or oiran courtesan.
North American publisher Vertical Inc released it in English in July 2012. 

== Plot ==

A young kamuro (maid in a brothel) is sold into the red-light district Yoshiwara and is put under the care of the oiran of the Tamagiku house, Shouhi, who names her Tomeki. The girl is very rebellious. It is because of this that the leaders of the household begin to think that she will be one day a great oiran, since they need not only beauty and talent, but also the tenacity to maintain their position.

Tomeki becomes O-Rin, a hikkomi (or courtesan-in-training), and later Kiyoha, the most beautiful girl in the Tamagiku household. Her popularity threatens the position of Tamagikus oiran Mikumo, which creates great tension between the two.

But rivalry is not the main problem for young Kiyoha, but rather the appearance of young Soujiro and the impossibility of love.
 

==Film==
A live-action film adaptation was released in Japan on February 24, 2007. The film stars Anna Tsuchiya and marks the directorial debut of photographer Mika Ninagawa. Ringo Sheena is the music director, her album Heisei Fūzoku acting as the soundtrack.

===Cast===
*Anna Tsuchiya ... Kiyoha
*Kippei Shiina ... Kuranosuke
*Yoshino Kimura ... Takao
*Hiroki Narimiya ... Sojiro
*Miho Kanno ... Shohi
*Masatoshi Nagase ... Mitsunobu
*Ando Masanobu ... Seiji

==External links==
*    
*  

== References ==
 

 

 
 
 
 
 
 
 
 
 
 
 

 