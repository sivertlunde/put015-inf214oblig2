Andha Oru Nimidam
{{Infobox Film 
| name =  Andha Oru Nimidam
| image = 
| image_size = 
| caption = 
| director = Major Sundarrajan
| producer = Pazha Karuppiah
| writer = Dinakaran
| narrator =  Urvashi Jayamalini 
| music = Ilaiyaraaja
| cinematography = T. S. Vinayagam
| editing = Devan
| studio = Major Films
| released = 31 May 1985
| runtime = 
| country = India
| language = Tamil
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1985 Tamil Urvashi in the lead roles while Jayamalini, Thengai Srinivasan and Pandari Bai play supporting roles. The film opened in May 1985 to a below average response.  The film was later dubbed in Hindi language as Mahaan.

==Summary==

Kamal (Kamal Haasan), a lawyer by profession also actively involves in social works falls in love with Vasanthi (Urvashi), and  her grandfather (Srinivaasan) works as house keeper in Raj Shekhars (Major) house. One day Rajshekhar murder his friend and partner Madan and cunningly blame on Grandfather and he is arrested and put behind bars. One day Grandfather escapes from prison to kill Raj Shekhar, he is murdered by Raj Shekhar. Kamal, with the help of Vasanthi starts investigating the murder of Madan and Grandfather. Why Rajshekhar murder Madan? Who is Anuradha? Whether Kamal catch the culprit Raj Shekhar? What happened to love of Kamal and Vasanthi? The climax reveals all these answers.

==Cast==
* Kamal Haasan  Urvashi
* Jayamalini
* Pandari Bai 
* Y. G. Mahendra
* Thengai Srinivasan
* Venu Arvind 
* Veera Raghavan
* Mahendran
* Raja
* Anuradha

==Release==
The film opened in May 1985 to poor reviews and performed below average at the box office.  Kamal Haasan went on to work with Urvashi again in the 1990 Crazy Mohan-written comedy film Michael Madana Kama Rajan.

==Soundtrack==
{{Infobox album|  
  Name        = Andha Oru Nimidam
|  Type        = Soundtrack
|  Artist      = Ilaiyaraaja
|  Cover       =
|  Released    = 1985
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Andha Oru Nimidam (1985)
|  Next album  = 
}}

The music was composed by Ilaiyaraaja and lyrics were written by Vairamuthu, Gangai Amaran and N. Kamarajan. 
{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Alaigalil Midhakkudhu  
| extra1          = S. P. Balasubrahmanyam, S. Janaki
| length1         =
| title2          = Pachondiyyeh Kelada  
| extra2          = S. P. Balasubrahmanyam
| length2         = 
| title3          = Kaathiruppadhu Pathiruppadhu 
| extra3          = S. P. Balasubrahmanyam, Malaysia Vasudevan
| length3         =
| title4          = Siriya Paravai 
| extra4          = S. P. Balasubrahmanyam, S. Janaki  
| length4         = 
| title5          = Thevai Indha Paavai  
| extra5          = S. P. Balasubrahmanyam, S. P. Sailaja
| length5         = 
| title6          = Nalla Neram Neram  
| extra6          = S. Janaki
| length6         =
}}

==References==
 

== External links ==
*  

 
 
 
 
 


 