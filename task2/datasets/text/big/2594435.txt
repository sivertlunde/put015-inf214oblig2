8 Women
 
{{Infobox film
| name           = 8 Women
| image          = 8-femmes-poster.jpg
| border         = yes
| caption        = French theatrical release poster
| director       = François Ozon
| producer       = Stéphane Célérier Olivier Delbosc Marc Missonnier
| writer         = François Ozon Marina de Van
| based on       =  
| starring       = Danielle Darrieux Catherine Deneuve Fanny Ardant Isabelle Huppert Emmanuelle Béart Virginie Ledoyen Ludivine Sagnier Firmine Richard
| music          = Krishna Levy
| cinematography = Jeanne Lapoirie
| editing        = Laurence Bawedin
| studio         = Canal+ Celluloid Dreams
| distributor    = Mars Distribution   BIM Distribuzione   Focus Features
| released       =  
| runtime        = 111 minutes
| country        = France Italy
| language       = French
| budget         = €8,460,000
| gross          = $67,770,154   	 
}} French dark Robert Thomas, it features an ensemble cast of  high-profile French actresses that includes Danielle Darrieux, Catherine Deneuve, Isabelle Huppert, Emmanuelle Béart, Fanny Ardant, Virginie Ledoyen, Ludivine Sagnier, and Firmine Richard. Revolving around an eccentric family of women and their employees in the 1950s, the film follows eight women as they gather to celebrate Christmas in an isolated, snowbound cottage only to find Marcel, the family patriarch, dead with a knife in his back. Trapped in the house, every woman becomes a suspect, each having her own motive and secret.
 The Women, theatrical feel. It also serves as a pastiche of and homage to the history of film and the actresses filmographies.
 Silver Bear. Best Film. Best Film Best Director; Best Actress for the eight principal actresses.

==Plot==
The film is set in the 1950s in a large country residence as a family and its servants are preparing for Christmas. When the master of the house is discovered dead in his bed with a dagger in his back, it is presumed that the murderer must be one of the eight women in the house. Over the course of the investigation, each woman has a tale to tell and secrets to hide.

The scene opens with Suzon returning from school for Christmas break, finding her mother Gaby, her younger sister Catherine, and her wheelchair-bound grandmother Mamy in the living room, where most of the action of the film takes place. Their conversation drifts to the subject of the patriarch of the family, Marcel, and Catherine leads the first song of the film, "Papa tes plus dans le coup" (roughly, "Dad, youre out of touch"). The singing wakes up Suzon and Catherines aunt Augustine, who initiates arguments with the rest of the family and the two servants (Madame Chanel and Louise), eventually returning upstairs and threatening to commit suicide. Mamy jumps out of her wheelchair trying to stop her, haphazardly explaining her ability to walk as a "Christmas miracle." Augustine is eventually calmed down, and she sings her song of longing, "Message personnel" (Personal Message).

The maid takes a tray upstairs, finds Marcels stabbed body, and screams. Catherine goes up to see what has happened and locks the door. The others finally go up to Marcels room to see him stabbed in the back. Catherine tells the others that they should not disturb the room until the police arrive, so they re-lock the door. Realizing that the dogs did not bark the night before the incident, it becomes clear that the murderer was known to the dogs and therefore must be one of the women in the house. Attempting to call the authorities, they find that the telephone line has been cut, so they will have to go in person to the police station. Before that can do so, the women are distracted by the announcement that someone is roaming the garden, someone whom the guard dogs are not chasing. The person turns out to be Marcels sister Pierrette, a nightclub singer who is also rumored to be a prostitute, and who has not been allowed into the house before due to Gabys dislike for her. When questioned, she claims that she received a mysterious telephone call in which she was informed her that her brother was dead. She  sings "A quoi sert de vivre libre" (Whats the point of living free?), commenting on her sexual freedom.

It is realized that she has been to the house before, as the dogs did not bark and she knew immediately which room belonged to her brother, making her the eighth potential killer. The women try to start the car, and find that it has been sabotaged, cutting them off from help until the storm subsides and they can hitchhike to town. The women spend their time trying to identify the murderer amongst them. It is learned that Suzon returned the night before to tell her father in secret that she was pregnancy|pregnant. She sings a song to Catherine, "Mon Amour, Mon Ami" (My Lover, My Friend), about her lover; however, she was  sexually abused by her father. We later learn that Suzon is not Marcels child but is  the child of Gabys first great love who was killed not long after the child was conceived; every time Gaby looks at Suzon she is reminded of her love for him.

Suspicion then swings to Madame Chanel, the housekeeper, whose actions the night before seem suspicious. It is revealed that she had been having an affair with Pierrette, who went to see her brother that night to ask for money to pay off her debts. When some members of the family react in outrage to the fact that she is a lesbian, Madame Chanel retreats to the kitchen, and sings "Pour ne pas vivre seul" (So as to not live alone).

In the meantime we find out that Mamy, Suzon and Catherines "old and sick" grandmother, not only can walk but also possesses some valuable stock shares that could have saved Marcel from his bankruptcy. Out of greed, she lied that her shares had been stolen by someone who knew where she was hiding them. The spotlight moves to Louise, the new maid, who is found out to be Marcels mistress. She declares affection for Gaby, but also expresses disappointment in her for her weakness and indecision. She sings "Pile ou Face" (literally Heads or Tails, but referring to the Ups and Downs of life), and removes the symbols of her servitude, her maids cap and apron, asserting herself as an equal to the other women. Gaby sings "Toi Jamais" (Never You), about Marcel, saying that he never paid enough attention to her, while other men did. It is revealed that she had an affair with Marcels business partner, Jacques Farneaux, the same man who has been having an affair with Pierrette. The two women get into a fight that turns into a passionate make-out session on the living room floor, a scene which the others walk in on and are stunned by.

Eventually, Madame Chanel discovers the solution to the mystery, but she is silenced by a gunshot. While not struck by the bullet, she becomes mute out of shock. Catherine takes the lead, revealing that she hid in her fathers closet from where she saw all the other women talk to Marcel the night before. She explains the mystery: Marcel  faked his own death, with her help, to see what was really going on in his house. She says that he is now free of the other womens clutches and rushes to his bedroom only to witness Marcel shoot himself in the head. Mamy closes the film with the song "Il ny a pas damour heureux" (There is no happy love) as the women clasp hands and face the audience.

==Cast==
* Danielle Darrieux as Mamy, the matriarch
* Isabelle Huppert as Augustine, her tachycardiac daughter
* Catherine Deneuve as Gaby, her other daughter, the victims wife
* Virginie Ledoyen as Suzon, the victims eldest daughter
* Ludivine Sagnier as Catherine, the victims youngest daughter
* Fanny Ardant as Pierrette, the victims sister
* Emmanuelle Béart as Louise, the new chambermaid
* Firmine Richard as Madame Chanel, the cook
* Dominique Lamure as Marcel, the victim, Gabys husband

==Musical numbers==
# "Papa tes plus dans le coup" (Dad, Youre Out of Touch) - Catherine, Company
# "Message personnel" (Personal Message) - Augustine
# "A quoi sert de vivre libre" (Whats the Point of Living Free?) - Pierrette
# "Mon Amour, Mon Ami" (My Lover, My Friend) - Suzon
# "Pour ne pas vivre seul" (So as Not to Live Alone) - Madame Chanel
# "Pile ou Face" (Heads or Tails/Ups and Downs) - Louise
# "Toi Jamais" (Never You) - Gaby
# "Il ny a pas damour heureux" (There is No Happy Love) - Mamy

==Production==
Ozon was inspired by the 1950s Ross Hunter productions of Douglas Sirk and Alfred Hitchcock. To achieve the look of the latter two directors films, Ozon had costume designer Pascaline Chavanne fashion an original costume for each character. Chavanne was inspired by Christian Dior SA#The "New Look" and Dior Parfums|Diors New Look  and the earlier film costumes of Edith Head.  Composer Krishna Levy also provided an instrumental score evocative of Bernard Herrmann, with touches of Miklos Rosza and Elmer Bernstein,  as well as a soundtrack featuring eight songs performed unexpectedly by the films title characters.

==Reception==

===Critical response===
  won several awards for her role in the film.]]
8 Women received generally positive reviews from critics. The review aggregator website Rotten Tomatoes reported that 78% of critics gave the film a positive rating, based on 115 reviews, with an average score of 6.9/10. Its consensus states "Featuring some of the best French actresses working today, 8 Women is frothy, delirious, over-the-top fun."  On Metacritic, the film holds a 64/100 rating, based on 30 reviews, indicating "generally favorable reviews". 

  from Entertainment Weekly felt that "what does matter is that a phalanx of Frances most famous actresses play a gay peekaboo with their own images in the guise of replicating a 50s technicolored production. The result is weightless entertainment thats both camp and true, a warped adoration of star-quality actresses as amazing creatures who can project the lives of fictional characters as well as the essence of their own fabulous selves. Hollywood, start your remake." 

  from The New York Times said, "The high-minded critical term of art for such a decadent delight is guilty pleasure, but a movie like this reveals that stuffy phrase to be both a redundancy and an oxymoron. 8 Women is indefensible, cynical, even grotesque; it is also pure—that is to say innocent and uncorrupted—fun."  Less enthusiastic with the film, Salon.com wrote that "despite the all-star cast of beautiful, talented actresses, this French whodunit never lives up to the grand musicals it rips off." 

Specific performances were cited for acclaim. Isabelle Hupperts portrayal of Augustine was met with praise.  Curiel wrote that "Hupperts song and dance—and her whole performance in the movie—is a study in determination. Huppert has a reputation for her intense portrayals, and in 8 Women, she steals every scene shes in as the uptight, melodramatic, bespectacled aunt."  Fanny Ardant also received praise for her performance with Hornaday saying that "the showstopper is Ardant, who in a sensational turn combining the earthiness of Ava Gardner and the fire of Rita Hayworth, explodes with an elemental force all her own. Ardant alone is worth the price of admission to 8 Women." 

===Box office===
The film was released on 6 February 2002 in France and opened at #3 in 493 theaters, grossing $5,246,358 in the opening weekend. It grossed $18,991,866 in the domestic market. The films international gross was in total $42,426,583. http://www.boxofficemojo.com/movies/?id=8women.htm 

==Accolades==
{| class="wikitable" 
|+List of awards and nominations
! Award
! Category
! Recipients and nominees
! Result
|- Berlin International Film Festival
| Silver Bear|Berliner Morgenpost Reader Jury 
| François Ozon
|  
|-
| Golden Bear
| François Ozon
|  
|- Silver Bear for Outstanding Artistic Achievement
| Cast
|  
|- 28th César Awards|César Awards Best Actress
| Fanny Ardant
|  
|-
| Isabelle Huppert
|  
|- Best Cinematography
| Jeanne Lapoirie
|  
|- Best Costume Design
| Pascaline Chavanne
|  
|- Best Director
| François Ozon
|  
|- Best Film
|  
|  
|- Best Music Written for a Film
| Krishna Levy
|  
|- Best Production Design
| Arnaud de Moleron
|  
|- Best Sound
| Pierre Gamet  Benoît Hillebrant  Jean-Pierre Laforce
|  
|- Best Supporting Actress
| Danielle Darrieux
|  
|- Best Writing
| François Ozon  Marina de Van
|  
|- Most Promising Actress
| Ludivine Sagnier
|  
|- Chicago Film Critics Association Awards
| Foreign Language Film 
| France
|  
|-
| rowspan="2"|Étoiles dOr
| Best Actress 
| Isabelle Huppert
|  
|-
| Best Composer 
| Krishna Levy
|  
|- European Film Awards Best Actress
| Cast
|  
|- European Film Best Actress (Jameson Peoples Choice Award)
| Fanny Ardant
|  
|-
| Isabelle Huppert
|  
|- Best Director (Jameson Peoples Choice Award)
| François Ozon
|  
|- Best Film
| Olivier Delbosc Marc Missonnier
|  
|- Best Screenwriter
| François Ozon
|  
|- GLAAD Media Awards Outstanding Film – Limited Release
|  
|  
|-
| Golden Reel Awards
| Best Sound Editing in a Feature - Music - Musical
| Benoît Hillebrant 
|  
|-
| Prix Lumières
| Best Director 
| François Ozon
|  
|- National Board of Review Awards Best Foreign Language Film
|  
|  
|- Online Film Online Film Critics Society Awards Best Costume Design
| Pascaline Chavanne
|  
|- Best Ensemble
| Cast
|  
|- Russian Guild of Film Critics Awards
| Best Foreign Actress
| Isabelle Huppert
|  
|- San Diego Film Critics Society Awards
| Special Award
| Isabelle Huppert
|  
|-
|}

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 