Witness for the Prosecution (1982 film)
 
  1982 TV Witness for the Prosecution.
 Alan Gibson, John Gay John Cameron.

The cast includes many veteran and well-known actors such as Ralph Richardson, Deborah Kerr, Diana Rigg, Donald Pleasence, Peter Sallis and Beau Bridges. Unlike the original Billy Wilder film, the TV version stays more faithful to the original Agatha Christie short story, including the scene where Sir Wilfred meets the scarred woman in an apartment at bad-fame streets of London, instead of meeting a cockney woman at the railway station as in the Wilder version.

This version, also, instead of opening with Sir Wilfrid (renamed "Sir Wilfred") returning home, features an opening prologue where Janet Mackenzie returns to her employers house, where she sees her laughing and drinking with someone, goes upstairs and takes a pattern from her room, and hears noise from downstairs, and discovers in shock her murdered employer, and the murderer escaped.

==Plot summary==
Sir Wilfred Robarts, a famed barrister, has just been released from the hospital in which he stayed for two months following a heart attack. Returning to his practise of law, he takes the case of Leonard Vole, an unemployed man who is accused of murdering his elderly friend, Mrs. Emily French. Vole claims hes innocent, although all evidence points to him as the killer, but his alibi witness, his cold German wife Christine, instead of entering the court as a witness for the defense, becomes the witness for the prosecution and defiantly testifies that her husband is guilty of the murder. Sir Wilfred believes theres something suspicious going on with the case, particularly with Mrs. Vole.

==Cast==
* Ralph Richardson as Sir Wilfred Robarts
* Deborah Kerr as Miss Plimsoll, the nurse
* Diana Rigg as Christine Helm
* Beau Bridges as Leonard Vole
* Donald Pleasence as Mr. Myers, the prosecutor
* Wendy Hiller as Janet Mackenzie, the housekeeper
* David Langdon as Mayhew, Voles solicitor
* Richard Vernon as Brogan-Moore, Sir Wilfreds colleague
* Peter Sallis as the butler, Carter
* Michael Gough as the Judge Frank Mills as Chief Inspector Hearne
* Primi Townsend as Diana
* Patricia Leslie as Mrs. French

==Notes==
Alan Gibson, the director of this film, also directed The Satanic Rites of Dracula, where Richard Vernon, who plays the part of Brogan-Moore in Witness for the Prosecution, had a small role.

John Gay, the writer of the teleplay, also wrote teleplays for the Lux Video Theatre, a television anthology series. Lux Video Theatre also produced an adaptation of Witness for the Prosecution, in 1953 (four years before the Wilder version).

==External links==
*  
*  
*  

 

 
 
 
 