Eternamente Pagú
{{Infobox film
| name           = Eternamente Pagú
| image          = Eternamente Pagu.jpg
| caption        = Theatrical release poster
| director       = Norma Bengell
| producer       = Jayme Del Cueto Agostinho Janequine
| writer         = Márcia de Almeida Geraldo Carneiro
| starring       = Carla Camurati
| music          = Turibio Santos Roberto Gnattali
| cinematography = Antônio Luiz Mendes Soares
| editing        = Dominique Paris
| studio         = Flai Cinematográfica Sky Light Cinema Maksoud Plaza Embrafilme
| distributor    = Embrafilme
| released       =  
| runtime        = 100 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}
Eternamente Pagú is a 1988 biopic about Patrícia Galvão, directed by Norma Bengell and starring Carla Camurati.    

==Plot== Dictatorship of Getúlio Vargas. After leaving prison, she abandons Communism in favor of Trotskyist Socialism, marries to Geraldo Ferraz, and starts a career as theatre director.

==Cast==
*Carla Camurati as Pagu|Patrícia "Pagu" Galvão
*Antônio Fagundes as Oswald de Andrade
*Esther Góes as Tarsila do Amaral
*Nina de Pádua as Sideria
*Otávio Augusto as Geraldo Ferraz
*Paulo Villaça as Pagus father
*Norma Bengell as Elsie Houston
*Antonio Pitanga
*Breno Moroni
*Kito Junqueira
*Maria Sílvia
*Suzana Faini
*Beth Goulart
*Marcelo Picchi
*Carlos Gregório
*Eduardo Lago
*Ariel Coelho

==Reception==
At the 16th Festival de Gramado, it received the Best Actress Award (Camurati) and the Best Adapted Score Award. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 