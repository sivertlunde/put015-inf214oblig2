Hell House (film)
{{Infobox film
| name           = Hell House
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = George Ratliff
| producer       = Devorah DeVries
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Trinity Church Youth Group
| music          = Bubba and Matt Kadane
| cinematography = Jawad Metni
| editing        = Michael LaHaie
| studio         = 
| distributor    = 
| released       =  
| runtime        = 85 min
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 haunted attraction, youth group of the Cedar Hill Trinity Church, documenting the work involved in creating the hell house, the performances themselves, and the personal lives of some of the participants. 

==Reception==
The film currently holds a rating of 94% "certified fresh" on review aggregator Rotten Tomatoes based on 34 reviews.  A reviewer for the New York Times praised Ratliff for "wisely   the temptation to make fun of his subjects, most of whom seem to believe sincerely that they are doing the work of God". 

==References==
 

==External links==
* 
* 

 
 
 
 

 