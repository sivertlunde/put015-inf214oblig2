Call of the Toad
{{Infobox film
| name           = Call of the Toad
| director       = Robert Gliński
| producer       = Regina Ziegler, Elke Ried, Ursual Vossen, Mike Downey, Sam Taylor, Henryk Romanowski
| writer         = Klaus Richter, Pawel Huelle and Cezary Harasimowicz (based on the book by Günter Grass)
| starring       = Matthias Habich, Krystyna Janda
| music          = Richard G. Mitchell 
| cinematography = Jacek Petrycki
| editing        = Krzysztof Szpetmanski	 	 
| released       = 2005
| runtime        = 98 minutes
| country        = German, Polish  English
}}
 German man and a Polish woman who become caught up in the advent of modern capitalism in Poland. It is based on the novel The Call of the Toad written by Günter Grass.

==Synopsis==
In Gdańsk, Poland, in 1989, Alexander Reschke and Alexandra Piatkowska first meet on their way to a cemetery. Both are survivors from the end of World War II when many people from Poland and Germany were displaced as borders were re-drawn according to new treaties.

But they discover that displacement and war is not all they have in common.  They have both been widowed and due of their upheaval  - they share the belief that survivors such as themselves should have the right to be returned home for burial.

As their love affair grows, their ambitions become entwined.  The pair establishes a Cemetery of Reconciliation to show that old hatreds are now dead.  They form a company that arranges for survivors to be buried in their original country but as the money rolls in, their good intentions are corrupted.

==Cast==
* Krystyna Janda - Aleksandra Piatkowska
* Matthias Habich - Alexander Reschke
* Dorothea Walda - Erna Brakup
* Bhasker Patel - Chatterjee
* Udo Samel - Vielbrand
* Marek Kondrat - Marczak
* Mareike Carrière - Johanna Detlaff

==Awards==
*Nominated Best Actress, Polish Film Awards.
*Nominated Best Film Score, Polish Film Awards.
*Nominated Best Production Design, Polish Film Awards.
*Nominated Best Supporting Actor, Polish Film Awards.

==External links==
*  
*http://www.cinefacts.de/kino/566/unkenrufe/filmreview.html
*http://www.moviemaster.de/archiv/film/film_3864.html
*http://www.moviereporter.net/filme/309
*http://www.zelluloid.de/filme/index.php3?id=5140

 
 
 


 
 