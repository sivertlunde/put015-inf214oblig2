Zapatlela
 

{{Infobox film name           = Zapatlela image          =  caption        = Tatya Vinchu director       = Mahesh Kothare producer       = screenplay     = story          = Mahesh Kothare starring       = Mahesh Kothare Laxmikant Berde Jayram Kulkarni Pooja Pawar Madhu Kambikar Kishori Ambiye Dilip Prabhavalkar Vijay Chavan music          = cinematography = editing        = distributor    = released       =   runtime        = 168 minutes country        = India language       = Marathi budget         = gross          =
}}
 Marathi horror comedy movie, which was released in 1993. It is directed by Mahesh Kothare. It is followed by sequel Zapatlela 2, released 20 years later (in 2013).

The film has the renowned artist ventriloquist Ramdas Padhye along with his famous evil puppet Tatya Vinchu. The film stars Laxmikant Berde as Laxmikant Bolke, with Mahesh Kothare as CID Inspector Mahesh Jadhav and Dilip Prabhavalkar as Tatya Vinchu.

==Plot==

The Film begins with a Car approaching in Night. Tatya Vinchu (Dilip Prabhavalkar) a famous gangster and his henchman kubdya Khavis enters the cave of Baba chamtkar (Raghvendra Kadkol) in search for mrutunjay mantra, a voodoo spell which can transfer the soul of the chanter to any living or nonliving object. Tatya threatens Baba Chamatkar for his mantra. Baba, out of fear, gives it to him. CID Inspector Mahesh Jadhav (Mahesh Kothare), who is trying to arrest Tatya raids his warehouse. Mahesh finds Tatya, and gives chase until Tatya hides in a post office. There, they fight and Tatya gets hit by a fatal gunshot. He uses the mantra to transfer his soul into the body of a ventriloquists doll lying nearby. Meanwhile Kubdya Khavis is captured and taken into custody. The police believes that Tatya is dead.

Gauri (Kishori Ambiye) returns from USA to India for pursuing PHD in Criminal Psychology. In the meantime Gauris Father gets transferred to Shrirangpur. She meets her brother Lakshya. Lakshya has a talent of ventriloquism, and Gauri gives him a doll as a gift she got from USA, which happens to be the doll in which Tatya Vinchus soul is trapped.

Tatya tells lakshya that he is not a doll, he is a criminal. Later, the Sarpanch of the village is insulted when Lakshya publicly makes fun of him. He takes all of Lakshyas belongings on the pretext of unpaid rent. He also takes Tatya Vinchu. Tatya Vinchu shows his true identity and kills him. Lakshya gets home, only to find his house completely ransacked by Sarpanchs men. An angry Lakshya goes to the Sarpanchs godown where he sees that the Sarpanch is dead. Just then, Mahesh comes in the scene and thinks that out of rage, Lakshya has killed the Sarpanch.

In jail Lakshya desperately tries to explain, but fails to prove his point. The doll is also kept as evidence. Tatya rises from the evidence box and asks Lakshya about transport which would get him to Mumbai. Lakshya in fear of Tatya tells him. Post-mortem reports prove the innocence of Lakshya and he is freed. While Mahesh & Gauri confess to each other their love. Tatya reaches Mumbai in the Cave of Baba Chamatkar,where he asks him about how to get transferred from a puppet to mortal human body. Baba disagrees to give him further knowledge,but tatya threatens him and makes him to tell. He understands to become mortal again he has gain the body of the 1st person whom he has told his name and that person is Lakshya.

Tatya goes back to Shrirangpur to gain the body of Lakshya. One night he climbs through the window and attempts to possess Lakshyas body, but Lakshya manages to lock him in the cupboard. In the morning lakshyas lover Gangi takes Tatya out from cupboard and buries him underground, to cure Lakshya from his anxiety on the nights events. Tatya still manages to dig his way out and scares  Lakshya. All the villagers think that Lakshya is going crazy and forcefully hospitalize him into a mental hospital. Taty attempts to possess Lakhya again, but Lakshya escapes just in time. Lakshya, scared, run away home.

Meanwhile Mahesh gets whereabouts of Baba Chamatkars cave. He approaches Baba and gets the truth about the possessed doll. Baba says that the only way to defeat the doll is to shoot him in between his eyebrows. Mahesh comes back to shrirangpur, in order to save Lakshya. Tatya meets Kubdya who has escaped from prison. He tells him about lakshya,and Kubdya takes him to Lakshyas house. Tatya once again attempts to possess him. Mahesh reaches lakshyas home just in time shrirangpur, where he and kubdya fight. Meanwhile Tatya chases Lakshya all the way to the terrace of the house. Mahesh injures Kubdya and reaches there just in time. But Tatya pushes him and he almost falls, hanging to the edge of the roof. Tatya then takes advantage of this situation and starts saying the mantra to possess Lakshya. When Tatya is just about to say the last verse, Mahesh gets up just in time and fires a shot from his gun, and it hits Tatya vinchu in between the eyebrows. The doll falls down, now lifeless. Kubdya is taken in custody again.

The Film ends by renowned artist ventriloquist Ramdas Padhye giving a present to Lakshya in the presence of Mahesh. It is a doll. Already fearing dolls since this incident, Lakshya yells to Mahesh in fear and the movie ends.

==Cast==

* Mahesh Kothare as CID Inspector Mahesh Jadhav
* Laxmikant Berde as Laxmikant (Lakshya) Bolke
* Dilip Prabhavalkar as Tatya Vinchu
* Kishori Ambiye as Gauri 
* Madhu Kambikar as Lakshyas Mother
* Raghvendra Kadkol as Baba Chamatkar
* Vijay Chavan as Havaldar Sakharam
* Jayram Kulkarni as Superintendent of Police

==Production==
===Set design===
 
The puppet was made by Ramdas Padhye in order to have a horror effect in the film. As the puppets eyes shows its immense effect of understanding facial movements.

===Sound design===
Due to the events that take place in the film, sound effects were much more advance than in the previous Mahesh Kothare Films. Sound designer and co-supervising sound editor had perfectly mixed the sound which gave exact horror effects,a film needs.

===Inspired by===
This film is loosely based on Childs Play (1988 film)|Childs Play, a film by Don Macini.

==Sequel==
A sequel to the film, titled Zapatlela 2 was released in 3D on 7 June 2013 with Adinath Kothare and Sonalee Kulkarni in lead roles. It also features Makarand Anaspure, Sai Tamhankar, Mahesh Kothare, Madhu Kambikar, Vishakha Subhedar in supporting roles.

==References==
 

== External links ==
*  
*  

 