Adventures of the Barber of Seville
 
{{Infobox film
| name           = Adventures of the Barber of Seville
| image          =
| caption        =
| director       = Ladislao Vajda
| producer       = Pierre Gérin Benito Perojo Miguel Tudela
| writer         = Jesús María de Arozamena Pierre Augustin Caron de Beaumarchais Alex Joffé Jean Marsan Cesare Sterbini
| starring       = Luis Mariano
| music          =
| cinematography = Antonio L. Ballesteros
| editing        = Antonio Ramírez de Loaysa Henri Taverna
| distributor    =
| released       =  
| runtime        = 92 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}

Adventures of the Barber of Seville ( ) is a 1954 Spanish comedy film directed by Ladislao Vajda. It was entered into the 1954 Cannes Film Festival.   

==Cast==
* Luis Mariano - Fígaro
* Lolita Sevilla - Pepilla
* Danielle Godet - Rosina
* José Isbert - Don Faustino
* Emma Penella - Duquesa de San Tirso
* Miguel Gila - Sargento
* Jean Galland - Don Bartolo Juan Calvo - El Cartujano
* José María Rodero - Conde de Almaviva
* Fernando Sancho - Sir Albert
* Pierre Cour - Mellao
* Antonio Riquelme - Cabo de alguaciles
* Mariano Asquerino - LAmiral
* Raúl Cancio - Rubio
* Carmen Sánchez - Doña Rosa
* Joaquín Roa - Bandido novato
* Carlos Díaz de Mendoza - Capitán
* Antonio Padilla - Saltamontes
* José Gómiz - Educao
* Juanita Azores - Señora en la barbería (as Juana Azores) Luis Rivera - Señor en la barbería
* Emilio Santiago - Barbero 2º
* Ángel Álvarez - Dueño de la posada

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 

 
 