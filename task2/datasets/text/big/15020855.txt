Lake Placid 2
{{Infobox television film
| name           = Lake Placid 2
| image          = Lake Placid 2 DVD.jpg
| caption        = DVD cover
| director       = David Flores
| producer       = Jeffery Beach Phillip Roth
| writer         = Todd Hurvitz Howie Miller John Schneider Sarah Lafleur Sam McMurray Chad Michael Collins Alicia Zielger Joe Holt Cloris Leachman
| music          = Nathan Furst
| cinematography = Lorenzo Senatore
| editing        = John Quinn
| distributor    = 20th Century Fox
| released       =   Sci Fi Channel
| runtime        = 87 minutes
| country        = United States
| language       = English
}} John Schneider Sony Pictures Sci Fi Lake Placid Sci Fi Channel original movie on April 28, 2007. The rated, and unrated DVD, releases of the film are distributed by 20th Century Fox. They also distributed the unrated Blu-Ray. The film was filmed in Bulgaria, Romania.  The DVD to the film was released on January 29, 2008.

The film was followed by   (2012), as well as the spin-off crossover Lake Placid vs. Anaconda (2015).

==Plot==
 

The film opens with two researchers on a rubber tube raft arguing until one of them is dragged into the water and killed by a crocodile. The surviving researcher, Frank Millis shows Sheriff James Riley the severed body parts of the one who was killed. James, Frank, and Officer Emma Warner venture to the lake to investigate what’s happening. After stopping the boat, Emma jumps into the lake to see if there’s anything, eventually to find Tillman’s severed head.

Next up, three adventurers, Mike, Edie, and Sharon visit the lake. James, Frank, and Emma return to land to visit a cabin home to an elderly lady named Sadie Bickerman. Emma requests a few questions from Sadie but she declines and slams her front door shut. James tries his attempt to have Sadie talk, and is a threat to post a warrant to her door if she doesn’t explain soon. On the three adventurers’ scene, Mike, Edie, and Sharon are all killed by a crocodile while swimming.

The sheriff’s son, Scott Riley wanders into the woods, mistaking an incoming crocodile for his pet dog, Daisy. Scott then meets Kerri and her boyfriend, Thad. Daisy barks about something moving in the lake but Kerri sees nothing. A much more lenient Sadie talks with a photographer at a boat dock before going back to her cabin and the photographer is captured and killed by a swarm of baby crocodiles while taking pictures.

Riding the boat again, James forcefully hits the brakes, knocking Frank into the lake but James and Emma bring him back on board to safety. When a crocodile appears, James draws his gun and the crocodile dives under to knock the three off the boat. The boat is demolished and all three go to land. The three meet Jack Struthers and Ahmad, who drove a plane to distract the crocodile.

The next scene shows Rachael and Larry, along with Scott, Kerri, and Thad venturing in the woods to some other side of the lake. On the scene of the Sheriff’s team, Deputy Dale Davis comes in to unload the boat while the crew sets up a tent for overnight camping. They next see a wild boar trapped in a rope net and feed it to the crocodile. The crew neutralizes the crocodile with their guns and Dale ties the mouth with a rope, but the crocodile easily breaks free, severs Dale’s arm and devours him. Frank dies from a fall and the others move on.

On the younger crew’s scene, Rachael mistakes Larry giving her a foot massage for a crocodile that drags her into the lakes, killing her. Larry runs away horrified. The other three of the younger crew find eggs in the woods. Thad breaks some eggs and is killed by a crocodile. The Sheriff’s crew feed a boar carcass to one crocodile and Ahmad shoots a harpoon arrow that accidentally touches Struthers plane. James and Ahmad abandon the boat and Struthers is thrown off his plane. Ahmad neutralizes the crocodile and lets out a celebrating yell. Thunderstorms strike the night, forcing the crew to their tents. Scott and Kerri are stranded in the woods until they find a horrified Larry. The Sheriff’s crew is wide asleep in their tents until the crocodile attacks and kills Ahmad.

The next morning, Scott, Kerri, and Larry climb a tree to avoid one crocodile. The Sheriff’s gang meets Sadie, who hints that only three of the younger crew are alive. Larry falls from the tree and on top of the crocodile, which kills him. James witnesses the two surviving teenagers and kills the crocodile with a grenade launcher. Sadie lets the teenagers in her house to be safe, until the elderly woman tricks them into entering the lake. Sadie is then devoured by a crocodile. The police crew battles one crocodile and Emma kills it by puncturing his mouth with a jackknife. Struthers hangs upside down on a tree and he is killed as his head is severed by the last crocodile. James kills the last one with multiple explosive substances that destroys the crocodiles nest.

At the end, Scott and Kerri express their love with a passionate kiss while James and Emma drive off somewhere.

==Cast== John Schneider as Sheriff James Riley
* Sarah Lafleur as Emma Warner
* Sam McMurray as Jack Struthers
* Chad Michael Collins as Scott Riley
* Alicia Ziegler as Kerri
* Joe Holt as Ahmad
* Ian Reed Kesler as Thad
* Justin Urich as Larry
* Cloris Leachman as Sadie Bickerman
* VJ Kewl as Rachel
* Robert Blush as Frank
* Jonas Talkington as Cal Miner
* Terence H. Winkless as Deputy Dale Davis

==Reception==
The film received generally negative reviews, citing an unoriginal plot and low-budget special effects.  

==Home media==
Lake Placid 2 was released to DVD on January 29, 2008 on both rated and unrated versions. The unrated Blu-Ray was released in 2011.

==See also==
*List of killer crocodile films

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 