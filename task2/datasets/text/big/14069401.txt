Jail Bait (1937 film)
 
{{Infobox film
| name           = Jail Bait
| director       = Charles Lamont
| producer       = {{plainlist|
*E. H. Allen
*E. W. Hammons
}}
| writer         = Paul Girard Smith
| starring       = Buster Keaton
| released       =  
| runtime        = 18 minutes
| country        = United States 
| language       = English
}}
 short comedy film starring Buster Keaton.

==Plot==
Following a kidnapping and murder, a reporter believes he knows the identity of the murderer, so he asks his room mate, a newspaper boy, to confess to the crime, in order to throw the police off the actual murderers trail – giving the reporter time to catch the murderer and claim the reward for himself. The newspaper boy is at first reluctant, but then agrees with the condition that his half of the reward be large enough to purchase a ring he wants. Having gotten himself arrested, the newspaper boy learns that his room mates plane has crashed, killing him and leaving the newspaper boy trapped in a death sentence for a crime he did not commit.

Following a prison break, the newspaper boy finds himself in the hideout of the man, Sawed-off Madison, responsible for the crimes he falsely confessed to. A shoot-out ensues, allowing both the actual murderer and the newspaper boy to escape. Back at the police station it is discovered that the newspaper boy did not commit the murder, and the police know it was Madison. At this time, the newspaper boy carries Sawed-off Madison into the station and claims the reward for his capture. The newspaper boy buys the ring he wanted.

==Cast==
* Buster Keaton Harold Goodwin 
* Bud Jamison
* Matthew Betz
* Betty André
* Stanley Blystone as Arresting officer (uncredited)
* Bobby Burns as Warden (uncredited)
* Allan Cavan as Desk Sergeant (uncredited)
* Harry Tenbrook as Prison guard (uncredited)

==See also==
* Buster Keaton filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 