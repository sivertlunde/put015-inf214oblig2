You Stupid Man
{{Infobox film
| name           = You Stupid Man
| image          = You Stupid Man.jpg
| caption        = You Stupid Man film poster.
| director       = Brian Burns
| producer       = Brian Burns Al Munteanu Peter Popp Gerald Schubert
| writer         = Brian Burns
| starring       = Milla Jovovich David Krumholtz William Baldwin Denise Richards Dan Montgomery Jr. Jessica Cauffiel
| music          = David Schwartz
| cinematography = David Herrington
| editing        = William Henry
| distributor    = 01 Distribuzione mediacs AG
| released       =
| runtime        = 95 minutes
| country        = United States English
| budget         =
| preceded by    =
| followed by    =
}}

You Stupid Man is a 2002 romantic comedy film written and directed by Brian Burns.

==Storyline==
The film follows the character of Owen as he begins to get over his former girlfriend, Chloe, who has recently moved to Los Angeles to become an actress. Owen finds love with another woman, Nadine, for a short period of time before his former girlfriends television show is canceled, and Chloe declares that she wants him back.

==Release==
You Stupid Man made its premiere at the The Hamptons International Film Festival on October 18, 2002. 

==Reception==
Reviewer Michael Rankins noted the plot was "while not stupid, the movie does manage to be agonizingly predictable"  and had many similarities to When Harry Met Sally, as one reviewer explained, the two characters "decided to follow   story line instead of coming up with their own" 

== Cast ==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Milla Jovovich || Nadine
|-
| David Krumholtz || Owen
|-
| Denise Richards || Chloe
|-
| Dan Montgomery Jr. || Jack
|-
| Jessica Cauffiel || Diane
|-
| William Baldwin || Brady
|-
| Landy Cannon || Rodger
|}

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 