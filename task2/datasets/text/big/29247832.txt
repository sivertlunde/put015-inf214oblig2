The Truth According to Wikipedia
{{Infobox film
| name           = The Truth According to Wikipedia
| image          = The Truth According to Wikipedia.jpg
| image_size     = 150px
| alt            =
| caption        =
| director       = IJsbrand van Veelen
| producer       = VPRO Backlight
| narrator       =
| starring       = Jimmy Wales Larry Sanger Andrew Keen Phoebe Ayers Tim OReilly Ndesanjo Macha Robert McHenry
| music          = Chris Everts Frank van der Sterre
| cinematography = Niels vant Hoff Pim Hawinkels Richard Kille
| editing        = Stefan Kamp Jos de Putter Doke Romeijn
| studio         =
| distributor    =
| released       =  
| runtime        = 48 minutes
| country        = Netherlands
| language       =  English
| budget         =
| gross          =
}}
The Truth According to Wikipedia (also referred to as Wikis Waarheid and Wikis Truth) is a Dutch documentary film about Wikipedia directed by IJsbrand van Veelen. The documentary examines the reliability of Wikipedia, and the dichotomy between usage of experts versus amateur editors. The film includes commentary from Wikipedia co-founders Jimmy Wales and Larry Sanger, The Cult of the Amateur author Andrew Keen, OReilly Media chief executive officer Tim OReilly, and former editor-in-chief of Encyclopædia Britannica Robert McHenry. Keen argues that experts should serve as guardians of information during the Web 2.0 phenomenon; this point of view is supported by analysis from Sanger.

The documentary premiered at the Next Web conference in Amsterdam in April 2008, and was broadcast by VPRO television in the Netherlands. It was subsequently made available through American Public Television. The Truth According to Wikipedia received a generally positive reception, with favorable commentary in a review from Film Quarterly,  and in an analysis published by the Center for Strategic and International Studies. 

==Contents==

  supports an analysis made by Andrew Keen in the documentary.]]

Director IJsbrand van Veelen examines questions about Wikipedia, including whether it will harm traditional encyclopaedias, including Encyclopædia Britannica and the reliability of Wikipedia. Veelen also addresses the idea that information wants to be free.   Individuals who are interviewed and appear as commentators in the film include Wikipedia co-founders Jimmy Wales and Larry Sanger; the author of The Cult of the Amateur, Andrew Keen; author of How Wikipedia Works and Wikipedia editor, Phoebe Ayers; Swahili Wikipedia contributor Ndesanjo Macha; OReilly Media chief executive officer Tim OReilly; We Think author Charles Leadbeater; and previous editor-in-chief over Encyclopædia Britannica, Robert McHenry.       

Discussion topics include how the contributions of both unqualified and expert users affect Wikipedia, and more broadly, the Web 2.0 phenomenon.    Andrew Keen is featured prominently in the film, and puts forth a thesis that veracity of information should be determined by experts who should function as guardians for such material.   Keens argument is supported in the film by commentary from Larry Sanger, who left Wikipedia over a conflict with Jimmy Wales regarding Sangers desire for experts to be given additional influence on the project.   

==Production==

The Truth According to Wikipedia was directed by IJsbrand van Veelen.    Van Veelen had previously directed Google Behind the Screen.  Interviews were conducted by Marijntje Denters, Martijn Kieft and IJsbrand van Veelen.  Marijntje Denters and William de Bruijn researched and gathered information for the film.  Judith van den Berg served as film producer, and film editors included Jos de Putter and Doke Romeijn.  The film utilized 60 seconds of footage from a video made by Chris Pirillo, who later objected that such usage was done without obtaining his permission or crediting him with the content.  The documentary film was released in 2008.    The controversy over the censorship of Wikipedia in China was ongoing during the time of the films release.  The film premiered globally at the Next Web conference in Amsterdam on 4 April 2008.   It was broadcast by VPRO on 7 April 2008.  The organization American Public Television (APT) began to make the film available in Summer 2008, and was contracted to show the documentary as an APT program, from February 2009 through January 2011. 

==Reception==

 

The Truth According to Wikipedia received a positive review in the journal  , the documentary was given a rating of "Good".    The documentary was given positive reception from Eric Schonfeld of TechCrunch, who commented, "The film is masterfully made and shows many points of view".  Schonfeld was critical of the documentary for its emphasis on Andrew Keen throughout the film, and noted, "it ends up being more than anything else a vehicle for Keen to put forth his diatribes against Wikipedia. You definitely get the sense that he wins the argument in the movie."  Of Keens argument, Schonfeld observed, "he misses the point that the relatively small handful of people who do most of the writing and editing on Wikipedia may very well be experts in their topic areas, or become experts by writing and researching Wikipedia articles." 
 commentator in online learning and new media Stephen Downes characterized the documentary as an "Interesting video about Wikipedia and Web 2.0."    Downes was critical of the film for using a documentary-style format in order to present its message, "The thing about this form – the video documentary – is that there is no really way (or requirement) to substantiate perspectives with argumentation and evidence."  Ernst-Jan Pfauth of The Next Web observed, "Van Veelen managed to get some interesting and authoritative people for his camera."    Pfauth posted questions raised by the films analysis, "Are equality and truth really reconcilable ideals? And most importantly, has the Internet brought us wisdom and truth, or is it high time for a cultural counterrevolution?" 

==See also==
 
*How Wikipedia Works
*La révolution Wikipédia
*The Wikipedia Revolution
*The World and Wikipedia
*Truth in Numbers?
*Wikipedia – The Missing Manual
*List of films about Wikipedia
*List of books about Wikipedia

==References==
 

==Further reading==
* 

==External links==
* , at watchdocumentary.com

 

 

 
 
 
 
 