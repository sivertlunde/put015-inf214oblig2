Raging Fists
{{Infobox film
| name           = Raging Fists
| image          = 
| image_size     = 
| caption        = 
| director       = Éric Le Hung
| producer       = André Génovès, Roger William, Pierre Belfond, Pierre Sayag, Jean-Claude Leroux
| writer         = Tony Gatlif
| narrator       = 
| starring       = Gilles Chevallier Françoise Dorner Tony Gatlif Marie-Georges Pascal Il était une fois 
| cinematography = Jean Monsigny
| editing        = Jean-Claude Bonfanti
| distributor    = 
| released       =  : 1976
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1975 cinema French drama film directed by Éric Le Hung. Cf. IMDb 

==Cast==
* Philippe Lavot : Antoine "Toni"
* Françoise Dorner : "BB la brune"
* Tony Gatlif : Nanar
* Marie-Georges Pascal : Christine
* Gilles Chevallier : Johnny
* Frédéric Norbert : Michou
* Patrick Jeantet : Dédé
* Sylvain Rougerie : Bouboule
* Fred Ulysse : Mr. Sevin
* Pascale Roberts : Mrs. Sevin
* Etienne Bierry : Tonys father
* Pierre Tornade : Jo, the bartender
* Roger Dumas : a Neighbour
* André Thorent : The Commissar

==References==
 

==External links==
*  
*   at Encyclo-ciné (French)

 
 
 
 
 
 


 