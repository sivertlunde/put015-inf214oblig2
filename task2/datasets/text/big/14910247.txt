Krazzy 4
 
{{Infobox film
| name           = Krazzy 4
| image          = Krazzy4.jpg
| image_size     =
| caption        = A promotional poster for Krazzy 4
| director       = Jaideep Sen
| producer       = Rakesh Roshan Sunaina Roshan
| writer         = Ashwni Dhir
| narrator       =
| starring       = Juhi Chawla Arshad Warsi Irrfan Khan Rajpal Yadav Suresh Menon Dia Mirza  Rajat Kapoor
| music          =
| cinematography = Ajit Bhat
| editing        = Meghna Ashchit
| distributor    = Eros Labs
| released       =    
| runtime        = 112 minutes
| country        = India
| language       = Hindi
| budget         = INR 2 million
| gross          =  
| preceded_by    =
| followed_by    =
| budget         = USD $1,16,452
}}

Krazzy 4 is a 2008 Bollywood comedy film directed by Jaideep Sen and produced by Rakesh Roshan. The film stars Juhi Chawla, Arshad Warsi, Irrfan Khan, Suresh Menon and Rajpal Yadav in lead roles while Shahrukh Khan and Hrithik Roshan appear in item numbers.  Music of the film is by Rajesh Roshan.
 The Dream Team, written originally by John Connolly and David Loucka.

==Plot==
Raja (Arshad Warsi) has a temper problem, Gangadhar (Rajpal Yadav) has an obsession of freedom, Dr. Mukherjee (Irrfan Khan), is a clean freak and Dabboo (Suresh Menon), has not spoken in years. Dr. Sonali (Juhi Chawla) is treating all four of them.
 schizophrenic Gangadhar Obsessive Compulsive Personality Disorder. Dabboo suffers from Selective Mutism. He has not spoken for years, and appears frightened all the time. He is loved by one and all.

Dr. Sonali negotiates with the mental hospitals top doctors to allow her to take the four of them for a cricket match between India and England which is being held on Independence Day. She eventually manages to get permission and the four of them get ready. The following morning, she takes them to the match and on the way stops at her clinic to pick up some important papers. She tells the group to stay in the car.

After a while, Dabboo gets out of the car and goes to a spot nearby to pee. From there, he watches Dr. Sonali being kidnapped by thugs. He notices one of the faces as a thug removes his mask. Dabboo then goes back to the car where his friends realize that something is wrong from the expression on his face, but soon ignore it as Dabboo cannot speak.

As time goes on, the four become bored. One by one, all of them except Dabboo get out of the car to look for Dr. Sonali.  They split up and go to different places.  Meanwhile, Dr. Sonali is being bribed to sign papers for declaring a criminal called Rana as insane at a hospital at 11 am the following day. As Raja searches for her, he passes by a store where there is a TV set showing a news report about Independence Day. Raja notices that the woman who is speaking is actually his ex-girlfriend, Shikha (Dia Mirza), who he had lost a long time ago. He tracks her, and the two have a tearful reunion.
 
Raja decides to go to Shikhas house to apologize to her father (Kenneth Desai), as he had been rude to him the last time they had met. He however meets Shikhas new boyfriend Pranav to who she will be engaged and married to soon. Raja becomes angry and starts beating Pranav up, and is once again thrown out of the house. Meanwhile, Gangadhar returns after having followed some school children singing Jana Gana Mana, and accidentally bumps into Dr. Mukherjee. They realise that the car is gone. Raja then returns and goes with the group to a nearby traffic station, where they find Dabboo and register a complaint that Dr. Sonali is missing.

Raja then comes up with the idea that they should call up Sonalis husband, R K Sanyal (Rajat Kapoor), and tell him what has happened. They get his number from the traffic station and find out that he is at a hotel. They find him there but he receives a call from a man who tells him that Sonali has been captured and is safe which leads the team to realize that he was behind the kidnapping. They are caught by the same man who Dabboo saw kidnapping Sonali. They manage to escape and head off to Shikhas office, begging her to help them. She reluctantly agrees, and they kidnap Sanyal at the Star Awards function. They make him announce that he will pay Rs50 million to anyone who finds Sonali.  Sanyal then escapes but this is just part of the plan as they have hidden a camera that films what he does and says.

Then, they go to Pranavs office and beg him to play the video as he works with Shikha, but he doesnt agree. They however play the tape and a very distressed husband and his partner watch it. The next day, a taxi driver picks up four people who have found Dr. Sonali and he immediately recognizes who she is and takes the four men to a village close by where they are captured and put into the earth like seeds. He then calls them and tells the four about the news. When they arrive at the scene, they find her missing again. They eventually find her wrapped in a bag and walk away only to be caught by the board of directors but escape after Sonali again when she suddenly disappears.
 
She gets into the car which her husbands partner is driving and goes to the hospital. The four follow her in an auto and arrive at the hospital moments later after Sonali was dropped off there. Dr. Mukherjee runs somewhere in the building leaving the others lost. When Sonali goes to the room where Rana is being held, the other three turn out to be the doctors who are also meant to examine Rana shortly joined by Dr. Mukherjee who ran off to see his wife who worked in the same hospital. They then torture him by injecting him with needles,  electrocuting him and by giving him a heart shock.  They then explain to Sonali that her husband was the one that kidnapped her and that it was just a trick to get her to do what they wanted.  They then catch the criminals responsible and the movie ends when all of them go back home.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Character !! Role
|-
| Arshad Warsi || Raja || An angry young man whose blood boils and fists clench, and who is ready to beat the daylights out of anyone who upsets him in any way. He is diagonosed with Intermittent Explosive Disorder.
|-
| Irrfan Khan || Dr. Mukherjee || Dr. Mukherjee suffers from Obsessive-compulsive Personality Disorder. He is obsessed with cleanliness and wants everything prim and proper. Obsessed to being in control of a situation, he doesnt realize that hes lost his mind.
|-
| Juhi Chawla || Dr Sonali || A psychiatrist who knows whats wrong with her patients, Dr. Sonali is a sensitive woman who firmly believes that "if you really want to make them well, you should treat them with your heart, not just your mind."
|-
| Rajpal Yadav || Gangadhar|| Suffering from Schizophrenia, he marches to a drum roll which only he can hear, and is fighting for a cause that is long over. He assumes an air of fighting for Swaraj (freedom). 
|-
| Suresh Menon|| Dabboo|| Cute and adorable, hes everyones pet. Even if he can hear and understand, Dabboo refuses to speak. He suffers from Selective Mutism.
|-
| Dia Mirza || Shikha || A TV reporter who controls the news on the channel called Aaj Tak, but is unable to control the rage of Raja, the man she loves.
|-
| Rajat Kapoor || R K Sanyal || An industrialist and Dr Sonalis husband who, in his quest for power and money, takes a step that has alarming repercussions for his wife and her four patients.
|- Zakir Hussain || Srivastav || A man neck deep in illegal activities, who stoops to anything to keep himself ahead.
|}

===Guest Appearances===

*Shahrukh Khan
*Hrithik Roshan
*Rakhi Sawant

==Production==
Krazzy 4 marks the first production-only venture of Rakesh Roshan. In all his previous films, he not only produced but also directed them. {{cite news|accessdate=2008-02-18|url=http://specials.rediff.com/movies/2008/feb/18slid1.htm
|work=Rediff.com|title=Krazzy 4, in pictures|date=18 February 2008}}  Sunaina, Roshans daughter, who recovered from cervical cancer, is the co-producer of this film. {{cite news|accessdate=2008-02-18|url=http://timesofindia.indiatimes.com/India_Buzz/My_brother_Hrithik/articleshow/2789880.cms
|work=The Times of India|title=My brother, Hrithik|date=18 February 2008}}  

It also marks Irrfan Khan and Juhi Chawla acting in the same film after a period of four years. {{cite news|accessdate=2008-02-18|url= http://www.glamsham.com/movies/scoops/08/feb/16-irrfan-juhi-krazzt4-arshad-warsi-rajpal-yadav-suresh-menon-191500.asp
|work=Glamsham.com|title=Irrfan-Juhi back after 4 years|date=16 February 2008}}  Chawla reprises her role of a psychiatrist after a gap of 10 years, when she did Deewana Mastana (1997). {{cite news|accessdate=2008-02-18|url= http://www.glamsham.com/movies/scoops/08/feb/13-juhi-returns-as-a-psychiatrist-after-a-decade-020802.asp
|work=Glamsham.com|title=Juhi returns as a psychiatrist after a decade|first=Joginder|last=Tuteja|date=13 February 2008}}  The last they were seen in a film was in 7½ Phere (2005). 

The film features an item number by Shahrukh Khan and Hrithik Roshan, Rakeshs son. While Hrithik will introduce the protagonists via a promotional video, Shahrukh will be shown performing at a function, as a part of an important sequence in the film. {{cite news|accessdate=2008-02-18|url= http://www.glamsham.com/movies/scoops/08/feb/18-srk-to-perform-at-a-function-for-the-roshans-020805.asp
|work=Glamsham.com|title=SRK to perform at a function for the Roshans!|date=18 February 2008}}  About Shahrukhs gesture by performing the item number, Hrithik said "its the first time something like this has been done, it highlights the solidarity of the industry. Shah Rukh has shown a lot of grace and respect for my father and were touched as a family." {{cite news|accessdate=2008-02-18|url= http://www.hindustantimes.com/StoryPage/FullcoverageStoryPage.aspx?id=dda2c069-e856-4e27-b155-2130e1148e8ejodhaaakbarmoviespecial_Special&&Headline=Its+unfair+to+judge+a+film+without+seeing+it%3a+Hrithik
|work=Hindustan Times|title=Its unfair to judge a film without seeing it: Hrithik|date=4 February 2008|last=Bhattacharya|first=Roshmila}}  While this marks the second item number for Hrithik after Dhoom 2 (2006), it will be Shahrukhs third after Kaal (2005) and Om Shanti Om (2007). {{cite news|accessdate=2008-02-18|url= http://www.ndtvmovies.com/newstory.asp?section=Movies&Slug=Hrithiks+upbeat+about+item+number&Id=ENTEN20080039657
|work=NDTV Movies|title=Hrithik upbeat about item number|date=28 January 2008|last=Jha|first=Subhash K}}  Rakhi Sawant also lent her bit by performing another item number in the film. {{cite news|accessdate=2008-02-18|url= http://movies.indiatimes.com/articleshow/2790260.cms
|work=Indiatimes.com|title=Rakhi Sawant in Yash Raj’s next?|date=18 February 2008|last=Kelkar|first=Reshma}} 

Ashwani Dhir, who wrote for Office Office, a sitcom on Indian television, wrote the dialogues and screenplay for the film. {{cite news|accessdate=2008-02-18|url= http://www.hindustantimes.com/StoryPage/StoryPage.aspx?id=41e3ffc5-17dd-48a2-919b-155bbb598267&&Headline=Writer+Ashwini+Dheer+turns+to+direction
|work=Hindustan Times|title=Writer Ashwini Dheer turns to direction|date=15 February 2008|last=Bhattacharya|first=Roshmila}}  Rajesh Roshan, Rakeshs brother has composed the music, while Javed Akhtar has penned the lyrics for the movie. {{cite news|accessdate=2008-02-18|url= http://www.hindustantimes.com/StoryPage/StoryPage.aspx?id=ebcfe0b7-7353-4f5c-ab5c-b9ffd56d1da5&&Headline=EMKrazzy+4%2fEM+trailers+hit+theatres+on+Jan+25
|work=Hindustan Times|title=Krazzy 4 trailers hit theatres on January 25|date=24 January 2008}} 

In an interview, Rakesh said that their intention was the finish the shooting by December 2007. {{cite news|accessdate=2008-02-18|url= http://www.hindustantimes.com/StoryPage/StoryPage.aspx?id=638cbaac-d459-402a-90fe-7391b35aea24&ParentID=da34ff0d-a7da-43c7-a857-8d61ae58218e&&Headline=If+were+not+inspired%2c+were+God%3a+Rajesh+Roshan
|work=Hindustan Times|title=If were not inspired, were God: Rajesh Roshan|date=17 July 2007|last=Dubey|first=Rachana}}  About why Hrithik was not a part of this film, he chose not to reveal the reasons.  Dia Mirza said about her experience working in the film:

  Im part of Rakesh Roshans Krazzy 4 though Im one of the sane members of the cast. Thats sad because Id have happily played one of the nuts. Its been good fun because I got to work with artistes Ive always admired like Irrfan Khan, Arshad Warsi and Juhi Chawla. I adore Juhi, to be working with her is like a dream come true. {{cite news|accessdate=2008-02-18|url= http://sify.com/movies/bollywood/fullstory.php?id=14605737
|work=Sify.com|title=Diya Mirza has super-blast in her first comedy|date=16 July 2007}}  

The trailers for the film were out on 25 January 2008. {{cite news|accessdate=2008-02-18|url= http://www.businessofcinema.com/boc/?file=story&id=6676 archiveurl = archivedate = 13 February 2008}} 

==Music==
{{Infobox album |  
 Name = Krazzy 4 |
 Cover       = |
 Caption     = Album cover |
 Type = Album |
 Artist = Rakesh Roshan |
 
 Released =  15 March 2008 (India)|
 Recorded = | Feature film soundtrack |
 Length = |
 Label =  T-Series|
 Producer = Rakesh Roshan |
 Reviews = |
 Last album = Krrish (2006)|
 This album = Krazzy 4 (2008)|
 Next album = Kites (film)|Kites (2010)|
}}

The music of the film is composed by Rajesh Roshan. 

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Title
! Artist
! Lyricist
! Pictured on
|-
|Break Free
| Vishal Dadlani
| Asif Ali Beg
| Shahrukh Khan
|-4:44
|Dekhta hai Tu Kya
| Sunidhi Chauhan & Kirti Sagathia
| Javed Akhtar
| Rakhi Sawant, Makrand Deshpande & Irrfan Khan
|-
| Ik Rupaya
| Bhavin Dhanak, Jimmy Moses, Kirti Sagathia, Labh Janjua, Rahul Vaidya & Sudesh Bhonsle
| Javed Akhtar
|  Ninad Kulkarni, Rajpal Yadav, Arshad Warsi, Irrfan Khan & Suresh Menon
|-6:57
|Krazzy 4
| Vishal Dadlani
| Javed Akhtar
| Hrithik Roshan
|-3:06
|O Re Lakad
| Kailash Kher, Neeraj Shridhar & Sowmya Raoh
| Javed Akhtar
| Juhi Chawla & Arshad Warsi, Irrfan Khan, Rajpal Yadav & Suresh Menon
|-
|}

Music composer Ram Sampath alleged that the music of two of the songs of this movie (Break Free and Krazzy 4) are copied from a Sony Ericsson advertisement, composed by him. He lodged a case against the producers the film claiming damages of Rs20&nbsp;million. On 10 April 2008 the Bombay High Court upheld his complaint and directed producer Rakesh Roshan to delete the songs from the film. {{cite news|accessdate=2008-04-10|url=http://www.rediff.com/movies/2008/apr/10krazzy.htm
|work=Rediff.com|title=Krazzy 4 music is a copy, declares Bombay HC|date=10 April 2008}}  The producers reached an out-of-court settlement with composer Ram Sampath, and the movie was released with the controversial songs on 11 April 2008.

==Reception and reviews==
AOL gave the film a mixed response with reviewer Noyon Jyoti Parasara giving it two and half stars. The review points out,"Overall, Jaideep Sens debut film Krazzy 4 seems like an off shoot of the 70s comedies from Hrishikesh Mukherjee. Its an honest venture. However, there are faults, quite a few actually. Probably the most glaring one is that the climax could have been more evoking.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 