Nightmare (1956 film)
{{Infobox film 
| name           = Nightmare
| image          = Nightmaremovie.jpg
| alt            = 
| image_size     = 175px
| caption        = Theatrical release poster
| director       = Maxwell Shane
| producer       = William H. Pine William C. Thomas
| screenplay     = Maxwell Shane
| based on       =   Kevin McCarthy
| music          = Herschel Burke Gilbert
| cinematography = Joseph F. Biroc
| editing        = George A. Gittens
| studio         = Pine-Thomas Productions United Artists 
| distributor    = United Artists
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English 
| budget         = 
| gross          =  
}}
Nightmare is a 1956 psychological thriller film noir starring Edward G. Robinson 
 Fear in the Night.

==Plot==
New Orleans big band clarinetist Stan Grayson has a nightmare in which he sees himself in a mirrored room, killing a man. He awakens to find blood on himself, bruises on his neck, and a key from the dream in his hand.

Grayson goes to his brother-in-law, police detective Rene Bressard, about the problem but is dismissed. Later, the two men go on a picnic in the country with Graysons girlfriend and sister.  Grayson leads them to an empty house, the house of his dream, when it begins to rain.  They are shocked to see that the house has a mirrored room just like in his dream.  After it’s found out that a murder did indeed take place, Grayson becomes Bressards number one suspect.

Grayson, stressed out and suicidal, protests his innocence, which makes Bressard dig deeper.  That leads to them finding out about a hypnotist in Graysons building who apparently set up the musician for murder.

==Cast==
* Edward G. Robinson as Rene Bressard Kevin McCarthy as Stan Grayson
* Connie Russell as Gina, Stans Girl
* Virginia Christine as Mrs. Sue Bressard
* Gage Clarke as Harry Britten

==Background== Billy May and His Orchestra perform in this version.

Even though Robinson was not the hypnotist in the film, he was promoted as such in the films movie posters.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 