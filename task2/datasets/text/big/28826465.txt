Cleopatra's Daughter
{{Infobox film
| name           = Cleopatras Daughter
| image          = Cleopatrasdaughter1962.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Fernando Cerchio
| producer       = 
| writer         = Damiano Damiani  Fernando Cerchio
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Giovanni Fusco
| cinematography = Anchise Brizzi
| editing        = Antonietta Zita
| studio         = 
| distributor    = 
| released       =   
| runtime        = 109 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}} historical drama Egypt during English version Italian script, setting the film in the 1st century BC, rather than the early Bronze Age.

==Plot==
After the death of Antony and Cleopatra Egypt is ruled by the young tyrant Pharaoh Nemorat and his mother Tegi. Cleopatra has left a surviving daughter, Shila, raised by the king and queen of Assyria. When Nemorat conquers Assyria, Shila is brought to the Egyptian court; and, at the instigation of Nemorats mother, marries the pharaoh. The pharaoh, who is a mentally disturbed hypochondriac, has a good and wise physician, Resi, who falls in love with Shila, and encourages her to go on living despite the killing of her Assyrian surrogate parents by Tegi. When Shila spurns an amorous Nemorat one evening, he goes into a violent rage causing him to faint. He is then poisoned by his ambitious chief overseer Kefren and his mistress. Shila is convicted of the murder and is sentenced to be buried alive with Nemorat. Resi comes up with a plan to save Shila by having her take a drug, which causes her to lapse into a temporary coma, while he bribes the chief of the "house of death" to allow him to take Shila away in the dead of night. The chief of the house of death proves treacherous by not allowing Resi to rescue Shila, but in a struggle with Resi he is killed. Resi, though, is seriously wounded and is rescued and tended by his faithful servant, but he does not recover before Shila is entombed with Pharaoh Nemorat. The common people, who have benefited from Resis kind care of them, aid him in capturing the royal architect, who helps Resi and his associates, greedy tomb robbers, to break into Nemorats tomb and save Shila. Shila and Resi ride off in freedom while Kefren gets his just desserts when Tegi finds out that he really poisoned Nemorat: he is killed by the palace guards.
==Cast==
*Debra Paget as "Shila"
*Ettore Manni as "Resi"
*Erno Crisa as "Kefren"
*Corrado Pani as "Khufu"
* Yvette Lebon as "Tegi"
* Robert Alda as "Inuni"
* Rosalba Neri

==See also==
*list of historical drama films

==External links==
*  

 
 
 
 
 
 

 