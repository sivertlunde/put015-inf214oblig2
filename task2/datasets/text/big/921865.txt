Beyond the Valley of the Dolls
 
{{Infobox film
| name           = Beyond the Valley of the Dolls
| image          = Poster3 beyond the valley of the dolls.jpg
| image_size     = 275px
| caption        = theatrical release poster
| director       = Russ Meyer
| producer       = Russ Meyer
| screenplay     = Roger Ebert
| story          = Russ Meyer Roger Ebert Dolly Read Cynthia Myers Marcia McBroom Erica Gavin John LaZar Michael Blodgett David Gurian Stu Phillips
| cinematography = Fred J. Koenekamp  
| editing        = Dann Cahn
| distributor    = 20th Century Fox
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $900,000   Chicago Sun-Times. Retrieved May 26, 2012.  or $2.09 million 
| gross          = $40 million 
}}
 satirical  musical melodrama Dolly Read, Cynthia Myers, Marcia McBroom, John LaZar, Michael Blodgett and David Gurian.  The cult classic was directed by Russ Meyer and co-written by Meyer and Roger Ebert.
 Valley of the Dolls—"dolls" being a slang term for depressant pills or "downers"—Beyond the Valley of the Dolls was instead revised as a parody of the commercially successful but critically reviled original.

==Plot== Dolly Read), Casey Anderson (Cynthia Myers), and Petronella "Pet" Danforth (Marcia McBroom)—perform in a rock band, The Kelly Affair, managed by Harris Allsworth (David Gurian), Kellys boyfriend. The four travel to Los Angeles to find Kellys estranged aunt, Susan Lake (Phyllis Davis), heiress to a family fortune.

Susan welcomes Kelly and her friends, even promising a third of her inheritance to her niece, but Susans sleazy financial advisor Porter Hall (Duncan McLeod) discredits them as "hippies" in an attempt to embezzle her fortune himself. Undeterred, Susan introduces The Kelly Affair to a flamboyant, well-connected rock producer, Ronnie "Z-Man" Barzell (John LaZar), who coaxes them into an impromptu performance at one of his outrageous parties (after a set by real-life band Strawberry Alarm Clock). The band is so well-received that   becomes their Svengali-style manager, changing their name to The Carrie Nations and starting a long-simmering feud with Harris.
 gigolo who porn star Ashley St. Ives (Edy Williams), but after losing Kelly he allows Ashley to seduce him. Ashley soon tires of his conventional nature and inability to perform sexually due to increasing drug and alcohol intake. Harris descends further into heavy drug and alcohol use, leading to a fistfight with Lance and a drug-addled one-night stand with Casey which results in pregnancy. Kelly ends her affair with Lance after he severely beats Harris. Casey, distraught at getting pregnant and wary of mens foibles, has a lesbian affair with clothes designer Roxanne (Erica Gavin), who pressures her to have an abortion.

Petronella has a seemingly enchanted romance with law student Emerson Thorne (Harrison Page). After a meet-cute at   party, they are shown running slow-motion through golden fields and frolicking in a haystack. Their fairy-tale romance frays when Pet sleeps with Randy Black (James Iglehart), a violent prize fighter who beats up Emerson and tries to run him down with a car.
 Charles Napier).
 drug use. Upset at being pushed to the sidelines, Harris attempts suicide by leaping from the rafters of a sound stage during a television appearance by the band. Harris survives the fall but becomes paraplegic from his injuries.

Kelly devotes herself to caring for Harris.  Emerson forgives Petronella for her infidelity. Casey and Roxanne have a steamy, intimate romance. But this idyllic existence ends when   invites Casey, Roxanne, and Lance to a  ) to death, and shoots Roxanne and Casey, killing them.

Responding to a desperate phone call Casey made shortly before her death, Kelly, Harris, Pet, and Emerson arrive at   house and try to subdue him. Petronella is wounded in the melee, which ends in   death.  Harris is able to move his feet, the start of his recovery from paralysis.

An epilogue follows, with a preachy, satirical voice-over monologue and scenes of Kelly and Harris (now in crutches) hiking on a log over a creek, and a final scene with the wedding of three couples in a courthouse - Kelly and Harris, Pet and Emerson, and Susan and Baxter - with Porter observing from outside the courthouse window.

==Cast==
   Dolly Read as Kelly Mac Namara
*Cynthia Myers as Casey Anderson
*Marcia McBroom as Petronella Danforth  
*John LaZar as Ronnie "Z-Man" Barzell
*Michael Blodgett as Lance Rocke
*David Gurian as Harris Allsworth
*Edy Williams as Ashley St. Ives
 
*Erica Gavin as Roxanne
*Phyllis Davis as Susan Lake
*Harrison Page as Emerson Thorne
*Duncan McLeod as Porter Hall
*James Iglehart as Randy Black Charles Napier as Baxter Wolfe Henry Rowland as Otto
 

;Cast notes
*Pam Grier has an uncredited bit part as a partygoer.

==Production==
Beyond the Valley of the Dolls was originally intended as a straightforward sequel to the   in 1980.   Meyers intention was for the film to "simultaneously be a satire, a serious melodrama, a rock musical, a comedy, a violent exploitation picture, a skin flick and a moralistic expose (so soon after the Sharon Tate murders) of what the opening crawl called the oft-times nightmarish world of Show Business." 

As a result, the studio placed a disclaimer at the beginning of the film informing the audience that the two films were not intended to be connected. Posters for the movie read, "This is not a sequel——there has never  been anything like it".
 X rating by the MPAA;  in 1990 in film|1990, it was re-classified as NC-17.  Meyers response to the original X rating was to attempt to re-edit the film to insert more nudity and sex, but Fox wanted to get the movie released quickly and wouldnt give him the time. 
 The Seven Minutes, although Meyers original deal was for three films.  Ebert said that Beyond the Valley of the Dolls seemed "like a movie that got made by accident when the lunatics took over the asylum." 

Because the film was put together so quickly, some plot decisions, such as the character Z-Man being revealed as a transvestite woman, were made on the spot, without the chance to bring previous already-shot scenes into alignment with the new development.   As they were shooting, the cast was uncertain whether the dialogue was intended to be comic or not, which would alter their approach to acting it.  Because Meyer always discussed their roles and the film so seriously, they did not want to unintentionally insult him by asking, so they broached the question to Ebert instead.  Meyers intention was to have the actors perform the material in a straightforward manner, saying "If the actors perform as if they know they have funny lines, it wont work."  Ebert described the resulting tone as "curious". 

In 1980, Ebert looked back on the film and said of it:

 I think of it as an essay on our generic expectations. Its an anthology of stock situations, characters, dialogue, clichés and stereotypes, set to music and manipulated to work as exposition and satire at the same time; its cause and effect, a wind-up machine to generate emotions, pure movie without message.  

Beyond the Valley of the Dolls was made while Fox was being sued by Jacqueline Susann who contended that the film had damaged her reputation as a serious author.  The suit did not go to trial until after Susanns death in September 1974. Her estate won a $2 million verdict against the studio in August 1975. 

==Music and soundtrack== Stu Phillips, The Monkees, Battlestar Galactica. The Sorcerers Apprentice for the psychedelic scene at Z-Mans house near the films end. Beyond the Valley of the Dolls: The Original Soundtrack. Liner notes from audio CD. Stu Phillips and Alex Patterson. Harkit: HRKCD 8032, 2003 

Members of the fictitious Carrie Nations neither sing nor play their own instruments in the film. Vocals for the lip-synced songs were performed by Lynn Carey, a blue-eyed soul singer based in Los Angeles. Careys voice is showcased on the apocalyptic rocker "Find It", the earnest folk anthem "Come With the Gentle People", the raunchy Rhythm and blues|R&B of "Sweet Talking Candyman", the lilting ballad "In the Long Run", and the soulful strut of "Look On Up At the Bottom".
 Incense and Come Saturday Morning LP.

Different versions of the soundtrack album exist because of disputes over royalties.  The original vinyl soundtrack, reissued in the early 2000s, substitutes Amy Rushes vocals for Lynn Careys originals; it also includes one song, "Once I Had Love", not on the 2003 CD reissue. However, the CD edition of the soundtrack contains 25 songs compared to the 12 songs on the vinyl version. "Incense and Peppermints", some incidental music, and the Strawberry Alarm Clocks Hammond organ instrumental "Toy Boy" are missing from all soundtrack releases.

==Character influences==
Roger Ebert revealed that many of BVDs themes and characters were based upon real people and events, but because neither Ebert nor Russ Meyer actually met these people, their characterizations were based on pure speculation.

*Ronnie "Z-Man" Barzell - The fictional eccentric rock producer turned Carrie Nations manager was loosely based on real life producer Phil Spector.  More than three decades later Spector was convicted of murder after the body of Lana Clarkson was found at his mansion, which is somewhat reminiscent of the events of the films climax.

*Randy Black - The heavyweight champ character was loosely based on the real World Heavyweight Champion Muhammad Ali. 
 the Manson Family. The film began production on December 2, 1969, shortly after the murders, which were covered heavily by the media.  Valley of the Dolls star Sharon Tate was among the murder victims, as was Jay Sebring. Vocalist Lynn Carey, who was dating Sebring and had been invited to join him the night of the Tate-LaBianca murders, refused his invitation, according to her comments on the DVD extras.
 a character actor who often played movie villains. 

* Susan Lake and Baxter Wolfe were, in an original draft script, Anne Welles and Lyon Burke from Valley of the Dolls. Their back-story stated in BVD ("He proposed to her but it was the wrong time", "Its been three years..."), matches the ending of the original. Following Jacqueline Susanns legal-action proceedings against 20th Century Fox, the characters were renamed and recast.   Barbara Parkins, who played Anne, was originally under contract to appear in BVD and was disappointed when she was abruptly removed from the project.  The BVD special edition DVD features a screen test with Michael Blodgett and Cynthia Myers enacting the bedroom scene between Lance and Kelly. Obviously based on an early script, the dialogue has them make reference to Anne Welles, not Susan Lake, as Kellys Aunt.

==Box office==
Despite an X rating and a modest budget of $900,000,  Beyond the Valley of the Dolls grossed ten times that amount in the U.S. market,    qualifying it as a hit for Fox. It has since grossed more than $40 million from theatrical revivals and video sales, according to Roger Ebert. 

==Legacy== Take One included BVD in their "Best Films of the 1970s" critics poll.  In 2001, the Village Voice named the film #87 on its list of the 100 Greatest Films of the Century. 

Beyond the Valley of the Dolls was released as a two-disc, special-edition DVD set on June 13, 2006, which is now out of print.

However,  , The Inn of the Sixth Happiness, and Valley of the Dolls, on May 4, 2010.

==In popular culture==
*A monologue from the film is quoted at the beginning of Sublime (band)|Sublimes cover of the song "Smoke Two Joints". Scottish electronic musician Mylo.
*The lyrics to the song "The Kelly Affair" by alternative rock band Be Your Own Pet are based on the events of the movie.
*"Look On Up At The Bottom" was covered by Redd Kross on their first full-length album, Born Innocent. The Fall during the end of the song "L.A." from The Falls 1985 album This Nations Saving Grace.
*Quotes from the film were sampled on select tracks from Natures self-titled album.
*Season Three, Episode Four of the   was titled "Beyond the Alley of the Dolls" after the film.

==Citations==
Notes
 

References
 

==External links==
 
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 