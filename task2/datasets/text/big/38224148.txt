Chance the Idol
{{Infobox film
| name           = Chance the Idol
| image          =
| caption        =
| director       = Graham Cutts
| producer       = Harry R. Sokal
| writer         = Henry Arthur Jones (play)   Curt J. Braun
| starring       = Jack Trevor   Agnes Esterhazy   Harry Liedtke   Gertrud de Lalsky
| music          = Felix Bartsch
| cinematography = Theodor Sparkuhl
| editing        = 
| studio         = H.R. Sokal Film
| distributor    = National-Film 
| released       = 1927
| runtime        = 
| country        = Germany
| awards         =
| language       = Silent   German intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}}
Chance the Idol (German: Die Spielerin) is a 1927 German silent film directed by Graham Cutts and starring Jack Trevor, Agnes Esterhazy and Harry Liedtke. It was based on a play by Henry Arthur Jones. Cutts was working in Germany at the time for Gainsborough Pictures.

==Cast==
* Jack Trevor as Golding
* Agnes Esterhazy as Ellen 
* Harry Liedtke as Ryves 
* Gertrud de Lalsky as Lady Nowell 
* Philipp Manning as Mr. Farndon 
* Dene Morel as Alan 
* Frida Richard as Mrs. Farndon 
* Elza Temary as  Sylvia

==Bibliography==
* Cook, Pam. Gainsborough Pictures. Cassell, 1997.
* Wood, Linda. British Films 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 