The Adventurers (1926 film)
{{Infobox film
| name           = The Adventurers 
| image          = 
| image_size     = 
| caption        = 
| director       = Rudolf Walther-Fein 
| producer       = 
| writer         = Ruth Goetz   Rudolf Herzog (novel)
| narrator       = 
| starring       = Harry Liedtke   Erna Morena   Margarete Schlegel   Paul Biensfeldt
| music          = 
| editing        =
| cinematography = Curt Oertel   Emil Schünemann
| studio         = Aafa-Film 
| distributor    = Aafa-Film 
| released       = 11 February 1926
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} silent adventure film directed by Rudolf Walther-Fein and starring Harry Liedtke, Erna Morena and Margarete Schlegel. The art direction was by Jacek Rotmil. The film was based on a novel by Rudolf Herzog. It premiered in Berlin on 11 February 1926. 

==Cast==
* Harry Liedtke as Dr. Josef Otten - ein berühmter Sänger 
* Erna Morena as Maria, seine Frau 
* Margarete Schlegel as Carmen - deren Tochter 
* Paul Biensfeldt as Klaus Güllich - Ottens Faktotum 
* Eduard von Winterstein as Karl Lüttgen - Hüttenwerksbesitzer 
* Mady Christians as Armely - seine Frau 
* Hans Brausewetter as Moritz Lachner - Akademiker 
* Franz Schönfeld as Franz Terbroich - Fabriksbesitzer 
* Ernst Hofmann as Laurenz - sein Sohn 
* Max Menden as Der Impresario 
* Maria Lingen as Marchesa della Margarita 
* Robert Leffler as Der Konzertdirektor

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 