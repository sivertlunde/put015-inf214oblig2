Mr. Bharath
{{Infobox film 
| name = Mr. Bharath
| image = Mr. Bharath.jpg
| caption = 
| director = SP. Muthuraman
| writer = Visu
| screenplay = Visu
| story = Salim-Javed Ambika Sharada Sharada Raghuvaran Goundamani Visu S. V. Shekhar
| producer = M. Saravanan (film producer)|M. Saravanan M. Balasubramaniam
| music = Ilaiyaraaja
| cinematography = T. S. Vinayagam
| editing = R. Vittal C. Lancy
| studio = AVM Productions
| distributor = AVM Productions
| released = 10 January 1986
| runtime = 
| country = India Tamil
| budget =
| gross =  2.00 crore
}}

Mr. Bharath is a Tamil film is directed by SP. Muthuraman. The film stars Rajnikant, Sathyaraj, Ambika (actress)|Ambika, Goundamani, S. V. Shekhar and others. The plot is about a sons revenge against his father. It is a remake of the 1978 Hindi hit film Trishul (film)|Trishul which starred Amitabh Bachchan and Sanjeev Kumar. The movie got good reviews and was declared a hit at the box office.

==Plot==

Bharath (Rajinikanth) a young man unknown who is his father was. Shanti (Sharada (actress)|Sharada) mother of Bharath tells him about his father at her deathbed. 

Gopinath (Sathyaraj) is a rich young man,who comes for Paapampatti village for his construction work. He woos Shanti an innocent village girl and tries to impress her pretending innocent. She in turn fully trusts him fully where he takes advantage of, making her pregnant. Gopinath leaves the village promising that he would return soon to marry her. Shanti finds that Gopinath is about to get married to another woman and travels to  visit Gopinath where he refuses to accept her stating that she has no proof regarding the intimacy that made her pregnant and says she was after him for his richness. Exasperated and Insulted, Shanti henceforth challenges Gopinath saying that one day her son will question him on the future to make him confess the truth about and then leaves the place. Shanti faces hardships and raises her son to be what he becomes now. 

Back to the Present, She requests Bharath to fulfill her challenge and passes off on her death bed. Bharath wows to her mothers challenge of making Gopinath (his biological father now) to confess the truth by next year August 31, the death day of his mother.

Bharath leaves for the city and finds out his father. He meets Gopinath as a some strange man and uses Gopinaths money to start his own venture in the construction field. Both Bharath and Gopinath become enemies and both of them try to overtake each other in illegitimate way. Bharath once helps a girl from goons and takes to her home and finds that the girl is Pushpa,daughter of Gopinath and is his sister. Bharath wins the affection of Mrs.Gopinath as her son. He arranges for marriage of Pushpa with the son of a wealthy man Kumeresa Gounder who helps him to fulfill the challenge which he have against Gopinath. Bharath also uses the love relationship between his brother and his friends sister to corner Gopinath. Bharath uses every possible ways to blackmail and torture Gopinath to make him realise his mistake but Gopinath does not reciprocate instead becomes more anger.

Bharath invites him to inaugurate his orphanage on August 31, the date when he planned to fulfill his mothers wish. Gopinath is very anger on Bharath and hires a local goon who once was his enemy and now to Bharath to kill him in the inaugural function. Bharath reveals his mothers identity in the function and waits for Gopinath to accept his mistake.

Did Gopinath recognized who Bharath is and did he accept him as his son forms the rest of the story.

==Cast==
* Rajinikanth
* Sathyaraj Ambika
* Saharada
* Goundamani
* S. Ve. Shekher
* Raghuvaran
* Visu
* Deepan
* Viji
* Vadivukkarasi
* Jothichandra
* Delhi Ganesh
* I. S. R

==Production==
Sathyaraj acted as Rajinikanths father in the film though he was in fact 4 years younger to Rajinikanth.  

==Music== same name also starring Sathyaraj. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vairamuthu || 04:31
|-
| 2 || Ennamma Kannu || Malaysia Vasudevan, S. P. Balasubrahmanyam || Vaali (poet)|Vaali|| 04:36
|-
| 3 || Enthan Uyirin || S. Janaki || Pulamaipithan || 04:24
|- Vaali || 06:08
|-
| 5 || Pacha Molaga Athu || Malaysia Vasudevan, S. Janaki || Gangai Amaran || 04:41
|}

==Release==
Mr Bharath got terrific openings but could not sustain at box office. The film was appreciated for Rajinis style of tackling Sathyaraj but the critics gave it a thumbs down for tampering the original script of Trishul and miscasting of Sathyaraj and S. Ve. Sekar in the roles of Sanjeev Kumar and Shashi Kapoor. Rajinis stardom saved the film from becoming flop to making it 100 day run in few centres. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 