Cecil B. Demented
 
{{Infobox film
| name = Cecil B. Demented
| image = Cecil b demented.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = John Waters
| producer = Joseph M. Caracciolo, Jr. John Fiedler Mark Tarlov
| writer = John Waters Mike Shannon
| music = Basil Poledouris Zoë Poledouris
| cinematography = Robert M. Stevens
| editing = Jeffrey Wolf Le Studio Canal+ Polar Entertainment
| distributor = Artisan Entertainment
| released =  
| runtime = 88 minutes  
| country = United States
| language = English
| budget = $10 million 
| gross = $1,961,544 
}} terrorist filmmakers; they force her to star in their underground film. Stephen Dorff stars as the eponymous character and leader of the group, with Alicia Witt, Adrian Grenier, Michael Shannon and Maggie Gyllenhaal co-starring as the rest of his gang of filmmakers. Each of the filmmakers in the film bears tattoos of various independent directors names, including Otto Preminger, Kenneth Anger, Sam Peckinpah, David Lynch, Herschell Gordon Lewis, Spike Lee, Rainer Werner Fassbinder, Pedro Almodóvar and Andy Warhol. 
 cameo role. As with all of Waters films, it was shot in Baltimore, Maryland. The film was given a limited release in North American cinemas on August 11, 2000. It was released to home media through Artisan Entertainment|Artisan, and later, Lionsgate Home Entertainment in the United States. 
 Worst Actress cult status since its release. 

==Plot==
Honey Whitlock is a Hollywood A-list actress whose public persona is that of a sweet and considerate woman, but who is actually profane, unreasonable, and demanding. While in Baltimore to attend a Film premiere|premiere, Honey is kidnapped by the manic film director, Cecil B. Demented, and his band of misfit, Andy Warhol–worshiping artists who have branded themselves "kamikaze filmmakers", going by the group name "SprocketHoles". Each of the SprocketHoles has infiltrated the staff of the theater where the premiere is to take place; they subsequently kidnap Honey as she concludes her remarks on stages. In the ensuing mayhem, the group escapes. 

Honey is taken to an abandoned movie theater where she is kept captive. Honey is introduced to Cecils crew of followers, each of whom wears a tattoo of a noted filmmaker and reveals unique, individual quirks. Cecil explains that he wants to make his masterpiece film and needs Honey to star as the lead. At first she resists, shooting scenes with no emotion, but when Cecil demands better results, Honey gives an over-the-top performance in the films opening scene which pleases him. Apart from the first scene, Cecil, Honey and the crew roam  around the city filming scenes at real (unapproved) locations, often involving innocent bystanders in the process.

The groups first location is a luncheon being hosted by the Baltimore Film Commission. The group crashes the event and Cecil orders Honey to jump off the roof of a nearby building, which she does without safety measures. A gunfight ensues between Cecils crew and the police. As gunfire is exchanged, Rodney the hairdresser is killed and Cecil is wounded. Honey uses the opportunity to turn herself in to the authorities and they take her away in a police car, but she is retrieved by the film group soon after. 

As Honey seems to become more comfortable with her situation, possibly developing Stockholm syndrome, she watches a television special discussing her disappearance. Persons who knew her, including her ex-husband, are interviewed and come clean about how mean-spirited she was in daily life. Honey now realizes that her desire to escape would only lead her back to Hollywood, where she is hated for being rude. She resists the idea of joining Cecils followers but changes her mind and declares herself "Demented forever", burning a brand into her arm and officially joining the motley crew.

After these events, the crew invades the set of the Forrest Gump sequel being filmed in Baltimore, at Honeys suggestion. When the SprocketHole crew arrives, they subdue and replace many of the films crew. A gunfight breaks out between Cecils friends and Teamsters who got free. Members of Cecils crew are either killed or wounded. The surviving SprocketHoles and Honey flee to a nearby pornographic theater and seek refuge inside. The audience helps Cecil escape. 

At their last location, Cecil is shooting the final scene at a local drive-in while law enforcement are alerted. Cecil and the crew take over the projection room, and he proceeds to excite the crowd into a frenzy. He asks Honey to light her hair on fire for the final shot (which she does). With the film finished, the SprocketHoles start having sex in public before the authorities step in. Cecil sets himself completely ablaze as police arrive, to give Honey a chance to run away. In the ensuing chaos, some crew members escape with the raw film footage while others are shot. Honey is taken into custody; she is surprised and pleased by the new affection shown to her by the crowd as she is put into the police van.

==Cast==
 
* Melanie Griffith as Honey Whitlock
* Stephen Dorff as Sinclair/Cecil B. Demented, the director (Otto Preminger)*
* Alicia Witt as Cherish, the actress (Andy Warhol)*
* Adrian Grenier as Lyle, the actor (Herschell Gordon Lewis)*
* Mink Stole as Mrs. Sylvia Mallory
* Ricki Lake as Libby, Honeys publicist
* Larry Gilliard, Jr. as Lewis, the art director (David Lynch)*
* Maggie Gyllenhaal as Raven, the makeup artist (Kenneth Anger)*
* Jack Noseworthy as Rodney, the hair stylist (Pedro Almodóvar)* Mike Shannon as Petie, the driver (Rainer Werner Fassbinder)*
* Eric M. Barry as Fidget, the costume designer (William Castle)*
* Zenzele Uzoma as Chardonnay, the sound artist (Spike Lee)*
* Erika Lynn Rupli as Pam, the cinematographer (Sam Peckinpah)*
* Harriet Dodge as Dinah, the producer (Sam Fuller)*
* Patricia Hearst as Fidgets mother
* Channing Wilroy as Shop steward
* Kevin Nealon as himself
* Roseanne Barr as herself
* Jeffrey Wei as William the heart patient
* Rosemary Knower as Sinclair/Cecils mother
* Doug Roberts as Sinclair/Cecils father
* Eric Roberts as Honeys ex-husband
* John Waters (uncredited) as Reporter
 

 *  Denote the directors name tattooed on the characters. 

==Release==
The film was screened out of competition at the 2000 Cannes Film Festival.   

===Reception===
Roger Ebert gave the film one and a half stars out of four, remarking that it was like "a home movie   a bunch of kids goofing off",  while others such as Peter Travers of Rolling Stone said "DeMented is Waters the way we like him—spiked with laughs and served with a twist". 

The review aggregator Rotten Tomatoes reported that 51% of critics gave Cecil B. Demented positive reviews, based on 78 reviews; the consensus states "The idea behind John Waters latest has much potential, but the movie ends up being too sloppy and underdeveloped in terms of script and direction. Also, by todays standards, it fails to shock."  Metacritic reported the film had an average score of 57 out of 100, based on 32 reviews. 

===Box office===
Cecil B. Demented was a box office failure, grossing a mere $1,961,544   Retrieved October 25, 2013  from an estimated $10 million budget.   

==Soundtrack==
 
The soundtrack was released August 8, 2000 by RCA Records. Moby
# "Nice Tranquil Thumbs in Mouth" – The Locust
# "Bankable Bitch" – DJ Class and Teflon the Bull
# "Upstart" – Meatjack
# "Everyday" – Substance D
# "No Budget" – DJ Class and Mayo
# "Broadway Brouhaha"
# "Loopy" – XXXBombshell
# "An Extra Piece of Dead Meat" – The Locust
# "Demented Forever" – Karen McMillan
# "Seduction" – The Sex-o-Rama Band
# "Ciao!" – Liberace
# "Chow" – Jerome Dillon

==See also==
* List of American films of 2000
* Cecil B. DeMille
* Patricia Hearst
* The King of Comedy (1983 film)|The King of Comedy—a 1983 film directed by Martin Scorsese with a similar plot

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 