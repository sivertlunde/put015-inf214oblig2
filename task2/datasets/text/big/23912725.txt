Killer Dill
 
{{Infobox film
| name           = Killer Dill
| image          = Killer Dill poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Lewis D. Collins Max M. King
| writer         = Alan Friedman  (story)  John ODea (writer)
| narrator       =
| starring       = John Thompson
| cinematography = William A. Sickner
| editing        = Martin G. Cohn
| distributor    = Screen Guild
| released       = 2 August 1947
| runtime        = 75 minutes
| country        =United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Killer Dill is a 1947 American film directed by Lewis D. Collins set in 1931 during the Prohibition Era.

== Plot ==
Low key lingerie salesman Johnny Dill loses his girlfriend Judy Parker to his long time friend, the charming lawyer William T. Allen. And when he takes his assistant Millie Gardner to a movie, all she talks about is the manly gangster hero Big Nick Moronie. Discouraged by the fact that every woman seems to want something completely different from what he has to offer, Johnny decides to change his ways and become more of a tough gangster himself to improve his chances. He drops into a bar and plays out his new act in full, succeeding in upsetting the real Big Nick Moronie, who is considered to be "public enemy number 21" on the ranking.

Big Nick has a beef with "public enemy number 24", Maboose, but when he sends his goon Little Joe, who should be called Big Joe, to mess with the lower ranked gangster, he is paid to kill Big Nick instead. A while later Little Joe kills Big Nick in the gangsters own apartment, which is just across the hall from Johnnys. Little Joe doesnt know how to dispose of the body, so he puts it in one of Johnnys lingerie trunks, without Johnnys knowledge.

When Johnny later finds the body in his trunk he puts it into the back of a car and drives off. The body falls of the car when Johnny is chased by a police car. Everyone thinks Johnny is the one who did off with Big Nick, and all over the news he is called "Killer Dill". Eventually he comes out of his hiding and a trial ensues. He is defended by his old friend William, and is found not guilty.

Everyone still believes he is the killer, and in the gangster world he is now known as "public enemy number 21" after the person he supposedly killed. Johnny is out of the ashes and into the fire, since Big Nicks brother Louie is eager to get revenge. After advice from the slightly incompetent William, Johnny tries to team up with Maboose for protection. But Little Joe is also making a deal with Maboose, to get rid of Louie. Before Louie is killed, Johnny bumps into Little Joe, and threatens him with a toy gun. Afraid of being shot, Little Joe confesses to killing Big Nick, and Johnny makes him write down a statement where he takes responsibility for the murder. Louie overhears their conversation, and when Little Joe eventually discovers that the gun is a toy and starts strangling Johnny, Louie comes to the rescue. Little Joe is thrown out the window. William, who has worked for Maboose all along, makes Johnny destroy the statement to not incriminate his boss. Judy finally sees what a stand-up guy Johnny really is. She breaks off her engagement to William, and proposes to Johnny instead. 

== Cast ==
*Stuart Erwin as Johnny Killer Dill
*Anne Gwynne as Judy Parker
*Frank Albertson as William T. Allen
*Mike Mazurki as Little Joe
*Milburn Stone as Maboose
*Dorothy Granger as Millie Gardner
*Anthony Warde as Louie Moroni
*Dewey Robinson as McGowan, house detective
*Ben Welden as Big Nick Moroni
*Julie Gibson as Joan, model
*Shirley Hunter as Gloria
*Lola Jensen as Other model with Joan
*Margaret Zane as Girl with Gloria
*Stanley Ross as Mushnose

== External links ==
* 
* 

==References==
 

 

 
 
 
 
 
 
 
 