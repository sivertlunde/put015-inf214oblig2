Moonrise Kingdom
 
{{Infobox film
|name=Moonrise Kingdom
|image=Moonrise Kingdom FilmPoster.jpeg
|alt=
|caption=Theatrical release poster
|director=Wes Anderson Steven Rales Jeremy Dawson
|writer=Wes Anderson Roman Coppola
|starring=  
|cinematography=Robert Yeoman
|music=Alexandre Desplat
|editing=Andrew Weisblum Indian Paintbrush
|distributor=Focus Features
|released= 
|runtime=94 minutes  
|country=United States
|language=English
|budget=$16 million   
|gross=$68,263,166 
}}
Moonrise Kingdom is a 2012 American film directed by Wes Anderson, written by Anderson and Roman Coppola. Described as an "eccentric pubescent love story", it features newcomers Jared Gilman and Kara Hayward in the films main roles and an ensemble cast.     
 Academy Award Best Original Screenplay. It was also nominated for the Golden Globe Award for Best Motion Picture – Musical or Comedy. 

==Plot==
 
In September 1965, on a New England island called New Penzance, 12-year-old orphan Sam Shakusky is attending Camp Ivanhoe, a Khaki Scout summer camp led by Scoutmaster Randy Ward. Suzy Bishop, also 12, lives on the island with her parents, Walt and Laura, both attorneys, and her three younger brothers in a house called Summers End. Sam and Suzy, both introverted, intelligent and mature for their age, met in the summer of 1964 during a church performance of Noyes Fludde and have been pen pals since then. Having fallen in love over the course of their correspondence, they have made a secret pact to reunite and run away together. Sam brings camping equipment, and Suzy brings her binoculars, six books, her kitten, and her brothers battery-powered record player. They hike, camp and fish together in the wilderness with the goal of reaching a secluded cove on the island. They are confronted by a group of Khaki Scouts who try to capture them, and during the resulting altercation, Suzy injures one of the Scouts with scissors and Camp Ivanhoes dog is killed by a stray shot from a bow and arrow wielded by one of the Scouts. The Scouts flee and Sam and Suzy hike to the cove which they name Moonrise Kingdom. They set up camp and go swimming. Later, while drying off, they begin dancing to Françoise Hardy in their underwear. As the romantic tension between them grows, they kiss repeatedly.
 Social Services" electroshock therapy.

The Camp Ivanhoe Scouts have a change of heart and decide to help the couple. Together, they paddle to neighboring St. Jack Wood Island to seek out the help of Cousin Ben, an older relative of one of the Scouts. Ben works at Fort Lebanon, a larger Khaki Scout summer camp located on St. Jack Wood Island and run by Commander Pierce, who is Wards boss and views Ward as incompetent. Ben decides that the best available option is to try to get Sam and Suzy aboard a crabbing boat anchored off the island so that Sam can work as a crewman and avoid Social Services, but before leaving he performs a "wedding" ceremony, which he admits is not legally binding. Sam and Suzy never make it onto the crabbing boat, and instead are pursued by Suzys parents, Captain Sharp, Social Services and the Scouts of Fort Lebanon under the command of Scoutmaster Ward, who displays great leadership after Commander Pierce is incapacitated.
 hurricane and flash flood strike only three days after Sam and Suzy first ran away from home and, after many twists and turns, Sharp apprehends Sam and Suzy on the steeple of the church in which they first met. The steeple is destroyed by lightning, but everyone survives. During the storm, Sharp decides to become Sams legal guardian, thus saving Sam from the orphanage, as well as allowing him to remain on New Penzance Island and maintain contact with Suzy.

At Summers End, Sam is painting a landscape of Moonrise Kingdom. Suzy and her brothers are called to dinner. On slipping out of the window to join Sharp in his patrol car, Sam tells Suzy that he will see her the following day.

==Cast==
*Jared Gilman as Sam Shakusky
*Kara Hayward as Suzy Bishop
*Bruce Willis as Captain Duffy Sharp
*Edward Norton as Scout Master Randy Ward
*Bill Murray as Walt Bishop
*Frances McDormand as Laura Bishop
*Tilda Swinton as Social Services
*Jason Schwartzman as Cousin Ben
*Bob Balaban as the Narrator
*Harvey Keitel as Commander Pierce
*Rob Campbell as Deluca
*Larry Pine as Mr. Herbert Billingsley
*Seamus Davey-Fitzpatrick as Roosevelt
*Chandler Frantz as Gadge
*Lucas Hedges as Redford
*Charlie Kilgore as Lazy-Eye
*Andreas Sheikh as Panagle
*L.J. Foley as Izod

==Production==

===Cinematography===
Cinematographer Robert Yeoman shot the movie on  ), using Aaton Xterà and A-Minima cameras.   

===Locations=== Trinity Church, Newport State Airport, Long Pond Woods, and Newports Ballard Park. 

Google Earth was used for initial location scouting, according to director Anderson.

 We had to figure out where we were shooting this movie—in Canada or Michigan or New England ...&nbsp;? We started out with "Where is this girl   house, and where is the naked wildlife we want?" So  , we traveled around a bit, to Cumberland Island in Georgia, to the Thousand Islands on the New York/Ontario border ... we checked out all these locations.    

A house in the Thousand Islands region in New York was used as the model for the interior of Suzy’s house on the set built for the movie. Conanicut Island Light, a decommissioned Rhode Island lighthouse, was used for the exterior. 

===Fictitious books and maps===
In the film, 12-year-old Suzy packs six (fictitious) storybooks she stole from the public library. Six artists were commissioned to create the jacket covers for the books, and Wes Anderson wrote passages for each of the books. Suzy is shown reading aloud from three of the books during the film. Anderson had considered incorporating animation for the reading scenes, but chose to show her reading with the other actors listening spellbound. In April 2012, Anderson decided to animate all six books and use them in a promotional video in which Bob Balaban, who plays the film’s narrator, introduces the segment for each of these imaginary books.   

On designing the maps for the fictitious New Penzance Island and St. Jack Wood Island, director Anderson said, "Its weird because youd think that you could make a fake island and map it, and it would be a simple enough matter, but to make it feel like a real thing, it just always takes a lot of attention".    Anderson further stated that the movie

 has maps, and it has books, and it has watercolor paintings and needle-points, and a lot of different things that we had to make. And all these things just take forever, but I feel like even if they dont get that much time  , you kind of feel whether or not they’ve got the layers of the real thing in them.  

==Release==
 .]]
The film premiered on May 16, 2012, as the opening film at the 2012 Cannes Film Festival, where it screened in competition.  It was released in French theaters the same day. The American limited release occurred on May 25, and set a record for the best per-theater-average for a non-animated movie by grossing an average of $130,752 in four theaters.   

Finishing its theatrical run on November 1, 2012, Moonrise Kingdom grossed $45,512,466 domestically and $22,750,700 in international markets for a worldwide total of $68,263,166. 

===Home media===
Moonrise Kingdom was released on   will give the film both a DVD and Blu-ray release. 

==Reception==

===Critical response===
  
Moonrise Kingdom received widespread acclaim; review aggregation website Rotten Tomatoes gives the film a rating of 94% based on reviews from 223 critics, with an average score of 8.2/10.  The consensus states, "Warm, whimsical, and poignant, the immaculately framed and beautifully acted Moonrise Kingdom presents writer/director Wes Anderson at his idiosyncratic best." Review aggregation website Metacritic gives the film a weighted average score of 84 (out of 100), based on 43 reviews, indicating "universal acclaim." 
 Christopher Orr of The Atlantic wrote that Moonrise Kingdom "captures the texture of childhood summers, the sense of having a limited amount of time in which to do unlimited things" and is "Andersons best live-action feature" because "it takes as its primary subject matter odd, precocious children, rather than the damaged and dissatisfied adults they will one day become."   

Kristen M. Jones of Film Comment wrote that the film "has a spontaneity and yearning that lend an easy comic rhythm", but it also has a "rapt quality, as if we are viewing the events through Suzys binoculars or reading the story under the covers by a flashlight." 

===Best of lists===
Film magazine Sight & Sound listed the film at #7 on its list of best films of 2012.  Manohla Dargis of The New York Times named Moonrise Kingdom among the best 10 films of 2012. 

===Accolades===
 
{|class="wikitable plainrowheaders" style="font-size: 95%;"
|+List of awards and nominations Award
!scope="col"|Date of ceremony Category
!scope="col"|Recipients and nominees Result
|- Academy Awards   85th Academy February 24, 2013 Academy Award Best Writing (Original Screenplay) Wes Anderson and Roman Coppola
| 
|- American Cinema Editors  February 16, 2013 Best Edited Feature Film - Comedy or Musical Andrew Weisblum
| 
|- American Film Institute  American Film December 2012 AFI Award for Movie of the Year Jeremy Dawson, Scott Rubin, Steven M. Rales, Wes Anderson
| 
|- AFI Film Award
|Moonrise Kingdom
| 
|- Austin Film Critics Association  Austin Film December 18, 2012 Best Film
|
| 
|- Bodil Awards  March 16, 2013 Best American Film
|Moonrise Kingdom
| 
|- British Academy of Film and Television Arts  February 10, 2013 Original Screenplay Wes Anderson, Roman Coppola
| 
|- Broadcast Film Critics Association 18th Critics January 10, 2013 Broadcast Film Best Film
|Moonrise Kingdom
| 
|- Broadcast Film Best Young Performer Kara Hayward
| 
|- Broadcast Film Best Cast
|Moonrise Kingdom cast
| 
|- Broadcast Film Best Original Screenplay Wes Anderson, Roman Coppola
| 
|- Broadcast Film Best Composer Alexandre Desplat
| 
|- Cannes Film Festival   2012 Cannes May 27, 2012 Palme dOr Wes Anderson 
| 
|- Golden Globe Awards     70th Golden January 13, 2013 Golden Globe Award for Best Motion Picture – Musical or Comedy
|Moonrise Kingdom
| 
|- Gotham Awards  Gotham Independent November 26, 2012 Best Ensemble Performance 
|Moonrise Kingdom cast
| 
|- Best Feature
|Moonrise Kingdom
| 
|- Guldbagge Awards    48th Guldbagge January 21, 2013 Best Foreign Film
|Moonrise Kingdom
| 
|- Independent Spirit Awards  February 23, 2013 Best Cinematography Robert Yeoman
| 
|- Best Director Wes Anderson
| 
|- Best Feature
|Moonrise Kingdom
| 
|- Best Screenplay Wes Anderson, Roman Coppola
| 
|- Best Supporting Male Bruce Willis
| 
|- Producers Guild of America January 26, 2013 Producers Guild Best Picture Scott Rudin & Wes Anderson, Jeremy Dawson, Steven M. Rales
| 
|- Writers Guild of America February 17, 2013 Writers Guild Best Original Screenplay Wes Anderson and Roman Coppola
| 
|- San Diego Film Critics Society  December 11, 2012 Best Original Screenplay Wes Anderson, Roman Coppola
| 
|- Best Production Design Adam Stockhausen
| 
|- Best Score Alexandre Desplat
| 
|- Satellite Awards  December 16, 2012 Best Original Screenplay Wes Anderson, Roman Coppola
| 
|- Best Picture
|Moonrise Kingdom
| 
|- World Soundtrack Awards  October 20, 2012 Soundtrack Composer of the Year Alexandre Desplat
| 
|- Young Artist Awards    34th Young May 5, 2013 Young Artist Best Performance in a Feature Film - Leading Young Actor Jared Gilman
| 
|- Young Artist Best Performance in a Feature Film - Leading Young Actress Kara Hayward
| 
|}

==Soundtrack==
{{Infobox album  
| Name        = Moonrise Kingdom
| Type        = Soundtrack
| Artist      = Alexandre Desplat, various artists
| Cover       = 
| Released    = May 22, 2012
| Recorded    = Classical
| Length      = 1:04:11 ABKCO
| Producer    =
| Chronology  = Wes Anderson film soundtrack Fantastic Mr. Fox (2009)
| This album  = Moonrise Kingdom (2012) The Grand Budapest Hotel (2014)
}}
 
The soundtrack features music by Benjamin Britten, a composer notable for his many works for childrens voices. At Cannes, during the post-screening press conference, Anderson said, The Britten music had a huge effect on the whole movie, I think. The movies sort of set to it. The play of Noyes Fludde that is performed in it—my older brother and I were actually in a production of that when I was ten or eleven, and that music is something Ive always remembered, and made a very strong impression on me. It is the colour of the movie in a way.   A Midsummer Nights Dream ("On the ground, sleep sound"). 
 Fantastic Mr. Fox, with percussion compositions by frequent Anderson collaborator Mark Mothersbaugh. The final credits of the film features a deconstructed rendition of Desplats original soundtrack in the style of Brittens Young Persons Guide, accompanied by a childs voice to introduce each instrumental section. 

==References==
 

==External links==
 
 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 