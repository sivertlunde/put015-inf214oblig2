Forbidden World
{{Infobox film
| name           = Forbidden World
| image          = Forbidden world.jpg
| image_size     =
| caption        = Promotional poster
| director       = Allan Holzman
| producer       = Roger Corman
| writer         = Tim Curnen, R.J. Robertson, Jim Wynorski
| narrator       =
| starring       = Jesse Vint, Dawn Dunlap, June Chadwick, Linden Chiles, Fox Harris, Raymond Oliver, Scott Paulin
| music          = Susan Justin
| cinematography = Tim Suhrstedt
| editing        = Allan Holzman, Martin Nicholson
| distributor    = New World Pictures
| released       = May 7, 1982
| runtime        = 77 min
| country        = United States English
| budget         = under $1 million Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 208 
| gross          = $4 million 
| preceded_by    =
| followed_by    =
}}

  
 science fiction/horror Michael Bowen, and Don Olivera.

The film received three nominations for the 1983  , with sex, nudity, uneven editing, cheap special effects, and an audio track that some found unpleasant.  It has, however, attained a certain cult status among fans of grungy, cheap, sleazy science fiction.   It is frequently paired with and compared to the previous years Corman-produced Alien (film)|Alien rip-off Galaxy of Terror, with which Forbidden World shares some of the same sets (designed by James Cameron). The movie also makes use of footage recycled from the 1980 movie Battle Beyond the Stars, which was also produced by Corman. It is notable for its gruesome violence, oddball electronica music score by Susan Justin (available in full as a DVD-ROM feature on the German release of the film), odd, choppy editing and a scene in which the two female leads take a shower together.

Forbidden World has also been released under the titles Mutant and Subject 20.

== Plot synopsis ==
In the distant future, a genetic research station is located on the remote desert planet of Xarbia, and a research team has created an experimental lifeform they have designated "Subject 20". This lifeform was built out of the synthetic DNA strain, "Proto B", and was intended to stave off a galaxy-wide food crisis. However, Subject 20 mutates rapidly and uncontrollably and has killed all of the laboratory subject animals before cocooning itself within an examination booth. After Subject 20 hatches from its cocoon, it begins killing the personnel at the station, starting with the lab tech charged with cleansing the subject lab of the dead animal test subjects.

Professional troubleshooter Mike Colby (Vint), accompanied by his robot assistant SAM-104 (Olivera), is called in to investigate the problem. After Colby settles in, his decision to terminate Subject 20 to prevent further deaths is met with research-minded secrecy and resistance. The staff of the station includes the head of research, Gordon Hauser (Chiles), his assistant Barbara Glaser (Chadwick), lab assistant Tracy Baxter (Dunlap), the station head of security and Cal Timbergen (Harris), the chief of bacteriology.

As Subject 20 continues to unleash a storm of gory fatalities that decimates most of the station crew, the lid on the deception is finally withdrawn: Subject 20s genetic design incorporates human DNA. Furthermore, its method of killing is most horrifying: this beast injects its prey with the Proto B DNA strain which then proceeds to remove all genetic differences within specific cells. The result is a terrifying death as the victims living body slowly erodes into gelatinous pile of pure protein which subject 20 then uses for food. After its final mutation, where the creature evolves into a huge insect-like being with a large mouth full of sharp teeth, the creature is eventually slain when it eats Cals cancer-ridden liver, its body genetically self-destructing from within. Mike and Tracy are the only survivors.

==DVD and Blu-ray Disc==
On July 20, 2010, Shout! Factory released Forbidden World on both DVD and Blu-ray Disc. The DVD is a 2-disc set. This set also includes the original Allan Holzman cut that was rejected by Roger Corman due to having humor, while Corman wanted the film to be done as a straight sci-fi/horror film. This is the first time this cut is available anywhere.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 