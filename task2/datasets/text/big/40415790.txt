An Australian by Marriage
 
 
{{Infobox film
| name           = An Australian by Marriage
| image          = 
| image_size     =
| caption        = 
| director       = Raymond Longford
| producer       = Raymond Longford
| writer         = 
| based on       = 
| narrator       =
| starring       = Lottie Lyell
| music          =
| cinematography = 
| editing        = 
| studio    = 
| distributor    = 
| released       = 1923
| runtime        = 
| country        = Australia
| language       = Silent filmintertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
An Australian by Marriage is a 1923 Australian dramatised documentary directed by Raymond Longford. It was made at the behest of the Australian government for the British Exhibition in order to attract migrants to Australia. 
==Plot==
Isobell (Lottie Lyell) emigrates from England to Australia after getting engaged to an Australian. She is met by the Y.W.C.A. on arrival and secures a position as a nursemaid. Her fiancee wears clothes made from Australian wool and she goes shopping.  
==Cast==
*Lottie Lyell as Isobell
==References==
 
 

 
 
 

 