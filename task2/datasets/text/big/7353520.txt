Midnight Sun (2006 film)
{{Infobox film name     = Taiyō no Uta image    = Yui - Taiyou no Uta.jpg img_capt = Taiyou no Uta Standard Edition director = Norihiro Koizumi writer   = Kenji Bandō caption  = starring = Yui Takashi Tsukamoto Kuniko Asagi Goro Kishitani Sogen Tanaka Airi Toyama music    = Yui distributor = Shochiku Co.,Ltd. (Japan) released = June 17, 2006 (Japan) runtime  = 119 min. language = Japanese
|}} Japanese artist and singer Yui (singer)|Yui. In the movie, she plays the role of Kaoru Amane (雨音 薫 Amane Kaoru), a 16 year old girl who has the rare skin condition xeroderma pigmentosum (XP), a disease that makes the ultraviolet radiation of sunlight potentially lethal to her. Yuis character is partly based on herself, as she is a singer and guitarist, and she performs three of Yuis songs in the movie; "Its Happy Line", "Good-bye Days" and "Skyline". There has also been a Japanese TV drama based on the movie, starring Takayuki Yamada and Erika Sawajiri, along with a manga by Bandō Kenji also based on the movie by Minatsuki Tsunami.

==Plot==
 
Kaoru has Xeroderma Pigmentosum (XP), and is not allowed to be exposed to sunlight. She sleeps during the day and is active at night. She busks every night in front of a station  playing guitar. Outside her bedroom window, she spots a high school boy with a surfboard. She watches him and his friends visit the ocean every morning, before going to sleep. One day, she introduces herself to him without letting him know about her illness. He is Kōji Fujishiro (Takashi Tsukamoto) When her cousin drags her home, they sit by her window while they watch Kōji meet his friends. Kaoru explains everything, and her cousin notes that she probably goes to the same school as him, and offers to spy on him for her.

 

The next evening she sits by the bus stop.  Kōji arrives on his scooter (motorcycle)|scooter. Both embarrassed, they start talking and Kōji eventually promises to meet her and listen to her sing another night, at the start of the school holidays. When they meet up, another obnoxious street performer has taken her spot. Kōji decides to take her to the city, where after seeing the sights, she starts playing in a square. (YUIs music video Skyline features this event).  A substantial crowd gathers to hear her sing. Afterwards they watch the ocean, and Kōji asks her out.

Their date, however, ends abruptly as the sun breaks out and Kaoru flees home. Kōji is soon informed of Kaorus condition and is taken aback. For a while, Kaoru stubbornly refuses to see him. Kōji learns of a recording studio where Kaoru could record her debut single, and takes up small jobs to earn the money to pay for it. Her father, out of concern, invites Kōji over one night. At dinner, Kōji reveals his plans for Kaorus CD. As they walk home that night, the two begin to talk and Kaoru slowly realizes how much Kōji truly cares for her.

 

As her disease worsens, she loses feeling in her hands and is unable to play guitar. She assures Kōji that she still has her voice.

In the studio, she asks her family and friends to leave. She asks them to wait for the CD.

Time passes. As promised, Kōji brings Kaoru to the beach to watch him surf. The protective suit she had left hanging for years is finally used. By now she is in a wheelchair. She complains that the suit is getting hot. With a painful expression that fades quickly, Kaorus father tries to convince her that if she takes off the suit, it cant bother her anymore, that she could run around freely. She declines. With that, she struggles to stand up and limps weakly toward Kōji. As she walks, she trips over the sand and Kōji rushes to catch her. She catches herself at the last minute, revealing that it was a feint and giggles at his surprised face.

Finally Kaoru is laid to rest in a coffin full of sunflowers. Kōji, Kaorus friends and family listen as Kaorus CD is finally released. In the final scene, Kōji rushes towards the waves, his mind replaying her voice.

==Cast==
* Yui (singer) - Kaoru Amane
* Takashi Tsukamoto - Koji Fujishiro
* Kuniko Asagi - Yuki Amane (Kaorus mother)
* Goro Kishitani - Ken Amane (Kaorus father)
* Airi Toriyama - Misaki Matsumae (Kaorus best friend)
* Eri Fuse
* Gaku Hamada
* Takashi Kobayashi Magy
* Sogen Tanaka - Haruo Kato

==Details==
{{listen
 | filename     = Goodbye Days - YUI.ogg
 | title        = "Good-bye Days" (2006)
 | description  = Taiyō no uta Theme Song, "Good-bye Days"
 | format       = Ogg
}}
* Title: タイヨウのうた
* Title (romaji): Taiyō no Uta
* Also known as: A Song of the Sun (The Suns Song), Midnight Sun, A Song to the Sun
* Genre: Romance, Human Drama
* Theme song: Good-bye Days by Yui (singer)|Yui. Skyline by Yui (singer)|Yui.
* Insert song: Good-bye Days|Its happy line by Yui (singer)|Yui.

==DVD release==
There are two Japanese DVD releases of Taiyō no uta, a standard and a premium edition. The premium edition includes a bonus disc with nearly 90 minutes of making-of footage, deleted scenes, the music video for the song "Good-bye Days", and interviews with Yui, Takashi Tsukamoto and the films director. It also has special features of Yuis Diary and a pick pendant with a necklace. Only making of Taiyō no Uta was released on June 2, 2006. Both standard edition and premium edition were released on June 11, 2006. Midnight Sun has also been released on DVD in Hong Kong, Taiwan and Singapore. The films original soundtrack is available on CD.

==External links==
*   
*  at the Internet Movie Database
* 
;Singapore
*http://www.scorpioeast.com.sg/movie.asp?pdttype=Movies&pdtsubtype=Others
;Hong Kong
*http://www.midnightsun-movie.com/
;Taiwan
* http://www.grouppower.com.tw/midnightsun/
* http://wmt.hilive.tv/grouppower/midnightsun.wmv

 
 
 
 
 
 
 