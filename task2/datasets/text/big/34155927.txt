Love Fiction
{{Infobox film
| name           = Love Fiction
| image          = Love Fiction-poster.jpg
| caption        = Promotional poster for Love Fiction 
| film name      = {{Film name
| hangul         = 러브 픽션
| rr             = Leobeu Piksieon
| mr             = Rŏbŭ P‘iksyŏn}}
| director       = Jeon Kye-soo
| producer       = Shin Young-il Eom Yong-hoon Na Byeong-joon
| writer         = Jeon Kye-soo
| starring       = Ha Jung-woo Gong Hyo-jin
| music          = Kim Dong-ki
| cinematography = Kim Yeong-min
| editing        = Kim Hyeong-joo Fantagio Pictures
| distributor    = Next Entertainment World  (South Korea)  Finecut  (international)  
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =     ( ) 
}}
Love Fiction ( ) is a 2012 South Korean romantic comedy film written and directed by Jeon Kye-soo, and starring Ha Jung-woo and Gong Hyo-jin.     

== Plot == European film serial with a main character loosely based on Hee-jin. However, with this newfound popularity he begins to discover more than he would like to know about his girlfriend’s complicated history with men.  

== Cast ==
* Ha Jung-woo - Goo Joo-wol / Detective Ma Dong-wook
* Gong Hyo-jin - Lee Hee-jin / Kim Hae-young
* Jo Hee-bong - Publisher Kwak / Detective squad chief Kwak
* Lee Byung-joon - M
* Ji Jin-hee - Goo Joo-ro, Joo-wols older brother
* Yoo In-na - Soo-jung / Kyung-sook
* Kim Ji-hoon - Hwang / Detective Ryu
* Seo Hyun-woo - Yi-gyu
* Choi Doo-ri - Kyung-ja 
* Kwak Do-won - Director Hwang / Murdered senator
* Kim Seong-gi - Dr. Pyo
* Choi Yoo-hwa - Min-ji / Veronica
* Park Young-soo - Young-shik / Director Jo
* Jo Yong-joon - Sysop
* Lee Joon-hyuk - Professor Jeong
* Kim Hye-hwa - Ma-yi
* Kim Jae-hwa - Joo-hee
* Park Joon-myun - Baek Sun-young
* Choi Won-tae - Baek Seon-il 
* Kang Shin-cheol - Hee-jins ex-husband
* Son Byung-wook - Joo-wols high school gym coach
* Jeon Soo-ji - art teacher
* Kim Hye-ji - beauty salon hairdresser
* Chi Woo - Joo-wol as a high school teen
* Yeom Hyun-seo - little girl Ye-ja

== Production == Baeksang award omnibus If indie Lost and Found.

With a background in stage and dance, Jeon said he was inspired by French director Jean-Pierre Jeunet and his 1991 film Delicatessen (film)|Delicatessen to become a director.   

The script was completed in 2007 but Jeon couldn’t find anyone to invest in the unconventional romantic comedy, largely because industry insiders considered the plot to be too difficult for the general public to understand. The script was written for Ha Jung-woo and he committed to the film from the beginning, but Jeon said, "We had this good actor Ha Jung-woo but investors changed their minds because they thought the script lacked widespread appeal and the public wouldn’t like or understand it." 
 The Crucible.) 

Filming began in 2011, with Gong Hyo-jin replacing Kang Hye-jung as the leading lady.     Jeon said of Ha and Gong, "They were perfect in many ways. They gave us their best in just a couple of takes. I couldn’t have asked for more than that." 

Explaining the puzzling name of Goo Joo-wol, Jeon said "I find the (names) sound important. It has that sound of a character that steps out the door of his house at around 2 p.m. in his pajamas, loitering about without doing much, like a neighborhood rogue complaining about society."
 Auberge Espagnole,  Joo-wols complex inner side is displayed through conversations shared with an imaginary character M.  In making Joo-wol the focus, the film shows how men beg for love and quickly lose interest in their partners once they are stuck in a relationship.   

Touted as a "Male Bridget Jones" and a "Korean 500 Days of Summer", the insightful love story tracks the hero’s bumbling journey through modern dating, which turns out to be a lot harder than he thought. "The film is hopefully everything that a man can experience in love," said Jeon, "in two hours" (of running time). 

== Release ==

=== Box office ===
Ticket sales surpassed expectations, reaching 1 million viewers in only 5 days of release, and breaking even on the 8th day.  It was the 16th most-watched Korean film in 2012, at 1,726,202 admissions.  

=== Critical reception ===
The film is unique in that it unfolds 100% from Joo-wol’s perspective, thus Jeon anticipated some negative feedback from women, saying Gong did have several problems with the story,  but the film is his "response to those concerns." Several female moviegoers who attended advance screenings expressed their discomfort after watching it, flooding the blogosphere with their voices against the quasi-universal praise from the press. Jeon said, "Reality can be ragged, pathetic and desperate," adding that he expected the audience to ask why they have to watch that sad truth on the silver screen instead of a fairy tale with a prince charming. Whereas most Korean films present a more positive and less nuanced picture of love, the film highlights the challenges in romantic relationships. The Korea Times describes the film as "a perplexing tribute to love, a brutally honest portrayal of its progressive steps — from courtship to fizzling out. It is a satire of the bachelor social ladder, on top of which sit three-piece suits and white gowns while the rock bottom is occupied by poor artists. It is a story of a writers block, which the protagonist hopes to overcome with a muse. It is a story of the man in its unpasteurized form: that needy, selfish and affection-seeking part of the human male species taking shape as the protagonist." 

=== Awards and nominations === 48th Baeksang Arts Awards
* Best Screenplay - Jeon Kye-soo
* Nomination - Best New Director - Jeon Kye-soo
 33rd Blue Dragon Film Awards
* Popular Star Award - Gong Hyo-jin
* Nomination - Best Actress - Gong Hyo-jin

== Soundtrack ==
{| class="wikitable"
|-
!Album information Tracklisting
|-
|Love Fiction OST 
*Released: February 27, 2012
*Label: Mirrorball Music  
|style="font-size: 85%;"|{{hidden|Tracklisting|
# 러브픽션 (Love fiction) - Park Ju-won
# 방울토마토 (Cherry tomatoes) - Kim Ji-hoon
# 하이파 여인 (Hyper woman) - Park Ju-won
# For Suicide - Kim Dong-ki
# Chanson De Ma Vie - Jeon Yoo-jin
# 구애서한 (求愛書翰) (Courtship letter) - Kim Dong-ki
# True Love - 서교동꽃거지, Wild Dingo
# Romantic Night - Kim Dong-ki
# Romantist - Lee Tae-kyung
# 바람에 기대어 (Leaning against the wind) - Jeon Yoo-jin
# Sweet Amore - Park Ju-won
# Detective Ma - Kim Dong-ki
# I Wanna Know You - Kim Ji-hoon
# Failure - Wild Dingo
# A Detectives Feeler - Kim Dong-ki
# Mephistoculas - Kim Dong-ki
# Laid Back - Jeon Yoo-jin
# Inside of Me - Taru
# Contrast - Park Ju-won
# Know (Letter to Alaska) - Jeon Yoo-jin
# 알라스카 (Alaska) - Ha Jung-woo, Kim Ji-hoon, Seo Hyun-woo, Choi Doo-ri
# 또 다른 알라스카 (Another Alaska) - Sang-woon}}
|-
|}

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 
 