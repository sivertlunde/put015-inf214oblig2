K-11 (film)
 
{{Infobox film
| name = K-11
| image = 
| image_size = 
| alt = 
| caption = 
| director = Jules Stewart
| producer = Tom Wright, Jr.
| writer = Jules Stewart Jared Kurt
| starring = Goran Višnjić Kate del Castillo D.B. Sweeney
| music = Phillip Marshall
| cinematography = Adam Silver
| editing = Duwayne Dunham
| studio = Libertine Films
| distributor = Breaking Glass Pictures
| released =  
| runtime = 88 minutes
| country = United States
| language = English
}}
K-11 is a 2012 American drama film co-written (with Jared Kurt) and directed by Jules Stewart. The film stars an ensemble cast of generally independent film actors including Goran Višnjić, Portia Doubleday, Jason Mewes, and Sonya Eddy. The film also includes Chiodos vocalist Craig Owens.

The term "K-11" refers to a dormitory section of the Los Angeles jail used to hold gay and transsexual inmates. 

==Plot==
Raymond Saxx Jr. is a powerful record producer who wakes from a drug-induced blackout to find himself locked up and classified "K-11". Plunged into a nightmarish world ruled by a transsexual diva named Mousey, Raymond is truly a fish out of water. Complicating matters are a troubled but kind young transgender girl named Butterfly, a predatory child molester named Detroit, and the ruthless Sheriffs Deputy, Lt. Johnson. Rays struggle to contact the outside world and regain his freedom seems impossible, but he must learn to navigate this new power structure if he is ever going to survive and be in control of his life again.

The movie starts with Saxx in an LA prison, so high on drugs that he cannot be questioned. However, a prison guard seems to take a special interest in him, bribes a colleague and puts him in a holding cell for cellblock K-11. With him is a young transsexual who calls herself "Butterfly". After one night, Saxx is put in block K-11, though Butterfly is to remain in the holding cell overnight. Among others, he meets the self-proclaimed boss Mousey and child molester Detroit. At first, the inmates frighten and disgust him, but he gradually begins to enjoy their company. There is a great trade in   and the murder of Detroit before being taken into custody and put in a cell next to Ben, who implies will have Johnson raped. Saxx deposits $13,000 on Mouseys account before being released and getting rid of the drug stash in his car.

==Cast==
* Goran Višnjić as Raymond Saxx, Jr.
* Kate del Castillo as Mousey
* D.B. Sweeney as Lt. Johnson
* Portia Doubleday as Butterfly
* Jason Mewes as Ben Shapiro
* Tommy "Tiny" Lister Jr. as Detroit
* Sonya Eddy as Teresa Luna
* Luis Moncada as ShyBoy
* Craig Owens as Ian Sheffield
* Tiffany Mulheron as Tia Saxx
* P. J. Byrne as C.R.
* Paul Zies as Washington
* Tara Buck as Crystal
* Lou Beatty, Jr. as Granny
* Billy Morrison as Hollywood
* Ralph Cole, Jr. as Kay-Kay
* Markus Redmond as Precious
* Cameron B. Stewart as Sledgehammer
* Franc Ross as Wino Tim De Zarn as Sgt. Cowboy WIlliams
* Michael Shamus Wiles as Captain Davis
* John Prosky as Simon Schwartz

==Production==
K-11 was announced in November 2008 with Jules Stewart to write and direct and Kristen Stewart, Jules daughter, and her The Twilight Saga (film series)|Twilight co-star Nikki Reed in roles,  but both later dropped out due to scheduling conflicts with the Twilight films. When Kristen and Nikki dropped out in July 2011, her brother, Cameron, was given a role. 

==Release== premiered at the Turin Film Festival on November 16, 2012 and was released in the United States on March 15, 2013.

==Reception== John Waters remake of The Shawshank Redemption." It was panned by movie critics and received an 8% " rotten" rating from Rotten Tomatoes.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 