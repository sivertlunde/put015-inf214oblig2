The Forbidden Quest
{{Infobox Film
| name           = The Forbidden Quest
| image          = Poster of the movie "The Forbidden Quest".jpg
| image_size     = 
| caption        = 
| director       = Peter Delpeut
| writer         = Peter Delpeut Roy Ward
| music          =
| cinematography =
| editing        = Peter Delpeut
| distributor    = Ariel Film Zeitgeist Films
| released       = 1993
| runtime        = 75 min.
| country        =  Netherlands English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Forbidden Quest is a 1993 mockumentary written and directed by Peter Delpeut. 

The film won the 1994 International Fantasy Film Special Jury Award at the Fantasporto (aka Festival Internacional de Cinema do Porto) in Portugal.  
The film won the 1993 Special Jury Prize at the Nederlands Film Festival (aka Nederlandse Filmdagen) It was released in Netherlands theaters on April 8, 1993.

==Plot==
In 1931, a documentary filmmaker hears of  J.C. Sullivan who may know the fate of the Hollandia, a Norwegian ship that sailed to Antarctica in 1905 and disappeared. J.C. Sullivan was the carpenter on that ill-fated voyage and is the last known surviving crewmember of the Hollandia.  The filmmaker interviews Sullivan who is also able to supply him with canisters of old film footage which back up the unbelievable accounts that Sullivan describes. 

The film, made in 1993, is presented as a 1931 documentary of a series of events that occurred in 1905.  The footage of the fictional expedition is from other polar expeditions of the time.  These clips are interspersed with the interview of J.C. Sullivan.

==Cast==
* Joseph OConor as J.C. Sullivan Roy Ward as the Interviewer/filmmaker

==Reception==
The New York Times Janet Maslin praised the "honest power of the films archival scenes" while condemning its narrative as slow-paced, portentous, and poorly written.   

==Notes==
 

==External links==
*  

 
 
 
 
 