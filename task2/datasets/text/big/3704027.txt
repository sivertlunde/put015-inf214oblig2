Miss Julie (1999 film)
{{Infobox film
| name           = Miss Julie
| image          = MissJulie.jpg
| image_size     =
| caption        =
| director       = Mike Figgis
| producer       = Harriet Cruickshank Mike Figgis
| writer         = Screenplay: Helen Cooper Play: August Strindberg
| narrator       =
| starring       = Saffron Burrows Peter Mullan
| music          = Mike Figgis
| cinematography = Benoît Delhomme
| editing        = Matthew Wood
| studio         =
| distributor    = Metro-Goldwyn-Mayer|MGM/UA
| released       =  
| runtime        = 103 minutes
| country        = United States United Kingdom
| language       = English
| budget         =
}}
 play of the same name by August Strindberg, starring Saffron Burrows in the role of Miss Julie and Peter Mullan in the role of Jean.

==Plot==
Midsummer night, 1894, in northern Sweden. The complex structures of class bind a man and a woman. Miss Julie, the inexperienced but imperious daughter of the manor, deigns to dance at the servants party. Shes also drawn to Jean, a footman who has traveled, speaks well, and doesnt kowtow. He is engaged to Christine, a servant, and while she sleeps, Jean and Miss Julie talk through the night in the kitchen. For part of the night its a power struggle, for part its the baring of souls, and by dawn, they want to break the chains of class and leave Sweden together. When Christine wakes and goes off to church, Jean and Miss Julie have their own decisions to make. 

==Cast==
* Saffron Burrows as Miss Julie
* Peter Mullan as Jean
* Maria Doyle Kennedy as Christine
* Tam Dean Burn as servant
* Heathcote Williams as servant
* Joanna Page as servant

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 

 