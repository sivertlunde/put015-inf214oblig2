Mazhavilkavadi
{{Infobox film
| name           = Mazhavilkavadi
| image          = Mazhavilkavadi.jpg
| image size     = 
| alt            = 
| caption        = VCD Cover
| director       = Sathyan Anthikkad
| producer       = Siyad Koker
| writer         = Raghunath Paleri Urvashi Sreeja Sithara Innocent Innocent Mamukkoya Krishnan Kutty Nair Oduvil Unnikrishnan Johnson Kaithapram Damodaran Namboothiri (lyrics)
| cinematography = Vipin Mohan
| editing        = K. Rajagopal
| studio         = Kokers Films
| distributor    = 
| released       = 1989
| runtime        = 130 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Krishnan Kutty Nair, and Mamukkoya.
 Best Actress Best Supporting Best Music Best Singer (K. S. Chithra).

The film was remade in Tamil as Subramaniya Swamy with Pandiarajan.

==Cast==
* Jayaram  as Velayudhankutty Urvashi as Anandavalli
* Sreeja as Vilasini Sithara as Amminikutty Innocent as Shankarankutty Menon Krishnan Kutty Nair as Kaleeswaran Kavalayil Kali Muthu
*Meenakumari as Nangeli
* Kaviyoor Ponnamma as Velayudhankuttys Mother
* Paravoor Bharathan as Vasu
* Philomina  as Velayudhankuttys Grandmother
* Oduvil Unnikrishnan as Kunjappu
* Bobby Kottarakkara as Murukan
* Karamana Janardanan Nair as Nanukuttan
* Mammukkoya as Kunjikhater Jagannathan as Ubaid
* Sankaradi as Kuriya Varkey
*Valsala Menon as Bhairavi

==Soundtrack==
{| class="wikitable"
|-
! Song !! Singer
|-
| Mainakaponmudiyil || G. Venugopal, Chorus
|-
| Pallitherundo || G. Venugopal, Sujatha
|-
| Thankathoni || K. S. Chithra
|}

==Awards==
; Kerala State Film Awards   Best Actress Urvashi
* Best Supporting Innocent
* Best Music Johnson
* Best Singer - K. S. Chithra

==References==
 

==External links==
*  
*   at the Malayalam Movie Database
*   at Metromatinee.com
*   at Oneindia.in

 
 
 
 
 
 
 

 
 