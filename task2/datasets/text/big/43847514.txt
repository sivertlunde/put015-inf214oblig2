Heroes & Zeros (film)
{{Infobox film
| name               = Heroes and Zeros
| image              = Heroes and Zeroes poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Niji Akanni
| producer           =  Koga Studios
| screenplay        = 
| writer         = Niji Akanni
| starring       =  
| music          = 
| costume          = Shakara Clothing
| cinematography =  Alfred Chia
| editing        = Abimbola Taiwo
| studio         = Koga Studios
| distributor   = 
| released       =   
| runtime        = 
| country        = Nigeria
| language       = English
| budget         = 
| gross          =
}}
 Best Director, Best Screenplay Best Editing.   

The film tells a story about the relationship between a 45 yr old married film director, Amos (Bimbo Manuel) and a film actress, Tonia (Nadia Buari). Amos, who is having troubles in his marriage seeks to be an international soccer star in the nearest future. His wife, Tinuke (Tina Mba) works in a financial institution and is having difficulty coping with the death of her only child. Amos is opportuned to be a part of a major big budget film  to revive his career. He eventually uses it re-establish himself in the film industry. He is greatly obsessed by Tonia and begins extra-marital affairs with her to the detriment of his wife.

==Cast==
*Nadia Buari as Tonia
*Olu Jacobs
*Norbert Young as Nnamdi
*Tina Mba as Tinuke
*Gabriel Afolayan
*Akin Lewis
*Bimbo Manuel as Amos
*Linda Ejiofor as Bisola

==Release==
It was premiered in Lagos on August 31, 2012.    The American premiere was held shortly afterwards.  The film was released in theaters nationwide on September 7, 2012.    In 2013, it premiered in the UK on March 15 at Odeon Cinema. 

==Accolades==
{| class="wikitable sortable" style="width:100%"
|+List of Awards
|-
!  Award !! Category !! Recipients and nominees !! Result
|-   Africa Movie Africa Film Academy   (9th Africa Movie Academy Awards)    Best Editing
| Abimbola Taiwo
|  
|- Best Screenplay
| Niji Akanni
|  
|- Best Actor
| Bimbo Manuel
|  
|- Best Director
| Niji Akanni
|  
|- Best Nigerian Film
| Niji Akanni
|  
|- Best Sound
| Mohammed Musulumi
|  
|-
|}

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 