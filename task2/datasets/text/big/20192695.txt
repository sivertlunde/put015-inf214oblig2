The Crocodile (film)
 
{{Infobox Film
| name           = The Crocodile
| image          = Crofilm.jpg
| caption        = Khmer promtional Poster
| director       = Mao Ayom
| producer       = Mao Ayom
| writer         = Mao Ayom
| narrator       = 
| starring       = Dy Saveth Preap Sovath Sam Solika
| music          = 
| cinematography = 
| editing        = 	 	
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Cambodia
| language       = Khmer
| budget         = 100,000$
| preceded_by    = 
| followed_by    = 
}} Cambodian award-winning Horror action film based on the crocodiles influence in Khmer culture. The film was released twice. It was originally released in July 2005 in more than 6 theatres and then screened again in 21–25 November 2007 at the Chenla Theatre  after its showing around all Cinema of Cambodia|Cambodia. It is unreleasable on VCD or DVD for a short amount of time.

== Plot ==
There once was a river where so many crocodiles roamed that it was far too dangerous for navigation or use. Many people from the surrounding villages had lost at least one of their relatives and loved ones to crocodile attacks. Among them, San, who changed his profession from being an ordinary farmer to a crocodile hunter, was no different. He wouldn’t have lost his dearest wife, relatives, neighbours, best friend, and nearly his own life if the notorious crocodiles had not been around. Ever since, he was determined that as long as he was alive, he had to put those crocodiles to death.

== Production and Release ==
The Film cost more than US$100,000, making it perhaps the most expensive Cambodian  Khmer most popular single, Preap Sovaths high price for this acting. 
No need to reveal of Box office failed to the film, With high standard producing the film, More than 100,000 people flocked to theaters in Phnom Penh for its one month season in October as well as receive several awards especially the best film in The Khmer national film celebration. 

== Awards ==
* Best Movie (First Prize)  
* Best Scenario
* Best Visual effects
* Best Director
* Best writer
* Best Actor
* Best Young Actor
* Best Creation

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 