Doraemon: Nobita and the Island of Miracles—Animal Adventure
{{Infobox film
| name           = Doraemon: Nobita and the Island of Miracles ~Animal Adventure~
| image          = Doraemon- Nobita and the Island of Miracles movie poster.jpg
| caption        = theatrical poster
| director       = Kôzô Kusuba
| producer       = Atsuishi Saitou Momoko Kawawkita Reina Takahashi Rika Tsuruzaki Shunsuke Okura Takumi Fujimori
| writer         = Higashi Shimizu Chiaki
| music          = Kan Sawada
| cinematography = Katsuyoshi Kishi
| studio         = Shin-Ei Animation
| editing        =
| distributor    = Toho
| released       =  
| runtime        = 99 minutes
| country        = Japan
| language       = Japanese
| gross          = $47,649,222 / 5.1 billion yen   
}}
  is a 2012 Japanese animated film based on the popular manga and anime series Doraemon. The film was released in Japan on March 3, 2012. A video game based on this film was released for Nintendo 3DS on March 1, 2012.   

==Plot==
Nobita catches a big rhinoceros beetle, which he promises to his dad to take good care of it. Later, Doraemons group finds a strange island where a tribe co-exists with prehistoric creatures long believed to extinct yet preserved by a mythical beetle spirit named Golden Hercules. However, the group has to deal with a group of criminals from the future who intend to capture Golden Hercules for a profit.

==Cast==
{| class = "wikitable"
! Character !! Japanese voice actor
|-
| Doraemon || Wasabi Mizuta
|-
| Nobita || Megumi Ohara
|-
| Shizuka || Yumi Kakazu
|-
| Gian || Subaru Kimura
|-
| Suneo || Tomokazu Seki
|-
| Koron || Nana Mizuki
|-
| Nobisuke || Masako Nozawa
|- Atsuko Tanaka
|- Chiaki
|-
| Nobitas Mom || Kotono Mitsuishi
|-
| Nobitas Dad || Yasunori Matsumoto
|-
| Sharman || Kouichi Yamadera
|-
|}

==Extinct animals featured==
* Ceratogaulus
* Chalicotherium
* Dodo
* Elasmotherium
* Gastornis
* Glyptodon
* Machairodus
* Megatherium
* Moa
* Paraceratherium

==Reception==
This movie gross $44,649,222  and was ranked 10th highest-grossing film of 2012 in Japan.

==Television broadcast==
The movie was released in India 24 May 2013 to theaters and aired on 29 June 2013 on Disney Channel India and on 1 October 2013 on Hungama TV as Doraemon The Movie Nobita Aur Jadooi Tapu. 

==See also==
* Fujiko Fujio

==References==
 

==External links==
*  
*    
*  

 
 

 
 
 
 
 


 