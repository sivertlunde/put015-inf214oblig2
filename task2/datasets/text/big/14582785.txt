Donald's Dilemma
{{Infobox Hollywood cartoon|
|cartoon_name=Donalds Dilemma
|series=Donald Duck Jack King Roy Williams Don Towsley Ed Aardal Emery Hawkins Sandy Strother
|layout_artist=Don Griffith
|background_artist=Maurice Greenberg
|voice_actor=Gloria Blondell Clarence Nash (both uncredited)
|musician=Oliver Wallace
|producer=Walt Disney  Walt Disney Studios
|distributor=RKO Pictures
|release_date=July 11, 1947
|color_process=Technicolor 
|runtime=7:16 minutes English
|preceded_by=Clown of the Jungle
|followed_by=Crazy with the Heat
}}

Donalds Dilemma is a 1947   and Daisy Duck. It was originally released on July 11, 1947 in the United States.  Although Donald is the official headliner for this cartoon, Daisy is the actual protagonist.  The dilemma of the title is actually offered to her, not to Donald. Reid (2007), p. 60-61 

==Synopsis== flashback scenes. spring day censored scene features her losing her will to live and pointing a gun at her head   She decided that she would see Donald once again, at any cost, but failed to do so. Thats when she decided to go to the psychologist - and the flashback meets the actual time of the cartoon.

At the end of the cartoon, the psychologist determines that Donald would regain his memory of Daisy if another flower pot (with the same flower from the first pot, which Daisy kept as the only thing she had to remember Donald) would fall on his head. But he warns that his improved voice may be lost along with his singing career. He offers Daisy a dilemma. Either the world has its singer but Daisy loses him or Daisy regains Donald but the world loses him. Posed with the question "her or the world", Daisy answers with a resounding and possessive scream - "Me! Me! Me! MEEE!!". Soon, Donald returns to his old self and forgets about his singing career and Daisy regains her lover.

==Analysis==
Daisy is the protagonist for once. She reportedly displays "a ruthlessly self-centered neurotic streak".  Reid (2007), p. 60-61  She maintains the audience as sympathy throughout the film.  Reid (2007), p. 60-61 

The film is unusual for a Disney short in featuring a definite story with a premise, plot development, and a resolution, not simply a series of gags. The central idea is cleverly developed and brilliantly characterized.  Reid (2007), p. 60-61 

Jack King was typically stolid as a director, but this film is an unusually atmospheric entry in his filmography.  Reid (2007), p. 60-61 

==Releases==
*1947 &ndash; Theatrical release Walt Disneys Wonderful World of Color, episode #8.6: "Inside Donald Duck" (TV)
*c. 1983 &ndash; Good Morning, Mickey!, episode #42 (TV)
*1984 &ndash; "Cartoon Classics - Limited Gold Edition: Daisy" (VHS)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #75 (TV) The Ink and Paint Club, episode #1.40: "Crazy Over Daisy" (TV)
*2005 &ndash; "Classic Cartoon Favorites: Extreme Music Fun" (DVD)
*2007 &ndash; " " (DVD)

==Censorship==
Most TV versions have edited out the "I couldnt eat, I didnt want to live" part to remove the shot that shows a suicidal Daisy pointing a gun at her head, though the scene has been shown intact on the VHS and DVD editions. 

==Sources==
*  

==References==
 

==External links==
*  at the Internet Movie Database
* 
*  entry at  
 
 
 
 
 
 