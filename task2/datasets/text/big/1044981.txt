The Idiots
 
 
{{Infobox film
| name           = The Idiots
| image          = The Idiots theatrical poster.jpg
| alt            = 
| caption        = Danish release poster
| director       = Lars von Trier
| producer       = Vibeke Windeløv
| writer         = Lars von Trier
| starring       = {{Plainlist|
* Bodil Jørgensen
* Jens Albinus}}
| cinematography = Lars von Trier
| editing        = Molly Malene Stensgaard
| studio         = {{Plainlist|
* Arte
* Canal+
* Danmarks Radio Nordisk Film- & TV-Fond
* October Films Zentropa Entertainments2 ApS
* Zweites Deutsches Fernsehen}}
| distributor    = Scanbox Entertainment
| released       =  
| runtime        = 114 minutes  
| country        = {{Plainlist|
* Denmark
* France
* Italy
* Netherlands
* Spain
* Sweden}}
| language       = Danish
| budget         = United States dollar|USD$2.5 million 
| gross          = $7,235 
}} Dogme 95 Manifesto, and is also known as Dogme #2. It is the second film in von Triers Golden Heart Trilogy, which includes Breaking the Waves (1996) and Dancer in the Dark (2000). It is among the first films to be shot entirely with digital cameras.

==Plot== developmentally disabled. The Idiots is not concerned with actual disability, or with distinguishing between mental retardation and physical impairment.

At a restaurant, patrons are disturbed by the groups mischief, but single diner Karen develops an appreciation of their antics. The members of the group refer to this behaviour as "spassing", a neologism derived from "spasser", the Danish equivalent of "Spastic|spaz" and an offensive slur. Karen takes a ride in a taxi cab with the people from the restaurant, and she finds herself at a big house. The apparent leader of the group, Stoffer, is supposed to be selling the house (which belongs to his uncle), but instead it becomes the focal point for group activities.

The "spassing" is a self-defeating attempt by the group to challenge the establishment through provocation. The self-styled idiots feel that the society-at-large treats their intelligence uncreatively and unchallengingly; thus, they seek the uninhibited self-expression that they imagine a romantic ideal of disability will allow.

Stoffer, at his birthday party, wishes for a "Group sex|gangbang", and both clothes and inhibitions are soon discarded. But when Stoffer calls for the group members to let idiocy invade their personal daily lives, only Karen takes up the challenge. She takes Susanne back to her house, where they are greeted by surprise by Karens mother. Karen had been missing for two weeks, following the death of her young baby; she offers no explanation of where she has been. Karen attempts to spaz in front of her family by dribbling the food she is eating, but this results in a violent slap from her husband, Anders. Karen and Susanne leave the house.

==Cast==
 
* Bodil Jørgensen as Karen
* Jens Albinus as Stoffer
* Anne Louise Hassing as Susanne
* Troels Lyby as Henrik
* Nikolaj Lie Kaas as Jeppe
* Louise Mieritz as Josephine
* Henrik Prip as Ped
* Luis Mesonero as Miguel
* Knud Romer Jørgensen as Axel
* Trine Michelsen as Nana
* Anne-Grethe Bjarup Riis as Katrine
* Paprika Steen as High-class lady
* Erik Wedersøe as Stoffers uncle
* Michael Moritzen as Man From Municipality
* Anders Hove as Josephines father
* Lars von Trier (uncredited voice) as Interviewer
 

==Production== the Netherlands, Cinema of Spain|Spain, and Cinema of Sweden|Sweden.

===Breaches of Dogme 95 rules===
The confession of a Dogme 95 film is an idea adapted by Thomas Vinterberg in the first Dogme 95 film: Make a confession if there were things happening on the shoot which are not in accordance with the strict interpretation of the Dogme 95 rules. It is written from the directors point of view. Accordingly, von Trier made the following confession:

 In relation to the production of Dogme 2 "The Idiots", I confess:
* To have used a stand-in in one case only (the sexual intercourse scene).
* To have organised payment of cash to the actors for shopping of accessories (food).
* To have intervened with the location (by moving sources of light – candlelight – to achieve exposure).
* To have been aware of the fact that the production had entered into an agreement of leasing a car (without the knowledge of the involved actor).

All in all, and apart from the above, I feel to have lived up to the intentions and rules of the manifesto: Dogme95.    
 diegetic music, a harmonica player was recorded during the shooting of some scenes, including the end credits, even if he is not seen onscreen. 

==Reception== screening of the film at Cannes, a spontaneous review for which he was ejected from the venue. 

Channel 4 aired the film unedited in 2005 as part of the channels "Banned" season exploring censorship and cinematic works. Viewer complaints prompted an Ofcom investigation,    which came out in favour of Channel 4.  In its ruling, Ofcom found the film "not in breach" of the relevant Code under the specific circumstances of the broadcast, that is "the serious contextualisation of the film within a season examining the censorship of film and television, its artistic purpose, the channel which transmitted it, the strong warnings before the film and prior to the scene in question and the scheduling after midnight."    Ofcom added the caveat that, "While we do not consider the film was in breach of the Code on this occasion, we must consider carefully the acceptability of any similar content on an individual basis." 
 MA rating), Chile, New Zealand, Norway, South Korea, Spain, Taiwan, and the United Kingdom. In Switzerland and Germany, the film ran uncut with a 16-years rating in theaters, followed by a DVD release with the same rating and several uncut television airings.

The Idiots was ranked #76 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010.    The magazine had previously given it a full five star rating on its release in UK cinemas. It is listed as #941 in the film reference book 1001 Movies You Must See Before You Die.

===Controversy=== penetrative (vaginal) theatrical release by the British Board of Film Classification, receiving an 18 certificate.  When it was shown on Film4 (then FilmFour) in 2000, the erection and the intercourse were obscured by pixelization, following an order from the Independent Television Commission.      

===Accolades===
The film was shown in competition at the 1998 Cannes Film Festival.
* Bodil Awards (1999)
** Won: Best Actress, Bodil Jørgensen
** Won: Best Supporting Actor, Nikolaj Lie Kaas
** Won: Best Supporting Actress, Anne Louise Hassing
** Nominated: Best Film Cannes Film Festival (1998)
** Nominated: Palme dOr   
* European Film Awards (1998)
** Nominated: European Film Award, Best Screenwriter
* London Film Festival (1998)
** Won: FIPRESCI Prize, Lars von Trier 
* Robert Festival (1999)
** Won: Best Actress, Bodil Jørgensen
* Valladolid International Film Festival (1998)
** Nominated: Golden Spike, Lars von Trier

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 