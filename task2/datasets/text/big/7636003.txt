The Zodiac (film)
{{Infobox Film
| name           = The Zodiac
| image          = Zodiacposter.jpg
| caption        = Promotional movie poster
| director       = Alexander Bulkley
| producer       = Corey Campodonico
| writer         = Kelley Bulkley Alexander Bulkley
| starring       = Justin Chambers Robin Tunney Rory Culkin Philip Baker Hall Brad Henke Marty Lindsey Rex Linn William Mapother
| music          = Michael Suby
| cinematography = Denis Maloney
| editing        = Greg Tillman
| distributor    = THINKFilm
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $1 million 
| gross          = $86,872   
}}
The Zodiac is a 2005 American criminal   who was active in and around northern California in the 1960s and who has never been captured. The Zodiac was directed by Alexander Bulkley and written by him and his brother, Kelley Bulkley. 

The film was released on March 17, 2006 into just 10 theaters on limited release  (with an R-rating by the MPAA)  and later released on DVD in North America on August 29, 2006.  The DVD hit the UK market on September 18. 

==Plot==
When two teenagers are gunned down on Lake Helena (in real-life, Herman) Road on December 20, 1968, the small town of Vallejo (Benicia) is thrown into a state of terror. Assigned to the case is Police Detective Matt Parish ( )  and 12-year-old son (Rory Culkin). 
 Zodiac strikes again. This time he guns down a couple in a deserted parking lot. Hes inches away as he pulls the trigger, but he never reveals his true identity. Just an hour after the shooting, the Vallejo Police Department receives an anonymous call, confessing to the murders that have just taken place. Days after the second murder, a letter is sent to Bay Area newspapers the San Francisco Chronicle and the San Francisco Examiner, threatening that 12 more people will die unless the three papers print the encoded letter theyve just received. The killer reveals that if they can decipher the note, his true identity will be revealed.

It becomes an obsession for Det. Parish to solve the case. He spends all his time with the coded sheet, sketches composed of the killer and various psychological reports, putting a greater strain on his family. The ever increasing publicity pushes him to the edge. When Parish receives more anonymous calls and ciphered letters (some suggesting the threatening chance that his next victim could be one of Parishs family), he thinks hes got his suspect. Disobeying orders by Chief Frank Perkins (Philip Baker Hall)  he goes in search for the killer. When he storms into the suspects house, his allegations are shattered because the man has no relation to the murders.

The police later hear of more killings, but the case doesnt get anywhere close to being resolved. On April 24, 1978, ten years after the first reported murder, the Chronicle receives another letter:

 

The film ends with the statement that the killer has not been captured.

==Cast==
* Justin Chambers as Det. Matt Parish, the main character of the story, his emotions play a large role in the development of the story and his character. He is well respected, confident, and has a good mind when it comes to catching the criminal. He tends to do things how he wants, and does not take much notice of his Chief.
* Robin Tunney as Laura Parish (wife to Matt Parish), is seen throughout the film comforting her husband. It is in these situations that we see the effects of the case on the family, and Matts true emotions are revealed. She makes every attempt to stop her son reading into the case.
* Rory Culkin as Johnny Parish (son of Matt and Laura Parish), has a marginally small role to play in the film, but hes very interested in the case, and at many times, hassles his dad for an insight into how the case is progressing.
* Shelby Alexis Irey as Bobbie, Johnnys girlfriend.
* Philip Baker Hall as Chief Frank Perkin, the Police Departments chief and is the head of the operations, at times he tries to stop Matt doing certain things, but evidently, Matt does what he wants.
* Brian Bloom as Zodiac Killer (voice), his voice is heard many times throughout the film in phone calls and letters.
* Brad Henke as Bill Gregory
* Marty Lindsey as Zodiac Killer. Throughout the whole film, the viewer will never catch a glimpse of his face, but his sense and presence is always there. He kills just four people in the film, and threatens to kill a further twelve.
* Rex Linn as Jim Martinez
* William Mapother as Dale Coverling, the news reporter who is seen throughout the film with Matt Parish when he makes statements on the cases progress.

==Production==
===Research===
The two Bulkley brothers, and producer Corey Campodonico all grew up in the San Francisco Bay Area. When they wanted to make their debut onto the filmmakers scene, and all figured it was a great story to adapt due to their history and knowledge of the location.   

In an interview with Rotten Tomatoes Alexander Bulkley said:  

With their idea in mind, Alexander and Kelly set about researching reports and documents related to the Zodiac story. They managed to find their main source of information from newspapers and media articles from around the time to fuel the project.  They finally decided upon telling the story of the detective involved on the case, and how it affected himself and family. They felt that it would be more emotional to the viewers, helping with their interaction in the story, than following the psychological path of the killer.   

They chose very distinct routes in the film, avoiding areas like the impact of the killings on the family. They say it was due to the killers interest in the murders he committed, because he was never really interested in who his victims were. 

The filmmakers main challenge was to keep the originality of the film, and to keep it authentic to the story. When questioned about this by Emanuel Levy (an online movie review site) they responded with:  

Once the screenplay was written by the Bulkley brothers, and Corey Campodonico, they gathered investors to finance the project and made it ShadowMachine Films first ever film production, which the three of them founded. 

===Filming===
The filming lasted 23 days. The Zodiac was filmed on location in Vallejo, California, where a large majority of the murders were committed. The film was finally released on March 17, 2006 in 10 movie cinemas.

==Soundtrack==
The film contained various musical compositions that accompanied different scenes from the movie. Some of the music had already been used in films such as  .   

The soundtrack contains some of the following pieces by Andy Williams, The Chambers Brothers, William S. Gilbert and Arthur Sullivan:

{| class="wikitable"
|-
! Track Name
! Artist
! Writer
! Track Length (mins)
|-
| With a Girl Like You
| The Troggs
| Reg Presley
| 02:09 
|-
| Its the Most Wonderful Time of the Year
| Andy Williams
| Edward Pola & George Wyle
| 02:47  
|-
| Papa Noel
| Brenda Lee
| Roy Botkin
| -
|-
| The Sun Whose Rays Are All Ablaze
| William S. Gilbert & Arthur Sullivan
| -
| -
|- Trouble No More
| Muddy Waters
| McKinley Morganfield
| 02:39 
|-
| Time Has Come Today
| The Chambers Brothers
| Joseph Chambers & Willie Chambers
| 11:00 
|-
|}

==Critical reception==
The film was widely panned by critics. The Rotten Tomatoes website, which compiles mostly North American reviews, showed that just seven out of 28 reviews were positive, with the average critics grade being four out of 10.   at   

The website classed the film as just 27% "rotten",  with users giving it a better rating of 58%. 

==References==
 

==External links==
*  
*  
*   at Yahoo! Movies

 

 
 
 
 
 
 
 
 
 
 
 
 