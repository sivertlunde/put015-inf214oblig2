Dead Space (film)
 

{{Infobox Film
| name           = Dead Space
| image          = Dead Space.jpg
| caption        = VHS Artwork
| director       = Fred Gallo
| producer       = Mike Elliott Roger Corman (executive producer) Jonathan Winfrey (associate producer)
 
| writer         = Catherine Cyran	
| narrator       = 
| starring       = Marc Singer Laura Tate Judith Chapman Bryan Cranston Randy Reinholz Lori Lively
| music          = Daniel May
| cinematography = Mark Parry
| editing        = Lawrence Jordan
| distributor    = Concorde Pictures
| released       = January 21, 1991
| runtime        = 72 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Dead Space is a 1991 science-fiction film involving the crew members of a space station orbiting Saturn when they face a killer virus.

In 2010 Shout! Factory released the film on DVD, packaged as a double feature with The Terror Within as part of the Roger Corman Cult Classics collection. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 


 