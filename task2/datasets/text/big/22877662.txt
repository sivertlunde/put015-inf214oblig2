Gallopin' Gals
{{Infobox Hollywood cartoon
| cartoon_name = Gallopin Gals
| series =
| image = Gals.jpg
| caption = Title card
| director = William Hanna Joseph Barbera
| story_artist = William Hanna Joseph Barbera
| animator =
| background_artist = Harlow Wilcox
| musician = Scott Bradley
| producer = Fred Quimby (unc. on original issue)
| studio = Metro-Goldwyn-Mayer cartoon studio|Metro-Goldwyn-Mayer Cartoons
| distributor = Metro-Goldwyn-Mayer
| release_date = October 26, 1940
| color_process = Technicolor
| runtime = 7 minutes
| preceded_by =
| followed_by = English
}}
 Harlow Wilcox.

==Plot== The Women (1939), except for the fact that the characters are fillies at the Kentucky Derby with New York accents, gossiping about some of the other contestants. The underdog of the story is a lonely shy horse named Maggie who has never won a race in her life and suffers from hay fever. During the race, the other horses competing are distracted by a photo finish. They fall short of the finish line in second place simultaneously while the picture is taken and Maggie crosses in front of them, winning the race.

==Censorship== A Day at the Races.

==References==
 
 

==External links==
* 

 
 
 
 
 
 
 
 