Conquest of the Planet of the Apes
{{Infobox film
| name           = Conquest of the Planet of the Apes 
| image          = conquest of the planet of the apes.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = J. Lee Thompson
| producer       = Arthur P. Jacobs 
| writer         = Paul Dehn
| based on       =   Tom Scott
| cinematography = Bruce Surtees
| editing        = Marjorie Fowler Alan L. Jaggs Don Murray Ricardo Montalbán Natalie Trundy
| studio         = APJAC Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 88 minutes 
| language       = English 
| budget         = $1.7 million    gross           = $9,700,000 
}}
 Planet of the Apes series produced by Arthur P. Jacobs.  It explores how the apes rebelled from humanitys ill treatment following Escape from the Planet of the Apes (1971). It was followed by Battle for the Planet of the Apes (1973).  
 reboot Rise of the Planet of the Apes (2011) has a similar premise to Conquest, but is not officially a remake.

==Plot summary== Armando (Ricardo Montalbán) explains that in 1983 (ten years after the end of Escape from the Planet of the Apes, which was set two years ahead of its theatrical release date), a disease killed the worlds cats and dogs, leaving humans with no pets.  To replace them, humans began keeping apes as household pets.  Realizing the apes capacity to learn and adapt, humans train them to perform household tasks. By 1991, American culture is based on ape slave labor (just as Cornelius described would happen in the previous film). It is also suggested that the North America of the 1990s is at least partly a police state, as apes and humans are being watched at all times.
 Cornelius and Zira (Planet of the Apes)|Zira, it would mean their deaths. They see apes performing various menial tasks, and are shocked at the harsh discipline on disobedient apes. Seeing an ape being beaten and drugged, Caesar shouts, "Lousy human bastards!" Quickly, Armando takes responsibility for the exclamation, explaining to the policemen that it was he who shouted, not his chimpanzee. The surrounding crowd becomes agitated, and Caesar flees.
 Don Murray). Breck allows the ape to name himself by randomly pointing to a word in a book handed to him and the chimpanzees finger rests upon the name "Caesar", feigning coincidence.  Caesar is then put to work by Brecks chief aide, MacDonald (Hari Rhodes), who sympathizes with the apes to the thinly veiled disgust of his boss.  MacDonald eventually figures out who Caesar really is.

Meanwhile, Armando is being interrogated by Inspector Kolp (Severn Darden), who suspects his "circus ape" is the child of the two talking apes from the future. Kolps assistant puts Armando under a machine, "The Authenticator," that psychologically forces people to be truthful. After admitting he had heard the name Cornelius before, Armando realizes he cannot fight the machine. A guard comes in to force him to continue the interrogation, but Armando struggles and jumps through a window, falling to his death. Learning of the death of his foster father, the only human that cared for him, Caesar loses faith in human kindness and begins plotting a rebellion.

Secretly, Caesar teaches combat to other apes and has them gather weapons. Meanwhile, Breck learns from Kolp that the vessel which supposedly delivered Caesar is from a region with no native chimpanzees. Suspecting Caesar is the ape the police are hunting, Brecks men arrest Caesar and electrically torture him until he speaks. Hearing him speak, Breck orders Caesars immediate death. Caesar survives his execution because MacDonald lowers the machines electrical output well below lethal levels. Once Breck leaves, Caesar kills his torturer and escapes.

Caesar leads an ape revolt against Ape Management. The apes are victorious after killing most of the riot police. After bursting into Brecks command post and killing most of the personnel, Caesar has Breck marched out to be executed. MacDonald is spared, and he appeals to Caesar to show mercy to his former persecutor. Caesar ignores him, and in a rage declares:

 

As the apes raise their rifles to beat Breck to death, Lisa (Natalie Trundy), Caesars love interest, voices her objection, "NO!" She is the first ape to speak other than Caesar. Caesar reconsiders and orders the apes to lower their weapons, saying:

 

==Cast== Caesar
*Don Don Murray Governor Breck Armando
*Natalie Lisa
*Hari MacDonald
*Severn Kolp
*Lou Wagner as Busboy John Randolph as Commission Chairman
*Asa Maynor as Mrs. Riley
*H.M. Wynant as Hoskyns David Chow as Aldo
*Buck Kartalian as Frank (Gorilla) John Dennis as Policeman
*Paul Comi as 2nd Policeman
*Gordon Jump as Auctioneer
*Dick Spangler as Announcer

==Production== Planet of the Apes, was hired to direct Conquest of the Planet of the Apes. Thompson had worked with Jacobs on two earlier films, What a Way to Go! and The Chairman, as well as during the initial stages of Planet, but scheduling conflicts had made him unavailable during its long development process. 

Thompson staged every scene with attention to detail, such as highlighting the conflicts with color: the humans wear black and other muted colors, while the apes suits are colorful.   contributed   jumpsuits from Voyage to the Bottom of the Sea, brown clothes and computers and cabinets for Ape Management that were used first on The Time Tunnel and other sets and props from other Allen productions.
 Carl Zeiss Group; the other Apes pictures were filmed in Panavision.

===Original opening and ending===
The original cut of Conquest ended with the brutal killing of Governor Breck, with an implicit message implying that the circle of hatred would never end. After a preview screening in Phoenix on June 1, 1972, the impact of the graphic content caused the producers to rework the film, even though they did not have the budget to do so. Roddy McDowell recorded a complement to Caesars final speech, which was portrayed through editing tricks - Caesar being mostly shown through close-ups of his eyes, the gorillas hitting Breck with his rifles played backwards to imply they were giving up  - and assured a lower rating.  The films Blu-ray version adds an unrated version, restoring the original ending and many other graphic scenes. 

Conquest is the only Apes film without a Pre-credit|pre-title sequence.  The films script and novelization describes a nighttime pre-title scene where police on night patrol shoot an escaping ape and discover that his body is covered with welts and bruises as evidence of severe abuse (in a later scene Governor Breck refers to the "ape that physically assaulted his master," thereby prompting MacDonald to report that the escape must have been the result of severe mistreatment). The scene appears in the first chapter of John Jakes novelization of the movie, and in the Marvel Comics adaption of the film in the early 1970s, both of which were probably based directly on the screenplay and not on the final edit of the actual film. An article in the Summer 1972 issue of Cinefantastique (volume 2, issue 2) by Dale Winogura shows and describes the scene being shot,  but it is unknown why it was cut. The Blu-ray extended cut does not contain the pre-credit opening.

===Continuity===
Screenplay writer Paul Dehn, who wrote and co-wrote the sequels, said in interviews with Cinefantastique  (quoted in The Planet of the Apes Chronicles, by Paul Woods) that the story he was writing had a circular timeline:

 

==Reception==
The film received mixed to positive reviews from critics at the time of its release. It currently holds a 44% rotten rating on Rotten Tomatoes, from 18 reviews, all published 2000—2008. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 