Living in Emergency: Stories of Doctors Without Borders
{{Infobox film
| name           = Living in Emergency
| image_size     = 
| image          = Living in Emergency- Stories of Doctors Without Borders FilmPoster.jpeg
| caption        = 
| director       = Mark N. Hopkins
| producer       = {{plainlist|
* Mark N. Hopkins
* Naisola Grimwood
* Daniel Holton-Roth
}}
| writer         = 
| narrator       = 
| starring       = {{plainlist|
* Chris Brasher
* Davinder Gill
* Tom Krueger
* Chiara Lepora
}}
| music          = Bruno Coulais
| cinematography = Sebastian Ischer
| editing        = {{plainlist|
* Bob Eisenhardt
* Sebastian Ischer
* Doug Rossini
}}
| studio         = Red Floor Pictures
| distributor    = Bev Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $1.5 million   
| gross          = $32,000–69,000  
}}
Living in Emergency: Stories of Doctors Without Borders is a 2008 American documentary film directed by Mark N. Hopkins.  It was among the 15 documentaries shortlisted for the Best Documentary Oscar by the Academy of Motion Picture Arts and Sciences for the 82nd Academy Awards. 

It is the first uncensored film about Médecins Sans Frontières (MSF or Doctors Without Borders) and seeks to viscerally portray the real life of western doctors in the field as they confront the many difficulties and dilemmas of working in extreme conditions with limited resources.  Although Living in Emergency is a cinema verité documentary film, it has been compared to fictional films like MASH (film)|M*A*S*H and the TV series House (TV series)|House. 

== Synopsis == Congo and post-conflict Liberia, Living in Emergency interweaves the stories of four doctors as they struggle to provide emergency medical care in extreme conditions.
 
Two of the doctors are new recruits: a 26 year-old Australian stranded in a remote bush clinic and an American surgeon struggling to cope under the load of emergency cases in a shattered capital city. Two others are experienced field hands: a dynamic Head of Mission, valiantly trying to keep morale high and tensions under control, and an exhausted veteran, who has seen too much horror and wants out.

Amidst the chaos, each volunteer must confront the severe challenges of the work, the tough choices, and the limits of their idealism.

== Production ==
 
It took six months for the filmmakers to persuade MSF to grant them unrestricted access to film the field operations. More than 25 production companies had tried and been rejected.

Undeterred, director Mark N. Hopkins and his team produced a 15-minute pilot about the organizations emergency response to the 2005–06 Niger food crisis and were consequently given permission.

For a period of two years the team filmed in various MSF hospitals. The shoots were organized to cover the various facets of the MSF organization: the administrative base, the conflict and post-conflict missions, and a response to natural disaster.
 Second Civil ongoing conflict after the Second Congo War.

Two other countries that were filmed did not make it into the final film.

== Release ==
Living in Emergency premiered at the 65th Venice Film Festival. It was released theatrically by Bev Pictures at the Lumiere Theater in San Francisco and other theaters in the U.S. on June 4, 2010.   First Run Features release it on DVD and VOD.

== Reception ==
  rated it 76/100 based on fifteen reviews.   Ronnie Scheib of Variety (magazine)|Variety wrote that the film captures the subjects "with rare candor and a refreshing lack of piety".   Sheri Linden of The Hollywood Reporter called it an "unforgettable chronicle" that "is a bracing blast of reality".   A. O. Scott of The New York Times wrote, " he film, just like any good hospital television series, is really about the curious psychology of the medical profession." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 