The Good Die Young
 Good Die Young}}
{{Infobox film
| name           = The Good Die Young
| caption        = US 1955 cinema poster
| image	=	The Good Die Young FilmPoster.jpeg
| director       = Lewis Gilbert
| producer       = John Woolf
| screenplay         = Vernon Harris Lewis Gilbert
| based on       =    John Ireland Rene Ray Stanley Baker Margaret Leighton
| music          = Georges Auric
| cinematography = Jack Asher
| editing        = Ralph Kemplen
| studio = Remus Films
| distributor    = IFD   United Artists  
| released       =  
| runtime        = 94 min.
| country        = United Kingdom 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        =
}} thriller film made in the United Kingdom by Remus Films, featuring a number of American characters. It was directed by Lewis Gilbert. The screenplay was based on the book of the same name written by Richard Macaulay.

The cast includes Laurence Harvey, Gloria Grahame, Joan Collins, Stanley Baker and Richard Basehart.

==Plot==

The film opens with four men in a car, apparently about to commit a serious crime. How each of the previously law-abiding men came to be in this position is then explored.
 John Ireland) is an AWOL American airman with an unfaithful actress wife (Gloria Grahame). The last man, Rave Ravenscourt (Laurence Harvey), is a gentleman sponger and a scoundrel with gambling debts and the unscrupulous leader who lures the other three. The film reaches a bloody climax at Heathrow Airport.

==Principal cast==
 
* Laurence Harvey as Miles Rave Ravenscourt
* Gloria Grahame as Denise Blaine
* Richard Basehart as Joe Halsey
* Joan Collins as Mary Halsey John Ireland as Eddie Blaine
* Rene Ray as Angela Morgan
* Stanley Baker as Mike Morgan
* Margaret Leighton as Eve Ravenscourt
* Robert Morley as Sir Francis Ravenscourt
* Freda Jackson as Mrs Freeman
* James Kenney as Dave, Angelas brother 
* Susan Shaw as Doris, girl in the pub 
* Lee Patterson as Tod Maslin
* Sandra Dorne as pretty girl 
* Leslie Dwyer as Stookey
* Patricia McCarron as Carole George Rose as Bunny 
* Joan Heal as Woman 
* Walter Hudd as Dr Reed 
 

==Production==
 BOAC Boeing Boeing Stratocruiser aircraft at Heathrow Airport and the District Line around Barbican. Laurence Harvey subsequently married Margaret Leighton, who played his wife in the film.

The films screenwriters changed the setting of Richard Macauleys original novel from America to 1950s England.  The British bank financing the film also required that the novels bank robbery be switched to a post office in the film version. 

The film opened in the UK on 2 March 1954, with general release following on 5 April. 

==Notes==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 