Krakatit
 
{{Infobox film
| name           = Krakatit
| image          = Krakatit poster.jpg
| caption        = Theatrical release poster
| director       = Otakar Vávra
| producer       = Otakar Vávra
| screenplay     = Otakar Vávra Jaroslav Vávra
| based on       = Krakatit by Karel Čapek
| starring       = Karel Höger
| music          = Jiří Srnka
| cinematography = Václav Hanuš
| editing        = Antonín Zelenka
| studio         = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}
Krakatit is a 1948 Czechoslovak science fiction mystery film directed by Otakar Vávra, starring Karel Höger as a chemist who suffers from delirium and regret after inventing a powerful explosive. The film is based on Karel Čapeks novel with the same title, written in 1922. The name is derived from the volcano Krakatoa.

==Plot==
A barely conscious, unidentified man is given intensive care by a doctor and a nurse. The mans hands are badly burned and cut. The doctor tells the nurse to give the man oxygen.

A sequence begins where the man walks along a street in delirium. An old classmate, Jiří Tomeš, greets him and we learn that the mans name is Prokop. He speaks incoherently about an explosion and something he calls krakatit. Jiří brings him home and puts him in bed. In a dream, a university professor asks Prokop about krakatit. He answers that it is a powerful explosive, named after the volcano Krakatoa, and explains its formula. As the dream ends we see that Jiří has written the formula down. Prokop wakes up alone in Jiřís apartment. He finds a note which says that Jiří has gone to his father. Prokop opens the door when a veiled woman rings the bell. She begs Prokop to deliver a letter to Jiří. Prokop, still not recovered, decides to visit Jiřís father, a countryside doctor. An old, horse-powered mail carrier gives Prokop lift. Upon arriving he learns that Jiří has not visited his father in a long time. Prokop collapses.

Tended by the doctor and his daughter Anči, Prokop slowly recovers, but cannot recall what has happened. One day he reads in a newspaper about krakatit, which is being promoted by someone named Carson, and Prokop suddenly remembers the explosion in his laboratory. He rushes to the laboratory where he meets Carson, a representative from Balttin Works, a foreign weapon manufacturer. Carson explains that Jiří had sold them Prokops stock of krakatit and the formula, and that their experiments with krakatit have shown that it is a highly deadly explosive which can be detonated with high-frequency radio signals. However while they have the formula, they have failed to figure out the production procedure, and offer Prokop to work for them. Prokop declines but is brought to the Balttin palace by force.

At the palace Prokop begins a romance with a princess. Meanwhile, he realizes he actually is in love with the veiled woman from Jiřís apartment. The Balttin executives offers him to marry the princess if he gives them krakatit. In anger, Prokop blows up the laboratory where he has been held captive, and with his coat loaded with explosives he confronts the princess, whose face dissolves. Prokop is helped to escape from the palace by an ambassador named dHémon, who brings him to a secret society of former world leaders and weapon dealers, who worship war and hail Prokop as Comrade Krakatit. As tumult breaks out, a can of krakatit, originally from Prokops old laboratory, is emptied and the members fight to gather some of the powder for themselves. Prokop and dHémon leave and briefly run into the veiled woman.

DHémon brings Prokop to the top of a hill made of magnetite, where Prokops old laboratory has been relocated and turned into a secret radio station. They enter and dHémon makes Prokop push a button. All remaining krakatit, scattered around European capitals, then explode in the far distance. DHémon explains that the explosions are certain to trigger a great war which can be controlled from the radio station with krakatit. Prokop becomes furious and screams at dHémon, who disappears before his eyes.

Prokop finds himself in a desolate, concrete landscape. He comes upon an enclosed factory, where he asks to see Jiří Tomeš. His requests is rejected, but he is allowed to meet a lab assistant, to whom he gives the letter from the veiled woman. Prokop walks away from the factory. After a while he sees it explode in the distance. The old mail carrier appears and gives Prokop a lift. The driver suggest Prokop should invent something that makes peoples lives easier instead of killing people. Back at the hospital from the first scene, the doctor says that the patient now is breathing normally and the oxygen mask can be removed.

==Cast==
* Karel Höger as Prokop
* Florence Marly as Princess Wilhelmina Hagen
* Eduard Linkers as Carson
* Jiří Plachý as dHémon
* Nataša Tanská as Anči Tomeš
* František Smolík as Dr. Tomeš
* Miroslav Homola as Jiří Tomeš
* Vlasta Fabianová as the veiled woman / the revolutionist woman
* Jaroslav Průcha as old mail carrier
* Jiřina Petrovická as the nurse
* Jaroslav Zrotal as the doctor
* Bedřich Vrbský as Baron Rohn
* Bohuš Hradil as Holz

==Reception==
The film premiered in Czechoslovakia on 9 April 1948.  In 1951 it was released in the United States. In The New York Times, the film was called "a strident preachment for peace and against destructive nuclear fission, but basically it is clouded and halting drama."
The critic wrote favourably about the performances of Höger, Marly, Tanská and Linkers, but felt: "Despite the adequate English subtitles, the rest of the cast moves through the scientists dream world much like the robots invented by Čapek in R.U.R.|R. U. R. They cant be blamed however, for Krakatit is sapped by a surfeit of symbolism." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 