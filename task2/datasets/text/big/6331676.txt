From Noon till Three
{{Infobox film
| name           = From Noon Till Three 
| image          = From noon till three ver2.jpg
| image size     = 
| alt            = A cartoon image of a gang of bank robbers. The text above reads: Itll keep you on the edge of your saddle. A speech bubble from the robber reads: Just keep going like nothing was wrong. 
| caption        = Film poster
| director       = Frank D. Gilroy  William Self 
| writer         = Frank D. Gilroy 
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Charles Bronson  Jill Ireland 
| music          = Elmer Bernstein 
| cinematography = Lucien Ballard 
| editing        = Maury Winetrobe 
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 99 minutes 
| country        = United States
| language       = English 
| budget         = 
| gross          = 
}}
From Noon Till Three is an American film released in 1976 by United Artists. It stars Charles Bronson and his wife, Jill Ireland.  It was written and directed by Frank D. Gilroy, based on his novel.

==Plot==
 
The setting is the American West, late 19th century. A gang of bank robbers led by Graham Dorsey (  in which the gang was wiped out during the robbery attempt. Worse, Grahams horse broke down and the gang members have to get another. They try at the ranch of the widow Amanda Starbuck ( , hoping to play on Amandas sympathy. The deception works, and they make love three times.  

As time passes, Graham and Amanda have a long, thoughtful discussion talking of their past lives, as well as their hopes and ambitions (Graham wants to go straight and work in a bank!). They even dance to Amandas music box ("Hello and Goodbye"), with Graham wearing Mr. Starbucks old  , and the first person Graham encounters after his escape was one of Dr. Fingers dissatisfied customers. He is put into prison on a year-long sentence for Dr. Fingers crimes.  

At first Amanda is ostracized by the townspeople. But an impassioned speech proclaiming her true love for him does a remarkable trick: the townspeople not only forgive her, they see a remarkable story in that of Graham and Amanda. This story forms the basis of a legend, one that spawns a popular book (From Noon Till Three), dime novels, a stage play, and even a popular song. The legend of Graham and Amanda becomes bigger than the reality of the two, and with her book a worldwide best seller it makes Amanda a wealthy woman. Graham, who reads the book while in prison, is amused by the distortions. Graham is described as being 63" (1.90 m), Southern, and very handsome; he is, in fact, none of these. After serving his time he is eager to renew his relationship with Amanda.  

A disguised Graham takes one of Amandas guided tours of her ranch, and stays behind, intending to reveal himself. When he does so, Amanda does not recognize him and becomes frightened. It is only when Graham shows her "something thats not in the book" that Amanda believes him. (Grahams exposing himself is cut from the TV version, causing brief audience confusion as to why Amanda finally "recognizes" him.) But instead of joy, Amanda is confused and worried. If word got out that Graham was alive, the legend of Graham and Amanda would be done for. Even Grahams suggestion that he live with her incognito is no good; after all, if Amanda were to live with another man, the legend would still be destroyed. The encounter ends up with Amanda pointing a gun at Graham... but at the last second she decides to shoot herself. 

Graham is heartbroken. Not only has he lost Amanda, the secret of his real identity is lost for good. He tries to forget what has happened, but there are reminders everywhere. He hears "their song" at a local saloon, and walks in on a stage production of From Noon Till Three. Worse, people he knew slightly laugh when he says he is Graham, since he looks nothing like his description in the book. At the end he is arrested and put in an insane asylum, where he meets the only people who believe him: his fellow inmates. He seems relieved.

==Awards==
Nominated for a Golden Globe for Best Original Song.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 