Vicky Donor
 
 
{{Infobox film
| name           = Vicky Donor
| image          = Vicky Donor 2012.jpg
| caption        = Theatrical release poster
| director       = Shoojit Sircar   John Abraham,  Ronnie Lahiri
| writer         = Juhi Chaturvedi    Puja Gupta
| music          = Abhishek-Akshay Bann Rochak Kohli Ayushmann Khurrana 
| cinematography = Kamaljeet Negi
| studio         = Rising Sun Films
| distributor    = John Abraham Entertainment Eros Entertainment
| released       =  
| runtime        = 125 minutes   
| country        = India Hindi Punjabi Punjabi Bengali Bengali
| budget         =  
| gross          =  (worldwide) 
}}
 John Abraham. {{cite web |url=http://www.hindustantimes.com/Entertainment/Bollywood/John-Abraham-proud-of-his-first-baby/Article1-812426.aspx |work=Hindustan Times |title=John Abraham proud of his first baby
|date=16 February 2012
|accessdate=18 February 2012
}}  It stars Ayushmann Khurrana, Yami Gautam and Annu Kapoor in the lead roles. It released on 20 April 2012 in around 750 screens across India    {{cite web
|url=http://ww.bollywoodhungama.com/moviemicro/cast/id/548152
|publisher=Bollywood Hungama
|title=Vicky Donor Cast & Crew http://www.youtube.com/watch?v=Jme-VkIzkoUaccessdate=February 18, 2012
}}  and received positive response worldwide.  Vicky Donor opened to critical acclaim and performed strongly at the box office and was declared a Super Hit by Box Office India.   The storyline is based on  sperm donation and infertility.    The story is loosely based on the Canadian French film Starbuck (film)|Starbuck.  

==Plot==
Dr. Baldev Chaddha (Annu Kapoor) is a fertility expert who runs a clinic and a sperm bank in Daryaganj, Delhi that guarantees high-quality and specialised sperm for couples. Unfortunately, he has more failed cases to his credit than successes. A healthy and high-performing donor is needed.
 Puja Gupta) is infatuated with him, but Vicky doesnt reciprocate which causes friction between the two.

A small brawl in the colony brings Chaddha and Vicky face to face; Chaddha concludes that Vicky could be the donor he has been looking for. From here on, Chaddhas days and nights are spent convincing Vicky to become a donor; he finally gives in. Although Vicky is hesitant at first, he accepts being a sperm donor after seeing the money he could make. He starts to spend money wildly and renovates his home and his mothers beauty parlor. He fulfills all his dreams with the money he is making. At first he openly discloses he is a sperm donor to his friends but, after repeatedly disgusting them, he starts to keep it a secret.
 Bengali bank employee, whom he meets when he goes to deposit money.  At first Ashima is repelled by Vicky and his attitude, but slowly becomes attracted to him. After dating, they admit they are in love and want to get married. Ashima also discloses that she was briefly married to a Bengali, who left her immediately after their wedding because he loved someone else. When she asks Vicky about his status, he hides the fact he is a sperm donor feeling ashamed of it. Despite initial friction between Vickys and Ashimas families due to conflicting cultures, they convince their families and get married.

After their wedding Vicky stops donating sperm out of faithfulness to Ashima. It is discovered that Ashima is infertile and Vicky, with a heavy heart, takes better care of her. One day while looking at the doctors reports, Ashima cannot find Vickys reports and asks him where they are. He admits to not having taken the fertility tests, because of his past as a sperm donor. Ashima becomes extremely upset as her husband has multiple children while she is unable to bear any herself. She is also upset that he lied to her. Ashima goes back to Kolkata to her fathers home.

Vicky is very upset as he loves Ashima a lot and cannot live without her. One night he gets a frantic call from his mother who claims that income tax agents have raided her beauty parlor. Vicky rushes to her aid. Once there he is arrested on suspicion of handling black money. While at the police station, Vicky is bailed out by Dr. Chaddha and he clear the air with the police about Vickys money. He explains that he is a sperm donor and the money was from the parents who used Vickys sperm. After being released by the police, Dr. Chaddha and Vicky have a chat. Vicky explains that Ashima has left because of his work as a sperm donor and that she is infertile. Dr. Chaddha tells him that he will fix his marriage.

He asks Vicky to bring Ashima to a guest house where he will host an event. Vicky is reluctant because he is not sure if Ashima will be willing to come with him. After Vicky has a chat with Dr. Chaddha he goes to his mother home. She is very upset with him and slaps him. She is angry with him about the sperm donation. While his grandmother is also clearly upset, she points out that Vicky brought happiness and joy to those parents and thats all that matters to her. She then encourages Vicky to go to Kolkata and bring back Ashima, which he does. Vicky then goes to Ashimas fathers home and tries to apologize for his deception. Although she is reluctant, Ashimas father sets her straight and tells her that Vicky is a good person, and she should not leave him because she is envious of his being able to have children.

She agrees to go with Vicky to Dr. Chaddhas event. Dr. Chaddha called all the families who received Vickys sperm to a 25th anniversary party for his clinic. She then sees how Vicky has brought happiness into the families lives. Dr. Chaddha then takes Vicky and Ashima to an orphanage. He explains that while tracking down two children who Vicky fathered, he came across one child he fathered who lost her parents in a car accident and had to be put in an orphanage. He says that Vicky and Ashima should adopt her. They happily agree and reconcile with each other.

The movie ends with Dr. Chaddha calling Vicky telling him that a request for his sperm has come in, asking if he will donate. With Ashimas approval he agrees.

==Cast==
* Ayushmann Khurrana as Vicky Arora from Delhi
* Yami Gautam as Ashima Roy Arora 
* Annu Kapoor as Dr. Baldev Chaddha
* Dolly Ahluwalia as Mrs. Arora 
* Kamlesh Gill as Biji  
* Jayant Das as Mr. Roy (Ashimas Father)
* Bupesh Pandya as Chaman
* K.V Rajni as nurse Lata.  Puja Gupta as Shweta John Abraham as himself in the song "Rum Whisky"

==Themes and development==
Vicky Donor deals with sperm donation and infertility. {{cite web
|url=http://www.hindustantimes.com/Entertainment/Bollywood/John-next-production-based-on-sperm-donors/Article1-812746.aspx|work=Hindustan Times|title=John next production based on sperm donors|date=17 February 2012|accessdate=18 February 2012}}  When questioned about the risky subject matter, Abraham responded, "Basically, Vicky Donor is a romantic comedy. But the concept is set against the background of sperm donation." Abraham hoped that the film could shed light on a serious issue still considered "taboo" in Indian society.

Sircar said, "I want to take a light-hearted look at the taboo attached to infertility and artificial insemination." Before filming began, Sircar researched the plot themes for three years. {{cite news
|url=http://articles.timesofindia.indiatimes.com/2011-10-07/news-interviews/30257028_1_ayushmann-khurrana-sperm-donor-film
|work=The Times of India|title=Ayushmanns film debut as sperm donor|date=7 October 2011|accessdate=19 February 2012
|deadurl=yes
}}    Juhi Chaturvedi, creative director at advertising agency Bates, who wrote the screenplay based on her own idea, had stayed in Lajpat Nagar as a student at College of Arts, Delhi. As the script went through several drafts, she met a couple who run a noted fertility clinic in Mumbai.    

==Production==
Despite the usual trend, Abraham did not take up the leading role in the film: "It was a conscious decision to not act in my first production because I believe producing a film is about creating quality content that I believe in." {{cite news |url=http://articles.timesofindia.indiatimes.com/2011-11-09/news-interviews/30373777_1_first-film-priyanka-chopra-john-abraham
|work=The Times of India
|title=John Abraham is looking for a Donor |date=9 November 2011
|accessdate=19 February 2012
}}  Director Sircar suggested Khurrana, a video jockey and television host, for the role of Vicky; he was considered because he was popular among the youth. He "can read and write   too."  {{cite web
|url=http://www.hindustantimes.com/Entertainment/Bollywood/Introducing-John-s-new-hero-Ayushman-Khurana/Article1-813255.aspx
|work=Hindustan Times
|title=Introducing John’s new hero: Ayushman Khurana
|date=18 February 2012
|accessdate=18 February 2012
}}  Khurrana turned down three film projects to play the lead in Vicky Donor. To prepare for the role, he met medical experts and donors to understand sperm donation; a major medical consultant specialising in the field was later inducted as a medical adviser for the film. Additionally, Khurrana studied acting and attended workshops with N.K. Sharma in Delhi.  It was later reported that Abraham would perform an "item number." {{cite web
|url=http://daily.bhaskar.com/article/ENT-john-turns-producer-and-item-boy-for-sperm-donors-2874314.html
|work=Daily Bhaskar
|title=John turns producer and item boy for sperm donors! |date=18 February 2012
|accessdate=18 February 2012
}}  Commenting on the his performance, Abraham said "I am glad I cast Ayushmann in the lead. Hes a complete natural, has all the trappings of a fine actor and has delivered a super performance. It doesnt seem like Vicky Donor is his first film." It was reported that the production crew would feature "the best technicians from Los Angeles" and acclaimed action choreographer J J Perry. Incidentally, Ayushmann who plays the lead role, successfully performed a task on the reality show MTV Roadies: Season 2, which was sperm donation.  

==Reception==

===Critical reception===
{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review Scores
|-
! Source
! Rating
|-
| FilmiTadka
|  
|-
| IMDB
|  
|-
| NDTV
|  
|-
| Zee News
|  
|-
| DNA India
|  
|-
| The Statesman
|  
|-
| The Times of India
|  
|-
| CNN-IBN
|  
|-
| Hindustan Times
|  
|-
| Bollywood3
|  
|-
| Rediff
|  
|-
| The Indian Express
|  
|-
| Yahoo
|  
|-
| Tehelka
|  
|- average rating of all reviews provided by the source
|}

Vicky Donor received near unanimous acclaim from top critics of India. Blessy Chettiar of DNA India rated it with 4 stars and said, "Run to the nearest theatre and surrender yourself to charm of Vicky and his team. This "Aryaputra" provides only good quality entertainment."  Writing for FilmiTadka, Janhavi Patel gave it 4 out of 5 stars and wrote in her review, "Vicky Donor is one of the funniest movies in recent times and the most enjoyable cinematic experience you will have in a long time. It proves that an adult comedy can be made with class, ingenuity, and finesse. You can easily watch this twice!"  Madhureeta Mukherjee of The Times of India gave Vicky Donor 3.5 stars and said, "It takes a man to make a film like this, literally. Kudos to John Abraham for his brave maiden production. Thankfully, this sperm hits bullseye."  Mathures Paul of the The Statesman gave the film 3.5 stars and wrote, "Vicky Donor is an admirable movie that is at once simple, emotional, daring and in your face." 

===Awards===
* National Film Award for Best Popular Film Providing Wholesome Entertainment at the 60th National Film Awards
* National Film Award for Best Supporting Actor at the 60th National Film Awards - Annu Kapoor
* National Film Award for Best Supporting Actress at the 60th National Film Awards - Dolly Ahluwalia
* Filmfare Award for Best Supporting Actor at the 58th Filmfare Awards - Annu Kapoor
* Filmfare Award for Best Male Playback Singer at the 58th Filmfare Awards - Ayushman Khurana
* Filmfare Award for Best Male Debut at the 58th Filmfare Awards - Ayushman Khurana
* Filmfare Award for Best Story at the 58th Filmfare Awards - Juhi Chaturvedi

===Box office===
The movie made 700% profit at the box office, earning approx   worldwide.

====India==== Paan Singh Tomar and Kahaani to receive such an acceptance by critics, multiplex goers and urban audience. It grossed   nett on Sunday. Vicky Donor had a decent three-day weekend as it grossed around   nett over its first weekend. 

It had a good first week of around   nett.  Vicky Donor had an excellent second week collecting around   nett taking its two-week total to around   nett.  Vicky Donor continued its strong run into the third week and totally grossed   in full run and was declared a Super Hit.   

====Overseas====
The film had a wide release with close to 125 prints and grossed a decent $350,000 overseas in four days.  It has done well in North America and decently in other markets.  It was declared a semi hit in overseas markets.

Grosses are as follow:
* UK: £90,329 ($140,600)
* USA: $466,467
* UAE: $286,000
* Australia: $88,682

Total: $1.2 million (65&nbsp;million) 

==Sequel and remakes== John Abraham announced that he was encouraged by the response of Vicky Donor and has therefore laid a sequel on the cards, which is expected to go on floors in mid-2013.
 Siddharth bought Tamil version. Dileep has Malayalam version. Madhura Sreedhar bought the remake rights of the film for the Telugu version named Danakarna. 

==Soundtrack==
 
{{Infobox album
| Name       = Vicky Donor
| Type       = Soundtrack
| Artist     = Abhishek-Akshay, Bann, Rochak Kohli and Ayushmann Khurrana
| Cover      = Vicky Donor 2012.jpg
| Released   =  
| Recorded   =
| Length     = 35:11 Feature film soundtrack
| Label      = Eros Music John Abraham
| Last album = Housefull 2 (2012)
| This album = Vicky Donor (2012)
| Next album = Dostana 2
}}

The soundtrack was composed by Abhishek-Akshay, Bann, Rochak Kohli, and Ayushmann Khurrana. The album was released on 9 April 2012.

IBNlive.in quotes "Vicky Donor album tracks are worth listening and contain rawness."  Ayushmann Khurrana won Filmfare Award for Best Male Playback Singer at 58th Filmfare Awards for "Pani Da Rang." 

===Track listing===
{{tracklist
| headline        =
| lyrics_credits = yes
| composers_credits = yes
| extra_column    = Artist(s)
| total_length    = 35:11

| title1          = Rok
| lyrics1        = Akshay Verma
| extra1          = Akshay Verma, Aditi Singh Sharma
| length1         = 4:28

| title2          = Kho Jaane Do
| lyrics2        = Juhi Chaturvedi
| extra2          = Clinton Cerejo, Aditi Singh Sharma
| length2         = 4:57

| title3          = Rum Whisky
| lyrics3        = Kusum Verma
| extra3          = Akshay Verma
| length3         = 4:04
 Pani Da Rang (Male)
| lyrics4        = Ayushmann Khurrana, Rochak Kohli
| extra4          = Ayushmann Khurrana
| length4         = 4:00

| title5          = Mar Jayian (Romantic)
| lyrics5      = Swanand Kirkire
| extra5          = Vishal Dadlani, Sunidhi Chauhan
| length5         = 4:48

| title6          = Chaddha
| lyrics6      = Vijay Maurya
| extra6          = Mika Singh
| length6         = 3:50
 Pani Da Rang (Female)
| lyrics7        = Ayushmann Khurrana, Rochak Kohli
| extra7          = Sukanya Purkayastha
| length7         = 4:49

| title8          = Mar Jayian (Sad)
| lyrics8      = Swanand Kirkire
| extra8         = Bann
| length8         = 4:14

}}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 