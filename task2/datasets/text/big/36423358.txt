Paranoia (2013 film)
{{Infobox film
| name           = Paranoia
| image          = Paranoia Poster.jpg
| caption        = Theatrical release poster
| director       = Robert Luketic
| producer       = Alexandra Milchan Scott Lambert Deepak Nayar
| screenplay     = Barry Levy Jason Dean Hall
| based on       =  
| starring       = Liam Hemsworth Gary Oldman Amber Heard Harrison Ford Lucas Till Embeth Davidtz Julian McMahon Josh Holloway Richard Dreyfuss
| music          = Junkie XL
| cinematography = David Tattersall
| editing        = Priscilla Nedd-Friendly Gaumont Film Entertainment One IM Global
| distributor    = Relativity Media
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $15–$35 million   -video/ 
| gross          = $13,785,015 
}} thriller film Barry L. novel of the same name by Joseph Finder.  It stars Liam Hemsworth, Gary Oldman, Amber Heard and Harrison Ford.

The film was released on August 16, 2013, and was a critical failure as well as a box office bomb.

==Plot==
Adam Cassidy is a low-level inventor who works for a corporation run by Nicholas Wyatt. After being fired for insubordination, Adam uses the companys credit card to pay for bottle service for his friends at a club. Wyatt and his enforcer, Miles Meachum, blackmail Adam into becoming a corporate spy for Wyatt by threatening to have him arrested for fraud.

Adam is trained by Judith Bolton and infiltrates a company run by Wyatts former mentor, Jock Goddard. He provides Goddard, who stole several of Wyatts ideas, with an advanced software able to hack into cellphones, with potential military applications. FBI Agent Gamble interrogates Adam, revealing that three other employees of Wyatt who transferred to Goddards company were found dead, but Adam ignores him.

Adam finds out Emma Jennings, a woman he met during the party, is the Director of Marketing of Goddards company. He initiates a relationship with Emma in order to steal files about Goddards upcoming projects. Wyatt threatens to kill Adams father, Frank Cassidy, if Adam doesnt steal a revolutionary prototype cellphone developed by Goddard. Adam later finds out Meachum and Bolton are monitoring him, so he destroys the cameras in his apartment. In retaliation, Meachum runs over Adams friend, Kevin, with a car, nearly killing him. Adam is given 48 hours to steal the prototype.

Adam uses Emmas thumbprint lifted from a spoon to gain security access to the companys vault. He is confronted there by Goddard, who intends to take over Wyatts company with evidence that Adam was acting as Wyatts spy. Emma finds out Adam used her. Adam recruits Kevin to help him. A meeting is set with Wyatt and Goddard, where it is revealed that Bolton has spied against Wyatt on Goddards behalf. Both men speak of the crimes they have committed to sabotage each others companies.

Adam has secretly used software to transmit their conversation to Kevin, whose computer recordings are turned over to the FBI. Goddard, Wyatt, Bolton and Meachum are arrested by Gamble, while Adam is released for contributing to the FBIs investigation. He reconciles with Emma and opens a small startup company in Brooklyn with Kevin and their friends.

== Cast ==
* Liam Hemsworth as Adam Cassidy
* Gary Oldman as Nicholas Wyatt
* Amber Heard as Emma Jennings 
* Harrison Ford as Augustine "Jock" Goddard
* Lucas Till as Kevin
* Embeth Davidtz as Dr. Judith Bolton
* Julian McMahon as Miles Meachum
* Josh Holloway as Agent Gamble
* Richard Dreyfuss as Frank Cassidy
* Angela Sarafyan as Allison
* Nickson Ruto as Fala William Peltz as Morgan
* Kevin Kilner as Tom Lundgren
* Christine Marzano as Nora Sommers
* Charlie Hofheimer as Richard McAllister
* Dennisha Pratt as Adams Ikon Assistant

== Production ==
Principal photography commenced on location in Philadelphia in July 2012, and returned for further filming at the end of November 2012.  The first trailer was released on June 6, 2013. 

== Release ==
Paranoia was a box office bomb.  The film debuted at #13 in the United States, generating only $3.5 million in its first weekend and going on to gross a total of $7,385,015 domestically.  It made $6,400,000 in other countries for a worldwide total of $13,785,015, roundly failing to recoup its $35 million budget.  Variety (magazine)|Variety magazine listed Paranoia as one of "Hollywoods biggest box office bombs of 2013".   

== Reception ==
  wrote, "Not since Taylor Lautner has Hollywood ogled a pretty boy this vacant and poorly prepared."   Andrew Barker of Variety (magazine)|Variety wrote, "No one seems paranoid enough in this indifferently made, nearly tension-free thriller."   Stephen Farber of The Hollywood Reporter called it "slick but muddled". 

== See also ==
* List of films featuring surveillance

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 