6'2
{{Infobox film
| name           = 6&#39;2
| image          = 
| alt            =  
| caption        = 
| director       = V. Senthil Kumar
| producer       = 
| writer         = 
| starring       = Sathyaraj Sunitha Varma Vadivelu
| music          = D. Imaan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Telugu with a dubbed version. 

==Plot==
James (Sathyaraj) and Mr. White (Vadivelu) work in a call centre in Chennai. James witnesses the murder of Krishnamurthy (Ajay Rathnam), a labor union leader. He promises the police to identify the murderer. In order to crack the case, a police inspector, Soundara Pandian (Raj Kapoor), urges the lady informer, Aishwarya (Sunitha Varma), to act as James wife and stay in a house opposite to Krishnamurthys.

Upon coming to the house, the couple tries to crack the case. In this regard, they befriend the family, but soon, Krishnamurthys father is also murdered. Towards the interval, one comes to know that the actual murderer is James himself. In order to avenge the murder of his parents by Krishnamurthy and his brother Ramamurthy (Raveendhar) a few years ago, James stays in the house opposite to theirs.

With knowledge of his motive, Aishwarya promises to help him in bumping off Ramamurthy, who returns from Australia.

==Cast==
*Sathyaraj as James
*Sunitha Varma as Aishwarya
*Vadivelu as Mr. White
*Ravinder as Ramamurthy
*Rajkapoor as Soundara Pandian
*Ajay Rathnam as Krishnamurthy
*Manobala
*Vincent Asokan
*Rinku Ghosh

==References==
 

 
 
 
 


 