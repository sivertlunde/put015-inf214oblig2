Kondaveeti Donga
{{Infobox film
| name           = Kondaveeti Donga
| image          = KondaveetiDongafilm.jpg
| caption        = DVD Cover
| director       = A. Kodandarami Reddy
| producer       = T Trivikrama Rao
| writer         = Paruchuri Brothers (Story & Dialogues)   Yandamuri Veerendranath (Screenplay)  Sharada Vijayashanti Radha Rao Gopal Rao Amrish Puri
| music          = Illayaraja
| cinematography = K S R Swamy
| editing        = Kotagiri Venkateswara Rao
| distributor    =
| released       = March 09, 1990
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
 swashbuckler thriller film starring Chiranjeevi directed by veteran A. Kodandarami Reddy. Upon release the film received positive reviews and emerged as a blockbuster. Subsequently, the film was dubbed into Tamil as Thangamalai Thirudan.   The technically brilliant film was the first Telugu film to be released on a 70&nbsp;mm 6-Track Stereophonic sound.   The film had collected a distributors share of   7.4 million on its opening weekend. 

==Plot==
Raja (Chiranjeevi) goes to town for higher studies with the donation of people living in a tribal village. He returns to his village after the studies. He then finds the sufferings of his people and how they are cheated local heads (Rao Gopal Rao and Mohanbabu). He then fills confidence in his men and tells them that they should face these illegal in the court of law. But this duo makes the villagers fools and Rajas attempts goes in vain. Few villagers get hurt by this act suicide as they have no other option and the blame is on Raja as he has forced them. Sametime Raja is selected for IAS but he rejects it and decides to fight against the villains and changes to a Robinhood avatar and names himself as Kondaveti Donga. He punishes the illegal and stands by the villagers thus all his folk starts admiring him. Trouble starts when the villains start doing mischief in the name of him. Vijayasanthi is an inspector comes to his place to catch hold of him. Radha her sister, working as a doctor knows his true identity and starts loving him. How this kondaveti donga punished the fraud people or is he caught by the police or what happens to him forms the rest of the story. There is also a dance routine with a Michael Jackson impersonator singing "Girly Man".

==Soundtrack==
The soundtrack composed by Illayaraja remained a chart buster.

*"Subhalekha Rasukunna"
*"Jeevithame Oka Aata"
*"Amma Shambhavi"
*"Chamak Chamak"
*"Kolo Koloyamma"
*"Sri Anjaneyam"
*"Tip Top Lokku"

==References==
 

 
 
 
 
 

 