Flying Saucer Daffy
{{Infobox Film |
  | name           = Flying Saucer Daffy
  | image          =Flyingsaucerdaffytitlecard1957.JPEG
  | caption        = |
  | director       = Jules White Jack White Warren Wilson
  | starring       = Moe Howard Larry Fine Joe Besser Gail Bonney Emil Sitka Harriette Tarler Diana Darrin Bek Nelson Joe Palma
  | cinematography = Fred Jackman Jr. 
  | editing        = Saul A. Goodkind
  | producer       = Jules White
  | distributor    = Columbia Pictures 
  | released       = October 9, 1958 
  | runtime        = 16 07" 
  | country        = United States
  | language       = English
 }}

Flying Saucer Daffy is the 187th short subject that stars American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
 
Joes accidental snapshot of a paper plate blown by a breeze, is mistaken for a picture of a Unidentified flying object|UFO. However, Moe and Larry take the credit for the photo, and are paid a huge sum. Angered, Joe leaves the boys for a camping trip, only to meet two genuine and beautiful aliens from Planet Zircon who allow Joe to photograph them. Moe and Larry end up arrested when their UFO picture is revealed to be a fraud. They are put in straitjackets and incarcerated in a psychiatric hospital while Joe becomes a national hero.

==Production notes==
Flying Saucer Daffy  features Moe and Larrys more "gentlemanly" haircuts, first suggested by Joe Besser. However, these had to be used sparingly, as most of the shorts with Besser were remakes of earlier films, and new footage had to be matched with old.  Besser later reported that Flying Saucer Daffy was his favorite Stooge comedy.  The short features stock footage from Earth vs the Flying Saucers. 

Over the course of their 24 years at Columbia Pictures, the Stooges would occasionally be cast as separate characters. This course of action always worked against the team; author Jon Solomon concluded "when the writing divides them, they lose their comic dynamic."    In addition to this split occurring in Flying Saucer Daffy, the trio also played separate characters in Rockin in the Rockies, He Cooked His Goose (and its remake Triple Crossed), Gypped in the Penthouse, Cuckoo on a Choo Choo and Sweet and Hot. 

===End of an era===
Though Flying Saucer Daffy was not the last short subject released by the Stooges (that honor goes to Sappy Bull Fighters), it was the last one produced. Filming took place on December 19–20, 1957.    Several hours after filming wrapped, the Stooges were unceremoniously fired from Columbia Pictures after 24 years making low-budget shorts. Joan Howard Maurer, daughter of Moe Howard, wrote the following in 1982: 
{{quote|text="The boys careers had suddenly come to an end. They were at Columbia one day and gone the next—no Thank yous, no farewell party for their 24 years of dedication and service and the dollars their comedies had reaped for the studio.

Moe Howard recalled that a few weeks after their exit from Columbia, he drove to the studio to say goodbye to several studio executives when he was stopped by a guard at the gate (obviously, not a Stooges fan) and, since he did not have the current years studio pass, was refused entry. For the moment, it was a crushing blow." }}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 