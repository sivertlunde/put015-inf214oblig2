Annayya (2000 film)
{{Infobox film
| name           = Annayya
| image          = Annayya 2000 Poster.jpg
| image_size     = 
| caption        = 
| director       =Muthyala Subbaiah
| producer       = 
| writer         = Satyanand
| narrator       = 
| starring       = Chiranjeevi Ravi Teja Soundarya Sarath Babu
| music          = Mani Sharma
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
}} Tollywood film which released on January 7, 2000 and was directed by Muthyala Subbaiah. This film stars Chiranjeevi, Ravi Teja, Soundarya, Sarath Babu and Gangadhar Panday. The music was composed by Mani Sharma. While Bhupati Raja provided story, writer Satyanand penned dialogues.

Annayya was a huge box office hit both critically and commercially due to Chirus performances. The film had collected a distributors share of Rs.60 million in the initial two weeks after its release.  It completed 50 days in 92 centres  and 100 days in 60 centres,  which made Annayya one of the highlest acclaimed films of Chiranjeevi in his career.

== Plot ==

Rajaram (Chiranjeevi), owner of a fleet of lorries, meets Devi (Soundarya), a garment factory owner. After that acquaintance, she once seeks his help in dealing with two street ruffians, who were teasing her sisters, Lata (Chandni (actress)|Chandini) and Geetha (Sishwa), and is shocked when the culprits turn out to be his brothers, Ravi (Ravi Teja) and Gopi (Venkat). Rajaram takes the incident lightly as the pranks of youngsters and is very lenient to them. With more such incidents, the two fall in love, with the brothers and sisters not far behind. 

On his brothers request, Rajaram approaches Devi for her sisters hands in marriage to his brothers. But she curtly refuses her consent on grounds that the two are wayward drunkards. Rajaram reacts sharply to it and vows to get his brothers married to her sisters. 

Since then on he is more exacting with his brothers and brings about a transformation in them. Devi agrees to their marriage. But the incorrigible brothers come in an inebriated condition for their engagement and Rajaram faces the brunt of Devis ire. She insults him and leaves with her sisters. An outraged Rajaram kicks his brothers out of home. 

The two brothers being state rank holders in engineering find favor with a businessman, Rangarao (Sarath Babu), who employs them. Rajaram, who is genuinely fond of his brothers, sells away his property and gives money secretly to Rangarao, paving way for a partnership deal between Rangarao and his brothers. Piqued at being kicked out of home, his brothers, unaware of his hand in securing the partnership for them, nurse a grudge against their brother and insult Rajaram in Devis presence. She, being in the know, slaps them and opens their eyes by informing them of his sacrifice. This brings about a total change in them and the repentant duo go to Rangarao to restore the property to their brother. 
 Bhupinder Singh), a criminal who was caught in the police dragnet with Rajarams help and later died. This is Rangaraos vendetta. In the ensuing melee, Rangarao is killed. Rajaram and his brothers reunite.

The film ends with all the three brothers getting married; Rajaram with Devi, Ravi with Latha, and Gopi with Geetha

Soundarya did her role as a responsible elder sister well. At the same time she is at her glamour best when it comes to hip shaking with Chiru in the songs. 

==Cast==
*Chiranjeevi as Rajaram
*Ravi Teja as Ravi
*Soundarya as Devi
*Venkat as Gopi Chandni as Lata
*Sishwa	as Geeta
*Kota Srinivasa Rao as Babai
*Sarath Babu as Rangarao Bhupinder Singh as Chinna Rao
*Gangadhar Panday as Personal Secretary Simran bagga in an item song

==Soundtrack==
{{Infobox album 
| Name = Annayya
| Caption = Album cover
| Artist = Mani Sharma
| Type = Soundtrack
| Cover = 
| Released = 
| Recorded = 1999-2000
| Genre = Film soundtrack
| Length = 24:38
| Label = Sabdaalaya Audio
| Producer = Mani Sharma
| Last album = Yuvakudu (2000)
| This album = Annayya (2000)
| Next album = Azad (film)|Azad (2000)
}}

Mani Sharma composed the Songs while Veturi Sundararama Murthy, Bhuvanachandra, Jonnavittula, Vennelakanti penned the Lyrics. The audio was an Instant Hit and the songs were Chart busters.

{| class="wikitable" style="width:70%;"
|-
! Song Title !! Singers !! Lyricist
|-
| Sayyare Sayya || S. P. Balasubrahmanyam || Vennelakanti
|-
| Hima Seemallo|| Hariharan (singer)|Hariharan, Harini || Veturi Sundararama Murthy
|- Sujatha || Veturi Sundararama Murthy
|- Sujatha || Veturi Sundararama Murthy
|-
| Baava Chandamaamalu || S. P. Balasubrahmanyam, K. S. Chithra|Chitra, S. P. B. Charan, Teja || Jonnavittula
|-
| Aata Kaavala || Sukhwinder Singh, Radhika || Bhuvanachandra
|}

==References==
 

== External links ==
* 

 

 
 