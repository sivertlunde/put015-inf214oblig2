Super Police
{{Infobox film
| name           = Super Police
| image          = Super Police Telugu Poster.jpg
| caption        =
| director       = K. Murali Mohan Rao
| producer       = Daggubati Suresh Babu|D. Suresh
| writer         = Venkatesh Nagma Soundarya
| music          = A.R. Rahman 
| cinematography =
| editing        =
| studio         = Suresh Productions
| distributor    =
| released       = 23rd June 1994 
| runtime        = 2:30:33
| country        = India Telugu
| budget         =
}} Telugu film produced by Daggubati Suresh Babu|D. Sureshon Suresh Productions banner, directed by K. Murali Mohan Rao. The film stars Daggubati Venkatesh|Venkatesh, Nagma, Soundarya in lead roles and music composed by A.R. Rahman. It was dubbed and released in Tamil with the same name, it was also dubbed into Hindi as Khel Khaladi Ka and released in 1997. This was the second film that A.R. Rahman composed music for a direct Telugu film and the first time that he composed the entire soundtrack for a Telugu film. The film recorded as flop at box-office.

==Plot==
Inspector Vijay (Daggubati Venkatesh|Venkatesh) a honest police officer, his past love Bharathi (Soundarya) a journalist, died in a road accident and from that day he became a drunker. Vijay comes on rental to the house of a journalist Renuka (Jayasudha) who had been divorced from her husband SP Prakash (Ahuti Prasad) because of his turpitude character and lives with her three children. Vijay comes closer to Renukas family and develops attachment with children. Abbanna (Kota Srinivasa Rao) a big shot in society, but he is big smuggler & gangster and also have underground contacts with terrorists, Home Minister (Chalapathi Rao) and SP Prakash are also their henchmen. Roja (Nagma) a petty thief falls in love with Vijay.

Vijay always gives a tough fight to Abbanna & gang and also makes his daughters marriage with her lover without his permission due to which Abbanna develops enmity on Vijay. Meanwhile, Vijay gather proofs against Abbanna & gang with help of Renuka, so they plan to kill Vijay. Abbanna keeps bomb in Vijays house but unfortunately Renuka dies in the bomb blast. In that anger Vijay shows his violence on Abbanna & gang while they are doing a ceremonial ritual, that leads to his suspension. At the same time SP Prakash throws out his three children from their house, Vijay adopts them. Suddendly Abbanna & gang attack Vijay and beat him very badly and he is hospitalized. Roja takes care of Vijay & children and he too starts loving her.

After speedy recovery, Vijay spots a handicapped person Ebrahim (Siva Krishna) who shots Abbanna in a meeting to kill. Abbannas men encounters on Ebrahim, Vijay stonewalls him. Vijay inquires Ebrahim regarding his animosity towards Abbanna, then Ebrahim reveals the past. Two years back he was watchman in Abbannas factory. One day he observes Abbannas secret allegations with terrorists and he informs Police & Press. Meanwhile, Abbanna kills Ebrahims family and makes him handicapper, incidentally Bharathi (Vijays love) arrives there and covers the entire episode in a video. They chase and kill her and created it as an accident, then Vijay understands that Bharathi didnt die in an accident and he also comes to know Bharathi has secretly hidden the video cassette. Vijay succeeds to acquire the cassette. Abbanna comes to know regarding this, he kidnaps children and blackmails Vijay for video cassette. Finally, Vijay protects the children, snaps all the baddies, handover them to police, gets back his job and marries Roja.

==Cast==
  Venkatesh  as Vijay
*Nagma as Roja
*Soundarya as Bharathi
*D. Ramanaidu as Himself
*Kota Srinivasa Rao as Abbanna
*Jayasudha as Journalist Renuka
*Brahmanandam as Citizen Ali as Constable AVS as Constable
*Ahuti Prasad as S.P. Prakash
*Chalapathi Rao as Home Minister Rallapalli as Pothuraju
*Gajar Khan as Terrorist Khan
*Siva Krishna as Ebrahim
*Devadasu Kanakala as I.G.
*Kota Shankar Rao as Thief
*Ashok Kumar as Constable
*Vidya Sagar as Intelligence Officer Rajendra Varma 
*Ananth as Priest Jenny as Prisoner
*Annuja as Prakashs Keep
*Master Baladitya as Sunny
*Master Kiran as Bunny
*Baby Sunaiya as Nanny
 

==Soundtrack==
{{Infobox album |
  Name        = Super Police |
  Type        = soundtrack |
  Artist      = A.R. Rahman |
  Cover       =|
  Background  = gainsboro |
  Released    = 1994|
  Recorded    = Panchathan Record Inn|
  Genre       = Film soundtrack |
  Length      =  25:50|
  Label       =  Suresh Productions |
  Producer    =  V. C. Guhanathan |  
  editing        = R.T Annadurai |
  Reviews     =  |
  Last album  =  Vandicholai Chinraasu  (1994) |
  This album  = Super Police  (1994) |
  Next album  =  Duet (1994 film)|Duet  (1994) |
}}

Telugu version (Super Police). Music composed by A.R. Rahman. Music released on MAGNA SOUND Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:50
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Super Police
| lyrics1 = Veturi Sundararama Murthy
| extra1  = Suresh Peters, Anupama (singer)|Anupama, Swarnalatha
| length1 = 5:40

| title2  = Baboo Love Cheyyara
| lyrics2 = Veturi Sundararama Murthy SP Balu, Chitra
| length2 = 5:07

| title3  = Telu Kuttina Thenalilo
| lyrics3 = Veturi Sundararama Murthy Sujatha
| length3 = 5:04

| title4  = Pakka Gentleman Ni"
| lyrics4 = Veturi Sundararama Murthy SP Balu, S. Janaki
| length4 = 5:31

| title5  = Mukkambe Mukkambe
| lyrics5 = Sahiti Chitra
| length5 = 4:27
}}

Tamil version (Super Police)

All lyrics written by Vairamuthu, all music composed by A. R. Rahman.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s) !! Length
|-
| "Muthamma Muthamma"
| Mano (singer)|Mano, K. S. Chithra
| 4:27
|-
| "Maamo Love"
| S. P. Balasubrahmanyam, K. S. Chithra
| 5:07
|-
| "Pakka Gentleman"
| S. P. Balasubrahmanyam, S. Janaki
| 5:31
|-
| "Chinnakuttiyo Chingariyo" Sujatha
| 5:04
|-
| "Sundara En Sundara"
| Swarnalatha, Anupama (singer)|Anupama, Suresh Peters
| 5:40
|}
Hindi (Khel Khiladi Ka)

All lyrics written by Mehboob, all music composed by A. R. Rahman.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s) !! Length
|-
| "Chumma De Chumma De"
| S. P. Balasubrahmanyam, K. S. Chithra
| 4:27
|-
| "Dil Se Dil Zara Mila Hi Lo"
| Udit Narayan, Alka Yagnik
| 5:04
|-
| "Khel Hai Yeh"
| Shweta Shetty, Suresh Peters, Swarnalatha
| 5:40
|-
| "Pakka Dilbar Jaani"
| Hariharan (singer)|Hariharan, Asha Bhonsle
| 5:31
|-
| "Babu Lo Chal Gaya"
| S. P. Balasubrahmanyam, Kavita Krishnamurthy, A.R.Rahman
| 5:37
|}

==External links==
* 

 

 
 
 
 