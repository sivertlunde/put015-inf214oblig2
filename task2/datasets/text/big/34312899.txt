Wings of Life
{{Infobox film
| name           = Wings of Life
| image          = Wings of Life poster.jpg
| caption        = North American release poster
| director       = Louis Schwartzberg
| producer       = Grady Candler Alix Tidmarsh
| writer         = 
| narrator       = Mélanie Laurent  (France)  Meryl Streep  (United States)  Bliss
| cinematography = 
| editing        = Jonathan P. Shaw
| studio         = Disneynature Blacklight Films Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 77 minutes
| country        = France United States
| language       = English French
| budget         = 
| gross          = 
| website                = http://nature.disney.com/wings-of-life
| website_title          = Official Website
}}
Wings of Life, known as Pollen in  , is a 2011 French-American nature documentary directed by Louis Schwartzberg and released by Disneynature. It was released theatrically in France on March 16, 2011 with narration by Melanie Laurent and in home media markets across the U.S. on April 16, 2013 with narration by Meryl Streep.

==Synopsis==
The films synopsis states: 
 

==Production==
 

==Release==
The film was released on Blu-ray and DVD by Walt Disney Studios Home Entertainment in North America as Wings of Life on April 16, 2013. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 