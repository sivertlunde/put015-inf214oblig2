Dear Heart
 
{{Infobox film
| name           = Dear Heart
| image          =
| caption        =
| director       = Delbert Mann
| producer       = Martin Manulis
| writer         = Tad Mosel
| starring       = Glenn Ford Geraldine Page Angela Lansbury Barbara Nichols
| music          = Henry Mancini
| cinematography = Russell Harlan
| editing        = Folmar Blangsted
| distributor    = Warner Bros.
| released       =  
| runtime        = 113 min
| country        = United States English
}}
 Dear Heart", was nominated for the Academy Award for Best Original Song.

==Plot==
Evie Jackson (Geraldine Page) is a middle-aged, single postmaster who is attending a postmasters convention in New York City. Honest and somewhat tactless, she has many friends but pines for love. Harry Mork (Glenn Ford) is a womanizing advertising executive who is staying in the same hotel while he finds an apartment. Mork is engaged to Phyllis (Angela Lansbury), a woman from Altoona, Pennsylvania. Mork has been told that Phyllis has a son, whom he assumes from a photo is about 13 years old. But the photo is old and her son, Patrick (Michael Anderson, Jr.), is actually 18 years old and a bohemian. Mork is surprised to discover Patricks true age. He is also embarrassed by Patricks casual attitude toward women and nudity, and suspicious of Patricks seemingly platonic relationship with fellow student Émile Zola Bernkrand (Joanna Crawford).

Evies numerous friends (including Miss Fox, Miss Tait, and Miss Moore—a trio of spinster postmasters) try to draw her into the parties and events of the postmasters convention, but Evie wants to find romance. She uses various means to make herself seem important to strangers, and meets Mork in the lobby of the hotel. Patrick helps Mork find an apartment in Greenwich Village. Mork is more interested in June Loveland (Barbara Nichols), the buxom blond who staffs the stationery store in the hotels lobby, than he is with Evie. Evie and Mork have lunch together, but Mork ignores Evie in favor of spending a few hours making love with Loveland in a hotel across the street.

The following day, Mork takes Evie to see his new apartment. She talks about married life. Later, Evie spends the evening shopping and seeing sights. She has some upsetting encounters in the hotel late at night, and Mork rescues her from a man with improper attentions toward her. The following day, while Evie attends her conference, Mork meets with his fiancée, Phyllis. Mork has begun to realize he is tired of womanizing and wants a home and a wife. Phyllis sees Mork as someone who can be a father-figure to Patrick and straighten him out, and who can give her the "good life" (free of cooking and cleaning) of hotel living.

Mork breaks off his engagement to Phyllis, realizing he loves Evie. Evie, convinced she is unwanted by any man, leaves for home. Mork has her paged in the railroad station, and they reunite.

==Cast==
* Glenn Ford as Harry Mork
* Geraldine Page as Evie Jackson
* Angela Lansbury as Phyllis
* Michael Anderson, Jr. as Patrick
* Charles Drake as Frank Taylor Richard Deacon as Cruikshank
* Barbara Nichols as June Loveland
* Mary Wickes as Miss Fox
* Ruth McDevitt as Miss Tait
* Alice Pearce as Miss Moore
* Joanna Crawford as Émile Zola Bernkrand
* Patricia Barry as Mitchell
* Neva Patterson as Connie Templeton

==Production==
Dear Heart was written by Tad Mosel, from his own story. Crowther, Bosley. "Screen: Dear Heart Is at Music Hall." New York Times. March 8, 1965. 

The film had a budget of about $1.8 million. Perry, Eleanor. "Martys Spoiled Rich Sister." Life (magazine)|Life. February 5, 1965, p. 18.  Principal shooting occurred from October 3 to November 22, 1963.  It was Geraldine Pages first solo leading lady role.  Ford was convinced that actress Hope Lange was the only woman who could make him happy. As the production commenced, however, Lange married producer Alan Pakula. Ford was heartbroken. Although production of Dear Heart was generally a positive one, Ford could not stop brooding over Lange.  Angela Lansbury took the role of the materialistic but good-hearted Phyllis because it gave her an opportunity to work with Geraldine Page. Edelman and Kupferberg, p. 174. 

Henry Mancini was hired to compose music for the film. Mancini felt such a gentle romantic film deserved a theme song. He quickly wrote music for the song, but it lacked lyrics. Mancini contacted Johnny Mercer, who was unavailable. So Mancini turned to Jay Livingston and Ray Evans. The lyricists read the script, and came up with the lyrics and title for the song based on their reading of Geraldine Pages character. Sackett and Rovins, p. 177.  The films original title was The Out-of-Towners. But Jay Livingston said that when Martin Manulis heard the theme song, he changed the title to Dear Heart. 

Warner Brothers was uncertain about when to release the film.  Mancini, who had a 50 percent interest in the films theme song with Larry Shayne, asked studio head Jack Warner to release the film so that it would qualify for the March 1965 Academy Awards. Warner agreed to release it for a week in Los Angeles (which, under Academy rules, would qualify it for the Oscars), if Mancini and Shayne would pay for the local advertising. Since this would cost just $10,000, Mancini and Shayne agreed to do so. 

The film premiered on December 3, 1964, in Los Angeles, California, to qualify it for the 1964 awards season. It made its general release premiere at Radio City Music Hall in New York City, New York, on March 8, 1965. 

==Reception== New York Times, called it "a stale, dull and humorless pretension at what its producers dare to describe as gay, sophisticated comedy, and it makes almost scandalous misuse of the recognized talents of Geraldine Page."  Eleanor Perry, writing for Life (magazine)|Life, felt that although the film was aimed at average people, it was condescending and patronizing to them. 

Writing in 2008, Leonard Maltin felt it had "excellent characterizations" and a solid supporting cast.  Film historians Rob Edelman and Audrey Kupferberg felt Lansburys performance was thoughtful. 

==Awards== Oscar nomination Best Original Song "Dear Heart." The music was composed by Henry Mancini, with lyrics by Jay Livingston and Ray Evans. 

==References==
 

==Bibliography==
*Edelman, Rob and Kupferberg, Audrey E. Angela Lansbury: A Life on Stage and Screen. Thorndike, Maine: G.K. Hall & Co., 1996.
*Ford, Peter. Glenn Ford: A Life. Madison, Wisc.: University of Wisconsin Press, 2011.
*Maltin, Leonard. Leonard Maltins 2009 Movie Guide. New York: Plume/Penguin, 2008.
*Mancini, Henry and Lees, Gene. Did They Mention the Music? New York: Cooper Square Press, 2001.
*Matthews, Charles. Oscar A to Z: A Complete Guide to More Than 2,400 Movies Nominated for Academy Awards. New York: Doubleday, 1995.
*Monush, Barry. Screen World Presents the Encyclopedia of Hollywood Film Actors. New York: Applause Theatre and Cinema Books, 2003.
*Sackett, Susan and Rovins, Marcia. Hollywood Sings!: An Inside Look at Sixty Years of Academy Award-Nominated Songs. New York: Billboard Books, 1995.
*Shelley, Peter. Grande Dame Guignol Cinema: A History of Hag Horror From Baby Jane to Mother. Jefferson, N.C.: McFarland & Company, 2009.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 