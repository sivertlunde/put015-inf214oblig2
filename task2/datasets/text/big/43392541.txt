She Must Be Seeing Things
{{Infobox film
| name           = She Must Be Seeing Things
| image          = She Must Be Seeing Things.jpeg
| alt            = 
| caption        = 
| film name      = 
| director       = Sheila McLaughlin
| producer       = Christine Le Goff
| writer         = 
| screenplay     = Sheila McLaughlin
| story          = 
| based on       =  
| starring       = Lois Weaver and Sheila Dabney
| narrator       = 
| music          = John Zorn
| cinematography = Mark Daniels and Heinz Emigholz
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = US
| language       = English
| budget         = 
| gross          = 
}}

She Must Be Seeing Things is a 1987 lesbian feminist film by Sheila McLaughlin. It was the film debut of both Lois Weaver and Peggy Shaw.    It was highly controversial when first released.

==Plot==
 .]]
The film focuses on the relationship between filmmaker Jo (Lois Weaver) and her girlfriend, lawyer Agatha (Sheila Dabney). While tidying the books and papers in Jos flat, Agatha finds suggestive photos of Jo and her former (male) lovers. She also finds a diary, with more photos and pages about men that Jo has known in the past. Agatha becomes suspicious that Jo is seeing a man as well as her, and even appears to follow her.

The film is ambiguous about whether Agatha is imagining events, or whether there really are things going on that she must be seeing. Despite the strains of an arduous filming schedule for Jos low-budget film, they are happily reconciled by the end, although  the issue of whether Agatha was imagining things is apparently left unresolved. Jos film-within-a-film involves the life of Catalina de Erauso (c. 1592–1650), a semi-legendary personality from 17th century Spain. Catalina was given a special dispensation by Pope Urban VII to live, work and dress as a man.   

The Catalina story to some extent mirrors the butch-femme aspect of Jo and Agathas relationship.

==Critical reception==
The film has elicited a wide range or responses, from delight to revulsion and outrage.   

Janet Maslin writing in the The New York Times notes that "sexual jealousy is the main force at work ... but youd never know it from the films amiable, even dispassionate mood", and called it "a modest, agreeable and doubtless very personal film".   

Teresa de Lauretis considers the film to be about "lesbian sexuality and its relations to fantasy".    De Lauretis also notes the ambiguity inherent in the title: is she imagining things, or is it that she must be seeing things &ndash; are there things that she ought to be seeing?   

Alison Darrens Lesbian Film Guide notes that it was a "deeply controversial film on release ... upset a wide section of the lesbian community by being a lesbian film largely concerned with heterosexuality ... dismissed outright by some as pornography".   

==Cast==
*Lois Weaver as Jo
*Sheila Dabney as Agatha
*Kyle deCamp as Catalina 
*John Erdman as Eric 
*Ed Bowes as Richard
*Peggy Shaw as Jewelry Saleswoman

==References==
 

==External Links==
* 

 
 
 
 
 
 
 
 