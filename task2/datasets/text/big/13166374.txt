Mortuary (1983 American film)
 
{{Infobox Film
| name           = Mortuary
| caption        = 
| image	=	Mortuary FilmPoster.jpeg
| director       = Howard Avedis
| producer       = Edward L. Montoro/Marlene Schmidt
| writer         = Howard Avedis 
| starring = {{Plainlist| Mary McDonough   David Wallace
* Bill Paxton
* Lynda Day George
* Christopher George
}} 
| music          = John Cacavas
| cinematography = Gary Graver
| editing        = Stanford C. Allen
| distributor    = Citadel Films/Artists Releasing Corporation (ARC)/Film Ventures International (FVI)/Vestron Video
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| gross          = $4.3 million 
}}
 slasher film/Horror horror movie Mary Beth David Wallace, Lynda Day George, with Michael Berryman (who only appears in the films trailer) and Christopher George in one of his final roles. It centered on a hooded face painted killer who stabbed or impaled his victims with/on an embalming trocar. On May 25, 2012 the film was release for the first time on DVD.

==Synopsis==
Christie Parson (Mary McDonaugh) is mourning the death of her father (Danny Rogers) by drowning. Her mother has convinced herself it was a tragic accident, but Christie is sure it was murder.  Christie suffers from nightmares in which a hooded figure, clutching an embalming trocar, pursues her. She turns detective, aided by her boyfriend (David Wallace) to find out the truth.  Her sleuthing draws her to a local mortuary, whose owner, Hank Andrews (Christopher George), together with his secretly demented offspring, Paul, is guarding an odious secret.
==DVD release==
On May 15th 2012, the film was finally transferred to dvd with a 16×9 (1.78:1) HD master from the original Inter-Negative. Scorpio Releasing in conjunction with Camelot Entertainment released the dvd with special features. The special features included, Play with or without the “Nightmare Theater” experience, On camera interview with composer John Cacavas and the original trailer.  
==Blu-Ray release==
On October 7th 2014, Scorpion Releasing released the film on blu-ray a Limited Edition with only 1200 copies.

==Cast== Mary Beth McDonough as Christie Parson  David Wallace as Greg Stevens
* Bill Paxton as Paul Andrews 
* Christopher George as Hank Andrews
* Lynda Day George as Eve Parson
* Bill Conklin as Sheriff Duncan
* Curt Ayers as Jim
* Donna Garrett as Mrs. Andrews
* Danny Rogers as Dr. Parson
* Greg Kaye as Mark
* Denis Mandel as Josh

==External links==
* 
*  

==Reference list==
 

 
 
 
 


 