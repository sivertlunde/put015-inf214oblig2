My Weakness (film)
{{Infobox film
| name = My Weakness 
| image =
| image_size =
| caption = David Butler
| producer = Buddy G. DeSylva 
 | writer = Buddy G. DeSylva    Bert Hanlon    David Butler
| narrator = Charles Butterworth   Harry Langdon
| music = Arthur Lange    Cyril J. Mockridge 
| cinematography = Arthur C. Miller 
| editing = Irene Morra     
| studio = Fox Film Corporation 
| distributor = Fox Film Corporation 
| released = September 22, 1933
| runtime = 73 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} David Butler Charles Butterworth.  It was the second of four films made by the German actress Harvey in Hollywood, who had emerged as major star during Weimar Germany.

==Cast==
*  Lilian Harvey as Looloo Blake  
* Lew Ayres as Ronnie Gregory   Charles Butterworth as Gerald Gregory 
* Harry Langdon as Dan Cupid  
* Sid Silvers as Maxie  
* Irene Bentley as Jane Holman  
* Henry Travers as Ellery Gregory  
* Adrian Rosley as Baptiste   Mary Howard as Diana Griffith  
* Irene Ware as Eve Millstead 
* Barbara Weeks as Lois Crowley  
* Susan Fleming as Jacqueline Wood  
* Marcelle Edwards as Marion  
* Marjorie King as Lillian  
* Jean Allen as Consuello  
* Gladys Blake as Mitzi  
* Dixie Francis as Dixie

==References==
 

==Bibliography==
* Solomon, Aubrey. The Fox Film Corporation, 1915-1935: A History and Filmography. McFarland, 2011.

==External links==
* 

 

 
 
 
 
 
 
 

 