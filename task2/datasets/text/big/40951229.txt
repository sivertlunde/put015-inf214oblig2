Spooks: The Greater Good
 
 
{{Infobox film
| name           = Spooks: The Greater Good
| image          = Spooks The Greater Good film poster.png
| alt            = 
| caption        = Film poster
| director       = Bharat Nalluri   
| producer       = {{Plainlist|
* Jane Featherstone | Ollie Madden 
* Ollie Madden}}
| writer         = {{Plainlist|
* Jonathan Brackley 
* Sam Vincent }}
| starring       = {{Plainlist|
* Peter Firth
* Kit Harington
* Jennifer Ehle
* Elyes Gabel
}}
| music          = Dominic Lewis
| cinematography = Hubert Taczanowski
| editing        = Jamie Pearson
| production companies = {{Plainlist|
* Isle of Man Film Kudos Film and Television Pinewood Pictures}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes  
| country        = United Kingdom English
| budget         = 
| gross          = 
}}

Spooks: The Greater Good is a forthcoming British spy film, scheduled to be released on 8 May 2015,  continuing from the British spy series Spooks (known as MI-5 in some countries), which aired on BBC One from 2002 to 2011.  Jonathan Brackley and Sam Vincent wrote the script for the film, with Bharat Nalluri directing. Peter Firth reprises his role as Harry Pearce, who appeared in all ten series of the programme.  Kit Harington and Jennifer Ehle also star.

==Plot==
During a handover to the head of counter-terrorism of MI5 Harry Pearce (Peter Firth), terrorist Adem Qasim (Elyes Gabel) escapes from MI5 custody.  When Harry disappears soon after, his protégé Will Holloway (Kit Harington) is tasked with finding out what happened as an impending attack on London looms. Holloway eventually uncovers a conspiracy that runs from Vietnam to the Mediterranean. 

==Cast==
 
* Peter Firth as Harry Pearce   
* Kit Harington as Will Holloway   
* Jennifer Ehle as Geraldine Maltby 
* Elyes Gabel as Adem Qasim 
* Lara Pulver as Erin Watts   
* Tim McInnerny as Oliver Mace 
* Hugh Simon as Malcolm Wynn-Jones 
* Eleanor Matsuura as Hannah Santo
* Tuppence Middleton as June
* Elliot Levey as Emerson
* Michael Wildman as Robert Vass

Keeley Hawes, who played Zoe Reynolds in the first three years of the programme, may return to play that role. 

==Production==
Plans of the film were first revealed to be in the works by Firth in March 2013, with a script in development. Firth stated that although "they should make it," he also thought it would cost a lot, "and theres not a lot of money to go round at the moment."  The film was officially announced on 1 November 2013. Jonathan Brackley and Sam Vincent, who took over as head writers for the last two years of the Spooks television series, wrote the script. It would be directed by Bharat Nalluri, who previously directed the first episodes of the programme. Jane Featherstone and Stephen Garrett of Kudos (production company)|Kudos, and Ollie Madden of Shine Group would produce the film.      

Principal photography commenced in March 2014 at locations including Berlin, Moscow, Isle Of Man and London as well as Pinewood Studios.  A trailer for the film was released on 27 March 2015.  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 