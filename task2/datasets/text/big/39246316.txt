The Secret Life of Walter Mitty (2013 film)
 
{{Infobox film
| name           = The Secret Life of Walter Mitty
| image          = The Secret Life of Walter Mitty poster.jpg
| image_size     = 220px
| border         = yes
| alt            = A side profile of a man running with a silver briefcase in hand. Behind him a cityscape.
| caption        = Theatrical release poster
| director       = Ben Stiller
| producer       = {{Plain list |
* Samuel Goldwyn, Jr.
* John Goldwyn
* Stuart Cornfeld
* Ben Stiller
}}
| screenplay     = Steve Conrad
| based on       =  
| starring       = {{Plain list |
* Ben Stiller
* Kristen Wiig
* Shirley MacLaine Adam Scott
* Kathryn Hahn
* Sean Penn
 
}} Theodore Shapiro 
| cinematography = Stuart Dryburgh
| editing        = Greg Hayden
| studio         = {{Plain list |
*Samuel Goldwyn Films
*Red Hour Films
*Blind Wink
*New Line Cinema 
*TSG Entertainment
}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 114 minutes  
| country        = {{Plain list |
*Australia
*Canada
*United Kingdom
*United States 
}}
| language       = English
| budget         = $90 million   
| gross          = $188,133,322      
}} romantic comedy-drama adventure cult short story 1947 version was produced by Samuel Goldwyn and directed by Norman Z. McLeod, with Danny Kaye playing the role of Walter Mitty.  The film premiered at the New York Film Festival on October 5, 2013.  The Secret Life of Walter Mitty was theatrically released on December 25, 2013 in North America, to generally mixed reception, and was a moderate box office success. 

==Plot==
 
Walter Mitty is a  " of Life and that it should be used for the cover of the Life (magazine)#As a monthly (1978–2000)|magazines final print issue as it converts to online status. The negative is missing, however, and Walter is forced to stall for time with corporate transition manager Ted Hendricks, who is handling the downsizing. While viewing the other negatives outside Lifes offices, Cheryl approaches Mitty and suggests that he think of the negatives as clues to Seans location. They look at three of them, including one of a persons thumb with a unique ring on it, and another of a curved piece of wood. A third picture of a boat leads Mitty to determine that OConnell is in Greenland. Mitty promptly flies there to find him.
 Space Oddity", gains a new confidence and boards the helicopter. Nearing the ship, Mitty learns the helicopter cannot land upon it. Misunderstanding the pilot, instead of jumping into a dinghy boat nearing to catch him, Mitty aims for the main vessel and misses and falls in the ocean. He splashes down into ice-cold, shark-infested waters, losing a box of ship-to-shore radio components before being brought aboard.

Mitty learns that OConnell departed the ship earlier. The crew offers him some cake OConnell left behind; Mitty discovers OConnells destinations in the wrapping paper. The itinerary directs Mitty to Iceland, where OConnell is photographing the volcano Eyjafjallajökull, and he arrives at the local village of Skógar near the volcano using a Longboard (skateboard)|longboard, and notices OConnell on the plane flying near the volcano. An eruption forces Mitty to flee, and as there is nothing left for him to do, he obeys a text message recalling him to New York.

For failing to recover the negative, his first failure in a long career with the magazine, Mitty is fired. He learns that Cheryl, who was let go earlier, seems to have reconciled with her estranged husband. Mitty returns home discouraged, throwing away the wallet when he visits his mother. To his surprise, Mitty recognizes the curve of the piano in his mothers house while looking at the last photograph. When asked, Mittys mom mentions having met OConnell. She had told Mitty before but he was daydreaming and failed to hear her.

Mitty discovers OConnell is in the Himalayas, and finds him photographing a rare   who has kept in contact during Mittys adventures.

While helping his mother sell her piano, Mitty recounts his story but mentions he does not have the wallet anymore. His mother says she always keeps his knickknacks and gives him the wallet that she retrieved from the trash. An emboldened Mitty delivers the negative to Life magazine, tells management that it was the photograph OConnell wanted for the final issue, and berates Hendricks for disrespecting the staff that made the magazine so honored before walking away from the office.

Mitty reunites with Cheryl and learns that Cheryls ex-husband was only at her house to repair the refrigerator. Mitty tells Cheryl of his adventures and admits that he still does not know what negative #25 shows. Mitty and Cheryl see the final issue of Life at a newsstand, with its cover dedicated to the staff. It is accompanied by the photograph from negative #25, showing Mitty sitting outside of the Life building, examining a contact sheet. Mitty and Cheryl continue their walk down the street holding hands.

==Cast== negative asset manager at Life (magazine)|Life
* Kristen Wiig as Cheryl Melhoff, Walters love interest and co-worker
* Shirley MacLaine as Edna Mitty, Walters mother Adam Scott as Ted Hendricks, Walters new boss
* Kathryn Hahn as Odessa Mitty, Walters sister
* Sean Penn as Sean OConnell, a photojournalist
* Patton Oswalt as Todd Maher, an eHarmony customer service representative
* Ólafur Darri Ólafsson as a Greenlandic helicopter pilot Jon Daly as Tim Naughton, one of Walters co-workers
* Terence Bernie Hines as Gary Mannheim, one of Walters co-workers
* Adrian Martinez as Hernando, Walters understudy and co-worker
* Kai Lennox as Phil Melhoff, Cheryls ex-husband
* Conan OBrien as himself
* Andy Richter as himself
* Joey Slotnick as a retirement home administrator

==Production==

===Development=== father produced 1947 film remake rights, The Mask (both 1994).    The studio bought the rights in 1995 with the understanding that The Samuel Goldwyn Company would be involved in creative decisions.  Babaloo Mandel and Lowell Ganz turned in the first draft of the screenplay in July 1997. Ron Howard entered negotiations to direct that same month, as well as cover producing duties with Brian Grazer and Imagine Entertainment.    Howard and Imagine Entertainment eventually left the project in favor of EDtv,  and The Secret Life of Walter Mitty languished in development hell over the challenges of using a contemporary storyline. 
 script and serve as Howards replacement. Filming was set to begin in early-2000, but was pushed back.  Around this time, Peter Tolan worked on rewrites.  In May 2001, Goldwyn filed a lawsuit against New Line for breach of contract. Goldwyn claimed that the studio extended their 1995 deal until May 2001, but then announced that it wanted to transfer the rights for the remake to another company and have Goldwyn surrender his creative input.    In November 2002, New Line was forced to revert the film rights back to Goldwyn, who won his lawsuit and took the property to Paramount Pictures.  During pre-production discussions between Paramount and DreamWorks on Lemony Snickets A Series of Unfortunate Events (which starred Carrey), Steven Spielberg, head of DreamWorks, rekindled interest in working with Carrey; the duo previously considered Meet the Parents, but the outing fell apart.  In May 2003, Spielberg agreed to direct,  and brought in DreamWorks to co-finance The Secret Life of Walter Mitty with Paramount (which would acquire DreamWorks in 2006). 
 War of Mark Waters to direct LaGraveneses script for Walter Mitty,  but Carrey had to drop out due to scheduling conflicts.    He was soon replaced by Owen Wilson. 
 turnaround in Mike Myers was attached to star in the title role. Jay Kogen was hired to write a new script that would be specifically tailored for Myers. 

In April 2010, Sacha Baron Cohen was offered and accepted the lead role.  Later that month, The Pursuit of Happyness writer Steven Conrad was hired to pen the screenplay,  with Gore Verbinski announced as director in June 2010.   

In April 2011, it was announced that Ben Stiller had been cast in the lead role, though no director was attached.  The following July, it was announced that Stiller was also due to direct the film. 

===Pre-production=== Adam Scott joined the film.   In April 2012, Kathryn Hahn was cast as Odessa, Walters sister, and Josh Charles was cast as the ex-husband of Kristen Wiigs character,    though he was replaced by Kai Lennox. Later that month, Sean Penn was cast in what was described as a "small but pivotal supporting role"    as photojournalist Sean OConnell.

The portions of the film set in Nuuk, Greenland, were in fact shot in Stykkishólmur, a village on the Snæfellsnes peninsula in Iceland, and Höfn, a village in southeast Iceland.  Later sequences set in Stykkishólmur were actually filmed in Seyðisfjörður.  The sequences where Walter Mitty follows Sean to Afghanistan were also filmed in Iceland, at the Skogafoss waterfall and in Vatnajökull National Park. 

==Distribution== theatrical trailer release in July, both of which began to spark awards speculation.    The trailer features a short segment of Queen (band)|Queens "Bohemian Rhapsody". The song is not used in the film.
 world premiere AFI Film Festival. 

==Marketing==
20th Century Fox hired filmmaker Casey Neistat to make a promotional video based on the theme of "live your dreams" but Neistat suggested instead to spend the budget on bringing disaster relief to the Philippines in the wake of Typhoon Haiyan. Fox agreed and gave him a budget of $25,000.  

==Reception==

===Critical response===
The film has received mixed reviews.  Review aggregation website   gives the film an averaged rating of 54/100 based on reviews from 39 critics, indicating "mixed or average reviews".    Audiences surveyed by CinemaScore gave the film a B+ rating. 
 The Curious Case of Benjamin Button shows the film could have been made funnier, but that the more serious emotional dimension ultimately makes the film feel more substantial. 

The film had its share of admirers, however. Peter Travers of Rolling Stone gave the film a positive review, saying "In his uniquely funny and unexpectedly tender movie, Stiller takes us on a personal journey of lingering resonance."  Joe Neumaier of New York Daily awarded the film five out of five stars, saying "The story Stiller tells manages to float in a most peculiar, satisfying way."  Political radio show host and film critic Michael Medved was also positive concerning the film, calling it "one of the  feel-good movies of the year."

The film received criticism from several sources for the product placement of several brands which featured prominently in the storyline, including Papa Johns, eHarmony, Time-Life and Cinnabon.  

===Box Office and Legacy===
Despite its polarizing reception and the lack of hype, the film was a moderate box office success, grossing $188,133,322 worldwide. Since its theatrical run, the film has developed a cult following, mainly amongst young adult film goers.

===Accolades===
 
{| class="wikitable"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Award/Film festival
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| National Board of Review  December 4, 2013
|  
| The Secret Life of Walter Mitty
|  
|-
| rowspan="2"| Satellite Awards  February 23, 2014 Best Cinematography
| Stuart Dryburgh
|  
|- Best Original Score Theodore Shapiro
|  
|-
| Location Managers Guild Awards 
| March 29, 2014
| Outstanding Location Feature Film
| The Secret Life of Walter Mitty
|  
|-
| Costume Designers Guild Awards 
| February 22, 2014
| Excellence In Contemporary Film
| Sarah Edwards
|  
|-
| rowspan="2"| Saturn Awards 
| rowspan="2"| 40th Saturn Awards|June, 2014
| Saturn Award for Best Fantasy Film
| The Secret Life of Walter Mitty
|  
|-
| Saturn Award for Best Actor
| Ben Stiller
|  
|-
| rowspan="2"| Key Art Awards http://www.imdb.com/title/tt0359950/awards 
| rowspan="2"| October, 2013
| Best Audio Visual Technique
| rowspan="3"| The Secret Life of Walter Mitty
|  
|-
| Best Trailer
|  
|-
| New York Film Festival 
| 2013
| Best Film
|  
|-
| Visual Effects Society Awards
| February 14, 2014
| Outstanding Supporting Effects in a Feature Motion Picture
| Guillaume Rocheron, Kurt Williams, Monette Dubin, and Ivan Moran
|  
|}

==References==

 

==External links==
 
*  
*  
*  
*  
*  
*   filming locations at Movieloci.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 