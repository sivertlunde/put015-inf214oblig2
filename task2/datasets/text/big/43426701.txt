A Master Builder
{{Infobox film
| name = A Master Builder
| director = Jonathan Demme
| writer = Henrik Ibsen Wallace Shawn
| starring = Wallace Shawn Julie Hagerty Andre Gregory
| editing = Tim Squyres
| cinematography = Declan Quinn
| released =   }}
| runtime = 130 minutes
| country = United States
| language = English
| gross =  $46,874 
}}
A Master Builder is a 2013 film directed by Jonathan Demme, based on The Master Builder by Henrik Ibsen. The film was released in the United States in June 2014 and stars Wallace Shawn, Julie Hagerty, and Andre Gregory. The film is a colour production of the Ibsen play dealing with the relationship between an aging architect and a younger woman. The play originally premiered in 1893.

== Plot ==
Halvard Solness (Shawn) is an aging architect of a small town in Norway who has managed to attain some distinction and local reputation, and long married to Aline (Hagerty). One day while having a visit from his friend Doctor Herdal (Pine), Solness is visited by Hilde Wangel (Joyce), a young woman of twenty-four from another town whom the doctor promptly recognizes from a recent trip. 

Soon after the Doctor leaves and Solness is alone with Hilde, she reminds him that they are not strangers and that they had previously met in her home town ten years ago when she was fourteen years of age. When Solness does not respond to her quickly enough she reminds him that at one point he had made advances upon her, offered a romantic interlude, and promised her "castles in the sky" during their encounter, which she believed. He denies this and she gradually convinces him, however, that she can assist him with his household duties and he takes her into his home.

During the construction of his most recent project which included towering steeples, Hilde learns that Solness suffers from acrophobia, a morbid fear of extreme heights, but nonetheless encourages him to climb the steeples to their very height at the public opening of the newly completed building. Solness, inspired by her words, begins his ascent to the top of the steeples and it is here that you come to find out that in fact, most of the movie has been played out in the head of Hilde, and that Solness had never left the bed from the first scene of the movie and passed away at the moment Hilde, in her head, believes he has Hung the Wreathe on the new house he built for his wife. This is symbolic in the sense that he had reached a peaceful and unbelievable resolution to his own happiness and to the happiness of those around him.

== Cast ==
* Wallace Shawn as  Halvard Solness 
* Julie Hagerty as Aline Solness 
* Lisa Joyce as Hilde Wangel 
* Larry Pine as Dr. Herdal 
* Andre Gregory as Knut Brovik 
* Emily Cass McDonnell as Kaia Fosli 
* Jeff Biehl as Ragnar Brovik 

== Production ==
Andre Gregory said that he was drawn to the play due to his age and the themes of redemption.   Gregory said of Joyce that she is "astounding" and hopes that her career takes off, and Demme said that they were all "blown away" by her performance. 

== Release ==
A Master Builder premiered at the Rome Film Festival under the title Fear of Falling.   Abramorama released the film in New York on July 23, 2014.  In February 2015, after a limited theatrical release in several cities in the United States, the film was released for distribution in streaming video through Amazon. 

== Reception == New York wrote that the film "brings the genius of Ibsen to the screen in a way I never thought was possible."   Jay Weissberg of Variety (magazine)|Variety called it "constricted and uninspired".   Stephen Holden of The New York Times wrote that Shawn "employs insidious sleight of hand to score moral points".   Jordan Mintzer of The Hollywood Reporter wrote, "Wallace Shawn shines in this well-acted piece of filmed theater."  The magazine Architectural Record gave the film a mixed review, singling out Julie Hagertys performance opposite Wallace Shawn as exceptional.   

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 