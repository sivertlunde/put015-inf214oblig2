Yatheem
{{Infobox film
| name = Yatheem
| image =
| caption =
| director = M. Krishnan Nair (director)|M. Krishnan Nair
| producer = TE Vasudevan
| writer = Moithu Padiyathu
| screenplay =
| starring = Sheela Adoor Bhasi Thikkurissi Sukumaran Nair Kottayam Santha
| music = MS Baburaj
| cinematography =
| editing =
| studio = Jaya Maruthi
| distributor = Jaya Maruthi
| released =   
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by TE Vasudevan. The film stars Sheela, Adoor Bhasi, Thikkurissi Sukumaran Nair and Kottayam Santha in lead roles. The film had musical score by MS Baburaj.   

==Cast==
  
*Sheela 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*Kottayam Santha 
*Prameela 
*Sankaradi 
*Sreemoolanagaram Vijayan
*Nilambur Balan 
*Sathar
*Unnimary 
*Bahadoor 
*K. P. Ummer  Khadeeja 
*Kunjava
*Nellikode Bhaskaran 
*Pala Thankam 
*Paravoor Bharathan 
*Philomina  Ravikumar 
*Santha Devi  Sudheer 
*Vanchiyoor Radha 
*Vidhubala 
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Allaavin Kaarunyamillenkil || K. J. Yesudas || P. Bhaskaran || 
|- 
| 2 || Innukaanum Ponkinakkal || Ambili || P. Bhaskaran || 
|- 
| 3 || Maanathu Sandhya || S Janaki, Chorus || P. Bhaskaran || 
|- 
| 4 || Manippiraave Ninte Kalithozhaninnu || Vani Jairam, LR Eeswari || P. Bhaskaran || 
|- 
| 5 || Neelamegha Maalikayil || P Jayachandran || P. Bhaskaran || 
|- 
| 6 || Pandu Pandoru Paadusha || P Susheela || P. Bhaskaran || 
|- 
| 7 || Pandu Pandoru Paadusha   || P Susheela || P. Bhaskaran || 
|- 
| 8 || Thankavarnappattudutha || LR Eeswari, Chorus || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 