Only the Strong (film)
 
{{Infobox film
| name        = Only the Strong
| image       = Only the Strong.jpg
| starring    =  
| director    = Sheldon Lettich
| writer      =  
| editing     = Stephen Semel
| music       = Harvey W. Mason
| producer    = Samuel Hadida
| studio      =  
| distributor =  
| released    = August 27, 1993   
| runtime     = 99 minutes
| language    = English
| budget      = $6,000,000 (US)
| gross       = $3,273,588 (US)   
|}}
 directed by Sheldon Lettich, starring Mark Dacascos. It is considered to be the only Hollywood film that showcases capoeira, an Afro-Brazilian martial art, from beginning to end.

==Plot== Green Beret Geoffrey Lewis), one of Stevens old teachers, sees the impact that Stevens has on the students. Kerrigan gives him the task of teaching Capoeira to a handful of the worst at-risk students at the school, giving Stevens an abandoned fire station as their dojo. While doing so, Stevens earns the ire of the local drug lord, Silverio (Paco Christian Prieto), whose younger cousin, Orlando (Richard Coca), is one of Stevens students. Silverio is also a master of Capoeira, and he engages Stevens in combat, beating him viciously. The horrified Orlando resolves to learn everything he can from Stevens. Stevens class learns quickly, and they become very skilled at Capoeira. The principal, delighted, proposes a district-wide Capoeira program to the school board. After a field trip with his class, Stevens once again crosses swords with Silverio, who declares war against him.

Silverios gang terrorizes the high school and sets fire to Kerrigans classroom, resulting in the death of one of Stevens students. As a result of this incident, Stevens is banished from the school grounds and the Capoeira program is terminated. In retaliation to the attack, Stevens sneaks into Silverios chop shop and defeats the workers before setting a cash-filled car on fire. Furious, Silverio orders the gang to bring Stevens to him alive. Orlando flees to get help. After a desperate battle, Stevens is finally captured and brought to a bonfire, where Silverio awaits. However, Stevens Capoeira students bar their path in an attempt to rescue their teacher. Before a brawl can ensue, the exhausted Stevens challenges Silverio to a single combat to win back his students. Despite a grueling battle, Stevens defeats Silverio before the police arrive, sending the gang scattering in all directions. With this defeat, Silverios reputation as crime lord is gone.

Stevens Capoeira program proves to be a success that his students graduate from high school. To celebrate, they join a Brazilian Capoeira team to perform for Stevens at the graduation ceremony.

==Cast==
* Mark Dacascos as Louis Stevens
* Stacey Travis as Dianna
* Paco Christian Prieto as Silverio Geoffrey Lewis as Mr. Kerrigan
* Todd Susman as Mr. Cochran
* Jeffrey Anderson-Gunter as Philippe
* Richard Coca as Orlando Aliveres
* Roman Cardwell as Shay
* Ryan Bollman as Donovan
* Christian Klemash as Eddie
* John Fionte as Hector Cervantes
* John Gregory Kasper as Coach Kasper
* Phyllis Sukoff as Mrs. Esposito
* Mellow Man Ace as the Student Rapper
* Sergio Kato as Silverios Bodyguard

==Production==
Though Mark Dacascos is a talented martial artist, his background is not initially in Capoeira.  Just prior to his audition, he received his training from famed Capoeirista Mestre Amen Santo, who was also responsible for much of the fight choreography.

== Soundtrack ==
 Jibril Serapis Bey, Ilesa Anago Africa and "Amen" Santo, along with hip-hop songs by Mellow Man Ace, Iki Levy, New Version of Soul and Miami Boyz. The most prominent songs in the film are Jibril Serapis Beys "Paranauȇ" and "Zoom-Zoom-Zoom" - the latter being more well known as the theme song for Mazdas TV commercials.   

{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Performers
| total_length    = 
| all_writing     = 
| all_lyrics      = 
| all_music       = 
| writing_credits = yes
| lyrics_credits  = 
| music_credits   = 
| title1          = Paranauê
| note1           = Produced by Kao Rossman, Stuart Shapiro
| writer1         = Kao Rossman, Stuart Shapiro
| lyrics1         = 
| music1          = 
| extra1          = Serapis Bey
| length1         = 3:10
| title2          = Miami Boyz
| note2           = Produced by Kid Fury of Bass Patrol
| writer2         = B. Graham, A. Cuff
| lyrics2         = 
| music2          = 
| extra2          = Miami Boyz
| length2         = 3:51
| title3          = Zoom-Zoom-Zoom
| note3           = Produced by Kao Rossman
| writer3         = Kao Rossman
| lyrics3         = 
| music3          = 
| extra3          = Serapis Bey
| length3         = 2:53
| title4          = Comin Together
| note4           = Produced by Scott G., Iki Levy
| writer4         = Iki Levy, Scott G.
| lyrics4         = 
| music4          = 
| extra4          = -
| length4         = 1:37
| title5          = Babalu Bad Boy
| note5           = Produced by DJ Muggs
| writer5         = U. Reyes, L. Muggerud
| lyrics5         = 
| music5          = 
| extra5          = Mellow Man Ace
| length5         = 3:43
| title6          = Swang Da Funk
| note6           = Produced by Blueblood, LROC
| writer6         = Blueblood, LROC, M. Johnson, King Black, C Wave, E. Roc Drah, M. Crocker
| lyrics6         = 
| music6          = 
| extra6          = New Version of Soul
| length6         = 4:17
| title7          = Donovans Mix
| note7           = Produced: Stuart Shapiro, Kao Rossman. Remixed: Jimmy Starr
| writer7         = Kao Rossman, Tony Vargas
| lyrics7         = 
| music7          = 
| extra7          = Vocal Samples: Corey Franklin
| length7         = 2:55
| title8          = Olelê, OLalá
| note8           = Produced by Kao Rossman
| writer8         = Tony Vargas
| lyrics8         = 
| music8          = 
| extra8          = Serapis Bey
| length8         = 3:09
| title9          = Enter The Dojo
| note9           = Produced by Scott G., Iki Levy
| writer9         = Iki Levy
| lyrics9         = 
| music9          = 
| extra9          = -
| length9         = 3:22
| title10         = Only the Strong
| note10          = Produced by Scott G., Iki Levy
| writer10        = Kao Rossman, Stuart Shapiro, Iki Levy, Scott G., Marcel "ICB" Branch, Patrick "Dizon" McCain, Donna Simon
| lyrics10        = 
| music10         = 
| extra10         = Marcel "ICB" Branch, Patrick "Dizon" McCain, Donna Simon
| length10        = 4:10
| title11         = Only the Strong (Remix)
| note11          = Produced & Remix by The Baker Boys
| writer11        = Kao Rossman, Stuart Shapiro, Iki Levy, Scott G., Marcel "ICB" Branch, Patrick "Dizon" McCain, Donna Simon
| lyrics11        = 
| music11         = 
| extra11         = Marcel "ICB" Branch, Patrick "Dizon" McCain, Donna Simon
| length11        = 3:04
| title12         = Ogum Drum Music
| note12          = Only in Movie
| writer12        = -
| lyrics12        = 
| music12         = 
| extra12         = Ilesa Anago Africa
| length12        = 
| title13         = Capoeira Song (Brazilian Village)
| note13          = Only in Movie
| writer13        = Claudio Carniero
| lyrics13        = 
| music13         = 
| extra13         = "Amen" Santo
| length13        = 
}}

==Box office and reception==
Only the Strong earned US$3,273,588 at the U.S. box office, making only more than half of its US$6 million budget. 

The film was universally panned by critics, earning a 0% approval rating on Rotten Tomatoes based on seven reviews.  Roger Ebert gave the film one out of four stars, saying, "The message of a movie like Only the Strong, building on the fascist undertones of its title, is almost cruel in its stupidity and naivete. Its almost a relief that few people in the audience for such a film ever remember if it even had a message or not."  Joe Brown of The Washington Post also wrote a scathing review, saying it "relies slightly less relentlessly on violence for its own sake than most in this genre, but the film is clumsily assembled and edited, heavy on the slow-mo, and its simplistic story plays like The Kids From Fame armed with very sharp knives." 

==References==
 

==External links==
* 
* 
*  at Box Office Mojo

 

 
 
 
 
 
 
 
 
 
 
 