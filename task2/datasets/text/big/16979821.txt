A Closer Walk
 
{{Infobox film

 | name = A Closer Walk
 | caption = Logo for A Closer Walk
 | director = Robert Bilheimer
 | producer = Robert Bilheimer Heidi Ostertag (co-producer) Bill Stetson (co-producer)
 | narrated = Glenn Close Will Smith
 | editing = Anthony DeLuca Richard Young
 | distributor = Worldwide Documentaries
 | released = 2003
 | runtime = 85 minutes
 | country = Australia
 | language = English
 | image = A Closer Walk VideoCover.jpeg
}} 
 global AIDS epidemic.  Narrated by Glenn Close and Will Smith, A Closer Walk features cinematography by Richard D. Young, interviews with the Dalai Lama, Bono, and Kofi Annan, and musical contributions by Annie Lennox, The Neville Brothers, Dido (singer)|Dido, Eric Clapton, Moby, Geoffrey Oryema, and Sade (band)|Sade. 

Subjects and storylines encompass the broad spectrum of the global AIDS experience and include people with HIV/AIDS from all walks of life; AIDS children and orphans and those caring for them; doctors, nurses, and social workers; human rights advocates; and prominent scientists, economists, researchers, government leaders, and NGO officials. The films basic themes remain, what are the underlying causes of AIDS; the relationship between health, dignity, and human rights; and the universal need for action, compassion, and commitment to counter what has become the worst plague in human history.

==Production== Kansas City, San Francisco, and Cambridge.  Production of A Closer Walk began in February 2000, and was completed in December 2002.

==Cast==

*Glenn Close  - Narrator
*Will Smith  - Narrator
*Kofi Annan  - Himself
*Bono  - Himself
*Dalai Lama  - Himself

==Critical response==
The film has received international critical acclaim. Writing for the Gannett News Service, chief film critic Jack Garner gave A Closer Walk his highest rating, calling it a "beautifully told story of suffering that inspires hope and action." In South Africa, reviewing the film prior to its national airing on South African television, Claire Keaton of the Sunday Times called the film "unforgettable." In New Delhi, following the films India premiere, Rajeshwari Sharma of the Economic Times said that A Closer Walk is "an absolutely brilliant account of the deadliest plague humankind has ever known." 

==Distribution==
A Closer Walk is a production of Worldwide Documentaries, a not-for-profit film production company focusing on matters of cultural, social, and humanitarian interest.

==References==
 

==External links==
*  
*  
* 
 

 
 
 
 
 
 
 
 