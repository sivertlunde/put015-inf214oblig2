Voodoo (film)
{{Infobox film
| name            = Voodoo
| image           = Voodoo (1995 film).jpg
| caption         = 
| director        = Rene Eram
| producer        = Donald P. Borchers
| writer          = Brian DiMuccio, Dino Vindeni
| starring        = {{plainlist|
* Corey Feldman
* Jack Nance
* Joel J. Edwards
* Diane Nadeau
* Ron Melendez
* Sarah Douglas
}}
| music           = Keith Bilderbeck
| editing         = Paulo Mazzucato
| distributor     = Planet Productions
| runtime         = 
| country         = United States
| language        = English
| budget          = 
}}
Voodoo is an 1995 American horror film directed by Rene Eram, and written by Brian DiMuccio and Dino Vindeni.  Corey Feldman stars as a youth who must content with a Voodoo cult.  Filmed in the United States in 1994, Voodoo was released on VHS  by Planet Productions in 1995, and has since been re-released in the United States on DVD format through Simitar Entertainment.

== Plot == voodoo along with his zombie friends.  

==Cast==
*Corey Feldman as Andy
*Jack Nance as Lewis
*Joel J. Edwards as Marsh
*Diane Nadeau as Rebecca
*Ron Melendez as Eric
*Sarah Douglas as Prof. Conner
*Amy Raasch as Wendy
*Brian Michael McGuire as Ken
*Christopher Kriesa as Baird
*Clark Tufts as Loomis
*Maury Ginsberg as Deitz
*Darren Eichhorn as David
*Brendan Hogan as Stan

== Reception ==
Writing in The Zombie Movie Encyclopedia, academic Peter Dendle said, "This run-of-the-mill campus thriller crosses Angel Heart with Revenge of the Nerds, as if that were a niche that really needed filling." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 