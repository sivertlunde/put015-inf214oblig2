The Gay Marriage Thing
{{Infobox Film | name = The Gay Marriage Thing
 | director =  
 | producer =   
 | composer =  
 | cinematography = Patrick Carey,   ,   ,  Mandy Minichello,  Gayle Green
 | editing =    
 | distributor = The Cinema Guild
 | released = 2005 (United States|USA)
 | runtime =  47 min. English
 }}

The Gay Marriage Thing is a 2005 documentary film directed by  Stephanie Higgins, who is a product of the Emerson College film school graduate program. The film follows the heated debate over same-sex marriage in Massachusetts in 2004 through the perspective of a lesbian couple who wishes to legally marry, as that state became the first in the U.S. to grant same-sex marriages (although those marriages were not recognized by the federal government).

==Location==
The film was shot entirely throughout Massachusetts.

==Synopsis==
“The Gay Marriage Thing” follows  Gayle and Lorre, thirtysomething college sweethearts who marked their 15th anniversary a year after the Massachusetts Supreme Court ruled a ban on same-sex marriage unconstitutional.

The film includes footage of protests outside the Massachusetts State House, the churches of the Reverends Rich Wiesenbach and Carlton Smith, and the state legislature leading up to May 17, 2004, the first date same-sex couples could file for marriage licenses in Massachusetts.

==Interviews==
The Gay Marriage Thing  includes interviews with United Church of Christ Reverend Richard Wiesenbach, Massachusetts State Representative Kathi-Anne Reinstein, Unitarian Universalist Reverend Carlton Smith, as well as the featured couple and a multitude of man-on-the-street interviews.

==Distribution==
In 2005 the Cinema Guild released the film on DVD in the United States.

==Film Festivals==
Artivist Film Festival Los Angeles – November 11, 2006

Boston Gay and Lesbian Film/Video Festival – May 21, 2006
 Boston Jewish Film Festival – November 8, 2004 (sneak preview work in progress)

Breckenridge Festival of Film, Colorado – September 10, 2005

Camden International Film Festival, Maine – September 30, 2005

Maine Jewish Film Festival – March 14, 2005

New England Film and Video Festival, Massachusetts – October 9, 2005

New Hampshire Film Expo – October 15, 2005

Chicago Reeling Film Festival, Chicago – November 4, 2005

Reel Identities Film Festival – New Orleans – June 11, 2005 *2nd place audience favorite

Rhode Island International Film Festival – August 11, 2005

Women on Film Festival Key West, FL – September 5, 2007

==References==
 
 
*  , Film Review, November 21, 2008.
*  , The Cinema Guild.
*  , August 2008.
*  , July 2, 2008.
*  , Filmmaker Interview, September 15, 2008.
*  , Women On Film Festival, Key West PFLAG Chapter, September 5, 2007.
*  , The Gay Marriage Thing Well Worth Toasting, December, 2006.
*  , Marriage Celebrated On Film, May 17, 2006.
*  , New England Film & Video Fest, Issue #61, Vol. 13: Winter 2006.
*  , Look Over Here On The Big Screen, October 12, 2005.
*  , Gay Marriage Thing Takes A Shot At The Festival Of Film, September 8, 2005.
*  , November 4, 2004
* "The Gay Marriage Review Thing," The Anchor, April 18, 2006
*  , MA Filmmaker Makes a Difference With Documentary, May 10, 2006

==External links==
*   official website.
*  

 
 
 
 
 
 
 
 