Vettaikaaran (2009 film)
 
 
{{Infobox film
| name           = Vettaikaaran
| image          = Vettaikaran hq poster.jpg
| caption        = Promotional Poster
| director       = Babu Sivan
| producer       = M. S. Guhan M. Saravanan (film producer)|M. Saravanan
| writer         = Babu Sivan Vijay Anushka Sathyan
| music          = Vijay Antony
| cinematography = Gopinath
| editing        = V. T. Vijayan
| studio         = AVM Productions FiveStar (Malaysia)  Sri Sai Ganesh Productions (Telugu language|Telugu) 
| released       =  
| country        = India Tamil
| runtime        = 166 minutes
| budget         = 
| gross = 
}} Indian Tamil Vijay and Anushka Shetty. Gopinath handled cinematography while V. T. Vijayan was the films editor. The film, produced by AVM Productions and distributed by Sun Pictures, was launched in February 2009 and the shooting for the film commenced the following month.The film revolves around a youngster who aims to be a cop just like a local police commissioner whom he idolizes, but during the process he is stopped by the evil done to his friends by a local mobster. The films soundtrack, composed by Vijay Antony,was released in September 2009. This film is dubbed in Hindi as Dangerous Khiladi 3.

==Plot==
Ravi (Vijay (actor)|Vijay) lives in Tuticorin. He aspires to become a policeman like his idol, Encounter Specialist Devaraj IPS (Srihari (actor)|Srihari). After completing his Plus 2, he joins a college in Chennai, and also earns a living by driving an auto rickshaw. During the course, he meets Suseela (Anushka Shetty) and falls in love with her instantly. Although Suseela rejects Ravis advances at first, with the help of her grandmother (Sukumari), he succeeds in winning Suseelas heart.
 Ravi Shankar),a drug smuggling and is expelled from college. Only Suseela is willing to actively help him. Suseela goes to Devaraj to garner support to help Ravi, but Devaraj refuses to help as his entire family had died at the hands of Vedanayagam and his henchmen as a result of trying to take action against them, with Devaraj himself made completely blind by Vedanayagam. However, with the help of his henchmen, he saves Ravi from being killed in a fake encounter led by Kattabomman. It is at this stage that Ravi takes up a new persona called "Police" Ravi to clean up the illegal activities of Vedanayagam and instill hope in the public, something that Devaraj was unable to do.

In the process, however, Vedanayagam kills Ravis close friend Sugu (Sathyan Sivakumar), prompting Ravi to kill Chella in retribution. Vedanayagam decides soon after to become a minister, knowing that it is the only way to put a stop to Ravis targeting of him and his activities. As Ravi finally plans to kill Vedanayagam, fully knowing the danger of him becoming a minister, the police arrive to arrest Ravi. However, Ravi sees Devaraj in the crowd and announces Vedanayagams location to him just as he is being arrested, allowing Devaraj to shoot and kill Vedanayagam, effectively taking his revenge.

In the end, Devaraj is reinstated into the police force and offers to make Ravi a police officer. However, Ravi refuses, stating that he has found the police officer within himself and that is all he needs to succeed in life.

==Cast==
 Vijay as Ravi, aka Police Ravi the protagonist
*Anushka Shetty as Susheela, Ravis love interest
*Srihari as Devaraj, the IPS officer who Ravi idolises
*Sayaji Shinde as Kattabomman, a corrupt police officer working for Vedanayagam
*Salim Ghouse as Vedanayagam, the main antagonist
*Sanchita Padukone as Uma, Ravis friend
*Sathyan Sivakumar as Sugu, Ravis friend Ravi Shankar as Chella Vedanayagam, Vedanayagams son Srinath as Valayapathi, Ravis friend
*Sukumari as Susheelas grandmother
*Manikka Vinayagam as Umas father
*Cochin Haneefa as the owner of a complex
*Raviprakash as Commissioner of Police
*Delhi Ganesh as Ravis father
*Lakshmi Ramakrishnan as Ravis mother
*Bala Singh as Rajasekhar
*Jayashree as Chellas Wife
*Kalairani
*Manobala
*Imman Annachi
*Raviprakash
*Jason Sanjay in a special appearance
*Madalasa Sharma in a cameo appearance

==Production==

===Development===
During the filming of Kuruvi, directed by S. Dharani, B. Babusivan served as one of his assistant directors in the film and wrote the dialogues. Sivan was later prompted to begin his maiden directorial venture with Vijay in the lead role. He was eventually chosen as the director for the next feature film to be produced AVM Productions. The project was originally titled as Police Ravi but in August 2008 it was re-titled as Vettaikkaaran, taken from the Vettaikaaran (1964 film) starring M. G. Ramachandran. 
 Dharani were present at the films inauguration.

===Casting=== Hari were mentioned, but AVM Productions chose B. Babusivan to be the director of the film.      

Several actresses were considered for the lead female role with Shriya Saran, Tamannaah, Ileana DCruz, Bhavana (actress)|Bhavana, Asin,    Nayantara and Anushka Shetty    being considered for the role. Afterwards, Anushka was finalized to portray the role. Cinematographer Gopinath was chosen to be the lead cameraman in the film after Ravi Varman was dropped from the film.    V. T. Vijayan was signed as the films editor.

==Soundtrack==
{{Infobox album|  
  Name        = Vettaikaaran
| Type        = Soundtrack
| Artist      = Vijay Antony
| Cover       = Vettaikaran hq poster.jpg
| Released    =  
| Recorded    = Audiophiles Feature film soundtrack
| Length      = 26:33
| Label       = Sony Music India
| Music       = Vijay Antony
| Last album  = Mahatma (2009)
| This album  = Vettaikaran (2009)
| Next album  = Rasikkum Seemane (2010)
}}

===Tamil===
{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Singers || Length (m:ss) ||Lyrics||Picturization
|-
| 1|| "Naan Adicha" ||   || Raj Mundhir
|-
| 2|| "Karigalan Kala" ||   || Pollachchi
|- Kabilan || AVM Studio and Madurai
|-
| 4|| "Oru Chinna Thamarai" || Krish (singer)|Krish, Dinesh Kanagaratnam, Bonekilla, Suchitra || 4:35 || Viveka || Punai
|-
| 5|| "En Uchimandai" || Krishna Iyer, Shoba Chandrasekhar, Charulatha Mani, Shakthisree Gopalan || 4:12 || Annamalai || AVM 
|}

===Telugu===
{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Singers || Length (m:ss) ||Lyrics||Picturization
|-
| 1|| "Adugesthey" || Tippu || 4:37 || Vennelakanthi || Raj Mundhir
|-
| 2|| "Karimabbu" || Saketh Naidu, Kavitha || 4:17 || Vennelakanthi || Pollachchi
|-
| 3|| "Pul Vetidi" || Ramu || 4:17 || Vennelakanthi || AVM Studio and Madurai
|-
| 4|| "Yeda Alapai" || Rakhi, Veena || 4:35 || Bhuvanachandra|| Punai
|-
| 5|| "Thikkedo" || R.S. John Vianni, Aishwarya || 4:12 || Bhuvanachandra || AVM Studio
|}

==Release== Sun TV. The film was given a "U" certificate by the Indian Censor Board.

===Critical reception=== Sify stated the movie is a paisa vasool film and entertainment is guaranted.The major plus for the movie are the five peppy songs tuned by Vijay Antony which are choreographed well. The introductory song Naan Adicha has lot of energy in it and the song rocked.The action scenes by Kanal Kannan are superbly choreographed.Gopinath|Gopinath’s camera is slick and editing is fast paced and rated 4/5.  Behindwoods rated 3/5 and stated "The charismatic screen presence of Vijay(actor)|Vijay,enjoyable musical tracks,sparkling stunts,fiery punch lines,the signature lighter moments and foot tapping numbers, makes the movie entertain the family audience.Behindwoods stated directed B. Babusivan had made a wholesome family entertainer movie.  The Times of India gave 3.5 stars out of 5 and stated "As far as commercial films are concerned,Vettaikaran is definitely assured filmmaking by director B. Babusivan" 

===Awards===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipient
! Result
|- Vijay Awards Vijay Award Favourite Hero Vijay (actor)|Vijay
| 
|- Vijay Award Favourite Heroine Anushka Shetty|Anushka
| 
|- Vijay Award Favourite Film Vettaikaran
| 
|- Vijay Award Favourite Song Chinna Thamarai
| 
|- Vijay Award Best Music Director Vijay Antony
| 
|- Vijay Award Best Villain Salim Ghouse
| 
|- Vijay Award Best Male Playback Singer Krish (singer)|Krish
| 
|- Vijay Award Best Lyricist Kabilan
| 
|- Vijay Award Best Stunt Director Kanal Kannan
| 
|- Filmfare Awards South
|- Filmfare Best Best Lyricist Kabilan (Karikaalan)
| 
|- Filmfare Best Best Lyricist Vivega (Oru Chinna Thamarai)
| 
|- Filmfare Best Best Male Playback Singer Krish (Oru Chinna Thamarai)
| 
|- Filmfare Best Best Female Playback Singer Suchitra (Oru Chinna Thamarai)
| 
|- Edison Awards Edison Awards Edison Awards Best Male Playback Singer Krish
| 
|- South Scope Cine Awards Best Tamil Male Play Back Singer Krish (Oru Chinna Thamarai)
| 
|- Best Tamil Female Playback Singer Suchitra (Oru Chinna Thamarai)
| 
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 