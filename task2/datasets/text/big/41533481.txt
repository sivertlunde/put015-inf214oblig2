Zatoichi and the Chest of Gold
{{Infobox film
| name = Zatoichi and the Chest of Gold
| image = 
| director = Kazuo Ikehiro 
| producer = Hiroshi Ozawa
| writer = Shozaburo Asai Akikazu Ota Kan Shimozawa (story)
| starring = Shintaro Katsu
| music = Ichirō Saitō
| cinematography = Kazuo Miyagawa
| released = 14 March 1964
| runtime = 83 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).  It also stars Tomisaburo Wakayama who would later play the lead in the Lone Wolf and Cub series.

Zatoichi and the Chest of Gold is the sixth episode in the 26-part film series devoted to the character of Zatoichi, following Zatoichi on the Road and preceding Zatoichis Flashing Sword.

==Plot==
Ichi travels to the village of Itakura to pay respects at the grave of a man he killed two years earlier. At the grave he reminisces on a fight between the two yakuza gangs (Iioka and Sasagawa from an earlier film) one man ran away and, though Ichi was not taking part in the fight, attacked Ichi. The dead mans sister, Chiyo, overhears Ichi say this to himself.
 Ryo with which to pay taxes to the local intendant. Ichi gladly joins in while the locals sing of their hero Chuji- who lives in the hills with a troupe, hiding from the constabulary while protecting the villagers. As they transport the gold to the local intendant (Gundayu) they are ambushed by three samurai (led by Jushiro, who wields a whip) and then by a larger band. Several of the villagers are slain and the rest flee. When the robbers chase the chest of gold to the base of the hill, they find Ichi sitting atop it smoking his pipe. He kills several men when attacked and the rest flee. In the village the local men accost Ichi, blaming him for its theft, before he promises to find the gold. The conversation is overheard by a well dressed woman called Ogin. Taking the backroads to avoid government checkpoints he encounters her again. When she questions his motive for being on the backroad he replies that he could not tell, as he is blind and then asks her why she is on the same road.
 Akagi several bandit that some of his men have gone rogue and stolen the chest of gold- though the two men are quick to confess and ask that he kill them for their acts. Losing his taste for the bandit life style, Chuji decides to leave and asks Ichi to take a different route- in order to escort the young nephew of one of the troupe to safety, before thanking Ichi for his faith in him.

In the nearby town, Ogin (the lover of Monji), accompanied by Chiyo, tells Monji about Ichi going to the bandtis and Monji promises revenge for the sister. Monji has just been promoted to head of the local constabulary and received his jitte (one pronged iron truncheon) as mark of office. He organizes a number of men to capture Ichi and the bandits on Mt Akagi. Ichi hears them coming and raises the alarm system the bandits set up at the bottom of the mountain, attracting the police and killing many of them to allow the others more time to escape. However the others are soon trapped and die defending their boss.
 dice hall. They approach him and after Jushiro cuts a mon coin in half by throwing his sword in the air, they bet Ichi 35 Ryō that he cant do the same. He accepts and wins. Outside, he confronts the two samurai whose conversation he overheard. Wile they deny knowledge, one of them inadvertently reveals the intendants involvement. Ichi pays them 10 Ryō, but they still attack him and are killed.

The Itakura village headman pleads for Intendant Gundayu to be lenient as the tax money was stolen. Gundayu accuses the headman of lying, insisting that the tax be paid in ten days. Jushiro talks to Gundayu, with Monji present, and reveals that Gundayu is the recipient of the stolen 1000 Ryō and demands more money. Ichi arrives and Jushiro calls Ichi a worm, though he wants to kill him privately so that the other two men get no pleasure from seeing their enemy killed. Monji orders his men to capture Ichi but Jushiro knocks them back with his whip, saying they will only die and Ichi leaves unmolested. Gundayu orders the headman and three other village men executed at dawn. On his return to Itakura, Ichi is mobbed, but they stop when some calls him a blind fool and his demeanour changes. He leaves and Chiyo follows him. He says he sensed who she was from the start and she pleads with Ichi to save them.
 Bodai Temple in an hour. Chiyo is waiting for Ichi and he gives her the gold. He also gives his gambling winnings for a proper headstone for her brother. Chiyo, possibly taken with Ichi, pleads with him to stay for the forthcoming celebration in the village.

Jushiro arrives at the duel on a horse and wraps his whip around Ichis neck, dragging him along behind and causing Ichi to drop his sword. However, Jushiro drags him back past his sword. Ichi pulls Jushiro from his horse, then reclaims his weapon and kills him, and apparently starts back towards the village to keep his promise to celebrate with Chiyo...

== Cast ==
*Shintaro Katsu as Zatoichi Shogo Shimada as Chuji Kunisada Machiko Hasegawa as Ogin
* Tomisaburo Wakayama as Jushiro
* Tatsuya Ishiguro as Enzo
* Matasaburo Niwa as Asataro
* Hikosaburo Kataoka as Iwajiro
* Mikiko Tsubouchi as Ochiyo  

==References==
 

==External links==
*  
*  review by Brian McKay for eFilmCritic.com (18 January 2004)
*  , review by J. Doyle Wallis for Home Vision Entertainment (29 April 2003)

 

 
 
 
 
 
 
 
 

 