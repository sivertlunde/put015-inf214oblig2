Who Is Killing the Great Chefs of Europe?
{{Infobox film
| name           = Who Is Killing the Great Chefs of Europe?
| image          = Who Is Killing the Great Chefs of Europe? film poster.jpg  
| image_size     =   
| director       = Ted Kotcheff
| producer       = William Aldrich
| writer         = Peter Stone
| narrator       =
| starring       = George Segal Jacqueline Bisset Robert Morley Jean-Pierre Cassel Philippe Noiret Jean Rochefort
| music          = Henry Mancini
| cinematography = John Alcott
| editing        =
| studio         = Lorimar
| distributor    = Warner Bros. October 5, 1978
| runtime        = 112 minutes
| country        = United States/West Germany
| language       = English
| budget         =
| preceded by    =
| followed by    =
 }} 1978 comedy mystery film starring George Segal, Jacqueline Bisset, and Robert Morley.  It is based on the novel Someone is Killing the Great Chefs of Europe by Nan and Ivan Lyons. 

The chefs are each killed in a manner reflecting their most famous dishes; for example, the lobster chef is drowned (in the book, the recipe for each dish is given).  The film was co-produced by the U.S.A., Italy, France and West Germany.  It was released in the UK under the title Too Many Chefs.

The film was originally distributed by Warner Bros., but it was produced by Lorimar, which owned all rights up until the studio was acquired by Warner Communications in 1989. Warner now again owns the rights to the film.

==Plot==

Natasha OBrien (Jacqueline Bisset) is a celebrated pastry chef invited to London to assist in preparing a state dinner for the Queen, organized by culinary critic Maximillian "Max" Vandeveer (Robert Morley).  Natashas ex-husband, Robby (George Segal) is a fast food entrepreneur ("the Taco King") serving the "everyman" consumer while she caters to the affluent.  Max is the "calamitously fat" grand gourmand publisher of a gourmet magazine Epicurious  and is patron of several famous European chefs, each renowned for a signature dish.

When Natasha arrives in London, Max is gloating over his latest issue, featuring "the worlds most fabulous meal", which highlights the culinary masterpieces of his favorite chefs.  However, Maxs health is failing from an addiction to those chefs specialties.  After completing the meal at Buckingham Palace, Natasha has a one-night fling with chef Louis Kohner (Jean-Pierre Cassel) whose speciality is baked pigeon in crust.  The next morning, Natasha finds Louis dead in a 450-degree oven.  After questioning by police, Natasha, and Robby, depart for Venice where Natasha is wooed by another chef, Fausto Zoppi (Stefano Satta Flores), whose speciality is a lobster dish.  When turning up for their date at his kitchen, Natasha finds Zoppi dead in a tank of lobsters.  After more questioning, Natasha receives a call from Robby to come to Paris to prevent one member of a group of French chefs from being murdered. 
 Bombe Richelieu." Meanwhile, Robby and Natasha begin falling in love again while in Paris.  Moulineau ends up dead, pushed headfirst into a duck-press.

In London, Natasha is set to be a guest on a cooking show. Robby stays in London to keep her safe.  However, as Robby is preparing to leave, he realizes that the cake that Natasha is set to light has a bomb inside of it. He arrives at the studio and rescues her just in time.

The murderer turns out to be Maxs dedicated assistant Beecham (Madge Ryan).  She was motivated to kill the chefs in a vain attempt to keep Max on his severe diet by removing the focus of his addiction. In the final scene, Robby and Natasha get re-married.

==Cast==
* George Segal as Robby Ross 
* Jacqueline Bisset as Natasha OBrien 
* Robert Morley as Maximillian Vandeveer 
* Jean-Pierre Cassel as Louis Kohner 
* Philippe Noiret as Moulineau 
* Jean Rochefort as Auguste Grandvilliers 
* Gigi Proietti as Ravello (as Luigi Proietti) 
* Stefano Satta Flores as Fausto Zoppi 
* Madge Ryan as Beecham 
* Frank Windsor as Blodgett 
* Peter Sallis as St. Claire 
* Tim Barlow as Doyle 
* John Le Mesurier as Dr. Deere 
* Joss Ackland as Cantrell
* Jean Gaven as Salpetre 
* Daniel Emilfork as Saint-Juste 
* Jacques Marin as Massenet 
* Jacques Balutin as Chappemain 
* Jean Parédès as Brissac 
* Michael Chow as Soong 
* Anita Graham as Blonde  Nicholas Ball as Skeffington  David Cook as Bussingbill 
* Nigel Havers as Counterman
* Caroline Langrishe as Loretta

==Awards== 1978 Los Angeles Film Critics Association Awards (1978) and at the National Society of Film Critics Awards (1979). He was also nominated for a Golden Globe for Best Motion Picture Actor in a Supporting Role along with Jacqueline Bisset for Best Motion Picture Actress (1979).

==Notes==
 

===References===
 
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 