Manasu Rendum Pudhusu
{{Infobox film
| name           = Manasu Rendum Pudhusu
| image          = Manasu Rendum Pudhusu DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = P. Amirtham
| producer       = A. Gunanithi
| writer         = P. Kalaimani  (dialogues) 
| screenplay     = P. Kalaimani
| story          = A. K. Lohithadas
| starring       =   Deva
| cinematography = Ravindar
| editing        = P. Venkateswara Rao
| distributor    =
| studio         = Poomalai Productions
| released       =  
| runtime        = 125 minutes
| country        = India
| language       = Tamil
}}
 1994 Tamil Tamil comedy Deva and was released on 23 June 1994. The film was a remake of the Malayalam film Sasneham.   

==Plot==

Thomas (Jayaram) and Lakshmi (Kushboo), both teachers, are a newly and happily married couple. They got married without their families consent. Lakshmi finally gets pregnant, the news reach their respective family. What transpires next forms the rest of the story.

==Cast==

*Jayaram as Thomas
*Kushboo as Lakshmi
*V. K. Ramasamy (actor)|V. K. Ramasamy as Paramesh
*R. Sundarrajan (director)|R. Sundarrajan as Anthony
*Poornam Vishwanathan as Narayanan Iyer
*Vinu Chakravarthy as Peter Raj
*Venniradai Moorthy as M. K. Varadaraja Bhagavathar
*Mannangatti as Somu
*Sukumari as Meenakshi
*Vadivukkarasi
*Kavitha
*S. N. Parvathi
*Radhabhai
*Shanmugasundari
*Anuja as Narayani
*Smitha Sajith

==Soundtrack==

{{Infobox Album |  
| Name        = Manasu Rendum Pudhusu
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1994
| Recorded    = 1994 Feature film soundtrack
| Length      = 23:59
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1994, features 5 tracks with lyrics written by Vaali (poet)|Vaali. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Aala Marathil || S. P. Balasubrahmanyam || 5:07
|- 2 || En Uyir || Vinod || 4:47
|- 3 || Jolikku || Mano (singer)|Mano, S. Janaki || 4:33
|- 4 || Kadham Vanil || Vinod || 4:42
|- 5 || Mylapore Mami || Mano, Swarnalatha || 4:50
|}

==Reception==

Malini Mannath of The New Indian Express gave the film a positive review, praising the performances of its lead pair. 

==References==
 

 
 
 
 
 
 