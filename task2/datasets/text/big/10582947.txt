Young Catherine
{{Infobox film
| name           = Young Catherine
| image          = YoungCatherine-VHS.jpg
| image size     =
| caption        = VHS cover, featuring Vanessa Redgrave and Julia Ormond Michael Anderson
| producer       = Chris Bryant
| narrator       =
| starring       = Vanessa Redgrave   Julia Ormond
| music          =
| cinematography = Ernest Day
| editing        =
| distributor    =
| released       =   17 February 1991
| runtime        = 150 minutes;   180 minutes (Turner Network Television|TNT)
| country        =   English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1991 United American  Empress Elizabeth.

The miniseries is known as Intrigues impériales in France and Die Junge Katharina in Germany.

==Plot summary==
 Catherine (Julia Peter (Reece Sir Charles Williams (Christopher Plummer), she must take the throne by force if she, her young son Paul I of Russia|Paul, and the Russian Empire are to survive.

==Cast== Empress Elizabeth Sir Charles Williams Grand Duchess Catherine Count Mikhail Vorontsov Princess Johanna
*Maximilian Schell ... Frederick the Great Count Grigory Orlov Grand Duke Peter
*Anna Kanakis ... Countess Vorontsova
*John Shrapnel ... Archimandrite Todorsky Prince Christian August
*Alexander Kerst ... Prussian Ambassador Princess Catherine Dashkova Elizabeth Vorontsova
*Katya Galitzine ... Maria Choglokov Alexis Orlov

==Award and nominations==
Primetime Emmy Awards
*Primetime Emmy Award for Outstanding Supporting Actress in a Miniseries or a Movie - Vanessa Redgrave (nomination)
*Primetime Emmy Award for Outstanding Costumes for a Miniseries, Movie or a Special - Larisa Konnikova (nomination)

Gemini Award
*Best Dramatic Mini-Series (won)
*Best Costume Design - Larisa Konnikova (nomination)
*Best Performance by an Actress in a Leading Role in a Dramatic Program or Mini-Series - Julia Ormond (nomination)
*Best Photography in a Dramatic Program or Series - Ernest Day (nomination)
*Best Production Design or Art Direction - Harold Thrasher, Natalya Vasilyeva (nomination)

==Versions==
Both a 150-minute version and a 180-minute Turner Network Television version were released on VHS on May 29, 1991, and both are currently out of print. A 187-minute Russian language version was at one time available on DVD.
The film became available on DVD from the Warner Bros Archive Collection on November 19, 2013. 

==References==
 

==External links==
* 
*Clips from Young Catherine:   -  

 

 
 
 
 
 
 
 
 
 