Kshanakkathu
{{Infobox film
| name           =Kshanakkathu
| image          =
| caption        =
| director       =TK Rajeev Kumar
| producer       = 
| writer         = 
| screenplay     =
| starring       =Niyaz Musaliyar, Athira
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1990
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, directed by TK Rajeev Kumar, starring Niyaz Musaliyar and Athira in the lead roles.    

==Cast==
* Niyaz Musaliyar
* Athira 
* Thilakan	
* Kaviyoor Ponnamma	
* Nedumudi Venu	
* Lakshmi
* Murali	
* KP Ummer	
* Philomina	
* Vettukkili Prakash

== Soundtrack ==
The films soundtrack contains 8 songs, all composed by Sharreth and Lyrics by Kaithapram Damodaran Namboothiri.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aa Raagam Madhumayamaam"
| K. J. Yesudas
|-
| 2
| "Aaksadeepamennum"
| K. J. Yesudas, K. S. Chitra
|-
| 3
| "Mangalangalarulum (M)"
| K. J. Yesudas
|-
| 4
| "Mangalangalarulum (F)"
| K. S. Chitra
|-
| 5
| "Sallaapam Kavithayaay"
| K. J. Yesudas
|-
| 6
| "Thaam Thakathakida Dhim"
| K. J. Yesudas
|}

==References==
 

==External links==
* 

 
 
 

 