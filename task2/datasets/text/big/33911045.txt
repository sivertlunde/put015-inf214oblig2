Fastest (film)
 
{{Infobox film
| name           = Fastest
| image          =
| caption        =
| director       = Mark Neale
| producer       = Mark Neale Paul Taublieb
| writer         = Mark Neale
| narrator       = Ewan McGregor
| starring       = Valentino Rossi Jorge Lorenzo Ben Spies Casey Stoner Marco Simoncelli Colin Edwards
| distributor    = Media X International, Inc.
| released       =  
| music          = tomandandy
| runtime        = 
| language       = English
}} Road Racing The Doctor, The Tornado and The Kentucky Kid and will be succeeded by Charge, currently awaiting release.

==Overview== 2010 season into a maximum-speed, full-length documentary feature film. In true cinematic fashion, it highlights the thrills, spills and incredible commitment and courage the sports demands of its stars.

With unprecedented behind-the-scenes access at all sixteen races, which include Valentino Rossi facing the toughest challenge of his life whilst chasing his tenth world title: a wave of ferociously fast young riders and a horrific leg-shattering crash during practice for his home grand prix at Mugello Circuit|Mugello. Rather astonishingly this caused Vale to miss his first GP ever, 14-years into his Grand Prix career to that point. Thought to be out for six-months, Rossi made his comeback just 41 days later, at the German grand prix, but the game has moved on already and theres one question on every riders lips - even the G.O.A.T. (Greatest Of All Time) himself must have been wondering...  Whos fastest now?

==Notable appearances==
* Colin Edwards
* Jorge Lorenzo
* Valentino Rossi
* Marco Simoncelli
* Ben Spies
* Casey Stoner
 Gresini Honda), during what was his rookie season in the MotoGP category.
 Laguna Seca]]

==Release== Bradley Smith, Scott Redding, Danny Kent, Eugene Laverty. The event was hosted by Eurosport commentator Toby Moody, with a late appearance by fellow commentator Julian Ryder, and the BBCs Matt Roberts. The film was introduced with a short clip especially for the London Premier by Valentino Rossi.

The film is available on DVD in the US, UK and Canada only at present. There are theater viewings and details are available on the official website or on the films Facebook page.

==References==
 

==External links==
*  
* 

 
 
 
 
 