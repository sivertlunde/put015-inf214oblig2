Bryllupet
 

{{Infobox film
| name           = Bryllupet
| image          = 
| image size     =
| caption        = 
| director       = Leidulv Risan
| producer       = 
| writer         = Leidulv Risan   Bjørn Skaar
| narrator       =
| starring       = Mads Ousdal   Susan Badrkhan 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 14 April 2000
| runtime        = 85 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film directed by Leidulv Risan, starring Mads Ousdal and Susan Badrkhan. The film is about the young biochemistry student Tom (Ousdal), who falls in love with his lab partner, the Iraqi Samira (Badrkhan), and the problems caused by cultural conflict. It was a made for TV movie, produced by the Norwegian Broadcasting Corporation.

==Reception== dice throw of three and called it "predictable".    Liv Jørgensen of Dagbladet gave it four points, applauding the films "insight and dramatic devices". {{cite news|url=http://www.dagbladet.no/kultur/2000/04/14/201200.html|title=Kulturkonflikt som griper
|last=Liv|first=Jørgensen|date=14 April 2000|work=Dagbladet|language=Norwegian|accessdate=7 December 2010}} 

==References==
 

==External links==
*  
*   at Filmweb.no (Norwegian)
*   at the Norwegian Film Institute

 
 
 


 