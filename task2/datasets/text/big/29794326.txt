His Picture in the Papers
 
{{Infobox film
| name           = His Picture in the Papers
| image          = His Picture in the Papers Poster.jpg
| image_size     =
| caption        = John Emerson
| producer       =
| writer         = John Emerson Anita Loos
| starring       = Douglas Fairbanks Loretta Blake
| cinematography = George W. Hill
| editing        =
| studio         = Fine Arts Picture Company
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = Silent English intertitles
| budget         = $42,599.94 
}} silent comedy John Emerson. Anita Loos also wrote the films scenario. The film stars Douglas Fairbanks and Loretta Blake and features Eric von Stroheim in a minor role.   

== Plot ==
Pete Prindle, son of Proteus, a vegetarian health food manufacturer wishes to marry Christine Cadwalader. She agrees. However, Proteus considers his son lazy, with no contributions to the company and therefore undeserving of his fathers wealth. His daughters have their pictures in the newspaper of them promoting the company products. Cassius refuses to consent to his daughters hand since he believes Pete to be lazy as well, with no real stake in his fathers company. Pete tries hard to get in the newspaper: He fakes a car accident, which gets an insignificant mention in the paper. He wins a boxing match, which turns out to be an illegally run ring which ends up being raided by police.

After a misunderstanding, he washes up on the shore in his pajamas after falling off a cruise ship, and proceeds to beat two police officers, his name is withheld by the newspaper. Finally, he saves many people on a train from a group of thugs intent on murdering Cassius by preventing a collision with another rail car. He receives a front page article in every major local newspaper and a large photo as well which pleases everyone.

== Cast ==
*Douglas Fairbanks as Pete Prindle
*Clarence Handyside as Proteus Prindle
*Rene Boucicault as Pansy Prindle
*Jean Temple as Pearl Prindle
*Charles Butler as Cassius Cadwalader
*Loretta Blake as Christine Cadwalader
*Homer Hunt as Melville
*Helena Rupport as Olga
*Eric von Stroheim as Eye Patch Thug

==Production notes== Yonkers and Atlantic City. Columbus Avenue in Manhattan. 

==Preservation status==
A print of His Picture in the Papers is preserved at the Library of Congress.  It was also released on DVD by Flicker Alley. 

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 