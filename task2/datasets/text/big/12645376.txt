A Home on the Range
{{multiple issues|
 
 
}}
{{Infobox Film
| name           = A Home on the Range: The Jewish Chicken Ranchers of Petaluma
| image          = Petaluma.jpg 
| image_size     =
| caption        = 
| director       = Bonnie Burt and Judith Montell
| producer       = Bonnie Burt and Judith Montell
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       = 2002 (USA) 
| runtime        = 100 min. USA
| English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 documentary by America to organized a socialist society in rural Northern California, where they relied on raising chickens to support themselves.

==Summary==
“Who said Jews could not be farmers? Spit in his eye, who would so harm us?” Scott Gerber, a former resident of Petaluma, belts out the lyrics to his acoustic guitar. Although the chicken ranches in Petaluma have more recently been abandoned, Scotts maintained his Jewish culture and his love for the land, working as a cowboy and singing Yiddish folk songs.
 McCarthy era. But with time and assimilation the quirky community dwindled, and, today, their chicken ranches have been replaced by telecommunication, dairy farms and vineyards.
 American society. They were second rate citizens, called names and not allowed in Country Clubs.

They had fled pogroms in Russia and antisemitism in urban America to live in a rural Jewish community, but still they faced antisemitism from their surrounding neighbors. One woman remembers, “they used to call us dirty Jew.” And another woman remembers a particularly frightening night when an antisemitic neighbor threw a barn party and the drunk and rowdy crowd terrified her parents so much that they couldnt sleep that night. These incidents ingrained the Jews with a painful feeling of being “lesser” than other Americans. But nothing can compare to the violent night that Gentile leaders in the neighboring community took out their antisemitism and anticommunist fears through brute force against the men of Petaluma.

It wasnt prejudice that destroyed their community, but acceptance. Once Jewish Americans were no longer looked down on, they assimilated into society and the vibrant community of chicken ranchers in Petaluma dwindled. One former resident who grew up on a ranch and raised her children on a ranch expresses her mixed feelings about assimilation. She pines that she lost, “the core,” sense of attachment but in exchange, “we were accepted in as Americans.”

==Production==
For the past twenty years, Bonnie Burt has been making documentaries about Jewish life. Her films have screened at the Museum of Modern Art and at Lincoln Center in New York. {{cite web
  | title = A Home on the Range: The Jewish Chicken Ranchers of Petaluma
  | work =
  | publisher = Bonnie Burt and Judith Montell
  | date =
  | url =http://www.jewishchickenranchers.com/index.htm
  | accessdate = August 6  }} 
Bonnie says shed heard about Petaluma before she moved to California from the East Coast. It wasnt until 1992 at the Madrid Jewish Film Festival that she teamed up with Judy Montell, and the two began to work on the project together.

==Reception==
A Home on the Range was well received. The San Diego Jewish Film Festival called it a modest film that "reads almost like an epic myth." {{cite web
  | title =San Diego Jewish Film Festival
  | publisher =San Diego Center for Jewish Culture
  | date =
  | url =http://sdcjc.lfjcc.org/sdjff/2003/feb_9.php
  | accessdate = August 6  }}  Other reviews praised it for being a through and entertaining documentary that raises issues that are still relevant in Jewish American society today.

==See also==
Other Documentaries about Jews in America:
*My Yiddish Momme McCoy
*Awake Zion
*From Swastika to Jim Crow
*Professional Revolutionary
*Song of a Jewish Cowboy

More information on Jewish communes:
*Kibbutz
*Moshav
*Socialism
*Israel

==Notes==
 

==References==
*{{cite news 
  | last = Fishkoff
  | first =Sue
  | coauthors =
  | title = When left-wingers and chicken wings populated Petaluma
  | work =
  | pages =
  | publisher =The Jewish News Weekly
  | date =7 May 1999 
  | url =http://www.jewishsf.com/content/2-0-/module/displaystory/story_id/11172/edition_id/214/format/html/displaystory.html
  | accessdate = August 6 }}

*{{cite web
  | title = A Home on the Range: The Jewish Chicken Ranchers of Petaluma
  | work =
  | publisher = Bonnie Burt and Judith Montell
  | date =
  | url =http://www.jewishchickenranchers.com/index.htm
  | accessdate = August 6  }}

*{{cite web
  | title =San Diego Jewish Film Festival
  | publisher =San Diego Center for Jewish Culture
  | date =
  | url =http://sdcjc.lfjcc.org/sdjff/2003/feb_9.php
  | accessdate = August 6  }}

*{{cite web
  | title = A Home on the Range: The Jewish Chicken Ranchers of Petaluma
  | publisher=New England Film
  | date = November 2002
  | url =http://www.newenglandfilm.com/news/archives/02november/bjff.htm
  | accessdate = August 6  }}

==External links==
*  
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 