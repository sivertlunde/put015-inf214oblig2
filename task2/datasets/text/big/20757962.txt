Hard to Hold (film)
{{Infobox film
| name           = Hard to Hold
| caption        =
| image	         = Hard to Hold FilmPoster.jpeg
| director       = Larry Peerce Dana Miller Kurt Neumann Thomas Hedley Jr Richard Rothstein
| starring = {{Plainlist|
* Rick Springfield
* Janet Eilber
* Patti Hansen
}} Tom Scott
| cinematography = Richard H. Kline
| editing        = Don Guidice Bob Wyman Universal Pictures
| released       =  
| runtime        = 93 min
| country        = United States
| language       = English
| budget         =
| gross          = $11,113,806
}}
 pop career, soundtrack to the film.

==Plot summary==

James "Jamie" Roberts (played by singer, song-writer Rick Springfield), being a pop idol, is used to having his way with women.  He meets child psychologist Diana Lawson (Janet Eilber) in a car accident, however, who not only doesnt swoon at his attentions, but has also never heard of him.  He tries to win her affection, but its complicated by the fact that his ex-lover, Nicky Nides (Patti Hansen), is still a member of his band.

==Reception==
Janet Maslin of the New York Times found the film an exercise in narcissistic excess: "Dripping sweat, with the backstage lights glinting off his jeweled belt and his single earring, James Roberts escapes to his dressing room, collapsing beside the Space Invaders machine. Hes drained. Hes exhausted. Hes a very famous rock star, and he has just whipped another adoring audience into a lather. ...Hard to Hold is a movie for anyone who thinks this sounds like real behind-the-scenes rock-and-roll ambiance and for anyone who thinks Rick Springfield is a real rock star. Its not a movie for anyone else, except perhaps film students, who will find that Larry Peerce has included more weak transitions, conversational cliches, unflattering camera angles and ethnic restaurant scenes in this films mere 93 minutes than some directors manage in an entire career." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 


 