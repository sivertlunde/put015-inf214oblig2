Shaolin Temple (1976 film)
 
 
 
{{Infobox film
| name        = Shaolin Temple
| image       =
| caption     = Hong Kong theatrical poster Fu Sheng Wang Lung Wei Kuo Chui
| director    = Chang Cheh
| producer    = Sir Run Run Shaw Shaw Brothers Studio
| runtime     = 116 min.
| released    =  
| country     = Hong Kong
| music       = Chen Yung Yu Mandarin 
}}

Shaolin Temple aka Death Chambers is a Shaw Brothers film directed by Chang Cheh. It is one of the Shaolin Temple themed martial arts films and concerns their rebellion against the Qings, with an all-star cast featuring the second generation of Shaw stars David Chiang, Ti Lung, and Fu Sheng and the introduction of the Venoms. The film is a prequel to Five Shaolin Masters.

==Plot==
The film opens with the chief Shaolin Monks realizing that time is not on their side and they must train more fighters to fight the Qings. The monk Hai Hsien (Shan Mao) opposes this as he is secretly working for the court. Outside many men are sitting in front of the temple waiting to be accepted in, Fang Shih Yu (Fu Sheng), Ma Chao-hsing (Tony Liu) and others, as the temple tests the will of potential students by making them wait outside for days, eventually the two are accepted in for "training". Fang Shih Yu becomes frustrated immediately as Shaolin methods of teaching martial arts are rather obscure, although Ma Chao-hsing begins learning the five animal styles from the start.

Elsewhere, escaped Ming soldiers Tsai Te-cheng (Ti Lung), Hu Te-ti (David Chiang), Yen Yung-chun (Szu Shih), and Ma Fu Yi (Wang Lung Wei) arrive close to Shaolin looking for a place to hide from the Qing. Hu Te-ti suggests they go to Shaolin and they are instantly accepted, which frustrates three more students trying to get acceptance into the temple, Lin Kwong Yao (Kuo Chui), Huang Sung Han (Lee I Min), and Hu Hui Chien (Kuan-Chun Chi). The three are eventually accepted and begin their obscure training. Tsai Te-cheng learns the wing chun style from a strange female monk and Hu Te-ti learns the iron whip. This forces Hai Hsein to secretly go to the court and report to Prince Hoo (Lu Feng) that Shaolin has been training new fighters.

The Qing scheme to destroy Shaolin. Hai Hsein returns to Shaolin looking to recruit more traitors to set up the temple for a raid. All the accepted students become close friends and proficient in their kung fu, except for Fang Shih Yu (who is unknowingly learning tiger boxing but is impatient). He keeps getting into fights with Ma Fu Yi, who is disgruntled with being at Shaolin. A mysterious figure at night (perhaps his friend Ma Chao-sing) begins teaching Fang Shih Yu the tiger-crane style and he defeats Ma Fu Yi in a friendly spar. This sparks the curiosity of Hai Hsein as he takes Ma Fu Yi under his wing and recruits him as a traitor. Things begin to go sour at the temple due to the treachery of Hai Hsein and Ma Fu Yi and it is eventually attacked by thousands of Qing soldiers, and an all-out battle occurs at Shaolin Temple with only eight Shaolin devotees escaping the ensuing massacre.

==Cast==
* David Chiang – Hu Te-ti
* Ti Lung – Tsai Te-cheng Fu Sheng – Fang Shih Yu (fong sai yuk.)
* Wang Lung Wei – Ma Fu Yi
* Shan Mao – Hai Hsein
* Lu Feng – Prince Hoo
* Kuo Chui – Lin Kwong-yao Kuan Chun Chi – Hu Hui Chien
* Tony Lui – Ma Chao-sing
* Szu Shih – Yen Yung-chun
* Lee I Min – Huang Sung Han
* Lo Mang – Junior monk
* Chiang Sheng – Junior monk

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 