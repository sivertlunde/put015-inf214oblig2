Where It's At (film)
{{Infobox film
| name           = Where Its At
| image          = Where Its At poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Garson Kanin Frank Ross
| writer         = Garson Kanin 	
| starring       = David Janssen Rosemary Forsyth Robert Drivas Brenda Vaccaro Don Rickles
| music          = Benny Golson
| cinematography = Burnett Guffey
| editing        = Stefan Arnsten 
| studio         = Frank Ross Productions TFT Productions
| distributor    = United Artists
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Where Its At is a 1969 American drama film written and directed by Garson Kanin. The film stars David Janssen, Rosemary Forsyth, Robert Drivas, Brenda Vaccaro and Don Rickles. The film was released on May 7, 1969, by United Artists.  

==Plot==
A.C. Smith owns and runs the Caesars Palace hotel and casino in Las Vegas. As pleased as he is to have grown son Andy pay a visit, he wishes Andy would express some interest in women or the hotel, two things A.C. values above all else. 

Andy stays to learn the business after losing a cut of cards to his dad. He soon begins flirtations with Molly and Diana, who happen to his dads secretary and mistress. A chip off the old block, Andy saves the hotel for A.C. by winning a game of chance. He ends up with Molly, and his dad ends up with the family business in good hands.

== Cast ==	 
*David Janssen as A.C.
*Rosemary Forsyth as Diana
*Robert Drivas as Andy	
*Brenda Vaccaro as Molly 
*Don Rickles as Willie
*Edy Williams as Phyllis Anthony Holland as Henry
*Vince Howard as Ralph 	
*Warrene Ott as Betty Avery  The Committee as Themselve

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 