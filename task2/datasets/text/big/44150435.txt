Ulpathi
{{Infobox film
| name           = Ulpathi
| image          =
| caption        =
| director       = VP Mohammed
| producer       =
| writer         = VP Muhammad
| screenplay     = Reena
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Reena in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prameela
*Balan K Nair
*K. P. Ummer Reena

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by PT Abdurahiman. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ariyille || B Vasantha || PT Abdurahiman || 
|-
| 2 || Ilaahi || K. J. Yesudas || PT Abdurahiman || 
|-
| 3 || Kanneerkkadalinu || P Jayachandran, Chorus || PT Abdurahiman || 
|-
| 4 || Vennilaa || Vani Jairam, VT Murali || PT Abdurahiman || 
|}

==References==
 

==External links==
*  

 
 
 

 