Chico Xavier (film)
{{Infobox film
| name           = Chico Xavier
| image          = Chico Xavier.jpg
| image size     =
| alt            =
| caption        =
| director       = Daniel Filho
| producer       = Claudia Bejarano Daniel Filho Júlio Uchoa
| writer         = Marcos Bernstein
| starring       = Nelson Xavier Christiane Torloni Giulia Gam Letícia Sabatella Giovanna Antonelli Tony Ramos
| music          =
| cinematography = Nonato Estrela
| editing        = Diana Vasconcellos
| studio         = Lereby Productions
| distributor    =
| released       =  
| runtime        = 124 minutes
| country        = Brazil
| language       = Portuguese
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Brazilian drama film directed by Daniel Filho, written by Marcos Bernstein, and starring Nelson Xavier, Christiane Torloni, Giulia Gam, Letícia Sabatella, Giovanna Antonelli and Tony Ramos. It was released in Brazil on April 2, 2010. The movie is a biography of the Brazilian medium Chico Xavier.

==Cast==
*Nelson Xavier as Chico Xavier (1969 to 1975)
*Ângelo Antônio as Chico Xavier (1931 to 1959)
*Matheus Costa as Chico Xavier (1918 to 1922)
*Tony Ramos as Orlando
*Christiane Torloni as Glória
*Giulia Gam as Rita
*Letícia Sabatella as Maria
*Luis Melo asJoão Candido
*Pedro Paulo Rangel as Father Scarzelo
*Giovanna Antonelli as Cidália
*André Dias as Emmanuel
*Paulo Goulart as Saulo Guimarães
*Cássia Kiss as Iara
*Cássio Gabus Mendes as Father Julio Maria
*Rosi Campos as Cleide
*Carla Daniel as Carmosina

==External links==
*  
*  
*   

 

 
 
 
 
 
 
 
 
 
 

 
 