The Girl from Nagasaki
{{Infobox film
| name           = The Girl from Nagasaki
| image          = TGFN poster.jpg
| alt            = 
| caption        = Film poster
| director       = Michel Comte
| producer       = Michel Comte Ayako Yoshida Amy Filbeck
| writer         = Michel Comte Anne-Marie Mackay	
| screenplay     = 
| starring       = Mariko Wordell Edoardo Ponti Christopher Lee Michael Wincott Michael Nyqvist Polina Semionova Ayako Yoshida
| music          = Luigi Ceccarelli Alessandro Cipriani
| cinematography = Pierluigi Malavasi
| editing        = Nick Tamburri
| studio         = M4 Films AG Shotz Fiction Film
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Italy Germany Japan United States
| language       = English Japanese
}} romantic musical musical drama film directed by Michel Comte.   The film had its premiere at as the closing film 2013 Naples Film Festival on November 19, 2013.  The film later screened at 2014 Sundance Film Festival on January 18, 2014. 

==Plot==

World-renowned photographer Michel Comte’s contemporary, 3D reworking of the classic opera, Madame Butterfly, in which Puccini’s tragic heroine, emerging from the ashes of the atomic bomb, begins her fateful story of obsession for an American pilot. 

==Cast==
*Mariko Wordell as Cho-Cho San
*Edoardo Ponti as Officer Pinkerton
*Christopher Lee as Old Officer Pinkerton
*Michael Wincott as Goro
*Michael Nyqvist as Father Lars
*Polina Semionova as Cho-Cho Sans Alter Ego
*Ayako Yoshida as Suzuki Robert Evans as U.S. Consul
*Clemens Schick as Prince Yamadori
*Nobu Matsuhisa as Cho-Cho Sans Father
*Lisa Zane as Jazz Singer
*Sasha Alexander as Adelaide
  
==Reception==
"The Girl from Nagasaki" received mixed reviews from critics. Marshall Fine in his review for Huffington Post said that "staging is avant-garde, bloody and surreal, with elements of modern dance, classical tableaux, kabuki and opera, as well as conventional melodrama. Comte returns often to that staged version to emphasize the action or outline it in a more symbolic way."  While, Dan Schindel in his review for Movie Mezzanine said that "Feels like every derisive joke about art house cinema brought to unironic life." 

==References==
 

==External links==
*  
*  

 
 
 
 