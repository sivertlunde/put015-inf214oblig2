Berlingot and Company
{{Infobox film
| name           = Berlingot and Company
| image          = 
| caption        =
| director       = Fernand Rivers
| producer       =  
| writer         = Jean Manse
| starring       =  Fernandel, Suzy Prim and Fernand Charpin
| music          = Roger Dumas
| cinematography = Jean Bachelet
| editing        = Jacques Desagneaux
| studio         = 
| distributor    = 
| released       = 25 November 1939 (Portugal) 
| runtime        =         85 min 
| country        = France
| awards         = French
| budget         = 
| preceded_by    =
| followed_by    =
}}
 French comedy film directed by Fernand Rivers and starring Fernandel, Suzy Prim and Fernand Charpin.

==Cast==
* Fernandel : François, vendeur de berlingots
* Fernand Charpin : Victor, lassocié de François
* Suzy Prim : Isabelle Granville
* Fréhel : Bohémia
* Jean Brochard : Le directeur de lasile
* Rivers Cadet : Le lutteur
* Monique Bert : Thérèse
* Édouard Delmont : Courtepatte
* Jean Témerson : Un client du café
* Josselyne Lane : Lisa
* René Alié : Dédé
* Marco Behar : Le fou
* Jacques Servière : Gaston
* Fernand Flament : Paulo
* Le petit Jacky : La petite Gisèle
* Marcel Maupi : Isidore
* Marguerite Chabert

==External links==
* 

 
 
 
 
 


 