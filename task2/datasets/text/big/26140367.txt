Sweet Hours (Dulces horas)
{{Infobox Film
| name = Sweet Hours (Dulces horas)
| image = Sweet_Hours_Saura_film_poster.jpg
| director = Carlos Saura
| writer = Carlos Saura
| starring = Inaki Aierra   Assumpta Serna
| released = 1982
| country = Spain
| language = Spanish
}}


Sweet Hours (Dulces horas) is a Spanish film, written and directed by Carlos Saura and released in 1982. The title  comes from words in the soundtrack song Recordar (To Remember) sung by the soprano Imperia Argentina.

==Plot==
The past is a riddle to Juan (Inaki Aierra), the playwright. He is tormented by it - by memories of the elderly father who went off, and the young mother who committed suicide, and he has written a play, Sweet Hours, which contains the key scenes of his early life. The play is in rehearsal and he attends the sessions, watchful and absorbed. He is searching for something. Juan slips in and out of the actor reconstructions and his own memories. Finally, light dawns on Juan, he dredges up the repressed material, and his Oedipus Complex is resolved.

And Juan falls in love with Berta (Assumpta Serna), the stunning young actress who is rehearsing the role of the mother.

==Cast==
* Inaki Aierra as Juan
* Assumpta Serna as Berta
* Alvaro de Luna as Tio Pepe
* Jacques Lalande as Tio Angelito

==Reception==
The film was reviewed by Pauline Kael in The New Yorker; " This movie has the kind of subtle obviousness that is generally described as literate. What it comes down to is that Carlos Saura has a feeling for dark, autumnal elegance, and a dexterous technique that he puts at the service of tired ideas. What saves him  from pedantry is that his films have occasional moments of erotic vibrancy. Scenes that arent explicitly sexual in content are sexualized, so that they become ambiguous and disturbing - even haunting...In a Saura film, something more directly sexual is often impending; it hovers in the atmosphere. ..the wide-eyed Assumpta Serna, when she smiles, has a teasing elusiveness..She makes the atmosphere hum..perhaps Saura means us to see that the cycle is inescapable..The enigma of Saura is his addiction to enigma."  

==References==
 

 

 
 
 

 