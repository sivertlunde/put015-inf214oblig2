Hadh: Life on the Edge of Death
{{Infobox film
| name           = Hadh: Life on the Edge of Death
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Thampi Kannanthanam
| producer       = Sandeep D. Shinde
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jackie Shroff Sharad Kapoor Ayesha Jhulka Suman Ranganathan
| music          = Viju Shah
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Hadh: Life on the Edge of Death is a 2001  Bollywood film directed by Thampi Kannanthanam and produced by Sandeep D. Shinde. It stars Jackie Shroff, Sharad Kapoor, Ayesha Jhulka and Suman Ranganathan in pivotal roles. 

==Cast==
* Jackie Shroff...Vishwa
* Sharad Kapoor...Shiva
* Ayesha Jhulka
* Suman Ranganathan
* Tej Sapru...Chotey
* Vikram Gokhale...Commissioner Shastri
* Kiran Kumar...Dalal

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hing La La"
| Udit Narayan, Poonam Bhatia
|-
| 2
| "Aangoori Badan"
| Suresh Raheja, Sapna Awasthi
|-
| 3
| "Kya Hoti Hai"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Tere Dil Ne"
| Manhar Udhas, Alka Yagnik
|}

==References==
 

==External links==
* 

 
 
 

 