The Duke of Burgundy
 
 
{{Infobox film
| name           = The Duke of Burgundy
| image          = The Duke of Burgundy UK Poster.jpg
| image size = 250px
| caption        = UK poster Peter Strickland
| producer       = Andy Starke 
| writer         = Peter Strickland
| starring       = {{Plainlist|
* Sidse Babett Knudsen
* Chiara DAnna}}
| music          = Cats Eyes
| cinematography = Nic Knowland
| editing        = Mátyás Fekete
| production designer = Pater Sparrow
| studio         = {{Plainlist|
* Rook Films
* Film4 Productions
* Ripken Productions   }}
| distributor    = Artificial Eye
| released       =  
| runtime        = 104 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Peter Strickland. The film was screened at various film festivals, including the Toronto International Film Festival the London Film Festival, and the International Film Festival Rotterdam to positive critical reviews.   

==Cast==
* Sidse Babett Knudsen as Cynthia  
* Chiara DAnna as Evelyn 
* Monica Swinn as Lorna 
* Eugenia Caruso as Dr. Fraxini 
* Fatma Mohamed as The Carpenter  
* Kata Bartsch as Dr. Lurida   
* Eszter Tompa as Dr. Viridana 
* Zita Kraszkó as Dr. Schuller 

==Reception==
The film received positive reviews. On review aggregator Rotten Tomatoes, the film holds a rating of 93%, based on 72 reviews, with a rating average of 8.1 out of 10, with the consensus reading Stylish, sensual, and smart, The Duke of Burgundy proves that erotic cinema can have genuine substance.. At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 88 based on 7 reviews. 

Strickland received The Wouter Barendrecht Pioneering Vision Award at the Hamptons International Film Festival for his work in the film.  The film also won the Grand Jury Prize at the 23rd Philadelphia Film Festival. 

==Soundtrack==
The Duke of Burgundy was released by Cats Eyes in February 2015. 
{{tracklist
|-
|title1=Forest Intro
|length1=0:38
-
|title2=The Duke of Burgundy
|length2=2:19
|-
|title3=Moth
|length3=1:29
|-
|title4=Door No. 1
|length4=1:11
|-
|title5=Pavane
|length5=0:58
|-
|title6=Dr. Schuller
|length6=0:13
|-
|title7=Lamplight
|length7=2:48
|-
|title8=Door No. 2
|length8=1:39
|-
|title9=Carpenter Arrival
|length9=3:33
|-
|title10=Reflection
|length10=1:43
|-
|title11=Door No. 3
|length11=1:39
|-
|title12=Black Madonna
|length12=1:49
|-
|title13=Silkworm
|length13=0:18
|-
|title14=Evelyns Birthday
|length14=2:07
|-
|title15=Evelyns Birthday 
|note15= 
|length15=1:52
|-
|title16=Black Madonna 
|note16= 
|length16=1:21 	
|-	
|title17=Night Crickets
|length17=0:20
|-
|title18=Requiem For the Duke of Burgundy
|length18=4:36 		
|-	
|title19=Hautbois
|length19=2:10
|-
|title20=Coat of Arms
|length20=2:48
}}
{{Album ratings
|MC=82/100 
|rev1=MusicOMH
|rev1Score=  
}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 