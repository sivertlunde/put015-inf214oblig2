Ilusión Nacional
{{Infobox film
| name           = Ilusión Nacional
| image          =
| alt            =
| caption        =
| director       = Olallo Rubio
| producer       = Olallo Rubio Jose Nacif 
| writer         = Olallo Rubio
| narrator       =
| starring       =
| music          = Javier Umpierrez
| cinematography =
| editing        = Juan Fontana
| studio         =
| distributor    =
| released       =   }}
| runtime        = 100 minutes
| country        = Mexico
| language       = Spanish
| budget         =
| gross          =
}} association football in Mexico.

== Synopsis ==
Olallo Rubio traces the history of association football in Mexico, and he discusses how it relates to Mexican politics and society.

== Cast ==
* Olallo Rubio

== Production ==
Co-producer José María Yazpik said that previously shot footage comprises the films visual content, but there is footage that has never been seen before. 

== Release ==
Ilusión Nacional was released in Mexico on June 4, 2014,  and was timed to coincide with the 2014 FIFA World Cup. 

== Reception ==
Eric Ortiz Garcia of Twitch Film called it "little more than a recap of Mexicos history in the World Cup" and Rubios worst documentary to date. 

== References ==
 

== Further reading ==
*  
*  

== External links ==
*  

 
 
 
 
 
 
 


 
 