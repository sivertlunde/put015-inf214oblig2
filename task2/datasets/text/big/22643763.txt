Dik Trom en zijn dorpsgenoten
 
{{Infobox film
| name           = Dik Trom en zijn dorpsgenoten
| image          =
| image_size     =
| caption        =
| director       = Ernst Winar
| producer       =
| writer         = Ernst Winar
| narrator       =
| starring       =
| music          =  
| cinematography = Emiel van Moerkerken
| editing        =
| distributor    =
| released       = 9 December 1947
| runtime        = 60 minutes
| country        = Netherlands Dutch
| budget         =
}} 1947 Netherlands|Dutch film directed by Ernst Winar. The film was based on the popular childrens book series Dik Trom by Cornelis Johannes Kieviet.
 Dik Trom.

==Plot==
In a small grocery shop, Kee is trying to buy beans, but the shopkeeper (Mrs. Boon) refuses to sell it to her because Kee owes her money. Kee mentions that her husband is sick and is not able to make a living, but the shopkeeper still refuses. At home, Dik asks his mother for food to give it to Kee and her husband. His mother agrees and prepares a basket with some food. Willem, Kees husband, is ill and lying in bed. Right after Kee gave Willem some water and bread, Dik arrives with the basket, containing eggs, coffee and meat, which Kee gratefully receives.
 superstitious women out of Mr. Mulders house, while Mr. Mulder himself hides under a bed. Dik and his friends find him and force the scared and superstitious Mr. Mulder to promise to never call Kee a witch again.

The following day, Dik meets a girl named Nellie. He says to her that he tries to figure out how Kee and Willem can earn a living. Nellie thinks it could be possible with a barrel organ, which the local blacksmith (Mr. Van Driel) in town happens to be selling. Dik goes to Mr. Van Driel, Pieters father, and asks him for the price. The smith says he wants 25 guilders for it, and after hearing that Dik wants to give the barrel organ to Kee and Willem offers to donate 5 guilders. Dik visits the towns mayor to get a permission to collect the money. The mayor gives him permission, and donates 5 guilders. After the money is raised, Dik counts the collected money with Pieter. They find out they can buy the barrel organ, and still have 19 guilders in excess. Dik, Pieter and Jan go to Kees house and deliver the barrel organ with the additional 19 guilders.

Shortly after, Kees cousin Bastiaan visits Kee and Willem. After hearing about the barrel organ and money, he tries to extort the 19 guilders from Kee. Kee refuses, but after Bastiaan threatens to hit Willem, Kee gives him the money. Before leaving, Bastiaan damages the barrel organ after Willem called him an evil man. After that, Dik visits Kee and Willem again. He hears about what Bastiaan has done, and gathers a group of peers to find Bastiaan. Dik splits the group up, and gives every subgroup a homing pigeon to inform Jan about Bastiaans location if someone happens to find him. Dik and Pieter suspect Bastiaan is in an old warehouse and sneak in the building.

But just as they are in, they hear footsteps of Bastiaan and his friend and they decide to hide. While Bastiaan and the voddeman are drinking liquor, they suddenly hear the noises of the homing pigeon and find Pieter in a cupboard. Dik en Pieter manage to escape, and lock themselves up in a small room. They just have enough time to send the homing pigeon to Jan in order to inform him of their location before they have to open the door. In his dovecote, Jan reads their message and gathers his friends with his trumpet. He informs them of Bastiaans location and they set out to the old warehouse. Meanwhile in the old warehouse, a comical fight starts between Dirk and Bastiaan and their friends. Just as Dik and Pieter are overpowered, Jan and his friend enter the room and start attacking Bastiaan and his friend. After Bastiaan and his friend are overpowered, Dik forces them to give back the stolen money.

After that, Willem and Kee are playing on their repaired barrel organ in front of Dik and his parents, before complimenting Diks parents on their sons behaviour.

==Cast==
* Theo Frenkel as the mayor
* Douwe Koridon as Dik Trom
* Elsje Niestadt
* Jules Verstraete as Flipse
* Alex de Meester
* Jo Vischer
* Clara Vischer-Blaaser
* Coby Ridder
* N.R. Josso
* Henny Schouten

==References==
 

== External links ==
*  

 

 
 
 

 