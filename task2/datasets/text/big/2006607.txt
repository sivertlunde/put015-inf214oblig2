Green for Danger (film)
{{Infobox film
| name = Green for Danger
| image =Green For Danger.JPG
| caption =Green for Danger film poster
| director = Sidney Gilliat
| producer = Frank Launder Sidney Gilliat
| writer = Christianna Brand (novel) Sidney Gilliat Claud Gurney
| starring =Alastair Sim Leo Genn Sally Gray Trevor Howard
| music = William Alwyn
| cinematography = Wilkie Cooper
| editing = Thelma Myers
| studio = Individual Pictures
| distributor = General Film Distributors
| released =  
| runtime = 91 min.
| country = United Kingdom
| language = English
| budget =
}} detective novel of the same name by Christianna Brand. The film was directed by Sidney Gilliat and stars Alastair Sim, Trevor Howard, Sally Gray and Rosamund John. The film was shot at Pinewood Studios in England. The title is a reference to the colour-coding used on Anaesthetic|anaesthetists gas bottles.

==Plot==
In August 1944, during the V-1 flying bomb|V-1 Doodlebug offensive on London, a murder is committed in Herons Park Emergency Hospital, a rural British hospital somewhere in the Southeast of England.  Joseph Higgins (Marriott) dies on the operating table after being injured by a flying bomb. The anaesthetist, Barney Barnes (Howard), has had a patient die in similar circumstances previously.

Inspector Cockrill (Sim) is asked to investigate when Sister Bates (Campbell) is killed after revealing that the death of Higgins was not an accident. Cockrill states at one point: "My presence lay over the hospital like a pall&nbsp;– I found it all tremendously enjoyable." Cockrills investigation is hampered by the conflict between Barnes and Eden (Genn) because of their competition over the affections of nurse Freddi (Gray). After another murder attempt is directed at Freddi, the inspector restages the operation in order to unmask the murderer.

==Cast==
*Sally Gray as Nurse Frederica Freddi Linley
*Trevor Howard as Dr. Barney Barnes
*Rosamund John as Nurse Esther Sanson
*Alastair Sim as Inspector Cockrill
*Leo Genn as Mr. Eden
*Judy Campbell as Sister Marion Bates
*Megs Jenkins as Nurse Woods
*Moore Marriott as Joseph Higgins (the Postman) Henry Edwards as Mr. Purdy Ronald Adam as Dr. White George Woodbridge as Detective Sergeant Hendricks
* Wendy Thompson as Sister Carter 
* John Rae as The Porter 
* Frank Ling as Rescue Worker

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 