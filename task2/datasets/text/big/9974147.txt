One Potato, Two Potato
 
{{Infobox film
| name           = One Potato, Two Potato
| image	=	One Potato, Two Potato FilmPoster.jpeg
| caption        = Film poster
| director       = Larry Peerce
| producer       = Sam Weston
| writer         = Orville H. Hampton Raphael Hayes
| starring       = Barbara Barrie Bernie Hamilton
| music          = Gerald Fried
| cinematography = Andrew Laszlo
| editing        = Robert Fritch
| distributor    = Cinema V
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
}}
One Potato, Two Potato is a 1964 black-and-white American drama film directed by Larry Peerce and starring Barbara Barrie and Bernie Hamilton.

==Plot==
Julie Cullen is a young parent, single for the past four years, since her husband abandoned her and their daughter Ellen, only a year old at the time.  At work, Julie, who is white, meets Frank Richards, who is black, and the two strike up a friendship that blossoms into a romance.  Their relationship is strained by the racial prejudices of many around them, including Franks parents, William and Martha, who oppose the pairing.  But ultimately Frank and Julie decide to persevere through such difficulties.  They marry, and Julie and Ellen move in with Frank and his parents.  Ellens arrival immediately softens Marthas heart, but William remains cool toward Julie, steadfast in his belief that Frank and Julies marriage is a foolish endeavor.  His attitude only changes when Frank and Julie have a son together.  When William first holds his new grandson, he loses any remaining animosity and the household becomes a happy one for all.

Eventually, Julies ex-husband Joe returns, seeking to establish a visitation relationship with Ellen.  However, when he finds that Julie and Ellens new family is black, he finds this unacceptable and petitions the court for legal custody of Ellen.  Franks lawyer tells him that Joe is likely to win.  Agreeing with the lawyers analysis, William advises Frank to take Julie and the children and flee the state.  Frank, however, decides to stay fight the case in court.  When Julie appeals to Joe directly, it only angers him, and he even briefly attempts to force himself on Julie physically.  When Frank learns what has happened, he is intensely frustrated by his inability to defend his wife by directly confronting Joe, since he knows that will be the end of whatever small chance he and Julie have winning the custody case.

The judge in the case looks carefully into Ellens family situation, including interviewing her directly.  She affirms how much she loves Frank and she seems oblivious to the racial issues at play.  When the judge asks her about her baby brother being "different" from her, the only thought that occurs to her is that her brother is a boy, while she is a girl.  The judge recognizes that the family situation in the Richards home is superior for Ellen in every way except for the fact that she is white, growing up in a black household.  While the judge does not condone racial prejudices and agrees that they should be fought, he also says that he cannot ignore that they exist and, if Ellen remains with Frank and Julie, will negatively impact her when she reaches adulthood.  For that reason, he grants Joes petition for custody.

When Joe arrives to pick up Ellen, she is excited, initially under the impression that her father is taking her for a short visit from which she will soon return.  When she finally realizes that shes being sent to live with him permanently, and that her brother is remaining behind, she assumes that she is being punished for having misbehaved in some way.  Joe loads Ellen and her clothes into a taxi as the family looks on in sorrow.  As the taxi drives away with Joe and Ellen in the back seat, she helplessly presses her face against the cars rear window, shouting back to her mother, pleading to be allowed to stay and promising that she will be a good girl.

==Additional information==
It was shot in its entirety in and around the small northeastern Ohio city of Painesville, Ohio|Painesville.   
 Best Actress award at the 1964 Cannes Film Festival.   

The screenplay, written by Orville H. Hampton and Raphael Hayes, was also nominated for Best Original Screenplay at the 1964 Academy Awards, losing to eventual winners S. H. Barnett, Peter Stone and Frank Tarloffo for Father Goose.

==Cast==
*Barbara Barrie as Julie Cullen Richards
*Bernie Hamilton as Frank Richards
*Richard Mulligan as Joe Cullen
*Harry Bellaver as Judge Powell
*Marti Mericka as Ellen Mary
*Robert Earl Jones as William Richards
*Vinnette Carroll as Martha Richards
*Sam Weston as Johnny Hruska
*Faith Burwell as Ann Hruska
*Jack Stamberger as The Minister
*Michael Shane as Jordan Hollis

==See also==
*List of American films of 1964

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 