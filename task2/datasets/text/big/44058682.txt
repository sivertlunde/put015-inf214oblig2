Manoradham
{{Infobox film
| name = Manoradham
| image =
| caption =
| director = P. Gopikumar
| producer = KH Khan Sahib
| writer = Jayasankar
| screenplay = Jayasankar Sharada Sankaradi K. P. Ummer
| music = V. Dakshinamoorthy
| cinematography = Anandakkuttan
| editing = K Sankunni
| studio = Kanthi Harsha
| distributor = Kanthi Harsha
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by P. Gopikumar and produced by KH Khan Sahib. The film stars P. Bhaskaran, Sharada (actress)|Sharada, Sankaradi and K. P. Ummer in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*P. Bhaskaran  Sharada 
*Sankaradi 
*K. P. Ummer 
*Ravi Menon  Seema 
*Vidhubala

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Chirakaala kaamitha || Vani Jairam || P. Bhaskaran || 
|- 
| 2 || Kazhinja Kaalathin || K. J. Yesudas, Ambili || P. Bhaskaran || 
|- 
| 3 || Maanasa Souvarna || K. J. Yesudas || P. Bhaskaran || 
|- 
| 4 || Madhura Swarga || K. J. Yesudas, Vani Jairam || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 