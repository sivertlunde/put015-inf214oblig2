Running on Empty (1982 film)
 
{{Infobox film|
  name     = Running On Empty |
image = Fast-Lane-Fever.jpg |
caption = Film poster under American title |
    writer         = Barry Tomblin |
  starring       = Terry Serio, Deborah Conway, Max Cullen |
  director       = John Clark |
  producer       = Pom Oliver |
  distributor    = Roadshow |
  released   = 1982|
  runtime        = 83 minutes |
  country = Australia |
  language = English |
  music          = |
  awards         = |
  budget         = A$2 million Philippa Hawker, "Running on Empty", Australian Film 1978-1992, Oxford Uni Press, 1993 p109  |
gross = A$1,218,000 (Australia)|
}}

Running On Empty (released in America as Fast Lane Fever) is a 1982 Australian film. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p361 

==Plot== street racer, Ford Falcon GT-HO Phase III. His best mate and mechanic, Tony (John Agius), are both steel workers by day, but when they arent working, they are racing.
 Dodge Challenger; Holden Monaro, who then, following the race loses control of his car in anger and ends up perishing in a fiery crash, no one has the stomach to race.....Fox is now facing a drought of racing and therefore money.

Julie is the voyeuristic young girl portrayed by singer Deborah Conway, and seemingly involved with Fox. Mike fancies her, and the feeling is mutual. Fox takes advantage of this weakness, and pursues Mike, and basically tells him that if he wants whats his (Julie) he has to race for it.
 351 Cleveland) EK Holden, who they knew they would beat and go on to do so. They agree on a double or nothing race for the next day.

The next morning they find themselves at Rebels garage, who they crossed paths with the day before, and they realise that Rebel (Max Cullen) is blind, but still has a great passion for life and cars. He still drives his prized blown 1957 Chevrolet|57 Chevy coupe, with the help of his wife Joan (Annie Semler).

Mike then sets out to find the racers from yesterday, with them leading Mike, Tony and Julie into a trap, where the racers threaten to burn the Falcon to the ground with them in it unless they give them back their money that they lost the day before. They proceed to get angrier wetting the car with gasoline and then setting it on fire, with Mike trying to drive away and in the process roll the car a few times, but managed to start it up again and drive the car into a shallow lake to quench the fire.

They arrive back at Rebels, the car barely moving, and Rebel allows them to stay just as long as it takes to get the car fixed.
Over the course of a few weeks, Rebel, Mike and Tony repair the car back to its former glory and after a few days of testing the car and tuning it with nitrous they return to the city to race Fox once again.

As the cars line up at the start line (The brick-yards in Homebush, New South Wales|Homebush, Sydney) As they set of and reach redline speed in what seems like a setup from Mikes point of view but was really just an accident, a truck pulls out in front of Fox and Mike during the race, destroying the Falcon and injuring Mike.

After Mikes recovery in hospital, it seems he has given up—until Tony is bashed by Foxs head thug for riding a bicycle after Fox told them "no wheels and off the street". Mike confronts Fox and challenges him to a race. Mike plans to race and bet Rebels Chevy, he knows it will beat the Dodge - he just has to convince Rebel to let him use it.
 Kemps Creek instead, which is a mile long race, figuring that he has the speed advantage.

After the eventful race through Kemps Creek and Mikes eventual victory, there is a dramatic sort of stand-off between the two, and Fox, possibly coming to terms that he lost, drives the Dodge full speed into a wall, killing himself and destroying the car, as if to make the point that Mike would never have his car. The movie ends with Mikes not so fussed reaction to this event and fades into the credits.

According to a very reliable source,  the XY hill jump after Mikes first loss to Fox was done somewhere around Austinmer/Thirroul in NSWs Southern Highlands. The Rocks/Pyrmont, Pyrmont in Five Dock and Wollongong. The Kemps Creek race was apparently not filmed in Kemps Creek according to a resident  of the area.

==Cars==

* Ford Falcon GTHO Phase III
* Dodge Challenger
* 57 Chevy

==Production==
The film was funded partly by the Film Corporation of Western Australia.

==Box Office==
Running On Empty grossed $1,218,000 at the box office in Australia,  which is equivalent to $3,544,880 in 2009 dollars.

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
*  at Oz Movies
* 

 
 
 
 
 
 
 