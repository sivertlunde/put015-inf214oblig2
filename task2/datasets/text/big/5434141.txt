Footrot Flats: The Dog's Tale
 
 
 
{{Infobox Film
| name           = Footrot Flats: The Dogs  Tail  Tale
| image          = Footrot Flats movie.png
| caption        = Poster for Footrot Flats: The Dogs  Tail  Tale
| director       = Murray Ball
| producer       = John Barnett Pat Cox Tom Scott
| based on       = Footrot Flats by Murray Ball John Clarke Peter Rowley Rawiri Paratene Fiona Samuel Peter Hayden
| music          = Dave Dobbyn
| cinematography = 
| editing        = Michael Horton Dennis Jones
| studio         = Magpie Productions Ltd.
| distributor    = Kerridge-Odeon
| released       = 1986
| runtime        = 71 minutes
| country        = Australia New Zealand
| language       = English
| budget         = 
| gross          = $2,500,000
| preceded_by    = 
| followed_by    = 
}}
 Australian animated film, based on cartoonist Murray Balls popular comic strip, Footrot Flats. It was New Zealands first feature-length animated film.

==Plot== John Clarke) and Cooch Windgrass (Peter Hayden) are shearing sheep on Wals farm. Cheeky Hobson (Fiona Samuel), Wals girlfriend, is driving on the highway when she is driven off the road by the Murphy brothers, Spit (Brian Sergent) and Hunk (Marshall Napier), flying in a helicopter. The Murphys then terrorise Wals property, leaving Dog (Peter Rowley) to drown in a sheep pit. As he is in the water, he has a flashback of when he was a little pup: how he was united with Wal for the first time, a gift from Aunt Dolly (Dorothy McKegg), and how he met Jess, who was nearly drowned by Spit and Hunk. Soon, Wal wakes Dog and Cooch and the dogs manage to foil the Murphys, who were in the process of capturing Coochs deer.

Later, Wal finds Rangi Jones (Rawiri Paratene) and Pongo Footrot (Fiona Samuel) playing catch with a rugby ball, and so he joins in to coach them. After a while, Rangi and Pongo tell Wal about an All Blacks selector coming to watch an upcoming rugby match, and Wal daydreams about being an All Black. Over the next few days, Wal works out and goes on a date with Cheeky. Wal takes her to a caravan restaurant selling fast food, run by Pawai (Billy T. James). Dog bursts in on them, thinking that Cheeky is trying to poison Wal, but ends up ruining the dinner after Cheeky vows never to see Wal ever again.

Wal leaves Dog tied up outside as punishment, and Dog is later attacked by rats but is rescued by the resident stray cat, Horse. It begins to rain heavily, and Jess is knocked out of her box and is lost somewhere near the Murphys. Meanwhile the Murphys, under the direction of Irish (Peter Hayden), steal Coochs stag under the cover of darkness. While Wal tries to move the bull, Wals goose tries to bite Wal on the butt (a running gag) and Rangi and Dog try to move the sheep, Rangi notices that Jess is gone. He tries to get help from Wal who refuses, his only concern being to play in the rugby game. Rangi and Dog decide to go looking for Jess themselves but are later separated. Rangi decides to go and get help from Wal, while Dog goes and looks for Jess, literally following in her footsteps. As the storm starts dying down, Cooch goes to feed his deer, but notices his stag is missing.

The next day, Rangi still tries to get help from Wal, but Wal wants "His Big Chance" to become an All Black, and so drives to the local rugby pitch with both Rangi and Pongo. During both the drive and the game (played against the Mill team and their star player Spit Murphy), Rangi tries to come up with a plan to get Wal to the Murphys. When one of Wals players are sent to the hospital wing, Rangi chooses to play with Wal in order to steal the ball and get Wal to follow him. When Rangi grabs the ball and runs off, Wal follows him, but only because Pongo mentioned that the Murphys stole Coochs stag, after eavesdropping on their conversation.

Meanwhile, Dog finds Jess under attack by rats, led by their leader, Vernon the Vermin. Soon, while running from the Murphys dogs, Dog kills Vernon with a log. Rangi hitches a ride on the top of the Murphys van and later arrives at the Murphys farm. Rangi is captured by Irish Murphy and locked in a shed. Irish decides to kill Dog, Jess and Horse by following them to the river with a gun. Wal and Pongo arrive. Pongo goes to help Rangi, while Wal chases after Irish and Wals goose chases after Wal. Spit Murphy tries to help by taking the helicopter, but its destroyed and Spit is captured by Pongo and Rangi. Irish manages to shoot Horse, while Wal swings in to get Murphy. First he falls in a mud pit, then he saves Irish from drowning and then finally ends up getting bitten "on the freckle" by his goose.

Dog saves Jess from crocopigs, with the unconscious Horse on the raft. Later, they float to a bridge, where Wal, Pongo and Rangi attempt to bring them up. Fortunately, Rangi manages to get a hold of Jess, but misses Dog and Horse. They pick up Cooch and decide to try down the beach, but no luck. They all leave, with the exception of Jess. After they leave, Jess starts barking, so Wal, Cooch, Pongo and Rangi turn back. Dog and Horse, now recovered, are still alive and arrive at shore.

The movie ends with Dog and Jess walking past Major, Wals pig dog, with puppies in tow.

==Cast== John Clarke - Wal
*Peter Rowley - Dog
*Rawiri Paratene - Rangi  
*Fiona Samuel - Cheeky Hobson / Pongo 
*Peter Hayden - Irish Murphy 
*Dorothy McKegg - Aunt Dolly 
*Billy T. James - Pawai 
*Brian Sergent - Spit Murphy 
*Marshall Napier -  Hunk Murphy 
*Michael Haigh - Rugby Commentator

==Production==
The film began as an idea from producer Pat Cox who suggested to Murray his popular comic strip be made into a feature length movie. Once Murray and writer Tom Scott had worked together to complete the script animator Robbert Smit was brought on to ensure the animation was successful. The main challenge faced in the animation was to get the voice acting right so that audiences who had created their own voice for the characters through the comic strip could relate it. This was solved by employing a cast of New Zealands best comedy talent at the time. 

==Soundtrack==
 
Featuring songs and music by New Zealand musician  ", and "Slice of Heaven" (featuring Herbs (band)|Herbs), which was popular in both New Zealand and Australia.

==Reception==
When screened in Los Angeles, California in 1987, Charles Solomon gave Footrot Flats: The Dogs Tale two and a half stars out of four.  "The raunchy humour," he said, "may surprise American audiences accustomed to the sanitized jokes of Saturday-morning kidvid." 

===Box office===
Footrot Flats: The Dogs Tale grossed $2,500,000 at the New Zealand box office (making it one of the most successful local movies of the 1980s). 
In Australia it grossed $4,317,000   which is equivalent to $8,677,170 in 2009.

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- World Animation Celebration Animated Works Over 30 Minutes Murray Ball
| 
|- New Zealand Film and TV Awards Best Original Screenplay
| 
|- Tom Scott Tom Scott
| 
|- Best Film Score Dave Dobbyn
| 
|- Best Contribution to a Film Soundtrack John McKay
| 
|-
|}

==See also==
*Cinema of New Zealand
*Cinema of Australia
*List of animated feature-length films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 