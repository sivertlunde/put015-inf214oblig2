It Pays to Advertise (film)
{{Infobox film
| name = It Pays to Advertise
| image =File:It Pays to Advertise lobby card.jpg
| caption =Lobby card
| director = Frank Tuttle
| producer =
| writer = Ethel Doherty Arthur Kober Norman Foster Carole Lombard
| music =
| cinematography = Archie Stout
| editing =
| distributor = Paramount Pictures
| released =  
| runtime = 63 min
| language = English
| budget =
| country = United States
}}
 play of Norman Foster and Carole Lombard, and directed by Frank Tuttle.

==Plot==
Rodney Martin sets up a soap business to rival his father. With the help of an advertising expert and his secretary, Mary, he develops a successful marketing campaign. His father ends up buying the company from him, while Rodney and Mary fall in love.   

==Cast== Norman Foster as Rodney Martin
*Carole Lombard as Mary Grayson
*Richard Skeets Gallagher as Ambrose Pearle
*Eugene Pallette as Cyrus Martin
*Louise Brooks as Thelma Temple

==Reception==
The film received positive reviews. Photoplay wrote that it has "plenty of speed and lots of laughs", and praised the "perfect cast". 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 