Unstrung Heroes
{{Infobox film
| name        = Unstrung Heroes
| image       = Unstrung Heroes poster.jpg
| caption     = Unstrung Heroes DVD cover
| writer      = Screenplay:  
| starring    = Andie MacDowell John Turturro Michael Richards
| director    = Diane Keaton
| editing     = Lisa Zeno Churgin
| music       = Thomas Newman
| cinematography = Phedon Papamichael Jr.
| producer    = Susan Arnold Bill Badalato Donna Roth
| studio      = Hollywood Pictures Buena Vista Pictures
| released    =  
| runtime     = 93 minutes
| country     = United States
| language    = English
| budget      =
| gross       = $7,929,434
| awards      =
| tagline     =
}} American comedy-drama film directed by Diane Keaton. The screenplay by Richard LaGravenese is based on a memoir by journalist Franz Lidz.

==Plot synopsis== pack rat paranoid Danny, in their cluttered apartment in the rundown King Edward Hotel. The two, who live in a setting worthy of the Collyer brothers, rechristen the boy with the more colorful name Franz and help him cope with his emotions by teaching him to value his own uniqueness. Learning from the odd pair that even though hope and science may fail us, art always survives, Franz secretly begins to create a memorial to his mother before she dies, filling a box with personal mementos&nbsp;— a tube of lipstick, an empty Chanel bottle, a cigarette lighter, and the like.

==Production notes==
The film shifted the original storys setting of New York to Southern California, and the four uncles from the memoir were condensed into two.   
 documentary Heaven (1987 film)|Heaven and the 1991 television movie Wildflower.

The film debuted at the 1995 Cannes Film Festival in May.    Prior to its release, it was shown at the Toronto International Film Festival and the Boston Film Festival, both held in September 1995. 

During its widest release in the US, the film played at only 576 theaters. It grossed $7,929,000. 

==Principal cast==
*Andie MacDowell as Selma Lidz
*John Turturro as Sid Lidz
*Michael Richards as Danny Lidz
*Maury Chaykin as Arthur Lidz
*Nathan Watt as Steven/Franz
*Celia Weston as Amelia
*Candice Azzara as Joanie Jack McGee as Lindquist

==Critical reception==
The film earned positive reviews from critics, holding a 74% rating on Rotten Tomatoes based on 23 reviews.
 Love Story." He added, "Someday somebody may find a cure for cancer, but the terminal sappiness of cancer movies is probably beyond remedy." 

Todd McCarthy of Variety (magazine)|Variety wrote: "A coming-of-age piece that is slight to the point of anemia, "Unstrung Heroes" sports a willful eccentricity that almost immediately becomes annoying. Diane Keatons debut dramatic feature aims for a distinctively offbeat tone that never really gels and the movies emotional power, stemming from personal growth through family tragedy, falls short of the goal as well." 

In her review in The New York Times, Janet Maslin called it "a warm, surprising, gently incandescent film . . .   becomes a celebration of quirky independence and the sustaining powers of art and memory . . .   also succeeds in becoming very moving without being maudlin . . .   screenplay runs the risk of being generically uplifting, even bland; instead, it has a sharply distinctive flavor, honest pathos and a hint of delightful household magic. Thomas Newmans sparkling musical score echoes that buoyant tone." 

Roger Ebert of the Chicago Sun-Times said the film "has been directed by Diane Keaton with an unusual combination of sentiment and quirky eccentricity. There are moments so touching that the heart almost stops." 
 To Kill a Mockingbird . . .   is that rare mainstream film that doesnt shout in our ear to make its points. It draws us in, subtly and gracefully, and casts a lingering charm." 

Rita Kempley of the Washington Post described it as "a sensitive coming-of-age story in the sublime tradition of My Life as a Dog." 

In Rolling Stone, Peter Travers opined, "Steven Lidz, a 12-year-old growing up in New York during the 60s with a dying mother and a distracted father, turns to his loony but life-affirming uncles. Sugar shock? Wait. Jump ahead to 1991. Steven, now Franz Lidz and a writer for Sports Illustrated, wins acclaim for a childhood memoir that doesnt choke on whimsy or schmaltz. Wait again. The film version, directed by Diane Keaton from a script by Richard LaGravenese (The Bridges of Madison County), isnt above a little tear-jerking. Theres also the matters of reducing Stevens four unstrung uncles to two, Danny (Michael Richards) and Arthur (Maury Chaykin); casting Andie MacDowell as Selma, Stevens Jewish mother; and shooting this Lower East Side story in Pasadena, Calif. Guess what? The movie works like a charm." 

===Box Office===

The movie performed well at the box office. 

==Awards and nominations== Grammy Award American Comedy Award for Funniest Supporting Actor in a Motion Picture.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 