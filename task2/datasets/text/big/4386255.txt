Madison (film)
{{Infobox film
| name           = Madison
| image          = Madisonposter.jpg
| caption        = Theatrical release poster
| director       = William Bindley
| producer      = William Bindley and
                  Carl Amari
| writer         = William Bindley Scott Bindley
| starring       = Jim Caviezel Jake Lloyd Mary McCormack Bruce Dern Brent Briscoe
| music          = Kevin Kiner
| cinematography = James Glennon
| editing        = William Hoy
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 APBA Hydroplane hydroplane racing in the 1970s that is based on a true story. Produced by Carl Amari, it stars Jim Caviezel as a driver who comes out of retirement to lead the Madison, Indiana community-owned racing team.

==Background== Unlimited hydroplane Seattle (WA), Kennewick (WA), Detroit (MI), San Diego Doha (Qatar).

The Regatta regularly draws about 70,000-100,000 people and is a tremendous source of pride for residents of the town. Also significant is that Madison has the worlds only community owned unlimited hydroplane, Miss Madison.  The boat was traditionally near the bottom of the circuit. In 40+ years of racing, U-6 (its number regardless of its name) had won just six races before 2005.

One of those was an upset in the 1971 Regatta, which is the basis for the movie. Making that victory even sweeter was that it was also for the APBA Gold Cup.

Caviezels character, Jim McCormick, was a real-life veteran racer and boat owner who drove Miss Madison in 1966 and 1969–71, then raced his own boat until seriously injured some years later. Many of his actual seven-man pit crew, including Harry Volpi, Bobby Humphrey, and Tony Steinhardt, were also portrayed in the film, while Steinhardt himself appeared as a fan in a cameo.

==Cast and characters==
*Jim Caviezel as Jim McCormick
*Jake Lloyd as Mike McCormick
*Mary McCormack as Bonnie McCormick
*Bruce Dern as Harry Volpi
*Paul Dooley as Mayor Don Vaughn
*Brent Briscoe as Tony Steinhardt
*Mark Fauser as Travis
*Reed Diamond as Skip Naughton
*Frank Knapp Jr. as Bobby Humphrey
*Chelcie Ross as Roger Epperson
*Jim Hendrick as Himself
*John Mellencamp as Narrator (adult Mike McCormick)
*Brie Larson as Racing Girl 2
*Carl Amari as Jake the Banker

==Delayed release==
Filmed in 2000 and completing post production in 2001, this film sat unreleased for nearly five years and was finally given a limited release on April 22, 2005. It ended up being the last film ever released by MGM as an independent company.

==External links==
* 
* , actual story of the 1971 Miss Madison

 
 
 
 
 
 
 
 