Hack Job
{{multiple issues|
 
 
}}

{{Infobox film
| name = Hack Job
| image = 
| alt = 
| director = James Balsamo
| producer = Lloyd Kaufman
| writer = James Balsamo
| starring = Debbie Rochon Lloyd Kaufman Lynn Lowry Dave Brockie
| music = Nightmare Sonata
| cinematography = Joseph White
| released =  
| runtime = 80 minutes
| country = United States
| language = English
| budget = $60,000
| gross = 
}}

Hack Job is an American horror-thriller film directed by James Balsamo and produced by Lloyd Kaufman. It stars Dave Brockie, Lloyd Kaufman, & Debbie Rochon. Nightmare Sonata provides music for the film. It was released on DVD on 2011 with plans for a 2012 Theatrical release.

==Cast==
*Debbie Rochon as Astral Angel
*Lloyd Kaufman as Rabbi Lloyd / Himself
*Lynn Lowry as Talent Agent
*Dave Brockie as Duke / Defendor
*James Balsamo as James Argento / Allen / Butch / Mark
*Michael Shershenovich as Mike Fulci / The Russian / Pete
*Keith J. Crocker as Keith the Bum (attached)
*Cathy Nardone as Round two girl
*Robert Youngren as Professor Heart / Bub Bob Socci as Gunther / Hunchy the Hunchback
*Michael Matteo as Ben
*Michael Balsamo as Steve / Johnny Walker
*Jon Keller as Lester
*Veronika Brussmannova as Betty
*Dan E. Danger as Danny Danger / Mummy / Ronnie
*Keith Matturro as Keith II. the Bum
*Kerri McConnell as Mindy / Donna / Angel
*Michael Perillo as Deamon / Dave
*Sparky Drakonis as Himself
*Daniel Underhill as Hanz
*Danny Karron as Pretentious Director / Harvy
*Matt Routzahn as Rumble
*Dominae Drakonis as Monster Victim
*Chainsaw Mike as Slick
*Michael Martinez as Georgio
*Jeremy Sunderland as Raa the Pharaoh

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 