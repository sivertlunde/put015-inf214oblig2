Sisters on the Road
{{Infobox film
| name           = Sisters on the Road 
| image          = File:Sisters_On_The_Road_poster.jpg
| film name = {{Film name
 | hangul         =  ,  
 | hanja          =  , 이대로가 좋아요 
 | rr             = Jigeum, idaeroga joayo
 | mr             = Chigŭm, idaeroga choayo}}
| director       = Boo Ji-young 
| producer       = Park Soon-hong 
| writer         = Boo Ji-young 
| starring       = Shin Mina Gong Hyo-jin 
| music          = Choi Seung-hyun
| cinematography = Kim Dong-eun 
| editing        = Kim Su-jin
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Sisters on the Road ( ; lit. "Now, things are just fine as they are") is a South Korean independent film written and directed by Boo Ji-young,  and starring real-life best friends Shin Mina and Gong Hyo-jin.  
 released in South Korean theaters in 2009.    

==Plot== abandonment issues, Myung-eun tells Hyun-ah she wants to start looking for her father after the funeral. Single-minded in her desire to dig up memories of her father and discover why he left, Myung-eun resents that Myung-ju, who like their mother is a carefree fish trader and an unmarried mother of a young daughter, seemingly doesnt care. At first Myung-ju is reluctant to accompany Myung-eun, but after Hyun-ah persuades her, guilt and her sense of duty as an older sibling prevails. And so the two sisters who are dissimilar in character, lifestyle and even fathers go on a road trip together. On their trip, Myung-eun and Myung-ju quarrel over their differences, share secrets, reminisce about their past, and eventually embrace each other as family.

Director Boo Ji-young captures the delicately subtle atmosphere floating between women, and how inscrutable life is.  

==Cast==
*Shin Mina ... Park Myung-eun
*Gong Hyo-jin ... Oh Myung-ju
*Kim Sang-hyun ... Hyun-ah
*Chu Kwi-jung ... Hye-sook 
*Moon Jae-woon ... Hyun-sik 
*Bae Eun-jin ... Seung-ah

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 


 