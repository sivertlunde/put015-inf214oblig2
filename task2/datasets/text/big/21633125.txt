Reconstruction (2003 film)
{{Infobox Film
| name           = Reconstruction
| image          = Reconstruction 2003film.jpg
| caption        = 
| director       = Christoffer Boe
| producer       = Tine Grew Pfeiffer
| writer         = Christoffer Boe Mogens Rukov
| starring       = Nikolaj Lie Kaas Maria Bonnevie Krister Henriksson Nicolas Bro Thomas Knack
| cinematography = Manuel Alberto Claro
| editing        = 
| studio         = Nordisk Film
| distributor    = 
| released       = September 26, 2003
| runtime        = 91 minutes
| country        = Denmark
| language       = Danish Swedish
| budget         = 
| gross          = 
|}}

Reconstruction is the psychological romantic drama film and the debut of Christoffer Boe, who also wrote the screenplay together with Mogens Rukov. It was filmed in Copenhagen and won the Camera D’Or at the Cannes Film Festival in 2003 Golden Plaque for Manuel Alberto Claros luminous wide-screen cinematography.

==Plot== Danish photographer with a Stockholm-bred girlfriend, Simone (Maria Bonnevie).

Late one evening Alex suddenly abandons his girlfriend, Simone, to pursue the beautiful Aimee (Maria Bonnevie). In his encounter with Aimee time and place dissolve for him and he becomes a stranger to Simone, to whom he cannot return.

“It’s all a film. It’s all a construction,” announces the narrator,  who is soon revealed to be a noted Swedish author, August (Krister Henriksson), as well as the tale’s apparent inventor.

==Shooting== Boe doesnt work with storyboards or set schedules. 
 Super 16 Arri SR3 using three different stocks. Then the film was scanned, color-graded, and digitally masked to CinemaScope. The scan was a simple one-light, and the team did no color correction, the opposite of todays trend to perform a digital intermediate. They also pushed the emulsion for extra grain. 

==Trivia==
* In one interview Christoffer Boe said that the film was inspired by Jacques-Henri Lartigues photograph of a woman standing in a room with empty bookcases. "Looking at that picture, I immediately felt I wanted to do a story about a man who comes home and his apartment has disappeared. And I knew it had to be a love story". 

* Characters do not use mobile phones or other means of modern communication. However, the story takes place nowadays. This emphasizes the fact that the plot is artificial and constructed.

* The scene where Alex and Aimee are talking in a cafe was shot in the Bobi Bar which is one of Christoffer Boe|Boes favorite and is close to where he lives.

==Cast==
*Alex David -- Nikolaj Lie Kaas
*Simone,Aimee -- Maria Bonnevie
*Leo Sand -- Nicolas Bro
*Monica -- Ida Dwinger
*Nan Sand -- Helle Fagralid
*Journalist -- Isabella Miehe-Renard
*Fru Banum (Mrs. Banum) -- Malene Schwartz
*Mel David -- Peter Steen
*Tryllekunstner -- Klaus Mulbjerg
*August Holm -- Krister Henriksson
*Mercedes Sand -- Mercedes Claro Schelin
*Waiter -- Jens Blegaa
*Bartender -- Katrin Muth
*Bartender -- David Dencik

==Soundtrack==
* Samuel Barber - "Adagio for Strings" Night & Day"
* Charles Paul Wilp - "Madison Avenue Perfume Ad"

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 