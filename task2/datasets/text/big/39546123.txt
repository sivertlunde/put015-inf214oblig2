Brahma (2014 film)
 
 

{{Infobox film name = Brahma image =  caption =  director = R. Chandru producer = Manjunath Babu screenplay = R. Chandru story = R. Chandru starring =Upendra Pranitha Subhash Nassar Shayaji Shinde music = Gurukiran cinematography = Shekar Chandra  editing = K M Prakash studio = Mylari Enterprises released =  
|country= India
|language= Kannada   Telugu runtime =  budget =   250&nbsp;million  gross =   235&nbsp;million (till 18 feb) 
}} Action list historical drama Kannada and Telugu and subsequently dubbed into Tamil language|Tamil.  The film stars Upendra, playing dual roles, and Pranitha Subhash in the lead roles, while actors Nassar, Rahul Dev and Shayaji Shinde play other prominent roles. The film spanning four generations, from the 16th to the 21st century, features an original soundtrack by Gurukiran. Its Karnataka Distribution rights have been sold out for a record price of   150&nbsp;million.  Brahma is one of the most expensive Kannada movies of all time.  The film opened to an overwhelming response from the audience as well as the critics and got a grand opening at the Box Office by collecting a Record-Breaking   97.5&nbsp;million in its first weekend by releasing in a record 276 screens and 1076 shows all over Karnataka.  It was labeled as an out an out commercial action entertainer with all the elements of keeping the audience entertained, particularly in the second half.  The bilingual movie is now all set to release in Telugu language in March.  Brahma completed 50 days in a Grand fashion in 35 theatres making   2 Million even on the 50th day.  The film was declared a blockbuster at the box office. It completed 75 Days.  The film was dubbed in hindi as The Real Leader- Brahma.

==Plot==
The film opens with a lady is coming India from Malasyia . She takes Taxi and wants to go to Brahmas place.
The driver then starts telling Brahmas life.the movie goes into flackback.In 1970, Veer Brahma(Nassar) kills two rich man.Then his wife gives birth to a son.The movie then comes back in 2014,ACP Shinde(Sayaji Shinde) is handed the case of Brahma.Pranitha comes and tells her story - Pranitha(Pranitha Subhash) is a girl from Malasyia.She lives with her parents in Malasyia.She saw Brahma(Upendra) helping a beggar.After some events she falls in love with Brahma.Then Lucky man (Rangayana Raghu)  a millionaire was also fooled by Brahma.In Malasyia Bramha killed a international don named Rahul Dev(Rahul Dev) .Now we see that Brahma is a Don.Brahma goes to loot his house.There Brahma is living with a new name,Upendra.He works in his fathers house.Then We see some comedies of Sadhu(Sadhu Kokila),who is the leader of the servants in the house. Then Pranitha and Lucky man comes in that house.One day Brahma is caught by ACP Shinde in that house.Then Brahmas grandfather reveals their past.In 1600 BC A warrior named Brahma (Upendra again) is very kind to poor .He gives all his wealth to the poors.But In 1950,Their family becomes very poor.Then Veer Brahma says that he will take all the wealths his grandfather gave to others.Then Bramha is taken to court and he is gone to 2 years jail .There his father apolazies to him.After two year,Bramha is now the chief minister of Karnataka.The Lady came to kill brahma but she did not killed brahma.She is the wife of Rahul Dev.The movie ends with Upendra saying- Jo janta ko chodkar dourta hai woh leader nehi ho ta,Jo janta ko sath lekar dourta hai wahi ho ta hai leader.
In English,Which man runs without the public he is not leader,but who runs with the public he is the leader.

==Cast==
* Upendra ... Brahma/Upendra
* Pranitha Subhash ... Pranitha
* Rangayana Raghu ... Lucky Man
* Sadhu Kokila ... Sadhu
* Ananth Nag... Narayana Murthy Urvashi
* Nassar ... Veer Bramha
* Shayaji Shinde ... ACP Shinde
* Rahul Dev ... Rahul Dev
* Girish Karnad  Sumithra 
* Bullet Prakash
* Suchendra Prasad
* John Kokkin
* Padmaja Rao
* Suresh Mangaluru

==Production==

===Development and casting===
Director R. Chandru, who tasted success from his previous venture Charminar (film)|Charminar revealed his intention to direct his next venture on 1 April 2013.  He announced that he is going to direct Upendra in a film titled Bramha - The Leaderand the shoot for the film will commence in the first week of May. He also announced that the venture would be a Kannada - Telugu bilingual project and actors from the Kannada and Telugu film industries would be acting in Bramha. He then quickly roped in actors Nassar and Shayaji Shinde to play the pivotal roles.
 Trisha who Pranitha was signed in to play the role who immediately agreed upon hearing the script. 

===Filming===
The principal photography of the film began on 15 May 2013 at the backdrop of the Bangalore palace and the audio recording launch began two days prior to this at the Gurukiran studios.  After launch in Bangalore Palace and song recording muhurth in Gurukiran Studio the film team went straight to Lalith Mahal Palace in Mysore for eight days shooting. After this schedule the director and team is moving to Bangkok portion of shooting. This film will release in Kannada on 7 February and Telugu in the month of march

== Soundtrack ==
The music is composed by Gurukiran for Anand audio company and lyrics are written by Kaviraj (lyricist)|Kaviraj. 

==Release==

===Critical reception===
The film opened to mostly positive reviews from the critics. SandalwoodBoxOffice.com rated the film 3.75/5 and called it a brilliant entertainer with good story, acting & action.  Shyam Prasad of Bangalore mirror rated the film 3/5 saying "Shekar Chandru is at the top of his game. Be it the battle scenes of the 15th century or the car chase in Malaysia, he makes it look look great. Brahma aspires to be a film taken seriously. But in the end, it is just a commercial film."  CineLoka rated the film 3.75/5 and called it a "Brilliant Entertainer".  Sandesh of One India rated the film 3/5 and said "The director is successful in showing Upendra in five shades. As Upendra is also an admired director among Kannadigas and others, there is a lot of expectations from the movie. Well, to be more accurate, director R Chandru has not disappointed Upendras fans in the film."  The Hans India rated the film 3/5 and said "The film spans four generations and costumes are definitely the talking point. If you are an uppi fan, you will love this film which is a typical upendra film which bears all the trademarks of the actor."  GS Kumar of the Times of India rated the film 3.5/5 and said, "With complete grip over the lively script laced with witty dialogues, the director has narrated the story excellently keeping the interest alive till the end. Upendra rightly fits into the role and has done a neat job. Though Praneethas role is limited, her expressions are impressive. Rangayana Raghu is sure to make you laugh with a brilliant performance. Suresh Mangalore and Nazar are gracefull. Shekhar Chandras cinematograph is lovely. Music by Gurukiran has nothing much to offer."  India Glitz rated the film 7.5/10 saying "Director R Chandru is scaling heights with big thoughts, big canvas and tremendous marketing skills to promote his product. A successful director with most successful hero Upendra has not given room for drop for a few seconds in Brahma. He has made such a captivating, concern filled, chilling moments, close to heart at the end film Brahma - surely a wholesome entertainer with ingredients well measured and transmitted on silver screen. Upendra has given different shades required for the acting. He dances well, action is fabulous and looks in different get up is undoubtedly mass hero. The technical aspects of this film editing, graphics and 16th century well adjusted, 70s shade are convincingly portrayed on the screen." 

===Box office===
Brahma took a grand opening at the Box Office by collecting a whooping   36&nbsp;million on its first day in a record breaking 276 screens and 1076 shows all over Karnataka. Even though release in too many theaters limited the number of houseful screenings, the overall collection was huge. Brahma had a spectacular opening weekend and collected a total of   97.5&nbsp;million in its opening weekend which is the highest weekend collection for a Kannada movie.  The break-up, according to director R Chandru, is   36.5&nbsp;million (Friday),   24&nbsp;million (Saturday) and   37&nbsp;million (Sunday). According to Trade reports, even if Mondays collection is 10 percent of Saturdays   24&nbsp;million, the film will become the fastest to join the   100 million club of Kannada cinema|Sandalwood. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Tunta Tunta
| extra1 = Shweta Mohan Kaviraj
| length1 = 05:10
| title2 = Tingu Tingu
| extra2 = Nakash Aziz, Chaitra H. G.
| lyrics2 = Kaviraj
| length2 = 03:45
| title3 = Pesallagi Order Kotte
| extra3 = Gurukiran
| lyrics3 = Kaviraj
| length3 = 05:16
| title4 = Brahma
| extra4 = Ranjith (singer)|Ranjith, Gurukiran
| lyrics4 = Kaviraj
| length4 = 04:50
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 