King Richard and the Crusaders
{{Infobox film
| name = King Richard and the Crusaders
| image = King Richard and the Crusaders FilmPoster.jpeg
| caption = Theatrical poster David Butler 
Oren W. Haglund (assistant director)
| producer = Henry Blanke
| writer = John Twist Sir Walter Scott
| narrator = Lester Matthews Robert Douglas Michael Pate Paula Raymond Lester Matthews Anthony Eustrel Henry Corden Wilton Graff Nejla Ates Nick Cravat
| music = Max Steiner
| cinematography = J. Peverell Marley
| editing = Irene Morra
| distributor = Warner Bros.
| released =  
| runtime = 114 minutes
| country = United States
| language = English
| budget = $3,000,000 (estimated)
| gross = $2,100,000 (USA)
}}
 1954 historical David Butler The Talisman. Robert Douglas, Michael Pate and Paula Raymond. Years later, King Richard and the Crusaders was included as one of the choices in the book The Fifty Worst Films of All Time.

==Synopsis==
In 1191, King Richard the Lionheart, along with several other European monarchs, is in the Holy Land intent on retaking Jerusalem from the Saracens. There is much infighting and outright treachery in the European encampment however. Two nobles in particular, Sir Giles Amaury and Conrad of Montferrat, want to eliminate the English king and attempt to have him assassinated. Severely wounded and on his death bed, Richard is brought back to health by a Saracen doctor recruited by one of his loyal knights, Sir Kenneth of the Leopard. The king recovers from his wounds but when he hears that Sir Lawrence wishes to marry Lady Edith Plantagenet, the knight is banished only to be taken in by the very doctor who treated the king and who has an altogether different identity.

==Cast== Emir Hderim Sultan Saladin
*Virginia Mayo as Lady Edith Plantagenet King Richard the Lionheart
*Laurence Harvey as Sir Kenneth of Huntington Robert Douglas as Sir Giles Amaury
*Michael Pate as Conrad of Montferrat|Conrad, Marquis of Montferrat Queen Berengaria
*Lester Matthews as Archbishop of Tyre / Narrator
*Anthony Eustrel as Baron De Vaux King Philip of France Duke Leopold of Austria
*Nejla Ates as Moorish Dancing Girl
*Nick Cravat as Nectobanus
* Nejla Ates
* Leslie Bradley
* Bruce LEster
* Mark Dana
* Peter Ortiz

==Plot Summary==

The film begins with King Richard the Lionheart (George Sanders) planning to attack the Saracens and defeat their leader, Emir Hderim Sultan Saladin (Rex Harrison). Little does Richard know, though, the Castelaine knights who are supposed to follow him have other plans of their own. Giles of Conrad (Robert Douglas), a Castelaine supporter, sets up an assassination attempt of the King using a poisoned Saracen arrow in hopes that the arrow will kill the King, frame the enemy, and allow Giles of Conrad to promote one of his own to overtake the Saracens and be the victorious ruler of the land. The assassination attempt fails and King Richard is discovered to still be alive, just before a new leader is about to be named. At this time, Sir Kenneth of Huntington (Laurence Harvey) enters King Richard’s camp, blaming Giles of Conrad and the Castelaine knights for the assassination attempt. Kenneth announces his loyalty to the King, but the King dismisses his thought that his own knights would assassinate him. The King then sends Kenneth to lead the Queen’s caravan as a scout to prove his loyalty to the Kingdom. 
	
On his scouting voyage, Kenneth crosses paths with a Saracen, and the two begin to duel. This does not last long though as the two come to a chivalrous agreement to end the fighting and discuss their intentions of crossing through the desert. At this time, Kenneth learns the this Saracen is a physician named Emir Ilderim, sent by Saladin to heal King Richard. He says Saladin offers a truce until Richard is healed because he would like to speak with him in person about the affairs of the nation and the war. Kenneth helps Emir get to Richard safely and King Richard accepts the terms of Saladin’s truce, and the help of his physician to return to full health.
	
Emir successfully heals King Richard, but not without stirring up trouble in the King’s camp first. After his return, Kenneth is given another chance to prove himself to King Richard by guarding the English flag that flies at the outskirts of the camp. At the same time, Emir is speaking with Lady Edith Plantagenet (Virginia Mayo), Richard’s relative and Kenneth’s love interest, and suggests to her that a marriage between a beautiful Christian woman (Edith) and a Muslin leader could bring peace to the land, without war. Kenneth sees this proposal and gets jealous, leaving his post at the flag to confront Emir. At this time, we see the Castelaine knights re-enter the story and knock down the English flag that Kenneth is supposed to be guarding. King Richard enters and sees Kenneth speaking with Edith and the flag on the ground. Richard becomes furious and sentences Kenneth to trial by combat. At the trial, Richard gains the upper hand, knocking Kenneth unconscious, but before he kills him, Emir asks the King to spare Kenneth’s life, stating that he will take him back to the Saracen’s and Kenneth will no longer be allowed in the English kingdom. The King, respecting the chivalry of Saladin to send Emir, agrees and banishes Kenneth from England. 
	
When Kenneth awakes from the battle, he is in the Saracen camp being healed by Emir and living like royalty. Despite these gifts and Emir’s kindness to save his life, Kenneth can only think of Edith and how he is going to return to her. Emir then explains to Kenneth that he is banished but he needs his help. Emir reveals to Kenneth that he is actually Saladin, ruler of the Saracens, and that he has found who is trying to overthrow King Richard, the Castelaine knights. Kenneth returns to the King Richard in disguise to warm him about the danger he is in.
	
When Kenneth returns to King Richard, the Castelaine’s overhear Kenneth warning Richard of their plan. They then steal away Edith and attempt to return to the Castelaine castle and defend themselves from there. Because of the mutual interests in Edith by Richard, Kenneth, and Saladin, the three united to defeat the Castelaines. After a successful battle and chase across the country side, the Castelaines are defeated, Kenneth and Edith plan to marry, King Richard forgives Kenneth, and Saladin rides away safely back to his Kingdom.

==Historical Aspects in Production==

The Castelaine Knights from the movie are an invented group that plays the villain. They are supposed to be playing the part of the “Knights Templar”. There are two theories as to why these characters were changed from Knights Templar to Castelaines. The first is because of Production Code that prevents negative representation of clergy in film. The second is because the producers wanted to avoid upsetting the Masonic Knights Templar, a group that many Hollywood figures were members of. It is unclear which theory, if either, is that actual reason for changing the name. 

In Lorraine Stock’s article, “Now Starring in the Third Crusade: Depictions of Richard I and Saladin in Films and Television Series”, she suggests that the casting of George Sanders as Richard was relevant to the time period. By choosing a “grey-haired, avuncular" leader, it appears they were returning to the idea of the mature monarch, mirroring the current leaders of the 1950s in Winston Churchill and Dwight D. Eisenhower.   

Rex Harrison’s character, Saladin, is misrepresented in the film in an “ultimately demeaning way”. In addition to the use of brown face paint, the use of disguise and trickery by the Muslim leader is greatly exaggerated in the movie. The leader is feminized throughout the movie as well with his gaudy, light colored robes and gang of belly dancers. 

Lorraine Stock points out some of the anti-war sentiment in the movie. Three of the main characters, Sir Kenneth, Lady Edith, and Saladin, at some point mention their disapproval of war and discuss how unnecessary it is. Stock suggests this is a reflection of the audience’s viewpoint and an anti-war movement of the 1950s with audiences still remembering World War II and the more recent Korean War of 1950-1953. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 