Cheatin' (film)
 
{{Infobox film
| image= 
| caption= 
| director=Bill Plympton
| producer= Bill Plympton
| writer= Bill Plympton
| narrator= 
| starring= 
| music= Nicole Renaud
| cinematography = 
| editing= Kevin Palmer
| studio= Plymptoons Studio
| distributor= 
| released= 
| runtime=76  minutes
| country=United States
| language= 
| budget= 
| gross= 
}}
 romantic comedy-drama dramedy film by Bill Plympton. It is the directors seventh feature-length animated picture, and tenth feature film overall.

==Plot==
Ella, a beautiful woman tired of unwanted attention from men, strolls through a carnival while reading a book. A barker talks her into trying the bumper cars, but the result is a perilous accident that leaves Ella trapped. A stranger, the handsome and muscular Jake, rescues her, and the two fall in love and are soon married. Various women attempt to seduce Jake, but he remains steadfastly faithful.

Enraged by this slight, one of these women stages a photo of Ella, changing in a room full of male mannequins, and gives it to Jake. Jake, distraught by what he believes to be his wifes infidelity, contemplates suicide, but soon takes solace in a series of affairs. When Ella discovers this infidelity, she tries to hire a man to kill Jake, before finding a magician who has a machine that will allow her to temporarily transport her consciousness into the bodies of the women Jake is sleeping with.

==Background==
Cheatin is Plymptons seventh full-length animated feature. The animation consists of monochrome pencil-sketches, which are then digitally colored and composited to create a watercolor-like image. All animation was drawn by Plympton himself, but the colorization and compositing required a staff of about 10 people.

To finance this more expensive process, Plympton launched a crowdfunding campaign on Kickstarter on Dec 3, 2012 with a goal of $75,000. On Feb 1, 2013, the campaign completed with $100,916. On Aug 22, 2014, an online streaming version was delivered to backers, the first feature-length animated film funded on Kickstarter to be delivered. Physical copies of the film began shipping that December.

==Accolades==
{| class="wikitable plainrowheaders"
|+  List of Awards and Nominations 
|-
! scope="col" style="width:4%;"| Year
! scope="col" style="width:25%;"| Award
! scope="col" style="width:33%;"| Category
! scope="col" style="width:33%;"| Recipients and nominees
! scope="col" style="width:5%;"| Results
|-
! scope="row" rowspan="4" style="text-align:center;"| 2014 42nd Annual Annie Awards  Annie Award Best Animated Feature
|Cheatin
| 
|- Annie Award Directing in an Animated Feature Production Bill Plypton
| 
|- Annie Award Music in a Feature Production Nicole Renaud
| 
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 