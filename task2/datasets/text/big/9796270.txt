Daigoro vs. Goliath
{{Infobox Film 
  | name = Daigoro vs. Goliath
  | image = 
  | caption = 
  | director =  Toshihiro Iijima
  | producer = Hajime Tsuburaya
  | writer = Kitao Chiba
  | starring = Hiroshi Inuzuka Akiji Kobayashi
  | music = Toru Fuyuki
  | cinematography = Yuzo Inagaki Toho Studios  Tsuburaya Productions
  | distributor = Toho 
  | released = 1972 
  | runtime = 85 mins
  | country = Japan
  | language = Japanese
  }}
 1972 Kaiju film. The film was produced by Tsuburaya Productions.

==Plot==
Daigoro is a monster who was orphaned after the military used intercontinental missiles to kill his mother while she tried to protect him. Only one man stood against that decision. He pitied the infant, and took it as his own and raised him in Japan. But Daigoro grew too large and too expensive to feed. The man made Daigoro an icon for a business. Elsewhere Goliath, a monster who had been trapped in an asteroid for a long time, went to Earth and battled Daigoro. Goliath eventually defeated Daigoro by striking him with lightning from his horn. Goliath then left to pillage the world, leaving Daigoro to die. Daigoro recovered and practiced daily for his next battle against Goliath. After an intense fight, Daigoro breathed his fire ray and managed to defeat Goliath. The humans then grabbed Goliath while he was still weak and strapped him to a rocket and launched him into space.

==Production==
The film was planned as part of the yearlong celebration of  . Toho gave the project the green light, and agreed to loan Tsuburaya Productions a Godzilla suit. This project never saw the light of day in its original form.

Over time, it was heavily altered and repackaged as Daigoro vs. Goliath (1971), with Toho taking a distribution role and Tsuburaya producing the final product.

==References==
 
 

==External links==
* 

 
 
 
 
 
 
 


 