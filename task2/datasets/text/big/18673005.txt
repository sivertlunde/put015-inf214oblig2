Hollywood & Wine
 
{{Infobox film
| name           = Hollywood & Wine
| image          = Hollywood & Wine.jpg
| border         = yes
| caption        = 
| director       =  
| producer       =  
| writer         =  
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 8 million
}}
Hollywood & Wine is a 2010 American comedy film.

==Plot==
Diane Blaine has the face of a movie star. Unfortunately, fallen star/tabloid queen Jamie Stephens already made it famous. Hollywoods constant rejection due to what Diane refers to as "TJS" ("Too Jamie Stephens") has made her bitter, frustrated....and, yes, whiny. Co-worker/boyfriend Jack Sanders doesnt help matters. His idea of ambition is letting it ride. Now hes in major debt to a trigger-happy mobster who, interestingly enough, has a thing for Jamie Stephens. Jacks only way out? Convince Diane to be Jamie and wipe out the debt having one meal with a made man. Its literally the performance of her life. With Jacks on the line.   

==Cast==
* Nicky Whelan  as Diane Blaine/Jamie Stephens
* Chris Kattan  as Jack Sanders
* David Spade  as Harvey Harrison
* Chazz Palminteri  as Geno Scarpaci
* Norm Macdonald  as Sid Blaustein
* Jeremy London  as Jean-Luc Marceau
* Kevin Farley  as Bruno
* John P. Farley  as Joey
* Vivica A. Fox  as Jackie Johnson
* Pamela Anderson  as Jennifer Mary
* Horatio Sanz  as Tony
* Chris Parnell  as Peter West
* Leslie Easterbrook  as Hattie
* Miguel A. Nunez, Jr. as Hawk Miller

==References==
 

==External links==
*  

 
 
 
 
 
 
 