Visual Acoustics: The Modernism of Julius Shulman
{{Infobox film
| name           = Visual Acoustics: The Modernism of Julius Shulman
| image          = Visual_Acoustics_The_Modernism_of_Julius_Shulman.jpg
| alt            = 
| caption        = 
| director    = Eric Bricker
| producer    = Eric Bricker   Babette Zilch
| writer         = Eric Bricker   Phil Ethington   Jessica Hundley   Lisa Hughes   Deborah Dietsch
| starring       = Julius Shulman   Frank Gehry   Dante Spinotti   Kelly Lynch   Richard Neutra  Ed Ruscha   Tom Ford   Barbara Lamprecht   Benedikt Taschen   Thomas Hines
| narrator       = Dustin Hoffman
| music          = Charlie Campagna
| cinematography = Aiken Weiss   Dante Spinotti
| editing        = Charlton McMillan
| studio         = 
| distributor    = Arthouse Films
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Visual Acoustics: The Modernism Of Julius Shulman is a documentary film that explores the life and career of the much lauded architectural photographer Julius Shulman. His iconic photography shaped the careers of some of the great architects of the 20th century and helped define Modernism for the general public. {{cite news
|url=http://www.austinchronicle.com/calendar/film/2010-03-05/visual-acoustics/
|title=Visual Acoustics
|publisher=Austin Chronicle
|date=March 5, 2010
|accessdate= Oct 15, 2011}} 

== Subject ==
Narrated by Dustin Hoffman, Visual Acoustics is a documentary film that explores the life and career of the architectural photographer Julius Shulman, who died at age 98 in 2009.  Shulman was  widely considered to be the worlds greatest and most well-known architectural photographer.  The film highlights his iconic photos which were frequently populated with human models and striking landscapes, often combining the organic with the synthetic and melding nature with contemporary design. The film highlights how these images shaped the careers of some of the great architects of the 20th Century, notably Richard Neutra, John Lautner and the Case Study Architects.  His iconic image of Case Study House #22 is one of the most well-known and widely distributed photographs of Modernist architecture. Still working until his death,  Shulman continued his celebration of modern architecture with striking images of the work of modern greats like Frank Gehry.  His work in particular brought Southern California Modernism to life and made modern architecture accessible to many.  His larger than life personality was a driving force in his work and in this film. Exploring Shulmans art and uniquely creative life, Visual Acoustics forges an all-encompassing portrait of Modernisms most eloquent ambassador. 

== Critical response ==

Varietys  Robert Koehler writes in his review:"The modernist ethic receives its due in "Visual Acoustics: The Modernism of Julius Shulman." Eric Brickers handsome, functional if not very cinematic tribute to Shulman emphatically argues for his place as the most important of architecture photographers, and the person who has directly conveyed the titular style to a wider public. Something of a missionary statement for a missionary, this is nirvana for lovers of mid-century modern and fine-art photography, and a solid bet for wide-ranging and specialty fests along with arts-oriented cablers..." and

Shulman, now 97, is responsible for photographing many of the mod masterpieces in meticulous detail, particularly in the movements Southern California epicenter. As the film helpfully explains, Shulmans eye for one-point perspective (though not noted here, a considerable influence on Stanley Kubricks films from "Paths of Glory" on) accommodated the styles emphasis on open, airy space; long, geometrical lines; panoramic use of glass and windows; and the visual blending of exterior earth and sky with interior comfort zones." {{ cite news
|url=http://www.variety.com/review/VE1117937636/html
|title=Visual Acoustics: The Modernism of Julius Shulman Vanity Fair
|date=Jul 7, 2008   
|accessdate= Oct 15, 2011}} 

Review Aggregator website Rotten Tomatoes reported that 94% of reviewers gave the film  positive reviews, based on 17 reviews and with an average score of 7 out of 10 and reports that the average audience rating was 3.5 out of 5. 

== Production credits ==
* Distributor: Arthouse Films
* Directed by: Eric Bricker
* Produced by: Eric Bricker, Babette Zilch
* Executive Producers: Lisa Hughes, Michelle Oliver
* Editing by: Charlton McMillan
* Cinematography: Aiken Weiss, Dante Spinotti
* Written by: Eric Bricker, Phil Ethington, Jessica Hundley, Lisa Hughes, Deborah Dietsch
* Music by: Charlie Campagna
* Narration by:  Dustin Hoffman
* Graphics: Trollbäck + Company
* Release Date: 10/9/2009
* Running Time: 84 mins
* Country: US
* starring: Julius Shulman, Frank Gehry, Dante Spinotti, Kelly Lynch, Richard Neutra, Ed Ruscha, Tom Ford, Barbara Lamprecht, Benedikt Taschen, Thomas Hines

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 