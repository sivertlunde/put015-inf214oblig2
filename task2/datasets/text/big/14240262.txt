The Sitter Downers
{{Infobox Film |
  | name           = The Sitter Downers|
  | image          = SitterDownersTITLE.jpg |
  | caption        =  |
  | director       = Del Lord |
  | writer        = Ewart Adamson |
  | starring       = Moe Howard Larry Fine Curly Howard Marcia Healy June Gittelson Betty Mack James C. Morton|
  | cinematography = George Meehan |  Charles Nelson |
  | producer      = Jules White |
  | distributor    = Columbia Pictures |
  | released       =  |
  | runtime        = 15 34" |
  | country        = United States
  | language       = English
}}

The Sitter Downers is the 27th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are suitors who go on a sit down strike when their prospective father-in-law ( 

==Production notes==
The Stooges wives are named Florabell (June Gittelson), Corabell (Betty Mack), and Dorabell (Marcia Healy, sister of the Stooges former boss, Ted Healy. Ironically, this was the last Stooge film released during his lifetime: he died on December 21, 1937.    Filming commenced between May 27 and June 2, 1937.   
 colorized version of this film was released as part of the 2004 DVD collection entitled "Goofs on the Loose". 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 