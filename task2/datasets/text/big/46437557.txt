The Anarchists (film)
 
{{Infobox film
| name = The Anarchists
| image =
| caption =
| director = Elie Wajeman
| producer = Lola Gans
| writer = Elie Wajeman Gaëlle Macé
| starring = Tahar Rahim Adèle Exarchopoulos
| music =
| cinematography = David Chizallet
| editing        = François Quiqueré
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         = 
}}

The Anarchists ( )    is an upcoming French drama film directed by Elie Wajeman. It has been selected to open the International Critics Week section at the 2015 Cannes Film Festival.   

==Cast==
* Tahar Rahim as Corporal Jean Albertini    
* Adèle Exarchopoulos as Judith Lorillard
* Cédric Kahn as Gaspar
* Swann Arlaud as Elisée
* Sarah Le Picard as Marie-Louise Chevandier
* Guillaume Gouix as Eugène Lévêque
* Karim Leklou as Biscuit
* Emilie de Preissac as Clothilde Lapiower
* Thilbault Lacroix as Albert Vuillard
* Arieh Worthalter as Adrian
* Simon Bellouard as Hans
* Aurélia Poirier as Martha

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 