The Lioness of Castille
{{Infobox film
| name =   The Lioness of Castille
| image =
| image_size =
| caption =
| director = Juan de Orduña
| producer = 
| writer = Francisco Villaespesa (play)   Vicente Escrivá   Juan de Orduña
| narrator = Virgilio Teixeira   Alfredo Mayo   Manuel Luna  Juan Quintero  
| cinematography = Alfredo Fraile  
| editing = Petra de Nieva 
| studio = CIFESA
| distributor = CIFESA
| released = 28 May 1951
| runtime = 106 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}} historical drama Virgilio Teixeira and Alfredo Mayo.  Charles V. 

==Cast==
*  Amparo Rivelles as María de Pacheco  Virgilio Teixeira as Pedro de Guzmán 
* Alfredo Mayo as Manrique 
* Manuel Luna as Ramiro  
* Eduardo Fajardo as Tovar  
* Rafael Romero Marchent as Juan de Padilla hijo  
* Antonio Casas as Juan de Padilla 
* Laly del Amo as Doña Isabel  
* María Cañete as Sirvienta de María de Pacheco  
* Nicolás D. Perchicot as Cura en ejecución  
* Alberto Romea as Arzobispo  
* Manuel Arbó as Noble toledano  
* Francisco Pierrá as Noble toledano 
* José Jaspe as Juan Bravo 
* Teófilo Palou as Francisco Maldonado  Domingo Rivas as Noble toledano 
* Santiago Rivero as Líder de los Imperiales 
* Jesús Tordesillas as Don López 
*  Faustino Bretaño
* Adriano Dominguez 
* Miguel Pastor 
* Arturo Marín
* Antonio Riquelme 
* Luis Peña padre
* Rafael Cortés    Carlos Osorio  
* José Buhigas  
* Ángel Monís   
* César Guzman 
* Germán Cobos  Luis Fernandez
* Eduardo Bonada
* Francisco Maroto

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008. 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 

 