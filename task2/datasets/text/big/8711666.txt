The Wiz (film)
{{Infobox film
| name           = The WIZ
| image          = the-wiz-1978.jpg
| alt            = Four characters from the film dancing on top of a logo "THE WIZ". A city skyline just after dusk is behind them, and the entire scene is mirrored in water before them. The people are Dorothy, the Scarecrow, the Tin Woodman, and the Lion.
| caption        = Theatrical release poster
| director       = Sidney Lumet
| producer       = Rob Cohen 
| screenplay     = Joel Schumacher William F. Brown
| starring       = Diana Ross Michael Jackson Nipsey Russell Ted Ross Lena Horne Richard Pryor Nickolas Ashford Anthony Jackson Luther Vandross Quincy Jones
| cinematography = Oswald Morris
| editing        = Dede Allen Motown Productions
| distributor    = Universal Studios
| released       =  
| runtime        = 134 minutes
| country        = United States
| language       = English
| budget         = $24 million 
| gross          = $21 million 
}} musical adventure 1975 Broadway New York, Tin Man, and a Cowardly Lion, she travels through the land to seek an audience with the mysterious Wizard of Oz (character)|Wiz, who they say has the power to take her home.
 Nickolas Ashford & Valerie Simpson, were added for the film version. Upon its original theatrical release, The Wiz was a critical and commercial failure, and marked the end of the resurgence of African-American films that began with the blaxploitation movement of the 1970s.        Despite its initial failure, the film version of The Wiz would go on to become a cult classic, particularly among African-American audiences and fans of Michael Jackson.  

==Plot== Uncle Henry 125th Street, and refuses to move out and on with her life.
 Toto runs out the open kitchen door into a violent snowstorm. She succeeds in retrieving him, but finds herself trapped in the storm. A magical whirlwind made of snow the work of Glinda the Good Witch|Glinda, the Good Witch of the South – materializes and transports them to the Kingdom of Oz. Upon her arrival, Dorothy smashes through an electric "Oz" sign, which falls upon and kills Evermean, the Wicked Witch of the East. As a result, she frees the Munchkins who populate the playground into which she lands; they had been transformed by Evermean into graffiti for "tagging" the park walls.
 numbers runner" silver slippers to her. However, Dorothy desperately wants to get home. Miss One urges her to follow the yellow brick road to the Emerald City and find the mysterious "Wizard (Oz)|Wizard" who she believes holds the power to send Dorothy back to Harlem. The good witch and the Munchkins then disappear and she is left to search for the yellow brick road on her own.
 Scarecrow (Michael Tin Man (Nipsey Russell), a turn-of-the-century amusement park mechanical man, and the Cowardly Lion (Ted Ross), a vain dandy banished from the jungle who hid inside one of the stone lions in front of the New York Public Library. The Tin Man and Lion join them on their quest to find the Wizard, hoping to gain a heart and courage, respectively. Before the five adventurers reach the Emerald City, they must face obstacles such as a crazy subway peddler (a homeless man) with evil puppets in his control and the "Poppy" Girls (a reference to the poppy field from the original story), prostitutes who attempt to put Dorothy, Toto, and the Lion to sleep with magic dusting powder.
 Flying Monkeys motorcycle gang) to kidnap them.

After an extended chase, the Flying Monkeys succeed in capturing their prey and bring them back to Evillene. She dismembers the Scarecrow, flattens the Tin Man, and tortures the Lion in hopes of making Dorothy give her the silver shoes. When she threatens to throw Toto into a fiery cauldron, Dorothy nearly gives in until the Scarecrow hints to her to activate a   are freed from their costumes (revealing humans underneath) and their sweatshop tools disappear. They rejoice in dance and praise Dorothy as their emancipator. The Flying Monkeys give her and her friends a triumphant ride back to the Emerald City.

Upon arriving back at the Emerald City, the quartet takes a back door into the Wizards quarters and discovers that he is a "phony". The "great and powerful Oz" is actually Herman Smith, a failed politician from Atlantic City, New Jersey, who was transported to Oz when a balloon he was flying to promote his campaign to become the city dogcatcher was lost in a storm. The Scarecrow, Tin Man, and Lion are distraught that they will never receive their respective brain, heart, and courage, but Dorothy makes them realize that they already have these things. Just as it seems as if she will never be able to get home, Glinda the Good Witch of the South (Lena Horne), appears and implores her to find her way home by searching within and using her silver shoes. After thanking Glinda and saying goodbye to her friends, she takes Toto in her arms, thinks of home and the things she loves most about it and, after clicking her heels, finds herself back in her neighborhood. Now a changed woman, Dorothy carries Toto back to their apartment and closes the door.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| Diana Ross || Dorothy Gale
|- Scarecrow
|-
| Lena Horne || Glinda the Good Witch of the South
|-
| Ted Ross || Cowardly Lion
|- Tin Man
|-
| Thelma Carpenter || Addaperle/Miss One, the Good Witch of the North
|-
| Theresa Merritt || Aunt Em
|- Uncle Henry
|- The Wiz/Herman Smith
|-
| Mabel King || Evilene, the Wicked Witch of the West
|}

==Songs==
 
 
All songs written by Charlie Smalls, unless otherwise noted.
# "Overture Part I"  (instrumental) 
# "Overture Part II"  (instrumental) 
# "The Feeling That We Had"  - Aunt Emma and Chorus  Nickolas Ashford and Valerie Simpson)  - Dorothy 
# "Tornado"/"Glindas Theme"  (instrumental) 
# "Hes the Wizard"  - Miss One and Chorus 
# "Soon As I Get Home"/"Home"  - Dorothy  You Cant Win, You Cant Break Even"  - Scarecrow and The Four Crows 
# "Ease On Down the Road #1"  - Dorothy and Scarecrow 
# "What Would I Do If I Could Feel?"  - Tin Man 
# "Slide Some Oil to Me"  - Tin Man 
# "Ease On Down the Road #2"  - Dorothy, Scarecrow, and Tin Man 
# "Im a Mean Ole Lion"  - Cowardly Lion 
# "Ease On Down the Road #3"  - Dorothy, Scarecrow, Tin Man, and Cowardly Lion  Anthony Jackson)  (instrumental) 
# "Be a Lion"  - Dorothy, Scarecrow, Tin Man, and Cowardly Lion 
# "End Of The Yellow Brick Road"  (instrumental) 
# "Emerald City Sequence" (music: Jones, lyrics: Smalls)  - Chorus 
# "Is This What Feeling Gets? (Dorothys Theme)" (music: Jones, lyrics: Ashford & Simpson)  - Dorothy (vocal version not used in film) 
# "Dont Nobody Bring Me No Bad News"  - Evillene and the Winkies  Everybody Rejoice/A Brand New Day" (Luther Vandross)  - Dorothy, Scarecrow, Tin Man, Cowardly Lion, and Chorus 
# "Believe in Yourself (Dorothy)"  - Dorothy 
# "The Good Witch Glinda"  (instrumental) 
# "Believe in Yourself (Reprise)"  - Glinda the Good Witch  Home (Finale)"  - Dorothy 

==Production==

===Pre-production and development=== Motown Productions, the film/TV division of Berry Gordys Motown Records label. Gordy originally wanted the teenaged future Rhythm and blues|R&B singer Stephanie Mills, who had played the role on Broadway, to be cast as Dorothy. When Motown star Diana Ross asked Gordy if she could be cast as Dorothy, he declined, saying that Ross—then 33 years old—was too old for the role.    Ross went around Gordy and convinced executive producer Rob Cohen at Universal Pictures to arrange a deal where he would produce the film if Ross was cast as Dorothy. Gordy and Cohen agreed to the deal. Pauline Kael, a film critic, described Rosss efforts to get the film into production as "perhaps the strongest example of sheer will in film history." 
 stage musical, first refusal rights to the film production, which gave Universal an opening to finance the film.  Initially, Universal was so excited about the films prospects that they did not set a budget for production. 

Joel Schumachers script for The Wiz was influenced by Werner Erhards teachings and his Erhard Seminars Training ("est") movement, as both Schumacher and Diana Ross were "very enamored of Werner Erhard".    "Before I knew it," said Rob Cohen, "the movie was becoming an est-ian fable full of est buzzwords about knowing who you are and sharing and all that. I hated the script a lot. But it was hard to argue with   because she was recognizing in this script all of this stuff that she had worked out in est seminars."  Schumacher spoke positively of the results of the est training, stating that he was "eternally grateful for learning that I was responsible for my life."  However, he also complained that "everybody stayed exactly the way they were and went around spouting all this bullshit."  Of est and Erhard references in the film itself, The Grove Book of Hollywood notes that the speech delivered by Glinda the Good Witch at the end of the film was "a litany of est-like platitudes", and the book also makes est comparisons to the song "Believe in Yourself". 
 The Wizard of Oz, Lumet stated that "there was nothing to be gained from   other than to make certain we didnt use anything from it. They made a brilliant movie, and even though our concept is different — theyre Kansas, were New York; theyre white, were black, and the score and the books are totally different — we wanted to make sure that we never overlapped in any area." 
 The Jacksons, panthers in Tin Man. Lena Horne, mother-in-law to Sidney Lumet during the time of production, was cast as Glinda the Good Witch, and comedian Richard Pryor portrayed The Wiz.  

===Principal photography=== Astoria Studios The Cyclone as a backdrop, while the World Trade Center served as the Emerald City.    The scenes filmed at the Emerald City were elaborate, utilizing 650 dancers, 385 crew members and 1,200 costumes.     Costume designer Tony Walton enlisted the help of high fashion designers in New York City for the Emerald City sequence, and obtained exotic costumes and fabric from designers such as Oscar de la Renta and Norma Kamali.    Albert Whitlock created the films visual special effects,  while Stan Winston served as the head makeup artist. 

 , Thriller (Michael Jackson album)|Thriller, and Bad (album)|Bad.  Jones recalled working with Jackson as one of his favorite experiences from The Wiz, and spoke of Jacksons dedication to his role, comparing his acting style to Sammy Davis, Jr. 

==Commercial reaction==
The Wiz proved to be a commercial failure, as the United States dollar|$24 million production only earned $13.6 million at the box office.    Though prerelease television broadcast rights had been sold to CBS for over $10 million, in the end, the film produced a net loss of $10.4 million for Motown and Universal.     At the time, it was the most expensive film musical ever made.    The films failure steered Hollywood studios away from producing the all-black film projects that had become popular during the blaxploitation era of the early to mid-1970s for several years.   
 Thanksgiving Day (attributed to the opening scene of Dorothys family gathered for a Thanksgiving dinner).  

The film was released on DVD in 1999; Jackson, Alex (2008) " ". Film Freak Central. Retrieved March 9, 2008.  a  .  A Blu-ray Disc|Blu-ray version was released in 2010. 

==Critical reception== Scarecrow in the 1939 The Wizard of Oz film, did not think highly of The Wiz, stating "The Wiz is overblown and will never have the universal appeal   has obtained." 

  wrote positively of Ted Ross and Richard Pryors performances.    However, Nulls overall review of the film was critical, and he wrote that other than the song "Ease on Down the Road", "the rest is an acid trip of bad dancing, garish sets, and a Joel Schumacher-scripted mess that runs 135 agonizing minutes."  A 2005 piece by Hank Stuever in The Washington Post described the film as "a rather appreciable delight, even when its a mess", and felt that the singing – especially Diana Ross – was "a marvel". 
 Super Fly. 

Despite its lack of critical or commercial success in its original release, The Wiz would go on to become a cult classic among African-American audiences,    particularly for its notability as Michael Jacksons only starring theatrical film role.   

==Nominations== Edward Stewart, Best Costume Best Original Best Cinematography, but did not win any of them.  

==See also==
 
* The Wiz|The Wiz (Broadway musical)
* The Wizard of Oz (adaptations) – other adaptations of The Wonderful Wizard of Oz
* Wicked (musical)|Wicked (musical)
 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 