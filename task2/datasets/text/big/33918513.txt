Baby Mine (1917 film)
{{infobox film
| name           = Baby Mine
| image          = Baby Mine.jpg
| imagesize      =
| caption        =
| director       = John S. Robertson Hugo Ballin
| producer       = Samuel Goldwyn
| based on       =  
| writer         = Doty Hobart (adaptation)
| starring       = Madge Kennedy
| music          =
| cinematography = Arthur Edeson
| editing        =
| distributor    = Goldwyn Pictures
| released       =   reels
| country        = United States
| language       = Silent film (English intertitles)
}}
Baby Mine is a 1917 American silent film comedy directed by both John S. Robertson and Hugo Ballin and starring Madge Kennedy. The picture marked Kennedys screen debut and was one of the first films produced by Samuel Goldwyn as an independent after founding his own studio. 
 Margaret Mayo. Baby Mine (1928) with Charlotte Greenwood at MGM, the successor to Goldwyn Pictures. This version, however, at one time thought lost film|lost, is held in the French archive Cinematheque Francais.    

==Cast==
*Madge Kennedy - Zoie
*Kathryn Adams - Aggie (*as Katherine Adams)
*John Cumberland - Jimmie
*Frank Morgan - Alfred
*Sonia Marcelle - Italian Mother
*Virginia Madigan - Maggie
*Jack Ridgeway - OFlarity (*as Jack Ridgway)
*Nellie Fillmore - Mrs. OFlarity

==References==
 

==External links==
 
* 
*  

 

 
 
 
 
 
 
 
 


 