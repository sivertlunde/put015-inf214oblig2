Virtuous Wives
{{infobox film
| name           = Virtuous Wives
| image          = Virtuous Wives Anita Stewart 3 1918.png
| imagesize      =
| caption        = Ad for film with Anita Stewart
| director       = George Loane Tucker
| producer       = Louis B. Mayer Anita Stewart
| writer         = George Loane Tucker (scenario)
| based on       =  
| starring       = Anita Stewart
| music          = Phil Spitalny Ernest Palmer
| editing        =
| distributor    = First National Pictures
| released       =  
| runtime        = 60 mins.
| country        = United States
| language       = Silent (English intertitles)
}}

Virtuous Wives is a lost  1918 American  .  Its survival status is classified as being unknown,  which suggests that it is a lost film.

==Plot==
Based upon a review in a film magazine,  Amy (Stewart) and Andrew Forrester (Tearle) are happy in the first few weeks of their married life with the comforts that his $25,000 income brings. Andrew turns down a business opportunity with steel magnate Maurice Delabarre (Arden), but Delabarre decides he needs Andrews business abilities, and invites the couple to his house. Amy finds her living standard wanting, and demands that Andrew accept the offer even though it will cause them to be separated. After he accepts, Amy throws herself into the gaieties of the social set and even challenges the position of Delabarres wife Irma (Hopper). Irma, finding her social throne tottering, sends for Andrew. On his return, he judges Amys new lifestyle by old standards and wonders whether she is a virtuous wife.

==Cast==
* Anita Stewart - Amy Forrester
* Conway Tearle - Andrew Forrester
* Hedda Hopper|Mrs. DeWolf Hopper - Irma Delabarre
* Edwin Arden - Maurice Delabarre
* William "Stage" Boyd - Monte Bracken
* Virginia Norden - Mrs. Teake, Sr
* Katherine Lewis - Mrs. Teake Jr.
* Captain Mortimer - Jap; Laracy
* Harold Gwynn - Tubby Vandergrift
* Gwen Williams - Kitty Lightbody
* Lucille Clayton - Miss Rushin Thomas Carr - Bobby Delabarre
* Philip Leigh - Teddy Dawson
* George Stewart

==References==
 

==External links==
 
*  
*  
*   At the Internet Archive.

 
 
 
 
 
 
 
 
 
 
 
 


 