Sunset Heat (film)
{{Infobox Film
| name           = Sunset Heat
| director       = John Nicolella
| producer      = Kandice King, Lance King and John Michaels
| writer         = John Allen Nelson and Max Strom
| starring       = Michael Paré Adam Ant Dennis Hopper  Daphne Ashbrook Charlie Schlatter Tracy Tweed



| music       = Jan Hammer
| released       = 1992
| runtime        = 90 min
| country        = United States English
|}}
 The Telegraph called this a "forgettable film" which nonetheless allowed Hopper to pay his alimony. {{cite news|url= http://www.telegraph.co.uk/news/obituaries/culture-obituaries/film-obituaries/7786470/Dennis-Hopper.html |title=Film Obituaries, Dennis Hopper, 30 May 2010
|publisher= telegraph.co.uk|date= 2010-05-30|accessdate=2010-11-18|location=London}}  It featured original music by Jan Hammer, who had written the Miami Vice theme music. 




==Plot==

Photographer Eric Wright (played by Michael Paré) visits his friend Danny Rollins (Adam Ant) in California. Danny had stolen a million dollars from drug dealer Carl Madson (Dennis Hopper), a former partner of Eric. When Danny is murdered without having told anyone where the money is, Carl demands that Eric finds it for him even though he has no idea where it might be.

==Cast==
* Michael Paré as Eric Wright
* Adam Ant as Danny Rollins
* Dennis Hopper as Carl Madson 
* Daphne Ashbrook as Julie



==References==
 

==External links==
*   on the Internet Movie Database.

 
 
 
 

 