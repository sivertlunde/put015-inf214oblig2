Kaunri Kanya
 
 
{{Infobox film
| name           = Kaunri Kanya
| caption        = First Oriya 3D Film
| director       = Soumya Ranjan Sahu
| producer       = Ramya Ranjan Sahu
| starring       = Soumya Ranjan Sahu Kavya Kiran Payal Prakruti Mishra Rehan Ashutosh Bighna Ardhendu Sudipta
| Special Appearance =  
| Music Director = Satya Mithun
| Screenplay     = Soumya Ranjan Sahu
| studio         = Angel Bee Medias
| Shooting Locations = Odisha
| released       =  
| country        = India
| language       = Oriya
| release date   =  
}}

Kaunri Kanya, English: Possessed Girl, Oriya: କାଉଁରୀ କନ୍ୟା is the first 3D Oriya Film horror film,  directed by debutant director Soumya Ranjan Sahu  and produced by his brother Ramya Ranjan Sahu.  The film stars several debutant actors. The film was also dubbed into Tamil language|Tamil. It is the first Oriya film to feature an underwater sequence  which was shot in Vizag. While most of the shooting happened in Surangi, Ganjam district|Ganjam, Odisha, the background score and music were done by South Indian artists Satya and Mithun. Prakruti Mishra gives a special appearance in the film.
The movie is based around the story of a doctor whose wife gets possessed by an evil spirit. This film explores social stigmas viz. superstition, witchcraft, sorcery and belief of people on casting spells and summoning evil spirits to take girls into possession which leads to murdering of innocent people falsely alleged of practicing black magic.

==Plot==
Akash, a doctor by profession, is transferred to a village. After a sequence of unnatural incidents his wife, Riya gets possessed by an evil spirit. Akash takes help from psychiatrist Doctor Rajiv and tries to discover reasons behind the incidents and his wifes possession. He tries to bring his wife, Riya, out of possession of an evil spirit and thats when he comes to know about Ravi and Anusuya who belonged to the village and loved each other. The villagers killed Anusuya suspecting her for practicing witchcraft.

==References==
 

==External links==
* 

 
 
 
 
 


 
 