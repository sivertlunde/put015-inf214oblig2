The Bushwackers (film)
 
{{Infobox film
| name           = The Bushwhackers
| image_size     =
| image	=	The Bushwackers FilmPoster.jpeg
| caption        =
| director       = Rod Amateau
| producer       = Herman Cohen (associate producer) Larry Finley (producer) Jack Broder (executive producer)
| writer         = Rod Amateau (writer) Tom Gries (writer)
| narrator       =
| starring       = See below
| music          = Albert Glasser
| cinematography = Joseph F. Biroc
| editing        = Francis D. Lyon
| distributor    =
| released       = 8 January 1952
| runtime        = 70 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
The Bushwhackers is a 1952 American film directed by Rod Amateau.

==Plot==
Tired of killing, war veteran Jefferson Waring rides west, but in Missouri he sees "squatters" mowed down by men working for rich, ruthless Artemus Taylor.

He spends the night at Independence newspaperman Peter Sharpes place, but is jailed when daughter Cathy Sharpe finds this total stranger in her room. The local marshal, John Harding, is just one of many men on Taylors payroll.

Peters business is threatened by banker Stone unless he takes Taylors side against "squatters" settling in the region. The blind and wheelchair-bound Taylor and ambitious daughter Norah are secretly aware that railroad surveyors are considering laying tracks nearby, so they want all the land for themselves.

Jeff decides to leave. Norah and henchman Ding Bell intercept him; Norah shoots at him but misses. They take him to see Artemus, who tells a vocally reluctant Bell to take Jeff off to a remote canyon and murder him. Under Norahs instructions, Artemuss chief thug Sam Tobin goes after them to murder both; he wounds Jeff and kills Bell, but not before Bell hits him with a fatal shot. A doctor treats Jeffs wounds but Marshall Harding turns up and charges Jeff with the two killings.

When the situation escalates and two of Taylors thugs gun down Peter Sharpe, Jeff breaks out of jail and organizes a group of settlers to resist Taylors planned big attack. The settlers slaughter Taylors thugs; Taylor dies of a heart attack; Norah, having shot and she thinks killed banker Justin Stone in order to get some getaway money, is killed by him as she leaves. Jeff stays in town to run the paper with Cathy.

==Cast== John Ireland as Jefferson Waring Wayne Morris as Marshal John Harding
*Lawrence Tierney as Sam Tobin
*Dorothy Malone as Cathy Sharpe
*Lon Chaney Jr. as Artemus Taylor
*Myrna Dell as Norah Taylor
*Frank Marlowe as Peter Sharpe William Holmes as "Ding" Bell, ranchhand
*Jack Elam as Cree, first henchman
*Ward Wood as Second Henchman
*Charles Trowbridge as Justin Stone
*Norman Leavitt as Deputy Yale Stuart Randall as Slocum (settler) George Lynn as Guthrie, a settler
*Gordon Wynn as John Quigley, settler
*Gabriel Conrad as Kramer, immigrant-settler
*Eddie Parks as Funeral Franklin
*Bob Broder as Tommy Lloyd

==Soundtrack==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 


 