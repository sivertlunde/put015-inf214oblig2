An American Carol
{{Infobox film
| name           = An American Carol
| image          = American carol 08.jpg
| caption        = Theatrical release poster David Zucker John Shepherd Todd Matthew Burns Diane Hendricks 
| writer         = David Zucker Myrna Sokoloff
| starring       = Kevin Farley Kelsey Grammer Jon Voight Dennis Hopper Leslie Nielsen Jillian Murray Sammy Sheik
| music          = James L. Venable
| cinematography = Brian Baugh
| editing        = Vashi Nedomansky
| distributor    = Vivendi Entertainment
| released       =  
| runtime        = 83 minutes 
| country        = United States
| language       = English
| budget         = $20 million 
| gross          = $7,013,191
}}
 David Zucker liberal filmmaker Independence Day. The screenplay is written by Myrna Sokoloff and Zucker. The supporting cast includes Kelsey Grammer, Jon Voight, Dennis Hopper, Trace Adkins, Gary Coleman, Jillian Murray and Leslie Nielsen.  The film was released on October 3, 2008.

==Plot==
Left wing politics|Left-wing activist and filmmaker Michael Malone (Kevin Farley), a parody of Michael Moore, campaigns to end the celebration of the Fourth of July holiday. Malone holds pronounced anti-American views and truculently argues that Americas past and present are both offensive, and therefore should not be celebrated.

On the evening of July 3, Malone watches a speech from President John F. Kennedy and mistakenly interprets the speech to mean avoiding war at any cost. President Kennedy rises out of the television set, corrects Malone regarding the intent of the speech, and informs him that he will be visited by three spirits.
 Civil War. Malone later sees George Washington (Jon Voight) who gives a passionate speech about Gods gift of freedom and the price many people pay for others to have it. Malone is visited by the angel of death (Trace Adkins), who takes him to a future Los Angeles completely taken over by radical Islamism|Islamists. He is then taken to the ruins of his hometown in Michigan, which has been destroyed by a nuclear bomb planted by Al Qaeda. In a mortuary, Malone learns that he will be killed in this attack, leaving nothing behind but his trademark hat and "big ass."  Facing his death, Malone pleads for his life with the Angel, promising to change. However, all is not well after Malones revelation, for Aziz, a Middle Easterner he had interviewed, is actually a terrorist who will bomb a 4 July rally along with his underlings Ahmed and Fayed. However, when Fayed and Ahmed learn they are going to be detonated along with the planned bomb, they figure their slim chance of survival is by seeking out Malone.

Later, Malone arrives at an anti-Fourth of July protest rally and publicly renounces his former views. This triggers an outraged mob from which he is rescued, however, by American servicemen. Meeting up with Malone, Ahmed and Fayed defuse their own bomb, thus sparing the people at the anti-Independence Day rally and resulting in the capture of the terrorist Aziz. Safe inside a country music concert, the three are formally welcomed to "the real America" by Trace Adkins (this time as himself). A reformed Malone then goes to a Navy base to see his nephew Josh off to the Persian Gulf. He tells Josh how very proud he is of him and promises to look in on his wife and family during his deployment. In the final scene, Malone now decides to make films he feel people would appreciate, as well as Fayed and Ahmed as part of the crew, who have been pardoned for foiling the bombing. Malone is last seen working on a biographical film about President Kennedy.

==Cast==
* Kevin Farley as Michael Malone
* Kelsey Grammer as General George S. Patton
* Robert Davi as Aziz
* Serdar Kalsın as Ahmed
* Sammy Sheik as Fayed
* Geoffrey Arend as Mohammed
* Jon Voight as President George Washington
* James Woods as Todd Grosslight
* Chriss Anglin as President John F. Kennedy
* Leslie Nielsen as Grampa / Osama Bin Laden
* Jillian Murray as Heather
* Dennis Hopper as Judge Clarence Henderson
* Kevin Sorbo as George Mulrooney
* Travis Schuldt as Josh
* Trace Adkins as the angel of death / himself Bill OReilly as himself
* David Alan Grier as Rastus Malone (Michaels slave)
* Gary Coleman as Slave
* Mary Hart as Herself
* Fred Travalena as President Jimmy Carter

==Marketing== Republicans and Bill OReilly in the film. An American Carol has also been described by newspapers such as the Dallas Morning News as being "for the right wing".    
The American Conservative reported, "The movie has been promoted by bloggers on National Review Online. The Leadership Institute, an activist group that maintains contact with College Republicans nationwide, urged its charges to see the movie on opening weekend, even handing out tickets to its interns."   

==Reception==

===Critical=== David Zucker said the studio did not believe it would get a fair hearing due to its conservative political viewpoint.   
 average score normalized rating out of 100 to reviews from mainstream critics, the film has received an average score of 20, based on 12 reviews.    conservative contributor to Aint it Cool News said the film featured "ingenious comedy that we remember from Airplane!" and was "funny and inventive."  Kathleen Parker of Washington Post Writers Group called the film "An American Carol may not be The Best Movie You Ever Saw, but it’s something. It’s radical in its assault on the left wing; it’s brave given the risk of peer ridicule and the potential for career suicide. And it’s funny — if you like that sort of thing. Generally, I don’t." 

However, the film did have detractors, even among conservatives. Michael Brendan Doherty of The American Conservative wrote that:
 Is “An Episcopalian suppository bomber.”
 2004 spots, Arab terrorists fool Madeleine Albright by singing “Kumbaya.” In “Carol,” Hitler and his friends reach for their six-strings to serenade Neville Chamberlain, the Michael Moore stand-in, and a displeased Patton. Bill O’Reilly makes a cringe-inducing cameo.

Far from lampooning the Left, “Carol” insults conservatives by presuming that they are so simple as to be won over by fat jokes and flatulence. But the audience, imagining itself to be persecuted by Hollywood, is so grateful to be flattered by Zucker and company that they chuckle obediently at every cheap laff. Conservatives, once the scourge of coarsening culture, are happy to play crass as long as the joke is on liberals. }}

Steven Rea of The Philadelphia Inquirer gave the movie one star out of five, called it "jaw-droppingly awful," and "about as not-funny as a comedy can get."  Lou Lumenick of the New York Post wrote, "Even if it werent three years too late to parody Moore (ineptly played by Kevin Farley), Moores ridiculous tribute to Cuban health care in Sicko (film)|Sicko is far funnier than anything in this desperately laughless farce from David Zucker." 

On September 5, 2008, Michael Moore was a guest on Larry King Live and was shown a clip from the film where Malone (while lying down on his bed, drinking a Big Gulp and watching archival footage of inaugural address of John F. Kennedy|JFKs inaugural address) is startled by Kennedy, who materializes out of Malones television screen, and confronts him on his misguided views of American history. Moore said that he was vaguely familiar with the film, and then jokingly said he thought that Viggo Mortensen should be portraying him. When King asked him his opinion, Moore shrugged and said, "I hope its funny."

The Flop House podcast devoted an episode to this movie in March 2009, a large part of the hosts premise being that even if they were conservatives, the film was simply incompetently made. 

===Commercial===
An American Carol which opened on 1,639 screens nationwide, finished ninth at the box office that week, with a gross of $3.8 million, or a per-screen average of $2,325. For its second weekend, An American Carol had a 58.8 percent drop in box office receipts and dropped to #15, grossing $1,505,000 at 1,621 theaters or $928 per screen. 

The film faded in the box office in its third weekend dropping 73.8 percent and finishing #21 at 599 theaters grossing $365,000 or $609 per screen.  In its fourth weekend, it dropped to #41 at 109 theaters grossing $60,000 or $550 per screen. 

As of October 2009, An American Carol has grossed $7 million after having a production budget of $20 million. 

Zucker, in an interview with National Review Online, had suggested a sequel as his next possible project, but now says he is done making conservative comedies. Zucker stated that the audience for this type of film is one who waits for it to be available on DVD.   

==Home media== Vivendi Entertainment. 
 David Zucker and Kevin Farley along with several scenes and footage cut from the theatrical release.

==See also==
* List of ghost films Old Glory — 1939 cartoon
* Adaptations of A Christmas Carol

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 