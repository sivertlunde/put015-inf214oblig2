Affairs of Cappy Ricks
{{Infobox film
| name           = Affairs of Cappy Ricks
| image          = Walter brennan affairs of cappy ricks ss2.jpg
| image size     =
| caption        = Walter Brennan in the film
| director       = Ralph Staub
| producer       = Burt Kelly
| writer         = Lester Cole (writer) Peter B. Kyne (characters)
| narrator       = Frank Melton
| music          = Alberto Colombo Ernest Miller William Morgan
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Affairs of Cappy Ricks is a 1937 American film directed by Ralph Staub.

== Plot ==
Cappy Ricks (Walter Brennan) has returned home from a long voyage at sea only to find that his family and business are not as he left them.  His daughter Frankie (Mary Brian) is engaged to a dimwit that he isnt fond of.  His future mother-in-law has plans for his business and for his prized ship.  Cappy Ricks knows he has to end the chaos and set things straight.  Now all he needs is a plan.

== Cast ==
*Walter Brennan as Cappy Ricks
*Mary Brian as Frances Frankie Ricks
*Lyle Talbot as Bill Peck
*Frank Shields as Waldo P. Bottomly Jr. Frank Melton as Matt Peasely
*Georgia Caine as Mrs. Amanda Peasely
*Phyllis Barry as Ellen Ricks Peasely
*William B. Davidson as Waldo P. Bottomly Sr.
*Frank Shannon as Captain Braddock
*Howard Brooks as Revere the Butler

== Soundtrack ==
 

== External links ==
 
* 
* 

 
 
 
 
 
 
 
 


 