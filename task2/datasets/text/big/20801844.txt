The Same River Twice
The Same River Twice is a 2003 documentary by  . The excursion was captured in a short film, Riverdogs, a visual celebration of naked, exuberant youth set against the spectacular vistas of the Grand Canyon. Now, a quarter-century later, Moss tracks down five of his old comrades for a witty and insightful now-and-then portrait to see how they have fared after coping with children, careers, responsibilities, ageing and changing attitudes.” {{Cite web
  | last       = 
  | first      = 
  | authorlink = 
  | coauthors  = 
  | title      = Synopsis of The Same River Twice
  | work       = 
  | publisher  = Next Life Films
  | date       = 
  | url        = http://www.samerivertwice.com/film/index.html
  | doi        =  Cathy Shaw, shown in the 1970s as a young couple in love, and then in 2000, divorced with two children. {{Cite web
  | last       = 
  | first      = 
  | authorlink = 
  | coauthors  = 
  | title      = Principal Characters in The Same River Twice
  | work       = 
  | publisher  = Next Life Films
  | date       = 
  | url        = http://www.samerivertwice.com/film/characters.html
  | doi        = 
  | accessdate = }}  The film debuted at the Sundance Film Festival in 2003 {{Cite web
  | last       = 
  | first      = 
  | authorlink = 
  | coauthors  = 
  | title      = The Same River Twice
  | work       = 
  | publisher  = Sundance Channel (United States)
  | year       = 2003
  | url        = http://www.sundancechannel.com/films/500013435/
  | format     = 
  | doi        = 
  | accessdate = }}  and was nominated for two awards, the Truer Than Fiction Award at the Independent Spirit Awards in 2004 and the Grand Jury Prize.

The title refers to a traditional saying "you cannot step into the same river twice", which dates to Ancient Greek philosophy – see Panta rhei (Heraclitus).

The Same River Twice won Best Documentary at the Birmingham Sidewalk Moving Picture Festival in 2003 and Best Documentary Feature at the Nashville Film Festival in 2003.

==Notes==
 

==External links==
*  
*   at Rotten Tomatoes

 
 
 
 
 


 