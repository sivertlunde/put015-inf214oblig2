Ashok Kumar (film)
{{Infobox film
| name           = Ashok Kumar
| image          = Ashok kumar 1941.jpg
| caption        = Theatrical poster
| director       = Raja Chandrasekhar
| writer         =  Ranjan
| producer       = 
| distributor    = Murugan Talkies Film Company
| music          = Papanasam Sivan
| cinematography = 
| editing        = 
| released       = 7 October 1941
| runtime        =  Tamil
}}

Ashok Kumar was a 1941 Tamil-language film directed by Raja Chandrasekhar. It starred M. K. Thyagaraja Bhagavathar, Chittor V. Nagaiah, P. Kannamba, N. S. Krishnan, T. A. Madhuram, M. G. Ramachandran and Ranjan (actor)|Ranjan.

== Plot ==
 Lord Buddha and the king acquits of all the charges.

== Cast ==
* M. K. Thyagaraja Bhagavathar ... Prince Kunal
* Chittor V. Nagaiah ... Emperor Ashoka
* P. Kannamba ... Queen Tishyarakshita
* T. V. Kumudhini ... Wife of Prince Kunal
* M. G. Ramachandran ... Mahendran
* Nagercoil Mahadevan ... Buddha bhikshu Ranjan ... Gautama Buddha (credited as "R. Ramani")

== Production ==

Almost all of MKTs movies were great successes. And Ashok Kumar was no exception. Ashok Kumar premiered in 1941 and was a roaring success. A Tollywood actress Pasupuleti Kannamba played the role of Queen Tishyarakshita. Ashok Kumar was her second Tamil film and she did not know the language.    She was provided with a script in which Tamil words have been transliterated into Telugu. However, despite these issues, Kannamba did a commendable job with her dialogue delivery.
 Ranjan who later starred in hit movies as Mangamma Sabatham and Chandralekha (1948 film)|Chandralekha. The background music was provided by G. Ramanathan-Elangovan combine which later produced hits as Sivakavi and Haridas (1944 film)|Haridas. 

One of the highlights of the film was a song and dance sequence, Unnai kandu mayangaatha in which Kannamba dances to M. K. Thyagaraja Bhagavathars singing. The song was shot in a single night at Newtone Studio.

== Notes ==

 

== References ==

*  

== External links ==
*  
*  

 
 
 
 
 
 