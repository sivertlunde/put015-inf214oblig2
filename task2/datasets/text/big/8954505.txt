Autocine Mon Amour
{{Infobox film
| name = Autocine Mon Amour
| image =  Autocine72shot.jpg
| image size =
| caption =
| director = Fernando Siro
| producer =
| writer = Ángel Cortese  Carlos Alberto Cresté
| narrator =
| starring = Luis Brandoni Marta Bianchi
| music =
| cinematography =
| editing =
| distributor =
| released = 5 October 1972
| runtime =
| country = Argentina
| language = Spanish
| budget =
| preceded by =
| followed by =
}} Argentine comedy film directed by Fernando Siro and written by Ángel Cortese and Carlos Alberto Cresté.

==Cast==
*Luis Brandoni
*Marta Bianchi
*Ulises Dumont
*Ricardo Bauleo
*Gilda Lousek
*Maurice Jouvet
*Vicente Rubino
*Fernando Siro
*Nelly Beltrán
*Cristina Del Valle
*Claudio Levrino
*Oscar Viale
*Ovidio Fuentes
*Edgardo Cané
*Hugo Caprera

==Release and acclaim==
The film premiered on 5 October 1972.

==External links==
*  

 
 
 
 
 
 