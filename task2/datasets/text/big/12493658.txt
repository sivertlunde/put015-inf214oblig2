Departure of a Grand Old Man
{{Infobox film
| name           = Departure of a Grand Old Man (Уход великого старца)
| image          =
| caption        =
| director       = Yakov Protazanov Elizaveta Thiman
| producer       = Pavell Thiman
| writer         = Isaak Teneromo
| starring       = Olga Petrova Vladimir Shaternikov Mikhail Tamarov Elizaveta Thiman
| cinematography = Aleksandr Levitsky Georges Meyer
| editing        =
| studio         = P. Thiman and F. Reinhardt
| distributor    =
| released       =  
| runtime        = 31 minutes
| country        = Russian Empire Russian intertitles
| budget         =
| gross          =
}}
Departure of a Grand Old Man ( , Transliteration|translit.&nbsp;Ukhod velikovo startza) is a 1912 Russian silent film about the last days of author Leo Tolstoy. The film was directed by Yakov Protazanov and Elizaveta Thiman, and was actress Olga Petrovas first film.

==Plot==
The film depicts a group of peasants who come to the home of Leo Tolstoy to ask for land. We see the old sage attempting to help the peasants while his wife, Sofia Andreevna, is counting money and quarreling. 

Overwhelmed, Tolstoy is driven to the edge of suicide. It ends with Tolstoy on his death bed being visited by Christ. The film was banned because of the negative portrayal of Tolstoys wife Sofia Tolstaya, who threatened to sue the filmmakers for libel. 

==Cast==
*Olga Petrova as Sofia Tolstaya
*Vladimir Shaternikov as Leo Tolstoy
*Leo Tolstoy as himself (from documentary footage filmed by A. Drankov)
*Mikhail Tamarov as Vladimir Chertkov
*Elizaveta Thiman as Alexandra Lvovna Tolstaya

==References==
 

==Citations==
*Kenez, Peter, Cinema and Soviet Society: 1917-1953 (Cambridge: Cambridge University Press, 1992)

==External links==
* 

 
 

 
 
 
 
 
 

 
 