Merry Dog
{{Infobox Hollywood cartoon|
| cartoon_name = Merry Dog
| series = Pooch the Pup
| image = 
| caption = 
| director = Walter Lantz
| story_artist = 
| animator = Manuel Moreno Lester Kline George Cannata Bill Weber Fred Kopietz Charles Hastings
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = January 2, 1933
| color_process = Black and white
| runtime = 8:02 English
| Cats and Dogs
| followed_by = The Terrible Troubadour
}}

Merry Dog is a short animated film by Walter Lantz Productions, and is the sixth of the Pooch the Pup theatrical cartoons.

==Plot==
It is Christmas time, and Santa Claus flies across the evening sky in his reindeer-drawn sleigh. Down on the snowy terrain, Pooch rides on a dachshund-pulled sled, and sings a jazzy version of the song Jingle Bells. On the way, a hungry husky spots and starts to pursue the dog.

Pooch arrives at a house. Opening the door and letting him in is his friend the girl coonhound. The husky also arrives just outside but struggles to get in. Inside the house, Pooch recites the poem A Visit from St. Nicholas. A rat in a hole appears to be bothered by some words in the poem, and therefore comes out to play. The rats play then disturbs a cat which resulted a chase.

While still struggling to get in the house, the husky notices the sleigh of Santa Claus landing nearby. The husky captures and ties up Santa. The husky then dons Santas clothes and beard before going down into the chimney.

The impostor Santa shows up in the house, much to the amazement of Pooch and the girl coonhound. The two dogs are further delighted when they are invited by him to go on a sleigh ride. Momentarily, the cat and a pack of rats, in a chase, jump into the fake Santas beard. A scuffle in it occurs until the beard falls off, and the husky is exposed. Pooch and the girl coonhound attempt to run but the husky manages to catch their legs. Hopefully, the toy soldiers and other toys in the house come to life, and go on to help the two dogs by disrupting the husky. This goes on until Pooch places a noose around the huskys legs and has him hanging on the ceiling. The real Santa finally breaks from the ropes, and enters the house. To get back at the perpetrator, Santa delivers a strong straight punch which sends the husky airborne many yards away.

In the house several minutes later, Pooch and the girl coonhound are sitting on the laps of Santa who recites the poem Pooch recited earlier. The cat returns and expresses liking to the recitation after having consumed all the rats.

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 
 
 
 
 
 
 


 